﻿<%@ Page Language="C#" AutoEventWireup="true" EnableViewState="True" CodeBehind="frmRptLegalConsultationReport.aspx.cs"
    Inherits="HTP.Reports.frmRptLegalConsultationReport" EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Online Legal Consultation</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <link href="../Styles.css" type="text/css" rel="stylesheet" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <%-- <style type="text/css">
        .modalBackground
        {
            background-color: Gray;
            filter: alpha(opacity=70);
            opacity: 0.7;
        }
        .modalPopup
        {
            background-color: #FFD9D5;
            border-width: 3px;
            border-style: solid;
            border-color: Gray;
            padding: 3px;
            width: 250px;
            position: static;
            left: 200px;
            top: -65px;
        }
        A:link
        {
            font-size: 8pt;
            font-family: Verdana,Arial, Helvetica, sans-serif;
            color: #4169E1;
            text-decoration: none;
            font-weight: bold;
        }
        A:Visited
        {
            font-size: 8pt;
            font-family: Verdana,Arial, Helvetica, sans-serif;
            color: #4169E1;
            text-decoration: none;
            font-weight: bold;
        }
    </style>--%>
</head>
<body>
    <form id="form1" runat="server">
    &nbsp;<div>
        <aspnew:ScriptManager ID="ScriptManager2" runat="server" />
        <table id="TableMain" align="center" border="0" cellpadding="0" cellspacing="0" style="z-index: 101"
            width="780">
            <tbody>
                <tr>
                    <td style="height: 14px">
                        <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td background="../images/separator_repeat.gif" height="11">
                    </td>
                </tr>
                <tr>
                    <td background="../Images/subhead_bg.gif" class="clssubhead" align="right" style="height: 34px;">
                        <table style="width: 100%;">
                            <tr>
                                <td background="../Images/subhead_bg.gif" height="34" class="clssubhead" align="right">
                                    <aspnew:UpdatePanel ID="updatepnlpaging" runat="server" RenderMode="Inline">
                                        <ContentTemplate>
                                            <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                        </ContentTemplate>
                                    </aspnew:UpdatePanel>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                
                <tr>
                    <td align="center">
                        <asp:Label ID="lblMessage" runat="server" CssClass="label" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" colspan="2">
                        <aspnew:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpnlGvw">
                            <ProgressTemplate>
                                <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl1" runat="server" Text="Please Wait ......"
                                    CssClass="clsLabel"></asp:Label>
                            </ProgressTemplate>
                        </aspnew:UpdateProgress>
                        <aspnew:UpdatePanel ID="UpnlGvw" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="gv_records" runat="server" AutoGenerateColumns="False" Width="100%"
                                    AllowPaging="True" OnPageIndexChanging="gv_records_PageIndexChanging" CellPadding="0"
                                    PageSize="30" CssClass="clsLeftPaddingTable" AllowSorting="True">
                                    <Columns>
                                        <asp:TemplateField HeaderText="S#" HeaderStyle-Width="30px">
                                            <ItemTemplate>
                                                <asp:HyperLink ID="hl_Sno" CssClass="clssubhead" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                    runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SNo") %>'></asp:HyperLink>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Name">
                                            <ItemTemplate>
                                                <asp:Label runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.ClientName") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" Width="20%" />
                                            <ItemStyle HorizontalAlign="Left" CssClass="clsLeftPaddingTable" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Comments">
                                            <ItemTemplate>
                                                <div id="div1" style="overflow: auto; height: 45px; width: 450px;">
                                                    <asp:Label ID="lblcomments" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.Comments")%>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <%--<asp:BoundField DataField="Comments" HeaderText="Comments" HtmlEncode="false">
                                            <ItemStyle CssClass="GridItemStyle" />
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        </asp:BoundField>--%>
                                        <%--Yasir 5357 12/24/2008   Date on which comment was  added--%>
                                        <%--<asp:TemplateField HeaderText="Recieve Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblrecievedate" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.RecieveDate")%>'></asp:Label><br />
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" Width="18%" />
                                        </asp:TemplateField>--%>
                                        <%--End Yasir 5357 12/24/2008 --%>
                                        <%--<asp:TemplateField HeaderText="Contact Info">
                                            <ItemTemplate>
                                                <asp:Label ID="lblcontact1" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.contact1")%>'></asp:Label><br />
                                                <asp:Label ID="lblcontact2" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.contact2")%>'></asp:Label><br />
                                                <asp:Label ID="lblcontact3" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.contact3")%>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" Width="18%" />
                                        </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="Contact Status">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCallBackStatus" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.Description")%>'></asp:Label><br />
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" Width="14%" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Record Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRecieveDate" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.RecieveDate")%>'></asp:Label><br />
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" Width="10%" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Center" />
                                    <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                        FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                </asp:GridView>
                            </ContentTemplate>
                        </aspnew:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td width="100%" background="../Images/separator_repeat.gif" height="11">
                    </td>
                </tr>
                <tr>
                    <td>
                        <uc1:Footer ID="Footer1" runat="server" />
                    </td>
                </tr>
                <tr>
                </tr>
            </tbody>
        </table>
    </div>
    </form>
</body>
</html>

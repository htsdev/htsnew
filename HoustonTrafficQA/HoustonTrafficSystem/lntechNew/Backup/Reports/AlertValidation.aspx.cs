using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;


namespace HTP.Reports
{
    public partial class AlertValidation : System.Web.UI.Page
    {
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        clsLogger bugTracker = new clsLogger();
        clscalls clscall = new clscalls();

        // Noufil 3498 04/0/2008 new file

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindGrid();
            }
            Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
        }


        protected void BindGrid()
        {
            DataTable dt = clscall.getvalidationreport();
            //DataSet ds = (DataSet) dt;

            if (dt.Rows.Count == 0)
            {
                pngrid.Visible = false;
                lblMessage.Text = "No records found";
            }
            if (dt.Columns.Contains("sno") == false)
            {
                dt.Columns.Add("sno");
                int sno = 1;
                if (dt.Rows.Count >= 1)
                    dt.Rows[0]["sno"] = 1;
                if (dt.Rows.Count >= 2)
                {
                    for (int i = 1; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[i - 1]["ticketid"].ToString() != dt.Rows[i]["ticketid"].ToString())
                        {
                            dt.Rows[i]["sno"] = ++sno;
                        }
                    }
                }
            }

            gv_records.DataSource = dt;
            gv_records.DataBind();

            Pagingctrl.PageCount = gv_records.PageCount;
            Pagingctrl.PageIndex = gv_records.PageIndex;
            Pagingctrl.SetPageIndex();
        }

        protected void gv_records_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            lblMessage.Visible = false;
            if (e.CommandName == "ticket")
            {
                int ticketid = Convert.ToInt32(e.CommandArgument);
                DataTable dt1 = clscall.Getcontactinfo(ticketid);
                if (dt1.Rows.Count > 0)
                {
                    gv_info.DataSource = dt1;
                    gv_info.DataBind();
                    gv_info.Visible = true;
                    ModalPopupExtender1.Show();
                    lblPopupmessage.Text = "";
                }
                else
                {
                    gv_info.Visible = false;
                    lblPopupmessage.Text = "No Reocrds Found";
                    ModalPopupExtender1.Show();

                }
            }
        }
        void Pagingctrl_PageIndexChanged()
        {
            gv_records.PageIndex = Pagingctrl.PageIndex - 1;
            BindGrid();
        }

        protected void gv_records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            if (e.NewPageIndex != -1) 
            {
                gv_records.PageIndex = e.NewPageIndex;
                BindGrid();
            }
        }
    }
}

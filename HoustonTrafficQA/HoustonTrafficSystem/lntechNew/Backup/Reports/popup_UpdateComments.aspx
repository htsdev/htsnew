<%@ Page Language="C#" AutoEventWireup="true" Codebehind="popup_UpdateComments.aspx.cs"
    Inherits="lntechNew.Reports.popup_UpdateComments" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Comments</title>
    <base target="_self" />
    <link href="../Styles.css" type="text/css" rel="stylesheet" />

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>

    <script type="text/javascript">
    function submitform()
    {
   
    
    var recordID = '<% = Request.QueryString["RecordID"].ToString() %>';
    var values = document.getElementById('txt_Comments').value;
    
    if(values.length>1000)
    {
    alert('Comments must be less than 1000 characters.');
    return(false);
    }
   
    window.returnValue = values;
    window.close();
   
    }
    
    function cancel()
    {
    
    window.returnValue=-1;
    window.close();
    }
    
   function RefreshPage()
   {
//   window.location.reload();
   }
    
    </script>

</head>
<body onload="RefreshPage()">
    <form id="form1" runat="server">
        <div>
            <table id="TableMain" 
                cellspacing="0" cellpadding="0" align="left" border="0">
                <tr>
                    <td background="../../images/separator_repeat.gif" colspan="2" height="11">
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table class="clsLeftPaddingTable" id="tblsub" height="20" cellspacing="1" cellpadding="0"
                            width="510" align="center" border="0">
                            <tr>
                            </tr>
                            <tr>
                                <td class="clsaspcolumnheader" height="22">
                                    Name</td>
                                <td height="22">
                                    <asp:Label ID="lbl_Name" runat="server" Width="176px"></asp:Label></td>
                            </tr>
                            <tr>
                                <td class="clsaspcolumnheader" height="22">
                                    &nbsp;Comments&nbsp;</td>
                                <td height="22" valign="top">
                                    <p>
                                        &nbsp;</p>
                                    <p>
                                        <asp:TextBox ID="txt_Comments" runat="server" Width="100%" Height="60px" TextMode="MultiLine" MaxLength="1000"></asp:TextBox>&nbsp;</p>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:TextBox ID="txtbefore" runat="server" Width="10px" Visible="False"></asp:TextBox></td>
                                <td align="right">
                                    <asp:Button ID="Btn_TrialNotes_Submit" runat="server" Width="80px" Text="Submit"
                                        CssClass="clsbutton" OnClientClick=" return submitform()"></asp:Button></td>
                            </tr>
                        </table>
                        <asp:Label ID="lbl_Message" runat="server" Visible="False" ForeColor="Red"></asp:Label>
                    </td>
                    <tr>
                        <td background="../../images/separator_repeat.gif" colspan="2" height="22" style="height: 22px">
                        </td>
                    </tr>
            </table>
        </div>
    </form>
</body>
</html>

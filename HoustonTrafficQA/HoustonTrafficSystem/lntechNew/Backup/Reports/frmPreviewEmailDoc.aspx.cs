using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using FrameWorkEnation.Components;

namespace lntechNew.Reports
{
	/// <summary>
	/// Summary description for frmPreviewEmailDoc.
	/// </summary>
	public partial class frmPreviewEmailDoc : System.Web.UI.Page
	{
		clsENationWebComponents ClsDb = new clsENationWebComponents();
		private void Page_Load(object sender, System.EventArgs e)
		{
            //Change by Ajmal
            //SqlDataReader dr_email=null;
			IDataReader dr_email=null;
			try
			{
				int Emailid=Convert.ToInt32(Request.QueryString["emailid"]);
				string[] key1 = {"@Emailid"};		
				object[] value11 = {Emailid};
				dr_email=ClsDb.Get_DR_BySPArr("usp_hts_get_clientemailinfo",key1,value11);
				dr_email.Read();			
				string lnk=dr_email["filename"].ToString();
				Response.ClearContent(); 
				Response.ClearHeaders(); 
				Response.ContentType = "application/pdf"; 
				Response.WriteFile(lnk);
				Response.Flush(); 
				Response.Close(); 
			}
			finally
			{
				dr_email.Close();
			}
			
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}

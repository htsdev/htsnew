using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.Reports
{
    public partial class AuthorizeNetReport : System.Web.UI.Page
    {
        clsLogger bugTracker = new clsLogger();
        clsSession ClsSession = new clsSession();
        DataTable dtRecords;
        private clsENationWebComponents clsDb = new clsENationWebComponents();
        private NewClsPayments objPayments = new NewClsPayments();

        public SortDirection GridViewSortDirection
        {

            get
            {

                if (ViewState["sortDirection"] == null)

                    ViewState["sortDirection"] = SortDirection.Ascending;

                return (SortDirection)ViewState["sortDirection"];

            }

            set { ViewState["sortDirection"] = value; }

        }

        private void SortGridView(string sortExpression, string direction)
        {


            GetRecords();

            DataTable dt = dtRecords;

            DataView dv = new DataView(dt);

            dv.Sort = sortExpression + " " + direction;

            gv_Records.DataSource = dv;

            gv_Records.DataBind();

        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (ClsSession.IsValidSession(this.Request) == false)
                Response.Redirect("../frmLogin.aspx", false);

            lbl_Message.Text = "";
            if (!IsPostBack)
            {
                try
                {
                    GetRecords();
                    if (dtRecords.Rows.Count == 0)
                        lbl_Message.Text = "No Records!";
                    gv_Records.DataSource = dtRecords;
                    gv_Records.DataBind();
                }
                catch (Exception ex)
                {
                    lbl_Message.Text = ex.Message;
                    bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                }
            }
        }

        private void GetRecords()
        {
            dtRecords = objPayments.GetAuthorizeNetReport(chk_ShowTestTrans.Checked, cal_startdate.SelectedDate, cal_enddate.SelectedDate, ddl_Database.SelectedValue, ddl_source.SelectedValue, ddl_transtype.SelectedValue);

        }

        protected void gv_Records_Sorting(object sender, GridViewSortEventArgs e)
        {
            string sortExpression = e.SortExpression;
            Session["SortExpression"] = sortExpression;

            if (GridViewSortDirection == SortDirection.Ascending)
            {

                GridViewSortDirection = SortDirection.Descending;
                Session["SortDirection"] = " desc";
                SortGridView(sortExpression, " desc");

            }

            else
            {

                GridViewSortDirection = SortDirection.Ascending;
                Session["SortDirection"] = " asc";
                SortGridView(sortExpression, " asc");

            }
        }

        protected void chk_ShowTestTrans_CheckedChanged(object sender, EventArgs e)
        {
            GetRecords();
            gv_Records.DataSource = dtRecords;
            gv_Records.DataBind();
        }

        protected void btn_Submit_Click(object sender, EventArgs e)
        {
            try
            {
                GetRecords();
                if (dtRecords.Rows.Count == 0)
                    lbl_Message.Text = "No Records!";
                gv_Records.DataSource = dtRecords;
                gv_Records.DataBind();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void ddl_Database_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void gv_Records_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (ddl_Database.SelectedValue == "2")
                    ((HyperLink)e.Row.FindControl("hl_TicketNumber")).NavigateUrl = ConfigurationManager.AppSettings["DallasURL"] + "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + ((HiddenField)e.Row.FindControl("hf_ticketid")).Value;
            }
        }
    }
}

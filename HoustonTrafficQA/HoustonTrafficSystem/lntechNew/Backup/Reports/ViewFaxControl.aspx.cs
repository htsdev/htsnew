﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using lntechNew.Components.ClientInfo;

namespace lntechNew.Reports
{
    public partial class ViewFaxControl : System.Web.UI.Page
    {
        clsSession uSession = new clsSession();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                // Noufil 3999 05/09/2008 Setting fax control properties  
                Faxcontrol1.Ticketid = Convert.ToInt32(Session["ticketid"].ToString());
                Faxcontrol1.Attachment = Session["reportpath"].ToString();
                Faxcontrol1.Empid = Convert.ToInt32(uSession.GetCookie("sEmpID", this.Request));
                
                Faxcontrol1.FieldSet = true;
            }
        }
    }
}

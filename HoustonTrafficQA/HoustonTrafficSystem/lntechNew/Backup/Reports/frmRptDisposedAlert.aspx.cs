﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;
using HTP.Components;

namespace HTP.Reports
{
    /// <summary>
    /// <Generate report for all client cases with verified status  of 'Disposed' but an Auto status of 'Jury, Judge, PreTrial and Arraignment' with future court date>
    /// </summary>
    public partial class frmRptDisposedAlert : System.Web.UI.Page
    {
        #region Variables
        clsCase cCase = new clsCase();
        ValidationReports reports = new ValidationReports();
        BussinessLogic Blogic = new BussinessLogic();
        clsLogger bugTracker = new clsLogger();
        int Report_ID = 0;
        int day = 0;
        #endregion
        #region Properties        
        public SortDirection GridViewSortDirection
          
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Ascending;
                return (SortDirection)ViewState["sortDirection"];
            }
            set { ViewState["sortDirection"] = value; }

        }
        public string GridViewSortExpression
        {
            get
            {
                if (ViewState["sortExpression"] == null)
                    ViewState["sortExpression"] = "";
                return ViewState["sortExpression"].ToString();
            }
            set { ViewState["sortExpression"] = value; }

        }
        #endregion 
        #region Methods
        /// <summary>
        /// <Populate grid from database>
        /// </summary>
        private void FillGrid()
        {
            DataSet ds = new DataSet();
            reports.getRecords("USP_HTP_GET_disposedAlert");
            ds = reports.records;
            if (ds.Tables[0].Rows.Count > 0)
            {
                ds.Tables[0].Columns.Add("sno");
                DataView dv = ds.Tables[0].DefaultView;
                dv.Sort = GridViewSortExpression + " " + (GridViewSortDirection == SortDirection.Ascending ? "ASC" : "DESC");
                DataTable dt = dv.ToTable();
                GenerateSerialNo(dt);
                Pagingctrl.GridView = gv_Records;
                gv_Records.DataSource = dt;
                gv_Records.DataBind();
            }
            else    //SAEED-7752-05/06/2010, if no record found set gridview to null, so that it will display "No Records Found"  message.
            {
                gv_Records.DataSource = null;
                gv_Records.DataBind();
            }

            Pagingctrl.PageCount = gv_Records.PageCount;
            Pagingctrl.PageIndex = gv_Records.PageIndex;
            Pagingctrl.SetPageIndex();
        }
        /// <summary>
        /// <Populate grid>
        /// </summary>
        void UpdateFollowUpInfo2_PageMethod()
        {
           
            {
                FillGrid();
            }
        }
        
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            
            {
                if (pageSize > 0)
                {
                    gv_Records.PageIndex = 0;
                    gv_Records.PageSize = pageSize;
                    gv_Records.AllowPaging = true;
                }
                else
                {
                    gv_Records.AllowPaging = false;
                }
                FillGrid();
            }
        }
        void Pagingctrl_PageIndexChanged()
        {

            {
                gv_Records.PageIndex = Pagingctrl.PageIndex - 1;
                FillGrid();
            }
        }
        
        /// <summary>
        /// <Generate Serial Number>
        /// </summary>
        /// <param name="dtRecords"></param>
        public void GenerateSerialNo(DataTable dtRecords)
        {
            int sno = 1;
            if (!dtRecords.Columns.Contains("sno"))
                dtRecords.Columns.Add("sno");


            if (dtRecords.Rows.Count >= 1)
                dtRecords.Rows[0]["sno"] = 1;

            if (dtRecords.Rows.Count >= 2)
            {
                for (int i = 1; i < dtRecords.Rows.Count; i++)
                {
                    if (dtRecords.Rows[i - 1]["ticketid_pk"].ToString() != dtRecords.Rows[i]["ticketid_pk"].ToString())
                    {
                        dtRecords.Rows[i]["sno"] = ++sno;
                    }
                }
            }

        }
        #endregion
        #region Events        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack == false)
            {
               
                gv_Records.PageIndex = 0;
                GridViewSortDirection = SortDirection.Ascending;
                GridViewSortExpression = "FollowUpDate";
                FillGrid();
            }
            UpdateFollowUpInfo2.PageMethod += new HTP.WebControls.PageMethodHandler(UpdateFollowUpInfo2_PageMethod);
            Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
            Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
            Pagingctrl.GridView = gv_Records;
        }
        protected void gv_Records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_Records.PageIndex = e.NewPageIndex;
                FillGrid();
            }
            catch (Exception ex)
            {

                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        protected void gv_Records_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                string sortExpression = e.SortExpression;

                if (GridViewSortDirection == SortDirection.Ascending && sortExpression == GridViewSortExpression)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                else
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }

                GridViewSortExpression = sortExpression;
                FillGrid();
                Pagingctrl.PageCount = gv_Records.PageCount;
                Pagingctrl.PageIndex = gv_Records.PageIndex;
            }
            catch (Exception ex)
            {
                    lbl_Message.Text = ex.Message;
                    clsLogger.ErrorLog(ex);
            }
        }
        protected void gv_Records_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "btnclick")
            {
                int RowID = Convert.ToInt32(e.CommandArgument);
                string firstname = (((HiddenField)gv_Records.Rows[RowID].FindControl("hf_fname")).Value);
                string causeno = (((HiddenField)gv_Records.Rows[RowID].FindControl("hf_causeno")).Value);
                string ticketno = (((HiddenField)gv_Records.Rows[RowID].FindControl("hf_ticketnumber")).Value);
                string lastname = (((HiddenField)gv_Records.Rows[RowID].FindControl("hf_lname")).Value);
                string courtname = (((HiddenField)gv_Records.Rows[RowID].FindControl("hf_courtid")).Value);
                string followupDate = (((Label)gv_Records.Rows[RowID].FindControl("lbl_FollowUpDate")).Text);
               
                cCase.TicketID = Convert.ToInt32((((HiddenField)gv_Records.Rows[RowID].FindControl("hf_ticketno")).Value));
                string comm = cCase.GetGeneralCommentsByTicketId();
                UpdateFollowUpInfo2.Freezecalender = true;

                
               
                    UpdateFollowUpInfo2.Title = "Disposed Alert Followup Date";
                    
                    UpdateFollowUpInfo2.Freezecalender = true;
                    string url = Request.Url.AbsolutePath.ToString();
                    DataTable dtDays = Blogic.GetBusinessLogicInformationByURL(url, "Days");
                    if (dtDays.Rows.Count > 0)
                        Report_ID = Convert.ToInt32(dtDays.Rows[0]["Report_ID"]);

                    dtDays = Blogic.GetBusinessLogicSetDaysInformation("Days", Report_ID.ToString());
                    if (dtDays.Rows.Count > 0)
                        day = Convert.ToInt32(dtDays.Rows[0]["Attribute_Value"].ToString());

                    UpdateFollowUpInfo2.binddate(day.ToString(), clsGeneralMethods.GetBusinessDayDate(DateTime.Now, day), DateTime.Today, firstname, lastname, ticketno, cCase.TicketID.ToString(), causeno, comm, mpeTrafficwaiting.ClientID, "", HTP.Components.FollowUpType.DisposeAlert.ToString(), followupDate);
                    UpdateFollowUpInfo2.followUpType = HTP.Components.FollowUpType.DisposeAlert;
                    
                
                

                mpeTrafficwaiting.Show();
                Pagingctrl.Visible = true;
            }

        }
        protected void gv_Records_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    
                    ((ImageButton)e.Row.FindControl("img_Add")).CommandArgument = e.Row.RowIndex.ToString();
                }
            }
            catch (Exception)
            {

            }
        }
        #endregion

      
        

    }
}

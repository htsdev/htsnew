<%@ Page Language="C#" AutoEventWireup="true" Codebehind="AuthorizeNetReport.aspx.cs"
    Inherits="lntechNew.Reports.AuthorizeNetReport" %>

<%@ Register Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2"
    Namespace="eWorld.UI" TagPrefix="ew" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc1" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Authorize.Net Report</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
    
    function SelectionChanged()
    {//a;
//        var ddl_db = document.getElementById('ddl_Database');
//        var ddl_src = document.getElementById('ddl_Source');
//        if(ddl_db.value == '2')
//        {
//            ddl_src.value = '1';
//            ddl_src.disabled = true;
//        }
//        else
//        {
//            ddl_src.disabled = false;
//        }
    }
    
    
    function CheckTransactionType()
    {
        //Zeeshan Ahmed 3931 05/27/2008
        var ddl_db = document.getElementById('ddl_Database');
        var ddl_src = document.getElementById('ddl_Source');
    
        if(ddl_db.value == '2')
        {
            if ( ddl_src.value == '2')
            {
                alert("Manual CC transactions not exists for Dallas.")
                return false;
            }
        }
        return true;
    }
   
    </script>
</head>
<body onload="SelectionChanged()">
    <form id="form1" runat="server">
        <div>
            <table cellspacing="0" cellpadding="0" width="780" align="center" border="0">
                <tr>
                    <td>
                        <uc1:ActiveMenu ID="ActiveMenu1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="center" background="../Images/separator_repeat.gif" style="height: 16px">
                    </td>
                </tr>
                <tr>
                    <td align="left" style="height: 16px">
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 140px; height: 23px;">
                                    <asp:Label ID="Label1" runat="server" CssClass="clslabel" Text="Date: "></asp:Label>
                                    <ew:CalendarPopup ID="cal_startdate" runat="server" ControlDisplay="TextBoxImage" ImageUrl="../Images/calendar.gif"
                                        Width="86px">
                                        <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Black" />
                                        <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Black" />
                                        <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Gray" />
                                        <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Black" />
                                        <TextboxLabelStyle CssClass="clsinputadministration" />
                                    </ew:CalendarPopup>
                                </td>
                                <td style="height: 23px; width: 107px;">
                                    <ew:CalendarPopup ID="cal_enddate" runat="server" ControlDisplay="TextBoxImage" ImageUrl="../Images/calendar.gif"
                                        Width="86px">
                                        <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Black" />
                                        <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <TextboxLabelStyle CssClass="clsinputadministration" />
                                        <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Black" />
                                        <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Gray" />
                                        <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Black" />
                                    </ew:CalendarPopup>
                                </td>
                                <td style="width: 130px; height: 23px;">
                                    <asp:Label ID="Label2" runat="server" CssClass="clslabel" Text="Database: "></asp:Label>
                                    <asp:DropDownList ID="ddl_Database" runat="server" CssClass="clsinputcombo" onChange = "SelectionChanged()">
                                        <asp:ListItem Selected="True" Value="1">Houston</asp:ListItem>
                                        <asp:ListItem Value="2">Dallas</asp:ListItem>
                                    </asp:DropDownList></td>
                                <td style="width: 141px; height: 23px;">
                                    <asp:Label ID="Label3" runat="server" CssClass="clslabel" Text="Source: "></asp:Label><asp:DropDownList
                                        ID="ddl_source" runat="server" CssClass="clsinputcombo">
                                        <asp:ListItem Value="-1">All Sources</asp:ListItem>
                                        <asp:ListItem Value="0">Public Site</asp:ListItem>
                                        <asp:ListItem Value="1">Traffic System</asp:ListItem>
                                        <asp:ListItem Value="2">Manual CC</asp:ListItem>
                                    </asp:DropDownList></td>
                                <td style="width: 185px; height: 23px;">
                                    <asp:Label ID="Label4" runat="server" CssClass="clslabel" Text="Transaction Type: "></asp:Label><asp:DropDownList
                                        ID="ddl_transtype" runat="server" CssClass="clsinputcombo">
                                        <asp:ListItem Selected="True" Value="-1">All</asp:ListItem>
                                        <asp:ListItem Value="1">Approved</asp:ListItem>
                                        <asp:ListItem Value="2">Void</asp:ListItem>
                                    </asp:DropDownList></td>
                                <td style="height: 23px">
                                    <asp:Button ID="btn_Submit" runat="server" CssClass="clsbutton" Text="Submit" OnClick="btn_Submit_Click" OnClientClick="return CheckTransactionType();" /></td>
                            </tr>
                        </table>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" background="../Images/separator_repeat.gif" style="height: 16px">
                    </td>
                </tr>
                <tr>
                    <td style="height: 16px" align="left">
                        <asp:CheckBox ID="chk_ShowTestTrans" runat="server" Checked="True"
                            OnCheckedChanged="chk_ShowTestTrans_CheckedChanged" Text="Show Test Transactions"
                            CssClass="clslabelnew" /></td>
                </tr>
                <tr>
                    <td align="center">
                        &nbsp;<asp:Label ID="lbl_Message" runat="server" CssClass="clslabel" ForeColor="Red"></asp:Label></td>
                </tr>
                <tr>
                    <td align="center" style="height: 16px">
                        <asp:GridView ID="gv_Records" runat="server" AutoGenerateColumns="False" CssClass="clsleftpaddingtable"
                            Width="100%" AllowSorting="True" OnSorting="gv_Records_Sorting" OnRowDataBound="gv_Records_RowDataBound">
                            <Columns>
                                <asp:TemplateField HeaderText="Trans ID" SortExpression="TransID">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_TransID" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.TransID") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Ticket Number" SortExpression="TicketNumber">
                                    <ItemTemplate>
                                        &nbsp;<asp:HyperLink ID="hl_TicketNumber" runat="server" NavigateUrl='<%# "../ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.TicketID") %>'
                                            Text='<%# DataBinder.Eval(Container, "DataItem.TicketNumber") %>'></asp:HyperLink><asp:HiddenField
                                                ID="hf_ticketid" Value='<%# DataBinder.Eval(Container, "DataItem.TicketID") %>' runat="server" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Invoice Number" SortExpression="InvoiceNumber">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_InvoiceNumber" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.InvoiceNumber") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Trans Status" SortExpression="TransStatus">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_TransStatus" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.TransStatus") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Submit Date" SortExpression="SubmitDate">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_SubmitDate" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.SubmitDate") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Customer" SortExpression="Customer">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_Customer" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.Customer") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Card" SortExpression="Card">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_Card" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.Card") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sales Rep" SortExpression="SalesRep">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_SalesRep" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.SalesRep") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Payment Method" SortExpression="PaymentMethod">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_PaymentMethod" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentMethod") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Payment Amount" SortExpression="Amount">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_PaymentAmount" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.Amount","{0:C}") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Void" SortExpression="isVoid">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_Void" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.isVoid") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Appr." SortExpression="isApproved">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_approved" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.isApproved") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Source" SortExpression="Source">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_Source" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.Source") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td align="center" background="../Images/separator_repeat.gif" style="height: 16px">
                    </td>
                </tr>
                <tr>
                    <td>
                        <uc2:Footer ID="Footer" runat="server" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>

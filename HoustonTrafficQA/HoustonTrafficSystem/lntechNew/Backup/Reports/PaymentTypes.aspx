﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PaymentTypes.aspx.cs" Inherits="HTP.Reports.PaymentTypes" %>

<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Payment Types</title>
    
        <script type="text/javascript">
    
    function ValidateAddCategory() {
     
        var txt = document.getElementById('txtPaymentType');
        var shortdesc = document.getElementById('txtShortDesc');
        
        
        if(txt.value == '')
        {
            alert('Please enter some text for the payment description');
            txt.focus();
            return(false);
        }
        if (shortdesc == '')
        {
            alert('Please enter some text for short payment description');
            shortdesc.focus();
            return(false);
        }
        
    }

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <aspnew:ScriptManager ID="ScriptManager1" runat="server" />
        <table cellspacing="0" cellpadding="0" width="900px" align="center" border="0">
            <tr>
                <td>
                    <uc1:ActiveMenu ID="ActiveMenu1" runat="server" />
                </td>
            </tr>
            <tr>
                <td style="width: 100%" background="../Images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td align="center" style="width: 819px">
                    <table style="width: 622px">
                        <tr>
                            <td style="width: 234px;">
                                <asp:Label ID="Lbl_PaymentType" runat="server" CssClass="clsSubhead" Text="Payment Type:"
                                    Width="123px"></asp:Label>
                            </td>
                            <td style="width: 239px">
                                &nbsp;<asp:TextBox ID="txtPaymentType" runat="server" CssClass="clsinputadministration"
                                    Width="200px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 234px;">
                                <asp:Label ID="Lbl_ShortDesc" runat="server" CssClass="clsSubhead" Text="Short Description:"
                                    Width="123px"></asp:Label>
                            </td>
                            <td style="width: 239px">
                                &nbsp;<asp:TextBox ID="txtShortDesc" runat="server" CssClass="clsinputadministration" Width="200px"></asp:TextBox>
                            </td>
                            <td style="width: 266px" align="left">
                                <asp:Button ID="btnAdd" runat="server" CssClass="clsbutton" OnClick="btnAdd_Click"
                                    OnClientClick="return ValidateAddCategory()" Text="Add" Width="60px" />
                            &nbsp;<asp:Button ID="btnUpdate" runat="server" CssClass="clsbutton" OnClick="btnUpdate_Click"
                                    OnClientClick="return ValidateAddCategory()" Text="Update" Width="60px" 
                                    Visible="False" />
                            &nbsp;<asp:Button ID="btnCancel" runat="server" CssClass="clsbutton" 
                                    OnClick="btnCancel_Click" Text="Cancel" Width="60px" Visible="False" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 234px;">
                                <asp:Label ID="LblIsActive" runat="server" CssClass="clsSubhead" Text="Is Active"
                                    Width="123px"></asp:Label>
                            </td>
                            <td style="width: 239px">
                                <asp:CheckBox ID="ChkIsActive" runat="server" />
                            </td>
                            <td style="width: 266px" align="left">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width: 234px;">
                                &nbsp;
                            </td>
                            <td colspan="2">
                                &nbsp;
                                <asp:HiddenField ID="HF_PaymentTypeId" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td background="../Images/subhead_bg.gif" class="clssubhead" align="left" style="height: 34px">
                    <table width="100%">
                        <tr>
                            <td align="left" class="clssubhead">
                                Payment types
                            </td>
                            <td align="right" class="clssubhead">
                                <aspnew:UpdatePanel ID="updatepnlpaging" runat="server" RenderMode="Inline">
                                    <ContentTemplate>
                                        <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                    </ContentTemplate>
                                </aspnew:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table id="TableGrid" bgcolor="white" border="0" cellpadding="0" cellspacing="0"
                        width="100%">
                        <tr>
                            <td align="center" style="width: 100%">
                                <asp:Label ID="lbl_Message" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="width: 100%">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2" valign="top">
                                <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlResult">
                                    <ProgressTemplate>
                                        <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl1" runat="server" Text="Please Wait ......"
                                            CssClass="clsLabel"></asp:Label>
                                    </ProgressTemplate>
                                </aspnew:UpdateProgress>
                                <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline">
                                </aspnew:UpdatePanel>
                                <asp:GridView ID="gv_Records" runat="server" AutoGenerateColumns="False" CssClass="clsLeftPaddingTable"
                                    CellPadding="3" Height="141px" Width="404px" OnRowCommand="gv_Records_RowCommand"
                                    OnRowDeleting="gv_Records_RowDeleting" DataKeyNames="Paymenttype_PK" 
                                    onselectedindexchanged="gv_Records_SelectedIndexChanged">
                                    <Columns>
                                        <%--  <asp:TemplateField HeaderText="SNo">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSno" runat="server" CssClass="clssubhead" 
                                                                        Text='<%# DataBinder.Eval(Container, "DataItem.SNo") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="Description">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Description") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbtn_Desc" runat="server" Text='<%# Bind("Description") %>' CommandName="Select"
                                                
                                                CommandArgument='<%# Bind("Paymenttype_PK") %>'
                                                ></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Short Description">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("ShortDescription") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="LblShortDesc" runat="server" Text='<%# Bind("ShortDescription") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Is Active">
                                            <EditItemTemplate>
                                                <asp:CheckBox ID="ChkIsActive" runat="server" Checked='<%# Bind("IsActive") %>' />
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="ChkIsActive" runat="server" Checked='<%# Bind("IsActive") %>' 
                                                    Enabled="false" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/remove2.gif"
                                                    CommandArgument='<%# DataBinder.Eval(Container, "DataItem.Paymenttype_PK") %>'
                                                    CommandName="Delete" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td background="../../images/separator_repeat.gif" colspan="2" style="height: 11px;
                                width: 780">
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td style="display: none;">
                                <asp:TextBox ID="txt_totalrecords" runat="server" CssClass="label" ForeColor="Black"></asp:TextBox>
                            </td>
                            <td>
                                <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

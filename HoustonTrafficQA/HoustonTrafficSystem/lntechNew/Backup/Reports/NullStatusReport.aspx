﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NullStatusReport.aspx.cs"
    Inherits="HTP.Reports.NullStatusReport" %>

<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Null Status Report</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <asp:ScriptManager runat="server"  />
    <asp:UpdatePanel ID="upnlResult" runat="server" >
    <ContentTemplate>
    
    
 
        <table cellspacing="0" cellpadding="0" width="780" align="center" border="0">
            <tr>
                <td align="center">
                    <uc1:ActiveMenu ID="ActiveMenu1" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="center" style="background-image: url(../images/separator_repeat.gif)"
                    height="11">
                </td>
            </tr>
            <tr>
                <td style="background-image: url(../Images/subhead_bg.gif)" class="clssubhead" align="left"
                    height="34px">
                    <table style="width: 100%">
                        <tr>
                            <td>
                            </td>
                            <td align="right" valign="middle">
                                <uc3:PagingControl ID="Pagingctrl" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center" style="background-image: url(../images/separator_repeat.gif)"
                    height="11">
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="lbl_message" runat="server" CssClass="Label" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:UpdateProgress ID="updateprogress" runat="server" AssociatedUpdatePanelID="upnlResult">
                        <ProgressTemplate>
                            <img alt="" src="../images/plzwait.gif" />&nbsp;<asp:Label ID="lbl1" runat="server"
                                CssClass="clssubhead" Text="Please Wait ......"></asp:Label>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:GridView ID="gv_Data" AllowPaging="True" runat="server" HeaderStyle-HorizontalAlign="Left"
                        CellSpacing="0" AutoGenerateColumns="False" CssClass="clsLeftPaddingTable" Width="100%"
                        PageSize="20" OnPageIndexChanging="gv_Data_PageIndexChanging">
                        <PagerStyle HorizontalAlign="Center" />
                        <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                            FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                        <Columns>
                            <asp:TemplateField HeaderText="S.No" HeaderStyle-Width="30px">
                                <ItemTemplate>
                                    <asp:HyperLink ID="hl_Sno" CssClass="clssubhead" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                        runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SNo") %>'></asp:HyperLink>
                                </ItemTemplate>
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Last Name">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_LastName" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.[LastName]") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="First Name">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_FirstName" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.[FirstName]") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DOB">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_DOB" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.[DOB]") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Ticket Number">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_TicketNumber" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.TicketNumber") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Cause  Number">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_CauseNumber" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.CauseNumber") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td align="center" style="background-image: url(../images/separator_repeat.gif)"
                    height="11">
                </td>
            </tr>
            <tr>
                <td>
                    <uc2:Footer ID="Footer1" runat="server" />
                </td>
            </tr>
        </table>
           </ContentTemplate>
    
    </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using System.Data.SqlClient;

namespace lntechNew.Reports
{
	/// <summary>
	/// Summary description for WebForm1.
	/// </summary>
	public partial class WebForm1 : System.Web.UI.Page
	{
		int ticketid;
		int empid;
		protected System.Web.UI.WebControls.TextBox txtComments;
		protected System.Web.UI.WebControls.Button btn_submit;
			clsENationWebComponents ClsDb = new clsENationWebComponents();
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			
				ticketid =Convert.ToInt32(Request.QueryString["ticketID"]);
				Session["employeeid"] = (int) Convert.ChangeType(Request.Cookies["sEmpID"].Value,typeof(int));
				empid=(int)Session["employeeid"];
				//empid=3992;
				
			if(!IsPostBack)
			{
				GetComments();
			}
				
		
		}


		private void GetComments()
		{		
			string [] key1 =  {"@ticketid"} ;
			object [] value1 =  {ticketid} ;
            //Change by Ajmal
			IDataReader sqldr = ClsDb.Get_DR_BySPArr("usp_getGeneralCommentsByTicketID",key1,value1);
			sqldr.Read();
			txtComments.Text = sqldr[0].ToString();
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btn_submit.Click += new System.EventHandler(this.btn_submit_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		


		private void btn_submit_Click(object sender, System.EventArgs e)
		{			
				string comments = txtComments.Text.Trim() ;

				string [] key1 =  {"@ticketid","@empid","@comments"} ;
				object [] value1 =  {ticketid,empid,comments} ;
				ClsDb.ExecuteSP("usp_setGeneralCommentsByTicketID",key1,value1);			
		}
	}
}

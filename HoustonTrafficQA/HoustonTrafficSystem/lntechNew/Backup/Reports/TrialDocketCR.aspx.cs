using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace lntechNew.Reports
{
    public partial class TrialDocketCR : System.Web.UI.Page
    {
        string courtloc;
        string courtdate;
        string page;
        string courtnumber;
        string datetype;
        string singles;
        string showdetail;

        protected void Page_Load(object sender, EventArgs e)
        {


           courtloc = Convert.ToString(Request.QueryString["Courtloc"]);
           ViewState["Courtloc"] = courtloc;
           courtdate = Convert.ToString(Request.QueryString["Courtdate"]);
           ViewState["Courtdate"] = courtdate;
           page = Convert.ToString(Request.QueryString["Page"]);
           ViewState["Page"] = page;
           courtnumber = Convert.ToString(Request.QueryString["Courtnumber"]);
           ViewState["Courtnumber"] = courtnumber;
           datetype = Convert.ToString(Request.QueryString["Datetype"]);
           ViewState["Datetype"] = datetype;
           singles = Convert.ToString(Request.QueryString["singles"]);
           ViewState["singles"] = singles;
           showdetail = Convert.ToString(Request.QueryString["showowesdetail"]);
           ViewState["showdetail"] = showdetail;
           
          
        }
    }
}

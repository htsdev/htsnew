using System;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;
using FrameWorkEnation.Components;
using System.IO;
using HTP.WebControls;
using MetaBuilders.WebControls;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Configuration;
using WebSupergoo.ABCpdf6;
using WebSupergoo.ABCpdf6.Objects;
using WebSupergoo.ABCpdf6.Atoms;
using System.Security.AccessControl;
//HMCsubmittedcases
namespace HTP.Reports
{
    public partial class HMCsubmittedcases : System.Web.UI.Page
    {

        clsCase cCase = new clsCase();
        clsCrsytalComponent crystal = new clsCrsytalComponent();
        ValidationReports reports = new ValidationReports();
        clsSession cSession = new clsSession();
        string StrExp = String.Empty;
        string StrAcsDec = String.Empty;
        clsENationWebComponents clsDb = new clsENationWebComponents();
        DataTable dtRecords = new DataTable();
        DataSet DS;
        DataView DV;

        //Yasir 5423 02/11/2009 cases having follow up date in next 6 business days

        protected override void OnInit(EventArgs e)
        {
            ShowSetting.OnErr += new HTP.WebControls.ShowSetting.ErrHandler(ShowSetting_OnErr);
            ShowSetting.dbBind += new HTP.WebControls.ShowSetting.Databind(ShowSetting_dbBind);
            base.OnInit(e);
        }

        //5423 end 

        protected void Page_Load(object sender, EventArgs e)
        {

            lbl_Message.Text = "";

            if (!IsPostBack)
            {
                try
                {
                    //Zeeshan Ahmed 4420 08/06/2008 Refactor Code
                    FillGrid();
                    //Zeeshan Ahmed 4645 0//20/2008 Save Complete Name For Signature
                    //ViewState["RepName"] = Convert.ToString(cSession.GetCookie("sEmpName", this.Request)).ToUpper();
                    // Noufil 4645 08/21/2008 Use first name space last name in signature
                    clsUser cUser = new clsUser(Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request)));
                    ViewState["RepFName"] = Convert.ToString(cUser.FirstName.ToUpper());
                    ViewState["RepLName"] = Convert.ToString(cUser.LastName.ToUpper());

                }
                catch (Exception ex)
                {
                    lbl_Message.Text = ex.Message;
                    LNHelper.Logger.Instance.LogError(ex);
                }
            }


            //Kazim 3424 3/24/2008 Use delegate to call this portion of code after returning back frtom updatefollowup control 
            UpdateFollowUpInfo2.PageMethod += delegate()
            {
                FillGrid();
            };
        }

        private void FillGrid()
        {

            //Yasir 5423 02/11/2009 cases having follow up date in next 6 business days
            string[] keys = { "@showAll", "@validation" };
            object[] values = { ShowAll.Checked, 0 };
            reports.HasParameter = true;
            reports.keys = keys;
            reports.values = values;
            // 5423 end

            //Zeeshan Ahmed 4420 08/06/2008 Display Report Cases
            reports.getReportData(DG_HMC_cases, lbl_Message, ValidationReport.HMCsubmittedcases);
            DS = reports.records;
            hf_RecordCount.Value = Convert.ToString(DS.Tables[0].Rows.Count);
            //Sabir Khan 4894 10/04/2008
            Session["ExportToExcel"] = DS;
            if (DS.Tables[0].Rows.Count > 0)
            {
                DV = new DataView(reports.records.Tables[0]);
                Session["DS"] = DV;
                cbPassedCourtDate.Visible = true;
            }
            else
            {
                cbPassedCourtDate.Visible = false;
            }
        }

        private void SortGrid(string SortExp)
        {
            try
            {
                SetAcsDesc(SortExp);
                DV = (DataView)Session["DS"];
                DV.Sort = StrExp + " " + StrAcsDec;
                DG_HMC_cases.DataSource = DV;
                DG_HMC_cases.DataBind();
                Session["DS"] = DV;
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                LNHelper.Logger.Instance.LogError(ex);
            }
        }

        public void FixSerial()
        {
            int stind = (DG_HMC_cases.PageIndex * DG_HMC_cases.PageSize) + 1;

            try
            {
                for (int i = 0; i <= DG_HMC_cases.PageSize; i++)
                {
                    HyperLink lb_sno = (HyperLink)DG_HMC_cases.Rows[i].FindControl("sno");
                    lb_sno.Text = stind.ToString();
                    stind++;
                }
            }
            catch
            {

            }
        }

        private void SetAcsDesc(string Val)
        {
            try
            {
                StrExp = Session["StrExp"].ToString();
                StrAcsDec = Session["StrAcsDec"].ToString();
            }
            catch
            {

            }

            if (StrExp == Val)
            {
                if (StrAcsDec == "DESC")
                {
                    StrAcsDec = "ASC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
                else
                {
                    StrAcsDec = "DESC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
            }
            else
            {
                StrExp = Val;
                StrAcsDec = "DESC";
                Session["StrExp"] = StrExp;
                Session["StrAcsDec"] = StrAcsDec;
            }
        }

        protected void gv_Records_Sorting(object sender, GridViewSortEventArgs e)
        {
            SortGrid(e.SortExpression);
        }

        protected void DG_HMC_cases_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {

                if ((e.Row.RowType != DataControlRowType.Header) && (e.Row.RowType != DataControlRowType.Footer) && (e.Row.RowType != DataControlRowType.Pager))
                {

                    HiddenField TicketId = ((HiddenField)e.Row.FindControl("hf_TicketId"));

                    Label FollowUpDate = ((Label)e.Row.FindControl("lbl_FollowUpD"));
                    Label LastName = ((Label)e.Row.FindControl("lbl_LastName"));
                    Label FirstName = ((Label)e.Row.FindControl("lbl_FirstName"));
                    Label CaseNumber = ((Label)e.Row.FindControl("lbl_CaseNumber"));
                    Label CauseNumber = ((Label)e.Row.FindControl("lbl_CauseNo"));

                    cCase.TicketID = Convert.ToInt32(TicketId.Value);
                    string generalcomments = cCase.GetGeneralCommentsByTicketId();
                    string dbid = ((HiddenField)e.Row.FindControl("hfdbid")).Value;
                    string sbid = ((HiddenField)e.Row.FindControl("hfsbid")).Value;
                    //ozair 4879 09/29/2008 Set Ids  to Display Arrignment Setting Documents from old structure
                    string docid = ((HiddenField)e.Row.FindControl("hf_docid")).Value;


                    Label recid = (Label)e.Row.FindControl("lbl_recordid");
                    //Zeeshan Ahmed 4420 08/06/2008 Set Ids  to Display Arrignment Setting Documents
                    HiddenField hfgeneralComments = ((HiddenField)e.Row.FindControl("hfgeneralComments"));
                    hfgeneralComments.Value = generalcomments;

                    if (dbid == "0" && sbid == "0" && docid == "0")
                    {
                        ((ImageButton)e.Row.FindControl("img_summary")).Style[HtmlTextWriterStyle.Display] = "none";
                    }
                    else if (dbid != "0" && sbid != "0")
                    {
                        ((ImageButton)e.Row.FindControl("img_summary")).Attributes.Add("onclick", "return PopUp(" + dbid + "," + sbid + ");");
                    }
                    else if (docid != "0")
                    {
                        ((ImageButton)e.Row.FindControl("img_summary")).Attributes.Add("OnClick", "return PopUpNew(" + docid + ");");
                    }

                    //Kazim 3640 4/10/20008 Call method  SelectCheckBoxBeforShowPopup on image click
                    //Kazim 3729 4/14/2008 Add '\' with General Comments
                    ((ImageButton)e.Row.FindControl("img_Add")).Attributes.Add("onclick", "return SelectCheckBoxBeforShowPopup(\"" + hfgeneralComments.ClientID + "\",'" + CaseNumber.ClientID + "','" + TicketId.Value + "','DG_HMC_cases','" + FollowUpDate.Text + "','" + LastName.Text + "','" + FirstName.Text + "','" + CaseNumber.Text + "','" + CauseNumber.Text + "');");
                }
            }
            catch (Exception e1)
            {
                lbl_Message.Text = "from databound" + e1.Message.ToString();
            }
        }

        protected void SendMail(object sender, ImageClickEventArgs e)
        {
            //Kazim 3424 3/24/2008 This method is used to generate the Crystal Report and email the report to specific addresses 
            FormatEmailBody();
            mpeEmail.Show();
        }

        private string ShowPDFN(int DBID, int SBID, int SBDID, string printdate)
        {
            //Noufil 4707 09/12/2008 Save Files With Proper Names
            string strFile, fn;
            string strFileName = Server.MapPath("") + "\\Temp\\" + this.Session.SessionID.ToString() + "\\";
            strFile = printdate + " Arrainment Setting Summary.pdf";
            strFileName += strFile;

            fn = Convert.ToString(SBID) + "-" + Convert.ToString(SBDID) + "-" + Convert.ToString(DBID) + ".jpg";
            fn = ConfigurationManager.AppSettings["NTPATHDocumentTrackingImage"].ToString() + fn;
            fn = fn.Replace("\\\\", "\\");

            if (File.Exists(strFileName) && File.Exists(fn))
            {
                Doc theDoc = new Doc();
                theDoc.Rect.Inset(0, 0);
                theDoc.Page = theDoc.AddPage();

                int theID;
                theID = theDoc.AddImageFile(fn);
                while (true)
                {
                    theDoc.FrameRect(); // add a black border
                    if (!theDoc.Chainable(theID))
                        break;
                    theDoc.Page = theDoc.AddPage();
                    theID = theDoc.AddImageToChain(theID);
                }
                for (int i = 1; i <= theDoc.PageCount; i++)
                {
                    theDoc.PageNumber = i;
                    theDoc.Flatten();
                }
                Doc theDoc1 = new Doc();
                theDoc1.Read(strFileName);
                theDoc.Append(theDoc1);
                theDoc1.Clear();
                theDoc1.Dispose();
                theDoc.Save(strFileName);
            }
            else
            {
                //Zeeshan Ahmed 4944 10/14/2008 Fixed Image Size Issue.
                Doc theDoc = new Doc();
                theDoc.Rect.Inset(0, 0);
                theDoc.Page = theDoc.AddPage();
                int theID = theDoc.AddImageFile(fn);
                theDoc.Save(strFileName);
            }
            return strFileName;
        }

        /// <summary>
        /// ozair 4789 10/03/2008
        /// this function is used to send documents through emails
        /// which are scanned through old scanning structure
        /// it gets the pdf file and copy them to the folder created for this purpose.
        /// </summary>
        /// <param name="docid"></param>
        /// <param name="printdate"></param>
        /// <returns></returns>
        private string ShowPDFN(int docid, string printdate)
        {
            string strFile, fn;
            string strFileName = Server.MapPath("") + "\\Temp\\" + this.Session.SessionID.ToString() + "\\";
            ///strFile = printdate + " Arrainment Setting Summary.pdf";
            //strFileName += strFile;

            string[] keys = { "@RecordID" };
            object[] values = { docid };
            DataSet ds_Print = clsDb.Get_DS_BySPArr("USP_HTS_GET_HTSNOTES", keys, values);

            //To get the address of file            
            fn = Convert.ToString(ConfigurationManager.AppSettings["NTPATHBatchPrinte"]);
            strFile = ds_Print.Tables[0].Rows[0]["DocPath"].ToString();
            fn = fn + strFile;

            if (File.Exists(fn))
            {
                strFileName = strFileName + printdate + " " + strFile;
                if (File.Exists(strFileName))
                {
                    File.Delete(strFileName);
                }
                File.Copy(fn, strFileName);
                return strFileName;
            }
            else
            {
                return "";
            }
        }

        private void FormatEmailBody()
        {
            try
            {
                //Zeeshan Ahmed 4420 08/06/2008 Set Email Body
                hfTicketIds.Value = "";
                tbSubject.Text = "Please remove Clients in DLQ Status.";
                tbBody.Text = "Please find attached  Arraignment Setting Summaries signed by a HMC Clerk. I called 311 today and the cases listed below are in DLQ status.   Please have someone look into these cases:\n\n";

                foreach (GridViewRow Row in DG_HMC_cases.Rows)
                {
                    if (((SelectorField.SelectorFieldCheckBox)(Row.Cells[10].Controls[0])).Checked)
                    {
                        string CauseNo = ((Label)Row.FindControl("lbl_CauseNo")).Text;
                        string CaseNumber = ((Label)Row.FindControl("lbl_CaseNumber")).Text;
                        string LastName = ((Label)Row.FindControl("lbl_lastname")).Text;
                        string FirstName = ((Label)Row.FindControl("lbl_firstname")).Text;
                        string dob = ((HiddenField)Row.FindControl("hfDob")).Value;
                        tbBody.Text += "\n" + LastName + " , " + FirstName + " (" + dob + ") - " + CauseNo + " / " + CaseNumber;
                        hfTicketIds.Value += ((HiddenField)Row.FindControl("hf_ticketid")).Value + ",";
                    }
                }
            }
            catch (Exception ex)
            {
                LNHelper.Logger.Instance.LogError(ex);
            }
        }

        private void SendHMCAttachments()
        {
            string filepath = Server.MapPath("") + "\\Temp\\" + this.Session.SessionID.ToString() + "\\" + this.Session.SessionID.ToString() + ".pdf";
            string directoryPath = Server.MapPath("") + "\\Temp\\" + this.Session.SessionID;
            try
            {
                lbl_Message.Text = "";
                reports.getReportData(DG_HMC_cases, lbl_Message, ValidationReport.HMCsubmittedcases);
                //Yasir 5423 02/11/2009 cases having follow up date in next 6 business days
                DataSet ds = reports.GetRecords_HMCsubmittedcases(1, 1, hfTicketIds.Value);
                //5423 end
                crystal.CreateReport(ds, Server.MapPath("") + "//" + "HMC Arraignment Waiting Alert.rpt", Server.MapPath("") + "\\Temp\\" + this.Session.SessionID.ToString() + "\\", this.Session, this.Response);
                string newfilepath = Server.MapPath("") + "\\Temp\\" + this.Session.SessionID.ToString() + "\\" + "Cases In DLQ Status" + ".pdf";
                if (File.Exists(newfilepath))
                {
                    File.Delete(newfilepath);
                }
                File.Move(filepath, newfilepath);
                clsUser user = new clsUser();
                int EmpId = int.Parse(cSession.GetCookie("sEmpID", this.Request));
                user.EmpID = EmpId;
                DataTable dtUserInfo = user.GetSystemUserInfo();
                string email = dtUserInfo.Rows[0]["email"].ToString();
                string ticketids = String.Empty;
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ticketids = ticketids + ds.Tables[0].Rows[i]["ticketid_pk"].ToString();
                    ticketids = ticketids + ",";
                }

                string allfilenames = String.Empty;
                string concatfilenames = String.Empty;

                //ozair 4879 09/29/2008 Get Arraignment waiting scanned documents from document tracking along with old structure
                DataSet dsScannedDocuments = cCase.GetHMCArrWaitingScannedDocuments(hfTicketIds.Value);
                for (int i = 0; i < dsScannedDocuments.Tables[0].Rows.Count; i++)
                {
                    allfilenames = ShowPDFN(Convert.ToInt32(dsScannedDocuments.Tables[0].Rows[i]["DBID"]), Convert.ToInt32(dsScannedDocuments.Tables[0].Rows[i]["SBID"]), Convert.ToInt32(dsScannedDocuments.Tables[0].Rows[i]["SBDID"]), Convert.ToDateTime(dsScannedDocuments.Tables[0].Rows[i]["BatchDate"]).ToString("MM-dd-yyyy"));

                    if (!concatfilenames.Contains(allfilenames))
                        concatfilenames = concatfilenames + allfilenames + ",";
                }

                for (int i = 0; i < dsScannedDocuments.Tables[1].Rows.Count; i++)
                {
                    allfilenames = ShowPDFN(Convert.ToInt32(dsScannedDocuments.Tables[1].Rows[i]["docid"]), Convert.ToDateTime(dsScannedDocuments.Tables[1].Rows[i]["printdate"]).ToString("MM-dd-yyyy"));
                    if (allfilenames != "" && !concatfilenames.Contains(allfilenames))
                    {
                        concatfilenames = concatfilenames + allfilenames + ",";
                    }
                }
                //end ozair 4789
                //File.Copy(Server.MapPath("") + "\\Temp\\" + this.Session.SessionID + ".pdf", Server.MapPath("") + "\\Temp\\" + "HMC Arraignment Waiting DLQ Alert " + DateTime.Now.Date.ToString("MM-dd-yyyy") + ".pdf", true);
                concatfilenames = concatfilenames + Server.MapPath("") + "\\Temp\\" + this.Session.SessionID.ToString() + "\\" + "Cases In DLQ Status" + ".pdf";

                string[] filecount = concatfilenames.Split(',');
                string Body = tbBody.Text.Replace("\n", "<br/>");
                //Body += "<br/><br/>Beverly A. Rivas<br/>Paralegal<br/>Sullo & Sullo, LLP � Houston<br/>Attorneys at Law<br/>2020 Southwest Freeway, Suite #300<br/>Houston, Texas  77098<br/>Tel: 713-839-9026<br/>Fax: 713-523-6634<br/>www.sullolaw.com";

                if (filecount.Length > 1)
                {
                    MailClass.SendMailWithtMultipleAttchments(email, tbToEmails.Text, tbSubject.Text, Body, concatfilenames);
                }
                else
                {
                    MailClass.SendMailWithtMultipleAttchments(email, tbToEmails.Text, tbSubject.Text, Body, concatfilenames);
                }

                //Remove Temporary Directory
                if (Directory.Exists(directoryPath))
                    Directory.Delete(directoryPath, true);

                //Update Email Count
                reports.updateArrignmentWaitingEmailCounts(hfTicketIds.Value);
                FillGrid();

            }
            catch (Exception ex)
            {
                if (File.Exists(filepath))
                    File.Delete(filepath);

                lbl_Message.Text = ex.Message;
                LNHelper.Logger.Instance.LogError(ex);
            }
        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            SendHMCAttachments();
            cbPassedCourtDate.Checked = false;
            this.Session.Remove(this.Session.SessionID);

        }
        //Sabir Khan 4894 10/04/2008
        protected void imgExp_Ds_to_Excel_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                DataSet Exp_DataSet = (DataSet)Session["ExportToExcel"];
                if (Exp_DataSet != null)
                {
                    FetchSelectedRecord();
                    Session["ds"] = Exp_DataSet;
                    Response.Redirect("../QuickEntryNew/FrmTableExportToExcel.aspx?Flag=HMCDLQ");
                }
            }
            catch (Exception ex)
            {
                this.lbl_Message.Text = "Unable to export data in Excel.";
                LNHelper.Logger.Instance.LogError(ex);
            }

        }
        //Sabir Khan 4894 10/06/2008
        private void FetchSelectedRecord()
        {
            DataTable dtHMCDLQ = new DataTable();
            dtHMCDLQ.Columns.Add("S No");
            dtHMCDLQ.Columns.Add("Cause No");
            dtHMCDLQ.Columns.Add("Ticket No");
            dtHMCDLQ.Columns.Add("Last Name");
            dtHMCDLQ.Columns.Add("First Name");
            dtHMCDLQ.Columns.Add("DOB");
            dtHMCDLQ.Columns.Add("Crt Date & Time");
            dtHMCDLQ.Columns.Add("ArrWaitDate & Time");
            dtHMCDLQ.Columns.Add("Follow Up Date");
            dtHMCDLQ.Columns.Add("Emails");

            foreach (GridViewRow Row in DG_HMC_cases.Rows)
            {

                if (((SelectorField.SelectorFieldCheckBox)(Row.Cells[11].Controls[0])).Checked)
                {
                    DataRow dr = dtHMCDLQ.NewRow();
                    string Sno = ((HyperLink)Row.FindControl("sno")).Text;
                    string CauseNo = ((Label)Row.FindControl("lbl_CauseNo")).Text;
                    string CaseNumber = ((Label)Row.FindControl("lbl_CaseNumber")).Text;
                    string LastName = ((Label)Row.FindControl("lbl_lastname")).Text;
                    string FirstName = ((Label)Row.FindControl("lbl_firstname")).Text;
                    string dob = ((HiddenField)Row.FindControl("hfDob")).Value;
                    string CourtDate = ((Label)Row.FindControl("lbl_CourtDate")).Text;
                    string AWdate = ((Label)Row.FindControl("lbl_ArrWaitDate")).Text;
                    string FollowUpD = ((Label)Row.FindControl("lbl_FollowUpD")).Text;
                    string EmailCount = ((Label)Row.FindControl("lblEmailCount")).Text;
                    string TicketID = ((HiddenField)Row.FindControl("hf_ticketid")).Value;
                    dr["S No"] = Sno;
                    dr["Cause No"] = CauseNo;
                    dr["Ticket No"] = CaseNumber;
                    dr["Last Name"] = LastName;
                    dr["First Name"] = FirstName;
                    dr["DOB"] = dob;
                    dr["Crt Date & Time"] = CourtDate;
                    dr["ArrWaitDate & Time"] = AWdate;
                    dr["Follow Up Date"] = FollowUpD;
                    dr["Emails"] = EmailCount;
                    dtHMCDLQ.Rows.Add(dr);
                }
            }
            Session["ExportToExcel1"] = dtHMCDLQ;
        }

        //Yasir 5423 02/11/2009 cases having follow up date in next 6 business days

        /// <summary>
        /// showAll check Box change event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ShowAll_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                FillGrid();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                LNHelper.Logger.Instance.LogError(ex);
            }
        }

        /// <summary>
        /// Show Setting Control Databind
        /// </summary>
        void ShowSetting_dbBind()
        {
            FillGrid();

        }
        /// <summary>
        /// ShowSetting display message on error
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="ErrorMsg"></param>
        void ShowSetting_OnErr(object sender, string ErrorMsg)
        {
            lbl_Message.Text = ErrorMsg;

        }
    }
}


<%@ Page language="c#" AutoEventWireup="false" Inherits="HTP.Reports.Word_TrialNotification" Codebehind="Word_TrialNotification.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Trial Notification Letter</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<script src="../Scripts/Validationfx.js" type="text/javascript"></script>
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script>
		function refresh()
		{
		
		 //window.opener.location.reload(); //onload="refresh();"
		 opener.location.href = opener.location;
		 var txtflag=document.getElementById("TextBox1").value;
		 if(txtflag == "1")
		 { 
		    ShowEmail();
		 }
		 var flag_email = "<% = Request.QueryString["EmailFlag"] %>";
		 if(flag_email == "1")
		 { 
		    ShowEmail();
		 }
		 
		 var batch = document.getElementById("TextBox2").value
		 if (batch == "")
		    SendToBatch();
		}
		
		
		
		
		
		function setdefault()
		{//a;
			var rowhide=document.getElementById("tremail");  //onload="setdefault();"
			rowhide.style.display='none'
		}
		function ShowEmail()
		{//a;
			var rowhide=document.getElementById("tremail");  //onload="setdefault();"
			rowhide.style.display='block'
			rowhide=document.getElementById("trpdf");  //onload="setdefault();"
			rowhide.style.display='none'
			return false;							
		}
		function HideEmail()
		{//a;
			var rowhide=document.getElementById("tremail");  //onload="setdefault();"
			rowhide.style.display='none'
			rowhide=document.getElementById("trpdf");  //onload="setdefault();"
			rowhide.style.display='block'
			return false;							
			
		}
		//Validate email addresses
		function Validate()
		{				
			var addr;
			//validate To field
			addr=document.getElementById("txt_to").value;
			if(multiEmail(addr)==false)
			{
				document.getElementById("txt_to").focus();				
				return false;
			}
			addr=document.getElementById("txt_cc").value;
			if (addr!="")
			{
				if(multiEmail(addr)==false)
				{
					document.getElementById("txt_cc").focus();
					return false;
				}
			}
			addr=document.getElementById("txt_bcc").value;
			if (addr!="")
			{
				if(multiEmail(addr)==false)
				{
					document.getElementById("txt_bcc").focus();
					return false;
				}
			}			
		}
		//Split email
		function multiEmail(email_field)
		{
			var email = email_field.split(',');
			for(var i = 0; i < email.length; i++) 
			{
				if (!isEmail(email[i])) 
					{
						alert('one or more email addresses entered is invalid');
						return false;
					}
			}
			return true;
		} 
		
		function SendToBatch()
		{
		    var CourtID = " <% = Request.QueryString["CourtID"] %>";
		    var flg_split = "<% = Request.QueryString["flg_split"] %>";
		    var flg_print = "<% = Request.QueryString["flg_print"] %>";
		    var SplitContinue;// = false;
		    var PrintContinue;// = false;
		    var retBatch = true;
		   
		    //a;
		     if(flg_split==1)
	           {
	                SplitContinue = confirm("The Trial Notification Letter you are printing has a split court date, time or room number. Would you like to continue?");
	           }
	           if(SplitContinue==false)
	           return;
	           
	            if(flg_print==1)
	                    {
	                        PrintContinue = confirm("Case number "+CourtID +" already exists in batch print queue. Would you like to override the existing letter with this new letter");
	                    
	                    
	                    if(PrintContinue == true)
	                    {
	                     OpenPopUpSmall("frmBatchEntry.aspx?Batch=true&LetterID=2&casenumber=<%=ViewState["vTicketId"]%>&Exists=1");	
	                     alert("Letter has been sent to batch print");
	                     return(false);
	                    }
	                    else
	                    {
	                    alert('Letter was not sent to batch');
	                    return(false);
	                    }
	                    }
	              else
	              {
	                OpenPopUpSmall("frmBatchEntry.aspx?Batch=true&LetterID=2&casenumber=<%=ViewState["vTicketId"]%>&Exists=0");
	                 window.opener.location.reload();
	                alert("Letter has been sent to batch print");
	               
	              }
	                /*if(PrintContinue==false)
	                
	                    OpenPopUpSmall("frmBatchEntry.aspx?Batch=true&LetterID=2&casenumber=<%=ViewState["vTicketId"]%>&Exists=0");
	                else
				        OpenPopUpSmall("frmBatchEntry.aspx?Batch=true&LetterID=2&casenumber=<%=ViewState["vTicketId"]%>&Exists=1");	*/
			//alert("Letter has been sent to batch print");
			//window.close();
			return false;
		
		}
		
		function OpenPopUpSmall(path)  //(var path,var name)
		{
		
		 window.open(path,'',"fullscreen=no,toolbar=no,width=2,height=2,left=0,top=0,status=no,menubar=no,resizable=no");
		  //return true;
		}
		
		
		</script>
	</HEAD>
	<body onload="refresh();" MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
				<TR>
					<TD background="../Images/subhead_bg.gif" height="31"></TD>
				</TR>
				<tr>
					<td>
						<table class="clsmainhead" id="Table3" cellSpacing="0" cellPadding="0" width="100%" align="left"
							border="0">
							<tr>
								<td vAlign="bottom" width="31"><IMG height="17" src="../Images/head_icon.gif" width="31"></td>
								<td class="clssubhead" vAlign="baseline">&nbsp;Trial Letter</td>
								<td align="right" colSpan="2">
                                    <asp:ImageButton ID="btn_SendToBatch" runat="server" Height="24px" ImageUrl="~/Images/BriefCase.png"
                                        ToolTip="Send to batch" OnClick="btn_SendToBatch_Click1" />&nbsp;
                                    <asp:imagebutton id="imgbtn_email" runat="server" ImageUrl="../Images/Email.jpg" ToolTip="Email"></asp:imagebutton><asp:imagebutton id="IBtn" runat="server" Height="25px" ImageUrl="../Images/MSSmall.gif" ToolTip="Word"></asp:imagebutton></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td background="../Images/separator_repeat.gif" colSpan="3" height="9"></td>
				</tr>
				<tr id="tremail" style="DISPLAY: none">
					<td>
						<table id="tblemail" width="100%">
							<TR>
								<TD vAlign="bottom" align="left" width="12%" style="height: 37px">&nbsp;
									<asp:imagebutton id="imgbtn_send" runat="server" ImageUrl="..\Images\SendMail.ICO" ToolTip="Send"></asp:imagebutton>&nbsp;
									<asp:imagebutton id="imgbtn_cancel" runat="server" ImageUrl="..\Images\cancel.ico" ToolTip="Cancel"></asp:imagebutton></TD>
								<TD style="height: 37px"><IMG src="../Images/trialLetter.gif" name="Trial Letter"></TD>
							</TR>
							<tr>
								<td>&nbsp;<strong>To.</strong></td>
								<td><asp:textbox id="txt_to" runat="server" Width="430px" CssClass="textbox"></asp:textbox></td>
							</tr>
							<tr>
								<td>&nbsp;<strong>CC</strong></td>
								<td><asp:textbox id="txt_cc" runat="server" Width="430px" CssClass="textbox"></asp:textbox></td>
							</tr>
							<TR>
								<TD>&nbsp;<strong>BCC</strong></TD>
								<TD><asp:textbox id="txt_bcc" runat="server" Width="430px" CssClass="textbox"></asp:textbox></TD>
							</TR>
							<tr>
								<td>&nbsp;<strong>Subject</strong></td>
								<td><asp:textbox id="txt_subject" runat="server" Width="430px" CssClass="textbox"></asp:textbox></td>
							</tr>
							<TR>
								<TD colSpan="2"><asp:textbox id="txt_message" runat="server" Height="240px" Width="520px" CssClass="textbox"
										TextMode="MultiLine"></asp:textbox></TD>
							</TR>
						</table>
					</td>
				</tr>
				<tr id="trpdf" height="500">
					<td colSpan="3">&nbsp; <iframe tabIndex="0" src="frmTrial_Notification.aspx?casenumber=<%=ViewState["vTicketId"]%>" frameBorder="1" width="100%" scrolling="auto"
							height="95%"></iframe>
					</td>
				</tr>
				<TR>
					<TD><uc1:footer id="Footer1" runat="server"></uc1:footer>
                       </TD>
				</TR>
				<tr>
				    <td style="display:none"> <asp:TextBox ID="TextBox1" runat="server">0</asp:TextBox>&nbsp;
                        <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
				    </td>
				</tr>
			</table>
		</form>
	</body>
</HTML>

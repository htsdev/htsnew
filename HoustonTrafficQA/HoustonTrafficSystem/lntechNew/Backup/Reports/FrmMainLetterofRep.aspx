<%@ Page Language="c#" AutoEventWireup="false" Inherits="HTP.Reports.FrmMainLetterofRep"
    CodeBehind="FrmMainLetterofRep.aspx.cs" %>

<%@ Register Src="~/WebControls/faxcontrol.ascx" TagName="Faxcontrol" TagPrefix="Fc" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="AjaxToolkit" %>
<%--<%@ Register Src="~/WebControls/ShowPdfControl.ascx" TagName="PdfControl" TagPrefix="Pdf" %>--%>
<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title id="PageTitle" runat="server">Letter of Rep</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="C#" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../Styles.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../scripts/subModal.css" />

    <script type="text/javascript" src="../scripts/common.js"></script>

    <script type="text/javascript" src="../scripts/subModal.js"></script>

    <style type="text/css">
        .modalBackground
        {
            background-color: Gray;
            filter: alpha(opacity=70);
            opacity: 0.7;
        }
        .modalPopup
        {
            background-color: #FFD9D5;
            border-width: 3px;
            border-style: solid;
            border-color: Gray;
            padding: 3px;
            width: 250px;
            position: static;
            left: 200px;
            top: -65px;
        }
        .MyCalendar .ajax__calendar_container
        {
            border: 1px solid #646464;
            background-color: lemonchiffon;
            color: red;
        }
    </style>
</head>

<script type="text/javascript" language="javascript">
		function refresh()
		{			
		 opener.location.href = opener.location;
		}
		
		function changeStackOrder()
        {      
        document.getElementById("testframe").style.display = "none";
        }
        
        function showDialog()        
        {
            //Fahad 5172 11/26//2008 
            if(document.getElementById("hfLORMethodCount").value=='0') 
            {
                alert("The associated LOR method for this court is not �Fax�, Please change the associated LOR method to �Fax� for this court.");
                return false;            
            }
        
          document.getElementById('testframe').style.display='none';
          showPopWin('../reports/ViewFaxControl.aspx', 520, 370, null);
          //Fahad 5172 11/26//2008
          return true;
        }
        function HideDailog()
        {           
          document.getElementById('testframe').style.display='block';          
          hidePopWin(true,0);
        }
        
</script>

<body ms_positioning="GridLayout" onload="refresh();">
    <form id="Form1" method="post" runat="server" visible="True">
    <table border="0" width="100%" height="100%">
        <tr>
            <td height="46" colspan="3" background="../Images/subhead_bg.gif" style="height: 34px">
            </td>
        </tr>
        <tr>
            <td style="height: 33px" valign="bottom" align="right">
                <table class="clsmainhead" id="Table3" cellspacing="0" cellpadding="0" width="185"
                    align="left" border="0">
                    <tr>
                        <td width="31">
                            <img height="17" src="../Images/head_icon.gif" width="31">
                        </td>
                        <td id="tdRepHead" runat="server" width="140" valign="baseline" class="clssubhead">
                            Letter Of Rep
                        </td>
                    </tr>
                </table>
            </td>
            <td style="height: 33px" colspan="2" align="right">
                <a id="fax1" runat="server" href="#" title="Send Fax" onclick="return showDialog();">
                    <img src='../Images/fax.jpg' border='0' style="height: 25px; width: 31px" /></a>
                <asp:ImageButton ID="IBtn" runat="server" Width="78px" ImageUrl="../Images/MSSmall.gif"
                    Height="23px"></asp:ImageButton>
                <div id="divMap" style="width: 100%; height: 95%; z-index: 1">
                </div>
            </td>
        </tr>
        <tr id="trframe">
            <td colspan="3">
                <iframe id="testframe" src="RptWordLetter.aspx?casenumber=<%=ViewState["vTicketId"]%>&lettertype=<%=ViewState["vLetterType"]%>&batch=<%=ViewState["vBatch"]%>"
                    width="100%" height="95%" scrolling="auto" frameborder="1"></iframe>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hfLORMethodCount" runat="server" />
    </form>
</body>
</html>

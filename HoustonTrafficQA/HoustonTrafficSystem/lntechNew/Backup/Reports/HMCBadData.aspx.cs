using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;

namespace lntechNew.Reports
{
    public partial class HMCBadData : System.Web.UI.Page
    {
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        clsLogger bugTracker = new clsLogger();


        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lbl_Message.Text = "";
                if (!IsPostBack)
                {
                    DataTable dt = GetRecords();
                    gv_Data.DataSource = dt;
                    if (dt.Rows.Count == 0)
                        lbl_Message.Text = "No Records!";
                    else
                        gv_Data.DataBind();
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        private DataTable GetRecords()
        {
            return ClsDb.Get_DT_BySPArr("USP_HTS_HMC_BAD_DATA_RECORDS");
        }
    }
}

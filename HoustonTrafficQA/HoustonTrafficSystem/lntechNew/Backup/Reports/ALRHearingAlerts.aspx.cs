﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;
using HTP.Components;

namespace HTP.Reports
{
    public partial class ALRHearingAlerts : System.Web.UI.Page
    {
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        clsSession ClsSession = new clsSession();
        clsCase cCase = new clsCase();
        string StrExp = String.Empty;
        string StrAcsDec = String.Empty;
        DataView dv;

        public SortDirection GridViewSortDirection
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Ascending;
                return (SortDirection)ViewState["sortDirection"];
            }
            set { ViewState["sortDirection"] = value; }

        }

        public string GridViewSortExpression
        {
            get
            {
                if (ViewState["sortExpression"] == null)
                    ViewState["sortExpression"] = "";
                return ViewState["sortExpression"].ToString();
            }
            set { ViewState["sortExpression"] = value; }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                int Empid = 0;
                if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false ||
                            (!int.TryParse(ClsSession.GetCookie("sEmpID", this.Request), out Empid)))
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else
                {
                    if (!IsPostBack)
                    {
                        GridViewSortDirection = SortDirection.Ascending;
                        GridViewSortExpression = "ticketid_pk";
                        BindGrid();
                    }
                    Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
                    Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
                    UpdateFollowUpInfo2.PageMethod += new HTP.WebControls.PageMethodHandler(UpdateFollowUpInfo2_PageMethod);
                    Pagingctrl.GridView = Gv_ALR;
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void Gv_ALR_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                string sortExpression = e.SortExpression;

                if (GridViewSortDirection == SortDirection.Ascending && sortExpression == GridViewSortExpression)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                else
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }

                GridViewSortExpression = sortExpression;
                BindGrid();
                Pagingctrl.PageCount = Gv_ALR.PageCount;
                Pagingctrl.PageIndex = Gv_ALR.PageIndex;

            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }


        }

        //private void SortGrid(string SortExp)
        //{
        //    try
        //    {
        //        SetAcsDesc(SortExp);
        //        DV = (DataView)Session["DS"];
        //        DV.Sort = StrExp + " " + StrAcsDec;
        //        DV.Table.Columns.Remove("sno");
        //        if (DV.Table.Columns.Contains("sno") == false)
        //        {
        //            DV.Table.Columns.Add("sno");
        //        }
        //        Gv_ALR.DataSource = DV;
        //        Gv_ALR.DataBind();
        //        Session["DS"] = DV;
        //    }
        //    catch (Exception ex)
        //    {
        //        lbl_Message.Text = ex.Message;
        //    }
        //}

        private void SetAcsDesc(string Val)
        {
            try
            {
                StrExp = Session["StrExp"].ToString();
                StrAcsDec = Session["StrAcsDec"].ToString();
            }
            catch
            {

            }

            if (StrExp == Val)
            {
                if (StrAcsDec == "DESC")
                {
                    StrAcsDec = "ASC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
                else
                {
                    StrAcsDec = "DESC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
            }
            else
            {
                StrExp = Val;
                StrAcsDec = "DESC";
                Session["StrExp"] = StrExp;
                Session["StrAcsDec"] = StrAcsDec;
            }
        }


        //Yasir Kamal 7337 02/03/2010   pagging performance issue fixed.
        protected void Gv_ALR_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (e.NewPageIndex != -1)
                {
                    Gv_ALR.PageIndex = e.NewPageIndex;
                    Gv_ALR.DataSource = (DataView)Session["dvBR"];
                    Gv_ALR.DataBind();

                    Pagingctrl.PageCount = Gv_ALR.PageCount;
                    Pagingctrl.PageIndex = Gv_ALR.PageIndex;
                    Pagingctrl.SetPageIndex();
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void Gv_ALR_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "ShowFollowupdate")
            {
                int RowID = Convert.ToInt32(e.CommandArgument);
                string lastname = (((Label)Gv_ALR.Rows[RowID].FindControl("lbl_LastName")).Text);
                string firstname = (((Label)Gv_ALR.Rows[RowID].FindControl("lbl_FirstName")).Text);
                string causeno = (((Label)Gv_ALR.Rows[RowID].FindControl("lbl_CauseNumber")).Text);
                string ticketno = (((HiddenField)Gv_ALR.Rows[RowID].FindControl("hf_ticketno")).Value);
                string courtname = "ALRHEARING";
                string subdoctype = (((HiddenField)Gv_ALR.Rows[RowID].FindControl("hf_Subdoctypeid")).Value);
                string followupDate = (((Label)Gv_ALR.Rows[RowID].FindControl("lbl_follow")).Text);
                string BusinessDay = (((HiddenField)Gv_ALR.Rows[RowID].FindControl("hf_MaxDays")).Value);
                int ticketid = Convert.ToInt32((((HiddenField)Gv_ALR.Rows[RowID].FindControl("hf_ticketid")).Value));
                cCase.TicketID = ticketid;
                string comm = cCase.GetGeneralCommentsByTicketId();
                string follow = string.Empty;
                UpdateFollowUpInfo2.followUpType = FollowUpType.ALRHearingFollowUpdate;
                UpdateFollowUpInfo2.Freezecalender = true;
                UpdateFollowUpInfo2.Title = (((Label)Gv_ALR.Rows[RowID].FindControl("lbl_status")).Text);
                UpdateFollowUpInfo2.binddate(BusinessDay, clsGeneralMethods.GetBusinessDayDate(DateTime.Now, 0), DateTime.Today, firstname, lastname, ticketno, ticketid.ToString(), causeno, comm, ModalPopupExtender1.ClientID, courtname, subdoctype, followupDate);
                UpdateFollowUpInfo2.Visible = true;
                ModalPopupExtender1.Show();
            }
        }


        protected void Gv_ALR_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ((LinkButton)e.Row.FindControl("img_Add")).CommandArgument = e.Row.RowIndex.ToString();
            }

        }


        #region methods

        void Pagingctrl_PageIndexChanged()
        {
            try
            {
                DataTable dtView;
                dv = (DataView)Session["dvBR"];
                dtView = dv.ToTable();
                Gv_ALR.PageIndex = Pagingctrl.PageIndex - 1;
                Gv_ALR.DataSource = dtView;
                Gv_ALR.DataBind();
                Pagingctrl.PageCount = Gv_ALR.PageCount;
                Pagingctrl.PageIndex = Gv_ALR.PageIndex;
                Pagingctrl.SetPageIndex();
                Session["dvBR"] = dtView.DefaultView;
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            try
            {
                if (pageSize > 0)
                {
                    Gv_ALR.PageIndex = 0;
                    Gv_ALR.PageSize = pageSize;
                    Gv_ALR.AllowPaging = true;
                }
                else
                {
                    Gv_ALR.AllowPaging = false;
                }
                Gv_ALR.DataSource = (DataView)Session["dvBR"];
                Gv_ALR.DataBind();
                Pagingctrl.PageCount = Gv_ALR.PageCount;
                Pagingctrl.PageIndex = Gv_ALR.PageIndex;
                Pagingctrl.SetPageIndex();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        private void BindGrid()
        {
            try
            {
                DataTable dt = ClsDb.Get_DT_BySPArr("USP_HTP_GET_ALRHEARINGREPORT");
                if (dt.Rows.Count > 0)
                {
                    GenerateSerialNo(dt);
                    DataView dv = dt.DefaultView;
                    dv.Sort = GridViewSortExpression + " " + (GridViewSortDirection == SortDirection.Ascending ? "ASC" : "DESC");
                    if (Session[""] != null) Session.Clear();
                    Session["dvBR"] = dv;
                    dt = dv.ToTable();
                }
                Gv_ALR.DataSource = dt;
                Gv_ALR.DataBind();
                Pagingctrl.PageCount = Gv_ALR.PageCount;
                Pagingctrl.PageIndex = Gv_ALR.PageIndex;
                Pagingctrl.SetPageIndex();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        void UpdateFollowUpInfo2_PageMethod()
        {
            BindGrid();
        }

        public void GenerateSerialNo(DataTable dt)
        {
            try
            {
                if (dt.Columns.Contains("sno") == false)
                {
                    dt.Columns.Add("sno");
                }

                int sno = 1;
                if (dt.Rows.Count >= 1)
                    dt.Rows[0]["sno"] = 1;
                if (dt.Rows.Count >= 2)
                {
                    for (int i = 1; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[i - 1]["ticketid_pk"].ToString() != dt.Rows[i]["ticketid_pk"].ToString())
                        {
                            dt.Rows[i]["sno"] = ++sno;
                        }
                        else
                            dt.Rows[i]["sno"] = ".";
                    }
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }
        #endregion

    }
} //7337 end

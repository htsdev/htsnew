using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using lntechNew.Components;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.Reports
{
	/// <summary>
	/// Summary description for FrmMainCONTINUANCE.
	/// </summary>
	public partial class FrmMainCONTINUANCE : System.Web.UI.Page
	{
		int ticketno;
		int empid;
		clsSession uSession = new clsSession();
		protected System.Web.UI.WebControls.ImageButton IBtn;		
		clsENationWebComponents ClsDb = new clsENationWebComponents(); 
		clsLogger bugTracker = new clsLogger();
        string EmpID = string.Empty;

		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				//ticketno =Convert.ToInt32(uSession.GetCookie("sTicketID",this.Request));
				ticketno =Convert.ToInt32(Request.QueryString["casenumber"]);
				ViewState["vTicketId"]=ticketno;
                EmpID = uSession.GetCookie("sEmpID", this.Request);
                //Aziz 2603 01/17/08 Bug was fixed in previous version by Aswar
                if (EmpID != "" && EmpID.Length > 0)
                    empid = Convert.ToInt32(EmpID);
				Session["Batch"] = Request.QueryString["Batch"].ToString();
			}
			catch(Exception ex)
			{
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support  - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.IBtn.Click += new System.Web.UI.ImageClickEventHandler(this.IBtn_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	
		private void cratereportCon()
		{
			try
			{
				clsCrsytalComponent Cr = new clsCrsytalComponent();
				string[] key = {"@Ticketid","@employeeid"};
				object[] value1 = {ticketno,empid};
				Int32  LetterType = 5;			
				string filename  = Server.MapPath("") + "\\CONTINUANCE.rpt";
				Cr.CreateReportWord(filename,"USP_HTS_Continuance_Letter", key, value1,"false",LetterType, this.Session, this.Response );		
			}
			catch(Exception ex)
			{
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}
		
		private void IBtn_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			cratereportCon();
		}
	}
}

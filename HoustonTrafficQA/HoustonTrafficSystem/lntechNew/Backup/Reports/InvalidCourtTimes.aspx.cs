using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;
using WebSupergoo.ABCpdf6;
using WebSupergoo.ABCpdf6.Objects;
using WebSupergoo.ABCpdf6.Atoms;

namespace lntechNew.Reports
{
    public partial class InvalidCourtTimes: System.Web.UI.Page
    {
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        clsCourts ClsCourt = new clsCourts();
        clsCaseStatus ClsCaseStatus = new clsCaseStatus();
        clsLogger bugTracker = new clsLogger();

        protected void Page_Load(object sender, EventArgs e)
        {
            lbl_Message.Text = "";

            if (!IsPostBack)
            {
                try
                {
                 
                    FillGrid();

                }
                catch (Exception ex)
                {
                    lbl_Message.Text = ex.Message;
                    bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                }
            }

        }

        private void FillGrid()
        {
            try
            {
              DataTable dtRecords = GetRecords();
                if (dtRecords.Rows.Count == 0)
                {
                    lbl_Message.Text = "No records!";
                }
              
                DataSet ds = new DataSet();
                ds.Tables.Add(dtRecords.Copy()); ;
               
                gv_Result.DataSource = dtRecords;
                gv_Result.DataBind();
                
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        
        private DataTable GetRecords()
        {

            DataTable dt = ClsDb.Get_DT_BySPArr("USP_RPT_Invalid_CourtTimes_ver2");
            DataColumn dc = new DataColumn("S No", typeof(int));
            dt.Columns.Add(dc);
            dc.SetOrdinal(0);

            int no = 1;
            string ticketid = "";

            foreach (DataRow dr in dt.Rows)
            {
                string tid = dr["TicketID_PK"].ToString();
                if (tid != "")
                {
                    if (no == 1)
                    {
                        ticketid = tid;
                        dr["S No"] = no;
                        no = no + 1;
                    }
                    else
                    {
                        if (ticketid != tid)
                        {
                            dr["S No"] = no;
                            ticketid = tid;
                            no = no + 1;
                        }
                        else
                        {
                            dr["S No"] = no - 1;
                        }
                        
                    }   
              
                }
            }
           

            return dt;
        }

        protected void gv_Result_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowIndex > 0)
                {
                    GridViewRow gvr = gv_Result.Rows[e.Row.RowIndex - 1];
                    HyperLink hl_prevSno = (HyperLink)gvr.FindControl("hl_SNo");
                    HyperLink hl_currSno = (HyperLink)e.Row.FindControl("hl_SNo");

                    if (hl_currSno != null && hl_prevSno != null)
                    {
                        if (hl_prevSno.Text == hl_currSno.Text)
                        {
                            //hl_currSno.Attributes.Add("Display", "none");
                            hl_currSno.Style[HtmlTextWriterStyle.Display] = "none";
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
            }
        }

        protected void gv_Result_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_Result.PageIndex = e.NewPageIndex;
                if (gv_Result.PageIndex > gv_Result.PageCount)
                { }
                else
                {
                    FillGrid();
                }
            }
           catch(Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
           }

        }

        


    }
}

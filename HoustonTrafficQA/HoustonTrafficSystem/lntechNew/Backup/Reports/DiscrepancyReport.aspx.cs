using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace HTP.Reports
{
	/// <summary>
	/// 
	/// </summary>
    /// 
	public partial class DiscrepancyReport : System.Web.UI.Page
	{

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_submit.Click += new System.EventHandler(this.Button1_Click);
            this.cmbPageNo.SelectedIndexChanged += new System.EventHandler(this.cmbPageNo_SelectedIndexChanged_1);
            this.dg_valrep.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_valrep_ItemCommand);
            this.dg_valrep.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dg_valrep_PageIndexChanged);
            this.dg_valrep.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dg_valrep_ItemDataBound);
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion
    
        #region Controls

        protected eWorld.UI.CalendarPopup calQueryFrom;
        protected System.Web.UI.WebControls.DataGrid dg_valrep;
		protected System.Web.UI.WebControls.Button btn_submit;
		protected eWorld.UI.CalendarPopup calQueryTo;
		protected System.Web.UI.WebControls.Label lbl_message;
		protected System.Web.UI.WebControls.DropDownList cmbPageNo;
		protected System.Web.UI.WebControls.Label lblGoto;
		protected System.Web.UI.WebControls.Label lblPNo;
		protected System.Web.UI.WebControls.Label lblCurrPage;
		protected System.Web.UI.WebControls.CheckBox chk_showall;

        #endregion

        #region Variables
                
        DataSet ds_val;
        clsENationWebComponents ClsDb = new clsENationWebComponents();
		clsSession ClsSession = new clsSession(); 
		static int counter;

        #endregion

        #region Events
        
        private void Page_Load(object sender, System.EventArgs e)
		{
			//Validating session
			if (ClsSession.IsValidSession(this.Request)==false)
			{
				Response.Redirect("../frmlogin.aspx",false);
			}
			else
			{
                //Zeeshan Ahmed 3979 05/20/2008 Add New Update Violation Control
                UpdateViolationSRV1.PageMethod += 
                delegate() 
                {
                    lbl_message.Text = UpdateViolationSRV1.ErrorMessage;
                    FillGrid(); 
                };
               
                lbl_message.Text = "";

				if(Convert.ToString(Session["FromUpdate"])=="true")
				{
					FillGrid();
					Session["FromUpdate"]="false";
				}

				if(!IsPostBack)
				{
					calQueryFrom.SelectedDate=DateTime.Now.Date;
					calQueryTo.SelectedDate=DateTime.Now.Date;				
				}
				//When no items then disable the controls
				if(dg_valrep.Items.Count<1)
				{
					lblCurrPage.Visible=false;
					lblGoto.Visible=false;
					lblPNo.Visible=false;
					cmbPageNo.Visible=false;
				}			
			}
		}

		//When search button is clicked
		private void Button1_Click(object sender, System.EventArgs e)
		{
			dg_valrep.CurrentPageIndex=0;
            FillGrid();
        }

        //In order to redirect to violation fees page
        private void dg_valrep_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            DataRowView drv = (DataRowView)e.Item.DataItem;
            if (drv == null)
                return;

            try
            {

                LinkButton lnkBtn = (LinkButton)e.Item.FindControl("lnkb_manverify");
                lnkBtn.Attributes.Add("onClick", "return showpopup('" + UpdateViolationSRV1.ClientID + "','" + e.Item.ClientID + "',0);");

                ((HyperLink)e.Item.FindControl("HLTicketno")).NavigateUrl = "../ClientInfo/ViolationFeeold.aspx?search=0&casenumber=" + (int)drv["TicketID_PK"];

                //extend # with courtnumber
                if (((Label)e.Item.FindControl("lbl_verifiedcrtno")).Text != "")
                {
                    ((Label)e.Item.FindControl("lbl_verifiedcrtno")).Text = "#" + ((Label)e.Item.FindControl("lbl_verifiedcrtno")).Text;
                }

            }
            catch
            {

            }

        }
        
        private void dg_valrep_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
        {
            dg_valrep.CurrentPageIndex = e.NewPageIndex;
            FillGrid();
        }
       
        //When manual verify is clicked	
        private void dg_valrep_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            if (e.CommandName == "Clicked")
            {	//To avoid event firing when popup windows is closed
                if (Convert.ToString(Session["UP"]) != counter.ToString())
                {
                    try
                    {
                        string ticketid = ((Label)e.Item.FindControl("lbl_ticketid")).Text;
                        int violationid = 0;
                        string search = ((Label)e.Item.FindControl("lbl_search")).Text;

                        ClsSession.CreateCookie("sTicketID", ticketid, this.Request, this.Response);
                        violationid = Convert.ToInt32(((Label)(e.Item.FindControl("lbl_violationid"))).Text);
                        //	Session["Update"]=violationid;
                        //	if (violationid!=Convert.ToInt32(Session["Update"]))
                        //	{
                        HttpContext.Current.Response.Write("<script language='javascript'>window.open('../ClientInfo/UpdateViolation.aspx?ticketid=" + ticketid + "&search=" + search + "&ticketsViolationID=" + violationid + "','','status=yes,left=20,top=20, width=850,height=600,scrollbars=yes'); </script>");
                        //	}
                        counter += 1;
                        Session["UP"] = counter;

                        //	((LinkButton)e.Item.FindControl("lnkb_manverify")).Attributes.Add("OnClick","return OpenEditWin(" + violationid + ");"); 
                    }
                    catch (Exception ex)
                    {
                        lbl_message.Text = ex.Message;
                    }
                }
                else
                {
                    counter += 1;
                }
            }
        }


        #endregion

        #region Methods

        //Method to fill grid
        private void FillGrid()
        {
            int showall = 0;
            if (chk_showall.Checked == true)
            {
                showall = 1;
            }
            // Add Court Location By Zeeshan Ahmed On 23rd August 2007
            string[] key ={ "@startdate", "@enddate", "@showall", "@courtlocation" };
            object[] value1 ={ calQueryFrom.SelectedDate, calQueryTo.SelectedDate, showall, ddl_courtlocation.SelectedValue };
            ds_val = ClsDb.Get_DS_BySPArr("usp_discrepancyReport", key, value1);

            if (ds_val.Tables[0].Rows.Count < 1)
            {
                lbl_message.Text = "No Record Found";
            }

            dg_valrep.DataSource = ds_val;
            dg_valrep.DataBind();
            BindReport();
            FillPageList();
            SetNavigation();
        }

        //Method To Generate Serial No
        private void BindReport()
        {
            ///	int violationid=0;
            //	string ticketid="";
            long sNo = (dg_valrep.CurrentPageIndex) * (dg_valrep.PageSize);
            try
            {
                foreach (DataGridItem ItemX in dg_valrep.Items)
                {
                    sNo += 1;

                    ((Label)(ItemX.FindControl("lbl_Sno"))).Text = sNo.ToString();
                    //	violationid=Convert.ToInt32(((Label)(ItemX.FindControl("lbl_violationid"))).Text);
                    //	ticketid=((Label)(ItemX.FindControl("lbl_ticketid"))).Text;				
                    //	ClsSession.SetSessionVariable("sTicketID",ticketid,this.Session);
                    //	((LinkButton) ItemX.FindControl("lnkb_manverify")).Attributes.Add("OnClick","return OpenEditWin(" + violationid + ");"); 


                }
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
            }
        }

        #endregion
        
        #region Navigation Logic

        //----------------------------------Navigational LOGIC-----------------------------------------//

        // Procedure for filling page numbers in page number combo........
        private void FillPageList()
        {
            Int16 idx;

            cmbPageNo.Items.Clear();
            for (idx = 1; idx <= dg_valrep.PageCount; idx++)
            {
                cmbPageNo.Items.Add("Page - " + idx.ToString());
                cmbPageNo.Items[idx - 1].Value = idx.ToString();
            }
        }

        // Procedure for setting data grid's current  page number on header ........
        private void SetNavigation()
        {
            try
            {
                // setting current page number
                lblPNo.Text = Convert.ToString(dg_valrep.CurrentPageIndex + 1);
                lblCurrPage.Visible = true;
                lblPNo.Visible = true;

                // filling combo with page numbers
                lblGoto.Visible = true;
                cmbPageNo.Visible = true;
                //FillPageList();

                cmbPageNo.SelectedIndex = dg_valrep.CurrentPageIndex;
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
            }
        }

        // Procedure for setting the grid page number as per selected from page no. combo.......
        private void cmbPageNo_SelectedIndexChanged_1(object sender, System.EventArgs e)
        {
            try
            {
                dg_valrep.CurrentPageIndex = cmbPageNo.SelectedIndex;
                FillGrid();
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
            }

        }
        #endregion


	}
}

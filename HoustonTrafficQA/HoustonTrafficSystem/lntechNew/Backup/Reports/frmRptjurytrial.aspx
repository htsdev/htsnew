<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmRptjurytrial.aspx.cs" Inherits="lntechNew.Reports.frmRptjurytrial" %>

<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc2" TagName="MenuTop" Src="../WebControls/MenuTop.ascx" %>

<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Tried Cases Report</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
     <SCRIPT src="../Scripts/boxover.js" type="text/javascript"></SCRIPT>
     <SCRIPT SRC="../Scripts/ClipBoard.js"></SCRIPT>
     
     
     
<script>

        var err = null;
        
        function StateTracePoupup(ControlName)
		{	
		     CursorIcon();
		     err = null;
		     err = document.getElementById(ControlName).innerText;
		     ShowMsg()
		     //document.getElementById("txtb_StateTraceMsg").value = err;
		     //document.getElementById("txtb_StateTraceMsg").style.visibility = 'visible';
		    // document.getElementById("ddl_court").style.visibility='hidden';
		     return false;
		}
		function CursorIcon()
		{
		    document.body.style.cursor = 'pointer';
		}
		function ShowMsg()
        {
            document.getElementById("txt_StateTraceMsg").value=err;
        }
        function CursorIcon2()
		{
		    document.body.style.cursor = 'default';
		    // document.getElementById("ddl_court").style.visibility='visible';
		}
</script>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    	<TABLE id="tblMain" cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
			<TBODY>
				<TR>
					<TD style="width: 790px">
						<uc1:ActiveMenu id="ActiveMenu1" runat="server"></uc1:ActiveMenu></TD>
				</TR>
				<tr>
			<td background="../../images/separator_repeat.gif"  height="11"></td>
				</tr>
				<TR>
					<TD align="center" style="width: 790px">
						<TABLE id="tblSelectionCriteria" cellSpacing="0" cellPadding="0" width="100%" border="0">
							
								<TR>
									<TD style="width: 97px" Height="15px">
										<asp:label id="lblFromDate" runat="server" Height="16px" CssClass="frmtd" Width="65px"> From:</asp:label>
										</TD>
									<TD style="width: 152px"><ew:calendarpopup id="dtFrom" runat="server" Width="80px" EnableHideDropDown="True" Font-Names="Tahoma"
											ControlDisplay="TextBoxImage" CalendarLocation="Bottom" ShowGoToToday="True" AllowArbitraryText="False"
											Culture="(Default)" UpperBoundDate="12/31/9999 23:59:00" PadSingleDigits="True" ToolTip="Select start date"
											ImageUrl="../images/calendar.gif" Font-Size="8pt">
											<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
											<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="White"></WeekdayStyle>
											<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="Yellow"></MonthHeaderStyle>
											<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
												BackColor="AntiqueWhite"></OffMonthStyle>
											<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="White"></GoToTodayStyle>
											<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="LightGoldenrodYellow"></TodayDayStyle>
											<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="Orange"></DayHeaderStyle>
											<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="LightGray"></WeekendStyle>
											<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="Yellow"></SelectedDateStyle>
											<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="White"></ClearDateStyle>
											<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="White"></HolidayStyle>
										</ew:calendarpopup>
										</TD>
									<TD style="width: 80px">
										<asp:label id="lblToDate" runat="server" Height="16px" CssClass="frmtd" Width="60px">To:</asp:label></TD>
									<TD style="WIDTH: 131px"><ew:calendarpopup id="dtTo" runat="server" Width="80px" EnableHideDropDown="True" Font-Names="Tahoma"
											ControlDisplay="TextBoxImage" CalendarLocation="Bottom" ShowGoToToday="True" AllowArbitraryText="False" Culture="(Default)"
											UpperBoundDate="12/31/9999 23:59:00" PadSingleDigits="True" ToolTip="Select end date" ImageUrl="../images/calendar.gif"
											Font-Size="8pt">
											<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
											<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="White"></WeekdayStyle>
											<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="Yellow"></MonthHeaderStyle>
											<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
												BackColor="AntiqueWhite"></OffMonthStyle>
											<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="White"></GoToTodayStyle>
											<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="LightGoldenrodYellow"></TodayDayStyle>
											<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="Orange"></DayHeaderStyle>
											<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="LightGray"></WeekendStyle>
											<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="Yellow"></SelectedDateStyle>
											<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="White"></ClearDateStyle>
											<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="White"></HolidayStyle>
										</ew:calendarpopup></TD>
									<TD >
										</TD>
								</TR>
								<TR>
									<TD style="width: 97px">
										<asp:label id="lblPCCR" runat="server" CssClass="frmtd" Width="85px">Attorney:</asp:label>
										</TD>
									<TD style="width: 152px">
                                        <asp:DropDownList ID="ddl_attorney" runat="server" CssClass="label" Width="106px">
                                        </asp:DropDownList></TD>
									<TD style="width: 80px">
										<asp:label id="Label1" runat="server" CssClass="frmtd" Width="64px">Court:</asp:label>
										</TD>
									<TD>
                                        <asp:DropDownList ID="ddl_court" runat="server" CssClass="label" Width="235px">
                                        </asp:DropDownList></TD>
									<TD align="left">&nbsp; &nbsp;<asp:Button ID="btn_search" runat="server" CssClass="clsbutton" 
                                            Text="Search" OnClick="btn_search_Click" />&nbsp;&nbsp;&nbsp;</TD>
					
					
				</TR>
			</TABLE>
			<tr>
			<td style="width: 790px" align="center">
			<asp:label id="lblMessage" runat="server" Font-Size="X-Small" ForeColor="Red" Width="414px" Visible="False"></asp:label>
			</TD>
			</TR>
			<tr>
			<td background="../../images/separator_repeat.gif" colSpan="7" height="11"></td>
				</tr>
			<TR>
				<td style="width: 790px">
					<table cellSpacing="0" cellPadding="0" width="100%" border="0">
						<tr>
							<td class="clsbutton" width="155" height="20" align="left"><font color="#ffffff">&nbsp;&nbsp;Jury Trial</font></td>
							<td class="clsbutton" align="right" width="20%">
								<table id="tblPageNavigation" cellSpacing="0" cellPadding="0" border="0" style="display:none">
									<tr>
										<td align="right" width="28%" style="height: 18px"><asp:label id="lblCurrPage" runat="server" Width="97px" Font-Names="Verdana" Font-Size="8.5pt"
												ForeColor="White" Font-Bold="True">Current Page :</asp:label></td>
										<td align="left" style="height: 18px; width: 27%;"><asp:label id="lblPNo" runat="server" Width="48px" Font-Names="Verdana" Font-Size="8.5pt" ForeColor="White" Font-Bold="True">0</asp:label></td>
										<td align="right" width="28%" style="height: 18px"><asp:label id="lblGoto" runat="server" Width="16px" Font-Names="Verdana" Font-Size="8.5pt"
												ForeColor="White" Font-Bold="True">Goto</asp:label></td>
										<td align="right" width="17%" style="height: 18px"><asp:dropdownlist id="cmbPageNo" runat="server" CssClass="frmtd" Font-Size="Smaller"
												AutoPostBack="True" OnSelectedIndexChanged="cmbPageNo_SelectedIndexChanged">
                                        </asp:dropdownlist></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</TR>
			<TR>
				<TD style="width: 790px"><asp:datagrid id="dgJuryTrial" runat="server" Width="781px" AutoGenerateColumns="False" AllowPaging="false"
						PageSize="12" OnItemDataBound="dgJuryTrial_ItemDataBound" OnPageIndexChanged="dgJuryTrial_PageIndexChanged">
						<AlternatingItemStyle BackColor="#EEEEEE"></AlternatingItemStyle>
						<Columns>
                            
                            <asp:TemplateColumn HeaderText="Date">
                            
                                <ItemTemplate>
                                    <asp:Label id="lblrecdate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.recdate","{0:d}") %>'></asp:Label>                                    
                                    <asp:Label ID="lblrowid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.rowid") %>'
                                        Visible="False"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                            </asp:TemplateColumn>
                            
                            <asp:TemplateColumn HeaderText="Attorney">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblattorney" Text='<%# DataBinder.Eval(Container, "DataItem.attorney") %>'></asp:Label>                                    
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                            </asp:TemplateColumn>
                            
                            <asp:TemplateColumn HeaderText="Verdict">                            
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblverdict" Text='<%# DataBinder.Eval(Container, "DataItem.verdict") %>'></asp:Label>
                                </ItemTemplate>                                
                                <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                            </asp:TemplateColumn>
                            
                            <asp:TemplateColumn HeaderText="Case No">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblcaseno" Text='<%# DataBinder.Eval(Container, "DataItem.caseno") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                            </asp:TemplateColumn>
                            
                            <asp:TemplateColumn HeaderText="Fine">
                                <ItemTemplate>
                                    <asp:Label ID="lblfine" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.fine","{0:C2}") %>'></asp:Label>
                                </ItemTemplate>                                
                                <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                            </asp:TemplateColumn>
                            
                            <asp:TemplateColumn HeaderText="Brief Facts">
                                <ItemTemplate>
                                
                                                       
                                    
                                     <DIV TITLE="div_status=[on] offsetx=[-418] offsety=[-10] singleclickstop=[on] requireclick=[off] header=[<table border='0' width='400px'><tr><td width='100%' align='right'><img src='../Images/close_button.png' border='0' onmouseover='CursorIcon()' onmouseout='CursorIcon2()' onclick='hideBox()'></td></tr></table>] body=[<table border='0' width='400px'><tr><td><textarea id='txt_StateTraceMsg' name='txt_StateTraceMsg' cols='46' rows='10'></textarea></td></tr></table>] ">
                                <asp:Label ID="lblbrieffacts" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.brieffacts") %>' style="Display :none"></asp:Label>
                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/textfile.gif"  />
                                
                                &nbsp;</DIV>
                                
                                  
                                    
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" Width="75px" CssClass="GrdHeader" />
                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" HorizontalAlign="Center" />
                            </asp:TemplateColumn>
                            
                            <asp:TemplateColumn HeaderText="Edit">
                                <ItemTemplate>
                                    <asp:HyperLink id ="hlnk_edit" runat="server" NavigateUrl='<%# "../activities/jurytrial.aspx?id="+DataBinder.Eval(Container, "DataItem.rowid") %>'    Text="Edit" ></asp:HyperLink>
                                    
                                </ItemTemplate>
                                 <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                            </asp:TemplateColumn>
						
						</Columns>
						<PagerStyle NextPageText="Next &gt;" PrevPageText="&lt; Previous" HorizontalAlign="Center" PageButtonCount="5"></PagerStyle>
					</asp:datagrid></TD>
			</TR>
			<tr>
			<td background="../../images/separator_repeat.gif"  height="11"></td>
				</tr>
				<TR>
												<TD ><uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
											</TR>
			
			</TBODY>
			</TABLE>
    </form>
</body>
</html>

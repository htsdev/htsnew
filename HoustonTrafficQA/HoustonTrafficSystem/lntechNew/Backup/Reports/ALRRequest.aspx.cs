using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;
using lntechNew.Components;

namespace lntechNew.Reports
{
    public partial class ALRRequest : System.Web.UI.Page
    {
        ValidationReports reports = new ValidationReports();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindGrid();
            }
            Pagingctrl.PageIndexChanged +=new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
        }

        protected void BindGrid()
        {
            reports.getReportData(gv_records, lbl_message, ValidationReport.ALRRequest);
            Pagingctrl.PageCount = gv_records.PageCount;
            Pagingctrl.PageIndex = gv_records.PageIndex;
            Pagingctrl.SetPageIndex();

        }

        void Pagingctrl_PageIndexChanged()
        {
            gv_records.PageIndex = Pagingctrl.PageIndex - 1;
            BindGrid();
        }

        protected void gv_records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gv_records.PageIndex = e.NewPageIndex;
            BindGrid();
        }

    }


}

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.Reports
{
    public partial class DocketQuickUpdate : System.Web.UI.Page
    {
        private clsENationWebComponents clsDB = new clsENationWebComponents("Connection String");
        private DataTable dtRecord;
        clsLogger bugTracker = new clsLogger();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetCourtStatus();
                GetData();
                FillData();
                
            }
        }

        private void FillData()
        {
            try
            {
                if (dtRecord.Rows.Count > 0)
                {
                    DateTime courtdate = Convert.ToDateTime(dtRecord.Rows[0]["CourtDatemain"].ToString());
                    txt_DD.Text = courtdate.Day.ToString();
                    txt_MM.Text = courtdate.Month.ToString();
                    txt_YYYY.Text = courtdate.Year.ToString();


                    if (ddl_CourtTime.Items.FindByText(courtdate.ToShortTimeString().ToLower()) != null)
                        ddl_CourtTime.Items.FindByText(courtdate.ToShortTimeString().ToLower()).Selected = true;

                    if (ddl_CourtTime.Items.FindByText(courtdate.ToShortTimeString().ToUpper()) != null)
                        ddl_CourtTime.Items.FindByText(courtdate.ToShortTimeString().ToUpper()).Selected = true;

                    txt_CourtLoc.Text = dtRecord.Rows[0]["CourtNumbermain"].ToString();

                    ddl_QuickUpdate_Status.SelectedValue = dtRecord.Rows[0]["CourtViolationStatusIDMain"].ToString();

                    ViewState["BondFlag"] = dtRecord.Rows[0]["BondFlag"];
                }
            }
            catch (Exception ex)
            {
                lbl_Error.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        private void GetData()
        {
            try
            {
                lbl_Error.Text = "";
                string[] Keys = { "@TicketsViolationID" };
                object[] Values = { Request.QueryString["TicketID"] };

                dtRecord = clsDB.Get_DT_BySPArr("USP_HTS_DocketCloseOut_Get_Single_Record", Keys, Values);
            }
            catch (Exception ex)
            {
                lbl_Error.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void btn_QuickUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                string[] keys = { "@TicketsViolationID", "@CourtDate", "@CourtLoc","@EmpID", "@Status" };
                object[] values = { Request.QueryString["TicketID"], txt_MM.Text + "/" + txt_DD.Text + "/" + txt_YYYY.Text + " " + Convert.ToDateTime(ddl_CourtTime.SelectedValue).ToShortTimeString(), txt_CourtLoc.Text, Convert.ToInt32(Request.QueryString["emp"]) ,ddl_QuickUpdate_Status.SelectedValue };
                clsDB.ExecuteSP("USP_HTS_DOCKETCLOSEOUT_QuickUpdate", keys, values);
                HttpContext.Current.Response.Write("<script language='javascript'> opener.location.reload(); self.close();   </script>");
                
            }

            catch (Exception ex)
            {
                lbl_Error.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void GetCourtStatus()
        {
            try
            {
                DataTable dtStatus = clsDB.Get_DT_BySPArr("USP_HTS_DOCKETCLOSEOUT_GET_ALL_COURTSTATUS");
                ddl_QuickUpdate_Status.Items.Clear();
                ddl_QuickUpdate_Status.Items.Add("<-- Choose -->");
                foreach (DataRow dr in dtStatus.Rows)
                {
                    ListItem li = new ListItem(dr["Description"].ToString(), dr["CourtViolationStatusID"].ToString());
                    ddl_QuickUpdate_Status.Items.Add(li);
                }
            }
            catch (Exception ex)
            {
                lbl_Error.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

		
    }
}



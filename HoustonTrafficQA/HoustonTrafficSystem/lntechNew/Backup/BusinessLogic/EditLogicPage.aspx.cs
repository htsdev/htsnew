using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;
using HTP.Components;


namespace HTP.BusinessLogic
{
    public partial class EditLogicPage : System.Web.UI.Page
    {
        BussinessLogic objBusinessLogic = new BussinessLogic();
       
        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            //lblMessage.Text = " ";
            if (!IsPostBack)
            {
                try
                {
                    //lblMessage.Text = " ";
                    DataTable dtBusinessLogic = new DataTable();
                    ViewState["sMenu"] = Convert.ToInt32(Request.QueryString["MenuID"]);
                    dtBusinessLogic = objBusinessLogic.GetBusinessLogicByMenuID(Convert.ToInt32(ViewState["sMenu"]));
                    if (dtBusinessLogic.Rows.Count > 0)
                    {
                        string MyBussinessLogic = dtBusinessLogic.Rows[0]["BusinessLogic"].ToString();
                        MyBussinessLogic = MyBussinessLogic.Replace("&nbsp;", string.Empty);

                        if (MyBussinessLogic.Trim() != string.Empty)
                        {
                            ftbBusinessLogic.Text = dtBusinessLogic.Rows[0]["BusinessLogic"].ToString();
                        }
                        else
                        {
                            ftbBusinessLogic.Text = "Business Logic not yet available.";
                        }
                    }
                    else
                    {
                        ftbBusinessLogic.Text = "Business Logic not yet available.";
 
                    }
                    hlk_BackLogicPage.NavigateUrl = "../BusinessLogic/LogicPage.aspx?" + Request.QueryString.ToString();
                }
                catch (Exception ex)
                {
                    lblMessage.Text = ex.Message;
                    lblMessage.Visible = true;
                    clsLogger.ErrorLog(ex);
                    
                }
            }
        }

        

        protected void btnupSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                lblMessage.Text = " ";
                if (ftbBusinessLogic.Text != string.Empty && ftbBusinessLogic.Text!=null)
                {
                    bool isUpdated = objBusinessLogic.UpdateNewBussinessLogic(Convert.ToInt32(ViewState["sMenu"]), ftbBusinessLogic.Text);
                    Response.Redirect("~/BusinessLogic/logicpage.aspx?" + Request.QueryString.ToString(),false);
                }
                else
                {
                    
                    lblMessage.Text = "Please insert the Text before Click the Submit button";
                    lblMessage.Visible = true;
                }
                
            }
            catch (Exception ex)
            {
              lblMessage.Text = ex.Message;
              lblMessage.Visible = true;
              clsLogger.ErrorLog(ex);
            }

        }
        #endregion
    }
}

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LogicPage.aspx.cs" Inherits="HTP.BusinessLogic.LogicPage" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Business Logic</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
//    Fahad 16/01/2009 5376 --ask to delete from User---- 
    function DeleteCheck()
	{
		var x=confirm("Are you sure you want to delete this document.[OK] Yes [Cancel] No");
		if(x)
		{	
			//document.getElementById("FPUpload").outerHTML = "";  
			document.getElementById("FPUpload").outerHTML = document.getElementById("FPUpload").outerHTML.substring(0,56)+">"
            document.getElementById("hf_CheckDelete").value ="1";
            return true;	        
		}
		else
		{
			 document.getElementById("hf_CheckDelete").value ="0";	
			 return false;
		}
				
	}
//    Fahad 16/01/2009 5376 --Open new window for bussinessLogic---- 
	function OpenPopUpNew(path)  //(var path,var name)
	{

		    window.open(path,'',"height=540,width=760,resizable=no,status=no,toolbar=no,scrollbars=yes,menubar=no,location=center");
		    //window.closed();
		    return false;
		
    }
    function ViewCheck()
    {
        return true;
    }

    //    Fahad 18/02/2009 5376 --Allowed file extensions and file size checking---- 
    function CheckEmpty()
    { 
   
         if (document.getElementById("FPUpload").value.length==0)
         {
            alert("Please select file to Upload!");
            return false;
         }
         else if(document.getElementById("FPUpload").value.length!="")
         {
            if(document.getElementById("FPUpload").value!=null)
            {
          
                var Filepath = document.getElementById('FPUpload').value;
                var filext = Filepath.substring(Filepath.lastIndexOf(".")+1);
                if (filext != "pdf" && filext != "html" && filext != "mhtml" && filext != "doc" && filext != "docx" && filext != "xls" && filext != "xlsx" && filext != "vsd")
                {
                    alert("Invalid file format! Supported file formats are .pdf, .html, .mhtml, .doc, .docx, .xls, .xlsx and .vsd"); 
                    document.getElementById("FPUpload").outerHTML = document.getElementById("FPUpload").outerHTML.substring(0,56)+">"    
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                alert("Please insert Correct File!");
                return false;
            }
       }
      
    }
    
    </script>

</head>
<body style="margin-bottom: 0px; margin-left: 0px; margin-right: 0px; margin-top: 0px">
    <form id="form1" runat="server">
    <table>
        <tr style="font-family: Tahoma">
            <td style="background-image: url(../Images/subhead_bg.gif)" height="34">
                <table width="100%">
                    <tr>
                        <td style="width: 60%">
                            <span class="clssubhead">Business Logic</span>
                        </td>
                        <td id="EditHelp" align="right" style="width: 40%">
                            <asp:HyperLink ID="hlk_EditBusinessLogic" runat="server" Font-Underline="True">Edit Help Content</asp:HyperLink>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label ID="lblMessage" runat="server" Text="" ForeColor="red"> </asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <div id="divBusinessLogic" runat="server" style="overflow: scroll; width: 750px;
                    height: 200px">
                </div>
            </td>
        </tr>
        <tr>
            <td style="background-image: url(../Images/subhead_bg.gif)" height="34">
                <span class="clssubhead">Associate Documents</span>
            </td>
        </tr>
        <tr id="trBusinessLogic" runat="server">
            <td>
                <div style="overflow: scroll; width: 750px; height: 200px">
                    <asp:DataGrid ID="dgrdDoc" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        Font-Names="Verdana" Font-Size="2px" PageSize="20" Width="100%" OnItemCommand="dgrdDoc_ItemCommand">
                        <FooterStyle CssClass="GrdFooter" />
                        <PagerStyle CssClass="GrdFooter" Font-Names="Arial" HorizontalAlign="Center" Mode="NumericPages"
                            NextPageText="" PageButtonCount="50" PrevPageText="" />
                        <AlternatingItemStyle BackColor="#EEEEEE" />
                        <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Center" VerticalAlign="Middle" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="ID" Visible="false">
                                <ItemStyle HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lblDocID" runat="server" CssClass="GrdLbl" Font-Size="Smaller" Text='<%# DataBinder.Eval(Container, "DataItem.HelpDocID_PK") %>'
                                        Visible="False"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Left" Font-Size="Smaller" Width="160px" />
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Uploaded Date">
                                <ItemStyle HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lblDateTime" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.UploadedDate") %>'
                                        Font-Size="Smaller"></asp:Label>&nbsp; &nbsp;
                                </ItemTemplate>
                                <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Left" Font-Size="Smaller" Width="160px" />
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Document Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblDocName" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.DocName") %>'
                                        Font-Size="Smaller"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Left" Font-Size="Smaller" />
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Rep">
                                <ItemTemplate>
                                    <asp:Label ID="lblAbb1" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.UserName") %>'
                                        Font-Size="Smaller"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Left" Font-Size="Smaller" />
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="View">
                                <ItemStyle HorizontalAlign="Center" />
                                <%--<ItemTemplate>
                                    <a onclick='OpenPopUpNew("previewdoc.aspx?docname=<%# DataBinder.Eval(Container, "DataItem.SavedDocName") %>");'>
                                        <asp:Image ImageUrl="../images/Preview.gif" BorderWidth="0" runat="server" ID="img" /></a>
                                </ItemTemplate>--%>
                                <ItemTemplate>
                                    <asp:Label ID="lblSaveDoc" runat="server" CssClass="GrdLbl" Font-Size="Smaller" Text='<%# DataBinder.Eval(Container, "DataItem.SavedDocName") %>'
                                        Visible="False"></asp:Label>
                                    <asp:LinkButton ID="lnkbtn_View" runat="server" CommandName="View" Font-Size="Smaller">
                                        <asp:Image ImageUrl="../images/Preview.gif" BorderWidth="0" runat="server" ID="img" />
                                    </asp:LinkButton>&nbsp;
                                </ItemTemplate>
                                <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Left" />
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Options">
                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkbtn_Delete" runat="server" OnClientClick="return DeleteCheck();"
                                        CommandName="Delete" Font-Size="Smaller">Delete</asp:LinkButton>&nbsp;
                                </ItemTemplate>
                                <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Left" Font-Size="Smaller" />
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </td>
        </tr>
        <tr>
            <td style="background-image: url(../../images/separator_repeat.gif)" height="11">
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr id="UploadSection" runat="server">
                        <td>
                            <asp:Label ID="lblUpload" CssClass="clsLeftPaddingTable" runat="server" Text="Upload File"></asp:Label>
                        </td>
                        <td>
                            <asp:FileUpload ID="FPUpload" runat="server" CssClass="clsInputcommentsfield" />
                        </td>
                        <td>
                            <asp:Button ID="btnUpload" runat="server" CssClass="clsbutton" OnClick="btnUpload_Click"
                                Text="Upload" OnClientClick="return CheckEmpty();" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="background-image: url(../../images/separator_repeat.gif)" height="11">
                <asp:HiddenField ID="hf_CheckDelete" runat="server" />
            </td>
        </tr>
    </table>
    </form>
</body>
</html>

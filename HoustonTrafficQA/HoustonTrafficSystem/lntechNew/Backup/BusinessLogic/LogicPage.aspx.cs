using System;
using System.Data;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using lntechNew.Components.ClientInfo;
using HTP.Components;



namespace HTP.BusinessLogic
{
    //Umair 7837 08/27/2010 add comments tag to remove warning
    /// <summary>
    /// </summary>
    public partial class LogicPage : System.Web.UI.Page
    {
        readonly BussinessLogic _objBusinessLogic = new BussinessLogic();
        DataTable _dtBusinessLogic = new DataTable();

        #region Events
        //Umair 7837 08/27/2010 add comments tag to remove warning
        /// <summary>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["Event"] = null;
            lblMessage.Text = string.Empty;
            if (!IsPostBack)
            {

                try
                {
                    lblMessage.Text = "";
                    clsSession ClsSession = new clsSession();
                    ViewState["sMenu"] = Convert.ToInt32(Request.QueryString["MenuID"]);
                    _dtBusinessLogic = _objBusinessLogic.GetBusinessLogicByMenuID(Convert.ToInt32(ViewState["sMenu"]));
                    ViewState["empid"] = Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request));
                    ViewState["Accesstype"] = _objBusinessLogic.GetAccessType(Convert.ToInt32(ViewState["empid"]));
                    SetView(Convert.ToInt32(ViewState["empid"]));

                   
                    if (_dtBusinessLogic.Rows.Count > 0)
                    {
                        string MyBussinessLogic = _dtBusinessLogic.Rows[0]["BusinessLogic"].ToString();
                        MyBussinessLogic = MyBussinessLogic.Replace("&nbsp;", string.Empty);

                        if (MyBussinessLogic.Trim() != string.Empty)
                        {
                            // Fahad 5376 01/27/2009 Disable decoding to Show html tags too                        
                            divBusinessLogic.InnerHtml = _dtBusinessLogic.Rows[0]["BusinessLogic"].ToString();
                        }
                        else
                        {
                            divBusinessLogic.InnerHtml = "Business Logic not yet available.";
                            divBusinessLogic.Style[HtmlTextWriterStyle.Height] = Convert.ToString(Unit.Pixel(80));
                        }
                    }

                    else
                    {
                        divBusinessLogic.InnerHtml = "Business Logic not yet available.";
                        divBusinessLogic.Style[HtmlTextWriterStyle.Height] = Convert.ToString(Unit.Pixel(80));
                    }

                        hlk_EditBusinessLogic.NavigateUrl = "../BusinessLogic/EditLogicPage.aspx?" + Request.QueryString.ToString();
                        BindGrid();
                    
                }
                catch (Exception ex)
                {
                    lblMessage.Text = ex.Message;
                    lblMessage.Visible = true;
                    clsLogger.ErrorLog(ex);
                   
                }
            }
        }

        /// <summary>
        /// Fahad 5376 12/15/2009
        /// This method is bound the Cloumns and appropirate action to teh grid and occur when user click accordingly
        /// </summary>
        protected void dgrdDoc_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            lblMessage.Text = "";
            int uploadedDocID = System.Convert.ToInt32(((Label)(e.Item.FindControl("lblDocID"))).Text);
            string Savedoc = (((Label)(e.Item.FindControl("lblSaveDoc"))).Text).ToString();

            try
            {
                if (e.CommandName == "Delete")
                {
                    if (hf_CheckDelete.Value == "1")
                    {
                        bool isDeleted = false;
                        isDeleted = _objBusinessLogic.DeleteUploadedDocs(Convert.ToInt32(ViewState["sMenu"]), uploadedDocID);
                        if (isDeleted)
                            Response.Redirect("~/BusinessLogic/logicpage.aspx?" + Request.QueryString.ToString(), false);
                        else
                            lblMessage.Text = "File not deleted successfully";
                    }

                }
                else if (e.CommandName == "View")
                {
                   
                    string helpDocPath = ConfigurationManager.AppSettings["NTPATHHelpDoc"].ToString();
                    string fullFilePath = helpDocPath.Replace("\\\\", "\\") + Savedoc;
                    string strExtensionName = System.IO.Path.GetExtension(fullFilePath);
                    switch (strExtensionName.Trim().ToLower())
                    {
                        case (".pdf"):
                            Response.ContentType = "application/pdf";
                            break;

                        case (".html"):
                            Response.ContentType = "application/html";
                            break;

                        case (".doc"):
                            Response.ContentType = "application/doc";
                            break;

                        case (".vsd"):
                            Response.ContentType = "application/vsd";
                            break;

                        case (".xls"):
                            Response.ContentType = "application/xls";
                            break;

                        case (".docx"):
                            Response.ContentType = "application/docx";
                            break;

                        case (".mhtml"):
                            Response.ContentType = "application/mhtml";
                            break;

                        case (".xlsx"):
                            Response.ContentType = "application/xlsx";
                            break;

                        default:
                            break;
                    }

                    Response.AppendHeader("Content-Disposition", "attachment; filename=" + Savedoc);
                    Response.TransmitFile(fullFilePath);

                }
            }
            catch (Exception ex)
            {
                lblMessage.Visible = true;
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
            finally
            {
                Response.End();
                Response.Clear();
                Response.ClearHeaders();
                Response.Flush();
            }
        }
        
        /// <summary>
        /// Fahad 5376 12/15/2009
        /// This method is Upload the file and occur when user click Upload Button
        /// </summary>
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            try
            {
                lblMessage.Text = "";
                if (FPUpload.PostedFile!=null)
                {
                    bool isUploaded = false;
                    long size =FPUpload.PostedFile.ContentLength; 
                    if (size<= 2097152)
                    {
                        string UniqueFileName = DateTime.Now.ToFileTime().ToString() + "_" + Convert.ToString(ViewState["sMenu"]) + "_" + FPUpload.FileName;
                        string filepath = ConfigurationManager.AppSettings["NTPATHHelpDoc"].ToString() + UniqueFileName;
                        filepath.Replace("////", "//");
                        FPUpload.SaveAs(filepath);
                        isUploaded = _objBusinessLogic.UploadNewDocument(Convert.ToInt32(ViewState["sMenu"]), FPUpload.FileName, Convert.ToInt32(ViewState["empid"]), UniqueFileName);
                        if (!isUploaded)
                        {
                            lblMessage.Text = "There is some problem to Upload the File.";
                        }
                        else
                        {
                            Response.Redirect("~/BusinessLogic/logicpage.aspx?" + Request.QueryString.ToString(), false);

                        }
                    }
                    else
                    {
                        lblMessage.Text = "File size is too large! Maximum file size allowed upto 2MB";
                        lblMessage.Visible = true;
                    }
                }
                else
                {
                    lblMessage.Text = "Please insert Correct File!";
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
                clsLogger.ErrorLog(ex);

            }
        }
        #endregion


        #region Method

        /// <summary>
        /// Fahad 5376 12/15/2009
        /// This method is bind data grid view
        /// </summary>
        public void BindGrid()
        {
            try
            {
                _dtBusinessLogic = _objBusinessLogic.GetUploadedDocument(Convert.ToInt32(ViewState["sMenu"]));
                if (_dtBusinessLogic.Rows.Count > 0)
                {
                    trBusinessLogic.Style["Display"] = "Block";
                    dgrdDoc.DataSource = _dtBusinessLogic;
                    dgrdDoc.DataBind();

                }
                else
                {
                    trBusinessLogic.Style["Display"] = "None";
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
                clsLogger.ErrorLog(ex);

            }
        }

        /// <summary>
        /// Fahad 5376 12/15/2009
        /// to set the view for Secondary and Primary User
        /// </summary>
        /// <param name="empid"></param>
        public void SetView(int empid)
        {
            int useAccessType;
            useAccessType = _objBusinessLogic.GetAccessType(empid);
            if (useAccessType == 1)
            {
                dgrdDoc.Columns[5].Visible = false;
                dgrdDoc.Columns[3].Visible = false;
                hlk_EditBusinessLogic.Visible = false;
                UploadSection.Style["Display"] = "None";
            }
            else
            {
                dgrdDoc.Columns[5].Visible = true;
                dgrdDoc.Columns[3].Visible = true;
                hlk_EditBusinessLogic.Visible = true;
                UploadSection.Style["Display"] = "Block";

            }
        }

        #endregion

    }
}

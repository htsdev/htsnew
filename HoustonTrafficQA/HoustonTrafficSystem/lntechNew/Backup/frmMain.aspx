<%@ Register TagPrefix="uc1" TagName="Footer" Src="WebControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="WebControls/ActiveMenu.ascx" %>

<%@ Page Language="c#" AutoEventWireup="false" Inherits="HTP.frmMain" SmartNavigation="False"
    CodeBehind="frmMain.aspx.cs" ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Home</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <link href="Styles.css" type="text/css" rel="stylesheet" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />

    <script src="Scripts/Validationfx.js" type="text/javascript"></script>

    <script lang="javascript" type="text/javascript">
		    
						
        //ozair 4625 08/27/2008 removed classic search option
        function KeyDownHandler()
        {   
            //Process only the Enter key
            if (event.keyCode == 13)
            {
                //Cancel the default submit
                event.returnValue=false;
                event.cancel = true;
                    
                //Submit the form by programmatically clicking the specified button
                document.getElementById("btnSearchAdvance").click();
            }
        }
	
	    function ValidateInputAdvance()
			{ 
				//error list
				var error1 = 'Choose one of 3 criterias';
				var error2 = 'Ticket Number must be empty';
				var error3 = 'Choose any criteria from dropdownlist';
				
				//var txtTicketNumber = document.getElementById("txtTicketNumber").value;
				var txtSearchKeyWord1ad = document.getElementById("txtSearchKeyWord1ad").value;
				var txtSearchKeyWord2ad = document.getElementById("txtSearchKeyWord2ad").value;
				var txtSearchKeyWord3ad = document.getElementById("txtSearchKeyWord3ad").value;
				var txtTicketNumberad =document.getElementById("txtTicketNumberad").value;
				var txtlid  = document.getElementById("txtLidad").value;
				
				var ddlSearchKeyType1ad = document.getElementById("ddlSearchKeyType1ad");
				var ddlSearchKeyType2ad = document.getElementById("ddlSearchKeyType2ad");
				var ddlSearchKeyType3ad = document.getElementById("ddlSearchKeyType3ad");
				
				var Message = document.getElementById("Message") ;
				
				var flag = false;
			    var V;	
			    if(txtlid != "")
			    {
			        for(i=0; i<txtlid.length; i++)
			        {
			            V = txtlid.charAt(i);
			            if(isNaN(V))
			            {
			             flag=true;
			             break;			    
			            }    			    
			        }
			    }			  
			    if(flag==true)
			    {			      
			        /*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
				   Message.innerHTML = "Invalid Value";
				   document.getElementById("grid").style.display='none';
				   return false;
			    }

				
				if (txtTicketNumberad =="") 
				{
					if ( (txtSearchKeyWord1ad   !="") && (ddlSearchKeyType1ad.value==-1) )
					{/*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
						Message.innerHTML = error3;
						document.getElementById("grid").style.display='none';
						return false;
					}
					
					if ( (txtSearchKeyWord2ad   !="") && (ddlSearchKeyType2ad.value==-1) )
					{/*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
						Message.innerHTML = error3;
						document.getElementById("grid").style.display='none';
						return false;
					}										
					
					if ( (txtSearchKeyWord3ad   !="") && (ddlSearchKeyType3ad.value==-1) )
					{/*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
						Message.innerHTML = error3;
						document.getElementById("grid").style.display='none';
						return false;
					}	
				}
								
				if(isNaN(txtlid))
				{/*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
				    Message.innerHTML = "Please enter numeric value.";
					document.getElementById("grid").style.display='none';
					return false;
				}
				
				if ( txtlid != "" && txtlid.length > 8 )
                {/*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
                    Message.innerHTML = "Letter Id cannot be greater than eight character.";
                    document.getElementById("grid").style.display='none';						
                    return false;
                }
								
				    if ( (txtSearchKeyWord1ad!="") && (ddlSearchKeyType1ad.value==-1) )
					{/*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
						Message.innerHTML = error3;
						document.getElementById("grid").style.display='none';
						return false;
					}
					
					if ( (txtSearchKeyWord2ad!="") && (ddlSearchKeyType2ad.value==-1) )
					{/*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
						Message.innerHTML = error3;
						document.getElementById("grid").style.display='none';
						return false;
					}									
					if ( (txtSearchKeyWord3ad!="") && (ddlSearchKeyType3ad.value==-1) )
					{/*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
						Message.innerHTML = error3;
						document.getElementById("grid").style.display='none';
						return false;
					}
				
				//Zeeshan Ahmed 3124 02/19/2008				
				//Validate First Keyword
				if (!ValidateKeyWord('txtSearchKeyWord1ad','ddlSearchKeyType1ad')) return false;
				
				//Validate Second Keyword
				if (!ValidateKeyWord('txtSearchKeyWord2ad','ddlSearchKeyType2ad')) return false;
				
				//Validate Third Keyword
				if (!ValidateKeyWord('txtSearchKeyWord3ad','ddlSearchKeyType3ad')) return false;
				
				document.getElementById("grid").style.display='block';
				/*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
				Message.innerHTML="";
				
				//Nasir 5863 05/30/2009 payment date and hire date cannot be seach at the same time
				if($get("rdbtnQuotead").checked)
				{				
				    var ddlSearchKeyType1=document.getElementById("ddlSearchKeyType1ad");
				    var ddlSearchKeyType2=document.getElementById("ddlSearchKeyType2ad");
				    var ddlSearchKeyType3=document.getElementById("ddlSearchKeyType3ad");
				    var ddlSearchKeyWord1 = document.getElementById("txtSearchKeyWord1ad");
                    var ddlSearchKeyWord2 = document.getElementById("txtSearchKeyWord2ad");
                    var ddlSearchKeyWord3 = document.getElementById("txtSearchKeyWord3ad");
				    if(ddlSearchKeyType1.options[11].selected && ddlSearchKeyWord1.value != "")
				    {
				       if((ddlSearchKeyType2.options[12].selected &&ddlSearchKeyWord2.value != "") ||(ddlSearchKeyType3.options[12].selected && ddlSearchKeyWord3.value != ""))  
				       {/*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
				        Message.innerHTML="System not allow to group search on hire date and payment date simultaneously"
				        return false;
				       }
				    }
				    if(ddlSearchKeyType2.options[11].selected && ddlSearchKeyWord2.value != "")
				    {
				       if((ddlSearchKeyType1.options[12].selected && ddlSearchKeyWord1.value != "") ||(ddlSearchKeyType3.options[12].selected && ddlSearchKeyWord3.value != ""))  
				       {/*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
				        Message.innerHTML="System not allow to group search on hire date and payment date simultaneously"
				        return false;
				       }
				    }
				    if(ddlSearchKeyType3.options[11].selected && ddlSearchKeyWord3.value != "")
				    {
				       if((ddlSearchKeyType1.options[12].selected && ddlSearchKeyWord1.value != "") ||(ddlSearchKeyType2.options[12].selected && ddlSearchKeyWord2.value != ""))  
				       {/*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
				        Message.innerHTML="System not allow to group search on hire date and payment date simultaneously"
				        return false;
				       }
				    }
				    if(ddlSearchKeyType1.options[12].selected && ddlSearchKeyWord1.value != "")
				    {
				       if((ddlSearchKeyType2.options[11].selected && ddlSearchKeyWord2.value != "") ||(ddlSearchKeyType3.options[11].selected && ddlSearchKeyWord3.value != ""))  
				       {/*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
				        Message.innerHTML="System not allow to group search on hire date and payment date simultaneously"
				        return false;
				       }
				    }
				    if(ddlSearchKeyType2.options[12].selected && ddlSearchKeyWord2.value != "")
				    {
				       if((ddlSearchKeyType1.options[11].selected && ddlSearchKeyWord1.value != "") ||(ddlSearchKeyType3.options[11].selected && ddlSearchKeyWord3.value != ""))  
				       {/*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
				        Message.innerHTML="System not allow to group search on hire date and payment date simultaneously"
				        return false;
				       }
				    }
				    if(ddlSearchKeyType3.options[12].selected && ddlSearchKeyWord3.value != "")
				    {
				       if((ddlSearchKeyType1.options[11].selected && ddlSearchKeyWord1.value != "") ||(ddlSearchKeyType2.options[11].selected && ddlSearchKeyWord2.value != ""))  
				       {/*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
				        Message.innerHTML="System not allow to group search on hire date and payment date simultaneously"
				        return false;
				       }
				    }
				}
				
				//Nasir 4921 07/15/2009 check for enter at least one value to search
				
				    if($get("txtLidad").value=="" &&  $get("txtcausenumberad").value==""  &&  $get("txtTicketNumberad").value=="" &&  $get("txtSearchKeyWord1ad").value=="" &&  $get("txtSearchKeyWord2ad").value=="" &&  $get("txtSearchKeyWord3ad").value=="")
				    {
				        alert("Blank search is not allowed, Please enter some value.")    
				        return false;
				    }
				
			}			 
    </script>

    <script type="text/javascript">
	    
      
//ozair 4625 08/27/2008 removed classic search option      
function ResetControls()
{
    document.getElementById("txtSearchKeyWord3ad").value="";
    document.getElementById("txtSearchKeyWord2ad").value="";
    document.getElementById("txtSearchKeyWord1ad").value="";
    document.getElementById("txtLidad").value="";
    document.getElementById("txtTicketNumberad").value="";
    document.getElementById("txtcausenumberad").value="";
    document.getElementById("ddlSearchKeyType1ad").selectedIndex=3;
    document.getElementById("ddlSearchKeyType2ad").selectedIndex=2;
    document.getElementById("ddlSearchKeyType3ad").selectedIndex=5;
    document.getElementById("ddlCourtsAd").selectedIndex=0;
    
    document.getElementById("rdbtnClientad").checked=true; 
    document.getElementById("rdbtnQuotead").checked=true;
    document.getElementById("rdbtnNonClientad").checked=false;
    document.getElementById("rdbtnJimsad").checked=false;
    
    if(document.getElementById("dgResult") != null)
    document.getElementById("dgResult").style.display="none";

    if(document.getElementById("lblResults") != null)
    document.getElementById("lblResults").style.display="none";
    
    if(document.getElementById("tblColorCode") != null)
    document.getElementById("tblColorCode").style.display="none";
/*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
    document.getElementById("Message").innerHTML="";
    //Nasir 4921 07/16/2009 resolve reset issue
    AddSearchOption();
    return false;
}
      
function CheckCaseType()
{
    if ( document.getElementById("ddlCaseType").value == "0")
    {
        alert("Please select case type.");
        document.getElementById("ddlCaseType").focus();
        return false;
    }
    closeViolationAmountPopup();
    return true;
}

function closeViolationAmountPopup()
{
    var modalPopupBehavior = $find('MPECaseCategory');
    modalPopupBehavior.hide();
    return false;
}

    </script>

    <script id="SmartValidations">


function ValidateKeyWord( textboxid , listid)
{
   
    var searchText = document.getElementById(textboxid).value;
    var option = document.getElementById(listid).value;
    var Message = document.getElementById("Message") ;
    
    if ( option != -1 && searchText !="" )
    {
       
       //Validate DOB , Court Date and Contact Date
       //Nasir 5863 05/06/2009 check for hire date and payment date and contact date
       if ( option ==4 ||   option ==6 ||  option == 8 ||  option == 11 ||  option == 12 ||  option == 13) 
       {    
            if(chkdob(searchText)==false)
			{/*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
				Message.innerHTML ="Invalid Date";
				document.getElementById("grid").style.display='none';
				return false;
			}
            
            if (!CheckDate(searchText))
            {        /*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
                Message.innerHTML = "Date should be greater than or equal to 1/1/1900.";            
                return false;
            }
            
            if ( option==4 && CheckDOB(searchText) ==false ) 
            {/*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
                Message.innerHTML ="Date of birth can not be in future.";
		        document.getElementById("grid").style.display='none';
		        return false;
            }	
       }    
       
       //Validate Phone Number
       if ( option == 5 )
       {       
           if (!checknumber(searchText))       
                //if (isNaN(searchText))
           {          /*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
                    Message.innerHTML = "Invalid phone number.";            
                    return false;
           }           
       }   
        
      // Noufil 3478 03/24/2008 number are not allowed to input
       //Validate First Name and Last Name       
       if ( option == 1 || option == 2)
       {
            if (!isNaN(searchText))
            {/*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
                Message.innerHTML = "Name should only contains alphabets.";            
                return false;
            }           
            if(alphanumeric(searchText)==false)
            {/*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
                Message.innerHTML = "Name should only contains alphabets.";            
                return false;
            }
            
            /*if ( !CheckName(searchText))
            {
                Message.innerText = "Name should only contains alphabets.";            
                return false;
            }*/
       }   
       //Validate Letter ID
       //ozair 4625 08/26/2008 input string error resolved
       //excluded check for criminal case type as we are also sending letters to them.
       if ( option == 9 )
       {
            
            if(searchText != "")
		    {
		        for(i=0; i<searchText.length; i++)
		        {
		            V = searchText.charAt(i);
		            if(isNaN(V))
		            {/*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
		            	  Message.innerHTML = "Invalid Value";
				          document.getElementById("grid").style.display='none';
				          return false;    
		            }    			    
		        }
		    }			
                       
            if (searchText!="" && isNaN(searchText) ) 
		    {/*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
			    Message.innerHTML = "Please enter numeric value.";
			    return false;
			}
			
	        if ( searchText != "" && searchText.length > 8 )
            {/*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
                Message.innerHTML = "Letter Id cannot be greater than eight character.";
                return false;
            }
       }
       
        if ( option == 10 )
        {
	        if(document.getElementById("rdbtnJimsad").checked==true)
	        {/*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
		        Message.innerHTML = "Cause number can not be searched for Jims";
		        document.getElementById("grid").style.display='none';						
		        return false;
	        }
        }
        //Fahad 9725 11/21/2011 Added section for Email address option
         if ( option == 14 )//When email address selected
        {
        	 if(isEmail(searchText)== false)
			   {
			       alert ("Please enter Email Address in Correct format.");
			       document.getElementById(textboxid).focus(); 
			       return false;			   
			   }
        }
         
    }    
    return true;
}

// noufil 4275 06/20/2008 This function checks the whether the input are numbers or not
    function checknumber(number)
    {
	    var numaric = number;
	    for(var j=0; j<numaric.length; j++)
		{
		   var alphaa = numaric.charAt(j);
		   var hh = alphaa.charCodeAt(0);
		  //if((hh > 47 && hh<58) || (hh > 64 && hh<91) || (hh > 96 && hh<123))
		  // ASCII code for numbers
		  if (!(hh > 47 && hh<58))
		  {
		    return false;
		  }
		}
		 return true;
	}
   
// noufil 4275 06/20/2008 This function checks the whether the input are alphanumeric or not
    function alphanumeric(alphane)
    {
	    var numaric = alphane;
	    for(var j=0; j<numaric.length; j++)
		    {
		          var alphaa = numaric.charAt(j);
		          var hh = alphaa.charCodeAt(0);
		          //if((hh > 47 && hh<58) || (hh > 64 && hh<91) || (hh > 96 && hh<123))
		          //ASCII code for aphabhets
		          if (!((hh > 64 && hh<91) || (hh > 96 && hh<123)))
		          {
		          return false;
		          }    
		    }
	}


//Check Date Of Birth
function CheckDOB(dateString)
{               
    var dob = new Date(formatDate(dateString));
    var today = new  Date();
           
    if ( dob > today)
    return false;
    
    return true;    
}	 

//Check Court Date Should Be Greater Than 1/1/1900
function CheckDate(dateString)
{
   var mindate = new Date("1/1/1900")
   var userDate = new Date(dateString);  
   
   if ( parseInt(userDate-mindate) < 0) 
   return false;
   
   return true;
}

//Check Name
function CheckName(name)
{
    for ( i = 0 ; i < name.length ; i++)
    {
        var asciicode =  name.charCodeAt(i)
        //If not valid alphabet 
        if (  ! ((asciicode >= 64 && asciicode <=90) || ( asciicode >= 97 && asciicode <=122)))
        return false;
   }
    return true;
}
//Nasir 5863 05/05/2009 remove option hire date payment date and contact date
function RemoveSearchOption()
{ 
if(document.getElementById("ddlSearchKeyType1ad").options[11] != null)
{
  if(document.getElementById("ddlSearchKeyType1ad").options[11].selected|| document.getElementById("ddlSearchKeyType1ad").options[12].selected || document.getElementById("ddlSearchKeyType1ad").options[13].selected || document.getElementById("ddlSearchKeyType1ad").options[14].selected)
    {
        document.getElementById("ddlSearchKeyType1ad").options[3].selected=true;
        document.getElementById("txtSearchKeyWord1ad").value='';
    }
    if(document.getElementById("ddlSearchKeyType1ad").options[11].selected || document.getElementById("ddlSearchKeyType2ad").options[12].selected || document.getElementById("ddlSearchKeyType2ad").options[13].selected || document.getElementById("ddlSearchKeyType2ad").options[14].selected)
    {
        document.getElementById("ddlSearchKeyType2ad").options[2].selected=true;
        document.getElementById("txtSearchKeyWord2ad").value='';
    }
    if(document.getElementById("ddlSearchKeyType3ad").options[11].selected || document.getElementById("ddlSearchKeyType3ad").options[12].selected || document.getElementById("ddlSearchKeyType3ad").options[13].selected || document.getElementById("ddlSearchKeyType3ad").options[14].selected)
    {
        document.getElementById("ddlSearchKeyType3ad").options[5].selected=true;
        document.getElementById("txtSearchKeyWord3ad").value='';
    }
   //Afaq 9245 07/21/2011 add one more line in 3 dropdown to remove email address.
   RemoveOptionsFromEnd(document.getElementById("ddlSearchKeyType1ad"),4);
   RemoveOptionsFromEnd(document.getElementById("ddlSearchKeyType2ad"),4);
   RemoveOptionsFromEnd(document.getElementById("ddlSearchKeyType3ad"),4);
 }   
}

//Nasir 5863 05/05/2009 add option hire date payment date and contact date
function AddSearchOption()
{
       //Afaq 9245 07/21/2011 add email address in the dropdown lists and call removesearchoption to remove the already added values.
       RemoveSearchOption();
       CreateAndAddOption('ddlSearchKeyType1ad','Hire Date','11');
       CreateAndAddOption('ddlSearchKeyType1ad','Payment Date','12');
       CreateAndAddOption('ddlSearchKeyType1ad','Last Contact Date','13');
       CreateAndAddOption('ddlSearchKeyType1ad','Email address','14');
       
       CreateAndAddOption('ddlSearchKeyType2ad','Hire Date','11');
       CreateAndAddOption('ddlSearchKeyType2ad','Payment Date','12');
       CreateAndAddOption('ddlSearchKeyType2ad','Last Contact Date','13');
       CreateAndAddOption('ddlSearchKeyType2ad','Email address','14');
       
       CreateAndAddOption('ddlSearchKeyType3ad','Hire Date','11');
       CreateAndAddOption('ddlSearchKeyType3ad','Payment Date','12');
       CreateAndAddOption('ddlSearchKeyType3ad','Last Contact Date','13');
       CreateAndAddOption('ddlSearchKeyType3ad','Email address','14');
        
}

    </script>

    <%--Muhammad Nadir Siddiqui 9245 05/04/2011 Increased textbox length for Email address option--%>

    <script type="text/javascript">
function setTextBoxLength(val,txtbox)
{
 
   if(val=='14')
   {
     document.getElementById(txtbox).maxLength=100;
   }
}
    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="MainForm" method="post" runat="server" onkeypress="KeyDownHandler();">
    <table id="tblMain" cellspacing="0" cellpadding="0" width="920" align="center" border="0">
        <tr>
            <td colspan="2">
                <uc1:ActiveMenu ID="ActiveMenu1" runat="server" OnLoad="ActiveMenu1_Load"></uc1:ActiveMenu>
            </td>
        </tr>
        <tr>
            <td colspan="2">
            </td>
        </tr>
        <tr>
            <td colspan="2" align="right">
                <table id="Table7" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
                    <tr>
                        <td width="100%" background="images/separator_repeat.gif" colspan="5" style="height: 11px">
                            <aspnew:ScriptManager ID="smFrmMain" runat="server">
                                <Scripts>
                                    <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
                                </Scripts>
                            </aspnew:ScriptManager>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <div id="dSearchCriteria2" runat="server" enableviewstate="true">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td class="clsLeftPaddingTable" align="right" style="height: 26px;">
                                <strong>LID Number</strong>
                            </td>
                            <td align="left" class="clsLeftPaddingTable" style="width: 12px; height: 26px">
                            </td>
                            <td class="clsLeftPaddingTable" align="left" style="height: 26px">
                                <asp:TextBox CssClass="clsInputadministration" ID="txtLidad" runat="server" TabIndex="1"
                                    Width="124px" MaxLength="8" Height="16px"></asp:TextBox>
                            </td>
                            <td align="right" class="clsLeftPaddingTable" style="width: 12px; height: 26px">
                            </td>
                            <td class="clsLeftPaddingTable" align="right" style="width: 115px; height: 26px;">
                                <strong>Cause Number</strong>
                            </td>
                            <td align="left" class="clsLeftPaddingTable" style="width: 12px; height: 26px">
                            </td>
                            <td class="clsLeftPaddingTable" align="left" style="height: 26px">
                                <asp:TextBox CssClass="clsInputadministration" ID="txtcausenumberad" runat="server"
                                    TabIndex="2" Width="124px" MaxLength="20" Height="16px"></asp:TextBox>
                            </td>
                            <td align="right" class="clsLeftPaddingTable" style="width: 12px; height: 26px">
                            </td>
                            <td class="clsLeftPaddingTable" align="right" style="height: 26px">
                                <strong>Ticket Number</strong>
                            </td>
                            <td align="left" class="clsLeftPaddingTable" style="width: 12px; height: 26px">
                            </td>
                            <td align="left" class="clsLeftPaddingTable" style="height: 26px">
                                <asp:TextBox CssClass="clsInputadministration" ID="txtTicketNumberad" runat="server"
                                    TabIndex="3" Width="124px" MaxLength="20" Height="16px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="clsLeftPaddingTable" align="right" style="height: 26px">
                                <asp:DropDownList ID="ddlSearchKeyType2ad" runat="server" CssClass="clsInputCombo"
                                    TabIndex="4" Width="124px" Height="19px" onchange="setTextBoxLength(this.value,'txtSearchKeyWord2ad');">
                                    <asp:ListItem Value="-1">&lt; Choose &gt;</asp:ListItem>
                                    <asp:ListItem Value="0">Ticket Number</asp:ListItem>
                                    <asp:ListItem Value="1" Selected="True">Last Name</asp:ListItem>
                                    <asp:ListItem Value="2">First Name</asp:ListItem>
                                    <asp:ListItem Value="3">MID Number</asp:ListItem>
                                    <asp:ListItem Value="4">DOB</asp:ListItem>
                                    <asp:ListItem Value="5">Phone No</asp:ListItem>
                                    <asp:ListItem Value="6">Court Date</asp:ListItem>
                                    <asp:ListItem Value="7">Driver License</asp:ListItem>
                                    <asp:ListItem Value="9">LID</asp:ListItem>
                                    <asp:ListItem Value="10">Cause Number</asp:ListItem>
                                    <asp:ListItem Value="11">Hire Date</asp:ListItem>
                                    <asp:ListItem Value="12">Payment Date</asp:ListItem>
                                    <asp:ListItem Value="13">Last Contact Date</asp:ListItem>
                                    <asp:ListItem Value="14">Email address</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td align="left" class="clsLeftPaddingTable" style="width: 12px; height: 26px">
                            </td>
                            <td class="clsLeftPaddingTable" style="height: 26px" align="left">
                                <asp:TextBox ID="txtSearchKeyWord2ad" runat="server" Width="124px" CssClass="clsInputadministration"
                                    TabIndex="5" MaxLength="50" Columns="30" Height="16px"></asp:TextBox>
                            </td>
                            <td align="right" class="clsLeftPaddingTable" style="width: 12px; height: 26px">
                            </td>
                            <td class="clsLeftPaddingTable" align="right" style="height: 26px; width: 115px;">
                                <asp:DropDownList ID="ddlSearchKeyType1ad" runat="server" CssClass="clsInputCombo"
                                    TabIndex="6" Width="124px" Height="19px" onchange="setTextBoxLength(this.value,'txtSearchKeyWord1ad');">
                                    <asp:ListItem Value="-1">&lt; Choose &gt;</asp:ListItem>
                                    <asp:ListItem Value="0">Ticket Number</asp:ListItem>
                                    <asp:ListItem Value="1">Last Name</asp:ListItem>
                                    <asp:ListItem Value="2" Selected="True">First Name</asp:ListItem>
                                    <asp:ListItem Value="3">MID Number</asp:ListItem>
                                    <asp:ListItem Value="4">DOB</asp:ListItem>
                                    <asp:ListItem Value="5">Phone No</asp:ListItem>
                                    <asp:ListItem Value="6">Court Date</asp:ListItem>
                                    <asp:ListItem Value="7">Driver License</asp:ListItem>
                                    <asp:ListItem Value="9">LID</asp:ListItem>
                                    <asp:ListItem Value="10">Cause Number</asp:ListItem>
                                    <asp:ListItem Value="11">Hire Date</asp:ListItem>
                                    <asp:ListItem Value="12">Payment Date</asp:ListItem>
                                    <asp:ListItem Value="13">Last Contact Date</asp:ListItem>
                                    <asp:ListItem Value="14">Email address</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td align="left" class="clsLeftPaddingTable" style="width: 12px; height: 26px">
                            </td>
                            <td class="clsLeftPaddingTable" style="height: 26px" align="left">
                                <asp:TextBox ID="txtSearchKeyWord1ad" runat="server" CssClass="clsInputadministration"
                                    TabIndex="7" Width="124px" MaxLength="50" Columns="30" Height="16px"></asp:TextBox>
                            </td>
                            <td align="right" class="clsLeftPaddingTable" style="width: 12px; height: 26px">
                            </td>
                            <td class="clsLeftPaddingTable" align="right" style="height: 26px;">
                                <asp:DropDownList ID="ddlSearchKeyType3ad" runat="server" CssClass="clsInputCombo"
                                    TabIndex="8" Width="120px" Height="19px" onchange="setTextBoxLength(this.value,'txtSearchKeyWord3ad');">
                                    <asp:ListItem Value="-1">&lt; Choose &gt;</asp:ListItem>
                                    <asp:ListItem Value="0">Ticket Number</asp:ListItem>
                                    <asp:ListItem Value="1">Last Name</asp:ListItem>
                                    <asp:ListItem Value="2">First Name</asp:ListItem>
                                    <asp:ListItem Value="3">MID Number</asp:ListItem>
                                    <asp:ListItem Value="4" Selected="True">DOB</asp:ListItem>
                                    <asp:ListItem Value="5">Phone No</asp:ListItem>
                                    <asp:ListItem Value="6">Court Date</asp:ListItem>
                                    <asp:ListItem Value="7">Driver License</asp:ListItem>
                                    <asp:ListItem Value="9">LID</asp:ListItem>
                                    <asp:ListItem Value="10">Cause Number</asp:ListItem>
                                    <asp:ListItem Value="11">Hire Date</asp:ListItem>
                                    <asp:ListItem Value="12">Payment Date</asp:ListItem>
                                    <asp:ListItem Value="13">Last Contact Date</asp:ListItem>
                                    <asp:ListItem Value="14">Email address</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td align="left" class="clsLeftPaddingTable" style="width: 12px; height: 26px">
                            </td>
                            <td class="clsLeftPaddingTable" style="height: 26px" align="left">
                                <asp:TextBox ID="txtSearchKeyWord3ad" runat="server" CssClass="clsInputadministration"
                                    TabIndex="9" Width="124px" MaxLength="50" Columns="30" Height="16px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="clsLeftPaddingTable" style="height: 26px">
                                <strong>Court Location</strong>
                            </td>
                            <td class="clsLeftPaddingTable" colspan="1" style="width: 12px; height: 26px">
                            </td>
                            <td class="clsLeftPaddingTable" colspan="3" style="height: 26px">
                                <asp:DropDownList ID="ddlCourtsAd" runat="server" CssClass="clsInputCombo" TabIndex="10"
                                    Width="100%">
                                    <asp:ListItem Value="0">&lt; All Courts &gt;</asp:ListItem>
                                    <asp:ListItem Value="-1">&lt; All HMC Courts &gt;</asp:ListItem>
                                    <asp:ListItem Value="-2">&lt; All HCJP Courts &gt;</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td align="left" class="clsLeftPaddingTable" colspan="1" style="width: 12px; height: 26px"
                                valign="middle">
                            </td>
                            <td align="center" class="clsLeftPaddingTable" colspan="3" style="height: 26px" valign="middle">
                                <asp:RadioButton ID="rdbtnClientad" runat="server" CssClass="clsLabelNew" Text="Client"
                                    GroupName="optCriteriaad" Height="16px" TabIndex="11" Style="display: none">
                                </asp:RadioButton>&nbsp;<asp:RadioButton ID="rdbtnQuotead" runat="server" Checked="True"
                                    CssClass="clsLabelNew" Text="Client/Quote" GroupName="optCriteriaad" onClick="AddSearchOption();"
                                    Height="16px" TabIndex="12"></asp:RadioButton>&nbsp;
                                <asp:RadioButton ID="rdbtnNonClientad" runat="server" CssClass="clsLabelNew" Text="Traffic"
                                    GroupName="optCriteriaad" Height="16px" TabIndex="13" onClick="RemoveSearchOption();"
                                    Width="54px"></asp:RadioButton>&nbsp;
                                <asp:RadioButton ID="rdbtnJimsad" runat="server" CssClass="clsLabelNew" Text="Criminal"
                                    GroupName="optCriteriaad" Height="16px" onClick="RemoveSearchOption();" TabIndex="14">
                                </asp:RadioButton>
                                <asp:RadioButton ID="rdbtnCivilad" runat="server" CssClass="clsLabelNew" GroupName="optCriteriaad"
                                    Height="16px" TabIndex="14" onClick="RemoveSearchOption();" Text="Family Law" />
                            </td>
                            <td align="right" class="clsLeftPaddingTable" colspan="2" style="height: 26px">
                                <asp:Button ID="btnSearchAdvance" runat="server" CssClass="clsbutton" OnClick="btnSearchAdvance_Click"
                                    Text="Lookup" TabIndex="15" Width="62px" />
                                <asp:Button ID="btnResetAdvance" runat="server" CssClass="clsbutton" OnClick="btnResetAdvance_Click"
                                    Text="Reset" TabIndex="16" Width="62px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table id="Table5" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
                    <tr>
                        <td width="100%" background="images/separator_repeat.gif" colspan="5" style="height: 11px">
                        </td>
                    </tr>
                </table>
                <asp:Label ID="lblMessage" runat="server" Font-Size="XX-Small" Font-Names="Verdana"
                    ForeColor="Red" Font-Bold="True"></asp:Label><asp:Label ID="Message" runat="server"
                        Font-Size="XX-Small" Font-Names="Verdana" ForeColor="Red" Font-Bold="True"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="clssubhead" width="37%" background="images/subhead_bg.gif" style="height: 34px">
                            &nbsp;<asp:Label ID="lblResults" runat="server" Text="Results" Visible="False"></asp:Label>
                        </td>
                        <td align="center" class="clssubhead" background="images/subhead_bg.gif" style="height: 34px">
                            <table id="tblColorCode" runat="server" visible="false">
                                <tr>
                                    <td>
                                        <asp:Label ID="lbl_greencolor" runat="server" Style="background-color: #A9F5A9; height: 15px;"
                                            BorderColor="Black" BorderWidth="1px" Width="20px"></asp:Label>&nbsp;
                                        <asp:Label ID="lbl_greentext" runat="server" Text="Clients"></asp:Label>&nbsp;&nbsp;
                                        <asp:Label ID="lbl_defaulfcolor" runat="server" Style="background-color: #E6EEF9;
                                            height: 15px;" BackColor="#E6EEF9" Width="20px" BorderColor="Black" BorderWidth="1px"></asp:Label>
                                        &nbsp;
                                        <asp:Label ID="lblellowtext" runat="server" Text="Quotes"></asp:Label>
                                        &nbsp;
                                        <asp:Label ID="lbl_yellowcolor" runat="server" Style="background-color: #F5E5B8;
                                            height: 15px;" Width="20px" BorderColor="Black" BorderWidth="1px"></asp:Label>&nbsp;
                                        <asp:Label ID="lbl_defaulttext" runat="server" Text="Non Clients"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="clssubhead" align="right" width="25%" background="images/subhead_bg.gif"
                            style="height: 34px">
                            <asp:LinkButton ID="lnkbtn_AddNewTicket" runat="server" CssClass="clsbutton" TabIndex="17">Add a New Case</asp:LinkButton>&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td id="grid" valign="top" colspan="2">
                <asp:DataGrid ID="dgResult" runat="server" Width="930px" CssClass="clsLeftPaddingTable"
                    AutoGenerateColumns="False" PageSize="20" BorderColor="DarkGray" BorderStyle="Solid"
                    AllowSorting="True" TabIndex="13" BorderWidth="1px" CellPadding="0">
                    <Columns>
                        <asp:TemplateColumn SortExpression="causenumber" HeaderText="Cause Number">
                            <HeaderStyle Width="12%" CssClass="clsaspcolumnheaderblack" Height="26px"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblcausenumber" runat="server" CssClass="Labelfrmmain" Text='<%# DataBinder.Eval(Container, "DataItem.causenumber") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="CaseNumber" HeaderText="Ticket Number">
                            <HeaderStyle Width="12%" CssClass="clsaspcolumnheaderblack"></HeaderStyle>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkbtnCaseNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CaseNumber") %>'
                                    CommandName="CaseNo">
                                </asp:LinkButton>
                                <asp:HyperLink ID="HLCaseNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CaseNumber") %>'
                                    Visible="False">
                                </asp:HyperLink>
                                <asp:Label ID="lblTicketId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TicketId") %>'
                                    Visible="False">
                                </asp:Label>
                                <asp:Label ID="lblCourtId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CourtId") %>'
                                    Visible="False">
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="FirstName" HeaderText="First Name">
                            <HeaderStyle Width="10%" CssClass="clsaspcolumnheaderblack"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label5" runat="server" CssClass="Labelfrmmain" Text='<%# DataBinder.Eval(Container, "DataItem.Firstname") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="LastName" HeaderText="Last Name">
                            <HeaderStyle Width="10%" CssClass="clsaspcolumnheaderblack"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" CssClass="Labelfrmmain" Text='<%# DataBinder.Eval(Container, "DataItem.Lastname") %>'>
                                </asp:Label>
                                <asp:HiddenField ID="hf_MailerIDFlag" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.MailerIDFlag") %>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="DOB" HeaderText="DOB">
                            <HeaderStyle Width="8%" CssClass="clsaspcolumnheaderblack"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" CssClass="Labelfrmmain" Text='<%# DataBinder.Eval(Container, "DataItem.DOB", "{0:MM/dd/yyyy}") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn HeaderText="Matter" SortExpression="Matter" HeaderStyle-Width="20%"
                            DataField="Matter" HeaderStyle-CssClass="clssubhead" ItemStyle-CssClass="GridItemStyleBig">
                        </asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Court" SortExpression="Court" HeaderStyle-Width="9%"
                            DataField="Court" HeaderStyle-CssClass="clssubhead" ItemStyle-CssClass="GridItemStyleBig">
                        </asp:BoundColumn>
                        <asp:TemplateColumn SortExpression="CourtDate" HeaderText="Court Date">
                            <HeaderStyle Width="9%" CssClass="clsaspcolumnheaderblack"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" CssClass="Labelfrmmain" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDate", "{0:MM/dd/yyyy}") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="Address" HeaderText="Address" Visible="False">
                            <HeaderStyle Width="24%" CssClass="clsaspcolumnheaderblack"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label6" runat="server" CssClass="Labelfrmmain" Text='<%# DataBinder.Eval(Container, "DataItem.address") %>'>
                                </asp:Label>
                                <asp:Label ID="lblAdd1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.address1") %>'
                                    Visible="False">
                                </asp:Label>
                                <asp:Label ID="lblZip" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.zipcode") %>'
                                    Visible="False">
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="CaseStatus" HeaderText="Status">
                            <HeaderStyle Width="5%" CssClass="clsaspcolumnheaderblack"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" CssClass="Labelfrmmain" Text='<%# DataBinder.Eval(Container, "DataItem.CaseStatus") %>'>
                                </asp:Label>
                                <asp:Label ID="lbl_dbid" runat="server" CssClass="Labelfrmmain" Text='<%# DataBinder.Eval(Container, "DataItem.dbid") %>'
                                    Visible="False"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="CaseType" HeaderText="CaseType">
                            <HeaderStyle Width="7%" CssClass="clsaspcolumnheaderblack"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="lbl_casetype" runat="server" CssClass="Labelfrmmain" Text='<%# DataBinder.Eval(Container, "DataItem.CaseType") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False" SortExpression="MIDNo" HeaderText="MID #">
                            <HeaderStyle CssClass="clsaspcolumnheaderblack"></HeaderStyle>
                            <ItemTemplate>
                                <asp:HyperLink ID="HLMidNumber" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MIDNo") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle NextPageText="Next&amp;gt" PrevPageText="&amp;lt;Previous" HorizontalAlign="Center">
                    </PagerStyle>
                </asp:DataGrid>
            </td>
        </tr>
        <tr>
            <td colspan="2" valign="top">
                <asp:Panel ID="pnlCaseCategory" runat="server" Height="50px" Width="350px">
                    <table border="1" cellpadding="0" cellspacing="0" style="border-left-color: navy;
                        border-bottom-color: navy; border-top-color: navy; border-collapse: collapse;
                        border-right-color: navy; width: 100%;">
                        <tr>
                            <td class="clssubhead" style="height: 34px" background="Images/subhead_bg.gif">
                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; height: 100%">
                                    <tr>
                                        <td class="clssubhead" style="width: 50%">
                                            &nbsp;Case Type Info
                                        </td>
                                        <td align="right" style="width: 50%">
                                            <asp:LinkButton ID="lnkbtnclose" runat="server">X</asp:LinkButton>&nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" class="clsLeftPaddingTable" style="width: 100%;
                                    height: 64px">
                                    <tr>
                                        <td class="clssubhead" style="width: 123px; padding-left: 5px;">
                                            Case Type
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlCaseType" runat="server" CssClass="clsInputadministration"
                                                DataTextField="casetypename" DataValueField="casetypeid" Width="150px">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 123px">
                                        </td>
                                        <td>
                                            <asp:Button ID="btnAddCase" runat="server" CssClass="clsbutton" Text="Add" Width="60px"
                                                OnClick="btnAddCase_Click" OnClientClick="return CheckCaseType();" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="MPECaseCategory" runat="server" BackgroundCssClass="modalBackground"
                    CancelControlID="lnkbtnclose" PopupControlID="pnlCaseCategory" TargetControlID="lnkbtn_AddNewTicket">
                </ajaxToolkit:ModalPopupExtender>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table id="Table6" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
                    <tr>
                        <td width="100%" background="images/separator_repeat.gif" colspan="5" height="11">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
            </td>
        </tr>
    </table>
    &nbsp;
    </form>
</body>
</html>

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WebSupergoo.ABCpdf6;
using WebSupergoo.ABCpdf6.Objects;
using WebSupergoo.ABCpdf6.Atoms;
using FrameWorkEnation.Components;
using System.Web.Mail;

namespace lntechNew.QuickEntry
{
    public partial class docketprint : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!Page.IsPostBack)
            {
                //network path
                ViewState["vNTPATHDocketPDF"] = ConfigurationSettings.AppSettings["NTPATHDocketPDF"].ToString();
                ViewState["vNTPATH1"] = ConfigurationSettings.AppSettings["NTPATH1"].ToString();
                //
                string path = string.Empty;
                if (Request.QueryString.Count == 1)
                {
                    if (Request.QueryString["Arr"].ToString() == "50")
                    {
                        path = "http://" + Request.ServerVariables["SERVER_NAME"] + "/quickentry/ArraignmentSnapShot.aspx";
                        GeneratePDF(path);
                        //RenderPDF();
                        SendMail();
                    }
                    else if (Request.QueryString["Arr"].ToString() != "50" & Request.QueryString["Arr"].ToString().ToUpper()!="ALL")
                    {
                        path = "http://" + Request.ServerVariables["SERVER_NAME"] + "/quickentry/ArraignmentSnapShotLongPrint.aspx?criteria=" + Request.QueryString["Arr"].ToString();
                        GeneratePDF(path);
                        //RenderPDF();
                        SendMail();
                    }
                    else if (Request.QueryString["Arr"].ToString().ToUpper() == "ALL")
                    {
                        path = "http://" + Request.ServerVariables["SERVER_NAME"] + "/quickentry/ArraignmentSnapShot.aspx";

                        GeneratePDF(path, 0);
                        path = "http://" + Request.ServerVariables["SERVER_NAME"] + "/quickentry/ArraignmentSnapShotLongPrint.aspx?criteria=2";
                        GeneratePDF(path, 1);
                        path = "http://" + Request.ServerVariables["SERVER_NAME"] + "/quickentry/ArraignmentSnapShotLongPrint.aspx?criteria=3";
                        GeneratePDF(path, 1);
                        path = "http://" + Request.ServerVariables["SERVER_NAME"] + "/quickentry/ArraignmentSnapShotLongPrint.aspx?criteria=4";
                        GeneratePDF(path, 1);
                        path = "http://" + Request.ServerVariables["SERVER_NAME"] + "/quickentry/ArraignmentSnapShotLongPrint.aspx?criteria=5";
                        GeneratePDF(path, 1);
                        path = "http://" + Request.ServerVariables["SERVER_NAME"] + "/quickentry/ArraignmentSnapShotLongPrint.aspx?criteria=6";
                        GeneratePDF(path, 1);
                        path = "http://" + Request.ServerVariables["SERVER_NAME"] + "/quickentry/ArraignmentSnapShotLongPrint.aspx?criteria=99";
                        GeneratePDF(path, 1);

                        Doc theDoc1 = (Doc)Session["vdoc1"];
                        theDoc1.Save(ViewState["vFullPath"].ToString());
                        theDoc1.Clear();
                        Session["vdoc1"] = null;
                        //RenderPDF();
                        SendMail();
                    }
                }
                else if(Request.QueryString.Count>1)
                {
                    if (Request.QueryString["All"].ToString() == "0")
                    {
                        path = "http://" + Request.ServerVariables["SERVER_NAME"] + "/quickentry/PrintTrialDocket.aspx?courtloc=" + Request.QueryString["courtloc"].ToString() + "&courtdate=" + Request.QueryString["courtdate"].ToString() + "&page=" + "1" + "&courtnumber=" + Request.QueryString["courtnumber"].ToString() + "&datetype=" + Request.QueryString["datetype"].ToString() + "&RecdType=" + 1;
                        GeneratePDF(path);
                        //RenderPDF();
                        SendMail();
                    }
                    else if (Request.QueryString["All"].ToString() == "1")
                    {
                        path = "http://" + Request.ServerVariables["SERVER_NAME"] + "/quickentry/PrintTrialDocket.aspx?courtloc=None" + "&courtdate=" + Request.QueryString["courtdate"].ToString() + "&page=" + "1" + "&courtnumber=" + Request.QueryString["courtnumber"].ToString() + "&datetype=" + Request.QueryString["datetype"].ToString() + "&RecdType=" + 1;
                        GeneratePDF(path, 0);

                        string[] key = { "@courtloc", "@courtdate", "@page", "@courtnumber", "@datetype" };
                        object[] value1 = { "None", Request.QueryString["courtdate"].ToString(), "1", Request.QueryString["courtnumber"].ToString(), Request.QueryString["datetype"].ToString() };
                        clsENationWebComponents ClsDb = new clsENationWebComponents();
                        DataSet Ds_FillTable = ClsDb.Get_DS_BySPArr("usp_hts_get_trial_docket_report_new", key, value1);
                        if (Ds_FillTable.Tables[2].Rows.Count > 0)
                        {
                            for (int i = 0; i < Ds_FillTable.Tables[2].Rows.Count; i++)
                            {
                                path = "http://" + Request.ServerVariables["SERVER_NAME"] + "/quickentry/PrintTrialDocket.aspx?courtloc=" + Ds_FillTable.Tables[2].Rows[i]["currentcourtloc"].ToString() + "&courtdate=" + Request.QueryString["courtdate"].ToString() + "&page=" + "1" + "&courtnumber=" + Request.QueryString["courtnumber"].ToString() + "&datetype=" + Request.QueryString["datetype"].ToString() + "&RecdType=" + 1;
                                GeneratePDF(path, 1);
                            }
                        }
                        Doc theDoc1 = (Doc)Session["vdoc1"];
                        theDoc1.Save(ViewState["vFullPath"].ToString());
                        theDoc1.Clear();
                        Session["vdoc1"] = null;
                        //RenderPDF();
                        SendMail();
                    }
                }                
            }
        }
    
        private void GeneratePDF(string path)
        {
            Doc theDoc = new Doc();
            theDoc.Rect.Inset(35, 45);
            theDoc.Page = theDoc.AddPage();
            //theDoc.HtmlOptions.BrowserWidth = 800;
            int theID;
            theID = theDoc.AddImageUrl(path);
            while (true)
            {
                theDoc.FrameRect(); // add a black border
                if (!theDoc.Chainable(theID))
                    break;
                theDoc.Page = theDoc.AddPage();
                theID = theDoc.AddImageToChain(theID);
            }
            for (int i = 1; i <= theDoc.PageCount; i++)
            {
                theDoc.PageNumber = i;
                theDoc.Flatten();
            }
            string name = Session.SessionID + ".pdf";
            //for website
            ViewState["vFullPath"] = ViewState["vNTPATHDocketPDF"].ToString() + name;
            ViewState["vPath"] = ViewState["vNTPATHDocketPDF"].ToString();
            //website end
            string[] files = System.IO.Directory.GetFiles(ViewState["vPath"].ToString(), name);
            for (int i = 0; i < files.Length; i++)
            {
                System.IO.File.Delete(files[i]);
            }
            theDoc.Save(ViewState["vFullPath"].ToString());
            theDoc.Clear();
        }
        private void RenderPDF()
        {
            //for website
            string vpth = ViewState["vNTPATHDocketPDF"].ToString().Substring((ViewState["vNTPATH1"].ToString().Length) - 1);
            string link = "../DOCSTORAGE" + vpth;
            link = link + Session.SessionID + ".pdf";
            //website end
            Response.Redirect(link, false);
            string createdate = String.Empty;
            string currDate = DateTime.Today.ToShortDateString();
            DateTime CurrDate = Convert.ToDateTime(currDate);
            DateTime fileDate = new DateTime();
            string[] files = System.IO.Directory.GetFiles(ViewState["vPath"].ToString(), "*.pdf");
            for (int i = 0; i < files.Length; i++)
            {
                createdate = System.IO.File.GetCreationTime(files[i]).ToShortDateString();
                fileDate = Convert.ToDateTime(createdate);
                if (fileDate.CompareTo(CurrDate) < 0)
                {
                    System.IO.File.Delete(files[i]);
                }
            }

        }

        private void GeneratePDF(string path, int seq)
        {
            if (seq == 0)
            {
                Doc theDoc1 = new Doc();
                theDoc1.Rect.Inset(35, 45);
                theDoc1.Page = theDoc1.AddPage();
                //theDoc1.HtmlOptions.BrowserWidth = 800;
                int theID;
                theID = theDoc1.AddImageUrl(path);
                while (true)
                {
                    theDoc1.FrameRect(); // add a black border
                    if (!theDoc1.Chainable(theID))
                        break;
                    theDoc1.Page = theDoc1.AddPage();
                    theID = theDoc1.AddImageToChain(theID);
                }
                for (int i = 1; i <= theDoc1.PageCount; i++)
                {
                    theDoc1.PageNumber = i;
                    theDoc1.Flatten();
                }
                string name = Session.SessionID + ".pdf";
                //for website
                ViewState["vFullPath"] = ViewState["vNTPATHDocketPDF"].ToString() + name;
                ViewState["vPath"] = ViewState["vNTPATHDocketPDF"].ToString();
                //website end
                //theDoc1.Save(ViewState["vFullPath"].ToString());
                Session["vdoc1"] = theDoc1;
                //theDoc1.Clear();
            }
            if (seq > 0)
            {
                Doc theDoc = new Doc();
                theDoc.Rect.Inset(35, 45);
                theDoc.Page = theDoc.AddPage();
                //theDoc.HtmlOptions.BrowserWidth = 800;
                int theID;
                theID = theDoc.AddImageUrl(path);
                while (true)
                {
                    theDoc.FrameRect(); // add a black border
                    if (!theDoc.Chainable(theID))
                        break;
                    theDoc.Page = theDoc.AddPage();
                    theID = theDoc.AddImageToChain(theID);
                }
                for (int i = 1; i <= theDoc.PageCount; i++)
                {
                    theDoc.PageNumber = i;
                    theDoc.Flatten();
                }
                Doc theDoc1 = (Doc)Session["vdoc1"];
                theDoc1.Append(theDoc);
                Session["vdoc1"] = theDoc1;
                theDoc.Clear();
            }


            //theDoc1.Clear();
        }

        public void SendMail()
        {

            try
            {
                string vpth = ViewState["vNTPATHDocketPDF"].ToString();
                string link = vpth + Session.SessionID + ".pdf";
                
                string fromemail = ConfigurationSettings.AppSettings["FromEmail"].ToString();
                string useremail = ConfigurationSettings.AppSettings["ToEmail"].ToString();
                string message = "";


                string smtpServer = ConfigurationSettings.AppSettings["SMTPServer"].ToString();

                MailMessage msg = new MailMessage();

                msg.From = fromemail;
                msg.To = useremail;
                msg.Subject = "Report";
                MailAttachment mAtach=new MailAttachment(link);
                msg.Attachments.Add(mAtach);
                msg.Body = message;
                msg.BodyFormat = System.Web.Mail.MailFormat.Html;
                SmtpMail.SmtpServer = smtpServer;
                SmtpMail.Send(msg);
                Response.Write("Email sent to:" + useremail);
            }
            catch (Exception)
            {
                Response.Write("Error occured! Email Not Sent.");
            }
        }
    }
}

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ArrCombined.aspx.cs" Inherits="lntechNew.QuickEntry.ArrCombined" %>

<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Scheduled Arraignments</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript">
		function Hide()
		{
			if( document.getElementById("ddl_WeekDay").value=="-1")
			{
				document.getElementById("jt").style.display='none';
				document.getElementById("jtgrd").style.display='none';
				document.getElementById("ost").style.display='none';
				document.getElementById("ostgrd").style.display='none';
				document.getElementById("uaagrd").style.display='none';
				document.getElementById("a1").style.display='none';
				document.getElementById("a2").style.display='none';
				document.getElementById("a3").style.display='none';				
			}
			else if(document.getElementById("ddl_WeekDay").value==100)
			{
				document.getElementById("jt").style.display='block';
				document.getElementById("jtgrd").style.display='block';
				document.getElementById("ost").style.display='none';
				document.getElementById("ostgrd").style.display='none';
				document.getElementById("uaagrd").style.display='none';
				document.getElementById("a1").style.display='none';
				document.getElementById("a2").style.display='none';
				document.getElementById("a3").style.display='none';
			}
			else if(document.getElementById("ddl_WeekDay").value!=99)
			{
				document.getElementById("jt").style.display='none';
				document.getElementById("jtgrd").style.display='none';
				document.getElementById("ost").style.display='none';
				document.getElementById("ostgrd").style.display='none';
				document.getElementById("uaagrd").style.display='block';
				document.getElementById("a1").style.display='none';
				document.getElementById("a2").style.display='none';
				document.getElementById("a3").style.display='none';
			}
			else
			{
				document.getElementById("jt").style.display='block';
				document.getElementById("jtgrd").style.display='block';
				document.getElementById("ost").style.display='block';
				document.getElementById("ostgrd").style.display='block';
				document.getElementById("uaagrd").style.display='block';
				document.getElementById("a1").style.display='block';
				document.getElementById("a2").style.display='block';
				document.getElementById("a3").style.display='block';				
			}
		}
		</script>

        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
	</HEAD>
	<body onload="javascript: Hide();" MS_POSITIONING="GridLayout">		
			<FORM id="Form2" method="post" runat="server">
				<table cellpadding=0 cellspacing=0 border=0 align="center">
				    <tr>
				        <td>
				    
				    <TABLE id="TableMain" cellSpacing="0" cellPadding="0" align=center width="780" border="0">
					    <tr>
						    <td >
						    <table align=center  border=0> <tr><td><uc1:activemenu id="ActiveMenu1" runat="server"></uc1:activemenu> </td></tr></table>
							    
						    </td>
					    </tr>
					    <tr>
						    <td height="9">
						    </td>
					    </tr>
					    <TR>
						    <TD><asp:dropdownlist id="ddl_WeekDay" runat="server" AutoPostBack="True" CssClass="clsinputcombo" OnSelectedIndexChanged="ddl_WeekDay_SelectedIndexChanged1">
								    <asp:ListItem Value="-1">Select a Day</asp:ListItem>
                                <asp:ListItem Value="50">Arraignment(SnapShot)</asp:ListItem>
								    <asp:ListItem Value="2">Monday</asp:ListItem>
								    <asp:ListItem Value="3">Tuesday</asp:ListItem>
								    <asp:ListItem Value="4">Wednesday</asp:ListItem>
								    <asp:ListItem Value="5">Thursday</asp:ListItem>
								    <asp:ListItem Value="6">Friday</asp:ListItem>
                                <asp:ListItem Value="100">Judge Trial</asp:ListItem>
								    <asp:ListItem Value="99">Not Assigned</asp:ListItem>
							    </asp:dropdownlist>&nbsp;&nbsp;&nbsp;&nbsp;
							    <asp:label id="lbl_CurrDateTime" runat="server" Font-Names="Times New Roman" Font-Bold="True"></asp:label></TD>
					    </TR>
					    <TR>
						    <TD style="HEIGHT: 9px"></TD>
					    </TR>
					    <TR>
						    <TD><asp:label id="lbl_Message" runat="server" CssClass="Label" Font-Bold="True" ForeColor="Green"
								    Visible="False">There is currently no clients arraigned for arraignments</asp:label></TD>
					    </TR>
					    <TR >
						    <TD style="HEIGHT: 9px" width="100%" background="../../images/separator_repeat.gif" colSpan="5"
							    height="9">
						    </TD>
					    </TR>
					    <TR>
						    <TD align="right">
							    <asp:imagebutton id="Imagebutton1" runat="server" ImageUrl="../Images/print_02.jpg" Visible="False" OnClick="Imagebutton1_Click" ToolTip="Print" ></asp:imagebutton>&nbsp;<asp:ImageButton ID="img_pdfShort" runat="server" ImageUrl="../Images/pdf.jpg"
                                    Visible="False" OnClick="img_pdf_Click" ToolTip="Print Single" />
                                <asp:ImageButton ID="imgPrint_Ds_to_Printer" runat="server" ImageUrl="../Images/print_02.jpg"
                                    OnClick="imgPrint_Ds_to_Printer_Click1" Visible="False" ToolTip="Print" />&nbsp;<asp:ImageButton
                                        ID="img_pdfLong" runat="server" ImageUrl="../Images/pdf.jpg" 
                                        Visible="False" OnClick="ImageButton2_Click" ToolTip="Print Single" />
                                <asp:ImageButton ID="img_PdfAll" runat="server" ImageUrl="../Images/pdfall.jpg" OnClick="img_PdfAll_Click" ToolTip="Print All" Visible="False" />&nbsp;
						    </TD>
					    </TR>
					    <TR >
						    <TD style="HEIGHT: 9px" width="100%" background="../../images/separator_repeat.gif" colSpan="5"
							    height="9">
						    </TD>
					    </TR>
					    <TR>
						    <TD id=longshot runat=server style="display:none">
							    <TABLE id="TableGrid" cellSpacing="0" cellPadding="0" width="100%" bgColor="white" border="0">
								    <tr>
									    <td style="HEIGHT: 16px"></td>
								    </tr>
								    <TR>
									    <TD id="uaagrd" style="HEIGHT: 9px" width="100%" colSpan="5" height="9"><asp:datagrid id="dg_Report" runat="server" EnableViewState="False" AutoGenerateColumns="False"
											    BorderColor="Black" BorderStyle="Solid" Width="100%">
											    <ItemStyle Font-Names="Times New Roman"></ItemStyle>
											    <HeaderStyle ForeColor="Black" BackColor="#FFCC00"></HeaderStyle>
											    <Columns>
												    <asp:TemplateColumn HeaderText="No:">
													    <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													    <ItemStyle Font-Names="Times New Roman"></ItemStyle>
													    <ItemTemplate>
														    <asp:Label id="lbl_sno" runat="server">0</asp:Label>
														    <asp:Label id="lbl_Ticketid" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.TicketID_PK") %>'>
														    </asp:Label>
													    </ItemTemplate>
												    </asp:TemplateColumn>
												    <asp:TemplateColumn HeaderText="Flags">
													    <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													    <ItemStyle Font-Names="Times New Roman"></ItemStyle>
													    <ItemTemplate>
														    <asp:Label id="Label14" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BondFlag") %>' >
														    </asp:Label>
													    </ItemTemplate>
												    </asp:TemplateColumn>
												    <asp:TemplateColumn HeaderText="Case #">
													    <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													    <ItemStyle Font-Names="Times New Roman" Font-Bold="True"></ItemStyle>
													    <ItemTemplate>
														    <asp:HyperLink id=hlnkCaseNo runat="server" Font-Bold="True" NavigateUrl='<%# String.Concat("../clientinfo/ViolationFeeOld.aspx?caseNumber=",DataBinder.Eval(Container, "DataItem.TicketID_PK"),"&amp;search=0") %>' Target="_self" Text='<%# DataBinder.Eval(Container.DataItem, "TicketNumber_PK") %>' Font-Underline="True">
														    </asp:HyperLink>
													    </ItemTemplate>
												    </asp:TemplateColumn>
												    <asp:TemplateColumn HeaderText="Seq">
													    <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													    <ItemStyle Font-Names="Times New Roman"></ItemStyle>
													    <ItemTemplate>
														    <asp:Label id=Label15 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Violations") %>'>
														    </asp:Label>&nbsp;
													    </ItemTemplate>
												    </asp:TemplateColumn>
												    <asp:TemplateColumn HeaderText="Last Name">
													    <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													    <ItemStyle Font-Names="Times New Roman"></ItemStyle>
													    <ItemTemplate>
														    <asp:Label id="Label16" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Lastname") %>' >
														    </asp:Label>&nbsp;
													    </ItemTemplate>
												    </asp:TemplateColumn>
												    <asp:TemplateColumn HeaderText="First Name">
													    <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													    <ItemStyle Font-Names="Times New Roman"></ItemStyle>
													    <ItemTemplate>
														    <asp:Label id="Label17" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Firstname") %>' >
														    </asp:Label>&nbsp;
													    </ItemTemplate>
												    </asp:TemplateColumn>
												    <asp:TemplateColumn HeaderText="DL">
													    <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													    <ItemStyle Font-Names="Times New Roman"></ItemStyle>
													    <ItemTemplate>
														    <asp:Label id="Label18" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DLNumber") %>' >
														    </asp:Label>&nbsp;
													    </ItemTemplate>
												    </asp:TemplateColumn>
												    <asp:TemplateColumn HeaderText="DOB">
													    <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													    <ItemStyle Font-Names="Times New Roman"></ItemStyle>
													    <ItemTemplate>
														    <asp:Label id=lbl_dob runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DOB") %>'>
														    </asp:Label>
													    </ItemTemplate>
												    </asp:TemplateColumn>
												    <asp:TemplateColumn HeaderText="MID">
													    <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													    <ItemStyle Font-Names="Times New Roman"></ItemStyle>
													    <ItemTemplate>
														    <asp:Label id="Label20" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MID") %>' >
														    </asp:Label>
													    </ItemTemplate>
												    </asp:TemplateColumn>
												    <asp:TemplateColumn HeaderText="Set Date">
													    <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													    <ItemStyle Font-Names="Times New Roman"></ItemStyle>
													    <ItemTemplate>
														    <asp:Label id="lbl_arrdate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TrialDateTime") %>' >
														    </asp:Label>
													    </ItemTemplate>
												    </asp:TemplateColumn>
											    </Columns>
										    </asp:datagrid></TD>
								    </TR>
								    <tr>
									    <td style="HEIGHT: 16px"></td>
								    </tr>
								    <tr>
									    <td id="jt" style="HEIGHT: 14px"><STRONG>Judge Trials</STRONG></td>
								    </tr>
								    <tr>
									    <td id="a1" style="HEIGHT: 14px"></td>
								    </tr>
								    <TR>
									    <TD id="jtgrd" style="HEIGHT: 9px" width="100%" colSpan="5" height="9"><asp:datagrid id="dg_ReportJT" runat="server" EnableViewState="False" AutoGenerateColumns="False"
											    BorderColor="Black" BorderStyle="Solid" Width="100%">
											    <ItemStyle Font-Names="Times New Roman"></ItemStyle>
											    <HeaderStyle ForeColor="Black" BackColor="#FFCC00"></HeaderStyle>
											    <Columns>
												    <asp:TemplateColumn HeaderText="No:">
													    <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													    <ItemStyle Font-Names="Times New Roman"></ItemStyle>
													    <ItemTemplate>
														    <asp:Label id="lbl_sno1" runat="server">0</asp:Label>
														    <asp:Label id=lbl_Ticketid1 runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.TicketID_PK") %>'>
														    </asp:Label>
													    </ItemTemplate>
												    </asp:TemplateColumn>
												    <asp:TemplateColumn HeaderText="Flags">
													    <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													    <ItemStyle Font-Names="Times New Roman"></ItemStyle>
													    <ItemTemplate>
														    <asp:Label id="Label141" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BondFlag") %>' >
														    </asp:Label>
													    </ItemTemplate>
												    </asp:TemplateColumn>
												    <asp:TemplateColumn HeaderText="Case #">
													    <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													    <ItemStyle Font-Names="Times New Roman" Font-Bold="True"></ItemStyle>
													    <ItemTemplate>
														    <asp:HyperLink id=hlnkCaseNo1 runat="server" NavigateUrl='<%# String.Concat("../clientinfo/ViolationFeeOld.aspx?caseNumber=",DataBinder.Eval(Container, "DataItem.TicketID_PK"),"&amp;search=0") %>' Target="_self" Text='<%# DataBinder.Eval(Container.DataItem, "TicketNumber_PK") %>' Font-Underline="True">
														    </asp:HyperLink>
													    </ItemTemplate>
												    </asp:TemplateColumn>
												    <asp:TemplateColumn HeaderText="Seq">
													    <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													    <ItemStyle Font-Names="Times New Roman"></ItemStyle>
													    <ItemTemplate>
														    <asp:Label id=Label151 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Violations") %>'>
														    </asp:Label>&nbsp;
													    </ItemTemplate>
												    </asp:TemplateColumn>
												    <asp:TemplateColumn HeaderText="Last Name">
													    <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													    <ItemStyle Font-Names="Times New Roman"></ItemStyle>
													    <ItemTemplate>
														    <asp:Label id="Label161" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Lastname") %>' >
														    </asp:Label>&nbsp;
													    </ItemTemplate>
												    </asp:TemplateColumn>
												    <asp:TemplateColumn HeaderText="First Name">
													    <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													    <ItemStyle Font-Names="Times New Roman"></ItemStyle>
													    <ItemTemplate>
														    <asp:Label id="Label171" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Firstname") %>' >
														    </asp:Label>&nbsp;
													    </ItemTemplate>
												    </asp:TemplateColumn>
												    <asp:TemplateColumn HeaderText="DL">
													    <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													    <ItemStyle Font-Names="Times New Roman"></ItemStyle>
													    <ItemTemplate>
														    <asp:Label id="Label181" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DLNumber") %>' >
														    </asp:Label>&nbsp;
													    </ItemTemplate>
												    </asp:TemplateColumn>
												    <asp:TemplateColumn HeaderText="DOB">
													    <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													    <ItemStyle Font-Names="Times New Roman"></ItemStyle>
													    <ItemTemplate>
														    <asp:Label id="lbl_dob" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DOB") %>' >
														    </asp:Label>
													    </ItemTemplate>
												    </asp:TemplateColumn>
												    <asp:TemplateColumn HeaderText="Court Information">
													    <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													    <ItemStyle Font-Names="Times New Roman"></ItemStyle>
													    <ItemTemplate>
														    <asp:Label id=lbl_CCDate runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CurrentDateSet") %>'>
														    </asp:Label>
														    <asp:Label id=lbl_CCNum runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.CurrentCourtNum") %>'>
														    </asp:Label>
													    </ItemTemplate>
												    </asp:TemplateColumn>
												    <asp:TemplateColumn HeaderText="Officer Day">
													    <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													    <ItemStyle Font-Names="Times New Roman"></ItemStyle>
													    <ItemTemplate>
														    <asp:Label id=lbl_OfficerDay runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.OfficerDay") %>'>
														    </asp:Label>
													    </ItemTemplate>
												    </asp:TemplateColumn>
											    </Columns>
										    </asp:datagrid></TD>
								    </TR>
								    <tr>
									    <td id="a2" style="HEIGHT: 16px"></td>
								    </tr>
								    <tr>
									    <td id="ost" style="HEIGHT: 16px"><STRONG>Outside Trials</STRONG></td>
								    </tr>
								    <tr>
									    <td id="a3" style="HEIGHT: 16px"></td>
								    </tr>
								    <TR>
									    <TD id="ostgrd" style="HEIGHT: 9px" width="100%" colSpan="5" height="9"><asp:datagrid id="dg_ReportOT" runat="server" EnableViewState="False" AutoGenerateColumns="False"
											    BorderColor="Black" BorderStyle="Solid" Width="100%">
											    <ItemStyle Font-Names="Times New Roman"></ItemStyle>
											    <HeaderStyle ForeColor="Black" BackColor="#FFCC00"></HeaderStyle>
											    <Columns>
												    <asp:TemplateColumn HeaderText="No:">
													    <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													    <ItemStyle Font-Names="Times New Roman"></ItemStyle>
													    <ItemTemplate>
														    <asp:Label id="lbl_sno2" runat="server">0</asp:Label>
														    <asp:Label id=lbl_Ticketid2 runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.TicketID_PK") %>'>
														    </asp:Label>
													    </ItemTemplate>
												    </asp:TemplateColumn>
												    <asp:TemplateColumn HeaderText="Flags">
													    <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													    <ItemStyle Font-Names="Times New Roman"></ItemStyle>
													    <ItemTemplate>
														    <asp:Label id=Label142 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BondFlag") %>'>
														    </asp:Label>
													    </ItemTemplate>
												    </asp:TemplateColumn>
												    <asp:TemplateColumn HeaderText="Case #">
													    <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													    <ItemStyle Font-Names="Times New Roman" Font-Bold="True"></ItemStyle>
													    <ItemTemplate>
														    <asp:HyperLink id=hlnkCaseNo2 runat="server" NavigateUrl='<%# String.Concat("../clientinfo/ViolationFeeOld.aspx?caseNumber=",DataBinder.Eval(Container, "DataItem.TicketID_PK"),"&amp;search=0") %>' Target="_self" Text='<%# DataBinder.Eval(Container.DataItem, "TicketNumber_PK") %>' Font-Underline="True">
														    </asp:HyperLink>
													    </ItemTemplate>
												    </asp:TemplateColumn>
												    <asp:TemplateColumn HeaderText="Seq">
													    <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													    <ItemStyle Font-Names="Times New Roman"></ItemStyle>
													    <ItemTemplate>
														    <asp:Label id=Label152 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Violations") %>'>
														    </asp:Label>&nbsp;
													    </ItemTemplate>
												    </asp:TemplateColumn>
												    <asp:TemplateColumn HeaderText="Last Name">
													    <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													    <ItemStyle Font-Names="Times New Roman"></ItemStyle>
													    <ItemTemplate>
														    <asp:Label id="Label162" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Lastname") %>' >
														    </asp:Label>&nbsp;
													    </ItemTemplate>
												    </asp:TemplateColumn>
												    <asp:TemplateColumn HeaderText="First Name">
													    <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													    <ItemStyle Font-Names="Times New Roman"></ItemStyle>
													    <ItemTemplate>
														    <asp:Label id="Label172" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Firstname") %>' >
														    </asp:Label>&nbsp;
													    </ItemTemplate>
												    </asp:TemplateColumn>
												    <asp:TemplateColumn HeaderText="DL">
													    <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													    <ItemStyle Font-Names="Times New Roman"></ItemStyle>
													    <ItemTemplate>
														    <asp:Label id="Label182" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DLNumber") %>' >
														    </asp:Label>&nbsp;
													    </ItemTemplate>
												    </asp:TemplateColumn>
												    <asp:TemplateColumn HeaderText="DOB">
													    <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													    <ItemStyle Font-Names="Times New Roman"></ItemStyle>
													    <ItemTemplate>
														    <asp:Label id="lbl_dob" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DOB") %>' >
														    </asp:Label>
													    </ItemTemplate>
												    </asp:TemplateColumn>
												    <asp:TemplateColumn HeaderText="Court Information">
													    <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													    <ItemStyle Font-Names="Times New Roman"></ItemStyle>
													    <ItemTemplate>
														    <asp:Label id=lbl_CCDate runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CurrentDateSet") %>'>
														    </asp:Label>
														    <asp:Label id=lbl_CCNum runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.CurrentCourtNum") %>'>
														    </asp:Label>
													    </ItemTemplate>
												    </asp:TemplateColumn>
											    </Columns>
										    </asp:datagrid></TD>
								    </TR>
							    </TABLE>
						    </TD>
					    </TR>
				    </table>
				    
				        </td>
				    </tr>
				    
				    <tr>
				    <td align=left style="display:none" id=snapshot runat=server >
				    
				    <TABLE id="Table1" cellSpacing="0" cellPadding="0" width="950" align="left" border="0">
				    <tr>
					    <td style="HEIGHT: 21px"><STRONG><FONT size="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MONDAY</FONT></STRONG>
					    </td>
					    <td style="HEIGHT: 21px"><STRONG><FONT size="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TUESDAY</FONT></STRONG>
					    </td>
					    <td><STRONG><FONT size="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WEDNESDAY</FONT></STRONG>
					    </td>
					    <td><STRONG><FONT size="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;THURSDAY</FONT></STRONG>
					    </td>
					    <td><STRONG><FONT size="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FRIDAY</FONT></STRONG>
					    </td>
				    </tr>
				    <tr>
					    <td vAlign="top" align="left" width="20%"><asp:datagrid id="dg_Mon" runat="server" AutoGenerateColumns="False" CellPadding="0" BorderStyle="Solid"
							    BorderColor="Black" Width="80%" EnableViewState="False">
							    <Columns>
								    <asp:TemplateColumn HeaderText="No">
									    <HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
									    <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									    <ItemTemplate>
										    <asp:Label id=lblNoM runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SEQ_CustNumber") %>'>
										    </asp:Label>
										    <asp:Label id=lblTicketidM runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MON_TicketID_PK") %>' Visible="False">
										    </asp:Label>
									    </ItemTemplate>
								    </asp:TemplateColumn>
								    <asp:TemplateColumn HeaderText="Com">
									    <HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
									    <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									    <ItemTemplate>
										    <asp:Label id=lblTrialM runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MON_TrialDateTime") %>'>
										    </asp:Label>
										    <asp:Label id=lblBondM runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MON_BondFlag") %>'>
										    </asp:Label>
										    <asp:Label id=lblTrialCM runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MON_TrialComments") %>'>
										    </asp:Label>
										    <asp:Label id="lblPlusM" runat="server"></asp:Label>
										    <asp:Label id=lblPreM runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MON_pretrialstatus") %>'>
										    </asp:Label>
									    </ItemTemplate>
								    </asp:TemplateColumn>
								    <asp:TemplateColumn HeaderText="Arr">
									    <HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
									    <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman" HorizontalAlign="Center"></ItemStyle>
									    <ItemTemplate>
										    <asp:Label id=lblArrM runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MON_ViolationDate") %>'>
										    </asp:Label>
									    </ItemTemplate>
								    </asp:TemplateColumn>
								    <asp:TemplateColumn HeaderText="Name">
									    <HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
									    <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									    <ItemTemplate>
										    <asp:HyperLink id=hlnkNameM runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MON_Name") %>' Font-Underline="True" NavigateUrl='<%# String.Concat("../clientinfo/ViolationFeeOld.aspx?caseNumber=",DataBinder.Eval(Container, "DataItem.MON_TicketID_PK"),"&amp;search=0") %>'>
										    </asp:HyperLink>
									    </ItemTemplate>
								    </asp:TemplateColumn>
							    </Columns>
						    </asp:datagrid></td>
					    <td vAlign="top" width="20%"><asp:datagrid id="dg_Tue" runat="server" AutoGenerateColumns="False" BorderStyle="Solid" BorderColor="Black"
							    Width="80%" EnableViewState="False">
							    <Columns>
								    <asp:TemplateColumn HeaderText="No">
									    <HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
									    <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									    <ItemTemplate>
										    <asp:Label id=lblNoT runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SEQ_CustNumber") %>'>
										    </asp:Label>
										    <asp:Label id=lblTicketidT runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TUE_TicketID_PK") %>' Visible="False">
										    </asp:Label>
									    </ItemTemplate>
								    </asp:TemplateColumn>
								    <asp:TemplateColumn HeaderText="Com">
									    <HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
									    <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									    <ItemTemplate>
										    <asp:Label id=lblTrialT runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TUE_TrialDateTime") %>'>
										    </asp:Label>
										    <asp:Label id=lblBondT runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TUE_BondFlag") %>'>
										    </asp:Label>
										    <asp:Label id=lblTrialCT runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TUE_TrialComments") %>'>
										    </asp:Label>
										    <asp:Label id="lblPlusT" runat="server"></asp:Label>
										    <asp:Label id=lblPreT runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TUE_pretrialstatus") %>'>
										    </asp:Label>
									    </ItemTemplate>
								    </asp:TemplateColumn>
								    <asp:TemplateColumn HeaderText="Arr">
									    <HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
									    <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman" HorizontalAlign="Center"></ItemStyle>
									    <ItemTemplate>
										    <asp:Label id="lblArrT" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TUE_ViolationDate") %>'>
										    </asp:Label>
									    </ItemTemplate>
								    </asp:TemplateColumn>
								    <asp:TemplateColumn HeaderText="Name">
									    <HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
									    <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									    <ItemTemplate>
										    <asp:HyperLink id=hlnkNameT runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TUE_Name") %>' Font-Underline="True" NavigateUrl='<%# String.Concat("../clientinfo/ViolationFeeOld.aspx?caseNumber=",DataBinder.Eval(Container, "DataItem.TUE_TicketID_PK"),"&amp;search=0") %>'>
										    </asp:HyperLink>
									    </ItemTemplate>
								    </asp:TemplateColumn>
							    </Columns>
						    </asp:datagrid></td>
					    <td vAlign="top" width="20%"><asp:datagrid id="dg_Wed" runat="server" AutoGenerateColumns="False" BorderStyle="Solid" BorderColor="Black"
							    Width="80%" EnableViewState="False">
							    <Columns>
								    <asp:TemplateColumn HeaderText="No">
									    <HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
									    <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									    <ItemTemplate>
										    <asp:Label id=lblNoW runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SEQ_CustNumber") %>'>
										    </asp:Label>
										    <asp:Label id=lblTicketidW runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.WED_TicketID_PK") %>' Visible="False">
										    </asp:Label>
									    </ItemTemplate>
								    </asp:TemplateColumn>
								    <asp:TemplateColumn HeaderText="Com">
									    <HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
									    <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									    <ItemTemplate>
										    <asp:Label id=lblTrialW runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.WED_TrialDateTime") %>'>
										    </asp:Label>
										    <asp:Label id=lblBondW runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.WED_BondFlag") %>'>
										    </asp:Label>
										    <asp:Label id=lblTrialCW runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.WED_TrialComments") %>'>
										    </asp:Label>
										    <asp:Label id="lblPlusW" runat="server"></asp:Label>
										    <asp:Label id=lblPreW runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.WED_pretrialstatus") %>'>
										    </asp:Label>
									    </ItemTemplate>
								    </asp:TemplateColumn>
								    <asp:TemplateColumn HeaderText="Arr">
									    <HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
									    <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman" HorizontalAlign="Center"></ItemStyle>
									    <ItemTemplate>
										    <asp:Label id="lblArrW" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.WED_ViolationDate") %>'>
										    </asp:Label>
									    </ItemTemplate>
								    </asp:TemplateColumn>
								    <asp:TemplateColumn HeaderText="Name">
									    <HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
									    <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									    <ItemTemplate>
										    <asp:HyperLink id=hlnkNameW runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.WED_Name") %>' Font-Underline="True" NavigateUrl='<%# String.Concat("../clientinfo/ViolationFeeOld.aspx?caseNumber=",DataBinder.Eval(Container, "DataItem.WED_TicketID_PK"),"&amp;search=0") %>'>
										    </asp:HyperLink>
									    </ItemTemplate>
								    </asp:TemplateColumn>
							    </Columns>
						    </asp:datagrid></td>
					    <td vAlign="top" width="20%"><asp:datagrid id="dg_Thu" runat="server" AutoGenerateColumns="False" BorderStyle="Solid" BorderColor="Black"
							    Width="80%" EnableViewState="False">
							    <Columns>
								    <asp:TemplateColumn HeaderText="No">
									    <HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
									    <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									    <ItemTemplate>
										    <asp:Label id=lblNoTh runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SEQ_CustNumber") %>'>
										    </asp:Label>
										    <asp:Label id=lblTicketidTh runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.THR_TicketID_PK") %>' Visible="False">
										    </asp:Label>
									    </ItemTemplate>
								    </asp:TemplateColumn>
								    <asp:TemplateColumn HeaderText="Com">
									    <HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
									    <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									    <ItemTemplate>
										    <asp:Label id=lblTrialTh runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.THR_TrialDateTime") %>'>
										    </asp:Label>
										    <asp:Label id=lblBondTh runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.THR_BondFlag") %>'>
										    </asp:Label>
										    <asp:Label id=lblTrialCTh runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.THR_TrialComments") %>'>
										    </asp:Label>
										    <asp:Label id="lblPlusTh" runat="server"></asp:Label>
										    <asp:Label id=lblPreTh runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.THR_pretrialstatus") %>'>
										    </asp:Label>
									    </ItemTemplate>
								    </asp:TemplateColumn>
								    <asp:TemplateColumn HeaderText="Arr">
									    <HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
									    <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman" HorizontalAlign="Center"></ItemStyle>
									    <ItemTemplate>
										    <asp:Label id="lblArrTh" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.THR_ViolationDate") %>'>
										    </asp:Label>
									    </ItemTemplate>
								    </asp:TemplateColumn>
								    <asp:TemplateColumn HeaderText="Name">
									    <HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
									    <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									    <ItemTemplate>
										    <asp:HyperLink id=hlnkNameTh runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.THR_Name") %>' Font-Underline="True" NavigateUrl='<%# String.Concat("../clientinfo/ViolationFeeOld.aspx?caseNumber=",DataBinder.Eval(Container, "DataItem.THR_TicketID_PK"),"&amp;search=0") %>'>
										    </asp:HyperLink>
									    </ItemTemplate>
								    </asp:TemplateColumn>
							    </Columns>
						    </asp:datagrid></td>
					    <td vAlign="top" width="20%"><asp:datagrid id="dg_Fri" runat="server" AutoGenerateColumns="False" BorderStyle="Solid" BorderColor="Black"
							    Width="80%" EnableViewState="False">
							    <Columns>
								    <asp:TemplateColumn HeaderText="No">
									    <HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
									    <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									    <ItemTemplate>
										    <asp:Label id=lblNoF runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SEQ_CustNumber") %>'>
										    </asp:Label>
										    <asp:Label id=lblTicketidF runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FRI_TicketID_PK") %>' Visible="False">
										    </asp:Label>
									    </ItemTemplate>
								    </asp:TemplateColumn>
								    <asp:TemplateColumn HeaderText="Com">
									    <HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
									    <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									    <ItemTemplate>
										    <asp:Label id=lblTrialF runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FRI_TrialDateTime") %>'>
										    </asp:Label>
										    <asp:Label id=lblBondF runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FRI_BondFlag") %>'>
										    </asp:Label>
										    <asp:Label id=lblTrialCF runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FRI_TrialComments") %>'>
										    </asp:Label>
										    <asp:Label id="lblPlusF" runat="server"></asp:Label>
										    <asp:Label id=lblPreF runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FRI_pretrialstatus") %>'>
										    </asp:Label>
									    </ItemTemplate>
								    </asp:TemplateColumn>
								    <asp:TemplateColumn HeaderText="Arr">
									    <HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
									    <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman" HorizontalAlign="Center"></ItemStyle>
									    <ItemTemplate>
										    <asp:Label id="lblArrF" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FRI_ViolationDate") %>'>
										    </asp:Label>
									    </ItemTemplate>
								    </asp:TemplateColumn>
								    <asp:TemplateColumn HeaderText="Name">
									    <HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
									    <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									    <ItemTemplate>
										    <asp:HyperLink id=hlnkNameF runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FRI_Name") %>' Font-Underline="True" NavigateUrl='<%# String.Concat("../clientinfo/ViolationFeeOld.aspx?caseNumber=",DataBinder.Eval(Container, "DataItem.FRI_TicketID_PK"),"&amp;search=0") %>'>
										    </asp:HyperLink>
									    </ItemTemplate>
								    </asp:TemplateColumn>
							    </Columns>
						    </asp:datagrid></td>
				    </tr>
				    <tr>
					    <td><STRONG><FONT size="2">Comments:</FONT></STRONG>
					    </td>
					    <td><STRONG><FONT size="2">Comments:</FONT></STRONG>
					    </td>
					    <td><STRONG><FONT size="2">Comments:</FONT></STRONG>
					    </td>
					    <td><STRONG><FONT size="2">Comments:</FONT></STRONG>
					    </td>
					    <td><STRONG><FONT size="2">Comments:</FONT></STRONG>
					    </td>
				    </tr>
				    <tr>
					    <td vAlign="top"><asp:datagrid id="dg_MonC" runat="server" AutoGenerateColumns="False" EnableViewState="False"
							    ShowHeader="False" GridLines="None">
							    <Columns>
								    <asp:TemplateColumn>
									    <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									    <ItemTemplate>
										    <asp:Label id=lblNo1M runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SEQ_CustNumber") %>'>
										    </asp:Label>
										    <asp:Label id=lblTicketid1M runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MON_TicketID_PK") %>' Visible="False">
										    </asp:Label>
									    </ItemTemplate>
								    </asp:TemplateColumn>
								    <asp:TemplateColumn>
									    <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									    <ItemTemplate>
										    <asp:Label id="lblCommM" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MON_TrialComments") %>'>
										    </asp:Label>
									    </ItemTemplate>
								    </asp:TemplateColumn>
							    </Columns>
						    </asp:datagrid></td>
					    <td vAlign="top"><asp:datagrid id="dg_TueC" runat="server" AutoGenerateColumns="False" EnableViewState="False"
							    ShowHeader="False" GridLines="None">
							    <Columns>
								    <asp:TemplateColumn>
									    <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									    <ItemTemplate>
										    <asp:Label id=lblNo1T runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SEQ_CustNumber") %>'>
										    </asp:Label>
										    <asp:Label id=lblTicketid1T runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TUE_TicketID_PK") %>' Visible="False">
										    </asp:Label>
									    </ItemTemplate>
								    </asp:TemplateColumn>
								    <asp:TemplateColumn>
									    <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									    <ItemTemplate>
										    <asp:Label id="lblCommT" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Tue_TrialComments") %>'>
										    </asp:Label>
									    </ItemTemplate>
								    </asp:TemplateColumn>
							    </Columns>
						    </asp:datagrid></td>
					    <td vAlign="top"><asp:datagrid id="dg_WedC" runat="server" AutoGenerateColumns="False" EnableViewState="False"
							    ShowHeader="False" GridLines="None">
							    <Columns>
								    <asp:TemplateColumn>
									    <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									    <ItemTemplate>
										    <asp:Label id=lblNo1W runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SEQ_CustNumber") %>'>
										    </asp:Label>
										    <asp:Label id=lblTicketid1W runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.WED_TicketID_PK") %>' Visible="False">
										    </asp:Label>
									    </ItemTemplate>
								    </asp:TemplateColumn>
								    <asp:TemplateColumn>
									    <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									    <ItemTemplate>
										    <asp:Label id="lblCommW" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.WED_TrialComments") %>'>
										    </asp:Label>
									    </ItemTemplate>
								    </asp:TemplateColumn>
							    </Columns>
						    </asp:datagrid></td>
					    <td vAlign="top"><asp:datagrid id="dg_ThuC" runat="server" AutoGenerateColumns="False" EnableViewState="False"
							    ShowHeader="False" GridLines="None">
							    <Columns>
								    <asp:TemplateColumn>
									    <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									    <ItemTemplate>
										    <asp:Label id=lblNo1Th runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SEQ_CustNumber") %>'>
										    </asp:Label>
										    <asp:Label id=lblTicketid1Th runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.THR_TicketID_PK") %>' Visible="False">
										    </asp:Label>
									    </ItemTemplate>
								    </asp:TemplateColumn>
								    <asp:TemplateColumn>
									    <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									    <ItemTemplate>
										    <asp:Label id="lblCommTh" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.THR_TrialComments") %>'>
										    </asp:Label>
									    </ItemTemplate>
								    </asp:TemplateColumn>
							    </Columns>
						    </asp:datagrid></td>
					    <td vAlign="top"><asp:datagrid id="dg_FriC" runat="server" AutoGenerateColumns="False" EnableViewState="False"
							    ShowHeader="False" GridLines="None">
							    <Columns>
								    <asp:TemplateColumn>
									    <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									    <ItemTemplate>
										    <asp:Label id=lblNo1F runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SEQ_CustNumber") %>'>
										    </asp:Label>
										    <asp:Label id=lblTicketid1F runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FRI_TicketID_PK") %>' Visible="False">
										    </asp:Label>
									    </ItemTemplate>
								    </asp:TemplateColumn>
								    <asp:TemplateColumn>
									    <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									    <ItemTemplate>
										    <asp:Label id="lblCommF" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Fri_TrialComments") %>'>
										    </asp:Label>
									    </ItemTemplate>
								    </asp:TemplateColumn>
							    </Columns>
						    </asp:datagrid></td>
				    </tr>
			    </TABLE>
			   
			    </td>
			    </tr>
			    <tr>
				    <td align=center>
			
				    <table align=center cellpadding=0 cellspacing=0 border=0 width=780>	
					    <TR >
						    <TD width="100%" background="../../images/separator_repeat.gif" 
							    height="9">
						    </TD>
					    </TR>
					    <tr>
						    <td width=100%>
							    <uc1:footer id="Footer1" runat="server"></uc1:footer>
						    </td>
					    </tr>
				    </TABLE>
			
				    </td>
				    </tr>
			    </table>
			</FORM>
		
	</body>
</HTML>

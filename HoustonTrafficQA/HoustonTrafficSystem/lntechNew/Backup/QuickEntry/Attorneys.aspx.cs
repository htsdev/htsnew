using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;

namespace lntechNew.QuickEntry
{
    public partial class Attorneys : System.Web.UI.Page
    {

        clsENationWebComponents ClsDB = new clsENationWebComponents();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                BindGrid();

            }

        }

        private void BindGrid()
        {
            try
            {
                DataSet ds = ClsDB.Get_DS_BySP("USP_HTS_GetAttorneyAddress");
                gv_list.DataSource = ds.Tables[0];
                gv_list.DataBind();
             }
            catch (Exception ex){

                lbl_error.Text = ex.Message.ToString();
            }
        }

        protected void btn_refresh_Click(object sender, EventArgs e)
        {

            try
            {
                string[] keys = { "@Email" };
                object[] values = { tb_email.Text };
                int i = (int)ClsDB.ExecuteScalerBySp("USP_HTS_ADDAttorneyAddress", keys, values);


                if (i == 1)
                {


                    BindGrid();
                    tb_email.Text = "";
                }
                else if (i == 0)
                {
                    Response.Write("<script> alert('Please provide different email address.This email address is already exist.');</script>");
                }


            }
            catch (Exception ex)
            {
                lbl_error.Text = ex.Message.ToString();
            }


        }

        protected void gv_list_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "delete")
            {

                try
                {
                    string[] keys = { "@Id" };
                    object[] values = { e.CommandArgument.ToString() };
                    ClsDB.ExecuteSP("USP_HTS_DeleteAttorneyAddress", keys, values);
                    BindGrid();

                }
                catch (Exception ex){
                    lbl_error.Text = ex.Message.ToString();
                }

            
            
            }
        }

        protected void gv_list_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }

        protected void gv_list_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}

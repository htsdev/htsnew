using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.QuickEntry
{
    public partial class popup_UpdateMidnum : System.Web.UI.Page
    {
        clsLogger bugTracker = new clsLogger();
        private clsENationWebComponents clsDb = new clsENationWebComponents();
        private DataTable dtRecord = new DataTable();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Get_Records();
                if (dtRecord.Rows.Count > 0)
                {
                    tr_cancel.Visible = false;
                    tr_yesno.Visible = true;
                    lbl_CauseNumber.Text = Request.QueryString["CauseNo"];
                    lbl_Fullname.Text = dtRecord.Rows[0]["Fullname"].ToString();
                    lbl_MidNum.Text = Request.QueryString["MidNo"];
                    hf_OldMidnum.Value = dtRecord.Rows[0]["MidNum"].ToString();
                }
                else
                {
                    lbl_Info.Text = "No such cause number exists. Please enter a correct cause number.";
                    tr_cancel.Visible = true;
                    tr_yesno.Visible = false;

                }
                
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void Get_Records()
        {
            string[] keys = {"@CauseNumber" };
            object[] values = { Request.QueryString["CauseNo"] };
            dtRecord = clsDb.Get_DT_BySPArr("USP_HTS_MIDNUMBER_GETRECORD", keys, values);
        }
    }
}

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.QuickEntry
{
	/// <summary>
	/// Summary description for ArraignmentSnapShot.
	/// </summary>
	public partial class ArraignmentSnapShot : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.DataGrid dg_Tue;
		protected System.Web.UI.WebControls.DataGrid dg_Wed;
		protected System.Web.UI.WebControls.DataGrid dg_Thu;
		protected System.Web.UI.WebControls.DataGrid dg_Fri;
		protected System.Web.UI.WebControls.DataGrid dg_MonC;
		protected System.Web.UI.WebControls.DataGrid dg_TueC;
		protected System.Web.UI.WebControls.DataGrid dg_WedC;
		protected System.Web.UI.WebControls.DataGrid dg_ThuC;
		protected System.Web.UI.WebControls.DataGrid dg_Mon;
		protected System.Web.UI.WebControls.DataGrid dg_FriC;
		clsLogger bugTracker = new clsLogger();
		private DateTime CSDMon;	
		private DateTime CSDTue;
		private DateTime CSDWed;
		private DateTime CSDThr;
		private DateTime CSDFri;
		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				// Put user code to initialize the page here
				if(!IsPostBack)
				{
					clsSession ClsSession=new clsSession();
					// Check for valid Session if not then redirect to login page	
					/*if (ClsSession.IsValidSession(this.Request)==false)
					{
						Response.Redirect("../frmlogin.aspx");
					}
					ClsSession.SetSessionVariable("sMoved","False",this.Session);
					*/
					clsENationWebComponents ClsDB=new clsENationWebComponents();
                    lblPrinDate.Text = "Print Date: " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();

					DataSet	ds=ClsDB.Get_DS_BySP("usp_HTS_ArraignmentSnapShot_VER_2");
					#region Moday
					if(ds.Tables[0].Rows.Count>0)
					{
						dg_Mon.DataSource=ds.Tables[0];
						dg_Mon.DataBind();
						dg_MonC.DataSource=ds.Tables[0];
						dg_MonC.DataBind();
						GenerateSerial();				
					}
					#endregion
					#region Tuesday
					if(ds.Tables[1].Rows.Count>0)
					{
						dg_Tue.DataSource=ds.Tables[1];
						dg_Tue.DataBind();
						dg_TueC.DataSource=ds.Tables[1];
						dg_TueC.DataBind();
						GenerateSerial1();					
					}
					#endregion
					#region Wednesday
					if(ds.Tables[2].Rows.Count>0)
					{
						dg_Wed.DataSource=ds.Tables[2];
						dg_Wed.DataBind();
						dg_WedC.DataSource=ds.Tables[2];
						dg_WedC.DataBind();
						GenerateSerial2();					
					}
					#endregion
					#region Thursday
					if(ds.Tables[3].Rows.Count>0)
					{
						dg_Thu.DataSource=ds.Tables[3];
						dg_Thu.DataBind();
						dg_ThuC.DataSource=ds.Tables[3];
						dg_ThuC.DataBind();
						GenerateSerial3();					
					}
					#endregion	
					#region Friday
					if(ds.Tables[4].Rows.Count>0)
					{
						dg_Fri.DataSource=ds.Tables[4];
						dg_Fri.DataBind();
						dg_FriC.DataSource=ds.Tables[4];
						dg_FriC.DataBind();
						GenerateSerial4();					
					}
					#endregion
				
				}
			}
			catch(Exception	ex)
			{
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void FindStartDate()
		{
			DateTime strDate=DateTime.Now.AddDays(21);
			int intDay=0;
			for (intDay=0;intDay<=6;intDay++)
			{
				strDate = DateTime.Now.AddDays(intDay); //DateAdd("d",intDay,date())		
				if(strDate.DayOfWeek!=DayOfWeek.Saturday && strDate.DayOfWeek!=DayOfWeek.Sunday )
				{
					if (strDate.DayOfWeek== DayOfWeek.Monday )
					{
						CSDMon = strDate;
					}
					if (strDate.DayOfWeek== DayOfWeek.Tuesday)
					{
						CSDTue = strDate;
					}
					if (strDate.DayOfWeek== DayOfWeek.Wednesday )
					{
						CSDWed = strDate;
					}
					if (strDate.DayOfWeek== DayOfWeek.Thursday)
					{
						CSDThr = strDate;
					}
					if (strDate.DayOfWeek== DayOfWeek.Friday )
					{
						CSDFri = strDate;
					}
				}
			}	
		}

		//Monday
		private void GenerateSerial()
		{
							
			try
			{
				foreach (DataGridItem ItemX in dg_Mon.Items) 
				{ 					
					Label tid=(Label) ItemX.FindControl("lblTicketidM");
					Label no=(Label) ItemX.FindControl("lblNoM");
					if(tid.Text!="")
					{
						Label arrdate=(Label) ItemX.FindControl("lblArrM");
						Label tdt=(Label) ItemX.FindControl("lblTrialM");
						Label bf=(Label) ItemX.FindControl("lblBondM");
						Label tc=(Label) ItemX.FindControl("lblTrialCM");
						Label pt=(Label) ItemX.FindControl("lblPreM");
						Label pl=(Label) ItemX.FindControl("lblPlusM");
						
						if(arrdate.Text!="")
						{
							DateTime adate=Convert.ToDateTime(arrdate.Text.ToString());
							arrdate.Text=String.Concat(adate.Month,"/",adate.Day);
							DateTime curr=DateTime.Now;
							int dif= adate.Day-curr.Day;
							if(dif>=0 && dif<=2 && curr.Month==adate.Month && curr.Year==adate.Year)
							{
								pl.Text="+";
							}
							else
							{
								pl.Visible=false;
							}
						}
						if(tdt.Text.ToUpper()=="10:30AM")
						{
							tdt.Text="t";
						}
						else
						{
							tdt.Visible=false;
						}
						if(Convert.ToInt32(bf.Text)>0)
						{
							bf.Text="b";
						}
						else
						{
							bf.Visible=false;
						}
						if(tc.Text!="")
						{
							tc.Text="*";
						}
						else
						{
							tc.Visible=false;
						}
						if( pt.Text!="P")
						{
							pt.Visible=false;
						}
					}					
				}
				foreach (DataGridItem ItemX in dg_MonC.Items) 
				{ 					
					Label com=(Label) ItemX.FindControl("lblCommM");
					Label no=(Label) ItemX.FindControl("lblNo1M");
					if(com.Text=="")
					{
						no.Visible=false;
					}
				}
			}
			catch(Exception ex)

			{
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}		
		}

		//Tuesday
		private void GenerateSerial1()
		{
							
			try
			{
				foreach (DataGridItem ItemX in dg_Tue.Items) 
				{ 					
					Label no=(Label) ItemX.FindControl("lblNoT");
					Label tid=(Label) ItemX.FindControl("lblTicketidT");
					if(tid.Text!="")
					{
						Label arrdate=(Label) ItemX.FindControl("lblArrT");
						Label tdt=(Label) ItemX.FindControl("lblTrialT");
						Label bf=(Label) ItemX.FindControl("lblBondT");
						Label tc=(Label) ItemX.FindControl("lblTrialCT");
						Label pt=(Label) ItemX.FindControl("lblPreT");
						Label pl=(Label) ItemX.FindControl("lblPlusT");
						if(arrdate.Text!="")
						{
							DateTime adate=Convert.ToDateTime(arrdate.Text.ToString());
							arrdate.Text=String.Concat(adate.Month,"/",adate.Day);
							DateTime curr=DateTime.Now;
							int dif= adate.Day-curr.Day;
							if(dif>=0 && dif<=2 && curr.Month==adate.Month && curr.Year==adate.Year)
							{
								pl.Text="+";
							}
							else
							{
								pl.Visible=false;
							}
						}
						if(tdt.Text.ToUpper()=="10:30AM")
						{
							tdt.Text="t";
						}
						else
						{
							tdt.Visible=false;
						}
						if(Convert.ToInt32(bf.Text)>0)
						{
							bf.Text="b";
						}
						else
						{
							bf.Visible=false;
						}
						if(tc.Text!="")
						{
							tc.Text="*";
						}
						else
						{
							tc.Visible=false;
						}
						if( pt.Text!="P")
						{
							pt.Visible=false;
						}
					}
				}
				foreach (DataGridItem ItemX in dg_TueC.Items) 
				{ 					
					Label com=(Label) ItemX.FindControl("lblCommT");
					Label no=(Label) ItemX.FindControl("lblNo1T");
					if(com.Text=="")
					{
						no.Visible=false;
					}
				}
			}
			catch(Exception ex)

			{
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}		
		}

		//Wednesday
		private void GenerateSerial2()
		{
							
			try
			{
				foreach (DataGridItem ItemX in dg_Wed.Items) 
				{ 					
					Label no=(Label) ItemX.FindControl("lblNoW");
					Label tid=(Label) ItemX.FindControl("lblTicketidW");
					if(tid.Text!="")
					{
						Label arrdate=(Label) ItemX.FindControl("lblArrW");
						Label tdt=(Label) ItemX.FindControl("lblTrialW");
						Label bf=(Label) ItemX.FindControl("lblBondW");
						Label tc=(Label) ItemX.FindControl("lblTrialCW");
						Label pt=(Label) ItemX.FindControl("lblPreW");
						Label pl=(Label) ItemX.FindControl("lblPlusW");
						if(arrdate.Text!="")
						{
							DateTime adate=Convert.ToDateTime(arrdate.Text.ToString());
							arrdate.Text=String.Concat(adate.Month,"/",adate.Day);
							DateTime curr=DateTime.Now;
							int dif= adate.Day-curr.Day;
							if(dif>=0 && dif<=2 && curr.Month==adate.Month && curr.Year==adate.Year)
							{
								pl.Text="+";
							}
							else
							{
								pl.Visible=false;
							}
						}
						if(tdt.Text.ToUpper()=="10:30AM")
						{
							tdt.Text="t";
						}
						else
						{
							tdt.Visible=false;
						}
						if(Convert.ToInt32(bf.Text)>0)
						{
							bf.Text="b";
						}
						else
						{
							bf.Visible=false;
						}
						if(tc.Text!="")
						{
							tc.Text="*";
						}
						else
						{
							tc.Visible=false;
						}
						if( pt.Text!="P")
						{
							pt.Visible=false;
						}
					}
				}
				foreach (DataGridItem ItemX in dg_WedC.Items) 
				{ 					
					Label com=(Label) ItemX.FindControl("lblCommW");
					Label no=(Label) ItemX.FindControl("lblNo1W");
					if(com.Text=="")
					{
						no.Visible=false;
					}
				}
			}
			catch(Exception ex)

			{
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}		
		}

		//Thursday
		private void GenerateSerial3()
		{
							
			try
			{
				foreach (DataGridItem ItemX in dg_Thu.Items) 
				{ 					
					Label no=(Label) ItemX.FindControl("lblNoTh");
					Label tid=(Label) ItemX.FindControl("lblTicketidTh");
					if(tid.Text!="")
					{
						Label arrdate=(Label) ItemX.FindControl("lblArrTh");
						Label tdt=(Label) ItemX.FindControl("lblTrialTh");
						Label bf=(Label) ItemX.FindControl("lblBondTh");
						Label tc=(Label) ItemX.FindControl("lblTrialCTh");
						Label pt=(Label) ItemX.FindControl("lblPreTh");
						Label pl=(Label) ItemX.FindControl("lblPlusTh");
						if(arrdate.Text!="")
						{
							DateTime adate=Convert.ToDateTime(arrdate.Text.ToString());
							arrdate.Text=String.Concat(adate.Month,"/",adate.Day);
							DateTime curr=DateTime.Now;
							int dif= adate.Day-curr.Day;
							if(dif>=0 && dif<=2 && curr.Month==adate.Month && curr.Year==adate.Year)
							{
								pl.Text="+";
							}
							else
							{
								pl.Visible=false;
							}
						}
						if(tdt.Text.ToUpper()=="10:30AM")
						{
							tdt.Text="t";
						}
						else
						{
							tdt.Visible=false;
						}
						if(Convert.ToInt32(bf.Text)>0)
						{
							bf.Text="b";
						}
						else
						{
							bf.Visible=false;
						}
						if(tc.Text!="")
						{
							tc.Text="*";
						}
						else
						{
							tc.Visible=false;
						}
						if( pt.Text!="P")
						{
							pt.Visible=false;
						}
					}
				}
				foreach (DataGridItem ItemX in dg_ThuC.Items) 
				{ 					
					Label com=(Label) ItemX.FindControl("lblCommTh");
					Label no=(Label) ItemX.FindControl("lblNo1Th");
					if(com.Text=="")
					{
						no.Visible=false;
					}
				}
			}
			catch(Exception ex)

			{
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}		
		}

		//Friday
		private void GenerateSerial4()
		{
							
			try
			{
				foreach (DataGridItem ItemX in dg_Fri.Items) 
				{ 					
					Label no=(Label) ItemX.FindControl("lblNoF");
					Label tid=(Label) ItemX.FindControl("lblTicketidF");
					if(tid.Text!="")
					{
						Label arrdate=(Label) ItemX.FindControl("lblArrF");
						Label tdt=(Label) ItemX.FindControl("lblTrialF");
						Label bf=(Label) ItemX.FindControl("lblBondF");
						Label tc=(Label) ItemX.FindControl("lblTrialCF");
						Label pt=(Label) ItemX.FindControl("lblPreF");
						Label pl=(Label) ItemX.FindControl("lblPlusF");
						if(arrdate.Text!="")
						{
							DateTime adate=Convert.ToDateTime(arrdate.Text.ToString());
							arrdate.Text=String.Concat(adate.Month,"/",adate.Day);
							DateTime curr=DateTime.Now;
							int dif= adate.Day-curr.Day;
							if(dif>=0 && dif<=2 && curr.Month==adate.Month && curr.Year==adate.Year)
							{
								pl.Text="+";
							}
							else
							{
								pl.Visible=false;
							}
						}
						if(tdt.Text.ToUpper()=="10:30AM")
						{
							tdt.Text="t";
						}
						else
						{
							tdt.Visible=false;
						}
						if(Convert.ToInt32(bf.Text)>0)
						{
							bf.Text="b";
						}
						else
						{
							bf.Visible=false;
						}
						if(tc.Text!="")
						{
							tc.Text="*";
						}
						else
						{
							tc.Visible=false;
						}
						if( pt.Text!="P")
						{
							pt.Visible=false;
						}
					}					
				}
				foreach (DataGridItem ItemX in dg_FriC.Items) 
				{ 					
					Label com=(Label) ItemX.FindControl("lblCommF");
					Label no=(Label) ItemX.FindControl("lblNo1F");
					if(com.Text=="")
					{
						no.Visible=false;
					}
				}
			}
			catch(Exception ex)

			{
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}		
		}
	}
}

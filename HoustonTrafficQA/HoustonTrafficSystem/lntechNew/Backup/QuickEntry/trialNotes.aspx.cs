using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components ;
using lntechNew.Components.ClientInfo;
using System.Data.SqlClient;
using lntechNew.Components ;




namespace lntechNew.QuickEntry
{
	/// <summary>
	/// Summary description for trialNotes.
	/// </summary>
	public partial class trialNotes : System.Web.UI.Page
	{
		clsSession clsSession=new clsSession ();  
		clsLogger bugTracker = new clsLogger();
		clsENationWebComponents ClsDb=new clsENationWebComponents ();

		int queryString=0;
		int empid=0;
		
		protected System.Web.UI.WebControls.Label lbl_DispTicketNo;
		protected System.Web.UI.WebControls.Label Lbl_DispFLName;
		protected System.Web.UI.WebControls.TextBox Txt_DispTrialComments;
		protected System.Web.UI.WebControls.Button Btn_TrialNotes_Submit;
		protected System.Web.UI.WebControls.TextBox txtbefore;
		protected System.Web.UI.WebControls.Label lbl_Message;
		
		private void Page_Load(object sender, System.EventArgs e)
		{
			 queryString=Convert.ToInt32(Request["casenumber"]);
             empid=Convert.ToInt32 (clsSession .GetCookie ("sEmpID",this.Request));
             //trial comments when page load
			if(!Page.IsPostBack )
				Disp_CaseNumber_Name_TrialComments ();
		}

		private void Disp_CaseNumber_Name_TrialComments()
		{
			string[] key    = {"@TicketID_PK"};
			object[] value1 = {queryString};
			DataSet Ds_TextFields = ClsDb.Get_DS_BySPArr("usp_hts_Get_casenumber_name_trialcomments",key,value1);
            Lbl_DispFLName.Text=Ds_TextFields.Tables[0].Rows[0][0].ToString();
            lbl_DispTicketNo.Text=Ds_TextFields.Tables[0].Rows[0][1].ToString();
			Txt_DispTrialComments.Text=Ds_TextFields.Tables[0].Rows[0][2].ToString();
			
             txtbefore.Text =Ds_TextFields.Tables[0].Rows[0][2].ToString();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Btn_TrialNotes_Submit.Click += new System.EventHandler(this.Btn_TrialNotes_Submit_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void Btn_TrialNotes_Submit_Click(object sender, System.EventArgs e)
		{
			try
			{
				if ( ! txtbefore.Text.Equals(Txt_DispTrialComments.Text)  )
				{
					string[] key    = {"@TicketNumber","@trialComments","@EMPID"};
					object[] value1 = {lbl_DispTicketNo .Text,this.Txt_DispTrialComments .Text,empid};
					ClsDb .ExecuteSP ("usp_hts_update_trialcomments_for_Ticket_ID",key,value1);
					HttpContext.Current.Response.Write("<script language='javascript'> opener.location.reload(); self.close();   </script>");
					
				}
				else
				{
					HttpContext.Current.Response.Write("<script language='javascript'> self.close();  </script>");
				}

			}
			
			catch(Exception ex)
			{
				lbl_Message.Visible=true;
				lbl_Message.Text=ex.Message;				
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}		
		}

		
	}
}

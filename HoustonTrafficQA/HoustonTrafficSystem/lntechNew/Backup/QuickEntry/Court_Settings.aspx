<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.QuickEntry.Court_Settings" Codebehind="Court_Settings.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Court_Settings</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE id="TableMain" style="Z-INDEX: 101; LEFT: 88px; POSITION: absolute; TOP: 16px" cellSpacing="0"
				cellPadding="0" width="780" align="center" border="0">
				<TR>
					<TD style="WIDTH: 815px" colSpan="4"></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 816px" colSpan="4" align="left">
						<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
							<TR>
								<TD vAlign="top" align="left" colSpan="4">
									<uc1:ActiveMenu id="ActiveMenu1" runat="server"></uc1:ActiveMenu></TD>
							</TR>
							<TR>
								<TD width="100%" background="../Images/separator_repeat.gif" colSpan="4" height="11"></TD>
							</TR>
							<TR>
								<TD align="center" width="100%" colSpan="4" height="11" rowSpan="1">
									<TABLE id="Table2" cellSpacing="1" cellPadding="1" width="100%" border="1" class="clsleftpaddingtable"
										borderColor="#ffffff">
										<TR>
											<TD class="clssubhead" style="WIDTH: 77px">Show Me</TD>
											<TD style="WIDTH: 105px"><asp:dropdownlist id="ddl_weeks" runat="server" CssClass="clsinputcombo" Width="72px"></asp:dropdownlist></TD>
											<TD style="WIDTH: 106px"><asp:button id="btn_refresh" runat="server" CssClass="clsbutton" Width="104px" CommandName="search"
													Text="Refresh List"></asp:button></TD>
											<TD style="WIDTH: 146px"><asp:button id="btn_update" runat="server" CssClass="clsbutton" Width="144px" CommandName="search"
													Text="Update Court Schedule"></asp:button></TD>
											<TD></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD style="WIDTH: 760px; HEIGHT: 75px" align="left" colSpan="0"><asp:datalist id="dl_result" runat="server" CssClass="clsleftpaddingtable" Width="780px" RepeatDirection="Horizontal"
										RepeatColumns="5" BorderColor="White" BorderWidth="1px" CellPadding="0" GridLines="Both">
										<HeaderTemplate>
											<TABLE id="Table5" borderColor="#ffffff" cellSpacing="0" cellPadding="0" width="100%" border="1">
												<TR>
													<TD class="clssubhead" align="center">Monday</TD>
													<TD class="clssubhead" align="center">Tuesday</TD>
													<TD class="clssubhead">Wednesday</TD>
													<TD class="clssubhead" align="center">Thursday</TD>
													<TD class="clssubhead" align="center">Friday</TD>
												</TR>
												<TR>
													<TD>
														<TABLE id="Table4" borderColor="white" cellSpacing="0" cellPadding="0" width="100%" border="1">
															<TR>
																<TD class="clssubhead" align="center" width="30">Date</TD>
																<TD class="clssubhead" width="40">Court/People</TD>
																<TD class="clssubhead" width="30">Status</TD>
															</TR>
														</TABLE>
													</TD>
													<TD>
														<TABLE id="Table4" borderColor="#ffffff" cellSpacing="0" cellPadding="0" width="100%" border="1">
															<TR>
																<TD class="clssubhead">Date</TD>
																<TD class="clssubhead">Court/People</TD>
																<TD class="clssubhead">Status</TD>
															</TR>
														</TABLE>
													</TD>
													<TD>
														<TABLE id="Table4" borderColor="#ffffff" cellSpacing="0" cellPadding="0" width="100%" border="1">
															<TR>
																<TD class="clssubhead">Date</TD>
																<TD class="clssubhead">Court/People</TD>
																<TD class="clssubhead">Status</TD>
															</TR>
														</TABLE>
													</TD>
													<TD>
														<TABLE id="Table4" borderColor="#ffffff" cellSpacing="0" cellPadding="0" width="100%" border="1">
															<TR>
																<TD class="clssubhead">Date</TD>
																<TD class="clssubhead">Court/People</TD>
																<TD class="clssubhead">Status</TD>
															</TR>
														</TABLE>
													</TD>
													<TD>
														<TABLE id="Table4" borderColor="#ffffff" cellSpacing="0" cellPadding="0" width="100%" border="1">
															<TR>
																<TD class="clssubhead">Date</TD>
																<TD class="clssubhead">Court/People</TD>
																<TD class="clssubhead">Status</TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE>
										</HeaderTemplate>
										<ItemTemplate>
											<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
												<TR>
													<TD align="center" width="30%">
														<asp:Label id=lbl_date runat="server" Width="46px" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.court_date", "{0:d}") %>'>
														</asp:Label></TD>
													<TD align="center" width="40%">
														<asp:TextBox id=tb_crtnum runat="server" Width="20px" CssClass="clsinputadministration" Text='<%# DataBinder.Eval(Container, "DataItem.CourtNumber") %>'>
														</asp:TextBox>/
														<asp:TextBox id=tb_pplassign runat="server" Width="25px" CssClass="clsinputadministration" Text='<%# DataBinder.Eval(Container, "DataItem.peopleassigned830") %>'>
														</asp:TextBox></TD>
													<TD align="center" width="30%">
														<asp:Label id=lbl_status runat="server" Width="46px" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.status") %>'>
														</asp:Label></TD>
												</TR>
											</TABLE>
											<asp:Label id=lblcrtdate runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.crtdate") %>' Visible="False">
											</asp:Label>
										</ItemTemplate>
									</asp:datalist></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 760px; HEIGHT: 13px" align="left" background="../Images/separator_repeat.gif"
									colSpan="4"></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 760px" align="center" colSpan="4"><asp:label id="lbl_error" runat="server" ForeColor="Red"></asp:label></TD>
							</TR>
						</TABLE>
						<uc1:Footer id="Footer1" runat="server"></uc1:Footer>
					</TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>

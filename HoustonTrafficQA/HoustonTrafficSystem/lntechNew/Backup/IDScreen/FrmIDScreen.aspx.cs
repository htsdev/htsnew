using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;


namespace lntechNew.IDScreen
{
	/// <summary>
	/// Summary description for FrmIDScreen.
	/// </summary>
	/// 

	public partial class FrmIDScreen : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label Label_TicketNo;
		protected System.Web.UI.WebControls.Label Label_LName;
		protected System.Web.UI.WebControls.Label label_fname;
		protected System.Web.UI.WebControls.TextBox txt_fname;
		protected System.Web.UI.WebControls.TextBox txt_ticketno;
		protected System.Web.UI.WebControls.TextBox txt_lname;
		clsENationWebComponents ClsDb = new clsENationWebComponents();
	
		string fname ;
		string lname ;		
		string Tickno ;
		string StrExp ;
		string StrAcsDec ;
        string Causenumber;
		
		DataSet ds;
		protected System.Web.UI.WebControls.ImageButton Btn_search;
		protected System.Web.UI.WebControls.ImageButton btn_Reset;
		protected System.Web.UI.WebControls.Label lblMessage;
		protected System.Web.UI.WebControls.RegularExpressionValidator RegularExpressionValidator1;	
		protected System.Web.UI.WebControls.DataGrid DG_IDScreen;
		clsSession ClsSession =new clsSession();
		private void Page_Load(object sender, System.EventArgs e)
		{
		//	DG_IDScreen.EditItemIndex = -1;
			if (ClsSession.IsValidSession(this.Request)==false)
			{
				Response.Redirect("../frmlogin.aspx",false);
			}
			else //To stop page further execution
			{
				FillGrid();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Btn_search.Click += new System.Web.UI.ImageClickEventHandler(this.Btn_search_Click);
			this.btn_Reset.Click += new System.Web.UI.ImageClickEventHandler(this.btn_Reset_Click);
			this.DG_IDScreen.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.DG_IDScreen_PageIndexChanged);
			this.DG_IDScreen.CancelCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.DG_IDScreen_CancelCommand);
			this.DG_IDScreen.EditCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.DG_IDScreen_EditCommand);
			this.DG_IDScreen.SortCommand += new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.DG_IDScreen_SortCommand);
			this.DG_IDScreen.UpdateCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.DG_IDScreen_UpdateCommand);
			this.DG_IDScreen.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.DG_IDScreen_ItemDataBound);
			this.DG_IDScreen.SelectedIndexChanged += new System.EventHandler(this.DG_IDScreen_SelectedIndexChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
        
		//Method to fill the grid
		public void FillGrid()
		{
			try
			{		
				if(txt_fname.Text== "") 
					fname = "";
				else 
					fname =txt_fname.Text.Trim();
				if(txt_lname.Text== "") 
					lname = "";
				else 
					lname =txt_lname.Text.Trim();
				if(txt_ticketno.Text== "") 
					Tickno = "";
				else 
					Tickno =txt_ticketno.Text.Trim();
                if (txt_causenum.Text == "")
                    Causenumber = "";
                else
                    Causenumber = txt_causenum.Text.Trim();
				
				string[] key    = {"@firstname","@lastname","@TicketNo","@Causeno"};
                object[] value1 = { fname, lname, Tickno, Causenumber };
				ds = ClsDb.Get_DS_BySPArr("Sp_get_Idtickets",key,value1);
				
				DG_IDScreen.DataSource=ds;
				if(!IsPostBack)
				{
					DG_IDScreen.DataBind();
					BindReport();				
				}
				lblMessage.Text="";
				if (ds.Tables[0].Rows.Count  < 1 )
				{
					lblMessage.Text = "No record found";
				}
			}
			
			catch(Exception ex)
			{
				Response.Write(ex.Message);
			}
		}

		//Data Grid Edit Event
	

		//Data Grid Cancel Event
	

		//Method to sort Data Grid
		private void SortDataGrid(string Sortnm)
		{
			DataView Dv = ds.Tables[0].DefaultView;
			Dv.Sort = Sortnm + " " + StrAcsDec ;
			DG_IDScreen.DataSource= Dv;
			DG_IDScreen.DataBind();	
			BindReport();			
		}
        //Method to set 'ASC' 'DESC' 
		private void SetAcsDesc(string Val)
		{
			try
			{
				StrExp = Session["StrExp"].ToString();
				StrAcsDec = Session["StrAcsDec"].ToString();
			}
			catch
			{

			}

			if (StrExp == Val)
			{
				if (StrAcsDec == "ASC")
				{
					StrAcsDec = "DESC";
					Session["StrAcsDec"] = StrAcsDec ;
				}
				else 
				{
					StrAcsDec = "ASC";
					Session["StrAcsDec"] = StrAcsDec ;
				}
			}
			else 
			{
				StrExp = Val;
				StrAcsDec = "ASC";
				Session["StrExp"] = StrExp ;
				Session["StrAcsDec"] = StrAcsDec ;				
			}
		}
        //Method to generate S# for Data Grid
		private void BindReport()
		{
			long sNo=(DG_IDScreen.CurrentPageIndex)*(DG_IDScreen.PageSize);									
			try
			{
				foreach (DataGridItem ItemX in DG_IDScreen.Items) 
				{ 					
					sNo +=1 ;

					((Label)(ItemX.FindControl("lblSno"))).Text= sNo.ToString()   ; 				
					((Label)(ItemX.FindControl("Label1"))).ToolTip = "Full Name : " + ((HyperLink)(ItemX.FindControl("hlkFulnm"))).Text +  "\nPhone No : " + ((HyperLink)(ItemX.FindControl("hlkphone"))).Text;
				}
			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message ;
			}		
		}

		//Data Grid Sort Event

		
		//Data Grid Update Event
		

		//Data Grid Page Index Event
		//Search Button Click Event

	
        //Reset Button Click Event
	

		private void DG_IDScreen_EditCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
            lblerror.Text = "";
			DG_IDScreen.EditItemIndex=e.Item.ItemIndex;	
			DG_IDScreen.DataSource = ds;
			DG_IDScreen.DataBind();
			BindReport();	
		}

		private void DG_IDScreen_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			DataRowView drv = (DataRowView) e.Item.DataItem;
			if (drv == null)
				return;

			try
			{
				((HyperLink) e.Item.FindControl("HLTicketno")).NavigateUrl = "../ClientInfo/ViolationFeeold.aspx?search=0&caseNumber="+ (int) drv["TicketID_PK"];
                ((HyperLink)e.Item.FindControl("HLcauseno")).NavigateUrl = "../ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + (int)drv["TicketID_PK"];
			}
			catch
			{

			}
		}

		private void DG_IDScreen_SortCommand(object source, System.Web.UI.WebControls.DataGridSortCommandEventArgs e)
		{
			string SortNm = e.SortExpression;			
			SetAcsDesc(SortNm);
			SortDataGrid(SortNm);
		}

		private void DG_IDScreen_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			DG_IDScreen.CurrentPageIndex = e.NewPageIndex;
			FillGrid();
			DG_IDScreen.DataBind();
			BindReport();
		}

		private void DG_IDScreen_UpdateCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
            int returntype = 0;
            lblerror.Text = "";
			foreach (DataGridItem i in this.DG_IDScreen.Items)
			{
				if (i.ItemIndex == e.Item.ItemIndex)
				{                    
					string TicketNo = ((TextBox) i.FindControl("txtTktNo")).Text;
                    if (TicketNo.Trim().Length == 0)
                    {
                        lblerror.Text = "This Ticketno does not exits in our database.";
                        return;
                    }
                    string causeno = ((TextBox)i.FindControl("txtcaseno")).Text;
                    int TicketVilationID = (int)Convert.ChangeType(((HyperLink)i.FindControl("HlkTktKey")).Text, typeof(int));
                    int TicketID = (int)Convert.ChangeType(((HyperLink)i.FindControl("HLkTicketID")).Text, typeof(int));
					//int empid = 3992;
					int empid= (int) Convert.ChangeType(Request.Cookies["sEmpID"].Value,typeof(int)); 
					//int voilationId = (int) Convert.ChangeType(((HyperLink) i.FindControl("HLkVoilationNo")).Text,typeof(int));					
                    string[] key = { "@TicketVilationID","@ticketId", "@ticketNo", "@causeno", "@empId", "@returnvalue" };
                    object[] value1 = { TicketVilationID,TicketID, TicketNo, causeno, empid, 0 };
                    object returnvalue = ClsDb.InsertBySPArrRet("Sp_Update_IdTickets", key, value1);
                    returntype = Convert.ToInt32(returnvalue);
                    if (returntype == -1)
                    {
                        lblerror.Text = "This Cause No already exist";
                        lblerror.Visible = true;
                    }
                    else
                    {
                        lblerror.Text = "Record updated successfully";
                        lblerror.Visible = true;
                    }
				}
			}
			DG_IDScreen.EditItemIndex = -1;
			FillGrid();
			DG_IDScreen.DataBind();
			BindReport();	
		}

		private void DG_IDScreen_CancelCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
            lblerror.Text = "";
			DG_IDScreen.EditItemIndex = -1;
			DG_IDScreen.DataBind();
			BindReport();	
		}

		private void Btn_search_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			DG_IDScreen.CurrentPageIndex=0;
            lblerror.Text = "";
			DG_IDScreen.EditItemIndex = -1;
			FillGrid();
			DG_IDScreen.DataBind();		
			BindReport();
		}

		private void btn_Reset_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			txt_fname.Text="";
			txt_lname.Text="";
			txt_ticketno.Text="";
            txt_causenum.Text = "";
			DG_IDScreen.EditItemIndex = -1;
            lblerror.Text = "";
			FillGrid();
			DG_IDScreen.DataBind();
			BindReport();
		}

		private void DG_IDScreen_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}

			
	}
}

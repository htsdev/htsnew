using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;
using HTP.Components;

namespace HTP.QuickEntryNew
{
    /// <summary>
    /// Summary description for ReminderNotes.
    /// </summary>
    public partial class ReminderNotes : System.Web.UI.Page
    {

        clsENationWebComponents ClsDb = new clsENationWebComponents();
        clsSession ClsSession = new clsSession();
        clsLogger clog = new clsLogger();
        clscalls clscall = new clscalls();
        clsBatch clsbatch = new clsBatch();
        clsLogger log = new clsLogger();
        // Agha Usman Ahmed for Bug Id 3882 04/28/2008
        const string reloadCode = "<script language='javascript'> window.opener.refreshContent();self.close();   </script>";
        int TicketID = 0, EmpID = 0;
        //int TicketViolationID = 0;
        DateTime SearchDate;
        clscalls.CallType Call;


        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                //Waqas 5057 03/17/2009 Checking employee info in session
                if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false)
                {
                    //sending back to parent page.	
                    if (litScript.Text == "")
                    {
                        litScript.Text = reloadCode;
                    }
                }
                TicketID = Convert.ToInt32(Request.QueryString["casenumber"]);
                //Zeeshan Ahmed 4837 09/23/2008 Save TicketViolationId's In View State 
                ViewState["TicketViolationIDs"] = Convert.ToString(Request.QueryString["violationID"]);
                SearchDate = Convert.ToDateTime(Request.QueryString["searchdate"]);
                string recid = Request.QueryString["Recid"];
                Session["ReminID"] = recid;
                EmpID = Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request));
                ViewState["EmpID"] = EmpID.ToString();
                Session["Updatereminder"] = "0";

                //Fahad 5296 12/12/2008
                Call = (clscalls.CallType)Convert.ToInt32(Request.QueryString["calltype"]);
                hfCallType.Value = Call.ToString();
                if (hfCallType.Value == "FTACall")
                {
                    txt_RComments.Visible = false;

                }
                else
                    txt_RComments.Visible = true;

                if (!IsPostBack)
                {


                    GetReminderStatus();
                    //String[] keys = { "@ticketid", "@courtdate" };
                    //Object[] values ={ TicketID, SearchDate };
                    //Fahad 1370 ( 1-30-08 )
                    //Code Modified By Fahad Use DataTable Instead of Dataset 
                    DataTable dt_ReminderNotes = clscall.GetReminderNotes(TicketID, SearchDate);
                    if (dt_ReminderNotes.Rows.Count == 0)
                    {
                        lblMessage.Text = "Exception Occurs";
                    }
                    else
                    {
                        DataRow dRow = dt_ReminderNotes.Rows[0];
                        lbl_Firm.Text = Convert.ToString(dRow["FirmAbbreviation"]);
                        lbl_Name.Text = Convert.ToString(dRow["LastName"]) + ", " + Convert.ToString(dRow["firstName"]);
                        lbl_Status.Text = Convert.ToString(dRow["trialdesc"]);
                        //ozair 4837 10/08/2008 commented courtadress
                        //lbl_location.Text = Convert.ToString(dRow["courtaddress"]);
                        lbl_contact1.Text = Convert.ToString(dRow["Contact1"]);
                        lbl_Contact2.Text = Convert.ToString(dRow["Contact2"]);
                        lbl_Contact3.Text = Convert.ToString(dRow["Contact3"]);
                        lbl_Language.Text = Convert.ToString(dRow["LanguageSpeak"]);
                        lbl_Bond.Visible = (dRow["bondflag"].ToString() == "1" ? true : false);
                        lblNotes.Text = dRow["GeneralComments"].ToString();

                        if (Call == clscalls.CallType.ReminderCall)
                        {
                            hdnStatus.Value = Convert.ToString(dRow["ReminderCallStatus"]);
                            lblComment.Text = Convert.ToString(dRow["ReminderComments"]);
                            td_comments.Style.Add("Display", "none");
                            divNotes.Style.Add("Display", "none");
                        }
                        else if (Call == clscalls.CallType.SetCall)
                        {
                            hdnStatus.Value = Convert.ToString(dRow["SetCallStatus"]);
                            lblComment.Text = Convert.ToString(dRow["SetCallComments"]);
                            td_comments.Style.Add("Display", "none");
                            divNotes.Style.Add("Display", "none");

                        }
                        // Zahoor 3977 8/18/2008
                        else if (Call == clscalls.CallType.FTACall)
                        {
                            hdnStatus.Value = Convert.ToString(dRow["FTACallStatus"]);
                            lblComment.Text = Convert.ToString(dRow["FTACallComments"]);

                            td_comments.Style.Add("Display", "block");
                            divNotes.Style.Add("Display", "block");
                            //lblgen_comments.Visible = true;
                        }

                        //Nasir 5690 03/25/2009 for CallType HMCAWDLQCalls
                        else if (Call == clscalls.CallType.HMCAWDLQCalls)
                        {
                            hdnStatus.Value = Convert.ToString(dRow["HMCAWDLQCallStatus"]);

                            tr_Ccomments.Style.Add("Display", "none");


                            td_comments.Style.Add("Display", "block");
                            divNotes.Style.Add("Display", "block");
                            ddl_rStatus.Items.RemoveAt(4);
                            ddl_rStatus.Items.RemoveAt(4);
                            //lblgen_comments.Visible = true;
                        }

                        if (lblNotes.Text == "" || lblNotes.Text == string.Empty)
                        {
                            divNotes.Style.Add("Display", "none");
                            //divgComments.Style.Add("Display", "block");
                        }
                        //Ended 3977

                        if (lblComment.Text == "")
                        {
                            div1.Style.Add("Display", "none");
                        }
                        ddl_rStatus.SelectedValue = Convert.ToString(hdnStatus.Value);
                        hdnStatusName.Value = ddl_rStatus.SelectedItem.ToString();



                    }
                    //Modified By Fahad 1370 ( 1 - 29 - 2008 )
                    // Comment Control has been added 
                    //WCC_ContactComments.Initialize(TicketID, EmpID, 3);
                    //WCC_ContactComments.Button_Visible = true;
                    //WCC_ContactComments.btn_AddComments.Visible = true;

                    //End Fahad
                    // filling the firm & according to firm contact nos are displayed
                    if (lbl_Firm.Text != "SULL")
                    {
                        lbl_outsidefirm.Visible = true;
                        lbl_Firm.Visible = true;
                    }
                    // there is no cantact no; formatting of page
                    if (lbl_contact1.Text.StartsWith("(") &&
                        lbl_Contact2.Text.StartsWith("(") &&
                        lbl_Contact3.Text.StartsWith("("))
                    {
                        lbl_contact1.Text = "No contact";
                        lbl_Contact2.Visible = false;
                        lbl_Contact3.Visible = false;
                    }
                    else
                    {
                        if (lbl_contact1.Text.StartsWith("(") || lbl_contact1.Text.StartsWith("000"))
                        {
                            lbl_contact1.Visible = false;
                        }
                        if (lbl_Contact2.Text.StartsWith("(") || lbl_Contact2.Text.StartsWith("000"))
                        {
                            lbl_Contact2.Visible = false;
                        }
                        if (lbl_Contact3.Text.StartsWith("(") || lbl_Contact3.Text.StartsWith("000"))
                        {
                            lbl_Contact3.Visible = false;
                        }
                    }
                }
                //End Fahad
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_update.Click += new System.EventHandler(this.btn_update_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion

        protected void btn_update_Click(object sender, System.EventArgs e)
        {	// Update procedure
            try
            {
                bool closeSelf = false;

                if (Session["Updatereminder"] != null && Convert.ToString(Session["Updatereminder"]) == "1")
                {
                    //HttpContext.Current.Response.Write(reloadCode);

                    if (litScript.Text == "")
                    {
                        litScript.Text = reloadCode;
                    }


                    Session["Updatereminder"] = null;
                    return;
                }

                // Noufil 3498 04/05/2008 check weather selected dropdown value is wrong number to show popup screen
                if (ddl_rStatus.SelectedValue == "4" && hdnFlag.Value == "1" && (Call == clscalls.CallType.SetCall || Call == clscalls.CallType.ReminderCall))
                {
                    //Rab Nawaz 8908 02/10/2011 Reminder Call report has been added.
                    if(Call == clscalls.CallType.SetCall)
                    {
                        lblmessageshow.Text = "Are you sure you want to activate this Flag and send a letter to the client requesting for updated contact information?";
                    }
                    //Rab Nawaz 8908 02/10/2011 Reminder Call report has been added.
                    else if (Call == clscalls.CallType.ReminderCall)
                    {
                        lblmessageshow.Text = "Are you sure you want to activate this Flag?";  
                    }

                    this.ModalPopupExtender1.Show();
                    return;
                }
                else if ((hdnStatus.Value != ddl_rStatus.SelectedValue))
                {//Nasir 5690 03/25/2009 add HMCAWDLQCalls
                    if (Call == clscalls.CallType.FTACall || this.txt_RComments.Text.Length > 0 || Call == clscalls.CallType.HMCAWDLQCalls)
                    {
                        //Noufil task 3498 04/08/2008
                        // for remindercalls
                        // Zahoor 3977 8/18/2008: General comments parameter addition (txt_Notes.Text)
                        //Zeeshan Ahmed 4837 09/23/2008 Send TicketViolationId's In The Procedure
                        clscall.InsertStatus(this.txt_RComments.Text, Convert.ToInt32(ddl_rStatus.SelectedValue), TicketID, EmpID, Convert.ToInt32(Call), this.txt_Notes.Text, Convert.ToString(ViewState["TicketViolationIDs"]));

                        //lblMessage.Text = "Reminder comments and status updated!";   
                        string description = string.Empty;

                        if (Call == clscalls.CallType.ReminderCall)
                        {
                            description = "Reminder ";
                        }
                        else if (Call == clscalls.CallType.SetCall)
                        {
                            description = "Set ";
                        }

                        else if (Call == clscalls.CallType.FTACall)
                        {
                            description = "FTA";
                        }
                        //Nasir 5690 03/25/2009 for CallType HMCAWDLQCalls
                        else if (Call == clscalls.CallType.HMCAWDLQCalls)
                        {
                            description = "HMCAWDLQ";
                        }

                        description += "Call Status: Changed from " + hdnStatusName.Value + " To " + ddl_rStatus.SelectedItem.ToString();
                        log.AddNote(EmpID, description, "", TicketID);
                        if (litScript.Text == "")
                        {
                            litScript.Text = reloadCode;
                        }
                        closeSelf = false;
                    }
                    else
                    {
                        closeSelf = true;
                    }
                }

                else if ((hdnStatus.Value == ddl_rStatus.SelectedValue))
                {
                    //Nasir 5690 03/25/2009 for CallType HMCAWDLQCalls
                    if (Call == clscalls.CallType.FTACall || this.txt_RComments.Text.Length > 0 || Call == clscalls.CallType.HMCAWDLQCalls)
                    {
                        // Zahoor 3977 8/18/2008: General comments parameter addition (txt_Notes.Text)
                        //Zeeshan Ahmed 4837 09/23/2008 Send TicketViolationId's In The Procedure
                        clscall.InsertStatus(this.txt_RComments.Text, Convert.ToInt32(ddl_rStatus.SelectedValue), TicketID, EmpID, Convert.ToInt32(Call), this.txt_Notes.Text, Convert.ToString(ViewState["TicketViolationIDs"]));
                        //HttpContext.Current.Response.Write(reloadCode);
                        if (litScript.Text == "")
                        {
                            litScript.Text = reloadCode;
                        }
                        closeSelf = false;
                    }
                    else
                    {
                        closeSelf = true;
                    }
                }

                //else
                if (closeSelf)
                {
                    litScript.Text = "<script language='javascript'>  self.close();   </script>";
                }
                Session["Updatereminder"] = "1";
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void GetReminderStatus()
        {
            try
            {
                //Noufil task 3498 04/08/2008 clscalls class use
                DataTable dtStatus = clscall.GetReminderStatus();

                if (dtStatus.Rows.Count > 0)
                {
                    ddl_rStatus.Items.Clear();
                    foreach (DataRow dr in dtStatus.Rows)
                    {
                        //Farrukh 10367 09/04/2012 Excluding SMS Reply Confirmed & Autodialer-Confirmed Status
                        if (int.Parse(dr["Reminderid_PK"].ToString()) != 7 && int.Parse(dr["Reminderid_PK"].ToString()) != 8)
                        {
                            ListItem item = new ListItem(dr["Description"].ToString(), dr["Reminderid_PK"].ToString());
                            ddl_rStatus.Items.Add(item);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        // Noufil 3498 04/05/2008 change the status to wrongnumber if wrong number is selected fron dropdown
        protected void btnok_Click(object sender, EventArgs e)
        {

            try
            {
                //Rab Nawaz 8908 02/10/2011 Reminder Call report has been added.
                if (Call == clscalls.CallType.SetCall || Call == clscalls.CallType .ReminderCall)
                {
                    ClsFlags cflags = new ClsFlags();
                    cflags.TicketID = this.TicketID;
                    cflags.FlagID = Convert.ToInt32(FlagType.WrongNumber);
                    cflags.EmpID = this.EmpID;
                    string description = "";
                    if (!cflags.HasFlag())
                    {
                        cflags.AddNewFlag();
                    }
                    // Zahoor 3977 8/18/2008: General comments parameter addition (txt_Notes.Text)
                    //Zeeshan Ahmed 4837 09/23/2008 Send TicketViolationId's In The Procedure
                    if (Call == clscalls.CallType.SetCall)
                    {
                        clscall.InsertStatus(this.txt_RComments.Text, Convert.ToInt32(ddl_rStatus.SelectedValue),
                                             TicketID, EmpID, Convert.ToInt32(clscalls.CallType.SetCall),
                                             this.txt_Notes.Text, Convert.ToString(ViewState["TicketViolationIDs"]));
                        description = "Set ";
                    }
                    //Rab Nawaz 8908 02/10/2011 Reminder Call report has been added.
                    if (Call == clscalls.CallType.ReminderCall)
                    {
                        clscall.InsertStatus(this.txt_RComments.Text, Convert.ToInt32(ddl_rStatus.SelectedValue),
                                              TicketID, EmpID, Convert.ToInt32(clscalls.CallType.ReminderCall),
                                              this.txt_Notes.Text, Convert.ToString(ViewState["TicketViolationIDs"]));
                        description = "Reminder "; 
                    }
                    //if (Call == clscalls.CallType.ReminderCall)
                    //{
                    //    description = "Reminder ";
                    //}
                    //else if (Call == clscalls.CallType.SetCall)
                    //{
                    //    description = "Set ";
                    //}
                    description += "Call Status: Changed from " + hdnStatusName.Value + " To " + ddl_rStatus.SelectedItem.ToString();
                    log.AddNote(EmpID, description, "", TicketID);
                    //Rab Nawaz 8908 02/10/2011 Need to send Set Call Letter to batch print only.
                    if (Call == clscalls.CallType.SetCall)
                    {
                        Hashtable htable = new Hashtable();
                        htable.Add(TicketID, TicketID);
                        clsbatch.SendLetterToBatch(htable, BatchLetterType.SetCallLetter, this.EmpID);
                    }
                    if (litScript.Text == "")
                    {
                        litScript.Text = reloadCode;
                    }
                }
            }

            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void ddl_rStatus_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

    }
}

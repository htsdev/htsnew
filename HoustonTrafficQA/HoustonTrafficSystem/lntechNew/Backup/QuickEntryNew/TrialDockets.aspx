<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>

<%@ Page Language="c#" CodeBehind="TrialDockets.aspx.cs" AutoEventWireup="false"
    Inherits="HTP.QuickEntryNew.TrialDockets" %>

<%@ Register Src="~/WebControls/AttorneyScheduler.ascx" TagName="AttorneyScheduler"
    TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
    <title>HTP Trial Docket</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <link href="../Styles.css" type="text/css" rel="stylesheet">

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>

    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">

    <script>
		
		
		
		
		function selectemail()
		{
		
		var WinSettings = "center:yes;resizable:no;dialogHeight:300px;dialogwidth:310px;scroll:yes"
        var Arg = 1;
        var val = window.showModalDialog("SelectEmails.aspx",Arg,WinSettings);
                             
            if ( val=="")
            {
              return false;
            }
            else
            {
            document.getElementById("hf_emails").value = val;
            return true;
            
           }
             
		
		}
		function OpenSingleCR()
		{
		
		var Courtloc = document.getElementById("DDLCourts").value;
		var Courtdate = document.getElementById("dtpCourtDate").value;
		var Courtnumber = document.getElementById("TxtCourtNo").value;
		var Datetype = document.getElementById("LstCaseStatus").value;
		window.open('../Reports/TrialDocketCR.aspx?Courtloc='+Courtloc+'&Courtdate=' + Courtdate + '&Page=1&Courtnumber='+ Courtnumber + '&Datetype=' + Datetype + '&singles=1','','fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=yes');
		return(false);
		}
		
		function OpenCRAll()
		{
		
		var Courtloc = document.getElementById("DDLCourts").value;
		var Courtdate = document.getElementById("dtpCourtDate").value;
		var Courtnumber = document.getElementById("TxtCourtNo").value;
		var Datetype = document.getElementById("LstCaseStatus").value;
		var showsdetail = document.getElementById("cbowesdetail").checked;
		window.open('../Reports/TrialDocketCR.aspx?Courtloc='+Courtloc+'&Courtdate=' + Courtdate + '&Page=1&Courtnumber='+ Courtnumber + '&Datetype=' + Datetype + '&singles=0&showowesdetail='+ showsdetail ,'','fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=yes');
		return(false);
		}
		
		
		
		
		function OpenPopUp(path, Val)  //(var path,var name)
		{
		 window.open(path,'',"height=300,width=400,resizable=yes, status=no,toolbar=no,scrollbars=yes,menubar=no,location=no");
		  //return true;
		}
		 //  Noufil 5772 04/14/2009 Validate Court Number
                function CheckName(name)
                { 
                    for ( i = 0 ; i < name.length ; i++)
                    {
                        var asciicode =  name.charCodeAt(i)
                        //If not valid alphabet 
                        if (!((asciicode >= 48 && asciicode <=57)))
                        return false;
                   }
                    return true;
                }
              function CheckCourtNumber()
              {      
                if(CheckName(document.getElementById("TxtCourtNo").value)==false)
                {
                    alert("Invalid Court Number : Please use number");
                    return false;
                }
              }
              
              
             //Nasir 6968 12/14/2009 add new method ShowProgressOnUpdate display loading on show setting click 
          	function ShowProgressOnUpdate()
	        { 
	        
	            //document.getElementById(tbl.id).style.display = 'block';
                //document.getElementById("btn_Update").style.display = 'none';                
                $find("MPELoding").show();
                $get('td_iframe').style.display = 'none';
                return false;
	        }
      
    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <aspnew:ScriptManager ID="ScriptManager2" runat="server" />
    <table id="MainTable" cellspacing="0" cellpadding="0" align="center" border="0" style="width: 780px">
        <tbody>
            <tr>
                <td style="width: 780px">
                    &nbsp;<uc1:ActiveMenu ID="ActiveMenu1" runat="server" />
                </td>
            </tr>
            <tr>
                <td style="height: 9px" width="780" background="../../images/separator_repeat.gif"
                    height="9">
                </td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" border="0" style="width: 780px" >
                        <tr height="58px">
                            <td style="width: 230px" valign = "middle">
                                <asp:DropDownList ID="DDLCourts" runat="server" CssClass="clsInputCombo" Width="220px"
                                    ToolTip="Court Location">
                                    <asp:ListItem Value="None">--Choose--</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td style="width: 100px">
                                <ew:CalendarPopup ID="dtpCourtDate" runat="server" Text=" " Width="80px" ImageUrl="../images/calendar.gif"
                                    SelectedDate="2006-03-14" EnableHideDropDown="True" ControlDisplay="TextBoxImage"
                                    CalendarLocation="Bottom" ShowGoToToday="True" AllowArbitraryText="False" Culture="(Default)"
                                    Nullable="True" UpperBoundDate="12/31/9999 23:59:00" PadSingleDigits="True" ToolTip="Trial Date"
                                    Font-Names="Tahoma" Font-Size="8pt">
                                    <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                    <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                        BackColor="White"></WeekdayStyle>
                                    <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                    <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                        BackColor="AntiqueWhite"></OffMonthStyle>
                                    <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                    <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                        BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                    <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                    <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                        BackColor="LightGray"></WeekendStyle>
                                    <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                    <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="White"></ClearDateStyle>
                                    <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                        BackColor="White"></HolidayStyle>
                                </ew:CalendarPopup>
                            </td>
                            <td style="width: 39px">
                                &nbsp;<asp:TextBox ID="TxtCourtNo" runat="server" CssClass="clsInputadministration"
                                    Width="30px" ToolTip="Court Room No"></asp:TextBox>
                            </td>
                            <td style="width: 97px">
                                &nbsp;<%--Sabir Khan 4527 08/25/2008 %>
                                        <%--<asp:DropDownList ID="DropDownList2" runat="server" CssClass="clsInputCombo">
                                        <asp:ListItem Value="0">Trial Docket</asp:ListItem>
                                        <asp:ListItem Value="1">Trial Results</asp:ListItem>
                                    </asp:DropDownList>--%>
                            </td>
                            <td style="width: 112px">
                                <asp:ListBox ID="LstCaseStatus" runat="server" CssClass="clsInputCombo" Height="48px"
                                    ToolTip="Court Status">
                                    <asp:ListItem Value="0" Selected="True">---Choose----</asp:ListItem>
                                    <asp:ListItem Value="2">Arraignement</asp:ListItem>
                                    <asp:ListItem Value="3">Pre Trial Setting</asp:ListItem>
                                    <asp:ListItem Value="5">JudgeTrial</asp:ListItem>
                                    <asp:ListItem Value="4">Jury Trail Setting</asp:ListItem>
                                    <asp:ListItem Value="52">Non Issue</asp:ListItem>
                                    <asp:ListItem Value="57">Scire</asp:ListItem>
                                    <asp:ListItem Value="51">Other</asp:ListItem>
                                </asp:ListBox>
                            </td>
                            <td>
                                &nbsp;<asp:CheckBox ID="cbowesdetail" runat="server" CssClass="clslabel" Text="$ Detail"
                                    Checked="true" />
                            </td>
                            <td style="width: 102px">
                                <asp:Button ID="Button_Submit" runat="server" CssClass="clsbutton" Text="Submit"
                                    OnClientClick="return CheckCourtNumber();"></asp:Button>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td background="../../images/separator_repeat.gif" height="9" style="width: 780px;
                    height: 9px">
                </td>
            </tr>
            <tr>
                <td>
                    <table id="Table1" cellspacing="1" cellpadding="1" border="0" style="width: 780px">
                        <tr>
                            <td align="left">
                                <asp:Label ID="lblRecStatus" runat="server" Font-Size="10px" ForeColor="Red" Visible="False"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 82px">
                                <table width="100%" cellspacing="1" cellpadding="1" border="1" style="border-collapse: collapse">
                                    <tr>
                                        <td align="center" style="font-size: 10pt; width: 34%;">
                                            <asp:ImageButton ID="ImageButton2" runat="server" OnClick="SendMail" ImageUrl="../Images/Email.jpg"
                                                ToolTip="Email" OnClientClick="return selectemail();" /><br />
                                            <font face="Arial" size="1">EMAIL</font>
                                        </td>
                                        <td align="center" style="font-size: 10pt; display: none">
                                            <asp:ImageButton ID="imgPrint_Ds_to_Printer" runat="server" ImageUrl="../Images/print_02.jpg"
                                                ToolTip="Print"></asp:ImageButton><br />
                                            <font face="Arial" size="1">HTML PRINT</font>
                                        </td>
                                        <%--<td align="center" style="font-size: 10pt">
                                            <asp:ImageButton ID="imgExp_Ds_to_Excel" runat="server" ImageUrl="../Images/excel_icon.gif"
                                                Width="81px" Height="36px" ToolTip="Excel"></asp:ImageButton><br />
                                            <font face="Arial" size="1">EXCEL</font>
                                        </td>--%>
                                        <td align="center" style="font-size: 10pt; width: 33%">
                                            <asp:ImageButton ID="btnPDFCourts" runat="server" ImageUrl="../Images/pdfall.jpg"
                                                OnClick="img_PDFCourt" ToolTip="Print All Court" Width="39px" />
                                            <asp:DropDownList ID="ddl_Copies" runat="server">
                                                <asp:ListItem>1</asp:ListItem>
                                                <asp:ListItem>2</asp:ListItem>
                                                <asp:ListItem>3</asp:ListItem>
                                                <asp:ListItem>4</asp:ListItem>
                                                <asp:ListItem Selected="True">5</asp:ListItem>
                                            </asp:DropDownList>
                                            <br />
                                            <font face="Arial" size="1">MULTIPLES</font>
                                        </td>
                                        <td align="center" style="font-size: 10pt; width: 33%">
                                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/pdfRM.JPG" ToolTip="Court Room"
                                                OnClick="Single_Click" />
                                            <br />
                                            <font face="Arial" size="1">SINGLES</font>
                                        </td>
                                        <%--<td align="center" style="font-size: 10pt; width: 90px;">
                                            <asp:ImageButton ID="Button_CR" runat="server" ImageUrl="~/Images/Cr.jpg" ToolTip="CrystalReport Version"
                                                Height="41px" OnClientClick="return OpenCRAll()" />&nbsp;&nbsp;<br />
                                            <font face="Arial" size="1">PDF</font>
                                        </td>--%>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="clssubhead" align="left" background="../Images/subhead_bg.gif" height="30">
                    <asp:LinkButton ID="lnk_ShowSetting" Text="Show Setting" runat="server"
                        OnClick="lnk_ShowSetting_Click" OnClientClick="ShowProgressOnUpdate();"></asp:LinkButton>
                </td>
            </tr>
            <tr style="font-size: 10pt">
                <td id="td_iframe" width="100%" runat="server" height="700" style="display: none">
                    &nbsp;
                    <iframe id="reportFrame" runat="server" tabindex="0" frameborder="1" scrolling="auto"
                        width="98%" height="99%"></iframe>
                </td>
            </tr>
            <tr style="font-size: 10pt">
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr style="font-size: 10pt">
                <td>
                    <table cellspacing="0" cellpadding="0" align="center" border="0" style="font-size: 10pt;
                        width: 780px;">
                        <tbody>
                            <tr>
                                <td style="width: 780px; height: 80px;">
                                    <table cellspacing="0" cellpadding="0" align="center" border="0" style="width: 780px">
                                        <tbody>
                                            <tr>
                                                <td width="780" background="../images/separator_repeat.gif" height="11">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="780">
                                                    <p>
                                                        <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                                                    </p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <asp:HiddenField ID="hf_emails" runat="server" />
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <aspnew:UpdatePanel ID="upnlcourt" runat="server">
                                    <ContentTemplate>
                                        <ajaxToolkit:ModalPopupExtender ID="MPEShowSettingPopup" runat="server" BackgroundCssClass="modalBackground"
                                            PopupControlID="pnlShowSettingPopup" TargetControlID="Button2">
                                        </ajaxToolkit:ModalPopupExtender>
                                        <%--CancelControlID="lnkbtnCancelPopUp"--%>
                                        <asp:Button ID="Button2" runat="server" Style="display: none" Text="Button" />
                                        <asp:Panel ID="pnlShowSettingPopup" runat="server" Width="550px">
                                            <uc1:AttorneyScheduler ID="AttorneySchedulerID" runat="server" />
                                        </asp:Panel>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp35:AsyncPostBackTrigger ControlID="lnk_ShowSetting" EventName="Click" />
                                    </Triggers>
                                </aspnew:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <ajaxToolkit:ModalPopupExtender ID="MPELoding" runat="server" BackgroundCssClass="modalBackground"
                        PopupControlID="Loading" TargetControlID="Button1">
                    </ajaxToolkit:ModalPopupExtender>
                    <asp:Button ID="Button1" runat="server" Style="display: none" />
                    <div id="Loading">
                        <img alt="Please wait" src="../Images/page_loading_ani.gif" />
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    </form>
</body>
</html>

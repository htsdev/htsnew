using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace HTP.QuickEntryNew
{
    public partial class PopUp : System.Web.UI.Page
    {
        #region Variables

        clsLogger bugTracker = new clsLogger();
        clsENationWebComponents ClsDb = new clsENationWebComponents();

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString.Count > 0)
                {
                    int Ticketid = Convert.ToInt32(Request.QueryString["ticketid"]);
                    BindGrid(Ticketid);
                }
            }
        }

        protected void dgdoc_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    string recordid = ((Label)e.Item.FindControl("lblrecordid")).Text.ToString();
                    //ozair 4073 05/26/2008
                    string rectype = ((Label)e.Item.FindControl("lbl_RecType")).Text.ToString();

                    if (rectype == "1")
                    {
                        string dbID = ((HyperLink)e.Item.FindControl("HP")).Text.ToString();
                        string sbID = recordid;
                        ((HyperLink)e.Item.FindControl("HP")).Attributes.Add("OnClick", "javascript:return PopUpShowPreviewDT(" + dbID + "," + sbID + ");");
                    }
                    else
                    {
                        ((HyperLink)e.Item.FindControl("HP")).Attributes.Add("onclick", "javascript:return PopUp(" + recordid.ToString() + ");");
                    }
                    //end ozair 4073
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        #endregion

        #region Methods

        private void BindGrid(int ticketid)
        {
            try
            {
                string[] key = { "@Ticketid" };
                object[] value = { ticketid };
                DataSet DS = ClsDb.Get_DS_BySPArr("usp_hts_GETBatchID", key, value);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    dgdoc.DataSource = DS;
                    dgdoc.DataBind();
                }

                else
                {
                    lblMessage.Text = "No Document Found";
                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        #endregion

    }


}

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace HTP.Reports
{
	/// <summary>
	/// Summary description for frmRptDocketUL.
	/// </summary>
	public partial class frmRptDocketULNew : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.DropDownList ddlCourtLoc;
		protected System.Web.UI.WebControls.DropDownList ddlCaseStatus;
		protected eWorld.UI.CalendarPopup dtFrom;
		protected System.Web.UI.WebControls.DataGrid dgDocket;
		protected System.Web.UI.WebControls.Button btnSubmit;
		clsENationWebComponents ClsDb = new clsENationWebComponents();
		protected eWorld.UI.CalendarPopup dtTo;
		clsSession cSession = new clsSession();
		clsLogger bugTracker = new clsLogger();
		DataView dv_Result;
        //Sabir Khan 4990 10/18/2008 Create object for Out side clients
        clsFirms ClsFirms = new clsFirms();
		string StrExp = String.Empty;
		protected System.Web.UI.WebControls.Label lblCurrPage;
		protected System.Web.UI.WebControls.DropDownList ddlPageNo;
		string StrAcsDec = String.Empty;
		protected System.Web.UI.WebControls.Label lblMsg;
		DataSet dsRes;
	
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            //Sabir Khan 5831 04/23/2009 Paging control has been implemented...
			//this.ddlPageNo.SelectedIndexChanged += new System.EventHandler(this.ddlPageNo_SelectedIndexChanged);
			//this.dgDocket.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dgDocket_PageIndexChanged);
			//this.dgDocket.SortCommand += new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.dgDocket_SortCommand);
			//this.dgDocket.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgDocket_ItemDataBound);
			this.Load += new System.EventHandler(this.Page_Load);
            Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
            Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
            GridViewSortExpression = "sortpaydate";
		}
		#endregion
        #region Properties

        public SortDirection GridViewSortDirection
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Ascending;
                return (SortDirection)ViewState["sortDirection"];
            }
            set { ViewState["sortDirection"] = value; }

        }
        public string GridViewSortExpression
        {
            get
            {
                if (ViewState["sortExpression"] == null)
                    ViewState["sortExpression"] = "";
                return ViewState["sortExpression"].ToString();
            }
            set { ViewState["sortExpression"] = value; }

        }

        #endregion
		
        private void Page_Load(object sender, System.EventArgs e)
		{
            //Waqas 5057 03/17/2009 Checking employee info in session               
            if (cSession.IsValidSession(this.Request, this.Response, this.Session) == false)
			{
				Response.Redirect("../frmlogin.aspx",false);
			}
			else //To stop page further execution
			{
				// Put user code to initialize the page here
                Pagingctrl.GridView = gv_Records;
                Pagingctrl.Visible = true;

				if (!IsPostBack)
				{
					dtTo.SelectedDate = DateTime.Today;
					dtFrom.SelectedDate  = DateTime.Today;
					
					FillCourtLocations();
					FillCaseStatuses();
                    //Sabir Khan 4990 10/18/2008 Fill Out side Firm drop down
                    FillFirms(ddl_FirmAbbreviation);
					//dtFrom.SelectedDate = null;
					//dtTo.Text = "";
					Search();
					btnSubmit.Attributes.Add("OnClick","return CalendarValidation();");
					btnSubmit.Attributes.Add("OnClick", "return validate();");
				}
                // Sabir Khan 5831 04/22/2009 
             
                
			}
		}

		private void FillCourtLocations()
		{
		
			DataSet ds = ClsDb.Get_DS_BySP("USP_HTS_GET_All_Courts");
			//int idx=0;

            //Yasir Kamal 5025 01/02/2009  �Non HMC/HCJP Cases� under Courts drop down box

            ddlCourtLoc.DataTextField = "shortcourtname";
            ddlCourtLoc.DataValueField = "courtid";
            ddlCourtLoc.DataSource = ds;
            ddlCourtLoc.DataBind();

            ListItem lst3 = new ListItem("Non HMC/HCJP Cases", "LL");
            ddlCourtLoc.Items.Insert(0, lst3);
            ListItem lst2 = new ListItem("All Outside Court", "JP");
            ddlCourtLoc.Items.Insert(0, lst2);
            ListItem lst1 = new ListItem("Houston Municipal Court", "IN");
            ddlCourtLoc.Items.Insert(0, lst1);
            ListItem lst = new ListItem("All Courts", "ALL");
            ddlCourtLoc.Items.Insert(0, lst);

                        
		}

		private void FillCaseStatuses()
		{
			DataSet ds = ClsDb.Get_DS_BySP("USP_HTS_GET_AllDateTypes");
/*
            int idx = 0;

            for (idx = 0; idx < ds.Tables[0].Rows.Count; idx++)
            {
                ddlCaseStatus.Items.Add(ds.Tables[0].Rows[idx]["Description"].ToString());
                ddlCaseStatus.Items[ddlCaseStatus.Items.Count - 1].Value = ds.Tables[0].Rows[idx]["TypeId"].ToString();
            }
            ListItem lst = new ListItem("All", "OPEN");
            ddlCaseStatus.Items.Insert(0, lst);
*/
            int idx=0;

			for (idx=-2; idx < ds.Tables[0].Rows.Count; idx++)
			{
				switch (idx)
				{
					case -2:
						ListItem lst = new ListItem("Open Cases","OPEN");
						ddlCaseStatus.Items.Add(lst);
						break;
                    //case -2:
                    //    ListItem lst1 = new ListItem("Waiting","WAIT");
                    //    ddlCaseStatus.Items.Add(lst1);
                    //    break;
					case -1:
						ListItem lst2 = new ListItem("Appearance Dates","APP");
						ddlCaseStatus.Items.Add(lst2);
						break;
                    //case 0:
                    //    ListItem lst3 = new ListItem("DAR/Case Missing","DAR");
                    //    ddlCaseStatus.Items.Add(lst3);
                    //    break;
					default:
						ddlCaseStatus.Items.Add(ds.Tables[0].Rows[idx]["Description"].ToString());
						ddlCaseStatus.Items[ddlCaseStatus.Items.Count -1].Value = ds.Tables[0].Rows[idx]["TypeId"].ToString();
						break;
				}
			}
 
		}

		private void btnSubmit_Click(object sender, System.EventArgs e)
		{
			try
			{	
				Search();
			}
			catch(Exception ex)
			{
				lblMsg.Text = ex.Message;
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}

		private void FillGrid()
		{
			try
			{			
				DataView dvTemp = (DataView)   cSession.GetSessionObject("dvResult",this.Session);
                gv_Records.Visible = true;
                dvTemp.Sort = GridViewSortExpression + " " + (GridViewSortDirection == SortDirection.Ascending ? "ASC" : "DESC");
                DataTable dt = dvTemp.ToTable();
                if ( dt.Rows.Count > 0 )
                {
                //Sabir Khan 5521 02/16/2009 Generate sno for Docket report ...
                generateSerialNo(dt);
                //Sabir Khan 5975 10/22/2009  added blank line for the following statuses...                   
                DataTable dt1;
                if (ddlCaseStatus.SelectedValue == "3" || ddlCaseStatus.SelectedValue == "4" || ddlCaseStatus.SelectedValue == "5" || ddlCaseStatus.SelectedValue == "9")
                {
                    dt1 = (DataTable)InsertBlankRows(dt);
                }
                else
                {
                    dt1 = dt;
                }

                
                //dgDocket.DataSource = 	dvTemp;
                //dgDocket.DataBind();	
                Pagingctrl.GridView = gv_Records;
                gv_Records.DataSource = dt1;
                gv_Records.DataBind();				
				//FillPageList();
				//SetNavigation();
                Pagingctrl.PageCount = gv_Records.PageCount;
                Pagingctrl.PageIndex = gv_Records.PageIndex;
                Pagingctrl.SetPageIndex();
                //if (dgDocket.Items.Count == 0)
                }
                else
                {
                    Pagingctrl.PageCount = 0;
                    Pagingctrl.PageIndex = 0;
                    Pagingctrl.SetPageIndex();                    
                    gv_Records.Visible = false;
                    lblMsg.Text = "No record found.";
                }
			}
			catch (Exception ex)
			{
                //if (dgDocket.CurrentPageIndex > dgDocket.PageCount -1)
                //{
                //    dgDocket.CurrentPageIndex = 0;
                //    dgDocket.DataBind();
                if (gv_Records.PageIndex > gv_Records.PageCount - 1)
                {
                    gv_Records.PageIndex = 0;
                    gv_Records.DataBind();

                    //SetNavigation();
				}
				else
				{
					lblMsg.Text = ex.Message;
					bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);

				}
			}

		}
		
		private void PullData()
		{
            // Noufil 4369 07/18/2008 Enable checkall option to get data from Store procedures
			//if (dtFrom.Text == "" && dtTo.Text == "" )			
            if ((dtFrom.SelectedDate.ToShortDateString() == "1/1/0001" && dtTo.SelectedDate.ToShortDateString() == "1/1/0001") || (chk_showall.Checked == true))
            {
                string[] key = { "@courtlocation", "@casestatus", "@firmid" };
                object[] value1 = { ddlCourtLoc.SelectedValue, ddlCaseStatus.SelectedValue,ddl_FirmAbbreviation.SelectedValue};
                dsRes = ClsDb.Get_DS_BySPArr("USP_HTS_Get_DocketReport_UL", key, value1);
            }
            else if (chk_showall.Checked == false)
            {
                //Sabir Khan 4990 10/18/2008 Add perameter @firmid for Out side clients
                string[] key = { "@courtlocation", "@casestatus", "@startcourtdate", "@endcourtdate" , "@firmid" };
                object[] value1 = { ddlCourtLoc.SelectedValue, ddlCaseStatus.SelectedValue, dtFrom.SelectedDate, dtTo.SelectedDate, Convert.ToInt32(ddl_FirmAbbreviation.SelectedValue) };
                dsRes = ClsDb.Get_DS_BySPArr("USP_HTS_Get_DocketReport_UL", key, value1);
            }

	
			DataSet dsResTemp;
			//if (ddlCaseStatus.SelectedValue == "3" || ddlCaseStatus.SelectedValue == "4" || ddlCaseStatus.SelectedValue == "5" || ddlCaseStatus.SelectedValue == "9" )
			//	dsResTemp = (DataSet)  InsertBlankRows(dsRes);
			//else
				dsResTemp = dsRes;           
			//cSession.SetSessionVariable("dvResult",dsResTemp.Tables[0].DefaultView,this.Session);
			cSession.SetSessionVariable("dvResult",dsResTemp.Tables[0].DefaultView,this.Session);
		}
        //Sabir Khan 5831 04/23/2009 Dategrid code commented...
        #region Comment Code
        //private void gv_Records_RowDataBound(object sender, System.Web.UI.WebControls.gri e)
        //{
        //    if(e.Item.ItemType == ListItemType.Header)
        //    {
        //        e.Item.Cells.RemoveAt(7);
        //        e.Item.Cells.RemoveAt(7);
        //        e.Item.Cells[6].ColumnSpan = 3;

        //        //e.Item.Cells.RemoveAt(6);
        //        //e.Item.Cells[5].ColumnSpan = 2;
        //    }
        //    else 
        //        if(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //        {		
        //        try
        //        {
        //            int iTicketId = (int)  (Convert.ChangeType(((Label)(e.Item.FindControl("lbl_TicketId"))).Text,typeof(int)));
					
        //            ((HyperLink) (e.Item.FindControl("hlk_LName"))).NavigateUrl = "../ClientInfo/ViolationFeeOld.aspx?search=0&casenumber="+ iTicketId ;
				
        //            // changing row color on mouse hover.....
        //            e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='#FFF2D9'");
        //            e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#EFF4FB'");
        //        }
        //        catch 
        //        {
        //            int idx=0;
        //            for(idx = e.Item.Cells.Count -1 ; idx> 0 ;idx--)
        //                e.Item.Cells.RemoveAt(idx);
        //            e.Item.Cells[0].ColumnSpan = 13;
        //            e.Item.Cells[0].BackColor = System.Drawing.Color.White;
        //            e.Item.Cells[0].Height = 10;
        //        }
        //        }
        //}

        //Sabir Khan 5521 02/16/2009 Generate Serial No...
        //public void generateSerialNo(DataTable dtRecords)
        //{
        //    int sno = 1;
        //    try
        //    {
        //    if (dtRecords.Columns.Contains("sno") == false)
        //    {
        //        dtRecords.Columns.Add("sno");
        //        //sno = 1;
        //    }
        //    if (dtRecords.Rows.Count >= 1)
        //        dtRecords.Rows[0]["sno"] = 1;

        //    if (dtRecords.Rows.Count >= 2)
        //    {
        //        for (int i = 1; i < dtRecords.Rows.Count; i++)
        //        {
        //            try
        //            {
        //                if (dtRecords.Rows[i - 1]["ticketid_pk"].ToString() != dtRecords.Rows[i]["ticketid_pk"].ToString())
        //                {
        //                    dtRecords.Rows[i]["sno"] = ++sno;
        //                }
        //            }
        //            catch
        //            {}
        //        }
        //    }
        //    }
        //   catch(Exception ex)
        //    {
        //       bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
        //       lblMsg.Text = ex.Message ;
        //    }
        //}
        #endregion
        public void generateSerialNo(DataTable dtRecords)
        {
            int sno = 1;
            if (!dtRecords.Columns.Contains("sno"))
                dtRecords.Columns.Add("sno");


            if (dtRecords.Rows.Count >= 1)
                dtRecords.Rows[0]["sno"] = 1;

            if (dtRecords.Rows.Count >= 2)
            {
                for (int i = 1; i < dtRecords.Rows.Count; i++)
                {
                    if (dtRecords.Rows[i - 1]["ticketid_pk"].ToString() != dtRecords.Rows[i]["ticketid_pk"].ToString())
                    {
                        dtRecords.Rows[i]["sno"] = ++sno;
                    }
                }
            }

        }
        //Sabir Khan 5831 04/23/2009 Dategrid code commented...
        #region Commentcode
        //private void GenerateSerial()
        //{
        //    // Procedure for writing serial numbers in "S.No." column in data grid.......	

			
        //    long sNo=(dgDocket.CurrentPageIndex)*(dgDocket.PageSize);									
        //    try
        //    {
        //        // looping for each row of record.............
        //        foreach (DataGridItem ItemX in dgDocket.Items) 
        //        { 					
					
        //            try
        //            {
        //                int iTicketId =  (int) (Convert.ChangeType(((Label) ( ItemX.FindControl("lbl_Ticketid"))).Text,typeof(int)))  ;
        //                sNo +=1 ;
        //                // setting text of hyper link to serial number after bouncing of hyper link...
        //                ((Label)(ItemX.FindControl("lblSerial"))).Text  = (string) Convert.ChangeType(sNo,typeof(string));
        //            }
        //            catch
        //            {}

				 
        //        }
        //    }
        //    catch(Exception ex)

        //    {
        //        bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);

        //        lblMsg.Text = ex.Message ;
        //    }		
        //}

        //private void dgDocket_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
        //{
        //    dgDocket.CurrentPageIndex = e.NewPageIndex;
        //    FillGrid();
        //}

        //private void SortGrid(string SortExp)
        //{
        //    try
        //    {
        //        SetAcsDesc(SortExp);
        //        dv_Result = (DataView) (cSession.GetSessionObject("dvResult",this.Session));
        //        dv_Result.Sort = StrExp + " " + StrAcsDec;
        //        cSession.SetSessionVariable("dvResult",dv_Result,this.Session);
        //        FillGrid();

        //    }
        //    catch(Exception ex)
        //    {
        //        lblMsg.Text = ex.Message;
        //        bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);

        //    }
        //}

        //private void SetAcsDesc(string Val)
        //{
        //    try
        //    {
        //        StrExp =  cSession.GetSessionVariable ("StrExp",this.Session);
        //        StrAcsDec = cSession.GetSessionVariable("StrAcsDec",this.Session);
        //    }
        //    catch
        //    {

        //    }

        //    if (StrExp == Val)
        //    {
        //        if (StrAcsDec == "ASC")
        //        {
        //            StrAcsDec = "DESC";
        //            cSession.SetSessionVariable("StrAcsDec",StrAcsDec, this.Session);
        //        }
        //        else 
        //        {
        //            StrAcsDec = "ASC";
        //            cSession.SetSessionVariable("StrAcsDec",StrAcsDec,this.Session);
        //        }
        //    }
        //    else 
        //    {
        //        StrExp = Val;
        //        StrAcsDec = "ASC";
        //        cSession.SetSessionVariable("StrExp", StrExp,this.Session) ;
        //        cSession.SetSessionVariable("StrAcsDec", StrAcsDec, this.Session) ;				
        //    }
        //}

        //private void dgDocket_SortCommand(object source, System.Web.UI.WebControls.DataGridSortCommandEventArgs e)
        //{
        //    lblMsg.Text="";
        //    SortGrid(e.SortExpression);
        //}

        //private void SetNavigation()
        //{
        //    // Procedure for setting data grid's current  page number on header ........
			
        //    try
        //    {
        //        // setting current page number
        //        lblCurrPage.Text =Convert.ToString(dgDocket.CurrentPageIndex+1);

        //        for (int idx =0 ; idx < ddlPageNo.Items.Count ;idx++)
        //        {
        //            ddlPageNo.Items[idx].Selected = false;
        //        }
        //        ddlPageNo.Items[dgDocket.CurrentPageIndex].Selected=true;
			
        //    }
        //    catch (Exception ex)
        //    {
        //        lblMsg.Text = ex.Message ;
        //        bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);

        //    }
        //}

        //private void ddlPageNo_SelectedIndexChanged(object sender, System.EventArgs e)
        //{
        //    dgDocket.CurrentPageIndex = ddlPageNo.SelectedIndex;
        //    FillGrid();
        //}

        //private void FillPageList()
        //{
        //    try
        //    {
        //        int i =0;
        //        ddlPageNo.Items.Clear();
        //        for(i=1; i<=dgDocket.PageCount;i++ )
        //        {
        //            ddlPageNo.Items.Add("Page - " + i.ToString());
        //            ddlPageNo.Items[i-1].Value = i.ToString();
        //        }
        //    }
        //    catch(Exception ex)
        //    {
        //        lblMsg.Text = ex.Message;
        //        bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);

        //    }

        //}
        #endregion
        private void Search()
		{
			lblMsg.Text="";
            //dgDocket.CurrentPageIndex = 0;
            gv_Records.PageIndex = 0;
			PullData();
			FillGrid();
		}

        // Sabir Khan 5975 10/22/2009 Return type and Arguments have been changed from dataset into datatable...
        /// <summary>
        /// This method is used to add blank row into data table when a court date of a record is different from previouse records 
        /// </summary>
        /// <param name="dsTemp"></param>
        /// <returns>Data Table</returns>
        private DataTable InsertBlankRows(DataTable dsTemp)
        {
            int idx = 1;
            int iRows = dsTemp.Rows.Count - 1;


            // inserting line break after each ticketid....
            for (idx = 1; idx < iRows; idx++)
            {
                //Fahad 5975 Comment below line and add line containing Substring method to remove the Exception
                //DateTime fDate =  Convert.ToDateTime(dsTemp.Tables[0].Rows[idx]["courtdatemain"]) ;
                DateTime fDate = Convert.ToDateTime(dsTemp.Rows[idx]["courtdatemain"].ToString().Substring(0, dsTemp.Rows[idx]["courtdatemain"].ToString().IndexOf("@") - 1));
                //Fahad 5975 Comment below line and add line containing Substring method to remove the Exception
                //DateTime lDate = Convert.ToDateTime(dsTemp.Tables[0].Rows[idx - 1]["courtdatemain"]) ;
                DateTime lDate = Convert.ToDateTime(dsTemp.Rows[idx - 1]["courtdatemain"].ToString().Substring(0, dsTemp.Rows[idx - 1]["courtdatemain"].ToString().IndexOf("@") - 1));
                if (fDate.ToShortDateString() != lDate.ToShortDateString())
                {
                    DataRow dr = dsTemp.NewRow();
                    dsTemp.Rows.InsertAt(dr, idx);
                    iRows++;
                    idx++;
                }
            }
            return dsTemp;
        }
        //private DataSet InsertBlankRows(DataSet dsTemp)
        //{	
        //    int idx=1;
        //    int iRows = dsTemp.Tables[0].Rows.Count - 1;
			

        //    // inserting line break after each ticketid....
        //    for (idx=1; idx < iRows; idx++)
        //    {
        //        //Fahad 5975 Comment below line and add line containing Substring method to remove the Exception
        //        //DateTime fDate =  Convert.ToDateTime(dsTemp.Tables[0].Rows[idx]["courtdatemain"]) ;
        //        DateTime fDate = Convert.ToDateTime(dsTemp.Tables[0].Rows[idx]["courtdatemain"].ToString().Substring(0, dsTemp.Tables[0].Rows[idx]["courtdatemain"].ToString().IndexOf("@") - 1));
        //        //Fahad 5975 Comment below line and add line containing Substring method to remove the Exception
        //        //DateTime lDate = Convert.ToDateTime(dsTemp.Tables[0].Rows[idx - 1]["courtdatemain"]) ;
        //        DateTime lDate = Convert.ToDateTime(dsTemp.Tables[0].Rows[idx - 1]["courtdatemain"].ToString().Substring(0, dsTemp.Tables[0].Rows[idx - 1]["courtdatemain"].ToString().IndexOf("@") - 1));
        //        if ( fDate.ToShortDateString() != lDate.ToShortDateString() )
        //        {
        //            DataRow dr = dsTemp.Tables[0].NewRow();
        //            dsTemp.Tables[0].Rows.InsertAt(dr,idx);
        //            iRows++;
        //            idx++;
        //        }
        //    }
        //    return dsTemp;
        //}
        //Sabir Khan 4990 10/18/2008 Fill Out side Firm drop down
        private void FillFirms(DropDownList ddl_FirmAbbreviation)
        {
            try
            {
                DataSet ds_Firms = ClsFirms.GetActiveFirms(0);
                ddl_FirmAbbreviation.DataSource = ds_Firms.Tables[0];
                ddl_FirmAbbreviation.DataTextField = "FirmAbbreviation";
                ddl_FirmAbbreviation.DataValueField = "FirmID";
                ddl_FirmAbbreviation.DataBind();
                ddl_FirmAbbreviation.SelectedValue = "3000";
                ddl_FirmAbbreviation.Items.Insert(0, new ListItem("-ALL Firms-", "0"));
                ddl_FirmAbbreviation.SelectedValue = "0";
            }
            catch (Exception ex)
            {
               
                lblMsg.Visible = true;
                lblMsg.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        //Sabir Khan 5831 04/23/2009 implement gridview...
        protected void gv_Records_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            
            
                    try
                    {
                       
                        int iTicketId = (int)(Convert.ChangeType(((Label)(e.Row.FindControl("lbl_TicketId"))).Text, typeof(int)));

                        ((HyperLink)(e.Row.FindControl("hlk_LName"))).NavigateUrl = "../ClientInfo/ViolationFeeOld.aspx?search=0&casenumber=" + iTicketId;

                        // changing row color on mouse hover.....
                        e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#FFF2D9'");
                        e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='#EFF4FB'");
                    }
                    catch
                    {
                                               
                            e.Row.Cells[0].Height = 10;
                          
                        
                    }
                
        }

       
        protected void gv_Records_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                string sortExpression = e.SortExpression;

                if (GridViewSortDirection == SortDirection.Ascending && sortExpression == GridViewSortExpression)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                else
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }

                GridViewSortExpression = sortExpression;
                FillGrid();
                Pagingctrl.PageCount = gv_Records.PageCount;
                Pagingctrl.PageIndex = gv_Records.PageIndex;
            }
            catch (Exception ex)
            {
                lblMsg.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void gv_Records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_Records.PageIndex = e.NewPageIndex;
                FillGrid();
            }
            catch (Exception ex)
            {

                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        void Pagingctrl_PageIndexChanged()
        {
            gv_Records.PageIndex = Pagingctrl.PageIndex - 1;
            FillGrid();
        }
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            if (pageSize > 0)
            {
                gv_Records.PageIndex = 0;
                gv_Records.PageSize = pageSize;
                gv_Records.AllowPaging = true;
            }
            else
            {
                gv_Records.AllowPaging = false;
            }
            FillGrid();
        }

        
  
	/*	private DataView InsertBlankRows(DataView dvTemp)
		{	
			int idx=1;
			int iRows = dvTemp.Table.Rows.Count - 1;
			

			// inserting line break after each ticketid....
			for (idx=1; idx < iRows; idx++)
			{
				DateTime fDate =  Convert.ToDateTime(dvTemp.Table.Rows[idx]["courtdatemain"]) ;
				DateTime lDate = Convert.ToDateTime(dvTemp.Table.Rows[idx - 1]["courtdatemain"]) ;
				if ( fDate.ToShortDateString() != lDate.ToShortDateString() )
				{
					DataRow dr = dvTemp.Table.NewRow();
					dvTemp.Table.Rows.InsertAt(dr,idx);
					iRows++;
					idx++;
				}
			}
			return dvTemp;
		}
  */
	}
}

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;
using HTP.WebControls;
using FrameWorkEnation.Components;

namespace HTP.QuickEntryNew
{
    public partial class frmRptCourtArrignmentAppearanceAlert : System.Web.UI.Page
    {
        // Fahad Muhammad Qureshi 5722 04/02/2009  grouping variables       
        #region Variable

        ValidationReports report = new ValidationReports();
        clsSession ClsSession = new clsSession();
        clsLogger clog = new clsLogger();
        clsCase cCase = new clsCase();
        string StrExp = String.Empty;   // grid sorting expression variable declaration.
        string StrAcsDec = String.Empty;    // grid sorting type variable decalation.
        DataSet DS; // data set variable for poplulating data into it.
        DataView dvBR; // data view variable for storing data view in to session.

        #endregion

        // Fahad Muhammad Qureshi 5722 04/02/2009  grouping Events Handler       
        #region Event Handler
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else
                {
                    if (!IsPostBack)
                    {
                        BindData(); // binding data in grid.
                    }
                    Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
                    UpdateFollowUpInfo2.PageMethod += new HTP.WebControls.PageMethodHandler(UpdateFollowUpInfo2_PageMethod);
                    Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
                    Pagingctrl.GridView = gv_records;
                    ViewState["empid"] = Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request));
                }
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                lbl_message.Text = ex.ToString();

            }

        }

        protected void gv_records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_records.PageIndex = e.NewPageIndex;
                BindData();
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                lbl_message.Text = ex.ToString();

            }

        }


        protected void gv_records_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "btnclick")
                {

                    int RowID = Convert.ToInt32(e.CommandArgument);
                    string lastname = ((Label)gv_records.Rows[RowID].Cells[1].FindControl("lbl_lastname")).Text;
                    string firstname = ((Label)gv_records.Rows[RowID].Cells[2].FindControl("lbl_firstname")).Text;
                    string causeno = ((Label)gv_records.Rows[RowID].Cells[3].FindControl("lbl_causenum")).Text;
                    string ticketno = ((Label)gv_records.Rows[RowID].Cells[4].FindControl("lbl_ticketnumber")).Text;
                    string ticketid = ((HiddenField)gv_records.Rows[RowID].Cells[0].FindControl("hf_TicketID")).Value;
                    string court = "Non Hmc";
                    string followupDate = (((Label)gv_records.Rows[RowID].FindControl("lbl_FollowUpD")).Text);
                    string nextfollowupDate = (((HiddenField)gv_records.Rows[RowID].FindControl("hf_NextFollowUpdate")).Value);
                    cCase.TicketID = Convert.ToInt32(ticketid);
                    string commments = cCase.GetGeneralCommentsByTicketId();
                    UpdateFollowUpInfo2.Freezecalender = true;
                    UpdateFollowUpInfo2.Title = "Non HMC Follow Up Date";
                    UpdateFollowUpInfo2.followUpType = HTP.Components.FollowUpType.NonHMCFollowUpDate;
                    DateTime nextfollow = (followupDate == string.Empty) ? Convert.ToDateTime("01/01/1900") : Convert.ToDateTime(followupDate);
                    UpdateFollowUpInfo2.binddate(Convert.ToDateTime(nextfollowupDate), nextfollow, firstname, lastname, ticketno, causeno, commments, mpeTrafficwaiting.ClientID, court, Convert.ToInt32(ticketid));
                    mpeTrafficwaiting.Show();
                    Pagingctrl.Visible = true;
                }
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                lbl_message.Text = ex.ToString();
            }
        }
        protected void gv_records_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ((LinkButton)e.Row.FindControl("img_Add")).CommandArgument = e.Row.RowIndex.ToString();
                    pnlFollowup.Style["display"] = "none";
                    Label followup = (Label)e.Row.FindControl("lbl_followup");

                }
            }

            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                lbl_message.Text = ex.ToString();
            }

        }
        //protected void btn_search_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        BindData();
        //    }
        //    catch (Exception ex)
        //    {
        //        clsLogger.ErrorLog(ex);
        //        lbl_message.Text = ex.ToString();
        //    }

        //}
        protected void gv_records_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                SortGrid(e.SortExpression);
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                lbl_message.Text = ex.ToString();
            }

        }

        #endregion

        // Fahad Muhammad Qureshi 5722 04/02/2009  grouping Methods 
        #region Methods
        /// <summary>
        /// Fahad 5722 04/02/2009
        /// For changing the page control index
        /// </summary>
        protected void Pagingctrl_PageIndexChanged()
        {

            try
            {
                DataTable dtView;// decalring data table local varibale 
                dvBR = (DataView)Session["dvBR"];// getting data view from sessions 
                dtView = dvBR.ToTable();  // creating new instance of data table.
                gv_records.PageIndex = Pagingctrl.PageIndex - 1;  // set page index to grid.
                gv_records.DataSource = dtView; // set grid data source
                gv_records.DataBind();  // bind grid.
                Pagingctrl.PageCount = gv_records.PageCount;
                Pagingctrl.PageIndex = gv_records.PageIndex;
                Pagingctrl.SetPageIndex();
                Session["dvBR"] = dtView.DefaultView;// assing default view into session.
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                lbl_message.Text = ex.ToString();
            }

        }

        /// <summary>
        /// Fahad 5722 04/02/2009
        /// Maintained the page size
        /// </summary>
        /// <param name="pageSize"></param>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            if (pageSize > 0)
            {
                gv_records.PageIndex = 0;
                gv_records.PageSize = pageSize;
                gv_records.AllowPaging = true;
            }
            else
            {
                gv_records.AllowPaging = false;
            }
            BindData();
        }

        /// <summary>
        /// Fahad 5722 04/02/2009
        /// Maintained the data in update followUp date Control
        /// </summary>
        void UpdateFollowUpInfo2_PageMethod()
        {
            BindData();
        }

        /// <summary>
        /// Fahad 5722 04/02/2009
        /// Bind Grid to the data 
        /// </summary>

        public void BindData()
        {
            try
            {
                string[] Keys = { "@ShowAll" };
                object[] Values = { chkShowAllUserRecords.Checked };
                report.getRecords("USP_HTP_Get_CourtArrignmantAppearance_Report", Keys, Values); // populating data from data grid.                
                DS = report.records;   // referencing dataset.
                generateSerialNo(DS.Tables[0]); // generating serial number for data grid.
                Pagingctrl.GridView = gv_records;
                gv_records.DataSource = DS.Tables[0];
                gv_records.DataBind();
                Pagingctrl.PageCount = gv_records.PageCount;
                Pagingctrl.PageIndex = gv_records.PageIndex;
                Pagingctrl.SetPageIndex();
                if (DS.Tables[0].Rows.Count > 0)
                {
                    if (Session[""] != null) Session.Clear();
                    dvBR = new DataView(DS.Tables[0]);
                    Session["dvBR"] = dvBR;
                    this.lbl_message.Text = "";
                }
                else
                {
                    this.lbl_message.Text = "No records found";
                }

            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                lbl_message.Text = ex.ToString();
            }


        }

        /// <summary>
        /// Fahad 5722 04/02/2009
        /// Sort the Grid according to Requirement
        /// </summary>
        /// <param name="SortExp"></param>
        private void SortGrid(string SortExp)
        {
            DataTable dtView;

            try
            {
                SetAcsDesc(SortExp);
                dvBR = (DataView)Session["dvBR"];
                dvBR.Sort = StrExp + " " + StrAcsDec;
                dtView = dvBR.ToTable();
                generateSerialNo(dtView);
                gv_records.DataSource = dtView;
                gv_records.DataBind();
                Pagingctrl.PageCount = gv_records.PageCount;
                Pagingctrl.PageIndex = gv_records.PageIndex;
                Pagingctrl.SetPageIndex();
                Session["dvBR"] = dtView.DefaultView;

            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                lbl_message.Text = ex.ToString();
            }

        }

        /// <summary>
        /// Fahad 5722 04/02/2009
        /// Sense the sorting either Ascending OR Descending in Order
        /// </summary>
        /// <param name="Val"></param>
        private void SetAcsDesc(string Val)
        {
            try
            {
                StrExp = Session["StrExp"].ToString();
                StrAcsDec = Session["StrAcsDec"].ToString();
            }
            catch
            {

            }

            if (StrExp == Val)
            {
                if (StrAcsDec == "ASC")
                {
                    StrAcsDec = "DESC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
                else
                {
                    StrAcsDec = "ASC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
            }
            else
            {
                StrExp = Val;
                StrAcsDec = "ASC";
                Session["StrExp"] = StrExp;
                Session["StrAcsDec"] = StrAcsDec;
            }
        }

        /// <summary>
        /// Fahad 5722 04/02/2009
        /// Generate the serial no on the basis of Ticket Id
        /// </summary>
        /// <param name="dtRecords"></param>
        public void generateSerialNo(DataTable dtRecords)
        {
            int sno = 1;
            if (dtRecords.Columns.Contains("sno") == false)
            {
                dtRecords.Columns.Add("sno");
            }

            if (dtRecords.Rows.Count >= 1)
                dtRecords.Rows[0]["sno"] = 1;

            if (dtRecords.Rows.Count >= 2)
            {
                for (int i = 1; i < dtRecords.Rows.Count; i++)
                {
                    if (dtRecords.Rows[i - 1]["TICKETID_PK"].ToString() != dtRecords.Rows[i]["TICKETID_PK"].ToString())
                    {
                        dtRecords.Rows[i]["sno"] = ++sno;
                    }
                }
            }

        }

        #endregion

        protected void chkShowAllUserRecords_CheckedChanged(object sender, EventArgs e)
        {
            BindData();
        }




    }
}

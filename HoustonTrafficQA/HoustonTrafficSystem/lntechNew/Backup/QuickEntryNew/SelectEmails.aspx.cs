using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;

namespace lntechNew.QuickEntryNew
{
    public partial class SelectEmails : System.Web.UI.Page
    {

        clsENationWebComponents ClsDB = new clsENationWebComponents();
               
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

                try
                {
                    DataSet ds_emails = ClsDB.Get_DS_BySP("USP_HTS_GetAttorneyAddress");

                    if (ds_emails.Tables[0].Rows.Count > 0)
                    {
                        hf_totalrec.Value = ds_emails.Tables[0].Rows.Count.ToString();
                        gv_emails.DataSource = ds_emails.Tables[0];
                        gv_emails.DataBind();
                    }
                    else
                    {
                        hf_totalrec.Value = "0";
                    }
                             
                }
                catch
                { }


            }

        }
    }
}

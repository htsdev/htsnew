﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;
using MetaBuilders.WebControls;
using HTP.Components;


namespace HTP.QuickEntryNew
{
    // Noufil 4461 09/01/2008 Get Visitor records from Public site and online consultation records from Public site
    // Noufil 5524 02/16/2009 Remove Online Inquiries Grid to a separate WebPage.

    public partial class SullowlawRemindercall : System.Web.UI.Page
    {
        clsSession ClsSession = new clsSession();
        clscalls clscall = new clscalls();

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (ClsSession.IsValidSession(this.Request) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else
                {
                    if (!IsPostBack)
                    {
                        //if ((Request.QueryString["recdate"] != null ) && (Request.QueryString["id"] != null))
                        //{
                        //    cb_showall.Checked = false;
                        //    calStartDate.SelectedDate = DateTime.Today;
                        //    calenddate.SelectedDate = DateTime.Today;
                        //    rb_report.SelectedIndex = 1;
                        //    GetReminderStatus(dd_callback, 1);
                        //    dd_callback.SelectedIndex = 1;
                        //    GetReminderStatus(ddl_onlinecallback, 0);
                        //    GetReminderStatus(ddl_rStatus, 0);
                        //    FillGrid();
                        //}
                        //else
                        //{
                        ViewState["empid"] = Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request));
                        calStartDate.SelectedDate = DateTime.Today;
                        //GetReminderStatus(dd_callback, 1);
                        // GetReminderStatus(ddl_onlinecallback, 0);
                        GetReminderStatus(dd_callback, 1);
                        GetReminderStatus(ddl_rStatus, 0);
                        //Asif Ghouri 4858 09/25/2008 By Default Display Records on the report
                        FillGrid();
                        //}
                    }
                    Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
                    Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
                    Pagingctrl.GridView = dg_ReminderCalls;
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
            }
        }

        private void FillGrid() // Filling the Grid
        {
            try
            {
                DataTable dtReminderCalls = SullolawReports.GetOnlineTrackingReport(1, Convert.ToInt32(cb_showall.Checked), calStartDate.SelectedDate, calenddate.SelectedDate, Convert.ToInt32(dd_callback.SelectedValue.ToString()));
                int serialnumber = 1;

                if (dtReminderCalls.Rows.Count > 0)
                {
                    if (!dtReminderCalls.Columns.Contains("Sno"))
                    {
                        dtReminderCalls.Columns.Add("Sno");
                    }
                    for (int i = 0; i < dtReminderCalls.Rows.Count; i++)
                    {
                        dtReminderCalls.Rows[i]["Sno"] = serialnumber.ToString();
                        serialnumber++;
                    }

                    dg_ReminderCalls.Visible = true;
                    dg_ReminderCalls.DataSource = dtReminderCalls;
                    dg_ReminderCalls.DataBind();
                    Pagingctrl.PageCount = dg_ReminderCalls.PageCount;
                    Pagingctrl.PageIndex = dg_ReminderCalls.PageIndex;
                    Pagingctrl.SetPageIndex();
                }
                else
                {
                    lblMessage.Text = "No Records Found";
                    dg_ReminderCalls.Visible = false;
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }
        }

        protected void btn_onlineupdate_Click(object sender, EventArgs e)
        {
            try
            {
                // For Consultation Report Comment Update
                // Noufil 4919 10/8/2008 enddate added 
                //if (Convert.ToInt32(ViewState["commenttype"]) == 1)
                //{
                //    SullolawReports.UpdateComments(Convert.ToInt32(ViewState["legalid"]), Convert.ToInt32(ViewState["empid"]), txt_onlinecomment.Text, Convert.ToInt32(ViewState["Sitetype"]), Convert.ToInt32(ddl_onlinecallback.SelectedValue));                   
                //    FillGrid();
                //}
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }
        }


        protected void btn_updatecomments_Click(object sender, EventArgs e)
        {
            try
            {
                // For Visitor Report Comment Update
                if (Convert.ToInt32(ViewState["commenttype"]) == 0)
                {
                    SullolawReports.UpdateComments(Convert.ToInt32(ViewState["customerid"]), Convert.ToInt32(ViewState["empid"]), txt_comment.Text, 0, Convert.ToInt32(ddl_rStatus.SelectedValue));
                    FillGrid();
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }
        }

        protected void btn_update1_Click1(object sender, EventArgs e)
        {
            try
            {

                FillGrid(); //filling the grid                
                tdData.Style.Add("display", "block");
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }

        }

        protected void dg_ReminderCalls_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Add_comments")
                {
                    ClearControls();
                    ViewState["commenttype"] = 0;
                    int RowID = Convert.ToInt32(e.CommandArgument);
                    ViewState["customerid"] = Convert.ToInt32((((Label)dg_ReminderCalls.Rows[RowID].Cells[0].FindControl("lbl_Customerid")).Text));
                    lbl_Name.Text = (((LinkButton)dg_ReminderCalls.Rows[RowID].Cells[0].FindControl("lnkName")).Text);
                    lbl_Status.Text = (((Label)dg_ReminderCalls.Rows[RowID].Cells[0].FindControl("lbl_Status")).Text);
                    lbl_location.Text = (((Label)dg_ReminderCalls.Rows[RowID].Cells[0].FindControl("lbl_Location")).Text);
                    lbl_contact1.Text = (((Label)dg_ReminderCalls.Rows[RowID].Cells[0].FindControl("lbl_contact1")).Text);
                    lbl_Contact2.Text = (((Label)dg_ReminderCalls.Rows[RowID].Cells[0].FindControl("lbl_Contact2")).Text);
                    lbl_Contact3.Text = (((Label)dg_ReminderCalls.Rows[RowID].Cells[0].FindControl("lbl_Contact3")).Text);
                    ddl_rStatus.SelectedValue = (((HiddenField)dg_ReminderCalls.Rows[RowID].Cells[0].FindControl("hf_callback")).Value);
                    lblOldComments.Text = (((Label)dg_ReminderCalls.Rows[RowID].Cells[0].FindControl("lbl_comments")).Text);
                    if (lblOldComments.Text.Trim() == "No Comments")
                    {
                        lblOldComments.Text = "";
                    }
                    divComment.Style["Display"] = (lblOldComments.Text == "") ? "none" : "block";

                    if (txt_comment.Text.Trim() == "No Comments")
                    {
                        txt_comment.Text = "";
                    }
                    hf_oldcomments.Value = (((Label)dg_ReminderCalls.Rows[RowID].Cells[0].FindControl("lbl_comments")).Text);
                    if (hf_oldcomments.Value.Trim() == "No Comments")
                    {
                        hf_oldcomments.Value = "";
                    }
                    ModalPopupExtender1.Show();
                    //Modal_gvrecords.Hide();
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }
        }

        protected void dg_ReminderCalls_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ((LinkButton)e.Row.FindControl("lnk_comments")).CommandArgument = e.Row.RowIndex.ToString();
                    LinkButton lbtn_lname = (LinkButton)e.Row.FindControl("lnkName");
                    lbtn_lname.OnClientClick = "window.open('" + lbtn_lname.CommandArgument + "','','');return false;";

                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }
        }

        protected void gv_records_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Add_gv_Comments")
                {

                    //ClearControls();
                    //ViewState["commenttype"] = 1;
                    //int RowID = Convert.ToInt32(e.CommandArgument);
                    //ViewState["legalid"] = Convert.ToInt32((((Label)gv_records.Rows[RowID].Cells[0].FindControl("lbl_legalid")).Text));
                    //ViewState["Sitetype"] = Convert.ToInt32((((HiddenField)gv_records.Rows[RowID].Cells[0].FindControl("hf_comm")).Value));
                    //lblOldOnlineComments.Text = (((Label)gv_records.Rows[RowID].Cells[0].FindControl("lbl_comment")).Text);
                    //if (lblOldOnlineComments.Text.Trim() == "No Comments")
                    //{
                    //    lblOldOnlineComments.Text = "";
                    //}
                    //divCommentOnline.Style["Display"] = (lblOldOnlineComments.Text == "") ? "none" : "block";

                    //if (txt_onlinecomment.Text.Trim() == "No Comments")
                    //{
                    //    txt_onlinecomment.Text = "";
                    //}
                    //lbl_onlinename.Text = (((Label)gv_records.Rows[RowID].Cells[0].FindControl("lnk_name")).Text);
                    //lbl_onlineemail.Text = (((HiddenField)gv_records.Rows[RowID].Cells[0].FindControl("hf_email")).Value);
                    //lbl_onlinequestion.Text = (((HiddenField)gv_records.Rows[RowID].Cells[0].FindControl("hf_question")).Value);
                    //lbl_ph.Text = (((Label)gv_records.Rows[RowID].Cells[0].FindControl("lbl_phone")).Text);
                    //ddl_onlinecallback.SelectedValue = (((HiddenField)gv_records.Rows[RowID].Cells[0].FindControl("hf_onlinecallback")).Value);
                    //hf_onlineoldcomments.Value = (((Label)gv_records.Rows[RowID].Cells[0].FindControl("lbl_comment")).Text);
                    //if (hf_onlineoldcomments.Value.Trim() == "No Comments")
                    //{
                    //    hf_onlineoldcomments.Value = "";
                    //}

                    //Modal_gvrecords.Show();
                    //ModalPopupExtender1.Hide();
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }

        }

        protected void gv_records_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ((LinkButton)e.Row.FindControl("lnk_comment")).CommandArgument = e.Row.RowIndex.ToString();
                    Label lbl_emmail = ((Label)e.Row.FindControl("lbl_emailadress"));
                    Label lbl_ques = ((Label)e.Row.FindControl("lbl_Question"));
                    if (lbl_emmail.Text.Length > 28)
                        lbl_emmail.Text = lbl_emmail.Text.Substring(0, 25) + "...";
                    if (lbl_ques.Text.Length > 28)
                        lbl_ques.Text = lbl_ques.Text.Substring(0, 25) + "...";
                    HiddenField lbltype = (HiddenField)e.Row.FindControl("hf_comm");
                    if (Convert.ToInt32(lbltype.Value) == 1)
                        ((Label)e.Row.FindControl("lbl_sitetype")).Text = "Consultation Request";
                    else if (Convert.ToInt32(lbltype.Value) == 2)
                        ((Label)e.Row.FindControl("lbl_sitetype")).Text = "Contact us Request";
                    // Noufil 4947 11/10/2008 Check added for Offline Support Request
                    else if (Convert.ToInt32(lbltype.Value) == 3)
                        ((Label)e.Row.FindControl("lbl_sitetype")).Text = "Offline Support Request";
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }
        }

        private void GetReminderStatus(DropDownList ddl, int showall)
        {
            SullolawReports slReport = new SullolawReports();
            //DataTable dtStatus = slReport.GetReminderStatus();
            DataTable dtStatus = clscall.GetReminderStatus();
            ddl.DataSource = dtStatus;
            ddl.DataMember = "Description";
            ddl.DataValueField = "Reminderid_PK";
            ddl.DataTextField = "Description";
            ddl.DataBind();
            if (showall == 1)
            {
                ListItem itm = new ListItem("All", "5");
                ddl.Items.Insert(0, itm);
                //Asif Ghouri 4857 09/25/2008 Select by Default Pending Status
                ddl.SelectedValue = "0";
            }
        }

        private void ClearControls()
        {
            lbl_Name.Text = lbl_Status.Text = lbl_location.Text = lbl_contact1.Text = lbl_Contact2.Text = lbl_Contact3.Text = txt_comment.Text = hf_oldcomments.Value = "";
            //txt_onlinecomment.Text = lbl_onlinename.Text = lbl_onlineemail.Text = lbl_onlinequestion.Text = hf_onlineoldcomments.Value = hf_onlineoldcomments.Value = "";
        }

        protected void dg_ReminderCalls_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dg_ReminderCalls.PageIndex = e.NewPageIndex;
            FillGrid();
        }

        void Pagingctrl_PageIndexChanged()
        {
            dg_ReminderCalls.PageIndex = Pagingctrl.PageIndex - 1;
            FillGrid();
        }

        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            if (pageSize > 0)
            {
                dg_ReminderCalls.PageIndex = 0;
                dg_ReminderCalls.PageSize = pageSize;
                dg_ReminderCalls.AllowPaging = true;
            }
            else
            {
                dg_ReminderCalls.AllowPaging = false;
            }
            FillGrid();
        }

    }



}

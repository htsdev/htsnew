<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>

<%@ Page Language="c#" AutoEventWireup="false" Inherits="HTP.Reports.frmRptDocketULNew"
    CodeBehind="frmRptDocketULNew.aspx.cs" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>Docket Report</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../Styles.css" type="text/css" rel="stylesheet" />

    <script src="../Scripts/dates.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
		function validate()
		{			
			var d1 = document.getElementById("dtFrom").value
			var d2 = document.getElementById("dtTo").value			
			
			if (d1 == d2)
				return true;
			if (compareDates(d2,'MM/dd/yyyy',d1,'MM/dd/yyyy')==false)
				{
					alert("start date cannot greater than end date");
					document.getElementById("dtTo").focus(); 
					return false;
				}
				return true;
		}
		function CalendarValidation()
		{ 
					
			var dtfrom=document.getElementById("dtFrom").value;
			var dtTo=document.getElementById("dtTo").value;
			if(dtfrom > dtTo)
			{
			alert("Please Specify Valid Future Date!");
			return false;
			}
		}
		
		function showall()
		{		
		 //Yasir Kamal 5025 01/21/2009  �Non HMC/HCJP Cases� under Courts drop down box
		    if (document.getElementById("chk_showall").checked)
		    {
		       document.getElementById("LblTo").disabled =true;
		        document.getElementById("dtFrom").disabled =true;
		        document.getElementById("LblFrom").disabled =true;
		        document.getElementById("dtTo").disabled =true;
		    }
		    else
		    {
		       document.getElementById("LblTo").disabled =false;
		        document.getElementById("dtFrom").disabled =false;
		       document.getElementById("LblFrom").disabled =false;
		        document.getElementById("dtTo").disabled =false;
		    }
		}	
		
		
    </script>

    <style type="text/css">
        .style1
        {
            width: 109px;
        }
        .style2
        {
            width: 111px;
        }
    </style>
</head>
<body bottommargin="0" leftmargin="0" topmargin="0" rightmargin="0" ms_positioning="GridLayout"
    onload="showall()">
    <form id="Form1" method="post" runat="server">
    <table id="tblMain" cellspacing="0" cellpadding="0" width="800" align="center" border="0">
        <tr>
            <td align="left">
                <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
            </td>
        </tr>
        <!-- 				<tr vAlign="middle">
					<td align="left"><IMG height="18" src="../Images/head_icon.gif" width="25">&nbsp;<STRONG>
							Docket Report</STRONG></td>
				</tr> -->
        <tr>
            <td background="../images/separator_repeat.gif" height="11">
            </td>
        </tr>
        <tr>
            <td align="left" style="height: 33px">
                <table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr valign="bottom">
                        <td>
                            <%--Yasir Kamal 5025 01/21/2009  �Non HMC/HCJP Cases� under Courts drop down box--%>
                            <asp:Label ID="LblCourtLocation" runat="server" Text="Court Location" CssClass="clssubhead"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="LblCaseStatus" runat="server" Text="Case Status" CssClass="clssubhead"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="LblFrom" runat="server" Text="Court Date From" CssClass="clssubhead"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="LblTo" runat="server" Text="Court Date To" CssClass="clssubhead"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lbl_FirmAbbrevation" runat="server" Text="Out side Firm" CssClass="clssubhead"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:DropDownList ID="ddlCourtLoc" runat="server" Width="150px" CssClass="clsinputcombo">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlCaseStatus" runat="server" Width="110px" CssClass="clsinputcombo">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <ew:CalendarPopup ID="dtFrom" runat="server" Height="19px" Width="100px" Font-Size="8pt"
                                Font-Names="Tahoma" DisplayOffsetY="20" ToolTip="Date From" PadSingleDigits="True"
                                UpperBoundDate="12/31/9999 23:59:00" ShowClearDate="False" Nullable="True" Culture="(Default)"
                                AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Left" ImageUrl="../images/calendar.gif"
                                ControlDisplay="TextBoxImage" EnableHideDropDown="True" Text=" ">
                                <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                    BackColor="White"></WeekdayStyle>
                                <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                    BackColor="AntiqueWhite"></OffMonthStyle>
                                <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                    BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                    BackColor="LightGray"></WeekendStyle>
                                <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="White"></ClearDateStyle>
                                <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                    BackColor="White"></HolidayStyle>
                            </ew:CalendarPopup>
                        </td>
                        <td>
                            <ew:CalendarPopup ID="dtTo" runat="server" Height="19px" Width="90px" Font-Size="8pt"
                                Font-Names="Tahoma" DisplayOffsetY="20" ToolTip="Date To" PadSingleDigits="True"
                                UpperBoundDate="12/31/9999 23:59:00" ShowClearDate="False" Nullable="True" Culture="(Default)"
                                AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Left" ImageUrl="../images/calendar.gif"
                                ControlDisplay="TextBoxImage" EnableHideDropDown="True" Text=" ">
                                <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                    BackColor="White"></WeekdayStyle>
                                <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                    BackColor="AntiqueWhite"></OffMonthStyle>
                                <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                    BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                    BackColor="LightGray"></WeekendStyle>
                                <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="White"></ClearDateStyle>
                                <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                    BackColor="White"></HolidayStyle>
                            </ew:CalendarPopup>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddl_FirmAbbreviation" runat="server" CssClass="clsinputcombo"
                                Font-Size="XX-Small">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:CheckBox ID="chk_showall" runat="server" Text="Show All" CssClass="clssubhead"
                                onclick="showall()" />
                        </td>
                        <td>
                            <asp:Button ID="btnSubmit" runat="server" CssClass="clsbutton" Text="Submit" Width="87px"
                                OnClientClick="showall()"></asp:Button>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td background="../images/separator_repeat.gif" height="11">
            </td>
        </tr>
        <tr>
            <td width="100%">
                <table id="tblTitle" cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="clssubhead" height="34px" background="../Images/subhead_bg.gif">
                            &nbsp;
                        </td>
                        <td class="clssubhead" align="right" height="34px" background="../../Images/subhead_bg.gif">
                            <uc3:PagingControl ID="Pagingctrl" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left">
                <%--Sabir Khan 5831 04/23/2009 datagrid has been changed into Gridview.--%>
                <asp:GridView ID="gv_Records" runat="server" AutoGenerateColumns="False" Width="100%"
                    CssClass="clsLeftPaddingTable" AllowSorting="True" AllowPaging="True" PageSize="30"
                    OnRowDataBound="gv_Records_RowDataBound" OnPageIndexChanging="gv_Records_PageIndexChanging"
                    OnSorting="gv_Records_Sorting">
                    <Columns>
                        <asp:TemplateField HeaderText="S#">
                            <ItemTemplate>
                                <%--Sabir Khan 5521 02/16/2009 set sno for client profile...--%>
                                <%--  <asp:Label ID="lblSerial" runat="server" CssClass="clsLabel" Text='<%# Bind("sno") %>'></asp:Label>--%>
                                &nbsp;<asp:HyperLink ID="lblSerial" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                    Text='<%# DataBinder.Eval(Container, "DataItem.sno") %>'></asp:HyperLink>
                                <%--<asp:Label ID="lblSerial" runat="server"></asp:Label>--%>
                                <asp:Label ID="lbl_Ticketid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                    Visible="False">
                                </asp:Label>
                            </ItemTemplate>
                            <HeaderStyle CssClass="clssubhead" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="&lt;u&gt;Last Name&lt;/u&gt;" 
                            SortExpression="lastname">
                            <ItemTemplate>
                                <asp:Label ID="lblLastName" runat="server" Text='<%# Bind("lastname") %>'></asp:Label>
                            </ItemTemplate>                           
                            <HeaderStyle Font-Underline="True" HorizontalAlign="Center" />
                            <ItemStyle CssClass="GridItemStyle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="&lt;u&gt;First Name&lt;/u&gt;" 
                            SortExpression="firstname">
                            <ItemTemplate>
                                <asp:Label ID="lblFirstName" runat="server" Text='<%# Bind("firstname") %>'></asp:Label>
                            </ItemTemplate>                        
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle CssClass="GridItemStyle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="<u>Entry</u>" SortExpression="sortentry">
                            <ItemTemplate>
                                <asp:Label ID="lbl_Paydate" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.entry") %>'
                                    Width="100%"> </asp:Label>
                            </ItemTemplate>
                            <HeaderStyle CssClass="clssubhead" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="&lt;u&gt;Location&lt;/u&gt;" 
                            SortExpression="court" >
                            <ItemTemplate>
                                <asp:Label ID="lblCourt" runat="server" Text='<%# Bind("court") %>'></asp:Label>
                            </ItemTemplate>                           
                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                            <ItemStyle CssClass="GridItemStyle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="<u>Last Pay Date</u>" SortExpression="sortpaydate">
                            <ItemTemplate>
                                <asp:Label ID="lbl_Paydate" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.paydate") %>'
                                    Width="100%"> </asp:Label>
                            </ItemTemplate>
                            <HeaderStyle CssClass="clssubhead" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="&lt;u&gt;Court Date&lt;/u&gt;" 
                            SortExpression="sortcourtdate">
                            <ItemTemplate>
                                <asp:Label ID="lbl_Courtdate" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.courtdatemain") %>'
                                    Width="100%"> </asp:Label>
                            </ItemTemplate>
                            <HeaderStyle CssClass="clssubhead" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="&lt;u&gt;Status&lt;/u&gt;" 
                            SortExpression="description">
                            <ItemTemplate>
                                <asp:Label ID="lblDescription" runat="server" Text='<%# Bind("description") %>'></asp:Label>
                            </ItemTemplate>                          
                            <ItemStyle CssClass="GridItemStyle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Firm">
                            <ItemTemplate>
                                <asp:Label ID="lblFirm" runat="server" Text='<%# Bind("firmabbreviation") %>'></asp:Label>
                            </ItemTemplate>                     
                            <ItemStyle CssClass="GridItemStyle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="&lt;u&gt;Bond&lt;/u&gt;" 
                            SortExpression="bondflag">
                            <ItemTemplate>
                                <asp:Label ID="lblBond" runat="server" Text='<%# Bind("bondflag") %>'></asp:Label>
                            </ItemTemplate>                       
                            <ItemStyle CssClass="GridItemStyle" />
                        </asp:TemplateField>
                    </Columns>
                    <PagerStyle HorizontalAlign="Center" />
                    <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                        FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td background="../images/separator_repeat.gif" height="11">
            </td>
        </tr>
        <tr>
            <td style="width: 760px" align="left" colspan="4">
                <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>

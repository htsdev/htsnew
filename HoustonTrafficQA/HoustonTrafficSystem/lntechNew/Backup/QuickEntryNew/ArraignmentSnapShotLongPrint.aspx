<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.QuickEntryNew.ArraignmentSnapShotLongPrint" Codebehind="ArraignmentSnapShotLongPrint.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ArraignmentSnapShotLongPrint</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript">
		function Hide()
		{
			if( document.getElementById("ddl_WeekDay").value=="-1")
			{
				document.getElementById("jt").style.display='none';
				document.getElementById("jtgrd").style.display='none';
				document.getElementById("ost").style.display='none';
				document.getElementById("ostgrd").style.display='none';
				document.getElementById("uaagrd").style.display='none';
				document.getElementById("a1").style.display='none';
				document.getElementById("a2").style.display='none';
				document.getElementById("a3").style.display='none';				
			}
			else if(document.getElementById("ddl_WeekDay").value==100)
			{
				document.getElementById("jt").style.display='block';
				document.getElementById("jtgrd").style.display='block';
				document.getElementById("ost").style.display='none';
				document.getElementById("ostgrd").style.display='none';
				document.getElementById("uaagrd").style.display='none';
				document.getElementById("a1").style.display='none';
				document.getElementById("a2").style.display='none';
				document.getElementById("a3").style.display='none';
			}
			else if(document.getElementById("ddl_WeekDay").value!=99)
			{
				document.getElementById("jt").style.display='none';
				document.getElementById("jtgrd").style.display='none';
				document.getElementById("ost").style.display='none';
				document.getElementById("ostgrd").style.display='none';
				document.getElementById("uaagrd").style.display='block';
				document.getElementById("a1").style.display='none';
				document.getElementById("a2").style.display='none';
				document.getElementById("a3").style.display='none';
			}
			
			else
			{
				document.getElementById("jt").style.display='block';
				document.getElementById("jtgrd").style.display='block';
				document.getElementById("ost").style.display='block';
				document.getElementById("ostgrd").style.display='block';
				document.getElementById("uaagrd").style.display='block';
				document.getElementById("a1").style.display='block';
				document.getElementById("a2").style.display='block';
				document.getElementById("a3").style.display='block';				
			}
		}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout" onload="javascript: Hide();">
		<form id="Form1" method="post" runat="server">
			<TABLE id="TableMain" cellSpacing="0" cellPadding="0" width="725" align="left" border="0">
				<TR>
					<TD><asp:dropdownlist id="ddl_WeekDay" runat="server" AutoPostBack="True" CssClass="clsinputcombo" Enabled="False">
							<asp:ListItem Value="-1">Select a Day</asp:ListItem>
							<asp:ListItem Value="2">Monday</asp:ListItem>
							<asp:ListItem Value="3">Tuesday</asp:ListItem>
							<asp:ListItem Value="4">Wednesday</asp:ListItem>
							<asp:ListItem Value="5">Thursday</asp:ListItem>
							<asp:ListItem Value="6">Friday</asp:ListItem>
                        <asp:ListItem Value="100">Judge Trial</asp:ListItem>
							<asp:ListItem Value="99">Not Assigned</asp:ListItem>
						</asp:dropdownlist>&nbsp;&nbsp;&nbsp;&nbsp;
						<asp:label id="lbl_CurrDateTime" runat="server" Font-Names="Times New Roman" Font-Bold="True"></asp:label></TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 13px"></TD>
				</TR>
				<TR>
					<TD><asp:label id="lbl_Message" runat="server" CssClass="Label" Font-Bold="True" Visible="False">There is currently no clients arraigned for arraignments</asp:label></TD>
				</TR>
				<TR>
					<TD>
						<TABLE id="TableGrid" cellSpacing="0" cellPadding="0" width="100%" bgColor="white" border="0">
							<tr>
								<td style="HEIGHT: 16px"></td>
							</tr>
							<TR>
								<TD id="uaagrd" style="HEIGHT: 9px" width="780" colSpan="5" height="9"><asp:datagrid id="dg_Report" runat="server" EnableViewState="False" AutoGenerateColumns="False"
										BorderColor="Black" BorderStyle="Solid" Width="100%">
										<ItemStyle Font-Names="Times New Roman"></ItemStyle>
										<HeaderStyle ForeColor="Black"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn HeaderText="No:">
												<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
												<ItemStyle Font-Names="Times New Roman"></ItemStyle>
												<ItemTemplate>
													<asp:Label id="lbl_sno" runat="server">0</asp:Label>
													<asp:Label id="lbl_Ticketid" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.TicketID_PK") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Flags">
												<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
												<ItemStyle Font-Names="Times New Roman"></ItemStyle>
												<ItemTemplate>
													<asp:Label id="Label14" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BondFlag") %>' >
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Case #">
												<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
												<ItemStyle Font-Names="Times New Roman" Font-Bold="True"></ItemStyle>
												<ItemTemplate>
													<asp:Label id="Label5" runat="server" Font-Bold="True" Text='<%# DataBinder.Eval(Container, "DataItem.TicketNumber_PK") %>' >
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Seq">
												<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
												<ItemStyle Font-Names="Times New Roman"></ItemStyle>
												<ItemTemplate>
													<asp:Label id=Label15 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Violations") %>'>
													</asp:Label>&nbsp;
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Last Name">
												<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
												<ItemStyle Font-Names="Times New Roman"></ItemStyle>
												<ItemTemplate>
													<asp:Label id="Label16" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Lastname") %>' >
													</asp:Label>&nbsp;
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="First Name">
												<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
												<ItemStyle Font-Names="Times New Roman"></ItemStyle>
												<ItemTemplate>
													<asp:Label id="Label17" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Firstname") %>' >
													</asp:Label>&nbsp;
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="DL">
												<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
												<ItemStyle Font-Names="Times New Roman"></ItemStyle>
												<ItemTemplate>
													<asp:Label id="Label18" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DLNumber") %>' >
													</asp:Label>&nbsp;
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="DOB">
												<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
												<ItemStyle Font-Names="Times New Roman"></ItemStyle>
												<ItemTemplate>
													<asp:Label id=lbl_dob runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DOB") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="MID">
												<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
												<ItemStyle Font-Names="Times New Roman"></ItemStyle>
												<ItemTemplate>
													<asp:Label id="Label20" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MID") %>' >
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Set Date">
												<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
												<ItemStyle Font-Names="Times New Roman"></ItemStyle>
												<ItemTemplate>
													<asp:Label id="lbl_arrdate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TrialDateTime") %>' >
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
									</asp:datagrid></TD>
							</TR>
							<tr>
								<td style="HEIGHT: 16px"></td>
							</tr>
							<tr>
								<td id="jt" style="HEIGHT: 14px"><STRONG>
                                    <asp:Label ID="lbl_Judge" runat="server" Text="Judge Trials" Visible="False"></asp:Label></STRONG></td>
							</tr>
							<tr>
								<td id="a1" style="HEIGHT: 14px"></td>
							</tr>
							<TR>
								<TD id="jtgrd" style="HEIGHT: 9px" width="780" colSpan="5" height="9"><asp:datagrid id="dg_ReportJT" runat="server" EnableViewState="False" AutoGenerateColumns="False"
										BorderColor="Black" BorderStyle="Solid" Width="100%">
										<ItemStyle Font-Names="Times New Roman"></ItemStyle>
										<HeaderStyle ForeColor="Black"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn HeaderText="No:">
												<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
												<ItemStyle Font-Names="Times New Roman"></ItemStyle>
												<ItemTemplate>
													<asp:Label id="lbl_sno1" runat="server">0</asp:Label>
													<asp:Label id=lbl_Ticketid1 runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.TicketID_PK") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Flags">
												<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
												<ItemStyle Font-Names="Times New Roman"></ItemStyle>
												<ItemTemplate>
													<asp:Label id="Label141" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BondFlag") %>' >
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Case #">
												<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
												<ItemStyle Font-Names="Times New Roman" Font-Bold="True"></ItemStyle>
												<ItemTemplate>
													<asp:Label id="Label6" runat="server" Font-Bold="True" Text='<%# DataBinder.Eval(Container, "DataItem.TicketNumber_PK") %>' >
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Seq">
												<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
												<ItemStyle Font-Names="Times New Roman"></ItemStyle>
												<ItemTemplate>
													<asp:Label id=Label151 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Violations") %>'>
													</asp:Label>&nbsp;
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Last Name">
												<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
												<ItemStyle Font-Names="Times New Roman"></ItemStyle>
												<ItemTemplate>
													<asp:Label id="Label161" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Lastname") %>' >
													</asp:Label>&nbsp;
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="First Name">
												<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
												<ItemStyle Font-Names="Times New Roman"></ItemStyle>
												<ItemTemplate>
													<asp:Label id="Label171" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Firstname") %>' >
													</asp:Label>&nbsp;
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="DL">
												<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
												<ItemStyle Font-Names="Times New Roman"></ItemStyle>
												<ItemTemplate>
													<asp:Label id="Label181" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DLNumber") %>' >
													</asp:Label>&nbsp;
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="DOB">
												<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
												<ItemStyle Font-Names="Times New Roman"></ItemStyle>
												<ItemTemplate>
													<asp:Label id=lbl_dob1 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DOB") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Court Information">
												<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
												<ItemStyle Font-Names="Times New Roman"></ItemStyle>
												<ItemTemplate>
													<asp:Label id=lbl_CCDate runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CurrentDateSet") %>'>
													</asp:Label>
													<asp:Label id=lbl_CCNum runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.CurrentCourtNum") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Officer Day">
												<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
												<ItemStyle Font-Names="Times New Roman"></ItemStyle>
												<ItemTemplate>
													<asp:Label id=lbl_OfficerDay runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.OfficerDay") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
									</asp:datagrid>
									<table width="100%" id="tbl_PR_Report" runat="server"  style="display:none">
                                             <tr>
                                            <td style="height: 14px" >
                                                <strong>Probation Request Report</strong></td>
                                            </tr><tr>
                                        <td id="Td4" style="height: 14px">
                                        </td>
                                    </tr><tr>
                                        <td>
                                        <asp:DataGrid ID="dg_ProbReq" runat="server" AutoGenerateColumns="False" BorderColor="Black"
                                    BorderStyle="Solid" Width="100%" >
                                    <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                    <HeaderStyle ForeColor="Black"></HeaderStyle>
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="ticketid" Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_TicketID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TicketID_PK") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="No">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                &nbsp;<asp:Label ID="lblSNo" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.SNO") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Last Name" SortExpression="lastname">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_LastName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Lastname") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="First Name">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_FirstName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Firstname") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Cause No">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                &nbsp;<asp:Label ID="lbl_CauseNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CauseNumber") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Ticket No">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_TicketNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TicketNumber") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Violation Desc">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_VioDesc" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ViolationDescription") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Crt Date">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_CourtDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDate","{0:d}") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Crt Time">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_CourtTime" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDate","{0:t}") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Room">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_CourtRoom" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CourtRoom") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Status">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Status" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Status") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Flags">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_FlagsBF" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BondFlag") %>'>
                                                </asp:Label>
                                                <asp:Label ID="lbl_PR_Flag" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PRFlag") %>'>
                                                </asp:Label>
                                                <asp:Label ID="lbl_POA_Flag" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.POAFlag") %>'>
                                                </asp:Label>    
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Trial Comments">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman" Font-Size="XX-Small"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_TrialComments" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TrialComments") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                                        </td>
                                    </tr>
                                            </table>
									</TD>
							</TR>
							<tr>
								<td id="a2" style="HEIGHT: 16px"></td>
							</tr>
							<tr>
								<td id="ost" style="HEIGHT: 16px"><STRONG>
                                    <asp:Label ID="lbl_Outside" runat="server" Text="Outside Trials" Visible="False"></asp:Label></STRONG></td>
							</tr>
							<tr>
								<td id="a3" style="HEIGHT: 16px"></td>
							</tr>
							<TR>
								<TD id="ostgrd" style="HEIGHT: 9px" width="780" colSpan="5" height="9"><asp:datagrid id="dg_ReportOT" runat="server" EnableViewState="False" AutoGenerateColumns="False"
										BorderColor="Black" BorderStyle="Solid" Width="100%">
										<ItemStyle Font-Names="Times New Roman"></ItemStyle>
										<HeaderStyle ForeColor="Black"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn HeaderText="No:">
												<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
												<ItemStyle Font-Names="Times New Roman"></ItemStyle>
												<ItemTemplate>
													<asp:Label id="lbl_sno2" runat="server">0</asp:Label>
													<asp:Label id=lbl_Ticketid2 runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.TicketID_PK") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Flags">
												<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
												<ItemStyle Font-Names="Times New Roman"></ItemStyle>
												<ItemTemplate>
													<asp:Label id=Label142 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BondFlag") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Case #">
												<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
												<ItemStyle Font-Names="Times New Roman" Font-Bold="True"></ItemStyle>
												<ItemTemplate>
													<asp:Label id="Label7" runat="server" Font-Bold="True" Text='<%# DataBinder.Eval(Container, "DataItem.TicketNumber_PK") %>' >
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Seq">
												<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
												<ItemStyle Font-Names="Times New Roman"></ItemStyle>
												<ItemTemplate>
													<asp:Label id=Label152 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Violations") %>'>
													</asp:Label>&nbsp;
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Last Name">
												<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
												<ItemStyle Font-Names="Times New Roman"></ItemStyle>
												<ItemTemplate>
													<asp:Label id="Label162" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Lastname") %>' >
													</asp:Label>&nbsp;
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="First Name">
												<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
												<ItemStyle Font-Names="Times New Roman"></ItemStyle>
												<ItemTemplate>
													<asp:Label id="Label172" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Firstname") %>' >
													</asp:Label>&nbsp;
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="DL">
												<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
												<ItemStyle Font-Names="Times New Roman"></ItemStyle>
												<ItemTemplate>
													<asp:Label id="Label182" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DLNumber") %>' >
													</asp:Label>&nbsp;
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="DOB">
												<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
												<ItemStyle Font-Names="Times New Roman"></ItemStyle>
												<ItemTemplate>
													<asp:Label id=lbl_dob2 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DOB") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Court Information">
												<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
												<ItemStyle Font-Names="Times New Roman"></ItemStyle>
												<ItemTemplate>
													<asp:Label id="lbl_CCDate1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CurrentDateSet") %>'>
													</asp:Label>
													<asp:Label id="lbl_CCNum1" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.CurrentCourtNum") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
									</asp:datagrid></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using lntechNew.Components.ClientInfo;
using HTP.ClientController;
using POLMDTO;

namespace HTP.Configuration.Polm
{
    /// <summary>
    /// This class represents all information about the Division Information Setting.
    /// </summary>
    public partial class DvisionInformationSetting : WebComponents.BasePage
    {
        #region Variables

        //Variable Declared for POLM controller class
        readonly PolmController _htpController = new PolmController();

        //Variable Declared for holding the Loop Counter of ListBox Items.
        int _loopcount;

        ////Variable Declared for holding the Selecetd Items Count of ListBox Items.
        int _selecteditemcount;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Fahad 6766 04/28/2010 Two separate Labels Defined for both Quick legal Matter Description and as well as bodily Damage setting
                lblQLMDMessage.Text = string.Empty;
                lblBodilyDamageMessage.Text = string.Empty;
                //Checking Access Type if Not Primary then redirecting to Login Error Page
                if (AccessType != 2)
                {
                    Response.Redirect("~/LoginAccesserror.aspx", false);
                }
                else if (!IsPostBack)
                {
                    EmptyAllLists();
                    FillDivisionCombo();
                    FillAllLists();
                    SetDefaultView();
                }
            }
            catch (Exception ex)
            {
                //Fahad 6766 04/28/2010 Two separate Labels Defined for both Quick legal Matter Description and as well as bodily Damage setting
                lblQLMDMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void imgAssignQLMD_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                imgUnassignQLMD.Enabled = true;
                var totallistcount = lbUnAssignedQLMD.Items.Count - 1;
                for (var i = 0; i < lbUnAssignedQLMD.Items.Count; i++)
                {
                    if (!lbUnAssignedQLMD.Items[i].Selected) continue;
                    var isaagined = _htpController.AddQlmdOfSOurce(Convert.ToInt32(lbUnAssignedQLMD.Items[i].Value), Convert.ToInt32(dd_division.SelectedValue));
                    //Fahad 6766 04/28/2010 Two separate Labels Defined for both Quick legal Matter Description and as well as bodily Damage setting
                    lblQLMDMessage.Text = isaagined ? "Value(s) Assigned Successfully." : "Value(s) not Assigned Successfully.";
                    //setting loop count for current record
                    _loopcount = i;
                    //increasing the loop count by 1 for selected item count
                    _selecteditemcount += 1;

                }
                FillAssignedQlmd();
                FillUnAssignedQlmd();

                //For setting the Selection of very next item of the List
                SetListView(totallistcount, _selecteditemcount, _loopcount, lbUnAssignedQLMD, imgAssignQLMD);
            }
            catch (Exception ex)
            {
                //Fahad 6766 04/28/2010 Two separate Labels Defined for both Quick legal Matter Description and as well as bodily Damage setting
                lblQLMDMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }


        }

        protected void imgUnassignQLMD_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                imgAssignQLMD.Enabled = true;
                var totallistcount = lbAssignedQLMD.Items.Count - 1;
                for (var i = 0; i < lbAssignedQLMD.Items.Count; i++)
                {
                    if (!lbAssignedQLMD.Items[i].Selected) continue;

                    var isUnAssigned = _htpController.DeleteQlmdOfSOurce(Convert.ToInt32(lbAssignedQLMD.Items[i].Value));
                    //Fahad 6766 04/28/2010 Two separate Labels Defined for both Quick legal Matter Description and as well as bodily Damage setting
                    lblQLMDMessage.Text = isUnAssigned ? "Value(s) Un-Assigned Successfully." : "Value(s) not Un-Assigned Successfully.";
                    //setting loop count for current record
                    _loopcount = i;
                    //increasing the loop count by 1 for selected item count
                    _selecteditemcount += 1;

                }
                FillAssignedQlmd();
                FillUnAssignedQlmd();

                //For setting the Selection of very next item of the List
                SetListView(totallistcount, _selecteditemcount, _loopcount, lbAssignedQLMD, imgUnassignQLMD);
            }
            catch (Exception ex)
            {
                //Fahad 6766 04/28/2010 Two separate Labels Defined for both Quick legal Matter Description and as well as bodily Damage setting
                lblQLMDMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        protected void imgAssignBodilyDamage_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                imgUnassignBodilyDamage.Enabled = true;
                var totallistcount = lbUnassignedBD.Items.Count - 1;
                for (var i = 0; i < lbUnassignedBD.Items.Count; i++)
                {
                    if (!lbUnassignedBD.Items[i].Selected) continue;
                    var isaagined = _htpController.AddBodilyDamageOfSOurce(Convert.ToInt32(lbUnassignedBD.Items[i].Value), Convert.ToInt32(dd_division.SelectedValue));
                    //Fahad 6766 04/28/2010 Two separate Labels Defined for both Quick legal Matter Description and as well as bodily Damage setting
                    lblBodilyDamageMessage.Text = isaagined ? "Value(s) Assigned Successfully." : "Value(s) not Assigned Successfully.";
                    //setting loop count for current record
                    _loopcount = i;
                    //increasing the loop count by 1 for selected item count
                    _selecteditemcount += 1;

                }
                FillAssignedBd();
                FillUnAssignedBd();
                //For setting the Selection of very next item of the List
                SetListView(totallistcount, _selecteditemcount, _loopcount, lbUnassignedBD, imgAssignBodilyDamage);
            }
            catch (Exception ex)
            {
                //Fahad 6766 04/28/2010 Two separate Labels Defined for both Quick legal Matter Description and as well as bodily Damage setting
                lblBodilyDamageMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        protected void imgUnassignBodilyDamage_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                imgAssignBodilyDamage.Enabled = true;
                var totallistcount = lbAssignedBD.Items.Count - 1;
                for (var i = 0; i < lbAssignedBD.Items.Count; i++)
                {
                    if (!lbAssignedBD.Items[i].Selected) continue;

                    var isUnAssigned = _htpController.DeleteBodilyDamageOfSOurce(Convert.ToInt32(lbAssignedBD.Items[i].Value));
                    //Fahad 6766 04/28/2010 Two separate Labels Defined for both Quick legal Matter Description and as well as bodily Damage setting
                    lblBodilyDamageMessage.Text = isUnAssigned ? "Value(s) Un-Assigned Successfully." : "Value(s) not Un-Assigned Successfully.";
                    //setting loop count for current record
                    _loopcount = i;
                    //increasing the loop count by 1 for selected item count
                    _selecteditemcount += 1;
                }
                FillAssignedBd();
                FillUnAssignedBd();

                //For setting the Selection of very next item of the List
                SetListView(totallistcount, _selecteditemcount, _loopcount, lbAssignedBD, imgUnassignBodilyDamage);
            }
            catch (Exception ex)
            {
                //Fahad 6766 04/28/2010 Two separate Labels Defined for both Quick legal Matter Description and as well as bodily Damage setting
                lblBodilyDamageMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }


        }

        protected void dd_division_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                EmptyAllLists();
                FillAllLists();
                SetDefaultView();
            }
            catch (Exception ex)
            {
                lblQLMDMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        #endregion

        #region Methods

        /// <summary>
        /// This method is used to Get All Assigned Quick Legal Matter Description.
        /// </summary>
        private void FillAssignedQlmd()
        {
            //Call the Polm Controller method to get All Quick Legal Matter Description Of Source and assigned to the List.
            var qlmdlist = _htpController.GetAllQuickLegalMatterDescriptionOfSource(Convert.ToInt32(dd_division.SelectedValue));

            //Checking if list is not empty or Null.
            if (qlmdlist != null)
            {
                lbAssignedQLMD.DataSource = qlmdlist;
                lbAssignedQLMD.DataTextField = "Name";
                lbAssignedQLMD.DataValueField = "PrimaryId";
                lbAssignedQLMD.DataBind();
            }
            else
            {
                lbAssignedQLMD.Items.Clear();
            }
        }

        /// <summary>
        /// This method is used to Get All UnAssigned Quick Legal Matter Description.
        /// </summary>
        private void FillUnAssignedQlmd()
        {
            //Call the Polm Controller method to get All Quick Legal Matter Description Of Source and assigned to the List.
            var assignqlmdlist = _htpController.GetAllQuickLegalMatterDescriptionOfSource(Convert.ToInt32(dd_division.SelectedValue));

            //Call the Polm Controller method to get All Active Quick Legal Matter Description and assigned to the List.
            var allqlmdlist = _htpController.GetAllQuickLegalMatterDescription(true);
            List<PolmDto> unassignedlist = null;

            //Checking if list is not empty or Null.
            if (allqlmdlist != null)
            {
                unassignedlist = new List<PolmDto>();

                //Iterate through the all items of the List
                foreach (var polmDto in allqlmdlist)
                {
                    //Calling of the Local Defined method which find out the assigned and Unassigned Quick Legal Matter Description
                    if (!FindValue(assignqlmdlist, polmDto.Id))
                    {
                        //If item is not assinged then add in Unassgined List
                        unassignedlist.Add(polmDto);
                    }
                }

            }
            //Checking if list is not empty or Null then bind with Grid.
            if (unassignedlist != null)
            {
                lbUnAssignedQLMD.DataSource = unassignedlist;
                lbUnAssignedQLMD.DataTextField = "Name";
                lbUnAssignedQLMD.DataValueField = "Id";
                lbUnAssignedQLMD.DataBind();
            }
            else
            {
                lbUnAssignedQLMD.Items.Clear();
            }
        }

        /// <summary>
        /// This method finds All Assigned and UnAssigned.
        /// </summary>
        /// <param name="source">Source List on which we have to find Id</param>
        /// <param name="id">Id which find on Source</param>
        /// <returns>True/False on the basis of Value Found or Not</returns>
        private bool FindValue(IEnumerable<PolmDivisionDto> source, int id)
        {
            //Checking if source list is not empty Or Null.
            if (source != null)
            {
                //Iterate through the all items of the LIst
                foreach (var polmdivisionDto in source)
                {
                    //If List Contains Id same as Polm Division Object then return True.
                    if (id == polmdivisionDto.Id)
                    {
                        return true;
                    }
                }
                return false;


            }
            return false;
        }

        /// <summary>
        /// This method is used to Get All Assigned Bodily Damages Value.
        /// </summary>
        private void FillAssignedBd()
        {
            //Call the Polm Controller method to get All Bodily Damage Of Division and assigned to the List.
            var bdlist = _htpController.GetAllBodilyDamageOfDivision(Convert.ToInt32(dd_division.SelectedValue));

            //Checking if list is not empty or Null.
            if (bdlist != null)
            {
                lbAssignedBD.DataSource = bdlist;
                lbAssignedBD.DataTextField = "Name";
                lbAssignedBD.DataValueField = "PrimaryId";
                lbAssignedBD.DataBind();
            }
            else
            {
                lbAssignedBD.Items.Clear();
            }

        }

        /// <summary>
        ///     This method is used to Get All UnAssigned Bodily Damages Value.
        /// </summary>
        private void FillUnAssignedBd()
        {
            //Call the Polm Controller method to get All Bodily Damage Of Source and assigned to the List.
            var assignbdlist = _htpController.GetAllBodilyDamageOfDivision(Convert.ToInt32(dd_division.SelectedValue));

            //Call the Polm Controller method to get All Bodily Damage and assigned to the List.
            var allbdlist = _htpController.GetAllBodilyDamage(true);
            List<PolmDto> unassignedbdlist = null;

            //Checking if list is not empty or Null.
            if (allbdlist != null)
            {
                unassignedbdlist = new List<PolmDto>();

                //Iterate through the all items of the List
                foreach (var polmDto in allbdlist)
                {
                    //Calling of the Local Defined method which find out the assigned and Unassigned Bodily Damage Values.
                    if (!FindValue(assignbdlist, polmDto.Id))
                    {
                        //If item is not assinged then add in Unassgined List
                        unassignedbdlist.Add(polmDto);
                    }
                }

            }
            //Checking if list is not empty or Null then bind with Grid.
            if (unassignedbdlist != null)
            {
                lbUnassignedBD.DataSource = unassignedbdlist;
                lbUnassignedBD.DataTextField = "Name";
                lbUnassignedBD.DataValueField = "Id";
                lbUnassignedBD.DataBind();
            }
            else
            {
                lbUnassignedBD.Items.Clear();
            }

        }

        /// <summary>
        /// This method is used to Get All Information about the Divisions.
        /// </summary>
        private void FillDivisionCombo()
        {
            var divisionlist = _htpController.GetAllSource(true);
            if (divisionlist == null) return;
            dd_division.DataSource = divisionlist;
            dd_division.DataTextField = "Name";
            dd_division.DataValueField = "Id";
            dd_division.DataBind();
        }

        /// <summary>
        /// This method is used to Call for populating All Lists.
        /// </summary>
        private void FillAllLists()
        {
            FillAssignedQlmd();
            FillAssignedBd();
            FillUnAssignedQlmd();
            FillUnAssignedBd();
        }

        /// <summary>
        /// This method is used to Set Or Empty All the lists or Controls.
        /// </summary>
        private void EmptyAllLists()
        {
            lbAssignedBD.Items.Clear();
            lbAssignedQLMD.Items.Clear();
            lbUnassignedBD.Items.Clear();
            lbUnAssignedQLMD.Items.Clear();
            //Fahad 6766 04/28/2010 Two separate Labels Defined for both Quick legal Matter Description and as well as bodily Damage setting
            lblBodilyDamageMessage.Text = string.Empty;
            lblQLMDMessage.Text = string.Empty;
        }

        /// <summary>
        /// This methods sets the List boxes selection on Selection ,Assingmen and Un-Assignment
        /// </summary>
        /// <param name="totalListCount">Total No of items in the list.</param>
        /// <param name="totalSelectedItem">Total Selected Items in the List.</param>
        /// <param name="loopCount">No of times loop iterate on the items of the list.</param>
        /// <param name="listId">Id of the list box.</param>
        /// <param name="imgBtnId">Id of the image button.</param>
        private void SetListView(int totalListCount, int totalSelectedItem, int loopCount, ListBox listId, ImageButton imgBtnId)
        {
            if ((listId.Items.Count > 1) && (loopCount != totalListCount))
            {
                listId.Items[loopCount - (totalSelectedItem - 1)].Selected = true;
            }
            else if (listId.Items.Count == 0)
            {
                imgBtnId.Enabled = false;
                listId.Items.Clear();
            }
            else if (listId.Items.Count == 1 || loopCount == totalListCount)
            {
                listId.Items[0].Selected = true;
            }
            else
            {
                imgBtnId.Enabled = true;
            }
        }

        /// <summary>
        /// This method sets button Disabled/Enabled property on the basis of items in List Box. 
        /// </summary>
        private void SetDefaultView()
        {
            imgUnassignBodilyDamage.Enabled = lbAssignedBD.Items.Count != 0;
            imgAssignBodilyDamage.Enabled = lbUnassignedBD.Items.Count != 0;
            imgAssignQLMD.Enabled = lbUnAssignedQLMD.Items.Count != 0;
            imgUnassignQLMD.Enabled = lbAssignedQLMD.Items.Count != 0;

        }

        #endregion


    }
}

﻿using System;
using System.Web.UI.WebControls;
using lntechNew.Components.ClientInfo;
using HTP.ClientController;

namespace HTP.Configuration.Polm
{
    /// <summary>
    /// This class represents all information about the Language.
    /// </summary>
    public partial class Languages : WebComponents.BasePage
    {
        #region Varibles

        //Variable Declared for POLM controller class
        readonly PolmController _htpController = new PolmController();

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Checking Access Type if Not Primary then redirecting to Login Error Page
                if (AccessType != 2)
                {
                    Response.Redirect("~/LoginAccesserror.aspx", false);
                }
                else if (!IsPostBack)
                    FillGrid();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                switch (btnAdd.Text)
                {
                    case "Add":
                        if (Add())
                        {
                            SetEmptyControl();
                            FillGrid();
                            lbl_Message.Text = "Record added successfully.";
                        }
                        else
                            lbl_Message.Text = "Record not added successfully.";
                        break;
                    case "Update":
                        if (Update())
                        {
                            SetEmptyControl();
                            FillGrid();
                            lbl_Message.Text = "Record updated successfully.";
                        }
                        else
                            lbl_Message.Text = "Record not updated successfully.";
                        break;
                }

            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        protected void gvLanguage_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                switch (e.CommandName)
                {
                    case "ImgDelete":
                        {
                            var associatedCases = Convert.ToInt32((((HiddenField)gvLanguage.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("hfcases")).Value));
                            if (associatedCases == 0)
                            {
                                var id = Convert.ToInt32((((HiddenField)gvLanguage.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("hfValueId")).Value));
                                _htpController.DeleteLanguage(id);
                                SetEmptyControl();
                                lbl_Message.Text = "Record deleted successfully";
                                FillGrid();
                            }
                            else
                                lbl_Message.Text = "Language cannot be deleted as it is associated with case. Please update case information first then try again.";
                        }
                        break;
                    case "lnkbutton":
                        txtLanguage.Text = ((LinkButton)gvLanguage.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("lnkbtnValue")).Text;
                        ViewState["ValueID"] = ((HiddenField)gvLanguage.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("hfValueId")).Value;
                        chkIsActive.Checked = Convert.ToBoolean(((HiddenField)gvLanguage.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("hfIsactive")).Value);
                        btnAdd.Text = "Update";
                        break;
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void gvLanguage_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ((LinkButton)e.Row.FindControl("lnkbtnValue")).CommandArgument = Convert.ToString(e.Row.RowIndex);
                    ((ImageButton)e.Row.FindControl("ImgDelete")).CommandArgument = Convert.ToString(e.Row.RowIndex);
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void lnk_AddNewRecord_Click(object sender, EventArgs e)
        {
            try
            {
                SetEmptyControl();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        #endregion

        #region Method

        /// <summary>
        /// This method used to set the default view of all the controls on the page.
        /// </summary>
        private void SetEmptyControl()
        {
            lbl_Message.Text = string.Empty;
            txtLanguage.Text = string.Empty;
            chkIsActive.Checked = false;
            if (ViewState != null) ViewState["ValueID"] = String.Empty;
            btnAdd.Text = "Add";
        }

        /// <summary>
        /// This method is used to Fill Data into the Grid.
        /// </summary>
        private void FillGrid()
        {
            var languagelist = _htpController.GetAllLanguage(null);
            if (languagelist != null)
            {
                gvLanguage.DataSource = languagelist;
                gvLanguage.DataBind();
            }
            else
            {
                gvLanguage.DataSource = null;
                gvLanguage.DataBind();
            }
        }

        /// <summary>
        /// This method is used to Add/Insert New Language.
        /// </summary>
        /// <returns>True/False</returns>
        private bool Add()
        {
            return _htpController.AddLanguage(Server.HtmlEncode(txtLanguage.Text), chkIsActive.Checked);
        }

        /// <summary>
        /// This Method used to Update the existing Language.
        /// </summary>
        /// <returns>True/False</returns>
        private bool Update()
        {
            return _htpController.UpdateLanguage(Convert.ToInt32(ViewState["ValueID"]), Server.HtmlEncode(txtLanguage.Text), chkIsActive.Checked);
        }

        #endregion
    }
}

﻿using System;
using System.Web.UI.WebControls;
using lntechNew.Components.ClientInfo;
using HTP.ClientController;

namespace HTP.Configuration.Polm
{
    /// <summary>
    /// This class represents all information about the Bodily Damage Setting.
    /// </summary>
    public partial class BodilyDamageSetting : WebComponents.BasePage
    {
        #region Variables

        //Variable Declared for POLM controller class
        readonly PolmController _htpController = new PolmController();

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Checking Access Type if Not Primary then redirecting to Login Error Page
                if (AccessType != 2)
                {
                    Response.Redirect("~/LoginAccesserror.aspx", false);
                }
                else if (!IsPostBack)
                    FillGrid();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                switch (btnAdd.Text)
                {
                    case "Add":
                        if (Add())
                        {
                            SetEmptyControl();
                            FillGrid();
                            lbl_Message.Text = "Record added successfully.";
                        }
                        else
                            lbl_Message.Text = "Record not added successfully.";
                        break;
                    case "Update":
                        if (Update())
                        {
                            SetEmptyControl();
                            FillGrid();
                            lbl_Message.Text = "Record updated successfully.";
                        }
                        else
                            lbl_Message.Text = "Record not updated successfully.";
                        break;
                }

            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        protected void gvBd_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                switch (e.CommandName)
                {
                    case "image":
                        {
                            var associatedCases = Convert.ToInt32((((HiddenField)gvBd.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("hfcases")).Value));

                            //Checking If case is not associated then Delete process will perform.
                            if (associatedCases == 0)
                            {
                                var id = Convert.ToInt32((((HiddenField)gvBd.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("hfValueId")).Value));

                                //Call the Polm Controller method to Delete Bodily Damage.
                                _htpController.DeleteBodilyDamage(id);
                                SetEmptyControl();
                                lbl_Message.Text = "Record deleted successfully";
                                FillGrid();
                            }
                            else
                                lbl_Message.Text = "Bodily damage cannot be deleted as it is associated with source/case. Please update source/case information first then try again.";
                        }
                        break;
                    case "lnkbutton":
                        txtBodilyDamage.Text = ((LinkButton)gvBd.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("lnkbtnValue")).Text;
                        ViewState["ValueID"] = ((HiddenField)gvBd.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("hfValueId")).Value;
                        chkIsActive.Checked = Convert.ToBoolean(((HiddenField)gvBd.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("hfIsactive")).Value);
                        btnAdd.Text = "Update";
                        break;
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void gvBd_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ((LinkButton)e.Row.FindControl("lnkbtnValue")).CommandArgument = Convert.ToString(e.Row.RowIndex);
                    ((ImageButton)e.Row.FindControl("ImgDelete")).CommandArgument = Convert.ToString(e.Row.RowIndex);
                }

            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        protected void lnk_AddNewRecord_Click(object sender, EventArgs e)
        {
            try
            {
                SetEmptyControl();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// This method is used to Fill Data into the Grid.
        /// </summary>
        private void FillGrid()
        {
            //Call POLM Controller method to Get All the Bodily Damage Setting.
            var bodilyDamagelist = _htpController.GetAllBodilyDamage(null);

            //Checking if list is not empty or Null then bind with Grid.
            if (bodilyDamagelist != null)
            {
                gvBd.DataSource = bodilyDamagelist;
                gvBd.DataBind();
            }
            else
            {
                gvBd.DataSource = null;
                gvBd.DataBind();
            }
        }

        /// <summary>
        /// This method is used to Add/Insert New Bodily Damage Setting.
        /// </summary>
        /// <returns>True/False</returns>
        private bool Add()
        {
            //Call POLM Controller method to Add the Bodily Damage Setting.
            return _htpController.AddBodilyDamage(Server.HtmlEncode(txtBodilyDamage.Text), chkIsActive.Checked);
        }

        /// <summary>
        /// This Method used to Update the existing Bodily Damage Setting.
        /// </summary>
        /// <returns>True/False</returns>
        private bool Update()
        {
            //Call POLM Controller method to Update the Bodily Damage Setting.
            return _htpController.UpdateBodilyDamage(Convert.ToInt32(ViewState["ValueID"]), Server.HtmlEncode(txtBodilyDamage.Text), chkIsActive.Checked);
        }

        /// <summary>
        /// This method used to set the default view of all the controls on the page.
        /// </summary>
        private void SetEmptyControl()
        {
            lbl_Message.Text = string.Empty;
            txtBodilyDamage.Text = string.Empty;
            chkIsActive.Checked = false;
            if (ViewState != null) ViewState["ValueID"] = String.Empty;
            btnAdd.Text = "Add";
        }

        #endregion
    }
}

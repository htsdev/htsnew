﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CaseStatusSetting.aspx.cs"
    Inherits="HTP.Configuration.Polm.CaseStatusSetting" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="~/WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Case Status Setting</title>
    <link href="../../Styles.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
        function ShowDeleteMessage()
        {
            if (confirm("Are you sure you want to delete?"))
                return true;
            return false;
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <aspnew:ScriptManager ID="ScriptManager1" runat="server">
            <Scripts>
                <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="~/Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
            </Scripts>
        </aspnew:ScriptManager>
        <aspnew:UpdatePanel ID="pnl_main" runat="server">
            <ContentTemplate>
                <table id="TableMain" cellspacing="0" cellpadding="0" width="780px" align="center"
                    border="0">
                    <tr>
                        <td>
                            <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td background="../../Images/separator_repeat.gif" height="9" width="750px">
                        </td>
                    </tr>
                    <tr>
                        <td class="clsLeftPaddingTable" align="center">
                            <table style="width: 60%">
                                <tr>
                                    <td align="right">
                                        <asp:Label ID="lblCaseStatus" runat="server" CssClass="clssubhead" Text="Case Status :"></asp:Label>
                                    </td>
                                    <td align="left" valign="middle">
                                        <asp:TextBox ID="TxtCaseStatus" runat="server" CssClass="clsInputadministration"
                                            Width="200px" MaxLength="60"></asp:TextBox>&nbsp;
                                        <asp:CheckBox ID="chkIsActive" runat="server" Text="IsActive" CssClass="clsLeftPaddingTable" />&nbsp;
                                        <asp:Button ID="btnAdd" runat="server" CssClass="clsbutton" OnClick="btnAdd_Click"
                                            Text="Add" Width="60px" ValidationGroup="contact" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td align="left" valign="middle">
                                        <asp:ValidationSummary ID="ValidationSummary1" CssClass="clssubhead" DisplayMode="List"
                                            runat="server" ValidationGroup="contact" />
                                        <asp:RequiredFieldValidator ID="rfvValue" CssClass="clssubhead" ControlToValidate="TxtCaseStatus"
                                            runat="server" Display="None" ErrorMessage="Please enter case status." ValidationGroup="contact"></asp:RequiredFieldValidator>
                                        <%-- Fahad 7783 05/10/2010 Corrected the spelling of Alphabets in Error message and allow @,',? character in Name--%>
                                        <asp:RegularExpressionValidator ID="regexpName" CssClass="clssubhead" runat="server"
                                            ValidationGroup="contact" ErrorMessage="Please insert valid case status." Display="None"
                                            ControlToValidate="TxtCaseStatus" ValidationExpression="^[ a-zA-Z!”$%&’()@'?*\+,+-\/;\[\\\]\^_`{|}~]+$" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../../Images/separator_repeat.gif" height="9" width="750px">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="819px" height="34px" background="../../Images/subhead_bg.gif" class="clssubhead">
                                <tr>
                                    <td align="left" style="width=410px" class="clssubhead">
                                        &nbsp;Case Status &nbsp;&nbsp;
                                    </td>
                                    <td align="right" style="width=409px" class="clssubhead">
                                        <asp:LinkButton ID="lnk_AddNewRecord" runat="server" OnClick="lnk_AddNewRecord_Click">Add New Record</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../../Images/separator_repeat.gif" height="9" width="750px">
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="width: 819px">
                            <asp:Label ID="lbl_Message" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                            <br />
                            <asp:GridView ID="gvCaseStatus" runat="server" AutoGenerateColumns="False" CssClass="clsLeftPaddingTable"
                                CellPadding="3" CellSpacing="0" OnRowCommand="gvCaseStatus_RowCommand" OnRowDataBound="gvCaseStatus_RowDataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="S#" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSno" runat="server" CssClass="clssubhead" Text='<%# Eval("SerialNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Case Status" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkbtnValue" runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'
                                                CommandName="lnkbutton"></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Active" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblIsActive" runat="server" CssClass="clsLabel" Text='<%# (Convert.ToInt32(Eval("IsActive")) == 0) ? "No" : "Yes" %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImgDelete" runat="server" ImageUrl="~/Images/remove2.gif" CommandArgument='<%# Eval("Id") %>'
                                                CommandName="image" OnClientClick="return ShowDeleteMessage();" />
                                            <asp:HiddenField ID="hfValueId" runat="server" Value='<%# Eval("Id") %>' />
                                            <asp:HiddenField ID="hfcases" runat="server" Value='<%# Eval("AssociatedCase") %>' />
                                            <asp:HiddenField ID="hfIsactive" runat="server" Value='<%# Eval("IsActive") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                        <td align="center">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td background="../../Images/separator_repeat.gif" height="9" width="750px">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc1:Footer ID="Footer1" runat="server" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
            <Triggers>
                <aspnew:AsyncPostBackTrigger ControlID="btnAdd" EventName="Click" />
            </Triggers>
        </aspnew:UpdatePanel>
    </div>
    </form>
</body>
</html>

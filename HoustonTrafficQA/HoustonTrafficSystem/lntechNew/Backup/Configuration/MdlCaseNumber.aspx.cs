﻿using System;
using System.Web.UI.WebControls;
using HTP.ClientController;
using lntechNew.Components.ClientInfo;
using RoleBasedSecurity.DataTransferObjects;
using System.Data;
using FrameWorkEnation.Components;

namespace HTP.Configuration
{
    /// <summary>
    /// This class represents all information about the Company.
    /// </summary>
    public partial class MdlCaseNumber : WebComponents.BasePage
    {

        #region Variables

        //Variable Declared for role based security controller class
        readonly RoleBasedSecurityController _roleBasedSecurityController = new RoleBasedSecurityController();
        clsENationWebComponents ClsDb = new clsENationWebComponents();

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lbl_Message.Text = string.Empty;
                //Checking Access Type if Not Primary then redirecting to Login Error Page
                if (AccessType != 2)
                {
                    Response.Redirect("~/LoginAccesserror.aspx", false);
                }
                else if (!IsPostBack)
                {
                    //Call Role Based Security Controller method to get User by Traffic Program ID and assigned to the List.
                    var tpuserList = _roleBasedSecurityController.GetUserByTpId(new UserDto { TpUserId = EmpId });

                    //Setting Userid in View State for further Use.
                    ViewState["LoginEmployeeId"] = tpuserList[0].UserId;
                    FillGrid();
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    lbl_Message.Text = string.Empty;
                    switch (btnAdd.Text)
                    {
                        case "Add":
                            if (Add())
                            {
                                SetEmptyControl();
                                FillGrid();
                                lbl_Message.Text = "Record added successfully.";
                            }
                            else
                                lbl_Message.Text = "Record not added successfully.";
                            break;
                        case "Update":
                            if (Update())
                            {
                                SetEmptyControl();
                                FillGrid();
                                lbl_Message.Text = "Record updated successfully.";
                            }
                            else
                                lbl_Message.Text = "Record not updated successfully.";
                            break;
                    }
                }
                else
                {
                    rfvValue.ErrorMessage = "Controls are not validated properly. Please try again.";
                    ValidationSummary1.ShowSummary = true;
                }

            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        protected void gvMDLNumberRowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                lbl_Message.Text = string.Empty;
                switch (e.CommandName)
                {
                    case "deleting":
                        {
                            var id = Convert.ToInt32((((HiddenField)gvMDLNumber.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("HfValueId")).Value));
                            
                            string[] key1 = {"@CaseId"};
                            object[] value1 = {id};

                            ClsDb.InsertBySPArr("USP_HTP_Delete_MDLCase", key1, value1);
                            SetEmptyControl();
                            lbl_Message.Text = "Record deleted successfully";
                            FillGrid();
                        }
                        break;
                    case "editing":
                        {
                            txtMDLNumber.Text = ((Label)gvMDLNumber.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("lblMDLNumber")).Text;
                            ViewState["ValueID"] = ((HiddenField)gvMDLNumber.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("HfValueId")).Value;
                            chkIsActive.Checked = Convert.ToBoolean(((HiddenField)gvMDLNumber.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("HfIsactive")).Value);
                            btnAdd.Text = "Update";
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void gvMDLNumberRowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ((LinkButton)e.Row.FindControl("lnkbtnDelete")).CommandArgument = Convert.ToString(e.Row.RowIndex);
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        #endregion

        #region Method

        /// <summary>
        /// This method used to set the default view of all the controls on the page.
        /// </summary>
        private void SetEmptyControl()
        {
            txtMDLNumber.Text = string.Empty;
            chkIsActive.Checked = false;
            lbl_Message.Text = string.Empty;
            if (ViewState != null) ViewState["ValueID"] = String.Empty;
            btnAdd.Text = "Add";
        }

        /// <summary>
        /// This method is used to Fill Data into the Grid.
        /// </summary>
        private void FillGrid()
        {
            DataTable dtMDLCases = ClsDb.Get_DS_BySPArr("USP_HTP_Get_MDLCase").Tables[0]; 
            
            if (dtMDLCases != null)
            {
                dtMDLCases.Columns.Add("SNo");

                if (dtMDLCases.Rows.Count > 0)
                {
                    for (int i = 0; i < dtMDLCases.Rows.Count; i++)
                    {
                        dtMDLCases.Rows[i]["SNo"] = i;
                    }
                }


                gvMDLNumber.DataSource = dtMDLCases;
                gvMDLNumber.DataBind();
            }
            else
            {
                gvMDLNumber.DataSource = null;
                gvMDLNumber.DataBind();
            }
        }

        /// <summary>
        /// This method is used to Add/Insert New MDL Case Number.
        /// </summary>
        /// <returns>True/False</returns>
        private bool Add()
        {
            string[] key1 = {"@CaseNumber", "@Date", "@EmployeeID", "@isActive"};
            object[] value1 = {txtMDLNumber.Text.Trim(), DateTime.Now, 3991, chkIsActive.Checked};

            ClsDb.InsertBySPArr("USP_HTP_Insert_MDLCase", key1, value1);
            
            return true;
        }

        /// <summary>
        /// This Method used to Update the existing MDL Case Number.
        /// </summary>
        /// <returns>True/False</returns>
        private bool Update()
        {
            string[] key1 = { "@CaseNumber", "@Date", "@EmployeeID", "@isActive", "@Id" };
            object[] value1 = { txtMDLNumber.Text.Trim(), DateTime.Now, 3991, chkIsActive.Checked, ViewState["ValueID"].ToString() };

            ClsDb.InsertBySPArr("USP_HTP_Update_MDLCase", key1, value1);
            return true;
        }

        #endregion
    }
}

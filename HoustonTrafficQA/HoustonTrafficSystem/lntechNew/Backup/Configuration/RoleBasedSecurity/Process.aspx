﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Process.aspx.cs" Inherits="HTP.Configuration.RoleBasedSecurity.Process" %>

<%@ Register Src="~/WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="~/WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="~/WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Process</title>
    <link href="../../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <aspnew:ScriptManager ID="ScriptManager1" runat="server">
        <Scripts>
            <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="~/Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
        </Scripts>
    </aspnew:ScriptManager>
    <aspnew:UpdatePanel ID="pnl_main" runat="server">
        <ContentTemplate>
            <div>
                <table id="TableMain" cellspacing="0" cellpadding="0" width="780px" align="center"
                    border="0">
                    <tr>
                        <td>
                            <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td background="../../Images/separator_repeat.gif" height="9" width="750px">
                        </td>
                    </tr>
                    <tr>
                        <td class="clsLeftPaddingTable">
                            <table style="width: 100%">
                                <tr id="treditprocess" runat="server">
                                    <td align="left" valign="middle">
                                        <asp:Label ID="lblApplication" runat="server" CssClass="clssubhead" Text="Application Name :"></asp:Label>
                                        &nbsp;
                                        <asp:DropDownList ID="ddlApplication" runat="server" CssClass="clsInputadministration"
                                            Width="100px">
                                        </asp:DropDownList>
                                        &nbsp;
                                        <asp:Label ID="lblCompany" runat="server" CssClass="clssubhead" Text="Process Name :"></asp:Label>
                                        &nbsp;
                                        <asp:TextBox ID="txtProcess" runat="server" CssClass="clsInputadministration" Width="200px"
                                            MaxLength="100"></asp:TextBox>
                                        &nbsp;
                                        <asp:CheckBox ID="chkIsActive" runat="server" Text="IsActive" CssClass="clsLeftPaddingTable" />&nbsp;
                                        <asp:Button ID="btnUpdate" runat="server" CssClass="clsbutton" Text="Update" Width="60px"
                                            ValidationGroup="contact" OnClick="btnUpdate_Click" />&nbsp;&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" valign="middle">
                                        <asp:ValidationSummary ID="ValidationSummary1" CssClass="clssubhead" DisplayMode="List"
                                            runat="server" ValidationGroup="contact" />
                                        <asp:RequiredFieldValidator ID="rfvProcess" CssClass="clssubhead" ControlToValidate="txtProcess"
                                            runat="server" Display="None" ErrorMessage="Please enter process name." ValidationGroup="contact"></asp:RequiredFieldValidator>
                                        <asp:RequiredFieldValidator ID="rfvApplication" CssClass="clssubhead" ControlToValidate="ddlApplication"
                                            runat="server" Display="None" InitialValue="0" ErrorMessage="Please select application."
                                            ValidationGroup="contact"></asp:RequiredFieldValidator>
                                        <%-- Fahad 7783 05/10/2010 Corrected the spelling of Alphabets in Error message and allow @,',? character in Name--%>
                                        <asp:RegularExpressionValidator ID="regexpName" CssClass="clssubhead" runat="server"
                                            ValidationGroup="contact" ErrorMessage="Please insert valid process name." Display="None"
                                            ControlToValidate="txtProcess" ValidationExpression="^[ a-zA-Z!”$%&’()@'?*\+,+-\/;\[\\\]\^_`{|}~]+$" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../../Images/separator_repeat.gif" height="9" width="780px">
                        </td>
                    </tr>
                    <tr>
                        <td width="100%">
                            <table style="width: 100%" height="34px" background="../../Images/subhead_bg.gif"
                                class="clssubhead">
                                <tr>
                                    <td align="left" style="width: 200px" class="clssubhead">
                                        &nbsp;Process &nbsp;&nbsp;
                                    </td>
                                    <td align="right" class="clssubhead">
                                        <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../../Images/separator_repeat.gif" height="9" width="780px">
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Label ID="lbl_Message" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                            <asp:GridView ID="gvProcess" runat="server" AutoGenerateColumns="False" CssClass="clsLeftPaddingTable"
                                CellPadding="3" CellSpacing="0" Width="780px" AllowPaging="true" PageSize="20"
                                OnRowCommand="gvProcess_RowCommand" OnRowDataBound="gvProcess_RowDataBound" OnPageIndexChanging="gvProcess_PageIndexChanging">
                                <Columns>
                                    <asp:TemplateField HeaderText="S#" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="LblSno" runat="server" CssClass="GridItemStyle" Text='<%# Eval("SerialNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Process Name" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LnkbtnValue" runat="server" Text='<%# Eval("ProcessName") %>'
                                                ToolTip='<%# Eval("ProcessName") %>' CommandName="lnkbutton"></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Application Name" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="LblApplication" runat="server" CssClass="clsLabel" Text='<%# Eval("ApplicationName") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Active" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="LblIsActive" runat="server" CssClass="clsLabel" Text='<%# (Convert.ToInt32(Eval("IsActive")) == 0) ? "No" : "Yes" %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Inserted By" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="LblInserted" runat="server" CssClass="clsLabel" Text='<%# Eval("InsertedUserName") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Updated By" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="Lblupdated" runat="server" CssClass="clsLabel" Text='<%# Eval("LastUpDateUserName") %>'></asp:Label>
                                            <asp:HiddenField ID="HfValueId" runat="server" Value='<%# Eval("ProcessId") %>' />
                                            <asp:HiddenField ID="HfIsactive" runat="server" Value='<%# Eval("IsActive") %>' />
                                            <asp:HiddenField ID="HfApplicationId" runat="server" Value='<%# Eval("ApplicationId") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle HorizontalAlign="Center" />
                                <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                    FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                            </asp:GridView>
                        </td>
                        <td align="center">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td background="../../Images/separator_repeat.gif" height="9" width="750px">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc1:Footer ID="Footer1" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
        <Triggers>
            <aspnew:AsyncPostBackTrigger ControlID="btnUpdate" EventName="Click" />
        </Triggers>
    </aspnew:UpdatePanel>
    </form>
</body>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserInRole.aspx.cs" Inherits="HTP.Configuration.RoleBasedSecurity.UserInRole" %>

<%@ Register Src="~/WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="~/WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>User In Role</title>
    <link href="../../Styles.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
        function ShowDeleteMessage()
        {
            if (confirm("Are you sure you want to delete?"))
                return true;
            return false;
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <aspnew:ScriptManager ID="ScriptManager1" runat="server">
        <Scripts>
            <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="~/Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
        </Scripts>
    </aspnew:ScriptManager>
    <aspnew:UpdatePanel ID="pnl_main" runat="server">
        <ContentTemplate>
            <div>
                <table id="TableMain" cellspacing="0" cellpadding="0" width="780px" align="center"
                    border="0">
                    <tr>
                        <td>
                            <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td background="../../Images/separator_repeat.gif" height="9" width="750px">
                        </td>
                    </tr>
                    <tr>
                        <td class="clsLeftPaddingTable">
                            <table style="width: 100%">
                                <tr>
                                    <td valign="middle">
                                        <asp:Label ID="lblUser1" runat="server" CssClass="clssubhead" Text="User :"></asp:Label>
                                        &nbsp;
                                        <asp:DropDownList ID="ddlUser" runat="server" CssClass="clsInputadministration" Width="100px">
                                        </asp:DropDownList>
                                        &nbsp;
                                        <asp:Label ID="lblCompany1" runat="server" CssClass="clssubhead" Text="Company :"></asp:Label>
                                        &nbsp;
                                        <asp:DropDownList ID="ddlCompany" runat="server" CssClass="clsInputadministration"
                                            Width="100px">
                                        </asp:DropDownList>
                                        &nbsp;
                                        <asp:Label ID="lblRole1" runat="server" CssClass="clssubhead" Text="Role :"></asp:Label>
                                        &nbsp;
                                        <asp:DropDownList ID="ddlRole" runat="server" CssClass="clsInputadministration" Width="100px">
                                        </asp:DropDownList>
                                        &nbsp;
                                        <asp:CheckBox ID="chkIsActive" runat="server" Text="IsActive" CssClass="clsLeftPaddingTable" />&nbsp;
                                        <asp:Button ID="btnAdd" runat="server" CssClass="clsbutton" OnClick="btnAdd_Click"
                                            Text="Add" Width="60px" ValidationGroup="contact" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" valign="middle">
                                        <asp:ValidationSummary ID="ValidationSummary1" CssClass="clssubhead" DisplayMode="List"
                                            runat="server" ValidationGroup="contact" />
                                        <asp:RequiredFieldValidator ID="rfvApplication" CssClass="clssubhead" ControlToValidate="ddlUser"
                                            runat="server" Display="None" InitialValue="0" ErrorMessage="Please select user."
                                            ValidationGroup="contact"></asp:RequiredFieldValidator>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" CssClass="clssubhead" ControlToValidate="ddlCompany"
                                            runat="server" Display="None" InitialValue="0" ErrorMessage="Please select company."
                                            ValidationGroup="contact"></asp:RequiredFieldValidator>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" CssClass="clssubhead" ControlToValidate="ddlRole"
                                            runat="server" Display="None" InitialValue="0" ErrorMessage="Please select role."
                                            ValidationGroup="contact"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../../Images/separator_repeat.gif" height="9" width="750px">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="819px" height="34px" background="../../Images/subhead_bg.gif" class="clssubhead">
                                <tr>
                                    <td align="left" style="width: 410px" class="clssubhead">
                                        &nbsp;User In Role
                                    </td>
                                    <td align="right" style="width: 409px" class="clssubhead">
                                        <asp:LinkButton ID="lnk_AddNewRecord" runat="server" OnClick="lnk_AddNewRecord_Click">Add New Record</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../../Images/separator_repeat.gif" height="9" width="750px">
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="width: 819px">
                            <asp:Label ID="lbl_Message" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                            <asp:GridView ID="gvUserInRole" runat="server" AutoGenerateColumns="False" CssClass="clsLeftPaddingTable"
                                CellPadding="3" CellSpacing="0" OnRowCommand="gvUserInRole_RowCommand" OnRowDataBound="gvUserInRole_RowDataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="S#" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="LblSno" runat="server" CssClass="GridItemStyle" Text='<%# Eval("SerialNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="User Name" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LnkbtnUser" runat="server" Text='<%# Eval("UserName") %>' ToolTip='<%# Eval("UserName") %>'
                                                CommandName="lnkbutton"></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Company Name" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="LblCompany" runat="server" CssClass="GridItemStyle" Text='<%# Eval("CompanyName") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Role Name" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="LblRole" runat="server" CssClass="GridItemStyle" Text='<%# Eval("RoleName") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Inserted By" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="LblnsertedBy" runat="server" CssClass="GridItemStyle" Text='<%# Eval("InsertedUserName") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Updated By" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="LblUpdatedBy" runat="server" CssClass="GridItemStyle" Text='<%# Eval("LastUpDateUserName") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Active" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="LblIsActive" runat="server" CssClass="GridItemStyle" Text='<%# (Convert.ToInt32(Eval("IsActive")) == 0) ? "No" : "Yes" %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImgDelete" runat="server" ImageUrl="~/Images/remove2.gif" CommandArgument='<%# Eval("UserRoleId") %>'
                                                CommandName="image" OnClientClick="return ShowDeleteMessage();" />
                                            <asp:HiddenField ID="HfValueId" runat="server" Value='<%# Eval("UserRoleId") %>' />
                                            <asp:HiddenField ID="HfIsactive" runat="server" Value='<%# Eval("IsActive") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                        <td align="center">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td background="../../Images/separator_repeat.gif" height="9" width="750px">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc1:Footer ID="Footer1" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
        <Triggers>
            <aspnew:AsyncPostBackTrigger ControlID="btnAdd" EventName="Click" />
        </Triggers>
    </aspnew:UpdatePanel>
    </form>
</body>
</html>

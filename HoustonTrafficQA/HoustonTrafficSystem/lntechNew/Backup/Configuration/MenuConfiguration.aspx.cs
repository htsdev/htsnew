﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using FrameWorkEnation.Components;
//Nasir 7067 01/02/2010 remove warning 
using System.Data;
using System.Web.Services;
using lntechNew.Components.ClientInfo;

//Waqas 7077 12/14/2009
namespace HTP.Configuration
{
    /// <summary>
    /// This page is used to for menu configuration 
    /// </summary>
    public partial class MenuConfiguration : System.Web.UI.Page
    {
        clsSession ClsSession = new clsSession();
        clsLogger bugTracker = new clsLogger();
        /// <summary>
        /// This event is called on loading of the page.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                int Empid = 0;
                if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false ||
                        (!int.TryParse(ClsSession.GetCookie("sEmpID", this.Request), out Empid)))
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else
                {
                    //If administrator user
                    if (ClsSession.GetCookie("sAccessType", this.Request).ToString() != "2")
                    {
                        Response.Redirect("../LoginAccesserror.aspx", false);
                        Response.End();
                    }
                    else //To stop page further execution
                    {
                        //page method call it
                        MenuControl1.PageMethod += delegate()
                        {
                            lbl_Message.Text = MenuControl1.ErrorMessage;
                            FillTree();
                        };

                        if (!IsPostBack)
                        {
                            lbl_Message.Text = "";
                            FillTree();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);                
            }
        }

        /// <summary>
        /// This method is used to fill the tree on the page.
        /// </summary>
        private void FillTree()
        {

            try
            {
                MenuTree.Nodes.Clear();

                clsENationWebComponents clsDB = new clsENationWebComponents();
                DataSet ds = clsDB.Get_DS_BySP("usp_htp_get_MenuItemsTree");

                StringWriter StrWriterLink = new StringWriter();
                Html32TextWriter ItemWriterLink = new Html32TextWriter(StrWriterLink);

                StringWriter StrWriterLabel = new StringWriter();
                Html32TextWriter ItemWriterLabel = new Html32TextWriter(StrWriterLabel);

                Label LnkButton = new Label();
                LnkButton.Text = "<u>Add</u>";
                LnkButton.CssClass = "Label";
                //add click event attribute to show popup control to add first level menu item
                LnkButton.Attributes.Add("OnClick", "return ShowAddControl(1,0);");
                LnkButton.RenderControl(ItemWriterLink);

                Label lbl = new Label();
                lbl.Text = "Traffic Program Menu";
                lbl.RenderControl(ItemWriterLabel);

                //Add Label in tree as node
                TreeNode tn = new TreeNode(StrWriterLabel.ToString() + " - " + StrWriterLink.ToString());

                if (MenuTree.Nodes.Count == 0)
                {
                    tn.ToolTip = "";
                    MenuTree.Nodes.Add(tn);
                }

                if (ds.Tables.Count > 0)
                {
                    string PreviousNode1 = "";
                    string PreviousNode2 = "";
                    int CurrentNodeID = -1;
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        DataRow dr = ds.Tables[0].Rows[i];

                        string CurrentNode1 = dr["FirstID"].ToString();
                        if (PreviousNode1 != CurrentNode1)
                        {
                            StrWriterLink = new StringWriter();
                            ItemWriterLink = new Html32TextWriter(StrWriterLink);

                            StrWriterLabel = new StringWriter();
                            ItemWriterLabel = new Html32TextWriter(StrWriterLabel);

                            string MenuID = dr["FirstID"].ToString();
                            //Create label object
                            LnkButton = new Label();
                            LnkButton.Text = "<u>Add Sub Menu</u>";
                            LnkButton.CssClass = "Label";
                            //add click event attribute to show popup control to add menu item
                            LnkButton.Attributes.Add("OnClick", "return ShowAddControl(2," + MenuID + ");");
                            LnkButton.RenderControl(ItemWriterLink);

                            lbl = new Label();
                            lbl.Text = dr["FIRST"].ToString();
                            //add click event attribute to show popup control to update first level menu item
                            lbl.Attributes.Add("OnClick", "return ShowUpdateControl(1," + MenuID + ");");
                            lbl.RenderControl(ItemWriterLabel);

                            //Add Label in tree as node
                            tn = new TreeNode(StrWriterLabel.ToString() + " - " + StrWriterLink.ToString());
                            tn.ToolTip = "";
                            MenuTree.Nodes[0].ChildNodes.Add(tn);

                            PreviousNode1 = CurrentNode1;
                            CurrentNodeID++;

                            string CurrentNode2 = dr["SecondID"].ToString();
                           
                            StrWriterLink = new StringWriter();
                            ItemWriterLink = new Html32TextWriter(StrWriterLink);

                            StrWriterLabel = new StringWriter();
                            ItemWriterLabel = new Html32TextWriter(StrWriterLabel);


                            MenuID = dr["SecondID"].ToString();

                            if (MenuID != "")
                            {

                                lbl = new Label();
                                lbl.Text = dr["SECOND"].ToString();
                                //add click event attribute to show popup control to update second level menu item
                                lbl.Attributes.Add("OnClick", "return ShowUpdateControl(2," + MenuID + ");");
                                lbl.RenderControl(ItemWriterLabel);

                                tn = new TreeNode(StrWriterLabel.ToString());
                                tn.ToolTip = "";
                                //Add Label in tree as child node
                                MenuTree.Nodes[0].ChildNodes[CurrentNodeID].ChildNodes.Add(tn);

                            }
                            PreviousNode2 = CurrentNode2;
                            
                        }
                        else
                        {
                            string CurrentNode2 = dr["SECOND"].ToString();
                            
                            StrWriterLink = new StringWriter();
                            ItemWriterLink = new Html32TextWriter(StrWriterLink);

                            StrWriterLabel = new StringWriter();
                            ItemWriterLabel = new Html32TextWriter(StrWriterLabel);


                            string MenuID = dr["SecondID"].ToString();
                            if (MenuID != "")
                            {

                                lbl = new Label();
                                lbl.Text = CurrentNode2;
                                //add click event attribute to show popup control to update Second level menu item
                                lbl.Attributes.Add("OnClick", "return ShowUpdateControl(2," + MenuID + ");");
                                lbl.RenderControl(ItemWriterLabel);

                                tn = new TreeNode(StrWriterLabel.ToString());
                                tn.ToolTip = "";
                                //add child node in tree view
                                MenuTree.Nodes[0].ChildNodes[CurrentNodeID].ChildNodes.Add(tn);

                            }
                            PreviousNode2 = CurrentNode2;
                           

                        }

                    }

                    //Collapse all child node
                    for (int i = 0; i < MenuTree.Nodes[0].ChildNodes.Count; i++)
                    {
                        MenuTree.Nodes[0].ChildNodes[i].Collapse();
                    }
                    //if there is not child node then expand all
                    if (MenuTree.Nodes.Count != 0)
                    {
                        MenuTree.Nodes[0].Expand();
                    }
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }        

        /// <summary>
        /// This method is used to show popup
        /// </summary>
        /// <param name="menuid">It represent the menu id of the menu.</param>
        public void ShowPopup(int menuid)
        {
            ModalPopup1.Show();
        }
        
    }
}

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportSettingAttribute.aspx.cs" Inherits="HTP.Configuration.ReportSettingAttribute" %>

<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
    <title>Report Attribute</title><title>Admin</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="../Styles.css" type="text/css" rel="stylesheet" />
    <%-- <script src="../Scripts/Validationfx.js" type="text/javascript"></script>--%>

    <script src="../Scripts/jsDate.js" type="text/javascript"></script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="javascript">
		
		
		function trimAll(sString) 
		{
			while (sString.substring(0,1) == ' ')
			{
				sString = sString.substring(1, sString.length);
			}
			while (sString.substring(sString.length-1, sString.length) == ' ')
			{
				sString = sString.substring(0,sString.length-1);
			}
			return sString;
		}
		
		function alphanumeric(alphane)
        {
	        var numaric = alphane;
	        for(var j=0; j<numaric.length; j++)
		        {
		              var alphaa = numaric.charAt(j);
		              var hh = alphaa.charCodeAt(0);		              
		              //ASCII code for aphabhets
		              if (!((hh > 64 && hh<91) || (hh > 96 && hh<123)))
		              {
		              return false;
		              }    
		        }
	    }
	    function CheckName(name)
        {       
            for ( i = 0 ; i < name.length ; i++)
            {
                var asciicode =  name.charCodeAt(i)
                //If not valid alphabet 
                if (  ! ((asciicode >= 64 && asciicode <=90) || ( asciicode >= 97 && asciicode <=122)||(asciicode >= 48 && asciicode <=57)))
                return false;
           }
            return true;
        }
	    
	    
	    function formSubmit() 
		{		
		   var lname=document.getElementById("tb_aname").value;
            
            if (trimAll(lname) == "") {
	            alert("Please specify Attribute Name");
	            document.getElementById("tb_aname").focus();
	            return false;
            }
            
            if (!IsAlphabets(lname))
            {
                alert("Attribute name should only contains alphabets.");
                document.getElementById("tb_aname").focus();
                return false;
            }           
            
          
            var fname=document.getElementById("tb_sname").value;
            
            if (trimAll(fname) == "") 
            {
	            alert("Please specify Short Name");
	            document.getElementById("tb_sname").focus();
	            return false;
            }
            
            if (!IsAlphabets(fname))
            {
                alert("Short name should only contains alphabets.");
                document.getElementById("tb_sname").focus();
                return false;
            }
            
            if(document.getElementById("ddl_attributetype").value=="0")
            {
                alert("Please select attribute type");
                return false;
            }
                       
            return true;
           }
            //Saeed 8101 10/07/2010 function which allows only alphabets,space & '.'.
            function IsAlphabets(checkString) 
            {
            var tempString="";
            var regExp = /^[A-Z .a-z]$/;
            if(checkString != null && checkString != "")
            {
              for(var i = 0; i < checkString.length; i++)
              { 
                if (!checkString.charAt(i).match(regExp))
                {
                  return false;
                }
              }
            }
            else
            {
              return false;
            }
            return true;
            }
            

	
    </script>
    

</head>
<body>
    <div>
        <form id="form1" runat="server">
        <aspnew:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="0" >
         <Scripts>
                <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
         </Scripts>
        </aspnew:ScriptManager>
        <aspnew:UpdatePanel ID="upnlResult" runat="server">
            <ContentTemplate>
                <table id="TableMain" cellspacing="0" cellpadding="0" width="780" align="center"
                    border="0">
                    <tr>
                        <td colspan="4">
                            <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 14px" width="100%" background="../Images/separator_repeat.gif"
                            colspan="4" height="14">
                        </td>
                    </tr>
                    <tr>
                        <td id="pri" colspan="4">
                            <table id="Table1" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
                                <tr>
                                    <td class="clsLeftPaddingTable" valign="top" colspan="6" style="height: 30px">
                                        <table id="Table2" border="0" bordercolor="#ffffff" cellpadding="1" cellspacing="1"
                                            width="100%">
                                            <tr>
                                                <td class="clssubhead" style="width: 100px">
                                                    Attribute Name :
                                                </td>
                                                <td style="width: 260px">
                                                    <asp:TextBox ID="tb_aname" runat="server" CssClass="clsInputadministration" MaxLength="300"
                                                        Width="250px"></asp:TextBox>
                                                </td>
                                                <td class="clssubhead" align="left" style="width: 100px">
                                                    Short Name :
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="tb_sname" runat="server" CssClass="clsInputadministration" MaxLength="50"
                                                        Width="200px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="clssubhead">
                                                    Description :
                                                </td>
                                                <td style="width: 260px">
                                                    <asp:TextBox ID="tb_description" runat="server" CssClass="clsInputadministration"
                                                        MaxLength="500" Width="250px"></asp:TextBox>
                                                </td>
                                                <td class="clssubhead" align="left">
                                                    Attribute Type :
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddl_attributetype" CssClass="clsInputCombo" runat="server"
                                                        Width="202px">
                                                        <asp:ListItem Value="0">--Choose--</asp:ListItem>
                                                        <asp:ListItem Value="1">Text</asp:ListItem>
                                                        <asp:ListItem Value="2">Number</asp:ListItem>
                                                    </asp:DropDownList>
                                                    &nbsp;
                                                    <asp:Button ID="btn_update" runat="server" CssClass="clsbutton" Text="Add" OnClick="btn_update_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="clsLeftPaddingTable" style="height: 12px" background="../Images/separator_repeat.gif"
                            colspan="4">
                        </td>
                    </tr>
                    <tr>
                        <td class="clssubhead" align="left" background="../Images/subhead_bg.gif" colspan="4">
                            <table width="100%">
                                <tr style="width: 391px; height: 25px;">
                                    <td class="clssubhead" valign="middle">
                                        Report Attribute
                                    </td>
                                    <td class="clssubhead" align="right">
                                        <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click">Add New Record</asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="clsLeftPaddingTable" style="height: 12px" background="../Images/separator_repeat.gif"
                            colspan="4">
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Label ID="lbl_Message" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                            <asp:DataGrid ID="dg_results" runat="server" AutoGenerateColumns="False" BorderColor="White"
                                CssClass="clsLeftPaddingTable" Width="100%" OnItemCommand="dg_results_ItemCommand"
                                PageSize="10">
                                <Columns>
                                    <asp:TemplateColumn HeaderText="Attribute Name" HeaderStyle-HorizontalAlign="Center">
                                        <HeaderStyle CssClass="clssubhead" />
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlnk_attributename" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AttributeName") %>'
                                                Visible="False">
                                            </asp:HyperLink>
                                            <asp:LinkButton ID="lnkbtn_attributename" runat="server" CommandName="click" Text='<%# DataBinder.Eval(Container, "DataItem.AttributeName") %>'>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" CssClass="clsLeftPaddingTable" />
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Short Name" HeaderStyle-HorizontalAlign="Center">
                                        <HeaderStyle CssClass="clssubhead" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_sname" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.ShortName") %>'>
                                            </asp:Label>
                                            <asp:Label ID="lbl_aid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AttributeID") %>'
                                                Visible="False">
                                            </asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" CssClass="clsLeftPaddingTable" />
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Description" HeaderStyle-HorizontalAlign="Center">
                                        <HeaderStyle CssClass="clssubhead" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_abbrev" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.Description") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" CssClass="clsLeftPaddingTable" />
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImgDelete" runat="server" ImageUrl="~/Images/remove2.gif" CommandName="image"
                                                OnClientClick="return confirm('Are you sure you want to delete?');" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" CssClass="clsLeftPaddingTable" />
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </td>
                    </tr>
                    <tr>
                        <td width="100%" background="../Images/separator_repeat.gif" colspan="4" height="11">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 760px" align="left" colspan="4">
                            <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>            
        </aspnew:UpdatePanel>
        </form>
    </div>
</body>
</html>

<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.LoginAccesserror" Codebehind="LoginAccesserror.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>LoginAccesserror</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<table align="center" width="780" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td height="87">
						<asp:image id="Image1" runat="server" ImageUrl="Images/lnlogo.jpg"></asp:image></td>
				</tr>
			</table>
			<table align="center" border="0" width="780" cellspacing="0" cellpadding="0" bordercolor="blue">
				<tr>
					<td height="36" valign="middle" background="images/navi_repeat.jpg"><img SRC="images/accessdenied.jpg" HEIGHT="36"></td>
				</tr>
			</table>
			<table Class="clsLeftPaddingTable" width="780" border="0" align="center" cellpadding="0"
				cellspacing="0">
				<tr>
					<br>
					<td>
						<font color="red">You are not Authorized to use this feature. Please Click <A HREF="javascript:history.back()">
								Here</A> to go Back.</font>
					</td>
				</tr>
			</table>
			<table width="780" border="0" align="center" cellpadding="0" cellspacing="0" id="Table5">
				<tr background="images/separator_repeat.gif">
					<td colspan="5" width="780" height="11" background="images/separator_repeat.gif"></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>

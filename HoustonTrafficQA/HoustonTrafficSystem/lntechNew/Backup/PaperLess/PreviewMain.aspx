<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.PaperLess.PreviewMain" Codebehind="PreviewMain.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>PreviewMain</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script>
			 function HideBox()
		     {																		        
				 element = document.getElementById('txtflag').style;
		         element.display='none';
		         /*element = document.getElementById('txtFlagUpdated').style;
		         element.display='none';*/
				
			 }
			 
			 function ClearFiles()			 
			 {
				window.navigate("../ClearFiles.aspx?DocID="+<%=ViewState["DocID"]%>); 
				
			 }
			 function CloseWindow() 
			 {
			    window.opener = self;
                window.close();
			 }
		</script>
	</HEAD>
	<body onunload="ClearFiles();" MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<iframe src="PreviewPDF.aspx?DocID=<%=ViewState["DocID"]%>&DocNum=<%=ViewState["DocNum"]%>&RecType=<%=ViewState["RecType"]%>&DocExt=<%=ViewState["DocExt"]%>" width="100%" height="100%"></iframe>
		</form>
	</body>
</HTML>

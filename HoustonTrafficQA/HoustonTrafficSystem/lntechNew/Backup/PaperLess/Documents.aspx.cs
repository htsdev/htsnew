using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;
using lntechNew.WebControls;
using System.IO;
using WebSupergoo.ABCpdf6;
using Configuration.Helper;
using Configuration.Client;


namespace HTP.PaperLess
{
    public partial class Documents : System.Web.UI.Page
    {

        #region Variables


        clsENationWebComponents ClsDb = new clsENationWebComponents();
        clsSession ClsSession = new clsSession();
        clsLogger bugTracker = new clsLogger();
        clsCase ClsCase = new clsCase();
        clsReadNotes notes = new clsReadNotes();
        //Sabir Khan 5763 05/01/2009 declare variables...
        clsCourts ClsCourt = new clsCourts();
        clsCaseDetail CaseDetails = new clsCaseDetail();

        bool Bond = false;
        bool LetterOfRep = false;
        bool Continuance = false;
        bool TrialLetter = false;

        //Added by Khalid On 14/11/2007
        DataTable LetterType = new DataTable();

        #endregion

        //Nasir 6181 07/28/2009  remove IsLORBatchActive property

        // Abid Ali 5359 2/11/2008
        protected string CauseNumber
        {
            get
            {
                return (ViewState["CauseNumber"] == null ? string.Empty : Convert.ToString(ViewState["CauseNumber"]));
            }
            set
            {
                ViewState["CauseNumber"] = value;
            }
        }

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Waqas 5057 03/17/2009 Checking employee info in session
                if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false)
                {
                    //by khalid for bug 2391  18-12-07
                    //Response.Redirect("../frmlogin.aspx");
                    Response.Redirect("../frmlogin.aspx", false);
                    //reason for using overloaded method can be found at
                    //http://support.microsoft.com/kb/312629/EN-US/ 

                    //end bug 2391
                }
                else //To stop page further execution
                {

                    lblMessage.Text = "";
                    if (!IsPostBack)
                    {
                        LetterType.Columns.Add("name");
                        LetterType.Columns.Add("value");
                        if (Request.QueryString.Count >= 2)
                        {
                            ViewState["vTicketId"] = Request.QueryString["casenumber"];
                            ViewState["vSearch"] = Request.QueryString["search"];
                        }
                        else
                        {
                            Response.Redirect("../frmMain.aspx", false);
                            goto SearchPage;
                        }

                        ViewState["vNTPATHCaseSummary"] = ConfigurationManager.AppSettings["NTPATHCaseSummary"];
                        ViewState["vNTPATHScanTemp"] = ConfigurationManager.AppSettings["NTPATHScanTemp"];
                        ViewState["vNTPATHScanImage"] = ConfigurationManager.AppSettings["NTPATHScanImage"];


                        txtempid.Text = ClsSession.GetCookie("sEmpID", this.Request);
                        txtsessionid.Text = Session.SessionID;
                        int tID = 0;
                        //khalid 2730 date:1/31/08 null check added for view state
                        if (ViewState["vTicketId"] != null)
                        {
                            //Kazim 2730 2/7/2008 
                            //First parse the ticketid, if it is not parsed correctly then automatically redirected to main page

                            Int32.TryParse(Convert.ToString(ViewState["vTicketId"]), out tID);


                        }

                        //Nasir 6483 10/06/2009 set value for bond feild
                        if (ClsSession.GetCookie("sAccessType", this.Request).ToString() != "2")
                        {
                            hf_CheckBondReportForSecUser.Value = CaseDetails.SetForBondReport(Convert.ToInt32(ViewState["vTicketId"]));
                        }

                        if (tID == 0)
                        {
                            Response.Redirect("../frmlogin.aspx", false);
                        }
                        else
                        {
                            //Ozair 5546 02/17/2009 setting ticketid in hidden field to pass this 
                            //to scanning component concatenated with session id
                            hf_TicketID.Value = tID.ToString();
                            BindControls();
                            ShowHideLettersType();
                            DisplayInfo();
                            BindGrid();
                            IsAlreadyInBatchPrint();
                            CheckSpilitCases();
                            CheckReadNotes(tID);

                            #region ForScanningPurPose

                            string strServer;
                            strServer = "http://" + Request.ServerVariables["SERVER_NAME"];
                            Session["objTwain"] = "<OBJECT id='OZTwain1' classid='" + strServer + "/OZTwain_1.dll#OZTwain.OZTwain' height='1' width='1' VIEWASTEXT> </OBJECT>";//for scanning function call in javascript.
                            btnScan.Attributes.Add("onclick", "return StartScan();");

                            #endregion
                        }

                        if (!ClsCase.HasALRHearingViolation(Convert.ToInt32(ViewState["vTicketId"]), 0))
                        {
                            // Noufil 6358 08/13/2009 If ALR case is dispose then set value to 1
                            hf_IsALRDispoaseCase.Value = "1";
                        }
                        else
                            hf_IsALRDispoaseCase.Value = "0";


                    }
                }
                ActiveMenu am = (ActiveMenu)this.FindControl("ActiveMenu1");
                TextBox txt1 = (TextBox)am.FindControl("txtid");

                //Modified by kazim for task-2602
                //This bug can be come if viewstate value expired.For resolving this issue i applied a check
                //which assign viewstate["vticketid"]=0 when it does not found viewstate.

                if (ViewState["vTicketId"] == null)
                {
                    ViewState["vTicketId"] = "0";
                }

                // Abid Ali 5359 2/11/2009 Check LOR records in Batch print
                IsLORAlreadyInBatchPrint();
                CauseNumber = Request.QueryString["casenumber"].ToString();

                txt1.Text = Convert.ToString(ViewState["vTicketId"]);
                TextBox txt2 = (TextBox)am.FindControl("txtsrch");
                txt2.Text = Convert.ToString(ViewState["vSearch"]);
            SearchPage:
                { }

                //Nasir 6181 07/31/2009 change onclick to onchange
                ddlOperation.Attributes.Add("onchange", "HideNShow();");
                btnSubmit.Attributes.Add("onclick", "return check();");
                btnGenerateDocs.Attributes.Add("onclick", "return PrintLetter();");
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        //protected void btnUpdate_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        //SetValuesForGC();
        //        //DataSet ds_GC = ClsCase.UpdateGeneralComments();
        //   //  txtGeneralComments.Text = ds_GC.Tables[0].Rows[0]["generalcomments"].ToString();

        //    }
        //    catch (Exception ex)
        //    {
        //        lblMessage.Text = ex.Message;
        //        bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
        //    }
        //}

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (FPUpload.PostedFile != null) //Checking for valid file
                {
                    // Since the PostedFile.FileNameFileName gives the entire path we use Substring function to rip of the filename alone.
                    string StrFileName = FPUpload.PostedFile.FileName.Substring(FPUpload.PostedFile.FileName.LastIndexOf("\\") + 1);
                    string StrFileType = FPUpload.PostedFile.ContentType;
                    int IntFileSize = FPUpload.PostedFile.ContentLength;
                    //Checking for the length of the file. If length is 0 then file is not uploaded.
                    if (IntFileSize <= 0)
                        lblMessage.Text = " <font color='Red' size='2'>Uploading of file " + StrFileName + " failed </font>";
                    else
                    {
                        string sid = txtsessionid.Text;
                        string eid = txtempid.Text;
                        string StrFileName1 = sid + eid + StrFileName;
                        //File1.PostedFile.SaveAs(Server.MapPath(".\\tempimages\\" + StrFileName1));
                        FPUpload.PostedFile.SaveAs(ViewState["vNTPATHScanTemp"].ToString() + StrFileName1);
                        //Response.Write( "<font color='green' size='2'>Your file " + StrFileName + " of type " + StrFileType + " and size " + IntFileSize.ToString() + " bytes was uploaded successfully</font>");
                        lblMessage.Text = "Your file " + StrFileName + " of type " + StrFileType + " and size " + IntFileSize.ToString() + " bytes was uploaded successfully";

                        //Session["ScanDocType"] = cmbDocType.SelectedItem.Text;
                        ViewState["ScanDocType"] = ddlDocType.SelectedItem.Text;
                        //Session["BookID"]= 0;
                        ViewState["BookID"] = 0;
                        //Session["DocCount"]= 0;
                        ViewState["DocCount"] = 0;
                        ViewState["Events"] = "Upload";

                        //Perform database Uploading Task

                        //string searchpat="*"+ Session.SessionID + Session["sEmpID"].ToString() +"*.*";
                        string searchpat = "*" + Session.SessionID + eid + "*.*";
                        //string[] fileName=Directory.GetFiles(Server.MapPath("tempimages/"),searchpat);	
                        string[] fileName = Directory.GetFiles(ViewState["vNTPATHScanTemp"].ToString(), searchpat);

                        //string Ticket = Session["sTicketID"].ToString();
                        //int empid= (int) Convert.ChangeType(Session["sEmpID"].ToString() ,typeof(int)); //
                        //int TicketID = (int) Convert.ChangeType(Session["sTicketID"].ToString() ,typeof(int));
                        string Ticket = ViewState["vTicketId"].ToString();//ClsSession.GetCookie("sTicketID",this.Request);
                        int empid = Convert.ToInt32(eid);
                        int TicketID = Convert.ToInt32(Ticket);
                        //string bType=Session["ScanDocType"].ToString().Trim(); 
                        string bType = ViewState["ScanDocType"].ToString().Trim();
                        int subdoctypeid = 0;
                        if (ddl_SubDocType.Items.Count > 0)
                        {
                            subdoctypeid = Convert.ToInt32(ddl_SubDocType.SelectedValue);
                        }
                        int BookId = 0, picID = 0;
                        string picName, picDestination;
                        int DocCount = 1;
                        //ADDED BY TAHIR AHMED DT: 3/27/08 BUG ID 3543
                        // IF USER IS UPLOADING A PDF DOCUMENT THEN NEED TO GET CORRECT NUMBER
                        // OF PAGES IN THE DOCUMENT.....
                        if (StrFileType.ToLower().Equals("application/pdf"))
                            DocCount = GetDocPageCount(ViewState["vNTPATHScanTemp"].ToString() + StrFileName1);

                        for (int i = 0; i < fileName.Length; i++)
                        {
                            string exten = fileName[i].Substring(fileName[i].LastIndexOf(".") + 1);
                            string[] key = { "@updated", "@extension", "@Description", "@DocType", "@SubDocTypeID", "@Employee", "@TicketID", "@Count", "@Book", "@Events", "@BookID" };
                            object[] value1 = { DateTime.Now, exten.ToUpper(), "(Uploaded File) " + txtDescription.Text.Trim().ToUpper(), bType, subdoctypeid, empid, TicketID, DocCount, BookId, ViewState["Events"].ToString(), "" };
                            //call sP and get the book ID back from sP
                            picName = ClsDb.InsertBySPArrRet("usp_hts_NewAddScan", key, value1).ToString();
                            string BookI = picName.Split('-')[0];
                            string picI = picName.Split('-')[1];
                            BookId = (int)Convert.ChangeType(BookI, typeof(int)); ; //
                            picID = (int)Convert.ChangeType(picI, typeof(int)); ; //
                            //Move file
                            //picDestination= Server.MapPath("images/")+ picName + "." + exten; //DestinationImage
                            picDestination = ViewState["vNTPATHScanImage"].ToString() + picName + "." + exten; //DestinationImage

                            System.IO.File.Copy(fileName[i].ToString(), picDestination);

                            System.IO.File.Delete(fileName[i].ToString());
                        }

                        BindGrid();
                        RefreshControls();
                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }


        protected void btnScan_Click(object sender, EventArgs e)
        {
            try
            {

                ViewState["ScanDocType"] = ddlDocType.SelectedItem.Text;
                ViewState["BookID"] = 0;
                ViewState["DocCount"] = 0;
                ViewState["Events"] = "Scan";

                if (txtbID.Text.Length > 0)
                {
                    ViewState["sSDDocCount"] = ViewState["sSDDocCount"].ToString();
                }
                else
                {
                    ViewState["sSDDocCount"] = 1;
                }


                ViewState["sSDDesc"] = txtDescription.Text.Trim().ToUpper();

                //Perform database Uploading Task

                //Ozair 5546 02/17/2009 icluded ticketid whcih is concatenated with session id in scanning component
                string searchpat = "*" + Session.SessionID + hf_TicketID.Value + txtempid.Text + "*.jpg";
                //string[] fileName = Directory.GetFiles(Server.MapPath("tempimages/"), searchpat);
                string[] fileName = Directory.GetFiles(ViewState["vNTPATHScanTemp"].ToString(), searchpat);
                //
                if (fileName.Length > 1)
                {
                    fileName = sortfilesbydate(fileName);
                }
                //
                string Ticket = ViewState["vTicketId"].ToString();

                int empid = Convert.ToInt32(txtempid.Text); //
                int TicketID = (int)Convert.ChangeType(ViewState["vTicketId"].ToString().ToString(), typeof(int));

                string bType = ViewState["ScanDocType"].ToString().Trim();
                int subdoctypeid = 0;
                if (ddl_SubDocType.Items.Count > 0)
                {
                    subdoctypeid = Convert.ToInt32(ddl_SubDocType.SelectedValue);
                }

                int BookId = 0, picID = 0;
                if (txtbID.Text.Length > 0)
                {
                    BookId = Convert.ToInt32(txtbID.Text);
                }
                string picName, picDestination;

                int DocCount = Convert.ToInt32(ViewState["sSDDocCount"].ToString());

                string description = ViewState["sSDDesc"].ToString().ToUpper();

                for (int i = 0; i < fileName.Length; i++)
                {
                    string[] key = { "@updated", "@extension", "@Description", "@DocType", "@SubDocTypeID", "@Employee", "@TicketID", "@Count", "@Book", "@Events", "@BookID" };
                    object[] value1 = { DateTime.Now, "JPG", description, bType, subdoctypeid, empid, TicketID, DocCount, BookId, ViewState["Events"].ToString(), "" };
                    //call sP and get the book ID back from sP
                    picName = ClsDb.InsertBySPArrRet("usp_hts_NewAddScan", key, value1).ToString();
                    string BookI = picName.Split('-')[0];
                    string picI = picName.Split('-')[1];
                    BookId = (int)Convert.ChangeType(BookI, typeof(int)); ; //
                    picID = (int)Convert.ChangeType(picI, typeof(int)); ; //
                    if (ddlDocType.SelectedValue.ToString() != "0")//if (Adf.Checked == false)
                    {
                        txtbID.Text = BookId.ToString();
                    }
                    DocCount = DocCount + 1;
                    ViewState["sSDDocCount"] = Convert.ToString(DocCount);

                    //Move file
                    //picDestination = Server.MapPath("images/") + picName + ".jpg"; //DestinationImage
                    picDestination = ViewState["vNTPATHScanImage"].ToString() + picName + ".jpg"; //DestinationImage

                    System.IO.File.Copy(fileName[i].ToString(), picDestination);

                    System.IO.File.Delete(fileName[i].ToString());
                }
                //if (ddlDocType.SelectedValue.ToString() == "0")//if (Adf.Checked)
                if (ddlOperation.SelectedValue == "0")//if (Adf.Checked)
                {

                    txtbID.Text = "";
                    txtDescription.Text = "";
                    //Adf.Checked = false;
                }
                //BindGrid(ViewState["vTicketId"].ToString());

                BindGrid();
                RefreshControls();
                //
                HttpContext.Current.Response.Write("<script>window.open('PreviewMain.aspx?DocID=" + BookId + "&RecType=1&DocNum=0&DocExt=JPG');</script>");
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void dgrdDoc_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            string lngDocID;
            string lngRecordType;
            string lngDocNum;
            string lngDocExt;
            try
            {
                if (e.CommandName == "Delete")//for delete operation..
                {
                    try
                    {
                        lngDocID = ((Label)(e.Item.FindControl("lblDocID"))).Text;
                        lngDocNum = ((Label)(e.Item.FindControl("lblDocNum"))).Text;
                        lngRecordType = ((Label)(e.Item.FindControl("lblRecType"))).Text;
                        lngDocExt = ((Label)(e.Item.FindControl("lblDocExtension"))).Text;
                        int picid = Convert.ToInt32(((Label)(e.Item.FindControl("lbldid"))).Text);
                        string[] key = { "@docid", "@RecType", "@DocNum", "@DocExt" };
                        object[] value1 = { Convert.ToInt32(lngDocID), Convert.ToInt32(lngRecordType), Convert.ToInt32(lngDocNum), lngDocExt };
                        ClsDb.ExecuteSP("usp_HTS_Delete_ScanDoc", key, value1);

                        BindGrid();
                    }
                    catch (Exception)
                    {
                        lblMessage.Text = "Operation not completed successfully: Plz Perform this action again.";
                    }
                }
                else if (e.CommandName == "OCR")
                {
                    try
                    {
                        lngDocID = (((Label)(e.Item.FindControl("lblDocID"))).Text);
                        string did = (((Label)(e.Item.FindControl("lbldid"))).Text);
                        string picDestination;
                        string[] key3 = { "@sbookid" };
                        object[] value3 = { Convert.ToInt32(lngDocID) };
                        DataSet ds_bid = ClsDb.Get_DS_BySPArr("usp_hts_get_totalpicsbybookid", key3, value3);
                        for (int i = 0; i < ds_bid.Tables[0].Rows.Count; i++)
                        {
                            did = ds_bid.Tables[0].Rows[i]["docid"].ToString();
                            string picName = lngDocID + "-" + did;
                            //picDestination= Server.MapPath("images/")+ picName + ".jpg"  ; //DestinationImage


                            picDestination = ConfigurationManager.AppSettings["NTPATHScanImage1"] + picName + ".jpg"; //DestinationImage
                            clsGeneralMethods CGeneral = new clsGeneralMethods();
                            string ocr_data = CGeneral.OcrIt(picDestination);
                            string[] key2 = { "@doc_id", "@docnum_2", "@data_3" };
                            object[] value2 = { Convert.ToInt32(lngDocID), Convert.ToInt32(did), ocr_data };
                            //call sP and get the book ID back from sP
                            ClsDb.InsertBySPArr("usp_scan_insert_tblscandata", key2, value2);
                        }
                        BindGrid();
                    }
                    catch
                    {
                        lblMessage.Text = "Operation not completed successfully: Plz Perform this action again.";
                    }
                }
                else if (e.CommandName == "View")
                {
                    ViewState["sdoc_id"] = (((Label)(e.Item.FindControl("lblDocID"))).Text);
                    ViewState["sdocnum"] = (((Label)(e.Item.FindControl("lbldid"))).Text);
                    ViewState["sdoctypedesc"] = (((Label)(e.Item.FindControl("lblDocType"))).Text);
                    ViewState["sdocdesc"] = (((Label)(e.Item.FindControl("lblDocDescription"))).Text);
                    Response.Redirect("ViewOCR.aspx?search=" + ViewState["vSearch"].ToString() + "&casenumber=" + ViewState["vTicketId"].ToString() + "&sdoc_id=" + ViewState["sdoc_id"].ToString() + "&sdocnum=" + ViewState["sdocnum"].ToString() + "&sdoctypedesc=" + ViewState["sdoctypedesc"].ToString() + "&sdocdesc=" + ViewState["sdocdesc"].ToString(), false);
                }
                else if (e.CommandName.ToLower() == "msg")
                {


                    string[] key = { "@DocID" };
                    //object[] value1={Convert.ToInt32(Session["DocID"].ToString())};
                    object[] value1 = { Convert.ToInt32(e.CommandArgument.ToString()) };
                    DataTable dt = ClsDb.Get_DS_BySPArr("usp_dts_getScanByDocId", key, value1).Tables[0];

                    if (dt.Rows.Count != 0)
                    {
                        
                        // Abbas Qamar 9195 10/21/2011 which check the returned email path if exist then pick the Bad emai path from Configuration service.
                        string folderPath = string.Empty;

                        if ((((Label)(e.Item.FindControl("lblDocType"))).Text).Contains("Returned Email"))
                        {
                            IConfigurationClient _client = ConfigurationClientFactory.GetConfigurationClient();
                            folderPath = _client.GetValueOfKey(Division.HOUSTON, Module.BAD_EMAIL_SERVICE, SubModule.EMAIL, Key.File_Path);
                        }
                        else
                        {
                            folderPath = ConfigurationManager.AppSettings["NTPATHScanImage"];
                        }

                        string fileName = dt.Rows[0]["ScanBookId"] + "-" + dt.Rows[0]["DOCID"] + ".msg";

                        fileName = Path.Combine(folderPath, fileName);
                        //-- if something was passed to the file querystring 
                        if (fileName != "")
                        {
                            System.IO.FileInfo file = new System.IO.FileInfo(fileName);
                            //-- if the file exists on the server 
                            if (file.Exists)
                            {
                                //set appropriate headers 
                                Response.Clear();
                                Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                                Response.AddHeader("Content-Length", file.Length.ToString());
                                Response.ContentType = "application/octet-stream";
                                Response.WriteFile(file.FullName);
                                //if file does not exist 
                            }
                            else
                            {
                                lblMessage.Text = "This file does not exist.";
                            }
                            //nothing in the URL as HTTP GET 
                        }
                        else
                        {
                            lblMessage.Text = "Please provide a file to download.";
                        }

                    }

                }
                //Adil 5419 1/29/2009 adding support for EML files.
                else if (e.CommandName.ToLower() == "eml")
                {
                    string[] key = { "@DocID" };
                    object[] value1 = { Convert.ToInt32(e.CommandArgument.ToString()) };
                    DataTable dt = ClsDb.Get_DS_BySPArr("usp_dts_getScanByDocId", key, value1).Tables[0];

                    if (dt.Rows.Count != 0)
                    {
                        string folderPath = ConfigurationManager.AppSettings["NTPATHScanImage"];
                        string fileName = dt.Rows[0]["ScanBookId"] + "-" + dt.Rows[0]["DOCID"] + ".eml";
                        fileName = Path.Combine(folderPath, fileName);
                        //-- if something was passed to the file querystring 
                        if (fileName != "")
                        {
                            System.IO.FileInfo file = new System.IO.FileInfo(fileName);
                            //-- if the file exists on the server 
                            if (file.Exists)
                            {
                                //set appropriate headers 
                                Response.Clear();
                                Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                                Response.AddHeader("Content-Length", file.Length.ToString());
                                Response.ContentType = "application/octet-stream";
                                Response.WriteFile(file.FullName);
                                //if file does not exist 
                            }
                            else
                            {
                                lblMessage.Text = "This file does not exist.";
                            }
                            //nothing in the URL as HTTP GET 
                        }
                        else
                        {
                            lblMessage.Text = "Please provide a file to download.";
                        }

                    }

                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void btnGenerateDocs_Click(object sender, EventArgs e)
        {
            try
            {
                // Abid Ali 5359 2/11/2009 set waiting                
                LinkButton btn = sender as LinkButton;
                bool isUpdate = true;
                //Nasir 6181 07/22/2009 change button name
                if (btn != null && btn.Text.Trim().ToLower() == "fax lor")
                {
                    isUpdate = false;

                    clsCrsytalComponent Cr = new clsCrsytalComponent();
                    Cr.CreateApprovalReportLOR(Convert.ToInt32(ViewState["vTicketId"]), Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request)), 6, Server.MapPath("../Reports") + "\\Wordreport.rpt", this.Session, this.Response);

                    //Nasir 6013 07/16/2009 remove status update code 
                    //Nasir 6181 07/22/2009 fax window
                    Faxcontrol1.Ticketid = Convert.ToInt32(ViewState["vTicketId"]);
                    Faxcontrol1.Empid = Convert.ToInt32((ClsSession.GetCookie("sEmpID", this.Request)));
                    Faxcontrol1.Attachment = Session["reportpath"].ToString();
                    Faxcontrol1.GetCaseInformation();
                    ModalPopupExtender1.Show();

                }
                else if (btn != null && btn.Text.Trim().ToLower() == "batch lor")
                {
                    isUpdate = false;
                }

                // Abid Ali 5359 2/11/2009 set waiting status for letter of rep
                if (isUpdate)
                {
                    ClsCase.TicketID = Convert.ToInt32(ViewState["vTicketId"]);

                    // Save Case Summary Page
                    string name = ViewState["vTicketId"].ToString() + "-Matter Summary-" + DateTime.Now.ToFileTime() + ".pdf";
                    string path = ViewState["vNTPATHCaseSummary"].ToString();// +name;
                    ClsCase.CreateCaseSummary(path, name);

                    //Comment By Khalid
                    //string[] key_2 ={ "@ticketid", "@subject", "@notes", "@employeeid" };
                    //object[] value_2 ={ Convert.ToInt32(ViewState["vTicketId"]), "<a href=\"javascript:window.open('../DOCSTORAGE/CaseSummary/" + name + "');void('');\">Case Summary</a>", "Case Summary Report", ClsPayments.EmpID.ToString() };
                    //ClsDb.InsertBySPArr("sp_Add_Notes", key_2, value_2);

                    int TicketID = Convert.ToInt32(ViewState["vTicketId"]);

                    string[] keys = { "@TicketID_FK", "@BatchDate", "@PrintDate", "@IsPrinted", "@LetterID_FK", "@EmpID", "@DocPath" };
                    object[] values = { TicketID, System.DateTime.Now.Date, System.DateTime.Now.Date, 1, 9, Convert.ToInt32(txtempid.Text), name };
                    ClsDb.InsertBySPArr("USP_HTS_Insert_BatchPrintLetter", keys, values);

                    //Open Summary Page
                    string path2 = "../ClientInfo/CaseSummaryNew.aspx?casenumber=" + ViewState["vTicketId"].ToString() + "&search=" + ViewState["vSearch"].ToString();
                    string prop = "height=505,width=600,resizable=yes, status=no,toolbar=no,scrollbars=yes,menubar=no,location=no";
                    Response.Write("<script>window.open('" + path2 + "','" + "','" + prop + "');</script>");
                }

                // Abid Ali 5359 2/11/2009 set waiting status for letter of rep
                ViewState["vTicketId"] = Convert.ToInt32(ViewState["vTicketId"]);
                ViewState["vEmpID"] = Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request));

                BindGrid();
            }
            catch (Exception ex)
            {
                // lblMessage.Text = ex.Message + "----" +ViewState["vFullPath"].ToString();
                lblMessage.Text = "unable to get the document";
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void ddlDocType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ddl_SubDocType.Items.Clear();
                string[] key = { "@DocTypeID" };
                object[] value1 = { Convert.ToInt32(ddlDocType.SelectedValue) };
                DataSet ds = ClsDb.Get_DS_BySPArr("usp_HTS_Get_SubDocTypeBYDocTypeID", key, value1);
                if (ds.Tables[0].Rows.Count == 0)
                {
                    ddl_SubDocType.Items.Add(new ListItem("-----", "0"));
                }
                else
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        string Description = ds.Tables[0].Rows[i]["SubDocType"].ToString().Trim();
                        string ID = ds.Tables[0].Rows[i]["SubDocTypeID"].ToString().Trim();
                        ddl_SubDocType.Items.Add(new ListItem(Description, ID));
                    }

                    // Noufil 5819 05/14/2009 If case is dispose then hide all the violation except ALR hearing disposition              
                    if (!ClsCase.HasALRHearingViolation(Convert.ToInt32(ViewState["vTicketId"]), 0))
                    {
                        // Noufil 6358 08/13/2009 If ALR case is dispose then set value to 1
                        hf_IsALRDispoaseCase.Value = "1";
                        for (int i = 5; i <= 20; i++)
                        {
                            // Noufil 6358 08/13/2009 Add "ALR Hearing Wiever"
                            if (i != 12 && i != 13)
                            {
                                ListItem itemALR = ddl_SubDocType.Items.FindByValue(i.ToString());
                                if (itemALR != null)
                                    ddl_SubDocType.Items.Remove(itemALR);
                            }
                        }
                    }
                    // Noufil 6358 08/13/2009 If ALR case is dispose then set value to 1
                    else
                        hf_IsALRDispoaseCase.Value = "0";
                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void dl_generatedocs_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            LinkButton lb = (LinkButton)e.Item.FindControl("lb_docs");
            if (lb != null)
            {
                lb.Attributes.Add("onclick", "return PrintLetter(" + lb.CommandArgument + ")");
            }
        }





        protected void lb_docs_Command(object sender, CommandEventArgs e)
        {
            //Useless Code
            //string st = e.CommandArgument.ToString();
        }

        #endregion

        #region Methods

        private int GetDocPageCount(string sFileToUpload)
        {
            int pageCount = 1;
            Doc doc = new Doc();
            doc.Read(sFileToUpload.Replace("\\\\", "\\"));
            pageCount = doc.PageCount;
            doc.Dispose();
            return pageCount;
        }


        private string[] sortfilesbydate(string[] fileName)
        {
            for (int i = 0; i < fileName.Length; i++)
            {
                DateTime cDateTime = File.GetCreationTime(fileName[i]);
                for (int j = i + 1; j < fileName.Length; j++)
                {
                    DateTime cDateTime1 = File.GetCreationTime(fileName[j]);
                    if (DateTime.Compare(cDateTime1, cDateTime) < 0)
                    {
                        string fname = fileName[j];
                        fileName[j] = fileName[i];
                        fileName[i] = fname;
                        i = -1;
                        break;
                    }
                }
            }
            return fileName;
        }

        private void CheckReadNotes(int ticketid)
        {
            bool checkflag = false;
            checkflag = notes.CheckReadNoteComments(ticketid);
            if (checkflag == true)
            {

                read.Style[HtmlTextWriterStyle.Display] = "block";

            }
            else
            {

                read.Style[HtmlTextWriterStyle.Display] = "none";
            }


        }


        void BindControls()
        {
            try
            {

                ddlDocType.Items.Clear();
                DataSet ds = ClsDb.Get_DS_BySP("usp_Get_All_DocType");
                //Fill Document Type List
                ddlDocType.DataSource = ds;
                ddlDocType.DataTextField = "DocType";
                ddlDocType.DataValueField = "DocTypeID";
                ddlDocType.DataBind();

                //Insert Choose Item
                ddl_SubDocType.Items.Add(new ListItem("-----", "0"));

                // Noufil 5819 05/14/2009 This violation is shifted into Tblsubdoctype under doctype ALR and will hide automatically
                //ozair 4443 07/22/2008 Remove ALR Confirmation Document Type If ALR Hearing Not Exists
                //if (ClsCase.GetCaseType(Convert.ToInt32(ViewState["vTicketId"])) != 2)
                //{
                if (!ClsCase.HasALRHearingViolation(Convert.ToInt32(ViewState["vTicketId"]), 1))
                {

                    ListItem item = ddlDocType.Items.FindByValue("20");
                    if (item != null)
                        ddlDocType.Items.Remove(item);
                }
                //}
                //end ozair 4443
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        public void BindGrid()
        {
            try
            {
                hf_Subdoctype.Value = string.Empty;
                string[] key = { "@TicketID" };
                object[] value = { ViewState["vTicketId"].ToString() };
                DataSet DS = ClsDb.Get_DS_BySPArr("usp_hts_GetAllScanDocs", key, value);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    dgrdDoc.DataSource = DS;
                    dgrdDoc.DataBind();
                    BindLinks();

                    int subDocTypeID = 0;
                    bool isLatest = false;
                    foreach (DataRow dr in DS.Tables[0].Rows)
                    {
                        //if (Convert.ToString(dr["Subdoctype"]) == "15")
                        //    hf_Subdoctype.Value += dr["Subdoctype"] + "$" + dr["updatedatetime"] + ",";
                        //else if (Convert.ToString(dr["Subdoctype"]) == "17")
                        //    hf_Subdoctype.Value += dr["Subdoctype"] + "$" + dr["updatedatetime"] + ",";
                        //else if (Convert.ToString(dr["Subdoctype"]) == "7")
                        //    hf_Subdoctype.Value += dr["Subdoctype"] + "$" + dr["updatedatetime"] + ",";
                        //else
                        //    hf_Subdoctype.Value += dr["Subdoctype"] + ",";

                        subDocTypeID = Convert.ToInt32(dr["Subdoctype"]);
                        if (subDocTypeID == 15 || subDocTypeID == 17 || subDocTypeID == 7)
                        {
                            if (!isLatest)
                            {
                                hf_Subdoctype.Value += subDocTypeID.ToString() + ",";
                                isLatest = true;
                            }
                        }
                        else if (subDocTypeID != 15 || subDocTypeID != 17 || subDocTypeID != 7)
                        {
                            hf_Subdoctype.Value += subDocTypeID.ToString() + ",";
                        }

                    }
                }
                else
                {
                    dgrdDoc.DataSource = DS;
                    dgrdDoc.DataBind();
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        //commented by khalid when replacing  by control 7-1-08
        //private void SetValuesForGC()
        //{               
        //    ClsCase.TicketID = Convert.ToInt32(ViewState["vTicketId"]);
        //    ClsCase.EmpID = Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request));
        //    ClsCase.GeneralComments = txtGeneralComments.Text.ToString();
        //}

        private void DisplayInfo()
        {
            try
            {
                string[] key = { "@TicketID_PK" };
                object[] value = { Convert.ToInt32(ViewState["vTicketId"]) };
                DataSet DS = ClsDb.Get_DS_BySPArr("USP_HTS_Get_ClientInfo", key, value);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    lbl_FirstName.Text = DS.Tables[0].Rows[0]["Firstname"].ToString();
                    lbl_LastName.Text = DS.Tables[0].Rows[0]["Lastname"].ToString();
                    lbl_CaseCount.Text = DS.Tables[0].Rows[0]["CaseCount"].ToString();
                    hlnk_MidNo.Text = DS.Tables[0].Rows[0]["Midnum"].ToString();
                    //added by khalid on 7-1-08
                    // txtGeneralComments.Text = DS.Tables[0].Rows[0]["GeneralComments"].ToString();
                    //Nasir 6098 08/22/2009 replace with overloaded mehtod
                    WCC_GeneralComments.Initialize(Convert.ToInt32(ViewState["vTicketId"]), Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request)), 1, "Label", "clsinputadministration", "clsbutton", "Label");

                    WCC_GeneralComments.btn_AddComments.Visible = false;
                    WCC_GeneralComments.Button_Visible = false;

                    string casetype = ViewState["vSearch"].ToString();

                    switch (casetype)
                    {
                        case "0":
                            hlnk_MidNo.NavigateUrl = "../frmMain.aspx?search=0&lstcriteriaValue3=" + hlnk_MidNo.Text + "&lstcriteria3=3";
                            break;
                        case "1":
                            hlnk_MidNo.NavigateUrl = "../frmMain.aspx?search=1&lstcriteriaValue3=" + hlnk_MidNo.Text + "&lstcriteria3=3";
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void BindLinks()
        {
            long lngDocID;
            long RecType;
            long lngDocNum;
            string DocExt;
            string Event = "";

            // Abid Ali 5359 1/16/2009 letter of rep confirmed path
            string docstoragLORPath = "../docstorage/" + ConfigurationSettings.AppSettings["NTPATHLORPrint"].ToString().Replace(ConfigurationSettings.AppSettings["NTPATH"].ToString(), string.Empty);
            string urlLORDoc = string.Empty;

            try
            {
                foreach (DataGridItem dgItem in dgrdDoc.Items)
                {
                    lngDocID = System.Convert.ToInt64(((Label)(dgItem.FindControl("lblDocID"))).Text);
                    lngDocNum = System.Convert.ToInt64(((Label)(dgItem.FindControl("lblDocNum"))).Text);
                    RecType = System.Convert.ToInt64(((Label)(dgItem.FindControl("lblRecType"))).Text);
                    DocExt = System.Convert.ToString(((Label)(dgItem.FindControl("lblDocExtension"))).Text);
                    Event = System.Convert.ToString(((Label)(dgItem.FindControl("lblEvent"))).Text);


                    if (lngDocNum == -1)
                    {
                        string lnk = "javascript: return OpenPopUpNew('../ClientInfo/frmPreview.aspx?RecordID=" + lngDocID + "')";
                        ((ImageButton)(dgItem.FindControl("imgPreviewDoc"))).Attributes.Add("OnClick", lnk);
                    }
                    else if (DocExt.ToLower() == "msg")
                    {
                        // ((ImageButton)(dgItem.FindControl("imgPreviewDoc"))).Attributes.Add("OnClick", "javascript:return PopUpShowPreviewMSG(" + lngDocID.ToString() + "," + RecType + "," + lngDocNum.ToString() + ",'" + DocExt + "'" + ");");
                        ((ImageButton)(dgItem.FindControl("imgPreviewDoc"))).CommandName = "msg";
                        ((ImageButton)(dgItem.FindControl("imgPreviewDoc"))).CommandArgument = lngDocID.ToString();
                        if (((Label)dgItem.FindControl("lblDocType")).Text.Trim() == "Returned Emails")
                        {
                            ((LinkButton)dgItem.FindControl("lnkbtn_Delete")).Visible = false;
                        }
                    }
                    //Adil 5419 1/29/2009 adding support for EML files.
                    else if (DocExt.ToLower() == "eml")
                    {
                        ((ImageButton)(dgItem.FindControl("imgPreviewDoc"))).CommandName = "eml";
                        ((ImageButton)(dgItem.FindControl("imgPreviewDoc"))).CommandArgument = lngDocID.ToString();
                    }
                    else
                    {
                        //Ozair 8039 07/20/2010 code refactored
                        switch (RecType)
                        {
                            //checking if rec is from document tracking 
                            case 2:
                                {
                                    int dbID = Convert.ToInt32(lngDocID);
                                    int sbID = Convert.ToInt32(System.Convert.ToInt32(((Label)(dgItem.FindControl("lbldid"))).Text));
                                    ((ImageButton)(dgItem.FindControl("imgPreviewDoc"))).Attributes.Add("OnClick", "javascript:return PopUpShowPreviewDT(" + dbID.ToString() + "," + sbID.ToString() + ");");
                                    ((LinkButton)dgItem.FindControl("lnkbtn_Delete")).Visible = false;
                                    ((LinkButton)dgItem.FindControl("lnkbtn_OCR")).Visible = false;
                                    ((LinkButton)dgItem.FindControl("lnkbtn_VOCR")).Visible = false;
                                }
                                break;
                            //checking if rec is from of letter of rep 
                            case 3:
                                urlLORDoc = docstoragLORPath + DocExt;
                                ((LinkButton)dgItem.FindControl("lnkbtn_Delete")).Visible = false;
                                ((LinkButton)dgItem.FindControl("lnkbtn_OCR")).Visible = false;
                                ((LinkButton)dgItem.FindControl("lnkbtn_VOCR")).Visible = false;
                                ((ImageButton)(dgItem.FindControl("imgPreviewDoc"))).Attributes.Add("OnClick", "return OpenReport('" + urlLORDoc + "');");
                                break;
                            //checking if rec is from Arr/Bond Summary Reports
                            case 4:
                                ((ImageButton)(dgItem.FindControl("imgPreviewDoc"))).Attributes.Add("OnClick", "javascript:return PopUpShowPreviewPDF('*" + lngDocID.ToString() + "'," + RecType + "," + lngDocNum.ToString() + ",'" + DocExt + "'" + ");");
                                break;
                            default:
                                ((ImageButton)(dgItem.FindControl("imgPreviewDoc"))).Attributes.Add("OnClick", "javascript:return PopUpShowPreviewPDF(" + lngDocID.ToString() + "," + RecType + "," + lngDocNum.ToString() + ",'" + DocExt + "'" + ");");
                                break;
                        }
                        //end ozair 3643
                    }




                    //if (ClsSession.GetSessionVariable("sAccessType",this.Session).ToString()!="2")
                    {
                        if (((Label)dgItem.FindControl("lblDocType")).Text.Trim() == "Resets")
                        {
                            ((LinkButton)dgItem.FindControl("lnkbtn_Delete")).Visible = false;
                            ((LinkButton)dgItem.FindControl("lnkbtn_OCR")).Visible = false;
                            ((LinkButton)dgItem.FindControl("lnkbtn_VOCR")).Visible = false;
                        }
                        if (Event == "Print" || Event == "")
                        {
                            ((LinkButton)dgItem.FindControl("lnkbtn_Delete")).Visible = false;
                            ((LinkButton)dgItem.FindControl("lnkbtn_OCR")).Visible = false;
                            ((LinkButton)dgItem.FindControl("lnkbtn_VOCR")).Visible = false;
                        }

                    }

                    //Allow Only Primary User To Delete Document
                    if (ClsSession.GetCookie("sAccessType", this.Request).ToString() == "2")
                    {

                        DateTime recDate = Convert.ToDateTime(((Label)dgItem.FindControl("lblDateTime")).Text);
                        DateTime CurDate = DateTime.Now;
                        int a = CurDate.Day - recDate.Day;
                        if (a > 1)
                        {
                            ((LinkButton)dgItem.FindControl("lnkbtn_Delete")).Visible = false;
                        }
                        else
                        {
                            // Noufil 5819 05/07/2009 Sending Delete link button ID
                            ((LinkButton)dgItem.FindControl("lnkbtn_Delete")).Attributes.Add("OnClick", "return DeleteCheck(this.id);");
                        }
                    }
                    else
                        ((LinkButton)dgItem.FindControl("lnkbtn_Delete")).Visible = false;

                    if (((Label)dgItem.FindControl("lblocrid")).Text != "0")
                    {
                        ((LinkButton)dgItem.FindControl("lnkbtn_OCR")).Visible = false;
                        ((LinkButton)dgItem.FindControl("lnkbtn_VOCR")).Visible = true;
                    }
                    if (((Label)dgItem.FindControl("lblDocExtension")).Text != "000" && ((Label)dgItem.FindControl("lblDocExtension")).Text != "JPG")
                    {
                        ((LinkButton)dgItem.FindControl("lnkbtn_OCR")).Visible = false;
                        ((LinkButton)dgItem.FindControl("lnkbtn_VOCR")).Visible = false;
                    }
                    //Nasir 6483 10/07/2009 if sec user bond client and other logic click not display bond view image
                    if ((((Label)dgItem.FindControl("lblDocType")).Text.Trim() == "BOND") && ClsSession.GetCookie("sAccessType", this.Request).ToString() != "2")
                    {
                        ((ImageButton)dgItem.FindControl("imgPreviewDoc")).Visible = false;
                    }
                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        private void ShowHideLettersType()
        {
            try
            {
                // ddlLetterType.Visible = true;
                // btnGenerateDocs.Visible = true;
                string[] key = { "@ticketid" };
                object[] value = { Convert.ToInt32(ViewState["vTicketId"]) };
                DataSet DS = ClsDb.Get_DS_BySPArr("USP_HTS_GetCaseInfo", key, value);
                if (DS.Tables.Count > 0 && DS.Tables[0].Rows.Count > 0) //table count check added by khalid 22-1-08 bug 2619
                {

                    if (Convert.ToInt32(DS.Tables[0].Rows[0]["ActiveFlag"]) == 1)
                    {

                        //Comment By Zeeshan Ahmed No Need of this because nothing Is Modified in the Condition                           
                        //  if (ddlOperation.Items.Count == 3)
                        //   {
                        //Comment By Khalid
                        // ddlOperation.Items.Add(new ListItem("Generate", "3"));
                        //   }

                        ddlLetterType.Items.Clear();

                        //Comment by Khalid
                        //ddlLetterType.Items.Add(new ListItem("--Choose--", "-1"));
                        //ddlLetterType.Items.Add(new ListItem("Matter Summary", "1"));
                        //ddlLetterType.Items.Add(new ListItem("Payment Receipt", "3"));

                        //Adding Rows To Letter Type Table
                        Addrows("Matter Summary", "1");
                        Addrows("Payment Receipt", "3");


                        string[] key1 = { "@TicketID" };
                        object[] value1 = { Convert.ToInt32(ViewState["vTicketId"]) };
                        DataSet DS_Underlying = ClsDb.Get_DS_BySPArr("USP_HTS_Get_PaymentInfo_ButtonStatus", key1, value1);

                        if (DS_Underlying.Tables[0].Rows.Count > 0)
                        {
                            # region previous visibility logic
                            for (int i = 0; i < DS_Underlying.Tables[0].Rows.Count; i++)
                            {//payment info if (Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["Bondflag"]) == 1 && (Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["categoryid"]) == 1 || Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["categoryid"]) == 2))

                                //Ozair 6460 08/25/2009 Setting bond hidden field
                                if (Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["Bondflag"]) == 1)
                                {
                                    hf_IsBond.Value = "1";
                                }

                                if (Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["Bondflag"]) == 1 && (Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["categoryid"]) == 1 || Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["categoryid"]) == 2))
                                {
                                    //ddlLetterType.Items.Add(new ListItem("Bond", "4"));   
                                    Bond = true;
                                    lbl_courtid.Text = DS_Underlying.Tables[0].Rows[i]["CourtId"].ToString();
                                }
                                else
                                {//p-info  if (Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["categoryid"]) == 12)
                                    if (Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["categoryid"]) == 12)
                                    {
                                        // ddlLetterType.Items.Add(new ListItem("Bond", "4"));   
                                        Bond = true;
                                        lbl_courtid.Text = DS_Underlying.Tables[0].Rows[i]["CourtId"].ToString();
                                    }

                                }

                                DateTime dcourt = Convert.ToDateTime(DS_Underlying.Tables[0].Rows[i]["CourtDateMain"]);
                                if ((dcourt >= DateTime.Now.Date) && (Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["categoryid"]) == 2 || Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["categoryid"]) == 3 || Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["categoryid"]) == 4 || Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["categoryid"]) == 5) && (Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["CourtID"]) != 3001 && Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["CourtID"]) != 3002 && Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["CourtID"]) != 3003))
                                {//p-info     if ((dcourt >= DateTime.Now.Date) && (Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["categoryid"]) == 2 || Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["categoryid"]) == 3 || Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["categoryid"]) == 4 || Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["categoryid"]) == 5) && (Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["CourtID"]) != 3001 && Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["CourtID"]) != 3002 && Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["CourtID"]) != 3003))


                                    //ddlLetterType.Items.Add(new ListItem("Trial Letter", "2")); 
                                    TrialLetter = true;
                                    if (DS.Tables[0].Rows[0]["email"].ToString() != "")
                                    {
                                        // Following line was commented by Farhan Sabir to address bug#1721
                                        //btn_EmailTrial.Visible = true;
                                        ViewState["emailid"] = DS.Tables[0].Rows[0]["email"].ToString();

                                    }
                                    else
                                    {
                                        ViewState["emailid"] = "";

                                    }
                                }
                                else
                                    //Inside court status in Arr
                                    if ((dcourt >= DateTime.Now.Date) && Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["categoryid"]) == 4 && (Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["CourtId"]) == 3001 || Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["CourtId"]) == 3002 || Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["CourtId"]) == 3003))
                                    {

                                        //ddlLetterType.Items.Add(new ListItem("Trial Letter", "2"));
                                        TrialLetter = true;
                                        if (DS.Tables[0].Rows[0]["email"].ToString() != "")
                                        {
                                            // Following line was commented by Farhan Sabir to address bug#1721
                                            //btn_EmailTrial.Visible = true;
                                            ViewState["emailid"] = DS.Tables[0].Rows[0]["email"].ToString();

                                        }
                                        else
                                        {
                                            ViewState["emailid"] = "";

                                        }
                                    }
                                // Checking for LETTER OF REP //status in Waiting,Arr,Waiting2nd,Bond and court not to be Lubbok/Mykawa

                                //original if ((Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["categoryid"]) == 1 || Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["categoryid"]) == 2 || Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["categoryid"]) == 11 || Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["categoryid"]) == 12) && (Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["CourtID"]) != 3001 && Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["CourtID"]) != 3002 && Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["CourtID"]) != 3003))
                                if (Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["CourtID"]) != 3001 && Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["CourtID"]) != 3002 && Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["CourtID"]) != 3003)
                                {//p-info  if (Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["CourtID"]) != 3001 && Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["CourtID"]) != 3002 && Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["CourtID"]) != 3003)

                                    //Sabir Khan 5763 05/01/2009 Get LOR Subpoena Flag and LOR Motion of discovery flag...
                                    DataSet dscourt = ClsCourt.GetCourtInfo(Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["CourtID"]));
                                    hf_IsLORSubpoena.Value = (Convert.ToBoolean(dscourt.Tables[0].Rows[0]["IsLORSubpoena"]) == true ? "1" : "0");
                                    hf_IsLORMOD.Value = (Convert.ToBoolean(dscourt.Tables[0].Rows[0]["IsLORMOD"]) == true ? "1" : "0");
                                    //Sabir Khan 5763 04/16/2009 check if a client different court date...
                                    hf_HasSameCourtdate.Value = (CaseDetails.HasSameCourtDate(Convert.ToInt32(ViewState["vTicketId"])) == true ? "1" : "0");
                                    //Sabir Khan 5763 04/16/2009 check if a client has more then one speeding violations...
                                    hf_HasMoreSpeedingviol.Value = (CaseDetails.HasMoreSpeedingViolation(Convert.ToInt32(ViewState["vTicketId"])) <= 1 ? "1" : "0");
                                    //Nasir 6181 07/22/2009 disposed case and FAX method count
                                    int MyCount = clsCourts.GetLORMethodByCase(Convert.ToInt32(ViewState["vTicketId"]));
                                    hfLORMethodCount.Value = MyCount.ToString();
                                    hf_IsnotDisposed.Value = Convert.ToString(Convert.ToInt32(ClsCase.HasDisposedViolations(Convert.ToInt32(ViewState["vTicketId"]))));
                                    //ddlLetterType.Items.Add(new ListItem("Letter Of Rep (LOR)", "6"));
                                    LetterOfRep = true;
                                    //Nasir 6181 07/28/2009  remove code of setting IsLORBatchActive
                                }

                                if (Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["ContinuanceAmount"]) > 0)
                                {
                                    //   ddlLetterType.Items.Add(new ListItem("Continuance", "5"));
                                    Continuance = true;
                                }


                            }
                        }
                            #endregion




                    }
                }
                else
                {
                    ddlLetterType.Visible = false;
                    btnGenerateDocs.Visible = false;
                    if (ddlOperation.Items.Count > 3)
                    {
                        ddlOperation.Items.Remove("Generate");
                    }
                }
                if (Bond == true)
                {
                    //Comment by Khalid
                    // ddlLetterType.Items.Add(new ListItem("Bond", "4"));
                    Addrows("Bond", "4");
                }
                if (TrialLetter == true)
                {
                    //Comment by Khalid
                    //ddlLetterType.Items.Add(new ListItem("Trial Letter", "2"));
                    Addrows("Trial Letter", "2");
                }
                if (LetterOfRep == true)
                {
                    //Comment by Khalid
                    //ddlLetterType.Items.Add(new ListItem("Letter Of Rep (LOR)", "6"));
                    //Nasir 6181 07/22/2009 replace button Fax LOR,Batch LOR
                    //Ozair 6460 08/25/2009 included check for bond flag
                    if (hfLORMethodCount.Value != "0" && hf_IsBond.Value == "0")
                    {
                        Addrows("Fax LOR", "6");
                    }
                    else
                    {
                        Addrows("Batch LOR", "6");
                    }
                }
                if (Continuance == true)
                {
                    //Comment by Khalid
                    //ddlLetterType.Items.Add(new ListItem("Continuance", "5"));
                    Addrows("Continuance", "5");
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void GeneratePDF(string path, string name)
        {
            Doc theDoc = new Doc();
            theDoc.Rect.Inset(35, 45);
            theDoc.Page = theDoc.AddPage();
            //theDoc.HtmlOptions.BrowserWidth = 800;
            int theID;
            theID = theDoc.AddImageUrl(path);
            while (true)
            {
                //theDoc.FrameRect(); // add a black border
                if (!theDoc.Chainable(theID))
                    break;
                theDoc.Page = theDoc.AddPage();
                theID = theDoc.AddImageToChain(theID);
            }
            for (int i = 1; i <= theDoc.PageCount; i++)
            {
                theDoc.PageNumber = i;
                theDoc.Flatten();
            }


            ViewState["vFullPath"] = ViewState["vNTPATHCaseSummary"].ToString() + name;
            ViewState["vPath"] = ViewState["vNTPATHCaseSummary"].ToString();
            theDoc.Save(ViewState["vFullPath"].ToString());
            theDoc.Clear();

        }

        private void CheckSpilitCases()
        {
            try
            {
                string[] keys = { "@ticketid" };
                object[] values = { Request.QueryString["casenumber"] };
                DataTable dtSplit = new DataTable();
                dtSplit = ClsDb.Get_DT_BySPArr("USP_HTS_PAYMENTINFO_GET_SPLITCaseCount", keys, values);

                if (Convert.ToInt32(dtSplit.Rows[0][0].ToString()) > 0)
                    lbl_IsSplit.Text = "1";
                else
                    lbl_IsSplit.Text = "0";
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void IsAlreadyInBatchPrint()
        {
            try
            {
                lbl_court.Text = Request.QueryString["casenumber"].ToString();
                string[] keys = { "@TicketID" };
                object[] values = { Request.QueryString["casenumber"] };
                DataTable dtBatchPrint = new DataTable();
                dtBatchPrint = ClsDb.Get_DT_BySPArr("USP_HTS_GET_BATCHLETTER_CHECK_BATCHPRINT", keys, values);
                if (Convert.ToInt32(dtBatchPrint.Rows[0][0].ToString()) > 0)
                    lbl_IsAlreadyInBatchPrint.Text = "1";
                else
                    lbl_IsAlreadyInBatchPrint.Text = "0";
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void RefreshControls()
        {
            try
            {
                ddlDocType.SelectedValue = "1";
                ddl_SubDocType.Items.Clear();
                ddl_SubDocType.Items.Add(new ListItem("-----", "0"));
                ddlLetterType.SelectedValue = "-1";
                txtDescription.Text = "";
                ddlOperation.SelectedValue = "0";

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        //Missing Khalid Comments
        public void Addrows(string name, string val)
        {
            DataRow dr;
            dr = LetterType.NewRow();
            dr["name"] = name;
            dr["value"] = val;
            LetterType.Rows.Add(dr);
            dl_generatedocs.DataSource = LetterType;
            dl_generatedocs.DataBind();


        }

        //Nasir 6181 07/22/2009 create report for lor
        /// <summary>
        /// create report for lor
        /// </summary>
        /// <param name="ticketno"></param>
        /// <param name="empid"></param>
        /// <param name="lettertype"></param>
        //public void CreateApprovalReport(int ticketno, int empid, int lettertype)
        //{
        //    clsCrsytalComponent Cr = new clsCrsytalComponent();
        //    string[] key = { "@ticketid", "@empid" };
        //    object[] value1 = { ticketno, empid };
        //    string filename;
        //    string filepath;

        //    if (lettertype == 6)
        //    {
        //        // Noufil 3999 05/09/2008 Creating report and getting its path from where it is created
        //        filename = Server.MapPath("../Reports") + "\\Wordreport.rpt";


        //        // Abid Ali 5359 12/30/2008 Letter or send to batch or not
        //        string sendToBatch = "false";
        //        if (ViewState["vBatch"] != null)
        //        {
        //            bool isBatch;
        //            if (Boolean.TryParse(ViewState["vBatch"].ToString(), out isBatch))
        //                sendToBatch = isBatch.ToString().ToLower();
        //        }

        //        filepath = Cr.CreateReport(filename, "USP_HTS_LETTER_OF_REP", key, value1, lettertype, empid, this.Session, this.Response);
        //        Session["reportpath"] = filepath;
        //    }

        //}

        #endregion
        //Added By Fahad (15-1-08)
        protected void btnupdate_Click(object sender, EventArgs e)
        {
            WCC_GeneralComments.AddComments();
        }
        //End Fahad

        // Abid Ali 5359 2/11/2009 Check LOR Already in batch print
        /// <summary>
        /// Check LOR already in batch print       
        /// </summary>
        private void IsLORAlreadyInBatchPrint()
        {
            try
            {
                string[] keys = { "@TicketID", "@LetterType" };
                object[] values = { Request.QueryString["casenumber"], 6 };
                DataTable dtBatchPrint = new DataTable();
                dtBatchPrint = ClsDb.Get_DT_BySPArr("USP_HTS_GET_BATCHLETTER_CHECK_BATCHPRINT", keys, values);
                if (Convert.ToInt32(dtBatchPrint.Rows[0][0].ToString()) > 0)
                    lbl_IsLORAlreadyInBatchPrint.Text = "1";
                else
                    lbl_IsLORAlreadyInBatchPrint.Text = "0";
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }



    }
}

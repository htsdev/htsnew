﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using lntechNew.Components.ClientInfo;
using HTP.Components.ClientInfo;
using System.IO;
namespace HTP.PaperLess
{
    public partial class FrmPreviewDocuments : System.Web.UI.Page
    {
        clsLogger cLog = new clsLogger();
        CourtsFiles files = new CourtsFiles();
        string lnk;
        protected void Page_Load(object sender, EventArgs e)
        {
            //Kazim 3697 5/6/2008 This page is used to view the uploaded court files.
            try
            {
                lnk = Request.QueryString["fielid"].ToString();
                lnk = files.ViewFilepath(Convert.ToInt16(lnk));
                string ext = Path.GetExtension(lnk);
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();
                if (ext == ".pdf")
                {
                    Response.ContentType = "application/pdf";
                    Response.WriteFile(lnk);
                    Response.Flush();
                    Response.Close();
                }
                else if (ext == ".doc" || ext == ".docx")
                {

                    //Kazim 4071 5/19/2008 use filestream function to check uploaded file length
  
                    FileStream fs = new FileStream(lnk, FileMode.Open);
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(lnk));
                    Response.AddHeader("Content-Length", fs.Length.ToString());
                    Response.ContentType = "application/octet-stream";
                    fs.Close();
                    Response.WriteFile(lnk);
                    Response.Write("javascript:CloseWindow();");
                    Response.End();

                }
            }
            catch (Exception ex)
            {
                if (!File.Exists(lnk))
                    Response.Write("The specified file has been moved or deleted from the server.");
            }

        }
    }
}

using System;
using System.Collections;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using iTextSharp.text; 
using iTextSharp.text.pdf; 
using ICSharpCode; 
using System.IO;
using FrameWorkEnation.Components;
using BaxterSoft.Graphics;
using System.Configuration;
//Nasir 5437 01/26/2009 use this for DeleteRootTempFile method in class clsGeneralMethods
using lntechNew.Components;
//Nasir 5437 01/26/2009 to log exception
using lntechNew.Components.ClientInfo;


namespace HTP.PaperLess
{
	/// <summary>
	/// Summary description for PreviewPDF.
	/// </summary>
	/// 
	

	public partial class PreviewPDF : System.Web.UI.Page
	{
		//clsWeb objDS = new clsWeb(clsWeb.enmDBServer.TrafficDoc); 
		clsENationWebComponents ClsDb = new clsENationWebComponents();
        
        
		//clsDB.structResult structResult; 
		string strFileName;

		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!(Page.IsPostBack)) 
			{ 
				//long docID = System.Convert.ToInt64(Session["DocID"].ToString());
				//long docID = System.Convert.ToInt64(Context.Items["DocID"].ToString());
				long docID = System.Convert.ToInt64(Request.QueryString["DocID"].ToString());
				//long docNUM = System.Convert.ToInt64(Session["DocNum"].ToString());
				//long docNUM = System.Convert.ToInt64(Context.Items["DocNum"].ToString());
				long docNUM = System.Convert.ToInt64(Request.QueryString["DocNum"].ToString());
				//long recType = System.Convert.ToInt64(Session["RecType"].ToString());
				//long recType = System.Convert.ToInt64(Context.Items["RecType"].ToString());
				long recType = System.Convert.ToInt64(Request.QueryString["RecType"].ToString());
                //network path
                ViewState["vNTPATHScanTemp"] = ConfigurationSettings.AppSettings["NTPATHScanTemp"].ToString();
                ViewState["vNTPATHScanImage"] = ConfigurationSettings.AppSettings["NTPATHScanImage"].ToString();
                ViewState["vNTPATH"] = ConfigurationSettings.AppSettings["NTPATH"].ToString();
                //
				
				//String ss = Session["DocID"].ToString(); 			 
			//if(recType == 0 && Session["DocExt"].ToString()=="000") 
			if(recType == 0 && Request.QueryString["DocExt"].ToString()=="000") 
				//ShowPDF(docID); 
				ShowPDFUsingPDFToolKit(docID,docNUM);
			else if(recType == 1)
				ShowPDFN(docID); 
	
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion

		private void ShowPDF(long DocID) 

		{   
			  
			Document objdocument = new Document(); 
			DataSet ds;
			//string strImgNo; 
//			DataRow ImageRow; 
			DataTable dtlTemp1;
			string strFile; 
			strFileName = Server.MapPath("../Temp/"); 
			strFile = Session.SessionID + DocID.ToString() + ".pdf"; 
			strFileName += strFile; 
			PdfWriter.getInstance(objdocument, new FileStream(strFileName, FileMode.Create)); 
			objdocument.Open(); 
		
		//What is Parameter Name ?
			string[] key    = {"@DocID"};
			object[] value1 = {DocID};

			
			ds = ClsDb.Get_DS_BySPArr("usp_Get_SacanDocImageByDocID2",key,value1);

			dtlTemp1 = ds.Tables[0];

			if (dtlTemp1.Rows.Count > 0) 
			{ 
				foreach (DataRow ImageRow in ds.Tables[0].Rows) 
				{ 
					iTextSharp.text.Image Pic = iTextSharp.text.Image.getInstance((byte[]) ImageRow["DOCPIC"]); 
					Pic.scalePercent(20, 19); 
					objdocument.Add(Pic); 
				} 
			} 
			objdocument.Close(); 
			//Session["DocID"] = strFileName; 
			ViewState["DocID"] = strFileName; 
            
			Response.Redirect("../Temp/" + strFile); 
		}

		


		private void ShowPDFUsingPDFToolKit(long DocID, long DocNum) 

		{   
			
			try
			{

				Document objdocument = new Document(); 
				DataSet ds;

			
		 
				System.Data.DataTable dtlTemp1;
			
				string[] key    = {"@DocID", "@DocNum"};
				object[] value1 = {DocID, DocNum};

				byte[] imageData ;
		
			
				ds = ClsDb.Get_DS_BySPArr("usp_Get_SacanDocImageByDocIDandDocNum",key,value1);

				dtlTemp1 = ds.Tables[0];
			
			
				if (dtlTemp1.Rows.Count > 0) 
				{ 
					imageData =(byte[]) dtlTemp1.Rows[0]["DOCPIC"] ;
                    
                    ////testing by ozair
                    //MemoryStream m = new MemoryStream(imageData);
                    //System.Drawing.Bitmap bp = new Bitmap(m);
                    //bp.Save("c:\\aa", System.Drawing.Imaging.ImageFormat.Jpeg);
                    ////end testing

					strFileName = Server.MapPath("../Temp/"); 
					string strfn=strFileName + Session.SessionID + DocID.ToString() + DocNum.ToString() + Convert.ToString(DateTime.Now.ToFileTime());
					FileStream fs=new FileStream(strfn,FileMode.CreateNew, FileAccess.Write );
					fs.Write(imageData, 0, imageData.Length);
					fs.Close ();
					FileStream fs1 = new FileStream(strfn,FileMode.Open);
                    TIFFReader tr = new TIFFReader( fs1); 
					TIFFDocument tdoc = tr.Read(false); 
					tr.Close(); 
					//fs1.Flush();
					fs1.Close();
					File.Delete(strfn);
                    
					
					//fs.Flush();
					PDFWriter pdf = new PDFWriter(tdoc); 
					Response.Clear();
					Response.ContentType = "application/pdf";
					Response.AddHeader("Content-Type", "application/pdf");
					byte[] output = pdf.SavePDF().ToArray(); 
					Response.BinaryWrite(output);
					Response.Flush();
					Response.Close();

					//Response.End();
					
				} 
				else
				{
					Response.Write ("No Images Found");
					//Response.End();
				}
			}
			catch(Exception ex)
			{
				Response.Write (ex.Message) ;
				//Response.End();
			}
		}

	private void ShowPDFN(long DocID) 
		{ 
		try
		{
			Document objdocument = new Document();
            //Nasir 5437 01/26/2009 use this class for DeleteRootTempFile
            clsGeneralMethods GM = new clsGeneralMethods();
			DataSet  ds;
			//string strImgNo; 
			//			DataRow ImageRow; 
			DataTable dtlTemp1;
			string strFile; 
			strFileName = Server.MapPath("../Temp/"); 
			strFile = Session.SessionID + DocID.ToString() + ".pdf"; 
			strFileName += strFile;
            Session["DocID"] = strFileName; 
			PdfWriter.getInstance(objdocument, new FileStream(strFileName, FileMode.Create)); 
			objdocument.Open(); 
		
			
			string[] key    = {"@DocID"};
			object[] value1 = {DocID};

			
			ds = ClsDb.Get_DS_BySPArr("usp_Get_SacanDocImageByDocID_NEW",key,value1);

			dtlTemp1 = ds.Tables[0];

			if (dtlTemp1.Rows.Count > 0) 
			{
				
				//iTextSharp.text.Image Pic;
				//foreach (DataRow ImageRow in dtlTemp1.Rows) 
				objdocument.setMargins(1,0,0,0);
				for(int a=0;a<dtlTemp1.Rows.Count;a++ )
				{
					string fn;
					

					//fn=dtlTemp1.Rows[a]["BOOKID"].ToString() + "-" + dtlTemp1.Rows[a]["DOCPIC"].ToString() + "." + Session["DocExt"].ToString(); 
					fn=dtlTemp1.Rows[a]["BOOKID"].ToString() + "-" + dtlTemp1.Rows[a]["DOCPIC"].ToString() + "." + Request.QueryString["DocExt"].ToString(); 
					//if(Session["DocExt"].ToString()!="JPG")
					if(Request.QueryString["DocExt"].ToString()!="JPG")
					{
						//fn="HTTP://" + Request.ServerVariables["SERVER_NAME"] + "/Paperless/images/" + fn;
                        string vpth = ViewState["vNTPATHScanImage"].ToString().Substring((ViewState["vNTPATH"].ToString().Length)-1);
                       

                        fn = "../DOCSTORAGE" + vpth + fn;
                        //Sabir Khan 5571 02/23/2009 Comment code...
						//objdocument.Close();
                        //Nasir 5437 02/02/2009 to handle Exeception thread aborted
                        Response.Clear();
                        Response.Redirect(fn, false);
                        Response.Flush(); //Adil 6202 08/25/2009 to handle thread aborted exeception.
                        //Nasir 5437 01/26/2009 use this for Delete Root Temp Files
                        //Sabir Khan 5571 02/23/2009 Comment code...
                        //GM.DeleteTempFiles(Server.MapPath("../Temp/"), "*.pdf");
						                       
                       
					}
					else
					{
						//fn = Server.MapPath("Images/")+ fn; 
                        fn = ViewState["vNTPATHScanImage"].ToString() + fn;

                        try
                        {

                            iTextSharp.text.Image pic = iTextSharp.text.Image.getInstance(fn);

                            //iTextSharp.text.Image jpeg = iTextSharp.text.Image.getInstance((byte[]) ds.GetValue(1));
                            pic.scaleToFit(PageSize.A4.Width - 50, PageSize.A4.Height - 50);

                            //pic.scalePercent(36, 32); //for 200 dpi
                            //pic.scalePercent(21, 25); //for 300 dpi

                            objdocument.Add(pic);
                            //					Bread.Close();	
                            //Sabir Khan 5571 02/23/2009 Internet client sign up receipts...
                            Response.Redirect("../Temp/" + strFile, false); 
                            
                        }
                        catch
                        {

                        }
					}
					// LOAD FILE INTO BYTE ARRAY
					
					/*BinaryReader  Bread = new  BinaryReader(File.Open(fn, FileMode.Open));
					int len= (int) Bread.BaseStream.Length;
					byte[] pi = new byte[len];   
					Bread.Read(pi,0,len); */
					
					
					
				} 
				
				
			}
            //Nasir 5437 02/02/2009 take out from if condition to close document
            objdocument.Close();
			// objdocument.Close(); 
			//
			ViewState["DocID"] = strFileName;
            
            //Nasir 5437 01/26/2009 removing the files from upload folder which are left behind
            GM.DeleteTempFiles(Server.MapPath("../Temp/"), "*.pdf");
			
		}
        //Nasir 5437 01/26/2009 to log exception
        catch (Exception ex)
        {
           clsLogger.ErrorLog(ex);
        }
		}

	/*	private void ShowPDFN(long DocID) 
		{ 
			Document objdocument = new Document(); 
			SqlDataReader   ds;
			//string strImgNo; 
			//			DataRow ImageRow; 
			DataTable dtlTemp1;
			string strFile; 
			strFileName = Server.MapPath("../Temp/"); 
			strFile = Session.SessionID + DocID.ToString() + ".pdf"; 
			strFileName += strFile; 
			PdfWriter.getInstance(objdocument, new FileStream(strFileName, FileMode.Create)); 
			objdocument.Open(); 
		
			
			string[] key  = {"@DocID"};
			object[] value1 = {DocID};

			
			ds = ClsDb.Get_DR_BySPArr("usp_Get_SacanDocImageByDocID_NEW",key,value1);

			
				while (ds.Read())
				{
					    iTextSharp.text.Image jpeg = iTextSharp.text.Image.getInstance((byte[]) ds.GetValue(1));
						jpeg.scaleToFit(PageSize.A4.Width - 16 , PageSize.A4.Height);
						objdocument.Add(jpeg);
						
										
				}
				objdocument.Close();
			 
			// objdocument.Close();
			Session["DocID"] = strFileName; 
			Response.Redirect("../Temp/" + strFile); 
		}*/

	}
}

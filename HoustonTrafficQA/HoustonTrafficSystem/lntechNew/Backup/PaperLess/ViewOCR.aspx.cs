using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;

namespace lntechNew.PaperLess
{
	/// <summary>
	/// Summary description for ViewOCR.
	/// </summary>
	public partial class ViewOCR : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Button btn_Update;
		protected System.Web.UI.WebControls.Label lblDocType;
		protected System.Web.UI.WebControls.TextBox txtOCRData;
		protected System.Web.UI.WebControls.Label lblDesc;
		
		clsSession ClsSession=new clsSession();
		protected System.Web.UI.WebControls.Button btnCancel;
		clsENationWebComponents ClsDB=new clsENationWebComponents();	
		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				if (ClsSession.IsValidSession(this.Request)==false)
				{
					Response.Redirect("../frmlogin.aspx");
				}
				else //To stop page further execution
				{

					if(!IsPostBack)
					{
						//added
						ViewState["vSearch"]=Request.QueryString["search"];
						ViewState["vTicketId"]=Request.QueryString["casenumber"];
						//
						//lblDocType.Text=Session["sdoctypedesc"].ToString();
						lblDocType.Text=Request.QueryString["sdoctypedesc"].ToString();
						//lblDesc.Text=Session["sdocdesc"].ToString();
						lblDesc.Text=Request.QueryString["sdocdesc"].ToString();
						string[] key    = {"@doc_id","@docnum"};
						//object[] value1 = {Convert.ToInt32(Session["sdoc_id"].ToString()),Convert.ToInt32(Session["sdocnum"].ToString())};
						object[] value1 = {Convert.ToInt32(Request.QueryString["sdoc_id"].ToString()),Convert.ToInt32(Request.QueryString["sdocnum"].ToString())};
						DataSet ds= ClsDB.Get_DS_BySPArr("usp_hts_get_ocrscandata",key,value1);
						txtOCRData.Text="";
						for(int i=0;i<ds.Tables[0].Rows.Count;i++)
						{
							txtOCRData.Text=txtOCRData.Text+ds.Tables[0].Rows[i]["data"].ToString();
						}
					}
				}
			}
			catch
			{}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btn_Update.Click += new System.EventHandler(this.btn_Update_Click);
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btn_Update_Click(object sender, System.EventArgs e)
		{
			try
			{
				string[] key    = {"@doc_id","@docnum","@data"};

				//object[] value1 = {Convert.ToInt32(Session["sdoc_id"].ToString()),Convert.ToInt32(Session["sdocnum"].ToString()),txtOCRData.Text};
				object[] value1 = {Convert.ToInt32(Request.QueryString["sdoc_id"].ToString()),Convert.ToInt32(Request.QueryString["sdocnum"].ToString()),txtOCRData.Text};
					
				ClsDB.ExecuteSP("USP_HTS_Update_ScanData",key,value1);

				//HttpContext.Current.Response.Write("<script language='javascript'> opener.location.reload(); self.close(); </script>");
				Response.Redirect("Documents.aspx?casenumber="+ViewState["vTicketId"].ToString()+"&search="+ViewState["vSearch"].ToString());
			}
			catch
			{
				
			}
			
			
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			try
			{
                if (Convert.ToInt32(Request.QueryString["flagLicence"]) == 1)
                    Response.Redirect("/clientinfo/generalinfo.aspx?sMenu=62&casenumber=" + ViewState["vTicketId"].ToString() + "&search=" + ViewState["vSearch"].ToString());
                else
				    Response.Redirect("Documents.aspx?casenumber="+ViewState["vTicketId"].ToString()+"&search="+ViewState["vSearch"].ToString());
			}
			catch
			{}
		}
	}
}

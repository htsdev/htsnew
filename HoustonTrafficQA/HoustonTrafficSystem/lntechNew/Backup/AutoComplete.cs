using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using HTP.Components.ClientInfo;
using lntechNew.Components.ClientInfo;

//waqas 6666 10/22/2009 disable javascript suggestion

namespace HTP
{
    /// <summary>
    /// Summary description for AutoComplete
    /// </summary>
    //[WebService(Namespace = "HTP")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]

    public class AutoComplete : System.Web.Services.WebService
    {
        [WebMethod]
        public string[] GetEmployer(string prefixText)
        {
            clsCase Ccase = new clsCase();
            return Ccase.GetCaseEmployer(prefixText);
        }
        [WebMethod]
        public string[] GetOccupation(string prefixText)
        {
            clsCase Ccase = new clsCase();
            return Ccase.GetCaseOccupation(prefixText);
        }
    }
}

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.backroom
{
	/// <summary>
	/// Summary description for ViewBugs.
	/// </summary>
	public partial class ViewBugs : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.HyperLink hp_addnewbug;

		DataView DV; 
		string StrAcsDec = String.Empty ;
		string StrExp = String.Empty ;
        Bug View_Bugs = new Bug();
        clsSession cSession = new clsSession();
        clsLogger BugTracker = new clsLogger();
		protected void Page_Load(object sender, System.EventArgs e)
		{
            if (cSession.IsValidSession(this.Request) == false)
            {
                Response.Redirect("../frmlogin.aspx", false);
            }
            else
            {
                if (!IsPostBack)
                {
                    lblMessage.Visible = false;
                    lblMessage.Text = "";
                    BindGrid();
                }
            }
		}
		private void BindGrid()
		{
            string Test = "";
			try
            { 

                DataSet ds_result = View_Bugs.ViewBugs();
				if(ds_result.Tables[0].Rows.Count > 0)
				{ 
					dg_Viewbug.DataSource=ds_result;
					DV=new DataView(ds_result.Tables[0]);
					Session["sDV"]=DV;
					dg_Viewbug.DataBind();

				}
				else
				{
					lblMessage.Text="Sorry No Record Found";
					lblMessage.Visible=true;

				}
			}
			catch(Exception ex)
			{
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
				lblMessage.Text=ex.Message;
				lblMessage.Visible=true;
			
			}
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dg_Viewbug.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_Viewbug_ItemCommand);
			this.dg_Viewbug.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dg_Viewbug_PageIndexChanged);
			this.dg_Viewbug.SortCommand += new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.dg_Viewbug_SortCommand);

		}
		#endregion

		private void dg_Viewbug_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			try
			{ 
				if(e.CommandName == "Edit")
				{
					string bugid=((Label)e.Item.FindControl("lblbugid")).Text.ToString();
					Response.Redirect("UpdateDeveloper.aspx?Bugid=" + bugid,false);
				}

			}
			catch(Exception ex)
			{
                    BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
					lblMessage.Text=ex.Message;
					lblMessage.Visible=true;
			}
		}

		private void dg_Viewbug_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			try
			{ 
				dg_Viewbug.CurrentPageIndex=e.NewPageIndex;
				
				DV=(DataView)Session["sDV"];
				dg_Viewbug.DataSource=DV;
				dg_Viewbug.DataBind();
			}
			catch(Exception ex)
			{
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
				lblMessage.Text=ex.Message;
				lblMessage.Visible=true;
			}

		}
		private void SortGrid(string SortExp)
		{
			try
			{
				SetAcsDesc(SortExp);
				DV = (DataView) Session["sDV"];
				DV.Sort = StrExp + " " + StrAcsDec;
				dg_Viewbug.DataSource = DV;
				dg_Viewbug.DataBind();
			}
			catch(Exception ex)
			{
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
				lblMessage.Text=ex.Message;
				lblMessage.Visible=true;
			}
		}
		private void SetAcsDesc(string Val)
		{
			try
			{
				StrExp =  Session["StrExp"].ToString();
				StrAcsDec = Session["StrAcsDec"].ToString();
			}
			catch 
			{
				
			}

			if (StrExp == Val)
			{
				if (StrAcsDec == "ASC")
				{
					StrAcsDec = "DESC";
					Session["StrAcsDec"]=StrAcsDec;
				}
				else 
				{
					StrAcsDec = "ASC";
					Session["StrAcsDec"]=StrAcsDec;
				}
			}
			else 
			{
				StrExp = Val;
				StrAcsDec = "ASC";
				Session["StrExp"]= StrExp;
				Session["StrAcsDec"]= StrAcsDec;
			}
		}

		private void dg_Viewbug_SortCommand(object source, System.Web.UI.WebControls.DataGridSortCommandEventArgs e)
		{
			try
			{
				SortGrid (e.SortExpression);
			}
			catch(Exception ex)
			{
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
				lblMessage.Text=ex.Message;
				lblMessage.Visible=true;
			}

		}

        protected void lnkback_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("GeneralBug.aspx",false);
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void dg_Viewbug_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                string status = ((Label)e.Item.FindControl("lblstatus")).Text.ToString();
                if (status == "Closed")
                {
                    ((Label)e.Item.FindControl("lblstatus")).ForeColor = System.Drawing.Color.Red;
                }
            }
        }

        
	}
}

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using eWorld.UI;
//using SQlSrvMrg;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;


namespace HTP.backroom
{
    /// <summary>
    /// This Form Is Used To Execute Procedure Directly for the Application
    /// </summary>
    public partial class frmExeSpByRptId : System.Web.UI.Page
    {
        #region Controls

        protected System.Web.UI.WebControls.Label lblmsg;
        protected System.Web.UI.WebControls.DataGrid DgResults;
        protected System.Web.UI.WebControls.DataGrid Dg_Output;
        protected System.Web.UI.WebControls.Button imgbtnGo;
        protected System.Web.UI.WebControls.ImageButton ImgBtnExport;
        protected System.Web.UI.WebControls.HyperLink Hyperlink2;
        protected System.Web.UI.WebControls.HyperLink hlback;
        protected System.Web.UI.WebControls.Label lbltotalrow;
        protected System.Web.UI.WebControls.Label lblrows;
        protected System.Web.UI.WebControls.Label lblRptNm;
        protected System.Web.UI.HtmlControls.HtmlInputHidden txtHidden;

        #endregion

        #region Variables

        string RptId;
        clsENationWebComponents clsDB = new clsENationWebComponents();
        string ConStr = ConfigurationSettings.AppSettings["connString"].ToString();
        clsLogger clog = new clsLogger();

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ImgBtnExport.Click += new System.Web.UI.ImageClickEventHandler(this.ImgBtnExport_Click);
            this.Dg_Output.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.Dg_Output_ItemDataBound);
            this.imgbtnGo.Click += new System.EventHandler(this.imgbtnGo_Click);
            this.DgResults.ItemCreated += new System.Web.UI.WebControls.DataGridItemEventHandler(this.DgResults_ItemCreated);
            this.DgResults.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.DgResults_ItemDataBound);
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion

        #region Events

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                imgbtnGo.Attributes.Add("onclick", "javascript: return ValidateInput();");

                lblmsg.Text = ""; //Clear the lblMsg b/c of view state
                RptId = Convert.ToString(Request.QueryString["RptId"]);

                if (IsPostBack == false)
                {
                    // TAHIR AHMED 3733 05/14/2008
                    // FIXED THE BUG THAT OCCURS IF NOT REFEREED FROM
                    // THE REPORTS PAGE BUT ACCESSED DIRECTLY THROUGH URL.
                    if (Request.UrlReferrer == null)
                    {
                        hlback.NavigateUrl = "frmreports.aspx";
                    }
                    else
                    {
                        hlback.NavigateUrl = Request.UrlReferrer.ToString();
                    }
                    // END 3733

                    //if (Request.UrlReferrer!=null) //by khalid 2-1-08 bug 1255
                    //    hlback.NavigateUrl = Request.UrlReferrer.ToString();
                    ImgBtnExport.Visible = false;
                    if (RptId != null) //by khalid 2-1-08 bug 1255
                        ListParameters(RptId);
                }


            }
            catch (Exception ex)
            {
                lblmsg.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void DgResults_ItemCreated(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            //DgResults.HeaderStyle.CssClass = "clsaspcolumnheader";
            //DgResults.HeaderStyle.Font.Bold = true;
        }

        private void Dg_Output_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            try
            {
                DataRowView drv = (DataRowView)e.Item.DataItem;

                if (drv == null) return;
                int colVis = (int)Convert.ChangeType(drv["colVisibility"], typeof(int));

                if (colVis == 1)//colVis==1 means Parameter will be visible
                {

                    int objType = (int)Convert.ChangeType(drv["objectType"], typeof(int));

                    //adding control(TextBox or Dropdownlist) into ParamValue Column of DataGrid dynamically
                    if (objType == 1)
                    {
                        //objType==1 means textbox

                        ((TextBox)e.Item.FindControl("txtParamValue")).Visible = true;
                        ((DropDownList)e.Item.FindControl("ddlParamValue")).Visible = false;
                        ((eWorld.UI.CalendarPopup)((e.Item.FindControl("dtPicker")))).Visible = false;
                        string strDefVal = ((Label)e.Item.FindControl("lblDefaultVal")).Text;
                        ((TextBox)e.Item.FindControl("txtParamValue")).Text = strDefVal;

                        //string ParamType = ((Label)e.Item.FindControl("lblParamType")).Text;
                        //((System.Web.UI.HtmlControls.HtmlInputText)e.Item.FindControl("hiddenVal")).Value = ParamType;
                    }
                    else if (objType == 2)
                    {
                        //objType==2 means cmbo
                        ((TextBox)e.Item.FindControl("txtParamValue")).Visible = false;
                        ((DropDownList)e.Item.FindControl("ddlParamValue")).Visible = true;
                        ((eWorld.UI.CalendarPopup)((e.Item.FindControl("dtPicker")))).Visible = false;

                        int CmdType = (int)Convert.ChangeType(drv["CommandType"], typeof(int));
                        string CmdText = (string)Convert.ChangeType(drv["CommandText"], typeof(string));

                        DataSet ds = ExecuteCommandText(CmdType, CmdText);
                        if (ds.Tables.Count != 0) // for invalid Commmand Text
                        {
                            if (ds.Tables[0].Columns.Count >= 2)
                            {
                                ((DropDownList)e.Item.FindControl("ddlParamValue")).DataSource = ds;
                                //frist column always represents DataValueField of DropDownList
                                ((DropDownList)e.Item.FindControl("ddlParamValue")).DataValueField = ds.Tables[0].Columns[0].ColumnName;
                                //2nd column always represents DataTextField of DropDownList
                                ((DropDownList)e.Item.FindControl("ddlParamValue")).DataTextField = ds.Tables[0].Columns[1].ColumnName;
                                ((DropDownList)e.Item.FindControl("ddlParamValue")).DataBind();
                            }
                            else
                            {
                                lblmsg.Text = "DropdownList could not be fill b/c of invalide Command Text entered by Report Editor";
                            }
                        }
                        else
                        {
                            lblmsg.Text = "DropdownList could not be fill b/c of invalide Command Text entered by Report Editor";
                        }



                    }
                    else if (objType == 3)
                    {
                        //objType==3 means CalenderGroup
                        ((TextBox)e.Item.FindControl("txtParamValue")).Visible = false;
                        ((DropDownList)e.Item.FindControl("ddlParamValue")).Visible = false;
                        ((eWorld.UI.CalendarPopup)((e.Item.FindControl("dtPicker")))).Visible = true;
                        string strDefVal = ((Label)e.Item.FindControl("lblDefaultVal")).Text;

                        //if ( clsGeneralMethods.isDate(strDefVal))
                        //{
                        //    ((eWorld.UI.CalendarPopup)((e.Item.FindControl("dtPicker")))).SelectedDate = DateTime.Parse(strDefVal);                            
                        //}
                        ((eWorld.UI.CalendarPopup)((e.Item.FindControl("dtPicker")))).SelectedDate = DateTime.Today;
                    }
                }
                else//colVis ==0 means Parameter will invisible but defaultVal is assigned in it
                {
                    int objType = (int)Convert.ChangeType(drv["objectType"], typeof(int));

                    if (objType == 1)
                    {
                        ((TextBox)e.Item.FindControl("txtParamValue")).Visible = true;
                        ((DropDownList)e.Item.FindControl("ddlParamValue")).Visible = false;
                        ((eWorld.UI.CalendarPopup)((e.Item.FindControl("dtPicker")))).Visible = false;
                        string strDefVal = ((Label)e.Item.FindControl("lblDefaultVal")).Text;
                        ((TextBox)e.Item.FindControl("txtParamValue")).Text = strDefVal;

                    }
                    else if (objType == 3)
                    {
                        //ObjType == 3 means CalendarPopup of eworld.ui
                        ((TextBox)e.Item.FindControl("txtParamValue")).Visible = false;
                        ((DropDownList)e.Item.FindControl("ddlParamValue")).Visible = false;
                        ((eWorld.UI.CalendarPopup)((e.Item.FindControl("dtPicker")))).Visible = true;
                        string strDefVal = ((Label)e.Item.FindControl("lblDefaultVal")).Text;
                        //if ( clsGeneralMethods.isDate(strDefVal))
                        //{
                        //    //((eWorld.UI.CalendarPopup)((e.Item.FindControl("dtPicker")))).SelectedDate = DateTime.Parse(strDefVal);                            
                        //}
                        ((eWorld.UI.CalendarPopup)((e.Item.FindControl("dtPicker")))).SelectedDate = DateTime.Today;
                    }
                    //current row will be invisible
                    e.Item.Visible = false;
                }
            }
            catch (Exception ex)
            {
                lblmsg.Text = ex.Message;
            }
        }

        private void ImgBtnExport_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            //    ' One line handles all of the export. We're simply calling the component (cmpDataGridToExcel),
            //    ' then we're using it's only method (DataGridToExcel), and we're passing our DataGrid (dgToExport) and the value reponse. Note: If you're using VS.Net, once you
            //    ' build your solution after creating the component, Intellisense will now include your 
            //    ' component. Just remember you have to build it first.
            //    ' 
            //    ' You could also modify your datagrid here before exporting it. For instance in my
            //    ' invoice example we had a checkbox in our datagrid. If you have one of those the export
            //    ' will generate an error so we simply removed the column first like this before exporting:
            //    ' dgToExport.Columns.Remove(dgToExport.Columns.Item(11))
            //cmpDataGridToExcel.DataGridToExcel(DgResults, Response);
            Response.Redirect("../Reports/FrmExportToExcel.aspx", false);

        }

        private void imgbtnGo_Click(object sender, System.EventArgs e)
        {
            RunReport();

        }

        private void DgResults_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            try
            {


                ((Label)e.Item.FindControl("lbl_sno")).Text = Convert.ToString(e.Item.ItemIndex + 1);


            }
            catch (Exception)
            {

            }

        }

        #endregion

        #region Methods

        public void RunReport()
        {

            //

            try
            {
                //DgResults.DataSource = new DataSet();
                //DgResults.DataBind();      

                lblmsg.Text = "";
                int idx = 0;
                string SpName = "";

                if (Dg_Output.Items.Count != 0)
                {
                    SqlParameter[] arParms = new SqlParameter[Dg_Output.Items.Count];
                    foreach (DataGridItem ItemX in Dg_Output.Items)
                    {
                        string name = ((Label)ItemX.FindControl("lblParamName")).Text;
                        string type = ((Label)ItemX.FindControl("lblParamType")).Text;
                        string len = ((Label)ItemX.FindControl("lblParamLength")).Text;
                        string alias = ((Label)ItemX.FindControl("lblParamAlias")).Text;


                        object sValue = "";

                        if (((Label)ItemX.FindControl("lblObjType")).Text == "1")
                        {
                            //ObjType == 1 means Textbox
                            sValue = ((TextBox)ItemX.FindControl("txtParamValue")).Text;
                        }
                        else if (((Label)ItemX.FindControl("lblObjType")).Text == "2")
                        {
                            //ObjType == 2 means DropdownList
                            sValue = ((DropDownList)ItemX.FindControl("ddlParamValue")).SelectedItem.Value;
                        }
                        else if (((Label)ItemX.FindControl("lblObjType")).Text == "3")
                        {
                            //ObjType == 3 means CalendarPopup of eworld.ui
                            sValue = ((eWorld.UI.CalendarPopup)((ItemX.FindControl("dtPicker")))).SelectedDate.ToString("MM/dd/yyyy");
                        }

                        //every row has same SP Name
                        SpName = ((Label)ItemX.FindControl("lblSpName")).Text;

                        //int length = ((int)(Convert.ChangeType(len,typeof(int)) ));

                        string CastValue = ((Label)ItemX.FindControl("lblParamType")).Text;

                        sValue = SetDataType(CastValue, sValue.ToString());

                        arParms[idx] = new SqlParameter(name, sValue);
                        idx++;
                    }

                    //Making call Stored Procedure and Binding to grid
                    SqlConnection Conn = new SqlConnection(ConStr);

                    SqlCommand myCommand = new SqlCommand();
                    myCommand.CommandType = CommandType.StoredProcedure;
                    myCommand.CommandText = SpName;

                    for (int i = 0; i < arParms.Length; i++)
                    {
                        myCommand.Parameters.Add(arParms[i]);
                    }

                    myCommand.CommandTimeout = 999;
                    myCommand.Connection = Conn;
                    myCommand.Connection.Open();

                    SqlDataAdapter da = new SqlDataAdapter(myCommand);
                    DataSet ds = new DataSet();

                    da.Fill(ds);



                    if (ds.Tables.Count > 0)
                    {
                        //AddSerialNo(ds);
                        //Sabir Khan 7723 04/27/2010 Remove S.No from Revenue/Client/Mailer count by Violation Description...
                        //Zeeshan Haider 11267 07/17/2013 Commented for extra column in report header.
                        //if (Convert.ToInt32(Request.QueryString["RptId"].ToString()) != 69)
                        //{
                        //    ds.Tables[0].Columns.Add("S.No");
                        //}
                        ShowRowCount(ds.Tables[0].Rows.Count);
                    }
                    //Faique 10706 to generate Excle file if Signed up Clients Report only 
                    //Farrukh 11123 05/27/2013 Added Check for Disposed Cases Report
                    string spName = Session["SpName"].ToString().ToLower();
                    if (spName == "usp_htp_getSignUpClient_byDate".ToLower() || spName == "usp_htp_getDisposedClient_byDate".ToLower())
                    {
                        SignupCliendXls(ds, Response);
                        ImgBtnExport.Visible = false;
                    }
                    else
                    {

                        DgResults.DataSource = ds;
                        DgResults.DataBind();
                        Conn.Close();

                        //FormatDataGrid();
                        Session["MoveDataSet"] = ds;
                        ImgBtnExport.Visible = true;
                    }

                    //					DataSet ds = SqlHelper.ExecuteDataset(ConStr,CommandType.StoredProcedure,SpName,arParms);
                    //					DgResults.DataSource = ds;
                    //					DgResults.DataBind();
                    //					Session["MoveDataSet"] = ds;					
                }
                else
                {
                    //ExecuteDataset of SP without params will call b/c Dg_Output.Items.Count == 0 
                    string strSpName = (string)Convert.ChangeType(Session["SpName"], typeof(string));
                    SqlConnection Conn = new SqlConnection(ConStr);
                    SqlCommand myCommand = new SqlCommand();
                    myCommand.CommandType = CommandType.StoredProcedure;
                    myCommand.CommandText = strSpName;
                    myCommand.CommandTimeout = 999;
                    myCommand.Connection = Conn;
                    myCommand.Connection.Open();

                    SqlDataAdapter da = new SqlDataAdapter(myCommand);
                    DataSet ds = new DataSet();

                    //ds.Tables[0].Columns.Add("S.No");

                    da.Fill(ds);


                    if (ds.Tables.Count > 0)
                    {
                        //AddSerialNo(ds);
                        ShowRowCount(ds.Tables[0].Rows.Count);
                    }
                    DgResults.DataSource = ds;
                    DgResults.DataBind();

                    Session["MoveDataSet"] = ds;
                    Conn.Close();
                    //FormatDataGrid();
                    //					string strSpName = (string)Convert.ChangeType(Session["SpName"],typeof(string));
                    //					DataSet ds = SqlHelper.ExecuteDataset(ConStr,CommandType.StoredProcedure,strSpName);
                    //					Session["MoveDataSet"] = ds;
                    //					DgResults.DataSource = ds;
                    //					DgResults.DataBind();
                    ImgBtnExport.Visible = true;
                }



            }
            catch (Exception ex)
            {
                ImgBtnExport.Visible = false;
                lblmsg.Text = ex.Message;
            }



            //

        }

        public void AddSerialNo(DataSet ds)
        {






            /*
                ds.Tables[0].Columns.Add("S.No");
                for( int i=0 ; i < ds.Tables[0].Rows.Count ; i++)
                {
								
                    ds.Tables[0].Rows[i]["S.No"]=i+1;
						
                }
            */


            DataTable temp = new DataTable("Temp");


            temp.Columns.Add("S.No");

            for (int k = 0; k < ds.Tables[0].Columns.Count; k++)
            {
                temp.Columns.Add(ds.Tables[0].Columns[k].ColumnName);
            }





            DataRow dr;
            //dr=temp.NewRow;


            for (int row = 0; row < ds.Tables[0].Rows.Count; row++)
            {



                dr = temp.NewRow();



                for (int col = 0; col < temp.Columns.Count; col++)
                {

                    if (col == 0)
                    {
                        dr[col] = row + 1;
                    }
                    else
                    {

                        dr[col] = ds.Tables[0].Rows[row][col - 1];
                    }



                }

                temp.Rows.Add(dr);

            }



            int rcount = temp.Rows.Count;
            int count = temp.Columns.Count;


            ds.Tables.Remove(ds.Tables[0].TableName);
            ds.Tables.Add(temp);



        }

        public void ShowRowCount(int RowCount)
        {

            if (RowCount > 0)
            {

                lblrows.Text = RowCount.ToString();
                lblrows.Visible = true;
                lbltotalrow.Visible = true;

            }
            else
            {
                lblrows.Visible = false;
                lbltotalrow.Visible = false;

            }



        }

        public void FormatDataGrid()
        {
            //DgResults.HeaderStyle.CssClass = "clsaspcolumnheader";
            //DgResults.HeaderStyle.Font.Bold = true;


            //int a = ds.Tables[0].Columns.Count;
            int b = DgResults.Columns.Count;
            /*

                    for ( int i=0 ; i <ds.Tables[0].Columns.Count ; i++ )
                    {
		
                        //DgResults.Columns.Add;
                        DgResults.Columns[i].HeaderStyle.Font.Bold=true;
     	
                    } */



        }

        private void ListParameters(string RptId)
        {
            try
            {
                int iRptId = (int)(Convert.ChangeType(RptId, typeof(int)));

                //SqlParameter [] arParm = new SqlParameter[1];
                //arParm[0] = new SqlParameter("@RptId",iRptId);
                //DataSet ds = SqlHelper.ExecuteDataset(ConStr,CommandType.StoredProcedure,"ExeSpByRptId",arParm); 
                string[] key = { "@RptId" };
                object[] value1 = { iRptId };
                DataSet ds = clsDB.Get_DS_BySPArr("ExeSpByRptId", key, value1);

                Session["SpName"] = ((string)(Convert.ChangeType(ds.Tables[0].Rows[0]["SpName"], typeof(string)))).Trim();
                Session["RptName"] = ((string)(Convert.ChangeType(ds.Tables[0].Rows[0]["RptName"], typeof(string)))).Trim();
                lblRptNm.Text = (string)Session["RptName"];
                string str = (string)(Convert.ChangeType(ds.Tables[0].Rows[0]["ParamName"], typeof(string)));

                if (str.Length < 1)
                {
                    RunReport();
                    return;
                }
                else
                {
                    Dg_Output.DataSource = ds;
                    Dg_Output.DataBind();
                }

                //following code will save total rows of grid into hidden textBox used by javascripting
                txtHidden.Value = Dg_Output.Items.Count.ToString();

            }
            catch (Exception ex)
            {
                lblmsg.Text = ex.Message;
                txtHidden.Value = "";
            }
        }

        private Object SetDataType(string StrType, string StrValue)
        {
            switch (StrType.ToUpper())
            {
                case "VARCHAR":
                case "CHAR":
                    return StrValue.ToString();

                case "NVARCHAR":
                    return StrValue.ToString();

                case "DATETIME":
                    return ConvertIntoDate(StrValue.ToString());

                case "INT":
                case "SMALLINT":
                case "TINYINT":
                case "BIGINT":
                    return ((int)Convert.ChangeType(StrValue, typeof(int)));

                case "NUMERIC":
                    return ((int)Convert.ChangeType(StrValue, typeof(int)));

                case "BIT":
                    return ((Boolean)Convert.ChangeType(StrValue, typeof(Boolean)));

                case "MONEY":
                case "FLOAT":
                    return ((Double)Convert.ChangeType(StrValue, typeof(Double)));

                default:
                    return StrType.ToString();


            }




        }

        private DateTime ConvertIntoDate(string Strdate)
        {
            try
            {
                Int16 StartPos = 0;
                Int16 EndPos = 0;
                string dtFormat = "MM/dd/yyyy";

                string month = null;
                string day = null;
                string year = null;

                StartPos = (Int16)dtFormat.IndexOf("M");
                EndPos = (Int16)dtFormat.LastIndexOf("M");
                //			dtFormat.
                month = Strdate.Substring(StartPos, (EndPos + 1) - StartPos);

                StartPos = (Int16)dtFormat.IndexOf("d");
                EndPos = (Int16)dtFormat.LastIndexOf("d");
                day = Strdate.Substring(StartPos, (EndPos + 1) - StartPos);

                StartPos = (Int16)dtFormat.IndexOf("y");
                EndPos = (Int16)dtFormat.LastIndexOf("y");
                year = Strdate.Substring(StartPos, (EndPos + 1) - StartPos);

                return Convert.ToDateTime(month + "/" + day + "/" + year);
            }
            catch
            {
                return Convert.ToDateTime("01/01/1900");
            }

        }

        private DataSet ExecuteCommandText(int CmdType, string CmdText)
        {
            DataSet ds = new DataSet();
            try
            {
                if (CmdType == 1)
                {
                    // CmdType == 1 means inline Qry
                    ds = SqlHelper.ExecuteDataset(ConStr, CommandType.Text, CmdText.Trim());
                }
                else if (CmdType == 2)
                {
                    // CmdType == 2 means SP
                    ds = SqlHelper.ExecuteDataset(ConStr, CommandType.StoredProcedure, CmdText.Trim());
                }
            }
            catch (Exception ex)
            {
                lblmsg.Text = ex.Message;
                return ds;
            }

            return ds;
        }

        public bool isNumeric(string str)
        {
            try
            {
                Int32.Parse(str);
            }
            catch
            {
                return false;
            }
            return true;
        }

        public void SignupCliendXls(DataSet DS, HttpResponse response)
        {
            //Faique 10706 02/14/2013 to generate Excel file with hyperlinked to each client profile
            DataTable dt = new DataTable();
            dt = DS.Tables[0];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["TICKETID"].ToString() != "0")
                {

                    dt.Rows[i]["TICKET NUMBER"] = "<a href=http://ln.legalhouston.com/ClientInfo/ViolationFeeold.aspx?sMenu=61&search=0&caseNumber=" + dt.Rows[i]["TICKETID"].ToString() + "  target=_blank>" + dt.Rows[i]["TICKET NUMBER"].ToString() + "</a>";
                    dt.Rows[i]["CASE NUMBER"] = "<a href=http://ln.legalhouston.com/ClientInfo/ViolationFeeold.aspx?sMenu=61&search=0&caseNumber=" + dt.Rows[i]["TICKETID"].ToString() + "  target=_blank>" + dt.Rows[i]["CASE NUMBER"].ToString() + "</a>";

                }
            }
            dt.Columns.Remove("TICKETID");
            dt.Columns.Remove("S.No");

            response.Clear();
            response.Charset = "";
            response.ContentType = "application/vnd.ms-excel";
            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htmlWrite = new System.Web.UI.HtmlTextWriter(stringWrite);
            DataGrid dg = new DataGrid();
            dg.DataSource = DS.Tables[0];
            dg.GridLines = GridLines.None;
            dg.HeaderStyle.Font.Bold = true;
            dg.DataBind();
            dg.RenderControl(htmlWrite);
            response.Write(stringWrite.ToString());
            response.Flush();
            response.Close();
        }

        #endregion

    }
}

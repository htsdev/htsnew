﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AutoDialerSettingsHistory.aspx.cs"
    Inherits="HTP.backroom.AutoDialerSettingsHistory" %>

<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Auto Dialer Settings History</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="TableMain" cellspacing="0" cellpadding="0" width="780px" align="center"
            border="0">
            <tr>
                <td style="width: 100%">
                    <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                </td>
            </tr>
            <tr>
                <td background="../Images/subhead_bg.gif" style="height: 34px; width: 100%" class="clssubhead">
                    <table style="width: 100%">
                        <tr>
                            <td class="clssubhead" align="left">
                                Auto Dialer Settings History
                            </td>
                            <td align="right">
                                <aspnew:ScriptManager ID="ScriptManager2" runat="server" />
                                <aspnew:UpdatePanel ID="UpdatePanel2" runat="server">
                                    <ContentTemplate>
                                        <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                    </ContentTemplate>
                                </aspnew:UpdatePanel>
                            </td>
                            <td align="right">
                                <asp:HyperLink ID="hlk_Settings" runat="server">Settings</asp:HyperLink>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%" align="center">
                    <asp:Label ID="lbl_Message" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="height: 11px; width: 100%" background="../Images/separator_repeat.gif">
                </td>
            </tr>
            <tr>
                <td style="width: 100%">
                    <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline">
                        <ContentTemplate>
                            <asp:GridView ID="gv_Records" runat="server" AutoGenerateColumns="False" Width="100%"
                                AllowPaging="True" OnPageIndexChanging="gv_Records_PageIndexChanging" CellPadding="3"
                                PageSize="30" CssClass="clsLeftPaddingTable">
                                <Columns>
                                    <asp:BoundField DataField="recorddatetime" HeaderText="Date & Time">
                                        <ItemStyle CssClass="GridItemStyle" />
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="empname" HeaderText="User">
                                        <ItemStyle CssClass="GridItemStyle" />
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" Width="100px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="eventtypename" HeaderText="Event Type">
                                        <ItemStyle CssClass="GridItemStyle" />
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="notes" HeaderText="Description">
                                        <ItemStyle CssClass="GridItemStyle" />
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                    </asp:BoundField>
                                </Columns>
                                <PagerStyle HorizontalAlign="Center" />
                                <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                    FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                            </asp:GridView>
                        </ContentTemplate>
                    </aspnew:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td background="../Images/separator_repeat.gif" style="height: 11px; width: 100%">
                </td>
            </tr>
            <tr>
                <td style="width: 100%">
                    <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient ;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;




namespace lntechNew
{
	/// <summary>
	/// Summary description for frmAddPricePlan.
	/// </summary>
	///

	public partial class frmAddPricePlan : System.Web.UI.Page
	{
		protected eWorld.UI.CalendarPopup calFrom;
		protected System.Web.UI.WebControls.CheckBox chkb_savetemplate;
		protected System.Web.UI.WebControls.Label Label1;
		clsENationWebComponents ClsDb = new clsENationWebComponents();
		protected System.Web.UI.WebControls.DropDownList ddl_crtloc;
		protected System.Web.UI.WebControls.TextBox txtPlanName;
		protected System.Web.UI.WebControls.DropDownList ddl_TimeFrom;
		protected System.Web.UI.WebControls.DropDownList ddl_TimeTo;		
		clsCourts ClsCourts =  new clsCourts();
		protected eWorld.UI.CalendarPopup calTo;
		protected System.Web.UI.WebControls.RadioButton rdo_Active;
		protected System.Web.UI.WebControls.RadioButton rdo_Inactive;
		protected System.Web.UI.WebControls.DataGrid dg_PlanDetail;
		protected System.Web.UI.WebControls.DropDownList ddl_templetes;
		protected System.Web.UI.WebControls.Button btn_Save;
		protected System.Web.UI.WebControls.Label test;
		protected System.Web.UI.HtmlControls.HtmlInputHidden txtHidden;
		protected System.Web.UI.WebControls.Label lblMessage; 
		clsPricingPlans ClsPricingPlans = new clsPricingPlans();
		protected System.Web.UI.WebControls.HyperLink HyperLink1;
		protected System.Web.UI.WebControls.TextBox txt_ShortName;
		clsSession ClsSession = new clsSession();
		clsLogger clog = new clsLogger();
				
		private void Page_Load(object sender, System.EventArgs e)
		{ 
			try
			{
				if (ClsSession.IsValidSession(this.Request)==false)
				{
					Response.Redirect("../frmlogin.aspx",false);
				}
				else //To stop page further execution
				{
					if (ClsSession.GetCookie("sAccessType",this.Request).ToString()!="2")
					{
						Response.Redirect("../LoginAccesserror.aspx",false);
						Response.End();
					}
					else //To stop page further execution
					{
						
						ClsPricingPlans.EmployeeID  = Convert.ToInt32(ClsSession.GetCookie("sEmpID",this.Request));
				
				
						if (Page.IsPostBack != true)
						{
							// filling drop down lists
							FillTimeDDL(ddl_TimeFrom);
							FillTimeDDL(ddl_TimeTo);
							FillCourtsLocation(ddl_crtloc);
							FillPricingTemplete();
							//setPlanDetailsDefault();
				
							if (Request.QueryString.Count ==2)// is page is comming from referenced page
							{
								if (Request.QueryString["Posted"] == "1")
								{
									ClsPricingPlans.PlanID  = Convert.ToInt32(Request.QueryString["ID"].ToString());
									Int32 PlanID;
									PlanID = ClsPricingPlans.PlanID;
									setPlanDataByID();
									setPlanDetails();
								}
							}
							else if (Request.QueryString.Count == 0)// new page 
							{
								setPlanDetailsDefault();
								rdo_Active.Checked = true;
							}
						}
				
						txtHidden.Value  = Convert.ToString(dg_PlanDetail.Items.Count) ;
						btn_Save.Attributes.Add("OnClick","return ValidateInput();"); 
					}
				}
			}
			catch(Exception ex )
			{
				test.Text = ex.Message.ToString();
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
	}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btn_Save.Click += new System.EventHandler(this.btn_Save_Click);
			this.ddl_templetes.SelectedIndexChanged += new System.EventHandler(this.ddl_templetes_SelectedIndexChanged);
			this.dg_PlanDetail.SelectedIndexChanged += new System.EventHandler(this.dg_PlanDetail_SelectedIndexChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		private void FillCourtsLocation(DropDownList ddl_Court)
		{
            //Change by Ajmal
			//SqlDataReader  rdr_Courts=null;
             IDataReader rdr_Courts = null;
			try
			{
				Int32 iCount=0;
				rdr_Courts = ClsCourts.GetAllCourtName();
				while (rdr_Courts.Read())
				{
					ddl_Court.Items.Add (rdr_Courts["shortcourtname"].ToString());
					ddl_Court.Items[iCount].Value = rdr_Courts["courtid"].ToString() ;
					iCount++ ;
				}
			}
			catch(Exception ex)
			{
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
			finally
			{
				rdr_Courts.Close();
			}
		}
		private void FillTimeDDL(DropDownList ddl_CrtTime)
		{
			try
			{
				Int32 i;
				ddl_CrtTime.Items.Add("--------");
				ddl_CrtTime.Items[0].Value = "00:00";
					
				ddl_CrtTime.Items.Add( "8:00 am");
				ddl_CrtTime.Items[1].Value =("08:00");
				ddl_CrtTime.Items.Add("8:15 am");
				ddl_CrtTime.Items[2].Value =("08:15");
				ddl_CrtTime.Items.Add("8:30 am");
				ddl_CrtTime.Items[3].Value =("08:30");
				ddl_CrtTime.Items.Add("8:45 am");
				ddl_CrtTime.Items[4].Value =("08:45");

				ddl_CrtTime.Items.Add( "9:00 am");
				ddl_CrtTime.Items[5].Value =("09:00");
				ddl_CrtTime.Items.Add("9:15 am");
				ddl_CrtTime.Items[6].Value =("09:15");
				ddl_CrtTime.Items.Add("9:30 am");
				ddl_CrtTime.Items[7].Value =("09:30");
				ddl_CrtTime.Items.Add("9:45 am");
				ddl_CrtTime.Items[8].Value =("09:45");
				
				Int32 iTime = 9;
				for ( i = 10;i <12;i++)
				{						
					ddl_CrtTime.Items.Add(i.ToString()  + ":00 am");
					ddl_CrtTime.Items[iTime].Value =(i.ToString()  + ":00");
					ddl_CrtTime.Items.Add(i.ToString() + ":15 am");
					ddl_CrtTime.Items[iTime+=1].Value =(i.ToString()  + ":15");
					ddl_CrtTime.Items.Add(i.ToString() + ":30 am");
					ddl_CrtTime.Items[iTime+=1].Value =(i.ToString()  + ":30");
					ddl_CrtTime.Items.Add(i.ToString() + ":45 am");
					ddl_CrtTime.Items[iTime+=1].Value =(i.ToString()  + ":45");
					iTime+=1 ;
				}	
		
		
				ddl_CrtTime.Items.Add("12:00 pm");
				ddl_CrtTime.Items[iTime].Value =("12:00");
				ddl_CrtTime.Items.Add("12:15 pm");
				ddl_CrtTime.Items[iTime+=1].Value =("12:15");
				ddl_CrtTime.Items.Add("12:30 pm");
				ddl_CrtTime.Items[iTime+=1].Value =("12:30");
				ddl_CrtTime.Items.Add("12:45 pm");
				ddl_CrtTime.Items[iTime+=1].Value =("12:45");
				iTime+=1 ;
			
			
				
				Int32 iTimeClock = 12;
				for ( i = 1;i <9;i++)
				{
					ddl_CrtTime.Items.Add(i.ToString() + ":00 pm");
					iTimeClock+=1;
					ddl_CrtTime.Items[iTime].Value =(iTimeClock.ToString()  + ":00");
					ddl_CrtTime.Items.Add(i.ToString() + ":15 pm");
					ddl_CrtTime.Items[iTime+=1].Value =(iTimeClock.ToString()  + ":15");
					ddl_CrtTime.Items.Add(i.ToString() + ":30 pm");
					ddl_CrtTime.Items[iTime+=1].Value =(iTimeClock.ToString()  + ":30");
					ddl_CrtTime.Items.Add(i.ToString() + ":45 pm");
					ddl_CrtTime.Items[iTime+=1].Value =(iTimeClock.ToString()  + ":45");
					iTime+=1 ;
				}
				ddl_CrtTime.Items.Add("9:00 pm");
				iTimeClock+=1;
				ddl_CrtTime.Items[iTime].Value =(iTimeClock.ToString()  + ":00");


			
				ddl_CrtTime.SelectedIndex = 0;
		
			}
			catch(Exception ex)
			{
				test.Text = ex.Message.ToString();
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}
		
		private void setPlanDataByID()
        {// setting plan info by Id 
			try
			{
				DateTime EffDate,EndDate;
				DataSet ds_PlanInfo =  ClsPricingPlans.GetPlan(ClsPricingPlans.PlanID);
				txtPlanName.Text = ds_PlanInfo.Tables[0].Rows[0]["PlanDescription"].ToString();
				txt_ShortName.Text = ds_PlanInfo.Tables[0].Rows[0]["PlanShortName"].ToString();
				EffDate = Convert.ToDateTime(ds_PlanInfo.Tables[0].Rows[0]["EffectiveDate"].ToString());
				EndDate = Convert.ToDateTime(ds_PlanInfo.Tables[0].Rows[0]["EndDate"].ToString());
			
				calFrom.SelectedDate = EffDate.Date;
				calTo.SelectedDate  = EndDate.Date;

				ddl_TimeFrom.SelectedValue = EffDate.TimeOfDay.ToString().Substring(0,5); 
				ddl_TimeTo.SelectedValue = EndDate.TimeOfDay.ToString().Substring(0,5);

				ddl_crtloc.SelectedValue = ds_PlanInfo.Tables[0].Rows[0]["CourtId"].ToString();
			
				if (ds_PlanInfo.Tables[0].Rows[0]["IsActivePlan"].ToString() == "")
				{
					rdo_Inactive.Checked = true;
					rdo_Active.Checked = false;
				}
				else 
				{
					rdo_Active.Checked = true;
					rdo_Inactive.Checked = false;
				}
			}
			catch(Exception ex)
			{
				test.Text = ex.Message.ToString();
			}
		}
		private void setPlanDetails()
		{// setting plan details
			try 
			{
				DataSet ds_PlanDetails = ClsPricingPlans.GetPlanDetails(ClsPricingPlans.PlanID); 
				dg_PlanDetail.DataSource = ds_PlanDetails;
				dg_PlanDetail.DataBind();
			}
			catch(Exception ex)
			{
				test.Text = ex.Message.ToString();
			}
		}

		private void FillPricingTemplete()
		{// fill combo with names of Templetets
            //Change by Ajmal
			//SqlDataReader rdr_PricingTemplate=null;
            IDataReader rdr_PricingTemplate = null;
			try
			{
				
				Int32 iCount=0;
				rdr_PricingTemplate = ClsPricingPlans.Get_rdr_PricingTemplates();
				ddl_templetes.Items.Add ("Available Template");
				ddl_templetes.Items[iCount].Value = "0";
				iCount++;
				while (rdr_PricingTemplate.Read())
				{
					ddl_templetes.Items.Add (rdr_PricingTemplate["TemplateName"].ToString());
					ddl_templetes.Items[iCount].Value = rdr_PricingTemplate["TemplateID"].ToString();
					iCount++;
				}
			}
			catch(Exception ex)
			{
				test.Text = ex.Message.ToString();
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
			finally
			{
				rdr_PricingTemplate.Close();
			}
		}

		private void ddl_templetes_SelectedIndexChanged(object sender, System.EventArgs e)
		{// filling prices with plans 
			try
			{
				DataSet ds_PlanDetails = ClsPricingPlans.GetPricingTemplateDetails(Convert.ToInt32(ddl_templetes.SelectedValue));
				dg_PlanDetail.DataSource = ds_PlanDetails;
				dg_PlanDetail.DataBind();
			}
			catch(Exception	ex)
			{
				test.Text = ex.Message.ToString();
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}

		private void btn_Save_Click(object sender, System.EventArgs e)
		{
			try
			{
				DbTransaction trans = ClsDb.DBBeginAndReturnTransaction();

				if (Request.QueryString["Posted"] == "1") // update
				{
					ClsPricingPlans.PlanID  = Convert.ToInt32(Request.QueryString["ID"].ToString());
					PerformUpdate(trans);
				}
				else if (Request.QueryString.Count  == 0) // Insert
				{
					PerformInsert(trans);
				}
				if (chkb_savetemplate.Checked == true)
				{
					AddTemplates(trans);
					AddTemplatesDetails(trans);
				}
			ClsDb.DBTransactionCommit();
			
			Response.Redirect ("PrincingMain.aspx");
			
			}
			catch(SqlException  ex)
			{
				test.Text = ex.Message.ToString();
				ClsDb.DBTransactionRollback();
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}
		private void PerformInsert(DbTransaction trans)
		{
			try
			{

				PlanInfoInsert(trans);
				PlanDetails(ClsPricingPlans.PlanID,trans);
			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message + " - " + ex.Source;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
			
		}
		private void PerformUpdate(DbTransaction trans)
		{
			try
			{
				PlanInfo(ClsPricingPlans.PlanID,trans);
				DeletePlanDetails(ClsPricingPlans.PlanID,trans);
				PlanDetails(ClsPricingPlans.PlanID,trans);
			}
			catch(Exception ex )
			{
				test.Text = ex.Message.ToString();
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}
		private void PlanInfo(int planID,DbTransaction trans)

		{
			try
			{
				ClsPricingPlans.PlanID = planID ;
				ClsPricingPlans.PlanDescription =  txtPlanName.Text ;
				ClsPricingPlans.PlanShortName = txt_ShortName.Text ;
				ClsPricingPlans.EffectiveDate = calDate(calFrom.SelectedDate,ddl_TimeFrom.SelectedValue);	
				
				ClsPricingPlans.EndDate  = calDate(calTo.SelectedDate,ddl_TimeTo.SelectedValue ) ;
		
				ClsPricingPlans.CourtID = Convert.ToInt32(ddl_crtloc.SelectedValue) ;
			
				if (rdo_Inactive.Checked == true)
				{
					ClsPricingPlans.ShowAll = 0;
				}
				else if (rdo_Active.Checked == true)
				{
					ClsPricingPlans.ShowAll = 1;
				}
				ClsPricingPlans.UpdatePlanInfo (trans);
			}
			catch(Exception ex)
			{
				test.Text= ex.Message.ToString();
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);				
			}
		}
		private DateTime  calDate(DateTime sDate,string strTime)
		{
			DateTime tempdt = Convert.ToDateTime(sDate);
			string newSubDate = tempdt.Date.ToShortDateString() ;
			String newDate;
			newDate = newSubDate + " "  + strTime.ToString();
			
			DateTime dtTime ;
			//DateTime dtDate ;
			dtTime = (DateTime)(Convert.ChangeType(newDate,typeof(System.DateTime)));
			
			double dHours = (double) (Convert.ChangeType(strTime.Substring(0,2),typeof(double))) ;
			double dMinutes = (double) (Convert.ChangeType(strTime.Substring(3,2),typeof(double))) ;
			sDate.AddHours(dHours);
			sDate.AddMinutes(dMinutes);

			
			return sDate;
		}
		
		private void PlanDetails(int planid,DbTransaction trans)
		{
			try
			{	
				if (planid != 0)
				{

					ClsPricingPlans.PlanID=planid;
					ClsPricingPlans.CourtID = Convert.ToInt32(ddl_crtloc.SelectedValue); 
			
					foreach (DataGridItem  items in dg_PlanDetail.Items) 
					{
						//String[] ArrPricingPlan = new string[12];
					
						String ArrPricingPlan0 =((Label)items.FindControl("lbl_hidCategoryID")).Text ;
				
						String ArrPricingPlan1 = ((TextBox) items.FindControl("tb_baseprice")).Text;
						String ArrPricingPlan2 = ((TextBox) items.FindControl("tb_secprice")).Text;
						String ArrPricingPlan3 =((TextBox) items.FindControl("tb_basepcent")).Text;
						String ArrPricingPlan4 =((TextBox) items.FindControl("tb_secpcent")).Text;
						String ArrPricingPlan5 =((TextBox) items.FindControl("tb_bondbase")).Text;
						String ArrPricingPlan6 =((TextBox) items.FindControl("tb_bondsec")).Text;
						String ArrPricingPlan7 =((TextBox) items.FindControl("tb_bondbasepcent")).Text;
						String ArrPricingPlan8 = ((TextBox) items.FindControl("tb_bondsecpcent")).Text;
						String ArrPricingPlan9 = ((TextBox) items.FindControl("tb_bondassump")).Text;
						String ArrPricingPlan10 = ((TextBox) items.FindControl("tb_fineassump")).Text;
				
						//	for(int i=1;i<11;i++)
						//{
						//	if(ArrPricingPlan[i].ToString() != "0")
						//	{
						//	ArrPricingPlan[11] = "1"; // go to update
						//	break;
						//}
						//	ArrPricingPlan[11] = "0"; //not go to update 
						//	}	
			


						if ((ArrPricingPlan1 != "0") || (ArrPricingPlan2 != "0") || (ArrPricingPlan3 != "0") || (ArrPricingPlan4 != "0") || (ArrPricingPlan5 != "0") || (ArrPricingPlan6 != "0") || (ArrPricingPlan7 != "0") || (ArrPricingPlan8 != "0") || (ArrPricingPlan9 != "0") || (ArrPricingPlan10 != "0"))
						{
							ClsPricingPlans.CategoryID =Convert.ToInt32(ArrPricingPlan0);
							ClsPricingPlans.BasePrice = Convert.ToDouble(ArrPricingPlan1);
							ClsPricingPlans.SecondaryPrice = Convert.ToDouble(ArrPricingPlan2);
							ClsPricingPlans.BasePercentage =Convert.ToDouble(ArrPricingPlan3);
							ClsPricingPlans.SecondaryPercentage =Convert.ToDouble(ArrPricingPlan4);
							ClsPricingPlans.BondBase =Convert.ToDouble(ArrPricingPlan5);
							ClsPricingPlans.BondSecondary = Convert.ToDouble(ArrPricingPlan6);
							ClsPricingPlans.BondBasePercentage = Convert.ToDouble(ArrPricingPlan7);
							ClsPricingPlans.BondSecondaryPercentage = Convert.ToDouble(ArrPricingPlan8);
							ClsPricingPlans.BondAssumtion =Convert.ToDouble(ArrPricingPlan9);
							ClsPricingPlans.FineAssumption = Convert.ToDouble(ArrPricingPlan10);
						
							ClsPricingPlans.UpdatePlanDetails(trans);
						
						}
					}
				}
				else
				{
					lblMessage.Text = "Invalid Transaction";
				}
			}
			catch(Exception ex )
			{
				test.Text = ex.Message.ToString();
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}
		
		private void setPlanDetailsDefault()
		{
			try
			{

				DataSet ds_PlanDetails = ClsPricingPlans.GetPricingTemplateDetails(0); 
				dg_PlanDetail.DataSource = ds_PlanDetails;
				dg_PlanDetail.DataBind();
			}
			catch(Exception ex)
			{
				test.Text = ex.Message.ToString();
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}

		private void dg_PlanDetail_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}

		private void DeletePlanDetails(int planID,DbTransaction trans)
		{
			try
			{
				if (ClsPricingPlans.DeletePlanDetails(planID,trans) == false)
				{
					//ClsDb.DBTransactionRollback();
				}
			}
			catch(Exception ex)
			{
				test.Text = ex.Message.ToString();
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);				
			}
		}
		private void AddTemplates(DbTransaction trans)
		{
			try
			{
				ClsPricingPlans.TempateName = txtPlanName.Text + "_Template";
				ClsPricingPlans.EmployeeID  = Convert.ToInt32(ClsSession.GetCookie("sEmpID",this.Request));	
				ClsPricingPlans.TemplateDescription = txtPlanName.Text + "_Template";
				
				ClsPricingPlans.TemplateID = ClsPricingPlans.AddPricingTemplate(trans);
				if (ClsPricingPlans.TemplateID == 0)
				{
					ClsDb.DBTransactionRollback();
				}
				//	SqlDataReader rdr_TempID =  ClsDb.Get_All_RecordsByQuery("select max(TemplateID) as maxTempID from tblpricingtemplate");
				//	ClsPricingPlans.TemplateID = Convert.ToInt32(rdr_TempID["maxTempID"].ToString()) ;
			}
			catch(Exception ex)
			{
				test.Text = ex.Message.ToString();
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}
		private void PlanInfoInsert(DbTransaction trans)
		{
			try
			{
				  
				ClsPricingPlans.PlanDescription =  txtPlanName.Text ;
				ClsPricingPlans.PlanShortName = txt_ShortName.Text ;
				ClsPricingPlans.EffectiveDate = calDate(calFrom.SelectedDate,ddl_TimeFrom.SelectedValue);	
				
				ClsPricingPlans.EndDate  = calDate(calTo.SelectedDate,ddl_TimeTo.SelectedValue ) ;
		
				ClsPricingPlans.CourtID = Convert.ToInt32(ddl_crtloc.SelectedValue) ;
			
				if (rdo_Inactive.Checked == true)
				{
					ClsPricingPlans.ShowAll  = 0;
				}
				else if (rdo_Active.Checked == true)
				{
					ClsPricingPlans.ShowAll = 1;
				}
				ClsPricingPlans.PlanID = ClsPricingPlans.AddPlanInfo(trans);
				
				if (ClsPricingPlans.PlanID == 0)
				{
					ClsDb.DBTransactionRollback();
				}
			}
			catch(Exception ex)
			{
				test.Text = ex.Message.ToString();
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}
		private void AddTemplatesDetails(DbTransaction trans)
		{
			try
			{
				
				foreach (DataGridItem  items in dg_PlanDetail.Items) 
				{
					ClsPricingPlans.CategoryID =Convert.ToInt32(((Label)items.FindControl("lbl_hidCategoryID")).Text) ;
					ClsPricingPlans.BondBase = Convert.ToDouble(((TextBox) items.FindControl("tb_bondbase")).Text);
					
					ClsPricingPlans.SecondaryPrice = Convert.ToDouble(((TextBox) items.FindControl("tb_secprice")).Text);
					ClsPricingPlans.BasePrice = Convert.ToDouble(((TextBox) items.FindControl("tb_baseprice")).Text);
					ClsPricingPlans.BasePercentage =Convert.ToDouble(((TextBox) items.FindControl("tb_basepcent")).Text);
					ClsPricingPlans.SecondaryPercentage =Convert.ToDouble(((TextBox) items.FindControl("tb_secpcent")).Text);
					
					ClsPricingPlans.BondSecondary = Convert.ToDouble(((TextBox) items.FindControl("tb_bondsec")).Text);
					ClsPricingPlans.BondBasePercentage = Convert.ToDouble(((TextBox) items.FindControl("tb_bondbasepcent")).Text);
					ClsPricingPlans.BondSecondaryPercentage = Convert.ToDouble(((TextBox) items.FindControl("tb_bondsecpcent")).Text);
					ClsPricingPlans.BondAssumtion = Convert.ToDouble(((TextBox) items.FindControl("tb_bondassump")).Text);
					ClsPricingPlans.FineAssumption = Convert.ToDouble(((TextBox) items.FindControl("tb_fineassump")).Text);
					ClsPricingPlans.AddPricingTemplateDetail(trans);
				}
			}
			catch(Exception ex)
			{
				test.Text = ex.Message.ToString();
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);				
			}
		}
	}
		

}
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;
using System.Data.SqlClient;

namespace HTP.backroom
{
    public partial class ValidationEmail : System.Web.UI.Page
    {
        clsLogger clog = new clsLogger();
        ValidationReports VReport = new ValidationReports();

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                btn_email.Attributes.Add("onclick", "return ValidationEmail();");
                ViewState["ValidationEmailOthers"] = ConfigurationSettings.AppSettings["ValidationEmailOthers"].ToString();
            }
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            try
            {
                //Clear Messages 
                lbl_message.Text = "";
                lbl_result.Text = "";

                string email = "";

                if (rb_demail.Checked == false)
                    email = tb_emailaddress.Text;

                //Yasir Kamal 5460 02/17/2009 Email Alert/Report Summary 
                //Send Validation Report To The User
                VReport.GetValidationReport(email, false, chkDetail.Checked);
                //end 5460 
                lbl_result.Text = "Validation email sent, you will receive it shortly.";    //Saeed 6255 07/14/2010 display message after emailing validation report.

            }

            catch (SqlException ex)
            {
                lbl_message.Text = ex.Message.ToString();
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_result.Text = "Error In Procedure USP_HTS_ValidationEmailReportHouston";
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message.ToString();
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_result.Text = "Error In Procedure USP_HTS_ValidationEmailReportHouston";
            }
        }

        protected void btn_displayreport_Click(object sender, EventArgs e)
        {
            try
            {
                //Clear Messages 
                lbl_message.Text = "";
                lbl_result.Text = "";
                //checked by khalid for bug 2260 2-1-08
                //Yasir Kamal 5460 03/03/2009 Display Alert/Report Summary 
                string ReportContent = VReport.GetValidationReport("", true, chkDetail.Checked);
                //5460 end
                //Display Report If Content Exist
                if (ReportContent != "")
                {
                    pnl_report.Controls.Add(new LiteralControl(ReportContent));
                    pnl_report.Visible = true;
                }
                else
                {
                    pnl_report.Visible = false;
                }

                tdWait.Style["display"] = "none";
                tdReport.Style["display"] = "inline";   //Saeed 6255 07/06/2010 display summary report

            }
            catch (Exception ex)
            {
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_message.Text = ex.Message.ToString();
            }
        }

        #endregion
    }
}

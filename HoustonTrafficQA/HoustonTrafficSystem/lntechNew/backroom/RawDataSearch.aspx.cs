using System;
using System.Data;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using HTP.Components;
using System.Collections.Generic;

namespace HTP.backroom
{
    /// <summary>
    /// RawDataSearch class file.
    /// </summary>
    public partial class RawDataSearch : System.Web.UI.Page
    {

        clsENationWebComponents clsDB = new clsENationWebComponents("LoaderFilesArchive");
        private DataTable dtInfo;
        ClsRawDataSearch ObjRawDataSearch = new ClsRawDataSearch();

        /// <summary>
        /// Page load event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
            //Saeed 8127 08/06/2010 populate loader's dropdown, and initally display default criteria's on interface.
            if (!IsPostBack)
            {
                FillDropDown();
                ScriptManager.RegisterStartupScript(this, typeof(Page), "CallOnLoad", "ResetControls();", true);
            }
            //Saeed 8127 08/06/2010 END
        }

        /// <summary>
        /// Button click event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_Submit_Click(object sender, EventArgs e)
        {
            try
            {
                gv_Details.PageIndex = 0;
                GetData();

                if (gv_Details.Rows.Count < 4)
                {
                    divgrid.Style.Add(HtmlTextWriterStyle.Height, "300px");
                }
                else
                {
                    divgrid.Style.Add(HtmlTextWriterStyle.Height, "600px");
                }

            }

            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }
        //Saeed 8127 08/06/2010 method created.
        /// <summary>
        /// Fill dropdown with Loader's list. Also sort the item list in ascending order.
        /// </summary>
        private void FillDropDown()
        {
            List<ListItem> itemList = new List<ListItem>();
            ListItem loaderList = new ListItem("HMC-Arraignment", "2");
            itemList.Add(loaderList);
            loaderList = new ListItem("HMC-Disposed", "7");
            itemList.Add(loaderList);
            loaderList = new ListItem("HMC-DLQ", "3");
            itemList.Add(loaderList);
            loaderList = new ListItem("HMC-Events", "4");
            itemList.Add(loaderList);
            loaderList = new ListItem("HMC-Warrant", "5");
            itemList.Add(loaderList);
            loaderList = new ListItem("SMC-Arraignment", "6");
            itemList.Add(loaderList);
            loaderList = new ListItem("FW-Arraignment", "8");
            itemList.Add(loaderList);
            loaderList = new ListItem("DMC-Arraignment", "9");
            itemList.Add(loaderList);
            loaderList = new ListItem("DMC-Bonds", "10");
            itemList.Add(loaderList);
            loaderList = new ListItem("DCJP", "12");
            itemList.Add(loaderList);
            loaderList = new ListItem("DCCC-PTH", "13");
            itemList.Add(loaderList);
            loaderList = new ListItem("DCCC-BI", "14");
            itemList.Add(loaderList);
            loaderList = new ListItem("Irving-Arraignment", "15");
            itemList.Add(loaderList);
            loaderList = new ListItem("Irving-Bonds", "16");
            itemList.Add(loaderList);
            loaderList = new ListItem("HCJP-Entered", "17");
            itemList.Add(loaderList);
            loaderList = new ListItem("HCJP-Events", "18");
            itemList.Add(loaderList);
            loaderList = new ListItem("HCJP-Disposed", "19");
            itemList.Add(loaderList);
            loaderList = new ListItem("HCCC", "20");
            itemList.Add(loaderList);
            loaderList = new ListItem("PMC-Arraignment", "21");
            itemList.Add(loaderList);
            loaderList = new ListItem("PMC-Warrant", "22");
            itemList.Add(loaderList);
            loaderList = new ListItem("Tickler Extract", "23");
            itemList.Add(loaderList);
            loaderList = new ListItem("Dallas Events", "24");
            itemList.Add(loaderList);
            loaderList = new ListItem("AMC-Arraignment", "25");
            itemList.Add(loaderList);
            loaderList = new ListItem("AMC-Warrant", "26");
            itemList.Add(loaderList);
            loaderList = new ListItem("DCCC-CBF", "27");
            itemList.Add(loaderList);
            loaderList = new ListItem("DCCC-BN", "28");
            itemList.Add(loaderList);
            loaderList = new ListItem("Criminal-Montgomery", "29");
            itemList.Add(loaderList);
            loaderList = new ListItem("FW-Warrant", "30");
            itemList.Add(loaderList);
            loaderList = new ListItem("GrandPrairie-ARR", "31");
            itemList.Add(loaderList);
            loaderList = new ListItem("GrandPrairie-Warrant", "32");
            itemList.Add(loaderList);
            loaderList = new ListItem("Plano-Arraignment", "33");
            itemList.Add(loaderList);
            loaderList = new ListItem("Plano-Warrant", "34");
            itemList.Add(loaderList);
            loaderList = new ListItem("Stafford-Arraignment", "35");
            itemList.Add(loaderList);
            loaderList = new ListItem("Stafford-Warrant", "36");
            itemList.Add(loaderList);
            loaderList = new ListItem("SugarLand-DL", "37");
            itemList.Add(loaderList);
            loaderList = new ListItem("SugarLand-Dispose", "38");
            itemList.Add(loaderList);
            loaderList = new ListItem("SugarLand-Events", "39");
            itemList.Add(loaderList);
            loaderList = new ListItem("FW-DLQ", "40");
            itemList.Add(loaderList);
            loaderList = new ListItem("Stafford-Event", "41"); // Adil 8349 10/18/2010 Stafford Events added.
            itemList.Add(loaderList);
            loaderList = new ListItem("PMC-Events", "42"); // Kashif Jawed 8981 03/18/2011 PMC Events added.
            itemList.Add(loaderList);

            loaderList = new ListItem("SoHo-Entered", "43"); // Muhammad Nadir Siddiqui 9303 05/24/2011 SOHO-Entered Loader added.
            itemList.Add(loaderList);

            loaderList = new ListItem("SoHo-Disposed", "44"); // Muhammad Nadir Siddiqui 9303 05/24/2011 SOHO-Disposed Loader added.
            itemList.Add(loaderList);

            loaderList = new ListItem("SoHo-Events", "45"); // Muhammad Nadir Siddiqui 9303 05/24/2011 SOHO-Events Loader added.
            itemList.Add(loaderList);

            loaderList = new ListItem("SoHo-ARR", "46"); // Adil 9303 06/06/2011 SOHO-ARR Loader added.
            itemList.Add(loaderList);

            loaderList = new ListItem("SMC-Appearence", "47"); // Farrukh 9618 08/22/2011 SMC-APP Loader added.
            itemList.Add(loaderList);

            loaderList = new ListItem("JVMC-ARR", "48"); // Adil 9698 09/29/2011 JVMC-ARR Loader added.
            itemList.Add(loaderList);

            loaderList = new ListItem("JVMC-ARR-Ext", "49"); // Adil 9715 10/07/2011 JVMC-ARR-Ext Loader added.
            itemList.Add(loaderList);

            loaderList = new ListItem("Business-Startup", "50"); // Adil 9664 11/03/2011 Harris Business Startup Loader added.
            itemList.Add(loaderList);

            loaderList = new ListItem("TCCC-Book-In", "61"); // Zeeshan 10304 07/06/2012 Tarrant County Criminal Court [Booked In] Loader added.
            itemList.Add(loaderList);

            loaderList = new ListItem("TCCC-Bond-Out", "62"); // Zeeshan 10304 07/06/2012 Tarrant County Criminal Court Loader [Bond out] Loader added.
            itemList.Add(loaderList);

            loaderList = new ListItem("TCCC-First-Setting", "63"); // Zeeshan 10304 07/06/2012 Tarrant County Criminal Court Loader [First setting] Loader added.
            itemList.Add(loaderList);

            loaderList = new ListItem("TCCC-Pass-to-Hire", "64"); // Zeeshan 10304 07/06/2012 Tarrant County Criminal Court Loader [Pass to hire] Loader added.
            itemList.Add(loaderList);

            loaderList = new ListItem("Missouri Arraignment", "65"); // Rab Nawaz Khan 10401 08/15/2012 Missouri Arraignment Loader Added. . . 
            itemList.Add(loaderList);

            loaderList = new ListItem("Baytown Arraignment", "66"); // Rab Nawaz Khan 10436 09/15/2012 Baytown Arraignment Loader Added. . . 
            itemList.Add(loaderList);

            // Farrukh 10438 11/16/2012 HCCC Criminal Daily, Monthly, JIMS009, JIMS203, Monthly Disposed Loaders Added. . . 
            loaderList = new ListItem("HCCC-Filing Daily", "69"); 
            itemList.Add(loaderList);

            loaderList = new ListItem("HCCC-Filing Monthly", "70"); 
            itemList.Add(loaderList);

            loaderList = new ListItem("HCCC-JIM009", "71"); 
            itemList.Add(loaderList);

            loaderList = new ListItem("HCCC-JIM203", "72"); 
            itemList.Add(loaderList);

            loaderList = new ListItem("HCCC-Disp-Monthly", "73"); 
            itemList.Add(loaderList);

            // Farrukh 10447 12/19/2012 Texas Dept of Transportation Loaders Added. . . 
            loaderList = new ListItem("Crash-Data-TDT", "67");
            itemList.Add(loaderList);

            loaderList = new ListItem("Person-Data-TDT", "68");
            itemList.Add(loaderList);

            // Zeeshan 10595 01/07/2013 Austin Municipal Loader
            loaderList = new ListItem("Austin-Loader", "74");
            itemList.Add(loaderList);

            loaderList = new ListItem("NISI Loader", "75"); // Rab Nawaz Khan 10729 03/21/2013 NISI Cases Loader Added. . . 
            itemList.Add(loaderList);

            loaderList = new ListItem("HMC Docket Extract", "76"); // Rab Nawaz Khan 10729 03/26/2013 Houston Docket Extract Loader Added. . . 
            itemList.Add(loaderList);

            // Farrukh 11309 08/07/2013 Unit, Charges, Endorsements, Damages, Driver Data Texas Dept of Transportation Added. . .
            loaderList = new ListItem("Unit-Data-TDT", "77");
            itemList.Add(loaderList);

            loaderList = new ListItem("Charges-Data-TDT", "78");
            itemList.Add(loaderList);

            loaderList = new ListItem("Endorsements-Data-TDT", "79");
            itemList.Add(loaderList);

            loaderList = new ListItem("Damages-Data-TDT", "81");
            itemList.Add(loaderList);

            loaderList = new ListItem("Driver-Data-TDT", "83");
            itemList.Add(loaderList);

            loaderList = new ListItem("Pearland-loader", "84"); // Rab Nawaz Khan 11108 03/06/2013 Pearland Municipal Loader Added. . . 
            itemList.Add(loaderList);

            loaderList = new ListItem("HCCC�Today's Filings", "85"); // Rab Nawaz Khan 11277 09/20/2013 HCCC Scraper Loader Added. . . 
            itemList.Add(loaderList);

            SortedList sortedList = new SortedList();
            foreach (var each in itemList)
                sortedList.Add(each.Text, each.Value);

            ddlLoader.DataSource = sortedList;
            ddlLoader.DataValueField = "Value";
            ddlLoader.DataTextField = "Key";
            ddlLoader.DataBind();
        }

        private void GetData()
        {
            ViewState["LoaderId"] = Convert.ToInt32(ddlLoader.SelectedValue);
            int LoaderId = Convert.ToInt32(ddlLoader.SelectedValue);

            
            switch (LoaderId)
            {
                case 1:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdBlank.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    break;

                case 2:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdBlank.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    break;

                case 3:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdBlank.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    break;

                case 4:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdBlank.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    break;

                case 5:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "none");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdBlank.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    break;

                // Noufil 5335 12/19/2008 New report added for sugarland
                case 6:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdBlank.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    break;

                // Adil 5374 1/1/2009 HMC Dispossed cases raw data search
                case 7:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "none");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdBlank.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    break;

                // Fahad 5581 02/27/2009 Fort Worth Arraignment cases raw data search
                case 8:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "none");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_MiddleName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_txtMiddleName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdBlank.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    break;

                // Fahad 5835 05/21/2009 DMC-Arraignment cases of raw data search added
                case 9:
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "none");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_MiddleName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_txtMiddleName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdBlank.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    break;

                // Fahad 5835 05/21/2009 DMC-Bonds cases of raw data search added
                case 10:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "none");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_MiddleName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_txtMiddleName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdBlank.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    break;

                // Fahad 5835 05/21/2009 DCJP cases of raw data search added
                case 12:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "none");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_MiddleName.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_txtMiddleName.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "None");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "None");
                    tdBlank.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    break;

                // Fahad 5835 05/21/2009 DCCC-PTH cases of raw data search added
                case 13:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_MiddleName.Style.Add(HtmlTextWriterStyle.Display, "None");
                    td_txtMiddleName.Style.Add(HtmlTextWriterStyle.Display, "None");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdBlank.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    break;

                // Fahad 5835 05/21/2009 DCCC-BI cases of raw data search added
                case 14:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "none");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_MiddleName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_txtMiddleName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdBlank.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    break;

                // Fahad 5835 05/21/2009 Irving-Arraignmnet cases of raw data search added
                case 15:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "none");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_MiddleName.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_txtMiddleName.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdBlank.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    break;

                // Fahad 5835 05/21/2009 IrvingBonds cases of raw data search added
                case 16:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "none");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_MiddleName.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_txtMiddleName.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdBlank.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    break;

                // Fahad 5835 05/21/2009 Event,Disposed and Entered cases of raw data search added
                case 17:
                case 18:
                case 19:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_MiddleName.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_txtMiddleName.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "block");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdBlank.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    break;

                // Fahad 5835 05/21/2009 HarrisCountyCriminalCourt cases of raw data search added
                case 20:
                case 69: // Farrukh 10438 11/16/2012 HCCC Criminal Daily, Monthly, Disposed Loaders Added
                case 70:
                case 73:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "none");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_MiddleName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_txtMiddleName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdBlank.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    break;
                case 85: // Rab Nawaz 11277 09/20/2013 HCCC Scraper Loaders Added. . . 
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "none");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_MiddleName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_txtMiddleName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdBlank.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    break;

                // Fahad 5835 05/21/2009 Pasadena Arraignmnet cases of raw data search added
                case 21:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "block");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "block");
                    td_MiddleName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_txtMiddleName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdBlank.Style.Add(HtmlTextWriterStyle.Display, "None");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    break;

                // Fahad 5835 05/21/2009 PasadenaWarrant cases of raw data search added
                case 22:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "block");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "block");
                    td_MiddleName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_txtMiddleName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "block");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdBlank.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    break;

                //Sabir Khan 5963 06/02/2009 Tickler Extract loader...
                //----------------------------------------------------
                case 23:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    trName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "none");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdBlank.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    break;
                //----------------------------------------------------
                // Fahad 6061 06/17/2009 Dallas Events cases of raw data search added
                case 24:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "block");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "block");
                    td_MiddleName.Style.Add(HtmlTextWriterStyle.Display, "block");
                    td_txtMiddleName.Style.Add(HtmlTextWriterStyle.Display, "block");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "block");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdBlank.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    break;
                // Adil 6592 10/09/2009 Arlington Arraignment cases of raw data search added
                case 25:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "none");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_MiddleName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_txtMiddleName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdBlank.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "None");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "None");
                    break;
                // Adil 6592 10/09/2009 Arlington Warrant cases of raw data search added
                case 26:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "none");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_MiddleName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_txtMiddleName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdBlank.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "None");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "None");
                    break;
                // Adil 6848 10/27/2009 DCCC CBF raw data search added
                case 27:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_MiddleName.Style.Add(HtmlTextWriterStyle.Display, "None");
                    td_txtMiddleName.Style.Add(HtmlTextWriterStyle.Display, "None");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdBlank.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    break;
                // Adil 6850 11/04/2009 DCCC BN raw data search added
                case 28:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "none");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_MiddleName.Style.Add(HtmlTextWriterStyle.Display, "None");
                    td_txtMiddleName.Style.Add(HtmlTextWriterStyle.Display, "None");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdBlank.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    break;
                //Waqas 6791 11/18/2009 Montgomery
                case 29:
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    trName.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_MiddleName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_txtMiddleName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdBlank.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    break;
                // Adil 7105 12/14/2009 Fort Worth Warrant raw data search added
                case 30:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_MiddleName.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_txtMiddleName.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    break;
                // Adil 7205 01/05/2010 Grand Prairie arraignment raw data search added
                case 31:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_MiddleName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_txtMiddleName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    break;
                // Adil 7235 01/20/2010 Grand Prairie warrant raw data search added
                case 32:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_MiddleName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_txtMiddleName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    break;
                // Adil 7353 01/29/2010 Plano Arraignment raw data search added
                case 33:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_MiddleName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_txtMiddleName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    break;
                // Adil 7362 02/09/2010 Plano Warrant raw data search added
                case 34:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_MiddleName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_txtMiddleName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    break;
                // Adil 7364 02/17/2010 Stafford Arraignment raw data search added
                case 35:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_MiddleName.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_txtMiddleName.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    break;
                // Adil 7420 02/22/2010 Stafford Warrant raw data search added
                case 36:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_MiddleName.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_txtMiddleName.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    break;
                // Adil 7988 07/22/2010 SugarLand Events raw data search added
                case 37:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_MiddleName.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_txtMiddleName.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    break;
                // Adil 7989 07/22/2010 SugarLand Events raw data search added
                case 38:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_MiddleName.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_txtMiddleName.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    break;
                // Adil 7990 07/22/2010 SugarLand Events raw data search added
                case 39:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_MiddleName.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_txtMiddleName.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    break;
                // Adil 8117 08/24/2010 Fort Worth DLQ raw data search added
                case 40:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "None");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "None");
                    td_MiddleName.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_txtMiddleName.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "None");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "None");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    break;
                // Adil 8349 10/18/2010 Stafford Events raw data search added
                case 41:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_MiddleName.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_txtMiddleName.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    break;
                // Kashif Jawed 8981 03/18/2011 PMC Events raw data search added
                case 42:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_MiddleName.Style.Add(HtmlTextWriterStyle.Display, "None");
                    td_txtMiddleName.Style.Add(HtmlTextWriterStyle.Display, "None");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    break;
                // Muhammad Nadir Siddiqui  9303 05/24/2011 SOHO-Entered Loader raw data search added
                case 43:
                case 44:
                case 45:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_MiddleName.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_txtMiddleName.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    break;
                // Adil 9303 06/06/2011 SOHO-ARR Loader raw data search added
                case 46:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdBlank.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    break;
                // Farrukh 9618 08/22/2011 SMC-APP Loader raw data search added
                case 47:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "block");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdBlank.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    break;
                // Adil 9698 09/29/2011 Jersey Village ARR Loader raw data search added
                case 48:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdBlank.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    break;
                // Adil 9715 10/07/2011 Jersey Village ARR Extended Loader raw data search added
                case 49:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdBlank.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    break;
                // Adil 9664 11/03/2011 Harris Business Startup Loader raw data search added
                case 50:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdBlank.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    td_MiddleName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_txtMiddleName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    rb_CauseNumber.Text = "File Number";
                    rb_LastName_DOB.Text = "Business Name";
                    rb_CrtDate.Text = "Reg Date";
                    lbl_FirstName.Text = "Owner Name";
                    break;

                // Zeeshan Haider 10304 08/03/2012 Tarrant County Criminal Court [Booked In] Loader raw data search added.
                case 61:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "none");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdBlank.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    td_MiddleName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_txtMiddleName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    break;

                // Zeeshan Haider 10304 08/03/2012 Tarrant County Criminal Court Loader [Bond out] Loader raw data search added.

                case 62:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "none");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdBlank.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    td_MiddleName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_txtMiddleName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    break;

                // Zeeshan Haider 10304 08/03/2012 Tarrant County Criminal Court Loader [First setting] Loader raw data search added.

                case 63:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "block");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "block");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdBlank.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_MiddleName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_txtMiddleName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    break;

                // Zeeshan Haider 10304 08/03/2012 Tarrant County Criminal Court Loader [Pass to hire] Loader raw data search added.

                case 64:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "block");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "block");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdBlank.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_MiddleName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_txtMiddleName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    break;
                // Rab Nawaz Khan 10401 08/15/2012 Missouri Arraignment Loader Added. . . 
                case 65:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "block");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdBlank.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    break;
                // Farrukh 10438 11/16/2012 HCCC Criminal JIMS009 & JIMS203 Loaders Added 
                case 71:
                case 72:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "");
                    td_MiddleName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_txtMiddleName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdBlank.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    break;
                // Farrukh 10447 12/19/2012 Case added for Crash Data Texas Dept of Transportation
                case 67:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "none");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_MiddleName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_txtMiddleName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdBlank.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdrbCrashId.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtCrashId.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbCaseId.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtCaseId.Style.Add(HtmlTextWriterStyle.Display, "block");
                    break;
                //Farrukh 10447 12/19/2012 Case added for 
                //Farrukh 11309 08/07/2013 Case added for 
                case 68: //Person Data Texas Dept of Transportation
                case 77: //Unit Data Texas Dept of Transportation
                case 78: //Charges Data Texas Dept of Transportation
                case 79: //CDL Endorsements Data Texas Dept of Transportation
                case 81: //Damages Data Texas Dept of Transportation
                case 83: //Driver Data Texas Dept of Transportation
                    trName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "none");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_MiddleName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_txtMiddleName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdBlank.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdrbCrashId.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtCrashId.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbCaseId.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdtxtCaseId.Style.Add(HtmlTextWriterStyle.Display, "none");
                    break;

                // Zeeshan 10595 01/07/2013 Austin Municipal Loader
                case 74:
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "block");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "block");
                    trName.Style.Add(HtmlTextWriterStyle.Display, "block");
                    td_MiddleName.Style.Add(HtmlTextWriterStyle.Display, "block");
                    td_txtMiddleName.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdBlank.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdrbCrashId.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdtxtCrashId.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdrbCaseId.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdtxtCaseId.Style.Add(HtmlTextWriterStyle.Display, "none");
                    break;

                // Rab Nawaz 10729 03/21/2013 Added NISI Cases Loader. . . 
                case 75:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "none");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_MiddleName.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_txtMiddleName.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "Block");
                break;
                // Rab Nawaz 10729 03/21/2013 Added Houston Docket Extract Loader. . . 
                case 76:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    trName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "none");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdBlank.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                break;
                // Rab Nawaz 11108 08/06/2013 Added Pearland Municipal Loader. . . 
                case 84:
                    trName.Style.Add(HtmlTextWriterStyle.Display, "");
                    tdrbCrtDate.Style.Add(HtmlTextWriterStyle.Display, "");
                    txt_CrtDate.Style.Add(HtmlTextWriterStyle.Display, "");
                    td_MiddleName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_txtMiddleName.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOB.Style.Add(HtmlTextWriterStyle.Display, "none");
                    td_DOBname.Style.Add(HtmlTextWriterStyle.Display, "none");
                    tdBlank.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbTicktNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtTicketNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdrbCauseNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                    tdtxtCauseNo.Style.Add(HtmlTextWriterStyle.Display, "block");
                break;
            }
            dtInfo = this.GetRecord(LoaderId);
            if (dtInfo.Rows.Count == 0)
            {

                divPaging.Visible = false;
                gv_Details.Visible = btnExport.Visible = false;
                lblMessage.Text = "No Record found !";
            }
            else
            {

                lblMessage.Text = "";
                gv_Details.Visible = btnExport.Visible = true;
                gv_Details.DataSource = dtInfo;
                gv_Details.DataBind();


                Pagingctrl.PageCount = gv_Details.PageCount;
                Pagingctrl.PageIndex = gv_Details.PageIndex;
                Pagingctrl.SetPageIndex();

                if (gv_Details.PageCount > 1)
                {
                    divPaging.Visible = true;
                    if (gv_Details.PageIndex != 0)
                    {
                        lbPrev.Enabled = true;
                    }
                    else
                    {
                        lbPrev.Enabled = false;
                    }

                    if (gv_Details.PageIndex != (gv_Details.PageCount - 1))
                    {
                        lbNext.Enabled = true;
                    }
                    else
                    {
                        lbNext.Enabled = false;
                    }
                }
                else
                {
                    divPaging.Visible = false;
                }

            }
        }


        void Pagingctrl_PageIndexChanged()
        {
            gv_Details.PageIndex = Pagingctrl.PageIndex - 1;
            GetData();
        }
        //Fahad 5835 05/29/2009
        /// <summary>
        /// This Method sets the LoaderID and ModeId to get the data according to LoaderId ModeID
        /// </summary>
        /// <param name="Loader"></param>
        /// <returns></returns>
        private DataTable GetRecord(int Loader)
        {
            DataTable dt = new DataTable();
            ObjRawDataSearch.LoaderId = Loader;
            if (rb_TicketNumber.Checked == true)
            {
                ObjRawDataSearch.Mode = 1;
                ObjRawDataSearch.TicketNumber = txt_TicketNumber.Text.Trim();
                dt = ObjRawDataSearch.GetData(ObjRawDataSearch.LoaderId, ObjRawDataSearch.Mode);
            }
            else if (rb_CauseNumber.Checked == true)
            {
                ObjRawDataSearch.Mode = 2;
                ObjRawDataSearch.CauseNumber = txt_CauseNumber.Text.Trim();
                dt = ObjRawDataSearch.GetData(ObjRawDataSearch.LoaderId, ObjRawDataSearch.Mode);
            }
            else if (rb_LastName_DOB.Checked == true)
            {
                ObjRawDataSearch.Mode = 3;
                ObjRawDataSearch.lastname = txt_LastName.Text.Trim();
                ObjRawDataSearch.firstname = txt_FirstName.Text.Trim();
                ObjRawDataSearch.middlename = txt_MiddleName.Text.Trim();
                if (txt_DOB.Text != "") ObjRawDataSearch.DateOfBirth = DateTime.Parse(txt_DOB.Text.Trim());
                dt = ObjRawDataSearch.GetData(ObjRawDataSearch.LoaderId, ObjRawDataSearch.Mode);
            }

            else if (rbFileDate.Checked == true)
            {
                ObjRawDataSearch.Mode = 4;
                ObjRawDataSearch.CourtDate = DateTime.Parse(txtFileDate.Text.Trim());
                dt = ObjRawDataSearch.GetData(ObjRawDataSearch.LoaderId, ObjRawDataSearch.Mode);
            }
            else if (rb_CrtDate.Checked == true)
            {
                ObjRawDataSearch.Mode = 5;
                ObjRawDataSearch.CourtDate = DateTime.Parse(txt_CrtDate.Text.Trim());
                dt = ObjRawDataSearch.GetData(ObjRawDataSearch.LoaderId, ObjRawDataSearch.Mode);
            }
            //Farrukh 10447 12/19/2012 Added new check for Texas Dept of Transportation Loaders
            else if (rb_CrashId.Checked)
            {
                ObjRawDataSearch.Mode = 1;
                ObjRawDataSearch.TicketNumber = txt_CrashId.Text.Trim();
                dt = ObjRawDataSearch.GetData(ObjRawDataSearch.LoaderId, ObjRawDataSearch.Mode);
            }
            //Farrukh 10447 12/19/2012 Added new check for Texas Dept of Transportation Loaders
            else if (rb_CaseId.Checked)
            {
                ObjRawDataSearch.Mode = 3;
                ObjRawDataSearch.CauseNumber = txt_CaseId.Text.Trim();
                dt = ObjRawDataSearch.GetData(ObjRawDataSearch.LoaderId, ObjRawDataSearch.Mode);
            }
            return dt;
        }
        /// <summary>
        /// Export to excel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnExport_Click(object sender, EventArgs e)
        {

            if (ViewState["LoaderId"] != null)
            {
                DataTable dt = this.GetRecord(Convert.ToInt32(ViewState["LoaderId"]));
                RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
                objExport.ExportDetails(dt, RKLib.ExportData.Export.ExportFormat.Excel, "RawDataSearch.xls");
            }
        }
        /// <summary>
        /// Gridview page index change event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_Details_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            if (e.NewPageIndex != -1)
            {
                gv_Details.PageIndex = e.NewPageIndex;
                GetData();
            }
        }
        /// <summary>
        /// Display next page records.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lbNext_Click(object sender, EventArgs e)
        {
            if (gv_Details.PageIndex < (gv_Details.PageCount - 1))
            {
                gv_Details.PageIndex += 1;
                GetData();
            }
        }
        /// <summary>
        /// Display previous page records. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lbPrev_Click(object sender, EventArgs e)
        {
            if (gv_Details.PageIndex > 0)
            {
                gv_Details.PageIndex -= 1;
                GetData();
            }
        }


    }
}
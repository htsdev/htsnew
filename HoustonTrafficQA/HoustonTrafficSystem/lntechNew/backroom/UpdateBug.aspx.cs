using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components;
using System.IO;
using lntechNew.Components.ClientInfo;
using System.Web.Mail;

namespace lntechNew.backroom
{
    /// <summary>
    /// Added By Fahad All Code has been moved to the class 12/13/2007
    /// Restrict File Uploading 
    /// Restrict File Size
    /// Data list control issues has been resolved 
    /// Now comments & bug description will be seperated 
    /// Email Format issues has been fixed 
    /// END Fahad 12/15/2007
    /// </summary>
    /// </summary>
    public partial class UpdateBug : System.Web.UI.Page
    {
        #region Controls

        protected System.Web.UI.WebControls.TextBox txt_bugdate;

        #endregion

        #region Variables

        clsENationWebComponents ClsDb = new clsENationWebComponents("TroubleTicketDB");
        Bug Update_Bug = new Bug();
        Attachment UpdateAttachment = new Attachment();
        clsSession cSession = new clsSession();
        clsLogger BugTracker = new clsLogger();
        string bugid = "";
        int attachid = 0;
        string Developerid = string.Empty;
        string predev = String.Empty;

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dg_attachment.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_attachment_ItemCommand);
            this.dg_attachment.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dg_attachment_ItemDataBound);
        }
        #endregion

        #region Events

        protected void Page_Load(object sender, System.EventArgs e)
        {
            if (cSession.IsValidSession(this.Request) == false)
            {
                Response.Redirect("../frmlogin.aspx", false);
            }

            else
            {
                lblMessage.Text = "";
                if (!IsPostBack)
                {
                    if (Request.QueryString["Bugid"] != null)
                    {
                        string Bugid = Request.QueryString["Bugid"].ToString();
                        bugid = Request.QueryString["Bugid"].ToString();
                        int Empid = Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request));
                        ViewState["Empid"] = Empid;
                        ViewState["BugID"] = Bugid;
                        lblMessage.Text = "";
                        lblMessage.Visible = true;
                        BindControls();
                        DisplayBugs(Bugid);
                        BindGrid(Bugid);
                        DisplayComments(Bugid);
                        ViewState["vNTPATHTroubleTicketUploads"] = System.Configuration.ConfigurationSettings.AppSettings["NTPATHTroubleTicketUploads"].ToString();
                        ViewState["fileextension"] = System.Configuration.ConfigurationSettings.AppSettings["FileExtension"].ToString();
                        ViewState["vNTPATH"] = System.Configuration.ConfigurationSettings.AppSettings["NTPATH"].ToString();
                        ViewState["FileSize"] = System.Configuration.ConfigurationSettings.AppSettings["FileSize"].ToString();
                        hpcomments.Attributes.Add("onclick", "OpenPop('AddComments.aspx?Bugid=" + ViewState["BugID"].ToString() + "&Empid=" + Empid + "&Flag=1" + "');");
                      
                    }
                }
            }
        }

        protected void btnSubmit_Click(object sender, System.EventArgs e)
        {
            Update();
        }

        private void dg_attachment_ItemDataBound(object source, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            try
            {

                if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer)
                {

                    ((LinkButton)e.Item.FindControl("lnkView")).Attributes.Add("onClick", "return Open("  + ((Label)e.Item.FindControl("lblattachmentid")).Text + ");");

                }

                
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }
        }

        private void dg_attachment_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "View")
                {
                    string FileName = ((Label)e.Item.FindControl("lblfilename")).Text.ToString();
                    string attachmentid = ((Label)e.Item.FindControl("lblattachmentid")).Text.ToString();

                    string vpath = ViewState["vNTPATHTroubleTicketUploads"].ToString() + ViewState["BugID"].ToString() + "_" + attachmentid + "_" + FileName;



                    string vpath1 = ViewState["vNTPATHTroubleTicketUploads"].ToString().Substring((ViewState["vNTPATH"].ToString().Length) - 1) + ViewState["BugID"].ToString() + "_" + attachmentid + "_" + FileName;


                    //if (!File.Exists(vpath))
                    //{
                    //    HttpContext.Current.Response.Write("<script> alert('The specified file has been moved or deleted from the server.'); </script>");
                    //    return;
                    //}
                    //else
                    //{
                    //    Response.Redirect("../Docstorage" + vpath1, false);
                    //}




                }
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }
        }

        protected void lnk_comments_Click(object sender, System.EventArgs e)
        {
            //HttpContext.Current.Response.Write("<script language='javascript'>function OpenPopUp(path,val){window.open(path,'','height=130,width=400,resizable=yes,status=no,scrollbar=no,menubar=no,location=no');}OpenPopUp('AddComments.aspx?Bugid=" + ViewState["BugID"].ToString() + "&Flag=1" + "');</script>");

        }

        protected void lnkBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("GeneralView.aspx");
        }

        #endregion

        #region Methods

        private void BindGrid(string Bugid)
        {
            try
            {
                string[] key ={ "@Bugid" };
                object[] value1 ={ Bugid };

                DataSet DS_Attachment = Update_Bug.BindGridUpdateBug(Convert.ToInt32(Bugid));
               
                if (DS_Attachment.Tables[0].Rows.Count > 0)
                {
                    dg_attachment.DataSource = DS_Attachment;
                    dg_attachment.DataBind();
                    TextBox1.Text = "1";//Assign Value for JavaScript.
                }

            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }



        public void DisplayComments(string Bugid)
        {
            DataSet DS = Update_Bug.DisplayCommentsForUpdateBug(Bugid, dl_comments);
            string valid = Convert.ToString(DS.Tables[0].Rows[0]["commentsdate"]);
            ViewState["title"] = Convert.ToString(DS.Tables[0].Rows[0]["title"]);
            if ((valid == null) || (valid == "") )
            {
                dl_comments.Visible = false;
                Textbox2.Text = "0";
            }
            else
            {

                Textbox2.Text = "1";
            }

        }
        private void BindControls()
        {
            Update_Bug.FillDropdown(ddl_priority, DropdownType.FillPriority);
            Update_Bug.FillDropdown(ddl_Status, DropdownType.Fillstatus);
            Update_Bug.FillDropdown(ddl_users, DropdownType.FillUser);
        }


        private void DisplayBugs(string Bugid)
        {

            try
            {

                DataSet DS_DisplayBug = Update_Bug.DisplayBugFromBugid(Convert.ToInt32(Bugid));

                if (DS_DisplayBug.Tables[0].Rows.Count > 0)
                {
                    ddl_priority.SelectedValue = DS_DisplayBug.Tables[0].Rows[0]["priorityid"].ToString();
                    ddl_Status.SelectedValue = (DS_DisplayBug.Tables[0].Rows[0]["statusid"]).ToString();
                    ViewState["status"] = (DS_DisplayBug.Tables[0].Rows[0]["statusid"]).ToString();
                    Developerid = DS_DisplayBug.Tables[0].Rows[0]["developerid"].ToString();
                    lbldesc.Text = Convert.ToString(DS_DisplayBug.Tables[0].Rows[0]["description"]);

                    if (Developerid != "")
                    {
                        Session["PreDev"] = Developerid;
                        Session["bgid"] = Convert.ToInt32(ViewState["BugID"]);
                        ddl_users.SelectedValue = DS_DisplayBug.Tables[0].Rows[0]["developerid"].ToString();

                    }


                }
                lblBugId.Text = DS_DisplayBug.Tables[0].Rows[0]["bug_id"].ToString();
                if (DS_DisplayBug.Tables[0].Rows[0]["pageurl"].ToString() != "")
                {
                    hlPageurl.Text = DS_DisplayBug.Tables[0].Rows[0]["pageurl"].ToString();
                    
                }
                else
                {
                    hlPageurl.Text = "N/A";
                }
                if (DS_DisplayBug.Tables[0].Rows[0]["ticketid"].ToString() != "")
                {
                    TicketNo.Text = DS_DisplayBug.Tables[0].Rows[0]["ticketid"].ToString();

                }
                else
                {
                    TicketNo.Text = "N/A";
                }

            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void Update()
        {
            lblMessage.Text = "";
            lblMessage.Visible = false;
            try
            {

                if (Update_Bug.UpdateBug(Convert.ToInt32(ddl_priority.SelectedValue), Convert.ToInt32(ddl_users.SelectedValue), Convert.ToInt32(ViewState["BugID"]), Convert.ToInt32(ddl_Status.SelectedValue))) ;
                {
                    lblMessage.Text = "Bug Updated Sucesfully";
                    lblMessage.Visible = true;
                }


                string dev = (string)Session["PreDev"];

                if (dev != ddl_users.SelectedValue)
                {

                    Update_Bug.SendMail(Convert.ToInt32(ddl_users.SelectedValue), ViewState["BugID"].ToString(), Convert.ToString(Request.ServerVariables["SERVER_NAME"]), ddl_priority.SelectedItem.ToString(), ddl_Status.SelectedItem.ToString(), TicketNo.Text.ToString(), hlPageurl.Text.ToString(), false, Convert.ToString(ViewState["title"]));
                }
                else
                {
                    if ((dev == ddl_users.SelectedValue) && ((Convert.ToUInt32(Session["bgid"])) != (Convert.ToUInt32(ViewState["BugID"].ToString()))))
                        Update_Bug.SendMail(Convert.ToInt32(ddl_users.SelectedValue), ViewState["BugID"].ToString(), Convert.ToString(Request.ServerVariables["SERVER_NAME"]), ddl_priority.SelectedValue.ToString(), ddl_Status.SelectedValue.ToString(), TicketNo.Text.ToString(), hlPageurl.Text.ToString(), false, Convert.ToString(ViewState["title"]));

                }
                if (ddl_Status.SelectedValue == "10" && (ddl_Status.SelectedValue != ViewState["status"].ToString()))
                {

                   Update_Bug.SendCloseTroubleTicketEmail(hlPageurl.Text, ddl_users.SelectedItem.Text.ToString(), lblBugId.Text.ToString(), ddl_priority.SelectedItem.ToString(), ddl_Status.SelectedItem.ToString(), TicketNo.Text.ToString(),hlPageurl.Text.ToString(),false);


                }


            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }


        public void UploadFile()
        {
            string filename = "";
            int temp = 0;

            try
            {
                filename = fp_file.PostedFile.FileName.ToString();
                if (filename != "")
                {

                    SaveFile(Convert.ToInt32(ViewState["BugID"]));
                    BindGrid(ViewState["BugID"].ToString());
                    temp = 1;
                }

                if (temp == 0)
                {
                    lblMessage.Text = "Please Select a file to attach ";
                    lblMessage.Visible = true;

                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = "Unable to Attach File ";
                lblMessage.Visible = true;
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

            }


        }

        public void SaveFile(int Bugid)
        {
            string FileName = "";
            int filesize = 0;
            string fName = "";
            string fileextensions = Convert.ToString(ViewState["fileextension"]).ToUpper();
            int filelimit = Convert.ToInt32(ViewState["FileSize"]);
            try
            {
                fName = fp_file.PostedFile.FileName.ToString();
                if (fName != "" && fp_file.PostedFile.ContentLength <= filelimit && (fileextensions.Contains(Path.GetExtension(fName).ToUpper())))
                {
                    filesize = fp_file.PostedFile.ContentLength;
                    if (filesize < 0)
                    {
                        lblMessage.Text = "File Uploading Failed";
                        lblMessage.Visible = true;
                    }
                    else
                    {
                        FileName = fp_file.PostedFile.FileName.Substring(fp_file.PostedFile.FileName.LastIndexOf("\\") + 1);
                        try
                        {

                            attachid = UpdateAttachment.AddNewAttachment(fp_file.PostedFile.FileName.Substring(fp_file.PostedFile.FileName.LastIndexOf("\\") + 1), txt_filedesc.Text.ToString().Trim(), fp_file.PostedFile.ContentLength, System.DateTime.Today, fp_file.PostedFile.ContentType, Convert.ToInt32(ViewState["Empid"]), Bugid);
                            fp_file.PostedFile.SaveAs(ViewState["vNTPATHTroubleTicketUploads"].ToString() + Bugid + "_" + attachid + "_" + FileName);
                            HttpContext.Current.Response.Write("<script language='javascript'>alert('File Sucessfully Uploaded');</script>");
                            txt_filedesc.Text = "";
                        }
                        catch (Exception ex)
                        {
                            UpdateAttachment.RollbackAttachment(Convert.ToInt32(attachid), Bugid);
                            HttpContext.Current.Response.Write("<script language='javascript'>alert('File Uploading Failed'); </script>");
                            lblMessage.Text = ex.Message.ToString();
                            
                        }
                    }
                }
                else
                {
                    HttpContext.Current.Response.Write("<script language='javascript'>alert('File Uploading Failed.Please Upload File Of Extensions " + fileextensions + " And File Size Must Be Less Than " + filelimit + " Bytes"+  " '); </script>");
                }
            }

            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message.ToString();
                lblMessage.Visible = true;

            }
        }

        protected void btn_upload_Click(object sender, EventArgs e)
        {
            UploadFile();
        }


        #endregion

        protected void hlPageurl_Click(object sender, EventArgs e)
        {
            Response.Redirect(hlPageurl.Text.ToString());
        }
    }
}

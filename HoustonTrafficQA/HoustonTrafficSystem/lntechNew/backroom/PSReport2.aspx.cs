using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.backroom
{
    public partial class PSReport2 : System.Web.UI.Page
    {
        clsSession ClsSession = new clsSession();
        clsLogger clog = new clsLogger();
        clsENationWebComponents ClsDB = new clsENationWebComponents("Connection String");
        int statuscount = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (ClsSession.IsValidSession(this.Request) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }

                if (!IsPostBack)
                {
                    calstartdate.SelectedDate = DateTime.Today;
                    calenddate.SelectedDate = DateTime.Today;

                    FillGrids();
                }


            }
            catch (Exception Ex)
            {
                lbl_error.Text = Ex.Message;
                clog.ErrorLog(Ex.Message, Ex.Source, Ex.TargetSite.ToString(), Ex.StackTrace);
            }
        }
        private void FillGrids()
        {
            try
            {
                string[] key ={ "@StartDate","@EndDate" };
                object[] value1 ={calstartdate.SelectedDate.ToShortDateString(),calenddate.SelectedDate.ToShortDateString()};

                DataSet DS_GetSp = ClsDB.Get_DS_BySPArr("USP_PS_Report2", key, value1);

                if (DS_GetSp.Tables[0].Rows.Count > 0)
                {
                    dgresult.DataSource = DS_GetSp;
                    dgresult.DataBind();
                }
            }
            catch (Exception Ex)
            {
                lbl_error.Text = Ex.Message;
                clog.ErrorLog(Ex.Message, Ex.Source, Ex.TargetSite.ToString(), Ex.StackTrace);
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            FillGrids();
        }

        protected void dgresult_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            try
            {
                if ((e.Item.ItemType == ListItemType.Header) || (e.Item.ItemType == ListItemType.Footer))
                {
                }
                else
                {
                    statuscount++;
                    Label status = ((Label)e.Item.FindControl("lblhirestatus"));
                    HiddenField hfstatus = ((HiddenField)e.Item.FindControl("hf_status"));
                    HiddenField hfticketid = ((HiddenField)e.Item.FindControl("hf_ticketid"));
                    HiddenField ActiveFlag = ((HiddenField)e.Item.FindControl("hf_activeflag"));


                    if (hfstatus.Value == "1")
                        status.Text = "Hired From Website";
                    else if (hfstatus.Value=="0" && ActiveFlag.Value == "1" )
                        status.Text = "Hired From Office";
                    else if (hfstatus.Value == "0" && hfticketid.Value != "0")
                        status.Text = "Quote From Office";
                       else if (hfstatus.Value=="0" && hfticketid.Value == "0"   )
                        status.Text = "Quote From Website";
                    else  
                        status.Text = "None";

                    Label lblno = ((Label)e.Item.FindControl("lbl_sno"));
                    lblno.Text = statuscount.ToString();

                 
                    if (hfticketid.Value != "0")
                    {
                        ((LinkButton)e.Item.FindControl("lnkbtn_clientname")).Visible = true;
                        ((Label)e.Item.FindControl("lbltktno")).Visible = false;
                        ((LinkButton)e.Item.FindControl("lnkbtn_clientname")).PostBackUrl = "~/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + hfticketid.Value;
                    }
                    else
                    {
                        ((LinkButton)e.Item.FindControl("lnkbtn_clientname")).Visible = false;
                        ((Label)e.Item.FindControl("lbltktno")).Visible = true;

                    }
                
                }
            }
            catch (Exception Ex)
            {
                lbl_error.Text = Ex.Message;
            }
        }
    }
}

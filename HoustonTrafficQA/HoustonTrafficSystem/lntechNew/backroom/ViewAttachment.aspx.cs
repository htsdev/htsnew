using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components;
using System.IO;
using lntechNew.Components.ClientInfo;

namespace lntechDallasNew.backroom
{
    public partial class ViewAttachment : System.Web.UI.Page
    {
        Attachment UpdateAttachment = new Attachment();

        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Request.QueryString["attachmentid"] != null))
            {
               
                string  attachmentid = Convert.ToString(Request.QueryString["attachmentid"]);
                DataSet filenamedata = UpdateAttachment.GetAttachment(attachmentid);
                string filename = Convert.ToString(filenamedata.Tables[0].Rows[0]["attachfile"]);
                string bugid = Convert.ToString(filenamedata.Tables[0].Rows[0]["bugid"]);
                ViewState["vNTPATHTroubleTicketUploads"] = System.Configuration.ConfigurationSettings.AppSettings["NTPATHTroubleTicketUploads"].ToString();
                ViewState["vNTPATH"] = System.Configuration.ConfigurationSettings.AppSettings["NTPATH"].ToString();
                string vpath = ViewState["vNTPATHTroubleTicketUploads"].ToString() + bugid + "_" + attachmentid + "_" + filename;
                string vpath1 = ViewState["vNTPATHTroubleTicketUploads"].ToString().Substring((ViewState["vNTPATH"].ToString().Length) - 1) + bugid + "_" + attachmentid + "_" + filename;


                if (!File.Exists(vpath))
                {
                    HttpContext.Current.Response.Write("<script> alert('The specified file has been moved or deleted from the server.');self.close(); </script>");
                    return;
                }
                else
                {
                    Response.Redirect("../Docstorage" + vpath1, false);
                }

            }
        }
    }
}

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components;
using System.IO;
using NUnit.Framework;
using NUnit.Extensions.Asp;
using NUnit.Extensions.Asp.AspTester;
using NUnit.Extensions.Asp.HtmlTester;
using System.Web.Mail;
using lntechNew.Components.ClientInfo;

namespace lntechNew.backroom
{
	/// <summary>
	/// Summary description for UpdateDeveloper.
	/// </summary>
    
	public partial class UpdateDeveloper : System.Web.UI.Page
	{

		#region Variables
		clsENationWebComponents ClsDb = new clsENationWebComponents();
		Bug Update_Bug=new Bug();
        lntechNew.Components.Attachment UpdateAttachment = new lntechNew.Components.Attachment();
		 string bugid="";
        clsSession cSession = new clsSession();
        clsLogger BugTracker = new clsLogger();
        //string MailServer = (string)System.Configuration.ConfigurationSettings.AppSettings["SMTPServer"];
        //string EmailFrom = (string)System.Configuration.ConfigurationSettings.AppSettings["MailFrom"];
        //string CCEmail = (string)System.Configuration.ConfigurationSettings.AppSettings["CCEmail"];
        //string userName = (string)System.Configuration.ConfigurationSettings.AppSettings["SMTPUser"];
        //string password = (string)System.Configuration.ConfigurationSettings.AppSettings["SMTPPassword"];

		#endregion
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
            if (cSession.IsValidSession(this.Request) == false)
            {
                Response.Redirect("../frmlogin.aspx", false);
            }
            else
            {
                if (!IsPostBack)
                {
                    if (Request.QueryString["Bugid"] != null)
                    {
                        string Bugid = Request.QueryString["Bugid"].ToString();
                        bugid = Request.QueryString["Bugid"].ToString();
                        ViewState["BugID"] = Bugid;
                        lblMessage.Text = "";
                        lblMessage.Visible = true;
                        BindControls();
                        DisplayBugs(Bugid);
                        BindGrid(Bugid);
                        DisplayComments(Bugid);
                        ViewState["vNTPATHTroubleTicketUploads"] = System.Configuration.ConfigurationSettings.AppSettings["NTPATHTroubleTicketUploads"].ToString();
                        ViewState["vNTPATH"] = System.Configuration.ConfigurationSettings.AppSettings["NTPATH"].ToString();
                        hpcomments.Attributes.Add("onclick", "OpenPop('AddComments.aspx?Bugid=" + ViewState["BugID"].ToString() + "&Flag=2" + "');");

                    }
                }
            }
			btnSubmit.Attributes.Add("onclick","return Validation();");
		}

		private void BindControls()
		{ 
			#region FillPriorityDropDown

			DataSet DS_Priority = ClsDb.Get_DS_BySP("usp_bug_getpriorities");
			if (DS_Priority.Tables[0].Rows.Count > 0)
			{
				ddl_priority.Items.Clear();
				for (int i = 0; i < DS_Priority.Tables[0].Rows.Count; i++)
				{
					string PriorityName = DS_Priority.Tables[0].Rows[i]["priority_name"].ToString().Trim();
					string Priorityid = DS_Priority.Tables[0].Rows[i]["priority_id"].ToString().Trim();
					ddl_priority.Items.Add(new ListItem(PriorityName, Priorityid));
				}
			}
			
			#endregion

			#region FillUsersDropDown
				DataSet DS_Users=ClsDb.Get_DS_BySP("usp_bug_getusers");
			if(DS_Users.Tables[0].Rows.Count > 0)
			{ 
				ddl_users.Items.Clear();
				ddl_users.Items.Insert(0,new ListItem("Choose","0"));
				for (int i = 0; i < DS_Users.Tables[0].Rows.Count; i++)
				{
					string UserName = DS_Users.Tables[0].Rows[i]["UserName"].ToString().Trim();
					string Userid = DS_Users.Tables[0].Rows[i]["Userid"].ToString().Trim();
					ddl_users.Items.Add(new ListItem(UserName, Userid));
				}

			}
			#endregion

            #region FillStatusDropDown

            DataSet DS_Status = ClsDb.Get_DS_BySP("usp_bug_getstatus");
            if (DS_Status.Tables[0].Rows.Count > 0)
            {
                ddl_Status.Items.Clear();
                for (int i = 0; i < DS_Status.Tables[0].Rows.Count; i++)
                {

                    string StatusName = DS_Status.Tables[0].Rows[i]["status_name"].ToString().Trim();
                    string StatusID = DS_Status.Tables[0].Rows[i]["status_id"].ToString().Trim();
                    ddl_Status.Items.Add(new ListItem(StatusName, StatusID));
                }
            }

            #endregion
			
		}

		private void BindGrid(string Bugid)
		{ 
			try
			{
				string[] key={"@Bugid"};
				object[] value1={Bugid};

				DataSet DS_Attachment=ClsDb.Get_DS_BySPArr("usp_Bug_GetAttachment",key,value1);
				if(DS_Attachment.Tables[0].Rows.Count > 0)
				{ 
					dg_attachment.DataSource=DS_Attachment;
					dg_attachment.DataBind();
					TextBox1.Text="1";//Assign Value for JavaScript.
				}
					
			}
			catch(Exception ex)
			{
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
			}
		}

		private void DisplayComments(string Bugid)
		{
			try
			{
				string[] key={"@Bugid"};
				object[] value1={Bugid};
				DataSet DS_DisplayComments=ClsDb.Get_DS_BySPArr("usp_bug_get_comments",key,value1);

				if(DS_DisplayComments.Tables[0].Rows.Count > 0)
				{ 
					dl_comments.DataSource=DS_DisplayComments;
					dl_comments.DataBind();
					Textbox2.Text="1";//Assign Value for JavaScript.
				}
			}
			catch(Exception ex)
			{
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
			}
		}

		private void Update()
		{ 
			lblMessage.Text="";
			lblMessage.Visible=false;
			
			
			try
			{

                Update_Bug.BugID = Convert.ToInt32(ViewState["BugID"]);
				Update_Bug.PriorityID=Convert.ToInt32(ddl_priority.SelectedValue);
				Update_Bug.DeveloperID=Convert.ToInt32(ddl_users.SelectedValue);
                Update_Bug.StatusID = Convert.ToInt32(ddl_Status.SelectedValue);
				Update_Bug.UpdateBug();
                lblMessage.Text = "Record Updated Succesfully";
                lblMessage.Visible = true;
                SendMail(Convert.ToInt32(ddl_users.SelectedValue));
                //SendEmail(Convert.ToInt32(ddl_users.SelectedValue));
			}
			catch(Exception ex)
			{
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
				lblMessage.Text=ex.Message;
				lblMessage.Visible=true;
			}
		}
        private void SendEmail(int Developerid)
        {
            //SmtpClient smtpClient = new SmtpClient();
            //MailMessage Message = new MailMessage();
            //Bug GetEmailAddress = new Bug();
            //DataSet DS_Email;
            //string EmailAddress = "";
            //string DeveloperName = "";
            //try
            //{
            //    DS_Email = GetEmailAddress.GetEmail(Developerid);
            //    if (DS_Email.Tables[0].Rows.Count > 0)
            //    {
            //        EmailAddress = DS_Email.Tables[0].Rows[0]["EmailAddress"].ToString();
            //        DeveloperName = DS_Email.Tables[0].Rows[0]["UserName"].ToString();

            //        MailAddress fromAdress = new MailAddress(EmailFrom,"Admin");
            //        smtpClient.Host = MailServer;
            //        Message.From = fromAdress;
            //        Message.To.Add(EmailAddress);
                    
            //        Message.Subject = "Bug on Houston Traffic Program";
            //        Message.Bcc.Add(new MailAddress(EmailAddress));
            //        Message.Bcc.Add(new MailAddress(CCEmail));
            //        Message.IsBodyHtml = false;
            //        Message.Body = DeveloperName + ", Bug Has Been Assigned to you Please visit the Houston Traffic Program to view the Bug, " + "BugID is " + ViewState["BugID"].ToString();
            //        smtpClient.Send(Message);
                    
            //    }
                
            //}
            //catch (Exception ex)
            //{
            //    lblMessage.Text = "Send Email Failed";
            //    lblMessage.Visible = true;
            //}
        }

        public void SendMail(int Developerid)
        {

            Bug GetEmailAddress = new Bug();
            DataSet DS_Email;
            string EmailAddress = "";
            string DeveloperName = "";
            string MailServer = (string)System.Configuration.ConfigurationSettings.AppSettings["SMTPServer"];
            string EmailFrom = (string)System.Configuration.ConfigurationSettings.AppSettings["MailFrom"];
            string CCEmail = (string)System.Configuration.ConfigurationSettings.AppSettings["CCEmail"];
            string userName = (string)System.Configuration.ConfigurationSettings.AppSettings["SMTPUser"];
            string password = (string)System.Configuration.ConfigurationSettings.AppSettings["SMTPPassword"];
            string bid = ViewState["BugID"].ToString();
            try
            {
                DS_Email = GetEmailAddress.GetEmail(Developerid);

                if (DS_Email.Tables[0].Rows.Count > 0)
                {
                    EmailAddress = DS_Email.Tables[0].Rows[0]["EmailAddress"].ToString();
                    DeveloperName = DS_Email.Tables[0].Rows[0]["UserName"].ToString();


                    int cdoBasic = 1;
                    int cdoSendUsingPort = 2;
                    MailMessage msg = new MailMessage();
                    if (userName.Length > 0)
                    {
                        msg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpserver", MailServer);
                        msg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpserverport", 25);
                        msg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusing", cdoSendUsingPort);
                        msg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", cdoBasic);
                        msg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", userName);
                        msg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", password);
                    }
                    msg.To = EmailAddress;
                    msg.From = EmailFrom;
                    
                    msg.Subject = "Legal Houston Bugs";
                    msg.Body = DeveloperName + ", New Houston Traffic Program Bug has been assigned to you please follow the link " + "http://errors.legalhouston.com/troubleticket/UpdateDeveloper.aspx?Bugid=" + bid;
                    //msg.BodyFormat = System.Web.Mail.MailFormat.Html;




                    SmtpMail.SmtpServer = MailServer;
                    SmtpMail.Send(msg);
                }


            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                string s = ex.Message.ToString();

            }
        }


		private void DisplayBugs(string Bugid)
		{ 
			string Developerid="";
			try
			{
				string[] key={"@Bugid"};
				object[] value1={Bugid};
				DataSet DS_DisplayBug=ClsDb.Get_DS_BySPArr("usp_bug_get_bugs",key,value1);

				if(DS_DisplayBug.Tables[0].Rows.Count > 0)
				{ 
					ddl_priority.SelectedValue= DS_DisplayBug.Tables[0].Rows[0]["priorityid"].ToString();
					Developerid=DS_DisplayBug.Tables[0].Rows[0]["developerid"].ToString();
					if(Developerid != "")
					{ 
						ddl_users.SelectedValue=DS_DisplayBug.Tables[0].Rows[0]["developerid"].ToString();
							
					}
                    lblBugId.Text = DS_DisplayBug.Tables[0].Rows[0]["bug_id"].ToString();
                    if (DS_DisplayBug.Tables[0].Rows[0]["pageurl"].ToString() != "")
                    {
                        lblPageurl.Text = DS_DisplayBug.Tables[0].Rows[0]["pageurl"].ToString();
                    }
                    if (DS_DisplayBug.Tables[0].Rows[0]["ticketid"].ToString() != "")
                    {
                        TicketNo.Text = DS_DisplayBug.Tables[0].Rows[0]["ticketid"].ToString();
                        TickNo.Visible = true;
                    }
                    else
                    { TickNo.Visible = false; }
				}

					
			}
			catch(Exception ex)
			{
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dg_attachment.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_attachment_ItemCommand);

		}
		#endregion

		protected void btnSubmit_Click(object sender, System.EventArgs e)
		{
			Update();
		}

		protected void lnk_comments_Click(object sender, System.EventArgs e)
		{
            //HttpContext.Current.Response.Write("<script language='javascript'>function OpenPopUp(path,val){window.open(path,'','height=130,width=415,resizable=yes,status=no,scrollbar=no,menubar=no,location=no');return false;}OpenPopUp('AddComments.aspx?Bugid=" + ViewState["BugID"].ToString() + "&Flag=2" + "');</script>");
            
		}

		private void dg_attachment_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			try
			{
				if(e.CommandName == "View")
				{	
					string FileName=((Label)e.Item.FindControl("lblfilename")).Text.ToString();	
					string attachmentid=((Label)e.Item.FindControl("lblattachmentid")).Text.ToString();	

					//string dpath = Request.ServerVariables["APPL_PHYSICAL_PATH"] + "uploads\\" + bugid + "_" + attachmentid + "_" + FileName;
					//string path = "uploads/" + bugid + "_" + attachmentid + "_" + FileName ;
                    string vpath = ViewState["vNTPATHTroubleTicketUploads"].ToString() + ViewState["BugID"].ToString() + "_" + attachmentid + "_" + FileName;
                    string vpath1 = ViewState["vNTPATHTroubleTicketUploads"].ToString().Substring((ViewState["vNTPATH"].ToString().Length) - 1) + ViewState["BugID"].ToString() + "_" + attachmentid + "_" + FileName;                                       

					if (!File.Exists(vpath))
					{
						HttpContext.Current.Response.Write("<script> alert('The specified file has been moved or deleted from the server.'); </script>");
						return;
					}
					else
					{
                        Response.Redirect("../Docstorage" + vpath1,false);
					}
				}
			}
			catch(Exception ex)
			{
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
				lblMessage.Text=ex.Message;
				lblMessage.Visible=true;
			}
		}

        protected void lnkBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("ViewBugs.aspx");
        }
		

		
	}
}

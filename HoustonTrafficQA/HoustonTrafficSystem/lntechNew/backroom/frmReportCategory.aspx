<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmReportCategory.aspx.cs" Inherits="lntechNew.backroom.frmReportCategory" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Report Category</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    
    <script type="text/javascript">
        function Validate()
        {
            var cat =document.getElementById("txtcategory");
            if( cat.value == "" || cat.length == 0)
            {
                alert("Please Enter Category Name");
                cat.focus();
                return false;
            }
                        
        }
        
        function DeletCategoryFunction()
        {
            var doyou;
            var cat =document.getElementById("txtcategory");
            
            if(cat.value != "" && cat.value.length >0)
            {
                doyou =confirm("Are you sure you want to delete this category ?")
                
                if(doyou ==true)
                    return true;
                 else
                    return false;    
            }
            else             
              return false;             
        }
        
        function SetCategoryName(category,catid)
        {
            var cat =document.getElementById("txtcategory");           
           cat.value = category;
           document.getElementById("hf_categoryid").value =catid;           
           return false;           
        }
    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="tblMain" cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
				<tr>
					<td>
						<!-- For Header --><uc1:activemenu id="ActiveMenu1" runat="server"></uc1:activemenu></td>
				</tr>
				<tr>
					<td>
						<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
							<TBODY>
								<TR>
									<TD style="HEIGHT: 9px" align="left" background="../../images/separator_repeat.gif"></TD>
								</TR>
								<tr> <!-- tahir   -->
									<td class="clsleftpaddingtable" style="HEIGHT: 25px" vAlign="bottom" align="center"
										width="100%"><STRONG><FONT face="Verdana" color="#0066cc">
												<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="100%" border="0">
													<TBODY>
														<TR>
															<TD class="clsleftpaddingtable" style="WIDTH: 95px" width="95"><SPAN class="style2">Category Name:</SPAN></TD>
															<TD class="clsleftpaddingtable" style="WIDTH: 126px" width="126"><asp:textbox id="txtcategory" Runat="server" CssClass="FrmTDLetter" Width="300px"></asp:textbox></TD>
															<TD class="clsleftpaddingtable" style="WIDTH: 100px" width="160"><asp:button id="btnsubmit" runat="server" CssClass="clsbutton" Text="Submit" OnClick="btnsubmit_Click"></asp:button></TD>
															<TD class="clsleftpaddingtable" style="WIDTH: 109px" align="left">&nbsp;<asp:button id="btndelete" runat="server" CssClass="clsbutton" Text="Delete" OnClick="btndelete_Click" />
                                                            </TD>
															<TD class="clsleftpaddingtable" align="right">
                                                                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/backroom/frmManageCategory.aspx">Category Manager</asp:HyperLink>&nbsp;</TD>
															<TD class="clsleftpaddingtable" align="right">&nbsp;</TD>
															<TD align="right">&nbsp;</TD>
														</TR>
													</TBODY>
												</TABLE>
											</FONT></STRONG>
										<asp:label id="lblNoRec" runat="server" CssClass="cmdlinks" Width="184px" ForeColor="Red" Visible="False"></asp:label></td>
								</tr>
								<TR>
									<TD style="HEIGHT: 14px" vAlign="bottom" align="center" width="100%" background="../../images/separator_repeat.gif"></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 34px" vAlign="middle" align="left" width="100%" background="../../Images/subhead_bg.gif"><STRONG><STRONG class="clssubhead">&nbsp;Lists 
												of Report Categories</STRONG></STRONG></TD>
								</TR>
								<TR>
									<td vAlign="top" width="100%">
                                        <asp:GridView ID="GV_Category" CssClass="clsleftpaddingtable" runat="server" Width="100%" AutoGenerateColumns="False" CellPadding="0" PageSize="20" AllowPaging="true" OnRowDataBound="GV_Category_RowDataBound">
                                            <HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle" />
                                            <FooterStyle CssClass="GrdFooter" />
                                            <Columns>                                                                                               
                                                <asp:TemplateField HeaderText="S.No">
                                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="clsaspcolumnheader" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_sno" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Category Name">   
                                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" CssClass="clsaspcolumnheader" />                                                 
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="hfcategoryname" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CategoryID") %>' />                                                        
                                                        <asp:HyperLink ID="hl_categoryname" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CategoryName") %>' NavigateUrl="#" >HyperLink</asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:GridView>
                                    </td>
								</TR>
								<TR>
									<TD align="center" width="800"><asp:label id="lblmsg" runat="server" CssClass="cmdlinks" ForeColor="Red" Visible="False" Font-Bold="True"></asp:label></TD>
								</TR>
							</TBODY>
						</TABLE>
                        <asp:HiddenField ID="hf_categoryid" runat="server" />
					</td>
				</tr>
				<tr>
					<td><uc1:footer id="Footer1" runat="server"></uc1:footer></td>
				</tr>
			</table>
    </div>
    </form>
</body>
</html>

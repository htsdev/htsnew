﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PolmReport.aspx.cs" MaintainScrollPositionOnPostback="true"
    Inherits="HTP.Backroom.PolmReport" %>

<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/UpdateFollowUpInfo.ascx" TagName="UpdateFollowUpInfo"
    TagPrefix="uc3" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>POLM Report</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />

    <script src="../Scripts/BoxOver.js"></script>

    <script src="../Scripts/ClipBoard.js"></script>

    <script src="../Scripts/Dates.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
      function HideEditPopup() 
        {
            var modalPopupBehavior = $find('ModalPopupExtender1');
            modalPopupBehavior.hide();
            return true;
        }
        
        var err = null;
		
		function CommentMsgPoupup(ControlName)
		{	
		     CursorIcon();
		     err = null;
		     err = document.getElementById(ControlName).value;
		     ShowMsg();		     
		}
		
		function ShowMsg()
        {
            document.getElementById("txtCommentMsg").value=err;
        }
        
        //change cursor icon
		function CursorIcon()
		{
		    document.body.style.cursor = 'pointer';
		}
		function CursorIcon2()
		{
		    document.body.style.cursor = 'default';
		}
		
		function CheckDateValidation()
        {
            if (IsDatesEqualOrGrater(document.form1.calstartdate.value,'MM/dd/yyyy','01/01/1900','MM/dd/yyyy')==false)
            {
			    alert("Please enter valid date, Start Date must be greater than or equal to 1/1/1900");
				return false;
			}
			
			if (IsDatesEqualOrGrater(document.form1.calenddate.value,'MM/dd/yyyy','01/01/1900','MM/dd/yyyy')==false)
            {
			    alert("Please enter valid date, End Date must be greater than or equal to 1/1/1900");
				return false;
			}
            if (IsDatesEqualOrGrater(document.form1.calenddate.value,'MM/dd/yyyy',document.form1.calstartdate.value,'MM/dd/yyyy')==false)
            {
			    alert("Please enter valid date, End Date must be greater than or equal to Start Date");
				return false;
			}
			
        }
        
        function Showhide(tdLabel, tdDropdown, tdImage,dropdown)
        { 
            if (document.getElementById(tdLabel).style.display == "")
            {
                document.getElementById(tdLabel).style.display = "none";
                document.getElementById(tdDropdown).style.display = "";
                document.getElementById(tdImage).style.display = "";
            }
            else
            {
                document.getElementById(tdLabel).style.display = "";
                document.getElementById(tdDropdown).style.display = "none";
                document.getElementById(tdImage).style.display = "";
                document.getElementById(dropdown).selectedIndex = "1";
            }
            return false;
         }
         
         function ShowStatusDropDownChange()
         {
            var casestatus = document.getElementById("ddcasestatusinner").value;
            if (casestatus == "1" || casestatus == "3" || casestatus == "4")
            {
                document.getElementById("tdfollowuptitle").style.display = "";
                document.getElementById("tdfollowupdate").style.display = "";                
                 document.getElementById("hffollowupdate").value = "1"
                
            }
            else
            {
                document.getElementById("tdfollowuptitle").style.display = "none";
                document.getElementById("tdfollowupdate").style.display = "none";                
                document.getElementById("hffollowupdate").value = "0"
            }
            return false;
         }
         
         function ValidateControl()
         {
            if (document.getElementById('td_languageLabel').style.display == "")
                ValidatorEnable(document.getElementById('<%= RequiredFieldValidator1.ClientID%>'), false); 
            if (document.getElementById('td_ContactLabel1').style.display == "")            
                ValidatorEnable(document.getElementById('<%= RequiredFieldValidator3.ClientID%>'), false);        
            if (document.getElementById('td_caseTypeLabel').style.display == "")            
                ValidatorEnable(document.getElementById('<%= rfvcasetype.ClientID%>'), false); 
                
                var divisionRow = document.getElementById('td_DivisionLabel');
            if (divisionRow != null)   
            {
                if (divisionRow.style.display == "")            
                    ValidatorEnable(document.getElementById('<%= RfvDivision.ClientID%>'), false);        
            }
            if (Page_ClientValidate())
            {
                var casestatus = document.getElementById("ddcasestatusinner").value;
                var comments = document.getElementById("txtUserComments").value;
                if (casestatus == "6" && comments.length == 0 )
                {
                       alert("Please enter closing comments.");
                       document.getElementById("txtUserComments").focus();
                       return false;
                }
                return true;
            }
            else
                return false;
         }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table width="100%">
            <tr>
                <td align="center">
                    <aspnew:ScriptManager ID="ScriptManager1" runat="server">
                        <Scripts>
                            <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
                        </Scripts>
                    </aspnew:ScriptManager>
                    <aspnew:UpdatePanel ID="pnl_main" runat="server">
                        <ContentTemplate>
                            <table width="950px">
                                <tr>
                                    <td style="height: 118px">
                                        <uc2:ActiveMenu ID="ActiveMenu1" runat="server"></uc2:ActiveMenu>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td background="../images/separator_repeat.gif" height="11">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" class="clsLeftPaddingTable">
                                        <table>
                                            <tr>
                                                <td>
                                                    <span class="clssubhead">Start Date :</span>&nbsp;
                                                </td>
                                                <td>
                                                    <ew:CalendarPopup ID="calstartdate" runat="server" ToolTip="Call Back Date" PadSingleDigits="True"
                                                        ShowClearDate="True" Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True"
                                                        CalendarLocation="Left" ImageUrl="../images/calendar.gif" ControlDisplay="TextBoxImage"
                                                        Width="99px" Font-Size="8pt" Font-Names="Tahoma">
                                                        <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                                        <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                            BackColor="White"></WeekdayStyle>
                                                        <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                            ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                                        <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                                            BackColor="AntiqueWhite"></OffMonthStyle>
                                                        <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                            ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                                        <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                            BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                                        <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                            ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                                        <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                            BackColor="LightGray"></WeekendStyle>
                                                        <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                            ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                                        <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                            ForeColor="Black" BackColor="White"></ClearDateStyle>
                                                        <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                            BackColor="White"></HolidayStyle>
                                                    </ew:CalendarPopup>
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    <span class="clssubhead">End Date :</span>&nbsp;
                                                </td>
                                                <td>
                                                    <ew:CalendarPopup ID="calenddate" runat="server" ToolTip="Call Back Date" PadSingleDigits="True"
                                                        ShowClearDate="True" Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True"
                                                        CalendarLocation="Left" ImageUrl="../images/calendar.gif" ControlDisplay="TextBoxImage"
                                                        Width="99px" Font-Size="8pt" Font-Names="Tahoma">
                                                        <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                                        <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                            BackColor="White"></WeekdayStyle>
                                                        <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                            ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                                        <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                                            BackColor="AntiqueWhite"></OffMonthStyle>
                                                        <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                            ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                                        <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                            BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                                        <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                            ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                                        <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                            BackColor="LightGray"></WeekendStyle>
                                                        <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                            ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                                        <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                            ForeColor="Black" BackColor="White"></ClearDateStyle>
                                                        <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                            BackColor="White"></HolidayStyle>
                                                    </ew:CalendarPopup>
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    <span class="clssubhead">Case Status :</span>&nbsp;
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddCaseStatus" CssClass="clsInputCombo" runat="server" DataTextField="Name"
                                                        DataValueField="Id" Width="182px">
                                                    </asp:DropDownList>
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    <span class="clssubhead">Attorney :</span>&nbsp;
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddAttorney" runat="server" CssClass="clsInputCombo" DataTextField="FullName"
                                                        DataValueField="UserName" Width="160px">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="5" align="right">
                                                    <span class="clssubhead">Partner :</span>&nbsp;
                                                </td>
                                                <td colspan="1" align="left">
                                                    <asp:DropDownList ID="ddDivisionOuter" runat="server" CssClass="clsInputCombo" DataTextField="CompanyName"
                                                        DataValueField="CompanyId" Width="182px">
                                                    </asp:DropDownList>
                                                </td>
                                                <td colspan="2" align="right">
                                                    <asp:CheckBox ID="chkShowAll" runat="server" Text="Show All" CssClass="clsLeftPaddingTable" />&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <asp:Button ID="btnSearch" runat="server" CssClass="clsbutton" OnClientClick="return CheckDateValidation()"
                                                        Text="Search" OnClick="btnSearch_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td background="../images/separator_repeat.gif" height="11">
                                    </td>
                                </tr>
                                <tr>
                                    <td background="../Images/subhead_bg.gif" height="34" class="clssubhead" align="right">
                                        <table width="100%">
                                            <tr>
                                                <td align="left" style="width: 50%">
                                                    <span class="clssubhead">Polm Report</span>
                                                </td>
                                                <td align="right" style="text-align: right;" valign="middle">
                                                    <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" background="../Images/separator_repeat.gif" style="height: 11px;">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="pnl_main"
                                            DisplayAfter="2">
                                            <ProgressTemplate>
                                                <img alt="" src="../images/plzwait.gif" />&nbsp;<asp:Label ID="lbl1" runat="server"
                                                    CssClass="clssubhead" Text="Please Wait ......"></asp:Label>
                                            </ProgressTemplate>
                                        </aspnew:UpdateProgress>
                                        <%--<aspnew:UpdatePanel ID="pnl_main" runat="server">
                                    <ContentTemplate>--%>
                                        <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label>
                                        <asp:GridView ID="gvPolm" runat="server" AutoGenerateColumns="False" Width="100%"
                                            CssClass="clsLeftPaddingTable" AllowPaging="True" PageSize="20" OnPageIndexChanging="gvPolm_PageIndexChanging"
                                            CellPadding="0" CellSpacing="0" OnSorting="gvPolm_Sorting" OnRowCommand="gvPolm_RowCommand"
                                            OnRowDataBound="gvPolm_RowDataBound">
                                            <Columns>
                                                <asp:TemplateField HeaderText="S.No">
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblSno" CssClass="clssubhead" runat="server" Text='<%# Container.DataItemIndex+1 %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Opened">
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblFiledDate" runat="server" Text='<%# Eval("ProspectCaseFileDate","{0:MM/dd/yyyy}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Client">
                                                    <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblFirstName" runat="server" Text='<%# Eval("ProspectFullName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Status">
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblCaseStatus" runat="server" Text='<%# Eval("ProspectCaseStatus") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Value">
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblDamagesvalue" runat="server" Text='<%# Eval("ProspectDamageValue") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Ad Source">
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblSource" runat="server" Text='<%# Eval("ProspectCaseSource") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Case Type">
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblQlmd" runat="server" Text='<%# Eval("ProspectQuickLegalMatterDescription") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Last Contact">
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblLastCaseStatusUpdateDate" runat="server" Text='<%# Eval("ProspectLastCaseStatusUpdateDate","{0:MM/dd/yyyy}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Follow Up Date">
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="left" CssClass="clsLeftPaddingTable" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblFollowupdate" runat="server" Text='<%# Eval("ProspectCaseFollowUpDate","{0:MM/dd/yyyy}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Assignment">
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="left" CssClass="clsLeftPaddingTable" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblAssignedAttorney" runat="server" Text='<%# Eval("ProspectCaseAssignAttorney","{0:MM/dd/yyyy}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Date Assign">
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="left" CssClass="clsLeftPaddingTable" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblAssignedDate" runat="server" Text='<%# Eval("ProspectCaseAssignDate","{0:MM/dd/yyyy}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Date Closed">
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="left" CssClass="clsLeftPaddingTable" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblClosedDate" runat="server" Text='<%# Eval("ProspectCaseCloseDate","{0:MM/dd/yyyy}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Referring Sullo">
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="left" CssClass="clsLeftPaddingTable" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblReffering" runat="server" Text='<%# Eval("ProspectCaseReffAttorneyName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="">
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="left" CssClass="clsLeftPaddingTable" />
                                                    <ItemTemplate>
                                                        <div title="hideselects=[on] offsetx=[-410] offsety=[-200] singleclickstop=[on] requireclick=[off] header=[<table border='0' width='400px'><tr><td width='100%' align='right'><img src='../Images/close_button.png' border='0' onmouseover='CursorIcon()' onmouseout='CursorIcon2()' onclick='hideBox()'></td></tr></table>] body=[<table border='0' width='400px'><tr><td><textarea id='txtCommentMsg' name='txtCommentMsg' cols='46' rows='10'></textarea></td></tr></table>] ">
                                                            <asp:Image ID="ImgCommnetsMsg" ImageUrl="~/Images/comments.png" runat="server" onclick="copyToClipboard(document.getElementById('txtCommentMsg').value);" />
                                                        </div>
                                                        <asp:HiddenField ID="HfCommentMsg" runat="server" Value='<%# Server.HtmlEncode((string)Eval("ProspectLegalMatterDescription")) %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="ImgAddnew" runat="server" ToolTip="Edit Prospect" ImageUrl="~/Images/add.gif"
                                                            CommandArgument='<%# Eval("ProspectId") %>' CommandName="click" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerStyle HorizontalAlign="Center" />
                                            <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                        </asp:GridView>
                                        <%--</ContentTemplate>
                                </aspnew:UpdatePanel>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" background="../Images/separator_repeat.gif" style="height: 11px;">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <uc1:Footer ID="Footer1" runat="server" />
                                    </td>
                                </tr>
                            </table>
                            <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="Button1"
                                PopupControlID="pnlAddnNew" BackgroundCssClass="modalBackground" HideDropDownList="false">
                            </ajaxToolkit:ModalPopupExtender>
                            <asp:Button ID="Button1" runat="server" Text="Button" Style="display: none;" />
                            <asp:Panel ID="pnlAddnNew" runat="server" BackColor="White" Width="900px">
                                <table id="table1" style="border-top: black thin solid; border-left: black thin solid;
                                    border-bottom: black thin solid; border-right: black thin solid;" cellpadding="0"
                                    cellspacing="0" width="100%" class="clsLeftPaddingTable">
                                    <tr>
                                        <td>
                                            <ajaxToolkit:CollapsiblePanelExtender ID="CBECaseIdentification" runat="Server" TargetControlID="pnlCaseIdentification"
                                                CollapsedSize="0" Collapsed="false" ExpandControlID="panelTitleCaseIdentification"
                                                CollapseControlID="panelTitleCaseIdentification" AutoCollapse="False" AutoExpand="False"
                                                CollapsedText="Show Details..." ExpandedText="Hide Details" ImageControlID="imgInfo"
                                                CollapsedImage="../Images/collapse.jpg" ExpandedImage="../Images/expand.jpg" />
                                            <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="Server"
                                                TargetControlID="pnlCommentsHistory" CollapsedSize="0" Collapsed="True" ExpandControlID="pnlCommentsHistoryTitle"
                                                CollapseControlID="pnlCommentsHistoryTitle" AutoCollapse="False" AutoExpand="False"
                                                CollapsedText="Show Details..." ExpandedText="Hide Details" ImageControlID="imgcommentsHistory"
                                                ExpandDirection="Vertical" CollapsedImage="../Images/collapse.jpg" ExpandedImage="../Images/expand.jpg" />
                                            <asp:Panel ID="panelTitleCaseIdentification" runat="server" Height="30px" BackImageUrl="~/images/subhead_bg.gif"
                                                CssClass="clssubhead" Width="100%">
                                                <table border="0" width="100%" style="height: 100%">
                                                    <tr>
                                                        <td align="left">
                                                            <asp:Image ID="imgInfo" runat="server" ImageUrl="../images/collapse.jpg" Style="cursor: pointer;" />&nbsp;
                                                            <span class="clssubhead">Edit POLM :</span>
                                                        </td>
                                                        <td align="right">
                                                            <asp:LinkButton ID="LinkButton1" OnClientClick="return HideEditPopup();" OnClick="LinkButton1_Click"
                                                                runat="server">X</asp:LinkButton>&nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <asp:Panel ID="pnlCaseIdentification" runat="server" Width="100%" Height="100%">
                                                <div style="height: 450px; overflow: auto; width: 930px">
                                                    <table width="98%">
                                                        <tr>
                                                            <td colspan="4" background="../images/separator_repeat.gif" height="11">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4" align="left">
                                                                <%--Fahad 7783 05/13/2010 Informative message modified--%>
                                                                <span style="color: Red; font-size: small">Information mark with * is mandatory</span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" valign="middle">
                                                                <span class="clssubhead">First Name :</span>
                                                            </td>
                                                            <td valign="middle" align="left">
                                                                <asp:TextBox ID="txtfisrtname" runat="server" Width="220px" MaxLength="50" CssClass="clsInputadministration"></asp:TextBox>
                                                                &nbsp;<span style="color: red">*</span>
                                                            </td>
                                                            <td align="right" valign="middle">
                                                                <span class="clssubhead">Last Name :</span>
                                                            </td>
                                                            <td valign="middle" align="left">
                                                                &nbsp;<asp:TextBox ID="txtLastname" runat="server" Width="220px" MaxLength="50" CssClass="clsInputadministration"></asp:TextBox>
                                                                &nbsp;<span style="color: red">*</span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" valign="middle">
                                                                <span class="clssubhead">Mid Name :</span>
                                                            </td>
                                                            <td valign="middle" align="left">
                                                                <asp:TextBox ID="Txtmiddlename" runat="server" Width="220px" MaxLength="10" CssClass="clsInputadministration"></asp:TextBox>
                                                                &nbsp;
                                                            </td>
                                                            <td align="right" valign="middle">
                                                                <span class="clssubhead">Languages :</span>
                                                            </td>
                                                            <td align="left">
                                                                <table>
                                                                    <tr>
                                                                        <td id="td_language" runat="server" valign="middle" align="left" style="width: 245px">
                                                                            <asp:DropDownList ID="ddlanguages" runat="server" Width="225px" DataTextField="Name"
                                                                                DataValueField="Id" CssClass="clsInputCombo">
                                                                            </asp:DropDownList>
                                                                            &nbsp;<span style="color: red">*</span>
                                                                        </td>
                                                                        <td style="display: none; width: 245px" runat="server" id="td_languageLabel" valign="middle"
                                                                            align="left">
                                                                            <asp:Label ID="lbllanguage" CssClass="clsLeftPaddingTable" runat="server"></asp:Label>
                                                                        </td>
                                                                        <td style="display: none;" runat="server" id="td_languageImage" valign="middle" align="left">
                                                                            <asp:ImageButton ID="imgLanguage" runat="server" ImageAlign="Middle" ImageUrl="~/images/edit.gif"
                                                                                OnClientClick="return Showhide('td_languageLabel','td_language','td_languageImage','ddlanguages');" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" valign="middle">
                                                                <span class="clssubhead">DOB :</span>
                                                            </td>
                                                            <td valign="middle" align="left">
                                                                <asp:TextBox ID="txtDOB" runat="server" Width="220px" MaxLength="10" CssClass="clsInputadministration"></asp:TextBox>
                                                                &nbsp;<span style="color: red">*</span>
                                                            </td>
                                                            <td align="right" valign="middle">
                                                                <span class="clssubhead">Address :</span>
                                                            </td>
                                                            <td valign="middle" align="left">
                                                                &nbsp;<asp:TextBox ID="txtAddress" runat="server" Width="220px" MaxLength="200" CssClass="clsInputadministration"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" valign="middle">
                                                                <span class="clssubhead">Contact Number 1 :</span>
                                                            </td>
                                                            <td valign="middle" align="left">
                                                                <asp:TextBox ID="txtContactnumber1" runat="server" Width="220px" MaxLength="15" CssClass="clsInputadministration"></asp:TextBox>
                                                                &nbsp;<span style="color: red">*</span>
                                                            </td>
                                                            <td align="right" valign="middle">
                                                                <span class="clssubhead">Contact Type 1 :</span>
                                                            </td>
                                                            <td align="left">
                                                                <table>
                                                                    <tr>
                                                                        <td id="td_Contact1" valign="middle" runat="server" align="left" style="width: 245px">
                                                                            <asp:DropDownList ID="ddContactType1" runat="server" Width="225px" DataValueField="Id"
                                                                                DataTextField="Name" CssClass="clsInputCombo">
                                                                            </asp:DropDownList>
                                                                            &nbsp;<span style="color: red">*</span>
                                                                        </td>
                                                                        <td style="display: none; width: 245px" runat="server" id="td_ContactLabel1" valign="middle"
                                                                            align="left">
                                                                            <asp:Label ID="lblContactType1" CssClass="clsLeftPaddingTable" runat="server"></asp:Label>
                                                                        </td>
                                                                        <td style="display: none;" runat="server" id="td_ContactImage1" valign="middle" align="left">
                                                                            <asp:ImageButton ID="imgContacttype1" runat="server" ImageAlign="Middle" ImageUrl="~/images/edit.gif"
                                                                                OnClientClick="return Showhide('td_ContactLabel1','td_Contact1','td_ContactImage1','ddContactType1');" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" valign="middle">
                                                                <span class="clssubhead">Contact Number 2 :</span>
                                                            </td>
                                                            <td valign="middle" align="left">
                                                                <asp:TextBox ID="txtContactnumber2" runat="server" Width="220px" MaxLength="15" CssClass="clsInputadministration"></asp:TextBox>
                                                                &nbsp;
                                                            </td>
                                                            <td align="right" valign="middle">
                                                                <span class="clssubhead">Contact Type 2 :</span>
                                                            </td>
                                                            <td align="left">
                                                                <table>
                                                                    <tr>
                                                                        <td id="td_Contact2" valign="middle" runat="server" align="left" style="width: 245px">
                                                                            <asp:DropDownList ID="ddContactType2" runat="server" Width="225px" DataValueField="Id"
                                                                                DataTextField="Name" CssClass="clsInputCombo">
                                                                            </asp:DropDownList>
                                                                            &nbsp;
                                                                        </td>
                                                                        <td style="display: none; width: 245px" runat="server" id="td_ContactLabel2" valign="middle"
                                                                            align="left">
                                                                            <asp:Label ID="lblContact2" CssClass="clsLeftPaddingTable" runat="server"></asp:Label>
                                                                        </td>
                                                                        <td style="display: none;" runat="server" id="td_ContactImage2" valign="middle" align="left">
                                                                            <asp:ImageButton ID="imgContacttype2" runat="server" ImageAlign="Middle" ImageUrl="~/images/edit.gif"
                                                                                OnClientClick="return Showhide('td_ContactLabel2','td_Contact2','td_ContactImage2','ddContactType2');" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" valign="middle">
                                                                <span class="clssubhead">Contact Number 3 :</span>
                                                            </td>
                                                            <td valign="middle" align="left">
                                                                <asp:TextBox ID="txtContactnumber3" runat="server" Width="220px" MaxLength="15" CssClass="clsInputadministration"></asp:TextBox>
                                                                &nbsp;
                                                            </td>
                                                            <td align="right" valign="middle">
                                                                <span class="clssubhead">Contact Type 3 :</span>
                                                            </td>
                                                            <td align="left">
                                                                <table>
                                                                    <tr>
                                                                        <td valign="middle" align="left" id="td_Contact3" runat="server" style="width: 245px">
                                                                            <asp:DropDownList ID="ddContactType3" runat="server" Width="225px" DataValueField="Id"
                                                                                DataTextField="Name" CssClass="clsInputCombo">
                                                                            </asp:DropDownList>
                                                                            &nbsp;
                                                                        </td>
                                                                        <td style="display: none; width: 245px" runat="server" id="td_ContactLabel3" valign="middle"
                                                                            align="left">
                                                                            <asp:Label ID="lblContactType3" CssClass="clsLeftPaddingTable" runat="server"></asp:Label>
                                                                        </td>
                                                                        <td style="display: none;" runat="server" id="td_ContactImage3" valign="middle" align="left">
                                                                            <asp:ImageButton ID="imgContactType3" runat="server" ImageAlign="Middle" ImageUrl="~/images/edit.gif"
                                                                                OnClientClick="return Showhide('td_ContactLabel3','td_Contact3','td_ContactImage3','ddContactType3');" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" valign="middle">
                                                                <span class="clssubhead">Email Address :</span>
                                                            </td>
                                                            <td valign="middle" align="left">
                                                                <asp:TextBox ID="TxtEmail" runat="server" Width="220px" MaxLength="60" CssClass="clsInputadministration"></asp:TextBox>
                                                            </td>
                                                            <td align="right" valign="middle">
                                                                <span class="clssubhead">DL# :</span>
                                                            </td>
                                                            <td valign="middle" align="left">
                                                                &nbsp;<asp:TextBox ID="TxtDL" runat="server" Width="220px" MaxLength="20" CssClass="clsInputadministration"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4" background="../images/separator_repeat.gif" height="11">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" valign="middle">
                                                                <span class="clssubhead">Case Status :</span>
                                                            </td>
                                                            <td align="left">
                                                                <table>
                                                                    <tr>
                                                                        <td valign="middle" align="left" id="td_caseStatus" runat="server" style="width: 245px">
                                                                            <asp:DropDownList ID="ddcasestatusinner" onchange="return ShowStatusDropDownChange();"
                                                                                runat="server" Width="220px" DataValueField="Id" DataTextField="Name" CssClass="clsInputCombo">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td style="display: none; width: 245px" runat="server" id="td_caseStatusLabel" valign="middle"
                                                                            align="left">
                                                                            <asp:Label ID="lblCaseStatus" CssClass="clsLeftPaddingTable" runat="server"></asp:Label>
                                                                        </td>
                                                                        <td style="display: none;" runat="server" id="td_caseStatusImage" valign="middle"
                                                                            align="left">
                                                                            <asp:ImageButton ID="imgCaseStatus" runat="server" ImageAlign="Middle" ImageUrl="~/images/edit.gif"
                                                                                OnClientClick="return Showhide('td_caseStatusLabel','td_caseStatus','td_caseStatusImage','ddcasestatusinner');" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td align="right" valign="middle">
                                                                <span class="clssubhead">Case Type :</span>
                                                            </td>
                                                            <td align="left">
                                                                <table>
                                                                    <tr>
                                                                        <td valign="middle" align="left" id="td_caseType" runat="server" style="width: 245px">
                                                                            <asp:DropDownList ID="ddcasetype" runat="server" Width="220px" DataTextField="Name"
                                                                                DataValueField="Id" CssClass="clsInputCombo">
                                                                            </asp:DropDownList>
                                                                            &nbsp;<span style="color: red">*</span>
                                                                        </td>
                                                                        <td style="display: none; width: 245px" runat="server" id="td_caseTypeLabel" valign="middle"
                                                                            align="left">
                                                                            <asp:Label ID="lblCaseType" CssClass="clsLeftPaddingTable" runat="server"></asp:Label>
                                                                        </td>
                                                                        <td style="display: none;" runat="server" id="td_CaseTypeImage" valign="middle" align="left">
                                                                            <asp:ImageButton ID="imgCaseType" runat="server" ImageAlign="Middle" ImageUrl="~/images/edit.gif"
                                                                                OnClientClick="return Showhide('td_caseTypeLabel','td_caseType','td_CaseTypeImage','ddcasetype');" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" valign="middle">
                                                                <span class="clssubhead">Partner :</span>
                                                            </td>
                                                            <td align="left">
                                                                <table>
                                                                    <tr>
                                                                        <td id="td_Division" runat="server" valign="middle" align="left" style="width: 245px">
                                                                            <asp:DropDownList ID="ddDivision" runat="server" Width="220px" DataTextField="CompanyName"
                                                                                DataValueField="CompanyId" CssClass="clsInputCombo">
                                                                            </asp:DropDownList>
                                                                            &nbsp;<span style="color: red">*</span>
                                                                        </td>
                                                                        <td style="display: none; width: 245px" runat="server" id="td_DivisionLabel" valign="middle"
                                                                            align="left">
                                                                            <asp:Label ID="lblDivision" CssClass="clsLeftPaddingTable" runat="server"></asp:Label>
                                                                        </td>
                                                                        <td style="display: none;" runat="server" id="td_DivisionLabelImage" valign="middle"
                                                                            align="left">
                                                                            <asp:ImageButton ID="imgDivision" runat="server" ImageAlign="Middle" ImageUrl="~/images/edit.gif"
                                                                                OnClientClick="return Showhide('td_DivisionLabel','td_Division','td_DivisionLabelImage','ddDivision');" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td align="right" valign="middle">
                                                                <span class="clssubhead">Bodily Damages :</span>
                                                            </td>
                                                            <td align="left">
                                                                <table>
                                                                    <tr>
                                                                        <td valign="middle" align="left" id="td_BodilyDamage" runat="server" style="width: 245px">
                                                                            <asp:DropDownList ID="ddBodilyDamages" runat="server" Width="220px" DataValueField="Id"
                                                                                DataTextField="Name" CssClass="clsInputCombo">
                                                                            </asp:DropDownList>
                                                                            &nbsp;
                                                                        </td>
                                                                        <td style="display: none; width: 245px" runat="server" id="td_BodilyDamageLabel"
                                                                            valign="middle" align="left">
                                                                            <asp:Label ID="lblbodilyDamage" CssClass="clsLeftPaddingTable" runat="server"></asp:Label>
                                                                        </td>
                                                                        <td style="display: none;" runat="server" id="td_BodilyDamageImage" valign="middle"
                                                                            align="left">
                                                                            <asp:ImageButton ID="imgBodilyDamage" runat="server" ImageAlign="Middle" ImageUrl="~/images/edit.gif"
                                                                                OnClientClick="return Showhide('td_BodilyDamageLabel','td_BodilyDamage','td_BodilyDamageImage','ddBodilyDamages');" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" valign="middle">
                                                                <span class="clssubhead">Quote and Fee Information :</span>
                                                            </td>
                                                            <td valign="middle" align="left">
                                                                &nbsp;<asp:TextBox ID="TxtFeeInformation" runat="server" Width="215px" CssClass="clsInputadministration"
                                                                    MaxLength="100"></asp:TextBox>
                                                            </td>
                                                            <td align="right" valign="middle">
                                                                <span class="clssubhead">Assigned User :</span>
                                                            </td>
                                                            <td align="left">
                                                                <table>
                                                                    <tr>
                                                                        <td valign="middle" align="left" id="td_Attorney" runat="server" style="width: 245px">
                                                                            <asp:DropDownList ID="ddAttorneyinner" runat="server" Width="220px" DataTextField="FullName"
                                                                                DataValueField="UserId" CssClass="clsInputCombo">
                                                                            </asp:DropDownList>
                                                                            &nbsp;
                                                                        </td>
                                                                        <td style="display: none; width: 245px" runat="server" id="td_AttorneyLabel" valign="middle"
                                                                            align="left">
                                                                            <asp:Label ID="lblAttorney" CssClass="clsLeftPaddingTable" runat="server"></asp:Label>
                                                                        </td>
                                                                        <td style="display: none;" runat="server" id="td_AttorneyImage" valign="middle" align="left">
                                                                            <asp:ImageButton ID="imgAttorney" runat="server" ImageAlign="Middle" ImageUrl="~/images/edit.gif"
                                                                                OnClientClick="return Showhide('td_AttorneyLabel','td_Attorney','td_AttorneyImage','ddAttorneyinner');" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" valign="middle">
                                                                <span class="clssubhead">Please attach case file :</span>
                                                            </td>
                                                            <td align="left">
                                                                &nbsp;<asp:FileUpload ID="fpAttachment" CssClass="clssubhead" runat="server" Width="250px"
                                                                    ToolTip="Upload File" /><br />
                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Only JPG, TIFF, PDF, DOC, DOCX, XLS, XLSX, XLSM and PDF file format is allowed to upload."
                                                                    ControlToValidate="fpAttachment" ValidationGroup="AddEditProspect" Display="Dynamic"
                                                                    ValidationExpression="(.*?)\.(jpg|JPG|jpeg|JPEG|tiff|TIFF|doc|DOC|docx|DOCX|pdf|PDF|xls|xlsx|xlsm|XLS|XLSX|XLSM)$"></asp:RegularExpressionValidator>
                                                            </td>
                                                            <td valign="middle" align="right">
                                                                <span class="clssubhead">Damages Value :</span>
                                                            </td>
                                                            <td align="left">
                                                                <table>
                                                                    <tr>
                                                                        <td valign="middle" align="left" id="td_DamageValue" runat="server" style="width: 245px">
                                                                            <asp:DropDownList ID="ddDamageValue" runat="server" Width="220px" DataValueField="Id"
                                                                                DataTextField="Name" CssClass="clsInputCombo">
                                                                            </asp:DropDownList>
                                                                            &nbsp;
                                                                        </td>
                                                                        <td style="display: none; width: 245px" runat="server" id="td_DamageValueLabel" valign="middle"
                                                                            align="left">
                                                                            <asp:Label ID="lblDamageValue" CssClass="clsLeftPaddingTable" runat="server"></asp:Label>
                                                                        </td>
                                                                        <td style="display: none;" runat="server" id="td_DamageValueImage" valign="middle"
                                                                            align="left">
                                                                            <asp:ImageButton ID="imgDamageValue" runat="server" ImageAlign="Middle" ImageUrl="~/images/edit.gif"
                                                                                OnClientClick="return Showhide('td_DamageValueLabel','td_DamageValue','td_DamageValueImage','ddDamageValue');" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" valign="middle">
                                                                <span class="clssubhead">Legal Matter Description :</span>
                                                            </td>
                                                            <td valign="middle" align="left">
                                                                <asp:Label ID="lblQuicklegal" CssClass="clsLeftPaddingTable" runat="server" Width="220px"
                                                                    Text=""></asp:Label>
                                                            </td>
                                                            <td class="clssubhead" align="right" id="tdfollowuptitle" style="display: none">
                                                                Follow Up Date :
                                                            </td>
                                                            <td align="left" id="tdfollowupdate" runat="server" style="display: none">
                                                                &nbsp;
                                                                <ew:CalendarPopup ID="calFollowupDate" runat="server" ToolTip="Call Back Date" PadSingleDigits="True"
                                                                    ShowClearDate="True" Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True"
                                                                    CalendarLocation="Left" ImageUrl="../images/calendar.gif" ControlDisplay="TextBoxImage"
                                                                    Width="99px" Font-Size="8pt" Font-Names="Tahoma">
                                                                    <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                                                    <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                                        BackColor="White"></WeekdayStyle>
                                                                    <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                        ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                                                    <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                                                        BackColor="AntiqueWhite"></OffMonthStyle>
                                                                    <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                        ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                                                    <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                                        BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                                                    <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                        ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                                                    <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                                        BackColor="LightGray"></WeekendStyle>
                                                                    <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                        ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                                                    <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                        ForeColor="Black" BackColor="White"></ClearDateStyle>
                                                                    <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                                        BackColor="White"></HolidayStyle>
                                                                </ew:CalendarPopup>
                                                            </td>
                                                        </tr>
                                                        <tr align="left">
                                                            <td>
                                                            </td>
                                                            <td colspan="3" class="clsLeftPaddingTable" align="left">
                                                                <asp:Label ID="lblFileHistory" runat="server" ForeColor="Red"></asp:Label>
                                                                <asp:GridView ID="GridFileHistory" runat="server" AutoGenerateColumns="False" CssClass="clsLeftPaddingTable"
                                                                    CellPadding="0" CellSpacing="0" BorderWidth="1" Width="350px" BorderColor="Black"
                                                                    OnRowCommand="GridFileHistory_RowCommand" OnRowDataBound="GridFileHistory_RowDataBound">
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="S#">
                                                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" Width="20px" />
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="LblSno" CssClass="clssubhead" runat="server" Text='<%# Eval("SerialNumber") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Date">
                                                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" Width="70px" />
                                                                            <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="LblUploadedDate" runat="server" Text='<%# Eval("UploadedDate","{0:MM/dd/yyyy}") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="File Name">
                                                                            <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="LblOriginalFileName" runat="server" Text='<%# Eval("OriginalFileName") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Uploaded By">
                                                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                                                            <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="LblUserName" runat="server" Text='<%# Eval("UserName") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField ItemStyle-VerticalAlign="Middle" FooterStyle-VerticalAlign="Middle">
                                                                            <HeaderStyle Width="50px" />
                                                                            <ItemTemplate>
                                                                                <%--Fahad 7783 05/13/2010 Image replaced by pencil image to preview image and Tool Tip Added in both images--%>
                                                                                <asp:ImageButton ID="ImgView" runat="server" ImageUrl="~/Images/preview.gif" ToolTip="View"
                                                                                    CommandName="editimage" />
                                                                                <asp:ImageButton ID="ImgDelete" runat="server" ImageUrl="~/Images/cross.gif" ToolTip="Delete"
                                                                                    OnClientClick="return confirm('Are you sure you want to delete this file?');"
                                                                                    CommandName="deleteimage" />
                                                                                <asp:HiddenField ID="HfId" runat="server" Value='<%# Eval("AttachmentId") %>' />
                                                                                <asp:HiddenField ID="HfStoreFileName" runat="server" Value='<%# Eval("StoredFileName") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" valign="middle">
                                                                <span class="clssubhead">Comments :</span>
                                                            </td>
                                                            <td colspan="3">
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td align="left">
                                                                            <asp:TextBox ID="txtUserComments" runat="server" Width="635px" TextMode="MultiLine"
                                                                                Height="120px" CssClass="clsInputadministration"></asp:TextBox><br />
                                                                            <asp:Label ID="lblcomments" runat="server" ForeColor="Red" Text="" Visible="true"></asp:Label><br />
                                                                            <asp:RegularExpressionValidator ID="revComments" runat="server" ControlToValidate="txtUserComments"
                                                                                ErrorMessage="Too many characters in comments" SetFocusOnError="true" ValidationGroup="createuser"
                                                                                ValidationExpression="^[\S\s]{1,1969}$" Display="Dynamic"></asp:RegularExpressionValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                            </td>
                                                            <td colspan="3">
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td align="left">
                                                                            <asp:Button ID="BtnSubmitInfo" runat="server" Text="Save" ValidationGroup="createuser"
                                                                                OnClick="BtnSubmitInfo_Click" OnClientClick="return ValidateControl()" CssClass="clsbutton">
                                                                            </asp:Button><br />
                                                                            &nbsp;
                                                                            <asp:Label ID="lblPopupMessage" runat="server" ForeColor="Red" Text="" Visible="false"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                            </td>
                                                            <td align="left">
                                                                <asp:ValidationSummary ID="vSummary" runat="server" ValidationGroup="createuser"
                                                                    DisplayMode="BulletList" HeaderText="Please provide the following information:" />
                                                                <asp:RequiredFieldValidator ID="rfvFisrtName" runat="server" ErrorMessage="Please enter first name"
                                                                    Display="None" ControlToValidate="txtfisrtname" ValidationGroup="createuser"></asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="revFirstName" runat="server" ErrorMessage="Please provide valid first name."
                                                                    Display="None" ControlToValidate="txtfisrtname" ValidationExpression="^[a-zA-Z ]*$"
                                                                    ValidationGroup="createuser"></asp:RegularExpressionValidator>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please select language"
                                                                    ControlToValidate="ddlanguages" Display="None" ValidationGroup="createuser" InitialValue="-1"></asp:RequiredFieldValidator>
                                                                <asp:RequiredFieldValidator ID="RfvLastName" runat="server" Display="None" ErrorMessage="Please enter last name"
                                                                    ControlToValidate="txtLastname" ValidationGroup="createuser"></asp:RequiredFieldValidator>
                                                                <asp:RequiredFieldValidator ID="RfvDivision" runat="server" ErrorMessage="Please select Partner."
                                                                    Display="None" ControlToValidate="ddDivision" ValidationGroup="createuser" InitialValue="-1"></asp:RequiredFieldValidator>
                                                                <asp:RequiredFieldValidator ID="rfvRequired" runat="server" Display="None" ErrorMessage="Please enter contact number 1"
                                                                    ControlToValidate="txtContactnumber1" ValidationGroup="createuser"></asp:RequiredFieldValidator>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please select contact type 1"
                                                                    Display="None" ControlToValidate="ddContactType1" ValidationGroup="createuser"
                                                                    InitialValue="-1"></asp:RequiredFieldValidator>
                                                                <asp:RequiredFieldValidator ID="RfvDOB" runat="server" ErrorMessage="Please enter DOB"
                                                                    ControlToValidate="txtDOB" Display="None" ValidationGroup="createuser"></asp:RequiredFieldValidator>
                                                                <asp:RequiredFieldValidator ID="rfvcasetype" runat="server" ErrorMessage="Please select case type"
                                                                    ControlToValidate="ddcasetype" Display="None" ValidationGroup="createuser" InitialValue="-1"></asp:RequiredFieldValidator>
                                                                <asp:RangeValidator ID="rv_Dob" runat="server" Display="None" ControlToValidate="txtDOB"
                                                                    Type="Date" ErrorMessage="Please enter valid DOB within 01/01/1910 to yesterday's date."
                                                                    Font-Size="Smaller">
                                                                </asp:RangeValidator>
                                                                <asp:RegularExpressionValidator ID="revEmail" runat="server" ErrorMessage="Please enter valid email address. eg : abc@john.com"
                                                                    Display="None" Font-Size="Small" ControlToValidate="TxtEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                                                <asp:RegularExpressionValidator ID="revLastName" runat="server" ErrorMessage="Please provide valid last name."
                                                                    Display="None" ControlToValidate="txtLastname" ValidationExpression="^[a-zA-Z ]*$"
                                                                    ValidationGroup="createuser"></asp:RegularExpressionValidator>
                                                                <asp:RegularExpressionValidator ID="revDLNumber" runat="server" ErrorMessage="Invalid DL #: Please enter alphanumeric characters."
                                                                    Display="None" ControlToValidate="TxtDL" ValidationExpression="^[0-9a-zA-Z]+$"
                                                                    ValidationGroup="createuser"></asp:RegularExpressionValidator>
                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Only JPG TIFF PDF DOC DOCX PDF file format is allowed to upload."
                                                                    ControlToValidate="fpAttachment" ValidationGroup="createuser" Display="None"
                                                                    ValidationExpression="(.*?)\.(jpg|JPG|jpeg|JPEG|tiff|doc|DOC|docx|DOCX|pdf|PDF|xls|xlsx)$"></asp:RegularExpressionValidator>
                                                                <asp:HiddenField ID="hffollowupdate" runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </asp:Panel>
                                            <asp:Panel ID="pnlCommentsHistoryTitle" runat="server" Width="100%">
                                                <table width="100%" border="0" cellspacing="0">
                                                    <tr>
                                                        <td background="../Images/subhead_bg.gif" valign="bottom" width="100%" style="height: 32px"
                                                            align="left">
                                                            <table border="0" width="100%" style="height: 100%">
                                                                <tr>
                                                                    <td align="left">
                                                                        <asp:Image ID="imgcommentsHistory" runat="server" ImageUrl="../images/collapse.jpg"
                                                                            Style="cursor: pointer;" />&nbsp; <span class="clssubhead">Comments History :</span>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <asp:Panel ID="pnlCommentsHistory" runat="server" Width="100%">
                                                <div style="height: 250px; overflow: auto; width: 930px">
                                                    <table width="100%">
                                                        <tr>
                                                            <td align="center" colspan="2">
                                                                <asp:Label ID="lblCommentsHistory" runat="server" ForeColor="Red"></asp:Label>
                                                                <asp:GridView ID="GridCommentsHistory" runat="server" AutoGenerateColumns="False"
                                                                    Width="635px" BorderWidth="1" BorderColor="Black" CssClass="clsLeftPaddingTable"
                                                                    CellPadding="0" CellSpacing="0" AllowSorting="false" AllowPaging="false">
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="User">
                                                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                                                            <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="LblUserName" runat="server" Text='<%# Eval("UserName") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Date">
                                                                            <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" Width="130px" />
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="LblInsertedDate" runat="server" Text='<%# Eval("InsertedDate","{0:MM/dd/yyyy @ hh:mm tt}") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Comments">
                                                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                                                            <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="LblNote" runat="server" Text='<%# Eval("Note") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                            <aspnew:AsyncPostBackTrigger ControlID="GridFileHistory" />
                            <aspnew:PostBackTrigger ControlID="BtnSubmitInfo" />
                        </Triggers>
                    </aspnew:UpdatePanel>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;

namespace HTP.backroom
{
    public partial class CompetitorsReport_New : System.Web.UI.Page
    {
        clsSession ClsSession = new clsSession();
        //clsLogger clog = new clsLogger();
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        //DataSet ds_val;
        int type = 0;
        DataTable dtRecords;
        DataView dvcompetitors;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Waqas 5057 03/17/2009 Checking employee info in session
                if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else //To stop page further execution
                {
                    if (ClsSession.GetCookie("sAccessType", this.Request).ToString() != "2")
                    {
                        Response.Redirect("../LoginAccesserror.aspx", false);
                        Response.End();
                    }
                    else //To stop page further execution
                    {
                        lbl_message.Text = "";
                        if (!IsPostBack)
                        {
                            calQueryFrom.SelectedDate = DateTime.Today;
                            calQueryTo.SelectedDate = DateTime.Today;
                            btn_submit.Attributes.Add("OnClick", "return validate();");
                        }
                    }
                    //Waqas Javed 5255 12/16/08
                    if (rdbtn_out_analysis.Checked == false)
                    {
                        ddlType.Style.Add("Display", "none");
                        ddlRecords.Style.Add("Display", "none");
                    }
                    else
                    {
                        ddlType.Style.Add("Display", "block");
                        ddlRecords.Style.Add("Display", "block");
                    }

                    if (rdbtn_Att_Archive.Checked == false)
                    {
                        if (rdbtn_out_analysis.Checked == false)
                        {
                            ddlRecords.Style.Add("Display", "none");
                        }
                        else
                        {
                            ddlRecords.Style.Add("Display", "block");
                        }
                    }
                    else
                    {
                        ddlRecords.Style.Add("Display", "block");
                    }



                }
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
            }
        }

        protected void btn_submit_Click(object sender, EventArgs e)
        {
            try
            {
                //Waqas 5057 03/17/2009 Checking employee info in session               
                if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else //To stop page further execution
                {
                    dg_valrep.Visible = drepDateRange.Visible = false;
                    //Waqas Javed 5255 12/16/08
                    dg_valrep.Visible = drepDateRangeOutAnalysis.Visible = false;

                    if (this.rdbtn_Att_Archive.Checked == false && this.rdbtn_out_analysis.Checked == false)
                    {
                        dg_valrep.CurrentPageIndex = 0;
                        GetRecords();
                        if (dtRecords.Rows.Count == 0)
                        {
                            lbl_message.Text = "No Record Found";
                            dg_valrep.Visible = false;
                        }
                        else
                        {
                            dg_valrep.Visible = true;
                            dg_valrep.DataSource = dtRecords;
                            dg_valrep.DataBind();
                        }

                        if (dtRecords.Rows.Count > 0)
                        {
                            dvcompetitors = new DataView(dtRecords);
                            Session["dvcompetitors"] = dvcompetitors;
                        }

                        if (type == 1)
                        {
                            dg_valrep.Columns[4].Visible = true;
                            dg_valrep.Columns[6].Visible = true;
                            dg_valrep.Columns[8].Visible = true;
                        }
                        else
                        {
                            dg_valrep.Columns[4].Visible = false;
                            dg_valrep.Columns[6].Visible = false;
                            dg_valrep.Columns[8].Visible = false;
                        }
                        //dg_valrep.Visible = true;
                    }
                    else if (this.rdbtn_Att_Archive.Checked)
                    {
                        DateTime startDate = new DateTime(calQueryFrom.SelectedDate.Year, calQueryFrom.SelectedDate.Month, 1);
                        DateTime endDate = GetLastDate(calQueryTo.SelectedDate.Year, calQueryTo.SelectedDate.Month);

                        DataTable dtDateRange = new DataTable("DateRange");
                        dtDateRange.Columns.Add(new DataColumn("StartDate", Type.GetType("System.DateTime")));
                        dtDateRange.Columns.Add(new DataColumn("EndDate", Type.GetType("System.DateTime")));

                        DateTime tempStartDate, tempEndDate;
                        tempStartDate = startDate;
                        //making a data table of dates by breaking the date range in months.
                        while (tempStartDate < endDate)
                        {
                            tempEndDate = this.GetLastDate(tempStartDate.Year, tempStartDate.Month);
                            DataRow dRow = dtDateRange.NewRow(); dtDateRange.Rows.Add(dRow);
                            dRow["StartDate"] = tempStartDate; dRow["EndDate"] = tempEndDate;
                            tempStartDate = tempStartDate.AddMonths(1);
                        }
                        drepDateRange.DataSource = dtDateRange;
                        drepDateRange.DataBind();
                        drepDateRange.Visible = true;
                    }
                    //Waqas 5255 12/16/2008 New Option for Outcome Analysis Report... 
                    else if (this.rdbtn_out_analysis.Checked)
                    {
                        //Sabir Khan 5462 03/26/2009 Start date and End Date logic has been changed...
                        //-----------------------------------------------------------------------------
                        //DateTime startDate = new DateTime(calQueryFrom.SelectedDate.Year, calQueryFrom.SelectedDate.Month, 1);
                        //DateTime endDate = GetLastDate(calQueryTo.SelectedDate.Year, calQueryTo.SelectedDate.Month);

                        DateTime startDate = calQueryFrom.SelectedDate;
                        DateTime endDate = calQueryTo.SelectedDate;

                        DataTable dtDateRange = new DataTable("DateRange");
                        dtDateRange.Columns.Add(new DataColumn("StartDate", Type.GetType("System.DateTime")));
                        dtDateRange.Columns.Add(new DataColumn("EndDate", Type.GetType("System.DateTime")));

                        DateTime tempStartDate, tempEndDate;
                        tempStartDate = startDate;
                        //making a data table of dates by breaking the date range in months.
                        while (tempStartDate <= endDate)
                        {
                            if (endDate.Month != tempStartDate.Month)
                            {
                                tempEndDate = this.GetLastDate(tempStartDate.Year, tempStartDate.Month);
                            }
                            else
                            {
                                tempEndDate = endDate;
                            }
                            DataRow dRow = dtDateRange.NewRow(); dtDateRange.Rows.Add(dRow);
                            dRow["StartDate"] = tempStartDate; dRow["EndDate"] = tempEndDate;
                            //Sabir Khan 5462 03/26/2009 Start date and End Date logic has been changed...
                            //tempStartDate = tempStartDate.AddMonths(1);
                            tempStartDate = tempEndDate.AddDays(1);
                        }
                        //-------------------------------------------------------------------------------
                        drepDateRangeOutAnalysis.DataSource = dtDateRange;
                        drepDateRangeOutAnalysis.DataBind();
                        drepDateRangeOutAnalysis.Visible = true;
                    }


                    if (rdbtn_out_analysis.Checked == false)
                    {
                        pnl_Att_Bond_Att.Style.Add("Display", "block");
                        pnl_Out_Analysis.Style.Add("Display", "none");
                    }
                    else
                    {
                        pnl_Att_Bond_Att.Style.Add("Display", "none");
                        pnl_Out_Analysis.Style.Add("Display", "block");
                    }

                    if (rdbtn_Att.Checked == true)
                        lbl_status.Text = rdbtn_Att.Text;
                    else if (rdbtn_Att_Archive.Checked == true)
                        lbl_status.Text = rdbtn_Att_Archive.Text;
                    else if (rdbtn_out_analysis.Checked == true)
                        lbl_status.Text = rdbtn_out_analysis.Text;
                    else
                        lbl_status.Text = rdbtn_bond.Text;
                }
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }

        }

        private void GetRecords()
        {
            try
            {
                if (rdbtn_bond.Checked)
                    type = 1;

                string[] key = { "@startdate", "@enddate", "@Type" };
                object[] value1 = { calQueryFrom.SelectedDate, calQueryTo.SelectedDate, type };
                dtRecords = ClsDb.Get_DT_BySPArr("usp_hts_CompetitorReport_New", key, value1);
                generateSerialNo();

                lbl_message.Text = "";
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.ToString();
                clsLogger.ErrorLog(ex);
            }
        }

        protected void dg_valrep_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Header)
            {
                if (rdbtn_bond.Checked)
                {
                    type = 1;
                }

                if (type == 1)
                {
                    ((LinkButton)e.Item.FindControl("LinkButton1")).Text = "Bonding Company";
                    ((LinkButton)e.Item.FindControl("LinkButton2")).Text = "No. of Bond cases";
                    ((LinkButton)e.Item.FindControl("LinkButton3")).Text = "No. of Bond Clients";
                    ((LinkButton)e.Item.FindControl("LinkButton5")).Text = "Bond Amount";
                    ((LinkButton)e.Item.FindControl("LinkButton4")).Text = "% of Bonds";
                }
                else
                {
                    ((LinkButton)e.Item.FindControl("LinkButton1")).Text = "Attorney Name";
                    ((LinkButton)e.Item.FindControl("LinkButton2")).Text = "Cases";
                    ((LinkButton)e.Item.FindControl("LinkButton3")).Text = "Clients";
                    ((LinkButton)e.Item.FindControl("LinkButton5")).Text = "Bond Amount";
                    ((LinkButton)e.Item.FindControl("LinkButton4")).Text = "% of Cases";
                }
            }

            //Kamran 3624 4/4/08 add footer
            object caseTotal = dtRecords.Compute("SUM(noofcases)", "");
            object clientsTotal = dtRecords.Compute("SUM(sumBond_clients)", "");
            if (e.Item.ItemType == ListItemType.Footer)
            {
                e.Item.Cells[1].Font.Bold = true;
                e.Item.Cells[2].Font.Bold = true;
                e.Item.Cells[3].Font.Bold = true;

                e.Item.Cells[1].Text = "Total";
                e.Item.Cells[2].Text = caseTotal.ToString();
                e.Item.Cells[3].Text = clientsTotal.ToString();
            }
        }

        public SortDirection GridViewSortDirection
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Ascending;

                return (SortDirection)ViewState["sortDirection"];
            }
            set { ViewState["sortDirection"] = value; }
        }

        private void SortGridView(string sortExpression, string direction)
        {
            GetRecords();
            DataTable dt = dtRecords;
            DataView dv = (DataView)(Session["dvcompetitors"]);
            dv.Sort = sortExpression + " " + direction;
            dg_valrep.DataSource = dv;
            dg_valrep.DataBind();
        }

        protected void dg_valrep_SortCommand(object source, DataGridSortCommandEventArgs e)
        {
            string sortExpression = e.SortExpression;
            Session["SortExpression"] = sortExpression;

            if (GridViewSortDirection == SortDirection.Ascending)
            {
                GridViewSortDirection = SortDirection.Descending;
                Session["SortDirection"] = " desc";
                SortGridView(sortExpression, " desc");
            }
            else
            {
                GridViewSortDirection = SortDirection.Ascending;
                Session["SortDirection"] = " asc";
                SortGridView(sortExpression, " asc");
            }
        }

        protected void dg_valrep_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            string sortExpression = e.CommandName.ToString();
            Session["SortExpression"] = sortExpression;

            if (GridViewSortDirection == SortDirection.Ascending)
            {
                GridViewSortDirection = SortDirection.Descending;
                Session["SortDirection"] = " desc";
                if (e.CommandName != "Page")
                {
                    SortGridView(sortExpression, " desc");
                }
            }
            else
            {
                GridViewSortDirection = SortDirection.Ascending;
                Session["SortDirection"] = " asc";
                if (e.CommandName != "Page")
                {
                    SortGridView(sortExpression, " asc");
                }
            }
        }

        /// <summary>
        /// This Method is used as an event of pageindexchange of paging control.
        /// </summary>
        /// <description>
        ///This method is called automatically when a user change the page number from paging control. 
        /// </description>

        //kazim 2720 01/29/08 add the pageindex changes event for enable paging in the grid
        protected void Pagingctrl_PageIndexChanged()
        {
            try
            {
                dg_valrep.DataSource = Session["dvcompetitors"];
                dg_valrep.DataBind();
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// This Method is used as an event of pageindexchange.
        /// </summary>
        /// <description>
        ///This method is called automatically when a user change the page number from grid . 
        /// </description>

        //Kazim 2720 01/29/2008 This event is used to set the paging on the datgrid.
        protected void dg_valrep_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            try
            {

                dg_valrep.CurrentPageIndex = e.NewPageIndex;
                dg_valrep.DataSource = Session["dvcompetitors"];
                dg_valrep.DataBind();
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// This method is used to generate serial no 
        /// </summary>
        /// <description>
        ///This method automatically add the serial number column if a table does not have, and also fill the serial number
        /// with each record
        /// </description>
        /// <example >
        ///DataTable dt=assignatable;
        ///GenerateSerialNo()
        /// </example>

        //Kazim 2720 01/29/2008 The method is used to add a column s.no in the grid and generate serial number 
        public void generateSerialNo()
        {
            if (dtRecords.Columns.Contains("sno") == false)
            {
                dtRecords.Columns.Add("sno");
                int sno = 1;

                //added By Aziz to check if rows existed
                if (dtRecords.Rows.Count >= 1)
                    dtRecords.Rows[0]["sno"] = 1;

                if (dtRecords.Rows.Count >= 2)
                {
                    for (int i = 1; i < dtRecords.Rows.Count; i++)
                    {
                        dtRecords.Rows[i]["sno"] = ++sno;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Year">Year part of the date</param>
        /// <param name="Month">Month part of the date</param>
        /// <returns>Date having last day of the given month/year</returns>
        private DateTime GetLastDate(int Year, int Month)
        {
            return new DateTime(Year, Month, DateTime.DaysInMonth(Year, Month));
        }

        /// <summary>
        /// Add serial no column in the data table
        /// </summary>
        /// <param name="dtRecords">Reference of the Data Table</param>
        private void GenerateSerialNo(DataTable dtRecords)
        {
            if (dtRecords.Columns.Contains("sno") == false)
            {
                dtRecords.Columns.Add("sno");
                int sno = 1;
                if (dtRecords.Rows.Count >= 1)
                    dtRecords.Rows[0]["sno"] = 1;

                if (dtRecords.Rows.Count >= 2)
                {
                    for (int i = 1; i < dtRecords.Rows.Count; i++)
                    {
                        dtRecords.Rows[i]["sno"] = ++sno;
                    }
                }
            }
        }

        protected void drepDateRange_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                try
                {
                    DateTime startDate = (DateTime)((DataRowView)e.Item.DataItem)["StartDate"];
                    DateTime endDate = (DateTime)((DataRowView)e.Item.DataItem)["EndDate"];

                    Label lblStartDate = (Label)e.Item.FindControl("lblStartDate");
                    lblStartDate.Text = startDate.ToString("MMM yyyy");

                    string[] key = { "@startdate", "@enddate", "@Type", "@NoOfRecords" };
                    object[] value1 = { startDate, endDate, 0, this.ddlRecords.SelectedValue };
                    // tahir 4138 09/11/2008 rename the SP as per standard....
                    DataTable dtMonthRecords = ClsDb.Get_DT_BySPArr("USP_HTP_CompetitorReportArchive", key, value1);
                    GenerateSerialNo(dtMonthRecords);

                    DataGrid dgAttorney = (DataGrid)e.Item.FindControl("dgAttorney");
                    dgAttorney.DataSource = dtMonthRecords;
                    dgAttorney.DataBind();
                }
                catch (Exception exp)
                {
                    clsLogger.ErrorLog(exp);
                }
            }
        }
        //Waqas Javed 5255 12/16/08 
        protected void drepDateRangeOutAnalysis_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                try
                {
                    DateTime startDate = (DateTime)((DataRowView)e.Item.DataItem)["StartDate"];
                    DateTime endDate = (DateTime)((DataRowView)e.Item.DataItem)["EndDate"];

                    Label lblStartDate = (Label)e.Item.FindControl("lblStartDate");
                    lblStartDate.Text = startDate.ToString("MMM yyyy");

                    int TypeClient = 1;
                    if (this.ddlType.SelectedValue == "Clients")
                    {
                        TypeClient = 1;
                    }
                    else if (this.ddlType.SelectedValue == "Cases")
                    {
                        TypeClient = 0;
                    }
                    string[] key = { "@sdate", "@edate", "@Type", "@NoOfRecords" };
                    object[] value1 = { startDate, endDate, TypeClient, this.ddlRecords.SelectedValue };

                    DataTable dtMonthRecords = ClsDb.Get_DT_BySPArr("USP_HTP_CompetitorOutcomeAnalysis", key, value1);
                    GenerateSerialNo(dtMonthRecords);

                    DataGrid dg_out_analysis = (DataGrid)e.Item.FindControl("dg_out_analysis");

                    dg_out_analysis.DataSource = dtMonthRecords;



                    if (this.ddlType.SelectedValue == "Clients")
                    {
                        dg_out_analysis.Columns[3].Visible = false;
                        dg_out_analysis.Columns[5].Visible = false;
                        dg_out_analysis.Columns[7].Visible = false;
                        dg_out_analysis.Columns[9].Visible = false;
                        dg_out_analysis.Columns[11].Visible = false;
                        dg_out_analysis.Columns[13].Visible = false;

                    }
                    else if (this.ddlType.SelectedValue == "Cases")
                    {
                        dg_out_analysis.Columns[2].Visible = false;
                        dg_out_analysis.Columns[4].Visible = false;
                        dg_out_analysis.Columns[6].Visible = false;
                        dg_out_analysis.Columns[8].Visible = false;
                        dg_out_analysis.Columns[10].Visible = false;
                        dg_out_analysis.Columns[12].Visible = false;

                    }


                    dg_out_analysis.DataBind();



                }
                catch (Exception exp)
                {
                    clsLogger.ErrorLog(exp);
                }
            }
        }


    }

}

<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.backroom.CategoryInfo" Codebehind="CategoryInfo.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>CategoryInfo</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<script>
	/*	function OpenWinEdit(strForm)
	      {  
	          var Win;
		      //Win = window.open("FrmAddNewCategory.aspx","","fullscreen=no,toolbar=no,width=450,height=180,left=250,top=250,status=no,menubar=no,scrollbars=yes,resizable=yes");
		      //Win = window.open('"strForm"','ds',"scrollbars=yes,width=600,height=300");
		      Win = window.open("'"+strForm+"'",'ds',"fullscreen=no,toolbar=no,width=570,height=350,left=120,top=200,status=no,menubar=no,scrollbars=yes,resizable=yes");				

		      return false;				      
	       }   
	      */
	      function OpenDelWin(intCategoryID)
	      {  
	          var Win;
		      //Win = window.open("FrmAddNewCategory.aspx","","fullscreen=no,toolbar=no,width=450,height=180,left=250,top=250,status=no,menubar=no,scrollbars=yes,resizable=yes");
		      //Win = window.open('"strForm"','ds',"scrollbars=yes,width=600,height=300");
		      //Win = window.open("FrmDelCategory.aspx?CategoryID="+intCategoryID,'ds',"fullscreen=no,toolbar=no,width=2,height=2,left=0,top=0,status=no,menubar=no,resizable=no");				
		      Win = window.open("FrmDelCategory.aspx?CategoryID="+intCategoryID,'ds',"fullscreen=no,toolbar=no,width=700,height=500,left=120,top=200,status=no,menubar=no,resizable=no");				
		      return false;				      
	       }   
	     
		  function OpenWinEdit(intCategoryID)
	      {  
	          var Win;
		      //Win = window.open("FrmAddNewCategory.aspx","","fullscreen=no,toolbar=no,width=450,height=180,left=250,top=250,status=no,menubar=no,scrollbars=yes,resizable=yes");
		      //Win = window.open('"strForm"','ds',"scrollbars=yes,width=600,height=300");
		      Win = window.open("FrmAddNewCategory.aspx?CategoryID="+intCategoryID,'ds',"fullscreen=no,toolbar=no,width=700,height=500,left=120,top=200,status=no,menubar=no,resizable=no");				
		      return false;				      
	       }   

		function OpenWin()
		{
	          var Win;
		      Win = window.open("FrmAddNewCategory.aspx",'ds',"fullscreen=no,toolbar=no,width=500,height=225,left=120,top=200,status=no,menubar=no,scrollbars=yes,resizable=yes");				
		      return false;				      
		}
	       
	     
		</script>

        <link href="../Styles.css" rel="stylesheet" type="text/css" />
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE id="tblMain" cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
				<TBODY>
					<tr>
					</tr>
					<TR>
						<TD>
							<TABLE id="tblsub" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
								<tr>
									<td><uc1:activemenu id="ActiveMenu1" runat="server"></uc1:activemenu></td>
								</tr>
								<tr>
									<td background="../../images/separator_repeat.gif"  height="11"></td>
								</tr>
								<tr>
									<td align="center">
										<table id="gridhead" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
											<tr>
												<td background="../../images/headbar_headerextend.gif" colSpan="4" height="5"></td>
											</tr>
											<tr>
												<td class="clssubhead" height=34  background="../Images/subhead_bg.gif"
													colSpan="3" >&nbsp;Category Information</td>
												<td class="clssubhead" vAlign="middle" align="right"background="../Images/subhead_bg.gif" ><asp:linkbutton id="lnkb_addnewcat" runat="server"  >Add New Category</asp:linkbutton></td>
											</tr>
											<tr>
												<td background="../../images/headbar_footerextend.gif" height: 25px;></td>
											</tr>
										</table>
										<asp:label id="lblMessage" runat="server" EnableViewState="False" Font-Size="XX-Small"
											Font-Names="Verdana" ForeColor="Red" Font-Bold="True"></asp:label></td>
								</tr>
								<tr>
									<td>
										<table id="tblgrid" cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
											<TR>
												<TD align="center"><asp:datagrid id="dg_catinfo" runat="server" Width="780px"  CssClass="clsLeftPaddingTable"
														AutoGenerateColumns="False">
														<Columns>
															<asp:TemplateColumn Visible="False" HeaderText="Category ID">
																<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
																<ItemStyle HorizontalAlign="Left" VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id=lbl_id runat="server" Width="116px" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.CategoryID") %>'>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Category Name">
																<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
																<ItemStyle HorizontalAlign="Left" VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id=lbl_categorynam runat="server" Width="116px" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.CategoryName") %>'>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Category Description">
																<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
																<ItemStyle HorizontalAlign="Left" VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id=lbl_catdesc runat="server" Width="116px" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.CategoryDescription") %>'>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Short Description">
                                                            <HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblshortdesc" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.shortdescription") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Edit">
																<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
																<ItemStyle HorizontalAlign="Left" VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<!--<A onclick="OpenEditWin();"></A> -->
																	<asp:LinkButton id="Linkbutton1" runat="server" Text="Edit">Edit</asp:LinkButton>&nbsp;&nbsp;
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Delete">
																<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
																<ItemStyle HorizontalAlign="Left" VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<asp:LinkButton id="LinkButton2" runat="server">Delete</asp:LinkButton>
																</ItemTemplate>
															</asp:TemplateColumn>
														</Columns>
													</asp:datagrid></TD>
											</TR>
										</table>
									</td>
								</tr>
								<tr>
									<td background="../../images/separator_repeat.gif"  height="11"></td>
								</tr>
								<tr>
									<TD colSpan="5"><uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
								</tr>
							</TABLE>
						</TD>
					</TR>
				</TBODY>
			</TABLE>
		</form>
	</body>
</HTML>

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.backroom
{
    /// <summary>
    /// Summary description for AddComments.
    /// </summary>
    public partial class AddComments : System.Web.UI.Page
    {
        
        #region Variables
        
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        Comments Add_Comments = new Comments();
        clsSession cSession = new clsSession();
        clsLogger BugTracker = new clsLogger();
        string Bugid = "";
        string Flag = "";

        #endregion

        #region Events

        protected void Page_Load(object sender, System.EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["Bugid"] != null)
                {
                    Bugid = Request.QueryString["Bugid"].ToString();
                    Flag = Request.QueryString["Flag"].ToString();
                    int empid =Convert.ToInt32(Request.QueryString["Empid"]);
                    ViewState["empid"] = empid;
                    ViewState["BugID"] = Bugid;
                    ViewState["Flag"] = Flag;
                }
            }
            btnSubmit.Attributes.Add("onclick", "return Validation();");
        }

        protected void btnSubmit_Click(object sender, System.EventArgs e)
        {
            AddNewComments();
        }

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion

        #region Methods

        private void AddNewComments()
        {
            try
            {


                Add_Comments.AddNewComments(txt_Desc.Text.ToString().Trim(), System.DateTime.Today, Convert.ToInt32(ViewState["empid"]), Convert.ToInt32(ViewState["BugID"]));
                if (ViewState["Flag"].ToString() == "1")
                {
                    HttpContext.Current.Response.Write("<script language='javascript'>opener.location.reload('UpdateBug.aspx?Bugid=" + ViewState["BugID"].ToString() + "');self.close();</script>");
                }
                if (ViewState["Flag"].ToString() == "2")
                {
                    HttpContext.Current.Response.Write("<script language='javascript'>opener.location.reload('UpdateDeveloper.aspx?Bugid=" + ViewState["BugID"].ToString() + "');self.close();</script>");
                }
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        #endregion

    }
}

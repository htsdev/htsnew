using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using System.Data.SqlClient;


namespace SPUtility
{
	/// <summary>
	/// Summary description for WebForm1.
	/// </summary>
	public partial class WebForm1 : System.Web.UI.Page
	{
		protected eWorld.UI.CalendarPopup cal_FromDate;

		protected System.Web.UI.WebControls.TextBox txt_SP;
		protected System.Web.UI.WebControls.Button btn_Search;
		protected System.Web.UI.WebControls.DataGrid DG_SP;

		protected eWorld.UI.CalendarPopup cal_ToDate;

		//clsCheck ClsCheck=new clsCheck();

		clsENationWebComponents clsDB=new clsENationWebComponents();
		int alpha=1;

		DataView DV;
		protected System.Web.UI.WebControls.DropDownList ddl_User;
		protected System.Web.UI.WebControls.Label lbl_Msg;
		
		string sp_PreviousValue;

		
		private void Page_Load(object sender, System.EventArgs e)
		{
			if(!IsPostBack)
			{ 
				cal_FromDate.SelectedDate=DateTime.Today;
				cal_ToDate.SelectedDate=DateTime.Today;
				btn_Search.Attributes.Add("onClick","return validate()");
				//btn_Save.Attributes.Add("onClick","return validate()");
				lbl_Msg.Text="";
				lbl_Msg.Visible=false;
				alpha=1;
				BindUsers();


			}
			// Put user code to initialize the page here
			
		}
		private void BindUsers()
		{ 
			try
			{ 
				ddl_User.Items.Clear();
				ddl_User.Items.Add(new ListItem("All","-1"));
				
				DataSet Ds_Users=clsDB.Get_DS_BySP("USP_SPU_GET_Users");
				for(int i=0;i<Ds_Users.Tables[0].Rows.Count;i++)
				{
					ddl_User.Items.Add(new ListItem(Ds_Users.Tables[0].Rows[i]["UserName"].ToString(),Ds_Users.Tables[0].Rows[i]["ID"].ToString()));
				}
				

			}
			catch (Exception ex)
			{ 
				lbl_Msg.Visible=true;
				lbl_Msg.Text=ex.Message;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btn_Search.Click += new System.EventHandler(this.btn_Search_Click);
			this.DG_SP.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.DG_SP_ItemCommand);
			this.DG_SP.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.DG_SP_PageIndexChanged);
			this.DG_SP.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.DG_SP_ItemDataBound);
			this.DG_SP.SelectedIndexChanged += new System.EventHandler(this.DG_SP_SelectedIndexChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		/*//private int BindSerialNo()
		{	
			
//			long sNo=(DG_SP.CurrentPageIndex)*(DG_SP.PageSize);									
//			try
//			{
//				
//				foreach (DataGridItem ItemX in DG_SP.Items) 
//				{ 					
//						sNo +=1 ;
//						((Label)(ItemX.FindControl("lbl_Sno"))).Text= sNo.ToString(); 				
//					
//					
//				}
//			}
//			catch (Exception ex)
//			{ 
//				Response.Write(ex.Message);
//			}
		}*/

		private void btn_Search_Click(object sender, System.EventArgs e)
		{
			try
			{
				
				lbl_Msg.Text="";
				lbl_Msg.Visible=false;

				string FromDate=cal_FromDate.SelectedDate.ToShortDateString();
				string ToDate=cal_ToDate.SelectedDate.ToShortDateString();
				string SPName;
				
				
				if(txt_SP.Text =="")
				{

					SPName="%";
				}
				else
				{ 
					SPName=txt_SP.Text.ToString();
				}

			
				int userid=Convert.ToInt32(ddl_User.SelectedValue);
				
					
				
					string[] key={"@FromDate","@ToDate","@SP_Name","@User_id"};
					object[] value1={FromDate,ToDate,SPName,userid};

					DataSet Ds_GETSP=clsDB.Get_DS_BySPArr("USP_SPU_GET_SearchSP",key,value1);
					if(Ds_GETSP.Tables[0].Rows.Count>0)
					{
						DG_SP.CurrentPageIndex=0;
						DG_SP.Visible=true;
						DG_SP.DataSource=Ds_GETSP;
						DV=new DataView(Ds_GETSP.Tables[0]);
						Session["DV"]=DV;
						DG_SP.DataBind();
					}
					else
					{ 
						lbl_Msg.Text="No Record Found";
						lbl_Msg.Visible=true;
						DG_SP.Visible=false;
					}
				}
			
								
			
			catch (Exception ex)
			{  
				lbl_Msg.Visible=true;
				lbl_Msg.Text=ex.Message;
			}
		}

		private void DG_SP_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			//BindSerialNo();
			/*sp_PreviousValue=((Label) e.Item.FindControl("lbl_SPName")).Text.ToString();
			if(e.Item.ItemType==ListItemType.Item || e.Item.ItemType==ListItemType.AlternatingItem)
			{
				string sp_NextValue;
				
				for(int i=0;i<DG_SP.Items.Count;i++)
				{ 
					sp_NextValue=((Label) e.Item.FindControl("lbl_SPName")).Text.ToString();
					if(sp_PreviousValue.Equals(sp_NextValue))
					{ 
						BindSerialNo();
					}
				}
			}*/
			if(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				if(alpha>1)
				{
					if(sp_PreviousValue == ((Label)e.Item.Cells[2].FindControl("lbl_SPName")).Text)
					{
						((Label)e.Item.Cells[0].FindControl("lbl_Sno")).Text = Convert.ToString(alpha);
						alpha++;}
					else
					{
						sp_PreviousValue = ((Label)e.Item.Cells[2].FindControl("lbl_SPName")).Text;
						alpha=1;
						((Label)e.Item.Cells[0].FindControl("lbl_Sno")).Text = Convert.ToString(alpha);
						alpha++;
					}


				}
				else if (alpha==1)
				{
					this.sp_PreviousValue = ((Label)e.Item.Cells[2].FindControl("lbl_SPName")).Text;
					((Label)e.Item.Cells[0].FindControl("lbl_Sno")).Text = alpha.ToString();
					alpha++;
				}

			}
		}
		private void DG_SP_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}

		private void DG_SP_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			try
			{
				if (e.CommandName=="view")
				{ 
					string SPID=((Label) e.Item.FindControl("lbl_SID")).Text.ToString();
					Response.Redirect("ProcedureDetail.aspx?SPID="+SPID);
				}
				/*if (e.CommandName=="RollBack")
				{ 
					string Spname=((Label)e.Item.FindControl("lbl_SPName")).Text;
					
					clsDB.ExecuteNonQuery("drop procedure Azwer_Test_Procedure");
					//clsDB.ExecuteNonQuery("create procedure  Azwer_Test_Procedure as select * from tbl_SourceSafe_Details");
					

				}*/
			}
			catch (Exception ex)
			{ 
				lbl_Msg.Visible=true;
				lbl_Msg.Text=ex.Message;
			}
		}

		private void DG_SP_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			try
			{ 
				DG_SP.CurrentPageIndex=e.NewPageIndex;
				DV=(DataView) Session["DV"];
				DG_SP.DataSource=DV;
				DG_SP.DataBind();
				
			}
			catch (Exception ex)
			{ 
				lbl_Msg.Visible=true;
				lbl_Msg.Text=ex.Message;
			}
		}
	}
}

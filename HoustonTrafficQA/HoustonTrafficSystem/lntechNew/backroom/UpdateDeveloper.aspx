<%@ Page language="c#" Codebehind="UpdateDeveloper.aspx.cs" AutoEventWireup="True" Inherits="lntechNew.backroom.UpdateDeveloper" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="~/WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>UpdateDeveloper</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript">
		function ShowHide()
		{ 
			var td=document.getElementById("tdattachment");
			var txt=document.getElementById("TextBox1").value;
			var tdcomments=document.getElementById("tdComments");
			var txtcomments=document.getElementById("TextBox2").value;
			
			if(txt == "1")
			{ 
				td.style.display='block';
			}
			if(txt == "0")
			{ 
				td.style.display='none';
			}
			if(txtcomments == "1") 
			{ 
				tdcomments.style.display='block';
			}
			if(txtcomments == "0")
			{ 
				tdcomments.style.display='none';
			} 
		}
		function Validation()
		{ 
			var dllusers=document.getElementById("ddl_users").value;
			if(dllusers == 0)
			{ 
				alert("PLease Select Assigned To User");
				document.getElementById("ddl_users").focus();
				return false;
			}
		}
		function OpenPop(path)
		{ 
		    
		     
		    
		    window.open(path,'',"height=130,width=400,resizable=yes,status=no,scrollbar=no,menubar=no,location=no");
		    return false;
		    
		
		}
		
		</script>

        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
	</HEAD>
	<body MS_POSITIONING="GridLayout" bottomMargin="0" topMargin="0" onload="ShowHide();">
		<form id="Form1" method="post" runat="server">
			<TABLE id="TableMain" cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
			<tr><td style="width: 781px" >
			    <uc1:activemenu id="ActiveMenu1" runat="server"></uc1:activemenu>
			</td></tr>
			<TR>
								<TD background="../../images/separator_repeat.gif" Height="11" style="width: 781px" ></TD>
							</TR>
				<TR>
					<TD class="clssubhead" background="../Images/subhead_bg.gif" height="34" style="width: 781px">
					<table border="0" cellpadding="0" cellspacing="0" width="100%" >
					<tr>
					<td class="clssubhead" style="height: 13px" >
					&nbsp;Assigned Bug
					</td>
					<td align="right" class="clssubhead" style="height: 13px" >
                        <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click">View Bugs</asp:LinkButton>&nbsp;|&nbsp;
                        <asp:HyperLink ID="hpttm" runat="server" NavigateUrl="~/backroom/GeneralBug.aspx">Trouble Ticket Management</asp:HyperLink>&nbsp;
                        </td>
                        </tr>
                        </table>
                        </TD>
				</TR>
				<TR>
					<TD vAlign="top" >
						<TABLE id="tbl1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
							<td colspan="3" style="width: 790px">
							    <table border="0" cellpadding="0" cellspacing="0" width="100%">
							        <tr>
							            <td class="clsLeftPaddingTable" style="height: 15px" >
							                BugID
							            </td>
							            <td class="clsLeftPaddingTable">
							                <asp:Label ID="lblBugId" runat="server"></asp:Label>
							            </td>
							            <td class="clsLeftPaddingTable" width="15%" >
							               <asp:Label ID="TickNo" runat="server">Ticket Number</asp:Label>
							            </td>
							            <td class="clsLeftPaddingTable" colspan="7">
							                <asp:Label ID="TicketNo" runat="server"></asp:Label>
							            </td>
							            
							        </tr>
							        <tr><td colspan="9" height="15"></td></tr>
							        <tr><td class="clsLeftPaddingTable">Page Url</td>
							            <td colspan="9" class="clsLeftPaddingTable" >
							                <asp:Label ID="lblPageurl" runat="server"></asp:Label>
							            </td></tr>
							        <tr><td colspan="9" height="15"></td></tr>
							    <tr>
							    <TD  width="10%" class="clsLeftPaddingTable" >
                                   Priority</TD>
								<TD  class="clsLeftPaddingTable" width="12%" >
									<asp:dropdownlist id="ddl_priority" runat="server" Width="70px"></asp:dropdownlist></TD>
									<td class="clsLeftPaddingTable" >Status</td>
									<td class="clsLeftPaddingTable" ><asp:dropdownlist id="ddl_Status" runat="server" Width="135px">
                                    </asp:DropDownList></td>
									<TD class="clsLeftPaddingTable" style=" width: 10%">
                                        &nbsp;Assign To
								</TD>
								<TD align="left" class="clsLeftPaddingTable" width="16%" >
									<asp:dropdownlist id="ddl_users" runat="server" Width="100px"></asp:dropdownlist>
									</TD>
									<TD class="clsLeftPaddingTable" align="left" width="12%" >
									&nbsp;&nbsp;<asp:button id="btnSubmit" runat="server" Text="Update" CssClass="clsButton" onclick="btnSubmit_Click"></asp:button></TD>
									<TD class="clsLeftPaddingTable" align="left" width="34%" colspan="2" >
                                        &nbsp;<asp:HyperLink ID="hpcomments" runat="server" NavigateUrl="#">Add Comments</asp:HyperLink></TD>
							    </tr>
							    </table>
							</td>
							</tr>
							
							<TR>
								<TD class="clsLeftPaddingTable" align="center" style="height: 13px; width: 790px;" colspan="3" >
									<asp:label id="lblMessage" runat="server" ForeColor="Red"></asp:label></TD>
							</TR>
							<TR>
								<TD class="clssubhead" id="tdattachment" background="../Images/subhead_bg.gif" colSpan="3"
									height="34" style="width: 790px">&nbsp;Attachment&nbsp;
								</TD>
							</TR>
							<TR>
								<TD class="clsLeftPaddingTable" align="left" colSpan="3" style="width: 790px">
									<asp:datagrid id="dg_attachment" runat="server" Width="100%" AutoGenerateColumns="False">
										<Columns>
											<asp:TemplateColumn HeaderText="File Description">
												<HeaderStyle HorizontalAlign="Left" CssClass="clssubhead"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id=lblfiledesc runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="File Name">
												<HeaderStyle HorizontalAlign="Left" CssClass="clssubhead"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id=lblfilename runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.attachfilename") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Uploaded Date">
												<HeaderStyle HorizontalAlign="Left" CssClass="clssubhead"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id=lblUploadedDate runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container,"DataItem.uploadeddate") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="UserName">
												<HeaderStyle HorizontalAlign="Left" CssClass="clssubhead"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id=lblusername runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.username") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn>
												<ItemTemplate>
													<asp:LinkButton id="lnkView" runat="server" CommandName="View">View</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn Visible="False" HeaderText="attachid">
												<ItemTemplate>
													<asp:Label id=lblattachmentid runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.attachmentid") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD class="clssubhead" id="tdComments" background="../Images/subhead_bg.gif" colSpan="3"
									height="34" style="width: 790px">&nbsp;Comments
								</TD>
							</TR>
							<TR>
								<TD class="clsLeftPaddingTable" align="left" colSpan="3" style="width: 790px" ></TD>
							</TR>
							<TR>
								<TD class="clsLeftPaddingTable" align="left" colSpan="3" style="width: 790px">
									<asp:datalist id="dl_comments" runat="server" Width="100%" Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False">
										
										<AlternatingItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False"></AlternatingItemStyle>
										<ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False"></ItemStyle>
										<ItemTemplate>
											<TABLE id="tbllist" cellSpacing="0" cellPadding="0" border="0">
												<TR>
													<TD class="clsLeftPaddingTable"><span style="color: Red">Comment Posted By :</span>
														<asp:Label id=lblName runat="server" CssClass="clsLeftPaddingTable"  ForeColor="Red" Text='<%# DataBinder.Eval(Container, "DataItem.username") %>'>
														</asp:Label>&nbsp;<span style="color: Red">on</span>
														<asp:Label id=lbldatetime runat="server" ForeColor="Red" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.commentsdate") %>'>
														</asp:Label>&nbsp;</TD>
												</TR>
												<TR>
													<TD>
														<asp:Label id=lblcomments Font-Size="10" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container,"DataItem.description") %>'>
														</asp:Label></TD>
												</TR>
											</TABLE>
										</ItemTemplate>
										<HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False"></HeaderStyle>
									</asp:datalist></TD>
							</TR>
							<tr>
									<td background="../../images/separator_repeat.gif" colSpan="3" height="11" style="width: 790px"></td>
								</tr>
								<tr>
									<TD colSpan="3" style="width: 790px"><uc1:footer id="Footer1" runat="server"></uc1:footer></td>
								</tr>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD vAlign="top" colSpan="3">
						<asp:textbox id="TextBox1" runat="server" Width="0px">0</asp:textbox>
						<asp:textbox id="Textbox2" runat="server" Width="0px">0</asp:textbox></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>

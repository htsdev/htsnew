<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Page language="c#" AutoEventWireup="false" Inherits="SPUtility.ProcedureDetail" Codebehind="ProcedureDetail.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ProcedureDetail</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript">
		function Not()
		{
			alert('You Can not Modify this Stored Procedure.');
			document.getElementById("btn_Rollback").focus();
			return false;
		}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
				<tr>
					<td>
						<uc1:activemenu id="ActiveMenu1" runat="server"></uc1:activemenu></td>
				</tr>
				<TR>
					<TD>
						<TABLE id="Table6" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD class="clsLeftPaddingTable">&nbsp;<STRONG>Stored Procedure Name :</STRONG>
								</TD>
								<TD class="clsLeftPaddingTable"><asp:label id="lbl_SPName" runat="server"></asp:label></TD>
								<TD class="clsLeftPaddingTable">&nbsp;<STRONG>User Name :</STRONG>
								</TD>
								<TD class="clsLeftPaddingTable" colSpan="2"><asp:label id="lbl_username" runat="server"></asp:label></TD>
							</TR>
							<tr>
								<td></td>
							</tr>
							<TR>
								<TD class="clsLeftPaddingTable">&nbsp;<STRONG>Date :</STRONG></TD>
								<TD class="clsLeftPaddingTable"><asp:label id="lbl_Date" runat="server"></asp:label></TD>
								<td class="clsLeftPaddingTable">&nbsp;<strong>Time :</strong></td>
								<td class="clsLeftPaddingTable"><asp:label id="lbl_Time" runat="server"></asp:label></td>
								<td class="clsLeftPaddingTable"><asp:button id="btn_Rollback" runat="server" CssClass="clsbutton" Text="Roll Back"></asp:button></td>
							</TR>
							<TR>
								<TD></TD>
							</TR>
							<TR>
								<TD class="clssubhead" colspan="5" style="HEIGHT: 34px" align="left" background="../../Images/subhead_bg.gif"><strong><A onclick="history.back();" href="#">Back</strong>
									</A>
								</TD>
							</TR>
							<tr>
								<td class="clsLeftPaddingTable" align="center" colSpan="6"><asp:label id="lbl_Msg" runat="server" CssClass="normalfont" ForeColor="Red"></asp:label></td>
							<TR>
								<td></td>
							</TR>
						</TABLE>
						<TABLE id="tbl" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD class="clsLeftPaddingTable" vAlign="top">&nbsp;<strong>Store Procedure :</strong>
								</TD>
								<td class="clsLeftPaddingTable"><asp:textbox id="txt_StoreProcedure" runat="server" Width="666px" CssClass="clsinputadministration"
										TextMode="MultiLine" Height="500px" Wrap="False"></asp:textbox></td>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>

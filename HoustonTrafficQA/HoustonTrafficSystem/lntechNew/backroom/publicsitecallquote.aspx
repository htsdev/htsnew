<%@ Page Language="C#" AutoEventWireup="true" Codebehind="publicsitecallquote.aspx.cs"
    Inherits="HTP.backroom.publicsitecallquote" %>

<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Quote Call Report</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table id="TableMain" cellspacing="0" cellpadding="0" width="980" align="center"
                border="0">
                <tr>
                    <td colspan="4">
                        <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <table id="Table1" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
                            <tr>
                                <td width="100%" background="../Images/separator_repeat.gif" colspan="4" height="10">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <table id="Table2" style="height: 14px" cellspacing="1" cellpadding="1"
                                        width="100%" border="0">
                                        <tr>
                                            <td class="clsLeftPaddingTable" style="width: 193px">
                                                <strong>Start Date</strong>&nbsp;:&nbsp;
                                                <ew:CalendarPopup ID="calstartdate" runat="server" ToolTip="Call Back Date" PadSingleDigits="True"
                                                    ShowClearDate="True" Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True"
                                                    CalendarLocation="Left" ImageUrl="../images/calendar.gif" ControlDisplay="TextBoxImage"
                                                    Width="99px" Font-Size="8pt" Font-Names="Tahoma">
                                                    <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                                    <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                        BackColor="White"></WeekdayStyle>
                                                    <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                                    <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                                        BackColor="AntiqueWhite"></OffMonthStyle>
                                                    <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                                    <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                        BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                                    <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                                    <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                        BackColor="LightGray"></WeekendStyle>
                                                    <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                                    <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="White"></ClearDateStyle>
                                                    <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                        BackColor="White"></HolidayStyle>
                                                </ew:CalendarPopup>
                                            </td>
                                            <td class="clsLeftPaddingTable" style="width: 60px">
                                                <strong>End Date:</strong></td>
                                            <td class="clsLeftPaddingTable" style="width: 124px">
                                                <ew:CalendarPopup ID="calenddate" runat="server" ToolTip="Call Back Date" PadSingleDigits="True"
                                                    ShowClearDate="True" Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True"
                                                    CalendarLocation="Left" ImageUrl="../images/calendar.gif" ControlDisplay="TextBoxImage"
                                                    Width="102px" Font-Size="8pt" Font-Names="Tahoma">
                                                    <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                                    <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                        BackColor="White"></WeekdayStyle>
                                                    <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                                    <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                                        BackColor="AntiqueWhite"></OffMonthStyle>
                                                    <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                                    <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                        BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                                    <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                                    <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                        BackColor="LightGray"></WeekendStyle>
                                                    <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                                    <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="White"></ClearDateStyle>
                                                    <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                        BackColor="White"></HolidayStyle>
                                                </ew:CalendarPopup>
                                            </td>
                                            <td class="clsLeftPaddingTable" style="width: 95px">
                                                <asp:DropDownList ID="ddlClientType" runat="server" CssClass="clsinputadministration"
                                                    Width="90px">
                                                    <asp:ListItem Value="0">All</asp:ListItem>
                                                    <asp:ListItem Value="1">Houston Cases</asp:ListItem>
                                                    <asp:ListItem Value="2">Dallas Cases</asp:ListItem>
                                                </asp:DropDownList></td>
                                            <td class="clsLeftPaddingTable">
                                                <asp:Button ID="btnSearch" runat="server" CssClass="clsbutton" Text="Submit" OnClick="btnSearch_Click">
                                                </asp:Button></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 11px" align="center" background="../Images/separator_repeat.gif"
                                    colspan="4">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td background="../Images/subhead_bg.gif" class="clssubhead" align="right" style="height: 34px"
                                    valign="middle">
                                    <uc2:PagingControl ID="PagingControl1" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td align="center" background="../Images/separator_repeat.gif" style="width: 980px;
                                    height: 11px;">
                                </td>
                            </tr>
                            <tr>
                                <td align="center" style="width: 980px;">
                                    <asp:Label ID="lblMessage" runat="server" CssClass="clslabel" Font-Bold="True" ForeColor="Red"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="left" colspan="4">
                                    <asp:GridView ID="gv_Results" runat="server" Width="100%" CssClass="clsLeftPaddingTable"
                                        Height="100%" OnPageIndexChanging="gv_PageIndexChanging" PageSize="30" AllowSorting="True"
                                        AllowPaging="True" BorderColor="White" BackColor="#EFF4FB" AutoGenerateColumns="False"
                                        OnRowDataBound="gv_Results_RowDataBound">
                                        <Columns>
                                            <asp:TemplateField HeaderText="S#">
                                                <HeaderStyle HorizontalAlign="Left" Width="2%" CssClass="clsaspcolumnheader"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSNo" runat="server" CssClass="label" Text='<%#bind("sno") %>'></asp:Label>
                                                    <asp:HiddenField ID="hfTicketId" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.TicketID") %>' />
                                                    <asp:HiddenField ID="hf_siteid" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.siteid") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Ticket No.">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkbtnClientname" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TicketNumber") %>'></asp:LinkButton>
                                                    <asp:Label ID="lbltktno" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.TicketNumber") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Left" Width="100px" CssClass="clsaspcolumnheader" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="ClientName" HeaderText="Client Name" >
                                                <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Left" />
                                                <ItemStyle CssClass="GridItemStyleBig" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="useremail" HeaderText="Email" >
                                                <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Left" />
                                                <ItemStyle CssClass="GridItemStyleBig" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="SiteQuote" HeaderText="Website Quote" DataFormatString="{0:C0}" >
                                                <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Left" />
                                                <ItemStyle CssClass="GridItemStyleBig" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="callquote" HeaderText="Call In Quote" DataFormatString="{0:C0}" >
                                                <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Left" />
                                                <ItemStyle CssClass="GridItemStyleBig" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="VCount" HeaderText="Total Violations" >
                                                <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Left" />
                                                <ItemStyle CssClass="GridItemStyleBig" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="FTA" HeaderText="FTA" >
                                                <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Left" />
                                                <ItemStyle CssClass="GridItemStyleBig" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="Hire Status">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblhirestatus" runat="server" CssClass="label"></asp:Label>
                                                    <br />
                                                    <asp:HiddenField ID="hfStatus" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.ActiveFlag") %>' />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Left" Width="40px" CssClass="clsaspcolumnheader" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="CaseType" HeaderText="Case Type">
                                                <ItemStyle CssClass="GridItemStyleBig" />
                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                            </asp:BoundField>
                                        </Columns>
                                        <PagerStyle HorizontalAlign="Center" />
                                        <PagerSettings Mode="NextPreviousFirstLast" NextPageText="Next &gt;" PreviousPageText="&lt; Previous" FirstPageText="&amp;lt;&amp;lt; First" LastPageText="Last &amp;gt;&amp;gt;" />
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td width="100%" background="../Images/separator_repeat.gif" colspan="4" height="11">
                                </td>
                            </tr>
                            <tr>
                                <td align="center" width="100%" colspan="4" style="height: 11px">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 760px" align="left" colspan="4">
                                    <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>

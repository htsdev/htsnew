﻿using System;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using HTP.ClientController;
using HTP.Components.Services;
using lntechNew.Components.ClientInfo;
using System.IO;
using System.Web.UI;
using POLMDTO;
using System.Collections.Generic;
using System.Collections;


namespace HTP.Backroom
{
    /// <summary>
    ///     Polm Report to Get all consultation request added and manage them.
    /// </summary>
    public partial class PolmReport : WebComponents.BasePage
    {
        #region Declaration

        readonly PolmController _polmController = new PolmController();
        readonly RoleBasedSecurityController _roleBasedSecurityController = new RoleBasedSecurityController();
        //Fahad 6766 03/15/2010 Implemented Configuration Service into POLM-Declared variable for using Configuration Service
        readonly ConfigurationKeys _objConfigurationKeys = new ConfigurationKeys();
        const int HoutonSourceId = 1;
        private const int PolmSecondaryUser = 4;
        private const int PolmPrimaryUser = 3;
        private const int ForwardedtoAttorneyCaseStatus = 9;


        #endregion

        #region Events

        /// <summary>
        ///     Event fire on page load
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Checking Access Type if Not Primary then redirecting to Login Error Page
                if (AccessType != 2)
                {
                    Response.Redirect("~/LoginAccesserror.aspx", false);
                }
                else
                {
                    if (!IsPostBack)
                    {
                        // Make File Upload Read Only.
                        fpAttachment.Attributes.Add("onkeypress", "return false;");
                        fpAttachment.Attributes.Add("onkeyup", "return false;");
                        fpAttachment.Attributes.Add("onpaste", "return false;");

                        //Call Role Based Security Controller method to get User by Traffic Program ID and assigned to the List.
                        var tpuserList = _roleBasedSecurityController.GetUserByTpId(new RoleBasedSecurity.DataTransferObjects.UserDto { TpUserId = EmpId });
                        //Setting Userid in View State for further Use.
                        ViewState["LoginEmployeeId"] = tpuserList[0].UserId;
                        //Setting user full name in View State for further Use.
                        ViewState["LoginEmployeeName"] = tpuserList[0].FullName;

                        //Setting Abbreviation in View State for further Use.
                        ViewState["Abbreviation"] = tpuserList[0].Abbreviation;

                        //Setting Rqange for DOB
                        rv_Dob.MinimumValue = Convert.ToDateTime("01/01/1910").ToShortDateString();
                        rv_Dob.MaximumValue = DateTime.Today.AddDays(-1).ToShortDateString();

                        FilCaseStatusMain();
                        FillDivisionOuter();
                        FillAttorney();
                        FillGrid(null, (ProspectReportSearchDto)Session["saveSearch"]);
                        FillPopUpDropdown();
                    }
                    Pagingctrl.PageIndexChanged += Pagingctrl_PageIndexChanged;
                    Pagingctrl.PageSizeChanged += Pagingctrl_PageSizeChanged;
                    Pagingctrl.GridView = gvPolm;
                }
                

            }
            catch (Exception ex)
            {
                lblmessage.Visible = true;
                lblmessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        ///     Event fires when searching button pressed
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                lblmessage.Visible = false;
                FillGrid(null, null);
            }
            catch (Exception ex)
            {
                lblmessage.Visible = true;
                lblmessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.Url.ToString());
        }

        /// <summary>
        ///     Sorting event
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">GridViewSortEventArgs</param>
        protected void gvPolm_Sorting(object sender, GridViewSortEventArgs e)
        {
            //SortGrid(e.SortExpression);
            try
            {
                var sortExpression = e.SortExpression;

                if (GridViewSortDirection == SortDirection.Ascending && sortExpression == GridViewSortExpression)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                else
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }

                GridViewSortExpression = sortExpression;
                FillGrid(null, null);
                Pagingctrl.PageCount = gvPolm.PageCount;
                Pagingctrl.PageIndex = gvPolm.PageIndex;

            }
            catch (Exception ex)
            {
                lblmessage.Visible = true;
                lblmessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        ///     Event fires on grid paging
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">GridViewPageEventArgs</param>
        protected void gvPolm_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvPolm.PageIndex = e.NewPageIndex;
                FillGrid(null, null);
            }
            catch (Exception ex)
            {
                lblmessage.Visible = true;
                lblmessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        ///     Event fires on Gird row command
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">GridViewCommandEventArgs</param>
        protected void gvPolm_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "click")
                {
                    lblcomments.Text =
                        lblCommentsHistory.Text =
                        lblFileHistory.Text = lblmessage.Text = lblPopupMessage.Text = lblQuicklegal.Text = string.Empty;
                    ViewState["clientId"] = e.CommandArgument;

                    var prospectDto = _polmController.GetProspect(Convert.ToInt32(e.CommandArgument));
                    SetData(prospectDto);
                    ModalPopupExtender1.Show();
                }
            }
            catch (Exception ex)
            {
                lblmessage.Visible = true;
                lblmessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        ///     Division drop down change index
        /// </summary>
        /// <param name="sender">object </param>
        /// <param name="e">EventArgs</param>
        protected void ddDivision_SelectedIndexChanged(object sender, EventArgs e)
        {
            //try
            //{
            //    var ddvalue = Convert.ToInt32(ddDivision.SelectedValue);
            //    FillQlmd(ddvalue);
            //    FillAttorney(ddvalue);
            //}
            //catch (Exception ex)
            //{
            //    lblmessage.Visible = true;
            //    lblmessage.Text = ex.Message;
            //    clsLogger.ErrorLog(ex);
            //}

        }

        /// <summary>
        ///     Event fires when button pressed from ajax popup to update information.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void BtnSubmitInfo_Click(object sender, EventArgs e)
        {
            try
            {
                SetLabelVIsibility();
                if (Page.IsValid)
                {

                    lblCommentsHistory.Text = String.Empty;
                    var prospectDto = SetProspect();
                    if (fpAttachment.HasFile)
                    {
                        var saveDirectory = _objConfigurationKeys.FileSaveLocation;
                        var storeFileName = DateTime.Now.ToFileTime().ToString();
                        prospectDto.FileOriginalName = fpAttachment.FileName;
                        prospectDto.FileStoredName = storeFileName + Path.GetExtension(fpAttachment.FileName);
                        prospectDto.FileUploadedBy = Convert.ToInt32(ViewState["LoginEmployeeId"]);
                        if (!Directory.Exists(saveDirectory))
                            Directory.CreateDirectory(saveDirectory);
                        if (!File.Exists(saveDirectory + prospectDto.FileStoredName + Path.GetExtension(fpAttachment.FileName)))
                            fpAttachment.SaveAs(saveDirectory + storeFileName + Path.GetExtension(fpAttachment.FileName));
                    }

                    if (_polmController.UpdateProspect(prospectDto))
                    {
                        AddComments();
                        lblPopupMessage.Visible = true;
                        lblPopupMessage.Text = "Prospect updated successfully.";
                        //Binding Main Report grid
                        FillGrid(null, null);

                        var prospectDtoUpdated = _polmController.GetProspect(Convert.ToInt32(prospectDto.ClientInfo.ClientId));
                        SetData(prospectDtoUpdated);

                        //Binding File History grid
                        FillAttachments(prospectDto);


                    }
                    else
                    {
                        lblPopupMessage.Visible = true;
                        lblPopupMessage.Text = "Prospect not updated successfully.";
                    }
                }
                else
                {
                    lblPopupMessage.Visible = false;
                    lblPopupMessage.Text = "Validation required";
                    vSummary.ShowSummary = true;
                }
            }
            catch (Exception ex)
            {
                lblPopupMessage.Visible = true;
                lblPopupMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
            finally
            {
                ModalPopupExtender1.Show();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="prospectDto"></param>
        private void FillAttachments(ProspectDto prospectDto)
        {
            var returnFileList = _polmController.GetAttachments(Convert.ToInt32(prospectDto.ClientInfo.ClientId));
            // if prospect has any attachments then display grid else hide grid
            if (returnFileList != null)
            {
                GridFileHistory.DataSource = returnFileList;
                GridFileHistory.DataBind();
                GridFileHistory.Visible = true;
            }
            else
            {
                GridFileHistory.Visible = false;
            }
        }

        /// <summary>
        ///     Events fire when button pressed to add comments
        /// </summary>
        private void AddComments()
        {

            lblcomments.Text = string.Empty;
            lblCommentsHistory.Text = string.Empty;
            if (!string.IsNullOrEmpty(txtUserComments.Text.Trim()))
            {
                var prospectDto = new ProspectDto();
                prospectDto.ClientInfo = new ClientInfoDto();
                prospectDto.CaseInfo = new CaseInfoDto();
                prospectDto.UserComments = Server.HtmlEncode(txtUserComments.Text) + " (" + DateTime.Now + " - " +
                                           ViewState["Abbreviation"] + ")";
                prospectDto.InsertedCommentsUserId = ViewState["LoginEmployeeId"].ToString();
                prospectDto.ClientInfo.ClientId = Convert.ToInt32(ViewState["clientId"]);
                prospectDto.CaseInfo.SourceId = HoutonSourceId;
                if (_polmController.AddComments(prospectDto))
                {
                    txtUserComments.Text = string.Empty;
                    lblcomments.Text = "Comments added successfully.";
                    var returnList =
                        _polmController.GetUserComments(Convert.ToInt32(ViewState["clientId"]));
                    if (returnList.Count > 0)
                    {
                        GridCommentsHistory.DataSource = returnList;
                        GridCommentsHistory.DataBind();
                        GridCommentsHistory.Visible = true;
                    }
                    else
                    {
                        lblCommentsHistory.Text = "No Records Found.";
                        GridCommentsHistory.Visible = false;
                    }
                }
                else
                {
                    lblcomments.Text = "Comments not added successfully";
                }
                ModalPopupExtender1.Show();
            }

        }

        /// <summary>
        ///     Event fire when user click on edit or delete icon in File grid
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">GridViewCommandEventArgs</param>
        protected void GridFileHistory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                lblPopupMessage.Visible = false;
                var path = _objConfigurationKeys.FileSaveLocation;
                var fileName = ((HiddenField)GridFileHistory.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("hfStoreFileName")).Value;
                var filePath = path + fileName;
                var existFile = File.Exists(filePath);

                switch (e.CommandName)
                {
                    case "editimage":
                        Response.ClearContent();
                        Response.ClearHeaders();
                        if (existFile)
                        {
                            var fs = new FileStream(filePath, FileMode.Open);

                            // which will force the open/cance/save dialog to show, to the header
                            Response.AddHeader("Content-Disposition", "attachment; filename=" + filePath);

                            // Add the file size into the response header
                            Response.AddHeader("Content-Length", fs.Length.ToString());
                            fs.Flush();
                            fs.Close();
                            Response.ContentType = "application/octet-stream";
                            Response.WriteFile(filePath);
                            Response.Flush();
                            Response.Close();
                        }
                        else
                        {
                            lblPopupMessage.Visible = true;
                            lblPopupMessage.Text = "File not Found.";
                        }
                        break;
                    case "deleteimage":
                        {
                            // set prospectDto object and fill properties
                            var prospectDto = new ProspectDto
                            {
                                AttachmentId = Convert.ToInt32(((HiddenField)GridFileHistory.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("hfID")).Value),
                                CaseInfo = new CaseInfoDto { FiledBy = Convert.ToInt32(ViewState["LoginEmployeeId"]), SourceId = HoutonSourceId }
                            };

                            // Delete selected attachment from DB
                            if (_polmController.DeleteAttachments(prospectDto))
                            {
                                // Get Prospect information and fill file grid again.
                                var prospectDtoInner = _polmController.GetProspect(Convert.ToInt32(ViewState["clientId"]));
                                //Fill File History Grid
                                FillFileHistoryGrid(prospectDtoInner);
                                lblmessage.Text = "File deleted successfully.";
                            }
                            //Delete File if Exist
                            if (existFile)
                            {
                                File.Delete(filePath);
                            }

                        }
                        break;
                }
                //Display Popup 
                ModalPopupExtender1.Show();
            }
            catch (Exception ex)
            {
                lblPopupMessage.Visible = true;
                lblPopupMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        ///     Event fire when File gird binds
        /// </summary>
        /// <param name="sender">object </param>
        /// <param name="e">GridViewRowEventArgs</param>
        protected void GridFileHistory_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                lblPopupMessage.Visible = false;
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ((ImageButton)e.Row.FindControl("ImgView")).CommandArgument = e.Row.RowIndex.ToString();
                    ((ImageButton)e.Row.FindControl("ImgDelete")).CommandArgument = e.Row.RowIndex.ToString();
                    ScriptManager.GetCurrent(Page).RegisterPostBackControl(e.Row.FindControl("ImgView"));
                }
            }
            catch (Exception ex)
            {
                lblPopupMessage.Visible = true;
                lblPopupMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void gvPolm_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.DataRow) return;
            var controlName = e.Row.FindControl("HfCommentMsg").NamingContainer.ClientID;
            ((Image)e.Row.FindControl("ImgCommnetsMsg")).Attributes.Add("OnMouseOver",
                                                                        "CommentMsgPoupup('" + controlName +
                                                                        "_HfCommentMsg" + "');");
            ((Image)e.Row.FindControl("ImgCommnetsMsg")).Attributes.Add("OnMouseOut", "CursorIcon2()");
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Fill Grid
        /// </summary>
        /// <param name="refAttorneyName">refAttorneyName</param>
        private void FillGrid(string refAttorneyName, ProspectReportSearchDto prospectReportSearchDto)
        {
            ProspectReportSearchDto prospectDto;

            if (prospectReportSearchDto != null)
            {
                prospectDto = prospectReportSearchDto;
                calstartdate.SelectedDate = (prospectDto.AssignStartDate == null) ? DateTime.Today : Convert.ToDateTime(prospectDto.AssignStartDate);
                calenddate.SelectedDate = (prospectDto.AssignEndDate == null) ? DateTime.Today : Convert.ToDateTime(prospectDto.AssignEndDate);
                ddCaseStatus.SelectedValue = (prospectDto.ProspectCaseStatusId == null) ? "-1" : Convert.ToString(prospectDto.ProspectCaseStatusId);
                ddAttorney.SelectedValue = (prospectDto.ProspectCaseAssignAttorney == null) ? "-1" : Convert.ToString(prospectDto.ProspectCaseAssignAttorney);

                if (prospectDto.ProspectCaseDivision == null)
                    ddDivisionOuter.SelectedIndex = 0;
                else
                    ddDivisionOuter.Items.FindByText(Convert.ToString(prospectDto.ProspectCaseDivision)).Selected = true;

                chkShowAll.Checked = prospectDto.ShowAll;
            }
            else
            {
                prospectDto = new ProspectReportSearchDto();

                if (string.IsNullOrEmpty(Convert.ToString(calstartdate.SelectedDate)))
                    prospectDto.AssignStartDate = null;
                else
                    prospectDto.AssignStartDate = calstartdate.SelectedDate;

                if (string.IsNullOrEmpty(Convert.ToString(calenddate.SelectedDate)))
                    prospectDto.AssignEndDate = null;
                else
                    prospectDto.AssignEndDate = calenddate.SelectedDate;

                if (Convert.ToString(ddCaseStatus.SelectedValue) == "-1")
                    prospectDto.ProspectCaseStatusId = null;
                else
                    prospectDto.ProspectCaseStatusId = Convert.ToInt32(ddCaseStatus.SelectedValue);

                if (string.IsNullOrEmpty(ddAttorney.SelectedValue) || ddAttorney.SelectedValue == "-1")
                    prospectDto.ProspectCaseAssignAttorney = null;
                else
                    prospectDto.ProspectCaseAssignAttorney = ddAttorney.SelectedValue;

                if (string.IsNullOrEmpty(ddDivisionOuter.SelectedValue) || ddDivisionOuter.SelectedValue == "-1")
                    prospectDto.ProspectCaseDivision = null;
                else
                    prospectDto.ProspectCaseDivision = ddDivisionOuter.SelectedItem.ToString();


                prospectDto.ShowAll = chkShowAll.Checked;
                prospectDto.ProspectFullName = null;
                prospectDto.LastName = null;
                prospectDto.ProspectCaseFileDate = null;
                prospectDto.ProspectCaseAssignDate = null;
                prospectDto.ProspectLastCaseStatusUpdateDate = null;
                prospectDto.ProspectCaseFollowUpDate = null;
                prospectDto.ProspectCaseSource = null;
                prospectDto.ProspectCaseReffAttorneyName = string.IsNullOrEmpty(refAttorneyName)
                                                               ? null
                                                               : refAttorneyName;

                // Give super user rights so that HTP can show all users.
                List<RoleBasedSecurity.DataTransferObjects.UserDto> userDtoList =
                    _roleBasedSecurityController.GetUserByRole(1, 2);

                if (userDtoList.Count > 0)
                    prospectDto.UserId = userDtoList[0].UserId;

                Session["saveSearch"] = prospectDto;
            }
            var returnList = _polmController.GetProspectReport(prospectDto);

            if (returnList.Count > 0)
            {
                gvPolm.Visible = true;
                gvPolm.DataSource = returnList;
                gvPolm.DataBind();
                Pagingctrl.GridView = gvPolm;
                Pagingctrl.PageCount = gvPolm.PageCount;
                Pagingctrl.PageIndex = gvPolm.PageIndex;
                Pagingctrl.SetPageIndex();
                lblmessage.Visible = false;
                Pagingctrl.Visible = true;

            }
            else
            {
                gvPolm.PageIndex = 0;
                gvPolm.PageSize = 20;
                lblmessage.Visible = true;
                gvPolm.Visible = false;
                lblmessage.Text = "No Records Found";
                Pagingctrl.Visible = false;
            }
        }

        /// <summary>
        ///     Method use to perform operation on page index changed of paging control.
        /// </summary>
        void Pagingctrl_PageIndexChanged()
        {
            gvPolm.PageIndex = Pagingctrl.PageIndex - 1;
            FillGrid(null, null);
        }

        /// <summary>
        ///     Method use to perform operation on page size changed of paging control.
        /// </summary>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            if (pageSize > 0)
            {
                gvPolm.PageIndex = 0;
                gvPolm.PageSize = pageSize;
                gvPolm.AllowPaging = true;
            }
            else
            {
                gvPolm.AllowPaging = false;
            }
            FillGrid(null, null); ;
        }

        /// <summary>
        ///     Fill Atotrney Dropdown
        /// </summary>
        private void FillAttorney()
        {
            // Get all active users of polm aplication.
            var listData = _roleBasedSecurityController.GetAllUsers();
            // if list is not null or list count is > 0 then bind dropdown
            if (listData != null && listData.Count > 0)
            {
                ddAttorney.DataSource = listData;
                ddAttorney.DataBind();
            }
            // Check the existance of choose in dropdown.
            var listItem = ddAttorney.Items.FindByValue("-1");
            // if chooose already added then dont add it again.
            if (listItem == null)
                ddAttorney.Items.Insert(0, new ListItem("--- Choose ---", "-1"));
            if (ddAttorney.Items.FindByValue("0") == null)
                ddAttorney.Items.Insert(1, new ListItem("Unassigned", "0"));
            if (ddAttorney.Items.FindByValue("01") == null)
                ddAttorney.Items.Insert(2, new ListItem("Assigned", "01"));
        }

        /// <summary>
        ///     Fill Case Status Drop Down.
        /// </summary>
        private void FilCaseStatusMain()
        {
            ddCaseStatus.DataSource = _polmController.GetAllCaseStatus(true);
            ddCaseStatus.DataBind();
            ddCaseStatus.Items.Insert(0, new ListItem("--- Choose ---", "-1"));
            ddCaseStatus.SelectedValue = "8";
        }

        /// <summary>
        ///     Set Sort Direction
        /// </summary>
        public SortDirection GridViewSortDirection
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Ascending;
                return (SortDirection)ViewState["sortDirection"];
            }
            set { ViewState["sortDirection"] = value; }

        }

        /// <summary>
        ///     Set Sort Expression
        /// </summary>
        private string GridViewSortExpression
        {
            get
            {
                if (ViewState["sortExpression"] == null)
                    ViewState["sortExpression"] = "";
                return ViewState["sortExpression"].ToString();
            }
            set { ViewState["sortExpression"] = value; }

        }

        /// <summary>
        ///     Set Prospect Object
        /// </summary>
        /// <param name="prospectDto">prospectDto</param>
        private void SetData(ProspectDto prospectDto)
        {
            try { txtfisrtname.Text = prospectDto.ClientInfo.FirstName; }
            catch { txtfisrtname.Text = string.Empty; }

            try { Txtmiddlename.Text = prospectDto.ClientInfo.MiddleName; }
            catch { Txtmiddlename.Text = string.Empty; }

            try { txtLastname.Text = prospectDto.ClientInfo.LastName; }
            catch { txtLastname.Text = string.Empty; }

            try { txtContactnumber1.Text = prospectDto.ClientInfo.ContactNumber1; }
            catch { txtContactnumber1.Text = string.Empty; }

            try { txtContactnumber2.Text = prospectDto.ClientInfo.ContactNumber2; }
            catch { txtContactnumber2.Text = string.Empty; }

            try { txtContactnumber3.Text = prospectDto.ClientInfo.ContactNumber3; }
            catch { txtContactnumber3.Text = string.Empty; }

            try { TxtEmail.Text = prospectDto.ClientInfo.EmailAddress; }
            catch { TxtEmail.Text = string.Empty; }

            try { TxtDL.Text = prospectDto.ClientInfo.DlNumber; }
            catch { TxtDL.Text = string.Empty; }

            try { txtAddress.Text = prospectDto.ClientInfo.Address; }
            catch { txtAddress.Text = string.Empty; }



            try { TxtFeeInformation.Text = prospectDto.CaseInfo.QuoteandFeeInformation; }
            catch { TxtFeeInformation.Text = string.Empty; }

            try { lblQuicklegal.Text = prospectDto.CaseInfo.LegalMetterDescription; }
            catch { lblQuicklegal.Text = string.Empty; }

            DateTime? defaultdate = new DateTime(1900, 01, 01);
            try { txtDOB.Text = (prospectDto.ClientInfo.Dob == null || prospectDto.ClientInfo.Dob == defaultdate) ? string.Empty : Convert.ToDateTime(prospectDto.ClientInfo.Dob).ToString("MM/dd/yyyy"); }
            catch { txtDOB.Text = string.Empty; }

            try
            {
                // If prospect has some information that not exists on Drop down then show that text on label
                SetAssignedValue(ddContactType1, lblContactType1, Convert.ToString(prospectDto.ClientInfo.ContactType1),
                                 prospectDto.ClientInfo.ContactTypeDescription1, td_Contact1, td_ContactLabel1, td_ContactImage1);

            }
            catch
            {
                ddContactType1.SelectedIndex = 0;
                td_Contact1.Style[HtmlTextWriterStyle.Display] = "";
                td_ContactLabel1.Style[HtmlTextWriterStyle.Display] = "";
                td_ContactImage1.Style[HtmlTextWriterStyle.Display] = "";
            }



            try
            {
                // If prospect has some information that not exists on Drop down then show that text on label
                SetAssignedValue(ddContactType2, lblContact2, Convert.ToString(prospectDto.ClientInfo.ContactType2),
                                 prospectDto.ClientInfo.ContactTypeDescription2, td_Contact2, td_ContactLabel2, td_ContactImage2);

            }
            catch
            {
                ddContactType2.SelectedIndex = 0;
                td_Contact2.Style[HtmlTextWriterStyle.Display] = "";
                td_ContactLabel2.Style[HtmlTextWriterStyle.Display] = "";
                td_ContactImage2.Style[HtmlTextWriterStyle.Display] = "";
            }


            try
            {
                // If prospect has some information that not exists on Drop down then show that text on label
                SetAssignedValue(ddContactType3, lblContactType3, Convert.ToString(prospectDto.ClientInfo.ContactType3),
                                 prospectDto.ClientInfo.ContactTypeDescription3, td_Contact3, td_ContactLabel3, td_ContactImage3);

            }
            catch
            {
                ddContactType3.SelectedIndex = 0;
                td_Contact3.Style[HtmlTextWriterStyle.Display] = "";
                td_ContactLabel3.Style[HtmlTextWriterStyle.Display] = "";
                td_ContactImage3.Style[HtmlTextWriterStyle.Display] = "";
            }


            try
            {
                // If prospect has some information that not exists on Drop down then show that text on label
                SetAssignedValue(ddlanguages, lbllanguage, Convert.ToString(prospectDto.ClientInfo.Language),
                                 prospectDto.ClientInfo.LanguageDescription, td_language, td_languageLabel, td_languageImage);

            }
            catch
            {
                ddlanguages.SelectedIndex = 0;
                td_language.Style[HtmlTextWriterStyle.Display] = "";
                td_languageLabel.Style[HtmlTextWriterStyle.Display] = "";
                td_languageImage.Style[HtmlTextWriterStyle.Display] = "";
            }

            try
            {
                // If prospect has some information that not exists on Drop down then show that text on label
                SetAssignedValue(ddcasestatusinner, lblCaseStatus, Convert.ToString(prospectDto.CaseInfo.CaseStatus),
                                 prospectDto.CaseInfo.CaseStatusDescription, td_caseStatus, td_caseStatusLabel, td_caseStatusImage);
                ViewState["casestatus"] = ddcasestatusinner.SelectedValue;
            }
            catch
            {
                ddcasestatusinner.SelectedIndex = 0;
                td_caseStatus.Style[HtmlTextWriterStyle.Display] = "";
                td_caseStatusLabel.Style[HtmlTextWriterStyle.Display] = "";
                td_caseStatusImage.Style[HtmlTextWriterStyle.Display] = "";
            }



            try
            {
                // If prospect has some information that not exists on Drop down then show that text on label
                SetAssignedValue(ddDivision, lblDivision, Convert.ToString(prospectDto.CaseInfo.Division),
                                 prospectDto.CaseInfo.DivisionDescription, td_Division, td_DivisionLabel, td_DivisionLabelImage);
            }
            catch
            {
                ddDivision.SelectedIndex = 0;
                td_Division.Style[HtmlTextWriterStyle.Display] = "";
                td_DivisionLabel.Style[HtmlTextWriterStyle.Display] = "";
                td_DivisionLabelImage.Style[HtmlTextWriterStyle.Display] = "";
            }

            try
            {
                // If prospect has some information that not exists on Drop down then show that text on label
                SetAssignedValue(ddAttorneyinner, lblAttorney, Convert.ToString(prospectDto.CaseInfo.AssigendAttorney),
                                 prospectDto.CaseInfo.AssigendAttorneyDescription, td_Attorney, td_AttorneyLabel, td_AttorneyImage);
            }
            catch
            {
                ddAttorneyinner.SelectedIndex = 0;
                td_Attorney.Style[HtmlTextWriterStyle.Display] = "";
                td_AttorneyLabel.Style[HtmlTextWriterStyle.Display] = "";
                td_AttorneyImage.Style[HtmlTextWriterStyle.Display] = "";
            }


            try
            {
                // If prospect has some information that not exists on Drop down then show that text on label
                SetAssignedValue(ddcasetype, lblCaseType, Convert.ToString(prospectDto.CaseInfo.QuickLegalMatterDescription),
                                 prospectDto.CaseInfo.QuickLegalMatterDescriptionText, td_caseType, td_caseTypeLabel, td_CaseTypeImage);
            }
            catch
            {
                ddcasetype.SelectedIndex = 0;
                td_caseType.Style[HtmlTextWriterStyle.Display] = "";
                td_caseTypeLabel.Style[HtmlTextWriterStyle.Display] = "";
                td_CaseTypeImage.Style[HtmlTextWriterStyle.Display] = "";
            }

            try
            {
                // If prospect has some information that not exists on Drop down then show that text on label
                SetAssignedValue(ddDamageValue, lblDamageValue, Convert.ToString(prospectDto.CaseInfo.DamageValue),
                                 prospectDto.CaseInfo.DamageValueDescription, td_DamageValue, td_DamageValueLabel, td_DamageValueImage);
            }
            catch
            {
                ddDamageValue.SelectedIndex = 0;
                td_DamageValue.Style[HtmlTextWriterStyle.Display] = "";
                td_DamageValueLabel.Style[HtmlTextWriterStyle.Display] = "";
                td_DamageValueImage.Style[HtmlTextWriterStyle.Display] = "";
            }

            try
            {
                // If prospect has some information that not exists on Drop down then show that text on label
                SetAssignedValue(ddBodilyDamages, lblbodilyDamage, Convert.ToString(prospectDto.CaseInfo.BodilyDamage),
                                 prospectDto.CaseInfo.BodilyDamageDescription, td_BodilyDamage, td_BodilyDamageLabel, td_BodilyDamageImage);
            }
            catch
            {
                ddBodilyDamages.SelectedIndex = 0;
                td_BodilyDamage.Style[HtmlTextWriterStyle.Display] = "";
                td_BodilyDamageLabel.Style[HtmlTextWriterStyle.Display] = "";
                td_BodilyDamageImage.Style[HtmlTextWriterStyle.Display] = "";
            }


            FillFileHistoryGrid(prospectDto);
            FillCommentsHistoryGrid(prospectDto);

        }

        /// <summary>
        ///     This method set dropdown values. If values in database doesnot matches with dropdown values then hide dropdown and show label with image
        /// </summary>
        /// <param name="ddList">Dropdown on which values will be set</param>
        /// <param name="label">Label on which text will be shown</param>
        /// <param name="textToCheck">Text to check with dropdown</param>
        /// <param name="dropDownColumn">Column in which drop down exists.</param>
        /// <param name="imageColumn">Column in which image exists.</param>
        private void SetAssignedValue(DropDownList ddList, Label label, string textToCheck, string textToShowOnLabel, HtmlTableCell dropDownColumn, HtmlTableCell imageLabel, HtmlTableCell imageColumn)
        {
            if (textToCheck == "0" || string.IsNullOrEmpty(textToCheck))
                textToCheck = "-1";
            ListItem listItem = ddList.Items.FindByValue(textToCheck.Trim());

            if (listItem == null)
            {
                label.Text = string.IsNullOrEmpty(textToShowOnLabel) ? "N/A" : textToShowOnLabel;
                dropDownColumn.Style[HtmlTextWriterStyle.Display] = "none";
                imageLabel.Style[HtmlTextWriterStyle.Display] = "";
                imageColumn.Style[HtmlTextWriterStyle.Display] = "";
            }
            else
            {
                imageColumn.Style[HtmlTextWriterStyle.Display] = "none";
                imageLabel.Style[HtmlTextWriterStyle.Display] = "none";
                dropDownColumn.Style[HtmlTextWriterStyle.Display] = "";
                ddList.SelectedValue = textToCheck.Trim();
            }
        }

        /// <summary>
        ///     Fill Grid which will hold Rep Comments
        /// </summary>
        /// <param name="prospectDto">ProspectDto</param>
        private void FillCommentsHistoryGrid(ProspectDto prospectDto)
        {
            try
            {
                if (prospectDto.CommentsHistory != null && prospectDto.CommentsHistory.Count > 0)
                {
                    lblCommentsHistory.Text = string.Empty;
                    GridCommentsHistory.Visible = true;
                    GridCommentsHistory.DataSource = prospectDto.CommentsHistory;
                    GridCommentsHistory.DataBind();
                }
                else
                {
                    GridCommentsHistory.Visible = false;
                    lblCommentsHistory.Text = "No Records Found.";
                }

            }
            catch (Exception ex)
            {
                GridCommentsHistory.Visible = false;
                lblCommentsHistory.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        ///     Fill Grid which will hold Client File History
        /// </summary>
        /// <param name="prospectDto">ProspectDto</param>
        private void FillFileHistoryGrid(ProspectDto prospectDto)
        {
            try
            {
                if (prospectDto.FileUploadHistory != null && prospectDto.FileUploadHistory.Count > 0)
                {
                    GridFileHistory.Visible = true;
                    GridFileHistory.DataSource = prospectDto.FileUploadHistory;
                    GridFileHistory.DataBind();
                }
                else
                {
                    //lblFileHistory.Text = "No Records Found.";
                    GridFileHistory.Visible = false;
                }

            }
            catch (Exception ex)
            {
                lblmessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
                GridFileHistory.Visible = false;
            }
        }

        /// <summary>
        ///     Fill Quick Legal Matter Description Drop Down in popup
        /// </summary>
        private void FillQlmd()
        {
            try
            {
                var qlmdController = new PolmController();
                ddcasetype.DataSource = qlmdController.GetAllQuickLegalMatterDescriptionOfSource(HoutonSourceId);
                ddcasetype.DataBind();
            }
            catch { ddcasetype.DataSource = null; }
            finally { ddcasetype.Items.Insert(0, new ListItem("--- Choose ---", "-1")); }
        }

        /// <summary>
        ///     Fill attorney Drop Down in popup
        /// </summary>
        /// <param name="division">Division Id</param>
        private void FillAttorneyInner()
        {
            // Get all active users of polm aplication.
            var listData = _roleBasedSecurityController.GetAllUsers();
            // if list is not null or list count is > 0 then bind dropdown
            if (listData != null && listData.Count > 0)
            {
                ddAttorneyinner.DataSource = listData;
                ddAttorneyinner.DataBind();
            }
            // Check the existance of choose in dropdown.
            var listItem = ddAttorneyinner.Items.FindByValue("-1");
            // if chooose already added then dont add it again.
            if (listItem == null)
                ddAttorneyinner.Items.Insert(0, new ListItem("--- Choose ---", "-1"));
        }

        /// <summary>
        ///     Mail Method to fill all Dropdown in popup
        /// </summary>
        private void FillPopUpDropdown()
        {
            FillLanguageData();
            FillContactTypeData();
            FillDivision();
            FillCaseStatusInner();
            FillBodilyDamage();
            FillDamageValue();
            FillAttorneyInner();
            FillQlmd();

        }

        /// <summary>
        ///     Fill Language Drop Down in popup
        /// </summary>
        private void FillLanguageData()
        {
            var returnList = _polmController.GetAllLanguage(true);
            SetDropDown(ddlanguages, returnList);
        }

        /// <summary>
        ///     Fill Contact Type Drop Down in popup
        /// </summary>
        private void FillContactTypeData()
        {
            var contactTypeController = new PolmController();
            var returnList = contactTypeController.GetAllContactType(true);
            SetDropDown(ddContactType1, returnList);
            SetDropDown(ddContactType2, returnList);
            SetDropDown(ddContactType3, returnList);
        }

        /// <summary>
        ///     Common method to set dropdown Data Source to aovid code repitition
        /// </summary>
        /// <param name="dropDownId"></param>
        /// <param name="listData"></param>
        private void SetDropDown(DropDownList dropDownId, List<PolmDto> listData)
        {
            dropDownId.DataSource = listData;
            dropDownId.DataBind();
            dropDownId.Items.Insert(0, new ListItem("--- Choose ---", "-1"));
        }

        /// <summary>
        ///     Fill Division Drop down in popup
        /// </summary>
        private void FillDivision()
        {

            var returnList = _roleBasedSecurityController.GetAllDivisions();
            if (returnList != null && returnList.Count > 0)
            {
                ddDivision.DataSource = returnList;
                ddDivision.DataBind();
            }
            ddDivision.Items.Insert(0, new ListItem("--- Choose ---", "-1"));

        }

        /// <summary>
        ///     Fill Division Drop down in report other than popup
        /// </summary>
        private void FillDivisionOuter()
        {
            var returnList = _roleBasedSecurityController.GetAllDivisions();
            if (returnList != null && returnList.Count > 0)
            {
                ddDivisionOuter.DataSource = returnList;
                ddDivisionOuter.DataBind();
            }
            ddDivisionOuter.Items.Insert(0, new ListItem("--- Choose ---", "-1"));
        }

        /// <summary>
        ///     Fill Caee Status Drop down in popup
        /// </summary>
        private void FillCaseStatusInner()
        {
            var returnList = _polmController.GetAllCaseStatus(true);
            SetDropDown(ddcasestatusinner, returnList);
        }

        /// <summary>
        ///     Fill Damage Value Drop down in popup
        /// </summary>
        private void FillDamageValue()
        {
            var returnList = _polmController.GetAllDamageValue(true);
            SetDropDown(ddDamageValue, returnList);
        }

        /// <summary>
        ///     Fill Bodily Damage Drop down in popup
        /// </summary>
        private void FillBodilyDamage()
        {
            var returnList = _polmController.GetAllBodilyDamageOfDivision(HoutonSourceId);
            ddBodilyDamages.DataSource = returnList;
            ddBodilyDamages.DataBind();
            ddBodilyDamages.Items.Insert(0, new ListItem("--- Choose ---", "-1"));
        }

        /// <summary>
        ///     Set Proespect Dto, filling them by extracting value from controls.
        /// </summary>
        /// <returns></returns>
        private ProspectDto SetProspect()
        {
            var prospectDto = new ProspectDto();
            prospectDto.CaseInfo = new CaseInfoDto();
            prospectDto.ClientInfo = new ClientInfoDto();

            prospectDto.ClientInfo.FirstName = txtfisrtname.Text;
            prospectDto.ClientInfo.MiddleName = Txtmiddlename.Text;
            prospectDto.ClientInfo.LastName = txtLastname.Text;
            prospectDto.ClientInfo.ContactNumber1 = txtContactnumber1.Text;
            prospectDto.ClientInfo.ContactType1 = SetDropDownValue(ddContactType1);
            prospectDto.ClientInfo.ContactNumber2 = txtContactnumber2.Text;
            prospectDto.ClientInfo.ContactType2 = SetDropDownValue(ddContactType2);
            prospectDto.ClientInfo.ContactType3 = SetDropDownValue(ddContactType3);
            prospectDto.ClientInfo.ContactNumber3 = txtContactnumber3.Text;
            prospectDto.ClientInfo.Language = SetDropDownValue(ddlanguages);
            prospectDto.ClientInfo.EmailAddress = TxtEmail.Text;
            prospectDto.ClientInfo.DlNumber = TxtDL.Text;
            prospectDto.ClientInfo.Address = txtAddress.Text;

            var updateBy = Convert.ToInt32(ViewState["LoginEmployeeId"]);
            var resultRole = false;
            if (Convert.ToInt32(ddAttorneyinner.SelectedValue) > 0)
            {
                // Get all roles of login user id
                var loginPrimaryRoleDto = _roleBasedSecurityController.GetUserRole(updateBy);

                // Check if login user is in secondary user role 
                var assignedRolePrimary = (loginPrimaryRoleDto == null) ? false : loginPrimaryRoleDto.FindAll(bk => bk.RoleId == PolmPrimaryUser).Count > 0;
                // Get all roles of login user id
                var loginSecondaryRoleDto = _roleBasedSecurityController.GetUserRole(Convert.ToInt32(ddAttorneyinner.SelectedValue));
                // Check if login user is in secondary user role
                var assignedRoleSecondary = (loginSecondaryRoleDto == null) ? false : loginSecondaryRoleDto.FindAll(bk => bk.RoleId == PolmSecondaryUser).Count > 0;
                resultRole = assignedRoleSecondary && assignedRolePrimary;
            }
            prospectDto.CaseInfo.CaseStatus = resultRole ? ForwardedtoAttorneyCaseStatus : SetDropDownValue(ddcasestatusinner);
            //prospectDto.CaseInfo.CaseStatus = SetDropDownValue(ddcasestatusinner);

            prospectDto.CaseInfo.QuickLegalMatterDescription = SetDropDownValue(ddcasetype);
            prospectDto.CaseInfo.DamageValue = SetDropDownValue(ddDamageValue);
            prospectDto.CaseInfo.Division = SetDropDownValue(ddDivision);
            prospectDto.CaseInfo.QuoteandFeeInformation = TxtFeeInformation.Text;
            prospectDto.CaseInfo.LegalMetterDescription = lblQuicklegal.Text;
            prospectDto.CaseInfo.AssigendAttorney = SetDropDownValue(ddAttorneyinner);

            if (!string.IsNullOrEmpty(txtDOB.Text))
                prospectDto.ClientInfo.Dob = Convert.ToDateTime(txtDOB.Text);
            else
                prospectDto.ClientInfo.Dob = null;

            if (prospectDto.CaseInfo.AssigendAttorney == null || prospectDto.CaseInfo.AssigendAttorney == Convert.ToInt32(ddAttorney.SelectedValue))
                prospectDto.CaseInfo.AssignedDate = null;
            else
                prospectDto.CaseInfo.AssignedDate = DateTime.Now;

            prospectDto.CaseInfo.SourceId = HoutonSourceId;
            prospectDto.CaseInfo.BodilyDamage = SetDropDownValue(ddBodilyDamages);

            prospectDto.CaseInfo.LastUpdatedBy = updateBy;

            DateTime? followupdate = null;
            if (hffollowupdate.Value == "1")
                followupdate = calFollowupDate.SelectedDate;

            prospectDto.CaseInfo.Followupdate = followupdate;
            prospectDto.CaseInfo.ReferringAttorneyId = null;
            prospectDto.CaseInfo.ClosingNoteId = null;

            if (Convert.ToString(ViewState["casestatus"]) != "6" && ddcasestatusinner.SelectedValue == "6")
                prospectDto.CaseInfo.ClosedDate = DateTime.Now;
            else
                prospectDto.CaseInfo.ClosedDate = null;

            if (Convert.ToInt32(ddcasestatusinner.SelectedValue) == prospectDto.CaseInfo.LastUpdatedBy)
                prospectDto.CaseInfo.LastCaseStatusUpdateDate = null;
            else
                prospectDto.CaseInfo.LastCaseStatusUpdateDate = DateTime.Now;

            prospectDto.ClientInfo.ClientId = Convert.ToInt32(ViewState["clientId"]);
            return prospectDto;
        }

        /// <summary>
        ///     Nullable integer check
        /// </summary>
        /// <param name="dropdown">Drop Down</param>
        /// <returns>Nullable int</returns>
        private int? SetDropDownValue(DropDownList dropdown)
        {
            if (dropdown.SelectedValue == "-1")
                return null;
            return Convert.ToInt32(dropdown.SelectedValue);
        }

        /// <summary>
        /// 
        /// </summary>
        private void SetLabelVIsibility()
        {
            var hashtable = new Hashtable
                                      {
                                          {"td_languageLabel", "RequiredFieldValidator1"},
                                          {"td_ContactLabel1", "RequiredFieldValidator3"},
                                          {"td_caseTypeLabel", "rfvcasetype"},
                                          {"td_DivisionLabel", "RfvDivision"}
                                      };
            foreach (DictionaryEntry item in hashtable)
            {
                Control tbControl = FindControl(Convert.ToString(item.Key));
                Control rfvControl = FindControl(Convert.ToString(item.Value));
                if (tbControl != null && rfvControl != null)
                {
                    if (tbControl.Visible)
                        ((RequiredFieldValidator)rfvControl).IsValid = true;
                }
            }
        }

        #endregion

    }
}

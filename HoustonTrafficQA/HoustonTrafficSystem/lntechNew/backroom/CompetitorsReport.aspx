<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CompetitorsReport.aspx.cs" Inherits="lntechNew.backroom.CompetitorsReport" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Competitors Report</title>
    <link href="../Styles.css" type="text/css" rel="stylesheet">
		<script src="../Scripts/Validationfx.js" type="text/javascript"></script>
		<script src="../Scripts/Dates.js" type="text/javascript"></script>
		<script language="javascript">
		
	      
	        function validate()
		    {
			    //a;
			    var d1 = document.getElementById("calQueryFrom").value
			    var d2 = document.getElementById("calQueryTo").value			
    			
			    if (d1 == d2)
				    return true;
			    if (compareDates(d2,'MM/dd/yyyy',d1,'MM/dd/yyyy')==false)
				{
					alert("start date cannot greater than end date");
					document.getElementById("calQueryTo").focus(); 
					return false;
				}
				return true;
		    }
	  
		</script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <TABLE id="TableMain" style="Z-INDEX: 200" cellSpacing="0" cellPadding="0" width="780"
				align="center" border="0">
				<tbody>
					<tr>
						<td style="HEIGHT: 14px" colSpan="4">
						<uc1:activemenu id="ActiveMenu1" runat="server"></uc1:activemenu>
						</td>
					</tr>
					<TR>
						<TD>
							<TABLE id="TableSub" cellSpacing="0" cellPadding="0" width="100%" border="0">
								<tr>
									<td background="../../images/separator_repeat.gif" style="height: 7"></td>
								</tr>
								<TR>
									<TD align="center" style="height: 50px">
										<TABLE id="Table6" cellSpacing="0" cellPadding="0" bgColor="white" border="0" style="width: 777px; height: 1px">
											<tr>
												<TD style="WIDTH: 257px; HEIGHT: 13px" width="257" height="13"><STRONG>&nbsp;Date 
														:&nbsp;</STRONG>&nbsp;
													<ew:calendarpopup id="calQueryFrom" runat="server" Nullable="True" Width="80px" ImageUrl="../images/calendar.gif"
														Font-Names="Tahoma" Font-Size="8pt" ControlDisplay="TextBoxImage" CalendarLocation="Left" ShowGoToToday="True"
														AllowArbitraryText="False" Culture="(Default)" UpperBoundDate="12/31/9999 23:59:00" PadSingleDigits="True"
														ToolTip="Call Back Date" ShowClearDate="True">
														<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
														<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></WeekdayStyle>
														<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="Yellow"></MonthHeaderStyle>
														<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
															BackColor="AntiqueWhite"></OffMonthStyle>
														<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></GoToTodayStyle>
														<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="LightGoldenrodYellow"></TodayDayStyle>
														<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="Orange"></DayHeaderStyle>
														<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="LightGray"></WeekendStyle>
														<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="Yellow"></SelectedDateStyle>
														<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></ClearDateStyle>
														<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></HolidayStyle>
													</ew:calendarpopup>&nbsp;-&nbsp;</TD>
												<TD style="HEIGHT: 13px" width="200"><ew:calendarpopup id="calQueryTo" runat="server" Nullable="True" Width="80px" ImageUrl="../images/calendar.gif"
														Font-Names="Tahoma" Font-Size="8pt" ControlDisplay="TextBoxImage" CalendarLocation="Left" ShowGoToToday="True" AllowArbitraryText="False"
														Culture="(Default)" UpperBoundDate="12/31/9999 23:59:00" PadSingleDigits="True" ToolTip="Call Back Date"
														ShowClearDate="True">
														<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
														<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></WeekdayStyle>
														<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="Yellow"></MonthHeaderStyle>
														<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
															BackColor="AntiqueWhite"></OffMonthStyle>
														<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></GoToTodayStyle>
														<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="LightGoldenrodYellow"></TodayDayStyle>
														<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="Orange"></DayHeaderStyle>
														<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="LightGray"></WeekendStyle>
														<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="Yellow"></SelectedDateStyle>
														<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></ClearDateStyle>
														<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></HolidayStyle>
													</ew:calendarpopup></TD>
												<TD style="HEIGHT: 13px" width="200">
                                                    <asp:RadioButton ID="rdbtn_Att" runat="server" Checked="True" CssClass="clslabelnew"
                                                        GroupName="att" Text="Attorney Tracker" /></TD>
												<TD style="HEIGHT: 13px" width="200">
                                                    <asp:RadioButton ID="rdbtn_bond" runat="server" CssClass="clslabelnew" GroupName="att"
                                                        Text="Bond Tracker" /></TD>
												<TD style="HEIGHT: 13px" width="200"></TD>
												<TD style="HEIGHT: 13px" align="left" width="80" height="13"><asp:button id="btn_submit" runat="server" CssClass="clsbutton" Text="Submit" OnClick="btn_submit_Click"></asp:button></TD>
												<td style="HEIGHT: 13px" align="right" width="10%" height="13">&nbsp;&nbsp;
												</td>
											</tr>
											<tr>
												<td width="780" background="../../images/separator_repeat.gif" colSpan="6" style="height: 6"></td>
												<TD width="780" background="../../images/separator_repeat.gif" style="height: 11px"></TD>
											</tr>
										</TABLE>
										<asp:label id="lbl_message" runat="server" CssClass="label" ForeColor="Red"></asp:label></TD>
								</TR>
								<TR>
									<TD>
										<TABLE id="TableGrid" cellSpacing="0" cellPadding="0" width="100%" bgColor="white" border="0">
											<tr>
												<td vAlign="top" align="center" colSpan="2"><asp:datagrid id="dg_valrep" runat="server" Width="100%" CssClass="clsLeftPaddingTable" 
														BackColor="#EFF4FB" AutoGenerateColumns="False" BorderColor="White" BorderStyle="None" OnItemDataBound="dg_valrep_ItemDataBound" AllowSorting="True" CellPadding="0" OnSortCommand="dg_valrep_SortCommand" OnItemCommand="dg_valrep_ItemCommand">
														<Columns>
															<asp:TemplateColumn HeaderText="name" SortExpression="name">
																<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
																<ItemStyle VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id="lbl_abbrev" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.name") %>'></asp:Label>
																</ItemTemplate>																
                                                                <HeaderTemplate>
                                                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="name">LinkButton</asp:LinkButton>
                                                                </HeaderTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="no" SortExpression="noofcases">
																<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
																<ItemStyle VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id="lbl_clickscount" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.noofcases") %>'></asp:Label>
																</ItemTemplate>
                                                                <HeaderTemplate>
                                                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="noofcases">LinkButton</asp:LinkButton>
                                                                </HeaderTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="clients" SortExpression="sumBond_clinents">
																<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
																<ItemStyle VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id="lbl_totalquote" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.sumBond_clients") %>'></asp:Label>
																</ItemTemplate>
                                                                <HeaderTemplate>
                                                                    <asp:LinkButton ID="LinkButton3" runat="server" CommandName="sumBond_clients">LinkButton</asp:LinkButton>
                                                                </HeaderTemplate>
															</asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="amount" SortExpression="AmountSum">
                                                                <HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
																<ItemStyle VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id="lbl_totalsum" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.AmountSum","{0:C}") %>'></asp:Label>
																</ItemTemplate>
                                                                <HeaderTemplate>
                                                                    <asp:LinkButton ID="LinkButton5" runat="server" CommandName="AmountSum">LinkButton</asp:LinkButton>
                                                                </HeaderTemplate>
                                                            </asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="percent" SortExpression="percentage">
																<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
																<ItemStyle VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id="lbl_totalclient" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.percentage") %>'></asp:Label>
																</ItemTemplate>
                                                                <HeaderTemplate>
                                                                    <asp:LinkButton ID="LinkButton4" runat="server" CommandName="percentage">LinkButton</asp:LinkButton>
                                                                </HeaderTemplate>
															</asp:TemplateColumn>
														</Columns>														
													</asp:datagrid></td>
											</tr>
											<tr>
												<td width="780" background="../../images/separator_repeat.gif"  height="11"></td>
											</tr>
											<TR>
												<TD ><uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
							</TABLE>
						</TD>
					</TR>
				</tbody>
			</TABLE>
    </div>
    </form>
</body>
</html>

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using System.Data.SqlClient;
using lntechNew.Components.ClientInfo;
//Waqas 5057 03/19/2009 Changed to HTP
namespace HTP.backroom
{
    /// <summary>
    /// Summary description for Admin.
    /// </summary>
    public partial class Admin : System.Web.UI.Page
    {
        protected System.Web.UI.WebControls.Label lblRecCount;
        protected System.Web.UI.WebControls.Label lblMessage;
        protected System.Web.UI.WebControls.DataGrid dg_results;
        protected System.Web.UI.HtmlControls.HtmlForm Form1;
        protected System.Web.UI.WebControls.Button btn_clear;
        protected System.Web.UI.WebControls.Button btn_update;
        protected System.Web.UI.WebControls.TextBox tb_lname;
        protected System.Web.UI.WebControls.TextBox tb_fname;
        protected System.Web.UI.WebControls.TextBox tb_abbrev;
        protected System.Web.UI.WebControls.TextBox tb_password;
        protected System.Web.UI.WebControls.DropDownList ddl_accesstype;
        protected System.Web.UI.WebControls.TextBox tb_uname;
        //Sabir Khan 5193 11/21/2008
        //static string employeeid = String.Empty;
        clsSession ClsSession = new clsSession();
        protected System.Web.UI.WebControls.CheckBox chkCanView;
        protected System.Web.UI.WebControls.TextBox txt_email;
        clsLogger clog = new clsLogger();
        clsUser cUser = new clsUser();
        clsFirms ClsFirms = new clsFirms();


        private void Page_Load(object sender, System.EventArgs e)
        {
            TD1.Style["visibility"] = "hidden";
            TD2.Style["visibility"] = "hidden";
            //Waqas 5057 03/17/2009 Checking employee info in session
            if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false)
            {
                Response.Redirect("../frmlogin.aspx", false);
            }
            else //To stop page further execution
            {
                if (ClsSession.GetCookie("sAccessType", this.Request).ToString() != "2")
                {
                    Response.Redirect("../LoginAccesserror.aspx", false);
                    Response.End();
                }
                else //To stop page further execution
                {

                    if (!IsPostBack)
                    {
                        //Fahad 5196 26/11/2008
                        FillddlFirms();
                        ddl_spnsearch.Attributes.Add("onchange", "return TD1TD2();");
                        // chkb_CanSPNSearch.Attributes.Add("onclick", "return TD1TD2();");

                        try
                        {
                            btn_update.Attributes.Add("onclick", "return formSubmit();");
                            //Sabir Khan 5193 11/21/2008 viewstate has been set empty for new user...
                            ViewState["EmployeeID"] = "";
                            // Display User Infomation In The Grid
                            bindGrid();
                        }
                        catch (Exception ex)
                        {
                            lblMessage.Text = "Cannot Find Record";
                            clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                        }
                    }
                }
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_clear.Click += new System.EventHandler(this.btn_clear_Click);
            this.dg_results.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_results_ItemCommand);
            this.dg_results.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dg_results_ItemDataBound);
            this.dg_results.SelectedIndexChanged += new System.EventHandler(this.dg_results_SelectedIndexChanged);
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion

        private void dg_results_SelectedIndexChanged(object sender, System.EventArgs e)
        {

        }

        private void dg_results_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {


            try
            {
                // Highlight Current DataGrid Row

                if (e.Item.ItemType == ListItemType.Header)
                {

                }
                else
                {
                    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                    {
                        e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='#FFF2D9'");
                    }

                    if (e.Item.ItemType == ListItemType.Item)
                    {
                        e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#EFF4FB'");
                    }
                    else
                    {
                        e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#EFF4FB'");
                    }
                }
            }
            catch (Exception ex)
            {
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

            }


        }

        public void bindGrid()
        {

            // Getting List of All Users 

            lblMessage.Text = "";
            //string[] keys = { "@status", "@orderby" };
            //object[] values = { rbl_status.SelectedValue, ddl_sort.SelectedValue };
            //DataSet ds = ClsDB.Get_DS_BySPArr("USP_HTS_list_all_users", keys, values);
            DataSet ds = cUser.GetAllUserList(rbl_status.SelectedValue, ddl_sort.SelectedValue);
            // Displaying List of Users In DataGrid

            if (ds.Tables[0].Rows.Count > 0)
            {
                dg_results.DataSource = ds;
                dg_results.DataBind();

            }
            else
            {
                lblMessage.Text = "No Record Found";
                dg_results.DataBind();
            }
        }
        private void ClearValues()
        {
            // Clearing ALL Text Boxes

            tb_fname.Text = "";
            tb_lname.Text = "";
            tb_abbrev.Text = "";
            tb_password.Text = "";
            tb_uname.Text = "";
            tb_NoofDays.Text = "";
            ViewState["EmployeeID"] = "";
            //employeeid = "";

            txt_email.Text = "";
            txt_SPNUserName.Text = "";
            txt_SPNPassword.Text = "";
            txt_NTUserID.Text = "";

            ddl_closeout.SelectedIndex = 0;
            ddl_spnsearch.SelectedIndex = 0;
            ddl_accesstype.SelectedIndex = 0;
            ddl_status.SelectedIndex = 0;

            //Aziz 2382 12/19/2007
            //IsAttorney Flag added
            this.chkAttorney.Checked = false;
            //ozair 5196 11/28/2008 setting to default view
            lblFirm.Style["display"] = "none";
            ddl_Firm.Style["display"] = "none";
            ddl_Firm.SelectedValue = "3000";

            //khalid 3188 2/20/08 service ticket admin
            //Asad Ali 7781 05/12/2010 Allow Multiple Admin
            //this.chk_StAdmin.Checked = false;


            //Nasir 6968 12/04/2009 Clear contact number   
            txt_CC11.Text = "";
            txt_CC12.Text = "";
            txt_CC13.Text = "";
            txt_CC14.Text = "";


        }
        ///Fahad 5196 26/11/2008
        /// <summary>
        /// This method is popualting the ddl by firm abbreviation and firm id.
        /// use to associate the firm with user.
        /// </summary>
        private void FillddlFirms()
        {
            try
            {
                //ozair 5306 12/11/2008 binding full name in firm ddl
                DataSet ds_Firms = ClsFirms.GetAllFullFirmName();

                ddl_Firm.DataSource = ds_Firms.Tables[0];
                //ozair 5306 12/05/2008 binding full name in firm ddl
                ddl_Firm.DataTextField = "FirmFullName";
                ddl_Firm.DataValueField = "FirmID";
                ddl_Firm.DataBind();
                ddl_Firm.SelectedValue = "3000";
            }
            catch (Exception ex)
            {
                lblMessage.Visible = true;
                lblMessage.Text = ex.Message;


            }
        }
        private void btn_clear_Click(object sender, System.EventArgs e)
        {
            ClearValues();

        }

        private void dg_results_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            if (e.CommandName == "click")
            {
                // If Selected Than Display Informaiton In The TextBoxs
                Label eid = (Label)e.Item.FindControl("lbl_eid");
                //Sabir Khan 5193 11/21/2008 set employee id into view state ...
                ViewState["EmployeeID"] = eid.Text;
                //employeeid = eid.Text;

                clsUser objUser = new clsUser(Convert.ToInt32(eid.Text));

                try
                {
                    //noufil 4232 07/03/2008 using clsuser properties to get values

                    //Noufil 4378 08/06/2008 using trim to remove spaces
                    //Displays Information In Text Boxes
                    tb_fname.Text = objUser.FirstName.Trim();
                    tb_lname.Text = objUser.LastName.Trim();
                    tb_abbrev.Text = objUser.Abbreviation.Trim();
                    tb_password.Text = objUser.Password.Trim();
                    tb_uname.Text = objUser.LoginID.Trim();

                    txt_email.Text = objUser.Email.Trim();
                    txt_NTUserID.Text = objUser.NTUserID.Trim();

                    ddl_accesstype.SelectedValue = Convert.ToInt32(objUser.AccessType).ToString();
                    if (ddl_accesstype.SelectedValue == "1")
                    {
                        ddl_closeout.Style[HtmlTextWriterStyle.Display] = "visible";
                        ddl_closeout.SelectedValue = Convert.ToInt32(objUser.canUpdateCloseOut).ToString();
                        if (ddl_closeout.SelectedValue == "1")
                        {
                            tb_NoofDays.Style[HtmlTextWriterStyle.Display] = "visible";
                            tb_NoofDays.Text = objUser.CloseOutAccessDay.ToString();
                        }
                        else
                        {
                            tb_NoofDays.Style[HtmlTextWriterStyle.Display] = "hidden";
                        }
                    }
                    else
                    {
                        ddl_closeout.Style[HtmlTextWriterStyle.Display] = "hidden";
                    }

                    ddl_status.SelectedValue = Convert.ToInt32(objUser.Status).ToString();
                    ddl_spnsearch.SelectedValue = Convert.ToInt32(objUser.IsSPNUser).ToString();



                    //Aziz 2382 12/19/2007
                    //IsAttorney Flag added
                    this.chkAttorney.Checked = Convert.ToBoolean(objUser.Isattorney);
                    //Fahad 5196 11/28/2008
                    if (chkAttorney.Checked)
                    {
                        lblFirm.Style[HtmlTextWriterStyle.Display] = "Block";
                        ddl_Firm.Style[HtmlTextWriterStyle.Display] = "Block";
                        ddl_Firm.SelectedValue = Convert.ToInt32(objUser.AssociatedFirmID).ToString();

                        //Nasir 6968 12/04/2009 set contact number in text boxes
                        if (objUser.ContactNumber != String.Empty)
                        {
                            if (objUser.ContactNumber.Length >= 3)
                                txt_CC11.Text = objUser.ContactNumber.Substring(0, 3);
                            if (objUser.ContactNumber.Length >= 6)
                                txt_CC12.Text = objUser.ContactNumber.Substring(3, 3);
                            if (objUser.ContactNumber.Length >= 10)
                                txt_CC13.Text = objUser.ContactNumber.Substring(6, 4);
                            if (objUser.ContactNumber.Length > 10)
                                txt_CC14.Text = objUser.ContactNumber.Substring(10);
                        }
                        else
                        {
                            txt_CC11.Text = "";
                            txt_CC12.Text = "";
                            txt_CC13.Text = "";
                            txt_CC14.Text = "";
                        }


                    }
                    else
                    {
                        ddl_Firm.Style[HtmlTextWriterStyle.Display] = "None";
                        lblFirm.Style[HtmlTextWriterStyle.Display] = "None";
                        //ddl_Firm.Style[HtmlTextWriterStyle.Display] = "Block";
                        //ddl_Firm.SelectedValue = Convert.ToInt32(objUser.AssociatedFirmID).ToString();

                    }

                    //khalid   2/19/2008  //added  for ServiceTicket Default assigned To user
                    this.chk_StAdmin.Checked = Convert.ToBoolean(objUser.Iservicetadmin);
                    //Asad Ali 7781 05/12/2010 Allow Multiple Admin
                    //if (this.chk_StAdmin.Checked == true)
                    //{
                    //    this.chk_StAdmin.Enabled = false;
                    //}
                    //else
                    //{
                    //    this.chk_StAdmin.Enabled = true;

                    //}
                    if (Convert.ToInt32(objUser.IsSPNUser) == 1)
                    {

                        txt_SPNUserName.Text = objUser.UserNameSPN.Trim();
                        txt_SPNPassword.Text = objUser.PasswordSPN.Trim();
                    }
                    else
                    {

                        txt_SPNPassword.Text = "";
                        txt_SPNUserName.Text = "";
                    }
                }

                catch (Exception ex)
                {
                    lblMessage.Text = "Cannot Display Record";
                    clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                }

            }
        }

        protected void btn_update_Click1(object sender, EventArgs e)
        {
            try
            {


                // Update Value of User
                //Sabir Khan 5193 11/21/2008 set hidden field value to empployee id variable ...
                // string empid = employeeid;

                string empid = Convert.ToString(ViewState["EmployeeID"]);
                // Check If No User Is Selected Than Set Employee ID to 0

                if (empid == "")
                    empid = "0";

                //check for user existance.
                int exist = 0;
                DataSet ds_check = cUser.CheckUser(tb_uname.Text.Trim(), empid);
                if (ds_check.Tables[0].Rows.Count > 0)
                {
                    exist = Convert.ToInt32(ds_check.Tables[0].Rows[0]["exist"].ToString());
                }
                if (exist == 0)
                {
                    //ozair 4759 09/13/2008 implemented bool variable for canspnsearh
                    bool canSPNSearch;
                    string SPNUserName;
                    string SPNPassword;
                    if (ddl_spnsearch.SelectedValue == "1")
                    {
                        //ozair 4759 09/13/2008 implemented bool variable for canspnsearh
                        canSPNSearch = true;
                        SPNUserName = txt_SPNUserName.Text;
                        SPNPassword = txt_SPNPassword.Text;
                    }
                    else
                    {
                        //ozair 4759 09/13/2008 implemented bool variable for canspnsearh
                        canSPNSearch = false;
                        SPNUserName = "";
                        SPNPassword = "";
                    }

                    //Nasir 6968 12/04/2009 concadinate contact number
                    string ContactNumber = txt_CC11.Text + txt_CC12.Text + txt_CC13.Text + txt_CC14.Text;


                    // Noufil 4232 07/09/2008 add user function switch to clsuser class
                    //ozair 4759 09/13/2008 implemented bool variable for canspnsearh
                    //Fahad 5196 11/27/2008 add ddl value to add firm

                    //Nasir 6968 12/04/2009 add contact number
                    cUser.Adduser(tb_fname.Text, tb_lname.Text, tb_abbrev.Text, tb_uname.Text.Trim(), tb_password.Text, ddl_accesstype.SelectedValue, ddl_closeout.SelectedValue, empid, txt_email.Text.Trim(), canSPNSearch, SPNUserName, SPNPassword, ddl_status.SelectedValue, txt_NTUserID.Text.Trim(), this.chkAttorney.Checked, this.chk_StAdmin.Checked, tb_NoofDays.Text, ddl_Firm.SelectedValue.ToString(), ContactNumber);

                    //Zeeshan Ahmed 4703 08/28/2008 If Login User & Update User Are Same Update The Password In The Cookie
                    if (Convert.ToString(ClsSession.GetCookie("sUserID", this.Request)) == tb_uname.Text.Trim())
                    {
                        ClsSession.CreateCookie("Password", tb_password.Text.Trim(), this.Request, this.Response);
                        //Redirect To Update Ticket Desk Password In the URL
                        Response.Redirect("Admin.aspx", false);
                    }
                    else
                    {
                        bindGrid();
                    }

                    //Sabir Khan 5193 11/21/2008
                    ClearValues();

                }
                else
                {
                    //Sabir Khan 5193 11/21/2008 when user name already exist...

                    HttpContext.Current.Response.Write("<script language='javascript'> alert('User name already exist! Please specify another user name.'); </script>");
                }
            }
            catch (Exception ex)
            {
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void rbl_status_SelectedIndexChanged(object sender, EventArgs e)
        {
            bindGrid();
        }

        protected void ddl_sort_SelectedIndexChanged(object sender, EventArgs e)
        {
            bindGrid();
        }
    }
}

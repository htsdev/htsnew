using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.backroom
{
	/// <summary>
	/// Summary description for WebForm1.
	/// </summary>
	public partial class WebForm1 : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.DataGrid dgViolInfo;
		protected System.Web.UI.WebControls.CheckBox Chkb_activeplan;
		protected System.Web.UI.WebControls.DropDownList ddl_crtloc;
		protected eWorld.UI.CalendarPopup cal_EffectiveFrom;
		protected System.Web.UI.WebControls.DropDownList ddl_EffectiveFrom;
		protected eWorld.UI.CalendarPopup cal_EffectiveTo;
		protected System.Web.UI.WebControls.DropDownList ddl_EffectiveTo;
		protected eWorld.UI.CalendarPopup cal_EndFrom;
		protected System.Web.UI.WebControls.DropDownList ddl_EndFrom;
		protected eWorld.UI.CalendarPopup cal_EndTo;
		protected System.Web.UI.WebControls.DropDownList ddl_EndTo;
		protected System.Web.UI.WebControls.Button btn_SearchPricing;
		clsENationWebComponents ClsDb = new clsENationWebComponents();
		clsCourts ClsCourts = new clsCourts();		
		clsPricingPlans ClsPricingPlans = new clsPricingPlans(); 
		clsSession ClsSession=new clsSession();
		clsLogger clog = new clsLogger();
		
		private void Page_Load(object sender, System.EventArgs e)
		{
			//dgViolInfo.DataSource = ClsDb.Get_DS_BySP("usp_tempprice");
			//dgViolInfo.DataBind();	
			if (ClsSession.IsValidSession(this.Request)==false)
			{
				Response.Redirect("../frmlogin.aspx",false);
			}
			else //To stop page further execution
			{
				if (ClsSession.GetCookie("sAccessType",this.Request).ToString()!="2")
				{
					Response.Redirect("../LoginAccesserror.aspx",false);
					Response.End();
				}
				else //To stop page further execution
				{
					if (Page.IsPostBack != true)
					{
						try
						{
							btn_SearchPricing.Attributes.Add("OnClick", "return validate();");
							// Filling time in Dropdowns
							FillTimeDDL(ddl_EffectiveFrom );
							FillTimeDDL(ddl_EffectiveTo);
							FillTimeDDL(ddl_EndFrom);
							FillTimeDDL(ddl_EndTo);
							FillCourtsLocation(ddl_crtloc);
							//AddJavascriptEvents();

							//btn_SearchPricing.Attributes.Add("OnClick","javascript:return validation();");
						}
						catch(Exception ex)
						{
							
							clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
						}
			
					}
				}
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btn_SearchPricing.Click += new System.EventHandler(this.btn_SearchPricing_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		private void FillTimeDDL(DropDownList ddl_CrtTime)
		{
			Int32 i;
			ddl_CrtTime.Items.Add("--------");
			ddl_CrtTime.Items[0].Value = "00:00";
					
			ddl_CrtTime.Items.Add( "8:00 am");
			ddl_CrtTime.Items[1].Value =("08:00");
			ddl_CrtTime.Items.Add("8:15 am");
			ddl_CrtTime.Items[2].Value =("08:15");
			ddl_CrtTime.Items.Add("8:30 am");
			ddl_CrtTime.Items[3].Value =("08:30");
			ddl_CrtTime.Items.Add("8:45 am");
			ddl_CrtTime.Items[4].Value =("08:45");

			ddl_CrtTime.Items.Add( "9:00 am");
			ddl_CrtTime.Items[5].Value =("09:00");
			ddl_CrtTime.Items.Add("9:15 am");
			ddl_CrtTime.Items[6].Value =("09:15");
			ddl_CrtTime.Items.Add("9:30 am");
			ddl_CrtTime.Items[7].Value =("09:30");
			ddl_CrtTime.Items.Add("9:45 am");
			ddl_CrtTime.Items[8].Value =("09:45");
				
			Int32 iTime = 9;
			for ( i = 10;i <12;i++)
			{						
				ddl_CrtTime.Items.Add(i.ToString()  + ":00 am");
				ddl_CrtTime.Items[iTime].Value =(i.ToString()  + ":00");
				ddl_CrtTime.Items.Add(i.ToString() + ":15 am");
				ddl_CrtTime.Items[iTime+=1].Value =(i.ToString()  + ":15");
				ddl_CrtTime.Items.Add(i.ToString() + ":30 am");
				ddl_CrtTime.Items[iTime+=1].Value =(i.ToString()  + ":30");
				ddl_CrtTime.Items.Add(i.ToString() + ":45 am");
				ddl_CrtTime.Items[iTime+=1].Value =(i.ToString()  + ":45");
				iTime+=1 ;
			}	
		
		
			ddl_CrtTime.Items.Add("12:00 pm");
			ddl_CrtTime.Items[iTime].Value =("12:00");
			ddl_CrtTime.Items.Add("12:15 pm");
			ddl_CrtTime.Items[iTime+=1].Value =("12:15");
			ddl_CrtTime.Items.Add("12:30 pm");
			ddl_CrtTime.Items[iTime+=1].Value =("12:30");
			ddl_CrtTime.Items.Add("12:45 pm");
			ddl_CrtTime.Items[iTime+=1].Value =("12:45");
			iTime+=1 ;
			
			
			
			Int32 iTimeClock = 12;
			for ( i = 1;i <9;i++)
			{
				ddl_CrtTime.Items.Add(i.ToString() + ":00 pm");
				iTimeClock+=1;
				ddl_CrtTime.Items[iTime].Value =(iTimeClock.ToString()  + ":00");
				ddl_CrtTime.Items.Add(i.ToString() + ":15 pm");
				ddl_CrtTime.Items[iTime+=1].Value =(iTimeClock.ToString()  + ":15");
				ddl_CrtTime.Items.Add(i.ToString() + ":30 pm");
				ddl_CrtTime.Items[iTime+=1].Value =(iTimeClock.ToString()  + ":30");
				ddl_CrtTime.Items.Add(i.ToString() + ":45 pm");
				ddl_CrtTime.Items[iTime+=1].Value =(iTimeClock.ToString()  + ":45");
				iTime+=1 ;
			}
			ddl_CrtTime.Items.Add("9:00 pm");
			iTimeClock+=1;
			ddl_CrtTime.Items[iTime].Value =(iTimeClock.ToString()  + ":00");


			
			ddl_CrtTime.SelectedIndex = 0;
		
		}
		private void FillCourtsLocation(DropDownList ddl_Court)
		{
		Int32 iCount=0;
            //Change by Ajmal
		 //SqlDataReader  rdr_Courts = ClsCourts.GetAllCourtName();
         IDataReader rdr_Courts = ClsCourts.GetAllCourtName();
			while (rdr_Courts.Read())
			{
				ddl_Court.Items.Add (rdr_Courts["shortcourtname"].ToString());
				ddl_Court.Items[iCount].Value = rdr_Courts["courtid"].ToString() ;
				iCount++ ;
			}
			rdr_Courts.Close();
		}

		private void btn_SearchPricing_Click(object sender, System.EventArgs e)
		{	
			try 
			{
				if (cal_EffectiveFrom.SelectedDate.ToString() == "1/1/0001 12:00:00 AM" && cal_EffectiveTo.SelectedDate.ToString() == "1/1/0001 12:00:00 AM" && cal_EndFrom.SelectedDate.ToString() == "1/1/0001 12:00:00 AM" && cal_EndTo.SelectedDate.ToString() == "1/1/0001 12:00:00 AM")
				{
					ClsPricingPlans.CourtID = Convert.ToInt32(ddl_crtloc.SelectedValue);
					ClsPricingPlans.ShowAll = retvalueCheckbox(Chkb_activeplan);
					String[] keys = {"@DefaultDate","@CourtID","@ShowAll"};
					Object[] values = {DateTime.Now.ToString(),ClsPricingPlans.CourtID,ClsPricingPlans.ShowAll};
					DataSet ds_Pricing = ClsDb.Get_DS_BySPArr("USP_HTS_GET_PricingPlans_OnActiveFlag",keys,values);
					dgViolInfo.DataSource = ds_Pricing;
				}
                else
				{
					ClsPricingPlans.EffectiveDateFrom = validDate(calDate(cal_EffectiveFrom.SelectedDate.ToString().Trim(),ddl_EffectiveFrom.SelectedValue),1);	
				
					ClsPricingPlans.EffectiveDateTo =  validDate(calDate(cal_EffectiveTo.SelectedDate.ToString().Trim(),ddl_EffectiveTo.SelectedValue),3) ;
			
					ClsPricingPlans.EndDateFrom = validDate(calDate(cal_EndFrom.SelectedDate.ToString().Trim(),ddl_EndFrom.SelectedValue),1);

					ClsPricingPlans.EndDateTo = validDate(calDate(cal_EndTo.SelectedDate.ToString().Trim(),ddl_EndTo.SelectedValue),3);
					ClsPricingPlans.CourtID = Convert.ToInt32(ddl_crtloc.SelectedValue);
					ClsPricingPlans.ShowAll = retvalueCheckbox(Chkb_activeplan);
				
					DataSet ds_Pricing = ClsPricingPlans.SearchPricingPlans();
					dgViolInfo.DataSource = ds_Pricing;
				}
			
				dgViolInfo.DataBind();
				dgStatus();
				setGridLinks();
			}
			catch(Exception ex)
			{
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}

		}

		private int retvalueCheckbox(CheckBox chk)
		{
			if (chk.Checked  == true) 
			{
				return 1;
			}
			else
			{
				return 0;
			}
		
		}
		private DateTime  calDate(string strDate,string strTime)
		{
			DateTime tempdt = Convert.ToDateTime(strDate);
			string newSubDate = tempdt.Date.ToString() ;
			String newDate;
			newDate = newSubDate + " "  + strTime.ToString();
			
			DateTime dtTime ;
			//DateTime dtDate ;
			dtTime = (DateTime)(Convert.ChangeType(newDate,typeof(System.DateTime)));
			
			
			return dtTime;
		}

		private void dgStatus()
		{
			foreach(DataGridItem dgItem in dgViolInfo.Items)
			{
				if ((((Label) dgItem.FindControl("lblStatus")).Text) == "True")
				{
					((Label) dgItem.FindControl("lblStatus")).Text = "Active";
				}
				else if (((Label) dgItem.FindControl("lblStatus")).Text == "")
				{
					((Label) dgItem.FindControl("lblStatus")).Text = "InActive";
				}
				else if (((Label) dgItem.FindControl("lblStatus")).Text == "False")
				{
					((Label) dgItem.FindControl("lblStatus")).Text = "InActive";
				}
			}
		}

		private void AddJavascriptEvents()
		{
			cal_EffectiveFrom.Attributes.Add("onChange","javascript: ActiveDate('" + cal_EffectiveFrom.ClientID +"');");
			cal_EffectiveTo.Attributes.Add("onChange","javascript: ActiveDate('" + cal_EffectiveTo .ClientID +"');");
			cal_EndTo.Attributes.Add("onclick","javascript: ActiveDate('" + cal_EndTo.ClientID +"');");
			cal_EndFrom.Attributes.Add("onclick","javascript: ActiveDate('" + cal_EndFrom.ClientID +"');");

		}
		private DateTime validDate(DateTime vDate,int sel)
		{
			switch (sel)
			{
				case (1):
				{
					if (vDate < Convert.ToDateTime("1/1/1900"))
					{
						vDate = Convert.ToDateTime("1/1/1900") ;
					}
					break;
				}
				case (2):
				{
					if ((vDate > DateTime.Now) || (vDate < Convert.ToDateTime("1/1/1900")))
					{
						vDate = DateTime.Now.AddYears(10);
					}	 
				break;
				}
				case (3):
				{
					if ((vDate <= Convert.ToDateTime("1/1/1900")))
					{
						vDate = DateTime.Now;
					}	 
					break;
				}
				case (4):
				{
					
						vDate = DateTime.Now;
						vDate = vDate.AddYears(10);
					break;
				}
			}
		return vDate;
		}

		private void setGridLinks()
		{
			foreach ( DataGridItem  items in dgViolInfo.Items) 
			{
				((HyperLink) items.FindControl("lnk_plandesc")).NavigateUrl = "frmAddPricePlan.aspx?ID="+((Label) items.FindControl("lbl_hidPlanID")).Text + "&Posted=1" ;
			}
		}
	}

}

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components;
using System.IO;
using lntechNew.Components.ClientInfo;

namespace lntechNew.backroom
{
    /// <summary>
    /// Summary description for GeneralView.
    /// </summary>
    public partial class GeneralView : System.Web.UI.Page
    {
        /// <summary>
        /// Following modifications has been made
        /// 1) Apply paging at report 
        /// 2) Fixed Sorting issues 
        /// 3) Add new Column & Assigned to
        /// 4) Date Format fixed 
        /// 5) All business logic has ben moved from presentation payer to business layer
        /// 
        /// End Fahad ( 12/15/2007 )
        /// </summary>
        #region Variables

        DataView DV;
        clsSession cSession = new clsSession();
        clsLogger BugTracker = new clsLogger();
        Bug clsbug = new Bug();
        string StrAcsDec = String.Empty;
        string StrExp = String.Empty;

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dg_Viewbug.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_Viewbug_ItemCommand);
            this.dg_Viewbug.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dg_Viewbug_PageIndexChanged);
            this.dg_Viewbug.SortCommand += new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.dg_Viewbug_SortCommand);
            this.dg_Viewbug.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(dg_Viewbug_ItemDataBound);

        }
        #endregion

        #region Events

        protected void Page_Load(object sender, System.EventArgs e)
        {
            if (cSession.IsValidSession(this.Request) == false)
            {
                Response.Redirect("../frmlogin.aspx", false);
            }
            else
            {
                if (!IsPostBack)
                {
                    chk_all_records.Checked = true;
                    BindGrid(0);
                }
            }
            chk_all_records.CheckedChanged += new EventHandler(chkall_CheckedChanged);
            /// Add Paging Control by fahad 12/15/2007
            Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
        }

        private void dg_Viewbug_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
        {
            try
            {


                dg_Viewbug.CurrentPageIndex = e.NewPageIndex;

                Pagingctrl.PageCount = dg_Viewbug.PageCount;
                Pagingctrl.PageIndex = dg_Viewbug.CurrentPageIndex;
                Pagingctrl.SetPageIndex();

                DV = (DataView)Session["DV"];
                dg_Viewbug.DataSource = DV;
                dg_Viewbug.DataBind();
                Pagingctrl.SetPageIndex();

            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }
        }
        /// <summary>
        /// Added By Fahad When Usre Click Edit button then system will redirect user to the UpdateBug.aspx (12/15/2007 - Fahad )
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private void dg_Viewbug_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Edit")
                {
                    string bugid = ((Label)e.Item.FindControl("lblbugid")).Text.ToString();
                    Response.Redirect("UpdateBug.aspx?Bugid=" + bugid, false);
                }
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }
        }
        /// <summary>
        /// Added By Fahad For Showing All Open & Closed Tickets 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void chkall_CheckedChanged(object sender, EventArgs e)
        {
            if (chk_all_records.Checked == true)
            {
                Pagingctrl.PageIndex = 0;
                dg_Viewbug.CurrentPageIndex = 0;
                BindGrid(0);

            }
            else
            {
                Pagingctrl.PageIndex = 0;
                dg_Viewbug.CurrentPageIndex = 0;
                BindGrid(1);
            }
        }
        /// <summary>
        /// Added By Fahad for sorting purpose (12/15/2007 - Fahad )
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private void dg_Viewbug_SortCommand(object source, System.Web.UI.WebControls.DataGridSortCommandEventArgs e)
        {
            try
            {
                SortGrid(e.SortExpression);
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }

        }

        protected void lnkBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("GeneralBug.aspx");
        }

        #endregion

        #region Methods

        private void BindGrid(int showall)
        {
            try
            {
                DataSet DS_GetBugs = clsbug.BindGridGeneralView(Convert.ToInt32(showall));


                if (DS_GetBugs.Tables[0].Rows.Count > 0)
                {
                    dg_Viewbug.DataSource = DS_GetBugs;

                    if (Session["DV"] != null)
                        DV = (DataView)Session["DV"];
                    else
                        DV = new DataView();

                    DV.Table = DS_GetBugs.Tables[0];
                    Session["DV"] = DV;
                    dg_Viewbug.DataBind();
                }
                else
                {
                    lblMessage.Text = "No Record Found";
                    lblMessage.Visible = true;
                    chk_all_records.Visible = false;
                }

                Pagingctrl.PageCount = dg_Viewbug.PageCount;
                Pagingctrl.PageIndex = dg_Viewbug.CurrentPageIndex;
                Pagingctrl.SetPageIndex();

            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }
        }



        private void SortGrid(string SortExp)
        {
            try
            {
                SetAcsDesc(SortExp);
                DV = (DataView)Session["DV"];
                DV.Sort = StrExp + " " + StrAcsDec;
                dg_Viewbug.DataSource = DV;
                dg_Viewbug.DataBind();
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }
        }

        private void SetAcsDesc(string Val)
        {
            try
            {
                StrExp = Session["StrExp"].ToString();
                StrAcsDec = Session["StrAcsDec"].ToString();
            }
            catch
            {

            }

            if (StrExp == Val)
            {
                if (StrAcsDec == "ASC")
                {
                    StrAcsDec = "DESC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
                else
                {
                    StrAcsDec = "ASC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
            }
            else
            {
                StrExp = Val;
                StrAcsDec = "ASC";
                Session["StrExp"] = StrExp;
                Session["StrAcsDec"] = StrAcsDec;
            }
        }

        void Pagingctrl_PageIndexChanged()
        {
            dg_Viewbug.CurrentPageIndex = Pagingctrl.PageIndex - 1;
            DV = (DataView)Session["DV"];
            dg_Viewbug.DataSource = DV;
            dg_Viewbug.DataBind();
        }




        #endregion
        /// <summary>
        /// Added By Fahad For Displaying High Priority in Red Color & Closed Bugs in Red Color (12/15/2007 - Fahad )
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dg_Viewbug_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                string status = ((Label)e.Item.FindControl("lblstatus")).Text.ToString();
                string priority = ((Label)e.Item.FindControl("lblpriority")).Text.ToString();
                if (status.ToUpper() == "CLOSED")
                {
                    ((Label)e.Item.FindControl("lblstatus")).ForeColor = System.Drawing.Color.Red;
                }
                if (priority.ToUpper() == "HIGH")
                {
                    ((Label)e.Item.FindControl("lblpriority")).ForeColor = System.Drawing.Color.Red;
                }

                if (status.Length > 10)
                {
                    ((Label)e.Item.FindControl("lblstatus")).ToolTip = status;
                    ((Label)e.Item.FindControl("lblstatus")).Text = (status.Substring(0, 8)) + "...";

                }


                string desc = ((Label)e.Item.FindControl("lblshortdesc")).Text.ToString();
                if (desc.Length > 15)
                {
                    ((Label)e.Item.FindControl("lblshortdesc")).ToolTip = desc;
                    ((Label)e.Item.FindControl("lblshortdesc")).Text = (desc.Substring(0, 15)) + "...";

                }

            }

        }


    }
}

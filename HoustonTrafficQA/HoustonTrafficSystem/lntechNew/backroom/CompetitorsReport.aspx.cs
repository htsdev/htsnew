using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.backroom
{
    public partial class CompetitorsReport : System.Web.UI.Page
    {
        clsSession ClsSession = new clsSession();
        clsLogger clog = new clsLogger();
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        DataSet ds_val;
        int type = 0;
        DataTable dtRecords;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (ClsSession.IsValidSession(this.Request) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else //To stop page further execution
                {
                    if (ClsSession.GetCookie("sAccessType", this.Request).ToString() != "2")
                    {
                        Response.Redirect("../LoginAccesserror.aspx", false);
                        Response.End();
                    }
                    else //To stop page further execution
                    {

                        lbl_message.Text = "";
                        if (!IsPostBack)
                        {
                            calQueryFrom.SelectedDate = DateTime.Today;
                            calQueryTo.SelectedDate = DateTime.Today;
                            btn_submit.Attributes.Add("OnClick", "return validate();");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void btn_submit_Click(object sender, EventArgs e)
        {
            GetRecords();
            if (dtRecords.Rows.Count == 0)
                lbl_message.Text = "No Record Found";
            dg_valrep.DataSource = dtRecords;
            dg_valrep.DataBind();
            if (type == 1)
                dg_valrep.Columns[3].Visible = true;
            else
                dg_valrep.Columns[3].Visible = false;

        }

        private void GetRecords()
        {
            try
            {
                if (rdbtn_bond.Checked)
                    type = 1;

                string[] key ={ "@startdate", "@enddate", "@Type" };
                object[] value1 ={ calQueryFrom.SelectedDate, calQueryTo.SelectedDate, type };
                dtRecords = ClsDb.Get_DT_BySPArr("usp_hts_CompetitorReport", key, value1);

                lbl_message.Text = "";
                //if (ds_val.Tables[0].Rows.Count < 1)
                //{
                //    lbl_message.Text = "No Record Found";
                //    dg_valrep.Visible = false;
                //}

                //else
                //{
                //    dg_valrep.Visible = true;
                //    dg_valrep.DataSource = ds_val;
                //    dg_valrep.DataBind();

                //}
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.ToString();
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        protected void dg_valrep_ItemDataBound(object sender, DataGridItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Header)
            {
                if (type == 1)
                {
                    ((LinkButton ) e.Item.FindControl("LinkButton1")).Text = "Bonding Company";
                    ((LinkButton )e.Item.FindControl("LinkButton2")).Text = "No. of Bond cases";
                    ((LinkButton)e.Item.FindControl("LinkButton3")).Text = "No. of Bond Clients";
                    ((LinkButton )e.Item.FindControl("LinkButton5")).Text = "Bond Amount";
                    ((LinkButton)e.Item.FindControl("LinkButton4")).Text = "% of Bonds";
                }
                else
                {
                    ((LinkButton)e.Item.FindControl("LinkButton1")).Text = "Attorney Name";
                    ((LinkButton)e.Item.FindControl("LinkButton2")).Text = "No. of Cases";
                    ((LinkButton)e.Item.FindControl("LinkButton3")).Text = "No. of Clients";
                    ((LinkButton)e.Item.FindControl("LinkButton5")).Text = "Bond Amount";
                    ((LinkButton)e.Item.FindControl("LinkButton4")).Text = "% of Jury Sets";
                }
            }
            //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            //{
            //    if (type == 1)
            //        ((Label)e.Item.FindControl("lbl_totalsum")).Text = String.Format("{0:C}", Convert.ToDouble(((Label)e.Item.FindControl("lbl_totalquote")).Text));
            //}
        }

        public SortDirection GridViewSortDirection
        {

            get
            {

                if (ViewState["sortDirection"] == null)

                    ViewState["sortDirection"] = SortDirection.Ascending;

                return (SortDirection)ViewState["sortDirection"];

            }

            set { ViewState["sortDirection"] = value; }

        }

        private void SortGridView(string sortExpression, string direction)
        {


            GetRecords();

            DataTable dt = dtRecords;

            DataView dv = new DataView(dt);

            dv.Sort = sortExpression + " " + direction;

            dg_valrep.DataSource = dv;

            dg_valrep.DataBind();

        }

        protected void dg_valrep_SortCommand(object source, DataGridSortCommandEventArgs e)
        {
            string sortExpression = e.SortExpression;
            Session["SortExpression"] = sortExpression;

            if (GridViewSortDirection == SortDirection.Ascending)
            {

                GridViewSortDirection = SortDirection.Descending;
                Session["SortDirection"] = " desc";
                SortGridView(sortExpression, " desc");

            }

            else
            {

                GridViewSortDirection = SortDirection.Ascending;
                Session["SortDirection"] = " asc";
                SortGridView(sortExpression, " asc");

            } 
        }

        protected void dg_valrep_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            string sortExpression = e.CommandName.ToString();
            Session["SortExpression"] = sortExpression;

            if (GridViewSortDirection == SortDirection.Ascending)
            {

                GridViewSortDirection = SortDirection.Descending;
                Session["SortDirection"] = " desc";
                SortGridView(sortExpression, " desc");

            }

            else
            {

                GridViewSortDirection = SortDirection.Ascending;
                Session["SortDirection"] = " asc";
                SortGridView(sortExpression, " asc");

            } 
        }
    }
}

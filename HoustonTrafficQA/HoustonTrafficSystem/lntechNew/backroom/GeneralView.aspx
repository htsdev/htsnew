<%@ Page language="c#" Codebehind="GeneralView.aspx.cs" AutoEventWireup="True" Inherits="lntechNew.backroom.GeneralView" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc2" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>GeneralView</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
	</HEAD>
	<body MS_POSITIONING="GridLayout" bottomMargin="0" topMargin="0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="TableMain" cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
			    <tr><td colspan="2">
			    <uc1:activemenu id="ActiveMenu1" runat="server"></uc1:activemenu>
			</td></tr>
			<TR>
								<TD background="../../images/separator_repeat.gif" Height="11" ></TD>
							</TR>
							<tr>
							<td align="right" style="height: 58px">
							<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
							
					            <TD  class="clssubhead" background="../Images/subhead_bg.gif" style="height: 38px" >
					            <table bord="0" cellpadding="0" cellspacing="0" width="100%">
					            <tr>
					            <td class="clssubhead" style="height: 13px">
                                    &nbsp;Bug&nbsp;
                                    
                                    </td>
                                    <td align="right" class="clssubhead" style="height: 13px">
                                        <uc2:PagingControl ID="Pagingctrl" runat="server" />
                                    &nbsp;
                                    </td>
                                    </tr>
                                    </table>
                                   
                                   
                                  
                                    
                                    </TD>
					
							</tr>
							</table>
							 <asp:CheckBox ID="chk_all_records" runat="server" OnCheckedChanged="chkall_CheckedChanged" Text="Show All Open Tickets  " CssClass="clslabelnew"  AutoPostBack="True" Width="134px"  /></td>
							</tr>
							
				
				<TR>
					<TD vAlign="top" colSpan="2">
						<TABLE id="tbl1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD align="center" colSpan="2">
									<asp:Label id="lblMessage" runat="server" ForeColor="Red"></asp:Label></TD>
							</TR>
							<TR>
								<TD align="center" colSpan="2"></TD>
							</TR>
							<TR>
								<TD align="left" colSpan="2">
									<asp:DataGrid id="dg_Viewbug" runat="server" Width="100%" AutoGenerateColumns="False" AllowPaging="True"
										AllowSorting="True" PageSize="30" >
										<AlternatingItemStyle BackColor="#EEEEEE"></AlternatingItemStyle>
										<HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader" VerticalAlign="Middle"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn  SortExpression="bug_id" HeaderText="Bugid">
												<ItemTemplate>
													<asp:Label id=lblbugid runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container,"DataItem.bug_id") %>'></asp:Label>
												</ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Left" Width="50px" />
											</asp:TemplateColumn>
											
											<asp:TemplateColumn SortExpression="shortdescription" HeaderText="Title">
												<ItemTemplate>
													<asp:Label id=lblshortdesc runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container,"DataItem.shortdescription") %>'>
													</asp:Label>
												</ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Left" Width="200px" />
											</asp:TemplateColumn>
											<asp:TemplateColumn SortExpression="TicketNumber" HeaderText="Ticket.No">
												<ItemTemplate>
													<asp:Label id=lblTicketNo runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container,"DataItem.TicketNumber") %>' Width="119px"></asp:Label>
												</ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Left" />
											</asp:TemplateColumn>
                                            <asp:TemplateColumn  SortExpression="username" HeaderText="User">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblusername" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.username") %>' Width="87px"></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Left" />
                                            </asp:TemplateColumn>
											<asp:TemplateColumn SortExpression="posteddate" HeaderText="Posted Date/Time">
												<ItemTemplate>
													<asp:Label id=lblDate runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container,"DataItem.posteddate") %>' Width="142px"></asp:Label>
												</ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Left" />
											</asp:TemplateColumn>
											<asp:TemplateColumn SortExpression="priority_name" HeaderText="Priority">
												<ItemTemplate>
													<asp:Label id=lblpriority runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.priority_name") %>'>
													</asp:Label>
												</ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Left" />
											</asp:TemplateColumn>
											<asp:TemplateColumn SortExpression="status_name" HeaderText="Status">
												<ItemTemplate>
													<asp:Label id=lblstatus runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.status_name") %>'>
													</asp:Label>
												</ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Left" Width="100px" />
											</asp:TemplateColumn>
											<asp:TemplateColumn SortExpression = "DeveloperName" HeaderText = "Assigned To">
											 <ItemTemplate>
											 <asp:Label id="lblDeveloperName" runat="server" CssClass="label" Text='<%# Bind("DeveloperName") %>' Width="112px"></asp:Label>
											 </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Left" />
											</asp:TemplateColumn>
											<asp:TemplateColumn>
												<ItemTemplate>
													<asp:LinkButton id="lnkEdit" runat="server" CommandName="Edit">Edit</asp:LinkButton>
												</ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Left" />
											</asp:TemplateColumn>
										</Columns>
										<PagerStyle HorizontalAlign="Center" Mode="NumericPages"></PagerStyle>
									</asp:DataGrid></TD>
							</TR>
							<tr>
									<td background="../../images/separator_repeat.gif" colSpan="6" height="11"></td>
								</tr>
								<tr>
									<TD colSpan="5"><uc1:footer id="Footer1" runat="server"></uc1:footer></td>
								</tr>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>

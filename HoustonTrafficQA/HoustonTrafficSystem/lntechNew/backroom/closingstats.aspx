<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.backroom.closingstats" Codebehind="closingstats.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Closing Report</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">		
		<meta name="vs_defaultClientScript" content="JavaScript">		
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<SCRIPT src="../Scripts/dates.js" type="text/javascript"></SCRIPT>
		<script language="javascript">
		function validate()
		{
			//a;
			var d1 = document.getElementById("cal_from").value
			var d2 = document.getElementById("cal_to").value			
						
			if (d1 == d2)
				return true;
				
			if (compareDates(d2,'MM/dd/yyyy',d1,'MM/dd/yyyy')==false)
				{
					alert("start date cannot greater than end date");
					document.getElementById("cal_to").focus(); 
					return false;
				}
				return true;
		}
	</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
	
		<form id="Form1" method="post" runat="server">
			<TABLE id="TableMain" style="Z-INDEX: 101; LEFT: 104px; POSITION: absolute; TOP: 24px"
				cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
				<TR>
					<TD style="WIDTH: 815px" colSpan="4">
						<uc1:ActiveMenu id="ActiveMenu1" runat="server"></uc1:ActiveMenu></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 816px" colSpan="4">
						<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
							<TR>
								<TD width="100%" background="../Images/separator_repeat.gif" colSpan="4" height="10"></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 39px" width="100%" colSpan="4" height="39">
									<TABLE id="Table3" style="WIDTH: 788px; HEIGHT: 24px" cellSpacing="1" cellPadding="1" width="788"
										border="0">
										<TR>
											<TD class="clssubhead" style="WIDTH: 119px">&nbsp;
												<ew:calendarpopup id="cal_from" runat="server" Width="80px" ControlDisplay="TextBoxImage" ImageUrl="../images/calendar.gif"
													CalendarLocation="Left" ShowGoToToday="True" AllowArbitraryText="False" Culture="(Default)" Nullable="True"
													ShowClearDate="True" PadSingleDigits="True" ToolTip="Call Back Date" Font-Names="Tahoma" Font-Size="8pt"
													DisableTextboxEntry="False">
													<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
													<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="White"></WeekdayStyle>
													<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="Yellow"></MonthHeaderStyle>
													<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
														BackColor="AntiqueWhite"></OffMonthStyle>
													<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="White"></GoToTodayStyle>
													<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="LightGoldenrodYellow"></TodayDayStyle>
													<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="Orange"></DayHeaderStyle>
													<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="LightGray"></WeekendStyle>
													<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="Yellow"></SelectedDateStyle>
													<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="White"></ClearDateStyle>
													<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="White"></HolidayStyle>
												</ew:calendarpopup></TD>
											<TD align="left" style="WIDTH: 109px">
												<ew:calendarpopup id="cal_to" runat="server" Width="80px" ControlDisplay="TextBoxImage" ImageUrl="../images/calendar.gif"
													CalendarLocation="Left" ShowGoToToday="True" AllowArbitraryText="False" Culture="(Default)" Nullable="True"
													ShowClearDate="True" PadSingleDigits="True" ToolTip="Call Back Date" Font-Names="Tahoma" Font-Size="8pt"
													DisableTextboxEntry="False">
													<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
													<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="White"></WeekdayStyle>
													<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="Yellow"></MonthHeaderStyle>
													<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
														BackColor="AntiqueWhite"></OffMonthStyle>
													<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="White"></GoToTodayStyle>
													<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="LightGoldenrodYellow"></TodayDayStyle>
													<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="Orange"></DayHeaderStyle>
													<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="LightGray"></WeekendStyle>
													<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="Yellow"></SelectedDateStyle>
													<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="White"></ClearDateStyle>
													<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="White"></HolidayStyle>
												</ew:calendarpopup></TD>
											<TD align="left" style="WIDTH: 73px">
												<asp:button id="btnsubmit" runat="server" CssClass="clsbutton" Text="Submit"></asp:button></TD>
											<TD align="right"></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD align="left" colSpan="4">
									<asp:datagrid id="dgresult" runat="server" CssClass="clsleftpaddingtable" Width="100%" AutoGenerateColumns="False"
										BorderStyle="None" CellPadding="2" BorderColor="White" ShowHeader="False">
										<Columns>
											<asp:TemplateColumn ItemStyle-Width="10%">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id=lbl_desc runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn ItemStyle-Width="10%">
												<HeaderStyle Width="10%" CssClass="clsaspcolumnheader"></HeaderStyle>
												<HeaderTemplate>
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												</HeaderTemplate>
												<ItemTemplate>
													<asp:Label id=lbl_violcount runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.violationcount") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn>
												<HeaderStyle Width="80%"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id="lbl_result" runat="server" CssClass="Label"></asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD width="100%" background="../Images/separator_repeat.gif"  height="11"></TD>
							</TR>
							<TR>
								<TD align="center" width="100%"  height="11">
									<asp:label id="lbl_error" runat="server" ForeColor="Red"></asp:label></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 760px" align="left" >
									<uc1:Footer id="Footer1" runat="server"></uc1:Footer></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>

<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>

<%@ Page Language="c#" AutoEventWireup="false" Inherits="HTP.backroom.AutoDialerCallOutCome"
    CodeBehind="AutoDialerCallOutCome.aspx.cs" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>Auto Dialer Call Outcomes</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../Styles.css" type="text/css" rel="stylesheet" />

    <script src="../Scripts/dates.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
		function validate()
		{		
		    var cnt=document.getElementById("hf_ResponseCount").value;
		    var isSel=false;
		    for(var i=0;i<cnt;i++)
		    {
		        var chk="chkbl_ResponseType_"+i;
		        if(document.getElementById(chk).checked)
		        {
		            isSel=true;
		        }
		    }
		    
		    if(isSel==false)
		    {
		        alert("Please select response type");
		        return false;
		    }
		    
			var d1 = document.getElementById("dtFrom").value
			var d2 = document.getElementById("dtTo").value			
			
			if (d1 == d2)
				return true;
			if (compareDates(d2,'MM/dd/yyyy',d1,'MM/dd/yyyy')==false)
				{
					alert("start date cannot greater than end date");
					document.getElementById("dtTo").focus(); 
					return false;
				}
				return true;
		}
				
		function showall()
		{		
		    if (document.getElementById("chk_showall").checked)
		    {
		        document.getElementById("td_to").disabled =true;
		        document.getElementById("dtFrom").disabled =true;
		        document.getElementById("td_from").disabled =true;
		        document.getElementById("dtTo").disabled =true;
		    }
		    else
		    {
		        document.getElementById("td_to").disabled =false;
		        document.getElementById("dtFrom").disabled =false;
		        document.getElementById("td_from").disabled =false;
		        document.getElementById("dtTo").disabled =false;
		    }
		}	
		
		
    </script>

</head>
<body bottommargin="0" leftmargin="0" topmargin="0" rightmargin="0" ms_positioning="GridLayout"
    onload="showall()">
    <form id="Form1" method="post" runat="server">
    <table id="tblMain" cellspacing="0" cellpadding="0" width="870" align="center" border="0">
        <tr>
            <td align="left">
                <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
            </td>
        </tr>
        <tr>
            <td background="../images/separator_repeat.gif" height="11">
            </td>
        </tr>
        <tr>
            <td align="left" style="height: 33px">
                <table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="clssubhead">
                            Court:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddl_Courts" runat="server" CssClass="clsinputcombo">
                                <asp:ListItem Selected="True" Value="0">All Courts</asp:ListItem>
                                <asp:ListItem Value="1">HMC Courts</asp:ListItem>
                                <asp:ListItem Value="2">Non HMC Courts</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="clssubhead">
                            Event:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddl_Events" runat="server" CssClass="clsinputcombo">
                            </asp:DropDownList>
                        </td>
                        <td class="clssubhead" id="td_from">
                            From:
                        </td>
                        <td valign="middle" id="td_fromdate">
                            <ew:CalendarPopup ID="dtFrom" runat="server" Width="90px" Font-Size="8pt" Font-Names="Tahoma"
                                DisplayOffsetY="20" ToolTip="Date From" PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00"
                                ShowClearDate="True" Nullable="True" Culture="(Default)" AllowArbitraryText="False"
                                ShowGoToToday="True" CalendarLocation="Left" ImageUrl="../images/calendar.gif"
                                ControlDisplay="TextBoxImage" EnableHideDropDown="True" Text=" ">
                                <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                    BackColor="White"></WeekdayStyle>
                                <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                    BackColor="AntiqueWhite"></OffMonthStyle>
                                <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                    BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                    BackColor="LightGray"></WeekendStyle>
                                <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="White"></ClearDateStyle>
                                <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                    BackColor="White"></HolidayStyle>
                            </ew:CalendarPopup>
                        </td>
                        <td class="clssubhead" id="td_to">
                            To:
                        </td>
                        <td id="td_todate">
                            <ew:CalendarPopup ID="dtTo" runat="server" Width="90px" Font-Size="8pt" Font-Names="Tahoma"
                                DisplayOffsetY="20" ToolTip="Date To" PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00"
                                ShowClearDate="True" Nullable="True" Culture="(Default)" AllowArbitraryText="False"
                                ShowGoToToday="True" CalendarLocation="Left" ImageUrl="../images/calendar.gif"
                                ControlDisplay="TextBoxImage" EnableHideDropDown="True" Text=" ">
                                <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                    BackColor="White"></WeekdayStyle>
                                <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                    BackColor="AntiqueWhite"></OffMonthStyle>
                                <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                    BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                    BackColor="LightGray"></WeekendStyle>
                                <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="White"></ClearDateStyle>
                                <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                    BackColor="White"></HolidayStyle>
                            </ew:CalendarPopup>
                        </td>
                        <td>
                            <asp:CheckBox ID="chk_showall" runat="server" Text="Show All" CssClass="clssubhead"
                                onclick="showall()" />
                        </td>
                        <td>
                            <asp:Button ID="btnSubmit" runat="server" CssClass="clsbutton" Text="Submit" OnClientClick="showall()">
                            </asp:Button>
                        </td>
                    </tr>
                    <tr>
                        <td class="clssubhead">
                            Response Type:
                        </td>
                        <td colspan="9" align="left">
                            <asp:CheckBoxList ID="chkbl_ResponseType" RepeatDirection="Horizontal" CssClass="clsLabel"
                                runat="server">
                            </asp:CheckBoxList>
                            <asp:HiddenField ID="hf_ResponseCount" runat="server"></asp:HiddenField>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td background="../images/separator_repeat.gif" height="11">
            </td>
        </tr>
        <tr>
            <td width="100%">
                <table id="tblTitle" cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="clssubhead" height="34px" background="../Images/subhead_bg.gif">
                            &nbsp;
                        </td>
                        <td class="clssubhead" align="right" height="34px" background="../../Images/subhead_bg.gif">
                            <table id="tblPageNavigation" cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <td class="clslabel" align="right" style="width: 48px">
                                        <strong>Page</strong> #
                                    </td>
                                    <td align="left">
                                        &nbsp;<asp:Label ID="lblCurrPage" runat="server" Width="25px" CssClass="clslabel"
                                            Font-Bold="True"></asp:Label>
                                    </td>
                                    <td class="clslabel" align="right">
                                        &nbsp;<strong>Goto</strong> &nbsp;
                                    </td>
                                    <td align="right">
                                        <asp:DropDownList ID="ddlPageNo" runat="server" CssClass="clsinputcombo" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:DataGrid ID="dg_OutCome" runat="server" Width="100%" CssClass="clsLeftPaddingTable"
                    BorderColor="White" CellPadding="0" AllowPaging="True" AutoGenerateColumns="False"
                    AllowSorting="True" PageSize="30" OnItemCommand="dg_OutCome_ItemCommand">
                    <ItemStyle Height="18px"></ItemStyle>
                    <Columns>
                        <asp:TemplateColumn HeaderText="S#">
                            <HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" CssClass="GridItemStyle" VerticalAlign="Middle">
                            </ItemStyle>
                            <ItemTemplate>
                                <asp:HyperLink ID="hlk_Serial" runat="server">
                                </asp:HyperLink>                                
                                <asp:Label ID="lbl_Ticketid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ticketid_fk") %>'
                                    Visible="False">
                                </asp:Label>
                                <asp:Label ID="lbl_CallID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.callid") %>'
                                    Visible="False">
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="Bond" HeaderText="Bond" SortExpression="Bond">
                            <HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" CssClass="GridItemStyle" VerticalAlign="Middle">
                            </ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="eventtypename" SortExpression="eventtypename" HeaderText="Event Type">
                            <HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" CssClass="GridItemStyle" VerticalAlign="Middle">
                            </ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="calldatetime" SortExpression="calldateforsorting" HeaderText="Call Date & Time">
                            <HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Right" CssClass="GridItemStyle" VerticalAlign="Middle">
                            </ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="calldateforsorting" Visible="false">
                            <HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Right" CssClass="GridItemStyle" VerticalAlign="Middle">
                            </ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="lastname" SortExpression="lastname" HeaderText="Last Name">
                            <HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" CssClass="GridItemStyle" VerticalAlign="Middle">
                            </ItemStyle>
                        </asp:BoundColumn>                        
                        <asp:BoundColumn DataField="firstname" SortExpression="firstname" HeaderText="First Name">
                            <HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" CssClass="GridItemStyle" VerticalAlign="Middle">
                            </ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="contactnumber" HeaderText="Telephone No">
                            <HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Right" CssClass="GridItemStyle" VerticalAlign="Middle">
                            </ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="settinginfo" HeaderText="Setting Information">
                            <HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" CssClass="GridItemStyle" VerticalAlign="Middle">
                            </ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="responsetype" SortExpression="responsetype" HeaderText="Response">
                            <HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" CssClass="GridItemStyle" VerticalAlign="Middle">
                            </ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn>
                            <HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" CssClass="GridItemStyle" VerticalAlign="Middle">
                            </ItemStyle>
                            <ItemTemplate>
                                <asp:ImageButton ID="img_Record" Visible="false" ImageUrl="~/Images/phone_recorder.gif"
                                    CommandName="Listen" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.recordingpath") %>'
                                    runat="server" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle NextPageText="&nbsp; Next >" PrevPageText="< Prev &nbsp;" HorizontalAlign="Center">
                    </PagerStyle>
                </asp:DataGrid>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td background="../images/separator_repeat.gif" height="11">
            </td>
        </tr>
        <tr>
            <td style="width: 760px" align="left" colspan="4">
                <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.backroom
{
	/// <summary>
	/// Summary description for FrmAddNewCategory.
	/// </summary>
	public partial class FrmAddNewCategory : System.Web.UI.Page
	{
		
		protected System.Web.UI.WebControls.Button btn_submit;
		protected System.Web.UI.WebControls.Label lblMessage;
		
		clsENationWebComponents ClsDb = new clsENationWebComponents();
		protected System.Web.UI.WebControls.TextBox txt_CategoryDescription;
		protected System.Web.UI.WebControls.TextBox txt_CategoryName;
		clsViolationCategory ClsViolationCategory=new clsViolationCategory();
		clsSession ClsSession=new clsSession();
		clsLogger clog = new clsLogger();


		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				btn_submit.Attributes.Add("OnClick","return saveClick();"); 
				//btn_submit.Attributes.Add("OnClick","closewin();");

				if (ClsSession.IsValidSession(this.Request)==false)
				{
					//Response.Redirect("frmlogin.aspx");
					HttpContext.Current.Response.Write("<script language='javascript'> opener.location.reload(); self.close();   </script>");
				}
				if(! IsPostBack)
				{
					
					if (Request.QueryString.Count !=0  )
					{
						int intCategoryId = Convert.ToInt32(Request.QueryString["CategoryID"].ToString().Trim ());
						if (intCategoryId>0)
						{
							DataSet ds=ClsViolationCategory.GetCategory(intCategoryId);

							string strCategoryName = ((string)Convert.ChangeType(ds.Tables[0].Rows[0]["CategoryName"],typeof(string)) );
							string strCategoryDescription = ((string)Convert.ChangeType( ds.Tables[0].Rows[0]["CategoryDescription"],typeof(string)) );
                            string strShortDescription = ((string)Convert.ChangeType(ds.Tables[0].Rows[0]["shortdescription"], typeof(string)));

							txt_CategoryName.Text = strCategoryName;
							txt_CategoryDescription.Text = strCategoryDescription;
                            txt_shortdesc.Text = strShortDescription;
						}
					}
				}
			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btn_submit.Click += new System.EventHandler(this.btn_submit_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

        private void btn_submit_Click(object sender, System.EventArgs e)
		{
			try
			{

//				string strCategoryId = Request.QueryString["CategoryID"].ToString();
//				if (strCategoryId!="")
//				{
//					string[] key = {"@CategoryID"};
//					object[] value1 = {strCategoryId};
//					DataSet ds = ClsDb.Get_DS_BySPArr("usp_HTS_GetAllCategories",key,value1);
//					
//					string strCategoryName = ((string)Convert.ChangeType(ds.Tables[0].Rows[0]["CategoryName"],typeof(string)) );
//					string strCategoryDescription = ((string)Convert.ChangeType( ds.Tables[0].Rows[0]["CategoryDescription"],typeof(string)) );
//
//					txt_CategoryName.Text = strCategoryName;
//					txt_CategoryDescription.Text = strCategoryDescription;
//					btn_submit.Text = "Update";


				if (Request.QueryString.Count!=0 )
				{
					//QueryString["CategoryID"]!="" means edit
					int intCategoryId = Convert.ToInt32(Request.QueryString["CategoryID"].ToString().Trim ());
					if (intCategoryId>0)
					{
						ClsViolationCategory.CategoryID = intCategoryId;
						ClsViolationCategory.CategoryName = txt_CategoryName.Text;
						ClsViolationCategory.CategoryDescription = txt_CategoryDescription.Text;
                        ClsViolationCategory.ShortDescription = txt_shortdesc.Text;

						bool isUpdated = ClsViolationCategory.UpdateCategory();
						if(isUpdated==false)
						{
							lblMessage.Text = "Category Name '"+ txt_CategoryName.Text + "' is already exist";
						}
						else
						{
							lblMessage.ForeColor=System.Drawing.Color.Blue;
							lblMessage.Text = "Category Name  '"+ txt_CategoryName.Text + "' is updated successfully";
							HttpContext.Current.Response.Write("<script language='javascript'> opener.location.reload(); self.close();   </script>");
						}

					}
				}
				else
				{
					//QueryString["CategoryID"]=="" means add
					ClsViolationCategory.CategoryName = txt_CategoryName.Text;
					ClsViolationCategory.CategoryDescription = txt_CategoryDescription.Text;
                    ClsViolationCategory.ShortDescription = txt_shortdesc.Text;
					if(ClsViolationCategory.CategoryName!="")
					{
						bool isAdd =  ClsViolationCategory.AddCategory();
						if(isAdd==false)
						{
							lblMessage.Text = "Category Name '"+ txt_CategoryName.Text + "' is already exist";
						}
						else
						{
							lblMessage.ForeColor=System.Drawing.Color.Blue;
							lblMessage.Text = "Category Name '"+  txt_CategoryName.Text + "' is added successfully";
							HttpContext.Current.Response.Write("<script language='javascript'> opener.location.reload(); self.close();   </script>");
						}
					}

				}
			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}

		
		}

        
	}
}

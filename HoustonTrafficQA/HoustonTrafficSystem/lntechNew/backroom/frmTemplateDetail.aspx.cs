using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.Common;

using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;


namespace lntechNew
{
	/// <summary>
	/// Summary description for frmAddPricePlan.
	/// </summary>
	public partial class frmTemplateDetail: System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.DataGrid dg_newplan;
		protected System.Web.UI.WebControls.Label lblMessage;
		protected System.Web.UI.WebControls.TextBox txtTemplateName;
		protected System.Web.UI.WebControls.TextBox txtTemplateDesc;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.HtmlControls.HtmlInputHidden txtHidden;

		clsENationWebComponents ClsDb = new clsENationWebComponents();
		clsPricingPlans ClsPricingPlans = new clsPricingPlans();
		protected System.Web.UI.WebControls.HyperLink HyperLink1;
		clsSession ClsSession=new clsSession();
		clsLogger clog= new clsLogger();
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				if (ClsSession.IsValidSession(this.Request)==false)
				{
					Response.Redirect("../frmlogin.aspx",false);
				}
				else //To stop page further execution
				{
					if (ClsSession.GetCookie("sAccessType",this.Request).ToString()!="2")
					{
						Response.Redirect("../LoginAccesserror.aspx",false);
						Response.End();
					}
					else //To stop page further execution
					{
						btnSave.Attributes.Add("OnClick","return ValidateInput();"); 

						//				dg_newplan.DataSource = ClsDb.Get_DS_BySP("temp_priceplan");
						//				dg_newplan.DataBind();
						if( !IsPostBack)
						{
							if( Page.Request.QueryString.Count>0)//means page is called for update
							{
								//getTemplates (Master Table) for update
								int intTemplateID = Convert.ToInt32(Request.QueryString["TemplateID"]);
								DataSet ds = ClsPricingPlans.GetPricingTemplates(intTemplateID);
								txtTemplateName.Text = ds.Tables[0].Rows[0]["TemplateName"].ToString();
								txtTemplateDesc.Text = ds.Tables[0].Rows[0]["TemplateDescription"].ToString();

								//getTemplatesDetail (Detail Table) for update
								ds = ClsPricingPlans.GetPricingTemplateDetails(intTemplateID); // with Param
								dg_newplan.DataSource = ds;
								dg_newplan.DataBind();
                        
								//save total rows of grid into hidden textBox
								int count = dg_newplan.Items.Count;
								txtHidden.Value = Convert.ToString(count);
								//SetJavaScript();

								//change btn Text to Update
								btnSave.Text = "Update Template";
							}
							else //means page is called for Add
							{
								//getTemplateDetail (Detail Table) for Add
								DataSet ds = ClsPricingPlans.GetPricingTemplateDetails();// without param
								dg_newplan.DataSource = ds;
								dg_newplan.DataBind();
								//save total rows of grid into hidden textBox
								int count = dg_newplan.Items.Count;
								txtHidden.Value = Convert.ToString(count);
								//SetJavaScript();

								//change btn Text to Add
								btnSave.Text = "Add Template";
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
			
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			if(Request.QueryString.Count>0)
			{
				//if page is called with query string then update
				update();
			}
			else
			{
				//if page is called without query string then add
				add();
			}
		}

		private void update()
		{
			string error0 = "Template Name already exist";

			try
			{
				
				// Begin Transaction
                //DbTransaction trans = ClsDb.DBBeginAndReturnTransaction();
                DbTransaction trans = ClsDb.DBBeginAndReturnTransaction();

				//Update Master Table (PricingTemplates)
				string strTemplateID = Request.QueryString["TemplateID"].ToString();
				ClsPricingPlans.TemplateID = ((int) Convert.ChangeType(strTemplateID,typeof(int)));
				ClsPricingPlans.TempateName = txtTemplateName.Text;
				ClsPricingPlans.TemplateDescription = txtTemplateDesc.Text;
				ClsPricingPlans.EmployeeID = Convert.ToInt32(ClsSession.GetCookie("sEmpID",this.Request));

				int ret = ClsPricingPlans.UpdatePricingTemplate(trans); //for master 
				if ( ret==0 )
				{
					//Template Name already exist
					ClsDb.DBTransactionRollback();
					lblMessage.Text = error0;
					return;
				}


				//Update Detail Table (PricingTemplateDetails)
				foreach(DataGridItem item in dg_newplan.Items)
				{
					ClsPricingPlans.DetailId = ToInt(((Label)(item.FindControl("lbl_DetailID"))).Text);
					ClsPricingPlans.TemplateID = ToInt(strTemplateID); // from Query String
					ClsPricingPlans.CategoryID = ToInt(((Label)(item.FindControl("lbl_CategoryID"))).Text);

					ClsPricingPlans.BasePrice = Convert.ToDouble( ((TextBox)(item.FindControl("txt_BasePrice"))).Text);
					ClsPricingPlans.SecondaryPrice = Convert.ToDouble( ((TextBox)(item.FindControl("txt_secprice"))).Text);
					ClsPricingPlans.BasePercentage = Convert.ToDouble(((TextBox)(item.FindControl("txt_basepcent"))).Text);
					ClsPricingPlans.SecondaryPercentage = Convert.ToDouble(((TextBox)(item.FindControl("txt_secpcent"))).Text);
					ClsPricingPlans.BondBase = Convert.ToDouble(((TextBox)(item.FindControl("txt_bondbase"))).Text);
					ClsPricingPlans.BondSecondary = Convert.ToDouble(((TextBox)(item.FindControl("txt_bondsec"))).Text);
					ClsPricingPlans.BondBasePercentage = Convert.ToDouble(((TextBox)(item.FindControl("txt_bondbasepcent"))).Text);
					ClsPricingPlans.BondSecondaryPercentage = Convert.ToDouble(((TextBox)(item.FindControl("txt_bondsecpcent"))).Text);
					ClsPricingPlans.BondAssumtion = Convert.ToDouble(((TextBox)(item.FindControl("txt_bondassump"))).Text);
					ClsPricingPlans.FineAssumption = Convert.ToDouble(((TextBox)(item.FindControl("txt_fineassump"))).Text);

					ClsPricingPlans.UpdatePricingTemplateDetail(trans);//for detail Table
				}

				//On successfull update both master and detail tables, do Commit
				ClsDb.DBTransactionCommit();
			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message;
				//on Exception do Rollback
				ClsDb.DBTransactionRollback();
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}

		}

		private int ToInt(string str)
		{
			if (str.Trim()!="" )
			{
				//str = (str.IndexOf(".")==-1? str: str.Substring(0,str.IndexOf(".") )) ; 
				str.TrimStart('-');
				if(str.IndexOf(".")!=-1)
				{
					double d = Convert.ToDouble(str);
					d = Math.Round(d);
					return Convert.ToInt32(d);
				}
				else
				{
					return 	Convert.ToInt32(str);
				}
			}
			else
			{
				return 0;
			}
		}
		
		public void add()
		{
			string error0 = "Template Name already exist";
			try
			{
				// Begin Transaction
				DbTransaction trans =  ClsDb.DBBeginAndReturnTransaction();
				

				//add Master Table (PricingTemplates)
				ClsPricingPlans.TempateName = txtTemplateName.Text;
				ClsPricingPlans.TemplateDescription = txtTemplateDesc.Text;
				ClsPricingPlans.EmployeeID = Convert.ToInt32(ClsSession.GetCookie("sEmpID",this.Request));
				
				int intTemplate = ClsPricingPlans.AddPricingTemplate(trans); //for master 
				if ( intTemplate==0 ) // means no insertion
				{
					ClsDb.DBTransactionRollback();
					lblMessage.Text = error0;
					return;
				}


				//Add Detail Table (PricingTemplateDetails)
				foreach(DataGridItem item in dg_newplan.Items)
				{
//					Hashtable ht = new Hashtable();
//					ht["DetailId"] =				((Label)(item.FindControl("lbl_DetailID"))).Text;
//					ht["TemplateID"] =				intTemplate;	//((Label)(item.FindControl("lbl_TemplateID"))).Text;
//					ht["CategoryId"] =				((Label)(item.FindControl("lbl_CategoryID"))).Text;
//					ht["BasePrice"] =				((TextBox)(item.FindControl("txt_BasePrice"))).Text;
//					ht["SecondaryPrice"] =			((TextBox)(item.FindControl("txt_secprice"))).Text;
//					ht["BasePercentage"] =			((TextBox)(item.FindControl("txt_basepcent"))).Text;
//					ht["SecondaryPercentage"] =		((TextBox)(item.FindControl("txt_secpcent"))).Text;
//					ht["BondBase"] =				((TextBox)(item.FindControl("txt_bondbase"))).Text;
//					ht["BondSecondary"] =			((TextBox)(item.FindControl("txt_bondsec"))).Text;
//					ht["BondBasePercentage"] =		((TextBox)(item.FindControl("txt_bondbasepcent"))).Text;
//					ht["BondSecondaryPercentage"] =	((TextBox)(item.FindControl("txt_bondsecpcent"))).Text;
//					ht["BondAssumption"] =			((TextBox)(item.FindControl("txt_bondassump"))).Text;
//					ht["FineAssumption"] =			((TextBox)(item.FindControl("txt_fineassump"))).Text;
//					ClsPricingPlans.AddPricingTemplateDetail(ht);//for detail Table

					ClsPricingPlans.DetailId = ToInt(((Label)(item.FindControl("lbl_DetailID"))).Text);
					ClsPricingPlans.TemplateID = intTemplate; // from Adding method to Master Table
					ClsPricingPlans.CategoryID = ToInt(((Label)(item.FindControl("lbl_CategoryID"))).Text);

					ClsPricingPlans.BasePrice = Convert.ToDouble( ((TextBox)(item.FindControl("txt_BasePrice"))).Text);
					ClsPricingPlans.SecondaryPrice = Convert.ToDouble( ((TextBox)(item.FindControl("txt_secprice"))).Text);
					ClsPricingPlans.BasePercentage = Convert.ToDouble(((TextBox)(item.FindControl("txt_basepcent"))).Text);
					ClsPricingPlans.SecondaryPercentage = Convert.ToDouble(((TextBox)(item.FindControl("txt_secpcent"))).Text);
					ClsPricingPlans.BondBase = Convert.ToDouble(((TextBox)(item.FindControl("txt_bondbase"))).Text);
					ClsPricingPlans.BondSecondary = Convert.ToDouble(((TextBox)(item.FindControl("txt_bondsec"))).Text);
					ClsPricingPlans.BondBasePercentage = Convert.ToDouble(((TextBox)(item.FindControl("txt_bondbasepcent"))).Text);
					ClsPricingPlans.BondSecondaryPercentage = Convert.ToDouble(((TextBox)(item.FindControl("txt_bondsecpcent"))).Text);
					ClsPricingPlans.BondAssumtion = Convert.ToDouble(((TextBox)(item.FindControl("txt_bondassump"))).Text);
					ClsPricingPlans.FineAssumption = Convert.ToDouble(((TextBox)(item.FindControl("txt_fineassump"))).Text);

					ClsPricingPlans.AddPricingTemplateDetail(trans);//for detail Table

				}

				//On successfull update both master and detail tables, do Commit
				ClsDb.DBTransactionCommit();
			}
			catch(Exception ex)
			{
				//on Exception do Rollback
				ClsDb.DBTransactionRollback();
				lblMessage.Text = ex.Message;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}

		private void SetJavaScript()
		{
			
			foreach ( DataGridItem  items in dg_newplan.Items)
			{			
				try
				{
					((TextBox) items.FindControl("txt_BasePrice")).Attributes.Add("onchange","javascript: ValidateBasePrice('" + ((TextBox) items.FindControl("txt_BasePrice")).ClientID +"');");
//					((TextBox) items.FindControl("txt_secprice")).Attributes.Add("onchange","javascript: ValidateSecPrice('" + ((TextBox) items.FindControl("txt_secprice")).ClientID +"');");
//					((TextBox) items.FindControl("txt_basepcent")).Attributes.Add("onchange","javascript: ValidateBasePcent('" + ((TextBox) items.FindControl("txt_basepcent")).ClientID +"');");
//					((TextBox) items.FindControl("txt_secpcent")).Attributes.Add("onchange","javascript: ValidateSecPrice('" + ((TextBox) items.FindControl("txt_secpcent")).ClientID +"');");
//					((TextBox) items.FindControl("txt_bondbase")).Attributes.Add("onchange","javascript: ValidateBondBase('" + ((TextBox) items.FindControl("txt_bondbase")).ClientID +"');");
//					((TextBox) items.FindControl("txt_bondsec")).Attributes.Add("onchange","javascript: ValidateBondSec('" + ((TextBox) items.FindControl("txt_bondsec")).ClientID +"');");
//					((TextBox) items.FindControl("txt_bondbasepcent")).Attributes.Add("onchange","javascript: ValidateBondBasePcent('" + ((TextBox) items.FindControl("txt_bondbasepcent")).ClientID +"');");
//					((TextBox) items.FindControl("txt_bondsecpcent")).Attributes.Add("onchange","javascript: ValidateBondSecPcent('" + ((TextBox) items.FindControl("txt_bondsecpcent")).ClientID +"');");
//					((TextBox) items.FindControl("txt_bondassump")).Attributes.Add("onchange","javascript: ValidateBondAssump('" + ((TextBox) items.FindControl("txt_bondassump")).ClientID +"');");
//					((TextBox) items.FindControl("txt_fineassump")).Attributes.Add("onchange","javascript: ValidateFineAssump('" + ((TextBox) items.FindControl("txt_fineassump")).ClientID +"');");

				
				}
				catch(Exception ex)
				{
					lblMessage.Text = ex.Message;
					clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
				}
			}
		}

	}
}

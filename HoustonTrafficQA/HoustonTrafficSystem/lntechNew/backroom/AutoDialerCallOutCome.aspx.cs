using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using lntechNew.Components.ClientInfo;
using HTP.Components;

namespace HTP.backroom
{
    /// <summary>
    /// Summary description for AutoDialerCallOutCome.
    /// </summary>
    public partial class AutoDialerCallOutCome : Page
    {
        #region Variables/others

        protected DropDownList ddl_Courts;
        protected DropDownList ddl_Events;
        protected CheckBox chk_showall;
        protected eWorld.UI.CalendarPopup dtFrom;
        protected DataGrid dg_OutCome;
        protected Button btnSubmit;
        protected eWorld.UI.CalendarPopup dtTo;
        protected Label lblCurrPage;
        protected DropDownList ddlPageNo;
        protected Label lblMsg;


        DataTable _dtRes;
        readonly AutoDialer _clsAD = new AutoDialer();
        readonly clsSession _cSession = new clsSession();
        readonly clsLogger _bugTracker = new clsLogger();
        DataView _dvResult;
        string _strExp = String.Empty;
        string _strAcsDec = String.Empty;

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            this.ddlPageNo.SelectedIndexChanged += new System.EventHandler(this.ddlPageNo_SelectedIndexChanged);
            this.dg_OutCome.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dg_OutCome_PageIndexChanged);
            this.dg_OutCome.SortCommand += new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.dg_OutCome_SortCommand);
            this.dg_OutCome.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dg_OutCome_ItemDataBound);
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion

        #region Events

        private void Page_Load(object sender, EventArgs e)
        {
            //Waqas 5057 03/17/2009 Checking employee info in session
            if (_cSession.IsValidSession(Request, Response, Session) == false)
            {
                Response.Redirect("../frmlogin.aspx", false);
            }
            else //To stop page further execution
            {
                if (!IsPostBack)
                {
                    btnSubmit.Attributes.Add("OnClick", "return validate();");
                    dtTo.SelectedDate = DateTime.Today;
                    dtFrom.SelectedDate = DateTime.Today;
                    FillEvents();
                    FillResponseType();
                    Search();
                }
            }
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                Search();
            }
            catch (Exception ex)
            {
                lblMsg.Text = ex.Message;
                _bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void dg_OutCome_ItemDataBound(object sender, DataGridItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                try
                {
                    var iTicketId = (int)(Convert.ChangeType(((Label)(e.Item.FindControl("lbl_Ticketid"))).Text, typeof(int)));

                    ((HyperLink)(e.Item.FindControl("hlk_Serial"))).NavigateUrl = "../ClientInfo/ViolationFeeOld.aspx?search=0&casenumber=" + iTicketId;
                    // changing row color on mouse hover.....
                    e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='#FFF2D9'");
                    e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#EFF4FB'");
                    var img = (ImageButton)e.Item.FindControl("img_Record");
                    img.Visible = img.CommandArgument != "";
                }
                catch
                {

                }
            }
        }

        private void dg_OutCome_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dg_OutCome.CurrentPageIndex = e.NewPageIndex;
            FillGrid();
        }

        private void dg_OutCome_SortCommand(object source, DataGridSortCommandEventArgs e)
        {
            lblMsg.Text = "";
            SortGrid(e.SortExpression);
        }

        private void ddlPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            dg_OutCome.CurrentPageIndex = ddlPageNo.SelectedIndex;
            FillGrid();
        }

        protected void dg_OutCome_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            if (e.CommandName == "Listen")
            {
                string recPath = e.CommandArgument.ToString();
                recPath = recPath.Remove(0, recPath.ToUpper().IndexOf("DOCSTORAGE"));
                recPath = recPath.Replace("\\", "/");
                recPath = "../" + recPath;
                HttpContext.Current.Response.Write("<script>window.open('" + recPath + "','','fullscreen=no,toolbar=no,width=2,height=2,left=0,top=0,status=no,menubar=no,resizable=no');</script>");
            }
        }

        #endregion

        #region Methods

        private void FillEvents()
        {
            var dt = _clsAD.GetAllEventTypes();
            int idx;
            for (idx = -1; idx < dt.Rows.Count; idx++)
            {
                switch (idx)
                {
                    case -1:
                        ddl_Events.Items.Add(new ListItem("All", "0"));
                        break;
                    default:
                        ddl_Events.Items.Add(new ListItem(dt.Rows[idx]["eventtypename"].ToString(), dt.Rows[idx]["eventtypeid"].ToString()));
                        break;
                }
            }

        }

        private void FillResponseType()
        {
            var dt = _clsAD.GetResponseTypes();
            chkbl_ResponseType.DataSource = dt;
            chkbl_ResponseType.DataTextField = "responsetype";
            chkbl_ResponseType.DataValueField = "responseid";
            chkbl_ResponseType.DataBind();
            hf_ResponseCount.Value = dt.Rows.Count.ToString();

            for (var i = 0; i < dt.Rows.Count; i++)
            {
                chkbl_ResponseType.Items[i].Selected = true;
            }
        }

        private void FillGrid()
        {
            try
            {
                //Fahad 8556 12/20/2010 Renamed the DataView 
                var dvTemp = (DataView)_cSession.GetSessionObject("dvOutComeResult", Session);
                dg_OutCome.DataSource = dvTemp;
                dg_OutCome.DataBind();
                GenerateSerial();
                FillPageList();
                SetNavigation();
                if (dg_OutCome.Items.Count == 0)
                    lblMsg.Text = "No record found.";
            }
            catch (Exception ex)
            {
                if (dg_OutCome.CurrentPageIndex > dg_OutCome.PageCount - 1)
                {
                    dg_OutCome.CurrentPageIndex = 0;
                    dg_OutCome.DataBind();
                    GenerateSerial();
                    SetNavigation();
                }
                else
                {
                    lblMsg.Text = ex.Message;
                    _bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

                }
            }

        }

        private void PullData()
        {
            _clsAD.EventTypeID = Convert.ToInt16(ddl_Events.SelectedValue);
            _clsAD.CourtID = Convert.ToInt32(ddl_Courts.SelectedValue);
            _clsAD.ResponseTypeIDs = GetSeletedResponseTypes();

            if ((dtFrom.SelectedDate.ToShortDateString() == "1/1/0001" && dtTo.SelectedDate.ToShortDateString() == "1/1/0001") || chk_showall.Checked)
            {
                _clsAD.ShowAll = true;
                _clsAD.StartDate = DateTime.Now;
                _clsAD.EndDate = DateTime.Now;
                _dtRes = _clsAD.GetCallOutComes();
            }
            else if (chk_showall.Checked == false)
            {
                _clsAD.ShowAll = false;
                _clsAD.StartDate = dtFrom.SelectedDate;
                _clsAD.EndDate = dtTo.SelectedDate;
                _dtRes = _clsAD.GetCallOutComes();
            }
            //Fahad 8147 09/27/2010 Renamed the DataView 
            _cSession.SetSessionVariable("dvOutComeResult", _dtRes.DefaultView, Session);
        }

        private void GenerateSerial()
        {
            // Procedure for writing serial numbers in "S.No." column in data grid.......	


            long sNo = (dg_OutCome.CurrentPageIndex) * (dg_OutCome.PageSize);
            try
            {
                // looping for each row of record.............
                foreach (DataGridItem itemX in dg_OutCome.Items)
                {

                    try
                    {
                        var iTicketId = (int)(Convert.ChangeType(((Label)(itemX.FindControl("lbl_CallID"))).Text, typeof(int)));
                        sNo += 1;
                        // setting text of hyper link to serial number after bouncing of hyper link...
                        ((HyperLink)(itemX.FindControl("hlk_Serial"))).Text = (string)Convert.ChangeType(sNo, typeof(string));
                    }
                    catch
                    { }


                }
            }
            catch (Exception ex)
            {
                _bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMsg.Text = ex.Message;
            }
        }

        private void SortGrid(string sortExp)
        {
            try
            {
                SetAcsDesc(sortExp);
                //Fahad 8556 12/20/2010 Renamed the DataView 
                _dvResult = (DataView)(_cSession.GetSessionObject("dvOutComeResult", Session));
                _dvResult.Sort = _strExp + " " + _strAcsDec;
                //Fahad 8147 09/27/2010 Renamed the DataView 
                _cSession.SetSessionVariable("dvOutComeResult", _dvResult, Session);
                FillGrid();

            }
            catch (Exception ex)
            {
                lblMsg.Text = ex.Message;
                _bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

            }
        }

        private void SetAcsDesc(string val)
        {
            try
            {
                //Fahad 8147 09/27/2010 Renamed the DataView 
                _strExp = _cSession.GetSessionVariable("StrOutComeExp", Session);
                //Fahad 8147 09/27/2010 Renamed the DataView 
                _strAcsDec = _cSession.GetSessionVariable("StrOutComeAcsDec", Session);
            }
            catch
            {

            }

            if (_strExp == val)
            {
                if (_strAcsDec == "ASC")
                {
                    _strAcsDec = "DESC";
                    //Fahad 8147 09/27/2010 Renamed the DataView 
                    _cSession.SetSessionVariable("StrOutComeAcsDec", _strAcsDec, Session);
                }
                else
                {
                    _strAcsDec = "ASC";
                    //Fahad 8147 09/27/2010 Renamed the DataView 
                    _cSession.SetSessionVariable("StrOutComeAcsDec", _strAcsDec, Session);
                }
            }
            else
            {
                _strExp = val;
                _strAcsDec = "ASC";
                //Fahad 8147 09/27/2010 Renamed the DataView 
                _cSession.SetSessionVariable("StrOutComeExp", _strExp, Session);
                //Fahad 8147 09/27/2010 Renamed the DataView 
                _cSession.SetSessionVariable("StrOutComeAcsDec", _strAcsDec, Session);
            }
        }

        private void SetNavigation()
        {
            // Procedure for setting data grid's current  page number on header ........
            try
            {
                // setting current page number
                lblCurrPage.Text = Convert.ToString(dg_OutCome.CurrentPageIndex + 1);
                for (var idx = 0; idx < ddlPageNo.Items.Count; idx++)
                {
                    ddlPageNo.Items[idx].Selected = false;
                }
                //Nasir 6291 11/24/2009 implement check if datagrid index is greater than ddl count then set zero to stop exception
                if (ddlPageNo.Items.Count > dg_OutCome.CurrentPageIndex)
                {
                    ddlPageNo.Items[dg_OutCome.CurrentPageIndex].Selected = true;
                }
                else
                {
                    ddlPageNo.Items[ddlPageNo.Items.Count - 1].Selected = true;
                }

            }
            catch (Exception ex)
            {
                lblMsg.Text = ex.Message;
                _bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

            }
        }

        private void FillPageList()
        {
            try
            {
                int i;
                ddlPageNo.Items.Clear();
                for (i = 1; i <= dg_OutCome.PageCount; i++)
                {
                    ddlPageNo.Items.Add("Page - " + i);
                    ddlPageNo.Items[i - 1].Value = i.ToString();
                }
            }
            catch (Exception ex)
            {
                lblMsg.Text = ex.Message;
                _bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

            }

        }

        private void Search()
        {
            lblMsg.Text = "";
            dg_OutCome.CurrentPageIndex = 0;
            PullData();
            FillGrid();
        }

        private string GetSeletedResponseTypes()
        {
            var ids = String.Empty;
            for (var i = 0; i < chkbl_ResponseType.Items.Count; i++)
            {
                if (chkbl_ResponseType.Items[i].Selected)
                {
                    ids = ids + chkbl_ResponseType.Items[i].Value + ",";
                }
            }
            return ids;
        }

        #endregion

    }
}

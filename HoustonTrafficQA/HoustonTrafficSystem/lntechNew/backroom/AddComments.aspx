<%@ Page language="c#" Codebehind="AddComments.aspx.cs" AutoEventWireup="True" Inherits="lntechNew.backroom.AddComments" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>AddComments</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript">
		function Validation()
		{ 
			var txtdesc=document.getElementById("txt_Desc").value;
			if(txtdesc == "")
			{ 
				alert("PLease Enter Comments");
				document.getElementById("txt_Desc").focus();
				return false;
			}
			
			if(txtdesc.length > 2000)
			{
			    alert("You Cannot Type More Than 2000 Characters In Comments Box");
				document.getElementById("txt_Desc").focus();
				return false;
			}
		}
		function setfocus()
		{
		    document.getElementById("txt_Desc").focus();
		}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout" bottomMargin="0" topMargin="0" leftmargin="0" onload="setfocus();">
		<form id="Form1" method="post" runat="server">
			<TABLE id="TableMain" cellSpacing="0" cellPadding="0" width="400"  border="0">
				<TR>
					<TD class="clssubhead" background="../Images/subhead_bg.gif" colSpan="2" height="34">&nbsp; Comments:&nbsp;
					</TD>
				</TR>
				<TR>
					<TD vAlign="top" colSpan="2">
						<TABLE id="tbl1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD class="clsLeftPaddingTable" align="left" colspan="2">
									<asp:TextBox id="txt_Desc" runat="server" CssClass="clstextarea" Width="393px" TextMode="MultiLine"
										Height="65px" MaxLength="2000"></asp:TextBox></TD>
							</TR>
							<tr>
							<TD class="clsLeftPaddingTable" align="right" style="height:5px; width: 235px;"></TD>
							<TD class="clsLeftPaddingTable" align="right" style="height:5px"></TD>
							</tr>
							<TR>
								<TD class="clsLeftPaddingTable" align="center">
									<asp:Button id="btnSubmit" runat="server" Text="Add Comments" CssClass="clsbutton" onclick="btnSubmit_Click" Width="100px"></asp:Button></TD>
							</TR>
							<TR>
								<TD class="clsLeftPaddingTable" align="center" colSpan="2"></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>

<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.backroom.TemplateMain" Codebehind="TemplateMain.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
	<head>
		<title>Templates</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C#" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema"/>
		<link href="../Styles.css" type="text/css" rel="stylesheet"/>
	</head>
	<body >
		<form id="Form1" method="post" runat="server">
			<table id="TableMain" cellspacing="0" cellpadding="0" width="780" align="center" border="0">
				<tr>
					<td>
						<uc1:ActiveMenu id="ActiveMenu1" runat="server"></uc1:ActiveMenu></td>
				</tr>
				<tr>
					<td background="../../images/separator_repeat.gif" height="11"></td>
				</tr>
				<tr>
					<td colspan="2">
						<table id="tableSub" align="center" cellspacing="0" cellpadding="0"
							width="780" border="0">

							<tr>
								<td class="clssubhead" background="../../Images/subhead_bg.gif" height="34">&nbsp;Templates</td>
								<td class="clssubhead" align="right" background="../../Images/subhead_bg.gif"
									height="25"><A href="frmTemplateDetail.aspx">Add New Template&nbsp;</A>&nbsp;</td>
							</tr>
							
							<tr>
								<td colspan="4">
									<table id="tblTemplate" cellspacing="0" cellpadding="0" width="100%" border="0">
										<tr>
											<td><asp:datagrid id="dg_template" runat="server" Width="100%" CssClass="clsLeftPaddingTable" AutoGenerateColumns="False"
													PageSize="20" AllowPaging="True">
													<Columns>
														<asp:TemplateColumn Visible="False" HeaderText="TemplateID">
															<ItemTemplate>
																<asp:Label id="lblTemplateID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TemplateID") %>'>
																</asp:Label>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Name">
															<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
															<ItemTemplate>
																<asp:HyperLink id="hlnkPriceTemplates" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TemplateName") %>'>
																</asp:HyperLink>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Description">
															<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
															<ItemTemplate>
																<asp:Label id="lblTemplateDesc" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.TemplateDescription") %>'>
																</asp:Label>
															</ItemTemplate>
														</asp:TemplateColumn>
													</Columns>
													<PagerStyle NextPageText="Next &gt;&gt;" PrevPageText="&lt;&lt; Previous" HorizontalAlign="Center"></PagerStyle>
												</asp:datagrid></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
							    <td colspan="6">
							        <asp:label id="lblMessage" runat="server"  ForeColor="Red"></asp:label>
							    </td>
							</tr>
							<tr>
								<td background="../../images/separator_repeat.gif" colspan="7" height="11"></td>
							</tr>
							<tr>
								<td colspan="6"><uc1:footer id="Footer1" runat="server"></uc1:footer></td>
							</tr>
							
						</table>						
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using HTP.Components;

namespace HTP.backroom
{
    public partial class AutoDialerSettingsHistory : System.Web.UI.Page
    {
        #region Variables

        AutoDialer clsAD = new AutoDialer();
        clsSession cSession = new clsSession();
        clsLogger BugTracker = new clsLogger();

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            //Waqas 5057 03/17/2009 Checking employee info in session
            if (cSession.IsValidSession(this.Request, this.Response, this.Session) == false)
            {
                Response.Redirect("../frmlogin.aspx", false);
            }
            else //To stop page further execution
            {
                if (cSession.GetCookie("sAccessType", this.Request).ToString() != "2")
                {
                    Response.Redirect("../LoginAccesserror.aspx", false);
                    Response.End();
                }
                else //To stop page further execution
                {
                    if (!IsPostBack)
                    {
                        if (Request.QueryString["sMenu"] != "" & Request.QueryString["sMenu"] != null)
                        {
                            hlk_Settings.NavigateUrl = "~/backroom/AutoDialerSettings.aspx?sMenu=" + Request.QueryString["sMenu"].ToString();
                        }
                        BindGrid();
                    }
                    Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
                }
            }
        }

        protected void gv_Records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            if (e.NewPageIndex != -1)
            {
                gv_Records.PageIndex = e.NewPageIndex;
                BindGrid();
            }
        }

        #endregion

        #region Methods

        protected void BindGrid()
        {
            try
            {
                lbl_Message.Text = String.Empty;

                DataTable dt = clsAD.GetEventConfigSettingsHistory();

                if (dt.Rows.Count == 0)
                {
                    lbl_Message.Text = "No records found";
                }

                gv_Records.DataSource = dt;
                gv_Records.DataBind();

                Pagingctrl.PageCount = gv_Records.PageCount;
                Pagingctrl.PageIndex = gv_Records.PageIndex;
                Pagingctrl.SetPageIndex();
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;
            }
        }

        void Pagingctrl_PageIndexChanged()
        {
            gv_Records.PageIndex = Pagingctrl.PageIndex - 1;
            BindGrid();
        }
        #endregion
    }
}

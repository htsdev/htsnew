<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NisiCompetitorReport.aspx.cs" Inherits="HTP.backroom.NisiCompetitorReport" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>NISI Competitor Report</title>
    <link href="../Styles.css" type="text/css" rel="stylesheet">
    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>
    <script src="../Scripts/Dates.js" type="text/javascript"></script>
    <script language="javascript">		
            function validate()
		    {			   
			    
			    var d1 = document.getElementById("calQueryFrom").value
			    var d2 = document.getElementById("calQueryTo").value			
    			if(d1 == '')
				{
				  alert("Please enter start date");
				  document.getElementById("calQueryFrom").focus(); 
				  return false;  
				}
				if(d2 == '')
				{
				  alert("Please enter end date");
				  document.getElementById("calQueryTo").focus(); 
				  return false;  
				}
			    if (d1 == d2)
				    return true;
			    if (compareDates(d2,'MM/dd/yyyy',d1,'MM/dd/yyyy')==false)
				{
					alert("Start date cannot greater than end date");
					document.getElementById("calQueryTo").focus(); 
					return false;
				}				
				return true;
		    }	    
    </script>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="TableMain" style="z-index: 200" cellspacing="0" cellpadding="0" width="780"
            align="center" border="0">
            <tbody>
                <tr>
                    <td style="height: 14px" colspan="4">
                        <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="TableSub" cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tr>
                                <td background="../../images/separator_repeat.gif" style="height: 11px" colspan="6">
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <table id="Table6" cellspacing="0" cellpadding="0" bgcolor="white" border="0" style="width: 100%;">
                                        <tr>
                                            <td align="left" style="width: 140;" valign="middle">
                                                Start Date:<ew:CalendarPopup ID="calQueryFrom" runat="server" Nullable="True" Width="80px"
                                                    ImageUrl="../images/calendar.gif" Font-Names="Tahoma" Font-Size="8pt" ControlDisplay="TextBoxImage"
                                                    CalendarLocation="Left" ShowGoToToday="True" AllowArbitraryText="False" Culture="(Default)"
                                                    UpperBoundDate="12/31/9999 23:59:00" PadSingleDigits="True" ToolTip="Call Back Date"
                                                    ShowClearDate="True">
                                                    <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                                    <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                        BackColor="White"></WeekdayStyle>
                                                    <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                                    <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                                        BackColor="AntiqueWhite"></OffMonthStyle>
                                                    <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                                    <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                        BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                                    <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                                    <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                        BackColor="LightGray"></WeekendStyle>
                                                    <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                                    <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="White"></ClearDateStyle>
                                                    <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                        BackColor="White"></HolidayStyle>
                                                </ew:CalendarPopup>
                                            </td>
                                            <td align="left" style="width: 140;" valign="middle">
                                                End Date:
                                                <ew:CalendarPopup ID="calQueryTo" runat="server" Nullable="True" Width="80px" ImageUrl="../images/calendar.gif"
                                                    Font-Names="Tahoma" Font-Size="8pt" ControlDisplay="TextBoxImage" CalendarLocation="Left"
                                                    ShowGoToToday="True" AllowArbitraryText="False" Culture="(Default)" UpperBoundDate="12/31/9999 23:59:00"
                                                    PadSingleDigits="True" ToolTip="Call Back Date" ShowClearDate="True">
                                                    <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                                    <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                        BackColor="White"></WeekdayStyle>
                                                    <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                                    <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                                        BackColor="AntiqueWhite"></OffMonthStyle>
                                                    <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                                    <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                        BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                                    <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                                    <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                        BackColor="LightGray"></WeekendStyle>
                                                    <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                                    <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="White"></ClearDateStyle>
                                                    <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                        BackColor="White"></HolidayStyle>
                                                </ew:CalendarPopup>
                                            </td>
                                            <td align="left" style="width: 75;" valign="bottom">
                                                <asp:DropDownList ID="ddlBreakDown" runat="server" CssClass="clsinputcombo">
                                                    <asp:ListItem Value="Monthly">Monthly</asp:ListItem>
                                                    <asp:ListItem Value="Quarterly">Quarterly</asp:ListItem>
                                                    <asp:ListItem Value="Yearly">Yearly</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td align="left" style="width: 75;" valign="bottom">
                                                <asp:DropDownList ID="ddlNoOfRecords" runat="server" CssClass="clsinputcombo">
                                                    <asp:ListItem Value="10">Top 10</asp:ListItem>
                                                    <asp:ListItem Value="20">Top 20</asp:ListItem>
                                                    <asp:ListItem Value="30">Top 30</asp:ListItem>
                                                    <asp:ListItem Value="40" Selected="True">Top 40</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td align="right" style="width: 100;" valign="bottom">
                                                <asp:Button ID="btn_submit" runat="server" CssClass="clsbutton" Text="Submit" OnClick="btn_submit_Click"
                                                    OnClientClick="return validate();"></asp:Button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td background="../../images/separator_repeat.gif" colspan="6" style="height: 7">
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:Label ID="lbl_message" runat="server" CssClass="label" ForeColor="Red"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table id="TableGrid" cellspacing="0" cellpadding="0" width="100%" bgcolor="white"
                                        border="0">
                                        <tr>
                                            <td background="../Images/subhead_bg.gif" class="clssubhead" align="right" style="height: 34px;
                                                width: 743px;">                                                
                                            </td>
                                        </tr>                                        
                                        <tr>
                                            <td>
                                            <asp:Repeater ID="repNisiCompititor" runat="server" OnItemDataBound="repNisiCompititor_ItemDataBound">
                                                        <ItemTemplate>
                                                            <table width="100%">
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblStartDate" runat="server" CssClass="Label"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:DataGrid ID="dgNisiCompitoter" runat="server" AutoGenerateColumns="False" BackColor="#EFF4FB"
                                                                            BorderColor="White" BorderStyle="None" CellPadding="0" CssClass="clsLeftPaddingTable"
                                                                            Width="100%">
                                                                            <Columns>
                                                                                <asp:TemplateColumn HeaderText="S.No">
                                                                                    <HeaderStyle CssClass="clssubhead" />
                                                                                    <ItemStyle VerticalAlign="Top" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lbl_sno" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.sno") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn HeaderText="Bondsmen/Attorney Name">
                                                                                    <HeaderStyle CssClass="clssubhead" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblAttorneyName" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.bondingCompanyName") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn HeaderText="NISI's Paid $">
                                                                                    <HeaderStyle CssClass="clssubhead" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblPaid" runat="server" CssClass="Label" Text='<%# String.Format("{0:C2}",DataBinder.Eval(Container, "DataItem.nisiPaid")) %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn HeaderText="NISI's issued">
                                                                                    <HeaderStyle CssClass="clssubhead" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblIssued" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.nisiIssued") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>                                                                               
                                                                            </Columns>
                                                                        </asp:DataGrid>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td background="../../images/separator_repeat.gif" style="width: 743px; height: 12px">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                            </td>
                                            
                                        </tr>
                                        <tr>
                                            <td background="../../images/separator_repeat.gif" style="width: 743px; height: 12px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" align="center" colspan="2">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td background="../../images/separator_repeat.gif" style="width: 743px; height: 12px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 743px">
                                                <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    </form>
</body>
</html>

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
//Waqas 5057 03/19/2009 Changed to HTP
namespace HTP.Backroom
{

    public partial class ExchangeDocket : System.Web.UI.Page
    {
        protected eWorld.UI.CalendarPopup calQueryFrom;

        protected System.Web.UI.WebControls.DataGrid dg_valrep;
        protected System.Web.UI.WebControls.Button btn_submit;
        DataSet ds_val;
        protected System.Web.UI.WebControls.Label lbl_message;
        protected System.Web.UI.WebControls.DropDownList cmbPageNo;
        protected System.Web.UI.WebControls.Label lblGoto;
        protected System.Web.UI.WebControls.Label lblPNo;
        protected System.Web.UI.WebControls.Label lblCurrPage;
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        clsSession cSession = new clsSession();
        clsLogger clog = new clsLogger();
        protected eWorld.UI.CalendarPopup calqueryTo;
        protected System.Web.UI.WebControls.DropDownList ddl_attorney;

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {	//Validating session
                //Waqas 5057 03/17/2009 Checking employee info in session
                if (cSession.IsValidSession(this.Request, this.Response, this.Session) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else //To stop page further execution
                {
                    if (cSession.GetCookie("sAccessType", this.Request).ToString() != "2")
                    {
                        Response.Redirect("../LoginAccesserror.aspx", false);
                        Response.End();
                    }
                    else //To stop page further execution
                    {

                        if (!IsPostBack)
                        {
                            calQueryFrom.SelectedDate = DateTime.Now.Date;
                            calqueryTo.SelectedDate = DateTime.Now.Date;
                            btn_submit.Attributes.Add("OnClick", "return validate();");

                            //Populating Employee Dropdown
                            DataSet ds_attorney = ClsDb.Get_DS_BySP("usp_HTS_GetAllFirms");
                            ddl_attorney.DataSource = ds_attorney;
                            ddl_attorney.DataTextField = ds_attorney.Tables[0].Columns[1].ColumnName;
                            ddl_attorney.DataValueField = ds_attorney.Tables[0].Columns[0].ColumnName;
                            ddl_attorney.DataBind();
                            ddl_attorney.Items.Insert(0, "--All--");
                            ddl_attorney.Items[0].Value = "0";
                            ddl_attorney.SelectedValue = "0";
                        }
                        //If no records then disable controls
                        if (dg_valrep.Items.Count < 1)
                        {
                            lblCurrPage.Visible = false;
                            lblGoto.Visible = false;
                            lblPNo.Visible = false;
                            cmbPageNo.Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_submit.Click += new System.EventHandler(this.Button1_Click);
            this.cmbPageNo.SelectedIndexChanged += new System.EventHandler(this.cmbPageNo_SelectedIndexChanged_1);
            this.dg_valrep.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dg_valrep_PageIndexChanged);
            this.dg_valrep.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dg_valrep_ItemDataBound);
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion
        //When submit is clicked
        private void Button1_Click(object sender, System.EventArgs e)
        {
            //Nasir 7007 11/18/2009 add try catch
            try
            {
                dg_valrep.CurrentPageIndex = 0;
                FillGrid();
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message.ToString();
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        //Method to fill Grid
        private void FillGrid()
        {
                string[] key = { "@startdate", "@enddate", "@firmid" };
                object[] value1 = { calQueryFrom.SelectedDate, calqueryTo.SelectedDate, ddl_attorney.SelectedValue };
                ds_val = ClsDb.Get_DS_BySPArr("USP_HTS_GET_ExchangeDocket_Report", key, value1);
                lbl_message.Text = "";
                if (ds_val.Tables[0].Rows.Count < 1)
                {
                    lbl_message.Text = "No Record Found";
                }

                dg_valrep.DataSource = ds_val;
                dg_valrep.DataBind();
                BindReport();
                FillPageList();
                SetNavigation();
        }
        //Method To Generate Serial No
        private void BindReport()
        {   
                //int docid=0;
                //int docnum=0;
                long sNo = (dg_valrep.CurrentPageIndex) * (dg_valrep.PageSize);

                foreach (DataGridItem ItemX in dg_valrep.Items)
                {
                    sNo += 1;

                    ((Label)(ItemX.FindControl("lbl_Sno"))).Text = sNo.ToString();
                }
        }

        private void dg_valrep_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            //Nasir 7007 11/18/2009 increased try catch scope .
            try
            {
                DataRowView drv = (DataRowView)e.Item.DataItem;
                if (drv == null)
                    return;

                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    //In order to redirect to violation fees page
                    ((HyperLink)e.Item.FindControl("hnk_clientname")).NavigateUrl = "../ClientInfo/violationfeeold.aspx?search=0&casenumber=" + (int)drv["TicketID_PK"];

                    //Format Fee
                    //string fee=((Label)(e.Item.FindControl("lbl_fee"))).Text;
                    //fee="$"+fee;
                    //((Label)(e.Item.FindControl("lbl_fee"))).Text=fee;
                    //fee=fee.Substring(0,fee.IndexOf("."));	 
                    //((Label)(e.Item.FindControl("lbl_fee"))).Text=fee;			
                    //Mark record with red color not having SULL
                    string coverage = ((Label)(e.Item.FindControl("lbl_coverage"))).Text;
                    if (coverage != "SULL")
                    {
                        ((Label)(e.Item.FindControl("lbl_coverage"))).ForeColor = System.Drawing.Color.Red;
                        ((Label)(e.Item.FindControl("lbl_fee"))).ForeColor = System.Drawing.Color.Red;
                    }
                }
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message.ToString();
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }
        #region Navigation

        //----------------------------------Navigational LOGIC-----------------------------------------//

        // Procedure for filling page numbers in page number combo........
        private void FillPageList()
        {
           
                Int16 idx;

                cmbPageNo.Items.Clear();
                for (idx = 1; idx <= dg_valrep.PageCount; idx++)
                {
                    cmbPageNo.Items.Add("Page - " + idx.ToString());
                    cmbPageNo.Items[idx - 1].Value = idx.ToString();
                }
                        
        }

        // Procedure for setting data grid's current  page number on header ........
        private void SetNavigation()
        {
           
                // setting current page number
                lblPNo.Text = Convert.ToString(dg_valrep.CurrentPageIndex + 1);
                lblCurrPage.Visible = true;
                lblPNo.Visible = true;

                // filling combo with page numbers
                lblGoto.Visible = true;
                cmbPageNo.Visible = true;
                //FillPageList();

                cmbPageNo.SelectedIndex = dg_valrep.CurrentPageIndex;
           
        }

        // Procedure for setting the grid page number as per selected from page no. combo.......
        private void cmbPageNo_SelectedIndexChanged_1(object sender, System.EventArgs e)
        {
            //Nasir 7007 11/18/2009 try catch added.
            try
            {
                dg_valrep.CurrentPageIndex = cmbPageNo.SelectedIndex;
                FillGrid();
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        #endregion
        //Event fired when page changed
        private void dg_valrep_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
        {
            //Nasir 7007 11/18/2009 try catch added.
            try
            {
                dg_valrep.CurrentPageIndex = e.NewPageIndex;
                FillGrid();
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message.ToString();
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

    }
}

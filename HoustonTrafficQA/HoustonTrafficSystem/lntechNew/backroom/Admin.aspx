<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>

<%@ Page Language="c#" AutoEventWireup="false" Inherits="HTP.backroom.Admin" CodeBehind="Admin.aspx.cs" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>Admin</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../Styles.css" type="text/css" rel="stylesheet" />

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>

    <script src="../Scripts/jsDate.js" type="text/javascript"></script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body onload="javascript:TD1TD2();">

    <script type="text/javascript" language="javascript">
    function accesstype()
    {
       if (document.getElementById("ddl_accesstype").value== 1)
       {
            document.getElementById("td_closeout").style.visibility = 'visible';
		    document.getElementById("ddl_closeout").style.visibility = 'visible';            
	   }
	   else
	   {
		    document.getElementById("td_closeout").style.visibility = 'hidden';
		    document.getElementById("ddl_closeout").style.visibility = 'hidden';
		    document.getElementById("tb_NoofDays").style.visibility = 'hidden';
		    document.getElementById("td_nodays").style.visibility = 'hidden';
	   }
    }
    
    function showdaysbox()
    {
        if (document.getElementById("ddl_closeout").value== 1)
       {
            document.getElementById("tb_NoofDays").style.visibility = 'visible';
		    document.getElementById("td_nodays").style.visibility = 'visible';            
	   }
	   else
	   {
		    document.getElementById("tb_NoofDays").style.visibility = 'hidden';
		    document.getElementById("td_nodays").style.visibility = 'hidden';
	   }
    }
		function TD1TD2()
		{
		    if (document.getElementById("ddl_accesstype").value != 1)
		    {
		        document.getElementById("td_closeout").style.visibility = 'hidden';
		        document.getElementById("ddl_closeout").style.visibility = 'hidden';
		    }
		    
		    if (document.getElementById("ddl_closeout").value!= 1)
            {
                document.getElementById("tb_NoofDays").style.visibility = 'hidden';
		        document.getElementById("td_nodays").style.visibility = 'hidden';      
	        }
		   	   
		    
		    if ( document.Form1.ddl_spnsearch.value != 1 )
		    {		    
		        document.getElementById("TD1").style.visibility = 'hidden';
		        document.getElementById("TD2").style.visibility = 'hidden';
		    }
		    //if(document.getElementById("chkb_CanSPNSearch").checked==true)
		    else
		    {
		        document.getElementById("TD1").style.visibility = 'visible';
		        document.getElementById("TD2").style.visibility = 'visible';
		    }
		    //ozair 5196 11/29/2008
		     ShowHideFirmddl();
		}
		
		var whitespace = " \t\n\r";

		
		function isWhitespace (s)
           {   var i;

    
            if (isEmpty(s)) return true;
            for (i = 0; i < s.length; i++)
            {          
                var c = s.charAt(i);
                if (whitespace.indexOf(c) == -1) return false;
            }
     
            return true;
           }

		
		
function stripWhitespace (s)
{   return stripCharsInBag (s, whitespace)
}
		
		function isEmpty(s)
        {   return ((s == null) || (s.length == 0))
        }
		
		function isEmail (s)
{   if (isEmpty(s)) 
       if (isEmail.arguments.length == 1) return false;
       else return (isEmail.arguments[1] == true);
   
    
    if (isWhitespace(s)) return false;
       
    
    
    var i = 1;
    var sLength = s.length;

    
    while ((i < sLength) && (s.charAt(i) != "@"))
    { i++
    }

    if ((i >= sLength) || (s.charAt(i) != "@")) return false;
    else i += 2;

    
    while ((i < sLength) && (s.charAt(i) != "."))
    { i++
    }

    
    if ((i >= sLength - 1) || (s.charAt(i) != ".")) return false;
    else return true;
}
    // Noufil 4232 07/07/2008 Function to check the letter in the input
	function alphanumeric(alphane)
    {
	    var numaric = alphane;
	    for(var j=0; j<numaric.length; j++)
		    {
		          var alphaa = numaric.charAt(j);
		          var hh = alphaa.charCodeAt(0);
		          //if((hh > 47 && hh<58) || (hh > 64 && hh<91) || (hh > 96 && hh<123))
		          //ASCII code for aphabhets
		          if (!((hh > 64 && hh<91) || (hh > 96 && hh<123)))
		          {
		          return false;
		          }    
		    }
	}
	
		
		function trimAll(sString) 
		{
			while (sString.substring(0,1) == ' ')
			{
				sString = sString.substring(1, sString.length);
			}
			while (sString.substring(sString.length-1, sString.length) == ' ')
			{
				sString = sString.substring(0,sString.length-1);
			}
			return sString;
		}
		// Noufil 4378 08/06/2008 Function to accpet only alphabets and numbers removing special character
		function CheckName(name)
        {       
            for ( i = 0 ; i < name.length ; i++)
            {
                var asciicode =  name.charCodeAt(i)
                //If not valid alphabet 
                if (  ! ((asciicode >= 64 && asciicode <=90) || ( asciicode >= 97 && asciicode <=122)||(asciicode >= 48 && asciicode <=57)))
                return false;
           }
            return true;
        }
		
		function formSubmit() 
		{
		    	    
            var lname=document.getElementById("tb_lname").value;
            
            if (trimAll(lname) == "") {
	            alert("Please specify Last Name");
	            document.getElementById("tb_lname").focus();
	            return false;
            }
            
            if (!isNaN(lname))
            {
                alert("Name should only contains alphabets.");
                return false;
            }           
            
            if(alphanumeric(lname)==false)
            {
                alert("Name should only contains alphabets.");
                return false;
            }	
            var fname=document.getElementById("tb_fname").value;
            
            if (trimAll(fname) == "") 
            {
	            alert("Please specify First Name");
	            document.getElementById("tb_fname").focus();
	            return false;
            }
            
            if (!isNaN(fname))
            {
                alert("Name should only contains alphabets.");
                return false;
            }           
            
            if(alphanumeric(fname)==false)
            {
                alert("Name should only contains alphabets.");
                return false;
            }	

            if (trimAll(document.getElementById("tb_abbrev").value) == "") {
	            alert("Please Users Abbreviation");
	            document.getElementById("tb_abbrev").focus();
	            return  false;
            }

            if (trimAll(document.getElementById("tb_uname").value) == "") {
	            alert("Please specify UserName");
	            document.getElementById("tb_uname").focus();
	            return  false;
            }
            if (CheckName (document.getElementById("tb_uname").value)== false)
            {
                alert("User name must only contain alphabets and numbers");
	            document.getElementById("tb_uname").focus();
                return false;
            }
            
            if (trimAll(document.getElementById("tb_password").value) == "") {
	            alert("Please specify Password");
	            document.getElementById("tb_password").focus();
	            return  false;
            }
            // Noufil 4378 08/06/2008 Allow only alphabets and number in password n user name
            if (CheckName (document.getElementById("tb_password").value)== false)
            {
            
                alert("Password must only contain alphabets and numbers");
	            document.getElementById("tb_password").focus();
                return false;
            }

            if(trimAll(document.getElementById("txt_email").value) == "")
            { 
	            alert("Please specify EmailAddress");
	            document.getElementById("txt_email").focus();
	            return false;
            }
            else
            { 
	           if(isEmail(document.getElementById("txt_email").value)== false)
			   {
			       alert ("Please enter Email Address in Correct format.");
			       document.getElementById("txt_email").focus(); 
			       return false;			   
			   }
            }

            if (document.getElementById("ddl_accesstype").value == 0) {
	            alert("Please specify Access Type");
	            document.getElementById("ddl_accesstype").focus();
	            return  false;
            }

            if( document.getElementById("ddl_accesstype").value == "1")
            {
                if (document.getElementById("ddl_closeout").value == -1) {
	                alert("Please specify Close Out Access Type");
	                document.getElementById("ddl_closeout").focus();
	                return  false;
                }
                //ozair 4812 09/13/2008 validation check implemented
                if(document.getElementById("ddl_closeout").value=="1")
                {
                    if (document.getElementById("tb_NoofDays").value == "") 
                    {
	                    alert("Please enter Close Out Access days");
	                    document.getElementById("tb_NoofDays").focus();
	                    return  false;
                    }
                }                
            }
            if (document.getElementById("ddl_status").value == -1) {
	            alert("Please specify user status");
	            document.getElementById("ddl_status").focus();
	            return  false;
            }

            if (document.getElementById("ddl_spnsearch").value == -1) {
	            alert("Please specify SPN search status");
	            document.getElementById("ddl_spnsearch").focus();
	            return  false;
            }
            else if (document.getElementById("ddl_spnsearch").value == 1)
            {

                            if(document.getElementById("txt_SPNUserName").value == "")
		                    {
		                        alert("Please enter SPN UserName");
	                            document.getElementById("txt_SPNUserName").focus();
	                            return false;
		                    }
            		        
		                    if(document.getElementById("txt_SPNPassword").value == "")
		                    {
		                        alert("Please enter SPN Password");
	                            document.getElementById("txt_SPNPassword").focus();
	                            return false;
		                    }
            }
             var cb = document.getElementById("chkAttorney");
            if (cb.checked)
                {
                   
                    //Nasir 6968 01/22/2010 validate contact number
                     var txtContactID1 = document.getElementById('txt_CC11');
                    var txtContactID2 = document.getElementById('txt_CC12');
                    var txtContactID3 = document.getElementById('txt_CC13');
                    var txtContactID4 = document.getElementById('txt_CC14');
                    
                    var ContactNumber = txtContactID1.value + txtContactID2.value+ txtContactID3.value;
                     if(ContactNumber!="")
                        {
                           if(ContactNumber.length < 10 || isNaN(txtContactID1.value) || isNaN(txtContactID2.value) || isNaN(txtContactID3.value) ||isNaN(txtContactID4.value))
                           {
                            alert("Please Enter Correct Phone Number");
                            txtContactID1.focus();
                            return false;
                           }
                        }       
                }

                return true;
}
//Fahad 5196 11/28/2008 Function to Show or Hide the Firm ddl according to the 
function ShowHideFirmddl()
{
    var cb = document.getElementById("chkAttorney");
       
    if (cb.checked)
    {
 
        document.getElementById("ddl_Firm").style.display = "block";
        document.getElementById("lblFirm").style.display = "block";
        //Nasir 6968 11/23/2009
        document.getElementById("tdAttorCol").colSpan = "2";
        document.getElementById("tdContNum").style.display = "block";
        document.getElementById("tdContactNum").style.display = "block";
    }  
    else if (!cb.checked)
    {
       document.getElementById("ddl_Firm").style.display = "None";
       document.getElementById("lblFirm").style.display = "None";
       document.getElementById("ddl_Firm").Value="3000";
       //Nasir 6968 11/23/2009
       document.getElementById("tdAttorCol").colSpan = "5";
        document.getElementById("tdContNum").style.display = "None";
        document.getElementById("tdContactNum").style.display = "None";
    }
}


	
    </script>

    <div ms_positioning="GridLayout">
        <form id="Form1" method="post" runat="server">
        <table id="TableMain" cellspacing="0" cellpadding="0" width="780" align="center"
            border="0">
            <tbody>
                <tr>
                    <td colspan="4">
                        <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                    </td>
                </tr>
                <tr>
                    <td style="height: 14px" width="100%" background="../Images/separator_repeat.gif"
                        height="14">
                    </td>
                </tr>
                <tr>
                    <td id="pri" colspan="4">
                        <table id="Table1" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
                            <tr>
                                <td class="clsleftpaddingtable" valign="top" colspan="4" style="height: 30px">
                                    <table id="Table2" bordercolor="#ffffff" cellspacing="1" cellpadding="1" width="100%"
                                        border="0">
                                        <tr>
                                            <td class="clssubhead" style="width: 111px; height: 22px">
                                                Last Name
                                            </td>
                                            <td class="clssubhead" style="width: 116px; height: 22px" align="left">
                                                <asp:TextBox ID="tb_lname" runat="server" CssClass="clsinputadministration" Width="104px"
                                                    MaxLength="20"></asp:TextBox>
                                            </td>
                                            <td class="clssubhead" style="width: 96px; height: 22px">
                                                First Name
                                            </td>
                                            <td class="clssubhead" style="width: 152px; height: 22px" align="left">
                                                <asp:TextBox ID="tb_fname" runat="server" CssClass="clsinputadministration" Width="104px"
                                                    MaxLength="20"></asp:TextBox>
                                            </td>
                                            <td class="clssubhead" style="width: 105px; height: 22px">
                                                Short
                                            </td>
                                            <td class="clssubhead" style="height: 22px; width: 136px;">
                                                <asp:TextBox ID="tb_abbrev" runat="server" CssClass="clsinputadministration" Width="112px"
                                                    MaxLength="20"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="clssubhead" style="width: 111px; height: 22px">
                                                User Name
                                            </td>
                                            <td align="left" class="clssubhead" style="width: 116px; height: 22px">
                                                <asp:TextBox ID="tb_uname" runat="server" CssClass="clsinputadministration" Width="104px"
                                                    MaxLength="20"></asp:TextBox>
                                            </td>
                                            <td class="clssubhead" style="width: 96px; height: 22px">
                                                Password
                                            </td>
                                            <td align="left" class="clssubhead" style="width: 152px; height: 22px">
                                                <asp:TextBox ID="tb_password" runat="server" CssClass="clsinputadministration" Width="104px"
                                                    MaxLength="20"></asp:TextBox>
                                            </td>
                                            <td class="clssubhead" style="width: 105px; height: 22px">
                                                Email Address
                                            </td>
                                            <td class="clssubhead" style="height: 22px; width: 136px;">
                                                <asp:TextBox ID="txt_email" runat="server" CssClass="clsinputadministration" Width="112px"
                                                    MaxLength="100"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 111px; height: 22px" class="clssubhead">
                                                NT User ID
                                            </td>
                                            <td style="width: 116px; height: 22px">
                                                <asp:TextBox ID="txt_NTUserID" runat="server" CssClass="clsinputadministration" MaxLength="20"
                                                    Width="104px"></asp:TextBox>
                                            </td>
                                            <td style="width: 96px; height: 22px" class="clssubhead">
                                                Status
                                            </td>
                                            <td style="width: 152px; height: 22px">
                                                <asp:DropDownList ID="ddl_status" runat="server" CssClass="clsinputcombo" Width="104px">
                                                    <asp:ListItem Value="-1">--Choose--</asp:ListItem>
                                                    <asp:ListItem Value="1">Active</asp:ListItem>
                                                    <asp:ListItem Value="0">InActive</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 105px; height: 22px; direction: ltr;" class="clssubhead">
                                                Access Type
                                            </td>
                                            <td style="width: 136px; height: 22px">
                                                <asp:DropDownList ID="ddl_accesstype" runat="server" CssClass="clsinputcombo" Width="112px"
                                                    onchange="return accesstype();">
                                                    <asp:ListItem Value="0">--Choose--</asp:ListItem>
                                                    <asp:ListItem Value="1">Secondary</asp:ListItem>
                                                    <asp:ListItem Value="2">Primary</asp:ListItem>
                                                    <asp:ListItem Value="3">Read Only</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 111px; height: 22px" class="clssubhead">
                                                SPN Search
                                            </td>
                                            <td style="width: 116px; height: 22px">
                                                <asp:DropDownList ID="ddl_spnsearch" runat="server" CssClass="clsinputcombo" Width="104px">
                                                    <asp:ListItem Value="-1">--Choose--</asp:ListItem>
                                                    <asp:ListItem Value="1">Access</asp:ListItem>
                                                    <asp:ListItem Value="0">Not Access</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td id="td_closeout" style="width: 96px; height: 22px" class="clssubhead" runat="server">
                                                Close Out Page
                                            </td>
                                            <td style="width: 152px; height: 22px" runat="server">
                                                <asp:DropDownList ID="ddl_closeout" runat="server" CssClass="clsinputcombo" Width="104px"
                                                    onchange="return showdaysbox();">
                                                    <asp:ListItem Value="-1">--Choose--</asp:ListItem>
                                                    <asp:ListItem Value="1">Access</asp:ListItem>
                                                    <asp:ListItem Value="0">Not Access</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td id="td_nodays" style="width: 105px; height: 22px" class="clssubhead" runat="server">
                                                <span id="lblNoofDaysTitle" runat="server" style="display: block">No of Days</span>
                                            </td>
                                            <td style="width: 136px; height: 22px">
                                                <asp:TextBox ID="tb_NoofDays" runat="server" CssClass="clsinputadministration" MaxLength="20"
                                                    Width="112px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td id="TD2" style="width: 111px; height: 22px" class="clssubhead" runat="server">
                                                <asp:TextBox ID="txt_SPNUserName" runat="server" CssClass="clsinputadministration"
                                                    Width="104px"></asp:TextBox>
                                            </td>
                                            <td id="TD1" style="width: 116px; height: 22px" class="clssubhead" runat="server">
                                                <asp:TextBox ID="txt_SPNPassword" runat="server" CssClass="clsinputadministration"
                                                    Width="104px" Columns="80" MaxLength="80" Wrap="False"></asp:TextBox>
                                            </td>
                                            <td style="width: 186px; height: 22px">
                                                <asp:CheckBox ID="chk_StAdmin" runat="server" CssClass="clssubhead" Text=" Service Ticket Admin" />
                                            </td>
                                            <td style="width: 152px; height: 22px" runat="server">
                                                <asp:CheckBox ID="chkAttorney" runat="server" CssClass="clssubhead" Text=" Is Attorney "
                                                    Width="97px" onclick="ShowHideFirmddl();" />
                                            </td>
                                            <td style="width: 105px; height: 22px" class="clssubhead">
                                                <span id="lblFirm" runat="server" class="clssubhead" style="display: None;">Associated
                                                    Firm</span>
                                            </td>
                                            <td style="width: 136px; height: 22px">
                                                <asp:DropDownList ID="ddl_Firm" runat="server" CssClass="clsinputcombo" Width="112px"
                                                    Style="display: None;">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td id="tdAttorCol" colspan="5" align="right">
                                                <span runat="server" style="display: block"></span>
                                            </td>
                                            <td id="tdContNum" class="Label" style="width: 186px;display:none;">
                                                <span  runat="server" class="clssubhead">Contact Number</span>
                                            </td>
                                            <td id="tdContactNum" colspan="2" style="height: 22px;width:318px;display: None;" >
                                                <asp:TextBox ID="txt_CC11" onkeyup="return autoTab(this, 3, event)" runat="server"
                                                    Width="32px" CssClass="clsInputadministration" TabIndex="14"></asp:TextBox>
                                                -
                                                <asp:TextBox ID="txt_CC12" onkeyup="return autoTab(this, 3, event)" runat="server"
                                                    Width="32px" CssClass="clsInputadministration" TabIndex="15"></asp:TextBox>
                                                -
                                                <asp:TextBox ID="txt_CC13" onkeyup="return autoTab(this, 4, event)" runat="server"
                                                    Width="32px" CssClass="clsInputadministration" TabIndex="16"></asp:TextBox>
                                                x
                                                <asp:TextBox ID="txt_CC14" onkeyup="return autoTab(this, 4, event)" runat="server"
                                                    Width="40px" CssClass="clsInputadministration" TabIndex="17"></asp:TextBox>
                                            </td>
                                            
                                            <td style="width: 136px; height: 22px">
                                                <asp:Button ID="btn_update" runat="server" CssClass="clsbutton" Text="Update" OnClick="btn_update_Click1">
                                                </asp:Button>
                                                <asp:Button ID="btn_clear" runat="server" CssClass="clsbutton" Width="48px" Text="Clear">
                                                </asp:Button>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="clsLeftPaddingTable" style="height: 16px" background="../Images/separator_repeat.gif">
                                </td>
                            </tr>
                            <tr>
                                <td class="clssubhead" valign="middle" align="left" background="../Images/subhead_bg.gif"
                                    colspan="4">
                                    <table width="100%">
                                        <tr>
                                            <td class="clssubhead" style="width: 449px;" valign="middle">
                                                Results
                                            </td>
                                            <td>
                                                <asp:RadioButtonList ID="rbl_status" runat="server" CssClass="clssubhead" RepeatDirection="Horizontal"
                                                    AutoPostBack="True" OnSelectedIndexChanged="rbl_status_SelectedIndexChanged">
                                                    <asp:ListItem Value="1" Selected="True">&lt;span class=&quot;clssubhead&quot; &gt;Active Users &lt;/span&gt;</asp:ListItem>
                                                    <asp:ListItem Value="0">&lt;span class=&quot;clssubhead&quot; &gt;Inactive Users&lt;/span&gt;</asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                            <td style="width: 100px;" valign="middle" align="right">
                                                <asp:DropDownList ID="ddl_sort" runat="server" CssClass="clsinputcombo" Width="88px"
                                                    AutoPostBack="True" OnSelectedIndexChanged="ddl_sort_SelectedIndexChanged">
                                                    <asp:ListItem Value="lastname">Sort By</asp:ListItem>
                                                    <asp:ListItem Value="LastName">Last Name</asp:ListItem>
                                                    <asp:ListItem Value="AccessType">AccessType</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" colspan="4">
                                    <asp:DataGrid ID="dg_results" runat="server" CssClass="clsleftpaddingtable" Width="100%"
                                        BorderColor="White" AutoGenerateColumns="False">
                                        <Columns>
                                            <asp:TemplateColumn HeaderText="Last Name">
                                                <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="hlnk_lname" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.lastname") %>'
                                                        Visible="False">
                                                    </asp:HyperLink>
                                                    <asp:LinkButton ID="lnkbtn_lname" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.lastname") %>'
                                                        CommandName="click">
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="First Name">
                                                <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_fname" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.firstname") %>'>
                                                    </asp:Label>
                                                    <asp:Label ID="lbl_eid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.employeeid") %>'
                                                        Visible="False">
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Short">
                                                <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_abbrev" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.abbreviation") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="User Name">
                                                <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_uname" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.username") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Access Type">
                                                <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_accesstype" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.access") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Close Out Page">
                                                <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_Canview" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.CanUpdateCloseOutLog") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="SPN Search">
                                                <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CanSPNSearch" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.CanSPNSearch") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="NTUserID">
                                                <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_NTUserID" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.NTUserID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Status">
                                                <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_status" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Status") %>'
                                                        CssClass="label"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Attorney">
                                                <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_Attorney" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.IsAttorney") %>'
                                                        CssClass="label"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Access ST">
                                                <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_stAdmin" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AccessST") %>'
                                                        CssClass="label"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </td>
                            </tr>
                            <tr>
                                <td width="100%" background="../Images/separator_repeat.gif" height="11">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 760px" align="center">
                                    <asp:Label ID="lblMessage" runat="server" Width="538px" ForeColor="Red" Font-Size="X-Small"
                                        Font-Names="Arial"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 760px" align="left" colspan="4">
                                    <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                                    <asp:Label ID="lblRecCount" runat="server" ForeColor="White"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
        </form>
    </div>
</body>
</html>

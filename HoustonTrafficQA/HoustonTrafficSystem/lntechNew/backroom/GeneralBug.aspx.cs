using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components;
using System.IO;
using lntechNew.Components.ClientInfo;
namespace lntechNew.backroom
{
	/// <summary>
	/// Summary description for GeneralBug.
	/// </summary>
	public partial class GeneralBug : System.Web.UI.Page
	{

		clsENationWebComponents ClsDb = new clsENationWebComponents();

		Bug AddBug=new Bug();
		Attachment AddAttachment=new Attachment();
		Comments AddComments=new Comments();
		DataView DV;
        clsSession cSession = new clsSession();
        clsLogger BugTracker = new clsLogger();
		 int attachid=0;

        string MailServer = (string)System.Configuration.ConfigurationSettings.AppSettings["SMTPServer"];
        string EmailFrom = (string)System.Configuration.ConfigurationSettings.AppSettings["MailFrom"];
        string CCEmail = (string)System.Configuration.ConfigurationSettings.AppSettings["CCEmail"];
		
		protected void Page_Load(object sender, System.EventArgs e)
		{
            if (cSession.IsValidSession(this.Request) == false)
            {
                Response.Redirect("../frmlogin.aspx", false);
            }
            else
            {
                if (!IsPostBack)
                {
                    
                    ViewState["vNTPATHTroubleTicketUploads"] = System.Configuration.ConfigurationSettings.AppSettings["NTPATHTroubleTicketUploads"].ToString();
                    BindControls();
                    
                }
            }
			btnSubmit.Attributes.Add("onclick","return Validation();");
		}

		private void BindControls()
		{ 
			#region FillPriorityDropDown

			DataSet DS_Priority = ClsDb.Get_DS_BySP("usp_bug_getpriorities");
			if (DS_Priority.Tables[0].Rows.Count > 0)
			{
				ddl_priority.Items.Clear();
				for (int i = 0; i < DS_Priority.Tables[0].Rows.Count; i++)
				{
					string PriorityName = DS_Priority.Tables[0].Rows[i]["priority_name"].ToString().Trim();
					string Priorityid = DS_Priority.Tables[0].Rows[i]["priority_id"].ToString().Trim();
					ddl_priority.Items.Add(new ListItem(PriorityName, Priorityid));
				}
			}
			
			#endregion
			#region FillStatusDropDown

			DataSet DS_Status=ClsDb.Get_DS_BySP("usp_bug_getstatus"); 
			if(DS_Status.Tables[0].Rows.Count > 0)
			{ 
				ddl_status.Items.Clear();
				for(int i=0;i<DS_Status.Tables[0].Rows.Count;i++)
				{ 
					
					string StatusName=DS_Status.Tables[0].Rows[i]["status_name"].ToString().Trim();
					string StatusID=DS_Status.Tables[0].Rows[i]["status_id"].ToString().Trim();
					ddl_status.Items.Add(new ListItem(StatusName,StatusID));
				}
			}

			#endregion
		}
		

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
		private void AddGeneralBug()
		{ 
			int bugid=0;
			string filename="";
			

			try
			{
				
				filename=fp_file.PostedFile.FileName.ToString();

				
				AddBug.BugDescription=txt_shortdesc.Text.ToString().Trim();
                AddBug.EmployeeID = Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request));    
				AddBug.PageUrl=txt_pageurl.Text.ToString().Trim();
				AddBug.PostedDate=System.DateTime.Today;
				AddBug.PriorityID=Convert.ToInt32(ddl_priority.SelectedValue);
				AddBug.StatusID=Convert.ToInt32(ddl_status.SelectedValue);
				AddBug.TicketID=txt_ticketno.Text.ToString().Trim();
                AddBug.CauseNo = txtCauseNo.Text.ToString().Trim();
				bugid=AddBug.AddNewBug();
				if(txt_Desc.Text != "")
				{
					AddComments.BugID=bugid;
					AddComments.CommentDate=System.DateTime.Today;
					AddComments.Description=txt_Desc.Text.ToString().Trim();
                    AddComments.EmployeeID = Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request));    
					AddComments.AddNewComments();
				}
				
				if(filename != "")
				{ 
					AddAttachment.BugID=bugid;
					AddAttachment.ContentType=fp_file.PostedFile.ContentType;
					AddAttachment.Description=txt_filedesc.Text.ToString().Trim();
					AddAttachment.FileName=fp_file.PostedFile.FileName.Substring(fp_file.PostedFile.FileName.LastIndexOf("\\") + 1);
					AddAttachment.Size=fp_file.PostedFile.ContentLength;
					AddAttachment.UploadDate=System.DateTime.Today;
                    AddAttachment.EmployeeID = Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request));    
					attachid=AddAttachment.AddNewAttachment();

					SaveFile(bugid,attachid);
					
				}
				Response.Redirect("GeneralView.aspx",false);
				
			
			}
			catch(Exception ex)
			{
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
				lblMessage.Text=ex.Message.ToString();
				lblMessage.Visible=true;
			}
		}
		private void SaveFile(int Bugid,int attachmentid)
		{ 
			string FileName="";
			int filesize=0;
			string fName="";

			try
			{
				fName=fp_file.PostedFile.FileName.ToString();
				if(fName != "")
				{ 
					filesize=fp_file.PostedFile.ContentLength;
					if(filesize < 0)
					{ 
						lblMessage.Text="Uploading of File Failed";
						lblMessage.Visible=true;
					}
					else
					{ 
						FileName=fp_file.PostedFile.FileName.Substring(fp_file.PostedFile.FileName.LastIndexOf("\\") + 1);
                        fp_file.PostedFile.SaveAs(ViewState["vNTPATHTroubleTicketUploads"].ToString() + Bugid + "_" + attachmentid + "_" + FileName);
							
						
					}
				}

			}
			catch(Exception ex)
			{
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
				lblMessage.Text=ex.Message;
				lblMessage.Visible=true;
			} 
		}

		protected void btnSubmit_Click(object sender, System.EventArgs e)
		{
			AddGeneralBug();
		}

		
	}
}

<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>

<%@ Page Language="c#" AutoEventWireup="false" Inherits="HTP.backroom.frmExeSpByRptId"
    CodeBehind="frmExeSpByRptId.aspx.cs" %>

<%@ Register TagPrefix="uc2" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>frmExeSpByRptId</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="../Styles.css" type="text/css" rel="stylesheet">

    <script src="SqlSrvMgr.js" type="text/javascript"></script>

    <script lang="javascript">

				function ValidateInput()
				{
					//var cnt = document.Form1.TextBox2.value;
					
					var cnt = parseInt(document.getElementById("txtHidden").value);
					var grdName = "Dg_Output";
					var idx=2;
					
				       
				      
				 
					for (idx=2; idx <= (cnt+2); idx++)
					{
						var ctlValue = "";
						var ctltype = "";
						
						if(idx < 10)
						{
						 ctlValue = grdName+ "_ctl0" + idx + "_txtParamValue";
						 ctltype = grdName+ "_ctl0" + idx + "_lblParamType";
						
						}
						else
						{
						var ctlValue = grdName+ "_ctl" + idx + "_txtParamValue";
						var ctltype = grdName+ "_ctl" + idx + "_lblParamType";
						}
						//fol 2 lines will make TextBox and Label reference of txtParamValue and lblParamType resp.
						var txtParamValue = document.getElementById(ctlValue);	
						var lblParamType = document.getElementById(ctltype);	
						
						
						//txtParamValue==null means there is ddlParamValue not txtParamValue in the idx row of grid
						//go for next iteration
						if (txtParamValue == null)continue;
						
						
						if (txtParamValue.value == "")
						{
							alert("Please type values.");
							txtParamValue.focus();
							return false;
						}
					//	a;
						switch(lblParamType.innerText)
						{
							case "datetime":
								var strDate  = txtParamValue.value;
								if(!isDate(strDate))
								{
									txtParamValue.focus(); 
									return false;
								}
								break;
					
							case "int":
							case "numeric":
							case "tinyint":
							case "smallint":
								var strInt = txtParamValue.value;
								if ( !isNonnegativeInteger(strInt))
								{
									alert("Please Enter a valid number");
									txtParamValue.focus();
									return false;
								}
								break;
								
							case "varchar":
							case "nvarchar":
							case "text":
								//return true;
								break;
								
							case "bit":
								var strBool = txtParamValue.value;
								if (strBool=='1' || strBool=='0'|| strBool.toLowerCase()=='false' || strBool.toLowerCase()=='true')
								{
									break;
								}
								else
								{
									alert("Please Enter a valid boolean value");
									txtParamValue.focus();
									return false;
								}
								break;
				//			a;
							case "money":
								var strValue = txtParamValue.value;
								if( strValue.charAt(0)=='$' )
								{
									strValue = strValue.substring(1,strValue.length);
									txtParamValue.value = strValue;
								}	

								if( !isFloat(strValue) )
								{
									alert("Please Enter a valid value");
									txtParamValue.focus();
									return false;
								}	

								break;

							case "decimal":
							case "float":
								var strValue = txtParamValue.value;
								if( !isFloat(strValue) )
								{
									alert("Please Enter a valid value");
									txtParamValue.focus();
									return false;
								}	
								
								break;

							default:
								//return false;
						}

						
						
						
					}
				}


// isDate(strDate) defination and its supporting function
var dtCh= "/";
var minYear=1900;
var maxYear=2100;

function isInteger(s){
	var i;
    for (i = 0; i < s.length; i++){   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag){
	var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++){   
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function daysInFebruary (year){
	// February has 29 days in any year evenly divisible by four,
    // EXCEPT for centurial years which are not also divisible by 400.
    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
}
function DaysArray(n) {
	for (var i = 1; i <= n; i++) {
		this[i] = 31
		if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
		if (i==2) {this[i] = 29}
   } 
   return this
}

function isDate(dtStr){
	var daysInMonth = DaysArray(12)
	var pos1=dtStr.indexOf(dtCh)
	var pos2=dtStr.indexOf(dtCh,pos1+1)
	var strMonth=dtStr.substring(0,pos1)
	var strDay=dtStr.substring(pos1+1,pos2)
	var strYear=dtStr.substring(pos2+1)
	strYr=strYear
	if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
	if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
	for (var i = 1; i <= 3; i++) {
		if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
	}
	month=parseInt(strMonth)
	day=parseInt(strDay)
	year=parseInt(strYr)
	if (pos1==-1 || pos2==-1){
		alert("Please Enter the date in 'mm/dd/yyyy' format ")
		return false
	}
	if (strMonth.length<1 || month<1 || month>12){
		alert("Please enter a valid month")
		return false
	}
	if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
		alert("Please enter a valid day")
		return false
	}
	if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
		alert("Please enter a valid 4 digit year between "+minYear+" and "+maxYear)
		return false
	}
	if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
		alert("Please enter a valid date")
		return false
	}
return true
}

function ValidateForm(){
	var dt=document.frmSample.txtDate
	if (isDate(dt.value)==false){
		dt.focus()
		return false
	}
    return true
 }
// end of isDate() function and its Supporting function

    </script>

</head>
<body bottommargin="0" leftmargin="0" topmargin="0" rightmargin="0">
    <form id="Form1" method="post" runat="server">
    |
    <div>
        <strong></strong>&nbsp;</div>
    <table id="tblMain" bordercolor="#e4e2e2" cellspacing="0" cellpadding="0" width="764"
        align="center" border="0">
        <tr>
            <td style="height: 14px" valign="top">
                <!-- For Header -->
                <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
            </td>
        </tr>
        <tr>
            <td style="height: 21px" valign="middle" align="left">
                <p class="clsmainhead">
                    &nbsp;</p>
            </td>
        </tr>
        <tr>
            <td style="height: 11px" valign="middle" align="left" background="../images/separator_repeat.gif">
            </td>
        </tr>
        <tr>
            <td class="clsleftpaddingtable" align="right">
                <asp:ImageButton ID="ImgBtnExport" runat="server" ToolTip="Export to Excel" ImageUrl="../../Images/excel_icon.gif">
                </asp:ImageButton>
            </td>
        </tr>
        <tr>
            <td>
                <table id="Table2" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
                    <tbody>
                        <tr>
                            <td align="center" width="100%">
                                <table id="tblhide" cellspacing="0" cellpadding="0" width="100%" border="0">
                                    <tr>
                                        <td class="clssubhead" style="height: 31px" valign="middle" align="left" width="547"
                                            background="../../Images/subhead_bg.gif">
                                            &nbsp;<asp:Label ID="lblRptNm" runat="server" CssClass="clssubhead" BackColor="#EEC75E"
                                                Font-Names="Verdana">Label</asp:Label>
                                        </td>
                                        <td class="clssubhead" style="height: 32px" valign="middle" align="right" width="780"
                                            background="../../Images/subhead_bg.gif">
                                            <asp:HyperLink ID="hlback" runat="server" CssClass="clssubhead" BackColor="Transparent"
                                                Width="30px" NavigateUrl="frmReports.aspx">Back</asp:HyperLink>&nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <tr>
                                <td style="height: 35px">
                                    <asp:DataGrid ID="Dg_Output" runat="server" CssClass="clsleftpaddingtable" Font-Names="Verdana"
                                        Width="264px" Font-Size="2px" ShowHeader="False" CellPadding="0" BorderWidth="0px"
                                        BorderStyle="None" AutoGenerateColumns="False" GridLines="None">
                                        <SelectedItemStyle Font-Bold="True" ForeColor="#CCFF99" BackColor="#009999"></SelectedItemStyle>
                                        <ItemStyle ForeColor="#003399" BackColor="White"></ItemStyle>
                                        <HeaderStyle Font-Bold="True" ForeColor="#CCCCFF" BackColor="White"></HeaderStyle>
                                        <FooterStyle ForeColor="#003399" BackColor="#99CCCC"></FooterStyle>
                                        <Columns>
                                            <asp:TemplateColumn HeaderText="Param Alias">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblParamAlias" runat="server" Width="180px" CssClass="cmdlinks" Text='<%# DataBinder.Eval(Container, "DataItem.ParamAlias") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Parameter Value">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtParamValue" runat="server" Width="182px" CssClass="clsinputadministration"
                                                        Height="18px" Visible="False"></asp:TextBox>
                                                    <asp:DropDownList ID="ddlParamValue" runat="server" CssClass="clsinputcombo" Visible="False">
                                                    </asp:DropDownList>
                                                    <ew:CalendarPopup ID="dtPicker" runat="server" ToolTip="Select starting list date"
                                                        ImageUrl="../Images/calendar.gif" Font-Names="Tahoma" Font-Size="8pt" Width="60px"
                                                        DisableTextboxEntry="False" EnableHideDropDown="True" ControlDisplay="TextBoxImage"
                                                        CalendarLocation="Bottom" ShowGoToToday="True" AllowArbitraryText="False" Culture="(Default)"
                                                        UpperBoundDate="12/31/9999 23:59:00" PadSingleDigits="True">
                                                        <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                                        <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                            BackColor="White"></WeekdayStyle>
                                                        <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                            ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                                        <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                                            BackColor="AntiqueWhite"></OffMonthStyle>
                                                        <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                            ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                                        <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                            BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                                        <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                            ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                                        <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                            BackColor="LightGray"></WeekendStyle>
                                                        <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                            ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                                        <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                            ForeColor="Black" BackColor="White"></ClearDateStyle>
                                                        <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                            BackColor="White"></HolidayStyle>
                                                    </ew:CalendarPopup>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn Visible="False" HeaderText="Parameter Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblParamName" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.ParamName") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Parameter Type">
                                                <HeaderStyle Width="0px"></HeaderStyle>
                                                <ItemStyle Width="0px"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblParamType" runat="server" ForeColor="White" Font-Size="X-Small"
                                                        Width="0" Text='<%# DataBinder.Eval(Container, "DataItem.ParamType") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                                <FooterStyle Width="0px"></FooterStyle>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn Visible="False" HeaderText="Parameter Length">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblParamLength" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ParamLength") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn Visible="False" HeaderText="SpName">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSpName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SpName") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn Visible="False" HeaderText="Default Value">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDefaultVal" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DefaultVal") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn Visible="False" HeaderText="Column Visibility">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblVisibilty" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ColVisibility") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn Visible="False" HeaderText="Object Type">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblObjType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ObjectType") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn Visible="False" HeaderText="Command Type">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCmdType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CommandType") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn Visible="False" HeaderText="Command Text">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCmdText" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CommandText") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                        <PagerStyle HorizontalAlign="Left" ForeColor="#003399" BackColor="#99CCCC" Mode="NumericPages">
                                        </PagerStyle>
                                    </asp:DataGrid>
                                </td>
                                <tr>
                                    <td align="right">
                                        <asp:Button ID="imgbtnGo" runat="server" CssClass="clsbutton" Text="Run Report">
                                        </asp:Button>
                                    </td>
            </td>
        </tr>
        <tr>
            <td style="height: 18px" align="center">
                <asp:Label ID="lblmsg" runat="server" CssClass="cmdlinks" EnableViewState="False"
                    ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:DataGrid ID="DgResults" runat="server" CssClass="clsleftpaddingtable" BorderWidth="1px"
                    BorderStyle="Solid">
                    <AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
                    <ItemStyle Font-Bold="True"></ItemStyle>
                    <HeaderStyle Width="5%" BackColor="#A8CAF9"></HeaderStyle>
                    <Columns>
                        <asp:TemplateColumn HeaderText="S.NO">
                            <ItemTemplate>
                                <asp:Label ID="lbl_sno" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle CssClass="GrdAdminHeader"></PagerStyle>
                </asp:DataGrid>
            </td>
        </tr>
        <tr>
            <td style="height: 25px" align="left">
                <asp:Label ID="lbltotalrow" runat="server" CssClass="label" Visible="False">Total Rows :</asp:Label><asp:Label
                    ID="lblrows" runat="server" CssClass="label" Visible="False"></asp:Label><input id="txtHidden"
                        type="hidden" runat="server">
            </td>
        </tr>
        <tr>
            <td>
                <uc2:Footer ID="Footer1" runat="server"></uc2:Footer>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;

namespace lntechNew.backroom
{
    public partial class VacationDays : System.Web.UI.Page
    {

        clsENationWebComponents ClsDB = new clsENationWebComponents();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                RangeValidator1.MinimumValue = DateTime.Now.ToShortDateString();
                //Fahad 10409 08/15/2012 Maximized the date range from 1 year to 12 years
                RangeValidator1.MaximumValue = DateTime.Now.AddYears(12).ToShortDateString();
                BindGrid();

            }

        }

        private void BindGrid()
        {
            try
            {
                DataSet ds = ClsDB.Get_DS_BySP("USP_HTS_Get_VacationDays");
                gv_list.DataSource = ds.Tables[0];
                gv_list.DataBind();
             }
            catch (Exception ex){

                lbl_error.Text = ex.Message.ToString();
            }
        }

        protected void btn_refresh_Click(object sender, EventArgs e)
        {

            try
            {
                string[] keys = { "@VacationDate" };
                object[] values = { cal_EffectiveFrom.SelectedDate.ToShortDateString()};
                bool i = (bool)ClsDB.ExecuteScalerBySp("USP_HTS_Insert_VacationDate", keys, values);


                if (i == true)
                {


                    BindGrid();
                    cal_EffectiveFrom.Text = "";
                }
                else if (i == false)
                {
                    Response.Write("<script> alert('The specified date is already signed as vacation.');</script>");
                }


            }
            catch (Exception ex)
            {
                lbl_error.Text = ex.Message.ToString();
            }


        }

        protected void gv_list_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "delete")
            {

                try
                {
                    string[] keys = { "@Id" };
                    object[] values = { e.CommandArgument.ToString() };
                    ClsDB.ExecuteSP("USP_HTS_Delete_VacationDate", keys, values);
                    BindGrid();

                }
                catch (Exception ex){
                    lbl_error.Text = ex.Message.ToString();
                }

            
            
            }
        }

        protected void gv_list_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }

        protected void gv_list_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}

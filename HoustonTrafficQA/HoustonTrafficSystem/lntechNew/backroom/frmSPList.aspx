<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.backroom.SqlSrvMrg.frmSPList" Codebehind="frmSPList.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Stored Procedure List</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript">

		// Enabling and Disabling Dg_Output(datagrid) Controls(Combobox and textbox) 
		function ShowHide(d1,t1,d2)
		{
			
			var ddl1 = document.getElementById(d1 );
			var ddl2 = document.getElementById(d2 );
			var txt = document.getElementById(t1 );

			if (ddl1.selectedIndex == 0)
				{
				ddl2.disabled = true;
				txt.disabled = true;
				}
			else
				{
				ddl2.disabled = false;
				txt.disabled = false;
				}

		}

			// Validating the Controls
    	 function DoValidate()
		{
			var cnt = (document.Form1.TextBox2.value) * 1 ;
			cnt= cnt + 2;
			
			var grdName = "Dg_Output";
			var idx=2;
		
			var reportname = document.getElementById("txtreportname");
			if (reportname.value == "")
			{
					alert("Please type Report name where you want to save parameters field.");
					return false;
			} 
			for (idx=2; idx < cnt; idx++)
			{
				var ctlName ="";
				var ctext = "";
				var calias = "";
				
				if(idx < 10)
				{
				 ctlName = grdName+ "_ctl0" + idx + "_DDLObjType";
				 ctext = grdName+ "_ctl0" + idx + "_txtCmd";
				 calias = grdName+ "_ctl0" + idx + "_txtParamAlias";
				}
				else
				{
				 ctlName = grdName+ "_ctl" + idx + "_DDLObjType";
				 ctext = grdName+ "_ctl" + idx + "_txtCmd";
				 calias = grdName+ "_ctl" + idx + "_txtParamAlias";
				}
				
				var elemnt = document.getElementById(ctlName);	
				var etext = document.getElementById(ctext);
				var ealias = document.getElementById(calias);
				if ((elemnt.selectedIndex == 1  && etext.value == "") )
					{
					alert("Please type SQL query or specify procedure name.");
					etext.focus();
					return false;
					}
				if (ealias.value =="")
				{
					alert("Please type Parameter Alias.");
					return false;
				}
				
				
			}
		}
		 
		</script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" >
		<form id="Form1" method="post" runat="server">
			<table id="tblMain" cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
				<tr>
					<td>
						<!-- For Header --><uc1:activemenu id="ActiveMenu1" runat="server"></uc1:activemenu></td>
				</tr>
				<tr>
					<td>
						<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
							<TBODY>
								<TR>
									<TD style="HEIGHT: 9px" align="left" background="../../images/separator_repeat.gif"></TD>
								</TR>
								<tr> <!-- tahir   -->
									<td class="clsleftpaddingtable" style="HEIGHT: 25px" vAlign="bottom" align="center"
										width="100%"><STRONG><FONT face="Verdana" color="#0066cc">
												<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="100%" border="0">
													<TBODY>
														<TR>
															<TD class="clsleftpaddingtable" style="WIDTH: 95px" width="95"><SPAN class="style2">Stored&nbsp;Procedure</SPAN></TD>
															<TD class="clsleftpaddingtable" style="WIDTH: 126px" width="126"><asp:textbox id="txtSearchSp" Runat="server" CssClass="FrmTDLetter"></asp:textbox></TD>
															<TD class="clsleftpaddingtable" style="WIDTH: 160px" width="160">&nbsp;<asp:button id="ImageButton1" runat="server" CssClass="clsbutton" Text="Search"></asp:button></TD>
															<TD class="clsleftpaddingtable" style="WIDTH: 159px" align="right">&nbsp;</TD>
															<TD class="clsleftpaddingtable" style="WIDTH: 99px" align="right"><asp:label id="lblCurrPage" runat="server" Font-Size="Smaller">Current Page :</asp:label>&nbsp;<asp:label id="lblPNo" runat="server" Font-Size="Smaller" Width="16px" Height="10px">a</asp:label></TD>
															<TD class="clsleftpaddingtable" style="WIDTH: 28px" align="right">&nbsp;<asp:label id="lblGoto" runat="server" Font-Size="Smaller" Width="16px">Goto</asp:label></TD>
															<TD align="right">&nbsp;<asp:dropdownlist id="cmbPageNo" runat="server" CssClass="clsinputcombo" Font-Size="Smaller" AutoPostBack="True"></asp:dropdownlist></TD>
														</TR>
													</TBODY>
												</TABLE>
											</FONT></STRONG>
										<asp:label id="lblNoRec" runat="server" CssClass="cmdlinks" Width="184px" ForeColor="Red" Visible="False"></asp:label></td>
								</tr>
								<TR>
									<TD style="HEIGHT: 14px" vAlign="bottom" align="center" width="100%" background="../../images/separator_repeat.gif"></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 34px" vAlign="middle" align="left" width="100%" background="../../Images/subhead_bg.gif"><STRONG><STRONG class="clssubhead">&nbsp;Lists 
												of Stored Procedures</STRONG></STRONG></TD>
								</TR>
								<TR>
									<td vAlign="top" width="100%"><asp:datagrid id="DgSqlProcedure" runat="server" CssClass="clsleftpaddingtable" Width="100%" AllowPaging="True"
											AutoGenerateColumns="False" CellPadding="0" PageSize="20">
											<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle"></HeaderStyle>
											<FooterStyle CssClass="GrdFooter"></FooterStyle>
											<Columns>
												<asp:TemplateColumn HeaderText="Procedure Name">
													<HeaderStyle Font-Bold="True" HorizontalAlign="Left" CssClass="clsaspcolumnheader"></HeaderStyle>
													<ItemTemplate>
														<asp:HyperLink id=HLSpnm runat="server" CssClass="cmdLinks2" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.ProcedureName") %>'>
														</asp:HyperLink>
														<asp:LinkButton id=lnkSPName runat="server" CssClass="cmdLinks2" Text='<%# DataBinder.Eval(Container, "DataItem.ProcedureName") %>' CommandName="ShowParam">
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Create Date">
													<HeaderStyle Font-Bold="True" HorizontalAlign="Left" CssClass="clsaspcolumnheader"></HeaderStyle>
													<ItemTemplate>
														<asp:Label id=Label1 runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.CrtDate") %>'>
														</asp:Label>
													</ItemTemplate>
													<EditItemTemplate>
														<asp:TextBox id=TextBox1 runat="server" CssClass="clsinputadministration" Text='<%# DataBinder.Eval(Container, "DataItem.crdate") %>' MaxLength="11">
														</asp:TextBox>
													</EditItemTemplate>
												</asp:TemplateColumn>
											</Columns>
											<PagerStyle NextPageText="Next &gt;&gt;" PrevPageText="&lt;&lt; Previous" HorizontalAlign="Center"></PagerStyle>
										</asp:datagrid></td>
								</TR>
								<TR>
									<TD align="center" width="800"><asp:label id="lblmsg" runat="server" CssClass="cmdlinks" ForeColor="Red" Visible="False"></asp:label></TD>
								</TR>
								<TR>
									<TD style="VISIBILITY: hidden; HEIGHT: 20px" align="right"> <!-- DESIGNTIMEDRAGDROP="1460" --><asp:textbox id="txtSpName" runat="server" Visible="False"></asp:textbox><asp:textbox id="txtchk" runat="server">0</asp:textbox><asp:textbox id="txtNoParam" runat="server">0</asp:textbox><asp:textbox id="TextBox2" runat="server"></asp:textbox></TD>
								</TR>
							</TBODY>
						</TABLE>
					</td>
				</tr>
				<tr>
					<td><uc1:footer id="Footer1" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>

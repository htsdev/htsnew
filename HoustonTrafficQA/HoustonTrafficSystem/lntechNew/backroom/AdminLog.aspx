﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdminLog.aspx.cs" Inherits="HTP.backroom.AdminLog" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="~/WebControls/ActiveMenu.ascx" TagPrefix="uc1" TagName="activemenu" %>
<%@ Register Src="~/WebControls/PagingControl.ascx" TagName="pagingcontrol" TagPrefix="uc3" %>
<%@ Register Src="~/WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Namespace="eWorld.UI" TagPrefix="ew" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="../Styles.css" rel="Stylesheet" type="text/css" />
    <script type="text/javascript" language="javascript">
        function Validate()
        {
        
            if(document.getElementById("ddlReport")[document.getElementById("ddlReport").selectedIndex].index == 0)
            {
                alert("Please select report.");
                return false;
            }
        }
    </script>

    <title>Admin Log</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <aspnew:ScriptManager ID="ScriptManager1" runat="server" />
        <table cellpadding="0" cellspacing="0" width="900px" align="center" border="0">
            <tr>
                <td>
                    <uc1:activemenu ID="ActiveMenu1" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="center" style="width: 100%">
                    <asp:Label ID="lblMessage" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 100%" background="../Images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellpadding="0" width="100%">
                        <tr style="width: 100%">
                            <td style="width: 8%">
                                <span class="clssubhead">Start Date :</span>
                            </td>
                            <td style="width: 10%">
                                <ew:CalendarPopup Visible="true" ID="cal_FromDateFilter" runat="server" AllowArbitraryText="False"
                                    CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Culture="(Default)" EnableHideDropDown="True"
                                    Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif" Nullable="True"
                                    PadSingleDigits="True" ShowClearDate="True" ShowGoToToday="True" Text=" " ToolTip="Date"
                                    UpperBoundDate="9999-12-29" Width="65px">
                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TextboxLabelStyle CssClass="clstextarea" />
                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Gray" />
                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                </ew:CalendarPopup>
                            </td>
                            <td style="width: 8%">
                                <span class="clssubhead">End Date :</span>
                            </td>
                            <td style="width: 10%">
                                <ew:CalendarPopup Visible="true" ID="cal_ToDateFilter" runat="server" AllowArbitraryText="False"
                                    CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Culture="(Default)" EnableHideDropDown="True"
                                    Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif" Nullable="True"
                                    PadSingleDigits="True" ShowClearDate="True" ShowGoToToday="True" Text=" " ToolTip="Date"
                                    UpperBoundDate="9999-12-29" Width="65px">
                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TextboxLabelStyle CssClass="clstextarea" />
                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Gray" />
                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                </ew:CalendarPopup>
                            </td>
                            <td style="width: 8%">
                                <asp:CheckBox ID="chk_ShowAll" Text="Show All" CssClass="clsLabel" runat="server" />
                            </td>
                            <td style="width: 2%">
                            </td>
                            <td style="width: 8%">
                                <span class="clssubhead">Report :</span>
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="ddlReport" CssClass="clsInputCombo" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td align="left">
                                <asp:Button ID="btnSearch" Text="Search" OnClick="btnSearch_Click" CssClass="clsbutton"
                                    runat="server" OnClientClick="return Validate();" />
                            </td>
                            <td align="left">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td background="../Images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td background="../Images/subhead_bg.gif" height="34" class="clssubhead" align="right">
                    <table width="100%">
                        <tr>
                            <td align="left">
                            </td>
                            <td align="right">
                                <aspnew:UpdatePanel ID="updatepnlpaging" runat="server" RenderMode="Inline">
                                    <ContentTemplate>
                                        <uc3:pagingcontrol ID="Pagingctrl" runat="server" />
                                    </ContentTemplate>
                                </aspnew:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table id="TableGrid" bgcolor="white" border="0" cellpadding="0" cellspacing="0"
                        width="100%">
                        <tr>
                            <td background="../../images/separator_repeat.gif" colspan="5" height="11" style="height: 11px"
                                width="780">
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Label ID="lbl_Message" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2" valign="top">
                                <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlResult">
                                    <ProgressTemplate>
                                        <img src="../Images/plzwait.gif" /><asp:Label ID="lbl1" runat="server" Text="Please Wait ......"
                                            CssClass="clsLabel"></asp:Label>
                                    </ProgressTemplate>
                                </aspnew:UpdateProgress>
                                <aspnew:UpdatePanel ID="upnlResult" runat="server">
                                    <ContentTemplate>
                                        <asp:GridView ID="gv_Records" runat="server" AutoGenerateColumns="False" Width="100%"
                                            CssClass="clsLeftPaddingTable" AllowSorting="True" AllowPaging="True" PageSize="20"
                                            OnPageIndexChanging="gv_Records_PageIndexChanging" OnSorting="gv_Records_Sorting"
                                            CellPadding="0" CellSpacing="0">
                                            <Columns>
                                                <asp:TemplateField HeaderText="<u>Date and Time</u>" SortExpression="RecDate" HeaderStyle-CssClass="clssubhead"
                                                    HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_RecordDate" runat="server" CssClass="GridItemStyle" Text='<%# Eval("RecDate","{0:MM/dd/yyyy}") + " @ " + Eval("RecDate","{0:hh:mm tt}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" Width="20%" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>Rep Name</u>" SortExpression="RepName" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_LName" runat="server" CssClass="GridItemStyle" Text='<%# Eval("RepName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" Width="10%" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>Description</u>" SortExpression="[Note]" HeaderStyle-CssClass="clssubhead"
                                                    HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_Notes" runat="server" CssClass="GridItemStyle" Text='<%# Eval("[Note]") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" Width="70%" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerStyle HorizontalAlign="Center" />
                                            <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                        </asp:GridView>
                                    </ContentTemplate>
                                </aspnew:UpdatePanel>
                                <asp:HiddenField ID="hf_NoteIDs" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td background="../../images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td>
                    <uc1:Footer ID="Footer1" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:HiddenField ID="hf_ticketviolationids" runat="server" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

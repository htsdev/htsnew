﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace HTP.backroom
{
    public partial class ShowImage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            clsENationWebComponents ClsDB = new clsENationWebComponents();
            clsLogger clog = new clsLogger();

            try
            {
                if (Request.QueryString["firmid"] != null)
                {
                    int firmid = Convert.ToInt32(Request.QueryString["firmid"]);

                    DataTable dt = null;
                    string[] key = { "@firmid" };
                    object[] values = { firmid };

                    dt = ClsDB.Get_DT_BySPArr("usp_hts_get_firm_info", key, values);

                    if (dt != null && dt.Rows[0]["AttorneySignatureImage"].ToString() != "" && dt.Rows[0]["AttorneySignatureImage"] != null)
                    {

                        Byte[] bytes = (Byte[])dt.Rows[0]["AttorneySignatureImage"];
                        Response.Buffer = true;
                        Response.Charset = "";
                        Response.Cache.SetCacheability(HttpCacheability.NoCache);
                        Response.AddHeader("content-disposition", "attachment;filename=" + dt.Rows[0]["FirmName"].ToString());
                        Response.BinaryWrite(bytes);
                        Response.Flush();
                    }

                }
            }
            catch (Exception ex)
            {
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

    }
}

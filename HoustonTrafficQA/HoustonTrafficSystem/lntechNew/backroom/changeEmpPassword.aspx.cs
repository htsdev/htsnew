﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using lntechNew.Components.ClientInfo;
using HTP.WebComponents;

namespace lntechDallasNew.backroom
{
    public partial class changeEmpPassword : MatterBasePage
    {

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btn_Submit_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.EmpId != 0)
                {
                    // Noufil 4378 07/23/2008 Update Employee Password
                    clsUser cUser = new clsUser();
                    cUser.EmpID = this.EmpId;
                    cUser.Password = txt_password.Text;

                    if (cUser.ChangeEmployeePassword(txt_newpassword.Text))
                    {
                        //Zeeshan Ahmed 4703 08/28/2008 Update Password In Ticket Desk System
                        clsSession cSession = new clsSession();
                        cUser.UpdateTicketDeskPassword(Convert.ToString(cSession.GetCookie("sUserID", this.Request)), txt_newpassword.Text, "");
                        //Set New Password In Cookie
                        cSession.CreateCookie("Password", txt_newpassword.Text, this.Request, this.Response);
                        Response.Write("<script type='text/javascript' language='javascript'>alert('Your Password has been changed Successfully');opener.location.reload();self.close();</script>");
                    }
                    else
                    {
                        Response.Write("<script type='text/javascript' language='javascript'>alert('Password mismatch please retry with correct password or contact system administrator');</script>");
                    }
                }
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
            }
        }




    }
}

﻿// The following code was generated by Microsoft Visual Studio 2005.
// The test owner should check each test for validity.
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Text;
using System.Collections.Generic;
using lntechNew.Components;
using System.Data;
namespace TestProject_lntechNew
{
    /// <summary>
    ///This is a test class for lntechNew.Components.FreeConsultation and is intended
    ///to contain all lntechNew.Components.FreeConsultation Unit Tests
    ///</summary>
    [TestClass()]
    public class FreeConsultationTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for GetData ()
        ///</summary>
        [TestMethod()]
        public void GetDataTest()
        {
            object[] value ={ "01/01/2007", "01/01/2007" };

            FreeConsultation target = new FreeConsultation(value);

            DataTable actual;

            actual = target.GetData();

           
        }
        [TestMethod()]
        public void GetDataTest1()
        {
            object[] value ={ "" ,""};

            FreeConsultation target = new FreeConsultation(value);

            DataTable actual;

            actual = target.GetData();


        }

    }


}

using System;
using System.Text;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using lntechNew.Components;
using HTP.Components;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;
using System.IO;


namespace HTP.DocumentTracking
{
    public partial class SearchBatch : System.Web.UI.Page
    {
     // Fahad Muhammad Qureshi 5167-5168 02/21/2008  grouping variables
        #region Variables

        clsLogger BugTracker = new clsLogger();
        clsENationWebComponents clsDb = new clsENationWebComponents();
        clsSession cSession = new clsSession();
        clsDocumentTracking clsDT = new clsDocumentTracking();
        string PicID = String.Empty;
        string path = "";

        #endregion

     // Fahad Muhammad Qureshi 5167-5168 02/21/2008  grouping Propertise
        # region Propertise

        public SortDirection GridViewSortDirection
        {
            get
            {
                if ( ViewState["sortDirection"] == null )
                    ViewState["sortDirection"] = SortDirection.Ascending;
                return ( SortDirection ) ViewState["sortDirection"];
            }
            set { ViewState["sortDirection"] = value; }

        }

        public string GridViewSortExpression
        {
            get
            {
                if ( ViewState["sortExpression"] == null )
                    ViewState["sortExpression"] = "BatchID";
                return ViewState["sortExpression"].ToString();
            }
            set { ViewState["sortExpression"] = value; }

        }

        #endregion

     // Fahad Muhammad Qureshi 5167-5168 02/21/2008  grouping Event Handlers
        #region Event Handler


        protected void Page_Load(object sender, EventArgs e)
        {
            //Fahad 5167/5168 02/19/2009 Try and Catch block added
            try
            {
                if (cSession.IsValidSession(this.Request) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else
                {
                    if (!IsPostBack)
                    {
                        lbl_Message.Visible = false;
                        if (Request.QueryString["sMenu"] != "" & Request.QueryString["sMenu"] != null)
                        {
                            ViewState["vSMenuID"] = Request.QueryString["sMenu"].ToString();
                            hlk_Scan.NavigateUrl = "~/DocumentTracking/ScanBatch.aspx?sMenu=" + ViewState["vSMenuID"];
                        }
                        ViewState["vNTPATHScanImage"] = ConfigurationManager.AppSettings["NTPATHDocumentTrackingImage"].ToString();
                        //Pagingctrl.GridView = gv_SearchBatch;
                        BindGrid();
                    }
                    
                    Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
                    Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
                    Pagingctrl.GridView = gv_SearchBatch;
                    
                    //Fahad 5167/5168 02/19/2009 Paging Control added
                    
                }
               
            }
          
             catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
 
           
        }

        protected void img_delete_Command(object sender, CommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "delete")
                {
                    int SBID = Convert.ToInt32(e.CommandArgument.ToString());
                    string searchPat = String.Empty;
                    lbl_Message.Text = "";
                    clsDT.ScanBatchID = SBID;
                    DataSet ds_DeletePics = clsDT.GetBatchForDelete();

                    if (ds_DeletePics.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds_DeletePics.Tables[0].Rows.Count; i++)
                        {
                            PicID = ds_DeletePics.Tables[0].Rows[i]["scanbatchdetailid"].ToString();
                            searchPat = SBID + "-" + PicID + "*.jpg";
                            string[] filename = Directory.GetFiles(ViewState["vNTPATHScanImage"].ToString(), searchPat);//get scann images
                            if (filename.Length >= 1)
                            {
                                for (int j = 0; j < filename.Length; j++)
                                {
                                    path = filename[j].ToString();
                                    if (File.Exists(path))
                                    {
                                        File.Delete(path);
                                    }
                                }
                            }
                        }
                        clsDT.ScanBatchID = SBID;
                        clsDT.DeleteBatch();
                        lbl_Message.Text = "Batch Deleted Successfuly";
                        lbl_Message.Visible = true;
                    }
                    BindGrid();
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            //Fahad 5168/5167 02/20/2009 Try-Catch block is added
            try
            {
                SearchResult();
                Pagingctrl.Visible = true;
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void gv_SearchBatch_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //Fahad 5168/5167 02/20/2009 Try-Catch block is added
            try   
            {
                string SBID = "";

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HyperLink hlk_BatchID = (HyperLink)e.Row.FindControl("hlk_BatchID");
                    HyperLink hlk_NoLetterID = (HyperLink)e.Row.FindControl("hlk_NoLetterID");
                    ImageButton img_delete = (ImageButton)e.Row.FindControl("img_delete");
                    Label lbl_Verified = (Label)e.Row.FindControl("lbl_Verified");

                    SBID = hlk_BatchID.Text;

                    if (hlk_NoLetterID.Text == "0")
                    {
                        hlk_BatchID.NavigateUrl = "~/DocumentTracking/BatchDetail.aspx?sMenu=" + ViewState["vSMenuID"] + "&SBID=" + SBID;
                    }
                    else
                    {
                        hlk_NoLetterID.NavigateUrl = "~/DocumentTracking/UpdateBatch.aspx?sMenu=" + ViewState["vSMenuID"] + "&SBID=" + SBID;
                    }

                    if (lbl_Verified.Text == "0")
                    {
                        img_delete.Enabled = true;
                        //img_delete.Attributes.Add("onclick", "return DeleteConfirm();");
                    }
                    else
                    {
                        //ozair 4035 06/12/2008 delete image will not be available in case of verifed documents
                        img_delete.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void gv_SearchBatch_PageIndexChanging(object sender, GridViewPageEventArgs e) 
        {
            //Fahad 5168/5167 02/20/2009 Try-Catch block is added
                try
                {
                    if (e.NewPageIndex != -1)
                    {
                        gv_SearchBatch.PageIndex = e.NewPageIndex;//paging
                        BindGrid();

                    }

                }
                catch (Exception ex)
                {
                    lbl_Message.Visible = true;
                    lbl_Message.Text = ex.Message;
                    clsLogger.ErrorLog(ex);
                }
               
        }

        protected void gv_SearchBatch_Sorting(object sender, GridViewSortEventArgs e)
        {
            lbl_Message.Visible = false;
            try
            {
                string sortExpression = e.SortExpression;

                if ( GridViewSortDirection == SortDirection.Ascending && sortExpression == GridViewSortExpression )
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                else
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }

                GridViewSortExpression = sortExpression;
                BindGrid();
                Pagingctrl.PageCount = gv_SearchBatch.PageCount;
                Pagingctrl.PageIndex = gv_SearchBatch.PageIndex;
            }
            catch ( Exception ex )
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog( ex );
            }
        }
           
     
        #endregion

     // Fahad Muhammad Qureshi 5167-5168 02/21/2008  Custom Methods
        #region Methods


        private void SearchBatch_PageMethod()
        {
            Session["PageEvent"] = "true";
            Response.Redirect(Request.Url.AbsolutePath, false);
        }

        void Pagingctrl_PageIndexChanged()
        {
            gv_SearchBatch.PageIndex = Pagingctrl.PageIndex - 1;
            BindGrid();
        }

       
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            //Fahad 5168/5167 02/20/2009 Try-Catch block is added
            try
            {

                if (pageSize > 0)
                {
                    gv_SearchBatch.PageIndex = 0;
                    gv_SearchBatch.PageSize = pageSize;
                    gv_SearchBatch.AllowPaging = true;
                }
                else
                {
                    gv_SearchBatch.AllowPaging = false;
                }
                BindGrid();
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        private void BindGrid()
        {
                DataSet DS_GetBatch = clsDT.GetSearchBatch();
                if (DS_GetBatch.Tables[0].Rows.Count > 0)
                {
                    DataView dv = DS_GetBatch.Tables[0].DefaultView;
                    dv.Sort = GridViewSortExpression + " " + (GridViewSortDirection == SortDirection.Ascending ? "ASC" : "DESC");
                    //DataTable dt = dv.ToTable();
                    Pagingctrl.GridView = gv_SearchBatch;
                    gv_SearchBatch.DataSource = dv;
                    gv_SearchBatch.DataBind();
                    Pagingctrl.PageCount = gv_SearchBatch.PageCount;
                    Pagingctrl.PageIndex = gv_SearchBatch.PageIndex;
                    Pagingctrl.SetPageIndex();
                }
                else
                {
                    Pagingctrl.PageCount = 0;
                    Pagingctrl.PageIndex = 0;
                    Pagingctrl.SetPageIndex();
                    lbl_Message.Text = "No Records Found";
                    gv_SearchBatch.Visible = false;
                }
                
       }

        private void SearchResult()
        {
            lbl_Message.Visible = false;
            gv_SearchBatch.Visible = true;

            //if (chk.Checked == true)//Fahad Commented check and added new one
            if(chk.Checked)
            {
                clsDT.CheckStatus = 1;
            }
            else
            {
                clsDT.CheckStatus = 0;
            }
           
            if (cal_StartDate.SelectedDate.ToShortDateString() != "1/1/0001" && cal_EndDate.SelectedDate.ToShortDateString() != "1/1/0001")
            {
                clsDT.StartDate = cal_StartDate.SelectedDate;
                clsDT.EndDate = cal_EndDate.SelectedDate;
                BindGrid();
            }

        }


        # endregion

        



    }
}

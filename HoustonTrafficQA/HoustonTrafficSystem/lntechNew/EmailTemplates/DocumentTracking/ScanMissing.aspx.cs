using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;
using HTP.Components;
using lntechNew.Components;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;


namespace HTP.DocumentTracking
{
    public partial class ScanMissing : System.Web.UI.Page
    {
        #region Variables

        Batch NewBatch = new Batch();
        clsGeneralMethods CGeneral = new clsGeneralMethods();
        Picture NewPic = new Picture();
        Log NewLog = new Log();
        OCR NewOCR = new OCR();
        clsLogger BugTracker = new clsLogger();
        clsENationWebComponents clsDb = new clsENationWebComponents();
        clsSession cSession = new clsSession();
        clsDocumentTracking clsDT = new clsDocumentTracking();

        string ocr_data = String.Empty;
        string picDestination = String.Empty;
        string CourtStatus = String.Empty;

        string strServer;
        string searchpath = "";
        string picName = "";
        int scanBatchDetailID = 0;
        int PicID = 0;
        int Batchid = 0;
        StringBuilder sb = new StringBuilder();
        string StrAcsDec = String.Empty;
        string StrExp = String.Empty;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (cSession.IsValidSession(this.Request) == false)
            {
                Response.Redirect("../frmlogin.aspx", false);
            }
            else
            {
                if (!IsPostBack)
                {
                    lbl_Message.Visible = false;

                    txtSrv.Text = Request.ServerVariables["SERVER_NAME"].ToString();//for scanning function in javascript.
                    strServer = "http://" + Request.ServerVariables["SERVER_NAME"];//for scanning function in javascript.
                    Session["objTwain"] = "<OBJECT id='OZTwain1' classid='" + strServer + "/OZTwain_1.dll#OZTwain.OZTwain' height='1' width='1' VIEWASTEXT> </OBJECT>";//for scanning function call in javascript.

                    txtsessionid.Text = Session.SessionID.ToString();//for scanning function in javascript.
                    txtempid.Text = cSession.GetCookie("sEmpID", this.Request).ToString();//for scanning function in javascript.
                    ViewState["vNTPATHScanTemp"] = ConfigurationManager.AppSettings["NTPATHDocumentTrackingTemp"].ToString();//path to and get save images
                    ViewState["vNTPATHScanImage"] = ConfigurationManager.AppSettings["NTPATHDocumentTrackingImage"].ToString();//path to save and get images
                    btn_Scan.Attributes.Add("onclick", "return StartScan();");
                    if (Request.QueryString["DBID"] != "" & Request.QueryString["DBID"] != null)
                    {
                        ViewState["DBID"] = Request.QueryString["DBID"].ToString();
                        ViewState["SBID"] = Request.QueryString["SBID"].ToString();
                        BindData();
                    }
                }
            }
        }

        protected void btn_Scan_Click(object sender, EventArgs e)
        {
            lbl_Message.Visible = false;

            try
            {
                searchpath = "*" + Session.SessionID + txtempid.Text + "*.jpg";
                string[] filename = Directory.GetFiles(ViewState["vNTPATHScanTemp"].ToString(), searchpath);//get scann images
                if (filename.Length > 1)
                {
                    filename = clsDT.SortFilesByDate(filename);//sort files by date..
                }
                if (filename.Length >= 1)
                {

                    for (int i = 0; i < filename.Length; i++)
                    {
                        clsDT.ScanBatchID = Convert.ToInt32(lbl_BatchID.Text.Trim());
                        clsDT.DocumentBatchID = Convert.ToInt32(lbl_LetterID.Text.Trim());
                        clsDT.DocumentBatchPageCount = Convert.ToInt32(lbl_PageCount.Text.Trim());
                        clsDT.DocumentBatchPageNo = Convert.ToInt32(txt_PageNo.Text.Trim());
                        clsDT.EmpID = Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request));

                        scanBatchDetailID = clsDT.GetSBDIDByInsertingScanBatchDetail();

                        picName = clsDT.ScanBatchID.ToString() + "-" + scanBatchDetailID.ToString() + "-" + clsDT.DocumentBatchID.ToString();
                        picDestination = ViewState["vNTPATHScanImage"].ToString() + picName + ".jpg";
                        File.Copy(filename[i].ToString(), picDestination);//save images with batchid and picid like 1-1.jpg.
                        File.Delete(filename[i].ToString());//delete from intial position.
                    }
                }
                //HttpContext.Current.Response.Write("<script language='javascript'> opener.location.href = opener.location; self.close();   </script>");
                HttpContext.Current.Response.Write("<script language='javascript'> opener.location.reload(); self.close();   </script>");			
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;
                lbl_Message.Visible = true;
                tbl_plzwait1.Style["display"] = "none";
            }
        }

        #endregion

        #region Methods

        private void BindData()
        {
            try
            {
                clsDT.DocumentBatchID = Convert.ToInt32(ViewState["DBID"]);
                clsDT.ScanBatchID = Convert.ToInt32(ViewState["SBID"]);
                DataSet ds_ScanMissing = clsDT.GetScanMissing();

                if (ds_ScanMissing.Tables[0].Rows.Count > 0)
                {
                    lbl_BatchID.Text = ds_ScanMissing.Tables[0].Rows[0]["BatchID"].ToString();
                    lbl_LetterID.Text = ds_ScanMissing.Tables[0].Rows[0]["LetterID"].ToString();
                    lbl_PageCount.Text = ds_ScanMissing.Tables[0].Rows[0]["PageCount"].ToString();
                }
                else
                {
                    lbl_Message.Text = "No record found! Please close this window and try again.";
                    lbl_Message.Visible = true;
                }
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;
                lbl_Message.Visible = true;
            }
        }

        #endregion

    }
}

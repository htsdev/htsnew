using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using lntechNew.Components;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using System.IO;
using System.Drawing;
using BaxterSoft.Graphics;

namespace lntechNew.WebScan
{
    public partial class TestOCRForm : System.Web.UI.Page
    {
        string ocr_data = String.Empty;
        string picDestination = String.Empty;
        string CourtStatus = String.Empty;
        string causeno = "";
        string courtdate = "";
        string time = "";
        string TempString = "";

        string courtno = "";
        clsGeneralMethods CGeneral = new clsGeneralMethods();

        private static bool imageCallBack()
        {
            return true;
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            OCR();
        }

        private void OCR()
        {



            try
            {





                try
                {

                    picDestination = @"D:\DOCSTORAGE\WebScanDocs\temp\"+txt_name.Text.Trim()+".jpg";//get Image from folder for OCR..


                    try
                    {
                        ocr_data = CGeneral.OcrIt(picDestination);//OCR Process..
                        Response.Write(ocr_data);
                        
                        //Bitmap bmpOZ = new Bitmap(picDestination);
                        //Bitmap bmpnew = new Bitmap(1264, 1650);

                        //bmpnew = (Bitmap)bmpOZ.GetThumbnailImage(1264, 1650, imageCallBack, IntPtr.Zero);
                        //bmpOZ.Dispose();
                        //System.IO.File.Delete(picDestination);
                        //bmpnew.Save(picDestination, System.Drawing.Imaging.ImageFormat.Jpeg);
                        //bmpnew.Dispose();

                    }
                    catch (Exception ex)
                    {
                        string str = ex.Message;
                        Response.Write(ex.Message);

                    }

                    if (ocr_data.Length > 0)
                    {

                        TempString = ocr_data.ToUpper();
                        TempString = TempString.Replace("\r\n", " ");
                        #region Cause No
                        try
                        {

                            if (TempString.Contains("CAUSE NUMBER") == true)
                            {

                                causeno = TempString.Substring(TempString.LastIndexOf("CAUSE NUMBER") + 13, 17);
                                causeno = causeno.Replace(" ", "");
                                causeno = causeno.Replace(".", "");
                                causeno.Trim();

                                if (courtno.IndexOf("O") > -1)
                                {
                                    courtno = courtno.Replace("O", "0");
                                }
                                else if (courtno.IndexOf("o") > -1)
                                {
                                    courtno = courtno.Replace("o", "0");
                                }
                                if (courtno.IndexOf("I") > -1)
                                {
                                    courtno = courtno.Replace("I", "1");
                                }
                                else if (courtno.IndexOf("i") > -1)
                                {
                                    courtno = courtno.Replace("i", "1");
                                }

                                if (courtno.IndexOf("L") > -1)
                                {
                                    courtno = courtno.Replace("L", "1");
                                }
                                else if (courtno.IndexOf("l") > -1)
                                {
                                    courtno = courtno.Replace("l", "1");
                                }

                                if ((causeno.Substring(0, 4) != "2007") & (causeno.Substring(0, 4).LastIndexOf("7") == 3))
                                {
                                    causeno = causeno.Remove(0, 4);
                                    causeno = causeno.Insert(0, "2007");
                                }
                                if ((causeno.Substring(0, 4) != "2006") & (causeno.Substring(0, 4).LastIndexOf("6") == 3))
                                {
                                    causeno = causeno.Remove(0, 4);
                                    causeno = causeno.Insert(0, "2006");
                                }
                                if ((causeno.Substring(0, 4) != "2005") & (causeno.Substring(0, 4).LastIndexOf("5") == 3))
                                {
                                    causeno = causeno.Remove(0, 4);
                                    causeno = causeno.Insert(0, "2005");
                                }
                                if ((causeno.Substring(0, 4) != "2004") & (causeno.Substring(0, 4).LastIndexOf("4") == 3))
                                {
                                    causeno = causeno.Remove(0, 4);
                                    causeno = causeno.Insert(0, "2004");
                                }
                                if ((causeno.Substring(0, 4) != "2003") & (causeno.Substring(0, 4).LastIndexOf("3") == 3))
                                {
                                    causeno = causeno.Remove(0, 4);
                                    causeno = causeno.Insert(0, "2003");
                                }
                                if ((causeno.Substring(0, 4) != "2002") & (causeno.Substring(0, 4).LastIndexOf("2") == 3))
                                {
                                    causeno = causeno.Remove(0, 4);
                                    causeno = causeno.Insert(0, "2002");
                                }
                                if ((causeno.Substring(0, 4) != "2001") & (causeno.Substring(0, 4).LastIndexOf("1") == 3))
                                {
                                    causeno = causeno.Remove(0, 4);
                                    causeno = causeno.Insert(0, "2001");
                                }
                                if ((causeno.Substring(0, 4) != "2000") & (causeno.Substring(0, 4).LastIndexOf("0") == 3))
                                {
                                    causeno = causeno.Remove(0, 4);
                                    causeno = causeno.Insert(0, "2000");
                                }
                                if ((causeno.Substring(0, 4) != "1999") & (causeno.Substring(0, 4).LastIndexOf("9") == 3))
                                {
                                    causeno = causeno.Remove(0, 4);
                                    causeno = causeno.Insert(0, "1999");
                                }
                                if ((causeno.Substring(0, 4) != "1998") & (causeno.Substring(0, 4).IndexOf("8") == 3))
                                {
                                    causeno = causeno.Remove(0, 4);
                                    causeno = causeno.Insert(0, "1998");
                                }

                                if (causeno.Substring(4, 1) == "T" & causeno.Substring(4, 2) != "TR")
                                {
                                    causeno = causeno.Remove(4, 2);
                                    causeno = causeno.Insert(4, "TR");
                                }

                                else if (causeno.Substring(4, 1) == "N" & causeno.Substring(4, 2) != "NT")
                                {
                                    causeno = causeno.Remove(4, 2);
                                    causeno = causeno.Insert(4, "NT");
                                }

                                else if (causeno.Substring(4, 1) == "F" & causeno.Substring(4, 3) != "FTA")
                                {
                                    causeno = causeno.Remove(4, 3);
                                    causeno = causeno.Insert(4, "FTA");
                                }
                                else if (causeno.Substring(4, 3).IndexOf("A") > -1)
                                {
                                    causeno = causeno.Remove(4, 3);
                                    causeno = causeno.Insert(4, "FTA");
                                }
                            }

                        }
                        catch
                        {
                            causeno = "";
                        }
                        #endregion
                        #region Court No
                        try
                        {
                            if (TempString.Contains("COURT NO") == true)
                            {

                                courtno = TempString.Substring(TempString.LastIndexOf("COURT NO") + 10, 2);
                                if (courtno.IndexOf("O") > -1)
                                {
                                    courtno = courtno.Replace("O", "0");
                                }
                                else if (courtno.IndexOf("o") > -1)
                                {
                                    courtno = courtno.Replace("o", "0");
                                }

                                if (courtno.IndexOf("I") > -1)
                                {
                                    courtno = courtno.Replace("I", "1");
                                }
                                else if (courtno.IndexOf("i") > -1)
                                {
                                    courtno = courtno.Replace("i", "1");
                                }

                                if (courtno.IndexOf("L") > -1)
                                {
                                    courtno = courtno.Replace("L", "1");
                                }
                                else if (courtno.IndexOf("l") > -1)
                                {
                                    courtno = courtno.Replace("l", "1");
                                }


                                try
                                {
                                    courtno = Convert.ToInt32(courtno.Trim()).ToString();
                                }
                                catch
                                {

                                }


                            }
                        }
                        catch
                        {
                            courtno = "";
                        }
                        #endregion
                        #region Court Date
                        try
                        {
                            if (TempString.Contains("COURT DATE") == true)
                            {
                                courtdate = TempString.Substring(TempString.IndexOf("COURT DATE") + 11, 12);
                                courtdate = courtdate.Trim();
                                courtdate = courtdate.Replace(" ", "");


                                if (courtdate.IndexOf("I") > -1)
                                {
                                    courtdate = TempString.Substring(TempString.IndexOf("COURT DATE") + 11, 13);
                                    courtdate = courtdate.Replace(" ", "");
                                    courtdate = courtdate.Replace("I", "1");
                                }
                                else if (courtdate.IndexOf("i") > -1)
                                {
                                    courtdate = TempString.Substring(TempString.IndexOf("COURT DATE") + 11, 13);
                                    courtdate = courtdate.Replace(" ", "");
                                    courtdate = courtdate.Replace("i", "1");
                                }

                                if (courtdate.IndexOf("O") > -1)
                                {
                                    courtdate = courtdate.Replace("O", "0");
                                }
                                else if (courtdate.IndexOf("o") > -1)
                                {
                                    courtdate = courtdate.Replace("o", "0");
                                }


                                if (courtdate.Substring(2, 1) != "/")
                                {
                                    courtdate = courtdate.Remove(2, 1);
                                    courtdate = courtdate.Insert(2, "/");
                                }
                                if (courtdate.Substring(5, 1) != "/")
                                {
                                    courtdate = courtdate.Remove(5, 1);
                                    courtdate = courtdate.Insert(5, "/");
                                }
                                courtdate = courtdate.Trim();
                                if (courtdate.Length > 10)
                                {
                                    courtdate = courtdate.Substring(0, 10);
                                }
                            }
                        }
                        catch
                        {
                            courtdate = "";
                        }
                        #endregion
                        #region Time
                        try
                        {
                            if (TempString.Contains("TIME") == true)
                            {
                                time = TempString.Substring(TempString.IndexOf("TIME") + 5, 9);
                                if(time.Contains("AND")==true)
                                {
                                    time = TempString.Substring(TempString.LastIndexOf("TIME") + 5, 9);
                                }
                                if (time.IndexOf("8") > -1)
                                {
                                    time = "8:00 AM";
                                }
                                else if (time.IndexOf("9") > -1)
                                {
                                    time = "9:00 AM";
                                }
                                else if (time.IndexOf("1") > -1)
                                {
                                    time = "10:30 AM";
                                }
                                else if (time.IndexOf("3") > -1)
                                {
                                    time = "10:30 AM";
                                }
                                else
                                {
                                    time = "";
                                }

                            }
                        }
                        catch
                        {
                            time = "";
                        }
                        #endregion


                        Response.Write("       Cause No: "+ causeno +" Court Date: "+courtdate +" Time: "+time+" No: "+courtno);



                    }
                    else
                    {


                    }
                }
                catch (Exception ex)
                {
                    string str = ex.Message;

                }


            }





            catch (Exception ex)
            {

            }

        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet ds;

                System.Data.DataTable dtlTemp1;

                //string[] key = { "@DocID", "@DocNum" };
                //object[] value1 = { DocID, DocNum };

                byte[] imageData;

                clsENationWebComponents clsDb = new clsENationWebComponents();
                ds = clsDb.Get_DS_BySP("usp_get_faxdatafromfaxDB");

                dtlTemp1 = ds.Tables[0];


                if (dtlTemp1.Rows.Count > 0)
                {
                    imageData = (byte[])dtlTemp1.Rows[0]["attdata"];

                    ////testing by ozair
                    //MemoryStream m = new MemoryStream(imageData);
                    //System.Drawing.Bitmap bp = new Bitmap(m);
                    //bp.Save("c:\\aa", System.Drawing.Imaging.ImageFormat.Jpeg);
                    ////end testing

                    string strFileName = Server.MapPath("../Temp/");
                    string strfn = strFileName + Session.SessionID + Convert.ToString(DateTime.Now.ToFileTime())+".jpg";
                    FileStream fs = new FileStream(strfn, FileMode.CreateNew, FileAccess.Write);
                    fs.Write(imageData, 0, imageData.Length);
                    fs.Close();
                    //FileStream fs1 = new FileStream(strfn, FileMode.Open);
                    
                    //TIFFReader tr = new TIFFReader(fs1);
                    //TIFFDocument tdoc = tr.Read(false);
                    //tr.Close();
                    ////fs1.Flush();
                    //fs1.Close();
                    //File.Delete(strfn);


                    ////fs.Flush();
                    //PDFWriter pdf = new PDFWriter(tdoc);
                    //Response.Clear();
                    //Response.ContentType = "application/image";
                    //Response.AddHeader("Content-Type", "application/image");
                    //byte[] output = pdf.SavePDF().ToArray();
                    //byte[] output = fs1.ToArray();
                    //Response.BinaryWrite(output);

                    //Response.Write(strfn);


                    //Response.Flush();
                   // Response.Close();

                    //Response.End();

                }
                else
                {
                    Response.Write("No Images Found");
                    //Response.End();
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
                //Response.End();
            }
        }
    }
}

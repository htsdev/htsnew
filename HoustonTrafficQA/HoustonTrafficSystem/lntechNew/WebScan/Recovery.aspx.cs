using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;
using System.IO;

namespace lntechNew.WebScan
{
    public partial class Recovery : System.Web.UI.Page
    {
        clsENationWebComponents clsDb = new clsENationWebComponents();
        clsSession cSession = new clsSession();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private void IsAlreadyInBatchPrint()
        {
            string[] keys = { "@TicketID" };
            object[] values = { Convert.ToInt32(ViewState["vTicketID"].ToString()) };
            DataTable dtBatchPrint = new DataTable();
            dtBatchPrint = clsDb.Get_DT_BySPArr("USP_HTS_GET_BATCHLETTER_CHECK_BATCHPRINT", keys, values);
            if (Convert.ToInt32(dtBatchPrint.Rows[0][0].ToString()) > 0)
                lbl_IsAlreadyInBatchPrint.Text = "1";
            else
                lbl_IsAlreadyInBatchPrint.Text = "0";
        }

        private void RemoveExistingEntry()
        {
            //FileInfo fi = new FileInfo("");
            //fi.Delete(); 
            string[] keys = { "@TicketID" };
            object[] values = { Convert.ToInt32(ViewState["vTicketID"].ToString()) };
            clsDb.ExecuteSP("USP_HTS_BATCHLETTERS_DELETE_EXISTING", keys, values);

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            //try
            {
               // DataSet dsn = clsDb.Get_DS_BySP("usp_WEBSCAN_Recovery");
                DataSet dsn = clsDb.Get_DS_BySP("usp_WEBSCAN_Recovery_19MAR07");
                ViewState["vNTPATHScanImage"] = ConfigurationSettings.AppSettings["NTPATHScanImage2"].ToString();
                if (dsn.Tables.Count > 0)
                {
                    if (dsn.Tables[0].Rows.Count > 0)
                    {
                        for (int k = 0; k < dsn.Tables[0].Rows.Count; k++)
                        {
                            int empid;
                            int TicketID;
                            string[] key ={ "@CauseNo" };
                            object[] value1 ={ dsn.Tables[0].Rows[k]["causeno"].ToString().Replace(" ", "").ToUpper() };
                            DataSet ds = clsDb.Get_DS_BySPArr("usp_webscan_GetTicketIDByCauseNo", key, value1);
                            if (ds.Tables.Count > 0)
                            {
                                if (ds.Tables[0].Rows.Count > 0)
                                {
                                    for (int l = 0; l < ds.Tables[0].Rows.Count; l++)
                                    {
                                        clsCase ClsCase = new clsCase();
                                        TicketID = Convert.ToInt32(ds.Tables[0].Rows[l]["TicketID"].ToString());
                                        ViewState["vTicketID"] = TicketID;
                                        //Perform database Uploading Task

                                        string searchpat = dsn.Tables[0].Rows[k]["batchid"].ToString() + "-" + dsn.Tables[0].Rows[k]["picid"].ToString() + ".jpg";
                                        //string[] fileName = Directory.GetFiles(Server.MapPath("tempimages/"), searchpat);
                                        string[] fileName = Directory.GetFiles(ViewState["vNTPATHScanImage"].ToString(), searchpat);
                                        empid = Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request));

                                        string bType = "Resets";
                                        int BookId = 0, picID = 0;

                                        string picName, picDestination;

                                        int DocCount = 1;
                                        for (int ii = 0; ii < fileName.Length; ii++)
                                        {
                                            string[] key1 = { "@updated", "@extension", "@Description", "@DocType", "@Employee", "@TicketID", "@Count", "@Book", "@BookID" };
                                            object[] value11 = { DateTime.Now, "JPG", "BSDA", bType, empid, TicketID, DocCount, BookId, "" };
                                            //call sP and get the book ID back from sP
                                            picName = clsDb.InsertBySPArrRet("usp_Add_ScanImage", key1, value11).ToString();
                                            string BookI = picName.Split('-')[0];
                                            string picI = picName.Split('-')[1];
                                            BookId = (int)Convert.ChangeType(BookI, typeof(int)); ; //
                                            picID = (int)Convert.ChangeType(picI, typeof(int)); ; //

                                            //Move file
                                            picDestination = ConfigurationSettings.AppSettings["NTPATHScanImage"].ToString() + picName + ".jpg"; //DestinationImage

                                            System.IO.File.Copy(fileName[ii].ToString(), picDestination);

                                            // Sending the trial notification to Batch

                                            //
                                            //Outside court and status in Arr,Pre,Jury,Judge today or future courtdate
                                            DateTime dcourt = Convert.ToDateTime(ds.Tables[0].Rows[l]["CourtDateMain"]);
                                            if ((dcourt >= DateTime.Now.Date) && (Convert.ToInt32(ds.Tables[0].Rows[l]["categoryid"]) == 2 || Convert.ToInt32(ds.Tables[0].Rows[l]["categoryid"]) == 3 || Convert.ToInt32(ds.Tables[0].Rows[l]["categoryid"]) == 4 || Convert.ToInt32(ds.Tables[0].Rows[l]["categoryid"]) == 5) && (Convert.ToInt32(ds.Tables[0].Rows[l]["CourtID"]) != 3001 && Convert.ToInt32(ds.Tables[0].Rows[l]["CourtID"]) != 3002 && Convert.ToInt32(ds.Tables[0].Rows[l]["CourtID"]) != 3003))
                                            {
                                                if (ClsCase.ValidateTrialLetterPrintOption(TicketID))
                                                {
                                                    //
                                                    IsAlreadyInBatchPrint();
                                                    if (lbl_IsAlreadyInBatchPrint.Text == "1")
                                                    {
                                                        RemoveExistingEntry();
                                                    }
                                                    string[] key2 = { "@TicketIDList", "@empid" };
                                                    object[] value12 = { TicketID, empid };
                                                    Int32 LetterType = 2;
                                                    string filename = Server.MapPath("..\\Reports") + "\\Trial_Notification.rpt";
                                                    clsCrsytalComponent ClsCr = new clsCrsytalComponent();
                                                    ClsCr.CreateReportForEntry(filename, "USP_HTS_Trialletter", key2, value12, "true", LetterType, this.Session, this.Response);

                                                    //
                                                }

                                            }
                                            else
                                                //Inside court status in Arr
                                                if ((dcourt >= DateTime.Now.Date) && Convert.ToInt32(ds.Tables[0].Rows[l]["categoryid"]) == 4 && (Convert.ToInt32(ds.Tables[0].Rows[l]["CourtId"]) == 3001 || Convert.ToInt32(ds.Tables[0].Rows[l]["CourtId"]) == 3002 || Convert.ToInt32(ds.Tables[0].Rows[l]["CourtId"]) == 3003))
                                                {
                                                    if (ClsCase.ValidateTrialLetterPrintOption(TicketID))
                                                    {
                                                        //
                                                        IsAlreadyInBatchPrint();
                                                        if (lbl_IsAlreadyInBatchPrint.Text == "1")
                                                        {
                                                            RemoveExistingEntry();
                                                        }
                                                        string[] key2 = { "@TicketIDList", "@empid" };
                                                        object[] value12 = { TicketID, empid };
                                                        Int32 LetterType = 2;
                                                        string filename = Server.MapPath("..\\Reports") + "\\Trial_Notification.rpt";
                                                        clsCrsytalComponent ClsCr = new clsCrsytalComponent();
                                                        ClsCr.CreateReportForEntry(filename, "USP_HTS_Trialletter", key2, value12, "true", LetterType, this.Session, this.Response);

                                                        //
                                                    }

                                                }
                                            // 
                                        }
                                    }
                                    
                                }
                            }
                        }


                    }
                }

                
            }
            //catch (Exception ex)
            //{
            //    Response.Write(ex.Message);
            //}
        }
    }
}

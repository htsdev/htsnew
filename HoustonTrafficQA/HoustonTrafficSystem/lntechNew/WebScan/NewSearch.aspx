<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewSearch.aspx.cs" Inherits="lntechNew.WebScan.NewSearch" %>

<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="~/WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Search Page</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />

    <script language="javascript">
    function DeleteConfirm()
    {
        var isDelete=confirm("Are You Sure You Want To Delete This Batch");
        if(isDelete== true)
        {
           
        }
        
        else
        {
            return false;
        }
    }
    
    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="TableMain" cellspacing="0" cellpadding="0" width="780" align="center"
            border="0">
            <tr>
                <td colspan="2">
                    <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                </td>
            </tr>
            <tr>
                <td>
                    <table border="0" cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td class="clssubhead" background="../Images/subhead_bg.gif" height="34">
                                &nbsp;Search Documents
                            </td>
                            <td class="clssubhead" background="../Images/subhead_bg.gif" height="34" align="right">
                                &nbsp;<asp:HyperLink ID="scan" runat="server" NavigateUrl="~/WebScan/Default.aspx">Scan</asp:HyperLink>&nbsp;&nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="clsLeftPaddingTable">
                </td>
            </tr>
            <tr>
                <td class="clsLeftPaddingTable" colspan="2" width="100%">
                    <table id="tbl" cellpadding="0" cellspacing="0" width="100%" border="0">
                        <tr>
                            <td class="clsLeftPaddingTable" width="25%">
                                <strong><span style="font-size: 9pt">Search Scanned Batch Dates:</span></strong>
                            </td>
                            <td width="16%">
                                <ew:CalendarPopup ID="fromdate" runat="server" AllowArbitraryText="False" CalendarLocation="Bottom"
                                    ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma" Font-Size="8pt"
                                    ImageUrl="~/Images/calendar.gif" PadSingleDigits="True" ShowGoToToday="True"
                                    ToolTip="Select Scan Date From" UpperBoundDate="12/31/9999 23:59:00" Width="90px"
                                    LowerBoundDate="1900-01-01">
                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TextboxLabelStyle CssClass="clstextarea" />
                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Gray" />
                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                </ew:CalendarPopup>
                            </td>
                            <td class="clsLeftPaddingTable" width="4%">
                                <strong>To</strong>
                            </td>
                            <td width="16%">
                                <ew:CalendarPopup ID="enddate" runat="server" AllowArbitraryText="False" CalendarLocation="Bottom"
                                    ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma" Font-Size="8pt"
                                    ImageUrl="~/Images/calendar.gif" PadSingleDigits="True" ShowGoToToday="True"
                                    ToolTip="Select Scan Date From" UpperBoundDate="12/31/9999 23:59:00" Width="90px"
                                    LowerBoundDate="1900-01-01">
                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TextboxLabelStyle CssClass="clstextarea" />
                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Gray" />
                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                </ew:CalendarPopup>
                            </td>
                            <td width="25%">
                                <asp:CheckBox ID="chk" runat="server" CssClass="clsLeftPaddingTable" Text="Show Batches With Non Verified"
                                    Checked="True" />
                            </td>
                            <td>
                                <asp:Button ID="btnSearch" runat="server" CssClass="clsbutton" Text="Search" OnClick="btnSearch_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2" class="clsLeftPaddingTable">
                    <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2" class="clsLeftPaddingTable" id="tdProcess" style="display: none">
                    <asp:Label ID="lblProess" runat="server" Text="Please Wait Scanning and OCR is in Process..."
                        ForeColor="Red" Font-Bold="true"></asp:Label>
                </td>
            </tr>
            <tr>
                <td valign="top" colspan="2" style="height: 144px">
                    <table id="tbl1" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td width="100%">
                                <asp:GridView ID="gv_Scan" runat="server" Width="100%" AutoGenerateColumns="False"
                                    AllowPaging="True" AllowSorting="True" PageSize="15" OnPageIndexChanging="gv_Scan_PageIndexChanging"
                                    OnRowDataBound="gv_Scan_RowDataBound" OnSorting="gv_Scan_Sorting">
                                    <AlternatingRowStyle BackColor="#EEEEEE" />
                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" CssClass="GrdHeader" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Batch" SortExpression="batchid">
                                            <ItemTemplate>
                                                <asp:Label ID="lblbatchid" runat="server" Text='<%# bind("batchid") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle ForeColor="#006699" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Images" SortExpression="ImagesCount">
                                            <ItemTemplate>
                                                <asp:Label ID="lblimages" CssClass="label" runat="server" Text='<%# bind("ImagesCount") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle ForeColor="#006699" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Scan Type" SortExpression="scantype">
                                            <ItemTemplate>
                                                <asp:Label ID="lblScanType" CssClass="label" runat="server" Text='<%# bind("scantype") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle ForeColor="#006699" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Scan Date" SortExpression="scandate">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDate" CssClass="label" runat="server" Text='<%# bind("scandate") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle ForeColor="#006699" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Verified" SortExpression="verifiedCount">
                                            <HeaderStyle ForeColor="#006699" />
                                            <ItemTemplate>
                                                <asp:HyperLink ID="verified" ForeColor="RoyalBlue" runat="server" Text='<%# bind("verifiedCount") %>'></asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="No Cause" SortExpression="NoCauseCount">
                                            <HeaderStyle ForeColor="#006699" />
                                            <ItemTemplate>
                                                <asp:HyperLink ID="nocause" ForeColor="RoyalBlue" runat="server" Text='<%# bind("NoCauseCount") %>'></asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Queued" SortExpression="QueuedCount">
                                            <HeaderStyle ForeColor="#006699" />
                                            <ItemTemplate>
                                                <asp:HyperLink ID="queued" ForeColor="RoyalBlue" runat="server" Text='<%# bind("QueuedCount") %>'></asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Discrepancy" SortExpression="DiscrepancyCount">
                                            <HeaderStyle ForeColor="#006699" />
                                            <ItemTemplate>
                                                <asp:HyperLink ID="discrepancy" ForeColor="RoyalBlue" runat="server" Text='<%# bind("DiscrepancyCount") %>'></asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Pending" SortExpression="PendingCount">
                                            <HeaderStyle ForeColor="#006699" />
                                            <ItemTemplate>
                                                <asp:HyperLink ID="pending" ForeColor="RoyalBlue" runat="server" Text='<%# bind("PendingCount") %>'></asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="img_delete" runat="server" ImageUrl="~/Images/cross.gif" CommandArgument='<%# bind("batchid") %>'
                                                    OnCommand="delete" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkbtn_verify" runat="server" CommandArgument='<%# bind("batchid") %>'
                                                    CommandName="AutoVerify" OnCommand="lnkbtn_verify_Command">Auto Verify</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Center" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td background="../Images/separator_repeat.gif" style="height: 11px">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                            </td>
                        </tr>
                        <tr>
                            <td style="visibility: hidden">
                                <asp:TextBox ID="txtsessionid" runat="server"></asp:TextBox>
                                <asp:TextBox ID="txtempid" runat="server"></asp:TextBox>
                                <asp:TextBox ID="txtSrv" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="display: none">
                    <asp:Label ID="lbl_IsAlreadyInBatchPrint" runat="server" Text="Label"></asp:Label><asp:Label
                        ID="lbl_IsSplit" runat="server" Text="Label"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

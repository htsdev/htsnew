using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;
using HTP.Components;

namespace lntechNew.WebControls
{

    public delegate void PageMethodHandler();

    public partial class UpdateViolation : System.Web.UI.UserControl
    {

        #region Events

        // Page Event Handler    
        public event PageMethodHandler PageMethod;

        // Page Load Event 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                alrhouston = false;
                // Get Violations
                FillViolations(0);
                // Get Courts
                FillCourtLocation();
                // Get Court Status
                deleteALR = true;

                FillStatus(Convert.ToInt32(hf_courtid.Value));
                // Set Control Scripts
                setControlScripts();
                // Fill Charge Level
                FillChargeLevel(0);
                //Get NOS Flag Information
                GetNOSFlagInfo();
            }
            else
            {
                if (hf_courtid.Value != ddl_pcrtloc.SelectedValue)
                    ddl_pcrtloc_SelectedIndexChanged(null, null);
            }
        }

        // Save Violation Event
        protected void btn_popup_Click(object sender, EventArgs e)
        {
            //This Function is Used to Update Violation Information
            updatePanel_update();

            if (PageMethod != null)
                PageMethod();

            //Comment By Zeeshan Ahmed
            //No Need To Set This Value Because Popup Has Been Closed
            //if (hf_iscriminalcourt.Value != "1")
            //{
            //    ddl_pvlevel.Text = "0";
            //}

        }


        // Control Pre Render Event
        protected override void OnPreRender(EventArgs e)
        {

            if (!IsPostBack)
            {
                if (ddl_pcrtloc.Items.FindByText("---Choose---") == null)
                    ddl_pstatus.Items.Insert(0, new ListItem("---Choose---", "0"));

                if (!iscasedisposition)
                {
                    ddl_pstatus.Items.Remove(ddl_pstatus.Items.FindByText("DISPOSED"));
                }
            }

        }

        // Court Location Index Changed Event
        protected void ddl_pcrtloc_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ADDED BY FAHAD FOR DISPLAYING HARRIS COUNTY CRIMINAL COURT STATUS & VIOLATIONS
            if (ddl_pcrtloc.SelectedValue == "3037")
            {

                pctrstatus.Value = ddl_pstatus.SelectedValue.ToString();
                defviolation = ddl_pvdesc.SelectedValue.ToString();
                hcviolation = ddl_pvdesc.SelectedValue.ToString();
                deleteALR = false;
                alrhouston = false;
            }
            else if (ddl_pcrtloc.SelectedValue == "3047")
            {
                alrhouston = true;
                hf_status.Value = "0";
                hf_violationid.Value = "0";
                deleteALR = false;
            }

            int courtid;
            if (("0" == hf_newviolation.Value) && ("0" == ddl_pcrtloc.SelectedValue))
                courtid = Convert.ToInt32(hf_courtid.Value);
            else
                courtid = Convert.ToInt32(ddl_pcrtloc.SelectedValue);

            FillStatus(courtid);
            FillPricePlan();
            FillViolations(courtid);
            FillChargeLevel(courtid);
            // Set Drop Down List Style
            ddl_pvdesc.Style["Display"] = hf_vstyle.Value;
            ddl_pstatus.Style["Display"] = hf_sstyle.Value;

            //Zeeshan Ahmed 3724 05/12/2008
            bool CriminalCourt = ClsCourts.CheckCriminalCourt(courtid);

            if (courtid != 3047)
            {
                if (CriminalCourt)
                {
                    tr_chargelevel.Style["display"] = "block";
                    tr_bondamount.Style["display"] = "none";
                    tr_fineamount.Style["display"] = "none";
                    tr_arrestdate.Style["display"] = "block";
                    hf_iscriminalcourt.Value = "1";

                }
                else
                {
                    tr_chargelevel.Style["display"] = "none";
                    tr_fineamount.Style["display"] = "block";
                    tr_bondamount.Style["display"] = "block";
                    tr_arrestdate.Style["display"] = "none";
                    hf_iscriminalcourt.Value = "0";

                }
            }
            else
            {
                tr_chargelevel.Style["display"] = "none";
                tr_fineamount.Style["display"] = "block";

            }

            if (courtid == 3037)
                tr_cdi.Style["display"] = "block";
            else
                tr_cdi.Style["display"] = "none";

            //added by Ozair for task 2515 on 01/02/2008
            if (courtid != 3001 && courtid != 3002 && courtid != 3003 && !CriminalCourt)
            {
                tr_bondamount.Style["display"] = "block";
            }
            else
            {
                tr_bondamount.Style["display"] = "none";
            }

            //Added By Zeeshan Ahmed On 1/25/2008
            if (ddl_ppriceplan.Items.FindByValue(hf_planid.Value) != null)
                ddl_ppriceplan.SelectedValue = hf_planid.Value;

            if (ddl_pcrtloc.Items.FindByValue(hf_courtid.Value) != null)
                lbl_pcname.Style["display"] = "none";
            else
            {
                ddl_pcrtloc.Style["display"] = "none";
                lbl_pcname.Style["display"] = "block";
                lbl_pcname.Text = hf_pcname.Value;
            }

            if (ddl_pcrtloc.SelectedValue != hf_courtid.Value && ddl_pcrtloc.Items.FindByValue(hf_courtid.Value) != null)
                hf_courtid.Value = "0";

            //Fixed By Zeeshan Ahmed Bug ID : 2745
            //Set Charge Level 
            if (ddl_pvlevel.Items.FindByValue(hf_Level.Value) != null)
                ddl_pvlevel.SelectedValue = hf_Level.Value;

            //Zeeshan Ahmed 4163 06/10/2008 Set Status In The List
            if (ddl_pstatus.Items.FindByValue(hf_status.Value) != null && hf_sstyle.Value == "none")
                ddl_pstatus.SelectedValue = hf_status.Value;
        }

        protected void imgbtn_Click(object sender, ImageClickEventArgs e)
        {
            if (ddl_pcrtloc.SelectedValue == "3037")
            {
                deleteALR = false;
                FillViolations(Convert.ToInt32(ddl_pcrtloc.SelectedValue));
                FillStatus(Convert.ToInt32(ddl_pcrtloc.SelectedValue));
            }
            if (ddl_pcrtloc.SelectedValue == "3047")
            {
                alrhouston = true;
                deleteALR = false;
                FillViolations(3047);
                FillStatus(Convert.ToInt32(ddl_pcrtloc.SelectedValue));
            }
            ddl_pstatus.Style["Display"] = hf_sstyle.Value;
            ddl_pvdesc.Style["Display"] = hf_vstyle.Value;
        }

        protected void imgbtn_status(object sender, ImageClickEventArgs e)
        {
            if (ddl_pcrtloc.SelectedValue == "3047")
            {
                alrhouston = true;
                deleteALR = false;
                FillViolations(3047);
                FillStatus(Convert.ToInt32(ddl_pcrtloc.SelectedValue));
            }
            ddl_pstatus.Style["Display"] = hf_sstyle.Value;
            ddl_pvdesc.Style["Display"] = hf_vstyle.Value;

        }


        #endregion

        #region Variables

        private string error;
        private string gridfield;
        private string gridid;
        private string divid;
        bool iscasedisposition = false;
        bool deleteALR = true;
        bool alrhouston = false;

        string hcviolation;
        string defviolation;



        clsCaseDetail ClsCaseDetail = new clsCaseDetail();
        clsCourts ClsCourts = new clsCourts();
        clsENationWebComponents clsdb = new clsENationWebComponents();
        clsCaseStatus ClsCaseStatus = new clsCaseStatus();
        clsSession ClsSession = new clsSession();
        clsLogger bugTracker = new clsLogger();
        clsCase ClsCase = new clsCase();


        #endregion

        #region Properties

        public bool IsCaseDisposition
        {
            set
            {
                iscasedisposition = value;
            }
        }

        public string GridID
        {
            set
            {
                gridid = value;
            }
        }

        public string DivID
        {
            set
            {
                divid = value;
            }
        }

        public string GridField
        {
            set
            {
                gridfield = value;
            }
        }

        public string ErrorMessage
        {
            get
            {
                return error;
            }
        }

        #endregion

        #region Methods

        // Update Violation Information
        public void updatePanel_update()
        {

            updatePanel upnl = new updatePanel();
            // Initialize Update Panel Class

            //Setting Casuse No and Ticket Number

            upnl.CauseNo = tb_causeno.Text;
            upnl.TicketNumber = tb_pticketno.Text;
            /// Added by asghar oscare court 
            upnl.OscareCourtDetail = tb_oscare_court.Text;
            //Setting Violation ID
            upnl.ViolationID = Convert.ToInt32(hf_violationid.Value);
            //Setting Fine And Bond Amount
            upnl.FineAmount = Convert.ToDouble(tb_pfineamount.Text);
            upnl.BondAmount = Convert.ToDouble(tb_pbondamount.Text);
            //Setting Bond Flag
            upnl.BondFlag = rbl_bondflag.SelectedValue;
            // Setting Court Date
            if (tb_pmonth.Text == "0" || tb_pmonth.Text == String.Empty || tb_pday.Text == "0" || tb_pday.Text == String.Empty || tb_pyear.Text == "0" || tb_pyear.Text == String.Empty)
            {
                DateTime courtDate = Convert.ToDateTime("01/01/1900");
                upnl.CourtDate = courtDate;
            }
            else
            {
                DateTime courtDate = Convert.ToDateTime(String.Concat(tb_pmonth.Text.ToString() + "/", tb_pday.Text.ToString() + "/", tb_pyear.Text.ToString() + " ", ddl_pcrttime.SelectedValue.ToString()));
                upnl.CourtDate = courtDate;
            }

            //Zeeshan Ahmed 3724 05/13/2008
            DateTime ArrestDate = DateTime.Parse("1/1/1900");

            if (!(tb_arrmonth.Text == "0" || tb_arrmonth.Text == String.Empty || tb_arrday.Text == "0" || tb_arrday.Text == String.Empty || tb_arryear.Text == "0" || tb_arryear.Text == String.Empty))
                ArrestDate = Convert.ToDateTime(String.Concat(tb_arrmonth.Text.ToString() + "/", tb_arrday.Text.ToString() + "/", tb_arryear.Text.ToString()));

            upnl.ArrestDate = ArrestDate;
            // Setting Court Time
            upnl.CourtTime = ddl_pcrttime.SelectedValue;
            //Setting Court Number
            if (tb_prm.Text != "")
                upnl.CourtNo = Convert.ToInt32(tb_prm.Text);
            else
                upnl.CourtNo = 0;

            // Added By Zeeshan For Criminal Courts
            if (hf_iscriminalcourt.Value == "1")
            {
                upnl.FineAmount = 0;
                if (hf_Level.Value != "0" && ddl_pvlevel.SelectedValue == "0")
                {
                    ddl_pvlevel.SelectedValue = hf_Level.Value;
                }
                upnl.ChargeLevel = Convert.ToInt32(ddl_pvlevel.SelectedValue);
            }
            else
            {
                upnl.FineAmount = Convert.ToDouble(tb_pfineamount.Text);
                upnl.ChargeLevel = 0;

            }

            //Setting Court Status
            upnl.Status = Convert.ToInt32(hf_status.Value);
            //Setting Court Location
            upnl.CourtLocation = Convert.ToInt32(hf_courtid.Value);
            //Setting Price Plan
            upnl.PricePlan = Convert.ToInt32(ddl_ppriceplan.SelectedValue);

            if (hf_newviolation.Value == "1")
                upnl.IsNewViolation = true;
            else
                upnl.IsNewViolation = false;

            // Setting Employee ID
            int EmpID = Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request));
            upnl.EmpID = EmpID;
            // Specify Grid
            upnl.dgViolationInfo = (DataGrid)Page.FindControl(gridid); // dgViolationInfo;
            // Specify Grid Field
            // upnl.GridField = "hf_ticketviolationid";
            upnl.GridField = gridfield;
            upnl.clsdb = clsdb;
            // Set Case Number
            ClsCase.TicketID = Convert.ToInt32(hf_ticketid.Value);

            upnl.ClsCase = ClsCase;
            upnl.ClsCaseDetail = ClsCaseDetail;
            upnl.bugTracker = bugTracker;

            upnl.TicketViolationID = Convert.ToInt32(hf_ticketviolationid.Value);
            upnl.UpdateAll = cb_updateall.Checked;

            // if Arraingment Waiting  status updated
            if (ddl_pstatus.SelectedValue == "201")
                upnl.ArrignmentWaitingDate = true;
            else
                upnl.ArrignmentWaitingDate = false;

            // if Bond Waiting  status updated
            if (ddl_pstatus.SelectedValue == "202")
                upnl.BondWaitingDate = true;

            else
                upnl.BondWaitingDate = false;

            upnl.CDI = ddl_cdi.SelectedValue;

            //Zeeshan 4163 06/02/2008 Check Missed Court Type
            string CurrMissedCourtType = "-1";
            string PrevMissedCourtType = hf_missedcourttype.Value;

            CurrMissedCourtType = (rbtnNoAction.Checked ? "1" : CurrMissedCourtType);
            CurrMissedCourtType = (rbtnPledOut.Checked ? "2" : CurrMissedCourtType);

            upnl.MissedCourtType = Convert.ToInt32(CurrMissedCourtType);
            upnl.UpdateViolationDetail();
            error = upnl.Error;


            //added by Agha Usman for task 2630 on 01/22/2008

            if (ddl_pcrtloc.SelectedValue == "3061" || hf_courtloc.Value == "3061")// In case of Orcar client
            {
                if (hf_courtroomno.Value != tb_prm.Text || hv_viostatus.Value != ddl_pstatus.SelectedValue || hf_courttime.Value != ddl_pcrttime.SelectedValue || hf_day.Value != tb_pday.Text || hf_Month.Value != tb_pmonth.Text || hf_year.Value != tb_pyear.Text || hf_courtloc.Value != ddl_pcrtloc.SelectedValue)
                {
                    UpdateTicketMailStatus(int.Parse(hf_ticketid.Value), 1);
                }

                MailOscarClient(Convert.ToInt32(hf_ticketid.Value));
            }
            //end

            //Zeeshan Ahmed 3486 04/04/2008
            //Send Missed Court Letter to Batch If Selected
            //Zeeshan Ahmed 4163 06/03/2008 If Missed Court Type Changed
            if (PrevMissedCourtType != CurrMissedCourtType && hf_status.Value == "105")
            {
                if (rbtnNoAction.Checked || rbtnPledOut.Checked)
                {
                    Hashtable ticketTable = new Hashtable();
                    ticketTable.Add(ClsCase.TicketID, ClsCase.TicketID);
                    clsDocketCloseOut docketCloseOut = new clsDocketCloseOut();
                    docketCloseOut.EmpId = EmpID;
                    if (rbtnNoAction.Checked)
                        docketCloseOut.SendMissedCourtLetterToBatch(ticketTable, BatchLetterType.MissedCourtLetter);
                    else if (rbtnPledOut.Checked)
                        docketCloseOut.SendMissedCourtLetterToBatch(ticketTable, BatchLetterType.PledOutLetter);
                }
            }

            //Zeeshan Ahmed 3757 04/17/2008
            //Add NOS Flag If Cause Number Is Blank
            if (tb_causeno.Text == "" && hf_hasNOS.Value == "0" && hf_UpdateNOS.Value == "1")
            {
                ClsFlags flags = new ClsFlags();
                clsNOS nos = new clsNOS();

                //Check NOS Flag
                flags.FlagID = 15; flags.TicketID = ClsCase.TicketID; flags.EmpID = EmpID;
                //Update NOS Date
                nos.UpdateNOS(flags.TicketID, flags.EmpID);

                //Add If Flag Not Exist
                if (!flags.HasFlag())
                {
                    //Add Flag
                    if (flags.AddNewFlag())
                    {
                        hf_UpdateNOS.Value = "0";
                        hf_hasNOS.Value = "1";
                    }
                }
            }

        }

        private void MailOscarClient(int ticketid)
        {
            string[] keys = { "@TicketID" };
            object[] values = { ticketid };
            DataTable dt = clsdb.Get_DT_BySPArr("USP_HTS_GET_OSCARMAILFLAG", keys, values);

            if (dt.Rows.Count > 0)
                if (Convert.ToInt32(dt.Rows[0]["OscarMailFlag"]) == 1)
                {
                    CaseSummary(ticketid);
                    SendMail();
                    UpdateTicketMailStatus(ticketid, 0);
                    //string[] keys1 = { "@TicketID", "@OscarMailFlag" };
                    //object[] values1 = { ticketid, "0" };
                    //clsdb.ExecuteSP("USP_HTS_SET_OSCARMAILFLAG", keys1, values1);

                }

        }


        //added by Agha Usman for task 2630 on 01/22/2008
        protected void UpdateTicketMailStatus(int TicketId, int status)
        {
            string[] keys = { "@TicketID", "@OscarMailFlag" };
            //object[] values = { Convert.ToInt32(hf_ticketid.Value), "1" };
            object[] values = { Convert.ToInt32(TicketId), status };
            clsdb.ExecuteSP("USP_HTS_SET_OSCARMAILFLAG", keys, values);
        }
        //end
        private void CaseSummary(int ticketid)
        {
            try
            {

                DataSet ds;

                string RptSetting = "123456";

                String[] reportname = new String[] { "Notes.rpt", "Matters.rpt" };

                string filename = Server.MapPath("\\Reports") + "\\CaseSummary.rpt";


                string[] keyssub = { "@TicketID" };
                object[] valuesub = { ticketid };
                ds = clsdb.Get_DS_BySPArr("USP_HTS_GET_SubReports", keyssub, valuesub);

                lntechNew.Components.clsCrsytalComponent Cr = new lntechNew.Components.clsCrsytalComponent();
                string path = ConfigurationManager.AppSettings["NTPATHCaseSummary"].ToString();
                string[] key = { "@TicketId_pk", "@Sections" };
                object[] value1 = { ticketid, RptSetting };
                string name = ticketid.ToString() + "-CoverSheet-" + DateTime.Now.ToFileTime() + ".pdf";
                Cr.CreateSubReports(filename, "usp_hts_get_casesummary_report_data", key, value1, reportname, ds, 1, this.Session, this.Response, path, ticketid.ToString(), false, name);

                ViewState["vFullPath"] = path + name;
            }
            catch (Exception ex)
            {
                //lblMessage.Text = ex.Message.ToString();
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void SendMail()
        {
            try
            {

                //Kazim 2866 2/7/2008 replace the key "osemailto" with "ocupdateemailto" 
                string toUser = (string)ConfigurationManager.AppSettings["OcUpdateEmailTo"];
                string ccUser = (string)ConfigurationManager.AppSettings["OsEmailCC"];
                lntechNew.Components.MailClass.SendMailToAttorneys("This is a system generated email.", "An Oscar client updated.", Convert.ToString(ViewState["vFullPath"]), toUser, ccUser);
            }
            catch (Exception ex)
            {
                //lblMessage.Text = "Email not sent.";
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        // Get All Violations
        private void FillViolations(int courtid)
        {
            try
            {
                clsViolations ClsViolations = new clsViolations();
                DataSet ds_Violation;


                if (courtid == 0)
                    ds_Violation = ClsViolations.GetAllViolations();
                else
                    ds_Violation = ClsViolations.GetAllViolationsByCourtLocation(courtid);

                // Display Violations 
                ddl_pvdesc.Items.Clear();
                ddl_pvdesc.DataSource = ds_Violation.Tables[0];
                ddl_pvdesc.DataTextField = "Description";
                //checked by khalid for bug 2209 not occuring now
                ddl_pvdesc.DataValueField = "ID";
                ddl_pvdesc.DataBind();



                //Added By fahad for displaying violations for  Harris County Criminal Court
                if (deleteALR == true)
                    hf_lastcourtid.Value = "0";

                else if (deleteALR == false && alrhouston == false)
                {
                    ddl_pvdesc.SelectedIndex = 0;
                    hf_lastcourtid.Value = "1";

                    //Kazim 2754 4/3/2008 Check the existance of item before removing it from list. 


                    if (ddl_pvdesc.Items.FindByText("ALR") != null)
                    {
                        ddl_pvdesc.Items.Remove(ddl_pvdesc.Items.FindByText("ALR"));
                    }

                }




                if (ddl_pvdesc.Items.FindByValue(hf_violationid.Value) != null)
                    ddl_pvdesc.SelectedValue = hf_violationid.Value;

                if (alrhouston == true)
                {
                    //Kazim 2754 4/3/2008 Check the existance of item before selected it. 

                    if (ddl_pvdesc.Items.FindByValue("16159") != null)
                    {
                        ddl_pvdesc.SelectedValue = "16159"; //Live ID 16159 //local 16159
                    }
                }



                //End Fahad
            }
            catch (Exception ex)
            {
                this.error = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        // Get Selected Price Plan
        private void FillPricePlan()
        {

            try
            {

                bool isNew = false;

                if (hf_newviolation.Value == "1")
                    isNew = true;

                clsPricingPlans ClsPricingPlans = new clsPricingPlans();

                DataSet ds_PricePlan;
                //if (txtinactive.Text == "1")
                {
                    try
                    {
                        //       ClsPricingPlans.CourtID = Convert.ToInt32(lblinactiveCourtID.Text);
                        //     ddl_PricePlan.Enabled = false;
                    }
                    catch { }
                }
                // else
                {
                    // Aziz 1517
                    int courtid;
                    if (("0" == hf_newviolation.Value) && ("0" == ddl_pcrtloc.SelectedValue))
                        courtid = Convert.ToInt32(hf_courtid.Value);
                    else
                        courtid = Convert.ToInt32(ddl_pcrtloc.SelectedValue);


                    ClsPricingPlans.CourtID = Convert.ToInt32(courtid);
                    //Aziz end
                    ddl_pcrtloc.Enabled = true;

                    if (ClsPricingPlans.CourtID == 3061)
                    {
                        tb_oscare_court.Style["display"] = "block";
                        tb_oscare_court.Focus();
                    }
                    else
                    {
                        tb_oscare_court.Text = "";
                        tb_oscare_court.Style["display"] = "none";
                    }
                }
                //if (IsNewViolation==true)
                if (isNew)
                {
                    ClsPricingPlans.IsNew = 1;
                    ds_PricePlan = ClsPricingPlans.GetAllPlan();
                }
                else
                {
                    ClsPricingPlans.IsNew = 0;
                    ds_PricePlan = ClsPricingPlans.GetAllPlan();
                }


                ddl_ppriceplan.Items.Clear();
                ddl_ppriceplan.Items.Add(new ListItem("--Choose--", "0"));
                for (int i = 0; i < ds_PricePlan.Tables[0].Rows.Count; i++)
                {
                    string description = ds_PricePlan.Tables[0].Rows[i]["PlanShortName"].ToString().Trim();
                    string ID = ds_PricePlan.Tables[0].Rows[i]["ID"].ToString().Trim();
                    ddl_ppriceplan.Items.Add(new ListItem(description, ID));
                }


                //if (IsNewViolation==true)
                if (isNew)
                {
                    try
                    {
                        string date = DateTime.Now.ToShortDateString();
                        DataSet ds_PlanID = ClsPricingPlans.GetPlan(date, ClsPricingPlans.CourtID, 0);
                        ddl_ppriceplan.SelectedValue = ds_PlanID.Tables[0].Rows[0]["planid"].ToString().Trim();
                    }
                    catch (Exception)
                    { }
                }
                else if (ClsCaseDetail.PlanID != 0)
                {
                    try
                    {
                        ddl_ppriceplan.SelectedValue = ClsCaseDetail.PlanID.ToString();
                    }
                    catch (Exception)
                    { }
                }
                else if (ClsCaseDetail.PlanID == 0)
                {
                    try
                    {
                        string date = string.Concat(tb_pmonth.Text, "/", tb_pday.Text, "/", tb_pyear.Text);
                        DataSet ds_PlanID = ClsPricingPlans.GetPlan(date, ClsPricingPlans.CourtID, 1);
                        ddl_ppriceplan.SelectedValue = ds_PlanID.Tables[0].Rows[0]["planid"].ToString().Trim();

                    }
                    catch (Exception)
                    {
                    }
                }
            }
            catch (Exception ex)
            {
                this.error = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        // Fills Court Location and All Price Plan
        private void FillCourtLocation()
        {
            try
            {
                DataSet ds_ActiveCourts = ClsCourts.GetAllActiveCourtName();
                //added by khalid to avoid 2110 bug
                //2-1-08
                if (ds_ActiveCourts == null)
                {
                    this.error = "Active courts can't be found";
                    return;
                }
                //end khalid for 2210 bug


                // Filling Court Location
                ddl_pcrtloc.DataSource = ds_ActiveCourts.Tables[0];
                ddl_pcrtloc.DataTextField = "shortname";
                ddl_pcrtloc.DataValueField = "courtid";
                ddl_pcrtloc.DataBind();

                // Filling Price Plan List 
                ddl_ppriceplan.DataSource = clsdb.Get_DT_BySPArr("USP_HTS_getAllPricePlan");
                ddl_ppriceplan.DataTextField = "planshortname";
                ddl_ppriceplan.DataValueField = "planid";
                ddl_ppriceplan.DataBind();

            }
            catch (Exception ex)
            {
                this.error = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        //fills Case status drop down list
        private void FillStatus()
        {
            try
            {
                ddl_pstatus.Items.Clear();
                DataSet ds_Status;
                if (alrhouston == true)
                {
                    ds_Status = ClsCaseStatus.GetALRCourtCaseStatus(iscasedisposition);
                }
                else
                {
                    ds_Status = ClsCaseStatus.GetAllCourtCaseStatus();
                }
                ddl_pstatus.DataSource = ds_Status.Tables[0];
                ddl_pstatus.DataTextField = "Description";
                ddl_pstatus.DataValueField = "ID";
                ddl_pstatus.DataBind();

                pctrstatus.Value = ddl_pstatus.SelectedValue.ToString();

                if (deleteALR == true)
                {

                    ddl_pstatus.Items.Remove(ddl_pstatus.Items.FindByText("ALR Hearing"));


                    ddl_pstatus.SelectedValue = Convert.ToString(pctrstatus.Value);

                }
                ddl_pstatus.SelectedValue = Convert.ToString(pctrstatus.Value);

                //if (alrhouston == true)
                //{
                //    ddl_pstatus.Items.Remove(ddl_pstatus.Items.FindByText("ARRAIGNMENT"));
                //    ddl_pstatus.Items.Remove(ddl_pstatus.Items.FindByText("ALR Hearing"));
                //    ddl_pstatus.Items.Remove(ddl_pstatus.Items.FindByText("BOND"));
                //    ddl_pstatus.Items.Remove(ddl_pstatus.Items.FindByText("PRETRIAL"));
                //    ddl_pstatus.Items.Remove(ddl_pstatus.Items.FindByText("JUDGE"));
                //    ddl_pstatus.Items.Remove(ddl_pstatus.Items.FindByText("JURY"));
                //    ddl_pstatus.Items.Remove(ddl_pstatus.Items.FindByText("BOND FORFEITURE"));
                //    ddl_pstatus.Items.Remove(ddl_pstatus.Items.FindByText("NON ISSUE"));
                //    ddl_pstatus.Items.Remove(ddl_pstatus.Items.FindByText("SCIRE"));
                //    ddl_pstatus.Items.Remove(ddl_pstatus.Items.FindByText("MISSED COURT"));
                //    ddl_pstatus.Items.Remove(ddl_pstatus.Items.FindByText("DAR/MISSING CASE"));
                //    ddl_pstatus.Items.Remove(ddl_pstatus.Items.FindByText("OTHER"));
                //    ddl_pstatus.Items.Remove(ddl_pstatus.Items.FindByText("DISPOSED"));
                //    ddl_pstatus.Items.Remove(ddl_pstatus.Items.FindByText("NO HIRE"));

                //}


                //Added By fahad for displaying ALR HEARING status for  Harris County Criminal Court

                //end Fahad


            }
            catch (Exception ex)
            {
                this.error = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        //fills Case status drop down list for the selected court
        private void FillStatus(int courtid)
        {
            try
            {
                ddl_pstatus.Items.Clear();
                DataSet ds_Status;
                ds_Status = ClsCaseStatus.GetAllCourtCaseStatus(courtid, iscasedisposition);
                ddl_pstatus.DataSource = ds_Status.Tables[0];
                ddl_pstatus.DataTextField = "Description";
                ddl_pstatus.DataValueField = "ID";
                ddl_pstatus.DataBind();

                pctrstatus.Value = ddl_pstatus.SelectedValue.ToString();

                if (deleteALR == true)
                {

                    ddl_pstatus.Items.Remove(ddl_pstatus.Items.FindByText("ALR Hearing"));


                    ddl_pstatus.SelectedValue = Convert.ToString(pctrstatus.Value);

                }
                ddl_pstatus.SelectedValue = Convert.ToString(pctrstatus.Value);

            }
            catch (Exception ex)
            {
                this.error = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        // Set Javascript on the Control
        private void setControlScripts()
        {
            try
            {
                lbtn_close.Attributes.Add("onClick", "closepopup('" + Disable.ClientID + "','pnl_violation');return false;");
                img_violation.Attributes.Add("onClick", "DisplayToggleP('" + ddl_pvdesc.ClientID + "','" + lbl_pvdesc.ClientID + "','" + hf_vstyle.ClientID + "');");
                imgbtn_violation.Attributes.Add("onClick", "return DisplayTogglePNew('" + ddl_pvdesc.ClientID + "','" + lbl_pvdesc.ClientID + "','" + hf_vstyle.ClientID + "','" + ddl_pcrtloc.ClientID + "','" + hf_lastcourtid.ClientID + "');");
                img_status.Attributes.Add("onClick", "DisplayToggleP('" + ddl_pstatus.ClientID + "','" + lbl_pstatus.ClientID + "','" + hf_sstyle.ClientID + "');");
                btn_popup.Attributes.Add("onClick", "return submitPopup('" + this.ClientID + "','" + gridid + "','" + divid + "','" + hf_sstyle.ClientID + "');");
                ddl_pcrtloc.Attributes.Add("onChange", "chargelevel('" + this.ClientID + "','" + ddl_pvdesc.ClientID + "','" + lbl_pvdesc.ClientID + "','" + hf_vstyle.ClientID + "');");
                ImgCrtLoc.Attributes.Add("onClick", "return DisplayToggleP2('" + ddl_pstatus.ClientID + "','" + lbl_pstatus.ClientID + "','" + hf_sstyle.ClientID + "','" + ddl_pcrtloc.ClientID + "','" + hf_status.ClientID + "','" + rbtnNoAction.ClientID + "','" + rbtnPledOut.ClientID + "');");
                ddl_pstatus.Attributes.Add("onchange", "checkMissedCourtStatus(this,'" + hf_status.ClientID + "');");
            }
            catch (Exception ex)
            {
                this.error = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        // Fill Charge Label 
        private void FillChargeLevel(int courtid)
        {
            clsViolations ClsViolations = new clsViolations();
            DataSet chargelevel = ClsViolations.GetAllChargeLevel(courtid);
            ddl_pvlevel.Items.Clear();
            ddl_pvlevel.DataSource = chargelevel;
            ddl_pvlevel.DataTextField = "LevelCode";
            ddl_pvlevel.DataValueField = "ID";
            ddl_pvlevel.Items.Insert(0, new ListItem("--Choose--", "0"));
            ddl_pvlevel.DataBind();

        }

        private void GetNOSFlagInfo()
        {
            try
            {
                //Zeeshan Ahmed 3757 04/17/2008
                if (Request.QueryString["caseNumber"] != null)
                {
                    ClsFlags flags = new ClsFlags();
                    //Check NOS Flag
                    flags.FlagID = 15;
                    flags.TicketID = Convert.ToInt32(Request.QueryString["caseNumber"]);
                    hf_hasNOS.Value = Convert.ToInt32(flags.HasFlag()).ToString();
                }
                else
                    hf_hasNOS.Value = "0";
            }
            catch (Exception ex)
            {
                hf_hasNOS.Value = "0";
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        #endregion


    }
}
<%@ Control Language="c#" AutoEventWireup="false" CodeFile="LetterMainMenu.ascx.cs" Inherits="lntechNew.WebControls.LetterMainMenu" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>



<TABLE class="tableheader" id="tblLetterSubMenu" height="20" cellSpacing="0" 
cellPadding="0" width="100%">
  <TR>
    <TD align=right><asp:linkbutton id=lnkHome EnableViewState="False" ForeColor="Black" runat="server" CssClass="cmdlinks">Home</asp:linkbutton>&nbsp;<asp:label id=Label1 CssClass="VHeaderBar" Runat="server">|</asp:Label> 
<asp:linkbutton id=lnkUserList EnableViewState="False" ForeColor="Black" runat="server" CssClass="cmdlinks" Enabled="False">User List</asp:linkbutton>&nbsp;<asp:label id=lblVBar1 CssClass="VHeaderBar" Runat="server">|</asp:Label> 
<asp:linkbutton id=lnkUserManagement EnableViewState="False" ForeColor="Black" runat="server" CssClass="cmdlinks" Enabled="False">User Management</asp:linkbutton>&nbsp;<asp:label id=lblVBar2 CssClass="VHeaderBar" Runat="server">|</asp:Label> 
<asp:linkbutton id=lnkRoleManagement EnableViewState="False" ForeColor="Black" runat="server" CssClass="cmdlinks" Enabled="False">Role Management</asp:linkbutton>&nbsp;<asp:label id=Label2 CssClass="VHeaderBar" Runat="server">|</asp:Label> 
<asp:linkbutton id=lnkLogout EnableViewState="False" ForeColor="Black" runat="server" CssClass="cmdlinks">Logout</asp:linkbutton>&nbsp;</TD></TR></TABLE>

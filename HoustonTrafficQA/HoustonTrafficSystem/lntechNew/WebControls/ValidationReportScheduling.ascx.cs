﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using HTP.Components.ValidationReportConfig;
using lntechNew.Components.ClientInfo;
using System.Data;

namespace HTP.WebControls
{
    /// <summary>
    /// ValidationReportScheduling calss file.
    /// </summary>
    public partial class ValidationReportScheduling : UserControl
    {
        #region Variables

        readonly ValidationReportSettings _oValidationReportMapper = new ValidationReportSettings();
        #endregion

        #region Properties
        /// <summary>
        /// Report ID
        /// </summary>
        public string ReportId { get; set; }
        #endregion

        #region Events
        /// <summary>
        /// Page load event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// OnInit event.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            BtnUpdate.Click += BtnUpdate_Click;
        }
        /// <summary>
        /// Update button click handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void BtnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                object[] obj = {
                                   hdnReportID.Value,
                                   ddlWeekTimings1.SelectedValue,
                                   ddlWeekTimings2.SelectedValue,
                                   ddlWeekTimings3.SelectedValue,

                                   ddlSatTimings1.SelectedValue,
                                   ddlSatTimings2.SelectedValue,
                                   ddlSatTimings3.SelectedValue,

                                   hdnWeekdayTiming1.Value,
                                   hdnWeekdayTiming2.Value,
                                   hdnWeekdayTiming3.Value,

                                   hdnSaturdayTiming1.Value,
                                   hdnSaturdayTiming2.Value,
                                   hdnSaturdayTiming3.Value
                               };
                _oValidationReportMapper.UpdateValidationSchedule(obj);
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
            }
        }
        #endregion

        #region Methods
        //SAEED 7791 06/25/2010 method created.
        /// <summary>
        /// Fill dropdown list with all available timings.
        /// </summary>
        /// <param name="ddl">Dropdown object that needs to be populated with email timings.</param>
        /// <param name="isWeekDay">Weather dropdown belongs to weekday or saturday.  True if it belongs to weekday otherwise false.</param>
        private void FillTimings(ListControl ddl, bool isWeekDay)
        {
            try
            {
                var dt = _oValidationReportMapper.GetEmailTimings();
                ddl.Items.Clear();
                foreach (DataRow row in dt.Rows)
                {
                    var timingId = Convert.ToInt32(row["ID"]);
                    ListItem li;
                    if (isWeekDay && timingId < 4) //Weekday timings
                    {
                        li = new ListItem(row["Timings"].ToString(), row["ID"].ToString());
                        ddl.Items.Add(li);
                    }
                    else if (!isWeekDay && timingId>3 &&  timingId <= 6) //Saturday timings
                    {
                        li = new ListItem(row["Timings"].ToString(), row["ID"].ToString());
                        ddl.Items.Add(li);
                    }
                }
                ddl.Items.Insert(0, new ListItem("", "-1"));
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
            }
        }

        //SAEED 7791 06/25/2010 method created.
        /// <summary>
        /// A wrapper method to fill all timings dropdowns with available timings for weekdays and saturday.
        /// </summary>        
        private void FillAllDropDowns()
        {
            FillTimings(ddlWeekTimings1, true);
            FillTimings(ddlWeekTimings2, true);
            FillTimings(ddlWeekTimings3, true);
            FillTimings(ddlSatTimings1, false);
            FillTimings(ddlSatTimings2, false);
            FillTimings(ddlSatTimings3, false);
        }

        //SAEED 7791 06/25/2010 method created.
        /// <summary>
        /// Display email schedule timings for selected report.
        /// </summary>        
        public void DisplayReportSchedule()
        {
            try
            {
                FillAllDropDowns();
                if (!string.IsNullOrEmpty(ReportId))
                {
                    hdnReportID.Value = ReportId;
                    object[] obj = { Convert.ToInt32(ReportId), true };
                    var dt = _oValidationReportMapper.GetEmailTimingsByReportId(obj);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        lblReport.Text = dt.Rows[0]["Title"].ToString();

                        ddlWeekTimings1.SelectedValue = dt.Rows[0]["timingIdWeekday1"].ToString();
                        ddlWeekTimings2.SelectedValue = dt.Rows[0]["timingIdWeekday2"].ToString();
                        ddlWeekTimings3.SelectedValue = dt.Rows[0]["timingIdWeekday3"].ToString();

                        ddlSatTimings1.SelectedValue = dt.Rows[0]["timingIdSaturday1"].ToString();
                        ddlSatTimings2.SelectedValue = dt.Rows[0]["timingIdSaturday2"].ToString();
                        ddlSatTimings3.SelectedValue = dt.Rows[0]["timingIdSaturday3"].ToString();


                        hdnWeekdayTiming1.Value = dt.Rows[0]["scheduleIdWeekday1"].ToString();
                        hdnWeekdayTiming2.Value = dt.Rows[0]["scheduleIdWeekday2"].ToString();
                        hdnWeekdayTiming3.Value = dt.Rows[0]["scheduleIdWeekday3"].ToString();
                        hdnSaturdayTiming1.Value = dt.Rows[0]["scheduleIdSaturday1"].ToString();
                        hdnSaturdayTiming2.Value = dt.Rows[0]["scheduleIdSaturday2"].ToString();
                        hdnSaturdayTiming3.Value = dt.Rows[0]["scheduleIdSaturday3"].ToString();

                    }
                }
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
            }
        }
        #endregion
    }
}
﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using lntechNew.Components.ClientInfo;
using HTP.Components;
using FrameWorkEnation.Components;


namespace HTP.WebControls
{
    /// <summary>
    /// 
    /// </summary>
    public partial class UpdateNisiContactInfo : System.Web.UI.UserControl
    {
        #region Variables
        bool _freezecalender;

        /// <summary>
        /// 
        /// </summary>
        public event PageMethodHandler PageMethod = null;
        clsLogger bugTracker = new clsLogger();
        clsENationWebComponents ClsDb = new clsENationWebComponents();

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public string Title
        {
            set
            {
                lbl_head.Text = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool Freezecalender
        {
            get { return _freezecalender; }
            set { _freezecalender = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public FollowUpType followUpType
        {
            set
            {
                hf_FollowUpType.Value = value.ToString();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool EnableHideDropDown
        {
            set
            {
                this.cal_followupdate.EnableHideDropDown = value;
            }
        }

        #endregion

        #region Events

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                btnsave.Attributes.Add("onClick", "return ClosePopUp('" + Disable.ClientID + "','" + pnl_control.ClientID + "');");
                lbtn_close.Attributes.Add("onClick", "return ClosePopUp('" + Disable.ClientID + "','" + pnl_control.ClientID + "');");
                pnl_control.Attributes.Add("style", "z-index: 3; display: none; position: absolute; background-color: transparent; left: 0px; top: 0px;");
                GetNisiContactTypes();
            }

            cal_followupdate.Visible = !DesignMode;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="firstname"></param>
        /// <param name="lastname"></param>
        /// <param name="underlyingTicketnumber"></param>
        /// <param name="ticketid"></param>
        /// <param name="nisiCause"></param>
        /// <param name="comments"></param>
        /// <param name="panelId"></param>
        /// <param name="followUpDate"></param>
        /// <param name="contactTypeId"></param>
        /// <param name="nisiGeneratedDate"></param>
        /// <param name="employeeId"></param>
        public void CallPopUp(string firstname, string lastname, string underlyingTicketnumber, string ticketid, string nisiCause, string comments, string panelId, string followUpDate, string contactTypeId, string nisiGeneratedDate, int employeeId, bool isFreezed, string multiCause, string multiUnderlying)
        {
            if (_freezecalender)
            {
                ViewState["freestate"] = _freezecalender;
                if (tb_GeneralComments.Text != string.Empty)
                {
                    tb_GeneralComments.Text = "";
                }
                cal_followupdateDisabled.Enabled = false;
                cal_followupdateDisabled.SelectedDate = !(followUpDate.Equals("N/A")) ? Convert.ToDateTime(followUpDate).Date : DateTime.Now.Date;

                //cal_followupdate.Enabled = true;
                HfModelPopupControlId.Value = panelId;
                lbl_FullName.Text = firstname + " " + lastname;
                lblTicketNumber.Text = underlyingTicketnumber;
                lblCurrentFollup.Text = followUpDate;
                hf_ticid.Value = underlyingTicketnumber;
                hf_ticketid.Value = ticketid;
                lblCauseNo.Text = nisiCause;
                HfMultiNisiCause.Value = multiCause;
                HfMultiUnderlying.Value = multiUnderlying;
                ChkNoFollowUpDate.Checked = isFreezed;
                if (isFreezed)
                {
                    disabledCalender.Style["display"] = "block";
                    enabledCalender.Style["display"] = "none";
                    HfAlreadyFreezed.Value = "1";
                }
                else
                {
                    disabledCalender.Style["display"] = "none";
                    enabledCalender.Style["display"] = "block";
                    HfAlreadyFreezed.Value = "0";
                }
                if (Convert.ToInt32(contactTypeId) != 0)
                {
                    try
                {
                    ddl_callback.SelectedValue = contactTypeId;
                }
                    catch (Exception ex)
                    {
                    bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                    }
                }
                HfPreviousContactTypeId.Value = contactTypeId;
                HfNisiGeneratedDate.Value = nisiGeneratedDate;
                hf_StatusAtStartup.Value = ddl_callback.SelectedValue;

                lblComments.Text = comments;
                cal_followupdate.SelectedDate = !(followUpDate.Equals("N/A")) ? Convert.ToDateTime(followUpDate).Date : DateTime.Now.Date;
                hf_FollowUpDateAtStartup.Value = cal_followupdate.SelectedDate.ToShortDateString();
                pnl_control.Style["display"] = "";
                pnl_control.Style["position"] = "";
                lbtn_close.Attributes.Remove("onClick");
                lbtn_close.Attributes.Add("onClick", "return closeModalPopup('" + panelId + "');");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnsave_Click(object sender, EventArgs e)
        {
            int ticketId = int.Parse(this.hf_ticketid.Value);
            clsCase cCase = new clsCase();
            clsSession cSession = new clsSession();
            clsLogger cLog = new clsLogger();
            cCase.TicketID = ticketId;
            int EmpId = int.Parse(cSession.GetCookie("sEmpID", this.Request));
            cCase.EmpID = EmpId;

            if(ChkNoFollowUpDate.Checked)
            {
                hf_FollowUpDateAtStartup.Value = "N/A";
                cal_followupdate.SelectedDate = Convert.ToDateTime("01/01/1900");
            }

            var freeState = (ViewState["freestate"] == null ? string.Empty : ViewState["freestate"].ToString());
            if (hf_FollowUpType.Value.Equals(FollowUpType.NisiFollowUpDate.ToString()))
            {
                var objUpdateFolloUp = new ClsUpdateFollowUpInfo();
                objUpdateFolloUp.UpdateNisiFollowUpDate(ticketId, EmpId, hf_FollowUpDateAtStartup.Value,
                                                    cal_followupdate.SelectedDate, tb_GeneralComments.Text, Convert.ToInt32(ddl_callback.SelectedValue), TxtNotes.Text, ChkNoFollowUpDate.Checked);
                TxtNotes.Text = "";

            }

            if (Convert.ToInt32(ddl_callback.SelectedValue) == 7)
            {
                Session["TicketId"] = ticketId.ToString();
                Session["ContactType"] = ddl_callback.SelectedValue;
                Session["EmployeeIdForLetters"] = EmpId.ToString();
                Session["NISICauseMulti"] = HfMultiNisiCause.Value;
                Session["UnderlyingCauseMulti"] = HfMultiUnderlying.Value;
                System.Web.UI.ScriptManager.RegisterStartupScript(this.upnl, upnl.GetType(), "newWindow", "window.open('../Reports/frmNisiLetters.aspx','','fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=no');;", true);
                //System.Web.UI.ScriptManager.RegisterStartupScript(upnl, upnl.GetType(), "opnewin", "window.open('../Reports/PreviewNisiLetters.aspx',''); void(0);", true);
            }
            if (PageMethod != null)
                PageMethod();
        }

        ///<summary>
        /// Used to Get the NISI Contact Types for the update case conrtorl
        ///</summary>
        public void GetNisiContactTypes()
        {
            try
            {
                DataTable dtStatus = ClsDb.Get_DT_BySPArr("USP_HTP_Get_NisiContactTypes");

                if (dtStatus.Rows.Count > 0)
                {
                    ddl_callback.Items.Clear();
                    foreach (DataRow dr in dtStatus.Rows)
                    {
                        ddl_callback.Items.Add(new ListItem(dr["Description"].ToString(), dr["ContactTypeid"].ToString()));
                    }
                    ddl_callback.Items.Insert(0, new ListItem("Choose", "-1"));
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        #endregion
    }
}
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;

namespace lntechNew.WebControls
{
    public partial class ReadNotes : System.Web.UI.UserControl
    {
        clsReadNotes notes = new clsReadNotes();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!this.DesignMode)
            {
                if (!IsPostBack)
                {
                    if ((Request.QueryString["caseNumber"] != null) || (Request.QueryString["casenumber"] != "") )
                    {
                        
                        //kazim 2730 2/7/2008 
                        //Parse the ticketid before it used.

                        int tid = 0;
                        Int32.TryParse(Convert.ToString(Request.QueryString["casenumber"]),out tid);
                                               
                        if (tid != 0)
                        {
                            notes.FillReadNotes(txtreadcomments, tid);
                        }
                    }
                }
            }

        }
    }
}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContactIDLookUp.ascx.cs"
    Inherits="HTP.WebControls.ContactIDLookUp" %>
<link href="../Styles.css" type="text/css" rel="Stylesheet" />

<script language="javascript" type="text/javascript">
 function closeModalPopup(popupid)
    {
        var modalPopupBehavior = $find(popupid);
        modalPopupBehavior.hide();
        return false;
    }
    function CheckOnOff()
    {
        
        var a = 0;
        var all = document.getElementsByTagName("input");
        for(i=0;i<all.length;i++)
            { 
                if(all[i].type=="radio" && all[i].id == "rbnContactID")
                    {
                        if(all[i].checked==true)
                        {
                            a = 1;
                        }                        
                   }
            }
        if(a==0)
        {
            alert("Please select a contact to associate.");
            return false;
        }
    }

</script>

<div id="pnl_CIDLookUp" runat="server">
    <table border="2" enableviewstate="true" style="border-color: navy; border-collapse: collapse;
        width: 542px">
        <tr>
            <td background="../Images/subhead_bg.gif" valign="bottom">
                <table border="0" width="100%">
                    <tr>
                        <td class="clssubhead" style="height: 24px">
                            <asp:Label ID="lbl_head" Text="CID Look Up Info" runat="server"></asp:Label>
                        </td>
                        <td align="right">
                            &nbsp;<asp:LinkButton ID="lbtn_close" runat="server">X</asp:LinkButton>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table cellpadding="1" cellspacing="0" border="1" style="width: 100%; border-collapse: collapse"
                    class="clsLeftPaddingTable">
                    <tr>
                        <td style="width: 100%">
                            <asp:Label ID="lbl_Message" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                            <asp:HiddenField ID="hdnTicketID" runat="server" />
                            <asp:HiddenField ID="hdnFirstName" runat="server" />
                            <asp:HiddenField ID="hdnLastName" runat="server" />
                            <asp:HiddenField ID="hdnDOB" runat="server" />
                            <asp:GridView ID="grdvContactLookUp" runat="server" CssClass="clsLeftPaddingTable"
                                Style="width: 100%" AutoGenerateColumns="False">
                                <Columns>
                                    <asp:TemplateField HeaderText=" ">
                                        <ItemTemplate>
                                            <%--<asp:RadioButton id="rbnContactID" GroupName="rdBox" runat="server" Text='<%# Eval("ContactID") %>' ></asp:RadioButton>--%>
                                            <input id="rbnContactID" name="rbnContactID" type="radio" value='<%# Eval("ContactID") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Last Name" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlnk_Name" runat="server" NavigateUrl='<%# "/ClientInfo/AssociatedMatters.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid") %>'
                                                Text='<%# DataBinder.Eval(Container, "DataItem.LastName") %>'></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="First Name" DataField="FirstName" HeaderStyle-HorizontalAlign="Left" />
                                    <asp:BoundField HeaderText="DOB" DataField="DOB" HeaderStyle-HorizontalAlign="Left" />
                                    <asp:BoundField HeaderText="DL" DataField="DLNumber" HeaderStyle-HorizontalAlign="Left" />
                                    <asp:TemplateField HeaderText="Address" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" ToolTip='<%# Bind("Address") %>' Text='<%#DataBinder.Eval(Container.DataItem,"Address1").ToString()%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnAssociate" runat="server" CssClass="clsbutton" Text="Associate"
                                Width="93px" OnClick="btnAssociate_Click" OnClientClick="return CheckOnOff();" />&nbsp;
                            <asp:Button ID="btnNewContact" runat="server" CssClass="clsbutton" Text="New Contact"
                                Width="93px" OnClick="btnNewContact_Click" />&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>

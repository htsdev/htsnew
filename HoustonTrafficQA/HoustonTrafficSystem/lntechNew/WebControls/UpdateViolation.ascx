<%@ Control Language="C#" AutoEventWireup="true" Codebehind="UpdateViolation.ascx.cs"
    Inherits="lntechNew.WebControls.UpdateViolation" %>
<link href="../Styles.css" type="text/css" rel="stylesheet" />
<%@ Register Assembly="System.Web.Extensions" Namespace="System.Web.UI" TagPrefix="aspnew" %>

<script src="../Scripts/Validationfx.js" type="text/javascript"></script>

<script src="../Scripts/Dates.js" type="text/javascript"></script>

<script type="text/javascript">

    // Close Popup Script
    
   
    function closepopup(ctrl1,ctrl2)
	    {	    
	         document.getElementById(ctrl1).style.display = 'none';
	         document.getElementById(ctrl2).style.display = 'none';	
	         restoreTab();
	         return false;
        }
    
    // Toggle Image and Drop Down List
    function DisplayToggleP(ctrl1,ctrl2,ctrlstyle)
		{	   
			
		    var temp =   document.getElementById(ctrl1).style.display;
			document.getElementById(ctrl1).style.display = document.getElementById(ctrl2).style.display;
		    document.getElementById(ctrl2).style.display = temp;
		    document.getElementById(ctrlstyle).value  =document.getElementById(ctrl1).style.display
		    
		    
		}
		
		function DisplayToggleP2(ctrl1,ctrl2,ctrlstyle,ddl_court,hf_oldstatus,noaction,pledout)
		{	   
			
		    var temp =   document.getElementById(ctrl1).style.display;
		    var currCourtLocation = document.getElementById(ddl_court).value;
		    document.getElementById(ctrl1).style.display = document.getElementById(ctrl2).style.display;
		    document.getElementById(ctrl2).style.display = temp;
		    document.getElementById(ctrlstyle).value  =document.getElementById(ctrl1).style.display;
			
		    
		    //Zeeshan Ahmed 3486 04/04/2008
		    var ddlStatus = document.getElementById(ctrl1);
		    //Get Old Status
		    var oldstatus = document.getElementById(hf_oldstatus).value;
		    var trMissedCourt = document.getElementById("trMissedCourt");
		    
		    //Zeeshan Ahmed 4163 06/02/2008 Show/Hide Missed Court Types 
            if ( (ddlStatus.style.display == "none" && oldstatus == "105") || (ddlStatus.style.display =="block"  && ddlStatus.value == 105 ))
                trMissedCourt.style.display = "block";
            else if ( (ddlStatus.style.display =="block"  && ddlStatus.value != 105) || (ddlStatus.style.display == "none" && oldstatus != "105") )
                trMissedCourt.style.display = "none";
          
            return true;
		}
    	
   	// Toggle Image and Drop Down List and Fill Violation List
    function DisplayTogglePNew(ctrl1,ctrl2,ctrlstyle,ddl_court ,lastcourtid)
	{
		    
		    
		    var temp =   document.getElementById(ctrl1).style.display;
			document.getElementById(ctrl1).style.display = document.getElementById(ctrl2).style.display;
		    document.getElementById(ctrl2).style.display = temp;
		    document.getElementById(ctrlstyle).value  =document.getElementById(ctrl1).style.display
		    
		      if ( temp=='block')
		        return false;
		        
		      var hasHCViolations = document.getElementById(lastcourtid).value;
		      var currCourtLocation = document.getElementById(ddl_court).value;
            
              if ( hasHCViolations == "1" && currCourtLocation !="3037" )
              return true;
              
              else if ( hasHCViolations == "0" && currCourtLocation == "3037")
              return true;
              
              else if (  currCourtLocation == "3047")
              return true;
              
              else
              return false;	      		      		        
		        
	}	

    // Add New Violation Code
    	function addnewviolation(updatecontrol)
		{
    	       	       	    
    	    var linkbuttonid  = document.getElementById("hf_lastrow").value ; 
		  
		    //For New Violation Set Value = 1 
		    // Clearing All PopUp Controls
	        document.getElementById(updatecontrol + "_lbl_pvdesc").style.display = 'none';
	        document.getElementById(updatecontrol + "_lbl_pstatus").style.display = 'none';
		    document.getElementById(updatecontrol + "_lbl_pvdesc").innerText = '';
	        document.getElementById(updatecontrol + "_lbl_pstatus").innerText = '';
		    document.getElementById(updatecontrol + "_ddl_pvdesc").style.display = 'block';
		    document.getElementById(updatecontrol + "_ddl_pstatus").style.display = 'block';
		    document.getElementById(updatecontrol + "_tb_causeno").value = "";
		    document.getElementById(updatecontrol + "_tb_pfineamount").value = "0";
		    document.getElementById(updatecontrol + "_tb_pbondamount").value = "0";
	        document.getElementById(updatecontrol + "_hf_violationid").value = "-1";
	        document.getElementById(updatecontrol + "_hf_ticketviolationid").value = "-1";
	        document.getElementById(updatecontrol + "_hf_ticketsviolationid").value = "0";
	        document.getElementById(updatecontrol + "_ddl_pvdesc").selectedIndex =  0;
            document.getElementById(updatecontrol + "_ddl_pcrtloc").style.display = "block";	
            document.getElementById(updatecontrol + "_ddl_pvlevel").value = 0; 	    
		    document.getElementById(updatecontrol + "_lbl_pcname").style.display = "none";	
		    showpopup(updatecontrol,linkbuttonid,1);
		}


	// Show Update Popup
	function showpopup(updatecontrol,linkbuttonid,isnew)
		{				
		 
		 	    
		 
		  //Zeeshan 3486 04/04/2008
		  //Reset Missed Court Control
		  document.getElementById("trMissedCourt").style.display = "none";
		  document.getElementById(updatecontrol + "_rbtnNoAction").checked = false;
		  document.getElementById(updatecontrol + "_rbtnPledOut").checked = false;
		  	   
		  
		  //Zeeshan Ahmed 3925 05/01/2008
		   document.getElementById(updatecontrol + "_hf_coveringFirmId").value = document.getElementById(linkbuttonid+"_hf_coveringFirmId").value;
		  //Zeeshan Ahmed 3535 04/08/2008
		  //FTA Modifications		
          document.getElementById(updatecontrol + "_hf_isFTAViolation").value = document.getElementById(linkbuttonid+"_hf_isFTAViolation").value;
	      document.getElementById(updatecontrol + "_hf_hasFTAViolations").value = document.getElementById(linkbuttonid+"_hf_hasFTAViolations").value;
		 
		  var causenumber =  document.getElementById(updatecontrol + "_tb_causeno");
		  var ticketnumber = document.getElementById(updatecontrol + "_tb_pticketno");
		 
		  causenumber.value =   document.getElementById(linkbuttonid + "_hf_causeno").value;
		  ticketnumber.value =   document.getElementById(linkbuttonid + "_hf_ticketNumber").value;
		 	
		   // Check The Court Is Criminal Court
		   var criminalcourt = document.getElementById(linkbuttonid + "_hf_iscriminalcourt").value;
		   document.getElementById(linkbuttonid + "_hf_iscriminalcourt").value = criminalcourt;
		   //a;
		   document.getElementById(updatecontrol + "_ddl_pvlevel").value = document.getElementById(linkbuttonid + "_hf_chargelevel").value;
		    document.getElementById(updatecontrol + "_hf_Level").value = document.getElementById(linkbuttonid + "_hf_chargelevel").value;
    		  				   
		   if ( criminalcourt == "1")
		   {
		       //Zeeshan Ahmed 3724 05/13/2008
               document.getElementById(updatecontrol +"_tr_bondamount").style.display="none";  
               document.getElementById(updatecontrol +"_tr_fineamount").style.display="none";
		       document.getElementById(updatecontrol +"_tr_chargelevel").style.display="block";  
		       document.getElementById(updatecontrol +"_tr_arrestdate").style.display="block";    
		       // If Cause Number Not Exist then fill it with the ticketnumber
		       if (causenumber.value=="")
		            causenumber.value = ticketnumber.value;   
		      
		   }
		   else
		   {
		       document.getElementById(updatecontrol +"_tr_bondamount").style.display="block";  
		       document.getElementById(updatecontrol +"_tr_fineamount").style.display="block";
		       document.getElementById(updatecontrol +"_tr_chargelevel").style.display="none";  
		       document.getElementById(updatecontrol +"_tr_arrestdate").style.display="none"; 
		   } 	
		 		 
		  // Displaying Court Date 
		  var date = document.getElementById(linkbuttonid + "_hf_courtdate").value;
		
				 		 
		  if ( date == "1/1/1900" || date == "01/01/1900")
		  {
              document.getElementById(updatecontrol + "_tb_pmonth").value =  "";
		      document.getElementById(updatecontrol + "_tb_pday").value =   "";
		      document.getElementById(updatecontrol + "_tb_pyear").value = "";
		  }
		  else
		  {	   		    
		       document.getElementById(updatecontrol + "_tb_pmonth").value =   date.substring(0, date.indexOf('/'));
		       document.getElementById(updatecontrol + "_tb_pday").value =   date.substring(date.indexOf('/')+1 , date.lastIndexOf('/'));
		       document.getElementById(updatecontrol + "_tb_pyear").value =   date.substring(date.lastIndexOf('/') + 1+2 ,date.length);
		       
		       
		       document.getElementById(updatecontrol + "_hf_day").value =   date.substring(date.indexOf('/')+1 , date.lastIndexOf('/'));
		       document.getElementById(updatecontrol + "_hf_month").value =  date.substring(0, date.indexOf('/'));
		       document.getElementById(updatecontrol + "_hf_year").value =  date.substring(date.lastIndexOf('/') + 1+2 ,date.length);
		       
		  }
		  
		  // Displaying Arrest Date 
		  var date = document.getElementById(linkbuttonid + "_hf_arrestdate").value;
						 		 
		  if ( date == "1/1/1900" || date == "01/01/1900")
		  {
              document.getElementById(updatecontrol + "_tb_arrmonth").value =  "";
		      document.getElementById(updatecontrol + "_tb_arrday").value =   "";
		      document.getElementById(updatecontrol + "_tb_arryear").value = "";
		  }
		  else
		  {	   		    
		       document.getElementById(updatecontrol + "_tb_arrmonth").value =   date.substring(0, date.indexOf('/'));
		       document.getElementById(updatecontrol + "_tb_arrday").value =   date.substring(date.indexOf('/')+1 , date.lastIndexOf('/'));
		       document.getElementById(updatecontrol + "_tb_arryear").value =   date.substring(date.lastIndexOf('/') + 1+2 ,date.length);
		  }		  
            		  		  
		  		  
		 document.getElementById(updatecontrol + "_tb_prm").value = document.getElementById(linkbuttonid + "_hf_crm").value;
		 document.getElementById(updatecontrol + "_hf_courtroomno").value = document.getElementById(linkbuttonid + "_hf_crm").value;
		 if (isnew != "1")
		 {
		    document.getElementById(updatecontrol + "_tb_pfineamount").value = document.getElementById(linkbuttonid+"_hf_fineamount").value;
		    document.getElementById(updatecontrol + "_tb_pbondamount").value = document.getElementById(linkbuttonid+"_hf_bondamount").value;
		 }
	     document.getElementById(updatecontrol + "_hf_violationid").value = document.getElementById(linkbuttonid+"_hf_violationid").value;
	     document.getElementById(updatecontrol + "_hf_ticketviolationid").value = document.getElementById(linkbuttonid+"_hf_ticketviolationid").value;
		 		 	 
		 // Display Court Time 
	     document.getElementById(updatecontrol + "_ddl_pcrttime").value =   document.getElementById(linkbuttonid + "_hf_ctime").value;
	     document.getElementById(updatecontrol + "_hf_courttime").value =   document.getElementById(linkbuttonid + "_hf_ctime").value;
		 document.getElementById(updatecontrol + "_lbl_pvdesc").innerText =   document.getElementById(linkbuttonid + "_hf_VDesc").value;
		 		 
		 document.getElementById(updatecontrol + "_ddl_pstatus").value = document.getElementById(linkbuttonid + "_hf_courtviolationstatusid").value;
		 document.getElementById(updatecontrol + "_hv_viostatus").value = document.getElementById(linkbuttonid + "_hf_courtviolationstatusid").value;
		 document.getElementById(updatecontrol + "_hf_status").value = document.getElementById(linkbuttonid + "_hf_courtviolationstatusid").value;
		 document.getElementById(updatecontrol + "_lbl_pstatus").innerText =   document.getElementById(linkbuttonid + "_hf_courtstatus").value;
          
         if (document.getElementById(updatecontrol + "_lbl_pstatus").innerText == "")
		 {
		    document.getElementById(updatecontrol + "_lbl_pstatus").innerText = "N/A";
		    document.getElementById(updatecontrol + "_hf_status").value = "0";
		    document.getElementById(updatecontrol + "_ddl_pstatus").selectedIndex = 0;
		    
		 }				 
        
				
		 // Filling Hidden Fields
		 document.getElementById(updatecontrol + "_hf_AutoStatusId").value =   document.getElementById(linkbuttonid + "_hf_courtviolationstatusid").value;
		 document.getElementById(updatecontrol + "_hf_AutoNo").value =   document.getElementById(linkbuttonid + "_hf_crm").value;
		 document.getElementById(updatecontrol + "_hf_AutoCourtDate").value =   document.getElementById(linkbuttonid + "_hf_courtdate").value;
		 document.getElementById(updatecontrol + "_hf_AutoTime").value =   document.getElementById(linkbuttonid + "_hf_ctime").value;
		 document.getElementById(updatecontrol + "_hf_ticketid").value =   document.getElementById(linkbuttonid + "_hf_ticketid").value;

		 //Zeeshan Ahmed 3757 04/17/2008
		 if (document.getElementById(linkbuttonid + "_hf_hasNOS") != null)
		    document.getElementById(updatecontrol + "_hf_hasNOS").value = document.getElementById(linkbuttonid + "_hf_hasNOS").value;
		 
		 document.getElementById(updatecontrol + "_hf_UpdateNOS").value = "0";
		 // Hiding Violation DropDownList   
		 document.getElementById(updatecontrol + "_ddl_pvdesc").style.display='none';
		 document.getElementById(updatecontrol + "_ddl_pstatus").style.display='none';
		 document.getElementById(updatecontrol + "_lbl_pvdesc").style.display='block';
		 document.getElementById(updatecontrol + "_lbl_pstatus").style.display='block';
		 document.getElementById(updatecontrol + "_hf_vstyle").value='none';
		 document.getElementById(updatecontrol + "_hf_sstyle").value='none';
		 
		 		  
	        var  bondflag = document.getElementById(linkbuttonid+"_hf_bondflag").value;
		   
		    if ( bondflag == "YES" || bondflag=="yes")
		       document.getElementById(updatecontrol + "_rbl_bondflag_0").checked= true;
		    else
		      document.getElementById(updatecontrol + "_rbl_bondflag_1").checked = true;
		    
		    //Selects Plan
		    var srch = document.getElementById(linkbuttonid + "_hf_priceplan").value;
		    
		    //Added By Zeeshan Ahmed On 1/25/2008
		    document.getElementById(updatecontrol + "_hf_planid").value = srch;
		    
		    var ddl = document.getElementById(updatecontrol + "_ddl_ppriceplan")
	        selectValues(ddl,srch);
	        
	        //Selects CourtName
	        var getPricePlan = false;
        
	        var srch = document.getElementById(linkbuttonid + "_hf_courtid").value;
	        var courtLoc = document.getElementById(updatecontrol + "_hf_courtloc");
            	        
	        //Added By Zeeshan Ahmed To Fixed Criminal Court Lock Issue
	        courtLoc.value = document.getElementById(linkbuttonid + "_hf_courtid").value;
	        
	        if(srch == "3047")
		   	{
		   	    
		   	   document.getElementById(updatecontrol +"_tr_fineamount").style.display="block";
		       document.getElementById(updatecontrol +"_tr_chargelevel").style.display="none"
		   	}
            
            //Added by Ozair For Task 2515 on 01/02/2008
            if(srch != "3001" && srch != "3002" && srch != "3003" && criminalcourt != "1")
            {
                document.getElementById(updatecontrol +"_tr_bondamount").style.display="block";
            }
            else
            {
               document.getElementById(updatecontrol +"_tr_bondamount").style.display="none";
            }
            //end
                        	       
		   	var hfCourtId= document.getElementById(updatecontrol + "_hf_courtid")
		    
		    if (hfCourtId.value != srch)
            getPricePlan = true;
		   	
		    
		    hfCourtId.value = srch;
		   			   	
		   	var ddl = document.getElementById(updatecontrol + "_ddl_pcrtloc");
	     
	        ddl.selectedIndex = 0 
		   	selectValues(ddl,srch);
		        
	        if (ddl.selectedIndex != 0)
	        {
		        document.getElementById(updatecontrol + "_ddl_pcrtloc").style.display = "block";		    
		        document.getElementById(updatecontrol + "_lbl_pcname").style.display = "none";		    
		    }
		    else
		    {
		         document.getElementById(updatecontrol + "_ddl_pcrtloc").style.display = "none";		    
		         document.getElementById(updatecontrol + "_lbl_pcname").style.display = "block";		
		         document.getElementById(updatecontrol + "_lbl_pcname").innerText = document.getElementById(linkbuttonid + "_hf_courtloc").value;
		         document.getElementById(updatecontrol + "_hf_pcname").innerText = document.getElementById(linkbuttonid + "_hf_courtloc").value;
		    }
	
	        //Select Violation
	        var srch = document.getElementById(linkbuttonid + "_hf_violationid").value;
	        
		   	var ddl = document.getElementById(updatecontrol + "_ddl_pvdesc")
		   	selectValues(ddl,srch);
	        document.getElementById(updatecontrol + "_hf_violationid").value = srch;
	        // Select Status
	    
	        if(document.getElementById(updatecontrol + "_ddl_pcrtloc").value =="3061" || document.getElementById(updatecontrol + "_hf_courtid").value =="3061")
		    {
		        document.getElementById(updatecontrol + "_tb_oscare_court").style.display = "block";
		        document.getElementById(updatecontrol + "_tb_oscare_court").value = document.getElementById(linkbuttonid + "_hf_OscareCourt").value;
		    }
		    else
		    {
		         document.getElementById(updatecontrol + "_tb_oscare_court").value ="";
		         document.getElementById(updatecontrol + "_tb_oscare_court").style.display = "none";		         
		    }
                    
            if(document.getElementById(updatecontrol + "_ddl_pcrtloc").value =="3037")
            {
                document.getElementById(updatecontrol + "_hf_violationcheck").value = 1;
                document.getElementById(updatecontrol +"_tr_cdi").style.display="block";
                document.getElementById(updatecontrol + "_ddl_cdi").value = document.getElementById(linkbuttonid + "_hf_cdi").value;
            }
	        else
	        {
	            document.getElementById(updatecontrol + "_hf_violationcheck").value = 0;
	            document.getElementById(updatecontrol +"_tr_cdi").style.display="none";
	            document.getElementById(updatecontrol + "_ddl_cdi").selectedIndex = 0;
	        }
	        	
	          //Zeeshan Ahmed 4163 06/02/2008 Set Missed Court Status Type.
		      var StatusId =  document.getElementById(linkbuttonid + "_hf_courtviolationstatusid").value;
		      
		      document.getElementById(updatecontrol + "_hf_missedcourttype").value = document.getElementById(linkbuttonid+"_hf_missedcourttype").value;
		      var MissedCourtType =document.getElementById(updatecontrol + "_hf_missedcourttype").value;
    		   
    		  if ( StatusId=="105")
    		    document.getElementById("trMissedCourt").style.display = "block";	
    		  	    		  	
		      if ( MissedCourtType == "1" && StatusId=="105")
		      {
		        document.getElementById(updatecontrol + "_rbtnNoAction").checked = true;
		        document.getElementById(updatecontrol + "_rbtnPledOut").checked = false;
		      }
		      else if (MissedCourtType == "2" && StatusId=="105")
		      {
		         document.getElementById(updatecontrol + "_rbtnNoAction").checked = false;
		         document.getElementById(updatecontrol + "_rbtnPledOut").checked = true;
		      }	
	        	        
	        
	        if ( isnew == "0")
	          document.getElementById(updatecontrol + "_hf_newviolation").value = 0;
	        else
             document.getElementById(updatecontrol + "_hf_newviolation").value = 1;            
             
  	        setpopuplocation(updatecontrol);  
            
	        disableTab("pnl_violation");
	        
	        
	        //Added By Zeeshan Ahmed
	        //Get Price Plan If Court Location Changes
	        if (getPricePlan) setTimeout('__doPostBack(\'UpdateViolation1$ddl_pcrtloc\',\'\')', 0);
	        
		    return false;    
		}
		
	// Set Popup Control Location
    function setpopuplocation(updatecontrol)
		{
	        var top  = 400;
	        var left = 400;
	        var height = document.body.offsetHeight;
	        var width  = document.body.offsetWidth
            
            // Setting Panel Location
	        if ( width > 1100 || width <= 1280) left = 575;
	        if ( width < 1100) left = 500;
    	    
		    // Setting popup display
	        document.getElementById("pnl_violation").style.top =top;
		    document.getElementById("pnl_violation").style.left =left;
    						
		    if (  document.body.offsetHeight > document.body.scrollHeight )
			    document.getElementById(updatecontrol + "_Disable").style.height = document.body.offsetHeight;
		    else
		    document.getElementById(updatecontrol + "_Disable").style.height = document.body.scrollHeight ;
             document.getElementById(updatecontrol + "_Disable").style.position = "absolute";    			
		    document.getElementById(updatecontrol + "_Disable").style.width = document.body.offsetWidth;
		    document.getElementById(updatecontrol + "_Disable").style.display = 'block'
		    document.getElementById("pnl_violation").style.display = 'block'
		}

   // Update Violation Validation
    function submitPopup(updatecontrol,gridname,divname,ctrlstyl)
		{

		
		    if (document.getElementById(updatecontrol + "_ddl_pcrtloc").style.display != "none")
			{
			        if ( document.getElementById(updatecontrol + "_ddl_pcrtloc").selectedIndex == 0 )
			        {
			            alert("Please select court location.");
			            document.getElementById(updatecontrol + "_ddl_pcrtloc").focus();
			            return false;
        			
			        }
			        if(document.getElementById(updatecontrol + "_ddl_pcrtloc").value =="3061")
			        {
			            if(document.getElementById(updatecontrol + "_tb_oscare_court").value =="")
			            {
			                alert("Please enter Oscare Court Detail.");
			                document.getElementById(updatecontrol + "_tb_oscare_court").focus();
			                return false;
			            }
			        }
			       document.getElementById(updatecontrol + "_hf_courtid").value =document.getElementById(updatecontrol + "_ddl_pcrtloc").value;
			
			}
		    
		    
		    //Validating Ticket Number
			var casenumber = document.getElementById(updatecontrol + "_tb_pticketno");
			var refcasenumber = casenumber.value;
			
			if (refcasenumber == "" ) 
			{
				alert("Invalid Ticket Number.");
				casenumber.focus();
				return false;
			}
			if (refcasenumber.length<5)
			{
				alert("Ticket Number must be greater then four characters.");
				casenumber.focus();
				return false;
			}
			if (casenumber.value.indexOf(" ") != -1 ) 
			{
				alert("Please make sure that there are no spaces in the ticketnumber input box"); 
				casenumber.focus();
				return false;
			}
		    
		    // Validating Cause Number
		    var courtnum = document.getElementById(updatecontrol + "_tb_prm").value ;
		   
		    if ( courtnum.length > 1)
		     {
		       if ( courtnum.substring(0,1) == "0")
		       {
		          courtnum = courtnum.substring(1);
		       }
		    }
		    
		    //Zeeshan Ahmed 4173 06/10/2008 Fixed Missed Court Status Bug		    
			var datetype =(document.getElementById(updatecontrol + "_ddl_pstatus").style.display =="block")?document.getElementById(updatecontrol + "_ddl_pstatus").value: document.getElementById( updatecontrol +"_hf_status").value; 
			selectedindex = document.getElementById(updatecontrol + "_ddl_pstatus").selectedIndex;
			var strdatetype =document.getElementById(updatecontrol + "_ddl_pstatus").value;
			var varhours = document.getElementById(updatecontrol + "_ddl_pcrttime").value;
			//ozair 3901 on 04/30/2008 included the trim method here to avoid bug related to 3825
			var strdate = trim(document.getElementById(updatecontrol + "_tb_pmonth").value) + "/" + trim(document.getElementById(updatecontrol + "_tb_pday").value) + "/20" + trim(document.getElementById(updatecontrol + "_tb_pyear").value);
			var strdatewithtime = strdate + " " + varhours;
			var courtstatusauto = document.getElementById(updatecontrol + "_hf_AutoStatusID").value;
			var courtnumauto =document.getElementById(updatecontrol + "_hf_AutoNo").value;
			var courtdateauto = document.getElementById(updatecontrol + "_hf_AutoCourtDate").value + " " + document.getElementById(updatecontrol + "_hf_AutoTime").value;
			//Zeeshan Ahmed 3724 05/13/2008
			var ArrestDate = trim(document.getElementById(updatecontrol + "_tb_arrmonth").value) + "/" + trim(document.getElementById(updatecontrol + "_tb_arrday").value) + "/20" + trim(document.getElementById(updatecontrol + "_tb_arryear").value);
			//Violation Description Check
			
			var ddl_vdesc =document.getElementById(updatecontrol + "_ddl_pvdesc"); 
			var hf_violationid =document.getElementById(updatecontrol+"_hf_violationid");	
			var lbl_vdesc = document.getElementById(updatecontrol + "_lbl_pvdesc"); 
			
			if(ddl_vdesc.style.display != 'none')
			{			   
			    var ddltext=ddl_vdesc.options[ddl_vdesc.selectedIndex].text;
				if (ddl_vdesc.value==0)
				{
					alert("Please Select ViolationDescription.");
					ddl_vdesc.focus();
					return false;
				}
				if(ddltext.substring(0,3)== "---")
				{ 
				    alert("Please Select ViolationDescription.");
					ddl_vdesc.focus();
					return false;
				}
					   
			   hf_violationid.value = ddl_vdesc.value;
			    
			}
		    
		    if ( hf_violationid.value  == "0" ) 
		    {
		    
		    alert("Please Select ViolationDescription.");
			
			if ( ddl_vdesc.style.display == "none")
			{
                DisplayToggleP(ddl_vdesc.id,lbl_vdesc.id,ctrlstyl);
			    ddl_vdesc.focus();
			}
			else
			ddl_vdesc.focus();
			
			return false;
		    }
		    
		    //Zeeshan Ahmed 3535 04/08/2008
		    //FTA Modifications 
		    //Do Not Allow Multiple FTA In Case For Inside Courts
		    var selectedCourt = (document.getElementById(updatecontrol + "_ddl_pcrtloc").style.display != "none")?document.getElementById(updatecontrol + "_ddl_pcrtloc"):document.getElementById(updatecontrol + "_hf_courtid");						
		    if (selectedCourt.value =="3001" ||  selectedCourt.value =="3002" || selectedCourt.value =="3003")
		    {
		       var isNewViolation = (document.getElementById(updatecontrol + "_hf_newviolation").value =="1") ? true:false;
		       var hasFTAViolations = document.getElementById(updatecontrol + "_hf_hasFTAViolations").value;
		       var isFTAViolation = document.getElementById(updatecontrol + "_hf_isFTAViolation").value;
               
               //Do Not Allow New FTA Violation To Add If Case has already FTA violations Or
               //Do Not Allow Old Violation To FTA Case already have FTA violations             
               //Zeeshan Ahmed 3895 04/29/2008
               //if ( (isNewViolation && hasFTAViolations == "1" && hf_violationid.value == "11189") || (!isNewViolation && isFTAViolation=="0" && hasFTAViolations=="1" &&  hf_violationid.value == "11189" ))
               //{    
               //   alert("Case already has a FTA violaton. Please select different violation");
               //  
               //   if ( ddl_vdesc.style.display == "none")
               //   DisplayToggleP(ddl_vdesc.id,lbl_vdesc.id,ctrlstyl);
               // 
               //   ddl_vdesc.focus();
               //   return false;
		       //}
               //If Bond Flag Selected
		       var bond =document.getElementById(updatecontrol + "_rbl_bondflag_0");
		       
		       if (bond.checked)
		       {
		           if ( (isNewViolation && hasFTAViolations =="0" && hf_violationid.value != "11189") || (!isNewViolation && hasFTAViolations =="0" && isFTAViolation == "0" && hf_violationid.value != "11189" ))
		           {
		               alert("To change bond flag to Yes. Case must have a FTA ticket.");
		               bond.focus();
		               return false;
		           }
		       }
		    }
            
		    //Verify Cause NUmber for HCJP Courts
			var causenum=document.getElementById(updatecontrol + "_tb_causeno");
			if(parseInt(selectedCourt.value)>=3007 && parseInt(selectedCourt.value)<=3022)
			{
			  if ( causenum.value != "" )
			  {
			        if(causenum.value.length != 12)
			        {
			            alert('Please Enter Cause number for HCJP courts exactly 12 characters');
			            return(false);
			        }
    			    
			        if(causenum.value.substring(0,2).toUpperCase()  != 'TR' && causenum.value.substring(0,2).toUpperCase()  != 'BC' && causenum.value.substring(0,2).toUpperCase()  != 'CR')
			        {
			            alert('Please Enter Cause number for HCJP courts Beginning  with \'TR\',\'BC\' or \'CR\'');
			            return(false);
			        }
			   }
			}
		    
		    
		    // Check Charge Level 
		    
		  
		    
		    if (  document.getElementById(updatecontrol + "_tr_chargelevel").style.display != "none")
		    {//a;
		        var chargelevel =  document.getElementById(updatecontrol + "_ddl_pvlevel");
		        if ( chargelevel.value == "0")
		        {
		            alert("Please select charge level");
		            chargelevel.focus();
		            return false;
		        }
		        //added for task 2171
		        document.getElementById(updatecontrol + "_hf_Level").value=chargelevel.value;
		    }		    
            
            if (  document.getElementById(updatecontrol + "_tr_cdi").style.display != "none")
		    {
		        var cdi =  document.getElementById(updatecontrol + "_ddl_cdi");
		        if ( cdi.value == "0")
		        {
		            alert("Please select charge level");
		            cdi.focus();
		            return false;
		        }
		    }
            		    		    
		    //Fine AMount Check
			var fineamount =  document.getElementById(updatecontrol + "_tb_pfineamount");

			if ( document.getElementById(updatecontrol + "_tr_fineamount").style.display != "none")
			{
			    if (fineamount.value=="") 
			    {
				    alert("Please enter Fine Amount."); 
				    fineamount.focus();
				    return false;
			    }
			    if (fineamount.value.indexOf(" ") != -1) 
			    {
				    alert("Please make sure that there are no spaces in the Fine Amount input box"); 
				    fineamount.focus();
				    return false;
			    }
			    if(isNaN(fineamount.value))
			    {
				    alert("Invalid Fine Amount.");
				    fineamount.focus();
				    return false;
			    }
			}
			//added by ozair on 01/29/2008 [if fine amount text box is not visible then set 0]
			else
			{
				fineamount.value=0;
			}
			//Bond AMount Check
			
			var bondamount = document.getElementById(updatecontrol + "_tb_pbondamount");
			//Added/modified by Ozair for Task 2723 on 01/29/2008
			if ( document.getElementById(updatecontrol + "_tr_bondamount").style.display != "none")
			{
		        if (bondamount.value=="") 
			    {
				    alert("Please enter Bond Amount."); 
				    bondamount.focus();
				    return false;
			    }
    			
			    if(isNaN(bondamount.value))
			    {
				    alert("Invalid Bond Amount.");
				    bondamount.focus();
				    return false;
			    }
			}
			//if bond amount text box is not visible then set 0
			else
			{
			    bondamount.value=0;
			}
			//end task 2723
			var ValidChars = "-";
			var Char;
			var sText=fineamount.value;
			
			for (i = 0; i < sText.length; i++) 
			{ 
				
				Char = sText.charAt(i); 
				if (ValidChars.indexOf(Char) != -1) 
				{
					alert("Invalid fine amount");
				    fineamount.focus();
					return false;
				}
			}
			var sText=bondamount.value;
			for (i = 0; i < sText.length; i++) 
			{ 
				
				Char = sText.charAt(i); 
				if (ValidChars.indexOf(Char) != -1) 
				{
					alert("Invalid bond amount");
					bondamount.focus();
					return false;
				}
			}
		    //Validate Arrest Date
		    //Zeeshan Ahmed 3924 05/13/2008 
            if ( document.getElementById(updatecontrol + "_tr_arrestdate") != "none")
		    {
		        if ( ArrestDate != "//20")
		        {
		            var month = document.getElementById(updatecontrol + "_tb_arrmonth");
		            var day = document.getElementById(updatecontrol + "_tb_arrday");
		            var year = document.getElementById(updatecontrol + "_tb_arryear");
		        
		            if ( !validateCourtDate(month,day,year))
		            return false;
		            
		            if(!isvaliddate ("20"+ year.value, day.value, month.value ))
		            {
		                alert('Invalid arrest date entered.\nPlease enter date in MM/DD/YYYY format.');
		                month.focus();
		                return false;
		            }
		        }
		    }
		   		   	   
		    // Remaining Validation
		    var month = document.getElementById(updatecontrol + "_tb_pmonth");
		    var day = document.getElementById(updatecontrol + "_tb_pday");
		    var year = document.getElementById(updatecontrol + "_tb_pyear");
		    
		    
		     if (	 month.value =="" || day.value =="" || year.value == "")
		     {
		            alert("Please enter court date.");
		            month.focus();
		            return false;
		     }
		    
		    if (day.value =="0" )
		     {
		            alert("Please enter valid day.");
		            day.focus();
		            return false;
		     }
		      if (month.value =="0" )
		     {
		            alert("Please enter valid month.");
		            month.focus();
		            return false;
		     }
		   
		    if (year.value =="0" )
		     {
		            alert("Please enter valid year.");
		           year.focus();
		            return false;
		     }
		     
		    if (isNaN(month.value) )
		     {
		            alert("Please enter valid month.");
		            month.focus();
		            return false;
		     }
		        
		     if (  month.value > 12 )
		     {
		            alert("Please enter valid month.");
		           month.focus();
		            return false;
		     }
		     
		     if (isNaN(day.value))
             {
	                alert("Please enter valid day.");
	                day.focus();
	                return false;
             }		     
			    
	        if (isNaN(year.value))
	        {
                alert("Please enter valid year.");
                year.focus();
                return false;
	        }
		   		   
		    //Zeeshan Ahmed 3825 04/23/2008
		    //TRIM COURT DATE
		    month.value  = trim(month.value);
		    day.value = trim( day.value);
		    year.value = trim (year.value)
		    
			if( isvaliddate ("20"+ year.value, day.value, month.value ) == false )
		    {
		    alert('Invalid court date entered.\nPlease enter date in MM/DD/YYYY format.');
		    month.focus();
		    return false;
		    }
		    
		    //Status Check
		    
		    
		    var ddl_pstatus = document.getElementById( updatecontrol +"_ddl_pstatus");
		    var lbl_pstatus = document.getElementById( updatecontrol +"_lbl_pstatus");
		    var hf_status = document.getElementById( updatecontrol +"_hf_status");
					
			if(ddl_pstatus.style.display != 'none')
			{
			    //var ddltext=ddl_pstatus.options[ddl_pstatus.selectedIndex].text;
				if (ddl_pstatus.value==0 ||ddl_pstatus.value==-1 )
				{
					alert("Please Select Case Status.");
					ddl_pstatus.focus();
					return false;
				}
			  hf_status.value = ddl_pstatus.value;
			}
					
			if (hf_status.value=="0")
			{
				alert("Please Select Case Status.");
							
				if (ddl_pstatus.style.display == "none")
				{
				   DisplayToggleP(ddl_pstatus.id,lbl_pstatus.id,ctrlstyl);
				   ddl_pstatus.focus();
						
				}
				else
				{					
					ddl_pstatus.focus();
				}
						
				return false;
			}
				    
		    //Zeeshan 3486 04/04/2008
		     var NoAction = document.getElementById(updatecontrol + "_rbtnNoAction"); 
		     var PledOut =  document.getElementById(updatecontrol + "_rbtnPledOut");
		     		     
		     if ( document.getElementById("trMissedCourt").style.display != "none" )
		     {
		        if ( !NoAction.checked  && ! PledOut.checked)
		        {
		            alert("Please select missed court option");
		            NoAction.focus();
		            return false;
		        }  	 
		     }
		    
			if ( document.getElementById(updatecontrol + "_ddl_ppriceplan").selectedIndex == 0 )
			{
			    alert("Please select price plan.");
			    document.getElementById(updatecontrol + "_ddl_ppriceplan").focus();
			    return false;
			
			}
			
			// Added By Zeeshan Ahmed On 10th September 2007
			// If Status is Not Hire then does not perform other validations
			if ( ddl_pstatus.value == "239")
			{
			    document.getElementById(divname).style.display = "block";
		        document.getElementById(gridname).style.display = "none";
		        document.getElementById(divname).focus();
		        closepopup("pnl_violation",updatecontrol + "_Disable");
			    return true;						
			
			}
						
			// Checking Violation
			
			var oldcourtid = document.getElementById(updatecontrol + "_hf_courtid").value;
			var newcourtid = document.getElementById(updatecontrol + "_ddl_pcrtloc").value;
			
			if ( oldcourtid == "3037" && newcourtid != oldcourtid)
			{	
			     alert("Please Select Another Violation for the court.");
			     return false;
			   
			}
			
			var courtid = 3001 ;
			courtid =  document.getElementById(updatecontrol + "_hf_courtid").value;
			//Check For Court Date
			var bflag=0;
			if(document.getElementById(updatecontrol + "_rbl_bondflag_0").checked)
			{
				bflag = 1;
			}
			else
			{
				bflag = 0;
			}
			if (bflag ==0 && datetype != 104 && datetype != 105 && datetype != 135)
			{
			
				if (chkdate(strdate)==false)
				{
					alert("Invalid Court Date.");
					month.focus();
					return false;			
				}
			}
			
			var samedateflag = false;
			
			if (DateDiff(strdatewithtime, courtdateauto) == 0) 
			{
				samedateflag  = true;
			}
			
			
					// Sugar Land Court Validations
		if ( courtid == "3060")
		{
                if ( courtnum == "")
            	{
            	    document.getElementById(updatecontrol + "_tb_prm").value = "0";    	
            	    courtnum ="0";
            	}
                    
		    	if ( courtnum != "0" && courtnum !=null  )
		    	{
		    	    alert("Court number 0 can only be set for sugar land court.");
		    	    return false;
		    	}
		    	
		    	if (  (datetype=="66" ||  datetype=="77" ||  datetype=="85" ||  datetype=="101" )  && varhours !="1:30 PM" && varhours !="3:30 PM")
		    	{
		    	    alert("Pre Trial for sugar land court must be at 1:30 PM or 3:30 PM.");
		    	    return false;
		    	}
		    	
		}
			
			
			
			var doyou;
			if ((courtid == "3001") ||(courtid == "3002") || (courtid == "3003")) 
			{
				if (courtstatusauto == strdatetype) 
				{
					if ((samedateflag == false) || (parseInt(courtnumauto) != parseInt(courtnum))) 
					{
						doyou = confirm("The verified court status and auto load courts status are the same but have conflicting setting information.  Would you like to proceed? (OK = Yes   Cancel = No)");
						if (doyou == true) 
						{
						}
						else
						{
							return false;
						}
					}
				}

				if ((refcasenumber.substring(0,1) == "0" ) || (refcasenumber.substring(0,1) == "M") || (refcasenumber.substring(0,1) == "A")) 
				{
    
				}

		

		
		if(courtid == "3001")
		{ 
			var courtno = courtnum * 1;
			if ((courtno < 0) || (courtno > 17)||(courtno > 12 && courtno < 15))
			{
				alert("Please Enter 0 to 12 or 15 to 17 for Lubbock Court");
				return false;
			}
			
		}
		
				if((courtid == "3002")||(courtid)== "3003")
				{ 
					if ((datetype == "4") || (datetype == "24") || (datetype == "26") || (datetype == "28")) 
					{ 
						alert("Jury Trial is not a Valid Status");
						return false;
					}
				}
				//borntowin
				/*if(courtid == "3002")
				{ 
					if ( datetype == "103" ) 
					{ 
						alert("Judge Trial can only be set for HMC-L and HMC-D.");
						return false;
					}
				}*/

				if ((courtid == "3001") || (courtid == "3002") || (courtid == "3003")) 
				{
					if ((datetype == "4") || (datetype == "24") || (datetype == "26") || (datetype == "28")) 
					{
						if ((varhours != "8:00 AM") && (varhours != "9:00 AM") && (varhours != "10:30 AM") && (varhours != "10:00 AM")) 
						{
							alert("Jury Trial must be at one of the following times: 8am, 9am, 10am or 10:30am");
							document.getElementById(updatecontrol + "_ddl_pcrttime").focus();
							return false;
						}
						
						
						
						var bodflag=0;
						if(document.getElementById(updatecontrol + "_rbl_bondflag_0").checked)
						{
							bodflag = 1;
						}
						else
						{
							bodflag = 0;
						}
						if (bodflag ==0 && datetype != 104 && datetype != 105 && datetype != 135)
						{
							if ((courtnum != "6") && (courtnum != "1") && (courtnum != "2") && (courtnum != "3") && (courtnum != "8") && (courtnum != "11")  && (courtnum != "12")) 
							{
								alert("Jury Trial sets at this location must be in one of the following court rooms: 1,2,3,6,8,11,12");
								document.getElementById(updatecontrol + "_tb_prm").focus();
								return false;
							}
						}
					}
					//alert(datetype)
					if ((datetype == "66") || (datetype == "85") || (datetype == "101") || (datetype == "104")) 
					{
						alert("This court location cannot have a status of Waiting or Pre-Trial");
						
						
						if ( ddl_pstatus.style.display == "none")
						{
						    DisplayToggleP(ddl_pstatus.id,lbl_pstatus.id,ctrlstyl);
						    ddl_pstatus.focus();
						
						}
						else
						{					
						ddl_pstatus.focus();
						}
						return false;
					}
				}
				
				var bdflag=0;
				if(document.getElementById(updatecontrol +  "_rbl_bondflag_0").checked)
				{
					bdflag = 1;
				}
				else
				{
					bdflag = 0;
				}
				if (bdflag ==0 && datetype != 104 && datetype != 105 && datetype != 135)
				{
					if (courtid != "3003") 
					{
						if ((courtnum == "18") ) 
						{
							alert("This court number can only be set for Dairy Ashford Court");
							document.getElementById(updatecontrol + "_tb_prm").focus();
							return false;
						}
					}
					if (courtid == "3003") 
					{
						if ((courtnum != "18") ) 
						{
							alert("Court number 18 can only be set for Dairy Ashford Court");
							document.getElementById(updatecontrol + "_tb_prm").focus();
							return false;
						}
					}

					if (courtid != "3002") 
					{
						if ((courtnum == "13") || (courtnum == "14") ) 
						{
							alert("This court number can only be set for Mykawa");
							document.getElementById(updatecontrol + "_tb_prm").focus();
							return false;
						}
					}
					//borntowin
					if (courtid == "3002") 
					{
						if ((courtnum != "13") && (courtnum != "14") ) 
						{
							alert("Court number 13 or 14 can only be set for Mykawa.");
							document.getElementById(updatecontrol + "_tb_prm").focus();
							return false;
						}
					}
				}
			}	
			
			var rbl_bondflag = document.getElementById(updatecontrol +  "_rbl_bondflag_0");
			var strdate = month.value + "/" + day.value + "/20" + year.value; 
			var strtime = 	document.getElementById(updatecontrol + "_ddl_pcrttime").value;
			var bbflags=0;
			if(rbl_bondflag.checked)
			{
				bbflags = 1;
			}
			else
			{
				bbflags = 0;
			}
			if (bbflags ==0 && datetype != 104 && datetype != 105 && datetype != 135)
			{
				if (strtime == "--") 
				{
					alert("Invalid Court Time. ");
					document.getElementById(updatecontrol + "_ddl_pcrttime").focus();
					return false;
				}
				if (strtime == "11") 
				{
					if (datetype != 80 )
					{
						selectedindex1 = document.getElementById(updatecontrol + "_ddl_ViolationDescription").selectedIndex;
						var strviolationnew = trimAll(document.getElementById(updatecontrol + "_ddl_ViolationDescription").options[selectedindex1].text);
					
						if (trimAll(strviolationnew) != '------Default Out----')
						{
							alert("Invalid Court Time. ");
							document.getElementById(updatecontrol + "_ddl_pcrttime").focus();
							return false;
						}
					}
				}
			}
		
			//Check For Court No
			var bndflag=0;
			var datetype1 = ddl_pstatus.value;
			if(rbl_bondflag.checked)
			{
				bndflag = 1;
			}
			else
			{
				bndflag = 0;
			}
			if (bndflag ==0 && datetype1 != 104 && datetype1 != 105 && datetype1 != 135)
			{
				var courtnumber =document.getElementById(updatecontrol + "_tb_prm");
				if (courtnumber.value.indexOf(" ") != -1 ) 
				{
					alert("Please make sure that there are no spaces in the CourtNumber input box"); 
					courtnumber.focus();
					return false;
				}
				if (courtnumber.value=="") 
				{
					courtnumber.value=0;
					//alert("Please Enter Court Number.");
					//document.frmviolationfee.tb_prm.focus();
					//return false;
				}
/*				if (courtnumber=="0") 
				{
					alert("Invalid Court Number.");
					document.UpdateViolation.txt_CourtNo.focus();
					return false;
				}
*/				
				var ValidChars = "0123456789";
				var Char;
				for (j = 0; j < courtnumber.value.length; j++) 
				{ 
					Char = courtnumber.value.charAt(j); 
					if (ValidChars.indexOf(Char) == -1) 
					{
						alert("Invalid Court Number");
					    courtnumber.focus();
						return false;
					}
				}
			}
			
			
			if (strdate == "0/--/2005" )   
			{
				var bflags=0;
				if(rbl_bondflag.checked)
				{
					bflags = 1;
				}
				else
				{
					bflags = 0;
				}
				if (bflags ==0 && datetype != 104 && datetype != 105 && datetype != 135)
				{
					if (datetype != 80) 
					{
						selectedindex1 = ddl_vdesc.selectedIndex;
						var strviolationnew = trimAll(ddl_vdesc.options[selectedindex1].text);		
						if (trimAll(strviolationnew) != '------Default Out----')
						{
							alert("Invalid Court Date. ");
							document.getElementById(updatecontrol + "_tb_pmonth").focus();
							return false;
						}
					}
				}
			}
			else 
			{
				//alert(launchCheck(strdate))
				var fticketsflag1 ="";// frmviolationfee.txt_FTicketCount.value;
				//var bondflag = UpdateViolation.txt_BondFlag.value;
				var bondflag = 0;
				if(rbl_bondflag.checked)
				{
					bondflag = 1;
				}
				else
				{
					bondflag = 0;
				}
								
				      
				       
				if ((courtid == "3001") ||(courtid == "3002") || (courtid == "3003")) 
				{
					if (bondflag == 0) 
					{
						if (datetype != 80 && datetype != 104 && datetype != 105 && datetype != 135 && datetype != 123)  // Not Disposed
						{
							if (launchCheck(strdate) == false && document.getElementById(updatecontrol +  "_hf_newviolation").value != "1" && document.getElementById(updatecontrol + "_hf_allowolddate").value =="0" )  // if date is in the past
							{
								alert("Court Date has already passed. Please specify a future Date. ");
								month.focus();
								return false;
							}
						}
					//}
						//a;
						var bondflagT = 1;//frmviolationfee.txt_BondFlag.value;
/*						if(bondflagT == 0)
						{
							if ( fticketsflag1 == 0 & UpdateViolation.txt_IsNew.value=="0" & datetype != 135 & launchCheck(strdate) == false ) 
							{
								alert("Please Enter at least one Fticket to process this client. ");
								return false;
							}
						}
*/						
					}	
				}
				if (i != 3 ) 
				{
					var violationnumber;
					var fticketflag = false;
					if (launchCheck(strdate) == false ) 
					{
						//alert(strdate)
						for (var i=0; i<=ddl_vdesc.length-1;i++) 
						{
							violationnumber =ddl_vdesc[i].value;
							if ((violationnumber == "50") || (violationnumber == "448") || (violationnumber == "2821") || (violationnumber == "2822") || (violationnumber == "2824") || (violationnumber == "4337") || (violationnumber == "4999") || (violationnumber == "5103")) 
							{
								fticketflag = true;
								break;
							}
						}

						if (fticketflag == false)
						{
							if (datetype != 80) 
							{
								//alert("Court Date has already passed. Please specify a different Date or Click Insert Violation link. ");
								//return false;
							}
						}
					}
				}		
			}
		  
		   //Zeeshan Ahmed 3486 04/04/2008
		   if ( document.getElementById("trMissedCourt").style.display != "none" ) 
		   {
		        //Zeeshan Ahmed  4163 06/05/2008 If Missed Court Type Change Than Sent Letter To Batch
		        var CurrMissedCourtType = "-1"
		        var PrevMissedCourtType =  document.getElementById(updatecontrol + "_hf_missedcourttype").value;     
		           
		        
		        if (NoAction.checked)     
		            CurrMissedCourtType = "1";
		        if ( PledOut.checked)
		            CurrMissedCourtType = "2";
		       
		        if (CurrMissedCourtType != PrevMissedCourtType)
		        {		       
		           if ( NoAction.checked && bndflag == "1" )
		           {
		                if ( !confirm("Client already have Bond selected. Are you sure you want to update status to Missed Court - No Action. Press OK to Yes or Cancel for No."))
		                return false; 
		           }
		           else if ( PledOut.checked && bndflag == "0" )
		           {
		                if ( !confirm("Client don't have Bond selected. Are you sure you want to update status to Missed Court - Pled Out. Press OK to Yes or Cancel for No."))
		                return false; 
		           }
		        }    
		   }
		  
		   //Zeeshan Ahmed 3757 04/17/2008
		   //If HCJP Case
		   if(parseInt(selectedCourt.value)>=3007 && parseInt(selectedCourt.value)<=3022)
		   {		   
		       var casenumber = document.getElementById(updatecontrol + "_tb_causeno").value;
		       var hf_hasNOS = 	document.getElementById(updatecontrol + "_hf_hasNOS").value;
		       var hf_UpdateNOS = 	document.getElementById(updatecontrol + "_hf_UpdateNOS");
    		   	
		       //If Case Has No NOS Flag and Cause Number Empty	
		       if (casenumber == "" && hf_hasNOS == "0")
		       {
		            if ( confirm("This Ticket has no CauseNumber. Do you want to set the NOS flag on Contacts Page. Please press Ok for Yes or Cancel for No"))
		        	    hf_UpdateNOS.value = "1";
		            else
		        	    hf_UpdateNOS.value = "0";   
		       }
               else
                hf_UpdateNOS.value = "0"; 		   
           }
		  
		 //Zeeshan Ahmed 3825 04/23/2008
		 //If Not Criminal Court And Court Date Is Changed 
          var prevCourtDate =courtdateauto.substring(0,courtdateauto.indexOf(' '));  
         //Zeeshan Ahmed 3925 05/01/2008
          var CoveringFirmId = document.getElementById(updatecontrol + "_hf_coveringFirmId").value;
		  if ( !(courtid == 3036 || courtid == 3037 ||  courtid == 3043 || courtid == 3047 || courtid == 3048 || courtid == 3057 || courtid == 3058 ) && (strdate != prevCourtDate && prevCourtDate != "" && launchCheck(strdate) ) && CoveringFirmId != "3000")
		      alert("The coverage attorney for this case will be reset to Sullo & Sullo.");  
		  
		  
		document.getElementById(divname).style.display = "block";
		document.getElementById(gridname).style.display = "none";
		document.getElementById(divname).focus();
		  
		closepopup("pnl_violation",updatecontrol + "_Disable");
		return true;
		}
        
        
// **********Added By : Zeeshan Ahmed ************//
//   Added For Hiding and Showing Drop Down List  //
//************************************************//


    var  _backgroundElement = null;
    var _foregroundElement = null;
    var _saveTabIndexes = new Array();
    var _saveDesableSelect = new Array();
    var _tagWithTabIndex = new Array('A','BUTTON','TEXTAREA','INPUT','IFRAME');

function disableTab(panel)  {
                
                
        _tagWithTabIndex = new Array('A','BUTTON','TEXTAREA','INPUT','IFRAME');
              
        var i = 0;
        var tagElements;
        var tagElementsInPopUp = new Array();
        Array.clear(_saveTabIndexes);
        
         _foregroundElement = document.getElementById(panel);

       //Save all popup's tag in tagElementsInPopUp
        for (var j = 0; j < _tagWithTabIndex.length; j++) {
            tagElements = _foregroundElement.getElementsByTagName(_tagWithTabIndex[j]);
            for (var k = 0 ; k < tagElements.length; k++) {
                tagElementsInPopUp[i] = tagElements[k];
                i++;
            }
        }

        i = 0;
        for (var j = 0; j < _tagWithTabIndex.length; j++) {
            tagElements = document.getElementsByTagName(_tagWithTabIndex[j]);
            for (var k = 0 ; k < tagElements.length; k++) {
                if (Array.indexOf(tagElementsInPopUp, tagElements[k]) == -1)  {
                    _saveTabIndexes[i] = {tag: tagElements[k], index: tagElements[k].tabIndex};
                    tagElements[k].tabIndex="-1";
                    i++;
                }
            }
        }

        //IE6 Bug with SELECT element always showing up on top
       
        i = 0;
            //Save SELECT in PopUp
             var tagSelectInPopUp = new Array();
            for (var j = 0; j < _tagWithTabIndex.length; j++) 
            {
                tagElements = _foregroundElement.getElementsByTagName('SELECT');
                for (var k = 0 ; k < tagElements.length; k++) {
                    tagSelectInPopUp[i] = tagElements[k];
                    i++;
                }
            }

            i = 0;
            Array.clear(_saveDesableSelect);
            tagElements = document.getElementsByTagName('SELECT');
            for (var k = 0 ; k < tagElements.length; k++) 
              {
                if (Array.indexOf(tagSelectInPopUp, tagElements[k]) == -1)  
                {
                    _saveDesableSelect[i] = {tag: tagElements[k], visib: getCurrentStyle(tagElements[k], 'visibility')} ;
                    tagElements[k].style.visibility = 'hidden';
                    i++;
                }
             }
    }
           
    

   function restoreTab()
     {
        
        for (var i = 0; i < _saveTabIndexes.length; i++) {
            _saveTabIndexes[i].tag.tabIndex = _saveTabIndexes[i].index;
        }

        for (var k = 0 ; k < this._saveDesableSelect.length; k++) {
                _saveDesableSelect[k].tag.style.visibility = _saveDesableSelect[k].visib;
            
        }
    }


        function getCurrentStyle (element, attribute, defaultValue) {
    

        var currentValue = null;
        if (element) {
            if (element.currentStyle) {
                currentValue = element.currentStyle[attribute];
            } else if (document.defaultView && document.defaultView.getComputedStyle) {
                var style = document.defaultView.getComputedStyle(element, null);
                if (style) {
                    currentValue = style[attribute];
                }
            }
            
            if (!currentValue && element.style.getPropertyValue) {
                currentValue = element.style.getPropertyValue(attribute);
            }
            else if (!currentValue && element.style.getAttribute) {
                currentValue = element.style.getAttribute(attribute);
            }       
        }
        
        if ((!currentValue || currentValue == "" || typeof(currentValue) === 'undefined')) {
            if (typeof(defaultValue) != 'undefined') {
                currentValue = defaultValue;
            }
            else {
                currentValue = null;
            }
        }   
        return currentValue;  
    }

    		
		function DateDiff(date1, date2)
		{
			var objDate1=new Date(date1);
			var objDate2=new Date(date2);
			return (objDate1.getTime()-objDate2.getTime())/1000;
		}
		
		
		function launchCheck(strdate) 
		{
			var today = new Date(); // today
			var date = new Date(strdate); // your launch date
			//alert(date.getTime())
			//alert(today.getTime())
			var oneday = 1000*60*60*24
			//alert(Math.ceil(today.getTime()-date.getTime())/oneday)
			if (Math.ceil(today.getTime()-date.getTime())/oneday >= 1 ) 
			//if ( date < today )
			{
				return false;
			}
			return true;
		}
		
	
	    function plzwait(hidepanelname  , showpanelname)
	    {
	        document.getElementById(showpanelname).style.display = "block";
		    document.getElementById(hidepanelname).style.display = "none";
		    document.getElementById(showpanelname).focus();
	    }
	    
	   	
		
		function selectValues(ddl ,srch)
		{	     
	                
	        for ( i =0 ; i < ddl.options.length ; i++)
		    {
		            var text = ddl.options[i].innerText;
		            var value = ddl.options[i].value;
		            
		            if ( srch == value)
		            {
		               ddl.options[i].selected = true;
		               break;
		            }
		    }
		}
						
        function saveStatus(control)
        {
            alert("hi");
        }
        
    function chargelevel(updatepanel,ctrl1,ctrl2,ctrlstyle)
    {
    

     var courtid = document.getElementById(updatepanel + "_ddl_pcrtloc").value;
     
     if(courtid== "3047")
     {
     
       document.getElementById(updatepanel+"_tr_chargelevel").style.display='none';
       document.getElementById(updatepanel+"_tr_fineamount").style.display='block';
      
        document.getElementById(updatepanel+"_ddl_pvdesc").style.display='block';
        document.getElementById(updatepanel+"_lbl_pvdesc").style.display='none';
        document.getElementById(ctrlstyle).value =  document.getElementById(updatepanel+"_ddl_pvdesc").style.display;
        
          

     }
     
     
    }
        
   
//************************************************//                

//Zeeshan Ahmed 3486 04/03/2008
function checkMissedCourtStatus(status,hfstatus)
{
    document.getElementById("trMissedCourt").style.display=(status.value == "105")?"block":"none";
}

//Zeeshan Ahmed 3742 05/13/2008
function validateCourtDate (month,day,year)
{   
 		    
 		     if ( month.value =="" || day.value =="" || year.value == "")
		     {
		            alert("Please enter arrest date.");
		            month.focus();
		            return false;
		     }
 		    
		    if (day.value =="0" )
		     {
		            alert("Please enter valid day.");
		            day.focus();
		            return false;
		     }
		      if (month.value =="0" )
		     {
		            alert("Please enter valid month.");
		            month.focus();
		            return false;
		     }
		   
		    if (year.value =="0" )
		     {
		            alert("Please enter valid year.");
		           year.focus();
		            return false;
		     }
		     
		    if (isNaN(month.value) )
		     {
		            alert("Please enter valid month.");
		            month.focus();
		            return false;
		     }
		        
		     if (  month.value > 12 )
		     {
		            alert("Please enter valid month.");
		           month.focus();
		            return false;
		     }
		     
		     if (isNaN(day.value))
             {
	                alert("Please enter valid day.");
	                day.focus();
	                return false;
             }		     
			    
	        if (isNaN(year.value))
	        {
                alert("Please enter valid year.");
                year.focus();
                return false;
	        }
		   		   
		    //Zeeshan Ahmed 3825 04/23/2008
		    //TRIM COURT DATE
		    month.value  = trim(month.value);
		    day.value = trim( day.value);
		    year.value = trim (year.value)
		    return true;
}

</script>

<div runat="server" class="modalBackground" id="Disable" style="z-index: 0; display: none;
    position: absolute; left: 0; top: 0;">
</div>
<div id="pnl_violation" style="z-index: 3; display: none; position: absolute; background-color: transparent">
    <table border="2" enableviewstate="true" style="border-left-color: navy; border-bottom-color: navy;
        border-top-color: navy; border-collapse: collapse; border-right-color: navy">
        <tr>
            <td background="../Images/subhead_bg.gif" valign="bottom">
                <table border="0" width="350">
                    <tr>
                        <td class="clssubhead" style="height: 26px">
                            Update Violation Info</td>
                        <td align="right">
                            <asp:LinkButton ID="lbtn_close" runat="server">X</asp:LinkButton>
                            &nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <table border="1" cellpadding="0" cellspacing="0" class="clsLeftPaddingTable" style="border-top: 0px;
                    border-right-style: none; border-left-style: none; border-collapse: collapse;
                    border-bottom-style: none;" width="350">
                    <tr style="color: #000000; font-family: Tahoma">
                        <td class="clssubhead" style="width: 107px; height: 30px">
                            Court Location</td>
                        <td style="width: 161px;" valign="bottom" height="25px">
                            <aspnew:UpdatePanel ID="upnl_courts" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; display: inline;">
                                        <tr>
                                            <td valign="middle" style="height: 20px;">
                                                <asp:DropDownList ID="ddl_pcrtloc" runat="server" AutoPostBack="True" CssClass="clsInputCombo"
                                                    OnSelectedIndexChanged="ddl_pcrtloc_SelectedIndexChanged" Width="100px">
                                                </asp:DropDownList><asp:Label ID="lbl_pcname" runat="server" CssClass="Label" Style="display: none;"
                                                    Width="125px"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tb_oscare_court" runat="server" Width="100px" CssClass="clsInputadministration"></asp:TextBox></td>
                                        </tr>
                                    </table>
                                    <asp:HiddenField ID="hf_courtid" runat="server" Value="0" />
                                </ContentTemplate>
                            </aspnew:UpdatePanel>
                        </td>
                    </tr>
                    <tr style="color: #000000; font-family: Tahoma">
                        <td class="clssubhead" style="width: 107px; height: 20px">
                            Cause No</td>
                        <td style="width: 161px; height: 20px">
                            <asp:TextBox ID="tb_causeno" runat="server" CssClass="clsInputadministration" MaxLength="20"></asp:TextBox></td>
                    </tr>
                    <tr style="font-family: Tahoma">
                        <td class="clssubhead" style="width: 107px">
                            Ticket Number</td>
                        <td style="width: 161px">
                            <asp:TextBox ID="tb_pticketno" runat="server" CssClass="clsInputadministration" MaxLength="50"></asp:TextBox></td>
                    </tr>
                    <tr style="font-family: Tahoma">
                        <td class="clssubhead" style="width: 107px;">
                            Matter</td>
                        <td valign="middle" align="left" style="width: 161px">
                            <table>
                                <tr valign="middle">
                                    <td valign="middle">
                                        <aspnew:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                            <ContentTemplate>
                                                <table>
                                                    <tr>
                                                        <td valign="middle">
                                                            <asp:DropDownList ID="ddl_pvdesc" runat="server" CssClass="clsInputCombo" EnableViewState="true"
                                                                Style="display: none;" Width="150px">
                                                            </asp:DropDownList>
                                                            <asp:HiddenField ID="hf_vstyle" runat="server" Value="none" />
                                                            <asp:HiddenField ID="hf_lastcourtid" runat="server" />
                                                            <asp:HiddenField ID="hf_violationid" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                            <Triggers>
                                                <aspnew:AsyncPostBackTrigger ControlID="imgbtn_violation" EventName="Click" />
                                                <aspnew:AsyncPostBackTrigger ControlID="ddl_pcrtloc" EventName="SelectedIndexChanged" />
                                                <aspnew:AsyncPostBackTrigger ControlID="btn_popup" EventName="Click" />
                                            </Triggers>
                                        </aspnew:UpdatePanel>
                                    </td>
                                    <td>
                                        <asp:Label ID="lbl_pvdesc" runat="server" CssClass="Label" EnableViewState="true"
                                            Width="150"></asp:Label></td>
                                    <td>
                                        <img id="img_violation" runat="server" alt="" src="../Images/Rightarrow.gif" visible="false" />
                                        <aspnew:UpdatePanel ID="UpdatePanel3" runat="server">
                                            <ContentTemplate>
                                                <asp:ImageButton ID="imgbtn_violation" runat="server" ImageUrl="../Images/Rightarrow.gif"
                                                    OnClick="imgbtn_Click" />
                                            </ContentTemplate>
                                        </aspnew:UpdatePanel>
                                    </td>
                                </tr>
                                <tr valign="middle">
                                    <td valign="middle">
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <aspnew:UpdatePanel ID="upnl_chargeamount" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table border="1" cellpadding="0" cellspacing="0" class="clsLeftPaddingTable" style="border-collapse: collapse;
                            border-top-style: none; border-bottom-style: none;" width="350">
                            <tr id="tr_chargelevel" runat="server">
                                <td class="clssubhead" style="width: 91px; height: 30px;">
                                    Level</td>
                                <td style="width: 162px; height: 30px;">
                                    <asp:DropDownList ID="ddl_pvlevel" runat="server" CssClass="clsInputCombo" Width="88px">
                                    </asp:DropDownList><asp:HiddenField ID="hf_Level" runat="server" Value="0" />
                                </td>
                            </tr>
                            <tr id="tr_cdi" runat="server">
                                <td class="clssubhead" style="width: 91px; height: 30px">
                                    CDI</td>
                                <td style="width: 162px; height: 30px">
                                    <asp:DropDownList ID="ddl_cdi" runat="server" CssClass="clsInputCombo" Width="112px">
                                        <asp:ListItem Value="0">--Choose--</asp:ListItem>
                                        <asp:ListItem Value="002">Misdemeanor (002)</asp:ListItem>
                                        <asp:ListItem Value="003">Felony (003)</asp:ListItem>
                                    </asp:DropDownList></td>
                            </tr>
                            <tr id="tr_fineamount" runat="server">
                                <td id="tr_fineamount1" class="clssubhead" style="width: 91px; height: 30px">
                                    Fine Amount</td>
                                <td id="tr_fineamount2" style="width: 162px; height: 30px;">
                                    <asp:TextBox ID="tb_pfineamount" runat="server" CssClass="clsInputadministration"></asp:TextBox>
                                    <asp:HiddenField ID="hf_iscriminalcourt" runat="server" Value="0" />
                                </td>
                            </tr>
                            <tr id="tr_bondamount" style="font-family: Tahoma" runat="server">
                                <td id="tr_bondamount1" class="clssubhead" style="width: 91px; height: 25px;">
                                    Bond Amount</td>
                                <td id="tr_bondamount2" style="width: 161px; height: 20px;">
                                    <asp:TextBox ID="tb_pbondamount" runat="server" CssClass="clsInputadministration"></asp:TextBox>
                                </td>
                            </tr>
                            <tr id="tr_arrestdate" runat="server" style="font-family: Tahoma">
                                <td class="clssubhead" style="width: 91px; height: 25px">
                                    Arrest Date</td>
                                <td style="width: 161px; height: 20px">
                                    <asp:TextBox ID="tb_arrmonth" runat="server" CssClass="clsInputadministration" MaxLength="2"
                                        onkeyup="return autoTab(this, 2, event)" Width="26px"></asp:TextBox>
                                    /
                                    <asp:TextBox ID="tb_arrday" runat="server" CssClass="clsInputadministration" MaxLength="2"
                                        onkeyup="return autoTab(this, 2, event)" Width="28px"></asp:TextBox>
                                    /
                                    <asp:TextBox ID="tb_arryear" runat="server" CssClass="clsInputadministration" MaxLength="2"
                                        onkeyup="return autoTab(this, 2, event)" Width="32px"></asp:TextBox></td>
                            </tr>
                        </table>
                    </ContentTemplate>
                    <Triggers>
                        <aspnew:AsyncPostBackTrigger ControlID="ddl_pcrtloc" EventName="SelectedIndexChanged" />
                        <aspnew:AsyncPostBackTrigger ControlID="btn_popup" EventName="Click" />
                        <aspnew:AsyncPostBackTrigger ControlID="ddl_ppriceplan" EventName="Load" />
                    </Triggers>
                </aspnew:UpdatePanel>
                <table border="1" cellpadding="0" cellspacing="0" class="clsLeftPaddingTable" style="border-collapse: collapse;
                    border-bottom-style: none;" width="350">
                    <tr style="font-family: Tahoma; border-top-style: none;">
                        <td class="clssubhead" style="width: 92px; height: 30px">
                            Bond</td>
                        <td valign="middle" style="width: 161px">
                            <asp:RadioButtonList ID="rbl_bondflag" runat="server" CssClass="Label" RepeatDirection="Horizontal">
                                <asp:ListItem Value="1">&lt;span class=Label&gt;Yes&lt;span&gt;</asp:ListItem>
                                <asp:ListItem Value="0">&lt;span class=Label&gt;No&lt;/span&gt;</asp:ListItem>
                            </asp:RadioButtonList></td>
                    </tr>
                    <tr style="font-family: Tahoma">
                        <td class="clssubhead" style="width: 92px">
                            Court Date</td>
                        <td style="width: 161px">
                            <asp:TextBox ID="tb_pmonth" runat="server" CssClass="clsInputadministration" MaxLength="2"
                                onkeyup="return autoTab(this, 2, event)" Width="26px"></asp:TextBox>
                            /
                            <asp:TextBox ID="tb_pday" runat="server" CssClass="clsInputadministration" MaxLength="2"
                                onkeyup="return autoTab(this, 2, event)" Width="28px"></asp:TextBox>
                            /
                            <asp:TextBox ID="tb_pyear" runat="server" CssClass="clsInputadministration" MaxLength="2"
                                onkeyup="return autoTab(this, 2, event)" Width="32px"></asp:TextBox></td>
                    </tr>
                    <tr style="font-family: Tahoma">
                        <td class="clssubhead" style="width: 92px; height: 18px">
                            Court Time</td>
                        <td style="width: 161px; height: 18px">
                            <asp:DropDownList ID="ddl_pcrttime" runat="server" CssClass="clsInputCombo">
                                <asp:ListItem Value="8:00 AM">8:00 AM</asp:ListItem>
                                <asp:ListItem Value="8:15 AM">8:15 AM</asp:ListItem>
                                <asp:ListItem Value="8:30 AM">8:30 AM</asp:ListItem>
                                <asp:ListItem Value="8:45 AM">8:45 AM</asp:ListItem>
                                <asp:ListItem Value="9:00 AM">9:00 AM</asp:ListItem>
                                <asp:ListItem Value="9:15 AM">9:15 AM</asp:ListItem>
                                <asp:ListItem Value="9:30 AM">9:30 AM</asp:ListItem>
                                <asp:ListItem Value="9:45 AM">9:45 AM</asp:ListItem>
                                <asp:ListItem Value="10:00 AM">10:00 AM</asp:ListItem>
                                <asp:ListItem Value="10:15 AM">10:15 AM</asp:ListItem>
                                <asp:ListItem Value="10:30 AM">10:30 AM</asp:ListItem>
                                <asp:ListItem Value="10:45 AM">10:45 AM</asp:ListItem>
                                <asp:ListItem Value="11:00 AM">11:00 AM</asp:ListItem>
                                <asp:ListItem Value="11:15 AM">11:15 AM</asp:ListItem>
                                <asp:ListItem Value="11:30 AM">11:30 AM</asp:ListItem>
                                <asp:ListItem Value="11:45 AM">11:45 AM</asp:ListItem>
                                <asp:ListItem Value="12:00 PM">12:00 PM</asp:ListItem>
                                <asp:ListItem Value="12:15 PM">12:15 PM</asp:ListItem>
                                <asp:ListItem Value="12:30 PM">12:30 PM</asp:ListItem>
                                <asp:ListItem Value="12:45 PM">12:45 PM</asp:ListItem>
                                <asp:ListItem Value="1:00 PM">1:00 PM</asp:ListItem>
                                <asp:ListItem Value="1:15 PM">1:15 PM</asp:ListItem>
                                <asp:ListItem Value="1:30 PM">1:30 PM</asp:ListItem>
                                <asp:ListItem Value="1:45 PM">1:45 PM</asp:ListItem>
                                <asp:ListItem Value="2:00 PM">2:00 PM</asp:ListItem>
                                <asp:ListItem Value="2:15 PM">2:15 PM</asp:ListItem>
                                <asp:ListItem Value="2:30 PM">2:30 PM</asp:ListItem>
                                <asp:ListItem Value="2:45 PM">2:45 PM</asp:ListItem>
                                <asp:ListItem Value="3:00 PM">3:00 PM</asp:ListItem>
                                <asp:ListItem Value="3:15 PM">3:15 PM</asp:ListItem>
                                <asp:ListItem Value="3:30 PM">3:30 PM</asp:ListItem>
                                <asp:ListItem Value="3:45 PM">3:45 PM</asp:ListItem>
                                <asp:ListItem Value="4:00 PM">4:00 PM</asp:ListItem>
                                <asp:ListItem Value="4:15 PM">4:15 PM</asp:ListItem>
                                <asp:ListItem Value="4:30 PM">4:30 PM</asp:ListItem>
                                <asp:ListItem Value="4:45 PM">4:45 PM</asp:ListItem>
                                <asp:ListItem Value="5:00 PM">5:00 PM</asp:ListItem>
                                <asp:ListItem Value="5:15 PM">5:15 PM</asp:ListItem>
                                <asp:ListItem Value="5:30 PM">5:30 PM</asp:ListItem>
                                <asp:ListItem Value="5:45 PM">5:45 PM</asp:ListItem>
                                <asp:ListItem Value="6:00 PM">6:00 PM</asp:ListItem>
                                <asp:ListItem Value="6:15 PM">6:15 PM</asp:ListItem>
                                <asp:ListItem Value="6:30 PM">6:30 PM</asp:ListItem>
                                <asp:ListItem Value="6:45 PM">6:45 PM</asp:ListItem>
                                <asp:ListItem Value="7:00 PM">7:00 PM</asp:ListItem>
                                <asp:ListItem Value="7:15 PM">7:15 PM</asp:ListItem>
                                <asp:ListItem Value="7:30 PM">7:30 PM</asp:ListItem>
                                <asp:ListItem Value="7:45 PM">7:45 PM</asp:ListItem>
                                <asp:ListItem Value="8:00 PM">8:00 PM</asp:ListItem>
                                <asp:ListItem Value="8:15 PM">8:15 PM</asp:ListItem>
                                <asp:ListItem Value="8:30 PM">8:30 PM</asp:ListItem>
                                <asp:ListItem Value="8:45 PM">8:45 PM</asp:ListItem>
                                <asp:ListItem Value="9:00 PM">9:00 PM</asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>
                    <tr style="font-family: Tahoma">
                        <td class="clssubhead" style="width: 92px">
                            Court No</td>
                        <td style="width: 161px">
                            <asp:TextBox ID="tb_prm" runat="server" CssClass="clsInputadministration" Width="61px"></asp:TextBox></td>
                    </tr>
                    <tr style="font-family: Tahoma">
                        <td class="clssubhead" style="width: 92px">
                            Status</td>
                        <td valign="middle" style="width: 161px" align="left">
                            <table>
                                <tr>
                                    <td valign="middle">
                                        <aspnew:UpdatePanel ID="UpdatePanel2" runat="server">
                                            <ContentTemplate>
                                                <table>
                                                    <tr>
                                                        <td valign="middle" style="width: 198px">
                                                            <asp:DropDownList ID="ddl_pstatus" runat="server" CssClass="clsInputCombo" EnableViewState="true"
                                                                Width="100px">
                                                            </asp:DropDownList>
                                                            <asp:HiddenField ID="hf_sstyle" runat="server" Value="none" />
                                                            <asp:HiddenField ID="hf_violationcheck" runat="server" />
                                                            <asp:HiddenField ID="hf_status" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                            <Triggers>
                                                <aspnew:AsyncPostBackTrigger ControlID="ddl_pcrtloc" EventName="SelectedIndexChanged" />
                                                <aspnew:AsyncPostBackTrigger ControlID="imgbtn_violation" EventName="Click" />
                                                <aspnew:AsyncPostBackTrigger ControlID="ImgCrtLoc" EventName="Click" />
                                            </Triggers>
                                        </aspnew:UpdatePanel>
                                        <asp:Label ID="lbl_pstatus" runat="server" CssClass="Label" EnableViewState="true"></asp:Label>
                                    </td>
                                    <td align="right" style="width: 24px">
                                        <img id="img_status" runat="server" alt="" src="../Images/Rightarrow.gif" visible="false" />
                                    </td>
                                    <td align="right" style="width: 24px">
                                        <aspnew:UpdatePanel ID="UpdatePanel5" runat="server">
                                            <ContentTemplate>
                                                <asp:ImageButton ID="ImgCrtLoc" runat="server" ImageUrl="../Images/Rightarrow.gif"
                                                    OnClick="imgbtn_status" />&nbsp;
                                            </ContentTemplate>
                                        </aspnew:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr id="trMissedCourt1">
                        <td class="clssubhead" style="width: 92px">
                        </td>
                        <td align="left" style="width: 161px" valign="middle">
                            <aspnew:UpdatePanel ID="upnlMissedCourt" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tr id="trMissedCourt" style="display: none;">
                                            <td class="clssubhead" style="width: 92px">
                                                <asp:RadioButton ID="rbtnNoAction" runat="server" CssClass="Label" Text="No Action"
                                                    GroupName="grpmissedcourt" />&nbsp;</td>
                                            <td class="clssubhead" style="width: 92px; height: 20px;">
                                                <asp:RadioButton ID="rbtnPledOut" runat="server" CssClass="Label" Text="Pled Out"
                                                    GroupName="grpmissedcourt" /></td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </aspnew:UpdatePanel>
                        </td>
                    </tr>
                </table>
                <aspnew:UpdatePanel ID="UpdatePanel4" runat="server">
                    <ContentTemplate>
                        <table style="border-collapse: collapse" class="clsLeftPaddingTable" cellspacing="0"
                            cellpadding="0" width="350" border="1">
                            <tbody>
                                <tr style="font-family: Tahoma">
                                    <td style="width: 90px; height: 25px" class="clssubhead">
                                        Price Plan</td>
                                    <td style="width: 161px">
                                        <asp:DropDownList ID="ddl_ppriceplan" runat="server" Width="100px" CssClass="clsInputCombo">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr style="font-family: Tahoma">
                                    <td style="width: 90px; height: 30px" class="clssubhead">
                                        &nbsp;
                                        <asp:HiddenField ID="hf_hasNOS" runat="server" Value="0"></asp:HiddenField>
                                        <asp:HiddenField ID="hf_UpdateNOS" runat="server" Value="0"></asp:HiddenField>
                                    </td>
                                    <td style="width: 161px" class="transbox">
                                        <asp:Button ID="btn_popup" OnClick="btn_popup_Click" runat="server" CssClass="clsbutton"
                                            Text="Update"></asp:Button>
                                        <asp:CheckBox ID="cb_updateall" runat="server" CssClass="Label" Text="Update All"></asp:CheckBox></td>
                                </tr>
                            </tbody>
                        </table>
                    </ContentTemplate>
                    <Triggers>
                        <aspnew:AsyncPostBackTrigger ControlID="ddl_pcrtloc" EventName="SelectedIndexChanged" />
                        <aspnew:AsyncPostBackTrigger ControlID="btn_popup" EventName="Click" />
                    </Triggers>
                </aspnew:UpdatePanel>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hf_ticketviolationid" runat="server" />
    <asp:HiddenField ID="hf_newviolation" runat="server" Value="0" />
    <asp:HiddenField ID="hf_temp2" runat="server" Value="0" />
    <asp:HiddenField ID="hf_ticketsviolationid" runat="server" />
    <asp:HiddenField ID="hf_temp1" runat="server" Value="0" />
    <asp:HiddenField ID="hf_allowolddate" runat="server" Value="0" />
    <asp:HiddenField ID="hf_lastrow" runat="server" />
    <asp:HiddenField ID="hf_AutoStatusID" runat="server" />
    <asp:HiddenField ID="hf_AutoNo" runat="server" />
    <asp:HiddenField ID="pctrstatus" runat="server" />
    <asp:HiddenField ID="hf_AutoTime" runat="server" />
    <asp:HiddenField ID="hf_AutoCourtDate" runat="server" />
    <asp:HiddenField ID="hf_ticketid" runat="server" />
    <asp:HiddenField ID="hf_courtroomno" runat="server" />
    <asp:HiddenField ID="hv_viostatus" runat="server" />
    <asp:HiddenField ID="hf_courttime" runat="server" />
    <asp:HiddenField ID="hf_day" runat="server" />
    <asp:HiddenField ID="hf_Month" runat="server" />
    <asp:HiddenField ID="hf_year" runat="server" />
    <asp:HiddenField ID="hf_courtloc" runat="server" />
    <asp:HiddenField ID="hf_pcname" runat="server" />
    <asp:HiddenField ID="hf_planid" runat="server" />
    <asp:HiddenField ID="hf_isFTAViolation" runat="server" Value="0" />
    <asp:HiddenField ID="hf_hasFTAViolations" runat="server" Value="0" />
    <asp:HiddenField ID="hf_coveringFirmId" runat="server" Value="0" /><asp:HiddenField ID="hf_missedcourttype" runat="server" Value="-1" />
    &nbsp;&nbsp;
</div>

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using lntechNew.Components.ClientInfo;

using System.Threading;

namespace DTP.WebControls
{
    public partial class SMSControl : System.Web.UI.UserControl
    {
        ClsSMS Clssms = new ClsSMS();
        clsCase ClsCase = new clsCase();

        #region Declaration


        string _mobilephone1 = "";
        string _mobilephone2 = "";
        string _mobilephone3 = "";
        string _mobilephoneOther = "";
        string _smsmessage = "";
        string _clientfirstname = "";
        string _clientlastname = "";
        int _ticketid = 0;
        int _empid = 0;
        string _historymessage = "";
        string _modalpopupidtohide = "";
        bool Isphonenumberreadonly = true;
        bool Isothernumberreadonly = true;

        #endregion

        #region Properties

        public string ClientFirstName
        {
            get { return _clientfirstname; }
            set { _clientfirstname = value; }
        }

        public string ClientLastName
        {
            get { return _clientlastname; }
            set { _clientlastname = value; }
        }

        public int EmpID
        {
            get { return _empid; }
            set { _empid = value; }
        }

        public bool IsOtherNumberReadOnly
        {
            get { return Isothernumberreadonly; }
            set { Isothernumberreadonly = value; }
        }

        public bool IsPhoneNumberReadOnly
        {
            get { return Isphonenumberreadonly; }
            set { Isphonenumberreadonly = value; }
        }

        public string ModalPopupIDtoHide
        {
            get { return _modalpopupidtohide; }
            set { _modalpopupidtohide = value; }
        }

        public string HistoryNoteMessage
        {
            get { return _historymessage; }
            set { _historymessage = value; }
        }


        public string SmsMessageBody
        {
            get { return _smsmessage; }
            set { _smsmessage = value; }
        }

        public int TicketId
        {
            get { return _ticketid; }
            set { _ticketid = value; }
        }

        public string MobilePhone1
        {
            get { return _mobilephone1; }
            set { _mobilephone1 = value; }
        }

        public string MobilePhone2
        {
            get { return _mobilephone2; }
            set { _mobilephone2 = value; }
        }

        public string MobilePhone3
        {
            get { return _mobilephone3; }
            set { _mobilephone3 = value; }
        }

        public string MobilePhoneOther
        {
            get { return _mobilephoneOther; }
            set { _mobilephoneOther = value; }
        }



        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btn_sendsms_Click(object sender, EventArgs e)
        {
            try
            {
                string formatphonenumber = string.Empty;
                ((Label)this.FindControl("lbl_chk_SendSMS1")).Text = "";
                ((Image)this.FindControl("img_chk_SendSMS1")).Style["display"] = "none";
                ((Label)this.FindControl("lbl_chk_SendSMS2")).Text = "";
                ((Image)this.FindControl("img_chk_SendSMS2")).Style["display"] = "none";
                ((Label)this.FindControl("lbl_chk_SendSMS3")).Text = "";
                ((Image)this.FindControl("img_chk_SendSMS3")).Style["display"] = "none";
                ((Label)this.FindControl("lbl_chk_Other")).Text = "";
                ((Image)this.FindControl("img_chk_Other")).Style["display"] = "none";

                Clssms.UserName = Convert.ToString(ConfigurationManager.AppSettings["SmsServiceUserID"]);
                Clssms.Password = Convert.ToString(ConfigurationManager.AppSettings["SmsServicePassword"]);
                // Noufil 6177 08/16/2009 New property use
                Clssms.Api_id = Convert.ToString(ConfigurationManager.AppSettings["SmsServiceAPI_ID"]);
                string response = Clssms.IsvalidAccount();

                if (!response.Contains("Error"))
                {
                    if (Convert.ToString(ConfigurationManager.AppSettings["TestSmsKey"]).Trim() == "ON")
                    {
                        Clssms.PhoneNumber = Convert.ToString(ConfigurationManager.AppSettings["TestPhoneNumbertoSMS"]).Trim();
                        Clssms.MessageText = TxtMessage.Text;
                        if (Convert.ToString(ConfigurationManager.AppSettings["TestSmsSendKey"]).Trim() == "ON")
                            Clssms.SendSms(response);
                        lbl_chk_Other.Text = "SMS Successfully send to Test Mobile Number.";
                    }
                    else
                    {
                        string[] Checkboxname = new string[4];
                        Checkboxname[0] = "chk_SendSMS1";
                        Checkboxname[1] = "chk_SendSMS2";
                        Checkboxname[2] = "chk_SendSMS3";

                        string[] MobileNumber = new string[4];
                        MobileNumber[0] = TxtContact1_1.Text + TxtContact1_2.Text + TxtContact1_3.Text;
                        MobileNumber[1] = TxtContact2_1.Text + TxtContact2_2.Text + TxtContact2_3.Text;
                        MobileNumber[2] = TxtContact3_1.Text + TxtContact3_2.Text + TxtContact3_3.Text;


                        if (rb_existingContacts.Checked)
                        {
                            for (int i = 0; i < 3; i++)
                            {
                                if (((CheckBox)this.FindControl(Checkboxname[i])).Checked)
                                {
                                    Clssms.PhoneNumber = "1" + MobileNumber[i];
                                    // Noufil 6461 08/26/2009 Format phone number
                                    formatphonenumber = MobileNumber[i].Substring(0, 3) + "-" + MobileNumber[i].Substring(3, 3) + "-" + MobileNumber[i].Substring(6);
                                    Clssms.MessageText = TxtMessage.Text;

                                    try
                                    {

                                        if (Convert.ToString(ConfigurationManager.AppSettings["TestSmsSendKey"]).Trim() == "ON")
                                            Clssms.SendSms(response);
                                        ((Label)this.FindControl("lbl_" + Checkboxname[i])).Text = "SMS send successfully";
                                        ((Image)this.FindControl("img_" + Checkboxname[i])).ImageUrl = "~/Images/btnOK.ico";
                                        ((Image)this.FindControl("img_" + Checkboxname[i])).Style["display"] = "block";
                                        // Noufil 6461 08/26/2009 Format phone number use
                                        Clssms.AddNoteToHistory(Convert.ToInt32(ViewState["ticketid"]), Convert.ToString(ViewState["HistoryNoteMessage"]) + formatphonenumber, Convert.ToInt32(ViewState["empid"]));
                                        ClsCase.TicketID = Convert.ToInt32(ViewState["ticketid"]);
                                        DataTable dt = ClsCase.GetNearestFutureCourtDate();
                                        if (dt.Rows.Count > 0)
                                        {
                                            // Noufil 6461 08/26/2009 Format phone number use
                                            // Rab Nawaz Khan 10914 06/28/2013 Added emp id when sending the SMS for general comments and modifiying Manula text to not insert as emp resend info. . . 
                                            Clssms.InsertHistory(Convert.ToString(ViewState["firstname"]) + " " + Convert.ToString(ViewState["lastname"]), TxtMessage.Text.Replace("\n", "</br>"), Convert.ToInt32(ViewState["ticketid"]), Convert.ToDateTime(dt.Rows[0]["CourtDateMain"].ToString().Replace("@", "")), dt.Rows[0]["CourtNumbermain"].ToString(), formatphonenumber, "Manual Send", dt.Rows[0]["ticketnumber"].ToString(), ViewState["empid"].ToString());
                                        }
                                    }
                                    catch
                                    {
                                        ((Label)this.FindControl("lbl_" + Checkboxname[i])).Text = "Invalid Mobile Number";
                                        ((Image)this.FindControl("img_" + Checkboxname[i])).ImageUrl = "~/Images/cross.gif";
                                        ((Image)this.FindControl("img_" + Checkboxname[i])).Style["display"] = "block";
                                    }
                                }
                            }
                        }
                        else if (rb_other.Checked && chk_Other.Checked)
                        {
                            Clssms.PhoneNumber = "1" + txtOther1.Text + txtOther2.Text + txtOther3.Text;
                            // Noufil 6461 08/26/2009 Format phone number
                            formatphonenumber = txtOther1.Text + "-" + txtOther2.Text + "-" + txtOther3.Text;
                            Clssms.MessageText = TxtMessage.Text;


                            try
                            {
                                if (Convert.ToString(ConfigurationManager.AppSettings["TestSmsSendKey"]).Trim() == "ON")
                                    Clssms.SendSms(response);
                                lbl_chk_Other.Text = "SMS send successfully";
                                img_chk_Other.ImageUrl = "~/Images/btnOK.ico";
                                img_chk_Other.Style["display"] = "block";
                                // Noufil 6461 08/26/2009 Format phone number use
                                Clssms.AddNoteToHistory(Convert.ToInt32(ViewState["ticketid"]), Convert.ToString(ViewState["HistoryNoteMessage"]) + formatphonenumber, Convert.ToInt32(ViewState["empid"]));
                                ClsCase.TicketID = Convert.ToInt32(ViewState["ticketid"]);
                                DataTable dt = ClsCase.GetNearestFutureCourtDate();
                                if (dt.Rows.Count > 0)
                                {
                                    // Noufil 6461 08/26/2009 Format phone number use
                                    // Rab Nawaz Khan 10914 06/28/2013 Added emp id when sending the SMS for general comments and modifiying Manula text to not insert as emp resend info. . . 
                                    Clssms.InsertHistory(Convert.ToString(ViewState["firstname"]) + " " + Convert.ToString(ViewState["lastname"]), TxtMessage.Text.Replace("\n", "</br>"), Convert.ToInt32(ViewState["ticketid"]), Convert.ToDateTime(dt.Rows[0]["CourtDateMain"].ToString().Replace("@", "")), dt.Rows[0]["CourtNumbermain"].ToString(), formatphonenumber, "Manual Send", dt.Rows[0]["ticketnumber"].ToString(), ViewState["empid"].ToString());
                                }
                            }
                            catch
                            {
                                lbl_chk_Other.Text = "Invalid Mobile Number";
                                img_chk_Other.ImageUrl = "~/Images/cross.gif";
                                img_chk_Other.Style["display"] = "block";
                            }
                        }
                    }
                }
                else
                    lbl_SmsError.Text = "API Account not verified.";
            }

            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                lbl_SmsError.Text = ex.Message;
            }

            finally
            {
                ((AjaxControlToolkit.ModalPopupExtender)Page.FindControl("ModalPopupExtender_sms")).Show();
            }

        }

        protected void btn_cancel_Click(object sender, EventArgs e)
        {

        }

        #endregion

        #region Methods


        public void BindData()
        {
            try
            {
                //Sabir Khan 6293 08/05/2009 If there is no any mobile number available for the client...
                if (string.IsNullOrEmpty(_mobilephone1) && string.IsNullOrEmpty(_mobilephone2) && string.IsNullOrEmpty(_mobilephone3))
                {
                    lblMsg.Attributes.Add("style", "display:block;");
                    lblexisting.Visible = true;
                    lblexisting.Text = "No mobile number available.";
                    rb_other.Checked = true;
                }
                else
                {
                    rb_existingContacts.Checked = true;
                    lblexisting.Text = "";
                    lblexisting.Visible = false;
                    lblMsg.Attributes.Add("style", "display:none;");
                }
                if (!string.IsNullOrEmpty(_mobilephone1))
                {
                    //Sabir Khan 6293 08/05/2009 Text boxes of contact no 1 has been displayed...
                    trContact1.Attributes.Add("style", "display:block;");
                    if (_mobilephone1.Length >= 3)
                        TxtContact1_1.Text = _mobilephone1.Substring(0, 3);
                    if (_mobilephone1.Length >= 6)
                        TxtContact1_2.Text = _mobilephone1.Substring(3, 3);
                    if (_mobilephone1.Length >= 10)
                        TxtContact1_3.Text = _mobilephone1.Substring(6, 4);
                }
                else
                {
                    //Sabir Khan 6293 08/05/2009 Text boxes of contact no 1 has been set to invisible...
                    trContact1.Attributes.Add("style", "display:none;");
                }

                if (!string.IsNullOrEmpty(_mobilephone2))
                {
                    //Sabir Khan 6293 08/05/2009 Text boxes of contact no 2 has been displayed...
                    trContact2.Attributes.Add("style", "display:block;");
                    chk_SendSMS2.Visible = true;
                    if (_mobilephone2.Length >= 3)
                        TxtContact2_1.Text = _mobilephone2.Substring(0, 3);
                    if (_mobilephone2.Length >= 6)
                        TxtContact2_2.Text = _mobilephone2.Substring(3, 3);
                    if (_mobilephone2.Length >= 10)
                        TxtContact2_3.Text = _mobilephone2.Substring(6, 4);
                }
                else
                {
                    //Sabir Khan 6293 08/05/2009 Text boxes of contact no 2 has been set to invisible...
                    trContact2.Attributes.Add("style", "display:none;");
                }
                if (!string.IsNullOrEmpty(_mobilephone3))
                {
                    //Sabir Khan 6293 08/05/2009 Text boxes of contact no 3 has been displayed...
                    trContact3.Attributes.Add("style", "display:block;");
                    if (_mobilephone3.Length >= 3)
                        TxtContact3_1.Text = _mobilephone3.Substring(0, 3);
                    if (_mobilephone3.Length >= 6)
                        TxtContact3_2.Text = _mobilephone3.Substring(3, 3);
                    if (_mobilephone3.Length >= 10)
                        TxtContact3_3.Text = _mobilephone3.Substring(6, 4);
                }
                else
                {
                    //Sabir Khan 6293 08/05/2009 Text boxes of contact no 3 has been set to invisible...
                    trContact3.Attributes.Add("style", "display:none;");
                }

                if (!string.IsNullOrEmpty(_mobilephoneOther))
                {
                    if (_mobilephoneOther.Length >= 3)
                        txtOther1.Text = _mobilephoneOther.Substring(0, 3);
                    if (_mobilephoneOther.Length >= 6)
                        txtOther2.Text = _mobilephoneOther.Substring(3, 3);
                    if (_mobilephoneOther.Length >= 10)
                        txtOther3.Text = _mobilephoneOther.Substring(6, 4);
                }
                if (!string.IsNullOrEmpty(_mobilephoneOther))
                    rb_other.Checked = true;
                else
                    rb_existingContacts.Checked = true;

                if (Isphonenumberreadonly)
                {
                    TxtContact1_1.ReadOnly = true;
                    TxtContact1_2.ReadOnly = true;
                    TxtContact1_3.ReadOnly = true;
                    TxtContact2_1.ReadOnly = true;
                    TxtContact2_2.ReadOnly = true;
                    TxtContact2_3.ReadOnly = true;
                    TxtContact3_1.ReadOnly = true;
                    TxtContact3_2.ReadOnly = true;
                    TxtContact3_3.ReadOnly = true;
                }
                else if (!(Isphonenumberreadonly))
                {
                    TxtContact1_1.ReadOnly = false;
                    TxtContact1_2.ReadOnly = false;
                    TxtContact1_3.ReadOnly = false;
                    TxtContact2_1.ReadOnly = false;
                    TxtContact2_2.ReadOnly = false;
                    TxtContact2_3.ReadOnly = false;
                    TxtContact3_1.ReadOnly = false;
                    TxtContact3_2.ReadOnly = false;
                    TxtContact3_3.ReadOnly = false;

                }
                else if (Isothernumberreadonly)
                {
                    txtOther1.ReadOnly = true;
                    txtOther2.ReadOnly = true;
                    txtOther3.ReadOnly = true;
                }
                else if (!Isothernumberreadonly)
                {
                    txtOther1.ReadOnly = false;
                    txtOther2.ReadOnly = false;
                    txtOther3.ReadOnly = false;
                }

                TxtMessage.Text = _smsmessage;

                ViewState["HistoryNoteMessage"] = HistoryNoteMessage;
                ViewState["ticketid"] = TicketId;
                ViewState["SmsMessage"] = SmsMessageBody;
                ViewState["empid"] = EmpID;
                ViewState["firstname"] = _clientfirstname;
                ViewState["lastname"] = _clientlastname;

            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                lbl_SmsError.Text = ex.Message;
            }

        }


        #endregion






    }
}
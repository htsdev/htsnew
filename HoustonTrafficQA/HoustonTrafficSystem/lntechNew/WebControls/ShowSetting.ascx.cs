﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;
//Nasir 6968 112/18/2009 add namespace
using HTP.Components.Services;

namespace HTP.WebControls
{
    public partial class ShowSetting : System.Web.UI.UserControl
    {
        //Delegate & Event for Error Handling
        public delegate void ErrHandler(object sender, string ErrorMsg);
        public event ErrHandler OnErr;

        public delegate void Databind();
        public event Databind dbBind;

        clsENationWebComponents ClsDb = new clsENationWebComponents();
        protected string AttributeKey = "";
        protected int ReportID = 0;
        protected string lblTextBefore = "";
        protected string lblTextAfter = "";

        public string Attribute_Key
        {
            get
            {
                return AttributeKey;
            }
            set
            {
                AttributeKey = value;
            }
        }

        private int Report_ID
        {
            get
            {
                return ReportID;
            }
            set
            {
                ReportID = value;
            }
        }

        public string lbl_TextBefore
        {
            get
            {
                return lblTextBefore;
            }
            set
            {
                lblTextBefore = value;
            }
        }

        public string lbl_TextAfter
        {
            get
            {
                return lblTextAfter;
            }
            set
            {
                lblTextAfter = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Waqas 5653 03/31/2009 Checking for Negative value
                string url = Request.Url.AbsolutePath.ToString();
                // Noufil 5691 04/08/2009 Set number of business days greater than 0 for pastcourt for hmc and Non HMC report.
                if (url.ToLower().Trim().Contains("/reports/nolorreport.aspx") || url.ToLower().Trim().Contains("/reports/pastcourtdates.aspx")
                     || url.ToLower().Trim().Contains("/reports/pastcourtdatenonhmc.aspx"))
                {
                    //Fahad 5808 04/23/2009 new method added
                    btnUpdate.Attributes.Add("onClick", "return CheckNumber();");
                }

                if (Request.Cookies["sAccessType"] == null)
                {
                    //trday.Visible = false;
                    tdPrimary.Visible = false;
                }
                else
                {
                    if (Request.Cookies["sAccessType"].Value.ToString() == "2")
                    {

                        lblHeadingbefore.Text = lblTextBefore;
                        lblHeadingafter.Text = lblTextAfter;

                        //trday.Visible = true;
                        tdPrimary.Visible = true;
                        if (AttributeKey == "")
                        {
                            if (OnErr != null)
                            {
                                OnErr(tdPrimary, "All the properties of control should be set before use");
                            }
                        }
                        else
                        {
                            validateData();
                        }
                    }
                }
            }

        }
        /// <summary>
        /// Method for Validation
        /// </summary>
        protected void validateData()
        {
            string url = Request.Url.AbsolutePath.ToString();
            string[] key = { "@Report_Url", "@Attribute_key" };
            object[] value = { url, Attribute_Key };
            DataTable dtDays = new DataTable();
            try
            {
                dtDays = ClsDb.Get_DT_BySPArr("usp_htp_get_reportByUrl", key, value);

                if (dtDays.Rows.Count == 0)
                {
                    this.Visible = false;
                    if (OnErr != null)
                    {
                        OnErr(txtNumberofDays, "You cannot use this control because it is not specified in Reports table.");
                    }
                    return;
                }
                else
                {
                    Report_ID = Convert.ToInt32(dtDays.Rows[0]["Report_ID"]);
                    ViewState["Report_Id"] = Report_ID.ToString();
                    if (Convert.ToInt32(dtDays.Rows[0]["Report_Count"]) == 0)
                    {
                        if (OnErr != null)
                        {
                            OnErr(txtNumberofDays, "The report value with " + Attribute_Key + " Attribute key does not exists in Business Logic. Use show setting to set it for first time.");
                        }
                    }
                    bindData();
                }
            }
            catch (Exception ex)
            {
                if (OnErr != null)
                {
                    OnErr(ClsDb, ex.Message);
                }
            }
            finally
            {
                dtDays.Dispose();
                key = null;
                value = null;
                url = null;
            }
        }

        /// <summary>
        /// Method to Bind Data
        /// </summary>
        protected void bindData()
        {
            try
            {

                string[] key = { "@Attribute_Key", "@Report_Id" };
                object[] value = { AttributeKey, ViewState["Report_Id"].ToString() };

                DataTable dtDays = ClsDb.Get_DT_BySPArr("usp_hts_get_BusinessLogicDay", key, value);

                if (dtDays.Rows.Count == 0)
                {
                    txtNumberofDays.Text = "";

                }
                else
                {
                    txtNumberofDays.Text = dtDays.Rows[0]["Attribute_Value"].ToString();
                }
                //Nasir 6968 12/18/2009 save old value of number of days to save history
                hfNumberofDays.Value = txtNumberofDays.Text;
            }

            catch (Exception ex)
            {
                if (OnErr != null)
                { OnErr(txtNumberofDays, ex.Message); }
            }
        }

        /// <summary>
        /// To handle btnUpdate click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {

                    string[] key = { "@Attribute_Key", "@Attribute_Value", "@Report_Id" };
                    object[] value = { AttributeKey, int.Parse(txtNumberofDays.Text), ViewState["Report_Id"].ToString() };
                    ClsDb.InsertBySPArr("usp_hts_add_BusinessLogicDay", key, value);
                    //Nasir 6968 12/18/2009 save history
                    clsSession session = new clsSession();
                    ShowSettingHistoryService showSettingHistory = new ShowSettingHistoryService();
                    int EmployeeID = Convert.ToInt32(session.GetCookie("sEmpID", this.Request));
                    string Notes = lbl_TextBefore.Replace(":", "") + lbl_TextAfter + " changed From " + hfNumberofDays.Value + " to " + txtNumberofDays.Text + " " + AttributeKey;
                    int ReportID = !string.IsNullOrEmpty(Request.QueryString["sMenu"]) ? Convert.ToInt32(Request.QueryString["sMenu"]) : 0;
                    showSettingHistory.InsertShowSettingHistory(ReportID, Notes, EmployeeID);
                    bindData();
                    if (dbBind != null) { dbBind(); }

                }
            }
            catch (FormatException)
            {
                if (OnErr != null)
                {
                    OnErr(txtNumberofDays, "Please enter number of business days from 1 till 999");
                }
            }
            catch (InvalidCastException)
            {
                if (OnErr != null)
                { OnErr(txtNumberofDays, "Please enter number of business days from 1 till 999"); }

            }
            catch (Exception ex)
            {
                if (OnErr != null)
                {
                    OnErr(ClsDb, ex.Message);
                }

            }
        }

    }
}
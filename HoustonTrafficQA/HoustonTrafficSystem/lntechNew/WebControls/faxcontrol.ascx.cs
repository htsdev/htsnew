﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;
using System.IO;

namespace lntechNew.WebControls
{
    // Noufil 3999 06/12/2008 fax control which send fax
    public partial class faxcontrol : System.Web.UI.UserControl
    {
        #region Declaration
        clsSession uSession = new clsSession();
        // M.Asif 4491 09/24/08 start
        //Sabir Khan 9917 11/18/2011 Faxmaker DB has been removed.
        //clsENationWebComponents Cls_DAB = new clsENationWebComponents("FaxMaker");
        clsENationWebComponents Cls_DAB = new clsENationWebComponents("Connection String");
        // M.Asif 4491 09/24/08 end
        string Empname = string.Empty;
        string Courtfax = string.Empty;
        string Empemail = string.Empty;
        bool recall = false;
        #endregion

        #region Properties

        private string toAddress = string.Empty;
        private string body = string.Empty;
        private string attachment = string.Empty;
        private string subject = string.Empty;
        private int ticketid;
        private int empid;
        private bool fieldSet;
        //Nasir 6013 07/10/2009 fax id 
        private int faxid;

        public int Empid
        {
            get { return empid; }
            set { empid = value; }
        }

        public int Ticketid
        {
            get { return ticketid; }
            set { ticketid = value; }
        }

        public string Subject
        {
            get { return subject; }
            set { subject = value; }
        }

        public string Attachment
        {
            get { return attachment; }
            set { attachment = value; }
        }

        public string ToAddress
        {
            get { return toAddress; }
            set { toAddress = value; }
        }

        public string Body
        {
            get { return body; }
            set { body = value; }
        }

        public bool FieldSet
        {
            get { return fieldSet; }
            set { fieldSet = value; }
        }

        public bool Recall
        {
            get { return recall; }
            set { recall = value; }
        }

        //Nasir 6013 07/09/2009 New Property FaxID
        public int FaxID
        {
            get { return faxid; }
            set { faxid = value; }
        }
        

        #endregion

        #region events
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    Session["DS"] = null;
                    if (ticketid != 0 && empid != 0)
                    {
                        GetCaseInformation();
                    }

                }
                //Sabir Khan 6123 07/08/2009 Need to reload the dynamically created control...
                if (recall)
                {
                    AutofaxControl1.Recall();
                }
            }
            catch (Exception ex)
            {
                //lbl_message.Text = ex.Message;
                clsLogger.ErrorLog(ex);

            }
        }



        #endregion

        #region Methods

        //public void bindValues()
        //{
        //    txtto.Text = toAddress;
        //    Txtbody.Text = body;
        //    txtattachment.Text = System.IO.Path.GetFileName(attachment);
        //    txtsubject.Text = subject;
        //}

        public void GetCaseInformation()
        {
            if (ticketid != 0)
            {

                //Nasir 6013 07/10/2009 add parameter fax id 
                string[] key = { "@systemid", "@ticketid", "@FaxID" , "@empid" };
                object[] value1 = { FaxControl.Settings.ProgramSource.Houston, ticketid,FaxID, empid };
                //Sabir Khan 9917 11/18/2011 Procedure name has been changed.
                DataTable dt = Cls_DAB.Get_DT_BySPArr("USP_HTS_GetCaseinfo_FaxMaker", key, value1);
                
                if (dt.Rows.Count > 0)
                {


                    string RecipientName = dt.Rows[0]["Lastname"].ToString() + " " + dt.Rows[0]["firstname"].ToString();
                    string refno = dt.Rows[0]["refcasenumber"].ToString();
                    AutofaxControl1.SenderName = dt.Rows[0]["ELname"].ToString() + " " + dt.Rows[0]["EFname"].ToString();
                    //ozair 5046 10/27/2008 get sender email from config file
                    //AutofaxControl1.SenderEmail = dt.Rows[0]["email"] == DBNull.Value ? "hfax@sullolaw.com" : dt.Rows[0]["email"].ToString();
                    AutofaxControl1.SenderEmail = ConfigurationManager.AppSettings["AutoFaxSender"].ToString();
                    AutofaxControl1.ToAddress = dt.Rows[0]["FAXNO"].ToString();
                    AutofaxControl1.Subject = "Letter Of Rep for " + RecipientName + " (" + refno + ")";
                    AutofaxControl1.Attachment = this.attachment;
                    //Nasir 6013 07/09/2009 set body
                    if (FaxID != 0)
                    {                        
                        AutofaxControl1.Body = dt.Rows[0]["Body"].ToString();
                        AutofaxControl1.Faxid = FaxID;
                        AutofaxControl1.PendingNote.SecondText = "- Re-Sent to fax Number :";
                    }

                    AutofaxControl1.PendingNote.ThirdText = "For CourtHouse [" + dt.Rows[0]["ShortName"].ToString() + "]- Waiting confirmation";
                    AutofaxControl1.ConfirmNote.ThirdText = "For CourtHouse [" + dt.Rows[0]["ShortName"].ToString() + "]- Confirmed";
                    AutofaxControl1.DeclineNote.ThirdText = "For CourtHouse [" + dt.Rows[0]["ShortName"].ToString() + "]- Declined";
                    //ozair 5046 10/27/2008 hard coded employee id removed
                    AutofaxControl1.Empid = empid;
                    AutofaxControl1.SetHiddenField = fieldSet;
                    AutofaxControl1.Ticketid = ticketid;
                    if (recall)
                    {
                        AutofaxControl1.Recall();
                    }

                }











            }
        }

        #endregion
    }
}



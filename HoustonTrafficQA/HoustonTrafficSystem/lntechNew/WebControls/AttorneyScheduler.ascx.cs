﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using lntechNew.Components.ClientInfo;
using HTP.Components.Services;
using AjaxControlToolkit;


namespace HTP.WebControls
{
    public partial class AttorneyScheduler : System.Web.UI.UserControl
    {

        #region variables
        ShowSettingHistoryService ShowSettingService = new ShowSettingHistoryService();
        AttorneySchedulerService attorneySchedulerService = new AttorneySchedulerService();
        clsUser objclsuser = new clsUser();
        clsLogger BugTracker = new clsLogger();

        #endregion

        #region Properties

        public string ModalPopUpID { get { return Convert.ToString(ViewState["ModalPopUpID"]); } set { ViewState["ModalPopUpID"] = value; } }

        public string ModalPopUpLoadingID { get { return Convert.ToString(ViewState["ModalPopUpLoadingID"]); } set { ViewState["ModalPopUpLoadingID"] = value; } }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void rptShowSetting_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    GridView gv = (GridView)e.Item.FindControl("gvShowSetting");
                    DataRowView drv = (DataRowView)e.Item.DataItem;
                    Button btn = (Button)e.Item.FindControl("btn_update");
                    // HtmlTable tbl = (HtmlTable)e.Item.FindControl("tblProgressUpdatePage");
                    btn.Attributes.Add("OnClick", "return validateNumber(" + btn.ClientID + ");");
                    CollapsiblePanelExtender aj = (CollapsiblePanelExtender)e.Item.FindControl("ColPnlAttorneySetting");
                    aj.BehaviorID = aj.ClientID +"_Behaviour";
                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "opnewin", "var cpeMap = $find('" + aj.ClientID + "');cpeMap.add_expanded(cpeMapExpanded);", false);

                    BindGrid(gv, drv["StartDate"].ToString(), drv["EndDate"].ToString());                    
                }
            }
            catch (Exception ex)
            {

                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        protected void gvShowSetting_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DropDownList ddlAttorney = (DropDownList)e.Row.FindControl("ddlAttorney");
                    HiddenField hfAttorney = (HiddenField)e.Row.FindControl("hf_AttorneyID");
                    HiddenField hfContact = (HiddenField)e.Row.FindControl("hf_ContactNumber");
                    TextBox txtContact1 = (TextBox)e.Row.FindControl("txt_CC11");
                    TextBox txtContact2 = (TextBox)e.Row.FindControl("txt_CC12");
                    TextBox txtContact3 = (TextBox)e.Row.FindControl("txt_CC13");
                    TextBox txtContact4 = (TextBox)e.Row.FindControl("txt_CC14");





                    ddlAttorney.Items.Insert(0, new ListItem("-----Choose-----", "-1"));

                    //foreach (DataRow rows in dtAttorney.Rows)
                    //{
                    //    string[] str = rows["NameAndContact"].ToString().Split('-');
                    //    ddlAttorney.Items.Add(new ListItem(str[0].ToString(), rows["EmployeeID"].ToString()));



                    ddlAttorney.Attributes.Add("onchange", "SetContactNumber(" + ddlAttorney.ClientID + "," + txtContact1.ClientID + "," + txtContact2.ClientID + "," + txtContact3.ClientID + "," + txtContact4.ClientID + ")");

                    //    if (str[1].ToString() != "")
                    //    {
                    //        ddlAttorney.Attributes.Add("onchange", "SetContactNumber(" + str[1].ToString() + "," + txtContact1.ClientID + "," + txtContact2.ClientID + "," + txtContact3.ClientID + "," + txtContact4.ClientID + ")");
                    //    }
                    //    else
                    //    {
                    //        ddlAttorney.Attributes.Add("onchange", "SetContactNumber('Not'," + txtContact1.ClientID + "," + txtContact2.ClientID + "," + txtContact3.ClientID + "," + txtContact4.ClientID + ")");
                    //    }

                    //}



                    ddlAttorney.SelectedValue = hfAttorney.Value;

                    if (hfContact.Value != String.Empty)
                    {
                        if (hfContact.Value.Length >= 3)
                            txtContact1.Text = hfContact.Value.Substring(0, 3);
                        if (hfContact.Value.Length >= 6)
                            txtContact2.Text = hfContact.Value.Substring(3, 3);
                        if (hfContact.Value.Length >= 10)
                            txtContact3.Text = hfContact.Value.Substring(6, 4);
                        if (hfContact.Value.Length > 10)
                            txtContact4.Text = hfContact.Value.Substring(10);
                    }

                    //dd.Controls.Add("onchange=");

                }

            }
            catch (Exception ex)
            {

                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

            }

        }

        protected void btn_update_Click(object sender, EventArgs e)
        {
            try
            {
                clsSession session = new clsSession();
                int index = Convert.ToInt32(((System.Web.UI.WebControls.WebControl)(sender)).ClientID.Substring(38, 2));
                int EmployeeID = Convert.ToInt32(session.GetCookie("sEmpID", this.Request));
                GridView gv = (GridView)rptShowSetting.Items[index].FindControl("gvShowSetting");
                for (int i = 0; i < gv.Rows.Count; i++)
                {
                    HiddenField hfAttorneySchedule = (HiddenField)gv.Rows[i].FindControl("hf_AttorneyScheduleID");
                    DropDownList ddlAtorney = (DropDownList)gv.Rows[i].FindControl("ddlAttorney");
                    HiddenField hfAttorney = (HiddenField)gv.Rows[i].FindControl("hf_AttorneyID");
                    HiddenField hfContact = (HiddenField)gv.Rows[i].FindControl("hf_ContactNumber");
                    Label lblDocketDate = (Label)gv.Rows[i].FindControl("lblDateTime");
                    TextBox txtContact1 = (TextBox)gv.Rows[i].FindControl("txt_CC11");
                    TextBox txtContact2 = (TextBox)gv.Rows[i].FindControl("txt_CC12");
                    TextBox txtContact3 = (TextBox)gv.Rows[i].FindControl("txt_CC13");
                    TextBox txtContact4 = (TextBox)gv.Rows[i].FindControl("txt_CC14");
                    string ContactNumber = txtContact1.Text + txtContact2.Text + txtContact3.Text + txtContact4.Text;
                    bool IsAttorneyChange = (ddlAtorney.SelectedValue != hfAttorney.Value);
                    bool IsContactChange = (hfContact.Value != ContactNumber);
                    if (IsAttorneyChange || IsContactChange)
                    {
                        var OldAttorneyInfo = hfAttorney.Value.Split('-');
                        var NewAttorneyInfo = ddlAtorney.SelectedValue.Split('-');

                        if (hfAttorneySchedule.Value == "0")
                        {
                            hfAttorneySchedule.Value = attorneySchedulerService.InsertAttorneySchedule(lblDocketDate.Text, Convert.ToInt32(NewAttorneyInfo[0]), ContactNumber, Convert.ToInt32(EmployeeID));
                            hfAttorney.Value = ddlAtorney.SelectedValue;//NewAttorneyInfo[0] + '-' + hfContact.Value;
                            hfContact.Value = ContactNumber;
                        }
                        else if (ddlAtorney.SelectedValue == "-1")
                        {
                            attorneySchedulerService.DeleteAttorneySchedule(Convert.ToInt32(hfAttorneySchedule.Value));
                            ShowSettingService.InsertShowSettingHistory(Convert.ToInt32(Request.QueryString["sMenu"]), "For " + lblDocketDate.Text + " Attorney " + GetAttorneysName(OldAttorneyInfo[0]) + " has heen removed", EmployeeID);
                            hfAttorneySchedule.Value = "0";
                            hfAttorney.Value = "-1";
                            hfContact.Value = "";

                        }
                        else
                        {
                            attorneySchedulerService.UpdateAttorneySchedule(Convert.ToInt32(hfAttorneySchedule.Value), lblDocketDate.Text, Convert.ToInt32(NewAttorneyInfo[0]), ContactNumber, Convert.ToInt32(EmployeeID));
                            

                            if (IsAttorneyChange)
                            {
                                ShowSettingService.InsertShowSettingHistory(Convert.ToInt32(Request.QueryString["sMenu"]), "For " + lblDocketDate.Text + " Attorney name  change from " + GetAttorneysName(OldAttorneyInfo[0]) + " to " + ddlAtorney.SelectedItem.Text, EmployeeID);
                            }
                            if (IsContactChange)
                            {
                                ShowSettingService.InsertShowSettingHistory(Convert.ToInt32(Request.QueryString["sMenu"]), "For " + lblDocketDate.Text + " Attorney Contact Number  change from " + hfContact.Value + " to " + ContactNumber, EmployeeID);
                            }
                            hfAttorney.Value = ddlAtorney.SelectedValue;
                            hfContact.Value = ContactNumber;

                        }

                    }

                }

               
                if (!string.IsNullOrEmpty(ModalPopUpID))
                {
                    ((AjaxControlToolkit.ModalPopupExtender)Page.FindControl(ModalPopUpID)).Show();
                }

                if (!string.IsNullOrEmpty(ModalPopUpLoadingID))
                {
                    ((AjaxControlToolkit.ModalPopupExtender)Page.FindControl(ModalPopUpLoadingID)).Hide();
                }
            }
            catch (Exception ex)
            {

                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

            }

        }

        #endregion

        #region Methods

        /// <summary>
        /// Bind repeater
        /// </summary>
        public void BindRepeaterControl()
        {
            rptShowSetting.DataSource = attorneySchedulerService.GetShowSettingWeeks();
            rptShowSetting.DataBind();
            //Response.Write("<script type='text//javascript' language='javascript'>alert('test');OnExpand();</script>");
            

        }

        /// <summary>
        /// Bind grid view
        /// </summary>
        /// <param name="gv">gridview</param>
        /// <param name="StartDate">start date</param>
        /// <param name="EndDate">Enddate</param>
        private void BindGrid(GridView gv, string StartDate, string EndDate)
        {
            gv.DataSource = attorneySchedulerService.GetAttorneysWeeklySettings(StartDate, EndDate);
            gv.DataBind();

        }

        /// <summary>
        /// returns Attorneys name
        /// </summary>
        /// <returns>datatable</returns>
        public DataTable GetAttorneys()
        {
            return objclsuser.GetAttorneyList(1).Tables[0];
        }

        /// <summary>
        /// filter Attorneys name
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public string GetAttorneysName(string ID)
        {
            DataTable dt = GetAttorneys();
            DataView dv = dt.DefaultView;
            dv.RowFilter = "EmployeeID=" + ID;
            return dv[0][0].ToString();

        }

        #endregion
    }
}
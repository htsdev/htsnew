﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="faxcontrol.ascx.cs"
    Inherits="lntechNew.WebControls.faxcontrol" %>
<%@ Register assembly="FaxControl" namespace="FaxControl" tagprefix="cc1" %>
<link href="../Styles.css" rel="stylesheet" type="text/css" />
<div id="div1" runat="server" style="width: 100%; height: 326px;">
    <table>
        <tr>
            <td>
                <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline">
                    <ContentTemplate>
                        <cc1:AutofaxControl ID="AutofaxControl1" runat="server" 
                            WaitImage="~/Images/plzwait.gif" 
                            WaitMessage="Sending Fax Please wait ....">
                            <DeclineNote FirstText="Letter of Rep : Ref#" 
                                SecondText="- Decline from fax Number :" 
                                ThirdText="For CourtHouse [HCJP 4-1]- Waiting confirmation" />
                            <ConfirmNote FirstText="Letter of Rep : Ref#" 
                                SecondText="- Confirm to fax Number :" 
                                ThirdText="For CourtHouse [HCJP 4-1]- Waiting confirmation" />
                            <PendingNote FirstText="Letter of Rep : Ref#" 
                                SecondText="- Sent to fax Number :" 
                                ThirdText="For CourtHouse [HCJP 4-1]- Waiting confirmation" />
                        </cc1:AutofaxControl>
                    </ContentTemplate>
                </aspnew:UpdatePanel>
            </td>
        </tr>
    </table>
    <script type="text/javascript">
                                    var postbackElement;
                                    Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginRequest);
                                    Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(pageLoaded);
                                    
                                    function beginRequest(sender, args) {
                                        postbackElement = args.get_postBackElement();
                                    }
                                    function pageLoaded(sender, args) {
                                        
                                        
                                        if (document.getElementById("Faxcontrol1_AutofaxControl1_hf_showalert").value == "1")
                                        {   
                                            alert('Fax has been sent successfully.');                                 
                                            document.getElementById("Faxcontrol1_AutofaxControl1_hf_showalert").value = "0";
                                        }
                                        
                                        if (document.getElementById("Faxcontrol1_AutofaxControl1_hf_closepopup").value == "1")
                                        {   
                                            window.parent.HideDailog();           
                                            document.getElementById("Faxcontrol1_AutofaxControl1_hf_closepopup").value = "0";
                                        }
                                    }
                            </script>
</div>

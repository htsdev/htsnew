﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UpdateNisiContactInfo.ascx.cs"
    Inherits="HTP.WebControls.UpdateNisiContactInfo" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<link href="../Styles.css" type="text/css" rel="stylesheet" />

<script src="../Scripts/Dates.js" type="text/javascript"></script>

<script src="../Scripts/jsDate.js" type="text/javascript"></script>

<script language="javascript" type="text/javascript">



function ClosePopUp(backdiv,frontdiv)
{
    
    document.getElementById(backdiv).style.display="none";        
    document.getElementById(frontdiv).style.display="none"; 
    return false;
}

 function closeModalPopup(popupid)
    {
        var modalPopupBehavior = $find(popupid);
        modalPopupBehavior.hide();
        return false;
    }


function noFollowUpdateCheck ()
{
    var ChkNoFollowUpDate = document.getElementById ("<%= ChkNoFollowUpDate.ClientID %>");
    if (ChkNoFollowUpDate.checked)
        {
            document.getElementById("<%= enabledCalender.ClientID %>").style.display = 'none';
            document.getElementById("<%= disabledCalender.ClientID %>").style.display = 'block';
            
        }
    else 
        {
            document.getElementById("<%= enabledCalender.ClientID %>").style.display = 'block';
            document.getElementById("<%= disabledCalender.ClientID %>").style.display = 'none';
        }
        return false;
}

function ValidateInput()
{   
    var nisiGeneratedDate = document.getElementById('<%=this.HfNisiGeneratedDate.ClientID %>').value;
    var followUpDate = document.getElementById('<%=this.ClientID %>_cal_followupdate').value;
    var followUpDateNew = document.getElementById('<%=this.hf_FollowUpDateAtStartup.ClientID %>').value;
    var genStr = document.getElementById ("<%= tb_GeneralComments.ClientID %>").value;
    var ChkNoFollowUpDate = document.getElementById ("<%= ChkNoFollowUpDate.ClientID %>");
    var newContactTypeId = document.getElementById('<%= ddl_callback.ClientID %>').value;
    var firstTimeFollowUpDate = document.getElementById('<%= lblCurrentFollup.ClientID %>').innerText;
    var oldContactTypeId = document.getElementById('<%=this.HfPreviousContactTypeId.ClientID %>').value;
    var notes = document.getElementById('<%=this.TxtNotes.ClientID %>').value;
    var modelPopupControl = document.getElementById('<%=this.HfModelPopupControlId.ClientID %>').value;
    var tr_FollowupFreezed = document.getElementById('<%=this.disabledCalender.ClientID %>');
    var allreadyFreezed = document.getElementById('<%=this.HfAlreadyFreezed.ClientID %>').value;
    var currentTime = new Date()
    var mydate = (currentTime.getMonth() + 1) + "/" + currentTime.getDate() + "/" + currentTime.getFullYear();
    var diff =Math.ceil(DateDiff(followUpDate, nisiGeneratedDate));
    var futureDate =Math.ceil(DateDiff(mydate, followUpDate));
    
    var followupdiff =Math.ceil(DateDiff(followUpDate, followUpDateNew));
    
    //if (followUpDate)
    
    
    if (futureDate > 0 && (!ChkNoFollowUpDate.checked))
    {
        alert("Please select future date");
    	return false;
    }
    
    if (diff > 60 && (!ChkNoFollowUpDate.checked))
    {   
        alert("Please select follow up date within next 60 days from NISI Generated date");
    	return false;
    }

    if (ChkNoFollowUpDate.checked && trim(genStr)=="" && allreadyFreezed == '0')
    {
        alert("If no Follow up Date then Please enter comments");
        return false;  
    }

    // if NISI Folowup date changed. . . 
    if (followupdiff != 0 || ((firstTimeFollowUpDate == 'N/A') && (!ChkNoFollowUpDate.checked)))
    {   
        if(trim(genStr)=="")
        {
            alert("Please enter General Comments");
            return false;       
        }
    }
    if(newContactTypeId != oldContactTypeId)
    {
        if(trim(notes) == "")
        {
            alert("Please enter Notes in Notes text box");
            return false;  
        }
    }
     //ClosePopUp('<%= this.pnl_control.ClientID %>','<%=this.Disable.ClientID %>');
      var modalPopupBehavior = $find(modelPopupControl);
       modalPopupBehavior.hide();
     
    //return CheckDate(document.getElementById("<%= this.ClientID %>_cal_followupdate").value,document.getElementById("<%= this.ClientID %>_cal_followupdate") )
    //ClosePopUp('<%= this.pnl_control.ClientID %>','<%=this.Disable.ClientID %>');
}

function trim(stringToTrim) 
{
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}
	
	function DateDiff(date1, date2)
	{
	    var one_day=1000*60*60*24;
		var objDate1=new Date(date1);
		var objDate2=new Date(date2);
		return (objDate1.getTime()-objDate2.getTime())/one_day;
	}
    
	function CheckFollowUpDate() {

	    CalendarPopup_Up_SelectDate('UpdateFollowUpInfo2_cal_followupdate', '', 'UpdateFollowUpInfo2_cal_followupdate_div', 'UpdateFollowUpInfo2_cal_followupdate_monthYear', document.getElementById("UpdateFollowUpInfo2_cal_followupdate").value, 1, true, true, 'UpdateFollowUpInfo2_cal_followupdate_Up_PostBack', '', 'UpdateFollowUpInfo2_cal_followupdate_outer_VisibleDate')
	}


</script>

<asp:Panel ID="Disable" runat="server" Style="display: none; z-index: 1; filter: alpha(opacity=50);
    left: 1px; position: absolute; top: 1px; height: 1px; background-color: silver">
</asp:Panel>
<style type="text/css">
    .style1
    {
    	text-align: left;
        font-weight: bold;
        font-size: 8pt;
        color: #3366cc;
        font-family: Tahoma;
        text-decoration: none;
        width: 190px;
        padding-left: 6px;
    }
    
    
    .style2
    {
    	font-family: Tahoma;
	    font-size: 8pt;
	    color: #123160;
	    border-bottom-width: 0;
	    border-left-width: 0;
	    border-right-width: 0;
	    border-top-width: 0;
	    text-align: left;
    }
</style>
<div id="pnl_control" runat="server">
<aspnew:UpdatePanel ID = "upnl" runat = "server" RenderMode = "Inline">
<ContentTemplate>
    <table border="2" enableviewstate="true" style="border-color: navy; border-collapse: collapse;
        width: 380px">
        <tr>
            <td background="../Images/subhead_bg.gif" valign="bottom">
                <table border="0" width="100%">
                    <tr>
                        <td class="clssubhead" style="height: 26px">
                            <asp:Label ID="lbl_head" Text = "Update Case Status" runat="server"></asp:Label>
                        </td>
                        <td align="right">
                            &nbsp;<asp:LinkButton ID="lbtn_close" runat="server">X</asp:LinkButton>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" class="clsLeftPaddingTable" border="0">
                    <tr>
                        <td class="style1">
                            Client Name :
                        </td>
                        <td class ="style2" style="height: 30px;">
                            <asp:Label ID="lbl_FullName" runat="server" CssClass="clsLabel"></asp:Label>
                        </td>
                    </tr>
                    <tr id="tr_TicketNumber" runat="server">
                        <td class="style1">
                            Underlying Ticket Number :
                        </td>
                        <td class ="style2" style="height: 30px;">
                            <asp:Label ID="lblTicketNumber" runat="server" CssClass="clsLabel"></asp:Label>
                        </td>
                    </tr>
                    <tr id="tr_CauseNumber" runat="server">
                        <td class="style1">
                            Cause Number :
                        </td>
                        <td class ="style2" style="height: 30px;">
                            <asp:Label ID="lblCauseNo" runat="server" CssClass="clsLabel"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            Current Follow up Date :
                        </td>
                        <td class ="style2" style="height: 30px">
                            <asp:Label ID="lblCurrentFollup" runat="server" CssClass="clsLabel"></asp:Label>
                        </td>
                    </tr>
                    <tr id = "enabledCalender" runat = "server" style="display:block">
                        <td class="style1">
                            Next Follow up Date :
                        </td>
                        <td class ="style2" style="height: 30px;">
                            <ew:CalendarPopup Visible="false" ID="cal_followupdate" runat="server" AllowArbitraryText="False"
                                CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Culture="(Default)" EnableHideDropDown="True"
                                Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif" Nullable="True"
                                PadSingleDigits="True" ShowClearDate="False" ShowGoToToday="True" Text="" ToolTip="Date"
                                UpperBoundDate="9999-12-29" Width="101px" OnClientChange="callme()" >
                                <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    Font-Size="XX-Small" ForeColor="Black" />
                                <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                    ForeColor="Black" />
                                <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                    ForeColor="Black" />
                                <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                    ForeColor="Black" />
                                <TextboxLabelStyle CssClass="clstextarea" />
                                <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    Font-Size="XX-Small" ForeColor="Black" />
                                <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                    ForeColor="Black" />
                                <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                    ForeColor="Black" />
                                <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    Font-Size="XX-Small" ForeColor="Gray" />
                                <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                    ForeColor="Black" />
                                <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    Font-Size="XX-Small" ForeColor="Black" />
                            </ew:CalendarPopup>
                        </td>
                    </tr>
                    
                    <tr id = "disabledCalender" runat = "server" style="display:none">
                    
                    <td class="style1">
                            Next Follow up Date :
                        </td>
                        <td class ="style2" style="height: 30px;">
                            <ew:CalendarPopup Visible="true" ID="cal_followupdateDisabled" runat="server" AllowArbitraryText="False"
                                CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Culture="(Default)" EnableHideDropDown="True"
                                Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif" Nullable="True"
                                PadSingleDigits="True" ShowClearDate="False" ShowGoToToday="True" Text="" ToolTip="Date"
                                UpperBoundDate="9999-12-29" Width="101px" OnClientChange="callme()" >
                                <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    Font-Size="XX-Small" ForeColor="Black" />
                                <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                    ForeColor="Black" />
                                <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                    ForeColor="Black" />
                                <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                    ForeColor="Black" />
                                <TextboxLabelStyle CssClass="clstextarea" />
                                <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    Font-Size="XX-Small" ForeColor="Black" />
                                <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                    ForeColor="Black" />
                                <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                    ForeColor="Black" />
                                <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    Font-Size="XX-Small" ForeColor="Gray" />
                                <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                    ForeColor="Black" />
                                <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    Font-Size="XX-Small" ForeColor="Black" />
                            </ew:CalendarPopup>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            No Follow Up Date :</td>
                        <td class ="style2" style="height: 30px;">
                            <asp:CheckBox ID = "ChkNoFollowUpDate" onclick="noFollowUpdateCheck(this);" runat = "server" CssClass ="clsleftpaddingtable" />
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            Comments :
                        </td>
                        <td class ="style2" style="height: 20px;">
                            &nbsp;
                        </td>
                    </tr>
                    <tr id="tr_comment" runat="server">
                        <td valign="top" colspan="2">
                            <table enableviewstate="true" style="border-color: navy; border-collapse: collapse;
                                width: 323px;">
                                <tr>
                                    <td style="height: 15px; width: 314px; padding-left: 6px; text-align: left;">
                                        <div id="divcomment" runat="server" style="overflow: auto; height: 50px; width: 375px;">
                                            <asp:Label ID="lblComments" runat="server" CssClass="style2"></asp:Label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 314px; padding-left: 6px;" >
                                        <asp:TextBox ID="tb_GeneralComments" runat="server" Height="70px" TextMode="MultiLine"
                                            Width="375px"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="hf_ticketid" runat="server" Value="0" />
                            <asp:HiddenField ID="hf_FollowUpDate" runat="server" />
                            <asp:HiddenField ID="hf_ticid" runat="server" />
                            <asp:HiddenField ID="hf_FollowUpType" runat="server" />
                            <asp:HiddenField ID="hf_FollowUpDateAtStartup" runat="server" Value="" />
                            <asp:HiddenField ID="hf_StatusAtStartup" runat="server" Value="" />
                            <asp:HiddenField ID="HfNisiGeneratedDate" runat="server" Value="" />
                            <asp:HiddenField ID="HfPreviousContactTypeId" runat="server" Value="" />
                            <asp:HiddenField ID="HfModelPopupControlId" runat="server" Value="" />
                            <asp:HiddenField ID="HfMultiNisiCause" runat="server" Value="" />
                            <asp:HiddenField ID="HfMultiUnderlying" runat="server" Value="" />
                            <asp:HiddenField ID="HfAlreadyFreezed" runat="server" Value="" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="2">
                            <table cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td class="style1" style ="width:35%">
                                        Contact Status 
                                    </td>
                                    <td style="height: 50px;">
                                        <asp:DropDownList ID="ddl_callback" runat="server" Width = "200px" CssClass="clsInputCombo"></asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style1" valign = "top">
                                        Notes
                                    </td>
                                    <td style="height: 20px;">
                                    <asp:TextBox ID ="TxtNotes" runat="server" Height="70px" TextMode= "MultiLine" Width="200px"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center" style="height: 20px">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center" style="height: 40px">
                            <asp:Button ID="btnsave" runat="server" CssClass="clsbutton" Text="Update" Width="75px"
                                OnClientClick="return ValidateInput();" OnClick="btnsave_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </ContentTemplate>
    </aspnew:UpdatePanel>
</div>

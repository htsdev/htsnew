namespace lntechNew.WebControls
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using FrameWorkEnation.Components; 
	/// <summary>
	///		Summary description for LetterMainMenu.
	/// </summary>
	public partial class LetterMainMenu : System.Web.UI.UserControl
	{
		
		
		
		
		
		
		
		
		protected System.Web.UI.WebControls.Label lblVBar3;

		clsENationWebComponents objEnationFramework  = new clsENationWebComponents();

		private void Page_Load(object sender, System.EventArgs e)
		{			
			string strPageName ;			
			DataSet  dsSecurityMatrix;

			int ApplicationID= 0; //3=LetterManagement

			// Get Application ID for Admin Security Module from Main/Login Form			
			ApplicationID = Convert.ToInt32(Session["AppID"]);
		
			try
			{				
				strPageName = Request.RawUrl;
				strPageName =  strPageName.Substring(strPageName.LastIndexOf("/")+ 1); 
				strPageName = strPageName.Substring(0,strPageName.LastIndexOf("."));
                string[] key = {"@AdminApplicationID","@AdminUserID","@AdminPageName" };
                object[] value = {ApplicationID,Session["AdminUserID"],strPageName,};
                dsSecurityMatrix = objEnationFramework.Get_DS_BySPArr("usp_Get_AdminUserMatrixByUserID_PageName",key,value  ); 
                //dsSecurityMatrix  = objEnationFramework.Get_DS_BySPByThreeParmameter("usp_Get_AdminUserMatrixByUserID_PageName", "AdminApplicationID", ApplicationID, "AdminUserID", Session["AdminUserID"], "AdminPageName", strPageName ); 
				
				if (dsSecurityMatrix.Tables[0].Rows.Count > 0)
				{
					foreach (DataRow ControlRow in dsSecurityMatrix.Tables[0].Rows) 
					{		
						if ( Convert.ToInt32(ControlRow["IsMenu"]) == 1)
						{
							//ControlRow["PageControlType"].ToString().ToLower()
							int intControlType = Convert.ToInt32( ControlRow["PageControlTypeID"]);
							switch(intControlType)
							{									
								case 5:
									((LinkButton)this.FindControl(Convert.ToString(ControlRow["PageControlName"]))).Enabled = Convert.ToBoolean(ControlRow["ControlEnabled"]);
									break;							
								case 6:
									((HyperLink)this.FindControl(ControlRow["PageControlName"].ToString())).Enabled = Convert.ToBoolean(ControlRow["ControlEnabled"]);
									
									break;
									//case "dropdownlist":
									//	((DropDownList)Page.FindControl(ControlRow["PageControlName"].ToString())).Enabled = Convert.ToBoolean(ControlRow["ControlEnabled"]);								
							}														
						}
					}
				}
			}
			catch
			{
				Response.Redirect("~\\LetterAccessDeny.aspx");
			}
			
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            //this.lnkHome.Click += new System.EventHandler(this.lnkHome_Click);
            //this.lnkUserList.Click += new System.EventHandler(this.lnkUserList_Click);
            //this.lnkUserManagement.Click += new System.EventHandler(this.lnkUserManagement_Click);
            //this.lnkRoleManagement.Click += new System.EventHandler(this.lnkRoleManagement_Click);
            //this.lnkLogout.Click += new System.EventHandler(this.lnkLogout_Click);
            //this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void lnkUserList_Click(object sender, System.EventArgs e)
		{
			Response.Redirect ("../Security/ListUser.aspx");
		}

		private void lnkUserManagement_Click(object sender, System.EventArgs e)
		{
			Response.Redirect ("../Security/AdminUserManagement.aspx");
		}

		private void lnkRoleManagement_Click(object sender, System.EventArgs e)
		{
			Response.Redirect ("../Security/AdminRoleManagement.aspx");
		}

		private void lnkHome_Click(object sender, System.EventArgs e)
		{
			Response.Redirect ("../Letters/LetterMain.aspx");
		
		}

		private void lnkLogout_Click(object sender, System.EventArgs e)
		{
			
			Session["LoginID"]="";
			Session["AdminUserID"]="";        
			Response.Redirect("../Letters/LettersDefault.aspx");
		}
	}
}

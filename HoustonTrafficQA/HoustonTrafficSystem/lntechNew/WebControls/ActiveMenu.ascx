<%@ Control Language="c#" AutoEventWireup="false" Inherits="lntechNew.WebControls.ActiveMenu"
    TargetSchema="http://schemas.microsoft.com/intellisense/ie5" CodeBehind="ActiveMenu.ascx.cs" %>
<link href="../Styles.css" type="text/css" rel="stylesheet" />
<%--<script src="../Scripts/Validationfx.js" type="text/javascript"></script>--%>

<script language="javascript" type="text/javascript">
    function OpenPopUpTTM(str)
     {                     
        window.open("/backroom/GeneralBugPopUp.aspx?pageurl="+ location.href  +"&casenumber=" + str,'',"height=260,width=410,resizable=no,status=no,scrollbar=no,menubar=no,location=no");
        return false;      
     }
 
     function OpenCourtSearch()
     {
        window.open('http://courtsearch.legalhouston.com');
        return false;
     }
// Fahad 13/01/2009 5376 ---------Method to open new window in which Business Logic is appear------- 
    function OpenLogic()
    {
        var myWindow;
		var width = 760;
        var height = 540;
        var left = parseInt((screen.availWidth/2) - (width/2));
        var top = parseInt((screen.availHeight/2) - (height/2));
        var windowFeatures = "width=" + width + ",height=" + height + ",status,resizable=no,left=" + left + ",top=" + top + "screenX=" + left + ",screenY=" + top;
        //Ozair 7379 04/29/2010 fix business logic popup issue for POLM and Role Based security nested folder
        myWindow = window.open("<%=ViewState["hfServerAdd"]%>/businesslogic/logicpage.aspx?MenuID=<%=ViewState["sMenu"]%>", "subWind", windowFeatures);
		return false;
    }
    
    // Noufil 4378 07/23/2008 Function to open a window at page center.... this fuction use to open change password page
      // Sabir Khan 5650 03/12/2009 Function to open a window at page center...
		function openCenteredWindow(url,width,height) 
        {
            var left = parseInt((screen.availWidth/2) - (width/2));
            var top = parseInt((screen.availHeight/2) - (height/2));
            var windowFeatures = "width=" + width + ",height=" + height + 
                ",status,left=" + left + ",top=" + top + 
                "screenX=" + left + ",screenY=" + top;
            window.open(url, "", windowFeatures);
            return false;
        }
        // Haris Ahmed 10181 04/13/2009 Function to open a window at page top...
        function openTopWindow(url,width,height) 
        {
            var left = parseInt((screen.availWidth/2) - (width/2));
            var top = parseInt((screen.availHeight/2) - (height / 2));
            var windowFeatures = "width=" + width + ",height=" + height + 
                ",status,left=" + left + ",top=" + top + 
                "screenX=" + left + ",screenY=" + top + ",scrollbars=yes";
            window.open(url, "", windowFeatures);
            return false;
        }    
    
</script>

<!--
<script language="JavaScript1.2">mmLoadMenus();</script>
-->
<body>
    <table id="Table1" style="font-weight: bold; font-size: 11px; width: 100%; color: white;
        font-family: Tahoma, Sans-Serif; height: 30px" cellspacing="0" cellpadding="1"
        width="780" align="left" border="0">
        <tr>
            <td style="width: 780; height: 7px" colspan="9">
            </td>
        </tr>
        <tr>
            <td class="" style="width: 100%; height: 56px" colspan="9">
                <table width="100%">
                    <tbody>
                        <tr>
                            <td>
                                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/lnlogo.jpg"></asp:Image>
                            </td>
                            <td width="62%">
                                <table width="100%">
                                    <tr>
                                        <td align="right">
                                            <asp:Label ID="lbl_Name" runat="server" ForeColor="RoyalBlue"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <asp:Label ID="lblClientIP" runat ="server" Text="Client IP Address" ForeColor="RoyalBlue"></asp:Label>
                                            &nbsp;&nbsp;&nbsp;
                                            <asp:Label ID="lblClientIPAddress" runat ="server"  ForeColor="RoyalBlue"></asp:Label>
                                        </td>                                        
                                    </tr>
                                     <tr>
                                        <td align="right">                                       
                                            <asp:Label ID="lblBranch" runat ="server" Text="Branch" ForeColor="RoyalBlue"></asp:Label>
                                            &nbsp;&nbsp;&nbsp;
                                            <asp:Label ID="lblBranchName" runat ="server"  ForeColor="RoyalBlue"></asp:Label>
                                        </td>                                        
                                    </tr>
                                    <tr>
                                        <td valign="bottom" align="right">
                                            <table width="100%">
                                                <tr>
                                                    <td align="center">
                                                        <asp:HyperLink ID="HP" runat="server" NavigateUrl="http://intranet.legalhouston.com">Intranet </asp:HyperLink>
                                                    </td>
                                                    <td width="1%">
                                                        <span style="color: RoyalBlue">|</span>
                                                    </td>
                                                    <td align="center">
                                                        <asp:LinkButton ID="tt" runat="server" Visible="false">Trouble Ticket </asp:LinkButton>
                                                        <asp:HyperLink ID="hlTicketDesk" runat="server" Target="_blank">Trouble Ticket </asp:HyperLink>
                                                    </td>
                                                    <td width="1%">
                                                        <span style="color: RoyalBlue">|</span>
                                                    </td>
                                                    <td style="width: 125px" align="center">
                                                        <asp:LinkButton ID="lnkb_password" runat="server" Text="Change Password" ToolTip="Change Password"
                                                            OnClientClick="return openCenteredWindow('../backroom/changeEmpPassword.aspx',400,200);"></asp:LinkButton>
                                                    </td>
                                                    <td width="1%">
                                                        <span style="color: RoyalBlue">|</span>
                                                    </td>
                                                    <td align="center">
                                                        <%--<asp:LinkButton ID="lnkbtn_CourtSearch" OnClientClick="return OpenCourtSearch();"
                                                            runat="server">Court Search </asp:LinkButton>--%>
                                                            <asp:LinkButton ID="lnkb_addnewlead" runat="server" Text="Add New Lead" ToolTip="Add New Lead"
                                                            OnClientClick="return openTopWindow('../clientinfo/addnewlead.aspx',400,420);"></asp:LinkButton>
                                                    </td>
                                                    <td id="td_logoff1" runat="server" width="1%">
                                                        <asp:Label ID="lblSep" runat="server" Text="|" ForeColor="RoyalBlue"></asp:Label>
                                                    </td>
                                                    <td id="td_logoff2" runat="server" align="center">
                                                        <asp:LinkButton ID="lnkb_LogOff" runat="server" ForeColor="RoyalBlue">Log Off</asp:LinkButton>
                                                    </td>
                                                    <td>
                                                        &nbsp; <span style="color: RoyalBlue">|</span>&nbsp;
                                                    </td>
                                                    <td>
                                                        <asp:Image ID="img_Logic" onclick="OpenLogic();" runat="server" ImageUrl="~/Images/help.gif"
                                                            ToolTip="Show Business Logic" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
    </table>
    <tr height="7">
        <td colspan="9">
        </td>
    </tr>
    <tr>
        <td>
            <table style="font-weight: bold; font-size: 11px; width: 100%; color: white; font-family: Tahoma, Sans-Serif;
                height: 30px" cellspacing="0" cellpadding="1" width="780" align="left" border="0">
                <tr>
                    <%=mainMenu()%>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="left">
            <table bordercolor="#ff9966" cellspacing="0" cellpadding="0" width="100%" bgcolor="#ffcc00"
                border="1">
                <tr>
                    <%=subMenu()%>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="left">
            <img height="17" hspace="0" src="../../images/head_icon.gif" width="30" align="left"><asp:Label
                ID="lblheading" runat="server" CssClass="clsmainhead"></asp:Label>
        </td>
    </tr>
    <tr>
        <td style="display: none" align="left">
            <asp:TextBox ID="txtid" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtsrch" runat="server"></asp:TextBox>            
        </td>
    </tr>
</body>

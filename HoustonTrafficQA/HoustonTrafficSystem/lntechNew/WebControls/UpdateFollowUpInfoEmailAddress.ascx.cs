using System;
using System.Data;
using System.Web.UI.WebControls;
using lntechNew.Components.ClientInfo;
using HTP.Components;
using FrameWorkEnation.Components;


namespace HTP.WebControls
{
    /// <summary>
    /// 
    /// </summary>
    public partial class UpdateFollowUpInfoEmailAddress : System.Web.UI.UserControl
    {
        #region Variables
        bool _freezecalender;

        /// <summary>
        /// 
        /// </summary>
        public event PageMethodHandler PageMethod = null;
        clsLogger bugTracker = new clsLogger();
        clsENationWebComponents ClsDb = new clsENationWebComponents();

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public string Title
        {
            set
            {
                lbl_head.Text = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool Freezecalender
        {
            get { return _freezecalender; }
            set { _freezecalender = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public FollowUpType followUpType
        {
            set
            {
                hf_FollowUpType.Value = value.ToString();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool EnableHideDropDown
        {
            set
            {
                this.cal_followupdate.EnableHideDropDown = value;
            }
        }

        #endregion

        #region Events

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                btnsave.Attributes.Add("onClick", "return ClosePopUp('" + Disable.ClientID + "','" + pnl_control.ClientID + "');");
                btnCancel.Attributes.Add("onClick", "return ClosePopUp('" + Disable.ClientID + "','" + pnl_control.ClientID + "');");
                lbtn_close.Attributes.Add("onClick", "return ClosePopUp('" + Disable.ClientID + "','" + pnl_control.ClientID + "');");
                pnl_control.Attributes.Add("style", "z-index: 3; display: none; position: absolute; background-color: transparent; left: 0px; top: 0px;");
                GetReminderStatus();
            }

            if (DesignMode)
            {
                cal_followupdate.Visible = false;
            }
            else
            {
                cal_followupdate.Visible = true;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="BusinessDay"></param>
        /// <param name="upp"></param>
        /// <param name="low"></param>
        /// <param name="firstname"></param>
        /// <param name="lastname"></param>
        /// <param name="ticketno"></param>
        /// <param name="ticketid"></param>
        /// <param name="causeno"></param>
        /// <param name="comments"></param>
        /// <param name="PanelID"></param>
        /// <param name="court"></param>
        /// <param name="courtname"></param>
        /// <param name="followUpDate"></param>
        public void binddate(string BusinessDay, DateTime upp, DateTime low, string firstname, string lastname, string ticketno, string ticketid, string causeno, string comments, string PanelID, string court, string courtname, string followUpDate, string email, string ContactNumber1, string ContactNumber2, string ContactNumber3, string isContacted, int reviewedEmailStatus)
        {
            if (_freezecalender)
            {
                ViewState["freestate"] = _freezecalender;
                if (tb_GeneralComments.Text != string.Empty)
                {
                    tb_GeneralComments.Text = "";
                }

                lbl_FullName.Text = firstname + " " + lastname;
                lblTicketNumber.Text = ticketno;
                lblCurrentFollup.Text = followUpDate;
                hf_ticid.Value = ticketno;
                hf_ticketid.Value = ticketid;
                this.BusinessDay.Value = BusinessDay;
                lblCauseNo.Text = causeno;
                lblContactNumber1.Text = ContactNumber1;
                lblContactNumber2.Text = ContactNumber2;
                lblContactNumber3.Text = ContactNumber3;
                //Muhammad Nasir 10071 12/10/2012 Save actual email value at startup
                txt_Email.Text = hf_EmailAtStartup.Value = email;
                hf_isContacted.Value = isContacted;

                //Muhammad Nasir 10071 11/22/2012 
                try
                {
                    ddl_callback.SelectedValue = reviewedEmailStatus.ToString();
                }
                catch (Exception ex)
                {
                    bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                }
                //Muhammad Nasir 10071 12/20/2012 keep followup date at startup
                hf_StatusAtStartup.Value = ddl_callback.SelectedValue;

                lblComments.Text = comments;
                cal_followupdate.SelectedDate = upp;
                //Muhammad Nasir 10071 12/20/2012 keep status at startup
                hf_FollowUpDateAtStartup.Value = cal_followupdate.SelectedDate.ToShortDateString();
                pnl_control.Style["display"] = "";
                pnl_control.Style["position"] = "";
                chk_UpdateAll.Visible = false;

                if (court == "HMC-FTA")
                {
                    chk_IsHMCFTARemoved.Visible = true;
                    chk_IsHMCFTARemoved.Checked = false;
                }
                hf_court.Value = court;
                Hf_field.Value = "1";
                hf_courtname.Value = courtname;

                ViewState["freestate"] = courtname;
                btnCancel.Attributes.Remove("onClick");
                lbtn_close.Attributes.Remove("onClick");

                btnCancel.Attributes.Add("onClick", "return closeModalPopup('" + PanelID + "');");
                lbtn_close.Attributes.Add("onClick", "return closeModalPopup('" + PanelID + "');");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnsave_Click(object sender, EventArgs e)
        {
            int ticketId = int.Parse(this.hf_ticketid.Value);
            int isContacted = Convert.ToInt32(this.hf_isContacted.Value);
            clsCase cCase = new clsCase();
            clsSession cSession = new clsSession();
            clsLogger cLog = new clsLogger();
            cCase.TicketID = ticketId;
            int EmpId = int.Parse(cSession.GetCookie("sEmpID", this.Request));
            cCase.EmpID = EmpId;
            cCase.Email = txt_Email.Text;

            string CallBackStatus;

            if (Convert.ToString(hf_VerifyEmailCallBackStatus.Value) != "Contacted")
                CallBackStatus = ddl_callback.SelectedItem.Text;
            else
                CallBackStatus = "Contacted";

            if (isContacted == 0)
                CallBackStatus = "Contacted";

            UpdateRecords(cCase.TicketID, cCase.EmpID, CallBackStatus, cCase.Email, isContacted);

            var objUpdateFolloUp = new ClsUpdateFollowUpInfo();
            var freeState = (ViewState["freestate"] == null ? string.Empty : ViewState["freestate"].ToString());

            if (hf_FollowUpType.Value == FollowUpType.AddressValidationReport.ToString())
            {
                objUpdateFolloUp.UpdateAddressValidationFollowUpDate(Convert.ToInt32(hf_ContactID.Value), Convert.ToInt32(hf_ticketid.Value), EmpId, lblCurrentFollup.Text, cal_followupdate.SelectedDate, tb_GeneralComments.Text);
            }
            else
            {
                objUpdateFolloUp.UpdateFollowUpDate(this.hf_ticketid.Value, EmpId, lblCurrentFollup.Text, hf_FollowUpType.Value, cal_followupdate.SelectedDate, tb_GeneralComments.Text, freeState, hf_ticid.Value, chk_UpdateAll.Checked, chk_IsHMCFTARemoved.Checked);
            }



            if (PageMethod != null)
                PageMethod();
        }

        public void GetReminderStatus()
        {
            try
            {
                //Muhammad Nasir 10071 11/21/2012
                //DataTable dtStatus = ClsDb.Get_DT_BySPArr("USP_Get_Reminder_Status");
                DataTable dtStatus = ClsDb.Get_DT_BySPArr("USP_HTP_Get_Confirmation_Status");

                if (dtStatus.Rows.Count > 0)
                {
                    ddl_callback.Items.Clear();
                    foreach (DataRow dr in dtStatus.Rows)
                    {
                        //ListItem item = new ListItem(dr["Description"].ToString(), dr["Reminderid_PK"].ToString());
                        //Muhammad Nasir 10071 12/10/2012
                        ListItem item = new ListItem(dr["Description"].ToString(), dr["Confirmationid_PK"].ToString());
                        ddl_callback.Items.Add(item);
                    }
                    //Muhammad Nasir 10071 11/26/2012
                    ddl_callback.Items.Insert(0, new ListItem("Choose", "-1"));
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        public void UpdateRecords(int TicketID, int EmpID, string CallBackStatus, string email, int isContacted)
        {
            //Muhammad Nasir 10071 12/19/2012 Added comments parameter
            string[] keys = { "@ticketid", "@empID", "@CallBackStatus", "@email", "@isContactedPage", "@comments" };
            object[] values = { TicketID, EmpID, CallBackStatus, email, isContacted, tb_GeneralComments.Text };
            ClsDb.ExecuteScalerBySp("usp_htp_update_BadEmailRecords", keys, values);
        }

        #endregion
    }
}
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UpdateViolationSRV.ascx.cs"
    Inherits="lntechNew.WebControls.UpdateViolationSRV" %>
<%@ Register Assembly="System.Web.Extensions" Namespace="System.Web.UI" TagPrefix="aspnew" %>
<link href="../Styles.css" rel="stylesheet" type="text/css" />

<script src="../Scripts/Validationfx.js" type="text/javascript"></script>

<script src="../Scripts/Dates.js" type="text/javascript"></script>

<script type="text/javascript">

    // Close Popup Script
    
   
    function closepopup(ctrl1,ctrl2)
	    {	    
	         document.getElementById(ctrl1).style.display = 'none';
	         document.getElementById(ctrl2).style.display = 'none';	
	         restoreTab();
	         return false;
        }
    
    // Toggle Image and Drop Down List
    function DisplayToggleP(ctrl1,ctrl2,ctrlstyle)
		{	   
			
		    var temp =   document.getElementById(ctrl1).style.display;
			document.getElementById(ctrl1).style.display = document.getElementById(ctrl2).style.display;
		    document.getElementById(ctrl2).style.display = temp;
		    document.getElementById(ctrlstyle).value  =document.getElementById(ctrl1).style.display
		    
		    
		}
 // Toggle Image and Drop Down List
    function ToggleCourt(ctrl1,ctrl2,ctrlstyle,court)
		{	   
		   
		    var temp =   document.getElementById(ctrl1).style.display;
			document.getElementById(ctrl1).style.display = document.getElementById(ctrl2).style.display;
		    document.getElementById(ctrl2).style.display = temp;
		    document.getElementById(ctrlstyle).value  =document.getElementById(ctrl1).style.display
		    
		    var courtlist = document.getElementById(ctrl1); 
		    
		    if ( courtlist.value != document.getElementById(court).value && document.getElementById(ctrl1).value !="0" && courtlist.style.display=="none")
    		    {
    		        courtlist.value = "0";
    		        setTimeout('__doPostBack(\'UpdateViolationSRV1$lbtn_close\',\'\')', 0);
    		    }
		    
		}	
		
		function DisplayToggleP2(ctrl1,ctrl2,ctrlstyle,ddl_court,hf_oldstatus,noaction,pledout,trMissedCourt)
		{	   
			
		    var temp =   document.getElementById(ctrl1).style.display;
		    var currCourtLocation = document.getElementById(ddl_court).value;
		    document.getElementById(ctrl1).style.display = document.getElementById(ctrl2).style.display;
		    document.getElementById(ctrl2).style.display = temp;
		    document.getElementById(ctrlstyle).value  =document.getElementById(ctrl1).style.display;
					    
		    //Zeeshan Ahmed 3486 04/04/2008
		    var ddlStatus = document.getElementById(ctrl1);
		    //Get Old Status
		    var oldstatus = document.getElementById(hf_oldstatus).value;
		    var trMissedCourt = document.getElementById(trMissedCourt);
		    
		    //Zeeshan Ahmed 4163 06/02/2008 Show/Hide Missed Court Types 
            if ( (ddlStatus.style.display == "none" && oldstatus == "105") || (ddlStatus.style.display =="block"  && ddlStatus.value == 105 ))
                trMissedCourt.style.display = "block";
            else if ( (ddlStatus.style.display =="block"  && ddlStatus.value != 105) || (ddlStatus.style.display == "none" && oldstatus != "105") )
                trMissedCourt.style.display = "none";
                
            return false;
		}

    // Toggle Image and Drop Down List and Fill Violation List
    function DisplayTogglePNew(ctrl1,ctrl2,ctrlstyle)
	{		    
		    var temp =   document.getElementById(ctrl1).style.display;
			document.getElementById(ctrl1).style.display = document.getElementById(ctrl2).style.display;
		    document.getElementById(ctrl2).style.display = temp;
		    document.getElementById(ctrlstyle).value  =document.getElementById(ctrl1).style.display
		    return false;           
	}
    	
   	// Toggle Image and Drop Down List and Fill Violation List
//    function DisplayTogglePNew(ctrl1,ctrl2,ctrlstyle,ddl_court ,lastcourtid)
//	{
//		    
//		    
//		    var temp =   document.getElementById(ctrl1).style.display;
//			document.getElementById(ctrl1).style.display = document.getElementById(ctrl2).style.display;
//		    document.getElementById(ctrl2).style.display = temp;
//		    document.getElementById(ctrlstyle).value  =document.getElementById(ctrl1).style.display
//		    
//		      if ( temp=='block')
//		        return false;
//		        
//		      var hasHCViolations = document.getElementById(lastcourtid).value;
//		      var currCourtLocation = document.getElementById(ddl_court).value;
//            
//              if ( hasHCViolations == "1" && currCourtLocation !="3037" )
//              return true;
//              
//              else if ( hasHCViolations == "0" && currCourtLocation == "3037")
//              return true;
//              
//              else if (  currCourtLocation == "3047")
//              return true;
//              
//              else
//              return false;	      		      		        
//		        
//	}	

    // Add New Violation Code
    	function addnewviolation(updatecontrol)
		{    	      
    	    var linkbuttonid  = document.getElementById("hf_lastrow").value ; 
		  
		    //For New Violation Set Value = 1 
		    // Clearing All PopUp Controls
	        document.getElementById(updatecontrol + "_tb_pvdesc").style.display = 'none';
	        document.getElementById(updatecontrol + "_tb_pstatus").style.display = 'none';
		    document.getElementById(updatecontrol + "_tb_pvdesc").value = '';
	        document.getElementById(updatecontrol + "_tb_pstatus").value = '';
		    document.getElementById(updatecontrol + "_ddl_pvdesc").style.display = 'block';
		    document.getElementById(updatecontrol + "_ddl_pstatus").style.display = 'block';
		    document.getElementById(updatecontrol + "_tb_causeno").value = "";
		    document.getElementById(updatecontrol + "_tb_pfineamount").value = "0";
		    document.getElementById(updatecontrol + "_tb_pbondamount").value = "0";
	        document.getElementById(updatecontrol + "_hf_violationid").value = "-1";
	        document.getElementById(updatecontrol + "_hf_ticketviolationid").value = "-1";
	        document.getElementById(updatecontrol + "_hf_ticketsviolationid").value = "0";
	        document.getElementById(updatecontrol + "_ddl_pvdesc").selectedIndex =  0;
            document.getElementById(updatecontrol + "_ddl_pcrtloc").style.display = "block";	
            document.getElementById(updatecontrol + "_ddl_pvlevel").value = 0; 	    
		    document.getElementById(updatecontrol + "_lbl_pcname").style.display = "none";	
		    showpopup(updatecontrol,linkbuttonid,1);
		}


	// Show Update Popup
	function showpopup(updatecontrol,linkbuttonid,isnew)
		{ 
		    //Get Case Type
		  var OldCaseType = document.getElementById(updatecontrol + "_hf_casetype").value
          document.getElementById(updatecontrol + "_hf_casetype").value = document.getElementById(linkbuttonid+"_hf_casetype").value;
          document.getElementById(updatecontrol + "_hf_oldcourtid").value = document.getElementById(linkbuttonid+"_hf_courtid").value;
          document.getElementById(updatecontrol + "_hf_oldstatus").value = document.getElementById(linkbuttonid+"_hf_courtviolationstatusid").value;          
          document.getElementById(updatecontrol + "_hf_oldccourtdate").value =document.getElementById(linkbuttonid + "_hf_courtdate").value;
          var CaseType = document.getElementById(updatecontrol + "_hf_casetype").value 
          
//          var PreTrialDiversion = <%=ViewState["PreTrialDiversion"]%>;
//          var MotionHiring = <%=ViewState["MotionHiring"]%>;
//          document.getElementById(updatecontrol + "_hf_PreTrialDiversion").value = PreTrialDiversion;
//          document.getElementById(updatecontrol + "_hf_MotionHiring").value = MotionHiring;
		  //Zeeshan 3486 04/04/2008
		  //Reset Missed Court Control
		  document.getElementById(updatecontrol + "_trMissedCourt").style.display = "none";
		  document.getElementById(updatecontrol + "_rbtnNoAction").checked = false;
		  document.getElementById(updatecontrol + "_rbtnPledOut").checked = false;
		  //Zeeshan Ahmed 3925 05/01/2008
		   document.getElementById(updatecontrol + "_hf_coveringFirmId").value = document.getElementById(linkbuttonid+"_hf_coveringFirmId").value;
		  //Zeeshan Ahmed 3535 04/08/2008
		  //FTA Modifications		
          document.getElementById(updatecontrol + "_hf_isFTAViolation").value = document.getElementById(linkbuttonid+"_hf_isFTAViolation").value;
	      document.getElementById(updatecontrol + "_hf_hasFTAViolations").value = document.getElementById(linkbuttonid+"_hf_hasFTAViolations").value;
		 
		  var causenumber =  document.getElementById(updatecontrol + "_tb_causeno");
		  var ticketnumber = document.getElementById(updatecontrol + "_tb_pticketno");
		 
		  //Waqas Bug 6077 06/25/2009
		  if(document.getElementById(linkbuttonid + "_hf_causeno").value == "N/A")
		  {
		    causenumber.value = '';
		  }
		  else
		  {
		    causenumber.value =   document.getElementById(linkbuttonid + "_hf_causeno").value;
		  }
		  ticketnumber.value =   document.getElementById(linkbuttonid + "_hf_ticketNumber").value;
          //Sabir Khan 7181 12/24/2009 Set "" string for ticket no for pasadena municipal court if ticketno contains PMC or empty... 
          if(((document.getElementById(linkbuttonid + "_hf_ticketNumber").value == "N/A")|| ( document.getElementById(updatecontrol + "_tb_pticketno").value.substring(0,3).toUpperCase() == "PMC")) && (document.getElementById(linkbuttonid + "_hf_courtid").value == "3004"))
             document.getElementById(updatecontrol + "_tb_pticketno").value = "";		 	
		   // Check The Court Is Criminal Court
		   var criminalcourt = document.getElementById(linkbuttonid + "_hf_iscriminalcourt").value;
		   document.getElementById(linkbuttonid + "_hf_iscriminalcourt").value = criminalcourt;
		   
		   document.getElementById(updatecontrol + "_ddl_pvlevel").value = document.getElementById(linkbuttonid + "_hf_chargelevel").value;
		   document.getElementById(updatecontrol + "_hf_Level").value = document.getElementById(linkbuttonid + "_hf_chargelevel").value;
    		  				   
		   
		   //If Case Type Is Criminal Or Civil Do Not Show Fine And Bond Amount
		   if ( CaseType == "2" || CaseType=="3")
		   {
		        document.getElementById(updatecontrol +"_tr_bondamount").style.display="block";  
		       document.getElementById(updatecontrol +"_tr_fineamount").style.display="block";
		   }
           else
           {   		   
		       document.getElementById(updatecontrol +"_tr_bondamount").style.display="none";  
               document.getElementById(updatecontrol +"_tr_fineamount").style.display="none";
           }
           
		   if ( criminalcourt == "1")
		   {
		       //Zeeshan Ahmed 3724 05/13/2008
		       // tahir 5117 11/11/2008 hide Level & CDI ....
		       if (document.getElementById(updatecontrol +"_hf_violationid").value == "18534")
		       {
		            document.getElementById(updatecontrol +"_tr_chargelevel").style.display="none";  
		            document.getElementById(updatecontrol +"_tr_cdi").style.display="none";  
		       }
               else 
               {
                    document.getElementById(updatecontrol +"_tr_chargelevel").style.display="block";  
               }
		       document.getElementById(updatecontrol +"_tr_arrestdate").style.display="block";
		       // Noufil 4369 07/17/2008 Hiding ticket number row for criminal cases    
		       document.getElementById(updatecontrol +"_tr_ticketno").style.display="none";
		       // If Cause Number Not Exist then fill it with the ticketnumber
		       if (causenumber.value=="")		      
		            causenumber.value = ticketnumber.value; 
		       //Yasir Kamal 6194 07/24/2009 hide bond Question row for criminal cases.
		       document.getElementById(updatecontrol +"_tr_bond").style.display="none";
		  
		   
		   }
		   else
		   {
		       document.getElementById(updatecontrol +"_tr_chargelevel").style.display="none";  
		       document.getElementById(updatecontrol +"_tr_arrestdate").style.display="none"; 
		       document.getElementById(updatecontrol +"_tr_ticketno").style.display="block";
		       document.getElementById(updatecontrol +"_tr_bond").style.display="block";
		   }
		   
		   // Noufil 6164 08/11/2009 Hide bond Question If Case is not off Traffic
		   if (CaseType != "1")
		   {
		        document.getElementById(updatecontrol +"_tr_bond").style.display = "none";
		   }

		   //Waqas 5864 07/30/2009 Check for family Case
		   if (CaseType == "4") 
		   {
		       document.getElementById(updatecontrol + "_tr_ticketno").style.display = "none";
		   }
		 		 
		  // Displaying Court Date 
		  var date = document.getElementById(linkbuttonid + "_hf_courtdate").value;
		  
		    
		
				 		 
		  if ( date == "1/1/1900" || date == "01/01/1900")
		  {
              document.getElementById(updatecontrol + "_tb_pmonth").value =  "";
		      document.getElementById(updatecontrol + "_tb_pday").value =   "";
		      document.getElementById(updatecontrol + "_tb_pyear").value = "";
		  }
		  else
		  {	  		        		    
		       document.getElementById(updatecontrol + "_tb_pmonth").value =   date.substring(0, date.indexOf('/'));
		       document.getElementById(updatecontrol + "_tb_pday").value =   date.substring(date.indexOf('/')+1 , date.lastIndexOf('/'));
		       document.getElementById(updatecontrol + "_tb_pyear").value =   date.substring(date.lastIndexOf('/') + 1+2 ,date.length);
		       
		       
		       document.getElementById(updatecontrol + "_hf_day").value =   date.substring(date.indexOf('/')+1 , date.lastIndexOf('/'));
		       //Waqas 6666 10/29/2009 Make it case sensitives to work proper for all other exploreres
		       document.getElementById(updatecontrol + "_hf_Month").value =  date.substring(0, date.indexOf('/'));
		       document.getElementById(updatecontrol + "_hf_year").value =  date.substring(date.lastIndexOf('/') + 1+2 ,date.length);
		       
		  }
		  
		  // Displaying Arrest Date 
		  var date = document.getElementById(linkbuttonid + "_hf_arrestdate").value;
		  
		    
            
		  if ( date == "1/1/1900" || date == "01/01/1900")
		  {
              document.getElementById(updatecontrol + "_tb_arrmonth").value =  "";
		      document.getElementById(updatecontrol + "_tb_arrday").value =   "";
		      document.getElementById(updatecontrol + "_tb_arryear").value = "";
		  }
		  else
		  {	   		    
		       document.getElementById(updatecontrol + "_tb_arrmonth").value =   date.substring(0, date.indexOf('/'));
		       document.getElementById(updatecontrol + "_tb_arrday").value =   date.substring(date.indexOf('/')+1 , date.lastIndexOf('/'));
		       document.getElementById(updatecontrol + "_tb_arryear").value =   date.substring(date.lastIndexOf('/') + 1+2 ,date.length);
		  }		  
            		  		  
		 document.getElementById(updatecontrol + "_tb_prm").value = document.getElementById(linkbuttonid + "_hf_crm").value;
		 document.getElementById(updatecontrol + "_hf_courtroomno").value = document.getElementById(linkbuttonid + "_hf_crm").value;
		 if (isnew != "1")
		 {
		    document.getElementById(updatecontrol + "_tb_pfineamount").value = document.getElementById(linkbuttonid+"_hf_fineamount").value;
		    document.getElementById(updatecontrol + "_tb_pbondamount").value = document.getElementById(linkbuttonid+"_hf_bondamount").value;
		 }
	     document.getElementById(updatecontrol + "_hf_violationid").value = document.getElementById(linkbuttonid+"_hf_violationid").value;
	     document.getElementById(updatecontrol + "_hf_ticketviolationid").value = document.getElementById(linkbuttonid+"_hf_ticketviolationid").value;
		 		 	 
		 // Display Court Time 
	     document.getElementById(updatecontrol + "_ddl_pcrttime").value =   document.getElementById(linkbuttonid + "_hf_ctime").value;
	     document.getElementById(updatecontrol + "_hf_courttime").value =   document.getElementById(linkbuttonid + "_hf_ctime").value;
		 document.getElementById(updatecontrol + "_tb_pvdesc").value =   document.getElementById(linkbuttonid + "_hf_VDesc").value;
		 		 
		 document.getElementById(updatecontrol + "_ddl_pstatus").value = document.getElementById(linkbuttonid + "_hf_courtviolationstatusid").value;
		 document.getElementById(updatecontrol + "_hv_viostatus").value = document.getElementById(linkbuttonid + "_hf_courtviolationstatusid").value;
		 document.getElementById(updatecontrol + "_hf_status").value = document.getElementById(linkbuttonid + "_hf_courtviolationstatusid").value;
		 document.getElementById(updatecontrol + "_tb_pstatus").value =   document.getElementById(linkbuttonid + "_hf_courtstatus").value;
          
         if (document.getElementById(updatecontrol + "_tb_pstatus").value == "")
		 {
		    document.getElementById(updatecontrol + "_tb_pstatus").value = "N/A";
		    document.getElementById(updatecontrol + "_hf_status").value = "0";
		    document.getElementById(updatecontrol + "_ddl_pstatus").selectedIndex = 0;
		    
		 }				 
        
				
		 // Filling Hidden Fields
		 //ozair 5171 11/19/2008 setting auto category id
		 document.getElementById(updatecontrol + "_hf_AutoCategoryID").value =   document.getElementById(linkbuttonid + "_hf_categoryid").value;
		 //Waqas 6666 10/29/2009 Make it case sensitives to work proper for all other exploreres
		 document.getElementById(updatecontrol + "_hf_AutoStatusID").value =   document.getElementById(linkbuttonid + "_hf_courtviolationstatusid").value;
		 document.getElementById(updatecontrol + "_hf_AutoNo").value =   document.getElementById(linkbuttonid + "_hf_crm").value;
		 document.getElementById(updatecontrol + "_hf_AutoCourtDate").value =   document.getElementById(linkbuttonid + "_hf_courtdate").value;
		 document.getElementById(updatecontrol + "_hf_AutoTime").value =   document.getElementById(linkbuttonid + "_hf_ctime").value;
		 document.getElementById(updatecontrol + "_hf_ticketid").value =   document.getElementById(linkbuttonid + "_hf_ticketid").value;


		 //Zeeshan Ahmed 3757 04/17/2008
		 if (document.getElementById(linkbuttonid + "_hf_hasNOS") != null)
		    document.getElementById(updatecontrol + "_hf_hasNOS").value = document.getElementById(linkbuttonid + "_hf_hasNOS").value;
		 
		 document.getElementById(updatecontrol + "_hf_UpdateNOS").value = "0";
		 // Hiding Violation DropDownList   
		 document.getElementById(updatecontrol + "_ddl_pvdesc").style.display='none';
		 document.getElementById(updatecontrol + "_ddl_pstatus").style.display='none';
		 document.getElementById(updatecontrol + "_tb_pvdesc").style.display='block';
		 document.getElementById(updatecontrol + "_tb_pstatus").style.display='block';
		 document.getElementById(updatecontrol + "_hf_vstyle").value='none';
		 document.getElementById(updatecontrol + "_hf_sstyle").value='none';
		 document.getElementById(updatecontrol + "_hf_cstyle").value='block';
		 		  
	        var  bondflag = document.getElementById(linkbuttonid+"_hf_bondflag").value;
		   
		    if ( bondflag == "YES" || bondflag=="yes")
		       document.getElementById(updatecontrol + "_rbl_bondflag_0").checked= true;
		    else
		      document.getElementById(updatecontrol + "_rbl_bondflag_1").checked = true;
		    
		    //Selects Plan
		    var srch = document.getElementById(linkbuttonid + "_hf_priceplan").value;
		    
		    //Added By Zeeshan Ahmed On 1/25/2008
		    document.getElementById(updatecontrol + "_hf_planid").value = srch;
		    
		    var ddl = document.getElementById(updatecontrol + "_ddl_ppriceplan")
	        selectValues(ddl,srch);
	        
	        //Selects CourtName
	        var getPricePlan = false;
        
	        var srch = document.getElementById(linkbuttonid + "_hf_courtid").value;
	        var courtLoc = document.getElementById(updatecontrol + "_hf_courtloc");
            	        
	        //Added By Zeeshan Ahmed To Fixed Criminal Court Lock Issue
	        courtLoc.value = document.getElementById(linkbuttonid + "_hf_courtid").value;
	        
	        //ozair 4442 07/22/2008 ALR Courts(Houston, Fort bend, Conroe)
	        //Nasir 5310 12/29/2008 check the court is ALR Court 
	        //if(srch == "3047" || srch == "3048" || srch == "3070" || srch == "3079")
	        //Waqas 5864 07/17/2009 Fixed Charge Level issue
	        if(document.getElementById(updatecontrol +"_hf_IsALRProcess").value == "True")
		   	{   
		   	   document.getElementById(updatecontrol +"_tr_fineamount").style.display="block";
		       document.getElementById(updatecontrol +"_tr_chargelevel").style.display="none"
		   	}
		   	
		   	
            //Added by Ozair For Task 2515 on 01/02/2008
            if(srch != "3001" && srch != "3002" && srch != "3003" && CaseType == "1")
            {
                document.getElementById(updatecontrol +"_tr_bondamount").style.display="block";
            }
            else
            {
               document.getElementById(updatecontrol +"_tr_bondamount").style.display="none";
            }
            //end
                        	       
		   	var hfCourtId= document.getElementById(updatecontrol + "_hf_courtid")
		    
		    if (hfCourtId.value != srch)
            getPricePlan = true;
		   	
		    
		    hfCourtId.value = srch;
		   			   	
		   	var ddl = document.getElementById(updatecontrol + "_ddl_pcrtloc");
	     
	        ddl.selectedIndex = 0 
		   	selectValues(ddl,srch);
		        
	        if (ddl.selectedIndex != 0)
	        {
		        document.getElementById(updatecontrol + "_ddl_pcrtloc").style.display = "block";		    
		        document.getElementById(updatecontrol + "_lbl_pcname").style.display = "none";	
		        document.getElementById(updatecontrol + "_imgbtncourt").style.display = "none";	    
		        document.getElementById(updatecontrol + "_hf_cstyle").value='block';
		    }
		    else
		    {
		         document.getElementById(updatecontrol + "_ddl_pcrtloc").style.display = "none";		    
		         document.getElementById(updatecontrol + "_lbl_pcname").style.display = "block";		
		         document.getElementById(updatecontrol + "_lbl_pcname").innerText = document.getElementById(linkbuttonid + "_hf_courtloc").value;
		         document.getElementById(updatecontrol + "_hf_pcname").innerText = document.getElementById(linkbuttonid + "_hf_courtloc").value;
		         document.getElementById(updatecontrol + "_imgbtncourt").style.display = "block";
		          document.getElementById(updatecontrol + "_hf_cstyle").value='none';
		    }
	
	        //Select Violation
	        var srch = document.getElementById(linkbuttonid + "_hf_violationid").value;
	        
		   	var ddl = document.getElementById(updatecontrol + "_ddl_pvdesc")
		   	selectValues(ddl,srch);
	        document.getElementById(updatecontrol + "_hf_violationid").value = srch;
	        // Select Status
	    
	        if(document.getElementById(updatecontrol + "_ddl_pcrtloc").value =="3061" || document.getElementById(updatecontrol + "_hf_courtid").value =="3061")
		    {
		        document.getElementById(updatecontrol + "_tr_oscar").style.display = "block";
		        document.getElementById(updatecontrol + "_tb_oscare_court").value = document.getElementById(linkbuttonid + "_hf_OscareCourt").value;
		    }
		    else
		    {
		         document.getElementById(updatecontrol + "_tb_oscare_court").value ="";
		         document.getElementById(updatecontrol + "_tr_oscar").style.display = "none";		         
		    }
                    
            if(document.getElementById(updatecontrol + "_ddl_pcrtloc").value =="3037")
            {
                document.getElementById(updatecontrol + "_hf_violationcheck").value = 1;

                // tahir 5117 11/11/2008 hide Level & CDI ....
                if (document.getElementById(updatecontrol +"_hf_violationid").value == "18534")
                {
                    document.getElementById(updatecontrol +"_tr_cdi").style.display="none";
                }
                else
                {
                    document.getElementById(updatecontrol +"_tr_cdi").style.display="block";
                }
                document.getElementById(updatecontrol + "_ddl_cdi").value = document.getElementById(linkbuttonid + "_hf_cdi").value;
            }
	        else
	        {
	            document.getElementById(updatecontrol + "_hf_violationcheck").value = 0;
	            document.getElementById(updatecontrol +"_tr_cdi").style.display="none";
	            document.getElementById(updatecontrol + "_ddl_cdi").selectedIndex = 0;
	        }
            
              //Zeeshan Ahmed 4163 06/02/2008 Set Missed Court Status Type.
		      var StatusId =  document.getElementById(linkbuttonid + "_hf_courtviolationstatusid").value;
		      
		      document.getElementById(updatecontrol + "_hf_missedcourttype").value = document.getElementById(linkbuttonid+"_hf_missedcourttype").value;
		      var MissedCourtType =document.getElementById(updatecontrol + "_hf_missedcourttype").value;
    		   
    		  if ( StatusId=="105")
    		    document.getElementById(updatecontrol + "_trMissedCourt").style.display = "block";	
    		  	    		  	
		      if ( MissedCourtType == "1" && StatusId=="105")
		      {
		        document.getElementById(updatecontrol + "_rbtnNoAction").checked = true;
		        document.getElementById(updatecontrol + "_rbtnPledOut").checked = false;
		      }
		      else if (MissedCourtType == "2" && StatusId=="105")
		      {
		         document.getElementById(updatecontrol + "_rbtnNoAction").checked = false;
		         document.getElementById(updatecontrol + "_rbtnPledOut").checked = true;
		      }		        	        
	        
	        if ( isnew == "0")
	          document.getElementById(updatecontrol + "_hf_newviolation").value = 0;
	        else
             document.getElementById(updatecontrol + "_hf_newviolation").value = 1;            
             
  	        setpopuplocation(updatecontrol);  
            
	        disableTab("pnl_violation");
	        
	        
	        //Added By Zeeshan Ahmed
	        //Get Price Plan If Court Location Changes
	        if (getPricePlan || (OldCaseType !=CaseType )) setTimeout('__doPostBack(\'UpdateViolationSRV1$lbtn_close\',\'\')', 0);
	        
		    return false;    
		}
		
	
	// Set Popup Control Location
    function setpopuplocation(updatecontrol)
		{
	        var top  = 400;
	        var left = 300;
	        var height = document.body.offsetHeight;
	        var width  = document.body.offsetWidth
            
            // Setting Panel Location
//	        if ( width > 1100 || width <= 1280) left = 575;
//	        if ( width < 1100) left = 500;
//    	    
		    // Setting popup display
	        document.getElementById("pnl_violation").style.top =top+'px';
		    document.getElementById("pnl_violation").style.left =left+'px';
    						
		    if (  document.body.offsetHeight > document.body.scrollHeight )
			    document.getElementById(updatecontrol + "_Disable").style.height = document.body.offsetHeight;
		    else
		    document.getElementById(updatecontrol + "_Disable").style.height = document.body.scrollHeight ;
            
            document.getElementById(updatecontrol + "_Disable").style.position = "absolute";    			
		    document.getElementById(updatecontrol + "_Disable").style.width = document.body.offsetWidth;
		    document.getElementById(updatecontrol + "_Disable").style.display = 'block'
            document.getElementById("pnl_violation").style.position = "absolute";   
		    document.getElementById("pnl_violation").style.display = 'block'
		   
		}

   // Update Violation Validation
   
    function submitPopup(updatecontrol,gridname,divname,ctrlstyl)
		{
		    var oldStatus = document.getElementById(updatecontrol + "_hf_oldstatus").value;
            var newStatus = document.getElementById(updatecontrol + "_ddl_pstatus").value;
		    var CaseType = document.getElementById(updatecontrol + "_hf_casetype").value; 
		    
			        //Afaq 8311 09/30/2010 if profile has any bond violation.
			        if (parseInt(document.getElementById(updatecontrol + "_hf_statusCount").value)>0 )
			        {    // if profile has only one Bond violation and Current status is Bond and selected status is Not Bond return True.
			            if (parseInt(document.getElementById(updatecontrol + "_hf_statusCount").value)==1 && oldStatus=="135" && newStatus!="135")
			            {
			                
			            }    
			            // If bond flag is Yes return true.
			            else if (document.getElementById(updatecontrol + "_rbl_bondflag_0").checked== true )
			            {}
			            //If current status not bond and update all check box is checked return true.
			            else if (newStatus!="135" &&  document.getElementById(updatecontrol + "_cb_updateall").checked== true)
			            {}
			            else
			            {
			                alert("You can�t select NO for the bond question when �BOND� is selected as case status.");
			                return false;
			            }
			        }
			        // If selected status is Bond and bond flag is false return false.
			        else if (newStatus ==135 && document.getElementById(updatecontrol + "_rbl_bondflag_0").checked== false)
			        {
			            alert("You can�t select NO for the bond question when �BOND� is selected as case status.");
			            return false;
			        }
			      if (document.getElementById(updatecontrol + "_ddl_pcrtloc").style.display != "none")
			      {
			        if ( document.getElementById(updatecontrol + "_ddl_pcrtloc").selectedIndex == 0 )
			        {
			            alert("Please select court location.");
			            document.getElementById(updatecontrol + "_ddl_pcrtloc").focus();
			            return false;
        			
			        }
			        if(document.getElementById(updatecontrol + "_ddl_pcrtloc").value =="3061")
			        {
			            if(document.getElementById(updatecontrol + "_tb_oscare_court").value =="")
			            {
			                alert("Please enter Oscare Court Detail.");
			                document.getElementById(updatecontrol + "_tb_oscare_court").focus();
			                return false;
			            }
			        }
			        
			        //Waqas Javed 5173 01/20/2008 updating covering firm to OSCAR
			        if ( (document.getElementById(updatecontrol + "_hf_oldcourtid").value != document.getElementById(updatecontrol + "_ddl_pcrtloc").value)
			        && document.getElementById(updatecontrol + "_ddl_pcrtloc").value == "3061" )
		             {	
		                alert("The coverage attorney for this case will be set to OSCAR TREVINO"); 
		             }
		             
		             //Waqas Javed 5173 01/20/2008 updating covering from OSCAR to SULL
			        if ( (document.getElementById(updatecontrol + "_hf_oldcourtid").value != document.getElementById(updatecontrol + "_ddl_pcrtloc").value)
			        && document.getElementById(updatecontrol + "_hf_oldcourtid").value == "3061" )
		             {	
		                alert("The coverage attorney for this case will be set to SULLO & SULLO"); 
		             }		
		            
			       
			       document.getElementById(updatecontrol + "_hf_courtid").value =document.getElementById(updatecontrol + "_ddl_pcrtloc").value;
			
			}
			// noufil 4747 09/09/2008 Restrict User to enter court number in range to 1 to 4 for court houuse Harris Co Family Crt (AG)
			var ddl_new = document.getElementById(updatecontrol + "_ddl_pcrtloc");
            if ((CaseType=="3" || CaseType=="4") && ddl_new.value == 3077)
            {            
                var courtnumber =document.getElementById(updatecontrol + "_tb_prm").value;
                //Yasir Kamal 5748 04/14/2009 allow three numeric characters for family law cases
                if (courtnumber.length <=3)
                {
                    for ( i = 0 ; i < courtnumber.length ; i++)
                    {                       
           	           var asciicode =  courtnumber.charCodeAt(i);
                       if (! ((asciicode >= 48 && asciicode <=57)))
                       {
                          alert("Only 3 numeric characters are allowed for court number");
                          return false;
                       }
                    }
                }
                else
                {
                    alert("Only 3 numeric characters allowed for court number");
                    return false;
                }
                //5748 end
            } 
                      
		    //Validating Ticket Number
			var casenumber = document.getElementById(updatecontrol + "_tb_pticketno");
			var refcasenumber = casenumber.value;			
			var PMCCourt = (document.getElementById(updatecontrol + "_ddl_pcrtloc").style.display != "none")?document.getElementById(updatecontrol + "_ddl_pcrtloc"):document.getElementById(updatecontrol + "_hf_courtid");						
			if (document.getElementById(updatecontrol +"_tr_ticketno").style.display=="block")
			{
			//Asad Ali 8052 11/03/2010 Allow general validation for PMC as per requirement
			//Muhammad Muneer 8276 9/14/2010 added the new functionality to validate Cause Number 
			//if(parseInt(PMCCourt.value,10)!=3004)
			//{
			    if (refcasenumber == "" ) 
			    {
				    alert("Invalid Ticket Number.");
				    casenumber.focus();
				    return false;
			    }
			    if (refcasenumber.length<5)
			    {
				    alert("Ticket Number must be greater then four characters.");
				    casenumber.focus();
				    return false;
			    }
			 //}
			    if (casenumber.value.indexOf(" ") != -1 ) 
			    {
				    alert("Please make sure that there are no spaces in the ticketnumber input box"); 
				    casenumber.focus();
				    return false;
			    }
			    
			    
			    //Asad Ali 8502 11/03/2010 Remove All validations related to PMC 
                //if((parseInt(PMCCourt.value,10)==3004) && (refcasenumber.substring(0,3).toUpperCase() == "PMC")) //Sabir Khan 7181 01/05/2010
                //{
                //alert("Please provide a valid ticket number for PMC");
                //casenumber.focus();
                //return false;
                //}
                    //Sabir Khan 8058 07/23/2010 Check for valid PMC Ticket Number
			        //var PMCCausenumber =document.getElementById(updatecontrol + "_tb_causeno");
			        //var PMCCause = PMCCausenumber.value;
//			        if()
//			        {
//			            alert("PMC Cause Number should be only 7 characters");
//			            PMCCausenumber.focus();
//			            return false; 
//			        }
//			        else
//			        {
			            //Sabir Khan 8058 07/23/2010 Check for valid PMC Ticket Number
//			            if(((parseInt(PMCCourt.value,10)==3004)&&(PMCCause.length != 7))&&((parseInt(PMCCourt.value,10)==3004)&&((refcasenumber.substring(0,1).toUpperCase() == "6")||(refcasenumber.substring(0,1).toUpperCase() == "7")||(refcasenumber.substring(0,1).toUpperCase() == "E"))))
                     //Muhammad Muneer 8276 9/14/2010 added the new functionality to validate Cause Number
                    //var PMCCheck ="false";
//                        if((parseInt(PMCCourt.value,10)==3004) && (refcasenumber.length != ""))
//                        {
//                            if((parseInt(PMCCourt.value,10)==3004) && ((refcasenumber.substring(0,1).toUpperCase() != "6" && refcasenumber.substring(0,1).toUpperCase() != "7" && refcasenumber.substring(0,1).toUpperCase() != "E")))
//                            {
//                              //Muhammad Muneer 8276 9/14/2010 added point 3 in alert message
//                                alert(" Please provide either a valid ticket number or cause number for PMC according to the following rules:\n \n Cause Number length must be exactly 7 characters long and it should start with 1.\n \n Ticket number: \n 1. If start with 6 or 7 then length must be exactly 6 characters long \n 2. If start with E then length must be exactly 8 characters long. \n 3. If Cause Number is valid, then Ticket Number should be valid or it will be blank");
//                                PMCCheck = "true";
//                                return false;
//                            }
//                            else if((parseInt(PMCCourt.value,10)==3004) && (PMCCause.length != 7) && ((refcasenumber.substring(0,1).toUpperCase() == "6" || refcasenumber.substring(0,1).toUpperCase() == "7") && (refcasenumber.length != 6)))
//                            {
//                                 //Muhammad Muneer 8276 9/14/2010 added point 3 in alert message
//                                alert(" Please provide either a valid ticket number or cause number for PMC according to the following rules:\n \n Cause Number length must be exactly 7 characters long and it should start with 1.\n \n Ticket number: \n 1. If start with 6 or 7 then length must be exactly 6 characters long \n 2. If start with E then length must be exactly 8 characters long.\n 3. If Cause Number is valid, then Ticket Number should be valid or it will be blank");
//                                PMCCheck = "true";
//                                return false;
//                            }
//                            else if((parseInt(PMCCourt.value,10)==3004) && (PMCCause.length == 7) && ((refcasenumber.substring(0,1).toUpperCase() == "6" || refcasenumber.substring(0,1).toUpperCase() == "7") && (refcasenumber.length != 6)))
//                            {
//                                alert(" Please provide either a valid ticket number or cause number for PMC according to the following rules:\n \n Cause Number length must be exactly 7 characters long and it should start with 1.\n \n Ticket number: \n 1. If start with 6 or 7 then length must be exactly 6 characters long \n 2. If start with E then length must be exactly 8 characters long.\n 3. If Cause Number is valid, then Ticket Number should be valid or it will be blank");
//                               PMCCheck = "true";
//                                return false;
//                            }
//                            else
//                            if((parseInt(PMCCourt.value,10)==3004) && ((refcasenumber.substring(0,1).toUpperCase() == "E") && (refcasenumber.length != 8)))
//                            {
//                                alert(" Please provide either a valid ticket number or cause number for PMC according to the following rules:\n \n Cause Number length must be exactly 7 characters long and it should start with 1.\n \n Ticket number: \n 1. If start with 6 or 7 then length must be exactly 6 characters long \n 2. If start with E then length must be exactly 8 characters long.\n 3. If Cause Number is valid, then Ticket Number should be valid or it will be blank");
//                                PMCCheck = "true";
//                                return false;
//                            }
//                        }
                        
//                        //Muhammad Muneer 8276 9/14/2010 added the new functionality to validate Cause Number 
//                        if(PMCCheck =="true")
//                        {
//                        if((parseInt(PMCCourt.value,10)==3004) && (PMCCause.length != ""))
//                        {
//                        if((PMCCause.length != 7)||(PMCCause.substring(0,1).toUpperCase() != "1"))
//                        {
//                            alert(" Please provide either a valid ticket number or cause number for PMC according to the following rules:\n \n Cause Number length must be exactly 7 characters long and it should start with 1.\n \n Ticket number: \n 1. If start with 6 or 7 then length must be exactly 6 characters long \n 2. If start with E then length must be exactly 8 characters long.\n 3. If Cause Number is valid, then Ticket Number should be valid or it will be blank");
//                            return false;
//                        }
//                        }
//                        }
//                        else
//                        {
//                        
//                        }
//                        
//                        if((parseInt(PMCCourt.value,10)==3004) && (PMCCause.length != "")&&(refcasenumber.length == ""))
//                        {
//                        if((PMCCause.length != 7)||(PMCCause.substring(0,1).toUpperCase() != "1"))
//                        {
//                            alert(" Please provide either a valid ticket number or cause number for PMC according to the following rules:\n \n Cause Number length must be exactly 7 characters long and it should start with 1.\n \n Ticket number: \n 1. If start with 6 or 7 then length must be exactly 6 characters long \n 2. If start with E then length must be exactly 8 characters long.\n 3. If Cause Number is valid, then Ticket Number should be valid or it will be blank");
//                            return false;
//                        }
//                        }
//                        
//                       //Muhammad Muneer 8276 9/14/2010 added the new functionality to validate Cause Number 
//                        if(parseInt(PMCCourt.value,10)==3004)
//                        {
//                        if((PMCCause.length == "") && (refcasenumber.length == ""))
//                        {
//                            alert(" Please provide either a valid ticket number or cause number for PMC according to the following rules:\n \n Cause Number length must be exactly 7 characters long and it should start with 1.\n \n Ticket number: \n 1. If start with 6 or 7 then length must be exactly 6 characters long \n 2. If start with E then length must be exactly 8 characters long.\n 3. If Cause Number is valid, then Ticket Number should be valid or it will be blank");
//                            return false;
//                        }
//                        }
                        
//////////////////////////                        if((parseInt(PMCCourt.value,10)==3004)&&((PMCCause.length != 7)||((refcasenumber.substring(0,1).toUpperCase() == "6")||(refcasenumber.substring(0,1).toUpperCase() == "7")||(refcasenumber.substring(0,1).toUpperCase() == "E"))))
//////////////////////////			            {
//////////////////////////			                if((parseInt(PMCCourt.value,10)==3004) &&((PMCCause.length != 7) && ((refcasenumber.substring(0,1).toUpperCase() == "6" || refcasenumber.substring(0,1).toUpperCase() == "7") && (refcasenumber.length != 6))))
//////////////////////////			                {
//////////////////////////			                    alert("PMC Cause Number should be only 7 characters");
//////////////////////////			                    PMCCausenumber.focus();
//////////////////////////			                    return false; 
//////////////////////////			                }
//////////////////////////			                else
//////////////////////////			                {			                
//////////////////////////			                    if((parseInt(PMCCourt.value,10)==3004) && ((refcasenumber.substring(0,1).toUpperCase() == "6" || refcasenumber.substring(0,1).toUpperCase() == "7") && (refcasenumber.length != 6)))
//////////////////////////			                    {
//////////////////////////			                        alert("PMC Ticket Number should be according to the following business logic: \n 1. When start with 6 or 7 then length should be 6 character \n 2. When start with E then length should be 8 characters");
//////////////////////////			                        casenumber.focus();
//////////////////////////			                        return false;
//////////////////////////			                    }
//////////////////////////			    
//////////////////////////			                    //Sabir Khan 8058 07/23/2010 Check for valid PMC Ticket Number
//////////////////////////			                    if((parseInt(PMCCourt.value,10)==3004) && ((refcasenumber.substring(0,1).toUpperCase() == "E") && (refcasenumber.length != 8)))
//////////////////////////			                    {
//////////////////////////			                        alert("PMC Ticket Number should be according to the following business logic: \n 1. When start with 6 or 7 then length should be 6 character \n 2. When start with E then length should be 8 characters");
//////////////////////////			                        casenumber.focus();
//////////////////////////			                        return false;
//////////////////////////			                    }
//////////////////////////			              	    
//////////////////////////			                }
//////////////////////////			             }
//			            else if(parseInt(PMCCourt.value,10)==3004)
//			                   {
//			                    alert("PMC Ticket Number should be according to the following business logic: \n 1. When start with 6 or 7 then length should be 6 character \n 2. When start with E then length should be 8 characters");
//			                    casenumber.focus();
//			                    return false;
//			                   }
			       //}
			    
			        
			    
			}
		    
		    // Validating Cause Number
		    //ozair 5171 11/19/2008 triming court number
		    var courtnum = document.getElementById(updatecontrol + "_tb_prm").value.trim();
		   
		    if ( courtnum.length > 1)
		     {
		       if ( courtnum.substring(0,1) == "0")
		       {
		          courtnum = courtnum.substring(1);
		       }
		    }
   		    //Zeeshan Ahmed 4163 06/10/2008 Fixed Missed Court Status Bug		    
			var datetype =(document.getElementById(updatecontrol + "_ddl_pstatus").style.display =="block")?document.getElementById(updatecontrol + "_ddl_pstatus").value: document.getElementById( updatecontrol +"_hf_status").value; 
		    selectedindex = document.getElementById(updatecontrol + "_ddl_pstatus").selectedIndex;
			var strdatetype =document.getElementById(updatecontrol + "_ddl_pstatus").value;
			var varhours = document.getElementById(updatecontrol + "_ddl_pcrttime").value;
			
			//Sabir Khan 8930 02/17/2011 Stop updating violation when setting to pre trial for pasadena.
			if((parseInt(PMCCourt.value,10)==3004) && ((datetype == "101") && (oldStatus != "101")) && (document.getElementById('<%=hf_IsDispositionpage.ClientID %>').value == "0"))
			{
			    alert("Please fax a LOR to Pasadena Municipal Court. The system will automatically set the status of this case once the LOR has been confirmed.");
			    return false;
			}
			
			//Sabir Khan 6264 08/03/2009 check court time for family law cases...
			if(CaseType == "4" && varhours != "8:00 AM")			
			{
			    alert("Court time must be 8:00 AM for Family Cases.");
			    document.getElementById(updatecontrol + "_ddl_pcrttime").focus();
			    return false;
			}
			//ozair 3901 on 04/30/2008 included the trim method here to avoid bug related to 3825
			var strdate = trim(document.getElementById(updatecontrol + "_tb_pmonth").value) + "/" + trim(document.getElementById(updatecontrol + "_tb_pday").value) + "/20" + trim(document.getElementById(updatecontrol + "_tb_pyear").value);
			var strdatewithtime = strdate + " " + varhours;
			//ozair 5171 11/19/2008 setting category ids
			var currentstatuscategoryid=document.getElementById(updatecontrol + "_ddl_StatusCategory").value;
			var courtstatusautocategoryid = document.getElementById(updatecontrol + "_hf_AutoCategoryID").value;
			var courtstatusauto = document.getElementById(updatecontrol + "_hf_AutoStatusID").value;
			var courtnumauto =document.getElementById(updatecontrol + "_hf_AutoNo").value.trim();
			var courtdateauto = document.getElementById(updatecontrol + "_hf_AutoCourtDate").value + " " + document.getElementById(updatecontrol + "_hf_AutoTime").value;
			//Zeeshan Ahmed 3724 05/13/2008
			var ArrestDate = trim(document.getElementById(updatecontrol + "_tb_arrmonth").value) + "/" + trim(document.getElementById(updatecontrol + "_tb_arrday").value) + "/20" + trim(document.getElementById(updatecontrol + "_tb_arryear").value);
			//Violation Description Check
			
			var ddl_vdesc =document.getElementById(updatecontrol + "_ddl_pvdesc"); 
			var hf_violationid =document.getElementById(updatecontrol+"_hf_violationid");	
			var lbl_vdesc = document.getElementById(updatecontrol + "_tb_pvdesc"); 
			
			if(ddl_vdesc.style.display != 'none')
			{			   
			    var ddltext=ddl_vdesc.options[ddl_vdesc.selectedIndex].text;
				if (ddl_vdesc.value==0)
				{
					alert("Please Select ViolationDescription.");
					ddl_vdesc.focus();
					return false;
				}
				if(ddltext.substring(0,3)== "---")
				{ 
				    alert("Please Select ViolationDescription.");
					ddl_vdesc.focus();
					return false;
				}
					   
			   hf_violationid.value = ddl_vdesc.value;
			   
			   
			    
			}
		    
		    if ( hf_violationid.value  == "0" ) 
		    {
		    
		    alert("Please Select ViolationDescription.");
			
			if ( ddl_vdesc.style.display == "none")
			{
                DisplayToggleP(ddl_vdesc.id,lbl_vdesc.id,ctrlstyl);
			    ddl_vdesc.focus();
			}
			else
			ddl_vdesc.focus();
			
			return false;
		    }
		    
		    //Zeeshan Ahmed 3535 04/08/2008
		    //FTA Modifications 
		    //Do Not Allow Multiple FTA In Case For Inside Courts
		    var selectedCourt = (document.getElementById(updatecontrol + "_ddl_pcrtloc").style.display != "none")?document.getElementById(updatecontrol + "_ddl_pcrtloc"):document.getElementById(updatecontrol + "_hf_courtid");						
		    if (selectedCourt.value =="3001" ||  selectedCourt.value =="3002" || selectedCourt.value =="3003"|| selectedCourt.value =="3075")
		    {
		       var isNewViolation = (document.getElementById(updatecontrol + "_hf_newviolation").value =="1") ? true:false;
		       var hasFTAViolations = document.getElementById(updatecontrol + "_hf_hasFTAViolations").value;
		       var isFTAViolation = document.getElementById(updatecontrol + "_hf_isFTAViolation").value;
               
               //Do Not Allow New FTA Violation To Add If Case has already FTA violations Or
               //Do Not Allow Old Violation To FTA Case already have FTA violations             
               //Zeeshan Ahmed 3895 04/29/2008
               //if ( (isNewViolation && hasFTAViolations == "1" && hf_violationid.value == "11189") || (!isNewViolation && isFTAViolation=="0" && hasFTAViolations=="1" &&  hf_violationid.value == "11189" ))
               //{    
               //   alert("Case already has a FTA violaton. Please select different violation");
               //  
               //   if ( ddl_vdesc.style.display == "none")
               //   DisplayToggleP(ddl_vdesc.id,lbl_vdesc.id,ctrlstyl);
               // 
               //   ddl_vdesc.focus();
               //   return false;
		       //}
               //If Bond Flag Selected
		       var bond =document.getElementById(updatecontrol + "_rbl_bondflag_0");
		       
		       if (bond.checked)
		       {
		           if ( (isNewViolation && hasFTAViolations =="0" && hf_violationid.value != "11189") || (!isNewViolation && hasFTAViolations =="0" && isFTAViolation == "0" && hf_violationid.value != "11189" ))
		           {
		               alert("To change bond flag to Yes. Case must have a FTA ticket.");
		               bond.focus();
		               return false;
		           }
		       }
		    }
            
		    //Verify Cause NUmber for HCJP Courts
			var causenum=document.getElementById(updatecontrol + "_tb_causeno");
			if(parseInt(selectedCourt.value,10)>=3007 && parseInt(selectedCourt.value,10)<=3022)
			{
			  if ( causenum.value != "" )
			  {
			        if(causenum.value.length != 12)
			        {
			            alert('Please Enter Cause number for HCJP courts exactly 12 characters');
			            return(false);
			        }
    			    //Sabir Khan 6674 09/30/2009 allow CV for HCJP courts...
			        if(causenum.value.substring(0,2).toUpperCase()  != 'TR' && causenum.value.substring(0,2).toUpperCase()  != 'BC' && causenum.value.substring(0,2).toUpperCase()  != 'CR' && causenum.value.substring(0,2).toUpperCase()  != 'CV')
			        {
			            alert('Please Enter Cause number for HCJP courts Beginning  with \'TR\',\'BC\',\'CR\' or \'CV\'');
			            return(false);
			        }
			   }
			}
		    
		    
		    // Check Charge Level 
		    
		  
		    
		    if (  document.getElementById(updatecontrol + "_tr_chargelevel").style.display != "none")
		    {//a;
		        var chargelevel =  document.getElementById(updatecontrol + "_ddl_pvlevel");
		        if ( chargelevel.value == "0")
		        {
		            alert("Please select charge level");
		            chargelevel.focus();
		            return false;
		        }
		        		        
		        //added for task 2171
		        document.getElementById(updatecontrol + "_hf_Level").value=chargelevel.value;
		    }		    
            
            if (  document.getElementById(updatecontrol + "_tr_cdi").style.display != "none")
		    {
		        var cdi =  document.getElementById(updatecontrol + "_ddl_cdi");
		        if ( cdi.value == "0")
		        {
		            alert("Please select charge level");
		            cdi.focus();
		            return false;
		        }
		    }
		    
		    // Arrest date
		    // Noufil 5819 05/14/2209 Dont allow blank arrest date on ALR hearing violation selection.
		    if (ddl_vdesc.value == "16159" && CaseType=="2")
		    {		   
		        var month = document.getElementById(updatecontrol + "_tb_arrmonth").value;
		        var day = document.getElementById(updatecontrol + "_tb_arrday").value;
		        var year = document.getElementById(updatecontrol + "_tb_arryear").value;
		        
		        if (month.length == 0 || day.length == 0 || year.length == 0)
		        {
		            alert ("Please specify arrest date.");
		            document.getElementById(updatecontrol + "_tb_arrmonth").focus();
		            return false;
		        }
		    }
		    
            		    		    
		    //Fine AMount Check
			var fineamount =  document.getElementById(updatecontrol + "_tb_pfineamount");

			if ( document.getElementById(updatecontrol + "_tr_fineamount").style.display != "none")
			{
			    if (fineamount.value=="") 
			    {
				    alert("Please enter Fine Amount."); 
				    fineamount.focus();
				    return false;
			    }
			    if (fineamount.value.indexOf(" ") != -1) 
			    {
				    alert("Please make sure that there are no spaces in the Fine Amount input box"); 
				    fineamount.focus();
				    return false;
			    }
			    if(isNaN(fineamount.value))
			    {
				    alert("Invalid Fine Amount.");
				    fineamount.focus();
				    return false;
			    }
			}
			//added by ozair on 01/29/2008 [if fine amount text box is not visible then set 0]
			else
			{
				fineamount.value=0;
			}
			//Bond AMount Check
			
			var bondamount = document.getElementById(updatecontrol + "_tb_pbondamount");
			//Added/modified by Ozair for Task 2723 on 01/29/2008
			if ( document.getElementById(updatecontrol + "_tr_bondamount").style.display != "none")
			{
		        if (bondamount.value=="") 
			    {
				    alert("Please enter Bond Amount."); 
				    bondamount.focus();
				    return false;
			    }
    			
			    if(isNaN(bondamount.value))
			    {
				    alert("Invalid Bond Amount.");
				    bondamount.focus();
				    return false;
			    }
			}
			//if bond amount text box is not visible then set 0
			else
			{
			    bondamount.value=0;
			}
			//end task 2723
			var ValidChars = "-";
			var Char;
			var sText=fineamount.value;
			
			for (i = 0; i < sText.length; i++) 
			{ 
				
				Char = sText.charAt(i); 
				if (ValidChars.indexOf(Char) != -1) 
				{
					alert("Invalid fine amount");
				    fineamount.focus();
					return false;
				}
			}
			var sText=bondamount.value;
			for (i = 0; i < sText.length; i++) 
			{ 
				
				Char = sText.charAt(i); 
				if (ValidChars.indexOf(Char) != -1) 
				{
					alert("Invalid bond amount");
					bondamount.focus();
					return false;
				}
			}
		    //Validate Arrest Date
		    //Zeeshan Ahmed 3924 05/13/2008 
		    //Farrukh 9376 07/15/2011 modified condition for object display check
            if ( document.getElementById(updatecontrol + "_tr_arrestdate").style.display != "none")
		    {
		    
		        var month = document.getElementById(updatecontrol + "_tb_arrmonth");
		        var day = document.getElementById(updatecontrol + "_tb_arrday");
		        var year = document.getElementById(updatecontrol + "_tb_arryear");
		         
		        //Muhammad Nadir Siddiqui 9236 05/04/2011 Added check on null arrest date in criminal case
                if (month.value == "" || day.value == "" || year.value == "") {
                    alert("Please enter arrest date.");
                    month.focus();
                    return false;
                }
            
		        
		        if ( ArrestDate != "//20")
		        {
		           
		        
		            if ( !validateCourtDate(month,day,year))
		            return false;
		            
		            if(!isvaliddate ("20"+ year.value, day.value, month.value ))
		            {
		                alert('Invalid arrest date entered.\nPlease enter date in MM/DD/YY format.');
		                month.focus();
		                return false;
		            }
		            
		            // tahir 4401 07/12/2008
		            // do not allow future arrest dates....
		            var dtNow = new Date();
		            var dtArrest = new Date();
		            dtArrest.setMonth(1 * month.value - 1 , day.value);
		            dtArrest.setYear("20"+ year.value);
		            if ( dtArrest > dtNow  )
		            {
		                alert('You can not enter future arrest date.');
		                month.focus();
		                return false;
		            }
		        }
		
		    
		    
		    }
		    
		    
		   		   	   
		    // Remaining Validation
		    var month = document.getElementById(updatecontrol + "_tb_pmonth");
		    var day = document.getElementById(updatecontrol + "_tb_pday");
		    var year = document.getElementById(updatecontrol + "_tb_pyear");

		    //Waqas 5864 07/30/2009 Check for ALR Arrest and court dates
		    if (ddl_vdesc.value == "16159" && CaseType == "2" && document.getElementById(updatecontrol + "_hf_activeflag").value != "1") 
		    {
		        if (month.value != "" && day.value != "" && year.value != "") {

		            var dtALRCourtDate = new Date();
		            var dtALRArrestDate = new Date();
		            var dtOldALRArrestDate = new Date();

		            var Arrestmonth = document.getElementById(updatecontrol + "_tb_arrmonth");
		            var Arrestday = document.getElementById(updatecontrol + "_tb_arrday");
		            var Arrestyear = document.getElementById(updatecontrol + "_tb_arryear");

		            dtALRArrestDate.setDate(Arrestday.value);
		            dtALRArrestDate.setMonth(Arrestmonth.value - 1);
		            dtALRArrestDate.setYear("20" + Arrestyear.value);
		            
		            dtOldALRArrestDate = new Date(dtALRArrestDate.toDateString());
		            dtALRArrestDate.setDate(dtALRArrestDate.getDate() + 15);

		            var weekDay = dtALRCourtDate.getDay();

		            if (weekDay == 0) {

		                dtALRArrestDate.setDate(Arrestday.value);
		                dtALRArrestDate.setMonth(Arrestmonth.value - 1);
		                dtALRArrestDate.setYear("20" + Arrestyear.value);

		                dtOldALRArrestDate = new Date(dtALRArrestDate.toDateString());
		                dtALRArrestDate.setDate(dtALRArrestDate.getDate() + 14);
		            }


		            dtALRCourtDate.setDate(day.value);
		            dtALRCourtDate.setMonth(month.value - 1);		            
		            dtALRCourtDate.setYear("20" + year.value);
		            

		            if (dtALRCourtDate > dtALRArrestDate || dtALRCourtDate < dtOldALRArrestDate) {
		                alert("Court Date must be within 15 days from arrest date.");
		                month.focus();
		                return false;
		            }

		        }
                          
	    
		    }
		    else 
		    {
		        if (month.value == "" || day.value == "" || year.value == "") {
		            alert("Please enter court date.");
		            month.focus();
		            return false;
		        }
		    }



		    //Waqas 5864 07/30/2009 Check for ALR Arrest and court dates
		    if (ddl_vdesc.value == "16159" && CaseType == "2" && document.getElementById(updatecontrol + "_hf_activeflag").value != "1") {

		        if (month.value == "" && day.value == "" && year.value == "") {

		            var dtALRArrestDate = new Date();

		            var Arrestmonth = document.getElementById(updatecontrol + "_tb_arrmonth");
		            var Arrestday = document.getElementById(updatecontrol + "_tb_arrday");
		            var Arrestyear = document.getElementById(updatecontrol + "_tb_arryear");

		            dtALRArrestDate.setDate(Arrestday.value);
		            dtALRArrestDate.setMonth(Arrestmonth.value - 1);		            
		            dtALRArrestDate.setYear("20" + Arrestyear.value);

		            dtALRArrestDate.setDate(dtALRArrestDate.getDate() + 15);

		            var weekDay = dtALRArrestDate.getDay();

		            if (weekDay == 0) {
		                dtALRArrestDate.setDate(Arrestday.value);
		                dtALRArrestDate.setMonth(Arrestmonth.value - 1);
		                dtALRArrestDate.setYear("20" + Arrestyear.value);

		                dtALRArrestDate.setDate(dtALRArrestDate.getDate() + 14);
		            }

		            var yy = new String(dtALRArrestDate.getYear());
		            var mon = dtALRArrestDate.getMonth();

		            var doyou = confirm("Court Date will be set to " + (parseInt(mon) + 1) + "/" + dtALRArrestDate.getDate() + "/20" + yy.substring(2) + ". Would you like to proceed? (OK = Yes   Cancel = No)");

		            if (doyou == false) {
		                month.focus();
		                return false;
		            }

		            month.value = dtALRArrestDate.getMonth() + 1;
		            day.value = dtALRArrestDate.getDate();
		            year.value = yy.substring(2);
		        }

		    }


		    if (month.value == "0" ) {
		        alert("Please enter valid month.");
		        month.focus();
		        return false;
		    }
		    
		    if(day.value == "0" )
		     {
		            alert("Please enter valid day.");
		            day.focus();
		            return false;
		     }
		     
		   
		    if (year.value =="0" )
		     {
		            alert("Please enter valid year.");
		           year.focus();
		            return false;
		     }
		     
		    if (isNaN(month.value) )
		     {
		            alert("Please enter valid month.");
		            month.focus();
		            return false;
		     }
		        
		     if (  month.value > 12 )
		     {
		            alert("Please enter valid month.");
		           month.focus();
		            return false;
		     }
		     
		     if (isNaN(day.value))
             {
	                alert("Please enter valid day.");
	                day.focus();
	                return false;
             }		     
			    
	        if (isNaN(year.value))
	        {
                alert("Please enter valid year.");
                year.focus();
                return false;
	        }
		   		   
		    //Zeeshan Ahmed 3825 04/23/2008
		    //TRIM COURT DATE
		    month.value  = trim(month.value);
		    day.value = trim( day.value);
		    year.value = trim (year.value)


		    
		    
		    
			if( isvaliddate ("20"+ year.value, day.value, month.value ) == false )
		    {
		    alert('Invalid court date entered.\nPlease enter date in MM/DD/YYYY format.');
		    month.focus();
		    return false;
		    }
		    
		    
		    //Status Check
		    
		    
		    var ddl_pstatus = document.getElementById( updatecontrol +"_ddl_pstatus");
		    var lbl_pstatus = document.getElementById( updatecontrol +"_tb_pstatus");
		    var hf_status = document.getElementById( updatecontrol +"_hf_status");
					
			if(ddl_pstatus.style.display != 'none')
			{
			    //var ddltext=ddl_pstatus.options[ddl_pstatus.selectedIndex].text;
				if (ddl_pstatus.value==0 ||ddl_pstatus.value==-1 )
				{
					alert("Please Select Case Status.");
					ddl_pstatus.focus();
					return false;
				}
			  hf_status.value = ddl_pstatus.value;
			}
					
			if (hf_status.value=="0")
			{
				alert("Please Select Case Status.");
							
				if (ddl_pstatus.style.display == "none")
				{
				   DisplayToggleP(ddl_pstatus.id,lbl_pstatus.id,ctrlstyl);
				   ddl_pstatus.focus();
						
				}
				else
				{					
					ddl_pstatus.focus();
				}
						
				return false;
			}
			
			
			
			//Zeeshan 3486 04/04/2008
		     var NoAction = document.getElementById(updatecontrol + "_rbtnNoAction"); 
		     var PledOut =  document.getElementById(updatecontrol + "_rbtnPledOut");
		     		     
		     if ( document.getElementById(updatecontrol + "_trMissedCourt").style.display != "none" )
		     {
		        if ( !NoAction.checked  && ! PledOut.checked)
		        {
		            alert("Please select missed court option");
		            NoAction.focus();
		            return false;
		        }  	 
		     }
		    
			if ( document.getElementById(updatecontrol + "_ddl_ppriceplan").selectedIndex == 0 )
			{
			    alert("Please select price plan.");
			    document.getElementById(updatecontrol + "_ddl_ppriceplan").focus();
			    return false;
			
			}
			
			
			// Added By Zeeshan Ahmed On 10th September 2007
			// If Status is Not Hire then does not perform other validations
			if ( ddl_pstatus.value == "239")
			{
			    document.getElementById(divname).style.display = "block";
		        document.getElementById(gridname).style.display = "none";
		        document.getElementById(divname).focus();
		        closepopup("pnl_violation",updatecontrol + "_Disable");
			    return true;						
			
			}
						
//			// Noufil 4896 10/04/2008 Restric user to select appropiate violation description for court 3077            
//            var CaseDisp=document.getElementById(updatecontrol + "_hfIsDispositionPage").value;
//           
//            if (selectedCourt.value=="3077")
//            {
//                if (CaseDisp==0 && !(datetype==101 || datetype==103 || datetype==26 || datetype==147))
//                {
//                    alert("Only 'Pre Trial', 'Judge', 'Jury' or 'Other' status is allow for this court location.");
//                    return false;
//                }
//                if (CaseDisp==1 && !(datetype==101 || datetype==103 || datetype==26 || datetype==147 || datetype==80))
//                {
//                    alert("Only 'Pre Trial', 'Judge', 'Jury', 'Other' or 'Disposed' status is allow for this court location.");
//                    return false;
//                }
//            }
					
			// Checking Violation
			
			var oldcourtid = document.getElementById(updatecontrol + "_hf_courtid").value;
			var newcourtid = document.getElementById(updatecontrol + "_ddl_pcrtloc").value;
			
			if ( oldcourtid == "3037" && newcourtid != oldcourtid)
			{	
			     alert("Please Select Another Violation for the court.");
			     return false;
			   
			}
			
			
			
			var courtid = 3001 ;
			courtid =  document.getElementById(updatecontrol + "_hf_courtid").value;
			//Check For Court Date
			var bflag=0;
			if(document.getElementById(updatecontrol + "_rbl_bondflag_0").checked)
			{
				bflag = 1;
			}
			else
			{
				bflag = 0;
			}
			if (bflag ==0 && datetype != 104 && datetype != 105 && datetype != 135)
			{

			    //Waqas  5864 07/30/2009 setting court date again
			    strdate = trim(document.getElementById(updatecontrol + "_tb_pmonth").value) + "/" + trim(document.getElementById(updatecontrol + "_tb_pday").value) + "/20" + trim(document.getElementById(updatecontrol + "_tb_pyear").value);
				if (chkdate(strdate)==false)
				{
					alert("Invalid Court Date.");
					month.focus();
					return false;			
				}
			}
			
			var samedateflag = false;
			
			if (DateDiff(strdatewithtime, courtdateauto) == 0) 
			{
				samedateflag  = true;
			}
		
		
		//Zeeshan 4613 08/13/2008 Add Trial Status Validations
		//If Status Is Jury , Judge , Pretrial , Juv - Jury Trial , Juv- Judge Trial
		var dtCourtDate = new Date();
        dtCourtDate.setMonth(1 * month.value - 1 , day.value);
        dtCourtDate.setYear("20"+ year.value);
        var weekDay = dtCourtDate.getDay();
		
		if ( datetype == "101" ||  datetype == "26" ||  datetype == "103" || datetype == "256" || datetype == "257" )
		{
    		
            //If Saturday
            if ( weekDay == 6)
            {
               if ( datetype == "101")
                    alert("Pretrial cannot be set for Saturday. Please select another date."); 
               else if (datetype == "26" || datetype == "256")
                    alert("Jury Trial cannot be set for Saturday. Please select another date.");
               else if (datetype == "103" || datetype == "257")
                    alert("Judge Trial cannot be set for Saturday. Please select another date.");                                
                
                month.focus();
                return false;
            }
		}
		//If Sunday
        if ( weekDay == 0)
        {
            alert("Court date cannot be set for Sunday. Please select another date.");
            month.focus();
            return false;
        }	
			
	    // Sugar Land Court Validations
		if ( courtid == "3060")
		{
                if ( courtnum == "")
            	{
            	    document.getElementById(updatecontrol + "_tb_prm").value = "0";    	
            	    courtnum ="0";
            	}
               //Sabir Khan 7979 07/02/2010 removed court room no validation for sugarland court. 
               var iChars = "!@#$%^&*()+=-[]\';,./{}|\":<>~`?";
               for (var i = 0; i < courtnum.length; i++) 
               {
  	            if (iChars.indexOf(courtnum.charAt(i)) != -1) 
  	                {
  	                    alert("Special character are not allowed for court number.");
                        return false;
  		            }
  		       }
		    	//if ( courtnum != "0" && courtnum !=null  )
		    	//{
		    	//    alert("Court number 0 can only be set for sugar land court.");
		    	//    return false;
		    	//}
		    	
		    	//Sabir Khan 8188 08/19/2010 9:00 AM has been added in court time for pre trial sugarland
		    	if (  (datetype=="66" ||  datetype=="77" ||  datetype=="85" ||  datetype=="101" )  && varhours !="1:30 PM" && varhours !="3:30 PM" && varhours !="9:00 AM")
		    	{
		    	    alert("Pre Trial for sugar land court must be at 1:30 PM or 3:30 PM or 9:00 AM.");
		    	    return false;
		    	}
		    	
		}
			
		//Sabir Khan 8623 12/14/2010 Validation for Pre trial and Motion hiring flag...
		if(CaseType == "1" && (courtid != "3002" && courtid != "3002" && courtid != "3003") && (datetype == "104" && oldStatus == "101") && (document.getElementById('<%=hf_PreTrialDiversion.ClientID %>').value !=0 || document.getElementById('<%=hf_MotionHiring.ClientID %>').value !=0))
		{
		
		  if(document.getElementById('<%=hf_PreTrialDiversion.ClientID %>').value !=0)
		  {
		    alert("The client has a 'Pre Trial Diversion' status, This case can't be reschedule. please first remove 'Pre Trial Diversion' flag to update the case");		    
		    return false;
		  }
		  
		  if(document.getElementById('<%=hf_MotionHiring.ClientID %>').value !=0)
		  {
		    alert("The client has a 'Motion Hiring' status, This case can't be reschedule. please first remove 'Motion Hiring' flag to update the case");		   
		    return false;
		  }
		  
		}
		//Sabir Khan 8623 12/14/2010 Validation for Pre trial and Motion hiring flag...
		
		if(CaseType == "1" && (courtid != "3002" && courtid != "3002" && courtid != "3003") && (document.getElementById('<%=hf_PreTrialDiversion.ClientID %>').value !=0 || document.getElementById('<%=hf_MotionHiring.ClientID %>').value !=0) && (datetype == "104" && document.getElementById(updatecontrol + "_cb_updateall").checked== true))
		{
		  if(document.getElementById('<%=hf_PreTrialDiversion.ClientID %>').value !=0)
		  {
		    alert("The client has a 'Pre Trial Diversion' status, This case can't be reschedule. please first remove 'Pre Trial Diversion' flag to update the case");		    
		    return false;
		  }
		  
		  if(document.getElementById('<%=hf_MotionHiring.ClientID %>').value !=0)
		  {
		    alert("The client has a 'Motion Hiring' status, This case can't be reschedule. please first remove 'Motion Hiring' flag to update the case");		    
		    return false;
		  } 
		}
		
		//------------------------------------------------------------------------
		//Sabir Khan 8862 02/15/2011 Validation for JUVENILE FLAG....  139, 256, 257, 258, 259
		if(CaseType == "1" && (parseInt(selectedCourt.value,10)>=3007 && parseInt(selectedCourt.value,10)<=3022) && (datetype == "104" && (oldStatus == "139" || oldStatus == "256" || oldStatus == "257" || oldStatus == "258" || oldStatus == "259" )) && (document.getElementById('<%=hf_JUVNILE.ClientID %>').value !=0))
		{
		    alert("A JUVENILE CASE CANNOT BE RESET-CLIENT MUST BE PRESENT AND NOT ALLOW AGENTS TO CHANGE STATUS OF CASE. ALL OUR JUVENILE CASES MUST BE PRESENT IN COURT");		    
		    return false;
		}
		if(CaseType == "1" && (parseInt(selectedCourt.value,10)>=3007 && parseInt(selectedCourt.value,10)<=3022) && (document.getElementById('<%=hf_JUVNILE.ClientID %>').value !=0) && (datetype == "104" && document.getElementById(updatecontrol + "_cb_updateall").checked== true))
		{
		    alert("A JUVENILE CASE CANNOT BE RESET-CLIENT MUST BE PRESENT AND NOT ALLOW AGENTS TO CHANGE STATUS OF CASE. ALL OUR JUVENILE CASES MUST BE PRESENT IN COURT");		    
		    return false;		    
		}
		
		//------------------------------------------------------------------------
		   //Abbas Qamar 10088 04/24/2012 Court Room for HMC W
        var crtnumber = document.getElementById(updatecontrol + "_tb_prm").value;
        if (crtnumber == 20 && courtid == "3075") {

            //Farrukh 11254 07/17/2013 Allowed Court Room # 20 for HMC W and Jury Trial Status
            //if ((datetype == "4") || (datetype == "24") || (datetype == "28")) {
            if ((datetype == "24") || (datetype == "28")) {
                alert("Jury Trial is not a Valid Status");
                return false;
            }


        }        
        else if (crtnumber == 20 && courtid != "3075") {
            alert("Court number 20 can only be set for West Montgomery.");
            return false;
        }
        else if (crtnumber != 20 && courtid == "3075") {
            alert("Please Enter court number 20 for West Montgomery.");
            return false;
        }
        // End Abbas Qamar   

			var doyou;
			if ((courtid == "3001") ||(courtid == "3002") || (courtid == "3003")|| (courtid == "3075")) 
			{
			    //ozair 5171 11/19/2008 checking disposed cases and past/today date 
				if ( currentstatuscategoryid=="50" && launchCheck(strdate)==false)
				{
				}
				else if ((currentstatuscategoryid != courtstatusautocategoryid) || (samedateflag == false) || (parseInt(courtnumauto,10) != parseInt(courtnum,10))) 
				{
					doyou = confirm("The verified court status and auto load courts status are different OR have conflicting setting information.  Would you like to proceed? (OK = Yes   Cancel = No)");
					if (doyou == false) 
					{
					return false;
					}
				}
				

				if ((refcasenumber.substring(0,1) == "0" ) || (refcasenumber.substring(0,1) == "M") || (refcasenumber.substring(0,1) == "A")) 
				{
    
				}

		//Farrukh 11254 07/17/2013 Allow Court room # 20 for HMC W for Jury Trial Status
		if (courtid != "3075") {
		    if (courtnum == "20") {
		        alert("Please select HMC W. Montgomery Court for this case since court room number 20 is set at HMC W");
		        document.getElementById(updatecontrol + "_tb_prm").focus();
		        return false;
		    }
		}
		
		//Yasir Kamal 7678 04/07/2010 allow only 18 and 20 court room# for HMC Dairy Ashford        
		if (courtid != "3003") 
		{
	    	if (courtnum == "18") 
		    	{
			    	alert("Please select HMC Dairy Ashford Court for this case since court room number 18 is set at Dairy Ashford");
					document.getElementById(updatecontrol + "_tb_prm").focus();
					return false;
				}
        }
        
		
		if (courtid == "3003") 
		{
		    if (courtnum != "18")
			    {
					alert("Please select court room number 18 for this case since court room number 18 is set at Dairy Ashford");
					document.getElementById(updatecontrol + "_tb_prm").focus();
					return false;
				}
        }
        
        if (courtid == "3075") {
            if (courtnum != "20") {
                alert("Please select court room number 20 for this case since court room number 20 is set at HMC W");
                document.getElementById(updatecontrol + "_tb_prm").focus();
                return false;
            }
        }

		
		if(courtid == "3001")
		{ 
		   //Zeeshan Ahmed 4317 06/27/2008 Add HMC New Court Logic
			var courtno = courtnum * 1;	
			//Sabir Khan 7690 04/12/2010 Removed crt # 20 logic...		
			if (!(courtno >= 0 && courtno <= 12) && !(courtno >= 15 && courtno <= 17))
			{
				alert("Please Enter 0 to 12 or 15 to 17 for Lubbock Court");
				return false;
			}
			
			// Sabir Khan 7398 02/11/2010 Display alert when HMC(Lubbock) Judge Trial has court time not b/w 08:00 AM and 12:00 PM...
            if(datetype == "103")
            {                
               var sttime = varhours.substring(varhours.length -2,varhours.length);
               var hourindex = varhours.indexOf(":");
               var hour = varhours.substring(0,hourindex);
               var min = varhours.substring(hourindex+1,varhours.length -3);              
               if(((parseInt(hour)==12)&&(sttime=="PM")&&(parseInt(min)>0))||((parseInt(hour) <8)&&(sttime == "AM")) || ((parseInt(hour)<12)&&(sttime=="PM")))
               {
                 alert("Please inform the client that Attorney will attempt to reset client's judge trial")
                 //Yasir Kamal 7398 02/15/2010 Just display alert if time not b/w 08:00 AM and 12:00 PM for HMC(Lubbock) Judge Trial
                 //return false;
               } //Yasir Kamal 7453 03/01/2010 display mesg in red if time b/w 08:00 AM and 12:00 PM for HMC(Lubbock) Judge Trial
               else 
               {
                 alert("Client must appear on this date and time")
               }
            }
	        //Zeeshan Ahmed 4317 06/27/2008 Add HMC New Court Logic
	        //Sabir Khan 5331 12/17/2008 allow all HMC court statuses for court 20...
//	        if ( courtno== 20 && datetype != 3 && datetype != 236)
//	        {
//	            alert("Only Arraignment status case be set for this court no.");
//          
//	            if(ddl_pstatus.style.display != 'none')
//				   ddl_pstatus.focus();
//           
//	            return false
//	        }
		}
		
	
		 
		        //Sabir Khan 8541 11/22/2010 Court Room for HMC D
				if(courtid == "3002") //||(courtid)== "3003")
				{ 
					if ((datetype == "4") || (datetype == "24") || (datetype == "26") || (datetype == "28")) 
					{ 
						alert("Jury Trial is not a Valid Status");
						return false;
					}
				}
				//borntowin
				/*if(courtid == "3002")
				{ 
					if ( datetype == "103" ) 
					{ 
						alert("Judge Trial can only be set for HMC-L and HMC-D.");
						return false;
					}
				}*/

				if ((courtid == "3001") || (courtid == "3002") || (courtid == "3003")|| (courtid == "3075")) 
				{
					if ((datetype == "4") || (datetype == "24") || (datetype == "26") || (datetype == "28")) 
					{
						if ((varhours != "8:00 AM") && (varhours != "9:00 AM") && (varhours != "10:30 AM") && (varhours != "10:00 AM")) 
						{
							alert("Jury Trial must be at one of the following times: 8am, 9am, 10am or 10:30am");
							document.getElementById(updatecontrol + "_ddl_pcrttime").focus();
							return false;
						}
						
						
						
						var bodflag=0;
						if(document.getElementById(updatecontrol + "_rbl_bondflag_0").checked)
						{
							bodflag = 1;
						}
						else
						{
							bodflag = 0;
						}
						if (bodflag ==0 && datetype != 104 && datetype != 105 && datetype != 135)
						{
						    //Sabir Khan 8541 11/22/2010 Court Room for HMC D
						    //Sabir Khan 8622 12/10/2010 allow court room number 5 for HMC L.
						    //Sameeullah Daris 8636 12/28/2010 requirement (i) gramatical correction. for jury trial HMC D
						    
						    if((courtid == "3003") && (courtnum == "20") && (datetype == 26))							
							{
							    //Babar Ahmad 9891 10/31/2011 Added Friday for HMC-D for court # 18 and Jury Trial.
							    alert("Jury trial set at this location must be at court room # 18 and day must be Thursday/Firday");
							    document.getElementById(updatecontrol + "_tb_prm").focus();
							    return false;
							}

							//Farrukh 11254 07/17/2013 Allowed Court Room # 20 for HMC W and Jury Trial Status
							if (courtid == "3075") {
							    if ((courtnum != "6") && (courtnum != "1") && (courtnum != "2") && (courtnum != "3") && (courtnum != "8") && (courtnum != "11") && (courtnum != "12") && (courtnum != "18") && (courtnum != "5") && (courtnum != "20")) {
							        alert("Jury Trial sets at this location must be in one of the following court rooms: 1,2,3,5,6,8,11,12,18,20");
							        document.getElementById(updatecontrol + "_tb_prm").focus();
							        return false;
							    }
							}
							else if ((courtnum != "6") && (courtnum != "1") && (courtnum != "2") && (courtnum != "3") && (courtnum != "8") && (courtnum != "11") && (courtnum != "12") && (courtnum != "18") && (courtnum != "5")) {
							    //Abbas Shahid 8622 06/15/2011 fixed room no 18 issues for HMC L
							    //Fahad 9422 07/29/2011 Included Room No 18 in alert message
							    alert("Jury Trial sets at this location must be in one of the following court rooms: 1,2,3,5,6,8,11,12,18");
							    document.getElementById(updatecontrol + "_tb_prm").focus();
							    return false;
							}
						    	
						}
						//Sabir Khan 8541 11/22/2010 Court Room for HMC D
						//Sameeullah Daris 8636 12/28/2010 requirement (ii) gramatical correction.
						 //Babar Ahmad 9891 10/31/2011 Added Friday for HMC-D for court # 18 and Jury Trial.
						if((courtid == "3003") && (courtnum == "18") && (weekDay != 4 && weekDay != 5) && (datetype != 104 && datetype != 105 && datetype != 135))							
							{
							
							    alert("Jury trial set at this location must be at court room # 18 and day must be Thursday/Friday");
							    month.focus();
							    return false;
							}
						if((courtid == "3003") && (courtnum == "20") && (datetype != 104 && datetype != 105 && datetype != 135))							
							{
							    //Babar Ahmad 9891 10/31/2011 Added Friday for HMC-D for court # 18 and Jury Trial.
							    alert("Jury trial set at this location must be at court room # 18 and day must be Thursday/Friday");
							    document.getElementById(updatecontrol + "_tb_prm").focus();
							    return false;
							}
					}
					//alert(datetype)
					if ((datetype == "66") || (datetype == "85") || (datetype == "101") || (datetype == "104")) 
					{
						alert("This court location cannot have a status of Waiting or Pre-Trial");
						
						
						if ( ddl_pstatus.style.display == "none")
						{
						    DisplayToggleP(ddl_pstatus.id,lbl_pstatus.id,ctrlstyl);
						    ddl_pstatus.focus();
						
						}
						else
						{					
						ddl_pstatus.focus();
						}
						return false;
					}
				}
				
				var bdflag=0;
				if(document.getElementById(updatecontrol +  "_rbl_bondflag_0").checked)
				{
					bdflag = 1;
				}
				else
				{
					bdflag = 0;
				}
				if (bdflag ==0 && datetype != 104 && datetype != 105 && datetype != 135)
				{
				//Yasir Kamal 7678 04/07/2010 allow only 18 and 20 court room# for HMC Dairy Ashford 
//					if (courtid != "3003") 
//					{
//						if ((courtnum == "18") ) 
//						{
//							alert("This court number can only be set for Dairy Ashford Court");
//							document.getElementById(updatecontrol + "_tb_prm").focus();
//							return false;
//						}
//					}
//					if (courtid == "3003") 
//					{
//						if ((courtnum != "18") ) 
//						{
//							alert("Court number 18 can only be set for Dairy Ashford Court");
//							document.getElementById(updatecontrol + "_tb_prm").focus();
//							return false;
//						}
//					}

					if (courtid != "3002") 
					{
						if ((courtnum == "13") || (courtnum == "14") ) 
						{
							alert("This court number can only be set for Mykawa");
							document.getElementById(updatecontrol + "_tb_prm").focus();
							return false;
						}
					}
					//borntowin
					if (courtid == "3002") 
					{
						if ((courtnum != "13") && (courtnum != "14") ) 
						{
							alert("Court number 13 or 14 can only be set for Mykawa.");
							document.getElementById(updatecontrol + "_tb_prm").focus();
							return false;
						}
					}
				}
			}	
			
			var rbl_bondflag = document.getElementById(updatecontrol +  "_rbl_bondflag_0");
			var strdate = month.value + "/" + day.value + "/20" + year.value; 
			var strtime = 	document.getElementById(updatecontrol + "_ddl_pcrttime").value;
			var bbflags=0;
			if(rbl_bondflag.checked)
			{
				bbflags = 1;
			}
			else
			{
				bbflags = 0;
			}
			if (bbflags ==0 && datetype != 104 && datetype != 105 && datetype != 135)
			{
				if (strtime == "--") 
				{
					alert("Invalid Court Time. ");
					document.getElementById(updatecontrol + "_ddl_pcrttime").focus();
					return false;
				}
				if (strtime == "11") 
				{
					if (datetype != 80 )
					{
						selectedindex1 = document.getElementById(updatecontrol + "_ddl_ViolationDescription").selectedIndex;
						var strviolationnew = trimAll(document.getElementById(updatecontrol + "_ddl_ViolationDescription").options[selectedindex1].text);
					
						if (trimAll(strviolationnew) != '------Default Out----')
						{
							alert("Invalid Court Time. ");
							document.getElementById(updatecontrol + "_ddl_pcrttime").focus();
							return false;
						}
					}
				}
			}
		
			//Check For Court No
			var bndflag=0;
			var datetype1 = ddl_pstatus.value;
			if(rbl_bondflag.checked)
			{
				bndflag = 1;
			}
			else
			{
				bndflag = 0;
			}
			if (bndflag ==0 && datetype1 != 104 && datetype1 != 105 && datetype1 != 135)
			{
				var courtnumber =document.getElementById(updatecontrol + "_tb_prm");
				if (courtnumber.value.indexOf(" ") != -1 ) 
				{
					alert("Please make sure that there are no spaces in the CourtNumber input box"); 
					courtnumber.focus();
					return false;
				}
				//ozair 5105 11/19/2008
				if (courtnumber.value.trim()=="") 
				{
					courtnumber.value=0;
					document.getElementById(updatecontrol + "_tb_prm").value=0;
					//alert("Please Enter Court Number.");
					//document.frmviolationfee.tb_prm.focus();
					//return false;
				}
/*				if (courtnumber=="0") 
				{
					alert("Invalid Court Number.");
					document.UpdateViolation.txt_CourtNo.focus();
					return false;
				}
*/				
				var ValidChars = "0123456789";
				var Char;
				for (j = 0; j < courtnumber.value.length; j++) 
				{ 
					Char = courtnumber.value.charAt(j); 
					if (ValidChars.indexOf(Char) == -1 && courtid != "3060") 
					{
						alert("Invalid Court Number");
					    courtnumber.focus();
						return false;
					}
				}
			}
			
			
			if (strdate == "0/--/2005" )   
			{
				var bflags=0;
				if(rbl_bondflag.checked)
				{
					bflags = 1;
				}
				else
				{
					bflags = 0;
				}
				if (bflags ==0 && datetype != 104 && datetype != 105 && datetype != 135)
				{
					if (datetype != 80) 
					{
						selectedindex1 = ddl_vdesc.selectedIndex;
						var strviolationnew = trimAll(ddl_vdesc.options[selectedindex1].text);		
						if (trimAll(strviolationnew) != '------Default Out----')
						{
							alert("Invalid Court Date. ");
							document.getElementById(updatecontrol + "_tb_pmonth").focus();
							return false;
						}
					}
				}
			}
			else 
			{
				//alert(launchCheck(strdate))
				var fticketsflag1 ="";// frmviolationfee.txt_FTicketCount.value;
				//var bondflag = UpdateViolation.txt_BondFlag.value;
				var bondflag = 0;
				if(rbl_bondflag.checked)
				{
					bondflag = 1;
				}
				else
				{
					bondflag = 0;
				}
								
				      
				       
				if ((courtid == "3001") ||(courtid == "3002") || (courtid == "3003") || (courtid == "3075")) 
				{
					if (bondflag == 0) 
					{
						if (datetype != 80 && datetype != 104 && datetype != 105 && datetype != 135 && datetype != 123)  // Not Disposed
						{
							if (launchCheck(strdate) == false && document.getElementById(updatecontrol +  "_hf_newviolation").value != "1" && document.getElementById(updatecontrol + "_hf_allowolddate").value =="0" )  // if date is in the past
							{
								alert("Court Date has already passed. Please specify a future Date. ");
								month.focus();
								return false;
							}
						}
					//}
						//a;
						var bondflagT = 1;//frmviolationfee.txt_BondFlag.value;
/*						if(bondflagT == 0)
						{
							if ( fticketsflag1 == 0 & UpdateViolation.txt_IsNew.value=="0" & datetype != 135 & launchCheck(strdate) == false ) 
							{
								alert("Please Enter at least one Fticket to process this client. ");
								return false;
							}
						}
*/						
					}	
				}
				if (i != 3 ) 
				{
					var violationnumber;
					var fticketflag = false;
					if (launchCheck(strdate) == false ) 
					{
						//alert(strdate)
						for (var i=0; i<=ddl_vdesc.length-1;i++) 
						{
							violationnumber =ddl_vdesc[i].value;
							if ((violationnumber == "50") || (violationnumber == "448") || (violationnumber == "2821") || (violationnumber == "2822") || (violationnumber == "2824") || (violationnumber == "4337") || (violationnumber == "4999") || (violationnumber == "5103")) 
							{
								fticketflag = true;
								break;
							}
						}

						if (fticketflag == false)
						{
							if (datetype != 80) 
							{
								//alert("Court Date has already passed. Please specify a different Date or Click Insert Violation link. ");
								//return false;
							}
						}
					}
				}		
			}
		  
		   
		    //Zeeshan Ahmed 3486 04/04/2008
		   if ( document.getElementById(updatecontrol + "_trMissedCourt").style.display != "none" ) 
		   {
		        //Zeeshan Ahmed  4163 06/05/2008 If Missed Court Type Change Than Sent Letter To Batch
		        var CurrMissedCourtType = "-1"
		        var PrevMissedCourtType =  document.getElementById(updatecontrol + "_hf_missedcourttype").value;
		        //Yasir Kamal 6194 07/24/2009 hide bond row for crminal court.     
                var isCriminalCourt = document.getElementById(updatecontrol + "_hf_iscriminalcourt").value;
		       
		        if (NoAction.checked)     
		            CurrMissedCourtType = "1";
		        if ( PledOut.checked)
		            CurrMissedCourtType = "2";
		       
		       //Yasir Kamal 6194 07/24/2009 hide bond row for crminal court.
		        if (CurrMissedCourtType != PrevMissedCourtType && isCriminalCourt != "1" )
		        {		       
		           if ( NoAction.checked && bndflag == "1" )
		           {
		                if ( !confirm("Client already have Bond selected. Are you sure you want to update status to Missed Court - No Action. Press OK to Yes or Cancel for No."))
		                return false; 
		           }
		           else if ( PledOut.checked && bndflag == "0" )
		           {
		                if ( !confirm("Client don't have Bond selected. Are you sure you want to update status to Missed Court - Pled Out. Press OK to Yes or Cancel for No."))
		                return false; 
		           }
		        }    
		   }
		   
		  
		   //Zeeshan Ahmed 3757 04/17/2008
		   //If HCJP Case
		   if(parseInt(selectedCourt.value,10)>=3007 && parseInt(selectedCourt.value,10)<=3022)
		   {		   
		       var casenumber = document.getElementById(updatecontrol + "_tb_causeno").value;
		       var hf_hasNOS = 	document.getElementById(updatecontrol + "_hf_hasNOS").value;
		       var hf_UpdateNOS = 	document.getElementById(updatecontrol + "_hf_UpdateNOS");
    		   	
		       //If Case Has No NOS Flag and Cause Number Empty	
		       if (casenumber == "" && hf_hasNOS == "0")
		       {
		            if ( confirm("This Ticket has no CauseNumber. Do you want to set the NOS flag on Contacts Page. Please press Ok for Yes or Cancel for No"))
		        	    hf_UpdateNOS.value = "1";
		            else
		        	    hf_UpdateNOS.value = "0";   
		       }
               else
                hf_UpdateNOS.value = "0"; 		   
           }
		  
		 //Zeeshan Ahmed 3825 04/23/2008
		 //If Not Criminal Court And Court Date Is Changed 
          var prevCourtDate =courtdateauto.substring(0,courtdateauto.indexOf(' '));  
         //Zeeshan Ahmed 3925 05/01/2008
          var CoveringFirmId = document.getElementById(updatecontrol + "_hf_coveringFirmId").value;
		  //ozair 4442 07/22/2008 ALR Courts(Houston, Fort bend, Conroe)
		  // tahir 4731 09/03/2008 Civil courts (Harris County Civil Court,Harris County Title 4D Ct)
          //Yasir Kamal 5793 04/14/2009 don't change Attorney to sullo & sullo if it is an Oscar court
          if ( !( courtid == 3061 || courtid == 3071 || courtid == 3077 || courtid == 3036 || courtid == 3037 ||  courtid == 3043 || courtid == 3047 || courtid == 3048 || courtid == 3057 || courtid == 3058 || courtid == 3070 ) && (strdate != prevCourtDate && prevCourtDate != "" && launchCheck(strdate) ) && CoveringFirmId != "3000")
		      alert("The coverage attorney for this case will be reset to Sullo & Sullo.");  
		 
		
		
		document.getElementById(divname).style.display = "block";
		document.getElementById(gridname).style.display = "none";
		document.getElementById(divname).focus();
		  
		closepopup("pnl_violation",updatecontrol + "_Disable");
		return true;
		}
        
        
// **********Added By : Zeeshan Ahmed ************//
//   Added For Hiding and Showing Drop Down List  //
//************************************************//


    var  _backgroundElement = null;
    var _foregroundElement = null;
    var _saveTabIndexes = new Array();
    var _saveDesableSelect = new Array();
    var _tagWithTabIndex = new Array('A','BUTTON','TEXTAREA','INPUT','IFRAME');

function disableTab(panel)  {
                
                
        _tagWithTabIndex = new Array('A','BUTTON','TEXTAREA','INPUT','IFRAME');
              
        var i = 0;
        var tagElements;
        var tagElementsInPopUp = new Array();
        Array.clear(_saveTabIndexes);
        
         _foregroundElement = document.getElementById(panel);

       //Save all popup's tag in tagElementsInPopUp
        for (var j = 0; j < _tagWithTabIndex.length; j++) {
            tagElements = _foregroundElement.getElementsByTagName(_tagWithTabIndex[j]);
            for (var k = 0 ; k < tagElements.length; k++) {
                tagElementsInPopUp[i] = tagElements[k];
                i++;
            }
        }

        i = 0;
        for (var j = 0; j < _tagWithTabIndex.length; j++) {
            tagElements = document.getElementsByTagName(_tagWithTabIndex[j]);
            for (var k = 0 ; k < tagElements.length; k++) {
                if (Array.indexOf(tagElementsInPopUp, tagElements[k]) == -1)  {
                    _saveTabIndexes[i] = {tag: tagElements[k], index: tagElements[k].tabIndex};
                    tagElements[k].tabIndex="-1";
                    i++;
                }
            }
        }

        //IE6 Bug with SELECT element always showing up on top
       
        i = 0;
            //Save SELECT in PopUp
             var tagSelectInPopUp = new Array();
            for (var j = 0; j < _tagWithTabIndex.length; j++) 
            {
                tagElements = _foregroundElement.getElementsByTagName('SELECT');
                for (var k = 0 ; k < tagElements.length; k++) {
                    tagSelectInPopUp[i] = tagElements[k];
                    i++;
                }
            }

            i = 0;
            Array.clear(_saveDesableSelect);
            tagElements = document.getElementsByTagName('SELECT');
            for (var k = 0 ; k < tagElements.length; k++) 
              {
                if (Array.indexOf(tagSelectInPopUp, tagElements[k]) == -1)  
                {
                    _saveDesableSelect[i] = {tag: tagElements[k], visib: getCurrentStyle(tagElements[k], 'visibility')} ;
                    tagElements[k].style.visibility = 'hidden';
                    i++;
                }
             }
    }
           
    

   function restoreTab()
     {
        
        for (var i = 0; i < _saveTabIndexes.length; i++) {
            _saveTabIndexes[i].tag.tabIndex = _saveTabIndexes[i].index;
        }

        for (var k = 0 ; k < this._saveDesableSelect.length; k++) {
                _saveDesableSelect[k].tag.style.visibility = _saveDesableSelect[k].visib;
            
        }
    }


        function getCurrentStyle (element, attribute, defaultValue) {
    

        var currentValue = null;
        if (element) {
            if (element.currentStyle) {
                currentValue = element.currentStyle[attribute];
            } else if (document.defaultView && document.defaultView.getComputedStyle) {
                var style = document.defaultView.getComputedStyle(element, null);
                if (style) {
                    currentValue = style[attribute];
                }
            }
            
            if (!currentValue && element.style.getPropertyValue) {
                currentValue = element.style.getPropertyValue(attribute);
            }
            else if (!currentValue && element.style.getAttribute) {
                currentValue = element.style.getAttribute(attribute);
            }       
        }
        
        if ((!currentValue || currentValue == "" || typeof(currentValue) === 'undefined')) {
            if (typeof(defaultValue) != 'undefined') {
                currentValue = defaultValue;
            }
            else {
                currentValue = null;
            }
        }   
        return currentValue;  
    }

    		
		function DateDiff(date1, date2)
		{
			var objDate1=new Date(date1);
			var objDate2=new Date(date2);
			return (objDate1.getTime()-objDate2.getTime())/1000;
		}
		
		
		function launchCheck(strdate) 
		{
			var today = new Date(); // today
			var date = new Date(strdate); // your launch date
			//alert(date.getTime())
			//alert(today.getTime())
			var oneday = 1000*60*60*24
			//alert(Math.ceil(today.getTime()-date.getTime())/oneday)
			if (Math.ceil(today.getTime()-date.getTime())/oneday >= 1 ) 
			//if ( date < today )
			{
				return false;
			}
			return true;
		}
		
	
	    function plzwait(hidepanelname  , showpanelname)
	    {
	        document.getElementById(showpanelname).style.display = "block";
		    document.getElementById(hidepanelname).style.display = "none";
		    document.getElementById(showpanelname).focus();
	    }
	    
	   	
		
		function selectValues(ddl ,srch)
		{	     
	                
	        for ( i =0 ; i < ddl.options.length ; i++)
		    {
		            var text = ddl.options[i].innerText;
		            var value = ddl.options[i].value;
		            
		            if ( srch == value)
		            {
		               ddl.options[i].selected = true;
		               break;
		            }
		    }
		}
						
        function saveStatus(control)
        {
            alert("hi");
        }
        
    function chargelevel(updatepanel,ctrl1,ctrl2,ctrlstyle)
    {
    

     var courtid = document.getElementById(updatepanel + "_ddl_pcrtloc").value;
     
     
     //ozair 4442 07/22/2008 ALR Courts(Houston, Fort bend, Conroe)
     //if(courtid== "3047" || courtid== "3048" || courtid== "3070" || courtid == "3079")
     //Nasir 5310 12/29/2008 check the court is ALR Court .
     //Waqas 5864 07/17/2009 Fixed Charge Level issue
	 if(document.getElementById(updatepanel +"_hf_IsALRProcess").value == "True")
     {
       
        document.getElementById(updatepanel+"_tr_chargelevel").style.display='none';
        document.getElementById(updatepanel+"_tr_fineamount").style.display='block';
      
        document.getElementById(updatepanel+"_ddl_pvdesc").style.display='block';
        document.getElementById(updatepanel+"_tb_pvdesc").style.display='none';
        document.getElementById(ctrlstyle).value =  document.getElementById(updatepanel+"_ddl_pvdesc").style.display;     
     }
   }
        
   
//************************************************//                

//Zeeshan Ahmed 3486 04/03/2008
function checkMissedCourtStatus(status,hfstatus,trMissedCourt)
{
     document.getElementById(trMissedCourt).style.display=(status.value == "105")?"block":"none";
     //ozair 5171 11/19/2008 setting category id at runtime selection of status
     document.getElementById("UpdateViolationSRV1_ddl_StatusCategory").selectedIndex= document.getElementById("UpdateViolationSRV1_ddl_pstatus").selectedIndex;
}

//Zeeshan Ahmed 3742 05/13/2008
function validateCourtDate (month,day,year)
{   
 		    
 		     if ( month.value =="" || day.value =="" || year.value == "")
		     {
		            alert("Please enter arrest date.");
		            month.focus();
		            return false;
		     }
 		    
		    if (day.value =="0" )
		     {
		            alert("Please enter valid day.");
		            day.focus();
		            return false;
		     }
		      if (month.value =="0" )
		     {
		            alert("Please enter valid month.");
		            month.focus();
		            return false;
		     }
		   
		    if (year.value =="0" )
		     {
		            alert("Please enter valid year.");
		           year.focus();
		            return false;
		     }
		     
		    if (isNaN(month.value) )
		     {
		            alert("Please enter valid month.");
		            month.focus();
		            return false;
		     }
		        
		     if (  month.value > 12 )
		     {
		            alert("Please enter valid month.");
		           month.focus();
		            return false;
		     }
		     
		     if (isNaN(day.value))
             {
	                alert("Please enter valid day.");
	                day.focus();
	                return false;
             }		     
			    
	        if (isNaN(year.value))
	        {
                alert("Please enter valid year.");
                year.focus();
                return false;
	        }
		   		   
		    //Zeeshan Ahmed 3825 04/23/2008
		    //TRIM COURT DATE
		    month.value  = trim(month.value);
		    day.value = trim( day.value);
		    year.value = trim (year.value)
		    return true;
}
function setchargelevel()
{
    // Noufil 5072 11/03/2008 select default charge level for matter description occupational liscence

        // tahir 5117 11/11/2008 hide Level & CDI ....
        if (document.getElementById("UpdateViolationSRV1_hf_casetype").value == "2")
        {
			if (document.getElementById("UpdateViolationSRV1_ddl_pvdesc").value == "18534" )
			{
			    // tahir
			    document.getElementById("UpdateViolationSRV1_ddl_pvlevel").value="11";			    
			    document.getElementById("UpdateViolationSRV1_tr_chargelevel").style.display ="none";			    
			    document.getElementById("UpdateViolationSRV1_tr_cdi").style.display = "none";			    
			}
			else
			{
			    document.getElementById("UpdateViolationSRV1_tr_chargelevel").style.display ="block";
			    if(document.getElementById("UpdateViolationSRV1_ddl_pcrtloc").value =="3037")		    
			    {
			        document.getElementById("UpdateViolationSRV1_tr_cdi").style.display = "block";			    
			    }
			    else
			    {
			        document.getElementById("UpdateViolationSRV1_tr_cdi").style.display = "none";			    
			    }
			}
		}
		
		//Waqas 5864 07/17/2009 Fixed Charge Level issue
        if(document.getElementById("UpdateViolationSRV1_hf_IsALRProcess").value == "True")
	   	{   
	   	   document.getElementById("UpdateViolationSRV1_tr_fineamount").style.display="block";
	       document.getElementById("UpdateViolationSRV1_tr_chargelevel").style.display="none"
	   	}
		
}

</script>

<div id="Disable" runat="server" class="modalBackground" style="z-index: 3; display: none;
    left: 0px; position: absolute; top: 0px">
</div>
<div id="pnl_violation" style="display: none; z-index: 7; background-color: transparent">
    <aspnew:UpdatePanel ID="upnlUpdateViolation" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table style="border-left-color: navy; border-bottom-color: navy; border-top-color: navy;
                border-collapse: collapse; border-right-color: navy" border="2" enableviewstate="true">
                <tbody>
                    <tr>
                        <td valign="bottom" background="../Images/subhead_bg.gif">
                            <table width="350" border="0">
                                <tbody>
                                    <tr>
                                        <td style="height: 26px" class="clssubhead">
                                            Update Violation Info
                                        </td>
                                        <td align="right">
                                            <asp:LinkButton ID="lbtn_close" OnClick="lbtn_close_Click" runat="server">X</asp:LinkButton>
                                            &nbsp;
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr style="font-size: 10pt; font-family: Tahoma">
                        <td valign="top">
                            <table style="width: 350px" class="clsLeftPaddingTable" cellspacing="0" cellpadding="0"
                                border="1">
                                <tbody>
                                    <tr>
                                        <td style="width: 130px; height: 22px" class="clssubhead">
                                            Court Location
                                        </td>
                                        <td>
                                            <table style="width: 100%" cellspacing="0" cellpadding="0" border="0">
                                                <tbody>
                                                    <tr>
                                                        <td style="width: 105px">
                                                            <asp:DropDownList ID="ddl_pcrtloc" runat="server" DataTextField="shortname" DataValueField="courtid"
                                                                AutoPostBack="True" CssClass="clsInputCombo" OnSelectedIndexChanged="ddl_pcrtloc_SelectedIndexChanged"
                                                                Width="115px">
                                                            </asp:DropDownList>
                                                            <asp:Label Style="display: none" ID="lbl_pcname" runat="server" CssClass="Label"
                                                                Width="100%"></asp:Label>
                                                        </td>
                                                        <td align="left">
                                                            <img id="imgbtncourt" src="../Images/Rightarrow.gif" runat="server" />
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr style="display: none" id="tr_oscar" runat="server">
                                        <td style="width: 130px; height: 22px" class="clssubhead">
                                            Oscar Court Location
                                        </td>
                                        <td style="height: 21px">
                                            <asp:TextBox ID="tb_oscare_court" runat="server" CssClass="clsInputadministration"
                                                Width="112px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 130px; height: 22px" class="clssubhead">
                                            Cause No
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tb_causeno" runat="server" CssClass="clsInputadministration" Width="112px"
                                                MaxLength="20"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr id="tr_ticketno" runat="server">
                                        <td style="width: 130px; height: 22px" class="clssubhead">
                                            Ticket No
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tb_pticketno" runat="server" CssClass="clsInputadministration" Width="112px"
                                                MaxLength="50"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 130px; height: 22px" class="clssubhead">
                                            Matter
                                        </td>
                                        <td>
                                            <table style="width: 100%" cellspacing="0" cellpadding="0">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <asp:DropDownList Style="display: none" ID="ddl_pvdesc" runat="server" DataTextField="Description"
                                                                DataValueField="ID" CssClass="clsInputCombo" Width="135px" EnableViewState="true"
                                                                onclick="setchargelevel();">
                                                            </asp:DropDownList>
                                                            <asp:TextBox onkeydown="return false" ID="tb_pvdesc" onkeypress="return false;" runat="server"
                                                                CssClass="clsinputadministrationlabel" Width="100%" Height="100%" TextMode="MultiLine"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:ImageButton ID="imgbtn_violation" runat="server" ImageUrl="../Images/Rightarrow.gif">
                                                            </asp:ImageButton>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr id="tr_chargelevel" runat="server">
                                        <td style="width: 130px; height: 22px" class="clssubhead">
                                            Level
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddl_pvlevel" runat="server" DataTextField="LevelCode" DataValueField="ID"
                                                CssClass="clsInputCombo" Width="115px">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr id="tr_cdi" runat="server">
                                        <td style="width: 130px; height: 22px" class="clssubhead">
                                            CDI
                                        </td>
                                        <td style="height: 18px">
                                            <asp:DropDownList ID="ddl_cdi" runat="server" CssClass="clsInputCombo" Width="115px">
                                                <asp:ListItem Value="0">--Choose--</asp:ListItem>
                                                <asp:ListItem Value="002">Misdemeanor (002)</asp:ListItem>
                                                <asp:ListItem Value="003">Felony (003)</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr id="tr_fineamount" runat="server">
                                        <td style="width: 130px; height: 22px" class="clssubhead">
                                            Fine Amount
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tb_pfineamount" runat="server" CssClass="clsInputadministration"
                                                Width="112px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr id="tr_bondamount" runat="server">
                                        <td style="width: 130px; height: 22px" class="clssubhead">
                                            Bond Amount
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tb_pbondamount" runat="server" CssClass="clsInputadministration"
                                                Width="112px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr id="tr_arrestdate" runat="server">
                                        <td style="width: 130px; height: 22px" class="clssubhead">
                                            Arrest Date
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tb_arrmonth" onkeyup="return autoTab(this, 2, event)" runat="server"
                                                CssClass="clsInputadministration" Width="26px" MaxLength="2"></asp:TextBox>
                                            /
                                            <asp:TextBox ID="tb_arrday" onkeyup="return autoTab(this, 2, event)" runat="server"
                                                CssClass="clsInputadministration" Width="26px" MaxLength="2"></asp:TextBox>
                                            /
                                            <asp:TextBox ID="tb_arryear" runat="server" CssClass="clsInputadministration" Width="26px"
                                                MaxLength="2"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr id="tr_bond" runat="server">
                                        <td style="width: 130px; height: 22px" class="clssubhead">
                                            Bond
                                        </td>
                                        <td>
                                            <asp:RadioButtonList ID="rbl_bondflag" runat="server" CssClass="Label" RepeatDirection="Horizontal">
                                                <asp:ListItem Value="1">&lt;span class=Label&gt;Yes&lt;span&gt;</asp:ListItem>
                                                <asp:ListItem Value="0">&lt;span class=Label&gt;No&lt;/span&gt;</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 130px; height: 22px" class="clssubhead">
                                            Court Date
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tb_pmonth" onkeyup="return autoTab(this, 2, event)" runat="server"
                                                CssClass="clsInputadministration" Width="26px" MaxLength="2"></asp:TextBox>
                                            /
                                            <asp:TextBox ID="tb_pday" onkeyup="return autoTab(this, 2, event)" runat="server"
                                                CssClass="clsInputadministration" Width="26px" MaxLength="2"></asp:TextBox>
                                            /
                                            <asp:TextBox ID="tb_pyear" onkeyup="return autoTab(this, 2, event)" runat="server"
                                                CssClass="clsInputadministration" Width="26px" MaxLength="2"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 130px; height: 22px" class="clssubhead">
                                            Court Time
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddl_pcrttime" runat="server" CssClass="clsInputCombo" Width="114px">
                                                <asp:ListItem Value="8:00 AM">8:00 AM</asp:ListItem>
                                                <asp:ListItem Value="8:15 AM">8:15 AM</asp:ListItem>
                                                <asp:ListItem Value="8:30 AM">8:30 AM</asp:ListItem>
                                                <asp:ListItem Value="8:45 AM">8:45 AM</asp:ListItem>
                                                <asp:ListItem Value="9:00 AM">9:00 AM</asp:ListItem>
                                                <asp:ListItem Value="9:15 AM">9:15 AM</asp:ListItem>
                                                <asp:ListItem Value="9:30 AM">9:30 AM</asp:ListItem>
                                                <asp:ListItem Value="9:45 AM">9:45 AM</asp:ListItem>
                                                <asp:ListItem Value="10:00 AM">10:00 AM</asp:ListItem>
                                                <asp:ListItem Value="10:15 AM">10:15 AM</asp:ListItem>
                                                <asp:ListItem Value="10:30 AM">10:30 AM</asp:ListItem>
                                                <asp:ListItem Value="10:45 AM">10:45 AM</asp:ListItem>
                                                <asp:ListItem Value="11:00 AM">11:00 AM</asp:ListItem>
                                                <asp:ListItem Value="11:15 AM">11:15 AM</asp:ListItem>
                                                <asp:ListItem Value="11:30 AM">11:30 AM</asp:ListItem>
                                                <asp:ListItem Value="11:45 AM">11:45 AM</asp:ListItem>
                                                <asp:ListItem Value="12:00 PM">12:00 PM</asp:ListItem>
                                                <asp:ListItem Value="12:15 PM">12:15 PM</asp:ListItem>
                                                <asp:ListItem Value="12:30 PM">12:30 PM</asp:ListItem>
                                                <asp:ListItem Value="12:45 PM">12:45 PM</asp:ListItem>
                                                <asp:ListItem Value="1:00 PM">1:00 PM</asp:ListItem>
                                                <asp:ListItem Value="1:15 PM">1:15 PM</asp:ListItem>
                                                <asp:ListItem Value="1:30 PM">1:30 PM</asp:ListItem>
                                                <asp:ListItem Value="1:45 PM">1:45 PM</asp:ListItem>
                                                <asp:ListItem Value="2:00 PM">2:00 PM</asp:ListItem>
                                                <asp:ListItem Value="2:15 PM">2:15 PM</asp:ListItem>
                                                <asp:ListItem Value="2:30 PM">2:30 PM</asp:ListItem>
                                                <asp:ListItem Value="2:45 PM">2:45 PM</asp:ListItem>
                                                <asp:ListItem Value="3:00 PM">3:00 PM</asp:ListItem>
                                                <asp:ListItem Value="3:15 PM">3:15 PM</asp:ListItem>
                                                <asp:ListItem Value="3:30 PM">3:30 PM</asp:ListItem>
                                                <asp:ListItem Value="3:45 PM">3:45 PM</asp:ListItem>
                                                <asp:ListItem Value="4:00 PM">4:00 PM</asp:ListItem>
                                                <asp:ListItem Value="4:15 PM">4:15 PM</asp:ListItem>
                                                <asp:ListItem Value="4:30 PM">4:30 PM</asp:ListItem>
                                                <asp:ListItem Value="4:45 PM">4:45 PM</asp:ListItem>
                                                <asp:ListItem Value="5:00 PM">5:00 PM</asp:ListItem>
                                                <asp:ListItem Value="5:15 PM">5:15 PM</asp:ListItem>
                                                <asp:ListItem Value="5:30 PM">5:30 PM</asp:ListItem>
                                                <asp:ListItem Value="5:45 PM">5:45 PM</asp:ListItem>
                                                <asp:ListItem Value="6:00 PM">6:00 PM</asp:ListItem>
                                                <asp:ListItem Value="6:15 PM">6:15 PM</asp:ListItem>
                                                <asp:ListItem Value="6:30 PM">6:30 PM</asp:ListItem>
                                                <asp:ListItem Value="6:45 PM">6:45 PM</asp:ListItem>
                                                <asp:ListItem Value="7:00 PM">7:00 PM</asp:ListItem>
                                                <asp:ListItem Value="7:15 PM">7:15 PM</asp:ListItem>
                                                <asp:ListItem Value="7:30 PM">7:30 PM</asp:ListItem>
                                                <asp:ListItem Value="7:45 PM">7:45 PM</asp:ListItem>
                                                <asp:ListItem Value="8:00 PM">8:00 PM</asp:ListItem>
                                                <asp:ListItem Value="8:15 PM">8:15 PM</asp:ListItem>
                                                <asp:ListItem Value="8:30 PM">8:30 PM</asp:ListItem>
                                                <asp:ListItem Value="8:45 PM">8:45 PM</asp:ListItem>
                                                <asp:ListItem Value="9:00 PM">9:00 PM</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr id="tr_CourtNo" runat="server">
                                        <td style="width: 130px; height: 22px" class="clssubhead">
                                            Court No
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tb_prm" runat="server" CssClass="clsInputadministration" Width="112px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 130px; height: 22px" class="clssubhead">
                                            Status
                                        </td>
                                        <td>
                                            <table style="width: 100%" cellspacing="0" cellpadding="0" border="0">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <asp:DropDownList ID="ddl_pstatus" runat="server" DataTextField="Description" DataValueField="ID"
                                                                CssClass="clsInputCombo" Width="135px" EnableViewState="true">
                                                            </asp:DropDownList>
                                                            <table border="0" cellpadding="0" cellspacing="0" style="display: none">
                                                                <tr>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddl_StatusCategory" runat="server" DataTextField="Description"
                                                                            DataValueField="ID" EnableViewState="true">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <asp:TextBox onkeydown="return false" ID="tb_pstatus" onkeypress="return false;"
                                                                runat="server" CssClass="clsinputadministrationlabel" Width="100%"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:ImageButton ID="ImgCrtLoc" runat="server" ImageUrl="../Images/Rightarrow.gif">
                                                            </asp:ImageButton>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr id="trMissedCourt1">
                                        <td style="width: 130px" class="clssubhead">
                                        </td>
                                        <td>
                                            <table cellspacing="0" cellpadding="0" width="100%">
                                                <tbody>
                                                    <tr style="display: none" id="trMissedCourt" runat="server">
                                                        <td style="width: 92px" class="clssubhead">
                                                            <asp:RadioButton ID="rbtnNoAction" runat="server" CssClass="Label" GroupName="grpmissedcourt"
                                                                Text="No Action"></asp:RadioButton>&nbsp;
                                                        </td>
                                                        <td style="width: 92px; height: 20px" class="clssubhead">
                                                            <asp:RadioButton ID="rbtnPledOut" runat="server" CssClass="Label" GroupName="grpmissedcourt"
                                                                Text="Pled Out"></asp:RadioButton>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 130px; height: 22px" class="clssubhead">
                                            Price Plan
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddl_ppriceplan" runat="server" DataTextField="planshortname"
                                                DataValueField="planid" CssClass="clsInputCombo" Width="115px">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 130px; height: 22px" class="clssubhead">
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:Button ID="btn_popup" OnClick="btn_popup_Click" runat="server" CssClass="clsbutton"
                                                Text="Update"></asp:Button><asp:CheckBox ID="cb_updateall" runat="server" CssClass="Label"
                                                    Text="Update All"></asp:CheckBox>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
            <asp:HiddenField ID="hf_ticketviolationid" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hf_newviolation" runat="server" Value="0"></asp:HiddenField>
            <asp:HiddenField ID="hf_temp2" runat="server" Value="0"></asp:HiddenField>
            <asp:HiddenField ID="hf_ticketsviolationid" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hf_temp1" runat="server" Value="0"></asp:HiddenField>
            <asp:HiddenField ID="hf_allowolddate" runat="server" Value="0"></asp:HiddenField>
            <asp:HiddenField ID="hf_lastrow" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hf_AutoStatusID" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hf_AutoCategoryID" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hf_AutoNo" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="pctrstatus" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hf_AutoTime" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hf_AutoCourtDate" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hf_ticketid" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hf_courtroomno" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hv_viostatus" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hf_courttime" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hf_day" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hf_Month" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hf_year" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hf_courtloc" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hf_pcname" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hf_planid" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hf_isFTAViolation" runat="server" Value="0"></asp:HiddenField>
            <asp:HiddenField ID="hf_hasFTAViolations" runat="server" Value="0"></asp:HiddenField>
            <asp:HiddenField ID="hf_coveringFirmId" runat="server" Value="0"></asp:HiddenField>
            <asp:HiddenField ID="hf_iscriminalcourt" runat="server" Value="0"></asp:HiddenField>
            <asp:HiddenField ID="hf_Level" runat="server" Value="0"></asp:HiddenField>
            <asp:HiddenField ID="hf_sstyle" runat="server" Value="none"></asp:HiddenField>
            <asp:HiddenField ID="hf_violationcheck" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hf_status" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hf_oldstatus" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hf_oldccourtdate" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hf_courtid" runat="server" Value="0"></asp:HiddenField>
            <asp:HiddenField ID="hf_oldcourtid" runat="server" Value="0"></asp:HiddenField>
            <asp:HiddenField ID="hf_hasNOS" runat="server" Value="0"></asp:HiddenField>
            <asp:HiddenField ID="hf_UpdateNOS" runat="server" Value="0"></asp:HiddenField>
            <asp:HiddenField ID="hf_vstyle" runat="server" Value="none"></asp:HiddenField>
            <asp:HiddenField ID="hf_lastcourtid" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hf_violationid" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hf_cstyle" runat="server" Value="none"></asp:HiddenField>
            <asp:HiddenField ID="hf_IsALRProcess" runat="server" Value="0"></asp:HiddenField>
            <asp:HiddenField ID="hf_casetype" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hf_missedcourttype" runat="server" Value="-1"></asp:HiddenField>
            <asp:HiddenField ID="hfIsDispositionPage" runat="server" Value="0"></asp:HiddenField>
            <asp:HiddenField ID="hf_activeflag" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hf_statusCount" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hf_PreTrialDiversion" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hf_MotionHiring" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hf_JUVNILE" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hf_IsDispositionpage" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hf_AllowCourtNumbers" runat="server"></asp:HiddenField>
        </ContentTemplate>
        <Triggers>
            <aspnew:AsyncPostBackTrigger ControlID="ddl_pcrtloc" EventName="SelectedIndexChanged" />
        </Triggers>
    </aspnew:UpdatePanel>
</div>

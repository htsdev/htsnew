using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace lntechNew.WebControls
{
    // Abid 5387 01/12/2009 Page Size Changed Handler
    public delegate void PageSizeChangedMethodHandler(int pageSize);

    // Yasir Kamal 5427 06/02/2009 Enum for pagging

    public enum GridType
    {
        /// <summary>
        /// DataGrid
        /// </summary>
        DataGrid,
        /// <summary>
        /// GridView
        /// </summary>
        GridView
        
    }

    //Yasir Kamal 5947 05/20/2009 No of Records per page
    /// <summary>
    /// All
    /// 10
    /// 20
    /// 30
    /// 40
    /// 50 
    /// 100 
    /// </summary>
    public enum RecordsPerPage
    {
        All,
        Ten = 10,
        Twenty = 20,
        Thirty = 30,
        Fourty = 40,
        Fifty = 50,
        Hundred = 100
    }



    public partial class PagingControl : System.Web.UI.UserControl
    {
               

        #region Events

        public event PageMethodHandler PageIndexChanged;
        public event PageSizeChangedMethodHandler PageSizeChanged;
       
        protected void Page_Load( object sender, EventArgs e )
        {

            // Yasir Kamal 5427 06/02/2009 Pagging for DataGrid
            
            if (grdType == GridType.DataGrid)
            {
                IsPagingVisible = (dg_records != null && dg_records.PageCount > 1);
                IsRecordsSizeVisible = (dg_records != null && PageSizeChanged != null && dg_records.PageCount > 0);
            }
            else
            {
                //Sabir Khan 4635 10/08/2008
                // Abid 5387 01/12/2009 set page control visibility
                                
                IsPagingVisible = (gv_records != null && gv_records.PageCount > 1);
                IsRecordsSizeVisible = (gv_records != null && PageSizeChanged != null && gv_records.PageCount > 0);
            }
            
                if (!IsPostBack)
                {
                    if (PageCount > 0)
                    {
                        Fill_ddlist();
                        LB_curr.Text = (PageIndex + 1).ToString();
                        

                    }
                    grdType = GridType.GridView;
                }
            
        }

        protected void DL_pages_SelectedIndexChanged(object sender, EventArgs e)
        {
            int x = Convert.ToInt32(this.DL_pages.SelectedValue);
            PageIndex = x;
            LB_curr.Text = x.ToString();

            if (PageIndexChanged != null)
                PageIndexChanged();
           

        }

        //Yasir Kamal 5947 05/20/2009 No of Records per page

        /// <summary>
        /// Sets the Size of Records Per Page
        /// </summary>
        public RecordsPerPage Size
        {
            set
            {
                DL_Records.ClearSelection();
                               
                DL_Records.SelectedValue = Convert.ToInt32(value).ToString();              
            }
        }

        // Abid 5387 01/12/2009 event handler to get page size 
        /// <summary>
        /// Page size change selected index event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DL_Records_SelectedIndexChanged( object sender, EventArgs e )
        {
            //Yasir Kamal 5427 06/02/2009 Pagging for DataGrid

            if (grdType == GridType.DataGrid)
            {
                if (dg_records != null)
                {
                    if (PageSizeChanged != null)
                        PageSizeChanged(Convert.ToInt32(DL_Records.SelectedValue));
                }

            }
            else
            {
                if (gv_records != null)
                {
                    if (PageSizeChanged != null)
                        PageSizeChanged(Convert.ToInt32(DL_Records.SelectedValue));
                }
            }
        }

        #endregion
        

        #region Variables

        // Abid 5387 01/12/2009 pagecount and pageindex field deleted
        GridView gv_records = null;
        // Yasir Kamal 5427 06/02/2009 Pagging for DataGrid
        DataGrid dg_records = null;
        public static GridType grdType = GridType.GridView ;

        #endregion

        #region Properties

        public int PageCount
        {
            set
            {               
                //Sabir Khan 4635 10/23/2008 Disable pageing control when pagecount is 0 or 1...
                // Abid 5387 01/12/2009 set page control visibility
                ViewState["PageCount"] = value;
                IsPagingVisible = ( value > 1 );
            }
            get
            {
                // Abid 5387 01/15/2009 page count
                if ( ViewState["PageCount"] == null )
                    ViewState["PageCount"] = 0;

                return Convert.ToInt32( ViewState["PageCount"] );
            }
        }

        // Yasir Kamal 5427 06/02/2009 Grid view Name changed

        public GridView GridView
        { // 5427 end 
            set
            {
                gv_records = value;

                // Abid Ali 5387 01/12/2009 Grid view page
                if ( gv_records == null )
                {
                    IsRecordsSizeVisible = false;
                }
                else if ( gv_records.AllowPaging && Convert.ToInt32( DL_Records.SelectedValue ) > 0)
                {
                    if ( gv_records != value )
                    {
                     //Yasir Kamal 5555 03/03/2009 Allow user to select 10, 20, 30, 40, 50, 100 and All
                        gv_records.PageSize = 20;
                     // 5555 end
                        gv_records.PageIndex = 0;
                    }
                    else
                    {
                        gv_records.PageSize = Convert.ToInt32( DL_Records.SelectedValue );
                    }                    
                }
            }
        }

        // Yasir Kamal 5427 06/02/2009 Pagging for DataGrid

        public DataGrid DataGrid
        {
            set
            {
                dg_records = value;
                                
                if (dg_records == null)
                {
                    IsRecordsSizeVisible = false;
                }
                else if (dg_records.AllowPaging && Convert.ToInt32(DL_Records.SelectedValue) > 0)
                {
                    if (dg_records != value)
                    {
                        //Yasir Kamal 5555 03/03/2009 Allow user to select 10, 20, 30, 40, 50, 100 and All
                        dg_records.PageSize = 20;
                        //5555 end
                        dg_records.CurrentPageIndex = 0;
                    }
                    else
                    {
                        dg_records.PageSize = Convert.ToInt32(DL_Records.SelectedValue);
                    }
                }
            }
        }
        //Yasir Kamal 5427 end

        public int PageIndex
        {
            get
            {
                // Abid 5387 01/15/2009 PageIndex
                if ( ViewState["PageIndex"] == null )
                    ViewState["PageIndex"] = 0;

                return Convert.ToInt32( ViewState["PageIndex"] );
            }
            set 
            {
                // Abid 5387 01/15/2009 PageIndex
                ViewState["PageIndex"] = value;
            }
        }

        // Abid Ali 5387 01/12/2009 Get or set paging visiblity
        /// <summary>
        /// Get or Set Paging Visible Property
        /// </summary>
        protected bool IsPagingVisible
        {
            get
            {
                if ( ViewState["IsPagingVisible"] == null )
                    ViewState["IsPagingVisible"] = true;

                return Convert.ToBoolean(ViewState["IsPagingVisible"]);
            }
            set
            {
                ViewState["IsPagingVisible"] = value;
            }
        }

        // Abid Ali 5387 01/12/2009 Get or set visibility of number of records per page
        /// <summary>
        /// Get or Set number of records per page Visible Property
        /// </summary>
        protected bool IsRecordsSizeVisible
        {
            get
            {
                if ( ViewState["IsRecordsSizeVisible"] == null )
                    ViewState["IsRecordsSizeVisible"] = false;

                return Convert.ToBoolean(ViewState["IsRecordsSizeVisible"]);
            }
            set
            {
                ViewState["IsRecordsSizeVisible"] = value;
            }
        }

        #endregion

        #region Methods

        private void Fill_ddlist()
        {

            DL_pages.Items.Clear();

            for (int i = 1; i <= PageCount; i++)
            {
                DL_pages.Items.Add(new ListItem(Convert.ToString(i), Convert.ToString(i)));
            }

            //Yasir Kamal 5427 06/02/2009 Pagging for DataGrid

            if (DL_Records.SelectedValue == "0")
            {
                PageIndex = 0;
            }
            //Yasir Kamal 5427 06/02/2009 Pagging for DataGrid
            DL_pages.SelectedValue = (PageIndex + 1).ToString();
        }

        public void SetPageIndex()
        {
            // Abid 5387 01/12/2009 set page control visibility
            IsPagingVisible = ( PageCount > 1 );

            //Yasir Kamal 5427 06/02/2009 Pagging for DataGrid

            if (grdType == GridType.DataGrid)
            {
                IsRecordsSizeVisible = (dg_records != null && PageSizeChanged != null && PageCount > 0);

                if (IsRecordsSizeVisible)
                {
                    DL_Records.ClearSelection();
                    if (dg_records.AllowPaging)
                        DL_Records.Items.FindByText(dg_records.PageSize.ToString()).Selected = true;
                    else
                        DL_Records.Items[0].Selected = true;
                }
            }
            else
            {
                IsRecordsSizeVisible = (gv_records != null && PageSizeChanged != null && PageCount > 0);

                if (IsRecordsSizeVisible)
                {
                    DL_Records.ClearSelection();
                    if (gv_records.AllowPaging)
                        DL_Records.Items.FindByText(gv_records.PageSize.ToString()).Selected = true;
                    else
                        DL_Records.Items[0].Selected = true;
                }
            }
            // Yasir 5427 end
            Fill_ddlist();
            LB_curr.Text = (PageIndex + 1).ToString();
            DL_pages.SelectedValue = (PageIndex + 1).ToString();            
        }

       
        #endregion      

    }
}
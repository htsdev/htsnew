<%@ Register TagPrefix="uc1" TagName="LoginMenu" Src="WebControls/LoginMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="WebControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuTop" Src="WebControls/MenuTop.ascx" %>

<%@ Page Language="c#" AutoEventWireup="false" Inherits="HTP.frmLogin" CodeBehind="frmLogin.aspx.cs" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>Login - Legal Nation</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <link href="Styles.css" type="text/css" rel="stylesheet" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <style type="text/css">
        .style3
        {
            font-size: 11pt;
        }
        .style4
        {
            font-size: 11px;
        }
    </style>

    <script type="text/javascript" language="javascript">
     // Noufil 4378 08/06/2008 Allow only alphabets and number in password n user name
		function CheckName(name)
        {       
            for ( i = 0 ; i < name.length ; i++)
            {
                var asciicode =  name.charCodeAt(i)
                //If not valid alphabet 
                if (  ! ((asciicode >= 64 && asciicode <=90) || ( asciicode >= 97 && asciicode <=122)||(asciicode >= 48 && asciicode <=57)))
                return false;
           }
            return true;
        }
        
		function ValidateLogin()
		{		
			if (document.LoginForm.txtUsrId.value=='')  
			{ 
				alert ("Please enter User ID.");
				document.LoginForm.txtUsrId.focus(); 
				return(false);					
			}				
			if (document.LoginForm.txt_Password.value=='')  
			{ 
				alert ("Please enter Password.");
				document.LoginForm.txt_Password.focus(); 
				return(false);					
			}
			
			if (document.getElementById("ddl_Branch").selectedIndex==0) 
			{
		        alert ("Please select your branch location to proceed");
		        document.getElementById("ddl_Branch").focus();
		        return false;
			}
			
			if (CheckName(document.getElementById("txtUsrId").value)== false)
			{
			    alert("Username must contain alphabets and numbers");
			    document.getElementById("txtUsrId").focus();
			    return false;
			}
			if (CheckName(document.getElementById("txt_Password").value)== false)
			{
			    alert("Password must contain alphabets and numbers");
			    document.getElementById("txt_Password").focus();
			    return false;
			}
		}
				
    </script>

</head>
<body onload="LoginForm.txtUsrId.focus()">
    <form id="LoginForm" method="post" runat="server">
    <table id="tblMain" cellspacing="0" cellpadding="0" width="780" align="center" border="0">
        <tr>
            <td>
                <img src="Images/lnlogo.jpg">
            </td>
        </tr>
        <tr>
            <td background="../../images/navi_repeat.jpg">
                <img height="36" width="80" src="Images/loginarea.gif">
            </td>
        </tr>
        <tr>
            <td>
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td colspan="6">
                            <table cellspacing="1" cellpadding="0" width="100%" border="0">
                                <tr>
                                    <td class="clsLeftPaddingTable" style="width: 203px; height: 20px;" align="right"
                                        width="203" >
                                        <strong>User ID:</strong>&nbsp;
                                    </td>
                                    <td align="left" width="151" class="clsLeftPaddingTable" style="height: 20px">
                                        <asp:TextBox ID="txtUsrId" runat="server" CssClass="clsInputadministration" MaxLength="20"
                                            Width="125px"></asp:TextBox>
                                    </td>
                                    <td class="clsLeftPaddingTable" align="right" width="80" style="height: 20px">
                                        <strong>Password:</strong>&nbsp;
                                    </td>
                                    <td width="160" class="clsLeftPaddingTable" style="height: 20px">
                                        &nbsp;<asp:TextBox ID="txt_Password" runat="server" CssClass="clsInputadministration"
                                            MaxLength="20" Width="125px" TextMode="Password"></asp:TextBox>
                                    </td>
                                    <td class="clsLeftPaddingTable" align="right" width="80" style="height: 20px">
                                        <strong>Branch:</strong>&nbsp;
                                    </td>
                                    <td width="160" class="clsLeftPaddingTable" style="height: 20px">
                                        &nbsp;<asp:DropDownList ID="ddl_Branch" runat="server" CssClass="clsInputCombo">
                            </asp:DropDownList>
                                    </td>
                                    <td align="left" width="180" class="clsLeftPaddingTable" style="padding-bottom:13px" >
                                        &nbsp; &nbsp;<asp:Button ID="btn_Login" runat="server" CssClass="clsbutton" Text="Login"
                                            Width="42px" OnClientClick="return ValidateLogin();" />&nbsp;&nbsp;
                                        <asp:CheckBox ID="chk_SPassword" runat="server" Checked="True" CssClass="clsLabelNew"
                                            Text="Save Password" Width="99px" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="clsLeftPaddingTable" align="center" colspan="7" height="20">
                                        <asp:Label ID="lblMsg" runat="server" Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red"
                                            Visible="False"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table id="Table5" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
                    <tr>
                        <td background="../images/separator_repeat.gif" style="height: 11px">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>   
    </form>
</body>
</html>

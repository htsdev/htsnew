<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DocketReportCR.aspx.cs" Inherits="lntechNew.Reports.DocketReportCR" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<HTML>
	<HEAD>
		<title>Docket Report[Crystal Report Version]</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<style type="text/css">.style1 { COLOR: #ffffff }
		</style>
		<script type="text/javascript">
		
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout"" >
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
				 <tr id="tr1" height=100%>
					<td colspan="3">
						<iframe tabIndex="0"  src="frmDocketReport.aspx?courtid=<%=ViewState["courtid"]%>&type=<%=ViewState["type"]%>&datefrom=<%=ViewState["datefrom"]%>&dateto=<%=ViewState["dateto"]%>&alldates=<%=ViewState["alldates"]%>&status=<%=ViewState["status"]%>&coverfirm=<%=ViewState["coverfirm"]%>&showbonds=<%=ViewState["showbonds"]%>" width="98%" height="98%" scrolling="auto" frameborder="1">
						</iframe>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>



 
				
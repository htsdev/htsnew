<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.Reports.frmRptBondReminder" Codebehind="frmRptBondReminder.aspx.cs" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<title>Bond Reminder</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<SCRIPT src="../Scripts/dates.js" type="text/javascript"></SCRIPT>
		<script language="javascript">
		function validate()
		{
			//a;
			var d1 = document.getElementById("dtFrom").value
			var d2 = document.getElementById("dtTo").value			
			
			if (d1 == d2)
				return true;
			if (compareDates(d2,'MM/dd/yyyy',d1,'MM/dd/yyyy')==false)
				{
					alert("start date cannot greater than end date");
					document.getElementById("dtTo").focus(); 
					return false;
				}
				return true;
		}
	</script>
</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE id="tblMain" cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
				<TR>
					<TD align="left">
						<uc1:activemenu id="ActiveMenu1" runat="server"></uc1:activemenu></TD>
				</TR>
				<TR>
					<TD align="left">
						<table class="clsmainhead" id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<!--<tr>
								<td width="5%" height="20"><IMG height="17" src="../../images/head_icon.gif"></td>
								<td vAlign="baseline" width="90%" height="20">
									<STRONG><font color="#3366cc"> Bond Reminder </font></STRONG>
								</td>
							</tr>-->
							<tr >
								<td width="100%" background="../../images/separator_repeat.gif" colSpan="2" height="11"></td>
							</tr>
						</table>
						<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD class="GrdItem" width="53" style="height: 31px">&nbsp;From:</TD>
								<TD style="width: 140px; height: 31px" align="center"><ew:calendarpopup id="dtFrom" runat="server" ImageUrl="../images/calendar.gif" Font-Size="8pt" ToolTip="Select start date"
										PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00" Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True"
										CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Font-Names="Tahoma" Width="80px">
										<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
										<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></WeekdayStyle>
										<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></MonthHeaderStyle>
										<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
											BackColor="AntiqueWhite"></OffMonthStyle>
										<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></GoToTodayStyle>
										<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGoldenrodYellow"></TodayDayStyle>
										<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Orange"></DayHeaderStyle>
										<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGray"></WeekendStyle>
										<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></SelectedDateStyle>
										<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></ClearDateStyle>
										<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></HolidayStyle>
									</ew:calendarpopup></TD>
								<TD class="GrdItem" align="left" style="width: 25px; height: 31px">&nbsp;To:&nbsp;</TD>
								<TD align="center" style="width: 126px; height: 31px"><ew:calendarpopup id="dtTo" runat="server" ImageUrl="../images/calendar.gif" Font-Size="8pt" ToolTip="Select ending date"
										PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00" Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Bottom"
										ControlDisplay="TextBoxImage" Font-Names="Tahoma" Width="80px">
										<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
										<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></WeekdayStyle>
										<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></MonthHeaderStyle>
										<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
											BackColor="AntiqueWhite"></OffMonthStyle>
										<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></GoToTodayStyle>
										<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGoldenrodYellow"></TodayDayStyle>
										<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Orange"></DayHeaderStyle>
										<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGray"></WeekendStyle>
										<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></SelectedDateStyle>
										<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></ClearDateStyle>
										<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></HolidayStyle>
									</ew:calendarpopup>&nbsp;</TD>
								<TD class="GrdItem" align="left" width="116" style="height: 31px">&nbsp;Client Type:</TD>
								<TD align="left" width="201" style="height: 31px"><asp:dropdownlist id="ddlClientType" runat="server" Width="137px" CssClass="clslabel">
										<asp:ListItem Value="2">All</asp:ListItem>
										<asp:ListItem Value="1">Client</asp:ListItem>
										<asp:ListItem Value="0">Quote</asp:ListItem>
									</asp:dropdownlist></TD>
								<TD align="left" width="143" style="height: 31px"><asp:button id="btnSubmit" runat="server" Width="58px" CssClass="clsbutton" Height="20px" Text="Submit"></asp:button>&nbsp;<asp:button id="btnReset" runat="server" Width="56px" CssClass="clsbutton" Text="Reset"></asp:button></TD>
								<TD align="left" width="88" style="height: 31px"></TD>
							</TR>
							<TR align="center">
								<TD align="center" colSpan="8"><asp:label id="lblMessage" runat="server" ForeColor="Red"></asp:label></TD>
							</TR>
							<TR background="../../images/separator_repeat.gif">
								<TD background="../../images/separator_repeat.gif" colSpan="8" height="10"></TD>
							</TR>
							<TR>
								<TD colSpan="8"><asp:panel id="Panel1" runat="server" Width="780px">
            <TABLE cellSpacing=0 cellPadding=0 width=780>
              <TR>
                <TD background=../../images/headbar_headerextend.gif colSpan=2 
                height=5></TD></TR>
              <TR>
                <TD class=clssubhead width="85%" 
                background=../../images/headbar_midextend.gif 
                  height=18>&nbsp;Bond Reminder</TD>
                <TD class=clssubhead width="15%" 
                background=../../images/headbar_midextend.gif height=18>
                  <TABLE id=tblPageNavigation cellSpacing=0 cellPadding=0 
                  width="100%" border=0>
                    <TR>
                      <TD align=right>
<asp:label id=lblCurrPage runat="server" Width="88px" Font-Names="Verdana" Font-Size="8.5pt" CssClass="clslabel" Visible="False">Current Page :</asp:label></TD>
                      <TD align=left>
<asp:label id=lblPNo runat="server" Width="48px" Font-Names="Verdana" Font-Size="8.5pt" CssClass="clslabel" Visible="False"></asp:label></TD>
                      <TD align=right>
<asp:label id=lblGoto runat="server" Width="16px" Font-Names="Verdana" Font-Size="8.5pt" CssClass="clslabel" Visible="False">Goto</asp:label></TD>
                      <TD align=right>
<asp:dropdownlist id=cmbPageNo runat="server" Font-Size="Smaller" CssClass="clslabel" Visible="False" AutoPostBack="True"></asp:dropdownlist></TD></TR></TABLE></TD></TR>
              <TR>
                <TD background=../../images/headbar_footerextend.gif 
                  colSpan=2><IMG height=10 src="images/headbar_footerextend.gif" 
                  width=9></TD></TR></TABLE>
<asp:DataGrid id=dgClient runat="server" Width="780px" GridLines="None" ShowHeader="False" AutoGenerateColumns="False" AllowPaging="True" PageSize="5">
											<Columns>
												<asp:TemplateColumn>
													<ItemTemplate>
														<TABLE>
															<TR>
																<TD height="64">
																	<TABLE id="tblDataMain" cellSpacing="1" cellPadding="0" width="780" border="0">
																		<TR>
																			<TD vAlign="top" width="45%" height="64">
																				<TABLE id="tblClient" cellSpacing="1" cellPadding="0" width="100%" border="0">
																					<TR>
																						<TD class="clsleftpaddingtable" height="16">&nbsp;Name:</TD>
																						<TD class="clsleftpaddingtable" height="16">
																							<asp:HyperLink id=hlkclientName runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ClientName") %>'>
																							</asp:HyperLink>
																							<asp:Label id=lblTicketId runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.TicketId") %>' Height="16px" Visible="False">
																							</asp:Label></TD>
																					</TR>
																					<TR>
																						<TD class="clsleftpaddingtable" height="16">&nbsp;Court Date:</TD>
																						<TD class="clsleftpaddingtable" height="16">
																							<asp:Label id=lblCourtDateTime runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.courtdate") %>' Height="16px">
																							</asp:Label>
																							<asp:Label id="Label1" runat="server" CssClass="label">-</asp:Label>
																							<asp:Label id=lblCaseStatus runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.casestatus") %>' Height="16px" Font-Bold="True">
																							</asp:Label></TD>
																					</TR>
																					<TR>
																						<TD class="clsleftpaddingtable" height="16">&nbsp;Address:</TD>
																						<TD class="clsleftpaddingtable" height="16">
																							<asp:Label id=lblAddress runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.courtaddress") %>' Height="16px">
																							</asp:Label></TD>
																					</TR>
																					<TR>
																						<TD class="clsleftpaddingtable" height="16">&nbsp;Language:</TD>
																						<TD class="clsleftpaddingtable" height="16">
																							<asp:Label id=lblLanguage runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.language") %>' Height="16px">
																							</asp:Label></TD>
																					</TR>
																				</TABLE>
																			</TD>
																			<TD vAlign="top" align="left" width="25%" height="64">
																				<TABLE id="tblContact" cellSpacing="1" cellPadding="0" width="100%" border="0">
																					<TR>
																						<TD class="clsleftpaddingtable" height="16">
																							<asp:Label id="lblContact1" runat="server" CssClass="label" Height="16px"></asp:Label></TD>
																					</TR>
																					<TR>
																						<TD class="clsleftpaddingtable" height="16">
																							<asp:Label id="lblContact2" runat="server" CssClass="label" Height="16px"></asp:Label></TD>
																					</TR>
																					<TR>
																						<TD class="clsleftpaddingtable" height="16">
																							<asp:Label id="lblContact3" runat="server" CssClass="label" Height="16px"></asp:Label></TD>
																					</TR>
																					<TR>
																						<TD class="clsleftpaddingtable" height="16">
																							<asp:Label id=lblContact runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.contact") %>' Height="16px" Visible="False">
																							</asp:Label></TD>
																					</TR>
																				</TABLE>
																			</TD>
																			<TD vAlign="top" align="left" width="30%" height="64">
																				<TABLE id="tblComments" cellSpacing="1" cellPadding="0" width="100%" border="0">
																					<TR>
																						<TD class="clsleftpaddingtable" height="16">Comments:</TD>
																					</TR>
																					<TR>
																						<TD class="clsleftpaddingtable" vAlign="top" height="48">
																							<asp:TextBox id=txtcomments runat="server" Width="100%" CssClass="clsInputcommentsfield" Text='<%# DataBinder.Eval(Container, "DataItem.comments") %>' Height="48px" ReadOnly="True" TextMode="MultiLine">
																							</asp:TextBox></TD>
																					</TR>
																				</TABLE>
																			</TD>
																		</TR>
																	</TABLE>
																</TD>
															</TR>
															<TR>
																<TD width="100%">
																	<TABLE id="tblSeparator">
																		<TR background="../../images/separator_repeat.gif">
																			<TD width="780" background="../../images/separator_repeat.gif" height="10"></TD>
																		</TR>
																	</TABLE>
																</TD>
															</TR>
														</TABLE>
													</ItemTemplate>
												</asp:TemplateColumn>
											</Columns>
											<PagerStyle NextPageText="Next &gt;&gt;" PrevPageText="&lt;&lt; Previous" HorizontalAlign="Center"></PagerStyle>
										</asp:DataGrid>
									</asp:panel></TD>
							</TR>
						</TABLE>
						<TABLE id="tblGrid" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<tr >
								<td width="100%" background="../../images/separator_repeat.gif" colSpan="2" height="11"></td>
							</tr>
				<TR>
					<TD ><uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
					</TR>
				<TR>
					<TD align="left"></TD>
				</TR>
				<TR>
					<TD align="left"></TD>
				</TR>
				<TR>
					<TD align="left"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>

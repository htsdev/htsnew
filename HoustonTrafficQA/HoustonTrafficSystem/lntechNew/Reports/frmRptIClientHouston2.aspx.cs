using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using lntechNew.Components.ClientInfo;
using HTP.Components;

namespace HTP.Reports
{
    public partial class frmRptIClientHouston2 : System.Web.UI.Page
    {
        clsSession ClsSession = new clsSession();
        DataView DV;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (ClsSession.IsValidSession(this.Request) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else
                {
                    if (!IsPostBack)
                    {
                        calDateTo.SelectedDate = DateTime.Now;
                        calDateFrom.SelectedDate = DateTime.Now;
                    }
                    PagingControl1.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(PagingControl1_PageIndexChanged);
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            Session["Ch2"] = null;
            FillGrid();
        }

        private void FillGrid()
        {
            try
            {
                lbl_Message.Text = "";
                lblMessage.Text = "";
                DataTable dt = SullolawReports.GetTransactionDetail(calDateFrom.SelectedDate.ToShortDateString(), calDateTo.SelectedDate.ToShortDateString(), "1");

                if (dt != null && dt.Rows.Count > 0)
                {

                    if (Session["Ch2"] == null)
                    {
                        DV = new DataView(dt);
                        Session["Ch2"] = DV;
                        GenerateSerial(dt);
                    }
                    else
                    {
                        DV = (DataView)Session["Ch2"];
                    }

                    gv_Results.DataSource = DV;
                    gv_Results.DataBind();
                    gv_Results.Visible = true;

                    PagingControl1.PageIndex = gv_Results.PageIndex;
                    PagingControl1.PageCount = gv_Results.PageCount;
                    PagingControl1.SetPageIndex();

                }
                else
                {
                    lblMessage.Text = "No Record Found";
                    gv_Results.Visible = false;
                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }


        }

        private void GenerateSerial(DataTable dt)
        {

            int no = 1;

            if (dt.Columns["SNO"] == null)
            {
                dt.Columns.Add("SNO");
                dt.Rows[0]["SNO"] = no;
            }
            else
            {
                dt.Columns.Remove("SNO");
                dt.Columns.Add("SNO");
                dt.Rows[0]["SNO"] = no;
            }
            for (int i = 1; i < dt.Rows.Count; i++)
            {

                no++;
                dt.Rows[i]["SNO"] = no;
            }

        }



        void PagingControl1_PageIndexChanged()
        {
            gv_Results.PageIndex = PagingControl1.PageIndex - 1;
            FillGrid();
        }

        protected void gv_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gv_Results.PageIndex = e.NewPageIndex;
            FillGrid();
        }





    }
}

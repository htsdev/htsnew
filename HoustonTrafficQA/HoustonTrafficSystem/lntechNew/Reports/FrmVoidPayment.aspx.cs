using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.Reports
{
	/// <summary>
	/// Summary description for FrmIDScreen.
	/// </summary>
	public partial class FrmVoidPayment : System.Web.UI.Page
	{
		clsENationWebComponents ClsDb = new clsENationWebComponents();	
		clsSession cSession =new clsSession(); 	
		clsLogger bugTracker = new clsLogger();
		protected System.Web.UI.WebControls.Label lblMessage;
		protected System.Web.UI.WebControls.ImageButton Btn_search;
		protected System.Web.UI.WebControls.ImageButton btn_Reset;
		protected System.Web.UI.WebControls.Label Label_todate;
		protected System.Web.UI.WebControls.DataGrid dg_VoidReport;
		protected eWorld.UI.CalendarPopup Calendarpopup2;
		protected eWorld.UI.CalendarPopup Calendarpopup1;
		protected System.Web.UI.WebControls.Label label_from;				
		DataSet ds1;		
		string StrAcsDec;
        //ozair test 04/21/2009
		//protected MagicAjax.UI.Controls.AjaxPanel AjaxPanel1;
		protected System.Web.UI.WebControls.LinkButton lnk;
		string StrExp;
		clsSession ClsSession=new clsSession();
		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				if (ClsSession.IsValidSession(this.Request)==false)
				{
					Response.Redirect("../frmlogin.aspx",false);
				}
				else //To stop page further execution
				{
					if (Page.IsPostBack) 
					{
						dg_VoidReport.Attributes["ajaxcall"] = "sync";
						FillGrid();
					}	
				}
			}
			catch(Exception ex)
			{
				lblMessage.Text=ex.Message;
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
			
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Btn_search.Click += new System.Web.UI.ImageClickEventHandler(this.Btn_search_Click);
			this.btn_Reset.Click += new System.Web.UI.ImageClickEventHandler(this.btn_Reset_Click);
			this.dg_VoidReport.ItemCreated += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dg_VoidReport_ItemCreated);
			this.dg_VoidReport.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_VoidReport_ItemCommand);
			this.dg_VoidReport.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dg_VoidReport_PageIndexChanged);
			this.dg_VoidReport.SortCommand += new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.dg_VoidReport_SortCommand);
			this.dg_VoidReport.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dg_VoidReport_ItemDataBound);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		//Method to fill the grid
		public void FillGrid()
		{
			try
			{	
				dg_VoidReport.Visible=true;				
				string[] key    = {"@sdate","@edate"};
				object[] value1 = {Calendarpopup1.SelectedDate,Calendarpopup2.SelectedDate};
				ds1 = ClsDb.Get_DS_BySPArr("Sp_VoidReport",key,value1);		
				dg_VoidReport.DataSource = ds1;						
				lblMessage.Text = "";
				if (ds1.Tables[0].Rows.Count  < 1 )
				{
					lblMessage.Text = "No record found";
				}
			}
			catch(Exception ex)
			{
				lblMessage.Text=ex.Message;
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}    		
        //Method to generate S# for Data Grid		
		private void BindReport()
		{		
			long sNo=(dg_VoidReport.CurrentPageIndex)*(dg_VoidReport.PageSize);									
			try
			{
				foreach (DataGridItem ItemX in dg_VoidReport.Items) 
				{ 					
					sNo +=1 ;
					((LinkButton)(ItemX.FindControl("lblSno"))).Text= sNo.ToString();
				}
			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message ;
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}    
		//Search Button Click Event	
		private void Btn_search_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			try
			{
				dg_VoidReport.CurrentPageIndex=0;								
				FillGrid();
				dg_VoidReport.DataBind();				
         		BindReport();	
			}
			catch(Exception ex)
			{				
				lblMessage.Text=ex.Message;
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}
        //Reset Button Click Event
		private void btn_Reset_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			Calendarpopup1.SelectedDate=DateTime.Now.Date;
			Calendarpopup2.SelectedDate=DateTime.Now.Date;	
			dg_VoidReport.Visible=false;
			lblMessage.Text="";
		}
        
		
		//Data Grid Page Index Event
		private void dg_VoidReport_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			try
			{
				dg_VoidReport.CurrentPageIndex=e.NewPageIndex;			
				FillGrid();					
				dg_VoidReport.DataBind();				
				BindReport();
			}
			catch (Exception ex)
			{
				lblMessage.Text=ex.Message;
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}
        //Data Grid Sort Event	
		private void dg_VoidReport_SortCommand(object source, System.Web.UI.WebControls.DataGridSortCommandEventArgs e)
		{
			string SortNm = e.SortExpression;			
			SetAcsDesc(SortNm);
			SortDataGrid(SortNm);		
		}
		//Method to set 'ASC' 'DESC'
		private void SetAcsDesc (string val)
		{
			try
			{
				StrExp = Session["StrExp"].ToString();
				StrAcsDec = Session["StrAcsDec"].ToString();
			}
			catch
			{

			}

			if (StrExp == val)
			{
				if (StrAcsDec == "ASC")
				{
					StrAcsDec = "DESC";
					Session["StrAcsDec"] = StrAcsDec ;
				}
				else 
				{
					StrAcsDec = "ASC";
					Session["StrAcsDec"] = StrAcsDec ;
				}
			}
			else 
			{
				StrExp = val;
				StrAcsDec = "ASC";
				Session["StrExp"] = StrExp ;
				Session["StrAcsDec"] = StrAcsDec ;				
			}
		}
		//Method to sort Data Grid
		private void SortDataGrid(string Snm)
		{			
			DataView dv=ds1.Tables[0].DefaultView;
			dv.Sort=Snm+" "+StrAcsDec;
			dg_VoidReport.DataSource=dv;
			dg_VoidReport.DataBind();
			BindReport();
		}

		private void dg_VoidReport_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			if (e.CommandName == "MkSession")
			{	
			
				LinkButton lnkbtn = (LinkButton) e.Item.FindControl("HLTicketno");  //e.Item.Cells[0].Controls[0];
				
				Response.Redirect("../ClientInfo/NewPaymentInfo.aspx?search=0&casenumber="+lnkbtn.Text);
			}
		}

		private void dg_VoidReport_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			
		}

		private void dg_VoidReport_ItemCreated(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if(e.Item.ItemType==ListItemType.Item||e.Item.ItemType==ListItemType.AlternatingItem)
			{
				LinkButton lnkbtn = (LinkButton) e.Item.FindControl("HLTicketno");
				lnkbtn.Attributes["ajaxcall"] = "sync";
				LinkButton lnkbtn1 = (LinkButton) e.Item.FindControl("lblSno");
				lnkbtn1.Attributes["ajaxcall"] = "sync";
			}
		}
	}
}

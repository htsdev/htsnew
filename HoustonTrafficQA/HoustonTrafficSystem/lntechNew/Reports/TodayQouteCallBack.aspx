<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TodayQouteCallBack.aspx.cs"
    Inherits="HTP.Reports.TodayQouteCallBack" %>

<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc4" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Quote Follow Up Calls(Initial Contact)</title>
    <link href="../Styles.css" type="text/css" rel="stylesheet" />

    <script src="../Scripts/Dates.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
    
    //This Method used to validate the input dates from the user 
     function CheckDateValidation()
        {
            if (IsDatesEqualOrGrater(document.form1.calQueryFrom.value,'MM/dd/yyyy','01/01/1900','MM/dd/yyyy')==false)
            {
			    alert("Please enter valid date, Start Date must be greater than or equal to 1/1/1900");
				return false;
			}
			
			if (IsDatesEqualOrGrater(document.form1.calQueryTo.value,'MM/dd/yyyy','01/01/1900','MM/dd/yyyy')==false)
            {
			    alert("Please enter valid date, End Date must be greater than or equal to 1/1/1900");
				return false;
			}
            if (IsDatesEqualOrGrater(document.form1.calQueryTo.value,'MM/dd/yyyy',document.form1.calQueryFrom.value,'MM/dd/yyyy')==false)
            {
			    alert("Please enter valid date, End Date must be greater than or equal to Start Date");
				return false;
			}
			
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <aspnew:ScriptManager ID="ScriptManager1" runat="server" />
    <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline">
        <ContentTemplate>
            <div>
                <table id="TableMain" cellspacing="0" cellpadding="0" width="820px" align="center"
                    border="0">
                    <tr>
                        <td>
                            <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/separator_repeat.gif" colspan="7" height="11">
                        </td>
                    </tr>
                    <tr>
                        <td class="clsLeftPaddingTable" colspan="7">
                            <table id="tbl" cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr>
                                    <td align="left" style="width: 70px">
                                        <asp:Label ID="lblStartDate" runat="server" CssClass="clssubhead">Start Date</asp:Label>
                                    </td>
                                    <td style="width: 120px" align="left">
                                        <ew:CalendarPopup ID="calQueryFrom" runat="server" Width="90px" ImageUrl="../images/calendar.gif"
                                            Font-Names="Tahoma" Font-Size="8pt" ControlDisplay="TextBoxImage" CalendarLocation="Bottom"
                                            ShowGoToToday="True" AllowArbitraryText="False" Culture="(Default)" UpperBoundDate="12/31/9999 23:59:00"
                                            PadSingleDigits="True" ToolTip="Select Report Date Range" SelectedDate="2005-09-01"
                                            DisableTextboxEntry="true" ClearDateText="">
                                            <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                            <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                BackColor="White"></WeekdayStyle>
                                            <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                            <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                                BackColor="AntiqueWhite"></OffMonthStyle>
                                            <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                            <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                            <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                            <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                BackColor="LightGray"></WeekendStyle>
                                            <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                            <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                ForeColor="Black" BackColor="White"></ClearDateStyle>
                                            <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                BackColor="White"></HolidayStyle>
                                        </ew:CalendarPopup>
                                    </td>
                                    <td style="width: 70px" align="right">
                                        <asp:Label ID="lblEndDate" runat="server" CssClass="clssubhead">End Date</asp:Label>
                                    </td>
                                    <td style="width: 120px" align="right">
                                        <ew:CalendarPopup ID="calQueryTo" runat="server" Width="90px" ImageUrl="../images/calendar.gif"
                                            Font-Names="Tahoma" Font-Size="8pt" ControlDisplay="TextBoxImage" CalendarLocation="Bottom"
                                            ShowGoToToday="True" AllowArbitraryText="False" Culture="(Default)" UpperBoundDate="12/31/9999 23:59:00"
                                            PadSingleDigits="True" ToolTip="Select Report Date Range" SelectedDate="2006-09-01"
                                            DisableTextboxEntry="true">
                                            <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                            <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                BackColor="White"></WeekdayStyle>
                                            <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                            <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                                BackColor="AntiqueWhite"></OffMonthStyle>
                                            <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                            <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                            <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                            <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                BackColor="LightGray"></WeekendStyle>
                                            <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                            <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                ForeColor="Black" BackColor="White"></ClearDateStyle>
                                            <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                BackColor="White"></HolidayStyle>
                                        </ew:CalendarPopup>
                                    </td>
                                    <td align="left">
                                        &nbsp;&nbsp;&nbsp;<asp:Button ID="btn_submit" runat="server" Text="Submit" CssClass="clsbutton"
                                            OnClientClick="CheckDateValidation()" OnClick="btn_submit_Click"></asp:Button>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/separator_repeat.gif" colspan="7" height="11">
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 34px" background="../Images/subhead_bg.gif">
                            <table>
                                <tr>
                                    <td align="left" class="clssubhead" width="50%">
                                        Quote Follow Up Calls(Initial Contact)
                                    </td>
                                    <td align="right" width="50%">
                                        <uc4:PagingControl ID="Pagingctrl" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Label ID="lbl_Message" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlResult">
                                <ProgressTemplate>
                                    <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl1" runat="server" Text="Please Wait ......"
                                        CssClass="clsLabel"></asp:Label>
                                </ProgressTemplate>
                            </aspnew:UpdateProgress>
                            <asp:GridView ID="gv_Data" runat="server" AutoGenerateColumns="False" CssClass="clsLeftPaddingTable"
                                Width="100%" AllowPaging="True" OnPageIndexChanging="gv_Data_PageIndexChanging"
                                PageSize="20">
                                <Columns>
                                    <asp:TemplateField HeaderText="S.No" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="sno" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + Eval("ticketid_pk") %>'
                                                Text='<%# Eval("sno") %>'></asp:HyperLink>
                                            <asp:HiddenField ID="hf_ticketid" runat="server" Value='<%# Eval("TicketID_PK") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Client Name" ControlStyle-Width="140px" HeaderStyle-CssClass="clssubhead"
                                        ItemStyle-Width="100px" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_Client" runat="server" CssClass="clsLeftPaddingTable" Text='<%# Eval("Customer") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Contact" ControlStyle-Width="100px" HeaderStyle-CssClass="clssubhead"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStatus" runat="server" CssClass="clsLeftPaddingTable" Text='<%# Eval("LastContactDate")+ "("+ Eval("Days") +")" %>'>															
                                            </asp:Label>
                                        </ItemTemplate>
                                        <%--<ItemStyle Width="200px" />--%>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Court Date" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_CourtDateMain" runat="server" CssClass="clsLeftPaddingTable" Text='<%# Eval("CourtDateMain") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Court Location" HeaderStyle-CssClass="clssubhead"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_shortcourtname" runat="server" CssClass="clsLeftPaddingTable"
                                                Text='<%# Eval("shortcourtname") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Qoute" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_Qoute" runat="server" CssClass="clsLeftPaddingTable" Text='<%# Eval("calculatedtotalfee", "{0:C0}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Follow Up Status" HeaderStyle-CssClass="clssubhead"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_QuoteComments" runat="server" CssClass="clsLeftPaddingTable" Text='<%# Eval("QuoteResultDescription") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Call Back Date" HeaderStyle-CssClass="clssubhead"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_CallBackDate" runat="server" CssClass="clsLeftPaddingTable" Text='<%# Eval("CallBackDate") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_STATUS" runat="server" CssClass="clsLeftPaddingTable" Text='<%# Eval("STATUS") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Case Type" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_CaseType" runat="server" CssClass="clsLeftPaddingTable" Text='<%# Eval("CaseType") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle HorizontalAlign="Center" />
                                <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                    FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/separator_repeat.gif" colspan="7" height="11">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc1:Footer ID="Footer1" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
        <%--<Triggers>
                <aspnew:AsyncPostBackTrigger ControlID="Pagingctrl$DL_Records" EventName="SelectedIndexChanged" />
            </Triggers>--%>
    </aspnew:UpdatePanel>
    </form>
</body>
</html>

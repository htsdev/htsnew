using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace HTP.Reports
{
    public partial class PotentialVisaClientsHistory : System.Web.UI.Page
    {

        #region Variables

        clsENationWebComponents clsDB = new clsENationWebComponents();
        clsLogger bugTracker = new clsLogger();
        clsCase ClsCase = new clsCase();
        clsSession cSession = new clsSession();
        int pageindex = 0;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    txtDOB.SelectedDate = DateTime.Now;
                    txtDateOfInquiry.SelectedDate = DateTime.Now;
                    getImmigrationCommentStatus();
                    GetInteretSignUpRecords("");
                }
                // Noufil 5524 03/16/2009 Paging control added
                Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
                Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
                Pagingctrl.GridView = gv_records;
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void gv_records_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            
        }

        protected void gv_records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gv_records.PageIndex = e.NewPageIndex;
            pageindex = e.NewPageIndex * gv_records.PageSize;
            GetInteretSignUpRecords("");
        }

        protected void gv_records_Sorting(object sender, GridViewSortEventArgs e)
        {
            GetInteretSignUpRecords(e.SortExpression);
        }

        protected void btn_search_Click(object sender, EventArgs e)
        {
            try
            {
                gv_records.PageIndex = 0;
                GetInteretSignUpRecords("");
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        #endregion

        #region Methods

        public void GetInteretSignUpRecords(string SortExpression)
        {

            //Zeeshan Ahmed 3931 06/10/2008 Change Internet Signup Procedure
            string[] keys = { "@DOB", "@DOBFilter", "@InquiryDate", "@IDFilter", "@CaseStatus" };
            object[] values = {
                                  txtDOB.SelectedDate, cbDOBFilter.Checked, txtDateOfInquiry.SelectedDate, cbDOIFilter.Checked,
                                  ddlCaseStatus.SelectedValue
                              };

            DataSet records = clsDB.Get_DS_BySPArr("USP_HTS_Get_PotentialVisaClientHistory", keys, values);

            // Haris Ahmed 6217 08/06/2012 Fix issues
            lblMessage.Text = "";
            lblError.Text = "";
            if (SortExpression != "")
            {
                if (Session[SortExpression] != null)
                {
                    if (Convert.ToString(Session[SortExpression]) == "ASC")
                        Session[SortExpression] = "DESC";
                    else
                        Session[SortExpression] = "ASC";
                }
                else
                    Session[SortExpression] = "ASC";

                DataView dv = new DataView(records.Tables[0]);
                dv.Sort = SortExpression + " " + Session[SortExpression];
                gv_records.DataSource = dv;
                gv_records.DataBind();
                Pagingctrl.PageCount = gv_records.PageCount;
                Pagingctrl.PageIndex = gv_records.PageIndex;
                Pagingctrl.SetPageIndex();
                if (records.Tables[0].Rows.Count < 1)
                {
                    lblMessage.Text = "No Record(s) Found";
                }
            }
            else
            {
                gv_records.DataSource = records.Tables[0];
                gv_records.DataBind();
                Pagingctrl.PageCount = gv_records.PageCount;
                Pagingctrl.PageIndex = gv_records.PageIndex;
                Pagingctrl.SetPageIndex();
                if (records.Tables[0].Rows.Count < 1)
                {
                    lblMessage.Text = "No Record(s) Found";
                }
            }

        }
       
        void Pagingctrl_PageIndexChanged()
        {
            gv_records.PageIndex = Pagingctrl.PageIndex - 1;
            GetInteretSignUpRecords("");
        }

        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            if (pageSize > 0)
            {
                gv_records.PageIndex = 0;
                gv_records.PageSize = pageSize;
                gv_records.AllowPaging = true;
            }
            else
            {
                gv_records.AllowPaging = false;
            }
            GetInteretSignUpRecords("");
        }

        #endregion

        protected void gv_records_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (ddlUpdateCaseStatus.SelectedValue != "0")
            {
                ClsCase.UpdateImmigrationCaseStatus(Convert.ToInt32(hfTicketId.Value), Convert.ToInt32(ddlUpdateCaseStatus.SelectedValue));
                mpeShowPolm.Hide();
                GetInteretSignUpRecords("");
            }
            else
            {
                lblError.Text = "Please select updated Case Status";
                mpeShowPolm.Show();
            }
        }
        
        public void getImmigrationCommentStatus()
        {
            try
            {
                //Zeeshan Ahmed 3979 05/16/2008 Add Speeding List For Criminal Case
                DataSet dsCaseStatus = ClsCase.GetImmigrationCommentStatus();

                ddlCaseStatus.Items.Clear();
                ddlUpdateCaseStatus.Items.Clear();

                ddlCaseStatus.DataSource = dsCaseStatus.Tables[0];
                ddlUpdateCaseStatus.DataSource = dsCaseStatus.Tables[0];

                ddlCaseStatus.DataTextField = "Status";
                ddlCaseStatus.DataValueField = "StatusID";

                ddlUpdateCaseStatus.DataTextField = "Status";
                ddlUpdateCaseStatus.DataValueField = "StatusID";

                ddlCaseStatus.DataBind();
                ddlUpdateCaseStatus.DataBind();

                ddlCaseStatus.Items.Insert(0, new ListItem("All non rejected/closed cases", "0"));
                ddlUpdateCaseStatus.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;

                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

    }
}

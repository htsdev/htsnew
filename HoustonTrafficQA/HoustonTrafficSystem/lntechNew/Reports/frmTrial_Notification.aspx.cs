using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.Reports
{
	/// <summary>
	/// Summary description for Trial_Notification1.
	/// </summary>
	public partial class frmTrial_Notification : System.Web.UI.Page
	{		
		clsCrsytalComponent ClsCr = new clsCrsytalComponent();
		clsLogger clog = new clsLogger();
		clsSession uSession = new clsSession();
		int TicketIDList;
		int empid;
		
		private void Page_Load(object sender, System.EventArgs e)
		{
			
			TicketIDList = Convert.ToInt32(Request.QueryString["casenumber"]);
			empid =Convert.ToInt32(uSession.GetCookie("sEmpID",this.Request));
			Create_TrialNotification_Report();
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		public void Create_TrialNotification_Report()
		{			
			try
			{
				string [] key2 = {"@TicketIDList","@empid"};
				object [] value12 = {TicketIDList,empid};
				Int32 LetterType = 2;				
				string filename  = Server.MapPath("") + "\\Trial_Notification.rpt";				
				string filepath=ClsCr.CreateReport_Retfname(filename,"USP_HTS_Trialletter",key2,value12,Session["Batch"].ToString() ,LetterType,this.Session, this.Response);
				Session["Tfilepath"]=filepath;
			}
			catch(Exception ex)
			{
				Response.Write(ex.Message);
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}		
	}
}

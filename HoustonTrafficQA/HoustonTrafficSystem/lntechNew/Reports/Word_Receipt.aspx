<%@ Page Language="c#" AutoEventWireup="false" Inherits="HTP.Reports.Word_Receipt"
    CodeBehind="Word_Receipt.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>Word_Receipt</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <link href="../Styles.css" type="text/css" rel="stylesheet">
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>

    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">

    <script>
		    window.resizeTo (screen.availWidth-300, screen.availHeight-100);
                
          //Mohammad Ali 8132 23/02/2012 If user input e-mail address in invalid format then need to display the message.                 
          function Validate2()
           {
               var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;               
               var strEmail = document.getElementById("txt_to").value;
                  
               if(strEmail == "")
               {
                    ShowEmail();
                    hide_pnl_Email2()
                    alert('Please enter To Email Address');                    
                    document.getElementById("txt_to").focus();
                    return false;
               }
               else if(reg.test(strEmail) == false)
                {   
                    ShowEmail();
                    hide_pnl_Email2()
                    alert('Please enter To Email Address in Correct format');                    
                    document.getElementById("txt_to").focus();
                    return false;
                }
               
               strEmail = document.getElementById("txt_cc").value;
                  
               if(strEmail != "")
               {
                    if(reg.test(strEmail) == false)
                    {   
                        ShowEmail();
                        hide_pnl_Email2()
                        alert('Please enter CC Email Address in Correct format');                    
                        document.getElementById("txt_cc").focus();
                        return false;
                    }
               }
               
               strEmail = document.getElementById("txt_bcc").value;
                  
               if(strEmail != "")
               {
                    if(reg.test(strEmail) == false)
                    {   
                        ShowEmail();
                        hide_pnl_Email2()
                        alert('Please enter BCC Email Address in Correct format');                    
                        document.getElementById("txt_bcc").focus();
                        return false;
                    }
               }
            }             
                
       function hide_pnl_Email2()
		{	
		    var img = document.getElementById("tdWait");		        
		     img.style.display = "none";    		    
	    	
		}
       
       //End of 8132
                
		function refresh()
		{
		//a;
		 //window.opener.location.reload(); //onload="refresh();"
		 opener.location.href = opener.location;
		 var flag_email= "<% = Request.QueryString["EmailFlag"] %>";
		 if(flag_email=="1")
		 ShowEmail();
		 setTimeout(function(){ifrLoad()}, 5000);
		}
		function setdefault()
		{
			var rowhide=document.getElementById("tremail");  //onload="setdefault();"
			rowhide.style.display='none'
		}
		function ShowEmail()
		{
			var rowhide=document.getElementById("tremail");  //onload="setdefault();"
			rowhide.style.display='block'
			rowhide=document.getElementById("trpdf");  //onload="setdefault();"
			rowhide.style.display='none'
			return false;							
		}
		function HideEmail()
		{
			var rowhide=document.getElementById("tremail");  //onload="setdefault();"
			rowhide.style.display='none'
			rowhide=document.getElementById("trpdf");  //onload="setdefault();"
			rowhide.style.display='block'
			return false;							
			
		}
		//Validate email addresses
		function Validate()
		{
			var addr;
			//validate To field
			addr=document.getElementById("txt_to").value;
			if(multiEmail(addr)==false)
			{
				document.getElementById("txt_to").focus();				
				return false;
			}
			addr=document.getElementById("txt_cc").value;
			if (addr!="")
			{
				if(multiEmail(addr)==false)
				{
					document.getElementById("txt_cc").focus();
					return false;
				}
			}
			addr=document.getElementById("txt_bcc").value;
			if (addr!="")
			{
				if(multiEmail(addr)==false)
				{
					document.getElementById("txt_bcc").focus();
					return false;
				}
			}			
		}
		//Split email
		function multiEmail(email_field)
		{
			var email = email_field.split(',');
			for(var i = 0; i < email.length; i++) 
			{
				if (!isEmail(email[i])) 
					{
						alert('one or more email addresses entered is invalid');
						return false;
					}
			}
			return true;
		} 		
		
		function OpenPopUpSmall(path)  //(var path,var name)
		{
		
		 window.open(path,'',"fullscreen=no,toolbar=no,width=2,height=2,left=0,top=0,status=no,menubar=no,resizable=no");
		  //return true;
		}
		
		function SendToBatch()
		{
		    var inBatch = '<% =ViewState["InBatch"] %>';
		    if(inBatch=='0')
		    {
			    OpenPopUpSmall("../Reports/frmBatchEntry.aspx?Batch=true&LetterID=3&casenumber=<%=ViewState["vTicketId"]%>&overwriteReceipt=0" );	
			}
			else
			{
			
			var con = confirm('Receipt already exists in batch. This would overwrite the existing receipt. Would you like to continue?');
			
			if(con==true)
			    {
			        OpenPopUpSmall("../Reports/frmBatchEntry.aspx?Batch=true&LetterID=3&casenumber=<%=ViewState["vTicketId"]%>&overwriteReceipt=1" );	
			    }
		    else
    			{
	    		    alert('Receipt not sent to batch');
	    		    return(false);
			    }
						    
			}
		}
		// Afaq 7961 07/26/2010 Hide email panel
		function hide_pnl_Email()
		{
		
		var pnl = document.getElementById('tremail');
		var img = document.getElementById("tdWait");
		if (pnl != null)
		{
		    pnl.style.display = "none";
		    img.style.display = "";
		    
		}
		}
		//Zeeshan Haider 11072 05/15/2013 Enable controls once iframe loaded
		function ifrLoad()
		{
		    if(document.getElementById('IBtnExportWord'))
		        document.getElementById('IBtnExportWord').disabled = false;
		    if(document.getElementById('btn_SendToBatch'))
		        document.getElementById('btn_SendToBatch').disabled = false;
		    if(document.getElementById('imgbtn_email'))
		        document.getElementById('imgbtn_email').disabled = false;
		}
		
    </script>

</head>
<body onload="refresh();" ms_positioning="GridLayout">
    <form id="Form1" method="post" runat="server">
    <table height="100%" cellspacing="0" cellpadding="0" width="100%" align="center"
        border="0">
        <tr>
            <td background="../Images/subhead_bg.gif" height="31">
            </td>
        </tr>
        <tr>
            <td>
                <table class="clsmainhead" id="Table3" cellspacing="0" cellpadding="0" width="100%"
                    align="left" border="0">
                    <tr>
                        <td valign="bottom" width="31">
                            <img height="17" src="../Images/head_icon.gif" width="31">
                        </td>
                        <td class="clssubhead" valign="baseline">
                            &nbsp;Contract/Receipt
                        </td>
                        <td align="right" colspan="2">
                            <asp:ImageButton ID="IBtnExportWord" runat="server" Height="23px" Width="78px" ImageUrl="~/Images/MSSmall.gif"
                                OnClick="IBtnExportWord_Click" />
                            <%--Fahad 5525 02/12/2009 image button added--%>
                            &nbsp;<asp:ImageButton ID="btn_SendToBatch" runat="server" Height="24px" ImageUrl="~/Images/BriefCase.png"
                                ToolTip="Send to batch" Visible="False" />
                            <asp:ImageButton ID="imgbtn_email" runat="server" ToolTip="Email" ImageUrl="../Images/Email.jpg">
                            </asp:ImageButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td background="../Images/separator_repeat.gif" height="9">
            </td>
        </tr>
        <tr id="tremail" style="display: none">
            <td>
                <table id="tblemail" width="100%">
                    <tr>
                        <td valign="bottom" align="left" width="12%">
                            &nbsp;
                            <asp:ImageButton ID="imgbtn_send" OnClientClick="hide_pnl_Email()" runat="server"
                                ToolTip="Send" ImageUrl="..\Images\SendMail.ICO"></asp:ImageButton>&nbsp;
                            <asp:ImageButton ID="imgbtn_cancel" runat="server" ToolTip="Cancel" ImageUrl="..\Images\cancel.ico">
                            </asp:ImageButton>
                        </td>
                        <td>
                            <img src="../Images/receipt.gif">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;<strong>To.</strong>
                        </td>
                        <td>
                            <asp:TextBox ID="txt_to" runat="server" CssClass="textbox" Width="430px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;<strong>CC</strong>
                        </td>
                        <td>
                            <asp:TextBox ID="txt_cc" runat="server" CssClass="textbox" Width="430px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;<strong>BCC</strong>
                        </td>
                        <td>
                            <asp:TextBox ID="txt_bcc" runat="server" CssClass="textbox" Width="430px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;<strong>Subject</strong>
                        </td>
                        <td>
                            <asp:TextBox ID="txt_subject" runat="server" CssClass="textbox" Width="430px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:TextBox ID="txt_message" runat="server" Height="240px" CssClass="textbox" Width="544px"
                                TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr style="height: 100%">
            <td align="center" class="clssubhead" id="tdWait" runat="server" style="display: none">
                Please wait...<br />
                <br />
                <img id="imgProgressbar" runat="server" src="../Images/Progressbar.gif" alt="Please wait.." />
            </td>
        </tr>
        <tr id="trpdf" height="100%">
            <%--Zeeshan Haider 11072 05/15/2013 Enable functionality once iframe loaded--%>
            <td>
                &nbsp;
                <iframe onload="ifrLoad();" tabindex="0" src="Rptreceipt.aspx?casenumber=<%=ViewState["vTicketId"]%>"
                    frameborder="1" width="98%" scrolling="auto" height="98%"></iframe>
            </td>
        </tr>
        <tr>
            <td>
                <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;


namespace lntechNew.Reports
{
    public partial class PopupContinuanceReport : System.Web.UI.Page
    {
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        clsLogger bugTracker = new clsLogger();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    string ticketid = Request.QueryString["ticketid"].ToString();
                    BindGrid(ticketid);
                }
                
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        private void BindGrid(string ticketid)
        {
            try
            {
                string[] key ={ "@TicketID" };
                object[] value ={ Convert.ToInt32(ticketid) };
                DataSet DS = ClsDb.Get_DS_BySPArr("usp_hts_get_Continuance_Scandoc", key, value);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    GV_ContinuanceDOC.DataSource = DS;
                    GV_ContinuanceDOC.DataBind();
                }

                else
                {
                    //lblMessage.Text = "No Document Found";
                    Response.Write("no scan document found.");
                }
                
            }
            catch (Exception ex)
            { 
            
            }
        }

        protected void GV_ContinuanceDOC_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.Header && e.Row.RowType != DataControlRowType.Footer)
                {
                    string lngDocID = ((Label)e.Row.FindControl("lblDocID")).Text.ToString();
                    string lngDocNum = ((Label)e.Row.FindControl("lblDocNum")).Text.ToString();
                    string RecType = ((Label)e.Row.FindControl("lblRecType")).Text.ToString();
                    string DocExt = ((Label)e.Row.FindControl("lblDocExtension")).Text.ToString();
                    string Event = ((Label)e.Row.FindControl("lblEvent")).Text.ToString();

                    ((ImageButton)e.Row.FindControl("imgbtnview")).Attributes.Add("OnClick", "javascript:return PopUpShowPreviewPDF(" + lngDocID.ToString() + "," + RecType + "," + lngDocNum.ToString() + ",'" + DocExt + "'" + ");");
                }
            }
            catch (Exception ex)
            {
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
           
        }
    }
}

<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.Reports.PreviewEmail" Codebehind="PreviewEmail.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>PreviewEmail</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script>
		//Validate email addresses
		function Validate()
		{				
			var addr;
			//validate To field
			addr=document.getElementById("txt_to").value;
			if(multiEmail(addr)==false)
			{
				document.getElementById("txt_to").focus();				
				return false;
			}
			addr=document.getElementById("txt_cc").value;
			if (addr!="")
			{
				if(multiEmail(addr)==false)
				{
					document.getElementById("txt_cc").focus();
					return false;
				}
			}
			addr=document.getElementById("txt_bcc").value;
			if (addr!="")
			{
				if(multiEmail(addr)==false)
				{
					document.getElementById("txt_bcc").focus();
					return false;
				}
			}			
		}
		//Split email
		function multiEmail(email_field)
		{
			var email = email_field.split(',');
			for(var i = 0; i < email.length; i++) 
			{
				if (!isEmail(email[i])) 
					{
						alert('one or more email addresses entered is invalid');
						return false;
					}
			}
			return true;
		} 	
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
				<TR>
					<TD background="../Images/subhead_bg.gif" height="31"></TD>
				</TR>
				<tr>
					<td>
						<table class="clsmainhead" id="Table3" cellSpacing="0" cellPadding="0" width="100%" align="left"
							border="0">
							<tr>
								<td vAlign="bottom" width="31"><IMG height="17" src="../Images/head_icon.gif" width="31"></td>
								<td class="clssubhead" vAlign="baseline">&nbsp;Email Preview&nbsp;
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td background="../Images/separator_repeat.gif" colSpan="3" height="9"></td>
				</tr>
				<tr id="tremail">
					<td vAlign="middle" align="center">
						<asp:Label id="lbl_message" runat="server" ForeColor="Red"></asp:Label>
						<table id="tblemail" width="100%">
							<TR>
								<TD vAlign="bottom" align="left" width="12%">&nbsp;
									<asp:ImageButton id="imgbtn_send" runat="server" ImageUrl="..\Images\SendMail.ICO" ToolTip="Send"></asp:ImageButton>&nbsp;
									<asp:ImageButton id="imgbtn_cancel" runat="server" ImageUrl="..\Images\cancel.ico" ToolTip="Cancel"></asp:ImageButton></TD>
								<TD>
									<asp:ImageButton id="imgbtn_receipt" runat="server" ImageUrl="../Images/receipt.gif" Visible="False"
										ToolTip="Preview Attachment"></asp:ImageButton>
									<asp:ImageButton id="imgbtn_trialleter" runat="server" ImageUrl="../Images/trialLetter.gif" Visible="False"
										ToolTip="Preview Attachment"></asp:ImageButton></TD>
							</TR>
							<tr>
								<td>&nbsp;<strong>To.</strong></td>
								<td><asp:textbox id="txt_to" runat="server" Width="430px" CssClass="textbox"></asp:textbox></td>
							</tr>
							<tr>
								<td>&nbsp;<strong>CC</strong></td>
								<td><asp:textbox id="txt_cc" runat="server" Width="430px" CssClass="textbox"></asp:textbox></td>
							</tr>
							<TR>
								<TD>&nbsp;<strong>BCC</strong></TD>
								<TD><asp:textbox id="txt_bcc" runat="server" Width="430px" CssClass="textbox"></asp:textbox></TD>
							</TR>
							<tr>
								<td>&nbsp;<strong>Subject</strong></td>
								<td><asp:textbox id="txt_subject" runat="server" Width="430px" CssClass="textbox"></asp:textbox></td>
							</tr>
							<TR>
								<TD colSpan="2"><asp:textbox id="txt_message" runat="server" Width="544px" TextMode="MultiLine" CssClass="textbox"
										Height="240px"></asp:textbox></TD>
							</TR>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>

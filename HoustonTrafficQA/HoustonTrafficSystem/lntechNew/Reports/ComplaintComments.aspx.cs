﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using lntechNew.Components.ClientInfo;
using HTP.Components.Services;
using OboutInc.Flyout2;

namespace HTP.Reports
{

      
    public partial class ComplaintComments : System.Web.UI.Page
    {
        #region variables
          
        clsLogger bugTracker = new clsLogger();
        DataView dvCom = new DataView();
        bool IsSearch;
        clsSession cSession = new clsSession();

        #endregion

        #region Properites

        public SortDirection GridViewSortDirection
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Ascending;
                return (SortDirection)ViewState["sortDirection"];
            }
            set { ViewState["sortDirection"] = value; }

        }

        public string GridViewSortExpression
        {
            get
            {
                if (ViewState["sortExpression"] == null)
                    ViewState["sortExpression"] = "";
                return ViewState["sortExpression"].ToString();
            }
            set { ViewState["sortExpression"] = value; }

        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    cal_FromDateFilter.SelectedDate = DateTime.Today;
                    cal_ToDateFilter.SelectedDate = DateTime.Today;
                    FillddlFirms();
                    FillddlCaseType();

                }


                Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
                Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
                Pagingctrl.GridView = gv_Records;
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                lbl_Message.Text = ex.ToString();
            }
        }

        protected void gv_Records_Sorting(object sender, GridViewSortEventArgs e)
        {

            string sortExpression = e.SortExpression;

            if (GridViewSortDirection == SortDirection.Ascending && sortExpression == GridViewSortExpression)
            {
                GridViewSortDirection = SortDirection.Descending;
            }
            else
            {
                GridViewSortDirection = SortDirection.Ascending;
            }

            GridViewSortExpression = sortExpression;
            BindGrid();
            //SortGrid(sortExpression);
            //Pagingctrl.PageCount = gv_Records.PageCount;
            //Pagingctrl.PageIndex = gv_Records.PageIndex;
        }

        protected void gv_Records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_Records.PageIndex = e.NewPageIndex;
                dvCom = (DataView)Session["dvCom"];
                gv_Records.DataSource = dvCom;
                gv_Records.DataBind();
                // Noufil 6098 09/25/2009 Set paging control
                Pagingctrl.PageCount = gv_Records.PageCount;
                Pagingctrl.PageIndex = gv_Records.PageIndex;
                Pagingctrl.SetPageIndex();
            }
            catch (Exception ex)
            {

                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            IsSearch = true;
            GridViewSortExpression = "FullName";
            BindGrid();
            IsSearch = false;

        }

        protected void gv_Records_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                 //string ControlName = e.Row.FindControl("hf_Comments").NamingContainer.ClientID;
                HiddenField hf_Comments = e.Row.FindControl("hf_Comments") as HiddenField;
                Flyout ToolTipComments = ((Flyout)e.Row.FindControl("ToolTipComments"));
                Panel pnlTooltipComments = ((Panel)ToolTipComments.FindControl("pnlTooltipComments"));
                TextBox txtToolTipComments = new TextBox();
                txtToolTipComments.Text = hf_Comments.Value;
                txtToolTipComments.Columns = 46;
                txtToolTipComments.Rows = 10;
                txtToolTipComments.TextMode = TextBoxMode.MultiLine;
                txtToolTipComments.ReadOnly = true;
                pnlTooltipComments.Controls.Add(txtToolTipComments);

                //((Label)e.Row.FindControl("lbl_Comments")).Attributes.Add("OnMouseOver", "CommentPoupup('" + ControlName + "_hf_Comments" + "');");
                //((Label)e.Row.FindControl("lbl_Comments")).Attributes.Add("OnMouseOut", "CursorIcon2()");
                
                
                // Asad Ali 7480 03/24/2010  shortened/fixed and put '.....' in Client Name and Comments Column 
                Label lbl_Name = ((Label)e.Row.FindControl("lbl_Name"));
                Label lblToolTipName = new Label();
                Flyout FlyoutToolTipName = ((Flyout)e.Row.FindControl("FlyoutToolTipFullName"));
                Panel pnlTooltipName = ((Panel)FlyoutToolTipName.FindControl("pnlTooltipFullName"));
                lblToolTipName.Text = lbl_Name.Text;
                pnlTooltipName.Controls.Add(lblToolTipName);
                lbl_Name.Text = (lbl_Name.Text.Length > 10 ? lbl_Name.Text.Substring(0, 10) + "..." : lbl_Name.Text);
                TextBox txtToolTipCompaint = new TextBox();
                Label lbl_Complaint = ((Label)e.Row.FindControl("lbl_Complaint"));
                Flyout FlyoutToolTip = ((Flyout)e.Row.FindControl("FlyoutToolTip"));
                Panel pnlTooltip = ((Panel)FlyoutToolTip.FindControl("pnlTooltip"));
                txtToolTipCompaint.Text = lbl_Complaint.Text;
                txtToolTipCompaint.Columns = 46;
                txtToolTipCompaint.Rows = 10;
                txtToolTipCompaint.TextMode = TextBoxMode.MultiLine;
                txtToolTipCompaint.ReadOnly = true;
                pnlTooltip.Controls.Add(txtToolTipCompaint);
                lbl_Complaint.Text = (lbl_Complaint.Text.Length >= 30 ? lbl_Complaint.Text.Substring(0, 30) + "...": lbl_Complaint.Text);
                //lbl_Complaint.Attributes.Add("onmouseover", "return escape('" + lbl_Complaint.Text + "')");
                
                



            }

        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            string NoteIDs = hf_NoteIDs.Value.ToString();
            if (NoteIDs != "" && NoteIDs != null)
            {
                CommentService CommSer = new CommentService();
                CommSer.UpdateComplaintIsReviewed(NoteIDs,Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request)));
            }
            IsSearch = true;
            BindGrid();
            IsSearch = false;

        }

        #endregion

        #region Method

        /// <summary>
        /// Fill firm drop down of attorneys
        /// </summary>
        private void FillddlFirms()
        {
            try
            {
                clsFirms ClsFirms = new clsFirms();
                DataSet ds_Firms = ClsFirms.GetAllFullFirmName();
                ddlAttorneys.DataSource = ds_Firms.Tables[0];
                ddlAttorneys.DataTextField = "FirmFullName";
                ddlAttorneys.DataValueField = "FirmID";
                ddlAttorneys.DataBind();
                ListItem lstAttor = new ListItem("All", "-1");
                ddlAttorneys.Items.Insert(0, lstAttor);

            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
            }
        }

        /// <summary>
        /// Fil case type drop down
        /// </summary>
        private void FillddlCaseType()
        {
            CaseType ctype = new CaseType();
            ddlCaseType.DataSource = ctype.GetAllCaseType();
            ddlCaseType.DataBind();
            ListItem lstCaseType = new ListItem("All", "-1");
            ddlCaseType.Items.Insert(0, lstCaseType);
        }

        /// <summary>
        /// on page index chage grid page change
        /// </summary>
        void Pagingctrl_PageIndexChanged()
        {
            try
            {
                if (Session["dvCom"] != null)
                {

                    //dvCom = (DataView)Session["dvCom"];
                    gv_Records.PageIndex = Pagingctrl.PageIndex - 1;
                    BindGrid();
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        /// <summary>
        /// grid size change
        /// </summary>
        /// <param name="pageSize"></param>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {

            try
            {

                if (pageSize > 0)
                {
                    gv_Records.PageIndex = 0;
                    gv_Records.PageSize = pageSize;
                    gv_Records.AllowPaging = true;
                }
                else
                {
                    gv_Records.AllowPaging = false;
                }
                BindGrid();
                //dvCom = (DataView)Session["dvCom"];
                //gv_Records.DataSource = dvCom;
                //gv_Records.DataBind();
            }
            catch (Exception ex)
            {
                lblMessage.Visible = true;
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// get data and bind grid for comment report
        /// </summary>
        public void BindGrid()
        {
            DataTable dt = new DataTable();
            if (IsSearch)
            {
                CommentService ComtSer = new CommentService();
                dt = ComtSer.GetComplaintReport(cal_FromDateFilter.SelectedDate, cal_ToDateFilter.SelectedDate, Convert.ToInt32(ddlCaseType.SelectedValue), Convert.ToInt32(ddlAttorneys.SelectedValue), chkShowAll.Checked);
                Session["dvCom"] = dt.DefaultView;
                gv_Records.PageSize = 20;
                gv_Records.PageIndex = 0;
            }

            if (Session["dvCom"] != null && dt.Rows.Count == 0)
            {
                dvCom = (DataView)Session["dvCom"];
                dt = dvCom.ToTable();

            }
            if (dt.Rows.Count > 0)
            {
                dvCom = dt.DefaultView;
                dvCom.Sort = GridViewSortExpression + " " + (GridViewSortDirection == SortDirection.Ascending ? "ASC" : "DESC");
                dt = dvCom.ToTable();
                GenerateSerialNo(dt);
                dvCom = dt.DefaultView;
                Session["dvCom"] = dvCom;
                gv_Records.DataSource = dvCom;
                gv_Records.DataBind();
                Pagingctrl.PageCount = gv_Records.PageCount;
                Pagingctrl.PageIndex = gv_Records.PageIndex;
                Pagingctrl.SetPageIndex();
                gv_Records.Visible = true;
                lbl_Message.Text = "";
                btnUpdate.Visible = true;
            }
            else
            {
                Pagingctrl.PageCount = 0;
                Pagingctrl.PageIndex = 0;
                Pagingctrl.SetPageIndex();
                lbl_Message.Text = "No Records Found";
                gv_Records.Visible = false;
                btnUpdate.Visible = false;
            }

        }

        /// <summary>
        /// Generate serial number in given datatable ticket wise
        /// </summary>
        /// <param name="dtRecords"></param>
        private void GenerateSerialNo(DataTable dtRecords)
        {
            try
            {
                int sno = 1;
                if (dtRecords.Columns.Contains("sno"))
                    dtRecords.Columns.Remove("sno");

                dtRecords.Columns.Add("sno");


                if (dtRecords.Rows.Count >= 1)
                    dtRecords.Rows[0]["sno"] = 1;

                if (dtRecords.Rows.Count >= 2)
                {
                    for (int i = 1; i < dtRecords.Rows.Count; i++)
                    {
                        if (dtRecords.Rows[i - 1]["ticketid_pk"].ToString() != dtRecords.Rows[i]["ticketid_pk"].ToString())
                        {
                            dtRecords.Rows[i]["sno"] = ++sno;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        #endregion

    }
}

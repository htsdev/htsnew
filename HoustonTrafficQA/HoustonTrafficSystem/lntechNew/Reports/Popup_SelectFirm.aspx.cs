using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.Reports
{
    public partial class Popup_SelectFirm : System.Web.UI.Page
    {
        private clsENationWebComponents clsDb = new clsENationWebComponents("Connection String");
        clsLogger bugTracker = new clsLogger();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    DataTable dt = clsDb.Get_DT_BySPArr("usp_Attorney_GetAllFirms");
                    ddl_Firm.DataTextField = "name";
                    ddl_Firm.DataValueField = "id";
                    ddl_Firm.DataSource = dt;
                    ddl_Firm.DataBind();
                }
                catch (Exception ex)
                {
                    bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                }
            }

        }

        protected void btn_Save_Click(object sender, EventArgs e)
        {
            try
            {
                string[] keys = { "@CourtDate", "@CourtID", "@CourtRoom", "@ViolationID", "@FirmID" };
                object[] values = { Request.QueryString["CourtDate"], Request.QueryString["CourtID"], Request.QueryString["CourtRoom"], Request.QueryString["ViolationID"], ddl_Firm.SelectedValue };
                clsDb.ExecuteSP("USP_HTS_UPDATE_CONCENTRATION", keys, values);
                UpdateFlag.Value = "1";
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }
    }
}

﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;

namespace HTP.Reports
{
    public partial class CertifiedMailConfirmation : System.Web.UI.Page
    {
        #region Variables
        clsSession ClsSession = new clsSession();
        clsCase cCase = new clsCase();
        clsENationWebComponents clsdb = new clsENationWebComponents();
        #endregion

        protected void Page_Load( object sender, EventArgs e )
        {
            lbl_message.Text = string.Empty;

            if ( !IsPostBack )
            {
                LoadGrid();
            }

            chkShowall.Attributes.Add( "onclick", "EnabledControls();" );
        }

        protected void gv_Records_RowDataBound( object sender, GridViewRowEventArgs e )
        {

        }

        private void LoadGrid()
        {
            try
            {
                gv_Records.Visible = true;
                string[] keys = { "@FromDate", "@ToDate", "@Status", "@ShowAll", "@FilterType" };
                object[] values = { fromdate.SelectedDate, todate.SelectedDate, Convert.ToInt32( ddlStatus.SelectedValue ), chkShowall.Checked, ( chkShowall.Checked ? 2 : 3 ) };
                DataSet DS = clsdb.Get_DS_BySPArr( "USP_HTP_GET_CertifiedLetterOfRep", keys, values );             

                if ( DS.Tables[0].Rows.Count > 0 )
                {                    
                    gv_Records.DataSource = DS.Tables[0].DefaultView;
                    gv_Records.DataBind();
                    SortGrid( gv_Records );

                }
                else
                {
                    lbl_message.Text = "No Records Found";
                    gv_Records.Visible = false;
                }
            }
            catch ( Exception ex )
            {
                lbl_message.Text = ex.Message;
                clsLogger.ErrorLog( ex );
            }
        }

        protected void SortGrid( GridView gv )
        {

            String oldCertifiedMailNumber = String.Empty;
            int renderIndex = 1;
            try
            {
                foreach ( GridViewRow grv in gv.Rows )
                {
                    if ( grv.RowType == DataControlRowType.DataRow )
                    {

                        Table tbl = ( grv.Parent as Table );
                        //Creating Cells 
                        TableCell tc = new TableCell();
                        TableCell tcBatchDateTime = new TableCell();
                        TableCell tcScanDateTime = new TableCell();
                        TableCell tcDocument = new TableCell();
                        TableCell tcUspsDocument = new TableCell();

                        TableCell tcRep = new TableCell();
                        TableCell tcStatus = new TableCell();
                        TableCell tcPrintRep = new TableCell();
                        
                        Label lbStatus = new Label();
                        lbStatus.Text = ( ( Label ) grv.FindControl( "lblStatus" ) ).Text;
                        
                        Label lbRep = new Label();
                        lbRep.Text = ( ( Label ) grv.FindControl( "lblRep" ) ).Text;

                        Label lblCon = new Label();
                        lblCon.Text = ( ( Label ) grv.FindControl( "lblCmail" ) ).Text;
                       
                        HyperLink hplDocument = new HyperLink();
                        hplDocument.NavigateUrl = "#";
                        string docstoragLORPath = "../docstorage/" + ConfigurationSettings.AppSettings["NTPATHLORPrint"].ToString().Replace( ConfigurationSettings.AppSettings["NTPATH"].ToString(), string.Empty );
                        docstoragLORPath = docstoragLORPath.Replace("\\/", "");
                        hplDocument.Attributes.Add( "onclick", "return OpenReport('" + docstoragLORPath + "/" + lblCon.Text + "_Printed.pdf');" );
                        hplDocument.Target = "_blank";
                        hplDocument.Text = "<img src='/images/preview.gif' alt='" + lblCon.Text + "-Printed.pdf'  border='0'/>";

                        Label lblEDeliveryFlag = ( Label ) grv.FindControl( "lblEDeliveryFlag" );
                        HyperLink hplCertifiedDocument = new HyperLink();
                        hplCertifiedDocument.NavigateUrl = "#";
                        if (lblEDeliveryFlag.Text.ToLower() == "true")
                        {
                            hplCertifiedDocument.Attributes.Add("onclick", "return OpenReport('" + docstoragLORPath + "/" + lblCon.Text + "_Confirmed.pdf');");
                            hplCertifiedDocument.Target = "_blank";
                            hplCertifiedDocument.Text = "<img src='/images/preview.gif' alt='" + lblCon.Text + "-Confirmed.pdf'  border='0'/>";
                        }

                        Label lblBatchDateTime = new Label();
                        lblBatchDateTime.Text = ( ( Label ) grv.FindControl( "lblBatchDate" ) ).Text;
                        DateTime date;
                        if (DateTime.TryParse(lblBatchDateTime.Text, out date))
                        {
                            lblBatchDateTime.Text = date.ToString("MM/dd/yyyy @ HH:mm:ss");
                        }
                        else
                        {
                            lblBatchDateTime.Text = string.Empty;
                        }

                        //Ozair 5665 03/20/2009 Displaying Empty date time and rep when status is pending
                        Label lbPrintRep = new Label();
                        Label lblScanDateTime = new Label();
                        lblScanDateTime.Text = ( ( Label ) grv.FindControl( "lblPrintDate" ) ).Text;
                        if (DateTime.TryParse(lblScanDateTime.Text, out date))
                        {
                            lblScanDateTime.Text = date.ToString("MM/dd/yyyy @ HH:mm:ss");
                            //Ozair 5665 03/20/2009 setting confirmed rep
                            lbPrintRep.Text = ((Label)grv.FindControl("lblPrintRep")).Text;
                        }
                        else
                        {
                            lblScanDateTime.Text = string.Empty;
                            //Ozair 5665 03/20/2009 setting confirmed rep to empty
                            lbPrintRep.Text = String.Empty;
                        }
                        
                        tc.CssClass = "TDHeading";
                        tc.ColumnSpan = 3;
                        tc.VerticalAlign = VerticalAlign.Middle;
                        tc.HorizontalAlign = HorizontalAlign.Left;

                        tcDocument.CssClass = "TDHeading";
                        tcDocument.VerticalAlign = VerticalAlign.Middle;
                        tcDocument.HorizontalAlign = HorizontalAlign.Center;

                        tcUspsDocument.CssClass = "TDHeading";
                        tcUspsDocument.VerticalAlign = VerticalAlign.Middle;
                        tcUspsDocument.HorizontalAlign = HorizontalAlign.Center;

                        tcBatchDateTime.CssClass = "TDHeading";
                        tcBatchDateTime.VerticalAlign = VerticalAlign.Middle;
                        tcBatchDateTime.HorizontalAlign = HorizontalAlign.Center;

                        tcStatus.CssClass = "TDHeading";
                        tcStatus.VerticalAlign = VerticalAlign.Middle;
                        tcStatus.HorizontalAlign = HorizontalAlign.Center;

                        tcRep.CssClass = "TDHeading";
                        tcRep.VerticalAlign = VerticalAlign.Middle;
                        tcRep.HorizontalAlign = HorizontalAlign.Center;

                        tcPrintRep.CssClass = "TDHeading";
                        tcPrintRep.VerticalAlign = VerticalAlign.Middle;
                        tcPrintRep.HorizontalAlign = HorizontalAlign.Center;

                        tcScanDateTime.CssClass = "TDHeading";
                        tcScanDateTime.VerticalAlign = VerticalAlign.Middle;
                        tcScanDateTime.HorizontalAlign = HorizontalAlign.Center;

                        lbStatus.Font.Bold = true;
                        lbStatus.ForeColor = System.Drawing.Color.White;
                        lbRep.Font.Bold = true;
                        lbRep.ForeColor = System.Drawing.Color.White;

                        lbPrintRep.Font.Bold = true;
                        lbPrintRep.ForeColor = System.Drawing.Color.White;

                        lblCon.Font.Bold = true;
                        lblCon.ForeColor = System.Drawing.Color.White;

                        lblBatchDateTime.Font.Bold = true;
                        lblBatchDateTime.ForeColor = System.Drawing.Color.White;


                        //If Row is first .....
                        if ( grv.RowIndex == 0 )
                        {
                            oldCertifiedMailNumber = String.Empty;
                            GridViewRow gv_row = new GridViewRow( grv.RowIndex, grv.DataItemIndex, DataControlRowType.DataRow, DataControlRowState.Normal );

                            gv_row.Controls.Clear();
                           
                            tc.Controls.Add( lblCon );
                            tcStatus.Controls.Add( lbStatus );
                            tcDocument.Controls.Add( hplDocument );
                            tcBatchDateTime.Controls.Add( lblBatchDateTime );
                            tcUspsDocument.Controls.Add( hplCertifiedDocument );
                            tcRep.Controls.Add( lbRep );
                            tcPrintRep.Controls.Add( lbPrintRep );
                            tcScanDateTime.Controls.Add( lblScanDateTime );

                            gv_row.Controls.Add( tc );
                            gv_row.Controls.Add( tcStatus );
                            gv_row.Controls.Add( tcBatchDateTime );
                            gv_row.Controls.Add( tcRep );
                            gv_row.Controls.Add( tcDocument );
                            gv_row.Controls.Add( tcScanDateTime );
                            gv_row.Controls.Add( tcPrintRep );
                            gv_row.Controls.Add( tcUspsDocument );
                            tbl.Rows.AddAt( grv.RowIndex + 1, gv_row );
                            

                            oldCertifiedMailNumber = lblCon.Text;
                            renderIndex++;

                        }
                        //if the Row is not first.... 
                        else
                        {

                            if ( lblCon.Text != oldCertifiedMailNumber )
                            {
                                GridViewRow gv_row = new GridViewRow( grv.RowIndex, grv.DataItemIndex, DataControlRowType.DataRow, DataControlRowState.Normal );

                                tc.Controls.Add( lblCon );
                                tcStatus.Controls.Add( lbStatus );
                                tcDocument.Controls.Add( hplDocument );
                                tcUspsDocument.Controls.Add( hplCertifiedDocument );
                                tcBatchDateTime.Controls.Add( lblBatchDateTime );
                                tcRep.Controls.Add( lbRep );
                                tcPrintRep.Controls.Add( lbPrintRep );
                                tcScanDateTime.Controls.Add( lblScanDateTime );

                                gv_row.Controls.Add( tc );
                                gv_row.Controls.Add( tcStatus );
                                gv_row.Controls.Add( tcBatchDateTime );
                                gv_row.Controls.Add( tcRep );
                                gv_row.Controls.Add( tcDocument );
                                gv_row.Controls.Add( tcScanDateTime );
                                gv_row.Controls.Add( tcPrintRep );
                                gv_row.Controls.Add( tcUspsDocument );

                                tbl.Rows.AddAt( gv_row.RowIndex + renderIndex, gv_row );
                                renderIndex++;
                            }
                            //Setting oldCertifiedMailNumber
                            oldCertifiedMailNumber = lblCon.Text;
                        }

                    }

                }
            }
            catch ( Exception ex )
            {

                lbl_message.Text = ex.Message;
                clsLogger.ErrorLog( ex);
            }

        }

        protected void btnSearch_Click( object sender, EventArgs e )
        {
            LoadGrid();
        }
    }
}

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.IO;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using lntechNew.Components;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using System.Windows.Forms;
//Yasir Kamal 7722 04/21/2010 namespace added.
using System.Configuration;
using Configuration.Client;
using Configuration.Helper;


namespace HTP.Reports
{
    /// <summary>
    /// Summary description for Word_TrialNotification.
    /// </summary>
    public partial class Word_TrialNotification : System.Web.UI.Page
    {
        clsCrsytalComponent Cr = new clsCrsytalComponent();
        clsSession uSession = new clsSession();
        clsLogger clog = new clsLogger();
        clsCase cCase = new clsCase();
        clsENationWebComponents ClsDb = new clsENationWebComponents();

        int TicketIDList;
        protected System.Web.UI.WebControls.TextBox txt_to;
        protected System.Web.UI.WebControls.TextBox txt_cc;
        protected System.Web.UI.WebControls.TextBox txt_subject;
        protected System.Web.UI.WebControls.TextBox txt_message;
        protected System.Web.UI.WebControls.TextBox txt_bcc;
        protected System.Web.UI.WebControls.ImageButton imgbtn_email;
        protected System.Web.UI.WebControls.ImageButton imgbtn_send;
        protected System.Web.UI.WebControls.ImageButton imgbtn_cancel;
        protected System.Web.UI.WebControls.ImageButton IBtn;
        int empid;

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {

                //TicketIDList = Convert.ToInt32(uSession.GetCookie("sTicketID",this.Request));
                TicketIDList = Convert.ToInt32(Request.QueryString["casenumber"]);
                ViewState["vTicketId"] = TicketIDList;
                DataSet ds = cCase.GetClientEmail(TicketIDList);
                ViewState["vEmail"] = ds.Tables[0].Rows[0][0].ToString();
                empid = Convert.ToInt32(uSession.GetCookie("sEmpID", this.Request));
                Session["Batch"] = Request.QueryString["Batch"].ToString();
                TextBox2.Text = Session["Batch"].ToString();
                imgbtn_email.Attributes.Add("onclick", "return ShowEmail();");
                imgbtn_cancel.Attributes.Add("onclick", "return HideEmail();");
                imgbtn_send.Attributes.Add("onclick", "return Validate();");
                btn_SendToBatch.Attributes.Add("onclick", "return SendToBatch();");
                //assigning email
                if (txt_to.Text == "")
                {
                    txt_to.Text = ViewState["vEmail"].ToString();
                }
                if (Request.QueryString["flag"].ToString() == "9")
                {
                    TextBox1.Text = "1";
                    txt_to.Text = Request.QueryString["emailaddress"].ToString();
                }
            }
            catch (Exception ex)
            {

                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.IBtn.Click += new System.Web.UI.ImageClickEventHandler(this.IBtn_Click);
            this.imgbtn_send.Click += new System.Web.UI.ImageClickEventHandler(this.imgbtn_send_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion
        //To create Word Report
        public void CreateReportWord()
        {
            try
            {
                string[] key2 = { "@TicketIDList", "@empid" };
                object[] value12 = { TicketIDList, empid };
                string filename = Server.MapPath("") + "\\Trial_Notification.rpt";
                //Int32 LetterType =2;
                //	Cr.CreateReportWord(filename,"USP_HTS_Trialletter",key2,value12,Request.QueryString["Batch"].ToString(),LetterType,this.Session, this.Response);
                Cr.CreateReportWord(filename, "USP_HTS_Trialletter", key2, value12, this.Session, this.Response);
            }
            catch (Exception ex)
            {
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                Response.Write(ex.Message);
            }
        }


        private void IBtn_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            CreateReportWord();
        }


        //Event fires when Send is clicked
        private void imgbtn_send_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            try
            {
                //Asad Ali 7925 07/21/2010 Get reply to value from configuration Services...
                IConfigurationClient Client = ConfigurationClientFactory.GetConfigurationClient();
                //Zeeshan Haider 11367 08/29/2013 Made changes for Trial Notification letter bug fixed.
                string[] key = { "@ticketID", "@letterID" };
                object[] value = { TicketIDList, 2 };
                DataSet ds = ClsDb.Get_DS_BySPArr("USP_HTP_GET_Updated_Cover_Sheet_Name", key, value);
                if (ds.Tables[0].Rows.Count > 0 && !(string.IsNullOrEmpty(Convert.ToString(Session["Tfilepath"]))))
                {
                    //string filename = Session["Tfilepath"].ToString();
                    string filename = ConfigurationManager.AppSettings["NTPATHBatchPrint"] + Convert.ToString(ds.Tables[0].Rows[0][0]);
                    string destPath = Request.ServerVariables["APPL_PHYSICAL_PATH"] + "\\reports\\" +
                                      filename.Substring(filename.LastIndexOf("\\"));
                    if (File.Exists(destPath))
                        File.Delete(destPath);
                    File.Copy(filename, destPath);
                    filename = destPath;

                    //Yasir Kamal 7722 04/21/2010 get email from config file.
                    //Getting sales rep email
                    //string emailrep = clog.SalesRepEmail(empid);
                    //if (emailrep.Length == 0)
                    //    emailrep = "info@sullolaw.com";
                    //string emailrep = (string)ConfigurationSettings.AppSettings["MailFrom"];
                    //Asad Ali 7925 07/21/2010 Get reply to value from configuration Services...
                    string emailrep = Client.GetValueOfKey(Division.HOUSTON, Module.TRAFFIC_PROGRAM, SubModule.EMAIL,
                                                           Key.EMAIL_FROM_ADDRESS);
                    //Calling send email function
                    //Calling send email function
                    clog.EmailReport(txt_to.Text, emailrep, txt_cc.Text, txt_bcc.Text, txt_subject.Text,
                                     txt_message.Text, filename);
                    //Add email info to database
                    int emailid = clog.SaveEmailInfo(TicketIDList, 2, filename, txt_to.Text, emailrep, txt_cc.Text,
                                                     txt_bcc.Text, txt_subject.Text, txt_message.Text);
                    //Adding To notes
                    clog.AddEmailNote(empid, txt_to.Text, TicketIDList, 2, emailid);
                    //If email addres not already exist of client then save this new one 
                    if (ViewState["vEmail"].ToString().Length == 0)
                    {
                        clog.UpdateClientEmail(TicketIDList, txt_to.Text);
                    }
                    //UpdateCaseHistory();
                    HttpContext.Current.Response.Write("<script language='javascript'> self.close();   </script>");
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }
        private void UpdateCaseHistory()
        {

            string[] keys = { "@ticketid", "@subject", "@notes", "@employeeid" };
            object[] values = { TicketIDList, "Trial Notification emailed", DBNull.Value, empid };
            ClsDb.InsertBySPArr("SP_ADD_NOTES", keys, values);
        }

        protected void btn_SendToBatch_Click(object sender, EventArgs e)
        {

        }

        protected void btn_SendToBatch_Click1(object sender, ImageClickEventArgs e)
        {

        }
    }
}

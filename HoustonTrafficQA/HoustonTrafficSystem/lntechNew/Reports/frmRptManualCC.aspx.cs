using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.WebControls;

namespace lntechNew.Reports
{
    public partial class frmRptManualCC : System.Web.UI.Page
    {        
        clsManualCC Cls_CCDetail =new clsManualCC();
        clsSession cSession = new clsSession();
        clsLogger bugTracker = new clsLogger();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //lbl_message.Visible = false;
                if (cSession.IsValidSession(this.Request) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else //to stop page further execution
                    ViewState["vEmpID"] = cSession.GetCookie("sEmpID", this.Request);

                if (!IsPostBack)
                {
                    calQueryTo.SelectedDate = DateTime.Now.Date;
                    calQueryFrom.SelectedDate = DateTime.Now.Date;
                    FillGrid();
                    SetNavigation();

                    calQueryFrom.SelectedDate =Convert.ToDateTime(DateTime.Now.ToShortDateString());
                    calQueryTo.SelectedDate = Convert.ToDateTime(DateTime.Now.ToShortDateString());
                }
                btn_popup.Attributes.Add("OnClick", "submitPopup()");
            }
            catch (Exception Ex)
            {
                this.lbl_message.Text = Ex.Message + Ex.InnerException;
                bugTracker.ErrorLog(Ex.Message, Ex.Source, Ex.TargetSite.ToString(), Ex.StackTrace);
            }
        }

        protected void btn_search_Click(object sender, EventArgs e)
        {
            try
            {
                this.lbl_message.Text = "";
                FillGrid();
                SetNavigation();
            }
            catch (Exception Ex)
            {
                lbl_message.Text = Ex.Message + Ex.InnerException;
                bugTracker.ErrorLog(Ex.Message, Ex.Source, Ex.TargetSite.ToString(), Ex.StackTrace);
            }
        }
        // FillGrid by Sart Date and End Date
        private void FillGrid()
        {
            try
            {
                DataTable ds_CCDetail = Cls_CCDetail.GetManualCCDetail(calQueryFrom.SelectedDate, calQueryTo.SelectedDate, Convert.ToInt32(ddl_clienttype.SelectedValue));
                dg_manualcc.DataSource = ds_CCDetail;
                dg_manualcc.DataBind();
                FillPageList();
                SetNavigation();
            }
            catch (Exception Ex)
            {
                lbl_message.Text = Ex.Message + Ex.InnerException;
                bugTracker.ErrorLog(Ex.Message, Ex.Source, Ex.TargetSite.ToString(), Ex.StackTrace);
            }
        }
        protected void dg_manualcc_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Header || e.Item.ItemType == ListItemType.Footer)
                    return;
 
                   //DataRowView drv = (DataRowView)e.Item.DataItem;
                   Label lbldes = (Label)e.Item.FindControl("lbl_description");
                   if (lbldes.Text != "")
                    {
                        HtmlControl ctrl = (HtmlControl)e.Item.FindControl("div_status");
                        string descript = lbldes.Text;                      
                        ctrl.Attributes.Add("Title", "hideselects=[on] offsetx=[-200] offsety=[0] singleclickstop=[on] requireclick=[off] header=[] body=[<table width='225px' border='0' cellspacing='0'  class='label' style='BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BORDER-BOTTOM-STYLE: none'><tr><td class='Label'>" + descript + "</td></tr></table>]");
                       // ctrl.Attributes.Add("Title", descript);
                    }
                    //if (drv != null)
                    //{

                        Label lblrecdate = (Label)e.Item.FindControl("lbl_tranddate");
                        LinkButton lnkBtn_void = (LinkButton)e.Item.FindControl("lnkbtn_void");
                        LinkButton lnkBtn_refund = (LinkButton)e.Item.FindControl("lnkbtn_refund");

                        string status = ((Label)(e.Item.FindControl("lbl_transstatus"))).Text;
                        if (status.ToUpper().Equals("DECLINED"))
                        {
                            //lnkBtn_refund.Enabled = false;
                            //lnkBtn_void.Enabled = false;
                            lnkBtn_refund.Visible = false;
                            lnkBtn_void.Visible = false;
                            return;
                        }


                        HiddenField lnkbtnrefund = (HiddenField)e.Item.FindControl("hf_ref");
                        HiddenField lnkbtnvoid = (HiddenField)e.Item.FindControl("hf_voidpayment");

                        if (lnkbtnrefund.Value == "True")
                            lnkBtn_refund.Enabled = false;
                        else
                            lnkBtn_refund.Attributes.Add("OnClick", "showpopup('" + e.Item.ClientID + "',1);return false");
                        
                        if (lnkbtnvoid.Value == "True")
                            lnkBtn_void.Enabled = false;
                        else
                                                  
                            lnkBtn_void.Attributes.Add("OnClick", "showpopup('" + e.Item.ClientID + "',0);return false");

                        
                        DateTime expdate = new DateTime();
                        expdate = Convert.ToDateTime(lblrecdate.Text);
                        expdate = expdate.AddDays(120);

                            if (expdate > DateTime.Now)
                            {
                                DateTime dt = new DateTime();
                                dt = Convert.ToDateTime(lblrecdate.Text);

                                if (dt.ToShortDateString().Equals(DateTime.Now.ToShortDateString()))
                                {
                                    lnkBtn_refund.Visible = false;
                                    lnkBtn_void.Visible = true;
                                }
                                else
                                {
                                    lnkBtn_refund.Visible = true;
                                    lnkBtn_void.Visible = false;
                                }
                                
                            }
                            else
                            {
                                lnkBtn_refund.Visible = true;
                                lnkBtn_void.Visible = false;
                               // lnkBtn_refund.Enabled = false;

                            }

                           
                    
               
            }
            catch (Exception Ex)
            {
                lbl_message.Text = Ex.Message + Ex.InnerException;
                bugTracker.ErrorLog(Ex.Message, Ex.Source, Ex.TargetSite.ToString(), Ex.StackTrace);
            }
        }

        // popup button to send request to gateway and save response in database
        protected void btn_popup_Click(object sender, EventArgs e)
        {
            try
            {
                this.lbl_message.Text = "";
                Cls_CCDetail.Description = tb_description.Text;
                Cls_CCDetail.EmployeeID = Convert.ToInt32(ViewState["vEmpID"]);
                
                if(hf_transtype.Value == "refund")
                    Cls_CCDetail.RefundCreditCardTransaction(Convert.ToInt32(hf_paymentid.Value));
                else
                    Cls_CCDetail.VoidCreditCardTransaction(Convert.ToInt32(hf_paymentid.Value));
            }
            catch (Exception Ex)
            {   
                lbl_message.Text = Ex.Message;
                bugTracker.ErrorLog(Ex.Message, Ex.Source, Ex.TargetSite.ToString(), Ex.StackTrace);
            }
        }

        protected void dg_manualcc_SelectedIndexChanged(object sender, EventArgs e)
        {
           

        }

        protected void dg_manualcc_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            try
            {
                this.lbl_message.Text = "";
                GotoPage(e.NewPageIndex);
                
                
            }
            catch (Exception Ex)
            {
                lbl_message.Text = Ex.Message;
                bugTracker.ErrorLog(Ex.Message, Ex.Source, Ex.TargetSite.ToString(), Ex.StackTrace);
            }
        }
        // GotoPage to navigation 
        private void GotoPage(int pageno)
        {
            try
            {
                dg_manualcc.CurrentPageIndex = pageno;
                lblPNo.Text = Convert.ToString(pageno);
                FillGrid();
                FillPageList();
                SetNavigation();
            }
            catch (Exception Ex)
            {
                lbl_message.Text = Ex.Message;
                bugTracker.ErrorLog(Ex.Message, Ex.Source, Ex.TargetSite.ToString(), Ex.StackTrace);
            }
        }
        // Fill Dropdown with all page number of grid
        private void FillPageList()
        {
            try
            {
                Int16 idx;

                cmbPageNo.Items.Clear();
                for (idx = 1; idx <= dg_manualcc.PageCount; idx++)
                {
                    cmbPageNo.Items.Add("Page - " + idx.ToString());
                    cmbPageNo.Items[idx - 1].Value = idx.ToString();
                }
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void cmbPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.lbl_message.Text = "";
                GotoPage(cmbPageNo.SelectedIndex);
               
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        private void SetNavigation()
        {
            try
            {
                // setting current page number
                lblPNo.Text = Convert.ToString(dg_manualcc.CurrentPageIndex + 1);
                lblCurrPage.Visible = true;
                lblPNo.Visible = true;

                // filling combo with page numbers
                lblGoto.Visible = true;
                cmbPageNo.Visible = true;             

                cmbPageNo.SelectedIndex = dg_manualcc.CurrentPageIndex;
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
    }
}

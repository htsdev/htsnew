using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;
using System.Data.SqlClient;
//Waqas 5057 03/19/2009 Changed to HTP
namespace HTP.Reports
{
    public partial class OpenServiceTicketReport : System.Web.UI.Page
    {

        #region Variables

        clsSession cSession = new clsSession();
        clsLogger clog = new clsLogger();
        OpenServiceTickets openServiceTickets = new OpenServiceTickets();
        clsSession ClsSession = new clsSession();
        DataView DV;

        string StrExp = String.Empty;
        string StrAcsDec = String.Empty;
        string usertype = "";
        string empid = "";
        string showAllMy = "0";
        string showAllUser = "0";
        int showAllfollowup = 0;

        #endregion


        override protected void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            PagingControl1.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(PagingControl1_PageIndexChanged);
            PagingControl1.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
        }


        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Waqas 5057 03/17/2009 Checking employee info in session
                if (cSession.IsValidSession(this.Request, this.Response, this.Session) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else
                {

                    PagingControl1.GridView = gv;

                    if (!IsPostBack)
                    {
                        if (Request.QueryString["showAllMy"] != null || Request.QueryString["fromPopup"] != null)
                        {
                            this.showAllMy = "1";
                            //Sabir Khan 5738 05/25/2009 code commented...
                            //this.chkShowAllMyRecords.Checked = true;
                            Session["ShowIncomplete"] = "1";
                        }
                        // Noufil 4429 07/21/2008 Make Session variable Expire
                        Session["OSDS"] = null;
                        Session["showAllMy"] = null;
                        Session["ShowIncomplete"] = null;

                        string usertype = cSession.GetCookie("sAccessType", this.Request).ToString();
                        string empid = cSession.GetCookie("sEmpID", this.Request).ToString();
                        //khalid 3016 2/12/08 adding note for closing  ticket
                        ViewState["empId"] = empid;
                        string IsSPNUser = cSession.GetCookie("sIsSPNUser", this.Request).ToString();

                        //Sabir Khan 5738 05/25/2009 code commented...
                        //if (usertype == "2" || IsSPNUser == "True")
                        //    chkShowAllUserRecords.Visible = true;
                        //else
                        //    chkShowAllUserRecords.Visible = false;

                        //Sabir Khan 5738 05/25/2009 Check for admin users.
                        //Sabir Khan 5738 05/25/2009 code commented...
                        //if (usertype == "2")
                        //{
                        //    cb_usedate.Visible = false;
                        //    chkShowAllMyRecords.Visible = false;
                        //    chkShowAllUserRecords.Visible = false;
                        //}
                        //else
                        //{
                        //    cb_usedate.Visible = true;
                        //    chkShowAllMyRecords.Visible = true;
                        //    chkShowAllUserRecords.Visible = true;
                        //}



                        lblMessage.Text = "";
                        caldatefrom.SelectedDate = DateTime.Now;
                        caldateto.SelectedDate = DateTime.Now;


                        FillControls();


                        FillGrid();


                    }
                    //Yasir Kamal 5759 04/09/2009 pagging control issue fixed.
                    //Sabir Khan 4910 10/07/2008
                    //---------------------------------
                    //if (Session["ReloadWindow"] != null)
                    //{
                    //    FillGrid();
                    //    Session["ReloadWindow"] = null;
                    //}
                    //---------------------------------
                                    
                }
                PagingControl1.Visible = true;
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        /// <summary>
        /// Method to handle PagingControl pageIndexChange Event.
        /// </summary>
        /// <param name="pageSize"></param>

        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            if (pageSize > 0)
            {
                gv.PageIndex = 0;
                gv.PageSize = pageSize;
                gv.AllowPaging = true;

            }
            else
            {
                gv.AllowPaging = false;
            }

            FillGrid();
         

        }

        protected void btnDum_Click(object sender, EventArgs e)
        {
            try
            {
                Session["OSDS"] = null;
                Session["showAllMy"] = null;
                Session["ShowIncomplete"] = null;
                FillGrid();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        //5759 end

        void PagingControl1_PageIndexChanged()
        {
            gv.PageIndex = PagingControl1.PageIndex - 1;
            FillGrid();
            PagingControl1.SetPageIndex();
        }

        protected void gv_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                //gv.PageIndex = e.NewPageIndex;
                //DV = (DataView)Session["OSDS"];
                //gv.DataSource = DV;
                //gv.DataBind();
                //SerialNumber();
                //DL_pages.SelectedIndex = e.NewPageIndex;
                //LB_curr.Text = (gv.PageIndex + 1).ToString();

                gv.PageIndex = e.NewPageIndex;
                FillGrid();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void gv_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "close")
            {

                //Code Is Modified By Zeeshan Ahmed On 1/22/2008
                string[] parameters;
                parameters = e.CommandArgument.ToString().Split(new Char[] { '-' });

                int caseTypeId = Convert.ToInt32(parameters[2]);
                int ticketId = Convert.ToInt32(parameters[1]);
                int fId = Convert.ToInt32(parameters[0]);
                string employeeName = cSession.GetCookie("sEmpName", HttpContext.Current.Request);

                //Close Service Ticket
                openServiceTickets.CloseServiceTicket(Convert.ToInt32(ticketId), Convert.ToInt32(fId));

                //Send Clode Service Ticket Notification Email                
                openServiceTickets.SendServiceTicketNotificationEmail(ServiceTicketType.Closed, ticketId, fId, employeeName, Request.Url.Authority, caseTypeId);
                //khalid 3016 2/12/08 adding note for open ticket
                clog.AddNote(Convert.ToInt32(ViewState["empId"]), "Service Ticket Closed", "", ticketId);

                //Refresh Grid
                //khalid 3016 2/12/08 this is added to refresh page when ticket is closed   
                Session["OSDS"] = null;

                FillGrid();

            }
        }

        protected void btn_search_Click(object sender, EventArgs e)
        {
            try
            {
                Session["OSDS"] = null;
                Session["showAllMy"] = null;
                Session["ShowIncomplete"] = null;
                gv.PageIndex = 0;
                FillGrid();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string ticketid = ((HiddenField)e.Row.FindControl("HFTicketid")).Value.ToString();
                    // khalid  2727 date : 30/1/08 to remove unused field 
                    //string flagid = ((HiddenField)e.Row.FindControl("HFFlagid")).Value.ToString();
                    // Zahoor 4770 09/16/08
                    string Fid = ((HiddenField)e.Row.FindControl("HFFid")).Value.ToString();
                    string catgory = ((Label)e.Row.FindControl("lblCategory")).Text;
                    if (catgory.ToUpper() == "CONTINUANCE")
                    {
                        ((Label)e.Row.FindControl("lblCategoryOption")).Visible = true;
                    }
                    else
                    {
                        ((Label)e.Row.FindControl("lblCategoryOption")).Visible = false;
                    }
                    string slabel = ((Label)e.Row.FindControl("lblCategoryOption")).Text;
                    if (slabel != "")
                    {
                        ((Label)e.Row.FindControl("lblCategoryOption")).Text = "[" + slabel + "]";
                    }
                    else
                    {
                        ((Label)e.Row.FindControl("lblCategoryOption")).Text = string.Empty;
                    }
                    // 4770 End

                    ViewState["ticketid2"] = Convert.ToInt32(ticketid);

                    //Added By Zeeshan Ahmed on 6/12/2007
                    string ClientName = ((HyperLink)e.Row.FindControl("HPClientName")).Text;

                    //Remove Extra Character From Client Name
                    ClientName = ClientName.Replace("\n", "");
                    ClientName = ClientName.Replace("\r", "");
                    ClientName = ClientName.Replace(" ", "");
                    ClientName = ClientName.Replace(",", " , ");
                    //Adding Delete Javascript
                    ((ImageButton)e.Row.FindControl("imgbtn_close")).OnClientClick = "return Delete('" + ClientName + "','" + ((Label)e.Row.FindControl("lbl_percentage")).Text + "');";

                    ((HyperLink)e.Row.FindControl("HPClientName")).NavigateUrl = "../clientinfo/violationfeeold.aspx?search=0&caseNumber=" + ticketid;
                    ((ImageButton)e.Row.FindControl("HpLastUpdate")).Attributes.Add("onclick", "return PopUp('" + ticketid + "','" + Fid + "')");

                    // Noufil 5069 11/04/2008 Code comment because date format is handle from SQL
                    //((Label)e.Row.FindControl("lbllastupdate")).Text = clsGeneralMethods.FormatDate(((Label)e.Row.FindControl("lbllastupdate")).Text);
                    //((Label)e.Row.FindControl("lblcloseddate")).Text = clsGeneralMethods.FormatDate(((Label)e.Row.FindControl("lblcloseddate")).Text);
                    ((Label)e.Row.FindControl("lbl_crtdate")).Text = clsGeneralMethods.FormatDate(((Label)e.Row.FindControl("lbl_crtdate")).Text);

                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void gv_Sorting(object sender, GridViewSortEventArgs e)
        {
            SortGrid(e.SortExpression);
        }

        #endregion

        #region Methods


        private void FillGrid()
        {
            try
            {
                lblMessage.Text = "";
                //Sabir Khan 5738 05/25/2009 code commented...
                //showAllMy = (chkShowAllMyRecords.Checked == true ? "1" : "0");
                //showAllUser = (chkShowAllUserRecords.Checked == true ? "1" : "0");
                //Sabir Khan 5738 Set follow up date
                showAllfollowup = (chkShowAllfollowupRecords.Checked == true ? 1 : 0);

                // tahir 6082 06/26/2009 added show all past due..
                //string showIncomplete = (Session["ShowIncomplete"] == null ? "0" : "1");
                string showIncomplete = (chkAllPastDue.Checked == true ? "1" : "0");

                //Sabir Khan 5738 05/25/2009 code commented...
                //----------------------------------------------
                //int usedate;
                //if (chkShowAllMyRecords.Checked == false && chkShowAllUserRecords.Checked == false && cb_usedate.Checked == false)
                //{
                //    cb_usedate.Checked = true;
                //    usedate = 1;
                //}
                //else
                //    usedate = Convert.ToInt32(cb_usedate.Checked);
                //-------------------------------------------------

                // Agha Usman 4271 06/25/2008
                // Noufil 5069 11/05/2008  Add new parameter date range.
                //Sabir Khan  5738 05/25/2009 old code has been commented...
                //--------------------------------------------------
                //DataTable DT = openServiceTickets.GetRecords(caldatefrom.SelectedDate.ToShortDateString(), caldateto.SelectedDate.ToShortDateString(),
                //    Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request)), rbl_tickettype.SelectedValue, showIncomplete, this.showAllMy, this.showAllUser, int.Parse(ddlDivision.SelectedValue), usedate, showAllfollowup);
                //Sabir Khan  5738 05/25/2009 Add follow update and remove showallmy ticket, showallticket and usedate from this method....
                DataTable DT = openServiceTickets.GetRecords(caldatefrom.SelectedDate.ToShortDateString(), caldateto.SelectedDate.ToShortDateString(),
                    Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request)), rbl_tickettype.SelectedValue, showIncomplete,  int.Parse(ddlDivision.SelectedValue), showAllfollowup);
                //---------------------------------------------------------
                //Kazim 2790 2/29/2008 Replace Session["DS"] to Session["OSDS"]

                //DataSet DS = FillData.GetRecords(caldatefrom.SelectedDate.ToShortDateString(), caldateto.SelectedDate.ToShortDateString(),2,3991,"0" ,"0");
                if (DT.Rows.Count > 0)
                {
                   // int index = -1;

                    SerialNumber(DT);

                    if (Session["OSDS"] == null)
                    {
                        DV = new DataView(DT);
                        Session["OSDS"] = DV;
                    }
                    else
                    {

                        DV = (DataView)Session["OSDS"];
                    }

                    usertype = cSession.GetCookie("sAccessType", this.Request).ToString();
                    empid = cSession.GetCookie("sEmpID", this.Request).ToString();
                    string IsSPNUser = cSession.GetCookie("sIsSPNUser", this.Request).ToString();

                 
                    PagingControl1.GridView = gv;
                    gv.DataSource = DV;
                    gv.DataBind();
                  
                    gv.Columns[11].Visible = false;
                    PagingControl1.PageIndex = gv.PageIndex;
                    PagingControl1.PageCount = gv.PageCount;
                    PagingControl1.SetPageIndex();

                    if (usertype == "2" || empid == "4028" || IsSPNUser == "True")
                        gv.Columns[16].Visible = true;
                    else
                        gv.Columns[16].Visible = false;

                    if (rbl_tickettype.SelectedValue == "0")
                    {
                        gv.Columns[11].Visible = false;
                        gv.Columns[16].Visible = false;
                        gv.Columns[12].Visible = true;


                    }
                    else
                    {
                        gv.Columns[11].Visible = true;
                        gv.Columns[16].Visible = true;
                        gv.Columns[12].Visible = false;
                    }


                    //Yasir Kamal 5759 04/09/2009 show ASSN Column
                    //Display Assigned To Column If User Select Show All User Tickets
                    //if (chkShowAllUserRecords.Checked & chkShowAllUserRecords.Visible)
                    //    gv.Columns[14].Visible = true;
                    //else
                    //    gv.Columns[14].Visible = false;

                    //5759 end 


                    gv.Visible = true;
                   
                }
                else
                {
                    PagingControl1.PageCount = 0;
                    PagingControl1.PageIndex = 0;
                    PagingControl1.SetPageIndex();
                    lblMessage.Text = "No Record Found";
                    gv.Visible = false;
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        // Agha Usman 4271 06/25/2008
        private void FillControls()
        {
            try
            {
                clsCase objCase = new clsCase();
                DataTable dt = objCase.GetCaseType();
                if (dt.Rows.Count > 0)
                {
                    ddlDivision.DataSource = dt;
                    ddlDivision.DataTextField = "CaseTypeName";
                    ddlDivision.DataValueField = "CaseTypeId";
                    ddlDivision.DataBind();

                    ddlDivision.Items.Insert(0, new ListItem("--- All ---", "-1"));
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        //Yasir Kamal 5759 04/10/2009 sorting issue fixed
        private void SerialNumber( DataTable DT)
        {

            try
            {
                int sno = 1;
                if (!DT.Columns.Contains("sno"))
                    DT.Columns.Add("sno");


                if (DT.Rows.Count >= 1)
                    DT.Rows[0]["sno"] = 1;

                if (DT.Rows.Count >= 2)
                {
                    for (int i = 1; i < DT.Rows.Count; i++)
                    {

                        DT.Rows[i]["sno"] = ++sno;
                        
                    }
                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void SortGrid(string SortExp)
        {
           DataTable sortData;
            try
            {
                SetAcsDesc(SortExp);
                DV = (DataView)Session["OSDS"];
                DV.Sort = StrExp + " " + StrAcsDec;

                sortData = DV.ToTable();
                SerialNumber(sortData);
                gv.DataSource = sortData;
                gv.DataBind();
                PagingControl1.PageCount = gv.PageCount;
                PagingControl1.PageIndex = gv.PageIndex;
                PagingControl1.SetPageIndex();
                Session["OSDS"] = sortData.DefaultView;
                
                //  Session["OSDS"] = DV;
                //  FillGrid();
                //gv.DataSource = DV;
                //gv.DataBind();
                //SerialNumber();
                //5759 end
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }

        private void SetAcsDesc(string Val)
        {
            try
            {
                StrExp = Session["StrExp"].ToString();
                StrAcsDec = Session["StrAcsDec"].ToString();
            }
            catch
            {

            }

            if (StrExp == Val)
            {
                if (StrAcsDec == "ASC")
                {
                    StrAcsDec = "DESC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
                else
                {
                    StrAcsDec = "ASC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
            }
            else
            {
                StrExp = Val;
                StrAcsDec = "ASC";
                Session["StrExp"] = StrExp;
                Session["StrAcsDec"] = StrAcsDec;
            }
        }

        public void StatusChanged()
        {
            lbl_Message.Text = "status changed";

        }

        #endregion

      

    }
}

﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using lntechNew.Components;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using System.IO;
using CrystalDecisions.CrystalReports.Engine;

namespace HTP.Reports
{
    public partial class frmCertifiedLORBatchPrint : System.Web.UI.Page
    {
        clsSession uSession = new clsSession();
        clsLogger bugTracker = new clsLogger();
        protected void Page_Load( object sender, EventArgs e )
        {
            try
            {
                if ( !Page.IsPostBack )
                {
                    if ( uSession.GetCookie( "sEmpID", this.Request ) != "" )
                    {
                        ViewState["vCMN"] = Request.QueryString["CMN"].ToString();
                    }
                    else
                    {
                        HttpContext.Current.Response.Write( "<script language='javascript'> alert('Session has been expired. please login Again');   </script>" );
                        HttpContext.Current.Response.Write( "<script language='javascript'> window.close();   </script>" );
                    }                   
                }
            }
            catch ( Exception ex )
            {
                bugTracker.ErrorLog( ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace );

            }
        }
    }
}

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HMCMissingXRefReport.aspx.cs"
    Inherits="HTP.Reports.HMCMissingXRefReport" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <title>HMC Missing X-Ref</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table cellspacing="0" cellpadding="0" width="780" align="center" border="0">
            <tr>
                <td align="center">
                    <uc1:ActiveMenu ID="ActiveMenu1" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="center" background="../images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td background="../Images/subhead_bg.gif" class="clssubhead" align="left" style="height: 34px">
                  <table style="width: 100%">
                                        <tr>
                                            <td>
                                            </td>
                                            <td align="right" valign="middle">
                                                <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                                
                                            </td>
                                        </tr>
                                    </table>
                </td>
            </tr>
           <tr>
                                <td align="center">
                                    <asp:Label ID="lbl_message" runat="server" CssClass="label" ForeColor="Red"></asp:Label>
                                </td>
                            </tr>
            <tr>
                <td align="center">
                    <asp:GridView ID="gv_Data" AllowPaging="True" runat="server" HeaderStyle-HorizontalAlign="Left" CellSpacing="0"
                        AutoGenerateColumns="False" CssClass="clsleftpaddingtable" Width="100%" PageSize="20"
                        onpageindexchanging="gv_Data_PageIndexChanging">
                        <PagerStyle HorizontalAlign="Center" />
                                                                <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                                    FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                        <Columns>
                            <asp:TemplateField HeaderText="S#" HeaderStyle-Width="30px">
                                <ItemTemplate>
                                    <asp:HyperLink ID="hl_Sno" CssClass="clssubhead" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                        runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SNo") %>'></asp:HyperLink>
                                </ItemTemplate>
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Client Name">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_CauseNumber" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.[Client Name]") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Date of birth">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_TicketNumber" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.[Date of birth]") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Hire Date">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_TicketNumber" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.[Hire Date]") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Court Date">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_CourtDate" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.Courtdate") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Ticket Number">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_FirstName" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.TicketNumber") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td width="100%">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="center" background="../images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td>
                    <uc2:Footer ID="Footer1" runat="server" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

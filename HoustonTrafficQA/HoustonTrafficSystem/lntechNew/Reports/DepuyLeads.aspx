﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DepuyLeads.aspx.cs" Inherits="lntechNew.Reports.DepuyLeads" %>

<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc12" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Depuy Leads</title>

    <script src="../Scripts/Dates.js" type="text/javascript"></script>

    <script type="text/javascript" language="javascript">
    
     function EnabledPaymentsFilterControls( cal_FromDateFilterClientId, cal_ToDateFilterClientId, chkShowAllClientId,actionNumber)
        {
            
            {
                var calFromCtrl = document.getElementById( cal_FromDateFilterClientId );
                var calToCtrl = document.getElementById( cal_ToDateFilterClientId );
                var chkShowAll = document.getElementById( chkShowAllClientId );
                calFromCtrl.disabled = chkShowAll.checked;
                calToCtrl.disabled = chkShowAll.checked;
            }  
        } 
     function CheckDateValidation()
        {
        
            var chkShowAll = document.getElementById( '<%=chkShowAll.ClientID%>' );
            if (!chkShowAll.checked)
            {
                var from = document.form1.cal_FromDateFilter.value;
                var To  = document.form1.cal_ToDateFilter.value;
            
                if (IsDatesEqualOrGrater(from,'MM/dd/yyyy','01/01/1900','MM/dd/yyyy')==false)
                {
			        alert("Please enter valid date, From Date must be grater then or equal to 1/1/1900");
			        return false;
			    }
    			
			    if (IsDatesEqualOrGrater(To,'MM/dd/yyyy','01/01/1900','MM/dd/yyyy')==false)
                {
			        alert("Please enter valid date, To Date must be grater then or equal to 1/1/1900");
    				
				    return false;
			    }
                if (IsDatesEqualOrGrater(To,'MM/dd/yyyy',from,'MM/dd/yyyy')==false)
                {
			        alert("Please enter valid date, To Date must be grater then or equal to From Date");
    			     
				    return false;
			    }
			    else
			    {
			        return true;
			    }
		    }
		    else
		    {
		        return true;
		    }
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <aspnew:ScriptManager ID="ScriptManager1" runat="server" />
        <table cellspacing="0" cellpadding="0" width="900px" align="center" border="0">
            <tr>
                <td>
                    <uc12:ActiveMenu ID="ActiveMenu1" runat="server" />
                </td>
            </tr>
            <tr>
                <td style="width: 100%" background="../Images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td>
                    <table border="0" width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="width: 135px">
                                <span class="clssubhead">Contact Date Range :</span>
                            </td>
                            <td align="left" style="width: 130px">
                                <span class="clsLabel">From :</span>
                                <ew:CalendarPopup Visible="true" ID="cal_FromDateFilter" runat="server" AllowArbitraryText="False"
                                    CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Culture="(Default)" EnableHideDropDown="True"
                                    Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif" Nullable="False"
                                    PadSingleDigits="True" ShowGoToToday="True" Text=" " ToolTip="Date" UpperBoundDate="9999-12-29"
                                    Width="65px">
                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TextboxLabelStyle CssClass="clstextarea" />
                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Gray" />
                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                </ew:CalendarPopup>
                            </td>
                            <td align="left" style="width: 120px">
                                <span class="clsLabel">To :&nbsp;</span><ew:CalendarPopup Visible="true" ID="cal_ToDateFilter"
                                    runat="server" AllowArbitraryText="False" CalendarLocation="Bottom" ControlDisplay="TextBoxImage"
                                    Culture="(Default)" EnableHideDropDown="True" Font-Names="Tahoma" Font-Size="8pt"
                                    ImageUrl="../images/calendar.gif" Nullable="False" PadSingleDigits="True" ShowGoToToday="True"
                                    Text=" " ToolTip="Date" UpperBoundDate="9999-12-29" Width="65px">
                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TextboxLabelStyle CssClass="clstextarea" />
                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Gray" />
                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                </ew:CalendarPopup>
                            </td>
                            <td align="left" style="width: 70px">
                                <span style="font-size: XX-Small;">
                                    <asp:CheckBox ID="chkShowAll" runat="server" Text="Show All" />
                                </span>
                            </td>
                            <td align="left">
                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="clsbutton" OnClick="btnSubmit_Click"
                                    OnClientClick="return CheckDateValidation();" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td background="../Images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td background="../Images/subhead_bg.gif" class="clssubhead" align="left" style="height: 34px">
                    <table width="100%">
                        <tr>
                            <td align="left" class="clssubhead">
                                Depuy Leads
                            </td>
                            <td align="right" class="clssubhead">
                                <uc3:PagingControl ID="Pagingctrl" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table id="TableGrid" bgcolor="white" border="0" cellpadding="0" cellspacing="0"
                        width="100%">
                        <tr>
                            <td background="../../images/separator_repeat.gif" colspan="5" height="11" style="height: 11px"
                                width="780">
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="width: 100%">
                                <asp:Label ID="lblMessage" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2" valign="top">
                                <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlResult">
                                    <ProgressTemplate>
                                        <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl1" runat="server" Text="Please Wait ......"
                                            CssClass="clsLabel"></asp:Label>
                                    </ProgressTemplate>
                                </aspnew:UpdateProgress>
                                <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:GridView ID="gv_Records" runat="server" AutoGenerateColumns="False" Width="100%"
                                            CssClass="clsLeftPaddingTable" AllowPaging="True" PageSize="20" OnPageIndexChanging="gv_Records_PageIndexChanging"
                                            OnRowDataBound="gv_Records_RowDataBound">
                                            <Columns>
                                                <asp:BoundField HeaderText="S#" DataField="SNo" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" />
                                                    <ItemStyle CssClass="GridItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Contact Name" DataField="PersonName" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" />
                                                    <ItemStyle CssClass="GridItemStyle" Width="80px" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Cont Date" DataField="RecDate" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead" DataFormatString="{0:MM/dd/yyyy}">
                                                    <HeaderStyle CssClass="clssubhead" />
                                                    <ItemStyle CssClass="GridItemStyle" Width="50px" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Address" DataField="Address1" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" />
                                                    <ItemStyle CssClass="GridItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Primary Phone & Type" DataField="Phone1" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" />
                                                    <ItemStyle CssClass="GridItemStyle" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Email Address" HeaderStyle-CssClass="clssubhead">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_Email" runat="server" CssClass="GridItemStyle"></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="clssubhead" />
                                                    <ItemStyle CssClass="GridItemStyle" HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                                <asp:BoundField HeaderText="DOB" DataField="DOB" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead" DataFormatString="{0:MM/dd/yyyy}">
                                                    <HeaderStyle CssClass="clssubhead" />
                                                    <ItemStyle CssClass="GridItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Srgy Date" DataField="SurgeryDate" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead" DataFormatString="{0:MM/dd/yyyy}">
                                                    <HeaderStyle CssClass="clssubhead" />
                                                    <ItemStyle CssClass="GridItemStyle" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Practice Area" HeaderStyle-CssClass="clssubhead">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_PracticeArea" runat="server" CssClass="GridItemStyle"></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="clssubhead" />
                                                    <ItemStyle CssClass="GridItemStyle" HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                                <asp:BoundField HeaderText="Manufacturer" DataField="Manufacturer" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" />
                                                    <ItemStyle CssClass="GridItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Remarks" DataField="Comments" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" />
                                                    <ItemStyle CssClass="GridItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="PainLevel" HeaderText="Pain Level" HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" />
                                                    <ItemStyle CssClass="GridItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="recalled" HeaderText="Recalled" HeaderStyle-CssClass="clssubhead"
                                                    Visible="False">
                                                    <HeaderStyle CssClass="clssubhead" />
                                                    <ItemStyle CssClass="GridItemStyle" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                    <ItemStyle CssClass="GridItemStyle" />
                                                </asp:BoundField>
                                            </Columns>
                                            <PagerStyle HorizontalAlign="Center" />
                                            <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                            <EmptyDataTemplate>
                                                <asp:Label ID="Label1" runat="server" Text="No Records Found" CssClass="clsLabel"
                                                    ForeColor="Red"></asp:Label>
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </ContentTemplate>
                                </aspnew:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td background="../../images/separator_repeat.gif" colspan="5" style="height: 11px;
                                width: 780">
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td style="display: none;">
                            </td>
                            <td>
                                <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

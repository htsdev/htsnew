using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using lntechNew.Components;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;
using lntechNew.Components.ClientInfo;


namespace lntechNew.Reports
{
	/// <summary>
	/// Summary description for RptWordLetter.
	/// </summary>
	public partial class RptWordLetter : System.Web.UI.Page
	{
		int ticketno;
		int empid;
        int lettertype;
		clsSession uSession = new clsSession();
		clsLogger clog = new clsLogger();
		
		private void Page_Load(object sender, System.EventArgs e)
		{
            if (!Page.IsPostBack)
            {
                // Zahoor 4676 09/17/08 For Cookies expiration handling.
                if (uSession.GetCookie("sEmpID", this.Request) != "")
                {
                    //ticketno =Convert.ToInt32(uSession.GetCookie("sTicketID",this.Request));		
                    ticketno = Convert.ToInt32(Request.QueryString["casenumber"]);
                    empid = Convert.ToInt32(uSession.GetCookie("sEmpID", this.Request));
                    lettertype = Convert.ToInt32(Request.QueryString["lettertype"]);
                    //clog.AddNote(empid,"Letter of rep Printed","",ticketno);

                    // Abid Ali 5359 2/10/2009 for letter of rep send to batch
                    ViewState["vBatch"] = (Request.QueryString["batch"] == null ? false.ToString() : Request.QueryString["batch"].ToString());                                       

                    CreateApprovalReport();                    
                }
                else
                {
                    Response.Redirect("../frmlogin.aspx");
                }
                // 4676
            }
		}

		public void CreateApprovalReport() 
		{ 
			clsCrsytalComponent Cr = new clsCrsytalComponent();
			string[] key = {"@ticketid","@empid"};
			object[] value1 = {ticketno, empid};
			string filename;
            string filepath;

            if (lettertype == 6)
            {
                // Noufil 3999 05/09/2008 Creating report and getting its path from where it is created
                filename = Server.MapPath("") + "\\Wordreport.rpt";

                // Abid Ali 5359 12/30/2008 Letter or send to batch or not
                string sendToBatch = "false";
                if (ViewState["vBatch"] != null)
                {
                    bool isBatch;
                    if (Boolean.TryParse(ViewState["vBatch"].ToString(), out isBatch))
                        sendToBatch = isBatch.ToString().ToLower();
                }

                filepath = Cr.CreateReport( filename, "USP_HTS_LETTER_OF_REP", key, value1, sendToBatch, lettertype, empid, this.Session, this.Response );
                Session["reportpath"] = filepath;
            }
            else if (lettertype == 7)
            {
                filename = Server.MapPath("") + "\\Motion_Limine.rpt";
                filepath = Cr.CreateReport(filename, "USP_HTS_Motion_Limine", key, value1, "false", lettertype, empid, this.Session, this.Response);
            }
            else if (lettertype == 8)
            {
                filename = Server.MapPath("") + "\\Motion_Discovery.rpt";
                filepath = Cr.CreateReport(filename, "USP_HTS_Motion_Discovery", key, value1, "false", lettertype, empid, this.Session, this.Response);
            }
			
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;
using System.Drawing;

namespace HTP.Reports
{
    // Noufil 6126 07/16/2009 New report created.
    public partial class NewHireReport : System.Web.UI.Page
    {
        #region Variables
        ValidationReports reports = new ValidationReports();
        clsCase cCase = new clsCase();
        clsSession cSession = new clsSession();
        DataTable dt;
        #endregion

        #region Properties
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (cSession.IsValidSession(this.Request, this.Response, this.Session) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }

                else
                {
                    lbl_Message.Text = "";
                    if (!IsPostBack)
                    {

                        ViewState["empid"] = Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request));
                        GetRecords();
                    }

                    Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
                    Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
                    Pagingctrl.GridView = gv_Data;

                    if (cSession.GetCookie("sAccessType", this.Request).ToString() != "2")
                    {
                        tr_showsetting.Style[HtmlTextWriterStyle.Display] = "none";
                        tr_showsettingseperator.Style[HtmlTextWriterStyle.Display] = "none";
                    }
                    else
                    {
                        tr_showsetting.Style[HtmlTextWriterStyle.Display] = "block";
                        tr_showsettingseperator.Style[HtmlTextWriterStyle.Display] = "block";
                    }

                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void gv_Data_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_Data.PageIndex = e.NewPageIndex;
                GetRecords();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected override void OnInit(EventArgs e)
        {
            ShowSetting.OnErr += new HTP.WebControls.ShowSetting.ErrHandler(ShowSetting_OnErr);
            ShowSetting.dbBind += new HTP.WebControls.ShowSetting.Databind(ShowSetting_dbBind);
            base.OnInit(e);
        }

        protected void gv_Data_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (((HyperLink)e.Row.FindControl("hlnk_SNo")).Text.Trim() == string.Empty)
                {
                    e.Row.CssClass = string.Empty;
                    for (int j = 0; j <= 6; j++)
                    {
                        e.Row.Cells[j].CssClass = "TDHeading";
                        e.Row.Cells[j].BorderWidth = Unit.Pixel(0);
                        e.Row.Cells[j].Height = Unit.Pixel(15);
                        ((Label)e.Row.FindControl("lbl_FirstName")).CssClass = "";
                        ((ImageButton)e.Row.FindControl("imgbtn_cancel")).Visible = false;
                    }
                }
            }
        }

        protected void btnupdatecomments_Click(object sender, EventArgs e)
        {
            try
            {
                cCase.UpdateGeneralComments(Convert.ToInt32(ViewState["ticketid"]), Convert.ToInt32(ViewState["empid"]), txt_Gcomments.Text);
                cCase.UpdateClientNewHireStatus(Convert.ToInt32(ViewState["ticketid"]), Convert.ToInt32(ViewState["empid"]), 2, false);
                GetRecords();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void gv_Data_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "remove")
            {
                ViewState["ticketid"] = e.CommandArgument;
                cCase.TicketID = Convert.ToInt32(e.CommandArgument);
                lbl_comments.Text = cCase.GetGeneralCommentsByTicketId();
                txt_Gcomments.Text = "";
                mp_Showcomments.Show();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        ///     ShowSetting display message on error
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="ErrorMsg"></param>
        void ShowSetting_OnErr(object sender, string ErrorMsg)
        {
            lbl_Message.Text = ErrorMsg;
        }


        /// <summary>
        ///     Show Setting Control Databind
        /// </summary>
        void ShowSetting_dbBind()
        {
            //GetRecords();
        }



        /// <summary>
        ///     To Fill data into Grid
        /// </summary>
        private void GetRecords()
        {
            try
            {
                dt = reports.GetNewHireReport();

                if (dt.Rows.Count == 0)
                {
                    Pagingctrl.PageCount = 0;
                    Pagingctrl.PageIndex = 0;
                    Pagingctrl.SetPageIndex();
                    lbl_Message.Text = "No Records!";
                    gv_Data.Visible = false;

                }
                else
                {
                    if (dt.Columns.Contains("sno") == false)
                    {
                        dt.Columns.Add("sno");
                        int sno = 1;
                        if (dt.Rows.Count >= 1)
                            dt.Rows[0]["sno"] = 1;
                        if (dt.Rows.Count >= 2)
                        {
                            for (int i = 1; i < dt.Rows.Count; i++)
                            {   //Nasir 6419 08/28/2009 serial no reset on court change
                                if (dt.Rows[i]["CourtID"].ToString() != dt.Rows[i - 1]["CourtID"].ToString())
                                {
                                    sno = 0;
                                }
                                if (dt.Rows[i - 1]["ticketid_pk"].ToString() != dt.Rows[i]["ticketid_pk"].ToString())
                                {
                                    dt.Rows[i]["sno"] = ++sno;
                                }
                                else
                                    dt.Rows[i]["sno"] = ".";
                            }
                        }
                    }
                }

                int courtid = 0;
                int rowcount = dt.Rows.Count;
                for (int i = 0; i < rowcount; i++)
                {
                    DataRow drnew = dt.NewRow();
                    drnew["Firstname"] = string.Empty;
                    if (courtid != Convert.ToInt32(dt.Rows[i]["CourtID"]))
                    {
                        dt.Rows.InsertAt(drnew, i);
                        dt.Rows[i]["Firstname"] = Convert.ToString(dt.Rows[i + 1]["CourtName"]);
                        i = i + 1;
                        rowcount = rowcount + 1;
                        courtid = Convert.ToInt32(dt.Rows[i]["CourtID"]);
                    }
                }

                gv_Data.Visible = true;
                gv_Data.DataSource = dt;
                gv_Data.DataBind();
                Pagingctrl.PageCount = gv_Data.PageCount;
                Pagingctrl.PageIndex = gv_Data.PageIndex;
                Pagingctrl.SetPageIndex();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        ///     Method when Page index changed
        /// </summary>
        void Pagingctrl_PageIndexChanged()
        {
            try
            {
                gv_Data.PageIndex = Pagingctrl.PageIndex - 1;
                GetRecords();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        ///     Method when page size will be changed
        /// </summary>
        /// <param name="pageSize"></param>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            try
            {
                if (pageSize > 0)
                {
                    gv_Data.PageIndex = 0;
                    gv_Data.PageSize = pageSize;
                    gv_Data.AllowPaging = true;
                }
                else
                {
                    gv_Data.AllowPaging = false;
                }
                GetRecords();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        #endregion
    }
}

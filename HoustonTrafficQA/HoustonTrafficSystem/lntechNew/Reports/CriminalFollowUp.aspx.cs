﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;

namespace HTP.Reports
{
    public partial class CriminalFollowUp : System.Web.UI.Page
    {
        #region Variables

        HccCriminalReport hccCrim = new HccCriminalReport();
        //clsLogger bugTracker = new clsLogger();
        clsCase cCase = new clsCase();
        DateTime date_from, date_to;
        DataView dv;

        #endregion

        #region Properties

        public SortDirection GridViewSortDirection
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Ascending;
                return (SortDirection)ViewState["sortDirection"];
            }
            set { ViewState["sortDirection"] = value; }

        }

        #endregion

        #region Events

        // Noufil 3589 05/14/2008 Get Criminal Reports
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (!IsPostBack)
                {
                    Session["DV_criminal"] = null;
                    ddl_Courts.DataSource = hccCrim.GetCriminalCOurt();
                    ddl_Courts.DataTextField = "Courtname";
                    //ddl_Courts.DataMember = "Courtname";
                    ddl_Courts.DataValueField = "courtid";
                    ddl_Courts.DataBind();
                    ddl_Courts.Items.Insert(0, new ListItem("All Criminal Courts", "-1"));
                    ddl_Courts.SelectedIndex = 0;
                    dtp_From.SelectedDate = DateTime.Today;
                    dtp_To.SelectedDate = DateTime.Today;
                }
                UpdateFollowUpInfo2.PageMethod += new HTP.WebControls.PageMethodHandler(UpdateFollowUpInfo2_PageMethod);
                Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        void UpdateFollowUpInfo2_PageMethod()
        {
            FillGrid();
            //throw new NotImplementedException();
        }

        void Pagingctrl_PageIndexChanged()
        {
            gv_Result.PageIndex = Pagingctrl.PageIndex - 1;
            FillGrid();
        }

        protected void gv_Result_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_Result.PageIndex = e.NewPageIndex;
                FillGrid();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void Button_Submit_Click(object sender, EventArgs e)
        {
            try
            {
                date_from = dtp_From.SelectedDate;
                date_to = dtp_To.SelectedDate;
                FillGrid();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                string sortExpression = e.SortExpression;

                if (GridViewSortDirection == SortDirection.Ascending)
                {
                    GridViewSortDirection = SortDirection.Descending;
                    SortGridView(sortExpression, "DESC");
                }
                else
                {
                    GridViewSortDirection = SortDirection.Ascending;
                    SortGridView(sortExpression, "ASC");
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        private void SortGridView(string sortExpression, string direction)
        {
            try
            {
                dv = (DataView)Session["DV_criminal"];
                dv.Sort = sortExpression + " " + direction;
                Session["DV_criminal"] = dv;

                FillGrid();
                Pagingctrl.PageCount = gv_Result.PageCount;
                Pagingctrl.PageIndex = gv_Result.PageIndex;

                ViewState["sortExpression"] = sortExpression;
                Session["SortDirection"] = direction;
                Session["SortExpression"] = sortExpression;
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        protected void gv_Result_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ((LinkButton)e.Row.FindControl("img_Add")).CommandArgument = e.Row.RowIndex.ToString();
                    pnlFollowup.Style["display"] = "none";
                }
            }

            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void gv_Result_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "btnclick")
            {

                int RowID = Convert.ToInt32(e.CommandArgument);
                string firstname = (((HiddenField)gv_Result.Rows[RowID].Cells[13].FindControl("hf_fname")).Value);
                string causeno = (((HiddenField)gv_Result.Rows[RowID].Cells[13].FindControl("hf_causeno")).Value);
                string ticketno = (((HiddenField)gv_Result.Rows[RowID].Cells[13].FindControl("hf_ticketno")).Value);
                string lastname = (((HiddenField)gv_Result.Rows[RowID].Cells[13].FindControl("hf_lname")).Value);
                string courtname = (((HiddenField)gv_Result.Rows[RowID].Cells[13].FindControl("hf_loc")).Value);
                string court = (((HiddenField)gv_Result.Rows[RowID].Cells[13].FindControl("hf_courtid")).Value);
                string followupDate = (((Label)gv_Result.Rows[RowID].Cells[13].FindControl("lbl_FollowUpD")).Text);
                cCase.TicketID = Convert.ToInt32(ticketno);
                string comm = cCase.GetGeneralCommentsByTicketId();
                string follow = string.Empty;
                //ozair 4442 07/22/2008 ALR Courts (Houston,Fort Bend, Conroe)
                int Days = (court == "3047" || court == "3048" || court == "3070") ? 7 : 14;
                follow = DateTime.Today.AddDays(Days).ToShortDateString();
                UpdateFollowUpInfo2.Freezecalender = true;
                UpdateFollowUpInfo2.binddate(DateTime.Today.AddDays(Days), DateTime.Today, firstname, lastname, ticketno, causeno, comm, ModalPopupExtender1.ClientID, court, courtname, followupDate);
                ModalPopupExtender1.Show();
                Pagingctrl.Visible = true;

            }

        }

        #endregion

        #region Methods

        private void FillGrid()
        {
            try
            {
                //ozair 4412 07/14/2008 implementing show all option   
                int courtid = -1;
                if (!chk_ShowAll.Checked)
                {
                    courtid = int.Parse(ddl_Courts.SelectedValue);
                }
                DataTable dt = hccCrim.Get_CriminalFollowUp(courtid, dtp_From.SelectedDate, dtp_To.SelectedDate, chk_ShowAll.Checked);
                //end ozair 4412
                if (dt.Rows.Count > 0)
                {
                    gv_Result.Visible = true;
                    lbl_Message.Text = "";
                    if (dt.Columns.Contains("sno") == false)
                    {
                        dt.Columns.Add("sno");
                        int sno = 1;
                        if (dt.Rows.Count >= 1)
                            dt.Rows[0]["sno"] = 1;
                        if (dt.Rows.Count >= 2)
                        {
                            for (int i = 1; i < dt.Rows.Count; i++)
                            {
                                if (dt.Rows[i - 1]["ticketid_pk"].ToString() != dt.Rows[i]["ticketid_pk"].ToString())
                                {
                                    dt.Rows[i]["sno"] = ++sno;
                                }
                            }
                        }
                    }
                    if (Session["DV_criminal"] == null)
                    {
                        dv = new DataView(dt);
                        Session["DV_criminal"] = dv;
                    }
                    else
                    {
                        dv = (DataView)Session["DV_criminal"];
                        string SortExp = dv.Sort;
                        dv = new DataView(dt);
                        dv.Sort = SortExp;
                    }
                    gv_Result.DataSource = dv;
                    gv_Result.DataBind();
                    Pagingctrl.PageCount = gv_Result.PageCount;
                    Pagingctrl.PageIndex = gv_Result.PageIndex;
                    Pagingctrl.SetPageIndex();
                }
                else
                {
                    lbl_Message.Text = "No Records Found";
                    gv_Result.Visible = false;
                }

            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);

            }

        }

        #endregion
    }
}

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HMCFTAFollowUp.aspx.cs" Inherits="HTP.Reports.HMCFTAFollowUp" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/ShowSetting.ascx" TagName="ShowSetting" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc4" %>
<%@ Register Src="../WebControls/UpdateFollowUpInfo.ascx" TagName="UpdateFollowUpInfo" TagPrefix="uc5" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>HMC FTA Follow Up</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <script language="javascript">
    //Waqas Javed 5697 03/26/2009
    function showhide(ele,caller) 
    {
        var srcElement = document.getElementById(ele);
        if(srcElement != null) {
        if(srcElement.style.display == "block") {
           caller.innerHTML = "Show Settings";
           srcElement.style.display= 'none';
        }
        else {
           caller.innerHTML = "Hide Settings";
           srcElement.style.display='block';
        }
        return false;
      }
    }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <aspnew:ScriptManager ID="ScriptManager1" runat="server"  AsyncPostBackTimeout = "0" />
            <table cellspacing="0" cellpadding="0" width="780" align="center" border="0">
                <tr>
                    <td align="center">
                        
                        <uc1:ActiveMenu ID="ActiveMenu1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="center" background="../images/separator_repeat.gif" height="11">
                    </td>
                </tr>
                <tr>
                    <td background="../Images/subhead_bg.gif" class="clssubhead" align="left" style="height: 34px">
                        <table width="100%">
                            <tr>                                
                                <td align="left" class="clssubhead">
                                    HMC FTA Follow Up Report
                                </td>
                                <td align="right" class="clssubhead">
                                <aspnew:UpdatePanel ID="updatepnlpaging" runat="server" RenderMode="Inline">
                                     <ContentTemplate>
                                    <uc4:PagingControl ID="Pagingctrl" runat="server" />
                                    </ContentTemplate>
                                    </aspnew:UpdatePanel>
                                </td>
                                
                            </tr>
                        </table>
                        
                        </td>                       
                </tr>
                
                <tr>
                    <td align="center" background="../images/separator_repeat.gif" height="11">
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="lbl_Message" runat="server" CssClass="label" ForeColor="Red" Text="Label"></asp:Label></td>
                </tr>
                <tr>
                <td align="center"><aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlResult">
                                    <ProgressTemplate>
                                        <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl1" runat="server" Text=" Please Wait ......"
                                            CssClass="clsLabel"></asp:Label>
                                    </ProgressTemplate>
                                </aspnew:UpdateProgress>
                                 <aspnew:UpdateProgress ID="updateprogress3" runat="server" AssociatedUpdatePanelID="updatepnlpaging">
                                    <ProgressTemplate>
                                        <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lblUp" runat="server" Text=" Please Wait ......"
                                            CssClass="clsLabel"></asp:Label>
                                    </ProgressTemplate>
                                </aspnew:UpdateProgress></td>
                
                </tr>
                <tr>
                    <td width="100%">
                                         
                        <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline">
                            <ContentTemplate>
                              <asp:GridView ID="gv_Data" runat="server" AutoGenerateColumns="False" CssClass="clsLeftPaddingTable"
                                Width="100%" onpageindexchanging="gv_Data_PageIndexChanging" 
                                onrowcommand="gv_Data_RowCommand" onrowdatabound="gv_Data_RowDataBound" 
                                AllowPaging="True" AllowSorting="True" 
                                PageSize="20" onsorting="gv_Data_Sorting">
                                <Columns>
                                    <asp:TemplateField HeaderText="S#">
                                        <ItemTemplate> 
                                            <asp:HyperLink ID="hlnk_SNo" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                Text='<%# DataBinder.Eval(Container, "DataItem.SNo") %>'></asp:HyperLink>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="clssubhead" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>FTA Cause Number</u>" SortExpression = "FTACauseNumber">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_CauseNumber" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.FTACauseNumber") %>'></asp:Label>
                                            <asp:HiddenField ID="hf_TicketID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.TicketID_PK") %>' />
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="clssubhead" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>Ticket Number</u>" SortExpression = "TicketNumber">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_TicketNumber" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.TicketNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>First Name</u>" SortExpression="FirstName" >
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_FirstName" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.FirstName") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle CssClass="clssubhead" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>Last Name</u>" SortExpression="lastname" >
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_LastName" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.LastName") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle CssClass="clssubhead" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>DOB</u>" SortExpression = "DOBSort">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_DOB" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.DOBSort","{0:MM/dd/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle CssClass="clssubhead" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>X-Ref</u>" SortExpression = "XRef">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_XRef" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.XRef") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle CssClass="clssubhead" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>Status</u>" SortExpression = "Status">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_Status" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.Status") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle CssClass="clssubhead" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>Court Date</u>" SortExpression = "CourtDateSort">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_CourtDate" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDateSort","{0:MM/dd/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle CssClass="clssubhead" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>Court Time</u>" SortExpression = "CourtDateSort">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_CourtTime" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.CourtTime") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle CssClass="clssubhead" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>Insert Date</u>" SortExpression = "FTAIssuedDateSort">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_FTAIssuedDate" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.FTAIssuedDateSort","{0:MM/dd/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle CssClass="clssubhead" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>Follow Up Date</u>" SortExpression = "HMCFTAFollowUpDateSort">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_followUp" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.HMCFTAFollowUpDate") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle CssClass="clssubhead" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:ImageButton ID="img_Add" runat="server" CommandName="btnclick" ImageUrl="../Images/add.gif" />
                                            <asp:HiddenField ID="hf_fname" runat="server" Value='<%#Eval("FIRSTNAME") %>' />
                                            <asp:HiddenField ID="hf_lname" runat="server" Value='<%#Eval("LASTNAME") %>' />
                                            <asp:HiddenField ID="hf_ticketid_pk" runat="server" Value='<%#Eval("ticketid_pk") %>' />
                                            <asp:HiddenField ID="hf_causeno" runat="server" Value='<%#Eval("FTACauseNumber") %>' />
                                            <asp:HiddenField ID="hf_courtid" runat="server" Value='<%#Eval("Courtid") %>' />
                                            <asp:HiddenField ID="hf_FollowUpDate" runat="server" Value='<%#Eval("HMCFTAFollowUpDate") %>' /> 
                                            <asp:HiddenField ID="hf_ticketnumber" runat="server" Value='<%#Eval("TicketNumber") %>' />                                       
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle HorizontalAlign="Center" />
                                        <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                            FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                            </asp:GridView>
                            </ContentTemplate>
                        </aspnew:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td align="center" background="../images/separator_repeat.gif" height="11">
                    </td>
                </tr>
                <tr>
                <td align="center"> <aspnew:UpdateProgress ID="updateprogress2" runat="server" AssociatedUpdatePanelID="UpdatePanelcrtl">
                        <ProgressTemplate>
                            <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl2" runat="server" Text=" Please Wait ......"
                                CssClass="clsLabel"></asp:Label>
                        </ProgressTemplate>
                    </aspnew:UpdateProgress></td></tr>
                <tr>
                <td>
                   <aspnew:UpdatePanel ID="UpdatePanelcrtl" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="pnlFollowup" runat="server">
                                <uc5:UpdateFollowUpInfo ID="UpdateFollowUpInfo" runat="server" Title="LOR Follow-up" />
                            </asp:Panel>
                        </ContentTemplate>
                    </aspnew:UpdatePanel>
                    <aspnew:UpdatePanel ID="UpdatePanelmodal" runat="server">
                        <ContentTemplate>
                            <ajaxToolkit:ModalPopupExtender ID="mpeTrafficwaiting" runat="server" BackgroundCssClass="modalBackground"
                                PopupControlID="pnlFollowup" TargetControlID="btn">
                            </ajaxToolkit:ModalPopupExtender>
                            <asp:Button ID="btn" runat="server" Style="display: none;" />
                        </ContentTemplate>
                    </aspnew:UpdatePanel>
                </td>
                </tr>
                <tr>
                    <td>
                        <uc2:Footer ID="Footer1" runat="server" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
    <script language="javascript" type="text/javascript">
        document.getElementById("UpdateFollowUpInfo_cal_followupdate_div").style.zIndex = 10003;
        document.getElementById("UpdateFollowUpInfo_cal_followupdate_monthYear").style.zIndex = 10004;
    </script>
</body>
</html>

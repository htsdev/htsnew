﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CriminalAlertsReport.aspx.cs"
    Inherits="lntechNew.Reports.CriminalAlertsReport" %>

<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc12" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Criminal Alerts Report</title>

    <script src="../Scripts/Dates.js" type="text/javascript"></script>

    <script type="text/javascript" language="javascript"></script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <aspnew:ScriptManager ID="ScriptManager1" runat="server" />
        <table cellspacing="0" cellpadding="0" width="900px" align="center" border="0">
            <tr>
                <td>
                    <uc12:ActiveMenu ID="ActiveMenu1" runat="server" />
                </td>
            </tr>
            <tr>
                <td style="width: 100%" background="../Images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td>
                    <table border="0" width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="width: 100px">
                                <span class="clssubhead">Search Criteria :</span>
                            </td>
                            <td align="left" style="width: 180px">
                                <span class="clsLabel">Upload Date :&nbsp;</span>
                                <ew:CalendarPopup Visible="true" ID="cal_UploadDateFilter" runat="server" AllowArbitraryText="False"
                                    CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Culture="(Default)" EnableHideDropDown="True"
                                    Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif" Nullable="False"
                                    PadSingleDigits="True" ShowGoToToday="True" Text=" " ToolTip="Date" UpperBoundDate="9999-12-29"
                                    Width="65px">
                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TextboxLabelStyle CssClass="clstextarea" />
                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Gray" />
                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                </ew:CalendarPopup>
                            </td>
                            <td align="left" style="width: 280px; display: inline;">
                                <span class="clsLabel">Case Status :&nbsp;</span>
                                <asp:DropDownList ID="ddlCaseStatus" runat="server" CssClass="clsInputCombo" Width="115px">
                                    <%--<asp:ListItem ="0">Client and Quotes</asp:ListItem>--%>
                                    <asp:ListItem Value="1">Client</asp:ListItem>
                                    <%--<asp:ListItem Value="2">Quotes</asp:ListItem>--%>
                                </asp:DropDownList>
                            </td>
                            <td align="left">
                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="clsbutton" OnClick="btnSubmit_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td background="../Images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <!--LastName, FirstName, DOB-->
            <tr>
                <td background="../Images/subhead_bg.gif" class="clssubhead" align="left" style="height: 34px">
                    <table width="100%">
                        <tr>
                            <td align="left" class="clssubhead">
                                Last Name, First Name & DOB
                            </td>
                            <td align="right" class="clssubhead">
                                <aspnew:UpdatePanel ID="updatepnlpaging1" runat="server" RenderMode="Inline">
                                    <ContentTemplate>
                                        <uc3:PagingControl ID="Pagingctrl1" runat="server" />
                                    </ContentTemplate>
                                </aspnew:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table id="TableGrid1" bgcolor="white" border="0" cellpadding="0" cellspacing="0"
                        width="100%">
                        <tr>
                            <td background="../../images/separator_repeat.gif" colspan="5" height="11" style="height: 11px"
                                width="780">
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="width: 100%">
                                <asp:Label ID="lblMessage1" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                        <!-- Table Grid1 Start-->
                        <tr>
                            <td align="center" colspan="2" valign="top">
                                <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlResult1">
                                    <ProgressTemplate>
                                        <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl1" runat="server" Text="Please Wait ......"
                                            CssClass="clsLabel"></asp:Label>
                                    </ProgressTemplate>
                                </aspnew:UpdateProgress>
                                <aspnew:UpdatePanel ID="upnlResult1" runat="server" RenderMode="Inline">
                                    <ContentTemplate>
                                        <asp:GridView ID="gv_Records1" runat="server" AutoGenerateColumns="False" Width="100%"
                                            CssClass="clsLeftPaddingTable" AllowPaging="True" PageSize="20" OnPageIndexChanging="gv_Records1_PageIndexChanging">
                                            <Columns>
                                                <asp:TemplateField HeaderText="S#">
                                                    <ItemTemplate>
                                                        &nbsp;<asp:HyperLink ID="HLTicketno1" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                            Text='<%# DataBinder.Eval(Container, "DataItem.sno") %>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                </asp:TemplateField>                                                
                                                <asp:BoundField HeaderText="Last Name" DataField="LastName" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" />
                                                    <ItemStyle CssClass="GridItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="First Name" DataField="FirstName" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" />
                                                    <ItemStyle CssClass="GridItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="DOB" DataField="DOB" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" />
                                                    <ItemStyle CssClass="GridItemStyle" />
                                                </asp:BoundField>                                                
                                                <asp:BoundField HeaderText="Zip" DataField="Zip" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" />
                                                    <ItemStyle CssClass="GridItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Case Type" DataField="CaseTypeName" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" />
                                                    <ItemStyle CssClass="GridItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Status" DataField="ClientStatus" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" />
                                                    <ItemStyle CssClass="GridItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="CRT D" DataField="CourtDate" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" />
                                                    <ItemStyle CssClass="GridItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Last Name" DataField="lastName_HCC" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" BackColor="#fbd4b4" />
                                                    <ItemStyle CssClass="GridItemStyle" BackColor="#fbd4b4" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="First Name" DataField="firstName_HCC" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" BackColor="#fbd4b4" />
                                                    <ItemStyle CssClass="GridItemStyle" BackColor="#fbd4b4" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="DOB" DataField="DOB_HCC" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" BackColor="#fbd4b4" />
                                                    <ItemStyle CssClass="GridItemStyle" BackColor="#fbd4b4" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Zip" DataField="Zip_HCC" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" BackColor="#fbd4b4" />
                                                    <ItemStyle CssClass="GridItemStyle" BackColor="#fbd4b4" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="HCDC Case No" DataField="HCDCCaseNo_HCC" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" BackColor="#fbd4b4" />
                                                    <ItemStyle CssClass="GridItemStyle" BackColor="#fbd4b4" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Criminal Violation" DataField="VDesc_HCC" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" BackColor="#fbd4b4" />
                                                    <ItemStyle CssClass="GridItemStyle" BackColor="#fbd4b4" />
                                                </asp:BoundField>
                                                <%--<asp:BoundField HeaderText="Court Loc." DataField="CourtLocation" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" BackColor="#fbd4b4" />
                                                    <ItemStyle CssClass="GridItemStyle" BackColor="#fbd4b4" />
                                                </asp:BoundField>--%>
                                            </Columns>
                                            <PagerStyle HorizontalAlign="Center" />
                                            <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                            <EmptyDataTemplate>
                                                <asp:Label ID="Label1" runat="server" Text="No Records Found" CssClass="clsLabel"
                                                    ForeColor="Red"></asp:Label>
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </ContentTemplate>
                                </aspnew:UpdatePanel>
                            </td>
                        </tr>
                        <!-- Table Grid1 End-->
                        <tr>
                            <td background="../../images/separator_repeat.gif" colspan="5" style="height: 11px;
                                width: 780">
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <!--LastName, DOB & ZIP -->
            <tr>
                <td background="../Images/subhead_bg.gif" class="clssubhead" align="left" style="height: 34px">
                    <table width="100%">
                        <tr>
                            <td align="left" class="clssubhead">
                                Last Name, DOB & Zip
                            </td>
                            <td align="right" class="clssubhead">
                                <aspnew:UpdatePanel ID="updatepnlpaging3" runat="server" RenderMode="Inline">
                                    <ContentTemplate>
                                        <uc3:PagingControl ID="Pagingctrl3" runat="server" />
                                    </ContentTemplate>
                                </aspnew:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table id="TableGrid3" bgcolor="white" border="0" cellpadding="0" cellspacing="0"
                        width="100%">
                        <tr>
                            <td background="../../images/separator_repeat.gif" colspan="5" height="11" style="height: 11px"
                                width="780">
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="width: 100%">
                                <asp:Label ID="lblMessage3" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                        <!-- Table Grid1 Start-->
                        <tr>
                            <td align="center" colspan="2" valign="top">
                                <aspnew:UpdateProgress ID="updateprogress3" runat="server" AssociatedUpdatePanelID="upnlResult3">
                                    <ProgressTemplate>
                                        <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl3" runat="server" Text="Please Wait ......"
                                            CssClass="clsLabel"></asp:Label>
                                    </ProgressTemplate>
                                </aspnew:UpdateProgress>
                                <aspnew:UpdatePanel ID="upnlResult3" runat="server" RenderMode="Inline">
                                    <ContentTemplate>
                                        <asp:GridView ID="gv_Records3" runat="server" AutoGenerateColumns="False" Width="100%"
                                            CssClass="clsLeftPaddingTable" AllowPaging="True" PageSize="20" OnPageIndexChanging="gv_Records3_PageIndexChanging">
                                            <Columns>
                                                <asp:TemplateField HeaderText="S#">
                                                    <ItemTemplate>
                                                        &nbsp;<asp:HyperLink ID="HLTicketno3" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                            Text='<%# DataBinder.Eval(Container, "DataItem.sno") %>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                </asp:TemplateField>                                                
                                                <asp:BoundField HeaderText="Last Name" DataField="LastName" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" />
                                                    <ItemStyle CssClass="GridItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="First Name" DataField="FirstName" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" />
                                                    <ItemStyle CssClass="GridItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="DOB" DataField="DOB" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" />
                                                    <ItemStyle CssClass="GridItemStyle" />
                                                </asp:BoundField>                                                
                                                <asp:BoundField HeaderText="Zip" DataField="Zip" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" />
                                                    <ItemStyle CssClass="GridItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Case Type" DataField="CaseTypeName" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" />
                                                    <ItemStyle CssClass="GridItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Status" DataField="ClientStatus" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" />
                                                    <ItemStyle CssClass="GridItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="CRT D" DataField="CourtDate" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" />
                                                    <ItemStyle CssClass="GridItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Last Name" DataField="lastName_HCC" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" BackColor="#fbd4b4" />
                                                    <ItemStyle CssClass="GridItemStyle" BackColor="#fbd4b4" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="First Name" DataField="firstName_HCC" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" BackColor="#fbd4b4" />
                                                    <ItemStyle CssClass="GridItemStyle" BackColor="#fbd4b4" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="DOB" DataField="DOB_HCC" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" BackColor="#fbd4b4" />
                                                    <ItemStyle CssClass="GridItemStyle" BackColor="#fbd4b4" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Zip" DataField="Zip_HCC" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" BackColor="#fbd4b4" />
                                                    <ItemStyle CssClass="GridItemStyle" BackColor="#fbd4b4" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="HCDC Case No" DataField="HCDCCaseNo_HCC" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" BackColor="#fbd4b4" />
                                                    <ItemStyle CssClass="GridItemStyle" BackColor="#fbd4b4" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Criminal Violation" DataField="VDesc_HCC" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" BackColor="#fbd4b4" />
                                                    <ItemStyle CssClass="GridItemStyle" BackColor="#fbd4b4" />
                                                </asp:BoundField>
                                                <%--<asp:BoundField HeaderText="Court Loc." DataField="CourtLocation" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" BackColor="#fbd4b4" />
                                                    <ItemStyle CssClass="GridItemStyle" BackColor="#fbd4b4" />
                                                </asp:BoundField>--%>
                                            </Columns>
                                            <PagerStyle HorizontalAlign="Center" />
                                            <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                            <EmptyDataTemplate>
                                                <asp:Label ID="Label3" runat="server" Text="No Records Found" CssClass="clsLabel"
                                                    ForeColor="Red"></asp:Label>
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </ContentTemplate>
                                </aspnew:UpdatePanel>
                            </td>
                        </tr>
                        <!-- Table Grid1 End-->
                        <tr>
                            <td background="../../images/separator_repeat.gif" colspan="5" style="height: 11px;
                                width: 780">
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <!--LastName, DOB -->
            <tr>
                <td background="../Images/subhead_bg.gif" class="clssubhead" align="left" style="height: 34px">
                    <table width="100%">
                        <tr>
                            <td align="left" class="clssubhead">
                                Last Name & DOB
                            </td>
                            <td align="right" class="clssubhead">
                                <aspnew:UpdatePanel ID="updatepnlpaging2" runat="server" RenderMode="Inline">
                                    <ContentTemplate>
                                        <uc3:PagingControl ID="Pagingctrl2" runat="server" />
                                    </ContentTemplate>
                                </aspnew:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table id="TableGrid2" bgcolor="white" border="0" cellpadding="0" cellspacing="0"
                        width="100%">
                        <tr>
                            <td background="../../images/separator_repeat.gif" colspan="5" height="11" style="height: 11px"
                                width="780">
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="width: 100%">
                                <asp:Label ID="lblMessage2" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                        <!-- Table Grid1 Start-->
                        <tr>
                            <td align="center" colspan="2" valign="top">
                                <aspnew:UpdateProgress ID="updateprogress2" runat="server" AssociatedUpdatePanelID="upnlResult2">
                                    <ProgressTemplate>
                                        <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl2" runat="server" Text="Please Wait ......"
                                            CssClass="clsLabel"></asp:Label>
                                    </ProgressTemplate>
                                </aspnew:UpdateProgress>
                                <aspnew:UpdatePanel ID="upnlResult2" runat="server" RenderMode="Inline">
                                    <ContentTemplate>
                                        <asp:GridView ID="gv_Records2" runat="server" AutoGenerateColumns="False" Width="100%"
                                            CssClass="clsLeftPaddingTable" AllowPaging="True" PageSize="20" OnPageIndexChanging="gv_Records2_PageIndexChanging">
                                            <Columns>
                                                <asp:TemplateField HeaderText="S#">
                                                    <ItemTemplate>
                                                        &nbsp;<asp:HyperLink ID="HLTicketno2" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                            Text='<%# DataBinder.Eval(Container, "DataItem.sno") %>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                </asp:TemplateField>                                                
                                                <asp:BoundField HeaderText="Last Name" DataField="LastName" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" />
                                                    <ItemStyle CssClass="GridItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="First Name" DataField="FirstName" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" />
                                                    <ItemStyle CssClass="GridItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="DOB" DataField="DOB" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" />
                                                    <ItemStyle CssClass="GridItemStyle" />
                                                </asp:BoundField>                                                
                                                <asp:BoundField HeaderText="Zip" DataField="Zip" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" />
                                                    <ItemStyle CssClass="GridItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Case Type" DataField="CaseTypeName" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" />
                                                    <ItemStyle CssClass="GridItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Status" DataField="ClientStatus" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" />
                                                    <ItemStyle CssClass="GridItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="CRT D" DataField="CourtDate" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" />
                                                    <ItemStyle CssClass="GridItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Last Name" DataField="lastName_HCC" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" BackColor="#fbd4b4" />
                                                    <ItemStyle CssClass="GridItemStyle" BackColor="#fbd4b4" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="First Name" DataField="firstName_HCC" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" BackColor="#fbd4b4" />
                                                    <ItemStyle CssClass="GridItemStyle" BackColor="#fbd4b4" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="DOB" DataField="DOB_HCC" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" BackColor="#fbd4b4" />
                                                    <ItemStyle CssClass="GridItemStyle" BackColor="#fbd4b4" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Zip" DataField="Zip_HCC" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" BackColor="#fbd4b4" />
                                                    <ItemStyle CssClass="GridItemStyle" BackColor="#fbd4b4" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="HCDC Case No" DataField="HCDCCaseNo_HCC" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" BackColor="#fbd4b4" />
                                                    <ItemStyle CssClass="GridItemStyle" BackColor="#fbd4b4" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Criminal Violation" DataField="VDesc_HCC" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" BackColor="#fbd4b4" />
                                                    <ItemStyle CssClass="GridItemStyle" BackColor="#fbd4b4" />
                                                </asp:BoundField>
                                                <%--<asp:BoundField HeaderText="Court Loc." DataField="CourtLocation" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" BackColor="#fbd4b4" />
                                                    <ItemStyle CssClass="GridItemStyle" BackColor="#fbd4b4" />
                                                </asp:BoundField>--%>
                                            </Columns>
                                            <PagerStyle HorizontalAlign="Center" />
                                            <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                            <EmptyDataTemplate>
                                                <asp:Label ID="Label2" runat="server" Text="No Records Found" CssClass="clsLabel"
                                                    ForeColor="Red"></asp:Label>
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </ContentTemplate>
                                </aspnew:UpdatePanel>
                            </td>
                        </tr>
                        <!-- Table Grid1 End-->
                        <tr>
                            <td background="../../images/separator_repeat.gif" colspan="5" style="height: 11px;
                                width: 780">
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <!--Last Name, ZIP -->
            <tr style="display: block;">
                <td background="../Images/subhead_bg.gif" class="clssubhead" align="left" style="height: 34px">
                    <table width="100%">
                        <tr>
                            <td align="left" class="clssubhead">
                                Last Name, Street Address & Zip
                            </td>
                            <td align="right" class="clssubhead">
                                <aspnew:UpdatePanel ID="updatepnlpaging4" runat="server" RenderMode="Inline">
                                    <ContentTemplate>
                                        <uc3:PagingControl ID="Pagingctrl4" runat="server" />
                                    </ContentTemplate>
                                </aspnew:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="display: block;">
                <td>
                    <table id="TableGrid4" bgcolor="white" border="0" cellpadding="0" cellspacing="0"
                        width="100%">
                        <tr>
                            <td background="../../images/separator_repeat.gif" colspan="5" height="11" style="height: 11px"
                                width="780">
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="width: 100%">
                                <asp:Label ID="lblMessage4" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                        <!-- Table Grid1 Start-->
                        <tr>
                            <td align="center" colspan="2" valign="top">
                                <aspnew:UpdateProgress ID="updateprogress4" runat="server" AssociatedUpdatePanelID="upnlResult4">
                                    <ProgressTemplate>
                                        <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl4" runat="server" Text="Please Wait ......"
                                            CssClass="clsLabel"></asp:Label>
                                    </ProgressTemplate>
                                </aspnew:UpdateProgress>
                                <aspnew:UpdatePanel ID="upnlResult4" runat="server" RenderMode="Inline">
                                    <ContentTemplate>
                                        <asp:GridView ID="gv_Records4" runat="server" AutoGenerateColumns="False" Width="100%"
                                            CssClass="clsLeftPaddingTable" AllowPaging="True" PageSize="20" OnPageIndexChanging="gv_Records4_PageIndexChanging">
                                            <Columns>
                                                <asp:TemplateField HeaderText="S#">
                                                    <ItemTemplate>
                                                        &nbsp;<asp:HyperLink ID="HLTicketno2" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                            Text='<%# DataBinder.Eval(Container, "DataItem.sno") %>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                </asp:TemplateField>                                                
                                                <asp:BoundField HeaderText="Last Name" DataField="LastName" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" />
                                                    <ItemStyle CssClass="GridItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="First Name" DataField="FirstName" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" />
                                                    <ItemStyle CssClass="GridItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="DOB" DataField="DOB" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" />
                                                    <ItemStyle CssClass="GridItemStyle" />
                                                </asp:BoundField>    
                                                <asp:BoundField HeaderText="Address" DataField="Address" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" />
                                                    <ItemStyle CssClass="GridItemStyle" />
                                                </asp:BoundField>                                            
                                                <asp:BoundField HeaderText="Zip" DataField="Zip" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" />
                                                    <ItemStyle CssClass="GridItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Case Type" DataField="CaseTypeName" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" />
                                                    <ItemStyle CssClass="GridItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Status" DataField="ClientStatus" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" />
                                                    <ItemStyle CssClass="GridItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="CRT D" DataField="CourtDate" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" />
                                                    <ItemStyle CssClass="GridItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Last Name" DataField="lastName_HCC" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" BackColor="#fbd4b4" />
                                                    <ItemStyle CssClass="GridItemStyle" BackColor="#fbd4b4" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="First Name" DataField="firstName_HCC" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" BackColor="#fbd4b4" />
                                                    <ItemStyle CssClass="GridItemStyle" BackColor="#fbd4b4" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="DOB" DataField="DOB_HCC" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" BackColor="#fbd4b4" />
                                                    <ItemStyle CssClass="GridItemStyle" BackColor="#fbd4b4" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Address" DataField="Address_HCC" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" BackColor="#fbd4b4" />
                                                    <ItemStyle CssClass="GridItemStyle" BackColor="#fbd4b4" />
                                                </asp:BoundField> 
                                                <asp:BoundField HeaderText="Zip" DataField="Zip_HCC" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" BackColor="#fbd4b4" />
                                                    <ItemStyle CssClass="GridItemStyle" BackColor="#fbd4b4" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="HCDC Case No" DataField="HCDCCaseNo_HCC" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" BackColor="#fbd4b4" />
                                                    <ItemStyle CssClass="GridItemStyle" BackColor="#fbd4b4" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Criminal Violation" DataField="VDesc_HCC" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" BackColor="#fbd4b4" />
                                                    <ItemStyle CssClass="GridItemStyle" BackColor="#fbd4b4" />
                                                </asp:BoundField>
                                                <%--<asp:BoundField HeaderText="Court Loc." DataField="CourtLocation" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clssubhead">
                                                    <HeaderStyle CssClass="clssubhead" BackColor="#fbd4b4" />
                                                    <ItemStyle CssClass="GridItemStyle" BackColor="#fbd4b4" />
                                                </asp:BoundField>--%>
                                            </Columns>
                                            <PagerStyle HorizontalAlign="Center" />
                                            <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                            <EmptyDataTemplate>
                                                <asp:Label ID="Label2" runat="server" Text="No Records Found" CssClass="clsLabel"
                                                    ForeColor="Red"></asp:Label>
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </ContentTemplate>
                                </aspnew:UpdatePanel>
                            </td>
                        </tr>
                        <!-- Table Grid1 End-->
                        <tr>
                            <td background="../../images/separator_repeat.gif" colspan="5" style="height: 11px;
                                width: 780">
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td style="display: none;">
                            </td>
                            <td>
                                <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CriminalFollowUp.aspx.cs"
    Inherits="HTP.Reports.CriminalFollowUp" EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<%@ Register Src="../WebControls/UpdateFollowUpInfo.ascx" TagName="UpdateFollowUpInfo"
    TagPrefix="uc3" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Criminal Follow Up Report</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .modalBackground
        {
            background-color: Gray;            
            filter: alpha(opacity=70);
            opacity: 0.7;
        }
        .modalPopup
        {
            background-color: #FFD9D5;
            border-width: 3px;
            border-style: solid;
            border-color: Gray;
            padding: 3px;
            width: 250px;
            position: static;
            left: 200px;
            top: -65px;
        }
        A:link
        {
            Font-size: 8pt;
            font-family: Verdana,Arial, Helvetica, sans-serif;
            Color:  #4169E1;
            TEXT-DECORATION: none;
	        font-weight:bold;
        }

        A:Visited
        {
            Font-size: 8pt;
            font-family: Verdana,Arial, Helvetica, sans-serif;
            Color:  #4169E1;
            TEXT-DECORATION: none;
	        font-weight:bold;
        }
    </style>
    <!-- ozair 4389 07/12/2008 implemented date check -->
    <script language="javascript" type="text/javascript">
        function CheckDateValidation()
        {
            if (IsDatesEqualOrGrater(document.form1.dtp_To.value,'MM/dd/yyyy',document.form1.dtp_From.value,'MM/dd/yyyy')==false)
            {
			    alert("Please enter valid date, To-Date must be grater then or equal to From-Date");
				document.form1.dtp_To.focus(); 
				return false;
			}
			else
			{
			    return true;
			}
        }
    </script>
    <!-- end ozair 4389 -->
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <aspnew:ScriptManager ID="ScriptManager2" runat="server" />
        <table id="MainTable" cellspacing="0" cellpadding="0" align="center" border="0" style="width: 920px">
            <tr>
                <td>
                    <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="center" style="width: 850px">
                    <asp:Label ID="lbl_Message" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td background="../images/separator_repeat.gif" height="11" style="width: 850px">
                </td>
            </tr>
            <tr>
                <td style="height: 28px;">
                    <table>
                        <tr>
                            <td>
                                <span class="clssubhead">Court House :</span>
                            </td>
                            <td style="width: 218px;">
                                <asp:DropDownList ID="ddl_Courts" runat="server" CssClass="clsInputCombo" DataTextField="FirmAbbreviation"
                                    DataValueField="FirmID" Width="200px">
                                </asp:DropDownList>
                            </td>
                            <td style="width: 134px">
                                <!-- ozair 4389 07/12/2008 chnaged court date to Follow Up Date -->
                                <span class="clssubhead">Follow Up Date Range :</span>
                                <!-- end ozair 4389 -->
                            </td>
                            <td style="width: 137px;">
                                <span class="clsLabel">&nbsp;From :</span>
                                <ew:CalendarPopup ID="dtp_From" runat="server" AllowArbitraryText="False" CalendarLocation="Bottom"
                                    ControlDisplay="TextBoxImage" Culture="(Default)" EnableHideDropDown="True" Font-Names="Tahoma"
                                    Font-Size="8pt" ImageUrl="../images/calendar.gif" Nullable="False" PadSingleDigits="True"
                                    SelectedDate="2006-03-14" ShowGoToToday="True" Text=" " ToolTip="Follow Up Date From"
                                    UpperBoundDate="12/31/9999 23:59:00" Width="78px">
                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TextboxLabelStyle CssClass="clstextarea" />
                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Gray" />
                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                </ew:CalendarPopup>
                            </td>
                            <td style="width: 130px;">
                                <span class="clsLabel">&nbsp;To :</span>
                                <ew:CalendarPopup ID="dtp_To" runat="server" AllowArbitraryText="False" CalendarLocation="Bottom"
                                    ControlDisplay="TextBoxImage" Culture="(Default)" EnableHideDropDown="True" Font-Names="Tahoma"
                                    Font-Size="8pt" ImageUrl="../images/calendar.gif" Nullable="False" PadSingleDigits="True"
                                    SelectedDate="2006-03-14" ShowGoToToday="True" Text=" " ToolTip="Follow Up Date To"
                                    UpperBoundDate="12/31/9999 23:59:00" Width="78px">
                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TextboxLabelStyle CssClass="clstextarea" />
                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Gray" />
                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                </ew:CalendarPopup>
                            </td>
                            <td style="width: 80px;">
                                <asp:CheckBox ID="chk_ShowAll" runat="server" Text="Show All" 
                                    CssClass="clsLabel"></asp:CheckBox>
                            </td>
                            <td>
                                <asp:Button ID="Button_Submit" runat="server" CssClass="clsbutton" Text="Submit"
                                    OnClick="Button_Submit_Click" OnClientClick="return CheckDateValidation();" />
                            </td>                            
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td background="../images/separator_repeat.gif" height="11" style="width: 850px">
                </td>
            </tr>
            <tr>
                <td background="../Images/subhead_bg.gif" class="clssubhead" align="right" style="height: 32px">
                    <table style="width: 100%">
                        <tr>
                            <td>
                            </td>
                            <td style="text-align: right">
                                <aspnew:UpdatePanel ID="UpdatePanel3" runat="server">
                                    <ContentTemplate>
                                        <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                    </ContentTemplate>
                                </aspnew:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 920px">
                    <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlResult">
                        <ProgressTemplate>
                            <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl1" runat="server" Text="Please Wait ......"
                                CssClass="clsLabel"></asp:Label>
                        </ProgressTemplate>
                    </aspnew:UpdateProgress>
                    <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline">
                        <ContentTemplate>
                            <asp:GridView ID="gv_Result" runat="server" AutoGenerateColumns="False" CssClass="clsLeftPaddingTable"
                                Width="920px" AllowPaging="True" PageSize="30" AllowSorting="True" OnSorting="GridView1_Sorting"
                                OnPageIndexChanging="gv_Result_PageIndexChanging" OnRowDataBound="gv_Result_RowDataBound"
                                OnRowCommand="gv_Result_RowCommand">
                                <Columns>
                                    <asp:TemplateField HeaderText="<u>S#</u>" SortExpression="sno">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hfComments" Value="" runat="server" />
                                            <asp:HiddenField ID="hf_TicketId" runat="server" Value='<%# bind("ticketid_pk") %>' />
                                            <asp:HyperLink ID="hl_SNo" runat="server" NavigateUrl='<%# "../ClientInfo/ViolationFeeOld.aspx?CaseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk")+"&search="+DataBinder.Eval(Container, "DataItem.ACTIVEFLAG") %>'
                                                Text='<%# DataBinder.Eval(Container, "DataItem.sno") %>'></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>Cause No</u>" SortExpression="CAUSENUMBER" HeaderStyle-Width="60px">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblCauseNo" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.CAUSENUMBER") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>Ticket No</u>" SortExpression="TICKETNUMBER" HeaderStyle-Width="70px">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_TicketNo" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.TICKETNUMBER") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>Last</u>" SortExpression="LASTNAME">
                                        <HeaderStyle HorizontalAlign="Left" CssClass="clssubhead" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_LastName" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.LASTNAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>First</u>" SortExpression="FIRSTNAME">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_FirstName" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.FIRSTNAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>DOB</u>" SortExpression="DOB">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemStyle Width="60" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_DOB" runat="server" CssClass="clsLabel" Text='<%# bind("DOB","{0 : MM/dd/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>DL</u>" SortExpression="DL">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemStyle Width="60" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_DL" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.DL") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>Stat</u>" SortExpression="STAT">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemStyle Width="35" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_Stat" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.STAT") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>Loc</u>" SortExpression="LOC">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemStyle Width="50" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_LOC" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.LOC") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Setting Info">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemStyle Width="135" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_CourtDate" runat="server" CssClass="clsLabel" Text='<%# bind("COURTDATE","{0 : MM/dd/yy hh:mm tt}") %>'></asp:Label>
                                            <asp:Label ID="lbl_CourtNo" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.COURTNO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>Cov</u>" SortExpression="COV">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemStyle Width="25" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_Cov" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.COV") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>Flags</u>" SortExpression="bflag">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" Width="40px" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblFlags" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.bflag") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>Follow Up Date</u>" SortExpression="FollowUpDate">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" Width="100px" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_FollowUpD" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.FollowUpDate","{0:g}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton Text="<img src='../Images/add.gif' border='0'/>" ID="img_Add" CommandName="btnclick"
                                                CommandArgument='<%#Eval("ticketid_pk") %>' runat="server" />
                                            <asp:HiddenField ID="hf_fname" runat="server" Value='<%#Eval("FIRSTNAME") %>' />
                                            <asp:HiddenField ID="hf_lname" runat="server" Value='<%#Eval("LASTNAME") %>' />
                                            <asp:HiddenField ID="hf_ticketno" runat="server" Value='<%#Eval("ticketid_pk") %>' />
                                            <asp:HiddenField ID="hf_causeno" runat="server" Value='<%#Eval("CAUSENUMBER") %>' />
                                            <asp:HiddenField ID="hf_loc" runat="server" Value='<%#Eval("LOC") %>' />
                                            <asp:HiddenField ID="hf_courtid" runat="server" Value='<%#Eval("courtid") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle HorizontalAlign="Center" />
                                <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                    FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                            </asp:GridView>
                        </ContentTemplate>
                    </aspnew:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td width="100%" background="../Images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td>
                    <uc1:Footer ID="Footer1" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <aspnew:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="pnlFollowup" runat="server">
                                <uc3:UpdateFollowUpInfo ID="UpdateFollowUpInfo2" runat="server" Title="Criminal Follow Up Date" />
                            </asp:Panel>
                        </ContentTemplate>
                    </aspnew:UpdatePanel>
                </td>
            </tr>
        </table>
        <aspnew:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="Button1"
                    PopupControlID="pnlFollowup" BackgroundCssClass="modalBackground" HideDropDownList="false">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Button ID="Button1" runat="server" Text="Button" Style="display: none;" />
            </ContentTemplate>
        </aspnew:UpdatePanel>
    </div>
    </form>

    <script language="javascript" type="text/javascript">
        document.getElementById("UpdateFollowUpInfo2_cal_followupdate_div").style.zIndex = 10003;
        document.getElementById("UpdateFollowUpInfo2_cal_followupdate_monthYear").style.zIndex = 10004;
    </script>

</body>
</html>

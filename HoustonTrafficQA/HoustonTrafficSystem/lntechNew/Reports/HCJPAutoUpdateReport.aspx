﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HCJPAutoUpdateReport.aspx.cs"
    Inherits="HTP.Reports.HCJPAutoUpdateReport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>HCJP Auto Update Report</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table align="center" border="0" cellpadding="0" cellspacing="0" style="z-index: 101"
            width="820px">
            <tr>
                <td>
                    <uc1:ActiveMenu ID="ActiveMenu1" runat="server" />
                </td>
            </tr>
            <tr>
                <td width="100%" background="../Images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td background="../Images/subhead_bg.gif" class="clssubhead" align="right" style="height: 34px">
                    <uc3:PagingControl ID="Pagingctrl" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="lbl_Message" runat="server" CssClass="clslabel" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="gv_Records" runat="server" AutoGenerateColumns="False" Width="100%"
                        PageSize="30" CssClass="clsLeftPaddingTable" AllowSorting="True" AllowPaging="True"
                        OnPageIndexChanging="gv_Records_PageIndexChanging" CellPadding="3">
                        <Columns>
                            <asp:TemplateField HeaderText="S#">
                                <ItemTemplate>
                                    <asp:HyperLink ID="hlnk_SNo" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                        Text='<%# DataBinder.Eval(Container, "DataItem.SNo") %>'></asp:HyperLink>
                                </ItemTemplate>
                                <ControlStyle Width="10%" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="bondflag" HeaderText="Bond">
                                <ItemStyle CssClass="GridItemStyle" />
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="refcasenumber" HeaderText="Cause#">
                                <ItemStyle CssClass="GridItemStyle" />
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="lastname" HeaderText="Last Name">
                                <ItemStyle CssClass="GridItemStyle" />
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="firstname" HeaderText="First Name">
                                <ItemStyle CssClass="GridItemStyle" />
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="shortname" HeaderText="CRT">
                                <ItemStyle CssClass="GridItemStyle" Width="50px" />
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="autostatus" HeaderText="A">
                                <ItemStyle CssClass="GridItemStyle" />
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="autocourtdate" HeaderText="Auto Date">
                                <ItemStyle CssClass="GridItemStyle" Width="120px" />
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="verifiedcourtstatus" HeaderText="V">
                                <ItemStyle CssClass="GridItemStyle" />
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="verifiedcourtdate" HeaderText="Verified Date">
                                <ItemStyle CssClass="GridItemStyle" Width="120px" />
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="waittime" HeaderText="Days">
                                <ItemStyle CssClass="GridItemStyle" />
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="LORSortDate" HeaderText="LOR">
                                <ItemStyle CssClass="GridItemStyle" />
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                            </asp:BoundField>
                        </Columns>
                        <PagerStyle HorizontalAlign="Center" />
                        <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                            FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td width="100%" background="../Images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td>
                    <uc2:Footer ID="Footer1" runat="server" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ARRSummary.aspx.cs" Inherits="HTP.Reports.ARRSummary" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Arraignment Summary Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <table border="1" cellpadding="0" cellspacing="0" width="100%" height="100%">
        <tr>
            <td>
                <iframe src="frmARRSummary.aspx?Flag=<%=ViewState["Flag"] %>" width="100%" height="95%"
                    scrolling="auto" frameborder="4"></iframe>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>

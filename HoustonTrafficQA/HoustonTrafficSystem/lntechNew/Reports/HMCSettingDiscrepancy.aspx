<%@ Page Language="c#" AutoEventWireup="true" Inherits="HTP.Reports.HMCSettingDiscrepancy"
    CodeBehind="HMCSettingDiscrepancy.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%--Sabir Khan 4635 09/30/2008--%>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%--<%@ Register Src="../WebControls/UpdateViolation.ascx" TagName="UpdateViolation"
    TagPrefix="uc2" %>--%>
<%@ Register Src="../WebControls/ShowSetting.ascx" TagName="ShowSetting" TagPrefix="uc5" %>
<%@ Register Src="../WebControls/UpdateFollowUpInfo.ascx" TagName="UpdateFollowUpInfo"
    TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>HMC Setting Discrepancy Report</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />

    <script src="../Scripts/Dates.js" type="text/javascript"></script>

    <script src="../Scripts/jsDate.js" type="text/javascript"></script>

</head>
<body ms_positioning="GridLayout">
    <form id="Form1" method="post" runat="server">
    <aspnew:ScriptManager ID="ScriptManager1" runat="server">
        <Scripts>
            <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
        </Scripts>
    </aspnew:ScriptManager>
    <aspnew:UpdatePanel ID="pnl_main" runat="server">
        <ContentTemplate>
    <table id="TableMain" style="z-index: 101" cellspacing="0" cellpadding="0" width="780"
        align="center" border="0">
        <tbody>
            <tr>
                <td style="height: 14px" colspan="4">
                    <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                </td>
            </tr>
            <tr>
                <td>
                    <table id="TableSub" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td background="../images/separator_repeat.gif" height="11">
                            </td>
                        </tr>
                        <tr class="clsLeftPaddingTable">
                            <td>
                                <table width="100%">
                                    <tr>
                                        <td align="left" style="height: 25px">
                                            <uc5:ShowSetting ID="ShowSetting" lbl_TextBefore="Number of business days in future allowed for follow up date:"
                                                runat="server" Attribute_Key="Days" />
                                        </td>
                                        <td align="right" style="height: 25px">
                                            <asp:CheckBox ID="cb_ShowAll" runat="server" Text="Show All" CssClass="clssubhead"
                                                OnCheckedChanged="cb_ShowAll_CheckedChanged" AutoPostBack="true" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td background="../images/separator_repeat.gif" height="11">
                            </td>
                        </tr>
                        <tr align="right">
                            <%--<td   background="../Images/subhead_bg.gif" class="clssubhead" align="right" style="height: 34px" align="right"  ><asp:label id="lblCurrPage" runat="server" Width="83px" Font-Size="Smaller" CssClass="cmdlinks"
                                                ForeColor="#3366cc" Font-Bold="True" Height="8px">Current Page :</asp:label><asp:label id="lblPNo" runat="server" Width="9px" Font-Size="Smaller" CssClass="cmdlinks" ForeColor="#3366cc"
                                                Font-Bold="True" Height="10px">a</asp:label>&nbsp;<asp:label id="lblGoto" runat="server" Width="16px" Font-Size="Smaller" CssClass="cmdlinks"
                                                ForeColor="#3366cc" Font-Bold="True" Height="7px">Goto</asp:label>&nbsp;
                                                <asp:dropdownlist id="cmbPageNo" runat="server" Font-Size="Smaller" CssClass="clinputcombo" ForeColor="#3366cc"
                                                Font-Bold="True" AutoPostBack="True" onselectedindexchanged="cmbPageNo_SelectedIndexChanged"></asp:dropdownlist>

                                                <uc3:PagingControl ID="Pagingctrl" runat="server"  />
                                                &nbsp;&nbsp;

                                        </td>--%>
                            <td background="../Images/subhead_bg.gif" class="clssubhead" align="right" style="height: 34px">
                                <uc3:PagingControl ID="Pagingctrl" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td width="780" background="../images/separator_repeat.gif" colspan="7" height="11">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table id="TableGrid" cellspacing="0" cellpadding="0" width="100%" bgcolor="white"
                                    border="0">
                                    <tr>
                                        <td>
                                            <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlResult">
                                                <ProgressTemplate>
                                                    <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl1" runat="server" Text="Please Wait ......"
                                                        CssClass="clsLabel"></asp:Label>
                                                </ProgressTemplate>
                                            </aspnew:UpdateProgress>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="center" colspan="2">
                                            <asp:Label ID="lbl_message" runat="server" CssClass="label" ForeColor="Red"></asp:Label>
                                            <%--<asp:datagrid id="dg_valrep" runat="server" Width="100%" CssClass="clsLeftPaddingTable" BorderStyle="None"
                                            BorderColor="White" AutoGenerateColumns="False" BackColor="#EFF4FB" AllowPaging="True" PageSize="20">
                                            <Columns>
                                            <asp:TemplateColumn HeaderText="S.No">
                                            <HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
                                            <ItemStyle VerticalAlign="Top"></ItemStyle>
                                            <ItemTemplate>
                                            <asp:Label id="lbl_Sno" runat="server" CssClass="label"></asp:Label>
                                            <asp:Label id=lbl_ticketid runat="server" Width="17px" Text='<%# DataBinder.Eval(Container.DataItem, "TicketID_pk") %>' CssClass="label" Visible="False">
                                            </asp:Label>
                                            <asp:Label ID="lbl_search" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem, "activeflag") %>'
                                            Visible="False" Width="17px"></asp:Label>
                                            </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Cause No.">
                                            <HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
                                            <ItemStyle VerticalAlign="Top"></ItemStyle>
                                            <ItemTemplate>
                                            <asp:HyperLink ID="hlk_CauseNo" runat="server" NavigateUrl='<%# "../ClientInfo/ViolationFeeold.aspx?search=0&casenumber=" +   DataBinder.Eval(Container,"DataItem.ticketid_pk") %>'
                                            Target="_blank" Text='<%# DataBinder.Eval(Container.DataItem, "causenumber") %>'></asp:HyperLink>
                                            </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Last Name">
                                            <HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
                                            <ItemStyle VerticalAlign="Top"></ItemStyle>
                                            <ItemTemplate>
                                            <asp:Label ID="lbl_LastName" CssClass="label" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "lastname") %>'></asp:Label>                                                                    
                                            </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="First Name">
                                            <HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
                                            <ItemStyle VerticalAlign="Top"></ItemStyle>
                                            <ItemTemplate>
                                            <asp:Label ID="lbl_FirstName" CssClass="label" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "firstname") %>'></asp:Label>                                                                    
                                            </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Ver. Status">
                                            <HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
                                            <ItemStyle VerticalAlign="Top"></ItemStyle>
                                            <ItemTemplate>
                                            <asp:Label id=lbl_vdescription runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.VDesc") %>' CssClass="Label">
                                            </asp:Label>
                                            </ItemTemplate>                                                                
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Ver. Date">
                                            <HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
                                            <ItemStyle VerticalAlign="Top"></ItemStyle>
                                            <ItemTemplate>
                                            <asp:Label id=lbl_verified runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.courtdatemain") %>' CssClass="label">
                                            </asp:Label>																	
                                            </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Ver. Time">
                                            <HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
                                            <ItemStyle VerticalAlign="Top"></ItemStyle>
                                            <ItemTemplate>
                                            <asp:Label id=lbl_verifiedTime runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.courttimemain") %>' CssClass="label">
                                            </asp:Label>
                                            </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Ver. Room">
                                            <HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
                                            <ItemStyle VerticalAlign="Top"></ItemStyle>
                                            <ItemTemplate>
                                            <asp:Label id=lbl_verifiedcrtno runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.courtnumbermain") %>' CssClass="label">
                                            </asp:Label>
                                            </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Auto Status">
                                            <HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
                                            <ItemStyle VerticalAlign="Top"></ItemStyle>
                                            <ItemTemplate>
                                            <asp:Label id=lbl_adescription runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ADesc") %>' CssClass="Label">
                                            </asp:Label>
                                            </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Auto Date">
                                            <HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
                                            <ItemStyle VerticalAlign="Top"></ItemStyle>
                                            <ItemTemplate>
                                            <asp:Label id=lbl_auto runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.courtdate") %>' CssClass="label">
                                            </asp:Label>																	
                                            </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Auto Time">
                                            <HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
                                            <ItemStyle VerticalAlign="Top"></ItemStyle>
                                            <ItemTemplate>
                                            <asp:Label id=lbl_autoTime runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.courttime") %>' CssClass="label">
                                            </asp:Label>
                                            </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Auto Room">
                                            <HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
                                            <ItemStyle VerticalAlign="Top"></ItemStyle>
                                            <ItemTemplate>
                                            <asp:Label id=lbl_autocrtno runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.courtnumber") %>' CssClass="label">
                                            </asp:Label>
                                            </ItemTemplate>
                                            </asp:TemplateColumn>

                                            </Columns>
                                            <PagerStyle VerticalAlign="Middle" NextPageText="   Next &gt;" PrevPageText="  &lt; Previous        "
                                            HorizontalAlign="Center"></PagerStyle>
                                            </asp:datagrid>--%>
                                            <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline">
                                                <ContentTemplate>
                                                    <asp:GridView ID="gv_Records" runat="server" AutoGenerateColumns="False" Width="100%"
                                                        CssClass="clsLeftPaddingTable" AllowSorting="True" AllowPaging="True" OnPageIndexChanging="gv_Records_PageIndexChanging"
                                                        OnSorting="gv_Records_Sorting" OnRowDataBound="gv_Records_RowDataBound" PageSize="20"
                                                        OnRowCommand="gv_Records_RowCommand">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="S.No">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_Sno" runat="server" CssClass="Label" Text='<%# Eval("sno") %>'>></asp:Label>
                                                                    <asp:Label ID="lbl_ticketid" runat="server" Width="17px" Text='<%# DataBinder.Eval(Container.DataItem, "TicketID_pk") %>'
                                                                        CssClass="Label" Visible="False">
                                                                    </asp:Label>
                                                                    <asp:Label ID="lbl_search" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container.DataItem, "activeflag") %>'
                                                                        Visible="False" Width="17px"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<u>Cause No.</u>" SortExpression="causenumber">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:HyperLink ID="hlk_CauseNo" runat="server" NavigateUrl='<%# "../ClientInfo/ViolationFeeold.aspx?search=0&casenumber=" +   DataBinder.Eval(Container,"DataItem.ticketid_pk") %>'
                                                                        Target="_blank" Text='<%# DataBinder.Eval(Container.DataItem, "causenumber") %>'></asp:HyperLink>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<u>Last Name</u>" SortExpression="lastname">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_LastName" CssClass="Label" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "lastname") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<u>First Name</u>" SortExpression="firstname">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_FirstName" CssClass="Label" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "firstname") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<u>V Status</u>" SortExpression="VDesc">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_vdescription" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.VDesc") %>'
                                                                        CssClass="Label">
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<u>V Date</u>" SortExpression="courtdatemain">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_verified" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.courtdatemain") %>'
                                                                        CssClass="Label">
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<u>V Time</u>" SortExpression="courttimemain">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_verifiedTime" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.courttimemain") %>'
                                                                        CssClass="Label">
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<u>V Room</u>" SortExpression="courtnumbermain">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_verifiedcrtno" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.courtnumbermain") %>'
                                                                        CssClass="Label">
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<u>A Status</u>" SortExpression="ADesc">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_adescription" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ADesc") %>'
                                                                        CssClass="Label">
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<u>A Date</u>" SortExpression="courtdate">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_auto" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.courtdate") %>'
                                                                        CssClass="Label">
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<u>A Time</u>" SortExpression="courttime">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_autoTime" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.courttime") %>'
                                                                        CssClass="Label">
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<u>A Room</u>" SortExpression="courtnumber">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_autocrtno" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.courtnumber") %>'
                                                                        CssClass="Label">
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<u>Follow Up Date</u>" SortExpression="sortfollowupdate">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_FollowUp" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.followupdate") %>'
                                                                        CssClass="Label">
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="img_Add" runat="server" CommandName="btnclick" ImageUrl="../Images/add.gif" />
                                                                    <asp:HiddenField ID="hf_fname" runat="server" Value='<%#Eval("FIRSTNAME") %>' />
                                                                    <asp:HiddenField ID="hf_lname" runat="server" Value='<%#Eval("LASTNAME") %>' />
                                                                    <asp:HiddenField ID="hf_ticketno" runat="server" Value='<%#Eval("ticketid_pk") %>' />
                                                                    <asp:HiddenField ID="hf_causeno" runat="server" Value='<%#Eval("causenumber") %>' />
                                                                    <asp:HiddenField ID="hf_followupdate" runat="server" Value='' />
                                                                    <asp:HiddenField ID="hf_ticketnumber" runat="server" Value='<%#Eval("RefCaseNumber") %>' />
                                                                    <asp:HiddenField ID="hf_ticketid" runat="server" Value='<%#Eval("TicketID_pk") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <PagerStyle HorizontalAlign="Center" />
                                                        <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                            FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                                    </asp:GridView>
                                                </ContentTemplate>
                                            </aspnew:UpdatePanel>
                                            <table style="display: none" id="tbl_plzwait1" width="800">
                                                <tbody>
                                                    <tr>
                                                        <td class="clssubhead" valign="middle" align="center" style="width: 785px">
                                                            <img src="../Images/plzwait.gif" />
                                                            Please wait While we update your Violation information.
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <aspnew:UpdateProgress ID="updateprogress2" runat="server" AssociatedUpdatePanelID="UpdatePanelcrtl">
                                                <ProgressTemplate>
                                                    <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl2" runat="server" Text="Please Wait ......"
                                                        CssClass="clsLabel"></asp:Label>
                                                </ProgressTemplate>
                                            </aspnew:UpdateProgress>
                                            <aspnew:UpdatePanel ID="UpdatePanelcrtl" runat="server">
                                                <ContentTemplate>
                                                    <asp:Panel ID="pnlFollowup" runat="server">
                                                        <uc3:UpdateFollowUpInfo ID="UpdateFollowUpInfo2" runat="server" Title="HMC Setting Discrepancy Follow Up Date" />
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </aspnew:UpdatePanel>
                                            <aspnew:UpdatePanel ID="UpdatePanelmodal" runat="server">
                                                <ContentTemplate>
                                                    <ajaxToolkit:ModalPopupExtender ID="mpehmcsetting" runat="server" BackgroundCssClass="modalBackground"
                                                        PopupControlID="pnlFollowup" TargetControlID="btn">
                                                    </ajaxToolkit:ModalPopupExtender>
                                                    <asp:Button ID="btn" runat="server" Style="display: none;" />
                                                </ContentTemplate>
                                            </aspnew:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="780" background="../images/separator_repeat.gif" colspan="5" height="11">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="5">
                                            <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    </ContentTemplate>
        <Triggers>
        </Triggers>
    </aspnew:UpdatePanel>
    </form>

    <script language="javascript" type="text/javascript">
    document.getElementById("UpdateFollowUpInfo2_cal_followupdate_div").style.zIndex = 10003;
    document.getElementById("UpdateFollowUpInfo2_cal_followupdate_monthYear").style.zIndex = 10004;
    </script>

</body>
</html>

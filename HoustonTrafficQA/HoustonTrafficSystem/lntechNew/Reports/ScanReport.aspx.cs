using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;
using System.Data.SqlClient;

namespace lntechNew.Reports
{
	
	public partial class ScanReport : System.Web.UI.Page
	{
		protected eWorld.UI.CalendarPopup calQueryFrom;
		
		protected System.Web.UI.WebControls.DataGrid dg_valrep;
		protected System.Web.UI.WebControls.Button btn_submit;
		DataSet ds_val;
		protected System.Web.UI.WebControls.Label lbl_message;
		protected System.Web.UI.WebControls.DropDownList cmbPageNo;
		protected System.Web.UI.WebControls.Label lblGoto;
		protected System.Web.UI.WebControls.Label lblPNo;
		protected System.Web.UI.WebControls.Label lblCurrPage;
		clsSession cSession	 =new clsSession();
		clsLogger bugTracker = new clsLogger();
		clsENationWebComponents ClsDb = new clsENationWebComponents();
		protected System.Web.UI.WebControls.TextBox txtHidden;
		protected System.Web.UI.WebControls.LinkButton btnDelRecs;
		protected System.Web.UI.WebControls.DropDownList ddlType;
        clsENationWebComponents ClsDbscan = new clsENationWebComponents("returnDockets");
		DataView dv_Result = null;
		string StrExp = String.Empty;
		string StrAcsDec = String.Empty;
		//Call on every page load
		private void Page_Load(object sender, System.EventArgs e)
		{
			if (cSession.IsValidSession(this.Request)==false)
			{
				Response.Redirect("../frmlogin.aspx",false);
			}
			else
			{
				if (cSession.GetCookie("sAccessType",this.Request).ToString()!= "2")
					btnDelRecs.Enabled = false;
				else
					btnDelRecs.Enabled = true;

				
				if(!IsPostBack)
				{
					calQueryFrom.SelectedDate=DateTime.Now.Date;
                    calQueryTo.SelectedDate = DateTime.Now.Date;
				}
				if(dg_valrep.Items.Count<1)
				{
					lblCurrPage.Visible=false;
					lblGoto.Visible=false;
					lblPNo.Visible=false;
					cmbPageNo.Visible=false;
					btnDelRecs.Visible=false;
				}	
				else
				{
					lblCurrPage.Visible=true;
					lblGoto.Visible=true;
					lblPNo.Visible=true;
					cmbPageNo.Visible=true;
					btnDelRecs.Visible=true;
				}
				
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btn_submit.Click += new System.EventHandler(this.Button1_Click);
			this.btnDelRecs.Click += new System.EventHandler(this.btnDelRecs_Click);
			this.cmbPageNo.SelectedIndexChanged += new System.EventHandler(this.cmbPageNo_SelectedIndexChanged_1);
			this.dg_valrep.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_valrep_ItemCommand);
			this.dg_valrep.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dg_valrep_PageIndexChanged);
			this.dg_valrep.SortCommand += new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.dg_valrep_SortCommand);
			this.dg_valrep.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dg_valrep_ItemDataBound);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		//When submit button is clicked
		private void Button1_Click(object sender, System.EventArgs e)
		{
			try
			{
				FillGrid();		
			}
			catch(Exception ex)
			{
				if (dg_valrep.CurrentPageIndex > dg_valrep.PageCount -1 )
				{
					dg_valrep.CurrentPageIndex = dg_valrep.PageCount - 1;
					dg_valrep.DataBind();
					BindReport();
					txtHidden.Text = dg_valrep.Items.Count.ToString();
					FillPageList();
					SetNavigation();
					if(dg_valrep.Items.Count<1)
					{
						lblCurrPage.Visible=false;
						lblGoto.Visible=false;
						lblPNo.Visible=false;
						cmbPageNo.Visible=false;
						btnDelRecs.Visible=false;
					}	
					else
					{
						lblCurrPage.Visible=true;
						lblGoto.Visible=true;
						lblPNo.Visible=true;
						cmbPageNo.Visible=true;
						btnDelRecs.Visible=true;
					}
				}
				else
				{
					bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
					lbl_message.Text=ex.Message;
				}

			}
		}
		//In order to populate grid
		private void FillGrid()
		{
			string[] key={"@sdate","@requesttype", "@edate"};
			object[] value1={calQueryFrom.SelectedDate,ddlType.SelectedValue,calQueryTo.SelectedDate};
			ds_val=ClsDb.Get_DS_BySPArr("USP_HTS_GET_ScanReport",key,value1);	
            lbl_message.Text="";
			if(ds_val.Tables[0].Rows.Count<1)
			{
				lbl_message.Text="No Record Found";				
			}

			dg_valrep.DataSource=ds_val;							
			dg_valrep.DataBind();
			BindReport();
			txtHidden.Text = dg_valrep.Items.Count.ToString();
			FillPageList();
			SetNavigation();
			if(dg_valrep.Items.Count<1)
			{
				lblCurrPage.Visible=false;
				lblGoto.Visible=false;
				lblPNo.Visible=false;
				cmbPageNo.Visible=false;
				btnDelRecs.Visible=false;
			}	
			else
			{
				lblCurrPage.Visible=true;
				lblGoto.Visible=true;
				lblPNo.Visible=true;
				cmbPageNo.Visible=true;
				btnDelRecs.Visible=true;
			}


		}
		//Method To Generate Serial No
		private void BindReport()
		{
			int docid=0;
			int docnum=0;
			long sNo=(dg_valrep.CurrentPageIndex)*(dg_valrep.PageSize);									
			try
			{
				
				foreach (DataGridItem ItemX in dg_valrep.Items) 
				{ 					
					sNo +=1 ;
      
					((Label)(ItemX.FindControl("lbl_Sno"))).Text= sNo.ToString();
					//((HyperLink)(ItemX.FindControl("hlk_Sno"))).Text= sNo.ToString();
					//((HyperLink) (ItemX.FindControl("hlkTicketId"))).Text = sNo.ToString();
					//((HyperLink) (ItemX.FindControl("hlkTicketId"))).NavigateUrl = "../ClientInfo/ViolationFeeOld.aspx?source=0&casenumber=" + ticketid ; 
					docid=Convert.ToInt32(((Label)(ItemX.FindControl("lbl_docid"))).Text);
					docnum=Convert.ToInt32(((Label)(ItemX.FindControl("lbl_docnum"))).Text);
					((LinkButton) ItemX.FindControl("lnkb_status")).Attributes.Add("OnClick","return OpenScanPopup(" + docid + "," + docnum + ");");  		
			
					((LinkButton) ItemX.FindControl("lnkb_ticketno")).Attributes.Add("OnClick","return OpenScanPopup1(" + docid + "," + docnum + ");");  		

					


				}
			}
			catch(Exception ex)
			{
				lbl_message.Text = ex.Message ;
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}		
		}

		private void dg_valrep_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			try
			{
				if (e.Item.ItemType == ListItemType.Header )
					((CheckBox)(e.Item.FindControl("chkDelAll"))).Attributes.Add("onclick","SelectDeselectAll()");
				else if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem  )
				{
					DataRowView drv = (DataRowView) e.Item.DataItem;
					if (drv == null)
						return;
					
					string status = drv["updateflag"].ToString();
					if (status.StartsWith("U")|| status.StartsWith("E"))
					{
						((LinkButton) e.Item.FindControl("lnkb_status")).Visible=false;
						((HyperLink) e.Item.FindControl("hlnk_status")).Visible=true;				
					}
				}
			}
			catch{}	
		}	
	
		#region Navigation

		//----------------------------------Navigational LOGIC-----------------------------------------//
		
		// Procedure for filling page numbers in page number combo........
		private void FillPageList()
		{
			Int16 idx;

			cmbPageNo.Items.Clear();
			for (idx=1; idx<=dg_valrep.PageCount;idx++)
			{
				cmbPageNo.Items.Add ("Page - " + idx.ToString());
				cmbPageNo.Items[idx-1].Value=idx.ToString();
			}
		}	

		// Procedure for setting data grid's current  page number on header ........
		private void SetNavigation()
		{
			try
			{			
				// setting current page number
				lblPNo.Text =Convert.ToString(dg_valrep.CurrentPageIndex+1);
				lblCurrPage.Visible=true;
				lblPNo.Visible=true;

				// filling combo with page numbers
				lblGoto.Visible=true;
				cmbPageNo.Visible=true;
				//FillPageList();

				cmbPageNo.SelectedIndex =dg_valrep.CurrentPageIndex;
			}
			catch (Exception ex)
			{
				lbl_message.Text = ex.Message ;
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}

		// Procedure for setting the grid page number as per selected from page no. combo.......
		private void cmbPageNo_SelectedIndexChanged_1(object sender, System.EventArgs e)
		{
			try
			{				
				dg_valrep.CurrentPageIndex= cmbPageNo.SelectedIndex ;												
				FillGrid();
			}
			catch (Exception ex)
			{
				lbl_message.Text=ex.Message;
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}
#endregion
		//When page index is changed
		private void dg_valrep_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			dg_valrep.CurrentPageIndex=e.NewPageIndex;
			FillGrid();
		}
		//When Ticket is clicked in order to show scan doc
		private void dg_valrep_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			if (e.CommandName=="Clicked")
			{
                //Change by Ajmal
				IDataReader dr_popup=null;
				try
				{				
					int docid=Convert.ToInt32(((Label)(e.Item.FindControl("lbl_docid"))).Text);
					int docnum=docnum=Convert.ToInt32(((Label)(e.Item.FindControl("lbl_docnum"))).Text);		
					//Get records by docid + docnum
				
					if (docnum>0)
					{
						string[] key={"@DocID","@DocNum"};
						object[] value1={docid,docnum};
						dr_popup=ClsDbscan.Get_DR_BySPArr("usp_Get_SacanDocImageByDocIDByDocNum",key,value1);                
					}
					else
					{				
						string[] key={"@DocID"};
						object[] value1={docid};
						dr_popup=ClsDbscan.Get_DR_BySPArr("usp_Get_SacanDocImageByDocID",key,value1);                				
					}		
                   SqlDataReader drtest = (SqlDataReader) dr_popup;
                   if (drtest.HasRows)
                   {
                       clsGeneralMethods cgMethods = new clsGeneralMethods();
                       string rptpath = cgMethods.AddImageToPDF(Server.MapPath(""), docid, dr_popup, this.Session, this.Response);
                       Session["PDFImage"] = rptpath;
                       //Response.Redirect("../frame.aspx");
                       HttpContext.Current.Response.Write("<script language='javascript'>  function OpenPDF(){	var Wind; Wind  = window.open('../frame.aspx','','fullscreen=no,toolbar=no,width=600,height=500,left=50,top=50,status=no,menubar=no,scrollbars=yes,resizable=yes'); return false; }  OpenPDF();</script>");
                   }
					else
						lbl_message.Text = "No scanned document found for the selected record.";
					
				}
				catch(Exception ex)
				{
					lbl_message.Text=ex.Message;
					bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
				}
				finally
				{
					dr_popup.Close();
				}
			}
		}

		private void btnDelRecs_Click(object sender, System.EventArgs e)
		{
			StringBuilder sb = new StringBuilder();		
			bool IsDeleted = false;
			foreach(DataGridItem ItemX in dg_valrep.Items)
			{
				if ( ((CheckBox) (ItemX.FindControl("chkDel"))).Checked )
					sb = sb.Append(((Label)(ItemX.FindControl("lblRecId"))).Text.ToString() + "," );
			}
			string[] key1 = {"@RecIDs"};
			string[] value1 = {sb.ToString()};
			IsDeleted = Convert.ToBoolean(ClsDb.ExecuteScalerBySp("USP_HTS_Update_RemoveScanTickets",key1,value1));

			if (IsDeleted)
			{
				HttpContext.Current.Response.Write("<script language=javascript> alert('Selected record(s) deleted successfully.'); </script>  ");
				FillGrid();
			}
			
		}

		private void dg_valrep_SortCommand(object source, System.Web.UI.WebControls.DataGridSortCommandEventArgs e)
		{
			SortGrid(e.SortExpression);
		}

		private void SortGrid(string SortExp)
		{
			try
			{
				SetAcsDesc(SortExp);
                string[] key ={ "@sdate", "@requesttype", "@edate" };
                object[] value1 ={ calQueryFrom.SelectedDate, ddlType.SelectedValue, calQueryTo.SelectedDate };
                ds_val = ClsDb.Get_DS_BySPArr("USP_HTS_GET_ScanReport", key, value1);	
				dv_Result = ds_val.Tables[0].DefaultView;
				dv_Result.Sort = StrExp + " " + StrAcsDec;


				lbl_message.Text="";
				if(ds_val.Tables[0].Rows.Count<1)
				{
					lbl_message.Text="No Record Found";				
				}

				dg_valrep.DataSource=dv_Result;	
				try
				{		
					dg_valrep.DataBind();
				}
				catch
				{
					if (dg_valrep.CurrentPageIndex > dg_valrep.PageCount -1 )
					{
						dg_valrep.CurrentPageIndex = dg_valrep.PageCount - 1;
						dg_valrep.DataBind();
					}
				}
				BindReport();
				txtHidden.Text = dg_valrep.Items.Count.ToString();
				FillPageList();
				SetNavigation();
				if(dg_valrep.Items.Count<1)
				{
					lblCurrPage.Visible=false;
					lblGoto.Visible=false;
					lblPNo.Visible=false;
					cmbPageNo.Visible=false;
					btnDelRecs.Visible=false;
				}	
				else
				{
					lblCurrPage.Visible=true;
					lblGoto.Visible=true;
					lblPNo.Visible=true;
					cmbPageNo.Visible=true;
					btnDelRecs.Visible=true;
				}				
				

			}
			catch(Exception ex)
			{
				lbl_message.Text = ex.Message;
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);

			}
		}

		private void SetAcsDesc(string Val)
		{
			try
			{
				StrExp =  cSession.GetSessionVariable ("StrExp",this.Session);
				StrAcsDec = cSession.GetSessionVariable("StrAcsDec",this.Session);
			}
			catch
			{

			}

			if (StrExp == Val)
			{
				if (StrAcsDec == "ASC")
				{
					StrAcsDec = "DESC";
					cSession.SetSessionVariable("StrAcsDec",StrAcsDec, this.Session);
				}
				else 
				{
					StrAcsDec = "ASC";
					cSession.SetSessionVariable("StrAcsDec",StrAcsDec,this.Session);
				}
			}
			else 
			{
				StrExp = Val;
				StrAcsDec = "ASC";
				cSession.SetSessionVariable("StrExp", StrExp,this.Session) ;
				cSession.SetSessionVariable("StrAcsDec", StrAcsDec, this.Session) ;				
			}
		}
	
	}
}

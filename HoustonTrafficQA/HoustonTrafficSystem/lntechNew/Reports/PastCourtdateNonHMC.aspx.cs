﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;
using HTP.Components;


namespace HTP.Reports
{
    public partial class PastCourtdateNonHMC : System.Web.UI.Page
    {
        ValidationReports reports = new ValidationReports();
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        clsSession ClsSession = new clsSession();
        clsCase cCase = new clsCase();
        string StrExp = String.Empty;
        string StrAcsDec = String.Empty;
        DataView DV;
        int day = 0;
        int Report_ID = 0;

        public SortDirection GridViewSortDirection
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Ascending;
                return (SortDirection)ViewState["sortDirection"];
            }
            set { ViewState["sortDirection"] = value; }

        }

        public string GridViewSortExpression
        {
            get
            {
                if (ViewState["sortExpression"] == null)
                    ViewState["sortExpression"] = "";
                return ViewState["sortExpression"].ToString();
            }
            set { ViewState["sortExpression"] = value; }

        }

        protected override void OnInit(EventArgs e)
        {
            ShowSetting.OnErr += new HTP.WebControls.ShowSetting.ErrHandler(ShowSetting_OnErr);
            ShowSetting.dbBind += new HTP.WebControls.ShowSetting.Databind(ShowSetting_dbBind);
            base.OnInit(e);
        }


        /// <summary>
        /// ShowSetting display message on error
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="ErrorMsg"></param>
        void ShowSetting_OnErr(object sender, string ErrorMsg)
        {
            lbl_Message.Text = ErrorMsg;
        }


        /// <summary>
        /// Show Setting Control Databind
        /// </summary>
        void ShowSetting_dbBind()
        {
            BindGrid();
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            int Empid = 0;
            if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false ||
                        (!int.TryParse(ClsSession.GetCookie("sEmpID", this.Request), out Empid)))
            {
                Response.Redirect("../frmlogin.aspx", false);
            }
            else
            {
                if (!IsPostBack)
                {
                    try
                    {
                        lbl_Message.Text = "";
                        GridViewSortDirection = SortDirection.Ascending;
                        GridViewSortExpression = "PastCourtDateFollowUpDate";
                        BindGrid();
                    }

                    catch (Exception ex)
                    {
                        lbl_Message.Text = ex.Message;
                        clsLogger.ErrorLog(ex);
                    }
                }

                Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
                Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
                UpdateFollowUpInfo2.PageMethod += new HTP.WebControls.PageMethodHandler(UpdateFollowUpInfo2_PageMethod);
                Pagingctrl.GridView = DG_pastcases;
            }
        }

        protected void gv_Records_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                string sortExpression = e.SortExpression;

                if (GridViewSortDirection == SortDirection.Ascending && sortExpression == GridViewSortExpression)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                else
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }

                GridViewSortExpression = sortExpression;
                BindGrid();
                Pagingctrl.PageCount = DG_pastcases.PageCount;
                Pagingctrl.PageIndex = DG_pastcases.PageIndex;

            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }


        }

        protected void DG_pastcases_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            DG_pastcases.PageIndex = e.NewPageIndex;
            BindGrid();

            //DataView dv = (DataView)Session["DS"];
            // LB_curr.Text = (e.NewPageIndex + 1).ToString();            
            //DG_pastcases.DataSource = dv;
            //DG_pastcases.DataBind();
            //FixSerial();

        }

        protected void DG_pastcases_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "ShowFolloupdate")
            {

                int RowID = Convert.ToInt32(e.CommandArgument);
                string lastname = (((Label)DG_pastcases.Rows[RowID].FindControl("lbl_LastName")).Text);
                string firstname = (((Label)DG_pastcases.Rows[RowID].FindControl("lbl_FirstName")).Text);
                string causeno = (((HiddenField)DG_pastcases.Rows[RowID].FindControl("hf_causenumber")).Value);
                string ticketno = (((Label)DG_pastcases.Rows[RowID].FindControl("lbl_CaseNumber")).Text);
                string courtname = "PastCourtDateHMC";
                string court = (((HiddenField)DG_pastcases.Rows[RowID].FindControl("hf_Courtid")).Value);
                string followupDate = (((Label)DG_pastcases.Rows[RowID].FindControl("lbl_follow")).Text);
                int ticketid = Convert.ToInt32((((HiddenField)DG_pastcases.Rows[RowID].FindControl("hf_ticketid")).Value));
                cCase.TicketID = ticketid;
                string comm = cCase.GetGeneralCommentsByTicketId();
                string follow = string.Empty;
                UpdateFollowUpInfo2.followUpType = FollowUpType.PastCourtDateNonHMCFollowupdate;
                UpdateFollowUpInfo2.Freezecalender = true;


                string url = Request.Url.AbsolutePath.ToString();
                string[] key = { "@Report_Url", "@Attribute_key" };
                object[] value = { url, "Days" };
                DataTable dtDays = new DataTable();

                dtDays = ClsDb.Get_DT_BySPArr("usp_htp_get_reportByUrl", key, value);

                if (dtDays.Rows.Count > 0)
                    Report_ID = Convert.ToInt32(dtDays.Rows[0]["Report_ID"]);

                string[] key1 = { "@Attribute_Key", "@Report_Id" };
                string[] value1 = { "Days", Report_ID.ToString() };
                dtDays = ClsDb.Get_DT_BySPArr("usp_hts_get_BusinessLogicDay", key1, value1);

                if (dtDays.Rows.Count > 0)
                    day = Convert.ToInt32(dtDays.Rows[0]["Attribute_Value"].ToString());

                UpdateFollowUpInfo2.binddate(day.ToString(), clsGeneralMethods.GetBusinessDayDate(DateTime.Now, day), DateTime.Today, firstname, lastname, ticketno, ticketid.ToString(), causeno, comm, ModalPopupExtender1.ClientID, courtname, courtname, followupDate);

                //UpdateFollowUpInfo2.binddate(DateTime.Today, DateTime.Today, firstname, lastname, ticketno, causeno, comm, ModalPopupExtender1.ClientID, courtname, ticketid);
                ModalPopupExtender1.Show();
                Pagingctrl.Visible = true;
            }
        }

        protected void DG_pastcases_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ((LinkButton)e.Row.FindControl("img_Add")).CommandArgument = e.Row.RowIndex.ToString();
                    pnlFollowup.Style["display"] = "none";
                }
            }

            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        private void SortGrid(string SortExp)
        {
            try
            {
                SetAcsDesc(SortExp);
                DV = (DataView)Session["DS"];
                DV.Sort = StrExp + " " + StrAcsDec;
                DV.Table.Columns.Remove("sno");
                if (DV.Table.Columns.Contains("sno") == false)
                {
                    DV.Table.Columns.Add("sno");
                }
                DG_pastcases.DataSource = DV;
                DG_pastcases.DataBind();
                Session["DS"] = DV;


                //FixSerial();
            }



            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
            }
        }

        public void FixSerial()
        {
            int stind = (DG_pastcases.PageIndex * DG_pastcases.PageSize) + 1;

            try
            {
                for (int i = 0; i <= DG_pastcases.PageSize; i++)
                {

                    HyperLink lb_sno = (HyperLink)DG_pastcases.Rows[i].FindControl("sno");
                    lb_sno.Text = stind.ToString();
                    stind++;

                }
            }
            catch (Exception)
            {
                //lbl_Message.Text = e.Message;
            }


        }

        private void SetAcsDesc(string Val)
        {
            try
            {
                StrExp = Session["StrExp"].ToString();
                StrAcsDec = Session["StrAcsDec"].ToString();
            }
            catch
            {

            }

            if (StrExp == Val)
            {
                if (StrAcsDec == "DESC")
                {
                    StrAcsDec = "ASC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
                else
                {
                    StrAcsDec = "DESC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
            }
            else
            {
                StrExp = Val;
                StrAcsDec = "DESC";
                Session["StrExp"] = StrExp;
                Session["StrAcsDec"] = StrAcsDec;
            }
        }

        private void BindGrid()
        {
            //Fahad 5808 04/18/2009 assign empty string to Error message 
            lbl_Message.Text = "";
            DataTable dt;
            dt = reports.GetRecords_PastCourtDatesForNonHMC(Convert.ToInt32(cb_ShowAll.Checked));
            if (dt.Rows.Count > 0)
            {
                if (dt.Columns.Contains("sno") == false)
                {
                    dt.Columns.Add("sno");
                    DataView dv = dt.DefaultView;
                    dv.Sort = GridViewSortExpression + " " + (GridViewSortDirection == SortDirection.Ascending ? "ASC" : "DESC");
                    dt = dv.ToTable();
                    int sno = 1;
                    if (dt.Rows.Count >= 1)
                        dt.Rows[0]["sno"] = 1;
                    if (dt.Rows.Count >= 2)
                    {
                        for (int i = 1; i < dt.Rows.Count; i++)
                        {
                            if (dt.Rows[i - 1]["ticketid_pk"].ToString() != dt.Rows[i]["ticketid_pk"].ToString())
                            {
                                dt.Rows[i]["sno"] = ++sno;
                            }
                            else
                                dt.Rows[i]["sno"] = ".";
                        }
                    }
                }

                DG_pastcases.DataSource = dt;
                DG_pastcases.DataBind();
                Pagingctrl.PageCount = DG_pastcases.PageCount;
                Pagingctrl.PageIndex = DG_pastcases.PageIndex;
                Pagingctrl.SetPageIndex();

                Session["DS"] = dt.DefaultView;
            }
            else
                lbl_Message.Text = "No Records Found";
        }

        void Pagingctrl_PageIndexChanged()
        {
            DG_pastcases.PageIndex = Pagingctrl.PageIndex - 1;
            BindGrid();
        }

        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            if (pageSize > 0)
            {
                DG_pastcases.PageIndex = 0;
                DG_pastcases.PageSize = pageSize;
                DG_pastcases.AllowPaging = true;
            }
            else
            {
                DG_pastcases.AllowPaging = false;
            }
            BindGrid();
        }

        void UpdateFollowUpInfo2_PageMethod()
        {
            BindGrid();
        }

        protected void cb_ShowAll_CheckedChanged(object sender, EventArgs e)
        {
            BindGrid();
        }
    }
}
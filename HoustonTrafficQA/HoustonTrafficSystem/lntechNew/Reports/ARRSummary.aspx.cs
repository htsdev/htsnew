using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace HTP.Reports
{
    public partial class ARRSummary : System.Web.UI.Page
    {
        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["Flag"] = Request.QueryString["Flag"].ToString();
                if (Request.QueryString["Flag"] == "1")
                {
                    HttpContext.Current.Response.Write("<script language='javascript'> window.opener.location.href= window.opener.location.href;</script>");
                }
                // Afaq 8039 08/19/2010 it will refresh the parent page For stopping event bubbling
                else
                {
                    HttpContext.Current.Response.Write("<script language='javascript'> window.opener.location.href='../QuickEntryNew/ArrCombined.aspx?ReportType=2';</script>");
                }
            }
        }

        #endregion
    }
}

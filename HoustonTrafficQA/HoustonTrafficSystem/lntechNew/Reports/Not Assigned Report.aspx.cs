﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using WebSupergoo.ABCpdf6;

namespace HTP.Reports
{
    //noufil 4213 06/16/2008 Code copied from quickentrynew/arrcombin and make this report as a separate report
    public partial class Not_Assigned_Report : System.Web.UI.Page
    {
        #region variables

        clsENationWebComponents ClsDb = new clsENationWebComponents();
        clsLogger bugTracker = new clsLogger();
        DataView dv;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    fill_dg_Report();
                    fill_dg_ReportOT();
                    ViewState["vNTPATHDocketPDF"] = ConfigurationSettings.AppSettings["NTPATHDocketPDF"].ToString();
                }
                Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
                PagingControl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(PagingControl_PageIndexChanged);
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        void PagingControl_PageIndexChanged()
        {
            try
            {
                dg_ReportOT.PageIndex = PagingControl.PageIndex - 1;
                fill_dg_ReportOT();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        void Pagingctrl_PageIndexChanged()
        {
            try
            {
                dg_Report.PageIndex = Pagingctrl.PageIndex - 1;
                fill_dg_Report();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void dg_Report_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                dg_Report.PageIndex = e.NewPageIndex;
                fill_dg_Report();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        protected void dg_ReportOT_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                dg_ReportOT.PageIndex = e.NewPageIndex;
                fill_dg_ReportOT();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        protected void dg_Report_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                string sortExpression = e.SortExpression;

                if (GridViewSortDirection == SortDirection.Ascending)
                {
                    GridViewSortDirection = SortDirection.Descending;
                    SortGridView(sortExpression, "DESC");
                }
                else
                {
                    GridViewSortDirection = SortDirection.Ascending;
                    SortGridView(sortExpression, "ASC");
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void dg_ReportOT_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                string sortExpression = e.SortExpression;

                if (GridViewSortDirection == SortDirection.Ascending)
                {
                    GridViewSortDirection = SortDirection.Descending;
                    SortGridViewOT(sortExpression, "DESC");
                }
                else
                {
                    GridViewSortDirection = SortDirection.Ascending;
                    SortGridViewOT(sortExpression, "ASC");
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }
       
        #endregion

        #region Methods
       
        private void fill_dg_Report()
        {
            string[] key1 = { "@ReportWeekDay" };
            object[] value2 = { 99 };
            DataTable dt = ClsDb.Get_DT_BySPArr("usp_HTS_ArraignmentSnapshotLong_ver_1_Update", key1, value2);
            GenerateSerial(dt);

            if (Session["DV_report"] == null)
            {
                dv = new DataView(dt);
                Session["DV_report"] = dv;
            }
            else
            {
                dv = (DataView)Session["DV_report"];
                string SortExp = dv.Sort;
                dv = new DataView(dt);
                dv.Sort = SortExp;
            }

            dg_Report.DataSource = dv;
            dg_Report.DataBind();
            FillPagging();

        }

        private void fill_dg_ReportOT()
        {
            DataTable ds_OT = ClsDb.Get_DT_BySPArr("usp_HTS_ArraignmentSnapshotLongFooterOutsideArraignment_ver_1_Update");
            GenerateSerial(ds_OT);

            if (ds_OT.Rows.Count > 0)
            {
                if (Session["DV_reportOT"] == null)
                {
                    dv = new DataView(ds_OT);
                    Session["DV_reportOT"] = dv;
                }
                else
                {
                    dv = (DataView)Session["DV_reportOT"];
                    string SortExp = dv.Sort;
                    dv = new DataView(ds_OT);
                    dv.Sort = SortExp;
                }

                
                dg_ReportOT.DataSource = dv;
                dg_ReportOT.DataBind();
                FillPagging();
            }
        }

        private void GenerateSerial(DataTable dt)
        {
            if (dt.Columns.Contains("SNO") == false)
            {
                dt.Columns.Add("SNO");
                int sno = 1;
                if (dt.Rows.Count >= 1)
                    dt.Rows[0]["sno"] = 1;
                if (dt.Rows.Count >= 2)
                {
                    for (int i = 1; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[i - 1]["ticketid_pk"].ToString() != dt.Rows[i]["ticketid_pk"].ToString())
                        {
                            dt.Rows[i]["sno"] = ++sno;
                        }
                    }
                }
            }
        }
        
        public SortDirection GridViewSortDirection
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Ascending;
                return (SortDirection)ViewState["sortDirection"];
            }
            set { ViewState["sortDirection"] = value; }

        }

        private void SortGridView(string sortExpression, string direction)
        {
            try
            {
                dv = (DataView)Session["DV_report"];
                dv.Sort = sortExpression + " " + direction;
                Session["DV_report"] = dv;

                fill_dg_Report();
                Pagingctrl.PageCount = dg_Report.PageCount;
                Pagingctrl.PageIndex = dg_Report.PageIndex;

                ViewState["sortExpression"] = sortExpression;
                Session["SortDirection"] = direction;
                Session["SortExpression"] = sortExpression;
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        private void SortGridViewOT(string sortExpression, string direction)
        {
            try
            {
                dv = (DataView)Session["DV_reportOT"];
                dv.Sort = sortExpression + " " + direction;
                Session["DV_reportOT"] = dv;

                fill_dg_ReportOT();
                PagingControl.PageCount = dg_ReportOT.PageCount;
                PagingControl.PageIndex = dg_ReportOT.PageIndex;

                ViewState["sortExpression"] = sortExpression;
                Session["SortDirection"] = direction;
                Session["SortExpression"] = sortExpression;
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        void FillPagging()
        {
            Pagingctrl.PageCount = dg_Report.PageCount;
            Pagingctrl.PageIndex = dg_Report.PageIndex;
            Pagingctrl.SetPageIndex();

            PagingControl.PageCount = dg_ReportOT.PageCount;
            PagingControl.PageIndex = dg_ReportOT.PageIndex;
            PagingControl.SetPageIndex();
        }
                        
        #endregion


        
    }
}

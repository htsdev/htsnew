﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;

namespace HTP.Reports
{
    public partial class NullStatusReport : System.Web.UI.Page
    {
        ValidationReports reports = new ValidationReports();
        clsSession ClsSession = new clsSession();

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else
                {
                    if (!IsPostBack)
                    {
                        BindGrid();
                    }
                    Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
                    Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
                    Pagingctrl.GridView = gv_Data;
                }
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
               
            }
        }


        /// <summary>
        /// handling pageIndexChange  of pagging control
        /// </summary>
        void Pagingctrl_PageIndexChanged()
        {
            try
            {
                gv_Data.PageIndex = Pagingctrl.PageIndex - 1;
                BindGrid();
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                
            }
        }

        /// <summary>
        /// handling PageSizeChanged  of pagging control
        /// </summary>
        /// <param name="pageSize"></param>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            try
            {
                if (pageSize > 0)
                {
                    gv_Data.PageIndex = 0;
                    gv_Data.PageSize = pageSize;
                    gv_Data.AllowPaging = true;
                }
                else
                    gv_Data.AllowPaging = false;
                BindGrid();
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// handling pageIndexChange  of Grid 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_Data_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_Data.PageIndex = e.NewPageIndex;
                BindGrid();
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
            }
        }

        #endregion

        #region Methods
        /// <summary>
        /// Binding data to the Grid.
        /// </summary>
        protected void BindGrid()
        {
            try
            {
                reports.getReportData(gv_Data, lbl_message, ValidationReport.NullStatusReport);
                Pagingctrl.PageCount = gv_Data.PageCount;
                Pagingctrl.PageIndex = gv_Data.PageIndex;
                Pagingctrl.SetPageIndex();
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
            }

        }

        #endregion

    }
}

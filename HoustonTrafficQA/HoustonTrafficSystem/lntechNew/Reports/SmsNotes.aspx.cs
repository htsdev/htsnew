using System;
using System.Collections;
using System.Data;
using System.Web.UI.WebControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using HTP.Components;

namespace HTP.Reports
{
    /// <summary>
    /// Summary description for ReminderNotes.
    /// </summary>
    public partial class SmsNotes : System.Web.UI.Page
    {

        clsENationWebComponents _clsDb = new clsENationWebComponents();
        clsSession ClsSession = new clsSession();
        clsLogger clog = new clsLogger();
        clscalls clscall = new clscalls();
        clsBatch clsbatch = new clsBatch();
        clsLogger log = new clsLogger();
        const string ReloadCode = "<script language='javascript'> window.opener.refreshContent();self.close();   </script>";
        int _ticketID = 0, _empID = 0;
        DateTime _searchDate;
        clscalls.CallType _call;


        private void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false)
                {
                    //sending back to parent page.	
                    if (litScript.Text == "")
                    {
                        litScript.Text = ReloadCode;
                    }
                }
                _ticketID = Convert.ToInt32(Request.QueryString["casenumber"]);
                ViewState["TicketViolationIDs"] = Convert.ToString(Request.QueryString["violationID"]);
                _searchDate = Convert.ToDateTime(Request.QueryString["searchdate"]);
                string recid = Request.QueryString["Recid"];
                Session["ReminID"] = recid;
                _empID = Convert.ToInt32(ClsSession.GetCookie("sEmpID", Request));
                ViewState["_empID"] = _empID.ToString();
                Session["Updatesms"] = "0";

                _call = (clscalls.CallType)Convert.ToInt32(Request.QueryString["calltype"]);
                hfCallType.Value = _call.ToString();
                if (hfCallType.Value == "FTACall")
                {
                    txt_RComments.Visible = false;

                }
                else
                    txt_RComments.Visible = true;

                if (!IsPostBack)
                {


                    GetReminderStatus();
                    DataTable dtReminderNotes = clscall.GetReminderNotes(_ticketID, _searchDate);
                    if (dtReminderNotes.Rows.Count == 0)
                    {
                        lblMessage.Text = "Exception Occurs";
                    }
                    else
                    {
                        DataRow dRow = dtReminderNotes.Rows[0];
                        lbl_Firm.Text = Convert.ToString(dRow["FirmAbbreviation"]);
                        lbl_Name.Text = Convert.ToString(dRow["LastName"]) + ", " + Convert.ToString(dRow["firstName"]);
                        lbl_Status.Text = Convert.ToString(dRow["trialdesc"]);
                        lbl_contact1.Text = Convert.ToString(dRow["Contact1"]);
                        lbl_Contact2.Text = Convert.ToString(dRow["Contact2"]);
                        lbl_Contact3.Text = Convert.ToString(dRow["Contact3"]);
                        lbl_Language.Text = Convert.ToString(dRow["LanguageSpeak"]);
                        lbl_Bond.Visible = (dRow["bondflag"].ToString() == "1" ? true : false);
                        lblNotes.Text = dRow["GeneralComments"].ToString();

                        if (_call == clscalls.CallType.ReminderCall)
                        {
                            hdnStatus.Value = Convert.ToString(dRow["ReminderCallStatus"]);
                            lblComment.Text = Convert.ToString(dRow["ReminderComments"]);
                            td_comments.Style.Add("Display", "none");
                            divNotes.Style.Add("Display", "none");
                        }
                        else if (_call == clscalls.CallType.SetCall)
                        {
                            hdnStatus.Value = Convert.ToString(dRow["SetCallStatus"]);
                            lblComment.Text = Convert.ToString(dRow["SetCallComments"]);
                            td_comments.Style.Add("Display", "none");
                            divNotes.Style.Add("Display", "none");

                        }
                        else if (_call == clscalls.CallType.FTACall)
                        {
                            hdnStatus.Value = Convert.ToString(dRow["FTACallStatus"]);
                            lblComment.Text = Convert.ToString(dRow["FTACallComments"]);

                            td_comments.Style.Add("Display", "block");
                            divNotes.Style.Add("Display", "block");
                        }
                        else if (_call == clscalls.CallType.HMCAWDLQCalls)
                        {
                            hdnStatus.Value = Convert.ToString(dRow["HMCAWDLQCallStatus"]);

                            tr_Ccomments.Style.Add("Display", "none");
                            td_comments.Style.Add("Display", "block");
                            divNotes.Style.Add("Display", "block");
                            ddl_rStatus.Items.RemoveAt(4);
                            ddl_rStatus.Items.RemoveAt(4);
                        }

                        if (lblNotes.Text == "" || lblNotes.Text == string.Empty)
                        {
                            divNotes.Style.Add("Display", "none");
                        }

                        if (lblComment.Text == "")
                        {
                            div1.Style.Add("Display", "none");
                        }
                        ddl_rStatus.SelectedValue = Convert.ToString(hdnStatus.Value);
                        hdnStatusName.Value = ddl_rStatus.SelectedItem.ToString();



                    }

                    if (lbl_Firm.Text != "SULL")
                    {
                        lbl_outsidefirm.Visible = true;
                        lbl_Firm.Visible = true;
                    }
                    if (lbl_contact1.Text.StartsWith("(") &&
                        lbl_Contact2.Text.StartsWith("(") &&
                        lbl_Contact3.Text.StartsWith("("))
                    {
                        lbl_contact1.Text = "No contact";
                        lbl_Contact2.Visible = false;
                        lbl_Contact3.Visible = false;
                    }
                    else
                    {
                        if (lbl_contact1.Text.StartsWith("(") || lbl_contact1.Text.StartsWith("000"))
                        {
                            lbl_contact1.Visible = false;
                        }
                        if (lbl_Contact2.Text.StartsWith("(") || lbl_Contact2.Text.StartsWith("000"))
                        {
                            lbl_Contact2.Visible = false;
                        }
                        if (lbl_Contact3.Text.StartsWith("(") || lbl_Contact3.Text.StartsWith("000"))
                        {
                            lbl_Contact3.Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_update.Click += new System.EventHandler(this.btn_update_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion

        protected void btn_update_Click(object sender, EventArgs e)
        {	// Update procedure
            try
            {
                bool closeSelf = false;

                if (Session["Updatesms"] != null && Convert.ToString(Session["Updatesms"]) == "1")
                {
                    if (litScript.Text == "")
                    {
                        litScript.Text = ReloadCode;
                    }

                    Session["Updatesms"] = null;
                    return;
                }

                if (ddl_rStatus.SelectedValue == "4" && hdnFlag.Value == "1" && (_call == clscalls.CallType.SetCall || _call == clscalls.CallType.ReminderCall))
                {
                    if (_call == clscalls.CallType.SetCall)
                    {
                        lblmessageshow.Text = "Are you sure you want to activate this Flag and send a letter to the client requesting for updated contact information?";
                    }
                    else if (_call == clscalls.CallType.ReminderCall)
                    {
                        lblmessageshow.Text = "Are you sure you want to activate this Flag?";
                    }

                    ModalPopupExtender1.Show();
                    return;
                }
                else if ((hdnStatus.Value != ddl_rStatus.SelectedValue))
                {
                    if (_call == clscalls.CallType.FTACall || txt_RComments.Text.Length > 0 || _call == clscalls.CallType.HMCAWDLQCalls)
                    {
                        clscall.InsertStatus(txt_RComments.Text, Convert.ToInt32(ddl_rStatus.SelectedValue), _ticketID, _empID, Convert.ToInt32(_call), txt_Notes.Text, Convert.ToString(ViewState["TicketViolationIDs"]));

                        string description = string.Empty;

                        if (_call == clscalls.CallType.ReminderCall)
                        {
                            description = "Reminder ";
                        }
                        else if (_call == clscalls.CallType.SetCall)
                        {
                            description = "Set ";
                        }

                        else if (_call == clscalls.CallType.FTACall)
                        {
                            description = "FTA";
                        }
                        else if (_call == clscalls.CallType.HMCAWDLQCalls)
                        {
                            description = "HMCAWDLQ";
                        }

                        description += "Call Status: Changed from " + hdnStatusName.Value + " To " + ddl_rStatus.SelectedItem;
                        log.AddNote(_empID, description, "", _ticketID);
                        if (litScript.Text == "")
                        {
                            litScript.Text = ReloadCode;
                        }
                        closeSelf = false;
                    }
                    else
                    {
                        closeSelf = true;
                    }
                }

                else if ((hdnStatus.Value == ddl_rStatus.SelectedValue))
                {
                    if (_call == clscalls.CallType.FTACall || txt_RComments.Text.Length > 0 || _call == clscalls.CallType.HMCAWDLQCalls)
                    {
                        clscall.InsertStatus(txt_RComments.Text, Convert.ToInt32(ddl_rStatus.SelectedValue), _ticketID, _empID, Convert.ToInt32(_call), txt_Notes.Text, Convert.ToString(ViewState["TicketViolationIDs"]));
                        if (litScript.Text == "")
                        {
                            litScript.Text = ReloadCode;
                        }
                        closeSelf = false;
                    }
                    else
                    {
                        closeSelf = true;
                    }
                }

                if (closeSelf)
                {
                    litScript.Text = "<script language='javascript'>  self.close();   </script>";
                }
                Session["Updatesms"] = "1";
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void GetReminderStatus()
        {
            try
            {
                DataTable dtStatus = clscall.GetReminderStatus();

                if (dtStatus.Rows.Count > 0)
                {
                    ddl_rStatus.Items.Clear();
                    foreach (DataRow dr in dtStatus.Rows)
                    {
                        if (int.Parse(dr["Reminderid_PK"].ToString()) != 7)
                        {
                            ListItem item = new ListItem(dr["Description"].ToString(), dr["Reminderid_PK"].ToString());
                            ddl_rStatus.Items.Add(item);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        protected void btnok_Click(object sender, EventArgs e)
        {

            try
            {
                if (_call == clscalls.CallType.SetCall || _call == clscalls.CallType.ReminderCall)
                {
                    ClsFlags cflags = new ClsFlags();
                    cflags.TicketID = this._ticketID;
                    cflags.FlagID = Convert.ToInt32(FlagType.WrongNumber);
                    cflags.EmpID = _empID;
                    string description = "";
                    if (!cflags.HasFlag())
                    {
                        cflags.AddNewFlag();
                    }
                    if (_call == clscalls.CallType.SetCall)
                    {
                        clscall.InsertStatus(txt_RComments.Text, Convert.ToInt32(ddl_rStatus.SelectedValue),
                                             _ticketID, _empID, Convert.ToInt32(clscalls.CallType.SetCall),
                                             txt_Notes.Text, Convert.ToString(ViewState["TicketViolationIDs"]));
                        description = "Set ";
                    }
                    if (_call == clscalls.CallType.ReminderCall)
                    {
                        clscall.InsertStatus(txt_RComments.Text, Convert.ToInt32(ddl_rStatus.SelectedValue),
                                              _ticketID, _empID, Convert.ToInt32(clscalls.CallType.ReminderCall),
                                              txt_Notes.Text, Convert.ToString(ViewState["TicketViolationIDs"]));
                        description = "Reminder ";
                    }

                    description += "Call Status: Changed from " + hdnStatusName.Value + " To " + ddl_rStatus.SelectedItem;
                    log.AddNote(_empID, description, "", _ticketID);
                    if (_call == clscalls.CallType.SetCall)
                    {
                        Hashtable htable = new Hashtable();
                        htable.Add(_ticketID, _ticketID);
                        clsbatch.SendLetterToBatch(htable, BatchLetterType.SetCallLetter, _empID);
                    }
                    if (litScript.Text == "")
                    {
                        litScript.Text = ReloadCode;
                    }
                }
            }

            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void ddl_rStatus_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

    }
}

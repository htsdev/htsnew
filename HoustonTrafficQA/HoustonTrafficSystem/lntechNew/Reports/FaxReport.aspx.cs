﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;

namespace HTP.Reports
{
    public partial class FaxReport : System.Web.UI.Page
    {
        #region Variables
        faxclass fclass = new faxclass();
        clsLogger clsLog = new clsLogger();

        #endregion

        #region events
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    Session["DV_faxreport"] = null;
                    //bindgrid(-1, fromdate.SelectedDate, todate.SelectedDate);
                    //Waqas 5436 02/09/2009 //For Pending Option

                    if (!ClientScript.IsStartupScriptRegistered("javascript"))
                    {
                        //string script = "<script>document.getElementById(""fromdate"").disabled = false;</script>";
                        string script = "<script>var fromdate = document.getElementById('fromdate'); " +
                                " var todate = document.getElementById('todate'); " +
                                " fromdate.disabled = true; " +
                                " todate.disabled = true; </script>";

                        ClientScript.RegisterStartupScript(typeof(Page), "javascript", script);
                    }
                    dd_faxstatus.SelectedIndex = 1;
                }
                Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
                Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
                Pagingctrl.GridView = gv_records;

            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void gv_records_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            // Noufil 3999 06/12/2008 Show fax control
            try
            {
                clsSession cSession = new clsSession();
                if (e.CommandName == "CancelFax")
                {
                    int RowID = Convert.ToInt32(e.CommandArgument);
                    int FaxID = Convert.ToInt32(((HiddenField)gv_records.Rows[RowID].Cells[10].FindControl("hf_FaxID")).Value);
                    FaxControl.Classes.faxclass objfaxClass = new FaxControl.Classes.faxclass();
                    objfaxClass.UpdateStatusCancelFax(FaxID);

                    string Subject = "Fax cancelled from the queue which was sent on " + (((HiddenField)gv_records.Rows[RowID].Cells[10].FindControl("hf_FaxDate")).Value);
                    int Ticketid = Convert.ToInt32(((HiddenField)gv_records.Rows[RowID].Cells[10].FindControl("hf_ticketid")).Value);
                    clsLog.AddNote(Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request)), Subject, Subject, Ticketid);
                    bindgrid();
                }
                if (e.CommandName == "ticket")
                {

                    int RowID = Convert.ToInt32(e.CommandArgument);
                    Faxcontrol1.Ticketid = Convert.ToInt32(((HiddenField)gv_records.Rows[RowID].Cells[10].FindControl("hf_ticketid")).Value);
                    Faxcontrol1.Empid = Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request));
                    string path = ((HiddenField)gv_records.Rows[RowID].Cells[10].FindControl("hf_docpath")).Value;
                    string filename = System.IO.Path.GetFileName(path);
                    Faxcontrol1.Attachment = path;
                    // tahir 6318 08/06/2009 first set the fax id and then recall...
                    //Nasir 6013 07/10/2009 set fax id 
                    Faxcontrol1.FaxID = Convert.ToInt32(((HiddenField)gv_records.Rows[RowID].Cells[10].FindControl("hf_FaxID")).Value);
                    Faxcontrol1.Recall = true;
                    Faxcontrol1.GetCaseInformation();
                    ModalPopupExtender1.Show();
        
                }

            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void gv_records_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (dd_faxstatus.SelectedValue == "1" || dd_faxstatus.SelectedValue == "2")
                    {

                        gv_records.Columns[13].Visible = false;
                        if (dd_faxstatus.SelectedValue == "1")
                        {
                            gv_records.Columns[12].Visible = false;
                        }
                        else
                        {
                            gv_records.Columns[12].Visible = true;
                        }

                    }
                    else
                    {
                        gv_records.Columns[12].Visible = true;
                        gv_records.Columns[13].Visible = true;
                    }
                    ImageButton ImgBtncan = ((ImageButton)e.Row.FindControl("imgbtn_cancel"));
                    LinkButton lnkBtnInfo = ((LinkButton)e.Row.FindControl("infoimage"));


                    lnkBtnInfo.CommandArgument = e.Row.RowIndex.ToString();
                    ImgBtncan.CommandArgument = e.Row.RowIndex.ToString();
                    ImgBtncan.Attributes.Add("onclick", "return DeleteConfirm();");

                    ImgBtncan.Visible = true;
                    lnkBtnInfo.Visible = true;

                    e.Row.Cells[6].Font.Bold = true;
                    if (e.Row.Cells[6].Text == "Confirmed")
                    {
                        e.Row.Cells[6].ForeColor = System.Drawing.Color.Green;
                        ImgBtncan.Visible = false;
                        lnkBtnInfo.Visible = false;
                    }
                    else if (e.Row.Cells[6].Text == "Declined")
                    {
                        e.Row.Cells[6].ForeColor = System.Drawing.Color.Red;
                    }
                    else if (e.Row.Cells[6].Text == "Cancelled")
                    {
                        ImgBtncan.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void btn_faxsearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["faxstatus"] = dd_faxstatus.SelectedValue;
                ViewState["fromdate"] = fromdate.SelectedDate;
                ViewState["todate"] = todate.SelectedDate;
                //Waqas 5436 02/09/2009
                ViewState["alldates"] = cb_showalldates.Checked;
                bindgrid();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void gv_records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (e.NewPageIndex != -1)
                {
                    gv_records.PageIndex = e.NewPageIndex;
                    bindgrid();
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }        

        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            if (pageSize > 0)
            {
                gv_records.PageIndex = 0;
                gv_records.PageSize = pageSize;
                gv_records.AllowPaging = true;
            }
            else
            {
                gv_records.AllowPaging = false;
            }
            bindgrid();
        }

        #endregion

        #region Methods

        void Pagingctrl_PageIndexChanged()
        {
            gv_records.PageIndex = Pagingctrl.PageIndex - 1;
            bindgrid();
        }

        protected void bindgrid()
        {
            // Noufil 3999 06/12/2008 Binding Grid With Databse            
            FaxControl.Classes.faxclass objfaxClass = new FaxControl.Classes.faxclass();



            //DataTable dt = fclass.GetFaxReport(Convert.ToInt32(ViewState["faxstatus"].ToString()), Convert.ToDateTime(ViewState["fromdate"].ToString()), Convert.ToDateTime(ViewState["todate"].ToString()));
            //Waqas 5436 02/09/2009
            DataTable dt = objfaxClass.GetFaxReport(FaxControl.Settings.ProgramSource.Houston, Convert.ToInt32(ViewState["faxstatus"].ToString()), Convert.ToDateTime(ViewState["fromdate"].ToString()), Convert.ToDateTime(ViewState["todate"].ToString()), Convert.ToInt16(ViewState["alldates"]));

            if (dt.Rows.Count > 0)
            {
                lbl_Message.Visible = false;
                gv_records.Visible = true;                                                

                int sno = 1;
                if (!dt.Columns.Contains("sno"))
                    dt.Columns.Add("sno");


                if (dt.Rows.Count >= 1)
                    dt.Rows[0]["sno"] = 1;

                if (dt.Rows.Count >= 2)
                {
                    for (int i = 1; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[i - 1]["ticketid"].ToString() != dt.Rows[i]["ticketid"].ToString())
                        {
                            dt.Rows[i]["sno"] = ++sno;
                        }
                    }
                }

                
                gv_records.DataSource = dt.DefaultView;
                gv_records.DataBind();

                Pagingctrl.PageCount = gv_records.PageCount;
                Pagingctrl.PageIndex = gv_records.PageIndex;
                Pagingctrl.SetPageIndex();
            }
            else
            {
                lbl_Message.Visible = true;
                gv_records.Visible = false;
                lbl_Message.Text = "No Records Found";
            }
        }

        #endregion
    }
}
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HCCCriminalreport.aspx.cs"
    Inherits="HTP.Reports.HCC_Criminalreport" %>

<%@ Register Src="../WebControls/MenuTop.ascx" TagName="MenuTop" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc3" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>HCC Criminal Report</title>
    <link href="../Styles.css" type="text/css" rel="stylesheet" />

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>

    <script src="../Scripts/Dates.js" type="text/javascript"></script>

    <style type="text/css">
        .clsInputadministration
        {
            border-right: #3366cc 1px solid;
            border-top: #3366cc 1px solid;
            font-size: 8pt;
            border-left: #3366cc 1px solid;
            width: 90px;
            color: #123160;
            border-bottom: #3366cc 1px solid;
            font-family: Tahoma;
            background-color: white;
            text-align: left;
        }
        .clsInputCombo
        {
            border-right: #e52029 1px solid;
            border-top: #e52029 1px solid;
            font-weight: normal;
            list-style-position: outside;
            font-size: 8pt;
            border-left: #e52029 1px solid;
            color: #123160;
            border-bottom: #e52029 1px solid;
            font-family: Tahoma;
            list-style-type: square;
            background-color: white;
        }
        .clsLabel
        {
            font-family: Tahoma;
            font-size: 8pt;
            color: #123160;
            border-bottom-width: 0;
            border-left-width: 0;
            border-right-width: 0;
            border-top-width: 0;
            text-align: left;
        }
        A:link
        {
            font-size: 8pt;
            font-family: Verdana,Arial, Helvetica, sans-serif;
            color: #4169E1;
            text-decoration: none;
            font-weight: bold;
        }
        A:Visited
        {
            font-size: 8pt;
            font-family: Verdana,Arial, Helvetica, sans-serif;
            color: #4169E1;
            text-decoration: none;
            font-weight: bold;
        }
    </style>

    <script type="text/javascript">

function OpenPopup(bookingnumber,casenumber)
{
    window.open("HCCCriminalPopup.aspx?bookingnumber="+bookingnumber + "&casenumber=" +casenumber ,'','resizable=no,scrollbars=yes,menubars=no,toolbars=no,height=650,width=790');
    return false;
}


function ClearForm()
{


document.getElementById('txt_causenumber').value='';
document.getElementById('txt_LastName').value='';
document.getElementById('txt_FirstName').value='';
document.getElementById('txt_spn').value='';
document.getElementById('ddl_cdi').selectedIndex = 0;
document.getElementById('drp_Attorney').selectedIndex = 0;
document.getElementById('txt_mm').value='';
document.getElementById('txt_dd').value='';
document.getElementById('txt_yy').value='';
document.getElementById('txt_courtroomnew').value='';
document.getElementById('ddl_Level').selectedIndex = 0;
// Agha Usman 4239 06/25/2008
document.getElementById('ddlPagesize').selectedIndex = 2;
document.getElementById('ddl_disposition').selectedIndex = 0;
document.getElementById ('cal_recdate').value='';
document.getElementById ('cal_Torecdate').value='';
document.getElementById ('cl_st_setting_date').value='';
document.getElementById ('cl_end_setting_date').value='';
document.getElementById ('txt_MatterDescription').value='';
if(document.getElementById ('tblresult')!=null)
{
    document.getElementById ('tblresult').style .display ="none";
}


return(false);
}

function ValidateForm()
{
    //var var1 = document.getElementById('txt_FirstName');
    //var var2 = document.getElementById('txt_LastName');
    //var chk=document.getElementById("cb_dae");
	var birthmonth=document.getElementById("txt_mm").value;
	var birthdate=document.getElementById("txt_dd").value;
	var birthyear=document.getElementById("txt_yy").value;

//    if(birthmonth == "")
//	{ 
//		alert("Date of birth month can not be empty");
//		document.getElementById("txt_mm").focus();
//		return false;
//	}
	
	if(isNaN(birthmonth) == true)
	{
	alert("The month must be a number");
	document.getElementById("txt_mm").focus();
	return false;
	
	}
//	if(birthdate == "")
//	{ 
//		alert("Date of birth date can not be empty");
//		document.getElementById("txt_dd").focus();
//		return false;
//	}
	if(isNaN(birthdate) == true)
	{
	alert("The day must be a number")
	document.getElementById("txt_dd").focus();
	return false;
	
	}
//	if(birthyear == "")
//	{ 
//		alert("Date of birth year can not be empty");
//		document.getElementById("txt_yy").focus();
//		return false;
//	}
 	if(isNaN(birthyear) == true)
	{
	alert("The year must be a number");
	document.getElementById("txt_yy").focus();
	return false;
	}
	if((birthmonth == "") && (birthdate == "") && (birthyear == ""))
	{
	    //no problem, empty date is allowed
	}
	else
	{			
	   
	    if( isvaliddate (birthyear, birthdate, birthmonth   ) == false )
        {
        alert('Invalid date of birth entered.\nPlease enter date in MM/DD/YYYY format.');
        document.getElementById("txt_dd").focus();
        return false;
        }
    }
   
    return true;
		    
}
    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <aspnew:ScriptManager ID="scriptManager" runat="server">
    </aspnew:ScriptManager>
    <div>
        <table cellspacing="0" cellpadding="0" width="780" align="center" border="0">
            <tr>
                <td>
                    <uc3:ActiveMenu ID="ActiveMenu1" runat="server" />
                </td>
            </tr>
            <tr>
                <td width="100%" background="../Images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="lbl_Message" runat="server" ForeColor="Red" CssClass="clsLabel" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                        <tr>
                            <td class="clsleftpaddingtable" colspan="" style="height: 39px">
                                &nbsp; &nbsp; &nbsp; &nbsp;<table align="center" style="width: 100%" cellpadding="0"
                                    cellspacing="0">
                                    <tr>
                                        <td align="left" class="clssubhead" style="width: 108px">
                                            CDI:
                                        </td>
                                        <td>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:DropDownList ID="ddl_cdi" runat="server"
                                                CssClass="clsInputCombo" Width="154px">
                                                <asp:ListItem Selected="True">-- Choose  --</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td class="clssubhead">
                                            Last Name:
                                        </td>
                                        <td>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="txt_LastName" runat="server"
                                                CssClass="clsInputadministration" Width="152px"></asp:TextBox>
                                        </td>
                                        <tr>
                                            <td align="left" class="clssubhead" style="width: 108px; height: 21px;">
                                                Cause Number:
                                            </td>
                                            <td>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="txt_causenumber" runat="server"
                                                    CssClass="clsInputadministration" Width="152px"></asp:TextBox>
                                            </td>
                                            <td class="clssubhead">
                                                First Name:
                                            </td>
                                            <td>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="txt_FirstName" runat="server"
                                                    CssClass="clsInputadministration" Width="150px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" class="clssubhead" style="width: 108px">
                                                SPN:
                                            </td>
                                            <td>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="txt_spn" runat="server"
                                                    CssClass="clsInputadministration" Width="152px"></asp:TextBox>
                                            </td>
                                            <td class="clssubhead">
                                                Matter Description:
                                            </td>
                                            <td>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="txt_MatterDescription"
                                                    runat="server" CssClass="clsInputadministration" Width="150px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" class="clssubhead" style="width: 108px; height: 25px;">
                                                Court Room:
                                            </td>
                                            <td style="height: 25px">
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="txt_courtroomnew" runat="server"
                                                    CssClass="clsInputadministration" Width="152px" MaxLength="5"></asp:TextBox>
                                            </td>
                                            <td class="clssubhead" style="height: 25px">
                                                Attorney:
                                            </td>
                                            <td>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:DropDownList ID="drp_Attorney" runat="server"
                                                    CssClass="clsInputCombo" Width="153px">
                                                    <asp:ListItem Selected="True" Value="-1">-- Choose  --</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" class="clssubhead" style="width: 108px">
                                                Level:
                                            </td>
                                            <td>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:DropDownList ID="ddl_Level" runat="server"
                                                    CssClass="clsInputCombo">
                                                    <asp:ListItem Value="-1" Selected="True">-- Choose --</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td class="clssubhead">
                                                Disposition:
                                            </td>
                                            <td>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:DropDownList ID="ddl_disposition"
                                                    runat="server" CssClass="clsInputCombo" Width="153px">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" class="clssubhead" style="width: 108px; height: 34px;" valign="middle">
                                                Next Setting Date:
                                            </td>
                                            <td style="height: 34px;">
                                                <table style="width: 100%">
                                                    <tr>
                                                        <td class="clsLabel" style="width: 121px">
                                                            From
                                                            <ew:CalendarPopup ID="cl_st_setting_date" runat="server" Width="70px" ImageUrl="../images/calendar.gif"
                                                                Font-Names="Tahoma" Font-Size="8pt" ControlDisplay="TextBoxImage" CalendarLocation="Bottom"
                                                                ShowGoToToday="True" AllowArbitraryText="False" Culture="(Default)" UpperBoundDate="12/31/9999 23:59:00"
                                                                PadSingleDigits="True" ToolTip="Select Report Date Range" Style="position: relative"
                                                                Nullable="True" VisibleDate="2008-04-07">
                                                                <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                                                <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                                    BackColor="White"></WeekdayStyle>
                                                                <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                    ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                                                <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                                                    BackColor="AntiqueWhite"></OffMonthStyle>
                                                                <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                    ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                                                <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                                    BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                                                <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                    ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                                                <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                                    BackColor="LightGray"></WeekendStyle>
                                                                <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                    ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                                                <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                    ForeColor="Black" BackColor="White"></ClearDateStyle>
                                                                <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                                    BackColor="White"></HolidayStyle>
                                                            </ew:CalendarPopup>
                                                        </td>
                                                        <td class="clsLabel" align="right">
                                                            To
                                                        </td>
                                                        <td>
                                                            <ew:CalendarPopup ID="cl_end_setting_date" runat="server" Width="70px" ImageUrl="../images/calendar.gif"
                                                                Font-Names="Tahoma" Font-Size="8pt" ControlDisplay="TextBoxImage" CalendarLocation="Bottom"
                                                                ShowGoToToday="True" AllowArbitraryText="False" Culture="(Default)" UpperBoundDate="12/31/9999 23:59:00"
                                                                PadSingleDigits="True" ToolTip="Select Report Date Range" Style="position: relative"
                                                                Nullable="True" VisibleDate="2008-04-07">
                                                                <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                                                <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                                    BackColor="White"></WeekdayStyle>
                                                                <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                    ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                                                <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                                                    BackColor="AntiqueWhite"></OffMonthStyle>
                                                                <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                    ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                                                <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                                    BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                                                <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                    ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                                                <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                                    BackColor="LightGray"></WeekendStyle>
                                                                <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                    ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                                                <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                    ForeColor="Black" BackColor="White"></ClearDateStyle>
                                                                <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                                    BackColor="White"></HolidayStyle>
                                                            </ew:CalendarPopup>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="height: 34px;" class="clssubhead">
                                                Search By Load Date:
                                            </td>
                                            <td style="height: 34px">
                                                <table style="width: 100%">
                                                    <tr>
                                                        <td class="clsLabel" style="width: 121px">
                                                            From&nbsp;<ew:CalendarPopup ID="cal_recdate" runat="server" Width="70px" ImageUrl="../images/calendar.gif"
                                                                Font-Names="Tahoma" Font-Size="8pt" ControlDisplay="TextBoxImage" CalendarLocation="Bottom"
                                                                ShowGoToToday="True" AllowArbitraryText="False" Culture="(Default)" UpperBoundDate="12/31/9999 23:59:00"
                                                                PadSingleDigits="True" ToolTip="Select Report Date Range" Style="position: relative"
                                                                VisibleDate="2008-04-07" Nullable="True">
                                                                <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                    Font-Size="XX-Small" ForeColor="Black" />
                                                                <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                    ForeColor="Black" />
                                                                <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                    ForeColor="Black" />
                                                                <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                    ForeColor="Black" />
                                                                <TextboxLabelStyle CssClass="clstextarea" />
                                                                <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                    Font-Size="XX-Small" ForeColor="Black" />
                                                                <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                    ForeColor="Black" />
                                                                <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                    ForeColor="Black" />
                                                                <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                    Font-Size="XX-Small" ForeColor="Gray" />
                                                                <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                    ForeColor="Black" />
                                                                <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                    Font-Size="XX-Small" ForeColor="Black" />
                                                            </ew:CalendarPopup>
                                                        </td>
                                                        <td class="clsLabel" align="right">
                                                            To
                                                        </td>
                                                        <td>
                                                            <ew:CalendarPopup ID="cal_Torecdate" runat="server" Width="70px" ImageUrl="../images/calendar.gif"
                                                                Font-Names="Tahoma" Font-Size="8pt" ControlDisplay="TextBoxImage" CalendarLocation="Bottom"
                                                                ShowGoToToday="True" AllowArbitraryText="False" Culture="(Default)" UpperBoundDate="12/31/9999 23:59:00"
                                                                PadSingleDigits="True" ToolTip="Select Report Date Range" Style="position: relative"
                                                                VisibleDate="2008-04-07" Nullable="True">
                                                                <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                    Font-Size="XX-Small" ForeColor="Black" />
                                                                <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                    ForeColor="Black" />
                                                                <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                    ForeColor="Black" />
                                                                <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                    ForeColor="Black" />
                                                                <TextboxLabelStyle CssClass="clstextarea" />
                                                                <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                    Font-Size="XX-Small" ForeColor="Black" />
                                                                <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                    ForeColor="Black" />
                                                                <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                    ForeColor="Black" />
                                                                <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                    Font-Size="XX-Small" ForeColor="Gray" />
                                                                <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                    ForeColor="Black" />
                                                                <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                    Font-Size="XX-Small" ForeColor="Black" />
                                                            </ew:CalendarPopup>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" class="clssubhead" style="width: 108px; height: 34px;" valign="middle">
                                                DOB:
                                            </td>
                                            <td style="height: 34px;">
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:TextBox ID="txt_mm" runat="server" CssClass="clsInputadministration" MaxLength="2"
                                                    onkeyup="return autoTab(this, 2, event)" Width="18px"></asp:TextBox>
                                                /<asp:TextBox ID="txt_dd" runat="server" CssClass="clsInputadministration" MaxLength="2"
                                                    onkeyup="return autoTab(this, 2, event)" Width="18px"></asp:TextBox>
                                                /<asp:TextBox ID="txt_yy" runat="server" CssClass="clsInputadministration" MaxLength="4"
                                                    Width="30px"></asp:TextBox>
                                            </td>
                                            <td style="height: 34px;" class="clssubhead">
                                                No of Records:</td>
                                            <td style="height: 34px">
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                &nbsp;<asp:DropDownList ID="ddlPagesize" runat="server" CssClass="clssubhead" 
                                                    Width="153px">
                                                            <asp:ListItem>250</asp:ListItem>
                                                            <asp:ListItem>500</asp:ListItem>
                                                            <asp:ListItem Selected="True">1000</asp:ListItem>
                                                            <asp:ListItem>1500</asp:ListItem>
                                                            <asp:ListItem>2000</asp:ListItem>
                                                            <asp:ListItem>3000</asp:ListItem>
                                                        </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" class="clssubhead" colspan="4" style="height: 22px">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" class="clssubhead" style="width: 108px; height: 22px;">
                                            </td>
                                            <td style="height: 22px;" align="right">
                                                <asp:Button ID="btn_save_cdi" runat="server" CssClass="clsbutton" Text="Search" OnClick="btn_save_cdi_Click"
                                                    Width="50px" OnClientClick="return ValidateForm();"></asp:Button>
                                            </td>
                                            <td colspan="1" style="height: 22px;">
                                                &nbsp;<asp:Button ID="btn_Reset_cdi" runat="server" CssClass="clsbutton" Text="Reset"
                                                    Width="50px" OnClientClick="return ClearForm();" OnClick="btn_Reset_cdi_Click">
                                                </asp:Button>
                                            </td>
                                            <td colspan="1" style="height: 22px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="4">
                                                &nbsp;
                                            </td>
                                        </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table id="tblresult" border="0" cellpadding="0" cellspacing="0" style="width: 100%;
                        height: 100%" runat="server" visible="false">
                        <tr>
                            <td style="height: 32px" valign="middle" background="../Images/subhead_bg.gif">
                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                    <tr>
                                        <td style="width: 50%" class="clssubhead">
                                            &nbsp;Resultt
                                        </td>
                                        <td style="width: 50%" align="right">
                                            <table id="tblPageNavigation" runat="server" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td align="right" class="clssubhead">
                                                        Page # &nbsp;
                                                        </td>
                                                    <td align="left">
                                                        <asp:Label ID="lblCurrPage" runat="server" CssClass="clssubhead" Width="25px"></asp:Label>
                                                    </td>
                                                    <td align="right" class="clssubhead">
                                                        Goto &nbsp;
                                                    </td>
                                                    <td align="right">
                                                        <asp:DropDownList ID="ddlPageNo" runat="server" AutoPostBack="True" CssClass="clssubhead"
                                                            OnSelectedIndexChanged="ddlPageNo_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="DG_Searchresult" runat="server" AutoGenerateColumns="False" CssClass="clsLeftPaddingTable"
                                    BackColor="#EFF4FB" BorderColor="White" BorderStyle="None" Font-Bold="False"
                                    Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Height="109px"
                                    AllowPaging="True" OnPageIndexChanging="DG_Searchresult_PageIndexChanging" OnRowDataBound="DG_Searchresult_RowDataBound"
                                    Width="100%" PageSize="50">
                                    <Columns>
                                        <asp:TemplateField HeaderText="S#">
                                            <HeaderStyle CssClass="GrdHeader" Width="3%" Font-Size="XX-Small" />
                                            <ItemTemplate>
                                                <asp:HyperLink ID="hlnkSNo" runat="server" NavigateUrl="#" Text='<%# DataBinder.Eval(Container.DataItem, "SNo") %>'></asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="LastName">
                                            <HeaderStyle CssClass="GrdHeader" Width="12%" Font-Size="XX-Small" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblLastName" CssClass="clsLabel" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "lastName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="First Name (Middle)">
                                            <HeaderStyle CssClass="GrdHeader" Width="14%" Font-Size="XX-Small" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_fname" runat="server" CssClass="clsLabel" Font-Size="XX-Small"
                                                    Text='<%# DataBinder.Eval(Container.DataItem, "FirstName") %>'></asp:Label>
                                                <asp:Label ID="lbl_middlename" runat="server" CssClass="clsLabel" Font-Size="XX-Small"
                                                    Text='<%# DataBinder.Eval(Container.DataItem, "MiddleName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="SPN">
                                            <HeaderStyle CssClass="GrdHeader" Font-Size="XX-Small" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_spn" CssClass="clsLabel" Font-Size="XX-Small" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Span") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Cause Number">
                                            <HeaderStyle CssClass="GrdHeader" Font-Size="XX-Small" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_causeno" CssClass="clsLabel" Font-Size="XX-Small" runat="server"
                                                    Text='<%# DataBinder.Eval(Container.DataItem, "casenumber") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Violation Description">
                                            <HeaderStyle CssClass="GrdHeader" Width="20%" Font-Size="XX-Small" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_vio_desc" runat="server" Font-Size="XX-Small" CssClass="clsLabel"
                                                    Text='<%# DataBinder.Eval(Container.DataItem, "ViolationDescription") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="LVL">
                                            <HeaderStyle CssClass="GrdHeader" Font-Size="XX-Small" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_crglevel" CssClass="clsLabel" runat="server" Font-Size="XX-Small"
                                                    Text='<%# DataBinder.Eval(Container.DataItem, "Chargelevel") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="CRT RM">
                                            <HeaderStyle CssClass="GrdHeader" Font-Size="XX-Small" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_crtroom" CssClass="clsLabel" Font-Size="XX-Small" runat="server"
                                                    Text='<%# DataBinder.Eval(Container.DataItem, "CourtRoom") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Floor">
                                            <HeaderStyle CssClass="GrdHeader" Font-Size="XX-Small" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_floor" runat="server" Font-Size="XX-Small" CssClass="clsLabel"
                                                    Text='<%# DataBinder.Eval(Container.DataItem, "Floor") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Next Set">
                                            <HeaderStyle CssClass="GrdHeader" Font-Size="XX-Small" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_n_date" runat="server" Font-Size="XX-Small" CssClass="clsLabel"
                                                    Text='<%# DataBinder.Eval(Container.DataItem, "NextSettingDate","{0:d}" ) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Reason">
                                            <HeaderStyle CssClass="GrdHeader" Font-Size="XX-Small" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_reason" runat="server" CssClass="clsLabel" Font-Size="XX-Small"
                                                    Text='<%# DataBinder.Eval(Container.DataItem, "SettingReason") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Disp">
                                            <HeaderStyle CssClass="GrdHeader" Font-Size="XX-Small" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_dispo" runat="server" CssClass="clsLabel" Font-Size="XX-Small"
                                                    Text='<%# DataBinder.Eval(Container.DataItem, "Disposition") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Rec Date">
                                            <HeaderStyle CssClass="GrdHeader" Font-Size="XX-Small" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_attorny" CssClass="clsLabel" runat="server" Font-Size="XX-Small"
                                                    Text='<%# DataBinder.Eval(Container.DataItem, "RecDate","{0:d}") %>'></asp:Label>
                                                <asp:HiddenField ID="HFBookingNumber" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "BookingNumber") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Atty">
                                            <HeaderStyle CssClass="GrdHeader" Font-Size="XX-Small" />
                                            <ItemTemplate>
                                                <asp:Image ID="imgRight" runat="server" ImageUrl="../Images/right.gif" Visible="false" />
                                                <asp:Image ID="imgCross" runat="server" ImageUrl="../Images/cross.gif" Visible="false" />
                                                <asp:HiddenField ID ="hfConnection" runat ="server" Value='<%#bind("connection") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerSettings Mode="NextPrevious" NextPageText="Next &gt;" PreviousPageText="&lt; Previous" />
                                    <PagerStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="100%" background="../Images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td>
                    <uc1:Footer ID="Footer1" runat="server" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

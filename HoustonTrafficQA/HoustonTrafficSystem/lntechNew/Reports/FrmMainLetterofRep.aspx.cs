using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using lntechNew.Components;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using System.IO;
using CrystalDecisions.CrystalReports.Engine;

//Nasir 5590 03/17/2009 change namespace lntechnew to HTP
namespace HTP.Reports
{
    /// <summary>
    /// Summary description for main.
    /// </summary>
    /// 

    public partial class FrmMainLetterofRep : System.Web.UI.Page
    {
        int ticketno;
        int empid;
        int lettertype;
        //int MyCount;
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        protected System.Web.UI.WebControls.ImageButton IBtn;
        clsSession uSession = new clsSession();
        clsLogger bugTracker = new clsLogger();
        private void Page_Load(object sender, System.EventArgs e)
        {
            
            try
            {
                if (!Page.IsPostBack)
                {
                    //ticketno =Convert.ToInt32(uSession.GetCookie("sTicketID",this.Request));	
                    ticketno = Convert.ToInt32(Request.QueryString["casenumber"]);
                    ViewState["vTicketId"] = ticketno;
                    // Noufil 3999 05/09/2008 Setting ticketid in session
                    Session["ticketid"] = ticketno;
                    
                    // Zahoor 4665 09/02/2008 :To cover the cookies expiraiton issue
                    if (uSession.GetCookie("sEmpID", this.Request) != "") 
                    {
                        empid = Convert.ToInt32(uSession.GetCookie("sEmpID", this.Request));
                        // Abid Ali 5359 2/10/2009 sent to batch
                        ViewState["vBatch"] = (Request.QueryString["batch"] == null ? false.ToString() : Request.QueryString["batch"].ToString());
                        lettertype = Convert.ToInt32(Request.QueryString["lettertype"]);
                        //Nasir 5590 03/13/2009  change page title
                        if (lettertype == 7)
                        {
                            PageTitle.InnerText = "Motion in Limine";
                            tdRepHead.InnerText = "Motion in Limine";
                        }
                        if (lettertype == 8)
                        {
                            PageTitle.InnerText = "Motion for Discovery";
                            tdRepHead.InnerText = "Motion for Discovery";
                        }
                        ViewState["Vempid"] = empid;
                        ViewState["vLetterType"] = lettertype;
                        //Fahad 5172 11/26/2008
                        int MyCount = clsCourts.GetLORMethodByCase(ticketno);                        
                        hfLORMethodCount.Value = MyCount.ToString();
                        // Abid Ali 5359 1/1/2009 Close when sent to batch
                        Session["CertifiedMailNumber"] = Request.QueryString["CMN"];
                        if (ViewState["vBatch"] != null && Convert.ToBoolean(ViewState["vBatch"]))
                        {
                            Response.Write("<script language='javascript'> window.close();   </script>");
                        }
                    }
                    else
                    { // Zahoor 4665 09/16/08
                        HttpContext.Current.Response.Write("<script language='javascript'> alert('Session has been expired. please login Again');   </script>");
                        HttpContext.Current.Response.Write("<script language='javascript'> window.close();   </script>");
                    }          
                    // 4665
                   
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

            }
        }


        public string GetPageContent(string pagename)
        {
            try
            {
                //Creating Memory Stream			
                MemoryStream m = new MemoryStream();
                //Create Stream Writer to Write Contents of Recipt Page
                StreamWriter wr = new StreamWriter(m);
                //Getting Recipt Page
                HttpContext.Current.Server.Execute(pagename, wr);
                // Setting Memory Stream at the start		
                m.Position = 0;
                // Initializing Message String
                string receiptContent = "";
                //Creating Stream Reader to Read Contents from Memory Strean
                StreamReader s = new StreamReader(m);
                // Getting the entire page from the string
                receiptContent += s.ReadToEnd();
                s.Close();

                return receiptContent;
            }                
            catch (Exception ex)
            {
                //ozair 5590 03/17/2009 exception logged in errorlog
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return "";
            }

        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.IBtn.Click += new System.Web.UI.ImageClickEventHandler(this.IBtn_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion

        /// <summary>
        /// Export to word and maintain history
        /// </summary>
        public void CreateApprovalReport()
        {
            try
            {

                clsCrsytalComponent Cr = new clsCrsytalComponent();                
                // Noufil 4908 10/06/2008 Using ViewState to get values in order not to effect Zeeshan zahoor's work
                string[] key = { "@ticketid", "@empid" };
                object[] value1 = { Convert.ToInt32(ViewState["vTicketId"]), Convert.ToInt32(ViewState["Vempid"]) };
                lettertype = Convert.ToInt32(ViewState["vLetterType"]);                
                string filename;
                //ozair 5700 03/31/2009  added to maintain history
                bool isPrinted = false;
                string note = String.Empty;
                if (lettertype == 6)
                {
                    filename = Server.MapPath("") + "\\Wordreport.rpt";
                    Cr.CreateReportWord(filename, "USP_HTS_LETTER_OF_REP", key, value1, "false", lettertype, empid, this.Session, this.Response);
                    //Ozair 5700 03/31/2009 added to maintain history
                    isPrinted = true;
                    note = "Letter of Rep Printed with Changes";                    
                }
                else if (lettertype == 7)
                {
                    filename = Server.MapPath("") + "\\Motion_Limine.rpt";
                    Cr.CreateReportWord(filename, "USP_HTS_MOTION_LIMINE", key, value1, "false", lettertype, empid, this.Session, this.Response);
                    //Ozair 5700 03/31/2009 added to maintain history
                    isPrinted = true;
                    note = "Motion in Limine Printed with Changes";
                }
                else if (lettertype == 8)
                {
                    filename = Server.MapPath("") + "\\Motion_Discovery.rpt";
                    Cr.CreateReportWord(filename, "USP_HTS_MOTION_DISCOVERY", key, value1, "false", lettertype, empid, this.Session, this.Response);
                    //Ozair 5700 03/31/2009 added to maintain history
                    isPrinted = true;
                    note = "Motion for Discovery Printed with Changes";
                }

                //ozair 5700 03/31/2009  added to maintain history
                if (isPrinted)
                {
                    bugTracker.AddNote(Convert.ToInt32(ViewState["Vempid"]), note, note, Convert.ToInt32(ViewState["vTicketId"]));
                }

            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void IBtn_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            CreateApprovalReport();
        }

       
    }
}

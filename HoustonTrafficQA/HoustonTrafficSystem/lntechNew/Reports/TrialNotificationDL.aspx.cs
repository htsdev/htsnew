using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.IO;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using lntechNew.Components;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using System.Windows.Forms;



namespace HTP.Reports
{
	/// <summary>
    /// Summary description for TrialNotificationDL.
	/// </summary>
	public partial class TrialNotificationDL : System.Web.UI.Page
	{		
		clsCrsytalComponent Cr = new clsCrsytalComponent();				
		clsSession uSession = new clsSession();
		clsLogger clog = new clsLogger();
        clsCase cCase = new clsCase();
        clsENationWebComponents ClsDb = new clsENationWebComponents(); 
		
		int TicketIDList;
        int SelectedReport;
		int empid;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
                // get ticket id and report to display
				TicketIDList = Convert.ToInt32(Request.QueryString["casenumber"]);
                SelectedReport = Convert.ToInt32(Request.QueryString["reportnumber"]); ;
				ViewState["vTicketId"]=TicketIDList;
                ViewState["vSelectedReport"] = SelectedReport;
                
                // get emp id
				empid =Convert.ToInt32(uSession.GetCookie("sEmpID",this.Request));									
                
                
                if (!IsPostBack)
                {
                    if (SelectedReport == 1)
                    {
                        lblTitle.Text = "Trial Notification - DL Suspension Letter";
                    }
                    else
                    {
                        lblTitle.Text = "Trial Notification - Occupation DL Letter";
                    }
                }
			}
			catch(Exception ex)
			{
				
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{   			
			this.IBtn.Click += new System.Web.UI.ImageClickEventHandler(this.IBtn_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		//To create Word Report
		public void CreateReportWord()
		{
			try
			{
                int violationNumber = (SelectedReport == 1) ? 16160 : 18534;
                string[] key2 = { "@TicketID", "@EmployeeID", "@ViolationNumber" };
                object[] value12 = { TicketIDList, empid, violationNumber };

                string filename = Server.MapPath("") + "\\Trial_Letter_DL.rpt";
                Cr.CreateReportWord(filename, "USP_HTP_TrialletterDL", key2, value12, this.Session, this.Response);
			}
			catch(Exception ex)
			{
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
				Response.Write(ex.Message);
			}
		}


		private void IBtn_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			CreateReportWord(); 
		}
	}
}

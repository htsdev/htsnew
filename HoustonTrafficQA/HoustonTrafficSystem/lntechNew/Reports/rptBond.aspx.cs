using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using lntechNew.Components;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;
//ozair 3643 on 05/01/2008
using HTP.Components;
//end ozair 3643

namespace HTP.Reports
{
	/// <summary>
	/// Summary description for rptBond.
	/// </summary>

	public partial class rptBond : System.Web.UI.Page
	{
        #region Variables

		clsCrsytalComponent Cr = new clsCrsytalComponent();
		clsSession uSession = new clsSession();
        //ozair 3643 on 05/01/2008
        clsDocumentTracking clsDT = new clsDocumentTracking();
        int documentBatchID = 0;
        //end ozair 3643
        int ticketno;
        int empid;
        int CourtType;
        int LetterType = 4;

        #endregion

        #region Events
        
        private void Page_Load(object sender, System.EventArgs e)
		{		
			ticketno =Convert.ToInt32(Request.QueryString["casenumber"]);
			empid =Convert.ToInt32(uSession.GetCookie("sEmpID",this.Request));

            // tahir 4477 07/29/2008
            // replaced the session variable with query string...
            CourtType = Convert.ToInt32(Request.QueryString["CourtType"]);

            //ozair 3643 on 05/01/2008 implemented the logic for document ID.
            clsDT.BatchDate = DateTime.Now;
            clsDT.DocumentID = 1; // 1: Bonds
            clsDT.TicketIDs = ticketno.ToString() + ",";
            clsDT.EmpID = empid;
            //get the document id for this report
            documentBatchID = clsDT.GetDBIDByInsertingBatchWithDetail();
            if (documentBatchID > 0)
            {
                //For Inside Court
                if (CourtType == 0)
                {
                    CreateBondReport();
                }
                else
                {
                    CreateBondSuretyReport();
                }
            }
            //end ozair 3643
            
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion

        #endregion

        #region Methods

        public void CreateBondReport() 
		{
            //ozair 3643 on 05/01/2008 implemented the logic to print document id
            string[] keyMain = { "@TicketIDList", "@empid", "@DocumentBatchID" };
            object[] valueMain = { ticketno, empid, documentBatchID };
			//end ozair 3643
            string[] keySub = { "@TicketID"};
            object[] valueSub = { ticketno};			

			string filename  = Server.MapPath("") + "\\Bond.rpt";

            //ozair 3643 on 05/01/2008 implemented the logic to print document id
            Cr.CreateBondReport(filename, "sp_BondLetter", "USP_HTS_LOR", keyMain, valueMain, keySub, valueSub, LetterType, this.Session, this.Response, documentBatchID);
            //end ozair 3643
            clsENationWebComponents clsDB = new clsENationWebComponents();
           
            string[] key ={ "@Ticketid_pk" };
            object[] value1 ={ ticketno };
            clsDB.ExecuteSP("USP_HTS_Remove_BondFlagConfirmedFlag", key, value1);


		}

		public void CreateBondSuretyReport()
        {
            //ozair 3643 on 05/01/2008 implemented the logic to print document id
            string[] key = { "@TicketIDList", "@empid", "@DocumentBatchID" };
            object[] value1 = { ticketno, empid, documentBatchID };
            //end ozair 3643
			string filename  = Server.MapPath("") + "\\Bond_Surety.rpt";

            //ozair 3643 on 05/01/2008 implemented the logic to print document id
            Cr.CreateReport(filename, "sp_BondLetter", key, value1, LetterType, this.Session, this.Response, documentBatchID);
            //end ozair 3643
        }

        #endregion
        
	}
}


<%@ Page Language="C#" AutoEventWireup="true" Codebehind="GeneralCommentsPopup.aspx.cs"
    Inherits="HTP.Reports.GeneralCommentsPopup" %>

<%@ Register Assembly="WCtl_Comments" Namespace="LNTech.CommentControl" TagPrefix="cc2" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Service Ticket Window</title>
    <script src="../Scripts/Dates.js" type="text/javascript"></script>
    <script src="../Scripts/jsDate.js" type="text/javascript"></script>
    <link href="../Styles.css" type="text/css" rel="stylesheet"/>

    <script language="javascript">
        function Validate()
        {            
            if(document.getElementById("ddl_per").value==-1)
            { 
                  alert("Please select percentage completion ");
                return false;
           
            }
            //added by kahlid  task 2595
            if(document.getElementById("drpCategory").value==-1)
            {
            alert("Please select a category");
            return false;
            }
                        
            var generalCheck = document.getElementById("cb_gencomments");
            var serviceCheck =  document.getElementById("cb_serinstruction");
            var assignto = document.getElementById("ddl_assignto");
            var showdocket = document.getElementById("cb_showondocket");
            var btnSubmit = document.getElementById("btnSubmit");
            var tblwait  =  document.getElementById("tblwait");
            
            //Sabir Khan 5738 06/01/2009 Check business days for follow up date date...
            //Yasir Kamal 7412 02/16/2010 do not check follow up date for 100% tickets.
            if(document.getElementById("ddl_per").value !="100")
            {
                var IsValidFollowupdate =  CheckDate(document.getElementById("calfollowupdate").value, "calfollowupdate");
                    if(IsValidFollowupdate == false)
                        {
                            return false;
                        }
            }
            
            if(showdocket.checked == true)
            {
                        
                //If Attorney is assigned than user must select atleat one comment to be display on docket report                        
                if ( assignto.selectedIndex != 0  && ( generalCheck.checked  == false  && serviceCheck.checked == false ) )
                     {
                        alert("Please select general comment or service ticket instruction to be display on the docket report.");
                        return false;
                     }
                     else
                     {
                        btnSubmit.style.display = "none";
                        tblwait.style.display = "block";
                        return true;
                     }
             }
             else
             {
                    btnSubmit.style.display = "none";
                    tblwait.style.display = "block";
                    return true;
             }

          
                
                if ( serviceCheck.checked && document.getElementById("tb_serinstruction").value=="")
                {
                    alert("Please Enter Service Ticket Instructions");
                    return false;
                }
            
//            if(document.getElementById("txtGC").value=="")
//            {
//                alert("Please Enter General Comments");
//                return false;
//            }   
               btnSubmit.style.display = "none";
               tblwait.style.display = "block";
            
            
        }
        
        function ShowHide()
        {
          
           var showdocket = document.getElementById("cb_showondocket");
           var scomment = document.getElementById("cb_serinstruction");
           var gcomment = document.getElementById("cb_gencomments");
           
           if(showdocket.checked  == false)
           {
             gcomment.parentElement.setAttribute('disabled','true');   
             gcomment.disabled = true;
             scomment.parentElement.setAttribute('disabled','true'); 
             scomment.disabled = true;
             
           }
           if(showdocket.checked  == true)
           {
             gcomment.parentElement.removeAttribute('disabled'); 
             scomment.parentElement.removeAttribute('disabled');  
             scomment.disabled = false;
             gcomment.disabled = false;
           
           }           
        }
        //Zahoor 4770 09/12/08
        function SetPercentValue()
        {   
        var option = document.getElementById("ddl_ContinuanceOption").value
        var ddl_Percent = document.getElementById("ddl_per");
        var pervalue;
        //ddl_Percent.disabled = false;
       //  alert(option);
           if (option == "1")
            {            
                pervalue = "0";
            }
            //else if (option == "Contract Sent" || option == "Contract Signed")
            else if (option == "2" || option == "3")
            {
                pervalue="25";
            }
            else if (option == "4" || option == "5")
            {
                pervalue = "50";
            }
            else 
            {
                pervalue = "100";
            }            
            
            ddl_Percent.value=pervalue;
        }
        
        //Sabir Khan 5711 04/01/2009 Scripts used for edit service Ticket comments...
        //---------------------------------
        function showhideservice()
        {
            var text = document.getElementById("tb_serinstruction");
            var btnEdt = document.getElementById("btnEditService");
            if(btnEdt.value == "Edit")
              {
                text.style.display="block";  
                btnEdt.value = "Cancel";
              }
              else
              {
                text.style.display="none";
                btnEdt.value = "Edit";
              }
        }
        //----------------------------------
    
    //Sabir Khan 5738 06/01/2009 Logic for selecting valid business days for follow update...
    //---------------------------------------------------------------------------------    
    function CheckDate(seldate, tbID)
	{	
	
	
	 //Yasir Kamal 7412 02/16/2010 do not check follow up date for 100% tickets.
	 if(document.getElementById("ddl_per").value !="100")
       {
	 
	    var w = document.Form1.drpCategory.selectedIndex;
        var selected_text = document.Form1.drpCategory.options[w].text;
	    today=new Date();	   
	    var diff =Math.ceil(DateDiff(seldate, today));	    	    
	    var weekday=new Array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday")
	    if(seldate != '')
	    {
	        var newseldatestr = formatDate((Date.parseInvariant(seldate).getMonth()+1)+"/"+Date.parseInvariant(seldate).getDate() + "/"+Date.parseInvariant(seldate).getFullYear(),"MM/dd/yyyy");        
            newseldate = Date.parseInvariant(newseldatestr,"MM/dd/yyyy");        
	        var datediff =Math.ceil(DateDiff(newseldate, today));	  
	        if (datediff>=0)
	        {
	              
	              //Sabir Khan 6245 08/03/2009 Get courtdate and check with week in future date...
	              var futuredate    =   dateAdd("d", 30, today);
	              var courtdate =  document.getElementById("hfcourtdate").value;
	              var courtdatediff = Math.ceil(DateDiff(courtdate,futuredate));
	              if(courtdatediff < 0)
	              {
	                var i=0,countbusinessdays=0,countHolidays=0;
	                for (i=1;i<=datediff;i++)
	                {
                        if ((weekday[today.getDay()]!="Sunday")&&(weekday[today.getDay()]!="Saturday"))           
                        {              
                            countbusinessdays++;
                        }
                        else if ((weekday[today.getDay()]=="Saturday") && (i==1))
                        {
                            countbusinessdays++;   
                        }
                        today=dateAdd("d", 1, today);             
	                }
	                
	                //Asad Ali 8492 11/03/2010 if courtbusiness days greater then 90 and cateory is Bond Forfeiture restrict user
	                if (selected_text == 'Bond Forfeiture' && (datediff>90 || countbusinessdays==0) && Querystring()==false)
	                 {
	                        alert("Please select Follow Up Date within next 90 days from today's Date for Bond Forfeiture Category");
	                        return false;
	                }  
	                //Asad Ali 8492 11/03/2010 if courtbusiness days greater then 90 and cateory is Bond Forfeiture restrict user
	                else if (((countbusinessdays>5) || (countbusinessdays==0)) && selected_text != 'Bond Forfeiture')
	                {
	                //Yasir Kamal 6936 12/09/2009 allow current followupdate for credit card dispute.
	                    if((Querystring() || selected_text == "Credit Card Dispute") && countbusinessdays==0)
	                    {
	                        return true;
	                    }
	                    else
	                    {
	                        alert("Please select Follow Up Date within next 5 business days from today's Date");
	                        return false;
	                    }
	                }
	                
	                
	              }  
	        }
	        else
	        {
	         var olddate = document.getElementById("lblCurrentFollowupdate").innerText;
	         if(seldate != olddate)
	         {
	            alert("Please select future date");
	            return false;
	         }
	        }
	      if (weekday[newseldate.getDay()] == "Saturday" || weekday[newseldate.getDay()] == "Sunday" ) 
          {
              alert("Please select any business day");
              return false;
          } 
       }
      } 
	}
	function DateDiff(date1, date2)
	{
	    var one_day=1000*60*60*24;
		var objDate1=new Date(date1);
		var objDate2=new Date(date2);
		var curr = (objDate1.getTime()-objDate2.getTime())/one_day;
		return (objDate1.getTime()-objDate2.getTime())/one_day;
		
	}
//Sabir Khan 6082 07/03/2009 Check Querystring for Open new service ticket...
function Querystring() 
{
	this.params = {};
	var qs = location.search.substring(1, location.search.length);
	if (qs.length == 0) 
	return false;
	qs = qs.replace(/\+/g, ' ');
	var args = qs.split('&'); // parse out name/value pairs separated via &
//    // split out each name=value pair
	for (var i = 0; i < args.length; i++) 
	{
		var pair = args[i].split('=');
		var name = decodeURIComponent(pair[0]);				
		if(name == "Fid")
		{
		    return false;
		}
	}
	return true;
}
function ShowPopUp_1()
    { 
       today=new Date();
       var dt = today; 
        var tst1;     
       var i=0,countbusinessdays=0,countHolidays=0;
        for (i=1;i<=5;i++)
	     {
	        tst1 = formatDate(dateAdd("d", i, today),"MM/dd/yyyy");
          if ((weekday[tst1.getDay()]!="Sunday")&&(weekday[tst1.getDay()]!="Saturday"))    
           {
            countbusinessdays += 2;
           }
           else
           {
            countbusinessdays +=1
           }
         }  
       dt = formatDate(dateAdd("d", countbusinessdays, dt),"MM/dd/yyyy");  
       document.getElementById('<%= this.ClientID %>_cal_followupdate').value = dt;
    }        
    ///------------------------------------------------------------------------------------    
    </script>

</head>
<body style="position: static; top: 0px; left: 0px; width: 0px; height: 0px;" class="clsleftpaddingtable">
    <form id="Form1" method="post" runat="server">
    <aspnew:ScriptManager ID="ScriptManager1" runat="server">
    </aspnew:ScriptManager>
    <aspnew:UpdatePanel ID="uppnl_main" runat="server">
     <ContentTemplate>
        <table id="TableMain" cellspacing="1" cellpadding="1" border="0" align="center" style="width: 552px;
            border-left-color: navy; border-bottom-color: navy; border-top-color: navy; border-collapse: collapse;
            border-right-color: navy">
            <tr align="center">
                <td class="clssubhead" valign="middle">
                    <asp:Label ID="lblDivision" runat="server" Font-Names="Verdana" ForeColor="Red" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr>
                
                <td class="clssubhead" background="../Images/subhead_bg.gif" valign="middle" style="height: 36px;
                    width: 558px;">
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                        <tr>
                            <td class="clssubhead" style="width: 50%; height: 13px;">
                                &nbsp;<asp:Label ID="lbl_ClientName" runat="server"></asp:Label>
                                ,
                                <asp:Label ID="lbl_fisrtname" runat="server"></asp:Label></td>
                            <td align="right" class="clssubhead" style="width: 50%; height: 13px;">
                                Service Ticket Window&nbsp; 
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 558px">
                    <table border="0" cellpadding="0" cellspacing="0" class="clsleftpaddingtable" style="width: 100%">
                        <tr>
                            <td class="clssubhead" style="width: 19%; height: 21px">
                                % Completion</td>
                            <td class="clssubhead" style="width: 18%; height: 21px" runat="server" visible="false">
                                Priority</td>
                            <td class="clssubhead" style="width: 33%; height: 21px">
                                Category</td>
                            <td class="clssubhead" style="width: 33%; height: 21px">
                                Assign To</td>
                            <td class="clssubhead" style="width: 25%; height: 21px">
                                <asp:Label ID="lbl_ContinuanceOption" runat="server" Text="Continuance Option" Visible="False"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="width: 19%; height: 24px">
                                <asp:DropDownList ID="ddl_per" runat="server" CssClass="clsinputadministration" Width="100px"> 
                                    <asp:ListItem Value="0">0%</asp:ListItem>
                                    <asp:ListItem Value="25">25%</asp:ListItem>
                                    <asp:ListItem Value="50">50%</asp:ListItem>
                                    <asp:ListItem Value="75">75%</asp:ListItem>
                                    <asp:ListItem Value="100">100%</asp:ListItem>
                                    
                                </asp:DropDownList></td>
                            <td style="width: 18%; height: 24px" runat="server" visible="false">
                                <asp:DropDownList ID="drpPriority" runat="server" CssClass="clsinputadministration">
                                    <asp:ListItem Value="2">High</asp:ListItem>
                                    <asp:ListItem Value="0">Low</asp:ListItem>
                                    <asp:ListItem Value="1">Medium</asp:ListItem>
                                   
                                </asp:DropDownList></td>
                            <td style="width: 33%; height: 24px">
                                <asp:DropDownList ID="drpCategory" runat="server" CssClass="clsinputadministration"
                                    Width="175px" AutoPostBack="True" DataTextField="Description" DataValueField="ID"
                                    OnSelectedIndexChanged="drpCategory_SelectedIndexChanged">
                                    
                                </asp:DropDownList></td>
                            <td style="width: 33%; height: 24px">
                                <asp:DropDownList ID="ddl_assignto" runat="server" CssClass="clsinputadministration"
                                    Width="107px" >
                                </asp:DropDownList></td>
                            <td style="width: 25%; height: 24px">
                            <asp:DropDownList ID="ddl_ContinuanceOption" runat="server" CssClass="clsinputadministration"
                                    Width="135px" DataTextField="Description" DataValueField="ID"  
                                      Onchange ="javascript:SetPercentValue()" Visible="False">
                            </asp:DropDownList></td>
                        </tr>
                         <tr>
                            <td class="clsleftpaddingtable" background="../../images/separator_repeat.gif" colspan="7"
                                style="height: 7px">
                            </td>
                        </tr>
                        <tr>
                            <td class="clssubhead" style="width: 50%; height: 21px">
                                Current Follow Up date</td>
                            <td id="Td1" class="clssubhead" style="width: 18%; height: 21px" runat="server">
                                Next Follow Up date</td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblCurrentFollowupdate" runat="server" Text=""> </asp:Label>
                            </td>
                            <td  style="width: 19%; height: 24px">
                                <ew:CalendarPopup ID="calfollowupdate" runat="server" AllowArbitraryText="False" CalendarLocation="Bottom"
                                                ControlDisplay="TextBoxImage" Culture="(Default)" DisableTextboxEntry="True"
                                                Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif" PadSingleDigits="True"
                                                ShowGoToToday="True" ToolTip="Select Report Date Range" UpperBoundDate="12/31/9999 23:59:00"
                                                Width="90px" JavascriptOnChangeFunction="CheckDate" >
                                                <TextboxLabelStyle CssClass="clstextarea" />
                                                <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                                <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    Font-Size="XX-Small" ForeColor="Black" />
                                                <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    Font-Size="XX-Small" ForeColor="Gray" />
                                                <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                                <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    Font-Size="XX-Small" ForeColor="Black" />
                                                <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                                <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                                <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    Font-Size="XX-Small" ForeColor="Black" />
                                                <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                                <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                            </ew:CalendarPopup>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center" class="clsleftpaddingtable" style="width: 558px">
                    <asp:Label ID="lblMessage" runat="server" ForeColor="Red" CssClass="label"></asp:Label></td>
            </tr>
            <tr>
                <td class="clsleftpaddingtable" background="../../images/separator_repeat.gif" colspan="7"
                    style="height: 7px">
                </td>
            </tr>
            <tr>
                <td class="clsleftpaddingtable" style="height: 20px; width: 558px;" valign="middle"
                    cssclass="label">
                    <asp:CheckBox ID="cb_showondocket" runat="server" CssClass="label" Text="Show case on trial docket" />&nbsp;
                </td>
            </tr>
            <tr>
                <td class="clsleftpaddingtable" background="../../images/separator_repeat.gif" colspan="7"
                    style="height: 7px">
                </td>
            </tr>
            <tr>
                <td class="clsleftpaddingtable" style="height: 5px; width: 558px;">
                    <table class="clsleftpaddingtable" style="width: 100%">
                        <tr>
                            <td style="width: 50%; height: 17px;" align="left">
                                <asp:Label ID="lbl_generalcomments" runat="server" Text="General Comments " CssClass="label"
                                    Font-Bold="True" Font-Underline="False"></asp:Label></td>
                            <td id="generalcomments2" align="right">
                                <asp:CheckBox ID="cb_gencomments" runat="server" Text="Show On Docket" CssClass="label"  /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="clsleftpaddingtable" style="height: 8px; width: 558px;" valign="top">
                    <cc2:WCtl_Comments ID="WCC_GeneralComments" runat="server" Height="55px" Width="545px">
                    </cc2:WCtl_Comments>
                </td>
            </tr>
            <tr>
                <td class="clsleftpaddingtable" background="../../images/separator_repeat.gif" colspan="7"
                    style="height: 7px">
                </td>
            </tr>
            <tr>
                <td class="clsleftpaddingtable" style="height: 20px; width: 558px;" valign="top">
                    <table class="clsleftpaddingtable" style="width: 100%">
                        <tr>
                            <td style="width: 50%; height: 17px;" align="left">
                                <asp:Label ID="lbl_serviceticket" runat="server" Text="Service Ticket Comments "
                                    CssClass="label" Font-Bold="True" Font-Underline="False"></asp:Label></td>
                            <td id="serviceticket2" align="right">
                                <asp:CheckBox ID="cb_serinstruction" runat="server" CssClass="label" Text="Show On Docket"/></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 10px; width: 558px;" valign="top" align="left" class="clsleftpaddingtable">
                    <asp:TextBox ID="lblServiceComments" runat="server" 
                        CssClass="clsleftpaddingtable" TextMode="MultiLine" ReadOnly="true" 
                        Width="550px" Height="55px" BorderStyle="None"></asp:TextBox> 
                    <asp:TextBox ID="tb_serinstruction" runat="server" CssClass="clstextarea" Height="30px"
                        TextMode="MultiLine" Width="550px"  style="display:none;">
                    </asp:TextBox>
                   
                    
                    &nbsp;&nbsp;&nbsp;<input ID="btnEditService" onclick="showhideservice();" 
                        style="border:0px;font-weight:bold;font-size:10pt;background-color: #EFF4FB; cursor: hand;color:#3366cc;font-family:Tahoma;text-align:center;" 
                        type="button" value="Edit" />
                    <br>
                    <br></br>
                    <br></br>
                    </br>
                   </td>
            </tr>
            <tr>
                <td class="clsleftpaddingtable" style="height: 30px; width: 558px;" valign="middle"
                    align="center">
                    &nbsp;<asp:Button ID="btnSubmit" runat="server" Text="Add Service Ticket" CssClass="clsbutton"
                        Width="137px" OnClientClick="return Validate();" OnClick="btnSubmit_Click"></asp:Button>&nbsp;
                    <table id="tblwait" border="0" cellpadding="0" cellspacing="0" style="width: 100%;display: none; ">
                        <tr>
                            <td align="center" class="clssubhead" style="height: 25px">
                                   Please wait while system updates service ticket information.</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <asp:HiddenField ID="hf_defaultassigned" runat="server" />
         <asp:HiddenField ID="hfcourtdate" runat="server" />
        </ContentTemplate>
            <Triggers>
                <aspnew:AsyncPostBackTrigger ControlID="btnSubmit" />
                
            </Triggers>
        </aspnew:UpdatePanel>
       
    </form>
</body>
</html>

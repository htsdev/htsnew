using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.Reports
{
	/// <summary>
	/// Summary description for PreviewEmail.
	/// </summary>
	public partial class PreviewEmail : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.ImageButton imgbtn_send;
		protected System.Web.UI.WebControls.ImageButton imgbtn_cancel;
		protected System.Web.UI.WebControls.TextBox txt_to;
		protected System.Web.UI.WebControls.TextBox txt_cc;
		protected System.Web.UI.WebControls.TextBox txt_bcc;
		protected System.Web.UI.WebControls.TextBox txt_subject;
		protected System.Web.UI.WebControls.TextBox txt_message;
		clsENationWebComponents ClsDb = new clsENationWebComponents();
		protected System.Web.UI.WebControls.Label lbl_message;
		protected System.Web.UI.WebControls.ImageButton imgbtn_receipt;
		protected System.Web.UI.WebControls.ImageButton imgbtn_trialleter; 
		clsSession uSession = new clsSession();
		clsLogger clog = new clsLogger();	
		int Emailid=0;	
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			if(!IsPostBack)
			{
				DataSet ds_email=null;
				try
				{
					Emailid=Convert.ToInt32(Request.QueryString["Emailid"]);
					string[] key1 = {"@Emailid"};		
					object[] value11 = {Emailid};
					ds_email=ClsDb.Get_DS_BySPArr("usp_hts_get_clientemailinfo",key1,value11);					
					txt_to.Text=ds_email.Tables[0].Rows[0]["emailto"].ToString();
					txt_cc.Text=ds_email.Tables[0].Rows[0]["emailcc"].ToString();
					txt_bcc.Text=ds_email.Tables[0].Rows[0]["emailbcc"].ToString();
					txt_subject.Text=ds_email.Tables[0].Rows[0]["emailsubject"].ToString();
					txt_message.Text= ds_email.Tables[0].Rows[0]["emailmessage"].ToString();
					Session["ds_email"]=ds_email;
					string lnk;
					lnk= "javascript: window.open('frmpreviewemaildoc.aspx?Emailid="+Emailid+ "','','height=505,width=600,resizable=yes, status=no,toolbar=no,scrollbars=yes,menubar=no,location=no'); return false;"; 
					if(ds_email.Tables[0].Rows[0]["lettertype"].ToString()=="3")//Receipt
					{
						imgbtn_receipt.Visible=true;												
						imgbtn_receipt.Attributes.Add("onclick",lnk);
					}
					else
					{
						imgbtn_trialleter.Visible=true;
						imgbtn_trialleter.Attributes.Add("onclick",lnk);
					}					
				}
				catch(Exception ex)
				{
					lbl_message.Text=ex.Message;
				}				
			}
			//imgbtn_send.Attributes.Add("onclick","return Validate();");
			imgbtn_cancel.Attributes.Add("onclick","javascript: self.close();");
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.imgbtn_send.Click += new System.Web.UI.ImageClickEventHandler(this.imgbtn_send_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void imgbtn_send_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{	
			DataSet ds_email=(DataSet)Session["ds_email"];			
			//Getting sales rep email
			string emailrep = clog.SalesRepEmail(Convert.ToInt32(uSession.GetCookie("sEmpID",this.Request)));
			//Calling send email function
			//	clog.EmailReport(txt_to.Text,emailrep,txt_cc.Text,txt_bcc.Text,txt_subject.Text,txt_message.Text,ds_email.Tables[0].Rows[0]["filename"].ToString());				
			//Add email info to database
			int emailid=clog.SaveEmailInfo(Convert.ToInt32(ds_email.Tables[0].Rows[0]["ticketid"]),Convert.ToInt32(ds_email.Tables[0].Rows[0]["lettertype"]),ds_email.Tables[0].Rows[0]["filename"].ToString(),txt_to.Text,emailrep,txt_cc.Text,txt_bcc.Text,txt_subject.Text,txt_message.Text);
			//Adding To notes
			clog.AddEmailNote(Convert.ToInt32(uSession.GetCookie("sEmpID",this.Request)),txt_to.Text,Convert.ToInt32(ds_email.Tables[0].Rows[0]["ticketid"]),Convert.ToInt32(ds_email.Tables[0].Rows[0]["lettertype"]),emailid);			
			HttpContext.Current.Response.Write("<script language='javascript'> self.close();   </script>");					
		}
	}
}

using System;
using System.Web.UI.WebControls;
using lntechNew.Components;
using LNHelper;
using lntechNew.Components.ClientInfo;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using System.Data;
using HTP.Components;




namespace HTP.Reports
{
    public partial class MissingCauseNumber : System.Web.UI.Page
    {
       
        ValidationReports reports = new ValidationReports();
        //Muhammad Ali 7747 06/30/2010--> initailiaze objects 
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        clsCase cCase = new clsCase();
        DataView dv;
        //int day = 0;
        //int Report_ID = 0;
        //End Muhammad Ali initailiaze objects 
        //Muhammad Ali 7747 06/30/2010 --> Grid view Sorting Direction .
        /// <summary>
        /// Grid view Sorting Direction 
        /// </summary>
        public SortDirection GridViewSortDirection
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Ascending;
                return (SortDirection)ViewState["sortDirection"];
            }
            set { ViewState["sortDirection"] = value; }

        }
        //Muhammad Ali 7747 06/29/2010 --> Events initaliazation.
        protected override void OnInit(EventArgs e)
        {
            // Show Setting Control Event initaliazation;
            ShowSetting_MissingCauseNumber.OnErr += new HTP.WebControls.ShowSetting.ErrHandler(ShowSetting_MissingCauseNumber_OnErr);
            ShowSetting_MissingCauseNumber.dbBind += new HTP.WebControls.ShowSetting.Databind(ShowSetting_MissingCauseNumber_dbBind);
            base.OnInit(e);
        }
        //Muhammad Ali 7747 06/29/2010 --> Show setting Control Event.
        /// <summary>
        /// Show Setting Control DataBind 
        /// </summary>
        void ShowSetting_MissingCauseNumber_dbBind()
        {
            BindGrid();
        }
        //Muhammad Ali 7747 06/29/2010 --> Show Setting controls Error Event
        /// <summary>
        /// Show Setting Control Disply Error Message.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="ErrorMsg">Error Message</param>
        void ShowSetting_MissingCauseNumber_OnErr(object sender, string ErrorMsg)
        {
            lbl_message.Text = ErrorMsg;

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Fahad 5530 03/17/2009 Try and Catch Block Added
            try
            {
                if (!IsPostBack)
                {
                    BindGrid();
                }
                //Fahad 5530 03/16/2009 Paging Control Event added
                Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
                //Muhammad Ali 7747 06/1/2010 --> initialize Follow Update listner.
                UpdateFollowUpInfo2.PageMethod += new HTP.WebControls.PageMethodHandler(UpdateFollowUpInfo2_PageMethod);
                Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
                Pagingctrl.GridView = gv_records;
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                lbl_message.Text = ex.Message;
            }

        }
         protected void BindGrid()
        {
             //Muhammad Ali 7747 06/30/2010 --> Add Sorting functionality.
            reports.getReportData(gv_records, lbl_message, ValidationReport.MissingCauseNumber);
            DataTable dt = (gv_records.DataSource) as DataTable;
            if (Session["DV_OldSortedData"] == null)
            {
                dv = new DataView(dt);
                Session["DV_OldSortedData"] = dv;
            }
            else
            {
                dv = (DataView)Session["DV_OldSortedData"];
                string SortExp = dv.Sort;
                dv = new DataView(dt);
                dv.Sort = SortExp;
                dt = dv.ToTable();
                if (dt.Columns.Contains("sno"))
                {
                    dt.Columns.Remove("sno");
                }
            }


            if (dt.Columns.Contains("sno") == false)
            {
                dt.Columns.Add("sno");
                int sno = 1;
                if (dt.Rows.Count >= 1)
                    dt.Rows[0]["sno"] = 1;
                if (dt.Rows.Count >= 2)
                {
                    for (int i = 1; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[i - 1]["ticketid_pk"].ToString() != dt.Rows[i]["ticketid_pk"].ToString())
                        {
                            dt.Rows[i]["sno"] = ++sno;
                        }
                    }
                }
            }
            dv = dt.DefaultView;
            gv_records.Visible = true;
            gv_records.DataSource = dv;
            gv_records.DataBind();
            Pagingctrl.PageCount = gv_records.PageCount;
            Pagingctrl.PageIndex = gv_records.PageIndex;
            Pagingctrl.SetPageIndex();

        }
        //muhammad Ali 7747 06/30/2010 --> Update Follow Up methods.
        void UpdateFollowUpInfo2_PageMethod()
        {   
            BindGrid();
        }
        void Pagingctrl_PageIndexChanged()
        {
            // Agha Usman - 4426 - 12/08/2008 Correct Paging 
            gv_records.PageIndex = Pagingctrl.PageIndex - 1;
            BindGrid();
        }
        /// Fahad 5530 03/16/2009 Paging Control Event added
        /// Allow to see the User the required no of records per page
        /// </summary>
        /// <param name="pageSize"></param>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {

            if (pageSize > 0)
            {
                gv_records.PageIndex = 0;
                gv_records.PageSize = pageSize;
                gv_records.AllowPaging = true;
            }
            else
            {
                gv_records.AllowPaging = false;
            }
            BindGrid();

        }
        protected void gv_records_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //Muhammad Ali 7747 06/30/2010 --> Bind index on Add Image.
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ((LinkButton)e.Row.FindControl("img_Add")).CommandArgument = e.Row.RowIndex.ToString();
                }
            }

            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                lbl_message.Text = ex.ToString();
            }
        }
        protected void gv_records_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            //Muhammad Ali 7747 06/30/2010 --> Bind Follow Up Control Data.
            try
            {
                if (e.CommandName == "btnclick")
                {
                    int RowID = Convert.ToInt32(e.CommandArgument);
                    string Sno = ((HiddenField)gv_records.Rows[RowID].FindControl("hf_Sno")).Value;
                    string CauseNo = ((HiddenField)gv_records.Rows[RowID].FindControl("hf_CauseNo")).Value;
                    string TicketNo = ((HiddenField)gv_records.Rows[RowID].FindControl("hf_ticketno")).Value;
                    string TicketID = ((HiddenField)gv_records.Rows[RowID].FindControl("hf_TicketID")).Value;
                    string Last = ((HiddenField)gv_records.Rows[RowID].FindControl("hf_Last")).Value;
                    string First = ((HiddenField)gv_records.Rows[RowID].FindControl("hf_First")).Value;
                    string DOB = (((HiddenField)gv_records.Rows[RowID].FindControl("hf_DOB")).Value);
                    string DL = (((HiddenField)gv_records.Rows[RowID].FindControl("hf_DL")).Value);
                    string CourtDate = (((HiddenField)gv_records.Rows[RowID].FindControl("hf_CourtDate")).Value);
                    string Status = (((HiddenField)gv_records.Rows[RowID].FindControl("hf_Status")).Value);
                    string Loc = (((HiddenField)gv_records.Rows[RowID].FindControl("hf_Loc")).Value);
                    string Flags = (((HiddenField)gv_records.Rows[RowID].FindControl("hf_Flags")).Value);
                    string FollowUpdate = (((HiddenField)gv_records.Rows[RowID].FindControl("hf_FollowUpdate")).Value);
                    string SortedFollowUpdate = (((HiddenField)gv_records.Rows[RowID].FindControl("hf_SortedFollowUpdate")).Value);
                    string court = "MissingCauseNumber";
                    cCase.TicketID = Convert.ToInt32(TicketID);
                    string commments = cCase.GetGeneralCommentsByTicketId();
                    UpdateFollowUpInfo2.Freezecalender = true;
                    UpdateFollowUpInfo2.Title = "Missing Cause Follow Up Date";
                    UpdateFollowUpInfo2.followUpType = HTP.Components.FollowUpType.MissingCauseFollowUpdate;
                    string nextfollow = (FollowUpdate.Equals("N/A")) ? "N/A" : FollowUpdate.ToString();
                    DateTime day = (FollowUpdate.Equals("N/A")) ? DateTime.Now : Convert.ToDateTime(SortedFollowUpdate);
                    UpdateFollowUpInfo2.binddate(day.ToString(), day, DateTime.Today, First, Last, TicketNo, TicketID, CauseNo, commments, mpemissingcause.ClientID, court, court, nextfollow.ToString());
                    mpemissingcause.Show();
                    Pagingctrl.Visible = true;
                }

            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                lbl_message.Text = ex.ToString();
            }
        }
        protected void gv_records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gv_records.PageIndex = e.NewPageIndex;
            BindGrid();
        }

        protected void gv_records_Sorting(object sender, GridViewSortEventArgs e)
        { //Muhammad Ali 7747 06/30/2010 --> Sorting Grid .
            try
            {
                string sortExpression = e.SortExpression;

                if (GridViewSortDirection == SortDirection.Ascending)
                {
                    GridViewSortDirection = SortDirection.Descending;
                    SortGridView(sortExpression, "DESC");
                }
                else
                {
                    GridViewSortDirection = SortDirection.Ascending;
                    SortGridView(sortExpression, "ASC");
                }
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }
       
        //Muhammad Ali 7747 06/30/2010 --> Sorting Grid According to Direction.
        private void SortGridView(string sortExpression, string direction)
        {
            dv = (DataView)Session["DV_OldSortedData"];
            dv.Sort = sortExpression + " " + direction;
            Session["DV_OldSortedData"] = dv;
            BindGrid();
            ViewState["sortExpression"] = sortExpression;
            Session["SortDirection"] = direction;
            Session["SortExpression"] = sortExpression;
        }
    }

}

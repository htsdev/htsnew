using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;
using LNHelper;

namespace HTP.Reports
{
    public partial class setcallvalidation : System.Web.UI.Page
    {
        DBComponent ClsDb = new DBComponent();
        clscalls clscall = new clscalls();
        clsLogger bugTracker = new clsLogger();

        //Yasir Kamal 5555 02/26/2009 issues fixed in pagging and showSetting

        protected override void OnInit(EventArgs e)
        {
            ShowSetting.OnErr += new HTP.WebControls.ShowSetting.ErrHandler(ShowSetting_OnErr);
            ShowSetting.dbBind += new HTP.WebControls.ShowSetting.Databind(ShowSetting_dbBind);
            Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
            Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
            base.OnInit(e);
        }
                
        // Noufil 3498 04/05/2008 new file

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Pagingctrl.GridView = gv_records;
                Pagingctrl.Visible = true;

                if (!IsPostBack)
                {

                    
                    //if (Request.Cookies["sAccessType"] == null)
                    //{

                    //    tdPrimary.Visible = false;
                    //}
                    //else
                    //{
                    //    if (Request.Cookies["sAccessType"].Value.ToString() == "2")
                    //    {

                    //        tdPrimary.Visible = true;
                    //    }
                    //}
                    //Fill_ddlist();
                    //LB_curr.Text = (gv_records.PageIndex + 1).ToString();
                    

                    BindGrid();
                    
                }
                
              

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        
        //private void Fill_ddlist()
        //{
        //    DL_pages.Items.Clear();
        //    for (int i = 1; i <= this.gv_records.PageCount; i++)
        //    {
        //        DL_pages.Items.Add(new ListItem(Convert.ToString(i), Convert.ToString(i)));
        //    }
        //    LB_curr.Text = Convert.ToString(DL_pages.SelectedIndex + 1);

        //}

        //Sabir 4272 07/08/2008 sets paging in gv_records 

        protected void gv_records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {

                gv_records.PageIndex = e.NewPageIndex;
                BindGrid();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

            
            //DL_pages.SelectedIndex = e.NewPageIndex;
            //LB_curr.Text = (e.NewPageIndex + 1).ToString();
            //DL_pages.SelectedIndex = e.NewPageIndex;
            //LB_curr.Text = (e.NewPageIndex + 1).ToString();
            
            

        }  
        
        //Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
        //protected void DL_pages_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    int x = Convert.ToInt32(this.DL_pages.SelectedValue);

        //    gv_records.PageIndex = x - 1;
        //    LB_curr.Text = x.ToString();
        //    BindGrid();
        //}
         

        /// <summary>
        /// Handling PaggingControl page Index Change event
        /// </summary>
        void Pagingctrl_PageIndexChanged()
        {
            try
            {

                gv_records.PageIndex = Pagingctrl.PageIndex - 1;
                BindGrid();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        /// <summary>
        /// Handling PaggingControl page Size Change event
        /// </summary>
        /// <param name="pageSize"></param>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            try
            {

                if (pageSize > 0)
                {
                    gv_records.PageIndex = 0;
                    gv_records.PageSize = pageSize;
                    gv_records.AllowPaging = true;

                }
                else
                {
                    gv_records.AllowPaging = false;
                }

                BindGrid();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }


        }

        protected void BindGrid()
        {
            try
            {
                lblMessage.Text = "";
                DataTable dt = clscall.getsetcallreport();
                if (dt.Rows.Count == 0)
                {
                    lblMessage.Text = "No records found";
                }

                if (dt.Columns.Contains("sno") == false)
                {
                    dt.Columns.Add("sno");
                    int sno = 1;
                    if (dt.Rows.Count >= 1)
                        dt.Rows[0]["sno"] = 1;
                    if (dt.Rows.Count >= 2)
                    {
                        for (int i = 1; i < dt.Rows.Count; i++)
                        {
                            if (dt.Rows[i - 1]["ticketid"].ToString() != dt.Rows[i]["ticketid"].ToString())
                            {
                                dt.Rows[i]["sno"] = ++sno;
                            }
                        }
                    }
                }

                gv_records.Visible = true;
                gv_records.DataSource = dt;
                gv_records.DataBind();
                Pagingctrl.PageCount = gv_records.PageCount;
                Pagingctrl.PageIndex = gv_records.PageIndex;
                Pagingctrl.SetPageIndex();
            }
         
        // Sabir Khan 4635 09/19/2008
        //-----------------------
        //DL_pages.Visible = false;
        //totlb.Visible = false;
        //LB_curr.Visible = false;
        //Label2.Visible = false;

        //     ----------------------

        // }
        //else
        //{
        //Sabir Khan 4635 09/19/2008
        //-----------------------
        //DL_pages.Visible = true;
        //totlb.Visible = true;
        //LB_curr.Visible = true;
        //Label2.Visible = true;
        //lblMessage.Text = "";

        //         if (gv_records.PageCount == 0)
        //{
        //    DL_pages.Visible = false;
        //    totlb.Visible = false;
        //    LB_curr.Visible = false;
        //    Label2.Visible = false;
        //}

        //         ----------------------
        //}
        //else
        //{
        //    DataColumn dc = new DataColumn("SNo");
        //    dc.DataType = typeof(Int32);
        //    dt.Columns.Add(dc);

        //     for (int i = 0; i < dt.Rows.Count; i++)
        //    {
        //        dt.Rows[i]["Sno"] = i + 1;
        //    }

        // }

        //         gv_records.DataSource = dt;
        //gv_records.DataBind();

        //          Sabir 4272 07/08/2008 Get number of business logic days 
        //string[] key = { "@Attribute_Key", "@Report_Id" };
        //object[] value = { "Days", "139" };

        //         DataTable dtDays = ClsDb.GetDTBySPArr("usp_hts_get_BusinessLogicDay", key, value);
        //if (dtDays.Rows.Count == 0)
        //    txtNumberofDays.Text = "";
        //else
        //{
        //    txtNumberofDays.Text = dtDays.Rows[0]["Attribute_Value"].ToString();
        //}
        ////Sabir 4272 07/16/2008  Convert datatable to dataview for sorting.                    
        //DataView dv = new DataView(dt);
        //if (ViewState["sortexp"] != null || ViewState["sortdir"] != null)
        //{
        //    dv.Sort = ViewState["sortexp"] + " " + ViewState["sortdir"];
        //}

        //     } 

            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        
        /// <summary>
        /// Show Setting Control Databind
        /// </summary>
        void ShowSetting_dbBind()
        {

            BindGrid();

        }
        /// <summary>
        /// ShowSetting display message on error
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="ErrorMsg"></param>
        void ShowSetting_OnErr(object sender, string ErrorMsg)
        {
            lblMessage.Text = ErrorMsg;

        }
        
       
        //Sabir 4272 07/08/2008 Implement number of business update feature in Set Call Validation
     
        //protected void btnUpdate_Click(object sender, EventArgs e)
        //{

        //    try
        //    {
        //        if (Page.IsValid)
        //        {

        //            string[] key ={ "@Attribute_Key", "@Attribute_Value", "@Report_Id" };
        //            object[] value ={ "Days", int.Parse(txtNumberofDays.Text), "139" };
        //            ClsDb.InsertBySPArr("usp_hts_add_BusinessLogicDay", key, value);
        //            BindGrid();
        //            //Fill_ddlist();

        //        }
        //    }
        //    catch (FormatException ) 
        //    {
        //        lblMessage.Text = "Please enter only digits in number fields";
        //    }
        //    catch (InvalidCastException )
        //    {

        //        lblMessage.Text = "Please enter only digits in number fields";
        //    }
        //    catch (Exception ex)
        //    {

        //        lblMessage.Text = ex.Message;
        //        bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
        //    }
           

        //}


        //Sabir 4272 07/16/2008  Implement sorting

        //protected void Gv_records_Sorting(object sender, GridViewSortEventArgs e)
        //{
        //    try
        //    {
        //        string sortExpression = string.Empty;
        //        string sortDirection = string.Empty;

        //        if (ViewState["sortexp"] == null && ViewState["sortdir"] == null)
        //        {
        //            ViewState["sortexp"] = "";
        //            ViewState["sortdir"] = "";
        //        }


        //        if (ViewState["sortexp"].ToString() == e.SortExpression)
        //        {
        //            sortExpression = e.SortExpression;

        //            if (ViewState["sortdir"].ToString() == "ASC")
        //            {
        //                sortDirection = "DESC";
        //            }
        //            else
        //            {
        //                sortDirection = "ASC";
        //            }
        //        }
        //        else
        //        {
        //            sortExpression = e.SortExpression;
        //            sortDirection = "ASC";
        //        }

        //        ViewState["sortexp"] = sortExpression;
        //        ViewState["sortdir"] = sortDirection;

        //        BindGrid();

              
        //    }
        //    catch (Exception ex)
        //    {
        //        lblMessage.Text = ex.Message;
        //        clsLogger.ErrorLog(ex);
        //    }
        //}
      
      

    }
}

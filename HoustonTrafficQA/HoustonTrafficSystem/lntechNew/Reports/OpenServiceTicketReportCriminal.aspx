<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OpenServiceTicketReportCriminal.aspx.cs"
    Inherits="HTP.Reports.OpenServiceTicketReportCriminal" %>

<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc2" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Criminal Service Ticket Report</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />

    <script type="text/jscript"> 
       
      
       function unCheck(id)
       {
           var chkbox = document.getElementById(id)
           if ( chkbox != null)
           chkbox.checked = false;
       }

        function ToggleSelection(id)
        {
         var AllTickets = document.getElementById ("chkShowAllfollowupRecords");
         var AllPending = document.getElementById ("chkAllPastDue");
         
         if (id == "chkShowAllfollowupRecords")
         {
            AllPending.checked = false;
         }
         else if (id == "chkAllPastDue")
         {
            AllTickets.checked = false;
         }
         
         return false;
         
        }
       
       function RecordsType()
       {

       return true;
       }
       
        function PopUp(ticketid, fid)
		{
		    //Fahad 7791 08/13/2010 Fixed Popup issue which is not updated every time
		    var getdate = new Date();
		    //Fahad 7791 08/13/2010 Concatenated the time slot with Query String
		    var path="GeneralCommentsPopup.aspx?ticketid="+ticketid+"&Fid="+fid+"&OSTR=1&Report=1&time="+getdate.getTime();
		    window.open(path,'',"fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=no,width=650,height=480,scrollbars=yes");
		    return false;
		}
		
		
		function ShowAttorneyList(ticketid,fid)
		{
		
		var div = document.getElementById("pnl_attorney");
		var hf = document.getElementById("hf_ticketid");
		var hffid = document.getElementById("hf_fid");
		
		 var IpopTop = (document.body.clientHeight - div.offsetHeight) / 2;
         var IpopLeft = (document.body.clientWidth - div.offsetWidth) / 2;
         
         div.style.left=IpopLeft + document.body.scrollLeft;
         div.style.top=IpopTop + document.body.scrollTop;
        				
		hf.value = ticketid;
		hffid.value = fid;
		div.style.display = "block";
		document.getElementById("ddl_attorney").value = "4028";
		return false;
				
		}
		
		function closepopup()
		{
		    document.getElementById("pnl_attorney").style.display = "none";
		    return false;		
		}
		 
		function Delete(username,comp)
		{		    
		    if ( comp != "100%")
		    {
	        alert("To close this service ticket please complete it to 100%.");
		        return false;
		    }		    
		  
		    if(ConfirmYesNo("Close service ticket for " + username ))
		    {
		        return true;   
		    }    
		    else
		    {
		        return false;
		        
		    }
		}
		
    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            width: 92px;
        }
        .style2
        {
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table border="0" width="1245px" cellpadding="0" cellspacing="0" align="center">
            <tr>
                <td style="height: 242px; width: 100%">
                    <table border="0" width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                            </td>
                        </tr>
                        <tr background="../images/separator_repeat.gif">
                            <td width="100%" background="../images/separator_repeat.gif" colspan="5" height="11">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5" width="100%" align="left" class="clsleftpaddingtable">
                                <table cellpadding="0" cellspacing="0" style="width: 100%; height: 100%" class="clsleftpaddingtable">
                                    <tr valign="middle">
                                        <td class="clssubhead">
                                            Start Date :
                                        </td>
                                        <td>
                                            <ew:CalendarPopup ID="caldatefrom" runat="server" AllowArbitraryText="False" CalendarLocation="Bottom"
                                                ControlDisplay="TextBoxImage" Culture="(Default)" DisableTextboxEntry="False"
                                                Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif" PadSingleDigits="True"
                                                ShowGoToToday="True" ToolTip="Select Report Date Range" UpperBoundDate="12/31/9999 23:59:00"
                                                Width="90px">
                                                <TextboxLabelStyle CssClass="clstextarea" />
                                                <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                                <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    Font-Size="XX-Small" ForeColor="Black" />
                                                <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    Font-Size="XX-Small" ForeColor="Gray" />
                                                <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                                <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    Font-Size="XX-Small" ForeColor="Black" />
                                                <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                                <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                                <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    Font-Size="XX-Small" ForeColor="Black" />
                                                <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                                <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                            </ew:CalendarPopup>
                                        </td>
                                        <td class="clssubhead" style="width: 57px">
                                            End Date :
                                        </td>
                                        <td>
                                            <ew:CalendarPopup ID="caldateto" runat="server" AllowArbitraryText="False" CalendarLocation="Bottom"
                                                ControlDisplay="TextBoxImage" Culture="(Default)" DisableTextboxEntry="False"
                                                Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif" PadSingleDigits="True"
                                                SelectedDate="2005-09-01" ShowGoToToday="True" ToolTip="Select Report Date Range"
                                                UpperBoundDate="12/31/9999 23:59:00" Width="90px">
                                                <TextboxLabelStyle CssClass="clstextarea" />
                                                <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                                <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    Font-Size="XX-Small" ForeColor="Black" />
                                                <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    Font-Size="XX-Small" ForeColor="Gray" />
                                                <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                                <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    Font-Size="XX-Small" ForeColor="Black" />
                                                <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                                <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                                <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    Font-Size="XX-Small" ForeColor="Black" />
                                                <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                                <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                            </ew:CalendarPopup>
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkShowAllfollowupRecords" runat="server" Text="All follow up dates"
                                                CssClass="clslabel" onclick="ToggleSelection(this.id);" />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkAllPastDue" runat="server" Text="All Past Due" CssClass="clslabel"
                                                Checked="true" onclick="ToggleSelection(this.id);" />
                                        </td>
                                        <td valign="left">
                                            <asp:RadioButtonList ID="rbl_tickettype" runat="server" RepeatDirection="Horizontal"
                                                CssClass="clslabel">
                                                <asp:ListItem Selected="True" Value="1">&lt;span class=clslabel&gt;Open Ticket&lt;/span&gt;</asp:ListItem>
                                                <asp:ListItem Value="0">&lt;span class=clslabel&gt;Close Ticket&lt;/label&gt;</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                        <td>
                                            <asp:Button ID="btn_search" runat="server" CssClass="clsbutton" OnClick="btn_search_Click"
                                                Text="Search" OnClientClick="return RecordsType()" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr background="../images/separator_repeat.gif">
                            <td background="../images/separator_repeat.gif" colspan="5" height="11" width="100%">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%">
                    <table border="0" width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="width: 100%" align="center">
                                <asp:Label ID="lbl_Message" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td background="../Images/subhead_bg.gif" class="clssubhead" align="left" style="height: 34px">
                                <table width="100%">
                                    <tr>
                                        <td align="left" class="clssubhead">
                                            Criminal Service Ticket Report
                                        </td>
                                        <td align="right" class="clssubhead">
                                            <uc2:PagingControl ID="PagingControl1" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" background="../Images/separator_repeat.gif" style="width: 896px;
                                height: 11px;">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table border="0" width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td align="center">
                                            <asp:Label ID="lblMessage" runat="server" ForeColor="Red" CssClass="clslabel" Font-Bold="True"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="gv" runat="server" Width="100%" AutoGenerateColumns="False" BackColor="#EFF4FB"
                                                BorderColor="White" CssClass="clsLeftPaddingTable" OnRowDataBound="gv_RowDataBound"
                                                AllowPaging="True" AllowSorting="True" OnPageIndexChanging="gv_PageIndexChanging"
                                                OnSorting="gv_Sorting" PageSize="20" OnRowCommand="gv_RowCommand">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="S.No">
                                                        <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Left" VerticalAlign="Middle" />
                                                        <ItemStyle VerticalAlign="Top" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="HPSNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SNo") %>'
                                                                CssClass="Label"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="COM">
                                                        <HeaderStyle CssClass="clsaspcolumnheader" VerticalAlign="Middle" HorizontalAlign="Center" />
                                                        <ItemStyle VerticalAlign="Top" />
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="HpLastUpdate" runat="server" ImageUrl="/Images/Add.gif" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Division">
                                                        <HeaderStyle CssClass="clsaspcolumnheader" VerticalAlign="Middle" HorizontalAlign="Center" />
                                                        <ItemStyle VerticalAlign="Top" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblDivision" runat="server" CssClass="Label" Text='<%#Eval("CaseTypeName") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Category" SortExpression="Category">
                                                        <HeaderStyle CssClass="clsaspcolumnheader" VerticalAlign="Middle" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCategory" runat="server" CssClass="Label" Text='<%# bind("category") %>'></asp:Label>
                                                            <asp:Label ID="lblCategoryOption" runat="server" CssClass="Label" Text="<%# bind('ContinuanceOption') %>"></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Wrap="False" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Name">
                                                        <HeaderStyle CssClass="clsaspcolumnheader" VerticalAlign="Middle" HorizontalAlign="Center" />
                                                        <ItemStyle VerticalAlign="Top" />
                                                        <ItemTemplate>
                                                            <asp:HiddenField ID="HFTicketid" runat="server" Value='<%# bind("ticketid_pk") %>' />
                                                            <!-- khalid  2727 date : 30/1/08 to remove unused field 
                                                                   <asp:HiddenField ID="HFFlagid" runat="server" Value='<%# bind("flagid") %>' />
                                                                 -->
                                                            <asp:HiddenField ID="HFFid" runat="server" Value='<%# bind("fid") %>' />
                                                            <asp:HyperLink ID="HPClientName" NavigateUrl="#" runat="server" Text='<%# bind("clientname") %>'></asp:HyperLink>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Crt Date">
                                                        <HeaderStyle CssClass="clssubhead" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_crtdate" runat="server" CssClass="Label" Text='<%# Bind("courtdate","{0:d}") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Crt Time">
                                                        <HeaderStyle CssClass="clssubhead" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_crttime" runat="server" CssClass="Label" Text='<%# Bind("courttime") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Status">
                                                        <HeaderStyle CssClass="clssubhead" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_crtstatus" runat="server" CssClass="Label" Text='<%# Bind("shortdescription") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Loc">
                                                        <HeaderStyle CssClass="clssubhead" Width="50px" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_crtloc" runat="server" CssClass="Label" Text='<%# Bind("settingInformation") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Open D" SortExpression="sortopendate">
                                                        <HeaderStyle CssClass="clsaspcolumnheader" VerticalAlign="Middle" HorizontalAlign="Left" />
                                                        <ItemStyle VerticalAlign="Top" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblOpenDate" CssClass="Label" runat="server" Text='<%# bind("opendate","{0:d}") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Open By" SortExpression="rep">
                                                        <HeaderStyle CssClass="clsaspcolumnheader" VerticalAlign="Middle" HorizontalAlign="Center" />
                                                        <ItemStyle VerticalAlign="Top" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblopenby" CssClass="Label" runat="server" Text='<%# bind("rep") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Update D" SortExpression="sortrecsortdate">
                                                        <HeaderStyle CssClass="clsaspcolumnheader" VerticalAlign="Middle" HorizontalAlign="Center" />
                                                        <ItemStyle VerticalAlign="Top" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbllastupdate" CssClass="Label" runat="server" Text='<%# Eval("recsortdate","{0:d}") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Closed D" SortExpression="closeddate">
                                                        <HeaderStyle CssClass="clsaspcolumnheader" VerticalAlign="Middle" HorizontalAlign="Center" />
                                                        <ItemStyle VerticalAlign="Top" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblcloseddate" CssClass="Label" runat="server" Text='<%# bind("closeddate") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Priority" SortExpression="Priority">
                                                        <HeaderStyle CssClass="clsaspcolumnheader" VerticalAlign="Middle" HorizontalAlign="Left" />
                                                        <ItemStyle VerticalAlign="Top" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPriority" CssClass="Label" runat="server" Text='<%# bind("priority") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="ASSN">
                                                        <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_assignto" runat="server" Text='<%# Eval("AssignedTo")%>' CssClass="Label"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Follow Up Date">
                                                        <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_followupdate" runat="server" Text='<%# Eval("followupdate")%>'
                                                                CssClass="Label"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="COMP.%">
                                                        <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_percentage" runat="server" Text='<%# Eval("completion") + "%" %>'
                                                                CssClass="Label"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imgbtn_close" runat="server" CommandName="close" ImageUrl="~/Images/cross.gif"
                                                                CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Fid").ToString() + "-" +  DataBinder.Eval(Container.DataItem, "ticketid_pk").ToString() + "-" + DataBinder.Eval(Container.DataItem, "CaseTypeId").ToString() %>'
                                                                ToolTip="Close Ticket" />
                                                            <asp:HiddenField ID="HFFid2" runat="server" Value='<%# Bind("Fid") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <PagerStyle HorizontalAlign="Center" />
                                                <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                    FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="display: none">
                                            <asp:Button ID="btnDum" runat="server" Text="Button" OnClick="btnDum_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td background="../images/separator_repeat.gif" height="11" style="width: 980px">
                </td>
            </tr>
            <tr>
                <td style="width: 980px">
                    <uc1:Footer ID="Footer1" runat="server" />
                </td>
            </tr>
        </table>
    </div>

    <script language="vbscript">
    
        Function ConfirmYesNo(msg)
                                         
                       
            if  MsgBox(msg,36,"Microsoft Internet Explorer                      ") = 6 then
            ConfirmYesNo =  true
            else
            ConfirmYesNo = false
            end if 
        
        End function             


    </script>

    </form>
</body>
</html>

using System;
using System.Data;
using System.Web.UI.WebControls;
using lntechNew.Components;
using HTP.Components;
using lntechNew.Components.ClientInfo;
using ReportSettingControl;

namespace HTP.Reports
{
    /// <summary>
    /// Bad Addresses Report class
    /// </summary>
    public partial class BadAddresses : System.Web.UI.Page
    {
        readonly ValidationReports _reports = new ValidationReports();
        readonly clsSession _clsSession = new clsSession();
        BussinessLogic Blogic = new BussinessLogic();
        readonly clsCase _cCase = new clsCase();
        DataTable dtRecords = new DataTable();
        DataSet _dsVal;
        private int _reportId;
        int day = 0;
        private const string PageUrl = "/Reports/BadAddresses.aspx";
        /// <summary>
        /// init event of page
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            ShowSetting.OnErr += new HTP.WebControls.ShowSetting.ErrHandler(ShowSetting_OnErr);
            ShowSetting.dbBind += new HTP.WebControls.ShowSetting.Databind(ShowSetting_dbBind);
            base.OnInit(e);
        }

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (_clsSession.IsValidSession(this.Request, this.Response, this.Session) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else
                {
                    if (!IsPostBack)
                    {
                        Fillgrid();
                    }
                    UpdateFollowUpInfo2.PageMethod += new HTP.WebControls.PageMethodHandler(UpdateFollowUpInfo2_PageMethod);
                    Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);                    
                    Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
                    Pagingctrl.GridView = gv_Records;
                }
                Pagingctrl.Visible = true;
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }
 
        /// <summary>
        /// Gridview page size handler
        /// </summary>
        /// <param name="pageSize"></param>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            if (pageSize > 0)
            {
                gv_Records.PageIndex = 0;
                gv_Records.PageSize = pageSize;
                gv_Records.AllowPaging = true;
            }
            else
            {
                gv_Records.AllowPaging = false;
            }
            Fillgrid();

        }
        
        /// <summary>
        /// Refresh grid with updated records
        /// </summary>
        void UpdateFollowUpInfo2_PageMethod()
        {
            Fillgrid();
        }

        /// <summary>
        /// Grid View Page Index Changing Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_Records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (e.NewPageIndex != -1)
                {
                    gv_Records.PageIndex = e.NewPageIndex;
                    Fillgrid();
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }
        /// <summary>
        /// Grid View Row Command Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_Records_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "btnclick")
                {                   
                                      
                    var rowId = Convert.ToInt32(e.CommandArgument);
                    //var firstname = ((HiddenField)gv_Records.Rows[rowId].FindControl("hf_fname")).Value;
                    var lastname = ((HiddenField)gv_Records.Rows[rowId].FindControl("hf_lname")).Value;
                    //var causeno = ((HiddenField)gv_Records.Rows[rowId].FindControl("hf_causeno")).Value;
                    //var ticketno = ((HiddenField)gv_Records.Rows[rowId].FindControl("hf_ticketno")).Value;
                    var ticketid = ((HiddenField)gv_Records.Rows[rowId].FindControl("hf_TicketID")).Value;
                    var followupDate = ((Label)gv_Records.Rows[rowId].FindControl("lbl_followupdate")).Text;
                    DateTime nextfollow = (string.IsNullOrEmpty(followupDate)) ? Convert.ToDateTime("01/01/1900") : Convert.ToDateTime(followupDate);
                    ViewState["TicketId"] = ticketid;
                    const string court = "BadAddresses";
                    _cCase.TicketID = Convert.ToInt32(ticketid);
                    var commments = _cCase.GetGeneralCommentsByTicketId();
                    UpdateFollowUpInfo2.Freezecalender = true;
                    UpdateFollowUpInfo2.Title = "Bad Addresses";
                    UpdateFollowUpInfo2.followUpType = Components.FollowUpType.BadAddressFollowUpDate;
                                    
                   // var courtid = Convert.ToInt32(((HiddenField)gv_Records.Rows[rowId].FindControl("hf_courtid")).Value);

                    string url = Request.Url.AbsolutePath.ToString();
                    DataTable dtDays = Blogic.GetBusinessLogicInformationByURL(url, "Days");
                    if (dtDays.Rows.Count > 0)
                        _reportId = Convert.ToInt32(dtDays.Rows[0]["Report_ID"]);

                    dtDays = Blogic.GetBusinessLogicSetDaysInformation("Days", _reportId.ToString());
                    if (dtDays.Rows.Count > 0)
                        day = Convert.ToInt32(dtDays.Rows[0]["Attribute_Value"].ToString());

                    UpdateFollowUpInfo2.binddate(day.ToString(), DateTime.Today, nextfollow, lastname, commments, mpeTrafficwaiting.ClientID, court, Convert.ToInt32(ticketid));
                    
                    //UpdateFollowUpInfo2.binddate(businessDaysToStop, clsGeneralMethods.GetBusinessDayDate(DateTime.Now, Convert.ToInt32(defaultFollowupDate)), Convert.ToDateTime(nextfollow),  lastname, ticketno,  commments, mpeTrafficwaiting.ClientID, court, Convert.ToInt32(ticketid));
                    mpeTrafficwaiting.Show();
                    Pagingctrl.Visible = true;
                }
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                lbl_Message.Text = ex.ToString();
            }
        }

        /// <summary>
        /// Grid View Row Data Bound Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_Records_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ((LinkButton)e.Row.FindControl("img_Add")).CommandArgument = e.Row.RowIndex.ToString();
                    pnlFollowup.Style["display"] = "none";
                }
            }

            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                lbl_Message.Text = ex.ToString();
            }
        }
        /// <summary>
        /// Show All Check box Checked Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void chkShowAllUserRecords_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                Fillgrid();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }
        /// <summary>
        /// Show Setting Control Databind
        /// </summary>
        void ShowSetting_dbBind()
        {
            Fillgrid();

        }

        /// <summary>
        /// ShowSetting display message on error
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="ErrorMsg"></param>
        void ShowSetting_OnErr(object sender, string ErrorMsg)
        {
            lbl_Message.Text = ErrorMsg;

        }
        /// <summary>
        /// Populate grid control
        /// </summary>
        private void Fillgrid()
        {
            
            try
            {
                _dsVal = _reports.GetBadAddresses(chkShowAll.Checked);
                dtRecords = _dsVal.Tables[0];
               // GenerateSerialNo(dtRecords); // generating serial number for data grid.
                Pagingctrl.GridView = gv_Records;
                gv_Records.DataSource = dtRecords;
                gv_Records.DataBind();
                Pagingctrl.PageCount = gv_Records.PageCount;
                Pagingctrl.PageIndex = gv_Records.PageIndex;
                Pagingctrl.SetPageIndex();
                lbl_Message.Text = "";
                if (_dsVal.Tables[0].Rows.Count < 1)
                {
                    lbl_Message.Text = "No Records Found";
                }

            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        /// <summary>
        /// Paging Control Page Index Changed Method
        /// </summary>
        void Pagingctrl_PageIndexChanged()
        {
            gv_Records.PageIndex = Pagingctrl.PageIndex - 1;
            Fillgrid();
        }
        
        /// <summary>
        /// Refresh parent grid after closing report setting control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnPostBack_Click(object sender, EventArgs e)
        {
            
            Session["DISPLAY_REPORT_SETTING"] = "0";
            Session["InsertFlag"] = "0";
            Response.Redirect("~/Reports/BadAddresses.aspx?sMenu=137", false);
        }
        //sameeullah  1/4/2011 8488 No need to generate serail Numbers,code is added in datagridview 
        
        ///// <summary>
        ///// Generate serial numbers
        ///// </summary>
        ///// <param name="dtRecords"></param>
        //private void GenerateSerialNo(DataTable dtRecords)
        //{
        //    var sno = 1;
        //    if (dtRecords.Columns.Contains("sno") == false)
        //    {
        //        dtRecords.Columns.Add("sno");
        //    }

        //    if (dtRecords.Rows.Count >= 1)
        //        dtRecords.Rows[0]["sno"] = 1;

        //    if (dtRecords.Rows.Count >= 2)
        //    {
        //        for (var i = 1; i < dtRecords.Rows.Count; i++)
        //        {
        //            if (dtRecords.Rows[i - 1]["TICKETID_PK"].ToString() != dtRecords.Rows[i]["TICKETID_PK"].ToString())
        //            {
        //                dtRecords.Rows[i]["sno"] = ++sno;
        //            }
        //        }
        //    }

        //}
        /// <summary>
        /// event for sort of gridview 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_Records_Sorting(object sender, GridViewSortEventArgs e)
        {
            string sortExpression = e.SortExpression;
            Session["SortExpression"] = sortExpression;

            if (GridViewSortDirection == SortDirection.Ascending)
            {

                GridViewSortDirection = SortDirection.Descending;
                Session["SortDirection"] = " desc";
                SortGridView(sortExpression, " desc");

            }

            else
            {

                GridViewSortDirection = SortDirection.Ascending;
                Session["SortDirection"] = " asc";
                SortGridView(sortExpression, " asc");

            }
            Pagingctrl.PageCount = gv_Records.PageCount;
            Pagingctrl.PageIndex = gv_Records.PageIndex;
            Pagingctrl.SetPageIndex();


        }

        ///<summary>
        /// property to get/ set  sorting direction in veiwstate
        ///</summary>
        public SortDirection GridViewSortDirection
        {

            get
            {

                if (ViewState["sortDirection"] == null)

                    ViewState["sortDirection"] = SortDirection.Ascending;

                return (SortDirection)ViewState["sortDirection"];

            }

            set { ViewState["sortDirection"] = value; }

        }
        /// <summary>
        /// method for setting sort experssion of gridview
        /// </summary>
        /// <param name="sortExpression">sort column</param>
        /// <param name="direction"> direction of the sort</param>
        private void SortGridView(string sortExpression, string direction)
        {

            // You can cache the DataTable for improving performance

            Fillgrid();

            DataTable dt = dtRecords;

            DataView dv = new DataView(dt);

            dv.Sort = sortExpression + direction;

            gv_Records.DataSource = dv;

            gv_Records.DataBind();
                        
            Pagingctrl.PageCount = gv_Records.PageCount;
            Pagingctrl.PageIndex = gv_Records.PageIndex;
            Pagingctrl.SetPageIndex();


        }

    }
}

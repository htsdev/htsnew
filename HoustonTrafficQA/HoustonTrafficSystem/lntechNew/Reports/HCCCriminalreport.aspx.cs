using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace HTP.Reports
{
    public partial class HCC_Criminalreport : System.Web.UI.Page
    {

        #region Variables

        public HccCriminalReport HCCReport = new HccCriminalReport();
        DataSet ds = new DataSet();
        clsLogger bugTracker = new clsLogger();
        clsViolations ClsViolations = new clsViolations();
        int pageindex = 0;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {


            if (!IsPostBack)
            {
                try
                {
                    
                    lbl_Message.Text = "";
                    resetProperties();
                    FillControls();
                    ViewState["tabindex"] = "1";
                 
                    
                }
                catch (Exception ex)
                {
                    lbl_Message.Text = ex.Message;
                    bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                }
            }

        }

        //protected void ddl_vio_level_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    ResetGrid();
        //    lbl_Message.Text = "";
        //    if (ddl_vio_level.SelectedValue != "-1")
        //    {
        //        FillViolationDesc(Convert.ToInt32(ddl_vio_level.SelectedValue));
        //    }

        //    ViewState["tabindex"] = "3";
        //    SetActiveTab();
        //}

        protected void btn_save_cdi_Click(object sender, EventArgs e)
        {
            try
            {
                
                lbl_Message.Text = "";
                HCCReport.SearchType = "1";
                DG_Searchresult.PageIndex = 0;
                BindDataGrid();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;

            }

        }

        protected void DG_Searchresult_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            HCCReport.SearchType = Convert.ToString(ViewState["tabindex"]);
            DG_Searchresult.PageIndex = e.NewPageIndex;
            BindDataGrid();
        }

        protected void ddlPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            DG_Searchresult.PageIndex = ddlPageNo.SelectedIndex;
            pageindex = ddlPageNo.SelectedIndex * DG_Searchresult.PageSize;
            HCCReport.SearchType = Convert.ToString(ViewState["tabindex"]);
            BindDataGrid();
        }

        protected void btn_Reset_cdi_Click(object sender, EventArgs e)
        {
            DG_Searchresult.PageIndex = 0;
            ResetGrid();
        }

        protected void DG_Searchresult_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string HF = ((HiddenField)e.Row.FindControl("HFBookingNumber")).Value.ToString();
                    ((HyperLink)e.Row.FindControl("hlnkSNo")).Attributes.Add("onclick", "return OpenPopup('" + HF + "','" + ((Label)e.Row.FindControl("lbl_causeno")).Text + "');");
                    int val=Convert.ToInt32 (((HiddenField)e.Row.FindControl("hfConnection")).Value);
                    
                    //Kazim 3735 5/26/2008 Set the visibilities of the images 
                    
                    if (val>=1)
                    {
                        ((Image)e.Row.FindControl("imgRight")).Visible = true;
                        ((Image)e.Row.FindControl("imgCross")).Visible = false;
                    }
                    else
                    {
                        ((Image)e.Row.FindControl("imgRight")).Visible = false;
                        ((Image)e.Row.FindControl("imgCross")).Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        #endregion

        #region Methods

        public void SetProperties()
        {
            HCCReport.CDI = ddl_cdi.SelectedValue.ToString();
            HCCReport.CaseNumber = txt_causenumber.Text.Trim();
            HCCReport.LastName = txt_LastName.Text.Trim();
            HCCReport.FirstName = txt_FirstName.Text.Trim();
            HCCReport.Spn = txt_spn.Text.Trim();
            HCCReport.CourtRoom = txt_courtroomnew.Text.Trim();
            // Agha Usman 4239 06/25/2008
            HCCReport.PageSize = int.Parse(ddlPagesize.SelectedValue);

            //Kazim 3569 4/7/2008 Assign values to Dispostion and Level variables  
           
            HCCReport.Disposition = ddl_disposition.SelectedValue;
            
            //Kazim 3735 5/13/2008 Set the ViolationDescription property 
            
            HCCReport.ViolationDescription = txt_MatterDescription.Text; 
            HCCReport.Level = ddl_Level.SelectedValue ;

            //by khalid for attorney
            HCCReport.Attorney = drp_Attorney.SelectedValue;
            if ((this.txt_yy.Text != string.Empty) && (this.txt_mm.Text != string.Empty) && (this.txt_dd.Text != string.Empty))
                HCCReport.DOB = new DateTime(int.Parse(this.txt_yy.Text), int.Parse(this.txt_mm.Text), int.Parse(this.txt_dd.Text)).ToShortDateString();
            else
                HCCReport.DOB = string.Empty;

            if ((cl_st_setting_date.PostedDate != string.Empty) && (cl_end_setting_date.PostedDate != string.Empty))
            {
                HCCReport.NextSettingDateFrom = cl_st_setting_date.SelectedDate.ToShortDateString();
                HCCReport.NextSettingDateTo = cl_end_setting_date.SelectedDate.ToShortDateString();
            }
            else
            {
                HCCReport.NextSettingDateFrom = "";
                HCCReport.NextSettingDateTo = "";
            }

            if ((cal_recdate.PostedDate != string.Empty) && (cal_Torecdate.PostedDate != string.Empty))
            {
                HCCReport.DataLoadDateFrom = cal_recdate.SelectedDate.ToShortDateString();
                HCCReport.DataLoadDateTo = cal_Torecdate.SelectedDate.ToShortDateString();
            }
            else
            {
                HCCReport.DataLoadDateFrom = "";
                HCCReport.DataLoadDateTo = "";
            }
        }

        public void BindDataGrid()
        {

            try
            {
                ResetGrid();
                //Set Other Properties
                SetProperties();
                //Search Database For Records
                ds = new DataSet();
                ds = HCCReport.SearchRecords();

                //Kazim 3735 5/13/2008 Call method to generate Serial Nos 
                
                ds=GenerateSeialNo(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    DG_Searchresult.DataSource = ds;
                    DG_Searchresult.DataBind();
                    lblCurrPage.Text = (DG_Searchresult.PageIndex + 1).ToString();
                    FillPageList();
                    ddlPageNo.SelectedIndex = DG_Searchresult.PageIndex;
                    tblresult.Visible = true;

                }
                else
                {
                    lbl_Message.Text = "No Records Found ";
                    tblresult.Visible = false;

                }

                // Display Active Tab                
            }
            catch (Exception ex)
            {
                tblresult.Visible = false;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                throw new ApplicationException(ex.Message, ex);

            }
        }

        private void resetProperties()
        {
            HCCReport.SearchType = "1";
            HCCReport.CDI = "001";
            HCCReport.CaseNumber = " ";
            HCCReport.LastName = " ";
            HCCReport.FirstName = " ";
            HCCReport.Spn = " ";
            HCCReport.DOB = " ";
            HCCReport.Attorney = " ";
            HCCReport.SettingReason = " ";
            HCCReport.NextSettingDateFrom = " ";
            HCCReport.NextSettingDateTo = " ";
            HCCReport.SentenceEndDateFrom = " ";
            HCCReport.SentenceEndDateTo = " ";
            HCCReport.BookingDateFrom = " ";
            HCCReport.BookingDateTo = " ";
            HCCReport.ReleaseDateFrom = " ";
            HCCReport.ReleaseDateTo = " ";
            HCCReport.DataLoadDateFrom = " ";
            HCCReport.DataLoadDateTo = " ";
            HCCReport.ViolationDescription = " ";
            HCCReport.Disposition = " ";
            HCCReport.AddressVerification = " ";
            HCCReport.Zipcode = " ";
            HCCReport.CourtRoom = " ";
            HCCReport.Floor = " ";
        }

        private void FillControls()
        {
            try
            {
                //Binding Court Division indicator CDI
                DataTable cdi = HCCReport.Fill_ddl_list("usp_Get_AllCDI", "JIMS");
                ddl_cdi.Items.Clear();
                ddl_cdi.DataSource = cdi;
                ddl_cdi.DataTextField = "description";
                ddl_cdi.DataValueField = "code";
                ddl_cdi.DataBind();
                ddl_cdi.Items.Insert(0, new ListItem("-- All --", "-1"));
                //ddl_cdi.Items.Insert(0, new ListItem("-- Choose --", ""));
                               
                //khalid
                drp_Attorney.DataTextField = "Name";
                drp_Attorney.DataValueField = "Name";
                drp_Attorney.DataSource = HCCReport.Get_Attorney_List();
                drp_Attorney.DataBind();
                drp_Attorney.Items.Insert(0, new ListItem("-- Choose --", "-1"));
                DateTime dt = DateTime.Today;
                dt = dt.AddDays(-1);

                // Getting violation level(violation category)
               // DataTable categories = HCCReport.Fill_ddl_list("USP_Get_All_Violation_Categories", "traffictickets");
                
                DataSet chargelevel = ClsViolations.GetAllChargeLevel(3037);
                ddl_Level.Items.Clear();
                ddl_Level.DataSource = chargelevel;
                ddl_Level.DataTextField = "LevelCode";
                ddl_Level.DataValueField = "LevelCode";
                ddl_Level.DataBind();
                ddl_Level.Items.Insert(0, new ListItem("-- Choose --", "0"));

                //Get all Disposition Codes
                DataTable dispositioncodes = HCCReport.Fill_ddl_list("usp_Get_All_Disposition_Codes", "JIMS");
                ddl_disposition.Items.Clear();
                ddl_disposition.DataSource = dispositioncodes;
                ddl_disposition.DataTextField = "description";
                ddl_disposition.DataValueField = "code";
                ddl_disposition.DataBind();
                ddl_disposition.Items.Insert(0, new ListItem("-- Choose --", ""));
               

                //cal_recdate.SelectedDate = dt;
                //cal_Torecdate.SelectedDate = dt;
                
           
            
            
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
            }
        }

        private void FillPageList()
        {
            try
            {
                int i = 0;
                ddlPageNo.Items.Clear();
                for (i = 1; i <= DG_Searchresult.PageCount; i++)
                {
                    ddlPageNo.Items.Add("Page - " + i.ToString());
                    ddlPageNo.Items[i - 1].Value = i.ToString();
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        private void ResetGrid()
        {
            //DataSet ds = null;

            DG_Searchresult.DataSource = null;
            DG_Searchresult.DataBind();
            ddlPageNo.Items.Clear();
            tblresult.Visible = false;
        }

        //Kazim 3735 5/13/2008 Create method to generate serialnos

        private DataSet GenerateSeialNo(DataSet ds)
        {
            int count=0;
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                ds.Tables[0].Columns.Add("SNo");
                for (int a = 0; a < ds.Tables[0].Rows.Count; a++)
                {
                    ds.Tables[0].Rows[a]["SNo"] = ++count;

                }
            }
            return ds;
        }

        #endregion

        protected void txt_causenumber_TextChanged(object sender, EventArgs e)
        {

        }

        

    }
}


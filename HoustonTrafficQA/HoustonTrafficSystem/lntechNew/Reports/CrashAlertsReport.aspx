﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CrashAlertsReport.aspx.cs" Inherits="HTP.Reports.CrashAlertsReport" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Crash Alerts Report</title>
    <link href="../Styles.css" type="text/css" rel="stylesheet" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <style type="text/css">
        .clsLeftPaddingTable
        {
            padding-left: 5px;
            font-size: 8pt;
            color: #123160;
            font-family: Tahoma;
            background-color: #EFF4FB;
        }
        .clsInputadministration
        {
            border-right: #3366cc 1px solid;
            border-top: #3366cc 1px solid;
            font-size: 8pt;
            border-left: #3366cc 1px solid;
            width: 90px;
            color: #123160;
            border-bottom: #3366cc 1px solid;
            font-family: Tahoma;
            background-color: white;
            text-align: left;
        }
        .clsInputCombo
        {
            border-right: #e52029 1px solid;
            border-top: #e52029 1px solid;
            font-weight: normal;
            list-style-position: outside;
            font-size: 8pt;
            border-left: #e52029 1px solid;
            color: #123160;
            border-bottom: #e52029 1px solid;
            font-family: Tahoma;
            list-style-type: square;
            background-color: white;
        }
        .label
        {
            font-family: Tahoma;
            font-size: 8pt;
            color: #123160;
            border-bottom-width: 0;
            border-left-width: 0;
            border-right-width: 0;
            border-top-width: 0;
            text-align: left;
        }
        .clsbutton
        {
            font-weight: bold;
            font-size: 8pt;
            border-left-color: #ffcc66;
            border-bottom-color: #ffcc66;
            cursor: hand;
            color: #3366cc;
            border-top-color: #ffcc66;
            font-family: Tahoma;
            background-color: #ffcc66;
            text-align: center;
            border-right-color: #ffcc66;
        }
        A:link
        {
            font-size: 8pt;
            font-family: Verdana,Arial, Helvetica, sans-serif;
            color: #4169E1;
            text-decoration: none;
            font-weight: bold;
        }
        A:Visited
        {
            font-size: 8pt;
            font-family: Verdana,Arial, Helvetica, sans-serif;
            color: #4169E1;
            text-decoration: none;
            font-weight: bold;
        }
    </style>
    
    <script language="javascript" type="text/javascript">
        function refreshContent() {
             document.getElementById("tdData").style.display = 'none';
             __doPostBack('lbRefresh', '');
        }

        function IsCheck() {
            var chkAlldates = document.getElementById("cb_showalldates");
            var fromdate = document.getElementById("fromdate");
            var todate = document.getElementById("todate");

            if (chkAlldates.checked == true) {
                fromdate.disabled = true;
                todate.disabled = true;

            }
            else {
                fromdate.disabled = false;
                todate.disabled = false;
            }

        }
        
        function DeleteConfirm() {
            var isDelete = confirm("Are you sure you want to Cancel this fax?");
            if (isDelete == true) {
                return true;
            }

            else {
                return false;
            }
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">    
    <div>
        <table id="TableMain" align="center" border="0" cellpadding="0" cellspacing="0" style="z-index: 101"
            width="950px">
            <tbody>
                <tr>
                    <td colspan="4" style="height: 14px">
                        <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="TableSub" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td background="../images/separator_repeat.gif" colspan="7" height="11">
                                </td>
                            </tr>
                            <tr>
                            <td>
                            <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                    <td>
                                        <asp:RadioButton ID="rdoStatusType" runat="server" Text="Status Type" CssClass="clssubhead" GroupName = "CrashAlert" Checked="true" />
                                    </td>
                                    <td align="left" style="height: 21px"> 
                                     
                                    <%-- <strong><span class="clssubhead">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Status Type&nbsp; </span>
                                    </strong>--%>
                                    <asp:DropDownList ID="dd_StatusType" runat="server" CssClass="clsInputCombo" 
                                        OnSelectedIndexChanged="dd_StatusType_SelectedIndexChanged">
                                        <asp:ListItem Value="All Clients" Selected="True">All Clients</asp:ListItem>
                                        <asp:ListItem Value="All Quote Clients">All Quote Clients</asp:ListItem>
                                        <asp:ListItem Value="All Non Clients">All Non Clients</asp:ListItem>
                                        <asp:ListItem Value="Client (F, L, YR, G, Zip)">Client (F, L, YR, G, Zip)</asp:ListItem>
                                        <asp:ListItem Value="Client (F,L,YR,Zip)">Client (F,L,YR,Zip)</asp:ListItem>
                                        <asp:ListItem Value="Client (F,L,YR)">Client (F,L,YR)</asp:ListItem>
                                        <asp:ListItem Value="Client (L,Zip)">Client (L,Zip)</asp:ListItem>
                                        <asp:ListItem Value="Quote (F, L, YR, G, Zip)">Quote (F, L, YR, G, Zip)</asp:ListItem>
                                        <asp:ListItem Value="Quote (F,L,YR,Zip)">Quote (F,L,YR,Zip)</asp:ListItem>                                        
                                        <asp:ListItem Value="Quote (F,L,YR)">Quote (F,L,YR)</asp:ListItem>
                                        <asp:ListItem Value="Quote (L,Zip)">Quote (L,Zip)</asp:ListItem>                                        
                                        <asp:ListItem Value="Non Client (F, L, YR, G, Zip)">Non Client (F, L, YR, G, Zip)</asp:ListItem>
                                        <asp:ListItem Value="Non Client (F,L,YR,Zip)">Non Client (F,L,YR,Zip)</asp:ListItem>
                                        <asp:ListItem Value="Non Client (F,L,YR)">Non Client (F,L,YR)</asp:ListItem>
                                        <asp:ListItem Value="Non Client (L,Zip)">Non Client (L,Zip)</asp:ListItem>                                        
                                    </asp:DropDownList>                                  
                                </td>
                                <td>
                                <asp:RadioButton ID="rdoCreationDate" runat="server" Text="Record Creation Date" CssClass="clssubhead" GroupName = "CrashAlert" />
                                </td>
                                <td>
                                    <asp:Label ID="Label6" runat="server" CssClass="clssubhead">From</asp:Label>&nbsp;
                                    <ew:CalendarPopup ID="calFrom1" runat="server" AllowArbitraryText="False" CalendarLocation="Left"
                                        ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma" Font-Size="8pt"
                                        ImageUrl="../images/calendar.gif" Nullable="True" PadSingleDigits="True" ShowClearDate="True"
                                        ShowGoToToday="True" ToolTip="Record Creation Date" Width="80px">
                                        <TextboxLabelStyle CssClass="clstextarea" />
                                        <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Black" />
                                        <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Gray" />
                                        <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Black" />
                                        <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Black" />
                                        <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                    </ew:CalendarPopup>
                                </td>
                                <td>
                                    <asp:Label ID="Label7" runat="server" CssClass="clssubhead">To</asp:Label>&nbsp;
                                    <ew:CalendarPopup ID="calTo1" runat="server" AllowArbitraryText="False" CalendarLocation="Left"
                                        ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma" Font-Size="8pt"
                                        ImageUrl="../images/calendar.gif" Nullable="True" PadSingleDigits="True" ShowClearDate="True"
                                        ShowGoToToday="True" ToolTip="Record Creation Date" UpperBoundDate="12/31/9999 23:59:00"
                                        Width="80px">
                                        <TextboxLabelStyle CssClass="clstextarea" />
                                        <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Black" />
                                        <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Gray" />
                                        <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Black" />
                                        <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Black" />
                                        <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                    </ew:CalendarPopup>
                                </td> 
                                <td>
                                    <asp:Button ID="btnSearch" runat = "server" Text="Search" CssClass="clsbutton"  
                                        onclick="btnSearch_Click" />
                                </td> 
                                </tr>
                            </table>
                                
                            </td>
                                
                            </tr>
                            <tr>
                                <td align="center" style="width: 850px">
                                    <asp:Label ID="lbl_Message" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td background="../images/separator_repeat.gif" colspan="7" height="11">
                                </td>
                            </tr>
                            <tr>
                                <td background="../Images/subhead_bg.gif" class="clssubhead" align="right" style="height: 34px">
                                 <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                </td>
                            </tr>
                            <%--<tr>
                                <td align="center">
                                 <img src="../images/plzwait.gif" alt="" />&nbsp;<asp:Label ID="lbl1" runat="server"
                                                Text="Please Wait ......" CssClass="clssubhead"></asp:Label>
                                        
                                </td>
                            </tr>--%>
                            <tr>
                                <td id="tdData" runat="server">
                                
                                    <asp:GridView ID="gvDocs" runat="server" AutoGenerateColumns="False" Width="100%"
                                                AllowPaging="True" OnPageIndexChanging="gv_records_PageIndexChanging" CellPadding="3"
                                                PageSize="20" CssClass="clsLeftPaddingTable" OnRowCommand="gv_records_RowCommand"
                                                OnRowDataBound="gv_records_RowDataBound">
                                          <Columns>                                         
                                            <asp:BoundField DataField="Name" HeaderText="File Name">
                                              <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="LastWriteTime" DataFormatString="{0:f}" HeaderText="File Creation Date">
                                              <ItemStyle HorizontalAlign="Center" Width="30%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Length" DataFormatString="{0:#,### bytes}" HeaderText="File Size">
                                              <ItemStyle HorizontalAlign="Right" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="">
                                            <HeaderStyle CssClass="clsaspcolumnheader" Font-Size="Smaller" Width="8%" />
                                            <ItemTemplate>
                                            <asp:LinkButton ID="lnkDownload" runat="server" onclick="lnkDownload_Click" CommandArgument='<%# Eval("Name") %>'>Download</asp:LinkButton>
                                               
                                            </ItemTemplate>
                                            </asp:TemplateField>
                                          </Columns>                                         
                                        </asp:GridView>                                        
                                    <asp:LinkButton ID="lbRefresh" runat="server" Style="display: none">Refresh</asp:LinkButton>
                                </td>
                            </tr>
                            <tr>
                                <td width="100%" background="../Images/separator_repeat.gif" height="11">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <uc1:Footer ID="Footer1" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    </form>
</body>
</html>

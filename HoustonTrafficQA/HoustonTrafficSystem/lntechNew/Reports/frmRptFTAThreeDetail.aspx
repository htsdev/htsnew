<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.Reports.frmRptFTAThreeDetail" Codebehind="frmRptFTAThreeDetail.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>FTA Trhee Analysis</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE id="tblMain" style="Z-INDEX: 101" cellSpacing="0" cellPadding="0" width="780" border="0"
				align="center">
				<TR>
					<TD align="left">
						<uc1:ActiveMenu id="ActiveMenu1" runat="server"></uc1:ActiveMenu></TD>
				</TR>
				<tr>
					<td background="../images/separator_repeat.gif" height="11"></td>
				</tr>
				<tr vAlign="middle">
					<td align="left"><IMG height="18" src="../Images/head_icon.gif" width="25">&nbsp;<STRONG>FTA 
							Three Report Detail</STRONG></td>
				</tr>
				<tr>
					<td background="../images/separator_repeat.gif" height="11"></td>
				</tr>
				<TR>
					<TD align="center" vAlign="top">
						<asp:label id="lblMessage" runat="server" Width="248px" ForeColor="Red" Height="13px"></asp:label>
					</TD>
				</TR>
				<tr>
					<td background="../images/headbar_headerextend.gif" height="5"></td>
				</tr>
				<TR>
					<td><TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0">
							<TBODY>
								<tr>
									<td width="15%" class="clssubhead" background="../images/headbar_midextend.gif" height="20">&nbsp;
										<asp:Label id="lblType" runat="server"></asp:Label></td>
									<td width="85%" class="clssubhead" align="right" background="../images/headbar_midextend.gif"
										height="20">
										<asp:Label id="Label1" runat="server">Mail Date:</asp:Label>&nbsp;
										<asp:Label id="lblMailDate" runat="server"></asp:Label>
										&nbsp;</td>
								</tr>
				</TR>
			</TABLE>
			</TD>
			<tr>
				<td background="../images/headbar_footerextend.gif" colSpan="2" height="10"></td>
			</tr>
			<TR>
				<TD align="center">
					<TABLE id="tblGrid" cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>
							<TD width="100%">
								<asp:DataGrid id="dgFTA" runat="server" Width="100%" AutoGenerateColumns="False" ShowFooter="True"
									CssClass="clsleftpaddingtable">
									<Columns>
										<asp:TemplateColumn HeaderText="No.">
											<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
											<ItemTemplate>
												<asp:Label id="lblSerial" runat="server"></asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:BoundColumn DataField="Client" HeaderText="Client">
											<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
											<ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
										</asp:BoundColumn>
										<asp:TemplateColumn HeaderText="Case Number">
											<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
											<ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
											<ItemTemplate>
												<asp:HyperLink id=hlkCaseNo runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.refCaseNumber") %>'>
												</asp:HyperLink>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:BoundColumn DataField="Amount" HeaderText="Amount" DataFormatString="{0:C}">
											<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
											<ItemStyle HorizontalAlign="Right" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
										</asp:BoundColumn>
										<asp:TemplateColumn Visible="False" HeaderText="ticketid">
											<ItemTemplate>
												<asp:Label id=lblTicketId runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'>
												</asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
									</Columns>
									<PagerStyle NextPageText="Next &gt;&gt;" PrevPageText="&lt;&lt; Prev " HorizontalAlign="Center"></PagerStyle>
								</asp:DataGrid></TD>
						</TR>
					</TABLE>
					<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>
							<TD width="10%"></TD>
							<TD width="70%"></TD>
							<TD align="right" width="20%">
								<asp:Label id="lblTotal" runat="server" Font-Size="Larger" Font-Bold="True"></asp:Label>&nbsp;</TD>
						</TR>
					</TABLE>
				</TD>
			</TR>
			<TR>
				<TD align="center"></TD>
			</TR>
			<TR>
				<TD align="center"></TD>
			</TR>
			<TR>
				<TD align="center"></TD>
			</TR>
			<TR>
				<TD align="center"></TD>
			</TR>
			<TR>
				<TD align="left"></TD>
			</TR>
			</TBODY></TABLE>
		</form>
	</body>
</HTML>

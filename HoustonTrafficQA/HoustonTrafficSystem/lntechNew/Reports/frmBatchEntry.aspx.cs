using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;


//using lntechNew.WordLetter;
using lntechNew.Components;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;

namespace lntechNew.Reports
{
	/// <summary>
	/// Summary description for frmBatchEntry.
	/// </summary>
	public partial class frmBatchEntry : System.Web.UI.Page
	{
		int ticketno;
		int empid;
		string  batch;
		int LetterType;
        int Exists;
        int deleteExistingReceipt;
		clsSession uSession = new clsSession();
		clsCrsytalComponent Cr = new clsCrsytalComponent();
        clsENationWebComponents cls_db = new clsENationWebComponents();
        

		private void Page_Load(object sender, System.EventArgs e)
		{
			//ticketno =Convert.ToInt32(uSession.GetCookie("sTicketID",this.Request));
			ticketno =Convert.ToInt32(Request.QueryString["casenumber"]);
			empid =Convert.ToInt32(uSession.GetCookie("sEmpID",this.Request));
			 batch = Request.QueryString["Batch"].ToString();
             Exists = Convert.ToInt32(Request.QueryString["Exists"]);
             deleteExistingReceipt = Convert.ToInt32(Request.QueryString["overwriteReceipt"]);

             if (Exists == 1)
             {
                 RemoveExistingEntry();
             }

             if (deleteExistingReceipt == 1)
             {
                 RemoveExistingReceipt();
             }

			LetterType =Convert.ToInt32(Request.QueryString["LetterID"].ToString());
			if (LetterType == 5)
			{
				CreateCONTINUANCEReport();
			}
			else if (LetterType == 2)
			{
				//trial
				Create_TrialNotification_Report();
			}
			else if (LetterType == 6)
			{
				CreateApprovalReport() ;
			}
			else if (LetterType==3)
			{
				CreateReceipt(); 
			}
            HttpContext.Current.Response.Write("<script language='javascript'> opener.location.reload(); self.close();   </script>");
            //HttpContext.Current.Response.Write("<script language='javascript'> opener.close(); self.close();   </script>");
		}

        private void RemoveExistingEntry()
        {
            //FileInfo fi = new FileInfo("");
            //fi.Delete(); 
            string[] keys = {"@TicketID"};
            object[] values = {ticketno };
            cls_db.ExecuteSP("USP_HTS_BATCHLETTERS_DELETE_EXISTING", keys, values);
            
        }

        private void RemoveExistingReceipt()
        {
            string[] keys = { "@TicketID" };
            object[] values = { ticketno };
            cls_db.ExecuteSP("USP_HTS_BATCHLETTERS_DELETE_EXISTING_RECEIPT", keys, values);
            
        }

		public void CreateCONTINUANCEReport()
 
		{ 		
			
			string[] key = {"@Ticketid","@employeeid"};
			object[] value1 = {ticketno,empid};
			string filename  = Server.MapPath("") + "\\CONTINUANCE.rpt";			
			Int32  LetterType = 5;			
			Cr.CreateReportForEntry(filename,"USP_HTS_Continuance_Letter", key, value1,batch,LetterType, this.Session, this.Response );
						
		}
		public void Create_TrialNotification_Report()
		{
			try
			{
				string [] key2 = {"@TicketIDList","@empid"};
				object [] value12 = {ticketno,empid};				
				string filename  = Server.MapPath("") + "\\Trial_Notification.rpt";
                //Yasir Kamal 7590 03/29/2010 trial letter modifications
                if (batch == "false")
                {
                    Cr.CreateReportForEntry(filename, "USP_HTS_Trialletter", key2, value12, batch, LetterType, this.Session, this.Response);
                }
                else
                {
                    Cr.InsertIntoBatch(Convert.ToInt32(value12[0]), Convert.ToInt32(value12[1]), LetterType);
                }
			}
			catch(Exception ex)
			{
				Response.Write(ex.Message);
			}
		}	
		public void CreateApprovalReport() 
		{ 			
			string[] key = {"@ticketid"};
			object[] value1 = {ticketno};			
			string filename  = Server.MapPath("") + "\\Wordreport.rpt";			
			Int32 LetterType = 6;			
			Cr.CreateReportForEntry(filename,"USP_HTS_LETTER_OF_REP", key, value1,batch,LetterType,empid, this.Session, this.Response );
		}
		public void CreateReceipt()
		{
			string[] key = {"@ticketid"};
			object[] value1 = {ticketno};					
			string filename  = Server.MapPath("") + "\\receipt.rpt";
			Int32 LetterType = 3;			
			Cr.CreateReportForEntry(filename,"USP_HTS_get_receipt_info", key, value1,batch,LetterType,empid, this.Session, this.Response );
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}

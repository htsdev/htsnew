﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmCertifiedLORBatchPrint.aspx.cs"
    Inherits="HTP.Reports.frmCertifiedLORBatchPrint" %>

<%@ Register Src="~/WebControls/faxcontrol.ascx" TagName="Faxcontrol" TagPrefix="Fc" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="AjaxToolkit" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Certified Letter of Rep</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="C#" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../Styles.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../scripts/subModal.css" />

    <script type="text/javascript" src="../scripts/common.js"></script>

    <script type="text/javascript" src="../scripts/subModal.js"></script>

    <style type="text/css">
        .modalBackground
        {
            background-color: Gray;
            filter: alpha(opacity=70);
            opacity: 0.7;
        }
        .modalPopup
        {
            background-color: #FFD9D5;
            border-width: 3px;
            border-style: solid;
            border-color: Gray;
            padding: 3px;
            width: 250px;
            position: static;
            left: 200px;
            top: -65px;
        }
        .MyCalendar .ajax__calendar_container
        {
            border: 1px solid #646464;
            background-color: lemonchiffon;
            color: red;
        }
    </style>
</head>

<script type="text/javascript" language="javascript">
		function refresh()
		{//Nasir 5681 05/19/2009 refresh targeted update panel
		 opener.location.href = opener.location;
		
		}
		
		function changeStackOrder()
        {      
        document.getElementById("testframe").style.display = "none";
        }
        
        function showDialog()        
        {           
          document.getElementById('testframe').style.display='none';
          showPopWin('../reports/ViewFaxControl.aspx', 520, 370, null);         
          return true;
        }
        function HideDailog()
        {           
          document.getElementById('testframe').style.display='block';          
          hidePopWin(true,0);
        }
        
</script>

<body ms_positioning="GridLayout" onload="refresh();">
    <form id="Form1" method="post" runat="server" visible="True">
    <table border="0" width="100%" height="100%">
        <tr>
            <td height="46" colspan="3" background="../Images/subhead_bg.gif" style="height: 34px">
            </td>
        </tr>
        <tr>
            <td style="height: 33px" valign="bottom" align="right">
                <table class="clsmainhead" id="Table3" cellspacing="0" cellpadding="0" width="185"
                    align="left" border="0">
                    <tr>
                        <td width="31">
                            <img height="17" src="../Images/head_icon.gif" width="31">
                        </td>
                        <td width="140" valign="baseline" class="clssubhead">
                            Certified Letter of Rep
                        </td>
                    </tr>
                </table>
            </td>
           
        </tr>
        <tr id="trframe">
            <td colspan="3">
                <iframe id="testframe" src="RptCeritifiedLORPrint.aspx?CMN=<%=ViewState["vCMN"]%>"
                    width="100%" height="95%" scrolling="auto" frameborder="1"></iframe>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>

<%@ Page Language="C#" AutoEventWireup="true" Codebehind="PostTrialSatisfactionReport.aspx.cs"
    Inherits="lntechNew.Reports.PostTrialSatisfactionReport" %>

<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2"
    Namespace="eWorld.UI" TagPrefix="ew" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Post-Trial Satisfaction Report</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
    
    function ShowPopup(RecordID, RecDate)
    {
    var WinSettings = "center:yes;resizable:no;dialogHeight:250px;dialogwidth:520px;scroll:no";
    var thisdate = new Date();
    
        var Arg = 1;
        var conf = window.showModalDialog("Popup_UpdateComments.aspx?RecordID="+RecordID+"&CrtDate="+RecDate+"&Time="+thisdate.getTime(),Arg,WinSettings);
        
        if(conf == -1 || conf == null)
        {
            return false;
        }
        else
        {
            document.getElementById('hf_comments').value = conf;
            return true;
        }
    
    return(false);
    }
    
    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table cellspacing="0" cellpadding="0" width="780" align="center" border="0">
                <tr>
                    <td>
                        <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td width="100%" background="../Images/separator_repeat.gif" height="11">
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lbl_text" runat="server" Text="Date: " CssClass="clslabel"></asp:Label>
                        <ew:CalendarPopup ID="cal_Date" runat="server" ControlDisplay="TextBoxImage" ImageUrl="~/Images/calendar.gif"
                            Width="86px" EnableHideDropDown="True">
                            <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Black" />
                            <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Black" />
                            <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Gray" />
                            <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Black" />
                            <TextboxLabelStyle CssClass="clsinputadministration" />
                        </ew:CalendarPopup>
                        &nbsp;
                        <asp:Button ID="btn_Submit" runat="server" CssClass="clsbutton" Text="Submit" OnClick="btn_Submit_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td width="100%" background="../Images/separator_repeat.gif" height="11">
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="lbl_Message" runat="server" Text="" ForeColor="red"></asp:Label></td>
                </tr>
                <tr>
                    <td>
                        <asp:GridView ID="gv_records" runat="server" AutoGenerateColumns="False" OnRowDataBound="gv_records_RowDataBound"
                            Width="780px" PageSize="20" CssClass="clsleftpaddingtable" OnRowCommand="gv_records_RowCommand">
                         <Columns>
                                <asp:TemplateField HeaderText="S.No">
                                    <HeaderStyle CssClass="clssubhead" Width="10px" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_sno" Font-Size="XX-Small" runat="server" CssClass="Label" Text='<%# Eval("SNo") %>'
                                            Visible="false"></asp:Label>
                                        <asp:HyperLink ID="hl_sno" runat="server" Text='<%# Eval("SNo") %>' NavigateUrl='<%# "../ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" +Eval("TicketID_PK") %>'></asp:HyperLink>
                                        <asp:HiddenField ID="hf_CourtID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "CourtID") %>' />
                                        <asp:HiddenField ID="hf_BondFlag" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "BondFlag") %>' />
                                        <asp:HiddenField ID="hf_rowtype" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "RowType") %>' />
                                        <asp:HiddenField ID="hf_TicketID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "TicketID_PK") %>' />
                                        <asp:HiddenField ID="hf_RecordID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "RecordID")%>' />
                                        <asp:HiddenField ID="hf_CourtDate" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "CourtDateMain", "{0:D}")%>' />
                                        <asp:Label ID="lbl_CourtNumber" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CourtNumber") %>'
                                            Visible="false"></asp:Label>
                                        <asp:Label ID="lbl_CourtTime" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CourtDateMain", "{0:t}")%>'
                                            Visible="false"></asp:Label>
                                        <asp:Label ID="lbl_LastUpdate" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LastUpdate")%>'
                                            Visible="false"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name">
                                    <HeaderStyle CssClass="clssubhead" Width="130px" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_name1" runat="server" Text='<%# Eval("LastName") + " , " +  Eval("FirstName") %>'
                                            Font-Size="XX-Small" Visible="False"></asp:Label>
                                        <asp:HyperLink ID="lbl_name" Font-Size="XX-Small" runat="server" NavigateUrl='<%# "../ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" +Eval("TicketID_PK") %>'
                                            Text='<%# Eval("LastName") + " , " +  Eval("FirstName") %>'></asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Ticket No">
                                    <HeaderStyle CssClass="clssubhead" Width="60px" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_ticketno" runat="server" Text='<%# Eval("ticketnumber") %>' Font-Size="XX-Small"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Snap Shot Status">
                                    <HeaderStyle CssClass="clssubhead" Width="160px" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_sstatus" runat="server" Font-Size="XX-Small" Text='<%# DataBinder.Eval(Container.DataItem, "SnapShotStatus") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Verified Status">
                                    <HeaderStyle CssClass="clssubhead" Width="160px" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_astatus" runat="server" Font-Size="XX-Small" Text='<%# DataBinder.Eval(Container.DataItem, "VerifiedStatus") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Contact No">
                                    <HeaderStyle CssClass="clssubhead"  Width="170px"/>
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_vstatus" runat="server" Font-Size="XX-Small" Text='<%# DataBinder.Eval(Container.DataItem, "ContactNo") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Comments">
                                    <HeaderStyle CssClass="clssubhead" Width="125px" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_comments" runat="server" Font-Size="XX-Small" Text='<%# DataBinder.Eval(Container.DataItem, "Comments") %>' ForeColor="Red"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="ibtn_Comments" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "TicketID_PK") %>'
                                            ImageUrl="~/Images/edit.gif" ToolTip="Write comments" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerStyle HorizontalAlign="Center" />
                        </asp:GridView>
                        <asp:GridView ID="gv_records_1" runat="server" AutoGenerateColumns="False" OnRowDataBound="gv_records_RowDataBound"
                            Width="780px" PageSize="20" CssClass="clsleftpaddingtable" OnRowCommand="gv_records_RowCommand">
                        <Columns>
                                <asp:TemplateField HeaderText="S.No">
                                    <HeaderStyle CssClass="clssubhead" Width="10px" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_sno" Font-Size="XX-Small" runat="server" CssClass="Label" Text='<%# Eval("SNo") %>'
                                            Visible="false"></asp:Label>
                                        <asp:HyperLink ID="hl_sno" runat="server" Text='<%# Eval("SNo") %>' NavigateUrl='<%# "../ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" +Eval("TicketID_PK") %>'></asp:HyperLink>
                                        <asp:HiddenField ID="hf_CourtID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "CourtID") %>' />
                                        <asp:HiddenField ID="hf_BondFlag" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "BondFlag") %>' />
                                        <asp:HiddenField ID="hf_rowtype" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "RowType") %>' />
                                        <asp:HiddenField ID="hf_TicketID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "TicketID_PK") %>' />
                                        <asp:HiddenField ID="hf_RecordID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "RecordID")%>' />
                                        <asp:HiddenField ID="hf_CourtDate" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "CourtDateMain", "{0:D}")%>' />
                                        <asp:Label ID="lbl_CourtNumber" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CourtNumber") %>'
                                            Visible="false"></asp:Label>
                                        <asp:Label ID="lbl_CourtTime" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CourtDateMain", "{0:t}")%>'
                                            Visible="false"></asp:Label>
                                        <asp:Label ID="lbl_LastUpdate" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LastUpdate")%>'
                                            Visible="false"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name">
                                    <HeaderStyle CssClass="clssubhead" Width="130px" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_name1" runat="server" Text='<%# Eval("LastName") + " , " +  Eval("FirstName") %>'
                                            Font-Size="XX-Small" Visible="False"></asp:Label>
                                        <asp:HyperLink ID="lbl_name" Font-Size="XX-Small" runat="server" NavigateUrl='<%# "../ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" +Eval("TicketID_PK") %>'
                                            Text='<%# Eval("LastName") + " , " +  Eval("FirstName") %>'></asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Ticket No">
                                    <HeaderStyle CssClass="clssubhead" Width="60px" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_ticketno" runat="server" Text='<%# Eval("ticketnumber") %>' Font-Size="XX-Small"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Snap Shot Status">
                                    <HeaderStyle CssClass="clssubhead" Width="160px" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_sstatus" runat="server" Font-Size="XX-Small" Text='<%# DataBinder.Eval(Container.DataItem, "SnapShotStatus") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Verified Status">
                                    <HeaderStyle CssClass="clssubhead" Width="160px" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_astatus" runat="server" Font-Size="XX-Small" Text='<%# DataBinder.Eval(Container.DataItem, "VerifiedStatus") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Contact No">
                                    <HeaderStyle CssClass="clssubhead"  Width="170px"/>
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_vstatus" runat="server" Font-Size="XX-Small" Text='<%# DataBinder.Eval(Container.DataItem, "ContactNo") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Comments">
                                    <HeaderStyle CssClass="clssubhead" Width="125px" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_comments" runat="server" Font-Size="XX-Small" Text='<%# DataBinder.Eval(Container.DataItem, "Comments") %>' ForeColor="Red"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="ibtn_Comments" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "TicketID_PK") %>'
                                            ImageUrl="~/Images/edit.gif" ToolTip="Write comments" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerStyle HorizontalAlign="Center" />
                        </asp:GridView>
                        <asp:HiddenField ID="hf_comments" runat="server" Value="" />
                    </td>
                </tr>
                <tr>
                    <td width="100%" background="../Images/separator_repeat.gif" height="11">
                    </td>
                </tr>
                <tr>
                    <td>
                        <uc1:Footer ID="Footer1" runat="server" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>

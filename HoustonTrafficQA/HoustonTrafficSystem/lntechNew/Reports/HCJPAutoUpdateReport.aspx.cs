﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;

namespace HTP.Reports
{
    //Noufil Khan 4748 09/08/2008 new report
    public partial class HCJPAutoUpdateReport : System.Web.UI.Page
    {
        ValidationReports reports = new ValidationReports();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    fillgrid();
                }
                Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
                
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }
        protected void gv_Records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (e.NewPageIndex != -1)
                {
                    gv_Records.PageIndex = e.NewPageIndex;
                    fillgrid();
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        private void fillgrid()
        {
            reports.getReportData(gv_Records, lbl_Message, ValidationReport.HcjpAutoUpdateReport);
            Pagingctrl.PageCount = gv_Records.PageCount;
            Pagingctrl.PageIndex = gv_Records.PageIndex;
            Pagingctrl.SetPageIndex();
        }        

        void Pagingctrl_PageIndexChanged()
        {
            gv_Records.PageIndex = Pagingctrl.PageIndex - 1;
            fillgrid();
        }
    }
}

   
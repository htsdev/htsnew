using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components;

namespace lntechNew.Reports
{
	/// <summary>
	/// Summary description for ScanPreviewReport.
	/// </summary>
	public partial class ScanPreviewReport : System.Web.UI.Page
	{
		int docid;
		int docnum;
        //Change by Ajmal

		IDataReader SqlDr;		
		clsENationWebComponents ClsDb = new clsENationWebComponents(); 
		clsGeneralMethods ClsGM = new clsGeneralMethods();
        
		private void Page_Load(object sender, System.EventArgs e)
		{
			docid = (int) Convert.ChangeType(Request.QueryString["docid"],typeof(int));
			docnum= (int) Convert.ChangeType(Request.QueryString["docnum"],typeof(int));
			//  docid=43;
			//	docnum=66;
			PreviewReport();
		}
		public void PreviewReport()
		{
			string[] key = {"@docid","@docnum"};
			object[] value1 = {docid,docnum};
			SqlDr = ClsDb.Get_DR_BySPArr("sp_display_scanned_image",key,value1);	
			string rptPath = ClsGM.AddImageToPDF(Server.MapPath(""),docid,SqlDr, this.Session, this.Response);		
			Session["DocNm"] = rptPath;
			Response.Redirect("frame.aspx");
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion





	}
}

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FlagsReport.aspx.cs" Inherits="HTP.Reports.FlagsReport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Flags Report</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="MainTable" cellspacing="0" cellpadding="0" align="center" border="0" style="width: 820px">
            <tr>
                <td>
                    <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                </td>
            </tr>
            <tr>
                <td background="../images/separator_repeat.gif" height="11" style="width: 850px">
                </td>
            </tr>
            <tr>
                <td align="center" style="width: 850px">
                    <asp:Label ID="lbl_Message" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="clsLabel">
                    Flag:
                    <asp:DropDownList ID="ddl_Flags" runat="server" CssClass="clsInputCombo" AutoPostBack="true"
                        OnSelectedIndexChanged="ddl_Flags_OnSelectedIndexChanged">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td background="../images/separator_repeat.gif" height="11" style="width: 850px">
                </td>
            </tr>
            <tr>
                <td background="../Images/subhead_bg.gif" class="clssubhead" align="right" style="height: 32px">
                    <table style="width: 100%">
                        <tr>
                            <td>
                            </td>
                            <td style="text-align: right">
                                <uc3:PagingControl ID="Pagingctrl" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td background="../images/separator_repeat.gif" height="11" style="width: 850px">
                </td>
            </tr>
            <tr id="tr_NotOnSystem" runat="server">
                <td width="100%">
                    <table width="100%">
                        <tr>
                            <td class="clslabel">
                            </td>
                        </tr>
                    </table>
                    <table width="100%">
                    </table>
                    <asp:GridView ID="dg_NotOnSystem" runat="server" AutoGenerateColumns="False" Width="100%"
                        OnItemDataBound="ASdg_ItemDataBound" OnSortCommand="dg_NotOnSystem_SortCommand"
                        AllowSorting="True" AllowPaging="True" PageSize="30" CssClass="clsLeftPaddingTable"
                        OnPageIndexChanging="dg_NotOnSystem_PageIndexChanging" OnSorting="dg_NotOnSystem_Sorting">
                        <HeaderStyle></HeaderStyle>
                        <Columns>
                            <asp:TemplateField HeaderText="ticketid" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="ASTIDS" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.ticketid") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<u>SNO</u>" SortExpression="SNO">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblSNo" runat="server" CssClass="clsLabel" NavigateUrl='<%# String.Concat("../clientinfo/ViolationFeeOld.aspx?caseNumber=",DataBinder.Eval(Container, "DataItem.ticketid"),"&amp;search=0") %>'
                                        Text='<%# DataBinder.Eval(Container, "DataItem.SNO") %>'></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="false">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chk1" runat="server"></asp:CheckBox>
                                </ItemTemplate>
                                <HeaderTemplate>
                                    <asp:CheckBox ID="CheckAlll" runat="server" onclick="return select_deselectAll2(this.checked, this.id);"
                                        Checked="false" />
                                </HeaderTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<U>CAUSE NO</U>" SortExpression="CAUSENO">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="AScauseno" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.CAUSENO") %>'></asp:Label>
                                    <%--<asp:HyperLink ID="AScauseno" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.CAUSENO") %>'></asp:HyperLink>--%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<u>TICKET NO</u>" SortExpression="TICKETNO">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="Asticketno" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.TICKETNO") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<u>LAST NAME</u>" SortExpression="lastname">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="ASlastname" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.lastname") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<u>FIRST NAME</u>" SortExpression="firstname">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="ASfirstname" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.firstname") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<u>DL</u>" SortExpression="dlnumber">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lblDLNumber" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.dlnumber") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<u>DOB</u>" SortExpression="DOB">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="ASDOB" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.DOB","{0:d}") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<u>Status</u>" SortExpression="status">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="ASStatus" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.status") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<u>CRT</u>" SortExpression="COURTDATE">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="ASCourtDate" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.COURTDATE","{0:d}") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<u>Time</u>" SortExpression="Time">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="btime" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.Time") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerStyle HorizontalAlign="Center" />
                        <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                            FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td background="../images/separator_repeat.gif" height="11" style="width: 850px">
                </td>
            </tr>
            <tr>
                <td>
                    <uc1:Footer ID="Footer1" runat="server" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

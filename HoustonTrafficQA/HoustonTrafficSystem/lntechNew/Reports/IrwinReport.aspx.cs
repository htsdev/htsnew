using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using FrameWorkEnation.Components;
using System.Xml;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;


namespace lntechNew.Reports
{
	
	public partial class IrwinReport : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Button btnSearch;
		protected System.Web.UI.WebControls.TextBox txtSdate;
		protected System.Web.UI.WebControls.TextBox txtEndDate;
		protected System.Web.UI.WebControls.Button BtnPrint;
		protected System.Web.UI.WebControls.Label lblMessage;
		protected System.Web.UI.WebControls.DataGrid dgrdQuote;		
		protected System.Web.UI.WebControls.RegularExpressionValidator RegularExpressionValidator2;
		protected System.Web.UI.WebControls.RegularExpressionValidator RegularExpressionValidator1;
		protected System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator2;
		protected System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator1;
		protected System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator3;
		protected System.Web.UI.WebControls.ValidationSummary ValidationSummary1;
		protected System.Web.UI.WebControls.RadioButton RadioButton_ViolationDate;
		protected System.Web.UI.WebControls.RadioButton Rbtn_recdt; 
		protected System.Web.UI.WebControls.RadioButtonList opt_btn;
		clsENationWebComponents ClsDb = new clsENationWebComponents();
		clsCrsytalComponent ClsCr = new clsCrsytalComponent();	
		clsLogger bugTracker = new clsLogger();
		string FTADates;
		int flag;		
		int empid= 3992;
		

		private void Page_Load(object sender, System.EventArgs e)
		{
			BtnPrint.Enabled=false;
			if (!IsPostBack)
			{
				
				DateTime  dt = new DateTime( DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day);
				//dt = dt.AddDays(-45);
				DateTime  dt1 = new DateTime( DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day);
				txtSdate.Text =(string) dt.ToString("MM/dd/yyyy") ;
				txtEndDate.Text = (string) dt1.ToString("MM/dd/yyyy") ;
			}
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.opt_btn.SelectedIndexChanged += new System.EventHandler(this.opt_btn_SelectedIndexChanged);
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
			this.dgrdQuote.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dgrdQuote_PageIndexChanged);
			this.dgrdQuote.SelectedIndexChanged += new System.EventHandler(this.dgrdQuote_SelectedIndexChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnSearch_Click(object sender, System.EventArgs e)
		{
			try
			{
				FillGrid();	
			}
			catch(Exception ex)
			{
				Response.Write(ex.Message);
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}

		}
		private void FillGrid()
		{
			try
			{
				DataSet ds;			
				if (opt_btn.Items[0].Selected==true)
					flag = 0;                  //RecordLoadDate Option is selected        
				else
					flag = 1;                  //ViolationDate Option is selected 
				string[] key = {"@sdate","@edate","@flag"};
				object[] value1 = {txtSdate.Text,txtEndDate.Text,flag};			
				ds = ClsDb.Get_DS_BySPArr("SP_IrwinLetter",key,value1);
				//SP "SP_IrwinLetter" to display no of letters,total number of prints with respect to dates  				
				dgrdQuote.DataSource = ds;
				lblMessage.Text = "";			      				
				dgrdQuote.DataBind();
				if (ds.Tables[0].Rows.Count  < 1 )
				{
					lblMessage.Text = "No record found";
				}
				BtnPrint.Enabled=true;
			}
			catch(Exception ex)
			{
				Response.Write(ex.Message);
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}			
		}
	
		private void dgrdQuote_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}
		private void dgrdQuote_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			dgrdQuote.CurrentPageIndex = e.NewPageIndex;			
			FillGrid();			
		}
		private void PrintReport()
		{
			try
			{
				//BtnPrint.Enabled=true;
				foreach (DataGridItem i in this.dgrdQuote.Items) 
				{
					CheckBox Child1 = (CheckBox) i.FindControl("Checkbox1");
					if (Child1.Checked) 
					{
						FTADates+=Child1.Text + ","	;
					}
				}
				if (opt_btn.Items[0].Selected==true)
					flag = 0;
				else
					flag = 1;
				string [] key = {"@DateArr","@empid","@LetterType","@flag"};
				object [] value1 = {FTADates.ToString(),empid,"zee k letter",flag};
				ClsDb.InsertBySPArr("SP_Insert_TblLetterNotes",key,value1);
				//SP "SP_Insert_TblLetterNotes" to insert individual letter plus 
				//batch insert with respect to date 
				string [] key2 = {"@sdate","@flag"};
				object [] value12 = {FTADates.ToString(),flag};				
			//	ClsCr.CreateReport(new irwin_report(),new Irving(),"SP_IrwinReport",key2, value12,this.Session, this.Response);
				// Crystal Method to Generate Letters 
			}
			catch(Exception ex)
			{
				Response.Write(ex.Message);
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}
		private void BtnPrint_Click(object sender, System.EventArgs e)
		{
			PrintReport();			
		}
		private void opt_btn_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}
/*		private void RenderReport()
				{
					rpt.ReportPath="/FTAletterReport/ViewLetter&rs:Command=Render&rs:Format=PDF";
			    	rpt.Parameters = Microsoft.Samples.ReportingServices.ReportViewer.multiState.False;
		
					try
					{
						rpt.ServerUrl=(string)ConfigurationSettings.AppSettings["ReportSever"];;
						string strCase;
						strCase = FTADates; 
						rpt.SetQueryParameter("sdate",strCase.Trim());
					}
					catch (Exception e1)
					{
						Response.Write(e1);
					}
				}

			private void DDLPeriod_SelectedIndexChanged(object sender, System.EventArgs e)
				{
					DateTime  dt = new DateTime( DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day);
					dt = dt.AddDays( - (Int16) Convert.ChangeType(DDLPeriod.SelectedItem.Text, typeof(Int16)));
					DateTime  dt1 = new DateTime( DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day);
					txtSdate.Text =(string) dt.ToString("MM/dd/yyyy") ;
					txtEndDate.Text = (string) dt1.ToString("MM/dd/yyyy") ;
				}	
*/
	}
}

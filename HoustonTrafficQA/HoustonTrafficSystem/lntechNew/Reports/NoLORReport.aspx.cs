using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;

//Waqas 5653 03/21/2009 Namespace modified
namespace HTP.Reports
{
    public partial class NoLORReport : System.Web.UI.Page
    {
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        clsLogger bugTracker = new clsLogger();
        //Waqas 5653 03/21/2009 Changes for No LOR Follow Up
        clsCase cCase = new clsCase();
        clsSession cSession = new clsSession();

        //Waqas 5653 03/21/2009 Changes for No LOR Follow Up
        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lbl_Message.Text = "";
                //Waqas 5653 03/21/2009 Changes for No LOR Follow Up
                if (cSession.IsValidSession(this.Request, this.Response, this.Session) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                if (!IsPostBack)
                {
                    GetRecords();
                }
                UpdateFollowUpInfo.PageMethod += new HTP.WebControls.PageMethodHandler(UpdateFollowUpInfo_PageMethod);
                Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
                Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
                Pagingctrl.GridView = gv_Data;

            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }        

        protected void gv_Data_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label desc = (Label)e.Row.FindControl("lbl_LastName");

                    if (desc.Text.Length > 10)
                    {
                        desc.ToolTip = desc.Text;
                        desc.Text = desc.Text.Substring(0, 10).ToUpperInvariant() + "...";
                    }
                    Label desce = (Label)e.Row.FindControl("lbl_FirstName");

                    if (desce.Text.Length > 10)
                    {
                        desce.ToolTip = desce.Text;
                        desce.Text = desce.Text.Substring(0, 10).ToUpperInvariant() + "...";
                    }

                    Label followup = (Label)e.Row.FindControl("lbl_followup");
                    followup.Text = (followup.Text.Trim() == "01/01/1900") ? "" : followup.Text;
                    ((ImageButton)e.Row.FindControl("img_Add")).CommandArgument = e.Row.RowIndex.ToString();
                }
            }
            catch (Exception)
            {

            }
        }

        protected void gv_Data_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "btnclick")
            {
                int RowID = Convert.ToInt32(e.CommandArgument);
                string firstname = (((HiddenField)gv_Data.Rows[RowID].FindControl("hf_fname")).Value);
                string causeno = (((HiddenField)gv_Data.Rows[RowID].FindControl("hf_causeno")).Value);
                string ticketno = (((HiddenField)gv_Data.Rows[RowID].FindControl("hf_ticketnumber")).Value);
                string lastname = (((HiddenField)gv_Data.Rows[RowID].FindControl("hf_lname")).Value);
                string courtname = (((HiddenField)gv_Data.Rows[RowID].FindControl("hf_courtid")).Value);
                string followupDate = (((Label)gv_Data.Rows[RowID].FindControl("lbl_followup")).Text);

                followupDate = (followupDate.Trim() == "") ? "" : followupDate;

                cCase.TicketID = Convert.ToInt32((((HiddenField)gv_Data.Rows[RowID].FindControl("hf_ticketid_pk")).Value));
                string comm = cCase.GetGeneralCommentsByTicketId();
                UpdateFollowUpInfo.Freezecalender = true;

                UpdateFollowUpInfo.Title = "LOR Follow-up";
                UpdateFollowUpInfo.followUpType = HTP.Components.FollowUpType.LORFollowUpDate;

                string url = Request.Url.AbsolutePath.ToString();
                string[] key = { "@Report_Url", "@Attribute_key" };
                object[] value = { url, "Days" };
                DataTable dtDays = new DataTable();
                
                dtDays = ClsDb.Get_DT_BySPArr("usp_htp_get_reportByUrl", key, value);
                int Report_ID = 0;
                if (dtDays.Rows.Count > 0)
                {
                    Report_ID = Convert.ToInt32(dtDays.Rows[0]["Report_ID"]);
                }
                                
                string[] key1 = { "@Attribute_Key", "@Report_Id" };
                string[] value1 = { "Days", Report_ID.ToString() };

                dtDays = ClsDb.Get_DT_BySPArr("usp_hts_get_BusinessLogicDay", key1, value1);
                int day = 0;
                if (dtDays.Rows.Count > 0)
                {
                    day = Convert.ToInt32(dtDays.Rows[0]["Attribute_Value"].ToString());
                }

                UpdateFollowUpInfo.binddate(day.ToString(), clsGeneralMethods.GetBusinessDayDate(DateTime.Now, day), DateTime.Today, firstname, lastname, ticketno, ((HiddenField)gv_Data.Rows[RowID].FindControl("hf_ticketid_pk")).Value, causeno, comm, mpeTrafficwaiting.ClientID, "NO LOR", "", followupDate);
                
                mpeTrafficwaiting.Show();
                Pagingctrl.Visible = true;
            }
        }

        protected void gv_Data_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_Data.PageIndex = e.NewPageIndex;
                GetRecords();
            }
            catch (Exception ex)
            {

                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void gv_Data_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        #endregion

        //Waqas 5653 03/21/2009 Changes for No LOR Follow Up
        #region Methods
        /// <summary>
        /// To Fill data into Grid
        /// </summary>
        private void GetRecords()
        {
            //Waqas 5653 03/21/2009 Changes for No LOR Follow Up
            DataTable dt = ClsDb.Get_DT_BySPArr("USP_HTS_HMC_NO_LOR");

            if (dt.Rows.Count == 0)
            {
                Pagingctrl.PageCount = 0;
                Pagingctrl.PageIndex = 0;
                Pagingctrl.SetPageIndex();
                lbl_Message.Text = "No Records!";
            }
            else
            {
                dt.Columns.Add("sno");
                DataView dv = dt.DefaultView;
                dt = dv.ToTable();
                GenerateSerialNo(dt); ;
                gv_Data.DataSource = dt;
                gv_Data.DataBind();
                Pagingctrl.PageCount = gv_Data.PageCount;
                Pagingctrl.PageIndex = gv_Data.PageIndex;
                Pagingctrl.SetPageIndex();
            }
        }

        /// <summary>
        /// Method to be called by UpdateFollowUpControl
        /// </summary>
        void UpdateFollowUpInfo_PageMethod()
        {
            GetRecords();
        }

        /// <summary>
        /// Method when Page index changed
        /// </summary>
        void Pagingctrl_PageIndexChanged()
        {
            gv_Data.PageIndex = Pagingctrl.PageIndex - 1;
            GetRecords();
        }

        /// <summary>
        /// Method when page size will be changed
        /// </summary>
        /// <param name="pageSize"></param>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            if (pageSize > 0)
            {
                gv_Data.PageIndex = 0;
                gv_Data.PageSize = pageSize;
                gv_Data.AllowPaging = true;
            }
            else
            {
                gv_Data.AllowPaging = false;
            }
            GetRecords();

        }

        /// <summary>
        /// Method to insert Serial numbers in Gridview
        /// </summary>
        /// <param name="dtRecords"></param>
        public void GenerateSerialNo(DataTable dtRecords)
        {
            int sno = 1;
            if (!dtRecords.Columns.Contains("sno"))
                dtRecords.Columns.Add("sno");


            if (dtRecords.Rows.Count >= 1)
                dtRecords.Rows[0]["sno"] = 1;

            if (dtRecords.Rows.Count >= 2)
            {
                for (int i = 1; i < dtRecords.Rows.Count; i++)
                {
                    if (dtRecords.Rows[i - 1]["ticketid_pk"].ToString() != dtRecords.Rows[i]["ticketid_pk"].ToString())
                    {
                        dtRecords.Rows[i]["sno"] = ++sno;
                    }
                }
            }

        }

        #endregion
    }
}

<%@ Page Language="c#" AutoEventWireup="false" Inherits="HTP.Reports.SplitReport"
    MaintainScrollPositionOnPostback="true" CodeBehind="SplitReport.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register Src="../WebControls/UpdateFollowUpInfo.ascx" TagName="UpdateFollowUpInfo"
    TagPrefix="uc3" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Split Report</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../Styles.css" type="text/css" rel="stylesheet" />

    <script src="../Scripts/Dates.js" type="text/javascript"></script>

    <script src="../Scripts/jsDate.js" type="text/javascript"></script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .modalBackground
        {
            background-color: Gray;
            filter: alpha(opacity=70);
            opacity: 0.7;
        }
        .modalPopup
        {
            background-color: #FFD9D5;
            border-width: 3px;
            border-style: solid;
            border-color: Gray;
            padding: 3px;
            width: 250px;
            position: static;
            left: 200px;
            top: -65px;
        }
        A:link
        {
            font-size: 8pt;
            font-family: Verdana,Arial, Helvetica, sans-serif;
            color: #4169E1;
            text-decoration: none;
            font-weight: bold;
        }
        A:Visited
        {
            font-size: 8pt;
            font-family: Verdana,Arial, Helvetica, sans-serif;
            color: #4169E1;
            text-decoration: none;
            font-weight: bold;
        }
    </style>

    <script language="javascript" type="text/javascript">
	      function OpenEditWin(violationid)
	      {
              var PDFWin
		      PDFWin = window.open("FrmComments.aspx?ticketID="+violationid,"","fullscreen=no,toolbar=no,width=380,height=200,left=120,top=100,status=no,menubar=no,scrollbars=yes,resizable=yes");				
		      return false;				      
	       } 
	  
    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <div>
        <aspnew:ScriptManager ID="ScriptManager1" runat="server">
            <Scripts>
                <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
            </Scripts>
        </aspnew:ScriptManager>
        <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline">
            <ContentTemplate>
                <table id="TableMain" style="z-index: 101" cellspacing="0" cellpadding="0" width="800"
                    align="center" border="0">
                    <tbody>
                        <tr>
                            <td style="height: 14px" colspan="4">
                                <uc2:ActiveMenu ID="ActiveMenu1" runat="server"></uc2:ActiveMenu>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table id="TableSub" cellspacing="0" cellpadding="0" width="800" border="0">
                                    <tr>
                                        <td background="../images/separator_repeat.gif" height="11" align="center">
                                        </td>
                                    </tr>
                                    <tr class="clsLeftPaddingTable">
                                        <td align="right">
                                            <table>
                                                <tr>
                                                    <td align="right">
                                                        <asp:CheckBox ID="chkShowAllUserRecords" runat="server" Text="Show All " CssClass="clssubhead"
                                                            OnCheckedChanged="chkShowAllUserRecords_CheckedChanged" AutoPostBack="true" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td background="../images/separator_repeat.gif" height="11" align="center">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td background="../Images/subhead_bg.gif" class="clssubhead" align="right" style="height: 34px">
                                            <table style="width: 100%">
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td style="text-align: right">
                                                        <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td background="../images/separator_repeat.gif" height="11" align="center">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table id="TableGrid" bgcolor="white" border="0" cellpadding="0" cellspacing="0"
                                    width="100%">
                                    <tr>
                                        <td align="center" style="width: 100%">
                                            <asp:Label ID="lbl_message" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="2" valign="top">
                                            <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlResult">
                                                <ProgressTemplate>
                                                    <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl1" runat="server" Text="Please Wait ......"
                                                        CssClass="clsLabel"></asp:Label>
                                                </ProgressTemplate>
                                            </aspnew:UpdateProgress>
                                            <asp:GridView ID="gv_SplitReport" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                                CssClass="clsLeftPaddingTable" OnPageIndexChanging="gv_SplitReport_PageIndexChanging"
                                                OnRowCommand="gv_SplitReport_RowCommand" OnRowDataBound="gv_SplitReport_RowDataBound"
                                                PageSize="20" Width="100%">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="S#">
                                                        <ItemTemplate>
                                                            &nbsp;<asp:HyperLink ID="HLTicketno" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                                Text='<%# DataBinder.Eval(Container, "DataItem.sno") %>'></asp:HyperLink>
                                                            <asp:HiddenField ID="hf_TicketID" runat="server" Value="<%# Bind('TicketID_PK') %>" />
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Ticket No">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_RefCaseNo" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container.DataItem, "RefCaseNumberseq") %>' />
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Cause Number">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_Causeno" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.casenumassignedbycourt") %>' />
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="X-ref number">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_Xrefnumber" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.Midnum") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Verified Court Info">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_verifieddate" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.VerifiedCourtInfo") %>'>
                                                            </asp:Label>
                                                            <asp:Label ID="lbl_violationid" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.TicketId_pk") %>'
                                                                Visible="false"> </asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Court Location">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_courtname" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.shortname") %>'> </asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Follow-Up Date">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_followupdate" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.FollowUpDate") %>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="img_Add" runat="server" CommandName="btnclick" Text="&lt;img src='../Images/add.gif' border='0'/&gt;" />
                                                            <asp:HiddenField ID="hf_fname" runat="server" Value='<%#Eval("Firstname") %>' />
                                                            <asp:HiddenField ID="hf_lname" runat="server" Value='<%#Eval("Lastname") %>' />
                                                            <asp:HiddenField ID="hf_ticketno" runat="server" Value='<%#Eval("refcasenumberseq") %>' />
                                                            <%--<asp:HiddenField ID="hf_TicketId" runat="server" Value='<%#Eval("ticketid_pk") %>' />--%>
                                                            <asp:HiddenField ID="hf_causeno" runat="server" Value='<%#Eval("casenumassignedbycourt") %>' />
                                                            <asp:HiddenField ID="hf_court_loc" runat="server" Value='<%#Eval("shortname") %>' />
                                                            <%-- <asp:HiddenField ID="hf_courtid" runat="server" Value='<%#Eval("COURTVIOLATIONSTATUSIDMAIN") %>' />--%>
                                                            <asp:HiddenField ID="hf_FollowUpDate" runat="server" Value='<%#Eval("FollowUpDate") %>' />
                                                            <asp:HiddenField ID="hf_NextFollowUpdate" runat="server" Value='<%#Eval("NextFollowUpDate") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle Font-Bold="true" ForeColor="black" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imgRemove" runat="server" CommandName="btnRemove" ImageUrl="../Images/cross.gif"
                                                                ToolTip="Mark as No Split" />
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                </Columns>
                                                <PagerStyle HorizontalAlign="Center" />
                                                <PagerSettings FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;"
                                                    Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td background="../images/separator_repeat.gif" height="11" width="780">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Panel ID="pnlFollowup" runat="server">
                                                <uc3:UpdateFollowUpInfo ID="UpdateFollowUpInfo2" runat="server" Title="FollowUp Date" />
                                            </asp:Panel>
                                            <ajaxToolkit:ModalPopupExtender ID="mpeTrafficwaiting" runat="server" BackgroundCssClass="modalBackground"
                                                PopupControlID="pnlFollowup" TargetControlID="btn">
                                            </ajaxToolkit:ModalPopupExtender>
                                            <asp:Button ID="btn" runat="server" Style="display: none;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <uc1:Footer ID="Footer1" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </ContentTemplate>
        </aspnew:UpdatePanel>
    </div>
    </form>

    <script language="javascript" type="text/javascript">
        document.getElementById("UpdateFollowUpInfo2_cal_followupdate_div").style.zIndex = 10003;
        document.getElementById("UpdateFollowUpInfo2_cal_followupdate_monthYear").style.zIndex = 10004;
    </script>

</body>
</html>

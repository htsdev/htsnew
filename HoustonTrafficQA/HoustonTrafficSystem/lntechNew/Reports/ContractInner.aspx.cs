using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;

namespace HTP.Reports
{
    /// <summary>
    /// Summary description for frmTrial_NotificationDL.
    /// </summary>
    public partial class ContractInner : System.Web.UI.Page
    {
        clsCrsytalComponent ClsCr = new clsCrsytalComponent();
        clsLogger clog = new clsLogger();
        clsSession uSession = new clsSession();
        int TicketIDList = 0;
        int LetterType = 0;
        int empid = 0;
        //Ozair 5505 02/09/2009 violationnumber changed to violationcategoryid
        int violationCategoryID = 0;
        //Ozair 5505 02/09/2009 Removed courtid variable
        int casetypeid = 0;
        int categoryid = 0;
        string filename = string.Empty;

        private void Page_Load(object sender, System.EventArgs e)
        {
            TicketIDList = Convert.ToInt32(Request.QueryString["casenumber"]);
            LetterType = Convert.ToInt32(Request.QueryString["lettertype"]);
            empid = Convert.ToInt32(uSession.GetCookie("sEmpID", this.Request));
            Create_Contract_Report();
        }
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion
        /// <summary>
        /// Fahad 5098 12/03/2008
        /// this method create report according to criteria
        /// </summary>
        public void Create_Contract_Report()
        {
            try
            {
                if (LetterType == 18)
                {
                    //Ozair 5505 02/09/2009 Removed courtid variable
                    casetypeid = 4;
                    filename = Server.MapPath("") + "\\AG_Case_Contract.rpt";

                }
                else if (LetterType == 19)
                {
                    //Ozair 5505 02/09/2009 violationnumber changed to violationcategoryid
                    violationCategoryID = 28;
                    casetypeid = 2;
                    filename = Server.MapPath("") + "\\Criminal_Contract_ALR.rpt";

                }
                else if (LetterType == 20)
                {
                    //Ozair 5505 02/09/2009 violationnumber changed to violationcategoryid
                    violationCategoryID = 27;
                    casetypeid = 2;
                    filename = Server.MapPath("") + "\\Criminal_Contract_DWI.rpt";
                }
                else if (LetterType == 21)
                {
                    //Ozair 5505 02/09/2009 violationnumber changed to violationcategoryid
                    violationCategoryID = 30;
                    casetypeid = 2;
                    filename = Server.MapPath("") + "\\Criminal_Contract_DLSuspension_Hearing.rpt";

                }
                else if (LetterType == 22)
                {
                    //Ozair 5505 02/09/2009 violationnumber changed to violationcategoryid
                    violationCategoryID = 29;
                    casetypeid = 2;
                    filename = Server.MapPath("") + "\\Criminal_Contract_OccupLicense.rpt";

                }
                else if (LetterType == 23)
                {
                    categoryid = 3;
                    casetypeid = 2;
                    filename = Server.MapPath("") + "\\Criminal_Contract_Pretrial.rpt";

                }
                else if (LetterType == 24 || LetterType == 25)
                {
                    //Fahad 5098 12/26/2008
                    if (LetterType == 24)
                    {
                        categoryid = 4;
                        casetypeid = 2;
                        filename = Server.MapPath("") + "\\Criminal_Contract_Trial.rpt";
                    }
                    else
                    {
                        //ozair 5505 02/13/2009 trial note for judge.
                        LetterType = 24;
                        categoryid = 5;
                        casetypeid = 2;
                        filename = Server.MapPath("") + "\\Criminal_Contract_Trial.rpt";
                    }


                }
                //Ozair 5505 02/09/2009 violationnumber changed to violationcategoryid and courtid removed
                string[] key2 = { "@TicketID", "@EmployeeID", "@ViolationCategoryID", "@CaseTypeID", "@CategoryID" };
                object[] value12 = { TicketIDList, empid, violationCategoryID, casetypeid, categoryid };
                ClsCr.CreateReport(filename, "USP_HTP_Get_ContractByParameter", key2, value12, "false", LetterType, this.Session, this.Response);
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
    }
}

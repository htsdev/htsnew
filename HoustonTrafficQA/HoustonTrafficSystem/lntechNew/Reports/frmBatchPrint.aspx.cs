using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.Reports
{
	/// <summary>
	/// Summary description for frmBatchPrint.
	/// </summary>
	public class frmBatchPrint : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.DataGrid dg_Letters;
		protected System.Web.UI.WebControls.DataGrid dg_LetterType;
		protected System.Web.UI.WebControls.DataGrid dg_Names;
		protected System.Web.UI.WebControls.TextBox txtHidID;
		protected System.Web.UI.WebControls.TextBox txtHidType;
		protected System.Web.UI.WebControls.TextBox txthidCount;
		protected System.Web.UI.WebControls.Button btn_PrintNames;
		protected System.Web.UI.HtmlControls.HtmlTableRow TRLetters;
		protected System.Web.UI.WebControls.Label lbl_Message;
		clsENationWebComponents ClsDb = new clsENationWebComponents();
		protected System.Web.UI.WebControls.CheckBox chkHeader;
		protected System.Web.UI.WebControls.Button btn_delete1;
		protected System.Web.UI.WebControls.TextBox txt_letcount1;
		protected System.Web.UI.WebControls.Label lbl_mis;	
		clsSession ClsSession = new clsSession();
		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				if (ClsSession.IsValidSession(this.Session)==false)
				{
					Response.Redirect("../frmlogin.aspx");
				}				
			
		
					DataSet ds_Letters = ClsDb.Get_DS_BySP("USP_HTS_GET_LETTERS");
					dg_Letters.DataSource = ds_Letters;
					dg_Letters.DataBind();	
					txt_letcount1.Text=ds_Letters.Tables[0].Rows.Count.ToString(); 					
					
					chkHeader.Attributes.Add("onclick","javascript: CheckAll();");
				//	TRLetters.Visible = false;
//					btn_delete1.Visible = false;
					dg_LetterType.Visible = false;
					//dg_Names.Visible = false;
				//	btn_PrintNames.Visible =false;
		
				if (txtHidID.Text != "0")
				{
					dg_LetterType.Visible = true;
					string[] key1 = {"@LType"};
					object[] Value1 ={ txtHidID.Text} ;
					DataSet ds_LetterType = ClsDb.Get_DS_BySPArr("USP_HTS_GET_LETTERBATCH",key1,Value1);
					dg_LetterType.DataSource = ds_LetterType ;
					dg_LetterType.DataBind();

					//not to hide detail tickets
					if(txtHidID.Text!=lbl_mis.Text)
					{
						TRLetters.Visible = false;
						dg_Names.Visible = false;
						btn_delete1.Visible = false;
						btn_PrintNames.Visible =false;
					}
					else
					{
						string[] key2 = {"@LType","@BatchDate"};
						object[] Value2 ={txtHidID.Text,txtHidType.Text} ;
						DataSet ds_LetterNames = ClsDb.Get_DS_BySPArr("USP_HTS_GET_BatchTicketID",key2,Value2);
						dg_Names.DataSource = ds_LetterNames  ;
						dg_Names.DataBind();
					}
				}	
				else
				{
					TRLetters.Visible = false;
					btn_delete1.Visible = false;
					btn_PrintNames.Visible =false;
				}
			//	}
			}
			catch(Exception ex)
			{
				lbl_Message.Text = ex.Message;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.dg_Letters.DeleteCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_Letters_DeleteCommand);
			this.dg_Letters.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dg_Letters_ItemDataBound);
			this.dg_LetterType.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_LetterType_ItemCommand);
			this.dg_LetterType.DeleteCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_LetterType_DeleteCommand);
			this.dg_LetterType.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dg_LettersType_ItemDataBound);
			this.btn_delete1.Click += new System.EventHandler(this.btn_delete1_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void dg_Letters_ItemDataBound(object source, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			try
			{
				if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
				{
					LinkButton btnButton = (LinkButton)e.Item.Cells[3].Controls[0]; 
					btnButton.Attributes.Add("onclick","return PromptDelete();");		
					if  ( ((Label) e.Item.FindControl("lbl_LetterCount")).Text == "0") 
					{
						((LinkButton) e.Item.FindControl("lnkbtn_Letter")).Attributes.Add("OnClick","javascript: AssignValue(0);");
						((Button) e.Item.FindControl("btn_Print")).Enabled = false;				
					}
					else
					{
						String lnk =  "javascript: AssignValue('" + ((Label) e.Item.FindControl("lbl_LetterID")).Text + "');";
						((LinkButton) e.Item.FindControl("lnkbtn_Letter")).Attributes.Add("OnClick",lnk);
						string lnk1 ="javascript: return OpenNewWindow(1,'" + ((Label) e.Item.FindControl("lbl_LetterID")).Text + "',1,1/1/1900,0,"+  ((Label) e.Item.FindControl("lbl_print1")).Text +  ");";
						((Button) e.Item.FindControl("btn_Print")).Attributes.Add("OnClick",lnk1);						

					}//flag,Lid,chk,fdate,tid
				}
			}
			catch(Exception ex)
			{
				lbl_Message.Text = ex.Message.ToString();
			}
		}
		

		private void dg_LettersType_ItemDataBound(object source, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			try
			{				
				if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
				{
					LinkButton btnButton = (LinkButton)e.Item.Cells[4].Controls[0]; 
					btnButton.Attributes.Add("onclick","return PromptDelete();");	

					String lnktype =  "javascript: AssignValueType('" + ((Label) e.Item.FindControl("lbl_LetterTypeFileDate")).Text + "');";
					((LinkButton) e.Item.FindControl("lnkLetterType")).Attributes.Add("OnClick",lnktype);
					DateTime dtSend = Convert.ToDateTime(((Label) e.Item.FindControl("lbl_LetterTypeFileDate")).Text);
					string lnk2 ="javascript: return OpenNewWindow(1,'" + txtHidID.Text + "',2,'"+ dtSend.Date  +"',0,"+((Label) e.Item.FindControl("lbl_print2")).Text +");";
					((Button) e.Item.FindControl("btn_printType")).Attributes.Add("OnClick",lnk2);
				}
			}
			catch(Exception ex)
			{
				lbl_Message.Text = ex.Message.ToString();
			}
		}
		
		//When delete by letter type
		private void dg_Letters_DeleteCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			try
			{
				int LetterType=Convert.ToInt32(((Label) e.Item.FindControl("lbl_LetterID")).Text);
				DateTime dt=Convert.ToDateTime("01/01/1900");

				String[] Key2 = {"@LetterID","@chk","@batchDate","@ticketidlist"};
				object[] value2 = {LetterType,1,dt,"0"};
				ClsDb.ExecuteSP("USP_HTS_Delete_LetterBatch",Key2,value2);	
				Page.Response.Redirect(Page.Request.Url.ToString(), false);		
			}
			catch(Exception ex)
			{
				lbl_Message.Text=ex.Message;
			}
		}
		//When delete by letter type and filedate
		private void dg_LetterType_DeleteCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			try
			{
				//string LetterType=((Label) e.Item.FindControl("lbl_LetterID")).Text;
				DateTime filedate=Convert.ToDateTime(((Label) e.Item.FindControl("lbl_LetterTypeFileDate")).Text);

				String[] Key2 = {"@LetterID","@chk","@batchDate","@ticketidlist"};
				object[] value2 = {Convert.ToInt32(txtHidID.Text),2,filedate,"0"};
				ClsDb.ExecuteSP("USP_HTS_Delete_LetterBatch",Key2,value2);	
				Page.Response.Redirect(Page.Request.Url.ToString(), false);		
			}
			catch(Exception ex)
			{
				lbl_Message.Text=ex.Message;
			}
		
		}
		//In order to delete check records
		private void btn_delete1_Click(object sender, System.EventArgs e)
		{
			try
			{
				string Ticketlist="";
				DateTime filedate=DateTime.Now;
				foreach (DataGridItem i in this.dg_Names.Items) 
				{
					CheckBox Child1 = (CheckBox) i.FindControl("chk");
					Label  TicketID= (Label) i.FindControl("lbl_TicketID");
					
					if (Child1.Checked) 
					{
						Ticketlist+=TicketID.Text + ",";					
						filedate=Convert.ToDateTime(((Label)i.FindControl("lbl_FileDate")).Text);	
					}							
				}
				
				String[] Key2 = {"@LetterID","@chk","@batchDate","@ticketidlist"};
				object[] value2 = {Convert.ToInt32(txtHidID.Text),3,filedate,Ticketlist};
				ClsDb.ExecuteSP("USP_HTS_Delete_LetterBatch",Key2,value2);	
				Page.Response.Redirect(Page.Request.Url.ToString(), false);		
			}
			catch(Exception ex)
			{
				lbl_Message.Text=ex.Message;
			}            		
		}

		private void dg_LetterType_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			if(e.CommandName=="clicked")
			{
				try
				{
					if (txtHidType.Text  != "0")
					{
						lbl_mis.Text= txtHidID.Text;
						TRLetters.Visible = true;
						dg_Names.Visible = true;
						btn_delete1.Visible=true;
						btn_PrintNames.Visible=true;
						TRLetters.Visible = true;
						chkHeader.Checked = false;
						string[] key2 = {"@LType","@BatchDate"};
						object[] Value2 ={txtHidID.Text,txtHidType.Text} ;
						DataSet ds_LetterNames = ClsDb.Get_DS_BySPArr("USP_HTS_GET_BatchTicketID",key2,Value2);
						dg_Names.DataSource = ds_LetterNames  ;
						dg_Names.DataBind();

						txthidCount.Text =  dg_Names.Items.Count.ToString();	
						btn_PrintNames.Visible = true;
						btn_delete1.Visible = true;
						btn_PrintNames.Attributes.Add("OnClick","javascript:return OpenLastGrid();");
						btn_delete1.Attributes.Add("OnClick","javascript:return OpenLastGridDel();");
					}
				}
				catch(Exception ex)
				{
					lbl_Message.Text=ex.Message; 
				}

			}
		}

		

			
	}
}

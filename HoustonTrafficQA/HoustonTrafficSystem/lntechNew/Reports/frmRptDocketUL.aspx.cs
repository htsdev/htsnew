using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.Reports
{
	/// <summary>
	/// Summary description for frmRptDocketUL.
	/// </summary>
	public partial class frmRptDocketUL : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.DropDownList ddlCourtLoc;
		protected System.Web.UI.WebControls.DropDownList ddlCaseStatus;
		protected eWorld.UI.CalendarPopup dtFrom;
		protected System.Web.UI.WebControls.DataGrid dgDocket;
		protected System.Web.UI.WebControls.Button btnSubmit;
		clsENationWebComponents ClsDb = new clsENationWebComponents();
		protected eWorld.UI.CalendarPopup dtTo;
		clsSession cSession = new clsSession();
		clsLogger bugTracker = new clsLogger();
		DataView dv_Result;
		string StrExp = String.Empty;
		protected System.Web.UI.WebControls.Label lblCurrPage;
		protected System.Web.UI.WebControls.DropDownList ddlPageNo;
		string StrAcsDec = String.Empty;
		protected System.Web.UI.WebControls.Label lblMsg;
		DataSet dsRes;
	
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
			this.ddlPageNo.SelectedIndexChanged += new System.EventHandler(this.ddlPageNo_SelectedIndexChanged);
			this.dgDocket.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dgDocket_PageIndexChanged);
			this.dgDocket.SortCommand += new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.dgDocket_SortCommand);
			this.dgDocket.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgDocket_ItemDataBound);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void Page_Load(object sender, System.EventArgs e)
		{
			if (cSession.IsValidSession(this.Request)==false)
			{
				Response.Redirect("../frmlogin.aspx",false);
			}
			else //To stop page further execution
			{
				// Put user code to initialize the page here
				
				if (!IsPostBack)
				{
					dtTo.SelectedDate = DateTime.Today;
					dtFrom.SelectedDate  = DateTime.Today;
					
					FillCourtLocations();
					FillCaseStatuses();
					//dtFrom.SelectedDate = null;
					//dtTo.Text = "";
					Search();
					btnSubmit.Attributes.Add("OnClick","return CalendarValidation();");
					btnSubmit.Attributes.Add("OnClick", "return validate();");
				}
			}
		}

		private void FillCourtLocations()
		{
		
			DataSet ds = ClsDb.Get_DS_BySP("USP_HTS_GET_All_Courts");
			int idx=0;

			for (idx=-2; idx<ds.Tables[0].Rows.Count; idx++)
			{
				switch (idx)
				{
					case -2:
						ListItem lst = new ListItem("All Courts","ALL");
						ddlCourtLoc.Items.Add(lst);
						break;
					case -1:
						ListItem lst1 = new ListItem("Houston Municipal Court","IN");
						ddlCourtLoc.Items.Add(lst1);
						break;
					case 0:
						ListItem lst2 = new ListItem("All Outside Court","JP");
						ddlCourtLoc.Items.Add(lst2);
						break;
					default:
						ddlCourtLoc.Items.Add(ds.Tables[0].Rows[idx]["shortcourtname"].ToString());
						ddlCourtLoc.Items[ddlCourtLoc.Items.Count -1].Value = ds.Tables[0].Rows[idx]["CourtId"].ToString();
						break;
				}
			}
		}

		private void FillCaseStatuses()
		{
			DataSet ds = ClsDb.Get_DS_BySP("USP_HTS_GET_AllDateTypes");
			int idx=0;

			for (idx=-3; idx<ds.Tables[0].Rows.Count; idx++)
			{
				switch (idx)
				{
					case -3:
						ListItem lst = new ListItem("Open Cases","OPEN");
						ddlCaseStatus.Items.Add(lst);
						break;
					case -2:
						ListItem lst1 = new ListItem("Waiting","WAIT");
						ddlCaseStatus.Items.Add(lst1);
						break;
					case -1:
						ListItem lst2 = new ListItem("Appearance Dates","APP");
						ddlCaseStatus.Items.Add(lst2);
						break;
					case 0:
						ListItem lst3 = new ListItem("DAR/Case Missing","DAR");
						ddlCaseStatus.Items.Add(lst3);
						break;
					default:
						ddlCaseStatus.Items.Add(ds.Tables[0].Rows[idx]["Description"].ToString());
						ddlCaseStatus.Items[ddlCaseStatus.Items.Count -1].Value = ds.Tables[0].Rows[idx]["TypeId"].ToString();
						break;
				}
			}
		}

		private void btnSubmit_Click(object sender, System.EventArgs e)
		{
			try
			{	
				Search();
			}
			catch(Exception ex)
			{
				lblMsg.Text = ex.Message;
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}

		private void FillGrid()
		{
			try
			{			
				DataView dvTemp = (DataView)   cSession.GetSessionObject("dvResult",this.Session);
				
				dgDocket.DataSource = 	dvTemp;
				dgDocket.DataBind();
				GenerateSerial();
				FillPageList();
				SetNavigation();
				if (dgDocket.Items.Count == 0)
					lblMsg.Text = "No record found.";
			}
			catch (Exception ex)
			{
				if (dgDocket.CurrentPageIndex > dgDocket.PageCount -1)
				{
					dgDocket.CurrentPageIndex = 0;
					dgDocket.DataBind();
					GenerateSerial();
					SetNavigation();
				}
				else
				{
					lblMsg.Text = ex.Message;
					bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);

				}
			}

		}
		
		private void PullData()
		{
			//if (dtFrom.Text == "" && dtTo.Text == "" )			
			if (dtFrom.SelectedDate.ToShortDateString() == "1/1/0001" && dtTo.SelectedDate.ToShortDateString() == "1/1/0001" )
			{
				string [] key = {"@courtlocation","@casestatus"};
				object[] value1 = {ddlCourtLoc.SelectedValue, ddlCaseStatus.SelectedValue};
				 dsRes = ClsDb.Get_DS_BySPArr("USP_HTS_Get_DocketReport_UL",key,value1);
			}
			else
			{
				string [] key = {"@courtlocation","@casestatus","@startcourtdate","@endcourtdate"};
				object[] value1 = {ddlCourtLoc.SelectedValue, ddlCaseStatus.SelectedValue, dtFrom.SelectedDate, dtTo.SelectedDate};
				 dsRes = ClsDb.Get_DS_BySPArr("USP_HTS_Get_DocketReport_UL",key,value1);
			}

	
			DataSet dsResTemp;
			if (ddlCaseStatus.SelectedValue == "3" || ddlCaseStatus.SelectedValue == "4" || ddlCaseStatus.SelectedValue == "5" || ddlCaseStatus.SelectedValue == "9" )
				dsResTemp = (DataSet)  InsertBlankRows(dsRes);
			else
				dsResTemp = dsRes;

			//cSession.SetSessionVariable("dvResult",dsResTemp.Tables[0].DefaultView,this.Session);
			cSession.SetSessionVariable("dvResult",dsResTemp.Tables[0].DefaultView,this.Session);
		}

		private void dgDocket_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if(e.Item.ItemType == ListItemType.Header)
			{
				e.Item.Cells.RemoveAt(8);
				e.Item.Cells.RemoveAt(8);
				e.Item.Cells[7].ColumnSpan = 3;

				e.Item.Cells.RemoveAt(6);
				e.Item.Cells[5].ColumnSpan = 2;
			}
			else 
				if(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
				{		
				try
				{
					int iTicketId = (int)  (Convert.ChangeType(((Label)(e.Item.FindControl("lbl_TicketId"))).Text,typeof(int)));
					
					((HyperLink) (e.Item.FindControl("hlk_LName"))).NavigateUrl = "../ClientInfo/ViolationFeeOld.aspx?search=0&casenumber="+ iTicketId ;
				
					// changing row color on mouse hover.....
					e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='#FFF2D9'");
					e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#EFF4FB'");
				}
				catch 
				{
					int idx=0;
					for(idx = e.Item.Cells.Count -1 ; idx> 0 ;idx--)
						e.Item.Cells.RemoveAt(idx);
					e.Item.Cells[0].ColumnSpan = 13;
					e.Item.Cells[0].BackColor = System.Drawing.Color.White;
					e.Item.Cells[0].Height = 10;
				}
				}
		}

		private void GenerateSerial()
		{
			// Procedure for writing serial numbers in "S.No." column in data grid.......	

			
			long sNo=(dgDocket.CurrentPageIndex)*(dgDocket.PageSize);									
			try
			{
				// looping for each row of record.............
				foreach (DataGridItem ItemX in dgDocket.Items) 
				{ 					
					
					try
					{
						int iTicketId =  (int) (Convert.ChangeType(((Label) ( ItemX.FindControl("lbl_Ticketid"))).Text,typeof(int)))  ;
						sNo +=1 ;
						// setting text of hyper link to serial number after bouncing of hyper link...
						((Label)(ItemX.FindControl("lblSerial"))).Text  = (string) Convert.ChangeType(sNo,typeof(string));
					}
					catch
					{}

				 
				}
			}
			catch(Exception ex)

			{
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);

				lblMsg.Text = ex.Message ;
			}		
		}

		private void dgDocket_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			dgDocket.CurrentPageIndex = e.NewPageIndex;
			FillGrid();
		}

		private void SortGrid(string SortExp)
		{
			try
			{
				SetAcsDesc(SortExp);
				dv_Result = (DataView) (cSession.GetSessionObject("dvResult",this.Session));
				dv_Result.Sort = StrExp + " " + StrAcsDec;
				cSession.SetSessionVariable("dvResult",dv_Result,this.Session);
				FillGrid();

			}
			catch(Exception ex)
			{
				lblMsg.Text = ex.Message;
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);

			}
		}

		private void SetAcsDesc(string Val)
		{
			try
			{
				StrExp =  cSession.GetSessionVariable ("StrExp",this.Session);
				StrAcsDec = cSession.GetSessionVariable("StrAcsDec",this.Session);
			}
			catch
			{

			}

			if (StrExp == Val)
			{
				if (StrAcsDec == "ASC")
				{
					StrAcsDec = "DESC";
					cSession.SetSessionVariable("StrAcsDec",StrAcsDec, this.Session);
				}
				else 
				{
					StrAcsDec = "ASC";
					cSession.SetSessionVariable("StrAcsDec",StrAcsDec,this.Session);
				}
			}
			else 
			{
				StrExp = Val;
				StrAcsDec = "ASC";
				cSession.SetSessionVariable("StrExp", StrExp,this.Session) ;
				cSession.SetSessionVariable("StrAcsDec", StrAcsDec, this.Session) ;				
			}
		}

		private void dgDocket_SortCommand(object source, System.Web.UI.WebControls.DataGridSortCommandEventArgs e)
		{
			lblMsg.Text="";
			SortGrid(e.SortExpression);
		}

		private void SetNavigation()
		{
			// Procedure for setting data grid's current  page number on header ........
			
			try
			{
				// setting current page number
				lblCurrPage.Text =Convert.ToString(dgDocket.CurrentPageIndex+1);

				for (int idx =0 ; idx < ddlPageNo.Items.Count ;idx++)
				{
					ddlPageNo.Items[idx].Selected = false;
				}
				ddlPageNo.Items[dgDocket.CurrentPageIndex].Selected=true;
			
			}
			catch (Exception ex)
			{
				lblMsg.Text = ex.Message ;
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);

			}
		}

		private void ddlPageNo_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			dgDocket.CurrentPageIndex = ddlPageNo.SelectedIndex;
			FillGrid();
		}

		private void FillPageList()
		{
			try
			{
				int i =0;
				ddlPageNo.Items.Clear();
				for(i=1; i<=dgDocket.PageCount;i++ )
				{
					ddlPageNo.Items.Add("Page - " + i.ToString());
					ddlPageNo.Items[i-1].Value = i.ToString();
				}
			}
			catch(Exception ex)
			{
				lblMsg.Text = ex.Message;
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);

			}

		}

		private void Search()
		{
			lblMsg.Text="";
			dgDocket.CurrentPageIndex = 0;
			PullData();
			FillGrid();
		}


		private DataSet InsertBlankRows(DataSet dsTemp)
		{	
			int idx=1;
			int iRows = dsTemp.Tables[0].Rows.Count - 1;
			

			// inserting line break after each ticketid....
			for (idx=1; idx < iRows; idx++)
			{
				DateTime fDate =  Convert.ToDateTime(dsTemp.Tables[0].Rows[idx]["courtdatemain"]) ;
				DateTime lDate = Convert.ToDateTime(dsTemp.Tables[0].Rows[idx - 1]["courtdatemain"]) ;
				if ( fDate.ToShortDateString() != lDate.ToShortDateString() )
				{
					DataRow dr = dsTemp.Tables[0].NewRow();
					dsTemp.Tables[0].Rows.InsertAt(dr,idx);
					iRows++;
					idx++;
				}
			}
			return dsTemp;
		}
  
	/*	private DataView InsertBlankRows(DataView dvTemp)
		{	
			int idx=1;
			int iRows = dvTemp.Table.Rows.Count - 1;
			

			// inserting line break after each ticketid....
			for (idx=1; idx < iRows; idx++)
			{
				DateTime fDate =  Convert.ToDateTime(dvTemp.Table.Rows[idx]["courtdatemain"]) ;
				DateTime lDate = Convert.ToDateTime(dvTemp.Table.Rows[idx - 1]["courtdatemain"]) ;
				if ( fDate.ToShortDateString() != lDate.ToShortDateString() )
				{
					DataRow dr = dvTemp.Table.NewRow();
					dvTemp.Table.Rows.InsertAt(dr,idx);
					iRows++;
					idx++;
				}
			}
			return dvTemp;
		}
  */
	}
}

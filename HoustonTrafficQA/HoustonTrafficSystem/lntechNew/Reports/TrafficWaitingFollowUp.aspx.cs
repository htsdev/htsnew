using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;

namespace HTP.Reports
{
    /// <summary>
    /// Abid Ali 5018 12/24/28 Enum for report category user in waiting follow up
    /// </summary>
    public enum WaitingFollowUpCategory
    {
        ///<summary>
        ///</summary>
        Traffic,
        ///<summary>
        ///</summary>
        Family,
        ///<summary>
        ///</summary>
        Criminal
    }
    /// <summary>
    /// TrafficWaitingFollowUp class file.
    /// </summary>
    public partial class TrafficWaitingFollowUp : Page
    {
        // Abid Ali 5018 12/26/2008  grouping variables
        #region Variables

        readonly clsSession _clsSession = new clsSession();
        readonly clsCase _cCase = new clsCase();
        readonly clsLogger _bugTracker = new clsLogger();
        DataSet _ds;
        readonly ValidationReports _reports = new ValidationReports();

        #endregion

        // Abid Ali 5018 12/26/2008 grouping Report properties
        #region Properties

        ///<summary>
        ///</summary>
        public SortDirection GridViewSortDirection
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Ascending;
                return (SortDirection)ViewState["sortDirection"];
            }
            set { ViewState["sortDirection"] = value; }

        }

        ///<summary>
        ///</summary>
        public string GridViewSortExpression
        {
            get
            {
                if (ViewState["sortExpression"] == null)
                    ViewState["sortExpression"] = "";
                return ViewState["sortExpression"].ToString();
            }
            set { ViewState["sortExpression"] = value; }

        }

        // Abid Ali 5018 12/28/2008 Traffic Waiting Follow Up report or Family Waiting folow up report
        private WaitingFollowUpCategory ReportCategory
        {
            get
            {
                if (ViewState["SelectedReport"] == null)
                    return WaitingFollowUpCategory.Traffic;

                return ((WaitingFollowUpCategory)ViewState["SelectedReport"]);
            }
            set
            {

                ViewState["SelectedReport"] = value;
            }
        } // end of property   

        // Abid Ali 5018 12/28/2008 Show All property
        private bool ShowAllRecords
        {
            get
            {
                if (ViewState["ShowAllRecords"] == null)
                    ViewState["ShowAllRecords"] = false;

                return Convert.ToBoolean(ViewState["ShowAllRecords"]);
            }
            set
            {
                ViewState["ShowAllRecords"] = value;
            }
        }
        // Abid Ali 5018 12/28/2008 Show Pasr property
        private bool ShowPastRecords
        {
            get
            {
                if (ViewState["ShowPastRecords"] == null)
                    ViewState["ShowPastRecords"] = true;

                return Convert.ToBoolean(ViewState["ShowPastRecords"]);
            }
            set
            {
                ViewState["ShowPastRecords"] = value;
            }
        }
        // Abid Ali 5018 12/28/2008 From Date property
        private DateTime FromDate
        {
            get
            {
                if (ViewState["FromDate"] == null)
                    ViewState["FromDate"] = new DateTime(1900, 1, 1);

                return Convert.ToDateTime(ViewState["FromDate"]);
            }
            set
            {
                ViewState["FromDate"] = value;
            }
        }
        // Abid Ali 5018 12/28/2008 From Date property
        private DateTime ToDate
        {
            get
            {
                if (ViewState["ToDate"] == null)
                    ViewState["ToDate"] = DateTime.Now;

                return Convert.ToDateTime(ViewState["ToDate"]);
            }
            set
            {
                ViewState["ToDate"] = value;
            }
        }
        #endregion

        // Abid Ali 5018 12/26/2008 grouping event handlers
        #region Event Handler
        /// <summary>
        /// Page_Load event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Waqas 5057 03/17/2009 Checking employee info in session
                if (_clsSession.IsValidSession(this.Request, this.Response, this.Session) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else
                {
                    // Abid Ali 5018 12/28/2008 Set empty message
                    lbl_Message.Text = string.Empty;

                    if (!IsPostBack)
                    {
                        // Abid Ali 5018 12/28/2008 Set Traffic Waiting Follow Up report
                        ReportCategory = WaitingFollowUpCategory.Traffic;
                        chkFamilyShowAll.Checked = false;
                        ShowAllRecords = false;
                        chkFamilyShowPast.Checked = true;
                        ShowPastRecords = true;
                        cal_FromDateFilter.SelectedDate = DateTime.Now;
                        FromDate = DateTime.Now;
                        cal_ToDateFilter.SelectedDate = DateTime.Now;
                        ToDate = DateTime.Now;

                        gv_CriminalFollowUp.Visible = false;
                        gv_Records.Visible = false;
                    }
                    UpdateFollowUpInfo2.PageMethod += UpdateFollowUpInfo2_PageMethod;
                    Pagingctrl.PageIndexChanged += Pagingctrl_PageIndexChanged;

                    // Abid 5387 01/12/2009 page size change event handler
                    Pagingctrl.PageSizeChanged += Pagingctrl_PageSizeChanged;
                    if (ReportCategory == WaitingFollowUpCategory.Criminal)
                    {
                        // Yasir Kamal 5427 01/23/2009 GridView Name Changed in PaggingControl

                        Pagingctrl.GridView = gv_CriminalFollowUp;
                    }
                    else
                    {
                        Pagingctrl.GridView = gv_Records;
                    }
                    // Yasir Kamal 5427 end.

                    // Abid Ali 5018 12/28/2008 Javascript for enable disable
                    const string javascript = "EnabledFamilyFilterControls('{0}','{1}','{2}','{3}',{4});";
                    chkFamilyShowAll.Attributes.Add("onclick", string.Format(javascript, cal_FromDateFilter.ClientID, cal_ToDateFilter.ClientID, chkFamilyShowAll.ClientID, chkFamilyShowPast.ClientID, 2));
                    chkFamilyShowPast.Attributes.Add("onclick", string.Format(javascript, cal_FromDateFilter.ClientID, cal_ToDateFilter.ClientID, chkFamilyShowAll.ClientID, chkFamilyShowPast.ClientID, 3));

                    if (chkFamilyShowAll.Checked)
                    {
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "javascript", string.Format("<script language=\"javascript\" type=\"text/javascript\"> EnabledFamilyFilterControls('{0}','{1}','{2}','{3}',{4});</script>", cal_FromDateFilter.ClientID, cal_ToDateFilter.ClientID, chkFamilyShowAll.ClientID, chkFamilyShowPast.ClientID, 2));
                    }
                    else if (chkFamilyShowPast.Checked)
                    {
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "javascript", string.Format("<script language=\"javascript\" type=\"text/javascript\"> EnabledFamilyFilterControls('{0}','{1}','{2}','{3}',{4});</script>", cal_FromDateFilter.ClientID, cal_ToDateFilter.ClientID, chkFamilyShowAll.ClientID, chkFamilyShowPast.ClientID, 3));
                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }

        /// <summary>
        /// Gridview RowDataBound event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_Records_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label desc = (Label)e.Row.FindControl("lbl_LastName");

                    if (desc.Text.Length > 10)
                    {
                        desc.ToolTip = desc.Text;
                        desc.Text = desc.Text.Substring(0, 10).ToUpperInvariant() + "...";
                    }
                    Label desce = (Label)e.Row.FindControl("lbl_FirstName");

                    if (desce.Text.Length > 10)
                    {
                        desce.ToolTip = desce.Text;
                        desce.Text = desce.Text.Substring(0, 10).ToUpperInvariant() + "...";
                    }

                    Label followup = (Label)e.Row.FindControl("lbl_followup");
                    followup.Text = (followup.Text.Trim() == "01/01/1900") ? "" : followup.Text;
                    ((ImageButton)e.Row.FindControl("img_Add")).CommandArgument = e.Row.RowIndex.ToString();
                }
            }
            catch
            {

            }
        }

        /// <summary>
        /// Gridview PageIndexChanging event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_Records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_Records.PageIndex = e.NewPageIndex;
                FillGrid();
            }
            catch (Exception ex)
            {

                _bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        /// <summary>
        /// Gridview RowCommand event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_Records_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "btnclick")
            {
                int rowId = Convert.ToInt32(e.CommandArgument);
                string firstname = (((HiddenField)gv_Records.Rows[rowId].FindControl("hf_fname")).Value);
                string causeno = (((HiddenField)gv_Records.Rows[rowId].FindControl("hf_causeno")).Value);
                string ticketno = (((HiddenField)gv_Records.Rows[rowId].FindControl("hf_ticketnumber")).Value);
                string lastname = (((HiddenField)gv_Records.Rows[rowId].FindControl("hf_lname")).Value);
                string courtname = (((HiddenField)gv_Records.Rows[rowId].FindControl("hf_courtid")).Value);
                string followupDate = (((Label)gv_Records.Rows[rowId].FindControl("lbl_followup")).Text);
                followupDate = (followupDate.Trim() == "") ? "1/1/1900" : followupDate;
                string trafficFollowUpDate = (((HiddenField)gv_Records.Rows[rowId].FindControl("hf_TrafficFollowUpDate")).Value);
                // Abid Ali 5018 12/28/2008 follow up date set
                trafficFollowUpDate = (trafficFollowUpDate.Trim() == "") ? "1/1/1900" : trafficFollowUpDate.Substring(0, trafficFollowUpDate.IndexOf("@")).Trim();
                _cCase.TicketID = Convert.ToInt32((((HiddenField)gv_Records.Rows[rowId].FindControl("hf_ticketno")).Value));
                string comm = _cCase.GetGeneralCommentsByTicketId();
                UpdateFollowUpInfo2.Freezecalender = true;

                // Abid Ali 5018 12/28/2008 set follow up type
                if (ReportCategory == WaitingFollowUpCategory.Traffic)
                {
                    UpdateFollowUpInfo2.Title = "Traffic Waiting Follow Up (Non HMC and HCJP) Date";
                    // Abid Ali 5425 1/20/2009 update FollowUpType referrence
                    UpdateFollowUpInfo2.followUpType = HTP.Components.FollowUpType.TrafficWaitingFollowUpDate;
                    // Noufil 8099 08/04/2010 Use overloaded method to get court name in javascript message
                    //UpdateFollowUpInfo2.binddate(DateTime.Today, Convert.ToDateTime(followupDate), firstname, lastname, ticketno, causeno, comm, mpeTrafficwaiting.ClientID, "Traffic Waiting", ((Label)gv_Records.Rows[rowId].FindControl("lbl_LOC")).Text, followupDate);
                    //Saeed 7791 08/11/2010 call overloaded 'binddate' method to display default follow up date after 1 week.
                    UpdateFollowUpInfo2.binddate(DateTime.Today, Convert.ToDateTime(followupDate), firstname, lastname, ticketno, causeno, comm, mpeTrafficwaiting.ClientID, "Traffic Waiting", Convert.ToInt32((((HiddenField)gv_Records.Rows[rowId].FindControl("hf_ticketno")).Value)));
                }
                else
                {
                    UpdateFollowUpInfo2.Title = "Family Waiting Follow Up Date";
                    // Abid Ali 5425 1/20/2009 update FollowUpType referrence
                    UpdateFollowUpInfo2.followUpType = HTP.Components.FollowUpType.FamilyWaitingFollowUpDate;
                    // Noufil 8099 08/04/2010 Use overloaded method to get court name in javascript message
                    //UpdateFollowUpInfo2.binddate(DateTime.Today, Convert.ToDateTime(followupDate), firstname, lastname, ticketno, causeno, comm, mpeTrafficwaiting.ClientID, "Family Waiting", ((Label)gv_Records.Rows[rowId].FindControl("lbl_LOC")).Text, followupDate);
                    //Saeed 7791 08/11/2010 call overloaded 'binddate' method to display default follow up date after 1 week.
                    UpdateFollowUpInfo2.binddate(DateTime.Today, Convert.ToDateTime(followupDate), firstname, lastname, ticketno, causeno, comm, mpeTrafficwaiting.ClientID, "Family Waiting", Convert.ToInt32((((HiddenField)gv_Records.Rows[rowId].FindControl("hf_ticketno")).Value)));
                }

                mpeTrafficwaiting.Show();
                Pagingctrl.Visible = true;
            }

        }
        /// <summary>
        /// PageMethod
        /// </summary>
        void UpdateFollowUpInfo2_PageMethod()
        {
            // Abid Ali 5018 12/28/2008 change according to report
            if (ReportCategory == WaitingFollowUpCategory.Criminal)
            {
                FillCriminalGrid();
            }
            else
            {
                FillGrid();
            }
        }
        /// <summary>
        /// Paging control PageIndexChanged even.t
        /// </summary>
        void Pagingctrl_PageIndexChanged()
        {
            // Abid Ali 5018 12/28/2008 change according to report
            if (ReportCategory == WaitingFollowUpCategory.Criminal)
            {
                gv_CriminalFollowUp.PageIndex = Pagingctrl.PageIndex - 1;
                FillCriminalGrid();
            }
            else
            {
                gv_Records.PageIndex = Pagingctrl.PageIndex - 1;
                FillGrid();
            }
        }

        // Abid 5387 01/12/2009 page size change event handler
        /// <summary>
        /// Page Size change event handler
        /// </summary>
        /// <param name="pageSize"></param>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            if (ReportCategory == WaitingFollowUpCategory.Criminal)
            {
                if (pageSize > 0)
                {
                    gv_CriminalFollowUp.PageIndex = 0;
                    gv_CriminalFollowUp.PageSize = pageSize;
                    gv_CriminalFollowUp.AllowPaging = true;
                }
                else
                {
                    gv_CriminalFollowUp.AllowPaging = false;
                }
                FillCriminalGrid();
            }
            else
            {
                if (pageSize > 0)
                {
                    gv_Records.PageIndex = 0;
                    gv_Records.PageSize = pageSize;
                    gv_Records.AllowPaging = true;
                }
                else
                {
                    gv_Records.AllowPaging = false;
                }
                FillGrid();
            }
        }

        // Abid Ali 5018 12/26/2008 Filter family report
        /// <summary>
        /// Event handler to filter report according to criteria
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnFamilySubmit_Click(object sender, EventArgs e)
        {
            string value = ddlReport.SelectedValue.ToLower();
            ShowAllRecords = chkFamilyShowAll.Checked;
            ShowPastRecords = chkFamilyShowPast.Checked;
            FromDate = cal_FromDateFilter.SelectedDate;
            ToDate = cal_ToDateFilter.SelectedDate;

            if (value == "traffic")
            {
                ReportCategory = WaitingFollowUpCategory.Traffic;
                gv_Records.PageIndex = 0;
                GridViewSortDirection = SortDirection.Descending;
                GridViewSortExpression = "TrafficWaitingFollowUp";
                gv_CriminalFollowUp.Visible = false;
                FillGrid();
            }
            else if (value == "family")
            {
                ReportCategory = WaitingFollowUpCategory.Family;
                gv_Records.PageIndex = 0;
                GridViewSortDirection = SortDirection.Descending;
                GridViewSortExpression = "TrafficWaitingFollowUp";
                gv_CriminalFollowUp.Visible = false;
                FillGrid();
            }
            else
            {
                ReportCategory = WaitingFollowUpCategory.Criminal;
                gv_CriminalFollowUp.PageIndex = 0;
                GridViewSortDirection = SortDirection.Descending;
                GridViewSortExpression = "FollowUpDate";
                gv_Records.Visible = false;
                FillCriminalGrid();
            }


        }

        /// <summary>
        /// Abid Ali 5018 12/26/2008 Sorting event handler for family and traffic follow up repotr
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_Records_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                string sortExpression = e.SortExpression;

                if (GridViewSortDirection == SortDirection.Ascending && sortExpression == GridViewSortExpression)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                else
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }

                GridViewSortExpression = sortExpression;
                FillGrid();
                Pagingctrl.PageCount = gv_Records.PageCount;
                Pagingctrl.PageIndex = gv_Records.PageIndex;
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Abid Ali 5018 12/26/2008 Sorting event handler for criminal report
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_CriminalFollowUp_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                string sortExpression = e.SortExpression;

                if (GridViewSortDirection == SortDirection.Ascending && sortExpression == GridViewSortExpression)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                else
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }

                GridViewSortExpression = sortExpression;
                FillCriminalGrid();
                Pagingctrl.PageCount = gv_CriminalFollowUp.PageCount;
                Pagingctrl.PageIndex = gv_CriminalFollowUp.PageIndex;
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        /// <summary>
        /// Abid Ali 5018 12/26/2008  row command event handler for criminal report
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_CriminalFollowUp_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "btnclick")
            {

                int rowId = Convert.ToInt32(e.CommandArgument);
                string firstname = (((HiddenField)gv_CriminalFollowUp.Rows[rowId].FindControl("hf_criminal_fname")).Value);
                string causeno = (((HiddenField)gv_CriminalFollowUp.Rows[rowId].FindControl("hf_criminal_causeno")).Value);
                string ticketno = (((HiddenField)gv_CriminalFollowUp.Rows[rowId].FindControl("hf_criminal_ticketno")).Value);
                string lastname = (((HiddenField)gv_CriminalFollowUp.Rows[rowId].FindControl("hf_criminal_lname")).Value);
                string courtname = (((HiddenField)gv_CriminalFollowUp.Rows[rowId].FindControl("hf_criminal_loc")).Value);
                string court = (((HiddenField)gv_CriminalFollowUp.Rows[rowId].FindControl("hf_criminal_courtid")).Value);
                string followupDate = (((Label)gv_CriminalFollowUp.Rows[rowId].FindControl("lbl_FollowUpD")).Text);
                _cCase.TicketID = Convert.ToInt32(ticketno);
                string comm = _cCase.GetGeneralCommentsByTicketId();
                string follow = string.Empty;
                int Days = (court == "3047" || court == "3048" || court == "3070") ? 7 : 14;
                follow = DateTime.Today.AddDays(Days).ToShortDateString();
                UpdateFollowUpInfo2.Freezecalender = true;
                UpdateFollowUpInfo2.Title = "Criminal Follow Up Date";
                // Abid Ali 5425 1/20/2009 update FollowUpType referrence
                UpdateFollowUpInfo2.followUpType = Components.FollowUpType.CriminalFollowUpDate;
                UpdateFollowUpInfo2.binddate(DateTime.Today.AddDays(Days), DateTime.Today, firstname, lastname, ticketno, causeno, comm, mpeTrafficwaiting.ClientID, court, courtname, followupDate);
                mpeTrafficwaiting.Show();
                Pagingctrl.Visible = true;

            }
        }

        // Abid Ali 5018 12/26/2008 event handler for page index change
        /// <summary>
        /// Abid Ali 5018 12/26/2008 Page Index event handler for criminal report
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_CriminalFollowUp_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_CriminalFollowUp.PageIndex = e.NewPageIndex;
                FillCriminalGrid();
            }
            catch (Exception ex)
            {

                _bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        /// <summary>
        /// Abid Ali 5018 12/26/2008 Row data bound event handler for criminal report
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_CriminalFollowUp_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ((LinkButton)e.Row.FindControl("img_AddCriminal")).CommandArgument = e.Row.RowIndex.ToString();
                    pnlFollowup.Style["display"] = "none";
                }
            }

            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }
        #endregion

        // Abid Ali 5018 12/26/2008 grouping methods
        #region Methods
        private void FillGrid()
        {
            // Abid Ali 5018 12/28/2008 Show Record according to report selection
            try
            {
                gv_Records.Visible = true;
                string[] keys = { "@ShowAllRecords", "@ShowAllPastRecords", "@FromFollowUpDate", "@ToFollowUpDate" };
                object[] values = { ShowAllRecords, ShowPastRecords, FromDate, ToDate };
                if (ReportCategory == WaitingFollowUpCategory.Traffic)
                {
                    _reports.getRecords("usp_HTP_Get_TrafficWaitingFollowUpReport", keys, values);
                }
                else
                {
                    _reports.getRecords("usp_HTP_Get_FamilyWaitingFollowUpReport", keys, values);
                }

                _ds = _reports.records;

                if (_ds.Tables[0].Rows.Count > 0)
                {
                    // Abid Ali 5387 01/13/2009 Set grivview page size 
                    _ds.Tables[0].Columns.Add("sno");
                    DataView dv = _ds.Tables[0].DefaultView;
                    dv.Sort = GridViewSortExpression + " " + (GridViewSortDirection == SortDirection.Ascending ? "ASC" : "DESC");
                    DataTable dt = dv.ToTable();
                    GenerateSerialNo(dt); ;

                    // Yasir Kamal 5427 02/06/2009 GridView Name Changed in PaggingControl
                    Pagingctrl.GridView = gv_Records;
                    // Yasir Kamal 5427 end
                    gv_Records.DataSource = dt;
                    gv_Records.DataBind();
                    Pagingctrl.PageCount = gv_Records.PageCount;
                    Pagingctrl.PageIndex = gv_Records.PageIndex;
                    Pagingctrl.SetPageIndex();
                }
                else
                {
                    // Abid Ali 5387 01/13/2009 Set grivview page size
                    Pagingctrl.PageCount = 0;
                    Pagingctrl.PageIndex = 0;
                    Pagingctrl.SetPageIndex();
                    lbl_Message.Text = "No Records Found";
                    gv_Records.Visible = false;
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Abid Ali 5018 12/26/2008 Fill criminal records
        /// </summary>
        private void FillCriminalGrid()
        {
            try
            {
                HccCriminalReport hccCrim = new HccCriminalReport();
                DataView dv;
                int courtid = -1;

                if (chkFamilyShowPast.Checked)
                {
                    FromDate = new DateTime(1900, 1, 1);
                    ToDate = DateTime.Now;
                }

                DataTable dt = hccCrim.Get_CriminalFollowUp(courtid, FromDate, ToDate, ShowAllRecords);
                if (dt.Rows.Count > 0)
                {
                    gv_CriminalFollowUp.Visible = true;
                    lbl_Message.Text = "";

                    // Abid Ali 5387 01/13/2009 Set grivview page size
                    dt.Columns.Add("sno");
                    dv = dt.DefaultView;
                    dv.Sort = GridViewSortExpression + " " + (GridViewSortDirection == SortDirection.Ascending ? "ASC" : "DESC");
                    dt = dv.ToTable();
                    GenerateSerialNo(dt);
                    // Yasir Kamal 5427 02/06/2009 GridView Name Changed in PaggingControl
                    Pagingctrl.GridView = gv_CriminalFollowUp;
                    // Yasir Kamal 5427 end
                    gv_CriminalFollowUp.DataSource = dt;
                    gv_CriminalFollowUp.DataBind();
                    Pagingctrl.PageCount = gv_CriminalFollowUp.PageCount;
                    Pagingctrl.PageIndex = gv_CriminalFollowUp.PageIndex;
                    Pagingctrl.SetPageIndex();
                }
                else
                {
                    // Abid Ali 5387 01/13/2009 Set grivview page size
                    Pagingctrl.PageCount = 0;
                    Pagingctrl.PageIndex = 0;
                    Pagingctrl.SetPageIndex();
                    lbl_Message.Text = "No Records Found";
                    gv_CriminalFollowUp.Visible = false;
                }

            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);

            }
        }

        // Abid Ali 5387 1/13/2009 Generate Serial Number
        /// <summary>
        /// Generate Serial Number
        /// </summary>
        /// <param name="dtRecords"></param>
        public void GenerateSerialNo(DataTable dtRecords)
        {
            int sno = 1;
            if (!dtRecords.Columns.Contains("sno"))
                dtRecords.Columns.Add("sno");


            if (dtRecords.Rows.Count >= 1)
                dtRecords.Rows[0]["sno"] = 1;

            if (dtRecords.Rows.Count >= 2)
            {
                for (int i = 1; i < dtRecords.Rows.Count; i++)
                {
                    if (dtRecords.Rows[i - 1]["ticketid_pk"].ToString() != dtRecords.Rows[i]["ticketid_pk"].ToString())
                    {
                        dtRecords.Rows[i]["sno"] = ++sno;
                    }
                }
            }

        }
        #endregion

    }
}

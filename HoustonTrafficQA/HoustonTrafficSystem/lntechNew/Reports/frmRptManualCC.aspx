<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmRptManualCC.aspx.cs" Inherits="lntechNew.Reports.frmRptManualCC" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Manual CC Report </title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />   
    <SCRIPT src="../Scripts/validatecreditcard.js" type="text/javascript"></SCRIPT>
    <SCRIPT src="../Scripts/boxover.js" type="text/javascript"></SCRIPT>
    
    <script language="javascript">
    function showpopup(v,w)
    { 
      ////////////// Geting Data From Form and Fill POPUP /////
         document.getElementById("lbl_message").value ="";
         document.getElementById("lbltransdate").innerText = document.getElementById(v+"_lbl_tranddate").innerText;
         document.getElementById("lblname").innerText = document.getElementById(v+"_lbl_cardholder").innerText;
         document.getElementById("lbltransno").innerText = document.getElementById(v+"_lbl_transacion").innerText;
         document.getElementById("lblchargedamount").innerText = document.getElementById(v+"_lbl_amount").innerText;         
         document.getElementById("hf_ccno").value =document.getElementById(v+"_lbl_cardno").innerText;         
         document.getElementById("hf_paymentid").value = document.getElementById(v+"_hf_paymentid").value
         var am =document.getElementById(v+"_lbl_amount").innerText;             
         document.getElementById("tb_refundamount").innerText = am.replace(".0000","");                       
         document.getElementById("tb_description").innerText="";         
         
        // document.getElementById("hf_void").value =document.getElementById(v+"_hf_voidpayment").value;
        // document.getElementById("hf_refund").value =document.getElementById(v+"_hf_ref").value;
                    
         if(w == "1")
            {    document.getElementById("hf_transtype").value ="refund"; 
                 document.getElementById("lblheading").innerText="Refund Transaction";   
                 document.getElementById("lblcurrentamoun").innerText ="Refund Amount";                                 
            }
         else
            {   document.getElementById("hf_transtype").value ="void";  
                document.getElementById("lblheading").innerText="Void Transaction";            
                document.getElementById("lblcurrentamoun").innerText="Void Amount";                 
            }           
           
         
         
      setpopuplocation();
    }
    //// Set PopUp Locaion by height and width
    function setpopuplocation()
	{    	    
	    var top  = 400;
	    var left = 400;
	    var height = document.body.offsetHeight;
	    var width  = document.body.offsetWidth
	    
	    var resolution =  width  + "x" + height;
	    	   	    
	    if ( width > 1100 || width <= 1280)
	    {
	        left = 575
	    }
	    
	    if ( width < 1100)
	    {
	        left = 500;
	    
	    }
	    
	    // Setting popup display
	    document.getElementById("zee").style.top =top;
		document.getElementById("zee").style.left =left;
		document.getElementById("Disable").style.height = document.body.offsetHeight * 2 ;
		document.getElementById("Disable").style.width = document.body.offsetWidth;
		document.getElementById("Disable").style.display = 'block'
		document.getElementById("zee").style.display = 'block'
	 }   
	 function closepopup()
	 {	    
		   
		    document.getElementById("zee").style.display = 'none';
	        document.getElementById("Disable").style.display = 'none';	
	      
	 }
	 //// Validate the PopUp before submiting the Request to Gateway
	 function submitPopup()
	 {
//	    var vtbamount =document.getElementById("tb_refundamount").value;
//	    if(vtbamount.lenght <= 0)
//	        {
//                alert("Please Enter Refund Amount");
//                document.getElementById("tb_refundamount").focus();
//                return false;
//            }
            
            ///Checking Valid amount    
//       if(isNum(document.getElementById("tb_refundamount").value)!=true)
//       {
//         alert("This is not Valid amount");
//         document.getElementById("tb_refundamount").focus();
//         return false;
//       }      
       var desc = document.getElementById("tb_description").value;
       if(desc.length <=0)
       {
        alert("Please Enter Description");
        document.getElementById("tb_description").focus();
        return false;
       }
      
       var trans =document.getElementById("lblcurrentamoun").innerText;
       trans =trans.replace("Amount","");
       
       doyou = confirm("Are you sure you want to "+trans+" this transaction? (OK = Yes   Cancel = No)");
		if (doyou == true) 
		{
		}
		else
		{
			 closepopup();	       
	        return false;
		}
	    closepopup();
	    plzwait();
	    //window.location.reload();
	 }
	 
	 function plzwait() 
		{
		
		document.getElementById("tbl_plzwait").style.display = 'block';
		document.getElementById("TableGrid").style.display = 'none';
		
		}
    </script>

   
    

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />

   
    
</head>


<body id="lbl_cardholdername">
    <form id="form1" runat="server">
    <div id="Disable" style="display :none;position: absolute;left :1 ;top : 1;height : 1px;background-color :Silver; filter : alpha(opacity=50)"></div>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
         
   
    <TABLE id="TableMain" style="Z-INDEX: 101" cellSpacing="0" cellPadding="0" width="780"
				align="center" border="0">
				<tbody>
					<tr>
						<td style="HEIGHT: 14px" colSpan="4"><uc1:activemenu id="ActiveMenu1" runat="server"></uc1:activemenu></td>
					</tr>
					<TR>
						<TD>
							<TABLE id="TableSub" cellSpacing="0" cellPadding="0" width="100%" border="0">
								<!--<tr vAlign="middle">
									<td><IMG height="18" src="../Images/head_icon.gif" width="25">&nbsp; <STRONG><font color="#3366cc">
												Validation II (v=a) </font></STRONG>
									</td>
								</tr>-->
								<tr>
									<td background="../images/separator_repeat.gif" colSpan="7" height="11"></td>
								</tr>
								<TR>
									<TD align="center">
										<TABLE id="Table6" cellSpacing="0" cellPadding="0" width="780" bgColor="white" border="0">
											<tr>
												<TD width="16%">
                                                    &nbsp;<ew:CalendarPopup ID="calQueryFrom" runat="server" AllowArbitraryText="False"
                                                        CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Culture="(Default)" DisableTextboxEntry="False"
                                                        Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif" PadSingleDigits="True"
                                                        SelectedDate="2005-09-01" ShowGoToToday="True" ToolTip="Select Report Date Range"
                                                        UpperBoundDate="12/31/9999 23:59:00" Width="90px">
                                                        <TextboxLabelStyle CssClass="clstextarea" />
                                                        <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                            ForeColor="Black" />
                                                        <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                            Font-Size="XX-Small" ForeColor="Black" />
                                                        <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                            Font-Size="XX-Small" ForeColor="Gray" />
                                                        <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                            ForeColor="Black" />
                                                        <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                            Font-Size="XX-Small" ForeColor="Black" />
                                                        <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                            ForeColor="Black" />
                                                        <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                            ForeColor="Black" />
                                                        <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                            Font-Size="XX-Small" ForeColor="Black" />
                                                        <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                            ForeColor="Black" />
                                                        <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                            ForeColor="Black" />
                                                    </ew:CalendarPopup>
                                                </TD>
												<TD width="16%">&nbsp;&nbsp;
                                                    <ew:CalendarPopup ID="calQueryTo" runat="server" AllowArbitraryText="False" CalendarLocation="Bottom"
                                                        ControlDisplay="TextBoxImage" Culture="(Default)" DisableTextboxEntry="False"
                                                        Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif" PadSingleDigits="True"
                                                        SelectedDate="2006-09-01" ShowGoToToday="True" ToolTip="Select Report Date Range"
                                                        UpperBoundDate="12/31/9999 23:59:00" Width="90px">
                                                        <TextboxLabelStyle CssClass="clstextarea" />
                                                        <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                            ForeColor="Black" />
                                                        <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                            Font-Size="XX-Small" ForeColor="Black" />
                                                        <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                            Font-Size="XX-Small" ForeColor="Gray" />
                                                        <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                            ForeColor="Black" />
                                                        <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                            Font-Size="XX-Small" ForeColor="Black" />
                                                        <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                            ForeColor="Black" />
                                                        <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                            ForeColor="Black" />
                                                        <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                            Font-Size="XX-Small" ForeColor="Black" />
                                                        <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                            ForeColor="Black" />
                                                        <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                            ForeColor="Black" />
                                                    </ew:CalendarPopup>
                                                </TD>
												<TD width="10%">
                                                    &nbsp;<asp:DropDownList ID="ddl_clienttype" runat="server" CssClass="clsinputcombo"
                                                        Width="100px">
                                                        <asp:ListItem Selected="True" Value="0">- Client Type-</asp:ListItem>
                                                        <asp:ListItem Value="1">Traffic</asp:ListItem>
                                                        <asp:ListItem Value="2">Oscar Client</asp:ListItem>
                                                        <asp:ListItem Value="3">Lisa Client</asp:ListItem>
                                                        <asp:ListItem Value="4">Oher</asp:ListItem>
                                                    </asp:DropDownList></TD>
												<TD align="left" width="18%">
                                                    &nbsp;<asp:button id="btn_search" runat="server" Text="Search" CssClass="clsbutton" OnClick="btn_search_Click"></asp:button></TD>
												<td align="right" width="39%">
                                                    
														
												</td>
											</tr>
											<tr>
												<td width="780" background="../images/separator_repeat.gif" colSpan="6" style="height: 11px"></td>
											</tr>
										</TABLE>
                                        
                                    </TD>
								</TR>
								<tr>
								    <td colspan=7 width=100% align=center>
								        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
										<asp:label id="lbl_message" runat="server" CssClass="label" ForeColor="Red" Width="527px"></asp:label>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="btn_popup" EventName="Click" />
                                            </Triggers>
                                        </asp:UpdatePanel>
								    </td>
								</tr>
								<TR>
									<TD>
									 <asp:UpdatePanel ID="pnl_ccgrid" runat="server" UpdateMode="Conditional"  >
        <ContentTemplate>
										<TABLE id="TableGrid" cellSpacing="0" cellPadding="0" width="100%" bgColor="white" border="0">
										<tr><td align=right>
										    <asp:label id="lblCurrPage" runat="server" Width="83px" Font-Size="Smaller" CssClass="cmdlinks"
														ForeColor="#3366cc" Font-Bold="True" Height="8px">Current Page :</asp:label><asp:label id="lblPNo" runat="server" Width="9px" Font-Size="Smaller" CssClass="cmdlinks" ForeColor="#3366CC"
														Font-Bold="True" Height="10px">1</asp:label>&nbsp;
<asp:label id="lblGoto" runat="server" Width="16px" Font-Size="Smaller" CssClass="cmdlinks"
														ForeColor="#3366cc" Font-Bold="True" Height="7px">Goto</asp:label>&nbsp;
														<asp:dropdownlist id="cmbPageNo" runat="server" Font-Size="Smaller" CssClass="clinputcombo" ForeColor="#3366cc"
														Font-Bold="True" AutoPostBack="True" OnSelectedIndexChanged="cmbPageNo_SelectedIndexChanged">
                                                            <asp:ListItem>1</asp:ListItem>
                                                        </asp:dropdownlist>
                                                
										</td></tr>
											<tr>
											
											<td style="HEIGHT: 136px" vAlign="top" align="center" colSpan="2" height="136">
												<asp:datagrid id="dg_manualcc" runat="server" Width="100%" CssClass="clsLeftPaddingTable" BorderStyle="None"
														BorderColor="White" AutoGenerateColumns="False" AllowPaging="True" PageSize="15" Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" OnItemDataBound="dg_manualcc_ItemDataBound" OnPageIndexChanged="dg_manualcc_PageIndexChanged" OnSelectedIndexChanged="dg_manualcc_SelectedIndexChanged" BackColor="#EFF4FB">
														<Columns>
                                                            <asp:TemplateColumn HeaderText="Card Holder">
								                                    <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server"  ID="lbl_cardholder" Text='<%# DataBinder.Eval(Container, "DataItem.cardholder") %>'></asp:Label><br />
                                                                    <asp:HiddenField ID="hf_ccno" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.ccno") %>' />
                                                                    <asp:HiddenField ID="hf_paymentid" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.PaymentID") %>' /><asp:HiddenField ID="hf_ref" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.refundamount") %>' />
                                                                    <asp:HiddenField ID="hf_voidpayment" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.voidpayment") %>' />
                                                                </ItemTemplate>
                                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                    Font-Underline="False" HorizontalAlign="Left" CssClass="GrdHeader" />
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Client Type">
                                                             <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                    Font-Underline="False" HorizontalAlign="Left" CssClass="GrdHeader" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_clienttype" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.clienttype") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="CIN" Visible="False">
								                                            <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" ID="lbl_cin" Text='<%# DataBinder.Eval(Container, "DataItem.cin") %>'></asp:Label>
                                                                </ItemTemplate>                                                               
                                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                    Font-Underline="False" HorizontalAlign="Left" CssClass="GrdHeader" />
                                                            </asp:TemplateColumn>
                                                            
                                                            <asp:TemplateColumn HeaderText="Card Number">
                                                                <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" ></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" id="lbl_cardno" Text='<%# DataBinder.Eval(Container, "DataItem.ccno") %>'></asp:Label>
                                                                </ItemTemplate>  
                                                                    <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                    Font-Underline="False" HorizontalAlign="Left" CssClass="GrdHeader" />                                                              
                                                            </asp:TemplateColumn>
                                                           
                                                            <asp:TemplateColumn HeaderText="Trans No">
								                                    <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" ></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" id="lbl_transacion" Text='<%# DataBinder.Eval(Container, "DataItem.transNum") %>'></asp:Label>
                                                                </ItemTemplate>                                                                
                                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                    Font-Underline="False" HorizontalAlign="Left" CssClass="GrdHeader" />
                                                            </asp:TemplateColumn>
                                                            
                                                            <asp:TemplateColumn HeaderText="Date">
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" ID="lbl_tranddate" Text='<%# DataBinder.Eval(Container, "DataItem.recdate") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                    Font-Underline="False" HorizontalAlign="Left" CssClass="GrdHeader" />
                                                                <EditItemTemplate>
                                                                    &nbsp;
                                                                </EditItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Expiry Date" Visible="False">
								                                    <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" ID="lbl_expirydate" Text='<%# DataBinder.Eval(Container, "DataItem.expdate") %>'></asp:Label>
                                                                </ItemTemplate>                                                                
                                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                    Font-Underline="False" HorizontalAlign="Left" CssClass="GrdHeader" />
                                                            </asp:TemplateColumn>
                                                           
                                                            <asp:TemplateColumn HeaderText="Amount">
								                                    <ItemStyle HorizontalAlign="Left" CssClass="grdlabel"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" id="lbl_amount" Text='<%# DataBinder.Eval(Container, "DataItem.amount","{0:C2}") %>'></asp:Label>
                                                                </ItemTemplate>                                                               
                                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                    Font-Underline="False" HorizontalAlign="Left" CssClass="GrdHeader" />
                                                            </asp:TemplateColumn>
                                                           
                                                            <asp:TemplateColumn HeaderText="Rep">
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" id="lbl_empname" Text='<%# DataBinder.Eval(Container, "DataItem.abbreviation") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                 Font-Underline="False" HorizontalAlign="Left" CssClass="GrdHeader" />                                                                
                                                            </asp:TemplateColumn>
                                                            
                                                            <asp:TemplateColumn HeaderText="Status">
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" id="lbl_transstatus" Text='<%# DataBinder.Eval(Container, "DataItem.response_reason_text") %>'></asp:Label>
                                                                </ItemTemplate>   
                                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                 Font-Underline="False" HorizontalAlign="Left" CssClass="GrdHeader" />                                                                                                                            
                                                            </asp:TemplateColumn>
                                                           
                                                           
                                                           
                                                            <asp:TemplateColumn>
                                                                <ItemTemplate>
                                                                <DIV runat="server" id="div_status">
                                                                <asp:Label ID="lbl_description" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>' Visible="False"></asp:Label>&nbsp;
                                                                
                                                                    <asp:Image ID="imgdescript" runat="server" ImageUrl="~/Images/info_desc.jpg" Height="18px" Width="18px" /><%--   <asp:Image ID="img_status" runat="server" ImageUrl="../Images/right.gif" />--%></DIV>                                                                 
                                                               
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            
                                                            
                                                            
                                                            
                                                            
                                                            <asp:TemplateColumn>
                                                                 <ItemStyle HorizontalAlign="Left" CssClass="grdlabel"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkbtn_void" runat="server" Text='Void' Visible="True"></asp:LinkButton>
                                                                    <asp:LinkButton ID="lnkbtn_refund" runat="server" Text="Refund"  Visible="True"></asp:LinkButton>
                                                                </ItemTemplate>
                                                                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                    Font-Underline="False" HorizontalAlign="Center" />
                                                            </asp:TemplateColumn>
														
														</Columns>
														<PagerStyle VerticalAlign="Middle" NextPageText="   Next &gt;" PrevPageText="  &lt; Previous        "
															HorizontalAlign="Center"></PagerStyle>
                                                    <AlternatingItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                        Font-Strikeout="False" Font-Underline="False" />
                                                    <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" />
													</asp:datagrid></td>
											</tr>
											<tr>
												<td width="780" background="../images/separator_repeat.gif" colSpan="5" height="11"></td>
											</tr>
											<TR>
												<TD colSpan="5"><uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
											</TR>
										</TABLE>
            <table id="tbl_plzwait" class="clssubhead" style="display: none; height: 60px" width="100%">
                <tr>
                    <td align="center" class="clssubhead" valign="middle">
                        <img src="../Images/plzwait.gif" />
                        &nbsp; Please wait while we process your request.
                    </td>
                </tr>
            </table>
            &nbsp;
                                        </ContentTemplate>
                                         <Triggers>
                                             <asp:AsyncPostBackTrigger ControlID="btn_popup" EventName="Click" />
                                         </Triggers>
        </asp:UpdatePanel>
                                        </TD>
								</TR>
							</TABLE>
                            <div id="zee" style="display: none; left: 785px; width: 250px; position: absolute;
                                top: 805px; background-color: transparent">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                
                                    <ContentTemplate>
                                    
                                        <asp:Panel ID="zeeans" runat="server" Height="50px" Width="125px">
                                            <table border="2" enableviewstate="true" style="border-left-color: navy; border-bottom-color: navy;
                                                border-top-color: navy; border-collapse: collapse; border-right-color: navy">
                                                <tr>
                                                    <td background="../Images/subhead_bg.gif" valign="bottom" align="left">
                                                        <table border="0" width="380">
                                                            <tr>
                                                                <td class="clssubhead" style="height: 26px; width: 250px;">
                                                                 <asp:Label ID="lblheading" runat="server" Text="Void Transaction"></asp:Label> </td>
                                                                <td align="right" style="width: 83px">
                                                                    <asp:LinkButton ID="lbtn_close" runat="server" OnClientClick="closepopup();return false;">X</asp:LinkButton></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top">
                                                        <table border="1" cellpadding="0" cellspacing="0" class="clsleftpaddingtable" style="border-top: 0px;
                                                            border-right-style: none; border-left-style: none; border-collapse: collapse;
                                                            border-bottom-style: none; width: 382px; height: 205px;">
                                                            <tr style="color: #000000; font-family: Tahoma">
                                                                <td class="clssubhead" style="width: 123px; height: 20px">
                                                                    Transaction Date</td>
                                                                <td style="width: 161px; height: 20px">
                                                                    <asp:Label ID="lbltransdate" runat="server" CssClass="label"></asp:Label></td>
                                                            </tr>
                                                            <tr style="font-family: Tahoma">
                                                                <td class="clssubhead" style="width: 123px; height: 19px">
                                                                    Name</td>
                                                                <td style="width: 161px; height: 19px">
                                                                    <asp:Label ID="lblname" runat="server" CssClass="label"></asp:Label></td>
                                                            </tr>
                                                            <tr style="font-family: Tahoma">
                                                                <td class="clssubhead" style="width: 123px; height: 17px">
                                                                    Transaction No</td>
                                                                <td style="width: 161px; height: 17px">
                                                                    <asp:Label ID="lbltransno" runat="server" CssClass="label"></asp:Label></td>
                                                            </tr>
                                                            <tr style="font-family: Tahoma">
                                                                <td class="clssubhead" style="width: 123px">
                                                                    Charged Amount</td>
                                                                <td style="width: 161px">
                                                                    <asp:Label ID="lblchargedamount" runat="server" CssClass="label"></asp:Label></td>
                                                            </tr>
                                                            <tr style="font-family: Tahoma">
                                                                <td class="clssubhead" style="width: 123px">
                                                                    <asp:Label ID="lblcurrentamoun" runat="server"></asp:Label></td>
                                                                <td style="width: 161px">
                                                                    <asp:TextBox ID="tb_refundamount" runat="server" CssClass="clsinputadministration" Width="107px" Enabled="False"></asp:TextBox></td>
                                                            </tr>
                                                            <tr style="font-family: Tahoma">
                                                                <td class="clssubhead" style="width: 123px">
                                                                    Description</td>
                                                                <td style="width: 161px">
                                                                    <asp:TextBox ID="tb_description" runat="server" CssClass="clsinputadministration"
                                                                        Height="61px" MaxLength="500" TextMode="MultiLine" Width="222px"></asp:TextBox></td>
                                                            </tr>
                                                            <tr style="font-family: Tahoma">
                                                                <td class="clssubhead" style="height: 28px; hegiht: 70px;">
                                                                </td>
                                                                <td style="height: 28px; hegiht: 70px;" valign="middle">
                                                                    <asp:Button ID="btn_popup" runat="server" CssClass="clsbutton"
                                                                        OnClientClick="return submitPopup();" Text="Update" OnClick="btn_popup_Click" />&nbsp;
                                                                        <asp:Button ID="btn_cancel" OnClientClick="closepopup();return false;" runat="server" CssClass="clsbutton" EnableViewState="False" Text="Cancel"
                                                                            Width="46px" /></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                                        </asp:Panel>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btn_popup" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                                <asp:HiddenField ID="hf_ccno" runat="server" />
                                <asp:HiddenField ID="hf_transtype" runat="server" />
                                <asp:HiddenField ID="hf_paymentid" runat="server" />
                            </div>
                            &nbsp;
                       
						</TD>
						  
					</TR>
				</tbody>
				
                   
				
			
			</TABLE>
			
      
    </form>
</body>
</html>

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using lntechNew.Components;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using System.Configuration;
using HTP.Components.Services;
//Nasir 7223 01/14/2010 add namespace Helper, Client
using Configuration.Helper;
using Configuration.Client;


namespace HTP.Reports
{
    public partial class RptTrialDocket : System.Web.UI.Page
    {

        //Comment By Zeeshan Ahmed Useless Code
        //clsSession uSession = new clsSession();
        //clsLogger clog = new clsLogger();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Create Trial Docket Report
                CreateReport();
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
            }
        }

        public void CreateReport()
        {

            clsCrsytalComponent Cr = new clsCrsytalComponent();
            clsENationWebComponents ClsDb = new clsENationWebComponents();

            //Getting Paremeters From The Query String

            string courtloc = Convert.ToString(Request.QueryString["Courtloc"]);
            DateTime courtdate = Convert.ToDateTime(Request.QueryString["Courtdate"]);
            string page = Convert.ToString(Request.QueryString["Page"]);
            string courtnumber = Convert.ToString(Request.QueryString["Courtnumber"]);
            string datetype = Convert.ToString(Request.QueryString["Datetype"]);
            string singles = Convert.ToString(Request.QueryString["singles"]);
            string filename = string.Empty;

            //Added By Zeeshan Ahmed On 1/1/2008
            string showowesdetail = Convert.ToString(Request.QueryString["showowesdetail"]);

            DataSet ds;
            string name = "TrialDocket-" + DateTime.Now.ToFileTime() + ".pdf";
            // Noufil 5772 04/10/2009 New sub Report Added.
            //Nasir 5817 04/29/2009 new sub report Officer Summary
            String[] reportname = new String[] { "ArraignmentWaiting.rpt", "Judge.rpt", "OfficerSummary.rpt", "Report1.rpt" };
            string[] keySub = { "@URL", "@Courtloc", "@courtdate" };
            object[] valueSub = { Request.ServerVariables["SERVER_NAME"].ToString(), courtloc, courtdate };
            ds = ClsDb.Get_DS_BySPArr("USP_HTS_GET_ST_TrialDocket", keySub, valueSub);
            string path = ConfigurationSettings.AppSettings["NTPATHDocketPDF"].ToString();

            //ShowOwesDetail Parameter Is Added By Zeeshan Ahmed On 1/1/2008
            string[] key = { "@courtloc", "@courtdate", "@page", "@courtnumber", "@datetype", "@singles", "@URL", "@ShowOwesDetail" };
            object[] value1 = { courtloc, courtdate, page, courtnumber, datetype, singles, Request.ServerVariables["SERVER_NAME"].ToString(), showowesdetail };

            //Nasir 7223 01/13/2010 get path form configuration service
            if (singles == "0")
            {
                //Nasir 7223 01/14/2010 Get Path From COnfiguration files
                ConfigurationService ConfigurationService = new ConfigurationService();                
                filename = ConfigurationService.GetFilePath(SubModule.GENERAL, Key.REPORT_FILE_PATH) + "\\TrialDocket.rpt"; ;                
            }
            else if (singles == "1")
                filename = Server.MapPath("") + "\\TrialDocketSingles.rpt";

            Cr.CreateSubReports(filename, "usp_hts_get_trial_docket_report_CR", key, value1, reportname, ds, 1, this.Session, this.Response, path, name.ToString());

        }


    }
}

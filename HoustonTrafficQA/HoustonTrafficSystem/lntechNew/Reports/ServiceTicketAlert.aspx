<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ServiceTicketAlert.aspx.cs" Inherits="lntechNew.Reports.ServiceTicketAlert" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <LINK href="../Styles.css" type="text/css" rel="stylesheet">
    <title>Service Ticket Alert</title>
    <script type="text/javascript">
    function RedirectWindow(redirectURL)
    {
        window.opener.location = redirectURL;
        window.close();
        return(false);
    }
    </script>
</head>
<body style="position: static; top: 0px; left: 0px; width: 0px; height: 0px;">
    <form id="form1" runat="server">
    <div>
    <TABLE id="TableMain" cellSpacing="0" cellPadding="0"  border="0" align="left" style="width: 459px; height: 200px"  >
				<TR>
					<TD class="clssubhead" background="../Images/subhead_bg.gif" colSpan="2" height="34">&nbsp;Service Ticket Alert&nbsp;</TD>
				</TR>
				<TR>
					<TD vAlign="top" colSpan="2" align="center">
						<TABLE id="tbl1" cellSpacing="0" cellPadding="0" width="99%" border="0">
						    <tr>
						        <td colspan="2" align="center" class="clsLeftPaddingTable" >
                                    &nbsp;</td>
						    </tr>
                            <tr>
                                <td align="left" class="clsLeftPaddingTable" valign="middle" rowspan="" style="height: 16px; width: 147px;">
                                    <strong class="Clssubhead">
                                    Pending Service Tickets</strong></td>
                                <td align="left" class="clsLeftPaddingTable" style="height: 16px">
                                    &nbsp;<asp:HyperLink ID="HPPendingTask" runat="server" NavigateUrl="#" Target="_blank">[HPPendingTask]</asp:HyperLink>
                                     &nbsp;<asp:CheckBox ID="chkShow" runat="server" Text="Do Not Show This Popup Again" />
                                    </td>
                            </tr>
							<tr>
							<TD class="clsLeftPaddingTable" align="right" style="height:5px; width: 147px;"></TD>
							<TD class="clsLeftPaddingTable" align="right" style="height:5px"></TD>
							</tr>
							<TR>
								<TD class="clsLeftPaddingTable" align="right" style="width: 147px; height: 20px;"></TD>
								<TD class="clsLeftPaddingTable" align="right" style="height: 20px">
									<asp:Button id="btnSubmit" runat="server" Text="Close This Window" CssClass="clsbutton" Width="119px" OnClick="btnSubmit_Click"></asp:Button></TD>
							</TR>
							
						</TABLE>
					</TD>
				</TR>
			</TABLE>
    </div>
    </form>
</body>
</html>

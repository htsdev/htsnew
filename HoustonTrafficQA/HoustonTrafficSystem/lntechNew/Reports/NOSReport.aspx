<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NOSReport.aspx.cs" Inherits="HTP.Reports.NOSReport"
    ValidateRequest="false" EnableEventValidation="false" %>

<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/UpdateFollowUpInfo.ascx" TagName="UpdateFollowUpInfo"
    TagPrefix="uc5" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Not on System Report</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />

    <script src="../Scripts/Dates.js" type="text/javascript"></script>

    <script src="../Scripts/jsDate.js" type="text/javascript"></script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <aspnew:ScriptManager ID="ScriptManager1" runat="server">
            <Scripts>
                <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
            </Scripts>                        
        </aspnew:ScriptManager>
        
                        
        <table id="TableMain" align="center" border="0" cellpadding="0" cellspacing="0" style="z-index: 101"
            width="780">
            <tbody>
                <tr>
                    <td colspan="4" style="height: 14px">
                        <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="TableSub" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td background="../images/separator_repeat.gif" colspan="7" height="11">
                                </td>
                            </tr>
                            <tr>
                                <td background="../Images/subhead_bg.gif" class="clssubhead" align="right" style="height: 34px">
                                    <table style="width: 100%">
                                        <tr>
                                            <td>
                                            </td>
                                            <td style="text-align: right">
                                                <aspnew:UpdatePanel ID="updatepnlpaging" runat="server" RenderMode="Inline">
                                                    <ContentTemplate>
                                                        <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                                    </ContentTemplate>
                                                </aspnew:UpdatePanel>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Label ID="lbl_message" runat="server" CssClass="Label" ForeColor="Red"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                            
                                <td>
                                    <table id="TableGrid" bgcolor="white" border="0" cellpadding="0" cellspacing="0"
                                        width="100%">
                                        <tr>
                                            <td colspan="5" width="780">
                                                <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline">
                                                    <ContentTemplate>
                                                        <asp:GridView ID="gv_records" runat="server" AutoGenerateColumns="False" Width="100%"
                                                            AllowPaging="True" OnPageIndexChanging="gv_records_PageIndexChanging" PageSize="20"
                                                            CssClass="clsLeftPaddingTable" OnRowCommand="gv_records_RowCommand" OnRowDataBound="gv_records_RowDataBound"
                                                            AllowSorting="True" OnSorting="gv_records_Sorting">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="S.No">
                                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        &nbsp;<asp:HyperLink ID="lbl_sno" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                                            Text='<%# DataBinder.Eval(Container, "DataItem.sno") %>'></asp:HyperLink>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Last Name">
                                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_firstname" runat="server" CssClass="Label" Text='<%# Bind("lastname") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="First Name">
                                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_lastname" runat="server" CssClass="Label" Text='<%# Bind("firstname") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Cause No">
                                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_causenum" runat="server" CssClass="Label" Text='<%# Bind("causenumber") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Ticket No">
                                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_ticketno" runat="server" CssClass="label" Text='<%# Bind("refcasenumber") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="<u>Court Loc</u>" SortExpression="shortname">
                                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_crtloc" runat="server" CssClass="Label" Text='<%# Bind("shortname") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="<u>Hire Date</u>" SortExpression="hiredatesort">
                                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_hiredate" runat="server" CssClass="Label" Text='<%# Bind("HireDate") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="<u>FollowUpDate</u>" SortExpression="followupdatesort">
                                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_followup" runat="server" CssClass="Label" Text='<%# Bind("followupdate") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="img_Add" runat="server" CommandName="btnclick" ImageUrl="../Images/add.gif" />
                                                                        <asp:HiddenField ID="hf_fname" runat="server" Value='<%#Eval("firstname") %>' />
                                                                        <asp:HiddenField ID="hf_lname" runat="server" Value='<%#Eval("lastname") %>' />
                                                                        <asp:HiddenField ID="hf_ticketid_pk" runat="server" Value='<%#Eval("ticketid_pk") %>' />
                                                                        <asp:HiddenField ID="hf_causeno" runat="server" Value='<%#Eval("causenumber") %>' />
                                                                        <asp:HiddenField ID="hf_courtid" runat="server" Value='<%#Eval("CourtId") %>' />
                                                                        <asp:HiddenField ID="hf_casetypeid" runat="server" Value='<%#Eval("CaseTypeId") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <PagerStyle HorizontalAlign="Center" />
                                                            <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                                FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                                        </asp:GridView>
                                                    </ContentTemplate>
                                                </aspnew:UpdatePanel>
                                                <ajaxToolkit:UpdatePanelAnimationExtender TargetControlID="upnlResult" runat=server>
                                                <Animations>
                                                
                                                </Animations>
                                                </ajaxToolkit:UpdatePanelAnimationExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td background="../images/separator_repeat.gif" colspan="5" height="11" width="780">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <aspnew:UpdateProgress ID="updateprogress2" runat="server" AssociatedUpdatePanelID="UpdatePanelcrtl">
                                                    <ProgressTemplate>
                                                        <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl2" runat="server" Text="Please Wait ......"
                                                            CssClass="clsLabel"></asp:Label>
                                                    </ProgressTemplate>
                                                </aspnew:UpdateProgress>
                                                <aspnew:UpdatePanel ID="UpdatePanelcrtl" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Panel ID="pnlFollowup" runat="server">
                                                            <uc5:UpdateFollowUpInfo ID="UpdateFollowUpInfo" runat="server" Title="LOR Follow-up" />
                                                        </asp:Panel>
                                                        <ajaxToolkit:ModalPopupExtender ID="mpeTrafficwaiting" runat="server" BackgroundCssClass="modalBackground"
                                                            PopupControlID="pnlFollowup" TargetControlID="btn">
                                                        </ajaxToolkit:ModalPopupExtender>
                                                        <asp:Button ID="btn" runat="server" Style="display: none;" />
                                                        <ajaxToolkit:AnimationExtender BehaviorID="AEFadeIn" runat="server" Enabled="True"
                                                            TargetControlID="hfFadeIn">
                                                            <Animations>
                                                                <OnClick>
                                                                    <%-- We need set the AnimationTarget with the control which needs to make animation --%>
                                                                    <Sequence AnimationTarget="pnlFollowup">
                                                                        <%--The FadeIn and Display animation.--%>                     
                                                                        <FadeIn Duration=".3" MinimumOpacity="0" MaximumOpacity="1" />
                                                                        <ReSize AnimationTarget="pnlFollowup" height="124px" width="430px" duration ="0" fps="0" unit="px" />
                                                                        
                                                                    </Sequence>
                                                                </OnClick>                                                                
                                                            </Animations>
                                                        </ajaxToolkit:AnimationExtender>
                                                        <asp:HiddenField runat="server" ID="hfFadeIn" />
                                                    </ContentTemplate>
                                                </aspnew:UpdatePanel>
                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="5">
                                                <uc1:Footer ID="Footer1" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <asp:HiddenField ID="CheckHMC" runat="server" />
    <asp:Panel ID="divDisable" runat="server" Style="z-index: 1; display: none; position: absolute;
        left: 1; top: 1; height: 1px; background-color: Silver; filter: alpha(opacity=50)">
    </asp:Panel>
    </form>

    <script language="javascript" type="text/javascript">
        document.getElementById("UpdateFollowUpInfo_cal_followupdate_div").style.zIndex = 10003;
        document.getElementById("UpdateFollowUpInfo_cal_followupdate_monthYear").style.zIndex = 10004;
    </script>

</body>
</html>

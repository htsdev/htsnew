<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HMCsubmittedcases.aspx.cs"
    Inherits="HTP.Reports.HMCsubmittedcases" EnableEventValidation="False" %>

<%@ Register Src="../WebControls/UpdateFollowUpInfo.ascx" TagName="UpdateFollowUpInfo"TagPrefix="uc3" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%--Yasir Kamal 5423 02/12/2009 Follow Up date in Next Six Business days --%>
<%@ Register Src="../WebControls/ShowSetting.ascx" TagName="ShowSetting" TagPrefix="uc4" %>
<%@ Register TagPrefix="mbrsc" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>HMC A/W DLQ Alert</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>

       <script language="javascript">
        
    function SelectCheckBoxBeforShowPopup(generalcomments,casenumber,ticid,dgname,followupdate,lastname,firstname,ticketnumber,causeno)
    {
        
        
        var tkiid;
		var chked;
		var count;
		var ticketid;
		
		tkiid = "_hf_TicketId";
		chked="_ctl00";
		ticketid="";
		count=document.getElementById ("hf_RecordCount").value;
	    casenumber=casenumber.replace("lbl_CaseNumber","ctl00");
	    var rows = document.getElementById (casenumber).parentElement.parentElement.parentElement.children.length;	
	    document.getElementById (casenumber).checked=true;
	    
	    //Zeeshan Ahmed 4420 08/06/2008 Refactor Code
	    //Sabir Khan 7541 03/09/2010 Fix follow update bug...
        var grid=document.getElementById("DG_HMC_cases");
        var rows=grid.rows.length;
        var colIndex = 11;
        for ( i = 1 ; i<rows ; i++)
        {      
            if (  grid.rows[i].cells[colIndex].getElementsByTagName("INPUT")[0].checked)
            {
                  ticketid += grid.rows[i].cells[6].getElementsByTagName("INPUT")[0].value + ",";
            }   
        }
              
        ShowPopUp_1(document.getElementById(generalcomments).value,ticid,ticketid,followupdate,lastname,firstname,ticketnumber,causeno);   
        return false; 
     }
     
    function PopUp(dbid,sbid)
    {
        if(dbid !=0 && sbid != 0)
        {
            var path="../documenttracking/PreviewMain.aspx?DBID="+dbid + "&SBID=" + sbid ;
	        window.open(path,'',"fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=yes");
        }
        else
        {
            alert("Sorry,NO Document Available");
        }
        return false;
    }
    
    //ozair 4879 09/29/2008 method to display old scanned pdf documents
    function PopUpNew(docid)  
	{
	    window.open("../ClientInfo/frmPreview.aspx?RecordID="+docid,'',"fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=yes");
	    return false;
	}
    
    function countcheck()
    {
        
        if(document.getElementById('hf_RecordCount').value == "0")
        {
              alert('No records to perform this operation');
              return(false);
        }
        else
        {
            return(true);
        }
    }
    
    
    function ShowEmailPopup()
    {
        if (CheckRecord())
        {
            fillBody();
            var modalPopupBehavior = $find('mpeEmail');
            modalPopupBehavior.show(); 
        }
        return false;
    }
    
    
//Zeeshan Ahmed 4420 08/06/2008 Select Past Court Date Records
function CheckPastCourtDateRecord(chkbox)
{
   var grid=document.getElementById("DG_HMC_cases");
   var rows=grid.rows.length;
   var colIndex = 11;
   var todayDate = new Date();
                         
    if (chkbox.checked)
    {   
        for ( i = 1 ; i<rows ; i++)
        {      
            var temp = grid.rows[i].cells[8].innerText;
            var followupdate = new Date(temp);
            grid.rows[i].cells[colIndex].getElementsByTagName("INPUT")[0].checked=(todayDate.getTime() >followupdate.getTime())?true : false;
        }
    }
    else
    {
        for ( i = 1 ; i<rows ; i++)
        {      
            grid.rows[i].cells[colIndex].getElementsByTagName("INPUT")[0].checked=false;
        }
    }
}
    
//Zeeshan Ahmed 4420 08/06/2008 Fill Body With Selected Cases
function fillBody()
{
    var Body = "Please find attached  Arraignment Setting Summaries signed by a HMC Clerk. I called 311 today and the cases listed below are in DLQ status.   Please have someone look into these cases:\n\n";
    var Subject = "Please remove Clients in DLQ Status.";
    var TicketIds= "";
    var grid=document.getElementById("DG_HMC_cases");
    var rows=grid.rows.length;
    var colIndex = 11;
    var firstNameCol= 4;       
    var LastNameCol= 3;      
    var TicketNoCol= 2  ;
    var CauseNoCol= 1  ;
    var DobCol = 9;

    for ( i = 1 ; i<rows ; i++)
    {      
        if (  grid.rows[i].cells[colIndex].getElementsByTagName("INPUT")[0].checked)
        {
              Body += "\n" +     grid.rows[i].cells[LastNameCol].innerText  + " , " + grid.rows[i].cells[firstNameCol].innerText  + " ( " +  grid.rows[i].cells[DobCol].getElementsByTagName("INPUT")[0].value + " ) - " + grid.rows[i].cells[CauseNoCol].innerText  + " / " + grid.rows[i].cells[TicketNoCol].innerText; 
              TicketIds += grid.rows[i].cells[6].getElementsByTagName("INPUT")[0].value + ",";
        }   
    }
    //Zeeshan 4645 08/20/2008 Display Signature in the email body
    // Noufil 4645 08/21/2008 Use first name space last name in signature
    Body += "\n\n\n"+ "<%= ViewState["RepFName"] %> "+"<%= ViewState["RepLName"] %>"+ "\nParalegal\nSullo & Sullo, LLP. Houston\nAttorneys at Law\n2020 Southwest Freeway, Suite #300\nHouston, Texas  77098\nTel: 713-839-9026\nFax: 713-523-6634\nwww.sullolaw.com"

    document.getElementById("tbBody").value = Body;
    document.getElementById("hfTicketIds").value = TicketIds;
    document.getElementById("tbSubject").value = Subject;
    document.getElementById("tbToEmails").value = "";
}   
    
   
//Zeeshan Ahmed 4420 08/06/2008 Check Whether records are selected or not   
function CheckRecord()
{
   var grid=document.getElementById("DG_HMC_cases");
   var rows=grid.rows.length;
   var colIndex = 11;
       
    for ( i = 0 ; i<rows ; i++)
    {      
         if (grid.rows[i].cells[colIndex].getElementsByTagName("INPUT")[0].checked)
         return true;
    }
    
    alert("Please select cases from the list.");
    return false;
}
   
//Zeeshan Ahmed 4420 08/06/2008 Verify Email Popup
function submitform()
{
    var values = document.getElementById('tbToEmails').value;
    
    if ( values == "")
    {
        alert("Please enter email address.");
        document.getElementById('tbToEmails').focus();
        return false;
    }
    
    if(!multiEmail(values))
    {
        document.getElementById('tbToEmails').focus();
        return false;
    }

    if ( document.getElementById("tbSubject").value == "")
    {
         alert("Please enter subject.");
         document.getElementById('tbSubject').focus();
         return false;
    }
    
    var modalPopupBehavior = $find('mpeEmail');
    modalPopupBehavior.hide(); 
    return true;
    
    
}

//Zeeshan Ahmed 4420 08/06/2008 Verify Email Addresses
function multiEmail(email_field)
{
	var email = email_field.split(',');
	for(var i = 0; i < email.length; i++) 
	{
		if (!isEmail(email[i])) 
			{
				alert('One or more email addresses entered is invalid');
				return false;
			}
	}
	return true;
} 		 

    		
    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            height: 34px;
            width: 32px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="TableMain" cellspacing="0" cellpadding="0" width="900" align="center"
            border="0">
            <tr>
                <td style="width: 896px;">
                    <uc2:ActiveMenu ID="ActiveMenu1" runat="server"></uc2:ActiveMenu>
                </td>
            </tr>
            <tr>
                <td style="width: 896px;" align="center">
                    <asp:Label ID="lbl_Message" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                    &nbsp; &nbsp; &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <table border="0" style="border-collapse: collapse" width="100%">
                        <tr>
                            <td align="right" style="font-size: 10pt; width: 87%; height: 34px">
                                &nbsp;
                            </td>
                            <%--Sabir Khan 4894 10/04/2008--%>
                            <td align="right" style="font-size: 10pt;" class="style1">
                                <asp:ImageButton ID="imgExp_Ds_to_Excel" runat="server" ImageUrl="../Images/excel_icon.gif"
                                    Width="81px" Height="36px" ToolTip="Excel" OnClick="imgExp_Ds_to_Excel_Click"
                                    OnClientClick="return CheckRecord();"></asp:ImageButton>
                                <font face="Arial" size="1">&nbsp;&nbsp;&nbsp;&nbsp;EXCEL</font>
                            </td>
                            <td align="center" style="font-size: 10pt; width: 20%; height: 34px;">
                                <asp:ImageButton ID="ibtn_Email" runat="server" ImageUrl="../Images/Email.jpg" ToolTip="Email"
                                    OnClick="SendMail" OnClientClick="return ShowEmailPopup();" />&nbsp;<br />
                                <font face="Arial" size="1">EMAIL</font>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td background="../Images/subhead_bg.gif" class="clssubhead" align="right" style="height: 34px"
                    valign="middle">
                    <table style="width: 100%">
                        <tr>
                            <td style="width:600px;" align="left">
                                <uc4:ShowSetting ID="ShowSetting" runat="server" lbl_TextBefore="FollowUpDate In Next"
                                    lbl_TextAfter="(Business Days)" Attribute_Key="Days" />
                            </td>
           
                           <td style="width:190px">
                                <asp:CheckBox ID="cbPassedCourtDate" runat="server" CssClass="clssubhead" Text="Select Past Follow Up Records"
                                    TextAlign="Left" onClick="CheckPastCourtDateRecord(this);" />
                            </td>
                            <td>
                                <asp:CheckBox TextAlign="Left" CssClass="clssubhead" AutoPostBack="true" Text="ShowAll" ID="ShowAll"
                                    runat="server" oncheckedchanged="ShowAll_CheckedChanged" />
                            </td>
                            
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center" background="../Images/separator_repeat.gif" style="width: 896px;
                    height: 11px;">
                </td>
            </tr>
            <tr>
                <td style="width: 896px; height: 126px;">
                    <aspnew:ScriptManager ID="ScriptManager1" runat="server">
                        <Scripts>
                            <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
                        </Scripts>
                    </aspnew:ScriptManager>
                    <aspnew:UpdatePanel ID="upnlEmail" runat="server">
                        <ContentTemplate>
                            <asp:GridView ID="DG_HMC_cases" runat="server" AutoGenerateColumns="False" CssClass="clsLeftPaddingTable"
                                Width="100%" PageSize="30" HorizontalAlign="Center" AllowSorting="True" OnSorting="gv_Records_Sorting"
                                OnRowDataBound="DG_HMC_cases_RowDataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="S#">
                                        <HeaderStyle CssClass="clsaspcolumnheader" />
                                        <ItemTemplate>
                                            &nbsp;
                                            <asp:HyperLink ID="sno" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                Text='<%# DataBinder.Eval(Container, "DataItem.sno") %>'></asp:HyperLink>
                                            <asp:HiddenField ID="hfsbid" runat="server" Value='<%# bind("sbid") %>' />
                                            <asp:HiddenField ID="hfdbid" runat="server" Value='<%# bind("dbid") %>' />
                                            <asp:HiddenField ID="hf_docid" runat="server" Value='<%# bind("docid") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Cause No">
                                        <HeaderStyle CssClass="clsaspcolumnheader" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_CauseNo" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.CauseNo") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="&lt;u&gt;Ticket No&lt;/u&gt;" SortExpression="ticketnumber">
                                        <HeaderStyle CssClass="clsaspcolumnheaderblack" Font-Underline="True" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_CaseNumber" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.ticketnumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="&lt;u&gt;Last Name&lt;/u&gt;" SortExpression="lname">
                                        <HeaderStyle CssClass="clsaspcolumnheaderblack" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_LastName" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.lname") %>'></asp:Label>
                                            <asp:HiddenField ID="hfgeneralComments" runat="server" Value="" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="&lt;u&gt;First Name&lt;/u&gt;" SortExpression="fname">
                                        <HeaderStyle CssClass="clsaspcolumnheaderblack" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_FirstName" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.fname") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="DOB">
                                        <HeaderStyle CssClass="clsaspcolumnheader" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_dob" runat="server" CssClass="clsLabel" Text='<%# Eval("dob","{0: MM/dd/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--Added sort Expression for Task ID 4712--%>
                                    <asp:TemplateField HeaderText="&lt;u&gt;Crt Date &amp; Time&lt;/u&gt;" SortExpression="courtdatemain"
                                        HeaderStyle-Width="125px">
                                        <HeaderStyle CssClass="clsaspcolumnheader" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_CourtDate" runat="server" CssClass="clsLabel" Text='<%# bind("courtdatemain")%>'></asp:Label>
                                            <asp:HiddenField ID="hf_TicketId" runat="server" Value='<%# bind("ticketid_pk") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <%--Added sort Expression for Task ID 4712--%>
                                    <asp:TemplateField HeaderText="&lt;u&gt;ArrWaitDate &amp; Time&lt;/u&gt;" SortExpression="arrwaitdate">
                                        <HeaderStyle CssClass="clsaspcolumnheader" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_ArrWaitDate" runat="server" CssClass="clsLabel" Text='<%# bind("arrwaitdate") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <%--Added sort Expression for Task ID 4712--%>
                                    <asp:TemplateField HeaderText="&lt;u&gt;Follow Up Date&lt;/u&gt;" SortExpression="FollowUpDate">
                                        <HeaderStyle CssClass="clsaspcolumnheader" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_FollowUpD" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.FollowUpDate","{0:MM/dd/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Emails">
                                        <HeaderStyle CssClass="clsaspcolumnheader" />
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hfDob" runat="server" Value='<%# Eval("Dob","{0:d}") %>' />
                                            <asp:Label ID="lblEmailCount" runat="server" CssClass="clsLabel" Text='<%# Eval("EmailCount") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton ImageUrl="../Images/add.gif" ID="img_Add" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <mbrsc:SelectorField SelectionMode="Multiple" AllowSelectAll="True">
                                    </mbrsc:SelectorField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            &nbsp;<asp:HiddenField ID="hf_recordid" runat="server" />
                                            <asp:ImageButton ID="img_summary" runat="server" ImageUrl="~/Images/preview.gif" />
                                            <asp:Label ID="lbl_recordid" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.recordid") %>'
                                                Visible="False"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerSettings NextPageText="Next &gt;" PreviousPageText="&lt; Previous" Mode="NextPrevious" />
                                <PagerStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:GridView>
                            <uc3:UpdateFollowUpInfo ID="UpdateFollowUpInfo2" runat="server" Title="A/W Follow Up Date"
                                EnableHideDropDown="false" />
                            <asp:HiddenField ID="hf_RecordCount" runat="server" />
                            <asp:HiddenField ID="hf_Emails" runat="server" />
                            <ajaxToolkit:ModalPopupExtender ID="mpeEmail" runat="server" BackgroundCssClass="modalBackground"
                                CancelControlID="btnCancel" PopupControlID="pnlEmail" TargetControlID="btnEmail">
                            </ajaxToolkit:ModalPopupExtender>
                            <asp:Panel ID="pnlEmail" runat="server" Height="400px" Width="500px" Style="display: none;">
                                <table border="2" width="100%" style="border-color: navy; border-collapse: collapse;">
                                    <tr>
                                        <td>
                                            <table border="0" cellpadding="0" cellspacing="0" class="clsLeftPaddingTable" width="100%">
                                                <tr>
                                                    <td style="height: 30px" background="../Images/subhead_bg.gif" class="clssubhead">
                                                        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; height: 100%">
                                                            <tr>
                                                                <td class="clssubhead" style="width: 454px; height: 36px">
                                                                    Email Arraignment Setting Summary Documents
                                                                </td>
                                                                <td align="right" style="height: 36px">
                                                                    <asp:LinkButton ID="btnCancel" runat="server">X</asp:LinkButton>&nbsp;
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <table border="0" cellpadding="0" cellspacing="0" id="email" width="100%">
                                                            <tr>
                                                                <td style="width: 59px; height: 28px" class="clssubhead">
                                                                    To :
                                                                </td>
                                                                <td style="height: 28px;">
                                                                    <asp:TextBox ID="tbToEmails" runat="server" CssClass="clsInputadministration" Width="418px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="clssubhead" style="width: 59px; height: 26px">
                                                                    Subject :
                                                                </td>
                                                                <td style="height: 26px">
                                                                    <asp:TextBox ID="tbSubject" runat="server" CssClass="clsInputadministration" Width="419px">Please remove Clients in DLQ Status.</asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 86px; height: 26px" class="clssubhead" colspan="3">
                                                                    <asp:TextBox ID="tbBody" runat="server" CssClass="clsInputadministration" Height="277px"
                                                                        TextMode="MultiLine" Width="479px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" class="clssubhead" colspan="3" style="height: 32px" valign="middle">
                                                                    <asp:Button ID="btnSend" OnClientClick="return submitform();" runat="server" CssClass="clsbutton"
                                                                        Height="20px" OnClick="btnSend_Click" Text="Send" Width="80px" />&nbsp;
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:HiddenField ID="hfTicketIds" runat="server" />
                                            <asp:Button ID="btnEmail" runat="server" Style="display: none;" />
                                        </td>
                                    </tr>
                                </table>
                                </tr> </table>
                            </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                            <aspnew:AsyncPostBackTrigger ControlID="ibtn_Email" EventName="Click" />
                            <aspnew:PostBackTrigger ControlID="btnSend" />
                            <aspnew:PostBackTrigger ControlID="UpdateFollowUpInfo2$btnsave" />
                        </Triggers>
                    </aspnew:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td align="center" background="../Images/separator_repeat.gif" height="11px" style="width: 896px">
                </td>
            </tr>
            <tr>
                <td style="width: 896px; height: 57px;">
                    <uc1:Footer ID="Footer1" runat="server" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>


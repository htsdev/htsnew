<%@ Page Language="C#" AutoEventWireup="true" Codebehind="ConcentrationReport.aspx.cs"
    Inherits="lntechNew.Reports.ConcentrationReport" %>

<%@ Register Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2"
    Namespace="eWorld.UI" TagPrefix="ew" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc1" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Concentration Report</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
     function ShowPopup(CourtDate,CourtID,CourtRoom,ViolationID)
        {
       
            var WinSettings = "center:yes;resizable:no;dialogHeight:150px;dialogwidth:275px;scroll:no"
            var Arg = 1;
            var conf = window.showModalDialog("Popup_SelectFirm.aspx?CourtDate="+CourtDate+"&CourtID="+CourtID+"&CourtRoom="+CourtRoom+"&ViolationID="+ViolationID,Arg,WinSettings);
            
            if(conf == 1)
            {
                return(true);
            }
            else
            {
                return(false);                
            }
        
        }
        
        function ShowHideCheckBox()
        {
            var courts = document.getElementById('ddl_Courts');
            if(courts.value == '0' || courts.value == '1')
            {
                document.getElementById('td_checkBox').style.display = 'block';
                document.getElementById('hf_CheckBoxSelected').value = '1';
            }
            else
            {
                document.getElementById('td_checkBox').style.display = 'none';
                document.getElementById('hf_CheckBoxSelected').value = '0';
            }
        }
        
        function EnableCheckBox()
        {
           if(document.getElementById('hf_CheckBoxSelected').value == '0')
                document.getElementById('td_checkBox').style.display = 'none';
           else
                document.getElementById('td_checkBox').style.display = 'block';
        }
        
    </script>

</head>
<body onload="EnableCheckBox()">
    <form id="form1" runat="server">
        <div>
            <table cellspacing="0" cellpadding="0" width="780" align="center" border="0">
                <tr>
                    <td>
                        <uc1:ActiveMenu ID="ActiveMenu1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <table align="center">
                            <tr>
                                <td align="right" class="clslabel" style="width: 300px">
                                    Date:
                                    <ew:CalendarPopup ID="dtp_From" runat="server" ControlDisplay="TextBoxImage" ImageUrl="~/Images/calendar.gif"
                                        Width="86px" EnableHideDropDown="True">
                                        <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Black" />
                                        <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Black" />
                                        <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Gray" />
                                        <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Black" />
                                    </ew:CalendarPopup>
                                    &nbsp; Court:<asp:DropDownList ID="ddl_Courts" runat="server" CssClass="clsinputcombo"
                                        onChange="return ShowHideCheckBox()">
                                    </asp:DropDownList>
                                </td>
                                <td align="right" class="clslabel">
                                    Sort By:<asp:DropDownList ID="ddlSort" runat="server" CssClass="clsinputcombo" Width="125px">
                                        <asp:ListItem Selected="True" Text="Time then Court" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="Court then Time" Value="1"></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:HiddenField ID="hf_CheckBoxSelected" runat="server" Value="1" />
                                </td>
                                <td id="td_checkBox" runat="server" style="display: block; width: 152px;" class="clslabel">
                                    <asp:CheckBox ID="chk_HMCDetail" runat="server" CssClass="clslabel" Text="Show Detail"
                                        Height="9px" Width="165px" Checked="True" OnCheckedChanged="chk_HMCDetail_CheckedChanged" />
                                    <asp:CheckBox ID="chk_hidestatus" runat="server" CssClass="clslabel" Text="Display Without Case Status"
                                        Height="9px" Width="167px" /><br />
                                </td>
                                <td>
                                    <asp:Button ID="btn_Submit" runat="server" Text="Submit" CssClass="clsbutton" OnClick="btn_Submit_Click" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" background="../Images/separator_repeat.gif" height="11" width="11">
                    </td>
                </tr>
                <tr>
                    <td align="center" style="height: 13px">
                        <asp:Label ID="lbl_Message" runat="server" CssClass="clslabel" ForeColor="Red"></asp:Label></td>
                </tr>
                <tr>
                    <td>
                        <table id="tblGrids" runat="server" align="center">
                            <tr>
                                <td align="center" valign="top" width="20%">
                                    <asp:GridView ID="gv_11" runat="server" CssClass="clsleftpaddingtable" AutoGenerateColumns="False"
                                        Width="100%" OnRowDataBound="gv_11_RowDataBound" OnRowCommand="gv_11_RowCommand" >
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRowSeperator" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderStyle BorderStyle="None" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtDate" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain","{0:t}") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_CourtDate" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtName" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ShortName") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_CourtID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtNum" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.CourtNumbermain") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_ClientCount" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ClientCount") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_Status" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ShortDescription") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_ViolationID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CategoryID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbtn_Attorney" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FirmAbbreviation") %>'></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    <asp:Label ID="lbl_Message1" runat="server" CssClass="clslabel" Font-Bold="True"></asp:Label></td>
                                <td align="center" valign="top" width="20%">
                                    <asp:GridView ID="gv_12" runat="server" CssClass="clsleftpaddingtable" AutoGenerateColumns="False"
                                        OnRowCommand="gv_11_RowCommand" OnRowDataBound="gv_11_RowDataBound" Width="100%" >
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRowSeperator" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderStyle BorderStyle="None" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtDate" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain","{0:t}") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_CourtDate" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtName" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ShortName") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_CourtID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtNum" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.CourtNumbermain") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_ClientCount" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ClientCount") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_Status" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ShortDescription") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_ViolationID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CategoryID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbtn_Attorney" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FirmAbbreviation") %>'></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    <asp:Label ID="lbl_Message2" runat="server" CssClass="clslabel" Font-Bold="True"></asp:Label></td>
                                <td align="center" valign="top" width="20%">
                                    <asp:GridView ID="gv_13" runat="server" CssClass="clsleftpaddingtable" AutoGenerateColumns="False"
                                        OnRowCommand="gv_11_RowCommand" OnRowDataBound="gv_11_RowDataBound" Width="100%" >
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRowSeperator" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderStyle BorderStyle="None" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtDate" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain","{0:t}") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_CourtDate" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtName" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ShortName") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_CourtID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtNum" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.CourtNumbermain") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_ClientCount" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ClientCount") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_Status" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ShortDescription") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_ViolationID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CategoryID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbtn_Attorney" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FirmAbbreviation") %>'></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    <asp:Label ID="lbl_Message3" runat="server" CssClass="clslabel" Font-Bold="True"></asp:Label></td>
                                <td align="center" valign="top" width="20%">
                                    <asp:GridView ID="gv_14" runat="server" CssClass="clsleftpaddingtable" AutoGenerateColumns="False"
                                        OnRowCommand="gv_11_RowCommand" OnRowDataBound="gv_11_RowDataBound" Width="100%">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRowSeperator" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderStyle BorderStyle="None" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtDate" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain","{0:t}") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_CourtDate" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtName" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ShortName") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_CourtID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtNum" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.CourtNumbermain") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_ClientCount" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ClientCount") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_Status" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ShortDescription") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_ViolationID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CategoryID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbtn_Attorney" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FirmAbbreviation") %>'></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    <asp:Label ID="lbl_Message4" runat="server" CssClass="clslabel" Font-Bold="True"></asp:Label></td>
                                <td align="center" valign="top" width="20%">
                                    <asp:GridView ID="gv_15" runat="server" CssClass="clsleftpaddingtable" AutoGenerateColumns="False"
                                        OnRowCommand="gv_11_RowCommand" OnRowDataBound="gv_11_RowDataBound" Width="100%">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRowSeperator" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderStyle BorderStyle="None" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtDate" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain","{0:t}") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_CourtDate" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtName" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ShortName") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_CourtID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtNum" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.CourtNumbermain") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_ClientCount" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ClientCount") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_Status" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ShortDescription") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_ViolationID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CategoryID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbtn_Attorney" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FirmAbbreviation") %>'></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    <asp:Label ID="lbl_Message5" runat="server" CssClass="clslabel" Font-Bold="True"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="center" valign="top" width="20%" style="height: 2px">
                                    <asp:GridView ID="gv_21" runat="server" CssClass="clsleftpaddingtable" AutoGenerateColumns="False"
                                        OnRowCommand="gv_11_RowCommand" OnRowDataBound="gv_11_RowDataBound" Width="100%" >
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRowSeperator" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderStyle BorderStyle="None" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtDate" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain","{0:t}") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_CourtDate" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtName" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ShortName") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_CourtID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtNum" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.CourtNumbermain") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_ClientCount" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ClientCount") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_Status" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ShortDescription") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_ViolationID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CategoryID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbtn_Attorney" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FirmAbbreviation") %>'></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    <asp:Label ID="lbl_Message6" runat="server" CssClass="clslabel" Font-Bold="True"></asp:Label></td>
                                <td align="center" valign="top" width="20%" style="height: 2px">
                                    <asp:GridView ID="gv_22" runat="server" CssClass="clsleftpaddingtable" AutoGenerateColumns="False"
                                        OnRowCommand="gv_11_RowCommand" OnRowDataBound="gv_11_RowDataBound" Width="100%" >
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRowSeperator" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderStyle BorderStyle="None" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtDate" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain","{0:t}") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_CourtDate" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtName" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ShortName") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_CourtID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtNum" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.CourtNumbermain") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_ClientCount" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ClientCount") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_Status" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ShortDescription") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_ViolationID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CategoryID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbtn_Attorney" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FirmAbbreviation") %>'></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    <asp:Label ID="lbl_Message7" runat="server" CssClass="clslabel" Font-Bold="True"></asp:Label></td>
                                <td align="center" valign="top" width="20%" style="height: 2px">
                                    <asp:GridView ID="gv_23" runat="server" CssClass="clsleftpaddingtable" AutoGenerateColumns="False"
                                        OnRowCommand="gv_11_RowCommand" OnRowDataBound="gv_11_RowDataBound" Width="100%" >
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRowSeperator" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderStyle BorderStyle="None" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtDate" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain","{0:t}") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_CourtDate" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtName" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ShortName") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_CourtID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtNum" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.CourtNumbermain") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_ClientCount" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ClientCount") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_Status" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ShortDescription") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_ViolationID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CategoryID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbtn_Attorney" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FirmAbbreviation") %>'></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    <asp:Label ID="lbl_Message8" runat="server" CssClass="clslabel" Font-Bold="True"></asp:Label></td>
                                <td align="center" valign="top" width="20%" style="height: 2px">
                                    <asp:GridView ID="gv_24" runat="server" CssClass="clsleftpaddingtable" AutoGenerateColumns="False"
                                        Width="100%" OnRowCommand="gv_11_RowCommand" OnRowDataBound="gv_11_RowDataBound">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRowSeperator" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderStyle BorderStyle="None" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtDate" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain","{0:t}") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_CourtDate" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtName" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ShortName") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_CourtID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtNum" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.CourtNumbermain") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_ClientCount" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ClientCount") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_Status" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ShortDescription") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_ViolationID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CategoryID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbtn_Attorney" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FirmAbbreviation") %>'></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    <asp:Label ID="lbl_Message9" runat="server" CssClass="clslabel" Font-Bold="True"></asp:Label></td>
                                <td align="center" valign="top" width="20%" style="height: 2px">
                                    <asp:GridView ID="gv_25" runat="server" CssClass="clsleftpaddingtable" AutoGenerateColumns="False"
                                        Width="100%" OnRowCommand="gv_11_RowCommand" OnRowDataBound="gv_11_RowDataBound">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRowSeperator" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderStyle BorderStyle="None" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtDate" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain","{0:t}") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_CourtDate" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtName" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ShortName") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_CourtID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtNum" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.CourtNumbermain") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_ClientCount" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ClientCount") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_Status" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ShortDescription") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_ViolationID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CategoryID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbtn_Attorney" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FirmAbbreviation") %>'></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    <asp:Label ID="lbl_Message10" runat="server" CssClass="clslabel" Font-Bold="True"></asp:Label></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td background="../Images/separator_repeat.gif" style="height: 6px">
                    </td>
                </tr>
                <tr>
                    <td>
                        <uc2:Footer ID="Footer" runat="server" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>

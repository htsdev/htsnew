<%@ Page Language="C#" AutoEventWireup="true" Codebehind="BondFlagDiscrepancy.aspx.cs" Inherits="lntechNew.Reports.BondFlagDiscrepancy" %>

<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Bond Flag Discrepancy Report</title>
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="../Styles.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table cellspacing="0" cellpadding="0" width="780" align="center" border="0">
                <tr>
                    <td>
                        <uc1:ActiveMenu ID="ActiveMenu1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td width="100%" background="../Images/separator_repeat.gif" height="11">
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td background="../Images/subhead_bg.gif" height="34" class="clssubhead" align="right">
                        <asp:Label ID="Label2" runat="server" CssClass="clsLabel" Text="Current Page : "></asp:Label>
                        <asp:Label ID="LB_curr" runat="server" CssClass="clsLabel" Width="41px"></asp:Label>
                        <asp:Label ID="totlb" runat="server" CssClass="clsLabel" Text="Go To :     "></asp:Label>
                        <asp:DropDownList ID="DL_pages" runat="server" OnSelectedIndexChanged="DL_pages_SelectedIndexChanged"
                            Width="61px" CssClass="clsInputCombo" AutoPostBack="True">
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td align="center" style="height: 13px">
                        <asp:Label ID="lblMessage" runat="server" CssClass="clslabel" ForeColor="Red"></asp:Label></td>
                </tr>
                <tr>
                    <td>
                        <table id="TableGrid" bgcolor="white" border="0" cellpadding="0" cellspacing="0"
                            width="100%">
                            <tr>
                                <td background="../../images/separator_repeat.gif" colspan="5" height="11" style="height: 11px"
                                    width="780">
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2" valign="top">
                                    <asp:GridView ID="gv_Records" runat="server" AutoGenerateColumns="False" Width="100%"
                                        CssClass="clsleftpaddingtable"
                                        AllowPaging="True" OnPageIndexChanging="gv_Records_PageIndexChanging"
                                        PageSize="30">
                                        <Columns>
                                            <asp:TemplateField HeaderText="SNo">
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="hlnk_SNo" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                        Text='<%# DataBinder.Eval(Container, "DataItem.SNo") %>'></asp:HyperLink>
                                                </ItemTemplate>
                                                <HeaderStyle ForeColor="Black" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="CaseNo" Visible="False">
                                                <ItemTemplate>
                                                    <%# DataBinder.Eval(Container, "DataItem.ticketid_pk")%>
                                                </ItemTemplate>
                                                <HeaderStyle ForeColor="Black" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Cause Number">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_Causeno" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.causenumber") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle ForeColor="Black" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Last Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_LastName" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.lastname") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle ForeColor="Black" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="First Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_FirstName" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.firstname") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle ForeColor="Black" />
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="Status">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_status" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.status") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle ForeColor="Black" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Court Info">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_cloc" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.courtinfo") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle ForeColor="Black" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Bond Flag">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_bondflag" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.bondflag") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle ForeColor="Black" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Bond Documents">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_Cno" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.bonddocuments") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle ForeColor="Black" />
                                            </asp:TemplateField>                                            
                                        </Columns>
                                        <PagerStyle HorizontalAlign="Center" />
                                        <PagerSettings Mode="NextPrevious" NextPageText="Next &gt;" PreviousPageText="&lt; Previous" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="display: none;">
                        <asp:TextBox ID="txt_totalrecords" runat="server" CssClass="lable" ForeColor="Black"></asp:TextBox></td>
                    <td>
                        <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>

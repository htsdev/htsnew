using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using lntechNew.Components.ClientInfo;
using System.Threading;

namespace HTP.Reports
{
    public partial class ReadComments : System.Web.UI.Page
    {
        /// <summary>
        /// Read Note Comments Popup (Fahad- 12/24/2007)
        /// </summary>
        clsReadNotes notes = new clsReadNotes();
        protected void Page_Load(object sender, EventArgs e)
        {
            ViewState["ticketid"] = Convert.ToInt32(Request.QueryString["ticketid"]);
            ViewState["empid"] = Convert.ToInt32(Request.QueryString["empid"]);
        }
        /// <summary>
        /// Update Read Note Comments (Fahad- 12/24/2007)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                //Commented By Ozair
                //HttpContext.Current.Response.Write("<script language = 'javascript'>  opener.location.reload(); self.close();    </script>");
                //Added by Ozair for task 2196 at 01/01/2008
                //Thread Applied By Fahad For Minimize Response Time 

                // Noufil 7040 12/01/2009 Code comment. No need to keep tread (as per discussion with CTO)
                //ThreadStart st = new ThreadStart(DoWork);
                //Thread readnotes = new Thread(st);
                //readnotes.IsBackground = true;
                //readnotes.Start();
                //if (readnotes.ThreadState == ThreadState.Stopped)
                //{
                //  readnotes.Suspend();
                // Noufil 7040 12/01/2009 Method called directly
                DoWork();
                HttpContext.Current.Response.Write("<script language = 'javascript'>  opener.focus(); opener.location.href = opener.location; self.close();    </script>");
                //}
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
            }
        }

        public void DoWork()
        {
            notes.InsertFlag(Convert.ToInt32(ViewState["ticketid"]), Convert.ToInt32(ViewState["empid"]));
            if (txtreadcomments.Text.Length > 0)
            {
                notes.UpdateReadComments(Convert.ToInt32(ViewState["ticketid"]), Convert.ToInt32(ViewState["empid"]), txtreadcomments.Text.ToString());
            }
        }
    }
}

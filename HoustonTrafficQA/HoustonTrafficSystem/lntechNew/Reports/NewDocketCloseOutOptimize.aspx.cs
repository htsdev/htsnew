using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using System.Messaging;
using HTP.Components;


namespace HTP.Reports
{
    public partial class testpopupoptimize : System.Web.UI.Page
    {
        //This datatable will store records from the snapshot table
        private DataSet dsRecords;
        //This datatable will be used to get scanned dockets
        private DataTable dtDockets;
        //Database object for TrafficTickets Database
        private clsENationWebComponents clsDb = new clsENationWebComponents("Connection String");
        //Database object for ReturnDockets Database
        clsENationWebComponents clsDB_RD = new clsENationWebComponents("Connection String");

        protected GroupingHelper m_oGroupingHelper = null;
        clsSession ClsSession = new clsSession();

        clsLogger bugTracker = new clsLogger();

        protected void Page_Load(object sender, EventArgs e)
        {
            lbl_Message.Text = "";
            if (!IsPostBack)
            {
                ViewState["vEmpID"] = ClsSession.GetCookie("sEmpID", this.Request).ToString();
            }
        }


        protected void btn_Submit_Click(object sender, EventArgs e)
        {
            try
            {
                GetRecords("", "", 0);

                if (dsRecords.Tables[0].Rows.Count > 0)
                {

                    FillDockets();

                    fillSortingLists(dsRecords);
                    tbl_sort.Visible = true;
                    btn_updatestatus.Visible = true;

                    fillgrid();

                    td_Title.Visible = true;
                    lbl_Title.Text = cal_Date.SelectedDate.ToShortDateString() + " Docket";
                    td_sep.Visible = true;
                    td_Options.Style[HtmlTextWriterStyle.Display] = "block";
                }
                else
                {
                    tbl_sort.Visible = false;
                    btn_updatestatus.Visible = false;
                    lbl_Message.Text = "No records found!";
                    td_HMC.Visible = false;
                    td_Title.Visible = false;
                    btn_updatestatus.Visible = false;
                    gv_records.Visible = false;
                    gv_records1.Visible = false;
                    td_sep.Visible = false;
                    //td_Options.Visible = false;
                    td_Options.Style[HtmlTextWriterStyle.Display] = "none";
                    //btn_DisposeSelected.Visible = false;
                }

                //bindGrid();

            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        //Gets all the records of the supplied date from the snapshot table
        private void GetRecords(string courtname, string status, int showclosedDocket)
        {
            try
            {
                if (!chk_RedX.Checked && !chk_QuestionMark.Checked && !chk_YellowCheck.Checked && !chk_GreenCheck.Checked)
                {
                    dsRecords = new DataSet();
                    dsRecords.Tables.Add();
                }
                else
                {
                    clsDocketCloseOut DocketCloseOutReport = new clsDocketCloseOut();
                    dsRecords = DocketCloseOutReport.GetDocketCloseOutReport(cal_Date.SelectedDate, courtname, status, showclosedDocket, chk_RedX.Checked, chk_QuestionMark.Checked, chk_YellowCheck.Checked, chk_GreenCheck.Checked);
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }



        //The following method fills the datatable for scanned dockets pop-up
        private void FillDockets()
        {
            try
            {
                string[] keys = { "@stDate", "@endDate", "@StrAtoreny" };
                object[] values = { /*Convert.ToDateTime("12/16/2004") */cal_Date.SelectedDate, /*Convert.ToDateTime("12/16/2004")*/cal_Date.SelectedDate, "%" };
                dtDockets = clsDB_RD.Get_DT_BySPArr("USP_HTS_List_Docket", keys, values);
                gv_ScannedDockets.DataSource = dtDockets;
                gv_ScannedDockets.DataBind();
                ddl_dockets.Items.Clear();
                if (dtDockets.Rows.Count == 0)
                    lbl_Popup_Message.Text = "No dockets uploaded on this date";
                else
                {
                    lbl_Popup_Message.Text = "";
                    ddl_dockets.DataSource = dtDockets;
                    ddl_dockets.DataTextField = "IMPORTANCE";
                    ddl_dockets.DataValueField = "docid";

                }

                ddl_dockets.Items.Insert(0, new ListItem("<Scanned Dockets>", "-1"));
                ddl_dockets.Attributes.Add("onchange", "return PopUpCallBack(this.value);");

            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        private void DeleteStatus(string statusID)
        {
            try
            {
                string[] keys = { "@UploadDate", "@TicketsViolationID", "@EmpID" };
                object[] values = { cal_Date.SelectedDate, statusID, ClsSession.GetCookie("sEmpID", this.Request).ToString() };
                clsDb.ExecuteSP("USP_HTS_DOCKETCLOSEOUT_DELETE_STATUS", keys, values);
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }
        // Dispose All Cases
        protected void btn_Dispose_Click(object sender, EventArgs e)
        {
            try
            {
                if (chk_DisposeAll.Checked)
                {
                    string[] keys = { "@CourtDate", "@EmpID" };
                    object[] values = { cal_Date.SelectedDate, ClsSession.GetCookie("sEmpID", this.Request).ToString() };
                    clsDb.ExecuteSP("USP_HTS_DOCKETCLOSEOUT_DISPOSE_HMC_CASES", keys, values);

                }
                btn_Submit_Click(null, null);
                chk_DisposeAll.Checked = false;
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        // Scanned Docket Row Data Bound
        protected void gv_ScannedDockets_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                //Find the document ID and attach the JavaScript accordingly to open the scanned dockets
                Label docid = (Label)e.Row.FindControl("lbl_docid");
                if (docid != null)
                {
                    LinkButton lbtn = (LinkButton)e.Row.FindControl("lbtn_DocketDate");
                    lbtn.Attributes.Add("onClick", "return PopUpCallBack(" + docid.Text + "  );");
                }


                //Label astatus = (Label)e.Row.FindControl("lbl_astatus"); 
                //Label vstatus = (Label)e.Row.FindControl("lbl_vstatus");
                //Image img = (Image)e.Row.FindControl("img_dis");

                //if (astatus.Text.Trim() == vstatus.Text.Trim())
                //{
                //    img.ImageUrl = "../Images/right.gif";
                //}
                //else
                //{
                //    img.ImageUrl = "../Images/cross.gif";
                //}

            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        // Update Case History
        private void UpdateCaseHistory(int ticketno, string sub, string notes, int empid)
        {
            try
            {
                string[] keys = { "@ticketid", "@subject", "@notes", "@employeeid" };
                object[] values = { ticketno, sub, DBNull.Value, empid };
                clsDb.InsertBySPArr("SP_ADD_NOTES", keys, values);
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        // Fill Sorting Drop Down Lists
        private void fillSortingLists(DataSet ds_records)
        {

            ddl_courts.Items.Clear();
            ddl_courts.Items.Add(new ListItem("ALL", ""));

            object[] rows = GetDistinctValues(ds_records.Tables[0], "ShortName");

            foreach (object OBJ in rows)
                ddl_courts.Items.Add((string)OBJ);

        }
        // Get Distinct Court Names
        public object[] GetDistinctValues(DataTable dtable, string colName)
        {
            Hashtable hTable = new Hashtable();
            foreach (DataRow drow in dtable.Rows)
            {
                try
                {
                    hTable.Add(drow[colName], string.Empty);
                }
                catch { }
            }
            object[] objArray = new object[hTable.Keys.Count];
            hTable.Keys.CopyTo(objArray, 0);
            return objArray;
        }
        // Update Auto Status with Varified Status
        protected void btn_updatestatus_Click(object sender, EventArgs e)
        {
            try
            {
                int empid = Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request).ToString());

                // Noufil 8268 09/17/2010 EmpId added
                string[] keys = { "@TicketsViolationID", "@empId" };
                object[] values = { "", empid };

                foreach (GridViewRow itm in gv_records.Rows)
                {

                    CheckBox chk = (CheckBox)itm.FindControl("cb_view");

                    if (chk != null)
                    {
                        if (chk.Checked)
                        {
                            values[0] = ((HiddenField)itm.FindControl("hf_TicketsViolationID")).Value;
                            clsDb.ExecuteSP("USP_HTS_DOCKETCLOSEOUT_UPDATE_Verified_With_Auto_STATUS", keys, values);

                            //Added by ozair for trial letter generation

                            MessageQueue MyMessageQ;
                            Message MyMessage;

                            DataTable dt = new DataTable();
                            dt.Columns.Add("tvid");
                            dt.Columns.Add("empid");

                            DataRow dr = dt.NewRow();
                            dr["tvid"] = ((HiddenField)itm.FindControl("hf_TicketsViolationID")).Value;
                            dr["empid"] = empid;
                            dt.Rows.Add(dr);

                            dt.TableName = "TVIDS";
                            try
                            {
                                MyMessageQ = new MessageQueue(ConfigurationSettings.AppSettings["QueuePath"].ToString());
                                MyMessage = new Message(dt);
                                MyMessageQ.Send(MyMessage);
                            }

                            catch (Exception ex)
                            {
                                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                            }
                            //
                        }
                    }

                }

                foreach (GridViewRow itm in gv_records1.Rows)
                {
                    CheckBox chk = (CheckBox)itm.FindControl("cb_view");

                    if (chk != null)
                    {
                        if (chk.Checked)
                        {
                            values[0] = ((HiddenField)itm.FindControl("hf_TicketsViolationID")).Value;
                            clsDb.ExecuteSP("USP_HTS_DOCKETCLOSEOUT_UPDATE_Verified_With_Auto_STATUS", keys, values);
                        }
                    }

                }
                GetRecords(ddl_courts.SelectedValue, ddl_status.SelectedIndex.ToString(), 0);
                fillgrid();

            }
            catch { }


        }
        // Dispose Selected Records
        protected void btn_DisposeSelected_Click(object sender, EventArgs e)
        {
            try
            {

                string[] keys = { "@TicketsViolationID", "@EmpID" };
                object[] values = { "", "" };



                foreach (GridViewRow itm in gv_records.Rows)
                {

                    CheckBox chk = (CheckBox)itm.FindControl("cb_view");

                    if (chk != null)
                    {
                        if (chk.Checked)
                        {
                            values[0] = ((HiddenField)itm.FindControl("hf_TicketsViolationID")).Value;
                            values[1] = Convert.ToInt32(ViewState["vEmpID"]);
                            clsDb.ExecuteSP("USP_HTS_DOCKETCLOSEOUT_UPDATE_Dispose_Selected", keys, values);
                        }
                    }

                }

                foreach (GridViewRow itm in gv_records1.Rows)
                {
                    CheckBox chk = (CheckBox)itm.FindControl("cb_view");

                    if (chk != null)
                    {
                        if (chk.Checked)
                        {
                            values[0] = ((HiddenField)itm.FindControl("hf_TicketsViolationID")).Value;
                            values[1] = Convert.ToInt32(ViewState["vEmpID"]);
                            clsDb.ExecuteSP("USP_HTS_DOCKETCLOSEOUT_UPDATE_Dispose_Selected", keys, values);
                        }
                    }

                }
                GetRecords(ddl_courts.SelectedValue, ddl_status.SelectedIndex.ToString(), 0);
                fillgrid();
            }
            catch { }
        }
        // Display Filtered Records
        protected void ddl_status_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetRecords(ddl_courts.SelectedValue, ddl_status.SelectedIndex.ToString(), 0);
            fillgrid();
        }
        // Show all Records
        protected void lnkbtn_showall_Click(object sender, EventArgs e)
        {
            GetRecords("", "", 1);
            fillgrid();
            if (dsRecords.Tables[0].Rows.Count > 0)
                td_Options.Style[HtmlTextWriterStyle.Display] = "block";
            else
                td_Options.Style[HtmlTextWriterStyle.Display] = "none";
        }
        // Grid Row Databound Event
        protected void gv_records_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {

                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    HiddenField rowtype = (HiddenField)e.Row.FindControl("hf_rowtype");

                    if (rowtype != null)
                    {
                        if (rowtype.Value == "1")
                        {
                            try
                            {
                                Label lbl1 = (Label)e.Row.FindControl("lbl_sno");
                                Label astatus = (Label)e.Row.FindControl("lbl_astatus");
                                Label vstatus = (Label)e.Row.FindControl("lbl_vstatus");
                                Image img = (Image)e.Row.FindControl("img_dis");

                                string discrepency = ((HiddenField)e.Row.FindControl("hf_discrepency")).Value;

                                switch (discrepency)
                                {
                                    case "0": img.ImageUrl = "../Images/right.gif"; break;
                                    case "1": img.ImageUrl = "../Images/cross.gif"; break;
                                    case "2": img.ImageUrl = "../Images/yellow_check.gif"; break;
                                    case "3": img.ImageUrl = "../Images/questionmark.gif"; break;
                                    default: img.ImageUrl = "../Images/cross.gif"; break;
                                }

                                //if (((HiddenField)e.Row.FindControl("hf_showquestionmark")).Value == "True")
                                //    img.ImageUrl = "../Images/questionmark.gif";

                                string status = astatus.Text.Substring(0, 4);
                                string courtdate = astatus.Text.Substring(4, 10);

                                if (status[3] != ' ')
                                    courtdate = astatus.Text.Substring(5, 10);

                                DateTime cdate = DateTime.Parse(courtdate);

                                if (cdate > DateTime.Now && (status.Trim() == "ARR" || status.Trim() == "JUR" || status.Trim() == "PRE" || status.Trim() == "JUD" || status.Trim() == "WAIT"))
                                {
                                    e.Row.Cells[7].Style.Add("background-color", "Yellow");
                                }
                            }
                            catch { }

                            ImageButton ibtn = (ImageButton)e.Row.FindControl("ImageButton1");

                            if (ibtn != null)
                            {
                                if (ibtn.Visible == true)
                                {
                                    HiddenField hf_date = (HiddenField)e.Row.FindControl("hf_CourtDate");

                                    if (hf_date.Value != "")
                                    {
                                        DateTime dt = Convert.ToDateTime(hf_date.Value);

                                        HiddenField hf_MM = (HiddenField)e.Row.FindControl("hf_MM");
                                        hf_MM.Value = dt.Month.ToString();

                                        HiddenField hf_DD = (HiddenField)e.Row.FindControl("hf_DD");
                                        hf_DD.Value = dt.Day.ToString();

                                        HiddenField hf_YY = (HiddenField)e.Row.FindControl("hf_YY");
                                        hf_YY.Value = dt.Year.ToString().Substring(2, 2);


                                        Label lbl_CourtNumber = (Label)e.Row.FindControl("lbl_CourtNumber");
                                        Label lbl_CourtTime = (Label)e.Row.FindControl("lbl_CourtTime");

                                        HiddenField hf_CourtID = (HiddenField)e.Row.FindControl("hf_CourtID");
                                        HiddenField hf_TicketsViolationID = (HiddenField)e.Row.FindControl("hf_TicketsViolationID");
                                        HiddenField hf_BondFlag = (HiddenField)e.Row.FindControl("hf_BondFlag");
                                        HiddenField hf_TicketID = (HiddenField)e.Row.FindControl("hf_TicketID");
                                        Label lbl_snapshotstatus = (Label)e.Row.FindControl("lbl_sstatus");


                                        //Zeeshan Ahmed 3486 03/31/2008
                                        //Display Bond Flag
                                        if (hf_BondFlag.Value == "1")
                                            ((Label)e.Row.FindControl("lblBondFlag")).Text = "B";
                                    }
                                }

                            }
                            Image ibtn2 = (ImageButton)e.Row.FindControl("ImageButton2");
                            Label lbl = (Label)e.Row.FindControl("lbl_LastUpdate");
                            if (ibtn2 != null)
                            {
                                if (lbl != null)
                                {
                                    if (lbl.Text.Contains("/"))
                                    {
                                        ibtn2.Visible = true;
                                        ibtn.Visible = false;
                                    }
                                    else
                                    {
                                        ibtn2.Visible = false;
                                        ibtn.Visible = true;
                                    }
                                }

                            }
                        }

                        else if (rowtype.Value == "2")
                        {

                            for (int i = 1; i < e.Row.Cells.Count; i++)
                                e.Row.Cells[i].Visible = false;

                            e.Row.Cells[0].ColumnSpan = 10;
                            e.Row.Cells[0].Style.Add("height", "20px");
                            e.Row.Cells[0].BackColor = System.Drawing.Color.Gray;

                        }

                        else if (rowtype.Value == "3")
                        {
                            for (int i = 1; i < e.Row.Cells.Count; i++)
                                e.Row.Cells[i].Visible = false;


                            e.Row.Cells[0].Style.Add("height", "20px");
                            e.Row.Cells[0].ColumnSpan = 10;

                            Label lbl = (Label)e.Row.FindControl("lbl_sno");

                            if (lbl.Text != "")
                            {
                                lbl.ForeColor = System.Drawing.Color.White;
                                e.Row.Cells[0].BackColor = System.Drawing.Color.Black;
                                e.Row.Cells[0].Font.Bold = true;
                                lbl.Visible = true;
                                ((HyperLink)e.Row.FindControl("hl_sno")).Visible = false; ;

                            }
                            if (lbl.Text == " , ")
                                e.Row.Visible = false;

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }


        }
        // Format Docket Report Records
        private void formatDataSet(DataSet ds_records, int tableno)
        {
            int sno = 1;

            DataColumn dc = new DataColumn("RowType");
            dc.DefaultValue = 1;

            ds_records.Tables[tableno].Columns.Add("SNO");
            ds_records.Tables[tableno].Columns.Add(dc);
            ds_records.Tables[tableno].Rows[0]["SNO"] = sno;
            DataRow dr1 = ds_records.Tables[tableno].NewRow();
            dr1["SNO"] = ds_records.Tables[tableno].Rows[0]["courtnamecomplete"].ToString();
            dr1["RowType"] = 3;
            ds_records.Tables[tableno].Rows.InsertAt(dr1, 0);
            sno++;

            for (int i = 1; i < ds_records.Tables[tableno].Rows.Count; i++)
            {

                if (ds_records.Tables[tableno].Rows[i - 1]["ticketid_pk"] != null)
                {
                    try
                    {

                        if (Convert.ToInt32(ds_records.Tables[tableno].Rows[i]["ticketid_pk"]) != Convert.ToInt32(ds_records.Tables[tableno].Rows[i - 1]["ticketid_pk"]))
                        {
                            ds_records.Tables[tableno].Rows[i]["SNO"] = sno;
                            sno++;
                        }

                        if (ds_records.Tables[tableno].Rows[i]["courtnamecomplete"].ToString() != ds_records.Tables[tableno].Rows[i - 1]["courtnamecomplete"].ToString())
                        {
                            DataRow dr = ds_records.Tables[tableno].NewRow();
                            dr["RowType"] = 3;
                            dr["SNO"] = ds_records.Tables[tableno].Rows[i]["courtnamecomplete"].ToString();
                            ds_records.Tables[tableno].Rows.InsertAt(dr, i);

                        }
                        else if (Convert.ToInt32(ds_records.Tables[tableno].Rows[i]["courtnumber"]) != Convert.ToInt32(ds_records.Tables[tableno].Rows[i - 1]["courtnumber"]))
                        {
                            DataRow dr = ds_records.Tables[tableno].NewRow();
                            dr["RowType"] = 2;
                            dr["SNO"] = "\n";
                            ds_records.Tables[tableno].Rows.InsertAt(dr, i);

                        }
                    }
                    catch { }
                }

            }

        }
        // Row Command Event
        protected void gv_records_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                //This code will get executed once the user clicks 'x' button on the repeater control
                //and status will be deleted
                if (e.CommandName == "Delete")
                {
                    DeleteStatus(e.CommandArgument.ToString());
                    btn_Submit_Click(null, null);

                }

            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        // Row Deleting Event
        protected void gv_records_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }
        // Paging Event
        protected void gv_records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gv_records.PageIndex = e.NewPageIndex;
            btn_Submit_Click(null, null);
        }
        // Display Docket Close Out Records
        private void fillgrid()
        {

            if (dsRecords.Tables[0].Rows.Count > 0)
            {
                gv_records.AllowPaging = !cb_showall.Checked;
                formatDataSet(dsRecords, 0);
                hf_Grid1_Rows.Value = dsRecords.Tables[0].Rows.Count.ToString();
                gv_records.DataSource = dsRecords.Tables[0];
                gv_records.DataBind();


                if (dsRecords.Tables[1].Rows.Count > 0)
                {
                    formatDataSet(dsRecords, 1);
                    hf_Grid2_Rows.Value = dsRecords.Tables[1].Rows.Count.ToString();
                    gv_records1.DataSource = dsRecords.Tables[1];
                    gv_records1.DataBind();
                    gv_records1.Visible = true;

                }
                else
                {
                    gv_records1.Visible = false;
                }

                gv_records.Visible = true;
                btn_updatestatus.Visible = true;
                btn_DisposeSelected.Visible = true;

            }
            else
            {
                lbl_Message.Text = "No records found!";
                btn_DisposeSelected.Visible = false;
                gv_records.Visible = false;
                gv_records1.Visible = false;
                btn_updatestatus.Visible = false;

            }

        }

        private void UpdateSelectedToDispose()
        {
            string[] keys = { "@TicketsViolationID", "@EmpID" };
            object[] values = { "", "" };

            //Zeeshan Haider 11008 06/18/2013 Do not allow case to be disposed if auto has a future court date (Judge/Jury/Pre Trial)
            bool alertMsg = false;

            foreach (GridViewRow itm in gv_records.Rows)
            {

                CheckBox chk = (CheckBox)itm.FindControl("cb_view");

                if (chk != null)
                {
                    if (chk.Checked)
                    {
                        //Zeeshan Haider 11008 06/18/2013 Do not allow case to be disposed if auto has a future court date (Judge/Jury/Pre Trial)
                        bool ValidToBeDisposed = true;
                        DateTime autoCourtDate = Convert.ToDateTime(((HiddenField)itm.FindControl("hf_AutoCourtDate")).Value);
                        if (autoCourtDate.Date > DateTime.Now.Date)
                        {
                            string autoStatusID = ((HiddenField)itm.FindControl("hf_AutoStatusID")).Value;
                            if (autoStatusID == "26" || autoStatusID == "101" || autoStatusID == "103")
                            {
                                ValidToBeDisposed = false;
                                alertMsg = true;
                            }
                        }
                        if (ValidToBeDisposed)
                        {
                            values[0] = ((HiddenField)itm.FindControl("hf_TicketsViolationID")).Value;
                            values[1] = Convert.ToInt32(ViewState["vEmpID"]);
                            clsDb.ExecuteSP("USP_HTS_DOCKETCLOSEOUT_UPDATE_Dispose_Selected", keys, values);
                        }
                    }
                }

            }

            foreach (GridViewRow itm in gv_records1.Rows)
            {
                CheckBox chk = (CheckBox)itm.FindControl("cb_view");

                if (chk != null)
                {
                    if (chk.Checked)
                    {
                        //Zeeshan Haider 11008 06/18/2013 Do not allow case to be disposed if auto has a future court date (Judge/Jury/Pre Trial)
                        bool ValidToBeDisposed = true;
                        DateTime autoCourtDate = Convert.ToDateTime(((HiddenField)itm.FindControl("hf_AutoCourtDate")).Value);
                        if (autoCourtDate.Date > DateTime.Now.Date)
                        {
                            string autoStatusID = ((HiddenField)itm.FindControl("hf_AutoStatusID")).Value;
                            if (autoStatusID == "26" || autoStatusID == "101" || autoStatusID == "103")
                            {
                                ValidToBeDisposed = false;
                                alertMsg = true;
                            }
                        }
                        if (ValidToBeDisposed)
                        {
                            values[0] = ((HiddenField)itm.FindControl("hf_TicketsViolationID")).Value;
                            values[1] = Convert.ToInt32(ViewState["vEmpID"]);
                            clsDb.ExecuteSP("USP_HTS_DOCKETCLOSEOUT_UPDATE_Dispose_Selected", keys, values);
                        }
                    }
                }

            }
            //Zeeshan Haider 11008 06/18/2013 Do not allow case to be disposed if auto has a future court date (Judge/Jury/Pre Trial)
            if (alertMsg)
            {
                string msg =
                    "Not all cases were able to be disposed as some cases had future auto court date. Please dispose those cases from the client's 'Case Disposition page'.";
                msg = @"alert(""" + msg;
                msg = msg + @""")";
                ScriptManager.RegisterStartupScript(this, GetType(), "Alert", msg, true);
            }
            GetRecords(ddl_courts.SelectedValue, ddl_status.SelectedIndex.ToString(), 0);
            fillgrid();
        }

        private void UpdateSelectedToMissedCourt(BatchLetterType batchLetterType)
        {

            Hashtable TicketIds = new Hashtable();
            clsDocketCloseOut DocketCloseOutRpt = new clsDocketCloseOut();

            //Set Rep ID
            DocketCloseOutRpt.EmpId = Convert.ToInt32(ViewState["vEmpID"]);
            //Zeeshan 4163 06/03/2008 Save Missed Court Type With Violation
            DocketCloseOutRpt.MissedCourtType = batchLetterType;
            ClsFlags flags = new ClsFlags();
            foreach (GridViewRow itm in gv_records.Rows)
            {
                CheckBox chk = (CheckBox)itm.FindControl("cb_view");

                if (chk != null && chk.Checked)
                {
                    DocketCloseOutRpt.TicketViolationId = Convert.ToInt32(((HiddenField)itm.FindControl("hf_TicketsViolationID")).Value);
                    DocketCloseOutRpt.SetMissedCourtStatus();

                    string TicketID = ((HiddenField)itm.FindControl("hf_TicketID")).Value;

                    if (!TicketIds.ContainsKey(TicketID))
                        //Sabir Khan 5442 02/10/2009 To exclude the records whose no letter flag is set...
                        if (!(flags.HasFlag(Convert.ToInt32(FlagType.NoLetter))))
                            TicketIds.Add(TicketID, TicketID);
                }
            }

            foreach (GridViewRow itm in gv_records1.Rows)
            {
                CheckBox chk = (CheckBox)itm.FindControl("cb_view");

                if (chk != null && chk.Checked)
                {
                    DocketCloseOutRpt.TicketViolationId = Convert.ToInt32(((HiddenField)itm.FindControl("hf_TicketsViolationID")).Value);
                    DocketCloseOutRpt.SetMissedCourtStatus();

                    string TicketID = ((HiddenField)itm.FindControl("hf_TicketID")).Value;

                    if (!TicketIds.ContainsKey(TicketID))
                        //Sabir Khan 5442 02/10/2009 To exclude the records whose no letter flag is set...
                        if (!(flags.HasFlag(Convert.ToInt32(FlagType.NoLetter))))
                            TicketIds.Add(TicketID, TicketID);
                }
            }

            //Send Missed Court Letter To Batch
            DocketCloseOutRpt.SendMissedCourtLetterToBatch(TicketIds, batchLetterType);

            //Get Records
            GetRecords(ddl_courts.SelectedValue, ddl_status.SelectedIndex.ToString(), 0);
            fillgrid();
        }

        private void UpdateVerifiedCourtdateWithAuto()
        {
            int empid = Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request).ToString());

            // Noufil 8268 09/17/2010 EmpId added
            string[] keys = { "@TicketsViolationID", "@empId" };
            object[] values = { "", empid };

            foreach (GridViewRow itm in gv_records.Rows)
            {

                CheckBox chk = (CheckBox)itm.FindControl("cb_view");

                if (chk != null)
                {
                    if (chk.Checked)
                    {
                        values[0] = ((HiddenField)itm.FindControl("hf_TicketsViolationID")).Value;
                        clsDb.ExecuteSP("USP_HTS_DOCKETCLOSEOUT_UPDATE_Verified_With_Auto_STATUS", keys, values);

                        //Added by ozair for trial letter generation

                        MessageQueue MyMessageQ;
                        Message MyMessage;

                        DataTable dt = new DataTable();
                        dt.Columns.Add("tvid");
                        dt.Columns.Add("empid");

                        DataRow dr = dt.NewRow();
                        dr["tvid"] = ((HiddenField)itm.FindControl("hf_TicketsViolationID")).Value;
                        dr["empid"] = empid;
                        dt.Rows.Add(dr);

                        dt.TableName = "TVIDS";
                        try
                        {
                            MyMessageQ = new MessageQueue(ConfigurationSettings.AppSettings["QueuePath"].ToString());
                            MyMessage = new Message(dt);
                            MyMessageQ.Send(MyMessage);
                        }


                        catch (Exception ex)
                        {
                            bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                        }
                        //

                        //Zeeshan Haider 11008 06/17/2013 Send trial letter to batch
                        DateTime autoCourtDate = Convert.ToDateTime(((HiddenField)itm.FindControl("hf_AutoCourtDate")).Value);
                        if (autoCourtDate.Date > DateTime.Now.Date)
                        {
                            string autoStatusID = ((HiddenField)itm.FindControl("hf_AutoStatusID")).Value;
                            if (autoStatusID == "26" || autoStatusID == "101" || autoStatusID == "103")
                            {
                                SendTrialLetterToBatch(Convert.ToInt32(((HiddenField)itm.FindControl("hf_TicketID")).Value));
                            }
                        }
                        GetRecords(ddl_courts.SelectedValue, ddl_status.SelectedIndex.ToString(), 0);
                        fillgrid();
                    }
                }
            }

            foreach (GridViewRow itm in gv_records1.Rows)
            {
                CheckBox chk = (CheckBox)itm.FindControl("cb_view");

                if (chk != null)
                {
                    if (chk.Checked)
                    {
                        values[0] = ((HiddenField)itm.FindControl("hf_TicketsViolationID")).Value;
                        clsDb.ExecuteSP("USP_HTS_DOCKETCLOSEOUT_UPDATE_Verified_With_Auto_STATUS", keys, values);

                        //Zeeshan Haider 11008 06/17/2013 Send trial letter to batch
                        DateTime autoCourtDate = Convert.ToDateTime(((HiddenField)itm.FindControl("hf_AutoCourtDate")).Value);
                        if (autoCourtDate.Date > DateTime.Now.Date)
                        {
                            string autoStatusID = ((HiddenField)itm.FindControl("hf_AutoStatusID")).Value;
                            if (autoStatusID == "26" || autoStatusID == "101" || autoStatusID == "103")
                            {
                                SendTrialLetterToBatch(Convert.ToInt32(((HiddenField)itm.FindControl("hf_TicketID")).Value));
                            }
                        }
                    }
                }

            }
            GetRecords(ddl_courts.SelectedValue, ddl_status.SelectedIndex.ToString(), 0);
            fillgrid();
        }

        private void UpdateSeletedToWaiting()
        {
            try
            {
                string TicketViolationIDs = String.Empty;
                foreach (GridViewRow itm in gv_records.Rows)
                {

                    CheckBox chk = (CheckBox)itm.FindControl("cb_view");

                    if (chk != null)
                    {
                        if (chk.Checked)
                        {
                            HiddenField hf = (HiddenField)itm.FindControl("hf_CourtID");
                            if (hf != null)
                                if (hf.Value == "3001" || hf.Value == "3002" || hf.Value == "3003")
                                    continue;
                            TicketViolationIDs += ((HiddenField)itm.FindControl("hf_TicketsViolationID")).Value + ",";
                        }
                    }

                }

                foreach (GridViewRow itm in gv_records1.Rows)
                {
                    CheckBox chk = (CheckBox)itm.FindControl("cb_view");

                    if (chk != null)
                    {
                        if (chk.Checked)
                        {
                            HiddenField hf = (HiddenField)itm.FindControl("hf_CourtID");
                            if (hf != null)
                                if (hf.Value == "3001" || hf.Value == "3002" || hf.Value == "3003")
                                    continue;
                            TicketViolationIDs += ((HiddenField)itm.FindControl("hf_TicketsViolationID")).Value + ",";
                        }
                    }

                }
                string[] keys = { "@TicketViolationIDs", "@EmpID" };
                object[] values = { TicketViolationIDs, ViewState["vEmpID"] };
                clsDb.ExecuteSP("usp_hts_docketcloseout_WaitingStatus", keys, values);
                GetRecords(ddl_courts.SelectedValue, ddl_status.SelectedIndex.ToString(), 0);
                fillgrid();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        //Zeeshan Haider 11008 06/17/2013 Send trial letter to batch
        private void SendTrialLetterToBatch(int TicketID)
        {
            try
            {
                if (TicketID > 0)
                {
                    clsDocketCloseOut clsDocketCloseOut = new clsDocketCloseOut();
                    int empid = Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request));
                    clsDocketCloseOut.RemoveExistingTrialLetterInBatch(TicketID);
                    clsDocketCloseOut.SendTrialLetterToBatch(TicketID, empid);
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void btn_UpdateSelected_Click(object sender, EventArgs e)
        {
            try
            {
                //Zeeshan Ahmed 3486 04/04/2008
                switch (ddl_UpdateSelected.SelectedValue)
                {
                    case "1":
                        UpdateSelectedToDispose();
                        break;
                    case "2":
                        UpdateSelectedToMissedCourt(BatchLetterType.MissedCourtLetter);
                        break;
                    case "3":
                        UpdateSeletedToWaiting();
                        break;
                    case "4":
                        break;
                    case "5":
                        UpdateVerifiedCourtdateWithAuto();
                        break;
                    case "6":
                        UpdateSelectedToMissedCourt(BatchLetterType.PledOutLetter);
                        break;
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void btn_Reset_Update_Click(object sender, EventArgs e)
        {
            try
            {
                string TicketViolationIDs = String.Empty;
                //ozair 4720 09/01/2008
                int empid = Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request).ToString());
                DataTable dt = new DataTable("TVIDS");
                dt.Columns.Add("tvid");
                dt.Columns.Add("empid");
                //end ozair 4720
                foreach (GridViewRow itm in gv_records.Rows)
                {

                    CheckBox chk = (CheckBox)itm.FindControl("cb_view");

                    if (chk != null)
                    {
                        if (chk.Checked)
                        {
                            TicketViolationIDs += ((HiddenField)itm.FindControl("hf_TicketsViolationID")).Value + ",";
                            //ozair 4720 09/01/2008
                            //Added by ozair for trial letter generation
                            DataRow dr = dt.NewRow();
                            dr["tvid"] = ((HiddenField)itm.FindControl("hf_TicketsViolationID")).Value;
                            dr["empid"] = empid;
                            dt.Rows.Add(dr);
                            //end ozair 4720
                        }
                    }

                }

                foreach (GridViewRow itm in gv_records1.Rows)
                {
                    CheckBox chk = (CheckBox)itm.FindControl("cb_view");

                    if (chk != null)
                    {
                        if (chk.Checked)
                        {
                            TicketViolationIDs += ((HiddenField)itm.FindControl("hf_TicketsViolationID")).Value + ",";
                        }
                    }

                }
                string[] keys = { "@TicketViolationIDs", "@EmpID", "@CourtDate", "@CourtLoc" };
                object[] values = { TicketViolationIDs, ViewState["vEmpID"], txt_Reset_MM.Text + "/" + txt_Reset_DD.Text + "/" + txt_Reset_YY.Text + " " + ddl_Reset_Time.SelectedItem.Text, txt_Reset_CourtNum.Text };
                clsDb.ExecuteSP("usp_hts_docketcloseout_reset", keys, values);

                //ozair 4720 09/01/2008 //Added by ozair for trial letter generation
                try
                {
                    if (dt.Rows.Count > 0)
                    {
                        MessageQueue MyMessageQ;
                        Message MyMessage;

                        MyMessageQ = new MessageQueue(ConfigurationSettings.AppSettings["QueuePath"].ToString());
                        MyMessage = new Message(dt);
                        MyMessageQ.Send(MyMessage);
                    }
                }

                catch (Exception ex)
                {
                    bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                }
                //end ozair 4720

                GetRecords(ddl_courts.SelectedValue, ddl_status.SelectedIndex.ToString(), 0);
                fillgrid();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
    }
}

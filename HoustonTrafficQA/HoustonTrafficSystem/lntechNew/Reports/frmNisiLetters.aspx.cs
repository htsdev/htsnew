﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;
using lntechNew;
using FrameWorkEnation.Components;
using HTP.Components;
using System.Text.RegularExpressions;


namespace HTP.Reports
{
    /// <summary>
    /// Summary description for frmBatchReport.
    /// </summary>
    public partial class frmNisiLetters : System.Web.UI.Page
    {

        clsBatch batch = new clsBatch();
        clsCrsytalComponent Cr = new clsCrsytalComponent();
        clsSession ClsSession = new clsSession();
        protected System.Web.UI.WebControls.Label lbl_message;
        clsENationWebComponents ClsDB = new clsENationWebComponents();



        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Request.QueryString["ClientLatter"] != null)
                    {
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.Clear();
                        Response.ContentType = "application/ms-word";
                        Response.AddHeader("Content-disposition", "inline; attachment; filename=NISIClientLatter.doc");
                        Response.WriteFile("D:\\DOCSTORAGE\\NisiPrintAnswersDoc\\" + Request.QueryString["ClientLatter"]);
                        Response.Flush();
                    }
                    else
                    {
                        lntechNew.Components.clsCrsytalComponent cr = new lntechNew.Components.clsCrsytalComponent();
                        string fileSaveDirectory = System.Configuration.ConfigurationManager.AppSettings["NisiDocsPath"];
                        ClsNisiCaseHandler nisi = new ClsNisiCaseHandler();

                        if (!System.IO.Directory.Exists(fileSaveDirectory))
                        {
                            System.IO.Directory.CreateDirectory(fileSaveDirectory);
                        }

                        if (Convert.ToInt32(Session["ContactType"]) == 7)
                        {
                            string reportFileName = Server.MapPath(".") + "\\NisiPrintAnswerClientCopy.rpt";
                            DataSet dsClietns = nisi.GetClientLettersToPrint(Convert.ToInt32(Session["TicketId"]), Convert.ToInt32(Session["EmployeeIdForLetters"].ToString()),
                                Session["NISICauseMulti"].ToString(), Session["UnderlyingCauseMulti"].ToString());
                            string filepath = string.Empty;
                            if (dsClietns.Tables[0].Rows.Count > 0)
                            {
                                filepath = cr.CreateReportWordForNISI(dsClietns, reportFileName, fileSaveDirectory, Convert.ToInt32(Session["TicketId"]));
                                //cr.CreateReportWord(dsClietns, reportFileName, fileSaveDirectory, this.Session, this.Response);
                            }
                            nisi.InsertHistoryNoteForNisiCllientLatter(Convert.ToInt32(Session["TicketId"]), Convert.ToInt32(Session["EmployeeIdForLetters"].ToString()), filepath);
                            Session["ContactType"] = null;
                            Session["NISICauseMulti"] = null;
                            Session["UnderlyingCauseMulti"] = null;
                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.Clear();
                            Response.ContentType = "application/ms-word";
                            Response.AddHeader("Content-disposition", "inline; attachment; filename=" + filepath);
                            Response.WriteFile("D:\\DOCSTORAGE\\NisiPrintAnswersDoc\\" + filepath);
                            Response.Flush();
                            
                            
                            //string reportFileName = Server.MapPath(".") + "\\NisiPrintAnswerClientCopy.rpt";
                            //DataSet dsClietns = nisi.GetClientLettersToPrint(Convert.ToInt32(Session["TicketId"]), Convert.ToInt32(Session["EmployeeIdForLetters"].ToString()),
                            //    Session["NISICauseMulti"].ToString(), Session["UnderlyingCauseMulti"].ToString());
                            //if (dsClietns.Tables[0].Rows.Count > 0)
                            //{
                            //    cr.CreateReportWord(dsClietns, reportFileName, fileSaveDirectory, this.Session, this.Response);
                            //}
                            //Session["ContactType"] = null;
                            //Session["NISICauseMulti"] = null;
                            //Session["UnderlyingCauseMulti"] = null;
                        }
                        else
                        {
                            string clientrowId = Session["printAnswers"].ToString();
                            bool newClients = Convert.ToBoolean(Session["newClients"]);
                            string reportFileName = Server.MapPath(".") + "\\ScireFaciasAnswer.rpt";

                            DataSet ds = (DataSet)Session["PrintAnswerDS"];
                            string filepath = string.Empty;
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                filepath = cr.CreateReportWordForNISI(ds, reportFileName, fileSaveDirectory, Convert.ToInt32(Session["TicketId"]));
                            }
                            Session["PrintAnswerDS"] = null;

                            Session["printAnswers"] = null;
                            Session["newClients"] = null;
                            Session["ContactType"] = null;

                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.Clear();
                            Response.ContentType = "application/ms-word";
                            Response.AddHeader("Content-disposition", "inline; attachment; filename=" + filepath);
                            //Response.Write(fileSaveDirectory + filepath);
                            Response.WriteFile("D:\\DOCSTORAGE\\NisiPrintAnswersDoc\\" + filepath);
                            Response.Flush();
                            Response.End();
                            //if (ds.Tables[0].Rows.Count > 0)
                            //{
                            //    cr.CreateReportWord(ds, reportFileName, fileSaveDirectory, this.Session, this.Response);
                            //}
                            //Session["printAnswers"] = null;
                            //Session["newClients"] = null;
                            //Session["ContactType"] = null;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
            }
        }



        private void DeleteBatch()
        {

        }
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion


    }

}

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AttorneyPayoutReport.aspx.cs"
    Inherits="HTP.Reports.AttorneyPayoutReport" %>

<%@ Register Src="~/WebControls/ActiveMenu.ascx" TagPrefix="uc1" TagName="activemenu" %>
<%@ Register Src="~/WebControls/PagingControl.ascx" TagName="pagingcontrol" TagPrefix="uc3" %>
<%@ Register Src="~/WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Namespace="eWorld.UI" TagPrefix="ew" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Attorney Payout Report</title>
    <link href="../Styles.css" type="text/css" rel="Stylesheet" />

    <script language="JavaScript" type="text/javascript">
    
    function Validate()
    {
        if (document.getElementById("ddlAttorneys").value == "-1")
        {
            alert("Please select attorney.");
            return false;
        }
    }
    
    function select_deselectAll(chkVal, idVal) 
	{
		var frm = document.forms[0];
		
		// Loop through all elements
		for (i=0; i<frm.length; i++) 
		{
		    if(frm.elements[i].name.substring(0,14) == 'gv_Records$ctl' )
		    {
			    // Look for our Header Template's Checkbox
			    if (idVal.indexOf ('chkReviewHeader') != -1) 
			    {
				    // Check if main checkbox is checked, then select or deselect datagrid checkboxes 
				    if(chkVal == true) 
				    {    					
					    if(frm.elements[i].disabled==true)
					    {
					    }
					    else
					    {
						    frm.elements[i].checked = true;
					    }
				    } 
				    else 
				    {
					    if(frm.elements[i].disabled==true)
					    {
					    }
					    else
					    {
						    frm.elements[i].checked = false;
					    }
				    }
			    }
			}
		}
	}
	
	
	function CheckSelectedRecords() 
	{	    
		var a = 0;     
        var c_value = "";
        var grid = document.getElementById("<%= gv_Records.ClientID%>");
        var cell;
        var chk;
        var hf;
         
        if(grid != null && grid.rows.length > 0)
        {       
            for(i = 2; i<= grid.rows.length;i++)
            {
                if( i < 10)
                {
                    chk = 'gv_Records_ctl0'+i.toString()+'_chkReview';
                    hf ='gv_Records_ctl0'+i.toString()+'_hf_ticketviolationid';
                }
                else
                {
                    chk = 'gv_Records_ctl'+i.toString()+'_chkReview';
                    hf='gv_Records_ctl'+i.toString()+'_hf_ticketviolationid';
                }
                
                var chkbox = document.getElementById(chk);
                                
                if(chkbox != null && chkbox.checked)
                {
                    a=1;
                    c_value = c_value + document.getElementById(hf).value + ",";
                }
            }
        }
                                              
        if(a==0)
        {
            alert("Please select any record to update attorney paid amount.");
            return false;
        }
        document.getElementById("hf_ticketviolationids").value = c_value;
               
	}
		
		
	function showHistoryPDF(filename)
	{	
	    window.open ("../DOCSTORAGE/BatchPrint/" + filename);
		return false;
	}
	
	function CheckCheckboxSelection()
	{
	    var a = 0;     
        var c_value = "";
        var grid = document.getElementById("<%= gv_Records.ClientID%>");
        var cell;
        var chk;
        var hf;
         
        if(grid != null && grid.rows.length > 0)
        {       
            for(i = 2; i<= grid.rows.length;i++)
            {
                if( i < 10)
                {
                    chk = 'gv_Records_ctl0'+i.toString()+'_chkReview';                    
                }
                else
                {
                    chk = 'gv_Records_ctl'+i.toString()+'_chkReview';                    
                }
                
                var chkbox = document.getElementById(chk);
                                
                if(chkbox != null && chkbox.checked)
                {
                    a += 1;
                    chkbox.checked = true;
                }
            }
            if (grid.rows.length-1 == a)
            {
                document.getElementById("gv_Records_ctl01_chkReviewHeader").checked = true;
            }
            else
            {
                document.getElementById("gv_Records_ctl01_chkReviewHeader").checked = false;
            }
        }
	}
	
	
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <aspnew:ScriptManager ID="ScriptManager1" runat="server" />
        <table cellpadding="0" cellspacing="0" width="900px" align="center" border="0">
            <tr>
                <td>
                    <uc1:activemenu ID="ActiveMenu1" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="center" style="width: 100%">
                    <asp:Label ID="lblMessage" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 100%" background="../Images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellpadding="0" width="100%">
                        <tr style="width: 100%">
                            <td style="width: 8%">
                                <span class="clssubhead">Start Date :</span>
                            </td>
                            <td style="width: 10%">
                                <ew:CalendarPopup Visible="true" ID="cal_FromDateFilter" runat="server" AllowArbitraryText="False"
                                    CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Culture="(Default)" EnableHideDropDown="True"
                                    Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif" Nullable="True"
                                    PadSingleDigits="True" ShowClearDate="True" ShowGoToToday="True" Text=" " ToolTip="Date"
                                    UpperBoundDate="9999-12-29" Width="65px">
                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TextboxLabelStyle CssClass="clstextarea" />
                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Gray" />
                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                </ew:CalendarPopup>
                            </td>
                            <td style="width: 8%">
                                <span class="clssubhead">End Date :</span>
                            </td>
                            <td style="width: 10%">
                                <ew:CalendarPopup Visible="true" ID="cal_ToDateFilter" runat="server" AllowArbitraryText="False"
                                    CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Culture="(Default)" EnableHideDropDown="True"
                                    Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif" Nullable="True"
                                    PadSingleDigits="True" ShowClearDate="True" ShowGoToToday="True" Text=" " ToolTip="Date"
                                    UpperBoundDate="9999-12-29" Width="65px">
                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TextboxLabelStyle CssClass="clstextarea" />
                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Gray" />
                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                </ew:CalendarPopup>
                            </td>
                            <td style="width: 8%">
                                <span class="clssubhead">Attorneys :</span>
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="ddlAttorneys" CssClass="clsInputCombo" runat="server" DataTextField="FirmName"
                                    DataValueField="CoveringFirmID">
                                </asp:DropDownList>
                            </td>
                            <td align="left">
                                <asp:Button ID="btnSearch" Text="Search" OnClick="btnSearch_Click" CssClass="clsbutton"
                                    runat="server" OnClientClick="return Validate();" />
                                <asp:Button ID="btnSearchhistory" Text="Search" OnClick="btnSearchhistory_Click"
                                    CssClass="clsbutton" runat="server" Visible="false" />
                            </td>
                            <td align="left">
                                <asp:Button ID="btnUpdate" Text="Print" OnClick="btnUpdate_Click" OnClientClick="return CheckSelectedRecords();"
                                    CssClass="clsbutton" runat="server" Visible="false" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td background="../Images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td background="../Images/subhead_bg.gif" height="34" class="clssubhead" align="right">
                    <table width="100%">
                        <tr>
                            <td align="left">
                                <asp:LinkButton ID="lnk_history" runat="server" Text="History" OnClick="lnk_history_Click"></asp:LinkButton>
                                <asp:LinkButton ID="lnk_back" runat="server" Text="Back" Visible="false" OnClick="lnk_back_Click"></asp:LinkButton>
                            </td>
                            <td align="right">
                                <aspnew:UpdatePanel ID="updatepnlpaging" runat="server" RenderMode="Inline">
                                    <ContentTemplate>
                                        <uc3:pagingcontrol ID="Pagingctrl" runat="server" />
                                        <uc3:pagingcontrol ID="PagingctrlHistory" runat="server" />
                                    </ContentTemplate>
                                </aspnew:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table id="TableGrid" bgcolor="white" border="0" cellpadding="0" cellspacing="0"
                        width="100%">
                        <tr>
                            <td background="../../images/separator_repeat.gif" colspan="5" height="11" style="height: 11px"
                                width="780">
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Label ID="lbl_Message" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2" valign="top">
                                <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlResult">
                                    <ProgressTemplate>
                                        <img src="../Images/plzwait.gif" /><asp:Label ID="lbl1" runat="server" Text="Please Wait ......"
                                            CssClass="clsLabel"></asp:Label>
                                    </ProgressTemplate>
                                </aspnew:UpdateProgress>
                                <aspnew:UpdatePanel ID="upnlResult" runat="server">
                                    <ContentTemplate>
                                        <asp:GridView ID="gv_Records" runat="server" AutoGenerateColumns="False" Width="100%"
                                            CssClass="clsLeftPaddingTable" AllowSorting="True" AllowPaging="True" PageSize="20"
                                            OnPageIndexChanging="gv_Records_PageIndexChanging" OnSorting="gv_Records_Sorting"
                                            CellPadding="0" CellSpacing="0">
                                            <Columns>
                                                <asp:TemplateField HeaderText="S#" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="hlnk_SNo" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + Eval("ticketid_pk") %>'
                                                            Text='<%# Eval("SNo") %>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>First Name</u>" SortExpression="Firstname" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_FName" runat="server" CssClass="GridItemStyle" Text='<%# Eval("Firstname") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>Last Name</u>" SortExpression="Lastname" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_Lname" runat="server" CssClass="GridItemStyle" Text='<%# Eval("Lastname") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>Case Number</u>" SortExpression="ticketnumber"
                                                    HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_casenumber" runat="server" CssClass="GridItemStyle" Text='<%# Eval("ticketnumber") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>Offense Description</u>" SortExpression="[Description]"
                                                    HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_descsiption" runat="server" CssClass="GridItemStyle" Text='<%# Eval("[Description]") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>Charge Level</u>" HeaderStyle-CssClass="clssubhead"
                                                    HeaderStyle-HorizontalAlign="Left" SortExpression="ChargeLevel">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_chargelevel" runat="server" CssClass="GridItemStyle" Text='<%# Eval("ChargeLevel") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>Total fee</u>" SortExpression="lockprice" HeaderStyle-CssClass="clssubhead"
                                                    HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_totalfee" runat="server" CssClass="GridItemStyle" Text='<%#  "$ " + Convert.ToInt32(Eval("lockprice")) %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>Fee Paid</u>" SortExpression="chargeamount" HeaderStyle-CssClass="clssubhead"
                                                    HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_Feepaid" runat="server" CssClass="GridItemStyle" Text='<%# "$ " + Convert.ToInt32(Eval("chargeamount")) %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>Attorney Fee</u>" SortExpression="Attorneyfee"
                                                    HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_AttorneyFee" runat="server" CssClass="GridItemStyle" Text='<%# "$ " + Convert.ToInt32(Eval("Attorneyfee")) %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>Case Disposition Date</u>" SortExpression="dispositiondate"
                                                    HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_casedisposition" runat="server" CssClass="GridItemStyle" Text='<%# Eval("dispositiondate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderStyle-HorizontalAlign="Center">
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="chkReviewHeader" runat="server" onclick="return select_deselectAll(this.checked, this.id);" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkReview" runat="server" onclick="return CheckCheckboxSelection();" />
                                                        <asp:HiddenField ID="hf_ticketviolationid" runat="server" Value='<%# Eval("TicketsViolationID") %>' />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerStyle HorizontalAlign="Center" />
                                            <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                        </asp:GridView>
                                        <asp:GridView ID="gv_history" runat="server" AutoGenerateColumns="False" Width="100%"
                                            CssClass="clsLeftPaddingTable" AllowPaging="True" PageSize="20" CellPadding="0"
                                            CellSpacing="0" OnRowDataBound="gv_history_RowDataBound" OnPageIndexChanging="gv_history_PageIndexChanging">
                                            <Columns>
                                                <asp:TemplateField HeaderText="S#" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_Sno" runat="server" CssClass="GridItemStyle" Text='<%# Eval("sno") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Employee Name" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_EName" runat="server" CssClass="GridItemStyle" Text='<%# Eval("empname") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Firm Name" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_Firmname" runat="server" CssClass="GridItemStyle" Text='<%# Eval("FirmName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Print Datetime" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_datetime" runat="server" CssClass="GridItemStyle" Text='<%# Eval("PrintDatetime","{0:MM/dd/yyyy}") + " @ " + Eval("PrintDatetime","{0:hh:mm tt}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="View" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imgPreviewDoc" runat="server" CommandName="View" ImageUrl="../images/Preview.gif"
                                                            Width="19" />
                                                        <asp:HiddenField ID="hf_filename" runat="server" Value='<%# Eval("Filepath") %>' />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerStyle HorizontalAlign="Center" />
                                            <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                        </asp:GridView>
                                    </ContentTemplate>
                                </aspnew:UpdatePanel>
                                <asp:HiddenField ID="hf_NoteIDs" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td background="../../images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td>
                    <uc1:Footer ID="Footer1" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:HiddenField ID="hf_ticketviolationids" runat="server" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

<%@ Page Language="C#" AutoEventWireup="true" Codebehind="DocketReport.aspx.cs" Inherits="HTP.Reports.DocketReport" %>

<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Docket Report</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
    function doValidation()
    {
    if( document.getElementById('ddl_Status').value == '-5')
    {
        alert('Please select a valid status');
        return(false);
    }
    else
    return(true);
    
    }
    
    function CheckRecordCount()
    {
     
     var checkbond;
     var showonlybonds;
     var checkalldates;
     var alldates;
     var courtid = document.getElementById("ddl_Courthouse").value;
     var type = document.getElementById("ddl_CourtDate").value;
     var datefrom = document.getElementById("dtp_From").value;
     var dateto = document.getElementById("dtp_To").value;
     checkalldates = document.getElementById("chk_AllDates");
     if(checkalldates.checked)
     {
      alldates = 1;
     }
     else
     {
      alldates = 0;
     }
     var status = document.getElementById("ddl_Status").value;
     var coverfirm = document.getElementById("ddl_firms").value;
     checkbond = document.getElementById("chk_ShowOnlyBonds");
     if(checkbond.checked)
     {
      showonlybonds = 1;
     }
      else
     {
       showonlybonds = 0;
     }
     if(document.getElementById('hf_RecordCount').value == "0")
      {
      alert('No records to perform this operation');
      return(false);
      }
      else
      {
     
       window.open('../Reports/DocketReportCR.aspx?courtid='+courtid+'&type='+type+'&datefrom='+datefrom+'&dateto='+dateto+'&alldates='+alldates+'&status='+status+'&coverfirm='+coverfirm+'&showbonds='+showonlybonds,'','fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=yes');
       return(false);
      }
    }
    function countcheck()
    {
    
      if(document.getElementById('hf_RecordCount').value == "0")
      {
      alert('No records to perform this operation');
      return(false);
      }
      else
      {
      return(true);
      }
    }
     function ShowPopup()
         {
         if(countcheck()==true)
        {
        var WinSettings = "center:yes;resizable:no;dialogHeight:150px;dialogwidth:555px;scroll:no"
        var Arg = 1;
        var conf = window.showModalDialog("Popup_Email.aspx",Arg,WinSettings);
        
        if(conf == -1 || conf == null)
        {
            return false;
        }
        else
        {
            document.getElementById('hf_Emails').value = conf;
            return true;
        }
        }
        else
            return false;
    
        }
    
    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table id="MainTable" cellspacing="0" cellpadding="0" align="center" border="0" style="width: 920px">
                <tr>
                    <td>
                        <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td background="../images/separator_repeat.gif" height="11" style="width: 850px">
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" cellpadding="0" cellspacing="0" class="clsleftpaddingtable">
                            <tr>
                                <td style="height: 25px;" class="clssubhead">
                                    Court House</td>
                                <td style="height: 25px;" class="clssubhead">
                                    Date Type and Range</td>
                                <td class="clssubhead" style="width: 129px; height: 25px">
                                    Status</td>
                                <td style="width: 94px; height: 25px;" class="clssubhead">
                                Covering Firm</td>
                                <td class="clssubhead" style="width: 89px; height: 25px">
                                    <asp:CheckBox ID="chk_AllDates" runat="server" Text="All Dates" CssClass="label" /></td>
                                <td class="clssubhead" style="width: 77px; height: 25px">
                                    <asp:Button ID="Button_Submit" runat="server" CssClass="clsbutton" Text="Submit"
                                        OnClick="Button_Submit_Click" OnClientClick="return doValidation()" /></td>
                            </tr>
                            <tr>
                                <td style="height: 28px;">
                                    <asp:DropDownList ID="ddl_Courthouse" runat="server" CssClass="clsInputCombo" Width="141px">
                                    </asp:DropDownList></td>
                                <td style="height: 28px;" class="label">
                                    <asp:DropDownList ID="ddl_CourtDate" runat="server" CssClass="clsInputCombo">
                                        <asp:ListItem Value="1">Court Date</asp:ListItem>
                                        <asp:ListItem Value="2">Hire Date</asp:ListItem>
                                        <asp:ListItem Value="3">Upload Date</asp:ListItem>
                                    </asp:DropDownList>
                                    From:
                                    <ew:CalendarPopup ID="dtp_From" runat="server" AllowArbitraryText="False" CalendarLocation="Bottom"
                                        ControlDisplay="TextBoxImage" Culture="(Default)" 
                                        EnableHideDropDown="True" Font-Names="Tahoma"
                                        Font-Size="8pt" ImageUrl="../images/calendar.gif" Nullable="True" PadSingleDigits="True"
                                        SelectedDate="2006-03-14" ShowGoToToday="True" Text=" " ToolTip="Call Back Date"
                                        UpperBoundDate="12/31/9999 23:59:00" Width="78px" 
                                        LowerBoundDate="1900-01-01">
                                        <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Black" />
                                        <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <TextboxLabelStyle CssClass="clstextarea" />
                                        <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Black" />
                                        <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Gray" />
                                        <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Black" />
                                    </ew:CalendarPopup>
                                    &nbsp;To:
                                    <ew:CalendarPopup ID="dtp_To" runat="server" AllowArbitraryText="False" CalendarLocation="Bottom"
                                        ControlDisplay="TextBoxImage" Culture="(Default)" 
                                        EnableHideDropDown="True" Font-Names="Tahoma"
                                        Font-Size="8pt" ImageUrl="../images/calendar.gif" Nullable="True" PadSingleDigits="True"
                                        SelectedDate="2006-03-14" ShowGoToToday="True" Text=" " ToolTip="Call Back Date"
                                        UpperBoundDate="12/31/9999 23:59:00" Width="78px" 
                                        LowerBoundDate="1900-01-01">
                                        <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Black" />
                                        <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <TextboxLabelStyle CssClass="clstextarea" />
                                        <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Black" />
                                        <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Gray" />
                                        <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Black" />
                                    </ew:CalendarPopup>
                                </td>
                                <td class="label" style="width: 129px; height: 28px">
                                        <asp:DropDownList Width="100%" ID="ddl_Status" runat="server" CssClass="clsInputCombo">
                                        </asp:DropDownList></td>
                                <td style="width: 94px; height: 28px;">
                                    <asp:DropDownList ID="ddl_firms" runat="server" CssClass="clsInputCombo" DataTextField="FirmAbbreviation"
                                        DataValueField="FirmID" Width="84px">
                                    </asp:DropDownList></td>
                                <td style="width: 89px; height: 28px">
                                    <asp:CheckBox ID="chk_ShowOnlyBonds" runat="server" Text="Only Bonds" CssClass="label" /></td>
                                <td style="width: 77px; height: 28px">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td background="../images/separator_repeat.gif" height="11" style="width: 850px">
                    </td>
                </tr>
                <tr>
                    <td style="height: 76px; width: 920px;">
                        <table border="1" cellpadding="1" cellspacing="1" style="border-collapse: collapse"
                            width="100%">
                            <tr>
                                <td align="center" style="font-size: 10pt;" width="15%">
                                    <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="../Images/Email.jpg"
                                        ToolTip="Email" OnClick="SendMail" OnClientClick="return ShowPopup();" /><br />
                                    <font face="Arial" size="1">EMAIL</font>
                                </td>
                                <td align="center" style="font-size: 10pt" width="15%">
                                    <asp:ImageButton ID="imgExp_Ds_to_Excel" runat="server" Height="36px" ImageUrl="../Images/excel_icon.gif"
                                        ToolTip="Excel" Width="68px" OnClick="imgExp_Ds_to_Excel_Click" OnClientClick="return countcheck();" /><br />
                                    <font face="Arial" size="1">EXCEL</font>
                                </td>
                                <td align="center" style="font-size: 10pt" width="15%">
                                    <asp:ImageButton ID="img_pdf" runat="server" ImageUrl="../Images/pdf.jpg" ToolTip="Print Report In CR Version"
                                        OnClientClick="return CheckRecordCount();" />&nbsp;<br />
                                    <font face="Arial" size="1">PDF</font>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td background="../images/separator_repeat.gif" height="11" style="width: 850px">
                    </td>
                </tr>
                <td background="../Images/subhead_bg.gif" class="clssubhead" align="right" style="height: 32px">
                    <table style="width: 100%">
                        <tr>
                            <td>
                            </td>
                            <td style="text-align: right">
                                <uc3:PagingControl ID="Pagingctrl" runat="server" />
                               
                            </td>
                        </tr>
                    </table>
                </td>
                <tr>
                    <td align="center" style="width: 850px">
                        <asp:Label ID="lbl_Message" runat="server" CssClass="clslabel" ForeColor="Red"></asp:Label></td>
                </tr>
                <tr>
                    <td style="width: 920px">
                        <asp:GridView ID="gv_Result" runat="server" AutoGenerateColumns="False" CssClass="clsleftpaddingtable"
                            Width="920px" AllowPaging="True" OnPageIndexChanging="gv_Result_PageIndexChanging"
                            PageSize="30" AllowSorting="True" OnSorting="GridView1_Sorting" OnRowDataBound="gv_Result_RowDataBound">
                            <Columns>
                                <asp:TemplateField HeaderText="<u>S#</u>" SortExpression="S NO">
                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_Ticketid" Visible="false" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.ticketid_pk")  %>'></asp:Label>
                                        <asp:HyperLink ID="hl_SNo" runat="server" NavigateUrl='<%# "../ClientInfo/ViolationFeeOld.aspx?CaseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk")+"&search="+DataBinder.Eval(Container, "DataItem.ACTIVEFLAG") %>'
                                            Text='<%# DataBinder.Eval(Container, "DataItem.S No") %>'></asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Cause No">
                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblCauseNo" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.CAUSENUMBER") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Ticket No">
                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_TicketNo" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.TICKETNUMBER") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<u>Hire</u>" SortExpression="hire" HeaderStyle-Width="100px">
                                    <HeaderStyle HorizontalAlign="Left" CssClass="clssubhead" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_Hire" runat="server" CssClass="clsLabel" Text='<%# bind("HIRE","{0 : MM/dd/yy hh:mm tt}") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<u>Update</u>" SortExpression="update" HeaderStyle-Width="100px">
                                    <HeaderStyle HorizontalAlign="Left" CssClass="clssubhead" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_Update" runat="server" CssClass="clsLabel" Text='<%# bind("UPDATE","{0 : MM/dd/yy hh:mm tt}") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<u>Last</u>" SortExpression="LASTNAME">
                                    <HeaderStyle HorizontalAlign="Left" CssClass="clssubhead" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_LastName" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.LASTNAME") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="First">
                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_FirstName" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.FIRSTNAME") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="DOB" HeaderStyle-Width="50px">
                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_DOB" runat="server" CssClass="clsLabel" Text='<%# bind("DOB","{0 : MM/dd/yyyy}") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="DL">
                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_DL" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.DL") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Crt Info">
                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" Width="110px" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_CourtDate" runat="server" CssClass="clsLabel" Text='<%# bind("COURTDATE","{0 : MM/dd/yy hh:mm tt}") %>'></asp:Label>
                                        <asp:Label ID="lbl_CourtNo" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.COURTNO") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Stat">
                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" Width="25px" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_Stat" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.STAT") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Loc" HeaderStyle-Width="50px">
                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left"/>
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_LOC" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.LOC") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="COV" HeaderText="Cov">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" Width="25px" />
                                    <ItemStyle CssClass="clslabel" BorderStyle="Solid" BorderWidth="1px" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Flags">
                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" Width="25px" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblFlags" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.bflag") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerSettings Mode="NextPreviousFirstLast" NextPageText="Next" PreviousPageText="Previous" />
                            <PagerStyle HorizontalAlign="Center" />
                        </asp:GridView>
                        <asp:HiddenField ID="hf_RecordCount" Value="0" runat="server" />
                        <asp:HiddenField ID="hf_Emails" Value="" runat="server" />
                        &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                    <td background="../images/separator_repeat.gif" height="11" style="width: 850px">
                    </td>
                </tr>
                <tr>
                    <td style="width: 780px">
                        <uc1:Footer ID="Footer1" runat="server" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using lntechNew.Components;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;


namespace lntechNew.Reports
{
    public partial class frmContactDiscrepancy : System.Web.UI.Page
    {
        #region Variables

        ContactDiscrepancy CD = new ContactDiscrepancy();
        clsGeneralMethods clsgenral = new clsGeneralMethods();
        clsLogger clog = new clsLogger();
        clsSession ClsSession = new clsSession();

        string address;
        string oldmidnumer;
        string newmidnumber;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    // Fill the datagrid method called on load
                    FillGrid();
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
            }

        }

        protected void gv_ContactDisc_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                // It will combine all three contacts and their types
                //It check contacts and convert into right format and then combined contacttype

                string Contact = "";
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    /// Contact 1
                    string c1 = ((HiddenField)e.Row.FindControl("hf_contact1")).Value;
                    if (c1 != "" & c1.Length > 0)
                        Contact += clsgenral.formatPhone(c1) + "(" + ((HiddenField)e.Row.FindControl("hf_contacttype1")).Value + ")";
                    /// Contact 2
                    string c2 = ((HiddenField)e.Row.FindControl("hf_contact2")).Value;
                    if (c2 != "" & c2.Length > 0)
                        Contact += "<br />" + clsgenral.formatPhone(c2) + "(" + ((HiddenField)e.Row.FindControl("hf_contacttype2")).Value + ")";
                    /// Contact 3
                    string c3 = ((HiddenField)e.Row.FindControl("hf_contact3")).Value;
                    if (c3 != "" & c3.Length > 0)
                        Contact += "<br />" + clsgenral.formatPhone(c3) + "(" + ((HiddenField)e.Row.FindControl("hf_contacttype3")).Value + ")";

                    ((Label)e.Row.FindControl("lbl_Contacts")).Text = Contact;

                    //string Tid = ((HiddenField)e.Row.FindControl("hf_TicketID")).Value;
                    //((HyperLink)e.Row.FindControl("hl_SNo")).NavigateUrl = "../ClientInfo/violationfeeold.aspx?search=0&casenumber=" + Tid;

                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
            }
        }

        // On pageing get table from session and again fill up the grid
        protected void gv_ContactDisc_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                lbl_Message.Text = "";
                ddl_Pages.SelectedValue = Convert.ToString(e.NewPageIndex + 1);
                gv_ContactDisc.PageIndex = e.NewPageIndex;
                DataView dv = (DataView)Session["DS"];
                gv_ContactDisc.DataSource = dv;
                gv_ContactDisc.DataBind();


            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
            }
        }

        protected void gv_ContactDisc_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "showaddress")
            {
                CD.MidNumber = e.CommandArgument.ToString();
                dl_addresses.DataSource = CD.GetAllAddresses();
                dl_addresses.DataBind();
                hf_showpopup.Value = "1";
            }
        }

        protected void dl_addresses_ItemCommand(object source, DataListCommandEventArgs e)
        {

            try
            {
                if (e.CommandName == "updateaddress")
                {
                    CD.TicketID = Convert.ToInt32(((HiddenField)e.Item.FindControl("hf_ticketid")).Value);
                    CD.MidNumber = e.CommandArgument.ToString();
                    CD.EmployeeID = Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request));
                    CD.UpdateContacts = Convert.ToBoolean(Convert.ToInt32(((HiddenField)e.Item.FindControl("hf_updatecontact")).Value));
                    if (CD.UpdateMidNumber())
                        FillGrid();
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
            }
        }

        protected void dl_addresses_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer)
            {
                ((Button)e.Item.FindControl("btn_save")).Attributes.Add("onClick", "submitAddress('" + ((HiddenField)e.Item.FindControl("hf_updatecontact")).ClientID + "');");

            }
        }

        protected void ddl_Pages_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                lbl_Message.Text = "";
                gv_ContactDisc.PageIndex = Convert.ToInt32(ddl_Pages.SelectedValue) - 1;
                DataView dv = (DataView)Session["DS"];
                gv_ContactDisc.DataSource = dv;
                gv_ContactDisc.DataBind();

            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        #endregion

        #region Methods

        //  It fill up the DataGrid 
        private void FillGrid()
        {
            try
            {
                lbl_Message.Text = "";
                DataSet ds = CD.GetContactDiscrepancy();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    lblPage.Visible = true;
                    ddl_Pages.Visible = true;
                    // Genrate serial numbers before fill grid
                    GenerateSerial(ds.Tables[0]);

                    // set DATA Table in session
                    DataView dv = new DataView(ds.Tables[0]);
                    Session["DS"] = dv;

                    gv_ContactDisc.DataSource = ds;
                    gv_ContactDisc.DataBind();
                    FillPageList();
                }
                else
                {
                    lbl_Message.Text = "No Record Found";
                    lblPage.Visible = false; 
                    ddl_Pages.Visible = false;
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
            }
        }

        // Fill the Drop down page list 
        private void FillPageList()
        {
            try
            {
                int rec = gv_ContactDisc.PageCount;
                ddl_Pages.Items.Clear();
                for (int i = 1; i <= rec; i++)
                {
                    ddl_Pages.Items.Add(new ListItem("Page- " + i.ToString(), i.ToString()));
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        //It will genrate serial numbers according to same mudnumber 
        private void GenerateSerial(DataTable dt)
        {

            try
            {


                int no = 1;
                string midnum = "";
                int sno = 1;
                if (dt.Columns["SNO"] == null)
                {
                    dt.Columns.Add("SNO");
                    dt.Rows[0]["SNO"] = no;
                }
                else
                {
                    dt.Columns.Remove("SNO");
                    dt.Columns.Add("SNO");
                    dt.Rows[0]["SNO"] = no;
                }
                midnum = dt.Rows[0]["MidNum"].ToString();

                for (int i = 1; i < dt.Rows.Count; i++)
                {

                    string mn = dt.Rows[i]["MidNum"].ToString();
                    if (mn != "")
                    {
                        if (midnum != mn)
                        {
                            no++;
                            dt.Rows[i]["SNO"] = no;
                            midnum = mn;

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }


        }

        #endregion

    }
}

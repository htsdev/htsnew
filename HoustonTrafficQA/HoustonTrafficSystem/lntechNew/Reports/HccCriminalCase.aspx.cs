using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using MSXML2;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.XPath;
using lntechNew.Components.ClientInfo;
using HTP.WebComponents;


namespace HTP.Reports
{
    public partial class HccCriminalCase : MatterBasePage 
    {
        int section = 0;
        string caseno = "";
        string sessionidspn = "";
        string spnusername; 
        string spnuserpassword;
        string response = "";
        clsUser cu = new clsUser();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                //Kazim 3791 4/22/2008 Get SPNUserName and password from DataBase instead of Web.config  
 
                cu.EmpID = this.EmpId;
                DataTable dt = cu.GetSystemUserInfo();
                if (dt != null && dt.Rows.Count > 0)
                {
                    spnusername = dt.Rows[0]["SPNUserName"].ToString();
                    spnuserpassword = dt.Rows[0]["SPNPassword"].ToString();
                }
           
                if (null == this.Request.QueryString["section"] || !int.TryParse(this.Request.QueryString["section"], out section))
                {
                    Response.Write("Invalid query string");
                    Response.End();
                }
                switch (section)
                {
                    case 1:
                        break;

                    case 2:
                        break;

                    case 3:
                    case 4:
                        if (null == this.Request.QueryString["spn"])
                        {
                            Response.Write("Invalid query string");
                            return;
                        }
                        string spn = this.Request.QueryString["spn"];
                        response = GetSection3And4Response(spn);
                        Response.Write(response);
                        break;


                    case 5:
                    case 6:
                        int court;
                        
                        if ((null == this.Request.QueryString["court"]) || (int.TryParse(this.Request.QueryString["court"], out court) == false))
                        {
                            Response.Write("Invalid query string");
                            return;
                        }
                        response = GetSection5Response(court);
                        Response.Redirect(response);
                        break;


                    case 7:
                        if (null == this.Request.QueryString["caseno"])
                        {
                            Response.Write("Invalid query string");
                            return;
                        }

                        caseno = this.Request.QueryString["caseno"];
                        string CDI = caseno.Substring(0, 3);
                        response = this.GetSection7Response(caseno, CDI);
                        Response.Write(response);
                        break;
                }
                
            }
            catch (Exception exp)
            {
                Response.Write(exp.Message);
            }
        }

        public void setRequestHeader(XMLHTTP ObjHttp)
        {
            ObjHttp.setRequestHeader("(Request-Line)", "POST  /subcrim/subcrim_logon.do HTTP/1.1");
            ObjHttp.setRequestHeader("Accept", "image/gif, image/x-xbitmap, image/jpeg, image/pjpeg, application/x-shockwave-flash, application/xaml+xml, application/vnd.ms-xpsdocument, application/x-ms-xbap, application/x-ms-application, application/vnd.ms-excel, application/vnd.ms-powerpoint, application/msword, */*");
            ObjHttp.setRequestHeader("Accept-Encoding", "gzip, deflate");
            ObjHttp.setRequestHeader("Accept-Language", "en-us,fr;q=0.5");
            ObjHttp.setRequestHeader("Cache-Control", "no-cache");
            ObjHttp.setRequestHeader("Connection", "Keep-Alive");
            ObjHttp.setRequestHeader("Content-Length", "63");
            ObjHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            ObjHttp.setRequestHeader("Host", "apps.jims.hctx.net");
        }

        private string GetSection7Response(string casenumber, string CDI)
        {

            string url = "http://apps.jims.hctx.net/subcrim/Logon.jsp";
            XMLHTTP ObjHttp = new XMLHTTP();
            //Connecting to site
            ObjHttp.open("get", url, false, null, null);
            ObjHttp.send("");

            // Get Session ID From Cookie 
            if (ObjHttp.getResponseHeader("Set-Cookie") != "")
            {
                sessionidspn = ObjHttp.getResponseHeader("Set-Cookie");
                sessionidspn = sessionidspn.Substring(sessionidspn.IndexOf("=") + 1, sessionidspn.IndexOf(";") - 11);
            }

            // Go To Login Page
            ObjHttp.open("post", "http://apps.jims.hctx.net/subcrim/subcrim_logon.do;jsessionid=" + sessionidspn, false, null, null);

            // Set Request Header
            setRequestHeader(ObjHttp);
            ObjHttp.setRequestHeader("Cookie", "JSESSIONID=" + sessionidspn);
            ObjHttp.setRequestHeader("Referer", "http://apps.jims.hctx.net/subcrim/Logon.jsp");
            ObjHttp.send("logonid=" + spnusername + "&password=" + spnuserpassword + "&newpasswordverify=&newpassword=");

            // Go to Search Page
            ObjHttp.open("post", "http://apps.jims.hctx.net/subcrim/CaseInquiry.do;jsessionid=" + sessionidspn, false, null, null);

            // Set Request Header
            setRequestHeader(ObjHttp);
            ObjHttp.setRequestHeader("Cookie", "JSESSIONID=" + sessionidspn);
            ObjHttp.setRequestHeader("Referer", "http://apps.jims.hctx.net/subcrim/CaseInquiry.do");
            ObjHttp.send("action=Create&courtDivision=" + CDI + "&caseNum=" + casenumber);


            // Goto Persons Connected to Case (QCOC) page
            ObjHttp.open("post", "http://apps.jims.hctx.net/subcrim/PersonsConnectedResults.do;jsessionid=" + sessionidspn, false, null, null);

            setRequestHeader(ObjHttp);
            ObjHttp.setRequestHeader("Cookie", "JSESSIONID=" + sessionidspn);
            ObjHttp.setRequestHeader("Referer", "http://apps.jims.hctx.net/subcrim/PersonsConnectedResults.do");
            ObjHttp.send("action=Persons Connected (QCOC)");


            if (ObjHttp.status.ToString() == "200")
            {
                return ObjHttp.responseText;
            }
            else
            {
                return "";
            }
        }

        private string GetSection3And4Response(string SPN)
        {
            try
            {

                string url = "http://apps.jims.hctx.net/victiminfo/inquiry.jsp";
                XMLHTTP ObjHttp = new XMLHTTP();
                //Connecting to site
                ObjHttp.open("get", url, false, null, null);
                ObjHttp.send("");

                // Get Session ID From Cookie 
                if (ObjHttp.getResponseHeader("Set-Cookie") != "")
                {
                    sessionidspn = ObjHttp.getResponseHeader("Set-Cookie");
                    sessionidspn = sessionidspn.Substring(sessionidspn.IndexOf("=") + 1, sessionidspn.IndexOf(";") - 11);
                }
                // Get Case Detail
                ObjHttp.open("post", "http://apps.jims.hctx.net/victiminfo/Submit.do;jsessionid=" + sessionidspn, false, null, null);

                // Set Request Header
                ObjHttp.setRequestHeader("(Request-Line)", "POST /victiminfo/Submit.do;jsessionid=" + sessionidspn + " HTTP/1.1");
                ObjHttp.setRequestHeader("Accept", "image/gif, image/x-xbitmap, image/jpeg, image/pjpeg, application/x-shockwave-flash, application/xaml+xml, application/vnd.ms-xpsdocument, application/x-ms-xbap, application/x-ms-application, application/vnd.ms-excel, application/vnd.ms-powerpoint, application/msword, */*");
                ObjHttp.setRequestHeader("Accept-Encoding", "gzip, deflate");
                ObjHttp.setRequestHeader("Accept-Language", "en-us,fr;q=0.5");
                ObjHttp.setRequestHeader("Cache-Control", "no-cache");
                ObjHttp.setRequestHeader("Connection", "Keep-Alive");
                ObjHttp.setRequestHeader("Content-Length", "63");
                ObjHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                ObjHttp.setRequestHeader("Cookie", "JSESSIONID=" + sessionidspn);
                ObjHttp.setRequestHeader("Host", "apps.jims.hctx.net");
                ObjHttp.setRequestHeader("Referer", "http://apps.jims.hctx.net/victiminfo/inquiry.jsp");
                ObjHttp.send("spn=" + SPN + "&caseType=&caseNum=&offenseNum=&submitAction=Submit");

                if (ObjHttp.status.ToString() == "200")
                {
                    return ObjHttp.responseText;
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        private string GetSection5Response(int court)
        {
            try
            {
                string url = "http://www.justex.net/CriminalCourtBuilding.aspx";
                XMLHTTP ObjHttp = new XMLHTTP();
                //Connecting to site
                ObjHttp.open("get", url, false, null, null);
                ObjHttp.send("");

                if (ObjHttp.status.ToString() != "200")
                {
                    return "Status Error";
                }
                string result =  ObjHttp.responseText;
                int index = 0;
                if (court < 20)
                {
                    index = result.IndexOf("\"><FONT size=2>" + court + "</FONT></A></TD>");
                }
                else
                {
                    index = result.IndexOf("\"><FONT size=2>" + court);
                }
                result = result.Substring(0,index);
                index = result.LastIndexOf("<A href=");
                return result.Substring(index + 9);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }



    }
}

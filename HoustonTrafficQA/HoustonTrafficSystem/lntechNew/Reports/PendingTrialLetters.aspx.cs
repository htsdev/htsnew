using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;


namespace lntechNew.Reports
{
    public partial class PendingTrialLetters : System.Web.UI.Page
    {

        clsSession ClsSession = new clsSession();
        clsLogger clog = new clsLogger();
        DataTable dtRecords = new DataTable();

        PendingTrialLetter FillData = new PendingTrialLetter();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lbl_Message.Text = "";

                if (!IsPostBack)
                {
                    GetRecords();
                    if (dtRecords.Rows.Count == 0)
                    {
                        lbl_Message.Text = "No Records!";

                    }
                    else
                    {

                        gv_Data.DataSource = dtRecords;
                        gv_Data.DataBind();

                        //khllid 3260 3/4/08 to add paging control
                        DataView dv = new DataView(dtRecords);
                        Session["Data"] = dv;
                        Pagingctrl.PageCount = gv_Data.PageCount;
                        Pagingctrl.PageIndex = gv_Data.PageIndex;
                        Pagingctrl.SetPageIndex();



                    }

                }

                //khalid 3260  3/4/08  to add paging control
                Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);

            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        public SortDirection GridViewSortDirection
        {

            get
            {

                if (ViewState["sortDirection"] == null)

                    ViewState["sortDirection"] = SortDirection.Ascending;

                return (SortDirection)ViewState["sortDirection"];

            }

            set { ViewState["sortDirection"] = value; }

        }


        private void GetRecords()
        {
            DataSet ds = FillData.GetData();
            dtRecords = ds.Tables[0];


        }

        protected void gv_Data_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowIndex >= 0)
                {
                    HyperLink hlnk = (HyperLink)e.Row.FindControl("hl_SNo");
                    //hlnk.Text = Convert.ToString(e.Row.RowIndex + 1);



                    HiddenField hf = (HiddenField)e.Row.FindControl("hf_TicketID");
                    hlnk.NavigateUrl = "javascript:window.open('../ClientInfo/violationfeeold.aspx?search=0&caseNumber=" + hf.Value + "');void('');";
                    ((ImageButton)e.Row.FindControl("img_delete")).Attributes.Add("onclick", "return DeleteConfirm();");
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }



        protected void gv_Data_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (Session["SortExpression"] != null && Session["SortDirection"] != null)
                {
                    //SortGridView(Convert.ToString(Session["SortExpression"]), Convert.ToString(Session["SortDirection"]));
                    GetRecords();
                    DataTable dt = dtRecords;
                    DataView dv = new DataView(dt);
                    Session["Data"] = dv;
                    dv.Sort = Convert.ToString(Session["SortExpression"]) + Convert.ToString(Session["SortDirection"]);
                    gv_Data.DataSource = dv;

                }
                else
                {
                    GetRecords();
                    gv_Data.DataSource = dtRecords;

                }

                gv_Data.PageIndex = e.NewPageIndex;
                gv_Data.DataBind();
                //khalid 3/13/2008 3260 to fix paging control bug
                Pagingctrl.PageCount = gv_Data.PageCount;
                Pagingctrl.PageIndex = gv_Data.PageIndex;
                Pagingctrl.SetPageIndex();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }



        protected void gv_Data_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Del")
                {
                    int TicketID = Convert.ToInt32(e.CommandArgument.ToString());

                    object[] value ={ TicketID, Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request)) };
                    PendingTrialLetter Update = new PendingTrialLetter(value);
                    Update.UpdateFlag();

                    //To counter event bubbling
                    Response.Redirect("PendingTrialLetters.aspx", false);

                }

            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void gv_Data_Sorting(object sender, GridViewSortEventArgs e)
        {
            string sortExpression = e.SortExpression;
            Session["SortExpression"] = sortExpression;

            if (GridViewSortDirection == SortDirection.Ascending)
            {

                GridViewSortDirection = SortDirection.Descending;
                Session["SortDirection"] = " desc";
                SortGridView(sortExpression, " desc");

            }

            else
            {

                GridViewSortDirection = SortDirection.Ascending;
                Session["SortDirection"] = " asc";
                SortGridView(sortExpression, " asc");

            }
            //khalid 3260  3/4/08 to add paging control
            Pagingctrl.PageCount = gv_Data.PageCount;
            Pagingctrl.PageIndex = gv_Data.PageIndex;
            Pagingctrl.SetPageIndex();


        }

        private void SortGridView(string sortExpression, string direction)
        {

            // You can cache the DataTable for improving performance

            GetRecords();

            DataTable dt = dtRecords;

            DataView dv = new DataView(dt);

            dv.Sort = sortExpression + direction;

            gv_Data.DataSource = dv;

            gv_Data.DataBind();

            //khalid 3260  3/4/08 to add paging control
            Pagingctrl.PageCount = gv_Data.PageCount;
            Pagingctrl.PageIndex = gv_Data.PageIndex;
            Pagingctrl.SetPageIndex();


        }

        /// <summary>
        /// khalid 3260 3/4/08 
        /// to manipulate grid when the page index changed
        /// </summary>
        protected void Pagingctrl_PageIndexChanged()
        {
            try
            {
                gv_Data.PageIndex = Pagingctrl.PageIndex - 1;
                gv_Data.DataSource = Session["Data"];
                gv_Data.DataBind();

                Pagingctrl.PageCount = gv_Data.PageCount;
                Pagingctrl.PageIndex = gv_Data.PageIndex;
                Pagingctrl.SetPageIndex();
            }
            catch (Exception ex)
            {
                //lbl_message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }


    }
}

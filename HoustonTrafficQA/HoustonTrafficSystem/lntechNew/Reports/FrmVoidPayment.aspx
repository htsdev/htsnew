

<%@ Register TagPrefix="uc1" TagName="MenuTop" Src="../WebControls/MenuTop.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Page language="c#" Codebehind="FrmVoidPayment.aspx.cs" AutoEventWireup="false" Inherits="lntechNew.Reports.FrmVoidPayment" smartNavigation="False"%>

<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc2" %>

<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Void Payment Report</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
			
				<TABLE id="Table2" cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
				<TR>
					<TD colSpan="7"><uc1:activemenu id="ActiveMenu1" runat="server"></uc1:activemenu></TD>
				</TR>
				<TR>
					<TD colSpan="5">
							<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
								<TR>
									<TD style="WIDTH: 92px" align="right" valign=middle >
                                        <strong>From:</strong>&nbsp;
									</TD>
									<TD style="WIDTH: 109px" valign=middle>
										<ew:calendarpopup id="Calendarpopup1" runat="server" Width="84px" ToolTip="Select Report Date"
											PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00" Culture="(Default)" AllowArbitraryText="False"
											ShowGoToToday="True" CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Font-Names="Tahoma"
											Font-Size="8pt" ImageUrl="../images/calendar.gif">
											<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
											<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="White"></WeekdayStyle>
											<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="Yellow"></MonthHeaderStyle>
											<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
												BackColor="AntiqueWhite"></OffMonthStyle>
											<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="White"></GoToTodayStyle>
											<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="LightGoldenrodYellow"></TodayDayStyle>
											<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="Orange"></DayHeaderStyle>
											<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="LightGray"></WeekendStyle>
											<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="Yellow"></SelectedDateStyle>
											<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="White"></ClearDateStyle>
											<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="White"></HolidayStyle>
										</ew:calendarpopup></TD>
									<TD style="WIDTH: 103px" valign=middle  align="right">
                                        <strong>To:</strong>&nbsp;
									</TD>
									<TD style="WIDTH: 110px" valign=middle >
										<ew:calendarpopup id="Calendarpopup2" runat="server" Width="84px" ToolTip="Select Report Date"
											PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00" Culture="(Default)" AllowArbitraryText="False"
											ShowGoToToday="True" CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Font-Names="Tahoma"
											Font-Size="8pt" ImageUrl="../images/calendar.gif">
											<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
											<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="White"></WeekdayStyle>
											<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="Yellow"></MonthHeaderStyle>
											<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
												BackColor="AntiqueWhite"></OffMonthStyle>
											<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="White"></GoToTodayStyle>
											<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="LightGoldenrodYellow"></TodayDayStyle>
											<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="Orange"></DayHeaderStyle>
											<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="LightGray"></WeekendStyle>
											<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="Yellow"></SelectedDateStyle>
											<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="White"></ClearDateStyle>
											<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="White"></HolidayStyle>
										</ew:calendarpopup></TD>
									<TD style="WIDTH: 87px" align="right">&nbsp;
									</TD>
									<TD></TD>
									<TD align="right">
										<asp:imagebutton id="Btn_search" runat="server" ImageUrl="../Images/search.jpg"></asp:imagebutton>&nbsp;</TD>
									<TD>&nbsp;&nbsp;
										<asp:imagebutton id="btn_Reset" runat="server" ImageUrl="../Images/reset.jpg"></asp:imagebutton></TD>
								</TR>
							</TABLE>
						</TD>
					</TR>
					<TR>
						<TD style="HEIGHT: 16px" align="center" colSpan="4">&nbsp;
							<asp:label id="lblMessage" runat="server" Width="128px" Height="13px" ForeColor="Red"></asp:label></TD>
					</TR>
					<TR>
						<TD class="TDHeadingnew" style="HEIGHT: 10px" colSpan="4">
							<TABLE class="clsmainhead" id="Table3" cellSpacing="0" cellPadding="0" width="272" align="left"
								border="0">
								<TR>
									<TD width="31"><IMG style="WIDTH: 24px; HEIGHT: 22px" height="22" src="../Images/void2.jpg" width="24"></TD>
									<TD class="TDHeadingnew" vAlign="bottom"><FONT color="#666666">Void Payment Report</FONT></TD>
								</TR>
							</TABLE>
						</TD>
					</TR>
					<TR>
						<TD style="HEIGHT: 1px" background="../Images/separator_repeat.gif" colSpan="4"></TD>
					</TR>
					<TR>
						<TD align="center" colSpan="4">
							<asp:datagrid id="dg_VoidReport" runat="server" Width="781px" Font-Names="Verdana" Font-Size="2px"
								ShowFooter="True" AllowPaging="True" PageSize="20" AutoGenerateColumns="False" AllowSorting="True">
								<AlternatingItemStyle BackColor="#EEEEEE"></AlternatingItemStyle>
								<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle"></HeaderStyle>
								<FooterStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle"></FooterStyle>
								<Columns>
									<asp:TemplateColumn HeaderText="Record ID">
										<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
										<ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
										<ItemTemplate>
											<asp:LinkButton id="lblSno" runat="server" CssClass="GrdLbl" ForeColor="Black" 
												CommandName="MkSession"></asp:LinkButton>&nbsp;
											<asp:LinkButton id=HLTicketno runat="server" Width="56px" Font-Size="XX-Small" ForeColor="Black"  CommandName="MkSession" Text='<%# DataBinder.Eval(Container.DataItem, "TicketID") %>' Visible="False">
											</asp:LinkButton>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn SortExpression="Firstname" HeaderText="First Name">
										<HeaderStyle HorizontalAlign="Center" ForeColor="#006699" CssClass="GrdHeader" VerticalAlign="Middle"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id=Label6 runat="server" CssClass="GrdLbl" Font-Size="XX-Small" Text='<%# DataBinder.Eval(Container, "DataItem.Firstname") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn SortExpression="Lastname" HeaderText="Last Name">
										<HeaderStyle HorizontalAlign="Center" ForeColor="#006699" CssClass="GrdHeader"></HeaderStyle>
										<ItemStyle VerticalAlign="Middle"></ItemStyle>
										<ItemTemplate>
											<asp:Label id=Label4 runat="server" CssClass="GrdLbl" Font-Size="XX-Small" Text='<%# DataBinder.Eval(Container, "DataItem.Lastname") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Payment Amount">
										<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
										<ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
										<ItemTemplate>
											<asp:Label id=lblPaymentAmout runat="server" CssClass="GrdLbl" Font-Size="XX-Small" Text='<%# DataBinder.Eval(Container, "DataItem.ChargeAmount") %>'>
											</asp:Label>&nbsp;
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Payment Date">
										<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
										<ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
										<ItemTemplate>
											<asp:Label id=Label3 runat="server" CssClass="GrdLbl" Font-Size="XX-Small" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentDate") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
								</Columns>
								<PagerStyle Font-Size="XX-Small" HorizontalAlign="Center" ForeColor="Black" BackColor="#999999"
									Mode="NumericPages"></PagerStyle>
							</asp:datagrid><br />
                            <table>
                                <tr>
                                    <td width="780">
                                        <br />
                                        <uc2:Footer ID="Footer1" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </TD>
					</TR>
				</TABLE>
				<asp:LinkButton id="lnk" ForeColor="White" Runat="server"></asp:LinkButton>
		</form>
	</body>
</HTML>

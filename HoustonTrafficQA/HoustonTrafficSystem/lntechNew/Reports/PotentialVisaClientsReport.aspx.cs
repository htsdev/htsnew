using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;
using lntechNew;
using FrameWorkEnation.Components;
using HTP.Components;


namespace HTP.Reports
{
    /// <summary>
    /// Summary description for frmBatchReport.
    /// </summary>
    public partial class PotentialVisaClientsReport : System.Web.UI.Page
    {

        clsBatch batch = new clsBatch();
        clsCrsytalComponent Cr = new clsCrsytalComponent();
        clsSession ClsSession = new clsSession();
        clsENationWebComponents ClsDB = new clsENationWebComponents();

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    BatchLetterType batchLetterType = (BatchLetterType)34;

                    Session["updatebatch"] = "true";
                    //Aziz 3220 02/22/08 Changed to session from queryString
                    int EmpID = Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request));
                    String LetterType = "34";
                    String TicketIds = Session["PotentialVisaClientsTicketID"].ToString();
                    BatchReport batchReport = batch.getReportName(batchLetterType);
                    //Yasir Kamal 7590 03/29/2010 trial letter modifications
                    String Batch = "True";

                    //Create Preview File
                    string[] key1 = { "@empid", "@LetterType", "@TicketIDList"};
                    object[] value1 = { EmpID, Convert.ToInt32(batchLetterType), TicketIds };

                    Cr.CreateReport(Server.MapPath("") + "\\" + batchReport.ReportName, batchReport.ReportProcedure, key1, value1, this.Session, this.Response, LetterType, TicketIds, Batch, TicketIds);
                    
                    //Update Batch Information
                    batch.updateBatchPrintInformation(batchLetterType, TicketIds, EmpID, TicketIds);
                }

            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
            }
        }



        private void DeleteBatch()
        {

        }
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion


    }

}

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient ;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.Reports
{
	/// <summary>
	/// Summary description for frmRptReminderCall.
	/// </summary>
	public partial class frmRptBondReminder : System.Web.UI.Page
	{
		DataSet ds;
		DataView dv;
		int recCount;

		clsENationWebComponents ClsDb = new clsENationWebComponents();		
		clsSession ClsSession=new clsSession();
		clsLogger bugTracker = new clsLogger();


		protected System.Web.UI.WebControls.Button btnSubmit;
		protected System.Web.UI.WebControls.Label lblMessage;
		protected System.Web.UI.WebControls.Label lblCurrPage;
		protected System.Web.UI.WebControls.Label lblPNo;
		protected System.Web.UI.WebControls.Label lblGoto;
		protected System.Web.UI.WebControls.DropDownList cmbPageNo;
		protected System.Web.UI.WebControls.Button btnReset;
		protected System.Web.UI.WebControls.DropDownList ddlClientType;
		protected eWorld.UI.CalendarPopup dtFrom;
		protected eWorld.UI.CalendarPopup dtTo;
		protected System.Web.UI.WebControls.Panel Panel1;
		protected System.Web.UI.WebControls.DataGrid dgClient;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				if (ClsSession.IsValidSession(this.Request)==false)
				{
					Response.Redirect("../frmlogin.aspx",false);
				}
				else //To stop page further execution
				{
					if (!IsPostBack)
					{
						// CALLING PROCEDURE THAT WILL POPULATE THE PAGE.......
						btnSubmit.Attributes.Add("OnClick", "return validate();");
						SearchRecords();

					}
				}
			}
			catch(Exception ex)
			{
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}

		private void FillPageList()
		{
			try
			{
				// Procedure for filling page numbers in page number combo........
				Int16 idx;

				if (dgClient.Items.Count == 0 )
				{
					cmbPageNo.Items.Clear();
					cmbPageNo.Items.Add ("--" );
					cmbPageNo.Items[0].Value= "--";
					lblPNo.Text = "";
					return;
				}
				cmbPageNo.Items.Clear();
				for (idx=1; idx<=dgClient.PageCount;idx++)
				{
					cmbPageNo.Items.Add ("Page - " + idx.ToString());
					cmbPageNo.Items[idx-1].Value=idx.ToString();
				}
			}
			catch(Exception ex)
			{
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}

		private void SetNavigation()
		{
			// Procedure for setting data grid's current  page number on header ........
			
			try
			{
				//pgSize=dgClient.PageSize ;
				//pgCounter=dgClient.PageCount;
				
				if (dgClient.Items.Count == 0 )
				{
					lblCurrPage.Visible = false;
					lblPNo.Visible = false;
					lblGoto.Visible = false;
					cmbPageNo.Visible = false;
				}
				else
				{
				
					// setting current page number
					lblPNo.Text =Convert.ToString(dgClient.CurrentPageIndex+1);
					lblCurrPage.Visible=true;
					lblPNo.Visible=true;

					// filling combo with page numbers
					lblGoto.Visible=true;
					cmbPageNo.Visible=true;
					//FillPageList();

					for (int idx =0 ; idx < cmbPageNo.Items.Count ;idx++)
					{
						cmbPageNo.Items[idx].Selected = false;
					}
					cmbPageNo.Items[dgClient.CurrentPageIndex].Selected=true;
				}
			}
			catch (Exception ex)
			{
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
				lblMessage.Text = ex.Message ;
			}
		}

		private void SearchRecords()
		{
			// procedure for filling grid with data...
			FillGrid();
			
			// filling page numbers in combo for page navigation...
			FillPageList();

			// setting page combo value to the grid's selected page number....
			SetNavigation();

		}

		private DataView PullData()
		{
			// Procedure for fetching data from the database.......
			int ClientType = Convert.ToInt16(ddlClientType.SelectedValue );
			try
			{		
				// Assigning stored procedure's parameter names in an array......
				string[] key    = {"@FromDate","@ToDate","@ClientType"};

				// Assigning stored procedure's parameter values in an array......
				object[] value1 = {dtFrom.SelectedDate ,dtTo.SelectedDate ,ClientType };
				
				// Calling wrapper class function for fetching in data set.........
				ds = ClsDb.Get_DS_BySPArr("usp_HTS_Get_BondReminder",key,value1);	
				dv = new DataView(ds.Tables[0]) ;
				Session["dv"] = dv;

				lblMessage.Text="";

				// saving record count in a global variable....
				recCount=ds.Tables[0].Rows.Count;
				Session["intRecordCount"] = recCount.ToString();

			

				// IF NO RECORD FOUND........
				if (recCount  < 1 )
				{
					lblMessage.Text = "No record found";

					// HIDING THE GRID & PAGE NAVIGATION CONTROLS....
					Panel1.Visible = false;
				}

				// IF RECORD FOUND.......
				else
				{
					lblMessage.Text ="";
					
					// HIDING THE GRID & PAGE NAVIGATION CONTROLS....
					Panel1.Visible = true;
				}
			}
			
			catch(Exception ex)
			{
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
				lblMessage.Text=ex.Message;
			}
			return dv;
		}

		private void FillGrid()
		{
			
			// Setting & binding data sources for data grid......
			dgClient.DataSource=PullData();
			try
			{
				dgClient.DataBind();

			}
			catch (Exception ex)
			{
				
				// Tahir Ahmed... Routing to handle the selected page index.
				if (dgClient.CurrentPageIndex > dgClient.PageCount -1)
				{
					dgClient.CurrentPageIndex=dgClient.PageCount -1;
					dgClient.DataBind();
					dgClient.SelectedIndex = dgClient.Items.Count -1; 
				}
				else
				{
					bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);

					lblMessage.Text = ex.Message.ToString();
				}
			}

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
			this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
			this.cmbPageNo.SelectedIndexChanged += new System.EventHandler(this.cmbPageNo_SelectedIndexChanged);
			this.dgClient.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dgClient_PageIndexChanged);
			this.dgClient.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgClient_ItemDataBound);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void dgClient_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			try
			{
				if (e.Item.ItemType  != ListItemType.Header && e.Item.ItemType != ListItemType.Footer )
				{
				
					// Making client name hyper link to violation fee page.....
					string strTicketId  = ((Label)(e.Item.FindControl("lblTicketId"))).Text ;

					((HyperLink ) ( e.Item.FindControl("hlkClientName"))).NavigateUrl = "../ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + strTicketId ;



					//string[] arrPhone = Convert.ToString(DataBinder.Eval(dgClient, "DataItem.contact")).Split(',');
					string[] arrPhone = ((Label)(e.Item.FindControl("lblContact"))).Text.Split(',');

				
					// SETTING PHONE NUMBERS IN RESPECTIVE FIELDS........
					for (int idx=0; idx < arrPhone.Length ; idx++)
					{
						if ( idx == 0 && arrPhone[idx] != "")
							((Label) (e.Item.FindControl("lblContact1"))).Text = arrPhone[idx];
						else if (idx == 1 && arrPhone[idx] != "" )
							((Label) (e.Item.FindControl("lblContact2"))).Text = arrPhone[idx];
						else if (idx == 2  && arrPhone[idx] != "")
							((Label) (e.Item.FindControl("lblContact3"))).Text = arrPhone[idx];
		
					}
				}
			}
			catch (Exception ex)
			{
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);

			}
		}

		private void btnSubmit_Click(object sender, System.EventArgs e)
		{
			// SETTING THE GRID TO FIRST PAGE.....
			dgClient.CurrentPageIndex = 0;

			// IF THE INPUTS ARE VALID.........
			if (DoValidation()) 
				SearchRecords();
		}
		private bool DoValidation()
		{
			if (dtTo.SelectedDate<dtFrom.SelectedDate)
			{
				lblMessage.Text="From Data should be less than To Date.";
				return false;
			}
			
			return true;


		}

		private void dgClient_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			GotoPage(e.NewPageIndex );
		}

		private void GotoPage(int intNewIndex)
		{
			// Procedure for handling page navigation in data grid......

			// setting current page to new page index in data grid....
			dgClient.CurrentPageIndex = intNewIndex;
			
			// Filling & binding data for new page index with data grid......
			SearchRecords();

		}

		private void cmbPageNo_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			GotoPage(cmbPageNo.SelectedIndex );
			SearchRecords();
		}

		private void btnReset_Click(object sender, System.EventArgs e)
		{
			// SETTING THE SELCTION CRITERIA CONTROLS TO THE DEFAULTS.....
			ClearSearch();
		}

		private void ClearSearch()
		{
			dtFrom.Clear ();
			dtTo.Clear ();

			dtFrom.Reset(DateTime.Now.Date);
			dtTo.Reset (DateTime.Now.Date);
			
			ddlClientType.SelectedIndex = 0;
			Panel1.Visible = false;

		}
	}
}

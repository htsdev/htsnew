﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;

namespace HTP.Reports
{
    ///<summary>
    ///</summary>
    public partial class SmsClientRepliesReport : Page
    {
        #region Variables
        faxclass _fclass = new faxclass();
        clsENationWebComponents ClsDb = new clsENationWebComponents();

        #endregion

        #region events
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    Session["DV_smsreplyreport"] = null;
                    
                    //if (!ClientScript.IsStartupScriptRegistered("javascript"))
                    //{
                    //    string script = "<script>var fromdate = document.getElementById('fromdate'); " +
                    //            " var todate = document.getElementById('todate'); " +
                    //            " fromdate.disabled = true; " +
                    //            " todate.disabled = true; </script>";

                    //    ClientScript.RegisterStartupScript(typeof(Page), "javascript", script);
                    //}
                    dd_SmsOutCome.SelectedIndex = 1;
                }
                else
                {
                    Bindgrid();
                }
                Pagingctrl.PageIndexChanged += Pagingctrl_PageIndexChanged;
                Pagingctrl.PageSizeChanged += Pagingctrl_PageSizeChanged;
                Pagingctrl.GridView = gv_records;

            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void gv_records_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            
        }

        protected void gv_records_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ((HyperLink) (e.Row.FindControl("lnk_Comments"))).NavigateUrl =
                        "javascript:window.open('smsnotes.aspx?casenumber=" +
                        ((Label) (e.Row.FindControl("lbl_TicketID"))).Text + "&violationID=" +
                        ((Label) (e.Row.FindControl("lbl_TicketViolationID"))).Text + "&searchdate=" +
                        ((Label) (e.Row.FindControl("lbl_CourtDate"))).Text +
                        "&calltype=" + ((Label)(e.Row.FindControl("lbl_SmsTextType"))).Text + "&Recid=" + ((HyperLink)(e.Row.FindControl("hl_Sno"))).Text +
                        "','','status=yes,width=530,height=410,scrollbars=yes');void('');";
                    
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void btn_faxsearch_Click(object sender, EventArgs e)
        {
            try
            {
                Bindgrid();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void gv_records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (e.NewPageIndex != -1)
                {
                    gv_records.PageIndex = e.NewPageIndex;
                    Bindgrid();
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }        

        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            if (pageSize > 0)
            {
                gv_records.PageIndex = 0;
                gv_records.PageSize = pageSize;
                gv_records.AllowPaging = true;
            }
            else
            {
                gv_records.AllowPaging = false;
            }
            Bindgrid();
        }

        #endregion

        #region Methods

        void Pagingctrl_PageIndexChanged()
        {
            gv_records.PageIndex = Pagingctrl.PageIndex - 1;
            Bindgrid();
        }

        protected void Bindgrid()
        {
            ViewState["smsoutcome"] = dd_SmsOutCome.SelectedValue;
            ViewState["fromdate"] = fromdate.SelectedDate;
            ViewState["todate"] = todate.SelectedDate;

            String[] parameters = {"@fromreceiveddate", "@toreceiveddate", "@smsoutcome"};
            object[] values = {
                                  Convert.ToDateTime(ViewState["fromdate"].ToString()),
                                  Convert.ToDateTime(ViewState["todate"].ToString()),
                                  Convert.ToInt32(ViewState["smsoutcome"].ToString())
                              };

            DataTable dt = ClsDb.Get_DS_BySPArr("USP_HTP_GET_SMSClientReplies", parameters, values).Tables[0];

            if (dt.Rows.Count > 0)
            {
                lbl_Message.Visible = false;
                gv_records.Visible = true;                                                

                int sno = 1;
                if (!dt.Columns.Contains("sno"))
                    dt.Columns.Add("sno");


                if (dt.Rows.Count >= 1)
                    dt.Rows[0]["sno"] = 1;

                if (dt.Rows.Count >= 2)
                {
                    for (int i = 1; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[i - 1]["TicketId"].ToString() != dt.Rows[i]["TicketId"].ToString())
                        {
                            dt.Rows[i]["sno"] = ++sno;
                        }
                    }
                }

                
                gv_records.DataSource = dt.DefaultView;
                gv_records.DataBind();

                Pagingctrl.PageCount = gv_records.PageCount;
                Pagingctrl.PageIndex = gv_records.PageIndex;
                Pagingctrl.SetPageIndex();
            }
            else
            {
                lbl_Message.Visible = true;
                gv_records.Visible = false;
                lbl_Message.Text = "No Records Found";
            }
        }

        #endregion
    }
}
﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;
using lntechNew.WebControls;

namespace HTP.Reports
{
    ///<summary>
    /// CurrentClientCourtDateChecker class
    ///</summary>
    public partial class CurrentClientCourtDateChecker : Page
    {
        // Abbas Qamar 9332 05/04/2011  grouping variables
        #region Variables

        readonly clsSession _clsSession = new clsSession();
        DataSet _ds;
        readonly ValidationReports _reports = new ValidationReports();

        #endregion
    
        // Abbas Qamar 9332 07/12/2011  grouping events
        #region Events

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent()
        {
            // Babar Ahmad 10/18/2011 Enabled paging control functionality.
            PagingControl.grdType = lntechNew.WebControls.GridType.DataGrid;
            dg_records.ItemCommand += dg_records_ItemCommand;
            dg_records.PageIndexChanged += dg_records_PageIndexChanged;
            dg_records.ItemDataBound += dg_records_ItemDataBound;
            // Babar Ahmad 9223 08/23/2011 Commented code due avoid repeated calls of Page_Load().
            Load += Page_Load; 

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!_clsSession.IsValidSession(Request, Response, Session))
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else
                {
                    if (!IsPostBack)
                    {
                        FillGrid();
                    }
                
                UpdateViolationSRV1.PageMethod += UpdateViolationSRV1_PageMethod;
                Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);//Pagingctrl_PageIndexChanged;
                Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);//Pagingctrl_PageSizeChanged;
                Pagingctrl.DataGrid = dg_records;
                }
            }

            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void dg_records_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            try
            {
                if (e.NewPageIndex != -1)
                {
                    dg_records.CurrentPageIndex = e.NewPageIndex;
                    FillGrid();
                }
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void dg_records_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "NoChangeClick")
                {
                    int ticketviolationId = Convert.ToInt32(e.CommandArgument);
                    HiddenField ticketId = (HiddenField)e.Item.FindControl("hf_ticketid");
                    _reports.UpdateNoChangeInCourtSettingFlag(Convert.ToInt32(ticketId.Value), ticketviolationId);
                    FillGrid();
                }
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.ToString();
                lbl_message.Visible = true;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void dg_records_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            DataRowView drv = (DataRowView)e.Item.DataItem;
            if (drv == null)
                return;
            try
            {
                LinkButton lnkBtn = (LinkButton)e.Item.FindControl("lnkb_manverify");
                lnkBtn.Attributes.Add("onClick", "return showpopup('" + UpdateViolationSRV1.ClientID + "','" + e.Item.ClientID + "',0);");
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.ToString();
                lbl_message.Visible = true;
                clsLogger.ErrorLog(ex);
            }
        }

        #endregion
        
        // Abbas Qamar 9332 07/12/2011  grouping Methods
        #region Methods

        /// <summary>
        /// Abbas Qamar 9332 07/12/2011
        /// Mthod to maintain the view or records of the page after update the Violations
        /// </summary>
        void UpdateViolationSRV1_PageMethod()
        {
            lbl_message.Text = UpdateViolationSRV1.ErrorMessage;
            FillGrid();
        }

        /// <summary>
        /// Abbas Qamar 9233 07/12/2011
        /// Maintain the Page index for Paging Control
        /// </summary>
        void Pagingctrl_PageIndexChanged()
        {
            // Babar Ahmad 10/18/2011 Enabled paging control functionality and changed datagrid name.

            try
            {
                dg_records.CurrentPageIndex = Pagingctrl.PageIndex - 1;  // set page index to grid.
                FillGrid();
                
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                lbl_message.Text = ex.ToString();
            }
            
        }
        
        /// <summary>
        /// Abbas Qamar 9233 07/12/2011
        /// Maintain the Page size for Paging Control
        /// </summary>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            if (pageSize > 0)
            {
                dg_records.CurrentPageIndex = 0;
                dg_records.PageSize = pageSize;
                dg_records.AllowPaging = true;
            }
            else
            {
                dg_records.AllowPaging = false;
            }
            FillGrid();
        }
        
        /// <summary>
        /// Abbas Qamar 9233 07/12/2011
        /// Fill the Grid 
        /// </summary>
        private void FillGrid()
        {
            try
            {
                tbl_plzwait1.Style.Clear();
                tbl_plzwait1.Style.Add(HtmlTextWriterStyle.Display, "none");
                lbl_message.Text = string.Empty;
                _reports.getRecords("USP_HTP_Get_CurrentClientCourtDateChecker");
                _ds = _reports.records;
                if (_ds.Tables[0].Rows.Count > 0)
                {
                    _ds.Tables[0].Columns.Add("sno");
                    DataView dv = _ds.Tables[0].DefaultView;
                    DataTable dt = dv.ToTable();
                    GenerateSerialNo(dt);

                    Pagingctrl.DataGrid = dg_records;
                    dg_records.DataSource = dt;
                    dg_records.DataBind();
                    Pagingctrl.PageCount = dg_records.PageCount;
                    Pagingctrl.PageIndex = dg_records.CurrentPageIndex;
                    Pagingctrl.SetPageIndex();
                }
                else
                {
                    Pagingctrl.PageCount = 0;
                    Pagingctrl.PageIndex = 0;
                    dg_records.DataSource = null;
                    dg_records.DataBind();
                    lbl_message.Text = "No Records Found";
                }
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Abbas Qamar 9233 07/12/2011
        /// Method To Generate Serial No on the basis of TicketId
        /// </summary>
        public void GenerateSerialNo(DataTable dtRecords)
        {
            int sno = 1;
            if (!dtRecords.Columns.Contains("sno"))
                dtRecords.Columns.Add("sno");

            if (dtRecords.Rows.Count >= 1)
                dtRecords.Rows[0]["sno"] = 1;

            if (dtRecords.Rows.Count >= 2)
            {
                for (int i = 1; i < dtRecords.Rows.Count; i++)
                {
                    if (dtRecords.Rows[i - 1]["ticketid_pk"].ToString() != dtRecords.Rows[i]["ticketid_pk"].ToString())
                    {
                        dtRecords.Rows[i]["sno"] = ++sno;
                    }
                }
            }
        }

        #endregion
    }
}

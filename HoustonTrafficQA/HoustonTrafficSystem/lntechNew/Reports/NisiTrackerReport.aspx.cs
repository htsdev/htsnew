﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Diagnostics;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;
using HTP.Components;
using lntechNew.WebControls;
using System.Text;



namespace HTP.Reports
{
    public partial class NisiTrackerReport : System.Web.UI.Page
    {
        #region Variables
        clsSession ClsSession = new clsSession();
        clsCase cCase = new clsCase();
        DataTable dt = new DataTable();
        ClsNisiCaseHandler _nisi = new ClsNisiCaseHandler();
        #endregion

        #region Properties
        
        private string GridViewSortExpression
        {
            get
            {
                if (ViewState["sortExpression"] == null)
                    ViewState["sortExpression"] = "";
                return ViewState["sortExpression"].ToString();
            }
            set { ViewState["sortExpression"] = value; }

        }
        private SortDirection GridViewSortDirection
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Ascending;
                return (SortDirection)ViewState["sortDirection"];
            }
            set { ViewState["sortDirection"] = value; }

        }

        public string sortOrder
        {
            get
            {
                if (ViewState["sortOrder"].ToString() == "desc")
                { ViewState["sortOrder"] = "asc"; }
                else
                {
                    ViewState["sortOrder"] = "desc";
                }
                return ViewState["sortOrder"].ToString();
            }
            set { ViewState["sortOrder"] = value; }
        }

        #endregion

        // Event Handlers region
        #region Event Handler
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                int Empid = 0;

                // Checking employee info in session               
                if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false ||
                        (!int.TryParse(ClsSession.GetCookie("sEmpID", this.Request), out Empid)))
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else
                {
                    Session["EmployeeId"] = Empid.ToString();
                    if (!IsPostBack)
                    {
                        ddl_nisiStatus.Items.Add(new ListItem("Active", "1"));
                        ddl_nisiStatus.Items.Add(new ListItem("Closed", "0"));
                        ddl_nisiStatus.SelectedValue = "1";

                        ddl_UnderlyingViolationsStatus.Items.Add(new ListItem("Pending", "Pending"));
                        ddl_UnderlyingViolationsStatus.Items.Add(new ListItem("Rebonded/Plea Entered", "Rebonded"));
                        ddl_UnderlyingViolationsStatus.Items.Add(new ListItem("All", "All"));
                        ddl_UnderlyingViolationsStatus.SelectedValue = "3";
                        td_printAnswers.Style["display"] = "none";
                        if (Session["NisiData"] != null)
                            Session["NisiData"] = null;
                        ViewState["sortOrder"] = ""; bindGridView("", "");
                        ViewState["empid"] = ClsSession.GetCookie("sEmpID", this.Request).ToString();
                    }
                    UpdateFollowUpInfo2.PageMethod += new HTP.WebControls.PageMethodHandler(UpdateFollowUpInfo2_PageMethod);
                    Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
                    Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
                    Pagingctrl.GridView = gv_Records;
                    ViewState["empid"] = Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request));
                    Pagingctrl.Visible = true;


                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Gridview Sorting event.
        /// </summary>
        /// <param name="sortExp"></param>
        /// <param name="sortDir"></param>

        public void bindGridView(string sortExp, string sortDir)
        {
            if (Session["NisiData"] != null)
            {
                DataView myDataView = Session["NisiData"] as DataView;

                if (sortExp != string.Empty)
                {
                    myDataView.Sort = string.Format("{0} {1}", sortExp, sortDir);
                }
                DataTable tbl = myDataView.ToTable();
                tbl.Columns.Remove("SNo");
                Session["NisiData"] = tbl.DefaultView;
                FillSortedGrid();
            }
        }
        protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
        {
            bindGridView(e.SortExpression, sortOrder);
        }


        protected void gv_Records_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    for (int i = 0; i < e.Row.Cells.Count; i++)
                    {
                        e.Row.Cells[i].Attributes.Add("style", "border:1px solid #000000");
                    }
                    e.Row.Cells[21].Visible = false;
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    for(int i =0; i < e.Row.Cells.Count; i++)
                    {
                        e.Row.Cells[i].Attributes.Add("style", "border:1px solid #000000");
                    }
                    
                    Label desc = (Label)e.Row.FindControl("LblLastName");
                    if (desc.Text.Length > 10)
                    {
                        desc.ToolTip = desc.Text;
                        desc.Text = desc.Text.Substring(0, 10).ToUpperInvariant() + "...";
                    }
                    Label desce = (Label)e.Row.FindControl("LblFirstName");

                    if (desce.Text.Length > 10)
                    {
                        desce.ToolTip = desce.Text;
                        desce.Text = desce.Text.Substring(0, 10).ToUpperInvariant() + "...";
                    }

                    Label lblViolation = (Label)e.Row.FindControl("LblViolationDescription");
                    if (lblViolation.Text.Length > 23)
                    {
                        lblViolation.ToolTip = lblViolation.Text;
                        lblViolation.Text = lblViolation.Text.Substring(0, 23).ToUpperInvariant() + "...";
                    }

                    // Formatting Amount. . . . 
                    string amount = ((Label) e.Row.FindControl("LblBondAmount")).Text;
                    ((Label)e.Row.FindControl("LblBondAmount")).Text = String.Format("{0:0.00}", Convert.ToDecimal(amount));


                    LinkButton lnkBtnPrintAnswer = (LinkButton)e.Row.FindControl("LnkBtnPrintAnswer");
                    if ((lnkBtnPrintAnswer.Text.Length > 0) && (! lnkBtnPrintAnswer.Text.Equals("01/01/1900")) && (!(lnkBtnPrintAnswer.Text.Equals("*****")))) // If not empty and Print Answer Date is Available
                    {
                        lnkBtnPrintAnswer.CommandName = "PrintAnswerAgain";
                    }
                    if (lnkBtnPrintAnswer.Text.Equals("01/01/1900"))
                        lnkBtnPrintAnswer.Visible = false;

                    HyperLink lnkBtnEmail = (HyperLink)e.Row.FindControl("HyprlnkEmail");
                    if (lnkBtnEmail.Text.Length > 0) // If not empty and Print Answer Date is Available
                    {
                        if (lnkBtnEmail.Text.Equals("*****") || lnkBtnEmail.Text.Equals("N/A"))
                        {
                            lnkBtnEmail.Visible = false;
                            if (lnkBtnEmail.Text.Equals("N/A")) 
                                ((Label) e.Row.FindControl("LblBtnEmail")).Text = "N/A";
                            ((Label) e.Row.FindControl("LblBtnEmail")).Visible = true;
                        }
                    }

                    if (((Label)e.Row.FindControl("LblFollowUpDate")).Text.Equals("01/01/1900"))
                        ((Label)e.Row.FindControl("LblFollowUpDate")).Text = "N/A";

                    LinkButton lnkImg = (LinkButton) e.Row.FindControl("ImgAdd");
                    if (lnkImg.Text.Length > 0)
                    {
                        if (lnkImg.Text.Equals("*****"))
                        {
                            lnkImg.Visible = false;
                            ((Label) e.Row.FindControl("LblImgAdd")).Visible = true;
                        }
                    }

                    TextBox tx = (TextBox) e.Row.FindControl("TxtNotes");
                    if (tx.Text.Equals("*****"))
                    {
                        tx.Visible = false;
                        ((Label) e.Row.FindControl("LblNotes")).Visible = true;
                    }

                    e.Row.Cells[21].Visible = false; // Hidding empty colum which appear in grid during border

                    ((LinkButton)e.Row.FindControl("ImgAdd")).CommandArgument = e.Row.RowIndex.ToString();
                    ((LinkButton)e.Row.FindControl("LnkBtnPrintAnswer")).CommandArgument = e.Row.RowIndex.ToString();
                    //((LinkButton)e.Row.FindControl("LnkBtnEmail")).CommandArgument = e.Row.RowIndex.ToString();
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }
        }

        protected void gv_Records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                //if (Session["SortExpression"] != null && Session["SortDirection"] != null)
                //{
                if (Session["NisiData"] != null)
                {
                    FillSortedGrid();
                }
                //}
                else
                {
                    FillGrid();
                }
                gv_Records.PageIndex = e.NewPageIndex;
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }
        }

        protected void gv_Records_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "btnRemove")
                {
                    ViewState["Ticketid"] = Convert.ToInt32(e.CommandArgument);
                    Pagingctrl.Visible = true;
                }
                else if (e.CommandName == "AddFollowupDate")
                {
                    int RowID = Convert.ToInt32(e.CommandArgument);
                    string firstName = (((HiddenField)gv_Records.Rows[RowID].FindControl("HfFname")).Value);
                    string lastName = (((HiddenField)gv_Records.Rows[RowID].FindControl("HfLname")).Value);
                    string followupDate = (((Label)gv_Records.Rows[RowID].FindControl("LblFollowUpDate")).Text);
                    string nisiCause = (((HiddenField)gv_Records.Rows[RowID].FindControl("HfNisiCauseno")).Value);
                    string underlyingTicketNum = (((HiddenField)gv_Records.Rows[RowID].FindControl("HfUnderlyingCause")).Value);
                    string contactTypeId = (((HiddenField)gv_Records.Rows[RowID].FindControl("HfContactType")).Value);
                    string nisiGeneratedDate = (((HiddenField)gv_Records.Rows[RowID].FindControl("HfNisiGeneratedDate")).Value);
                    bool isFreezed = Convert.ToBoolean((((HiddenField)gv_Records.Rows[RowID].FindControl("HfFollowUpDateFreezed")).Value));
                    cCase.TicketID = Convert.ToInt32((((HiddenField)gv_Records.Rows[RowID].FindControl("HfTicketId")).Value));
                    int empId = Convert.ToInt32(ViewState["empid"]);
                    string comm = cCase.GetGeneralCommentsByTicketId();
                    string multiCause = string.Empty;
                    string multiunderlyingCause = string.Empty;
                    foreach (GridViewRow row in gv_Records.Rows)
                    {
                        if (Convert.ToInt32(((HiddenField) row.Cells[21].FindControl("HfTicketId")).Value) == cCase.TicketID)
                        {
                            multiCause += ((HiddenField)row.Cells[21].FindControl("HfNisiCauseno")).Value + ",";
                            multiunderlyingCause += ((Label)row.FindControl("LblUnderlyingViolationCase")).Text + ",";
                            string test = string.Empty;
                        }
                    }

                    UpdateFollowUpInfo2.Freezecalender = true;
                    UpdateFollowUpInfo2.followUpType = FollowUpType.NisiFollowUpDate;
                    UpdateFollowUpInfo2.CallPopUp(firstName, lastName, underlyingTicketNum, cCase.TicketID.ToString(),
                                                  nisiCause, comm, mpeTrafficwaiting.ClientID, followupDate, contactTypeId, nisiGeneratedDate, empId, isFreezed, multiCause, multiunderlyingCause);
                    mpeTrafficwaiting.Show();
                    Pagingctrl.Visible = true;

                }

                if (e.CommandName == "PrintAnswerAgain")
                {
                    int RowID = Convert.ToInt32(e.CommandArgument);
                    string clientrowId = (((Label)gv_Records.Rows[RowID].FindControl("LblRowId")).Text + ",");

                    DataSet ds = _nisi.GetSelectClientsToPrintLetters(clientrowId, Convert.ToInt32(Session["EmployeeId"]), false, Session.SessionID);
                    Session["PrintAnswerDS"] = ds;
                    Session["printAnswers"] = clientrowId + ",";
                    Session["newClients"] = "false";
                    //ScriptManager.RegisterStartupScript(upnlResult, upnlResult.GetType(), "opnewin", "window.open('../Reports/PreviewNisiLetters.aspx',''); void(0);", true);
                    ScriptManager.RegisterStartupScript(this.upnlResult, upnlResult.GetType(), "newWindow", "window.open('../Reports/frmNisiLetters.aspx','','fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=no');;", true);
                    //FillGrid();
                }
                if (e.CommandName == "SendEmailToClient")
                {
                    int RowID = Convert.ToInt32(e.CommandArgument);
                    string clientrowId = (((Label)gv_Records.Rows[RowID].FindControl("LblRowId")).Text);
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }

        }

        void Pagingctrl_PageIndexChanged()
        {

            gv_Records.PageIndex = Pagingctrl.PageIndex - 1;
            //FillGrid();
            FillSortedGrid();
        }

        /// <summary>
        /// Page Size change event handler
        /// </summary>
        /// <param name="pageSize"></param>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            if (pageSize > 0)
            {
                gv_Records.PageIndex = 0;
                gv_Records.PageSize = pageSize;
                gv_Records.AllowPaging = true;
            }
            else
            {
                gv_Records.AllowPaging = false;
            }
            //FillGrid();
            FillSortedGrid();
        }

        /// <summary>
        /// Event handler to filter report according to criteria
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_Submit_Click(object sender, EventArgs e)
        {
            try
            {
                if (lblMessage.Text.Length > 0)
                    lblMessage.Text = "";
                gv_Records.PageIndex = 0;
                FillGrid();
                DataView dv = new DataView(dt);
                Session["NisiData"] = dv;
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }

        }

        protected void Btn_PrintAnswers_Click(object sender, EventArgs e)
        {
            string rowIds = "";
            try
            {
                foreach (GridViewRow row in gv_Records.Rows)
                {
                    if (((System.Web.UI.WebControls.CheckBox)row.FindControl("ChkPrintAnser")).Checked)
                        rowIds += ((Label)row.FindControl("LblRowId")).Text + ",";
                }

                Session["printAnswers"] = rowIds;
                Session["newClients"] = "true";
                Session["ContactType"] = "0";

                DataSet ds = _nisi.GetSelectClientsToPrintLetters(rowIds, Convert.ToInt32(Session["EmployeeId"]), true, Session.SessionID);
                Session["PrintAnswerDS"] = ds;
                Response.Write("<script language=JavaScript>window.open('frmNisiLetters.aspx');</script>");

                ResetControls();
                FillGrid();              
                
                //gv_Records.Visible = false;
                //td_printAnswers.Style["display"] = "none";
                //Pagingctrl.Visible = false;
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }
        }

        #endregion

        #region Methods

        private void ResetControls ()
        {
            try
            {
                // clearing header checkbox
                if (((CheckBox)gv_Records.HeaderRow.FindControl("ChkPrintAnswerHeader")).Checked)
                    ((CheckBox) gv_Records.HeaderRow.FindControl("ChkPrintAnswerHeader")).Checked = false;
                foreach (GridViewRow row in gv_Records.Rows)
                {
                    // Clearing all the checkboxs
                    if (((CheckBox)row.FindControl("ChkPrintAnser")).Checked)
                        ((CheckBox) row.FindControl("ChkPrintAnser")).Checked = false;
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Used to Fill the Grid
        /// </summary>
        private void FillGrid()
        {
            try
            {
                dt = _nisi.GetNisiDataForReport(Convert.ToInt32(ddl_nisiStatus.SelectedValue), ddl_UnderlyingViolationsStatus.SelectedValue);
                if (dt.Rows.Count == 0)
                {
                    gv_Records.Visible = false;
                    lblMessage.Text = "No reocrds Found";
                    td_printAnswers.Style["display"] = "none";
                }
                else
                {                
                    if (dt.Columns.Contains("SNo") == false)
                    {
                        dt.Columns.Add("SNo");
                        int sno = 1;
                        if (dt.Rows.Count >= 1)
                            dt.Rows[0]["SNo"] = 1;
                        if (dt.Rows.Count >= 2)
                        {
                            for (int i = 1; i < dt.Rows.Count; i++)
                            {
                                if (dt.Rows[i - 1]["ticketid"].ToString() != dt.Rows[i]["ticketid"].ToString())
                                {
                                    dt.Rows[i]["SNo"] = ++sno;
                                }
                            }
                        }

                    }
                    gv_Records.Visible = true;
                    gv_Records.DataSource = dt.DefaultView;
                    gv_Records.DataBind();

                    Pagingctrl.PageCount = gv_Records.PageCount;
                    Pagingctrl.PageIndex = gv_Records.PageIndex;
                    Pagingctrl.SetPageIndex();
                    td_printAnswers.Style["display"] = "block";
                }                   
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }


        private void FillSortedGrid()
        {
            try
            {
                if (Session["NisiData"] != null)
                {
                    DataView dv = Session["NisiData"] as DataView;
                    DataTable table = dv.ToTable();
                    if (table.Rows.Count == 0)
                    {
                        gv_Records.Visible = false;
                        lblMessage.Text = "No reocrds Found";
                        td_printAnswers.Style["display"] = "none";
                    }
                    else
                    {
                        if (table.Columns.Contains("SNo") == false)
                        {
                            table.Columns.Add("SNo");
                            int sno = 1;
                            if (table.Rows.Count >= 1)
                                table.Rows[0]["SNo"] = 1;
                            if (table.Rows.Count >= 2)
                            {
                                for (int i = 1; i < table.Rows.Count; i++)
                                {
                                    if (table.Rows[i - 1]["ticketid"].ToString() != table.Rows[i]["ticketid"].ToString())
                                    {
                                        table.Rows[i]["SNo"] = ++sno;
                                    }
                                }
                            }
                            Session["NisiData"] = table.DefaultView;
                        }
                        gv_Records.Visible = true;
                        gv_Records.DataSource = table.DefaultView;
                        gv_Records.DataBind();

                        Pagingctrl.PageCount = gv_Records.PageCount;
                        Pagingctrl.PageIndex = gv_Records.PageIndex;
                        Pagingctrl.SetPageIndex();
                        td_printAnswers.Style["display"] = "block";
                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        void UpdateFollowUpInfo2_PageMethod()
        {
            FillGrid();
        }
        #endregion
    }
}

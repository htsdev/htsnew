﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PastDueCallsTraffic.aspx.cs"
    Inherits="HTP.Reports.PastDueCallsTraffic" %>

<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Traffic Past Due Calls</title>
    <link href="../Styles.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <aspnew:ScriptManager ID="ScriptManager1" runat="server">
            <Scripts>
                <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
            </Scripts>
        </aspnew:ScriptManager>
        <aspnew:UpdatePanel ID="pnl_main" runat="server">
            <ContentTemplate>
                <table id="TableMain" align="center" border="0" cellpadding="0" cellspacing="0" style="z-index: 101"
                    width="780">
                    <tbody>
                        <tr>
                            <td colspan="4" style="height: 14px">
                                <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td background="../images/separator_repeat.gif" height="11">
                            </td>
                        </tr>
                        <tr>
                            <td background="../Images/subhead_bg.gif" align="left" valign="middle" style="height: 34px">
                                <table width="100%">
                                    <tr>
                                        <td align="left" class="clssubhead">
                                            Traffic Past Due Calls
                                        </td>
                                        <td align="right" class="clssubhead">
                                            <aspnew:UpdatePanel ID="updatepnlpaging" runat="server" RenderMode="Inline">
                                                <ContentTemplate>
                                                    <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                                </ContentTemplate>
                                            </aspnew:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td background="../images/separator_repeat.gif" height="11">
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Label ID="lblMessage" runat="server" CssClass="Label" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="pnl_main">
                                    <ProgressTemplate>
                                        <img alt="" src="../images/plzwait.gif" />&nbsp;<asp:Label ID="lbl1" runat="server"
                                            CssClass="clssubhead" Text="Please Wait ......"></asp:Label>
                                    </ProgressTemplate>
                                </aspnew:UpdateProgress>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:GridView ID="gv_records" runat="server" AutoGenerateColumns="False" Width="800px"
                                    AllowPaging="True" AllowSorting="True" OnPageIndexChanging="gv_records_PageIndexChanging"
                                    CellPadding="3" PageSize="20" CssClass="clsLeftPaddingTable" OnSorting="gv_records_Sorting">
                                    <Columns>
                                        <asp:TemplateField HeaderText="S#">
                                            <ItemTemplate>
                                                <asp:HyperLink ID="hl_Sno" CssClass="clssubhead" runat="server" Text='<%# Eval("SNo") %>'
                                                    NavigateUrl='<%# "../clientinfo/ViolationFeeold.aspx?casenumber=" + Eval("TicketID_PK")+"&search=0" %>'></asp:HyperLink>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<u>Client</u>" SortExpression="ClientName">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCli" runat="server" Text='<%# Bind("ClientName", "{0:d}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="clssubhead" Font-Underline="True" HorizontalAlign="Center" />
                                            <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<u>Crt</u>" SortExpression="crt">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCrt" runat="server" Text='<%# Bind("crt", "{0:d}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="clssubhead" Font-Underline="True" HorizontalAlign="Center" />
                                            <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<u>Crt Type</u>" SortExpression="CrtType">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCrtType" runat="server" Text='<%# Bind("CrtType", "{0:d}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                            <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<u>Crt Date</u>" SortExpression="CourtDate">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCourtDate" runat="server" Text='<%# Eval("CourtDate", "{0:MM/dd/yyyy}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="clssubhead" Font-Underline="True" HorizontalAlign="Center" />
                                            <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<u>Total Fee</u>" SortExpression="TotalFeeCharged">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTotalFeeCharged" runat="server" Text='<%# Bind("TotalFeeCharged", "{0:C0}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="clssubhead" Font-Underline="True" HorizontalAlign="Center" />
                                            <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<u>Amt Due</u>" SortExpression="Owes">
                                            <ItemTemplate>
                                                <asp:Label ID="lblOwes" runat="server" Text='<%# Bind("Owes", "{0:C0}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="clssubhead" Font-Underline="True" HorizontalAlign="Center" />
                                            <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<u>Payment Due</u>" SortExpression="PaymentDate">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPaymentDate" runat="server" Text='<%# Eval("PaymentDate", "{0:MM/dd/yyyy}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                            <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<u>Follow up</u>" SortExpression="PaymentFollowUpdate">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPaymentFollowUpdate" runat="server" Text='<%# Eval("PaymentFollowUpdate", "{0:MM/dd/yyyy}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                            <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<u>Past Days</u>" SortExpression="Pastdays">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPastdays" runat="server" Text='<%# Bind("Pastdays") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                            <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Center" />
                                    <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                        FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td width="100%" background="../Images/separator_repeat.gif" height="11">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <uc1:Footer ID="Footer1" runat="server" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </ContentTemplate>
        </aspnew:UpdatePanel>
    </div>
    </form>
</body>
</html>

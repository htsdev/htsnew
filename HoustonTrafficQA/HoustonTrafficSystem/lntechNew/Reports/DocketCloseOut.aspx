<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DocketCloseOut.aspx.cs" Inherits="lntechNew.Reports.DocketCloseOut" %>

<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc2" %>

<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc1" %>

<%@ Register Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2"
    Namespace="eWorld.UI" TagPrefix="ew" %>

<!--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Docket Close Out</title>
    <link  href="../Styles.css" type ="text/css" rel="stylesheet" />
       <%=Session["objTwain"].ToString()%>
    
    
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />

   
    
    
</head>

<body onload="EnableFeeder()">

    <form id="form1" runat="server">
    <div>
        <table cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
            <tr>
                <td>
                    <uc1:ActiveMenu ID="ActiveMenu1" runat="server" />
                </td>
            </tr>
            <tr>
                <td width="100%" background="../Images/separator_repeat.gif"  height="11"  >
                    </td>
            </tr>
            
            <tr>
                <td style="height: 35px" >
                    <ew:CalendarPopup ID="cal_Date" runat="server" ControlDisplay="TextBoxImage" ImageUrl="~/Images/calendar.gif" Width="86px">
                        <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                            Font-Size="XX-Small" ForeColor="Black" />
                        <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black" />
                        <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black" />
                        <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black" />
                        <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                            Font-Size="XX-Small" ForeColor="Black" />
                        <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black" />
                        <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black" />
                        <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                            Font-Size="XX-Small" ForeColor="Gray" />
                        <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black" />
                        <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                            Font-Size="XX-Small" ForeColor="Black" />
                    </ew:CalendarPopup>
                    &nbsp;&nbsp;
                    <asp:Button ID="btn_Submit" runat="server" OnClick="btn_Submit_Click" Text="Submit" CssClass="clsbutton" /></td>
            </tr>
            <tr>
                <td>
                    &nbsp;<table id="tbl_Main" style="width: 100%" runat="server" >
                        <tr>
                            <td background="../Images/subhead_bg.gif" height="34" class ="clssubhead" >
                    <asp:CheckBox ID="chk_SelectAll" runat="server" onClick="SelectAll();" />Docket Close Out</td>
                        </tr>
                        <tr>
                              <td>
                    <asp:GridView ID="gv_Details" runat="server" AutoGenerateColumns="False" CssClass="clsleftpaddingtable" Width="100%" OnRowDataBound="gv_Details_RowDataBound" OnRowCommand="gv_Details_RowCommand">
                        <Columns>
                            <asp:TemplateField HeaderText="chk_Select">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chk_Select" runat="server" style="display :block" />
                                    <asp:Label ID="lbl_TickedID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TicketID_PK") %>'
                                        Width="0px"></asp:Label>
                                    <asp:Label ID="lbl_TicketsViolationID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TicketsViolaionID") %>'
                                        Width="0px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Record_Count">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_SerialNo1" runat="server" CssClass="label" ></asp:Label>
                                    <asp:HyperLink ID="lbl_SerialNo" runat="server">HyperLink</asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Last Name">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_ClientCount" runat="server" CssClass="label" ForeColor="Blue" Text='<%# DataBinder.Eval(Container, "DataItem.ClientCount") %>'></asp:Label>
                                    <asp:Label ID="lbl_LastName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LastName") %>' CssClass="label"></asp:Label>
                                    <asp:HyperLink ID="hlnk_LastName" runat="server" NavigateUrl='#'
                                        Text='<%# DataBinder.Eval(Container, "DataItem.LastName") %>' Font-Underline="False"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="First Name">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_FirstName" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.FirstName") %>'></asp:Label>
                                    <asp:Label ID="lbl_MiddleName" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.MiddleName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="BondFlag">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_BondStatus" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.BondFlag") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CourtDate">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_CourtDate" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain","{0:d}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CourtDateOld">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_CourtDateOld" runat="server" CssClass="label"
                                        Text='<%# DataBinder.Eval(Container, "DataItem.OldCourtDate","{0:d}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CourtTime">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_CourtTime" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain","{0:t}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CourtTimeOld">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_CourtTimeOld" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.OldCourtDate","{0:t}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Location">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_CourtLoc" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.CourtNumberMain") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="LocationOld">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_CourtLocOld" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.OldCourtNumberMain") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Status">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_ViolationStatus" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.ShortDescription") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="StatusOld">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_ViolationStatusOld" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.OldShortDescription") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Comments">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_Comments" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.TrialComments") %>'></asp:Label>
                                    <asp:Label ID="lbl_CourtID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CourtID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Scan">
                                <ItemTemplate>
                                    <asp:ImageButton ID="ibtn_Scan" runat="server" ImageUrl="~/Images/preview.gif" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.ImagePath") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
                        </tr>
                        <tr>
                            <td width="100%" background="../Images/separator_repeat.gif" colSpan="5" height="11">
                </td>
                        </tr>
              <tr>
                <td style="height:auto">
                    <table width="100%" >
                        <tr>
                            <td width="10%">
                    <asp:DropDownList ID="ddl_Options" runat="server" CssClass="clsinputcombo" onClick="EnableFeeder()" >
                        <asp:ListItem>&lt; Choose &gt;</asp:ListItem>
                        <asp:ListItem Value="80">Disposed</asp:ListItem>
                        <asp:ListItem Value="37">Warrant</asp:ListItem>
                        <asp:ListItem Value="0">----------</asp:ListItem>
                        <asp:ListItem>Upload Docket</asp:ListItem>
                        <asp:ListItem>Remove Docket</asp:ListItem>
                    </asp:DropDownList></td>
                            <td width="5%">
                    <asp:Button ID="btn_DDL_Update" runat="server" OnClick="btn_DDL_Update_Click" Text="Update" CssClass="clsbutton" OnClientClick="return ValidateForm()" /></td>
                            <td >
                                <table id="tbl_feeder">
                                    <tr>
                                        <td >
                                            <asp:CheckBox ID="chk_UseFeeder" runat="server" Text="Use Feeder" ToolTip="Use Feeder to scan dockets" /></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
             <tr>
                <td width="100%" background="../Images/separator_repeat.gif" colSpan="5" height="11">
                </td>
                <td style="display:none">
                    <asp:TextBox ID="txtnoofscandoc" runat="server" Text="0"></asp:TextBox>
                </td>
            </tr>
                    </table>
                </td>
            
                <td style="height: 19px">
                    &nbsp;
                    </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lbl_Message" runat="server" CssClass="label" ForeColor="Red"></asp:Label></td>
                <td style="height: 19px">
                </td>
            </tr>
            <tr>
                <td>
                    <uc2:Footer ID="Footer1" runat="server" />
                </td>
                <td style="height: 19px">
                </td>
            </tr>
        </table>
    
    </div>
    <DIV id="ErrorString"></DIV>
    </form>
    		<SCRIPT language="JavaScript">

function StartScan()
{
		
	if (VSTwain1.maxImages == null)
	{
	  document.getElementById('ErrorString').innerHTML = "<a href=http://www.vintasoft.com/vstwain-dotnet-faq.html#faq17>Click here to see how to set the .NET Framework Security settings</a>";
	  alert("Your .NET Framework Security settings must be configured to run the components in your browser.");
	  return
	}	
	var temppath = "<% =ViewState["TempFolder"] %>";
    var sessionid = "<% = ViewState["SessionID"] %>";
    var empid = "<% = ViewState["EmployeeID"] %>";
    
	imagesCounter = 0
	VSTwain1.StartDevice()
	VSTwain1.showUI = false
	VSTwain1.maxImages = 1
	if (VSTwain1.SelectSource())
	{
	  VSTwain1.OpenDataSource()
	  //VSTwain1.unitOfMeasure=0;
	  VSTwain1.pixelType = 1;//PixelType.RGB;
      VSTwain1.resolution = 100;
	  /*if (document.Form1.Adf.checked == false)
	    VSTwain1.xferCount = 1
	  else
	    VSTwain1.xferCount = -1
	  VSTwain1.feederEnabled = document.Form1.Adf.checked*/
	  //
	  //var ii=1
	 
	  //
	  while (VSTwain1.AcquireModal())
	  {
	    //imagesCounter = imagesCounter + UploadToHttpServer()
	    var sid= sessionid;//document.getElementById("txtsessionid").value;
		var eid= empid;//document.getElementById("txtempid").value;
		//var fpat="E:\"; /*document.getElementById("txtnoofscandoc").value*/ +sid+eid+"img.jpg"
		var fpat= temppath + sid + eid + "img.jpg";
		imgPath = fpat;
	    if (VSTwain1.SaveImage(0,imgPath))
	    {
	      //document.Img1.src = imgPath
	    }
	    else
	    {
	      alert(VSTwain1.errorString)
	    }
  	    //var ii=Number(document.getElementById("txtnoofscandoc").value)+1
  	    //document.getElementById("txtnoofscandoc").value=ii;	    
	  }
	  
	  	  
	  /*if (document.Form1.Adf.checked == false)
	  {
		var x= confirm("Do you want to scan more pages [OK] Yes [Cancel] NO.");
		if(x)
		{			
			StartScan();
		}		
		else
		{
			//document.getElementById("txtbID").value="";
			//document.getElementById("txtnoofscandoc").value="1";
			//document.Form1.Adf.checked = true;				
		}
	  }*/
	  VSTwain1.CloseDataSource()
	  //document.getElementById("txtnoofscandoc").value="1";
	}
}
</script>
<script type="text/javascript">

function ValidateForm()
{
        var rec_count = "<% = ViewState["RecordCount"] %>";
		var option = document.getElementById("ddl_Options").value;
		var flag = false;
		var scanflag = true;
		if(option == "< Choose >" || option == "0"  )
		{
		alert('Please make a valid selection');
		return(false);
		}
		
		 for (i=1;i<=rec_count;i++)
        {
        
        var scan = "";
        var grd = "gv_Details_";
            if(i<10)
            {
                grd+= "ctl0" +i;
                
            }
            else
            {
            grd+= "ctl"+i;
            }
            scan+=grd+"_ibtn_Scan";
            grd += "_chk_Select";
            
            var chkbox = document.getElementById(grd);
            if(chkbox != null)
                if(chkbox.checked==true )
                    {
                    
                        flag=true;
                        var scan_btn = document.getElementById(scan).style.visibility;
                        if(scan_btn=="hidden")
                            {
                            scanflag=false;
                            }
                        
                    }
            
        }
        
        if(flag != true)
        {
            alert('Please make some selection first');
            return(false);
        }
        
        var selection = document.getElementById("ddl_Options").value;
        if(scanflag==false && selection=="Remove Docket")
        {
            alert('To remove a docket, please select only those records for which docket has been uploaded');
            return(false);
        }
        //gv_Details_ctl02_ibtn_Scan
        
        
        StartScanBySelection();
    	
    	
    	
}

function ShowPopup(ticket,  court)
{
       
        window.open("DocketQuickUpdate.aspx?TicketID="+ticket+"&CourtID="+court+"&emp="+<%= ViewState["EmployeeID"] %>,'QuickUpdate','left=20,top=20,width=450,height=160,toolbar=0,resizable=0' );
       
}

function StartScanBySelection()
{

		var option = document.getElementById("ddl_Options").value;
		
		if(option == "< Choose >" || option == "0"  )
		{
		alert('Please make a valid selection');
		return(false);
		}
		
		
		
		if(option != "Upload Docket")
		    return false;
    var temppath = "<% =ViewState["TempFolder"] %>";
    var sessionid = "<% = ViewState["SessionID"] %>";
    var empid = "<% = ViewState["EmployeeID"] %>";
		
	if (VSTwain1.maxImages == null)
	{
	  document.getElementById('ErrorString').innerHTML = "<a href=http://www.vintasoft.com/vstwain-dotnet-faq.html#faq17>Click here to see how to set the .NET Framework Security settings</a>";
	  alert("Your .NET Framework Security settings must be configured to run the components in your browser.");
	  return
	}	
	imagesCounter = 0
	VSTwain1.StartDevice()
	VSTwain1.showUI = false
	VSTwain1.maxImages = 1
	if (VSTwain1.SelectSource())
	{
	  VSTwain1.OpenDataSource()
	  document.getElementById("txtnoofscandoc").value="0";
	  //VSTwain1.unitOfMeasure=0;
	  VSTwain1.pixelType = 1;//PixelType.RGB;
      VSTwain1.resolution = 100;
	  if (document.getElementById("chk_UseFeeder").checked == false)
	    VSTwain1.xferCount = 1
	  else
	    VSTwain1.xferCount = -1
	  VSTwain1.feederEnabled = document.getElementById("chk_UseFeeder").checked 
	  //
	  //var ii=1
	 
	  //
	  while (VSTwain1.AcquireModal())
	  {
	    //imagesCounter = imagesCounter + UploadToHttpServer()
	    var sid= sessionid;//document.getElementById("txtsessionid").value;
		var eid= empid;//document.getElementById("txtempid").value;
		//var fpat="E:\"; /*document.getElementById("txtnoofscandoc").value*/ +sid+eid+"img.jpg"
		var fpat= temppath+ document.getElementById("txtnoofscandoc").value  + sid + eid + "img.jpg";
		
		imgPath = fpat;
	    if (VSTwain1.SaveImage(0,imgPath))
	    {
	      //document.Img1.src = imgPath
	    }
	    else
	    {
	      alert(VSTwain1.errorString)
	    }
  	    var ii=Number(document.getElementById("txtnoofscandoc").value)+1
  	    document.getElementById("txtnoofscandoc").value=ii;	    
	  }
	  
	  	  
	  /*if (document.Form1.Adf.checked == false)
	  {
		var x= confirm("Do you want to scan more pages [OK] Yes [Cancel] NO.");
		if(x)
		{			
			StartScan();
		}		
		else
		{
			//document.getElementById("txtbID").value="";
			//document.getElementById("txtnoofscandoc").value="1";
			//document.Form1.Adf.checked = true;				
		}
	  }*/
	  VSTwain1.CloseDataSource()
	  //document.getElementById("txtnoofscandoc").value="1";
	}
}

		</SCRIPT>
		
		 <script type="text/javascript" >
    
function SelectAll()
{
    var rec_count = "<% = ViewState["RecordCount"] %>";
    var chk = document.getElementById("chk_SelectAll");
    
    if(chk.checked == true)
    {
        for (i=1;i<=rec_count;i++)
        {
        
        var grd = "gv_Details_";
            if(i<10)
            {
                grd+= "ctl0" +i;
                
            }
            else
            {
            grd+= "ctl"+i;
            }
            grd += "_chk_Select";
            
            var chkbox = document.getElementById(grd);
            if(chkbox != null)
            {//a;
                if(/*chkbox.parentElement.style.display =="none" ||*/ chkbox.parentElement.style.visibility !="hidden")
                chkbox.checked = true;
            }
        }
        
    }
    if(chk.checked == false)
    {
        for (i=1;i<=rec_count;i++)
        {
        
        var grd = "gv_Details_";
            if(i<10)
            {
                grd+= "ctl0" +i;
                
            }
            else
            {
            grd+= "ctl"+i;
            }
            grd += "_chk_Select";
            
            var chkbox = document.getElementById(grd);
            if(chkbox != null)
            chkbox.checked = false;
        }
        
    }
}
function EnableFeeder()
{
    var val1 = document.getElementById('ddl_Options');
    if(val1==null)
        return;
    var val = document.getElementById('ddl_Options').value;
    
    if(val == "Upload Docket")
    {
        document.getElementById('tbl_feeder').style.display='block';
    }
    else
    {
    document.getElementById('tbl_feeder').style.display='none';
    }
    
}

    </script>
</body>
</html>

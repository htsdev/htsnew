using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.Reports
{
	/// <summary>
	/// Summary description for frmRptFTAThree.
	/// </summary>
	public partial class frmRptFTAThree : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label lblCurrPage;
		protected eWorld.UI.CalendarPopup dtFrom;
		protected eWorld.UI.CalendarPopup dtTo;
		protected System.Web.UI.WebControls.DataGrid dgFTA;
		protected System.Web.UI.WebControls.Button btnSubmit;
		protected System.Web.UI.WebControls.Label lblMessage;
		protected System.Web.UI.WebControls.ImageButton imgBtnExcel;
		protected System.Web.UI.WebControls.DropDownList ddlPageNo;
		protected System.Web.UI.WebControls.CheckBox chkAll;
		clsENationWebComponents ClsDb = new clsENationWebComponents();
		clsSession ClsSession=new clsSession();
		clsLogger bugTracker = new clsLogger();
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			if (ClsSession.IsValidSession(this.Request)==false)
			{
				Response.Redirect("../frmlogin.aspx",false);
			}
			else //To stop page further execution
			{
				// Put user code to initialize the page here
				if (!IsPostBack)
				{
                    dtFrom.SelectedDate = DateTime.Today;
                    dtTo.SelectedDate = DateTime.Today;


					if (chkAll.Checked == true)
					{
						dgFTA.AllowPaging = false;
					}
					else
					{
						dgFTA.AllowPaging = true;
						dgFTA.CurrentPageIndex = 0;
					}
					dtFrom.SelectedDate = DateTime.Today;
					dtTo.SelectedDate = DateTime.Today;


					FillGrid();
					GenerateSerial();
					FillPageList();
					SetNavigation();
					btnSubmit.Attributes.Add("OnClick"," return CalendarValidation();");
					btnSubmit.Attributes.Add("OnClick", "return validate();");

				}
			}
		}
		private void FillGrid()
		{
			try
			{
				lblMessage.Text="";


				string [] key = {"@FromDate","@Todate"};
				object [] value1 = {dtFrom.SelectedDate, dtTo.SelectedDate};

				DataSet ds = ClsDb.Get_DS_BySPArr("USP_HTS_GET_FTAThreeReport",key,value1);
				dgFTA.DataSource = ds;
				dgFTA.DataBind();

				
				// this will be used for exporting data grid in excel sheet from a diff. page........
                Session.Add("MoveDataSet", ds);


				}
			catch(Exception ex)
			{
				if (dgFTA.CurrentPageIndex > dgFTA.PageCount -1 )
				{
					dgFTA.CurrentPageIndex = 0;
				}
				else
				{
					lblMessage.Text = ex.Message;
					bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);

				}
			}

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
			this.imgBtnExcel.Click += new System.Web.UI.ImageClickEventHandler(this.imgBtnExcel_Click);
			this.ddlPageNo.SelectedIndexChanged += new System.EventHandler(this.ddlPageNo_SelectedIndexChanged);
			this.chkAll.CheckedChanged += new System.EventHandler(this.chkAll_CheckedChanged);
			this.dgFTA.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dgFTA_PageIndexChanged);
			this.dgFTA.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgFTA_ItemDataBound);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnSubmit_Click(object sender, System.EventArgs e)
		{
			dgFTA.CurrentPageIndex=0;
			FillGrid();
			GenerateSerial();
			FillPageList();
			SetNavigation();

		}

		private void FillPageList()
		{
			try
			{
				int i =0;
				ddlPageNo.Items.Clear();
				for(i=1; i<=dgFTA.PageCount;i++ )
				{
					ddlPageNo.Items.Add("Page - " + i.ToString());
					ddlPageNo.Items[i-1].Value = i.ToString();
				}
			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message;
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}

		}

		private void dgFTA_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			dgFTA.CurrentPageIndex = e.NewPageIndex;
			FillGrid();
			GenerateSerial();
			FillPageList();
			SetNavigation();
		}

		private void ddlPageNo_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			dgFTA.CurrentPageIndex = ddlPageNo.SelectedIndex ;
			FillGrid();
			GenerateSerial();
			FillPageList();
			SetNavigation();
		}	

		private void SetNavigation()
		{
			// Procedure for setting data grid's current  page number on header ........
			
			try
			{
					// setting current page number
					lblCurrPage.Text =Convert.ToString(dgFTA.CurrentPageIndex+1);

					for (int idx =0 ; idx < ddlPageNo.Items.Count ;idx++)
					{
						ddlPageNo.Items[idx].Selected = false;
					}
					ddlPageNo.Items[dgFTA.CurrentPageIndex].Selected=true;
			
			}
			catch (Exception ex)
			{
				lblMessage.Text = ex.Message ;
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}

		private void GenerateSerial()
		{
			// Procedure for writing serial numbers in "S.No." column in data grid.......	

			
			long sNo=(dgFTA.CurrentPageIndex)*(dgFTA.PageSize);									
			try
			{
				// looping for each row of record.............
				foreach (DataGridItem ItemX in dgFTA.Items) 
				{ 					
					sNo +=1 ;
					// setting text of hyper link to serial number after bouncing of hyper link...
					((Label)(ItemX.FindControl("lblSerial"))).Text  = (string) Convert.ChangeType(sNo,typeof(string));
				 
				}
			}
			catch(Exception ex)

			{
				lblMessage.Text = ex.Message ;
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}		
		}

		private void imgBtnExcel_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			if (dgFTA.Items.Count > 0 )
			{
				Response.Redirect("FrmExportToExcel.aspx");
			}
		}

		private void dgFTA_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if (e.Item.ItemType  != ListItemType.Header && e.Item.ItemType != ListItemType.Footer )
			{
				// making hyperlinks for hired clients detail page......
				if (((HyperLink)(e.Item.FindControl("hlkHired"))).Text != "0")
				{
					string sMailDate =  ((Label)  (e.Item.FindControl("lblMailDate"))).Text;
					((HyperLink)(e.Item.FindControl("hlkHired"))).NavigateUrl = "frmRptFTAThreeDetail.aspx?detail=hired&maildate=" + sMailDate;
				}
				else
				{
					((HyperLink)(e.Item.FindControl("hlkHired"))).Enabled=false;
				}

				// making hyperlinks for quoted clients detail page......
				if (((HyperLink)(e.Item.FindControl("hlkQuoted"))).Text != "0")
				{
					string sMailDate =  ((Label)  (e.Item.FindControl("lblMailDate"))).Text;
					((HyperLink)(e.Item.FindControl("hlkQuoted"))).NavigateUrl = "frmRptFTAThreeDetail.aspx?detail=quote&maildate=" + sMailDate;
				}
				else
				{
					((HyperLink)(e.Item.FindControl("hlkQuoted"))).Enabled=false;
				}

				
				double profit = ( (double)  (Convert.ChangeType( e.Item.Cells[8].Text  , typeof(double)))   );
				if (profit < 0 )
					e.Item.Cells[8].ForeColor = System.Drawing.Color.Red;

				double tReturn = ( (double)  (Convert.ChangeType( e.Item.Cells[9].Text  , typeof(double)))   );
				if (tReturn < 0 )
					e.Item.Cells[9].ForeColor = System.Drawing.Color.Red;


			}

		}

		private void chkAll_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkAll.Checked == true )
			{
				dgFTA.AllowPaging = false;
				dgFTA.CurrentPageIndex=0;
				FillGrid();
				GenerateSerial();
				FillPageList();
				SetNavigation();
			}
			else
			{
				dgFTA.AllowPaging = true;
				dgFTA.PageSize = 10;
				dgFTA.CurrentPageIndex = 0 ;
				FillGrid();
				GenerateSerial();
				FillPageList();
				SetNavigation();
			}
		}



	}
}

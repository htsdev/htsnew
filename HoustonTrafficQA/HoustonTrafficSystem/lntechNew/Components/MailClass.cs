
using System;
using System.Configuration;
using System.Data;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mail;
using FrameWorkEnation.Components;
using LNHelper;


namespace lntechNew.Components
{
    public class MailClass
    {
        #region Attroneys Email Functions

        public static void SendMailToAttorneys(string message, string subject, string filePath)
        {
            try
            {
                // Getting Email Addresses of Attorneys
                clsENationWebComponents ClsDb = new clsENationWebComponents();
                DataSet attorneys = ClsDb.Get_DS_BySP("USP_HTS_GetAttorneyAddress");
                string users = "";

                for (int i = 0; i < attorneys.Tables[0].Rows.Count; i++)
                    users += attorneys.Tables[0].Rows[i][1].ToString() + ",";

                SendMailToAttorneys(message, subject, filePath, users);
            }
            catch (Exception ex)
            {
                Logger.Instance.LogError(ex);
            }
        }

        public static void SendMailToAttorneys(string message, string subject, string filePath, string users)
        {
            SendMailToAttorneys(message, subject, filePath, users, "");
        }

        public static void SendMailToAttorneys(string message, string subject, string filePath, string tousers, string ccusers)
        {
            try
            {
                string From = (string)ConfigurationSettings.AppSettings["MailFrom"];

                MailMessage msg = new MailMessage();
                msg.To = tousers;
                msg.Cc = ccusers;
                msg.From = From;
                msg.Subject = subject;
                msg.Body = message;
                msg.BodyFormat = MailFormat.Html;

                if (filePath.Length > 0)
                {
                    MailAttachment Att = new MailAttachment(filePath);
                    msg.Attachments.Add(Att);
                }
                //Send Email
                SendEmailMessage(msg);
            }
            catch (Exception ex)
            {
                Logger.Instance.LogError(ex);
            }
        }

        public static void SendMailToAttorneysPayments(string subject, string filePath, string tousers, string ccusers, string ticketnumber, string comments, string fname, string lname, int type)
        {
            try
            {
                string From = (string)ConfigurationSettings.AppSettings["MailFrom"];

                MailMessage msg = new MailMessage();

                msg.To = tousers;
                msg.Cc = ccusers;
                msg.From = From;
                msg.Subject = subject;
                if (type == 4)
                {
                    msg.Body = "<html><body><h3> Criminal Case Hired (Payment Details Are Given Below) </h3><br><b> Ticket Number Is : </b> ";
                }
                else
                {
                    msg.Body = "<html><body><h3> Payment Details </h3><br><b> Ticket Number Is : </b> ";
                }
                msg.Body += ticketnumber.ToString().ToUpper() + "." + "<br>";
                msg.Body += "<br><b> Client Name Is : </b> ";
                msg.Body += fname.ToString().ToUpper() + "," + lname.ToString().ToUpper() + "." + "<br>";


                if (comments.Length > 0)
                {
                    msg.Body += "<br><b>Payment Comments :</b>";
                    msg.Body += "" + comments.ToString().ToUpper() + "<br>";
                }


                msg.Body += "<br>" + "<a href=" + HttpContext.Current.Request.Url + ">Please Click Here To Find Details</a><br>";
                msg.Body += "</body></html>";


                msg.BodyFormat = MailFormat.Html;

                if (filePath.Length > 0)
                {
                    MailAttachment Att = new MailAttachment(filePath);
                    msg.Attachments.Add(Att);
                }

                //Send Email
                SendEmailMessage(msg);

            }
            catch (Exception ex)
            {
                LNHelper.Logger.Instance.LogError(ex);
            }
        }

        #endregion

        #region Flag Notification Email

        // Added by kazim 28 August 2007
        // This Methods Send Email Open Service Ticket Flag Information
        public static void SendOpenServiceEmail(string message, string subject)
        {
            SendOpenServiceEmail(message, subject, string.Empty);
        }

        public static void SendOpenServiceEmail(string message, string subject, string CC)
        {
            string To = (string)ConfigurationManager.AppSettings["OpenServiceUser"];
            string From = (string)ConfigurationManager.AppSettings["MailFrom"];
            SendMail(From, To, subject, GetEmailBody(message), CC);
        }

        public static void SendOpenServiceAssign(string message, string subject, string To, string CC)
        {
            string From = (string)ConfigurationManager.AppSettings["MailFrom"];
            SendMail(From, To, subject, GetEmailBody(message), CC);
        }

        public static void SendCloseServiceEmail(string message, string subject)
        {
            string users = (string)ConfigurationManager.AppSettings["CloseServiceUser"];
            string From = (string)ConfigurationManager.AppSettings["MailFrom"];
            SendMail(From, users, subject, GetEmailBody(message), String.Empty);
        }

        public static void SendPaymentEmail(string message, string subject, string users, string CCUsers)
        {
            string From = (string)ConfigurationManager.AppSettings["MailFrom"];
            SendMail(From, users, subject, GetEmailBody(message), CCUsers);
        }

        public static string GetEmailBody(string contents)
        {

            string body = "";

            body += "<html><head><style type=\"text/css\">";
            body += ".Label{ FONT-WEIGHT: normal;   COLOR: #000000;  FONT-STYLE: normal;  FONT-FAMILY: Verdana; FONT-SIZE: 8.5pt; border-style:none;  border-width:0px;  }";
            body += ".clsLeftPaddingTable { PADDING-LEFT: 5px; FONT-SIZE: 8pt; COLOR: #123160;FONT-FAMILY: Tahoma; background-color:#EFF4FB;}";
            body += ".SmallLabel {FONT-WEIGHT: normal;   COLOR:Gray;  FONT-STYLE: normal;  FONT-FAMILY: Verdana; FONT-SIZE: 7pt; border-style:none;  border-width:0px;}</style></head>";
            body += "<body><table border=\"0\" bgcolor=\"#D9D9D9\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\" height=\"100%\" width=\"100%\">";
            //Yasir Kamal 7140 12/23/2009 case email summary issue fixed.
            body += "<tr><td style=\"FONT-WEIGHT: normal;   COLOR:Gray;  FONT-STYLE: normal;  FONT-FAMILY: Verdana; FONT-SIZE: 7pt; border-style:none;  border-width:0px;\" align=\"center\"><br /></td></tr><tr><td width=\"100%\" style=\"height: 100%\"><br>";
            body += "<table border=\"1\" bgcolor=\"white\" bordercolor=\"Silver\" cellpadding=\"8\" cellspacing=\"0\" align=\"center\" height=\"100%\" width=\"500\">";
            body += "<tr><td align=\"center\" style=\"height: 5px; border: 1; border-color: Silver;\"><img src=\"http://www.sullolaw.com/images/SulloLawlogo.jpg\" />";
            body += "</td></tr><tr><td valign=\"top\" width=\"100%\" style=\"height: 400px\">";
            body += "<table border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">";
            body += "<tr><td class=\"Label\" style=\"height: 15px\"></td><td class=\"Label\" style=\"height: 15px\"><br /><br /></td></tr>";
            body += "<tr><td align=\"center\" style=\"width: 13px\"></td><td height=\"150px\"align=\"center\" class=\"Label\" valign=\"left\">" + contents + "</td></tr>";
            body += "<tr><td class=\"Label\" style=\"width: 13px; height: 15px;\"></td><td class=\"Label\" style=\"height: 15px\"><br /><br /><br /><br /></td></tr>";
            body += "<tr><td style=\"width: 13px\"></td><td><font size=\"5\" face=\"Calibri\"><span style=\"font-size: 16.0pt; font-family: Calibri;\"><font size=\"5\" face=\"Calibri\"><span style=\"font-size: 16.0pt;font-family: Calibri;\">Sullo & Sullo, LLP</span></font></td></tr>";
            body += "<tr><td style=\"width: 13px; height: 19px\"></td><td style=\"height: 19px\"><font size=\"3\" class=\"Label\"><b>Tel #:</b></font> 713.839.9026</td></tr>";
            body += "<tr><td style=\"width: 13px\"></td><td><font size=\"3\" class=\"Label\"><b>Fax #:</b></font> 713.523.6634</td></tr>";
            body += "<tr><td style=\"width: 13px\"></td><td><u>http://www.sullolaw.com</u><br><br></td></tr></table></td></tr></table></td></tr><tr><td>";
            body += "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\" style=\"width: 452px;height: 1px;\"><tr><td valign=\"top\" align=\"center\"><table style=\"width: 500px\">";
            body += "<tr><td><hr /></td></tr></table></td></tr>"
            // Rab Nawaz 10914 07/30/2013 Removed Disclimer. . . 
            //<tr><td align=\"center\" width=\"100%\" style=\"FONT-WEIGHT: normal;   COLOR:Gray;  FONT-STYLE: normal;  FONT-FAMILY: Verdana; FONT-SIZE: 7pt; border-style:none;  border-width:0px;text-align :justify;\">This e-mail and the files transmitted with it are the property of Sullo & Sullo";
            //body += "LLP and/or its affiliates, are confidential, and are intended solely for use of the individual or entity to whom this e-mail is addressed. If you are not one of this message in error, please notify the sender at 713-839-9026 and delete this message immediately from your computer. Any other use, retention, dissemination,forwarding, printing, or copying of this e-mail is strictly prohibited.";
            //body += "<br /><br /></td></tr></table></td></tr><tr><td align=\"center\" style=\"FONT-WEIGHT: normal;   COLOR:Gray;  FONT-STYLE: normal;  FONT-FAMILY: Verdana; FONT-SIZE: 7pt; border-style:none;  border-width:0px;\">Copyright� 2007, Sullo & Sullo, LLP All Rights Reserved.<br /><br /><br /></td></tr>
            + "</table></body></html>";
            return body;
        }

        public static string SendOpenCloseTroubleTicketEmail(string message, string subject, string dev)
        {

            string users = string.Empty;
            try
            {
                if (subject.Contains("Closed"))
                    users = (string)ConfigurationManager.AppSettings["CloseTrobuleTicket"];
                else
                    users = (string)ConfigurationManager.AppSettings["NewTrobuleTicket"];

                if (dev.Length > 1)
                    users = dev;

                MailMessage msg = new MailMessage();

                msg.To = users;
                msg.From = Convert.ToString(ConfigurationManager.AppSettings["MailFrom"]);
                msg.Subject = subject;
                msg.Body = GetEmailBody(message);
                msg.BodyFormat = MailFormat.Html;

                //Send Email
                SendEmailMessage(msg);

                return "";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        /// <summary>
        /// Added  to format Void Payment Email
        /// khalid 2988 2/13/08 void payment email
        /// </summary>
        /// <param name="ticketid"> unique id of client</param>
        /// <param name="url">reference to web page </param>
        /// <param name="empName">sales rep processing payment </param>
        /// <param name="violations">table containing violations  </param>
        public static void SendPaymentNotificationEmail(int ticketid, string url, string empName, DataTable violations)
        {

            if (violations.Rows.Count > 0)
            {
                string firstName = Convert.ToString(violations.Rows[0]["FirstName"]);
                string lastName = Convert.ToString(violations.Rows[0]["LastName"]);
                string generalComments = Convert.ToString(violations.Rows[0]["generalcomments"]);

                url = "http://" + url + "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + Convert.ToString(ticketid);

                string subject = "";
                string toUser = (string)ConfigurationManager.AppSettings["paymentEmail"];
                string ccUser = (string)ConfigurationManager.AppSettings["paymentEmailCC"];
                string body = "";
                string name = "<a href=" + url + ">" + lastName + " " + firstName + "</a>";
                subject = "PaymentDetail----Void � " + lastName + " " + firstName;


                #region Service Ticket Email Body

                //Format HTML Of Case Violations
                string grid = "<table width='438' border=0 cellpedding=0 cellspacing =0><tr><td width='111'><b>Ticket#</b></td><td width='98' ><b>Court date</b></td><td width='89'><b>Time</b></td><td width='64'><b>Court#</b></td><td width='66'><b>Status</b></td></tr>";

                foreach (DataRow dr in violations.Rows)
                    grid += "<tr><td class=label>" + dr["refcasenumber"] + "</td><td class=label>" + dr["courtDate"] + "</td>" + "<td class=label>" + dr["courtTime"] + "</td><td class=label>&nbsp;&nbsp;" + dr["courtnumbermain"] + "</td><td class=label>" + dr["Courtviolationstatus"] + "</td></tr>\n";

                grid += "</table>";

                //Nasir 6483 10/07/2009 if bond client BOND CLIENT VOIDED test appears on top
                if (Convert.ToInt32(violations.Rows[0][3]) == 1)
                {
                    body = "<center><font size=\"5\" color=\"red\"><b>BOND CLIENT VOIDED</b></font></center><br />";
                }

                //Nasir 6483 10/07/2009 concate test
                body += "<center><font size=\"5\"><b>Payment Void Details</b></font></center><br />" + "<br /><br /><table width='460'><tr><td align=left><table class=clsLeftPaddingTable width='440' border='0' cellpadding='0' cellspacing='0'>\n<tr><td width='100' align='left' NOWRAP><b>Client Name: </b> </td><td width='340' align='left'>" + name + "</td></tr><tr><td><b>Sales Rep:</b></td><td>" + empName + "</td></tr><tr><td><b>" + "DateTime" + ":</b></td>\n<td>" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + "</td></tr>\n<tr><td><b>Comments:</b></td><td>" + generalComments + "</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td></tr></table></td>\n</tr><tr><td><center><font size=\"4\"><b>Case Information</b></font></center>" + grid + "</td></tr></table>";
                string test = body;
                #endregion

                MailClass.SendPaymentEmail(body, subject, toUser, ccUser);

            }
        }

        public static void SendMail(string From, string To, string Subject, string Body, string CC)
        {
            try
            {
                MailMessage msg = new MailMessage();
                msg.To = To;
                msg.From = From;
                msg.Subject = Subject;
                msg.Body = Body;
                msg.BodyFormat = MailFormat.Html;

                if (CC != string.Empty)
                    msg.Cc = CC;

                //Send Email
                SendEmailMessage(msg);

            }
            catch (Exception exp)
            {
                // tahir 6813 10/19/2009 logging the exception if any...
                Logger.Instance.LogError(exp);
            }
        }


      


        // Noufil 3999 05/17/2008 Sending Email With Attachment
        /// <summary>
        /// This Function is use to send email with attachment
        /// </summary>
        /// <param name="From"></param>
        /// <param name="To"></param>
        /// <param name="Subject"></param>
        /// <param name="Body"></param>
        /// <param name="attachment"></param>
        public static void SendMailWithtAttchment(string From, string To, string Subject, string Body, string attachment)
        {
            try
            {
                MailMessage msg = new MailMessage();
                msg.To = To;
                msg.From = From;
                msg.Subject = Subject;
                msg.Body = Body;
                msg.BodyFormat = MailFormat.Html;
                MailAttachment Att = new MailAttachment(attachment);
                msg.Attachments.Add(Att);
                //Send Email
                SendEmailMessage(msg);

            }
            catch (Exception exp)
            { }
        }

        //Kazim 3975 6/30/2008 Used to Send the mail with multiple attachments
        /// <summary>
        /// This method is used to send a mail with multiple attachements
        /// </summary>
        /// <param name="From"></param>
        /// <param name="To"></param>
        /// <param name="Subject"></param>
        /// <param name="Body"></param>
        /// <param name="attachments"></param>

        public static void SendMailWithtMultipleAttchments(string From, string To, string Subject, string Body, string attachments)
        {
            try
            {
                MailMessage msg = new MailMessage();
                msg.To = To;
                msg.From = From;
                msg.Subject = Subject;
                msg.Body = Body;
                msg.BodyFormat = MailFormat.Html;

                string[] attachment = attachments.Split(',');

                for (int count = 0; count < attachment.Length; count++)
                {
                    MailAttachment Att = new MailAttachment(attachment[count]);
                    msg.Attachments.Add(Att);
                }
                //Send Email
                SendEmailMessage(msg);

            }
            catch (Exception exp)
            { }
        }

        /// <summary>
        /// This function check the IP of the server if IP is Local then a Procedure is called which accpept only those Email Addresses 
        /// that wese defined in the SP else remove the remaining
        /// </summary>
        /// <param name="to">This parameter take the Receiver email address to whom email will send</param>
        /// <returns>It will return Valid Email Addresses that was saved on database  </returns>
        public static string VerifyToEmailAdresses(string to)
        {

            try
            {
                // Noufil 4407 07/21/2008 This function take reciever email address and return the email adddress that was save on database else remove
                IPAddress[] objip = Dns.GetHostAddresses(Dns.GetHostName());
                //string ServerIP = objip[0].ToString();
                string DefineIP = ConfigurationManager.AppSettings["ServerIP"].ToString();
                //Sabir Khan 5578 02/25/2009 convert IP address array into comma seperated string...
                string IPAddresses = "";
                for (int i = 0; i < objip.Length; i++)
                {
                    IPAddresses += objip[i] + ";";
                }
                if (!IPAddresses.Contains(DefineIP))
                {
                    clsENationWebComponents clsdb = new clsENationWebComponents();
                    string[] keys = { "@EmailAddresses" };
                    object[] values = { to };
                    return Convert.ToString(clsdb.ExecuteScalerBySp("USP_HTP_GetLocalValidEmailAdresses", keys, values));
                }
                else
                    return to;
            }
            catch (Exception ex)
            {
                Logger.Instance.LogError(ex);
            }
            return "";
        }


        public static void SendEmailMessage(MailMessage msg)
        {
            try
            {
                string ToAddresses = VerifyToEmailAdresses(msg.To);

                if (ToAddresses != "")
                {
                    int cdoBasic = 1;
                    int cdoSendUsingPort = 2;
                    string smtpServer = (string)ConfigurationManager.AppSettings["SMTPServer"];
                    string userName = (string)ConfigurationManager.AppSettings["SMTPUser"];
                    string password = (string)ConfigurationManager.AppSettings["SMTPPassword"];
                    
                    // Noufil 4747 New microsoft link added for adding port
                    if (userName.Length > 0)
                    {
                        msg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpserver", smtpServer);
                        if (smtpServer.Contains("gmail"))
                            msg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpserverport", 465);
                        else
                            msg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpserverport", 25);


                        msg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusing", cdoSendUsingPort);
                        msg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", cdoBasic);
                        msg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", userName);
                        msg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", password);
                        msg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpusessl", "true");
                    }

                    //Farrukh 11071 07/11/2013 Key Added for Receipts and Notifications 
                    string emailBackUpHouston = ConfigurationManager.AppSettings["EmailBackupHouston"];
                    ToAddresses = ToAddresses + "," + emailBackUpHouston;

                    msg.To = ToAddresses;
                    msg.Cc = VerifyToEmailAdresses(msg.Cc);
                    msg.Bcc = VerifyToEmailAdresses(msg.Bcc);
                    SmtpMail.SmtpServer = smtpServer;
                    SmtpMail.Send(msg);
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.LogError(ex);
            }
        }

        // Noufil 7157 12/16/2009 
        /// <summary>
        ///     This method create email body for Problem Client Flag
        /// </summary>
        /// <param name="content">Body text to show on Email</param>
        /// <returns>string which contains body in HTML format</returns>
        public string GetProblemClientEmailBody(string content)
        {
            StringBuilder body = new StringBuilder();
            body.Append("<html><head><style type=\"text/css\">.Label { font-weight: normal;color: #000000;font-style: normal;font-family: Verdana;font-size: 8.5pt;border-style: none;border-width: 0px;}.clsLeftPaddingTable{padding-left: 5px;font-size: 8pt;color: #123160;font-family: Tahoma; } .SmallLabel { font-weight: normal;color: Gray;font-style: normal;font-family: Verdana;font-size: 7pt;border-style: none;border-width: 0px;} </style></head>");
            body.Append("<body><table border=\"0\" bgcolor=\"#D9D9D9\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\" height=\"100%\" width=\"100%\"><tr><td style=\"font-weight: normal; color: Gray; font-style: normal; font-family: Verdana; font-size: 7pt; border-style: none; border-width: 0px;\" align=\"center\"><br /></td></tr><tr><td width=\"100%\" style=\"height: 100%\"><br /><table border=\"1\" bgcolor=\"white\" bordercolor=\"Silver\" cellpadding=\"8\" cellspacing=\"0\" align=\"center\" height=\"100%\" width=\"500\"><tr><td align=\"center\" style=\"height: 5px; border: 1; border-color: Silver;\"><img src=\"http://www.sullolaw.com/images/SulloLawlogo.jpg\" /></td></tr><tr><td valign=\"top\" width=\"100%\" style=\"height: 400px\">");
            body.Append("<table border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\"><tr><td class=\"Label\" style=\"height: 15px\"></td><td class=\"Label\" style=\"height: 15px\"><br /><br /></td></tr><tr><td align=\"center\" style=\"width: 13px\"></td><td height=\"60px\" align=\"left\" valign=\"top\" class=\"clsLeftPaddingTable\">" + content);
            body.Append("</td></tr><tr><td class=\"Label\" style=\"width: 13px; height: 15px;\"></td><td class=\"Label\" style=\"height: 15px\"><br /><br /><br /><br /></td></tr><tr><td style=\"width: 13px\"></td><td><font size=\"4\" face=\"Calibri\">Sullo & Sullo, LLP </font></td></tr><tr><td style=\"width: 13px; height: 19px\"></td><td style=\"height: 19px\"><font size=\"3\" face=\"Calibri\"><b>Tel #:</b>&nbsp;713.839.9026</font></td></tr><tr><td style=\"width: 13px\"></td><td><font size=\"3\" face=\"Calibri\"><b>Fax #:</b>&nbsp;713.523.6634</font></td></tr><tr><td style=\"width: 13px\"></td><td><u>http://www.sullolaw.com</u><br><br></td></tr></table></td></tr></table></td></tr></td></tr>");
            // Rab Nawaz 10914 07/30/2013 Removed Disclimer. . . 
            // body.Append("<tr><td><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\" style=\"width: 452px;height: 1px;\"><tr><td valign=\"top\" align=\"center\"><table style=\"width: 500px\"><tr><td><br /></tr></table></td></tr><tr><td align=\"center\" width=\"100%\" style=\"font-weight: normal; color: Gray; font-style: normal;font-family: Verdana; font-size: 7pt; border-style: none; border-width: 0px;text-align: justify;\">This e-mail and the files transmitted with it are the property of Sullo & SulloLLP and/or its affiliates, are confidential, and are intended solely for use of the individual or entity to whom th is e-mail is addressed. If you are not one of this message in error, please notify the sender at 713-839-9026 and delete this message immediately from your computer. Any other use, retention, dissemination,forwarding, printing, or copying of this e-mail is strictly prohibited.<br /><br /></td></tr></table>");
            body.Append("<tr><td align=\"center\" style=\"font-weight: normal; color: Gray; font-style: normal; font-family: Verdana;font-size: 7pt; border-style: none; border-width: 0px;\">Copyright� 2007, Sullo & Sullo, LLP All Rights Reserved.<br /><br /><br /></td></tr></table><br /><br />CONFIDENTIALITY NOTICE: The information contained in this communication, including attachments is privileged and confidential. It is intended only for the exclusive use of the addressee. If the reader of this message is not the intended recipient, or the employee or agent responsible for the delivering it to the intended recipient,you are hereby notified that any dissemination, distribution or copies of this communication is strictly prohibited. If you have received this communication in error please contact the sender by reply email and destroy all copied of the original message.Thank you.</body></html>");
            return body.ToString();
        }

        #endregion

        #region System.Net.Mail Class Function

        // Noufil 6766 03/10/2010 
        /// <summary>
        /// This method is used to send email messages with alternative views to user
        /// </summary>
        /// <param name="from">From Email Address</param>
        /// <param name="to">To Email Address</param>
        /// <param name="subject">Email Subject</param>
        /// <param name="alternativeView">Another View</param>
        /// <param name="cc">CC Email Address</param>
        /// <example> 
        /// <code>
        ///     MailClass.SendMailToAttorneys("from@lntechnologies.com","to@lntechnologies.com","Sbject of Email","Message Content","cc@lntechnologies.com");
        /// </code> 
        /// </example> 
        public static void SendMail(string from, string to, string subject, System.Net.Mail.AlternateView alternativeView, string cc)
        {           

            string smtpServer = ConfigurationManager.AppSettings["SMTPServer"];
            string userName = ConfigurationManager.AppSettings["SMTPUser"];
            string password = ConfigurationManager.AppSettings["SMTPPassword"];
            string senderName = "";
            System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient();
            System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();

            try
            {
                if (to.Contains(","))
                    msg.To.Add(to);
                else
                    msg.To.Add(to);

                if (ConfigurationManager.AppSettings["SMTPSenderName"] != null)
                    senderName = Convert.ToString(ConfigurationManager.AppSettings["SMTPSenderName"]);

                msg.From = new System.Net.Mail.MailAddress(from, senderName);
                msg.Subject = subject;

                if (alternativeView != null)
                    msg.AlternateViews.Add(alternativeView);

                msg.IsBodyHtml = true;

                if (!string.IsNullOrEmpty(cc))
                {
                    msg.CC.Add(cc);
                }

                if (!string.IsNullOrEmpty(userName) && !string.IsNullOrEmpty(password))
                {
                    NetworkCredential credentials = new NetworkCredential(userName, password);
                    client.Credentials = credentials;
                }

                if (smtpServer.Contains("gmail"))
                {
                    client.EnableSsl = true;
                    client.Port = 587;
                }

                client.Host = smtpServer;

                if (ConfigurationManager.AppSettings["SMTPReplyToAddress"] != null)
                {
                    System.Net.Mail.MailAddress replyTo = new System.Net.Mail.MailAddress(Convert.ToString(ConfigurationManager.AppSettings["SMTPReplyToAddress"]));
                    msg.ReplyTo = replyTo;
                }

                client.Send(msg);
                msg.Dispose();

            }
            catch (Exception ex)
            {
                msg.Dispose();              
                throw ex;
            }
            finally
            {
                msg.Dispose();
            }

        }


        #endregion
    }
}

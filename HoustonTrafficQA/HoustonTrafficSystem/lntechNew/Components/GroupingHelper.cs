using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

/// <summary>
/// Summary description for GroupingHelper
/// </summary>
public class GroupingHelper
{
	public GroupingHelper()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public DataTable SetupGroupedTable(DataTable dt, string sGroupCol)
    {
        dt.Columns.Add(new DataColumn("poorman_type", System.Type.GetType("System.String")));
        foreach (DataRow row in dt.Rows)
            row["poorman_type"] = "item";

        if (sGroupCol.Length == 0)
            return dt;

        string sLastGroupColValue = "-";
        DataTable dt2 = dt.Clone();
        dt2.Clear();
        int OldTicketid = 0;
        int TicketCounter = 0;
        string OldValue = null;
        foreach (DataRow row in dt.Rows)
        {
            int NewTicketid = Convert.ToInt32(row["ticketid_pk"]);
                //row["blankspace"];

            if (OldTicketid == 0)
            {
                TicketCounter++;
                row["blankspace"] = TicketCounter;
                OldTicketid = Convert.ToInt32(row["ticketid_pk"]);
            }
            else if (NewTicketid != OldTicketid)
            {
                TicketCounter++;
                row["blankspace"] = TicketCounter;
                OldTicketid = Convert.ToInt32(row["ticketid_pk"]);
            }

            //&&

            //Should be create a header?
            if (sGroupCol != "None" && row[sGroupCol].ToString() != sLastGroupColValue)
            {
                                //Yes
                DataRow rowNew = dt2.NewRow();
                rowNew["poorman_type"] = "groupheader";
                rowNew[sGroupCol] = row[sGroupCol];
                dt2.Rows.Add(rowNew);
                OldValue = null;
            }
            if (sGroupCol != "None")
                sLastGroupColValue = row[sGroupCol].ToString();
            //Yes
            string NewValue = row["CourtNumber"].ToString();

            if (OldValue != null && NewValue != OldValue)
            {
                DataRow rowNew = dt2.NewRow();
                rowNew["poorman_type"] = "CourtRoomChangefooter";
                //rowNew[sGroupCol] = row[sGroupCol];
                dt2.Rows.Add(rowNew);
            }
            OldValue = NewValue;
            dt2.ImportRow(row);
        }
        return dt2;
    }

    public string MinusImage = "";
    public string PlusImage = "";
    public void SetImageSwapper(string sMinus, string sPlus)
    {
        MinusImage = sMinus;
        PlusImage = sPlus;
    }
    public void AddGroup(string sClientID, string sImageID)
    {
        GroupRows.Add(new JSGroupItem(this, sClientID, sImageID));
    }
    public JSGroupItem CurrentGroupItem
    {
        get
        {
            return GroupRows[GroupRows.Count - 1];
        }
    }
    public string GetJS()
    {
        string JS = "<script type=\"text/javascript\">" + Environment.NewLine;
        foreach (JSGroupItem oItem in GroupRows)
        {
            JS += oItem.GetJS();
        }
        JS += "</script>";
        return JS;
    }
    public System.Collections.Generic.List<JSGroupItem> GroupRows = new List<JSGroupItem>();
}

public class JSGroupItem
{
    private string m_ClientID = "";
    private string m_sImageID = "";
    private GroupingHelper m_parent;
    public System.Collections.Generic.List<string> ClientRows = new List<string>();
    public JSGroupItem(GroupingHelper parent, string sClientID, string sImageID)
    {
        m_parent = parent;
        m_ClientID = sClientID;
        m_sImageID = sImageID;
    }
    public void AddClientRow(string sClientRowId)
    {
        ClientRows.Add(sClientRowId);
    }
    private string GetImageSwapperCall()
    {
        if (m_parent.MinusImage.Length == 0 || m_parent.PlusImage.Length == 0)
            return "";
        return "poorman_changeimage('" + m_sImageID + "','" + m_parent.MinusImage + "','" + m_parent.PlusImage + "');";
    }
    public string FunctionName
    {
        get
        {
            return "Toggle_" + m_ClientID ;
        }
    }
    public string GetJS()
    {
        string sRet = "function " + FunctionName + "()" + Environment.NewLine;
        sRet += "{" + Environment.NewLine;
        sRet += GetImageSwapperCall() + Environment.NewLine;
        foreach (string s in ClientRows)
                    sRet += "poorman_toggle('" + s + "');" + Environment.NewLine;
        sRet += "}" + Environment.NewLine;
        return sRet;
    }
}

using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.Components
{   

    public class TrialLetter
    {
        clsENationWebComponents ClsDb = new clsENationWebComponents("Connection String");



        #region Variables

        private int ticketid = 0;
        private DateTime stemaileddate =DateTime.Now;
        private DateTime endemaileddate =DateTime.Now;
        private DateTime stbatchdate =DateTime.Now;
        private DateTime endbatchdate= DateTime.Now;
        //Fahad 6429 09/04/2009 Declare Variable of thankemailstdate and thankemailenddate 
        private DateTime thankemailstdate = DateTime.Now;
        private DateTime thankemailenddate = DateTime.Now;
        #endregion

        #region Properties
        public int TicketID
        {
            set 
            {
                if (value == null)
                    throw new ArgumentException("Ticket id can not be null");
                ticketid = value;
            }
            get
            {
                return ticketid;
            }
        }
                
        public DateTime StEmailedDate
        {
            set
            {
                stemaileddate = value;
            }
            get
            {
                return stemaileddate;
            }
        }

        public DateTime EndEmailedDate
        {
            set
            {
                endemaileddate = value;
            }
            get
            {
                return endemaileddate;
            }
        }
        //Fahad 6429 09/04/2009 Declare property of ThankEmailStDate 
        public DateTime ThankEmailStDate
        {
            set
            {
                thankemailstdate = value;
            }
            get
            {
                return thankemailstdate;
            }
        }
        //Fahad 6429 09/04/2009 Declare property of ThankEmailEndDate
        public DateTime ThankEmailEndDate
        {
            set
            {
                thankemailenddate = value;
            }
            get
            {
                return thankemailenddate;
            }
        }

        public DateTime StBatchDate
        {
            set
            {
                stbatchdate = value;
            }
            get
            {
                return stbatchdate;
            }
        }

        public DateTime EndBatchDate
        {
            set
            {
                endbatchdate = value;
            }
            get
            {
                return endbatchdate;
            }
        }
        
        #endregion

        #region Methods
        //Fahad 6429 09/18/2009 Xml Comments added 
        /// <summary>
        /// This method used to get Data for Trial Notification Email.
        /// </summary>
        /// <returns></returns>
        public DataSet GetTrialLetterEmail()
        {
            try
            {
                DataSet ds = new DataSet();
                if ((stemaileddate == null) || (endbatchdate ==null))
                    return ds;

                string[] keys = { "@stEmailedDate", "@endEmailedDate" };
                object[] values = { stemaileddate,endemaileddate };
                ds =ClsDb.Get_DS_BySPArr("USP_HTS_GET_TrialLetterEmailedReport", keys, values);
                return ds;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //Fahad 6429 09/04/2009 Method to get data of thank email
        /// <summary>
        /// This method get data for thank email.
        /// </summary>
        /// <returns></returns>
        public DataSet GetThankEmail()
        {
            try
            {
                DataSet ds = new DataSet();
                if ((stemaileddate == null) || (endbatchdate == null))
                    return ds;

                string[] keys = { "@stEmailedDate", "@endEmailedDate" };
                object[] values = { thankemailstdate, thankemailenddate };
                ds = ClsDb.Get_DS_BySPArr("USP_HTP_GetThankYouEmailData", keys, values);
                return ds;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetTrialLetterBatch()
        {
            try
            {
                DataSet ds = new DataSet();

                if ((stbatchdate == null) || (endbatchdate == null))
                    return ds;

                string[] keys = { "@stBatchDate", "@endBatchDate" };
                object[] values = { stbatchdate, endbatchdate};
                ds = ClsDb.Get_DS_BySPArr("USP_HTS_GET_TrialLetterBatchReport", keys, values);
                return ds;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}

using System;
using System.Data;
using System.IO;
using System.Text;
using System.Collections;

namespace LntechLocalCaching
{
   /// <summary>
   /// Provide a means of storing and retrieving data
   /// to avoid constant re-transmission 
   /// </summary>
   public interface ICacheService
   {
      /// <summary>
      /// Find the latest date and version of the requested cached data
      /// </summary>
      /// <param name="name">Name of the cache</param>
      /// <param name="paramhash">Hash of the parameters that generated the data</param>
      /// <returns>The date and version of the data that's currently stored,
      /// or null if it's not stored</returns>
      DateAndVersion getDateAndVersion( string name, string paramhash );

      /// <summary>
      /// Retrieve the cached DataSet having the given name and 
      /// parameter hash
      /// </summary>
      /// <param name="name">Name of the cache</param>
      /// <param name="paramhash">Hash of the parameters that generated the data</param>
      /// <param name="MyType">typeof() the dataset type to be returned -- and it better be a DataSet!</param>
      /// <returns>The dataset that was instantiated and loaded</returns>
      System.Data.DataSet retrieveDataset( string name, string paramhash, System.Type MyType ) ;

	   /// <summary>
	   /// Retrieve the cached DataView having the given name and 
	   /// parameter hash
	   /// </summary>
	   /// <param name="name">Name of the cache</param>
	   /// <param name="paramhash">Hash of the parameters that generated the data</param>
	   /// <param name="MyType">typeof() the dataset type to be returned -- and it better be a DataSet!</param>
	   /// <returns>The dataset that was instantiated and loaded</returns>
	   System.Data.DataView  retrieveDataView( string name, string paramhash, System.Type MyType ) ;

	   
	   /// <summary>
      /// Find and open a file containing the cached data having 
      /// the given name and parameter hash
      /// </summary>
      /// <param name="name">Name of the cache</param>
      /// <param name="paramhash">Hash of the parameters that generated the data</param>
      /// <returns>A stream object from which the stored data can be read,
      /// or null if no data is found</returns>
      FileStream retrieveStream( string name, string paramhash );

      /// <summary>
      /// Stores a dataset into the cache, associating the given name, paramhash, freshness, and version
      /// </summary>
      /// <param name="name">Name of the cache</param>
      /// <param name="paramhash">Hash of the parameters that generated the data</param>
      /// <param name="freshness">The date and version of the data being stored</param>
      /// <param name="mydata">The dataset to be stored</param>
      void storeDataset( string name, string paramhash, DateAndVersion freshness, DataSet mydata);


	   /// <summary>
	   /// Stores a dataview into the cache, associating the given name, paramhash, freshness, and version
	   /// </summary>
	   /// <param name="name">Name of the cache</param>
	   /// <param name="paramhash">Hash of the parameters that generated the data</param>
	   /// <param name="freshness">The date and version of the data being stored</param>
	   /// <param name="mydata">The dataset to be stored</param>
	   void storeDataView( string name, string paramhash, DateAndVersion freshness, DataView mydata);



      /// <summary>
      /// Adds a cache entry, associating the given name, paramhash, freshness, and version,
      /// and returning a filestream into which an object can be persisted
      /// </summary>
      /// <param name="name">Name of the cache</param>
      /// <param name="paramhash">Hash of the parameters that generated the data</param>
      /// <param name="freshness">The date and version of the data being stored</param>
      /// <returns>A filestream into which the data can be persisted</returns>
      FileStream getStoreStream( string name, string paramhash, DateAndVersion freshness );

      /// <summary>
      /// Empty all cached data
      /// </summary>
      void clearCache();

      /// <summary>
      /// Empty the cache of all data added after the given timestamp
      /// </summary>
      /// <param name="minbirth"></param>
      void clearCache( DateTime minbirth );

      /// <summary>
      /// Empty the cache of all data having the given name (regardless of parameters and freshness)
      /// </summary>
      /// <param name="name">The name of the cache entries to purge</param>
      void clearCache( string name );

      /// <summary>
      /// For a given cached name, remove all entries whose version differs from the one specified
      /// </summary>
      /// <param name="name">The name of the cache entries to purge</param>
      /// <param name="GoodVersion">The version of those entries that should be KEPT; 
      /// any entries with a different value will be purged</param>
      void clearCache( string name, string GoodVersion );

      /// <summary>
      /// Remove a particular set of data, having the given name and 
      /// parameter hash. Useful after an update invalidates the data.
      /// </summary>
      /// <param name="name">Name of the cache</param>
      /// <param name="paramhash">Hash of the parameters that generated the data</param>
      void unStoreCache( string name, string paramhash );

      /// <summary>
      /// Not for general use -- intended only for a cache manager plugin to obtain statistics
      /// </summary>
      /// <returns>Dataset (actually a SHI.WebTeam.SalesTools.SalesCenterII.UI.SCIIMainMenu.DSContent)
      /// that lists all cached items)</returns>
      System.Data.DataSet RetrieveCacheIndex();

      /// <summary>
      /// Return all of the parameter keys currently stored for a single named cache
      /// </summary>
      /// <param name="name"></param>
      /// <returns></returns>
      System.Collections.Specialized.StringCollection  ListParams( string name );

   }


   /// <summary>
   /// Encapsulates the data that describes the "freshness" of a cache entry
   /// </summary>
   public class DateAndVersion
   {
      private DateTime  m_Timestamp;
      private string    m_Version;

      public DateAndVersion()
      {
         //
      }

      public DateAndVersion( DateTime Timestamp, string Version )
      {
         m_Timestamp = Timestamp;
         m_Version = Version;
      }

      public DateAndVersion( DateAndVersion dav )
      {
         m_Timestamp = dav.Timestamp;
         m_Version = dav.Version;
      }

      public DateTime Timestamp
      {
         get
         {
            return m_Timestamp;
         }
         set
         {
            m_Timestamp = value;
         }
      }

      public string Version 
      {
         get
         {
            return m_Version;
         }
         set
         {
            m_Version = value;
         }
      }

      /// <summary>
      /// How long ago was the timestamp (in seconds)
      /// Notice this uses UTC time for the current time!!!!!
      /// </summary>
      public double Age
      {
         get
         {
            return DateTime.UtcNow.Subtract(m_Timestamp).TotalSeconds;
         }
      }

      public override bool Equals(object rhs)
      {
         // lhs can't be null, so if rhs is, there's inequality
         if ( Object.Equals(rhs, null) )
         {
            return false;
         }
         if ( rhs is DateAndVersion )
         {
            DateAndVersion dav = (DateAndVersion)rhs;
            return ( (this.Timestamp == dav.Timestamp) && (this.Version == dav.Version) );
         }
         else
         {
            return false;
         }
      }

      public static bool Equals( DateAndVersion rhs1, DateAndVersion rhs2 )
      {
         // references to the same object are equal
         if ( Object.Equals( rhs1, rhs2 ) )
         {
            return true;
         }
         // if either is null (and they aren't both null because of above), they're unequal
         if ( object.Equals(rhs1, null) || Object.Equals(rhs2,null) )
         {
            return false;
         }
         // defer to the Equals method of the first operand
         return rhs1.Equals( rhs2 );
      }
      public static bool operator ==( DateAndVersion rhs1, DateAndVersion rhs2 )
      {
         return Equals( rhs1, rhs2 );
      }
      public static bool operator !=( DateAndVersion rhs1, DateAndVersion rhs2 )
      {
         return !Equals( rhs1, rhs2 );
      }

      public override int GetHashCode()
      {
         return base.GetHashCode ();
      }

         

   }


   /// <summary>
   /// Handy means of accumulating parameters, and escaping them to avoid illegal characters
   /// </summary>
   public class ParamHash
   {
      private const string cSeparator = "|";
      private ArrayList myValues;

      public ParamHash()
      {
         myValues = new ArrayList();
      }

      public override string ToString()
      {
         /*
         string val = HttpUtility.UrlEncode( myValue.ToString() );
         return val.Replace("'", "&apos;");
         */
         StringBuilder myValue = new StringBuilder();
         foreach (object par in myValues)
         {
            if (par != null)
            {
               myValue.Append( par.ToString() );
            }
            myValue.Append( cSeparator );
         }

         string val = myValue.ToString();
         // Apply AsciiEncoding to 'message' string to obtain it as an array of bytes.
         Encoding ascii = Encoding.ASCII;
         byte[] byteArray = new byte[ascii.GetByteCount(val)];
         byteArray = ascii.GetBytes(val);

         // Performing Base64 transformation.
         return Convert.ToBase64String(byteArray);
      }

      public static string Decode( string encoded )
      {
         byte[] b = Convert.FromBase64String( encoded );

         System.Text.StringBuilder s = new System.Text.StringBuilder( b.Length );
         for (int i = 0; i < b.Length; i++)
         {
            s.Append( System.Convert.ToChar( b[i] ) );
         }

         return s.ToString();
      }

      public static ParamHash operator +(ParamHash rv1, ParamHash rv2)
      {
         return ParamHash.Add( rv1, rv2 );
      }
      public static ParamHash Add(ParamHash rv1, ParamHash rv2)
      {
         ParamHash ret = new ParamHash();
         ret.myValues.InsertRange( 0, rv1.myValues );
         ret.myValues.InsertRange( ret.myValues.Count, rv2.myValues );

         return ret;
      }

      public void Append( string newValue )
      {
         myValues.Add( newValue );
      }

      public void Append( object[] paramlist )
      {
         foreach ( object par in paramlist )
         {
            if (par != null)
            {
               myValues.Add( par.ToString() );
            }
            else
            {
               myValues.Add(null);
            }
         }
      }


   }


}

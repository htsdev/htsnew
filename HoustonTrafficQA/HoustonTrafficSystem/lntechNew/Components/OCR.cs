using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;

namespace lntechNew.Components
{
    public class OCR
    {
        clsENationWebComponents clsDb = new clsENationWebComponents();

        #region Variables

        private int ocrID = 0;
        private int picID = 0;
        private string firstname = String.Empty;
        private string lastname = String.Empty;
        private string address = String.Empty;
        private string city = String.Empty;
        private string state = String.Empty;
        private int zip = 0;
        private string causeno = String.Empty;
        private string status = String.Empty;
        private string newcourtdate=String.Empty;
        private string todaydate=String.Empty;
        private string time=String.Empty;
        private string courtno = String.Empty;
        private string location = String.Empty;
        private DateTime updatedatescan;
        private DateTime courtdatescan;
        private DateTime _CourtDate;
        private DateTime _CourtDateMain;
        private int _checkStatus = 0;
        private string ocrdata = String.Empty;
        private string typeScan = String.Empty;
        #endregion 

        #region Properties

        public int OCRID
        {
            get
            {
                return ocrID;
            }
            set
            {
                ocrID = value;
            }
        }
        public int PicID
        {
            get
            {
                return picID;
            }
            set
            {
                picID = value;
            }
        }
        public string FirstName
        {
            get
            {
                return firstname;
            }
            set
            {
                firstname = value;
            }
        }
        public string LastName
        {
            get
            {
                return lastname;
            }
            set
            {
                lastname = value;
            }
        }
        public string Address
        {
            get
            {
                return address;
            }
            set
            {
                address = value;
            }
        }
        public string City
        {
            get
            {
                return city;
            }
            set
            {
                city = value;
            }
        }
        public string State
        {
            get
            {
                return state;
            }
            set
            {
                state = value;
            }
        }
        public int Zip
        {
            get
            {
                return zip;
            }
            set
            {
                zip = value;
            }
        }
        public string CauseNo
        {
            get
            {
                return causeno;
            }
            set
            {
                causeno = value;
            }
        }
        public string Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }
        public string NewCourtDate
        {
            get
            {
                return newcourtdate;
            }
            set
            {
                newcourtdate = value;
            }
        }
        public string TodayDate
        {
            get
            {
                return todaydate;
            }
            set
            {
                todaydate = value;
            }
        }
        public string CourtNo
        {
            get
            {
                return courtno;
            }
            set
            {
                courtno = value;
            }
        }
        public string Location
        {
            get
            {
                return location;
            }
            set
            {
                location = value;
            }
        }
        public string Time
        {
            get
            {
                return time;
            }
            set
            {
                time = value;
            }
        }
        public DateTime ScanUpdateDate
        {
            get
            {
                return updatedatescan;
            }
            set
            {
                updatedatescan = value;
            }
        }
        public DateTime CourtDateScan
        {
            get
            {
                return courtdatescan;
            }
            set
            {
                courtdatescan = value;
            }
        }
        public DateTime CourtDate
        {
            get
            {
                return _CourtDate;
            }
            set
            {
                _CourtDate = value;
            }
        }
        public DateTime CourtDateMain
        {
            get
            {
                return _CourtDateMain;
            }
            set
            {
                _CourtDateMain = value;
            }
        }
        public int CheckStatus
        {
            get
            {
                return _checkStatus;
            }
            set
            {
                _checkStatus = value;
            }
        }
        public string OCRData
        {
            get
            {
                return ocrdata;
            }
            set
            {
                ocrdata = value;
            }
        }

        public string TypeScan
        {
            get
            {
                return typeScan;
            }
            set
            {
                typeScan = value;
            }
        }
        #endregion 

        #region Methods

        public void InsertInOCR()
        {
            try
            {
                string[] key ={ "@PicID", "@CauseNo", "@Status", "@NewCourtDate", "@TodayDate", "@Location", "@Time", "@CourtNo", "@AdminVerified", "@ScanVerified","@CheckStatus","@OcrData" };
                object[] value1 ={ PicID,CauseNo,Status,NewCourtDate,TodayDate,Location,Time,CourtNo,0,0,CheckStatus,OCRData};
                clsDb.InsertBySPArr("usp_WebScan_InsertInOCR_NEW", key, value1);   
            }
            catch (Exception ex)
            { 
            }
        }

        public void InsertInOCRII()
        {
            try
            {
                string[] key ={ "@PicID", "@CauseNo", "@Status", "@NewCourtDate", "@TodayDate", "@Location", "@Time", "@CourtNo", "@AdminVerified", "@ScanVerified", "@CheckStatus", "@OcrData","@TypeScan" };
                object[] value1 ={ PicID, CauseNo, Status, NewCourtDate, TodayDate, Location, Time, CourtNo, 0, 0, CheckStatus, OCRData, TypeScan };
                clsDb.InsertBySPArr("usp_WebScan_InsertInOCR_NEW", key, value1);
            }
            catch (Exception ex)
            {
            }
        }

        public void InsertPendingOCR()
        {
            try
            {
                string[] key ={ "@PicID", "@CheckStatus","@Location","@Status" ,"@OcrData"};
                object[] value1 ={ PicID,  CheckStatus,Location,Status,OCRData };
                clsDb.InsertBySPArr("usp_WebScan_InsertPending_NEW", key, value1);
            }
            catch (Exception ex)
            {
            }
        }
        public void InsertPendingOCRII()
        {
            try
            {
                string[] key ={ "@PicID", "@CheckStatus", "@Location", "@Status", "@OcrData","@TypeScan" };
                object[] value1 ={ PicID, CheckStatus, Location, Status, OCRData, TypeScan };
                clsDb.InsertBySPArr("usp_WebScan_InsertPending_NEW", key, value1);
            }
            catch (Exception ex)
            {
            }
        }
        public DataSet GetRecords(string picid)
        {
            DataSet ds = null;
            try
            {
                string[] key ={ "@PicID"};
                object[] value1 ={ Convert.ToInt32(picid)};
                ds = clsDb.Get_DS_BySPArr("usp_WebScan_GetRecordsByPicID",key,value1);
                return ds;
            }
            catch (Exception ex)
            {
                return ds;
            }
        }
        public int UpdateRecords()
        {
            int flag = 0;
            try
            {
                string[] key ={ "@PicID", "@CauseNo", "@NewCourtDate", "@TodayDate", "@Time", "@Status", "@Location", "@CourtNo", "@ScanUpdateDate", "@CourtDateScan", "@Flag" };
                object[] value1 ={ PicID,CauseNo,NewCourtDate,TodayDate,Time,Status,Location,CourtNo,ScanUpdateDate,CourtDateScan,0};
                
                flag=(int)clsDb.InsertBySPArrRet("usp_WebScan_Update",key,value1);
                
                return flag;
            }
            catch (Exception ex)
            {
                return flag;
            }
        }
        public DataSet GetDataSet(string BatchID)
        {
            DataSet ds = null;
            try
            {
                string[] key ={ "@BathID" };
                object[] value1 ={ Convert.ToInt32(BatchID) };
                ds = clsDb.Get_DS_BySPArr("usp_WebScan_DataSetForNavigation", key, value1);
                return ds;
            }
            catch (Exception ex)
            {
                return ds;
            }
        }
        public DataSet GetDataSetMovePrev(string BatchID)
        {
            DataSet ds = null;
            try
            {
                string[] key ={ "@BathID" };
                object[] value1 ={ Convert.ToInt32(BatchID) };
                ds = clsDb.Get_DS_BySPArr("usp_WebScan_DataSetMovePrev", key, value1);
                return ds;
            }
            catch (Exception ex)
            {
                return ds;
            }
        }
        public void UpdateMain()
        {
            try
            {
                string[] key ={ "@PicID", "@CauseNo", "@Status", "@Location", "@CourtNo", "@ScanUpdateDate", "@CourtDateMain" };
                object[] value1 ={PicID, CauseNo,Status,Location,CourtNo,ScanUpdateDate,CourtDateScan};
                clsDb.ExecuteSP("usp_webscan_UpdateMain", key, value1);

            }
            catch (Exception ex)
            { 
            
            }
        }
        #endregion 
    }
}

using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using System.Collections;

namespace HTP.Components
{
    public class NoMailFlag
    {
        #region Variables
        private string _Firstname = String.Empty;
        private string _Lastname = String.Empty;
        private string _Address = String.Empty;
        private string _City = String.Empty;
        private int _StateID = 0;
        private string _ZipCode = String.Empty;
        private int _LetterID = 0;
        private int _DBId = 0;
        private int _RecordId = 0;
        private int _updatesource = 0;
        private string _TicketNumber = String.Empty;
        private string _State = String.Empty;
        private clsENationWebComponents ClsDb = new clsENationWebComponents("Connection String");
        private lntechNew.Components.ZP4Wrapper ProcessYDS = new lntechNew.Components.ZP4Wrapper();
        private clsLogger clog = new clsLogger();
        #endregion

        #region Constructors

        public NoMailFlag(int Letter_ID)
        {
            this.LetterID = Letter_ID;
        }
        public NoMailFlag(string TicketNum)
        {
            this.TicketNumber = TicketNum;
        }

        public NoMailFlag(string FName, string LName, string Addr, string Cty, int State_ID, string StateName, string Zip)
        {
            this.FirstName = FName;
            this.LastName = LName;
            this.Address = Addr;
            this.City = Cty;
            this.StateID = State_ID;
            this.State_ShortName = StateName;
            this.ZipCode = Zip;
        }

        // Abbas Qamar  9726 11/01/2011 
        public NoMailFlag(string FName, string LName, string Addr, string Cty, int State_ID, string Zip,int updatesource)
        {
            this.FirstName = FName;
            this.LastName = LName;
            this.Address = Addr;
            this.City = Cty;
            this.StateID = State_ID;
            this.UpdateSource=updatesource;
            this.ZipCode = Zip;
        }

        public NoMailFlag(string FName, string LName, string Addr, string Cty, int State_ID, string Zip )
        {
            this.FirstName = FName;
            this.LastName = LName;
            this.Address = Addr;
            this.City = Cty;
            this.StateID = State_ID;
            this.ZipCode = Zip;
        }

        // Abbas Qamar  9726 10/26/2011 
        public NoMailFlag(string Addr, string Cty, int State_ID, string StateName, string Zip)
        {
            this.Address = Addr;
            this.City = Cty;
            this.StateID = State_ID;
            this.State_ShortName = StateName;
            this.ZipCode = Zip;
        }

        // Abbas Qamar  9726 10/26/2011 

        public NoMailFlag(int dbid, int recordid)
        {
            this.DBId = dbid;
            this.RecordId = recordid;
            
        }
        public NoMailFlag(string FName, string LName, string Addr, string Cty, string StateName, string Zip, string TicketNum, int dbid, int recordid)
        {
            this.DBId = dbid;
            this.RecordId = recordid;
            this.FirstName = FName;
            this.LastName = LName;
            this.Address = Addr;
            this.City = Cty;
            this.State_ShortName = StateName;
            this.ZipCode = Zip;
            this.TicketNumber = TicketNum;
        }


        //Sabir Khan 6167 07/19/2009 Contructor for Do Not Mail Update...
        public NoMailFlag(string FName, string LName, string Addr, string Cty, string StateName, string Zip, int dbid, int recordid)
        {
            this.DBId = dbid;
            this.RecordId = recordid;
            this.FirstName = FName;
            this.LastName = LName;
            this.Address = Addr;
            this.City = Cty;
            this.State_ShortName = StateName;
            this.ZipCode = Zip;            
        }

        #endregion

        #region Properties

        public string FirstName
        {
            get { return _Firstname; }
            set { _Firstname = value; }
        }
        public string LastName
        {
            get { return _Lastname; }
            set { _Lastname = value; }
        }
        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }
        public string City
        {
            get { return _City; }
            set { _City = value; }
        }
        public int StateID
        {
            get { return _StateID; }
            set { _StateID = value; }
        }

        public int UpdateSource
        {
            get { return _updatesource; }
            set { _updatesource = value; }
        }

        public string ZipCode
        {
            get { return _ZipCode; }
            set { _ZipCode = value; }
        }
        public string State_ShortName
        {
            get { return _State; }
            set { _State = value; }
        }
        public int LetterID
        {
            get { return _LetterID; }
            set { _LetterID = value; }
        }
        public string TicketNumber
        {
            get { return _TicketNumber; }
            set { _TicketNumber = value; }
        }

        public int DBId
        {
            get { return _DBId; }
            set { _DBId = value; }
        }

        public int RecordId
        {
            get { return _RecordId; }
            set { _RecordId = value; }
        }

        #endregion

        #region Methods


        //tahir 4418 on 07/21/2008
        /// <summary>
        /// This function is used to check if the specified record already exists in our
        /// no mailing list.
        /// </summary>
        /// <param name="FirstName">First Name of the person</param>
        /// <param name="lastName">Last Name of the person</param>
        /// <param name="Address">Mailing address of the person</param>
        /// <param name="Zip">Zip code associated with the mailing address of the person</param>
        /// <returns>returns true if it exists in our no-mailing list otherwise false </returns>
        private bool IsAlreadyInList(string FirstName, string LastName, string Address, string Zip)
        {
            string[] keys = { "@FirstName", "@LastName", "@Address", "Zip" };
            object[] values = { FirstName, LastName, Address, Zip };
            DataTable dt = ClsDb.Get_DT_BySPArr("USP_HTS_NOMAILFLAG_Check_Existing", keys, values);

            if (Convert.ToInt32(dt.Rows[0]["RecCount"]) > 0)
            {
                return true;
            }
            else
                return false;
        }

        //tahir 4418 on 07/21/2008
        /// <summary>
        /// This function is used to set the donot mail flag for the selected person
        /// and to add the name & address of the person in our no-mailing list.
        /// </summary>
        /// <param></param>
        /// <returns>returns the update status:
        ///     1: updated, 
        ///     2: address not verified 
        ///     3: Already in no mail list
        ///     </returns>
        public int UpdateFlag()
        {
            int result = 0;
            string[] keys = { "@DBId", "@RecordId" };
            object[] values = { this.DBId, this.RecordId };
            if (!IsAlreadyInList(this.FirstName, this.LastName, this.Address, this.ZipCode))
            {
                ClsDb.ExecuteSP("USP_HTP_Update_SetNoMailFlag", keys, values);
                result = 1;
            }
            else
            {
                result = 3;
            }

            return result;
        }


        //tahir 4418 on 07/21/2008
        /// <summary>
        /// This function is used to search records in various databases by looking up the 
        /// ticket number.
        /// </summary>
        /// <param></param>
        /// <returns>returns the matching records that contains
        ///  name, address, matter type, case number, record identification in database
        ///     </returns>
        public DataSet GetDataByTicketNumber()
        {
            string[] keys = { "@TicketNumber" };
            object[] values = { this.TicketNumber };
            DataSet ds = ClsDb.Get_DS_BySPArr("USP_HTS_CheckAll_NOMAILFLAG_ChkByTicNum", keys, values);
            return ds;
        }

        //tahir 4418 on 07/21/2008
        /// <summary>
        /// This function is used to search records in various databases by looking up the 
        /// mailer ID.
        /// </summary>
        /// <param></param>
        /// <returns>returns the matching records that contains
        ///  name, address, matter type, case number, record identification in database
        ///     </returns>
        public DataSet GetDataByLetterId()
        {
            string[] keys = { "@LetterId" };
            object[] values = { this.LetterID };
            DataSet ds = ClsDb.Get_DS_BySPArr("usp_hts_CheckAll_NOMAILFLAG_ChkByLetterID", keys, values);
            return ds;
        }

        //tahir 4418 on 07/21/2008
        /// <summary>
        /// This function is used to search records in various databases by looking up
        /// person's name & address.
        /// </summary>
        /// <param></param>
        /// <returns>returns the matching records that contains
        ///  name, address, matter type, case number, record identification in database
        ///     </returns>
        public DataSet GetDataByDetails()
        {
            string[] keys = { "@FirstName", "@LastName", "@Address", "@Zip", "@City", "@stateid_fk" };
            object[] values = { this.FirstName, this.LastName, this.Address, this.ZipCode, this.City, this.StateID };
            DataSet ds = ClsDb.Get_DS_BySPArr("usp_hts_CheckAll_NoMailFlag_ChkByDetails", keys, values);
            return ds;
        }



        //Abbas Qamar  9726 10/26/2011 
        /// <summary>
        /// This function is used to search records in various databases by looking up
        /// person's address & City & zipcode.
        /// </summary>
        /// <param></param>
        /// <returns>returns the matching records that contains
        ///  address, Zip , City , record identification in database
        ///     </returns>
        public DataSet GetDatabyAddress()
        {
            string[] keys = { "@Address", "@Zip", "@City", "@stateid_fk" };
            object[] values = { this.Address, this.ZipCode, this.City, this.StateID };
            DataSet ds = ClsDb.Get_DS_BySPArr("usp_hts_CheckAll_NoMailFlag_ChkByAddress", keys, values);
            return ds;
        }



        //Abbas Qamar  9726 10/26/2011 
        /// <summary>
        /// This function is used to search records in various databases by looking up
        /// person's address & City & zipcode.
        /// </summary>
        /// <param></param>
        /// <returns>returns the matching records that contains
        ///  address, Zip , City , record identification in database
        ///     </returns>
        public DataSet SearchAndAddAsBadMail()
        {
            string[] keys = { "@FirstName", "@LastName", "@Address", "@Zip", "@City", "@stateid_fk", "@UpdateSource" };
            object[] values = { this.FirstName, this.LastName, this.Address, this.ZipCode, this.City, this.StateID,this.UpdateSource };
            DataSet ds = ClsDb.Get_DS_BySPArr("USP_hts_Add_donotmail_records", keys, values);
            return ds;
        }

        //Abbas Qamar  9726 10/26/2011 
        /// <summary>
        /// This function is used to search records in various databases by looking up
        /// person's address & City & zipcode.
        /// </summary>
        /// <param></param>
        /// <returns>returns the matching records that contains
        ///  address, Zip , City , record identification in database
        ///     </returns>
        public DataSet SearchAndAddAsBadMailByAddress()
        {
            string[] keys = { "@FirstName", "@LastName", "@Address", "@Zip", "@City", "@stateid_fk", "@UpdateSource" };
            object[] values = { this.FirstName, this.LastName, this.Address, this.ZipCode, this.City, this.StateID, this.UpdateSource };
            DataSet ds = ClsDb.Get_DS_BySPArr("USP_hts_Add_donotmail_records_ByAddress", keys, values);
            return ds;
        }


        #endregion
    }
}

using System;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.Components
{
    /// <summary>
    /// This enum is used to define some of the validation reports used in HTP
    /// </summary>
    public enum ValidationReport
    {
        /// <summary>
        /// OutSideCourtAppearance
        /// </summary>
        OutSideCourtAppearance,
        /// <summary>
        /// PastCourtDates
        /// </summary>
        PastCourtDates,//added khalid 17-10-07
        /// <summary>
        /// BondWaitingReport
        /// </summary>
        BondWaitingReport,
        /// <summary>
        /// HMCsubmittedcases
        /// </summary>
        HMCsubmittedcases,
        /// <summary>
        /// BondWaiting
        /// </summary>
        BondWaiting,
        /// <summary>
        /// ALRRequest
        /// </summary>
        ALRRequest,
        /// <summary>
        /// MissingCauseNumber
        /// </summary>
        MissingCauseNumber,
        /// <summary>
        /// HcjpAutoUpdateReport
        /// </summary>
        HcjpAutoUpdateReport, // Noufil 4748 09/08/2008 new Enum added for HCJP Auto Update Report
        // Zahoor 4845 09/24/08
        /// <summary>
        /// CourtArrignmentAppearanceAlert
        /// </summary>
        CourtArrignmentAppearanceAlert,
        //Yasir Kamal 5363 12/30/2008   HMC Missing X-Ref Report added
        /// <summary>
        /// HMCMissingXRefReport
        /// </summary>
        HMCMissingXRefReport,
        //Yasir 5423 02/11/2009 cases having follow up date in next 6 business days
        /// <summary>
        /// BondWaitingViolations
        /// </summary>
        BondWaitingViolations,
        //5423 end
        //Yasir Kamal 6030 06/24/2009 Null Status Report added.
        /// <summary>
        /// NullStatusReport
        /// </summary>
        NullStatusReport,
        //Fahad 7234 02/24/2010 New Report Added
        /// <summary>
        /// HmcSameDayArraignment
        /// </summary>
        HmcSameDayArraignment,
        // Afaq 8521 12/14/2010 New report added.
        /// <summary>
        /// Potential Bond Forfeitures.
        /// </summary>
        PotentialBondForfeitures
    }

    /// <summary>
    /// This Class is used to get validation reports
    /// </summary>
    public class ValidationReports
    {

        #region Variables

        clsENationWebComponents clsdb = new clsENationWebComponents();
        clsSession cSession = new clsSession();
        /// <summary>
        /// Use to hold dataset
        /// </summary>
        public DataSet records;
        bool hasParameter = false;
        /// <summary>
        /// string array of keys.
        /// </summary>
        public string[] keys;
        /// <summary>
        /// Object array of values.
        /// </summary>
        public object[] values;
        //khalid 20-10-07
        // int pgindex = 0;
        int pgsize = 0;

        #endregion

        #region Properties
        /// <summary>
        /// HasParameter
        /// </summary>
        public bool HasParameter
        {
            get { return hasParameter; }
            set { hasParameter = value; }
        }

        #endregion

        /// <summary>
        /// getReportData
        /// </summary>
        /// <param name="gv_records"></param>
        /// <param name="errormessage"></param>
        /// <param name="reporttype"></param>
        public void getReportData(GridView gv_records, Label errormessage, ValidationReport reporttype)
        {
            //Fahad 5530 03/16/2009 Comments PageIndex and PageSize already done in calling page and if clause is added
            //pgindex = gv_records.PageIndex;
            //pgsize = gv_records.PageSize;
            if (gv_records != null && errormessage != null)
            {
                switch (reporttype)
                {
                    case ValidationReport.OutSideCourtAppearance: getRecords("usp_hts_outsidecases"); break;
                    case ValidationReport.PastCourtDates: getRecords("usp_hts_past_court_date_report"); break;//procedure by fahad //added 17-10-07
                    case ValidationReport.BondWaitingReport: getRecords("Usp_hts_bondwaiting_report"); break;//procedure by fahad //added 17-10-07
                    case ValidationReport.HMCsubmittedcases: getRecords("usp_hts_HMC_AW_DLQ_Alert"); break;//khalid 24-10-07
                    case ValidationReport.BondWaiting: getRecords("USP_HTS_Get_BondWaitingViolations"); break;//Aziz 26-10-07
                    case ValidationReport.ALRRequest: getRecords("usp_ALR_Request_Alert"); break;
                    //Muhammad Ali 7747 07/26/2010 --> pass URL to SP.
                    case ValidationReport.MissingCauseNumber:
                        {
                            string url = HttpContext.Current.Request.Url.AbsolutePath.ToString();
                            string[] key = { "@ReportURL" };
                            object[] value = { url };
                            getRecords("USP_HTS_GET_MissingCauseNumberReport", key, value); break;
                        }

                    case ValidationReport.HcjpAutoUpdateReport: getRecords("USP_HTP_HcjpAutoReport"); break;// Noufil 4748 09/08/2008 new case added for HCJP Auto Update Report
                    // Zahoor 4845 09/24/08
                    case ValidationReport.CourtArrignmentAppearanceAlert: getRecords("USP_HTP_Get_CourtArrignmantAppearance_Report"); break;

                    //testing only bc procedure not created
                    // case ValidationReport.HMCsubmittedcases: getRecords("usp_hts_past_court_date_report"); break;//khalid 24-10-07
                    case ValidationReport.HMCMissingXRefReport: getRecords("USP_HTP_Get_MissingMidNumbers"); break;  //Yasir Kamal 5363 12/30/2008   HMC Missing X-Ref Report added
                    //Yasir 5423 02/11/2009 cases having follow up date in next 6 business days
                    case ValidationReport.BondWaitingViolations: getRecords("USP_HTS_Get_BondWaitingViolations"); break;
                    //5423 end
                    //Yasir Kamal 6030 06/24/2009 Null Status Report Added.
                    case ValidationReport.NullStatusReport: getRecords("USP_HTP_NullStatusReport"); break;
                    //Fahad 7234 02/24/2010 New Report Case Added
                    case ValidationReport.HmcSameDayArraignment: getRecords("USP_HTP_Get_HMCSameDayArr"); break;
                    // Afaq 8521 12/14/2010 New report added.
                    case ValidationReport.PotentialBondForfeitures: getRecords("usp_htp_get_potentialBond"); break;

                }
                if (reporttype == ValidationReport.PastCourtDates)
                {
                    displayPastRecords(gv_records, errormessage);
                }
                displayRecords(gv_records, errormessage);
            }
        }
        /// <summary>
        /// getReportData
        /// </summary>
        /// <param name="gv_records"></param>
        /// <param name="errormessage"></param>
        /// <param name="reporttype"></param>
        /// <param name="newindex"></param>
        public void getReportData(GridView gv_records, Label errormessage, ValidationReport reporttype, int newindex)
        {

            switch (reporttype)
            {
                case ValidationReport.OutSideCourtAppearance: getRecords("usp_hts_outsidecases"); break;
                case ValidationReport.PastCourtDates: getRecords("usp_hts_past_court_date_report"); break;//procedure by fahad
                case ValidationReport.BondWaitingReport: getRecords("Usp_hts_bondwaiting_report"); break;//procedure by fahad //added 17-10-07
                case ValidationReport.HMCsubmittedcases: getRecords("usp_hts_HMC_AW_DLQ_Alert"); break;//khalid 24-10-07
                case ValidationReport.BondWaiting: getRecords("USP_HTS_Get_BondWaitingViolations"); break;//Aziz 26-10-07
                //Muhammad Ali 7747 07/26/2010 --> pass URL to SP.
                case ValidationReport.MissingCauseNumber:
                    {
                        string url = HttpContext.Current.Request.Url.AbsolutePath.ToString();
                        string[] key = { "@ReportURL" };
                        object[] value = { url };
                        getRecords("USP_HTS_GET_MissingCauseNumberReport", key, value); break;
                    }
                case ValidationReport.HcjpAutoUpdateReport: getRecords("USP_HTP_HcjpAutoReport"); break;// Noufil 4748 09/08/2008 new case added for HCJP Auto Update Report
                case ValidationReport.HMCMissingXRefReport: getRecords("USP_HTP_Get_MissingMidNumbers"); break;  //Yasir Kamal 5363 12/30/2008   HMC Missing X-Ref Report added
                //Yasir 5423 02/11/2009 cases having follow up date in next 6 business days
                case ValidationReport.BondWaitingViolations: getRecords("USP_HTS_Get_BondWaitingViolations"); break;
                //5423 end
                //testing only bc procedure not created
                //case ValidationReport.HMCsubmittedcases: getRecords("usp_hts_past_court_date_report"); break;//khalid 24-10-07

            }
            gv_records.PageIndex = newindex;
            pgsize = gv_records.PageSize;
            if (reporttype == ValidationReport.PastCourtDates)
            {
                displayPastRecords(gv_records, errormessage);
            }
            displayRecords(gv_records, errormessage);
        }

        /// <summary>
        /// getRecords
        /// </summary>
        /// <param name="procedurename"></param>
        public void getRecords(string procedurename)
        {
            try
            {
                records = new DataSet();

                if (HasParameter)
                {
                    records = clsdb.Get_DS_BySPArr(procedurename, keys, values);
                }
                else
                {
                    records = clsdb.Get_DS_BySP(procedurename);
                }
            }
            catch
            {
                records = new DataSet();
                records.Tables.Add();
            }
        }
        /// <summary>
        /// getRecords
        /// </summary>
        /// <param name="procedurename"></param>
        /// <param name="keys"></param>
        /// <param name="values"></param>
        public void getRecords(string procedurename, string[] keys, object[] values)
        {
            try
            {
                records = new DataSet();
                records = clsdb.Get_DS_BySPArr(procedurename, keys, values);

            }
            catch
            {
                records = new DataSet();
                records.Tables.Add();
            }
        }

        /// <summary>
        /// displayRecords
        /// </summary>
        /// <param name="gv_records"></param>
        /// <param name="errormessage"></param>
        private void displayRecords(GridView gv_records, Label errormessage)
        {
            try
            {
                if (records.Tables[0].Rows.Count > 0)
                {
                    errormessage.Text = "";
                    generateSerialNo();
                    //generateAllSerialNo();
                    gv_records.DataSource = records.Tables[0];
                    gv_records.DataBind();
                }
                else
                {
                    errormessage.Text = "No Record Found";
                    gv_records.DataSource = records.Tables[0];
                    gv_records.DataBind();
                }
            }
            catch
            {

            }
        }
        /// <summary>
        /// displayPastRecords
        /// </summary>
        /// <param name="gv_records"></param>
        /// <param name="errormessage"></param>
        private void displayPastRecords(GridView gv_records, Label errormessage)
        {
            if (records.Tables[0].Rows.Count > 0)
            {
                errormessage.Text = "";
                generateAllSerialNo();
                gv_records.DataSource = records.Tables[0];
                gv_records.DataBind();
            }
            else
            {
                errormessage.Text = "No Record Found";
                gv_records.DataSource = records.Tables[0];
                gv_records.DataBind();
            }
        }
        /// <summary>
        /// GenerateSerialNo
        /// </summary>
        public void generateSerialNo()
        {
            if (records.Tables[0].Columns.Contains("sno") == false)
            {
                records.Tables[0].Columns.Add("sno");
                int sno = 1;

                //added By Aziz to check if rows existed
                if (records.Tables[0].Rows.Count >= 1)
                    records.Tables[0].Rows[0]["sno"] = 1;

                if (records.Tables[0].Rows.Count >= 2)
                {
                    for (int i = 1; i < records.Tables[0].Rows.Count; i++)
                    {
                        if (records.Tables[0].Rows[i - 1]["ticketid_pk"].ToString() != records.Tables[0].Rows[i]["ticketid_pk"].ToString())
                        {
                            records.Tables[0].Rows[i]["sno"] = ++sno;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// generateAllSerialNo
        /// </summary>
        public void generateAllSerialNo()
        {
            if (records.Tables[0].Columns.Contains("sno") == false)
            {
                records.Tables[0].Columns.Add("sno");
                int sno = 0;
                records.Tables[0].Rows[0]["sno"] = 1;

                for (int i = 0; i < records.Tables[0].Rows.Count; i++)
                {
                    ++sno;
                    //sno = (sno % (pgsize+1));
                    //if (sno == 0) sno = 1;
                    records.Tables[0].Rows[i]["sno"] = sno;
                }
            }
        }
        /// <summary>
        /// GenerateSerialNo
        /// </summary>
        /// <param name="ds"></param>
        public void generateSerialNo(DataSet ds)
        {
            if (ds.Tables[0].Columns.Contains("sno") == false)
            {
                ds.Tables[0].Columns.Add("sno");
                int sno = 1;
                ds.Tables[0].Rows[0]["sno"] = 1;

                for (int i = 1; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (ds.Tables[0].Rows[i - 1]["ticketid_pk"].ToString() != ds.Tables[0].Rows[i]["ticketid_pk"].ToString())
                    {
                        ds.Tables[0].Rows[i]["sno"] = ++sno;
                    }

                }

            }

        }
        /// <summary>
        /// generateAllserials
        /// </summary>
        /// <param name="ds"></param>
        public void generateAllserials(DataSet ds)
        {
            if (ds.Tables[0].Columns.Contains("sno") == false)
            {
                ds.Tables[0].Columns.Add("sno");
                int sno = 1;
                ds.Tables[0].Rows[0]["sno"] = 1;

                for (int i = 1; i < ds.Tables[0].Rows.Count; i++)
                {

                    ds.Tables[0].Rows[i]["sno"] = ++sno;


                }

            }

        }
        //Added By fahad For Sorting
        /// <summary>
        /// GetRecords
        /// </summary>
        /// <returns></returns>
        public DataSet GetRecords()
        {
            DataSet DS = null;

            try
            {
                DS = clsdb.Get_DS_BySP("usp_hts_outsidecases");

                generateSerialNo(DS);
                return DS;
            }
            catch
            {

                return DS;
            }
        }



        /// <summary>
        ///     // Noufil 5691 Noufil
        ///     Get All HMC Clients whose Court Date has been Passed.
        /// </summary>
        /// <returns></returns>
        public DataTable GetRecords_PastCourtDates(int showAll)
        {
            if (showAll == 0 || showAll == 1)
            {
                DataTable dt = new DataTable();
                string[] key = { "@showAll" };
                object[] value = { showAll };
                dt = clsdb.Get_DT_BySPArr("USP_HTP_Get_PastCourtDateForHMC", key, value);
                if (dt != null)
                    return dt;
                else
                    throw new Exception("Resultset not return in correcrt format");
            }
            else
                throw new ArgumentException("Argument not in correcrt format");

        }

        /// <summary>
        ///     // Noufil 5691 Noufil
        ///     Get All NON HMC Clients whose Court Date has been Passed.
        /// </summary>
        /// <returns></returns>
        public DataTable GetRecords_PastCourtDatesForNonHMC(int showAll)
        {
            if (showAll == 0 || showAll == 1)
            {
                string[] key = { "@showAll" };
                object[] value = { showAll };
                DataTable dt = clsdb.Get_DT_BySPArr("USP_HTP_Get_PastCourtDateForNonHMC", key, value);
                if (dt != null)
                    return dt;
                else
                    throw new Exception("Resultset not return in correcrt format");
            }
            else
                throw new ArgumentException("Argument not in correcrt format");
        }

        /// <summary>        
        /// To show records which have different address in cases.
        /// </summary>
        /// <param name="showAll">showAll parameter added in this method to display records based on this parameter.
        /// If its value is 'false' it will display all records whose followup date is null,past or today. If value is 'true', it will diaplay all records.
        /// </param>
        /// <returns></returns>
        public DataTable GetRecords_AddressValidationReport(bool showAll)
        {
            string[] key = { "@showAll" };   //SAEED 7844 06/02/2010 showAll parameter is added. Also remove validation parameter which was not using in the procedure. 
            object[] value = { showAll }; //SAEED 7844 06/02/2010 showAll parameter value is added.
            DataTable dt = clsdb.Get_DT_BySPArr("usp_htp_get_AddressCIDValidation", key, value);
            if (dt != null)
            {
                return dt;
            }
            else
            {
                throw new Exception("Unable to get records");
            }
        }

        /// <summary>
        /// Waqas 5770 04/22/2009 First Name validation Report
        /// To show records which have different first name in cases.
        /// </summary>
        /// <returns></returns>
        public DataTable GetRecords_FirstNameValidationReport()
        {
            string[] key = { "@Validation" };
            object[] value = { 0 };
            DataTable dt = clsdb.Get_DT_BySPArr("usp_htp_get_FirstNameCIDValidation", key, value);
            if (dt != null)
            {
                return dt;
            }
            else
            {
                throw new Exception("Unable to get records");
            }

        }

        //Waqas 5864 07/01/2009 Email Case Summary
        /// <summary>        
        /// To show case records which have not sent email a case summary.
        /// </summary>
        /// <returns></returns>
        public DataTable GetRecords_CaseEmailSummaryReport()
        {
            DataTable dt = clsdb.Get_DT_BySPArr("USP_HTP_Get_CaseEmailSummary");
            if (dt != null)
            {
                return dt;
            }
            else
            {
                throw new Exception("Unable to get records");
            }

        }



        //Yasir 5423 02/11/2009 cases having follow up date in next 6 business days
        /// <summary>
        /// This function is used to get HMC A/W Report Cases
        /// </summary>
        /// <param name="showAll"></param>
        /// <param name="validation"></param>
        /// <param name="TicketIds">Ticket Ids to get selected records</param>
        /// <returns>Return HMC A/W Cases</returns>
        public DataSet GetRecords_HMCsubmittedcases(int showAll, int validation, string TicketIds)
        {
            DataSet DS = null;
            try
            {
                //Zeeshan Ahmed 4420 08/06/2008
                //Kazim 3975 6/30/2008 add parameter pastfollowupdate 

                string[] key = { "@showAll", "@validation", "@Ticketids" };
                object[] value = { showAll, validation, TicketIds };
                DS = clsdb.Get_DS_BySPArr("usp_hts_HMC_AW_DLQ_Alert", key, value);

                if (DS.Tables[0].Rows.Count > 0)
                    generateAllserials(DS);
                return DS;

            }
            catch
            {
                return DS;
            }
        }
        /// <summary>
        /// Fahad 5557 03/05/2009 
        /// Add Method to get the data for Validation Report 
        /// </summary>
        /// <param name="Sdate"></param>
        /// <param name="Edate"></param>
        /// <param name="ShowAll"></param>
        /// <param name="ShowDisp"></param>
        /// <param name="NotShowAworBw"></param>
        /// <returns></returns>
        public DataSet GetValidationReport2(DateTime Sdate, DateTime Edate, int ShowAll, int ShowDisp, int NotShowAworBw)
        {
            DataSet dsValidation = null;
            if (Sdate != null && Edate != null && (ShowAll == 1 || ShowAll == 0) && (ShowDisp == 1 || ShowDisp == 0) && (NotShowAworBw == 1 || NotShowAworBw == 0))
            {
                string[] key = { "@startdate", "@enddate", "@showall", "@showdisp", "@NotShowAWorBW" };
                object[] value1 = { Sdate, Edate, ShowAll, ShowDisp, NotShowAworBw };
                dsValidation = clsdb.Get_DS_BySPArr("usp_validationReport_2", key, value1);
            }
            return dsValidation;
        }
        /// <summary>
        /// Fahad 5753 04/13/2009 
        /// Add Method to get the data for Split Report
        /// </summary>
        /// <param name="ShowAll"></param>
        /// <returns></returns>
        public DataSet GetSplitReport(bool ShowAll)
        {
            DataSet dsSplitReport = null;

            {
                string[] Keys = { "@ShowAll" };
                object[] Values = { ShowAll };
                dsSplitReport = clsdb.Get_DS_BySPArr("usp_splitreport_new", Keys, Values);
            }
            return dsSplitReport;


        }
        /// <summary>
        /// Babar Ahmad 9055 03/30/2011
        /// Add Method to get the data for Non-updated Reset Cases Report
        /// </summary>
        /// <param name="ShowAll">Show all records if value is true.</param>
        /// <returns>DataSet</returns>
        public DataSet GetNonupdatedResetCasesReport(bool ShowAll)
        {
            DataSet dsNonupdatedResetCases = null;

            {
                string[] Keys = { "@ShowAll" };
                object[] Values = { ShowAll };
                dsNonupdatedResetCases = clsdb.Get_DS_BySPArr("USP_HTP_GET_NonupdatedResetCases", Keys, Values);
            }
            return dsNonupdatedResetCases;


        }
        // Babar Ahmad 9727 10/19/2011 Added method to remove records form Non-Updated Reset Cases report.
        ///<summary>
        /// This function removes records from the report when "X" is clicked.
        ///</summary>
        ///<param name="ticketId">Ticket ID</param>
        ///<param name="ticketViolationId">Ticket Violation ID</param>
        public void UpdadeNonUpdateWaitingCases(int ticketId, int ticketViolationId)
        {
            string[] keys = { "@TicketID", "@TicketViolationID" };
            object[] values = { ticketId, ticketViolationId };
            clsdb.ExecuteSP("USP_HTP_Delete_NonUpdatedWaitingCases", keys, values);
        }

        /// <summary>
        /// Fahad 5753 04/13/2009 
        /// Add Method to Update No Split Flag in Split Report
        /// </summary>
        /// <param name="TicketId"></param>
        /// /// <param name="empid"></param>
        public void UpdateNosplitFlag(int TicketId, int empid)
        {
            clsLogger objclsLogger = new clsLogger();
            if (TicketId > 1 && empid > 1)
            {
                string[] keys = { "@TicketId" };
                object[] obj = { TicketId };
                clsdb.ExecuteScalerBySp("USP_HTS_MarkCaseAsNoSplit", keys, obj);
                objclsLogger.AddNote(empid, "Flag -No Split - Added", "No Split Flag has been Added", TicketId);
            }
        }

        //  Abbas Qamar 9223 05/06/2011 Added method to update NoChangeInCourtSetting Flag
        /// <summary>
        /// This Method used to Update NoChangeInCourtSetting Flag in Current Court Date Report
        /// </summary>
        /// <param name="TicketId">Ticket Id to fetch Records.</param>
        /// <param name="ticketViolationId"> ticketViolation Id to fetch Records.</param>
        /// <returns>Rows in Datatable>=0</returns>

        public object UpdateNoChangeInCourtSettingFlag(int TicketId, int ticketViolationId)
        {
            object result = new object();
            clsLogger objclsLogger = new clsLogger();
            if (TicketId > 1 && ticketViolationId > 1)
            {
                string[] keys = { "@ticketid", "@TicketsViolationId" };
                object[] obj = { TicketId, ticketViolationId };
                result = clsdb.ExecuteScalerBySp("USP_HTP_UPDATE_CurrentClientCourtDateChecker", keys, obj);

            }
            return result;
        }

        //Yasir Kamal 5460 02/17/2009 Email Alert/Report Summary 
        /// <summary>
        /// This method used to display validation email report. 
        /// </summary>
        /// <param name="EmailAddress">Email address on which validation report will be email.</param>
        /// <param name="DisplayReport">When seinding email if this parameter is true then report will also diaplay on browser.</param>
        /// <param name="Detail">If detail is false then only display reports summary otherwise display all detials.</param>
        /// <returns>String value containing html of reports</returns>
        public string GetValidationReport(string EmailAddress, bool DisplayReport, bool Detail)
        {

            string[] keys = { "@EmailAddress", "@DisplayReport", "@thisresultset", "@Detail", };
            object[] values = { EmailAddress, DisplayReport, "", Detail };

            if (!DisplayReport)
            {
                //Fahad 7791 06/30/2010 Time Out Parameter Introduced for Validation Report
                clsdb.ExecuteSP(900000, "USP_HTS_ValidationEmailReportHouston", keys, values);  //Saeed 7791 08/11/2010 increase timeout to 15 mintues.
            }
            else
            {
                return Convert.ToString(clsdb.ExecuteScalerBySp("USP_HTS_ValidationEmailReportHouston", keys, values, 900000)); //Saeed 7791 08/11/2010 increase time out to 15 mintues.
            }
            return "";
        }

        //5460 end

        // Agha Usman 4271 07/02/2008
        /// <summary>
        /// GetValidationReport
        /// </summary>
        /// <param name="EmailAddress"></param>
        /// <param name="DisplayReport"></param>
        /// <param name="CaseTypeId"></param>
        /// <returns></returns>
        public string GetValidationReport(string EmailAddress, bool DisplayReport, int CaseTypeId)
        {
            string[] keys = { "@EmailAddress", "@DisplayReport", "@thisresultset" };
            object[] values = { EmailAddress, DisplayReport, "" };
            string spName = string.Empty;
            bool NoParameter = false;

            // Noufil 4432 08/04/2008 New parameter added for validation email
            switch (CaseTypeId)
            {
                case 1: //Traffic                    
                    spName = "USP_HTS_ValidationEmailReportHouston";
                    break;
                case 2: //Criminal                    
                    spName = "USP_HTP_ValidationEmailReportHoustonCriminal";
                    break;
                case 3://Civil                    
                    spName = "USP_HTP_ValidationEmailReportHoustonCivil";
                    break;
                default:
                    spName = "USP_HTS_ValidationEmailReportHouston";
                    NoParameter = true;
                    break;
            }


            if (!DisplayReport)
            {
                if (NoParameter)
                    clsdb.ExecuteSP(spName);
                else
                    clsdb.ExecuteSP(spName, keys, values);
            }
            else
            {
                return Convert.ToString(clsdb.ExecuteScalerBySp(spName, keys, values));
            }

            return "";
        }

        /// <summary>
        /// This function is used to update emails count of the cases for which arraignment setting request has been sent
        /// </summary>
        /// <param name="TicketIds">Ticket ids</param>
        /// <returns>Return true if cases information updated</returns>
        public bool updateArrignmentWaitingEmailCounts(string TicketIds)
        {
            //Zeeshan Ahmed 4420 08/06/2008
            string[] Keys = { "@Ticketids" };
            string[] Values = { TicketIds };
            return Convert.ToBoolean(Convert.ToInt32(clsdb.ExecuteScalerBySp("USP_HTP_Update_ArrignmentSetting_Email_Count", Keys, Values)));
        }
        /// <summary>
        ///        This function display AG report
        /// </summary>
        /// <returns>DataTable</returns>
        public DataTable GetAGReport()
        {
            // Noufil 4747 09/12/2008 Fucntion added to display AG Report
            return clsdb.Get_DT_BySPArr("USP_HTP_GetAGCasesForValidation");
        }
        // Noufil 4461 09/25/2008 
        /// <summary>
        /// This function returns the cunsultation report
        /// </summary>
        /// <returns></returns>
        public DataTable GetLegalConsultationReport()
        {
            // Noufil 4919 10/04/2008 End date added
            clsENationWebComponents clsdbb = new clsENationWebComponents("SullolawDNN");
            string[] Keys = { "@siteid", "@showall", "@date", "@callback", "@enddate" };
            //ozair 5074 11/13/2008 all pending records
            //Yasir Kamal 7507 03/03/2010 display active call back statuses
            object[] Values = { 1, 1, DateTime.Now.ToString(), 1, DateTime.Now.ToString() };
            return clsdbb.Get_DT_BySPArr("USP_PS_Get_LegalConsultation", Keys, Values);

        }

        // Noufil 4461 09/25/2008 
        /// <summary>
        /// This function returns the Visitor report
        /// </summary>
        /// <returns>return datatable</returns>
        public DataTable GetVisitorReport()
        {
            clsENationWebComponents clsdbb = new clsENationWebComponents("Sullolaw");
            string[] Keys = { "@siteype", "@showall", "@date", "@enddate", "@callback" };
            //ozair 5074 11/13/2008 all pending records
            // Noufil 5524 03/16/2009 End date parameter Added
            object[] Values = { 1, 1, DateTime.Now.ToString(), DateTime.Now.ToString(), 0 };
            return clsdbb.Get_DT_BySPArr("USP_PS_Get_OnlineTracking", Keys, Values);

        }

        // Noufil 4840 10/14/2008 
        /// <summary>
        ///  This function returns NO LOR report
        /// </summary>
        /// <returns>DataTable</returns>
        public DataTable GetNoLORReport()
        {
            return clsdb.Get_DT_BySPArr("USP_HTP_Get_NOLOR");
        }

        // Muhammad Nadir Siddiqui 9134 4/29/2011
        /// <summary>
        ///  This function returns NO LOR report
        /// </summary>
        /// <param name="showAll">To show all records with follow up dates</param>
        /// <returns>DataTable</returns>
        public DataTable GetNoLORReport(bool showAll)
        {
            string[] key = { "@showAll" };
            object[] value = { showAll };
            return clsdb.Get_DT_BySPArr("USP_HTP_Get_NOLOR", key, value);

        }


        // Fahad 5302 12/05/2008 
        /// <summary>
        ///  This function returns Missed Court Date Calls Report
        /// </summary>
        /// <returns>DataTable</returns>
        public DataTable GetMissedCourtDateCalls()
        {
            return clsdb.Get_DT_BySPArr("USP_HTP_MissedCourtDateCalls");
        }

        //Nasir 5690 03/25/2009 add new function for report HMC A/W DLQ Calls
        /// <summary>
        ///  This function returns the data of HMC A/W DLQ Calls
        ///  return the records having contact status "Pending" since Dec' 01 2008.  
        /// </summary>
        /// <returns>DataTable</returns>
        public DataTable GetHMCAWDLQCalls()
        {
            return clsdb.Get_DT_BySPArr("USP_HTP_Get_HMCAWDLQCallsValidation");
        }

        /// <summary>
        /// // Sabir Khan 5977 07/06/2009
        /// Get All clients having discrepancy in case information...
        /// </summary>
        /// <returns></returns>
        public DataTable GetRecords_HMCSettingDiscrepancy(int showAll)
        {
            if (showAll == 0 || showAll == 1)
            {
                DataTable dt = new DataTable();
                string[] key = { "@showAll" };
                object[] value = { showAll };
                dt = clsdb.Get_DT_BySPArr("usp_hts_hmcsettingdiscrepancy", key, value);
                if (dt != null)
                    return dt;
                else
                    throw new Exception("Resultset not return in correcrt format");
            }
            else
                throw new ArgumentException("Argument not in correcrt format");

        }

        //      Noufil 6126 07/16/2009 Email Case Summary
        /// <summary>        
        ///     To show case records in JURY,JUDGE,PRETRIAL Status and court date in next 5 business days.
        /// </summary>
        /// <returns>DataTable</returns>
        public DataTable GetNewHireReport()
        {
            DataTable dt = clsdb.Get_DT_BySPArr("USP_HTP_GET_NEWHIRE_REPORT");
            if (dt != null)
            {
                return dt;
            }
            else
            {
                throw new ArgumentNullException("Unable to get records");
            }

        }

        //Fahad 6934 11/19/2009 
        /// <summary>
        /// This Method get Records for Today's Qoute Call Back
        /// </summary>
        /// <param name="startdate">Starting Date to fetch Records.</param>
        /// <param name="enddate">End Date to fetch Records.</param>
        /// <returns>Rows in Datatable>=0</returns>
        public DataTable GetTodayQouteCallBack(DateTime startdate, DateTime enddate)
        {
            DataTable dtQoute = null;
            string[] keys = { "@DateFrom", "@DateTo" };
            object[] values = { startdate, enddate };
            dtQoute = clsdb.Get_DT_BySPArr("USP_HTP_Get_All_TodayQouteCallDate", keys, values);
            return dtQoute;

        }

        //Fahad 6934 11/19/2009
        /// <summary>
        /// This Method get Records for Before Court date Qoute Call Back
        /// </summary>
        /// <returns></returns>
        public DataTable GetBeforeQouteCallBack()
        {
            DataTable dtBeforeQoute = null;
            return dtBeforeQoute = clsdb.Get_DT_BySPArr("USP_HTP_Get_BeforeQouteCallDate");
        }

        //Fahad 6934 12/07/2009 
        /// <summary>
        /// This Method get Records for Qoute Call Back Tracking Report
        /// </summary>
        /// <param name="startdate">Starting Date to fetch Records.</param>
        /// <param name="enddate">End Date to fetch Records.</param>
        /// <returns>Rows in Datatable>=0</returns>
        public DataTable GetQouteCallBackTrackingReport(DateTime startdate, DateTime enddate)
        {
            DataTable dtQouteCall = null;
            string[] keys = { "@DateFrom", "@DateTo" };
            object[] values = { startdate, enddate };
            dtQouteCall = clsdb.Get_DT_BySPArr("USP_HTP_Get_QuoteCallBackTracking", keys, values);
            return dtQouteCall;

        }
        //Saeed 8101 09/28/2010
        /// <summary>
        /// The method is used to get records for HCJP Auto Update Alert report
        /// </summary>
        /// <param name="showAll">If true display all records without any filter else display records whose follow up date is null,past or today</param>
        /// <returns>Returns dataset of HCJP Auto Update Alert report</returns>
        public DataSet GetHcjpAutoUpdateAlert(bool showAll)
        {
            DataSet ds = null;
            {
                string[] Keys = { "@showAll" };
                object[] Values = { showAll };
                ds = clsdb.Get_DS_BySPArr("usp_hts_outsidecases", Keys, Values);
            }
            return ds;
        }

        /// <summary>
        /// The method is used to get records for Bad Address Report
        /// </summary>
        /// <param name="showAll">If true display all records without any filter else display records whose follow up date is null,past or today</param>
        /// <returns>Returns dataset of Bad Address report</returns>
        public DataSet GetBadAddresses(bool showAll)
        {
            DataSet ds = null;
            {
                string[] Keys = { "@showAll" };
                object[] Values = { showAll };
                ds = clsdb.Get_DS_BySPArr("USP_HTP_Get_BadAddressClients", Keys, Values);
            }
            return ds;
        }

    }
}

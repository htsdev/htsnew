using System;
using System.Data;
using System.Data.SqlClient;
using FrameWorkEnation.Components;



namespace lntechNew.Components
{
	/// <summary>
	/// Summary description for Comments.
	/// </summary>

	public class Comments
	{
        clsENationWebComponents ClsDb = new clsENationWebComponents("TroubleTicketDB");
        
		#region Variables

		private string description=String.Empty;
		private DateTime commentdate;
		private int bugid=0;
		private int employeeid=0;


		#endregion

		#region Properties
		public string Description
		{ 
			get
			{
				return description;
			}
			set
			{ 
				description=value;
			}
		}
		public DateTime CommentDate
		{ 
			get
			{ 
				return commentdate;
			}
			set
			{ 
				commentdate=value;
			}
		}
		public int BugID
		{ 
			get
			{
				return bugid;
			}
			set
			{ 
				bugid=value;
			}
		}
		public int EmployeeID
		{ 
			get
			{ 
				return employeeid;
			}
			set
			{ 
				employeeid=value;
			}
		}
		#endregion

		#region Methods
        
		public void AddNewComments(string Description,DateTime CommentDate,int EmployeeID,int BugID)
		{
            try
            {
                string[] key ={ "@Description", "@CommentDate", "@EmployeeID", "@BugID" };
                object[] value1 ={Description,CommentDate, EmployeeID, BugID};
                ClsDb.InsertBySPArr("usp_Bug_InsertNewComment", key, value1);
            }
            catch (Exception ex)
            {
                
            }

        }
      
        public void TestAddNewComments()
        {
            Description = "Test";
            CommentDate = System.DateTime.Today;
            EmployeeID = 3991;
            BugID = 9;

            string[] key ={ "@Description", "@CommentDate", "@EmployeeID", "@BugID" };
            object[] value1 ={Description,CommentDate,EmployeeID, BugID};
            ClsDb.InsertBySPArr("usp_Bug_InsertNewComment", key, value1);
            
        }
		public void AddComments()
		{

            try
            {
                string[] key ={ "@Description", "@CommentDate", "@EmployeeID", "@BugID" };
                object[] value1 ={ Description, CommentDate, EmployeeID, BugID };

                ClsDb.ExecuteSP("usp_Bug_AddComments", key, value1);
            }
            catch (Exception ex)
            {
                
            }
			
		}
     
        public void TestAddComments()
        {

            Description = "TestCases";
            CommentDate = System.DateTime.Today;
            EmployeeID = 3991;
            BugID = 5;

            string[] key ={"@Description", "@CommentDate", "@EmployeeID", "@BugID"};
            object[] value1 ={Description,CommentDate, EmployeeID, BugID};
            ClsDb.ExecuteSP("usp_Bug_AddComments", key, value1);
            
        }
        
		#endregion
		public Comments()
		{
			
		}
	}
}

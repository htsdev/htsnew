using System;
using System.ComponentModel;
using System.Collections;
using System.Diagnostics;
using System.Web.UI.WebControls;
using System.Web;
using System.Data;

namespace lntechNew.Components
{
	/// <summary>
	/// Summary description for cmpDataGridToExcel.
	/// </summary>
	public class cmpDataGridToExcel : System.ComponentModel.Component
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public cmpDataGridToExcel(System.ComponentModel.IContainer container)
		{
			///
			/// Required for Windows.Forms Class Composition Designer support
			///
			container.Add(this);
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		public cmpDataGridToExcel()
		{
			///
			/// Required for Windows.Forms Class Composition Designer support
			///
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}


		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
		}
		#endregion

//		public static void DataGridToExcel(DataGrid dgExport,HttpResponse response )
//		{
//			//clean up the response.object
//			response.Clear();
//			response.Charset = "";
//			//set the response mime type for excel
//			response.ContentType = "application/vnd.ms-excel";
//
//			//create a string writer
//			System.IO.StringWriter stringWrite= new System.IO.StringWriter();
//			//create an htmltextwriter which uses the stringwriter
//			System.Web.UI.HtmlTextWriter htmlWrite = new System.Web.UI.HtmlTextWriter(stringWrite);
//
//			//instantiate a datagrid
//			DataGrid dg = new DataGrid();
//			//just set the input datagrid = to the new dg grid
//			dg = dgExport;
//
//			//I want to make sure there are no annoying gridlines
//			dg.GridLines = GridLines.None;
//			// Make the header text bold
//			dg.HeaderStyle.Font.Bold = true;
//
//			// If needed, here's how to change colors/formatting at the component level
//			//dg.HeaderStyle.ForeColor = System.Drawing.Color.Black
//			//dg.ItemStyle.ForeColor = System.Drawing.Color.Black
//
//			//bind the modified datagrid
//			dg.DataBind();
//			//tell the datagrid to render itself to our htmltextwriter
//			dg.RenderControl(htmlWrite);
//			//output the html
//			response.Write(stringWrite.ToString());
//			response.End();
//		}

		public static void DataGridToExcel(DataGrid dgExport, HttpResponse response) 
		{ 
			response.Clear(); 
			response.Charset = ""; 
			response.ContentType = "application/vnd.ms-excel"; 
			System.IO.StringWriter stringWrite = new System.IO.StringWriter(); 
			System.Web.UI.HtmlTextWriter htmlWrite = new System.Web.UI.HtmlTextWriter(stringWrite); 
			DataGrid dg = new DataGrid(); 
			dg = dgExport; 
			dg.GridLines = GridLines.None; 
			dg.HeaderStyle.Font.Bold = true; 
			dg.DataBind();
			dg.RenderControl(htmlWrite); 
			response.Write(stringWrite.ToString());
            response.Flush();
            response.Close();
			//response.End(); 
		}

        public static void DataTableToExcel(DataTable dgExport, HttpResponse response)
        {
            response.Clear();
            response.Charset = "";
            response.ContentType = "application/vnd.ms-excel";
            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htmlWrite = new System.Web.UI.HtmlTextWriter(stringWrite);
            DataSet ds = new DataSet();
            ds.Tables.Add(dgExport);
            DataGrid dg = new DataGrid();                       
            dg.GridLines = GridLines.None;
            dg.HeaderStyle.Font.Bold = true;
            dg.DataSource = ds;
            dg.DataBind();
            dg.RenderControl(htmlWrite);
            response.Write(stringWrite.ToString());
            response.Flush();
            response.Close();
        }

		//Overloaded Method to Convert Table into Excel Sheet
		public static void TableToExcel(System.Web .UI .WebControls .Table tbExport, HttpResponse response) 
		{ 
			response.Clear(); 
			response.Charset = ""; 
			response.ContentType = "application/vnd.ms-excel"; 
			System.IO.StringWriter stringWrite=new System.IO.StringWriter ();
			System.Web.UI.HtmlTextWriter htmlWrite = new System.Web.UI.HtmlTextWriter(stringWrite); 
			Table tbResults=new Table ();
			tbResults=tbExport;
			tbResults.RenderControl (htmlWrite);
			response.Write(stringWrite .ToString());
			response.End ();	
		}




	}
}

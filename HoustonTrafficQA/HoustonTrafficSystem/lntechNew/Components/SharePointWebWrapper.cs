using System;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Xml;
using lntechNew.SharePointLists;
using LNHelper;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;
using System.Net;
//Nasir 5765 11/19/2009 add namespace
using lntechNew.Components;

namespace HTP.Components
{
    public class SharePointWebWrapper
    {
        private static Lists _List;
        private static SharePointSettings _Settings;

        static SharePointWebWrapper()
        {
            _Settings = (HTP.Components.SharePointSettings)System.Configuration.ConfigurationManager.GetSection("SharePointSettings");
        }
        
        #region Update Item
        /// <summary>
        /// Method to update all the violations in a case on the calender
        /// </summary>
        /// <param name="pTicketId">Ticket Id of the case</param>
        /// <returns>No of items in the calender updated.</returns>
        public static int UpdateAllViolationsInCalendar(int pTicketId)
        {
            return UpdateViolationsInCalendar(pTicketId, -1);
        }

        /// <summary>
        /// Method to update a single violation on the calender.
        /// </summary>
        /// <param name="pTicketsViolationID">Violation Id</param>
        /// <returns>No of items updated.</returns>
        public static int UpdateSingleViolationInCalendar(int pTicketsViolationID)
        {
            return UpdateViolationsInCalendar(-1, pTicketsViolationID);
        }

        /// <summary>
        /// Private method to updated violations of a case on the calender. This private method
        /// is called by two overloaded public methods.
        /// 1. TicketId will be supplied and TicketViolationId will be -1 in case when you want to update all violations of a case.
        /// 2. TicketId will be -1 and TicketViolationId will be supplied in case when you want to update single violation.
        /// </summary>
        /// <param name="pTicketId">Ticket Id of the case</param>
        /// <param name="pTicketsViolationID">Violation Id</param>
        /// <returns>No of items updated on calender</returns>
        private static int UpdateViolationsInCalendar(int pTicketId, int pTicketsViolationID)
        {
            if (_Settings.PostToSharePoint.ToLower() == "no")
                return 0;

            DBComponent objDB = new DBComponent();
            string[] key = { "@TicketID", "@TicketsViolationID" };
            object[] value1 = { pTicketId, pTicketsViolationID };

            int UpdatedCount = 0;
            DataTable dtViolations = objDB.GetDTBySPArr("[USP_HTP_GetCriminalViolationsForSharePoint]", key, value1);            
            if (dtViolations.Rows.Count == 0)
                return 0;

            _List = new Lists();
            _List.Credentials = new NetworkCredential(_Settings.UserId, _Settings.Password, _Settings.Domain);
            _List.Url = _Settings.SharePointSite + "/_vti_bin/lists.asmx";

            foreach (DataRow dRowCase in dtViolations.Rows)
            {
                try
                {
                    string CourtName = Convert.ToString(dRowCase["CourtName"]);
                    string FirmName = Convert.ToString(dRowCase["FirmName"]);
                    string ClientLanguage = Convert.ToString(dRowCase["ClientLanguage"]);
                    int TotalFee = Convert.ToInt32(dRowCase["TotalFee"]);
                    int PaidFee = Convert.ToInt32(dRowCase["PaidFee"]);
                    string Lastname = Convert.ToString(dRowCase["Lastname"]);
                    int TicketsViolationID = Convert.ToInt32(dRowCase["TicketsViolationID"]);                    
                    string Title = CourtName + "|" + FirmName + "|" + ClientLanguage + "|$" +
                       (TotalFee - PaidFee).ToString() + "|" + Lastname;
                    string Description = "Some notes can be put by the Attorneyes";

                    XmlNode xItems = GetListItem(TicketsViolationID);
                    if (xItems.ChildNodes.Count > 0)
                    {
                        int listItemId = int.Parse(xItems.ChildNodes[0].Attributes["ows_ID"].Value);
                        UpdateListItem(listItemId, Title, FirmName, CourtName, Convert.ToDateTime(dRowCase["CourtDateMain"]));
                    }
                    else
                    {
                        int TicketId = Convert.ToInt32(dRowCase["TicketId"]);
                        string URL = string.Format(_Settings.TrafficProgramURL, TicketId);              
                        AddListItem(TicketsViolationID, Title, FirmName, Description, CourtName, Convert.ToDateTime(dRowCase["CourtDateMain"]), URL);
                    }
                    UpdatedCount++;
                }
                catch (Exception ex)
                {
                    clsLogger.ErrorLog(ex);
                }
            }
            return UpdatedCount;
        }

        /// <summary>
        /// Private method which will actually use sharepoint web service to updated a particular
        /// calender item. The ListItemId must be supplied with other arguments.
        /// </summary>
        /// <param name="listItemId">Id of the item in calender</param>
        /// <param name="title">Title of the item</param>
        /// <param name="Attorney">Attorney Short Name</param>
        /// <param name="Location">Court Location</param>
        /// <param name="caseDate">Date of the Violation</param>
        /// <returns>Flag to show if update is successful</returns>
        private static bool UpdateListItem(int listItemId, string title, string Attorney, string Location, DateTime caseDate)
        {
            try
            {
                System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                System.Xml.XmlElement batchElement = doc.CreateElement("Batch");
                string caseDateString = caseDate.ToString("yyyy-MM-ddTHH:mm:ssZ");

                batchElement.InnerXml =
                   "<Method ID='1' Cmd='Update'>" +
                   "<Field Name='ID'>" + listItemId + "</Field>" +
                   "<Field Name='Title'>" + title + "</Field></Method>" +

                   "<Method ID='1' Cmd='Update'>" +
                   "<Field Name='ID'>" + listItemId + "</Field>" +
                   "<Field Name='Location'>" + Location + "</Field></Method>" +

                   "<Method ID='1' Cmd='Update'>" +
                   "<Field Name='ID'>" + listItemId + "</Field>" +
                   "<Field Name='EventDate'>" + caseDateString + "</Field></Method>" +

                   "<Method ID='2' Cmd='Update'>" +
                   "<Field Name='ID'>" + listItemId + "</Field>" +
                   "<Field Name='EndDate'>" + caseDateString + "</Field>" +
                   "</Method>" +

                   "<Method ID='3' Cmd='Update'>" +
                   "<Field Name='ID'>" + listItemId + "</Field>" +
                   "<Field Name='Attorney'>" + Attorney + "</Field>" +
                   "</Method>";


                _List.UpdateListItems(_Settings.SharePointListId, batchElement);
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                return false;
            }
            return true;
        }

        #endregion

        #region  Add Item
        /// <summary>
        /// Private method which will use Share Point web service to add an item in the calender
        /// against a TicketViolationId
        /// </summary>
        /// <param name="TicketsViolationID">Violation Id against which an item will be added</param>
        /// <param name="title">Title of item</param>
        /// <param name="Attorney">Covering Firm Short Name</param>
        /// <param name="Description">Dummy description</param>
        /// <param name="Location">Court Location</param>
        /// <param name="caseDate">Court Date of Violation</param>
        /// <returns>successfully added or not</returns>        
        private static bool AddListItem(int TicketsViolationID, string title, string Attorney, string Description, string Location, DateTime caseDate, string TrafficProgramURL)
        {
            if ((TicketsViolationID <= 0) || (title == "") || (Attorney == "") ||
                (Description == "") || (Location == "") || (caseDate == null))
            {
                return false;
            }

            try
            {
                System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                System.Xml.XmlElement batchElement = doc.CreateElement("Batch");
                string caseDateString = caseDate.ToString("yyyy-MM-ddTHH:mm:ssZ");

                //Nasir 5765 11/19/2009 implement EncodeXMLString method to replace special character
                batchElement.InnerXml =
                   "<Method ID='1' Cmd='New'>" +
                    "<Field Name='TicketsViolationID'>" + TicketsViolationID + "</Field>" +
                   "<Field Name='Title'>" + clsGeneralMethods.EncodeXMLString(title) + "</Field>" +
                   "<Field Name='Description'>" + clsGeneralMethods.EncodeXMLString(Description) + "</Field>" +
                   "<Field Name='Location'>" + clsGeneralMethods.EncodeXMLString(Location) + "</Field>" +
                   "<Field Name='EventDate'>" + clsGeneralMethods.EncodeXMLString(caseDateString) + "</Field>" +
                   "<Field Name='EndDate'>" + clsGeneralMethods.EncodeXMLString(caseDateString) + "</Field>" +
                   "<Field Name='Attorney'>" + clsGeneralMethods.EncodeXMLString(Attorney) + "</Field>" +
                   "<Field Name='TrafficProgramURL'>" + clsGeneralMethods.EncodeXMLString(TrafficProgramURL) + ", Traffic Program URL" + "</Field>" +
                   "</Method>"
                   ;

             

                //TrafficProgramURL Format
                //[ValidUrl][Comma][WhiteSpace][Desctiptiontext] "http://localhost, My local webserver"
                 _List.UpdateListItems(_Settings.SharePointListId, batchElement);
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Method use to add all violations of a case. Usually it will be used when a client is hired.
        /// </summary>
        /// <param name="ticketId">Ticket Id of the case</param>
        /// <returns>No of items added</returns>
        public static int AddViolationsToCalendar(int ticketId)
        {
            return AddViolationsToCalendar(ticketId, -1);
        }

        /// <summary>
        /// Method use to add single violation in the calender.
        /// </summary>
        /// <param name="TicketsViolationID">Violation Id</param>
        /// <returns>No of items added</returns>
        public static int AddNewViolationToCalendar(int TicketsViolationID)
        {
            return AddViolationsToCalendar(-1, TicketsViolationID);
        }

        /// <summary>
        /// Private method to add violations of a case on the calender. This private method
        /// is called by two overloaded public methods.
        /// 1. TicketId will be supplied and TicketViolationId will be -1 in case when you want to add all violations of a case.
        /// 2. TicketId will be -1 and TicketViolationId will be supplied in case when you want to add single violation.
        /// </summary>
        /// <param name="pTicketId">Ticket Id of case</param>
        /// <param name="pTicketsViolationID">Violation Id</param>
        /// <returns>No of items added</returns>
        private static int AddViolationsToCalendar(int pTicketId, int pTicketsViolationID)
        {
            if (_Settings.PostToSharePoint.ToLower() == "no")
                return 0;

            DBComponent objDB = new DBComponent();
            string[] key = { "@TicketID", "@TicketsViolationID" };
            object[] value1 = { pTicketId, pTicketsViolationID };       
            DataTable dtViolations = objDB.GetDTBySPArr("[USP_HTP_GetCriminalViolationsForSharePoint]", key, value1);            
            if (dtViolations.Rows.Count == 0)
                return 0;

            return AddViolationsToCalendar(dtViolations);            
        }

        /// <summary>
        /// Add all active criminal violations to the calender regardless of ticket id
        /// </summary>
        /// <returns>No of items added</returns>
        public static int AddAllViolationsToCalendar()
        {    
            DBComponent objDB = new DBComponent();
            DataTable dtViolations = objDB.GetDSBySP("USP_HTP_GetAllCriminalViolationsForSharePoint").Tables[0];
            if (dtViolations.Rows.Count == 0)
                return 0;
            return AddViolationsToCalendar(dtViolations);
        }

        /// <summary>
        /// Add violations from a datatable to the Calender
        /// </summary>
        /// <param name="dtViolations">DataTable containing Violations</param>
        /// <returns>No of items added</returns>
        private static int AddViolationsToCalendar(DataTable dtViolations)
        {
            int AddedCount = 0;
            _List = new Lists();
            _List.Credentials = new NetworkCredential(_Settings.UserId, _Settings.Password, _Settings.Domain);
            _List.Url = _Settings.SharePointSite + "/_vti_bin/lists.asmx";

            foreach (DataRow dRowCase in dtViolations.Rows)
            {
                try
                {
                    string CourtName = Convert.ToString(dRowCase["CourtName"]);
                    string FirmName = Convert.ToString(dRowCase["FirmName"]);
                    string ClientLanguage = Convert.ToString(dRowCase["ClientLanguage"]);
                    int TotalFee = Convert.ToInt32(dRowCase["TotalFee"]);
                    int PaidFee = Convert.ToInt32(dRowCase["PaidFee"]);
                    string Lastname = Convert.ToString(dRowCase["Lastname"]);
                    int TicketsViolationID = Convert.ToInt32(dRowCase["TicketsViolationID"]);
                    int TicketId = Convert.ToInt32(dRowCase["TicketId"]);
                  

                    string URL = string.Format(_Settings.TrafficProgramURL, TicketId);

                    //Short court name | Attorney short name | Language(E or S) | amount owed | Client Last Name 

                    string Title = CourtName + "|" + FirmName + "|" + ClientLanguage + "|$" +
                       (TotalFee - PaidFee).ToString() + "|" + Lastname;
                    string Description = "Some notes can be put by the Attorneyes";

                    AddListItem(TicketsViolationID, Title, FirmName, Description, CourtName, Convert.ToDateTime(dRowCase["CourtDateMain"]), URL);
                    AddedCount++;
                }
                catch (Exception ex)
                {
                    clsLogger.ErrorLog(ex);
                }
            }
            return AddedCount;
        }


        #endregion

        #region Delete Item
        /// <summary>
        /// Delete all items from the calender
        /// </summary>
        /// <returns>No of items deleted</returns>
        public static int DeleteAllItems()
        {
            _List = new Lists();
            _List.Credentials = new NetworkCredential(_Settings.UserId, _Settings.Password, _Settings.Domain);
            _List.Url = _Settings.SharePointSite + "/_vti_bin/lists.asmx";

            XmlDocument doc = new XmlDocument();
            doc.LoadXml("<Document><Query /><ViewFields /><QueryOptions /></Document>");

            XmlNode listQuery = doc.SelectSingleNode("//Query");
            XmlNode listViewFields = doc.SelectSingleNode("//ViewFields");
            XmlNode listQueryOptions = doc.SelectSingleNode("//QueryOptions");
            XmlNode items = _List.GetListItems(_Settings.SharePointListId, "", listQuery, listViewFields, "", listQueryOptions, "");
            string strXML = items.InnerXml.Replace("\n", "");
            doc.LoadXml(strXML);

            XmlNode listItems = doc.ChildNodes[0];
            int DeletedItems = 0;
            foreach (XmlNode xItem in listItems.ChildNodes)
            {
                int listItemId = int.Parse(xItem.Attributes["ows_ID"].Value);
                if (DeleteListItem(listItemId))
                    DeletedItems++;
            }

            return DeletedItems;
        }

        /// <summary>
        /// Delete an item from the calender against the ListItemId
        /// </summary>
        /// <param name="listItemId">Id of the Item</param>
        /// <returns>Deleted successfully or Not</returns>
        private static bool DeleteListItem(int listItemId)
        {
            try
            {
                System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                System.Xml.XmlElement batchElement = doc.CreateElement("Batch");
                batchElement.InnerXml = "<Method ID='1' Cmd='Delete'><Field Name='ID'>" + listItemId + "</Field></Method>";
                _List.UpdateListItems(_Settings.SharePointListId, batchElement);
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                return false;
            }
            return true;
        }


        /// <summary>
        /// Method to delete all the violations in a case on the calender
        /// </summary>
        /// <param name="pTicketId">Ticket Id of the case</param>
        /// <returns>No of items in the calender deleted.</returns>
        // Tahir  4631 08/16/2008
        // added delete by ticket id facility...
        public static int DeleteAllViolationsInCalendar(int pTicketId)
        {
            return DeleteViolationsInCalendar(pTicketId, -1);
        }

        /// <summary>
        /// Method to delete a single violation on the calender.
        /// </summary>
        /// <param name="pTicketsViolationID">Violation Id</param>
        /// <returns>No of items updated.</returns>
        // Tahir  4631 08/16/2008
        // added delete by ticketsvioationid facility...
        public static int DeleteSingleViolationInCalendar(int pTicketsViolationID)
        {
            return DeleteViolationsInCalendar(-1, pTicketsViolationID);
        }

        /// <summary>
        /// Private method to delete violations of a case on the calender. This private method
        /// is called by two overloaded public methods.
        /// 1. TicketId will be supplied and TicketViolationId will be -1 in case when you want to delete all violations of a case.
        /// 2. TicketId will be -1 and TicketViolationId will be supplied in case when you want to delete single violation.
        /// </summary>
        /// <param name="pTicketId">Ticket Id of the case</param>
        /// <param name="pTicketsViolationID">Violation Id</param>
        /// <returns>No of items deleted on calender</returns>
        // Tahir  4631 08/16/2008
        // added delete by ticketid/ticketsvioationid facility...
        private static int DeleteViolationsInCalendar(int pTicketId, int pTicketsViolationID)
        {
            if (_Settings.PostToSharePoint.ToLower() == "no")
                return 0;

            DBComponent objDB = new DBComponent();
            string[] key = { "@TicketID", "@TicketsViolationID" };
            object[] value1 = { pTicketId, pTicketsViolationID };

            int DeletedCount = 0;
            DataTable dtViolations = objDB.GetDTBySPArr("USP_HTP_GetCriminalViolationsForSharePointDeletion", key, value1);
            if (dtViolations.Rows.Count == 0)
                return 0;

            _List = new Lists();
            _List.Credentials = new NetworkCredential(_Settings.UserId, _Settings.Password, _Settings.Domain);
            _List.Url = _Settings.SharePointSite + "/_vti_bin/lists.asmx";

            foreach (DataRow dRowCase in dtViolations.Rows)
            {
                try
                {
                    int TicketsViolationID = Convert.ToInt32(dRowCase["TicketsViolationID"]);
                    XmlNode xItems = GetListItem(TicketsViolationID);
                    if (xItems.ChildNodes.Count > 0)
                    {
                        int listItemId = int.Parse(xItems.ChildNodes[0].Attributes["ows_ID"].Value);
                        DeleteListItem(listItemId);
                    }
                    DeletedCount++;
                }
                catch (Exception ex)
                {
                    clsLogger.ErrorLog(ex);
                }
            }
            return DeletedCount;
        }


        #endregion

        /// <summary>
        /// Method used internally to check the existance of an item in the calender
        /// against a TicketViolationId
        /// </summary>
        /// <param name="TicketsViolationID">ViolationId</param>
        /// <returns></returns>
        private static System.Xml.XmlNode GetListItem(int TicketsViolationID)
        {
            string strQueryTemplate = "<Where><Eq><FieldRef Name=\"TicketsViolationID\" /><Value Type=\"Text\">{0}</Value></Eq></Where>";
            XmlDocument doc = new XmlDocument();
            doc.LoadXml("<Document><Query /><ViewFields /><QueryOptions /></Document>");

            XmlNode listQuery = doc.SelectSingleNode("//Query");
            listQuery.InnerXml = string.Format(strQueryTemplate, TicketsViolationID);

            XmlNode listViewFields = doc.SelectSingleNode("//ViewFields");
            XmlNode listQueryOptions = doc.SelectSingleNode("//QueryOptions");
            XmlNode items = _List.GetListItems(_Settings.SharePointListId, "", listQuery, listViewFields, "", listQueryOptions, "");
            string strXML = items.InnerXml.Replace("\n", "");
            doc.LoadXml(strXML);
            return doc.ChildNodes[0];
        }
    }

    #region SharePointSettings
    /// <summary>
    /// Class which loads setting from SharePointSettings section from web.config.
    /// </summary>
    public class SharePointSettings : ConfigurationSection
    {
        public SharePointSettings()
        {
        }
        /// <summary>
        /// It is getting value of "SharePointSite" from web.config which is assign there and seting its value here
        /// </summary>
        [ConfigurationProperty("SharePointSite", DefaultValue = "http://srv-qa:90/sites/HoustonTraffic", IsRequired = true)]
        public string SharePointSite
        {
            get { return (string)this["SharePointSite"]; }
            set { this["SharePointSite"] = value; }
        }

        /// <summary>
        /// It is getting value of "PostToSharePoint" from web.config which is assign there and seting its value here
        /// </summary>
        [ConfigurationProperty("PostToSharePoint", DefaultValue = "yes", IsRequired = true)]
        public string PostToSharePoint
        {
            get { return (string)this["PostToSharePoint"]; }
            set { this["PostToSharePoint"] = value; }
        }

        /// <summary>
        /// It is getting value of "SharePointListId" from web.config which is assign there and seting its value here
        /// </summary>
        [ConfigurationProperty("SharePointListId", DefaultValue = "1f564117-6327-4ef0-86d4-25d167922dc5", IsRequired = true)]
        public string SharePointListId
        {
            get { return (string)this["SharePointListId"]; }
            set { this["SharePointListId"] = (string)value; }
        }

        /// <summary>
        /// It is getting value of "TrafficProgramURL" from web.config which is assign there and seting its value here
        /// </summary>
        [ConfigurationProperty("TrafficProgramURL", DefaultValue = "http://dev.houston.com/clientinfo/ViolationFeeold.aspx?search=0&amp;amp;casenumber={0}", IsRequired = true)]
        public string TrafficProgramURL
        {
            get { return (string)this["TrafficProgramURL"]; }
            set { this["TrafficProgramURL"] = value; }
        }

        /// <summary>
        /// It is getting value of "UserId" from web.config which is assign there and seting its value here
        /// </summary>
        [ConfigurationProperty("UserId", DefaultValue = "build", IsRequired = true)]
        public string UserId
        {
            get { return (string)this["UserId"]; }
            set { this["UserId"] = value; }
        }

        /// <summary>
        /// It is getting value of "Password" from web.config which is assign there and seting its value here
        /// </summary>
        [ConfigurationProperty("Password", DefaultValue = "1234567", IsRequired = true)]
        public string Password
        {
            get { return (string)this["Password"]; }
            set { this["Password"] = value; }
        }

        /// <summary>
        /// It is getting value of "Domain" from web.config which is assign there and seting its value here
        /// </summary>
        [ConfigurationProperty("Domain", DefaultValue = "lntech", IsRequired = true)]
        public string Domain
        {
            get { return (string)this["Domain"]; }
            set { this["Domain"] = value; }
        }
    }
    #endregion

}

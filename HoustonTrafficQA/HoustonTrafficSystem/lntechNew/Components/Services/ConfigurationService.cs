﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Configuration.Client;
using Configuration.Helper;
using Configuration.Common;
using System.Runtime.Serialization;

namespace HTP.Components.Services
{

    /// <summary>
    /// This Class use Confiduration service to get key value
    /// </summary>
    public class ConfigurationService
    {
        //Nasir 7287 02/12/2010 declare variables
        #region Variables
        Division Division;
        Module Module;
        SubModule SubModule;
        Key Key;
        IConfigurationClient Client;
        #endregion

        #region Method
        /// <summary>
        /// Generate Configuration Object
        /// </summary>
        public ConfigurationService()
        {
           Client = ConfigurationClientFactory.GetConfigurationClient();
        }

        /// <summary>
        /// Get value File Path by Calling Configuration service
        /// </summary>
        /// <param name="SubModule">Sub Module Name</param>
        /// <param name="Path">Required Key Name</param>
        /// <returns>Return File Path</returns>
        public string GetFilePath(SubModule SubModule, Key Path)
        {
            return Client.GetValueOfKey(Division.HOUSTON, Module.TRAFFIC_PROGRAM, SubModule, Path);
        }

        //Nasir 7287 02/12/2010 New method added to set value against key
        /// <summary>
        /// This method gets value against the key of given sub-module, module and division in configuraion table.
        /// </summary>
        /// <param name="division">The division , e.g. Houston.</param>
        /// <param name="module">The module, e.g. Traffic Program. </param>
        /// <param name="subModule">The sub module, e.g. Billing Page</param>
        /// <param name="key">The key, e.g. smtp.userID</param>
        /// <param name="valueOfKey">The value, e.g. system@lntechnologies.com</param>
        /// <param name="description">The Description of the field</param>
        /// <returns>Returns true if added successfully, otherwise, false.</returns>
        internal bool SetKeyValue(string Program, string module, string subModule, string key, string value, string description)
        {
            SetVariablesValues(Program, module, subModule, key);
            return Client.SetKeyValue(Division, Module, SubModule, Key, value.Trim(), description.Trim());
        }

        //Nasir 7287 02/12/2010 New method added to Configuration records
        /// <summary>
        /// This method get configuration setting records against the given key ,sub-module, module and division in configuraion table
        /// </summary>
        /// <param name="division">The division , e.g. Houston.</param>
        /// <param name="module">The module, e.g. Traffic Program. </param>
        /// <param name="subModule">The sub module, e.g. Billing Page</param>
        /// <param name="key">The key, e.g. smtp.userID</param>
        /// <returns>Returns Datatable.</returns>        
        internal DataTable GetSelectedData(string Program, string module, string subModule, string key)
        {
            SetVariablesValues(Program, module, subModule, key);
            return Client.GetSelectedData(Division, Module, SubModule, Key);

        }

        //Nasir 7287 02/12/2010 New method added to set value in local variables
        /// <summary>
        /// Set values in Config data
        /// </summary>
        /// <param name="division">The division , e.g. Houston.</param>
        /// <param name="module">The module, e.g. Traffic Program. </param>
        /// <param name="subModule">The sub module, e.g. Billing Page</param>
        /// <param name="key">The key, e.g. smtp.userID</param>
        private void SetVariablesValues(string program, string module, string subModule, string key)
        {
            Division = (Division)Enum.Parse(typeof(Division), program);
            Module = (Module)Enum.Parse(typeof(Module), module);
            SubModule = (SubModule)Enum.Parse(typeof(SubModule), subModule);
            Key = (Key)Enum.Parse(typeof(Key), key);
        }
        #endregion

    }
}

﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using FrameWorkEnation.Components;

namespace HTP.Components.Services
{
    public class CommentService
    {
        clsENationWebComponents ClsDb = new clsENationWebComponents();

        /// <summary>
        /// Get all Complaints which are not reviewed
        /// </summary>
        /// <param name="StartDate"></param>
        /// <param name="EndDate"></param>
        /// <param name="CaseType"></param>
        /// <param name="Attorney"></param>
        /// <param name="ShowAll"></param>
        /// <returns></returns>
        public DataTable GetComplaintReport(DateTime StartDate, DateTime EndDate, int CaseType, int Attorney, bool ShowAll)
        {
            string[] key = { "@StartDate", "@EndDate", "@CaseType", "@Attorney", "@ShowAll" };
            object[] values = { StartDate, EndDate, CaseType, Attorney, ShowAll };
            return ClsDb.Get_DT_BySPArr("USP_HTP_GET_Complaint_Report", key, values);
        }

        /// <summary>
        /// Get all comments of specific comment type and other given search criteria
        /// </summary>
        /// <param name="StartDate"></param>
        /// <param name="EndDate"></param>
        /// <param name="CaseType"></param>
        /// <param name="Attorney"></param>
        /// <param name="CommentType"></param>
        /// <param name="ShowAll"></param>
        /// <returns></returns>
        public DataTable GetCommentReport(DateTime StartDate, DateTime EndDate, int CaseType, int Attorney, int CommentType, bool ShowAll)
        {
            string[] key = { "@StartDate", "@EndDate", "@CaseType", "@Attorney", "@CommenType", "@ShowAll" };
            object[] values = { StartDate, EndDate, CaseType, Attorney, CommentType, ShowAll };
            return ClsDb.Get_DT_BySPArr("USP_HTP_GET_Comments_Report", key, values);
        }

        /// <summary>
        /// Get client comments of given ticket
        /// </summary>
        /// <param name="TicketID"></param>
        /// <returns></returns>
        public DataSet GetClientCommentsByTicketID(int TicketID)
        {
            string[] key = { "@TicketID" };
            object[] values = { TicketID };
            return ClsDb.Get_DS_BySPArr("USP_HTP_Get_Comments_ByTicketID", key, values);
        }

        /// <summary>
        /// Update if complaint reviewed
        /// </summary>
        /// <param name="NoteIDs"></param>
        /// <returns></returns>
        public DataSet UpdateComplaintIsReviewed(string NoteIDs,int EmpID)
        {
            string[] key = { "@NoteIDs","@EmployeeID" };
            object[] values = { NoteIDs,EmpID };
            return ClsDb.Get_DS_BySPArr("USP_HTP_Update_ComplaintIsReviewed", key, values);
        }

        /// <summary>
        /// Get all comment types
        /// </summary>
        /// <returns></returns>
        public DataTable GetAllCommentTypes()
        {
            return ClsDb.Get_DT_BySPArr("USP_HTP_Get_AllCommentsType");

        }
    }
}

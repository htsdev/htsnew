﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using FrameWorkEnation.Components;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using HTP.Components.Entities;
namespace HTP.Components.Services
{
    public class CountyService
    {
        static clsENationWebComponents ClsDb = new clsENationWebComponents();
        #region Method
        //public DataSet GetCountyInfo(int CountyID)
        //{         
        //}
        public DataSet GetCounties(bool IsActive)
        {
            string[] keys = { "@isactive" };
            object[] values = { IsActive };
            return ClsDb.Get_DS_BySPArr("USP_HTP_GET_Counties", keys, values);
        }
        //public int AddCounty(County county)
        //{ 
        //}
        //public bool UpdateCounty(County county)
        //{ 
        //}
        //public int DeleteCounty(int CountyID)
        //{ 
        //}
        #endregion
    }
}

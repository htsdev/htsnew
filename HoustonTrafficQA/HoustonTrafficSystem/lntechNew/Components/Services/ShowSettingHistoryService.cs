﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using FrameWorkEnation.Components;

namespace HTP.Components.Services
{
    /// <summary>
    /// Show setting history maintain history of reports
    /// </summary>
    public class ShowSettingHistoryService
    {
        #region variables
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        #endregion

        #region Methods

        /// <summary>
        /// Get show setting history records(reports logs) of given dates range and report
        /// </summary>
        /// <param name="StartDate">history start date</param>
        /// <param name="EndDate">history end date</param>
        /// <param name="ReportID">Report identification</param>
        /// <param name="ShowAll">disable date range</param>
        /// <returns>DataTable</returns>
        public DataTable GetShowSettingHistory(DateTime StartDate, DateTime EndDate, int ReportID, bool ShowAll)
        {
            string[] key = { "@StartDate", "@EndDate", "@ReportID", "@ShowAll" };
            object[] values = { StartDate, EndDate, ReportID, ShowAll };
            return ClsDb.Get_DT_BySPArr("USP_HTP_Get_ShowSettingHistory", key, values);
        }

        /// <summary>
        /// maintain logs changes occurs in report.
        /// </summary>
        /// <param name="ReportID">report identification</param>
        /// <param name="Notes">notes of change	occurs in report</param>
        /// <param name="EmployeeID">Employee identification</param>
        /// <returns>bool</returns>
        public bool InsertShowSettingHistory(int ReportID, string Notes, int EmployeeID)
        {
            string[] key = { "@ReportID", "@Note", "@EmployeeID" };
            object[] value1 = { ReportID, Notes, EmployeeID };
            ClsDb.InsertBySPArr("USP_HTP_Insert_ShowSettingHistory", key, value1);
            return true;
        }        

        #endregion
                
    }
}

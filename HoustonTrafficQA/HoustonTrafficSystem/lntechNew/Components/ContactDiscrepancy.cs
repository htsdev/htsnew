using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.Components
{   

    public class ContactDiscrepancy
    {
        clsENationWebComponents ClsDb = new clsENationWebComponents("Connection String");

        #region " Variables "
        private int ticketid=0;
        private string midnumber = string.Empty;
        private string address1 = string.Empty;
        private string address2 = string.Empty;
        private int empid;
        bool updatecontact = false;
        #endregion


        #region " Properties "

        public string MidNumber
        {
            set
            {
                midnumber = value;
            }
            get
            {
                return midnumber;
            }
        }

        public string Address1
        {
            set {
                address1 = value;
            }
            get
            {
                return address1;
            }
        }
        public string Address2
        {
            set
            {
                address2 = value;
            }
            get
            {
                return address2;
            }
        }

        public int EmployeeID
        {
            set
            {
                empid = value;
            }
            get
            {
                return empid;
            }
        }

        public int TicketID
        {
            set
            {
                ticketid = value;
            }
            get
            {
                return ticketid;
            }
        }

        public bool UpdateContacts
        {

            set
            {
                updatecontact = value;
            }
        }

        #endregion

        #region Methods

        // This Method get all records of Contact discrepancy (who have same midnumber and diffrent address)
        public DataSet GetContactDiscrepancy()
        {
            try
            {
                DataSet ds = ClsDb.Get_DS_BySP("USP_HTS_GET_CONTACTDISCREPANCY");
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //  This update the addresses against midnumber
        public bool UpdateMidNumber()
        {
            try
            {
                string[] key ={ "@Midnum", "@TicketID", "@employeeid", "@updateContact" };
                object[] values ={ midnumber, ticketid, empid, updatecontact };
                ClsDb.ExecuteScalerBySp("usp_hts_update_AddressByMidnum", key, values);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return true;
        }

        // Get All Address By MID Number
        public DataSet GetAllAddresses()
        {
            DataSet ds = ClsDb.Get_DS_BySPByOneParmameter("USP_HTS_GET_Address_Infromation_By_MIDNUM", "@MidNum", midnumber);
            return ds;
        }

        #endregion
    }
}

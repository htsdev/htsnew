using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using FrameWorkEnation.Components;
using System.Collections;

namespace lntechNew.Components.ClientInfo
{
	/// <summary>
	/// Summary description for clsPricingPlans.
	/// </summary>
	public class clsPricingPlans
	{
		//Create an Instance of Data Access Class
		clsENationWebComponents ClsDb=new clsENationWebComponents();
clsLogger bugTracker = new clsLogger();
		#region Variables

		private int planID=0;
		private string planDescription=String.Empty;
		private string planshortname=String.Empty;
		private int courtID=0;
		private int categoryID=0;
		private int pPriceid=0;
		
		private double basePrice=0;
		private double secondaryPrice=0;
		private double basePercentage=0;
		private double secondaryPercentage=0;
		private double bondBase=0;
		private double bondSecondary=0;
		private double bondBasePercentage=0;
		private double bondSecondaryPercentage=0;
		private double bondAssumtion=0;
		private double fineAssumption=0;
		private DateTime effectiveDate=DateTime.Now;
		private DateTime effectiveDateTo=DateTime.Now;
		private DateTime effectiveDateFrom =DateTime.MinValue;
		private DateTime endDate=DateTime.Now;
		private DateTime endDateTo=DateTime.Now;
		private DateTime endDateFrom=DateTime.MinValue;
		//--------for Pricing Template
		private int templateID;
		private string templateName;
		private string templateDescription;
		private int empId;
		//-------for Pricing Template Details
		private int detailId;

		private int showAll=0;
		private int isNew=0;
		#endregion

		#region Properties
		public int IsNew
		{
			get
			{
				return isNew;
			}
			set
			{
				isNew= value;
			}
		}

		public int PlanID
		{
			get
			{
				return planID;
			}
			set
			{
				planID = value;
			}
		}

		public string PlanDescription
		{
			get
			{
				return planDescription;
			}
			set
			{
				planDescription = value;
			}
		}

		public string PlanShortName
		{
			get
			{
				return planshortname ;
			}
			set
			{
				planshortname  = value;
			}
		}
		public int CourtID
		{
			get
			{
				return courtID;
			}
			set
			{
				courtID = value;
			}
		}

		public int CategoryID
		{
			get
			{
				return categoryID;
			}
			set
			{
				categoryID = value;
			}
		}

		public int PriceID
		{
			get
			{
				return pPriceid;
			}
			set
			{
				pPriceid = value;
			}
		}
		public double BasePrice
		{
			get
			{
				return basePrice;
			}
			set
			{
				basePrice = value;
			}
		}

		public double SecondaryPrice
		{
			get
			{
				return secondaryPrice;
			}
			set
			{
				secondaryPrice = value;
			}
		}

		public double BasePercentage
		{
			get
			{
				return basePercentage;
			}
			set
			{
				basePercentage = value;
			}
		}

		public double SecondaryPercentage
		{
			get
			{
				return secondaryPercentage;
			}
			set
			{
				secondaryPercentage = value;
			}
		}

		public double BondBase
		{
			get
			{
				return bondBase;
			}
			set
			{
				bondBase = value;
			}
		}

		public double BondSecondary
		{
			get
			{
				return bondSecondary;
			}
			set
			{
				bondSecondary = value;
			}
		}

		public double BondBasePercentage
		{
			get
			{
				return bondBasePercentage;
			}
			set
			{
				bondBasePercentage = value;
			}
		}

		public double BondSecondaryPercentage
		{
			get
			{
				return bondSecondaryPercentage;
			}
			set
			{
				bondSecondaryPercentage = value;
			}
		}

		public double BondAssumtion
		{
			get
			{
				return bondAssumtion;
			}
			set
			{
				bondAssumtion = value;
			}
		}
		public double FineAssumption
		{
			get
			{
				return fineAssumption;
			}
			set
			{
				fineAssumption = value;
			}
		}
		public DateTime EffectiveDate
		{
			get
			{
				return effectiveDate;
			}
			set
			{
				effectiveDate = value;
			}
		}

		public DateTime EndDate
		{
			get
			{
				return endDate;
			}
			set
			{
				endDate = value;
			}
		}

		public DateTime   EffectiveDateTo
		{
			get
			{
				return effectiveDateTo;
			}
			set
			{
				effectiveDateTo = value;
			}
		}
		public DateTime EffectiveDateFrom
		{
			get
			{
				return effectiveDateFrom;
			}
			set
			{
				effectiveDateFrom = value;
			}
		}
		public DateTime EndDateTo
		{
			get
			{
				return endDateTo;
			}
			set
			{
				endDateTo = value;
			}
		}
		public DateTime EndDateFrom
		{
			get
			{
				return endDateFrom;
			}
			set
			{
				endDateFrom = value;
			}
		}

		public int ShowAll
		{
			get 
			{
				return showAll;
			}
			set
			{
				showAll = value;
			}
		}

		/// <summary>
		/// for Pricing Template
		/// </summary>
		public int TemplateID
		{
			get
			{	
				return templateID;
			}
			set
			{
				templateID = value;
			}
		}

		public string TempateName
		{
			get
			{
				return templateName;
			}
			set
			{
				templateName = value;
			}
		}

		public string TemplateDescription
		{
			get
			{
				return templateDescription;
			}
			set
			{
				templateDescription = value;
			}
		}

		public int EmployeeID
		{
			get
			{
				return empId;
			}
			set
			{
				empId = value;
			}
		}
		//-----------fro Pricing Template Detail
		public int DetailId
		{
			get
			{
				return detailId; 
			}
			set
			{
				detailId = value;			
			}
		}


		#endregion

		#region Methods

		/*
		public int AddPlanInfo()
		{
			
			string[] key1 = {"@PlanShortName","@PlanDescription","@EffectiveDate","@EndDate","@CourtId","@IsActivePlan","@EmployeeId"};
			object[] value1 = {PlanShortName,PlanDescription,EffectiveDate,EndDate,CourtID,ShowAll,EmployeeID};
			try
			{
				object intPlanID =ClsDb.ExecuteScalerBySp("USP_HTS_INSERT_PricingPlan", key1, value1);
				return Convert.ToInt32(intPlanID);
			
			}
			catch(Exception ex )
			{
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);

				return 0;
			}
		}
		*/
		public int AddPlanInfo(DbTransaction trans)
		{
			
			string[] key1 = {"@PlanShortName","@PlanDescription","@EffectiveDate","@EndDate","@CourtId","@IsActivePlan","@EmployeeId","@PlanID","@PlanIdNew"};
			object[] value1 = {PlanShortName,PlanDescription,EffectiveDate,EndDate,CourtID,ShowAll,EmployeeID,0,0};
			try
			{
				object intPlanID =ClsDb.InsertBySPArrRet("USP_HTS_INSERT_PricingPlan", key1, value1,trans);
				return Convert.ToInt32(intPlanID);
			
			}
			catch(Exception ex )
			{
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);

				return 0;
			}
		}
		/*
		public bool DeletePlanDetails(int pid)
		{
			string[] key1 = {"@planid"};
			object[] value1 ={pid};
			try
			{
				ClsDb.ExecuteSP("USP_HTS_Delete_PricePlanDetail",key1,value1);
				return true;
			}
			catch(Exception ex)
			{
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);

				return false;
			}
		}
		*/
		public bool DeletePlanDetails(int pid,DbTransaction trans)
		{
			string[] key1 = {"@planid"};
			object[] value1 ={pid};

			try
			{
				ClsDb.ExecuteSP("USP_HTS_Delete_PricePlanDetail",key1,value1,trans);
				return true;
			}
			catch(Exception ex)
			{
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);

				return false;
			}
		}

		/*
		 public bool UpdatePlanInfo()
		{
			
			string[] key1 = {"@PlanId","@PlanShortName","@PlanDescription","@EffectiveDate","@EndDate","@CourtId","@IsActivePlan","@EmployeeId"};
			object[] value1 = {PlanID,PlanShortName,PlanDescription,EffectiveDate,EndDate,CourtID,ShowAll,EmployeeID};
			try
			{
				ClsDb.ExecuteSP("USP_HTS_UPDATE_PricePlan", key1, value1);
				return true;
			}
			catch(Exception ex)
			{
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);

				return false;
			}
		}
		*/
		public bool UpdatePlanInfo(DbTransaction trans)
		{
			
			string[] key1 = {"@PlanId","@PlanShortName","@PlanDescription","@EffectiveDate","@EndDate","@CourtId","@IsActivePlan","@EmployeeId"};
			object[] value1 = {PlanID,PlanShortName,PlanDescription,EffectiveDate,EndDate,CourtID,ShowAll,EmployeeID};
			try
			{
				ClsDb.ExecuteSP("USP_HTS_UPDATE_PricePlan", key1, value1,trans);
				return true;
			}
			catch(Exception ex)
			{
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);

				return false;
			}
		}
		/*
		 public bool UpdatePlanDetails()
		{			
			string[] key1 = {"@CategoryId","@baseprice","@SecondaryPrice","@BasePercentage","@SecondaryPercent","@BondBase","@BondSecondary","@BondBasePercentage","@BondSecondaryPercentage","@FineAssumption","@BondAssumption","@CourtID","@planid"};
			object[] value1 = {CategoryID,BasePrice,SecondaryPrice,BasePercentage,SecondaryPercentage,BondBase,BondSecondary,BondBasePercentage,BondSecondaryPercentage,FineAssumption,BondAssumtion,CourtID,PlanID};
			try
			{
				ClsDb.ExecuteSP("USP_HTS_UPDATE_PricePlanDetail", key1, value1);
				return true;
			}
			catch(Exception ex)
			{
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);

				return false;
			}
		}
		*/
		public bool UpdatePlanDetails(DbTransaction trans)
		{			
			string[] key1 = {"@CategoryId","@baseprice","@SecondaryPrice","@BasePercentage","@SecondaryPercent","@BondBase","@BondSecondary","@BondBasePercentage","@BondSecondaryPercentage","@FineAssumption","@BondAssumption","@CourtID","@planid"};
			object[] value1 = {CategoryID,BasePrice,SecondaryPrice,BasePercentage,SecondaryPercentage,BondBase,BondSecondary,BondBasePercentage,BondSecondaryPercentage,FineAssumption,BondAssumtion,CourtID,PlanID};
			try
			{
				ClsDb.ExecuteSP("USP_HTS_UPDATE_PricePlanDetail", key1, value1,trans);
				return true;
			}
			catch(Exception ex)
			{
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);

				return false;
			}
		}

		public DataSet GetPlan(int PlanID)

		{
            string[] key1 ={"@PlanID"};
            object[] value1 = {PlanID };
			DataSet ds=ClsDb.Get_DS_BySPArr("USP_HTS_GET_PricingPlanById",key1,value1);
			return ds;
		}
		public DataSet GetPlanDetails(int PlanID)
		{
            string[] key1 = {"@PlanID" };
            object[] value1 = {PlanID };
			//DataSet ds=ClsDb.Get_DS_BySPByOneParmameter("USP_HTS_GET_PricePlanDetail","@PlanID",PlanID);
            DataSet ds = ClsDb.Get_DS_BySPArr("USP_HTS_GET_PricePlanDetail",key1 ,value1 );
			return ds;
		}
		public DataSet GetAllPlan()
		{
			string[] key1 = {"@courtid","@isNew"};
			object[] value1 = {CourtID,IsNew};
			DataSet ds=ClsDb.Get_DS_BySPArr("usp_HTS_GetAllPlan", key1, value1);
			return ds;
		}

		public DataSet GetPlan(string RecordLoadDate, int CourtID,int Old)
		{

            String[] key1 = { "@RecordLoadDate", "@CourtID", "@Old" };
            Object[] value1 = { RecordLoadDate, CourtID,Old };
			
            //DataSet ds=ClsDb.Get_DS_BySPByThreeParmameter("usp_HTS_GetPlanIDtoSelect","@RecordLoadDate",RecordLoadDate,"@CourtID",CourtID,"@Old",Old);
            DataSet ds = ClsDb.Get_DS_BySPArr("usp_HTS_GetPlanIDtoSelect", key1,value1);
			return ds;
		}
		

		/*public int UpdatePricingTemplate()
		{
			string[] key    = {"@TemplateID","@TemplateName","@TemplateDescription","@EmployeeID"};
			object[] value1 = {TemplateID,TempateName,TemplateDescription,EmployeeID };
			object intTemplateID =	 ClsDb.ExecuteScalerBySp("USP_HTS_UPDATE_PricingTemplate",key,value1);
			return Convert.ToInt32(intTemplateID);
		}*/

		public int UpdatePricingTemplate(DbTransaction trans)
		{
			string[] key    = {"@TemplateID","@TemplateName","@TemplateDescription","@EmployeeID"};
			object[] value1 = {TemplateID,TempateName,TemplateDescription,EmployeeID };
			object intTemplateID =	 ClsDb.ExecuteScalerBySp("USP_HTS_UPDATE_PricingTemplate",key,value1,trans);
			return Convert.ToInt32(intTemplateID);
		}

		/*public void UpdatePricingTemplateDetail()
		{
			string[] key    = {"@DetailID","@TemplateID","@CategoryId","@BasePrice","@SecondaryPrice","@BasePercentage","@SecondaryPercentage","@BondBase","@BondSecondary","@BondBasePercentage","@BondSecondaryPercentage","@BondAssumption","@FineAssumption"};
			object[] value1 = {DetailId,TemplateID,CategoryID,BasePrice,SecondaryPrice,BasePercentage,SecondaryPercentage,BondBase,BondSecondary,BondBasePercentage,BondSecondaryPercentage,BondAssumtion,FineAssumption};
			
			bool isAllZero = true;
			for(int i=3; i<value1.Length; i++)
			{
				double dblValue = Convert.ToDouble(value1[i]);
				if( dblValue!=0)
				{
					isAllZero = false;
					break;
				}
			}
			if( isAllZero==false ) // update category which does not have all Pricing zero
			{
				ClsDb.ExecuteScalerBySp("USP_HTS_UPDATE_PricingTemplateDetail",key,value1);
			}
		}*/


		public void UpdatePricingTemplateDetail(DbTransaction trans)
		{
			string[] key    = {"@DetailID","@TemplateID","@CategoryId","@BasePrice","@SecondaryPrice","@BasePercentage","@SecondaryPercentage","@BondBase","@BondSecondary","@BondBasePercentage","@BondSecondaryPercentage","@BondAssumption","@FineAssumption"};
			object[] value1 = {DetailId,TemplateID,CategoryID,BasePrice,SecondaryPrice,BasePercentage,SecondaryPercentage,BondBase,BondSecondary,BondBasePercentage,BondSecondaryPercentage,BondAssumtion,FineAssumption};
			
			bool isAllZero = true;
			for(int i=3; i<value1.Length; i++)
			{
				double dblValue = Convert.ToDouble(value1[i]);
				if( dblValue!=0)
				{
					isAllZero = false;
					break;
				}
			}
			if( isAllZero==false ) // update category which does not have all Pricing zero
			{
				ClsDb.ExecuteScalerBySp("USP_HTS_UPDATE_PricingTemplateDetail",key,value1,trans);
			}
		}


		public void UpdatePricingTemplateDetail(Hashtable ht)
		{
			string[] key    = {"@DetailID","@TemplateID","@CategoryId","@BasePrice","@SecondaryPrice","@BasePercentage","@SecondaryPercentage","@BondBase","@BondSecondary","@BondBasePercentage","@BondSecondaryPercentage","@BondAssumption","@FineAssumption"};
			object[] value1 = {
								  
								  ToInt(ht["DetailId"].ToString()),
								  ToInt(ht["TemplateID"].ToString()),
								  ToInt(ht["CategoryId"].ToString()),
								  ht["BasePrice"]=="" ? 0:Convert.ToDouble( ht["BasePrice"]),
								  ht["SecondaryPrice"]==""	? 0:Convert.ToDouble( ht["SecondaryPrice"]),
								  ToInt(ht["BasePercentage"].ToString()),
								  ToInt(ht["SecondaryPercentage"].ToString()),
								  ht["BondBase"]=="" ? 0:Convert.ToDouble(ht["BondBase"]),
								  ht["BondSecondary"]==""	? 0:Convert.ToDouble(ht["BondSecondary"]),
								  ToInt(ht["BondBasePercentage"].ToString()), 
								  ToInt(ht["BondSecondaryPercentage"].ToString()),
								  ht["BondAssumption"]==""	? 0:Convert.ToDouble(ht["BondAssumption"]),
								  ht["FineAssumption"]==""	? 0:Convert.ToDouble(ht["FineAssumption"])
							  };
			
			bool isAllZero = true;
			for(int i=3; i<value1.Length; i++)
			{
				if( value1[i].ToString()!="0" && value1[i].ToString()!="")
				{
					isAllZero = false;
					break;
				}
			}
			if( isAllZero==false ) // update category which does not have all Pricing zero
			{
				ClsDb.ExecuteScalerBySp("USP_HTS_UPDATE_PricingTemplateDetail",key,value1);
			}
		}


		/*
		public int AddPricingTemplate()
		{
			string[] key    = {"@TemplateName","@TemplateDescription","@EmployeeID"};
			object[] value1 = {TempateName,TemplateDescription,EmployeeID };
			object objTemplateID  = ClsDb.ExecuteScalerBySp("USP_HTS_Insert_PricingTemplate",key,value1);
			int intTemplateID = ((int) Convert.ChangeType(objTemplateID,typeof(int)));
			return intTemplateID;
		}
		*/

		public int AddPricingTemplate(DbTransaction trans)
		{
			string[] key    = {"@TemplateName","@TemplateDescription","@EmployeeID"};
			object[] value1 = {TempateName,TemplateDescription,EmployeeID };

			object objTemplateID  = ClsDb.ExecuteScalerBySp("USP_HTS_Insert_PricingTemplate",key,value1,trans);

			int intTemplateID = ((int) Convert.ChangeType(objTemplateID,typeof(int)));
			return intTemplateID;
		}


		/*
		public void AddPricingTemplateDetail()
		{
			string[] key    = {"@TemplateID","@CategoryId","@BasePrice","@SecondaryPrice","@BasePercentage","@SecondaryPercentage","@BondBase","@BondSecondary","@BondBasePercentage","@BondSecondaryPercentage","@BondAssumption","@FineAssumption"};
			object[] value1 = {TemplateID,CategoryID,BasePrice,SecondaryPrice,BasePercentage,SecondaryPercentage,BondBase,BondSecondary,BondBasePercentage,BondSecondaryPercentage,BondAssumtion,FineAssumption};
			bool isAllZero = true;
			for(int i=2; i<value1.Length; i++)
			{
				double dblValue = Convert.ToDouble(value1[i]);
				if( dblValue!=0)
				{
					isAllZero = false;
					break;
				}
			}

			if( isAllZero==false ) // Add category which does not have all Pricing zero
			{
				ClsDb.ExecuteScalerBySp("USP_HTS_Insert_PricingTemplateDetail",key,value1);
			}
		}
*/
		    public void AddPricingTemplateDetail(DbTransaction trans)
		{
			string[] key    = {"@TemplateID","@CategoryId","@BasePrice","@SecondaryPrice","@BasePercentage","@SecondaryPercentage","@BondBase","@BondSecondary","@BondBasePercentage","@BondSecondaryPercentage","@BondAssumption","@FineAssumption"};
			object[] value1 = {TemplateID,CategoryID,BasePrice,SecondaryPrice,BasePercentage,SecondaryPercentage,BondBase,BondSecondary,BondBasePercentage,BondSecondaryPercentage,BondAssumtion,FineAssumption};
			bool isAllZero = true;
			for(int i=2; i<value1.Length; i++)
			{
				double dblValue = Convert.ToDouble(value1[i]);
				if( dblValue!=0)
				{
					isAllZero = false;
					break;
				}
			}
			if( isAllZero==false ) // Add category which does not have all Pricing zero
			{
				ClsDb.ExecuteScalerBySp("USP_HTS_Insert_PricingTemplateDetail",key,value1,trans);
			}
		}


		public bool AddPricingTemplateDetail(Hashtable ht)
		{
			string[] key    = {"@TemplateID","@CategoryId","@BasePrice","@SecondaryPrice","@BasePercentage","@SecondaryPercentage","@BondBase","@BondSecondary","@BondBasePercentage","@BondSecondaryPercentage","@BondAssumption","@FineAssumption"};
			object[] value1 = {
								  ToInt(ht["TemplateID"].ToString()),
								  ToInt(ht["CategoryId"].ToString()),
								  ht["BasePrice"]==""	? 0:Convert.ToDouble( ht["BasePrice"]),
								  ht["SecondaryPrice"]==""	? 0:Convert.ToDouble( ht["SecondaryPrice"]),
								  ToInt(ht["BasePercentage"].ToString()),
								  ToInt(ht["BasePercentage"].ToString()),
								  ToInt(ht["BasePercentage"].ToString()),
								  ToInt(ht["SecondaryPercentage"].ToString()),
								  ht["BondBase"]=="" ? 0:Convert.ToDouble(ht["BondBase"]),
								  ht["BondSecondary"]=="" ? 0:Convert.ToDouble(ht["BondSecondary"]),
								  ToInt(ht["BondBasePercentage"].ToString()), 
								  ToInt(ht["BondSecondaryPercentage"].ToString()),
								  ht["BondAssumption"]==""	? 0:Convert.ToDouble(ht["BondAssumption"]),
								  ht["FineAssumption"]==""	? 0:Convert.ToDouble(ht["FineAssumption"])
							  };
			ClsDb.ExecuteScalerBySp("USP_HTS_Insert_PricingTemplateDetail",key,value1);
			return true;
		}


		private int ToInt(string str)
		{
			if (str.Trim()!="" )
			{
				//str = (str.IndexOf(".")==-1? str: str.Substring(0,str.IndexOf(".") )) ; 
				str.TrimStart('-');
				if(str.IndexOf(".")!=-1)
				{
					double d = Convert.ToDouble(str);
					d = Math.Round(d);
					return Convert.ToInt32(d);
				}
				else
				{
					return 	Convert.ToInt32(str);
				}
			}
			else
			{
				return 0;
			}

		}

		public DataSet GetPricingTemplates()
		{
			DataSet ds=ClsDb.Get_DS_BySP("usp_HTS_GetPricingTemplates");
			return ds;
		}
		public DataSet GetPricingTemplates(int intTemplateID)
		{
			DataSet ds=ClsDb.Get_DS_BySPByOneParmameter("usp_HTS_GetPricingTemplates","@TemplateID",intTemplateID);
			return ds;
		}
        //Change by Ajmal
        //public SqlDataReader Get_rdr_PricingTemplates()
		public IDataReader Get_rdr_PricingTemplates()
		{
			IDataReader  rdr=ClsDb.Get_DR_BySP("usp_HTS_GetPricingTemplates");
			return rdr;
		}

		//		public DataSet GetPricingTemplates(int TemplateID)
		//		{
		//			DataSet ds=ClsDb.Get_DS_BySPByOneParmameter("usp_HTS_GetPricingTemplates","@TemplateID",TemplateID);
		//			return ds;
		//		}


		//for Updating Purpose
		public DataSet GetPricingTemplateDetails(int TemplateID)
		{
			DataSet ds=ClsDb.Get_DS_BySPByOneParmameter("USP_HTS_GET_PricingTemplateDetail","@templateid",TemplateID);
			return ds;
		}

		//for Adding pupose
		public DataSet GetPricingTemplateDetails()
		{
			//USP_HTS_GET_PricingTemplateDetail without Parameter means only violationCategory info is gotten
			//not PricingTemplate into so that page only display Category and all fields will remain empty
			//to input data for Adding purpose
			DataSet ds=ClsDb.Get_DS_BySP("USP_HTS_GET_PricingTemplateDetail");
			return ds;
		}
        //Change by Ajmal
        //public SqlDataReader  GetAllCourtName()
		public IDataReader  GetAllCourtName()
		{
			IDataReader  rdr= ClsDb.Get_DR_BySP("usp_HTS_GetShortCourtName");
			return rdr;
		}
		
		public DataSet SearchPricingPlans()
		{
			string[] key  = {"@EffectiveDateFrom","@EffectiveDateTo","@EndDateFrom","@EndDateTo","@CourtId","@ShowAll"};

			object[] value1 = {EffectiveDateFrom,EffectiveDateTo,EndDateFrom,EndDateTo,CourtID,ShowAll};
			
			DataSet ds=ClsDb.Get_DS_BySPArr("USP_HTS_GET_PricingPlans",key,value1);
			return ds;
		}

		#endregion

		public clsPricingPlans()
		{
			//
			// TODO: Add constructor logic here
			//
		}
	}
}

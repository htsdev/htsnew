using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.WebControls;
using System.IO;
using WebSupergoo.ABCpdf6;
using MSXML2;
using System.Net;



namespace lntechNew.Components.ClientInfo
{


    public class clsManualCC
    {

        //Create an Instance of Data Access Class

        clsENationWebComponents ClsDb = new clsENationWebComponents();
        clsENationWebComponents clstran = new clsENationWebComponents(true);
        clsLogger bugTracker = new clsLogger();
        AppSettingsReader asReader = new AppSettingsReader();
        //  clsSession cSession = new clsSession();

        #region "Variables"

        string fname;
        string lname;
        string nameoncard;
        string ccno;
        string cin;
        string clientype;
        string description;
        //string transacttype;
        string transnnum = "";
        string retcode;
        string errmsg = "";
        string response_subcode;
        string response_reason_code;
        string response_reason_text;
        string auth_code;
        string avs_code;
        string expdate;
        bool status;
        decimal amount;
        int employeeid;
        int aproveflage;
        int clearflage;
        int payment_void;
        int voidpayment;
        public bool TransactionType;
        //Added By Zeeshan Ahmed
        string x_response_code = "";
        string x_response_subcode = "";
        string x_response_reason_code = "";
        string x_response_reason_text = "";
        string x_auth_code = "";
        string x_avs_code = "";
        string x_trans_id = "";
        string x_invoice_num = "";
        int invoiceno = 0;
        bool isVoid = false;
        string reftransactionnum = "";

        #endregion

        #region "Properties"
        public string Fname
        {
            get
            {
                return fname;
            }
            set
            {
                fname = value;
            }
        }
        public string Lname
        {
            get
            {
                return lname;
            }
            set
            {
                lname = value;
            }
        }
        public string Nameoncard
        {
            get
            {
                return nameoncard;
            }
            set
            {
                nameoncard = value;
            }
        }
        public string CCNO
        {
            get
            {
                return ccno;
            }
            set
            {
                ccno = value;
            }
        }
        public string CIN
        {
            get
            {
                return cin;
            }
            set
            {
                cin = value;
            }
        }
        public string ClientType
        {
            get
            {
                return clientype;
            }
            set
            {
                clientype = value;
            }
        }
        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }
        //public string TransactType
        //{
        //    get
        //    {
        //        return transacttype;
        //    }
        //    set
        //    {
        //        transacttype = value;
        //    }
        //}
        public string TransNum
        {
            get
            {
                return transnnum;
            }
            set
            {
                transnnum = value;
            }
        }
        public string RetCode
        {
            get
            {
                return retcode;
            }
            set
            {
                retcode = value;
            }
        }
        public string Errmsg
        {
            get
            {
                return errmsg;
            }
            set
            {
                errmsg = value;
            }
        }

        public string Response_Subcode
        {
            get
            {
                return response_subcode;
            }
            set
            {
                response_subcode = value;
            }
        }
        public string Response_Reason_Code
        {
            get
            {
                return response_reason_code;
            }
            set
            {
                response_reason_code = value;
            }
        }
        public string Response_Reason_Text
        {
            get
            {
                return response_reason_text;
            }
            set
            {
                response_reason_text = value;
            }
        }
        public string Auth_Code
        {
            get
            {
                return auth_code;
            }
            set
            {
                auth_code = value;
            }
        }
        public string Avs_Code
        {
            get
            {
                return avs_code;
            }
            set
            {
                avs_code = value;
            }
        }
        public string ExpDate
        {
            get
            {
                return expdate;
            }
            set
            {
                expdate = value;
            }
        }
        public bool Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }
        public decimal Amount
        {
            get
            {
                return amount;
            }
            set
            {
                amount = value;
            }
        }
        public int EmployeeID
        {
            get
            {
                return employeeid;
            }
            set
            {
                employeeid = value;
            }
        }
        public int Aproveflage
        {
            get
            {
                return aproveflage;
            }
            set
            {
                aproveflage = value;
            }
        }
        public int Clearflage
        {
            get
            {
                return clearflage;
            }
            set
            {
                clearflage = value;
            }
        }
        public int Payment_Void
        {
            get
            {
                return payment_void;
            }
            set
            {
                payment_void = value;
            }
        }
        public int VoidPayment
        {
            get
            {
                return voidpayment;
            }
            set
            {
                voidpayment = value;
            }
        }

        #endregion

        //Process Credit Card Transaction 
        #region "Process & Updae Credit Card Transaction "

        public void ProcessCreditCard(int paymentid, string ccno)
        {
            try
            {
                string PostData = "";
              
                PostData = "x_Login=" + asReader.GetValue("Login", typeof(string)).ToString();               
                PostData += "&x_tran_key=" + asReader.GetValue("Tran_Key", typeof(string)).ToString();
                PostData += "&x_Version=3.0";
                PostData += "&x_Test_Request=False";  //   ' Live Mode
                //PostData += "&x_Test_Request=True";  //   'True since we are testing                           
                PostData += "&x_ADC_Delim_Data=TRUE";  //'we want comma delimited
                PostData += "&x_ADC_URL=FALSE";  // 'do not want to redirect
                //Now add the form fields

                #region Commented Code
                
               
                //string PostData = "";
                //PostData = "x_Login=ant481205930";        //test account for Authorize.Net
                //PostData += "&x_tran_key=83gSdU2xvkXBTwtg";
                //PostData += "&x_Version=3.0";
                ////PostData += "&x_Test_Request=False";  //   ' since we are testing
                //PostData += "&x_Test_Request=" + TransactionType;
                ////PostData += "&x_Test_Request=TRUE";  //   'True since we are testing
                //PostData += "&x_ADC_Delim_Data=TRUE";  //'we want comma delimited
                //PostData += "&x_ADC_URL=FALSE";  // 'do not want to redirect
                ////Now add the form fields
                #endregion

                //Getting Info of client credit card for processing by paymentid number
                string[] key1 ={ "@pid" };
                object[] value1 ={ paymentid };
                DataSet ds_ccinfo = ClsDb.Get_DS_BySPArr("USP_HTS_GET_CREDITCARD_BYCC", key1, value1);
                invoiceno = paymentid;
                PostData += "&x_invoice_num=" + paymentid;
                PostData += "&x_Card_Num=" + ccno;
                PostData += "&x_Exp_Date=" + ds_ccinfo.Tables[0].Rows[0]["expdate"].ToString();
                PostData += "&x_amount   =" + ds_ccinfo.Tables[0].Rows[0]["amount"].ToString();
                PostData += "&x_card_code=" + ds_ccinfo.Tables[0].Rows[0]["cin"].ToString();              
                PostData += "&x_Description=" + ds_ccinfo.Tables[0].Rows[0]["clienttype"].ToString();
                PostData += "&x_First_Name=" + ds_ccinfo.Tables[0].Rows[0]["fname"].ToString();
                PostData += "&x_Last_Name =" + ds_ccinfo.Tables[0].Rows[0]["lname"].ToString();
                

                string connection = asReader.GetValue("ConnectionURL", typeof(string)).ToString();

                char[] sep = { ',' };

                #region commented code
                //XMLHTTP objHTTP = new XMLHTTP();
                //objHTTP.open("POST", connection, false, null, null);
                //objHTTP.send(PostData);
                ////string response =objHTTP.responseText;

                //if (objHTTP.status == 200)
                //{
                //    string res = objHTTP.responseText;

                //    string[] alag = res.Split(sep);
                //    //    ScheduleID = Scheduleid;
                //    UpdateCreditCardTransaction(alag);
                //}
                //else
                //{
                //    throw new Exception("Cannot Retrieve Response From Server");
                //}
                #endregion

                StreamWriter myWriter = null;
                string[] alag;

                HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(connection);

                objRequest.Method = "POST";
                objRequest.ContentLength = PostData.Length;
                objRequest.ContentType = "application/x-www-form-urlencoded";
                string res;

                try
                {
                    myWriter = new StreamWriter(objRequest.GetRequestStream());
                    myWriter.Write(PostData);
                    myWriter.Close();
                    HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
                    using (StreamReader sr = new StreamReader(objResponse.GetResponseStream()))
                    {
                        res = sr.ReadToEnd();
                        alag = res.Split(sep);

                        sr.Close();
                    }
                    //ScheduleID = Scheduleid;
                    UpdateCreditCardTransaction(alag);


                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// Update credit card table and payments table	   					
        private void UpdateCreditCardTransaction(string[] array)
        {
            try
            {
                x_response_code = array[0];
                //Response Code:
                //1 = This transaction has been approved.
                //2 = This transaction has been declined.
                //3 = There has been an error processing this transaction.
                x_response_subcode = array[1];
                x_response_reason_code = array[2];
                x_response_reason_text = array[3];
                x_auth_code = array[4];   //6 digit approval code
                x_avs_code = array[5];
                /*
                    'Address Verification system:
                'A = Address (street) matches, Zip does not
                'E = AVS error
                'N = No match on address or zip
                'P = AVS Not Applicable
                'R = Retry, system unavailable or timed out
                'S = service not supported by issuer
                'U = address information is unavailable
                'W = 9 digit Zip matches, address does not
                'X = exact AVS match
                'Y = address and 5 digit zip match
                'Z = 5 digit zip matches, address does not
                */
                x_trans_id = array[6];  //  'transaction id
                string x_payment_id = array[7];
                string x_description = array[8];
                string x_amount = array[9];
                string x_method = array[10];
                string x_type = array[11];
                string x_cust_id = array[12];
                string x_first_name = array[13];
                string x_last_name = array[14];
                string x_company = array[15];
                //string x_address = array[16];
                //string x_city = array[17];
                //string x_state = array[18];
                //string x_zip = array[19];
                //string x_country = array[20];
                //string x_phone = array[21];
                //string x_fax = array[22];
                //string x_email = array[23];
                string x_ship_to_first_name = array[24];
                string x_ship_to_last_name = array[25];
                string x_ship_to_company = array[26];
                string x_ship_to_address = array[27];
                string x_ship_to_city = array[28];
                string x_ship_to_state = array[29];
                //string x_ship_to_zip = array[30];
                string x_ship_to_country = array[31];
                string x_tax = array[32];
                string x_duty = array[33];
                string x_freight = array[34];
                string x_tax_exempt = array[35];
                string x_po_num = array[36];
                string x_md5_hash = array[37];

                // Log Transaction Detail
                isVoid = false;
                reftransactionnum = invoiceno.ToString();
                TrackTransactionLog("");

                if (x_response_code == "1")
                {
                    clstran.DBBeginTransaction();
                    string[] key1 ={ "@paymentid", "@status", "@errmsg", "@transnum", "@response_subcode", "@response_reason_code", "@response_reason_text", "@auth_code", "@avs_code", "@voidpayment" };
                    object[] value1 ={ x_payment_id, "1", "", x_trans_id, x_response_subcode, x_response_reason_code, x_response_reason_text, x_auth_code, x_avs_code, 0 };
                    clstran.ExecuteSP("USP_HTS_UpdateManualCC", key1, value1, 1);
                    clstran.DBTransactionCommit();
                    //sending email
                    SendEmail(Convert.ToInt32(x_payment_id), 0);

                }
                else if (x_response_code == "2")
                {
                    clstran.DBBeginTransaction();
                    string[] key1 ={ "@paymentid", "@status", "@errmsg", "@transnum", "@response_subcode", "@response_reason_code", "@response_reason_text", "@auth_code", "@avs_code", "@voidpayment" };
                    object[] value1 ={ x_payment_id, "0", "", x_trans_id, x_response_subcode, x_response_reason_code, x_response_reason_text, x_auth_code, x_avs_code, 0 };
                    clstran.ExecuteSP("USP_HTS_UpdateManualCC", key1, value1, 1);
                    clstran.DBTransactionCommit();
                    throw new Exception("The transaction was not approved: " + x_response_reason_text);
                }
                else
                {
                    clstran.DBBeginTransaction();
                    string[] key1 ={ "@paymentid", "@status", "@errmsg", "@transnum", "@response_subcode", "@response_reason_code", "@response_reason_text", "@auth_code", "@avs_code", "@voidpayment" };
                    object[] value1 ={ x_payment_id, "0", "", x_trans_id, x_response_subcode, x_response_reason_code, x_response_reason_text, x_auth_code, x_avs_code, 0 };
                    clstran.ExecuteSP("USP_HTS_UpdateManualCC", key1, value1, 1);
                    clstran.DBTransactionCommit();
                    throw new Exception("An error occured during processing. " + x_response_reason_text);
                }

            }
            catch (Exception ex)
            {
                // if (ex.Message.StartsWith("An error occured during processing.") || ex.Message.StartsWith("The transaction was not approved:"))
                // {
                throw new Exception(ex.Message);
                // }
                // else
                // {
                ////     clstran.DBTransactionRollback();
                //     bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                // }
            }
        }

        #endregion

        /// Voids Credit card transaction
        #region  "Void & VoidUpdae Credit Card Transaction "

        public void VoidCreditCardTransaction(int paymentid)
        {
            //Getting Info of client credit card for processing by paymentid number
            string[] key1 ={ "@pid" };
            object[] value1 ={ paymentid };
            DataSet ds_ccinfo = ClsDb.Get_DS_BySPArr("USP_HTS_GET_CREDITCARD_BYCC", key1, value1);

            // Added By Zeeshan Ahmed
            //Get Customer Information 
            ccno = ds_ccinfo.Tables[0].Rows[0]["ccno"].ToString();
            expdate = ds_ccinfo.Tables[0].Rows[0]["expdate"].ToString();
            nameoncard = ds_ccinfo.Tables[0].Rows[0]["fname"].ToString() + ds_ccinfo.Tables[0].Rows[0]["lname"].ToString();
            cin = ds_ccinfo.Tables[0].Rows[0]["cin"].ToString();
            amount = Convert.ToDecimal(ds_ccinfo.Tables[0].Rows[0]["amount"]);

            string PostData = "";
            //PostData = "x_Login=ant481205930";        //test account for Authorize.Net
            //PostData += "&x_tran_key=83gSdU2xvkXBTwtg";
            PostData = "x_Login=" + asReader.GetValue("Login", typeof(string)).ToString();
            PostData += "&x_tran_key=" + asReader.GetValue("Tran_Key", typeof(string)).ToString();

            PostData += "&x_Version=3.0";
            PostData += "&x_Test_Request=False";  //   'since we are not testing
            //PostData += "&x_Test_Request=TRUE";  //   'since we are testing
            PostData += "&x_ADC_Delim_Data=TRUE";  //'we want comma delimited
            PostData += "&x_ADC_URL=FALSE";  // 'do not want to redirect
            PostData += "&x_TYPE=VOID";
            PostData += "&x_TRANS_ID=" + ds_ccinfo.Tables[0].Rows[0]["transNum"].ToString();
            transnnum = ds_ccinfo.Tables[0].Rows[0]["transNum"].ToString();


            //string connection = "https://secure.authorize.net/gateway/transact.dll";
            string connection = asReader.GetValue("ConnectionURL", typeof(string)).ToString();

            char[] sep = { ',' };

            StreamWriter myWriter = null;
            string[] alag;

            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(connection);

            objRequest.Method = "POST";
            objRequest.ContentLength = PostData.Length;
            objRequest.ContentType = "application/x-www-form-urlencoded";
            string res;

            try
            {
                myWriter = new StreamWriter(objRequest.GetRequestStream());
                myWriter.Write(PostData);
                myWriter.Close();
                HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
                using (StreamReader sr = new StreamReader(objResponse.GetResponseStream()))
                {
                    res = sr.ReadToEnd();
                    alag = res.Split(sep);

                    sr.Close();
                }
                //ScheduleID = Scheduleid;
                UpdateCreditCardTransaction_Void(alag, paymentid);


            }
            catch (Exception ex)
            {
                throw ex;
            }
                                    //XMLHTTP objHTTP = new XMLHTTP();
            //objHTTP.open("POST", connection, false, null, null);
            //objHTTP.send(PostData);
            //string response = objHTTP.responseText;

            //if (objHTTP.status == 200)
            //{
            //    string res = objHTTP.responseText;

            //    string[] alag = res.Split(sep);
            //    UpdateCreditCardTransaction_Void(alag, paymentid);
            //}
            //else
            //{
            //    throw new Exception("Cannot Retrieve Response From Server");
            //}

        }

        private void UpdateCreditCardTransaction_Void(string[] array, int paymentid)
        {

            x_response_code = array[0];
            //Response Code:
            //1 = This transaction has been approved.
            //2 = This transaction has been declined.
            //3 = There has been an error processing this transaction.

            x_response_subcode = array[1];
            x_response_reason_code = array[2];
            x_response_reason_text = array[3];
            x_auth_code = array[4];   //6 digit approval code
            x_avs_code = array[5];
            string x_amount = array[9];
            /*
                'Address Verification system:
            'A = Address (street) matches, Zip does not
            'E = AVS error
            'N = No match on address or zip
            'P = AVS Not Applicable
            'R = Retry, system unavailable or timed out
            'S = service not supported by issuer
            'U = address information is unavailable
            'W = 9 digit Zip matches, address does not
            'X = exact AVS match
            'Y = address and 5 digit zip match
            'Z = 5 digit zip matches, address does not
            */

            x_trans_id = array[6];  //  'transaction id
            /*
            string x_invoice_num             = array[7];
            string x_description             = array[8];
            string x_amount                  = array[9];
            string x_method                  = array[10];
            string x_type                    = array[11];
            string x_cust_id                 = array[12];
            string x_first_name              = array[13];
            string x_last_name               = array[14];
            string x_company                 = array[15];
            string x_address                 = array[16];
            string x_city                    = array[17];
            string x_state                   = array[18];
            string x_zip                     = array[19];
            string x_country                 = array[20];
            string x_phone                   = array[21];
            string x_fax                     = array[22];
            string x_email                   = array[23];
            string x_ship_to_first_name      = array[24];
            string x_ship_to_last_name       = array[25];
            string x_ship_to_company         = array[26];
            string x_ship_to_address         = array[27];
            string x_ship_to_city            = array[28];
            string x_ship_to_state           = array[29];
            string x_ship_to_zip             = array[30];
            string x_ship_to_country         = array[31];
            string x_tax                     = array[32];
            string x_duty                    = array[33];
            string x_freight                 = array[34];
            string x_tax_exempt              = array[35];
            string x_po_num                  = array[36];
            string x_md5_hash                = array[37];
            */
            ClearCreditCardInformation();
            // Log TRansaction
            isVoid = true;
            reftransactionnum = transnnum;
            TrackTransactionLog(paymentid.ToString());

            if (x_response_code == "1")
            {
                clstran.DBBeginTransaction();
                string[] key1 ={ "@paymentid", "@status", "@errmsg", "@transnum", "@response_subcode", "@response_reason_code", "@response_reason_text", "@auth_code", "@avs_code", "@voidpayment", "@descript", "@empid" };
                object[] value1 ={ paymentid, "1", "", x_trans_id, x_response_subcode, x_response_reason_code, x_response_reason_text, x_auth_code, x_avs_code, 1, description, employeeid };
                clstran.ExecuteSP("USP_HTS_UpdateManualCC_Void", key1, value1, 1);
                clstran.DBTransactionCommit();
                //sending email
                SendEmail(paymentid, 2);
                //
            }
            else if (x_response_code == "2")
            {
                throw new Exception("The transaction was not approved: " + x_response_reason_text);
            }
            else
            {
                throw new Exception("An error occured during processing. " + x_response_reason_text);
            }
        }

        #endregion


        /// Refund Credit card transaction
        #region  "Refund & RefundUpdae Credit Card Transaction "

        public void RefundCreditCardTransaction(int paymentid)
        {
            //Getting Info of client credit card for processing by paymentid number
            string[] key1 ={ "@pid" };
            object[] value1 ={ paymentid };
            DataSet ds_ccinfo = ClsDb.Get_DS_BySPArr("USP_HTS_GET_CREDITCARD_BYCC", key1, value1);

            string PostData = "";
            //PostData = "x_Login=ant481205930";        //test account for Authorize.Net
            //PostData += "&x_tran_key=83gSdU2xvkXBTwtg";

            PostData = "x_Login=" + asReader.GetValue("Login", typeof(string)).ToString();
            PostData += "&x_tran_key=" + asReader.GetValue("Tran_Key", typeof(string)).ToString();

            PostData += "&x_Version=3.0";
           PostData += "&x_Test_Request=False";  //   'since we are not testing
           // PostData += "&x_Test_Request=TRUE";  //   'since we are testing
            PostData += "&x_ADC_Delim_Data=TRUE";  //'we want comma delimited
            PostData += "&x_ADC_URL=FALSE";  // 'do not want to redirect            
            string cc = ds_ccinfo.Tables[0].Rows[0]["ccno"].ToString();
            PostData += "&x_Card_Num=" + cc.Substring(cc.Length - 4, 4);
            PostData += "&x_amount=" + ds_ccinfo.Tables[0].Rows[0]["amount"].ToString();
            PostData += "&x_TYPE=CREDIT";
            PostData += "&x_TRANS_ID=" + ds_ccinfo.Tables[0].Rows[0]["transNum"].ToString();

         //   string connection = "https://secure.authorize.net/gateway/transact.dll";

            string connection = asReader.GetValue("ConnectionURL", typeof(string)).ToString();

            char[] sep = { ',' };
            //XMLHTTP objHTTP = new XMLHTTP();
            //objHTTP.open("POST", connection, false, null, null);
            //objHTTP.send(PostData);

            //if (objHTTP.status == 200)
            //{
            //    string res = objHTTP.responseText;

            //    string[] alag = res.Split(sep);
            //    UpdateCreditCardTransaction_Refund(alag, paymentid);
            //}
            //else
            //{
            //    throw new Exception("Cannot Retrieve Response From Server");
            //}

            StreamWriter myWriter = null;
            string[] alag;

            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(connection);

            objRequest.Method = "POST";
            objRequest.ContentLength = PostData.Length;
            objRequest.ContentType = "application/x-www-form-urlencoded";
            string res;

            try
            {
                myWriter = new StreamWriter(objRequest.GetRequestStream());
                myWriter.Write(PostData);
                myWriter.Close();
                HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
                using (StreamReader sr = new StreamReader(objResponse.GetResponseStream()))
                {
                    res = sr.ReadToEnd();
                    alag = res.Split(sep);

                    sr.Close();
                }
                //ScheduleID = Scheduleid;
                UpdateCreditCardTransaction_Refund(alag, paymentid);


            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private void UpdateCreditCardTransaction_Refund(string[] array, int paymentid)
        {

            //Response Code:
            //1 = This transaction has been approved.
            //2 = This transaction has been declined.
            //3 = There has been an error processing this transaction.

            string x_response_code = array[0];
            string x_response_subcode = array[1];
            string x_response_reason_code = array[2];
            string x_response_reason_text = array[3];
            string x_auth_code = array[4];   //6 digit approval code
            string x_avs_code = array[5];
            string x_amount = array[9];
            string x_type = array[11];

            string x_trans_id = array[6];  //  'transaction id

            if (x_response_code == "1")
            {
                clstran.DBBeginTransaction();
                string[] key1 ={ "@paymentid", "@status", "@errmsg", "@transnum", "@response_subcode", "@response_reason_code", "@response_reason_text", "@auth_code", "@avs_code", "@refundamount", "@descript", "@empid" };
                object[] value1 ={ paymentid, "1", "", x_trans_id, x_response_subcode, x_response_reason_code, x_response_reason_text, x_auth_code, x_avs_code, 1, description, employeeid };
                clstran.ExecuteSP("USP_HTS_UpdateManualCC_Refund", key1, value1, 1);
                clstran.DBTransactionCommit();
                //sending email
                SendEmail(paymentid, 1);

                //
            }
            else if (x_response_code == "2")
            {
                throw new Exception("The transaction was not approved: " + x_response_reason_text);
            }
            else
            {
                throw new Exception("An error occured during processing. " + x_response_reason_text);
            }
        }

        #endregion

        #region "Methods"

        public bool ProcessData()
        {
            //object for transaction begin
            clsENationWebComponents clstran = new clsENationWebComponents(true);
            int rowid = 0;
            try
            {
                {
                    //string tempcardnum=CardNumber.Substring(0,1)+"*****"+CardNumber.Substring(12,4);
                    string tempcc = "";
                    if (ccno.Length > 0)
                        tempcc = ccno.Substring(0, 1) + "*****" + ccno.Substring(ccno.Length - 4, 4);
                    clstran.DBBeginTransaction();
                    string[] key1 ={"@fname","@lname","@nameoncard","@ccno","@expdate","@cin","@amount","@clienttype","@description",
                        "@employeeid", "@rowid"};

                    object[] value1 ={fname,lname,nameoncard,tempcc,expdate,cin,amount,clientype,description,
                                      employeeid,rowid};
                    //clstran.ExecuteSP("USP_HTS_ProcessData", key1, value1, 1);
                    rowid = Convert.ToInt16(clstran.InsertBySPArrRet("USP_HTS_InsertManualCC", key1, value1));
                    clstran.DBTransactionCommit();

                    //To Validate/Process CreditCard   //ScheduleID for checking if normal payment is being paid or schedule payment
                    ProcessCreditCard(rowid, ccno); //providing cardno as it get hidden in DB
                }

                return true;
            }
            catch (Exception ex)
            {

                if (ex.Message.StartsWith("An error occured during processing.") || ex.Message.StartsWith("The transaction was not approved:"))
                {
                    throw new Exception(ex.Message);
                }
                else
                {
                    VoidCreditCardTransaction(rowid);
                    clstran.DBTransactionRollback();
                    bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                    return false;
                }
            }
        }

        /// ///// Fill datagrid manualcc report        
        public DataTable GetManualCCDetail(DateTime stdate, DateTime enddate, int clienttype)
        {
            try
            {

                string[] key1 ={ "@startdate", "@enddate", "@clienttype" };
                object[] value1 ={ stdate, enddate, clienttype };

                DataTable dt = ClsDb.Get_DT_BySPArr("USP_HTS_GET_Manualcc_Report", key1, value1);
                return dt;
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public DataSet GetCardType()
        {
            //			ClsDb.DBBeginTransaction();
            //			DataSet ds=ClsDb.Get_DS_BySP("USP_HTS_Get_CardType");
            //			ClsDb.DBTransactionCommit();
            //			return ds;
            return ClsDb.Get_DS_BySP("USP_HTS_Get_CardType");
        }

        //Added By Zeeshan To Log Transaction 
        public void TrackTransactionLog(string invoiceno)
        {
            try
            {
                string[] key_log = { "@Ticketid", "@invnumber", "@status", "@errmsg", "@transnum", "@response_subcode", "@response_reason_code", "@response_reason_text", "@auth_code", "@avs_code", "@approvedflag", "@amount", "@ccnum", "@expdate", "@name", "@cin", "@cardtype", "@paymentmethod", "@empid", "@RetCode", "@ClearFlag", "@TransType", "@isVoid", "@RefTransNum" };
                object[] value_log ={ 0, invoiceno, "0", errmsg, x_trans_id, x_response_subcode, x_response_reason_code, x_response_reason_text, x_auth_code, x_avs_code, aproveflage, amount, EncryptCreditCard(ccno), expdate, nameoncard, cin, 0, 5, employeeid, "", 0, 2, isVoid, reftransactionnum };
                clstran.ExecuteScalerBySp("USP_HTS_INSERT_CREDITCARD_INFO_FOR_LOG", key_log, value_log);
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        public string EncryptCreditCard(string ccno)
        {
            if (ccno != "")
                return ccno.Substring(0, 1) + "******" + ccno.Substring(ccno.Length - 4, 4);

            return ccno;
        }


        public void ClearCreditCardInformation()
        {
            //ccno="";
            //expdate="";
            //nameoncard="";
            //cin = "";
        }

        public void SendEmail(int payID, int type)
        {
            string[] key ={ "@PaymentID" };
            object[] value1 ={ payID };
            DataSet ds = ClsDb.Get_DS_BySPArr("usp_hts_MCC_Email", key, value1);
            if (ds.Tables[0].Rows.Count > 0)
            {
                string payType = String.Empty;
                if (type == 1)
                {
                    payType = "Refund";
                }
                else if (type == 2)
                {
                    payType = "Void";
                }
                else
                {
                    payType = "Charged";
                }

                string str = "<html> " +
                                "<table>" +
                                    "<tr>" +
                                    "<td style='height:11px'></td>" +
                                        "<td></td>" +
                                     "</tr>" +
                                    "<tr>" +
                                        "<td>Card Holder:</td>" +
                                        "<td>" + ds.Tables[0].Rows[0]["cardholder"].ToString() + "</td>" +
                                     "</tr>" +
                                     "<tr>" +
                                         "<td>Client Type:</td>" +
                                         "<td>" + ds.Tables[0].Rows[0]["clienttype"].ToString() + "</td>" +
                                     "</tr>" +
                                     "<tr>" +
                                         "<td>Payment Type:</td>" +
                                         "<td>" + payType + "</td>" +
                                     "</tr>" +
                                     "<tr>" +
                                         "<td>Card No.:</td>" +
                                         "<td>" + ds.Tables[0].Rows[0]["ccno"].ToString() + "</td>" +
                                     "</tr>" +
                                     "<tr>" +
                                         "<td>Exp. Date:</td>" +
                                         "<td>" + ds.Tables[0].Rows[0]["expdate"].ToString() + "</td>" +
                                     "</tr>" +
                                     "<tr>" +
                                         "<td>CIN:</td>" +
                                         "<td>" + ds.Tables[0].Rows[0]["cin"].ToString() + "</td>" +
                                     "</tr>" +
                                     "<tr>" +
                                         "<td>Transaction No.:</td>" +
                                         "<td>" + ds.Tables[0].Rows[0]["transNum"].ToString() + "</td>" +
                                     "</tr>" +
                                     "<tr>" +
                                         "<td>Date:</td>" +
                                         "<td>" + ds.Tables[0].Rows[0]["recdate"].ToString() + "</td>" +
                                     "</tr>" +
                                     "<tr>" +
                                         "<td>Amount:</td>" +
                                         "<td>" + string.Format("{0:C}", ds.Tables[0].Rows[0]["amount"]) + "</td>" +
                                     "</tr>" +
                                     "<tr>" +
                                         "<td>Sales Rep:</td>" +
                                         "<td>" + ds.Tables[0].Rows[0]["abbreviation"].ToString() + "</td>" +
                                     "</tr>" +
                                     "<tr>" +
                                         "<td>Status:</td>" +
                                         "<td>" + ds.Tables[0].Rows[0]["response_reason_text"].ToString() + "</td>" +
                                     "</tr>" +
                                     "<tr>" +
                                         "<td>Description:</td>" +
                                         "<td>" + ds.Tables[0].Rows[0]["description"].ToString() + "</td>" +
                                     "</tr>" +
                                 "</table>" +
                              "</html>";
                string toUser = (string)ConfigurationSettings.AppSettings["MCCEmailTo"];
                string ccUser = (string)ConfigurationSettings.AppSettings["MCCEmailCC"];

                lntechNew.Components.MailClass.SendMailToAttorneys("This is a system generated email.\n\r" + str, "Manual Credit Card Processed - [" + ds.Tables[0].Rows[0]["cardholder"].ToString() + "]", "", toUser, ccUser);

            }
        }


        #endregion

        public clsManualCC()
        {

        }

    }
}

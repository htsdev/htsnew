
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;

namespace lntechNew.Components.ClientInfo
{
    public class clscalls
    {
        // Noufil 3498 04/05/2008 new file
        public enum FlagType
        {
            WrongNumber = 33
        };

        public enum CallType
        {
            ReminderCall = 0,
            SetCall = 1,
            FTACall = 2,// Zahoor 3977 08/18/2008: for FTA Activity option
            //Nasir 5690 03/25/2009 for HMCAWDLQCalls option
            HMCAWDLQCalls = 3
        }

        clsENationWebComponents ClsDb = new clsENationWebComponents();
        /// <summary>/// This function return contact information in the Alert Validation report/// </summary>
        /// <param name="ticketid"></param>
        /// <returns> Datatable with or without records</returns>
        public DataTable Getcontactinfo(int ticketid)
        {
            string[] key = { "@ticketid" };
            object[] value = { ticketid };
            DataTable dt = ClsDb.Get_DT_BySPArr("Usp_Htp_Get_Wrongnumber_contactinfo", key, value);
            return dt;
        }

        /// <summary>///This function return report for Alert call validation report/// </summary>
        /// <returns>Datatable with or without records</returns>

        public DataTable getvalidationreport()
        {
            DataTable dt = ClsDb.Get_DT_BySPArr("USP_Htp_Get_WrongNumber_validation_report");
            return dt;
        }

        /// <summary>///This function return report for Set call validation report/// </summary>
        /// <returns>Datatable with or without records</returns>
        public DataTable getsetcallreport()
        {
            DataTable dt = ClsDb.Get_DT_BySPArr("USP_HTP_setCalls_For_ValidationReport");
            return dt;
        }

        /// <summary>
        /// This function return records for reminder call pages
        /// </summary>
        /// <param name="date"></param>
        /// <param name="status"></param>
        /// <param name="check"></param>
        /// <param name="reporttype"></param>
        /// <param name="Language"></param>
        /// <returns>DataSet with or without records</returns>
        ///  Sabir Khan 4272 07/12/08. Mode is added for Bond,Reguler and all clients.
        public DataSet remindercallgrid(DateTime date, int status, Boolean check, int reporttype, int lang, int Mode)
        {
            //Fahad 5503 03/03/2009 If clause added
            if (date != null)
            {
                String[] parameters = { "@reminderdate", "@status", "@showall", "@showreport", "@language", "@Mode" };
                object[] values = { date, status, check, reporttype, lang, Mode };

                // tahir 5507 02/06/2009 separated the set call sp.
                string sProcedureName = string.Empty;
                switch (reporttype)
                {
                    case 0: sProcedureName = "USP_HTS_Get_remainderCalls_Update"; break;
                    case 1: sProcedureName = "USP_HTP_Get_SetCallsActivities"; break;
                        //Yasir Kamal 5623 03/07/2009 sort by Court date
                    case 2: sProcedureName = "USP_HTP_Get_MissedCourtDateCalls"; break;
                        //5623 end
                }
                DataSet dt = ClsDb.Get_DS_BySPArr(sProcedureName, parameters, values);
                // end 5507
                return dt;
            }
            else
            {
                throw new ArgumentNullException();
            }
        }

        //Nasir 5690 03/25/2009 add HMCAWDLQCalls method
        /// <summary>
        /// get  client(s) and quote(s) which have Court status in Arraigment, Arraigment waiting, DLQ.  sort by courtdate 
        /// </summary>
        /// <param name="StartDate"></param>
        /// <param name="EndDate"></param>
        /// <param name="Status"></param>
        /// <param name="ShowAll"></param>
        /// <param name="Reporttype"></param>
        /// <param name="Lang"></param>
        /// <param name="Mode"></param>
        /// <returns>DataSet</returns>
        public DataSet HMCAWDLQCalls(DateTime StartDate, DateTime EndDate, int Status, Boolean ShowAll, int Reporttype, int Lang, int Mode)
        {
            
            if (StartDate != null && EndDate != null)
            {
                String[] parameters = { "@startdate", "@enddate", "@status", "@showall", "@showreport", "@language", "@Mode" };
                object[] values = { StartDate,EndDate, Status, ShowAll, Reporttype, Lang, Mode };
                
                
                DataSet dt = ClsDb.Get_DS_BySPArr("[USP_HTP_Get_HMCAWDLQCalls]", parameters, values);
                
                return dt;
            }
            else
            {
                throw new ArgumentNullException();
            }
        }

        /// <summary>
        /// This function simply return reocrd fron procedure
        /// </summary>
        /// <returns>Datatable with or without Records</returns>
        public DataTable GetReminderStatus()
        {
            DataTable dt = ClsDb.Get_DT_BySPArr("USP_Get_Reminder_Status_Update");
            return dt;
        }

        /// <summary>
        /// Nasir 5256 12/15/2008 get the data of legal Consultaion
        /// This function get the data of legal Consultaion
        /// </summary>
        /// <param name="sdate"></param>
        /// <param name="edate"></param>
        /// <param name="status"></param>
        /// <param name="showall"></param>
        /// <returns></returns>
        public DataTable GetLegalConsultationData(DateTime sdate, DateTime edate, int status, Boolean showall)
        {
            //Fahad 5533 02/25/2009 add If Clause
            if (status >= -1)
            {

                String[] parameters = { "@SDate", "@EDate", "@Status", "@ShowAll" };
                object[] values = { sdate, edate, status, showall };
                DataTable dt = ClsDb.Get_DT_BySPArr("USP_HTP_Get_Legal_Consultation_Request", parameters, values);
                return dt;
            }
            else
            {
                throw new ArgumentException("Argument is not Supported!");
            }
        }

        /// <summary>
        /// Nasir 5256 12/15/2008 update the consultation comment
        /// This function update the consultation comment and status 
        /// </summary>
        /// <param name="TicketID"></param>
        /// <param name="empid"></param>
        /// <param name="ConsultaionComments"></param>
        /// <param name="ConsultationStatusID"></param>
        public void UpdateLegalConsultationComments(int TicketID, int empid, string ConsultaionComments, int ConsultationStatusID)
        {
            //Fahad 5533 02/25/2009 add If Clause
            if (TicketID > 0 && empid > 0 && ConsultaionComments != null && ConsultationStatusID >= -1)
            {
                String[] parameters = { "@TicketID_PK", "@empid", "@ConsultaionComments", "@ConsultationStatusID" };
                object[] values = { TicketID, empid, ConsultaionComments, ConsultationStatusID };
                ClsDb.ExecuteSP("USP_HTP_Update_Consultation_Comments", parameters, values);
            }

            //return dt;
        }


        /// <summary>
        /// This Functiob Simply Returns records from procedure
        /// </summary>
        /// <returns>Data Table With Or WithOut Procedure</returns>
        public DataTable GetReminderStatuses()
        {
            DataTable dt = ClsDb.Get_DT_BySPArr("USP_HTS_GET_ALL_REMINDERCALL_STATUS_Update");
            return dt;
        }

        /// <summary>
        /// This Fucntion Returns a Single Row From The Procedure
        /// </summary>
        /// <param name="ticketid"></param>
        /// <param name="date"></param>
        /// <returns>Datatable with single Row</returns>

        public DataTable GetReminderNotes(int ticketid, DateTime date)
        {
            String[] keys = { "@ticketid", "@courtdate" };
            Object[] values = { ticketid, date };
            DataTable dt = ClsDb.Get_DT_BySPArr("usp_HTS_Get_reminderCall_comments_Update", keys, values);
            return dt;
        }

        /// <summary>
        /// This Fuction Insert And update Comments and status in the Reminder Notes page
        /// </summary>
        /// <param name="Text">Text</param>
        /// <param name="Status">Status</param>
        /// <param name="TicketId">Case Number</param>
        /// <param name="EmpID">Employee Id</param>
        /// <param name="Call">Callback Status</param>
        /// <param name="gComments">General Comments</param>
        /// <param name="TicketViolationIds">Ticket Violation Id's In CSV format</param>
        /// <returns>Return if records insert or not</returns>
        public bool InsertStatus(string Text, int Status, int TicketId, int EmpID, int Call, string gComments, string TicketViolationIds)
        {
            //Changes made by 
            //Usman  3885 05/01/2008 Changes made for null values check
            if (Text == null)
            {
                return false;
            }
            else
            {
                //Zeeshan Ahmed 4837 09/23/2008 Send Ticket Violation Id's In The Procedure
                String[] rem_parameters = { "@comments", "@status", "@ticketid", "@employeeid", "@callType", "@gComments", "@TicketViolationIds" };
                object[] rem_values = { Text, Status, TicketId, EmpID, Call, gComments, TicketViolationIds };
                ClsDb.InsertBySPArr("usp_HTS_update_ReminderCall_status_Update", rem_parameters, rem_values);
                return true;
            }
        }
        public DataTable Sulloremindercallgrid(DateTime date, int status, Boolean check, int lang)
        {
            String[] parameters = { "@reminderdate", "@status", "@showall", "@language" };
            object[] values = { date, status, check, lang };
            DataTable dt = ClsDb.Get_DT_BySPArr("USP_HTP_Sullo_Remindercall", parameters, values);
            return dt;
        }

    }
}

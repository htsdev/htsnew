﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.util;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;

namespace lntechNew.Components.ClientInfo
{
    // Rab Nawaz Khan 10431 10/02/2013 Added the Non Client control Manager Class for LMS address Chceck Report.
    /// <summary>
    /// ClsNonClientsManager
    /// </summary>
    public class ClsNonClientsManager
    {
        clsENationWebComponents clsdb;

        /// <summary>
        /// empty Contructor
        /// </summary>
        public ClsNonClientsManager ()
        {
            clsdb = new clsENationWebComponents();
        }

        /// <summary>
        /// This method is used to fill the Letter Types in Dropdown on Report.
        /// </summary>
        /// <returns>DataTable</returns>
        public DataTable FillLetterTypes()
        {
            try
            {
               return clsdb.Get_DT_BySPArr("USP_HTP_GetBadMailLetterTypes");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// This Method is Used to get all the records from database on letter type basis which are marked as bad mailer record.
        /// </summary>
        /// <param name="letterType">string LetterType</param>
        /// <param name="badMarkedRecordDateFrom">DateTime badMarkedRecordDateFrom</param>
        /// <param name="badMarkedRecordDateTo">DateTime badMarkedRecordDateTo</param>
        /// <param name="isShowAll">bool isShowAll</param>
        /// <param name="isValidationReport">bool isValidationReport</param>
        /// <returns>DataTable</returns>
        /// <exception cref="Exception"></exception>
        public DataTable GetBadMarkedRecords(string letterType, DateTime badMarkedRecordDateFrom, DateTime badMarkedRecordDateTo, bool isShowAll, bool isValidationReport)
        {
            try
            {
                string[] parameters = { "@letterType", "@badMarkedDateFrom", "@badMarkedDateTo", "@showAll", "@isValidationReport" };
                object[] values = { letterType, badMarkedRecordDateFrom, badMarkedRecordDateTo, isShowAll, isValidationReport };
                return clsdb.Get_DT_BySPArr("USP_HTP_GetAllBadMarkedRecords", parameters, values);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// This method is used to Get all the States from DB
        /// </summary>
        /// <returns>DataTable</returns>
        /// <exception cref="Exception"></exception>
        public DataTable GetStates()
        {
            try
            {
                return clsdb.Get_DT_BySPArr("USP_HTS_Get_State");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// This method is used to update the new address for the clients which are marked as bad mailer
        /// </summary>
        /// <param name="recordId">int recordId</param>
        /// <param name="firstname">string firstname</param>
        /// <param name="lastname">string lastname</param>
        /// <param name="newaddress1">string newaddress1</param>
        /// <param name="city1">string city1</param>
        /// <param name="stateId1">int stateId1</param>
        /// <param name="zipcode1">string zipcode1</param>
        /// <param name="dp2_1">string dp2_1</param>
        /// <param name="dpc_1">string dpc_1</param>
        /// <param name="zp4AddressStatus1">string zp4AddressStatus1</param>
        /// <param name="newaddress2">string newaddress2</param>
        /// <param name="city2">string city2</param>
        /// <param name="stateId2">int stateId2</param>
        /// <param name="zipcode2">string zipcode2</param>
        /// <param name="dp2_2">string dp2_2</param>
        /// <param name="dpc_2">string dpc_2</param>
        /// <param name="zp4AddressStatus2">string zp4AddressStatus2</param>
        /// <param name="insertedOrUpdatedBy">int insertedOrUpdatedBy</param>
        /// <param name="isNewAddress2Checked">bool isNewAddress2Checked</param>
        public void UpdateNewAddress(int recordId, string firstname, string lastname, string newaddress1, string city1, int stateId1, string zipcode1,
            string dp2_1, string dpc_1, string zp4AddressStatus1, string newaddress2, string city2, int stateId2, string zipcode2, 
            string dp2_2, string dpc_2, string zp4AddressStatus2, int insertedOrUpdatedBy, bool isNewAddress2Checked)
        {
            try
            {

                string[] keys =  {"@RecordId", "@firstName", "@lastName", "@NewAddress1", "@city1", "@state1", "@zipcode1", "@DP2_1", "@DPC_1", "@zp4AddressStatus1",
                                     "@NewAddress2", "@city2", "@state2", "@zipcode2", "@DP2_2", "@DPC_2", "@zp4AddressStatus2", "@InsertedOrUpdatedBy", "@IsNewAddress2Checked"};
                object[] values =
                {
                   recordId, firstname, lastname, newaddress1, city1, stateId1, zipcode1, dp2_1, dpc_1, zp4AddressStatus1, newaddress2, city2, stateId2, zipcode2, 
                   dp2_2, dpc_2, zp4AddressStatus2, insertedOrUpdatedBy, isNewAddress2Checked
                };
                clsdb.ExecuteSP("[dbo].[USP_HTP_InsertUpdateMailerAddresses]", keys, values);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// This method is used to Update the record as No New address
        /// </summary>
        /// <param name="recordid">int RecordId</param>
        /// <param name="lastName">string lastName</param>
        /// <param name="firstName">string firstName</param>
        /// <param name="empId">int EmpId</param>
        public void UpdateNoNewAddress(int recordid, string lastName, string firstName, int empId)
        {
            try
            {
                string[] keys = { "@recordId", "@lastName", "@firstName", "@empId" };
                object[] values = { recordid, lastName, firstName, empId };
                clsdb.ExecuteSP("[dbo].[USP_HTP_UpdateNoNewAddress]", keys, values);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

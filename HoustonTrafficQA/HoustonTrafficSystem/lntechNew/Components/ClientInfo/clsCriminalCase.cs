using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using WebSupergoo.ABCpdf6;
using WebSupergoo.ABCpdf6.Objects;
using WebSupergoo.ABCpdf6.Atoms;
using FrameWorkEnation.Components;


// This Class Contains All The Bussiness Logic Related To Criminal Case
namespace lntechNew.Components.ClientInfo
{
    public class clsCriminalCase
    {

        #region Variables

        int ticketid;
        int empid;
        string sessionid;
        string spnuser;
        string spnpassword;
        string errormessage = "";
        clsENationWebComponents ClsDB = new clsENationWebComponents();
        clsGeneralMethods GeneralMehod = new clsGeneralMethods();
        clsLogger bugTracker = new clsLogger();
        int seq; //for pdf generation


        #endregion

        #region Properties

        public int TicketID
        {
            get
            {
                return ticketid;
            }
            set
            {
                ticketid = value;
            }
        }

        public int EmpID
        {
            get
            {
                return empid;
            }
            set
            {
                empid = value;
            }
        }

        public string SessionID
        {
            get
            {
                return sessionid;
            }
            set
            {
                sessionid = value;
            }
        }

        #endregion

        #region Mehods

        // Save Criminal PDF File 
        public bool SaveCriminalCasePDF()
        {
            try
            {
                //int seq = 0;
                string StrFileName = "CrimianlCase.pdf";
                string StrFileName1 = sessionid + empid.ToString() + StrFileName;
                // Set SPN User Name and Password For Searching
                spnuser = Convert.ToString(ConfigurationSettings.AppSettings["SPNUser"]);
                spnpassword = Convert.ToString(ConfigurationSettings.AppSettings["SPNPassword"]);

                // Set File Path Variables
                string vNTPATHScanTemp = Convert.ToString(ConfigurationSettings.AppSettings["NTPATHScanTemp"]);
                string vNTPATHScanImage = Convert.ToString(ConfigurationSettings.AppSettings["NTPATHScanImage"]);
                errormessage = "There is a login issue on the Harris county court website therefore PDF for the case is not generated. Please manually upload the case information from the Harris county court website.";
                vNTPATHScanTemp = vNTPATHScanTemp.Replace("\\\\", "\\");
                vNTPATHScanImage = vNTPATHScanImage.Replace("\\\\", "\\");
                string sourcepath = vNTPATHScanTemp + StrFileName1;
                /////added by khalid 8-11-07
                DataSet ds = ClsDB.Get_DS_BySPByOneParmameter("usp_HTS_GetCaseInfoDetail", "@TicketID", TicketID);
                string response = " ";

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    DataRow dr = ds.Tables[0].Rows[i];
                    response = getCriminalFileHTML(Convert.ToString(dr["RefCaseNumber"]), Convert.ToString(dr["CDI"]));
                    //GeneratePDF(response,seq);
                    GeneratePDF(response, i);
                }

                //////////////////////end khalid/////////////
                #region prelogic
                //string response = getCriminalFileHTML();

                //if (response == "")
                //    return false;
                //else if (response.Contains("The System is Temporarily Unavailable - Please Try Later"))
                //    return false;
                //else if (response.Contains("Logon or Password is invalid - Please Try Again"))
                //    return false;
                //else if (response.Contains("No records found"))
                //    return false;
                //else if (response.Contains("Please Enter a Logon ID"))
                //    return false;
                //else if (response.Contains("SPN Not Found"))
                //    return false;
                //else if (response == "error")
                //    return false;
                ////Generate PDF File
                //comment by khalid// GeneratePDF(sourcepath, response);
                #endregion
                //added 8-11-07


                Doc theDoc1 = (Doc)HttpContext.Current.Session["DocToSave"];
                //Added by khalid 11-12-07 for fixing bug 2201
                if (theDoc1 != null)
                {
                    //GeneratePDF(response, 0);
                    theDoc1.Save(sourcepath);
                    theDoc1.Clear();
                    HttpContext.Current.Session["DocToSave"] = null;
                    /////////end khalid///////
                    //Perform database Uploading Task
                    int BookId = 0, picID = 0;
                    string picName, picDestination;

                    // Save Document Information 
                    string[] key = { "@updated", "@extension", "@Description", "@DocType", "@SubDocTypeID", "@Employee", "@TicketID", "@Count", "@Book", "@Events", "@BookID" };
                    object[] value1 = { DateTime.Now, "PDF", "(Auto Generated File) Criminal Case File", "Other", 0, empid, ticketid, 1, 0, "Auto Generated", "" };
                    picName = ClsDB.InsertBySPArrRet("usp_hts_NewAddScan", key, value1).ToString();

                    //Getting File Name            
                    string BookI = picName.Split('-')[0];
                    string picI = picName.Split('-')[1];
                    BookId = (int)Convert.ChangeType(BookI, typeof(int)); ; //
                    picID = (int)Convert.ChangeType(picI, typeof(int)); ; //

                    //Move file To Desctination File Path
                    picDestination = vNTPATHScanImage + picName + ".pdf";
                    System.IO.File.Copy(sourcepath, picDestination);
                    System.IO.File.Delete(sourcepath);
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return false;
            }

        }

        // Generate PDF 
        private void GeneratePDF(string path, string response)
        {
            System.IO.FileStream wFile;
            byte[] byteData = null;
            byteData = System.Text.Encoding.ASCII.GetBytes(response);

            string localfilepath = "C://Temp//" + DateTime.Today.ToFileTime() + ".html";
            wFile = new FileStream(localfilepath, FileMode.Create);
            wFile.Write(byteData, 0, byteData.Length);
            wFile.Close();
            File.Copy(localfilepath, path);

            //Doc theDoc = new Doc();
            //theDoc.Rect.Inset(0, 0);

            //theDoc.Page = theDoc.AddPage();
            ////theDoc.HtmlOptions.BrowserWidth = 800;
            //int theID;
            //theID = theDoc.AddImageHtml(response);
            //while (true)
            //{
            //    theDoc.FrameRect(); // add a black border
            //    if (!theDoc.Chainable(theID))
            //        break;
            //    theDoc.Page = theDoc.AddPage();
            //    theID = theDoc.AddImageToChain(theID);
            //}
            //for (int i = 1; i <= theDoc.PageCount; i++)
            //{
            //    theDoc.PageNumber = i;
            //    theDoc.Flatten();
            //}

            //theDoc.Save(path);
            //theDoc.Clear();

        }

        // Get Response From The Website
        private string getCriminalFileHTML(string refcase, string cdi)
        {//changed by khalid on 7-11-07
            string output = "";
            try
            {
                if (cdi != "0" && cdi != "" && cdi != "''")
                {
                    output += GeneralMehod.GetCaseNoSearchPage(spnuser, spnpassword, refcase, cdi);
                }
                else
                {
                    errormessage = "PDF for the case is not generated because CDI number of the case doesn't exist.";
                    return "CDI not Found";

                }
                #region prelogic
                //original//DataSet ds = ClsDB.Get_DS_BySPByTwoParmameter("USp_HTS_Get_Information_For_SPNSearch", "@TicketId", ticketid, "@EmpID", empid);
                //khalid
                //transfer to save//   DataSet ds=ClsDB.Get_DS_BySPByOneParmameter("usp_HTS_GetCaseInfoDetail","@TicketID",TicketID);

                // Uncomment This Line If User Want TO Access JIMS Website with its own credentials
                //return GeneralMehod.GetSPNSearchPage(Convert.ToString(ds.Tables[0].Rows[0]["SPNUser"]), Convert.ToString(ds.Tables[0].Rows[0]["SPNPassword"]), Convert.ToString(ds.Tables[0].Rows[0]["SPN"]));

                //This Code Use Credentials from Webconfig file to access JIMS website 
                //  string CaseSPN = Convert.ToString(ds.Tables[0].Rows[0]["SPN"]);



                //Actual before 7-11-07 //  return GeneralMehod.GetSPNSearchPage(spnuser, spnpassword, Convert.ToString(ds.Tables[0].Rows[0]["SPN"]));

                //7-11-07 by khalid//GetCaseNoSearchPage to get individual cases
                //////////////
                //string output=" ";
                //                 foreach (DataRow dr in ds.Tables[0].Rows)
                //        {
                //            if (dr["CDI"] != "0" && dr["CDI"] != "" && dr["CDI"] != "''")
                ////            {
                //                output += GeneralMehod.GetCaseNoSearchPage(spnuser, spnpassword, Convert.ToString(dr["RefCaseNumber"]), Convert.ToString(dr["CDI"]));
                //                output += "\n New vioation\n";

                //            }
                //            else
                //            {
                //                errormessage = "PDF for the case is not generated because SPN number of the case doesn't exist.";
                //                return "CDI not Found";

                //            }


                //}
                #endregion
                return output;


            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return "";
            }
        }

        public string PDFNotCreatedAlert()
        {
            return "<script> alert(\"" + errormessage + "\") </script>";
        }

        public string PDFCreatedAlert()
        {
            return "<script> alert(\"The case information has been saved from the jims website under document page.\") </script>";
        }

        /////////method to append pages in PDF\\\\\\\
        //added by khalid
        private int GeneratePDF(string response, int seq)
        {
            try
            {
                //  response+= "\n New vioation\n";
                if (response == "")
                    return 0;
                else if (response.Contains("The System is Temporarily Unavailable - Please Try Later"))
                    return 0;
                else if (response.Contains("Logon or Password is invalid - Please Try Again"))
                    return 0;
                else if (response.Contains("No records found"))
                    return 0;
                else if (response.Contains("Please Enter a Logon ID"))
                    return 0;
                else if (response.Contains("SPN Not Found"))
                    return 0;
                else if (response.Contains("CDI not Found")) //added
                    return 0;
                else if (response.Contains("NO RECORDS FOUND FOR THIS CASE"))//added
                    return 0;
                else if (response.Contains("Your password has expired"))//added
                    return 0;
                else if (response == "error")
                    return 0;

                if (seq == 0)
                {
                    Doc theDoc1 = new Doc();
                    theDoc1.Rect.Inset(0, 0);
                    theDoc1.Page = theDoc1.AddPage();
                    theDoc1.Rect.Bottom = 15.0;
                    theDoc1.Rect.Top = 785.0;
                    //theDoc1.HtmlOptions.BrowserWidth = 800;
                    int theID;
                    // theID = theDoc1.AddImageUrl(path);
                    theID = theDoc1.AddImageHtml(response);

                    while (true)
                    {
                        theDoc1.FrameRect(); // add a black border
                        if (!theDoc1.Chainable(theID))
                            break;
                        theDoc1.Page = theDoc1.AddPage();
                        theID = theDoc1.AddImageToChain(theID);

                    }
                    for (int i = 1; i <= theDoc1.PageCount; i++)
                    {
                        theDoc1.PageNumber = i;
                        theDoc1.Flatten();
                    }

                    HttpContext.Current.Session["DocToSave"] = theDoc1;
                    return (seq++);
                }
                if (seq > 0)
                {
                    Doc theDoc = new Doc();
                    theDoc.Rect.Inset(0, 0);
                    theDoc.Page = theDoc.AddPage();
                    //theDoc.HtmlOptions.BrowserWidth = 800;
                    int theID;
                    // theID = theDoc.AddImageUrl(path);
                    theID = theDoc.AddImageHtml(response);
                    while (true)
                    {
                        theDoc.FrameRect(); // add a black border
                        if (!theDoc.Chainable(theID))
                            break;
                        theDoc.Page = theDoc.AddPage();
                        theID = theDoc.AddImageToChain(theID);
                    }
                    for (int i = 1; i <= theDoc.PageCount; i++)
                    {
                        theDoc.PageNumber = i;
                        theDoc.Flatten();
                    }
                    Doc theDoc1 = (Doc)HttpContext.Current.Session["DocToSave"];


                    //HttpContext.Current.Session[]

                    theDoc1.Append(theDoc);
                    HttpContext.Current.Session["DocToSave"] = theDoc1;
                    theDoc.Clear();
                    return (seq++);
                }
                return seq;
            }
            catch (Exception ex)
            {
                return seq;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        //waqas 5864 07/03/2009 saving email in docstorage in msg format.
        /// <summary>
        /// This method is used to save case email summary in pdf format for the attorneys
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="response"></param>
        /// <param name="attorneyName"></param>
        /// <returns>bool</returns>
        public bool SaveCaseEmailSummaryPDF(string fileName, string response, string attorneyName)
        {
            try
            {
                // Set File Path Variables
                string vNTPATHScanTemp = Convert.ToString(ConfigurationSettings.AppSettings["NTPATHScanTemp"]);
                string vNTPATHScanImage = Convert.ToString(ConfigurationSettings.AppSettings["NTPATHScanImage"]);
                vNTPATHScanTemp = vNTPATHScanTemp.Replace("\\\\", "\\");
                vNTPATHScanImage = vNTPATHScanImage.Replace("\\\\", "\\");
                string sourcepath = vNTPATHScanTemp + fileName;

                GeneratePDF(sourcepath, response);

                int BookId = 0, picID = 0;
                string picName, picDestination;

                //Fahad 10227 04/24/2012 Chnaged File extension from Pdf to Html
                string[] key = { "@updated", "@extension", "@Description", "@DocType", "@SubDocTypeID", "@Employee", "@TicketID", "@Count", "@Book", "@Events", "@BookID" };
                object[] value1 = { DateTime.Now, "HTML", "Case summary email sent to attorney - " + attorneyName, "Other", 0, empid, ticketid, 1, 0, "Email", "" };
                picName = ClsDB.InsertBySPArrRet("usp_hts_NewAddScan", key, value1).ToString();

                //Getting File Name            
                string BookI = picName.Split('-')[0];
                string picI = picName.Split('-')[1];

                BookId = (int)Convert.ChangeType(BookI, typeof(int)); ; //
                picID = (int)Convert.ChangeType(picI, typeof(int)); ; //

                //Move file To Desctination File Path

                picDestination = vNTPATHScanImage + picName + ".html";
                System.IO.File.Copy(sourcepath, picDestination);
                System.IO.File.Delete(sourcepath);
                return true;

            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return false;
            }

        }


        #endregion


    }
}

﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using FrameWorkEnation.Components;
using LNHelper;

namespace lntechNew.Components.ClientInfo
{
    public class CaseType
    {

        // Agha Usman 4271 07/02/2008

        #region Variables

        int caseTypeId = -1;
        string caseTypeName;
        int serviceTicketEmpId;
        string serviceTicketEmailAlert;
        string ServiceTicketmailClose;
        string ServiceTicketAngrymailOpen;
        string ServiceTicketAngrymailClose;
        // Zahoor 4757 09/09/2008 For Service Ticket Update Email
        private string _ServiceTicketUpdateEmail;

        

        public const int Traffic = 1;
        public const int Criminal = 2;
        public const int Civil = 3;
        public const int FamilyLaw = 4; // Noufil 4782 09/23/2008 Family Lay Const added


        clsENationWebComponents ClsDb = new clsENationWebComponents("Connection String");

        #endregion

        #region Properties

        public int CaseTypeId
        {
            get { return caseTypeId; }
            set { caseTypeId = value; }
        }
        public string CaseTypeName
        {
            get { return caseTypeName; }
            set { caseTypeName = value; }
        }
        public int ServiceTicketEmpId
        {
            get { return serviceTicketEmpId; }
            set { serviceTicketEmpId = value; }
        }
        public string ServiceTicketEmailAlert
        {
            get { return serviceTicketEmailAlert; }
            set { serviceTicketEmailAlert = value; }
        }
        public string ServiceTicketAngryEmailClose
        {
            get { return ServiceTicketAngrymailClose; }
            set { ServiceTicketAngrymailClose = value; }
        }
        public string ServiceTicketAngryEmailOpen
        {
            get { return ServiceTicketAngrymailOpen; }
            set { ServiceTicketAngrymailOpen = value; }
        }
        public string ServiceTicketEmailClose
        {
            get { return ServiceTicketmailClose; }
            set { ServiceTicketmailClose = value; }
        }
        // Zahoor 4757 09/09/2008 For Service Ticket Update Email.
        public string ServiceTicketUpdateEmail
        {
            get { return _ServiceTicketUpdateEmail; }
            set { _ServiceTicketUpdateEmail = value; }
        }
        // End 4757
        #endregion

        #region Functions

        public DataTable GetCaseTypeInfo()
        {
            string[] key = { "@CaseTypeId" };
            object[] value1 = { caseTypeId };
            //TODO: Need to set properties of the case type and used it.
            DataTable dt = ClsDb.Get_DT_BySPArr("USP_HTP_GET_CASETYPE_INFO", key, value1);
            return dt;
        }

        /// <summary>
        ///   
        /// </summary>


        /// <summary>
        /// This function update case type information
        /// </summary>
        /// <returns>Return true if case information updated.</returns>
        public bool UpdateCaseType()
        {
            //Noufil 4338 07/10/2008 This function accept the Case type
            //Zahoor 4757 09/09/2008 Add @ServiceTicketUpdateEmail parameter
            string[] key = { "@casetypeid", "@ServiceTicketEmpId", "@ServiceTicketEmailAlert", "@ServiceTicketEmailClose", "@ServiceTicketAngryEmailOpen", "@ServiceTicketAngryEmailClose", "@CaseTypeName", "@ServiceTicketUpdateEmail" };
            object[] value1 = { caseTypeId, serviceTicketEmpId, serviceTicketEmailAlert, ServiceTicketmailClose, ServiceTicketAngrymailOpen, ServiceTicketAngrymailClose, caseTypeName, ServiceTicketUpdateEmail };
            return Convert.ToBoolean(Convert.ToInt32(ClsDb.ExecuteScalerBySp("USP_HTP_UPDATECASETYPE", key, value1)));
        }

        /// <summary>
        ///     Noufil 4632 09/17/2008 
        ///     This Function returns All information about Case Type
        /// </summary>
        /// <returns>DataTable</returns>
        public DataTable GetAllCaseType()
        {
            return ClsDb.Get_DT_BySPArr("USP_HTP_GET_ALL_CASETYPE");
        }

        #endregion
    }
}

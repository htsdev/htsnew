using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.WebControls;

namespace lntechNew.Components.ClientInfo
{
    public class ServiceTicketComponent
    {
        clsENationWebComponents clsdb = new clsENationWebComponents();
        public DataTable GetCategories()
        {
            DataTable dt = new DataTable();
            try
            {

                dt = clsdb.Get_DT_BySPArr("USP_HTS_GET_SERVICETICKET_CATEGORIES");
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
            }


        }


        public void updateServiceTicket(int fid, int ServiceTicketCategory, int Priority)
        {
            try
            {
                string[] keys = { "@fid", "@priority", "@ServiceTicketCategory " };
                object[] values = { fid, Priority, ServiceTicketCategory };
                clsdb.ExecuteSP("USP_UPDATE_GENERAL_INFO_FLAG", keys, values);
            }
            catch (Exception ex)
            {
            }

        }

        /// <summary>
        /// This method returns the violations for specific ticket id
        /// khalid 2988 2/12/08 to format void payment email
        /// </summary>
        /// <returns> DataTable</returns>
        public DataTable GetViolationInfoForEmail(int ticketID)
        {
            string[] key = { "@Ticketid" };
            object[] value = { ticketID };
            DataTable violations = clsdb.Get_DT_BySPArr("usp_hts_get_ServiceTicketEmail", key, value);
            return violations;
        }

        /// <summary>
        ///     Noufil  4632 09/18/2008
        ///     This Function get all Service Categories with their associated case type in comma seperated value.        ///      
        /// </summary>
        /// <returns>DataTable</returns>
        public DataTable GetServiceTicketsWithCasetype()
        {
            return clsdb.Get_DT_BySPArr("USP_HTP_Get_ServiceCategories_With_CaseType");
        }

        /// <summary>
        ///      Noufil  4632 09/18/2008
        ///      This function Delete the service ticket category
        /// </summary>
        /// <param name="categoryid">ID of service ticket category</param>
        public void DeleteServiceTicketCategory(string categoryid)
        {
            string[] keys = { "@CategoryID" };
            object[] values = { categoryid };
            clsdb.ExecuteSP("USP_HTS_DELETE_SERVICETICKET_CATEGORY", keys, values);
        }

        /// <summary>
        ///      Noufil  4632 09/18/2008
        ///      This function Checks the service ticket category
        /// </summary>
        /// <param name="categoryid">ID of service ticket category</param>
        /// <returns>DataTable</returns>
        public DataTable CheckServiceticket(string categoryid)
        {
            string[] keys = { "@CategoryID" };
            object[] values = { categoryid };
            return clsdb.Get_DT_BySPArr("USP_HTS_CHECK_SERVICETICKET_CATEGORY", keys, values);
        }

        /// <summary>
        ///     Noufil  4632 09/18/2008
        ///     This function adds new service ticket category
        /// </summary>
        /// <param name="newcategory">New category name</param>
        /// <param name="value">case type value</param>
        public void AddServiceCategory(string newcategory, int value, int AssignTo)//Fahad 6054 07/22/2009 Add new Parameter "AssignTo"
        {
            string[] keys = { "@Description", "@casetypeid", "@AssignTo" };
            object[] values = { newcategory, value, AssignTo };
            clsdb.ExecuteSP("USP_HTS_ADD_SERVICETICKET_CATEGORY", keys, values);
        }

        /// <summary>
        ///     Noufil  4632 09/18/2008
        ///     This function get case type for the appropiate service ticket category
        /// </summary>
        /// <param name="categoryid">ID of service ticket category</param>
        /// <returns>DataTable</returns>
        public DataTable GetCaseTypeByCategory(string categoryid)
        {
            string[] keys = { "@servicecategoryid" };
            object[] values = { categoryid };
            return clsdb.Get_DT_BySPArr("USP_HTP_Get_CaseType_by_sevicecategory", keys, values);
        }

        /// <summary>
        ///     Noufil  4632 09/18/2008
        ///     Noufil 4782 Family law case type updated
        ///     This function updates the service ticket category information
        /// </summary>
        /// <param name="categoryid">ID of service ticket category</param>
        /// <param name="description">New Name of Category</param>
        /// <param name="casetypetraffic">Case type id for Traffic</param>
        /// <param name="casetypecriminal">Case type id for criminal</param>
        /// <param name="casetypecivil">Case type id for Civil</param>
        public void UpdateServiceTicketCategories(string categoryid, string description, int casetypetraffic, int casetypecriminal, int casetypecivil, int casetypefamily, int AssignTo)//Fahad 6054 07/22/2009 New Parameter "AssignTo" added
        {
            string[] keys = { "@CategoryID", "@Description", "@casetypetraffic", "@iscasetypecriminal", "@iscasetypecivil", "@iscasetypeFamily", "@AssignTo" };//Fahad 6054 07/22/2009 New Parameter "@AssignTo" added
            object[] values = { categoryid, description, casetypetraffic, casetypecriminal, casetypecivil, casetypefamily, AssignTo };
            clsdb.ExecuteSP("USP_HTS_UPDATE_SERVICETICKET_CATEGORY", keys, values);
        }
        // sadaf  Aziz 10233 12/05/2012 ....
        public Boolean IsServiceTicketCategoryExist(string description)
        {
            string[] keys = { "@Description" };
            object[] values = { description };
            return Convert.ToBoolean(clsdb.ExecuteScalerBySp("[USP_HTP_CheckServiceTicketCategoryByName]", keys, values));
        }

    }
}

﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.IO;
using FrameWorkEnation.Components;


namespace HTP.Components.ClientInfo
{
    public class CourtsFiles
    {
        
        //Kazim 3697 5/6/2008 This class contains all the method regarding Court Files. User can Upload, Delete and View Files 
        //by using these methods.
        
        #region variables

        private int courtId;
        private int rep;

        clsENationWebComponents clsDb = new clsENationWebComponents();
        string filePath = ConfigurationManager.AppSettings["CourtFilespath"].ToString();

        #endregion
        
        #region Properties

        public int CourtId
        {
            get
            {
                return courtId;
            }
            set
            {
                courtId = value;
            }
        }

         public int Rep
        {
            get
            {
                return rep;
            }
            set
            {
                rep = value;
            }
        }
        
        #endregion

        #region methods

        /// <summary>
        /// This method is used to Upload Court Files on network path.
        /// </summary>
        /// <param name="fileupload"></param>
        /// <returns>true if file has been Uploaded else returns false</returns>
        
        public bool UploadFile(FileUpload fileupload)
        {
            string ext=Path.GetExtension(fileupload.PostedFile.FileName);
            if (ext == ".pdf" || ext == ".doc" || ext==".docx")
            {

                filePath = filePath + ( courtId + " "  + fileupload.FileName);

                fileupload.SaveAs(filePath);

                string[] keys = { "@CourtId", "@DocumentName", "@UploadedDate","@Rep","@DocumentPath"};
                object[] values = { courtId, fileupload.FileName, DateTime.Now, rep,filePath  };
                clsDb.ExecuteSP("USP_HTP_Insert_CourtFileUpload", keys, values);
                return true;
            }
            else
            {
                return false;
            }
            
        }

        /// <summary>
        /// This method is used to delete a file on the basis of its id.
        /// </summary>
        /// <param name="id"></param>
        ///<example>
        ///int id=7;
        ///CourtsFiles files = new CourtsFiles();
        ///files.DeleteFile(id)
        ///</example> 
               
        public int DeleteFile(int id)
        {
            string path=ViewFilepath(id);
            if(File .Exists (path))
            {
                File.Delete (path);
            }
            string[] key={"@id"};
            object[] value={id };
            return Convert.ToInt32(clsDb.ExecuteScalerBySp("USP_HTP_Delete_CourtFileUpload",key,value));
        }
                
        /// <summary>
        /// This method is used to delete all the files of particular court.
        /// </summary>
        ///<example>
        ///CourtsFiles files = new CourtsFiles();
        ///files.courtId=9;
        ///files.DeleteAllCourtFiles()
        ///</example>
        
        public int DeleteAllCourtFiles()
        {
            string[] key = { "@CourtId" };
            object[] value = { courtId };
            return Convert.ToInt16(clsDb.ExecuteScalerBySp("USP_HTP_Delete_AllCourtFiles", key, value));
        }

        /// <summary>
        /// This method is used to get all the uploaded files of a court.
        /// </summary>
        /// <returns >DataTable contains all the uploaded files details of a court</returns>
        ///<example>
        ///CourtsFiles files = new CourtsFiles();
        ///files.courtId=9;
        ///files.GetUploadedFiles()
        ///</example>
        
        public DataTable GetUploadedFiles()
        {
            string[] key = { "@CourtId" };
            object[] value = { courtId  };
            return clsDb.Get_DT_BySPArr("USP_HTP_Get_CourtUploadedFiles", key, value);
        }

        /// <summary>
        /// This method is used to get all the path of an uploaded file.
        /// </summary>
        /// <returns >string contains the path of relevent file</returns>
        ///<example>
        ///CourtsFiles files = new CourtsFiles();
        ///id=9;
        ///files.ViewFilepath(id)
        ///</example>
        
        public string ViewFilepath(int id)
        {
            string[] key = { "@id" };
            object[] value = { id };
            return (string)clsDb.ExecuteScalerBySp("USP_HTP_Get_FilePathById", key, value);
        }
        
        #endregion
    }
}

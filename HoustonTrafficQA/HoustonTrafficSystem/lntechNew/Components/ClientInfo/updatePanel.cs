using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;
using HTP.Components;
using System.Threading;
using System.Collections;
using Configuration.Client;
using Configuration.Helper;


namespace lntechNew.Components.ClientInfo
{
    public class updatePanel
    {

        private string causeno;
        private string ticketnumber;
        private int violationid;
        private double fineamount;
        private double bondamount;
        private string bondflag;
        private DateTime courtdate;
        private string courttime;
        private string courtno; //Sabir Khan 7979 07/02/2010 type has been changed from int into string
        private int status;
        private int courtlocation;
        private int priceplan;
        private bool newviolation;
        private bool allowolddate;
        private string error = "";
        private bool updateall;
        public DataGrid dgViolationInfo;
        private string gridfield;
        private int empid;
        private string CourtDetail;
        private bool arrignmentwaitingdate;
        private bool bondwaitingdate;
        //Added By Zeeshan Ahmed On 26th September 2007
        private int chargelevel;
        private string cdi;
        // Business Logic Components
        private bool _alrProcess;
        private int _chkAlrProcess;
        public clsCase ClsCase;
        public clsCaseDetail ClsCaseDetail;
        public clsLogger bugTracker;
        public clsENationWebComponents clsdb;
        //Sabir Khan 8058 07/26/2010 Initalize IConfiguratoinClient object
        IConfigurationClient Client = ConfigurationClientFactory.GetConfigurationClient();   
        public int ticketviolationid;
        //Zeeshan Ahmed 3724 05/13/2008 Add arrest date property
        private DateTime arrestDate;

        //Zeeshan Ahmed 4163 06/03/2008 Add MissedCourtType Property
        private int missedCourtType = -1;

        public int MissedCourtType
        {
            get { return missedCourtType; }
            set { missedCourtType = value; }
        }

        /// Public Properties

        public DateTime ArrestDate
        {
            get { return arrestDate; }
            set { arrestDate = value; }
        }

        public bool AlrProcess
        {
            get { return _alrProcess; }
            set { _alrProcess = value; }
        }
        public int chkAlrProcess
        {
            get { return _chkAlrProcess; }
            set { _chkAlrProcess = value; }
        }

        public string CDI
        { set { cdi = value; } }

        public string Error
        { get { return error; } }

        public int EmpID
        { set { empid = value; } }

        public int TicketViolationID
        { set { ticketviolationid = value; } }

        public string CauseNo
        { set { causeno = value; } }

        public string TicketNumber
        { set { ticketnumber = value; } }

        public string BondFlag
        { set { bondflag = value; } }

        public int ViolationID
        { set { violationid = value; } }

        public double FineAmount
        { set { fineamount = value; } }

        public double BondAmount
        { set { bondamount = value; } }
        //Sabir Khan 7979 07/02/2010 type has been changed from int into string
        public string CourtNo
        { set { courtno = value; } }

        public int Status
        { set { status = value; } }

        public int CourtLocation
        { set { courtlocation = value; } }

        public int PricePlan
        { set { priceplan = value; } }

        public DateTime CourtDate
        { set { courtdate = value; } }

        public string CourtTime
        { set { courttime = value; } }

        public bool IsNewViolation
        { set { newviolation = value; } }

        public bool AllowOldDate
        { set { allowolddate = value; } }

        public bool UpdateAll
        { set { updateall = value; } }

        public string GridField
        { set { gridfield = value; } }

        public string OscareCourtDetail
        { set { CourtDetail = value; } }

        public bool ArrignmentWaitingDate
        { set { arrignmentwaitingdate = value; } }

        public bool BondWaitingDate
        { set { bondwaitingdate = value; } }

        public int ChargeLevel
        { set { chargelevel = value; } }



        public void UpdateViolationDetail()
        {
            //in case of new violation
            if (newviolation)
            {
                if (checkCauseNo(causeno, courtlocation, 0))
                {
                    SetValuesForInsertion(newviolation);
                    int NewTicketsViolationID = ClsCaseDetail.InsertCaseDetail();

                    if (NewTicketsViolationID > 0)
                    {

                        ////Aziz 1974 06/20/08 Adding new violation to sharepoint 
                        ////Sabir Khan 6248 07/31/2009 Add event only for App, Arr, Pre, Jury and Judge statuses... 
                        //DataSet ds = ClsCaseDetail.GetCaseTypeCoveringFirm(-1, NewTicketsViolationID);
                        ////Sabir Khan 6248 07/31/2009 Get category and courtdate...
                        //int iCategory = int.Parse(ds.Tables[0].Rows[0]["Category"].ToString());
                        //if (iCategory == 2 || iCategory == 3 || iCategory == 4 || iCategory == 5)
                        //{
                        //    //SharePointWebWrapper.AddNewViolationToCalendar(NewTicketsViolationID);
                        //    //Sabir Khan 6248 07/31/2009 Update all events of client on share point portal...
                        //    //SharePointWebWrapper.AddNewViolationToCalendar(NewTicketsViolationID);
                        //    //Sabir Khan 6423 08/21/2009 Call sharepoint method in separate thread...
                        //    Thread NewThread = new Thread(() => ManageSharepointViolation(-1, NewTicketsViolationID));
                        //    NewThread.Start(); 
                        //}
                        //if (this.updateall)
                        //{
                        //    //Sabir Khan 6248 07/31/2009 Update all events of client on share point portal...
                        //    //ManageSharepointViolationEvents(ClsCase.TicketID, NewTicketsViolationID);
                        //    //Sabir Khan 6423 08/21/2009 Call sharepoint method in separate thread...
                        //    Thread NewThread = new Thread(() => ManageSharepointViolation(ClsCase.TicketID, -1));
                        //    NewThread.Start(); 
                            
                        //    //SharePointWebWrapper.UpdateAllViolationsInCalendar(ClsCase.TicketID);
                        //}

                        // Rab Nawaz Khan 10197 04/26/2012 Cases Information Update has been stoped on Matter Calender . . . 
                        //ManageSharepointViolation ( this.updateall ? ClsCase.TicketID : -1 , this.updateall ? -1: NewTicketsViolationID );

                    }
                }
                else
                {
                    //Cause No Exists Error Message Display
                    error = "Cause Number already exist in our system. Please Specify a different Cause Number.";
                }

            }
            //in case of existing violation
            else if (!newviolation)
            {
                if (checkCauseNo(causeno, courtlocation, ticketviolationid))
                {
                    SetValuesForUpdation(newviolation);
                    if (ClsCaseDetail.UpdateCaseDetail() == true)
                    {
                        ////Waqas 5173 01/20/2009                        
                        ////Sabir Khan 6248 07/31/2009 Update all events of client on share point portal...
                        ////ManageSharepointViolationEvents(ClsCase.TicketID, ClsCaseDetail.TicketViolationID);
                        ////Sabir Khan 6423 08/21/2009 Call sharepoint method in separate thread...
                        //Thread NewThread = new Thread(() => ManageSharepointViolation(ClsCase.TicketID, ClsCaseDetail.TicketViolationID));
                        //NewThread.Start();

                        ////Aziz 1974 06/20/08 Updating violation to sharepoint
                        ////if (this.updateall)
                        ////{
                        ////    SharePointWebWrapper.UpdateAllViolationsInCalendar(ClsCase.TicketID);
                        ////}
                        ////else
                        ////{
                        ////    SharePointWebWrapper.UpdateSingleViolationInCalendar(ClsCaseDetail.TicketViolationID);
                        ////}
                        // Rab Nawaz Khan 10197 04/26/2012 Cases Information Update has been stoped on Matter Calender . . . 
                        //ManageSharepointViolation(this.updateall ? ClsCase.TicketID : -1, this.updateall ? -1 : ClsCaseDetail.TicketViolationID);

                    }
                }
                else
                {
                    error = "Cause Number already exist in our system. Please Specify a different Cause Number.";
                }
            }
         
         //Sabir Khan 8058 07/23/2010 Check for PMC Pretial cases...
         if (ClsCase.GetActiveFlag() == 1)
         {
                CheckPMCPreTrialMaxOutClient();
         }

        }
        //Waqas 5173 01/20/2009
        /// <summary>
        /// Handling Sharepoint Portal Events for Updating and deletion
        /// </summary>
        /// <param name="TicketID">Ticket ID</param>
        /// <param name="TicketViolationID">TicketViolationID</param>        
        //public bool ManageSharepointViolationEvents(int TicketID, int TicketViolationID)
        //Sabir Khan 6423 08/27/2009 Method argument and return type has been changed for executing in a separate thread.
        public void ManageSharepointViolationEvents(object Parameter)
        {
            ArrayList objArraList = (ArrayList)Parameter;
            int TicketID = Convert.ToInt32(objArraList[0]);
            int TicketViolationID = Convert.ToInt32(objArraList[1]);
            DataSet ds = new DataSet();
            if (TicketID != -1) //Sabir Khan 6423 08/21/2009 condition changed from to check update all into ticketid...
            {
                ds = ClsCaseDetail.GetCaseTypeCoveringFirm(TicketID, -1);
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count != 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            int iCaseTypeID = int.Parse(dr["CaseTypeId"].ToString());
                            int iCoveringFirmID = int.Parse(dr["CoveringFirmID"].ToString());
                            int iTicketViolationID = int.Parse(dr["TicketsViolationID"].ToString());
                            //Sabir Khan 6248 07/31/2009 Get category and courtdate...
                            int iCategory = int.Parse(dr["Category"].ToString());
                            DateTime iCourtDate = DateTime.Parse(dr["CourtDateMain"].ToString());
                            DateTime currTime = DateTime.Today ;
                            int paidAmount = int.Parse(dr["PaidAmount"].ToString());

                            if (iCaseTypeID != 2 && iCaseTypeID != 4 && iCoveringFirmID == 3000)
                            {
                                SharePointWebWrapper.DeleteSingleViolationInCalendar(iTicketViolationID);
                            }
                                //Sabir Khan 6248 07/31/2009 If case status is set to disposed or no hire or court date is set in past...
                            else if (iCategory == 50 &&  iCourtDate >  currTime ) 
                            {
                                SharePointWebWrapper.DeleteSingleViolationInCalendar(iTicketViolationID);
                            }
                            //Sabir Khan 6248 07/31/2009 update matter calendar event only for Arr, Jury, Judge, App, and Pre...
                            else if ((iCaseTypeID == 2 || iCaseTypeID == 4 || iCoveringFirmID != 3000) && (iCategory == 2 || iCategory == 3 || iCategory == 4 || iCategory == 5) && (paidAmount != 0))
                            {
                                SharePointWebWrapper.UpdateSingleViolationInCalendar(iTicketViolationID);
                            }
                            else if (paidAmount == 0) //Sabir Khan 6423 08/25/2009 if payment is viod...
                            {
                               SharePointWebWrapper.DeleteSingleViolationInCalendar(iTicketViolationID); 
                            }
                        }
                    }
                }
            }
            else
            {
                ds = ClsCaseDetail.GetCaseTypeCoveringFirm(-1, TicketViolationID);
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count != 0)
                    {
                        int iCaseTypeID = int.Parse(ds.Tables[0].Rows[0]["CaseTypeId"].ToString());
                        int iCoveringFirmID = int.Parse(ds.Tables[0].Rows[0]["CoveringFirmID"].ToString());
                        //Sabir Khan 6248 07/31/2009 Get category and courtdate...
                        int iCategory = int.Parse(ds.Tables[0].Rows[0]["Category"].ToString());
                        DateTime iCourtDate = DateTime.Parse(ds.Tables[0].Rows[0]["CourtDateMain"].ToString());
                        DateTime currTime = DateTime.Today;
                        int paidAmount = int.Parse(ds.Tables[0].Rows[0]["PaidAmount"].ToString());

                        if (iCaseTypeID != 2 && iCaseTypeID != 4 && iCoveringFirmID == 3000)
                        {
                            SharePointWebWrapper.DeleteSingleViolationInCalendar(TicketViolationID);
                        }
                        //Sabir Khan 6248 07/31/2009 If case status is set to disposed or no hire or court date is set in past...
                        else if (iCategory == 50 || currTime > iCourtDate)
                        {
                            SharePointWebWrapper.DeleteSingleViolationInCalendar(TicketViolationID);
                        }
                        //Sabir Khan 6248 07/31/2009 update matter calendar event only for Arr, Jury, Judge, App, and Pre...
                        else if ((iCaseTypeID == 2 || iCaseTypeID == 4 || iCoveringFirmID != 3000) && (iCategory == 2 || iCategory == 3 || iCategory == 4 || iCategory == 5))
                        {
                            SharePointWebWrapper.UpdateSingleViolationInCalendar(TicketViolationID);
                        }
                        else if (paidAmount == 0) //Sabir Khan 6423 08/25/2009 if payment is viod...
                        {
                            SharePointWebWrapper.DeleteSingleViolationInCalendar(TicketViolationID);
                        }
                    }
                }
            }
            //return true;
        }
        public bool checkCauseNo(string causeno, int courtlocation, int violationid)
        {
            if (causeno == "") return true;


            try
            {

                DataSet ds_CheckCNum = ClsCaseDetail.CheckCauseNumberExistance(violationid, causeno, courtlocation);

                if (ds_CheckCNum.Tables[0].Rows[0][0].ToString() != "0")
                    return false;

            }
            catch (Exception ex)
            {
                error = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

                return false;
            }
            return true;

        }

        /// <summary>
        /// Nasir 5310 12/24/2008 Check ISARLProcess 
        /// </summary>
        /// <param name="CourtID"></param>
        /// <returns></returns>
        public bool IsALRProcess(int CourtID)
        {
            try
            {
                if (chkAlrProcess == 0)
                {
                    ClsCaseDetail = new clsCaseDetail();
                    bool check = ClsCaseDetail.CheckIsARLProcess(CourtID);
                    chkAlrProcess = 1;
                    AlrProcess = check;
                    return check;
                }
                else
                    return AlrProcess;
            }
            catch (Exception ex)
            {
                error = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return false;
            }
        }

        private void SetValuesForInsertion(bool isNew)
        {
            try
            {

                ClsCaseDetail.TicketID = ClsCase.TicketID;

                string sTVIDs = "";

                if (updateall)
                {


                    foreach (DataGridItem dgItem in dgViolationInfo.Items)
                    {
                        HiddenField lbl = (HiddenField)dgItem.FindControl(gridfield);

                        if (sTVIDs != String.Empty)
                        {
                            sTVIDs = String.Concat(sTVIDs, lbl.Value.ToString().Trim(), ",");
                        }
                        else
                        {
                            sTVIDs = String.Concat(lbl.Value.ToString().Trim(), ",");
                        }

                    }


                }


                ClsCaseDetail.TicketViolationID = ClsCaseDetail.TicketViolationID;// Convert.ToInt32(ddl_pvdesc.SelectedValue);

                // Business Logic Should Be Changed  for that 
                ClsCaseDetail.TicketViolationIDs = sTVIDs;// ddl_pvdesc.SelectedValue + ",";
                ClsCaseDetail.RefCaseNumber = ticketnumber;
                ClsCaseDetail.CauseNumber = causeno.Trim().Replace(" ", "");
                /*if (txt_SequenceNo.Text!="")
                {
                    ClsCaseDetail.SequencNumber=Convert.ToInt32(txt_SequenceNo.Text);
                }*/
                if (newviolation)//TxtValue.Text=="0")
                {
                    ClsCaseDetail.ViolationNumber = violationid;
                }
                else
                {
                    ClsCaseDetail.ViolationNumber = violationid;//Convert.ToInt32(ddl_pvdesc.SelectedValue);
                }
                ClsCaseDetail.FineAmount = fineamount;
                ClsCaseDetail.ChargeLevel = chargelevel;
                ClsCaseDetail.BondAmount = bondamount;
                ClsCaseDetail.BondFlag = Convert.ToInt32(bondflag);
                ClsCaseDetail.PlanID = priceplan;
                ClsCaseDetail.CourtViolationStatusID = status;
                ClsCaseDetail.UpdateMain = updateall;

                ClsCaseDetail.OscareCourtDetail = CourtDetail;

                //if (tb_pmonth.Text == "0" || tb_pmonth.Text == String.Empty || tb_pday.Text == "0" || tb_pday.Text == String.Empty || tb_pyear.Text == "0" || tb_pyear.Text == String.Empty)
                //{
                //DateTime courtDate = Convert.ToDateTime("01/01/1900");
                //  ClsCaseDetail.CourtDate = courtDate;
                //}

                //else
                //{
                //  DateTime courtDate = Convert.ToDateTime(String.Concat(tb_pmonth.Text.ToString() + "/", tb_pday.Text.ToString() + "/", tb_pyear.Text.ToString() + " ", ddl_pcrttime.SelectedValue.ToString()));
                ClsCaseDetail.CourtDate = courtdate;
                ClsCaseDetail.ArrignmentWaitingDate = arrignmentwaitingdate;
                ClsCaseDetail.BondWaitingDate = bondwaitingdate;
                //}

                ClsCaseDetail.CourtNumber = courtno.ToString();
                ClsCaseDetail.CourtID = courtlocation;
                ClsCaseDetail.EmpID = empid;
                ClsCaseDetail.CDI = cdi;
                //Zeeshan Ahmed 3724 05/13/2008 Set Arrest Date Parameter
                ClsCaseDetail.ArrestDate = ArrestDate;
                //Zeeshan Ahmed 4163 06/03/2008 Set Missed Court Type
                ClsCaseDetail.MissedCourtType = missedCourtType;
            }
            catch (Exception ex)
            {
                error = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

            }
        }

        private void SetValuesForUpdation(bool isNew)
        {
            try
            {
                if (updateall)
                    ClsCaseDetail.UpdateAll = true;
                else
                    ClsCaseDetail.UpdateAll = false;

                /* if (ClsSession.GetCookie("sIsChanged", this.Request) == "True")
                 {
                     ClsCaseDetail.TicketViolationID = Convert.ToInt32(ViewState["vTicketViolationID"]);
                 }*/

                ClsCaseDetail.TicketViolationID = ticketviolationid;

                //tring sTVIDs = "";
                //Fahad 5299 02/10/2009 Comment Code
                //if (updateall)
                //{
                //    foreach (DataGridItem dgItem in dgViolationInfo.Items)
                //    {
                        //HiddenField lbl = (HiddenField)dgItem.FindControl(gridfield);

                //        if (sTVIDs != String.Empty)
                //        {
                //            sTVIDs = String.Concat(sTVIDs, lbl.Value.ToString().Trim(), ",");
                //        }
                //        else
                //        {
                //            sTVIDs = String.Concat(lbl.Value.ToString().Trim(), ",");
                //        }
                //    }
                //}
                //else
                //{
                //    sTVIDs = ticketviolationid + ",";
                //}
                
                
               //sTVIDs = ticketviolationid + ",";
                //End Fahad 

                //ClsCaseDetail.TicketViolationIDs = sTVIDs;
                ClsCaseDetail.RefCaseNumber = ticketnumber;
                ClsCaseDetail.CauseNumber = causeno.Trim().Replace(" ", "");

                if (CourtDetail != null)
                    ClsCaseDetail.OscareCourtDetail = CourtDetail;
                else
                    ClsCaseDetail.OscareCourtDetail = "";
                /*if(txt_SequenceNo.Text.Trim()!="")
                {
                    ClsCaseDetail.SequencNumber=Convert.ToInt32(txt_SequenceNo.Text);
                }*/
                if (newviolation)//TxtValue.Text == "0")
                {
                    ClsCaseDetail.ViolationNumber = violationid;//Convert.ToInt32(hf_violationid.Value);
                }
                else
                {
                    ClsCaseDetail.ViolationNumber = violationid;//Convert.ToInt32(ddl_pvdesc.SelectedValue);
                }

                ClsCaseDetail.FineAmount = fineamount;
                ClsCaseDetail.ChargeLevel = chargelevel;
                ClsCaseDetail.BondAmount = bondamount;
                ClsCaseDetail.BondFlag = Convert.ToInt32(bondflag);
                ClsCaseDetail.PlanID = priceplan;
                ClsCaseDetail.CourtViolationStatusID = status;

                //if(ddl_Date_Month.SelectedIndex==0 || ddl_Date_Day.SelectedIndex==0 || ddl_Date_Year.SelectedIndex==0 || ddl_Time.SelectedIndex==0)
                //    if (tb_pmonth.Text == "0" || tb_pmonth.Text == String.Empty || tb_pday.Text == "0" || tb_pday.Text == String.Empty || tb_pyear.Text == "0" || tb_pyear.Text == String.Empty)
                {
                    //      DateTime courtDate = Convert.ToDateTime("01/01/1900");
                    ClsCaseDetail.CourtDate = courtdate;
                    ClsCaseDetail.ArrignmentWaitingDate = arrignmentwaitingdate;
                    ClsCaseDetail.BondWaitingDate = bondwaitingdate;
                }
                //else
                {
                    //  DateTime courtDate = Convert.ToDateTime(String.Concat(tb_pmonth.Text.ToString() + "/", tb_pday.Text.ToString() + "/", tb_pyear.Text.ToString() + " ", ddl_pcrttime.SelectedValue.ToString()));
                    ClsCaseDetail.CourtDate = courtdate;
                }

                ClsCaseDetail.CourtNumber = courtno.ToString();
                ClsCaseDetail.UpdateMain = updateall;
                ClsCaseDetail.CourtID = courtlocation;
                // if (txtinactive.Text == "1")
                {
                    //     ClsCaseDetail.CourtID = Convert.ToInt32(lblinactiveCourtID.Text);
                }
                // else
                {
                    //ClsCaseDetail.CourtID = Convert.ToInt32(ddl_pcrtloc.SelectedValue);
                }

                ClsCaseDetail.EmpID = empid;
                ClsCaseDetail.CDI = cdi;
                //Zeeshan Ahmed 3724 05/13/2008 Set Arrest Date Parameter
                ClsCaseDetail.ArrestDate = ArrestDate;
                //Zeeshan Ahmed 4163 06/03/2008 Set Missed Court Type
                ClsCaseDetail.MissedCourtType = missedCourtType;
            }
            catch (Exception ex)
            {
                error = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

            }
        }

        /// <summary>
        /// Sabir Khan 6423 08/21/2009
        /// To call sharepoint method.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ManageSharepointViolation(int iTicketID, int iNewTicketsViolationID)
        {
            try
            {
                ParameterizedThreadStart threadStart = new ParameterizedThreadStart(ManageSharepointViolationEvents);
                Thread sharepointThread = new Thread(threadStart);
                ArrayList objArrayList = new ArrayList();
                objArrayList.Add(iTicketID);
                objArrayList.Add(iNewTicketsViolationID);
                sharepointThread.Start(objArrayList);               
            }
            catch (Exception ex)
            {
                error = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);               
            }

        }
        //Sabir Khan 6248 07/31/2009 Constructor for initializing ClsCaseDetail object...
        public updatePanel()
        {

            ClsCaseDetail = new clsCaseDetail();
        }


        //Sabir Khan 8058 07/23/2010 Shoot an email to managment when PMC Pretial cases has max out
        /// <summary>
        /// Shoot an email to managment when PMC Pretial cases has max out
        /// </summary>
        public void CheckPMCPreTrialMaxOutClient()
        {
         if (courtlocation == 3004)
         {
            DataTable dt =  new DataTable();
            dt = ClsCaseDetail.GetTotalPMCPRreTrialCasesForSelectedDate(courtdate, 3004);
            if(dt.Rows.Count>0)
            {
                string MAXOUTCOUNT = Client.GetValueOfKey(Division.HOUSTON, Module.TRAFFIC_PROGRAM, SubModule.Pasadena, Key.MAXOUT_COUNT);
                if(Convert.ToInt32(dt.Rows[0]["totalcases"].ToString())>=Convert.ToInt32(MAXOUTCOUNT))
                {
                    
                     string PMCMaxOutTo = Client.GetValueOfKey(Division.HOUSTON, Module.TRAFFIC_PROGRAM, SubModule.Pasadena, Key.EMAIL_TO_ADDRESS);
                     string FromAddress = Client.GetValueOfKey(Division.HOUSTON, Module.TRAFFIC_PROGRAM, SubModule.Pasadena, Key.EMAIL_FROM_ADDRESS);

                     string PMCMaxOutSubject = "Pasadena Clients for Court Date=" + courtdate.ToString("d") + " has maxed out(" + dt.Rows[0]["totalcases"].ToString()+")";
                     string PMCMaxOutBody = "The total number of clients have maxed out for PreTrial in Pasadena for Court Date= " + courtdate.ToString("d") + ". Please call Pasadena and get a new court date from them";
                     //Asad Ali 8058 07/26/2010 send Email With Prority
                     //MailClass.SendMail(FromAddress,PMCMaxOutTo,PMCMaxOutSubject,PMCMaxOutBody,"");
                     LNHelper.MailClass.SendMailWithPriority(FromAddress, PMCMaxOutTo, PMCMaxOutSubject, PMCMaxOutBody, "", "", "", System.Net.Mail.MailPriority.High);
                }
            }
         }
        }

    }
}

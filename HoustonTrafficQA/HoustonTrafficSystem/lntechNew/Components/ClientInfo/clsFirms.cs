using System;
using System.Data;
using FrameWorkEnation.Components;


namespace lntechNew.Components.ClientInfo
{
    /// <summary>
    /// Summary description for clsFirms.
    /// </summary>
    public class clsFirms
    {
        //Create an Instance of Data Access Class
        clsENationWebComponents ClsDb = new clsENationWebComponents();


        #region Variables

        private int firmID = 0;
        private string firmName = String.Empty;
        private string firmAbbreviation = String.Empty;
        private string address = String.Empty;
        private string address2 = String.Empty;
        private string city = String.Empty;
        private int state = 0;
        private string zip = String.Empty;
        private string phone = String.Empty;
        private string fax = String.Empty;
        private string attorneyName = String.Empty;
        private string firmSubTitle = String.Empty;
        private double baseFee = 0;
        private double dayBeforeFee = 0;
        private double judgeFee = 0;

        #endregion

        #region Properties

        public int FirmID
        {
            get
            {
                return firmID;
            }
            set
            {
                firmID = value;
            }
        }

        public string FirmName
        {
            get
            {
                return firmName;
            }
            set
            {
                firmName = value;
            }
        }

        public string FirmAbbreviation
        {
            get
            {
                return firmAbbreviation;
            }
            set
            {
                firmAbbreviation = value;
            }
        }

        public string Address
        {
            get
            {
                return address;
            }
            set
            {
                address = value;
            }
        }

        public string Address2
        {
            get
            {
                return address2;
            }
            set
            {
                address2 = value;
            }
        }

        public string City
        {
            get
            {
                return city;
            }
            set
            {
                city = value;
            }
        }

        public int State
        {
            get
            {
                return state;
            }
            set
            {
                state = value;
            }
        }

        public string Zip
        {
            get
            {
                return zip;
            }
            set
            {
                zip = value;
            }
        }

        public string Phone
        {
            get
            {
                return phone;
            }
            set
            {
                phone = value;
            }
        }

        public string Fax
        {
            get
            {
                return fax;
            }
            set
            {
                fax = value;
            }
        }

        public string AttorneyName
        {
            get
            {
                return attorneyName;
            }
            set
            {
                attorneyName = value;
            }
        }

        public string FirmSubTitle
        {
            get
            {
                return firmSubTitle;
            }
            set
            {
                firmSubTitle = value;
            }
        }

        public double BaseFee
        {
            get
            {
                return baseFee;
            }
            set
            {
                baseFee = value;
            }
        }

        public double DayBeforeFee
        {
            get
            {
                return dayBeforeFee;
            }
            set
            {
                dayBeforeFee = value;
            }
        }

        public double JudgeFee
        {
            get
            {
                return judgeFee;
            }
            set
            {
                judgeFee = value;
            }
        }

        #endregion

        #region Methods

        public DataSet GetAllFirmInfo()
        {
            DataSet ds = ClsDb.Get_DS_BySP("usp_HTS_GetAllFirms");
            return ds;
        }

        /// <summary>
        /// ozair 5306 12/11/2008
        /// get all firm full names and ids order by full name ASC.   
        /// </summary>
        /// <returns> dataset</returns>
        public DataSet GetAllFullFirmName()
        {
            DataSet ds = ClsDb.Get_DS_BySP("usp_HTP_Get_AllFullFirmsName");
            return ds;
        }

        public DataSet GetFirmInfo(int FirmID)
        {
            DataSet ds = ClsDb.Get_DS_BySPByOneParmameter("usp_HTS_GetFirmInfo", "@FirmID", FirmID);
            return ds;
        }

        public DataSet GetActiveFirms(int FirmType)
        {
            // Created By Zeeshan Ahmed on 8th May Changes Request
            // 0 Mean Show Firm whom cases we are covered 
            // 1 Mean Show Firm that cover our cases 
            // FirmType is used to display firms for General Info Page and Case Disposition
            DataSet ds = ClsDb.Get_DS_BySPByOneParmameter("usp_HTS_GetActiveFirms", "@FirmType", FirmType);
            return ds;
        }

        public string GetFirmAbbreviation(int FirmID)
        {
            // Created By Zeeshan Ahmed on 8th May Changes Request
            string[] keys = { "@FirmID" };
            object[] values = { FirmID };
            return Convert.ToString(ClsDb.ExecuteScalerBySp("usp_HTS_GetFirmAbbreviation", keys, values));

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>

        public DataTable GetFirmsForCriminalPaymentReport()
        {
            DataSet ds = ClsDb.Get_DS_BySP("usp_HTP_Get_FirmsForCriminalPaymentReport");
            return ds.Tables[0];
        }

        public DataTable GetFirmTransactionDetails(DateTime FromDate, DateTime ToDate, int repid, int payId, int courtid)
        {
            DataSet ds;
            string[] keys = { "RecDate", "RecDateTo", "EmployeeID", "PaymentType", "CourtID", "@firmId" };
            object[] values = { FromDate, ToDate, repid, payId, courtid, (object)DBNull.Value };

            if (payId > 500)
            {
                ds = ClsDb.Get_DS_BySPArr("usp_Get_All_PaymentDetailOfCCByCriteriaRange", keys, values);
            }
            else
            {
                ds = ClsDb.Get_DS_BySPArr("usp_Get_PaymentDetailforPaymentReport", keys, values);
            }


            return ds.Tables[0];
        }

        public DataTable GetFirmCourtSumamry(DateTime FromDate, DateTime ToDate, int firmid)
        {
            string[] key1 = { "@RecDate", "@RecTo", "@firmId" };
            object[] value1 = { FromDate, ToDate, firmid };
            DataSet ds = ClsDb.Get_DS_BySPArr("usp_Get_All_CourtInfoByRecDateRange", key1, value1);
            return ds.Tables[0];
        }

        public DataTable GetFirmsReps(DateTime FromDate, DateTime ToDate)
        {
            string[] key2 = { "@sDate", "@eDate" };
            object[] value2 = { FromDate, ToDate };
            DataSet ds = ClsDb.Get_DS_BySPArr("usp_Get_All_RepList", key2, value2);
            return ds.Tables[0];

        }

        public IDataReader GetFirmCCType()
        {
            return ClsDb.Get_DR_BySP("usp_Get_All_CCType");
        }

        //Sabir Khan 5084 11/10/2008
        /// <summary>
        /// This function Get the firm text message info using ticket id 
        /// </summary>
        /// <param name="ticketid">TicketID</param>
        public DataTable GetFirmTextMessageInfo(int TicketID)
        {
            string[] keys2 = { "@TicketID" };
            object[] values2 = { TicketID };

            DataTable dtTextMessage = ClsDb.Get_DT_BySPArr("USP_HTP_GetTextMessageNumber", keys2, values2);

            return dtTextMessage;
        }

        //Fahad 07/20/2009 6054 This method use to get all Outsude Firms
        /// <summary>
        /// Method use to Get All Outside Firms
        /// </summary>
        /// <returns></returns>
        public DataSet GetAllOutsideFirmsInfo()
        {
            DataSet ds = ClsDb.Get_DS_BySP("USP_HTP_GetAllOutSideFirm");
            return ds;
        }

        //Fahad 07/24/2009 6054 This method use to get all Firms on the basis of TicketId
        /// <summary>
        /// Method use to Firms on the basis of TicketId
        /// </summary>
        /// <returns></returns>
        public int GetFirmByTicketId(int TicketID)
        {

            string[] keys2 = { "@TicketID" };
            object[] values2 = { TicketID };
            int OutsideFirm = Convert.ToInt32(ClsDb.ExecuteScalerBySp("USP_HTP_GetFirmByTicketId", keys2, values2));
            return OutsideFirm;

        }

        // Noufil 6052 09/14/2009 
        /// <summary>
        ///     This method returns all the charge level and the charge amount of the input Firm
        /// </summary>
        /// <param name="firmID">ID of Firm/Attorney</param>
        /// <returns>DataTable with Charge Level and the Associate Charge Amount</returns>
        public DataTable GetFirmChargeLevelAmount(int firmID)
        {
            DataTable dtchargelevel = null;
            if (firmID > 0)
            {
                dtchargelevel = new DataTable();
                string[] key = { "@FirmID" };
                object[] value = { firmID };
                dtchargelevel = ClsDb.Get_DT_BySPArr("USP_HTP_GET_ChargeLevelWithAmount", key, value);
            }
            else
                throw new ArgumentNullException("Firm ID cannot be null");
            return dtchargelevel;
        }

        // Noufil 6052 09/14/2009 
        /// <summary>
        ///     This method returns all the attorney of Criminal Case type.
        /// </summary>
        /// <returns>List of Attornies</returns>
        public DataTable GetCriminalAttorney()
        {
            DataTable dt = ClsDb.Get_DT_BySPArr("USP_HTP_GET_CriminalCase_Attorney");
            return dt;
        }


        // Noufil 6052 09/14/2009 
        /// <summary>
        ///     This method insert/Update charge level amount for a given Firm/Attorney
        /// </summary>
        /// <param name="FirmID">ID of Attorney</param>
        /// <param name="amount">Charge Level Amount</param>
        /// <param name="chargelevelid">ID of Charge Level</param>
        public void UpdateChargeAmount(int FirmID, double amount, int chargelevelid)
        {
            if (FirmID > 0 && chargelevelid > 0)
            {
                string[] key = { "@firmid", "@chargelevelid", "@amount" };
                object[] value = { FirmID, chargelevelid, amount };
                ClsDb.ExecuteSP("[USP_HTP_INSERT_FirmChargeLevel]", key, value);
            }
            else
                throw new ArgumentNullException("Parameter cannot be null for updating Charge amount");
        }


        // Noufil 6052 09/14/2009 
        /// <summary>
        ///         This method returns all those criminal dispose client whose attorney payment is due and need to be paid.
        /// </summary>
        /// <param name="startdate">Start Disposition Date</param>
        /// <param name="enddate">End Disposition Date</param>
        /// <param name="attorney">ID of Attorney</param>
        /// <param name="URL">Website URL</param>
        /// <returns>Datatable with Records</returns>
        public DataTable GetAttorneyPayoutReport(DateTime startdate, DateTime enddate, int attorney, string URL)
        {
            DataTable dt = null;
            if (startdate != null && enddate != null && attorney > 0)
            {
                string[] key = { "@startdate", "@enddate", "@Attorney", "@ReportURL" };
                object[] values = { startdate, enddate, attorney, URL };
                dt = ClsDb.Get_DT_BySPArr("[dbo].[USP_HTP_GetAttorneyPayout_Report]", key, values);
            }
            else
                throw new ArgumentNullException("Attorney Payout Report parameter cannot be null");

            return dt;
        }

        // Noufil 6052 09/14/2009 
        /// <summary>
        ///         This method update attorney paid amount and date when attorney paid letter printed.
        /// </summary>
        /// <param name="isattorneypayoutprinted">Is attorney paid letter printed</param>
        /// <param name="amount">Atotrney paid amount</param>
        /// <param name="printdate">Letter print date</param>
        /// <param name="ticketviolationid">Ticket Violation ID of Violation of case</param>
        public void UpdateAttorneyPaidAmount(int isattorneypayoutprinted, double amount, DateTime printdate, int ticketviolationid)
        {
            string[] key2 = { "@isattorneypayoutprinted", "@attorneypaidamount", "@attorneypaiddate", "@ticketsviolationID" };
            object[] value2 = { isattorneypayoutprinted, amount, printdate, ticketviolationid };
            ClsDb.ExecuteSP("USP_HTP_UpdateAttorneyPaid", key2, value2);
        }

        // Noufil 6052 09/14/2009 
        /// <summary>
        ///         This method insert printed document and related information into table TBLHTSNOTES
        /// </summary>
        /// <param name="batchid">Batch ID</param>
        /// <param name="ticketid">Ticket ID of Client</param>
        /// <param name="printdate">Letter print date</param>
        /// <param name="letterid">Letter ID</param>
        /// <param name="empid">User ID</param>
        /// <param name="note">Subject Note</param>
        /// <param name="filename">File name</param>
        public void InsertIntoHTSNotes(int batchid, int ticketid, DateTime printdate, int letterid, int empid, string note, string filename, string historynote)
        {
            string[] key = { "@BatchID", "@ticketid", "@PrintDate", "@LetterID_FK", "@EmpID", "@LetterName", "@DocPath", "@SUBJECT" };
            object[] value = { batchid, ticketid, printdate, letterid, empid, note, filename, historynote };
            ClsDb.ExecuteSP("USP_HTP_INSERT_TBLHTSNOTES", key, value);

        }

        // Noufil 6052 09/14/2009 
        /// <summary>
        ///     Ths method insert printed attorney paid letter history.
        /// </summary>
        /// <param name="empid">ID of Employee who had print the letter</param>
        /// <param name="firmid">ID of FIRM/Attorney</param>
        /// <param name="printdate">Date when letter printed</param>
        /// <param name="filename">File Name</param>
        public void InsertAttorneyPaidHistory(int empid, int firmid, DateTime printdate, string filename)
        {
            string[] key1 = { "@EmpID", "@FirmID", "@PrintDatetime", "@Filepath" };
            object[] value1 = { empid, firmid, printdate, filename };
            ClsDb.ExecuteSP("USP_HTP_INSERT_ATTORNEYPAYOUTHISTORY", key1, value1);
        }

        public DataTable GetAttorneyPaidInfoByTicketid(int TicketId)
        {
            DataTable dt = new DataTable();
            string[] key_attorney = { "@TicketID" };
            object[] object_attorney = { TicketId };
            dt = ClsDb.Get_DT_BySPArr("USP_HTP_GetAttorneyPaidAmountByTicketID", key_attorney, object_attorney);
            return dt;
        }

        #endregion

        public clsFirms()
        {
            //
            // TODO: Add constructor logic here
            //
            //Noufil 3389 03/13/08 testing
        }
    }
}

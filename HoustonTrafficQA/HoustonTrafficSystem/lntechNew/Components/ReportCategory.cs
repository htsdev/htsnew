using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;


namespace lntechNew.Components
{    

    public class ReportCategory
    {
        clsLogger clog = new clsLogger();
        clsENationWebComponents ClsDb = new clsENationWebComponents("Connection String");


        #region Variables 

        private int categoryid = 0;
        private int rptid = 0;
        private int assingedunassigned = 0;
        private string categoryname = string.Empty;
        
        #endregion


        #region Properties

        public string CategoryName
        {
            set
            {
                categoryname = value;
            }
            get
            {
                return categoryname;
            }
        }

        public int CategoryID
        {
            set
            {
                categoryid = value;
            }
            get
            {
                return categoryid;
            }
        }

        public int RptID
        {
            set
            {
                rptid = value;
            }
            get
            {
                return rptid;
            }
        }

        public int AssignedUnassinged
        {
            set
            {
                assingedunassigned = value;
            }
            get { return assingedunassigned; }
        }

        #endregion



        #region Methods

        public DataSet GetCategory()
        {
            try
            {
                DataSet ds = new DataSet();
                ds = ClsDb.Get_DS_BySP("USP_HTS_GET_ReportCategory");
                               
               return ds;
                

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public bool InsertCategory()
        {
            try
            {
                string[] keys ={ "@CategoryName" };
                object[] values = { categoryname };

                if (categoryname == "" || categoryname == null)
                    return false;

                ClsDb.InsertBySPArr("USP_HTS_Insert_ReportCategory", keys, values);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public DataSet DeleteCategory()
        {
            try
            {
                
                string[] keys ={ "@CategoryID" };
                object[] values = { categoryid };

                DataSet ds = new DataSet();

                if (categoryid == 0)
                    return ds;

                ds = ClsDb.Get_DS_BySPArr("USP_HTS_Delete_ReportCategory", keys, values);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetReportByCategory()
        {
            try
            {                
                DataSet ds = new DataSet();

                ds = ClsDb.Get_DS_BySPArr("USP_HTS_GET_AssignedUnassigned");
               return ds;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetReportByCategoryID()
        {
            try
            {
                string[] keys ={ "@ReportCategoryID" };
                object[] values = { categoryid };

                DataSet ds = new DataSet();
                               
                ds = ClsDb.Get_DS_BySPArr("USP_HTS_GET_ReportByCategoryID", keys, values);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdateAssignedUnassined()
        {
            try
            {
                string[] keys ={ "@ReportCategoryID", "@RptID", "@AssignedUnassigned" };
                object[] values = {categoryid,rptid, assingedunassigned };

                if ((rptid == 0) && (assingedunassigned !=0 || assingedunassigned != 1))
                    return false;

                ClsDb.ExecuteSP("Usp_Hts_Update_AssignedUnassignedCategory", keys, values);
                return true;

            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        //Nasir 6968 12/14/2009 add new method GetReportsName
        /// <summary>
        /// Get reports name
        /// </summary>
        /// <param name="IsAdminLog">is allow to main tain log</param>
        /// <returns>datatable</returns>
        public DataTable GetReportsName(bool IsAdminLog)
        {
            string[] keys = { "@IsAdminLog" };
            object[] values = { IsAdminLog };
            return ClsDb.Get_DT_BySPArr("USP_HTP_Get_ReportsNameAdminLog", keys, values);
        }

        #endregion

        
    }
}

﻿set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

/*
Author: Muhammad Adil Aleem
Created date : Jul-17th-2008
Business Logic: To get appropriate court status according to our system.
Parameters:
	@CourtStatus: Data file's court status.
	@DispositionCode: Case disposition code.
	@DispositionDate: Case disposition date.
	@JudgmentDate: Judgment Date.
	@ArrestingAgencyName: Arresting Agency Name.
	@PleaCode: Plea Code.
	@LastOpenWarrType: Last Open Warrant Type.
	@LoaderID: Loader ID.
	@JuvenileStatus: Juvenile Status.
	@TotalPaid:	Total Amount Paid.
*/
ALTER FUNCTION [dbo].[fn_HCJP_CourtStatus](@CourtStatus varchar(40), @DispositionCode varchar(10), @DispositionDate datetime, @JudgmentDate datetime, @ArrestingAgencyName varchar(50), @PleaCode varchar(10), @LastOpenWarrType varchar(20), @LoaderID int, @JuvenileStatus int, @TotalPaid money)
Returns int
As
Begin

declare @ViolationStatusID int

Set @CourtStatus = replace(@CourtStatus,'TRAIL','TRIAL')
Set @CourtStatus = replace(@CourtStatus,'COMUNITY','COMMUNITY')

If @LoaderID in (23, 24) -- Entered & Events Cases
	Begin
		Set @ViolationStatusID = (Case

		When isnull(@DispositionCode,'') not in ('','WS','JUD','JUR') OR @TotalPaid > 0 OR @LastOpenWarrType = 'CPF' then 161 -- Non Issue, -- Adil 7433 02/20/2010 Warrant Type CPF should go into non issue court status.
		
		When @CourtStatus = '' And (Isnull(@DispositionDate,'1/1/1900') <> '1/1/1900' or Isnull(@JudgmentDate,'1/1/1900') <> '1/1/1900' or @PleaCode Not In ('NG','')) then 161 -- Non Issue
		When @CourtStatus = '' And (Isnull(@DispositionDate,'1/1/1900') = '1/1/1900' And Isnull(@JudgmentDate,'1/1/1900') = '1/1/1900' And @PleaCode In ('NG','')) then 3 -- Arraignment

		When (@CourtStatus = 'APPEARANCE' or @CourtStatus = 'APP') And (Isnull(@DispositionDate,'1/1/1900') <> '1/1/1900' or Isnull(@JudgmentDate,'1/1/1900') <> '1/1/1900' or @PleaCode Not In ('NG','')) then 161 -- Non Issue
		When (@CourtStatus = 'APPEARANCE' or @CourtStatus = 'APP') And (Isnull(@DispositionDate,'1/1/1900') = '1/1/1900' And Isnull(@JudgmentDate,'1/1/1900') = '1/1/1900' And @PleaCode In ('NG','')) then 3 -- Arraignment

		When @CourtStatus = 'JURY TRIAL' And (Isnull(@DispositionDate,'1/1/1900') <> '1/1/1900' or Isnull(@JudgmentDate,'1/1/1900') <> '1/1/1900' or @PleaCode Not In ('NG','')) then 161 -- Non Issue
		When @CourtStatus = 'JURY TRIAL' And (Isnull(@DispositionDate,'1/1/1900') = '1/1/1900' And Isnull(@JudgmentDate,'1/1/1900') = '1/1/1900' And @PleaCode In ('NG','')) then 26 -- JURY

		When @CourtStatus = 'NONJURY TRIAL' And (Isnull(@DispositionDate,'1/1/1900') <> '1/1/1900' or Isnull(@JudgmentDate,'1/1/1900') <> '1/1/1900' or @PleaCode Not In ('NG','')) then 161 -- Non Issue
		When @CourtStatus = 'NONJURY TRIAL' And (Isnull(@DispositionDate,'1/1/1900') = '1/1/1900' And Isnull(@JudgmentDate,'1/1/1900') = '1/1/1900' And @PleaCode In ('NG','')) then 103 -- JUDGE TRIAL

		When @CourtStatus = 'NEXT SETTING' And (Isnull(@DispositionDate,'1/1/1900') <> '1/1/1900' or Isnull(@JudgmentDate,'1/1/1900') <> '1/1/1900' or @PleaCode Not In ('NG','')) then 161 -- Non Issue
		When @CourtStatus = 'NEXT SETTING' And (Isnull(@DispositionDate,'1/1/1900') = '1/1/1900' And Isnull(@JudgmentDate,'1/1/1900') = '1/1/1900' And @PleaCode In ('NG','')) then 104 -- Waiting

		When @CourtStatus = 'PENDING' 
		  OR @CourtStatus = 'MOTION DOCKET'
		  OR @CourtStatus = 'PENDING DOCKET SETTING' then 104 -- Waiting

		When @CourtStatus = 'TEEN COURT' then 3 -- Arraignment

		When @CourtStatus = 'PRETRIAL CONFERENCE' then 101 -- PRETRIAL

		When @CourtStatus = 'ADMINISTRATIVE HEARING'
		  OR @CourtStatus = 'HEARING' then 118 -- Hearing

		When @CourtStatus = 'CONTACT THE COURT IMMEDIATELY' And @LastOpenWarrType <> '' then 186 -- FTA Warrant
		When @CourtStatus = 'CONTACT THE COURT IMMEDIATELY' And @JuvenileStatus = 1 And @LastOpenWarrType = '' then 3 -- Arraignment
		When @CourtStatus = 'CONTACT THE COURT IMMEDIATELY' And @JuvenileStatus = 0 And @LastOpenWarrType = '' And @PleaCode In ('NG','') then 146 -- DLQ
		When @CourtStatus = 'PENDING REFERRAL REVIEW' And @JuvenileStatus = 0 And @LastOpenWarrType = '' And @PleaCode In ('NG','') then 146 -- DLQ
		When @CourtStatus = 'CONTACT THE COURT IMMEDIATELY' And @JuvenileStatus = 0 And @LastOpenWarrType = '' And @PleaCode Not In ('NG','') then 161 -- Non Issue

		When CHARINDEX('DPS-FTA Program Fee Due', @CourtStatus, 0) > 0 -- Adil 6499 09/01/2009 set non issue if court status contains 'DPS-FTA Program Fee Due'
		  or @CourtStatus = 'DRIVING SAFETY COURSE COMPLIANCE'
		  or @CourtStatus = 'DRIVING SAFETY COURSE SHOW CAUSE HEARING'
		  or @CourtStatus = 'SHOW CAUSE HEARING'
		  or @CourtStatus = 'PAYMENT DUE'
		  or @CourtStatus = 'COMMUNITY SERVICE REPORT DUE'
		  or @CourtStatus = 'DA COMPLIANCE'
		  or @CourtStatus = 'DPSFTA FEE DUE'
		  or @CourtStatus = 'PENDING CAPIAS PRO FINE LETTER' 
		  or @CourtStatus = 'DPS-FTA Program Fee Due/Contact the Cour'
		  then 161 -- Non Issue
		Else 147 -- Other
		End)
	End

Else If @LoaderID = 25 OR @CourtStatus = 'DRIVING SAFETY COURSE - MANDATORY' -- Disposed Cases
	BEGIN
		Set @ViolationStatusID = 80 -- Disposed
	End

Else
	Begin
		Set @ViolationStatusID = 147 -- Other
	End
--
--Select @ViolationStatusID as ViolationStatusID, 
--(Case @ViolationStatusID 
--When 161 then 'Non Issue'
--When 3 then 'Arraignment'
--When 147 then 'Other'
--When 104 then 'Waiting'
--When 26 then 'JURY'
--When 186 then 'FTA Warrant'
--When 146 then 'DLQ'
--When 103 then 'JUDGE TRIAL'
--When 101 then 'PRETRIAL'
--When 80 then 'Disposed'
--Else 'Not Found - Error' End) as CourtStatus

Return @ViolationStatusID

End

﻿USE LA_Houston
GO

/******  
* Created By :	  Farrukh Iftikhar
* Create Date :   11/01/2012 5:59:27 PM 
* Task ID :		  10438
* Business Logic : Get the loader summary log on the basis of loader name and loader Id  
* List of Parameter :
	@LoaderName Loader Name,
	CONVERT(VARCHAR,@GroupId) Loader Id,
	@TotalRecords Total record count,
	@ReliableRecords Reliable record count
* Column Return : Loader Information.    
******/
--[USP_HTS_LA_GET_SummaryLog_HCCC] 'abc',59255,7,7,2
CREATE PROCEDURE [dbo].[USP_HTS_LA_GET_SummaryLog_HCCC] 
	@LoaderName VARCHAR(100),
	@GroupId INT,
	@TotalRecords INT,
	@ReliableRecords INT,
	@DatbaseId INT
AS
	DECLARE @DatabaseName VARCHAR(20)
	
	SET @DatabaseName=CASE WHEN @DatbaseId=1 THEN 'TrafficTickets' ELSE 'DallasTrafficTickets' END
	DECLARE @sql VARCHAR(MAX)
	SET @sql=''
	SET @sql=@sql+'
	DECLARE @DataFileDate SMALLDATETIME
	DECLARE @LoaderID INT
	DECLARE @NonClientInsertedCount INT
	DECLARE @NonClientUpdatedCount INT
	DECLARE @NonClientTotalCount INT
	DECLARE @NonClientNotUpdatedCount INT 
	DECLARE @FileName VARCHAR(200)	
	DECLARE @EmailBody VARCHAR(MAX)
	DECLARE @tblStatus TABLE (DESCRIPTION VARCHAR(50),[VALUES] VARCHAR(200))	
	DECLARE @Summmary VARCHAR(MAX)
	DECLARE @CRLF char(2)     
	
	SELECT TOP 1 @DataFileDate = ISNULL(Load_Date, GETDATE()),@LoaderID = LoaderID FROM tbl_hts_la_group WHERE groupid = ' + CONVERT(VARCHAR(5),@GroupId) + '
	
	
	SET @CRLF=CHAR(13)+CHAR(10)	
	SET @NonClientTotalCount = ' + CONVERT(VARCHAR(5), @TotalRecords) + 
	'SET @NonClientUpdatedCount = 0
	SET @NonClientInsertedCount = 0	
	SET @Summmary=''''
	INSERT INTO @tblStatus VALUES (@CRLF+'' Load Date : '', CONVERT(VARCHAR(12),GETDATE(),101))
	INSERT INTO @tblStatus VALUES (@CRLF+'' Loader Name : '','''+@LoaderName+''')
	INSERT INTO @tblStatus VALUES (@CRLF+'' Total No. of Records : '','+CONVERT(VARCHAR,@TotalRecords)+')
		
	-- For Non Clients
	INSERT INTO @tblStatus values(@CRLF+'' For Non-Clients : '', '''')
	
	SELECT @NonClientUpdatedCount = @NonClientUpdatedCount + CASE 
																WHEN DATEDIFF(minute, @DataFileDate, ttva.loaderupdatedate) <= 40 THEN 1
																ELSE 0
															 END
	FROM  '+@DatabaseName+'.dbo.tblTicketsViolationsArchive ttva
	WHERE  ttva.GroupID = '+CONVERT(VARCHAR,@GroupId)+'
	AND UpdationLoaderId = @LoaderID
		       
	SELECT @NonClientInsertedCount = @NonClientInsertedCount + CASE 
																	WHEN DATEDIFF(minute, ttva.InsertDate, @DataFileDate) <= 40 THEN 1
																	ELSE 0
															   END
	FROM  '+@DatabaseName+'.dbo.tblTicketsViolationsArchive ttva
	WHERE  ttva.GroupID = '+CONVERT(VARCHAR,@GroupId)+'
	AND InsertionLoaderId = @LoaderID

	INSERT INTO @tblStatus values(@CRLF+'' No of Inserted Records : '', convert(varchar(5), @NonClientInsertedCount) + '' '')
	INSERT INTO @tblStatus 
	SELECT @CRLF + '' ''+ tcvs.[Description] + '' '', convert(varchar(5), COUNT(ttva.RowID)) + '' ''
	FROM '+@DatabaseName+'.dbo.tblTicketsViolationsArchive ttva 
	INNER JOIN '+@DatabaseName+'.dbo.tblCourtViolationStatus tcvs ON ttva.violationstatusid=tcvs.CourtViolationStatusID
	WHERE ttva.GroupID ='+CONVERT(VARCHAR,@GroupId)+' AND DATEDIFF(DAY, ttva.InsertDate, GETDATE()) = 0 AND ttva.InsertionLoaderId = @LoaderID GROUP BY tcvs.[Description]	
	INSERT INTO @tblStatus values(@CRLF+'' No of Updated Records : '', convert(varchar(5), @NonClientUpdatedCount) + '' '')
	INSERT INTO @tblStatus 
	
	SELECT @CRLF + '' ''+ tcvs.[Description] + '' '', convert(varchar(5), COUNT(ttva.RowID)) + '' ''
	FROM '+@DatabaseName+'.dbo.tblTicketsViolationsArchive ttva 
	INNER JOIN '+@DatabaseName+'.dbo.tblCourtViolationStatus tcvs ON ttva.violationstatusid=tcvs.CourtViolationStatusID
	WHERE ttva.GroupID ='+CONVERT(VARCHAR,@GroupId)+' AND DATEDIFF(DAY, @DataFileDate, ttva.loaderupdatedate)= 0 AND  ttva.UpdationLoaderId = @LoaderID GROUP BY tcvs.[Description]	  
	INSERT INTO @tblStatus values(@CRLF+'' No of Not Updated Records : '', @NonClientTotalCount-(@NonClientInsertedCount + @NonClientUpdatedCount))
 
	---------------------------------------
		
	INSERT INTO @tblStatus
	SELECT @CRLF+'' File No : '','+CONVERT(VARCHAR,@GroupId)+'
	INSERT INTO @tblStatus 
	SELECT @CRLF+'' File Name : '',thlg.SourceFileName FROM Tbl_HTS_LA_Group thlg 
	WHERE  thlg.GroupID = '+CONVERT(VARCHAR,@GroupId)+'
	INSERT INTO @tblStatus VALUES (@CRLF+'' Load By : '',''Loader Service'')
	--SELECT * FROM @tblStatus
	
	SELECT @Summmary=@Summmary+ISNULL ([DESCRIPTION],'''')+ISNULL([VALUES],'''') FROM @tblStatus 
	
	SELECT @Summmary'
	
	PRINT @sql
	EXEC (@sql)



GO
GRANT EXECUTE ON [dbo].[USP_HTS_LA_GET_SummaryLog_HCCC]  TO dbr_webuser



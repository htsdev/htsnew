﻿


/*
Author: Adil Aleem
Business Logic : Use to insert OR update warrants for the DMC court house cases through loader service.
It performs following activities.
1. Check violation in Violation's referential table if not exists then insert new violation.
2. Check defendant's profile in non-client if not exists then insert new defendant.
3. Check defendant's violations if not exists then insert new violations against defendant.

Parameters: 
	@Record_Date: Case record/insert date
	@List_Date: Court list date
	@Ticket_Number:Cause OR Violation number
	@Last_Name: Defendant's last name
	@First_Name: Defendant's first name
	@Race: Defendant's Race
	@Gender: Defendant's Gender
	@DOB: Defendant's Date of Birth
	@Address: Defendant's home address
	@Address2: Defendant's home address II
	@City: Defendant's home city
	@State: Defendant's home state
	@ZIP: Defendant's home zip code
	@DP2: Defendant's barcode part. return by zp4 webservice
	@DPC: Defendant's barcode part. return by zp4 webservice
	@Violation_Description: Violation's title of Defendant
	@violationcode : Violation COde
	@violation Date : Violation Date
	@GroupID: Data file ID
	@AddressStatus: Address status [y/d/s/n] of defendant
	@Flag_SameAsPrevious: insure wether this case belongs to previous one or not [It helps to cut-off researching the defendant profile]
	@PreviousRecordID: If this case belongs to previous case then previous case ID will be the value of @PreviousRecordID
	@LoaderID: Insure which loader is executing this case.
	@CurrentRecordID: It returns current Case-ID/Record-ID to calling program
	@Flag_Inserted: It returns status wether the record inserted or deleted
*/

ALTER PROCEDURE [dbo].[USP_HTS_LA_Insert_Tickets_DallasBond]
@Record_Date datetime,  
@List_Date datetime,  
@Ticket_Number as varchar(20),
@Last_Name as varchar(50),  
@First_Name as varchar(50),  
@Race as varchar(10),
@Gender as varchar(10),
@DOB as datetime,
@Address as varchar(50),  
@Address2 as varchar(50),  
@City as varchar(35),  
@State as Varchar(10),
@ZIP as varchar(10),  
@DP2 varchar(2),
@DPC varchar(1),
@Violation_Description as Varchar(2000),
@Violation_Code as Varchar(50),
@Violation_Date as smalldatetime,
@GroupID as Int,
@AddressStatus Char(1),
@Flag_SameAsPrevious as bit,
@PreviousRecordID as int,
@LoaderID as int,
@CurrentRecordID int output,
@Flag_Inserted int Output
AS
set nocount on  

Declare @StateID as Int
Declare @ViolationID as int
Declare @IDFound as int
Declare @TicketNum2 as varchar(20)
Declare @ChargeAmount as money
Declare @BondAmount as money
Declare @Court_Date as datetime
Declare @Bond_Date_Found as datetime
DECLARE @IsQuoted AS INT

SET @IsQuoted = 0
Set @First_Name = left(@First_Name,20)
Set @Last_Name = left(@Last_Name,20)
Set @Address = Replace(@Address, '  ', '')
Set @Violation_Code = 'XD' + @Violation_Code
Set @Flag_Inserted = 0
Set @Court_Date = dateadd(day,21,@Violation_Date)

if datepart(dw,@Court_Date) = 7 -- If Saturday then make it Friday (Court date should be in working days)
	begin
		set @Court_Date = @Court_Date - 1
	end
else if datepart(dw,@Court_Date) = 1 -- If Sunday then make it Friday (Court date should be in working days)
	begin
		set @Court_Date = @Court_Date - 2
	end

If @ZIP = '00000'
	Begin
		Set @ZIP = ''
	End

if @Gender = 'M'
	begin
		Set @Gender = 'Male'
	end
else if @Gender = 'F'
	begin
		Set @Gender = 'Female'
	end

Select @Race = case @Race 
when 'A' then 'Asian'
when 'B' then 'Black'
when 'H' then 'Hispanic'
when 'I' then 'Indian'
when 'M' then 'Mexican'
when 'O' then 'Other'
when 'U' then 'Unknown'
when 'W' then 'White'
when 'X' then 'Unknown'
else null
end

If Len(@State)<1
	Begin
		Set @State = 'TX'
	End

Set @ViolationID = null

Select Top 1 @ViolationID = ViolationNumber_PK, @ChargeAmount = isnull(ChargeAmount,0), @BondAmount = isnull(BondAmount,0) From DallasTrafficTickets.dbo.TblViolations Where (Description = @Violation_Description and ViolationCode = @Violation_Code and CourtLoc = 3049 and ViolationType = 4)

If @ViolationID is null
	Begin
		Insert Into DallasTrafficTickets.dbo.TblViolations (Description, Sequenceorder, IndexKey, IndexKeys, ChargeAmount, BondAmount, Violationtype, ChargeonYesflag, ShortDesc, ViolationCode, AdditionalPrice, Attorneyregularprice, Attorneybondprice, CourtLoc)
		Values(@Violation_Description, null, 2, 160, 160, 200, 4, 1, 'None', @Violation_Code, 0, 50, 95, 3049)
		Set @ViolationID = scope_identity()
		Set @ChargeAmount = 160
		Set @BondAmount = 200
	End

Set @StateID = (Select Top 1 StateID From DallasTrafficTickets.dbo.TblState Where State = @State)

Set @IDFound = null

SELECT Top 1 @IDFound = T.RecordID, @TicketNum2 = TicketNumber, @Bond_Date_Found = isnull(BondDate,'1/1/1900'), @IsQuoted = T.Clientflag
From DallasTrafficTickets.dbo.tblTicketsArchive T
Inner Join DallasTrafficTickets.dbo.tblTicketsViolationsArchive TV
On T.RecordID = TV.RecordID
Where ((TicketNumber = @Ticket_Number) OR (FirstName = @First_Name and LastName = @Last_Name and Address1 = @Address and datediff(day,DOB, @DOB) = 0
and ZipCode = @Zip and datediff(day,ViolationDate, @Violation_Date) = 0)) and TV.CourtLocation = 3049
AND TV.violationstatusid <> 80	-- Adil 7001 11/14/2009 Do not associate new cases with client, quote and disposed cases

If @IDFound is null
	Begin
		if Len(@Ticket_Number)<5
			Begin
				insert into DallasTrafficTickets.dbo.tblIDGenerator(recdate) values(getdate())
				select @Ticket_Number='ID' + convert(varchar(12),@@identity)
			End

		Insert Into DallasTrafficTickets.dbo.tblTicketsArchive (RecLoadDate, ListDate, CourtDate, ViolationDate, TicketNumber, MidNumber, FirstName, LastName, PhoneNumber, Race, Gender, DOB, Address1, Address2, City, StateID_FK, ZipCode, CourtID, OfficerNumber_FK, GroupID, Flag1, BondFlag, BondDate, DP2, DPC, InsertionLoaderID)   
		Values(@Record_Date, @List_Date, @Court_Date, @Violation_Date, @Ticket_Number, @Ticket_Number, @First_Name, @Last_Name, '0000000000', @Race, @Gender, @DOB, @Address, @Address2, @City, @StateID, @ZIP, 3049, 962528, @GroupID, @AddressStatus, 1, Getdate(), @DP2, @DPC, @LoaderID)
		
		Set @Flag_Inserted = 1

		Set @CurrentRecordID = scope_identity()
	End
Else
	Begin
		Set @CurrentRecordID = @IDFound
		If datediff(day,@Bond_Date_Found, GetDate()) <> 0
		Begin
			Update DallasTrafficTickets.dbo.tblTicketsArchive Set BondDate = Getdate(), BondFlag = 1, ViolationDate = @Violation_Date, GroupID = @GroupID, UpdationLoaderID = @LoaderID where RecordID = @IDFound
			Set @Flag_Inserted = 2
		End
		if Len(@Ticket_Number)<5
			Begin
				Set @Ticket_Number = @TicketNum2
			End
	End

Set @IDFound = null

Select top 1 @IDFound = RowID, @Bond_Date_Found = isnull(ViolationBondDate,'1/1/1900') From DallasTrafficTickets.dbo.tblTicketsViolationsArchive
			Where 
			((CauseNumber = @Ticket_Number)) and CourtLocation = 3049  -- Adil 6580 09/15/2009 Cause Number Included in search clause.
																	  --Afaq 7380 3/25/2010 remove check(AND RecordID = @CurrentRecordID)
If @IDFound is NULL
	BEGIN
		IF @IsQuoted = 1 -- Adil 7380 02/05/2010 if current client is quoted client and coming violation is different from existing then insert new ticket.
			Begin
				if Len(@Ticket_Number)<5
					Begin
						insert into DallasTrafficTickets.dbo.tblIDGenerator(recdate) values(getdate())
						select @Ticket_Number='ID' + convert(varchar(12),@@identity)
					End

				Insert Into DallasTrafficTickets.dbo.tblTicketsArchive (RecLoadDate, ListDate, CourtDate, ViolationDate, TicketNumber, MidNumber, FirstName, LastName, PhoneNumber, Race, Gender, DOB, Address1, Address2, City, StateID_FK, ZipCode, CourtID, OfficerNumber_FK, GroupID, Flag1, BondFlag, BondDate, DP2, DPC, InsertionLoaderID)   
				Values(@Record_Date, @List_Date, @Court_Date, @Violation_Date, @Ticket_Number, @Ticket_Number, @First_Name, @Last_Name, '0000000000', @Race, @Gender, @DOB, @Address, @Address2, @City, @StateID, @ZIP, 3049, 962528, @GroupID, @AddressStatus, 1, Getdate(), @DP2, @DPC, @LoaderID)
				
				Set @Flag_Inserted = 1

				Set @CurrentRecordID = scope_identity()
			End
		Insert Into DallasTrafficTickets.dbo.tblTicketsViolationsArchive (TicketNumber_PK, ViolationNumber_PK, CauseNumber, CourtDate, UpdatedDate, ViolationDescription, ViolationCode, FineAmount, BondAmount, CourtLocation, RecordID, ViolationStatusID, LoaderUpdateDate, InsertionLoaderId, GroupID, ViolationBondDate)
		Values(@Ticket_Number, @ViolationID, @Ticket_Number, @Court_Date, GetDate(), @Violation_Description, @Violation_Code, DallasTrafficTickets.dbo.GetDallasViolationFineamount(@Violation_Description, @ChargeAmount), @BondAmount, 3049, @CurrentRecordID, 116, getdate(), @LoaderID, @GroupID, GetDate())
		Set @Flag_Inserted = 1
	End
Else
	Begin
		If datediff(day,@Bond_Date_Found, GetDate()) <> 0
		Begin
			Update DallasTrafficTickets.dbo.tblTicketsViolationsArchive Set ViolationBondDate = Getdate(), LoaderUpdateDate = GetDate() , UpdationLoaderId = @LoaderID, GroupID = @GroupID where RowID = @IDFound
			Set @Flag_Inserted = 2
		End
	End
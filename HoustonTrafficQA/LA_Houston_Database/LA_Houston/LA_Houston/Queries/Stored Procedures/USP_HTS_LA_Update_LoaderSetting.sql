set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


-- USP_HTS_LA_Update_LoaderSetting 0,0,1,1
CREATE PROCEDURE [dbo].[USP_HTS_LA_Update_LoaderSetting]
	@FTP bit,
	@CourtWebsite bit,
	@DocFeeder bit,
	@emailAccount bit
AS
BEGIN		
	update dbo.Tbl_HTS_LA_LoaderSetting set IsActive=@FTP where SettingID=1
	update dbo.Tbl_HTS_LA_LoaderSetting set IsActive=@CourtWebsite where SettingID=2
	update dbo.Tbl_HTS_LA_LoaderSetting set IsActive=@DocFeeder where SettingID=3
	update dbo.Tbl_HTS_LA_LoaderSetting set IsActive=@emailAccount where SettingID=4

END


grant execute on dbo.USP_HTS_LA_Update_LoaderSetting to dbr_webuser
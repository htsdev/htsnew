﻿USE LA_Houston
GO


/*
Author			: Farrukh Iftikhar
Description		: To Upload Tarrant County Criminal [Booked in],[Bond out],[First setting],[Pass to hire] Cases From Loader Service
Type			: Loader Service
Created date	: 07/04/2012
Task ID			: 10304
Parameters: 
	@Record_Date: Case record/insert date
	@List_Date: Court list date
	@Name_First: Defendant's last name
	@Name_Last: Defendant's first name
	@Race: Defendant's Race
	@Sex: Defendant's Gender
	@Address: Defendant's home address
	@City: Defendant's home city
	@State: Defendant's home state
	@ZIP: Defendant's home zip code
	@DP2: Defendant's barcode part. return by zp4 webservice
	@DPC: Defendant's barcode part. return by zp4 webservice
	@Violation_Description: Violation's title of Defendant
	@Court_Date: Defendant's court date	
	@Court_RoomNo: Defendant's court room number	
	@CID: Defendant's CID
	@GroupID: Data file ID
	@AddressStatus: Address status [y/d/s/n] of defendant
	@Flag_SameAsPrevious: insure wether this case belongs to previous one or not [It helps to cut-off researching the defendant profile]
	@PreviousRecordID: If this case belongs to previous case then previous case ID will be the value of @PreviousRecordID
	@LoaderID: Insure which loader is executing this case.
	@CurrentRecordID: It returns current Case-ID/Record-ID to calling program
	@Flag_Inserted: It returns status wether the record inserted or deleted
*/

CREATE PROCEDURE [dbo].[Upload_Tickets_TCCC_BookedIn_BondOut_FirstSetting_PassToHire]
@Record_Date as datetime,	
@List_Date as smalldatetime,
@Name_First as varchar(50),
@Name_Last as varchar(50),
@Race as varchar(10), 
@Sex as varchar(6),
@Address as varchar(50),
@City as varchar(35),
@State as Varchar(10),
@ZIP as varchar(10),
@DP2 as varchar(2),
@DPC as varchar(1),
@Violation_Description as Varchar(200),
@Court_Date as DATETIME,
@Court_RoomNo as varchar(10),
@CID as varchar(20),
@Flag_Bond AS BIT,
@GroupID as Int,
@AddressStatus VARCHAR(5),
@Flag_SameAsPrevious as bit,
@PreviousRecordID as int,
@LoaderID as int,
@CurrentRecordID int output,
@Flag_Inserted int OUTPUT
AS
set nocount on  

Declare @StateID as Int
Declare @ViolationID as INT
Declare @IDFound as int
Declare @TicketNum2 as varchar(20)
Declare @ChargeAmount as money
Declare @BondAmount as money
Declare @Violation_Code as varchar(30)
DECLARE @IsQuoted AS INT
DECLARE @AddressFound AS VARCHAR(200)
Declare @AddressStatusFound as char(1)
DECLARE @Ticket_Number AS VARCHAR(20)
DECLARE @CourtID AS INT

SET @IsQuoted = 0
Set @Name_First = left(@Name_First,20)
Set @Name_Last = left(@Name_Last,20)
Set @Address = Replace(@Address, '  ', ' ')
Set @Violation_Code = 'XD-none'
Set @Flag_Inserted = 0
Set @ChargeAmount = 0
Set @BondAmount = 0
SET @Ticket_Number = ''
SET @CourtID = 3111


if len(isnull(@DP2,'')) = 1
begin
	if isnull(@DP2,'') <> '0'
	begin
		Set @DP2 = '0' + @DP2
	end
END

IF DATEDIFF(DAY,@Court_Date,'1/1/1900') <> 0
BEGIN

if datepart(dw,@Court_Date) = 7 -- If Saturday then make it Friday (Court date should be in working days)
	begin
		set @Court_Date = @Court_Date - 1
	end
else if datepart(dw,@Court_Date) = 1 -- If Sunday then make it Friday (Court date should be in working days)
	begin
		set @Court_Date = @Court_Date - 2
	end
	
END



Set @ViolationID = null

Select Top 1 @ViolationID = ViolationNumber_PK, @Violation_Code = ViolationCode,
@BondAmount = isnull(BondAmount,0)
FROM DallasTrafficTickets.dbo.TblViolations Where (Description = @Violation_Description and CourtLoc = @CourtID and ViolationType = 11)


If @ViolationID is null
	BEGIN
		Insert Into DallasTrafficTickets.dbo.TblViolations (Description, ViolationCode, ViolationType, CourtLoc, ChargeAmount, BondAmount) 
		Values(@Violation_Description, @Violation_Code, 11, @CourtID, @ChargeAmount, 0)
		Set @ViolationID = scope_identity()
	END
	
Set @StateID = (Select Top 1 StateID From DallasTrafficTickets.dbo.TblState Where State = @State)

If @Flag_SameAsPrevious = 0
	BEGIN
		Set @IDFound = null

		SELECT TOP 1 @IDFound = T.RecordID, @TicketNum2 = TicketNumber , @IsQuoted = T.Clientflag, @AddressFound = ISNULL(T.Address1, 'N/A'), @AddressStatusFound = ISNULL(T.Flag1, 'N')
		FROM DallasTrafficTickets.dbo.tblTicketsArchive T
		INNER JOIN DallasTrafficTickets.dbo.tblTicketsViolationsArchive TV
		ON T.RecordID = TV.RecordID
		WHERE (
		(FirstName = @Name_First and LastName = @Name_Last AND t.Address1 = @Address AND T.City = @City AND t.StateID_FK = @StateID AND t.ZipCode = @ZIP
		and datediff(day,TV.CourtDate, @Court_Date) = 0 )) and TV.CourtLocation = @CourtID

		If @IDFound IS NULL
			Begin				
				INSERT INTO DallasTrafficTickets.dbo.tblIDGenerator(recdate) values(getdate())
				select @Ticket_Number = 'TCCC' + convert(varchar(12),scope_identity())
				
				INSERT INTO DallasTrafficTickets.dbo.tblTicketsArchive (RecLoadDate, ListDate, TicketNumber, MidNumber, FirstName, LastName, Race, Gender, Address1, City, StateID_FK, ZipCode, CourtID, PhoneNumber, GroupID, Flag1, DP2, DPC, InsertionLoaderID, Bondflag, CID)
				Values(@Record_Date, @List_Date, @Ticket_Number, @Ticket_Number, @Name_First, @Name_Last, @Race, @Sex, @Address, @City, @StateID, @ZIP, @CourtID, '0000000000', @GroupID, @AddressStatus, @DP2, @DPC, @LoaderID, @Flag_Bond, @CID)
				
				Set @CurrentRecordID = scope_identity()
			End
		Else
			Begin
				Set @CurrentRecordID = @IDFound
				if Len(@Ticket_Number)<1
					Begin
						Set @Ticket_Number = @TicketNum2
					End
				IF @Address <> @AddressFound and @AddressStatus <> 'N' AND @AddressStatusFound = 'N'
					BEGIN
						-- Update defendant address when not available in non-client DB.
						UPDATE DallasTrafficTickets.dbo.tblTicketsArchive
						SET Address1 = @Address, City = @City, StateID_FK = @StateID, ZipCode = @ZIP, DP2 = @DP2, DPC = @DPC, flag1 = @AddressStatus, UpdationLoaderID = @LoaderID, GroupID = @GroupID, Bondflag = @Flag_Bond
						WHERE RecordID = @CurrentRecordID
						
						Set @Flag_Inserted = 2
					END
			End
	End
Else
	Begin
		Set @CurrentRecordID = @PreviousRecordID
	End

Set @IDFound = null
Set @IDFound = (Select top 1 Isnull(RowID,0) FROM DallasTrafficTickets.dbo.tblTicketsViolationsArchive TV
                WHERE 
				(TV.ViolationNumber_PK = @ViolationID AND TV.RecordID = @CurrentRecordID))

If @IDFound IS NULL
	BEGIN
		IF @IsQuoted = 1
			Begin
				if Len(@Ticket_Number)<1
					Begin
						insert into DallasTrafficTickets.dbo.tblIDGenerator(recdate) values(getdate())
						select @Ticket_Number = 'TCCC' + convert(varchar(12),scope_identity())
					End

				INSERT INTO DallasTrafficTickets.dbo.tblTicketsArchive (RecLoadDate, ListDate, TicketNumber, MidNumber, FirstName, LastName, Race, Gender, PhoneNumber, Address1, City, StateID_FK, ZipCode, CourtID, GroupID, Flag1, DP2, DPC, InsertionLoaderID, Bondflag, CID)
				Values(@Record_Date, @List_Date, @Ticket_Number, @Ticket_Number, @Name_First, @Name_Last, @Race, @Sex, '0000000000', @Address, @City, @StateID, @ZIP, @CourtID, @GroupID, @AddressStatus, @DP2, @DPC, @LoaderID, @Flag_Bond, @CID)

				Set @CurrentRecordID = scope_identity()
			END
		INSERT INTO DallasTrafficTickets.dbo.tblTicketsViolationsArchive (TicketNumber_PK, ViolationNumber_PK, CauseNumber, CourtDate, Courtnumber, InsertDate, UpdatedDate, ViolationDescription, ViolationCode, FineAmount, BondAmount, CourtLocation, RecordID, ViolationStatusID, LoaderUpdateDate, InsertionLoaderId, GroupID)
		Values(@Ticket_Number, @ViolationID, @Ticket_Number, @Court_Date, @Court_RoomNo, Getdate(), Getdate(), @Violation_Description, @Violation_Code, @ChargeAmount, 0, @CourtID, @CurrentRecordID, 116, getdate(), @LoaderID, @GroupID)

		Set @Flag_Inserted = 1
	End







GO
GRANT EXECUTE ON [dbo].[Upload_Tickets_TCCC_BookedIn_BondOut_FirstSetting_PassToHire] TO dbr_webuser
GO
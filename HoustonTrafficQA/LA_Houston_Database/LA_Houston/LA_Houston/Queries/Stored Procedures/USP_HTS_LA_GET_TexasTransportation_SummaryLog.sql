﻿USE [LA_Houston]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTS_LA_GET_TexasTransportation_SummaryLog]    Script Date: 08/28/2013 04:33:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******  
* Created By :	  Farrukh Iftikhar
* Create Date :   09/24/2012 10:13:27 PM 
* Task ID :		  10447
* Business Logic : Get the loader summary log on the basis of RecDate
* List of Parameter :
	@RecDate         : Record Date	
	@LoaderName      : Loader Name
	@TotalRecords    : Total record count
* Column Return      : Loader Information.    
******/

-- [USP_HTS_LA_GET_TexasTransportation_SummaryLog]  '09/25/2012','Person Data Texas Dept of Transportation',689244, 42866
ALTER PROCEDURE [dbo].[USP_HTS_LA_GET_TexasTransportation_SummaryLog] 
	@RecDate DATETIME,
	@LoaderName VARCHAR(100),
	@TotalRecords INT,
	@GroupId INT,
	@LoaderId INT = 0
AS
	DECLARE @DatabaseName VARCHAR(20)
	--Farrukh 11309 07/29/2013 Changed DataBase from "TrafficTickets" to "Crash" and Table(s)
	SET @DatabaseName = 'Crash'
	DECLARE @TableName VARCHAR(50)
	
	IF @LoaderName = 'Crash Data Texas Dept of Transportation'
		BEGIN
			SET @TableName = 'tbl_Crash'   	
		END
	ELSE IF @LoaderName = 'Person Data Texas Dept of Transportation'
		BEGIN
			SET @TableName = 'tbl_Person'   	
		END	
	ELSE IF @LoaderName = 'Unit Data Texas Dept of Transportation'
		BEGIN
			SET @TableName = 'tbl_Unit'   	
		END
	ELSE IF @LoaderName = 'Charges Data Texas Dept of Transportation'
		BEGIN
			SET @TableName = 'tbl_Charges'   	
		END
	ELSE IF @LoaderName = 'CDL Endorsements Data Texas Dept of Transportation'
		BEGIN
			SET @TableName = 'tbl_Endorsements'   	
		END
	ELSE IF @LoaderName = 'DL Restrictions Data Texas Dept of Transportation'
		BEGIN
			SET @TableName = 'DL_Restrictions_File_Spec'   	
		END
	ELSE IF @LoaderName = 'Damages Data Texas Dept of Transportation'
		BEGIN
			SET @TableName = 'tbl_Damages'   	
		END
	ELSE IF @LoaderName = 'Lookup Data Texas Dept of Transportation'
		BEGIN
			SET @TableName = 'Lookup_File_Specification'   	
		END
	ELSE IF @LoaderName = 'Driver Data Texas Dept of Transportation'
		BEGIN
			SET @TableName = 'tbl_Driver'   	
		END
	
	
		
	
	DECLARE @sql NVARCHAR(MAX)
	SET @sql=''
	SET @sql=@sql+N'	
	DECLARE @InsertedCount INT
	DECLARE @TotalCount INT		
	DECLARE @tblStatus TABLE (DESCRIPTION VARCHAR(50),[VALUES] VARCHAR(200))
	DECLARE @EmailBody VARCHAR(MAX)		
	DECLARE @Summmary VARCHAR(MAX)
	DECLARE @CRLF char(2)     
	
	
	SET @CRLF=CHAR(13)+CHAR(10)	
	SET @TotalCount = ' + CONVERT(VARCHAR(20), @TotalRecords) + 
	'SET @InsertedCount = 0
	SET @Summmary=''''
	INSERT INTO @tblStatus VALUES (@CRLF+'' Load Date : '', CONVERT(VARCHAR(12),GETDATE(),101))	
	INSERT INTO @tblStatus VALUES (@CRLF+'' Loader Name : '','''+@LoaderName+''')
	INSERT INTO @tblStatus VALUES (@CRLF+'' Total No. of Records found in Data file : '','+CONVERT(VARCHAR,@TotalRecords)+')	
		
	
	SELECT @InsertedCount = @InsertedCount + CASE 
												WHEN DATEDIFF(DAY, T.InsertDate, '''+CONVERT(VARCHAR,@RecDate)+''') = 0  THEN 1
												ELSE 0
											END
	FROM  '+@DatabaseName+'.dbo.'+@TableName+' T
	WHERE DATEDIFF(DAY, T.InsertDate, '''+CONVERT(VARCHAR,@RecDate)+''') = 0
	AND T.GroupID = '+CONVERT(VARCHAR,@GroupId)+'
    	
	INSERT INTO @tblStatus values(@CRLF+'' No of Uploaded Records : '', convert(varchar(20), @InsertedCount) + '' '')	
	
	INSERT INTO @tblStatus
	SELECT @CRLF+'' File No : '','+CONVERT(VARCHAR,@GroupId)+'
	INSERT INTO @tblStatus 
	SELECT @CRLF+'' File Name : '',thlg.SourceFileName FROM Tbl_HTS_LA_Group thlg 
	WHERE  thlg.GroupID = '+CONVERT(VARCHAR,@GroupId)+'
		
	INSERT INTO @tblStatus VALUES (@CRLF+'' Load By : '',''Loader Service'')
	
	SELECT @Summmary=@Summmary+ISNULL ([DESCRIPTION],'''')+ISNULL([VALUES],'''') FROM @tblStatus 
	
	SELECT @Summmary'
		
	
	EXECUTE sp_executesql @sql


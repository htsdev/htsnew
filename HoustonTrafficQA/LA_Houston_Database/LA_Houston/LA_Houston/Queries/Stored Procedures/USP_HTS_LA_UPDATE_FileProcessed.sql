﻿USE [LA_Houston]
GO
/****** Object:  StoredProcedure [dbo][dbo].[USP_HTS_LA_UPDATE_FileProcessed]  Script Date: 03/26/2013 22:02:04
* Created By :	Rab Nawaz 
* Task ID:		10729
* Date :		03/26/2013
* 
* ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
* Business Logic: This procedure is used to mark the file as processed if the file executed successfully. 
Type : Loader Service

Parameters: 
	@loaderId: ID of loader which need to updated. 
	@groupId: Group Id of processed File
*/
  
CREATE PROCEDURE [dbo].[USP_HTS_LA_UPDATE_FileProcessed]  
@loaderId INT,
@groupId INT
AS   
BEGIN
	UPDATE LA_Houston.dbo.Tbl_HTS_LA_Group SET IsProcessed = 1 WHERE DATEDIFF(DAY,Load_Date, GETDATE())=0 AND LoaderID = @loaderId AND GroupID = @groupId 	
END

GO
GRANT EXECUTE ON [dbo].[USP_HTS_LA_UPDATE_FileProcessed] TO dbr_webuser
GO

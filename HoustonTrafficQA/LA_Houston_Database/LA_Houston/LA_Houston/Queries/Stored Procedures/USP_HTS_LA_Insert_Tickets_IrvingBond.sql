﻿/*
Author: Muhammad Adil Aleem
Business Logic: To Upload Irving warrant Cases From Loader Service
Type : Loader Service
Created date : N/A
Parameters: 
	@Record_Date: Case record/insert date
	@List_Date: Court list date
	@Ticket_Number: Ticket Number.
	@Name_First: Defendant's first name
	@Name_Last: Defendant's last name
	@Name_Middle: Defendant's Middle name
	@DOB: Defendant's Date of Birth
	@Address: Defendant's home address
	@City: Defendant's home city
	@State: Defendant's home state
	@ZIP: Defendant's home zip code
	@DP2: Defendant's barcode part. return by zp4 webservice
	@DPC: Defendant's barcode part. return by zp4 webservice
	@Violation_Date: Violation's Date.
	@Violation_Description: Violation's title of Defendant
	@Violation_Code: Violation's Code.
	@GroupID: Data file ID
	@AddressStatus: Address status [y/d/s/n] of defendant
	@Flag_SameAsPrevious: insure wether this case belongs to previous one or not [It helps to cut-off researching the defendant profile]
	@PreviousRecordID: If this case belongs to previous case then previous case ID will be the value of @PreviousRecordID
	@LoaderID: Insure which loader is executing this case.
	@CurrentRecordID: It returns current Case-ID/Record-ID to calling program
	@Flag_Inserted: It returns status wether the record inserted or deleted
*/

ALTER PROCEDURE [dbo].[USP_HTS_LA_Insert_Tickets_IrvingBond]
@Record_Date datetime,  
@List_Date datetime,  
@Ticket_Number as varchar(20),
@First_Name as varchar(50),  
@Last_Name as varchar(50),  
@Middle_Name as varchar(20),  
@DOB datetime,
@Address as varchar(50),
@City as varchar(35),
@State as Varchar(10),
@ZIP as varchar(10),
@DP2 varchar(2),
@DPC varchar(1),
@Violation_Date as smalldatetime,
@Violation_Description as Varchar(2000),
@Violation_Code as Varchar(50),
@GroupID as Int,
@AddressStatus Char(1),
@Flag_SameAsPrevious as bit,
@PreviousRecordID as int,
@LoaderID as int,
@CurrentRecordID int output,
@Flag_Inserted int Output
AS
set nocount on  

Declare @StateID as Int
Declare @ViolationID as int
Declare @IDFound as int
Declare @TicketNum2 as varchar(20)
Declare @MID_Number as varchar(20)
Declare @ChargeAmount as money
Declare @BondAmount as money
Declare @Court_Date as datetime
Declare @Bond_Date_Found as datetime
DECLARE @IsQuoted AS INT

SET @IsQuoted = 0
Set @First_Name = upper(left(@First_Name,20))
Set @Last_Name = upper(left(@Last_Name,20))
Set @Middle_Name = upper(@Middle_Name)

SET @Ticket_Number = REPLACE(@Ticket_Number, ' ', '')

if len(@Middle_Name)>5
	Begin
		Set @Middle_Name = left(@Middle_Name,1)
	End

Set @Address = Replace(@Address, '  ', '')
Set @Violation_Code = ltrim(rtrim(@Violation_Code))
Set @Violation_Description = ltrim(rtrim(@Violation_Description))
Set @Flag_Inserted = 0
Set @Court_Date = dateadd(day,21,@Violation_Date)

if datepart(dw,@Court_Date) = 7 
	begin
		set @Court_Date = @Court_Date - 1
	end
else if datepart(dw,@Court_Date) = 1 
	begin
		set @Court_Date = @Court_Date - 2
	end

If @ZIP = '00000'
	Begin
		Set @ZIP = ''
	End

If Len(@State)<1
	Begin
		Set @State = 'TX'
	End

Set @ViolationID = null

Select Top 1 @ViolationID = ViolationNumber_PK, @ChargeAmount = isnull(ChargeAmount,100), @BondAmount = isnull(BondAmount,150) From DallasTrafficTickets.dbo.TblViolations Where (Description = @Violation_Description and ViolationCode = @Violation_Code and CourtLoc = 3052 and ViolationType = 10)

If @ViolationID is null
	Begin
		Insert Into DallasTrafficTickets.dbo.TblViolations (Description, Sequenceorder, IndexKey, IndexKeys, ChargeAmount, BondAmount, Violationtype, ChargeonYesflag, ShortDesc, ViolationCode, AdditionalPrice, Attorneyregularprice, Attorneybondprice, CourtLoc)
		Values(@Violation_Description, null, 2, 160, 100, 150, 10, 1, 'None', @Violation_Code, 0, 50, 95, 3052)
		Set @ViolationID = scope_identity()
		Set @ChargeAmount = 100
		Set @BondAmount = 150
	END
SET @BondAmount = CASE WHEN (CHARINDEX ('speed', @Violation_Description, 0)> 0 OR CHARINDEX ('spd', @Violation_Description, 0) > 0) THEN 0 ELSE @BondAmount END -- Adil 01/13/2010 7246 bond amount will be $0 in case of speeding.

Set @StateID = (Select Top 1 StateID From DallasTrafficTickets.dbo.TblState Where State = @State)
If @Flag_SameAsPrevious = 0
	Begin
		Set @IDFound = null

		SELECT Top 1 @IDFound = T.RecordID, @TicketNum2 = TicketNumber, @Bond_Date_Found = BondDate, @IsQuoted = T.Clientflag
		From DallasTrafficTickets.dbo.tblTicketsArchive T
		Inner Join DallasTrafficTickets.dbo.tblTicketsViolationsArchive TV
		On T.RecordID = TV.RecordID
		Where TicketNumber = @Ticket_Number and len(TicketNumber) > 1
		AND TV.violationstatusid <> 80	-- Adil 7001 11/14/2009 Do not associate new cases with client, quote and disposed cases
		
		If @IDFound is null
			Begin
				Set @IDFound = NULL
				SELECT Top 1 @IDFound = T.RecordID, @TicketNum2 = TicketNumber, @Bond_Date_Found = BondDate, @IsQuoted = T.Clientflag
				From DallasTrafficTickets.dbo.tblTicketsArchive T
				Inner Join DallasTrafficTickets.dbo.tblTicketsViolationsArchive TV
				On T.RecordID = TV.RecordID
				Where FirstName = @First_Name and LastName = @Last_Name and Address1 = @Address and ViolationDate = @Violation_Date and ViolationDate is not null
				AND TV.violationstatusid <> 80	-- Adil 7001 11/14/2009 Do not associate new cases with client, quote and disposed cases
			END

		If @IDFound is null
			Begin
				if Len(@Ticket_Number)<1
					Begin
						insert into DallasTrafficTickets.dbo.tblIDGenerator(recdate) values(getdate())
						select @Ticket_Number='ID' + convert(varchar(12),@@identity)
					End

				Insert Into DallasTrafficTickets.dbo.tblTicketsArchive (RecLoadDate, ListDate, CourtDate, ViolationDate, TicketNumber, FirstName, LastName, Initial, DOB, PhoneNumber, Address1, City, StateID_FK, ZipCode, CourtID, OfficerNumber_FK, GroupID, Flag1, BondFlag, BondDate, DP2, DPC, InsertionLoaderID)   
				Values(@Record_Date, @List_Date, @Court_Date, @Violation_Date, @Ticket_Number, @First_Name, @Last_Name, @Middle_Name, @DOB, '0000000000', @Address, @City, @StateID, @ZIP, 3052, 962528, @GroupID, @AddressStatus, 1, Getdate(), @DP2, @DPC, @LoaderID)
				
				Set @Flag_Inserted = 1

				Set @CurrentRecordID = scope_identity()

				Update DallasTrafficTickets.dbo.tblTicketsArchive Set MidNumber = (Select min(TicketNumber) from DallasTrafficTickets.dbo.tblTicketsArchive Where RecordID = @CurrentRecordID) Where RecordID = @CurrentRecordID
			End
		Else
			Begin
				Set @CurrentRecordID = @IDFound
				If convert(datetime, convert(varchar(12), isnull(@Bond_Date_Found,'1/1/1900'), 101)) <> convert(datetime, convert(varchar(12), GetDate(), 101))
				Begin
					Update DallasTrafficTickets.dbo.tblTicketsArchive Set BondDate = Getdate(), BondFlag = 1 , ViolationDate = @Violation_Date, GroupID = @GroupID, UpdationLoaderID = @LoaderID where RecordID = @IDFound
					Set @Flag_Inserted = 2
				End
				if Len(@Ticket_Number)<1
					Begin
						Set @Ticket_Number = @TicketNum2
					End
			End
	End
Else
	Begin
		Set @CurrentRecordID = @PreviousRecordID
	End

Set @IDFound = null

Select top 1 @IDFound = Isnull(RowID,0), @Bond_Date_Found = ViolationBondDate From DallasTrafficTickets.dbo.tblTicketsViolationsArchive
			Where 
			(TicketNumber_PK = @Ticket_Number ) --Afaq 7380 3/25/2010 remove check(AND RecordID = @CurrentRecordID)

If @IDFound is null
	BEGIN
		IF @IsQuoted = 1 -- Adil 7380 02/05/2010 if current client is quoted client and coming violation is different from existing then insert new ticket.
			Begin
				if Len(@Ticket_Number)<1
					Begin
						insert into DallasTrafficTickets.dbo.tblIDGenerator(recdate) values(getdate())
						select @Ticket_Number='ID' + convert(varchar(12),@@identity)
					End

				Insert Into DallasTrafficTickets.dbo.tblTicketsArchive (RecLoadDate, ListDate, CourtDate, ViolationDate, TicketNumber, FirstName, LastName, Initial, DOB, PhoneNumber, Address1, City, StateID_FK, ZipCode, CourtID, OfficerNumber_FK, GroupID, Flag1, BondFlag, BondDate, DP2, DPC, InsertionLoaderID)   
				Values(@Record_Date, @List_Date, @Court_Date, @Violation_Date, @Ticket_Number, @First_Name, @Last_Name, @Middle_Name, @DOB, '0000000000', @Address, @City, @StateID, @ZIP, 3052, 962528, @GroupID, @AddressStatus, 1, Getdate(), @DP2, @DPC, @LoaderID)
				
				Set @Flag_Inserted = 1

				Set @CurrentRecordID = scope_identity()

				Update DallasTrafficTickets.dbo.tblTicketsArchive Set MidNumber = (Select min(TicketNumber) from DallasTrafficTickets.dbo.tblTicketsArchive Where RecordID = @CurrentRecordID) Where RecordID = @CurrentRecordID
			END

		Insert Into DallasTrafficTickets.dbo.tblTicketsViolationsArchive (TicketNumber_PK, ViolationNumber_PK, CauseNumber, CourtDate, UpdatedDate, ViolationDescription, ViolationCode, FineAmount, BondAmount, CourtLocation, RecordID, ViolationStatusID, LoaderUpdateDate, InsertionLoaderId, GroupID, ViolationBondDate)
		Values(@Ticket_Number, @ViolationID, @Ticket_Number, @Court_Date, GetDate(), @Violation_Description, @Violation_Code, @ChargeAmount + 50, @BondAmount, 3052, @CurrentRecordID, 116, getdate(), @LoaderID, @GroupID, GetDate()) -- Adil 7246 01/15/2010 Warrant amount [$50] added into fine amount.
		Set @Flag_Inserted = 1
	End
Else
	Begin
		If convert(datetime, convert(varchar(12), isnull(@Bond_Date_Found,'1/1/1900'), 101)) <> convert(datetime, convert(varchar(12), GetDate(), 101))
		Begin
			Update DallasTrafficTickets.dbo.tblTicketsViolationsArchive Set FineAmount = FineAmount + 50, ViolationBondDate = Getdate(), LoaderUpdateDate = GetDate() , UpdationLoaderId = @LoaderID, GroupID = @GroupID where (RowID = @IDFound) -- Adil 7246 01/15/2010 Warrant amount [$50] added into fine amount.
			Set @Flag_Inserted = 2
		End
	End
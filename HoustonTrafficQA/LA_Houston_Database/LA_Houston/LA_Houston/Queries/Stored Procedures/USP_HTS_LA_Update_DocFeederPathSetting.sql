set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go



CREATE PROCEDURE [dbo].[USP_HTS_LA_Update_DocFeederPathSetting]
	@Path varchar(300),
	@DocCloseTime varchar(100)
AS
BEGIN
	
	SET NOCOUNT ON;

    update Tbl_HTS_LA_ConfigurationSetting set DocFeederPath =@path,DownloadFromDocFeederUntil=@DocCloseTime where ConfigurationID=1
END


grant execute on dbo.USP_HTS_LA_Update_DocFeederPathSetting to dbr_webuser
﻿USE [LA_Houston]
GO
/****** Object:  StoredProcedure [dbo].[Tickets_Upload_Stafford_Events]    Script Date: 12/20/2012 14:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







/*
Author: Muhammad Adil Aleem
Description: To Upload Stafford Event Cases From Loader Service
Type : Loader Service
Created date : Oct-18th-2010
Parameters: 
	@Record_Date: Case record/insert date
	@List_Date: Court list date
	@Ticket_Number: Ticket Number
	@Name_First: Defendant's first name
	@Name_Last: Defendant's last name
	@Name_Middle: Defendant's middle name
	@Race: Defendant's race
	@Sex: Defendant's sex
	@Address: Defendant's home address
	@City: Defendant's home city
	@State: Defendant's home state
	@ZIP: Defendant's home zip code
	@DP2: Defendant's barcode part. return by zp4 webservice
	@DPC: Defendant's barcode part. return by zp4 webservice
	@Violation_Description: offense description
	@Court_Date: offense date
	@Officer_FirstName: Officer's first name
	@Officer_LastName: Officer's last name
	@GroupID: Data file ID
	@AddressStatus: Address status [y/d/s/n] of defendant
	@Flag_SameAsPrevious: insure wether this case belongs to previous one or not [It helps to cut-off researching the defendant profile]
	@PreviousRecordID: If this case belongs to previous case then previous case ID will be the value of @PreviousRecordID
	@LoaderID: Insure which loader is executing this case.
	@CurrentRecordID: It returns current Case-ID/Record-ID to calling program
	@Flag_Inserted: It returns status wether the record inserted or deleted
*/
ALTER PROCEDURE [dbo].[Tickets_Upload_Stafford_Events]
@Ticket_Number as varchar(20),
@Cause_Number as varchar(20),
@Name_First as varchar(50),
@Name_Last as varchar(50),
@Name_Middle as varchar(50),
@PhoneNumber as varchar(20),
@DOB as DATETIME,
@Address as varchar(50),
@City as varchar(35),
@State as Varchar(10),
@ZIP as varchar(10),
@DP2 varchar(2),
@DPC varchar(1),
@Violation_Code as Varchar(10),
@Violation_Description as Varchar(2000),
@Violation_Date as DATETIME,
@Court_Date DATETIME,
@Court_Time VARCHAR(10),
@Officer_FirstName as varchar(50),
@Officer_LastName as varchar(50),
@Status as varchar(10),
@DispositionCode as varchar(10),
@GroupID as Int,
@AddressStatus VARCHAR(5),
@Flag_SameAsPrevious as bit,
@PreviousRecordID as int,
@LoaderID as INT,
@CurrentRecordID INT output,
@Flag_Inserted INT OUTPUT,
@Flag_ClientUpdated INT OUTPUT
AS
set nocount on  

Declare @StateID as Int
Declare @ViolationID as int
Declare @IDFound as int
Declare @TicketNum2 as varchar(20)
Declare @ChargeAmount as money
Declare @BondAmount as money
DECLARE @IsQuoted AS INT
DECLARE @OfficerIDFound INT
DECLARE @OfficerCode VARCHAR(20)
DECLARE @CourtViolationStatusID INT
DECLARE @DayOfMonth VARCHAR(15)
DECLARE @DOBFound DATETIME
DECLARE @PhoneNumberFound VARCHAR(20)
DECLARE @CourtStatusFound INT

SET @IsQuoted = 0
Set @Name_First = left(@Name_First,20)
Set @Name_Last = left(@Name_Last,20)
Set @Name_Middle = left(@Name_Middle,1)
Set @Address = Replace(@Address, '  ', ' ')
Set @Flag_Inserted = 0
Set @ChargeAmount = 0
Set @BondAmount = 0
SET @DayOfMonth = DATENAME(dw, @Court_Date)
SET @CourtViolationStatusID = 0

SET @CourtViolationStatusID =
CASE
-- Adil 8554 11/25/2010 Commented old logic.
--	WHEN @Status = 'OPEN' AND @DispositionCode = '' THEN 146 -- DLQ
--	WHEN @DispositionCode <> '' OR @Status IN ('CLOSED', 'APPEAL') THEN 80 -- Disposed
--	WHEN @Status IN ('DOCKET', 'PENDING') AND @DayOfMonth = 'Monday' AND DAY(@Court_Date) < 8 AND @Court_Time IN ('09:00 AM') THEN 101 -- Pre Trial
--	WHEN @Status IN ('DOCKET', 'PENDING') AND @DayOfMonth IN ('Monday', 'Thursday') AND @Court_Time IN ('09:00 AM', '01:00 PM') THEN 103 -- Judge Trial
--	WHEN @Status IN ('DOCKET', 'PENDING') AND @DayOfMonth IN ('Monday', 'Thursday') AND @Court_Time IN ('08:30 AM') THEN 26 -- Jury Trial
--	WHEN @Status IN ('DOCKET', 'PENDING') AND @DayOfMonth = 'Tuesday' THEN 116 -- Arraignment
--	WHEN @Status IN ('DOCKET') AND @DispositionCode = '' THEN 116 -- Arraignment
--	WHEN @Status IN ('PENDING') AND @DispositionCode = '' THEN 104 -- Waiting
--	ELSE 147

	WHEN @Status = 'OPEN' AND @DispositionCode = '' THEN 146 -- DLQ
	WHEN @DispositionCode <> '' OR @Status IN ('CLOSED', 'APPEAL', 'PENDING') THEN 80 -- Disposed
	WHEN @Status IN ('DOCKET') AND @DayOfMonth = 'Tuesday' THEN 116 -- Arraignment
	ELSE 0
END

if len(isnull(@DP2,'')) = 1
begin
	if isnull(@DP2,'') <> '0'
	begin
		Set @DP2 = '0' + @DP2
	end
end

if isnull(@Officer_FirstName,'') + isnull(@Officer_LastName,'') = ''
	Begin
		Set @Officer_FirstName = 'N/A'
		Set @Officer_LastName = 'N/A'
	End

IF (CHARINDEX ('speed', @Violation_Description, 0)> 0 OR CHARINDEX ('spd', @Violation_Description, 0) > 0)
BEGIN
	SET @ChargeAmount = 248
END

Set @OfficerIDFound = null
Select Top 1 @OfficerIDFound = OfficerNumber_PK from TrafficTickets.dbo.tblOfficer where (isnull(FirstName,'') = @Officer_FirstName AND isnull(LastName,'') = @Officer_LastName)
if @OfficerIDFound is null
	Begin
		Set @OfficerCode = (select max(OfficerNumber_PK) + 1 from TrafficTickets.dbo.tblofficer)
		Insert Into TrafficTickets.dbo.tblOfficer (OfficerNumber_PK, OfficerType, FirstName, LastName, Comments) 
		Values (@OfficerCode, 'N/A', @Officer_FirstName, @Officer_LastName, 'Inserted at ' + convert(varchar(25),getdate(),101))
	End
Else
	Begin
		Set @OfficerCode = @OfficerIDFound
	End

Set @ViolationID = null
Select Top 1 @ViolationID = ViolationNumber_PK, @Violation_Code = ViolationCode, @ChargeAmount = isnull(ChargeAmount,100), @BondAmount = isnull(BondAmount,175) From TrafficTickets.dbo.TblViolations Where (Description = @Violation_Description and CourtLoc = 3067 and ViolationType = 13)

If @ViolationID is null
	BEGIN
		Insert Into TrafficTickets.dbo.TblViolations (Description, ViolationCode, ViolationType, CourtLoc, ChargeAmount, BondAmount) Values(@Violation_Description, @Violation_Code, 13, 3067, 0, 0)
		Set @ViolationID = scope_identity()
	END
	
Set @StateID = (Select Top 1 StateID From TrafficTickets.dbo.TblState Where State = @State)

If @Flag_SameAsPrevious = 0
	BEGIN
		Set @IDFound = null

		SELECT TOP 1 @IDFound = T.RecordID, @TicketNum2 = ISNULL(TicketNumber, @Ticket_Number) , @IsQuoted = T.Clientflag, @PhoneNumberFound = T.PhoneNumber, @DOBFound = T.DOB, @OfficerCode = T.officerNumber_Fk
		FROM TrafficTickets.dbo.tblTicketsArchive T
		INNER JOIN TrafficTickets.dbo.tblTicketsViolationsArchive TV
		ON T.RecordID = TV.RecordID
		WHERE (TV.TicketNumber_PK = @Ticket_Number AND TV.CourtLocation = 3067 AND (TV.violationstatusid <> 80 OR @CourtViolationStatusID = 80))
	
		If @IDFound IS NULL
			Begin
				if Len(@Ticket_Number)<2
					Begin
						insert into TrafficTickets.dbo.tblIDGenerator(recdate) values(getdate())
						select @Ticket_Number='STMC' + convert(varchar(12),scope_identity())
					End

				Insert Into TrafficTickets.dbo.tblTicketsArchive (RecLoadDate, ListDate, TicketNumber, MidNumber, FirstName, LastName, Initial, DOB, PhoneNumber, Address1, City, StateID_FK, ZipCode, OfficerNumber_FK, OfficerName, GroupID, Flag1, DP2, DPC, InsertionLoaderID)
				Values(GETDATE(), GETDATE(), @Ticket_Number, @Ticket_Number, @Name_First, @Name_Last, @Name_Middle, @DOB, @PhoneNumber, @Address, @City, @StateID, @ZIP, @OfficerIDFound, @Officer_LastName + ', ' + @Officer_FirstName, @GroupID, @AddressStatus, @DP2, @DPC, @LoaderID)
				
				Set @CurrentRecordID = scope_identity()
			End
		Else
			Begin
				Set @CurrentRecordID = @IDFound
				if Len(@Ticket_Number) < 1
					Begin
						Set @Ticket_Number = @TicketNum2
					END
				IF @OfficerCode <> @OfficerIDFound OR DATEDIFF(DAY, @DOB, @DOBFound) <> 0 OR @PhoneNumber <> @PhoneNumberFound
				BEGIN
					UPDATE TrafficTickets.dbo.tblTicketsArchive
					SET officerNumber_Fk = @OfficerCode, PhoneNumber = @PhoneNumber, DOB = @DOB, UpdationLoaderID = @LoaderID, GroupID = @GroupID
					WHERE RecordID = @IDFound
					
				END
			End
	End
Else
	Begin
		Set @CurrentRecordID = @PreviousRecordID
	End

Set @IDFound = null

SELECT TOP 1 @IDFound = Isnull(RowID, 0), @CourtStatusFound = TV.violationstatusid From TrafficTickets.dbo.tblTicketsViolationsArchive TV
WHERE (tv.TicketNumber_PK = @Ticket_Number AND (tv.CauseNumber = @Cause_Number OR tv.ViolationNumber_PK = @ViolationID) AND TV.CourtLocation = 3067 AND TV.RecordID = @CurrentRecordID)

If @IDFound is NULL
	BEGIN
		IF @IsQuoted = 1
			Begin
				if Len(@Ticket_Number)<1
					Begin
						insert into DallasTrafficTickets.dbo.tblIDGenerator(recdate) values(getdate())
						select @Ticket_Number='STMC' + convert(varchar(12),scope_identity())
					End

				Insert Into TrafficTickets.dbo.tblTicketsArchive (RecLoadDate, ListDate, TicketNumber, MidNumber, FirstName, LastName, Initial, DOB, PhoneNumber, Address1, City, StateID_FK, ZipCode, CourtID, OfficerNumber_FK, OfficerName, GroupID, Flag1, DP2, DPC, InsertionLoaderID)
				Values(GETDATE(), GETDATE(), @Ticket_Number, @Ticket_Number, @Name_First, @Name_Last, @Name_Middle, @DOB, @PhoneNumber, @Address, @City, @StateID, @ZIP, 3067, @OfficerCode, @Officer_LastName + ', ' + @Officer_FirstName, @GroupID, @AddressStatus, @DP2, @DPC, @LoaderID)

				Set @CurrentRecordID = scope_identity()
			END
		
		Insert Into TrafficTickets.dbo.tblTicketsViolationsArchive (TicketNumber_PK, ViolationNumber_PK, CauseNumber, CourtDate, InsertDate, UpdatedDate, TicketViolationDate, ViolationDescription, ViolationCode, FineAmount, BondAmount, CourtLocation, RecordID, ViolationStatusID, TicketOfficerNumber, LoaderUpdateDate, DLQUpdateDate, InsertionLoaderId, GroupID)
		Values(@Ticket_Number, @ViolationID, @Cause_Number, @Court_Date, Getdate(), Getdate(), @Violation_Date, @Violation_Description, @Violation_Code, 0, 0, 3067, @CurrentRecordID, @CourtViolationStatusID, @OfficerCode, getdate(), CASE WHEN @CourtViolationStatusID = 146 THEN GETDATE() ELSE NULL END, @LoaderID, @GroupID)
		
		Set @Flag_Inserted = 1
	END
	ELSE
		BEGIN
			UPDATE TrafficTickets.dbo.tblTicketsViolationsArchive
			SET violationstatusid = CASE WHEN @CourtViolationStatusID <> 0 THEN @CourtViolationStatusID ELSE violationstatusid END,
			CourtDate = CASE WHEN @CourtViolationStatusID <> 80 THEN @Court_Date ELSE CourtDate END, CauseNumber = @Cause_Number,
			DLQUpdateDate = CASE WHEN @CourtViolationStatusID = 146 AND violationstatusid <> 146 THEN GETDATE() ELSE DLQUpdateDate END,
			TicketOfficerNumber = @OfficerCode, LoaderUpdateDate = GETDATE(), UpdationLoaderId = @LoaderID, updateddate = GETDATE(),
			GroupID = @GroupID
			WHERE RowID = @IDFound
			
			Set @Flag_Inserted = 2
		END

    -- Update Quote/Client Auto Info
    DECLARE @ClientID AS INT
	DECLARE @TicketID AS INT
	DECLARE @Court_Date_Found DATETIME
	DECLARE @Court_Status_Found INT
	DECLARE @Court_Status_Found_Str VARCHAR(10)
	DECLARE @Court_Status_Str VARCHAR(10)
	DECLARE @OfficerID AS INT
	DECLARE @OfficerCodeFound INT
	

	
	--
	
	SET @Flag_ClientUpdated = 0
	SET @ClientID = NULL
	SET @TicketID = NULL
	
	SELECT TOP 1 @TicketID = ttv.TicketID_PK, @ClientID = ttv.TicketsViolationID, @Court_Date_Found = ttv.CourtDate, @Court_Status_Found = ttv.CourtViolationStatusID, @OfficerCodeFound = ISNULL(ttv.TicketOfficerNumber, '')
	FROM TrafficTickets.dbo.tblTicketsViolations ttv
	WHERE ((ttv.RefCaseNumber = @Ticket_Number AND ttv.ViolationNumber_PK = @ViolationID)
	OR (ttv.casenumassignedbycourt = @Cause_Number)) AND ttv.CourtID = 3067

	IF @ClientID IS NOT NULL
	BEGIN
		UPDATE TrafficTickets.dbo.tblTicketsViolations
		SET CourtViolationStatusID = case when @CourtViolationStatusID <> 0 then @CourtViolationStatusID else CourtViolationStatusID end,
		CourtDate = CASE WHEN @CourtViolationStatusID <> 80 THEN @Court_Date ELSE CourtDate END,
		TicketOfficerNumber = @OfficerCode,
		GroupID = @GroupID, UpdatedDate = GETDATE(), loaderupdatedate = GETDATE(), VEmployeeID = 3992
		WHERE TicketsViolationID = @ClientID
		
		SET @Flag_ClientUpdated = 2
		
		-- Adding court violation status code into Auto history of client exactly what we received in data files.
		UPDATE TrafficTickets.dbo.tblTicketsViolationLog
		SET SugarlandEvtDataFileStatus = @Status
		WHERE TicketviolationID = @ClientID
		AND Rowid = (SELECT MAX(ttvl.Rowid)
		               FROM TrafficTickets.dbo.tblTicketsViolationLog ttvl
						WHERE ttvl.TicketviolationID = @ClientID)
		
		--INSERT history note WHEN auto court date time IS changed  by Event loader.
		IF @CourtViolationStatusID <> 80
		BEGIN
			
			--faique 10622 12/20/2012  insertion in history note table 'tblticketsnotes' only when values are not same as previous
			--INSERT INTO TrafficTickets.dbo.tblticketsnotes (ticketid, subject , employeeid)			
			--SELECT @TicketID, 'Auto Court Date Updated - ' + ISNULL(@Ticket_Number, '') + ': From [' + TrafficTickets.dbo.fn_DateFormat(@Court_Date_Found, 5, '/', ':', 1) + '] To [' + TrafficTickets.dbo.fn_DateFormat(@Court_Date, 5, '/', ':', 1) + '] Through Stafford Event Loader', 3992
		
				IF  TrafficTickets.dbo.fn_DateFormat(@Court_Date_Found, 5, '/', ':', 1)  <> TrafficTickets.dbo.fn_DateFormat(@Court_Date, 5, '/', ':', 1)
				BEGIN
					INSERT INTO TrafficTickets.dbo.tblticketsnotes (ticketid, subject , employeeid)
					SELECT @TicketID, 'Auto Court Date Updated - ' + ISNULL(@Ticket_Number, '') + ': From [' + TrafficTickets.dbo.fn_DateFormat(@Court_Date_Found, 5, '/', ':', 1) + '] To [' + TrafficTickets.dbo.fn_DateFormat(@Court_Date, 5, '/', ':', 1) + '] Through Stafford Event Loader', 3992
				END


		END
	
		SELECT @Court_Status_Str = ShortDescription From TrafficTickets.dbo.tblcourtviolationstatus
		WHERE CourtViolationStatusID = @CourtViolationStatusID

		SELECT @Court_Status_Found_Str = ShortDescription From TrafficTickets.dbo.tblcourtviolationstatus
		WHERE CourtViolationStatusID = @Court_Status_Found

		--INSERT history note when auto court status is changed.
		IF @CourtViolationStatusID IN (116, 80, 146) -- Adil 8554 11/25/2010 Update court status only when 'ARR', 'DISPOSED' or 'DLQ'
		BEGIN
						--faique 10622 12/20/2012  insertion in history note table 'tblticketsnotes' only when values are not same as previous
			--INSERT INTO TrafficTickets.dbo.tblticketsnotes (ticketid, subject , employeeid)
			--SELECT @TicketID, 'Auto Court Status Updated - ' + ISNULL(@Ticket_Number, '') + ': From [' + @Court_Status_Found_Str + '] To [' + @Court_Status_Str + '] Through Stafford Event Loader', 3992
			
			IF @Court_Status_Found_Str<>@Court_Status_Str  
			BEGIN
				INSERT INTO TrafficTickets.dbo.tblticketsnotes (ticketid, subject , employeeid)
			SELECT @TicketID, 'Auto Court Status Updated - ' + ISNULL(@Ticket_Number, '') + ': From [' + @Court_Status_Found_Str + '] To [' + @Court_Status_Str + '] Through Stafford Event Loader', 3992	
			END			

				
		END
		
		-- Adil 8397 11/02/2010 Advancing Follow-Up date. -- Adil 8554 11/27/2010 Advancing Follow-Up date has been disabled.
--		IF @Court_Status_Found_Str = 'WAIT' AND @CourtViolationStatusID = 0
--		BEGIN
--			DECLARE @FollowUpDate DATETIME
--			
--			SET @FollowUpDate = GETDATE() + 7
--			
--			IF datepart(dw,@FollowUpDate) = 7 -- If Saturday then make it Friday (Follow-Up date must be in working days)
--				BEGIN
--					SET @FollowUpDate = @FollowUpDate - 1
--				END
--			ELSE IF datepart(dw,@FollowUpDate) = 1 -- If Sunday then make it Friday (Follow-Up date must be in working days)
--				BEGIN
--					SET @FollowUpDate = @FollowUpDate - 2
--				END
--			UPDATE TrafficTickets.dbo.tblTickets SET trafficWaitingFollowUpDate = @FollowUpDate WHERE TicketID_PK = @TicketID
--			
--			INSERT INTO TrafficTickets.dbo.tblticketsnotes (ticketid, subject , employeeid)
--			SELECT @TicketID, ISNULL(@Ticket_Number, '') + ': Traffic waiting follow-up date advanced to 7 days through Stafford Event Loader', 3992
--		END
			
		--faique 10622 12/20/2012  insertion in history note table 'tblticketsnotes' only when values are not same as previous
		
		--	INSERT INTO TrafficTickets.dbo.tblticketsnotes (ticketid, subject , employeeid)
		--SELECT @TicketID, 'Officer Number Updated - ' + ISNULL(@Ticket_Number, '') + ': From [' + CONVERT(VARCHAR(20), @OfficerCodeFound) + '] To [' + CONVERT(VARCHAR(20), @OfficerCode) + '] Through Stafford Event Loader', 3992
		
		IF CONVERT(VARCHAR(20), @OfficerCodeFound)<>CONVERT(VARCHAR(20), @OfficerCode)
		BEGIN
			INSERT INTO TrafficTickets.dbo.tblticketsnotes (ticketid, subject , employeeid)
		SELECT @TicketID, 'Officer Number Updated - ' + ISNULL(@Ticket_Number, '') + ': From [' + CONVERT(VARCHAR(20), @OfficerCodeFound) + '] To [' + CONVERT(VARCHAR(20), @OfficerCode) + '] Through Stafford Event Loader', 3992
		
		END
			
		
		
	END

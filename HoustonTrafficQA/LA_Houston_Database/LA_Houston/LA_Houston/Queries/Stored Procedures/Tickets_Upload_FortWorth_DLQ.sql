﻿set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go




/*
Author: Muhammad Adil Aleem
Description: To Upload Fort Worth DLQ Cases From Loader Service
Type : Loader Service
Created date : Aug-21st-2010
Parameters: 
	@Record_Date: Case record/insert date
	@List_Date: Court list date
	@Ticket_Number: Ticket Number
	@Cause_Number: Cause Number
	@Name_First: Defendant's first name
	@Name_Last: Defendant's last name
	@Name_Middle: Defendant's middle name
	@Address: Defendant's home address
	@City: Defendant's home city
	@State: Defendant's home state
	@ZIP: Defendant's home zip code
	@DP2: Defendant's barcode part. return by zp4 webservice
	@DPC: Defendant's barcode part. return by zp4 webservice
	@Violation_Code: offense Code
	@Violation_Description: offense description
	@Violation_Date: offense date
	@GroupID: Data file ID
	@AddressStatus: Address status [y/d/s/n] of defendant
	@Flag_SameAsPrevious: insure wether this case belongs to previous one or not [It helps to cut-off researching the defendant profile]
	@PreviousRecordID: If this case belongs to previous case then previous case ID will be the value of @PreviousRecordID
	@LoaderID: Insure which loader is executing this case.
	@CurrentRecordID: It returns current Case-ID/Record-ID to calling program
	@Flag_Inserted: It returns status wether the record inserted or deleted
*/
Alter PROCEDURE [dbo].[Tickets_Upload_FortWorth_DLQ]
@Record_Date datetime,
@List_Date as smalldatetime,
@Ticket_Number as varchar(20),
@Cause_Number as varchar(20),
@Name_First as varchar(50),
@Name_Last as varchar(50),
@Name_Middle as varchar(50),
@Address as varchar(50),
@City as varchar(35),
@State as Varchar(10),
@ZIP as varchar(10),
@DP2 varchar(2),
@DPC varchar(1),
@Violation_Code as Varchar(20),
@Violation_Description as Varchar(2000),
@Violation_Date DATETIME,
@Status VARCHAR(20),
@Attorney VARCHAR(50),
@GroupID as Int,
@AddressStatus VARCHAR(5),
@Flag_SameAsPrevious as bit,
@PreviousRecordID as int,
@LoaderID as int,
@CurrentRecordID int output,
@Flag_Inserted int Output
AS
set nocount on  

Declare @StateID as INT
Declare @ViolationID as int
Declare @IDFound as INT
Declare @RowIDFound as int
DECLARE @IsQuoted AS INT
DECLARE @Court_Date DATETIME
DECLARE @DLQUpdateDate_Found DATETIME

SET @IsQuoted = 0
Set @Name_First = left(@Name_First,20)
Set @Name_Last = left(@Name_Last,20)
Set @Name_Middle = left(@Name_Middle,1)
Set @Address = Replace(@Address, '  ', '')
Set @Flag_Inserted = 0

if len(isnull(@DP2,'')) = 1
begin
	if isnull(@DP2,'') <> '0'
	begin
		Set @DP2 = '0' + @DP2
	end
end

SET @Court_Date = @Violation_Date + 11

Set @ViolationID = null
Select Top 1 @ViolationID = ViolationNumber_PK, @Violation_Code = ViolationCode From DallasTrafficTickets.dbo.TblViolations Where ((ViolationCode = @Violation_Code OR Description = @Violation_Description) and CourtLoc = 3059 and ViolationType = 4)

If @ViolationID is null
	BEGIN
		Insert Into DallasTrafficTickets.dbo.TblViolations (Description, ViolationCode, ViolationType, CourtLoc, ChargeAmount, BondAmount) 
		Values(@Violation_Description, @Violation_Code, 4, 3059, 0, 0)
		Set @ViolationID = scope_identity()
	END
	
Set @StateID = (Select Top 1 StateID From DallasTrafficTickets.dbo.TblState Where State = @State)

If @Flag_SameAsPrevious = 0
	BEGIN
		Set @IDFound = null

		SELECT TOP 1 @IDFound = T.RecordID, @IsQuoted = T.Clientflag
		FROM DallasTrafficTickets.dbo.tblTicketsArchive T
		INNER JOIN DallasTrafficTickets.dbo.tblTicketsViolationsArchive TV
		ON T.RecordID = TV.RecordID
		WHERE (TV.TicketNumber_PK = @Ticket_Number AND TV.CourtLocation = 3059 AND TV.violationstatusid <> 80)
	
		If @IDFound IS NULL
			Begin
				if Len(@Ticket_Number)<2
					Begin
						insert into DallasTrafficTickets.dbo.tblIDGenerator(recdate) values(getdate())
						select @Ticket_Number='FWMC' + convert(varchar(12),scope_identity())
					End

				Insert Into DallasTrafficTickets.dbo.tblTicketsArchive (RecLoadDate, ListDate, TicketNumber, MidNumber, FirstName, LastName, Initial, PhoneNumber, Address1, City, StateID_FK, ZipCode, CourtID, GroupID, Flag1, DP2, DPC, InsertionLoaderID)
				Values(@Record_Date, @List_Date, @Ticket_Number, @Ticket_Number, @Name_First, @Name_Last, @Name_Middle, '0000000000', @Address, @City, @StateID, @ZIP, 3059, @GroupID, @AddressStatus, @DP2, @DPC, @LoaderID)
				
				Set @CurrentRecordID = scope_identity()
			End
		Else
			BEGIN
				Set @CurrentRecordID = @IDFound
				IF @Status = 'TBS' OR @Attorney <> ''
					BEGIN
						UPDATE DallasTrafficTickets.dbo.tblTicketsArchive SET donotmailflag = 1, DoNotMailUpdateDate = GETDATE(), GroupID = @GroupID, UpdationLoaderID = @LoaderID
						WHERE RecordID = @IDFound
						SET @Flag_Inserted = 2
					END
				
			END
	End
Else
	Begin
		Set @CurrentRecordID = @PreviousRecordID
	End

Set @IDFound = NULL
Set @RowIDFound = null
	
SELECT TOP 1 @RowIDFound = Isnull(TV.RowID,0), @IDFound = ISNULL(TV.RecordID, 0), @DLQUpdateDate_Found = isnull(TV.DLQUpdateDate,'1/1/1900')
FROM DallasTrafficTickets.dbo.tblTicketsViolationsArchive TV
WHERE (TV.TicketNumber_PK = @Ticket_Number AND (TV.CauseNumber = @Cause_Number OR TV.ViolationNumber_PK = @ViolationID) AND CourtLocation = 3059 AND RecordID = @CurrentRecordID)

If @RowIDFound is null
	BEGIN
		IF @IsQuoted = 1
			Begin
				if Len(@Ticket_Number)<1
					Begin
						insert into DallasTrafficTickets.dbo.tblIDGenerator(recdate) values(getdate())
						select @Ticket_Number='FWMC' + convert(varchar(12),scope_identity())
					End

				Insert Into DallasTrafficTickets.dbo.tblTicketsArchive (RecLoadDate, ListDate, TicketNumber, MidNumber, FirstName, LastName, Initial, PhoneNumber, Address1, City, StateID_FK, ZipCode, CourtID, GroupID, Flag1, DP2, DPC, InsertionLoaderID, donotmailflag, DoNotMailUpdateDate)
				Values(@Record_Date, @List_Date, @Ticket_Number, @Ticket_Number, @Name_First, @Name_Last, @Name_Middle, '0000000000', @Address, @City, @StateID, @ZIP, 3059, @GroupID, @AddressStatus, @DP2, @DPC, @LoaderID, CASE WHEN @Status = 'TBS' THEN 1 ELSE 0 END, CASE WHEN @Status = 'TBS' THEN GETDATE() ELSE NULL END )

				Set @CurrentRecordID = scope_identity()
			END
		
		INSERT INTO DallasTrafficTickets.dbo.tblTicketsViolationsArchive (TicketNumber_PK, ViolationNumber_PK, CauseNumber, CourtDate, InsertDate, UpdatedDate, TicketViolationDate, DLQUpdateDate, ViolationDescription, ViolationCode, FineAmount, BondAmount, CourtLocation, RecordID, ViolationStatusID, LoaderUpdateDate, InsertionLoaderId, GroupID)
		VALUES (@Ticket_Number, @ViolationID, @Cause_Number, @Court_Date, Getdate(), Getdate(), @Violation_Date, GETDATE(), @Violation_Description, @Violation_Code, 0, 0, 3059, @CurrentRecordID, 116, getdate(), @LoaderID, @GroupID)
		
		SET @Flag_Inserted = 1
	END
ELSE
BEGIN
	IF DATEDIFF(DAY, @DLQUpdateDate_Found, GETDATE()) > 0
	BEGIN
		UPDATE DallasTrafficTickets.dbo.tblTicketsViolationsArchive
		SET DLQUpdateDate = GETDATE(), UpdationLoaderId = @LoaderID, GroupID = @GroupID, LoaderUpdateDate = GETDATE()
		WHERE RowID = @RowIDFound
		
		SET @Flag_Inserted = 2
	END
END

GO

GRANT EXECUTE ON [dbo].[Tickets_Upload_FortWorth_DLQ] TO dbr_webuser
GO
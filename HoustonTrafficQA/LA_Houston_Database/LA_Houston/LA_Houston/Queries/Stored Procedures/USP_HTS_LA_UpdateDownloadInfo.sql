set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

create PROCEDURE [dbo].[USP_HTS_LA_UpdateDownloadInfo]
@FileCode varchar(10)
AS
BEGIN
 Update tbl_HTS_LA_DatafileURL Set LastDownloadDate = getdate() Where FileCode = @FileCode
END


﻿


/*
Author: Muhammad Nasir
Description: To Upload SugarLand From Loader Service
Type : Loader Service
Created date : 12/18/2008
Parameters: 
	@Record_Date: Case record/insert date
	@List_Date: Court list date
	@Name_Last: Defendant's last name
	@Name_First: Defendant's first name
	@Address: Defendant's home address
	@City: Defendant's home city
	@State: Defendant's home state
	@ZIP: Defendant's home zip code
	@DP2: Defendant's barcode part. return by zp4 webservice
	@DPC: Defendant's barcode part. return by zp4 webservice
	@Court_Date: Defendant's court date	
	@Violation_Description: Violation's title of Defendant
	@GroupID: Data file ID
	@AddressStatus: Address status [y/d/s/n] of defendant
	@Flag_SameAsPrevious: insure wether this case belongs to previous one or not [It helps to cut-off researching the defendant profile]
	@PreviousRecordID: If this case belongs to previous case then previous case ID will be the value of @PreviousRecordID
	@LoaderID: Insure which loader is executing this case.
	@CurrentRecordID: It returns current Case-ID/Record-ID to calling program
	@Flag_Inserted: It returns status wether the record inserted or deleted
*/
--Nasir 4827 12/18/2008 Sugarland Loader



ALTER PROCEDURE [dbo].[USP_HTP_LA_Insert_Tickets_SugarlandArraignment]
@Record_Date datetime,  
@List_Date as smalldatetime,
@Ticket_Number as varchar(20),
@Name_First as varchar(50),  
@Name_Last as varchar(50),  
@Address as varchar(50),  
@City as varchar(35),  
@State as Varchar(10),
@ZIP as varchar(10),  
@DP2 varchar(2),
@DPC varchar(1),
@Court_Date as smalldatetime,
@Violation_Description as Varchar(2000),
@AddressStatus Char(1),
@Flag_SameAsPrevious as bit,
@PreviousRecordID as int,
@LoaderID as int,
@GroupID AS INT, 
@CurrentRecordID int output,
@Flag_Inserted int Output
AS
set nocount on  

Declare @StateID as Int
Declare @ViolationID as int
Declare @IDFound as INT
Declare @DLQUpdateDateFound as DATETIME
Declare @ViolationStatusFound as INT
Declare @TicketNum2 as varchar(20)
Declare @ChargeAmount as MONEY
Declare @BondAmount as MONEY
DECLARE @ViolationStatus AS INT
DECLARE @IsQuoted AS INT

SET @IsQuoted = 0
Set @Name_First = LTRIM(RTRIM(upper(left(@Name_First,20))))
Set @Name_Last = LTRIM(RTRIM(upper(left(@Name_Last,20))))

Set @Address = Replace(@Address, '  ', '')
Set @Ticket_Number = ltrim(rtrim(@Ticket_Number))
Set @Violation_Description = ltrim(rtrim(@Violation_Description))
Set @Flag_Inserted = 0
Set @ChargeAmount = 0
Set @BondAmount = 0
If @ZIP = '00000'
	Begin
		Set @ZIP = ''
	END

IF DATEDIFF(DAY,GETDATE(), @Court_Date) < 0 -- Adil 6984 11/26/2009 Past appearances DLQ cases
	BEGIN
		SET @ViolationStatus = 146	
	END
ELSE
	BEGIN
		SET @ViolationStatus = 116
	END

Set @ViolationID = null
Select Top 1 @ViolationID = ViolationNumber_PK, @ChargeAmount = isnull(ChargeAmount,0), @BondAmount = isnull(BondAmount,0) From TrafficTickets.dbo.TblViolations Where (Description = @Violation_Description  and CourtLoc = 3060 and ViolationType = 11)

If @ViolationID is null
	BEGIN
		Insert Into TrafficTickets.dbo.TblViolations (Description, Sequenceorder, IndexKey, IndexKeys, ChargeAmount, BondAmount, Violationtype, ChargeonYesflag, ShortDesc, ViolationCode, AdditionalPrice, CourtLoc)
		Values(@Violation_Description, null, 2, 100, 100, 140, 11, 1, 'None', 'none', 0, 3060)
		Set @ChargeAmount = 100
		Set @BondAmount = 200
		Set @ViolationID = scope_identity()
	End

Set @StateID = (Select Top 1 StateID From TrafficTickets.dbo.TblState Where State = @State)

If @Flag_SameAsPrevious = 0
	BEGIN
		Set @IDFound = null

		SELECT Top 1 @IDFound = T.RecordID, @TicketNum2 = TicketNumber, @DLQUpdateDateFound = TV.DLQUpdateDate, @ViolationStatusFound = TV.violationstatusid, @IsQuoted = T.Clientflag
		From TrafficTickets.dbo.tblTicketsArchive T
		Inner Join TrafficTickets.dbo.tblTicketsViolationsArchive TV
		On T.RecordID = TV.RecordID
		Where 
		((TicketNumber = @Ticket_Number and len(TicketNumber) > 1)
		OR
		(FirstName = @Name_First and LastName = @Name_Last and Address1 = @Address
		and ZipCode = @Zip and datediff(day,TV.CourtDate, @Court_Date) = 0 )) and TV.CourtLocation = 3060
		AND TV.violationstatusid <> 80	-- Adil 7001 11/14/2009 Do not associate new cases with client, quote and disposed cases

		If @IDFound is null
			Begin
				if Len(@Ticket_Number)<1
					Begin
						insert into TrafficTickets.dbo.tblIDGenerator(recdate) values(getdate())
						select @Ticket_Number='SMC' + convert(varchar(12),scope_identity())
					END
					
				Insert Into TrafficTickets.dbo.tblTicketsArchive (RecLoadDate, ListDate, CourtDate, TicketNumber, MidNumber, FirstName, LastName, PhoneNumber, Address1, City, StateID_FK, ZipCode, CourtID, OfficerNumber_FK, OfficerName, Flag1, DP2, DPC, InsertionLoaderID,GroupID)   
				Values(@Record_Date, @List_Date, @Court_Date, @Ticket_Number, @Ticket_Number, @Name_First, @Name_Last, '0000000000', @Address, @City, @StateID, @ZIP, 3060, 962528, 'N/A', @AddressStatus, @DP2, @DPC, @LoaderID,@GroupID)

				Set @CurrentRecordID = scope_identity()
			End
		Else
			BEGIN
				Set @CurrentRecordID = @IDFound
				if Len(@Ticket_Number)<1
					Begin
						Set @Ticket_Number = @TicketNum2
					End
			End
	End
Else
	Begin
		Set @CurrentRecordID = @PreviousRecordID
	End

Set @IDFound = null
Set @IDFound = (Select top 1 RowID From TrafficTickets.dbo.tblTicketsViolationsArchive
			Where 
			(TicketNumber_PK = @Ticket_Number and CourtLocation = 3060 )) --Afaq 7380 3/25/2010

If @IDFound is null
	BEGIN
		IF @IsQuoted = 1 -- Adil 7380 02/05/2010 if current client is quoted client and coming violation is different from existing then insert new ticket.
			BEGIN
				if Len(@Ticket_Number)<1
					Begin
						insert into TrafficTickets.dbo.tblIDGenerator(recdate) values(getdate())
						select @Ticket_Number='SMC' + convert(varchar(12),scope_identity())
					END
					
				Insert Into TrafficTickets.dbo.tblTicketsArchive (RecLoadDate, ListDate, CourtDate, TicketNumber, MidNumber, FirstName, LastName, PhoneNumber, Address1, City, StateID_FK, ZipCode, CourtID, OfficerNumber_FK, OfficerName, Flag1, DP2, DPC, InsertionLoaderID,GroupID)   
				Values(@Record_Date, @List_Date, @Court_Date, @Ticket_Number, @Ticket_Number, @Name_First, @Name_Last, '0000000000', @Address, @City, @StateID, @ZIP, 3060, 962528, 'N/A', @AddressStatus, @DP2, @DPC, @LoaderID,@GroupID)

				Set @CurrentRecordID = scope_identity()
			END

		Insert Into TrafficTickets.dbo.tblTicketsViolationsArchive (TicketNumber_PK, ViolationNumber_PK, CauseNumber, CourtDate, InsertDate, ViolationDescription, ViolationCode, FineAmount, BondAmount, CourtLocation, RecordID, ViolationStatusID, DLQUpdateDate, InsertionLoaderId, GroupID)
		Values(@Ticket_Number, @ViolationID, @Ticket_Number, @Court_Date, Getdate(), @Violation_Description, 'None', @ChargeAmount, @BondAmount, 3060, @CurrentRecordID, @ViolationStatus, (CASE WHEN @ViolationStatus = 146 THEN GETDATE() ELSE @DLQUpdateDateFound END), @LoaderID, @GroupID)
		Set @Flag_Inserted = 1
	END
ELSE IF @IDFound IS NOT NULL AND @ViolationStatusFound = 116 AND @ViolationStatus = 146 -- Adil 6984 11/26/2009 Past appearances DLQ cases
	BEGIN
		UPDATE TrafficTickets.dbo.tblTicketsViolationsArchive SET violationstatusid = 146, DLQUpdateDate = GETDATE(), UpdationLoaderId = @LoaderID, GroupID = @GroupID, updateddate = GETDATE() WHERE RowID = @IDFound
		Set @Flag_Inserted = 2
	END
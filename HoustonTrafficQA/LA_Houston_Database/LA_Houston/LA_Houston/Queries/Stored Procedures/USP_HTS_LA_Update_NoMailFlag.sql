﻿USE [LA_Houston]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTS_LA_UPDATE_NOMAILFLAG]    Script Date: 04/13/2012 20:24:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
* Business Logic: This procedure is used to mark the records as do not mail which is uploaded today.
Type : Loader Service

Parameters: 
	@LoadDate: Loader upload date
	@CourtID: Id of the court location
	@Database: 1 for Houston, 2 for Dallas, 3 for Jims
*/
--USP_HTS_LA_Update_NoMailFlag '08/18/2011','3049', 2
  
ALTER PROCEDURE [dbo].[USP_HTS_LA_UPDATE_NOMAILFLAG]  
@LoadDate datetime,  
@CourtID varchar(10),
@Database	int=1	 --  1=Traffictickets,2=DallasTraffictickets,3=JIMS
AS   
Declare @JPCourtIDs as varchar(100)

UPDATE Tbl_HTS_LA_Group SET IsProcessed = 1 WHERE DATEDIFF(DAY,Load_Date, GETDATE())=0

If @CourtID = 'JP'
	Begin
		Set @JPCourtIDs = '*'
		Select @JPCourtIDs = @JPCourtIDs + ',' + convert(varchar(10),CourtID) from dallastraffictickets.dbo.TblCourts where CourtCategoryNum = 11
		Set @JPCourtIDs = replace(@JPCourtIDs,'*,','')
		Set @CourtID = @JPCourtIDs
	End

--For Houston  
if @database=1
	update traffictickets.dbo.tblticketsarchive         
	 set DoNotMailFlag =1        
	WHERE         
	 datediff(day,RecLoadDate,@LoadDate) =0         
	 and        
	   exists         
		(select distinct firstname,        
		 lastname,        
		 address,    
	  zip         
	             
		from traffictickets.dbo.tbl_hts_donotmail_records         
		WHERE         
	     -- Abbas Qamar 9726 11-02-2011 Removing First name Filter 
--		 traffictickets.dbo.tblticketsarchive.firstname = traffictickets.dbo.tbl_hts_donotmail_records.FirstName         
--		 AND        
		 traffictickets.dbo.tblticketsarchive.lastname = traffictickets.dbo.tbl_hts_donotmail_records.LastName         
		 AND        
		 traffictickets.dbo.tblticketsarchive.address1 = traffictickets.dbo.tbl_hts_donotmail_records.Address      
	  AND  -- Sabir Khan 10200 04/14/2012 Need to check first five charachter of zip code.     
	  LEFT(LTRIM(RTRIM(traffictickets.dbo.tblticketsarchive.ZipCode)),5) = LEFT(LTRIM(RTRIM(traffictickets.dbo.tbl_hts_donotmail_records.Zip)),5)    	      
	 AND  
	 traffictickets.dbo.tblticketsarchive.CourtID in (@CourtID)
	 AND   
	 traffictickets.dbo.tblticketsarchive.DoNotMailFlag = 0  
	 )    

--Abbas Qamar 9726 11-02-2011 Check Clients on Zip and address base.

update traffictickets.dbo.tblticketsarchive         
	 set DoNotMailFlag =1        
	WHERE         
	 datediff(day,RecLoadDate,@LoadDate) =0         
	 and        
	   exists         
		(select distinct firstname,        
		 lastname,        
		 address,    
	  zip         
	             
		from traffictickets.dbo.tbl_hts_donotmail_records         
		WHERE         
		 traffictickets.dbo.tblticketsarchive.address1 = traffictickets.dbo.tbl_hts_donotmail_records.Address      
	     AND  -- Sabir Khan 10200 04/14/2012 Need to check first five charachter of zip code.         
	     LEFT(LTRIM(RTRIM(traffictickets.dbo.tblticketsarchive.ZipCode)),5) = LEFT(LTRIM(RTRIM(traffictickets.dbo.tbl_hts_donotmail_records.Zip)),5)    	      
  	     AND -- Sabir Khan 10200 04/14/2012 The last name should be null when any address is added from By addressess interface.       
		 Len(isnull(traffictickets.dbo.tbl_hts_donotmail_records.LastName,'')) = 0  
	     AND   
	     traffictickets.dbo.tblticketsarchive.DoNotMailFlag = 0  
	 )    



--For Dallas  
if @database=2
	update dallastraffictickets.dbo.tblticketsarchive         
	 set DoNotMailFlag =1        
	WHERE         
	 datediff(day,RecLoadDate,@LoadDate) =0         
	 and        
	   exists         
		(select distinct firstname,        
		 lastname,        
		 address,    
	  zip         
	             
		from traffictickets.dbo.tbl_hts_donotmail_records         
		WHERE         
	   -- Abbas Qamar 9726 11-02-2011 Removing First name Filter   
--		 dallastraffictickets.dbo.tblticketsarchive.firstname = traffictickets.dbo.tbl_hts_donotmail_records.FirstName         
--		 AND        
		 dallastraffictickets.dbo.tblticketsarchive.lastname = traffictickets.dbo.tbl_hts_donotmail_records.LastName         
		 AND        
		 dallastraffictickets.dbo.tblticketsarchive.address1 = traffictickets.dbo.tbl_hts_donotmail_records.Address      
	  AND      -- Sabir Khan 10200 04/14/2012 Need to check first five charachter of zip code.     
	  LEFT(LTRIM(RTRIM(dallastraffictickets.dbo.tblticketsarchive.ZipCode)),5) = LEFT(LTRIM(RTRIM(traffictickets.dbo.tbl_hts_donotmail_records.Zip)),5)    	      
	 AND  
	 convert(varchar(100),dallastraffictickets.dbo.tblticketsarchive.CourtID) in ( @CourtID )
	 AND   
	 dallastraffictickets.dbo.tblticketsarchive.DoNotMailFlag = 0  
	 )    
	 
	 
	 
	 --Abbas Qamar 9726 11-02-2011 Check Clients on Zip and address base.

update dallastraffictickets.dbo.tblticketsarchive         
	 set DoNotMailFlag =1        
	WHERE         
	 datediff(day,RecLoadDate,@LoadDate) =0         
	 and        
	   exists         
		(select distinct firstname,        
		 lastname,        
		 address,    
	  zip         
	             
		from traffictickets.dbo.tbl_hts_donotmail_records         
		WHERE         
		 dallastraffictickets.dbo.tblticketsarchive.address1 = traffictickets.dbo.tbl_hts_donotmail_records.Address      
	     AND  -- Sabir Khan 10200 04/14/2012 Need to check first five charachter of zip code.     
	     LEFT(LTRIM(RTRIM(dallastraffictickets.dbo.tblticketsarchive.ZipCode)),5) = LEFT(LTRIM(RTRIM(traffictickets.dbo.tbl_hts_donotmail_records.Zip)),5)    	      
  	     AND -- Sabir Khan 10200 04/14/2012 The last name should be null when any address is added from By addressess interface.       
		 Len(isnull(traffictickets.dbo.tbl_hts_donotmail_records.LastName,'')) = 0
		 AND   
	     dallastraffictickets.dbo.tblticketsarchive.DoNotMailFlag = 0  
	 )    


--For Jims
if  @database=3 
	update jims.dbo.CriminalCases         
	 set DoNotMailFlag =1        
	WHERE         
	 datediff(day,RecDate,@LoadDate) =0         
	 and        
	   exists         
		(select distinct firstname,        
		 lastname,        
		 address,    
	  zip         
	             
		from traffictickets.dbo.tbl_hts_donotmail_records         
		WHERE         
	             
--		 jims.dbo.CriminalCases.firstname = traffictickets.dbo.tbl_hts_donotmail_records.FirstName         
--		 AND        
		 jims.dbo.CriminalCases.lastname = traffictickets.dbo.tbl_hts_donotmail_records.LastName         
		 AND        
		 jims.dbo.CriminalCases.address = traffictickets.dbo.tbl_hts_donotmail_records.Address      
	  AND  -- Sabir Khan 10200 04/14/2012 Need to check first five charachter of zip code.     
	  LEFT(LTRIM(RTRIM(jims.dbo.CriminalCases.Zip)),5) = LEFT(LTRIM(RTRIM(traffictickets.dbo.tbl_hts_donotmail_records.Zip)),5)    	           
	     
	 AND  
	 jims.dbo.CriminalCases.DoNotMailFlag = 0  
	 )    

--Abbas Qamar 9726 11-02-2011 Check Clients on Zip and address base.

update jims.dbo.CriminalCases 
	 set DoNotMailFlag =1        
	WHERE         
	 datediff(day,RecDate,@LoadDate) =0         
	 and        
	   exists         
		(select distinct firstname,        
		 lastname,        
		 address,    
	  zip         
	             
		from traffictickets.dbo.tbl_hts_donotmail_records         
		WHERE         
		jims.dbo.CriminalCases.address = traffictickets.dbo.tbl_hts_donotmail_records.Address         
	     AND -- Sabir Khan 10200 04/14/2012 Need to check first five charachter of zip code.     
	     LEFT(LTRIM(RTRIM(jims.dbo.CriminalCases.Zip)),5) = LEFT(LTRIM(RTRIM(traffictickets.dbo.tbl_hts_donotmail_records.Zip)),5)    	                	    
		AND -- Sabir Khan 10200 04/14/2012 The last name should be null when any address is added from By addressess interface.       
		 Len(isnull(traffictickets.dbo.tbl_hts_donotmail_records.LastName,'')) = 0  			
		AND   
	    jims.dbo.CriminalCases.DoNotMailFlag = 0
	 )    





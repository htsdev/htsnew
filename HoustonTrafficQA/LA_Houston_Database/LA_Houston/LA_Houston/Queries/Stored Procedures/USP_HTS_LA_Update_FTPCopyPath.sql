set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go



CREATE PROCEDURE [dbo].[USP_HTS_LA_Update_FTPCopyPath]
@FTPCopySite varchar(300),
@FTPCopyUser varchar(300),
@FTPCopyPassword varchar(300)
AS
BEGIN
	update Tbl_HTS_LA_ConfigurationSetting set FTPCopysite=@FTPCopySite,FTPCopyUser=@FTPCopyUser,FTPCopyPassword=@FTPCopyPassword where ConfigurationId=1
END


grant execute on dbo.USP_HTS_LA_Update_FTPCopyPath to dbr_webuser
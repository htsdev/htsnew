﻿set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

/*
Author: Muhammad Adil Aleem
Description  : To get inserted defendants for address verification
Type         : Loader Service
Created date : Jul-17th-2009
Business Logic: This Procedure will get all the inserted defendants of specified GroupID for their addresses.
Parameters: 
	@GroupID: GroupID/File ID of data file.
*/

-- USP_HTS_LA_FTP_GetInsertedDefendants_For_ZP4 387
ALTER procedure [dbo].[USP_HTS_LA_FTP_GetInsertedDefendants_For_ZP4]
@GroupID as int
as

Select RecordID, Address1 as [Address Part I], Address2 as [Address Part II], City, St.State, ZipCode as [Zip Code] , -- Adil 7486 03/05/10 [Address Part II] added.
'' as [Address Status], 0 as [DP2], 0 as [DPC] 
From DallasTrafficTickets.dbo.tblTicketsArchive T 
INNER JOIN DallasTrafficTickets.dbo.TblState St
on T.StateID_FK = St.StateID
Where T.GroupID = @GroupID And datediff(day, T.RecLoadDate, getdate()) = 0


-- grant execute on dbo.USP_HTS_LA_FTP_GetInsertedDefendants_For_ZP4 to dbr_webuser


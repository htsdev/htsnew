set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


CREATE PROCEDURE [dbo].[USP_HTS_LA_Get_DownloadingTime]
	
AS
BEGIN
	select DownloadFromCourtWebSiteUntil,DownloadFromFTPUntil,DownloadFromEmailUntil,DownloadFromDocFeederUntil from Tbl_HTS_LA_ConfigurationSetting where ConfigurationId=1
END


grant execute on dbo.USP_HTS_LA_Get_DownloadingTime to dbr_webuser
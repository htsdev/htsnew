﻿ USE LA_Houston
GO

/*
Author: Farrukh Iftikhar
Task ID: 10438
Description: To Upload HCCC Misdemeanor JIMS-009 Cases From Loader Service
Type : Loader Service
Created date : N/A
Parameters: 
	@List_Date: Court list date
	@Ticket_Number: Ticket Number
	@Name_Last: Defendant's last name
	@Name_First: Defendant's first name
	@Name_Middle: Defendant's middle name
	@Court_Date: Court Date
	@Violation_Description: offense description
	@BondAmount: Violation Bond Amount.
	@Court_Number: Court Number.
	@GroupID: Data file ID
	@Flag_SameAsPrevious: insure wether this case belongs to previous one or not [It helps to cut-off researching the defendant profile]
	@PreviousRecordID: If this case belongs to previous case then previous case ID will be the value of @PreviousRecordID
	@PreviousTicketNumber: If this case belongs to previous case then previous record's Ticket Number will be passed
	@LoaderID: Insure which loader is executing this case.
	@CurrentRecordID: It returns current Case-ID/Record-ID to calling program
*/

CREATE PROCEDURE [dbo].[USP_HTS_LA_Update_Tickets_HCCC_JIMS009]
	@List_Date AS DATETIME,
	@Ticket_Number AS VARCHAR(20),
	@Name_Last AS VARCHAR(50), 
	@Name_First AS VARCHAR(50), 
	@Name_Middle AS VARCHAR(50), 
	@Court_Date AS DATETIME,
	@Violation_Description AS VARCHAR(500),
	@BondAmount AS MONEY,
	@Court_Number VARCHAR(5),
	@GroupID AS INT,
	@Flag_SameAsPrevious AS BIT,
	@PreviousRecordID AS INT,
	@PreviousTicketNumber AS VARCHAR(20),
	@LoaderID AS INT,
	@CurrentRecordID INT OUTPUT
AS
	SET NOCOUNT ON  
	
	DECLARE @IDFound AS INT
	DECLARE @TicketNum2 AS VARCHAR(20)
	DECLARE @LengthOfTicketNum AS INT
	DECLARE @IsQuoted AS INT
	
	SET @IsQuoted = 0
	
	SET @Name_First = LTRIM(RTRIM(UPPER(LEFT(@Name_First, 20))))
	SET @Name_Last = LTRIM(RTRIM(UPPER(LEFT(@Name_Last, 20))))
	SET @Ticket_Number = REPLACE(@Ticket_Number, ' ', '')	
	SET @IDFound = NULL
	SET @LengthOfTicketNum = 7
	
	IF CHARINDEX ('0',@Ticket_Number,1) =1
		BEGIN
			SET @TicketNum2 = substring(@Ticket_Number, patindex('%[^0]%',@Ticket_Number), 20)
			SET @LengthOfTicketNum = LEN(@TicketNum2)
		END
	ELSE
		BEGIN
			SET @TicketNum2 = @Ticket_Number				
		END
	    
    
    SELECT TOP 1 @IDFound = T.RecordID, @IsQuoted = ISNULL(T.Clientflag, 0)
    FROM   TrafficTickets.dbo.tblTicketsArchive T WITH(NOLOCK)
           INNER JOIN TrafficTickets.dbo.tblTicketsViolationsArchive TV WITH(NOLOCK)
                ON  TV.RecordID = T.RecordID
    WHERE  LEFT(T.TicketNumber,@LengthOfTicketNum) = LEFT(@TicketNum2, @LengthOfTicketNum)
           AND T.LastName = @Name_Last
           AND TV.violationstatusid <> 80
           AND (T.InsertionLoaderID IN (26, 69, 70) OR T.UpdationLoaderID = 71)
	
	IF @IDFound IS NOT NULL
	BEGIN
	    IF @IsQuoted = 0 AND DATEDIFF(DAY, @Court_Date, '01/01/1900') <> 0
	    BEGIN
	        IF LEN(@Ticket_Number) < 5
	        BEGIN
	            INSERT INTO TrafficTickets.dbo.tblIDGenerator (recdate) VALUES(GETDATE())
	            SELECT @Ticket_Number = 'HC' + CONVERT(VARCHAR(12), @@identity)
	        END
	        
	        UPDATE TrafficTickets.dbo.tblTicketsArchive
	        SET    UpdationLoaderID = @LoaderID, GroupID = @GroupID
	        WHERE  RecordID = @IDFound
	        
	        UPDATE TrafficTickets.dbo.tblTicketsViolationsArchive
	        SET    UpdationLoaderId = @LoaderID, CourtDate = @Court_Date, LoaderUpdateDate = GETDATE(), GroupID = @GroupID
	        WHERE  RecordID = @IDFound
	        
	        SET @CurrentRecordID = @IDFound
	    END
	END
	
	

GO
GRANT EXECUTE ON [dbo].[USP_HTS_LA_Update_Tickets_HCCC_JIMS009] TO dbr_webuser
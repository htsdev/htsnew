﻿USE [LA_Houston]
GO
/****** Object:  StoredProcedure [dbo].[Tickets_Upload_Sugarland_DL_Event_Dispose]    Script Date: 10/19/2011 02:42:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






/*
Author: Muhammad Adil Aleem
Description: To Upload Sugarland DL, Events and Dispose CASEs From Loader Service
Type : Loader Service
Created date : Jul-19th-2010
Parameters:
	@Ticket_Number: Ticket Number
	@Name_First: DefENDant's first name
	@Name_Last: DefENDant's last name
	@DLState: DefENDant's Driving License state
	@Violation_Description: offense description
	@Court_Date: offense date
	@FineAmount: Offense charges
	@BondAmount: Warrant amount
	@GroupID: Data file ID
	@LoaderID: Insure which loader IS executing thIS CASE.
	@Flag_INSERTed: It returns status wether the record INSERTed or deleted
*/
ALTER PROCEDURE [dbo].[Tickets_Upload_Sugarland_DL_Event_Dispose]
@Ticket_Number as varchar(20),
@Cause_Number as varchar(20),
@Name_First as varchar(50),
@Name_Last as varchar(50),
@DOB AS DATETIME,
@Race AS VARCHAR(10),
@Gender AS VARCHAR(10),
@OfficerFirstName as varchar(50),
@OfficerLastName as varchar(50),
@DLNumber as varchar(20),
@DLState as Varchar(10),
@PhoneNumber as varchar(15),
@WorkPhoneNumber as varchar(12),
@Violation_Description as Varchar(2000),
@Court_Date AS DATETIME,
@Court_Status as varchar(4),
@FineAmount FLOAT,
@BondAmount FLOAT,
@BondFlag as BIT,
@Statute_Number as varchar(50),
@GroupID as Int,
@LoaderID as int,
@Flag_Updated int OUTPUT,
@Flag_ClientUpdated int OUTPUT
AS
SET nocount on  

Declare @CourtStatusID as Int
Declare @DLStateID as Int
Declare @ViolationID as int
Declare @IDFound as INT
Declare @RowIDFound as INT
DECLARE @ClientID AS INT
DECLARE @TicketID AS INT
DECLARE @Court_Date_Found DATETIME
DECLARE @Court_Status_Found INT
DECLARE @OfficerID AS INT
DECLARE @OfficerCodeFound AS INT
DECLARE @Statute_Number_Found VARCHAR(50)
SET @Name_First = LEFT(@Name_First,20)
SET @Name_Last = LEFT(@Name_Last,20)
SET @Flag_Updated = 0
SET @Flag_ClientUpdated = 0

-- Faique Ali 10623 01/11/2013 Added to not inserting "This case is verified by Sugarland Event Loader" multiple history notes. . . 
DECLARE @Court_Status_Found_FORCLIENT INT
DECLARE @HistoryNote_Max_ID INT
SET @HistoryNote_Max_ID = 0

IF @Cause_Number = ''
BEGIN
	SET @Cause_Number = @Ticket_Number
END

IF LEFT(@Gender,1) = 'M'
	BEGIN
		SET @Gender = 'Male'
	END
ELSE IF LEFT(@Gender,1) = 'F'
	BEGIN
		SET @Gender = 'Female'
	END

SELECT @Race = CASE @Race
WHEN 'A' THEN 'Asian'
WHEN 'B' THEN 'Black'
WHEN 'H' THEN 'Hispanic'
WHEN 'I' THEN 'Indian'
WHEN 'M' THEN 'Mexican'
WHEN 'O' THEN 'Other'
WHEN 'U' THEN 'Unknown'
WHEN 'W' THEN 'White'
WHEN 'X' THEN 'Unknown'
ELSE NULL
END

IF DATEPART(dw,@Court_Date) = 7 -- IF SaturDAY THEN make it FriDAY (Court date should be in working DAYs)
	BEGIN
		SET @Court_Date = @Court_Date - 1
	END
ELSE IF DATEPART(dw,@Court_Date) = 1 -- IF SunDAY THEN make it FriDAY (Court date should be in working DAYs)
	BEGIN
		SET @Court_Date = @Court_Date - 2
	END

IF ISNULL(@OfficerFirstName,'') = ''
	BEGIN
		SET @OfficerFirstName = 'N/A'
	END
IF ISNULL(@OfficerLastName,'') = ''
	BEGIN
		SET @OfficerLastName = 'N/A'
	END
IF @LoaderID IN (45, 47)
BEGIN
	SET @OfficerID = NULL
	SELECT Top 1 @OfficerID = OfficerNumber_PK from TrafficTickets.dbo.tblOfficer where (ISNULL(FirstName,'') = @OfficerFirstName AND ISNULL(LastName,'') = @OfficerLastName)
	IF @OfficerID IS NULL
		BEGIN
			SET @OfficerID = (SELECT MAX(OfficerNumber_PK) + 1 from TrafficTickets.dbo.tblofficer)
			INSERT INTO TrafficTickets.dbo.tblOfficer (OfficerNumber_PK, OfficerType, FirstName, LastName, Comments) Values (@OfficerID, 'N/A', @OfficerFirstName, @OfficerLastName, 'Inserted at ' + CONVERT(varchar(25),getdate(),101))
		END
END

SET @ViolationID = NULL
--Farrukh 9702 10/20/2011 Correct BondAmount parameter instead of FineAmount
SELECT Top 1 @ViolationID = ViolationNumber_PK, @FineAmount = CASE WHEN @FineAmount = 0 THEN ISNULL(ChargeAmount,0) ELSE @FineAmount END, @BondAmount = CASE WHEN @BondAmount = 0 THEN ISNULL(BondAmount,0) ELSE @BondAmount END From TrafficTickets.dbo.TblViolations Where (Description = @Violation_Description  and CourtLoc = 3060 and ViolationType = 11)

IF @ViolationID IS NULL
	BEGIN
		INSERT INTO TrafficTickets.dbo.TblViolations (Description, Sequenceorder, IndexKey, IndexKeys, ChargeAmount, BondAmount, Violationtype, ChargeonYesflag, ShortDesc, ViolationCode, AdditionalPrice, CourtLoc)
		Values(@Violation_Description, NULL, 2, 100, @FineAmount, @BondAmount, 11, 1, 'None', 'none', 0, 3060)
		SET @ViolationID = SCOPE_IDENTITY()
	END

SET @CourtStatusID = 
CASE WHEN @LoaderID = 46 THEN 80 -- IF Dispose loader THEN SET court status to Dispose
WHEN @BondFlag = 2 THEN 161 -- Farrukh 9702 10/18/2011 IF Bond Flag is C THEN SET Court status to NON ISSUE
WHEN @BondFlag = 1  THEN 186 -- Rab Nawaz Khan 10442 09/17/2012 Warrant Status if bond flage is true . . . 
WHEN @Court_Status = 'IA' THEN 3 -- Arraignment
WHEN @Court_Status IN ('AA', 'AJ', 'AP', 'CD', 'CL', 'CS', 'D1', 'D2', 'DA', 'DD',
					   'DF', 'DH', 'DI', 'DJ', 'DL', 'DN', 'DP', 'DT', 'DX', 'EC',
					   'ED', 'EP', 'EX', 'JC', 'JS', 'MB', 'MD', 'OM', 'PD', 'PP',
					   'PR', 'PS', 'SH', 'TA', 'TS', 'VD', 'XJ') THEN 80 -- Dispose -- Adil 8134 08/11/2010 more statuses mapped.
WHEN @Court_Status = 'BT' THEN 103 -- Judge Trial
WHEN @Court_Status = 'JT' THEN 26 -- Jury Trial -- Adil 8134 08/11/2010 'JT' mapped to jury trial.
WHEN @Court_Status = 'PJ' THEN 104 -- Waiting / PENDing Jury Trial -- Adil 8134 08/11/2010 'PJ' mapped to waiting.
WHEN @Court_Status IN ('PH', 'PT') THEN 101 -- Pre Trial
WHEN @Court_Status = 'JV' THEN 147 -- Other
ELSE 147 END -- Other

SET @DLStateID = (SELECT TOP 1 StateID From TrafficTickets.dbo.TblState Where State = @DLState)

IF @BondFlag = 2 -- Farrukh 9702 10/18/2011 IF Bond Flag is C THEN SET Bond Flag to 0
BEGIN
	SET @BondFlag = 0 
END


SET @RowIDFound = NULL
SET @IDFound = NULL
--Farrukh 9702 10/20/2011 Record searching criteria changed by FirstName,LastName,CourtDate 
SELECT TOP 1 @IDFound = T.RecordID
FROM TrafficTickets.dbo.tblTicketsArchive T
INNER JOIN TrafficTickets.dbo.tblTicketsViolationsArchive TV
ON T.RecordID = TV.RecordID
-- Rab Nawaz Khan 10442 11/29/2012 Resolve the Multiple Profile issue. . . 
WHERE ((TV.TicketNumber_PK = @Ticket_Number) --Farrukh 9702 12/28/2011 Excluded violation sequence in lookup
		OR
		(FirstName = @Name_First and LastName = @Name_Last
		and datediff(day,TV.CourtDate, @Court_Date) = 0 )) and TV.CourtLocation = 3060

IF @IDFound IS NOT NULL
	BEGIN
		UPDATE TrafficTickets.dbo.tblTicketsArchive
		SET DLNumber = @DLNumber, DLStateID = @DLStateID, DOB = @DOB,
		PhoneNumber = CASE WHEN CHARINDEX('0000000', @PhoneNumber, 1) > 0 OR @LoaderID = 46 THEN PhoneNumber ELSE @PhoneNumber END,
		WorkPhoneNumber = CASE WHEN CHARINDEX('0000000', @WorkPhoneNumber, 1) > 0  OR @LoaderID = 46 THEN WorkPhoneNumber ELSE @WorkPhoneNumber END,
		Bondflag = CASE WHEN @BondFlag = 1 AND @LoaderID = 47 THEN 1 ELSE BondFlag END, UpdationLoaderId = @LoaderID, GroupID = @GroupID,
		ListDate = CASE WHEN @BondFlag = 1 AND @LoaderID = 47 THEN GETDATE() ELSE ListDate END -- Rab Nawaz Khan 10442 11/27/2012 Added List Date to update for mailers if Warrant Case and loader is Event. . . 
		WHERE RecordID = @IDFound
		
		SET @Flag_Updated = 2
		
	END
ELSE IF @LoaderID = 47 -- ADIL 8303 09/21/2010 IF CASE CAME INTO EVENTS THEN NEED TO INSERT., Adil 9618 08/25/2011 ARRAIGNMENT condition removed while inserting event cases.
	BEGIN
		IF Len(@Ticket_Number)<1
			BEGIN
				INSERT INTO TrafficTickets.dbo.tblIDGenerator(recdate) values(getdate())
				SELECT @Ticket_Number='SMC' + CONVERT(varchar(12),SCOPE_IDENTITY())
			END
			
		INSERT INTO TrafficTickets.dbo.tblTicketsArchive (RecLoadDate, ListDate, TicketNumber, MidNumber, FirstName, LastName, Race, Gender, PhoneNumber, WorkPhoneNumber, DLNumber, DLStateID, DOB, Address1, City, StateID_FK, ZipCode, Flag1, DP2, DPC, Bondflag, INSERTionLoaderID,GroupID)   
		Values(GETDATE(), GETDATE(), @Ticket_Number, @Ticket_Number, @Name_First, @Name_Last, @Race, @Gender, @PhoneNumber, @WorkPhoneNumber, @DLNumber, @DLStateID, @DOB, 'N/A', 'N/A', 45, '00000', 'N', '00', '0', @BondFlag, @LoaderID,@GroupID)

		SET @IDFound = SCOPE_IDENTITY()
	END

--Farrukh 9702 10/20/2011 Seperate lookup for defendant's violations
Set @RowIDFound = null
Select TOP 1 @RowIDFound = Isnull(RowID,0), @Court_Date_Found = ISNULL(TV.CourtDate,'1/1/1900'), @Court_Status_Found = ISNULL(tv.violationstatusid, 0) From TrafficTickets.dbo.tblTicketsViolationsArchive TV
                WHERE 
				((charindex('speed', @Violation_Description, 1) > 0 and charindex('speed', tv.ViolationDescription, 1) > 0
or left(tv.ViolationDescription, 20) = left(@Violation_Description, 20))
AND TV.CourtLocation = 3060 AND TV.RecordID = @IDFound)

IF @RowIDFound IS NOT NULL
	BEGIN
		UPDATE TrafficTickets.dbo.tblTicketsViolationsArchive
		SET TicketOfficerNumber = CASE WHEN @LoaderID  IN (45, 47) THEN @OfficerID ELSE TicketOfficerNumber END,
		violationstatusid = CASE WHEN @LoaderID  IN (46, 47) AND @Court_Status_Found <> @CourtStatusID THEN @CourtStatusID ELSE violationstatusid END,
		CourtDate = CASE WHEN @LoaderID = 47 AND @CourtStatusID IN (3, 26, 101, 103, 186) AND (DATEDIFF(DAY, @Court_Date_Found, @Court_Date) > 0) THEN @Court_Date ELSE CourtDate END, -- Only Arraignment, Jury Trial, Pre Trial, Judge Trial and Warrant statuses with future court date
		BondDate = CASE WHEN @BondFlag = 1 THEN @Court_Date ELSE BondDate END,
		-- Adil 8303 09/23/2010 If we have ticket number in cause number in database and we have a different cause number in data file then we will update the data file cause number into existing cause number field in non-client DB because existing cause number was not the valid cause number.
		--Farrukh 9702 10/20/2011 Loader check removed
		CauseNumber = CASE WHEN ISNULL(CauseNumber, '') <> ISNULL(@Cause_Number, '') AND ISNULL(@Cause_Number, '') <> '' THEN @Cause_Number ELSE CauseNumber END,
		TicketNumber_PK = CASE WHEN ISNULL(TicketNumber_PK, '') <> ISNULL(@Ticket_Number, '') AND ISNULL(@Ticket_Number, '') <> '' THEN @Ticket_Number ELSE TicketNumber_PK END,
		Statutenumber = @Statute_Number, -- Adil 8240 09/09/2010 Add Statute number to update.
		FineAmount = @FineAmount, -- Adil 8275 09/14/2010 Add fine amount to update.
		BondAmount = CASE WHEN @BondFlag = 1 AND @LoaderID = 47 THEN @BondAmount ELSE BondAmount END, -- Adil 9702 11/15/2011 Add bond amount to update.
		UpdationLoaderId = @LoaderID, updateddate = GETDATE(), LoaderUpdateDate = GETDATE(), GroupID = @GroupID 
		WHERE RowID = @RowIDFound
		
		SET @Flag_Updated = 2
	END
ELSE IF @LoaderID = 47 -- ADIL 8303 09/21/2010 IF ARRAIGNMENT CASE CAME INTO EVENTS THEN NEED TO INSERT., Adil 9618 08/25/2011 ARRAIGNMENT condition removed while inserting event cases.
	BEGIN
		INSERT INTO TrafficTickets.dbo.tblTicketsViolationsArchive (TicketNumber_PK, ViolationNumber_PK, CauseNumber, CourtDate, BondDate, INSERTDate, ViolationDescription, ViolationCode, FineAmount, BondAmount, CourtLocation, RecordID, ViolationStatusID, Statutenumber, TicketOfficerNumber, INSERTionLoaderId, GroupID)
		Values(@Ticket_Number, @ViolationID, @Cause_Number, @Court_Date, CASE WHEN @BondFlag = 1 THEN @Court_Date ELSE NULL END, Getdate(), @Violation_Description, 'None', @FineAmount, @BondAmount, 3060, @IDFound, @CourtStatusID, @Statute_Number, @OfficerID, @LoaderID, @GroupID)
		SET @Flag_Updated = 1
	END

	    -- Update Quote/Client Auto Info
		SET @ClientID = NULL
		SET @TicketID = NULL
		
		SELECT TOP 1 @TicketID = ttv.TicketID_PK, @ClientID = ttv.TicketsViolationID, @Court_Date_Found = ttv.CourtDate, @TicketID = ttv.TicketID_PK, @Court_Status_Found = ttv.CourtViolationStatusID,@Court_Status_Found_FORCLIENT=ttv.CourtViolationStatusIDmain, @OfficerCodeFound = ISNULL(ttv.TicketOfficerNumber, ''), @Statute_Number_Found = ISNULL(ttv.Statutenumber, '')
		FROM TrafficTickets.dbo.tblTicketsViolations ttv
		WHERE ((ttv.RefCASENumber = @Ticket_Number OR (ttv.RefCASENumber = SUBSTRING (@Ticket_Number, 1 , LEN(@Ticket_Number) - 2) AND ttv.ViolationNumber_PK = @ViolationID)) AND ttv.CourtID = 3060)

		IF @ClientID IS NOT NULL
		BEGIN
			UPDATE TrafficTickets.dbo.tblTicketsViolations
			--SET CourtViolationStatusID = CASE WHEN @LoaderID  IN (46, 47) AND @Court_Status_Found <> @CourtStatusID THEN @CourtStatusID ELSE CourtViolationStatusID END, --Adil 8221 08/31/2010 Code commented.
			SET CourtViolationStatusID = CASE WHEN @LoaderID  IN (46, 47) THEN @CourtStatusID ELSE CourtViolationStatusID END,
			--faique 10623 12/26/2012 update court status if change
			CourtViolationStatusIDmain = CASE WHEN @LoaderID  IN (46, 47) AND @Court_Status_Found_FORCLIENT<>@CourtStatusID THEN @CourtStatusID ELSE CourtViolationStatusID END,
			
			CourtDate = CASE WHEN @LoaderID = 47 AND @CourtStatusID IN (3, 26, 101, 103, 186) AND (DATEDIFF(DAY, @Court_Date_Found, @Court_Date) > 0) THEN @Court_Date ELSE CourtDate END, -- Only Arraignment, Jury Trial, Pre Trial, Judge and Warrant statuses with future court date
			TicketOfficerNumber = CASE WHEN @LoaderID  IN (45, 47) THEN @OfficerID ELSE TicketOfficerNumber END,
			GroupID = @GroupID, UpdatedDate = GETDATE(), loaderupdatedate = GETDATE(), VEmployeeID = 3992,
			Statutenumber = @Statute_Number -- Adil 8240 09/09/2010 Add Statute number to update.
			WHERE TicketsViolationID = @ClientID
			
			SET @Flag_ClientUpdated = 2
			
			-- Adil 8303 09/23/2010 Adding court violation status code into Auto history of client exactly what we received in data files.
			UPDATE TrafficTickets.dbo.tblTicketsViolationLog
			SET SugarlandEvtDataFileStatus = @Court_Status
			WHERE TicketviolationID = @ClientID
			AND Rowid = (SELECT MAX(ttvl.Rowid)
			               FROM TrafficTickets.dbo.tblTicketsViolationLog ttvl
							WHERE ttvl.TicketviolationID = @ClientID)
			
			--Farrukh 9702 10/20/2011 INSERT history note WHEN auto court date time IS changed  by Event loader.
			IF @LoaderID = 47 AND DATEDIFF(DAY, @Court_Date_Found, @Court_Date) <> 0 and DATEDIFF(hh, @Court_Date_Found, @Court_Date) <> 0 AND @CourtStatusID IN (3, 26, 101, 103) -- Arraignment, JuryTrial, PreTrial & JudgeTrial
			--IF @LoaderID = 47 AND @CourtStatusID IN (3, 26, 101, 103) 
			BEGIN
				INSERT INTO TrafficTickets.dbo.tblticketsnotes (ticketid, subject , employeeid)
				SELECT @TicketID, 'Auto Court Date Updated - ' + ISNULL(@Ticket_Number, '') + ': From [' + TrafficTickets.dbo.fn_DateFormat(@Court_Date_Found, 5, '/', ':', 1) + '] To [' + TrafficTickets.dbo.fn_DateFormat(@Court_Date, 5, '/', ':', 1) + '] Through Sugarland ' + CASE WHEN @LoaderID  = 46 THEN 'Dispose' WHEN  @LoaderID = 47 THEN 'Event' ELSE '' END + ' Loader', 3992
				--Faique 10623 01/11/2013 checking for history note entry
				SET @HistoryNote_Max_ID = 1
			END

			--Farrukh 9702 10/20/2011 INSERT history note WHEN auto court status IS changed by Event or Dispose loader.
			IF @LoaderID  IN (46, 47) AND @Court_Status_Found <> @CourtStatusID -- Adil 9667 9/16/2011 Check implemented to update only when it is different
			--IF @LoaderID  IN (46, 47)
			BEGIN
				SELECT @Court_Status = 
				CASE 
				WHEN @CourtStatusID = 80 THEN 'DISP' -- WHEN Dispose loader THEN SET status to Dispose
				WHEN @CourtStatusID = 3 THEN 'ARR'
				WHEN @CourtStatusID = 26 THEN 'JUR'
				WHEN @CourtStatusID = 101 THEN 'PRE'
				WHEN @CourtStatusID = 103 THEN 'JUD'
				WHEN @CourtStatusID = 104 THEN 'WAIT'
				WHEN @CourtStatusID = 186 THEN 'FTA' -- Rab Nawaz Khan 10442 09/17/2012 Warrant Status . . . 
				WHEN @CourtStatusID = 147 THEN 'OTHR'
				WHEN @CourtStatusID = 161 THEN 'N-ISU'
				END
				
				DECLARE @Court_Status_Found_Str VARCHAR(10)
				SELECT @Court_Status_Found_Str = ShortDescription From TrafficTickets.dbo.tblcourtviolationstatus 
				where CourtViolationStatusID = @Court_Status_Found

				INSERT INTO TrafficTickets.dbo.tblticketsnotes (ticketid, subject , employeeid)
				SELECT @TicketID, 'Auto Court Status Updated - ' + ISNULL(@Ticket_Number, '') + ':From [' + @Court_Status_Found_Str + '] To [' + @Court_Status + '] Through Sugarland ' + CASE WHEN @LoaderID  = 46 THEN 'Dispose' WHEN  @LoaderID = 47 THEN 'Event' ELSE '' END + ' Loader', 3992
				--Faique 10623 01/11/2013 checking for history note entry
				SET @HistoryNote_Max_ID = 1
			END
			
			-- Adil 8397 11/11/2010 Advancing Follow-Up date.
			--Farrukh 9702 10/20/2011 Check Added for Event/Dispose Loader
			IF @LoaderID  IN (46, 47) AND @Court_Status_Found = 104 AND @CourtStatusID = 104 -- If case is still in WAITING status then advance follow-up date to 14 days.
			BEGIN
				DECLARE @FollowUpDate DATETIME
				
				SET @FollowUpDate = GETDATE() + 14
				
				IF datepart(dw,@FollowUpDate) = 7 -- If Saturday then make it Friday (Follow-Up date must be in working days)
					BEGIN
						SET @FollowUpDate = @FollowUpDate - 1
					END
				ELSE IF datepart(dw,@FollowUpDate) = 1 -- If Sunday then make it Friday (Follow-Up date must be in working days)
					BEGIN
						SET @FollowUpDate = @FollowUpDate - 2
					END
				UPDATE TrafficTickets.dbo.tblTickets SET trafficWaitingFollowUpDate = @FollowUpDate WHERE TicketID_PK = @TicketID
				
				INSERT INTO TrafficTickets.dbo.tblticketsnotes (ticketid, subject , employeeid)
				SELECT @TicketID, 'Traffic waiting follow-up date advanced to 14 days through Sugarland ' + CASE WHEN @LoaderID  = 46 THEN 'Dispose' WHEN  @LoaderID = 47 THEN 'Event' ELSE '' END + ' Loader', 3992
				--Faique 10623 01/11/2013 checking for history note entry
				SET @HistoryNote_Max_ID = 1
			END

			
			--INSERT history note WHEN officer number changed.
			--Farrukh 9702 10/20/2011 Check Added for DL / Event Loader
			IF @LoaderID IN (45, 47) AND @OfficerCodeFound <> @OfficerID -- Adil 9667 9/16/2011 Check implemented to update only when it is different
			--IF @LoaderID IN (45, 47)
			BEGIN
				INSERT INTO TrafficTickets.dbo.tblticketsnotes (ticketid, subject , employeeid)
				SELECT @TicketID, 'Officer Number Updated - ' + ISNULL(@Ticket_Number, '') + ': From [' + CONVERT(VARCHAR(20), @OfficerCodeFound) + '] To [' + CONVERT(VARCHAR(20), @OfficerID) + '] Through Sugarland ' + CASE WHEN @LoaderID  = 45 THEN 'DL' WHEN @LoaderID  = 46 THEN 'Dispose' WHEN  @LoaderID = 47 THEN 'Event' ELSE '' END + ' Loader', 3992
				--Faique 10623 01/11/2013 checking for history note entry
				SET @HistoryNote_Max_ID = 1
			END
			
			-- Adil 8240 09/09/2010 INSERT history note WHEN statute number changed.
			IF @Statute_Number_Found <> @Statute_Number -- Adil 9667 9/16/2011 Check implemented to update only when it is different
			BEGIN
				INSERT INTO TrafficTickets.dbo.tblticketsnotes (ticketid, subject , employeeid)
				SELECT @TicketID, 'Statute Number Updated - ' + ISNULL(@Ticket_Number, '') + ': From [' + CONVERT(VARCHAR(20), @Statute_Number_Found) + '] To [' + CONVERT(VARCHAR(20), @Statute_Number) + '] Through Sugarland ' + CASE WHEN @LoaderID  = 45 THEN 'DL' WHEN @LoaderID  = 46 THEN 'Dispose' WHEN  @LoaderID = 47 THEN 'Event' ELSE '' END + ' Loader', 3992
				--Faique 10623 01/11/2013 checking for history note entry
				SET @HistoryNote_Max_ID = 1
			END
			
			IF @LoaderID in (46, 47) --Farrukh 9702 10/20/2011 If Dispose OR Events Loader
			BEGIN
				--faique 10623 12/26/2012 check if change in any record or change in history note then display verifier message.. 
				IF (@HistoryNote_Max_ID <> 0)
				BEGIN
					INSERT INTO TrafficTickets.dbo.tblticketsnotes (ticketid, subject , employeeid)
					SELECT @TicketID, 'This case is verified by Sugarland ' + CASE WHEN @LoaderID  = 46 THEN 'Dispose' WHEN  @LoaderID = 47 THEN 'Event' ELSE '' END + ' Loader', 3992
				END	
			End
		END



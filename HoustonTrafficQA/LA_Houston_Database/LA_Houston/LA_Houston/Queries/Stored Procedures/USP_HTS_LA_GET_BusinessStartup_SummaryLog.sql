﻿USE [LA_Houston]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTS_LA_GET_BusinessStartup_SummaryLog]    Script Date: 12/02/2011 05:30:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO








/******  
* Created By :	  Farrukh Iftikhar
* Create Date :   11/01/2011 5:22:27 PM 
* Task ID :		  9664
* Business Logic : Get the loader summary log on the basis of RecDate
* List of Parameter :
	@RecDate         : Record Date
	@ListDate	     : List Date
	@LoaderName      : Loader Name
	@TotalRecords    : Total record count
	@ReliableRecords : Reliable record count
* Column Return      : Loader Information.    
******/

-- [USP_HTS_LA_GET_BusinessStartup_SummaryLog]  '11/01/2011','11/01/2011','Harris County Business Start Up Loader',7,7
CREATE PROCEDURE [dbo].[USP_HTS_LA_GET_BusinessStartup_SummaryLog] 
	@RecDate DATETIME,
	@ListDate DATETIME,
	@LoaderName VARCHAR(100),
	@TotalRecords INT,
	@ReliableRecords INT
AS
	DECLARE @DatabaseName VARCHAR(20)
	SET @DatabaseName = 'AssumedNames'
	
	--SET @DatabaseName = CASE WHEN @DatbaseId=3 THEN 'TrafficTickets' ELSE 'DallasTrafficTickets' END
	
	DECLARE @sql VARCHAR(MAX)
	SET @sql=''
	SET @sql=@sql+'
	DECLARE @YAddressCount INT
	DECLARE @DAddressCount INT
	DECLARE @SAddressCount INT
	DECLARE @NAddressCount INT
	DECLARE @InsertedCount INT
	DECLARE @TotalCount INT	
	DECLARE @NonReliableRecords INT
	DECLARE @tblStatus TABLE (DESCRIPTION VARCHAR(50),[VALUES] VARCHAR(200))
	DECLARE @EmailBody VARCHAR(MAX)		
	DECLARE @Summmary VARCHAR(MAX)
	DECLARE @CRLF char(2)     
	
	
	SET @CRLF=CHAR(13)+CHAR(10)
	SET @YAddressCount = 0
	SET @DAddressCount = 0
	SET @SAddressCount = 0
	SET @NAddressCount = 0
	SET @TotalCount = ' + CONVERT(VARCHAR(5), @ReliableRecords) + 
	'SET @InsertedCount = 0
	SET @Summmary=''''
	INSERT INTO @tblStatus VALUES (@CRLF+'' Load Date : '', CONVERT(VARCHAR(12),GETDATE(),101))
	INSERT INTO @tblStatus VALUES (@CRLF+'' Loader Name : '','''+@LoaderName+''')
	INSERT INTO @tblStatus VALUES (@CRLF+'' Total No. of Records : '','+CONVERT(VARCHAR,@TotalRecords)+')
	INSERT INTO @tblStatus VALUES (@CRLF+'' Reliable Records : '','+CONVERT(VARCHAR,@ReliableRecords)+')
	INSERT INTO @tblStatus VALUES (@CRLF+'' Non-Reliable Records : '','+CONVERT(VARCHAR,@TotalRecords-@ReliableRecords)+')
	
	-- For Business
	INSERT INTO @tblStatus values(@CRLF+'' For Business Address(s) : '', '''')
	SELECT @YAddressCount = @YAddressCount + CASE 
	                                              WHEN (B.Flags = ''Y'') THEN 1
	                                              ELSE 0
	                                         END,
	       @DAddressCount = @DAddressCount + CASE 
	                                              WHEN (B.Flags = ''D'') THEN 1
	                                              ELSE 0
	                                         END,
	       @SAddressCount = @SAddressCount + CASE 
	                                              WHEN (B.Flags = ''S'') THEN 1
	                                              ELSE 0
	                                         END,
	       @NAddressCount = @NAddressCount + CASE 
	                                              WHEN (B.Flags = ''N'') THEN 1
	                                              ELSE 0
	                                         END
	FROM   '+@DatabaseName+'.dbo.Business B
	WHERE  DATEDIFF(DAY, B.RecDate, '''+CONVERT(VARCHAR,@RecDate)+''') = 0 
	AND DATEDIFF(DAY, B.RegistrationDate, '''+CONVERT(VARCHAR,@ListDate)+''') = 0 
	INSERT INTO @tblStatus VALUES (@CRLF+'' Address Verified Records (Y): '',@YAddressCount)
	INSERT INTO @tblStatus VALUES (@CRLF+'' Address Verified Records (D): '',@DAddressCount)
	INSERT INTO @tblStatus VALUES (@CRLF+'' Address Verified Records (S): '',@SAddressCount)
	INSERT INTO @tblStatus VALUES (@CRLF+'' Address Verified Records (N): '',@NAddressCount)
	
	-- For Owners
	INSERT INTO @tblStatus values(@CRLF+'' For Owners Address(s) : '', '''')
	SELECT @YAddressCount = @YAddressCount + CASE 
	                                              WHEN (BO.Flags = ''Y'') THEN 1
	                                              ELSE 0
	                                         END,
	       @DAddressCount = @DAddressCount + CASE 
	                                              WHEN (BO.Flags = ''D'') THEN 1
	                                              ELSE 0
	                                         END,
	       @SAddressCount = @SAddressCount + CASE 
	                                              WHEN (BO.Flags = ''S'') THEN 1
	                                              ELSE 0
	                                         END,
	       @NAddressCount = @NAddressCount + CASE 
	                                              WHEN (BO.Flags = ''N'') THEN 1
	                                              ELSE 0
	                                         END
	FROM  '+@DatabaseName+'.dbo.BusinessOwner BO 
	INNER JOIN '+@DatabaseName+'.dbo.Business B 
	ON BO.BusinessID = B.BusinessID 
	WHERE  DATEDIFF(DAY, B.RecDate, '''+CONVERT(VARCHAR,@RecDate)+''') = 0 
	AND DATEDIFF(DAY, B.RegistrationDate, '''+CONVERT(VARCHAR,@ListDate)+''') = 0 
	INSERT INTO @tblStatus VALUES (@CRLF+'' Address Verified Records (Y): '',@YAddressCount)
	INSERT INTO @tblStatus VALUES (@CRLF+'' Address Verified Records (D): '',@DAddressCount)
	INSERT INTO @tblStatus VALUES (@CRLF+'' Address Verified Records (S): '',@SAddressCount)
	INSERT INTO @tblStatus VALUES (@CRLF+'' Address Verified Records (N): '',@NAddressCount)
		       
	SELECT @InsertedCount = @InsertedCount + CASE 
												WHEN DATEDIFF(DAY, B.RecDate, '''+CONVERT(VARCHAR,@RecDate)+''') = 0 AND DATEDIFF(DAY, B.RegistrationDate, '''+CONVERT(VARCHAR,@ListDate)+''') = 0  THEN 1
												ELSE 0
											 END
	FROM  '+@DatabaseName+'.dbo.Business B
	WHERE DATEDIFF(DAY, B.RecDate, '''+CONVERT(VARCHAR,@RecDate)+''') = 0
    AND DATEDIFF(DAY, B.RegistrationDate, '''+CONVERT(VARCHAR,@ListDate)+''') = 0 
	
	INSERT INTO @tblStatus values(@CRLF+'' No of Insered Records : '', convert(varchar(5), @InsertedCount) + '' '')	
	INSERT INTO @tblStatus values(@CRLF+'' No of Not Inserted Records : '', @TotalCount-@InsertedCount)
	INSERT INTO @tblStatus VALUES (@CRLF+'' Load By : '',''Loader Service'')
	
	SELECT @Summmary=@Summmary+ISNULL ([DESCRIPTION],'''')+ISNULL([VALUES],'''') FROM @tblStatus 
	
	SELECT @Summmary'
		
	EXEC (@sql)





GO 
GRANT EXECUTE ON USP_HTS_LA_GET_BusinessStartup_SummaryLog TO dbr_webuser
GO
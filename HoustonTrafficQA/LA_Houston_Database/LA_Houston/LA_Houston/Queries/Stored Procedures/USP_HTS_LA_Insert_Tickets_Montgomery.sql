﻿ /*
* 
* Created By : Waqas Javed
* Task ID  : 6791/7023
* Created Date : 11/19/2009
* 
* 
* Business Logic : This procedure is used to insert records from montgomery site
* 
* Parameters :
* 
*	@Record_Date				:		This paramter is used to set record date 
*	@List_Date 					:		This paramter is used to set List Date
*	@Cause_Number 				:		This paramter is used to set cause number of the case 
*	@Ticket_Number 				:		This paramter is used to set ticket number of the case
*	@Last_Name 					:		This paramter is used to set last name of the client
*	@First_Name 				:		This paramter is used to set first name of the client
*	@Address_Part_I 			:		This paramter is used to set address part 1 of the client
*	@Address_Part_II 			:		This paramter is used to set address part 2 of the client
*	@City 						:		This paramter is used to set city of the client 
*	@State 						:		This paramter is used to set state of the client
*	@Zip 						:		This paramter is used to set zip of the client 
*	@DP2 						:		This paramter is used to set DP2 information of client address
*	@DPC 						:		This paramter is used to set DPC information of client address
*	@DOB 						:		This paramter is used to set date of birth of the client
*	@Race 						:		This paramter is used to set race of the client.
*	@Gender 					:		This paramter is used to set gender of the client
*	@Height 					:		This paramter is used to set Height of the client
*	@Weight 					:		This paramter is used to set wieght of the client
*	@DLNumber 					:		This paramter is used to set Driving License Number of the client
*	@Violation_Code 			:		This paramter is used to set violation code of the case
*	@Violation_Description 		:		This paramter is used to set violation description of the case
*	@OffenseDate 				:		This paramter is used to set Offense Date of the client
*	@ArrestDate 				:		This paramter is used to set Arrest Date of the client
*	@File_Date 					:		This paramter is used to set File_Date of the client
*	@Officer_Name 				:		This paramter is used to set Officer_Name of the case
*	@Arresting_Agency_Name 		:		This paramter is used to set Arresting_Agency_Name of the case
*	@Disposition_Date 			:		This paramter is used to set Disposition_Date of the case
*	@Judgement_Date 			:		This paramter is used to set Judgement_Date of the case
*	@Bar_Card_Number 			:		This paramter is used to set Bar_Card_Number of the case
*	@Attorney_Name 				:		This paramter is used to set Attorney_Name of the case
*	@Plea_Date 					:		This paramter is used to set Plea_Date of the case
*	@Bond_Date 					:		This paramter is used to set Bond_Date of the case
*	@Bonding_Number 			:		This paramter is used to set Bonding_Number of the case
*	@Bonding_Company_Name 		:		This paramter is used to set Bonding_Company_Name of the case
*	@Bond_Amount 				:		This paramter is used to set Bond_Amount of the case
*	@FTA_Issue_Date 			:		This paramter is used to set FTA_Issue_Date of the case
*	@Fine_Amount 				:		This paramter is used to set Fine_Amount of the case
*	@COURT_DATE 				:		This paramter is used to set COURT_DATE of the case
*	@COURT_STATUS 				:		This paramter is used to set COURT_STATUS of the case
*	@GroupID 					:		This paramter is used to set GroupID of the case
*	@AddressStatus 				:		This paramter is used to set AddressStatus of the case
*	@Flag_SameAsPrevious 		:		This paramter is used to set Flag_SameAsPrevious of the case
*	@PreviousRecordID 			:		This paramter is used to set PreviousRecordID of the case
*	@LoaderID 					:		This paramter is used to set LoaderID of the case
*	@Eyes						:		This paramter is used to set eyes of the client
*	@HairColor 					:		This paramter is used to set hair color of the client
*	@Employer 					:		This paramter is used to set employer of the client
*	@DLState 					:		This paramter is used to set state of the case
*	@CourtNumber 				:		This paramter is used to set court room number of the case
*	@Degree 					:		This paramter is used to set degree level of the violations
*	@CurrentRecordID  OUTPUT,	:		This paramter is used to get record id of the case
*	@Flag_Inserted OUTPUT 		:		This paramter is used to get flag inserted of the case
* 
*/
 
  
alter PROCEDURE [dbo].[USP_HTS_LA_Insert_Tickets_Montgomery]
	@Record_Date DATETIME,
	@List_Date SMALLDATETIME,
	@Cause_Number VARCHAR(20),
	@Ticket_Number VARCHAR(20),
	@Last_Name VARCHAR(50),
	@First_Name VARCHAR(50),
	@Address_Part_I VARCHAR(50),
	@Address_Part_II VARCHAR(50),
	@City VARCHAR(50),
	@State VARCHAR(10),
	@Zip VARCHAR(10),
	@DP2 VARCHAR(2),
	@DPC VARCHAR(1),
	@DOB DATETIME,
	@Race VARCHAR(15),
	@Gender VARCHAR(10),
	@Height VARCHAR(10),
	@Weight INT,
	@DLNumber VARCHAR(30),
	@Violation_Code VARCHAR(50),
	@Violation_Description VARCHAR(200),
    @OffenseDate DATETIME, -- Waqas 7128 12/16/2009 Offense date
	@ArrestDate DATETIME, -- Waqas 7111 12/09/2009 using arrest date
	@File_Date DATETIME,
	@Officer_First_Name VARCHAR(100),
	@Officer_Last_Name VARCHAR(100),
	@Arresting_Agency_Name VARCHAR(50),
	@Disposition_Date DATETIME,
	@Judgement_Date DATETIME,
	@Bar_Card_Number VARCHAR(50),
	@Attorney_Name VARCHAR(100),
	@Plea_Date DATETIME,
	@Bond_Date DATETIME,
	@Bonding_Number VARCHAR(20),
	@Bonding_Company_Name VARCHAR(100),
	@Bond_Amount MONEY,
	@FTA_Issue_Date DATETIME,
	@Fine_Amount MONEY,
	@COURT_DATE DATETIME,
	@COURT_STATUS VARCHAR(50),
	@GroupID INT,
	@AddressStatus CHAR(1),
	@Flag_SameAsPrevious BIT,
	@PreviousRecordID INT,	
	@LoaderID INT,
	@Eyes VARCHAR(10),
	@HairColor VARCHAR(10),
	@Employer VARCHAR(50),
	@DLState VARCHAR(10),
	@CourtNumber INT,
	@Degree VARCHAR(10),
	@CurrentRecordID INT OUTPUT,
	@Flag_Inserted INT OUTPUT
	
AS
	SET NOCOUNT ON    
	
	DECLARE @StateID INT  
	DECLARE @DLStateID INT  
	DECLARE @EmployerID INT  
	DECLARE @ViolationID INT  
	DECLARE @IDFound INT  
	DECLARE @TicketNum2 VARCHAR(20)  
	DECLARE @ViolationStatusID INT  
	DECLARE @ViolationStatusIDFound INT  
	DECLARE @FiledDateFound DATETIME  
	DECLARE @CourtDateFound DATETIME  
	DECLARE @BondFlag INT  
	DECLARE @OfficerCodeFound INT  
	DECLARE @OfficerCode INT
	
	DECLARE @AddressFound VARCHAR(50)  
	DECLARE @DOBFound DATETIME  
	DECLARE @CityFound VARCHAR(50)  
	DECLARE @ZipFound VARCHAR(12)  
	DECLARE @ChargeLevel int
	
	SET @Ticket_Number = REPLACE(@Ticket_Number, '-', '')
	SET @Cause_Number = REPLACE(@Cause_Number, '-', '')
	
	SET @First_Name = UPPER(LEFT(@First_Name, 20))  
	SET @Last_Name = UPPER(LEFT(@Last_Name, 20))  
	
	IF ISNULL(@Bond_Amount, 0) = 0
	BEGIN
		SET @Bond_Amount = 100
	END
	
	IF ISNULL(@Fine_Amount, 0) = 0
	BEGIN
		SET @Fine_Amount = 100
	END
	
	IF LEFT(@Gender, 1) = 'M'
	BEGIN
	    SET @Gender = 'Male'
	END
	ELSE 
	IF LEFT(@Gender, 1) = 'F'
	BEGIN
	    SET @Gender = 'Female'
	END  
	
	SELECT @Race = CASE @Race
	                    WHEN 'A' THEN 'Asian'
	                    WHEN 'B' THEN 'Black'
	                    WHEN 'H' THEN 'Hispanic'
	                    WHEN 'I' THEN 'Indian'
	                    WHEN 'M' THEN 'Mexican'
	                    WHEN 'O' THEN 'Other'
	                    WHEN 'U' THEN 'Unknown'
	                    WHEN 'W' THEN 'White'
	                    WHEN 'X' THEN 'Unknown'
	                    ELSE NULL
	               END  
	               
	SELECT @HairColor = CASE @HairColor 
				WHEN 'BLK' THEN 'Black'
				WHEN 'BLN' THEN 'Blonde'
				WHEN 'RED' THEN 'Red'
				WHEN 'BRO' THEN 'Brown'				
				ELSE 'Others'
	END
	
	SELECT @Eyes = CASE @Eyes 
				WHEN 'BLK' THEN 'Black'
				WHEN 'BLU' THEN 'Blue'
				WHEN 'GRN' THEN 'Green'
				WHEN 'HAZ' THEN 'Hazel'
				WHEN 'BRO' THEN 'Brown'				
				ELSE 'Others'
	end
				               
	               
	
	SET @Violation_Code = LTRIM(RTRIM(@Violation_Code))  
	SET @Violation_Description = LTRIM(RTRIM(@Violation_Description))  
	SET @Flag_Inserted = 0  
	
	SELECT @ViolationStatusID = CASE @COURT_STATUS
	                                 WHEN 'AR' THEN 3		-- Arraignment
	                                 WHEN 'JD' THEN 29		-- Jail Docket
	                                 WHEN 'PT' THEN 101		-- Pre Trial
	                                 WHEN 'JT' THEN 26		-- Jury
	                                 ELSE 161				-- Not Issue
	                            END
	
	IF ISNULL(@Officer_Last_Name, '') = '' AND ISNULL(@Officer_First_Name, '') = ''
	BEGIN
	    SET @Officer_Last_Name = 'N/A'
	    SET @Officer_First_Name = ''
	END	
	
	IF ISNULL(@Degree, '') <> ''
	BEGIN
		SET @ChargeLevel =(SELECT TOP 1 tc.ID FROM  TrafficTickets.dbo.tblChargelevel tc WHERE tc.LevelCode = @Degree)
	END
	ELSE
		BEGIN
			SET @ChargeLevel = null
		END
	
	
	--SET @Zip = REPLACE(@Zip, '-', '')
	SET @Violation_Code = REPLACE(@Violation_Code, '-', '')
	
	SET @OfficerCodeFound = NULL  
	SELECT TOP 1 @OfficerCodeFound = OfficerNumber_PK
	FROM   TrafficTickets.dbo.tblOfficer
	WHERE  (ISNULL(FirstName, '') = @Officer_First_Name) AND
	(ISNULL(LastName, '') = @Officer_Last_Name)
	
	IF @OfficerCodeFound IS NULL
	BEGIN
	    SET @OfficerCode = (
	            SELECT MAX(OfficerNumber_PK) + 1
	            FROM   TrafficTickets.dbo.tblofficer
	        )
	    
	    INSERT INTO TrafficTickets.dbo.tblOfficer
	      (
	        OfficerNumber_PK,
	        OfficerType,
	        FirstName,
	        LastName,
	        Comments
	      )
	    VALUES
	      (
	        @OfficerCode,
	        'N/A',
	        @Officer_First_Name,
	        @Officer_Last_Name,
	        'Inserted at ' + CONVERT(VARCHAR(25), GETDATE(), 101)
	      )
	END
	ELSE
	BEGIN
	    SET @OfficerCode = @OfficerCodeFound
	END  
	
	SET @ViolationID = NULL  
	SELECT TOP 1 @ViolationID = ViolationNumber_PK
	FROM   TrafficTickets.dbo.TblViolations 
	WHERE  (
	           DESCRIPTION = @Violation_Description
	           AND ViolationCode = @Violation_Code
	           AND CourtLoc = 3036 AND Violationtype = 5
	       )  
	
	IF @ViolationID IS NULL
	BEGIN
	    INSERT INTO TrafficTickets.dbo.TblViolations
	      (
	        DESCRIPTION,
	        Sequenceorder,
	        IndexKey,
	        IndexKeys,
	        ChargeAmount,
	        BondAmount,
	        Violationtype,
	        ChargeonYesflag,
	        ShortDesc,
	        ViolationCode,
	        AdditionalPrice,
	        CourtLoc
	      )
	    VALUES
	      (
	        @Violation_Description,
	        NULL,
	        2,
	        100,
	        @Fine_Amount,
	        @Bond_Amount,
	        5,
	        1,
	        'None',
	        @Violation_Code,
	        0,
	        3036
	      )  
	    SET @ViolationID = SCOPE_IDENTITY()
	END 
	
	
	
	SET @StateID = (
	        SELECT TOP 1 StateID
	        FROM   TrafficTickets.dbo.TblState
	        WHERE  STATE = ISNULL(@State, '')
	    )  
	
	
	SET @DLStateID = (
	        SELECT TOP 1 StateID
	        FROM   TrafficTickets.dbo.TblState
	        WHERE  STATE = ISNULL(@DLState , '')
	)
	
	IF RTRIM(LTRIM(@Employer)) <> 'UNEMPLOYED'
	BEGIN
		SET @EmployerID = (
				SELECT TOP 1 EmployerID
				FROM   TrafficTickets.dbo.Employer
				WHERE  EMPLOYER = ISNULL(@Employer , '')
		)
		IF @EmployerID IS NULL AND ISNULL(@Employer , '') <> ''
		BEGIN
			INSERT INTO TrafficTickets.dbo.Employer
			(Employer)
			VALUES(@Employer)	
			
			SET @EmployerID = SCOPE_IDENTITY()
		end
	END
	ELSE
		BEGIN
			SET @EmployerID = NULL
		END
	
	SET @IDFound = NULL 
	
	IF @Flag_SameAsPrevious = 0
	BEGIN
	    SET @IDFound = NULL  
	    
	    SELECT TOP 1 @IDFound = tta.RecordID,
	           @TicketNum2 = tta.TicketNumber,
	           @AddressFound = tta.Address1,
	           @DOBFound = tta.DOB,
	           @CityFound = tta.City,
	           @ZipFound = tta.ZipCode
	    FROM   TrafficTickets.dbo.tblTicketsArchive tta
	           INNER JOIN TrafficTickets.dbo.tblTicketsViolationsArchive 
	                ttva
	                ON  tta.recordid = ttva.recordid
	    WHERE  tta.LastName = @Last_Name
	           AND tta.FirstName = @First_Name
	           AND DATEDIFF(DAY, ISNULL(tta.DOB, '1/1/1900'), ISNULL(@DOB, '1/1/1900')) = 
	               0
	           AND tta.Zipcode = @Zip
	           AND tta.City = @CityFound
	           AND ttva.CourtLocation = 3036
	           AND tta.TicketNumber = @TicketNum2
	           AND tta.Clientflag = 0 AND ttva.violationstatusid <> 80	-- Do not associate new cases with client, quote and disposed cases
	    
	   
	    
	    IF @IDFound IS NULL
	    BEGIN
	        INSERT INTO TrafficTickets.dbo.tblTicketsArchive
	          (
	            RecLoadDate,
	            ListDate,
	            BondDate,
	            TicketNumber,
	            MidNumber,
	            FirstName,
	            LastName,
	            Race,
	            Gender,
	            DOB,
	            Height,
	            WEIGHT,
	            Address1,
	            Address2,
	            City,
	            StateID_FK,
	            ZipCode,
	            DP2,
	            DPC,
	            CourtID,
	            GroupID,
	            DLNumber,
	            officerNumber_Fk,
	            OfficerName,
	            InsertionLoaderID,
	            Eyes,
	            HairColor,
	            EmployerID,
	            DLStateID,
	            CourtNumber,
	            flag1,
	            ViolationDate -- Waqas 7128 12/16/2009 offense date
	          )
	        VALUES
	          (
	            @Record_Date,
	            @List_Date,
	            @Bond_Date,
	            @Ticket_Number,
	            @Ticket_Number,
	            @First_Name,
	            @Last_Name,
	            @Race,
	            @Gender,
	            @DOB,
	            @Height,
	            @Weight,
	            @Address_Part_I,
	            @Address_Part_II,
	            @City,
	            @StateID,
	            @ZIP,
	            @DP2,
	            @DPC,
	            3036,
	            @GroupID,
	            @DLNumber,
	            @OfficerCode,
	            @Officer_Last_Name,
	            @LoaderID,
	            @Eyes,
	            @HairColor,
	            @EmployerID,
	            @DLStateID,
	            @CourtNumber,
	            @AddressStatus,
	            @OffenseDate	-- Waqas 7128 12/16/2009 offense date
	            
	          )  
	        
	        SET @Flag_Inserted = 1  
	        
	        SET @CurrentRecordID = SCOPE_IDENTITY()
	    END
	END
	ELSE
	BEGIN
	    SET @CurrentRecordID = @PreviousRecordID
	END  
	
	
	INSERT INTO TrafficTickets.dbo.tblTicketsViolationsArchive
	      (
	        RecordID,
	        InsertDate,
	        TicketNumber_Pk,
	        ViolationNumber_PK,
	        CauseNumber,
	        FineAmount,
	        PleaDate,
	        JudgmentDate,
	        CourtLocation,
	        ViolationStatusID,
	        CourtDate,
	        TicketOfficerNumber,
	        ArrestingAgencyName,
	        BondDate,
	        BondingNumber,
	        BondingCompanyName,
	        BondAmount,
	        FTAIssueDate,	        
	        UpdatedDate,
	        LoaderUpdateDate,
	        FiledDate,
	        ViolationDescription,
	        ViolationCode,
	        TicketViolationDate, -- Waqas 7128 12/16/2009 offense date
	        ArrestDate, -- Waqas 7111 12/09/2009 using arrest date
	        AttorneyName,
	        BarCardNumber,
	        GroupID,
	        InsertionLoaderID,
	        Courtnumber,
	        ChargeLevel
	        
	      )
	    VALUES
	      (
	        @CurrentRecordID,
	        GETDATE(),
	        @Ticket_Number,
	        @ViolationID,
	        @Cause_Number,
	        @Fine_Amount,
	        @Plea_Date,
	        @Judgement_Date,
	        3036,
	        @ViolationStatusID,
	        ISNULL(@Court_Date, '1/1/1900'),
	        @OfficerCode,
	        @Arresting_Agency_Name,
	        @Bond_Date,
	        @Bonding_Number,
	        @Bonding_Company_Name,
	        @Bond_Amount,
	        @FTA_Issue_Date,	        
	        getdate(),
	        GETDATE(),	
	        @File_Date,
	        @Violation_Description,
	        @Violation_Code,
	        @OffenseDate, -- Waqas 7128 12/16/2009 offense date
	        @ArrestDate,	-- Waqas 7111 12/09/2009 using arrest date
	        @Attorney_Name,
	        @Bar_Card_Number,
	        @GroupID,
	        @LoaderID,
	        @CourtNumber,
	        @ChargeLevel
	      )  
	    
	    SET @Flag_Inserted = 1
	
	

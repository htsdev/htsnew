﻿






/******  
* Created By :	  Afaq Ahmed
* Create Date :   6/11/2011 3:35:27 PM 
* Task ID :		  7827
* Business Logic : Get the loader summary log on the basis of loader name and loader Id  
* List of Parameter :
	@LoaderName Loader Name,
	CONVERT(VARCHAR,@GroupId) Loader Id,
	@TotalRecords Total record count,
	@ReliableRecords Reliable record count
* Column Return : Loader Information.    
******/
--USP_HTS_LA_GET_SummaryLog 'abc',59255,7,7,2
ALTER PROCEDURE [dbo].[USP_HTS_LA_GET_SummaryLog] 
	@LoaderName VARCHAR(100),
	@GroupId INT,
	@TotalRecords INT,
	@ReliableRecords INT,
	@DatbaseId INT
AS
	DECLARE @DatabaseName VARCHAR(20)
	 
	-- Farrukh 9715 10/17/2011 Insert/Update count issue fixed.
	SET @DatabaseName=CASE WHEN @DatbaseId=1 THEN 'TrafficTickets' ELSE 'DallasTrafficTickets' END
	DECLARE @sql VARCHAR(MAX)
	SET @sql=''
	SET @sql=@sql+'
	DECLARE @DataFileDate SMALLDATETIME
	DECLARE @LoaderID INT
	DECLARE @YAddressCount INT
	DECLARE @DAddressCount INT
	DECLARE @SAddressCount INT
	DECLARE @NAddressCount INT
	DECLARE @NonClientInsertedCount INT
	DECLARE @NonClientUpdatedCount INT
	DECLARE @NonClientTotalCount INT
	DECLARE @NonClientNotUpdatedCount INT 
	DECLARE @FileName VARCHAR(200)
	DECLARE @NonReliableRecords INT
	DECLARE @EmailBody VARCHAR(MAX)
	DECLARE @tblStatus TABLE (DESCRIPTION VARCHAR(100),[VALUES] VARCHAR(200)) -- Rab Nawaz Khan 10729 03/20/2013 Increase the Column Lenght. . . 
	DECLARE @ClientUpdateCount INT 
	DECLARE @Summmary VARCHAR(MAX)
	DECLARE @CRLF char(2)     
	
	SELECT TOP 1 @DataFileDate = ISNULL(Load_Date, GETDATE()),@LoaderID = LoaderID FROM tbl_hts_la_group WHERE groupid = ' + CONVERT(VARCHAR(5),@GroupId) + '
	
	
	SET @CRLF=CHAR(13)+CHAR(10)
	SET @YAddressCount = 0
	SET @DAddressCount = 0
	SET @SAddressCount = 0
	SET @NAddressCount = 0
	-- Rab Nawaz Khan 10529 11/02/2012 Incorrect syntax near ''*'' issue has been Fixed. . . 
	SET @NonClientTotalCount = ' + CONVERT(VARCHAR(10), @ReliableRecords) + -- Adil 9618 08/25/2011 Total count issue fixed.
	'SET @NonClientUpdatedCount = 0
	SET @NonClientInsertedCount = 0
	SET @ClientUpdateCount =0
	SET @Summmary=''''
	-- Rab Nawaz 10729 03/25/2013 Added NISI Cases Loader Email format. . .
	IF ('''+@LoaderName+''' = ''NISI Cases Loader'')
	BEGIN
		DECLARE @TotalRecordsAgainstBondingNumber INT
		DECLARE @TotalNisiRecords INT
		SET @TotalRecordsAgainstBondingNumber = 0
		SET @TotalNisiRecords = 0
		
		INSERT INTO @tblStatus VALUES (@CRLF+'' Loader Name : '','''+@LoaderName+''')
		INSERT INTO @tblStatus VALUES (@CRLF+'' Load Date : '', CONVERT(VARCHAR(12),GETDATE(),101))
		
		SET @TotalNisiRecords = (SELECT COUNT(nisi.GroupId) FROM LoaderFilesArchive.dbo.NISI_DataArchiveTable nisi WHERE nisi.GroupID='+CONVERT(VARCHAR,@GroupId)+' )
		INSERT INTO @tblStatus values(@CRLF+'' Total No. of Records : '', convert(varchar(5), @TotalNisiRecords))
		
		SET @TotalRecordsAgainstBondingNumber = (SELECT COUNT(nisi.RowID) FROM LoaderFilesArchive.dbo.NISI_DataArchiveTable nisi WHERE nisi.GroupID='+CONVERT(VARCHAR,@GroupId)+' AND nisi.[Bonding license number] = ''01818976'')
		INSERT INTO @tblStatus values(@CRLF+'' Total Number Of Records get Against Bonding Number "01818976" : '', convert(varchar(5), @TotalRecordsAgainstBondingNumber))
		
		INSERT INTO @tblStatus values(@CRLF+'' No of Inserted Records : '', '+CONVERT(VARCHAR,@TotalRecords)+ ')
		 
		INSERT INTO @tblStatus values(@CRLF+'' No of Updated Records : '', '+CONVERT(VARCHAR,@ReliableRecords)+ ')
		
		INSERT INTO @tblStatus
		SELECT @CRLF+'' File No : '','+CONVERT(VARCHAR,@GroupId)+'
		INSERT INTO @tblStatus 
		SELECT @CRLF+'' File Name : '',thlg.SourceFileName FROM Tbl_HTS_LA_Group thlg 
		WHERE  thlg.GroupID = '+CONVERT(VARCHAR,@GroupId)+'
		INSERT INTO @tblStatus VALUES (@CRLF+'' Load By : '',''Loader Service'')
	END	
	-- Rab Nawaz 10729 03/25/2013 Added Houston Docket Extract Loader Email format. . .
	ELSE IF ('''+@LoaderName+''' = ''Houston Docket Extract Loader'')
	BEGIN
		INSERT INTO @tblStatus VALUES (@CRLF+'' Loader Name : '','''+@LoaderName+''')
		INSERT INTO @tblStatus VALUES (@CRLF+'' Load Date : '', CONVERT(VARCHAR(12),GETDATE(),101))
		
		INSERT INTO @tblStatus values(@CRLF+'' Total No. of Records : '', '+CONVERT(VARCHAR,@TotalRecords)+ ')
		INSERT INTO @tblStatus values(@CRLF+'' No of Uploaded Records  : '', '+CONVERT(VARCHAR,@TotalRecords)+ ')
		
		INSERT INTO @tblStatus
		SELECT @CRLF+'' File No : '','+CONVERT(VARCHAR,@GroupId)+'
		INSERT INTO @tblStatus 
		SELECT @CRLF+'' File Name : '',thlg.SourceFileName FROM Tbl_HTS_LA_Group thlg 
		WHERE  thlg.GroupID = '+CONVERT(VARCHAR,@GroupId)+'
		INSERT INTO @tblStatus VALUES (@CRLF+'' Load By : '',''Loader Service'')
	END	
	ELSE
	BEGIN			
	INSERT INTO @tblStatus VALUES (@CRLF+'' Load Date : '', CONVERT(VARCHAR(12),GETDATE(),101))
	INSERT INTO @tblStatus VALUES (@CRLF+'' Loader Name : '','''+@LoaderName+''')
	INSERT INTO @tblStatus VALUES (@CRLF+'' Total No. of Records : '','+CONVERT(VARCHAR(10),@TotalRecords)+')
	INSERT INTO @tblStatus VALUES (@CRLF+'' Reliable Records : '','+CONVERT(VARCHAR(10),@ReliableRecords)+')
	INSERT INTO @tblStatus VALUES (@CRLF+'' Non-Reliable Records : '','+CONVERT(VARCHAR(10),@TotalRecords-@ReliableRecords)+')
	--Farrukh 10438 12/05/2012 Null check added when checking address flag field
	SELECT @YAddressCount = @YAddressCount + CASE 
	                                              WHEN (isnull(tta.flag1,'''') = ''Y'') THEN 1
	                                              ELSE 0
	                                         END,
	       @DAddressCount = @DAddressCount + CASE 
	                                              WHEN (isnull(tta.flag1,'''') = ''D'') THEN 1
	                                              ELSE 0
	                                         END,
	       @SAddressCount = @SAddressCount + CASE 
	                                              WHEN (isnull(tta.flag1,'''') = ''S'') THEN 1
	                                              ELSE 0
	                                         END,
	       @NAddressCount = @NAddressCount + CASE 
	                                              WHEN (isnull(tta.flag1,'''') = ''N'') THEN 1
	                                              ELSE 0
	                                         END
	FROM   '+@DatabaseName+'.dbo.tblTicketsArchive tta
	WHERE  tta.GroupID = '+CONVERT(VARCHAR,CONVERT(VARCHAR,@GroupId))+'
	INSERT INTO @tblStatus VALUES (@CRLF+'' Address Verified Records (Y): '',@YAddressCount)
	INSERT INTO @tblStatus VALUES (@CRLF+'' Address Verified Records (D): '',@DAddressCount)
	INSERT INTO @tblStatus VALUES (@CRLF+'' Address Verified Records (S): '',@SAddressCount)
	INSERT INTO @tblStatus VALUES (@CRLF+'' Address Verified Records (N): '',@NAddressCount)
	-- For Non Clients
	INSERT INTO @tblStatus values(@CRLF+'' For Non-Clients : '', '''')
	
	SELECT @NonClientUpdatedCount = @NonClientUpdatedCount + CASE 
																WHEN DATEDIFF(minute, @DataFileDate, ttva.loaderupdatedate) <= 40 THEN 1
																ELSE 0
															 END
	FROM  '+@DatabaseName+'.dbo.tblTicketsViolationsArchive ttva
	WHERE  ttva.GroupID = '+CONVERT(VARCHAR,@GroupId)+'
	AND UpdationLoaderId = @LoaderID
		       
	SELECT @NonClientInsertedCount = @NonClientInsertedCount + CASE 
																	WHEN DATEDIFF(minute, ttva.InsertDate, @DataFileDate) <= 40 THEN 1
																	ELSE 0
															   END
	FROM  '+@DatabaseName+'.dbo.tblTicketsViolationsArchive ttva
	WHERE  ttva.GroupID = '+CONVERT(VARCHAR,@GroupId)+'
	AND InsertionLoaderId = @LoaderID

	INSERT INTO @tblStatus values(@CRLF+'' No of Inserted Records : '', convert(varchar(5), @NonClientInsertedCount) + '' '')
	INSERT INTO @tblStatus 
	SELECT @CRLF + '' ''+ tcvs.[Description] + '' '', convert(varchar(5), COUNT(ttva.RowID)) + '' ''
	FROM '+@DatabaseName+'.dbo.tblTicketsViolationsArchive ttva 
	INNER JOIN '+@DatabaseName+'.dbo.tblCourtViolationStatus tcvs ON ttva.violationstatusid=tcvs.CourtViolationStatusID
	WHERE ttva.GroupID ='+CONVERT(VARCHAR,@GroupId)+' AND DATEDIFF(DAY, ttva.InsertDate, GETDATE()) = 0 AND ttva.InsertionLoaderId = @LoaderID GROUP BY tcvs.[Description]	
	INSERT INTO @tblStatus values(@CRLF+'' No of Updated Records : '', convert(varchar(5), @NonClientUpdatedCount) + '' '')
	INSERT INTO @tblStatus 
	
	SELECT @CRLF + '' ''+ tcvs.[Description] + '' '', convert(varchar(5), COUNT(ttva.RowID)) + '' ''
	FROM '+@DatabaseName+'.dbo.tblTicketsViolationsArchive ttva 
	INNER JOIN '+@DatabaseName+'.dbo.tblCourtViolationStatus tcvs ON ttva.violationstatusid=tcvs.CourtViolationStatusID
	WHERE ttva.GroupID ='+CONVERT(VARCHAR,@GroupId)+' AND DATEDIFF(DAY, @DataFileDate, ttva.loaderupdatedate)= 0 AND ttva.UpdationLoaderId = @LoaderID GROUP BY tcvs.[Description]	  
	INSERT INTO @tblStatus values(@CRLF+'' No of Not Updated Records : '', @NonClientTotalCount-(@NonClientInsertedCount + @NonClientUpdatedCount))
 
	---------------------------------------
	
	--For Clients
	SET @ClientUpdateCount = (SELECT COUNT(*) FROM '+@DatabaseName+'.dbo.tblTicketsViolations ttv WHERE ttv.GroupID='+CONVERT(VARCHAR,@GroupId)+')
	INSERT INTO @tblStatus values(@CRLF+'' For Client/Qoute : '', '''')
	INSERT INTO @tblStatus values(@CRLF+'' No of Updated : '', convert(varchar(5), @ClientUpdateCount) + '' '')
	INSERT INTO @tblStatus 
	SELECT @CRLF+'' ''+tcvs.[Description]+'': '', convert(varchar(5), COUNT(ttv.TicketsViolationID)) + '' ''
	FROM '+@DatabaseName+'.dbo.tblTicketsViolations ttv 
	INNER JOIN '+@DatabaseName+'.dbo.tblCourtViolationStatus tcvs ON ttv.ViolationStatusID = tcvs.CourtViolationStatusID
	WHERE ttv.GroupID='+CONVERT(VARCHAR,@GroupId)+' GROUP BY tcvs.[Description]
	INSERT INTO @tblStatus
	SELECT @CRLF+'' File No : '','+CONVERT(VARCHAR,@GroupId)+'
	INSERT INTO @tblStatus 
	SELECT @CRLF+'' File Name : '',thlg.SourceFileName FROM Tbl_HTS_LA_Group thlg 
	WHERE  thlg.GroupID = '+CONVERT(VARCHAR,@GroupId)+'
	INSERT INTO @tblStatus VALUES (@CRLF+'' Load By : '',''Loader Service'')
	--SELECT * FROM @tblStatus
	END
	SELECT @Summmary=@Summmary+ISNULL ([DESCRIPTION],'''')+ISNULL([VALUES],'''') FROM @tblStatus 
	
	SELECT @Summmary'
	--PRINT @sql
	PRINT @sql
	EXEC (@sql)







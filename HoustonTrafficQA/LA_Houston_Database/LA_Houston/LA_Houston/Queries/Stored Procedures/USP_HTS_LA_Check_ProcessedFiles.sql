﻿USE [LA_Houston]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTS_LA_Check_ProcessedFiles]    Script Date: 08/28/2011 22:39:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*
* Created By : Adil Aleem
* Business Logic: To check datafile is already processed or not on the bases of file name and date.
* Parameters:
* @SourceFileName: Data file name.
* @DataFile_DateTime: Data file modified OR Created date time.
* @IsExist: Output parameter returns result as 0 OR 1.
*/
ALTER procedure [dbo].[USP_HTS_LA_Check_ProcessedFiles]
@SourceFileName varchar(100),
@DataFile_DateTime datetime,
@IsExist int Output
As

select @IsExist= count(GP.GroupID) from Tbl_HTS_LA_Group GP
inner join Tbl_HTS_LA_DataFile_DateTime DF
on GP.SourceFileName = replace(DF.DataFileName, '.pdf', '.txt') -- Adil 9618 08/27/2011 PDF file lookup issue resolved.
Where GP.SourceFileName = replace(@SourceFileName, '.pdf', '.txt')
and (DATEDIFF (DAY, DF.DataFile_DateTime, @DataFile_DateTime) = 0 -- Adil 6490 09/25/2009 excluded time part checking.
OR CHARINDEX ('new_cases_filed', @SourceFileName ,0) > 0 OR CHARINDEX ('outstanding_warrant', @SourceFileName ,0) > 0 OR CHARINDEX ('slpd', @SourceFileName ,0) > 0
-- Zeeshan 10595 12/26/2012 Austin Municipal Loader
OR CHARINDEX ('kalidas', @SourceFileName, 0) > 0)

return @IsExist

﻿set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go



/*
Author: Muhammad Adil Aleem
Business Logic : This Store procedure insert data in TblTicketsArchive and TblTicketsViolationArchive with some formating.
Parameters: 
	@Record_Date: Case record/insert date
	@List_Date: Court list date
	@Name_Last: Defendant's last name
	@Name_First: Defendant's first name
	@Address: Defendant's home address
	@City: Defendant's home city
	@State: Defendant's home state
	@ZIP: Defendant's home zip code
	@DP2: Defendant's barcode part. return by zp4 webservice
	@DPC: Defendant's barcode part. return by zp4 webservice
	@Court_Date: Defendant's court date	
	@violationcode : Violation COde
	@Violation_Description: Violation's title of Defendant
	@GroupID: Data file ID
	@AddressStatus: Address status [y/d/s/n] of defendant
	@Flag_SameAsPrevious: insure wether this case belongs to previous one or not [It helps to cut-off researching the defendant profile]
	@PreviousRecordID: If this case belongs to previous case then previous case ID will be the value of @PreviousRecordID
	@LoaderID: Insure which loader is executing this case.
	@CurrentRecordID: It returns current Case-ID/Record-ID to calling program
	@Flag_Inserted: It returns status wether the record inserted or deleted
*/

ALTER PROCEDURE [dbo].[USP_HTP_LA_Insert_Tickets_FortWorthArraignment]
	@Record_Date DATETIME,
	@List_Date SMALLDATETIME,
	@Ticket_Number VARCHAR(20),
	@Cause_Number AS VARCHAR(20),
	@Name_First AS VARCHAR(50), 
	@Name_Middle AS VARCHAR(50), 
	@Name_Last AS VARCHAR(50), 
	@Address AS VARCHAR(50), 
	@City AS VARCHAR(35), 
	@State AS VARCHAR(10), 
	@zipcode AS VARCHAR(10),
	@DP2 VARCHAR(2),
	@DPC VARCHAR(1),
	@Violation_Date AS SMALLDATETIME,
	@violationcode AS VARCHAR(15),
	@Violation_Description AS VARCHAR(2000),
	@AddressStatus CHAR(1),
	@Flag_SameAsPrevious AS BIT,
	@PreviousRecordID AS INT,
	@LoaderID AS INT,
	@GroupID AS INT, 
	@CurrentRecordID INT OUTPUT,
	@Flag_Inserted INT OUTPUT
	AS
	SET NOCOUNT ON  
	
	DECLARE @StateID AS INT
	DECLARE @ViolationID AS INT
	DECLARE @IDFound AS INT
	DECLARE @TicketNum2 AS VARCHAR(20)
	DECLARE @ChargeAmount AS MONEY
	DECLARE @BondAmount AS MONEY
	DECLARE @Court_Date AS DATETIME
	DECLARE @IsQuoted AS INT

	SET @IsQuoted = 0
	SET @Name_First = UPPER(LEFT(@Name_First, 20))
	SET @Name_Last = UPPER(LEFT(@Name_Last, 20))
	SET @Name_Middle = UPPER(LEFT(@Name_Middle, 1))
	
	SET @Address = REPLACE(@Address, '  ', '')
	SET @Ticket_Number = LTRIM(RTRIM(@Ticket_Number))
	SET @Violation_Description = LTRIM(RTRIM(@Violation_Description))
	SET @Cause_Number = REPLACE(@Cause_Number, ' ', '')
	SET @Flag_Inserted = 0
	SET @ChargeAmount = 0
	SET @BondAmount = 0
	SET @ViolationID = NULL
	
	SET @Court_Date = @Violation_Date + 11
	
--  Adil 8006 07/10/2010 non - business day logic commented.
--	IF DATEPART(dw, @Court_Date) = 7 -- If Saturday then make it Friday (Court date should be in working days)
--	BEGIN
--	    SET @Court_Date = @Court_Date - 1
--	END
--	ELSE 
--	IF DATEPART(dw, @Court_Date) = 1 -- If Sunday then make it Friday (Court date should be in working days)
--	BEGIN
--	    SET @Court_Date = @Court_Date - 2
--	END
	
	Set @Court_Date = convert(datetime,convert(varchar(12),@Court_Date,110) + ' 08:15 AM')
	
	SELECT TOP 1 @ViolationID = ViolationNumber_PK, @ChargeAmount = ISNULL(ChargeAmount, 100), @BondAmount = ISNULL(BondAmount, 175)
	FROM   DallasTrafficTickets.dbo.TblViolations
	WHERE  (
	           DESCRIPTION = @Violation_Description
	           AND CourtLoc = 3059
	           AND ViolationType = 4
	       )
	
	IF @ViolationID IS NULL
	BEGIN
	    INSERT INTO DallasTrafficTickets.dbo.TblViolations
                      (Description, SequenceOrder, IndexKey, IndexKeys, ChargeAmount, BondAmount, Violationtype, ChargeonYesflag, ShortDesc, ViolationCode, 
                      AdditionalPrice, Courtloc)
		VALUES     (@Violation_Description, NULL, 2, 100, 100, 175, 4, 1, 'None',@violationcode, 0, 3059)
	    SET @ChargeAmount = 100
	    SET @BondAmount = 175
	    SET @ViolationID = SCOPE_IDENTITY()
	END
	
	-- Adil 7245 01/16/2010 Fine amount and bond amount changes for speeding tickets.
	IF (CHARINDEX ('speed', @Violation_Description, 0)> 0 OR CHARINDEX ('spd', @Violation_Description, 0) > 0) AND CHARINDEX ('const', @Violation_Description, 0) > 0
	BEGIN
		SET @ChargeAmount = @ChargeAmount + 120 -- Construction zone $12 x 10 Miles
		SET @BondAmount = 0
	END
	ELSE IF (CHARINDEX ('speed', @Violation_Description, 0)> 0 OR CHARINDEX ('spd', @Violation_Description, 0) > 0)
	BEGIN
		SET @ChargeAmount = @ChargeAmount + 60 -- Non-Construction zone $6 x 10 Miles
		SET @BondAmount = 0
	END
	
	SET @StateID = (
	        SELECT TOP 1 StateID
	        FROM   DallasTrafficTickets.dbo.TblState
	        WHERE  STATE = @State
	    )
	
	IF @Flag_SameAsPrevious = 0
	BEGIN
	    SET @IDFound = NULL
	    
	    SELECT Top 1 @IDFound = T.RecordID, @TicketNum2 = TicketNumber, @IsQuoted = T.Clientflag
		From DallasTrafficTickets.dbo.tblTicketsArchive T
		Inner Join DallasTrafficTickets.dbo.tblTicketsViolationsArchive TV
		On T.RecordID = TV.RecordID
		Where 
		((TicketNumber = @Ticket_Number and len(TicketNumber) > 1)
		OR
		(FirstName = @Name_First and LastName = @Name_Last and Address1 = @Address
		and ZipCode = @zipcode and datediff(day,TV.CourtDate, @Court_Date) = 0 )) and TV.CourtLocation = 3059
		AND TV.violationstatusid <> 80	-- Adil 7001 11/14/2009 Do not associate new cases with client, quote and disposed cases
		
	    IF @IDFound IS NULL
	    BEGIN
	        IF LEN(@Ticket_Number) < 1
	        BEGIN
	            INSERT INTO DallasTrafficTickets.dbo.tblIDGenerator(recdate)
	            VALUES(GETDATE())
	            SELECT @Ticket_Number = 'FW' + CONVERT(VARCHAR(12), @@identity)
	        END	
	        
	        INSERT INTO DallasTrafficTickets.dbo.tblTicketsArchive
                      (RecLoadDate, ListDate, CourtDate, TicketNumber, MidNumber, FirstName, Initial, LastName, PhoneNumber, Address1, City, StateID_FK, ZipCode, CourtID, 
                      officerNumber_Fk, OfficerName, flag1, DP2, DPC, InsertionLoaderID, GroupID)
			VALUES     (@Record_Date,@List_Date,@Court_Date,@Ticket_Number,@Ticket_Number,@Name_First, @Name_Middle, @Name_Last, 
                      '0000000000',@Address,@City,@StateID,@zipcode, 3059, 962528, 'N/A',@AddressStatus,@DP2,@DPC,@LoaderID,@GroupID)
	        
	        SET @CurrentRecordID = SCOPE_IDENTITY()
	    END
	    ELSE
	    BEGIN
	        SET @CurrentRecordID = @IDFound
	        IF LEN(@Ticket_Number) < 1
	        BEGIN
	            SET @Ticket_Number = @TicketNum2
	        END
	    END
	END
	ELSE
	BEGIN
	    SET @CurrentRecordID = @PreviousRecordID
	END
	
	SET @IDFound = NULL
	SET @IDFound = (SELECT TOP 1 ISNULL(RowID, 0)
	        FROM   DallasTrafficTickets.dbo.tblTicketsViolationsArchive
	        WHERE  (CauseNumber = @Cause_Number and CourtLocation = 3059 )) --Afaq 7380 03/258/2010
	
	IF @IDFound IS NULL
	BEGIN
		IF @IsQuoted = 1 -- Adil 7380 02/05/2010 if current client is quoted client and coming violation is different from existing then insert new ticket.
			BEGIN
				IF LEN(@Ticket_Number) < 1
				BEGIN
					INSERT INTO DallasTrafficTickets.dbo.tblIDGenerator(recdate)
					VALUES(GETDATE())
					SELECT @Ticket_Number = 'FW' + CONVERT(VARCHAR(12), @@identity)
				END	
		        
				INSERT INTO DallasTrafficTickets.dbo.tblTicketsArchive
						  (RecLoadDate, ListDate, CourtDate, TicketNumber, MidNumber, FirstName, Initial, LastName, PhoneNumber, Address1, City, StateID_FK, ZipCode, CourtID, 
						  officerNumber_Fk, OfficerName, flag1, DP2, DPC, InsertionLoaderID, GroupID)
				VALUES     (@Record_Date,@List_Date,@Court_Date,@Ticket_Number,@Ticket_Number,@Name_First, @Name_Middle, @Name_Last, 
						  '0000000000',@Address,@City,@StateID,@zipcode, 3059, 962528, 'N/A',@AddressStatus,@DP2,@DPC,@LoaderID,@GroupID)
		        
				SET @CurrentRecordID = SCOPE_IDENTITY()
			END

	    INSERT INTO DallasTrafficTickets.dbo.tblTicketsViolationsArchive
                      (TicketNumber_PK, ViolationNumber_PK, CauseNumber, CourtDate, InsertDate, TicketViolationDate, ViolationDescription, ViolationCode, FineAmount, BondAmount, 
                      CourtLocation, RecordID, violationstatusid, InsertionLoaderId, GroupID)
		VALUES     (@Ticket_Number,@ViolationID,@Cause_Number,@Court_Date, GETDATE(), @Violation_Date, @Violation_Description,@violationcode,@ChargeAmount,@BondAmount, 
                      3059,@CurrentRecordID, 116,@LoaderID,@GroupID)
	    SET @Flag_Inserted = 1
	END

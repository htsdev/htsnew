set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


CREATE PROCEDURE [dbo].[USP_HTS_LA_Update_DownloadPath]
@AttachmentPath varchar(300),
@SavePath varchar(300)
AS
BEGIN
	update Tbl_HTS_LA_ConfigurationSetting set AttachmentDownloadPath=@AttachmentPath,SaveDownloadPath=@SavePath where ConfigurationId=1
END


grant execute on dbo.USP_HTS_LA_Update_DownloadPath to dbr_webuser
﻿USE [LA_Houston]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTS_LA_InsertOrUpdate_NisiCases]    Script Date: 03/21/2013 15:48:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
* Business Logic: To Upload NISI Cases FROM Loader Service
* Author: Rab Nawaz Khan 
* Created date : 03/18/2013 
* Task ID:	10729 
* Type : Loader Service

Parameters: 
	@Nisi_CauseNumber: NISI Cause Number
	@Nisi_bonding_LicenseNumber : bonding license number which must be only sullo and sullo. . . 
	@Name_First: First Name of client
	@Name_Last: last name of client
	@Name_Middle: middle name of client
	@Address1: primary address of client
	@Address2: secondry address of client
	@City: city of client
	@State: state of client
	@ZIP: zip code of client where he/she resides
	@Nisi_Violation_Short_Description: short violation description
	@Nisi_Violation_Description: violation description
	@GroupID: group id
	@uderlying_TicketNumber: ticket number on which the system will lookup the client if NISI cause number not found in system
	@underlying_CauseNumber: cause number on which the system will lookup the client if NISI cause number not found in system
	@Bond_Forfeiture_Amount: NISI violation amount
	@NISI_IssueDate: NISI issue date
	@LoaderID: Loader of NISI cases loader 
	@Flag_Inserted: return 1 if recored inserted else 0
	@Flag_Update: return 1 if recored updated else 0
*/
-- [dbo].[USP_HTS_LA_InsertOrUpdate_NisiCases]  '2013 NISI 0001123', '01818976', 'REBA', 'HICKS', 'V', '1825 FOOTE ST', NULL, NULL, 'TX', NULL, 'NISISB', 'NISI - SURETY BOND', 0, '118498423', '2008 TR 0968017', 327.50, '03/16/2013', 0, 0, 0, 0, 0

CREATE PROCEDURE [dbo].[USP_HTS_LA_InsertOrUpdate_NisiCases]  
@Nisi_CauseNumber AS VARCHAR(20),
@Nisi_bonding_LicenseNumber VARCHAR (20),
@Name_First AS VARCHAR(50),  
@Name_Last AS VARCHAR(50),  
@Name_Middle AS VARCHAR(10), 
@Address1 AS VARCHAR(50),
@Address2 AS VARCHAR(50),  
@City AS VARCHAR(35),  
@State AS VARCHAR(10),
@ZIP AS VARCHAR(10),  
@Nisi_Violation_Short_Description AS VARCHAR(500),
@Nisi_Violation_Description AS VARCHAR (2000),
@GroupID AS INT,
@uderlying_TicketNumber VARCHAR (50),
@underlying_CauseNumber VARCHAR (50),
@Bond_Forfeiture_Amount MONEY,
@NISI_IssueDate DATETIME,
@LoaderID AS INT,
@Flag_Inserted INT OUTPUT,
@Flag_Update INT OUTPUT
AS

SET NOCOUNT ON  

--DECLARE @StateID AS INT
DECLARE @NisiCaseAlreadyExists AS INT
DECLARE @NisiCaseNotExists AS INT
DECLARE @violationRowId INT
DECLARE @ChargeAmount AS money
DECLARE @BondAmount AS money
DECLARE @Court_Date SMALLDATETIME
DECLARE @courtNumMain VARCHAR(10)
DECLARE @courtNumAuto VARCHAR(10)
DECLARE @CourtId VARCHAR (10)
DECLARE @ClientFound BIT
SET @ClientFound = 0


-- getting the violation Id if violation already exists in DB. . . 
DECLARE @ViolationID AS INT
DECLARE @courtViolationType INT
DECLARE @clientviolationDesc VARCHAR (200)
SET @ViolationID = 17552 -- Setting the Violation type as N/A for NISI. . . 
SET @courtViolationType = NULL
SET @Nisi_Violation_Short_Description = ltrim(rtrim(@Nisi_Violation_Short_Description))
SELECT @clientviolationDesc = tv.[Description] FROM TrafficTickets.dbo.tblViolations tv WHERE tv.ViolationNumber_PK = @ViolationID

SELECT @courtViolationType = tcvs.CourtViolationStatusID FROM TrafficTickets.dbo.tblCourtViolationStatus tcvs WITH(NOLOCK) WHERE tcvs.[Description] LIKE  @Nisi_Violation_Description

IF @courtViolationType IS NULL
BEGIN
	INSERT INTO TrafficTickets.dbo.tblCourtViolationStatus ([Description], violationcategory, ShortDescription, CategoryID, SortOrder, 
	CourtStatusUpdateDate, StatusType, DispositionType)
	VALUES (@Nisi_Violation_Description, 0, 'NIS', 51, 0, GETDATE(), 0, 0)
	SET @courtViolationType = SCOPE_IDENTITY()
END	

-- Removing the Space between cause numbers. . . 
SET @Nisi_CauseNumber = REPLACE(@Nisi_CauseNumber, ' ', '')
SET @uderlying_TicketNumber = REPLACE(@uderlying_TicketNumber, ' ', '')
SET @underlying_CauseNumber = REPLACE(@underlying_CauseNumber, ' ', '')
SET @Address1 = Replace(@Address1, '  ', ' ')
SET @Nisi_Violation_Description = ltrim(rtrim(@Nisi_Violation_Description))
SET @Nisi_Violation_Short_Description = ltrim(rtrim(@Nisi_Violation_Short_Description))
SET @Name_First = LTRIM(RTRIM(UPPER(left(@Name_First,20))))
SET @Name_Last = LTRIM(RTRIM(UPPER(left(@Name_Last,20))))
SET @Name_Middle = LTRIM(RTRIM(UPPER(left(@Name_Middle,1))))


IF LEN(@Name_Middle)>5
	BEGIN
		SET @Name_Middle = left(@Name_Middle,1)
	END


SET @Flag_Inserted = 0
SET @Flag_Update = 0
--SET @StateID = (SELECT TOP 1 StateID FROM TrafficTickets.dbo.TblState WHERE State = @State)
SET @NisiCaseAlreadyExists = NULL
SET @violationRowId = NULL

	SELECT TOP 1 @NisiCaseAlreadyExists = t.TicketID_PK, @violationRowId = v.TicketsViolationID
	FROM TrafficTickets.dbo.tbltickets t INNER JOIN TrafficTickets.dbo.tblTicketsViolations v
				ON t.TicketID_PK = v.TicketID_PK
	WHERE ISNULL(t.Activeflag, 0) = 1 -- Only Clients. . . 
	AND v.CourtID IN (SELECT c.courtid FROM TrafficTickets.dbo.tblcourts c WHERE c.CourtCategorynum = 1) -- All HMC Courts. . . 
	--AND ISNULL(t.Firstname, '') = ISNULL(@Name_First, '') AND ISNULL(t.Lastname, '') = ISNULL(@Name_Last, '')
	AND ((ISNULL(v.casenumassignedbycourt, 'N/A') = @Nisi_CauseNumber) OR (ISNULL(v.RefCaseNumber, 'N/A') = @Nisi_CauseNumber))
	
	-- If NISI does not Exists Already then Search though Underlying ticket or cause number. . . 
	IF ((@NisiCaseAlreadyExists IS NULL) AND (@violationRowId IS NULL))
		BEGIN
			SELECT TOP 1 @NisiCaseNotExists = t.TicketID_PK, @violationRowId = v.TicketsViolationID, @CourtId = v.CourtID
			FROM TrafficTickets.dbo.tbltickets t INNER JOIN TrafficTickets.dbo.tblTicketsViolations v
						ON t.TicketID_PK = v.TicketID_PK
			WHERE ISNULL(t.Activeflag, 0) = 1 -- Only Clients. . . 
			AND v.CourtID IN (SELECT c.courtid FROM TrafficTickets.dbo.tblcourts c WHERE c.CourtCategorynum = 1) -- All HMC Courts. . . 
			--AND ISNULL(t.Firstname, '') = ISNULL(@Name_First, '') AND ISNULL(t.Lastname, '') = ISNULL(@Name_Last, '')
			AND ((ISNULL(v.casenumassignedbycourt, 'N/A') = @underlying_CauseNumber) OR (ISNULL(v.RefCaseNumber, 'N/A') = @uderlying_TicketNumber))
			-- When find the client ticket number through underlying cause or ticket number. . . 
			IF ((@NisiCaseNotExists IS NOT NULL) AND (@violationRowId IS NOT NULL))
			BEGIN
				INSERT INTO TrafficTickets.dbo.tblTicketsViolations ( Ticketid_pk, ViolationNumber_PK, FineAmount, RefCaseNumber, casenumassignedbycourt, 
				ViolationDescription, CourtDate, CourtDateMain, CourtViolationStatusID, CourtViolationStatusIDmain, CourtNumber, CourtNumbermain, CourtID, 
				TicketViolationDate, GroupID)
				VALUES (@NisiCaseNotExists, @ViolationID, 0, @Nisi_CauseNumber, @Nisi_CauseNumber, UPPER(@clientviolationDesc), '01/01/1900', '01/01/1900',
				@courtViolationType, @courtViolationType, 0, 0, @CourtId, @NISI_IssueDate, @GroupID) 
				
				
				INSERT INTO [TrafficTickets].[dbo].[tblNISI_ClientsUploadedByLoader]
			   ([TicketId] ,[First Name] ,[Middle Name] ,[Last Name] ,[Address 1] ,[NisiCauseNumber] ,[UnderlyingTicketNumber] ,[UnderlyingCauseNumber]
			   ,[InsertionLoaderId] ,[InsertionGroupId] ,[InsertDate] ,[NISI Issue Date]
			   ,[BondForfeitureAmount] ,[IsInsertedOrUpdatedInClientDB])
				VALUES (@NisiCaseNotExists, UPPER(@Name_First), UPPER(@Name_Middle), UPPER(@Name_Last), UPPER(@Address1), @Nisi_CauseNumber, @uderlying_TicketNumber, @underlying_CauseNumber,
				@LoaderID, @GroupID, GETDATE(), @NISI_IssueDate, @Bond_Forfeiture_Amount, 1)	
				-- updating the insert flag if recored inserted. . . 
				
				INSERT INTO [TrafficTickets].[dbo].[tblTicketsNotes] ([TicketID] ,[Subject] ,[Recdate] ,[EmployeeID])
				VALUES  (@NisiCaseNotExists, 'NEW VIOLATION ADDED : CASE # ' + Convert(VARCHAR(50), @Nisi_CauseNumber), GETDATE(), 3992) 
				
				SET @Flag_Inserted = 1
				SET @ClientFound = 1
			END
		END
	ELSE
		BEGIN 
			-- Updating the Violaiotn info if NISI Cause NUMBER found. . . 
			UPDATE TrafficTickets.dbo.tblticketsviolations
			SET CourtViolationStatusID = @courtViolationType, UpdatedDate = GETDATE(), GroupID = @GroupID, loaderupdatedate = GETDATE(), ViolationNumber_PK =  @ViolationID
			WHERE TicketID_PK = @NisiCaseAlreadyExists
			AND TicketsViolationID = @violationRowId
			-- updating the update flag if recored updated. . . 
			
			-- Checking if NISI Cause number found in client DB, then check if the same NISI exists in NISI Table then update loader info otherwise Insert in NISI Table. . . 
			IF EXISTS (SELECT nisi.TicketId FROM [TrafficTickets].[dbo].[tblNISI_ClientsUploadedByLoader] nisi WHERE nisi.TicketId = @NisiCaseAlreadyExists AND  nisi.NisiCauseNumber = @Nisi_CauseNumber)
			BEGIN
				UPDATE [TrafficTickets].[dbo].[tblNISI_ClientsUploadedByLoader]
				SET [UpdationLoaderId] = @LoaderID ,[UpdationGroupId] = @GroupID ,[UpdateDate] = GETDATE() ,[NISI Issue Date] = @NISI_IssueDate,
				[BondForfeitureAmount] = @Bond_Forfeiture_Amount, [IsInsertedOrUpdatedInClientDB] = 1
				WHERE TicketId = @NisiCaseAlreadyExists
				AND NisiCauseNumber = @Nisi_CauseNumber
			END
			ELSE
			BEGIN
				INSERT INTO [TrafficTickets].[dbo].[tblNISI_ClientsUploadedByLoader]
			   ( [TicketId], [First Name] ,[Middle Name] ,[Last Name] ,[Address 1] ,[NisiCauseNumber] ,[UnderlyingTicketNumber] ,[UnderlyingCauseNumber]
			   ,[InsertionLoaderId] ,[InsertionGroupId] ,[InsertDate] ,[NISI Issue Date] ,[BondForfeitureAmount] ,[IsInsertedOrUpdatedInClientDB])
				VALUES (@NisiCaseAlreadyExists,  UPPER(@Name_First), UPPER(@Name_Middle), UPPER(@Name_Last), UPPER(@Address1), @Nisi_CauseNumber, @uderlying_TicketNumber, @underlying_CauseNumber, @LoaderID,
			   @GroupID, GETDATE(), @NISI_IssueDate, @Bond_Forfeiture_Amount, 1)	
			END
			
			--INSERT INTO [TrafficTickets].[dbo].[tblTicketsNotes] ([TicketID] ,[Subject] ,[Recdate] ,[EmployeeID])
			--VALUES  (@NisiCaseNotExists, 'NEW VIOLATION ADDED : CASE # ' + Convert(VARCHAR(50), @Nisi_CauseNumber), GETDATE(), 3992)
			
			SET @Flag_Update = 1
			SET @ClientFound = 1
		END
		-- If NISI Cause number not found in System and also not found through Underlying Cause number  or Underlaying Ticket Number
		-- then insert in the NISI Table but with IsInsertedOrUpdatedInClientDB flag as 0
		IF (@ClientFound = 0)
		BEGIN
			INSERT INTO [TrafficTickets].[dbo].[tblNISI_ClientsUploadedByLoader]
           ( [First Name] ,[Middle Name] ,[Last Name] ,[Address 1] ,[NisiCauseNumber] ,[UnderlyingTicketNumber] ,[UnderlyingCauseNumber]
           ,[InsertionLoaderId] ,[InsertionGroupId] ,[InsertDate] ,[NISI Issue Date] ,[BondForfeitureAmount] ,[IsInsertedOrUpdatedInClientDB])
			VALUES ( UPPER(@Name_First), UPPER(@Name_Middle), UPPER(@Name_Last), UPPER(@Address1), @Nisi_CauseNumber, @uderlying_TicketNumber, @underlying_CauseNumber, @LoaderID,
           @GroupID, GETDATE(), @NISI_IssueDate, @Bond_Forfeiture_Amount, 0)
		END

GO
GRANT EXECUTE ON [dbo].[USP_HTS_LA_InsertOrUpdate_NisiCases] TO dbr_webuser
GO
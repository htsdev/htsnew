﻿set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go






/*
Author: Muhammad Adil Aleem
Business Logic : This Store procedure insert data in TblTicketsArchive and TblTicketsViolationArchive with some formating.
Parameters: 
	@Record_Date: Case record/insert date
	@List_Date: Court list date
	@Name_Last: Defendant's last name
	@Name_First: Defendant's first name
	@Address: Defendant's home address
	@City: Defendant's home city
	@State: Defendant's home state
	@ZIP: Defendant's home zip code
	@DP2: Defendant's barcode part. return by zp4 webservice
	@DPC: Defendant's barcode part. return by zp4 webservice
	@Bond_Date: Defendant's warrant issue date	
	@Violation_Description: Violation's title of Defendant
	@GroupID: Data file ID
	@AddressStatus: Address status [y/d/s/n] of defendant
	@Flag_SameAsPrevious: insure wether this case belongs to previous one or not [It helps to cut-off researching the defendant profile]
	@PreviousRecordID: If this case belongs to previous case then previous case ID will be the value of @PreviousRecordID
	@LoaderID: Insure which loader is executing this case.
	@CurrentRecordID: It returns current Case-ID/Record-ID to calling program
	@Flag_Inserted: It returns status wether the record inserted or deleted
*/

ALTER PROCEDURE [dbo].[USP_LA_Insert_Tickets_FortWorth_Warrant]
	@Record_Date DATETIME,
	@List_Date SMALLDATETIME,
	@Ticket_Number VARCHAR(20),
	@Cause_Number AS VARCHAR(20),
	@Name_First AS VARCHAR(50), 
	@Name_Middle AS VARCHAR(50), 
	@Name_Last AS VARCHAR(50), 
	@Address AS VARCHAR(50), 
	@City AS VARCHAR(35), 
	@State AS VARCHAR(10), 
	@zipcode AS VARCHAR(10),
	@DP2 VARCHAR(2),
	@DPC VARCHAR(1),
	@Bond_Date AS SMALLDATETIME,
	@Violation_Description AS VARCHAR(2000),
	@AddressStatus CHAR(1),
	@Flag_SameAsPrevious AS BIT,
	@PreviousRecordID AS INT,
	@LoaderID AS INT,
	@GroupID AS INT, 
	@CurrentRecordID INT OUTPUT,
	@Flag_Inserted INT OUTPUT
	AS
	SET NOCOUNT ON  
	
	DECLARE @StateID AS INT
	DECLARE @ViolationID AS INT
	DECLARE @IDFound AS INT
	DECLARE @TicketNum2 AS VARCHAR(20)
	DECLARE @ChargeAmount AS MONEY
	DECLARE @BondAmount AS MONEY
	DECLARE @Court_Date AS DATETIME
	DECLARE @Bond_Date_Found AS DATETIME
	
	SET @Name_First = UPPER(LEFT(@Name_First, 20))
	SET @Name_Last = UPPER(LEFT(@Name_Last, 20))
	SET @Name_Middle = UPPER(LEFT(@Name_Middle, 1))
	
	SET @Address = REPLACE(@Address, '  ', '')
	SET @Flag_Inserted = 0
	SET @ChargeAmount = 0
	SET @BondAmount = 0
	SET @ViolationID = NULL
	SET @zipcode = REPLACE (@zipcode, ' ', '')
	
	SET @Court_Date = @Bond_Date
	
	IF DATEPART(dw, @Court_Date) = 7 -- If Saturday then make it Friday (Court date should be in working days)
	BEGIN
	    SET @Court_Date = @Court_Date - 1
	END
	ELSE 
	IF DATEPART(dw, @Court_Date) = 1 -- If Sunday then make it Friday (Court date should be in working days)
	BEGIN
	    SET @Court_Date = @Court_Date - 2
	END
	
	Set @Court_Date = convert(datetime,convert(varchar(12),@Court_Date,110) + ' 08:00 AM')
	
	SELECT TOP 1 @ViolationID = ViolationNumber_PK, @ChargeAmount = ISNULL(ChargeAmount, 0), @BondAmount = ISNULL(BondAmount, 0)
	FROM   DallasTrafficTickets.dbo.TblViolations
	WHERE  (
	           DESCRIPTION = @Violation_Description
	           AND CourtLoc = 3059
	           AND ViolationType = 4
	       )
	
	IF @ViolationID IS NULL
	BEGIN
	    INSERT INTO DallasTrafficTickets.dbo.TblViolations
                      (Description, SequenceOrder, IndexKey, IndexKeys, ChargeAmount, BondAmount, Violationtype, ChargeonYesflag, ShortDesc, ViolationCode, 
                      AdditionalPrice, Courtloc, CategoryID)
		VALUES     (@Violation_Description, NULL, 2, 100, 100, 175, 4, 1, 'None','99999', 0, 3059, 31)
	    SET @ChargeAmount = 100
	    SET @BondAmount = 175
	    SET @ViolationID = SCOPE_IDENTITY()
	END
	
	-- Adil 7245 01/16/2010 Fine amount and bond amount changes for speeding tickets.
	IF (CHARINDEX ('speed', @Violation_Description, 0)> 0 OR CHARINDEX ('spd', @Violation_Description, 0) > 0) AND CHARINDEX ('const', @Violation_Description, 0) > 0
	BEGIN
		SET @ChargeAmount = @ChargeAmount + 120 -- Construction zone $12 x 10 Miles
		SET @BondAmount = 0
	END
	ELSE IF (CHARINDEX ('speed', @Violation_Description, 0)> 0 OR CHARINDEX ('spd', @Violation_Description, 0) > 0)
	BEGIN
		SET @ChargeAmount = @ChargeAmount + 60 -- Non-Construction zone $6 x 10 Miles
		SET @BondAmount = 0
	END
	
	SET @StateID = (
	        SELECT TOP 1 StateID
	        FROM   DallasTrafficTickets.dbo.TblState
	        WHERE  STATE = @State
	    )
	
	IF @Flag_SameAsPrevious = 0
	BEGIN
	    SET @IDFound = NULL
	    
	    SELECT Top 1 @IDFound = T.RecordID, @TicketNum2 = TicketNumber, @Bond_Date_Found = isnull(BondDate,'1/1/1900')
		From DallasTrafficTickets.dbo.tblTicketsArchive T
		Inner Join DallasTrafficTickets.dbo.tblTicketsViolationsArchive TV
		On T.RecordID = TV.RecordID
		Where 
		TV.CauseNumber = @Cause_Number OR TV.TicketNumber_PK = @Ticket_Number and len(TV.TicketNumber_pk) > 1
		and TV.CourtLocation = 3059
		AND T.Clientflag = 0 AND TV.violationstatusid <> 80	-- Do not associate new cases with client, quote and disposed cases
		
	    IF @IDFound IS NULL
	    BEGIN
	        IF LEN(@Ticket_Number) < 1
	        BEGIN
	            INSERT INTO DallasTrafficTickets.dbo.tblIDGenerator(recdate)
	            VALUES(GETDATE())
	            SELECT @Ticket_Number = 'FW' + CONVERT(VARCHAR(12), @@identity)
	        END	
	        
	        INSERT INTO DallasTrafficTickets.dbo.tblTicketsArchive
                        (RecLoadDate, ListDate, CourtDate, TicketNumber, MidNumber, FirstName, Initial, LastName, PhoneNumber, Address1, 
                        City, StateID_FK, ZipCode, CourtID, BondFlag, officerNumber_Fk, OfficerName, flag1, DP2, DPC, InsertionLoaderID, GroupID)
			VALUES      (@Record_Date,@List_Date,@Court_Date,@Ticket_Number,@Ticket_Number,@Name_First, @Name_Middle, @Name_Last, '0000000000',@Address,
						@City,@StateID,@zipcode, 3059, 1, 962528, 'N/A',@AddressStatus,@DP2,@DPC,@LoaderID,@GroupID)
	        
	        SET @CurrentRecordID = SCOPE_IDENTITY()
	    END
	    ELSE
	    BEGIN
	        SET @CurrentRecordID = @IDFound
    		If datediff(day,@Bond_Date_Found, GetDate()) > 0
			BEGIN
				Update DallasTrafficTickets.dbo.tblTicketsArchive Set BondDate = @Bond_Date, BondFlag = 1, GroupID = @GroupID, UpdationLoaderID = @LoaderID where RecordID = @IDFound
				Set @Flag_Inserted = 2
			End
	        IF LEN(@Ticket_Number) < 1
	        BEGIN
	            SET @Ticket_Number = @TicketNum2
	        END
	    END
	END
	ELSE
	BEGIN
	    SET @CurrentRecordID = @PreviousRecordID
	END
	
	SET @IDFound = NULL
	SET @IDFound = (SELECT TOP 1 ISNULL(RowID, 0)
	        FROM   DallasTrafficTickets.dbo.tblTicketsViolationsArchive
	        WHERE  (CauseNumber = @Cause_Number and CourtLocation = 3059 AND RecordID = @CurrentRecordID))
	
	IF @IDFound IS NULL
	BEGIN
	    INSERT INTO DallasTrafficTickets.dbo.tblTicketsViolationsArchive
                    (TicketNumber_PK, ViolationNumber_PK, CauseNumber, CourtDate, InsertDate, TicketViolationDate, ViolationDescription, ViolationCode, FineAmount, BondAmount, 
                    ViolationBondDate, updateddate, CourtLocation, RecordID, violationstatusid, InsertionLoaderId, GroupID)
		VALUES      (@Ticket_Number,@ViolationID,@Cause_Number,@Court_Date, GETDATE(), @Bond_Date, @Violation_Description,'99999',@ChargeAmount + 75, @BondAmount, 
					@Bond_Date, GETDATE(), 3059, @CurrentRecordID, 116, @LoaderID, @GroupID)
	    SET @Flag_Inserted = 1
	END
	ELSE
	Begin
		If datediff(day,@Bond_Date_Found, @Bond_Date) <> 0
		Begin
			Update DallasTrafficTickets.dbo.tblTicketsViolationsArchive SET FineAmount = @ChargeAmount + 75, BondAmount = @BondAmount, ViolationBondDate = @Bond_Date, LoaderUpdateDate = GetDate(), updateddate = GetDate(), UpdationLoaderId = @LoaderID, GroupID = @GroupID where RowID = @IDFound
			Set @Flag_Inserted = 2
		End
	End
	
	














﻿/*
* 
* Created By : Waqas Javed
* Task ID  : 6791/7023
* Created Date : 11/19/2009
* 
* Business Logic : This procedure is used to get web url loader informaions
* 
*/

Alter procedure dbo.USP_HTP_LA_GET_LOADER_WEB_URLS

as

begin

SELECT w.ID AS URL_ID, w.LoaderID, REPLACE(l.Loader_Name, ' ', '') AS Loader_Name , w.[URL], w.[Counter], ISNULL(w.IsYearWise, 0) AS IsYearWise, 
--Waqas 7061 11/26/2009  added execution time also.
ISNULL(l.ExecutionTime, '10:00') AS ExecutionTime
  FROM LoaderWebURL w INNER JOIN tbl_HTS_LA_Loaders l
  ON w.LoaderID = l.Loader_ID
	
END

 
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go



CREATE PROCEDURE [dbo].[USP_HTS_LA_Update_CourtWebSiteSetting]
	@URL varchar(300),
	@CourtCloseTime varchar(100)
AS
BEGIN
	
	SET NOCOUNT ON;

    update Tbl_HTS_LA_ConfigurationSetting set CourtURL =@URL,DownloadFromCourtWebSiteUntil=@CourtCloseTime where ConfigurationID=1
END


grant execute on dbo.USP_HTS_LA_Update_CourtWebSiteSetting to dbr_webuser
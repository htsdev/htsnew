﻿ set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

CREATE procedure [dbo].[USP_HTS_LA_Insert_AccuZip_RunDate]
@RunDate datetime
as
Insert into Tbl_HTS_LA_AccuZip_RunDate values(@RunDate, 'Executed')


--USP_HTS_LA_Insert_AccuZip_RunDate '12/29/2008'
--select * from Tbl_HTS_LA_AccuZip_RunDate
--truncate table Tbl_HTS_LA_AccuZip_RunDate


grant execute on dbo.USP_HTS_LA_Insert_AccuZip_RunDate to dbr_webuser

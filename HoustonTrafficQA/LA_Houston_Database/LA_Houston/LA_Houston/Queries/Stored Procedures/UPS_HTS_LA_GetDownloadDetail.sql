set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go



CREATE PROCEDURE [dbo].[UPS_HTS_LA_GetDownloadDetail]
	
AS
BEGIN	
	SET NOCOUNT ON;
	select DownloadPath,IsActive from dbo.Tbl_HTS_LA_LoaderSetting    
END

 
grant execute on dbo.UPS_HTS_LA_GetDownloadDetail to dbr_webuser

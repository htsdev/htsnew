﻿set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go





/*
Author: Muhammad Adil Aleem
Description: To Upload Plano Warrant Cases From Loader Service
Type : Loader Service
Created date : Feb-5th-2010
Parameters: 
	@Record_Date: Case record/insert date
	@List_Date: Court list date
	@Name_First: Defendant's first name
	@Name_Last: Defendant's last name
	@Address: Defendant's home address
	@City: Defendant's home city
	@State: Defendant's home state
	@ZIP: Defendant's home zip code
	@DP2: Defendant's barcode part. return by zp4 webservice
	@DPC: Defendant's barcode part. return by zp4 webservice
	@Violation_Description: offense description
	@Warrant_Date: Warrant date
	@GroupID: Data file ID
	@AddressStatus: Address status [y/d/s/n] of defendant
	@LoaderID: Insure which loader is executing this case.
	@Flag_Inserted: It returns status wether the record inserted or deleted
*/
ALTER PROCEDURE [dbo].[Tickets_Upload_Plano_Warrant]
@Record_Date datetime,  
@List_Date as smalldatetime,
@TicketNumber AS VARCHAR(20),
@CauseNumber AS VARCHAR(20),
@Name_First as varchar(50),  
@Name_Last as varchar(50),  
@Address as varchar(50),  
@City as varchar(35),  
@State as Varchar(10),
@ZIP as varchar(10),  
@DP2 varchar(2),
@DPC varchar(1),
@Violation_Description as Varchar(2000),
@Warrant_Date DATETIME,
@GroupID as Int,
@AddressStatus VARCHAR(5),
@Flag_SameAsPrevious as bit,
@PreviousRecordID as int,
@PreviousTicketNumber VARCHAR(20),
@LoaderID as INT,
@CurrentRecordID INT output,
@CurrentTicketNumber VARCHAR(20) OUTPUT,
@Flag_Inserted int Output
AS
set nocount on  

DECLARE @StateID AS INT
DECLARE @ViolationID AS INT
DECLARE @IDFound AS INT
DECLARE @RowIDFound AS INT
DECLARE @ChargeAmount AS MONEY
DECLARE @BondAmount AS MONEY
DECLARE @Court_Date AS DATETIME
DECLARE @Bond_Date_Found AS DATETIME
DECLARE @Update_Date_Found AS DATETIME
Declare @Violation_Code as varchar(30)
DECLARE @IsQuoted AS INT
DECLARE @TicketNum2 VARCHAR(20)

Set @Name_First = left(@Name_First,20)
Set @Name_Last = left(@Name_Last,20)
Set @Address = Replace(@Address, '  ', '')
Set @Violation_Code = 'XD-none'
Set @Flag_Inserted = 0
Set @ChargeAmount = 0
Set @BondAmount = 0
SET @RowIDFound = 0
SET @IsQuoted = 0

if len(isnull(@DP2,'')) = 1
begin
	if isnull(@DP2,'') <> '0'
	begin
		Set @DP2 = '0' + @DP2
	end
end

SET @Court_Date = @Warrant_Date -- warrant will be court date

if datepart(dw,@Court_Date) = 7 -- If Saturday then make it Friday (Court date should be in working days)
	begin
		set @Court_Date = @Court_Date - 1
	end
else if datepart(dw,@Court_Date) = 1 -- If Sunday then make it Friday (Court date should be in working days)
	begin
		set @Court_Date = @Court_Date - 2
	end

Set @Court_Date = convert(datetime,convert(varchar(12),@Court_Date,110) + ' 08:00 AM')

Set @ViolationID = null
Select Top 1 @ViolationID = ViolationNumber_PK, @Violation_Code = ViolationCode, @ChargeAmount = isnull(ChargeAmount,100), @BondAmount = isnull(BondAmount,175) From DallasTrafficTickets.dbo.TblViolations Where (Description = @Violation_Description and CourtLoc = 3051 and ViolationType = 4)

If @ViolationID is null
	BEGIN
		SET @ChargeAmount = 100
		SET @BondAmount = 50
		IF CHARINDEX ('speed', @Violation_Description, 0) > 0 AND CHARINDEX ('constr', @Violation_Description, 0) > 0 AND CHARINDEX ('school', @Violation_Description, 0) > 0
		SELECT @ChargeAmount = 327, @BondAmount = 0
		ELSE IF CHARINDEX ('speed', @Violation_Description, 0) > 0 AND CHARINDEX ('school', @Violation_Description, 0) > 0
		SELECT @ChargeAmount = 227, @BondAmount = 0
		ELSE IF CHARINDEX ('speed', @Violation_Description, 0) > 0 AND CHARINDEX ('constr', @Violation_Description, 0) > 0
		SELECT @ChargeAmount = 202, @BondAmount = 0
		ELSE IF CHARINDEX ('speed', @Violation_Description, 0) > 0
		SELECT @ChargeAmount = 152, @BondAmount = 0
		
		Insert Into DallasTrafficTickets.dbo.TblViolations (Description, ViolationCode, ViolationType, CourtLoc, ChargeAmount, BondAmount) Values(@Violation_Description, @Violation_Code, 4, 3051, @ChargeAmount, @BondAmount)
		Set @ViolationID = scope_identity()
	END
 
Set @StateID = (Select Top 1 StateID From DallasTrafficTickets.dbo.TblState Where State = @State)

IF @TicketNumber = '' AND @PreviousTicketNumber <> ''
	BEGIN
		SET @TicketNumber = @PreviousTicketNumber
	END
ELSE IF @TicketNumber = '' AND @CauseNumber <> ''
	BEGIN
		SET @TicketNumber = @CauseNumber
	END

IF @Flag_SameAsPrevious = 0
	BEGIN
		SET @IDFound = NULL

		SELECT TOP 1 @IDFound = T.RecordID, @IsQuoted = T.Clientflag, @TicketNum2 = TV.TicketNumber_PK
		FROM DallasTrafficTickets.dbo.tblTicketsArchive T
		INNER JOIN DallasTrafficTickets.dbo.tblTicketsViolationsArchive TV
		ON T.RecordID = TV.RecordID
		WHERE (TV.TicketNumber_PK = @TicketNumber AND @TicketNumber <> '' AND TV.CourtLocation = 3051 AND TV.violationstatusid <> 80)
	
		IF @IDFound IS NULL
		BEGIN
			SELECT TOP 1 @IDFound = T.RecordID, @IsQuoted = T.Clientflag, @TicketNum2 = TV.TicketNumber_PK
			From DallasTrafficTickets.dbo.tblTicketsArchive T
			INNER JOIN DallasTrafficTickets.dbo.tblTicketsViolationsArchive TV
			ON T.RecordID = TV.RecordID
			WHERE (T.FirstName = @Name_First AND T.LastName = @Name_Last AND T.ZipCode = @ZIP AND DATEDIFF(DAY, TV.CourtDate, @Court_Date) = 0)
			AND TV.CourtLocation = 3051 AND TV.violationstatusid <> 80
		END
		
		IF @TicketNumber = '' AND @TicketNum2 <> ''
		BEGIN
			SET @TicketNumber = @TicketNum2
		END
	
		IF @IDFound IS NULL
			BEGIN
				IF @TicketNumber = ''
					BEGIN
						INSERT INTO DallasTrafficTickets.dbo.tblIDGenerator(recdate) VALUES(GETDATE())
						SELECT @TicketNumber = 'PLMC' + CONVERT(VARCHAR(12),SCOPE_IDENTITY())
						SET @CurrentTicketNumber = @TicketNumber
					END

				INSERT INTO DallasTrafficTickets.dbo.tblTicketsArchive (RecLoadDate, Bondflag, ListDate, TicketNumber, MidNumber, FirstName, LastName, PhoneNumber, Address1, City, StateID_FK, ZipCode, CourtID, OfficerNumber_FK, OfficerName, GroupID, Flag1, DP2, DPC, InsertionLoaderID)
				VALUES(@Record_Date, 1, @List_Date, @TicketNumber, @TicketNumber, @Name_First, @Name_Last, '0000000000', @Address, @City, @StateID, @ZIP, 3051, 962528, 'N/A', @GroupID, @AddressStatus, @DP2, @DPC, @LoaderID)
				
				SET @CurrentRecordID = SCOPE_IDENTITY()
			END
		Else
			BEGIN
				SET @CurrentRecordID = @IDFound
				
				UPDATE DallasTrafficTickets.dbo.tblTicketsArchive
				SET Bondflag = 1, GroupID = @GroupID, UpdationLoaderID = @LoaderID
				WHERE RecordID = @IDFound
			END
	END
Else
	BEGIN
		SET @CurrentRecordID = @PreviousRecordID
		SET @CurrentTicketNumber = @PreviousTicketNumber
	END

SET @RowIDFound = NULL
	
SELECT TOP 1 @RowIDFound = TV.RowID, @Bond_Date_Found = ISNULL(TV.ViolationBondDate,'1/1/1900'), @Update_Date_Found = ISNULL(TV.LoaderUpdateDate,'1/1/1900'), @TicketNum2 = TV.TicketNumber_PK
FROM DallasTrafficTickets.dbo.tblTicketsViolationsArchive TV
WHERE (TV.RecordID = @CurrentRecordID AND ((TV.CauseNumber = @CauseNumber AND ISNULL(@CauseNumber, '') <> '') OR TV.ViolationNumber_PK = @ViolationID) AND CourtLocation = 3051)

IF @RowIDFound IS NULL
	BEGIN
		IF @IsQuoted = 1
			BEGIN
				IF @TicketNumber = ''
					BEGIN
						INSERT INTO DallasTrafficTickets.dbo.tblIDGenerator(recdate) VALUES(GETDATE())
						SELECT @TicketNumber = 'PLMC' + CONVERT(VARCHAR(12),SCOPE_IDENTITY())
						SET @CurrentTicketNumber = @TicketNumber
					END
				IF @TicketNumber = '' AND @TicketNum2 <> ''
				BEGIN
					SET @TicketNumber = @TicketNum2
				END

				INSERT INTO DallasTrafficTickets.dbo.tblTicketsArchive (RecLoadDate, Bondflag, ListDate, TicketNumber, MidNumber, FirstName, LastName, PhoneNumber, Address1, City, StateID_FK, ZipCode, CourtID, OfficerNumber_FK, OfficerName, GroupID, Flag1, DP2, DPC, InsertionLoaderID)
				VALUES(@Record_Date, 1, @List_Date, @TicketNumber, @TicketNumber, @Name_First, @Name_Last, '0000000000', @Address, @City, @StateID, @ZIP, 3051, 962528, 'N/A', @GroupID, @AddressStatus, @DP2, @DPC, @LoaderID)

				SET @CurrentRecordID = SCOPE_IDENTITY()
			END
		
		INSERT INTO DallasTrafficTickets.dbo.tblTicketsViolationsArchive (TicketNumber_PK, ViolationNumber_PK, CauseNumber, CourtDate, ViolationBondDate, InsertDate, UpdatedDate, TicketViolationDate, ViolationDescription, ViolationCode, FineAmount, BondAmount, CourtLocation, RecordID, ViolationStatusID, LoaderUpdateDate, InsertionLoaderId, GroupID)
		VALUES(@TicketNumber, @ViolationID, @CauseNumber, @Court_Date, @Warrant_Date, GETDATE(), GETDATE(), '1/1/1900', @Violation_Description, @Violation_Code, @ChargeAmount + @BondAmount, @ChargeAmount + @BondAmount, 3051, @CurrentRecordID, 116, getdate(), @LoaderID, @GroupID)
		
		SET @Flag_Inserted = 1
	END
ELSE
	BEGIN
		UPDATE DallasTrafficTickets.dbo.tblTicketsViolationsArchive
		SET ViolationBondDate = @Warrant_Date, FineAmount = FineAmount + @BondAmount, UpdationLoaderId = @LoaderID, GroupID = @GroupID,
		TicketNumber_PK = @TicketNumber, CauseNumber = @CauseNumber, LoaderUpdateDate = GETDATE()
		WHERE RowID = @RowIDFound
		
		SET @Flag_Inserted = 2
	END

SET @CurrentTicketNumber = CASE WHEN @TicketNumber = '' THEN ISNULL(@TicketNum2, @CauseNumber) ELSE @TicketNumber END


﻿/*
Author: Muhammad Adil Aleem
Type         : Loader Service
Created date : Jul-17th-2008
Business Logic: To update client/quote client court setting info for HCJP cases.
Parameters: 
	@GroupID: Data file unique ID.
	@LoaderName : just for display in output email.
*/
--grant execute on dbo.USP_HTS_LA_Update_ClientViolationInfo to dbr_webuser
--USP_HTS_LA_Update_ClientViolationInfo 2785, 'HCJP Test '
Alter procedure [dbo].[USP_HTS_LA_Update_ClientViolationInfo]
@GroupID int,
@LoaderName varchar(50)
As

Declare @Upd_Tickets as table (GroupID int, CauseNumber varchar(50), TicketNumber varchar(50), UpdatedViolationsStatus int, OldViolationStatus int, UpdatedCourtDate datetime, OldCourtDate datetime, UpdatedDate DATETIME)

update TV
Set TV.CourtViolationStatusID = TVA.ViolationStatusID, TV.CourtDate = (case when isnull(TVA.CourtDate,'1/1/1900') = '1/1/1900' then TV.CourtDate else TVA.CourtDate end) , TV.GroupID = @GroupID, TV.UpdatedDate = getdate(), tv.VEmployeeID = 3992 -- Adil 6447 22/08/2009 Fixing employee ID issue in client's case history
Output Inserted.GroupID, Inserted.CaseNumAssignedByCourt, Inserted.RefCaseNumber, inserted.CourtViolationStatusID, deleted.CourtViolationStatusID, inserted.CourtDate, deleted.CourtDate, inserted.UpdatedDate into @Upd_Tickets
from TrafficTickets.dbo.tblTicketsViolations TV inner join TrafficTickets.dbo.TblTicketsViolationsArchive TVA
on TV.CaseNumAssignedByCourt = TVA.CauseNumber
inner join TrafficTickets.dbo.TblTickets T
on T.ticketID_PK = TV.TicketID_PK
where (TV.CourtViolationStatusID <> TVA.ViolationStatusID Or TV.CourtDate <> TVA.CourtDate) -- Update Criteria
And Tv.courtid between 3007 And 3022		-- Court Location = HCJP
--7684 Afaq 04/09/2010 Remove check for Non client.
And (TV.CourtViolationStatusID <> 80 And TV.CourtViolationStatusIDMain <> 80)	-- Donot Update Disposed Cases
And TVA.GroupID = @GroupID	-- Specific GroupID

--This is a temporary monitoring------------------------------
insert into Tbl_HTS_LA_CheckHCJP_Update values(getdate(), @GroupID)
--------------------------------------------------------------


--Just For Emailing
--if (Select Count(GroupID) from @Upd_Tickets) > 0
--Begin
--declare @emailBody nvarchar(max)
--declare @emailSubject nvarchar(200)
--
--set @emailBody = '<style type="text/css">.clsLeftPaddingTable
--			{
--				PADDING-LEFT: 5px;
--				FONT-SIZE: 8pt;
--				COLOR: #123160;
--				FONT-FAMILY: Tahoma;
--				background-color:EFF4FB;
--				border-collapse:collapse;
--				border-width:1px;
--			}
--			
--			</style>
--			<H2 align="center"><font color="#3366cc"><STRONG>Updated Client Violations Info</strong></font></H2>
--			<table border="1" cellpadding="0" cellspacing="0" width="100%" bgcolor="#EFF4FB" class="clsLeftPaddingTable" BorderColor="black">
--			<tr>
--			<td ><font color="#3366cc"><STRONG>GroupID</strong></font></td>
--			<td><font color="#3366cc"><STRONG>TicketNumber</strong></font></td>
--			<td><font color="#3366cc"><STRONG>CauseNumber</strong></font></td>
--			<td><font color="#3366cc"><STRONG>UpdatedViolationsStatus</strong></font></td>
--			<td><font color="#3366cc"><STRONG>OldViolationStatus</strong></font></td>
--			<td><font color="#3366cc"><STRONG>UpdatedCourtDate</strong></font></td>
--			<td><font color="#3366cc"><STRONG>OldCourtDate</strong></font></td>
--			<td><font color="#3366cc"><STRONG>UpdatedDate</strong></font></td>
--			</tr>'
--			+ 
--
-- Cast ( (  select distinct  
--			td=	@GroupID, '',
--			td= upd.TicketNumber ,'',
--			td=	upd.CauseNumber ,'',
--			td= tcs.Description ,'',
--			td= tcs2.Description ,'',
--			td= upd.UpdatedCourtDate ,'',
--			td= upd.OldCourtDate ,'',
--			td= upd.UpdatedDate ,''
--		from @Upd_Tickets upd
--			inner join TrafficTickets.dbo.tblCourtViolationStatus tcs
--			on tcs.CourtViolationStatusID = upd.UpdatedViolationsStatus
--			inner join TrafficTickets.dbo.tblCourtViolationStatus tcs2
--			on tcs2.CourtViolationStatusID = upd.OldViolationStatus
--		FOR XML PATH ('tr'), Type) as NVarchar(max) )+'</table>'
--
--Set @emailSubject = @LoaderName + ': Updated Client Violations Info!'
--
--EXEC msdb.dbo.sp_send_dbmail
--    @profile_name = 'Traffic System',
--    @recipients = 'dataloaders@sullolaw.com; adil@lntechnologies.com',
--    @subject = @emailSubject,
--    @body = @emailBody,
--    @body_format = 'HTML';
--End




 
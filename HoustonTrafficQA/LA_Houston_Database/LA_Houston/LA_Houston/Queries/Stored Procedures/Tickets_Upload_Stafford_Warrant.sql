﻿set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go








/*
Author: Muhammad Adil Aleem
Description: To Upload Stafford Warrant Cases From Loader Service
Type : Loader Service
Created date : Feb-19th-2010
Parameters: 
	@Record_Date: Case record/insert date
	@List_Date: Court list date
	@Ticket_Number: Ticket Number
	@Name_First: Defendant's first name
	@Name_Last: Defendant's last name
	@Name_Middle: Defendant's middle name
	@Race: Defendant's race
	@Sex: Defendant's sex
	@Address: Defendant's home address
	@City: Defendant's home city
	@State: Defendant's home state
	@DLState: Defendant's Driving License state
	@ZIP: Defendant's home zip code
	@DP2: Defendant's barcode part. return by zp4 webservice
	@DPC: Defendant's barcode part. return by zp4 webservice
	@Violation_Description: offense description
	@Violation_Date: offense date
	@Court_Date: offense date
	@Warrant_Date: offense date
	@FineAmount: Offense charges
	@BondAmount: Warrant amount
	@GroupID: Data file ID
	@AddressStatus: Address status [y/d/s/n] of defendant
	@Flag_SameAsPrevious: insure wether this case belongs to previous one or not [It helps to cut-off researching the defendant profile]
	@PreviousRecordID: If this case belongs to previous case then previous case ID will be the value of @PreviousRecordID
	@LoaderID: Insure which loader is executing this case.
	@CurrentRecordID: It returns current Case-ID/Record-ID to calling program
	@Flag_Inserted: It returns status wether the record inserted or deleted
*/
ALTER PROCEDURE [dbo].[Tickets_Upload_Stafford_Warrant]
@Record_Date datetime,
@List_Date as smalldatetime,
@Ticket_Number as varchar(20),
@Cause_Number as varchar(20),
@Name_First as varchar(50),
@Name_Last as varchar(50),
@Name_Middle as varchar(50),
@Race as varchar(50),
@Sex as varchar(50),
@Address as varchar(50),
@City as varchar(35),
@State as Varchar(10),
@DLState as Varchar(10),
@ZIP as varchar(10),
@DP2 varchar(2),
@DPC varchar(1),
@Violation_Description as Varchar(2000),
@Violation_Date DATETIME,
@Court_Date DATETIME,
@Warrant_Date DATETIME,
@FineAmount FLOAT,
@BondAmount FLOAT,
@GroupID as Int,
@AddressStatus VARCHAR(5),
@Flag_SameAsPrevious as bit,
@PreviousRecordID as int,
@LoaderID as int,
@CurrentRecordID int output,
@Flag_Inserted int Output
AS
set nocount on  

Declare @StateID as INT
Declare @DLStateID as Int
Declare @ViolationID as int
Declare @IDFound as INT
Declare @RowIDFound as int
Declare @TicketNum2 as varchar(20)
Declare @Violation_Code as varchar(30)
DECLARE @IsQuoted AS INT
DECLARE @Bond_Date_Found DATETIME

SET @IsQuoted = 0
Set @Name_First = left(@Name_First,20)
Set @Name_Last = left(@Name_Last,20)
Set @Name_Middle = left(@Name_Middle,1)
Set @Address = Replace(@Address, '  ', '')
Set @Violation_Code = 'XD-none'
Set @Flag_Inserted = 0
--Set @FineAmount = 0 -- Adil 7968 07/08/2010 Fine amount is now available that's why no need to set zero for it.
--Set @BondAmount = 0 -- Adil 7968 07/08/2010 Fine amount is now available that's why no need to set zero for it.

if len(isnull(@DP2,'')) = 1
begin
	if isnull(@DP2,'') <> '0'
	begin
		Set @DP2 = '0' + @DP2
	end
end

if datepart(dw,@Court_Date) = 7 -- If Saturday then make it Friday (Court date should be in working days)
	begin
		set @Court_Date = @Court_Date - 1
	end
else if datepart(dw,@Court_Date) = 1 -- If Sunday then make it Friday (Court date should be in working days)
	begin
		set @Court_Date = @Court_Date - 2
	end

IF @Sex = 'M'
BEGIN
    SET @Sex = 'Male'
END
ELSE 
IF @Sex = 'F'
BEGIN
    SET @Sex = 'Female'
END

SELECT @Race = 
		CASE @Race
            WHEN 'A' THEN 'Asian'
            WHEN 'B' THEN 'Black'
            WHEN 'H' THEN 'Hispanic'
            WHEN 'I' THEN 'Indian'
            WHEN 'M' THEN 'Mexican'
            WHEN 'O' THEN 'Other'
            WHEN 'U' THEN 'Unknown'
            WHEN 'W' THEN 'White'
            WHEN 'X' THEN 'Unknown'
            ELSE NULL
		END

Set @ViolationID = null
Select Top 1 @ViolationID = ViolationNumber_PK, @Violation_Code = ViolationCode From TrafficTickets.dbo.TblViolations Where (Description = @Violation_Description and CourtLoc = 3067 and ViolationType = 13)

If @ViolationID is null
	BEGIN
		Insert Into TrafficTickets.dbo.TblViolations (Description, ViolationCode, ViolationType, CourtLoc, ChargeAmount, BondAmount) 
		Values(@Violation_Description, @Violation_Code, 13, 3067, 0, 0)
		Set @ViolationID = scope_identity()
	END
	
Set @StateID = (Select Top 1 StateID From TrafficTickets.dbo.TblState Where State = @State)
Set @DLStateID = (Select Top 1 StateID From TrafficTickets.dbo.TblState Where State = @DLState)

If @Flag_SameAsPrevious = 0
	BEGIN
		Set @IDFound = null

		SELECT TOP 1 @IDFound = T.RecordID, @TicketNum2 = ISNULL(TicketNumber, @Ticket_Number) , @IsQuoted = T.Clientflag
		FROM TrafficTickets.dbo.tblTicketsArchive T
		INNER JOIN TrafficTickets.dbo.tblTicketsViolationsArchive TV
		ON T.RecordID = TV.RecordID
		WHERE ((TV.TicketNumber_PK = @Ticket_Number OR TV.TicketNumber_PK = SUBSTRING(@Ticket_Number, 1, LEN(ISNULL(@Ticket_Number, ''))-1)) AND TV.CourtLocation = 3067 AND TV.violationstatusid <> 80)
	
		If @IDFound IS NULL
			Begin
				if Len(@Ticket_Number)<2
					Begin
						insert into TrafficTickets.dbo.tblIDGenerator(recdate) values(getdate())
						select @Ticket_Number='STMC' + convert(varchar(12),scope_identity())
					End

				Insert Into TrafficTickets.dbo.tblTicketsArchive (RecLoadDate, ListDate, TicketNumber, MidNumber, Bondflag, FirstName, LastName, Initial, Gender, Race, PhoneNumber, Address1, City, StateID_FK, DLStateID, ZipCode, CourtID, GroupID, Flag1, DP2, DPC, InsertionLoaderID)
				Values(@Record_Date, @List_Date, @Ticket_Number, @Ticket_Number, 1, @Name_First, @Name_Last, @Name_Middle, @Sex, @Race, '0000000000', @Address, @City, @StateID, @DLStateID, @ZIP, 3067, @GroupID, @AddressStatus, @DP2, @DPC, @LoaderID)
				
				Set @CurrentRecordID = scope_identity()
			End
		Else
			Begin
				Set @CurrentRecordID = @IDFound
				if Len(@Ticket_Number)<1
					Begin
						Set @Ticket_Number = @TicketNum2
					End
			End
	End
Else
	Begin
		Set @CurrentRecordID = @PreviousRecordID
	End

Set @IDFound = NULL
Set @RowIDFound = null
	
SELECT TOP 1 @RowIDFound = Isnull(TV.RowID,0), @IDFound = ISNULL(TV.RecordID, 0), @Bond_Date_Found = isnull(TV.BondDate,'1/1/1900')
FROM TrafficTickets.dbo.tblTicketsViolationsArchive TV
WHERE ((TV.TicketNumber_PK = @Ticket_Number OR TV.TicketNumber_PK = SUBSTRING(@Ticket_Number, 1, LEN(ISNULL(@Ticket_Number, ''))-1)) and ViolationNumber_PK = @ViolationID) and CourtLocation = 3067 AND RecordID = @CurrentRecordID

If @RowIDFound is null
	BEGIN
		IF @IsQuoted = 1
			Begin
				if Len(@Ticket_Number)<1
					Begin
						insert into TrafficTickets.dbo.tblIDGenerator(recdate) values(getdate())
						select @Ticket_Number='STMC' + convert(varchar(12),scope_identity())
					End

				Insert Into TrafficTickets.dbo.tblTicketsArchive (RecLoadDate, ListDate, TicketNumber, MidNumber, Bondflag, FirstName, LastName, Initial, Gender, Race, PhoneNumber, Address1, City, StateID_FK, DLStateID, ZipCode, CourtID, GroupID, Flag1, DP2, DPC, InsertionLoaderID)
				Values(@Record_Date, @List_Date, @Ticket_Number, @Ticket_Number, 1, @Name_First, @Name_Last, @Name_Middle, @Sex, @Race, '0000000000', @Address, @City, @StateID, @DLStateID, @ZIP, 3067, @GroupID, @AddressStatus, @DP2, @DPC, @LoaderID)

				Set @CurrentRecordID = scope_identity()
			END
		
		INSERT INTO TrafficTickets.dbo.tblTicketsViolationsArchive (TicketNumber_PK, ViolationNumber_PK, CauseNumber, CourtDate, InsertDate, UpdatedDate, TicketViolationDate, BondDate, ViolationDescription, ViolationCode, FineAmount, BondAmount, CourtLocation, RecordID, ViolationStatusID, LoaderUpdateDate, InsertionLoaderId, GroupID)
		VALUES (@Ticket_Number, @ViolationID, @Cause_Number, @Court_Date, Getdate(), Getdate(), @Violation_Date, @Warrant_Date, @Violation_Description, @Violation_Code, @FineAmount, @BondAmount, 3067, @CurrentRecordID, 116, getdate(), @LoaderID, @GroupID)
		
		SET @Flag_Inserted = 1
	END
ELSE
BEGIN
	IF DATEDIFF(DAY, @Bond_Date_Found, @Warrant_Date) <> 0
	BEGIN
		UPDATE TrafficTickets.dbo.tblTicketsArchive
		SET Bondflag = 1, UpdationLoaderID = @LoaderID, GroupID = @GroupID
		WHERE RecordID = @IDFound
		
		UPDATE TrafficTickets.dbo.tblTicketsViolationsArchive
		SET BondDate = @Warrant_Date, FineAmount = FineAmount + @BondAmount, UpdationLoaderId = @LoaderID, GroupID = @GroupID, LoaderUpdateDate = GETDATE()
		WHERE RowID = @RowIDFound
		
		SET @Flag_Inserted = 2
	END
END






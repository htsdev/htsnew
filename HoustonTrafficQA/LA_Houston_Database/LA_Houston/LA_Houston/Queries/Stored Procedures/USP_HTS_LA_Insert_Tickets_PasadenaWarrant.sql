USE [LA_Houston]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTS_LA_Insert_Tickets_PasadenaWarrant]    Script Date: 05/12/2010 01:57:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





/*
Author: Muhammad Adil Aleem
Description: To Upload Pasadena Warrants From Loader Service
Type : Loader Service
Created date : Jul-17th-2008
Parameters: 
	@Record_Date: Case record/insert date
	@List_Date: Court list date
	@Name_Last: Defendant's last name
	@Name_First: Defendant's first name
	@Address: Defendant's home address
	@City: Defendant's home city
	@State: Defendant's home state
	@ZIP: Defendant's home zip code
	@DP2: Defendant's barcode part. return by zp4 webservice
	@DPC: Defendant's barcode part. return by zp4 webservice
	@Phone_Number: Defendant's contact number
	@DL_Number: Defendant's driving license number
	@Race: Defendant's cast
	@Gender: Defendant's sex/gender
	@DOB: Defendant's date of birth
	@Court_Date: Defendant's court date
	@Violation_Date: Defendant's violation date
	@Bond_Date: Defendant's warrant date
	@Violation_Description: Violation's title of Defendant
	@Officer_Name: Ticket Officer Name
	@GroupID: Data file ID
	@AddressStatus: Address status [y/d/s/n] of defendant
	@Flag_SameAsPrevious: insure wether this case belongs to previous one or not [It helps to cut-off researching the defendant profile]
	@PreviousRecordID: If this case belongs to previous case then previous case ID will be the value of @PreviousRecordID
	@PreviousTicketNumber: If this case belongs to previous case then previous case Ticket number will be the value of @PreviousTicketNumber
	@LoaderID: Insure which loader is executing this case.
	@MidNumber: Client's Mid number. 
	@CurrentRecordID: It returns current Case-ID/Record-ID to calling program
	@CurrentTicketNumber: It returns current case ticket number to calling program
	@Flag_Inserted: It returns status wether the record inserted or deleted
*/
ALTER PROCEDURE [dbo].[USP_HTS_LA_Insert_Tickets_PasadenaWarrant]
@Record_Date as datetime,
@List_Date as datetime,
@Name_Last as varchar(50),
@Name_First as varchar(50),
@Address as varchar(50),
@City as varchar(35),
@State as Varchar(10),
@ZIP as varchar(10),
@DP2 varchar(2),
@DPC varchar(1),
@Phone_Number varchar(25),
@DL_Number varchar(20),
@Race varchar(15),
@Gender varchar(15),
@DOB as datetime,
@Court_Date as datetime,
@Violation_Date as datetime,
@Bond_Date as datetime,
@Violation_Description as Varchar(500),
@Officer_Name varchar(50),
@GroupID as Int,
@AddressStatus Char(1),
@Flag_SameAsPrevious as bit,
@PreviousRecordID as int,
@PreviousTicketNumber as varchar(20),
@LoaderID as int,
@MidNumber AS VARCHAR(50),	-- Adil 7720 05/05/10, MidNumber paramter is added.
@CurrentRecordID int output,
@CurrentTicketNumber varchar(20) output,
@Flag_Inserted int Output
AS
set nocount on
Declare @ViolationID as int
Declare @StateID as Int
Declare @IDFound as int
Declare @FineAmount as money
Declare @BondAmount as money
Declare @OfficerCode int
Declare @Bond_Date_Found datetime
DECLARE @IsQuoted AS INT

SET @IsQuoted = 0
Set @Name_First = left(@Name_First,20)
Set @Name_Last = left(@Name_Last,20)

Set @Flag_Inserted = 0

If @ZIP = '00000'
	Begin
		Set @ZIP = ''
	End

if @Gender = 'M'
	begin
		Set @Gender = 'Male'
	end
else if @Gender = 'F'
	begin
		Set @Gender = 'Female'
	end

Select @Race = case @Race 
when 'A' then 'Asian'
when 'B' then 'Black'
when 'H' then 'Hispanic'
when 'I' then 'Indian'
when 'M' then 'Mexican'
when 'O' then 'Other'
when 'U' then 'Unknown'
when 'W' then 'White'
when 'X' then 'Unknown'
else null
end

if isnull(@Officer_Name,'') = ''
	Begin
		Set @Officer_Name = 'N/A'
	End

Set @OfficerCode = null
Select Top 1 @OfficerCode = OfficerNumber_PK from TrafficTickets.dbo.tblOfficer where (isnull(FirstName,'') = @Officer_Name)
if @OfficerCode is null 
	Begin
		Set @OfficerCode = (select max(OfficerNumber_PK) + 1 from TrafficTickets.dbo.tblofficer)
		Insert Into TrafficTickets.dbo.tblOfficer (OfficerNumber_PK, OfficerType, FirstName, Comments) Values (@OfficerCode, 'N/A', @Officer_Name, 'Inserted at ' + convert(varchar(25),getdate(),101))
	End

Set @ViolationID = null
Select Top 1 @ViolationID = ViolationNumber_PK, @FineAmount = ChargeAmount, @BondAmount = BondAmount From TrafficTickets.dbo.TblViolations Where (ltrim(rtrim(Description)) = ltrim(rtrim(@Violation_Description)) and ViolationType = 6)

If @ViolationID is null
	Begin
		Insert Into TrafficTickets.dbo.TblViolations (Description, Sequenceorder, IndexKey, IndexKeys, ChargeAmount, BondAmount, Violationtype, ChargeonYesflag, ShortDesc, ViolationCode, AdditionalPrice, CourtLoc)
		Values(@Violation_Description, null, 2, 100, 100, 140, 6, 1, 'None', 'none', 0, 3004)
		Set @FineAmount = 100
		Set @BondAmount = 140
		Set @ViolationID = scope_identity()
	End

Set @StateID = (Select Top 1 StateID From TrafficTickets.dbo.TblState Where State = @State)

If @Flag_SameAsPrevious = 0
	Begin
		Set @IDFound = null
		SELECT Top 1 @IDFound = TTA.RecordID, @CurrentTicketNumber = TTA.TicketNumber, @IsQuoted = TTA.Clientflag
		From TrafficTickets.dbo.tblTicketsArchive TTA inner join TrafficTickets.dbo.tblTicketsViolationsArchive TTVA
		On TTA.RecordID = TTVA.RecordID
		WHERE TTA.MidNumber=@MidNumber	-- Adil 7720 05/05/10, add midNumber in where clause
		--Where TTA.LastName = @Name_Last And TTA.FirstName = @Name_First And ltrim(rtrim(isnull(TTA.Address1,'') + ' ' + isnull(TTA.Address2,''))) = ltrim(rtrim(@Address)) -- Adil 7720 05/05/10 code commented
		And datediff(day,isnull(TTVA.CourtDate,'1/1/1900'), @Court_Date) = 0
		AND TTVA.violationstatusid <> 80	-- Adil 7001 11/14/2009 Do not associate new cases with client, quote and disposed cases

		If @IDFound is null
			Begin
				insert into TrafficTickets.dbo.tblIDGenerator(recdate) values(getdate())
				select @CurrentTicketNumber = 'PMC' + convert(varchar(12),@@identity)

				Insert Into TrafficTickets.dbo.tblTicketsArchive (RecLoadDate, ListDate, ViolationDate, BondDate, BondFlag, TicketNumber, MidNumber, FirstName, LastName, PhoneNumber, DLNumber, Address1, City, StateID_FK, ZipCode, GroupID, Flag1, DP2, DPC, Race, Gender, DOB, InsertionLoaderID)   
				Values(@Record_Date, @List_Date, @Violation_Date, @Bond_Date, 1, @CurrentTicketNumber, @MidNumber, @Name_First, @Name_Last, @Phone_Number, @DL_Number, @Address, @City, @StateID, @ZIP, @GroupID, @AddressStatus, @DP2, @DPC, @Race, @Gender, @DOB, @LoaderID)

				Set @CurrentRecordID = scope_identity()
			End
		Else
			Begin
				Set @CurrentRecordID = @IDFound
			End

	End
Else
	Begin
		Set @CurrentRecordID = @PreviousRecordID
		Set @CurrentTicketNumber = @PreviousTicketNumber
	End

Set @IDFound = null
Select top 1 @IDFound = RowID, @Bond_Date_Found = isnull(BondDate,'1/1/1900') From TrafficTickets.dbo.tblTicketsViolationsArchive
			Where 
			(ViolationDescription = @Violation_Description  And RecordID = @CurrentRecordID)

If @IDFound is null
	BEGIN
		IF @IsQuoted = 1 -- Adil 7380 02/05/2010 if current client is quoted client and coming violation is different from existing then insert new ticket.
			Begin
				insert into TrafficTickets.dbo.tblIDGenerator(recdate) values(getdate())
				select @CurrentTicketNumber = 'PMC' + convert(varchar(12),@@identity)

				Insert Into TrafficTickets.dbo.tblTicketsArchive (RecLoadDate, ListDate, ViolationDate, BondDate, BondFlag, TicketNumber, MidNumber, FirstName, LastName, PhoneNumber, DLNumber, Address1, City, StateID_FK, ZipCode, GroupID, Flag1, DP2, DPC, Race, Gender, DOB, InsertionLoaderID)   
				Values(@Record_Date, @List_Date, @Violation_Date, @Bond_Date, 1, @CurrentTicketNumber, @MidNumber, @Name_First, @Name_Last, @Phone_Number, @DL_Number, @Address, @City, @StateID, @ZIP, @GroupID, @AddressStatus, @DP2, @DPC, @Race, @Gender, @DOB, @LoaderID)

				Set @CurrentRecordID = scope_identity()
			END
		Insert Into TrafficTickets.dbo.tblTicketsViolationsArchive (TicketNumber_PK, ViolationNumber_PK, CauseNumber, CourtLocation, CourtDate, TicketViolationDate, UpdatedDate, BondDate, TicketOfficerNumber, ViolationDescription, ViolationCode, FineAmount, BondAmount, RecordID, ViolationStatusID, LoaderUpdateDate, InsertionLoaderId, GroupID)
		Values(@CurrentTicketNumber, @ViolationID, @CurrentTicketNumber, 3004, @Court_Date, @Violation_Date, Getdate(), @Bond_Date, @OfficerCode, @Violation_Description, 'None', @FineAmount, @BondAmount, @CurrentRecordID, 186, getdate(), @LoaderID, @GroupID)
		Set @Flag_Inserted = 1
	End
Else
	Begin
		If datediff(day,@Bond_Date_Found, @Bond_Date) <> 0
		Begin
			Update TrafficTickets.dbo.tblTicketsViolationsArchive Set BondDate = @Bond_Date, LoaderUpdateDate = GetDate() , UpdationLoaderId = @LoaderID, GroupID = @GroupID where RowID = @IDFound
			Update TrafficTickets.dbo.tblTicketsArchive Set BondDate = @Bond_Date, BondFlag = 1, UpdationLoaderId = @LoaderID, GroupID = @GroupID where RecordID = @CurrentRecordID
			Set @Flag_Inserted = 2
		End
	End


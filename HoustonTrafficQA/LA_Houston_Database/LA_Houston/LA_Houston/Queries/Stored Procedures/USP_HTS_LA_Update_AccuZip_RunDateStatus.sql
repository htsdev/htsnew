﻿ set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

CREATE procedure [dbo].[USP_HTS_LA_Update_AccuZip_RunDateStatus]
@RunDate datetime
as
Update Tbl_HTS_LA_AccuZip_RunDate Set Status = 'Completed' Where datediff(day, RunDate, @RunDate) = 0


--USP_HTS_LA_Update_AccuZip_RunDateStatus '12/29/2008'


grant execute on dbo.USP_HTS_LA_Update_AccuZip_RunDateStatus to dbr_webuser

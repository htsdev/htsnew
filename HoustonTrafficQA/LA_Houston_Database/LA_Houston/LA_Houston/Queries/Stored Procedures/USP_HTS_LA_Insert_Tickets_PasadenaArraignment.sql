set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go



/*
Author: Muhammad Adil Aleem
Description: To Upload Pasadena Arraignment Cases From Loader Service
Type : Loader Service
Created date : Jul-17th-2008
Parameters: 
	@Record_Date: Case record/insert date
	@List_Date: Court list date
	@Court_Date: Defendant's court date
	@Name_Last: Defendant's last name
	@Name_First: Defendant's first name
	@Address: Defendant's home address
	@City: Defendant's home city
	@State: Defendant's home state
	@ZIP: Defendant's home zip code
	@DP2: Defendant's barcode part. return by zp4 webservice
	@DPC: Defendant's barcode part. return by zp4 webservice
	@Violation_Description: Violation's title of Defendant
	@GroupID: Data file ID
	@AddressStatus: Address status [y/d/s/n] of defendant
	@Flag_SameAsPrevious: insure wether this case belongs to previous one or not [It helps to cut-off researching the defendant profile]
	@PreviousRecordID: If this case belongs to previous case then previous case ID will be the value of @PreviousRecordID
	@PreviousTicketNumber: If this case belongs to previous case then previous case Ticket number will be the value of @PreviousTicketNumber
	@LoaderID: Insure which loader is executing this case.
	@MidNumber: Client's Mid number. 
	@CurrentRecordID: It returns current Case-ID/Record-ID to calling program
	@CurrentTicketNumber: It returns current case ticket number to calling program
	@Flag_Inserted: It returns status wether the record inserted or deleted
*/
ALTER PROCEDURE [dbo].[USP_HTS_LA_Insert_Tickets_PasadenaArraignment]
@CauseNumber AS VARCHAR(20),
@Record_Date as datetime,
@List_Date as datetime,
@Court_Date as datetime,
@Name_Last as varchar(50),
@Name_First as varchar(50),
@Address as varchar(50),
@City as varchar(35),
@State as Varchar(10),
@ZIP as varchar(10),
@DP2 varchar(2),
@DPC varchar(1),
@Violation_Description as Varchar(500),
@GroupID as Int,
@AddressStatus Char(1),
@Flag_SameAsPrevious as bit,
@PreviousRecordID as int,
@PreviousTicketNumber as varchar(20),
@LoaderID as int,
@MidNumber AS VARCHAR(50),	-- SAEED-7720-04/22/10, MidNumber paramter is added.
@CurrentRecordID int output,
@CurrentTicketNumber varchar(20) output,
@Flag_Inserted int Output
AS
set nocount on
Declare @ViolationID as int
Declare @StateID as Int
Declare @IDFound as int
Declare @FineAmount as money
DECLARE @IsQuoted AS INT

SET @IsQuoted = 0
SET @Violation_Description = LTRIM(RTRIM(@Violation_Description))

Set @Name_First = left(@Name_First,20)
Set @Name_Last = left(@Name_Last,20)

Set @Flag_Inserted = 0

If @ZIP = '00000'
	Begin
		Set @ZIP = ''
	End


Set @ViolationID = null
Select Top 1 @ViolationID = ViolationNumber_PK, @FineAmount = ChargeAmount From TrafficTickets.dbo.TblViolations Where (ltrim(rtrim(Description)) = ltrim(rtrim(@Violation_Description)) and ViolationType = 6)

If @ViolationID is null
	Begin
		Insert Into TrafficTickets.dbo.TblViolations (Description, Sequenceorder, IndexKey, IndexKeys, ChargeAmount, BondAmount, Violationtype, ChargeonYesflag, ShortDesc, ViolationCode, AdditionalPrice, CourtLoc)
		Values(@Violation_Description, null, 2, 100, 100, 140, 6, 1, 'None', 'none', 0, 3004)
		Set @FineAmount = 100
		Set @ViolationID = scope_identity()
	End

Set @StateID = (Select Top 1 StateID From TrafficTickets.dbo.TblState Where State = @State)

If @Flag_SameAsPrevious = 0
	Begin
		Set @IDFound = NULL

		SELECT TOP 1 @IDFound = TTA.RecordID, @CurrentTicketNumber = TTVA.TicketNumber_PK, @IsQuoted = TTA.Clientflag
		From TrafficTickets.dbo.tblTicketsArchive TTA inner join TrafficTickets.dbo.tblTicketsViolationsArchive TTVA
		On TTA.RecordID = TTVA.RecordID
		-- Adil 8331 09/29/2010 Cause Number Added
		WHERE ((TTVA.CauseNumber = @CauseNumber AND @CauseNumber <> '') OR TTA.MidNumber=@MidNumber)	-- SAEED-7720-04/22/10, add midNumber in where clause
		--Where TTA.LastName = @Name_Last And TTA.FirstName = @Name_First And ltrim(rtrim(isnull(TTA.Address1,'') + ' ' + isnull(TTA.Address2,''))) = ltrim(rtrim(@Address))	-- SAEED-7720-04/22/10,  remove these condition cause midNumber check is applied above -- Adil 7720 05/06/10 code commented
		And datediff(day,isnull(TTVA.CourtDate,'1/1/1900'), @Court_Date) = 0
		AND TTVA.violationstatusid <> 80	-- Adil 7001 11/14/2009 Do not associate new cases with client, quote and disposed cases
		AND TTVA.CourtLocation = 3004
		
		If @IDFound is NULL
		BEGIN
			SELECT TOP 1 @IDFound = TTA.RecordID, @CurrentTicketNumber = TTVA.TicketNumber_PK, @IsQuoted = TTA.Clientflag
			From TrafficTickets.dbo.tblTicketsArchive TTA inner join TrafficTickets.dbo.tblTicketsViolationsArchive TTVA
			On TTA.RecordID = TTVA.RecordID
			-- Adil 8331 09/29/2010 Cause Number Added
			WHERE ((TTVA.CauseNumber = @CauseNumber AND @CauseNumber <> '') OR TTA.MidNumber=@MidNumber)	-- SAEED-7720-04/22/10, add midNumber in where clause
			--Where TTA.LastName = @Name_Last And TTA.FirstName = @Name_First And ltrim(rtrim(isnull(TTA.Address1,'') + ' ' + isnull(TTA.Address2,''))) = ltrim(rtrim(@Address))	-- SAEED-7720-04/22/10,  remove these condition cause midNumber check is applied above -- Adil 7720 05/06/10 code commented
			And datediff(day,isnull(TTVA.CourtDate,'1/1/1900'), @Court_Date) = 0
			AND TTVA.violationstatusid <> 80	-- Adil 7001 11/14/2009 Do not associate new cases with client, quote and disposed cases
			AND TTVA.CourtLocation = 3004	
		END
		
		If @IDFound is null
			BEGIN
				Insert Into TrafficTickets.dbo.tblTicketsArchive (RecLoadDate, ListDate, ViolationDate, TicketNumber, MidNumber, FirstName, LastName, PhoneNumber, Address1, City, StateID_FK, ZipCode, GroupID, Flag1, DP2, DPC, InsertionLoaderID)   
				Values(@Record_Date, @List_Date, '1/1/1900', @CauseNumber, @MidNumber, @Name_First, @Name_Last, '', @Address, @City, @StateID, @ZIP, @GroupID, @AddressStatus, @DP2, @DPC, @LoaderID)	-- SAEED-7720-04/22/10, passing mid number

				Set @CurrentRecordID = scope_identity()
			End
		Else
			Begin
				Set @CurrentRecordID = @IDFound
			End
	End
Else
	Begin
		Set @CurrentRecordID = @PreviousRecordID
		Set @CurrentTicketNumber = @PreviousTicketNumber
	End

Set @IDFound = null
Set @IDFound = (Select top 1 RowID From TrafficTickets.dbo.tblTicketsViolationsArchive
			Where -- Adil 8331 09/29/2010 Cause Number Added
			(((CauseNumber = @CauseNumber AND @CauseNumber <> '') OR ViolationDescription = @Violation_Description AND RecordID = @CurrentRecordID AND CourtLocation = 3004))) --Afaq 7380 3/25/2010 remove check (AND RecordID = @CurrentRecordID) --SAEED-7720-04/22/10, add check (AND RecordID = @CurrentRecordID)

If @IDFound is null
	BEGIN
		IF @IsQuoted = 1 -- Adil 7380 02/05/2010 if current client is quoted client and coming violation is different from existing then insert new ticket.
			Begin
				Insert Into TrafficTickets.dbo.tblTicketsArchive (RecLoadDate, ListDate, ViolationDate, TicketNumber, MidNumber, FirstName, LastName, PhoneNumber, Address1, City, StateID_FK, ZipCode, GroupID, Flag1, DP2, DPC, InsertionLoaderID)   
				Values(@Record_Date, @List_Date, '1/1/1900', @CauseNumber, @MidNumber, @Name_First, @Name_Last, '', @Address, @City, @StateID, @ZIP, @GroupID, @AddressStatus, @DP2, @DPC, @LoaderID)	-- SAEED-7720-04/22/10, passing mid number

				Set @CurrentRecordID = scope_identity()
			END
		
		-- Adil 8331 09/29/2010 Cause Number Added
		-- Adil 8393 10/12/2010 Use cause number instead of blank for ticket number.
		Insert Into TrafficTickets.dbo.tblTicketsViolationsArchive (TicketNumber_PK, ViolationNumber_PK, CauseNumber, CourtLocation, CourtDate, TicketViolationDate, UpdatedDate, ViolationDescription, ViolationCode, FineAmount, RecordID, ViolationStatusID, LoaderUpdateDate, InsertionLoaderId, GroupID)
		Values(@CauseNumber, @ViolationID, @CauseNumber, 3004, @Court_Date, '1/1/1900', Getdate(), @Violation_Description, 'None', @FineAmount, @CurrentRecordID, 116, getdate(), @LoaderID, @GroupID)
		
		Set @Flag_Inserted = 1
	END
	ELSE
		BEGIN
			UPDATE TrafficTickets.dbo.tblTicketsViolationsArchive 
			-- Adil 8393 10/12/2010 Use cause number instead of blank for ticket number.
			SET CauseNumber = @CauseNumber, TicketNumber_PK = @CauseNumber, LoaderUpdateDate = GETDATE(), UpdationLoaderId = @LoaderID, GroupID = @GroupID
			WHERE RowID = @IDFound
			
			Set @Flag_Inserted = 2
		END

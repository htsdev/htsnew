﻿USE [LA_Houston]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTP_LA_Insert_PearLandRecords]    Script Date: 06/10/2013 16:16:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Author: Rab Nawaz Khan
Task : 11108
Date : 06/10/2013
Description: To Upload Pear Land Municipal court records FROM Loader Service
Type : Loader Service
Created date : 06/10/2013
Parameters: 
	@Ticket_Number --> Ticket Number
	@Cause_Number --> Cause number 
	@Violation_Status --> Current violation Status
	@Name_First --> Client First Name
	@Middle_Name --> Client Mid Name
	@Name_Last --> Client Last Name
	@Address --> Client Address
	@City --> Client City
	@State --> Client State
	@ZIP --> Client Zip
	@DP2 --> ZP4 DP2
	@DPC --> ZP4 DPC
	@Court_Date --> Client court date
	@Violation_Description --> violation summary
	@Violation_Date --> violation date
	@FineAmount --> charged amount
	@BondAmount --> bond amount
	@OfficerLastName --> office last name
	@OfficerFirstName  --> officer first name
	@AttorneyName  --> attorney name if assigned
	@AddressStatus  --> address verification statas by ZP4
	@Flag_SameAsPrevious --> same client with mutiple violations
	@PreviousRecordID --> Record Id
	@PreviousTicketID --> Ticket id if client
	@LoaderID --> loader Id
	@GroupID -->  Group Id
	@CurrentRecordID --> OUTPUT Parameter
	@CurrentTicketID --> OUTPUT Parameter
	@Flag_Inserted  --> OUTPUT Parameter
*/

ALTER PROCEDURE [dbo].[USP_HTP_LA_Insert_PearLandRecords]
	@Ticket_Number AS VARCHAR(30),
	@Cause_Number AS VARCHAR(30),
	@Violation_Status AS VARCHAR(50),
	@Name_First AS VARCHAR(50),
	@Middle_Name AS VARCHAR(50),  
	@Name_Last AS VARCHAR(50),  
	@Address AS VARCHAR(50),
	@City AS VARCHAR(35),  
	@State AS VARCHAR(10),
	@ZIP AS VARCHAR(10),
	@DP2 VARCHAR(2),
	@DPC VARCHAR(1),
	@Court_Date AS DATETIME,
	@Violation_Description AS VARCHAR(2000),
	@Violation_Date DATETIME,
	@FineAmount MONEY,
	@BondAmount MONEY,
	@OfficerLastName VARCHAR(50),
	@OfficerFirstName VARCHAR(50),
	@AttorneyName VARCHAR(100),
	@AddressStatus CHAR(1),
	@Flag_SameAsPrevious AS BIT,
	@PreviousRecordID AS INT,
	@PreviousTicketID AS INT,
	@LoaderID AS INT,
	@GroupID AS INT, 
	@CurrentRecordID INT OUTPUT,
	@CurrentTicketID INT OUTPUT,
	@Flag_Inserted INT OUTPUT
AS
BEGIN
	
	SET NOCOUNT ON  
	DECLARE @StateID AS INT
	DECLARE @ViolationID AS INT
	DECLARE @RecordIDFound AS INT
	DECLARE @TicketIDFound AS INT
	DECLARE @ViolationStatusFound AS INT
	DECLARE @AddressFound AS VARCHAR(200)
	DECLARE @AddressStatusFound AS CHAR(1)
	DECLARE @TicketNum2 AS VARCHAR(20)
	DECLARE @ViolationStatus AS INT
	DECLARE @IsQuoted AS INT
	DECLARE @shortDesc AS VARCHAR(100)
	DECLARE @TicketsViolationID AS INT
	DECLARE @IsFoundInClientDB AS BIT
	DECLARE @OfficerID INT
	DECLARE @NotFormatted_Cause_NumberNotFormatted VARCHAR(50)
	DECLARE @CauseNumberFound AS BIT
	
	SET @NotFormatted_Cause_NumberNotFormatted = @Cause_Number
    SET @Ticket_Number = REPLACE(@Ticket_Number, '-', '')
    SET @Cause_Number = REPLACE(@Cause_Number, '-', '') 
	SET @IsQuoted = 0
	SET @Name_First = UPPER(@Name_First)
	SET @Name_Last = UPPER(@Name_Last)
	SET @Address = REPLACE(@Address, '  ', ' ')
	SET @Flag_Inserted = 0
	SET @IsFoundInClientDB = 0
	SET @CauseNumberFound = 0
	IF @ZIP = '00000'
	BEGIN
		SET @ZIP = ''
	END

	-- Setting the Court Violation status. . . 
	IF (@Violation_Status = 'ARRAIGN')
	BEGIN
		SET @ViolationStatus = 3
	END
	
	ELSE IF ((@Violation_Status = 'TRJRY') OR (@Violation_Status = 'TRJYCNTDF') OR (@Violation_Status = 'TRJYCNTCT'))
	BEGIN
		SET @ViolationStatus = 26 -- Jury Trial. . . 
	END
	
	ELSE IF ((@Violation_Status = 'PTRESET') OR (@Violation_Status = 'ATTPRETR'))
	BEGIN
		SET @ViolationStatus = 101 -- Pre Trial. . . 
	END
	ELSE
		BEGIN
			SET @ViolationStatus = 147 -- By default setting violation Description as OTHER. . . 
		END
	
	
	-- Getting State Id. . . 
	SET @StateID = (SELECT TOP 1 StateID FROM TrafficTickets.dbo.TblState WHERE State = @State)
	
	-- Getting Violation Short Description. . . 
	SELECT TOP 1 @shortDesc = [Short Description] FROM	[LA_Houston].[dbo].[tblPearlandViolationDescriptions] WHERE LTRIM(RTRIM([Violation])) = @Violation_Description
	
	-- Getting Violation information if exists already. . . 
	SET @ViolationID = NULL
	SELECT TOP 1 @ViolationID = ViolationNumber_PK FROM  TrafficTickets.dbo.TblViolations WITH(NOLOCK)  WHERE (Description = @Violation_Description  AND CourtLoc = 3086 AND ViolationType = 17)
	
	-- Insert the Violation Information if its not exists already in our system against the Pearland court. . . 
	IF @ViolationID IS NULL
		BEGIN
			INSERT INTO TrafficTickets.dbo.TblViolations (Description, Sequenceorder, IndexKey, IndexKeys, ChargeAmount, BondAmount, Violationtype, ChargeonYesflag, ShortDesc, ViolationCode, AdditionalPrice, CourtLoc)
			VALUES(@Violation_Description, NULL, 2, 100, @FineAmount, @BondAmount, 17, 1, @shortDesc, 'none', 0, 3086)
			SET @ViolationID = SCOPE_IDENTITY()
		END

	-- Insert the Officer Information if its not exists already in our system. . . 
	SET @OfficerID = NULL
	    SELECT TOP 1 @OfficerID = OfficerNumber_PK
	    FROM   TrafficTickets.dbo.tblOfficer
	    WHERE  (
	               ISNULL(FirstName, '') = @OfficerFirstName
	               AND ISNULL(LastName, '') = @OfficerLastName
	    )
	    
	    IF @OfficerID IS NULL
	    BEGIN
	        SET @OfficerID = (SELECT MAX(OfficerNumber_PK) + 1 FROM   TrafficTickets.dbo.tblofficer)
	        
	        INSERT INTO TrafficTickets.dbo.tblOfficer ( OfficerNumber_PK, OfficerType, FirstName, LastName, Comments)
	        VALUES ( @OfficerID, 'N/A', @OfficerFirstName, @OfficerLastName,'Inserted at ' + CONVERT(VARCHAR(25), GETDATE(), 101) )
	    END

	---------------------------------------------------------- First lookup in client DB --------------------------------------------------------------------

	SET @TicketIDFound = NULL
	SET @TicketsViolationID = NULL
	-- Searching on Cause Number in Clients . . .
	IF EXISTS (SELECT casenumassignedbycourt FROM TrafficTickets.dbo.tblTicketsViolations WHERE LTRIM(RTRIM(casenumassignedbycourt)) = @Cause_Number AND CourtID = 3086)
	BEGIN
		SELECT TOP 1 @TicketIDFound = ttv.TicketID_PK, @TicketsViolationID = ttv.TicketsViolationID, @RecordIDFound = ttv.RecordID
		FROM TrafficTickets.dbo.tblTicketsViolations ttv WITH(NOLOCK) INNER JOIN TrafficTickets.dbo.tblTickets tt WITH(NOLOCK)
		ON ttv.TicketID_PK = tt.TicketID_PK
		WHERE ttv.CourtID = 3086 -- Pearland court 
		AND LTRIM(RTRIM(ttv.casenumassignedbycourt)) = @Cause_Number
		
		SET @CauseNumberFound = 1
		SET @IsFoundInClientDB = 1 
	END
	IF (@CauseNumberFound = 0)
	BEGIN
		IF EXISTS	(SELECT TOP 1 ttv.TicketID_PK	
				FROM TrafficTickets.dbo.tblTicketsViolations ttv WITH(NOLOCK) INNER JOIN TrafficTickets.dbo.tblTickets tt WITH(NOLOCK) 
				ON ttv.TicketID_PK = tt.TicketID_PK
				WHERE ttv.CourtID = 3086 -- Pearland court 
				AND LTRIM(RTRIM(tt.Firstname)) = @Name_First AND LTRIM(RTRIM(tt.Lastname)) = @Name_Last
				AND LTRIM(RTRIM(tt.Address1)) = @Address AND LTRIM(RTRIM(tt.Zip)) = @ZIP)
		-- Searching on FirstName, lastName, Address and Zip code in Clients. . . 
		BEGIN
			SELECT TOP 1 @TicketIDFound = ttv.TicketID_PK, @TicketsViolationID = ttv.TicketsViolationID, @RecordIDFound = ttv.RecordID
			FROM TrafficTickets.dbo.tblTicketsViolations ttv WITH(NOLOCK) INNER JOIN TrafficTickets.dbo.tblTickets tt WITH(NOLOCK)
			ON ttv.TicketID_PK = tt.TicketID_PK
			WHERE ttv.CourtID = 3086 -- Pearland court 
			AND LTRIM(RTRIM(tt.Firstname)) = @Name_First AND LTRIM(RTRIM(tt.Lastname)) = @Name_Last
			AND  LTRIM(RTRIM(tt.Address1)) = @Address AND LTRIM(RTRIM(tt.Zip)) = @ZIP
			
			SET @IsFoundInClientDB = 1
		END
	END
	
	-- If found in clients/quote. . . 
	IF (@IsFoundInClientDB = 1)
	BEGIN
		IF (@TicketsViolationID IS NOT NULL)
		BEGIN
			SET @CurrentTicketID = ISNULL(@TicketIDFound, 0)
			SET @CurrentRecordID = 0
			PRINT @CurrentTicketID
			UPDATE TrafficTickets.dbo.tblTicketsViolations
			SET CourtDate = @Court_Date, CourtViolationStatusID = @ViolationStatus, GroupID = @GroupID, loaderupdatedate = GETDATE(), 
			UpdatedDate = GETDATE(), VEmployeeID = 3992, TicketViolationDate = @Violation_Date
			WHERE TicketsViolationID = @TicketsViolationID AND TicketID_PK = @TicketIDFound
			
			SET @Flag_Inserted = 2
		END
		-- Client @TicketsViolationID is null means 2nd select clase execute. . . 
		--IF ((@TicketIDFound IS NOT NULL) AND (@TicketsViolationID IS NULL))
		ELSE
		BEGIN
			IF(ISNULL(@TicketIDFound, 0) <> 0)
			BEGIN
				INSERT INTO TrafficTickets.dbo.tblTicketsViolations (TicketID_PK, ViolationNumber_PK, FineAmount, refcasenumber, bondamount,
	            violationdescription, courtid, ticketviolationdate, TicketOfficerNumber, vemployeeid, CourtViolationStatusIDmain,
	            CourtNumbermain, CourtDatemain, casenumassignedbycourt, RecordID)
	            
	            VALUES (@TicketIDFound, @ViolationID, @FineAmount, @Ticket_Number, @BondAmount, @Violation_Description, 3086, @Violation_Date, @OfficerID, 3992,
	             @ViolationStatus, 0, @Court_Date,  @Cause_Number, @RecordIDFound)
	             SET @Flag_Inserted = 1
			END 
		END
		
		-- Adding Service Ticket if Client is in . . . 
		IF (@Violation_Status = 'TRJYCNTCT')
		BEGIN
			-- Searching the Previous violation status in archiving table. . . 
			DECLARE @PreviousViolationStatus VARCHAR(50)
			SET @PreviousViolationStatus = NULL
				
			SELECT TOP 1 @PreviousViolationStatus = pa.ViolationStatus
			FROM LoaderFilesArchive.dbo.tblPearland_ArrignmentOne_DataArchive pa WITH(NOLOCK)
			WHERE pa.CauseNumber = @NotFormatted_Cause_NumberNotFormatted
			ORDER BY pa.InsertDate DESC
				
			IF (@PreviousViolationStatus <> 'TRJYCNTCT')
			BEGIN
				DECLARE @folloupDate VARCHAR(50)
				SET @folloupDate = CONVERT(VARCHAR(50), [TrafficTickets].[dbo].[fn_getnextbusinessday] (GETDATE(),1), 101)
				EXEC TrafficTickets.dbo.usp_hts_AddServiceTicket @TicketID=@TicketIDFound,@EmpID=3992,@per_completion=0,@Category=4,@Priority=1,@ShowTrialDocket=1,@ShowServiceInstruction=1,@ServiceInstruction=N'Court data indicates court is filing for continuance ',@AssignedTo=4014,@ShowGeneralComments=1,@ContinuanceOption=1,@ServiceTicketfollowup = @folloupDate		
				EXEC TrafficTickets.dbo.USP_HTS_Insert_Comments @TicketID=@TicketIDFound,@EmployeeID=3992,@CommentID=N'1',@Comments=N'Court or DA filed for continuance ',@IsComplaint=0
			END
		END
	END
	
	---------------------------------------------------------- END lookup in client DB --------------------------------------------------------------------
	
	---------------------------------------------------------- Lookup in NON-Client DB if not found in client DB --------------------------------------------------------------------
	-- If record not found in Client DB then lookup in non-client DB. . . 
	IF (@IsFoundInClientDB = 0)
	BEGIN
		SET @CurrentTicketID = 0
		IF @Flag_SameAsPrevious = 0
		BEGIN
			SET @RecordIDFound = NULL
			-- Searching on Cause Number in Non-Clients . . .
			IF EXISTS (SELECT ttva.CauseNumber FROM TrafficTickets.dbo.tblTicketsViolationsArchive ttva WHERE LTRIM(RTRIM(ttva.CauseNumber)) = @Cause_Number)
			BEGIN
				SELECT TOP 1 @RecordIDFound = T.RecordID, @TicketNum2 = TicketNumber, @ViolationStatusFound = TV.violationstatusid, @IsQuoted = T.Clientflag, @AddressFound = ISNULL(T.Address1, 'N/A'), @AddressStatusFound = ISNULL(T.Flag1, 'N')
				FROM TrafficTickets.dbo.tblTicketsArchive T WITH(NOLOCK)
				INNER JOIN TrafficTickets.dbo.tblTicketsViolationsArchive TV WITH(NOLOCK) ON T.RecordID = TV.RecordID
				WHERE TV.CauseNumber = @Cause_Number
				AND TV.CourtLocation = 3086
			END
			ELSE
				-- Searching on FirstName, lastName, Address and Zip code in Non-Clients . . .
				BEGIN
					SELECT TOP 1 @RecordIDFound = T.RecordID, @TicketNum2 = TicketNumber, @ViolationStatusFound = TV.violationstatusid, @IsQuoted = T.Clientflag, @AddressFound = ISNULL(T.Address1, 'N/A'), @AddressStatusFound = ISNULL(T.Flag1, 'N')
					FROM TrafficTickets.dbo.tblTicketsArchive T WITH(NOLOCK)
					INNER JOIN TrafficTickets.dbo.tblTicketsViolationsArchive TV WITH(NOLOCK) ON T.RecordID = TV.RecordID
					WHERE FirstName = @Name_First 
					AND LastName = @Name_Last 
					AND Address1 = @Address
					AND ZipCode = @Zip
					AND TV.CourtLocation = 3086
				END
			-- If Record Not found then Insert the in Non-Client DB. . . 
			IF @RecordIDFound IS NULL
				BEGIN
					INSERT INTO TrafficTickets.dbo.tblTicketsArchive (RecLoadDate, ListDate, CourtDate, TicketNumber, MidNumber, FirstName, LastName, PhoneNumber, Address1, City, StateID_FK, ZipCode, CourtID, OfficerNumber_FK, OfficerName, Flag1, DP2, DPC, InsertionLoaderID,GroupID, ViolationDate)   
					VALUES(GETDATE(), GETDATE(), @Court_Date,  @Ticket_Number, @Ticket_Number, @Name_First, @Name_Last, '0000000000', @Address, @City, @StateID, @ZIP, 3086, @OfficerID, @OfficerFirstName + ' ' + @OfficerFirstName, @AddressStatus, @DP2, @DPC, @LoaderID,@GroupID, @Violation_Date)

					SET @CurrentRecordID = SCOPE_IDENTITY()
					SET @Flag_Inserted = 1
					
					INSERT INTO TrafficTickets.dbo.tblTicketsViolationsArchive (TicketNumber_PK, ViolationNumber_PK, CauseNumber, CourtDate, InsertDate, ViolationDescription, ViolationCode, FineAmount, BondAmount, CourtLocation, RecordID, ViolationStatusID, InsertionLoaderId, GroupID, AttorneyName, TicketViolationDate)
					VALUES(@Ticket_Number, @ViolationID, @Cause_Number, @Court_Date, GETDATE(), @Violation_Description, 'None', @FineAmount, @BondAmount, 3086, @CurrentRecordID, @ViolationStatus, @LoaderID, @GroupID, @AttorneyName, @Violation_Date)
				END
			-- If Record found then Update the Non-Client DB. . . 
			ELSE
				BEGIN
					SET @CurrentRecordID = @RecordIDFound
					IF @Address <> @AddressFound AND @AddressStatus <> 'N' AND @AddressStatusFound = 'N'
					BEGIN
						UPDATE TrafficTickets.dbo.tblTicketsArchive
						SET Address1 = @Address, City = @City, StateID_FK = @StateID, ZipCode = @ZIP, DP2 = @DP2, DPC = @DPC, flag1 = @AddressStatus, UpdationLoaderID = @LoaderID, GroupID = @GroupID, ViolationDate = @Violation_Date
						WHERE RecordID = @CurrentRecordID
						
						SET @Flag_Inserted = 2
						
						UPDATE TrafficTickets.dbo.tblTicketsViolationsArchive
						SET CourtDate = @Court_Date, ViolationStatusID = @ViolationStatus, UpdationLoaderId = @LoaderID, LoaderUpdateDate = GETDATE(), updateddate = GETDATE(), GroupID = @GroupID
						WHERE RowID = @RecordIDFound AND CourtLocation = 3086
						
					END
				END
		END
	ELSE
		BEGIN
			SET @CurrentRecordID = @PreviousRecordID		
			-- Require Update Query here to update the non-client database if its same as previous. . .
			SET @RecordIDFound = NULL
			SET @RecordIDFound = (SELECT TOP 1 RowID FROM TrafficTickets.dbo.tblTicketsViolationsArchive WITH(NOLOCK)
	        WHERE (CauseNumber = @Cause_Number AND CourtLocation = 3086 AND recordid = @PreviousRecordID)) 

			IF @RecordIDFound IS NULL
			BEGIN 
				INSERT INTO TrafficTickets.dbo.tblTicketsViolationsArchive (TicketNumber_PK, ViolationNumber_PK, CauseNumber, CourtDate, InsertDate, ViolationDescription, ViolationCode, FineAmount, BondAmount, CourtLocation, RecordID, ViolationStatusID, InsertionLoaderId, GroupID, AttorneyName, TicketViolationDate)
				VALUES(@Ticket_Number, @ViolationID, @Cause_Number, @Court_Date, GETDATE(), @Violation_Description, 'None', @FineAmount, @BondAmount, 3086, @CurrentRecordID, @ViolationStatus, @LoaderID, @GroupID, @AttorneyName, @Violation_Date)
				SET @Flag_Inserted = 1
			END
			ELSE
			BEGIN
				UPDATE TrafficTickets.dbo.tblTicketsViolationsArchive
				SET CourtDate = @Court_Date, ViolationStatusID = @ViolationStatus, UpdationLoaderId = @LoaderID, LoaderUpdateDate = GETDATE(), updateddate = GETDATE(), GroupID = @GroupID
				WHERE RowID = @RecordIDFound AND CourtLocation = 3086
				SET @Flag_Inserted = 2					
			END
		END -- End of Else . . . 
	END -- End Most Outer begin. . . 
END

GO
GRANT EXECUTE ON [dbo].[USP_HTP_LA_Insert_PearLandRecords] TO dbr_webuser
GO
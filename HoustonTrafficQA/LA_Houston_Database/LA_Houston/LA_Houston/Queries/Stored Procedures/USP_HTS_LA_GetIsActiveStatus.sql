set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go



CREATE PROCEDURE [dbo].[USP_HTS_LA_GetIsActiveStatus]
	@DownloadFrom varchar(100)
AS
BEGIN
	select IsActive from dbo.Tbl_HTS_LA_LoaderSetting where DownloadPath=@DownloadFrom   
END


grant execute on dbo.USP_HTS_LA_GetIsActiveStatus to dbr_webuser
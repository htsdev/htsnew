﻿/*
Author: Muhammad Adil Aleem
Business Logic: To dipose Houston cases loader wise
Type : Loader Service
Created date : 01/15/2010
Parameters: 
	@LoaderID : Specific loader ID.
	@GroupID : Specific data file number.
	@ClientDisposed [OUTPUT]: Returns client count for disposition.
	@NonClientDisposed [OUTPUT]: Returns non-client count for disposition.
*/
CREATE Procedure [dbo].[USP_LA_Houston_CaseDisposition]
@LoaderID int,
@GroupID INT,
@ClientDisposed int output,
@NonClientDisposed int output
as
Declare @LoopCounter int
Declare @RecordCount int

Create Table #Temp_Dispose_NonClient (SrNo int identity(1, 1), RowID int)

IF @LoaderID = 30
BEGIN
	Insert Into #Temp_Dispose_NonClient
	Select RowID
	From TrafficTickets.dbo.TblTicketsViolationsArchive
	WHERE DATEDIFF (DAY, CourtDate, GETDATE()) >= 1 AND violationstatusid = 116 AND CourtLocation = 3060
	AND TicketNumber_PK NOT IN (SELECT TicketNumber FROM LoaderFilesArchive.dbo.Sugarland_DataArchive WHERE GroupID = @GroupID)
END

Set @LoopCounter = 1
Set @RecordCount = (Select Count(*) from #Temp_Dispose_NonClient)

While @LoopCounter <= isnull(@RecordCount, 0)
	Begin
		Update TrafficTickets.dbo.TblTicketsViolationsArchive 
		Set ViolationStatusID = 80, UpdationLoaderID = @LoaderID, updateddate = GETDATE()
		Where RowID = (Select RowID From #Temp_Dispose_NonClient Where SrNo = @LoopCounter)
		Set @LoopCounter = @LoopCounter + 1
	End
drop table #Temp_Dispose_NonClient

Set @NonClientDisposed = @RecordCount

Create Table #Temp_Dispose_Client (SrNo int identity(1, 1), TicketsViolationID int)

IF @LoaderID = 0 -- Not is in use...
BEGIN
	Insert Into #Temp_Dispose_Client
	Select TicketsViolationID 
	From TrafficTickets.dbo.TblTicketsViolations
	where datediff(day, CourtDate, Getdate() - 60) >= 0 and CourtViolationStatusID in (26, 103)
END

Set @LoopCounter = 1
Set @RecordCount = (Select Count(*) from #Temp_Dispose_Client)

While @LoopCounter <= isnull(@RecordCount, 0)
	Begin
		Update TrafficTickets.dbo.TblTicketsViolations 
		Set CourtViolationStatusID = 80, UpdationLoaderID = @LoaderID
		Where TicketsViolationID = (Select TicketsViolationID From #Temp_Dispose_Client Where SrNo = @LoopCounter)
		Set @LoopCounter = @LoopCounter + 1
	End

Set @ClientDisposed = @RecordCount

GO
GRANT EXECUTE ON dbo.USP_LA_Houston_CaseDisposition TO dbr_webuser
GO 
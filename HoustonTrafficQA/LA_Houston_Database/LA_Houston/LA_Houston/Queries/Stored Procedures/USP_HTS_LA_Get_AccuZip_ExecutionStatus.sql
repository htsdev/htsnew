﻿ set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

CREATE procedure [dbo].[USP_HTS_LA_Get_AccuZip_ExecutionStatus]
@RunDate datetime
as

Select * from Tbl_HTS_LA_AccuZip_RunDate Where datediff(day, RunDate, @RunDate) = 0 and Status <> 'Completed'


--USP_HTS_LA_Get_AccuZip_ExecutionStatus '12/29/2008'


grant execute on dbo.USP_HTS_LA_Get_AccuZip_ExecutionStatus to dbr_webuser



﻿set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

/*
Author: Muhammad Adil Aleem
Description: To Upload Grand Prairie Warrant Cases From Loader Service
Type : Loader Service
Created date : 01/18/2010
Parameters: 
	@Record_Date: Case record/insert date
	@List_Date: Court list date
	@Ticket_Number: Ticket Number
	@Name_First: Defendant's first name
	@Name_Last: Defendant's last name
	@Address: Defendant's home address
	@City: Defendant's home city
	@State: Defendant's home state
	@ZIP: Defendant's home zip code
	@DP2: Defendant's barcode part. return by zp4 webservice
	@DPC: Defendant's barcode part. return by zp4 webservice
	@Violation_Description: offense description
	@GroupID: Data file ID
	@AddressStatus: Address status [y/d/s/n] of defendant
	@Flag_SameAsPrevious: insure wether this case belongs to previous one or not [It helps to cut-off researching the defendant profile]
	@PreviousRecordID: If this case belongs to previous case then previous case ID will be the value of @PreviousRecordID
	@LoaderID: Insure which loader is executing this case.
	@CurrentRecordID: It returns current Case-ID/Record-ID to calling program
	@Flag_Inserted: It returns status wether the record inserted or deleted
*/
ALTER PROCEDURE [dbo].[USP_LA_Insert_Tickets_GrandPrairie_Warrant]
@Record_Date datetime,  
@List_Date as smalldatetime,
@Ticket_Number as varchar(20),
@Name_First as varchar(50),  
@Name_Last as varchar(50),  
@Address as varchar(50),  
@City as varchar(35),  
@State as Varchar(10),
@ZIP as varchar(10),  
@DP2 varchar(2),
@DPC varchar(1),
@Violation_Description as Varchar(2000),
@Violation_Date DATETIME,
@Court_Date DATETIME,
@Fine_Amount FLOAT,
@GroupID as Int,
@AddressStatus VARCHAR(5),
@Flag_SameAsPrevious as bit,
@PreviousRecordID as int,
@LoaderID as int,
@CurrentRecordID int output,
@Flag_Inserted int Output
AS
set nocount on  

DECLARE @StateID AS INT
DECLARE @ViolationID AS INT
DECLARE @IDFound AS INT
DECLARE @TicketNum2 AS VARCHAR(20)
DECLARE @ChargeAmount AS MONEY
DECLARE @BondAmount AS MONEY
DECLARE @Warrant_Date AS DATETIME
DECLARE @Bond_Date_Found AS DATETIME
	
SET @Name_First = UPPER(LEFT(@Name_First, 20))
SET @Name_Last = UPPER(LEFT(@Name_Last, 20))

SET @Address = REPLACE(@Address, '  ', '')
SET @Flag_Inserted = 0
SET @ChargeAmount = 0
SET @BondAmount = 0
SET @ViolationID = NULL
SET @ZIP = REPLACE (@ZIP, ' ', '')

SET @Warrant_Date = @Court_Date

-- Adil 8484 11/11/2010 New file format changes.
--IF DATEPART(dw, @Court_Date) = 7 -- If Saturday then make it Friday (Court date should be in working days)
--BEGIN
--    SET @Court_Date = @Court_Date - 1
--END
--ELSE 
--IF DATEPART(dw, @Court_Date) = 1 -- If Sunday then make it Friday (Court date should be in working days)
--BEGIN
--    SET @Court_Date = @Court_Date - 2
--END

Set @Court_Date = convert(datetime,convert(varchar(12),@Court_Date,110) + ' 08:00 AM')

SELECT TOP 1 @ViolationID = ViolationNumber_PK, @ChargeAmount = ISNULL(ChargeAmount, 0), @BondAmount = ISNULL(BondAmount, 0)
FROM   DallasTrafficTickets.dbo.TblViolations
WHERE  (
           DESCRIPTION = @Violation_Description
           AND CourtLoc = 3049
           AND ViolationType = 4
       )

IF @ViolationID IS NULL
BEGIN
    INSERT INTO DallasTrafficTickets.dbo.TblViolations
                  (Description, SequenceOrder, IndexKey, IndexKeys, ChargeAmount, BondAmount, Violationtype, ChargeonYesflag, ShortDesc, ViolationCode, 
                  AdditionalPrice, Courtloc, CategoryID)
	VALUES     (@Violation_Description, NULL, 2, 100, 100, 175, 4, 1, 'None','99999', 0, 3049, 31)
    SET @ViolationID = SCOPE_IDENTITY()
END

SET @ChargeAmount = 0 -- as we don't have pricing schedule for Grand Prairie
SET @BondAmount = 0

SET @StateID = (
        SELECT TOP 1 StateID
        FROM   DallasTrafficTickets.dbo.TblState
        WHERE  STATE = @State
    )

IF @Flag_SameAsPrevious = 0
BEGIN
    SET @IDFound = NULL
    
    SELECT Top 1 @IDFound = T.RecordID, @TicketNum2 = TicketNumber, @Bond_Date_Found = isnull(BondDate,'1/1/1900')
	From DallasTrafficTickets.dbo.tblTicketsArchive T
	Inner Join DallasTrafficTickets.dbo.tblTicketsViolationsArchive TV
	On T.RecordID = TV.RecordID
	Where (SUBSTRING(TV.TicketNumber_PK, 1, LEN(ISNULL(TV.TicketNumber_PK, ''))-1) = SUBSTRING(@Ticket_Number, 1, LEN(ISNULL(@Ticket_Number, ''))-1) and TV.CourtLocation = 3055)
		AND T.Clientflag = 0 AND TV.violationstatusid <> 80
	
    IF @IDFound IS NULL
    BEGIN
        IF LEN(@Ticket_Number) < 1
        BEGIN
            INSERT INTO DallasTrafficTickets.dbo.tblIDGenerator(recdate)
            VALUES(GETDATE())
            SELECT @Ticket_Number = 'GP' + CONVERT(VARCHAR(12), @@identity)
        END	
        
        INSERT INTO DallasTrafficTickets.dbo.tblTicketsArchive
                    (RecLoadDate, ListDate, CourtDate, TicketNumber, MidNumber, FirstName, LastName, PhoneNumber, Address1, 
                    City, StateID_FK, ZipCode, CourtID, BondFlag, officerNumber_Fk, OfficerName, flag1, DP2, DPC, InsertionLoaderID, GroupID)
		VALUES      (@Record_Date,@List_Date,@Court_Date,@Ticket_Number,@Ticket_Number,@Name_First, @Name_Last, '0000000000',@Address,
					@City,@StateID,@ZIP, 3055, 1, 962528, 'N/A',@AddressStatus,@DP2,@DPC,@LoaderID,@GroupID)
        
        SET @CurrentRecordID = SCOPE_IDENTITY()
    END
    ELSE
    BEGIN
        SET @CurrentRecordID = @IDFound
		If datediff(day,@Bond_Date_Found, GetDate()) > 0
		BEGIN
			Update DallasTrafficTickets.dbo.tblTicketsArchive Set BondDate = @Warrant_Date, BondFlag = 1, GroupID = @GroupID, UpdationLoaderID = @LoaderID where RecordID = @IDFound
			Set @Flag_Inserted = 2
		End
        IF LEN(@Ticket_Number) < 1
        BEGIN
            SET @Ticket_Number = @TicketNum2
        END
    END
END
ELSE
BEGIN
    SET @CurrentRecordID = @PreviousRecordID
END

SET @IDFound = NULL
SELECT TOP 1 @IDFound = ISNULL(RowID, 0), @Bond_Date_Found = ISNULL(ViolationBondDate, '1/1/1900')
        FROM   DallasTrafficTickets.dbo.tblTicketsViolationsArchive
WHERE CauseNumber = @Ticket_Number and CourtLocation = 3055 AND RecordID = @CurrentRecordID

IF ISNULL(@IDFound, 0) = 0
BEGIN
    INSERT INTO DallasTrafficTickets.dbo.tblTicketsViolationsArchive
                (TicketNumber_PK, ViolationNumber_PK, CauseNumber, CourtDate, InsertDate, TicketViolationDate, ViolationDescription, ViolationCode, FineAmount, BondAmount, 
                ViolationBondDate, updateddate, CourtLocation, RecordID, violationstatusid, InsertionLoaderId, GroupID)
	VALUES      (@Ticket_Number,@ViolationID,@Ticket_Number,@Court_Date, GETDATE(), @Violation_Date, @Violation_Description,'99999',@ChargeAmount, @BondAmount, -- Adil 7297 01/19/2010 set 1/1/1900 to violation date.
				@Warrant_Date, GETDATE(), 3055, @CurrentRecordID, 116, @LoaderID, @GroupID)
    SET @Flag_Inserted = 1
END
ELSE
Begin
	If datediff(day,@Bond_Date_Found, @Warrant_Date) <> 0
	Begin
		Update DallasTrafficTickets.dbo.tblTicketsViolationsArchive SET FineAmount = @ChargeAmount, BondAmount = @BondAmount, ViolationBondDate = @Warrant_Date, LoaderUpdateDate = GetDate(), updateddate = GetDate(), UpdationLoaderId = @LoaderID, GroupID = @GroupID where RowID = @IDFound
		Set @Flag_Inserted = 2
	End
End







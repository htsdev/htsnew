﻿set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


/*
Author: Muhammad Adil Aleem
Description: To Upload Grand Prairie Appearance Cases From Loader Service
Type : Loader Service
Created date : Dec-31st-2009
Parameters: 
	@Record_Date: Case record/insert date
	@List_Date: Court list date
	@Ticket_Number: Ticket Number
	@Name_First: Defendant's first name
	@Name_Last: Defendant's last name
	@Address: Defendant's home address
	@City: Defendant's home city
	@State: Defendant's home state
	@ZIP: Defendant's home zip code
	@DP2: Defendant's barcode part. return by zp4 webservice
	@DPC: Defendant's barcode part. return by zp4 webservice
	@Violation_Description: offense description
	@Violation_Date: offense date
	@GroupID: Data file ID
	@AddressStatus: Address status [y/d/s/n] of defendant
	@Flag_SameAsPrevious: insure wether this case belongs to previous one or not [It helps to cut-off researching the defendant profile]
	@PreviousRecordID: If this case belongs to previous case then previous case ID will be the value of @PreviousRecordID
	@LoaderID: Insure which loader is executing this case.
	@CurrentRecordID: It returns current Case-ID/Record-ID to calling program
	@Flag_Inserted: It returns status wether the record inserted or deleted
*/
ALTER PROCEDURE [dbo].[USP_LA_Insert_Tickets_GrandPrairie_Arraignment]
@Record_Date datetime,  
@List_Date as smalldatetime,
@Ticket_Number as varchar(20),
@Name_First as varchar(50),  
@Name_Last as varchar(50),  
@Address as varchar(50),  
@City as varchar(35),  
@State as Varchar(10),
@ZIP as varchar(10),  
@DP2 varchar(2),
@DPC varchar(1),
@Violation_Description as Varchar(2000),
@Violation_Date DATETIME,
@Court_Date DATETIME,
@Fine_Amount FLOAT,
@GroupID as Int,
@AddressStatus VARCHAR(5),
@Flag_SameAsPrevious as bit,
@PreviousRecordID as int,
@LoaderID as int,
@CurrentRecordID int output,
@Flag_Inserted int Output
AS
set nocount on  

Declare @StateID as Int
Declare @ViolationID as int
Declare @IDFound as int
Declare @TicketNum2 as varchar(20)
Declare @ChargeAmount as money
Declare @BondAmount as money
Declare @Violation_Code as varchar(30)

Set @Name_First = left(@Name_First,20)
Set @Name_Last = left(@Name_Last,20)
Set @Address = Replace(@Address, '  ', '')
Set @Violation_Code = 'XD-none'
Set @Flag_Inserted = 0
Set @ChargeAmount = 0
Set @BondAmount = 0

if len(isnull(@DP2,'')) = 1
begin
	if isnull(@DP2,'') <> '0'
	begin
		Set @DP2 = '0' + @DP2
	end
END
-- Adil 8484 11/11/2010 New file format changes.
-- Adil 7408 02/12/2010 Use file date for court date if violation date is missing
--SET @Court_Date = (CASE WHEN DATEDIFF(DAY, @Violation_Date, '1/1/1900') <> 0 THEN @Violation_Date + 28 ELSE @Record_Date END) -- 28 days after offense date -- Adil 7244 01/13/2010 28 days added instead of 11 days.
--
--if datepart(dw,@Court_Date) = 7 -- If Saturday then make it Friday (Court date should be in working days)
--	begin
--		set @Court_Date = @Court_Date - 1
--	end
--else if datepart(dw,@Court_Date) = 1 -- If Sunday then make it Friday (Court date should be in working days)
--	begin
--		set @Court_Date = @Court_Date - 2
--	end
Set @Court_Date = convert(datetime,convert(varchar(12),@Court_Date,110) + ' 08:00 AM')
Set @ViolationID = null
Select Top 1 @ViolationID = ViolationNumber_PK, @Violation_Code = ViolationCode, @ChargeAmount = isnull(ChargeAmount,100), @BondAmount = isnull(BondAmount,175) From DallasTrafficTickets.dbo.TblViolations Where (Description = @Violation_Description and CourtLoc = 3049 and ViolationType = 4)

If @ViolationID is null
	Begin
		Insert Into DallasTrafficTickets.dbo.TblViolations (Description, ViolationCode, ViolationType, CourtLoc, ChargeAmount, BondAmount) Values(@Violation_Description, @Violation_Code, 4, 3049, 100, 200)
		Set @ViolationID = scope_identity()
	END
 
-- as we dont have pricing schedule for Grand Prairie	
SET @ChargeAmount = 0
SET @BondAmount = 0

Set @StateID = (Select Top 1 StateID From DallasTrafficTickets.dbo.TblState Where State = @State)

If @Flag_SameAsPrevious = 0
	BEGIN
		Set @IDFound = null

		SELECT Top 1 @IDFound = T.RecordID, @TicketNum2 = TicketNumber
		From DallasTrafficTickets.dbo.tblTicketsArchive T
		Inner Join DallasTrafficTickets.dbo.tblTicketsViolationsArchive TV
		On T.RecordID = TV.RecordID
		Where (SUBSTRING(TV.TicketNumber_PK, 1, LEN(ISNULL(TV.TicketNumber_PK, ''))-1) = SUBSTRING(@Ticket_Number, 1, LEN(ISNULL(@Ticket_Number, ''))-1) and TV.CourtLocation = 3055)
		AND T.Clientflag = 0 AND TV.violationstatusid <> 80
	
		If @IDFound is null
			Begin
				if Len(@Ticket_Number)<2
					Begin
						insert into DallasTrafficTickets.dbo.tblIDGenerator(recdate) values(getdate())
						select @Ticket_Number='GP' + convert(varchar(12),@@identity)
					End

				Insert Into DallasTrafficTickets.dbo.tblTicketsArchive (RecLoadDate, ListDate, TicketNumber, MidNumber, FirstName, LastName, PhoneNumber, Address1, City, StateID_FK, ZipCode, CourtID, OfficerNumber_FK, OfficerName, GroupID, Flag1, DP2, DPC, InsertionLoaderID)
				Values(@Record_Date, @List_Date, @Ticket_Number, @Ticket_Number, @Name_First, @Name_Last, '0000000000', @Address, @City, @StateID, @ZIP, 3055, 962528, 'N/A', @GroupID, @AddressStatus, @DP2, @DPC, @LoaderID)

				Set @CurrentRecordID = scope_identity()
			End
		Else
			Begin
				Set @CurrentRecordID = @IDFound
				if Len(@Ticket_Number)<1
					Begin
						Set @Ticket_Number = @TicketNum2
					End
			End
	End
Else
	Begin
		Set @CurrentRecordID = @PreviousRecordID
	End

Set @IDFound = null
Set @IDFound = (Select top 1 Isnull(RowID,0) From DallasTrafficTickets.dbo.tblTicketsViolationsArchive
			Where 
			(TicketNumber_PK = @Ticket_Number and ViolationNumber_PK = @ViolationID) and CourtLocation = 3055 AND RecordID = @CurrentRecordID)

If @IDFound is null
	Begin
		Insert Into DallasTrafficTickets.dbo.tblTicketsViolationsArchive (TicketNumber_PK, ViolationNumber_PK, CauseNumber, CourtDate, InsertDate, UpdatedDate, TicketViolationDate, ViolationDescription, ViolationCode, FineAmount, BondAmount, CourtLocation, RecordID, ViolationStatusID, LoaderUpdateDate, InsertionLoaderId, GroupID)
		Values(@Ticket_Number, @ViolationID, @Ticket_Number, @Court_Date, Getdate(), Getdate(), @Violation_Date, @Violation_Description, @Violation_Code, @ChargeAmount, @BondAmount, 3055, @CurrentRecordID, 116, getdate(), @LoaderID, @GroupID)
		Set @Flag_Inserted = 1
	End


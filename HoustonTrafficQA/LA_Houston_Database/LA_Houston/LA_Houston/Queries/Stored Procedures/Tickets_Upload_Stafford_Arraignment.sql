﻿USE [LA_Houston]
GO
/****** Object:  StoredProcedure [dbo].[Tickets_Upload_Stafford_Arraignment]    Script Date: 02/17/2010 19:37:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



/*
Author: Muhammad Adil Aleem
Description: To Upload Stafford Appearance Cases From Loader Service
Type : Loader Service
Created date : Feb-17th-2010
Parameters: 
	@Record_Date: Case record/insert date
	@List_Date: Court list date
	@Ticket_Number: Ticket Number
	@Name_First: Defendant's first name
	@Name_Last: Defendant's last name
	@Name_Middle: Defendant's middle name
	@Race: Defendant's race
	@Sex: Defendant's sex
	@Address: Defendant's home address
	@City: Defendant's home city
	@State: Defendant's home state
	@ZIP: Defendant's home zip code
	@DP2: Defendant's barcode part. return by zp4 webservice
	@DPC: Defendant's barcode part. return by zp4 webservice
	@Violation_Description: offense description
	@Court_Date: offense date
	@Officer_FirstName: Officer's first name
	@Officer_LastName: Officer's last name
	@GroupID: Data file ID
	@AddressStatus: Address status [y/d/s/n] of defendant
	@Flag_SameAsPrevious: insure wether this case belongs to previous one or not [It helps to cut-off researching the defendant profile]
	@PreviousRecordID: If this case belongs to previous case then previous case ID will be the value of @PreviousRecordID
	@LoaderID: Insure which loader is executing this case.
	@CurrentRecordID: It returns current Case-ID/Record-ID to calling program
	@Flag_Inserted: It returns status wether the record inserted or deleted
*/
CREATE PROCEDURE [dbo].[Tickets_Upload_Stafford_Arraignment]
@Record_Date datetime,
@List_Date as smalldatetime,
@Ticket_Number as varchar(20),
@Name_First as varchar(50),
@Name_Last as varchar(50),
@Name_Middle as varchar(50),
@Race as varchar(50),
@Sex as varchar(50),
@Address as varchar(50),
@City as varchar(35),
@State as Varchar(10),
@ZIP as varchar(10),
@DP2 varchar(2),
@DPC varchar(1),
@Violation_Description as Varchar(2000),
@Court_Date DATETIME,
@Officer_FirstName as varchar(50),
@Officer_LastName as varchar(50),
@GroupID as Int,
@AddressStatus VARCHAR(5),
@Flag_SameAsPrevious as bit,
@PreviousRecordID as int,
@LoaderID as int,
@CurrentRecordID int output,
@Flag_Inserted int Output
AS
set nocount on  

Declare @StateID as Int
Declare @ViolationID as int
Declare @IDFound as int
Declare @TicketNum2 as varchar(20)
Declare @ChargeAmount as money
Declare @BondAmount as money
Declare @Violation_Code as varchar(30)
DECLARE @IsQuoted AS INT
DECLARE @OfficerIDFound INT
DECLARE @OfficerCode VARCHAR(20)

SET @IsQuoted = 0
Set @Name_First = left(@Name_First,20)
Set @Name_Last = left(@Name_Last,20)
Set @Name_Middle = left(@Name_Middle,1)
Set @Address = Replace(@Address, '  ', '')
Set @Violation_Code = 'XD-none'
Set @Flag_Inserted = 0
Set @ChargeAmount = 0
Set @BondAmount = 0

if len(isnull(@DP2,'')) = 1
begin
	if isnull(@DP2,'') <> '0'
	begin
		Set @DP2 = '0' + @DP2
	end
end

if datepart(dw,@Court_Date) = 7 -- If Saturday then make it Friday (Court date should be in working days)
	begin
		set @Court_Date = @Court_Date - 1
	end
else if datepart(dw,@Court_Date) = 1 -- If Sunday then make it Friday (Court date should be in working days)
	begin
		set @Court_Date = @Court_Date - 2
	end

IF @Sex = 'M'
BEGIN
    SET @Sex = 'Male'
END
ELSE 
IF @Sex = 'F'
BEGIN
    SET @Sex = 'Female'
END

SELECT @Race = 
		CASE @Race
            WHEN 'A' THEN 'Asian'
            WHEN 'B' THEN 'Black'
            WHEN 'H' THEN 'Hispanic'
            WHEN 'I' THEN 'Indian'
            WHEN 'M' THEN 'Mexican'
            WHEN 'O' THEN 'Other'
            WHEN 'U' THEN 'Unknown'
            WHEN 'W' THEN 'White'
            WHEN 'X' THEN 'Unknown'
            ELSE NULL
		END

if isnull(@Officer_FirstName,'') + isnull(@Officer_LastName,'') = ''
	Begin
		Set @Officer_FirstName = 'N/A'
		Set @Officer_LastName = 'N/A'
	End

Set @OfficerIDFound = null
Select Top 1 @OfficerIDFound = OfficerNumber_PK from TrafficTickets.dbo.tblOfficer where (isnull(FirstName,'') = @Officer_FirstName AND isnull(LastName,'') = @Officer_LastName)
if @OfficerIDFound is null
	Begin
		Set @OfficerCode = (select max(OfficerNumber_PK) + 1 from TrafficTickets.dbo.tblofficer)
		Insert Into TrafficTickets.dbo.tblOfficer (OfficerNumber_PK, OfficerType, FirstName, LastName, Comments) 
		Values (@OfficerCode, 'N/A', @Officer_FirstName, @Officer_LastName, 'Inserted at ' + convert(varchar(25),getdate(),101))
	End
Else
	Begin
		Set @OfficerCode = @OfficerIDFound
	End

Set @ViolationID = null
Select Top 1 @ViolationID = ViolationNumber_PK, @Violation_Code = ViolationCode, @ChargeAmount = isnull(ChargeAmount,100), @BondAmount = isnull(BondAmount,175) From TrafficTickets.dbo.TblViolations Where (Description = @Violation_Description and CourtLoc = 3067 and ViolationType = 13)

If @ViolationID is null
	BEGIN
		Insert Into TrafficTickets.dbo.TblViolations (Description, ViolationCode, ViolationType, CourtLoc, ChargeAmount, BondAmount) Values(@Violation_Description, @Violation_Code, 13, 3067, 0, 0)
		Set @ViolationID = scope_identity()
	END
	
Set @StateID = (Select Top 1 StateID From TrafficTickets.dbo.TblState Where State = @State)

If @Flag_SameAsPrevious = 0
	BEGIN
		Set @IDFound = null

		SELECT TOP 1 @IDFound = T.RecordID, @TicketNum2 = ISNULL(TicketNumber, @Ticket_Number) , @IsQuoted = T.Clientflag
		FROM TrafficTickets.dbo.tblTicketsArchive T
		INNER JOIN TrafficTickets.dbo.tblTicketsViolationsArchive TV
		ON T.RecordID = TV.RecordID
		WHERE ((TV.TicketNumber_PK = @Ticket_Number OR TV.TicketNumber_PK = SUBSTRING(@Ticket_Number, 1, LEN(ISNULL(@Ticket_Number, ''))-1)) AND TV.CourtLocation = 3067 AND TV.violationstatusid <> 80)
	
		If @IDFound IS NULL
			Begin
				if Len(@Ticket_Number)<2
					Begin
						insert into TrafficTickets.dbo.tblIDGenerator(recdate) values(getdate())
						select @Ticket_Number='STMC' + convert(varchar(12),@@identity)
					End

				Insert Into TrafficTickets.dbo.tblTicketsArchive (RecLoadDate, ListDate, TicketNumber, MidNumber, FirstName, LastName, Initial, Gender, Race, PhoneNumber, Address1, City, StateID_FK, ZipCode, CourtID, OfficerNumber_FK, OfficerName, GroupID, Flag1, DP2, DPC, InsertionLoaderID)
				Values(@Record_Date, @List_Date, @Ticket_Number, @Ticket_Number, @Name_First, @Name_Last, @Name_Middle, @Sex, @Race, '0000000000', @Address, @City, @StateID, @ZIP, 3067, @OfficerIDFound, @Officer_LastName + ', ' + @Officer_FirstName, @GroupID, @AddressStatus, @DP2, @DPC, @LoaderID)
				
				Set @CurrentRecordID = scope_identity()
			End
		Else
			Begin
				Set @CurrentRecordID = @IDFound
				if Len(@Ticket_Number)<1
					Begin
						Set @Ticket_Number = @TicketNum2
					End
			End
	End
Else
	Begin
		Set @CurrentRecordID = @PreviousRecordID
	End

Set @IDFound = null
Set @IDFound = (Select top 1 Isnull(RowID,0) From TrafficTickets.dbo.tblTicketsViolationsArchive TV
                WHERE 
			((TV.TicketNumber_PK = @Ticket_Number OR TV.TicketNumber_PK = SUBSTRING(@Ticket_Number, 1, LEN(ISNULL(@Ticket_Number, ''))-1)) and TV.ViolationNumber_PK = @ViolationID) and TV.CourtLocation = 3067 AND TV.RecordID = @CurrentRecordID)

If @IDFound is null
	BEGIN
		IF @IsQuoted = 1
			Begin
				if Len(@Ticket_Number)<1
					Begin
						insert into DallasTrafficTickets.dbo.tblIDGenerator(recdate) values(getdate())
						select @Ticket_Number='STMC' + convert(varchar(12),@@identity)
					End

				Insert Into TrafficTickets.dbo.tblTicketsArchive (RecLoadDate, ListDate, TicketNumber, MidNumber, FirstName, LastName, Initial, Gender, Race, PhoneNumber, Address1, City, StateID_FK, ZipCode, CourtID, OfficerNumber_FK, OfficerName, GroupID, Flag1, DP2, DPC, InsertionLoaderID)
				Values(@Record_Date, @List_Date, @Ticket_Number, @Ticket_Number, @Name_First, @Name_Last, @Name_Middle, @Sex, @Race, '0000000000', @Address, @City, @StateID, @ZIP, 3067, 962528, 'N/A', @GroupID, @AddressStatus, @DP2, @DPC, @LoaderID)

				Set @CurrentRecordID = scope_identity()
			END
		Insert Into TrafficTickets.dbo.tblTicketsViolationsArchive (TicketNumber_PK, ViolationNumber_PK, CauseNumber, CourtDate, InsertDate, UpdatedDate, TicketViolationDate, ViolationDescription, ViolationCode, FineAmount, BondAmount, CourtLocation, RecordID, ViolationStatusID, LoaderUpdateDate, InsertionLoaderId, GroupID)
		Values(@Ticket_Number, @ViolationID, @Ticket_Number, @Court_Date, Getdate(), Getdate(), '1/1/1900', @Violation_Description, @Violation_Code, 0, 0, 3067, @CurrentRecordID, 116, getdate(), @LoaderID, @GroupID)
		Set @Flag_Inserted = 1
	End
	
GO

GRANT EXECUTE ON [dbo].[Tickets_Upload_Stafford_Arraignment] TO dbr_webuser
GO 
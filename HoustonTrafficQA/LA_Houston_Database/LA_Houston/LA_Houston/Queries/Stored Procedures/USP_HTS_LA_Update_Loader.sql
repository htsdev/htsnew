set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go



CREATE PROCEDURE [dbo].[USP_HTS_LA_Update_Loader]
	@LoaderID int,
	@ExecutionOrder int,
	@ExecutionTime varchar(10),
	@IsActive int
AS
BEGIN
	Update dbo.tbl_HTS_LA_Loaders set Execution_Order=@ExecutionOrder,IsActive=@IsActive,ExecutionTime=@ExecutionTime where Loader_ID=@LoaderID
END


grant execute on dbo.USP_HTS_LA_Update_Loader to dbr_webuser
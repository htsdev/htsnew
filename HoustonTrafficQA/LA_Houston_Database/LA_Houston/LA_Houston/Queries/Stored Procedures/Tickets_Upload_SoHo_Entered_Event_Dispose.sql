﻿set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go








/*
Author: Farrukh Iftikhar
Description: To Upload SoHo Entered, Events and Dispose CASEs From Loader Service
Type : Loader Service
Created date : May-24th-2011
Parameters:
	@Ticket_Number: Ticket Number
	@Cause_Number: Cause Number
	@Name_First: Defendant's first name
	@Name_Middle: Defendant's middle name
	@Name_Last: Defendant's last name
	@DOB: Defendant's date of birth
	@Race: Defendant's race
	@Gender: Defendant's gender
	@OfficerFirstName: Officer first name
	@OfficerLastName: Officer last name
	@DLNumber: Defendant's driving license number
	@DLState: Defendant's driving license state
	@PhoneNumber: Defendant's phone number
	@WorkPhoneNumber: Defendant's work phone number
	@Violation_Description: offense description
	@Court_Date: offense date
	@Court_Status: Court status
	@FineAmount: Offense charges
	@BondAmount: Warrant amount
	@BondFlag: Bond flag
	@Statute_Number: Statue Number
	@GroupID: Data file ID
	@LoaderID: Insure which loader IS executing this CASE
	@Warrant: Warrant flag
	@Conviction_Date: Conviction Date
	@Flag_Updated: It returns status wether the record inserted,updated or deleted	
	@Flag_ClientUpdated: It returns status wether the client record inserted,updated or deleted
*/
ALTER PROCEDURE [dbo].[Tickets_Upload_SoHo_Entered_Event_Dispose]
@Ticket_Number as varchar(20),
@Cause_Number as varchar(20),
@Name_First as varchar(50),
@Name_Middle as varchar(50),
@Name_Last as varchar(50),
@DOB AS DATETIME,
@Race AS VARCHAR(10),
@Gender AS VARCHAR(10),
@OfficerFirstName as varchar(50),
@OfficerLastName as varchar(50),
@DLNumber as varchar(20),
@DLState as Varchar(10),
@PhoneNumber as varchar(15),
@WorkPhoneNumber as varchar(12),
@Violation_Description as Varchar(2000),
@Court_Date AS DATETIME,
@Court_Status as varchar(4),
@FineAmount FLOAT,
@BondAmount FLOAT,
@BondFlag as BIT,
@Statute_Number as varchar(50),
@GroupID as Int,
@LoaderID as int,
@Warrant AS VARCHAR(2),
@Conviction_Date AS DATETIME,
@Flag_Updated int OUTPUT,
@Flag_ClientUpdated int OUTPUT
AS
SET nocount on  

Declare @CourtStatusID as Int
Declare @DLStateID as Int
Declare @ViolationID as int
Declare @IDFound as INT
Declare @RowIDFound as INT
DECLARE @ClientID AS INT
DECLARE @TicketID AS INT
DECLARE @Court_Date_Found DATETIME
DECLARE @Court_Status_Found INT
DECLARE @OfficerID AS INT
DECLARE @OfficerCodeFound AS INT
DECLARE @Statute_Number_Found VARCHAR(50)
DECLARE @CourtID INT
DECLARE @Violationtype INT

SET @CourtID = 3006
SET @Violationtype = 3
SET @Name_First = LEFT(@Name_First,20)
SET @Name_Last = LEFT(@Name_Last,20)
SET @Name_Middle = LEFT(@Name_Middle,1)
SET @Flag_Updated = 0
SET @Flag_ClientUpdated = 0

IF LEFT(@Gender,1) = 'M'
	BEGIN
		SET @Gender = 'Male'
	END
ELSE IF LEFT(@Gender,1) = 'F'
	BEGIN
		SET @Gender = 'Female'
	END



SELECT @Race = CASE @Race
					WHEN 'A' THEN 'Asian'
					WHEN 'B' THEN 'Black'
					WHEN 'H' THEN 'Hispanic'
					WHEN 'I' THEN 'Indian'
					WHEN 'M' THEN 'Mexican'
					WHEN 'O' THEN 'Other'
					WHEN 'U' THEN 'Unknown'
					WHEN 'W' THEN 'White'
					WHEN 'X' THEN 'Unknown'
					ELSE NULL
				END

IF DATEPART(dw,@Court_Date) = 7 -- IF SaturDAY THEN make it FriDAY (Court date should be in working DAYs)
	BEGIN
		SET @Court_Date = @Court_Date - 1
	END
ELSE IF DATEPART(dw,@Court_Date) = 1 -- IF SunDAY THEN make it FriDAY (Court date should be in working DAYs)
	BEGIN
		SET @Court_Date = @Court_Date - 2
	END

IF ISNULL(@OfficerFirstName,'') = ''
	BEGIN
		SET @OfficerFirstName = 'N/A'
	END
IF ISNULL(@OfficerLastName,'') = ''
	BEGIN
		SET @OfficerLastName = 'N/A'
	END

SET @OfficerID = NULL
SELECT Top 1 @OfficerID = OfficerNumber_PK from TrafficTickets.dbo.tblOfficer where (ISNULL(FirstName,'') = @OfficerFirstName AND ISNULL(LastName,'') = @OfficerLastName)
IF @OfficerID IS NULL
	BEGIN
		SET @OfficerID = (SELECT MAX(OfficerNumber_PK) + 1 from TrafficTickets.dbo.tblofficer)
		INSERT INTO TrafficTickets.dbo.tblOfficer (OfficerNumber_PK, OfficerType, FirstName, LastName, Comments) Values (@OfficerID, 'N/A', @OfficerFirstName, @OfficerLastName, 'Inserted at ' + CONVERT(varchar(25),getdate(),101))
	END

SET @ViolationID = NULL
SELECT Top 1 @ViolationID = ViolationNumber_PK, @FineAmount = CASE WHEN @FineAmount = 0 THEN ISNULL(ChargeAmount,0) ELSE @FineAmount END, @BondAmount = CASE WHEN @BondAmount = 0 THEN ISNULL(BondAmount,0) ELSE @BondAmount END From TrafficTickets.dbo.TblViolations Where (Description = @Violation_Description  and CourtLoc = @CourtID and ViolationType = @Violationtype)

IF @ViolationID IS NULL
	BEGIN
		INSERT INTO TrafficTickets.dbo.TblViolations (Description, Sequenceorder, IndexKey, IndexKeys, ChargeAmount, BondAmount, Violationtype, ChargeonYesflag, ShortDesc, ViolationCode, AdditionalPrice, CourtLoc)
		Values(@Violation_Description, NULL, 2, 100, @FineAmount, @BondAmount, @Violationtype, 1, 'None', 'none', 0, @CourtID)
		SET @ViolationID = SCOPE_IDENTITY()
	END

SET @CourtStatusID = 
CASE 
	WHEN UPPER(@Violation_Description) = 'FAILURE TO APPEAR/BAIL JUMPING' THEN 146 -- DLQ
	WHEN DATEDIFF(DAY,ISNULL(@Conviction_Date,'1/1/1900'), '1/1/1900') <> 0 THEN 80
	WHEN @LoaderID = 53 THEN 80 -- IF Dispose loader THEN SET court status to Dispose
	WHEN @Court_Status = 'AD' THEN 3 -- Arraignment
	WHEN @Court_Status IN ('EX', 'BH', 'PP', 'CL', 'CA', 'CD', 'CS', 'PJ', 'RA', 'SC') THEN 80 -- Dispose
	WHEN @Court_Status = 'TR' THEN 103 -- Judge Trial
	WHEN @Court_Status = 'TJ' THEN 26 -- Jury Trial
	WHEN @Court_Status IN ('PH', 'MD', 'PT') THEN 101 -- Pre Trial
	WHEN @Court_Status = 'SF' THEN 27 -- SCIRE	
	--WHEN @Court_Status IN ('BP' , 'VD') THEN 149 -- NO ACTION
	ELSE 147 -- Other
END

--IF @LoaderID = 52 AND @Warrant = 'W' AND @Court_Status IN ('AD', 'PH', 'MD', 'TR', 'TJ', 'SF', 'BH')
--	BEGIN
--		SET @BondFlag = 1
--	END
--ELSE
--	BEGIN
--		SET @BondFlag = 0
--	END

SET @DLStateID = (SELECT TOP 1 StateID From TrafficTickets.dbo.TblState Where State = @DLState)

SET @RowIDFound = NULL
SET @IDFound = NULL

SELECT TOP 1 @IDFound = T.RecordID
FROM TrafficTickets.dbo.tblTicketsArchive T
INNER JOIN TrafficTickets.dbo.tblTicketsViolationsArchive TV
ON T.RecordID = TV.RecordID
WHERE (TV.CauseNumber = @Cause_Number OR TV.TicketNumber_PK = @Ticket_Number OR 
LEFT(TV.TicketNumber_PK, LEN(TV.TicketNumber_PK) -2) = LEFT(@Ticket_Number, LEN(@Ticket_Number) -2))
AND TV.CourtLocation = @CourtID

IF @IDFound IS NOT NULL
	BEGIN
		UPDATE TrafficTickets.dbo.tblTicketsArchive
		SET DLNumber = @DLNumber, DLStateID = @DLStateID, DOB = @DOB,
		PhoneNumber = CASE WHEN CHARINDEX('0000000', @PhoneNumber, 1) > 0 THEN PhoneNumber ELSE @PhoneNumber END,
		WorkPhoneNumber = CASE WHEN CHARINDEX('0000000', @WorkPhoneNumber, 1) > 0 THEN WorkPhoneNumber ELSE @WorkPhoneNumber END,
		Bondflag = @BondFlag, UpdationLoaderId = @LoaderID, GroupID = @GroupID
		WHERE RecordID = @IDFound
		
		SET @Flag_Updated = 2
		
	END
ELSE IF @LoaderID = 52 OR (@CourtStatusID = 3 AND @LoaderID = 54) -- IF ARRAIGNMENT CASE CAME INTO EVENTS THEN NEED TO INSERT.
	BEGIN
		IF Len(@Ticket_Number)<1
			BEGIN
				INSERT INTO TrafficTickets.dbo.tblIDGenerator(recdate) values(getdate())
				SELECT @Ticket_Number='SHMC' + CONVERT(varchar(12),SCOPE_IDENTITY())
			END
			
		INSERT INTO TrafficTickets.dbo.tblTicketsArchive (RecLoadDate, ListDate, TicketNumber, MidNumber, FirstName, LastName, Initial, Race, Gender, PhoneNumber, WorkPhoneNumber, DLNumber, DLStateID, DOB, Address1, City, StateID_FK, ZipCode, Flag1, DP2, DPC, Bondflag, INSERTionLoaderID,GroupID)   
		Values(GETDATE(), GETDATE(), @Ticket_Number, @Ticket_Number, @Name_First, @Name_Last, @Name_Middle, @Race, @Gender, @PhoneNumber, @WorkPhoneNumber, @DLNumber, @DLStateID, @DOB, 'N/A', 'N/A', 45, '00000', 'N', '00', '0', @BondFlag, @LoaderID,@GroupID)

		SET @IDFound = SCOPE_IDENTITY()
	END

SELECT TOP 1 @RowIDFound = ISNULL(TV.RowID,0), @Court_Date_Found = ISNULL(TV.CourtDate,'1/1/1900'), @Court_Status_Found = ISNULL(tv.violationstatusid, 0)
FROM TrafficTickets.dbo.tblTicketsArchive T
INNER JOIN TrafficTickets.dbo.tblTicketsViolationsArchive TV
ON T.RecordID = TV.RecordID
WHERE TV.CauseNumber = @Cause_Number AND TV.CourtLocation = @CourtID

IF @RowIDFound IS NOT NULL
	BEGIN
		UPDATE TrafficTickets.dbo.tblTicketsViolationsArchive
		SET TicketOfficerNumber = CASE WHEN @LoaderID  IN (52, 54) THEN @OfficerID ELSE TicketOfficerNumber END,
		violationstatusid = CASE WHEN @Court_Status_Found <> @CourtStatusID THEN @CourtStatusID ELSE violationstatusid END,
		CourtDate = CASE WHEN @LoaderID IN (52, 54) AND @CourtStatusID IN (3, 26, 27, 101, 103, 146) AND (DATEDIFF(DAY, @Court_Date_Found, @Court_Date) <> 0) THEN @Court_Date ELSE CourtDate END, -- Only Arraignment, JuryTrial, SCIRE , PreTrial , JudgeTrial , DLQ statuses with future court date
		BondDate = CASE WHEN @BondFlag = 1 THEN @Court_Date ELSE BondDate END,
		-- if we have ticket number in cause number in database and we have a different cause number in data file then we will update the data file cause number into existing cause number field in non-client DB because existing cause number was not the valid cause number.
		CauseNumber = CASE WHEN @LoaderID  IN (52, 54) AND ISNULL(@Cause_Number, '') NOT IN ('', ISNULL(TicketNumber_PK, '')) THEN @Cause_Number ELSE CauseNumber END,
		Statutenumber = @Statute_Number, -- Add Statute number to update.
		FineAmount = @FineAmount, -- Add fine amount to update.
		UpdationLoaderId = @LoaderID, updateddate = GETDATE(), LoaderUpdateDate = GETDATE(), GroupID = @GroupID 
		WHERE RowID = @RowIDFound
		
		SET @Flag_Updated = 2
	END
ELSE IF @LoaderID = 52 OR (@CourtStatusID = 3 AND @LoaderID = 54) -- IF ARRAIGNMENT CASE CAME INTO EVENTS THEN NEED TO INSERT.
	BEGIN
		INSERT INTO TrafficTickets.dbo.tblTicketsViolationsArchive (TicketNumber_PK, ViolationNumber_PK, CauseNumber, CourtDate, BondDate, INSERTDate, ViolationDescription, ViolationCode, FineAmount, BondAmount, CourtLocation, RecordID, ViolationStatusID, Statutenumber, TicketOfficerNumber, INSERTionLoaderId, GroupID)
		Values(@Ticket_Number, @ViolationID, @Cause_Number, @Court_Date, CASE WHEN @BondFlag = 1 THEN @Court_Date ELSE NULL END, Getdate(), @Violation_Description, 'None', @FineAmount, @BondAmount, @CourtID, @IDFound, @CourtStatusID, @Statute_Number, @OfficerID, @LoaderID, @GroupID)
		SET @Flag_Updated = 1
	END

	    -- Update Quote/Client Auto Info
		SET @ClientID = NULL
		SET @TicketID = NULL
		
		SELECT TOP 1 @TicketID = ttv.TicketID_PK, @ClientID = ttv.TicketsViolationID, @Court_Date_Found = ttv.CourtDate, @TicketID = ttv.TicketID_PK, @Court_Status_Found = ttv.CourtViolationStatusID, @OfficerCodeFound = ISNULL(ttv.TicketOfficerNumber, ''), @Statute_Number_Found = ISNULL(ttv.Statutenumber, '')
		FROM TrafficTickets.dbo.tblTicketsViolations ttv
		WHERE ((ttv.RefCASENumber = @Ticket_Number OR (ttv.RefCASENumber = SUBSTRING (@Ticket_Number, 1 , LEN(@Ticket_Number) - 2) AND ttv.ViolationNumber_PK = @ViolationID)) AND ttv.CourtID = @CourtID)

		IF @ClientID IS NOT NULL
		BEGIN
			UPDATE TrafficTickets.dbo.tblTicketsViolations
			SET CourtViolationStatusID = CASE WHEN @Court_Status_Found <> @CourtStatusID THEN @CourtStatusID ELSE CourtViolationStatusID END,
			CourtDate = CASE WHEN @CourtStatusID IN (3, 26, 27, 101, 103, 146) AND (DATEDIFF(DAY, @Court_Date_Found, @Court_Date) <> 0) THEN @Court_Date ELSE CourtDate END, -- Only Arraignment, Jury Trial, Pre Trial , Judge Trial, JudgeTrial , DLQ statuses with future court date
			TicketOfficerNumber = CASE WHEN @LoaderID  IN (52, 54) THEN @OfficerID ELSE TicketOfficerNumber END,
			GroupID = @GroupID, UpdatedDate = GETDATE(), loaderupdatedate = GETDATE(), VEmployeeID = 3992,
			Statutenumber = @Statute_Number -- Add Statute number to update.
			WHERE TicketsViolationID = @ClientID
			
			SET @Flag_ClientUpdated = 2
			
			-- Adding court violation status code into Auto history of client exactly what we received in data files.
			UPDATE TrafficTickets.dbo.tblTicketsViolationLog
			SET SugarlandEvtDataFileStatus = @Court_Status
			WHERE TicketviolationID = @ClientID
			AND Rowid = (SELECT MAX(ttvl.Rowid)
			               FROM TrafficTickets.dbo.tblTicketsViolationLog ttvl
							WHERE ttvl.TicketviolationID = @ClientID)
			
			--INSERT history note WHEN auto court date time IS changed  by Event loader.
			IF DATEDIFF(hh, @Court_Date_Found, @Court_Date) <> 0 AND @CourtStatusID IN (3, 26, 27, 101, 103, 146) -- Arraignment, JuryTrial, SCIRE , PreTrial , JudgeTrial , DLQ
			BEGIN
				INSERT INTO TrafficTickets.dbo.tblticketsnotes (ticketid, subject , employeeid)
				SELECT @TicketID, 'Auto Court Date Updated - ' + ISNULL(@Ticket_Number, '') + ': From [' + TrafficTickets.dbo.fn_DateFormat(@Court_Date_Found, 5, '/', ':', 1) + '] To [' + TrafficTickets.dbo.fn_DateFormat(@Court_Date, 5, '/', ':', 1) + '] Through SoHo ' + CASE WHEN @LoaderID  = 53 THEN 'Dispose' WHEN  @LoaderID = 54 THEN 'Event' ELSE 'Entered' END + ' Loader', 3992
			END

			--INSERT history note WHEN auto court status IS changed by Event or Dispose loader.
			IF @Court_Status_Found <> @CourtStatusID
			BEGIN
				SELECT @Court_Status = 
				CASE 
				WHEN @CourtStatusID = 80 THEN 'DISP' -- WHEN Dispose loader THEN SET status to Dispose
				WHEN @CourtStatusID = 3 THEN 'ARR'
				WHEN @CourtStatusID = 26 THEN 'JUR'
				WHEN @CourtStatusID = 101 THEN 'PRE'
				WHEN @CourtStatusID = 103 THEN 'JUD'
				WHEN @CourtStatusID = 104 THEN 'WAIT'
				WHEN @CourtStatusID = 147 THEN 'OTHR'
				END
				
				DECLARE @Court_Status_Found_Str VARCHAR(10)
				SELECT @Court_Status_Found_Str = ShortDescription From TrafficTickets.dbo.tblcourtviolationstatus 
				where CourtViolationStatusID = @Court_Status_Found

				INSERT INTO TrafficTickets.dbo.tblticketsnotes (ticketid, subject , employeeid)
				SELECT @TicketID, 'Auto Court Status Updated - ' + ISNULL(@Ticket_Number, '') + ':From [' + @Court_Status_Found_Str + '] To [' + @Court_Status + '] Through SoHo ' + CASE WHEN @LoaderID  = 53 THEN 'Dispose' WHEN  @LoaderID = 54 THEN 'Event' ELSE 'Entered' END + ' Loader', 3992
				
				-- Advancing Follow-Up date.
				IF @Court_Status_Found = 104 AND @CourtStatusID = 104 -- If case is still in WAITING status then advance follow-up date to 14 days.
				BEGIN
					DECLARE @FollowUpDate DATETIME
					
					SET @FollowUpDate = GETDATE() + 14
					
					IF datepart(dw,@FollowUpDate) = 7 -- If Saturday then make it Friday (Follow-Up date must be in working days)
						BEGIN
							SET @FollowUpDate = @FollowUpDate - 1
						END
					ELSE IF datepart(dw,@FollowUpDate) = 1 -- If Sunday then make it Friday (Follow-Up date must be in working days)
						BEGIN
							SET @FollowUpDate = @FollowUpDate - 2
						END
					UPDATE TrafficTickets.dbo.tblTickets SET trafficWaitingFollowUpDate = @FollowUpDate WHERE TicketID_PK = @TicketID
					
					INSERT INTO TrafficTickets.dbo.tblticketsnotes (ticketid, subject , employeeid)
					SELECT @TicketID, 'Traffic waiting follow-up date advanced to 14 days through SoHo ' + CASE WHEN @LoaderID  = 53 THEN 'Dispose' WHEN  @LoaderID = 54 THEN 'Event' ELSE 'Entered' END + ' Loader', 3992
				END
			END
			
			--INSERT history note WHEN officer number changed.
			--IF @LoaderID IN (52, 54) AND @OfficerCodeFound <> @OfficerID
			IF @LoaderID IN (52, 54)
			BEGIN
				INSERT INTO TrafficTickets.dbo.tblticketsnotes (ticketid, subject , employeeid)
				SELECT @TicketID, 'Officer Number Updated - ' + ISNULL(@Ticket_Number, '') + ': From [' + CONVERT(VARCHAR(20), @OfficerCodeFound) + '] To [' + CONVERT(VARCHAR(20), @OfficerID) + '] Through SoHo ' + CASE WHEN @LoaderID  = 52 THEN 'Entered' WHEN @LoaderID  = 53 THEN 'Dispose' WHEN  @LoaderID = 54 THEN 'Event' ELSE 'Entered' END + ' Loader', 3992
			END
			
			-- INSERT history note WHEN statute number changed.
			IF @Statute_Number_Found <> @Statute_Number
			BEGIN
				INSERT INTO TrafficTickets.dbo.tblticketsnotes (ticketid, subject , employeeid)
				SELECT @TicketID, 'Statute Number Updated - ' + ISNULL(@Ticket_Number, '') + ': From [' + CONVERT(VARCHAR(20), @Statute_Number_Found) + '] To [' + CONVERT(VARCHAR(20), @Statute_Number) + '] Through SoHo ' + CASE WHEN @LoaderID  = 52 THEN 'Entered' WHEN @LoaderID  = 53 THEN 'Dispose' WHEN  @LoaderID = 54 THEN 'Event' ELSE 'Entered' END + ' Loader', 3992
			END
		END







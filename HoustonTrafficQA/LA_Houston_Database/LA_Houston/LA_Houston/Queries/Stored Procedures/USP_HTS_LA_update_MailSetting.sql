set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


CREATE PROCEDURE [dbo].[USP_HTS_LA_update_MailSetting]
@EmailSender varchar(200),
@EmailReceiver varchar(200),
@ErrorEmailReceiver varchar(200),
@ErrorEmailCounter varchar(200),
@DelayInCaseOfError varchar(10)	
AS
BEGIN
	update Tbl_HTS_LA_ConfigurationSetting set EmailSender =@EmailSender ,Receiver=@EmailReceiver,ErrorEmailReceiver=@ErrorEmailReceiver,ErrorEmailCounter=@ErrorEmailCounter,DelayInCaseOfError=@DelayInCaseOfError where ConfigurationID=1		
END


grant execute on dbo.USP_HTS_LA_update_MailSetting to dbr_webuser
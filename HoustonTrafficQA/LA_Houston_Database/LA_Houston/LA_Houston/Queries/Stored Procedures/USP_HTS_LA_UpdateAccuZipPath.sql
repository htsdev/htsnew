set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go



CREATE PROCEDURE [dbo].[USP_HTS_LA_UpdateAccuZipPath]
@AccuZipInput varchar(300),
@AccuZipOutput varchar(300),
@AccuZipExe varchar(300)
AS
BEGIN
	update Tbl_HTS_LA_ConfigurationSetting set AccuZipInputPath=@AccuZipInput,AccuZipOutputPath=@AccuZipOutput,AccuZipExePath=@AccuZipExe where ConfigurationId=1
END




grant execute on dbo.USP_HTS_LA_UpdateAccuZipPath to dbr_webuser
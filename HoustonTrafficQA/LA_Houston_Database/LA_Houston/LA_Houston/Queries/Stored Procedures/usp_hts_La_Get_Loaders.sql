set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go



CREATE PROCEDURE [dbo].[usp_hts_La_Get_Loaders]
	
AS
BEGIN
	
	SET NOCOUNT ON;

    Select Loader_ID as LoaderID,Loader_Name as Loader,Execution_Order as ExecutionOrder,IsActive,ExecutionTime from tbl_HTS_LA_Loaders order by Loader asc
END


grant execute on dbo.usp_hts_La_Get_Loaders to dbr_webuser
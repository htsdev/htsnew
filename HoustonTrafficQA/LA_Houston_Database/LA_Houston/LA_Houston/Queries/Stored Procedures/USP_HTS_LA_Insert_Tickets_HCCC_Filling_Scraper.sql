﻿USE [LA_Houston]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[USP_HTS_LA_Insert_Tickets_HCCC_Filling_Scraper]
@TicketNumber as varchar(20),
@CauseNumber as varchar(20),
@FirstName as varchar(50),
@MiddleName as varchar(1),
@LastName as varchar(50),
@SPN as varchar(35),
@CDI as varchar(3),
@CourtNumber as varchar(10),
@FileDate as varchar(10),
@CaseStatus as varchar(50),
@Offense as varchar(500),
@LastInstrumentFiled as varchar(100),
@CaseDisposition as varchar(100),
@CaseCompetionDate as varchar(10),
@DefendantStatus as varchar(50),
@BondAmount as varchar(10),
@LastSettingDate as varchar(10),
@Race as varchar(10),
@Sex as varchar(10),
@Height as varchar(MAX),
@Weight as varchar(MAX),
@Eyes as varchar(MAX),
@Hair as varchar(MAX),
@Skin as varchar(MAX),
@DOB as varchar(MAX),
@InCustody as varchar(MAX),
@USCitizen as varchar(MAX),
@Address as varchar(MAX),
@City as varchar(MAX),
@State as varchar(MAX),
@ZipCode as varchar(MAX),
@Markings as varchar(MAX),
@Build as varchar(MAX),
@PlaceOfBirth as varchar(MAX),
@LoaderID as varchar(MAX),
@AddressStatus as varchar(MAX),
@DP2 as varchar(MAX),
@DPC as varchar(MAX),
@GroupID as varchar(MAX),
@Flag_SameAsPrevious as bit,
@PreviousRecordID as int,
@PreviousTicketNumber as varchar(20),
@CurrentRecordID int output,
@Flag_Inserted int Output
AS
set nocount on
DECLARE @StateID      AS INT
DECLARE @ViolationID  AS INT
DECLARE @IDFound      AS INT
DECLARE @TicketNum2   AS VARCHAR(20)
DECLARE @IsQuoted     AS INT

SET @IsQuoted = 0
SET @SPN = REPLACE(@SPN, ',', '')
SET @FirstName = LTRIM(RTRIM(UPPER(LEFT(@FirstName, 20))))
SET @LastName = LTRIM(RTRIM(UPPER(LEFT(@LastName, 20))))
SET @TicketNumber = REPLACE(@TicketNumber, ' ', '')

SET @Flag_Inserted = 0

IF @ZipCode = '00000'
BEGIN
    SET @ZipCode = ''
END

SET @ViolationID = NULL
SELECT TOP 1 @ViolationID = ViolationNumber_PK FROM   TrafficTickets.dbo.TblViolations 
WHERE  (LTRIM(RTRIM(DESCRIPTION)) = LTRIM(RTRIM(@Offense)) AND ViolationType = 9)
IF NOT EXISTS(SELECT TOP 1 tva.Recordid  FROM   TrafficTickets.dbo.tblTicketsArchive ta INNER JOIN TrafficTickets.dbo.tblTicketsViolationsArchive tva ON tva.RecordID = ta.RecordID WHERE  left(tva.CauseNumber,7) = left(@CauseNumber,7))
BEGIN
IF @ViolationID IS NULL
BEGIN
    INSERT INTO TrafficTickets.dbo.TblViolations(DESCRIPTION,Sequenceorder,IndexKey,IndexKeys,ChargeAmount,BondAmount,Violationtype,ChargeonYesflag,ShortDesc,ViolationCode,AdditionalPrice,CourtLoc)
    VALUES(@Offense,NULL,2,100,0,@BondAmount,9,1,'None','none',0,3037)
    
    SET @ViolationID = SCOPE_IDENTITY()
END

IF LTRIM(RTRIM(ISNULL(@State, ''))) = ''
BEGIN
    SET @State = 'TX'
END

SET @StateID = (SELECT TOP 1 StateID  FROM   TrafficTickets.dbo.TblState  WHERE  STATE = @State)


    IF @Flag_SameAsPrevious = 0
    BEGIN
        SET @IDFound = NULL
        SELECT TOP 1 @IDFound = T.RecordID,@TicketNum2 = T.TicketNumber,@IsQuoted = T.Clientflag
        FROM   TrafficTickets.dbo.tblTicketsArchive T
               INNER JOIN TrafficTickets.dbo.tblTicketsViolationsArchive TV
               ON  TV.RecordID = T.RecordID
        WHERE  T.TicketNumber = @TicketNumber
               AND TV.violationstatusid <> 80
        
        IF @IDFound IS NULL
        BEGIN
            --- Insertion into Archive table	
            INSERT INTO TrafficTickets.dbo.tblTicketsArchive(TicketNumber,MidNumber,FirstName,LastName,Initial,Address1,City,StateID_FK,ZipCode,DOB,CourtDate,Clientflag,
            RecLoadDate,Race,Gender,Height,ListDate,CourtNumber,DP2,DPC,CourtID,GroupID,flag1,UpdationLoaderID,WEIGHT,InsertionLoaderID,
            SPNNumber,Eyes,HairColor,Skin,USCitizen,Markings,Build,InCustody,PlaceOfBirth)
            
            VALUES(@TicketNumber,@SPN,@FirstName,@LastName,@MiddleName,@Address,@City,@StateID,@ZipCode,@DOB,@LastSettingDate,0,GETDATE(),@Race,
            @Sex,@Height,@FileDate,@CourtNumber,@DP2,@DPC,3037,@GroupID,@AddressStatus,@LoaderID,@Weight,@LoaderID,@SPN,@Eyes,@Hair,@Skin,@USCitizen,@Markings,
            @Build,@InCustody,@PlaceOfBirth)
            ----------------------------------------------------------------------------------
            SET @CurrentRecordID = SCOPE_IDENTITY()
        END
        ELSE
        BEGIN
            SET @CurrentRecordID = @IDFound
            IF LEN(@TicketNumber) < 5
            BEGIN
                SET @TicketNumber = @TicketNum2
            END
        END
    END
    ELSE
    BEGIN
        SET @CurrentRecordID = @PreviousRecordID
        SET @TicketNum2 = ISNULL(@PreviousTicketNumber, '')
    END
    SET @IDFound = NULL
    SET @IDFound = (SELECT TOP 1 RowID FROM   TrafficTickets.dbo.tblTicketsViolationsArchive WHERE  (ISNULL(CauseNumber, '') = @TicketNumber))
    
    IF @IDFound IS NULL
    BEGIN
        IF @IsQuoted = 1
        BEGIN
            --- Insertion into Archive table	
			INSERT INTO TrafficTickets.dbo.tblTicketsArchive(TicketNumber,MidNumber,FirstName,LastName,Initial,Address1,City,StateID_FK,ZipCode,DOB,CourtDate,Clientflag,
            RecLoadDate,Race,Gender,Height,ListDate,CourtNumber,DP2,DPC,CourtID,GroupID,flag1,UpdationLoaderID,WEIGHT,InsertionLoaderID,
            SPNNumber,Eyes,HairColor,Skin,USCitizen,Markings,Build,InCustody,PlaceOfBirth)
            
            VALUES(@TicketNumber,@SPN,@FirstName,@LastName,@MiddleName,@Address,@City,@StateID,@ZipCode,@DOB,@LastSettingDate,0,GETDATE(),@Race,
            @Sex,@Height,@FileDate,@CourtNumber,@DP2,@DPC,3037,@GroupID,@AddressStatus,@LoaderID,@Weight,@LoaderID,@SPN,@Eyes,@Hair,@Skin,@USCitizen,@Markings,
            @Build,@InCustody,@PlaceOfBirth)
            
            SET @CurrentRecordID = SCOPE_IDENTITY()
        END
        ---Insertion into Violation Archive
        INSERT INTO TrafficTickets.dbo.tblTicketsViolationsArchive(TicketNumber_PK,ViolationDescription,ViolationCode,violationstatusid,CourtDate,Courtnumber,updateddate,CourtLocation,
        BondAmount,RecordID,CauseNumber,LoaderUpdateDate,GroupID,InsertDate,InsertionLoaderId,UpdationLoaderId,FiledDate,HCCC_CDI,
        ViolationNumber_PK,CaseCauseStatus,LastInstrumentFiled,CaseDisposition,CaseCompletionDate,DefendantStatus)
        VALUES(@TicketNumber,@Offense,'none',116,@LastSettingDate,@CourtNumber,GETDATE(),3037,@BondAmount,@CurrentRecordID,@CauseNumber,GETDATE(),@GroupID,
        GETDATE(),@LoaderID,@LoaderID,@FileDate,@CDI,@ViolationID,@CaseStatus,@LastInstrumentFiled,@CaseDisposition,@CaseCompetionDate,@DefendantStatus)
        -----------------------------------------------------------
        SET @Flag_Inserted = 1
    END
END

GO

GRANT EXECUTE ON [dbo].[USP_HTS_LA_Insert_Tickets_HCCC_Filling_Scraper] TO dbr_webuser

GO
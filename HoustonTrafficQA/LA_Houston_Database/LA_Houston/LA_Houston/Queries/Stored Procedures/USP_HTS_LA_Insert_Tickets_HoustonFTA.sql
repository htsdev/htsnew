﻿/*
Created By    : Fahad Muhammad Qureshi
Type          : Loader Service
TaskID		  : 6068
Created date  : 07/15/2009
Business Logic: This Procedure Load(Update OR Insert) the data as FTA cases on appropirate tables of Client/Non-Client in the system on the basis of following described Parameters.
Parameters: 
	@CauseNumber    :Cause no of the Defendant
	@Ticketnumber	:Ticket no of the Defendant
	@MidNumber		:used to combine multiple tickets for the sane defendant
	@SequenceNumber	:it uses in ticket number identicak for each violation
	@LinkID			:Date of the Court for the Defendant
	@UpdateDate		:Date when Record is Updating.
	@LoaderID		 :Id by which we can identify the Loader
	@GroupID		 :File id by which we extract Data
	@Flag_Inserted	 :Flag to check if record is insert or not 
	@IsNonClientUpdated :To check that Updating in Non-Client Records
	@IsNonClientInserted:To check that Insertion in Non-Client Records
*/ 
	CREATE PROCEDURE [dbo].[USP_HTS_LA_Insert_Tickets_HoustonFTA]
	@TicketNumber AS VARCHAR(50), 
	@CauseNumber AS VARCHAR(50), 
	@MidNumber AS VARCHAR(50) , 
	@SequenceNumber AS VARCHAR(50), 
	@LinkID AS VARCHAR(50), 
	@Updatedate AS DATETIME, 
	@LoaderID AS INT,
	@GroupID AS INT,
	@Flag_Inserted AS INT OUTPUT,
	@IsClientUpdated AS INT OUTPUT,
	@IsNonClientUpdated AS INT OUTPUT,
	@IsNonClientInserted AS INT OUTPUT
	
	AS
	DECLARE @ViolationIDFound AS INT
	DECLARE @UpdatedDateFound AS DATETIME
	DECLARE @IDFound AS INT
	DECLARE @TicketNum2 AS VARCHAR(20)
	DECLARE @lastrecordid AS VARCHAR(20)
	
	SET @IsClientUpdated=0
	SET @IsNonClientUpdated=0
	SET @IsNonClientInserted=0

	SET @IDFound=NULL 
			SELECT TOP 1 @IDFound = RowID,@UpdatedDateFound=TVA.updateddate,@ViolationIDFound =TVA.violationstatusid
			FROM   TrafficTickets.dbo.tblTicketsViolationsArchive TVA
			WHERE ((TVA.TicketNumber_PK=@TicketNumber OR TVA.TicketNumber_PK + CONVERT(VARCHAR, TVA.ViolationNumber_PK)=@TicketNumber) AND TVA.CourtLocation IN (3001, 3002, 3003))
	IF @IDFound is NULL
			BEGIN
						INSERT INTO TrafficTickets.dbo.tblticketsViolationsArchive
						  (TicketNumber_PK, ViolationNumber_PK,FineAmount,ViolationDescription,ViolationCode,violationstatusid,courtdate,courtnumber,courtlocation,bondamount,ticketviolationdate,CauseNumber,bonddate,updateddate,Ticketnumber_la,ftaissuedate,InsertionLoaderId,FTALinkId,GroupID)
						SELECT @ticketnumber,0,200,'FAILURE TO APPEAR (CUSTODY)','FTA',186,@updatedate,0,3001,200,@updatedate,@causenumber,@updatedate,@updatedate,@causenumber,@updatedate,@LoaderId,@LinkID,@GroupID
						SET @Flag_Inserted = 1
						SET @IsNonClientInserted=2
			END
	ELSE
		BEGIN
			IF (DATEDIFF(DAY ,ISNULL(@UpdatedDateFound ,'01/01/1900') ,@Updatedate) >=0 AND @ViolationIDFound<>186) OR (@ViolationIDFound = 146)
				BEGIN
					UPDATE TrafficTickets.dbo.tblticketsviolationsarchive
					SET    causenumber = @CauseNumber,ftalinkid = @LinkID,ftaissuedate = @Updatedate, updateddate = @Updatedate,bonddate = @Updatedate,
						   courtdate = CASE WHEN CHARINDEX('fta' ,@CauseNumber ,0)>0 THEN @Updatedate ELSE courtdate END
						  ,ticketviolationdate = CASE WHEN CHARINDEX('fta' ,@CauseNumber ,0)>0 THEN @Updatedate ELSE ticketviolationdate END
						  ,violationstatusid = 186,UpdationLoaderId = @LoaderId,GroupID = @GroupID 	
					WHERE  RowID = @IDFound

					SET @Flag_Inserted = 2
					SET @IsNonClientUpdated = 2
				END
		END
			
		SET @IDFound = NULL 
			
		SELECT TOP 1 @IDFound = tva.TicketsViolationID,@UpdatedDateFound=TVA.UpdatedDate,@ViolationIDFound =TVA.ViolationStatusID
		FROM   TrafficTickets.dbo.tblticketsviolations TVA
		WHERE (TVA.casenumassignedbycourt=@CauseNumber AND (DATEDIFF(DAY ,ISNULL(@UpdatedDateFound ,'01/01/1900'),@Updatedate))>=0 AND @ViolationIDFound<>186 AND TVA.CourtID IN (3001, 3002, 3003) )OR ((courtviolationstatusid=146)) 

			IF @IDFound IS NOT NULL
			BEGIN
				UPDATE TrafficTickets.dbo.tblticketsviolations
				SET    courtviolationstatusid = 186
					  ,courtdate = CASE WHEN CHARINDEX('fta' ,@CauseNumber ,0)<>0 THEN @Updatedate ELSE courtdate END
					  ,updateddate = @Updatedate
					  ,ticketviolationdate = CASE WHEN CHARINDEX('fta' ,@CauseNumber ,0) <>0 THEN @Updatedate ELSE ticketviolationdate END
					  ,GroupID = @GroupID
					  --Sabir 4640 08/21/2008 set bondreminderdate to today's date +2 business days when verified status is  "A/W" or "Arraignment" otherwise nothing to do with Bondreminderdate
					  ,BondReminderDate = CASE WHEN CourtViolationStatusIDmain IN (3 ,201) THEN CONVERT(VARCHAR(20) ,TrafficTickets.dbo.fn_getnextbusinessday(GETDATE() ,2) ,101) ELSE bondreminderdate END 
				WHERE 
						TicketsViolationID=@IDFound
						 SET @Flag_Inserted = 2
						 SET @IsClientUpdated=2

				END
GO
GRANT EXEC ON [dbo].[USP_HTS_LA_Insert_Tickets_HoustonFTA] TO dbr_webuser
GO  
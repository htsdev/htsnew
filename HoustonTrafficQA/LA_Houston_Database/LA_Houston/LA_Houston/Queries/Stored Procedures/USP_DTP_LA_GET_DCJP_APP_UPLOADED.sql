﻿/*
* Created Date : 10/13/2009
* Created By : Waqas Javed
* Task : 6731
* 
* Business Logic : 
* This procedure will check that DCJP Appearance loader file has been uploaded completely or not.
* 
* Parameter : 
* date : Specified datetime.
*/

Create PROCEDURE dbo.USP_DTP_LA_GET_DCJP_APP_UPLOADED
@date DATETIME
AS
BEGIN
	

SELECT DISTINCT  ISNULL(Isprocessed, 0) AS IsProcessed  FROM Tbl_HTS_LA_Group thlg WHERE 
DATEDIFF (DAY, thlg.List_Date, @date) = 0
AND thlg.LoaderID = 16 AND isprocessed IS NOT NULL --Waqas 6731 10/13/2009 not checking values

END

GO

GRANT EXECUTE ON dbo.USP_DTP_LA_GET_DCJP_APP_UPLOADED TO dbr_webuser   
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


--exec USP_HTS_LA_Login 'fahim', 'iqbal' , 0
CREATE PROCEDURE [dbo].[USP_HTS_LA_Login]
@User varchar(100),
@Password varchar(100),
@isUserfound int =0 output
AS
BEGIN
	Declare @UserName varchar(8000)
	set @UserName=null
	select @UserName = UserName from TrafficTickets.dbo.tblUsers where UserName=@User and Password=@Password
	if(@UserName is not null)
	set @isUserfound=1
	else
	set @isUserfound=0	
END
select @isUserfound as IsUserFound


grant execute on dbo.USP_HTS_LA_Login to dbr_webuser
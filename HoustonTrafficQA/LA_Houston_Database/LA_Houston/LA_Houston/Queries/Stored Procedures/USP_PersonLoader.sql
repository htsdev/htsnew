﻿USE [Crash]
GO
/****** Object:  StoredProcedure [dbo].[USP_PersonLoader]    Script Date: 09/02/2013 12:26:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sabir Khan Miani
-- Create date: 07/30/2013 TFS 11309
-- Description:	This procedure is used to insert person information for Crash cases.
-- =============================================
ALTER PROCEDURE [dbo].[USP_PersonLoader] 
@Crash_ID	int	,
@Unit_Nbr	int	,
@Prsn_Nbr	int	,
@Prsn_Type_ID	int	,
@Prsn_Occpnt_Pos_ID	int	,
@Prsn_Injry_Sev_ID	int	,
@Prsn_Age	int	,
@Prsn_Ethnicity_ID	int	,
@Prsn_Gndr_ID	int	,
@Prsn_Ejct_ID	int	,
@Prsn_Rest_ID	int	,
@Prsn_Airbag_ID	int	,
@Prsn_Helmet_ID	int	,
@Prsn_Sol_Fl	char(1)	,
@Prsn_Alc_Spec_Type_ID	int	,
@Prsn_Alc_Rslt_ID	int	,
@Prsn_Bac_Test_Rslt	decimal(4, 3)	,
@Prsn_Drg_Spec_Type_ID	int	,
@Prsn_Drg_Rslt_ID	int	,
@Prsn_Death_Time	VARCHAR(50)	,
@Incap_Injry_Cnt	varchar(100)	,
@Nonincap_Injry_Cnt	varchar(100)	,
@Poss_Injry_Cnt	varchar(100)	,
@Non_Injry_Cnt	varchar(100)	,
@Unkn_Injry_Cnt	varchar(100)	,
@Tot_Injry_Cnt	varchar(100)	,
@Death_Cnt	varchar(100)	,
@GroupID	int	,
@InsertionLoaderID	int	,
@UpdationLoaderID	int	
AS
BEGIN	
	SET NOCOUNT ON;	
	if not exists(SELECT c.Crash_ID FROM tbl_Person p inner join tbl_Crash c on c.Crash_ID = p.Crash_ID WHERE p.Crash_ID = @Crash_ID AND p.Prsn_Age = @Prsn_Age)
	--SELECT Crash_ID from tbl_Crash where Crash_ID = @Crash_ID AND DATEDIFF(DAY,insertDate,GETDATE()) = 0)
    begin
		Insert into tbl_Person(Crash_ID,Unit_Nbr,Prsn_Nbr,Prsn_Type_ID,Prsn_Occpnt_Pos_ID,Prsn_Injry_Sev_ID,Prsn_Age,Prsn_Ethnicity_ID,Prsn_Gndr_ID,Prsn_Ejct_ID,Prsn_Rest_ID,Prsn_Airbag_ID,
					Prsn_Helmet_ID,Prsn_Sol_Fl,Prsn_Alc_Spec_Type_ID,Prsn_Alc_Rslt_ID,Prsn_Bac_Test_Rslt,Prsn_Drg_Spec_Type_ID,Prsn_Drg_Rslt_ID,Prsn_Death_Time,Incap_Injry_Cnt,
					Nonincap_Injry_Cnt,Poss_Injry_Cnt,Non_Injry_Cnt,Unkn_Injry_Cnt,Tot_Injry_Cnt,Death_Cnt,GroupID,InsertionLoaderID,UpdationLoaderID)
		
		values(@Crash_ID,@Unit_Nbr,@Prsn_Nbr,@Prsn_Type_ID,@Prsn_Occpnt_Pos_ID,@Prsn_Injry_Sev_ID,@Prsn_Age,@Prsn_Ethnicity_ID,@Prsn_Gndr_ID,@Prsn_Ejct_ID,
				@Prsn_Rest_ID,@Prsn_Airbag_ID,@Prsn_Helmet_ID,@Prsn_Sol_Fl,@Prsn_Alc_Spec_Type_ID,@Prsn_Alc_Rslt_ID,@Prsn_Bac_Test_Rslt,@Prsn_Drg_Spec_Type_ID,
			   @Prsn_Drg_Rslt_ID,@Prsn_Death_Time,@Incap_Injry_Cnt,@Nonincap_Injry_Cnt,@Poss_Injry_Cnt,@Non_Injry_Cnt,@Unkn_Injry_Cnt,@Tot_Injry_Cnt,@Death_Cnt,
			   @GroupID,@InsertionLoaderID,@UpdationLoaderID)
	end	
END

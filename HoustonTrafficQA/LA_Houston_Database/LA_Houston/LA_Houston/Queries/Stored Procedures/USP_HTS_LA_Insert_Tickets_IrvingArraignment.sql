﻿




/*
Author: Muhammad Adil Aleem
Business Logic: To Upload Irving Entered Cases From Loader Service
Type : Loader Service
Created date : N/A
Parameters: 
	@Record_Date: Case record/insert date
	@List_Date: Court list date
	@Ticket_Number: Ticket Number
	@Name_First: Defendant's first name
	@Name_Last: Defendant's last name
	@Name_Middle: Defendant's Middle name
	@DOB: Defendant's Date of Birth
	@Address: Defendant's home address
	@City: Defendant's home city
	@State: Defendant's home state
	@ZIP: Defendant's home zip code
	@DP2: Defendant's barcode part. return by zp4 webservice
	@DPC: Defendant's barcode part. return by zp4 webservice
	@Violation_Date: Violation's Date.
	@Violation_Description: Violation's title of Defendant
	@Violation_Code: Violation's Code.
	@GroupID: Data file ID
	@AddressStatus: Address status [y/d/s/n] of defendant
	@Flag_SameAsPrevious: insure wether this case belongs to previous one or not [It helps to cut-off researching the defendant profile]
	@PreviousRecordID: If this case belongs to previous case then previous case ID will be the value of @PreviousRecordID
	@LoaderID: Insure which loader is executing this case.
	@CurrentRecordID: It returns current Case-ID/Record-ID to calling program
	@Flag_Inserted: It returns status wether the record inserted or deleted
*/

ALTER PROCEDURE [dbo].[USP_HTS_LA_Insert_Tickets_IrvingArraignment]
@Record_Date datetime,  
@List_Date as smalldatetime,
@Ticket_Number as varchar(20),
@Name_First as varchar(50),  
@Name_Last as varchar(50),  
@Name_Middle as varchar(10), 
@DOB datetime,
@Address as varchar(50),  
@City as varchar(35),  
@State as Varchar(10),
@ZIP as varchar(10),  
@DP2 varchar(2),
@DPC varchar(1),
@Violation_Date as smalldatetime,
@Violation_Description as Varchar(2000),
@Violation_Code as Varchar(50),
@GroupID as Int,
@AddressStatus Char(1),
@Flag_SameAsPrevious as bit,
@PreviousRecordID as int,
@LoaderID as int,
@CurrentRecordID int output,
@Flag_Inserted int Output
AS
set nocount on  

Declare @StateID as Int
Declare @ViolationID as int
Declare @IDFound as int
Declare @TicketNum2 as varchar(20)
Declare @ChargeAmount as money
Declare @BondAmount as money
Declare @Court_Date smalldatetime
DECLARE @IsQuoted AS INT

SET @IsQuoted = 0
SET @Ticket_Number = REPLACE(@Ticket_Number, ' ', '')

Set @Court_Date = dateadd(day,21,@Violation_Date)

if datepart(dw,@Court_Date) = 7 
	begin
		set @Court_Date = @Court_Date - 1
	end
else if datepart(dw,@Court_Date) = 1 
	begin
		set @Court_Date = @Court_Date - 2
	end

Set @Name_First = LTRIM(RTRIM(upper(left(@Name_First,20))))
Set @Name_Last = LTRIM(RTRIM(upper(left(@Name_Last,20))))
Set @Name_Middle = LTRIM(RTRIM(upper(left(@Name_Middle,1))))

if len(@Name_Middle)>5
	Begin
		Set @Name_Middle = left(@Name_Middle,1)
	End

Set @Address = Replace(@Address, '  ', '')
Set @Violation_Code = ltrim(rtrim(@Violation_Code))
Set @Violation_Description = ltrim(rtrim(@Violation_Description))
Set @Flag_Inserted = 0
Set @ChargeAmount = 0
Set @BondAmount = 0
If @ZIP = '00000'
	Begin
		Set @ZIP = ''
	End

Set @ViolationID = null
Select Top 1 @ViolationID = ViolationNumber_PK, @ChargeAmount = isnull(ChargeAmount,100), @BondAmount = isnull(BondAmount,150) From DallasTrafficTickets.dbo.TblViolations Where (Description = @Violation_Description and ViolationCode = @Violation_Code and CourtLoc = 3052 and ViolationType = 10)

If @ViolationID is null
	Begin
		Insert Into DallasTrafficTickets.dbo.TblViolations (Description, Sequenceorder, IndexKey, IndexKeys, ChargeAmount, BondAmount, Violationtype, ChargeonYesflag, ShortDesc, ViolationCode, AdditionalPrice, Attorneyregularprice, Attorneybondprice, CourtLoc)
		Values(@Violation_Description, null, 2, 160, 100, 150, 10, 1, 'None', @Violation_Code, 0, 50, 95, 3052)
		Set @ViolationID = scope_identity()
		Set @ChargeAmount = 100
		SET @BondAmount = 150 -- Adil 7246 01/16/2010 Bond amount will be fine amount + $50.
	End

SET @BondAmount = CASE WHEN (CHARINDEX ('speed', @Violation_Description, 0)> 0 OR CHARINDEX ('spd', @Violation_Description, 0) > 0) THEN 0 ELSE @BondAmount END -- Adil 01/15/2010 7246 bond amount will be $0 in case of speeding.

Set @StateID = (Select Top 1 StateID From DallasTrafficTickets.dbo.TblState Where State = @State)

If @Flag_SameAsPrevious = 0
	Begin
		Set @IDFound = null
		SELECT Top 1 @IDFound = T.RecordID, @TicketNum2 = TicketNumber , @IsQuoted = T.Clientflag
		From DallasTrafficTickets.dbo.tblTicketsArchive T
		Inner Join DallasTrafficTickets.dbo.tblTicketsViolationsArchive TV
		On T.RecordID = TV.RecordID
		Where TicketNumber = @Ticket_Number and len(TicketNumber) > 1
		AND TV.violationstatusid <> 80	-- Adil 7001 11/14/2009 Do not associate new cases with client, quote and disposed cases
		
		If @IDFound is null
			Begin
				Set @IDFound = NULL
				SELECT Top 1 @IDFound = T.RecordID, @TicketNum2 = TicketNumber 
				From DallasTrafficTickets.dbo.tblTicketsArchive T
				Inner Join DallasTrafficTickets.dbo.tblTicketsViolationsArchive TV
				On T.RecordID = TV.RecordID
				Where FirstName = @Name_First and LastName = @Name_Last and Address1 = @Address and ListDate = @List_Date and ListDate is not null
				AND T.Clientflag = 0 AND TV.violationstatusid <> 80	-- Adil 7001 11/14/2009 Do not associate new cases with client, quote and disposed cases
			End
	
		If @IDFound is null
			Begin
				if Len(@Ticket_Number)<1
					Begin
						insert into DallasTrafficTickets.dbo.tblIDGenerator(recdate) values(getdate())
						select @Ticket_Number='ID' + convert(varchar(12),@@identity)
					End

				Insert Into DallasTrafficTickets.dbo.tblTicketsArchive (RecLoadDate, ListDate, CourtDate, ViolationDate, TicketNumber, MidNumber, FirstName, LastName, Initial, DOB, PhoneNumber, Address1, City, StateID_FK, ZipCode, CourtID, OfficerNumber_FK, OfficerName, GroupID, Flag1, DP2, DPC, InsertionLoaderID)   
				Values(@Record_Date, @List_Date, @Court_Date, @Violation_Date, @Ticket_Number, @Ticket_Number, @Name_First, @Name_Last, @Name_Middle, @DOB, '0000000000', @Address, @City, @StateID, @ZIP, 3052, 962528, 'N/A', @GroupID, @AddressStatus, @DP2, @DPC, @LoaderID)

				Set @CurrentRecordID = scope_identity()
			End
		Else
			Begin
				Set @CurrentRecordID = @IDFound
				if Len(@Ticket_Number)<1
					Begin
						Set @Ticket_Number = @TicketNum2
					End
			End
	End
Else
	Begin
		Set @CurrentRecordID = @PreviousRecordID
	End

Set @IDFound = null
Set @IDFound = (Select top 1 Isnull(RowID,0) From DallasTrafficTickets.dbo.tblTicketsViolationsArchive
			Where 
			(TicketNumber_PK = @Ticket_Number)) --Afaq 7380 3/25/2010 remove check (AND RecordID = @CurrentRecordID)

If @IDFound is NULL
	BEGIN
		IF @IsQuoted = 1 -- Adil 7380 02/05/2010 if current client is quoted client and coming violation is different from existing then insert new ticket.
			Begin
				if Len(@Ticket_Number)<1
					Begin
						insert into DallasTrafficTickets.dbo.tblIDGenerator(recdate) values(getdate())
						select @Ticket_Number='ID' + convert(varchar(12),@@identity)
					End

				Insert Into DallasTrafficTickets.dbo.tblTicketsArchive (RecLoadDate, ListDate, CourtDate, ViolationDate, TicketNumber, MidNumber, FirstName, LastName, Initial, DOB, PhoneNumber, Address1, City, StateID_FK, ZipCode, CourtID, OfficerNumber_FK, OfficerName, GroupID, Flag1, DP2, DPC, InsertionLoaderID)   
				Values(@Record_Date, @List_Date, @Court_Date, @Violation_Date, @Ticket_Number, @Ticket_Number, @Name_First, @Name_Last, @Name_Middle, @DOB, '0000000000', @Address, @City, @StateID, @ZIP, 3052, 962528, 'N/A', @GroupID, @AddressStatus, @DP2, @DPC, @LoaderID)

				Set @CurrentRecordID = scope_identity()
			END
		Insert Into DallasTrafficTickets.dbo.tblTicketsViolationsArchive (TicketNumber_PK, ViolationNumber_PK, CauseNumber, CourtDate, UpdatedDate, TicketViolationDate, ViolationDescription, ViolationCode, FineAmount, BondAmount, CourtLocation, RecordID, ViolationStatusID, LoaderUpdateDate, InsertionLoaderId, GroupID)
		Values(@Ticket_Number, @ViolationID, @Ticket_Number, @Court_Date, Getdate(), @Violation_Date, @Violation_Description, @Violation_Code, @ChargeAmount, @BondAmount, 3052, @CurrentRecordID, 116, getdate(), @LoaderID, @GroupID)
		Set @Flag_Inserted = 1
	End
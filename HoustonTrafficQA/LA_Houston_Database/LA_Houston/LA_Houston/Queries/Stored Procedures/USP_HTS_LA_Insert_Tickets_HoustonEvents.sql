﻿/*
Created By    : Fahad Muhammad Qureshi
Type          : Loader Service
TaskID		  : 6068
Created date  : 07/14/2009
Business Logic: This Procedure Load(Update) the data as Events Cases on appropirate tables of Client/Non-Client cases in the system on the basis of following described Parameters.
Parameters: 
	@CauseNumber    :Cause no of the Defendant
	@Ticketnumber	:Ticket no of the Defendant
	@Status			:Case Status of the Defendant
	@CourtNumber	:Room Number of the Court
	@CourtDate		:Date of the Court for the Defendant
	@CourtTime		:Time Of the Court for the Defendant
	@CourtID		:Id by which we can identify the Court
	@UpdateDate		:Date when Record is Updating.
	@AssociatedCaseNumber:Number realted to case of the Defendant
	@BarNumber		:Number Identify the Attorney 
	@BondingNumber	:Number to Identify the Bonding Company
	@AttorneyName	:Attorney Name 
	@BondingCompanyName:Name of the Bonding Company
	@LoaderID		 :Id by which we can identify the Loader
	@GroupID		 :File id by which we extract Data
	@Flag_Inserted	 :Flag to check if record is insert or not 
	@IsClientUpdated :To check that Updating in Client or Non-Client Records
*/ 

CREATE PROCEDURE [dbo].[USP_HTS_LA_Insert_Tickets_HoustonEvents]
	@CauseNumber NVARCHAR(50),
	@TicketNumber NVARCHAR(50),
	@Status NVARCHAR(50),
	@CourtNumber NVARCHAR(3),
	@CourtDate DATETIME,
	@CourtTime DATETIME,
	@CourtID VARCHAR (10),
	@UpdateDate DATETIME,
	@AssociatedCaseNumber VARCHAR(50),
	@BarNumber VARCHAR(50),
	@BondingNumber VARCHAR(50),
	@AttorneyName VARCHAR(50),
	@BondingCompanyName VARCHAR(50),
	@LoaderID INT,
	@GroupID INT, 
	@Flag_Inserted INT OUTPUT,
    @IsClientUpdated INT OUTPUT,
    @IsNonClientUpdated INT OUTPUT
	AS
	
	DECLARE @ViolationStatusId INT
	DECLARE @TicketNum2 VARCHAR(20)
	
	SET @CourtDate = @CourtDate + @CourtTime
	SELECT @ViolationStatusId = TrafficTickets.dbo.fn_getviolationstatus(@STATUS)
	SET @IsClientUpdated=0
	SET @IsNonClientUpdated=0
	SET @CauseNumber = REPLACE(@CauseNumber,' ','')
	SET @Flag_Inserted = 0
	-----------------------------------------------------------------------------------------------------------------------
	-- INSERTING NEW CASE STATUSES IN TBLCOURTVIOLATIONSTATUS...........
	----------------------------------------------------------------------------------------------------------------------- 

If @ViolationStatusId is null
	BEGIN
		INSERT INTO TrafficTickets.dbo.tblcourtviolationstatus
	      (DESCRIPTION,violationcategory,ShortDescription,CategoryID,SortOrder,CourtStatusUpdateDate)
	    VALUES
	      (@status,0,LEFT(@status, 3),0,0,GETDATE())
		
		Set @ViolationStatusId = scope_identity()
	End

	-----------------------------------------------------------------------------------------------------------------------
	-- INSERTING NISI CASES IN A SEPARATE TABLE.............
	-----------------------------------------------------------------------------------------------------------------------    
	IF (
	       NOT EXISTS(
	  SELECT *
	  FROM   TrafficTickets.dbo.tbleventdataanlysis
	  WHERE  causenumber = @CauseNumber
	       )
	       AND (
	      CHARINDEX('nisi', @CauseNumber) <> 0
	      OR CHARINDEX('nisi', @Status) <> 0
	  )
	   )
	BEGIN
	    INSERT INTO TrafficTickets.dbo.tbleventdataanlysis
	      (causenumber,ticketnumber,STATUS,courtnumber,courtdate,courttime,updatedate,violationstatusid,assocmidnumber,barnumber,bondingnumber,attorneyname,bondingcompanyname)
	    VALUES
	      (@CauseNumber,@TicketNumber,@Status,@CourtNumber,@CourtDate,@CourtTime,@UpdateDate,@ViolationStatusId,@AssociatedCaseNumber,@BarNumber,@BondingNumber,@AttorneyName,@BondingCompanyName)
	END
	
	IF NOT EXISTS (SELECT NAME FROM   TrafficTickets.dbo.tblcompetitors WHERE  (NAME = ISNULL(@AttorneyName, @BarNumber)OR NAME = ISNULL(@BondingCompanyName, @BondingNumber)))
	BEGIN
	    INSERT INTO TrafficTickets.dbo.tblCompetitors
	      ([Name],BarCardNumber,BondingNumber,IsAttorney,IsBondingCompany,InsertDate)
	    VALUES
	      (ISNULL(@AttorneyName, @BarNumber),@BarNumber,ISNULL(@BondingCompanyName, @BondingNumber),1,@BondingNumber,GETDATE())
	END
	ELSE
	BEGIN
	    UPDATE TrafficTickets.dbo.tblCompetitors
	    SET    TrafficTickets.dbo.tblCompetitors.barcardnumber = @BarNumber,
	  TrafficTickets.dbo.tblCompetitors.IsAttorney = 1
	    WHERE  TrafficTickets.dbo.tblCompetitors.[Name] = @AttorneyName
	  AND LEN(ISNULL(@BarNumber, '')) > 0
	    
	    UPDATE TrafficTickets.dbo.tblCompetitors
	    SET    TrafficTickets.dbo.tblCompetitors.BondingNumber = @BondingNumber,
	  TrafficTickets.dbo.tblCompetitors.IsBondingCompany = 1
	    WHERE  TrafficTickets.dbo.tblCompetitors.[Name] = @BondingCompanyName
	  AND LEN(ISNULL(@BondingNumber, '')) > 0 
	      
	END
	
	DECLARE @IDFound INT
	DECLARE @ViolationStatusIDFound INT
	DECLARE @InitialEventDate_AttorneyFound DATETIME
	DECLARE @InitialEventDate_BondingCompanyFound DATETIME
	DECLARE @UpdatedDateFound DATETIME
	DECLARE @FTAIssueDateFound DATETIME
	
	Set @IDFound = NULL 
	SELECT TOP 1 @IDFound = RowID, @CauseNumber=CauseNumber,@ViolationStatusIDFound = ViolationStatusID, @InitialEventDate_AttorneyFound = ISNULL(InitialEventDate_Attorney, '1/1/1900'), @InitialEventDate_BondingCompanyFound = ISNULL(InitialEventDate_BondingCompany, '1/1/1900'), @UpdatedDateFound = ISNULL(UpdatedDate, '1/1/1900'), @FTAIssueDateFound = ISNULL(FTAIssueDate, '1/1/1900')
	FROM   TrafficTickets.dbo.tblTicketsViolationsArchive
	WHERE  (CauseNumber = @CauseNumber AND CourtLocation IN (3001, 3002, 3003))

		If @IDFound is NOT null
		BEGIN
			IF (@ViolationStatusIDFound <> @ViolationStatusId OR DATEDIFF (DAY,@UpdatedDateFound, @UpdateDate) >= 0)
					BEGIN
					UPDATE ttva
					SET    ttva.barcardnumber = CASE WHEN (LEN(@BarNumber) > 0 AND LEN(@AttorneyName)> 0 AND (DATEDIFF(DAY, @InitialEventDate_AttorneyFound, '1/1/1900') = 0)) THEN @BarNumber ELSE ttva.BarCardNumber END,
						   ttva.attorneyname = CASE WHEN (LEN(@BarNumber) > 0 AND LEN(@AttorneyName) > 0 AND  (DATEDIFF(DAY, @InitialEventDate_AttorneyFound, '1/1/1900') = 0)) THEN @AttorneyName ELSE ttva.AttorneyName END,
						   ttva.BondingNumber = CASE WHEN (LEN(@BondingNumber) > 0 AND LEN(@BondingCompanyName) > 0 AND (DATEDIFF(DAY,@InitialEventDate_BondingCompanyFound ,'1/1/1900')=0)) THEN @BondingNumber ELSE ttva.BondingNumber END,
						   ttva.BondingCompanyName = CASE WHEN (LEN(@BondingNumber) >0 AND LEN(@BondingCompanyName)> 0 AND(DATEDIFF(DAY,@InitialEventDate_BondingCompanyFound ,'1/1/1900')=0)) THEN @BondingCompanyName ELSE ttva.BondingCompanyName END,
						   ttva.InitialEventDate_BondingCompany = CASE WHEN (LEN(@BondingNumber)>0 AND LEN(@BondingCompanyName)> 0 AND (DATEDIFF(DAY,@InitialEventDate_BondingCompanyFound ,'1/1/1900')=0)) THEN GETDATE()ELSE ttva.InitialEventDate_BondingCompany END,
						   ttva.InitialEventDate_Attorney = CASE WHEN (LEN(@BarNumber)>0 AND LEN(@AttorneyName)> 0 AND (DATEDIFF(DAY, @InitialEventDate_AttorneyFound, '1/1/1900') = 0)) THEN GETDATE()ELSE ttva.InitialEventDate_Attorney END,
						   ttva.violationstatusid = @ViolationStatusId,
						   ttva.CourtDate = @CourtDate,
						   ttva.FTAIssueDate = CASE WHEN (@ViolationStatusId=186 AND DATEDIFF(DAY,'1/1/1900',@FTAIssueDateFound) =0) THEN GETDATE()ELSE ttva.FTAIssueDate END,
						   ttva.CourtLocation = @CourtId,
						   ttva.UpdationLoaderId = @LoaderID,
						   ttva.GroupID = @GroupID,
						   ttva.UpdatedDate = GETDATE() 
					FROM TrafficTickets.dbo.tblTicketsViolationsArchive ttva
					WHERE  RowID = @IDFound
					
					Set @Flag_Inserted = 2
					SET @IsNonClientUpdated=2
					END
		END
	
	Set @IDFound = NULL 
	SET @ViolationStatusIDFound=NULL
	SET @UpdatedDateFound=NULL 
	
	SELECT TOP 1 @IDFound = TrafficTickets.dbo.tblticketsviolations.TicketsViolationID, @ViolationStatusIDFound = TrafficTickets.dbo.tblticketsviolations.courtViolationStatusIDmain,@UpdatedDateFound=ISNULL(TrafficTickets.dbo.tblticketsviolations.UpdatedDate,'1/1/1900')
	FROM   TrafficTickets.dbo.tblTicketsViolations 
	WHERE  (TrafficTickets.dbo.tblticketsviolations.casenumassignedbycourt = @CauseNumber AND TrafficTickets.dbo.tblticketsviolations.CourtID IN (3001, 3002, 3003))
	
	
	If @IDFound is NOT null
	BEGIN
		IF @ViolationStatusIDFound <> @ViolationStatusId OR DATEDIFF(DAY,@UpdatedDateFound,@UpdateDate) >= 0
		BEGIN
		UPDATE TrafficTickets.dbo.tblticketsviolations
		SET    TrafficTickets.dbo.tblticketsviolations.courtviolationstatusid = @ViolationStatusId,
			   TrafficTickets.dbo.tblticketsviolations.courtdate = @CourtDate,
			   TrafficTickets.dbo.tblticketsviolations.UpdatedDate = @UpdateDate,
			   TrafficTickets.dbo.tblticketsviolations.courtnumber = @CourtNumber,
			   TrafficTickets.dbo.tblticketsviolations.courtid = @CourtId,
				TrafficTickets.dbo.tblticketsviolations.GroupID = @GroupID,
			   TrafficTickets.dbo.tblticketsviolations.BondReminderDate = 
			   CASE WHEN TrafficTickets.dbo.tblticketsviolations.CourtViolationStatusIDmain IN (3, 201) AND @ViolationStatusId IN (186, 274) -- 186 = FTA Warrant, 274 = Warrant, 3 = Arraignment, 201 = Arr/Waiting
			   THEN TrafficTickets.dbo.fn_getnextbusinessday(GETDATE(), 2)ELSE TrafficTickets.dbo.tblticketsviolations.bondreminderdate END
	   WHERE   TrafficTickets.dbo.tblticketsviolations.TicketsViolationID = @IDFound
	   
			   Set @Flag_Inserted = 2
			   SET @IsClientUpdated = 2
	   END		
	END		
GO
GRANT EXEC ON [dbo].[USP_HTS_LA_Insert_Tickets_HoustonEvents] TO dbr_webuser
GO  
﻿
/*        
Altered By     : Muhammad Adil Aleem.
Created Date   : 08/05/2009  
TasK		   : 6283        
Business Logic : This Stored Procedure Identify the records according to nature and following described mode that which are insert or update to the System.
				Insert_Mode 1: Insert-able Cases & Defendants
				Insert_Mode 2: Existing Updateable Cases
				Insert_Mode 3: Existing Cases
				Insert_Mode 4: Existing Defendants
				Insert_Mode 5: Past Court Date Cases      
*/

ALTER Procedure [dbo].[USP_HTS_LA_FTP_IdentifyCases]
@GroupID int
as
-- Identifying Existing Defendants
Update arch
Set arch.Insert_Mode = 4, arch.RecordID = T.RecordID
FROM         LoaderFilesArchive.dbo.DallasFTP_DataArchive AS arch INNER JOIN
                    DallasTrafficTickets.dbo.tblTicketsArchive AS T ON arch.[Ticket Number] = T.TicketNumber -- By Ticket Number
Where arch.GroupID = @GroupID

Update arch
Set arch.Insert_Mode = 4, arch.RecordID = TV.RecordID
FROM         LoaderFilesArchive.dbo.DallasFTP_DataArchive AS arch INNER JOIN
                    DallasTrafficTickets.dbo.tblTicketsViolationsArchive TV ON arch.[Cause Number] = TV.CauseNumber -- By Cause Number
					and isnull(TV.RecordID, 0) <> 0
Where arch.GroupID = @GroupID

-- Identifying Existing Non-Updateable Cases
Update arch
Set arch.Insert_Mode = 3
FROM         LoaderFilesArchive.dbo.DallasFTP_DataArchive AS arch
                      Inner JOIN DallasTrafficTickets.dbo.tblTicketsViolationsArchive AS TV 
					  ON arch.[Cause Number] = isnull(TV.CauseNumber, '') 
					  and datediff(day, convert(datetime,arch.[Court Date]), isnull(TV.CourtDate, '1/1/1900')) = 0 
					  and datediff(hh, convert(datetime, arch.[Court Date] + arch.[Court Time]), isnull(TV.CourtDate, '1/1/1900')) = 0
					  and (case arch.[Court Status] when 'J' then 26 when 'T' then 103 else 0 end) = isnull(TV.ViolationStatusID, 0)
Where arch.GroupID = @GroupID

-- Identifying Existing Updateable Cases
Update arch
Set arch.Insert_Mode = 2, arch.RecordID = TV.RecordID, arch.RowID = TV.RowID
FROM         LoaderFilesArchive.dbo.DallasFTP_DataArchive AS arch
                      INNER JOIN DallasTrafficTickets.dbo.tblTicketsViolationsArchive AS TV 
					  ON arch.[Cause Number] = isnull(TV.CauseNumber, '') 
Where isnull(arch.Insert_Mode,0) <> 3 and arch.GroupID = @GroupID

-- Identifying Past Court Date Cases
Update arch
Set arch.Insert_Mode = 5
FROM         LoaderFilesArchive.dbo.DallasFTP_DataArchive AS arch
Where datediff(day, Convert(datetime, arch.[Court Date]), getdate()) >= 1

-- Adil 5470 30/1/2009
-- Identifying Changed Attorney Cases
Update arch
Set arch.Insert_Mode = 6, arch.RecordID = TV.RecordID, arch.RowID = TV.RowID -- Adil 6283 04/08/2009 Fixing Attorney association issue.
FROM         LoaderFilesArchive.dbo.DallasFTP_DataArchive AS arch
                      Inner JOIN DallasTrafficTickets.dbo.tblTicketsViolationsArchive AS TV 
					  ON arch.[Cause Number] = isnull(TV.CauseNumber, '') 
					  inner JOIN  DallasTrafficTickets.dbo.Attorneys AT
					  ON isnull(TV.AttorneyID, 0) = AT.ID -- AttorneyID 0 means 'N/A'/'No Attorney'
					  and (replace(replace((isnull(AT.LastName, '') + isnull(AT.FirstName, '')),',',''),' ','') <> replace(replace(isnull(Arch.Attorney, ''), ',', ''), ' ', ''))
					  and ltrim(rtrim(replace(isnull(Arch.Attorney, ''), ',', ''))) <> ''
Where arch.GroupID = @GroupID

-- Adil 5470 30/1/2009 Insert_Mode 6 added in list
-- Identifying Insert-able Cases & Defendants
Update LoaderFilesArchive.dbo.DallasFTP_DataArchive Set Insert_Mode = 1 
Where isnull(Insert_Mode,0) not in (2, 3, 4, 5, 6) and GroupID = @GroupID


-- All Records After Identification
Select * From LoaderFilesArchive.dbo.DallasFTP_DataArchive Where GroupID = @GroupID and isnull(Insert_Mode,0) not in (3, 5)

-- grant execute on dbo.USP_HTS_LA_FTP_IdentifyCases to dbr_webuser





 
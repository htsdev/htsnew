﻿USE LA_Houston
GO 

/*
Author: Farrukh Iftikhar
TaskID:	10595
Created Date: 12/27/2012
Business Logic : Use to insert records for Austin Munisipal court house through loader service.
It performs following activities.
1. Check violation in Violation's referential table if not exists then insert new violation.
2. Check defendant's profile in non-client if not exists then insert new defendant.
3. Check defendant's violations if not exists then insert new violations against defendant.

Parameters: 
	@Record_Date: Case record/insert date
	@List_Date: Court list date
	@Ticket_Number:Cause OR Violation number
	@Last_Name: Defendant's last name
	@First_Name: Defendant's first name
	@Middle_Name: Defendant's middle name
	@PhoneNumber: Defendant's phone number	
	@Address: Defendant's home address
	@Address2: Defendant's home address II
	@City: Defendant's home city
	@State: Defendant's home state
	@ZIP: Defendant's home zip code
	@DP2: Defendant's barcode part. return by zp4 webservice
	@DPC: Defendant's barcode part. return by zp4 webservice
	@Violation_Description: Violation's title of Defendant
	@violationcode : Violation COde
	@violation Date : Violation Date
	@Court_Date: Defendant's court date	
	@Court_RoomNo: Defendant's court room number	
	@Officer_FirstName: Office first name
	@Officer_LastName: Office last name
	@FineAmount: Violation fine amount
	@GroupID: Data file ID
	@AddressStatus: Address status [y/d/s/n] of defendant
	@Flag_SameAsPrevious: insure wether this case belongs to previous one or not [It helps to cut-off researching the defendant profile]
	@PreviousRecordID: If this case belongs to previous case then previous case ID will be the value of @PreviousRecordID
	@LoaderID: Insure which loader is executing this case.
	@CurrentRecordID: It returns current Case-ID/Record-ID to calling program
	@Flag_Inserted: It returns status wether the record inserted or deleted
*/

CREATE PROCEDURE [dbo].[USP_HTS_LA_Insert_Tickets_AustinMunicipal]
	@Record_Date DATETIME,
	@List_Date DATETIME,
	@Ticket_Number VARCHAR(20),
	@Last_Name VARCHAR(50), 
	@First_Name VARCHAR(50), 
	@Middle_Name VARCHAR(50),
	@PhoneNumber VARCHAR(50), 
	@Address VARCHAR(50), 
	@Address2 VARCHAR(50), 
	@City VARCHAR(35), 
	@State VARCHAR(10),
	@ZIP VARCHAR(10), 
	@DP2 VARCHAR(2),
	@DPC VARCHAR(1),
	@Violation_Description VARCHAR(2000),
	@Violation_Location VARCHAR(200),
	@Violation_Code VARCHAR(20),
	@Violation_Date SMALLDATETIME,
	@Court_Date DATETIME,
	@Court_RoomNo VARCHAR(10),
	@Officer_FirstName VARCHAR(50),
	@Officer_LastName VARCHAR(50),	
	@FineAmount MONEY,
	@WarrantType VARCHAR(200),
	@GroupID INT,
	@AddressStatus CHAR(1),
	@Flag_SameAsPrevious BIT,
	@PreviousRecordID INT,
	@LoaderID INT,
	@Status VARCHAR(150),
	@StatusShortDesc VARCHAR(20),
	@CurrentRecordID INT OUTPUT,
	@Flag_Inserted INT OUTPUT
	AS
	SET NOCOUNT ON  
	
	DECLARE @StateID AS INT
	DECLARE @ViolationID AS INT
	DECLARE @IDFound AS INT
	DECLARE @TicketNum2 AS VARCHAR(20)
	DECLARE @BondAmount AS MONEY
	DECLARE @IsQuoted AS INT
	DECLARE @OfficerIDFound INT
	DECLARE @OfficerCode VARCHAR(20)
	DECLARE @ViolationStatusID INT
	DECLARE @CourtId INT
	
	SET @IsQuoted = 0
	SET @First_Name = LEFT(@First_Name, 20)
	SET @Last_Name = LEFT(@Last_Name, 20)
	SET @Middle_Name = LEFT(@Middle_Name, 1)
	SET @Address = REPLACE(@Address, '  ', ' ')
	SET @Violation_Code = 'XD' + @Violation_Code
	SET @Flag_Inserted = 0
	SET @ViolationStatusID = 116
	SET @CourtId = 3140
	
	SELECT DATEPART(dw, @Court_Date) 
	
	IF DATEPART(dw, @Court_Date) = 7 -- If Saturday then make it Friday (Court date should be in working days)
	BEGIN
	    SET @Court_Date = @Court_Date - 1
	END
	ELSE 
	IF DATEPART(dw, @Court_Date) = 1 -- If Sunday then make it Friday (Court date should be in working days)
	BEGIN
	    SET @Court_Date = @Court_Date - 2
	END
	
	IF @ZIP = '00000'
	BEGIN
	    SET @ZIP = ''
	END 
	
	
	IF LEN(@State) < 1
	BEGIN
	    SET @State = 'TX'
	END 
	
	IF ISNULL(@Officer_FirstName, '') = ''
	BEGIN
	    SET @Officer_FirstName = 'N/A'
	END 
	
	IF ISNULL(@Officer_LastName, '') = ''
	BEGIN
	    SET @Officer_LastName = 'N/A'
	END
	
	
	
	
	IF @Status = 'Appearance'
	BEGIN
	    SET @ViolationStatusID = 116 -- APPEARANCE
	END
	ELSE IF @Status = 'Dispose'
	BEGIN
	    SET @ViolationStatusID = 80 -- Disposed
	END
	ELSE IF @Status IN ('Bench Trial','Judge (Bench) Trial')
	BEGIN
	    SET @ViolationStatusID = 103 -- Judge (Bench) Trial
	END
	ELSE
	BEGIN		
		DECLARE @Court_Status_Found INT
		SET @Court_Status_Found = NULL
		
		SELECT @Court_Status_Found = CourtViolationStatusID FROM DallasTrafficTickets.dbo.tblCourtViolationStatus WITH(NOLOCK)
		WHERE [Description] = @Status
		
		DECLARE @Category_ID INT
		SET @Category_ID = 52
		
		IF @Status IN ('Warrant','Missed Court','FTA','DLQ')
		BEGIN
			SET @Category_ID = 54 -- MISSED COURT		
		END

		IF @Court_Status_Found IS NULL
		BEGIN						
			INSERT INTO DallasTrafficTickets.dbo.tblCourtViolationStatus ([Description],violationcategory,ShortDescription,CategoryID)
			VALUES (@Status,0,@StatusShortDesc,@Category_ID)
			
			SET @Court_Status_Found = SCOPE_IDENTITY()
		END
		
	    SET @ViolationStatusID = @Court_Status_Found
	END
	
	SET @OfficerIDFound = NULL
	SELECT TOP 1 @OfficerIDFound = OfficerNumber_PK
	FROM   DallasTrafficTickets.dbo.tblOfficer WITH(NOLOCK)
	WHERE  (
	           ISNULL(FirstName, '') = @Officer_FirstName
	           AND ISNULL(LastName, '') = @Officer_LastName
	       )
	
	IF @OfficerIDFound IS NULL
	BEGIN
	    SET @OfficerCode = (
	            SELECT MAX(OfficerNumber_PK) + 1
	            FROM   DallasTrafficTickets.dbo.tblOfficer WITH(NOLOCK)
	        )
	    
	    INSERT INTO DallasTrafficTickets.dbo.tblOfficer
	      (
	        OfficerNumber_PK, OfficerType, FirstName, LastName, Comments
	      )
	    VALUES
	      (
	        @OfficerCode, 'N/A', @Officer_FirstName, @Officer_LastName, 'Inserted at ' + CONVERT(VARCHAR(25), GETDATE(), 101)
	      )
	END
	ELSE
	BEGIN
	    SET @OfficerCode = @OfficerIDFound
	END	
	
	
	SET @ViolationID = NULL
	
	SELECT TOP 1 @ViolationID = ViolationNumber_PK,
	       @BondAmount = ISNULL(BondAmount, 0)
	FROM   DallasTrafficTickets.dbo.TblViolations WITH(NOLOCK)
	WHERE  (
	           DESCRIPTION = @Violation_Description
	           AND ViolationCode = @Violation_Code
	           AND CourtLoc = @CourtId
	           AND ViolationType = 4
	       )
	
	IF @ViolationID IS NULL
	BEGIN
	    INSERT INTO DallasTrafficTickets.dbo.TblViolations
	      (
	        DESCRIPTION, Sequenceorder, IndexKey, IndexKeys, ChargeAmount, BondAmount, Violationtype, ChargeonYesflag, ShortDesc, 
	        ViolationCode, AdditionalPrice, Attorneyregularprice, Attorneybondprice, CourtLoc
	      )
	    VALUES
	      (
	        @Violation_Description, NULL, 2, 160, @FineAmount, @FineAmount, 4,
			1, 'None', @Violation_Code, 0, 50, 95, @CourtId
	      )
	    SET @ViolationID = SCOPE_IDENTITY()
	END
	
	SET @StateID = 0
	SET @StateID = (
	        SELECT TOP 1 StateID
	        FROM   DallasTrafficTickets.dbo.TblState WITH(NOLOCK)
	        WHERE  STATE = @State
	    )
	
	IF @Flag_SameAsPrevious = 0
	BEGIN
	    SET @IDFound = NULL
	    
	    SELECT TOP 1 @IDFound = T.RecordID,
	           @TicketNum2 = TicketNumber,
	           @IsQuoted = T.Clientflag
	    FROM   DallasTrafficTickets.dbo.tblTicketsArchive T WITH(NOLOCK)
	           INNER JOIN DallasTrafficTickets.dbo.tblTicketsViolationsArchive TV WITH(NOLOCK)
	                ON  T.RecordID = TV.RecordID
	    WHERE  (
	               (TicketNumber = @Ticket_Number)
	               OR (
	                      FirstName = @First_Name
	                      AND LastName = @Last_Name
	                      AND Address1 = @Address
	                      AND ZipCode = @Zip
	                      AND DATEDIFF(DAY, ViolationDate, @Violation_Date) = 0
	                  )
	           )
	           AND TV.CourtLocation = @CourtId
	           --AND TV.violationstatusid <> 80	
	    
	    IF @IDFound IS NULL
	    BEGIN
	        IF LEN(@Ticket_Number) < 5
	        BEGIN
	            INSERT INTO DallasTrafficTickets.dbo.tblIDGenerator (recdate) VALUES (GETDATE())
	            SELECT @Ticket_Number = 'ID' + CONVERT(VARCHAR(12), @@identity)
	        END
	        
	        INSERT INTO DallasTrafficTickets.dbo.tblTicketsArchive
	          (
	            RecLoadDate, ListDate, CourtDate, ViolationDate, TicketNumber, MidNumber, FirstName, LastName, PhoneNumber, Address1, Address2, City,
				StateID_FK, ZipCode, CourtID, OfficerNumber_FK, GroupID, Flag1, DP2, DPC, InsertionLoaderID
	          )
	        VALUES
	          (
	            @Record_Date, @List_Date, @Court_Date, @Violation_Date, @Ticket_Number, @Ticket_Number, @First_Name, @Last_Name, @PhoneNumber, @Address,
				@Address2, @City, @StateID, @ZIP, @CourtId, 962528, @GroupID, @AddressStatus, @DP2, @DPC, @LoaderID
	          )
	        
	        SET @CurrentRecordID = SCOPE_IDENTITY()
	    END
	    ELSE
	    BEGIN
	        SET @CurrentRecordID = @IDFound
	        IF LEN(@Ticket_Number) < 1
	        BEGIN
	            SET @Ticket_Number = @TicketNum2
	        END
	    END
	END
	ELSE
	BEGIN
	    SET @CurrentRecordID = @PreviousRecordID
	END
	
	SET @IDFound = NULL
	SET @IDFound = (
	        SELECT TOP 1 ISNULL(RowID, 0)
	        FROM   DallasTrafficTickets.dbo.tblTicketsViolationsArchive WITH(NOLOCK)
	        WHERE RecordID = @CurrentRecordID AND ViolationNumber_PK = @ViolationID
	               AND CourtLocation = @CourtId
	    ) 
	
	
	IF @IDFound IS NULL
	BEGIN
	    IF @IsQuoted = 1
	    BEGIN
	        IF LEN(@Ticket_Number) < 5
	        BEGIN
	            INSERT INTO DallasTrafficTickets.dbo.tblIDGenerator (recdate) VALUES(GETDATE())
	            SELECT @Ticket_Number = 'ID' + CONVERT(VARCHAR(12), @@identity)
	        END
	        
	        INSERT INTO DallasTrafficTickets.dbo.tblTicketsArchive
	          (
	            RecLoadDate, ListDate, CourtDate, ViolationDate, TicketNumber, MidNumber, FirstName, LastName, PhoneNumber, Address1,
				Address2, City, StateID_FK, ZipCode, CourtID, OfficerNumber_FK, GroupID, Flag1, DP2, DPC, InsertionLoaderID
	          )
	        VALUES
	          (
	            @Record_Date, @List_Date, @Court_Date, @Violation_Date, @Ticket_Number, @Ticket_Number, @First_Name, @Last_Name, @PhoneNumber, @Address,
				@Address2, @City, @StateID, @ZIP, @CourtId, 962528, @GroupID, @AddressStatus, @DP2, @DPC, @LoaderID
	          )
	        
	        SET @CurrentRecordID = SCOPE_IDENTITY()
	    END
	    
	    INSERT INTO DallasTrafficTickets.dbo.tblTicketsViolationsArchive
	      (
	        TicketNumber_PK, ViolationNumber_PK, CauseNumber, CourtDate, Courtnumber, UpdatedDate, ViolationDescription, ViolationCode, FineAmount, BondAmount, 
	        CourtLocation, RecordID, ViolationStatusID, LoaderUpdateDate, InsertionLoaderId, GroupID, WarrantType
	      )
	    VALUES
	      (
	        @Ticket_Number, @ViolationID, @Ticket_Number, @Court_Date, @Court_RoomNo, GETDATE(), @Violation_Description,
			@Violation_Code, @FineAmount, @FineAmount, @CourtId, @CurrentRecordID, @ViolationStatusID, GETDATE(), @LoaderID, @GroupID, @WarrantType
	      )
	    SET @Flag_Inserted = 1	    
	END
	ELSE
	BEGIN
	    DECLARE @isExistViolation AS INT 
	    SELECT TOP 1 @isExistViolation = RowID,@IDFound = RecordID
	            FROM   DallasTrafficTickets.dbo.tblTicketsViolationsArchive WITH(NOLOCK)
	            WHERE  TicketNumber_PK = @Ticket_Number
	                   AND ViolationDescription = @Violation_Description
	        
	    
	    IF (@isExistViolation IS NOT NULL) --violation exist
	    BEGIN
	        --Profile Update
	        UPDATE DallasTrafficTickets.dbo.tblTicketsArchive
	        SET    UpdationLoaderID = @LoaderID, GroupID = @GroupID
	        WHERE RecordID = @IDFound
	        
	        --Violation Update
	        UPDATE DallasTrafficTickets.dbo.tblTicketsViolationsArchive
	        SET    LoaderUpdateDate = GETDATE(), UpdationLoaderId = @LoaderID, GroupID = @GroupID, CourtDate = @Court_Date, Courtnumber = @Court_RoomNo, violationstatusid = @ViolationStatusID, WarrantType = @WarrantType
	        WHERE RowID = @isExistViolation       
	    END
	END



GO
GRANT EXECUTE ON [dbo].[USP_HTS_LA_Insert_Tickets_AustinMunicipal] TO dbr_webuser

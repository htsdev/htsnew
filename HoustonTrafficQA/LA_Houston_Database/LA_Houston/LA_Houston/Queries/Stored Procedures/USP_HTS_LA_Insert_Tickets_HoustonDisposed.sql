﻿/*
Created By    : Fahad Muhammad Qureshi
Type          : Loader Service
TaskID		  : 6068
Created date  : 07/14/2009
Business Logic: This Procedure Update the data OR records as Disposed Cases on appropirate tables Client/Non-Client in the system on the basis of following described Parameters.
Parameters: 
	@CauseNumber    :Cause no of the Defendant
	@Ticketnumber	:Ticket no of the Defendant
	@Status			:This No show the batch identity of the Defendant
	@ClosedDate		:Date when case goes to Disposed.
	@LoaderID		:Id by which we can identify the Loader
	@GroupID		:File id by which we extract Data
	@IsClientUpdated:To check that Upadating in Client or Non-Client Records
*/

CREATE PROCEDURE [dbo].[USP_HTS_LA_Insert_Tickets_HoustonDisposed]
			@CauseNumber NVARCHAR(50),
	        @TicketNumber NVARCHAR(50),
	        @Status NVARCHAR(50),
	        @ClosedDate SMALLDATETIME,
			@LoaderID as int,
			@GroupID AS INT, 
			@IsClientUpdated INT OUTPUT,
			@IsNonClientUpdated INT OUTPUT
	AS
	
	DECLARE @IDFound INT
	SET @IsClientUpdated=0
	SET @IDFound=NULL
	-----------------------------------------------------------------------------------------------------------------------
	-- UPDATE STATUS, COURTDATE, COURT NUMBER FOR EXISTING REGULAR TICKETS IN NON CLIENTS
	-----------------------------------------------------------------------------------------------------------------------  
	SELECT TOP 1 @IDFound = ttva.RowID FROM  TrafficTickets.dbo.tblticketsviolationsarchive ttva
    WHERE (ttva.CauseNumber = @CauseNumber OR ttva.TicketNumber_PK = ISNULL(@TicketNumber,@CauseNumber) OR ttva.TicketNumber_PK + CONVERT(VARCHAR(2),ttva.ViolationNumber_PK) = @TicketNumber)
    AND ttva.violationstatusid <> 80
    AND ttva.CourtLocation IN (3001, 3002, 3003)
	
	If @IDFound is NOT NULL
	BEGIN
	UPDATE TrafficTickets.dbo.tblticketsviolationsarchive
	SET    TrafficTickets.dbo.tblticketsviolationsarchive.violationstatusid = 80,
	       TrafficTickets.dbo.tblticketsviolationsarchive.courtdate = @ClosedDate,
	       TrafficTickets.dbo.tblticketsviolationsarchive.updateddate = @ClosedDate,
	       TrafficTickets.dbo.tblticketsviolationsarchive.STATUS = @Status,
	       TrafficTickets.dbo.tblticketsviolationsarchive.GroupID = @GroupID
	WHERE RowID = @IDFound
	SET @IsNonClientUpdated = 2
	END
	
	-----------------------------------------------------------------------------------------------------------------------
	-- UPDATE STATUS, COURTDATE, COURT NUMBER FOR EXISTING TICKETS IN QUOTE/CLIENT
	------------------------------------------------------------------------------------------------------------------------    
	SET @IDFound=NULL 
	SELECT TOP 1 @IDFound = ttv.TicketsViolationID FROM TrafficTickets.dbo.tblticketsviolations ttv
	WHERE (ttv.casenumassignedbycourt = @CauseNumber OR ttv.RefCaseNumber = @TicketNumber OR @TicketNumber = ttv.RefCaseNumber + CONVERT(VARCHAR(2), ttv.SequenceNumber))
			AND ttv.CourtViolationStatusIDmain <> 80
			AND ttv.CourtID IN (3001, 3002, 3003)
	If @IDFound IS NOT NULL
	BEGIN
	UPDATE TrafficTickets.dbo.tblticketsviolations
	SET    TrafficTickets.dbo.tblticketsviolations.CourtViolationStatusIDmain = 80,
	       TrafficTickets.dbo.tblticketsviolations.courtdate = @ClosedDate,
	       TrafficTickets.dbo.tblticketsviolations.updateddate = @ClosedDate,
	       TrafficTickets.dbo.tblticketsviolations.vemployeeid = 3992,
	       TrafficTickets.dbo.tblticketsviolations.GroupID = @GroupID
	WHERE  TrafficTickets.dbo.tblticketsviolations.TicketsViolationID = @IDFound
	SET @IsClientUpdated = 2
	END
GO
GRANT EXEC ON [dbo].[USP_HTS_LA_Insert_Tickets_HoustonDisposed] TO dbr_webuser
GO  
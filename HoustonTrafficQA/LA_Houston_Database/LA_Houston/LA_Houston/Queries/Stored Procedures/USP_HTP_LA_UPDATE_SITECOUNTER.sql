﻿ /*
* Created By : Waqas Javed
* Task ID  : 6791/7023
* Created Date : 11/19/2009
* 
* Business Logic : This procedure is used to update last counter of the site.
* 
* Parameters : 
* @LoaderID		: It represents the loader id 
* @COUTNER		: It represents the counter to be update for the site.
* @URL_ID		: It represents the ID of the targeted site.
*/
 
 alter procedure dbo.USP_HTP_LA_UPDATE_SITECOUNTER
@LoaderID INT,
@COUNTER AS VARCHAR(50),
@URL_ID as int

as

begin

UPDATE LoaderWebURL 
		SET [Counter] = @COUNTER
		WHERE LoaderID = @LoaderID
	AND ID = @URL_ID

END

GO
GRANT EXECUTE ON dbo.USP_HTP_LA_UPDATE_SITECOUNTER TO dbr_webuser
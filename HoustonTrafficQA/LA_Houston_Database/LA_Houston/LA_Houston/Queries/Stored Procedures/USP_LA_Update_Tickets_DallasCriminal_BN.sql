﻿set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go










/*
Author: Muhammad Adil Aleem
Description: To Upload DCCC BN Cases From Loader Service
Type : Loader Service
Created date : Oct-23rd-2009

Parameters: 
	@Record_Date: Case record/insert date
	@List_Date: Court list date
	@Bond_Date: Court Date
	@Name_First: Defendant's first name
	@Name_Last: Defendant's last name
	@Name_Middle: Defendant's Middle name
	@Race: Defendant's Race
	@Gender: Defendant's Gender/Sex
	@DOB: Defendant's Date Of Birth
	@Address: Defendant's home address
	@City: Defendant's home city
	@State: Defendant's home state
	@ZIP: Defendant's home zip code
	@DP2: Defendant's barcode part. return by zp4 webservice
	@DPC: Defendant's barcode part. return by zp4 webservice
	@Violation_Description: offense description
	@Setting_Status: Setting Status
	@Location: Location
	@Comments: Comments
	@GroupID: Data file ID
	@AddressStatus: Address status [y/d/s/n] of defendant
	@Flag_SameAsPrevious: insure wether this case belongs to previous one or not [It helps to cut-off researching the defendant profile]
	@PreviousRecordID: If this case belongs to previous case then previous case ID will be the value of @PreviousRecordID
	@LoaderID: Insure which loader is executing this case.
	@CurrentRecordID: It returns current Case-ID/Record-ID to calling program
	@Flag_Inserted: It returns status wether the record inserted or deleted
*/
ALTER PROCEDURE [dbo].[USP_LA_Update_Tickets_DallasCriminal_BN]
@Record_Date datetime,  
@List_Date as smalldatetime,
@Bond_Date as smalldatetime,
@Name_First as varchar(50),  
@Name_Last as varchar(50),  
@Name_Middle as varchar(50), -- Abbas Qamar 9266 05/13/2011 Add the middle Name parameter
@DOB as datetime,
@Race AS VARCHAR(5),
@Gender AS VARCHAR(10),
@Address as varchar(50),  
@City as varchar(35),  
@State as Varchar(10),
@ZIP as varchar(10),  
@DP2 varchar(2),
@DPC varchar(1),
@Violation_Description as Varchar(2000),
@GroupID as Int,
@AddressStatus VARCHAR(5),
@Flag_SameAsPrevious as bit,
@PreviousRecordID as int,
@LoaderID as int,
@CurrentRecordID int output,
@Flag_Inserted int Output
AS
set nocount on  

Declare @StateID as Int
Declare @ViolationID as int
Declare @IDFound as int
Declare @TicketNum2 as varchar(20)
Declare @ChargeAmount as money
Declare @BondAmount as money
Declare @Violation_Code as varchar(30)
Declare @Court_Date as smalldatetime
DECLARE @Ticket_Number AS VARCHAR(20)

Set @Name_First = left(@Name_First,20)
Set @Name_Last = left(@Name_Last,20)
Set @Name_Middle = left(@Name_Middle,1)   -- Abbas Qamar 9266 05/13/2011 Add the middle Name parameter
Set @Address = Replace(@Address, '  ', ' ')
Set @Violation_Code = 'XD-none'
Set @Flag_Inserted = 0
Set @ChargeAmount = 0
Set @BondAmount = 0

if left(@Gender,1) = 'M'
	begin
		Set @Gender = 'Male'
	end
else if left(@Gender,1) = 'F'
	begin
		Set @Gender = 'Female'
	end

Select @Race = case @Race 
when 'A' then 'Asian'
when 'B' then 'Black'
when 'H' then 'Hispanic'
when 'I' then 'Indian'
when 'M' then 'Mexican'
when 'O' then 'Other'
when 'U' then 'Unknown'
when 'W' then 'White'
when 'X' then 'Unknown'
else null
END

if len(isnull(@DP2,'')) = 1
BEGIN
	if isnull(@DP2,'') <> '0'
	begin
		Set @DP2 = '0' + @DP2
	end
END

Set @Court_Date = convert(datetime,convert(varchar(12),@Bond_Date,110) + ' 08:00 AM')

if datepart(dw,@Court_Date) = 7 -- If Saturday then make it Friday (Court date should be in working days)
	begin
		set @Court_Date = @Court_Date - 1
	end
else if datepart(dw,@Court_Date) = 1 -- If Sunday then make it Friday (Court date should be in working days)
	begin
		set @Court_Date = @Court_Date - 2
	end

Set @ViolationID = null
Select Top 1 @ViolationID = ViolationNumber_PK, @Violation_Code = ViolationCode, @ChargeAmount = isnull(ChargeAmount,100), @BondAmount = isnull(BondAmount,200) From DallasTrafficTickets.dbo.TblViolations Where (Description = @Violation_Description and CourtLoc = 3110 and ViolationType = 8)

If @ViolationID is null
	Begin
		Insert Into DallasTrafficTickets.dbo.TblViolations (Description, ViolationCode, ViolationType, CourtLoc, ChargeAmount, BondAmount) Values(@Violation_Description, @Violation_Code, 8, 3110, 100, 200)
		Set @ViolationID = scope_identity()
	End

Set @StateID = (Select Top 1 StateID From DallasTrafficTickets.dbo.TblState Where State = @State)
IF ISNULL(@StateID, 0) = 0
	BEGIN
		SET @StateID = 45
	END

If @Flag_SameAsPrevious = 0
	BEGIN
		Set @IDFound = null
		
		SELECT Top 1 T.RecordID, TV.RowID INTO #TempIDs
		From DallasTrafficTickets.dbo.tblTicketsArchive T
		Inner Join DallasTrafficTickets.dbo.tblTicketsViolationsArchive TV
		On T.RecordID = TV.RecordID
		Where (t.FirstName = @Name_First AND t.LastName = @Name_Last AND DATEDIFF(DAY,t.DOB, @DOB) = 0 -- Adil 7169 12/21/2009 Where clause modified.
		AND LEFT(ISNULL(t.ZipCode, @ZIP), 5) = LEFT(@ZIP, 5)
		AND LEFT(ISNULL(tv.ViolationDescription, @Violation_Description), 10) = LEFT(@Violation_Description, 10)
		AND tv.CourtLocation IN (3110, 3123))
		AND TV.violationstatusid <> 80	-- Adil 7001 11/14/2009 Do not associate new cases with client, quote and disposed cases
		-- AND t.Clientflag = 0 -- Adil 7380 02/05/2010 code commented.
	    
		If NOT EXISTS (SELECT * FROM #TempIDs)
			BEGIN
				insert into DallasTrafficTickets.dbo.tblIDGenerator(recdate) values(getdate())
				select @Ticket_Number='DCBN' + convert(varchar(12),@@identity)

				Insert Into DallasTrafficTickets.dbo.tblTicketsArchive (RecLoadDate, ListDate, Bondflag, TicketNumber, MidNumber, FirstName, LastName, initial,DOB, Race, Gender, PhoneNumber, Address1, City, StateID_FK, ZipCode, CourtID, OfficerNumber_FK, OfficerName, GroupID, Flag1, DP2, DPC, InsertionLoaderID)
				-- Abbas Qamar 9266 05/13/2011 Add the middle Name parameter
				Values(@Record_Date, @List_Date, 1, @Ticket_Number, @Ticket_Number, @Name_First, @Name_Last,@Name_Middle, @DOB, @Race, @Gender, '0000000000', @Address, @City, @StateID, @ZIP, 3110, 962528, 'N/A', @GroupID, @AddressStatus, @DP2, @DPC, @LoaderID) 

				Set @CurrentRecordID = scope_identity()
				
				Insert Into DallasTrafficTickets.dbo.tblTicketsViolationsArchive (TicketNumber_PK, ViolationNumber_PK, CauseNumber, CourtDate, InsertDate, UpdatedDate, ViolationBondDate, TicketViolationDate, ViolationDescription, ViolationCode, FineAmount, BondAmount, CourtLocation, RecordID, ViolationStatusID, LoaderUpdateDate, InsertionLoaderId, GroupID)
				Values(@Ticket_Number, @ViolationID, @Ticket_Number, @Court_Date, Getdate(), Getdate(), @Bond_Date, @Bond_Date, @Violation_Description, @Violation_Code, DallasTrafficTickets.dbo.GetDallasViolationFineamount(@Violation_Description, @ChargeAmount), @BondAmount, 3110, @CurrentRecordID, 116, getdate(), @LoaderID, @GroupID)
				
				Set @Flag_Inserted = 1
			END
		Else
			BEGIN
				Set @CurrentRecordID = @IDFound
				UPDATE DallasTrafficTickets.dbo.tblTicketsArchive
				SET Bondflag = 1, GroupID = @GroupID, UpdationLoaderID = @LoaderID
				WHERE RecordID IN (SELECT RecordID FROM #TempIDs)
				
				UPDATE DallasTrafficTickets.dbo.tblTicketsViolationsArchive 
				SET ViolationBondDate = @Bond_Date, UpdationLoaderId = @LoaderID, GroupID = @GroupID, LoaderUpdateDate = GETDATE()
				WHERE RowID IN (SELECT RowID FROM #TempIDs)
				
				Set @Flag_Inserted = 2
			End
	End
Else
	Begin
		Set @CurrentRecordID = @PreviousRecordID
	End





﻿


/*
Author: Muhammad Adil Aleem
Description: To Upload DCCC CBF Cases From Loader Service
Type : Loader Service
Created date : Oct-23rd-2009
Parameters: 
	@Record_Date: Case record/insert date
	@List_Date: Court list date
	@Court_Date: Court Date
	@Ticket_Number: Ticket Number
	@Name_First: Defendant's first name
	@Name_Last: Defendant's last name
	@Race: Defendant's Race
	@Gender: Defendant's Gender/Sex
	@DOB: Defendant's Date Of Birth
	@Address: Defendant's home address
	@City: Defendant's home city
	@State: Defendant's home state
	@ZIP: Defendant's home zip code
	@DP2: Defendant's barcode part. return by zp4 webservice
	@DPC: Defendant's barcode part. return by zp4 webservice
	@Violation_Description: offense description
	@Setting_Status: Setting Status
	@Location: Location
	@Comments: Comments
	@GroupID: Data file ID
	@AddressStatus: Address status [y/d/s/n] of defendant
	@Flag_SameAsPrevious: insure wether this case belongs to previous one or not [It helps to cut-off researching the defendant profile]
	@PreviousRecordID: If this case belongs to previous case then previous case ID will be the value of @PreviousRecordID
	@LoaderID: Insure which loader is executing this case.
	@CurrentRecordID: It returns current Case-ID/Record-ID to calling program
	@Flag_Inserted: It returns status wether the record inserted or deleted
*/
ALTER PROCEDURE [dbo].[USP_LA_Insert_Tickets_DallasCriminal_CBF]
@Record_Date datetime,  
@List_Date as smalldatetime,
@Court_Date as smalldatetime,
@Ticket_Number as varchar(20),
@Name_First as varchar(50),  
@Name_Last as varchar(50),  
@Race as varchar(10),
@Gender as varchar(10),
@DOB as datetime,
@Address as varchar(50),  
@City as varchar(35),  
@State as Varchar(10),
@ZIP as varchar(10),  
@DP2 varchar(2),
@DPC varchar(1),
@Violation_Description as Varchar(2000),
@Setting_Status varchar(20),
@Location varchar(20),
@Comments varchar(40),
@GroupID as Int,
@AddressStatus VARCHAR(5),
@Flag_SameAsPrevious as bit,
@PreviousRecordID as int,
@LoaderID as int,
@CurrentRecordID int output,
@Flag_Inserted int Output
AS
set nocount on  

Declare @StateID as Int
Declare @ViolationID as int
Declare @IDFound as int
Declare @TicketNum2 as varchar(20)
Declare @ChargeAmount as money
Declare @BondAmount as money
Declare @Violation_Code as varchar(30)
DECLARE @IsQuoted AS INT

SET @IsQuoted = 0
Set @Name_First = left(@Name_First,20)
Set @Name_Last = left(@Name_Last,20)
Set @Address = Replace(@Address, '  ', '')
Set @Violation_Code = 'XD-none'
Set @Flag_Inserted = 0
Set @ChargeAmount = 0
Set @BondAmount = 0

if len(isnull(@DP2,'')) = 1
begin
	if isnull(@DP2,'') <> '0'
	begin
		Set @DP2 = '0' + @DP2
	end
end

if datepart(dw,@Court_Date) = 7 -- If Saturday then make it Friday (Court date should be in working days)
	begin
		set @Court_Date = @Court_Date - 1
	end
else if datepart(dw,@Court_Date) = 1 -- If Sunday then make it Friday (Court date should be in working days)
	begin
		set @Court_Date = @Court_Date - 2
	end

if @Gender = 'M'
	begin
		Set @Gender = 'Male'
	end
else if @Gender = 'F'
	begin
		Set @Gender = 'Female'
	end

Select @Race = case @Race 
when 'A' then 'Asian'
when 'B' then 'Black'
when 'H' then 'Hispanic'
when 'I' then 'Indian'
when 'M' then 'Mexican'
when 'O' then 'Other'
when 'U' then 'Unknown'
when 'W' then 'White'
when 'X' then 'Unknown'
else null
end

Set @ViolationID = null
Select Top 1 @ViolationID = ViolationNumber_PK, @Violation_Code = ViolationCode, @ChargeAmount = isnull(ChargeAmount,100), @BondAmount = isnull(BondAmount,200) From DallasTrafficTickets.dbo.TblViolations Where (Description = @Violation_Description and CourtLoc = 3110 and ViolationType = 8)

If @ViolationID is null
	Begin
		Insert Into DallasTrafficTickets.dbo.TblViolations (Description, ViolationCode, ViolationType, CourtLoc, ChargeAmount, BondAmount) Values(@Violation_Description, @Violation_Code, 8, 3110, 100, 200)
		Set @ViolationID = scope_identity()
	End

Set @StateID = (Select Top 1 StateID From DallasTrafficTickets.dbo.TblState Where State = @State)

If @Flag_SameAsPrevious = 0
	BEGIN
		Set @IDFound = null

		SELECT Top 1 @IDFound = T.RecordID, @TicketNum2 = TicketNumber, @IsQuoted = T.Clientflag
		From DallasTrafficTickets.dbo.tblTicketsArchive T
		Inner Join DallasTrafficTickets.dbo.tblTicketsViolationsArchive TV
		On T.RecordID = TV.RecordID
		Where (TicketNumber = @Ticket_Number and TV.CourtLocation = 3123)-- Adil 7125 12/10/2009 3123 = Dallas County District Court
		AND TV.violationstatusid <> 80	-- Adil 7001 11/14/2009 Do not associate new cases with client, quote and disposed cases
	
		If @IDFound is null
			Begin
				if Len(@Ticket_Number)<2
					Begin
						insert into DallasTrafficTickets.dbo.tblIDGenerator(recdate) values(getdate())
						select @Ticket_Number='DC' + convert(varchar(12),@@identity)
					End

				Insert Into DallasTrafficTickets.dbo.tblTicketsArchive (RecLoadDate, ListDate, TicketNumber, MidNumber, FirstName, LastName, Race, Gender, DOB, PhoneNumber, Address1, City, StateID_FK, ZipCode, CourtID, OfficerNumber_FK, OfficerName, GroupID, Flag1, DP2, DPC, InsertionLoaderID)
				Values(@Record_Date, @List_Date, @Ticket_Number, @Ticket_Number, @Name_First, @Name_Last, @Race, @Gender, @DOB, '0000000000', @Address, @City, @StateID, @ZIP, 3123, 962528, 'N/A', @GroupID, @AddressStatus, @DP2, @DPC, @LoaderID)

				Set @CurrentRecordID = scope_identity()
			End
		Else
			Begin
				Set @CurrentRecordID = @IDFound
				if Len(@Ticket_Number)<1
					Begin
						Set @Ticket_Number = @TicketNum2
					End
			End
	End
Else
	Begin
		Set @CurrentRecordID = @PreviousRecordID
	End

Set @IDFound = null
Set @IDFound = (Select top 1 Isnull(RowID,0) From DallasTrafficTickets.dbo.tblTicketsViolationsArchive
			Where 
			(TicketNumber_PK = @Ticket_Number and ViolationNumber_PK = @ViolationID) and CourtLocation = 3123 ) --Afaq 7380 3/25/2010 remove check(And RecordID = @CurrentRecordID)

If @IDFound is null
	BEGIN
		IF @IsQuoted = 1 -- Adil 7380 02/05/2010 if current client is quoted client and coming violation is different from existing then insert new ticket.
			Begin
				if Len(@Ticket_Number)<2
					Begin
						insert into DallasTrafficTickets.dbo.tblIDGenerator(recdate) values(getdate())
						select @Ticket_Number='DC' + convert(varchar(12),@@identity)
					End

				Insert Into DallasTrafficTickets.dbo.tblTicketsArchive (RecLoadDate, ListDate, TicketNumber, MidNumber, FirstName, LastName, Race, Gender, DOB, PhoneNumber, Address1, City, StateID_FK, ZipCode, CourtID, OfficerNumber_FK, OfficerName, GroupID, Flag1, DP2, DPC, InsertionLoaderID)
				Values(@Record_Date, @List_Date, @Ticket_Number, @Ticket_Number, @Name_First, @Name_Last, @Race, @Gender, @DOB, '0000000000', @Address, @City, @StateID, @ZIP, 3123, 962528, 'N/A', @GroupID, @AddressStatus, @DP2, @DPC, @LoaderID)

				Set @CurrentRecordID = scope_identity()
			END
		Insert Into DallasTrafficTickets.dbo.tblTicketsViolationsArchive (TicketNumber_PK, ViolationNumber_PK, CauseNumber, CourtDate, InsertDate, UpdatedDate, ViolationDescription, ViolationCode, SettingStatus, Location, Comments, FineAmount, BondAmount, CourtLocation, RecordID, ViolationStatusID, LoaderUpdateDate, InsertionLoaderId, GroupID)
		Values(@Ticket_Number, @ViolationID, @Ticket_Number, @Court_Date, Getdate(), Getdate(), @Violation_Description, @Violation_Code, @Setting_Status, @Location, @Comments, DallasTrafficTickets.dbo.GetDallasViolationFineamount(@Violation_Description, @ChargeAmount), @BondAmount, 3123, @CurrentRecordID, 116, getdate(), @LoaderID, @GroupID)
		Set @Flag_Inserted = 1
	End
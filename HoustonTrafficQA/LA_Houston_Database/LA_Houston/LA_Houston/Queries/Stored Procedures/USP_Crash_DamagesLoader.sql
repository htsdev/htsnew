﻿USE [Crash]
GO
/****** Object:  StoredProcedure [dbo].[USP_Crash_DamagesLoader]    Script Date: 09/02/2013 12:22:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sabir Khan Miani
-- Create date: 07/30/2013 TFS 11309
-- Description:	This procedure is used to insert crash cases.
-- =============================================
ALTER PROCEDURE [dbo].[USP_Crash_DamagesLoader] 
@Crash_ID INT,
@Damaged_Property VARCHAR(40),
@GroupID INT,
@InsertionLoaderID INT,
@UpdationLoaderID INT
AS
BEGIN	
	SET NOCOUNT ON;
	if not exists(select d.Crash_ID from tbl_Crash c INNER JOIN tbl_Damages d ON d.Crash_ID = c.Crash_ID where d.Crash_ID=@Crash_ID AND d.Damaged_Property = @Damaged_Property)
    begin
		Insert into tbl_Damages(Crash_ID,Damaged_Property,GroupID,InsertionLoaderID,UpdationLoaderID)
		values(@Crash_ID,@Damaged_Property,@GroupID,@InsertionLoaderID,@UpdationLoaderID)
	end	
END

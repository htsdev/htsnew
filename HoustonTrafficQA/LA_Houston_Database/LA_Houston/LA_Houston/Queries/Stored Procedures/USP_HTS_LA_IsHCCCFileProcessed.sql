﻿USE LA_Houston
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/****** 
Create by		: Farrukh Iftikhar
Task ID			: 10438
Created Date	: 11/07/2012

Business Logic : 
			This procedure checks either specific loader file already processed today.			

			
EXEC [USP_HTS_LA_IsHCCCFileProcessed] 69,0 
******/


CREATE PROCEDURE [dbo].[USP_HTS_LA_IsHCCCFileProcessed]
	@LoaderId INT,
	@IsExist INT OUTPUT
AS
BEGIN
    SELECT @IsExist = COUNT(GP.GroupID)
    FROM   Tbl_HTS_LA_Group GP           
    WHERE  GP.LoaderID = @LoaderId
           AND DATEDIFF(DAY, GP.Load_Date, GETDATE()) = 0           
           AND ISNULL(GP.IsProcessed, 0) = 1 
    
    
    RETURN @IsExist
END

GO
GRANT EXECUTE ON [dbo].[USP_HTS_LA_IsHCCCFileProcessed] TO dbr_webuser
GO


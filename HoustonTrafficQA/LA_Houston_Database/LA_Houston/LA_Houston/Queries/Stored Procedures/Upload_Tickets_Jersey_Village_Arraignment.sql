﻿USE [LA_Houston]
GO
/****** Object:  StoredProcedure [dbo].[Upload_Tickets_Jersey_Village_Arraignment]    Script Date: 12/30/2011 05:17:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




/*
Author: Muhammad Adil
Description: To Upload Jersey Village Arraignment Cases From Loader Service
Type : Loader Service
Created date : 08/22/2011
Parameters: 
	@Record_Date: Case record/insert date
	@List_Date: Court list date
	@Name_Last: Defendant's last name
	@Name_First: Defendant's first name
	@Address: Defendant's home address
	@City: Defendant's home city
	@State: Defendant's home state
	@ZIP: Defendant's home zip code
	@DP2: Defendant's barcode part. return by zp4 webservice
	@DPC: Defendant's barcode part. return by zp4 webservice
	@Court_Date: Defendant's court date	
	@Violation_Description: Violation's title of Defendant
	@GroupID: Data file ID
	@AddressStatus: Address status [y/d/s/n] of defendant
	@Flag_SameAsPrevious: insure wether this case belongs to previous one or not [It helps to cut-off researching the defendant profile]
	@PreviousRecordID: If this case belongs to previous case then previous case ID will be the value of @PreviousRecordID
	@LoaderID: Insure which loader is executing this case.
	@CurrentRecordID: It returns current Case-ID/Record-ID to calling program
	@Flag_Inserted: It returns status wether the record inserted or deleted
*/

ALTER PROCEDURE [dbo].[Upload_Tickets_Jersey_Village_Arraignment]
@Record_Date datetime,	
@List_Date as smalldatetime,
@Ticket_Number as varchar(20),
@Name_First as varchar(50),
@Name_Last as varchar(50),
@Name_Middle as varchar(50),
@Address as varchar(50),
@Race as varchar(10), --Farrukh 9873 11/11/2011 Parameters added (Race,Sex,Officer First Name, Office Last Name,Violation Date & Violation Location)
@Sex as varchar(6),
@Officer_FirstName as varchar(50),
@Officer_LastName as varchar(50),
@City as varchar(35),
@State as Varchar(10),
@ZIP as varchar(10),
@DP2 varchar(2),
@DPC varchar(1),
@Violation_Description as Varchar(200),
@Violation_Location as Varchar(200),
@Violation_Date DATETIME,
@Court_Date DATETIME,
@GroupID as Int,
@AddressStatus VARCHAR(5),
@Flag_SameAsPrevious as bit,
@PreviousRecordID as int,
@LoaderID as int,
@DOB AS DATETIME, -- Rab Nawaz Khan 10465 10/04/2012 Added DOB Parameter. . . 
@CurrentRecordID int output,
@Flag_Inserted int Output
AS
set nocount on  

Declare @StateID as Int
Declare @ViolationID as INT
DECLARE @OfficerIDFound AS INT
DECLARE @OfficerCode AS VARCHAR(20)
Declare @IDFound as int
Declare @TicketNum2 as varchar(20)
Declare @ChargeAmount as money
Declare @BondAmount as money
Declare @Violation_Code as varchar(30)
DECLARE @IsQuoted AS INT
DECLARE @AddressFound AS VARCHAR(200)
Declare @AddressStatusFound as char(1)
--Farrukh 9927 01/18/2012 Fine amount issue fixed
DECLARE @Temp_Violation_Description AS VARCHAR(200)
DECLARE @Violation_Description30 VARCHAR(31)
--Sabir Khan 9927 03/22/2012 declare variable for fine amount.
DECLARE @Violation_Description25 VARCHAR(26)
DECLARE @Violation_Description20 VARCHAR(21)
--DECLARE @Violation_Description8 VARCHAR(9)

SET @IsQuoted = 0
SET @OfficerIDFound = 0
SET @OfficerCode = ''
Set @Name_First = left(@Name_First,20)
Set @Name_Last = left(@Name_Last,20)
Set @Name_Middle = left(@Name_Middle,1)
Set @Address = Replace(@Address, '  ', ' ')
Set @Violation_Code = 'XD-none'
Set @Flag_Inserted = 0
Set @ChargeAmount = 0
Set @BondAmount = 0

--Farrukh 9927 01/18/2012 Fine amount issue fixed
SET @Temp_Violation_Description = @Violation_Description
SET @Violation_Description30 = LEFT(@Temp_Violation_Description,30) + '%'
SET @Violation_Description25 = LEFT(@Temp_Violation_Description,25) + '%'
SET @Violation_Description20 = LEFT(@Temp_Violation_Description,20) + '%'
--SET @Violation_Description8 = LEFT(@Temp_Violation_Description,8) + '%'  


if len(isnull(@DP2,'')) = 1
begin
	if isnull(@DP2,'') <> '0'
	begin
		Set @DP2 = '0' + @DP2
	end
END

IF @Sex = 'M'
BEGIN
    SET @Sex = 'Male'
END
ELSE 
IF @Sex = 'F'
BEGIN
    SET @Sex = 'Female'
END

SELECT @Race = 
		CASE @Race
            WHEN 'A' THEN 'Asian'
            WHEN 'B' THEN 'Black'
            WHEN 'H' THEN 'Hispanic'
            WHEN 'I' THEN 'Indian'
            WHEN 'M' THEN 'Mexican'
            WHEN 'O' THEN 'Other'
            WHEN 'U' THEN 'Unknown'
            WHEN 'W' THEN 'White'
            WHEN 'X' THEN 'Unknown'
            ELSE NULL
		END

if isnull(@Officer_FirstName,'') + isnull(@Officer_LastName,'') = ''
	Begin
		Set @Officer_FirstName = 'N/A'
		Set @Officer_LastName = 'N/A'
	End

Set @OfficerIDFound = null
Select Top 1 @OfficerIDFound = OfficerNumber_PK from TrafficTickets.dbo.tblOfficer where (isnull(FirstName,'') = @Officer_FirstName AND isnull(LastName,'') = @Officer_LastName)
if @OfficerIDFound is null
	Begin
		Set @OfficerCode = (select max(OfficerNumber_PK) + 1 from TrafficTickets.dbo.tblofficer)
		Insert Into TrafficTickets.dbo.tblOfficer (OfficerNumber_PK, OfficerType, FirstName, LastName, Comments) 
		Values (@OfficerCode, 'N/A', @Officer_FirstName, @Officer_LastName, 'Inserted at ' + convert(varchar(25),getdate(),101))
	End
Else
	Begin
		Set @OfficerCode = @OfficerIDFound
	END
	
if datepart(dw,@Court_Date) = 7 -- If Saturday then make it Friday (Court date should be in working days)
	begin
		set @Court_Date = @Court_Date - 1
	end
else if datepart(dw,@Court_Date) = 1 -- If Sunday then make it Friday (Court date should be in working days)
	begin
		set @Court_Date = @Court_Date - 2
	end

Set @ViolationID = null

Select Top 1 @ViolationID = ViolationNumber_PK, @Violation_Code = ViolationCode,
--@ChargeAmount = isnull(ChargeAmount,0),  --Farrukh 9927 12/30/2011 Changes for ChargeAmount
@BondAmount = isnull(BondAmount,0)
From TrafficTickets.dbo.TblViolations Where (Description = @Violation_Description and CourtLoc = 3034 and ViolationType = 3)

--Farrukh 9927 01/18/2012 Pick Fine Amount from Fine Table
Select @ChargeAmount = (SELECT TOP 1 a.total FROM (

SELECT * FROM LA_Houston.dbo.Jersey_FineSchedule Where ((VDesc LIKE @Temp_Violation_Description))
UNION ALL
SELECT  * FROM LA_Houston.dbo.Jersey_FineSchedule Where ((VDesc LIKE @Violation_Description30))
UNION ALL
SELECT  * FROM LA_Houston.dbo.Jersey_FineSchedule Where ((VDesc LIKE @Violation_Description25))
UNION ALL
SELECT  * FROM LA_Houston.dbo.Jersey_FineSchedule Where ((VDesc LIKE @Violation_Description20))
--UNION ALL
--SELECT  * FROM LA_Houston.dbo.Jersey_FineSchedule Where ((VDesc LIKE @Violation_Description8))
) a) 
PRINT @ChargeAmount
IF @ChargeAmount IS NULL
BEGIN
	SET @ChargeAmount = 0
END

If @ViolationID is null
	BEGIN
		Insert Into TrafficTickets.dbo.TblViolations (Description, ViolationCode, ViolationType, CourtLoc, ChargeAmount, BondAmount) 
		Values(@Violation_Description, @Violation_Code, 3, 3034, @ChargeAmount, 0)
		Set @ViolationID = scope_identity()
	END
	
Set @StateID = (Select Top 1 StateID From TrafficTickets.dbo.TblState Where State = @State)

If @Flag_SameAsPrevious = 0
	BEGIN
		Set @IDFound = null

		SELECT TOP 1 @IDFound = T.RecordID, @TicketNum2 = ISNULL(TicketNumber, @Ticket_Number) , @IsQuoted = T.Clientflag, @AddressFound = ISNULL(T.Address1, 'N/A'), @AddressStatusFound = ISNULL(T.Flag1, 'N')
		FROM TrafficTickets.dbo.tblTicketsArchive T
		INNER JOIN TrafficTickets.dbo.tblTicketsViolationsArchive TV
		ON T.RecordID = TV.RecordID
		WHERE ((TV.TicketNumber_PK = @Ticket_Number )
		OR
		(FirstName = @Name_First and LastName = @Name_Last
		and datediff(day,TV.CourtDate, @Court_Date) = 0 )) and TV.CourtLocation = 3034

		If @IDFound IS NULL
			Begin
				if Len(@Ticket_Number)<2
					Begin
						insert into TrafficTickets.dbo.tblIDGenerator(recdate) values(getdate())
						select @Ticket_Number='JVMC' + convert(varchar(12),scope_identity())
					End

				Insert Into TrafficTickets.dbo.tblTicketsArchive (RecLoadDate, ListDate, TicketNumber, MidNumber, FirstName, LastName, Initial, Race, Gender, Address1, City, StateID_FK, ZipCode, CourtID, PhoneNumber, OfficerNumber_FK, OfficerName, GroupID, Flag1, DP2, DPC, InsertionLoaderID, DOB) -- Rab Nawaz Khan 10465 10/04/2012 Added DOB Column and value. . . 
				Values(@Record_Date, @List_Date, @Ticket_Number, @Ticket_Number, @Name_First, @Name_Last, @Name_Middle, @Race, @Sex, @Address, @City, @StateID, @ZIP, 3034, '0000000000', @OfficerIDFound, @Officer_LastName + ', ' + @Officer_FirstName, @GroupID, @AddressStatus, @DP2, @DPC, @LoaderID, @DOB)
				
				Set @CurrentRecordID = scope_identity()
			End
		Else
			Begin
				Set @CurrentRecordID = @IDFound
				if Len(@Ticket_Number)<1
					Begin
						Set @Ticket_Number = @TicketNum2
					End
				IF @Address <> @AddressFound and @AddressStatus <> 'N' AND @AddressStatusFound = 'N'
					BEGIN
						-- Update defendant address when not available in non-client DB.
						UPDATE TrafficTickets.dbo.tblTicketsArchive
						SET Address1 = @Address, City = @City, StateID_FK = @StateID, ZipCode = @ZIP, DP2 = @DP2, DPC = @DPC, flag1 = @AddressStatus, UpdationLoaderID = @LoaderID, GroupID = @GroupID, DOB = @DOB -- Rab Nawaz Khan 10465 10/04/2012 Added DOB Column and value. . . 
						WHERE RecordID = @CurrentRecordID
						
						Set @Flag_Inserted = 2
					END
			End
	End
Else
	Begin
		Set @CurrentRecordID = @PreviousRecordID
	End

Set @IDFound = null
Set @IDFound = (Select top 1 Isnull(RowID,0) From TrafficTickets.dbo.tblTicketsViolationsArchive TV
                WHERE 
				(TV.ViolationNumber_PK = @ViolationID AND TV.RecordID = @CurrentRecordID)) --Farrukh 9927 12/30/2011 Changes updated from live

If @IDFound IS NULL
	BEGIN
		IF @IsQuoted = 1
			Begin
				if Len(@Ticket_Number)<1
					Begin
						insert into DallasTrafficTickets.dbo.tblIDGenerator(recdate) values(getdate())
						select @Ticket_Number='JVMC' + convert(varchar(12),scope_identity())
					End

				Insert Into TrafficTickets.dbo.tblTicketsArchive (RecLoadDate, ListDate, TicketNumber, MidNumber, FirstName, LastName, Initial, Race, Gender, PhoneNumber, Address1, City, StateID_FK, ZipCode, CourtID, OfficerNumber_FK, OfficerName, GroupID, Flag1, DP2, DPC, InsertionLoaderID, DOB) -- Rab Nawaz Khan 10465 10/04/2012 Added DOB Column and value. . . 
				Values(@Record_Date, @List_Date, @Ticket_Number, @Ticket_Number, @Name_First, @Name_Last, @Name_Middle, @Race, @Sex, '0000000000', @Address, @City, @StateID, @ZIP, 3034, @OfficerIDFound, @Officer_LastName + ', ' + @Officer_FirstName, @GroupID, @AddressStatus, @DP2, @DPC, @LoaderID, @DOB)

				Set @CurrentRecordID = scope_identity()
			END
		Insert Into TrafficTickets.dbo.tblTicketsViolationsArchive (TicketNumber_PK, ViolationNumber_PK, CauseNumber, CourtDate, TicketOfficerNumber, TicketViolationDate, OffenseLocation, InsertDate, UpdatedDate, ViolationDescription, ViolationCode, FineAmount, BondAmount, CourtLocation, RecordID, ViolationStatusID, LoaderUpdateDate, InsertionLoaderId, GroupID)
		Values(@Ticket_Number, @ViolationID, @Ticket_Number, @Court_Date, @OfficerIDFound, @Violation_Date, @Violation_Location, Getdate(), Getdate(), @Violation_Description, @Violation_Code, @ChargeAmount, 0, 3034, @CurrentRecordID, 116, getdate(), @LoaderID, @GroupID)

		Set @Flag_Inserted = 1
	End






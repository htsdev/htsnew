﻿/*
Created By    : Fahad Muhammad Qureshi
Type          : Loader Service
TaskID		  : 6068
Created date  : 07/14/2009
Business Logic: This Procedure Load(Update OR Insert) the data as Deliquent Cases on appropirate tables of Non-Client/Client in the system on the basis of following described Parameters.
Parameters: 
	@CauseNumber    :Cause no of the Defendant
	@Ticketnumber	:Ticket no of the Defendant
	@MidNumber		:This No show the batch identity of the Defendant
	@Lastname		:Last Name of the Defendant
	@Initial		:Middle Name of the Defendant
	@FirstName		:First Name of the Defendant
	@DOB			:Date of Birth of the Defendant
	@AddressPartI	:Address of the Defendant
	@city			:City of the Defendant
	@state			:State of the Defendant
	@ZipCode		:Zip Code of the Defendant
	@Phone			:Phone Number of the Defendant
	@CourtNumber	:Room Number of the Court
	@CourtDate		:Date of the Court for the Defendant
	@CourtTime		:Time Of the Court for the Defendant
	@CourtID		:Id by which we can identify the Court
	@ViolationDate	:Date of Violation.
	@OfficerNum		:Officer Number. 
	@FineAmount		:FineAmount/Price of the Defendant
	@BondAmount		:BondAmount of the Defendant
	@Race			:Race of the Defendant
	@Gender			:Gender of the Defendant
	@Height			:Height of the Defendant
	@ViolationDesc	:Violation Detail in form of Words.
	@ViolationCode	:Each Violation having Unique Code.
	@DP2			:Filled against by the response of Reliability Checking
	@DPC			:Filled against by the response of Reliability Checking	
	@AddressStatus	:Status Field filled by ZP4 service.
	@ListDate		:Date when Record is Listed.	
	@Flag_SameAsPrevious:Flag to check id is same as previous or the new one
	@PreviousRecordID:Save the previous record id 
	@LoaderID		 :Id by which we can identify the Loader
	@GroupID		 :File id by which we extract Data
	@CurrentRecordID :Point the Current record id
	@Flag_Inserted	 :Flag to check if record is insert or not 
	@IsClientInserted:To check that insertion in Client or Non-Client Records
	@IsClientUpdated :To check that Updating in Client or Non-Client Records
*/ 
CREATE PROCEDURE [dbo].[USP_HTS_LA_Insert_Tickets_HoustonDLQ]
			@CauseNumber NVARCHAR(50),
	        @TicketNumber NVARCHAR(50),
	        @MidNumber NVARCHAR(50),
	        @LastName NVARCHAR(20),
	        @Initial NVARCHAR(1),
	        @FirstName NVARCHAR(20),
	        @DOB DATETIME,
	        @AddressPartI NVARCHAR(50),
	        @City NVARCHAR(50),
	        @State NVARCHAR(50),
	        @ZipCode NVARCHAR(50),
	        @Phone NVARCHAR(50),
	        @CourtNumber NVARCHAR(3),
	        @CourtDate DATETIME,
	        @CourtTime DATETIME,
	        @CourtID int,
	        @ViolationDate DATETIME,
	        @OfficerNum NVARCHAR(50),
	        @FineAmount money,
	        @BondAmount money,
	        @Race NVARCHAR(50),
	        @Gender NVARCHAR(4),
	        @Height NVARCHAR(10),
	        @ViolationDesc NVARCHAR(500),
	        @ViolationCode NVARCHAR(50),
	        @DP2 NVARCHAR(50),
	        @DPC NVARCHAR(50),
	        @AddressStatus NVARCHAR(1),
	        @ListDate DATETIME,
			@Flag_SameAsPrevious as bit,
			@PreviousRecordID as int,
			@LoaderID as int,
			@GroupID AS INT, 
			@CurrentRecordID int OUTPUT,
			@Flag_Inserted int OUTPUT,
			@IsNonClientUpdated INT OUTPUT,
			@IsNonClientInserted INT OUTPUT
AS
set nocount on

Declare @StateID as Int
Declare @ViolationID as int
Declare @IDFound as int
Declare @TicketNum2 as varchar(20)
DECLARE @OfficerName AS VARCHAR(50)
DECLARE @BondDateFound DATETIME
DECLARE @ViolationStatusIDFound INT

Set @FirstName = upper(left(@FirstName,20))
Set @LastName = upper(left(@LastName,20))
SET @Initial=UPPER(LEFT(@Initial,20))
SET @IsNonClientUpdated=0
SET @IsNonClientInserted=0


SET @BondDateFound = '1/1/1900'
SET @ViolationStatusIDFound = 0
Set @Flag_Inserted = 0

SET @OfficerName = (
SELECT LTRIM(
           RTRIM(
               ISNULL(O.firstname, 'N/A') + ' ' + ISNULL(O.lastname, 'N/A')
           )
       )
FROM   TrafficTickets.dbo.tblOfficer O
WHERE  CONVERT(INT, @OfficerNum) = O.officernumber_pk
)

Set @ViolationID = null
Select Top 1 @ViolationID = ViolationNumber_PK  From TrafficTickets.dbo.TblViolations Where (Description = @ViolationDesc  and CourtLoc IN (3001,3002,3003) and ViolationType = 2)

If @ViolationID is null
	BEGIN
		Insert Into TrafficTickets.dbo.TblViolations (Description, Sequenceorder, IndexKey, IndexKeys, ChargeAmount, BondAmount, Violationtype, ChargeonYesflag, ShortDesc, ViolationCode, AdditionalPrice, CourtLoc)
		Values(@ViolationDesc, NULL,2,100,140,0,2,0,'None', @ViolationCode,0,0)
		
		Set @ViolationID = scope_identity()
	End

Set @StateID = (Select Top 1 StateID From TrafficTickets.dbo.TblState Where State = @State)

If @Flag_SameAsPrevious = 0
	BEGIN
		Set @IDFound = null

		SELECT Top 1 @IDFound = T.RecordID, @TicketNum2 = TicketNumber 
		From TrafficTickets.dbo.tblTicketsArchive T
		Inner Join TrafficTickets.dbo.tblTicketsViolationsArchive TV
		On T.RecordID = TV.RecordID
		Where 
		((TicketNumber = @TicketNumber and len(TicketNumber) > 1)
		OR
		(FirstName = @FirstName and LastName = @LastName and Address1 = @AddressPartI
		and ZipCode = @ZipCode and datediff(day,TV.CourtDate, @CourtDate) = 0 )) and TV.CourtLocation IN (3001,3002,3003)

		If @IDFound is null
			Begin
				if Len(@TicketNumber)<1
					Begin
						insert into TrafficTickets.dbo.tblIDGenerator(recdate) values(getdate())
						select @TicketNumber = 'DLQ' + convert(varchar(12),scope_identity())
					END
					
				Insert Into TrafficTickets.dbo.tblTicketsArchive (RecLoadDate, ListDate, CourtDate, TicketNumber, MidNumber, FirstName, LastName,Initial, PhoneNumber,DOB, Address1, City, StateID_FK, ZipCode,Race,Gender,Height, CourtID, OfficerNumber_FK, OfficerName, Flag1, DP2, DPC, InsertionLoaderID,GroupID)   
				Values(GETDATE(), @listdate, @CourtDate, @TicketNumber, @MidNumber, @FirstName, @LastName,@Initial,@Phone,@DOB,@AddressPartI, @City, @StateID, @ZipCode,@race,@Gender,@Height,@CourtId, @OfficerNum, @OfficerName, @AddressStatus, @DP2, @DPC, @LoaderID,@GroupID)

				Set @CurrentRecordID = scope_identity()
			End
		Else
			BEGIN
				Set @CurrentRecordID = @IDFound
				if Len(@TicketNumber)<1
					Begin
						Set @TicketNumber = @TicketNum2
					End
			End
	End
Else
	Begin
		Set @CurrentRecordID = @PreviousRecordID
	End

Set @IDFound = null
	SELECT TOP 1 @IDFound = RowID, @BondDateFound = BondDate, @ViolationStatusIDFound = violationstatusid
	FROM   TrafficTickets.dbo.tblTicketsViolationsArchive
	WHERE  (CauseNumber = @CauseNumber AND CourtLocation IN (3001, 3002, 3003))

If @IDFound is null
	BEGIN
		Insert Into TrafficTickets.dbo.tblTicketsViolationsArchive (TicketNumber_PK, ViolationNumber_PK, CauseNumber,STATUS,BondDate, CourtDate, Courtnumber, CourtLocation, InsertDate, ViolationDescription, ViolationCode, FineAmount, BondAmount, RecordID, ViolationStatusID, InsertionLoaderId, GroupID)
		Values(@TicketNumber, @ViolationID, @CauseNumber,'DLQ',@CourtDate,@CourtDate, @CourtNumber, @CourtId, Getdate(), @ViolationDesc, @ViolationCode, @FineAmount, @BondAmount, @CurrentRecordID, 146, @LoaderID, @GroupID)
		SET @Flag_Inserted = 1
		SET @IsNonClientInserted = 2
	END
ELSE
	BEGIN
		IF DATEDIFF(DAY, @BondDateFound, @CourtDate)>0 or @ViolationStatusIDFound NOT IN (80, 145, 146, 147, 148)
			BEGIN
				UPDATE TrafficTickets.dbo.tblTicketsViolationsArchive 
				SET violationstatusid = 146, BondDate = @CourtDate, BondAmount = @BondAmount, 
				updateddate = @CourtDate, [Status] = 'DLQ', UpdationLoaderId = @LoaderID,DLQUpdateDate = GETDATE(),LoaderUpdateDate = GETDATE(),GroupID = @GroupID
				WHERE RowID = @IDFound
				
				Set @Flag_Inserted = 2
				SET @IsNonClientUpdated = 2
			END
	END
GO
GRANT EXEC ON [dbo].[USP_HTS_LA_Insert_Tickets_HoustonDLQ] TO dbr_webuser
GO  
﻿/*
Created By    : Fahad Muhammad Qureshi
Type          : Loader Service
TaskID		  : 6068
Created date  : 07/14/2009
Business Logic: This Procedure Load(Update OR Insert) the Arraignmnet Caese in appropirate tables of Non-Client/QouteClinet OR Client in the system on the basis of following described Parameters.
Parameters: 
	@CauseNumber    :Cause no of the Defendant
	@Ticketnumber	:Ticket no of the Defendant
	@MidNumber		:This No show the batch identity of the Defendant
	@Lastname		:Last Name of the Defendant
	@Initial		:Middle Name of the Defendant
	@FirstName		:First Name of the Defendant
	@Address		:Address of the Defendant
	@city			:City of the Defendant
	@state			:State of the Defendant
	@zip			:Zip Code of the Defendant
	@Phone			:Phone Number of the Defendant
	@DOB			:Date of Birth of the Defendant
	@Race			:Race of the Defendant
	@Gender			:Gender of the Defendant
	@Height			:Height of the Defendant
	@FineAmount		:FineAmount/Price of the Defendant
	@OfficerNum		:Officer Number. 
	@Officer_firstname	:First Part of the Offiecr Name
	@Officer_lastname	:Last Part of the Officer name
	@CourtTime		:Time Of the Court for the Defendant
	@CourtDate		:Date of the Court for the Defendant
	@CourtNum		:Room Number of the Court
	@CourtID		:Id by which we can identify the Court
	@ViolationDesc	:Violation Detail in form of Words.
	@ViolationCode	:Each Violation having Unique Code.
	@ViolationDate	:Date of Violation.
	@OffenceLocation:Place where Violation Take place.
	@OffenceTime	:Time when Violation Take place.
	@AddressStatus	:Status Field filled by ZP4 service.	
	@DP2			:Filled against by the response of Reliability Checking
	@DPC			:Filled against by the response of Reliability Checking			
	@LoaderID		:Id by which we can identify the Loader
	@GroupID		:File id by which we extract Data
	@Flag_SameAsPrevious:Flag to check id is same as previous or the new one
	@PreviousRecordID	:Save the previous record id 
	@CurrentRecordID	:Point the Current record id
	@Flag_Inserted		:Flag to check if record is insert or not 
	@IsClientInserted	:To check that insertion in Client or Non-Client Records
*/
CREATE PROCEDURE [dbo].[USP_HTS_LA_Insert_Tickets_HoustonArraignment]
				@CauseNumber AS VARCHAR(50),
				@Ticketnumber AS VARCHAR(50),
				@MidNumber AS VARCHAR(50),
				@Lastname AS VARCHAR(50),
				@Initial AS VARCHAR(4),
				@FirstName AS VARCHAR(50),
				@Address AS VARCHAR(500),
				@city AS VARCHAR(60),
				@state AS VARCHAR(50),
				@zip AS VARCHAR(50),
				@Phone AS VARCHAR(50),
				@DOB AS DATETIME ,
				@Race AS VARCHAR(50),
				@Gender AS VARCHAR(10), 
				@Height AS VARCHAR(10),
				@FineAmount AS VARCHAR(50),
				@OfficerNum AS VARCHAR(50),
				@Officer_firstname AS VARCHAR(50), 
				@Officer_lastname AS VARCHAR(50),
				@CourtTime AS DATETIME,
				@CourtDate AS DATETIME,
				@CourtNum AS VARCHAR(50),
				@CourtID AS INT,
				@ViolationDesc AS VARCHAR(200),
				@ViolationCode AS VARCHAR(10),
				@ViolationDate AS DATETIME,
				@OffenceLocation AS VARCHAR(50),
				@OffenceTime AS DATETIME,
				@AddressStatus NVARCHAR(4),
				@DP2 NVARCHAR(50),
				@DPC NVARCHAR(50),
				@LoaderID AS INT,
				@GroupID AS INT,
				@Flag_SameAsPrevious AS INT,
				@PreviousRecordID AS INT,
				@CurrentRecordID AS INT OUTPUT,
				@Flag_Inserted AS INT OUTPUT,
				@IsNonClientInserted INT OUTPUT
				
	AS 
	Declare	@IDFound as INT,
			@StateID AS INT,
			@TicketNum2 as varchar(20),
			@ViolationID AS VARCHAR(50),
			@OfficerName AS VARCHAR(50)
     
     SET    @Flag_Inserted=0
	 SET	@IsNonClientInserted=0
      
	        
SET @OfficerName = (SELECT LTRIM(RTRIM(ISNULL(O.firstname, 'N/A') + ' ' + ISNULL(O.lastname, 'N/A')))FROM   TrafficTickets.dbo.tblOfficer O WHERE @OfficerNum = O.OfficerNumber_PK)

Set @ViolationID = null
Select Top 1 @ViolationID = ViolationNumber_PK  From TrafficTickets.dbo.TblViolations WHERE  ((ViolationCode = @ViolationCode OR Description = @ViolationDesc) and CourtLoc IN (3001,3002,3003) and ViolationType = 2)

If @ViolationID is null
	BEGIN
		Insert Into TrafficTickets.dbo.TblViolations (Description, Sequenceorder, IndexKey, IndexKeys, ChargeAmount, BondAmount, Violationtype, ChargeonYesflag, ShortDesc, ViolationCode, AdditionalPrice, CourtLoc)
		Values(@ViolationDesc, NULL,2,100,140,0,2,0,'None', @ViolationCode,0,0)
		
		Set @ViolationID = scope_identity()
		Set @ViolationID = null
	End

Set @StateID = (Select Top 1 StateID From TrafficTickets.dbo.TblState Where State = @State)

If @Flag_SameAsPrevious = 0
	Begin
		Set @IDFound = null
		SELECT Top 1 @IDFound = T.RecordID, @TicketNum2 = TicketNumber
		From traffictickets.dbo.tblTicketsArchive T
		Inner Join traffictickets.dbo.tblTicketsViolationsArchive TV
		On T.RecordID = TV.RecordID
		Where (TicketNumber = @Ticketnumber) OR (FirstName = @FirstName and LastName = @Lastname and Address1 = @Address and datediff(day,DOB, @DOB) = 0
		and ZipCode = @zip and datediff(day,ViolationDate, @ViolationDate) = 0 and TV.CourtLocation in (3001,3002,3003,3004))
		
		If @IDFound is null
			Begin
				if Len(@TicketNumber)<1
					Begin
						insert into traffictickets.dbo.tblIDGenerator(recdate) values(getdate())
						select @Ticketnumber ='ID' + convert(varchar(12),@@identity)
					End
					INSERT INTO traffictickets.dbo.tblticketsarchive (TicketNumber,MidNumber,FirstName,LastName,Initial,Address1,City,StateID_FK,PhoneNumber,ZipCode,DOB,ViolationDate,CourtDate,OfficerName,Race,Gender,Height,listdate,CourtNumber,Courttime,DP2,DPC,courtid,flag1,OfficerNumber_FK,insertionloaderid,GroupID,RecLoadDate)
					VALUES(@Ticketnumber,LEFT(@MidNumber, 20),LEFT(@FirstName, 20),LEFT(@Lastname, 20),@Initial,UPPER(LEFT(@Address, 50)),LEFT(UPPER(@city), 50),@StateID,LEFT(@Phone, 15),@zip,@DOB,@ViolationDate,@CourtDate,@OfficerName,	    
									@Race,@Gender,@Height,GETDATE(),@CourtNum,@CourtTime,@DP2,@DPC,@CourtID,@AddressStatus,ISNULL(@OfficerNum,'962528'),@LoaderId,@GroupID,GETDATE())
						
					Set @CurrentRecordID = scope_identity()	
					Set @Flag_Inserted = 1
			END	
			Else
			BEGIN
				Set @CurrentRecordID = @IDFound
				if Len(@TicketNumber)<1
					Begin
						Set @TicketNumber = @TicketNum2
					End
			End
	End	
	
	Else
	Begin
		Set @CurrentRecordID = @PreviousRecordID
	END


    Set @IDFound = NULL
	SELECT TOP 1 @IDFound = RowID
	FROM   TrafficTickets.dbo.tblTicketsViolationsArchive
	WHERE  (CauseNumber = @CauseNumber AND CourtLocation IN (3001, 3002, 3003))

If @IDFound is null
	BEGIN
		INSERT INTO TrafficTickets.dbo.tblticketsviolationsarchive  
				(TicketNumber_PK, ViolationNumber_PK, FineAmount, ViolationDescription, ViolationCode, violationstatusid, Status, courtdate, courtnumber, courtlocation, bondamount, ticketviolationdate, ticketofficernumber, CauseNumber, ftaissuedate, InsertionLoaderId, offenselocation, offensetime, recordid,GroupID)  
		VALUES
				(@Ticketnumber,0,CONVERT(MONEY, @FineAmount),@ViolationDesc,@violationcode,3,'ARR',@CourtDate,@CourtNum,@CourtID,0,@ViolationDate,@OfficerNum,@causenumber,GETDATE(),@LoaderId,@OffenceLocation,@OffenceTime,@CurrentRecordID,@GroupID) 
		Set @Flag_Inserted = 1
		SET @IsNonClientInserted=2

	END
GO
GRANT EXEC ON [dbo].[USP_HTS_LA_Insert_Tickets_HoustonArraignment] TO dbr_webuser
GO  
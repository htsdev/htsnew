﻿USE [LA_Houston]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTS_LA_Insert_Tickets_HCCC_Daily]    Script Date: 05/31/2013 04:53:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*
Author: Hafiz Khalid Nawaz
TFS ID: 10438
Description: To Upload HCCC Cases From Loader Service
Type : Loader Service
Created date : N/A
Parameters: 
	@Record_Date: Case record/insert date
	@List_Date: Court list date
	@Ticket_Number: Ticket Number
	@Name_Last: Defendant's last name
	@Name_First: Defendant's first name
	@Address: Defendant's home address
	@City: Defendant's home city
	@State: Defendant's home state
	@ZIP: Defendant's home zip code
	@DP2: Defendant's barcode part. return by zp4 webservice
	@DPC: Defendant's barcode part. return by zp4 webservice
	@Violation_Description: offense description
	@BondAmount: Violation Bond Amount.
	@Court_Number: Court Number.
	@SPN_Number: MID Number
	@CDI: CDI
	@CST: Court Status
	@DST: Disposition Status
	--added additional
	@Race ,
	@Gender 
	@Dob ,
	@Attorney_Name ,
	@ins ,
	@cad ,
	@cnc ,
	@rea ,
	@ChargeLevel ,
	@GroupID: Data file ID
	@AddressStatus: Address status [y/d/s/n] of defendant
	@Flag_SameAsPrevious: insure wether this case belongs to previous one or not [It helps to cut-off researching the defendant profile]
	@PreviousRecordID: If this case belongs to previous case then previous case ID will be the value of @PreviousRecordID
	@PreviousTicketNumber: If this case belongs to previous case then previous record's Ticket Number will be passed
	@LoaderID: Insure which loader is executing this case.
	@CurrentRecordID: It returns current Case-ID/Record-ID to calling program
	@Flag_Inserted: It returns status wether the record inserted or deleted
*/

ALTER PROCEDURE [dbo].[USP_HTS_LA_Insert_Tickets_HCCC_Daily]
	@Record_Date AS	DATETIME, 
	@List_Date AS DATETIME,
	@Ticket_Number AS VARCHAR(20),
	@Name_Last AS VARCHAR(50), 
	@Name_First AS VARCHAR(50), 
	@Address AS VARCHAR(50), 
	@City AS VARCHAR(35), 
	@State AS VARCHAR(10),
	@ZIP AS VARCHAR(10), 
	@DP2 VARCHAR(2),
	@DPC VARCHAR(1),
	@Violation_Description AS VARCHAR(500),
	@BondAmount AS MONEY,
	@Court_Number VARCHAR(5),
	@SPN_Number VARCHAR(20),
	@CDI VARCHAR(3),
	@CST VARCHAR(3),
	@DST VARCHAR(3),
	@Race VARCHAR(10),
	@Gender VARCHAR(10),
	@Dob VARCHAR(100),
	@Attorney_Name VARCHAR(150),
	@ins VARCHAR(10),
	@cad VARCHAR(10),
	@cnc VARCHAR(10),
	@rea VARCHAR(10),
	@ChargeLevel VARCHAR(10),
	@GroupID AS INT,
	@AddressStatus CHAR(1),
	@Flag_SameAsPrevious AS BIT,
	@PreviousRecordID AS INT,
	@PreviousTicketNumber AS VARCHAR(20),
	@LoaderID AS INT,
	@CurrentRecordID INT OUTPUT,
	@Flag_Inserted INT OUTPUT
	
	AS
	SET NOCOUNT ON  
	
	DECLARE @StateID AS INT
	DECLARE @ViolationID AS INT
	DECLARE @IDFound AS INT
	DECLARE @VioIDFound AS INT
	DECLARE @TicketNum2 AS VARCHAR(20)
	DECLARE @IsQuoted AS INT
	DECLARE @chargLCode AS INT 
	
	
	SET @IsQuoted = 0
	SET @SPN_Number = REPLACE(@SPN_Number, ',', '')
	SET @Name_First = LTRIM(RTRIM(UPPER(LEFT(@Name_First, 20))))
	SET @Name_Last = LTRIM(RTRIM(UPPER(LEFT(@Name_Last, 20))))
	SET @Ticket_Number = REPLACE(@Ticket_Number, ' ', '')
	
	SET @chargLCode = (
	        SELECT tc.ID
	        FROM    TrafficTickets.dbo.tblChargelevel tc
	        WHERE  tc.LevelCode = @ChargeLevel
	    ) 
	
	SET @Flag_Inserted = 0
	
	IF @ZIP = '00000'
	BEGIN
	    SET @ZIP = ''
	END
	
	
	SET @ViolationID = NULL
	SELECT TOP 1 @ViolationID = ViolationNumber_PK
	FROM   TrafficTickets.dbo.TblViolations WITH(NOLOCK)
	WHERE  (
	           LTRIM(RTRIM(DESCRIPTION)) = LTRIM(RTRIM(@Violation_Description))
	           AND ViolationType = 9
	       )
	
	IF @ViolationID IS NULL
	BEGIN
	    INSERT INTO TrafficTickets.dbo.TblViolations
	      (
	        DESCRIPTION,
	        Sequenceorder,
	        IndexKey,
	        IndexKeys,
	        ChargeAmount,
	        BondAmount,
	        Violationtype,
	        ChargeonYesflag,
	        ShortDesc,
	        ViolationCode,
	        AdditionalPrice,
	        CourtLoc
	      )
	    VALUES
	      (
	        @Violation_Description,
	        NULL,
	        2,
	        100,
	        0,
	        @BondAmount,
	        9,
	        1,
	        'None',
	        'none',
	        0,
	        3037
	      )
	    SET @ViolationID = SCOPE_IDENTITY()
	END
	
	IF LTRIM(RTRIM(ISNULL(@State, ''))) = ''
	BEGIN
	    SET @State = 'TX'
	END
	
	SET @StateID = (
	        SELECT TOP 1 StateID 
	        FROM   TrafficTickets.dbo.TblState WITH(NOLOCK)
	        WHERE  STATE = @State
	    )
	
	IF @Flag_SameAsPrevious = 0
	BEGIN
	    SET @IDFound = NULL
	    
	    SELECT TOP 1 @IDFound = T.RecordID,
	           @TicketNum2 = T.TicketNumber,
	           @IsQuoted = ISNULL(T.Clientflag, 0)
	    FROM   TrafficTickets.dbo.tblTicketsArchive T WITH(NOLOCK)
	           INNER JOIN TrafficTickets.dbo.tblTicketsViolationsArchive TV WITH(NOLOCK)
	                ON  TV.RecordID = T.RecordID
	    WHERE  (
	    			T.TicketNumber = @Ticket_Number OR 
					(t.LastName = @Name_Last AND t.FirstName = @Name_First AND DATEDIFF(DAY,t.DOB,@Dob) = 0 )
			   )	           
	           AND t.InsertionLoaderID IN (26, 69, 70) --for existing loaders
	           AND TV.violationstatusid <> 80 
	    
	    
	    IF @IDFound IS NULL
	    BEGIN
	        IF LEN(@Ticket_Number) < 5
	        BEGIN
	            INSERT INTO TrafficTickets.dbo.tblIDGenerator
	              (
	                recdate
	              )
	            VALUES
	              (
	                GETDATE()
	              )
	            SELECT @Ticket_Number = 'HC' + CONVERT(VARCHAR(12), @@identity)
	        END
	        
	        INSERT INTO TrafficTickets.dbo.tblTicketsArchive
	          (
	            RecLoadDate,
	            ListDate,
	            CourtDate,
	            ViolationDate,
	            BondDate,
	            TicketNumber,
	            MidNumber,
	            FirstName,
	            LastName,
	            PhoneNumber,
	            Address1,
	            City,
	            StateID_FK,
	            ZipCode,
	            GroupID,
	            Flag1,
	            DP2,
	            DPC,
	            InsertionLoaderID,
	            Race,
	            Gender,
	            DOB
	          )
	        VALUES
	          (
	            @Record_Date,
	            @List_Date,
	            '1/1/1900',
	            '1/1/1900',
	            '1/1/1900',
	            @Ticket_Number,
	            @SPN_Number,
	            @Name_First,
	            @Name_Last,
	            '',
	            @Address,
	            @City,
	            @StateID,
	            @ZIP,
	            @GroupID,
	            @AddressStatus,
	            @DP2,
	            @DPC,
	            @LoaderID,
	            @Race,
	            @Gender,
	            @Dob
	          )
	        
	        SET @CurrentRecordID = SCOPE_IDENTITY()	        
	    END
	    ELSE
	    BEGIN
	        --IF matching record found
	        SET @CurrentRecordID = @IDFound
	        IF LEN(@Ticket_Number) < 5
	        BEGIN
	            SET @Ticket_Number = @TicketNum2
	        END
	    END
	END
	ELSE
	BEGIN
	    SET @CurrentRecordID = @PreviousRecordID
	    SET @TicketNum2 = ISNULL(@PreviousTicketNumber, '')
	END
	--Sabir Khan 11125 05/30/2013 Optimized query
	SET @VioIDFound = NULL
	SET @VioIDFound = (
	        SELECT TOP 1 RowID
	        FROM   TrafficTickets.dbo.tblTicketsViolationsArchive WITH(NOLOCK)	        
	        WHERE  (ISNULL(TicketNumber_PK, '') = @Ticket_Number)
	        )
	IF(isnull(@VioIDFound,'') = '')
	BEGIN
		SET @VioIDFound =(       
	        SELECT TOP 1 RowID
	        FROM   TrafficTickets.dbo.tblTicketsViolationsArchive WITH(NOLOCK)	        
	        WHERE  (ISNULL(CauseNumber, '') = @Ticket_Number)
	        )
	END
	
	IF @VioIDFound IS NULL
	BEGIN
		
	    INSERT INTO TrafficTickets.dbo.tblTicketsViolationsArchive
	      (
	        TicketNumber_PK,
	        ViolationNumber_PK,
	        CauseNumber,
	        CourtNumber,
	        CourtLocation,
	        CourtDate,
	        TicketViolationDate,
	        BondDate,
	        UpdatedDate,
	        ViolationDescription,
	        ViolationCode,
	        FineAmount,
	        BondAmount,
	        RecordID,
	        ViolationStatusID,
	        HCCC_CDI,
	        HCCC_CST,
	        HCCC_DST,	        
	        InsertionLoaderId,
	        GroupID,
	        AttorneyName,
	        ins,
	        cad,
	        cnc,
	        rea,
	        ChargeLevel
	      )
	    VALUES
	      (
	        (
	            CASE 
	                 WHEN ISNULL(@TicketNum2, '') <> '' THEN @TicketNum2
	                 ELSE @Ticket_Number
	            END
	        ),
	        @ViolationID,
	        @Ticket_Number,
	        @Court_Number,
	        3037,
	        '1/1/1900',
	        '1/1/1900',
	        '1/1/1900',
	        GETDATE(),
	        @Violation_Description,
	        'None',
	        0,
	        @BondAmount,
	        @CurrentRecordID,
	        116,
	        @CDI,
	        @CST,
	        @DST,	        
	        @LoaderID,
	        @GroupID,
	        @Attorney_Name,
	        @ins,
	        @cad,
	        @cnc,
	        @rea,
	        @chargLCode
	      )
	    SET @Flag_Inserted = 1
	END--Updated the existing case
	ELSE
	BEGIN
	    DECLARE @isExistViolation AS INT 
	    --Sabir Khan 11125 05/30/2013 Optimized query
	    SELECT TOP 1 @isExistViolation = RowID,@IDFound = RecordID FROM   TrafficTickets.dbo.tblTicketsViolationsArchive WITH(NOLOCK)
		WHERE  (ISNULL(TicketNumber_PK, '') = @Ticket_Number) AND (UPPER(ViolationDescription) = UPPER(@Violation_Description))

		IF(ISNULL(@isExistViolation,'') = '' AND ISNULL(@IDFound,'') = '')
		BEGIN
			SELECT TOP 1 @isExistViolation = RowID,@IDFound = RecordID  FROM   TrafficTickets.dbo.tblTicketsViolationsArchive WITH(NOLOCK)
			WHERE  (ISNULL(CauseNumber, '') = @Ticket_Number) AND (UPPER(ViolationDescription) = UPPER(@Violation_Description))
		END
	    
	    IF (@isExistViolation IS NOT NULL) --violation exist
	    BEGIN
	        --
	        UPDATE TrafficTickets.dbo.tblTicketsArchive
	        SET    UpdationLoaderID = @LoaderID,
				   DOB = @Dob,				   
	               GroupID = @GroupID
	        WHERE RecordID = @IDFound
	        
	        ---
	        UPDATE TrafficTickets.dbo.tblTicketsViolationsArchive
	        SET    LoaderUpdateDate = GETDATE(),
	               UpdationLoaderId = @LoaderID,
	               GroupID = @GroupID
	        WHERE RecordID = @IDFound       
	    END
	    ELSE
	    BEGIN
	        INSERT INTO TrafficTickets.dbo.tblTicketsViolationsArchive
	          (
	            TicketNumber_PK,
	            ViolationNumber_PK,
	            CauseNumber,
	            CourtNumber,
	            CourtLocation,
	            CourtDate,
	            TicketViolationDate,
	            BondDate,
	            UpdatedDate,
	            ViolationDescription,
	            ViolationCode,
	            FineAmount,
	            BondAmount,
	            RecordID,
	            ViolationStatusID,
	            HCCC_CDI,
	            HCCC_CST,
	            HCCC_DST,	            
	            InsertionLoaderId,
	            GroupID,
	            AttorneyName,
	            ins,
	            cad,
	            cnc,
	            rea,
	            ChargeLevel
	          )
	        VALUES
	          (
	            (
	                CASE 
	                     WHEN ISNULL(@TicketNum2, '') <> '' THEN @TicketNum2
	                     ELSE @Ticket_Number
	                END
	            ),
	            @ViolationID,
	            @Ticket_Number,
	            @Court_Number,
	            3037,
	            '1/1/1900',
	            '1/1/1900',
	            '1/1/1900',
	            GETDATE(),
	            @Violation_Description,
	            'None',
	            0,
	            @BondAmount,
	            @CurrentRecordID,
	            116,
	            @CDI,
	            @CST,
	            @DST,	            
	            @LoaderID,
	            @GroupID,
	            @Attorney_Name,
	            @ins,
	            @cad,
	            @cnc,
	            @rea,
	            @chargLCode
	          )
	        SET @Flag_Inserted = 1
	    END
	    
	    --Two updates
	END
	

﻿USE [LA_Houston]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTS_LA_Insert_Tickets_ArlingtonArraignment_Excel]    Script Date: 05/31/2012 08:47:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*  
Author: Sabir Khan 
Description: To Upload Arlington Arraignment Cases From Loader Service Via Excel File
Type : Loader Service  
Created date : May-31st-2012  
Parameters:   
 @Record_Date: Case record/insert date  
 @List_Date: Court list date  
 @Ticket_Number: Ticket Number  
 @Cause_Number: Cause Number  
 @Name_First: Defendant's first name  
 @Name_Last: Defendant's last name  
 @Address: Defendant's home address  
 @City: Defendant's home city  
 @State: Defendant's home state  
 @ZIP: Defendant's home zip code  
 @DP2: Defendant's barcode part. return by zp4 webservice  
 @DPC: Defendant's barcode part. return by zp4 webservice  
 @Violation_Date: Offense date.  
 @Violation_Description: offense description  
 @Fine_Amount: Violation's fine amount.  
 @Filed_Date: Case filed date  
 @Officer_FirstName: Officer first ame.  
 @Officer_LastName: Officer last name.  
 @GroupID: Data file ID  
 @AddressStatus: Address status [y/d/s/n] of defendant  
 @Flag_SameAsPrevious: insure wether this case belongs to previous one or not [It helps to cut-off researching the defendant profile]  
 @PreviousRecordID: If this case belongs to previous case then previous case ID will be the value of @PreviousRecordID  
 @PreviousTicketNumber: If this case belongs to previous case then previous case Ticket number will be the value of @PreviousTicketNumber  
 @LoaderID: Insure which loader is executing this case.  
 @CurrentRecordID: It returns current Case-ID/Record-ID to calling program  
 @CurrentTicketNumber: It returns current case ticket number to calling program  
 @Flag_Inserted: It returns status wether the record inserted or deleted  
*/  
  
CREATE PROCEDURE [dbo].[USP_HTS_LA_Insert_Tickets_ArlingtonArraignment_Excel]  
@Record_Date datetime,    
@List_Date as smalldatetime,  
@Ticket_Number as varchar(20),  
@Cause_Number as varchar(20),  
@Name_First as varchar(50),    
@Name_Last as varchar(50),    
@Address as varchar(50),    
@City as varchar(35),    
@State as Varchar(10),  
@ZIP as varchar(10),    
@DP2 varchar(2),  
@DPC varchar(1),  
@Violation_Date datetime,    
@Violation_Description as Varchar(200),  
@FineAmount as Money,  
@Filed_Date as DATETIME,  
@Officer_FirstName as varchar(50),  
@Officer_LastName  as varchar(50),  
@GroupID as Int,  
@AddressStatus Char(1),  
@Flag_SameAsPrevious as bit,  
@PreviousRecordID as int,  
@LoaderID as int,  
@DOB  VARCHAR(15),
@Race VARCHAR(10),
@Sex VARCHAR(10),
@ViolType VARCHAR(200),
@offensestatute VARCHAR(200),
@plea VARCHAR(200),
@officeragency VARCHAR(200),
@AttorneyFirst VARCHAR(200),
@AttorneyLast VARCHAR(200),
@Courtdate DATETIME,
@CurrentRecordID int output,  
@Flag_Inserted int OUTPUT
AS  
set nocount on    

Declare @StateID as Int  
Declare @OfficerCodeFound INT
Declare @ViolationID as int  
Declare @IDFound as int  
Declare @TicketNum2 as varchar(20)  
Declare @Court_Date as datetime  
DECLARE @Violation_Code VARCHAR(12)  
DECLARE @IsQuoted AS INT  
DECLARE @OfficerFullName AS VARCHAR(100)  
SET @IsQuoted = 0  
Set @Name_First = left(@Name_First,20)  
Set @Name_Last = left(@Name_Last,20)  
  
Set @Address = Replace(@Address, '  ', ' ')  
Set @Flag_Inserted = 0  
Set @List_Date = convert(datetime, convert(varchar(12), @List_Date, 101))   
Set @Court_Date = dateadd(day,30,@Violation_Date)  
SET @Ticket_Number = REPLACE(@Ticket_Number, ' ', '')  
SET @Cause_Number = REPLACE(@Cause_Number, ' ', '')  
if datepart(dw,@Court_Date) = 7 -- If Saturday then make it Friday (Court date should be in working days)  
 begin  
  set @Court_Date = @Court_Date - 1  
 end  
else if datepart(dw,@Court_Date) = 1 -- If Sunday then make it Friday (Court date should be in working days)  
 begin  
  set @Court_Date = @Court_Date - 2  
 end  
  
if len(@Ticket_Number) < 5  
 Begin  
  Set @Ticket_Number = @Cause_Number  
 End  
  
if @Cause_Number = ''  
 Begin  
  Set @Cause_Number = @Ticket_Number  
 End  
  
If @ZIP = '00000'  
 Begin  
  Set @ZIP = ''  
 End  
  
If @FineAmount = 0  
 Begin  
  Set @FineAmount = 100  
 END  
  
If ISNULL(@State, '') = ''  
 Begin  
  Set @State = 'TX'  
 END
	
     IF NOT ISNULL(@Officer_LastName,'')=''  
     BEGIN
		SET @OfficerFullName=@Officer_FirstName + ', '+ @Officer_LastName
     END
     ELSE SET @OfficerFullName=@Officer_FirstName	
        
Set @ViolationID = null  
Select Top 1 @ViolationID = ViolationNumber_PK, @Violation_Code = ViolationCode From DallasTrafficTickets.dbo.TblViolations Where (Description = @Violation_Description and CourtLoc = 3068 and ViolationType = 4)  
  
If @ViolationID is null  
 Begin  
  Insert Into DallasTrafficTickets.dbo.TblViolations (Description, Sequenceorder, IndexKey, IndexKeys, ChargeAmount, BondAmount, Violationtype, ChargeonYesflag, ShortDesc, ViolationCode, AdditionalPrice, Attorneyregularprice, Attorneybondprice, CourtLoc) 
 
  Values(@Violation_Description, null, 2, 160, @FineAmount, 200, 4, 1, 'None', 'XD-None', 0, 50, 95, 3068)  
  Set @ViolationID = scope_identity()  
  
  SET @Violation_Code = 'XD-None'  
 End  
  
Set @StateID = (Select Top 1 StateID From DallasTrafficTickets.dbo.TblState Where State = @State)  

if isnull(@Officer_FirstName,'') = ''	
	Begin
		Set @Officer_FirstName = 'N/A'
	END
	  
Set @OfficerCodeFound = null
Select Top 1 @OfficerCodeFound = OfficerNumber_PK from DallasTrafficTickets.dbo.tblOfficer where (isnull(FirstName,'') = @Officer_FirstName AND isnull(LastName,'') = @Officer_LastName) -- SAEED-7694-04/15/2010, checking existance of officer name based on first and last name.
if @OfficerCodeFound is null 
	BEGIN
		select @OfficerCodeFound =max(OfficerNumber_PK)+1 from DallasTrafficTickets.dbo.tblofficer	
		Insert Into DallasTrafficTickets.dbo.tblOfficer (OfficerNumber_PK, OfficerType, FirstName, LastName, Comments) Values (@OfficerCodeFound, 'N/A', @Officer_FirstName, @Officer_LastName, 'Inserted at ' + convert(varchar(25),getdate(),101))  
		
	End


If @Flag_SameAsPrevious = 0  
 Begin  
  Set @IDFound = null  
  
  SELECT Top 1 @IDFound = T.RecordID, @TicketNum2 = TicketNumber, @IsQuoted = T.Clientflag  
  From DallasTrafficTickets.dbo.tblTicketsArchive T  
  Inner Join DallasTrafficTickets.dbo.tblTicketsViolationsArchive TV  
  On T.RecordID = TV.RecordID  
  Where ((TicketNumber = @Ticket_Number OR TicketNumber = SUBSTRING (@Ticket_Number, 0, LEN(@Ticket_Number)-1)) OR (FirstName = @Name_First and LastName = @Name_Last and Address1 = @Address   
  and ZipCode = @Zip and datediff(day,ViolationDate, @Violation_Date) = 0)) and TV.CourtLocation = 3068  
  AND TV.violationstatusid <> 80 
  
  If @IDFound is null  
   Begin  
    if Len(isnull(@Ticket_Number,''))<5  
     Begin  
      insert into DallasTrafficTickets.dbo.tblIDGenerator(recdate) values(getdate())  
      select @Ticket_Number='AR' + convert(varchar(12),@@identity)  
     End  
     
    Insert Into DallasTrafficTickets.dbo.tblTicketsArchive (RecLoadDate, ListDate, CourtDate, ViolationDate, TicketNumber, MidNumber, FirstName, LastName, PhoneNumber, Address1, City, StateID_FK, ZipCode, CourtID, officerNumber_Fk, OfficerName, GroupID, Flag1, DP2, DPC, InsertionLoaderID,DOB,Race,Gender)     
    Values(@Record_Date, @List_Date, @Courtdate, @Violation_Date, @Ticket_Number, @Ticket_Number, @Name_First, @Name_Last, '0000000000', @Address, @City, @StateID, @ZIP, 3068, @OfficerCodeFound, @OfficerFullName , @GroupID, @AddressStatus, @DP2, @DPC, @LoaderID,@DOB,@Race,@Sex)  
  
    Set @CurrentRecordID = scope_identity()  
   End  
  Else  
   Begin  
    Set @CurrentRecordID = @IDFound  
    if Len(isnull(@Ticket_Number,'')) < 5  
     Begin  
      Set @Ticket_Number = @TicketNum2  
     End  
   End  
 END  
Else  
 Begin  
  Set @CurrentRecordID = @PreviousRecordID  
  SELECT Top 1 @TicketNum2 = TicketNumber_PK  
  From DallasTrafficTickets.dbo.tblTicketsViolationsArchive TV  
  Where RecordID = @CurrentRecordID  
 End  
  
if Len(isnull(@Ticket_Number,'')) < 5  
 Begin  
  Set @Ticket_Number = @TicketNum2  
 End  
  
If isnull(@Cause_Number,'') = ''  
 Begin  
  Set @Cause_Number = @Ticket_Number + convert(varchar(3), (Select Count(*) from DallasTrafficTickets.dbo.tblTicketsViolationsArchive TV where RecordID = @CurrentRecordID) + 1)  
 End  
  
Set @IDFound = null  
Set @IDFound = (Select top 1 RowID From DallasTrafficTickets.dbo.tblTicketsViolationsArchive  
   Where   
   (CauseNumber = @Cause_Number) and CourtLocation = 3068 ) 
  
If @IDFound is null  
 BEGIN  
  IF @IsQuoted = 1 
   Begin  
    if Len(isnull(@Ticket_Number,''))<5  
     Begin  
      insert into DallasTrafficTickets.dbo.tblIDGenerator(recdate) values(getdate())  
      select @Ticket_Number='AR' + convert(varchar(12),@@identity)  
     End  
  
    Insert Into DallasTrafficTickets.dbo.tblTicketsArchive (RecLoadDate, ListDate, CourtDate, ViolationDate, TicketNumber, MidNumber, FirstName, LastName, PhoneNumber, Address1, City, StateID_FK, ZipCode, CourtID, OfficerName, GroupID, Flag1, DP2, DPC, InsertionLoaderID)     
    Values(@Record_Date, @List_Date, @Courtdate, @Violation_Date, @Ticket_Number, @Ticket_Number, @Name_First, @Name_Last, '0000000000', @Address, @City, @StateID, @ZIP, 3068, @OfficerFullName, @GroupID, @AddressStatus, @DP2, @DPC, @LoaderID)  
  
    Set @CurrentRecordID = scope_identity()  
   END  
  Insert Into DallasTrafficTickets.dbo.tblTicketsViolationsArchive (TicketNumber_PK, ViolationNumber_PK, CauseNumber, CourtDate, TicketViolationDate, UpdatedDate, ViolationDescription, ViolationCode, FineAmount, BondAmount, CourtLocation, RecordID, ViolationStatusID, LoaderUpdateDate, InsertionLoaderId, GroupID,ViolType,offensestatute,plea,officeragency)  
  Values(@Ticket_Number, @ViolationID, @Cause_Number, @Courtdate, @Violation_Date, Getdate(), @Violation_Description, @Violation_Code, @FineAmount, 200, 3068, @CurrentRecordID, 116, getdate(), @LoaderID, @GroupID,@ViolType,@offensestatute,@plea,@officeragency)  
  Set @Flag_Inserted = 1  
 End  
 


GO
GRANT EXECUTE ON [dbo].[USP_HTS_LA_Insert_Tickets_ArlingtonArraignment_Excel]  TO dbr_webuser
Go
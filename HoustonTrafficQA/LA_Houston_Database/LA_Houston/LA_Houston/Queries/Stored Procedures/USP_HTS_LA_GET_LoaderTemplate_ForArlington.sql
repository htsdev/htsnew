﻿ USE [LA_Houston]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTS_LA_GET_LoaderTemplate_ForArlington]    Script Date: 05/31/2012 08:43:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[USP_HTS_LA_GET_LoaderTemplate_ForArlington]   
 @LoaderID INT  
AS 
 SELECT 'Row No' AS Col1, 
            'Ticket Number' AS Col22, 
			'Cause Number' AS Col222,
            'Last Name' AS Col3, 
            'First Name' AS Col4, 
            'Address Part I' AS Col8, 
            'Address Part II' AS Col9, 
            'City' AS Col10, 
            'State' AS Col11, 
            'Zip Code' AS Col12, 
            'DP2' AS Col13, 
            'DPC' AS Col14, 
            'Violation Description' AS Col15, 
            'Violation Date' AS Col16, 
            'Filed Date' AS Col17, 
            'Fine Amount' AS Col18, 
            'Officer FirstName' AS Col19,	
            'Officer LastName' AS Col20,	
            'Address Status' AS Col21,		
            'Reliable' AS Col22,
            'Case Status' AS Col23,
            'DOB' AS Col24,
            'Race' AS Col25,
            'Age' AS Col26,
            'Sex' AS Col27,
            'ViolType' AS Col28,
            'offensestatute' AS Col29,
            'plea' AS Col30,
            'officeragency' AS Col31,
            'AttorneyFirst' AS Col32,
            'AttorneyLast' AS Col33,
            'Courtdate' AS Col34,
            'FileId' AS Col35,
            'GroupID' AS Col36
            





GO
GRANT EXECUTE ON [dbo].[USP_HTS_LA_GET_LoaderTemplate_ForArlington] TO dbr_webuser
GO
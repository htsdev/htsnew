set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


CREATE PROCEDURE [dbo].[USP_HTS_LA_Update_ReportPath]
@ReportPath varchar(300),
@GeneratedReportPath varchar(300)	
AS
BEGIN
	update Tbl_HTS_LA_ConfigurationSetting set ReportPath=@ReportPath,GeneratedReportPath=@GeneratedReportPath where ConfigurationId=1
END


grant execute on dbo.USP_HTS_LA_Update_ReportPath to dbr_webuser
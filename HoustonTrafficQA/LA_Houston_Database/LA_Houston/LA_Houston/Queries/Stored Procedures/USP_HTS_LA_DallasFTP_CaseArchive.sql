﻿set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


/*
Author: Muhammad Adil Aleem
Business Logic: To Upload Dallas Events cases into Case Archive storage
Created date : April-15th-2008
Parameters: 
	@GroupID: Data file ID
*/
-- [USP_HTS_LA_DallasFTP_CaseArchive] 1
ALTER Procedure [dbo].[USP_HTS_LA_DallasFTP_CaseArchive]
@GroupID int
as

Insert Into LoaderFilesArchive.dbo.DallasFTP_CaseArchive
Select Getdate(), [Cause Number], [Ticket Number], [Violation Code], [Violation Description], 
(case [Court Status] when 'J' then 26 when 'T' then 103 else 0 end) as [Court Status], convert(datetime, [Court Date]) + convert(datetime, [Court Time]), 
[Court Time], [Court Room No], [Violation Date], [Fine Amount], [Officer Code], [Officer Name], 
DOCKSUFF, ENTDATE, ABTYPE, ABNUMB, ABAMT, ABDATE, CBCODE, CBNUMB, CBAMT, CBDATE, CBRFLAG, CBRAMT, CBRDATE, 
OCBRFLAG, OCBRAMT, OCBRDATE, CBFCODE, CBFNUMB, CBFAMT, CBFDATE, Attorney, RecordID, RowID, @GroupID
FROM         LoaderFilesArchive.dbo.DallasFTP_DataArchive
-- Adil 6392 08/17/2008 Including Insert_Mode 6
where Insert_Mode in (1, 2, 4, 6) and GroupID = @GroupID -- 1: Insert-able Cases & Defendants, 2: Existing Updateable Cases, 4: Existing Defendants, 6: Changed Attorney Cases

-- grant execute on dbo.USP_HTS_LA_DallasFTP_CaseArchive to dbr_webuser







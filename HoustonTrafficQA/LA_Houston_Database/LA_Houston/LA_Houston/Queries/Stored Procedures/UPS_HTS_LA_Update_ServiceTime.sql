set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


CREATE PROCEDURE [dbo].[UPS_HTS_LA_Update_ServiceTime]
@StartTime varchar(100),
@Endtime varchar(100)
AS
BEGIN	
	update Tbl_HTS_LA_ConfigurationSetting set ServiceStartTime=@StartTime,ServiceEndTime=@EndTime where ConfigurationId=1
END

 
grant execute on dbo.UPS_HTS_LA_Update_ServiceTime to dbr_webuser
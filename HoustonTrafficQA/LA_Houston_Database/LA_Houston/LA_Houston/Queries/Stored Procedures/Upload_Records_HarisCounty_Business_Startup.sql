﻿USE [LA_Houston]
GO
/****** Object:  StoredProcedure [dbo].[Upload_Records_HarisCounty_Business_Startup]    Script Date: 12/02/2011 05:24:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







/*
Author: Farrukh Iftikhar
Description: To Upload Haris County Business Startup Data From Loader Service
Type : Loader Service
Created date : 10/28/2011
Task ID : 9664
Parameters: 
	@FileNumber: Record file number
	@BusinessName: Business Name
	@BusinessAddress: Business Address  	
	@BusinessCity: Business City
	@BusinessState: Business State
	@BusinessZIP: Business zip code
	@BusinessAddressStatus: Address Status[y/d/s/n] of Business 	
	@BusinessDP2: Business DP2
	@BusinessDPC: Business DPC
	@OwnerName: Owner Name
	@OwnerAddress: Owner Address  	
	@OwnerCity: Owner City
	@OwnerState: Owner State
	@OwnerZIP: Owner zip code
	@OwnerAddressStatus: Address Status[y/d/s/n] of Owner 
	@OwnerDP2: Owner DP2
	@OwnerDPC: Owner DPC
	@BusinessStatus: Record Status Type	
	@ListDate: Record's Download Date	
	@FilmCode: Record's Code
	@GroupID: Data file ID		
	@LoaderID: Insure which loader is executing this record.		
	@FiledDate: Loader's Inserted Date	
	@NoMailFlagOwner: Indicates No Mail Flag on Owner Address
	@NoMailFlagBusiness: Indicates No Mail Flag on Business Address
	@CurrentRecordID: Indicates Current Record ID
	@Flag_Inserted: Indicates Inserted Flag
*/

CREATE PROCEDURE [dbo].[Upload_Records_HarisCounty_Business_Startup]
	@FileNumber AS VARCHAR(15),
	@BusinessName AS VARCHAR(100),
	@BusinessAddress AS VARCHAR(100),
	@BusinessCity AS VARCHAR(50),
	@BusinessState AS VARCHAR(2),
	@BusinessZIP AS VARCHAR(10),
	@BusinessAddressStatus AS VARCHAR(1),
	@BusinessDP2 AS VARCHAR(2),
	@BusinessDPC AS VARCHAR(1),
	@OwnerName AS VARCHAR(100),
	@OwnerAddress AS VARCHAR(100),
	@OwnerCity AS VARCHAR(50),
	@OwnerState AS VARCHAR(2),
	@OwnerZIP AS VARCHAR(10),
	@OwnerAddressStatus AS VARCHAR(1),
	@OwnerDP2 AS VARCHAR(2),
	@OwnerDPC AS VARCHAR(1),
	@BusinessStatus AS VARCHAR(20),
	@ListDate AS DATETIME,
	@FilmCode AS VARCHAR(15),
	@GroupID AS INT,
	@LoaderID AS INT,
	@FiledDate AS DATETIME,
	@NoMailFlagOwner AS VARCHAR(4),
	@NoMailFlagBusiness AS VARCHAR(4),
	@CurrentRecordID INT OUTPUT,
	@Flag_Inserted INT OUTPUT 
	AS
	SET NOCOUNT ON  
	
	DECLARE @IDFoundB AS INT
	DECLARE @IDFoundO AS INT 
	
	SET @IDFoundB = NULL
	SET @IDFoundO = NULL
	SET @Flag_Inserted = 0
	
	
	SET @IDFoundB = (
	        SELECT TOP 1 ISNULL(BusinessID, 0)
	        FROM   AssumedNames.dbo.Business B
	        WHERE  B.FileNumber = @FileNumber
	    )
	
	IF @IDFoundB IS NULL
	BEGIN
	    --Insertion of Business StartUp Details
	    INSERT INTO AssumedNames.dbo.Business
	      ( BusinessName, ADDRESS, City, STATE, Zip, RegistrationDate, NoMailflag, FileNumber, FilmCode, RecDate, DP2,DPC, FLAGS)
	    VALUES
	      (@BusinessName, @BusinessAddress, @BusinessCity, @BusinessState, @BusinessZIP, @ListDate, @NoMailFlagBusiness, @FileNumber, @FilmCode, GETDATE(), @BusinessDP2, @BusinessDPC , @BusinessAddressStatus)
	    
	    SET @CurrentRecordID = SCOPE_IDENTITY()
	    
	    --Insertion of Owner's Details
	    INSERT INTO AssumedNames.dbo.BusinessOwner
	      (BusinessID, OwnerName, OwnerAddress, OwnerCity, OwnerState, OwnerZip, NoMailflag, Flags, DPC, DP2)
	    VALUES
	      (@CurrentRecordID, @OwnerName, @OwnerAddress, @OwnerCity, @OwnerState, @OwnerZIP, @NoMailFlagOwner, @OwnerAddressStatus, @OwnerDPC, @OwnerDP2)
	    
	    SET @Flag_Inserted = 1
	END
	ELSE
	BEGIN
	    SET @IDFoundO = (
	            SELECT TOP 1 ISNULL(BusinessID, 0)
	            FROM   AssumedNames.dbo.BusinessOwner BO
	            WHERE  BO.BusinessID = @IDFoundB
	                   AND BO.OwnerName = @OwnerName
	        )
	    
	    IF @IDFoundO IS NULL
	    BEGIN
	        --Insertion of Owner's Details
	    INSERT INTO AssumedNames.dbo.BusinessOwner
	      (BusinessID, OwnerName, OwnerAddress, OwnerCity, OwnerState, OwnerZip, NoMailflag, Flags, DPC, DP2)
	    VALUES
	      (@IDFoundB, @OwnerName, @OwnerAddress, @OwnerCity, @OwnerState, @OwnerZIP, @NoMailFlagOwner, @OwnerAddressStatus, @OwnerDPC, @OwnerDP2)
	       	        
	        SET @Flag_Inserted = 1
	    END
	END






Go 
GRANT EXECUTE ON Upload_Records_HarisCounty_Business_Startup TO dbr_webuser
GO 



﻿USE [Crash]
GO
/****** Object:  StoredProcedure [dbo].[USP_Crash_DriverLoader]    Script Date: 09/02/2013 12:23:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sabir Khan Miani
-- Create date: 07/30/2013 TFS 11309
-- Description:	This procedure is used to insert crash cases.
-- =============================================
ALTER PROCEDURE [dbo].[USP_Crash_DriverLoader] 
@Crash_ID INT,
@Unit_Nbr VARCHAR(50),
@Prsn_Nbr VARCHAR(50),
@Prsn_Type_ID VARCHAR(50),
@Prsn_Occpnt_Pos_ID VARCHAR(50),
@Prsn_Injry_Sev_ID VARCHAR(50),
@Prsn_Age VARCHAR(50),
@Prsn_Ethnicity_ID VARCHAR(50),
@Prsn_Gndr_ID VARCHAR(50),
@Prsn_Ejct_ID VARCHAR(50),
@Prsn_Rest_ID VARCHAR(50),
@Prsn_Airbag_ID VARCHAR(50),
@Prsn_Helmet_ID VARCHAR(50),
@Prsn_Sol_Fl VARCHAR(50),
@Prsn_Alc_Spec_Type_ID VARCHAR(50),
@Prsn_Alc_Rslt_ID VARCHAR(50),
@Prsn_Bac_Test_Rslt VARCHAR(50),
@Prsn_Drg_Spec_Type_ID VARCHAR(50),
@Prsn_Drg_Rslt_ID VARCHAR(50),
@Drvr_Drg_Cat_1_ID VARCHAR(50),
@Prsn_Death_Time VARCHAR(50),
@Incap_Injry_Cnt VARCHAR(50),
@Nonincap_Injry_Cnt VARCHAR(50),
@Poss_Injry_Cnt VARCHAR(50),
@Non_Injry_Cnt VARCHAR(50),
@Unkn_Injry_Cnt VARCHAR(50),
@Tot_Injry_Cnt VARCHAR(50),
@Death_Cnt VARCHAR(50),
@Drvr_Lic_Type_ID VARCHAR(50),
@Drvr_Lic_State_ID VARCHAR(50),
@Drvr_Lic_Cls_ID VARCHAR(50),
@GroupID INT,
@Drvr_Zip VARCHAR(50),
@InsertionLoaderID INT,
@UpdationLoaderID INT
AS
BEGIN	
	SET NOCOUNT ON;
	if not exists(select d.Crash_ID from tbl_Crash c INNER JOIN tbl_Driver d ON d.Crash_ID = c.Crash_ID where d.Crash_ID=@Crash_ID AND d.Unit_Nbr = @Unit_Nbr)
    begin
		Insert into tbl_Driver(Crash_ID,Unit_Nbr,Prsn_Nbr,Prsn_Type_ID,Prsn_Occpnt_Pos_ID,Prsn_Injry_Sev_ID,Prsn_Age,Prsn_Ethnicity_ID,Prsn_Gndr_ID,Prsn_Ejct_ID,Prsn_Rest_ID,
					Prsn_Airbag_ID,Prsn_Helmet_ID,Prsn_Sol_Fl,Prsn_Alc_Spec_Type_ID,Prsn_Alc_Rslt_ID,Prsn_Bac_Test_Rslt,Prsn_Drg_Spec_Type_ID,Prsn_Drg_Rslt_ID,
					Drvr_Drg_Cat_1_ID,Prsn_Death_Time,Incap_Injry_Cnt,Nonincap_Injry_Cnt,Poss_Injry_Cnt,Non_Injry_Cnt,Unkn_Injry_Cnt,Tot_Injry_Cnt,Death_Cnt,
					Drvr_Lic_Type_ID,Drvr_Lic_State_ID,Drvr_Lic_Cls_ID,GroupID,Drvr_Zip,InsertionLoaderID,UpdationLoaderID)
		values(@Crash_ID,@Unit_Nbr,@Prsn_Nbr,@Prsn_Type_ID,@Prsn_Occpnt_Pos_ID,@Prsn_Injry_Sev_ID,@Prsn_Age,@Prsn_Ethnicity_ID,@Prsn_Gndr_ID,@Prsn_Ejct_ID,@Prsn_Rest_ID,
			   @Prsn_Airbag_ID,@Prsn_Helmet_ID,@Prsn_Sol_Fl,@Prsn_Alc_Spec_Type_ID,@Prsn_Alc_Rslt_ID,@Prsn_Bac_Test_Rslt,@Prsn_Drg_Spec_Type_ID,@Prsn_Drg_Rslt_ID,
			   @Drvr_Drg_Cat_1_ID,@Prsn_Death_Time,@Incap_Injry_Cnt,@Nonincap_Injry_Cnt,@Poss_Injry_Cnt,@Non_Injry_Cnt,@Unkn_Injry_Cnt,@Tot_Injry_Cnt,@Death_Cnt,
			   @Drvr_Lic_Type_ID,@Drvr_Lic_State_ID,@Drvr_Lic_Cls_ID,@GroupID,@Drvr_Zip,@InsertionLoaderID,@UpdationLoaderID)
	end	
END

﻿

/*
Author: Muhammad Adil Aleem
Business Logic: To Upload HCJP Entered, Events & Disposed Cases From Loader Service
Type : Loader Service
Created date : April-15th-2008
Parameters: 
	@Record_Date: Case record/insert date
	@List_Date: Court list date
	@Ticket_Number
	@Cause_Number
	@Name_First: Defendant's first name
	@Name_Last: Defendant's last name
	@Court_Date: Defendant's court date
	@Address: Defendant's home address
	@City: Defendant's home city
	@State: Defendant's home state
	@ZIP: Defendant's home zip code
	@DP2: Defendant's barcode part. return by zp4 webservice
	@DPC: Defendant's barcode part. return by zp4 webservice
	@Violation_Description: Violation's title of Defendant
	@GroupID: Data file ID
	@AddressStatus: Address status [y/d/s/n] of defendant
	@Flag_SameAsPrevious: insure wether this case belongs to previous one or not [It helps to cut-off researching the defendant profile]
	@PreviousRecordID: If this case belongs to previous case then previous case ID will be the value of @PreviousRecordID
	@PreviousTicketNumber: If this case belongs to previous case then previous case Ticket number will be the value of @PreviousTicketNumber
	@LoaderID: Insure which loader is executing this case.
	@CurrentRecordID: It returns current Case-ID/Record-ID to calling program
	@CurrentTicketNumber: It returns current case ticket number to calling program
	@Flag_Inserted: It returns status wether the record inserted or deleted
*/

--USP_HTS_LA_Insert_Tickets_HCJP '3/26/2008 1:44:01 PM','3/26/2008 1:44:01 PM','100000L831179','TR11X0328378','CALHOON','ROBERT','','B','M','02/25/1941','6920039','',"5'7",'','130','TN545.413','','10','DEPARTMENT OF PUBLIC SAFETY','','','','','','713','','','','','','','','','','','Appearance','04/18/2008','10:00 AM','','0.00','03/24/2008','254 LENA','','HOUSTON','TX','77022','0','0','NO SEAT BELT-DRIVER','820619','03/24/2008','209','Y','0','0','23','0','0'

ALTER PROCEDURE [dbo].[USP_HTS_LA_Insert_Tickets_HCJP]
@Record_Date datetime,  
@List_Date smalldatetime,
@Ticket_Number varchar(20),
@Cause_Number varchar(20),
@Name_First varchar(50),  
@Name_Last varchar(50),  
@Name_Middle varchar(10), 
@Race varchar(15),
@Gender varchar(10),
@DOB datetime,
@HomePhoneNumber varchar(30),
@WorkPhoneNumber varchar(30),
@Height varchar(10),
@Weight int, 
@FineAmount money,
@StatutoryCite1  varchar(50),
@StatutoryCite2  varchar(50),
@OfficerCode int,
@OfficerName Varchar(50), 
@ArrestingAgencyCode int,
@ArrestingAgencyName varchar(50),
@PleaCode  varchar(5),
@PleaDate  datetime,
@DispositionCode  varchar(5),
@DispositionDate  datetime,
@JudgmentDate  datetime,
@HomeAreaCode  varchar(5),
@WorkAddress1  varchar(50),
@WorkAddress2  varchar(50),
@WorkAddCity  varchar(50),
@WorkState varchar(10),
@WorkZIPCode varchar(10),
@WorkAreaCode  varchar(5),
@SPNNumber  varchar(10),
@SPNPointer  varchar(10),
@LastOpenWarrType varchar(20),
@LastOpenWarrDate datetime,
@CourtStatus varchar(50),
@CourtDate datetime,
@CourtTime datetime,
@CourtID Int,
@BondDate datetime,
@BondAmount money,
@FiledDate datetime,
@Address1 varchar(50),  
@Address2 varchar(50),  
@City varchar(35),  
@State Varchar(10),
@ZIP varchar(10),  
@DP2 varchar(2),
@DPC varchar(1),
@Violation_Description Varchar(200),
@Violation_Code Varchar(50),
@Violation_Date datetime,
@TotalPaid money,
@GroupID Int,
@AddressStatus Char(1),
@Flag_SameAsPrevious bit,
@PreviousRecordID int,
@LoaderID int,
@CurrentRecordID int output,
@Flag_Inserted int Output
AS
set nocount on  

Declare @StateID Int
Declare @WStateID Int
Declare @ViolationID int
Declare @IDFound int
Declare @TicketNum2 varchar(20)
Declare @ViolationStatusID int
Declare @ViolationStatusIDFound int
Declare @FiledDateFound DATETIME
Declare @CourtDateFound DATETIME
Declare @BondFlag int
Declare @OfficerCodeFound int
DECLARE @IsQuoted AS INT

SET @IsQuoted = 0
Declare @AddressFound varchar(50)
Declare @DOBFound datetime
Declare @PhoneNumberFound varchar(50)
Declare @JuvenileFlag int
Declare @AddressUpdate int

Set @Name_First = upper(left(@Name_First,20))
Set @Name_Last = upper(left(@Name_Last,20))
Set @Name_Middle = upper(@Name_Middle)
Set @CourtStatus = upper(ltrim(rtrim(@CourtStatus)))
Set @CourtDate = @CourtDate + @CourtTime
SET @Ticket_Number = REPLACE(@Ticket_Number, ' ', '')
SET @Cause_Number = REPLACE(@Cause_Number, ' ', '')
Set @JudgmentDate = (case when @JudgmentDate = '' then null else @JudgmentDate end)
Set @PleaDate = (case when @PleaDate = '' then null else @PleaDate end)
Set @DispositionDate = (case when @DispositionDate = '' then null else @DispositionDate end)
Set @LastOpenWarrDate = (case when @LastOpenWarrDate = '' then null else @LastOpenWarrDate end)
Set @Violation_Date = (case when @Violation_Date = '' then null else @Violation_Date end)

IF @Ticket_Number LIKE '000000%' -- Adil 6893 10/30/2009 removing 000000 from Ticket Number.
BEGIN
	SET @Ticket_Number = REPLACE(@Ticket_Number, '000000', '')
END

if len(@Name_Middle)>5
	Begin
		Set @Name_Middle = left(@Name_Middle,1)
	End

if left(@Gender,1) = 'M'
	begin
		Set @Gender = 'Male'
	end
else if left(@Gender,1) = 'F'
	begin
		Set @Gender = 'Female'
	end

Select @Race = case @Race 
when 'A' then 'Asian'
when 'B' then 'Black'
when 'H' then 'Hispanic'
when 'I' then 'Indian'
when 'M' then 'Mexican'
when 'O' then 'Other'
when 'U' then 'Unknown'
when 'W' then 'White'
when 'X' then 'Unknown'
else null
end

Set @HomePhoneNumber = @HomeAreaCode + @HomePhoneNumber
Set @WorkPhoneNumber = @WorkAreaCode + @WorkPhoneNumber

Set @Address1 = Replace(@Address1, '  ', ' ')
Set @Address2 = Replace(@Address2, '  ', ' ')
Set @WorkAddress1 = Replace(@WorkAddress1, '  ', ' ')
Set @WorkAddress2 = Replace(@WorkAddress2, '  ', ' ')
Set @Violation_Code = ltrim(rtrim(@Violation_Code))
Set @Violation_Description = ltrim(rtrim(@Violation_Description))
Set @Flag_Inserted = 0
Set @AddressUpdate = 0

Set @JuvenileFlag = 
(Case 
When Charindex('ISD',@ArrestingAgencyName) >= 1 or Charindex('School',@ArrestingAgencyName) >= 1 then 1
When @WorkAddress1 = 'Student' then 1
When Charindex('School',@Violation_Description) >= 1 or Charindex('Parent',@Violation_Description) >= 1 then 1
When @CourtStatus in ('TEEN COURT', 'JUVENILE APPEARANCE') then 1
Else 0
End)

if @CourtStatus = 'JUVENILE APPEARANCE'
Begin
	Set @CourtStatus = 'APPEARANCE'
End

Set @ViolationStatusID = (Select DBO.fn_HCJP_CourtStatus(@CourtStatus, @DispositionCode, @DispositionDate, @JudgmentDate, @ArrestingAgencyName, @PleaCode, @LastOpenWarrType, @LoaderID, @JuvenileFlag, @TotalPaid))

If Not Exists (Select CaseStatus From Tbl_HTS_LA_CaseStatus Where CaseStatus = @CourtStatus)
Begin
	Insert Into Tbl_HTS_LA_CaseStatus (CaseStatus) Values(@CourtStatus)
End

if len(isnull(@Ticket_Number,'')) < 5 -- Checking for invalid TicketNumber
Begin
	Set @Ticket_Number = @Cause_Number -- If found invalid then replace with CauseNumber
End


Set @Height= replace(@Height,'''','ft')


if isnull(@OfficerName,'') = ''
	Begin
		Set @OfficerName = 'N/A'
	End

Set @OfficerCodeFound = null
Select Top 1 @OfficerCodeFound = OfficerNumber_PK from TrafficTickets.dbo.tblOfficer where (isnull(FirstName,'') = @OfficerName)
if @OfficerCodeFound is null 
	Begin
		Set @OfficerCode = (select max(OfficerNumber_PK) + 1 from TrafficTickets.dbo.tblofficer)
		Insert Into TrafficTickets.dbo.tblOfficer (OfficerNumber_PK, OfficerType, FirstName, Comments) Values (@OfficerCode, 'N/A', @OfficerName, 'Inserted at ' + convert(varchar(25),getdate(),101))
	End
Else
	Begin
		Set @OfficerCode = @OfficerCodeFound
	End

Set @ViolationID = null
Select Top 1 @ViolationID = ViolationNumber_PK From TrafficTickets.dbo.TblViolations Where (Description = @Violation_Description and ViolationCode = @Violation_Code and CourtLoc = 3013 and ViolationType = 3)

If @ViolationID is null
	Begin
		Insert Into TrafficTickets.dbo.TblViolations (Description, Sequenceorder, IndexKey, IndexKeys, ChargeAmount, BondAmount, Violationtype, ChargeonYesflag, ShortDesc, ViolationCode, AdditionalPrice, CourtLoc)
		Values(@Violation_Description, null, 2, 100, @FineAmount, @BondAmount, 3, 1, 'None', @Violation_Code, 0, 3013)
		Set @ViolationID = scope_identity()
	End


Set @BondFlag = (Case When @ViolationStatusID = 186 OR @ViolationStatusID = 146 then 1 else 0 end) -- Adil 6714 10/09/2009 Now if DLQ then set bond flag true.

Set @StateID = (Select Top 1 StateID From TrafficTickets.dbo.TblState Where State = isnull(@State,''))
Set @WStateID = (Select Top 1 StateID From TrafficTickets.dbo.TblState Where State = isnull(@WorkState,''))

Set @IDFound = null
--************************************ Update DOB/File Date ******************************
if isNull(@FiledDate,'1/1/1900') <> '1/1/1900'
Begin
	Update TrafficTickets.dbo.tblticketsviolationsarchive Set FiledDate = @FiledDate
	Where CauseNumber = @Cause_Number
End
if isNull(@DOB,'1/1/1900') <> '1/1/1900'
Begin
	Update TrafficTickets.dbo.tblticketsarchive Set DOB = @DOB
	Where RecordID = 
	(Select top 1 RecordID from TrafficTickets.dbo.tblticketsviolationsarchive Where CauseNumber = @Cause_Number)
End
--************************************ Violate Promise To Appear *************************
IF @Violation_Description = 'Violate Promise To Appear'
Begin
	SELECT Top 1 @IDFound = tta.RecordID, @TicketNum2 = tta.TicketNumber, @AddressFound = tta.Address1, @DOBFound = tta.DOB, @PhoneNumberFound = tta.PhoneNumber, @IsQuoted = tta.Clientflag
			From TrafficTickets.dbo.tblTicketsArchive tta inner join TrafficTickets.dbo.tblTicketsViolationsArchive ttva
			on tta.recordid = ttva.recordid 
			Where tta.LastName = @Name_Last and tta.FirstName = @Name_First and tta.Address1 = @Address1 and tta.city = @City and tta.zipcode = @Zip and tta.stateid_fk = @StateID and datediff(day,isnull(ttva.LastOpenWarrDate,'1/1/1900'), isnull(@LastOpenWarrDate,'1/1/1900')) = 0 and @Violation_Description <> 'Violate Promise To Appear'
			AND ttva.CourtLocation = @CourtID -- Adil 6723 10/12/2009 checking specific court ID.
			AND TtVa.violationstatusid <> 80	-- Adil 7001 11/14/2009 Do not associate new cases with client, quote and disposed cases

	If @IDFound is null
		Begin
			SELECT Top 1 @IDFound = tta.RecordID, @TicketNum2 = tta.TicketNumber, @AddressFound = tta.Address1, @DOBFound = tta.DOB, @PhoneNumberFound = tta.PhoneNumber, @IsQuoted = tta.Clientflag
					From TrafficTickets.dbo.tblTicketsArchive tta inner join TrafficTickets.dbo.tblTicketsViolationsArchive ttva
					on tta.recordid = ttva.recordid 
					Where tta.LastName = @Name_Last and tta.FirstName = @Name_First and tta.Address1 = @Address1 and tta.city = @City and tta.zipcode = @Zip and tta.stateid_fk = @StateID and datediff(day,isnull(ttva.LastOpenWarrDate,'1/1/1900') , isnull(@LastOpenWarrDate,'1/1/1900')) = 0
					AND ttva.CourtLocation = @CourtID -- Adil 6723 10/12/2009 checking specific court ID.
					AND TtVa.violationstatusid <> 80	-- Adil 7001 11/14/2009 Do not associate new cases with client, quote and disposed cases
		End

	If @IDFound is not null
		Begin
			Set @CurrentRecordID = @IDFound
		End
End
--************************************ End Of Violate Promise To Appear Logic ***********

If @Flag_SameAsPrevious = 0
	Begin
		If @IDFound is null
			Begin
			SELECT Top 1 @IDFound = tta.RecordID, @TicketNum2 = tta.TicketNumber, @AddressFound = tta.Address1, @DOBFound = tta.DOB, @PhoneNumberFound = tta.PhoneNumber, @IsQuoted = tta.Clientflag
					From TrafficTickets.dbo.tblTicketsArchive tta inner join TrafficTickets.dbo.tblTicketsViolationsArchive ttva
					on tta.recordid = ttva.recordid Where tta.LastName = @Name_Last And tta.FirstName = @Name_First And datediff(day,isnull(tta.DOB,'1/1/1900') , isnull(@DOB,'1/1/1900')) = 0 And tta.Zipcode = @Zip And datediff(day,isnull(ttva.FiledDate,'1/1/1900') , isnull(@FiledDate,'1/1/1900')) = 0
					AND ttva.CourtLocation = @CourtID -- Adil 6723 10/12/2009 checking specific court ID.
					AND TtVa.violationstatusid <> 80	-- Adil 7001 11/14/2009 Do not associate new cases with client, quote and disposed cases
			End

		If @IDFound is null
			Begin
				If @LoaderID <> 25 -- No need to insert disposed cases
					Begin
						if Len(@Ticket_Number)<5
							Begin
								insert into TrafficTickets.dbo.tblIDGenerator(recdate) values(getdate())
								Set @Ticket_Number = 'ID' + convert(varchar(12),@@identity)
								Set @Cause_Number = @Ticket_Number
							End

						Insert Into TrafficTickets.dbo.tblTicketsArchive (RecLoadDate, ListDate, BondDate, BondFlag, TicketNumber, MidNumber, FirstName, LastName, 
						Initial, Race, Gender, DOB, PhoneNumber, WorkPhoneNumber, Height, Weight,
						WorkAddress1, WorkAddress2, WorkAddCity, WorkStateID_FK, WorkAddZIPCode, SPNNumber, SPNPointer, Address1, Address2, 
						City, StateID_FK, ZipCode, DP2, DPC, CourtID, GroupID, Flag1, InsertionLoaderID)

						Values(@Record_Date, @List_Date, @BondDate, @BondFlag, @Ticket_Number, @Ticket_Number, @Name_First, @Name_Last, 
						@Name_Middle, @Race, @Gender, @DOB, @HomePhoneNumber, @WorkPhoneNumber, @Height, @Weight,
						@WorkAddress1, @WorkAddress2, @WorkAddCity, @WStateID, @WorkZIPCode, @SPNNumber, @SPNPointer, @Address1, @Address2, 
						@City, @StateID, @ZIP, @DP2, @DPC, @CourtID, @GroupID, @AddressStatus, @LoaderID)
						
						Set @Flag_Inserted = 1

						Set @CurrentRecordID = scope_identity()
					End
			End
		Else
			Begin
				If isnull(@AddressFound,'') <> @Address1 and @Address1 <> ''
				Begin
					Set @AddressUpdate = 1
				End
				If @ViolationStatusID = 186 or @ViolationStatusID = 146 -- Here 186 = FTA Warrant & 146 = DLQ
					OR datediff(day,isnull(@DOBFound,'1/1/1900') , @DOB) <> 0 or @AddressUpdate = 1 or isnull(@PhoneNumberFound,'') <> @HomePhoneNumber
					Begin
						Update TrafficTickets.dbo.tblTicketsArchive
						Set BondDate = (Case When @ViolationStatusID = 186 then @LastOpenWarrDate When @ViolationStatusID = 146 Then GetDate() Else BondDate End),
							BondFlag = (Case When @ViolationStatusID = 186 or @ViolationStatusID = 146 then 1 Else BondFlag End),
							DOB = (Case When datediff(day,isnull(@DOBFound,'1/1/1900') , @DOB) <> 0 then @DOB Else DOB End),
							Address1 = (Case When @AddressUpdate = 1 then @Address1 Else Address1 End),
							Address2 = (Case When @AddressUpdate = 1 then @Address2 Else Address2 End),
							City = (Case When @AddressUpdate = 1 then @City Else City End),
							StateID_FK = (Case When @AddressUpdate = 1 then @StateID Else StateID_FK End),
							ZipCode = (Case When @AddressUpdate = 1 then @Zip Else ZipCode End),
							DP2 = (Case When @AddressUpdate = 1 then @DP2 Else DP2 End),
							DPC = (Case When @AddressUpdate = 1 then @DPC Else DPC End),
							Flag1 = (Case When @AddressUpdate = 1 then @AddressStatus Else Flag1 End),
							PhoneNumber = (Case When isnull(@PhoneNumberFound,'') <> @HomePhoneNumber and @HomePhoneNumber <> '' then @HomePhoneNumber Else PhoneNumber End),
							GroupID = @GroupID, UpdationLoaderID = @LoaderID Where RecordID = @IDFound
					End
				Set @CurrentRecordID = @IDFound
			End
	End
Else
	Begin
		Set @CurrentRecordID = @PreviousRecordID
	End

Set @IDFound = null
Set @TicketNum2 = null
Set @FiledDateFound = null
Set @ViolationStatusIDFound = null

Select top 1 @IDFound = Isnull(RowID,0), @TicketNum2 = TicketNumber_PK, @ViolationStatusIDFound = ViolationStatusID, @CourtDateFound = CourtDate, @FiledDateFound = FiledDate 
  From TrafficTickets.dbo.tblTicketsViolationsArchive Where CauseNumber = @Cause_Number
AND CourtLocation = @CourtID  -- Adil 6723 10/12/2009 checking specific court ID.
															-- Afaq 7380 3/23/2010 remove check (AND RecordID = @CurrentRecordID)

If @IDFound is null
	Begin
		If @LoaderID <> 25 -- No need to insert disposed cases
			BEGIN
				IF @IsQuoted = 1 -- Adil 7380 02/05/2010 if current client is quoted client and coming violation is different from existing then insert new ticket.
					Begin
						if Len(@Ticket_Number)<5
							Begin
								insert into TrafficTickets.dbo.tblIDGenerator(recdate) values(getdate())
								Set @Ticket_Number = 'ID' + convert(varchar(12),@@identity)
								Set @Cause_Number = @Ticket_Number
							End

						Insert Into TrafficTickets.dbo.tblTicketsArchive (RecLoadDate, ListDate, BondDate, BondFlag, TicketNumber, MidNumber, FirstName, LastName, 
						Initial, Race, Gender, DOB, PhoneNumber, WorkPhoneNumber, Height, Weight,
						WorkAddress1, WorkAddress2, WorkAddCity, WorkStateID_FK, WorkAddZIPCode, SPNNumber, SPNPointer, Address1, Address2, 
						City, StateID_FK, ZipCode, DP2, DPC, CourtID, GroupID, Flag1, InsertionLoaderID)

						Values(@Record_Date, @List_Date, @BondDate, @BondFlag, @Ticket_Number, @Ticket_Number, @Name_First, @Name_Last, 
						@Name_Middle, @Race, @Gender, @DOB, @HomePhoneNumber, @WorkPhoneNumber, @Height, @Weight,
						@WorkAddress1, @WorkAddress2, @WorkAddCity, @WStateID, @WorkZIPCode, @SPNNumber, @SPNPointer, @Address1, @Address2, 
						@City, @StateID, @ZIP, @DP2, @DPC, @CourtID, @GroupID, @AddressStatus, @LoaderID)
						
						Set @Flag_Inserted = 1

						Set @CurrentRecordID = scope_identity()
					END
					
				Insert Into TrafficTickets.dbo.tblTicketsViolationsArchive (RecordID, InsertDate, TicketNumber_Pk, ViolationNumber_PK, CauseNumber, FineAmount, PleaCode, PleaDate, JudgmentDate,
				LastOpenWarrType, LastOpenWarrDate, CourtLocation, ViolationStatusID, CourtDate, TicketOfficerNumber,ArrestingAgencyCode, ArrestingAgencyName,
				StatutoryCite1, StatutoryCite2, BondDate, BondAmount, UpdatedDate, LoaderUpdateDate,
				FiledDate, ViolationDescription, ViolationCode, TicketViolationDate, JuvenileFlag,
				GroupID, InsertionLoaderID)

				Values(@CurrentRecordID, Getdate(), @Ticket_Number, @ViolationID, @Cause_Number, @FineAmount, @PleaCode, @PleaDate, @JudgmentDate, 
				@LastOpenWarrType, @LastOpenWarrDate, @CourtID, @ViolationStatusID, isnull(@CourtDate, '1/1/1900'), @OfficerCode, @ArrestingAgencyCode, @ArrestingAgencyName, 
				@StatutoryCite1, @StatutoryCite2, (case When @ViolationStatusID = 186 Then @LastOpenWarrDate When @ViolationStatusID = 146 Then GetDate() Else Null End), (Case When @ViolationStatusID = 186 or @ViolationStatusID = 146 then @BondAmount Else 0 End), Getdate(), Getdate(), -- Here 186 = FTA Warrant & 146 = DLQ
				@FiledDate, @Violation_Description, @Violation_Code, @Violation_Date, @JuvenileFlag,
				@GroupID, @LoaderID)

				Set @Flag_Inserted = 1
			End
	End
Else
	BEGIN -- Adil 6231 07/27/2009 Business Rules changes to update the case.
		If (isnull(@FiledDateFound,'1/1/1900') < @FiledDate) OR (isnull(@CourtDateFound,'1/1/1900') < @CourtDate) or (isnull(@ViolationStatusIDFound,0) <> @ViolationStatusID) -- Update when Filed Date is changed OR Court Date is changed or violation status is changed
			Begin
				Update TrafficTickets.dbo.tblTicketsViolationsArchive Set DispositionCode = (Case When @ViolationStatusID = 80  then isnull(@DispositionCode,'N/A') Else DispositionCode End), -- Here 80 = Disposed
				DispositionDate = (Case When @ViolationStatusID = 80 then (Case When isnull(@DispositionDate,'1/1/1900') = '1/1/1900' Then (Getdate() - 1) Else @DispositionDate End) Else DispositionDate End),
				PleaCode = IsNull(@PleaCode,PleaCode), PleaDate = isnull(@PleaDate,PleaDate), FineAmount = isnull(@FineAmount,FineAmount),
				FiledDate = @FiledDate, BondDate = (Case When @ViolationStatusID = 186 then @LastOpenWarrDate When @ViolationStatusID = 146 Then GetDate() Else BondDate End), -- Here 186 = FTA Warrant & 146 = DLQ
				BondAmount = (Case When @ViolationStatusID = 186 or @ViolationStatusID = 146 then @BondAmount Else BondAmount End), ViolationStatusID = @ViolationStatusID, 
				CourtDate = (CASE WHEN @BondFlag = 1 THEN CourtDate ELSE isnull(@CourtDate, CourtDate) END), -- Adil 6518 09/03/2009 Whenever we update bond status using HCJP loader, please don't update court date.
				UpdatedDate = getdate(), LoaderUpdateDate = getdate(), GroupID = @GroupID, UpdationLoaderID = @LoaderID
				where RowID = @IDFound

				Set @Flag_Inserted = 2
			End
	End
﻿USE [Crash]
GO
/****** Object:  StoredProcedure [dbo].[USP_Crash_EndorsementLoader]    Script Date: 09/02/2013 12:24:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sabir Khan Miani
-- Create date: 07/30/2013 TFS 11309
-- Description:	This procedure is used to insert crash cases.
-- =============================================
ALTER PROCEDURE [dbo].[USP_Crash_EndorsementLoader] 
@Crash_ID INT,
@Unit_Nbr INT,
@Drvr_Lic_Endors_ID INT,
@GroupID INT,
@InsertionLoaderID INT,
@UpdationLoaderID INT
AS
BEGIN	
	SET NOCOUNT ON;
	if not exists(select e.Crash_ID from tbl_Crash c INNER JOIN tbl_Endorsements e ON e.Crash_ID = c.Crash_ID where e.Crash_ID=@Crash_ID AND e.Unit_Nbr = @Unit_Nbr)
    begin
		Insert into tbl_Endorsements(Crash_ID,Unit_Nbr,Drvr_Lic_Endors_ID,GroupID,InsertionLoaderID,UpdationLoaderID)
		values(@Crash_ID,@Unit_Nbr,@Drvr_Lic_Endors_ID,@GroupID,@InsertionLoaderID,@UpdationLoaderID)
	end	
END


﻿set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

/*
Author: Muhammad Adil Aleem
Business Logic: To update Dallas Events cases into database for verified addresses.
Created date : April-15th-2008
Parameters: 
	@RecordID: Unique ID for particular Ticket/Case.
	@Address1: Verified addresses.
	@City: Verified city.
	@State: Verified state.
	@ZipCode: Verified ZipCode.
	@AddressStatus: Verified address status.
	@DP2: Verified DP2.
	@DPC: Verified DPC.
*/
-- USP_HTS_LA_FTP_UpdateAddresses_For_Defendants 387
ALTER procedure [dbo].[USP_HTS_LA_FTP_UpdateAddresses_For_Defendants]
@RecordID int,
@Address1 varchar(500),
@City varchar(100),
@State varchar(50),
@ZipCode varchar(20),
@AddressStatus varchar(2),
@DP2 VARCHAR(2),-- Adil 6863 10/27/2009 Fixing the issue by changing datatype from int to varchar.
@DPC VARCHAR(1)
as

if len(isnull(@DP2,'')) = 1 -- Adil 6863 10/27/2009 Fixing the issue by changing datatype from int to varchar.
begin
	if isnull(@DP2,'') <> '0'
	begin
		Set @DP2 = '0' + @DP2
	end
END

Declare @StateID int

Set @StateID = (Select StateID from DallasTrafficTickets.dbo.tblState Where State = @State)

Update DallasTrafficTickets.dbo.tblTicketsArchive 
Set 
Address1 = @Address1,
City = @City,
StateID_FK = @StateID,
ZipCode = @ZipCode,
Flag1 = @AddressStatus,
DP2 = @DP2,
DPC = @DPC
Where RecordID = @RecordID


-- grant execute on dbo.USP_HTS_LA_FTP_UpdateAddresses_For_Defendants to dbr_webuser


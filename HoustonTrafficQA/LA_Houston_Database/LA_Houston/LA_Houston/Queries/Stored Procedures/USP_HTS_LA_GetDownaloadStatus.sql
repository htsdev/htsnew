set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go



CREATE PROCEDURE [dbo].[USP_HTS_LA_GetDownaloadStatus]
	@Path varchar(100)
AS
BEGIN
	
	SET NOCOUNT ON;
	select IsActive from Tbl_HTS_LA_LoaderSetting where DownloadPath = @Path
    
END


grant execute on dbo.USP_HTS_LA_GetDownaloadStatus to dbr_webuser
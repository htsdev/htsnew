﻿USE [LA_Houston]
GO
/****** Object:  StoredProcedure [dbo].[Upload_Tickets_Jersey_Village_Arraignment_Extended]    Script Date: 12/30/2011 05:30:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





/*
Author: Muhammad Adil
Description: To Upload Jersey Village Arraignment Cases From Loader Service
Type : Loader Service
Created date : 08/22/2011
Task ID :		  9715
Parameters: 
	@Record_Date: Case record/insert date
	@List_Date: Court list date
	@Name_Last: Defendant's last name
	@Name_First: Defendant's first name
	@Address: Defendant's home address
	@City: Defendant's home city
	@State: Defendant's home state
	@ZIP: Defendant's home zip code
	@DP2: Defendant's barcode part. return by zp4 webservice
	@DPC: Defendant's barcode part. return by zp4 webservice
	@Court_Date: Defendant's court date	
	@Violation_Description: Violation's title of Defendant
	@GroupID: Data file ID
	@AddressStatus: Address status [y/d/s/n] of defendant
	@Flag_SameAsPrevious: insure wether this case belongs to previous one or not [It helps to cut-off researching the defendant profile]
	@PreviousRecordID: If this case belongs to previous case then previous case ID will be the value of @PreviousRecordID
	@LoaderID: Insure which loader is executing this case.
	@CurrentRecordID: It returns current Case-ID/Record-ID to calling program
	@Flag_Inserted: It returns status wether the record inserted or deleted
*/

ALTER PROCEDURE [dbo].[Upload_Tickets_Jersey_Village_Arraignment_Extended]
@Ticket_Number as varchar(20),
@Cause_Number as varchar(20),
@Name_First as varchar(50),
@Name_Last as varchar(50),
@Name_Middle as varchar(50),
@Violation_Description as Varchar(2000),
@OfficerCode as varchar(20),
@OfficerLastName as varchar(50),
@OfficerFirstName as varchar(50),
@DLState as varchar(3),
@FineAmount as money,
@BondAmount as money,
@FiledDate as datetime,
@Flag_SameAsPrevious as bit,
@PreviousRecordID as int,
@LoaderID as int,
@GroupID as Int,
@CurrentRecordID int output,
@Flag_Inserted int Output
AS
set nocount on  

Declare @DLStateID as Int
Declare @RowIDFound as int
Declare @IDFound as int
DECLARE @OfficerID AS INT
DECLARE @ChargeAmount AS MONEY --Farrukh 9927 12/30/2011 Declare new variables for fine amount and violation table

Set @Name_First = left(@Name_First,20)
Set @Name_Last = left(@Name_Last,20)
Set @Name_Middle = left(@Name_Middle,1)
Set @Flag_Inserted = 0
Set @DLStateID = 0
SET @ChargeAmount = 0 --Farrukh 9927 12/30/2011 Set default variable values

Set @DLStateID = (Select Top 1 StateID From TrafficTickets.dbo.TblState Where State = @DLState)

SET @OfficerID = NULL
SELECT Top 1 @OfficerID = OfficerNumber_PK from TrafficTickets.dbo.tblOfficer where (ISNULL(FirstName,'') = @OfficerFirstName AND ISNULL(LastName,'') = @OfficerLastName)
IF @OfficerID IS NULL
	BEGIN
		SET @OfficerID = (SELECT MAX(OfficerNumber_PK) + 1 from TrafficTickets.dbo.tblofficer)
		INSERT INTO TrafficTickets.dbo.tblOfficer (OfficerNumber_PK, OfficerType, FirstName, LastName, Comments) Values (@OfficerID, 'N/A', @OfficerFirstName, @OfficerLastName, 'Inserted at ' + CONVERT(varchar(25),getdate(),101))
	END

Set @IDFound = null
Set @RowIDFound = NULL

--Farrukh 9927 01/06/2012 Pick Fine Amount from Fine Table
Select @ChargeAmount = (Select Top 1 total FROM LA_Houston.dbo.Jersey_FineSchedule Where ((VDesc = @Violation_Description) OR (LEFT(VDesc,30) = LEFT(@Violation_Description,30)) OR (LEFT(VDesc,20) = LEFT(@Violation_Description,20)) OR (LEFT(VDesc,8) = LEFT(@Violation_Description,8)) ))

IF @ChargeAmount IS NULL
BEGIN
	SET @ChargeAmount = 0
END

SELECT TOP 1 @IDFound = T.RecordID, @RowIDFound = Isnull(RowID,0)
FROM TrafficTickets.dbo.tblTicketsArchive T
INNER JOIN TrafficTickets.dbo.tblTicketsViolationsArchive TV
ON T.RecordID = TV.RecordID
WHERE (TV.TicketNumber_PK = @Ticket_Number AND TV.ViolationDescription = @Violation_Description AND TV.CourtLocation = 3034)

If @RowIDFound IS NOT NULL
	BEGIN
		UPDATE TrafficTickets.dbo.tblTicketsViolationsArchive
		SET TicketOfficerNumber = @OfficerID,
		--Farrukh 9927 12/30/2011 Update fine amount from fine amount table
		FineAmount = CASE WHEN @ChargeAmount = 0 THEN ISNULL(@FineAmount,0) ELSE @ChargeAmount END,
		BondAmount = @BondAmount,
		FiledDate = @FiledDate,
		CauseNumber = @Cause_Number,
		UpdationLoaderId = @LoaderID, updateddate = GETDATE(), LoaderUpdateDate = GETDATE(), GroupID = @GroupID 
		WHERE RowID = @RowIDFound

		UPDATE TrafficTickets.dbo.tblTicketsArchive
		SET DLStateID = Case When @DLStateID <> 0 then @DLStateID else DLStateID end,
		UpdationLoaderId = @LoaderID, GroupID = @GroupID 
		WHERE RecordID = @IDFound

		Set @Flag_Inserted = 2
	End


GO
GRANT EXECUTE ON Upload_Tickets_Jersey_Village_Arraignment_Extended TO dbr_webuser
GO


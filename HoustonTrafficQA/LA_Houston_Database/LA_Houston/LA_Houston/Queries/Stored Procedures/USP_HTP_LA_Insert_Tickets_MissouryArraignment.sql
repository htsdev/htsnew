﻿USE [LA_Houston]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTP_LA_Insert_Tickets_MissouryArraignment]    Script Date: 08/10/2012 15:49:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*
Author: Rab Nawaz Khan
Task : 10401
Date : 08/10/2012
Description: To Upload Missoury From Loader Service
Type : Loader Service
Created date : 08/10/2012
Parameters: 
	@Record_Date: Case record/insert date
	@List_Date: Court list date
	@Name_Last: Defendant's last name
	@Name_First: Defendant's first name
	@Address: Defendant's home address
	@City: Defendant's home city
	@State: Defendant's home state
	@ZIP: Defendant's home zip code
	@DP2: Defendant's barcode part. return by zp4 webservice
	@DPC: Defendant's barcode part. return by zp4 webservice
	@Court_Date: Defendant's court date	
	@Violation_Description: Violation's title of Defendant
	@GroupID: Data file ID
	@AddressStatus: Address status [y/d/s/n] of defendant
	@Flag_SameAsPrevious: insure wether this case belongs to previous one or not [It helps to cut-off researching the defendant profile]
	@PreviousRecordID: If this case belongs to previous case then previous case ID will be the value of @PreviousRecordID
	@LoaderID: Insure which loader is executing this case.
	@CurrentRecordID: It returns current Case-ID/Record-ID to calling program
	@Flag_Inserted: It returns status wether the record inserted or deleted
*/

ALTER PROCEDURE [dbo].[USP_HTP_LA_Insert_Tickets_MissouryArraignment]
@Record_Date datetime,  
@List_Date as smalldatetime,
@Ticket_Number as varchar(20),
@Name_First as varchar(50),  
@Name_Last as varchar(50),  
@Address as varchar(50),  
@City as varchar(35),  
@State as Varchar(10),
@ZIP as varchar(10),  
@DP2 varchar(2),
@DPC varchar(1),
@Court_Date as smalldatetime,
@Violation_Description as Varchar(2000),
@AddressStatus Char(1),
@Flag_SameAsPrevious as bit,
@PreviousRecordID as int,
@LoaderID as int,
@GroupID AS INT, 
@CurrentRecordID int output,
@Flag_Inserted int Output
AS
set nocount on  

Declare @StateID as Int
Declare @ViolationID as int
Declare @IDFound as INT
Declare @DLQUpdateDateFound as DATETIME
Declare @ViolationStatusFound as INT
DECLARE @AddressFound AS VARCHAR(200)
Declare @AddressStatusFound as char(1)
Declare @TicketNum2 as varchar(20)
Declare @ChargeAmount as MONEY
Declare @BondAmount as MONEY
DECLARE @ViolationStatus AS INT
DECLARE @IsQuoted AS INT

SET @IsQuoted = 0
Set @Name_First = LTRIM(RTRIM(upper(left(@Name_First,20))))
Set @Name_Last = LTRIM(RTRIM(upper(left(@Name_Last,20))))

Set @Address = Replace(@Address, '  ', ' ')
Set @Ticket_Number = ltrim(rtrim(@Ticket_Number))
Set @Violation_Description = ltrim(rtrim(@Violation_Description))
Set @Flag_Inserted = 0
Set @ChargeAmount = 0
Set @BondAmount = 0
If @ZIP = '00000'
	Begin
		Set @ZIP = ''
	END

SET @ViolationStatus = 116
Set @ViolationID = NULL
-- Rab Nawaz Khan 10428 09/04/2012 Charge Amount and Bond Amount Variables has been removed. . . 
Select Top 1 @ViolationID = ViolationNumber_PK From TrafficTickets.dbo.TblViolations Where (Description = @Violation_Description  and CourtLoc = 3068 and ViolationType = 12)

-- Rab Nawaz Khan 10428 09/04/2012 Getting the Charge Amount from the Missouri Fine Amount Tables on violation Descripion. . . 
SELECT @ChargeAmount = mf.TOTAL FROM LA_Houston.dbo.tblMissouriViolationFineAmounts mf WHERE mf.VIOLATION_DESCRIPTION = @Violation_Description

IF @ChargeAmount IS NULL
BEGIN
	SET @ChargeAmount = 0
END
-- END 10428
If @ViolationID is null
	BEGIN
		Insert Into TrafficTickets.dbo.TblViolations (Description, Sequenceorder, IndexKey, IndexKeys, ChargeAmount, BondAmount, Violationtype, ChargeonYesflag, ShortDesc, ViolationCode, AdditionalPrice, CourtLoc)
		Values(@Violation_Description, null, 2, 100, @ChargeAmount, 0, 12, 1, 'None', 'none', 0, 3068)
		Set @ViolationID = scope_identity()
	End

Set @StateID = (Select Top 1 StateID From TrafficTickets.dbo.TblState Where State = @State)

If @Flag_SameAsPrevious = 0
	BEGIN
		Set @IDFound = null

		SELECT Top 1 @IDFound = T.RecordID, @TicketNum2 = TicketNumber, @DLQUpdateDateFound = TV.DLQUpdateDate, @ViolationStatusFound = TV.violationstatusid, @IsQuoted = T.Clientflag, @AddressFound = ISNULL(T.Address1, 'N/A'), @AddressStatusFound = ISNULL(T.Flag1, 'N')
		From TrafficTickets.dbo.tblTicketsArchive T
		Inner Join TrafficTickets.dbo.tblTicketsViolationsArchive TV
		On T.RecordID = TV.RecordID
		Where 
		((TV.TicketNumber_PK = @Ticket_Number OR SUBSTRING (TV.TicketNumber_PK, 1 , LEN(TV.TicketNumber_PK) - 1) = SUBSTRING (@Ticket_Number, 1 , LEN(@Ticket_Number) - 1))
		OR
		(FirstName = @Name_First and LastName = @Name_Last and Address1 = @Address
		and ZipCode = @Zip and datediff(day,TV.CourtDate, @Court_Date) = 0 )) and TV.CourtLocation = 3068
		
		If @IDFound is null
			Begin
				if Len(@Ticket_Number)<1
					Begin
						insert into TrafficTickets.dbo.tblIDGenerator(recdate) values(getdate())
						select @Ticket_Number='MCM' + convert(varchar(12),scope_identity())
					END
					
				Insert Into TrafficTickets.dbo.tblTicketsArchive (RecLoadDate, ListDate, CourtDate, TicketNumber, MidNumber, FirstName, LastName, PhoneNumber, Address1, City, StateID_FK, ZipCode, CourtID, OfficerNumber_FK, OfficerName, Flag1, DP2, DPC, InsertionLoaderID,GroupID)   
				Values(@Record_Date, @List_Date, @Court_Date, @Ticket_Number, @Ticket_Number, @Name_First, @Name_Last, '0000000000', @Address, @City, @StateID, @ZIP, 3068, 962528, 'N/A', @AddressStatus, @DP2, @DPC, @LoaderID,@GroupID)

				Set @CurrentRecordID = scope_identity()
			End
		Else
			BEGIN
				Set @CurrentRecordID = @IDFound
				if Len(@Ticket_Number)<1
					Begin
						Set @Ticket_Number = @TicketNum2
					END
				IF @Address <> @AddressFound and @AddressStatus <> 'N' AND @AddressStatusFound = 'N'
				BEGIN
					UPDATE TrafficTickets.dbo.tblTicketsArchive
					SET Address1 = @Address, City = @City, StateID_FK = @StateID, ZipCode = @ZIP, DP2 = @DP2, DPC = @DPC, flag1 = @AddressStatus, UpdationLoaderID = @LoaderID, GroupID = @GroupID
					WHERE RecordID = @CurrentRecordID
					
					Set @Flag_Inserted = 2
				END
			End
	End
Else
	Begin
		Set @CurrentRecordID = @PreviousRecordID
	End

Set @IDFound = null
Set @IDFound = (Select top 1 RowID From TrafficTickets.dbo.tblTicketsViolationsArchive
			Where 
			(TicketNumber_PK = @Ticket_Number and CourtLocation = 3068 )) 

If @IDFound is null
	BEGIN
		IF @IsQuoted = 1 
			BEGIN
				if Len(@Ticket_Number)<1
					Begin
						insert into TrafficTickets.dbo.tblIDGenerator(recdate) values(getdate())
						select @Ticket_Number='MMC' + convert(varchar(12),scope_identity())
					END
					
				Insert Into TrafficTickets.dbo.tblTicketsArchive (RecLoadDate, ListDate, CourtDate, TicketNumber, MidNumber, FirstName, LastName, PhoneNumber, Address1, City, StateID_FK, ZipCode, CourtID, OfficerNumber_FK, OfficerName, Flag1, DP2, DPC, InsertionLoaderID,GroupID)   
				Values(@Record_Date, @List_Date, @Court_Date, @Ticket_Number, @Ticket_Number, @Name_First, @Name_Last, '0000000000', @Address, @City, @StateID, @ZIP, 3068, 962528, 'N/A', @AddressStatus, @DP2, @DPC, @LoaderID,@GroupID)

				Set @CurrentRecordID = scope_identity()
			END

		Insert Into TrafficTickets.dbo.tblTicketsViolationsArchive (TicketNumber_PK, ViolationNumber_PK, CauseNumber, CourtDate, InsertDate, ViolationDescription, ViolationCode, FineAmount, BondAmount, CourtLocation, RecordID, ViolationStatusID, DLQUpdateDate, InsertionLoaderId, GroupID)
		Values(@Ticket_Number, @ViolationID, @Ticket_Number, @Court_Date, Getdate(), @Violation_Description, 'None', @ChargeAmount, @BondAmount, 3068, @CurrentRecordID, @ViolationStatus, (CASE WHEN @ViolationStatus = 146 THEN GETDATE() ELSE @DLQUpdateDateFound END), @LoaderID, @GroupID)
		Set @Flag_Inserted = 1
	END



GO
GRANT EXECUTE ON dbo.USP_HTP_LA_Insert_Tickets_MissouryArraignment TO dbr_webuser
GO


﻿USE [LA_Houston]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_HTS_Get_TotalRecordsUploadedHCCC]
@LoaderID int,
@GroupId INT,
@TotalUploaded Numeric Output
AS  
SET @TotalUploaded = 0

SELECT @TotalUploaded = COUNT(DISTINCT tva.rowid) 
FROM TrafficTickets.dbo.tblTicketsArchive ta 
INNER JOIN TrafficTickets.dbo.tblTicketsViolationsArchive tva ON ta.RecordID = tva.RecordID
WHERE tva.InsertionLoaderID = @LoaderID AND ta.InsertionLoaderID = @LoaderID
AND DATEDIFF(DAY,ta.RecLoadDate,GETDATE())= 0 AND DATEDIFF(DAY,tva.updateddate,GETDATE()) = 0
AND ta.GroupID = @GroupId AND tva.GroupID = @GroupId

Return @TotalUploaded
GO
GRANT EXECUTE ON [dbo].[USP_HTS_Get_TotalRecordsUploadedHCCC] TO dbr_webuser

GO




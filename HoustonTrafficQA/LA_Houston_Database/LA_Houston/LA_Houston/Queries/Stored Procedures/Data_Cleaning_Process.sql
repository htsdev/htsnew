﻿ set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

/*
Author: Muhammad Adil Aleem
Business Logic : This Stored procedure is designed to clean up manually created profiles for discrepancies.
Parameters: 
	@Database: Database to clean up, 0 for Dallas, 1 for Houston
*/
Alter PROCEDURE [dbo].[Data_Cleaning_Process]
@Database INT = 0
AS
BEGIN
	IF @Database = 0 -- if Dallas
		BEGIN
			EXEC DallasTrafficTickets.dbo.USP_DTP_Update_DataCleaningJob
		END
END

GO
GRANT EXECUTE ON [dbo].[Data_Cleaning_Process] TO dbr_webuser
GO
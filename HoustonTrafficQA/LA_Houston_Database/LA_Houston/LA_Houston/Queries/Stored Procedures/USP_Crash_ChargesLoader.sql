﻿USE [Crash]
GO
/****** Object:  StoredProcedure [dbo].[USP_Crash_ChargesLoader]    Script Date: 09/02/2013 12:19:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sabir Khan Miani
-- Create date: 07/30/2013 TFS 11309
-- Description:	This procedure is used to insert crash cases.
-- =============================================
ALTER PROCEDURE [dbo].[USP_Crash_ChargesLoader] 
@Crash_ID INT,
@Unit_Nbr INT,
@Prsn_Nbr INT,
@Charge_Cat_ID INT,
@Charge VARCHAR(80),
@Citation_Nbr VARCHAR(20),
@GroupId INT,
@InsertionLoaderID INT,
@UpdationLoaderID INT
AS
BEGIN	
	SET NOCOUNT ON;
	if not exists(select ch.Crash_ID from tbl_Crash c INNER JOIN tbl_Charges ch ON ch.Crash_ID = c.Crash_ID where ch.Crash_ID=@Crash_ID AND ch.Charge = @Charge)
    begin
		Insert into tbl_Charges(Crash_ID,Unit_Nbr,Prsn_Nbr,Charge_Cat_ID,Charge,Citation_Nbr,GroupID,InsertionLoaderID,UpdationLoaderID)
		values(@Crash_ID,@Unit_Nbr,@Prsn_Nbr,@Charge_Cat_ID,@Charge,@Citation_Nbr,@GroupId,@InsertionLoaderID,@UpdationLoaderID)
	end	
END

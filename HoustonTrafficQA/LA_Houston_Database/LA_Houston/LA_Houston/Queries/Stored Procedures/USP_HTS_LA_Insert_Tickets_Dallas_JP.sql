﻿

/*
Author: Muhammad Adil Aleem
Description: To Upload Dallas JP Arraignment Cases From Loader Service
Type : Loader Service
Created date : N/A
Parameters:
	@Record_Date: Case record/insert date
	@List_Date: Court list date
	@Ticket_Number: Ticket Number
	@Cause_Number: Cause Number
	@Name_First: Defendant's first name
	@Name_Last: Defendant's last name
	@Name_Middle: Defendant's Middle name
	@Address: Defendant's home address
	@City: Defendant's home city
	@State: Defendant's home state
	@ZIP: Defendant's home zip code
	@DP2: Defendant's barcode part. return by zp4 webservice
	@DPC: Defendant's barcode part. return by zp4 webservice
	@PrecinctID: JP Court ID.
	@Violation_Date: Offense date.
	@Violation_Description: offense description
	@Violation_Code: Violation Code
	@Fine_Amount: Violation's fine amount.
	@BondAmount: Violation's bond amount.
	@Officer_Name: Officer name.
	@Complainant_Agency: Sheriff.
	@GroupID: Data file ID
	@AddressStatus: Address status [y/d/s/n] of defendant
	@Flag_SameAsPrevious: insure wether this case belongs to previous one or not [It helps to cut-off researching the defendant profile]
	@PreviousRecordID: If this case belongs to previous case then previous case ID will be the value of @PreviousRecordID
	@LoaderID: Insure which loader is executing this case.
	@CurrentRecordID: It returns current Case-ID/Record-ID to calling program
	@CurrentTicketNumber: It returns current case ticket number to calling program
	@Flag_Inserted: It returns status wether the record inserted or deleted
*/
ALTER PROCEDURE [dbo].[USP_HTS_LA_Insert_Tickets_Dallas_JP]
@Record_Date datetime,  
@List_Date as smalldatetime,
@Ticket_Number as varchar(20),
@Cause_Number as varchar(20),
@Name_First as varchar(50),  
@Name_Last as varchar(50),  
@Name_Middle as varchar(50),  
@Address as varchar(50),  
@City as varchar(35),  
@State as Varchar(10),
@ZIP as varchar(10),  
@DP2 varchar(2),
@DPC varchar(1),
@PrecinctID varchar(5),
@Violation_Date datetime,  
@Violation_Description as Varchar(2000),
@Violation_Code as Varchar(50),
@FineAmount as Money,
@BondAmount as Money,
@Officer_Name as varchar(50),
@Complainant_Agency as varchar(50),
@GroupID as Int,
@AddressStatus Char(1),
@Flag_SameAsPrevious as bit,
@PreviousRecordID as int,
@LoaderID as int,
@CurrentRecordID int output,
@Flag_Inserted int Output
AS
set nocount on  

Declare @StateID as Int
Declare @ViolationID as int
Declare @IDFound as int
Declare @TicketNum2 as varchar(20)
Declare @Court_Date as datetime
Declare @CourtID as int
DECLARE @IsQuoted AS INT

SET @IsQuoted = 0
Set @Name_First = left(@Name_First,20)
Set @Name_Last = left(@Name_Last,20)
Set @Name_Middle = left(@Name_Middle,1)
Set @Address = Replace(@Address, '  ', ' ')
Set @Flag_Inserted = 0
Set @List_Date = convert(datetime, convert(varchar(12), @List_Date, 101)) 
Set @CourtID = 0
Set @Court_Date = dateadd(day,14,@Violation_Date)

SET @Ticket_Number = REPLACE(@Ticket_Number, ' ', '')
SET @Cause_Number = REPLACE(@Cause_Number, ' ', '')

if datepart(dw,@Court_Date) = 7 -- If Saturday then make it Friday (Court date should be in working days)
	begin
		set @Court_Date = @Court_Date - 1
	end
else if datepart(dw,@Court_Date) = 1 -- If Sunday then make it Friday (Court date should be in working days)
	begin
		set @Court_Date = @Court_Date - 2
	end

Set @CourtID = (Select Top 1 CourtID from DallasTrafficTickets.dbo.TblCourts Where PrecinctID = @PrecinctID)

if len(@Ticket_Number) < 5
	Begin
		Set @Ticket_Number = @Cause_Number
	End

if @Cause_Number = ''
	Begin
		Set @Cause_Number = @Ticket_Number
	End

If @ZIP = '00000'
	Begin
		Set @ZIP = ''
	End
if @Violation_Code = ''
	Begin
		Set @Violation_Code = 'None'
	End

Set @ViolationID = null
Select Top 1 @ViolationID = ViolationNumber_PK, @Violation_Code = ViolationCode From DallasTrafficTickets.dbo.TblViolations Where (Description = @Violation_Description and CourtLoc = @CourtID and ViolationType = 9)

If @ViolationID is null
	Begin
		Insert Into DallasTrafficTickets.dbo.TblViolations (Description, Sequenceorder, IndexKey, IndexKeys, ChargeAmount, BondAmount, Violationtype, ChargeonYesflag, ShortDesc, ViolationCode, AdditionalPrice, Attorneyregularprice, Attorneybondprice, CourtLoc)
		Values(@Violation_Description, null, 2, 160, @FineAmount, @BondAmount, 9, 1, 'None', @Violation_Code, 0, 50, 95, @CourtID)
		Set @ViolationID = scope_identity()
	End

Set @StateID = (Select Top 1 StateID From DallasTrafficTickets.dbo.TblState Where State = @State)

If @Flag_SameAsPrevious = 0
	Begin
		Set @IDFound = null

		SELECT Top 1 @IDFound = T.RecordID, @TicketNum2 = TicketNumber, @IsQuoted = T.Clientflag
		From DallasTrafficTickets.dbo.tblTicketsArchive T
		Inner Join DallasTrafficTickets.dbo.tblTicketsViolationsArchive TV
		On T.RecordID = TV.RecordID
		Where ((TicketNumber = @Ticket_Number) OR (FirstName = @Name_First and LastName = @Name_Last and Address1 = @Address 
		and ZipCode = @Zip and datediff(day,ViolationDate, @Violation_Date) = 0)) and TV.CourtLocation = @CourtID
		AND TV.violationstatusid <> 80	-- Adil 7001 11/14/2009 Do not associate new cases with client, quote and disposed cases

		If @IDFound is null
			Begin
				if Len(@Ticket_Number)<5
					Begin
						insert into DallasTrafficTickets.dbo.tblIDGenerator(recdate) values(getdate())
						select @Ticket_Number='DC' + convert(varchar(12),@@identity)
					End

				Insert Into DallasTrafficTickets.dbo.tblTicketsArchive (RecLoadDate, ListDate, CourtDate, ViolationDate, TicketNumber, MidNumber, FirstName, LastName, Initial, PhoneNumber, Address1, City, StateID_FK, ZipCode, CourtID, OfficerName, GroupID, Flag1, DP2, DPC, InsertionLoaderID)   
				Values(@Record_Date, @List_Date, @Court_Date, @Violation_Date, @Ticket_Number, @Ticket_Number, @Name_First, @Name_Last, @Name_Middle, '0000000000', @Address, @City, @StateID, @ZIP, @CourtID, @Officer_Name, @GroupID, @AddressStatus, @DP2, @DPC, @LoaderID)

				Set @CurrentRecordID = scope_identity()
			End
		Else
			Begin
				Set @CurrentRecordID = @IDFound
				if Len(@Ticket_Number) < 5
					Begin
						Set @Ticket_Number = @TicketNum2
					End
			End
	End
Else
	Begin
		Set @CurrentRecordID = @PreviousRecordID
	End

-- Adil 4875 27/09/2008
If @CourtID = 3086 -- Dallas JP 3-1 reduce fine amount by 2$
	Begin
		Set @FineAmount = (case When @FineAmount > 2 then @FineAmount - 2 else @FineAmount End)
	End

If isnull(@Cause_Number,'') = ''
	Begin
		Set @Cause_Number = @Ticket_Number
	End

Set @IDFound = null
Set @IDFound = (Select top 1 RowID From DallasTrafficTickets.dbo.tblTicketsViolationsArchive
			Where 
			(CauseNumber = @Cause_Number) and CourtLocation = @CourtID ) --Afaq 7380 3/25/2010

If @IDFound is null
	BEGIN
		IF charindex('DCHD', isnull(@Complainant_Agency,'')) = 0 
		BEGIN
			IF @IsQuoted = 1 -- Adil 7380 02/05/2010 if current client is quoted client and coming violation is different from existing then insert new ticket.
			Begin
				if Len(@Ticket_Number)<5
					Begin
						insert into DallasTrafficTickets.dbo.tblIDGenerator(recdate) values(getdate())
						select @Ticket_Number='DC' + convert(varchar(12),@@identity)
					End

				Insert Into DallasTrafficTickets.dbo.tblTicketsArchive (RecLoadDate, ListDate, CourtDate, ViolationDate, TicketNumber, MidNumber, FirstName, LastName, Initial, PhoneNumber, Address1, City, StateID_FK, ZipCode, CourtID, OfficerName, GroupID, Flag1, DP2, DPC, InsertionLoaderID)   
				Values(@Record_Date, @List_Date, @Court_Date, @Violation_Date, @Ticket_Number, @Ticket_Number, @Name_First, @Name_Last, @Name_Middle, '0000000000', @Address, @City, @StateID, @ZIP, @CourtID, @Officer_Name, @GroupID, @AddressStatus, @DP2, @DPC, @LoaderID)

				Set @CurrentRecordID = scope_identity()
			END
			
			Insert Into DallasTrafficTickets.dbo.tblTicketsViolationsArchive (TicketNumber_PK, ViolationNumber_PK, CauseNumber, CourtDate, TicketViolationDate, UpdatedDate, ViolationDescription, ViolationCode, FineAmount, BondAmount, CourtLocation, RecordID, ViolationStatusID, LoaderUpdateDate, InsertionLoaderId, GroupID, Complainant_Agency)
			Values(@Ticket_Number, @ViolationID, @Cause_Number, @Court_Date, @Violation_Date, Getdate(), @Violation_Description, @Violation_Code, @FineAmount, @BondAmount, @CourtID, @CurrentRecordID, 116, getdate(), @LoaderID, @GroupID, @Complainant_Agency)
			Set @Flag_Inserted = 1	
		END
	End

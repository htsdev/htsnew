﻿set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go






/*
Author: Muhammad Adil
Description: To Upload SugarLand Arraignment Cases From Loader Service
Type : Loader Service
Created date : 08/22/2011
Parameters: 
	@Record_Date: Case record/insert date
	@List_Date: Court list date
	@Name_Last: Defendant's last name
	@Name_First: Defendant's first name
	@Address: Defendant's home address
	@City: Defendant's home city
	@State: Defendant's home state
	@ZIP: Defendant's home zip code
	@DP2: Defendant's barcode part. return by zp4 webservice
	@DPC: Defendant's barcode part. return by zp4 webservice
	@Court_Date: Defendant's court date	
	@Violation_Description: Violation's title of Defendant
	@GroupID: Data file ID
	@AddressStatus: Address status [y/d/s/n] of defendant
	@Flag_SameAsPrevious: insure wether this case belongs to previous one or not [It helps to cut-off researching the defendant profile]
	@PreviousRecordID: If this case belongs to previous case then previous case ID will be the value of @PreviousRecordID
	@LoaderID: Insure which loader is executing this case.
	@CurrentRecordID: It returns current Case-ID/Record-ID to calling program
	@Flag_Inserted: It returns status wether the record inserted or deleted
*/

ALTER PROCEDURE [dbo].[Upload_Tickets_Sugarland_Arraignment]
@Record_Date datetime,	
@List_Date as smalldatetime,
@Ticket_Number as varchar(20),
@Name_First as varchar(50),
@Name_Last as varchar(50),
@Name_Middle as varchar(50),
@Race as varchar(50),
@Sex as varchar(50),
@Address as varchar(50),
@City as varchar(35),
@State as Varchar(10),
@ZIP as varchar(10),
@DP2 varchar(2),
@DPC varchar(1),
@DOB smalldatetime,
@HomePhoneNumber varchar(20),
@WorkPhoneNumber varchar(20),
@Violation_Description as Varchar(2000),
@Violation_Date as smalldatetime,
@Court_Date DATETIME,
@Accident VARCHAR(10),
@Officer_FirstName as varchar(50),
@Officer_LastName as varchar(50),
@PostedSpeed as int,
@AlligedSpeed as int,
@GroupID as Int,
@AddressStatus VARCHAR(5),
@Flag_SameAsPrevious as bit,
@PreviousRecordID as int,
@LoaderID as int,
@CurrentRecordID int output,
@Flag_Inserted int Output
AS
set nocount on  

Declare @StateID as Int
Declare @ViolationID as int
Declare @IDFound as int
Declare @TicketNum2 as varchar(20)
Declare @ChargeAmount as money
Declare @BondAmount as money
Declare @Violation_Code as varchar(30)
DECLARE @IsQuoted AS INT
DECLARE @OfficerIDFound INT
DECLARE @OfficerCode VARCHAR(20)
DECLARE @AddressFound AS VARCHAR(200)
Declare @AddressStatusFound as char(1)
Declare @SpeedZone as int
Declare @SpeedCharged as int

SET @IsQuoted = 0
Set @Name_First = left(@Name_First,20)
Set @Name_Last = left(@Name_Last,20)
Set @Name_Middle = left(@Name_Middle,1)
Set @Address = Replace(@Address, '  ', ' ')
Set @Violation_Code = 'XD-none'
Set @Flag_Inserted = 0
Set @ChargeAmount = 0
Set @BondAmount = 0
Set @SpeedZone = 0
Set @SpeedCharged = 0


if len(isnull(@DP2,'')) = 1
begin
	if isnull(@DP2,'') <> '0'
	begin
		Set @DP2 = '0' + @DP2
	end
end

if datepart(dw,@Court_Date) = 7 -- If Saturday then make it Friday (Court date should be in working days)
	begin
		set @Court_Date = @Court_Date - 1
	end
else if datepart(dw,@Court_Date) = 1 -- If Sunday then make it Friday (Court date should be in working days)
	begin
		set @Court_Date = @Court_Date - 2
	end

IF @Sex = 'M'
BEGIN
    SET @Sex = 'Male'
END
ELSE 
IF @Sex = 'F'
BEGIN
    SET @Sex = 'Female'
END

SELECT @Race = 
		CASE @Race
            WHEN 'A' THEN 'Asian'
            WHEN 'B' THEN 'Black'
            WHEN 'H' THEN 'Hispanic'
            WHEN 'I' THEN 'Indian'
            WHEN 'M' THEN 'Mexican'
            WHEN 'O' THEN 'Other'
            WHEN 'U' THEN 'Unknown'
            WHEN 'W' THEN 'White'
            WHEN 'X' THEN 'Unknown'
            ELSE NULL
		END

if isnull(@Officer_FirstName,'') = ''
	Begin
		Set @Officer_FirstName = 'N/A'
	End

if isnull(@Officer_LastName,'') = ''
	BEGIN
		Set @Officer_LastName = 'N/A'
	END
	
Set @OfficerIDFound = null
Select Top 1 @OfficerIDFound = OfficerNumber_PK from TrafficTickets.dbo.tblOfficer where (isnull(FirstName,'') = @Officer_FirstName AND isnull(LastName,'') = @Officer_LastName)
if @OfficerIDFound is null
	Begin
		Set @OfficerCode = (select max(OfficerNumber_PK) + 1 from TrafficTickets.dbo.tblofficer)
		Insert Into TrafficTickets.dbo.tblOfficer (OfficerNumber_PK, OfficerType, FirstName, LastName, Comments) 
		Values (@OfficerCode, 'N/A', @Officer_FirstName, @Officer_LastName, 'Inserted at ' + convert(varchar(25),getdate(),101))
	End
Else
	Begin
		Set @OfficerCode = @OfficerIDFound
	End

-- Adil 9662 9/16/2011 Speeding charges applied
Set @SpeedZone = 
Case 
	When charindex('speed', @Violation_Description) > 0 and charindex('school', @Violation_Description) > 0 then 1
	When charindex('speed', @Violation_Description) > 0 and charindex('construction', @Violation_Description) > 0 then 2
	When charindex('speed', @Violation_Description) > 0 then 3
	Else 0
end

Select @ChargeAmount = total from SMC_FineSchedule Where VDesc = @Violation_Description

if @SpeedZone in (1, 2, 3) and @ChargeAmount = 0
	begin
		Set @SpeedCharged = @AlligedSpeed - @PostedSpeed
		if @SpeedCharged < 10
			begin
				set @SpeedCharged = 10
			end
		Select @ChargeAmount = total from SMC_FineSchedule Where SpeedZone = @SpeedZone and SpeedMPH = @SpeedCharged
	end

Set @ViolationID = null

Select Top 1 @ViolationID = ViolationNumber_PK, @Violation_Code = ViolationCode,
@ChargeAmount = Case when @ChargeAmount = 0 then isnull(ChargeAmount,0) else @ChargeAmount end, @BondAmount = isnull(BondAmount,0)
From TrafficTickets.dbo.TblViolations Where (Description = @Violation_Description and CourtLoc = 3060 and ViolationType = 11)

If @ViolationID is null
	BEGIN
		Insert Into TrafficTickets.dbo.TblViolations (Description, ViolationCode, ViolationType, CourtLoc, ChargeAmount, BondAmount) 
		Values(@Violation_Description, @Violation_Code, 11, 3060, @ChargeAmount, 0)
		Set @ViolationID = scope_identity()
	END
	
Set @StateID = (Select Top 1 StateID From TrafficTickets.dbo.TblState Where State = @State)

If @Flag_SameAsPrevious = 0
	BEGIN
		Set @IDFound = null

		SELECT TOP 1 @IDFound = T.RecordID, @TicketNum2 = ISNULL(TicketNumber, @Ticket_Number) , @IsQuoted = T.Clientflag, @AddressFound = ISNULL(T.Address1, 'N/A'), @AddressStatusFound = ISNULL(T.Flag1, 'N')
		FROM TrafficTickets.dbo.tblTicketsArchive T
		INNER JOIN TrafficTickets.dbo.tblTicketsViolationsArchive TV
		ON T.RecordID = TV.RecordID
		-- Ticket number look up criteria fixed.
		WHERE ((TV.TicketNumber_PK = @Ticket_Number )
		OR
		(FirstName = @Name_First and LastName = @Name_Last
		and datediff(day,TV.CourtDate, @Court_Date) = 0 )) and TV.CourtLocation = 3060

		If @IDFound IS NULL
			Begin
				if Len(@Ticket_Number)<2
					Begin
						insert into TrafficTickets.dbo.tblIDGenerator(recdate) values(getdate())
						select @Ticket_Number='SMC' + convert(varchar(12),scope_identity())
					End

				Insert Into TrafficTickets.dbo.tblTicketsArchive (RecLoadDate, ListDate, TicketNumber, MidNumber, FirstName, LastName, Initial, DOB, Gender, Race, PhoneNumber, WorkPhoneNumber, Address1, City, StateID_FK, ZipCode, CourtID, OfficerNumber_FK, OfficerName, GroupID, Flag1, DP2, DPC, InsertionLoaderID)
				Values(@Record_Date, @List_Date, @Ticket_Number, @Ticket_Number, @Name_First, @Name_Last, @Name_Middle, @DOB, @Sex, @Race, @HomePhoneNumber, @WorkPhoneNumber, @Address, @City, @StateID, @ZIP, 3060, @OfficerIDFound, @Officer_LastName + ', ' + @Officer_FirstName, @GroupID, @AddressStatus, @DP2, @DPC, @LoaderID)
				
				Set @CurrentRecordID = scope_identity()
			End
		Else
			Begin
				Set @CurrentRecordID = @IDFound
				if Len(@Ticket_Number)<1
					Begin
						Set @Ticket_Number = @TicketNum2
					End
				IF @Address <> @AddressFound and @AddressStatus <> 'N' AND @AddressStatusFound = 'N'
					BEGIN
						-- Update defendant address when not available in non-client DB.
						UPDATE TrafficTickets.dbo.tblTicketsArchive
						SET Address1 = @Address, City = @City, StateID_FK = @StateID, ZipCode = @ZIP, DP2 = @DP2, DPC = @DPC, flag1 = @AddressStatus, UpdationLoaderID = @LoaderID, GroupID = @GroupID
						WHERE RecordID = @CurrentRecordID
						
						Set @Flag_Inserted = 2
					END
			End
	End
Else
	Begin
		Set @CurrentRecordID = @PreviousRecordID
	End

Set @IDFound = null
Set @IDFound = (Select top 1 Isnull(RowID,0) From TrafficTickets.dbo.tblTicketsViolationsArchive TV
                WHERE 
				(left(tv.ViolationDescription, 5) = left(@Violation_Description, 5) AND TV.CourtLocation = 3060 AND TV.RecordID = @CurrentRecordID))

If @IDFound is NULL
	BEGIN
		IF @IsQuoted = 1
			Begin
				if Len(@Ticket_Number)<1
					Begin
						insert into DallasTrafficTickets.dbo.tblIDGenerator(recdate) values(getdate())
						select @Ticket_Number='SMC' + convert(varchar(12),scope_identity())
					End

				Insert Into TrafficTickets.dbo.tblTicketsArchive (RecLoadDate, ListDate, TicketNumber, MidNumber, FirstName, LastName, Initial, DOB, Gender, Race, PhoneNumber, WorkPhoneNumber, Address1, City, StateID_FK, ZipCode, CourtID, OfficerNumber_FK, OfficerName, GroupID, Flag1, DP2, DPC, InsertionLoaderID)
				Values(@Record_Date, @List_Date, @Ticket_Number, @Ticket_Number, @Name_First, @Name_Last, @Name_Middle, @DOB, @Sex, @Race, @HomePhoneNumber, @WorkPhoneNumber, @Address, @City, @StateID, @ZIP, 3060, @OfficerIDFound, @Officer_LastName + ', ' + @Officer_FirstName, @GroupID, @AddressStatus, @DP2, @DPC, @LoaderID)

				Set @CurrentRecordID = scope_identity()
			END
		Insert Into TrafficTickets.dbo.tblTicketsViolationsArchive (TicketNumber_PK, ViolationNumber_PK, CauseNumber, CourtDate, TicketOfficerNumber, InsertDate, UpdatedDate, TicketViolationDate, ViolationDescription, ViolationCode, FineAmount, BondAmount, CourtLocation, RecordID, ViolationStatusID, LoaderUpdateDate, InsertionLoaderId, GroupID)
		Values(@Ticket_Number, @ViolationID, @Ticket_Number, @Court_Date, @OfficerCode, Getdate(), Getdate(), @Violation_Date, @Violation_Description, @Violation_Code, @ChargeAmount, 0, 3060, @CurrentRecordID, 116, getdate(), @LoaderID, @GroupID)

		IF LTRIM (RTRIM (@Accident)) = 'YES'
		BEGIN
			INSERT INTO traffictickets.dbo.tblTicketsArchivePCCR (RecordID_FK, PCCRStatusID, InsertDate, EmployeeID, IsAccident, Comments)
			VALUES (@CurrentRecordID, 1, getdate(), 3992, 1, 'Quoted')
		END

		Set @Flag_Inserted = 1
	End

﻿USE [LA_Houston]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTS_LA_GET_LoaderScheduler]    Script Date: 12/18/2009 11:07:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[USP_HTS_LA_GET_LoaderScheduler]   
  
AS 
BEGIN  
Select * from tbl_HTS_LA_Loaders Where IsActive = 1 
and (datepart(hh,convert(datetime,isnull(executiontime,'1/1/1900')) )<= datepart(hh,getdate())  or executiontime is null)
Order By Execution_Order
--Select * from tbl_HTS_LA_Loaders Where IsActive = 1 Order By Execution_Order
END  
go
grant execute on [dbo].[USP_HTS_LA_GET_LoaderScheduler] to dbr_webuser
go




 
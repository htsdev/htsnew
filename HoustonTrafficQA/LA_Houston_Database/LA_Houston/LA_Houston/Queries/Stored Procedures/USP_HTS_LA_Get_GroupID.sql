﻿/*
Author: Muhammad Adil Aleem
Created date : Jul-17th-2008
Business Logic: To get unique ID for each data file.
Parameters: 
	@Load_Date : Date when data file is going to be upload. usually today's date.
	@List_Date : Date when data file came from court house. usually mentioned in data file if not then assume today's date.
	@SourceFileName : Data file name.
	@LoaderID : Data loader unique ID.
	@GroupID (Return) : This SP will return GroupID for each data file
*/

--USP_HTS_LA_Get_GroupID '07/09/2009', '07/09/2009', 'Event Extract (Daily)2009-06-20-03-13-4774.xls', 2, 0
ALTER PROCEDURE [dbo].[USP_HTS_LA_Get_GroupID]
@Load_Date As DateTime,
@List_Date As DateTime,
@SourceFileName as varchar(300),
@LoaderID int,
@GroupID Numeric Output
AS  
SET @GroupID = 0
-- Adil 6490 08/31/2009 Gettig existing GroupID if already exists
SELECT @GroupID = GroupID FROM Tbl_HTS_LA_Group WHERE 
SourceFileName = @SourceFileName AND LoaderID = @LoaderID AND DATEDIFF(DAY, @Load_Date, Load_Date) = 0

IF @GroupID = 0
BEGIN
	Insert Into Tbl_HTS_LA_Group (LoaderID, Load_Date, List_Date, SourceFileName) Values(@LoaderID, @Load_Date, @List_Date, @SourceFileName )
	Set @GroupID = Scope_Identity()	
END

Return @GroupID

--select * from Tbl_HTS_LA_Group order by groupid desc

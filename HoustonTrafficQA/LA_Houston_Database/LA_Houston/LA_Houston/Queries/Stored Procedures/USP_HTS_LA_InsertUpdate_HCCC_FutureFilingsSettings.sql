﻿USE [LA_Houston]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTS_LA_Insert_Tickets_HCCC_Daily]    Script Date: 12/17/2013 15:49:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Author:		Rab Nawaz Khan
Task ID:	11530
Date:		12/17/2013
Description: This Store Procedure is used to upload the HCCC Filing with Future settings information.
Parameters: 
	@Ticket_Number: Ticket Number
	@Name_Last: Defendant's last name
	@Name_First: Defendant's first name
	@Address: Defendant's home address
	@City: Defendant's home city
	@State: Defendant's home state
	@ZIP: Defendant's home zip code
	@DP2: Defendant's barcode part. return by zp4 webservice
	@DPC: Defendant's barcode part. return by zp4 webservice
	@Violation_Description: offense description
	@BondAmount: Violation Bond Amount.
	@Court_Number: Court Number.
	@SPN_Number: MID Number
	@CDI: CDI
	@CST: Court Status
	@DST: Disposition Status
	--added additional
	@Race ,
	@Gender 
	@Dob ,
	@Attorney_Name ,
	@ins ,
	@cad ,
	@cnc ,
	@rea ,
	@ChargeLevel ,
	@GroupID: Data file ID
	@AddressStatus: Address status [y/d/s/n] of defendant
	@Flag_SameAsPrevious: insure wether this case belongs to previous one or not [It helps to cut-off researching the defendant profile]
	@PreviousRecordID: If this case belongs to previous case then previous case ID will be the value of @PreviousRecordID
	@LoaderID: Insure which loader is executing this case.
	@CurrentRecordID: It returns current Case-ID/Record-ID to calling program
	@Flag_Inserted: It returns status wether the record inserted or deleted
*/

ALTER PROCEDURE [dbo].[USP_HTS_LA_InsertUpdate_HCCC_FutureFilingsSettings] 
	@Ticket_Number AS VARCHAR(20),
	@Name_Last AS VARCHAR(50), 
	@Name_First AS VARCHAR(50), 
	@Address AS VARCHAR(50), 
	@City AS VARCHAR(35), 
	@State AS VARCHAR(10),
	@ZIP AS VARCHAR(10), 
	@DP2 VARCHAR(2),
	@DPC VARCHAR(1),
	@Violation_Description AS VARCHAR(500),
	@BondAmount AS MONEY,
	@Court_Number VARCHAR(5),
	@Court_Date DATETIME,
	@SPN_Number VARCHAR(20),
	@CDI VARCHAR(3),
	@CST VARCHAR(3),
	@DST VARCHAR(3),
	@Race VARCHAR(10),
	@Gender VARCHAR(10),
	@Dob VARCHAR(100),
	@Attorney_Name VARCHAR(150),
	@ins VARCHAR(10),
	@cad VARCHAR(10),
	@cnc VARCHAR(10),
	@rea VARCHAR(10),
	@ChargeLevel VARCHAR(10),
	@GroupID AS INT,
	@AddressStatus CHAR(1),
	@Flag_SameAsPrevious AS BIT,
	@PreviousRecordID AS INT,
	@LoaderID AS INT,
	@BondsManFirstName VARCHAR(200),
	@BondsManLastName VARCHAR(200),
	@BondsManMiddleName VARCHAR(200),
	@BondsManSPN VARCHAR(100),
	@CurrentRecordID INT OUTPUT,
	@Flag_Inserted INT OUTPUT
	
	AS
	BEGIN
		SET NOCOUNT ON  
		
		DECLARE @StateID AS INT
		DECLARE @ViolationID AS INT
		DECLARE @IDFound AS INT
		DECLARE @VioIDFound AS INT
		DECLARE @TicketNum2 AS VARCHAR(20)
		DECLARE @IsQuoted AS INT
		DECLARE @chargLCode AS INT 
		DECLARE @ExistingRowId INT
		DECLARE @ExistingBondAmount MONEY
		DECLARE @ExistingCourt_Number VARCHAR(5)
		DECLARE @ExistingCourt_Date DATETIME	
		DECLARE @ExistingSPN_Number VARCHAR(20)
		DECLARE @ExistingAddress AS VARCHAR(50) 
		DECLARE @ExistingCity VARCHAR(35) 
		DECLARE @ExistingState INT
		DECLARE @ExistingZIP VARCHAR(10) 
		DECLARE @ExistingDP2 VARCHAR(2)
		DECLARE @ExistingDPC VARCHAR(1)
		DECLARE @ExistingViolation_Description VARCHAR(500)
		DECLARE @ExistingCDI VARCHAR(3)
		DECLARE @ExistingCST VARCHAR(3)
		DECLARE @ExistingDST VARCHAR(3)
		DECLARE @ExistingRace VARCHAR(10)
		DECLARE @ExistingGender VARCHAR(10)
		DECLARE @ExistingDob VARCHAR(100)
		DECLARE @ExistingAttorney_Name VARCHAR(150)
		DECLARE @Existingins VARCHAR(10)
		DECLARE @Existingcad VARCHAR(10)
		DECLARE @Existingcnc VARCHAR(10)
		DECLARE @Existingrea VARCHAR(10)
		DECLARE @ExistingChargeLevel INT
		DECLARE @ExistingGroupID INT
		DECLARE @ExistingAddressStatus CHAR(1)
		
		SET @IsQuoted = 0
		SET @SPN_Number = REPLACE(@SPN_Number, ',', '')
		SET @Name_First = LTRIM(RTRIM(UPPER(LEFT(@Name_First, 20))))
		SET @Name_Last = LTRIM(RTRIM(UPPER(LEFT(@Name_Last, 20))))
		SET @Ticket_Number = REPLACE(@Ticket_Number, ' ', '')
		
		IF EXISTS (SELECT tc.ID	FROM TrafficTickets.dbo.tblChargelevel tc WHERE tc.LevelCode = @ChargeLevel)
		BEGIN
			SET @chargLCode = (
				SELECT tc.ID
				FROM    TrafficTickets.dbo.tblChargelevel tc
				WHERE  tc.LevelCode = @ChargeLevel
			)	
		END 
		ELSE
			BEGIN
				SELECT @chargLCode = MAX(tc.ID) + 1 FROM TrafficTickets.dbo.tblChargelevel tc
				INSERT INTO TrafficTickets.dbo.tblChargelevel
				VALUES (@chargLCode, @ChargeLevel)
			END
		
		SET @Flag_Inserted = 2
		
		IF @ZIP = '00000'
		BEGIN
			SET @ZIP = ''
		END
		
		SET @ViolationID = NULL
		SELECT TOP 1 @ViolationID = ViolationNumber_PK
		FROM   TrafficTickets.dbo.TblViolations WITH(NOLOCK)
		WHERE  (
				   DESCRIPTION = @Violation_Description
				   AND ViolationType = 9
			   )
		
		IF @ViolationID IS NULL
		BEGIN
			INSERT INTO TrafficTickets.dbo.TblViolations
			  (DESCRIPTION,Sequenceorder,IndexKey,IndexKeys,ChargeAmount,BondAmount,Violationtype,ChargeonYesflag,ShortDesc,ViolationCode,
				AdditionalPrice,CourtLoc)
			VALUES (@Violation_Description,NULL,2,100,0,@BondAmount,9,1,'None','none',0,3037)
			SET @ViolationID = SCOPE_IDENTITY()
		END
		
		IF ISNULL(@State, '') = ''
		BEGIN
			SET @State = 'TX'
		END
		
		SET @StateID = (
				SELECT TOP 1 StateID 
				FROM   TrafficTickets.dbo.TblState WITH(NOLOCK)
				WHERE  STATE = @State
			)
	----------------------------------------------------------------------------------------------------------------------------------------------
	------------------------------------ Searching on Ticket Number Basis ------------------------------------------------------------------------
	----------------------------------------------------------------------------------------------------------------------------------------------	
	IF @Flag_SameAsPrevious = 0
	BEGIN
	    SET @IDFound = NULL
	    SELECT TOP 1 @IDFound = T.RecordID, @ExistingRowId = TV.RowID, @ExistingBondAmount = tv.BondAmount,
	           @TicketNum2 = T.TicketNumber, @ExistingCourt_Number = tv.Courtnumber, @ExistingCourt_Date = ISNULL(tv.CourtDate, '01/01/1900'),
	           @IsQuoted = ISNULL(T.Clientflag, 0), @ExistingSPN_Number = t.SPNNumber, @ExistingAddress = t.Address1, @ExistingCity = t.City,
	           @ExistingState = t.StateID_FK, @ExistingZIP = t.ZipCode, @ExistingDP2 = t.DP2, @ExistingDPC = t.DPC, 
	           @ExistingViolation_Description = tv.ViolationDescription, @ExistingCDI = tv.HCCC_CDI, @ExistingCST = tv.HCCC_CST, @ExistingDST = tv.HCCC_DST,
	           @ExistingRace = t.Race, @ExistingGender = t.Gender, @ExistingDob = t.DOB, @ExistingAttorney_Name = tv.AttorneyName,
	           @Existingins = tv.ins, @Existingcad = tv.cad, @Existingcnc = tv.cnc, @Existingrea = tv.rea, @ExistingChargeLevel = tv.ChargeLevel,
	           @ExistingAddressStatus = t.Flag1
	    FROM   TrafficTickets.dbo.tblTicketsArchive T WITH(NOLOCK)
	           INNER JOIN TrafficTickets.dbo.tblTicketsViolationsArchive TV WITH(NOLOCK)
	                ON  TV.RecordID = T.RecordID
	    WHERE TV.CauseNumber = @Ticket_Number -- Check the Cause number if exits and not disposed. . . 	           
	    AND tv.CourtLocation = 3037 -- only HCCC courts. . . 
	    AND TV.violationstatusid <> 80 
	    AND ISNULL(t.Clientflag, 0) = 0
	    
	    IF @IDFound IS NOT NULL -- if Record id found in above query the only update the fields else Search again on lastName, first Name and DOB basis. . .
	    BEGIN
	    	IF ((@ExistingBondAmount <> @BondAmount) OR (@ExistingCourt_Number <> @Court_Number) OR (DATEDIFF(DAY, @ExistingCourt_Date,@Court_Date) <> 0)
	    	   OR (@ExistingViolation_Description <> @Violation_Description) OR (@ExistingCDI <> @CDI) OR (@ExistingCST <> @CST) OR (@ExistingDST <> @DST)
	    		OR (@ExistingAttorney_Name <> @Attorney_Name) OR (@Existingins <> @ins) OR (@Existingcad <> @cad) OR (@Existingcnc <> @cnc)
	    		OR (@Existingrea <> @rea) OR (@ExistingChargeLevel <> @chargLCode))
	    		BEGIN
	    			UPDATE TrafficTickets.dbo.tblTicketsViolationsArchive
	    			SET BondAmount = @BondAmount, Courtnumber = @Court_Number, CourtDate = @Court_Date, ViolationDescription = @Violation_Description,
	    			HCCC_CDI = @CDI, HCCC_CST = @CST, HCCC_DST = @DST, AttorneyName = @Attorney_Name, ins = @ins, cad = @cad, cnc = @cnc,
	    			rea = @rea, ChargeLevel = @chargLCode, LoaderUpdateDate = GETDATE(), UpdationLoaderId = @LoaderID, updateddate = GETDATE(),
	    			GroupID = @GroupID, ViolationNumber_PK = @ViolationID
	    			WHERE RecordID = @IDFound AND RowID = @ExistingRowId 
	    			
	    			IF (DATEDIFF(DAY, @ExistingCourt_Date,@Court_Date) <> 0)
	    			BEGIN
	    				UPDATE TrafficTickets.dbo.tblTicketsViolationsArchive
	    				SET CourtDateUpdateDate = GETDATE(), CourtDate = @Court_Date
	    				WHERE RecordID = @IDFound AND RowID = @ExistingRowId 
	    			END
	    			SET @Flag_Inserted = 2
	    		END
	    	
	    	IF ((@ExistingSPN_Number <> @SPN_Number) OR (@ExistingAddress <> @Address) OR (@ExistingCity <> @City) OR (@ExistingState <> @StateID)
	    		OR (@ExistingZIP <> @ZIP) OR (@ExistingRace <> @Race) OR (@ExistingGender <> @Gender) OR (DateDiff(day,@ExistingDob,@Dob) <> 0)
	    	OR (@ExistingAddressStatus <> @AddressStatus))	
	    	BEGIN
	    		UPDATE TrafficTickets.dbo.tblTicketsArchive
	    		SET SPNNumber = @SPN_Number, Race = @Race, Gender = @Gender, DOB = @Dob, UpdationLoaderID = @LoaderID, GroupID = @GroupID
	    		WHERE RecordID = @IDFound
	    		IF ((@ExistingAddressStatus LIKE 'N') AND (@AddressStatus NOT LIKE 'N'))
	    		BEGIN
	    			UPDATE TrafficTickets.dbo.tblTicketsArchive
	    			SET Address1 = @Address, City = @City, StateID_FK = @StateID, ZipCode = @ZIP, DP2 = @DP2, UpdationLoaderID = @LoaderID,
	    			DPC = @DPC,GroupID = @GroupID
	    			WHERE RecordID = @IDFound
	    		END
	    	END	
			SET @CurrentRecordID = @IDFound
	    END
	    ELSE -- if Record Id not found the search on last name, first name and DOB. . . 
	    BEGIN
	    	SET @IDFound = NULL
	    	SELECT TOP 1 @IDFound = T.RecordID
			FROM TrafficTickets.dbo.tblTicketsArchive T WITH(NOLOCK) INNER JOIN TrafficTickets.dbo.tblTicketsViolationsArchive TV WITH(NOLOCK) 
				ON  TV.RecordID = T.RecordID
			WHERE t.LastName = @Name_Last AND t.FirstName = @Name_First AND DATEDIFF(DAY,t.DOB,@Dob) = 0 -- Check the Cause number if exits and not disposed. . . 	           
			AND tv.CourtLocation = 3037 -- only HCCC courts. . . 
			AND TV.violationstatusid <> 80 
			AND ISNULL(t.Clientflag, 0) = 0
			
			-- If Record Not found other than disposed then insert the new profile of client as per TFS 7001. . . 
			IF @IDFound IS NULL
			BEGIN
				INSERT INTO TrafficTickets.dbo.tblTicketsArchive
				  ( RecLoadDate, ListDate, CourtDate, ViolationDate, BondDate, TicketNumber, MidNumber, FirstName, LastName, PhoneNumber, Address1,
					City, StateID_FK, ZipCode, GroupID, Flag1, DP2, DPC, InsertionLoaderID, Race, Gender, DOB )
				VALUES
				  ( GETDATE(), GETDATE(), @Court_Date, '1/1/1900', '1/1/1900', @Ticket_Number, @SPN_Number, @Name_First, @Name_Last, '',
					@Address, @City, @StateID, @ZIP, @GroupID, @AddressStatus, @DP2, @DPC, @LoaderID, @Race, @Gender, @Dob)
		        
				SET @CurrentRecordID = SCOPE_IDENTITY()	
		        
				INSERT INTO TrafficTickets.dbo.tblTicketsViolationsArchive
				(TicketNumber_PK, ViolationNumber_PK, CauseNumber, CourtNumber, CourtLocation, CourtDate, TicketViolationDate, BondDate, 
				ViolationDescription, ViolationCode, FineAmount, BondAmount, RecordID, ViolationStatusID, HCCC_CDI,	HCCC_CST, HCCC_DST, InsertDate,
				 InsertionLoaderId, GroupID, AttorneyName, ins, cad, cnc, rea, ChargeLevel)
				VALUES
				  (@Ticket_Number,	@ViolationID, @Ticket_Number, @Court_Number,3037, @Court_Date, '1/1/1900','1/1/1900', @Violation_Description, 
				  'None', 0, @BondAmount, @CurrentRecordID, 116, @CDI, @CST, @DST, GETDATE(), @LoaderID, @GroupID, @Attorney_Name, @ins, @cad, 
				  @cnc, @rea,@chargLCode)
				SET @Flag_Inserted = 1
			END -- END OF @IDFound IS NULL
			ELSE 
			BEGIN
				INSERT INTO TrafficTickets.dbo.tblTicketsViolationsArchive
				(TicketNumber_PK, ViolationNumber_PK, CauseNumber, CourtNumber, CourtLocation, CourtDate, TicketViolationDate, BondDate, 
				ViolationDescription, ViolationCode, FineAmount, BondAmount, RecordID, ViolationStatusID, HCCC_CDI,	HCCC_CST, HCCC_DST, InsertDate,
				 InsertionLoaderId, GroupID, AttorneyName, ins, cad, cnc, rea, ChargeLevel)
				VALUES
				  (@Ticket_Number,	@ViolationID, @Ticket_Number, @Court_Number,3037, @Court_Date, '1/1/1900','1/1/1900', @Violation_Description, 
				  'None', 0, @BondAmount, @IDFound, 116, @CDI, @CST, @DST, GETDATE(), @LoaderID, @GroupID, @Attorney_Name, @ins, @cad, 
				  @cnc, @rea,@chargLCode)
				  
				SET @Flag_Inserted = 1
				SET @CurrentRecordID = @IDFound
			END
	    END -- END Else
	END -- END Of Same as Previous. . . 
	
	ELSE
	BEGIN
		SET @IDFound = NULL
			SELECT TOP 1 @IDFound = T.RecordID, @ExistingRowId = TV.RowID, @ExistingBondAmount = tv.BondAmount,
	           @TicketNum2 = T.TicketNumber, @ExistingCourt_Number = tv.Courtnumber, @ExistingCourt_Date = ISNULL(tv.CourtDate, '01/01/1900'),
	           @IsQuoted = ISNULL(T.Clientflag, 0), @ExistingSPN_Number = t.SPNNumber, @ExistingAddress = t.Address1, @ExistingCity = t.City,
	           @ExistingState = t.StateID_FK, @ExistingZIP = t.ZipCode, @ExistingDP2 = t.DP2, @ExistingDPC = t.DPC, 
	           @ExistingViolation_Description = tv.ViolationDescription, @ExistingCDI = tv.HCCC_CDI, @ExistingCST = tv.HCCC_CST, @ExistingDST = tv.HCCC_DST,
	           @ExistingRace = t.Race, @ExistingGender = t.Gender, @ExistingDob = t.DOB, @ExistingAttorney_Name = tv.AttorneyName,
	           @Existingins = tv.ins, @Existingcad = tv.cad, @Existingcnc = tv.cnc, @Existingrea = tv.rea, @ExistingChargeLevel = tv.ChargeLevel,
	           @ExistingAddressStatus = t.Flag1
			FROM   TrafficTickets.dbo.tblTicketsArchive T WITH(NOLOCK)
				   INNER JOIN TrafficTickets.dbo.tblTicketsViolationsArchive TV WITH(NOLOCK)
						ON  TV.RecordID = T.RecordID
			WHERE T.RecordID = @PreviousRecordID
			AND tv.violationstatusid <> 80
			AND ISNULL(t.Clientflag, 0) = 0
			AND tv.CauseNumber = @Ticket_Number
			
			IF @IDFound IS NOT NULL
			BEGIN
				IF ((@ExistingBondAmount <> @BondAmount) OR (@ExistingCourt_Number <> @Court_Number) OR (DATEDIFF(DAY, @ExistingCourt_Date,@Court_Date) <> 0)
	    	   OR (@ExistingViolation_Description <> @Violation_Description) OR (@ExistingCDI <> @CDI) OR (@ExistingCST <> @CST) OR (@ExistingDST <> @DST)
	    		OR (@ExistingAttorney_Name <> @Attorney_Name) OR (@Existingins <> @ins) OR (@Existingcad <> @cad) OR (@Existingcnc <> @cnc)
	    		OR (@Existingrea <> @rea) OR (@ExistingChargeLevel <> @chargLCode))
	    		BEGIN
	    			UPDATE TrafficTickets.dbo.tblTicketsViolationsArchive
	    			SET BondAmount = @BondAmount, Courtnumber = @Court_Number, CourtDate = @Court_Date, ViolationDescription = @Violation_Description,
	    			HCCC_CDI = @CDI, HCCC_CST = @CST, HCCC_DST = @DST, AttorneyName = @Attorney_Name, ins = @ins, cad = @cad, cnc = @cnc,
	    			rea = @rea, ChargeLevel = @chargLCode, LoaderUpdateDate = GETDATE(), UpdationLoaderId = @LoaderID, updateddate = GETDATE(),
	    			GroupID = @GroupID, ViolationNumber_PK = @ViolationID
	    			WHERE RecordID = @IDFound AND RowID = @ExistingRowId 
	    			
	    			IF (DATEDIFF(DAY, @ExistingCourt_Date,@Court_Date) <> 0)
	    			BEGIN
	    				UPDATE TrafficTickets.dbo.tblTicketsViolationsArchive
	    				SET CourtDateUpdateDate = GETDATE(), CourtDate = @Court_Date, LoaderUpdateDate = GETDATE(), 
	    				UpdationLoaderId = @LoaderID, GroupID = @GroupID
	    				WHERE RecordID = @IDFound AND RowID = @ExistingRowId 
	    			END
	    			SET @Flag_Inserted = 2
	    		END
				SET @CurrentRecordID = @IDFound
			END
			ELSE
			BEGIN 
					INSERT INTO TrafficTickets.dbo.tblTicketsViolationsArchive
					(TicketNumber_PK, ViolationNumber_PK, CauseNumber, CourtNumber, CourtLocation, CourtDate, TicketViolationDate, BondDate, 
					ViolationDescription, ViolationCode, FineAmount, BondAmount, RecordID, ViolationStatusID, HCCC_CDI,	HCCC_CST, HCCC_DST, InsertDate,
					 InsertionLoaderId, GroupID, AttorneyName, ins, cad, cnc, rea, ChargeLevel)
					VALUES
					  (@Ticket_Number,	@ViolationID, @Ticket_Number, @Court_Number,3037, @Court_Date, '1/1/1900','1/1/1900', @Violation_Description, 
					  'None', 0, @BondAmount, @PreviousRecordID, 116, @CDI, @CST, @DST, GETDATE(), @LoaderID, @GroupID, @Attorney_Name, @ins, @cad, 
					  @cnc, @rea,@chargLCode)
					  
					SET @Flag_Inserted = 1
					SET @CurrentRecordID = @PreviousRecordID
				END
			
		END -- End of main Else. . . 
END-- End of Prodecure body. . . 
GO
	GRANT EXECUTE ON USP_HTS_LA_InsertUpdate_HCCC_FutureFilingsSettings TO dbr_webuser
GO
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go



CREATE PROCEDURE [dbo].[USP_HTS_LA_Update_FTPSetting]
	@FTPDirectory varchar(300),
	@FTPUser	  varchar(300),
	@FTPPassword  varchar(100),
	@FTPCloseTime varchar(100)
AS
BEGIN
	
	SET NOCOUNT ON;

   update Tbl_HTS_LA_ConfigurationSetting set FTPDirectory =@FTPDirectory ,FTPUser=@FTPUser,FTPPassword=@FTPPassword,DownloadFromFTPUntil=@FTPCloseTime where ConfigurationID=1	
END



grant execute on dbo.USP_HTS_LA_Update_FTPSetting to dbr_webuser

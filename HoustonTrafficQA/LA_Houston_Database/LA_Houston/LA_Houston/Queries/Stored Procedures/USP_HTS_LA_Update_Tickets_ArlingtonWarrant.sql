﻿set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


/*
Author: Muhammad Adil Aleem
Description: To Upload Arlington Warrant Cases From Loader Service
Type : Loader Service
Created date : Sep-30th-2009
Parameters: 
	@Cause_Number: Cause Number
	@Bond_Amount: Violation's fine amount.
	@GroupID: Data file ID
	@LoaderID: Insure which loader is executing this case.
	@Flag_Inserted: It returns status wether the record inserted or deleted
	@AddressStatus: It returns current address status of the defendant
	@Address: It returns current address of the defendant
	@State: It returns current state of the defendant
*/

ALTER PROCEDURE [dbo].[USP_HTS_LA_Update_Tickets_ArlingtonWarrant]
	@Cause_Number AS VARCHAR(20),
	@BondAmount AS MONEY,
	@GroupID AS INT,
	@LoaderID AS INT,
	@Flag_Inserted INT OUTPUT,
	@AddressStatus VARCHAR(10) OUTPUT,
	@Address VARCHAR(500) OUTPUT,
	@State VARCHAR(10) OUTPUT
	AS
	SET NOCOUNT ON  
	
	DECLARE @IDFound AS INT
	DECLARE @Bond_Date_Found DATETIME
	
	SET @Flag_Inserted = 0
	SET @AddressStatus = ''
	SET @Address = ''
	SET @State = ''
	
	SELECT TOP 1 @IDFound = ttva.RowID,
	       @Bond_Date_Found = ISNULL(ttva.ViolationBondDate, '1/1/1900'),
	       @AddressStatus = ISNULL(tta.flag1, 'N'),
	       @Address = ISNULL(tta.Address1, ''),
	       @State = ISNULL(tta.StateID_FK, 45)
	FROM   DallasTrafficTickets.dbo.tblTicketsViolationsArchive ttva
	       INNER JOIN DallasTrafficTickets.dbo.tblTicketsArchive tta
	            ON  ttva.RecordID = tta.RecordID
	WHERE  (ttva.CauseNumber = @Cause_Number AND ttva.CourtLocation = 3068) -- 3068 = Arlington Municipal Court
	AND TTA.Clientflag = 0 AND TTVA.violationstatusid <> 80	-- Adil 7001 11/14/2009 Do not associate new cases with client, quote and disposed cases
	
	Set @State = (Select Top 1 State From DallasTrafficTickets.dbo.TblState WHERE StateID = @State) -- Getting state name
	
	IF @IDFound IS NOT NULL
	BEGIN
	    IF DATEDIFF(DAY, @Bond_Date_Found, GETDATE()) <> 0 -- If bond date not today's date then update the case
	    BEGIN
	        UPDATE DallasTrafficTickets.dbo.tblTicketsViolationsArchive
	        SET    ViolationBondDate = GETDATE(),
	               LoaderUpdateDate = GETDATE(),
	               BondAmount = @BondAmount,
	               UpdationLoaderId = @LoaderID,
	               GroupID = @GroupID
	        WHERE  RowID = @IDFound
	        
	        SET @Flag_Inserted = 2
	    END
	END


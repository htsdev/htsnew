set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go



CREATE PROCEDURE [dbo].[USP_HTS_LA_Update_LogFilePath]
@MessageLog varchar(300),
@ErrorLog varchar(300)
AS
BEGIN
	update Tbl_HTS_LA_ConfigurationSetting set MessageLogFilePath=@MessageLog,ErrorLogFilePath=@ErrorLog where ConfigurationId=1
END


grant execute on dbo.USP_HTS_LA_Update_LogFilePath to dbr_webuser
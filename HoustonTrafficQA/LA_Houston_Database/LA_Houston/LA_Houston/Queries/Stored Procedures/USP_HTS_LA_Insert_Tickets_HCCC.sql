﻿
/*
Author: Muhammad Adil Aleem
Description: To Upload HCCC Cases From Loader Service
Type : Loader Service
Created date : N/A
Parameters: 
	@Record_Date: Case record/insert date
	@List_Date: Court list date
	@Ticket_Number: Ticket Number
	@Name_Last: Defendant's last name
	@Name_First: Defendant's first name
	@Address: Defendant's home address
	@City: Defendant's home city
	@State: Defendant's home state
	@ZIP: Defendant's home zip code
	@DP2: Defendant's barcode part. return by zp4 webservice
	@DPC: Defendant's barcode part. return by zp4 webservice
	@Violation_Description: offense description
	@BondAmount: Violation Bond Amount.
	@Court_Number: Court Number.
	@SPN_Number: MID Number
	@CDI: CDI
	@CST: Court Status
	@DST: Disposition Status
	@GroupID: Data file ID
	@AddressStatus: Address status [y/d/s/n] of defendant
	@Flag_SameAsPrevious: insure wether this case belongs to previous one or not [It helps to cut-off researching the defendant profile]
	@PreviousRecordID: If this case belongs to previous case then previous case ID will be the value of @PreviousRecordID
	@PreviousTicketNumber: If this case belongs to previous case then previous record's Ticket Number will be passed
	@LoaderID: Insure which loader is executing this case.
	@CurrentRecordID: It returns current Case-ID/Record-ID to calling program
	@Flag_Inserted: It returns status wether the record inserted or deleted
*/
--grant exec on dbo.USP_HTS_LA_Insert_Tickets_HCCC to dbr_webuser
ALTER PROCEDURE [dbo].[USP_HTS_LA_Insert_Tickets_HCCC]
@Record_Date as datetime,  
@List_Date as datetime,
@Ticket_Number as varchar(20),
@Name_Last as varchar(50),  
@Name_First as varchar(50),  
@Address as varchar(50),  
@City as varchar(35),  
@State as Varchar(10),
@ZIP as varchar(10),  
@DP2 varchar(2),
@DPC varchar(1),
@Violation_Description as Varchar(500),
@BondAmount as Money,
@Court_Number varchar(5),
@SPN_Number varchar(20),
@CDI varchar(3),
@CST varchar(3),
@DST varchar(3),
@GroupID as Int,
@AddressStatus Char(1),
@Flag_SameAsPrevious as bit,
@PreviousRecordID as int,
@PreviousTicketNumber as varchar(20),
@LoaderID as int,
@CurrentRecordID int output,
@Flag_Inserted int Output
AS
set nocount on  

Declare @StateID as Int
Declare @ViolationID as int
Declare @IDFound as int
Declare @TicketNum2 as varchar(20)
DECLARE @IsQuoted AS INT

SET @IsQuoted = 0
Set @SPN_Number = Replace(@SPN_Number,',','')
Set @Name_First = LTRIM(RTRIM(upper(left(@Name_First,20))))
Set @Name_Last = LTRIM(RTRIM(upper(left(@Name_Last,20))))
SET @Ticket_Number = REPLACE(@Ticket_Number, ' ', '')

Set @Flag_Inserted = 0

If @ZIP = '00000'
	Begin
		Set @ZIP = ''
	End


Set @ViolationID = null
Select Top 1 @ViolationID = ViolationNumber_PK From TrafficTickets.dbo.TblViolations Where (ltrim(rtrim(Description)) = ltrim(rtrim(@Violation_Description)) and ViolationType = 9)

If @ViolationID is null
	Begin
		Insert Into TrafficTickets.dbo.TblViolations (Description, Sequenceorder, IndexKey, IndexKeys, ChargeAmount, BondAmount, Violationtype, ChargeonYesflag, ShortDesc, ViolationCode, AdditionalPrice, CourtLoc)
		Values(@Violation_Description, null, 2, 100, 0, @BondAmount, 9, 1, 'None', 'none', 0, 3037)
		Set @ViolationID = scope_identity()
	End

if ltrim(rtrim(isnull(@State, ''))) = ''
	Begin
		Set @State = 'TX'
	End

Set @StateID = (Select Top 1 StateID From TrafficTickets.dbo.TblState Where State = @State)

If @Flag_SameAsPrevious = 0
	Begin
		Set @IDFound = NULL
		
		SELECT Top 1 @IDFound = T.RecordID, @TicketNum2 = T.TicketNumber, @IsQuoted = T.Clientflag
		From TrafficTickets.dbo.tblTicketsArchive T
		INNER JOIN TrafficTickets.dbo.tblTicketsViolationsArchive TV
		ON TV.RecordID = T.RecordID
		Where T.TicketNumber = @Ticket_Number
		AND TV.violationstatusid <> 80	-- Adil 7001 11/14/2009 Do not associate new cases with client, quote and disposed cases

		If @IDFound is null
			Begin
				if Len(@Ticket_Number)<5
					Begin
						insert into TrafficTickets.dbo.tblIDGenerator(recdate) values(getdate())
						select @Ticket_Number='HC' + convert(varchar(12),@@identity)
					End

				Insert Into TrafficTickets.dbo.tblTicketsArchive (RecLoadDate, ListDate, CourtDate, ViolationDate, BondDate, TicketNumber, MidNumber, FirstName, LastName, PhoneNumber, Address1, City, StateID_FK, ZipCode, GroupID, Flag1, DP2, DPC, InsertionLoaderID)   
				Values(@Record_Date, @List_Date, '1/1/1900', '1/1/1900', '1/1/1900', @Ticket_Number, @SPN_Number, @Name_First, @Name_Last, '', @Address, @City, @StateID, @ZIP, @GroupID, @AddressStatus, @DP2, @DPC, @LoaderID)

				Set @CurrentRecordID = scope_identity()
			End
		Else
			Begin
				Set @CurrentRecordID = @IDFound
				if Len(@Ticket_Number) < 5
					Begin
						Set @Ticket_Number = @TicketNum2
					End
			End
	End
Else
	Begin
		Set @CurrentRecordID = @PreviousRecordID
		Set @TicketNum2 = isnull(@PreviousTicketNumber,'')
	End

Set @IDFound = null
Set @IDFound = (Select top 1 RowID From TrafficTickets.dbo.tblTicketsViolationsArchive
			Where 
			(isnull(CauseNumber,'') = @Ticket_Number )) --Afaq 7382 3/25/2010 remove check (AND RecordID = @CurrentRecordID)

If @IDFound is null
	BEGIN
		IF @IsQuoted = 1 -- Adil 7380 02/05/2010 if current client is quoted client and coming violation is different from existing then insert new ticket.
			Begin
				if Len(@Ticket_Number)<5
					Begin
						insert into TrafficTickets.dbo.tblIDGenerator(recdate) values(getdate())
						select @Ticket_Number='HC' + convert(varchar(12),@@identity)
					End

				Insert Into TrafficTickets.dbo.tblTicketsArchive (RecLoadDate, ListDate, CourtDate, ViolationDate, BondDate, TicketNumber, MidNumber, FirstName, LastName, PhoneNumber, Address1, City, StateID_FK, ZipCode, GroupID, Flag1, DP2, DPC, InsertionLoaderID)   
				Values(@Record_Date, @List_Date, '1/1/1900', '1/1/1900', '1/1/1900', @Ticket_Number, @SPN_Number, @Name_First, @Name_Last, '', @Address, @City, @StateID, @ZIP, @GroupID, @AddressStatus, @DP2, @DPC, @LoaderID)

				Set @CurrentRecordID = scope_identity()
			END
		Insert Into TrafficTickets.dbo.tblTicketsViolationsArchive (TicketNumber_PK, ViolationNumber_PK, CauseNumber, CourtNumber, CourtLocation, CourtDate, TicketViolationDate, BondDate, UpdatedDate, ViolationDescription, ViolationCode, FineAmount, BondAmount, RecordID, ViolationStatusID, HCCC_CDI, HCCC_CST, HCCC_DST, LoaderUpdateDate, InsertionLoaderId, GroupID)
		Values((Case When isnull(@TicketNum2,'') <> '' Then @TicketNum2 Else @Ticket_Number End), @ViolationID, @Ticket_Number, @Court_Number, 3037, '1/1/1900', '1/1/1900', '1/1/1900', Getdate(), @Violation_Description, 'None', 0, @BondAmount, @CurrentRecordID, 116, @CDI, @CST, @DST, getdate(), @LoaderID, @GroupID)
		Set @Flag_Inserted = 1
	End
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using lntechNew.Components;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System.Data;

namespace HTPUnitTests
{

    /// <summary>
    ///This is a test class for OpenServiceTickets and is intended
    ///to contain all Its Unit Tests
    ///</summary>
    [TestClass()]
    public class OpenServiceTicketsTest
    {


        /// <summary>
        ///This is a Region for Method GetRecords which  returns DataTable.
        ///This Region is intended to contain all its Unit Tests.
        ///</summary>
        ///

        #region GetRecords

        /// <summary>
        ///This Test method will Check if the DataTable is Null or Not . 
        ///</summary>
        [TestMethod()]
        public void GetRecords_Null()
        {
            OpenServiceTickets target = new OpenServiceTickets();
           
            string StartDate = "04/02/2009";
            string EndDate = "04/02/2009";
            int  EmpID = 3991;
            string TicketType = "1";
            string ShowOnlyIncomplete = "0";           
            int CaseTypeId = -1;            
            int ShowAllfollowup = 1;
                         
           DataTable actual;
           actual = target.GetRecords(StartDate, EndDate, EmpID, TicketType, ShowOnlyIncomplete, CaseTypeId, ShowAllfollowup);
           Assert.IsNotNull(actual);
        }


        /// <summary>
        ///This Test method will Check Type of the DataTable.
        ///</summary>
        [TestMethod()]
        public void GetRecords_Type()
        {
            OpenServiceTickets target = new OpenServiceTickets();
            
            string StartDate = "04/02/2009";
            string EndDate = "04/02/2009";
            int EmpID = 3991;
            string TicketType = "1";
            string ShowOnlyIncomplete = "0";           
            int CaseTypeId = -1;         
            int ShowAllfollowup = 1;
            DataTable ds = new DataTable();
            DataTable expected = ds;
            DataTable actual;

            actual = target.GetRecords(StartDate, EndDate, EmpID, TicketType, ShowOnlyIncomplete, CaseTypeId,ShowAllfollowup);
            Assert.AreEqual(expected.GetType(), actual.GetType());
        }

        /// <summary>
        /// This Test method will Check No of Rows Return by dataTable is equal or greater than Zero . 
        /// </summary>
        /// 
        [TestMethod()]
        public void GetRecords_Rows()
        {
            OpenServiceTickets target = new OpenServiceTickets();

            string StartDate = "04/02/2009";
            string EndDate = "04/02/2009";
            int EmpID = 3991;
            string TicketType = "1";
            string ShowOnlyIncomplete = "0";           
            int CaseTypeId = -1;          
            int ShowAllfollowup = 1;

            bool expected = false, actual = false;
            int expnumber = 0;
            expected = (expnumber >= 0) ? true : false;
            actual = target.GetRecords(StartDate, EndDate, EmpID, TicketType, ShowOnlyIncomplete,CaseTypeId, ShowAllfollowup).Rows.Count >= 0 ? true : false;
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// This Test method will Check that the No of columns are equal to 26  . 
        /// </summary>
        [TestMethod()]
        public void GetRecords_NoOfCol()
        {
            OpenServiceTickets target = new OpenServiceTickets();

            string StartDate = "04/02/2009";
            string EndDate = "04/02/2009";
            int EmpID = 3991;
            string TicketType = "1";
            string ShowOnlyIncomplete = "0";       
            int CaseTypeId = -1;         
            int ShowAllfollowup = 1;
            int expected = 0, actual = 0;
            expected = 30;
            actual = target.GetRecords(StartDate, EndDate, EmpID, TicketType, ShowOnlyIncomplete, CaseTypeId,ShowAllfollowup).Columns.Count;
            Assert.AreEqual(expected, actual);
        }



        #endregion


        /// <summary>
        ///This method test the return type of AddServiceTicket Method.
        ///</summary>
        [TestMethod()]
       
        public void AddServiceTicketTest()
        {
            OpenServiceTickets target = new OpenServiceTickets(); 
            bool expected = false; 
            bool actual;
            actual = target.AddServiceTicket();
            Assert.AreEqual(expected, actual);           
        }

        /// <summary>
        ///This method test the return type of Update service ticket information.
        ///</summary>
        [TestMethod()]
        
        public void UpdateServiceTicketInformationTest()
        {
            OpenServiceTickets target = new OpenServiceTickets(); 
            bool expected = false; 
            bool actual;
            actual = target.UpdateServiceTicketInformation();
            Assert.AreEqual(expected, actual);
          
        }
    }
}

﻿using HTP.Components.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System;
using System.Data;

namespace HTPUnitTests
{
    
    
    /// <summary>
    ///This is a test class for CommentServiceTest and is intended
    ///to contain all CommentServiceTest Unit Tests
    ///</summary>
    [TestClass()]
    public class CommentServiceTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        
       

        [TestMethod]
        public void GetComplaintReportTestForNull()
        {
            CommentService ComtSer = new CommentService();
            DateTime SD = new DateTime(2009, 08, 04);
            DateTime ED = new DateTime(2009, 08, 06);
            DataTable ActualDt = ComtSer.GetComplaintReport(SD, ED, -1, -1, true);
            Assert.IsNotNull(ActualDt);
        }

        [TestMethod]
        public void GetComplaintReportTestForNormal()
        {
            CommentService ComtSer = new CommentService();
            DateTime SD = new DateTime(2009, 08, 04);
            DateTime ED = new DateTime(2009, 08, 06);
            DataTable ActualDt = ComtSer.GetComplaintReport(SD, ED, -1, -1, true);
            Assert.AreEqual(7, ActualDt.Columns.Count);
        }

        /// <summary>
        ///A test for GetCommentReport
        ///</summary>
         [TestMethod()]       
        public void GetCommentReportTest()
        {
            CommentService target = new CommentService(); // TODO: Initialize to an appropriate value
            DateTime StartDate = new DateTime(); // TODO: Initialize to an appropriate value
            DateTime EndDate = new DateTime(); // TODO: Initialize to an appropriate value
            int CaseType = 0; // TODO: Initialize to an appropriate value
            int Attorney = 0; // TODO: Initialize to an appropriate value
            int CommentType = 0; // TODO: Initialize to an appropriate value
            bool ShowAll = false; // TODO: Initialize to an appropriate value
            DataTable expected = null; // TODO: Initialize to an appropriate value
            DataTable actual;
            actual = target.GetCommentReport(StartDate, EndDate, CaseType, Attorney, CommentType, ShowAll);
            Assert.AreEqual(expected, actual);            
        }
        
        /// <summary>
        ///A test for GetCommentReport
        ///</summary>
        [TestMethod()]
        public void GetCommentReportTestForNull()
        {
            CommentService target = new CommentService(); // TODO: Initialize to an appropriate value
            DateTime SD = new DateTime(2009, 08, 04);
            DateTime ED = new DateTime(2009, 08, 06);
            int CaseType = -1; // TODO: Initialize to an appropriate value
            int Attorney = -1; // TODO: Initialize to an appropriate value
            int CommentType = 1; // TODO: Initialize to an appropriate value
            bool ShowAll = false; // TODO: Initialize to an appropriate value            
            DataTable actual;
            actual = target.GetCommentReport(SD, ED, CaseType, Attorney, CommentType, ShowAll);
            Assert.IsNotNull(actual);
        }



        /// <summary>
        ///A test for GetClientCommentsByTicketID
        ///</summary>
        [TestMethod()]        
        public void GetClientCommentsByTicketIDTest()
        {
            CommentService target = new CommentService(); // TODO: Initialize to an appropriate value
            int TicketID = 89654; // TODO: Initialize to an appropriate value            
            DataSet actual;
            actual = target.GetClientCommentsByTicketID(TicketID);
            Assert.IsNotNull(actual);
            
        }

        /// <summary>
        ///A test for GetClientCommentsByTicketID
        ///</summary>
        [TestMethod()]
        public void GetClientCommentsByTicketIDTestForNull()
        {
            CommentService target = new CommentService(); // TODO: Initialize to an appropriate value
            int TicketID = 89654; // TODO: Initialize to an appropriate value            
            DataSet Expected = new DataSet(); ;
            DataSet actual;
            actual = target.GetClientCommentsByTicketID(TicketID);
            Assert.AreEqual(2, actual.Tables.Count);


        }



        /// <summary>
        ///A test for UpdateComplaintIsReviewed
        ///</summary>
        [TestMethod()]        
        public void UpdateComplaintIsReviewedTestForNull()
        {
            CommentService target = new CommentService(); // TODO: Initialize to an appropriate value
            string NoteIDs = "896544"; // TODO: Initialize to an appropriate value            
            DataSet actual;
            actual = target.UpdateComplaintIsReviewed(NoteIDs,3992);
            Assert.IsNotNull(actual);
            
        }



        /// <summary>
        ///A test for GetAllCommentTypes
        ///</summary>
        [TestMethod()]        
        public void GetAllCommentTypesTest()
        {
            CommentService target = new CommentService(); // TODO: Initialize to an appropriate value            
            DataTable actual;
            actual = target.GetAllCommentTypes();
            Assert.AreEqual(true, actual.Rows.Count>0);         
        }

        /// <summary>
        ///A test for GetAllCommentTypes
        ///</summary>
        [TestMethod()]
        public void GetAllCommentTypesTestForNull()
        {
            CommentService target = new CommentService(); // TODO: Initialize to an appropriate value            
            DataTable actual;
            actual = target.GetAllCommentTypes();
            Assert.IsNotNull(actual);
         
        }
    }
}

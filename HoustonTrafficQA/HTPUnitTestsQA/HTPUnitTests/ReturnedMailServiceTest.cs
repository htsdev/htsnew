﻿using HTP.Components.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using HTP.Components.Entities;

namespace HTPUnitTests
{
    
    
    /// <summary>
    ///This is a test class for ReturnedMailServiceTest and is intended
    ///to contain all ReturnedMailServiceTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ReturnedMailServiceTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for ValidateFileFormat is used to test return type.
        ///</summary>
        [TestMethod()]        
        public void ValidateFileFormatTest_retuntype()
        {
            ReturnedMailService target = new ReturnedMailService();
            ReturnedMail rtnMail = new ReturnedMail();
            rtnMail.MailType = ReturnedMail.ReturnMailType.ReturnMailReguler;
            rtnMail.MailFormat = "00000000";
            bool expected = false; 
            bool actual;
            actual = target.ValidateFileFormat(rtnMail);
            Assert.AreEqual(expected.GetType(), actual.GetType());            
        }

        /// <summary>
        ///A test for ValidateFileFormat is used to test return value.
        ///</summary>
        [TestMethod()]
        public void ValidateFileFormatTest_retunvalue()
        {
            ReturnedMailService target = new ReturnedMailService();
            ReturnedMail rtnMail = new ReturnedMail();
            rtnMail.MailType = ReturnedMail.ReturnMailType.ReturnMailReguler;
            rtnMail.MailFormat = "00000000";
            bool expected = true;
            bool actual;
            actual = target.ValidateFileFormat(rtnMail);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for UpdateMailingList is used to test return type.
        ///</summary>
        [TestMethod()]      
        public void UpdateMailingListTest_returntype()
        {
            ReturnedMailService target = new ReturnedMailService(); 
            ReturnedMail rtnMail = new ReturnedMail();
            rtnMail.MailFormat = "00000000";       
            bool expected = false; 
            bool actual;
            actual = target.UpdateMailingList(rtnMail);
            Assert.AreEqual(expected.GetType(), actual.GetType());            
        }

        /// <summary>
        ///A test for UpdateMailingList is used to test return value.
        ///</summary>
        [TestMethod()]
        public void UpdateMailingListTest_returnvalue()
        {
            ReturnedMailService target = new ReturnedMailService();
            ReturnedMail rtnMail = new ReturnedMail();
            rtnMail.MailFormat = "00000000";
            bool expected = true;
            bool actual;
            actual = target.UpdateMailingList(rtnMail);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for UpdateDoNotMail is used to test the return type.
        ///</summary>
        [TestMethod()]      
        public void UpdateDoNotMailTest_retuntype()
        {
            ReturnedMailService target = new ReturnedMailService();
            ReturnedMail rtnMail = new ReturnedMail();            
            int expected = 0; 
            int actual;
            actual = target.UpdateDoNotMail(rtnMail);
            Assert.AreEqual(expected.GetType(), actual.GetType());            
        }

        /// <summary>
        ///A test for UpdateDoNotMail is used to test the return value.
        ///</summary>
        [TestMethod()]
        public void UpdateDoNotMailTest_retunvalue()
        {
            ReturnedMailService target = new ReturnedMailService();
            ReturnedMail rtnMail = new ReturnedMail();                        
            int expected = 0;
            int actual;
            actual = target.UpdateDoNotMail(rtnMail);
            Assert.AreEqual(expected, actual);
        }
    }
}

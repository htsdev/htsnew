﻿using lntechNew.Components;
using FrameWorkEnation.Components;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System.Web.UI.WebControls;
using System.Data;
using System;



namespace HTPUnitTests
{


    /// <summary>
    ///This is a test class for ValidationReportsTest and is intended
    ///to contain all ValidationReportsTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ValidationReportsTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        #region GetMissedCourtDateCalls
        /// <summary>
        ///This Test method will Check if the Dataset is Null or Not . ()
        ///</summary>
        [TestMethod()]
        public void GetMissedCourtDateCalls_Null()
        {
            ValidationReports target = new ValidationReports();

            DataTable actual;
            actual = target.GetMissedCourtDateCalls();
            Assert.IsNotNull(actual);


        }
        /// <summary>
        ///This Test method will Check Type of the Dataset.
        ///</summary>
        [TestMethod()]
        public void GetMissedCourtDateCalls_Type()
        {
            ValidationReports target = new ValidationReports();

            DataTable ds = new DataTable();
            DataTable expected = ds;
            DataTable actual;
            actual = target.GetMissedCourtDateCalls();
            Assert.AreEqual(expected.GetType(), actual.GetType());
        }
        /// <summary>
        /// This Test method will Check No of Rows Return by dataset is equal or greater than Zero . 
        /// </summary>
        /// 
        [TestMethod()]
        public void GetMissedCourtDateCalls_Rows()
        {
            ValidationReports target = new ValidationReports();
            bool expected = false, actual = false;
            int expnumber = 0;
            expected = (expnumber >= 0) ? true : false;
            actual = target.GetMissedCourtDateCalls().Rows.Count >= 0 ? true : false;
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// This Test method will Check that the No of columns are equal to 5  . 
        /// </summary>
        [TestMethod()]
        public void GetMissedCourtDateCalls_NoOfCol()
        {
            ValidationReports target = new ValidationReports();
            int expected = 0, actual = 0;
            expected = 12;
            actual = target.GetMissedCourtDateCalls().Columns.Count;
            Assert.AreEqual(expected, actual);
        }

        #endregion


        /// <summary>
        ///This is a Region for Method GetValidationReport which  returns DataSet.
        ///This Region is intended to contain all its Unit Tests.
        ///</summary>
        ///


        #region GetValidationReport

        /// <summary>
        /// This Test method will Check if email is Generated when @Detail is equal to 0 . 
        /// </summary>
        /// 

        [TestMethod]
        public void GetValidationReport_Detail_Unchecked()
        {
            clsENationWebComponents clsdb = new clsENationWebComponents();

            string[] keys = { "@EmailAddress", "@DisplayReport", "@thisresultset", "@Detail" };
            object[] values = { "yasir@lntechnologies.com", 0, "", 0 };

            clsdb.ExecuteSP("USP_HTS_ValidationEmailReportHouston", keys, values);
        }


        /// <summary>
        /// This Test method will Check if email is Generated when @Detail is equal to 1 . 
        /// </summary>
        /// 

        [TestMethod]
        public void GetValidationReport_Detail_Checked()
        {
            clsENationWebComponents clsdb = new clsENationWebComponents();

            string[] keys = { "@EmailAddress", "@DisplayReport", "@thisresultset", "@Detail" };
            object[] values = { "yasir@lntechnologies.com", 0, "", 1 };

            clsdb.ExecuteSP("USP_HTS_ValidationEmailReportHouston", keys, values);
        }


        #endregion



        /// <summary>
        ///Fahad 5557 03/09/2009 
        ///Unit test Case that accept all below parameters
        ///</summary>
        [TestMethod()]
        public void GetValidationReport2Test()
        {
            ValidationReports target = new ValidationReports(); // TODO: Initialize to an appropriate value
            DateTime Sdate = new DateTime(); // TODO: Initialize to an appropriate value
            DateTime Edate = new DateTime(); // TODO: Initialize to an appropriate value
            int ShowAll = 0; // TODO: Initialize to an appropriate value
            int ShowDisp = 0; // TODO: Initialize to an appropriate value
            int NotShowAworBw = 0; // TODO: Initialize to an appropriate value
            DataSet expected = null; // TODO: Initialize to an appropriate value
            DataSet actual;
            actual = target.GetValidationReport2(Sdate, Edate, ShowAll, ShowDisp, NotShowAworBw);
            Assert.AreNotEqual(expected, actual);

        }

        /// <summary>
        ///Fahad 5557 03/09/2009 
        ///Unit test Case that Reject all below parameters
        ///</summary>
        [TestMethod()]
        public void GetValidationReport2Testfornull()
        {
            ValidationReports target = new ValidationReports(); // TODO: Initialize to an appropriate value
            DateTime Sdate = new DateTime(); // TODO: Initialize to an appropriate value
            DateTime Edate = new DateTime(); // TODO: Initialize to an appropriate value
            int ShowAll = 10; // TODO: Initialize to an appropriate value
            int ShowDisp = 10; // TODO: Initialize to an appropriate value
            int NotShowAworBw = 0; // TODO: Initialize to an appropriate value
            DataSet expected = null; // TODO: Initialize to an appropriate value
            DataSet actual;
            actual = target.GetValidationReport2(Sdate, Edate, ShowAll, ShowDisp, NotShowAworBw);
            Assert.AreEqual(expected, actual);

        }

        /// <summary>
        ///A test for getReportData
        ///</summary>
        [TestMethod()]
        public void getReportDataTest()
        {
            ValidationReports target = new ValidationReports(); // TODO: Initialize to an appropriate value
            GridView gv_records = null; // TODO: Initialize to an appropriate value
            Label errormessage = null; // TODO: Initialize to an appropriate value
            ValidationReport reporttype = new ValidationReport(); // TODO: Initialize to an appropriate value
            target.getReportData(gv_records, errormessage, reporttype);
            Assert.IsNull(target);
        }



        /// <summary>
        ///A test for GetVisitorReport
        ///</summary>
        [TestMethod()]
        public void GetVisitorReportTestForDataTableType()
        {
            ValidationReports target = new ValidationReports();
            DataTable expected = new DataTable();
            DataTable actual = target.GetVisitorReport();
            Assert.AreEqual(expected.GetType(), actual.GetType());
        }

        /// <summary>
        ///A test for GetVisitorReport
        ///</summary>
        [TestMethod()]
        public void GetVisitorReportTestForDataTableRow()
        {
            ValidationReports target = new ValidationReports();
            bool actual = false;
            DataTable dt = target.GetVisitorReport();
            if (dt.Rows.Count >= 0)
                actual = true;
            Assert.AreEqual(true, actual);
        }

        /// <summary>
        ///     A test for GetRecords_PastCourtDates
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(System.ArgumentException))]
        public void GetRecords_PastCourtDatesTestForNullParameter()
        {
            ValidationReports target = new ValidationReports();
            int showAll = 3;
            target.GetRecords_PastCourtDates(showAll);

        }

        /// <summary>
        ///     A test for GetRecords_PastCourtDates
        ///</summary>
        [TestMethod()]
        public void GetRecords_PastCourtDatesTestForNotNullResultSet()
        {
            DataTable Actual;
            ValidationReports target = new ValidationReports();
            int showAll = 0;
            Actual = target.GetRecords_PastCourtDates(showAll);
            Assert.IsNotNull(Actual);
        }

        /// <summary>
        ///A test for GetRecords_PastCourtDatesForNonHMC
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(System.ArgumentException))]
        public void GetRecords_PastCourtDatesForNonHMCForNullParameterTest()
        {
            ValidationReports target = new ValidationReports();
            int showAll = 3;
            target.GetRecords_PastCourtDatesForNonHMC(showAll);

        }

        /// <summary>
        ///     A test for GetRecords_PastCourtDates
        ///</summary>
        [TestMethod()]
        public void GetRecords_PastCourtDatesForNonHMCForNotNullResultSet()
        {
            DataTable Actual;
            ValidationReports target = new ValidationReports();
            int showAll = 0;
            Actual = target.GetRecords_PastCourtDatesForNonHMC(showAll);
            Assert.IsNotNull(Actual);
        }

        /// 5753 Fahad 04/13/2009 
        ///A test for UpdateNosplitFlag
        ///</summary>
        [TestMethod()]
        public void UpdateNosplitFlagTestForZero()
        {
            ValidationReports target = new ValidationReports(); // TODO: Initialize to an appropriate value
            int TicketId = 0; // TODO: Initialize to an appropriate value
            int empid = 0; // TODO: Initialize to an appropriate value
            target.UpdateNosplitFlag(TicketId, empid);
            Assert.IsNotNull(target);

        }

        /// 5753 Fahad 04/13/2009 
        ///A test for UpdateNosplitFlag
        ///</summary>
        [TestMethod()]
        public void UpdateNosplitFlagTestForNegative()
        {
            ValidationReports target = new ValidationReports(); // TODO: Initialize to an appropriate value
            int TicketId = -100; // TODO: Initialize to an appropriate value
            int empid = -10; // TODO: Initialize to an appropriate value
            target.UpdateNosplitFlag(TicketId, empid);
            Assert.IsNotNull(target);

        }
        /// 5753 Fahad 04/13/2009 
        ///A test for GetSplitReport
        ///</summary>
        [TestMethod()]
        public void GetSplitReportTestForFalse()
        {
            ValidationReports target = new ValidationReports(); // TODO: Initialize to an appropriate value
            bool ShowAll = false; // TODO: Initialize to an appropriate value
            DataSet expected = null; // TODO: Initialize to an appropriate value
            DataSet actual;
            actual = target.GetSplitReport(ShowAll);

            Assert.AreNotEqual(expected, actual);

        }
        /// 5753 Fahad 04/13/2009 
        ///A test for GetSplitReport
        ///</summary>
        [TestMethod()]
        public void GetSplitReportTestForTrue()
        {
            ValidationReports target = new ValidationReports(); // TODO: Initialize to an appropriate value
            bool ShowAll = true; // TODO: Initialize to an appropriate value
            DataSet expected = null; // TODO: Initialize to an appropriate value
            DataSet actual;
            actual = target.GetSplitReport(ShowAll);

            Assert.AreNotEqual(expected, actual);

        }

        //Waqas 5770 04/22/2009 Address Validation Report

        /// <summary>
        ///     A test for GetRecords_AddressValidationReport
        ///</summary>
        [TestMethod()]
        public void GetRecords_AddressValidationReportTestForNotNullResultSet()
        {
            DataTable Actual;
            ValidationReports target = new ValidationReports();
            Actual = target.GetRecords_AddressValidationReport();
            Assert.IsNotNull(Actual);
        }

        /// <summary>
        ///     A test for GetRecords_AddressValidationReport
        ///</summary>
        [TestMethod()]
        public void GetRecords_AddressValidationReportTestForType()
        {
            DataTable Actual;
            ValidationReports target = new ValidationReports();
            Actual = target.GetRecords_AddressValidationReport();
            Assert.AreNotEqual(Actual.GetType(), target.GetType());
        }

        //Waqas 5770 04/22/2009 First Name Validation Report

        /// <summary>
        ///     A test for GetRecords_FirstNameValidationReport
        ///</summary>
        [TestMethod()]
        public void GetRecords_FirstNameValidationReportTestForNotNullResultSet()
        {
            DataTable Actual;
            ValidationReports target = new ValidationReports();
            Actual = target.GetRecords_FirstNameValidationReport();
            Assert.IsNotNull(Actual);
        }

        /// <summary>
        ///     A test for GetRecords_FirstNameValidationReport
        ///</summary>
        [TestMethod()]
        public void GetRecords_FirstNameValidationReportTestForType()
        {
            DataTable Actual;
            ValidationReports target = new ValidationReports();
            Actual = target.GetRecords_FirstNameValidationReport();
            Assert.AreNotEqual(Actual.GetType(), target.GetType());
        }

        /// <summary>
        ///A test for GetRecords_HMCSettingDiscrepancy
        ///</summary>
        [TestMethod()]
        public void GetRecords_HMCSettingDiscrepancyTestForType()
        {
            ValidationReports target = new ValidationReports();
            int showAll = 0;
            DataTable actual;
            DataTable expected = new DataTable();
            actual = target.GetRecords_HMCSettingDiscrepancy(showAll);
            Assert.AreEqual(actual.GetType(), expected.GetType());
        }
        /// <summary>
        ///A test for GetRecords_HMCSettingDiscrepancy
        ///</summary>
        [TestMethod()]
        public void GetRecords_HMCSettingDiscrepancyTestForNullResultSet()
        {
            ValidationReports target = new ValidationReports();
            int showAll = 0;
            DataTable actual;
            DataTable expected = null;
            actual = target.GetRecords_HMCSettingDiscrepancy(showAll);
            Assert.AreNotEqual(actual, expected);
        }

        //Waqas 5864 07/07/2009 ALR
        /// <summary>
        ///A test for GetRecords_CaseEmailSummaryReport
        ///</summary>
        [TestMethod()]
        public void GetRecords_CaseEmailSummaryReportTest_ForDataTable()
        {
            ValidationReports target = new ValidationReports();
            DataTable actual;
            actual = target.GetRecords_CaseEmailSummaryReport();
            Assert.IsNotNull(actual);
        }

        /// <summary>
        ///A test for GetRecords_CaseEmailSummaryReport
        ///</summary>
        [TestMethod()]
        public void GetRecords_CaseEmailSummaryReportTest_ForRows()
        {
            ValidationReports target = new ValidationReports();
            DataTable actual;
            actual = target.GetRecords_CaseEmailSummaryReport();
            bool expected = true;
            bool actualResult = (actual.Rows.Count >= 0) ? true : false;
            Assert.AreEqual(expected, actualResult);
        }

        #region GetNewHireReport

        /// <summary>
        ///     A test for GetNewHireReport to check null DataTable
        ///</summary>
        [TestMethod()]        
        public void GetNewHireReportTestForNullDataTable()
        {
            ValidationReports target = new ValidationReports();
            DataTable actual;
            actual = target.GetNewHireReport();
            Assert.IsNotNull(actual);
        }

        /// <summary>
        ///     A test for GetNewHireReport to check number of rows
        ///</summary>
        [TestMethod()]
        public void GetNewHireReportTestForRows()
        {
            ValidationReports target = new ValidationReports();
            DataTable actual;
            actual = target.GetNewHireReport();
            bool expected = true;
            bool actualResult = (actual.Rows.Count >= 0) ? true : false;
            Assert.AreEqual(expected, actualResult);
        }

        /// <summary>
        ///     A test for GetNewHireReport to check number of rows
        ///</summary>
        [TestMethod()]
        public void GetNewHireReportTestForTypes()
        {
            ValidationReports target = new ValidationReports();
            DataTable expected = new DataTable();
            DataTable actual;
            actual = target.GetNewHireReport();
            Assert.AreEqual(expected.GetType(), actual.GetType());
        }

        #endregion


    }
}

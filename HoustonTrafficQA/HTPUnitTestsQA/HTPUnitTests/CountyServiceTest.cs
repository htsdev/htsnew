﻿using HTP.Components.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System.Data;

namespace HTPUnitTests
{
    
    
    /// <summary>
    ///This is a test class for CountyServiceTest and is intended
    ///to contain all CountyServiceTest Unit Tests
    ///</summary>
    [TestClass()]
    public class CountyServiceTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for GetCounties
        ///</summary>
        [TestMethod()]
     
        public void GetCountiesTest_Type()
        {
            CountyService target = new CountyService(); // TODO: Initialize to an appropriate value
            bool IsActive = false; // TODO: Initialize to an appropriate value
            DataSet expected = new DataSet(); // TODO: Initialize to an appropriate value
            DataSet actual;
            actual = target.GetCounties(IsActive);
            Assert.AreEqual(expected.GetType(), actual.GetType());           
        }

        /// <summary>
        ///A test for GetCounties
        ///</summary>
        [TestMethod()]

        public void GetCountiesTest_NotNull()
        {
            CountyService target = new CountyService(); // TODO: Initialize to an appropriate value
            bool IsActive = true;  // TODO: Initialize to an appropriate value            
            DataSet actual;
            actual = target.GetCounties(IsActive);
            Assert.IsNotNull(actual);
        }


        /// <summary>
        ///A test for GetCounties
        ///</summary>
        [TestMethod()]

        public void GetCountiesTest_NoOfCol()
        {
            CountyService target = new CountyService(); // TODO: Initialize to an appropriate value
            bool IsActive = true;
            int actual = 2;
            int expacted = 0;
            expacted = target.GetCounties(IsActive).Tables[0].Columns.Count;
            Assert.AreEqual(actual, expacted);
        }
    }
}

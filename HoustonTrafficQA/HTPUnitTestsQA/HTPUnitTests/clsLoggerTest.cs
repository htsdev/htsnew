﻿using lntechNew.Components.ClientInfo;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System.Data;

namespace HTPUnitTests
{


    /// <summary>
    ///This is a test class for clsLoggerTest and is intended
    ///to contain all clsLoggerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class clsLoggerTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///This method is used to test type of return value.
        ///</summary>
        [TestMethod()]

        public void GetTicklerEventHistoryTestType()
        {
            clsLogger target = new clsLogger(); // TODO: Initialize to an appropriate value
            int TicketID = 0; // TODO: Initialize to an appropriate value
            DataTable expected = new DataTable(); // TODO: Initialize to an appropriate value
            DataTable actual;
            actual = target.GetTicklerEventHistory(TicketID);
            Assert.AreEqual(expected.GetType(), actual.GetType());

        }
        /// <summary>
        ///This method is used to check the return value for null.
        ///</summary>
        [TestMethod()]

        public void GetTicklerEventHistoryTestNull()
        {
            clsLogger target = new clsLogger(); // TODO: Initialize to an appropriate value
            int TicketID = 0; // TODO: Initialize to an appropriate value            
            DataTable actual;
            actual = target.GetTicklerEventHistory(TicketID);
            Assert.IsNotNull(actual);

        }

        #region GetSMSHistory

        /// <summary>
        ///A test for GetSMSHistory
        ///</summary>
        [TestMethod()]
        public void GetSMSHistoryTestForRowsCount()
        {
            clsLogger target = new clsLogger();
            int TicketID = 67371;
            int smstype = 0;
            bool expected = true;
            bool actual = false;
            actual = (target.GetSMSHistory(TicketID, smstype).Rows.Count >= 0) ? true : false;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for GetSMSHistory
        ///</summary>
        [TestMethod()]
        public void GetSMSHistoryTestForType()
        {
            clsLogger target = new clsLogger();
            int TicketID = 67371;
            int smstype = 0;
            DataTable expected = new DataTable();
            DataTable actual = null;
            actual = target.GetSMSHistory(TicketID, smstype);
            Assert.AreEqual(expected.GetType(), actual.GetType());
        }

        /// <summary>
        ///A test for GetSMSHistory
        ///</summary>
        [TestMethod()]
        public void GetSMSHistoryTestForNullDataTable()
        {
            clsLogger target = new clsLogger();
            int TicketID = 67371;
            int smstype = 0;
            DataTable actual = null;
            actual = target.GetSMSHistory(TicketID, smstype);
            Assert.IsNotNull(actual);
        }

        #endregion
    }
}

﻿using lntechNew.Components;
using FrameWorkEnation.Components;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System;
using System.Data;

namespace HTPUnitTests
{
    
    
    /// <summary>
    ///This is a test class for faxclassTest and is intended
    ///to contain all faxclassTest Unit Tests
    ///</summary>
    [TestClass()]
    public class faxclassTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///This test method will check not null of resulted Dataset
        ///</summary>
        [TestMethod()]
        public void GetJpAppearancereportTest_NotNull()
        {
            faxclass  target = new faxclass();            
            DataTable actual;
            actual = target.GetJpAppearancereport();
            Assert.IsNotNull(actual);

        }

        /// <summary>
        ///This Test method will Check Type of the Dataset.
        ///</summary>
        [TestMethod()]
        public void GetJpAppearancereportTest_Type()
        {
            faxclass target = new faxclass();
            DataTable ds = new DataTable();
            DataTable expected = ds;
            DataTable actual;
            actual = target.GetJpAppearancereport();            
            Assert.AreEqual(expected.GetType(), actual.GetType());
        }

        /// <summary>
        ///This Test method will Check No of Rows Return by dataset is equal or greater than Zero . 
        ///</summary>
        [TestMethod()]
        public void GetJpAppearancereportTest_Rows()
        {
            faxclass target = new faxclass();
            bool expected = false, actual = false;
            int expnumber = 0;
            expected = (expnumber >= 0) ? true : false;            
            actual = target.GetJpAppearancereport().Rows.Count >= 0 ? true : false;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///This Test method will Check that the No of columns are equal to 8  . 
        ///</summary>
        [TestMethod()]
        public void GetJpAppearancereportTest_NoOfCols()
        {
            faxclass target = new faxclass();
            int expected = 0, actual = 0;
            expected = 8;            
            actual = target.GetJpAppearancereport().Columns.Count;
            Assert.AreEqual(expected, actual);
        }

        
    }
}

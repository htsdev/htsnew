﻿using lntechNew.Components.ClientInfo;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System;

namespace HTPUnitTests
{


    /// <summary>
    ///This is a test class for ClsSMSTest and is intended
    ///to contain all ClsSMSTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ClsSMSTest
    {
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        #region IsvalidAccount

        /// <summary>
        ///A test for IsvalidAccount
        ///</summary>
        [TestMethod()]
        public void IsvalidAccountTestWithRightUsernamePassword()
        {
            ClsSMS target = new ClsSMS();
            bool expected = true;
            bool actual;
            target.UserName = "sullolaw";
            target.Password = "tickets123";
            target.Api_id = "3187005";
            actual = target.IsvalidAccount().Contains("OK");
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for IsvalidAccount
        ///</summary>
        [TestMethod()]
        public void IsvalidAccountTestWithfakeUsernamePassword()
        {
            ClsSMS target = new ClsSMS();
            bool expected = false;
            bool actual;
            target.UserName = "fake";
            target.Password = "fake";
            target.Api_id = "3187005";
            actual = target.IsvalidAccount().Contains("OK");
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for IsvalidAccount
        ///</summary>
        [TestMethod()]        
        public void IsvalidAccountTestForNullParameter()
        {
            ClsSMS target = new ClsSMS();
            bool expected = false;
            bool actual;
            target.UserName = "";
            target.Password = "";
            target.Api_id = "3187005";
            actual = target.IsvalidAccount().Contains("OK");
            Assert.AreEqual(expected, actual);
        }

        #endregion
        

        #region AddNoteToHistory

        /// <summary>
        ///A test for AddNoteToHistory
        ///</summary>
        [TestMethod()]
        public void AddNoteToHistoryTest()
        {
            ClsSMS target = new ClsSMS();
            int ticketid = 67371;
            string message = "Testing";
            int empid = 3991;
            target.AddNoteToHistory(ticketid, message, empid);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for AddNoteToHistory
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AddNoteToHistoryTestForNullParameter()
        {
            ClsSMS target = new ClsSMS();
            int ticketid = 67371;
            string message = "";
            int empid = 3992;
            target.AddNoteToHistory(ticketid, message, empid);
        }

        #endregion

        #region InsertHistory

        /// <summary>
        ///A test for InsertHistory
        ///</summary>
        [TestMethod()]
        public void InsertHistoryTest()
        {
            ClsSMS target = new ClsSMS();
            string ClientName = "Noufil Khan";
            string meesagebody = "Testing";
            int ticketid = 67371;
            DateTime courtdate = DateTime.Now;
            string courtnumber = "0";
            string phonenumber = "+923213794225";
            string smscalltype = "Set Call";
            string ticketnumber = "TR123332332";
            string resendby = "Noufil";
            target.InsertHistory(ClientName, meesagebody, ticketid, courtdate, courtnumber, phonenumber, smscalltype, ticketnumber, resendby);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for InsertHistory
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ArgumentNullException))]
        public void InsertHistoryTestWithNullParameter()
        {
            ClsSMS target = new ClsSMS();
            string ClientName = "Noufil Khan";
            string meesagebody = "Testing";
            int ticketid = 0;
            DateTime courtdate = DateTime.Now;
            string courtnumber = "0";
            string phonenumber = "+923213794225";
            string smscalltype = "Set Call";
            string ticketnumber = "TR123332332";
            string resendby = "Noufil";
            target.InsertHistory(ClientName, meesagebody, ticketid, courtdate, courtnumber, phonenumber, smscalltype, ticketnumber, resendby);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        #endregion
    }
}

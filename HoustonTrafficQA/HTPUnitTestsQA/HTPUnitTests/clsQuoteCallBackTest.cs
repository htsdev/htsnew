﻿using lntechNew.Components.ClientInfo;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System.Data;
using System;

namespace HTPUnitTests
{
    /// <summary>
    ///This is a test class for clsQuoteCallBackTest and is intended
    ///to contain all clsQuoteCallBackTest Unit Tests
    ///</summary>
    [TestClass()]
    public class clsQuoteCallBackTest
    {
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region GetAllQuoteCallBackQuery

        /// <summary>
        ///     A test for GetAllQuoteCallBackQuery. Checking if method returns DataTable
        ///</summary>
        [TestMethod()]
        public void GetAllQuoteCallBackQueryTestForType()
        {
            clsQuoteCallBack target = new clsQuoteCallBack();
            DataTable expected = new DataTable();
            DataTable actual;
            actual = target.GetAllQuoteCallBackQuery();
            Assert.AreEqual(expected.GetType(), actual.GetType());
        }

        /// <summary>
        ///     A test for GetAllQuoteCallBackQuery. Checking if datatable does not throw any invalid exception.
        ///</summary>
        [TestMethod()]
        public void GetAllQuoteCallBackQueryTest()
        {
            clsQuoteCallBack target = new clsQuoteCallBack();
            bool actual = false;
            DataTable dt = target.GetAllQuoteCallBackQuery();
            actual = (dt.Rows.Count >= 0) ? true : false;
            Assert.AreEqual(actual, true);
        }

        /// <summary>
        //      /A test for GetAllQuoteCallBackQuery. Checking Return Datable mut not be null
        ///</summary>
        [TestMethod()]
        public void GetAllQuoteCallBackQueryTestForNullDataTable()
        {
            clsQuoteCallBack target = new clsQuoteCallBack();
            Assert.IsNotNull(target.GetAllQuoteCallBackQuery());
        }

        #endregion

        #region GetQuoteCallBack

        /// <summary>
        ///     A test for GetQuoteCallBack
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetQuoteCallBackTestForNullParameter()
        {
            clsQuoteCallBack target = new clsQuoteCallBack();
            int Criteria = 0;
            DateTime Datefrom = DateTime.Now;
            DateTime DateTo = DateTime.Now;
            DateTime CallDate = DateTime.Now;
            int casetype = 0;
            target.GetQuoteCallBack(Criteria, Datefrom, DateTo, CallDate, casetype);
        }


        /// <summary>
        ///     A test for GetQuoteCallBack Second case for Switch function. Check for DataTable type.
        ///</summary>
        [TestMethod()]
        public void GetQuoteCallBackTestForFirstDataTable()
        {
            clsQuoteCallBack target = new clsQuoteCallBack();
            int Criteria = 1;
            DateTime Datefrom = DateTime.Now;
            DateTime DateTo = DateTime.Now;
            DateTime CallDate = DateTime.Now;
            int casetype = 1;
            DataTable Dt_actual = new DataTable();
            DataTable Dt_expected = new DataTable();
            Dt_actual = target.GetQuoteCallBack(Criteria, Datefrom, DateTo, CallDate, casetype);
            Assert.AreEqual(Dt_actual.GetType(), Dt_expected.GetType());
        }

        /// <summary>
        ///     A test for GetQuoteCallBack Second case for Switch function. Check for unexpected rows.
        ///</summary>
        [TestMethod()]
        public void GetQuoteCallBackTestForFirstDataTableRows()
        {
            clsQuoteCallBack target = new clsQuoteCallBack();
            int Criteria = 1;
            DateTime Datefrom = DateTime.Now;
            DateTime DateTo = DateTime.Now;
            DateTime CallDate = DateTime.Now;
            int casetype = 1;
            DataTable Dt_actual = target.GetQuoteCallBack(Criteria, Datefrom, DateTo, CallDate, casetype);
            bool actual = (Dt_actual.Rows.Count >= 0) ? true : false;
            Assert.AreEqual(actual, true);
        }

        /// <summary>
        ///     A test for GetQuoteCallBack first case for Switch function. Check if return datatable is not null
        ///</summary>
        [TestMethod()]
        public void GetQuoteCallBackTestForFirstNullDataTable()
        {
            clsQuoteCallBack target = new clsQuoteCallBack();
            int Criteria = 1;
            DateTime Datefrom = DateTime.Now;
            DateTime DateTo = DateTime.Now;
            DateTime CallDate = DateTime.Now;
            int casetype = 1;
            Assert.IsNotNull(target.GetQuoteCallBack(Criteria, Datefrom, DateTo, CallDate, casetype));
        }

        /// <summary>
        ///     A test for GetQuoteCallBack Second case for Switch function. Check for DataTable Type.
        ///</summary>
        [TestMethod()]
        public void GetQuoteCallBackTestForSecondDataTableType()
        {
            clsQuoteCallBack target = new clsQuoteCallBack();
            int Criteria = 2;
            DateTime Datefrom = DateTime.Now;
            DateTime DateTo = DateTime.Now;
            DateTime CallDate = DateTime.Now;
            int casetype = 1;
            DataTable Dt_actual = new DataTable();
            DataTable Dt_expected = new DataTable();
            Dt_actual = target.GetQuoteCallBack(Criteria, Datefrom, DateTo, CallDate, casetype);
            Assert.AreEqual(Dt_actual.GetType(), Dt_expected.GetType());
        }

        /// <summary>
        ///     A test for GetQuoteCallBack Second case for Switch function. Check for unexpected rows
        ///</summary>
        [TestMethod()]
        public void GetQuoteCallBackTestForSecondDataTableRows()
        {
            clsQuoteCallBack target = new clsQuoteCallBack();
            int Criteria = 2;
            DateTime Datefrom = DateTime.Now;
            DateTime DateTo = DateTime.Now;
            DateTime CallDate = DateTime.Now;
            int casetype = 1;
            DataTable Dt_actual = target.GetQuoteCallBack(Criteria, Datefrom, DateTo, CallDate, casetype);
            bool actual = (Dt_actual.Rows.Count >= 0) ? true : false;
            Assert.AreEqual(actual, true);
        }

        /// <summary>
        ///     A test for GetQuoteCallBack Second case for Switch function. Check if return DataTable is not null
        ///</summary>
        [TestMethod()]
        public void GetQuoteCallBackTestForSecondNullDataTable()
        {
            clsQuoteCallBack target = new clsQuoteCallBack();
            int Criteria = 2;
            DateTime Datefrom = DateTime.Now;
            DateTime DateTo = DateTime.Now;
            DateTime CallDate = DateTime.Now;
            int casetype = 1;
            Assert.IsNotNull(target.GetQuoteCallBack(Criteria, Datefrom, DateTo, CallDate, casetype));
        }

        #endregion


    }
}

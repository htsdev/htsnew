﻿using lntechNew.Components.ClientInfo;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System;
using System.Data;

namespace HTPUnitTests
{
    
    
    /// <summary>
    ///This is a test class for HccCriminalReportTest and is intended
    ///to contain all HccCriminalReportTest Unit Tests
    ///</summary>
    [TestClass()]
    public class HccCriminalReportTest
    {


        private TestContext testContextInstance;
        HccCriminalReport target = new HccCriminalReport();

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        #region Get_ContractFollowUp
        /// <summary>
        ///A test for Get_ContractFollowUp
        ///</summary>
        [TestMethod()]
        public void Get_ContractFollowUpTestForNull()
        {
            // TODO: Initialize to an appropriate value
            //DataTable expected = null; // TODO: Initialize to an appropriate value
            DataTable actual;
            actual = target.Get_ContractFollowUp();
            Assert.IsNotNull(actual);

        }
        /// <summary>
        /// This Test method will Check No of Rows Return by dataset is equal or greater than Zero . 
        /// </summary>
        /// 
        [TestMethod()]
        public void IsContractFollowUpContainRows()
        {
            bool expected, actual = false;
            int expnumber = 0;
            expected = (expnumber >= 0) ? true : false;
            actual = (target.Get_ContractFollowUp().Rows.Count >= 0) ? true : false;
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// This Test method will Check that the No of columns are equal to 18  . 
        /// </summary>
        [TestMethod()]
        public void Get_ContractFollowUpTestForNoOfCol()
        {
            bool expected , actual;                
            int expnumber = 0;
            expected = (expnumber >= 0) ? true : false;
            actual = (target.Get_ContractFollowUp().Columns.Count >= 0) ? true : false;
            Assert.AreEqual(expected, actual);
        }     
        #endregion


       
        
    }
}

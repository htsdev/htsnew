﻿using lntechNew.Components;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System.Data;
using FrameWorkEnation.Components;
using System.Configuration;
using System;

namespace HTPUnitTests
{
    [TestClass()]
    public class clsGeneralMethodsTest
    {


        clsGeneralMethods GM = new clsGeneralMethods();

        #region DeleteRootTempFile
        /// <summary>
        /// test DeleteTempFiles method that file deleted or not
        /// </summary>
        [TestMethod()]
        public void DeleteTempFileTest()
        {
            string path = ConfigurationManager.AppSettings["TempFilespath"].ToString(); //"D:\\Nasir\\Project\\Houston Traffic\\Temp\\";
          bool Expected = true;
          bool Actual = GM.DeleteTempFiles(path,"*.pdf");
          Assert.AreEqual(Expected, Actual);
        }

        /// <summary>
        /// test check exeption by providing wrong path
        /// </summary>
        [ExpectedException (typeof(System.IO.DirectoryNotFoundException))]
        [TestMethod()]        
        public void DeleteTempFileTestDirNotFound()
        {
            string path = "D:\\Nasir1\\Project\\Houston Traffic\\Temp\\";
            bool Expected = true;
            bool Actual = GM.DeleteTempFiles(path, "*.pdf");
            Assert.AreEqual(Expected, Actual);
        }



        #endregion

        //Nasir 5765 11/20/2009 
        /// <summary>
        ///A test for EscapeXml
        ///</summary>
        [TestMethod()]
        public void EscapeXmlTestForNull()
        {
            string Text = "Test&Test"; // TODO: Initialize to an appropriate value            
            string actual;
            actual = clsGeneralMethods.EncodeXMLString(Text);
            Assert.IsNotNull(actual);

        }

        //Nasir 5765 11/20/2009 
        /// <summary>
        ///A test for EscapeXml
        ///</summary>
        [TestMethod()]
        public void EscapeXmlTest()
        {
            string Text = "Test&Test"; // TODO: Initialize to an appropriate value            
            string actual;
            actual = clsGeneralMethods.EncodeXMLString(Text);
            Assert.AreEqual(true, actual.Contains("&amp"));
        }
        //Sabir Khan 6851 12/04/2009
        /// <summary>
        ///A test for CurrencyToDouble
        ///</summary>
        [TestMethod()]       
        public void CurrencyToDoubleTestForNull()
        {
            clsGeneralMethods target = new clsGeneralMethods(); // TODO: Initialize to an appropriate value
            string pCurrency = string.Empty; // TODO: Initialize to an appropriate value
            double expected = 0.00; // TODO: Initialize to an appropriate value
            double actual;
            actual = target.CurrencyToDouble(pCurrency);
            Assert.AreEqual(expected, actual);
            
        }
    
    }
}

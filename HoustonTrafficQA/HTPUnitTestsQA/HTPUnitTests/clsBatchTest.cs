﻿using HTP.Components;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System;
using System.Data;

namespace HTPUnitTests
{
    
    
    /// <summary>
    ///This is a test class for clsBatchTest and is intended
    ///to contain all clsBatchTest Unit Tests
    ///</summary>
    [TestClass()]
    public class clsBatchTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for getBatchPrintCount
        ///</summary>
        [TestMethod()]        
        public void getBatchPrintCountTestTableCount()
        {
            clsBatch target = new clsBatch(); // TODO: Initialize to an appropriate value
            bool isPrinted = false; // TODO: Initialize to an appropriate value
            DateTime startdate = DateTime.Now;
            DateTime enddate = DateTime.Now;
            string EmpID = "%";
            int courtId = 0;
            int expected = 6; // TODO: Initialize to an appropriate value
            DataSet actual;
            actual = target.getBatchPrintCount(isPrinted, startdate, enddate, EmpID, courtId);
            Assert.AreEqual(expected, actual.Tables.Count);
            
        }

        /// <summary>
        ///A test for getBatchPrintCount
        ///</summary>
        [TestMethod()]
        public void getBatchPrintCountTestType()
        {
            clsBatch target = new clsBatch(); // TODO: Initialize to an appropriate value
            bool isPrinted = false; // TODO: Initialize to an appropriate value
            DateTime startdate = DateTime.Now.AddDays(-7);
            DateTime enddate = DateTime.Now;
            string EmpID = "%";
            int courtId = 0;
            DataSet expected =new DataSet(); // TODO: Initialize to an appropriate value
            DataSet actual;
            actual = target.getBatchPrintCount(isPrinted, startdate, enddate, EmpID, courtId);
            Assert.AreEqual(expected.GetType(), actual.GetType());
            
        }

        /// <summary>
        ///A test for getBatchPrintCount
        ///</summary>
        [TestMethod()]        
        public void getBatchPrintCountTest()
        {
            clsBatch target = new clsBatch(); // TODO: Initialize to an appropriate value
            bool isPrinted = false; // TODO: Initialize to an appropriate value
            DateTime startdate = DateTime.Now.AddDays(-7);
            DateTime enddate = DateTime.Now;
            string EmpID = "%";
            int courtId = 0;
            DataSet expected = new DataSet(); // TODO: Initialize to an appropriate value
            DataSet actual;
            actual = target.getBatchPrintCount(isPrinted, startdate, enddate, EmpID, courtId);
            Assert.AreEqual(6, actual.Tables.Count);
            
        }
    }
}

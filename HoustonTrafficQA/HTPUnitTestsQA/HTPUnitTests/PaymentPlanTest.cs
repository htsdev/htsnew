﻿using lntechNew.Components;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System.Data;

namespace HTPUnitTests
{
    
    
    /// <summary>
    ///This is a test class for PaymentPlanTest and is intended
    ///to contain all PaymentPlanTest Unit Tests
    ///</summary>
    [TestClass()]
    public class PaymentPlanTest
    {

        PaymentPlan target = new PaymentPlan(); // TODO: Initialize to an appropriate value
        int Criteria = 0; 
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion



        /// <summary>
        ///Noufil 5618 04/03/2009
        ///A test for GetPastDueReports to check not allow Null values
        ///</summary>
        [TestMethod()]
        public void GetPastDueReportsTestForNull()
        {
            
            DataTable expected = null; // TODO: Initialize to an appropriate value
            DataTable actual;
            actual = target.GetPastDueReports(Criteria);
            Assert.AreNotEqual(expected, actual);
          
        }

        /// <summary>
        ///Noufil 5618 04/03/2009
        ///A test for GetPastDueReports to check values
        ///</summary>
        [TestMethod()]
        public void GetPastDueReportsTestForValues()
        {
            bool expected, actual = false;
            int expnumber = 0;
            expected = (expnumber >= 0) ? true : false;
            actual = (target.GetPastDueReports(Criteria).Rows.Count >= 0) ? true : false;
            Assert.AreEqual(expected, actual);

        }

        /// <summary>
        /// Noufil 5618 04/03/2009
        ///A test for GetPaymentDueReports to check not allow Null values
        ///</summary>
        [TestMethod()]
        public void GetPaymentDueReportsTest()
        {
           
            DataTable expected = null; // TODO: Initialize to an appropriate value
            DataTable actual;
            actual = target.GetPaymentDueReports();
            Assert.AreNotEqual(expected, actual);
            
        }

        /// <summary>
        ///Noufil 5618 04/03/2009
        ///A test for GetPastDueReports to check values
        ///</summary>
        [TestMethod()]
        public void GetPaymentDueReportsTestForValues()
        {
            bool expected, actual = false;
            int expnumber = 0;
            expected = (expnumber >= 0) ? true : false;
            actual = (target.GetPaymentDueReports().Rows.Count >= 0) ? true : false;
            Assert.AreEqual(expected, actual);

        }


        /// <summary>
        ///A test for GetPastDueReports
        ///</summary>
        [TestMethod()]
        //[HostType("ASP.NET")]
        //[AspNetDevelopmentServerHost("D:\\Fadi\\Projects\\HoustonTraffic", "/")]
        //[UrlToTest("http://localhost:11890/")]
        public void GetPastDueReportsTest()
        {
            PaymentPlan target = new PaymentPlan(); // TODO: Initialize to an appropriate value
           // TODO: Initialize to an appropriate value
            DataTable expected = null; // TODO: Initialize to an appropriate value
            DataTable actual;
            actual = target.GetPastDueReports(Criteria);
            Assert.AreNotEqual(expected, actual);
            
        }

      
    }
}

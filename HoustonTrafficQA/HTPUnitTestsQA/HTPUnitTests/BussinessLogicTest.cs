﻿using HTP.Components;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System.Data;
using FrameWorkEnation.Components;
using System;

namespace HTPUnitTests
{



    [TestClass()]
    public class BussinessLogicTest
    {


        BussinessLogic BL = new BussinessLogic();
        BussinessLogic target = new BussinessLogic();
        private TestContext testContextInstance;


        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }



        [TestMethod()]
        public void UploadNewDocumentTestForNegativeValues()
        {

            int MenuID = -67;
            string DocName = string.Empty;
            int EmpID = -3391;
            string SavDoc = "";
            bool expected = false;
            bool actual;
            actual = target.UploadNewDocument(MenuID, DocName, EmpID, SavDoc);
            Assert.AreEqual(expected, actual);

        }

        [TestMethod()]
        public void UploadNewDocumentTestForNull()
        {
            int MenuID = 0;
            string DocName = null;
            string SavDoc = null;
            int EmpID = 0;
            bool expected = false;
            bool actual;
            actual = target.UploadNewDocument(MenuID, DocName, EmpID, SavDoc);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void UpdateNewBussinessLogicTestForZeroValues()
        {
            int MenuID = 0;
            string busineslogic = string.Empty;
            bool expected = false;
            bool actual;
            actual = target.UpdateNewBussinessLogic(MenuID, busineslogic);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void UpdateNewBussinessLogicTestForNegativeValues()
        {
            int MenuID = -1000;
            string busineslogic = string.Empty;
            bool expected = false;
            bool actual;
            actual = target.UpdateNewBussinessLogic(MenuID, busineslogic);
            Assert.AreEqual(expected, actual);

        }

        [TestMethod()]
        public void GetUploadedDocumentTestForRowsReturn()
        {
            bool expected, actual = false;
            int MenuID = 67;
            expected = (MenuID >= 0) ? true : false;
            actual = (target.GetUploadedDocument(MenuID).Rows.Count >= 0) ? true : false;
            Assert.AreEqual(expected, actual);

        }

        [TestMethod()]
        public void GetUploadedDocumentTestForNull()
        {

            int MenuID = 67;
            DataTable actual;
            actual = target.GetUploadedDocument(MenuID);
            Assert.IsNotNull(actual);

        }

        [TestMethod()]
        public void GetBusinessLogicByMenuIDTestForRowsReturn()
        {
            int MenuID = 0;
            bool expected, actual = false;
            expected = (MenuID >= 0) ? true : false;
            actual = (target.GetBusinessLogicByMenuID(MenuID).Rows.Count >= 0) ? true : false;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void GetBusinessLogicByMenuIDTestForNull()
        {
            int MenuID = 0;
            DataTable expected = null;
            DataTable actual;
            actual = target.GetBusinessLogicByMenuID(MenuID);
            Assert.AreNotEqual(expected, actual);
        }
        [TestMethod()]
        public void GetBusinessLogicByMenuIDTestForNegativeValue()
        {
            int MenuID = -675;
            DataTable expected = null;
            DataTable actual;
            actual = target.GetBusinessLogicByMenuID(MenuID);
            Assert.AreNotEqual(expected, actual);
        }


        [TestMethod()]
        public void GetAccessTypeTestForRowReturn()
        {
            int EmpID = 3991;
            int expected = 2;
            int actual;
            actual = target.GetAccessType(EmpID);
            Assert.AreEqual(expected, actual);
        }
        [TestMethod()]
        public void GetAccessTypeTestForNegativeValue()
        {
            bool expected, actual = false;
            int expnumber = -3991;
            expected = (expnumber > 0) ? true : false;
            actual = (target.GetAccessType(expnumber) > 0) ? true : false;
            Assert.AreEqual(expected, actual);

        }

        [TestMethod()]
        public void DeleteUploadedDocsTestForZeroValues()
        {
            int MenuID = 0;
            int DocID = 0;
            bool expected = false;
            bool actual;
            actual = target.DeleteUploadedDocs(MenuID, DocID);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void DeleteUploadedDocsTestForNegativeValues()
        {
            int MenuID = -67;
            int DocID = -3;
            bool expected = false;
            bool actual;
            actual = target.DeleteUploadedDocs(MenuID, DocID);
            Assert.AreEqual(expected, actual);
        }
        [TestMethod()]
        public void DeleteUploadedDocsTestFornonNegativeValues()
        {
            int MenuID = 67;
            int DocID = 4;
            bool expected = true;
            bool actual;
            actual = target.DeleteUploadedDocs(MenuID, DocID);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void UploadedDateTest1()
        {

            DateTime expected = new DateTime();
            DateTime actual;
            target.UploadedDate = expected;
            actual = target.UploadedDate;
            Assert.AreEqual(expected, actual);

        }

        [TestMethod()]
        public void RowsCountTest1()
        {
            int expected = 0;
            int actual;
            target.RowsCount = expected;
            actual = target.RowsCount;
            Assert.AreEqual(expected, actual);

        }

        [TestMethod()]
        public void MenuIDTest1()
        {

            string expected = string.Empty;
            string actual;
            target.MenuID = expected;
            actual = target.MenuID;
            Assert.AreEqual(expected, actual);

        }


        [TestMethod()]
        public void IsDeletedTest1()
        {
            bool expected = false;
            bool actual;
            target.IsDeleted = expected;
            actual = target.IsDeleted;
            Assert.AreEqual(expected, actual);

        }


        [TestMethod()]
        public void HelpDocIDTest1()
        {
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            target.HelpDocID = expected;
            actual = target.HelpDocID;
            Assert.AreEqual(expected, actual);
        }


        [TestMethod()]
        public void EmpIDTest1()
        {

            int expected = 0;
            int actual;
            target.EmpID = expected;
            actual = target.EmpID;
            Assert.AreEqual(expected, actual);

        }


        [TestMethod()]
        public void DocNameTest1()
        {
            string expected = string.Empty;
            string actual;
            target.DocName = expected;
            actual = target.DocName;
            Assert.AreEqual(expected, actual);

        }


        [TestMethod()]
        public void BusinesLogicTest1()
        {
            string expected = string.Empty;
            string actual;
            target.BusinesLogic = expected;
            actual = target.BusinesLogic;
            Assert.AreEqual(expected, actual);
        }


        [TestMethod()]
        public void UpdateNewBussinessLogicTest1()
        {
            int MenuID = 0;
            string busineslogic = string.Empty;
            bool expected = false;
            bool actual;
            actual = target.UpdateNewBussinessLogic(MenuID, busineslogic);
            Assert.AreEqual(expected, actual);

        }

        [TestMethod()]
        public void GetAccessTypeTest1()
        {

            int EmpID = 0;
            int expected = 0;
            int actual;
            actual = target.GetAccessType(EmpID);
            Assert.AreEqual(expected, actual);

        }


        [TestMethod()]
        public void DeleteUploadedDocsTest1()
        {

            int MenuID = 0;
            int DocID = 0;
            bool expected = false;
            bool actual;
            actual = target.DeleteUploadedDocs(MenuID, DocID);
            Assert.AreEqual(expected, actual);

        }




        /// <summary>
        ///     A test for GetBusinessLogicInformationByURL
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(System.ArgumentNullException))]
        public void GetBusinessLogicInformationByURLTestForNullParameter()
        {
            BussinessLogic target = new BussinessLogic();
            string URL = null;
            string Attribute = null;
            target.GetBusinessLogicInformationByURL(URL, Attribute);
        }

        /// <summary>
        ///     A test for GetBusinessLogicInformationByURL
        ///</summary>
        [TestMethod()]
        public void GetBusinessLogicInformationByURLTest()
        {
            BussinessLogic target = new BussinessLogic();
            string URL = "/Reports/PastCourtDates.aspx";
            string Attribute = "Days";
            DataTable Actual = new DataTable();
            Actual = target.GetBusinessLogicInformationByURL(URL, Attribute);
            Assert.IsNotNull(Actual);
        }



        /// <summary>
        ///A test for GetBusinessLogicSetDaysInformation
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(System.ArgumentNullException))]
        public void GetBusinessLogicSetDaysInformationForNullParamterTest()
        {
            BussinessLogic target = new BussinessLogic();
            string Attribute = null;
            string ReportID = null;
            target.GetBusinessLogicSetDaysInformation(Attribute, ReportID);
        }

        /// <summary>
        ///A test for GetBusinessLogicSetDaysInformation
        ///</summary>
        [TestMethod()]
        public void GetBusinessLogicSetDaysInformationForNullResultSet()
        {
            BussinessLogic target = new BussinessLogic();
            string Attribute = "Days";
            string ReportID = "253";
            DataTable actual = target.GetBusinessLogicSetDaysInformation(Attribute, ReportID);
            Assert.IsNotNull(actual);
        }
    }
}

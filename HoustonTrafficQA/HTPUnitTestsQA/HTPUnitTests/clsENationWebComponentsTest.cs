﻿using FrameWorkEnation.Components;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System;
using System.Data;


namespace HTPUnitTests
{
    /// <summary>
    ///This is a test class for clsENationWebComponents and is intended
    ///to contain all its Unit Tests
    ///</summary>
    ///

    [TestClass]
   public class clsENationWebComponentsTest
    {

        /// <summary>
        ///This is a Region for Method Get_DS_BySP) which  returns DataSet.
        ///This Region is intended to contain all its Unit Tests.
        ///</summary>
        ///

        #region Get_DS_BySP

        /// <summary>
        /// This Test method will Check if the Dataset is Null or Not . 
        /// </summary>

        [TestMethod]
        public void IsGet_DS_BySPDatasetNotNull()
        {
            clsENationWebComponents target = new clsENationWebComponents();
            string spName = string.Empty;
            DataSet actual = new DataSet();
            spName = "USP_HTP_Get_MissingMidNumbers";
            actual = target.Get_DS_BySP(spName);
            Assert.IsNotNull(actual);
        }

        /// <summary>
        /// This Test method will Check Type of the Dataset  . 
        /// </summary>

        [TestMethod]
        public void Get_DS_BySPDatasetType()
        {
            clsENationWebComponents target = new clsENationWebComponents();
            string spName = string.Empty;
            DataSet actual = new DataSet();
            DataSet expected = new DataSet();
            spName = "USP_HTP_Get_MissingMidNumbers";
            actual = target.Get_DS_BySP(spName);
            Assert.AreEqual(actual.GetType(), expected.GetType());
        }

        /// <summary>
        /// This Test method will Check No of Rows Return by dataset is equal or greater than Zero  . 
        /// </summary>

        [TestMethod]
        public void CheckGet_DS_BySPRows()
        {
            clsENationWebComponents target = new clsENationWebComponents();
            string spName = string.Empty;
            bool expected, actual = false;
            int expnumber = 0;
            spName = "USP_HTP_Get_MissingMidNumbers";
            expected = (expnumber >= 0) ? true : false;
            actual = (target.Get_DS_BySP(spName).Tables[0].Rows.Count >= 0) ? true : false;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// This Test method will Check that the No of columns are equal to 5  . 
        /// </summary>

        [TestMethod]
        public void Get_DS_BySPColumnCount()
        {
            clsENationWebComponents target = new clsENationWebComponents();
            string spName = string.Empty;
            int expected = 0, actual = 0;
            spName = "USP_HTP_Get_MissingMidNumbers";
            expected = 5;
            actual = (target.Get_DS_BySP(spName).Tables[0].Columns.Count);
            Assert.AreEqual(expected, actual);

        }

        #endregion

        # region  Get_DS_BySPArr

        /// <summary>
        /// This Test method will Check if the Dataset is Null or Not . 
        /// </summary>

        [TestMethod]
        public void IsGet_DS_BySPArrDatasetNotNull_HMCDLQAlert()
        {
            clsENationWebComponents target = new clsENationWebComponents();
            string spName = string.Empty;
            string[] keys = { "@showAll", "@validation" };
            object[] values = { 0, 0 };
            DataSet actual = new DataSet();
            spName = "usp_hts_HMC_AW_DLQ_Alert";
            actual = target.Get_DS_BySPArr(spName,keys,values);
            Assert.IsNotNull(actual);
        }

        /// <summary>
        /// This Test method will Check Type of the Dataset  . 
        /// </summary>

        [TestMethod]
        public void Get_DS_BySPArrDatasetType__HMCDLQAlert()
        {
            clsENationWebComponents target = new clsENationWebComponents();
            string spName = string.Empty;
            DataSet actual = new DataSet();
            string[] keys = { "@showAll", "@validation" };
            object[] values = { 0, 0 };
            DataSet expected = new DataSet();
            spName = "usp_hts_HMC_AW_DLQ_Alert";
            actual = target.Get_DS_BySPArr(spName,keys,values);
            Assert.AreEqual(actual.GetType(), expected.GetType());
        }

        /// <summary>
        /// This Test method will Check No of Rows Return by dataset is equal or greater than Zero  . 
        /// </summary>

        [TestMethod]
        public void CheckGet_DS_BySPArrRows__HMCDLQAlert()
        {
            clsENationWebComponents target = new clsENationWebComponents();
            string spName = string.Empty;
            string[] keys = { "@showAll", "@validation" };
            object[] values = { 0, 0 };
            bool expected, actual = false;
            int expnumber = 0;
            spName = "usp_hts_HMC_AW_DLQ_Alert";
            expected = (expnumber >= 0) ? true : false;
            actual = (target.Get_DS_BySPArr(spName,keys,values).Tables[0].Rows.Count >= 0) ? true : false;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// This Test method will Check that the No of columns are equal to 19  . 
        /// </summary>

        [TestMethod]
        public void Get_Get_DS_BySPArrColumnCount__HMCDLQAlert()
        {
            clsENationWebComponents target = new clsENationWebComponents();
            string spName = string.Empty;
            string[] keys = { "@showAll", "@validation" };
            object[] values = { 0, 0 };
            int expected = 0, actual = 0;
            spName = "usp_hts_HMC_AW_DLQ_Alert";
            expected = 19;
            actual = (target.Get_DS_BySPArr(spName,keys,values).Tables[0].Columns.Count);
            Assert.AreEqual(expected, actual);

        }


        /// <summary>
        /// This Test method will Check if the Dataset is Null or Not . 
        /// </summary>

        [TestMethod]
        public void IsGet_DS_BySPArrDatasetNotNull_HMCBondWaiting()
        {
            clsENationWebComponents target = new clsENationWebComponents();
            string spName = string.Empty;
            string[] keys = {  "@validation","@showAll" };
            object[] values = { 0, 0 };
            DataSet actual = new DataSet();
            spName = "USP_HTS_Get_BondWaitingViolations";
            actual = target.Get_DS_BySPArr(spName, keys, values);
            Assert.IsNotNull(actual);
        }

        /// <summary>
        /// This Test method will Check Type of the Dataset  . 
        /// </summary>

        [TestMethod]
        public void Get_DS_BySPArrDatasetType__HMCBondWaiting()
        {
            clsENationWebComponents target = new clsENationWebComponents();
            string spName = string.Empty;
            DataSet actual = new DataSet();
            string[] keys = { "@validation", "@showAll" };
            object[] values = { 0, 0 };
            DataSet expected = new DataSet();
            spName = "USP_HTS_Get_BondWaitingViolations";
            actual = target.Get_DS_BySPArr(spName,keys,values);
            Assert.AreEqual(actual.GetType(), expected.GetType());
        }

        /// <summary>
        /// This Test method will Check No of Rows Return by dataset is equal or greater than Zero  . 
        /// </summary>

        [TestMethod]
        public void CheckGet_DS_BySPArrRows__HMCBondWaiting()
        {
            clsENationWebComponents target = new clsENationWebComponents();
            string spName = string.Empty;
            string[] keys = { "@validation", "@showAll" };
            object[] values = { 0, 0 };
            bool expected, actual = false;
            int expnumber = 0;
            spName = "USP_HTS_Get_BondWaitingViolations";
            expected = (expnumber >= 0) ? true : false;
            actual = (target.Get_DS_BySPArr(spName,keys,values).Tables[0].Rows.Count >= 0) ? true : false;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// This Test method will Check that the No of columns are equal to 17  . 
        /// </summary>

        [TestMethod]
        public void Get_Get_DS_BySPArrColumnCount__HMCBondWaiting()
        {
            clsENationWebComponents target = new clsENationWebComponents();
            string spName = string.Empty;
            string[] keys = { "@validation", "@showAll" };
            object[] values = { 0, 0 };
            int expected = 0, actual = 0;
            spName = "USP_HTS_Get_BondWaitingViolations";
            expected = 18;
            actual = (target.Get_DS_BySPArr(spName,keys,values).Tables[0].Columns.Count);
            Assert.AreEqual(expected, actual);

        }






        //Yasir Kamal 5744 04/06/2009 Test Cases

        /// <summary>
        /// This Test method will Check if the Dataset is Null or Not . 
        /// </summary>

        [TestMethod]
        public void DatasetNotNull_QouteCallBack()
        {
            clsENationWebComponents target = new clsENationWebComponents();
            string spName = string.Empty;
            string[] keys = { "@DateFrom", "@DateTo", "@casetypeid" };
            object[] values = { "04/08/2009","04/08/2009","1" };
            DataSet actual = new DataSet();
            spName = "usp_HTS_Get_All_DayBeforeCourtDate";
            actual = target.Get_DS_BySPArr(spName, keys, values);
            Assert.IsNotNull(actual);
        }

        /// <summary>
        /// This Test method will Check Type of the Dataset  . 
        /// </summary>

        [TestMethod]
        public void DatasetType_QouteCallBack()
        {
            clsENationWebComponents target = new clsENationWebComponents();
            string spName = string.Empty;
            DataSet actual = new DataSet();
            string[] keys = { "@DateFrom", "@DateTo", "@casetypeid" };
            object[] values = { "04/08/2009", "04/08/2009", "1" };
            DataSet expected = new DataSet();
            spName = "usp_HTS_Get_All_DayBeforeCourtDate";
            actual = target.Get_DS_BySPArr(spName, keys, values);
            Assert.AreEqual(actual.GetType(), expected.GetType());
        }

        /// <summary>
        /// This Test method will Check No of Rows Return by dataset is equal or greater than Zero  . 
        /// </summary>

        [TestMethod]
        public void Rows_QouteCallBack()
        {
            clsENationWebComponents target = new clsENationWebComponents();
            string spName = string.Empty;
            string[] keys = { "@DateFrom", "@DateTo", "@casetypeid" };
            object[] values = { "04/08/2009", "04/08/2009", "1" };
            bool expected, actual = false;
            int expnumber = 0;
            spName = "usp_HTS_Get_All_DayBeforeCourtDate";
            expected = (expnumber >= 0) ? true : false;
            actual = (target.Get_DS_BySPArr(spName, keys, values).Tables[0].Rows.Count >= 0) ? true : false;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// This Test method will Check that the No of columns are equal to 18  . 
        /// </summary>

        [TestMethod]
        public void ColumnCount_QouteCallBack()
        {
            clsENationWebComponents target = new clsENationWebComponents();
            string spName = string.Empty;
            string[] keys = { "@DateFrom", "@DateTo", "@casetypeid" };
            object[] values = { "04/08/2009", "04/08/2009", "1" };
            int expected = 0, actual = 0;
            spName = "usp_HTS_Get_All_DayBeforeCourtDate";
            expected = 18;
            actual = (target.Get_DS_BySPArr(spName, keys, values).Tables[0].Columns.Count);
            Assert.AreEqual(expected, actual);

        }


        
        #endregion


        #region ExecuteSP
        //Waqas 6342 08/26/2009 for alr criminal
        /// <summary>
        ///A test for ExecuteSP to update email sent flag.
        ///</summary>
        [TestMethod()]
        public void ExecuteSPTest()
        {
            clsENationWebComponents target = new clsENationWebComponents();
            string spName = "usp_htp_update_EmailSentFlag";
            string[] key = { "@TicketID" };
            object[] value1 = { 5065 }; 
            bool AllowNullValues = false; 
            target.ExecuteSP(spName, key, value1, AllowNullValues);            
        }


        /// <summary>
        ///A test for ExecuteSP to update status of the case.
        ///</summary>
        [TestMethod()]
        public void ExecuteSPTestStaus()
        {
            clsENationWebComponents target = new clsENationWebComponents();
            string spName = "USp_Htp_UpdateStatus";
            string[] key = { "@ticketid_pk", "@courtviolationstatus", "@empid" };
            object[] value1 = { 5065 , 104, 3991};
            bool AllowNullValues = false;
            target.ExecuteSP(spName, key, value1, AllowNullValues);
        }

        #endregion 
    }
}

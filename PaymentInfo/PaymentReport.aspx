﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PaymentReport.aspx.cs"
    Inherits="HTP.PaymentInfo.PaymentReport" %>

<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Payment Report</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="../Styles.css" type="text/css" rel="stylesheet">

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>

    <script src="../Scripts/Dates.js" type="text/javascript"></script>

    <script type="text/javascript" language="javascript">
        
        function SetFlag(Control,Value)
		{
		    document.getElementById(Control).value = Value ;		
		}
        function show( divid )
		{
			divid.style.display = 'block';
			divid.style.margin = '1.5px 0px';
		}

		function hide( divid )
		{
			divid.style.display = 'none';
			divid.style.margin = '0px';
		}
		
		function DefaultLoad()
		{
            
             // TRANSACTION DETAIL SECTION......
		    if (document.getElementById("txtAttorneyDetails").value == 1)
		    {
			    show(Div2);
			    show(Div3);
			    hide(Div1);			
		    }
		    else
		    {
			    hide(Div2);
			    hide(Div3);
			    show(Div1);		
		    }
		    
        
            // TRANSACTION DETAIL SECTION......
		    if (document.getElementById("txtTransactionDetail").value == 1)
		    {
			    show(HB2);
			    show(HB3);
			    hide(HB1);			
		    }
		    else
		    {
			    hide(HB2);
			    hide(HB3);
			    show(HB1);		
		    }
		    
		    // COURT SUMMARY SECTION......
			if (document.getElementById("txtCS").value == 1)
			{
				show(HC2);
				show(HC3);
				hide(HC1);			
			}
			else
			{
				hide(HC2);
				hide(HC3);
				show(HC1);		
			}	
    	}
				
    </script>

    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style3
        {
            width: 640px;
        }
        .style4
        {
            width: 142px;
        }
        .style5
        {
            font-weight: bold;
            color: #006699;
            font-family: Verdana, Arial, Helvetica, sans;
            font-size: 8pt;
            width: 81px;
        }
        .style6
        {
            width: 136px;
        }
        TABLE
        {
            font: 11px verdana,arial,helvetica,sans-serif;
        }
        DIV.divHidden
        {
            display: none;
            margin: 0px;
        }
        DIV
        {
            margin: 0px;
        }
    </style>
</head>
<body onload="DefaultLoad();">
    <form id="form1" runat="server">
    <div>
        <table id="tblMain" cellspacing="0" cellpadding="0" width="780" align="center" border="0"
            runat="server">
            <tr>
                <td>
                    <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="style1">
                        <tr>
                            <td class="style5">
                                Date
                            </td>
                            <td class="style4">
                                <ew:CalendarPopup ID="calDateFrom" runat="server" ImageUrl="../images/calendar.gif"
                                    Width="90px" ToolTip="Select Report Date Range" PadSingleDigits="True" Culture="(Default)"
                                    AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Bottom" ControlDisplay="TextBoxImage"
                                    Font-Names="Tahoma" Font-Size="8pt" DisableTextboxEntry="false">
                                    <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                    <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                        BackColor="White"></WeekdayStyle>
                                    <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                    <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                        BackColor="AntiqueWhite"></OffMonthStyle>
                                    <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                    <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                        BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                    <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                    <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                        BackColor="LightGray"></WeekendStyle>
                                    <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                    <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="White"></ClearDateStyle>
                                    <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                        BackColor="White"></HolidayStyle>
                                </ew:CalendarPopup>
                            </td>
                            <td class="style6">
                                <ew:CalendarPopup ID="calDateTo" runat="server" ImageUrl="../images/calendar.gif"
                                    Width="90px" ToolTip="Select Report Date Range" PadSingleDigits="True" Culture="(Default)"
                                    AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Bottom" ControlDisplay="TextBoxImage"
                                    Font-Names="Tahoma" Font-Size="8pt" DisableTextboxEntry="false">
                                    <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                    <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                        BackColor="White"></WeekdayStyle>
                                    <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                    <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                        BackColor="AntiqueWhite"></OffMonthStyle>
                                    <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                    <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                        BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                    <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                    <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                        BackColor="LightGray"></WeekendStyle>
                                    <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                    <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="White"></ClearDateStyle>
                                    <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                        BackColor="White"></HolidayStyle>
                                </ew:CalendarPopup>
                            </td>
                            <td>
                                <asp:Button ID="btnSearch" runat="server" CssClass="clsbutton" Text="Search" OnClick="btnSearch_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td class="style5">
                                &nbsp;
                            </td>
                            <td class="style4">
                                &nbsp;
                            </td>
                            <td class="style6">
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5" class="TDHeading" valign="middle">
                                <div id="Div1">
                                    &nbsp; <span onclick="SetFlag('txtAttorneyDetails',1);show(Div2);show(Div3);hide(Div1);">
                                        +</span>
                                    <asp:TextBox ID="Textbox3" runat="server" CssClass="ProductHead" ReadOnly="True">Attorney Details</asp:TextBox></div>
                                <div class="divHidden" id="Div2">
                                    &nbsp;<span onclick="SetFlag('txtTransactionDetail',0);show(Div1);hide(Div2);hide(Div3);">
                                        -</span>&nbsp;&nbsp;<asp:TextBox ID="Textbox4" runat="server" CssClass="ProductHead"
                                            ReadOnly="True">Attorney Details</asp:TextBox></div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5" style="height: 3px">
                                <div class="divHidden" id="Div3">
                                    <table id="tblAttorneyDetails" cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tr>
                                            <td>
                                                <asp:DataList ID="dlFirms" runat="server" OnItemDataBound="dlFirms_ItemDataBound">
                                                    <HeaderTemplate>
                                                        <b class="clssubhead">Covering Firm Name<b>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFirmName" runat="server" Text='<%# Bind("firmabbreviation") %>'
                                                            CssClass="Label"></asp:Label>
                                                        <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" Width="100%"
                                                            AllowPaging="True" CssClass="clsLeftPaddingTable" OnRowDataBound="gvData_RowDataBound">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Courts">
                                                                    <HeaderStyle CssClass="clssubhead" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblCourts" runat="server" CssClass="Label" Text='<%#bind("CourtName") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        <asp:Label ID="lblFooterCourt" runat="server" CssClass="Label" Text="Total"></asp:Label>
                                                                    </FooterTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Count">
                                                                    <HeaderStyle CssClass="clssubhead" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblCount" runat="server" CssClass="Label" Text='<%#bind("Clients") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        <asp:Label ID="lblTotalCount" runat="server" CssClass="Label"></asp:Label>
                                                                    </FooterTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Amount">
                                                                    <HeaderStyle CssClass="clssubhead" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblAmount" runat="server" CssClass="Label" Text='<%#bind("Amount") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        <asp:Label ID="lblTotalAmount" runat="server" CssClass="Label"></asp:Label>
                                                                    </FooterTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Rate">
                                                                    <HeaderStyle CssClass="clssubhead" />
                                                                    <FooterTemplate>
                                                                        <asp:Label ID="lblTotalRate" runat="server" CssClass="Label" Text="50%"></asp:Label>
                                                                    </FooterTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Commision">
                                                                    <HeaderStyle CssClass="clssubhead" />
                                                                    <FooterTemplate>
                                                                        <asp:Label ID="lblTotalCommision" runat="server" CssClass="Label"></asp:Label>
                                                                    </FooterTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </ItemTemplate>
                                                </asp:DataList>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="TDHeading" colspan="5" valign="middle">
                                <div id="HB1">
                                    &nbsp; <span onclick="SetFlag('txtTransactionDetail',1);show(HB2);show(HB3);hide(HB1);">
                                        +</span>
                                    <asp:TextBox ID="Textbox1" runat="server" CssClass="ProductHead" ReadOnly="True">Transaction Details</asp:TextBox></div>
                                <div class="divHidden" id="HB2">
                                    &nbsp;<span onclick="SetFlag('txtTransactionDetail',0);show(HB1);hide(HB2);hide(HB3);">
                                        -</span>&nbsp;&nbsp;<asp:TextBox ID="Textbox2" runat="server" CssClass="ProductHead"
                                            ReadOnly="True">Transaction Details</asp:TextBox></div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5" style="height: 3px">
                                <div id="HB3">
                                    <table id="Table7" cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tr>
                                            <td style="border-right: gray 1px solid; border-top: 1px solid; border-left: 1px solid;
                                                height: 25px;" align="center" bgcolor="#eeeeee" colspan="5">
                                                <asp:DropDownList ID="cmbRep" runat="server" Width="100px" CssClass="frmtd" 
                                                    AutoPostBack="True" onselectedindexchanged="cmbRep_SelectedIndexChanged" >
                                                </asp:DropDownList>
                                                &nbsp;&nbsp;
                                                <asp:DropDownList ID="cmbPayType" runat="server" Width="280px" CssClass="frmtd" 
                                                    AutoPostBack="True" onselectedindexchanged="cmbPayType_SelectedIndexChanged" >
                                                </asp:DropDownList>
                                                &nbsp;&nbsp;
                                                <asp:DropDownList ID="cmbCourt" runat="server" Width="150px" CssClass="frmtd" 
                                                    AutoPostBack="True" onselectedindexchanged="cmbCourt_SelectedIndexChanged" >
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="gvTransactionDetail" runat="server" Width="100%" Font-Names="Verdana"
                                                    Font-Size="2px" AutoGenerateColumns="False" OnItemCommand="DoGetQueryString"
                                                    AllowCustomPaging="True" PageSize="100" AllowSorting="True" OnRowDataBound="gvTransactionDetail_RowDataBound"
                                                    OnRowCommand="gvTransactionDetail_RowCommand" OnSorting="gvTransactionDetail_Sorting">
                                                    <AlternatingRowStyle BackColor="#EEEEEE"></AlternatingRowStyle>
                                                    <HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle">
                                                    </HeaderStyle>
                                                    <FooterStyle CssClass="GrdFooter"></FooterStyle>
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="No.">
                                                            <HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Center" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                                Font-Strikeout="False" Font-Underline="False" Wrap="False"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTicketID" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.TicketID") %>'
                                                                    Visible="false">
                                                                </asp:Label>
                                                                <asp:Label ID="lblCardTypeID" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.CardType") %>'
                                                                    Visible="false">
                                                                </asp:Label>
                                                                <asp:Label ID="lblNo" runat="server" CssClass="GrdLbl" Text="" Visible="True"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Date">
                                                            <HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Center" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                                Font-Strikeout="False" Font-Underline="False" Wrap="False"></ItemStyle>
                                                            <ItemTemplate>
                                                                &nbsp;<asp:Label ID="lblDate" runat="server" CssClass="GrdLbl" Visible="true" Text='<%# DataBinder.Eval(Container, "DataItem.RecDate") %>'>
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Time" SortExpression="sorttime">
                                                            <HeaderStyle HorizontalAlign="Center" CssClass="TaskGrid_Head"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Right" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                                Font-Strikeout="False" Font-Underline="False" Wrap="False"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTime" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.RecTime") %>'
                                                                    Visible="true">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField SortExpression="CustomerName" HeaderText="Client Name">
                                                            <HeaderStyle HorizontalAlign="Left" CssClass="TaskGrid_Head"></HeaderStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCustomer" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.CustomerName") %>'
                                                                    Visible="false">
                                                                </asp:Label>
                                                                <asp:LinkButton ID="lnkCustomer" runat="server" CssClass="ProductGrdLinks" ForeColor="black"
                                                                    CommandName="DoGetCustomer" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.TicketID") %>'>
															<%# DataBinder.Eval(Container, "DataItem.CustomerName") %>
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                Font-Underline="False" Wrap="False" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField SortExpression="Lastname" HeaderText="Rep">
                                                            <HeaderStyle HorizontalAlign="Left" CssClass="TaskGrid_Head"></HeaderStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblRep" runat="server" CssClass="GrdLbl" Visible="true" Text='<%# DataBinder.Eval(Container, "DataItem.Lastname") %>'>
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                Font-Underline="False" Wrap="False" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField SortExpression="bondflag" HeaderText="Bond">
                                                            <HeaderStyle HorizontalAlign="Left" CssClass="TaskGrid_Head"></HeaderStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblBond" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.BondFlag") %>'
                                                                    Visible="true">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                Font-Underline="False" Wrap="False" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField SortExpression="ChargeAmount" HeaderText="Paid">
                                                            <HeaderStyle HorizontalAlign="Left" CssClass="TaskGrid_Head"></HeaderStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPaidAmount" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.ChargeAmount", "{0:C0}") %>'>
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                Font-Underline="False" Wrap="False" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Adj1">
                                                            <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="Label1" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.adj1") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                Font-Underline="False" Wrap="False" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Adj2">
                                                            <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="Label2" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.adj2") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                Font-Underline="False" Wrap="False" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField SortExpression="PaymentType" HeaderText="Payment Type">
                                                            <HeaderStyle HorizontalAlign="Left" CssClass="TaskGrid_Head"></HeaderStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPayTypeFR" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.CCType") %>'
                                                                    Visible="true">
                                                                </asp:Label>
                                                                <asp:Label ID="lblPTypeId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentType") %>'
                                                                    Visible="False">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                Font-Underline="False" Wrap="False" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Card">
                                                            <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCardType" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.ChargeAmount") %>'
                                                                    Visible="true">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                Font-Underline="False" Wrap="False" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Court">
                                                            <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCourt" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.Court") %>'
                                                                    Visible="true">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                Font-Underline="False" Wrap="False" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="List">
                                                            <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblListDate" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.ListDate") %>'
                                                                    Visible="true">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                Font-Underline="False" Wrap="False" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Letter">
                                                            <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_MailType" Text='<%# DataBinder.Eval(Container, "DataItem.MailType") %>'
                                                                    runat="server" CssClass="GrdLbl" Visible="true">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Date">
                                                            <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_MailDate" Text='<%# DataBinder.Eval(Container, "DataItem.MailDate") %>'
                                                                    runat="server" CssClass="GrdLbl" Visible="true">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="5" height="20">
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="TDHeading" colspan="5">
                                <div id="HC1">
                                    &nbsp;<span onclick="SetFlag('txtCS',1);show(HC2);show(HC3);hide(HC1);">+</span>&nbsp;&nbsp;<asp:TextBox
                                        ID="Textbox5" runat="server" CssClass="ProductHead" ReadOnly="True"> Court Summary</asp:TextBox></div>
                                <div class="divHidden" id="HC2">
                                    &nbsp;<span onclick="SetFlag('txtCS',0);show(HC1);hide(HC2);hide(HC3);"> -</span>&nbsp;&nbsp;<asp:TextBox
                                        ID="Textbox6" runat="server" CssClass="ProductHead" ReadOnly="True"> Court Summary</asp:TextBox></div>
                            </td>
                        </tr>
                        <tr>
                            <td width="100%" colspan="5">
                                <div class="divHidden" id="HC3">
                                    <!--  tahir   -->
                                    <table id="Table61" cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tr>
                                            <td valign="top" align="left">
                                                <table id="Table21" cellspacing="0" cellpadding="0" width="100%" border="0">
                                                    <tr>
                                                        <td colspan="5" height="20">
                                                            <asp:GridView ID="gvCourtSummary" runat="server" Width="100%" Font-Names="Verdana"
                                                                Font-Size="2px" border="0" AutoGenerateColumns="False" ShowFooter="True" OnRowCommand="gvCourtSummary_RowCommand"
                                                                CellPadding="0" CellSpacing="0">
                                                                <AlternatingRowStyle BackColor="#EEEEEE"></AlternatingRowStyle>
                                                                <HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle">
                                                                </HeaderStyle>
                                                                <FooterStyle CssClass="GrdFooter"></FooterStyle>
                                                                <Columns>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>
                                                                            <table style="font-size: 2px; width: 100%; font-family: Verdana; border-collapse: collapse"
                                                                                cellspacing="0" rules="cols" border="1">
                                                                                <tr>
                                                                                    <td class="GrdHeader" align="left" width="60%">
                                                                                        Court Location
                                                                                    </td>
                                                                                    <td class="GrdHeader" align="center" width="15%">
                                                                                        Transactions
                                                                                    </td>
                                                                                    <td class="GrdHeader" align="center" width="20%">
                                                                                        Fee Amount&nbsp;&nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <table style="font-size: 2px; width: 100%; font-family: Verdana; border-collapse: collapse"
                                                                                bordercolor="gainsboro" cellspacing="0" cellpadding="0" rules="all" border="1">
                                                                                <tr id="trDetail" runat="server">
                                                                                    <td valign="top" align="left" width="60%">
                                                                                        <table style="border-top-style: none; border-right-style: none; border-left-style: none;
                                                                                            border-bottom-style: none" cellspacing="0" cellpadding="0" width="416" border="0">
                                                                                            <tr>
                                                                                                <td id="iParent" style="display: none" align="left" width="10" runat="server">
                                                                                                    <asp:Image ID="imgParent" ImageUrl="../Images/folderopen.gif" runat="server"></asp:Image>
                                                                                                </td>
                                                                                                <td id="iLink" style="display: none" align="left" width="10" runat="server">
                                                                                                    <asp:Image ID="imgLink" ImageUrl="../Images/T.gif" runat="server"></asp:Image>
                                                                                                </td>
                                                                                                <td id="iChild" style="display: none" align="left" width="10" runat="server">
                                                                                                    <asp:Image ID="imgChild" ImageUrl="../Images/folder.gif" runat="server"></asp:Image>
                                                                                                </td>
                                                                                                <td class="style1">
                                                                                                    &nbsp;<asp:Label ID="lblCourtID" CssClass="GrdLbl" runat="server" Visible="False"
                                                                                                        Text='<%# DataBinder.Eval(Container, "DataItem.CurrentCourtloc") %>'>
                                                                                                    </asp:Label>
                                                                                                    <asp:Label ID="lblIsCourtCategory" CssClass="GrdLbl" runat="server" Style="display: none"
                                                                                                        Text='<%# DataBinder.Eval(Container, "DataItem.IsCourtCategory") %>'>
                                                                                                    </asp:Label>
                                                                                                    <asp:Label ID="lblCategoryNumber" CssClass="GrdLbl" runat="server" Style="display: none"
                                                                                                        Text='<%# DataBinder.Eval(Container, "DataItem.CategoryNumber") %>'>
                                                                                                    </asp:Label>
                                                                                                    <asp:Label ID="lblCourtName" CssClass="GrdLbl" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.CourtName") %>'>
                                                                                                    </asp:Label>
                                                                                                    <asp:LinkButton ID="lnkbtnCourt" runat="server" ForeColor="black" CssClass="ProductGrdLinks"
                                                                                                        CommandName="DoGetCourt" Text='<%# DataBinder.Eval(Container, "DataItem.CourtName") %>' CommandArgument ='<%# DataBinder.Eval(Container, "DataItem.CurrentCourtloc") %>'>
                                                                                                    </asp:LinkButton>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td align="center" width="15%">
                                                                                        <asp:Label ID="lblTrans" CssClass="GrdLbl" runat="server" Visible="True" Text='<%# DataBinder.Eval(Container, "DataItem.Trans") %>'>
                                                                                        </asp:Label>
                                                                                    </td>
                                                                                    <td align="right" width="20%">
                                                                                        <asp:Label ID="lblFee" CssClass="GrdLbl" runat="server" Visible="true" Text='<%# DataBinder.Eval(Container, "DataItem.Amount") %>'>
                                                                                        </asp:Label>&nbsp;&nbsp;&nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </ItemTemplate>
                                                                        <FooterTemplate>
                                                                            <table style="font-size: 2px; width: 100%; font-family: Verdana; border-collapse: collapse"
                                                                                bordercolor="silver" cellspacing="0" rules="cols" border="1">
                                                                                <tr>
                                                                                    <td align="left" width="60%">
                                                                                        <asp:Label ID="Label1" runat="server" CssClass="grdheader">Total</asp:Label>
                                                                                    </td>
                                                                                    <td align="center" width="15%">
                                                                                        <asp:Label ID="lbl_Count" runat="server" CssClass="GrdLbl"></asp:Label>
                                                                                    </td>
                                                                                    <td align="right" width="20%">
                                                                                        <asp:Label ID="lbl_Amount" runat="server" CssClass="GrdLbl"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </FooterTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height: 20px" colspan="5">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height: 2px" valign="top" colspan="5">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txtTransactionDetail" runat="server" Width="0px" Height="18px">1</asp:TextBox>
                    <asp:TextBox ID="txtCS" runat="server" Width="0px" Height="32px">1</asp:TextBox>
                    <asp:TextBox ID="txtAttorneyDetails" runat="server" Width="0px" Height="32px">1</asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.PaymentInfo.FinancialReps" smartNavigation="False" Codebehind="FinancialReps.aspx.cs" %>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >



    


<HTML>
	<HEAD>
		<title>Financial Reps. Report</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<SCRIPT src="../Scripts/Validationfx.js" type="text/javascript"></SCRIPT>
		<SCRIPT src="../Scripts/Dates.js" type="text/javascript"></SCRIPT>
		<script language="javascript">
				
		//Date validation code
		function isDate()
{
     
	
	var startdate = document.getElementById("calQueryDate").value;
	var enddate = document.getElementById("calTo").value;
	
	if(validate_date(enddate)==true && validate_date(startdate)==true)
	 {
	    return true;
	 }
	 
	 else
	 {
	    return false;
	 }
	
}
function validate_date(inputwidget)
{
    
	var datestring = inputwidget;
	var first = datestring.indexOf('/');
	var last = datestring.lastIndexOf('/');

    if ( first == -1 ) 
    {
        alert("The date entered is not valid and date must be in the mm/dd/yyyy format.");
		return false;
    }
    if ( last == -1 ) 
    {
        alert("The date entered is not valid and date must be in the mm/dd/yyyy format.");
	    return false;
    }
    

	    var daypart= datestring.substring(0, first);
	    var monthpart  = datestring.substring(first + 1, last);
	    var yearpart = datestring.substring(last + 1, datestring.length);
	   
    	
	    if ((datestring.length > 0) && 
	         ((yearpart.length < 4) || (yearpart.length>4) || (yearpart<1900) || !isvaliddate(yearpart, monthpart, daypart)))  
	        
	    {
		   
		    inputwidget.value = "";
		    alert("The date entered is not valid and date must be in the mm/dd/yyyy format.");
		    return false;
	    }
	    else if(isNaN(yearpart))
	    {
	        alert("The date entered is not valid and date must be in the mm/dd/yyyy format.");
	        return false;
	    }
	    else if(isNaN(monthpart))
	    {
	        alert("The date entered is not valid and date must be in the mm/dd/yyyy format.");
	        return false;
	    }
	    else if(isNaN(daypart))
	    {
	        alert("The date entered is not valid and date must be in the mm/dd/yyyy format.");
	        return false;
	    }
	    else
	    {
	
		    return true;
	    }
	    

function isvaliddate(year, month, day)
{
	//return (month >= 1) && (month <= 12) && (day >= 1) && (day <= mostdaysinmonth(month, year));
	return (day >= 1) && (day <= 12) && (month >= 1) && (month <= mostdaysinmonth(day, year));
}
    	
	
	
}


//End of Date validation

		
		function validate()
		{
			var d1 = document.getElementById("calQueryDate").value
			var d2 = document.getElementById("calTo").value			
			
			if (d1 == d2)
				return true;
			if (compareDates(d2,'MM/dd/yyyy',d1,'MM/dd/yyyy')==false)
				{
					alert("start date cannot greater than end date");
					document.getElementById("calTo").focus(); 
					return false;
				}
				return true;
		}
		
		function ShowHideCCDetail(paytype)
		{  			
			
			var txt = document.getElementById("txtCC");	
			var txtcheck=document.getElementById("txtCheck");
			
			if (paytype==5)
			{
			if (txt.value  == '0')
				txt.value = '1';
			else
				txt.value = '0';
			}
			else if(paytype==200)
			{
	
			if (txtcheck.value == '0')
				txtcheck.value = '1';
			else
				txtcheck.value = '0';
		}
		
			var cnt = parseInt(document.getElementById("iCount").value);
			var grdName = "dgrdPayType";
			var idx=2;
			cnt = cnt + 2;

			for (idx=2; idx < (cnt); idx++)
			{
				var tr = ""; 
				var paytype = "";
				var img1 = "";
				var img2 = "";
				var img3 = "";
				 
				if( idx < 10)
				{
				 tr = grdName+ "_ctl0" + idx + "_trDetail";
				 paytype = grdName+ "_ctl0" + idx + "_lblPayTypeID";
				 img1 = grdName+ "_ctl0" + idx + "_iParent";
				 img2 = grdName+ "_ctl0" + idx + "_iChild";
				 img3 = grdName+ "_ctl0" + idx + "_iLink";
				
				}
				else
				{
				 tr = grdName+ "_ctl" + idx + "_trDetail";
				 paytype = grdName+ "_ctl" + idx + "_lblPayTypeID";
				 img1 = grdName+ "_ctl" + idx + "_iParent";
				 img2 = grdName+ "_ctl" + idx + "_iChild";
				 img3 = grdName+ "_ctl" + idx + "_iLink";
				}

				var trCC = document.getElementById(tr);
				var pType = document.getElementById(paytype);
				var img11 = document.getElementById(img1);
				var img22 = document.getElementById(img2);
				var img33 = document.getElementById(img3);
				
				pType = parseInt(pType.innerText);
		
				
				if (pType > 500 )
				{				
					if (txt.value == '1')
						{
						trCC.style.display = 'block';
						img11.style.display = 'none';
						img22.style.display = 'block';
						img33.style.display = 'block';
						
						}
					else
						{
						trCC.style.display = 'none';
						img11.style.display = 'none';
						img22.style.display = 'none';
						img33.style.display = 'none';
						trCC.style.margin= '0px';						

						}
				}
				 if (pType == 2)
				{ 
					if (txtcheck.value == '1')
					{ 
						trCC.style.display = 'block';
						img11.style.display = 'none';
						img22.style.display = 'block';
						img33.style.display = 'block';
					}
					else
					{ 
						trCC.style.display = 'none';
						img11.style.display = 'none';
						img22.style.display = 'none';
						img33.style.display = 'none';
						trCC.style.margin= '0px';	
					}
				}
				 if (pType == 4)
				{ 
					if (txtcheck.value == '1')
					{ 
						trCC.style.display = 'block';
						img11.style.display = 'none';
						img22.style.display = 'block';
						img33.style.display = 'block';
					}
					else
					{ 
						trCC.style.display = 'none';
						img11.style.display = 'none';
						img22.style.display = 'none';
						img33.style.display = 'none';
						trCC.style.margin= '0px';	
					}
				}

			}
			
		}




		

		function CheckMail()
		{
			var Email;
			//a;
			Email= document.getElementById("txtEmail").value;
			if ((Email == '')  || !(isEmail(Email)))
			{
				alert ("Please Enter valid email address");				
				return false;
			}
			return true;		
		}
		
		function ShowEmailBox()
		{		
			if (document.getElementById("TableEmail").style.display=='none')
			{
				document.getElementById("TableEmail").style.display='block'	;						
			}
			else
			{
				document.getElementById("TableEmail").style.display='none'	;				
			}		
		}		
				
		
		function ShowViolationFee(TicketID)
		{
			//a;
				//strURL = "../../ClientInfo/violationsfees.asp?caseNumber=" & intTicketID
			var strURL= "//ClientInfo/violationsfees.asp?caseNumber=" & TicketID;
			window.navigate(strURL);				
		}
			
			
		function show( divid )
		{
			divid.style.display = 'block';
			divid.style.margin = '1.5px 0px';
		}

		function hide( divid )
		{
			divid.style.display = 'none';
			divid.style.margin = '0px';
		}
		
		function DefaultLoad()
		{		

			//document.getElementById("txtPC").style.display='none';
			//document.getElementById("txtPR").style.display='none';

			// PAYMENT SUMMARY SECTION.....
			if (document.getElementById("txtPC").value == 1)			
			{
				show(HA2);
				show(HA3);
				hide(HA1);			
			}
			else
			{
				hide(HA2);
				hide(HA3);
				show(HA1);		
			}
			
			// TRANSACTION DETAIL SECTION......
			if (document.getElementById("txtPR").value == 1)
			{
				show(HB2);
				show(HB3);
				hide(HB1);			
			}
			else
			{
				hide(HB2);
				hide(HB3);
				show(HB1);		
			}			

			// REP. SUMMARY SECTION.....
			if (document.getElementById("txtRS").value == 1)
			{
				show(HAA2);
				show(HAA3);
				hide(HAA1);			
			}
			else
			{
				hide(HAA2);
				hide(HAA3);
				show(HAA1);		
			}			

			// COURT SUMMARY SECTION......
			if (document.getElementById("txtCS").value == 1)
			{
				show(HC2);
				show(HC3);
				hide(HC1);			
			}
			else
			{
				hide(HC2);
				hide(HC3);
				show(HC1);		
			}			
			
			// CATEGORY SUMMARY SECTION.....
			if (document.getElementById("txtCT").value == 1)
			{
				show(HD2);
				show(HD3);
				hide(HD1);			
			}
			else
			{
				hide(HD2);
				hide(HD3);
				show(HD1);		
			}	
		
			// CREDIT SUMMARY SECTION.....
			if (document.getElementById("txtCSS").value == 1)
			{
				show(CSS2);
				show(CSS3);
				hide(CSS1);			
			}
			else
			{
				hide(CSS2);
				hide(CSS3);
				show(CSS1);		
			}
			
			// Void Transaction Summary
			if (document.getElementById("txtVP").value == 1)
			{
				show(VPP);
				show(VP1);
				hide(VP);			
			}
			else
			{
				hide(VPP);
				hide(VP1);
				show(VP);		
			}
			
			// for Attorney Credit Payments
			if (document.getElementById("txtAtt").value == 1)
			{
				show(AttCr1);
				show(AttCr2);
				hide(AttCr);			
			}
			else
			{
				hide(AttCr1);
				hide(AttCr2);
				show(AttCr);		
			}	
			
			if (document.getElementById("txt28").value == 1)
			{
				show(div_act2);
				show(div_act3);
				hide(div_act1);			
			}
			else
			{
				hide(div_act2);
				hide(div_act3);
				show(div_act1);		
			}									
		
			// SHOW HIDE CREDIT CARD TYPES IN PAYMENT TYPE SUMMARY.....
			var txt = document.getElementById("txtCC");
			var txtcheck=document.getElementById("txtCheck");
			var cnt = parseInt(document.getElementById("iCount").value);
			var grdName = "dgrdPayType";
			var idx=2;

			cnt = cnt + 2;

			for (idx=2; idx < (cnt); idx++)
			{
				var tr = "";				
				var paytype = "";
				var img1 = "";
				var img2 = "";
				var img3 = "";
				
				if( idx < 10 )
				{
				 tr = grdName+ "_ctl0" + idx + "_trDetail";
				 paytype = grdName+ "_ctl0" + idx + "_lblPayTypeID";
				 img1 = grdName+ "_ctl0" + idx + "_iParent";
				 img2 = grdName+ "_ctl0" + idx + "_iChild";
				 img3 = grdName+ "_ctl0" + idx + "_iLink";
				
				}
				else
				{
				 tr = grdName+ "_ctl" + idx + "_trDetail";
				 paytype = grdName+ "_ctl" + idx + "_lblPayTypeID";
				 img1 = grdName+ "_ctl" + idx + "_iParent";
				 img2 = grdName+ "_ctl" + idx + "_iChild";
				 img3 = grdName+ "_ctl" + idx + "_iLink";
				}

				var trCC = document.getElementById(tr);
				var pType = document.getElementById(paytype);
				var img11 = document.getElementById(img1);
				var img22 = document.getElementById(img2);
				var img33 = document.getElementById(img3);
				
				pType = parseInt(pType.innerText);
				
				if (pType == 5 )
					img11.style.display = 'block';


				if (pType > 500)
				{
					if (txt.value == '1' )
						{
						trCC.style.display = 'block';
						img11.style.display = 'none';
						img22.style.display = 'block';
						img33.style.display = 'block';
						}
					else
						{
						trCC.style.display = 'none';
						img11.style.display = 'none';
						img22.style.display = 'none';
						img33.style.display = 'none';
						trCC.style.margin= '0px';						
						
						}
				}	
				 if (pType == 200)
					img11.style.display = 'block';

				 if (pType == 2)	
				{  
					if (txtcheck.value == '1')
					{ 
						trCC.style.display = 'block';
						img11.style.display = 'none';
						img22.style.display = 'block';
						img33.style.display = 'block';
					}
					else
					{ 
						trCC.style.display = 'none';
						img11.style.display = 'none';
						img22.style.display = 'none';
						img33.style.display = 'none';
						trCC.style.margin= '0px';
						
						
					}
					
				
				}
				 if (pType == 4)	
				{  
					if (txtcheck.value == '1')
					{ 
						trCC.style.display = 'block';
						img11.style.display = 'none';
						img22.style.display = 'block';
						img33.style.display = 'block';
					}
					else
					{ 
						trCC.style.display = 'none';
						img11.style.display = 'none';
						img22.style.display = 'none';
						img33.style.display = 'none';
						trCC.style.margin= '0px';	
						
						
					}
					
				
				}

			}
		}
		
		function SetFlag(Control,Value)
		{
			
			document.getElementById(Control).value = Value ;		
			//document.Form1.innerHTML ;
			//document.Form1.outerHTML ; 
			//a;
		}
		
		
		function ValidateMe(txtbox)
		{
			//a;
			//var AmountValue= document.Form1.TextBox3.value;
			var AmountValue= txtbox.value; 
			if (AmountValue !="")
			{
				if (!(isFloat(AmountValue) || isSignedFloat(AmountValue)))
				{
					alert("Please Enter actual amount in textbox");				
					return false;
				}					
			}
			return true;		
		}
		
		
		function PopUpShowPreviewPDF()
		{
				//a;
				//strURL = "../../ClientInfo/violationsfees.asp?caseNumber=" & intTicketID
				//var strURL= "PreviewPDF.aspx?DocID=" & DocID;
				//window.open(strURL);				
				
				//window.open ("QuoteCallBackDetail.aspx?TicketID="+ TicketID + "&QuoteID=" + QuoteID ,"QuoteCallBack", 'toolbar=no,location=no,status=yes,menubar=no,scrollbars=no,resizable=no,left=150,Top=100,height=430,width=500' );				
				//window.open ('PreviewPDF.aspx?DocID='+ DocID,'toolbar=yes,location=no,status=yes,menubar=yes,scrollbars=yes,resizable=yes,left=150,Top=100,height=430,width=500' );
				//window.open ("PreviewMain.aspx?DocID="+ DocID);
				window.open ("PreviewMain.aspx");
				
		}
		
		</script>
		<style type="text/css">TABLE { FONT: 11px verdana,arial,helvetica,sans-serif }
	DIV.divHidden { DISPLAY: none; MARGIN: 0px }
	DIV { MARGIN: 0px }
		</style>
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
	</HEAD>
	<body onload="DefaultLoad();" >
		<form id="Form1" method="post" runat="server">
			<TABLE id="tblMain" cellSpacing="0" cellPadding="0" width="780" align="center" border="0" runat="server">
				<TR>
					<TD colSpan="7"><uc1:activemenu id="ActiveMenu1" runat="server"></uc1:activemenu></TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 99px" colSpan="7">&nbsp;
						<table id="Table5" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
							<tr>
								<td class="frmtd" style="WIDTH: 2px" height="35">&nbsp;</td>
								<td class="frmtd" vAlign="middle" height="35" style="width: 31px">Date&nbsp;</td>
								<td vAlign="middle" align="left" height="35" style="width: 131px"><ew:calendarpopup id="calQueryDate" runat="server" ImageUrl="../images/calendar.gif" Width="90px"
										ToolTip="Select Report Date Range" PadSingleDigits="True" Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True"
										CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Font-Names="Tahoma" Font-Size="8pt" DisableTextboxEntry="false">
										<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
										<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></WeekdayStyle>
										<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></MonthHeaderStyle>
										<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
											BackColor="AntiqueWhite"></OffMonthStyle>
										<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></GoToTodayStyle>
										<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGoldenrodYellow"></TodayDayStyle>
										<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Orange"></DayHeaderStyle>
										<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGray"></WeekendStyle>
										<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></SelectedDateStyle>
										<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></ClearDateStyle>
										<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></HolidayStyle>
									</ew:calendarpopup>
									</td>
									<td style="width: 137px">
									<ew:calendarpopup id="calTo" runat="server" ImageUrl="../images/calendar.gif" Width="90px" ToolTip="Select Report Date Range"
										PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00" Culture="(Default)" AllowArbitraryText="false"
										ShowGoToToday="True" CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Font-Names="Tahoma"
										Font-Size="8pt" DisableTextboxEntry="false">
										<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
										<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></WeekdayStyle>
										<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></MonthHeaderStyle>
										<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
											BackColor="AntiqueWhite"></OffMonthStyle>
										<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></GoToTodayStyle>
										<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGoldenrodYellow"></TodayDayStyle>
										<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Orange"></DayHeaderStyle>
										<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGray"></WeekendStyle>
										<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></SelectedDateStyle>
										<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></ClearDateStyle>
										<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></HolidayStyle>
									</ew:calendarpopup>
                                    </td>
                                    <td style="width: 79px">
                                    <asp:DropDownList ID="ddlOutSideFirm" runat="server" CssClass="clsInputCombo">
                                    </asp:DropDownList>
                                        
                                    </td>
								<TD vAlign="middle" align="left" height="35">
                                    &nbsp;
                                    <asp:Button ID="btn_update1" runat="server" CssClass="clsbutton" Text="Search" OnClick="btn_update1_Click" /></TD>
								<TD vAlign="bottom" align="right" height="35">
								</TD>
								<TD vAlign="bottom" align="right" width="20%" colSpan="2" height="35"><asp:hyperlink id="lnkEmail" onclick="ShowEmailBox();" runat="server" CssClass="NormalLink" NavigateUrl="#">Email</asp:hyperlink>&nbsp;|<asp:linkbutton id="lnkbtnPrint" runat="server" ToolTip="Print Report in HTML" CssClass="NormalLink">Print Report</asp:linkbutton>&nbsp;
								</TD>
							</tr>
							<TR>
								<TD class="frmtd" style="HEIGHT: 5px" colspan="5">
									<TABLE id="TableEmail" style="DISPLAY: none" cellSpacing="0" cellPadding="0" 
										border="0">
										<TR>
											<TD class="frmtd" vAlign="middle">Email&nbsp;
											</TD>
											<TD style="WIDTH: 189px" vAlign="middle"><asp:textbox id="txtEmail" runat="server" Width="172px" CssClass="TextBox"></asp:textbox>&nbsp;</TD>
											<TD style="WIDTH: 35px" align="left"><asp:imagebutton id="imgbtnSendMail" runat="server" ImageUrl="../Images/SendMail.ICO" ToolTip="Send Mail"></asp:imagebutton></TD>
											<TD align="left">&nbsp;<IMG id="imgHide" onclick="ShowEmailBox()" alt="Cancel" src="../Images/DELETE.jpg">
                                                &nbsp;
                                            </TD>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TR>
									</TABLE>
								&nbsp;</TD>
								<TD style="HEIGHT: 5px" vAlign="bottom" align="right" colSpan="2" height="5">
								</TD>
							</TR>
							<TR>
								<TD class="frmtd" align="center" colSpan="7" style="height: 13px"><asp:label id="lblMessage" runat="server" ForeColor="Red"></asp:label></TD>
							</TR>
						</table>
                        <table id="tblPrintHeader" runat="server" border="0" cellpadding="0" cellspacing="0"
                            width="100%" visible="false">
                            <tr>
                                <td align="center" class="TopHeading" colspan="4" height="13" style="height: 13px;
                                    text-align: left">
                                    <img alt="" src="../Images/lnlogo.jpg" /></td>
                            </tr>
                            <tr>
                                <td align="center" class="TopHeading" colspan="4" height="13" style="height: 13px">
                                    PAYMENT DETAIL REPORT
                                    <asp:Label ID="lblFirm" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="center" class="TopHeading" colspan="4" height="13" style="height: 13px">
                                    For The Period
                                    <asp:Label ID="lblDate" runat="server" CssClass="TopHeading"></asp:Label></td>
                            </tr>
                            <tr>
                                <td class="frmtd" height="5" style="height: 5px" width="20">
                                </td>
                                <td class="frmtd" height="5" style="height: 5px" width="100">
                                </td>
                                <td height="5" style="display: none; height: 5px">
                                    <ew:CalendarPopup ID="Calendarpopup1" runat="server" AllowArbitraryText="False" AutoPostBack="True"
                                        CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma"
                                        Font-Size="8pt" ImageUrl="../images/calendar.gif" PadSingleDigits="True" ShowGoToToday="True"
                                        ToolTip="Select Report Date" UpperBoundDate="12/31/9999 23:59:00" Width="90px">
                                        <TextboxLabelStyle CssClass="clstextarea" />
                                        <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Black" />
                                        <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Gray" />
                                        <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Black" />
                                        <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Black" />
                                        <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                    </ew:CalendarPopup>
                                </td>
                                <td align="right" height="5" style="height: 5px" valign="bottom">
                                    &nbsp;
                                    <asp:LinkButton id="lnkBack" runat="server" CssClass="NormalLink" Text="Back"></asp:LinkButton>
                                    </td>
                            </tr>
                            <tr>
                                <td align="center" class="frmtd" colspan="4" height="5">
                                </td>
                            </tr>
                        </table>
					</TD>
				</TR>
				<TR>
					<TD class="TDHeading" colSpan="5">
						<div id="HD1">&nbsp;<span onclick="SetFlag('txtCT',1); show(HD2);show(HD3);hide(HD1);">+</span>&nbsp;&nbsp;<asp:textbox id="Textbox8" runat="server" CssClass="ProductHead" ReadOnly="True">Category Type Summary</asp:textbox></div>
						<div class="divHidden" id="HD2">&nbsp;<span onclick="SetFlag('txtCT',0);show(HD1);hide(HD2);hide(HD3);">
								-</span>&nbsp;&nbsp;<asp:textbox id="Textbox7" runat="server" CssClass="ProductHead" ReadOnly="True">Category Type Summary</asp:textbox></div>
					</TD>
				</TR>
				<TR>
					<TD colSpan="5" height="7"></TD>
				</TR>
				<tr>
					<td width="100%" colSpan="5">
						<div class="divHidden" id="HD3">
							<table cellSpacing="0" cellPadding="0" width="100%" border="0">
								<tr>
									<td><asp:datagrid id="dgCategoryType" runat="server" Width="100%" Font-Names="Verdana" Font-Size="2px"
											AutoGenerateColumns="False" ShowFooter="True">
											<AlternatingItemStyle BackColor="#EEEEEE"></AlternatingItemStyle>
											<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle"></HeaderStyle>
											<FooterStyle CssClass="GrdFooter"></FooterStyle>
											<Columns>
												<asp:TemplateColumn HeaderText="Category Type">
													<HeaderStyle HorizontalAlign="Left" Width="65%" CssClass="GrdHeader"></HeaderStyle>
													<ItemTemplate>
														<asp:Label id="Label2" runat="server" Width="20px" CssClass="ProductGrdLinks" Visible="False"></asp:Label>
														<asp:Label id=lblCategoryType runat="server" CssClass="ProductGrdLinks" Visible="True" Text='<%# DataBinder.Eval(Container, "DataItem.CategoryType") %>'>
														</asp:Label>
													</ItemTemplate>
													<FooterTemplate>
														<asp:Label id="Label3" runat="server" CssClass="grdheader">Total</asp:Label>
													</FooterTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Count">
													<HeaderStyle HorizontalAlign="Center" Width="15%" CssClass="GrdHeader"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center"></ItemStyle>
													<ItemTemplate>
														<asp:Label id=lblCount2 runat="server" CssClass="GrdLbl" Visible="True" Text='<%# DataBinder.Eval(Container, "DataItem.TotalCount") %>'>
														</asp:Label>
														<asp:Label id=lblDisplayCol runat="server" CssClass="GrdLbl" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.displaycol") %>'>
														</asp:Label>
													</ItemTemplate>
													<FooterStyle HorizontalAlign="Center"></FooterStyle>
													<FooterTemplate>
														<asp:Label id="lbl_SumCount" runat="server" CssClass="GrdLbl"></asp:Label>
													</FooterTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Amount">
													<HeaderStyle HorizontalAlign="Center" Width="20%" CssClass="GrdHeader"></HeaderStyle>
													<ItemStyle HorizontalAlign="Right"></ItemStyle>
													<ItemTemplate>
														<asp:Label id=lblAmount2 runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.TotalAmount", "{0:C}") %>'>
														</asp:Label>&nbsp;&nbsp;&nbsp;
													</ItemTemplate>
													<FooterStyle HorizontalAlign="Right"></FooterStyle>
													<FooterTemplate>
														<asp:Label id="lbl_SumAmount" runat="server" CssClass="GrdLbl"></asp:Label>&nbsp;&nbsp;&nbsp;
													</FooterTemplate>
												</asp:TemplateColumn>
											</Columns>
											<PagerStyle NextPageText="" PrevPageText="" HorizontalAlign="Center" Mode="NumericPages"></PagerStyle>
										</asp:datagrid></td>
								</tr>
								<TR>
									<TD style="HEIGHT: 20px" colSpan="5"></TD>
								</TR>
							</table>
						</div>
					</td>
				</tr>
				<TR>
					<TD colSpan="5" height="2"></TD>
				</TR>
				<!-- credit summary section starts   -->
				<TR>
					<TD class="TDHeading" colSpan="5">
						<div id="CSS1">&nbsp;<span onclick="SetFlag('txtCSS',1); show(CSS2);show(CSS3);hide(CSS1);">+</span>&nbsp;&nbsp;<asp:textbox id="Textbox9" runat="server" CssClass="ProductHead" ReadOnly="True">Credit Summary</asp:textbox></div>
						<div class="divHidden" id="CSS2">&nbsp;<span onclick="SetFlag('txtCSS',0);show(CSS1);hide(CSS2);hide(CSS3);">
								-</span>&nbsp;&nbsp;<asp:textbox id="Textbox10" runat="server" CssClass="ProductHead" ReadOnly="True">Credit Summary</asp:textbox></div>
					</TD>
				</TR>
				<TR>
					<TD colSpan="5" height="7"></TD>
				</TR>
				<tr>
					<td width="100%" colSpan="5">
						<div class="divHidden" id="CSS3">
							<table cellSpacing="0" cellPadding="0" width="100%" border="0">
								<tr>
									<td><asp:datagrid id="dgCredit" runat="server" Width="100%" Font-Names="Verdana" Font-Size="2px" AutoGenerateColumns="False"
											ShowFooter="True" Visible="true">
											<AlternatingItemStyle BackColor="#EEEEEE"></AlternatingItemStyle>
											<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle"></HeaderStyle>
											<FooterStyle CssClass="GrdFooter"></FooterStyle>
											<Columns>
												<asp:TemplateColumn HeaderText="Credit Type">
													<HeaderStyle HorizontalAlign="Left" Width="65%" CssClass="GrdHeader"></HeaderStyle>
													<ItemTemplate>
														<asp:Label id=Label5 runat="server" CssClass="ProductGrdLinks" Visible="True" Text='<%# DataBinder.Eval(Container, "DataItem.CategoryType") %>'>
														</asp:Label>
													</ItemTemplate>
													<FooterTemplate>
														<asp:Label id="Label6" runat="server" CssClass="grdheader">Total</asp:Label>
													</FooterTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Count">
													<HeaderStyle HorizontalAlign="Center" Width="15%" CssClass="GrdHeader"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center"></ItemStyle>
													<ItemTemplate>
														<asp:Label id=Label7 runat="server" CssClass="GrdLbl" Visible="True" Text='<%# DataBinder.Eval(Container, "DataItem.TotalCount") %>'>
														</asp:Label>
													</ItemTemplate>
													<FooterStyle HorizontalAlign="Center"></FooterStyle>
													<FooterTemplate>
														<asp:Label id="Label9" runat="server" CssClass="GrdLbl"></asp:Label>
													</FooterTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Amount">
													<HeaderStyle HorizontalAlign="Center" Width="20%" CssClass="GrdHeader"></HeaderStyle>
													<ItemStyle HorizontalAlign="Right"></ItemStyle>
													<ItemTemplate>
														<asp:Label id=Label10 runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.TotalAmount", "{0:C}") %>'>
														</asp:Label>&nbsp;&nbsp;&nbsp;
													</ItemTemplate>
													<FooterStyle HorizontalAlign="Right"></FooterStyle>
													<FooterTemplate>
														<asp:Label id="Label11" runat="server" CssClass="GrdLbl"></asp:Label>&nbsp;&nbsp;&nbsp;
													</FooterTemplate>
												</asp:TemplateColumn>
											</Columns>
											<PagerStyle NextPageText="" PrevPageText="" HorizontalAlign="Center" Mode="NumericPages"></PagerStyle>
										</asp:datagrid></td>
								</tr>
								<TR>
									<TD style="HEIGHT: 20px" colSpan="5"></TD>
								</TR>
							</table>
						</div>
					</td>
				</tr>
				<TR>
					<TD colSpan="5" height="2"></TD>
				</TR>
				<!-- credit summary section ends -->
				<TR>
					<TD class="TDHeading" colSpan="5">
						<div id="HA1">&nbsp;<span onclick="SetFlag('txtPC',1);show(HA2);show(HA3);hide(HA1);">+</span>&nbsp;&nbsp;<asp:textbox id="lblHeading1" runat="server" CssClass="ProductHead" ReadOnly="True">Payment Type Summary</asp:textbox></div>
						<div class="divHidden" id="HA2">&nbsp;<span onclick="SetFlag('txtPC',0);show(HA1);hide(HA2);hide(HA3);">
								-</span>&nbsp;&nbsp;<asp:textbox id="lblHeading1_1" runat="server" CssClass="ProductHead" ReadOnly="True">Payment Type Summary</asp:textbox></div>
					</TD>
				</TR>
				<TR>
					<TD colSpan="5" height="7"></TD>
				</TR>
				<TR>
					<TD width="100%" colSpan="5">
						<div class="divHidden" id="HA3">
							<TABLE id="Table6" cellSpacing="0" cellPadding="0" width="100%" border="0">
								<TR>
									<TD vAlign="top" align="left">
										<TABLE id="Table2" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<TR>
												<TD colSpan="5" height="20"><asp:datagrid id="dgrdPayType" runat="server" Width="100%" Font-Names="Verdana" Font-Size="2px"
														AutoGenerateColumns="False" ShowFooter="True" GridLines="None" CellPadding="0" OnItemCommand="DoGetQueryString">
														<AlternatingItemStyle BackColor="#EEEEEE"></AlternatingItemStyle>
														<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle"></HeaderStyle>
														<FooterStyle CssClass="GrdFooter"></FooterStyle>
														<Columns>
															<asp:TemplateColumn>
																<HeaderTemplate>
																	<TABLE style="FONT-SIZE: 2px; WIDTH: 100%; FONT-FAMILY: Verdana; BORDER-COLLAPSE: collapse"
																		cellSpacing="0" rules="cols" border="1">
																		<TR>
																			<TD class="GrdHeader" align="left" width="60%">Payment Method</TD>
																			<TD class="GrdHeader" align="center" width="15%">Count</TD>
																			<TD class="GrdHeader" align="center" width="20%">Amount&nbsp;&nbsp;
																			</TD>
																			<TD class="GrdHeader" style="DISPLAY: none" align="right" width="5%"></TD>
																		</TR>
																	</TABLE>
																</HeaderTemplate>
																<ItemTemplate>
																	<TABLE style="FONT-SIZE: 2px; WIDTH: 100%; FONT-FAMILY: Verdana; BORDER-COLLAPSE: collapse"
																		borderColor="gainsboro" cellSpacing="0" cellPadding="0" rules="all" border="1">
																		<TR id="trDetail" runat="server">
																			<TD vAlign="top" align="left" width="60%">
																				<TABLE style="BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BORDER-BOTTOM-STYLE: none"
																					cellSpacing="0" cellPadding="0" width="200" border="0">
																					<TR>
																						<TD id="iParent" style="DISPLAY: none" align="left" width="10" runat="server">
																							<asp:Image id="imgParent" ImageUrl="../Images/folderopen.gif" Runat="server"></asp:Image></TD>
																						<TD id="iLink" style="DISPLAY: none" align="left" width="10" runat="server">
																							<asp:Image id="imgLink" ImageUrl="../Images/T.gif" Runat="server"></asp:Image></TD>
																						<TD id="iChild" style="DISPLAY: none" align="left" width="10" runat="server">
																							<asp:Image id="imgChild" ImageUrl="../Images/folder.gif" Runat="server"></asp:Image></TD>
																						<TD width="170">&nbsp;
																							<asp:linkbutton id="lnkbtnDocumentID" runat="server" CssClass="ProductGrdLinks" ForeColor="black"
																								CommandName="DoGetPayment">
																								<%# DataBinder.Eval(Container, "DataItem.Description") %>
																							</asp:linkbutton>
																							<asp:Label id=lblPayType runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.Description") %>' Visible="False">
																							</asp:Label></TD>
																					</TR>
																				</TABLE>
																			</TD>
																			<TD align="center" width="15%">
																				<asp:Label id=lblCount runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.TotalCount") %>' Visible="True">
																				</asp:Label></TD>
																			<TD align="right" width="20%">
																				<asp:Label id=lblAmount runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.Amount") %>'>
																				</asp:Label>&nbsp; &nbsp;</TD>
																			<TD class="GrdHeader" style="DISPLAY: none" align="right" width="5%">
																				<asp:Label id=lblPayTypeID runat="server" Width="0px" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentType_PK") %>'>
																				</asp:Label></TD>
																		</TR>
																	</TABLE>
																</ItemTemplate>
																<FooterTemplate>
																	<TABLE style="FONT-SIZE: 2px; WIDTH: 100%; FONT-FAMILY: Verdana; BORDER-COLLAPSE: collapse"
																		borderColor="silver" cellSpacing="0" rules="cols" border="1">
																		<TR>
																			<TD align="left" width="60%">
																				<asp:Label id="Label1" runat="server" CssClass="grdheader">Total</asp:Label></TD>
																			<TD align="center" width="15%">
																				<asp:Label id="lbl_Count" runat="server" CssClass="GrdLbl"></asp:Label></TD>
																			<TD align="right" width="20%">
																				<asp:Label id="lbl_Amount" runat="server" CssClass="GrdLbl"></asp:Label></TD>
																			<TD class="GrdHeader" style="DISPLAY: none" align="right" width="5%"></TD>
																		</TR>
																	</TABLE>
																</FooterTemplate>
															</asp:TemplateColumn>
														</Columns>
														<PagerStyle NextPageText="" PrevPageText="" HorizontalAlign="Center" Mode="NumericPages"></PagerStyle>
													</asp:datagrid></TD>
											</TR>
											<TR>
												<TD style="HEIGHT: 20px" colSpan="5"></TD>
											</TR>
											<TR>
												<TD style="HEIGHT: 2px" vAlign="top" colSpan="5"></TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
							</TABLE>
						</div>
					</TD>
				</TR>
				<TR>
					<TD class="TDHeading" colSpan="5">
						<div id="HAA1">&nbsp;<span onclick="SetFlag('txtRS',1);show(HAA2);show(HAA3);hide(HAA1);">+</span>&nbsp;&nbsp;<asp:textbox id="Textbox3" runat="server" Width="168px" CssClass="ProductHead" ReadOnly="True">Representative Summary</asp:textbox></div>
						<div class="divHidden" id="HAA2">&nbsp;<span onclick="SetFlag('txtRS',0);show(HAA1);hide(HAA2);hide(HAA3);">
								-</span>&nbsp;&nbsp;<asp:textbox id="Textbox4" runat="server" Width="176px" CssClass="ProductHead" ReadOnly="True">Representative Summary</asp:textbox></div>
					</TD>
				</TR>
				<TR>
					<TD colSpan="5" height="7"></TD>
				</TR>
				<TR>
					<TD class="frmtd" colSpan="5">
						<div class="divHidden" id="HAA3">
							<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
								<TR>
									<TD class="frmtd" style="BORDER-TOP: 1px solid; BORDER-LEFT: 1px solid" vAlign="top"
										align="left" width="18%" bgColor="#eeeeee" colSpan="2" height="18">Rep</TD>
									<TD class="frmtd" style="BORDER-TOP: 1px solid" align="center" width="25%" bgColor="#eeeeee"
										height="18">Cash</TD>
									<TD class="frmtd" style="BORDER-TOP: 1px solid" align="center" width="25%" bgColor="#eeeeee"
										height="18">Check</TD>
									<TD class="frmtd" style="BORDER-RIGHT: gray 1px solid; BORDER-TOP: 1px solid" align="center"
										width="25%" bgColor="#eeeeee" height="18">Total</TD>
								</TR>
								<TR>
									<TD width="100%" colSpan="6"><asp:datagrid id="dgrdPayByRep" runat="server" Width="100%" Font-Names="Verdana" Font-Size="2px"
											AutoGenerateColumns="False" OnItemCommand="DoGetQueryString">
											<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle"></HeaderStyle>
											<FooterStyle CssClass="GrdFooter"></FooterStyle>
											<Columns>
												<asp:TemplateColumn>
													<HeaderStyle HorizontalAlign="Left" Width="20%" CssClass="GrdHeader"></HeaderStyle>
													<ItemTemplate>
														<asp:Label id="lblEmployeeID" CssClass="GrdLbl" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.RepID") %>'>
														</asp:Label>
														<asp:Label id="lblEmployee" CssClass="GrdLbl" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.RepName") %>'>
														</asp:Label>
														<asp:linkbutton id="lnkbtnRepID" runat="server" ForeColor="black" CssClass="ProductGrdLinks" CommandName="DoGetRep">
															<%# DataBinder.Eval(Container, "DataItem.RepName") %>
														</asp:linkbutton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="System">
													<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center"></ItemStyle>
													<ItemTemplate>
														<asp:Label id="lblSystemCash" CssClass="GrdLbl" runat="server" Visible="True" Text=''></asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Actual">
													<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center"></ItemStyle>
													<ItemTemplate>
														<asp:Label id="lblActualCash" CssClass="GrdLbl" runat="server" Visible="false" Text=''></asp:Label>
														<asp:TextBox ID="txtActualCash" CssClass="TextBox" ReadOnly="True" BorderStyle="None" Runat="server"
															Visible="True" Width="50" MaxLength="10"></asp:TextBox>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="System">
													<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center"></ItemStyle>
													<ItemTemplate>
														<asp:Label id="lblSystemCheck" CssClass="GrdLbl" runat="server" Visible="true" Text=''></asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Actual">
													<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center"></ItemStyle>
													<ItemTemplate>
														<asp:Label id="lblActualCheck" CssClass="GrdLbl" runat="server" Visible="false" Text=''></asp:Label>
														<asp:TextBox ID="txtActualCheck" CssClass="TextBox" ReadOnly="True" BorderStyle="None" Runat="server"
															Visible="True" Width="50" MaxLength="10"></asp:TextBox>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="System">
													<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center"></ItemStyle>
													<ItemTemplate>
														<asp:Label id="lblSystemTotal" CssClass="GrdLbl" runat="server" Visible="true" Text=''></asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Actual">
													<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center"></ItemStyle>
													<ItemTemplate>
														<asp:Label id="lblActualTotal" CssClass="GrdLbl" runat="server" Visible="true" Text=''></asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
											</Columns>
											<PagerStyle NextPageText="" PrevPageText="" HorizontalAlign="Center" Mode="NumericPages"></PagerStyle>
										</asp:datagrid></TD>
								</TR>
								<TR>
									<TD class="frmtd" style="BORDER-RIGHT: gray 1px solid; BORDER-LEFT: 1px solid; BORDER-BOTTOM: black 1px solid"
										colSpan="5">
										<TABLE id="Table4" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<TR>
												<TD class="frmtd" width="22%" height="25">Total</TD>
												<TD width="14%" height="25"><asp:label id="lblTotalCashSys" runat="server" CssClass="Label"></asp:label></TD>
												<TD width="13%" height="25"><asp:label id="lblTotalCashActual" runat="server" CssClass="Label"></asp:label></TD>
												<TD width="15%" height="25"><asp:label id="lblTotalCheckSys" runat="server" CssClass="Label"></asp:label></TD>
												<TD width="12%" height="25"><asp:label id="lblTotalCheckActual" runat="server" CssClass="Label"></asp:label></TD>
												<TD width="13%"><asp:label id="lblTotalSystem" runat="server" CssClass="Label"></asp:label></TD>
												<TD><asp:label id="lblTotalActual" runat="server" CssClass="Label" DESIGNTIMEDRAGDROP="4631"></asp:label></TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
								<tr>
									<td colSpan="5" height="20"></td>
								</tr>
							</TABLE>
						</div>
					</TD>
				</TR>
				<TR>
					<TD class="TDHeading" colSpan="5">
						<div id="HC1">&nbsp;<span onclick="SetFlag('txtCS',1);show(HC2);show(HC3);hide(HC1);">+</span>&nbsp;&nbsp;<asp:textbox id="Textbox5" runat="server" CssClass="ProductHead" ReadOnly="True"> Court Summary</asp:textbox></div>
						<div class="divHidden" id="HC2">&nbsp;<span onclick="SetFlag('txtCS',0);show(HC1);hide(HC2);hide(HC3);">
								-</span>&nbsp;&nbsp;<asp:textbox id="Textbox6" runat="server" CssClass="ProductHead" ReadOnly="True"> Court Summary</asp:textbox></div>
					</TD>
				</TR>
				<TR>
					<TD colSpan="5" height="7"></TD>
				</TR>
				<TR>
					<TD width="100%" colSpan="5">
						<div class="divHidden" id="HC3">
							<TABLE id="Table61" cellSpacing="0" cellPadding="0" width="100%" border="0">
								<TR>
									<TD vAlign="top" align="left">
										<TABLE id="Table21" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<TR>
												<TD colSpan="5" height="20"><asp:datagrid id="dgrdCourt" runat="server" Width="100%" Font-Names="Verdana" Font-Size="2px"
														AutoGenerateColumns="False" ShowFooter="True" OnItemCommand="DoGetQueryString">
														<AlternatingItemStyle BackColor="#EEEEEE"></AlternatingItemStyle>
														<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle"></HeaderStyle>
														<FooterStyle CssClass="GrdFooter"></FooterStyle>
														<Columns>
															<asp:TemplateColumn HeaderText="Court Location">
																<HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
																<ItemTemplate>
																	<asp:Label id="lblCourtID" CssClass="GrdLbl" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.CurrentCourtloc") %>'>
																	</asp:Label>
																	<asp:Label id="lblCourtName" CssClass="GrdLbl" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.CourtName") %>'>
																	</asp:Label>
																	<asp:linkbutton id="lnkbtnCourt" runat="server" ForeColor="black" CssClass="ProductGrdLinks" CommandName="DoGetCourt">
																		<%# DataBinder.Eval(Container, "DataItem.CourtName") %>
																	</asp:linkbutton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Transactions">
																<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
																<ItemStyle HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id="lblTrans" CssClass="GrdLbl" runat="server" Visible="True" Text='<%# DataBinder.Eval(Container, "DataItem.Trans") %>'>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Fee Amount">
																<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
																<ItemStyle HorizontalAlign="Right"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id="lblFee" CssClass="GrdLbl" runat="server" Visible="true" Text='<%# DataBinder.Eval(Container, "DataItem.Amount") %>'>
																	</asp:Label>&nbsp;&nbsp;&nbsp;
																</ItemTemplate>
															</asp:TemplateColumn>
														</Columns>
														<PagerStyle NextPageText="" PrevPageText="" HorizontalAlign="Center" Mode="NumericPages"></PagerStyle>
													</asp:datagrid></TD>
											</TR>
											<TR>
												<TD style="HEIGHT: 20px" colSpan="5"></TD>
											</TR>
											<TR>
												<TD style="HEIGHT: 2px" vAlign="top" colSpan="5"></TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
							</TABLE>
						</div>
					</TD>
				</TR>
				<TR>
					<TD class="TDHeading" colSpan="5" valign="middle">  <!-- <span   onclick="SetFlag('txtPR',1);show(HB2);show(HB3);hide(HB1);">+</span> -->
						<div id="HB1"   >&nbsp;  <span onclick="SetFlag('txtPR',1);show(HB2);show(HB3);hide(HB1);">+</span> &nbsp;&nbsp;<asp:textbox id="Textbox1" runat="server" CssClass="ProductHead" ReadOnly="True">Transaction Details</asp:textbox></div>
						<div class="divHidden" id="HB2">&nbsp;<span onclick="SetFlag('txtPR',0);show(HB1);hide(HB2);hide(HB3);">
								-</span>&nbsp;&nbsp;<asp:textbox id="Textbox2" runat="server" CssClass="ProductHead" ReadOnly="True">Transaction Details</asp:textbox></div>
					</TD>
				</TR>
				<tr>
					<TD colSpan="5" height="7"></TD>
				</tr>
				<TR>
					<TD colSpan="5" height="3">
						<div class="divHidden" id="HB3">
							<TABLE id="Table7" cellSpacing="0" cellPadding="0" width="100%" border="0">
								<TR>
									<TD style="BORDER-RIGHT: gray 1px solid; BORDER-TOP: 1px solid; BORDER-LEFT: 1px solid; height: 25px;"
										align="center" bgColor="#eeeeee" colSpan="5"><asp:dropdownlist id="cmbRep" runat="server" Width="100px" CssClass="frmtd" AutoPostBack="True"></asp:dropdownlist>&nbsp;&nbsp;
										<asp:dropdownlist id="cmbPayType" runat="server" Width="280px" CssClass="frmtd" AutoPostBack="True"></asp:dropdownlist>&nbsp;&nbsp;
										<asp:dropdownlist id="cmbCourt" runat="server" Width="150px" CssClass="frmtd" AutoPostBack="True"></asp:dropdownlist></TD>
								</TR>
								<TR>
									<TD><asp:datagrid id="dgrdPayDetail" runat="server" Width="100%" Font-Names="Verdana" Font-Size="2px"
											AutoGenerateColumns="False" OnItemCommand="DoGetQueryString" AllowCustomPaging="True" PageSize="100"
											AllowSorting="True">
											<AlternatingItemStyle BackColor="#EEEEEE"></AlternatingItemStyle>
											<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle"></HeaderStyle>
											<FooterStyle CssClass="GrdFooter"></FooterStyle>
											<Columns>
												<asp:TemplateColumn HeaderText="No.">
													<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center" Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" Wrap="False"></ItemStyle>
													<ItemTemplate>
														<asp:Label id=lblTicketID runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.TicketID") %>' Visible="false">
														</asp:Label>
														<asp:Label id=lblCardTypeID runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.CardType") %>' Visible="false">
														</asp:Label>
														<asp:Label id="lblNo" runat="server" CssClass="GrdLbl" Text="" Visible="True"></asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Date">
													<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center" Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" Wrap="False"></ItemStyle>
													<ItemTemplate>
                                                        &nbsp;<asp:Label id=lblDate runat="server" CssClass="GrdLbl" Visible="true" Text='<%# DataBinder.Eval(Container, "DataItem.RecDate") %>'>
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Time"  SortExpression="sorttime">
													<HeaderStyle HorizontalAlign="Center" CssClass="TaskGrid_Head"></HeaderStyle>
													<ItemStyle HorizontalAlign="Right" Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" Wrap="False"></ItemStyle>
													<ItemTemplate>
														<asp:Label id=lblTime runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.RecTime") %>' Visible="true">
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn SortExpression="CustomerName" HeaderText="Client Name">
													<HeaderStyle HorizontalAlign="Left" CssClass="TaskGrid_Head"></HeaderStyle>
													<ItemTemplate>
														<asp:Label id=lblCustomer runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.CustomerName") %>' Visible="false">
														</asp:Label>
														<asp:linkbutton id="lnkCustomer" runat="server" CssClass="ProductGrdLinks" ForeColor="black" CommandName="DoGetCustomer">
															<%# DataBinder.Eval(Container, "DataItem.CustomerName") %>
														</asp:linkbutton>
													</ItemTemplate>
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
												</asp:TemplateColumn>
												<asp:TemplateColumn SortExpression="Lastname" HeaderText="Rep">
													<HeaderStyle HorizontalAlign="Left" CssClass="TaskGrid_Head"></HeaderStyle>
													<ItemTemplate>
														<asp:Label id=lblRep runat="server" CssClass="GrdLbl" Visible="true" Text='<%# DataBinder.Eval(Container, "DataItem.Lastname") %>'>
														</asp:Label>
													</ItemTemplate>
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
												</asp:TemplateColumn>
												<asp:TemplateColumn SortExpression="bondflag" HeaderText="Bond">
													<HeaderStyle HorizontalAlign="Left" CssClass="TaskGrid_Head"></HeaderStyle>
													<ItemTemplate>
														<asp:Label id=lblBond runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.BondFlag") %>' Visible="true">
														</asp:Label>
													</ItemTemplate>
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
												</asp:TemplateColumn>
												<asp:TemplateColumn SortExpression="ChargeAmount" HeaderText="Paid">
													<HeaderStyle HorizontalAlign="Left" CssClass="TaskGrid_Head"></HeaderStyle>
													<ItemTemplate>
														<asp:Label id=lblPaidAmount runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.ChargeAmount", "{0:C0}") %>'>
														</asp:Label>
													</ItemTemplate>
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
												</asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Adj1">
                                                <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.adj1") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Adj2">
                                                <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.adj2") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
                                                </asp:TemplateColumn>
												<asp:TemplateColumn SortExpression="PaymentType" HeaderText="Payment Type">
													<HeaderStyle HorizontalAlign="Left" CssClass="TaskGrid_Head"></HeaderStyle>
													<ItemTemplate>
														<asp:Label id=lblPayTypeFR runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.CCType") %>' Visible="true">
														</asp:Label>
														<asp:Label id=lblPTypeId runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentType") %>' Visible="False">
														</asp:Label>
													</ItemTemplate>
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Card">
													<HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
													<ItemTemplate>
														<asp:Label id=lblCardType runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.ChargeAmount") %>' Visible="true">
														</asp:Label>
													</ItemTemplate>
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Court">
													<HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
													<ItemTemplate>
														<asp:Label id=lblCourt runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.Court") %>' Visible="true">
														</asp:Label>
													</ItemTemplate>
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="List">
													<HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
													<ItemTemplate>
														<asp:Label id=lblListDate runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.ListDate") %>' Visible="true">
														</asp:Label>
													</ItemTemplate>
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Letter">
                                                <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                                <ItemStyle HorizontalAlign="Center"  />
                                                    <ItemTemplate>
                                                        <asp:Label id="lbl_MailType" Text='<%# DataBinder.Eval(Container, "DataItem.MailType") %>' runat="server" CssClass="GrdLbl" Visible="true">
														</asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Date">
                                                <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                                <ItemStyle HorizontalAlign="Center"  />
                                                    <ItemTemplate>
                                                        <asp:Label id="lbl_MailDate" Text='<%# DataBinder.Eval(Container, "DataItem.MailDate") %>' runat="server" CssClass="GrdLbl" Visible="true">
														</asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
											</Columns>
											<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Center"></PagerStyle>
										</asp:datagrid></TD>
								</TR>
								<tr>
									<td colSpan="5" height="20"></td>
								</tr>
							</TABLE>
						</div>
					</TD>
				</TR>
				<TR>
					<TD colSpan="5">
						
					</TD>
				</TR>
				<TR>
					<TD class="TDHeading" colSpan="5" valign="middle">  <!-- <span   onclick="SetFlag('txtPR',1);show(HB2);show(HB3);hide(HB1);">+</span> -->
						 <div id="VP">&nbsp;<span onclick="SetFlag('txtVP',1);show(VP1);show(VPP);hide(VP);">+</span>&nbsp;&nbsp;<asp:textbox id="txtVoidPayment" runat="server" CssClass="ProductHead" ReadOnly="True">Void Transaction</asp:textbox></div>
					     <div class="divHidden" id="VP1">&nbsp;<span onclick="SetFlag('txtVP',0);show(VP);hide(VP1);hide(VPP);">
							                    -</span>&nbsp;&nbsp;<asp:textbox id="txt_vp" runat="server" CssClass="ProductHead" ReadOnly="True">Void Transaction</asp:textbox></div>
					</TD>
				</TR>
				<TR>
					<TD colSpan="5" align="left" valign="top">
					<div class="divHidden" id="VPP" >
                        <asp:datagrid id="DG_voidTransactions" runat="server" Width="100%" Font-Names="Verdana" Font-Size="2px"
											AutoGenerateColumns="False" OnItemCommand="DoGetQueryString" AllowCustomPaging="True" PageSize="100"
											AllowSorting="True" OnSortCommand="DG_VoidTransaction_SortCommand">
                            <FooterStyle CssClass="GrdFooter" />
                            <PagerStyle HorizontalAlign="Center" NextPageText="Next" PrevPageText="Previous" />
                            <AlternatingItemStyle BackColor="#EEEEEE" />
                            <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Center" VerticalAlign="Middle" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="No">
                               <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
													
                                
                                 <ItemTemplate>
                                <asp:Label id=lblTicketID runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.TicketID") %>' Visible="false">
														</asp:Label>
														<asp:Label id=lblCardTypeID runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.CardType") %>' Visible="false">
														</asp:Label>
														<asp:Label id="lblNo" runat="server" CssClass="GrdLbl" Text="" Visible="True"></asp:Label>
														
														</ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Date">
                                <HeaderStyle  HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblDate" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.RecDate") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Time" SortExpression="sorttime">
                                <HeaderStyle HorizontalAlign="Left" CssClass="TaskGrid_Head"></HeaderStyle>
                                
                                    <ItemTemplate>
                                        <asp:Label ID="lblTime" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.RecTime","{0:t}") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Client Name" SortExpression="CustomerName">
                                    <HeaderStyle HorizontalAlign="Left" CssClass="TaskGrid_Head"></HeaderStyle>
													
                                    <ItemTemplate>
                                        <asp:Label ID="lblCustomer" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.CustomerName") %>'
                                            Visible="False"></asp:Label><asp:LinkButton ID="lnkCustomer" runat="server" CommandName="DoGetCustomer"
                                            CssClass="ProductGrdLinks" ForeColor="black">
															<%# DataBinder.Eval(Container, "DataItem.CustomerName") %>
														</asp:LinkButton>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Rep" SortExpression="Lastname">
                               <HeaderStyle HorizontalAlign="Left" CssClass="TaskGrid_Head"></HeaderStyle>
													                                
                                    <ItemTemplate>
                                        <asp:Label ID="lblRep" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.Lastname") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Amount" SortExpression="ChargeAmount">
                                <HeaderStyle HorizontalAlign="Left" CssClass="TaskGrid_Head"></HeaderStyle>
													
                                
                                    <ItemTemplate>
                                        <asp:Label ID="lblPaidAmount" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.ChargeAmount", "{0:C0}") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Adj1">
                                <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                
                                    <ItemTemplate>
                                        <asp:Label ID="lb_adj1" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.adj1") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Adj2">
                                <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                
                                    <ItemTemplate>
                                        <asp:Label ID="lb_adj2" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.adj2") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Payment Type" SortExpression="PaymentType">
                                <HeaderStyle HorizontalAlign="Left" CssClass="TaskGrid_Head"></HeaderStyle>
													
                                    <ItemTemplate>
                                        <asp:Label ID="lblPayTypeFR" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.CCType") %>'
                                            Visible="true">
														</asp:Label><asp:Label ID="lblPTypeId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentType") %>'
                                            Visible="False"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Card">
                                <HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
                                
                                    <ItemTemplate>
                                        <asp:Label ID="lblCardType" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.ChargeAmount") %>'
                                            Visible="true">
														</asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Court">
                                <HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
                                
                                    <ItemTemplate>
                                        <asp:Label ID="lblCourt" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.Court") %>'
                                            Visible="true">
														</asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                        </div>
                        </TD>
				</TR>
				<TR>
					<TD colSpan="5">
						
					</TD>
				</TR>
				
				<TR>
					<TD class="TDHeading" colSpan="5" valign="middle">  <!-- <span   onclick="SetFlag('txtPR',1);show(HB2);show(HB3);hide(HB1);">+</span> -->
						 <div id="AttCr">&nbsp;<span onclick="SetFlag('txtAtt',1);show(AttCr1);show(AttCr2);hide(AttCr);">+</span>&nbsp;&nbsp;<asp:textbox id="Textbox11" runat="server" CssClass="ProductHead" ReadOnly="True">Attorney Credit Payments</asp:textbox></div>
					     <div class="divHidden" id="AttCr1">&nbsp;<span onclick="SetFlag('txtAtt',0);show(AttCr);hide(AttCr1);hide(AttCr2);">
							                    -</span>&nbsp;&nbsp;<asp:textbox id="Textbox12" runat="server" CssClass="ProductHead" ReadOnly="True">Attorney Credit Payments</asp:textbox></div>
					</TD>
				</TR>
				<TR>
					<TD colSpan="5" align="left" valign="top">
					<div class="divHidden" id="AttCr2" >
                        <asp:datagrid id="DG_AttorneyCredit" runat="server" Width="100%" Font-Names="Verdana" Font-Size="2px"
											AutoGenerateColumns="False"  AllowCustomPaging="True" PageSize="100"
											AllowSorting="True" OnSortCommand="DG_AttorneyCredit_SortCommand" OnItemCommand="DoGetQueryString" >
                            <FooterStyle CssClass="GrdFooter" />
                            <PagerStyle HorizontalAlign="Center" NextPageText="Next" PrevPageText="Previous" />
                            <AlternatingItemStyle BackColor="#EEEEEE" />
                            <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Center" VerticalAlign="Middle" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="No">
                               <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
													
                                
                                 <ItemTemplate>
                                <asp:Label id=lblTicketID runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.TicketID") %>' Visible="false">
														</asp:Label>
														<asp:Label id=lblCardTypeID runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.CardType") %>' Visible="false">
														</asp:Label>
														<asp:Label id="lblNo" runat="server" CssClass="GrdLbl" Text="" Visible="True"></asp:Label>
														
														</ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Date">
                                <HeaderStyle  HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblDate" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.RecDate") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Time" SortExpression="sorttime">
                                <HeaderStyle HorizontalAlign="Left" CssClass="TaskGrid_Head"></HeaderStyle>
                                
                                    <ItemTemplate>
                                        <asp:Label ID="lblTime" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.RecTime","{0:t}") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Client Name" SortExpression="CustomerName">
                                    <HeaderStyle HorizontalAlign="Left" CssClass="TaskGrid_Head"></HeaderStyle>
													
                                    <ItemTemplate>
                                        <asp:Label ID="lblCustomer" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.CustomerName") %>'
                                            Visible="False"></asp:Label><asp:LinkButton ID="lnkCustomer" runat="server" CommandName="DoGetCustomer"
                                            CssClass="ProductGrdLinks" ForeColor="black">
															<%# DataBinder.Eval(Container, "DataItem.CustomerName") %>
														</asp:LinkButton>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Rep" SortExpression="Lastname">
                               <HeaderStyle HorizontalAlign="Left" CssClass="TaskGrid_Head"></HeaderStyle>
													                                
                                    <ItemTemplate>
                                        <asp:Label ID="lblRep" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.Lastname") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Amount" SortExpression="ChargeAmount">
                                <HeaderStyle HorizontalAlign="Left" CssClass="TaskGrid_Head"></HeaderStyle>
													
                                
                                    <ItemTemplate>
                                        <asp:Label ID="lblPaidAmount" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.ChargeAmount", "{0:C0}") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Adj1">
                                <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                
                                    <ItemTemplate>
                                        <asp:Label ID="lb_adj1" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.adj1") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Adj2">
                                <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                
                                    <ItemTemplate>
                                        <asp:Label ID="lb_adj2" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.adj2") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Payment Type" SortExpression="PaymentType">
                                <HeaderStyle HorizontalAlign="Left" CssClass="TaskGrid_Head"></HeaderStyle>
													
                                    <ItemTemplate>
                                        <asp:Label ID="lblPayTypeFR" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.CCType") %>'
                                            Visible="true">
														</asp:Label><asp:Label ID="lblPTypeId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentType") %>'
                                            Visible="False"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                              
                                <asp:TemplateColumn HeaderText="Court">
                                <HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
                                
                                    <ItemTemplate>
                                        <asp:Label ID="lblCourt" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.Court") %>'
                                            Visible="true">
														</asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                        </div>
                        </TD>
				</TR>
				
				
				<TR>
					<TD colSpan="5">
						
					</TD>
				</TR>
				
				
				
				<TR>
					<TD class="TDHeading" colSpan="5" valign="middle" >  <!-- <span   onclick="SetFlag('txtPR',1);show(HB2);show(HB3);hide(HB1);">+</span> -->
						 <div id="div_act1">&nbsp;<span onclick="SetFlag('Txt28',1);show(div_act2);show(div_act3);hide(div_act1);">+</span>&nbsp;&nbsp;<asp:textbox id="Textbox13" runat="server" CssClass="ProductHead" ReadOnly="True">Payment Waiver</asp:textbox></div>
					     <div class="divHidden" id="div_act2">&nbsp;<span onclick="SetFlag('Txt28',0);show(div_act1);hide(div_act3);hide(div_act2);">
							                    -</span>&nbsp;&nbsp;<asp:textbox id="Textbox14" runat="server" CssClass="ProductHead" ReadOnly="True">Payment Waiver</asp:textbox></div>
					</TD>
				</TR>
				<TR>
					<TD colSpan="5" align="left" valign="top">
					<div class="divHidden" id="div_act3" >
                        <asp:datagrid id="DG_AttorneyCredit2" runat="server" Width="100%" Font-Names="Verdana" Font-Size="2px"
											AutoGenerateColumns="False"  AllowCustomPaging="True" PageSize="100"
											AllowSorting="True" OnSortCommand="DG_AttorneyCredit2_SortCommand" OnItemCommand="DoGetQueryString" >
                            <FooterStyle CssClass="GrdFooter" />
                            <PagerStyle HorizontalAlign="Center" NextPageText="Next" PrevPageText="Previous" />
                            <AlternatingItemStyle BackColor="#EEEEEE" />
                            <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Center" VerticalAlign="Middle" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="No">
                               <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
													
                                
                                 <ItemTemplate>
                                <asp:Label id=lblTicketID runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.TicketID") %>' Visible="false">
														</asp:Label>
														<asp:Label id=lblCardTypeID runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.CardType") %>' Visible="false">
														</asp:Label>
														<asp:Label id="lblNo" runat="server" CssClass="GrdLbl" Text="" Visible="True"></asp:Label>
														
														</ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Date">
                                <HeaderStyle  HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblDate" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.RecDate") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Time" SortExpression="RecTime">
                                <HeaderStyle HorizontalAlign="Left" CssClass="TaskGrid_Head"></HeaderStyle>
                                
                                    <ItemTemplate>
                                        <asp:Label ID="lblTime" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.RecTime","{0:t}") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Client Name" SortExpression="CustomerName">
                                    <HeaderStyle HorizontalAlign="Left" CssClass="TaskGrid_Head"></HeaderStyle>
													
                                    <ItemTemplate>
                                        <asp:Label ID="lblCustomer" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.CustomerName") %>'
                                            Visible="False"></asp:Label><asp:LinkButton ID="lnkCustomer" runat="server" CommandName="DoGetCustomer"
                                            CssClass="ProductGrdLinks" ForeColor="black">
															<%# DataBinder.Eval(Container, "DataItem.CustomerName") %>
														</asp:LinkButton>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Rep" SortExpression="Lastname">
                               <HeaderStyle HorizontalAlign="Left" CssClass="TaskGrid_Head"></HeaderStyle>
													                                
                                    <ItemTemplate>
                                        <asp:Label ID="lblRep" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.Lastname") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Amount" SortExpression="ChargeAmount">
                                <HeaderStyle HorizontalAlign="Left" CssClass="TaskGrid_Head"></HeaderStyle>
													
                                
                                    <ItemTemplate>
                                        <asp:Label ID="lblPaidAmount" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.ChargeAmount", "{0:C0}") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Adj1">
                                <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                
                                    <ItemTemplate>
                                        <asp:Label ID="lb_adj1" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.adj1") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Adj2">
                                <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                
                                    <ItemTemplate>
                                        <asp:Label ID="lb_adj2" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.adj2") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Payment Type" SortExpression="PaymentType">
                                <HeaderStyle HorizontalAlign="Left" CssClass="TaskGrid_Head"></HeaderStyle>
													
                                    <ItemTemplate>
                                        <asp:Label ID="lblPayTypeFR" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.CCType") %>'
                                            Visible="true">
														</asp:Label><asp:Label ID="lblPTypeId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentType") %>'
                                            Visible="False"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                              
                                <asp:TemplateColumn HeaderText="Court">
                                <HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
                                
                                    <ItemTemplate>
                                        <asp:Label ID="lblCourt" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.Court") %>'
                                            Visible="true">
														</asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                        </div>
                        </TD>
				</TR>
				
				<tr>
				<td>
				<asp:textbox id="Textbox15" runat="server" Width="0px" Height="18px">1</asp:textbox><asp:textbox id="Textbox16" runat="server" Width="0px" Height="18px">1</asp:textbox><asp:textbox id="Textbox17" runat="server" Width="0px" Height="18px">1</asp:textbox><asp:textbox id="Textbox18" runat="server" Width="0px" Height="18px">1</asp:textbox><asp:textbox id="Textbox19" runat="server" Width="0px" Height="32px">1</asp:textbox><asp:textbox id="Textbox20" runat="server" Width="0px" Height="18px">1</asp:textbox><asp:textbox id="Textbox21" runat="server" Width="0px" Height="18px">0</asp:textbox><asp:textbox id="Textbox22" runat="server" Width="0px" Height="18px">0</asp:textbox><asp:textbox id="Textbox23" runat="server" Width="0px" Height="18px"></asp:textbox><asp:textbox id="Textbox24" runat="server" Width="0px" Height="18px">0</asp:textbox><asp:textbox id="Textbox25" runat="server" Width="0px" Height="18px">1</asp:textbox><asp:textbox id="Textbox26" runat="server" Width="0px" Height="18px">1</asp:textbox><asp:textbox id="Txt28" runat="server" Width="0px" Height="18px">1</asp:textbox>
				</td>
				</tr>
				<tr>
				<td>
				<asp:textbox id="txtCSS" runat="server" Width="0px" Height="18px">1</asp:textbox><asp:textbox id="txtCT" runat="server" Width="0px" Height="18px">1</asp:textbox><asp:textbox id="txtPC" runat="server" Width="0px" Height="18px">1</asp:textbox><asp:textbox id="txtRS" runat="server" Width="0px" Height="18px">1</asp:textbox><asp:textbox id="txtCS" runat="server" Width="0px" Height="32px">1</asp:textbox><asp:textbox id="txtPR" runat="server" Width="0px" Height="18px">1</asp:textbox><asp:textbox id="txtCC" runat="server" Width="0px" Height="18px">0</asp:textbox><asp:textbox id="iCount" runat="server" Width="0px" Height="18px">0</asp:textbox><asp:textbox id="txtRemarks" runat="server" Width="0px" Height="18px"></asp:textbox><asp:textbox id="txtCheck" runat="server" Width="0px" Height="18px">0</asp:textbox><asp:textbox id="txtVP" runat="server" Width="0px" Height="18px">1</asp:textbox><asp:textbox id="txtAtt" runat="server" Width="0px" Height="18px">1</asp:textbox>
				</td>
				</tr>
				<TR>
					<TD colSpan="5">
						
					</TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>

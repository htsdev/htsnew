using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using System.IO;
//using System.Web.Mail;
using System.Net.Mail;
using System.Text;
using System.Net;
using lntechNew.Components.ClientInfo;


//using iTextSharp.text;
//using iTextSharp.text.pdf;


namespace HTP.PaymentInfo
{
    /// <summary>
    /// Summary description for PaymentInfo.
    /// </summary>
    public partial class CloseOutReport : System.Web.UI.Page
    {
        protected System.Web.UI.WebControls.DropDownList cmbType;

        clsENationWebComponents objEnationFramework = new clsENationWebComponents();
        //Waqas 5110 01/22/2009 Adding FinancialRep
        FinancialReports objFinancialReps = new FinancialReports();
        clsSession ClsSession = new clsSession();
        clsUser cUser = new clsUser();
        clsLogger clog = new clsLogger();
        //string sqlQuery;
        DataSet dsPaymentInfo;
        //string pdfile;
        string strSMTPServer = "";
        string strMailFrom = "";
        //string strMailTo="";
        //string strMailCC="";
        //string strMailBCC="";
        int branchId;
        Decimal CashSysTotal = 0;
        Decimal CashActualTotal = 0;
        Decimal CheckSysTotal = 0;
        Decimal CheckActualTotal = 0;
        Decimal TotalSysTotal = 0;
        Decimal TotalActualTotal = 0;
        double TotalAmount = 0;
        double TotalCourtFee = 0;
        Int32 TotalCount = 0;

        string StrExp = String.Empty;
        string StrAcsDec = String.Empty;
        DataView dv_Result;

        //protected eWorld.UI.CalendarPopup calQueryDatenewdt;
        protected System.Web.UI.WebControls.ImageButton imgbtnSendMail;
        protected System.Web.UI.WebControls.LinkButton lnkbtnPrint;
        protected System.Web.UI.WebControls.Label lblMessage;
        protected System.Web.UI.WebControls.TextBox lblHeading1;
        protected System.Web.UI.WebControls.TextBox lblHeading1_1;
        protected System.Web.UI.WebControls.DataGrid dgrdPayType;
        protected System.Web.UI.WebControls.TextBox Textbox3;
        protected System.Web.UI.WebControls.TextBox Textbox4;
        protected System.Web.UI.WebControls.DataGrid dgrdPayByRep;
        protected System.Web.UI.WebControls.Label lblTotalCashSys;
        protected System.Web.UI.WebControls.Label lblTotalCashActual;
        protected System.Web.UI.WebControls.Label lblTotalCheckSys;
        protected System.Web.UI.WebControls.Label lblTotalCheckActual;
        protected System.Web.UI.WebControls.Label lblTotalSystem;
        protected System.Web.UI.WebControls.Label lblTotalActual;
        protected System.Web.UI.WebControls.TextBox txtRemarks;
        protected System.Web.UI.WebControls.Button btnUpdate;
        protected System.Web.UI.WebControls.TextBox Textbox5;
        protected System.Web.UI.WebControls.TextBox Textbox6;
        protected System.Web.UI.WebControls.DataGrid dgrdCourt;
        protected System.Web.UI.WebControls.TextBox Textbox1;
        protected System.Web.UI.WebControls.TextBox Textbox2;
        protected System.Web.UI.WebControls.DropDownList cmbRep;
        protected System.Web.UI.WebControls.DropDownList cmbPayType;
        protected System.Web.UI.WebControls.DropDownList cmbCourt;
        protected System.Web.UI.WebControls.DataGrid dgrdPayDetail;
        protected System.Web.UI.WebControls.TextBox txtPR;
        protected System.Web.UI.WebControls.TextBox txtPC;
        protected System.Web.UI.WebControls.TextBox txtRS;
        protected System.Web.UI.WebControls.TextBox txtCS;
        protected System.Web.UI.WebControls.TextBox TextBox7;
        protected System.Web.UI.WebControls.TextBox txtEmail;
        protected System.Web.UI.WebControls.TextBox txtCC;
        protected System.Web.UI.WebControls.TextBox txtCheck;
        protected System.Web.UI.WebControls.TextBox iCount;
        Int32 TotalCourtTrans = 0;



        private void Page_Load(object sender, System.EventArgs e)
        {
            string cookyval = "0";
            try
            {
                //Waqas 5057 03/17/2009 Checking employee info in session
                if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false)
                {

                    Response.Redirect("../frmlogin.aspx", false);
                }
                else //To stop page further execution
                {
                    if (Convert.ToBoolean(ClsSession.GetCookie("sCanUpdateCloseOut", this.Request)) == false)
                    {
                        //Kazim 3928 5/7/2008 Remove Response.End after Response.Redirect 

                        Response.Redirect("../LoginAccesserror.aspx", false);
                        goto SearchPage;
                    }
                    else
                    {
                        if (!Page.IsPostBack)
                        {
                            calQueryDatenewdt.Text = calQueryDatenewdt.Text = DateTime.Today.ToShortDateString(); ;
                            // Sabir Khan 10920 05/27/2013 Fill Branch Drop Down.
                            GetBranchesName();
                            //Get_AdminEmail();
                            GetReps();
                            LoadInital();
                            BindControls();

                            GetPaymentTypeSumByDate();
                            GetCourtSummary();

                            GetPaymentDetailByDateByRep(Convert.ToDateTime(calQueryDatenewdt.Text));
                            GetPaymentDetailReport(Convert.ToDateTime(calQueryDatenewdt.Text));
                            GetMailerSummary(Convert.ToDateTime(calQueryDatenewdt.Text));

                            Session["RepID"] = cmbRep.SelectedIndex;
                            Session["PaymentID"] = cmbPayType.SelectedIndex;
                            Session["CourtID"] = cmbCourt.SelectedIndex;

                        }

                        if (Request.Cookies["CanUpdateCloseOutLog"] != null)
                        {
                            cookyval = Server.HtmlEncode(Request.Cookies["CanUpdateCloseOutLog"].Value);
                        }

                        cookyval = "1";

                        if (cookyval == "1")
                        {
                            btnUpdate.Enabled = true;
                            //lnkEmail.Enabled=true;
                            lnkbtnPrint.Enabled = true;
                            calQueryDatenewdt.Enabled = true;
                        }
                        else
                        {
                            btnUpdate.Enabled = false;
                            //lnkEmail.Enabled=false;
                            lnkbtnPrint.Enabled = false;
                            calQueryDatenewdt.Enabled = false;
                        }
                    }
                }
                
                SearchPage:
                { }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        protected void ddl_Branch_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            Session["flagPC"] = txtPC.Text;
            Session["flagPR"] = txtPR.Text;

            cmbPayType.SelectedIndex = 0;
            cmbRep.SelectedIndex = 0;
            cmbCourt.SelectedIndex = 0;
            GetReps();
            GetPaymentTypeSumByDate();
            GetCourtSummary();
            GetPaymentDetailByDateByRep(Convert.ToDateTime(calQueryDatenewdt.Text));
            GetPaymentDetailReport(Convert.ToDateTime(calQueryDatenewdt.Text));
            GetMailerSummary(Convert.ToDateTime(calQueryDatenewdt.Text));
        }

        private int GetDayCount()
        {
            if (DateTime.Now.Date.DayOfWeek == DayOfWeek.Monday)
                return 4;
            else
                return 3;
        }

        private void LoadInital()
        {
            try
            {
                ///////////////calQueryDatenewdt.UpperBoundDate = DateTime.Now.Date;/////////////////////////

                // If current user is not a 'Primary User' then show only last 3 days
                //if (ClsSession.GetCookie("sAccessType", this.Request).ToString() != "2")
                //    calQueryDatenewdt.LowerBoundDate = DateTime.Now.AddDays(-1 * GetDayCount());
                if (ClsSession.GetCookie("sAccessType", this.Request).ToString() != "2")
                {
                    // Sabir Khan 10920 05/27/2013 Get branch info of the selected user.
                    DataTable dtbranchInfo = objFinancialReps.GetBranchInfo(this.Request);
                    
                    ddl_Branch.Visible = false;
                    lblBranchTitle.Visible = false;
                    branchId = Convert.ToInt32(dtbranchInfo.Rows[0]["BranchId"]);
                    lblBranch.Text = dtbranchInfo.Rows[0]["BranchName"] + " Transactions";
                    ViewState["branchId"] = branchId;
                    ddl_Branch.SelectedValue = branchId.ToString();
                    ViewState["branchId"] = branchId;
                    //==========================================================================================
                    // Changed by Tahir Ahmed Task 3199 Dt: 02/20/2008
                    // Allow Kendall to access past two weeks...
                    // Allow Brenda Rodriguez to access last business day & today
                    // And for all other secondary users allow only for current date
                    // Primary users will have full access to select any date range

                    int iEmployeeId = Convert.ToInt16(ClsSession.GetCookie("sEmpID", this.Request));

                    // Noufil 4232 7/03/2008 get no of days from the database instead of hard coding
                    clsUser cUser = new clsUser(iEmployeeId);
                    cUser.EmpID = iEmployeeId;
                    cUser.GetSystemUserInfo();

                    if (cUser.CloseOutAccessDay != 0)
                    {
                        if (DateTime.Now.DayOfWeek.ToString().ToLower() == "monday")
                        {
                           //////////////// calQueryDatenewdt.LowerBoundDate = DateTime.Now.AddDays(-(cUser.CloseOutAccessDay + 2));////////////////
                        }
                        else
                        {
                           //////////////////////// calQueryDatenewdt.LowerBoundDate = DateTime.Now.AddDays(-(cUser.CloseOutAccessDay));////////////
                        }
                    }
                    else
                    {
                       //////////// calQueryDatenewdt.LowerBoundDate = DateTime.Now.Date;//////////////
                    }

                }
                calQueryDatenewdt.Text = DateTime.Now.Date.ToShortDateString();
                //calQueryDatenewdt.VisibleDate = calQueryDatenewdt.Text;
                //Waqas 5110 01/22/2009
                //objEnationFramework.FetchValuesInWebControlBysp(cmbCourt, "usp_Get_All_Court", "ShortName", "Courtid");


                cmbCourt.DataSource = objFinancialReps.GetAllCourtsForCloseOut();
                cmbCourt.DataTextField = "ShortName";
                cmbCourt.DataValueField = "Courtid";
                cmbCourt.DataBind();

                cmbCourt.Items[0].Text = "All Courts";

                // tahir 4225 07/16/2008
                // fixed the court summary hyperlink click bug...
                cmbCourt.Items.Insert(1, new ListItem("All HMC", "1"));
                cmbCourt.Items.Insert(2, new ListItem("All HCJP", "2"));
                cmbCourt.Items.Insert(3, new ListItem("All Other", "100"));
                // end 4225

                GetReps();


            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }




        }

        private void SortGrid(string SortExp)
        {
            try
            {
                SetAcsDesc(SortExp);
                dv_Result = (DataView)(ClsSession.GetSessionObject("dvResult", this.Session));
                dv_Result.Sort = StrExp + " " + StrAcsDec;
                ClsSession.SetSessionVariable("dvResult", dv_Result, this.Session);
                dgrdPayDetail.DataSource = dv_Result;
                dgrdPayDetail.DataBind();
                BindPaymentDetailReport();

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }

        private void SetAcsDesc(string Val)
        {
            try
            {
                StrExp = ClsSession.GetSessionVariable("StrExp", this.Session);
                StrAcsDec = ClsSession.GetSessionVariable("StrAcsDec", this.Session);
            }
            catch
            {

            }

            if (StrExp == Val)
            {
                if (StrAcsDec == "ASC")
                {
                    StrAcsDec = "DESC";
                    ClsSession.SetSessionVariable("StrAcsDec", StrAcsDec, this.Session);
                }
                else
                {
                    StrAcsDec = "ASC";
                    ClsSession.SetSessionVariable("StrAcsDec", StrAcsDec, this.Session);
                }
            }
            else
            {
                StrExp = Val;
                StrAcsDec = "ASC";
                ClsSession.SetSessionVariable("StrExp", StrExp, this.Session);
                ClsSession.SetSessionVariable("StrAcsDec", StrAcsDec, this.Session);
            }
        }

        public void GetReps()
        {

            //Waqas 5110 01/22/2009
            try
            {
                DataSet dsRepList = new DataSet();
                //Waqas 5110 01/22/2009
                //string[] key2 = { "@sDate", "@eDate" };
                //object[] value2 = { calQueryDatenewdt.Text, calQueryDatenewdt.Text };
                //dsRepList = objEnationFramework.Get_DS_BySPArr("usp_Get_All_RepList", key2, value2);
                dsRepList = objFinancialReps.GetAllRepList(Convert.ToDateTime(calQueryDatenewdt.Text), Convert.ToDateTime(calQueryDatenewdt.Text));
                dgrdPayByRep.DataSource = dsRepList;
                dgrdPayByRep.DataBind();

                cmbRep.Items.Clear();

                if (dsRepList.Tables.Count > 0)
                    if (dsRepList.Tables[0].Rows.Count > 0)
                    {
                        cmbRep.DataSource = dsRepList;
                        cmbRep.DataTextField = dsRepList.Tables[0].Columns[0].ColumnName;
                        cmbRep.DataValueField = dsRepList.Tables[0].Columns[1].ColumnName;
                        cmbRep.DataBind();
                    }
                cmbRep.Items.Insert(0, "All Reps");
                cmbRep.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        private void BindControls()
        {
            //Change by Ajmal
            //Waqas 5110 01/22/2009
            //IDataReader drRepList;
            DataSet drPaymentTypeList;

            try
            {
                //GetReps();
                //objEnationFramework.FetchValuesInWebControlBysp( cmbRep ,"usp_Get_All_RepList","RepName","RepID");
                /*string[] key1 = {"@sDate", "@eDate"};
                object[] value1 = {calQueryDatenewdt.Text, calQueryDatenewdt.Text};
                drRepList = objEnationFramework.Get_DR_BySPArr("usp_Get_All_RepList",key1,value1);
						
                cmbRep.Items.Add("All Reps");
			
                while (drRepList.Read())
                {
                    //MyContainer container = new MyContainer();
                    //container.SomeString = reader.GetString(0);
                    //container.SomeInt = reader.GetInt(1);
                    //alReps.Add(container);
                    System.Web.UI.WebControls.ListItem ItemY = new System.Web.UI.WebControls.ListItem();	
				
                    ItemY.Text= drRepList["RepName"].ToString(); 
                    ItemY.Value = drRepList["RepID"].ToString() ; 
                    cmbRep.Items.Add(ItemY);
                }
                drRepList.Close();*/

                //Waqas 5110 01/26/2009

                //drRepList = objEnationFramework.Get_DR_BySP("usp_Get_All_CCType");

                //System.Web.UI.WebControls.ListItem Item10 = new System.Web.UI.WebControls.ListItem("Show All (Incl. Attorney/Friend Credit)", "-1");
                //cmbPayType.Items.Add(Item10);

                //System.Web.UI.WebControls.ListItem Item1 = new System.Web.UI.WebControls.ListItem("Show All (Incl. Attorney Credit)", "0");
                //cmbPayType.Items.Add(Item1);

                //System.Web.UI.WebControls.ListItem Item11 = new System.Web.UI.WebControls.ListItem("Show All (Excl. Attorney/Friend Credit)", "-200");
                //cmbPayType.Items.Add(Item11);

                //System.Web.UI.WebControls.ListItem Item2 = new System.Web.UI.WebControls.ListItem("Show All (Excl. Attorney Credit)", "200");
                //cmbPayType.Items.Add(Item2);


                //System.Web.UI.WebControls.ListItem Item3 = new System.Web.UI.WebControls.ListItem("CC All (Including Manual)", "300");
                //cmbPayType.Items.Add(Item3);

                //System.Web.UI.WebControls.ListItem Item4 = new System.Web.UI.WebControls.ListItem("CC All (Excluding Manual)", "301");
                //cmbPayType.Items.Add(Item4);



                //while (drRepList.Read())
                //{
                //    //MyContainer container = new MyContainer();
                //    //container.SomeString = reader.GetString(0);
                //    //container.SomeInt = reader.GetInt(1);
                //    //alReps.Add(container);
                //    System.Web.UI.WebControls.ListItem ItemY = new System.Web.UI.WebControls.ListItem();

                //    ItemY.Text = drRepList["Description"].ToString();
                //    ItemY.Value = drRepList["PaymentType_PK"].ToString();
                //    cmbPayType.Items.Add(ItemY);
                //}
                //drRepList.Close();

                //// CC Types
                //System.Web.UI.WebControls.ListItem Item5 = new System.Web.UI.WebControls.ListItem("CC (VISA)", "501");
                //cmbPayType.Items.Add(Item5);
                //System.Web.UI.WebControls.ListItem Item6 = new System.Web.UI.WebControls.ListItem("CC (DISCOVER)", "504");
                //cmbPayType.Items.Add(Item6);
                //System.Web.UI.WebControls.ListItem Item7 = new System.Web.UI.WebControls.ListItem("CC (MASTER CARD)", "502");
                //cmbPayType.Items.Add(Item7);
                //System.Web.UI.WebControls.ListItem Item8 = new System.Web.UI.WebControls.ListItem("CC (AMEX)", "503");
                //cmbPayType.Items.Add(Item8);


                ////System.Web.UI.WebControls.ListItem Item9 = new System.Web.UI.WebControls.ListItem("CC (MANUAL)", "500" );				
                //cmbPayType.Items.Add(Item9);
                cmbPayType.Items.Clear();
                drPaymentTypeList = objFinancialReps.GetPaymentTypeForCloseOut();

                cmbPayType.DataSource = drPaymentTypeList;
                cmbPayType.DataTextField = "Description";
                cmbPayType.DataValueField = "PaymentType_PK";
                cmbPayType.DataBind();

                //cmbPayType.Items.Insert(0, new ListItem("Show All (Incl. Attorney/Friend Credit)", "-1"));
                //cmbPayType.Items.Insert(1, new ListItem("Show All (Incl. Attorney Credit)", "0"));
                //cmbPayType.Items.Insert(2, new ListItem("Show All (Excl. Attorney/Friend Credit)", "-200"));
                //cmbPayType.Items.Insert(3, new ListItem("Show All (Excl. Attorney Credit)", "200"));
                //Fahad 6054 07/30/2009 Comment ablove lines and Add Following Lines
                cmbPayType.Items.Insert(0, new ListItem("Show All (With All Credit)", "1"));
                cmbPayType.Items.Insert(2, new ListItem("Show All (Without Credit)", "200"));

                cmbPayType.Items.Insert(4, new ListItem("CC All (Including Manual)", "300"));
                cmbPayType.Items.Insert(5, new ListItem("CC All (Excluding Manual)", "301"));

                

                // CC Types
                cmbPayType.Items.Add(new ListItem("CC (VISA)", "501"));
                cmbPayType.Items.Add(new ListItem("CC (DISCOVER)", "504"));
                cmbPayType.Items.Add(new ListItem("CC (MASTER CARD)", "502"));
                cmbPayType.Items.Add(new ListItem("CC (AMEX)", "503"));

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        private void GetPaymentDetailByDateByRep(DateTime QueryDate)
        {
            DataSet dsWeeklyPaymentDetailsByRep;
            string strEmployeeID;
            //string Temp;			
            string strExpr;
            Decimal Total;
            Decimal TotalActual = 0;
            //			Decimal CashSysTotal=0;
            //			Decimal CashActualTotal=0;
            //			Decimal CheckSysTotal=0;
            //			Decimal CheckActualTotal=0;
            //			Decimal TotalSysTotal=0;
            //			Decimal TotalActualTotal=0;
            //bool bAlternate = false;

            //Waqas 5110 01/22/2009
            try
            {
                //waqas 5110 01/22/2009
                //string[] key1 = { "@RecDate", "@RecDateTo" };
                //object[] value1 = { QueryDate, QueryDate };
                ////dsPaymentInfo  = objEnationFramework.Get_DS_BySPByOneParmameter("usp_Get_All_PaymentDetailByRecDate","RecDate",QueryDate); 
                //dsPaymentInfo = objEnationFramework.Get_DS_BySPArr("usp_Get_All_PaymentDetailByRecDateRange", key1, value1);
                // Sabir Khan 10920 05/27/2013 Branch Id added.
                dsPaymentInfo = objFinancialReps.GetPaymentDetailByDateByRep(QueryDate, QueryDate, Convert.ToInt32(ddl_Branch.SelectedValue));
                //dsWeeklyPaymentDetailsByRep = objEnationFramework.Get_DS_BySPByOneParmameter("usp_Get_All_tblPaymentDetailWeeklyByDate", "TransDate", QueryDate);
                // Sabir Khan 10920 05/27/2013 Branch Id added.
                dsWeeklyPaymentDetailsByRep = objFinancialReps.GetWeeklyPaymentDetailByDateByRep(QueryDate, Convert.ToInt32(ddl_Branch.SelectedValue));
                // Get Day End Notes
                if (dsWeeklyPaymentDetailsByRep.Tables[0].Rows.Count > 0)
                {
                    txtRemarks.Text = dsWeeklyPaymentDetailsByRep.Tables[0].Rows[0]["Notes"].ToString();
                }
                else
                {
                    txtRemarks.Text = "";
                }

                //create and populate a DataColumn to set the PK
                //DataColumn [] pkColumn = new DataColumn[1];
                //pkColumn[0] = dsPaymentInfo.Tables[0].Columns["EmployeeID"];
                //set the primary key 
                //dsWeeklyPaymentDetailsByRep.Tables[0].PrimaryKey = pkColumn;			



                foreach (DataGridItem ItemX in dgrdPayByRep.Items)
                {
                    DataRow[] foundRows;
                    DataRow[] PaymentRows;
                    string txtboxID;
                    string fxName;
                    Decimal dActualCash = 0;
                    Decimal dActualCheck = 0;
                    Decimal dSysCash = 0;
                    Decimal dSysCheck = 0;




                    // js Validation Bind to textboxes
                    txtboxID = ((TextBox)(ItemX.FindControl("txtActualCash"))).ClientID.ToString();
                    fxName = "return ValidateMe(" + txtboxID + ");";
                    ((TextBox)(ItemX.FindControl("txtActualCash"))).Attributes.Add("Onblur", fxName);


                    txtboxID = ((TextBox)(ItemX.FindControl("txtActualCheck"))).ClientID.ToString();
                    fxName = "return ValidateMe(" + txtboxID + ");";
                    ((TextBox)(ItemX.FindControl("txtActualCheck"))).Attributes.Add("Onblur", fxName);
                    //  Bind End


                    strEmployeeID = ((Label)(ItemX.FindControl("lblEmployeeID"))).Text.ToString();
                    //find a row based on the value entered in the textbox
                    //DataRow rowWeeklyPay = dsWeeklyPaymentDetailsByRep.Tables[0].Rows.Find(EmployeeID);
                    dsWeeklyPaymentDetailsByRep.Tables[0].Select();

                    strExpr = "EmployeeID =" + strEmployeeID;
                    foundRows = dsWeeklyPaymentDetailsByRep.Tables[0].Select(strExpr);


                    if (foundRows.Length > 0)
                    {
                        ((TextBox)(ItemX.FindControl("txtActualCash"))).Text = String.Format("{0:#.##}", (foundRows[0]["ActualCash"]));
                        ((TextBox)(ItemX.FindControl("txtActualCheck"))).Text = String.Format("{0:#.##}", (foundRows[0]["ActualCheck"]));

                        TotalActual = Convert.ToDecimal(foundRows[0]["ActualCash"]) + Convert.ToDecimal(foundRows[0]["ActualCheck"]);
                        dActualCash = Convert.ToDecimal(foundRows[0]["ActualCash"]);
                        dActualCheck = Convert.ToDecimal(foundRows[0]["ActualCheck"]);

                        CashActualTotal += dActualCash;
                        CheckActualTotal += dActualCheck;
                    }
                    else
                    {
                        ((TextBox)(ItemX.FindControl("txtActualCash"))).Text = "";
                        ((TextBox)(ItemX.FindControl("txtActualCheck"))).Text = "";
                        TotalActual = 0;
                    }


                    dsPaymentInfo.Tables[0].Select();

                    strExpr = "EmpID =" + strEmployeeID;
                    PaymentRows = dsPaymentInfo.Tables[0].Select(strExpr);

                    if (PaymentRows.Length > 0)
                    {
                        ((Label)(ItemX.FindControl("lblSystemCash"))).Text = String.Format("{0:c}", PaymentRows[0]["CashAmount"]);
                        ((Label)(ItemX.FindControl("lblSystemCheck"))).Text = String.Format("{0:c}", PaymentRows[0]["CheckAmount"]);

                        Total = Convert.ToDecimal(PaymentRows[0]["CashAmount"]) + Convert.ToDecimal(PaymentRows[0]["CheckAmount"]);
                        dSysCash = Convert.ToDecimal(PaymentRows[0]["CashAmount"]);
                        dSysCheck = Convert.ToDecimal(PaymentRows[0]["CheckAmount"]);

                        CashSysTotal += dSysCash;
                        CheckSysTotal += dSysCheck;
                    }
                    else
                    {
                        ((Label)(ItemX.FindControl("lblSystemCash"))).Text = "";
                        ((Label)(ItemX.FindControl("lblSystemCheck"))).Text = "";
                        Total = 0;
                    }



                    // Check difference and highlight
                    if (dSysCash != dActualCash)
                    {
                        ((Label)(ItemX.FindControl("lblSystemCash"))).BackColor = Color.Red;
                        ((Label)(ItemX.FindControl("lblSystemCash"))).ForeColor = Color.White;
                    }
                    else
                    {
                        ((Label)(ItemX.FindControl("lblSystemCash"))).BackColor = Color.White;
                        ((Label)(ItemX.FindControl("lblSystemCash"))).ForeColor = Color.Black;
                    }


                    if (dSysCheck != dActualCheck)
                    {
                        ((Label)(ItemX.FindControl("lblSystemCheck"))).BackColor = Color.Red;
                        ((Label)(ItemX.FindControl("lblSystemCheck"))).ForeColor = Color.White;
                    }
                    else
                    {
                        ((Label)(ItemX.FindControl("lblSystemCheck"))).BackColor = Color.White;
                        ((Label)(ItemX.FindControl("lblSystemCheck"))).ForeColor = Color.Black;
                    }




                    // Row System Total 
                    if (Total != 0)
                    {
                        ((Label)(ItemX.FindControl("lblSystemTotal"))).Text = String.Format("{0:c}", Total);
                        TotalSysTotal += Total;
                    }
                    else
                    {
                        ((Label)(ItemX.FindControl("lblSystemTotal"))).Text = "";
                    }

                    // Row Actual Total 
                    if (TotalActual != 0)
                    {
                        ((Label)(ItemX.FindControl("lblActualTotal"))).Text = String.Format("{0:c}", TotalActual);
                        TotalActualTotal += TotalActual;
                    }
                    else
                    {
                        ((Label)(ItemX.FindControl("lblActualTotal"))).Text = "";
                    }

                    // If Total is zero so hide the row
                    if ((Total + TotalActual) == 0)
                    {
                        ItemX.Visible = false;
                    }
                    else
                    {
                        ItemX.Visible = true;

                        //						if ( bAlternate==true )
                        //						{
                        //							ItemX.BackColor = Color.FromArgb(238,238,238) ;
                        //						}
                        //					
                        //						bAlternate = ! bAlternate;

                    }

                }

                lblTotalCashSys.Text = String.Format("{0:c}", CashSysTotal);
                lblTotalCashActual.Text = String.Format("{0:c}", CashActualTotal);
                lblTotalCheckSys.Text = String.Format("{0:c}", CheckSysTotal);
                lblTotalCheckActual.Text = String.Format("{0:c}", CheckActualTotal);
                lblTotalSystem.Text = String.Format("{0:c}", TotalSysTotal);
                lblTotalActual.Text = String.Format("{0:c}", TotalActualTotal);

                //dgrdPayByRep.AlternatingItemStyle.BackColor= Color.FromArgb(238,238,238) ;
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }
        //
        //		private void GetPaymentSummaryByDate(DateTime  QueryDate)
        //		{
        //			string CCTypeID;
        //			
        //			try
        //			{
        //				dsPaymentInfo  = objEnationFramework.Get_DS_BySPByOneParmameter("usp_Get_All_PaymentInfoByRecDate","RecDate",QueryDate); 
        //			
        //				//create and populate a DataColumn to set the PK
        //				DataColumn [] pkColumn = new DataColumn[1];
        //				pkColumn[0] = dsPaymentInfo.Tables[0].Columns["PaymentType"];
        //				//set the primary key 
        //				dsPaymentInfo.Tables[0].PrimaryKey = pkColumn;
        //
        //			
        //				foreach (DataGridItem ItemX in dgrdPayType.Items) 
        //				{ 
        //					//((LinkButton)(dgItem.FindControl("lblCCTypeID"))).Attributes.Add("Onclick", "LoadDocument();"); 
        //
        //					CCTypeID=  ((Label)(ItemX.FindControl("lblPayTypeID"))).Text  ; 
        //				
        //					//find a row based on the value entered in the textbox
        //					DataRow rowFound = dsPaymentInfo.Tables[0].Rows.Find(CCTypeID);
        //
        //					if (rowFound != null)				
        //					{
        //						((Label)(ItemX.FindControl("lblCount"))).Text = rowFound["TotalCount"].ToString() ;				
        //						//((Label)(ItemX.FindControl("lblAmount"))).Text = String.Format("{0:#,###.##}", rowFound["Amount"]);
        //						((Label)(ItemX.FindControl("lblAmount"))).Text = String.Format("{0:c}", rowFound["Amount"]);
        //					}
        //					else
        //					{
        //						((Label)(ItemX.FindControl("lblCount"))).Text = "" ;									
        //						((Label)(ItemX.FindControl("lblAmount"))).Text = "" ;
        //					}
        //				
        //				}
        //			}
        //			catch(Exception ex)
        //			{
        //				lblMessage.Text = ex.Message ;
        //
        //			}		
        //
        //		}
        //	
        private void GetPaymentSummaryByDate(DateTime QueryDate)
        {
            string CCTypeID;
            //Waqas 5110 01/22/2009
            try
            {
                //Waqas 5110 01/21/2009 Calling procedure from FinancialReps class files.
                dsPaymentInfo = objFinancialReps.GetPaymentSummaryByDate(QueryDate);
                //dsPaymentInfo = objEnationFramework.Get_DS_BySPByOneParmameter("usp_Get_All_PaymentInfoByRecDate", "RecDate", QueryDate);
                //dsPaymentInfo  = objEnationFramework.Get_DS_BySPByTwoParmameter("usp_Get_All_PaymentInfoByRecDateRange","RecDate",QueryDate,"RecTo",calTo.SelectedDate ); 

                //create and populate a DataColumn to set the PK
                DataColumn[] pkColumn = new DataColumn[1];
                pkColumn[0] = dsPaymentInfo.Tables[0].Columns["PaymentType"];
                //set the primary key 
                dsPaymentInfo.Tables[0].PrimaryKey = pkColumn;


                foreach (DataGridItem ItemX in dgrdPayType.Items)
                {
                    //((LinkButton)(dgItem.FindControl("lblCCTypeID"))).Attributes.Add("Onclick", "LoadDocument();"); 

                    CCTypeID = ((Label)(ItemX.FindControl("lblPayTypeID"))).Text;

                    //find a row based on the value entered in the textbox
                    DataRow rowFound = dsPaymentInfo.Tables[0].Rows.Find(CCTypeID);

                    if (rowFound != null)
                    {
                        ((Label)(ItemX.FindControl("lblCount"))).Text = rowFound["TotalCount"].ToString();
                        //((Label)(ItemX.FindControl("lblAmount"))).Text = String.Format("{0:#,###.##}", rowFound["Amount"]);
                        ((Label)(ItemX.FindControl("lblAmount"))).Text = String.Format("{0:c}", rowFound["Amount"]);
                    }
                    else
                    {
                        ((Label)(ItemX.FindControl("lblCount"))).Text = "0";
                        ((Label)(ItemX.FindControl("lblAmount"))).Text = String.Format("{0:c}", 0);
                    }

                }
            }

            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void GetPaymentDetailReport(DateTime QueryDate)
        {
            DataSet dsFinReport;
            int RepID = 0;
            int PayID = 0;
            int CourtID = 0;

            try
            {

                if (cmbRep.SelectedIndex != 0)
                {
                    RepID = Convert.ToInt32(cmbRep.SelectedValue);
                }

                //if (cmbPayType.SelectedIndex!=0 ) 						
                //{
                PayID = Convert.ToInt32(cmbPayType.SelectedValue);
                //}

                if (cmbCourt.SelectedIndex != 0)
                {
                    CourtID = Convert.ToInt32(cmbCourt.SelectedValue);
                }

                //PayID = Convert.ToInt32(cmbPayType.SelectedValue); 

                //objEnationFramework.FetchValuesInWebControlBysp(dgrdPayDetail,"usp_Get_All_PaymentDetailByCriteria");  
                if (PayID > 500) // IF Credit Card
                {
                    //dsFinReport = objEnationFramework.Get_DS_BySPByThreeParmameter("usp_Get_All_PaymentDetailOfCCByCriteria","RecDate",QueryDate ,"EmployeeID", RepID, "PaymentType", PayID);
                    //Waqas 5110 01/21/2009
                    //string[] keys = { "@RecDate", "@RecDateTo", "@EmployeeID", "@PaymentType", "@CourtID" };
                    //object[] values = { calQueryDatenewdt.Text, calQueryDatenewdt.Text, RepID, PayID, CourtID };
                    //dsFinReport = objEnationFramework.Get_DS_BySPArr("usp_Get_All_PaymentDetailOfCCByCriteria", keys ,values ); 
                    //dsFinReport = objEnationFramework.Get_DS_BySPArr("usp_Get_All_PaymentDetailOfCCByCriteriaRange", keys, values);
                    // Sabir Khan 10920 05/27/2013 Branch Id added.
                    dsFinReport = objFinancialReps.GetPaymentDetailsForCreditCards(Convert.ToDateTime(calQueryDatenewdt.Text), Convert.ToDateTime(calQueryDatenewdt.Text), RepID, PayID, CourtID, Convert.ToInt32(ddl_Branch.SelectedValue));
                }
                else
                {
                    //Waqas 5110 01/22/2009
                    //dsFinReport = objEnationFramework.Get_DS_BySPByThreeParmameter("usp_Get_All_PaymentDetailByCriteria","RecDate",QueryDate ,"EmployeeID", RepID, "PaymentType", PayID);
                    //string[] keys = { "@RecDate", "@RecDateto", "@EmployeeID", "@PaymentType", "@CourtID" };
                    //object[] values = { calQueryDatenewdt.Text, calQueryDatenewdt.Text, RepID, PayID, CourtID };
                    //dsFinReport = objEnationFramework.Get_DS_BySPArr("usp_Get_All_PaymentDetailByCriteria", keys ,values ); 
                    //dsFinReport = objEnationFramework.Get_DS_BySPArr("usp_Get_All_PaymentDetailByCriteriaRange", keys, values);
                    // Sabir Khan 10920 05/27/2013 Branch Id added.
                    dsFinReport = objFinancialReps.GetPaymentDetailsForByCriteriaRange(Convert.ToDateTime(calQueryDatenewdt.Text), Convert.ToDateTime(calQueryDatenewdt.Text), RepID, PayID, CourtID, Convert.ToInt32(ddl_Branch.SelectedValue));
                }

                ClsSession.SetSessionVariable("dvResult", dsFinReport.Tables[0].DefaultView, this.Session);

                dgrdPayDetail.DataSource = dsFinReport;
                dgrdPayDetail.DataBind();

                BindPaymentDetailReport();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }


        }

        private void BindPaymentDetailReport()
        {
            int sNo = 0;

            try
            {

                foreach (DataGridItem ItemX in dgrdPayDetail.Items)
                {
                    //((LinkButton)(dgItem.FindControl("lblCCTypeID"))).Attributes.Add("Onclick", "LoadDocument();"); 
                    sNo += 1;

                    ((Label)(ItemX.FindControl("lblNo"))).Text = sNo.ToString();

                    if (((Label)(ItemX.FindControl("lblBond"))).Text == Convert.ToString('0'))
                    {
                        ((Label)(ItemX.FindControl("lblBond"))).Text = "";
                    }

                    // CardType Column					
                    ((Label)(ItemX.FindControl("lblCardType"))).Text = GetCardType(((Label)(ItemX.FindControl("lblCardTypeID"))).Text.ToString());

                    // RED MARKING THE REFUND TRANSACTIONS.......
                    int pTypeId = Convert.ToInt32(((Label)(ItemX.FindControl("lblPTypeId"))).Text);
                    //if (((Label)(ItemX.FindControl("lblPTypeId"))).Text   == "8")
                    if (pTypeId == 8)
                    {
                        ((Label)(ItemX.FindControl("lblNo"))).ForeColor = System.Drawing.Color.Red;
                        //((Label)(ItemX.FindControl("lblDate"))).ForeColor = System.Drawing.Color.Red;
                        ((Label)(ItemX.FindControl("lblTime"))).ForeColor = System.Drawing.Color.Red;
                        ((LinkButton)(ItemX.FindControl("lnkCustomer"))).ForeColor = System.Drawing.Color.Red;
                        ((Label)(ItemX.FindControl("lblRep"))).ForeColor = System.Drawing.Color.Red;
                        ((Label)(ItemX.FindControl("lblBond"))).ForeColor = System.Drawing.Color.Red;
                        ((Label)(ItemX.FindControl("lblPaidAmount"))).ForeColor = System.Drawing.Color.Red;
                        ((Label)(ItemX.FindControl("lblPayTypeFR"))).ForeColor = System.Drawing.Color.Red;
                        ((Label)(ItemX.FindControl("lblCardType"))).ForeColor = System.Drawing.Color.Red;
                        ((Label)(ItemX.FindControl("lblCourt"))).ForeColor = System.Drawing.Color.Red;
                        //((Label)(ItemX.FindControl("lblListDate"))).ForeColor = System.Drawing.Color.Red;
                    }

                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        private string GetCardType(string CardTypeID)
        {
            switch (CardTypeID)
            {
                case "1":
                    return ("VISA");
                case "2":
                    return ("MC");
                case "3":
                    return ("AMEX");
                case "4":
                    return ("DISC");
                default:
                    return ("");
            }

        }

        private void GetPaymentSummary(int CCTypeID)
        {


            //DR  = objEnationFramework.Get_All_RecordsBySPByOneParmameter("usp_Get_All_PaymentDetailByDate","Name",CCTypeID); 

        }

        public void DoGetQueryString(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            string strID;
            Session["flagPC"] = txtPC.Text;
            Session["flagPR"] = txtPR.Text;


            try
            {
                if (e.CommandName == "DoGetPayment")
                {
                    strID = ((Label)(e.Item.FindControl("lblPayTypeID"))).Text.ToString();

                    cmbPayType.SelectedValue = strID.ToString();
                    //cmbRep.SelectedIndex=0;
                    //cmbCourt.SelectedIndex=0;
                    Session["RepID"] = cmbRep.SelectedIndex;
                    Session["PaymentID"] = cmbPayType.SelectedIndex;
                    Session["CourtID"] = cmbCourt.SelectedIndex;

                    GetPaymentDetailReport(Convert.ToDateTime(calQueryDatenewdt.Text));
                }

                else if (e.CommandName == "DoGetRep")
                {
                    strID = ((Label)(e.Item.FindControl("lblEmployeeID"))).Text.ToString();

                    //cmbCourt.SelectedIndex=0;
                    cmbRep.SelectedValue = strID.ToString();
                    Session["RepID"] = cmbRep.SelectedIndex;
                    Session["PaymentID"] = cmbPayType.SelectedIndex;
                    Session["CourtID"] = cmbCourt.SelectedIndex;

                    GetPaymentDetailReport(Convert.ToDateTime(calQueryDatenewdt.Text));
                }

                else if (e.CommandName == "DoGetCustomer")
                {
                    strID = ((Label)(e.Item.FindControl("lblTicketID"))).Text.ToString();
                    //strID = "../../ClientInfo/violationsfees.asp?caseNumber=" + strID;
                    strID = "../ClientInfo/violationfeeold.aspx?search=0&caseNumber=" + strID;
                    Response.Redirect(strID, false);
                }

                else if (e.CommandName == "DoGetCourt")
                {
                    strID = ((Label)(e.Item.FindControl("lblCourtID"))).Text.ToString();

                    cmbCourt.SelectedValue = strID.ToString();
                    //cmbPayType.SelectedIndex=0;  
                    //cmbRep.SelectedIndex=0;
                    Session["RepID"] = cmbRep.SelectedIndex;
                    Session["PaymentID"] = cmbPayType.SelectedIndex;
                    Session["CourtID"] = cmbCourt.SelectedIndex;

                    //GetPaymentDetailReportByCourtID(calQueryDatenewdt.Text);
                    GetPaymentDetailReport(Convert.ToDateTime(calQueryDatenewdt.Text));
                }


            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            //this.calQueryDatenewdt.PageIndexChanged += new HTP.WebControls.PageMethodHandler(this.date_changed);
            this.lnkbtnPrint.Click += new System.EventHandler(this.lnkbtnPrint_Click);
            this.dgrdPayType.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgrdPayType_ItemDataBound);
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            this.dgrdCourt.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgrdCourt_ItemDataBound);
            this.cmbRep.SelectedIndexChanged += new System.EventHandler(this.cmbRep_SelectedIndexChanged);
            this.cmbPayType.SelectedIndexChanged += new System.EventHandler(this.cmbPayType_SelectedIndexChanged);
            this.cmbCourt.SelectedIndexChanged += new System.EventHandler(this.cmbCourt_SelectedIndexChanged);
            this.dgrdPayDetail.SortCommand += new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.dgrdPayDetail_SortCommand);
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion

        private void btnUpdate_Click(object sender, System.EventArgs e)
        {
            Session["flagPC"] = txtPC.Text;
            Session["flagPR"] = txtPR.Text;

            SaveReport();

            //GetPaymentSummaryByDate(calQueryDatenewdt.Text);  
            GetPaymentTypeSumByDate();
            GetCourtSummary();
            GetPaymentDetailByDateByRep(Convert.ToDateTime(calQueryDatenewdt.Text));
            GetPaymentDetailReport(Convert.ToDateTime(calQueryDatenewdt.Text));
        }

        private bool SaveReport()
        {
            //Waqas 5110 01/22/2009
            try
            {
                //Waqas 5110 01/20/2009
                //objEnationFramework.DeleteBySPByOneParmameter("usp_Del_tblPaymentDetailWeeklyByDate", "TransDate", calQueryDatenewdt.Text);
                objFinancialReps.DeleteWeeklyPaymentDetailsByDate(Convert.ToDateTime(calQueryDatenewdt.Text),Convert.ToInt32(ddl_Branch.SelectedValue));
                foreach (DataGridItem ItemX in dgrdPayByRep.Items)
                {
                    Decimal dCheck = 0;
                    Decimal dCash = 0;
                    string TempValue = "";

                    string EmployeeID = ((Label)(ItemX.FindControl("lblEmployeeID"))).Text.ToString();
                    TempValue = ((TextBox)(ItemX.FindControl("txtActualCash"))).Text;
                    if (TempValue != "")
                    {
                        dCash = Convert.ToDecimal(TempValue);
                    }

                    TempValue = ((TextBox)(ItemX.FindControl("txtActualCheck"))).Text;
                    if (TempValue != "")
                    {
                        dCheck = Convert.ToDecimal(TempValue);
                    }

                    if ((dCash + dCheck) != 0)
                    {
                        //Waqas 5110 01/26/2009
                        //objEnationFramework.InsertBySP("usp_Add_tblPaymentDetailWeekly", "EmployeeID", Convert.ToInt32(EmployeeID), "TransDate", calQueryDatenewdt.Text, "ActualCash", dCash, "ActualCheck", dCheck, "Notes", txtRemarks.Text.ToString());
                        // Sabir Khan 10920 05/27/2013 Branch Id added.
                        objFinancialReps.InsertWeeklyPaymentDetails(Convert.ToInt32(EmployeeID), Convert.ToDateTime(calQueryDatenewdt.Text), dCash, dCheck, txtRemarks.Text.ToString(),Convert.ToInt32(ddl_Branch.SelectedValue));
                    }
                }

                // Add Notes additionally
                //Waqas 5110 01/26/2009
                //objEnationFramework.InsertBySP("usp_Add_tblPaymentDetailWeekly", "EmployeeID", 0, "TransDate", calQueryDatenewdt.Text, "ActualCash", 0, "ActualCheck", 0, "Notes", txtRemarks.Text.ToString());
                // Sabir Khan 10920 05/27/2013 Branch Id added.
                objFinancialReps.InsertWeeklyPaymentDetails(0, Convert.ToDateTime(calQueryDatenewdt.Text), 0, 0, txtRemarks.Text.ToString(), Convert.ToInt32(ddl_Branch.SelectedValue));

                return true;
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return false;
            }

        }

        private void dgrdPayDetail_ItemCreated(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            //			ListItemType lit = e.Item.ItemType; 
            //			if(ListItemType.Header == lit) 
            //			{ 
            //				//*** Redirect the default header rendering method to our own method 
            //				//e.Item.SetRenderMethodDelegate(new RenderMethod(NewRenderMethod)); 				
            //				//clsENationWebComponents obj  = new clsENationWebComponents();
            //				
            //				//obj.FetchValuesInWebControlBysp( e.Item.FindControl("cmbRep"),"usp_Get_All_RepList","RepName","RepID");
            //			} 
        }




        private void lnkbtnPrint_Click(object sender, System.EventArgs e)
        {
            //Response.Redirect(Session["DocID"].ToString()) ;
            //Response.Redirect("PreviewMain.aspx");
            Session["RepID"] = cmbRep.SelectedIndex;
            Session["PaymentID"] = cmbPayType.SelectedIndex;
            Session["CourtID"] = cmbCourt.SelectedIndex;
            Session["ReportDate"] = calQueryDatenewdt.Text;
            Response.Write("<script>window.open ('CloseOutReportPreview.aspx?b=" + Convert.ToInt32(ddl_Branch.SelectedValue).ToString() + "&p=" + Convert.ToInt32(cmbPayType.SelectedValue).ToString() + "','_blank');</script>");
            //Response.Redirect("CloseOutReportPreview.aspx?b=" + Convert.ToInt32(ddl_Branch.SelectedValue).ToString() + "&p=" + Convert.ToInt32(cmbPayType.SelectedValue).ToString(), false);

        }

        private void cmbRep_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            //cmbCourt.SelectedIndex=0;
            Session["RepID"] = cmbRep.SelectedIndex;
            Session["PaymentID"] = cmbPayType.SelectedIndex;
            Session["CourtID"] = cmbCourt.SelectedIndex;
            GetPaymentDetailReport(Convert.ToDateTime(calQueryDatenewdt.Text));
        }

        private void cmbPayType_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            //cmbCourt.SelectedIndex=0;

            Session["RepID"] = cmbRep.SelectedIndex;
            Session["PaymentID"] = cmbPayType.SelectedIndex;
            Session["CourtID"] = cmbCourt.SelectedIndex;
            GetPaymentDetailReport(Convert.ToDateTime(calQueryDatenewdt.Text));
        }

        //		
        //		private bool GenratePDF()
        //		{
        //			string strFileName;
        //			
        //			pdfile = Server.MapPath("../Temp/");
        //			pdfile += Session.SessionID + ".pdf";
        //			Session["DocID"]= pdfile;
        //
        //			Document document = new Document(PageSize.A4,5,5,12,5);			
        //			
        //			//MemoryStream m = new MemoryStream(); 			
        //			//PdfWriter.getInstance(document, new FileStream(pdfile, FileMode.Create));
        //			//PdfWriter.getInstance (document,m);
        //			PdfWriter writer = PdfWriter.getInstance(document, new FileStream(pdfile, FileMode.Create));
        //			document.Open();
        //			
        //			PdfContentByte cb = writer.DirectContent;
        //			
        //
        //			strFileName = Server.MapPath("../Images/");
        //			strFileName += "ln_logo2.jpg";
        //
        //			iTextSharp.text.Image Pic= iTextSharp.text.Image.getInstance(strFileName);
        //            Pic.scalePercent(40,40);
        //            document.Add(Pic);
        //
        //			document.Add(new Paragraph("Date " + calQueryDatenewdt.Text.ToShortDateString()  ));
        //			
        //			//Font font0 = FontFactory.getFont(BaseFont.HELVETICA, BaseFont.WINANSI, 8.5);
        //			BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
        //			iTextSharp.text.Font font = new iTextSharp.text.Font(bf, 8, iTextSharp.text.Font.NORMAL);
        //
        //			cb.beginText();
        //			//cb.setFontAndSize(bf, 4);
        //			//cb.showTextAligned(PdfContentByte.ALIGN_CENTER, text + "This text is centered", 250, 700, 0);
        //			
        //
        //
        //
        //			iTextSharp.text.Table tableMain = new iTextSharp.text.Table(2);
        //			//tableMain.
        //			iTextSharp.text.Table tableLeft = new iTextSharp.text.Table(3);
        //			iTextSharp.text.Table tableRight = new iTextSharp.text.Table(7);
        //			iTextSharp.text.Table tableReport = new iTextSharp.text.Table(7);
        //
        //
        //			tableMain.WidthPercentage = 100;			
        //			//tableLeft.WidthPercentage = 40;
        //			tableRight.WidthPercentage = 100;
        //			tableReport.WidthPercentage= 100;
        //
        //			tableLeft.DefaultVerticalAlignment= Element.ALIGN_MIDDLE;
        //
        //			Cell cell01 = new Cell("CC Type");			
        //			//cell1.HorizontalAlignment=Element.ALIGN_CENTER;
        //			cell01.Width = Convert.ToString(300);
        //			
        //			tableLeft.addCell(cell01);
        //
        //			Cell cell02 = new Cell("Count");			
        //			cell02.HorizontalAlignment=Element.ALIGN_CENTER;
        //			cell02.Width = Convert.ToString(100);
        //			tableLeft.addCell(cell02);
        //
        //			Cell cell03 = new Cell("Amount");			
        //			cell03.HorizontalAlignment=Element.ALIGN_CENTER;
        //			cell03.Width = Convert.ToString(100);
        //			tableLeft.addCell(cell03);
        //							
        //			foreach (DataGridItem ItemX in dgrdPayType.Items) 
        //			{ 	
        //				string strText;
        //				
        //				strText = ((Label)(ItemX.FindControl("lblPayType"))).Text;
        //				tableLeft.addCell(strText);
        //
        //				strText = ((Label)(ItemX.FindControl("lblCount"))).Text;
        //				tableLeft.addCell(strText);
        //
        //				strText = ((Label)(ItemX.FindControl("lblAmount"))).Text;
        //				tableLeft.addCell(strText);
        //
        //			}
        //
        //
        //			tableRight.DefaultVerticalAlignment= Element.ALIGN_MIDDLE;
        //
        //			tableRight.addCell("Rep");
        //			Cell cell1 = new Cell("Cash");
        //			cell1.Colspan=2;
        //			cell1.HorizontalAlignment=Element.ALIGN_CENTER;
        //			tableRight.addCell(cell1);
        //
        //			Cell cell2 = new Cell("Check");
        //			cell2.Colspan=2;
        //			cell2.HorizontalAlignment=Element.ALIGN_CENTER;
        //			tableRight.addCell(cell2);
        //
        //			Cell cell3 = new Cell("Total");
        //			cell3.Colspan=2;
        //			cell3.HorizontalAlignment=Element.ALIGN_CENTER;
        //			tableRight.addCell(cell3);
        //
        //
        //
        //			foreach (DataGridItem ItemX in dgrdPayByRep.Items) 
        //			{ 	
        //				string strText;
        //				
        //				strText = ((Label)(ItemX.FindControl("lblEmployee"))).Text;
        //				tableRight.addCell(strText);
        //
        //				strText = ((Label)(ItemX.FindControl("lblSystemCash"))).Text;
        //				tableRight.addCell(strText);
        //				strText = ((TextBox)(ItemX.FindControl("txtActualCash"))).Text;
        //				tableRight.addCell(strText);
        //
        //				strText = ((Label)(ItemX.FindControl("lblSystemCheck"))).Text;
        //				tableRight.addCell(strText);
        //				strText = ((TextBox)(ItemX.FindControl("txtActualCheck"))).Text;
        //				tableRight.addCell(strText);
        //
        //				strText = ((Label)(ItemX.FindControl("lblSystemTotal"))).Text;
        //				tableRight.addCell(strText);
        //				strText = ((Label)(ItemX.FindControl("lblActualTotal"))).Text;
        //				tableRight.addCell(strText);
        //			}
        //
        //
        //
        //
        //
        //			tableReport.DefaultVerticalAlignment= Element.ALIGN_MIDDLE;
        //			
        //			Cell cellFR1 = new Cell("No.");			
        //			cellFR1.HorizontalAlignment=Element.ALIGN_CENTER;
        //			tableReport.addCell(cellFR1);
        //
        //			Cell cellFR2 = new Cell("Date");			
        //			cellFR2.HorizontalAlignment=Element.ALIGN_CENTER;
        //			tableReport.addCell(cellFR2);
        //
        //			Cell cellFR3 = new Cell("Client Name");						
        //			tableReport.addCell(cellFR3);
        //
        //			Cell cellFR4 = new Cell("Rep Name");						
        //			tableReport.addCell(cellFR4);
        //
        //			Cell cellFR5 = new Cell("Paid Amount");						
        //			tableReport.addCell(cellFR5);
        //
        //			Cell cellFR6 = new Cell("Payment Type");						
        //			tableReport.addCell(cellFR6);
        //
        //			Cell cellFR7 = new Cell("Court");						
        //			tableReport.addCell(cellFR7);
        //
        //
        //
        //			foreach (DataGridItem ItemX in dgrdPayDetail.Items) 
        //			{ 	
        //				string strText;
        //				
        //				strText = ((Label)(ItemX.FindControl("lblNo"))).Text;
        //				tableReport.addCell(strText);
        //
        //				strText = ((Label)(ItemX.FindControl("lblDate"))).Text;
        //				tableReport.addCell(strText);
        //
        //				strText = ((Label)(ItemX.FindControl("lblCustomer"))).Text;
        //				tableReport.addCell(strText);
        //
        //				strText = ((Label)(ItemX.FindControl("lblRep"))).Text;
        //				tableReport.addCell(strText);
        //
        //				strText = ((Label)(ItemX.FindControl("lblPaidAmount"))).Text;
        //				tableReport.addCell(strText);
        //
        //				strText = ((Label)(ItemX.FindControl("lblPayTypeFR"))).Text;
        //				tableReport.addCell(strText);
        //
        //				strText = ((Label)(ItemX.FindControl("lblCourt"))).Text;
        //				tableReport.addCell(strText);
        //			}
        //
        //
        //			Cell tableMainCell0 = new Cell(tableLeft);
        //			Cell tableMainCell1 = new Cell(tableRight);
        //			//Cell tableMainCell2 = new Cell(tableReport);
        //			//tableMainCell2.Colspan=2;
        //
        //			cb.endText(); 
        //
        //			tableMain.addCell(tableMainCell0, new Point(0,0));
        //			tableMain.addCell(tableMainCell1, new Point(0,1));			
        //			//tableMain.addCell(tableMainCell2, new Point(1,0));
        //
        //
        //			document.Add(tableMain);
        //			document.Add(tableReport);
        //
        //			document.Close();
        //
        //			return true;
        //
        //
        //
        //		}
        //

        private void btnSubmit_Click(object sender, System.EventArgs e)
        {

            //			GenerateMail();

        }
        //string strMessage
        private bool GenerateMail()
        {
            try
            {
                Get_EmailSetting();

                StringBuilder sbHTMLMailText = new StringBuilder();
                System.Net.Mail.MailMessage MyMail = new MailMessage();
                System.Net.Mail.SmtpClient mailClient = new SmtpClient();
                MyMail.From = new MailAddress(strMailFrom);
                MyMail.To.Add(new MailAddress(txtEmail.Text));
                MyMail.Subject = "Payment Detail Report " + calQueryDatenewdt.Text;
                MyMail.IsBodyHtml = true;

                #region Generate Email Body

                //Begin
                sbHTMLMailText.Append("<HTML><HEAD><TITLE>Payment Detail Report</TITLE>");
                sbHTMLMailText.Append("<style  type=text/css >");
                sbHTMLMailText.Append("BODY{FONT: 10pt Tahoma} .TextArea{BORDER-RIGHT: #000000 1px solid; BORDER-TOP: #000000 1px solid; FONT-SIZE: 9pt; BORDER-LEFT: #000000 1px solid; BORDER-BOTTOM: #000000 1px solid}");
                sbHTMLMailText.Append(".TextBox{FONT-SIZE: 8pt; FONT-FAMILY: Verdana; BACKGROUND-COLOR: #ffffff} .ProductHead{BORDER-RIGHT: 0px; BORDER-TOP: 0px; FONT: bold 8.5pt Tahoma; BORDER-LEFT: 0px; COLOR: white; BORDER-BOTTOM: 0px; BACKGROUND-COLOR: #006699; TEXT-DECORATION: none}");
                sbHTMLMailText.Append(".Label{BORDER-RIGHT: 0px; BORDER-TOP: 0px; FONT-WEIGHT: normal; FONT-SIZE: 8.5pt; BORDER-LEFT: 0px; COLOR: #000000; BORDER-BOTTOM: 0px; FONT-STYLE: normal; FONT-FAMILY: Verdana, Arial, Helvetica, sans}");
                sbHTMLMailText.Append(".DefaultPage{FONT: bold 7pt Verdana, Arial, Helvetica, sans-serif; COLOR: #000000; TEXT-DECORATION: none}");
                sbHTMLMailText.Append(".HeaderLink{FONT: bold 7pt Verdana, Arial, Helvetica, sans-serif; COLOR: #ffffff; TEXT-DECORATION: none} TD{FONT: 10pt Tahoma}");
                sbHTMLMailText.Append(".FrmTD{FONT-WEIGHT: bold; FONT-SIZE: 8pt; COLOR: #006699; FONT-FAMILY: Verdana, Arial, Helvetica, sans} .TopHeading{FONT-WEIGHT: bold; FONT-SIZE: medium; COLOR: #006699}");
                sbHTMLMailText.Append(".TDHeading{FONT: bold 10pt Tahoma; COLOR: white; BACKGROUND-COLOR: #006699; TEXT-DECORATION: none}");
                sbHTMLMailText.Append(".GrdHeader{FONT-WEIGHT: bold; FONT-SIZE: 8.5pt; COLOR: #006699; FONT-FAMILY: Verdana,Arial, Helvetica, sans-serif}");
                sbHTMLMailText.Append(".ProductGrdLinks{FONT-WEIGHT: bold; FONT-SIZE: 8.5pt; FONT-FAMILY: Verdana,Arial, Helvetica, sans-serif; TEXT-DECORATION: none}");
                sbHTMLMailText.Append(".GrdLbl{FONT-SIZE: 8.5pt; FONT-FAMILY: Verdana,Arial, Helvetica, sans-serif}");
                sbHTMLMailText.Append(".TextBoxReadOnly{Background-Color: #ffffff;Font-Size: 8pt;font-family:Verdana;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none}");

                sbHTMLMailText.Append("</style></HEAD>");
                sbHTMLMailText.Append("<BODY MS_POSITIONING='GridLayout'><TABLE id=Table1 cellSpacing=0 cellPadding=0 width='780' align='center' border=0>");
                sbHTMLMailText.Append("<TBODY> <TR> <TD colSpan=4><IMG src='http://www.legalhouston.com/images/ln_logo.gif'>&nbsp;");
                sbHTMLMailText.Append("<TABLE id=Table5 cellSpacing=0 cellPadding=0 width='100%' border=0> <TBODY>");
                sbHTMLMailText.Append("<TR> <TD class=TopHeading style='HEIGHT: 13px' align=middle colSpan=4 height=13>PAYMENT DETAIL REPORT</TD></TR>");
                sbHTMLMailText.Append("<TR><TD class=TopHeading style='HEIGHT: 13px' align=middle colSpan=4 height=13>For <SPAN class=TopHeading id=lblDate>");
                // Report Date				
                sbHTMLMailText.Append(calQueryDatenewdt.Text);

                sbHTMLMailText.Append("</SPAN></TD></TR>");
                sbHTMLMailText.Append("<TR><TD class=frmtd style='HEIGHT: 12px' width=20 height=5></TD><TD class=frmtd style='HEIGHT: 5px' width=100 height=5></TD>");
                sbHTMLMailText.Append("<TD style='DISPLAY: none; HEIGHT: 5px' height=5></TD><TD style='HEIGHT: 5px' vAlign=bottom align=right height=5></TD></TR></TBODY></TABLE></TD></TR>");
                sbHTMLMailText.Append("<TR> <TD class=TDHeading colSpan=5> <DIV id=HA2>&nbsp;&nbsp;&nbsp;<label class=ProductHead>Payment Type Summary</label></DIV></TD></TR>");
                sbHTMLMailText.Append("<TR> <TD height=7></TD> <TD height=7></TD> <TD height=7></TD> <TD height=7></TD> <TD height=7></TD></TR>");
                sbHTMLMailText.Append("<TR> <TD width='100%' colSpan=5> <DIV id=HA3>");
                sbHTMLMailText.Append("<TABLE id=Table6 cellSpacing=0 cellPadding=0 width='100%' border=0> <TBODY> <TR> <TD vAlign=top align=left>");
                sbHTMLMailText.Append("<TABLE id=Table2 cellSpacing=0 cellPadding=0 width='100%' border=0> <TBODY> <TR> <TD colSpan=5>");
                sbHTMLMailText.Append("<TABLE id=dgrdPayType WIDTH='780' style='FONT-SIZE: 2px; FONT-FAMILY: Verdana; BORDER-COLLAPSE: collapse' cellSpacing=0 rules=all border=1>");
                sbHTMLMailText.Append("<TBODY><TR class=GrdHeader vAlign=center align=middle><TD class=GrdHeader align=left>Payment Method</TD><TD class=GrdHeader align=middle>Count</TD><TD class=GrdHeader align=middle>Amount</TD></TR>");
                // Payment Types

                bool bAlternate = false;
                foreach (DataGridItem ItemX in dgrdPayType.Items)
                {
                    string strTemp = "";



                    if (bAlternate == true) // if Aternate Row
                    {
                        sbHTMLMailText.Append("<TR style='BACKGROUND-COLOR: #eeeeee'>");
                    }
                    else
                    {
                        sbHTMLMailText.Append("<TR>");
                    }

                    sbHTMLMailText.Append("<TD>");
                    strTemp = ((Label)(ItemX.FindControl("lblPayType"))).Text.ToString();

                    if (strTemp == "Credit Card")
                        sbHTMLMailText.Append("<IMG src='http://www.legalhouston.com/images/folderopen.gif'>&nbsp;");

                    if ((strTemp == "Visa") || (strTemp == "Master Card") || (strTemp == "American Express"))
                    {
                        sbHTMLMailText.Append("<IMG src='http://www.legalhouston.com/images/T.gif'>&nbsp;");
                        sbHTMLMailText.Append("<IMG src='http://www.legalhouston.com/images/folder.gif'>&nbsp;");
                    }

                    if (strTemp == "Discover")
                    {
                        sbHTMLMailText.Append("<IMG src='http://www.legalhouston.com/images/L.gif'>&nbsp;");
                        sbHTMLMailText.Append("<IMG src='http://www.legalhouston.com/images/folder.gif'>&nbsp;");
                    }


                    sbHTMLMailText.Append("<SPAN class=ProductGrdLinks >");

                    sbHTMLMailText.Append(strTemp.ToString());
                    sbHTMLMailText.Append("</SPAN> </TD> <TD align=middle><SPAN class=GrdLbl >");
                    strTemp = ((Label)(ItemX.FindControl("lblCount"))).Text.ToString();
                    sbHTMLMailText.Append(strTemp.ToString());
                    sbHTMLMailText.Append("</SPAN> </TD> <TD align=right><SPAN class=GrdLbl >");
                    strTemp = ((Label)(ItemX.FindControl("lblAmount"))).Text.ToString();
                    sbHTMLMailText.Append(strTemp.ToString());
                    sbHTMLMailText.Append("&nbsp;&nbsp;&nbsp;</SPAN> </TD></TR>");

                    bAlternate = !bAlternate;
                }

                // Total Payment Type
                TotalCount = Convert.ToInt32(ViewState["TotalPM"]);
                TotalAmount = Convert.ToDouble(ViewState["TotalPMAmount"]);

                sbHTMLMailText.Append("<TR bgcolor='#CCCCCC' ><TD class=frmtd>Total</TD><TD align=center class=GrdLbl>");
                sbHTMLMailText.Append(TotalCount);
                sbHTMLMailText.Append("</TD><TD align=right class=GrdLbl>");
                sbHTMLMailText.Append(string.Format("{0:c}", TotalAmount));
                sbHTMLMailText.Append("&nbsp;&nbsp;&nbsp;</TD></TR>");

                // End Payment Types
                sbHTMLMailText.Append("</TBODY></TABLE></TD></TR>");
                sbHTMLMailText.Append("<TR><TD height=7 class=frmtd vAlign=top colSpan=5></TD></TR>");
                //sbHTMLMailText.Append("<TR> <TD style='HEIGHT: 2px' vAlign=top colSpan=5><TEXTAREA class=TextArea id=txtRemarks title=Notes style='WIDTH: 100%; HEIGHT: 88px' name=txtRemarks readOnly>");
                //sbHTMLMailText.Append("<TR> <TD style='HEIGHT: 2px' vAlign=top colSpan=5>");

                // Notes
                //sbHTMLMailText.Append(txtRemarks.Text.ToString() );
                //sbHTMLMailText.Append("</TEXTAREA></TD></TR></TBODY></TABLE></TD>");

                sbHTMLMailText.Append("</TD></TR> <TR> <TD style='HEIGHT: 2px' vAlign=top colSpan=5>");
                //**********************
                sbHTMLMailText.Append("<TABLE id=Table3 style='WIDTH: 100%' cellSpacing=0 cellPadding=0 width='100%' border=0> <TBODY> ");
                sbHTMLMailText.Append("<TR> <TD class=TDHeading colSpan=5>  <DIV id=HAA2>&nbsp;&nbsp;&nbsp;	  <label class=ProductHead>Representative Summary</label> </DIV></TD></TR>");
                sbHTMLMailText.Append("<TR> <TD colSpan=5 height=7> </TD> </TR>");

                sbHTMLMailText.Append("<TR> <TD class=frmtd style='BORDER-TOP: 1px solid; BORDER-LEFT: 1px solid' vAlign=top align=left width='18%' bgColor=#eeeeee colSpan=2 height=18>Rep</TD>");

                sbHTMLMailText.Append("<TD class=frmtd style='BORDER-TOP: 1px solid' align=middle width='25%' bgColor=#eeeeee height=18>Cash</TD>");
                sbHTMLMailText.Append("<TD class=frmtd style='BORDER-TOP: 1px solid' align=middle width='25%' bgColor=#eeeeee height=18>Check</TD>");
                sbHTMLMailText.Append("<TD class=frmtd style='BORDER-RIGHT: gray 1px solid; BORDER-TOP: 1px solid' align=middle width='25%' bgColor=#eeeeee height=18>Total</TD></TR>");
                sbHTMLMailText.Append("<TR> <TD width='100%' colSpan=6>  <TABLE id=dgrdPayByRep style='FONT-SIZE: 2px; WIDTH: 100%; FONT-FAMILY: Verdana; BORDER-COLLAPSE: collapse' cellSpacing=0 rules=all border=1>");
                sbHTMLMailText.Append("<TBODY> <TR class=GrdHeader vAlign=center align=middle> <TD class=GrdHeader style='WIDTH: 20%' align=left>&nbsp;</TD>");
                sbHTMLMailText.Append("<TD class=GrdHeader align=middle>System</TD><TD class=GrdHeader align=middle>Actual</TD> ");
                sbHTMLMailText.Append("<TD class=GrdHeader align=middle>System</TD> <TD class=GrdHeader align=middle>Actual</TD>");
                sbHTMLMailText.Append("<TD class=GrdHeader align=middle>System</TD> <TD class=GrdHeader align=middle>Actual</TD></TR>");

                // Rep's List with Payment Details		
                bAlternate = false;
                foreach (DataGridItem ItemX in dgrdPayByRep.Items)
                {
                    string strTemp = "";

                    if (ItemX.Visible == true)
                    {
                        if (bAlternate == true) // if Aternate Row
                        {
                            sbHTMLMailText.Append("<TR style='BACKGROUND-COLOR: #eeeeee'>");
                        }
                        else
                        {
                            sbHTMLMailText.Append("<TR>");
                        }

                        sbHTMLMailText.Append("<TD><SPAN class=ProductGrdLinks >");
                        strTemp = ((Label)(ItemX.FindControl("lblEmployee"))).Text.ToString();
                        sbHTMLMailText.Append(strTemp);

                        if (((Label)(ItemX.FindControl("lblSystemCash"))).BackColor == Color.Red)
                        {
                            sbHTMLMailText.Append("</SPAN> </TD> <TD align=middle><SPAN class=GrdLbl style='BACKGROUND-COLOR: red; color: #FFFFFF;' >");
                        }
                        else
                        {
                            sbHTMLMailText.Append("</SPAN> </TD> <TD align=middle><SPAN class=GrdLbl >");
                        }

                        strTemp = ((Label)(ItemX.FindControl("lblSystemCash"))).Text.ToString();
                        sbHTMLMailText.Append(strTemp);
                        //sbHTMLMailText.Append("</SPAN> </TD>  <TD align=middle><INPUT class='TextBox' style='WIDTH: 50px' readOnly maxLength=10 value=");
                        sbHTMLMailText.Append("</SPAN></TD>  <TD align=middle><SPAN class=GrdLbl > ");

                        strTemp = ((TextBox)(ItemX.FindControl("txtActualCash"))).Text;
                        sbHTMLMailText.Append(strTemp);

                        if (((Label)(ItemX.FindControl("lblSystemCheck"))).BackColor == Color.Red)
                        {
                            sbHTMLMailText.Append("</SPAN></TD>  <TD align=middle><SPAN class='GrdLbl' style='BACKGROUND-COLOR: red; color: #FFFFFF;' >");
                        }
                        else
                        {
                            sbHTMLMailText.Append("</SPAN></TD>  <TD align=middle><SPAN class='GrdLbl' >");
                        }
                        strTemp = ((Label)(ItemX.FindControl("lblSystemCheck"))).Text.ToString();
                        sbHTMLMailText.Append(strTemp);
                        //sbHTMLMailText.Append("</SPAN></TD>  <TD align=middle><INPUT class='TextBox'  style='WIDTH: 50px' readOnly maxLength=10 value=");
                        sbHTMLMailText.Append("</SPAN></TD>  <TD align=middle> <SPAN class=GrdLbl >");

                        strTemp = ((TextBox)(ItemX.FindControl("txtActualCheck"))).Text;
                        sbHTMLMailText.Append(strTemp);

                        sbHTMLMailText.Append("</SPAN> </TD>  <TD align=middle><SPAN class=GrdLbl >");

                        strTemp = ((Label)(ItemX.FindControl("lblSystemTotal"))).Text.ToString();
                        sbHTMLMailText.Append(strTemp);
                        sbHTMLMailText.Append("</SPAN> </TD>  <TD align=middle><SPAN class=GrdLbl >");
                        strTemp = ((Label)(ItemX.FindControl("lblActualTotal"))).Text.ToString();
                        sbHTMLMailText.Append(strTemp);
                        sbHTMLMailText.Append("</SPAN> </TD></TR>");

                        //bAlternate = !bAlternate;
                    }
                }




                // Total Row
                sbHTMLMailText.Append("</TBODY></TABLE></TD></TR>");
                sbHTMLMailText.Append("<TR><TD class=frmtd style='BORDER-RIGHT: gray 1px solid; BORDER-LEFT: 1px solid; BORDER-BOTTOM: black 1px solid' colSpan=5>");
                sbHTMLMailText.Append("<TABLE id=Table4 cellSpacing=0 cellPadding=0 width='100%' border=0><TBODY>");
                sbHTMLMailText.Append("<TR> <TD class=frmtd width='22%' height=25>Total</TD>");

                sbHTMLMailText.Append("<TD width='14%' height=25><SPAN class=Label >");
                sbHTMLMailText.Append(lblTotalCashSys.Text.ToString() + "</SPAN></TD>");
                sbHTMLMailText.Append("<TD width='13%' height=25><SPAN class=Label id=lblTotalCashActual>");
                sbHTMLMailText.Append(lblTotalCashActual.Text.ToString() + "</SPAN></TD>");

                sbHTMLMailText.Append("<TD width='15%' height=25><SPAN class=Label id=lblTotalCheckSys>");
                sbHTMLMailText.Append(lblTotalCheckSys.Text.ToString() + "</SPAN></TD>");
                sbHTMLMailText.Append("<TD width='12%' height=25><SPAN class=Label id=lblTotalCheckActual>");
                sbHTMLMailText.Append(lblTotalCheckActual.Text.ToString() + "</SPAN></TD>");

                sbHTMLMailText.Append("<TD width='13%'><SPAN class=Label id=lblTotalSystem>");
                sbHTMLMailText.Append(lblTotalSystem.Text.ToString() + "</SPAN></TD>");
                sbHTMLMailText.Append("<TD><SPAN class=Label id=lblTotalActual DESIGNTIMEDRAGDROP='4631'>");
                sbHTMLMailText.Append(lblTotalActual.Text.ToString() + "</SPAN></TD>");


                sbHTMLMailText.Append("</TR></TBODY></TABLE></TD></TR>");
                sbHTMLMailText.Append("<TR> <TD class=frmtd colSpan=5 height=10 ></TD></TR>");
                //sbHTMLMailText.Append("<TR> <TD class=frmtd colSpan=5>&nbsp; </TD></TR>");
                // Notes
                sbHTMLMailText.Append("<TR> <TD class=frmtd colSpan=5>Notes</TD></TR>");
                sbHTMLMailText.Append("<TR> <TD style='HEIGHT: 2px' vAlign=top colSpan=5><TEXTAREA class=TextArea id=txtRemarks title=Notes style='WIDTH: 100%; HEIGHT: 50px' name=txtRemarks readOnly>");


                sbHTMLMailText.Append(txtRemarks.Text.ToString());
                sbHTMLMailText.Append("</TEXTAREA>");
                sbHTMLMailText.Append("</TD></TR></TBODY></TABLE></TD>");
                sbHTMLMailText.Append("</TBODY></TABLE></TD></TR>");
                //******************************



                //sbHTMLMailText.Append("<TD>&nbsp;</TD> <TD vAlign=top align=right width='80%'>");






                sbHTMLMailText.Append("<TR> <TD  height=10 ></TD><TD></TD><TD></TD></TR></TBODY></TABLE></DIV></TD></TR>");


                // Court Summary				
                sbHTMLMailText.Append("<TR> <TD class=TDHeading colSpan=5>  <DIV id=HC2>&nbsp;&nbsp;<label class=ProductHead>Court Summary</label> </DIV></TD></TR>");
                sbHTMLMailText.Append("<TR> <TD colSpan=5 height=7> </TD> </TR>");
                sbHTMLMailText.Append("<TR> <TD colSpan=5 > <TABLE id=dgrdCourt WIDTH='780' style='FONT-SIZE: 2px; FONT-FAMILY: Verdana; BORDER-COLLAPSE: collapse' cellSpacing=0 rules=all border=1>");
                sbHTMLMailText.Append("<TBODY><TR class=GrdHeader vAlign=center align=middle><TD class=GrdHeader align=left>Court Location</TD><TD class=GrdHeader align=middle>Transactions</TD><TD class=GrdHeader align=middle>Fee Amount</TD></TR>");

                bAlternate = false;
                foreach (DataGridItem ItemX in dgrdCourt.Items)
                {
                    string strTemp = "";

                    if (bAlternate == true) // if Aternate Row
                    {
                        sbHTMLMailText.Append("<TR style='BACKGROUND-COLOR: #eeeeee'>");
                    }
                    else
                    {
                        sbHTMLMailText.Append("<TR>");
                    }

                    sbHTMLMailText.Append("<TD>");
                    strTemp = ((Label)(ItemX.FindControl("lblCourtName"))).Text.ToString();

                    sbHTMLMailText.Append("<SPAN class=ProductGrdLinks >");

                    sbHTMLMailText.Append(strTemp.ToString());
                    sbHTMLMailText.Append("</SPAN> </TD> <TD align=center><SPAN class=GrdLbl >");
                    strTemp = ((Label)(ItemX.FindControl("lblTrans"))).Text.ToString();
                    sbHTMLMailText.Append(strTemp.ToString());
                    sbHTMLMailText.Append("</SPAN> </TD> <TD align=right><SPAN class=GrdLbl >");
                    strTemp = ((Label)(ItemX.FindControl("lblFee"))).Text.ToString();
                    sbHTMLMailText.Append(strTemp.ToString());
                    sbHTMLMailText.Append("&nbsp;&nbsp;&nbsp;</SPAN> </TD></TR>");

                    bAlternate = !bAlternate;
                }

                // Total Court
                TotalCourtTrans = Convert.ToInt32(ViewState["TotalCT"]);
                TotalCourtFee = Convert.ToDouble(ViewState["TotalCTAmount"]);


                sbHTMLMailText.Append("<TR bgcolor='#CCCCCC' ><TD class=frmtd>Total</TD><TD align=center class=GrdLbl>");
                sbHTMLMailText.Append(TotalCourtTrans);
                sbHTMLMailText.Append("</TD><TD align=right class=GrdLbl>");
                sbHTMLMailText.Append(string.Format("{0:c}", TotalCourtFee));
                sbHTMLMailText.Append("&nbsp;&nbsp;&nbsp;</TD></TR>");


                //sbHTMLMailText.Append("<TR> <TD colSpan=5 height=10> </TD> </TR>");				
                sbHTMLMailText.Append("</TBODY></TABLE></TD></TR>");
                sbHTMLMailText.Append("<TR> <TD colSpan=5 height=10> </TD> </TR>");




                // Report Section 
                //sbHTMLMailText.Append("<TR> <TD style='WIDTH: 20px'></TD> <TD colSpan=3></TD>    <TD></TD></TR>");
                sbHTMLMailText.Append("<TR> <TD class=TDHeading colSpan=5>  <DIV id=HB2>&nbsp;&nbsp;&nbsp;	  <label class=ProductHead>Transaction Details</label> </DIV></TD></TR>");
                sbHTMLMailText.Append("<TR> <TD colSpan=5 height=7></TD></TR>");
                //sbHTMLMailText.Append("<TR> <TD style='DISPLAY: none' align=middle colSpan=5 height=10>&nbsp; </TD>  </TR>");
                sbHTMLMailText.Append("<TR> <TD colSpan=5> <DIV id=HB3> <TABLE id='tblTransHead' cellSpacing=0 cellPadding=0 width='100%' border=0> ");
                sbHTMLMailText.Append("<TBODY> <TR> <TD style='BORDER-RIGHT: gray 1px solid; BORDER-TOP: 1px solid; BORDER-LEFT: 1px solid; HEIGHT: 3px' align=middle bgColor=#eeeeee colSpan=5 height=25 > <label class='frmtd'> ");
                sbHTMLMailText.Append(cmbRep.SelectedItem.Text.ToString());
                sbHTMLMailText.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                sbHTMLMailText.Append(cmbPayType.SelectedItem.Text.ToString());
                sbHTMLMailText.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                sbHTMLMailText.Append(cmbCourt.SelectedItem.Text.ToString());

                sbHTMLMailText.Append("</label> </TD></TR>  <TR> <TD>");

                sbHTMLMailText.Append("<TABLE id=tblTransDetail WIDTH='780' style='FONT-SIZE: 2px; FONT-FAMILY: Verdana; BORDER-COLLAPSE: collapse' cellSpacing=0 rules=all border=1>");
                sbHTMLMailText.Append("<TBODY> <TR class=GrdHeader vAlign=center align=middle> <TD class=GrdHeader align=middle>No.</TD> <TD class=GrdHeader align=middle>Date</TD> <TD class=GrdHeader align=middle>Time</TD>");
                sbHTMLMailText.Append("<TD class=GrdHeader align=left>Client Name</TD> <TD class=GrdHeader align=left>Rep</TD> <TD class=GrdHeader align=left>Bond</TD> <TD class=GrdHeader align=middle>Paid</TD>");
                sbHTMLMailText.Append("<TD class=GrdHeader align=left>Payment Type </TD><TD class=GrdHeader align=left>Card</TD> <TD class=GrdHeader align=left>Court</TD><TD class=GrdHeader align=left>List</TD></TR>");

                // Report Data

                bAlternate = false;
                foreach (DataGridItem ItemX in dgrdPayDetail.Items)
                {
                    string strTemp = "";

                    if (bAlternate == true) // if Aternate Row
                    {
                        sbHTMLMailText.Append("<TR style='BACKGROUND-COLOR: #eeeeee'>");
                    }
                    else
                    {
                        sbHTMLMailText.Append("<TR>");
                    }

                    sbHTMLMailText.Append("<TD align=middle><SPAN class=GrdLbl >");
                    strTemp = ((Label)(ItemX.FindControl("lblNo"))).Text.ToString();
                    sbHTMLMailText.Append(strTemp);
                    sbHTMLMailText.Append("</SPAN></TD> <TD align=middle><SPAN class=GrdLbl>");
                    strTemp = ((Label)(ItemX.FindControl("lblDate"))).Text.ToString();
                    sbHTMLMailText.Append(strTemp);

                    sbHTMLMailText.Append("</SPAN></TD> <TD Align='Right'><SPAN class=GrdLbl >");
                    strTemp = ((Label)(ItemX.FindControl("lblTime"))).Text.ToString();
                    sbHTMLMailText.Append(strTemp);

                    sbHTMLMailText.Append("</SPAN></TD> <TD><SPAN class=ProductGrdLinks >");
                    strTemp = ((Label)(ItemX.FindControl("lblCustomer"))).Text.ToString();
                    sbHTMLMailText.Append(strTemp);

                    sbHTMLMailText.Append("</SPAN></TD> <TD><SPAN class=GrdLbl >");
                    strTemp = ((Label)(ItemX.FindControl("lblRep"))).Text.ToString();
                    sbHTMLMailText.Append(strTemp);

                    sbHTMLMailText.Append("</SPAN></TD> <TD><SPAN class=GrdLbl >");
                    strTemp = ((Label)(ItemX.FindControl("lblBond"))).Text.ToString();
                    sbHTMLMailText.Append(strTemp);

                    sbHTMLMailText.Append("</SPAN></TD> <TD align=right><SPAN class=GrdLbl >");
                    strTemp = ((Label)(ItemX.FindControl("lblPaidAmount"))).Text.ToString();
                    sbHTMLMailText.Append(strTemp);

                    sbHTMLMailText.Append("</SPAN></TD> <TD><SPAN class=GrdLbl >");
                    strTemp = ((Label)(ItemX.FindControl("lblPayTypeFR"))).Text.ToString();
                    sbHTMLMailText.Append(strTemp);

                    sbHTMLMailText.Append("</SPAN></TD> <TD><SPAN class=GrdLbl >");
                    strTemp = ((Label)(ItemX.FindControl("lblCardType"))).Text.ToString();
                    sbHTMLMailText.Append(strTemp);

                    sbHTMLMailText.Append("</SPAN></TD> <TD><SPAN class=GrdLbl >");
                    strTemp = ((Label)(ItemX.FindControl("lblCourt"))).Text.ToString();
                    sbHTMLMailText.Append(strTemp);

                    sbHTMLMailText.Append("</SPAN></TD> <TD><SPAN class=GrdLbl >");
                    strTemp = ((Label)(ItemX.FindControl("lblListDate"))).Text.ToString();
                    sbHTMLMailText.Append(strTemp);


                    sbHTMLMailText.Append("</SPAN></TD> </TR>  ");



                    bAlternate = !bAlternate;
                }




                // Footer
                sbHTMLMailText.Append("<TR align=middle> <TD colSpan=7><SPAN></SPAN></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></DIV></TD></TR>");

                sbHTMLMailText.Append("<TR> <TD colSpan=5></TD></TR>");
                sbHTMLMailText.Append("<TR> <TD colSpan=5> <HR> </TD></TR>");
                sbHTMLMailText.Append("<TR> <TD colSpan=5></TD></TR></TBODY></TABLE>");
                sbHTMLMailText.Append("</BODY></HTML>");
                // End 

                #endregion

                MyMail.Body = sbHTMLMailText.ToString();
                mailClient.Send(MyMail);

                return true;
            }

            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return false;
            }


        }


        private void lnkbtnEmail_Click(object sender, System.EventArgs e)
        {
            //GenerateMail();
        }

        private void lnkbtnBack_Click(object sender, System.EventArgs e)
        {
            Response.Redirect("../../Activities/ReminderCalls.asp");
        }

        protected void date_changed(object sender, EventArgs e)
        {
            Session["flagPC"] = txtPC.Text;
            Session["flagPR"] = txtPR.Text;

            cmbPayType.SelectedIndex = 0;
            cmbRep.SelectedIndex = 0;
            cmbCourt.SelectedIndex = 0;
            GetReps();
            //GetPaymentSummaryByDate(calQueryDatenewdt.Text);  
            GetPaymentTypeSumByDate();
            GetCourtSummary();
            GetPaymentDetailByDateByRep(Convert.ToDateTime(calQueryDatenewdt.Text));
            GetPaymentDetailReport(Convert.ToDateTime(calQueryDatenewdt.Text));
            GetMailerSummary(Convert.ToDateTime(calQueryDatenewdt.Text));
        }

        private void imgbtnSendMail_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            //			WebClient objWebClient = new WebClient(); 
            //			//string strURL = "http://localhost/lntechNew/PaymentInfo/CloseOutReportPreview.aspx" ; 
            //			string strURL = "http://localhost/lntechNew/PaymentInfo/CloseOutReportPreview.aspx" ; 
            //			byte[] aRequestedHTML; 
            //			aRequestedHTML = objWebClient.DownloadData(strURL); 
            //			UTF8Encoding objUTF8 = new UTF8Encoding(); 
            //			string strRequestedHTML; 
            //			strRequestedHTML = objUTF8.GetString(aRequestedHTML);
            //			lblURL.Text=strRequestedHTML ;

            GenerateMail();
        }

        private void Get_AdminEmail()
        {
            try
            {
                //Change by Ajmal
                IDataReader drEmailSetting;
                drEmailSetting = objEnationFramework.Get_DR_BySP("usp_Get_All_EMailSetting");

                while (drEmailSetting.Read())
                {
                    //txtEmail.Text = drEmailSetting["EMailTo"].ToString() ;					
                }
                drEmailSetting.Close();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void Get_EmailSetting()
        {
            //Waqas 5110 01/22/2009
            try
            {
                //Change by Ajmal
                //SqlDataReader drEmailSetting; 
                
                //Waqas 5110 01/26/2009
                //IDataReader drEmailSetting;
                //drEmailSetting = objEnationFramework.Get_DR_BySP("usp_Get_All_EMailSetting");

                DataSet dsEmailSetting;
                dsEmailSetting = objFinancialReps.GetEmailSetting();

                //while (drEmailSetting.Read())
                //{
                //    strSMTPServer = drEmailSetting["SmtpServer"].ToString();

                //    strMailFrom = drEmailSetting["EMailFrom"].ToString();
                //    //strMailTo= drEmailSetting["EMailTo"].ToString() ;
                //    //txtEmail.Text = drEmailSetting["EMailTo"].ToString() ;

                //    //strMailCC = drEmailSetting["EMailCC"].ToString() ; 
                //    //strMailBCC = drEmailSetting["EMailBCC"].ToString() ; 
                //}
                //drEmailSetting.Close();

                if (dsEmailSetting.Tables.Count > 0)
                {
                    if (dsEmailSetting.Tables[0].Rows.Count > 0)
                    {
                        strSMTPServer = dsEmailSetting.Tables[0].Rows[0]["SmtpServer"].ToString();
                        strMailFrom = dsEmailSetting.Tables[0].Rows[0]["EMailFrom"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void GetPaymentTypeSumByDate()
        {
            try
            {
                //Change by Ajmal
                //SqlDataReader DRPaySum;
                DataSet DRPaySum;
                //Waqas 5110 01/22/2009
                //string[] key1 = { "@RecDate", "@RecTo" };
                //object[] value1 = { calQueryDatenewdt.Text, calQueryDatenewdt.Text };
                //DRPaySum =  objEnationFramework.Get_DR_BySPByOneParmameter("usp_Get_All_PaymentSumByRecDate","RecDate",calQueryDatenewdt.Text);
                //DRPaySum = objEnationFramework.Get_DR_BySPArr("usp_Get_All_PaymentSumByRecDateRange", key1, value1);
                // Sabir Khan 10920 05/27/2013 Branch Id added.
                DRPaySum = objFinancialReps.GetAllPaymentTypeSumByDate(Convert.ToDateTime(calQueryDatenewdt.Text), Convert.ToDateTime(calQueryDatenewdt.Text), Convert.ToInt32(ddl_Branch.SelectedValue));
                dgrdPayType.DataSource = DRPaySum;
                dgrdPayType.DataBind();

                iCount.Text = dgrdPayType.Items.Count.ToString();

                foreach (DataGridItem ItemX in dgrdPayType.Items)
                {
                    string PayType = ((Label)(ItemX.FindControl("lblPayType"))).Text;
                    if ((PayType == "Visa") || (PayType == "Master Card") || (PayType == "American Express") || (PayType == "Discover"))
                        ((Label)(ItemX.FindControl("lblPayTypeID"))).Text = "50" + ((Label)(ItemX.FindControl("lblPayTypeID"))).Text;

                    double dAmount = Convert.ToDouble(((Label)(ItemX.FindControl("lblAmount"))).Text.ToString());
                    ((Label)(ItemX.FindControl("lblAmount"))).Text = String.Format("{0:c}", dAmount);

                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void dgrdPayType_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                string PayType = ((Label)(e.Item.FindControl("lblPayType"))).Text;
                string PayTypeID = ((Label)(e.Item.FindControl("lblPayTypeID"))).Text;
                //Sabir 5755 04/06/2009 Check for Prepaid leagal
                if ((PayType != "Visa") && (PayType != "Master Card") && (PayType != "American Express") && (PayType != "Discover") && (PayTypeID != "2") && (PayTypeID != "4") && (PayTypeID != "3"))
                {
                    TotalCount += Convert.ToInt32(((Label)(e.Item.FindControl("lblCount"))).Text.ToString());
                    TotalAmount += Convert.ToDouble(((Label)(e.Item.FindControl("lblAmount"))).Text.ToString());
                }




                ((System.Web.UI.WebControls.Image)(e.Item.FindControl("imgParent"))).Attributes.Add("onclick", "ShowHideCCDetail('" + ((Label)(e.Item.FindControl("lblPayTypeID"))).Text + "');");

            }
            else if (e.Item.ItemType == ListItemType.Footer)
            {
                ((Label)(e.Item.FindControl("lbl_Count"))).Text = TotalCount.ToString();
                ((Label)(e.Item.FindControl("lbl_Amount"))).Text = (string.Format("{0:c}", TotalAmount) + "&nbsp;&nbsp;&nbsp;");

                ViewState.Add("TotalPM", TotalCount);
                ViewState.Add("TotalPMAmount", TotalAmount);
            }
        }

        private void GetCourtSummary()
        {
            try
            {
                //Change by Ajmal
                //SqlDataReader drCourt;
                DataSet drCourt;
                //Waqas 5110 01/22/2009
                //string[] key1 = { "@RecDate", "@RecTo" };
                //object[] value1 = { calQueryDatenewdt.Text, calQueryDatenewdt.Text };
                //drCourt  = objEnationFramework.Get_DR_BySPByOneParmameter("usp_Get_All_CourtInfoByRecDate","RecDate",calQueryDatenewdt.Text); 
                //drCourt = objEnationFramework.Get_DR_BySPArr("usp_Get_All_CourtInfoByRecDateRange", key1, value1);
                // Sabir Khan 10920 05/27/2013 Branch Id added.
                drCourt = objFinancialReps.GetAllCourtsInfoByDateRange(Convert.ToDateTime(calQueryDatenewdt.Text), Convert.ToDateTime(calQueryDatenewdt.Text),Convert.ToInt32(ddl_Branch.SelectedValue));
                dgrdCourt.DataSource = drCourt;
                dgrdCourt.DataBind();

                //tahir ahmed 3736 05/02/2008
                //for grouping of courts....
                iCourtCount.Text = dgrdCourt.Items.Count.ToString();

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        //Kazim 3736 5/6/2008 Bind Mailer Summary Section

        private void GetMailerSummary(DateTime QueryDate)
        {
            DataSet dsFinReport;
            //int FirmId = 0;
            int PayID = -1;
            int CourtID = 0;

            try
            {
                //Waqas 5110 01/22/2009
                //string[] keys = { "@RecDate", "@RecDateTo", "@EmployeeID", "@PaymentType", "@CourtID" };
                //object[] values = { calQueryDatenewdt.Text, calQueryDatenewdt.Text, 0, PayID, CourtID };
                //dsFinReport = objEnationFramework.Get_DS_BySPArr("usp_Get_All_PaymentDetailByCriteriaRange", keys, values);
                // Sabir Khan 10920 05/27/2013 Branch Id added.
                dsFinReport = objFinancialReps.GetAllMailerSummary(Convert.ToDateTime(calQueryDatenewdt.Text), Convert.ToDateTime(calQueryDatenewdt.Text), 0, PayID, CourtID, Convert.ToInt32(ddl_Branch.SelectedValue));
                //Sabir Khan 6047 06/25/2009 repace table[1] into table[0]...
                dgMailerSummary.DataSource = dsFinReport.Tables[0];
                dgMailerSummary.DataBind();
                //BindMailerSummaryReport();

                iMailerSummaryCount.Text = dgMailerSummary.Items.Count.ToString();

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;

            }

        }

        private void BindMailerSummaryReport()
        {
            int sNo = 0;
            try
            {
                foreach (DataGridItem ItemX in dgMailerSummary.Items)
                {
                    // Serial No.
                    //((LinkButton)(dgItem.FindControl("lblCCTypeID"))).Attributes.Add("Onclick", "LoadDocument();"); 
                    sNo += 1;
                    ((Label)(ItemX.FindControl("lblSNo"))).Text = sNo.ToString();

                }
            }

            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }

        private void dgrdCourt_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (((Label)(e.Item.FindControl("lblTrans"))).Text.ToString() != " " && ((Label)(e.Item.FindControl("lblFee"))).Text.ToString() != " ")
                {
                    if (((Label)(e.Item.FindControl("lblIsCourtCategory"))).Text.ToString() == "0")
                    {

                        TotalCourtTrans += Convert.ToInt32(((Label)(e.Item.FindControl("lblTrans"))).Text.ToString());
                        TotalCourtFee += Convert.ToDouble(((Label)(e.Item.FindControl("lblFee"))).Text.ToString());
                    }

                    double dAmount = Convert.ToDouble(((Label)(e.Item.FindControl("lblFee"))).Text.ToString());
                    ((Label)(e.Item.FindControl("lblFee"))).Text = String.Format("{0:c}", dAmount);
                }


                //TAHIR AHMED 3736 05/02/2008
                //SETTING JAVA SCRIPT FUNCTION FOR THE IMAGE IN COURT GROUP

                Int16 iGroup = Convert.ToInt16(((Label)(e.Item.FindControl("lblIsCourtCategory"))).Text);
                Int16 iCategory = Convert.ToInt16(((Label)(e.Item.FindControl("lblCategoryNumber"))).Text);

                if (iGroup == 1 && (iCategory == 2 || iCategory == 100))
                {
                    System.Web.UI.WebControls.Image iFolderOpen = ((System.Web.UI.WebControls.Image)(e.Item.FindControl("imgParent")));
                    System.Web.UI.WebControls.Image iFolderClose = ((System.Web.UI.WebControls.Image)(e.Item.FindControl("imgChild")));

                    iFolderOpen.Attributes.Add("onclick", "ShowHideCourtDetail('" + iCategory.ToString() + "');");
                    iFolderClose.Attributes.Add("onclick", "ShowHideCourtDetail('" + iCategory.ToString() + "');");
                }

            }
            else if (e.Item.ItemType == ListItemType.Footer)
            {
                Label lCount = (Label)(e.Item.FindControl("lbl_Count"));
                Label lAmount = (Label)(e.Item.FindControl("lbl_Amount"));

                lCount.Text = TotalCourtTrans.ToString();
                lCount.CssClass = "GrdLbl";
                lAmount.Text = (string.Format("{0:c}", TotalCourtFee) + "&nbsp;&nbsp;&nbsp;");
                lAmount.CssClass = "GrdLbl";

            }

            ViewState.Add("TotalCT", TotalCourtTrans);
            ViewState.Add("TotalCTAmount", TotalCourtFee);
        }

        /*
        private void dgrdPayByRep_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //TotalCount += Convert.ToInt32( ((Label)(e.Item.FindControl("lblCount"))).Text.ToString()) ;
                //TotalAmount += Convert.ToDouble ( ((Label)(e.Item.FindControl("lblAmount"))).Text.ToString()) ;
				
                //e.Item.Cells[1].Text = string.Format("{0:c}", Convert.ToDouble(e.Item.Cells[1].Text));
            }
            else if(e.Item.ItemType == ListItemType.Footer)
            {
                e.Item.Cells[0].CssClass="GrdHeader";
                e.Item.Cells[0].Text="Total";
				
                e.Item.Cells[1].CssClass="GrdLbl";
                e.Item.Cells[1].HorizontalAlign= HorizontalAlign.Right;
                e.Item.Cells[1].Text = (string.Format("{0:c}", TotalAmount)+ "&nbsp;");

                e.Item.Cells[2].CssClass="GrdLbl";
                e.Item.Cells[2].HorizontalAlign= HorizontalAlign.Right;
                e.Item.Cells[2].Text = (string.Format("{0:c}", TotalAmount)+ "&nbsp;");

                e.Item.Cells[3].CssClass="GrdLbl";
                e.Item.Cells[3].HorizontalAlign= HorizontalAlign.Right;
                e.Item.Cells[3].Text = (string.Format("{0:c}", TotalAmount)+ "&nbsp;");

                e.Item.Cells[4].CssClass="GrdLbl";
                e.Item.Cells[4].HorizontalAlign= HorizontalAlign.Right;
                e.Item.Cells[4].Text = (string.Format("{0:c}", TotalAmount)+ "&nbsp;");

                e.Item.Cells[5].CssClass="GrdLbl";
                e.Item.Cells[5].HorizontalAlign= HorizontalAlign.Right;
                e.Item.Cells[5].Text = (string.Format("{0:c}", TotalAmount)+ "&nbsp;");

                e.Item.Cells[6].CssClass="GrdLbl";
                e.Item.Cells[6].HorizontalAlign= HorizontalAlign.Right;
                e.Item.Cells[6].Text = (string.Format("{0:c}", TotalAmount)+ "&nbsp;");

				
            } 
		
        }*/

        //Waqas 5110 01/26/2009 Not Used in Houston Traffic
        //private void GetPaymentDetailReportByCourtID(DateTime QueryDate)
        //{
        //    try
        //    {
        //        //Waqas 5110 01/22/2009
        //        DataSet dsFinReport;
        //        //dsFinReport = objEnationFramework.Get_DS_BySPByTwoParmameter("usp_Get_All_PaymentDetailByCourtID", "RecDate", QueryDate, "CourtID", cmbCourt.SelectedValue);

        //        dsFinReport = objFinancialReps.GetPaymentDetailByCourtID(QueryDate, cmbCourt.SelectedValue);
        //        dgrdPayDetail.DataSource = dsFinReport;
        //        dgrdPayDetail.DataBind();

        //        BindPaymentDetailReport();
        //    }
        //    catch (Exception ex)
        //    {
        //        lblMessage.Text = ex.Message;
        //        clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
        //    }


        //}

        private void cmbCourt_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            //cmbPayType.SelectedIndex=0;
            //cmbRep.SelectedIndex=0;
            //GetPaymentDetailReportByCourtID(calQueryDatenewdt.Text);
            Session["RepID"] = cmbRep.SelectedIndex;
            Session["PaymentID"] = cmbPayType.SelectedIndex;
            Session["CourtID"] = cmbCourt.SelectedIndex;
            GetPaymentDetailReport(Convert.ToDateTime(calQueryDatenewdt.Text));

        }

        private void dgrdPayDetail_SortCommand(object source, System.Web.UI.WebControls.DataGridSortCommandEventArgs e)
        {
            SortGrid(e.SortExpression);
        }

        protected void dgMailerSummary_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            // tahir 4171 06/28/2008
            // to provide expand/collapse functionality for each court category.
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                bool IsGroup = Convert.ToBoolean(((Label)(e.Item.FindControl("lbl_IsGroup"))).Text);

                HtmlTable tblSummary = (HtmlTable)(e.Item.FindControl("tblSummary"));
                HtmlTableCell tdCategory = (HtmlTableCell)(e.Item.FindControl("tdCategory"));
                HtmlTableCell tdMailer = (HtmlTableCell)(e.Item.FindControl("tdMailer"));

                System.Web.UI.WebControls.Image imgFolderOpen = ((System.Web.UI.WebControls.Image)(e.Item.FindControl("imgParent")));
                System.Web.UI.WebControls.Image imgFolderClose = ((System.Web.UI.WebControls.Image)(e.Item.FindControl("imgChild")));
                System.Web.UI.WebControls.Image imgLink = ((System.Web.UI.WebControls.Image)(e.Item.FindControl("imgLink")));

                System.Web.UI.WebControls.TextBox txtToggle = ((System.Web.UI.WebControls.TextBox)(e.Item.FindControl("txtToggleImage")));

                // if current row is the court category header row........
                // hide the mailer type and add javascript function call on expand/collapse image
                if (IsGroup)
                {
                    tdCategory.Style["Display"] = "block";
                    tdMailer.Style["Display"] = "none";
                    imgLink.Style["display"] = "none";
                    tdCategory.Width = "60%";
                    tdMailer.Width = "0%";

                    ((Label)(e.Item.FindControl("lblTotalClients"))).Font.Bold = true;
                    ((Label)(e.Item.FindControl("lblTotalRev"))).Font.Bold = true;

                    imgFolderOpen.Attributes.Add("onclick", "ShowHideMailerDetail('" + txtToggle.ClientID + "');");
                    imgFolderClose.Attributes.Add("onclick", "ShowHideMailerDetail('" + txtToggle.ClientID + "');");
                }

                // if current row is the detail of the parent court category ........
                // hide the court category name & expand/collapse iamge. 
                else
                {
                    tdMailer.Style["Display"] = "block";
                    tdCategory.Style["Display"] = "none";
                    imgLink.Style["display"] = "block";
                    tdMailer.Width = "60%";
                    tdCategory.Width = "0%";

                    ((Label)(e.Item.FindControl("lblTotalClients"))).Font.Bold = false;
                    ((Label)(e.Item.FindControl("lblTotalRev"))).Font.Bold = false;
                }

            }
        }
        /// <summary>
        /// Task Id: 10920 
        /// Created By: Sabir Khan
        /// Date: 05/27/2013
        /// Method to get branches name with branches Ids...
        /// </summary>
        private void GetBranchesName()
        {
            DataTable dtBranches = objEnationFramework.Get_DT_BySPArr("USP_HTP_Get_BranchesName");
            if (dtBranches.Rows.Count > 0)
            {
                ddl_Branch.Items.Clear();
                foreach (DataRow dr in dtBranches.Rows)
                {
                    ListItem item = new ListItem(dr["BranchName"].ToString(), dr["Branchid"].ToString());
                    ddl_Branch.Items.Add(item);
                }
                //ddl_Branch.Items.Add(item);
                //ddl_Branch.SelectedValue = "3";
            }
        }

        /*	private void dgrdPayType_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
            {
                if(e.Item.ItemType==ListItemType.Item || e.Item.ItemType==ListItemType.AlternatingItem)
                {
                    sumCount=sumCount+Convert.ToInt64(((Label) e.Item.FindControl("lblCount2")).Text);
                    string amount = ((Label) e.Item.FindControl("lblAmount2")).Text;
                    sumAmount=sumAmount+Convert.ToDouble(amount.Substring(1));

			
                }
                if(e.Item.ItemType==ListItemType.Footer)
                {
                    ((Label) e.Item.FindControl("lbl_SumCount")).Text=sumCount.ToString();
                    ((Label) e.Item.FindControl("lbl_SumAmount")).Text=String.Format("{0:C}",sumAmount);
                }

            }

    */



    }
}

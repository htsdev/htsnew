﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.SessionState;
using FrameWorkEnation.Components;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;


namespace HTP.PaymentInfo
{
    public partial class PaymentReport : System.Web.UI.Page
    {
        clsLogger logger = new clsLogger();
        clsFirms firms = new clsFirms();
        FinancialReports reps = new FinancialReports();
        clsSession ClsSession = new clsSession();
        int totalCount = 0;
        Double totalAmount = 0, totalCommision = 0;
        string StrExp = "";
        string StrAcsDec = "";
        DataView DV;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {

                DataTable dtFirms = firms.GetFirmsForCriminalPaymentReport();
                this.dlFirms.DataSource = dtFirms;
                this.dlFirms.DataBind();

                Session["RepID"] = cmbRep.SelectedIndex;
                Session["PaymentID"] = cmbPayType.SelectedIndex;
                Session["CourtID"] = cmbCourt.SelectedIndex;
                BindControls();
                BindFirmsTransactionDetailReport();
                BindCourtSuymmary();
            }


        }

        protected void dlFirms_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView drv = (DataRowView)e.Item.DataItem;
                int FirmId = Convert.ToInt32(drv["FirmID"]);

                DataTable dtData = reps.GetDataForCriminalPaymentReport(FirmId, Convert.ToDateTime("04/15/2007"), Convert.ToDateTime("5/15/2008"));

                GridView gvData = (GridView)e.Item.FindControl("gvData");
                gvData.DataSource = dtData;
                gvData.DataBind();
            }
        }

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataRowView drv = (DataRowView)e.Row.DataItem;
                    totalCount += Convert.ToInt32(drv["Clients"]);
                    totalAmount += Convert.ToInt32(drv["Amount"]);
                    totalCommision += Convert.ToDouble((totalAmount / 2));
                }
                else if (e.Row.RowType == DataControlRowType.Footer)
                {
                    ((Label)e.Row.FindControl("lblTotalCount")).Text = Convert.ToString(totalCount);
                    ((Label)e.Row.FindControl("lblTotalAmount")).Text = Convert.ToString(totalAmount);
                    ((Label)e.Row.FindControl("lblTotalCommision")).Text = Convert.ToString(totalAmount);
                }
            }
            catch (Exception ex)
            {
                logger.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void BindFirmsTransactionDetailReport()
        {
            int RepID = 0;
            int PayID = 0;
            int CourtID = 0;

            if (cmbRep.SelectedIndex != 0)
            {
                RepID = Convert.ToInt32(cmbRep.SelectedItem.Value );
            }

            PayID = Convert.ToInt32(cmbPayType.SelectedItem.Value );


            if (cmbCourt.SelectedIndex != 0)
            {
                CourtID = Convert.ToInt32(cmbCourt.SelectedValue);
            }

            DataTable dt = firms.GetFirmTransactionDetails(calDateFrom.SelectedDate, calDateTo.SelectedDate, RepID, PayID, CourtID);
            DataView dv = new DataView(dt);
            Session["DS"] = dv;
            gvTransactionDetail.DataSource = dt;
            gvTransactionDetail.DataBind();
        }

        protected void gvTransactionDetail_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                }
            }
            catch (Exception ex)
            {
                logger.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindFirmsTransactionDetailReport();
            BindCourtSuymmary();
        }

        protected void gvTransactionDetail_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string strID;
            if (e.CommandName == "DoGetCustomer")
            {
                strID = Convert.ToString(e.CommandArgument);
                strID = "../ClientInfo/violationfeeold.aspx?search=0&caseNumber=" + strID;
                Response.Redirect(strID, false);
            }
        }

        private void SortGrid(string SortExp)
        {
            try
            {
                SetAcsDesc(SortExp);
                DV = (DataView)Session["DS"];
                DV.Sort = StrExp + " " + StrAcsDec;
                gvTransactionDetail.DataSource = DV;
                gvTransactionDetail.DataBind();
                Session["DS"] = DV;

            }
            catch (Exception ex)
            {
                //lblMessage.Text = ex.Message;
            }
        }

        private void SetAcsDesc(string Val)
        {

            try
            {
                StrExp = Session["StrExp"].ToString();
                StrAcsDec = Session["StrAcsDec"].ToString();
            }
            catch
            {

            }

            if (StrExp == Val)
            {
                if (StrAcsDec == "DESC")
                {
                    StrAcsDec = "ASC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
                else
                {
                    StrAcsDec = "DESC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
            }
            else
            {
                StrExp = Val;
                StrAcsDec = "DESC";
                Session["StrExp"] = StrExp;
                Session["StrAcsDec"] = StrAcsDec;
            }
        }

        protected void gvTransactionDetail_Sorting(object sender, GridViewSortEventArgs e)
        {
            SortGrid(e.SortExpression);
        }

        private void BindCourtSuymmary()
        {
            DataTable dtCourtSummary = firms.GetFirmCourtSumamry(calDateFrom.SelectedDate, calDateTo.SelectedDate, 3041);
            gvCourtSummary.DataSource = dtCourtSummary;
            gvCourtSummary.DataBind();
        }

        protected void gvCourtSummary_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string strID;
            if (e.CommandName == "DoGetCourt")
            {
                strID = Convert.ToString(e.CommandArgument);

                cmbCourt.SelectedValue = strID.ToString();
                Session["RepID"] = cmbRep.SelectedIndex;
                Session["PaymentID"] = cmbPayType.SelectedIndex;
                Session["CourtID"] = cmbCourt.SelectedIndex;
                BindFirmsTransactionDetailReport();
            }
        }

        private void BindControls()
        {
            //Fill Reps

            DataTable dtRep = firms.GetFirmsReps(calDateFrom.SelectedDate, calDateTo.SelectedDate);
            cmbRep.DataSource = dtRep;
            cmbRep.DataTextField = dtRep.Columns[0].ColumnName;
            cmbRep.DataValueField = dtRep.Columns[1].ColumnName;
            cmbRep.DataBind();
            cmbRep.Items.Insert(0, "All Reps");
            
            //Fill Payment Types

            IDataReader drRepList = null;
            drRepList = firms.GetFirmCCType();

            System.Web.UI.WebControls.ListItem Item10 = new System.Web.UI.WebControls.ListItem("Show All (Incl. Attorney/Friend Credit)", "-1");
            cmbPayType.Items.Add(Item10);

            System.Web.UI.WebControls.ListItem Item11 = new System.Web.UI.WebControls.ListItem("Show All (Incl. Attorney Credit)", "-200");
            cmbPayType.Items.Add(Item11);

            System.Web.UI.WebControls.ListItem Item1 = new System.Web.UI.WebControls.ListItem("Show All (Excl. Attorney/Friend Credit)", "0");
            cmbPayType.Items.Add(Item1);

            System.Web.UI.WebControls.ListItem Item2 = new System.Web.UI.WebControls.ListItem("Show All (Excl. Attorney Credit)", "200");
            cmbPayType.Items.Add(Item2);


            System.Web.UI.WebControls.ListItem Item3 = new System.Web.UI.WebControls.ListItem("CC All (Incl. Manual)", "300");
            cmbPayType.Items.Add(Item3);

            System.Web.UI.WebControls.ListItem Item4 = new System.Web.UI.WebControls.ListItem("CC All (Excl. Manual)", "301");
            cmbPayType.Items.Add(Item4);

            cmbPayType.SelectedValue = "-1";

            while (drRepList.Read())
            {
          
                System.Web.UI.WebControls.ListItem ItemY = new System.Web.UI.WebControls.ListItem();

                ItemY.Text = drRepList["Description"].ToString();
                ItemY.Value = drRepList["PaymentType_PK"].ToString();
                cmbPayType.Items.Add(ItemY);
            }

    
            System.Web.UI.WebControls.ListItem Item5 = new System.Web.UI.WebControls.ListItem("CC (VISA)", "501");
            cmbPayType.Items.Add(Item5);
            System.Web.UI.WebControls.ListItem Item6 = new System.Web.UI.WebControls.ListItem("CC (DISCOVER)", "504");
            cmbPayType.Items.Add(Item6);
            System.Web.UI.WebControls.ListItem Item7 = new System.Web.UI.WebControls.ListItem("CC (MASTER CARD)", "502");
            cmbPayType.Items.Add(Item7);
            System.Web.UI.WebControls.ListItem Item8 = new System.Web.UI.WebControls.ListItem("CC (AMEX)", "503");
            cmbPayType.Items.Add(Item8);

            //Fill Courts

            clsENationWebComponents clsdb = new clsENationWebComponents();
            clsdb.FetchValuesInWebControlBysp(cmbCourt, "usp_Get_All_Court", "ShortName", "Courtid");
            cmbCourt.Items[0].Text = "All Courts";

            cmbCourt.Items.Remove(cmbCourt.Items.FindByValue("3001"));
            cmbCourt.Items.Remove(cmbCourt.Items.FindByValue("3002"));
            cmbCourt.Items.Remove(cmbCourt.Items.FindByValue("3003"));
            cmbCourt.Items.Insert(1, new ListItem("HMC (All)", "1"));
            cmbCourt.Items.Insert(2, new ListItem("All Outside Courts", "2"));
         		
              
        }
           
        protected void cmbRep_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["RepID"] = cmbRep.SelectedIndex;
            Session["PaymentID"] = cmbPayType.SelectedIndex;
            Session["CourtID"] = cmbCourt.SelectedIndex;
            BindFirmsTransactionDetailReport();
        }

        protected void cmbPayType_SelectedIndexChanged(object sender, EventArgs e)
        {

            Session["RepID"] = cmbRep.SelectedIndex;
            Session["PaymentID"] = cmbPayType.SelectedIndex;
            Session["CourtID"] = cmbCourt.SelectedIndex;
            BindFirmsTransactionDetailReport();
        }

        protected void cmbCourt_SelectedIndexChanged(object sender, EventArgs e)
        {

            Session["RepID"] = cmbRep.SelectedIndex;
            Session["PaymentID"] = cmbPayType.SelectedIndex;
            Session["CourtID"] = cmbCourt.SelectedIndex;
            BindFirmsTransactionDetailReport();
        }
    }
}

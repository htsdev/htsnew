using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components; 
using lntechNew.Components.ClientInfo;
using System.IO;
//using iTextSharp.text;
//using iTextSharp.text.pdf;

//Waqas 5057 03/19/2009 Changed to HTP
namespace HTP.PaymentInfo
{
	/// <summary>
	/// Summary description for PaymentInfo.
	/// </summary>
    public partial class CloseOutReportPreview : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.DropDownList cmbType;
		
		clsENationWebComponents objEnationFramework  = new clsENationWebComponents();
		clsSession ClsSession=new clsSession();
        FinancialReports objFinancialReps = new FinancialReports();
		//clsLogger clog = new clsLogger();
		protected System.Web.UI.WebControls.TextBox lblHeading1;
		protected System.Web.UI.WebControls.DataGrid dgrdPayType;
		protected System.Web.UI.WebControls.TextBox txtRemarks;
		protected System.Web.UI.WebControls.DropDownList cmbRep;
		protected System.Web.UI.WebControls.DropDownList cmbPayType;
		protected System.Web.UI.WebControls.DataGrid dgrdPayDetail;
		protected System.Web.UI.WebControls.TextBox Textbox1;
		protected System.Web.UI.WebControls.TextBox Textbox2;
		protected eWorld.UI.CalendarPopup calQueryDate;
		//string sqlQuery;
		DataSet dsPaymentInfo;
		protected System.Web.UI.WebControls.Label lblRep;
		protected System.Web.UI.WebControls.Label lblPayType;
		protected System.Web.UI.WebControls.HyperLink lnkBack;
		protected System.Web.UI.WebControls.DataGrid dgrdPayByRep;
		protected System.Web.UI.WebControls.Label lblTotalCashSys;
		protected System.Web.UI.WebControls.Label lblTotalCashActual;
		protected System.Web.UI.WebControls.Label lblTotalCheckSys;
		protected System.Web.UI.WebControls.Label lblTotalCheckActual;
		protected System.Web.UI.WebControls.Label lblTotalSystem;
		protected System.Web.UI.WebControls.Label lblTotalActual;
		protected System.Web.UI.WebControls.TextBox Textbox3;
		protected System.Web.UI.WebControls.Label lblMessage;
		protected System.Web.UI.WebControls.Label lblDate;	
		//string pdfile;		
		protected System.Web.UI.WebControls.TextBox Textbox5;
		protected System.Web.UI.WebControls.DataGrid dgrdCourt;
		double TotalAmount=0;
		double TotalCourtFee=0;
		Int32 TotalCount=0;
		protected System.Web.UI.WebControls.Label lblCourt;
		protected System.Web.UI.WebControls.DropDownList cmbCourt;
		Int32 TotalCourtTrans=0;



		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			try
			{
                //Waqas 5057 03/17/2009 Checking employee info in session
                if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false)
				{
					Response.Redirect("../frmlogin.aspx",false);
				}
				else //To stop page further execution
				{
					if ( Convert.ToBoolean(ClsSession.GetCookie("sCanUpdateCloseOut",this.Request) ) == false   )
					{
						Response.Redirect("../LoginAccesserror.aspx",false);
						Response.End();
					}
					else
					{
						if (! Page.IsPostBack)
						{
							calQueryDate.SelectedDate = Convert.ToDateTime( Session["ReportDate"]);
							lblDate.Text= calQueryDate.SelectedDate.ToLongDateString() ;
							LoadInital();			
							BindControls();
							GetPaymentTypeSumByDate();
							GetCourtSummary();
							GetPaymentDetailByDateByRep(calQueryDate.SelectedDate);
							cmbRep.SelectedIndex = Convert.ToInt32( Session["RepID"]) ;
							cmbPayType.SelectedIndex = Convert.ToInt32( Session["PaymentID"]) ;
							cmbCourt.SelectedIndex = Convert.ToInt32( Session["CourtID"]) ;
							GetPaymentDetailReport(calQueryDate.SelectedDate);
                            GetMailerSummary(calQueryDate.SelectedDate);
						}
					}
					
				}
			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message ;
				//clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);	
			}		
		}

		private void LoadInital()
		{
//			calQueryDate.UpperBoundDate =DateTime.Now.Date;
//			calQueryDate.LowerBoundDate =  DateTime.Now.AddDays(-7);  
//			calQueryDate.SelectedDate = calQueryDate.UpperBoundDate;
//			calQueryDate.VisibleDate = calQueryDate.SelectedDate;


			try
			{
				//clsENationWebComponents objEnationFramework  = new clsENationWebComponents();
			
				//objEnationFramework.FetchValuesInWebControlBysp(dgrdPayType, "usp_Get_All_CCType");

				//sqlQuery= "SELECT Lastname as RepName, EmployeeID as RepID FROM tblUsers Where IsSalesRep=1"; 			
				//objEnationFramework.FetchValuesInWebControl(dgrdPayByRep, sqlQuery);

				objEnationFramework.FetchValuesInWebControlBysp(cmbCourt , "usp_Get_All_Court","ShortName","Courtid");
				cmbCourt.Items[0].Text="All Courts";

                // tahir 4225 07/16/2008
                // fixed the court summary hyperlink click bug...
                cmbCourt.Items.Insert(1, new ListItem("All HMC", "1"));
                cmbCourt.Items.Insert(2, new ListItem("All HCJP", "2"));
                cmbCourt.Items.Insert(3, new ListItem("All Other", "100"));
                // end 4225

				GetReps();
			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message ;
				//clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);	
				
			}		

			

		}


		public void GetReps()
		{
			
			DataSet dsRepList =new DataSet();
			string[] key1 = {"@sDate", "@eDate"};
			object[] value1 = {calQueryDate.SelectedDate, calQueryDate.SelectedDate};
			dgrdPayByRep.DataSource = objEnationFramework.Get_DS_BySPArr("usp_get_all_replist", key1, value1);
			dgrdPayByRep.DataBind();
	
			string[] key2 = {"@sDate", "@eDate"};
			object[] value2 = {calQueryDate.SelectedDate, calQueryDate.SelectedDate};
			dsRepList = objEnationFramework.Get_DS_BySPArr("usp_Get_All_RepList", key2, value2);
			
			cmbRep.DataSource=dsRepList;
			cmbRep.DataTextField=dsRepList.Tables[0].Columns[0].ColumnName;
			cmbRep.DataValueField=dsRepList.Tables[0].Columns[1].ColumnName;
			cmbRep.DataBind();	
			cmbRep.Items.Insert(0,"All Reps");
			
		}

		private void BindControls()
		{	
		    //Change By Ajmal
            //SqlDataReader drRepList = null;
			IDataReader drRepList=null ;
			try
			{
				GetReps();						

				drRepList = objEnationFramework.Get_DR_BySP("usp_Get_All_CCType");
			
				System.Web.UI.WebControls.ListItem Item10 = new System.Web.UI.WebControls.ListItem("Show All (Incl. Attorney/Friend Credit)", "-1" );				
				cmbPayType.Items.Add(Item10);
			
				System.Web.UI.WebControls.ListItem Item1 = new System.Web.UI.WebControls.ListItem("Show All (Incl. Attorney Credit)", "0" );				
				cmbPayType.Items.Add(Item1);

				System.Web.UI.WebControls.ListItem Item11 = new System.Web.UI.WebControls.ListItem("Show All (Excl. Attorney/Friend Credit)", "-200" );				
				cmbPayType.Items.Add(Item11);

				System.Web.UI.WebControls.ListItem Item2 = new System.Web.UI.WebControls.ListItem("Show All (Excl. Attorney Credit)", "200" );				
				cmbPayType.Items.Add(Item2);


				System.Web.UI.WebControls.ListItem Item3 = new System.Web.UI.WebControls.ListItem("CC All (Including Manual)", "300" );				
				cmbPayType.Items.Add(Item3);

				System.Web.UI.WebControls.ListItem Item4 = new System.Web.UI.WebControls.ListItem("CC All (Excluding Manual)", "301" );				
				cmbPayType.Items.Add(Item4);


			

				while (drRepList.Read())
				{
					//MyContainer container = new MyContainer();
					//container.SomeString = reader.GetString(0);
					//container.SomeInt = reader.GetInt(1);
					//alReps.Add(container);
					System.Web.UI.WebControls.ListItem ItemY = new System.Web.UI.WebControls.ListItem();	
				
					ItemY.Text= drRepList["Description"].ToString(); 
					ItemY.Value = drRepList["PaymentType_PK"].ToString() ; 
					cmbPayType.Items.Add(ItemY);
				}
				

				// CC Types
				System.Web.UI.WebControls.ListItem Item5 = new System.Web.UI.WebControls.ListItem("CC (VISA)", "501" );				
				cmbPayType.Items.Add(Item5);
				System.Web.UI.WebControls.ListItem Item6 = new System.Web.UI.WebControls.ListItem("CC (DISCOVER)", "504" );				
				cmbPayType.Items.Add(Item6);
				System.Web.UI.WebControls.ListItem Item7 = new System.Web.UI.WebControls.ListItem("CC (MASTER CARD)", "502" );				
				cmbPayType.Items.Add(Item7);
				System.Web.UI.WebControls.ListItem Item8 = new System.Web.UI.WebControls.ListItem("CC (AMEX)", "503" );				
				cmbPayType.Items.Add(Item8);
				//System.Web.UI.WebControls.ListItem Item9 = new System.Web.UI.WebControls.ListItem("CC (MANUAL)", "500" );				
				//cmbPayType.Items.Add(Item9);

			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message ;
				//clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);	
				
			}		
			finally
			{
				drRepList.Close();
			}
			
		}


		private void GetPaymentDetailByDateByRep(DateTime  QueryDate)
		{
			DataSet  dsWeeklyPaymentDetailsByRep; 
			string strEmployeeID;
			//string Temp;
			Decimal Total;
			Decimal TotalActual=0;			
			string strExpr;
			
			Decimal CashSysTotal=0;
			Decimal CashActualTotal=0;
			Decimal CheckSysTotal=0;
			Decimal CheckActualTotal=0;
			Decimal TotalSysTotal=0;
			Decimal TotalActualTotal=0;
			bool bAlternate = false;


			try
			{
                string[] key1 = { "@RecDate", "@RecDateTo", "@BranchID" };
                object[] value1 = { QueryDate, QueryDate, Convert.ToInt32(Request.QueryString["b"].ToString()) };
				//dsPaymentInfo  = objEnationFramework.Get_DS_BySPByOneParmameter("usp_Get_All_PaymentDetailByRecDate","RecDate",QueryDate); 
				dsPaymentInfo  = objEnationFramework.Get_DS_BySPArr("usp_Get_All_PaymentDetailByRecDateRange",key1,value1);
                // Sabir Khan 10920 05/27/2013 Branch Id added.
                dsWeeklyPaymentDetailsByRep = objFinancialReps.GetWeeklyPaymentDetailByDateByRep(QueryDate, Convert.ToInt32(Request.QueryString["b"].ToString()));
                //dsWeeklyPaymentDetailsByRep  = objEnationFramework.Get_DS_BySPByOneParmameter("usp_Get_All_tblPaymentDetailWeeklyByDate","TransDate",QueryDate); 

				// Get Day End Notes
				if(dsWeeklyPaymentDetailsByRep.Tables[0].Rows.Count > 0)
				{
					txtRemarks.Text = dsWeeklyPaymentDetailsByRep.Tables[0].Rows[0]["Notes"].ToString();
				}
				else
				{
					txtRemarks.Text ="";
				}			

				//create and populate a DataColumn to set the PK
				//DataColumn [] pkColumn = new DataColumn[1];
				//pkColumn[0] = dsPaymentInfo.Tables[0].Columns["EmployeeID"];
				//set the primary key 
				//dsWeeklyPaymentDetailsByRep.Tables[0].PrimaryKey = pkColumn;			
			
				foreach (DataGridItem ItemX in dgrdPayByRep.Items) 
				{ 
					DataRow[] foundRows;
					DataRow[] PaymentRows;
					//string txtboxID;
					//string fxName;
					Decimal dActualCash=0;
					Decimal dActualCheck=0;
					Decimal dSysCash=0;
					Decimal dSysCheck=0;

				

					// js Validation Bind to textboxes
					//txtboxID= ((TextBox)(ItemX.FindControl("txtActualCash"))).ClientID.ToString(); 
					//fxName = "return ValidateMe(" + txtboxID + ");" ;
					//((TextBox)(ItemX.FindControl("txtActualCash"))).Attributes.Add("Onblur", fxName); 


					//txtboxID= ((TextBox)(ItemX.FindControl("txtActualCheck"))).ClientID.ToString(); 
					//fxName = "return ValidateMe(" + txtboxID + ");" ;
					//((TextBox)(ItemX.FindControl("txtActualCheck"))).Attributes.Add("Onblur", fxName); 
					//  Bind End


					strEmployeeID = ((Label)(ItemX.FindControl("lblEmployeeID"))).Text.ToString() ; 
					//find a row based on the value entered in the textbox
					//DataRow rowWeeklyPay = dsWeeklyPaymentDetailsByRep.Tables[0].Rows.Find(EmployeeID);
					dsWeeklyPaymentDetailsByRep.Tables[0].Select();

					strExpr = "EmployeeID =" + strEmployeeID ;		
					foundRows = dsWeeklyPaymentDetailsByRep.Tables[0].Select(strExpr);
				

					if (foundRows.Length > 0)
					{
						((TextBox)(ItemX.FindControl("txtActualCash"))).Text = String.Format("{0:#.##}", (foundRows[0]["ActualCash"]));
						((TextBox)(ItemX.FindControl("txtActualCheck"))).Text = String.Format("{0:#.##}",(foundRows[0]["ActualCheck"]));
					
						TotalActual = Convert.ToDecimal(foundRows[0]["ActualCash"]) + Convert.ToDecimal(foundRows[0]["ActualCheck"]); 
						dActualCash = Convert.ToDecimal(foundRows[0]["ActualCash"]);
						dActualCheck = Convert.ToDecimal(foundRows[0]["ActualCheck"]);

						CashActualTotal += dActualCash ; 
						CheckActualTotal += dActualCheck ;
					}
					else
					{
						((TextBox)(ItemX.FindControl("txtActualCash"))).Text = "" ;
						((TextBox)(ItemX.FindControl("txtActualCheck"))).Text = "" ;
						TotalActual=0;
					}
				

					dsPaymentInfo.Tables[0].Select();

					strExpr = "EmpID =" + strEmployeeID ;		
					PaymentRows = dsPaymentInfo.Tables[0].Select(strExpr);
				
					if (PaymentRows.Length > 0)
					{
						((Label)(ItemX.FindControl("lblSystemCash"))).Text = String.Format("{0:c}", PaymentRows[0]["CashAmount"] );
						((Label)(ItemX.FindControl("lblSystemCheck"))).Text = String.Format("{0:c}", PaymentRows[0]["CheckAmount"] );

						Total = Convert.ToDecimal(PaymentRows[0]["CashAmount"]) + Convert.ToDecimal(PaymentRows[0]["CheckAmount"]); 
						dSysCash = Convert.ToDecimal(PaymentRows[0]["CashAmount"]);
						dSysCheck = Convert.ToDecimal(PaymentRows[0]["CheckAmount"]);

						CashSysTotal += dSysCash ;
						CheckSysTotal  += dSysCheck ;
					}
					else
					{
						((Label)(ItemX.FindControl("lblSystemCash"))).Text = "" ;
						((Label)(ItemX.FindControl("lblSystemCheck"))).Text = "" ;					
						Total=0;
					}



					// Check difference and highlight
					if (dSysCash != dActualCash)
					{
						((Label)(ItemX.FindControl("lblSystemCash"))).BackColor  = Color.Red ;
						((Label)(ItemX.FindControl("lblSystemCash"))).ForeColor  = Color.White ;
					}

					if (dSysCheck  != dActualCheck )
					{
						((Label)(ItemX.FindControl("lblSystemCheck"))).BackColor  = Color.Red ;
						((Label)(ItemX.FindControl("lblSystemCheck"))).ForeColor  = Color.White ;
					}




					// Row System Total 
					if (Total != 0) 
					{
						((Label)(ItemX.FindControl("lblSystemTotal"))).Text = String.Format("{0:c}", Total  );
						TotalSysTotal += Total;
					}
					else
					{
						((Label)(ItemX.FindControl("lblSystemTotal"))).Text = "" ;
					}

					// Row Actual Total 
					if (TotalActual != 0) 
					{
						((Label)(ItemX.FindControl("lblActualTotal"))).Text = String.Format("{0:c}", TotalActual  );
						TotalActualTotal += TotalActual;
					}
					else
					{
						((Label)(ItemX.FindControl("lblActualTotal"))).Text = "" ;
					}

					// If Total is zero so hide the row
					if ((Total + TotalActual) == 0 ) 
					{
						ItemX.Visible=false ;
					}
					else
					{
						ItemX.Visible=true;

						if ( bAlternate==true )
						{
							ItemX.BackColor = Color.FromArgb(238,238,238) ;
						}
					
						bAlternate = ! bAlternate;
					
					}


				



				}

				lblTotalCashSys.Text = String.Format("{0:c}", CashSysTotal);
				lblTotalCashActual.Text = String.Format("{0:c}", CashActualTotal);
				lblTotalCheckSys.Text = String.Format("{0:c}", CheckSysTotal);
				lblTotalCheckActual.Text = String.Format("{0:c}", CheckActualTotal);
				lblTotalSystem.Text = String.Format("{0:c}", TotalSysTotal);
				lblTotalActual.Text = String.Format("{0:c}", TotalActualTotal);

				//dgrdPayByRep.AlternatingItemStyle.BackColor= Color.FromArgb(238,238,238) ;
			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message ;
				//clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);	
				
			}		

		}


		private void GetPaymentSummaryByDate(DateTime  QueryDate)
		{
           
			
			try
			{
                string[] key1 = { "@RecDate" };
                object[] value1 ={ QueryDate };

                dsPaymentInfo = objEnationFramework.Get_DS_BySPArr("usp_Get_All_PaymentInfoByRecDate", key1, value1); 
			
				//create and populate a DataColumn to set the PK
				DataColumn [] pkColumn = new DataColumn[1];
				pkColumn[0] = dsPaymentInfo.Tables[0].Columns["PaymentType"];
				//set the primary key 
				dsPaymentInfo.Tables[0].PrimaryKey = pkColumn;

			
				foreach (DataGridItem ItemX in dgrdPayType.Items) 
				{ 
					//((LinkButton)(dgItem.FindControl("lblCCTypeID"))).Attributes.Add("Onclick", "LoadDocument();"); 
                    string strval;
					strval =  ((Label)(ItemX.FindControl("lblPayTypeID"))).Text  ; 
				
					//find a row based on the value entered in the textbox
					DataRow rowFound = dsPaymentInfo.Tables[0].Rows.Find(strval);

					if (rowFound != null)				
					{
						((Label)(ItemX.FindControl("lblCount"))).Text = rowFound["TotalCount"].ToString() ;				
						//((Label)(ItemX.FindControl("lblAmount"))).Text = String.Format("{0:#,###.##}", rowFound["Amount"]);
						((Label)(ItemX.FindControl("lblAmount"))).Text = String.Format("{0:c}", rowFound["Amount"]);
					}
					else
					{
						((Label)(ItemX.FindControl("lblCount"))).Text = "" ;									
						((Label)(ItemX.FindControl("lblAmount"))).Text = "" ;
					}
				
				}
			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message ;
				//clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);	
				
			}		
		}


		private void GetPaymentDetailReport(DateTime  QueryDate)
		{			
			DataSet   dsFinReport;
			int RepID=0;
			int PayID=0;
			int CourtID = 0;

			try
			{
				if (cmbRep.SelectedIndex!=0 ) 						
					RepID = Convert.ToInt32(cmbRep.SelectedValue) ;

				//if (cmbPayType.SelectedIndex!=0 ) 						
				//	PayID = Convert.ToInt32(cmbPayType.SelectedValue) ;
                if(Request.QueryString["p"] != null)
                PayID = Convert.ToInt32(Request.QueryString["p"].ToString());

				if (cmbCourt.SelectedIndex!=0 ) 						
					CourtID = Convert.ToInt32(cmbCourt.SelectedValue) ;


				if ( PayID > 500 ) // IF Credit Card
				{
					//dsFinReport = objEnationFramework.Get_DS_BySPByThreeParmameter("usp_Get_All_PaymentDetailOfCCByCriteria","RecDate",QueryDate ,"EmployeeID", RepID, "PaymentType", PayID);
                    string[] keys = { "@RecDate", "@RecDateTo", "@EmployeeID", "@PaymentType", "@CourtID", "@BranchID" };
                    object[] values = { calQueryDate.SelectedDate, calQueryDate.SelectedDate, RepID, PayID, CourtID, Convert.ToInt32(Request.QueryString["b"].ToString()) };									
					//dsFinReport = objEnationFramework.Get_DS_BySPArr("usp_Get_All_PaymentDetailOfCCByCriteria", keys ,values ); 
					dsFinReport = objEnationFramework.Get_DS_BySPArr("usp_Get_All_PaymentDetailOfCCByCriteriaRange", keys ,values ); 
				}
				else
				{
					//dsFinReport = objEnationFramework.Get_DS_BySPByThreeParmameter("usp_Get_All_PaymentDetailByCriteria","RecDate",QueryDate ,"EmployeeID", RepID, "PaymentType", PayID);
                    string[] keys = { "@RecDate", "@RecDateto", "@EmployeeID", "@PaymentType", "@CourtID", "@BranchID" };
                    object[] values = { calQueryDate.SelectedDate, calQueryDate.SelectedDate, RepID, PayID, CourtID, Convert.ToInt32(Request.QueryString["b"].ToString()) };									
					//dsFinReport = objEnationFramework.Get_DS_BySPArr("usp_Get_All_PaymentDetailByCriteria", keys ,values ); 
					dsFinReport = objEnationFramework.Get_DS_BySPArr("usp_Get_All_PaymentDetailByCriteriaRange", keys ,values ); 
				}

				//PayID = Convert.ToInt32(cmbPayType.SelectedValue); 
			
				//objEnationFramework.FetchValuesInWebControlBysp(dgrdPayDetail,"usp_Get_All_PaymentDetailByCriteria");  
				//dsFinReport = objEnationFramework.Get_DS_BySPByThreeParmameter("usp_Get_All_PaymentDetailByCriteria","RecDate",QueryDate ,"EmployeeID", RepID, "PaymentType", PayID);

				dgrdPayDetail.DataSource = dsFinReport;
				dgrdPayDetail.DataBind(); 

				BindPaymentDetailReport();
				lblRep.Text = cmbRep.SelectedItem.Text.ToString(); 
				lblPayType.Text = cmbPayType.SelectedItem.Text.ToString(); 
				lblCourt.Text= cmbCourt.SelectedItem.Text.ToString();
				//lblCourt.Text= Request.Form[0].cmob.text;
			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message ;
				//clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);	
				
			}		

		}

		private void BindPaymentDetailReport()
		{
			int sNo=0;

			try
			{
				foreach (DataGridItem ItemX in dgrdPayDetail.Items) 
				{ 
					//((LinkButton)(dgItem.FindControl("lblCCTypeID"))).Attributes.Add("Onclick", "LoadDocument();"); 
					sNo += 1;

					((Label)(ItemX.FindControl("lblNo"))).Text= sNo.ToString()   ; 

					if (((Label)(ItemX.FindControl("lblBond"))).Text == Convert.ToString('0'))
					{
						((Label)(ItemX.FindControl("lblBond"))).Text= "" ;
					}
					
					// CardType Column					
					((Label)(ItemX.FindControl("lblCardType"))).Text = GetCardType(((Label)(ItemX.FindControl("lblCardTypeID"))).Text.ToString()) ;
					
				}
			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message ;
				//clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);	
				
			}		
		}


		private string  GetCardType(string CardTypeID)
		{
			switch (CardTypeID)
			{
				case "1":
					return("VISA");						
				case "2":					
					return("MC");						
				case "3":
					return("AMEX");											
				case "4":
					return("DISC");											
				default:
					return("");											
			}

		}


		private void GetPaymentSummary(int CCTypeID)
		{
			

			//DR  = objEnationFramework.Get_All_RecordsBySPByOneParmameter("usp_Get_All_PaymentDetailByDate","Name",CCTypeID); 

		}

		public void DoGetQueryString(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e) 
		{
			string strID;
			//Session["flagPC"]=txtPC.Text ;
			//Session["flagPR"]=txtPR.Text ;
 

			try
			{
				if (e.CommandName == "DoGetPayment")
				{
					strID = ((Label )(e.Item.FindControl("lblPayTypeID"))).Text.ToString()  ;
				
					cmbPayType.SelectedValue = strID.ToString() ;
					cmbRep.SelectedIndex=0;
					GetPaymentDetailReport(calQueryDate.SelectedDate);
				}

				else if (e.CommandName == "DoGetRep")
				{
					strID = ((Label )(e.Item.FindControl("lblEmployeeID"))).Text.ToString()  ;
				
					cmbRep.SelectedValue = strID.ToString() ;

					GetPaymentDetailReport(calQueryDate.SelectedDate);
				}
			

				else if (e.CommandName == "DoGetCustomer")
				{
					//strID = ((Label )(e.Item.FindControl("lblTicketID"))).Text.ToString()  ;
					//strID = "../../ClientInfo/violationsfees.asp?caseNumber=" + strID;
					//Response.Redirect(strID);

				
				}
			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message ;
				//clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);	
				
			}		


 
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.calQueryDate.DateChanged += new eWorld.UI.DateChangedEventHandler(this.calQueryDate_DateChanged);
			this.dgrdPayType.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgrdPayType_ItemDataBound);
			this.dgrdCourt.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgrdCourt_ItemDataBound);
			this.cmbRep.SelectedIndexChanged += new System.EventHandler(this.cmbRep_SelectedIndexChanged);
			this.cmbPayType.SelectedIndexChanged += new System.EventHandler(this.cmbPayType_SelectedIndexChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		

		private void btnUpdate_Click(object sender, System.EventArgs e)
		{
			//Session["flagPC"]=txtPC.Text ;
			//Session["flagPR"]=txtPR.Text ;
 
			SaveReport();

			//GetPaymentSummaryByDate(calQueryDate.SelectedDate);  
			GetPaymentTypeSumByDate();
			GetPaymentDetailByDateByRep(calQueryDate.SelectedDate);
			GetPaymentDetailReport(calQueryDate.SelectedDate);
		}

		private bool SaveReport()
		{		
			
			try
			{
				objEnationFramework.DeleteBySPByOneParmameter("usp_Del_tblPaymentDetailWeeklyByDate","TransDate",calQueryDate.SelectedDate); 

				foreach (DataGridItem ItemX in dgrdPayByRep.Items) 
				{ 	
					Decimal dCheck=0;
					Decimal dCash=0;
					string TempValue="";

					string EmployeeID = ((Label)(ItemX.FindControl("lblEmployeeID"))).Text.ToString() ; 
					TempValue = ((TextBox )(ItemX.FindControl("txtActualCash"))).Text;
					if (TempValue != "") 
					{
						dCash = Convert.ToDecimal(TempValue)  ; }

					TempValue = ((TextBox )(ItemX.FindControl("txtActualCheck"))).Text;
					if (TempValue != "") 
					{
						dCheck = Convert.ToDecimal(TempValue)  ; }
				
					if ((dCash+dCheck)> 0)
					{
						objEnationFramework.InsertBySP("usp_Add_tblPaymentDetailWeekly","EmployeeID", Convert.ToInt32(EmployeeID) ,"TransDate",calQueryDate.SelectedDate,"ActualCash",dCash, "ActualCheck", dCheck, "Notes", txtRemarks.Text.ToString()  );

					}
				}
			
				return true;
			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message ;
				//clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);	
				return false;
			}		
		}

		private void dgrdPayDetail_ItemCreated(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			//			ListItemType lit = e.Item.ItemType; 
			//			if(ListItemType.Header == lit) 
			//			{ 
			//				//*** Redirect the default header rendering method to our own method 
			//				//e.Item.SetRenderMethodDelegate(new RenderMethod(NewRenderMethod)); 				
			//				//clsENationWebComponents obj  = new clsENationWebComponents();
			//				
			//				//obj.FetchValuesInWebControlBysp( e.Item.FindControl("cmbRep"),"usp_Get_All_RepList","RepName","RepID");
			//			} 
		}



		
		private void lnkbtnPrint_Click(object sender, System.EventArgs e)
		{
			//Response.Redirect(Session["DocID"].ToString()) ;
			//Response.Redirect("PreviewMain.aspx");


		}

		private void calQueryDate_DateChanged(object sender, System.EventArgs e)
		{
			//Session["flagPC"]=txtPC.Text ;
			//Session["flagPR"]=txtPR.Text ;
 
			cmbPayType.SelectedIndex=0;
			cmbRep.SelectedIndex=0;
			cmbCourt.SelectedIndex=0;
			//GetPaymentSummaryByDate(calQueryDate.SelectedDate);  
			GetPaymentTypeSumByDate();
			GetPaymentDetailByDateByRep(calQueryDate.SelectedDate);
			GetPaymentDetailReport(calQueryDate.SelectedDate);		
		}

		private void cmbRep_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			
			GetPaymentDetailReport(calQueryDate.SelectedDate);		
		}

		private void cmbPayType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			GetPaymentDetailReport(calQueryDate.SelectedDate);		
		}

		private void lnkbtnBack_Click(object sender, System.EventArgs e)
		{
			Response.Redirect ("NewPaymentInfo.aspx");
		}

		//		
		//		private bool GenratePDF()
		//		{
		//			string strFileName;
		//			
		//			pdfile = Server.MapPath("../Temp/");
		//			pdfile += Session.SessionID + ".pdf";
		//			Session["DocID"]= pdfile;
		//
		//			Document document = new Document(PageSize.A4,5,5,12,5);			
		//			
		//			//MemoryStream m = new MemoryStream(); 			
		//			//PdfWriter.getInstance(document, new FileStream(pdfile, FileMode.Create));
		//			//PdfWriter.getInstance (document,m);
		//			PdfWriter writer = PdfWriter.getInstance(document, new FileStream(pdfile, FileMode.Create));
		//			document.Open();
		//			
		//			PdfContentByte cb = writer.DirectContent;
		//			
		//
		//			strFileName = Server.MapPath("../Images/");
		//			strFileName += "ln_logo2.jpg";
		//
		//			iTextSharp.text.Image Pic= iTextSharp.text.Image.getInstance(strFileName);
		//            Pic.scalePercent(40,40);
		//            document.Add(Pic);
		//
		//			document.Add(new Paragraph("Date " + calQueryDate.SelectedDate.ToShortDateString()  ));
		//			
		//			//Font font0 = FontFactory.getFont(BaseFont.HELVETICA, BaseFont.WINANSI, 8.5);
		//			BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
		//			iTextSharp.text.Font font = new iTextSharp.text.Font(bf, 8, iTextSharp.text.Font.NORMAL);
		//
		//			cb.beginText();
		//			//cb.setFontAndSize(bf, 4);
		//			//cb.showTextAligned(PdfContentByte.ALIGN_CENTER, text + "This text is centered", 250, 700, 0);
		//			
		//
		//
		//
		//			iTextSharp.text.Table tableMain = new iTextSharp.text.Table(2);
		//			//tableMain.
		//			iTextSharp.text.Table tableLeft = new iTextSharp.text.Table(3);
		//			iTextSharp.text.Table tableRight = new iTextSharp.text.Table(7);
		//			iTextSharp.text.Table tableReport = new iTextSharp.text.Table(7);
		//
		//
		//			tableMain.WidthPercentage = 100;			
		//			//tableLeft.WidthPercentage = 40;
		//			tableRight.WidthPercentage = 100;
		//			tableReport.WidthPercentage= 100;
		//
		//			tableLeft.DefaultVerticalAlignment= Element.ALIGN_MIDDLE;
		//
		//			Cell cell01 = new Cell("CC Type");			
		//			//cell1.HorizontalAlignment=Element.ALIGN_CENTER;
		//			cell01.Width = Convert.ToString(300);
		//			
		//			tableLeft.addCell(cell01);
		//
		//			Cell cell02 = new Cell("Count");			
		//			cell02.HorizontalAlignment=Element.ALIGN_CENTER;
		//			cell02.Width = Convert.ToString(100);
		//			tableLeft.addCell(cell02);
		//
		//			Cell cell03 = new Cell("Amount");			
		//			cell03.HorizontalAlignment=Element.ALIGN_CENTER;
		//			cell03.Width = Convert.ToString(100);
		//			tableLeft.addCell(cell03);
		//							
		//			foreach (DataGridItem ItemX in dgrdPayType.Items) 
		//			{ 	
		//				string strText;
		//				
		//				strText = ((Label)(ItemX.FindControl("lblPayType"))).Text;
		//				tableLeft.addCell(strText);
		//
		//				strText = ((Label)(ItemX.FindControl("lblCount"))).Text;
		//				tableLeft.addCell(strText);
		//
		//				strText = ((Label)(ItemX.FindControl("lblAmount"))).Text;
		//				tableLeft.addCell(strText);
		//
		//			}
		//
		//
		//			tableRight.DefaultVerticalAlignment= Element.ALIGN_MIDDLE;
		//
		//			tableRight.addCell("Rep");
		//			Cell cell1 = new Cell("Cash");
		//			cell1.Colspan=2;
		//			cell1.HorizontalAlignment=Element.ALIGN_CENTER;
		//			tableRight.addCell(cell1);
		//
		//			Cell cell2 = new Cell("Check");
		//			cell2.Colspan=2;
		//			cell2.HorizontalAlignment=Element.ALIGN_CENTER;
		//			tableRight.addCell(cell2);
		//
		//			Cell cell3 = new Cell("Total");
		//			cell3.Colspan=2;
		//			cell3.HorizontalAlignment=Element.ALIGN_CENTER;
		//			tableRight.addCell(cell3);
		//
		//
		//
		//			foreach (DataGridItem ItemX in dgrdPayByRep.Items) 
		//			{ 	
		//				string strText;
		//				
		//				strText = ((Label)(ItemX.FindControl("lblEmployee"))).Text;
		//				tableRight.addCell(strText);
		//
		//				strText = ((Label)(ItemX.FindControl("lblSystemCash"))).Text;
		//				tableRight.addCell(strText);
		//				strText = ((TextBox)(ItemX.FindControl("txtActualCash"))).Text;
		//				tableRight.addCell(strText);
		//
		//				strText = ((Label)(ItemX.FindControl("lblSystemCheck"))).Text;
		//				tableRight.addCell(strText);
		//				strText = ((TextBox)(ItemX.FindControl("txtActualCheck"))).Text;
		//				tableRight.addCell(strText);
		//
		//				strText = ((Label)(ItemX.FindControl("lblSystemTotal"))).Text;
		//				tableRight.addCell(strText);
		//				strText = ((Label)(ItemX.FindControl("lblActualTotal"))).Text;
		//				tableRight.addCell(strText);
		//			}
		//
		//
		//
		//
		//
		//			tableReport.DefaultVerticalAlignment= Element.ALIGN_MIDDLE;
		//			
		//			Cell cellFR1 = new Cell("No.");			
		//			cellFR1.HorizontalAlignment=Element.ALIGN_CENTER;
		//			tableReport.addCell(cellFR1);
		//
		//			Cell cellFR2 = new Cell("Date");			
		//			cellFR2.HorizontalAlignment=Element.ALIGN_CENTER;
		//			tableReport.addCell(cellFR2);
		//
		//			Cell cellFR3 = new Cell("Client Name");						
		//			tableReport.addCell(cellFR3);
		//
		//			Cell cellFR4 = new Cell("Rep Name");						
		//			tableReport.addCell(cellFR4);
		//
		//			Cell cellFR5 = new Cell("Paid Amount");						
		//			tableReport.addCell(cellFR5);
		//
		//			Cell cellFR6 = new Cell("Payment Type");						
		//			tableReport.addCell(cellFR6);
		//
		//			Cell cellFR7 = new Cell("Court");						
		//			tableReport.addCell(cellFR7);
		//
		//
		//
		//			foreach (DataGridItem ItemX in dgrdPayDetail.Items) 
		//			{ 	
		//				string strText;
		//				
		//				strText = ((Label)(ItemX.FindControl("lblNo"))).Text;
		//				tableReport.addCell(strText);
		//
		//				strText = ((Label)(ItemX.FindControl("lblDate"))).Text;
		//				tableReport.addCell(strText);
		//
		//				strText = ((Label)(ItemX.FindControl("lblCustomer"))).Text;
		//				tableReport.addCell(strText);
		//
		//				strText = ((Label)(ItemX.FindControl("lblRep"))).Text;
		//				tableReport.addCell(strText);
		//
		//				strText = ((Label)(ItemX.FindControl("lblPaidAmount"))).Text;
		//				tableReport.addCell(strText);
		//
		//				strText = ((Label)(ItemX.FindControl("lblPayTypeFR"))).Text;
		//				tableReport.addCell(strText);
		//
		//				strText = ((Label)(ItemX.FindControl("lblCourt"))).Text;
		//				tableReport.addCell(strText);
		//			}
		//
		//
		//			Cell tableMainCell0 = new Cell(tableLeft);
		//			Cell tableMainCell1 = new Cell(tableRight);
		//			//Cell tableMainCell2 = new Cell(tableReport);
		//			//tableMainCell2.Colspan=2;
		//
		//			cb.endText(); 
		//
		//			tableMain.addCell(tableMainCell0, new Point(0,0));
		//			tableMain.addCell(tableMainCell1, new Point(0,1));			
		//			//tableMain.addCell(tableMainCell2, new Point(1,0));
		//
		//
		//			document.Add(tableMain);
		//			document.Add(tableReport);
		//
		//			document.Close();
		//
		//			return true;
		//
		//
		//
		//		}
		//
        
        //Kazim 3736 5/6/2008 Bind Mailer Summary Section

        private void GetMailerSummary(DateTime QueryDate)
        {
            DataSet dsFinReport;
            //int FirmId = 0;
            int PayID = -1;
            int CourtID = 0;

            try
            {
                string[] keys = { "@RecDate", "@RecDateTo", "@EmployeeID", "@PaymentType", "@CourtID", "@BranchID" };
                object[] values = { calQueryDate.SelectedDate, calQueryDate.SelectedDate, 0, PayID, CourtID, Convert.ToInt32(Request.QueryString["b"].ToString()) };
                //Sabir Khan 6047 06/30/2009 sp has been replaced...
                dsFinReport = objEnationFramework.Get_DS_BySPArr("USP_HTP_Get_MailerSummaryDetail", keys, values);
                
                dgMailerSummary.DataSource = dsFinReport.Tables[0];
                dgMailerSummary.DataBind();
                //BindMailerSummaryReport();
                iMailerSummaryCount.Text = dgMailerSummary.Items.Count.ToString();
                


            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;

            }

        }

        private void BindMailerSummaryReport()
        {
            int sNo = 0;
            try
            {
                foreach (DataGridItem ItemX in dgMailerSummary.Items)
                {
                    // Serial No.
                    //((LinkButton)(dgItem.FindControl("lblCCTypeID"))).Attributes.Add("Onclick", "LoadDocument();"); 
                    sNo += 1;
                    ((Label)(ItemX.FindControl("lblSNo"))).Text = sNo.ToString();

                }
            }

            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }

		private void GetPaymentTypeSumByDate()
		{
            //Change by Ajmal
            //SqlDataReader   DRPaySum;
            IDataReader   DRPaySum;
            string[] key1 = { "@RecDate", "@RecTo", "@BranchID" };
			object[] value1 = {calQueryDate.SelectedDate, calQueryDate.SelectedDate, Convert.ToInt32(Request.QueryString["b"].ToString())};
			//DRPaySum =  objEnationFramework.Get_DR_BySPByOneParmameter("usp_Get_All_PaymentSumByRecDate","RecDate",calQueryDate.SelectedDate);
			DRPaySum =  objEnationFramework.Get_DR_BySPArr("usp_Get_All_PaymentSumByRecDateRange",key1, value1);
			dgrdPayType.DataSource= DRPaySum;
			dgrdPayType.DataBind();


			foreach (DataGridItem ItemX in dgrdPayType.Items) 
			{ 					
				string PayType = ((Label)(ItemX.FindControl("lblPayType1"))).Text ;
				string PayTypeId = ((Label)(ItemX.FindControl("lblPayTypeID"))).Text ;	
				//((Label)(ItemX.FindControl("lblCount"))).Text = rowFound["TotalCount"].ToString() ;				
				//((Label)(ItemX.FindControl("lblAmount"))).Text = String.Format("{0:#,###.##}", rowFound["Amount"]);
				double dAmount = Convert.ToDouble ( ((Label)(ItemX.FindControl("lblAmount"))).Text.ToString())  ;
				((Label)(ItemX.FindControl("lblAmount"))).Text = String.Format("{0:c}", dAmount);				

				if (PayType=="Credit Card" || PayTypeId == "200")
					((System.Web.UI.WebControls.Image)(ItemX.FindControl("imgParent"))).Visible = true;

                //Waqas 5755 04/07/2009 Check for prepaid legal
                if ((PayType == "Visa") || (PayType == "Master Card") || (PayType == "American Express") || (PayType == "Discover") || PayTypeId == "2" || PayTypeId == "4" || PayTypeId == "3")
				{
					((System.Web.UI.WebControls.Image)(ItemX.FindControl("imgLink"))).Visible = true;				
					((System.Web.UI.WebControls.Image)(ItemX.FindControl("ImageChild"))).Visible = true;
					if (PayType=="Discover" || PayTypeId == "4" ) 
						((System.Web.UI.WebControls.Image)(ItemX.FindControl("imgLink"))).ImageUrl = "../Images/L.gif";

				}
			}

			

		}

		private void dgrdPayType_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				string PayType = ((Label)(e.Item.FindControl("lblPayType1"))).Text ;
				string PayTypeID=((Label)(e.Item.FindControl("lblPayTypeID"))).Text ;
                //Waqas 5755 04/07/2009 Check for prepaid legal 
                if ((PayType != "Visa") && (PayType != "Master Card") && (PayType != "American Express") && (PayType != "Discover") && (PayTypeID != "2") && (PayTypeID != "4") && (PayTypeID != "3"))
				{
					TotalCount += Convert.ToInt32( ((Label)(e.Item.FindControl("lblCount"))).Text.ToString()) ;
					TotalAmount += Convert.ToDouble ( ((Label)(e.Item.FindControl("lblAmount"))).Text.ToString()) ;
				}
				//e.Item.Cells[1].Text = string.Format("{0:c}", Convert.ToDouble(e.Item.Cells[1].Text));
			}
			else if(e.Item.ItemType == ListItemType.Footer )
			{
				e.Item.Cells[0].CssClass="GrdHeader";
				e.Item.Cells[0].Text="Total";
				
				e.Item.Cells[1].CssClass="GrdLbl";
				e.Item.Cells[1].HorizontalAlign= HorizontalAlign.Center;
				e.Item.Cells[1].Text= TotalCount.ToString() ;

				e.Item.Cells[2].CssClass="GrdLbl";
				e.Item.Cells[2].HorizontalAlign=HorizontalAlign.Right ;
				e.Item.Cells[2].Text = (string.Format("{0:c}", TotalAmount) + "&nbsp;&nbsp;&nbsp;");
				
			} 
		}

		private void dgrdCourt_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
                if ((Convert.ToInt16(((Label)(e.Item.FindControl("lblIsCourtCategory"))).Text) != 0)  || ((Convert.ToInt16(((Label)(e.Item.FindControl("lblIsCourtCategory"))).Text) == 0) && (Convert.ToInt16(((Label)(e.Item.FindControl("lblCategoryNumber"))).Text) != 100) && (Convert.ToInt16(((Label)(e.Item.FindControl("lblCategoryNumber"))).Text) != 2)))
                {
                    TotalCourtTrans += Convert.ToInt32(((Label)(e.Item.FindControl("lblTrans"))).Text.ToString());
                    TotalCourtFee += Convert.ToDouble(((Label)(e.Item.FindControl("lblFee"))).Text.ToString());
                }
				double dAmount = Convert.ToDouble ( ((Label)(e.Item.FindControl("lblFee"))).Text.ToString())  ;
				((Label)(e.Item.FindControl("lblFee"))).Text = String.Format("{0:c}", dAmount);

                //TAHIR AHMED 3736 05/02/2008
                //SETTING JAVA SCRIPT FUNCTION FOR THE IMAGE IN COURT GROUP

                Int16 iGroup = Convert.ToInt16(((Label)(e.Item.FindControl("lblIsCourtCategory"))).Text);
                Int16 iCategory = Convert.ToInt16(((Label)(e.Item.FindControl("lblCategoryNumber"))).Text);

                if (iGroup == 1 && (iCategory == 2 || iCategory == 100))
                {
                    System.Web.UI.WebControls.Image iFolderOpen = ((System.Web.UI.WebControls.Image)(e.Item.FindControl("imgParent")));
                    System.Web.UI.WebControls.Image iFolderClose = ((System.Web.UI.WebControls.Image)(e.Item.FindControl("imgChild")));

                    iFolderOpen.Attributes.Add("onclick", "ShowHideCourtDetail('" + iCategory.ToString() + "');");
                    iFolderClose.Attributes.Add("onclick", "ShowHideCourtDetail('" + iCategory.ToString() + "');");
                }
			}
			else if(e.Item.ItemType == ListItemType.Footer )
			{
                Label lCount = (Label)(e.Item.FindControl("lbl_Count"));
                Label lAmount = (Label)(e.Item.FindControl("lbl_Amount"));

                lCount.Text = TotalCourtTrans.ToString();
                lCount.CssClass = "GrdLbl";
                lAmount.Text = (string.Format("{0:c}", TotalCourtFee) + "&nbsp;&nbsp;&nbsp;");
                lAmount.CssClass = "GrdLbl";
				
			} 
		
		
		}

		private void GetCourtSummary()
		{
			try
			{
                //Change by Ajmal
                //SqlDataReader drCourt;



				IDataReader drCourt;
                string[] key1 = { "@RecDate", "@RecTo", "@BranchID" };
                object[] value1 = { calQueryDate.SelectedDate, calQueryDate.SelectedDate, Convert.ToInt32(Request.QueryString["b"].ToString()) };
				//drCourt  = objEnationFramework.Get_DR_BySPByOneParmameter("usp_Get_All_CourtInfoByRecDate","RecDate",calQueryDate.SelectedDate); 
				drCourt  = objEnationFramework.Get_DR_BySPArr("usp_Get_All_CourtInfoByRecDateRange",key1, value1); 
				dgrdCourt.DataSource=drCourt;
				dgrdCourt.DataBind();

                //tahir ahmed 3736 05/02/2008
                //for grouping of courts....
                iCourtCount.Text = dgrdCourt.Items.Count.ToString();
			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message ;
				//clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);	
			}
		}

        protected void dgMailerSummary_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            // tahir 4171 06/28/2008
            // to provide expand/collapse functionality for each court category.
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                bool IsGroup = Convert.ToBoolean(((Label)(e.Item.FindControl("lbl_IsGroup"))).Text);

                HtmlTable tblSummary = (HtmlTable)(e.Item.FindControl("tblSummary"));
                HtmlTableCell tdCategory = (HtmlTableCell)(e.Item.FindControl("tdCategory"));
                HtmlTableCell tdMailer = (HtmlTableCell)(e.Item.FindControl("tdMailer"));

                System.Web.UI.WebControls.Image imgFolderOpen = ((System.Web.UI.WebControls.Image)(e.Item.FindControl("imgParent")));
                System.Web.UI.WebControls.Image imgFolderClose = ((System.Web.UI.WebControls.Image)(e.Item.FindControl("imgChild")));
                System.Web.UI.WebControls.Image imgLink = ((System.Web.UI.WebControls.Image)(e.Item.FindControl("imgLink")));

                System.Web.UI.WebControls.TextBox txtToggle = ((System.Web.UI.WebControls.TextBox)(e.Item.FindControl("txtToggleImage")));

                // if current row is the court category header row........
                // hide the mailer type and add javascript function call on expand/collapse image
                if (IsGroup)
                {
                    tdCategory.Style["Display"] = "block";
                    tdMailer.Style["Display"] = "none";
                    imgLink.Style["display"] = "none";
                    tdCategory.Width = "60%";
                    tdMailer.Width = "0%";

                    ((Label)(e.Item.FindControl("lblTotalClients"))).Font.Bold = true;
                    ((Label)(e.Item.FindControl("lblTotalRev"))).Font.Bold = true;

                    imgFolderOpen.Attributes.Add("onclick", "ShowHideMailerDetail('" + txtToggle.ClientID + "');");
                    imgFolderClose.Attributes.Add("onclick", "ShowHideMailerDetail('" + txtToggle.ClientID + "');");
                }

                // if current row is the detail of the parent court category ........
                // hide the court category name & expand/collapse iamge. 
                else
                {
                    tdMailer.Style["Display"] = "block";
                    tdCategory.Style["Display"] = "none";
                    imgLink.Style["display"] = "block";
                    tdMailer.Width = "60%";
                    tdCategory.Width = "0%";

                    ((Label)(e.Item.FindControl("lblTotalClients"))).Font.Bold = false;
                    ((Label)(e.Item.FindControl("lblTotalRev"))).Font.Bold = false;
                }

            }
        }				



		
	}
}

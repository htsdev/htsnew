using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;

namespace lntechNew.Components
{
    public class ProjectSettings
    {
        #region Variables
        
        private string key;
        private string values;

        clsENationWebComponents cls = new clsENationWebComponents("Connection String");   
     
        #endregion

        #region Poperties

        public string Key
        {
            get
            {
                return key;
            }
            set
            {
                key = value ;
            }
        }
        

        public string Value
        {
            get
            {
                return values;
            }
            set
            {
                values = value ;
            }
        }
        #endregion

        #region Methods

        public void AddKeys()
        {
            string[] keys ={"@Key","@Value"};
            object[] val ={key, values};
            cls.ExecuteSP("USP_Add_Keys", keys, val);
        }

        public string GetValueFromKey()
        {
            string[] keys ={ "@key" };
            object[] val ={key};
            return Convert.ToString (cls.ExecuteScalerBySp("USP_Get_ValueFromKey", keys, val));
        }

        #endregion
    }
}

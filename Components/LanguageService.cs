﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using FrameWorkEnation.Components;

namespace HTP.Components
{
    public class LanguageService
    {

        #region Declaration

        clsENationWebComponents dbHelper = new clsENationWebComponents();

        #endregion

        /// <summary>
        ///     This method returns all the languages that are store in our database.
        /// </summary>
        /// <returns>DataTable with the list of Languages.(It may be list with no records)</returns>
        public DataTable GetAllLanguages()
        {
            return dbHelper.Get_DT_BySPArr("[dbo].[USP_HTP_GET_LANGUAGES]");
        }

        /// <summary>
        ///     This method insert language in database
        /// </summary>
        /// <param name="languageName">Name of Language to be added</param>
        /// <returns>(0 or 1) 0 means records not inserted in our database and 1 means records updated.</returns>
        public int InsertLanguage(string languageName)
        {
            if (!string.IsNullOrEmpty(languageName))
            {
                string[] key = { "@languagename" };
                object[] value = { languageName };
                return Convert.ToInt32(dbHelper.ExecuteScalerBySp("USP_HTP_INSERT_LANGUAGES", key, value));
            }
            else
                throw new ArgumentNullException("Language name not provided to insert.");
        }


    }
}

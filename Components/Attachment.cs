using System;
using System.Data;
using System.Data.SqlClient;
using FrameWorkEnation.Components;



namespace lntechNew.Components
{
	/// <summary>
	/// Summary description for Attachment.
    /// i have added 2 new methods 
    /// 1) RollbackAttachment for rollback if exception occurs 
    /// 2) GetAttachment for getting content of attachment
    /// 
    /// END FAHAD (11/15/2007)
	/// </summary>
    
	public class Attachment
	{
        clsENationWebComponents ClsDb = new clsENationWebComponents("TroubleTicketDB");
		
		#region Variables

		private string filename=String.Empty;
		private string description=String.Empty;
		private int size=0;
		private DateTime uploaddate;
		private string contenttype=String.Empty;
		private int bugid=0;
		private int employeeid=0;


			
		#endregion

		#region Properties
		public string FileName
		{ 
			get
			{ 
				return filename;
			}
			set
			{
				filename=value;
			}
		}
		public string Description
		{ 
			get
			{ 
				return description;
			}
			set
			{ 
				description=value;
			}
		}
		public int Size
		{ 
			get
			{ 
				return size;
			}
			set
			{ 
				size=value;
			}
		}

		public DateTime UploadDate
		{
			get
			{
				return uploaddate;
			}
			set
			{ 
				uploaddate=value;
			}
		}
		public string ContentType
		{ 
			get
			{ 
				return contenttype;
			}
			set
			{ 
				contenttype=value;
			}
		}
		public int EmployeeID
		{ 
			get
			{
				return employeeid;
			}
			set
			{ 
				employeeid=value;
			}
		}
		public int BugID
		{ 
			get
			{ 
				return bugid;
			}
			set
			{
				bugid=value;
			}
		}
			
		
		#endregion

		#region Methods
        public int AddNewAttachment(string FileName, string Description, int Size, DateTime UploadDate, string ContentType, int EmployeeID, int BugID)
		{ 
			int attachid=0;
			try
			{
				string[] key={"@FileName","@Description","@Size","@UploadDate","@ContentType","@EmployeeID","@BugID","@Attachmentid"};
				object[] value1={FileName,Description,Size,UploadDate,ContentType,EmployeeID,BugID,0};
				attachid=(int)ClsDb.InsertBySPArrRet("usp_bug_InsertNewAttachment",key,value1);
				return attachid;

			}
			catch(Exception ex)
			{ 
				return attachid;
			}

		}

        
        public void AddNewTestAttachment()
        {
            int attachid = 0;
            FileName = "Test.txt";
            Description = "Test Description";
            Size = 45;
            UploadDate = System.DateTime.Today;
            ContentType = "Test";
            EmployeeID = 3991;
            BugID = 2;

            string[] key ={"@FileName", "@Description", "@Size", "@UploadDate", "@ContentType", "@EmployeeID", "@BugID", "@Attachmentid" };
            object[] value1 ={ FileName, Description, Size, UploadDate, ContentType, EmployeeID, BugID, 0 };
            attachid = (int)ClsDb.InsertBySPArrRet("usp_bug_InsertNewAttachment", key, value1);
                
    
        }


		public void UpdateAttachment()
		{ 
			try
			{
				string[] key={"@FileName","@Description","@Size","@UploadDate","@ContentType","@EmployeeID","@BugID"} ;
				object[] value1={FileName,Description,Size,UploadDate,ContentType,EmployeeID,BugID};
				
				ClsDb.ExecuteSP("usp_Bug_UpdateAttachment",key,value1);
				
			}
			catch(Exception ex)
			{ 
				
			}
		}

        public DataSet GetAttachment(string attachmentid)
        {
            DataSet ds = new DataSet();
            try
            {
                string[] key ={ "@attachmentid" };
                object[] value1 ={attachmentid};

                ds = ClsDb.Get_DS_BySPArr("usp_Bug_getattachment_info", key, value1);
                return ds;

            }
            catch (Exception ex)
            {
                return ds;
            }
        }


        public void RollbackAttachment(int attachmentid , int bugid)
        {

            string[] key = { "@attachmentid , @bugid " };
                object[] value = { Convert.ToInt32(attachmentid),bugid };
                ClsDb.ExecuteSP("usp_hts_rollback_attachment", key, value);
     
          
        }
        
        public void UpdateTestAttachment( )
        {
            FileName = "TestFile";
            Description = "Test Description";
            Size = 78;
            UploadDate = System.DateTime.Today;
            ContentType = "TextFile";
            EmployeeID = 3991;
            BugID = 45;

            string[] key ={ "@FileName", "@Description", "@Size", "@UploadDate", "@ContentType", "@EmployeeID", "@BugID" };
            object[] value1 ={FileName,Description, Size, UploadDate, ContentType, EmployeeID, BugID };
            ClsDb.ExecuteSP("usp_Bug_UpdateAttachment", key, value1); 
        }
		#endregion

		public Attachment()
		{
			
		}
	}
}

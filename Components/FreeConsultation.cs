using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;


namespace lntechNew.Components
{
    public class FreeConsultation
    {
        clsENationWebComponents clsDb = new clsENationWebComponents("Connection String");
        clsLogger clog = new clsLogger();
        public DataTable DT= null;
        private object[] _values = null;

        #region Constructor

        public FreeConsultation()
        {
            
        }

        public FreeConsultation(object[] val)
        {
            this.Value = val;
        }

        #endregion

        #region Properties

        public object[] Value
        {
            get
            {
                return _values;
            }
            set
            {
                if (value == null || value.Length < 2)
                {
                    throw new ArgumentException("Parameter Value may not be null or blank");
                }
                _values=value;
            }
        }

        #endregion 

        #region Methods

        public DataTable GetData()
        {
            try
            {
                string[] key ={ "@startDate", "@endDate" };
                DT = clsDb.Get_DT_BySPArr("USP_HTS_GET_CONSULTATION_REPORT",key,this.Value);
                return DT; 
            }

            catch (Exception ex)
            {
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return DT;  
            }
        }

       

        #endregion 

    }
}

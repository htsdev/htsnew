﻿using System;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;

namespace HTP.Components
{
    /// <summary>
    /// Class ClsUpdateFollowUpInfo specially used to update follow up dates
    /// </summary>
    public class ClsUpdateFollowUpInfo
    {
        #region Variables
        private clsLogger objLogger;
        clsENationWebComponents ClsDb = new clsENationWebComponents("Connection String");
        #endregion

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public ClsUpdateFollowUpInfo()
        {
            objLogger = new clsLogger();
        }
        #endregion

        #region Methods

        //Waqas 5697 03/26/2009 For HMC-FTA Follow Up
        //Muhammad Ali 7747 07/08/2010 -->Add XML Comments for CHM Document Issuses..
        /// <summary>
        /// Update Follow Up Date
        /// </summary>
        /// <param name="ticketId">List of Ticket IDs in comma separted</param>
        /// <param name="empId">Employee ID</param>
        /// <param name="currentFollowUpDate">Current Follow Up Date</param>
        /// <param name="followUpType">Follow Up Type</param>
        /// <param name="followUpDate">New Follow Up Date</param>
        /// <param name="comments">Comments</param>
        /// <param name="freestate">Free State</param>
        /// <param name="ticketNo">Ticket Number</param>
        /// <param name="isUpdateAll">To Update All</param>
        /// <param name="isHMCFTARemoved">HMCFTA Removed</param>
        /// <returns>Return Success or Failure status</returns>
        public bool UpdateFollowUpDate(string ticketId, int empId, string currentFollowUpDate,
                                        string followUpType, DateTime followUpDate, string comments,
                                        string freestate, string ticketNo, bool isUpdateAll, bool isHMCFTARemoved)
        {
            // initialize return varaible
            bool isUpdatedSuccessfully = false;

            try
            {

                // Noufil 4980 10/30/2008 Update comments and bond waiting follow update for a client
                if (followUpType == FollowUpType.BondWaitingFollowUpDate.ToString())
                {
                    isUpdatedSuccessfully = UpdateBondWaitingFollowUpDate(Convert.ToInt32(ticketId), empId, currentFollowUpDate, followUpDate, comments);
                }
                // Zeeshan Zahoor 4481 08/06/2008 
                else if (followUpType == FollowUpType.BondFollowUpDate.ToString())
                {
                    isUpdatedSuccessfully = UpdateBondFollowUpDate(Convert.ToInt32(ticketId), empId, currentFollowUpDate, followUpDate, comments);
                }
                //Waqas 5653 03/21/2009 For No LOR Follow Up
                else if (followUpType == FollowUpType.LORFollowUpDate.ToString())
                {
                    isUpdatedSuccessfully = UpdateNoLORFollowUpDate(Convert.ToInt32(ticketId), empId, currentFollowUpDate, followUpDate, comments);
                }
                //Waqas 5697 03/26/2009 For HMC-FTA Follow Up
                else if (followUpType == FollowUpType.HMCFTAFollowUpDate.ToString())
                {
                    isUpdatedSuccessfully = UpdateHMCFTAFollowUpDate(Convert.ToInt32(ticketId), empId, currentFollowUpDate, followUpDate, comments, Convert.ToInt32(isHMCFTARemoved));
                }
                // Abid Ali 5018 12/24/2008 -- Family waiting follow up date
                // Abid Ali 4912 11/13/2008 -- Traffic waiting follow up date
                else if (followUpType == FollowUpType.TrafficWaitingFollowUpDate.ToString() ||
                         followUpType == FollowUpType.FamilyWaitingFollowUpDate.ToString())
                {
                    isUpdatedSuccessfully = UpdateTrafficWiatingFollowUpDate(Convert.ToInt32(ticketId), empId, currentFollowUpDate, followUpDate, comments);
                }
                //Fahad 5098 01/05/2009
                else if (followUpType == FollowUpType.ContractFollowUpDate.ToString())
                {
                    isUpdatedSuccessfully = UpdateContractFollowUpDate(Convert.ToInt32(ticketId), empId, currentFollowUpDate, followUpDate, comments);
                }
                //Fahad 5722 04/02/2009 Non NMC FollwUp Date 
                else if (followUpType == FollowUpType.NonHMCFollowUpDate.ToString())
                {
                    isUpdatedSuccessfully = UpdateNonHMCFollowUpDate(Convert.ToInt32(ticketId), empId, currentFollowUpDate, followUpDate, comments);
                }
                // Noufil 5691 04/06/2009 Update Past court date Followupdate for HMC clients
                else if (followUpType == FollowUpType.PastCourtDateHMCFollowupdate.ToString())
                {

                    isUpdatedSuccessfully = UpdatePastCourtdateFollowUpDate(Convert.ToInt32(ticketId), empId, currentFollowUpDate, followUpDate, comments, FollowUpType.PastCourtDateHMCFollowupdate);
                }
                else if (followUpType == FollowUpType.PastCourtDateNonHMCFollowupdate.ToString())
                {

                    isUpdatedSuccessfully = UpdatePastCourtdateFollowUpDate(Convert.ToInt32(ticketId), empId, currentFollowUpDate, followUpDate, comments, FollowUpType.PastCourtDateNonHMCFollowupdate);
                }
                //Sabir Khan 5977 07/02/2009 Update HMC Setting Discrepancy Follow Up Date...
                else if (followUpType == FollowUpType.HMCSettingDiscrepancyFollowUpDate.ToString())
                {
                    isUpdatedSuccessfully = UpdateHMCSettingDiscrepancyFollowUpDate(Convert.ToInt32(ticketId), empId, currentFollowUpDate, followUpDate, comments, FollowUpType.HMCSettingDiscrepancyFollowUpDate);
                }
                //Fahad 5753 04/10/2009 Split Case FollowUp Date
                else if (followUpType == FollowUpType.SplitCaseFollowUpDate.ToString())
                {
                    isUpdatedSuccessfully = UpdateSplitCaseFollowUpDate(Convert.ToInt32(ticketId), empId, currentFollowUpDate, followUpDate, comments);
                }
                else if (followUpType == FollowUpType.ALRHearingFollowUpdate.ToString())
                {
                    isUpdatedSuccessfully = UpdateALRHearingFollowUpDate(Convert.ToInt32(ticketId), empId, currentFollowUpDate, followUpDate, comments, freestate);
                }
                //Nasir 5938 05/25/2009 for NOS report
                else if (followUpType == FollowUpType.NOSFollowUpDate.ToString())
                {
                    clsNOS NOS = new clsNOS();
                    isUpdatedSuccessfully = NOS.Save(Convert.ToInt32(ticketId), empId, comments, followUpDate, currentFollowUpDate);
                }
                //Nasir 7234 01/08/2010 for HMC Same Day Arr.
                else if (followUpType == FollowUpType.HMCSameDayArrFollowUpDate.ToString())
                {
                    isUpdatedSuccessfully = UpdateHMCSameDayArrFollowUpDate(Convert.ToInt32(ticketId), empId, currentFollowUpDate, followUpDate, comments);
                }
                //Afaq 7752 05/04/2010 for Disposed Alert
                else if (followUpType == FollowUpType.DisposeAlert.ToString())
                {
                    isUpdatedSuccessfully = UpdateDisposeAlertFollowUpDate(Convert.ToInt32(ticketId), empId, currentFollowUpDate, followUpDate, comments);
                }
                //SAEED 7844 06/01/2010 check for Bad Email Address
                else if (followUpType == FollowUpType.BadEmailAddress.ToString())
                {
                    isUpdatedSuccessfully = UpdateBadEmailAddressFollowUpDate(Convert.ToInt32(ticketId), empId, currentFollowUpDate, followUpDate, comments);
                }//Muhammad Ali 7747 06/30/2010 --> Missing Cause Follow Update.
                else if (followUpType == FollowUpType.MissingCauseFollowUpdate.ToString())
                {
                    isUpdatedSuccessfully = UpdateMissingCauseFollowUpDate(Convert.ToInt32(ticketId), empId, currentFollowUpDate, followUpDate, comments);
                }
                //Saeed 8101 09/24/2010  HCJP Auto Alert Update Follow Up Date
                else if (followUpType == FollowUpType.HCJPAutoAlertFollowUpDate.ToString())
                {
                    isUpdatedSuccessfully = UpdateHCJPAutoAlertFollowUpDate(Convert.ToInt32(ticketId), empId, currentFollowUpDate, followUpDate, comments);
                }
                //Sabir Khan 8488 12/24/2010 Added if clause for bad addresses follo up date updating.
                else if (followUpType == FollowUpType.BadAddressFollowUpDate.ToString())
                {
                    isUpdatedSuccessfully = UpdateBadAddressesFollowUpDate(Convert.ToInt32(ticketId), empId, currentFollowUpDate, followUpDate, comments);
                }

                //Muhammad Nadir Siddiqui 9134 04/29/2011 
                else if (followUpType == FollowUpType.NoLORConfirmationFollowUpDate.ToString())
                {
                    isUpdatedSuccessfully = UpdateNoLORConfirmationFollowUpDate(Convert.ToInt32(ticketId), empId, currentFollowUpDate, followUpDate, comments);
                }

                else
                {
                    isUpdatedSuccessfully = UpdateArrWaitingFollowUpDate(ticketId, empId, currentFollowUpDate, followUpDate, comments, freestate, ticketNo, isUpdateAll);
                }


            }
            catch (Exception ex)
            {
                objLogger.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

            // return the values
            return isUpdatedSuccessfully;
        }
        //Muhammad Ali 7747 06/30/2010 --> Update Missing Cause Follow Update.
        /// <summary>
        /// Update Missing Cause Follow update.
        /// </summary>
        /// <param name="ticketId">Ticket Id</param>
        /// <param name="empId">Employ Id</param>
        /// <param name="currentFollowUpDate"> Current follow update</param>
        /// <param name="followUpDate">Follow update</param>
        /// <param name="comments">Comments</param>
        /// <returns>Return Success or Failure status</returns>
        public bool UpdateMissingCauseFollowUpDate(int ticketId, int empId, string currentFollowUpDate, DateTime followUpDate, string comments)
        {
            // initialize return varaible
            bool isUpdatedSuccessfully = false;

            try
            {
                currentFollowUpDate = currentFollowUpDate.Trim();
                clsCase Case = new clsCase();
                Case.EmpID = empId;
                Case.GeneralComments = comments.Trim();
                Case.TicketID = ticketId;
                Case.UpdateGeneralComments();

                string notes = string.Empty;
                if (currentFollowUpDate.Equals("N/A"))
                {
                    Case.UpdateMissingCauseFollowUpDate(ticketId, followUpDate, empId);
                    notes = "Missing Cause Follow Up Date has changed from N/A to " + followUpDate.ToShortDateString();
                    objLogger.AddNote(empId, notes, notes, ticketId);
                }
                else
                {
                    DateTime oldContractFollowUpDate;
                    if (DateTime.TryParse(currentFollowUpDate, out oldContractFollowUpDate))
                    {
                        //if (oldContractFollowUpDate.ToShortDateString() != followUpDate.ToShortDateString())
                        //{
                        Case.UpdateMissingCauseFollowUpDate(ticketId, followUpDate, empId);
                        notes = "Missing Cause Follow Up Date has changed from " + oldContractFollowUpDate.ToShortDateString() + " to " + followUpDate.ToShortDateString();
                        objLogger.AddNote(empId, notes, notes, ticketId);
                        //}
                    }
                }

                // set the updated flag
                isUpdatedSuccessfully = true;
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
            }

            // return the values
            return isUpdatedSuccessfully;
        }

        /// <summary>
        /// Update Array of Waiting Follow Up Date
        /// </summary>
        /// <param name="ticketId">List of Ticket IDs in comma separted</param>
        /// <param name="empId">Employee ID</param>
        /// <param name="currentFollowUpDate">Current Follow Up Date</param>
        /// <param name="followUpDate">New Follow Up Date</param>
        /// <param name="comments">Comments</param>
        /// <param name="freestate">Free State</param>
        /// <param name="ticketNo">Ticket id</param>
        /// <param name="isUpdateAll">To Update All</param>
        /// <returns></returns>
        public bool UpdateArrWaitingFollowUpDate(string ticketId, int empId, string currentFollowUpDate, DateTime followUpDate, string comments, string freestate, string ticketNo, bool isUpdateAll)
        {
            // initialize return varaible
            bool isUpdatedSuccessfully = false;
            try
            {
                string[] ticketids = ticketId.Split(',');
                currentFollowUpDate = currentFollowUpDate.Trim();
                comments = (comments == null ? string.Empty : comments.Trim());

                clsCase Case = new clsCase();
                Case.EmpID = empId;
                Case.GeneralComments = comments;

                //Kazim 3640 4/10/2008 Apply the logic of muliple updates togather  
                if (isUpdateAll)
                {
                    for (int a = 0; a < ticketids.Length - 1; a++)
                    {
                        Case.TicketID = Convert.ToInt32(ticketids[a]);
                        Case.UpdateGeneralComments();
                        //Zeeshan 4629 04/09/2008 Send Rep Id in the procedure.
                        Case.UpdateArrWaitingFollowUpDate(Case.TicketID, followUpDate, empId);
                        //Zeeshan 4629 04/09/2008 Avoid Inserting Blank Comments
                        if (!string.IsNullOrEmpty(comments))
                            objLogger.AddNote(empId, comments, comments, Case.TicketID);

                        isUpdatedSuccessfully = true;
                    }
                }
                else
                {
                    Case.TicketID = Convert.ToInt32(ticketNo);
                    Case.UpdateGeneralComments();

                    if (CanUpdateFollowUpDate(currentFollowUpDate, followUpDate))
                    {
                        if (freestate != string.Empty)
                        {
                            if (Convert.ToBoolean(freestate))
                            {
                                HccCriminalReport hccCrim = new HccCriminalReport();
                                // Noufil 3598 05/15/2008 To add Follow Update in Tbltickets table
                                hccCrim.UpdateFollowupdate(followUpDate, Case.TicketID);
                            }
                            else
                            {
                                //Zeeshan 4629 04/09/2008 Send Rep Id in the procedure.
                                Case.UpdateArrWaitingFollowUpDate(Case.TicketID, followUpDate, empId);
                            }

                            // Noufil 4175 06/06/2008 Adding Note in history
                            string description = "Criminal Follow Up Report : Follow Up Date has been changed to " + followUpDate.ToString("MM/dd/yyyy");
                            objLogger.AddNote(empId, description, "", Case.TicketID);
                        }
                        else
                        {
                            //Zeeshan 4629 04/09/2008 Send Rep Id in the procedure.
                            Case.UpdateArrWaitingFollowUpDate(Case.TicketID, followUpDate, empId);

                            //Zeeshan 4629 04/09/2008 Avoid Inserting Blank Comments
                            if (comments != string.Empty)
                                objLogger.AddNote(empId, comments, comments, Case.TicketID);
                        }
                    }

                    isUpdatedSuccessfully = true;
                }
            }
            catch (Exception ex)
            {
                objLogger.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

            // return the values
            return isUpdatedSuccessfully;
        }

        /// <summary>
        /// Update Bond Waiting Follow Up Date
        /// </summary>
        /// <param name="ticketId">Ticket ID</param>
        /// <param name="empId">Employee ID</param>
        /// <param name="currentFollowUpDate">Current Follow Up Date</param>
        /// <param name="followUpDate">New Follow Up Date</param>
        /// <param name="comments">Comments</param>
        /// <returns>Return Success or Failure status</returns>
        public bool UpdateBondWaitingFollowUpDate(int ticketId, int empId, string currentFollowUpDate, DateTime followUpDate, string comments)
        {
            // initialize return varaible
            bool isUpdatedSuccessfully = false;

            try
            {
                clsCase Case = new clsCase();
                Case.EmpID = empId;
                Case.GeneralComments = comments.Trim();
                Case.TicketID = ticketId;
                Case.UpdateGeneralComments();

                if (CanUpdateFollowUpDate(currentFollowUpDate, followUpDate))
                {
                    Case.UpdateBondWiatingFollowUpDate(ticketId, followUpDate, empId);
                    string description = "Bond waiting follow up date has been changed to " + followUpDate.ToString("MM/dd/yyyy");
                    objLogger.AddNote(empId, description, "", Case.TicketID);
                }

                // set the updated flag
                isUpdatedSuccessfully = true;
            }
            catch (Exception ex)
            {
                objLogger.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

            // return the values
            return isUpdatedSuccessfully;
        }

        /// <summary>
        /// Update Bond Follow UpDate
        /// </summary>
        /// <param name="ticketId">Ticket ID</param>
        /// <param name="empId">Employee ID</param>
        /// <param name="currentFollowUpDate">Current Follow Up Date</param>
        /// <param name="followUpDate">New Follow Up Date</param>
        /// <param name="comments">Comments</param>
        /// <returns>Return Success or Failure status</returns>
        public bool UpdateBondFollowUpDate(int ticketId, int empId, string currentFollowUpDate, DateTime followUpDate, string comments)
        {
            // initialize return varaible
            bool isUpdatedSuccessfully = false;

            try
            {
                clsCase Case = new clsCase();
                Case.EmpID = empId;
                Case.GeneralComments = comments.Trim();
                Case.TicketID = ticketId;
                Case.UpdateGeneralComments();

                if (CanUpdateFollowUpDate(currentFollowUpDate, followUpDate))
                    Case.UpdateBondFollowUpDate(ticketId, followUpDate, empId);//Muhammad Muneer 8285 9/17/2010 added the empId as the extra parameter

                // set the updated flag
                isUpdatedSuccessfully = true;
            }
            catch (Exception ex)
            {
                objLogger.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

            // return the values
            return isUpdatedSuccessfully;
        }

        /// <summary>
        /// Update Traffic Wiating Follow Up Date
        /// </summary>
        /// <param name="ticketId">Ticket ID</param>
        /// <param name="empId">Employee ID</param>
        /// <param name="currentFollowUpDate">Current Follow Up Date</param>
        /// <param name="followUpDate">New Follow Up Date</param>
        /// <param name="comments">Comments</param>
        /// <returns>Return Success or Failure status</returns>
        public bool UpdateTrafficWiatingFollowUpDate(int ticketId, int empId, string currentFollowUpDate, DateTime followUpDate, string comments)
        {
            // initialize return varaible
            bool isUpdatedSuccessfully = false;

            try
            {
                clsCase Case = new clsCase();
                Case.EmpID = empId;
                Case.GeneralComments = comments.Trim();
                Case.TicketID = ticketId;
                Case.UpdateGeneralComments();
                if (CanUpdateFollowUpDate(currentFollowUpDate, followUpDate))
                    Case.UpdateTrafficWiatingFollowUpDate(ticketId, followUpDate, empId);

                // set the updated flag
                isUpdatedSuccessfully = true;
            }
            catch (Exception ex)
            {
                objLogger.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

            // return the values
            return isUpdatedSuccessfully;
        }

        /// <summary>
        /// Update Contract Follow Up Date
        /// </summary>
        /// <param name="ticketId">Ticket ID</param>
        /// <param name="empId">Employee ID</param>
        /// <param name="currentFollowUpDate">Current Follow Up Date</param>
        /// <param name="followUpDate">New Follow Up Date</param>
        /// <param name="comments">Comments</param>
        /// <returns>Return Success or Failure status</returns>
        public bool UpdateContractFollowUpDate(int ticketId, int empId, string currentFollowUpDate, DateTime followUpDate, string comments)
        {
            // initialize return varaible
            bool isUpdatedSuccessfully = false;

            try
            {
                currentFollowUpDate = currentFollowUpDate.Trim();
                clsCase Case = new clsCase();
                Case.EmpID = empId;
                Case.GeneralComments = comments.Trim();
                Case.TicketID = ticketId;
                Case.UpdateGeneralComments();

                // Abid Ali 5425 1/20/2009 append "Contract" at the start in notes to show contract follow updation in history
                string notes = string.Empty;
                if (currentFollowUpDate.Length == 0)
                {
                    Case.UpdateContractFollowUpDate(ticketId, followUpDate, empId);
                    notes = "Contract Follow Up Date has changed from N/A to " + followUpDate.ToShortDateString();
                    objLogger.AddNote(empId, notes, notes, ticketId);
                }
                else
                {
                    DateTime oldContractFollowUpDate;
                    if (DateTime.TryParse(currentFollowUpDate, out oldContractFollowUpDate))
                    {
                        if (oldContractFollowUpDate.ToShortDateString() != followUpDate.ToShortDateString())
                        {
                            Case.UpdateContractFollowUpDate(ticketId, followUpDate, empId);
                            notes = "Contract Follow Up Date has changed from " + oldContractFollowUpDate.ToShortDateString() + " to " + followUpDate.ToShortDateString();
                            objLogger.AddNote(empId, notes, notes, ticketId);
                        }
                    }
                }

                // set the updated flag
                isUpdatedSuccessfully = true;
            }
            catch (Exception ex)
            {
                objLogger.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

            // return the values
            return isUpdatedSuccessfully;
        }

        /// <summary>
        /// Return true if dates are changed
        /// </summary>
        /// <param name="oldFollowUpDate">Old follow up date</param>
        /// <param name="newFollowUpDate">New follow up date</param>
        /// <returns>Return true or false</returns>
        private bool CanUpdateFollowUpDate(string oldFollowUpDate, DateTime newFollowUpDate)
        {
            bool isCanUpdate = false;
            try
            {
                oldFollowUpDate = oldFollowUpDate.Trim();
                if (oldFollowUpDate == string.Empty)
                {
                    isCanUpdate = true;
                }
                else
                {
                    DateTime oldContractFollowUpDate;
                    if (DateTime.TryParse(oldFollowUpDate, out oldContractFollowUpDate))
                    {
                        if (oldContractFollowUpDate.ToShortDateString() != newFollowUpDate.ToShortDateString())
                        {
                            isCanUpdate = true;
                        }
                    }
                }
            }
            catch
            {
                isCanUpdate = true;
            }

            // return value
            return isCanUpdate;
        }

        //Waqas 5653 03/21/2009
        /// <summary>
        /// Update NO LOR Follow Up Date
        /// </summary>
        /// <param name="ticketId">Ticket ID</param>
        /// <param name="empId">Employee ID</param>
        /// <param name="currentFollowUpDate">NO LOR Follow Up Date</param>
        /// <param name="followUpDate">New Follow Up Date</param>
        /// <param name="comments">Comments</param>
        /// <returns>Return Success or Failure status</returns>
        public bool UpdateNoLORFollowUpDate(int ticketId, int empId, string currentFollowUpDate, DateTime followUpDate, string comments)
        {
            // initialize return varaible
            bool isUpdatedSuccessfully = false;

            try
            {
                currentFollowUpDate = currentFollowUpDate.Trim();
                clsCase Case = new clsCase();
                Case.EmpID = empId;
                Case.GeneralComments = comments.Trim();
                Case.TicketID = ticketId;
                Case.UpdateGeneralComments();

                string notes = string.Empty;
                if (currentFollowUpDate.Length == 0)
                {
                    Case.UpdateNoLORFollowUpDate(ticketId, followUpDate, empId);
                    notes = "NO LOR Follow Up Date has changed from N/A to " + followUpDate.ToShortDateString();
                    objLogger.AddNote(empId, notes, notes, ticketId);
                }
                else
                {
                    DateTime oldContractFollowUpDate;
                    if (DateTime.TryParse(currentFollowUpDate, out oldContractFollowUpDate))
                    {
                        if (oldContractFollowUpDate.ToShortDateString() != followUpDate.ToShortDateString())
                        {
                            Case.UpdateNoLORFollowUpDate(ticketId, followUpDate, empId);
                            notes = "NO LOR Follow Up Date has changed from " + oldContractFollowUpDate.ToShortDateString() + " to " + followUpDate.ToShortDateString();
                            objLogger.AddNote(empId, notes, notes, ticketId);
                        }
                    }
                }

                // set the updated flag
                isUpdatedSuccessfully = true;
            }
            catch (Exception ex)
            {
                objLogger.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

            // return the values
            return isUpdatedSuccessfully;
        }

        //Muhammad Nadir Siddiqui 9134 04/29/2011
        /// <summary>
        /// Update NO LOR Confirmation Follow Up Date
        /// </summary>
        /// <param name="ticketId">Ticket ID</param>
        /// <param name="empId">Employee ID</param>
        /// <param name="currentFollowUpDate">NO LOR Follow Up Date</param>
        /// <param name="followUpDate">New Follow Up Date</param>
        /// <param name="comments">Comments</param>
        /// <returns>Return Success or Failure status</returns>
        public bool UpdateNoLORConfirmationFollowUpDate(int ticketId, int empId, string currentFollowUpDate, DateTime followUpDate, string comments)
        {
            // initialize return varaible
            bool isUpdatedSuccessfully = false;

            try
            {
                currentFollowUpDate = currentFollowUpDate.Trim();
                clsCase Case = new clsCase();
                Case.EmpID = empId;
                Case.GeneralComments = comments.Trim();
                Case.TicketID = ticketId;
                Case.UpdateGeneralComments();

                string notes = string.Empty;
                if (currentFollowUpDate.Length == 0)
                {
                    Case.UpdateNoLORConfirmationFollowUpDate(ticketId, followUpDate, empId);
                    notes = "NO LOR Confirmation Follow Up Date has changed from N/A to " + followUpDate.ToShortDateString();
                    objLogger.AddNote(empId, notes, notes, ticketId);
                }
                else
                {
                    DateTime oldContractFollowUpDate;
                    if (DateTime.TryParse(currentFollowUpDate, out oldContractFollowUpDate))
                    {
                        if (oldContractFollowUpDate.ToShortDateString() != followUpDate.ToShortDateString())
                        {
                            Case.UpdateNoLORConfirmationFollowUpDate(ticketId, followUpDate, empId);
                            notes = "NO LOR Confirmation Follow Up Date has changed from " + oldContractFollowUpDate.ToShortDateString() + " to " + followUpDate.ToShortDateString();
                            objLogger.AddNote(empId, notes, notes, ticketId);
                        }
                    }
                }

                // set the updated flag
                isUpdatedSuccessfully = true;
            }
            catch (Exception ex)
            {
                objLogger.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

            // return the values
            return isUpdatedSuccessfully;
        }





        //Waqas 5697 03/26/2009
        /// <summary>
        ///    
        /// Update HMC FTA Follow Up Date
        /// </summary>
        /// <param name="ticketId">Ticket ID</param>
        /// <param name="empId">Employee ID</param>
        /// <param name="currentFollowUpDate">HMC FTA Follow Up Date</param>
        /// <param name="followUpDate">New Follow Up Date</param>
        /// <param name="comments">Comments</param>
        /// <param name="IsHmcFTARemoved">Is HMC FTA Removed</param>
        /// <returns>Return Success or Failure status</returns>
        public bool UpdateHMCFTAFollowUpDate(int ticketId, int empId, string currentFollowUpDate, DateTime followUpDate, string comments, int IsHmcFTARemoved)
        {
            // initialize return varaible
            bool isUpdatedSuccessfully = false;

            try
            {
                currentFollowUpDate = currentFollowUpDate.Trim();
                clsCase Case = new clsCase();
                Case.EmpID = empId;
                Case.GeneralComments = comments.Trim();
                Case.TicketID = ticketId;
                Case.UpdateGeneralComments();

                string notes = string.Empty;
                if (currentFollowUpDate.Length == 0)
                {
                    Case.UpdateHMCFTAFollowUpDate(ticketId, followUpDate, empId, IsHmcFTARemoved);
                    notes = "HMC FTA Follow Up Date has changed from N/A to " + followUpDate.ToShortDateString();
                    objLogger.AddNote(empId, notes, notes, ticketId);
                }
                else
                {
                    DateTime oldContractFollowUpDate;
                    if (DateTime.TryParse(currentFollowUpDate, out oldContractFollowUpDate))
                    {
                        if (oldContractFollowUpDate.ToShortDateString() != followUpDate.ToShortDateString() || IsHmcFTARemoved == 1)
                        {
                            Case.UpdateHMCFTAFollowUpDate(ticketId, followUpDate, empId, IsHmcFTARemoved);

                            if (oldContractFollowUpDate.ToShortDateString() != followUpDate.ToShortDateString())
                            {
                                notes = "HMC FTA Follow Up Date has changed from " + oldContractFollowUpDate.ToShortDateString() + " to " + followUpDate.ToShortDateString();
                                objLogger.AddNote(empId, notes, notes, ticketId);
                            }
                            if (IsHmcFTARemoved == 1)
                            {
                                notes = "Client has been removed from HMC FTA Follow Up report";
                                objLogger.AddNote(empId, notes, notes, ticketId);
                            }
                        }
                    }
                }

                // set the updated flag
                isUpdatedSuccessfully = true;
            }
            catch (Exception ex)
            {
                objLogger.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

            // return the values
            return isUpdatedSuccessfully;
        }
        //Fahad 5722 03/21/2009
        /// <summary>
        /// Update Non HMC Follow Up Date
        /// </summary>
        /// 
        public bool UpdateNonHMCFollowUpDate(int ticketId, int empId, string currentFollowUpDate, DateTime followUpDate, string comments)
        {
            // initialize return varaible
            bool isUpdatedSuccessfully = false;

            try
            {
                currentFollowUpDate = currentFollowUpDate.Trim();
                clsCase Case = new clsCase();
                Case.EmpID = empId;
                Case.GeneralComments = comments.Trim();
                Case.TicketID = ticketId;
                Case.UpdateGeneralComments();

                string notes = string.Empty;
                if (currentFollowUpDate.Length == 0)
                {
                    Case.UpdateNonHMCFollowUpDate(ticketId, followUpDate, empId);
                    notes = "Non HMC Follow Up Date has changed from N/A to " + followUpDate.ToShortDateString();
                    objLogger.AddNote(empId, notes, notes, ticketId);
                }
                else
                {
                    DateTime oldContractFollowUpDate;
                    if (DateTime.TryParse(currentFollowUpDate, out oldContractFollowUpDate))
                    {
                        //if (oldContractFollowUpDate.ToShortDateString() != followUpDate.ToShortDateString())
                        //{
                        Case.UpdateNonHMCFollowUpDate(ticketId, followUpDate, empId);
                        notes = "Non HMC Follow Up Date has changed from " + oldContractFollowUpDate.ToShortDateString() + " to " + followUpDate.ToShortDateString();
                        objLogger.AddNote(empId, notes, notes, ticketId);
                        //}
                    }
                }

                // set the updated flag
                isUpdatedSuccessfully = true;
            }
            catch (Exception ex)
            {
                objLogger.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

            // return the values
            return isUpdatedSuccessfully;
        }
        ///Fahad 5753 04/10/2009
        /// <summary>
        ///Update Split Case Follow Up Date
        /// </summary>
        /// 
        public bool UpdateSplitCaseFollowUpDate(int ticketId, int empId, string currentFollowUpDate, DateTime followUpDate, string comments)
        {
            // initialize return varaible
            bool isUpdatedSuccessfully = false;

            try
            {
                currentFollowUpDate = currentFollowUpDate.Trim();
                clsCase Case = new clsCase();
                Case.EmpID = empId;
                Case.GeneralComments = comments.Trim();
                Case.TicketID = ticketId;
                Case.UpdateGeneralComments();

                string notes = string.Empty;
                if (currentFollowUpDate.Length == 0)
                {
                    Case.UpdateSplitCaseFollowUpDate(ticketId, followUpDate, empId);
                    notes = "Split Case Follow Up Date has changed from N/A to " + followUpDate.ToShortDateString();
                    objLogger.AddNote(empId, notes, notes, ticketId);
                }
                else
                {
                    DateTime oldContractFollowUpDate;
                    if (DateTime.TryParse(currentFollowUpDate, out oldContractFollowUpDate))
                    {
                        //if (oldContractFollowUpDate.ToShortDateString() != followUpDate.ToShortDateString())
                        //{
                        Case.UpdateSplitCaseFollowUpDate(ticketId, followUpDate, empId);
                        notes = "Split Case Follow Up Date has changed from " + oldContractFollowUpDate.ToShortDateString() + " to " + followUpDate.ToShortDateString();
                        objLogger.AddNote(empId, notes, notes, ticketId);
                        //}
                    }
                }

                // set the updated flag
                isUpdatedSuccessfully = true;
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
            }

            // return the values
            return isUpdatedSuccessfully;
        }

        //Saeed 8101 09/24/2010 Method created
        /// <summary>
        /// Update HCJP Auto Update Alert Follow Up Date
        /// </summary>
        /// <param name="ticketId">Ticket id</param>
        /// <param name="empId">Employee id</param>
        /// <param name="currentFollowUpDate">Current follow up date</param>
        /// <param name="followUpDate">Next follow up date</param>
        /// <param name="comments">User Comments</param>
        /// <returns>Returns true if record updated successfully else false</returns>
        public bool UpdateHCJPAutoAlertFollowUpDate(int ticketId, int empId, string currentFollowUpDate, DateTime followUpDate, string comments)
        {
            // initialize return varaible
            bool isUpdatedSuccessfully = false;

            try
            {
                currentFollowUpDate = currentFollowUpDate.Trim();
                clsCase Case = new clsCase();
                Case.EmpID = empId;
                Case.GeneralComments = comments.Trim();
                Case.TicketID = ticketId;
                Case.UpdateGeneralComments();

                string notes = string.Empty;
                if (currentFollowUpDate.Length == 0)
                {
                    Case.UpdateHCJPAutoAlertFollowUpDate(ticketId, followUpDate, empId);
                    notes = "HCJP Auto Update Alert Follow Up Date has changed from N/A to " + followUpDate.ToShortDateString();
                    objLogger.AddNote(empId, notes, notes, ticketId);
                }
                else
                {
                    DateTime oldContractFollowUpDate;
                    if (DateTime.TryParse(currentFollowUpDate, out oldContractFollowUpDate))
                    {
                        Case.UpdateHCJPAutoAlertFollowUpDate(ticketId, followUpDate, empId);
                        notes = "HCJP Auto Update Alert Follow Up Date has changed from " + oldContractFollowUpDate.ToShortDateString() + " to " + followUpDate.ToShortDateString();
                        objLogger.AddNote(empId, notes, notes, ticketId);

                    }
                }
                isUpdatedSuccessfully = true;
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
            }
            // return the values
            return isUpdatedSuccessfully;
        }

        //Noufil 5691 03/21/2009
        /// <summary>
        /// Update Past court date HMC client Follow Up Date
        /// </summary>
        /// 
        public bool UpdatePastCourtdateFollowUpDate(int ticketId, int empId, string currentFollowUpDate, DateTime followUpDate, string comments, FollowUpType ftype)
        {
            if (ticketId > 0 && empId > 0 && currentFollowUpDate != null && comments != null)
            {
                bool isUpdatedSuccessfully = false;
                try
                {
                    currentFollowUpDate = currentFollowUpDate.Trim();
                    clsCase Case = new clsCase();
                    Case.EmpID = empId;
                    Case.GeneralComments = comments.Trim();
                    Case.TicketID = ticketId;
                    Case.UpdateGeneralComments();
                    string text = "";
                    if (ftype == FollowUpType.PastCourtDateHMCFollowupdate)
                        text = "Past Court Date HMC";
                    else
                        text = "Past Court Date Non HMC";

                    string notes = string.Empty;
                    if (currentFollowUpDate.Length == 0)
                    {
                        Case.UpdatePastcourtHMCFollowUpDate(ticketId, followUpDate, empId);
                        notes = text + " Follow Up Date has been changed from N/A to " + followUpDate.ToShortDateString();
                        objLogger.AddNote(empId, notes, notes, ticketId);
                    }
                    else
                    {
                        DateTime oldContractFollowUpDate;
                        if (DateTime.TryParse(currentFollowUpDate, out oldContractFollowUpDate))
                        {
                            if (oldContractFollowUpDate.ToShortDateString() != followUpDate.ToShortDateString())
                            {
                                Case.UpdatePastcourtHMCFollowUpDate(ticketId, followUpDate, empId);
                                notes = text + " Follow Up Date has been changed from " + oldContractFollowUpDate.ToShortDateString() + " to " + followUpDate.ToShortDateString();
                                objLogger.AddNote(empId, notes, notes, ticketId);
                            }
                        }
                    }
                    // set the updated flag
                    isUpdatedSuccessfully = true;
                }
                catch (Exception ex)
                {
                    clsLogger.ErrorLog(ex);
                }

                // return the values
                return isUpdatedSuccessfully;
            }
            else
                throw new ArgumentNullException("Aurguments cannot be null");
        }


        //Sabir Khan 5977 07/02/2009 
        /// <summary>
        /// Update HMC Setting Discrepancy Follow Up Date
        /// </summary>
        /// 
        public bool UpdateHMCSettingDiscrepancyFollowUpDate(int ticketId, int empId, string currentFollowUpDate, DateTime followUpDate, string comments, FollowUpType ftype)
        {
            if (ticketId > 0 && empId > 0 && currentFollowUpDate != null && comments != null)
            {
                bool isUpdatedSuccessfully = false;
                try
                {
                    currentFollowUpDate = currentFollowUpDate.Trim();
                    clsCase Case = new clsCase();
                    Case.EmpID = empId;
                    Case.GeneralComments = comments.Trim();
                    Case.TicketID = ticketId;
                    Case.UpdateGeneralComments();
                    string notes = string.Empty;
                    if (currentFollowUpDate.Length == 0)
                    {

                        string[] key = { "@ticketid", "@followupdate" };
                        object[] value1 = { ticketId, followUpDate };
                        ClsDb.ExecuteSP("USP_HTP_Update_HMCSettingDiscrepancyFollowUpDate", key, value1);
                        notes = "HMC Setting Discrepancy Follow Up Date has been changed from N/A to " + followUpDate.ToShortDateString();
                        objLogger.AddNote(empId, notes, notes, ticketId);
                    }
                    else
                    {
                        DateTime oldContractFollowUpDate;
                        if (DateTime.TryParse(currentFollowUpDate, out oldContractFollowUpDate))
                        {
                            if (oldContractFollowUpDate.ToShortDateString() != followUpDate.ToShortDateString())
                            {
                                string[] key = { "@TicketID_PK", "@FollowUpDate" };
                                object[] value1 = { ticketId, followUpDate };
                                ClsDb.ExecuteSP("USP_HTP_Update_HMCSettingDiscrepancyFollowUpDate", key, value1);
                                notes = "HMC Setting Discrepancy Follow Up Date has been changed from " + oldContractFollowUpDate.ToShortDateString() + " to " + followUpDate.ToShortDateString();
                                objLogger.AddNote(empId, notes, notes, ticketId);
                            }
                        }
                    }
                    // set the updated flag
                    isUpdatedSuccessfully = true;
                }
                catch (Exception ex)
                {
                    clsLogger.ErrorLog(ex);
                }

                // return the values
                return isUpdatedSuccessfully;
            }
            else
                throw new ArgumentNullException("Aurguments cannot be null");
        }




        //Noufil 5819 05/13/2009
        /// <summary>
        ///         Update Alr HEaring Follow update
        /// </summary>
        /// 
        public bool UpdateALRHearingFollowUpDate(int ticketId, int empId, string currentFollowUpDate, DateTime followUpDate, string comments, string subdoctype)
        {
            if (ticketId > 0 && empId > 0 && currentFollowUpDate != null && comments != null)
            {
                bool isUpdatedSuccessfully = false;
                try
                {
                    currentFollowUpDate = currentFollowUpDate.Trim();
                    clsCase Case = new clsCase();
                    Case.EmpID = empId;
                    Case.GeneralComments = comments.Trim();
                    Case.TicketID = ticketId;
                    Case.UpdateGeneralComments();

                    string[] key = { "@ticketid", "@SubDocTypeID", "@empid", "@followupdate" };
                    object[] value1 = { ticketId, Convert.ToInt32(subdoctype), empId, followUpDate };
                    ClsDb.ExecuteSP("USP_HTP_UPDATE_ALR_FOLLOWUPDATE", key, value1);

                    isUpdatedSuccessfully = true;
                }
                catch (Exception ex)
                {
                    clsLogger.ErrorLog(ex);
                }

                // return the values
                return isUpdatedSuccessfully;
            }
            else
                throw new ArgumentNullException("Aurguments cannot be null");
        }

        //Nasir 7234 01/08/2010 HMC Same Day Arr.
        /// <summary>
        /// Update HMC Same Day Arr. Follow up date
        /// </summary>
        /// <param name="ticketId">Case ID</param>
        /// <param name="empId">Employee ID</param>
        /// <param name="currentFollowUpDate">current Follow Up Date</param>
        /// <param name="followUpDate">New follow Up Date</param>
        /// <param name="comments"> Comments set when update follow up</param>
        /// <returns>bool updated successfully or not</returns>
        public bool UpdateHMCSameDayArrFollowUpDate(int ticketId, int empId, string currentFollowUpDate, DateTime followUpDate, string comments)
        {
            if (ticketId > 0 && empId > 0 && currentFollowUpDate != null && comments != null)
            {
                bool isUpdatedSuccessfully = false;
                try
                {
                    currentFollowUpDate = currentFollowUpDate.Trim();
                    clsCase Case = new clsCase();
                    Case.EmpID = empId;
                    Case.GeneralComments = comments.Trim();
                    Case.TicketID = ticketId;
                    Case.UpdateGeneralComments();

                    string[] key = { "@FollowUpDate", "@TicketID", "empid" };
                    object[] value1 = { followUpDate, ticketId, empId };
                    ClsDb.ExecuteSP("USP_HTP_Update_HMCSameDayArrFollowUpDate", key, value1);

                    isUpdatedSuccessfully = true;
                }
                catch (Exception ex)
                {
                    clsLogger.ErrorLog(ex);
                }

                // return the values
                return isUpdatedSuccessfully;
            }
            else
                throw new ArgumentNullException("Aurguments cannot be null");
        }

        // 7752 05/004/2010 Afaq Update followup date of disposed alert.
        /// <summary>
        /// Update followup date of disposed alert.
        /// </summary>
        /// <param name="ticketId"></param>
        /// <param name="empId"></param>
        /// <param name="currentFollowUpDate"></param>
        /// <param name="followUpDate"></param>
        /// <param name="comments"></param>
        /// <returns></returns>
        public bool UpdateDisposeAlertFollowUpDate(int ticketId, int empId, string currentFollowUpDate, DateTime followUpDate, string comments)
        {
            // initialize return varaible
            bool isUpdatedSuccessfully = false;

            try
            {
                clsCase Case = new clsCase();
                Case.EmpID = empId;
                Case.GeneralComments = comments.Trim();
                Case.TicketID = ticketId;
                Case.UpdateGeneralComments();
                // Afaq 7752 05/21/2010 Add Comments in client’s case history on follow update change.
                if (currentFollowUpDate.Length == 0)
                {
                    Case.UpdateDisposedAlertFollowUpDate(ticketId, followUpDate, empId);
                    string description = "Disposed Alert follow up date has changed from N/A to " + followUpDate.ToString("MM/dd/yyyy");
                    objLogger.AddNote(empId, description, "", Case.TicketID);
                }
                // Afaq 7752 05/28/2010 replace if with else if.Else condition will run when current follw up date is not null.
                else if (CanUpdateFollowUpDate(currentFollowUpDate, followUpDate))
                {
                    Case.UpdateDisposedAlertFollowUpDate(ticketId, followUpDate, empId);
                    string description = "Disposed Alert follow up date has changed from " + Convert.ToDateTime(currentFollowUpDate).ToString("MM/dd/yyyy") + " to " + followUpDate.ToString("MM/dd/yyyy");
                    objLogger.AddNote(empId, description, "", Case.TicketID);
                }
                // set the updated flag
                isUpdatedSuccessfully = true;
            }
            catch (Exception ex)
            {
                objLogger.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

            // return the values
            return isUpdatedSuccessfully;
        }

        /// <summary>
        /// This method updates followup date for bad email address.
        /// </summary>
        /// <param name="ticketId">Represents TicketID whose follow up date need to be updated.</param>
        /// <param name="empId">Represents EmployeeID which need to be set</param>
        /// <param name="currentFollowUpDate">Represents current follow up date.</param>
        /// <param name="followUpDate">Represents followup date for bad email address which need to be set</param>
        /// <param name="comments">Represents user comments which need to be set</param>
        /// <returns></returns>
        public bool UpdateBadEmailAddressFollowUpDate(int ticketId, int empId, string currentFollowUpDate, DateTime followUpDate, string comments)
        {
            //SAEED 7844 06/23/2010 method created.
            bool isUpdatedSuccessfully = false;
            try
            {
                clsCase Case = new clsCase();
                Case.EmpID = empId;
                Case.GeneralComments = comments.Trim();
                Case.TicketID = ticketId;
                Case.UpdateGeneralComments();
                if (currentFollowUpDate.Length == 0)
                {
                    Case.UpdateBadEmailAddressFollowupDate(ticketId, followUpDate, empId);
                    string description = "Bad Email Follow Up Date has changed from N/A to " + followUpDate.ToString("MM/dd/yyyy");
                    objLogger.AddNote(empId, description, "", Case.TicketID);
                }
                else if (CanUpdateFollowUpDate(currentFollowUpDate, followUpDate))
                {
                    Case.UpdateBadEmailAddressFollowupDate(ticketId, followUpDate, empId);
                    string description = "Bad Email Follow Up Date has changed from " + Convert.ToDateTime(currentFollowUpDate).ToString("MM/dd/yyyy") + " to " + followUpDate.ToString("MM/dd/yyyy");
                    objLogger.AddNote(empId, description, "", Case.TicketID);
                }
                isUpdatedSuccessfully = true;
            }
            catch (Exception ex)
            {
                objLogger.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
            return isUpdatedSuccessfully;
        }

        /// <summary>
        /// This method updates followup date for address validation.
        /// </summary>
        /// <param name="contactID">Contact Id.</param>
        /// <param name="ticketId">Represents TicketID whose follow up date need to be updated.</param>
        /// <param name="empId">Represents EmployeeID which need to be set</param>
        /// <param name="currentFollowUpDate">Represents current follow up date.</param>
        /// <param name="followUpDate">Represents followup date for bad email address which need to be set</param>
        /// <param name="comments">Represents user comments which need to be set</param>
        /// <returns></returns>
        public bool UpdateAddressValidationFollowUpDate(int contactID, int ticketId, int empId, string currentFollowUpDate, DateTime followUpDate, string comments)
        {
            //SAEED 7844 06/23/2010 method created.
            bool isUpdatedSuccessfully = false;
            try
            {
                clsCase Case = new clsCase();
                Case.EmpID = empId;
                Case.GeneralComments = comments.Trim();
                Case.TicketID = ticketId;
                Case.Contact_ID = contactID;
                Case.UpdateGeneralComments();
                if (currentFollowUpDate.Length == 0)
                {
                    Case.UpdateAddressValidationReportFollowupDate(contactID, ticketId, followUpDate, empId);
                    string description = "Address Validation Follow Up Date has changed from N/A to " + followUpDate.ToString("MM/dd/yyyy");
                    objLogger.AddNote(contactID, empId, description, "", Case.TicketID);
                }
                else if (CanUpdateFollowUpDate(currentFollowUpDate, followUpDate))
                {
                    Case.UpdateAddressValidationReportFollowupDate(contactID, ticketId, followUpDate, empId);
                    string description = "Address Validation Follow Up Date has changed from " + Convert.ToDateTime(currentFollowUpDate).ToString("MM/dd/yyyy") + " to " + followUpDate.ToString("MM/dd/yyyy");
                    objLogger.AddNote(contactID, empId, description, "", Case.TicketID);
                }
                isUpdatedSuccessfully = true;
            }
            catch (Exception ex)
            {
                objLogger.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
            return isUpdatedSuccessfully;
        }

        //Sabir Khan 8488 12/24/2010 Method created
        /// <summary>
        /// Update Bad Addresses Follow Up Date
        /// </summary>
        /// <param name="ticketId">Ticket id</param>
        /// <param name="empId">Employee id</param>
        /// <param name="currentFollowUpDate">Current follow up date</param>
        /// <param name="followUpDate">Next follow up date</param>
        /// <param name="comments">User Comments</param>
        /// <returns>Returns true if record updated successfully else false</returns>
        public bool UpdateBadAddressesFollowUpDate(int ticketId, int empId, string currentFollowUpDate, DateTime followUpDate, string comments)
        {
            // initialize return varaible
            bool isUpdatedSuccessfully = false;

            try
            {
                currentFollowUpDate = currentFollowUpDate.Trim();
                clsCase Case = new clsCase();
                Case.EmpID = empId;
                Case.GeneralComments = comments.Trim();
                Case.TicketID = ticketId;
                Case.UpdateGeneralComments();

                string notes = string.Empty;
                if (currentFollowUpDate.Length == 0)
                {
                    Case.UpdateBadAddressesFollowUpDate(ticketId, followUpDate, empId);
                    notes = "Bad Address Follow Up Date has changed from N/A to " + followUpDate.ToShortDateString();
                    objLogger.AddNote(empId, notes, notes, ticketId);
                }
                else
                {
                    DateTime oldContractFollowUpDate;
                    if (DateTime.TryParse(currentFollowUpDate, out oldContractFollowUpDate))
                    {
                        Case.UpdateBadAddressesFollowUpDate(ticketId, followUpDate, empId);
                        notes = "Bad Address Follow Up Date has changed from " + oldContractFollowUpDate.ToShortDateString() + " to " + followUpDate.ToShortDateString();
                        objLogger.AddNote(empId, notes, notes, ticketId);

                    }
                }
                isUpdatedSuccessfully = true;
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
            }
            // return the values
            return isUpdatedSuccessfully;
        }


        // Rab Nawaz 10729 04/15/2013 Nisi followup date has been added. . . 
        ///<summary>
        ///</summary>
        ///<param name="ticketId"></param>
        ///<param name="empId"></param>
        ///<param name="currentFollowUpDate"></param>
        ///<param name="newfollowUpDate"></param>
        ///<param name="comments"></param>
        ///<param name="contactTypeId"></param>
        ///<param name="notes"></param>
        ///<returns></returns>
        public bool UpdateNisiFollowUpDate(int ticketId, int empId, string currentFollowUpDate, DateTime newfollowUpDate, string comments, int contactTypeId, string notes, bool isFreezed)
        {
            // initialize return varaible
            bool isUpdatedSuccessfully = false;

            try
            {
                clsCase Case = new clsCase();
                Case.EmpID = empId;
                Case.GeneralComments = comments.Trim();
                Case.TicketID = ticketId;
                Case.UpdateGeneralComments();
                Case.UpdateNisiFollowUpDate(ticketId, newfollowUpDate, empId, contactTypeId, notes, isFreezed);

                // set the updated flag
                isUpdatedSuccessfully = true;
            }
            catch (Exception ex)
            {
                objLogger.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

            // return the values
            return isUpdatedSuccessfully;
        }

        #endregion
    }
}

﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using HTP.Components;

//Waqas 7077 12/14/2009
namespace HTP.Components
{
    /// <summary>
    /// This class is used to modify the menu items.
    /// </summary>
    public class MenuSettings
    {
        /// <summary>
        /// This object is used to call database for changes in menus.
        /// </summary>
        clsENationWebComponents clsDB = new clsENationWebComponents();
        
        /// <summary>
        /// This object is used to log messages in DB
        /// </summary>
        clsLogger LogDB = new clsLogger();
        
        /// <summary>
        /// This method is used to get menu items to be filled in Tree
        /// </summary>
        /// <param name="LevelID">It represents the LevelID (1 for First Level (Main Menu), 2 for Second Level (Sub Menu) </param>
        /// <param name="MenuID">It represents the Menu Id of menu item</param>
        /// <returns>It returns the dateset holding all menu item required</returns>
        public DataSet GetMenuItems(int LevelID, int MenuID)
        {
            DataSet ds = null;
            try
            {
                string[] key = { "@LevelID", "@MENUID" };
                object[] obj = { LevelID, MenuID };
                ds = clsDB.Get_DS_BySPArr("usp_htp_get_MenuItemsAll", key, obj);
            }
            catch (Exception ex)
            {
                LogDB.ErrorLog(ex.Message.ToString(), ex.Source.ToString(), ex.TargetSite.ToString(), ex.StackTrace.ToString());
                ds = null;
            }
            return ds;
        }

        /// <summary>
        /// This method is used to get menu items from DB
        /// </summary>
        /// <returns>It returns dataset with menu items</returns>
        public DataSet GetMenuData()
        {
            DataSet ds = null;
            try
            {
                ds = clsDB.Get_DS_BySP("usp_tab_gettopmenu");               
            }
            catch (Exception ex)
            {
                LogDB.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                ds = null; ;
            }
            return ds;
        }

        /// <summary>
        /// This method is used to get sub menu items from DB
        /// </summary>
        /// <returns>It returns dataset with sub menu items</returns>
        public DataSet GetSubMenuData()
        {
            DataSet ds = null;
            try
            {
                ds = clsDB.Get_DS_BySP("USP_TAB_GETSUBMENU_by_MENUID");
            }
            catch (Exception ex)
            {
                LogDB.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                ds = null;
            }
            return ds;
        }

        /// <summary>
        /// Zeeshan Haider 11387 09/09/2013 Added access to configuration setting report page
        /// This method is used to get menu items from DB
        /// </summary>
        /// <returns>It returns dataset with menu items</returns>
        public DataSet GetMenuData(string sUserName)
        {
            DataSet ds;
            try
            {
                if (ConfigurationManager.AppSettings["ConfigurationSettingReportAccess"].Contains(sUserName.Trim()))
                {
                    string[] key = { "@sUserName" };
                    object[] value = { sUserName };
                    ds = clsDB.Get_DS_BySPArr("USP_TAB_GET_CUSTOM_TOP_SUB_MENU", key, value);
                    ds.Tables.Remove("Table1");
                }
                else
                    ds = clsDB.Get_DS_BySP("usp_tab_gettopmenu");
            }
            catch (Exception ex)
            {
                LogDB.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                ds = null; ;
            }
            return ds;
        }

        /// <summary>
        /// Zeeshan Haider 11387 09/09/2013 Added access to configuration setting report page
        /// This method is used to get sub menu items from DB
        /// </summary>
        /// <returns>It returns dataset with sub menu items</returns>
        public DataSet GetSubMenuData(string sUserName)
        {
            DataSet ds;
            try
            {
                if (ConfigurationManager.AppSettings["ConfigurationSettingReportAccess"].Contains(sUserName.Trim()))
                {
                    string[] key = { "@sUserName" };
                    object[] value = { sUserName };
                    ds = clsDB.Get_DS_BySPArr("USP_TAB_GET_CUSTOM_TOP_SUB_MENU", key, value);
                    ds.Tables.Remove("Table");
                }
                else
                    ds = clsDB.Get_DS_BySP("USP_TAB_GETSUBMENU_by_MENUID");
            }
            catch (Exception ex)
            {
                LogDB.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                ds = null;
            }
            return ds;
        }

        /// <summary>
        /// This method is used to add menu items in first level
        /// </summary>
        /// <param name="LevelID">It represents the Level of the Menu</param>
        /// <param name="Title">It represents the title of menu item</param>
        /// <param name="IsActive">It represents that menu item is active or not.</param>
       /// <param name="IsSubMenuHidden">need to hide submenu or not</param>
        public void AddFirstLevelMenuItems(int LevelID, string Title, int IsActive,bool IsSubMenuHidden)
        {
            try
            {
                string[] key = { "@LevelID", "@Title", "@IsActive", "@IsSubMenuHidden" };
                object[] obj = { LevelID, Title, IsActive, IsSubMenuHidden };
                clsDB.InsertBySPArr("usp_htp_Add_NewMenuItems", key, obj);
            }
            catch (Exception ex)
            {
                LogDB.ErrorLog(ex.Message.ToString(), ex.Source.ToString(), ex.TargetSite.ToString(), ex.StackTrace.ToString());
            }            
        }

        /// <summary>
        /// This method is used to add menu items in second level
        /// </summary>
        /// <param name="LevelID">It represents the Level of the Menu</param>
        /// <param name="Title">It represents the title of menu item</param>
        /// <param name="IsActive">It represents that menu item is active or not.</param>
        /// <param name="ParentMenuID">It representst the parent menu id</param>
        /// <param name="URL">It represents the url of the menu item</param>
        /// <param name="IsSelected">It represents that menu item is selected or not</param>
        /// <param name="IsLogging">It represents that admin log is allowed or not</param>
        /// <param name="Category">Represents report category, possible values for category are: Alerts=1, Reports=2</param>
        public void AddSecondLevelMenuItems(int LevelID, string Title, int IsActive, int ParentMenuID, string URL, int IsSelected, int IsLogging, int Category)  //SAEED 7791 05/25/2010 new parameter for report category added.
        {
            try
            {
                string[] key = { "@LevelID", "@Title", "@IsActive", "@ParentMenuID", "@URL", "@IsSelected", "@IsAdminLog", "@Category" };   //SAEED 7791 05/25/2010 new parameter for report category added.
                object[] obj = { LevelID, Title, IsActive, ParentMenuID, URL, IsSelected, IsLogging, Category };    //SAEED 7791 05/25/2010 new parameter for report category added.
                clsDB.InsertBySPArr("usp_htp_Add_NewMenuItems", key, obj);
            }
            catch (Exception ex)
            {
                LogDB.ErrorLog(ex.Message.ToString(), ex.Source.ToString(), ex.TargetSite.ToString(), ex.StackTrace.ToString());
            }
        }

        /// <summary>
        /// This method is used to update menu items in first level
        /// </summary>
        /// <param name="MenuID">It represents the Id of the menu item</param>
        /// <param name="LevelID">It represents the Level of the Menu</param>
        /// <param name="Title">It represents the title of menu item</param>
        /// <param name="IsActive">It represents that menu item is active or not.</param>
        /// <param name="OrderedIDs">It represents the comma seperated IDs to be ordered</param>
        /// <param name="IsSubMenuHidden">need to hide submenu or not</param>
        /// <param name="Category">Represents report category, possible values for category are: Alerts=1, Reports=2</param>
        public void UpdateFirstLevelMenuItem(int MenuID, int LevelID, string Title, int IsActive, string OrderedIDs, bool IsSubMenuHidden, int Category)  //SAEED 7791 05/25/2010 new parameter for report category added.
        {
            try
            {
                string[] key = { "@ID", "@LevelID", "@Title", "@IsActive", "@OrderedIDs", "@IsSubMenuHidden", "@Category" };    //SAEED 7791 05/25/2010 new parameter for report category added.
                object[] obj = { MenuID, LevelID, Title, IsActive, OrderedIDs, IsSubMenuHidden, Category }; //SAEED 7791 05/25/2010 new field added for report category.
                clsDB.InsertBySPArr("usp_htp_update_MenuItems", key, obj);  
            }
            catch (Exception ex)
            {
                LogDB.ErrorLog(ex.Message.ToString(), ex.Source.ToString(), ex.TargetSite.ToString(), ex.StackTrace.ToString());
            }  
        }

        /// <summary>
        /// This method is used to update menu items in second level
        /// </summary>
        /// <param name="MenuID">It represents the Id of the menu item</param>
        /// <param name="LevelID">It represents the Level of the Menu</param>
        /// <param name="Title">It represents the title of menu item</param>
        /// <param name="IsActive">It represents that menu item is active or not.</param>
        /// <param name="OrderedIDs">It represents the comma seperated IDs to be ordered</param>
        /// <param name="URL">It represents the url of the menu item</param>
        /// <param name="IsSelected">It represents that menu item is selected or not</param>
        /// <param name="IsLogging">It represents that admin log is allowed or not</param>
        /// <param name="Category">Represents report category, possible values for category are: Alerts=1, Reports=2</param>
        public void UpdateSecondLevelMenuItem(int MenuID, int LevelID, string Title, int IsActive, string OrderedIDs, string URL, int IsSelected, int IsLogging, int Category)   //SAEED 7791 05/25/2010 new parameter for report category added.
        {
            try
            {
                string[] key = { "@ID", "@LevelID", "@Title", "@IsActive", "@OrderedIDs", "@URL", "@IsSelected", "@IsAdminLog", "@Category" };   //SAEED 7791 05/25/2010 new parameter for report category added.
                object[] obj = { MenuID, LevelID, Title, IsActive, OrderedIDs, URL, IsSelected, IsLogging, Category }; //SAEED 7791 05/25/2010 new field added for report category.
                clsDB.InsertBySPArr("usp_htp_update_MenuItems", key, obj);
            }
            catch (Exception ex)
            {
                LogDB.ErrorLog(ex.Message.ToString(), ex.Source.ToString(), ex.TargetSite.ToString(), ex.StackTrace.ToString());
            }
        }
        
        /// <summary>
        /// This method returns true if the parent menu of the supplied submenu is of type 'VALIDATION', otherwise it returns false.
        /// </summary>
        /// <param name="menuID">Parent menu id</param>
        /// <returns></returns>
        public bool CheckIfValidationMenu(int menuID)
        {
            //SAEED 7791 05/25/2010 method created.
            DataSet ds=null;
            bool flag = false;
            try
            {
                string[] key = { "@MENUID" };
                object[] obj = { menuID };
                ds = clsDB.Get_DS_BySPArr("USP_HTP_ParentMenuIsValidation", key, obj);
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        flag = Convert.ToBoolean(ds.Tables[0].Rows[0]["isValidationMenu"]);
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                LogDB.ErrorLog(ex.Message.ToString(), ex.Source.ToString(), ex.TargetSite.ToString(), ex.StackTrace.ToString());
            }
            return flag;
        }

    }
  
}
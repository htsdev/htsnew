using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Principal;
using System.Reflection;

//5419 Waqas 02/03/2009 New file added for impersonation
namespace lntechNew.Components
{
    class Impersonation
    {
        [DllImport("advapi32.dll")]
        public static extern int LogonUser(String lpszUsername, String lpszDomain,
        String lpszPassword,
        int dwLogonType, int dwLogonProvider, ref IntPtr phToken);

        [DllImport("kernel32.dll")]
        public extern static bool CloseHandle(IntPtr hToken);

        public bool Impersonate(string userName, string domain, string password)
        {
            WindowsIdentity tempWindowsIdentity;
            IntPtr token = IntPtr.Zero;
            IntPtr tokenDuplicate = IntPtr.Zero;
            // request default security provider a logon token with
            //LOGON32_LOGON_NEW_CREDENTIALS

            
            // token returned is impersonation token, no need to duplicate
            if(LogonUser(userName, domain, password, 9, 0, ref token) != 0)
            {
                tempWindowsIdentity = new WindowsIdentity(token);
                impersonationContext = tempWindowsIdentity.Impersonate();
                // close impersonation token, no longer needed
                CloseHandle(token);
                if (impersonationContext != null)
                return true;
            }
            return false; // Failed to impersonate.
        }
        internal WindowsImpersonationContext impersonationContext;
    }
}

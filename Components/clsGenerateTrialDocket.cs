using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Configuration.Helper;
using FrameWorkEnation.Components;
using HTP.Components.Services;
using WebSupergoo.ABCpdf6;
using WebSupergoo.ABCpdf6.Objects;
using WebSupergoo.ABCpdf6.Atoms;
using lntechNew.Components.ClientInfo;


namespace lntechNew.Components
{
    public class clsGenerateTrialDocket
    {

        string courtLoc;
        DateTime courtDate;
        string pageNo;
        string courtNumber;
        string dateType;
        string Singles;
        //For Trial Docket Count
        Doc genFile = new Doc();
        clsLogger bugTracker = new clsLogger();

        public clsGenerateTrialDocket(string courtloc, DateTime courtdate, string page, string courtnumber, string datetype, string singles)
        {
            courtLoc = courtloc;
            courtDate = courtdate;
            pageNo = page;
            courtNumber = courtnumber;
            dateType = datetype;
            Singles = singles;

        }


        public string GenerateTrialDocketUsingCR(string Serverpath, System.Web.HttpRequest request)
        {
            clsENationWebComponents ClsDb = new clsENationWebComponents();
            clsCrsytalComponent Cr = new clsCrsytalComponent();

            DataSet ds;
            string name = "TrialDocket-" + DateTime.Now.ToFileTime() + ".pdf";
            // Noufil 5772 5772 Sub Report Added.
            //Nasir 5817 05/01/2009 add sub report Officer Summary
            String[] reportname = new String[] { "ArraignmentWaiting.rpt", "Judge.rpt", "OfficerSummary.rpt", "Report1.rpt" };
            string[] keySub = { "@URL", "@Courtloc", "@courtdate" };
            object[] valueSub = { request.ServerVariables["SERVER_NAME"].ToString(), courtLoc, courtDate };            
            ds = ClsDb.Get_DS_BySPArr("USP_HTS_GET_ST_TrialDocket", keySub, valueSub);
            string path = ConfigurationSettings.AppSettings["NTPATHDocketPDF"].ToString();
            string filename = null;

            string[] key = { "@courtloc", "@courtdate", "@page", "@courtnumber", "@datetype", "@singles", "@URL" };
            object[] value1 = { courtLoc, courtDate, pageNo, courtNumber, dateType, 1, request.ServerVariables["SERVER_NAME"].ToString() };
            if (Singles == "1")
            {
                filename = Serverpath + "\\TrialDocketSingles.rpt";
                return Cr.CreateReport(filename, "usp_hts_get_trial_docket_report_CR", key, value1, HttpContext.Current.Response, path, name.ToString());
            }
            else
            {
                // Afaq 7154 07/28/2010 Get service name from configuration service.
                //filename = Serverpath + "\\TrialDocket.rpt";
                ConfigurationService ConfigurationService = new ConfigurationService();
                filename = ConfigurationService.GetFilePath(SubModule.GENERAL, Key.REPORT_FILE_PATH) + "\\TrialDocket.rpt"; 
                return Cr.CreateSubReports(filename, "usp_hts_get_trial_docket_report_CR", key, value1, reportname, ds, 1, path, name.ToString());
            }

            // Generate trial docket from crystl report and return trial docket PDF path



        }

        public string GenerateTrialDocketUsingCR(string Serverpath, System.Web.HttpRequest request, int count)
        {
            string[] files = new string[count];
            string Filepath = String.Empty;
            string path = ConfigurationSettings.AppSettings["NTPATHDocketPDF"].ToString();

            for (int i = 0; i <= count - 1; i++)
            {
                files[i] = GenerateTrialDocketUsingCR(Serverpath, request);
                GeneratePDF(files[i], i);
            }
            Filepath = path + "\\TrialDocketMultiples-" + HttpContext.Current.Session.SessionID + ".pdf";
            Doc theDoc1 = (Doc)genFile;
            theDoc1.Save(Filepath);
            return Filepath;
        }

        private void GeneratePDF(string path, int seq)
        {
            try
            {
                if (seq == 0)
                {
                    Doc theDoc1 = new Doc();
                    theDoc1.Rect.Inset(0, 0);
                    theDoc1.Page = theDoc1.AddPage();
                    theDoc1.Rect.Bottom = 15.0;
                    theDoc1.Rect.Top = 785.0;
                    theDoc1.HtmlOptions.BrowserWidth = 800;
                    theDoc1.Read(path);
                    //int theID;
                    //theID = theDoc1.AddImage(path);
                    //while (true)
                    //{
                    //    theDoc1.FrameRect(); // add a black border
                    //    if (!theDoc1.Chainable(theID))
                    //        break;
                    //    theDoc1.Page = theDoc1.AddPage();
                    //    theID = theDoc1.AddImageToChain(theID);
                    //}
                    //for (int i = 1; i <= theDoc1.PageCount; i++)
                    //{
                    //    theDoc1.PageNumber = i;
                    //    theDoc1.Flatten();
                    //}
                    // string name = Session.SessionID + ".pdf";
                    //for website
                    //ViewState["vFullPath"] = ViewState["vNTPATHDocketPDF"].ToString() + name;
                    // ViewState["vPath"] = ViewState["vNTPATHDocketPDF"].ToString();
                    //website end
                    //theDoc1.Save(ViewState["vFullPath"].ToString());
                    genFile = theDoc1;
                    //theDoc1.Clear();
                }
                if (seq > 0)
                {
                    Doc theDoc = new Doc();
                    theDoc.Rect.Inset(0, 0);
                    theDoc.Page = theDoc.AddPage();
                    theDoc.HtmlOptions.BrowserWidth = 800;
                    theDoc.Read(path);
                    //int theID;
                    //theID = theDoc.AddImage(path);
                    //while (true)
                    //{
                    //    theDoc.FrameRect(); // add a black border
                    //    if (!theDoc.Chainable(theID))
                    //        break;
                    //    theDoc.Page = theDoc.AddPage();
                    //    theID = theDoc.AddImageToChain(theID);
                    //}
                    //for (int i = 1; i <= theDoc.PageCount; i++)
                    //{
                    //    theDoc.PageNumber = i;
                    //    theDoc.Flatten();
                    //}

                    genFile.Append(theDoc);
                    theDoc.Clear();
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
            //theDoc1.Clear();
        }


    }

}

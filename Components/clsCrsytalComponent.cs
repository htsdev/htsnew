using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using System.Web.SessionState;
using System.Web;

using System.Configuration;
using System.Collections;
using System.ComponentModel;

using System.IO;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;
using FrameWorkEnation.Components;
using HTP.Components;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using WebSupergoo.ABCpdf6;


using System.Drawing;

using System.Web.UI;

using System.Web.UI.HtmlControls;
using lntechNew.Components;

using lntechNew.Components.ClientInfo;


namespace lntechNew.Components
{
    /// <summary>
    /// Summary description for clsCrsytalComponent.
    /// </summary>
    /// clsENationWebComponents Cls_DAB 



    public class clsCrsytalComponent //: clsENationWebComponents
    {
        clsENationWebComponents Cls_DAB;



        public clsCrsytalComponent()
        {
            Cls_DAB = new clsENationWebComponents();
        }

        ~clsCrsytalComponent()
        {
            Cls_DAB = null;

        }

        # region Crystal Report Functions

        public void CreateReport(string filename, DataSet DataSet1, string StoredProcedure, string[] key, object[] value1, System.Web.SessionState.HttpSessionState session, System.Web.HttpResponse response)
        {
            //clsENationWebComponents Cls_DAB = new clsENationWebComponents();
            try
            {
                DataSet1 = Cls_DAB.Get_DS_BySPArr(StoredProcedure, key, value1);
            }
            catch (System.Exception ex)
            {
                response.Write("Exception...! " + ex.Message);
            }
            finally
            {
                //	System.IO.Directory dir; 
                if (!(Directory.Exists("C:\\Temp")))
                {
                    Directory.CreateDirectory("C:\\Temp");
                }
                ReportDocument ReportFile = new ReportDocument();

                ReportFile.Load(filename);
                ReportFile.SetDataSource(DataSet1.Tables[0]);

                CrystalDecisions.Shared.ExportOptions myExportOptions;
                CrystalDecisions.Shared.DiskFileDestinationOptions myDiskFileDestinationOptions;

                string myExportFile;

                myExportFile = "C:\\Temp\\" + session.SessionID.ToString() + ".pdf";
                myDiskFileDestinationOptions = new CrystalDecisions.Shared.DiskFileDestinationOptions();
                myDiskFileDestinationOptions.DiskFileName = myExportFile;
                myExportOptions = ReportFile.ExportOptions;

                myExportOptions.DestinationOptions = myDiskFileDestinationOptions;
                myExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
                myExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
                ReportFile.Export();

                //Sabir Khan 9255 05/06/2011 Need to close and dispose Report Document Object.
                ReportFile.Close();
                ReportFile.Dispose();

                response.ClearContent();
                response.ClearHeaders();
                response.ContentType = "application/pdf";
                response.WriteFile(myExportFile);
                response.Flush();
                response.Close();



                System.IO.File.Delete(myExportFile);
            }
        }


        // When coming from Frmbatchprint
        public void CreateReport(string filename, string StoredProcedure, string[] key, object[] value1, System.Web.SessionState.HttpSessionState session, System.Web.HttpResponse response)
        {
            //clsENationWebComponents Cls_DAB = new clsENationWebComponents();//zeeshan
            System.Data.DataSet DS1 = null;
            try
            {
                DS1 = Cls_DAB.Get_DS_BySPArr(StoredProcedure, key, value1);

            }
            catch (System.Exception ex)
            {
                response.Write("Exception...! " + ex.Message);
            }
            finally
            {

                if (!(Directory.Exists("C:\\Temp")))
                {
                    Directory.CreateDirectory("C:\\Temp");
                }
                ReportDocument ReportFile = new ReportDocument();

                ReportFile.Load(filename);

                // Task Id 3595 05/21/2008 Agha Usman
                if (filename.Contains("PledOutBatchLetter.rpt"))
                {
                    ReportFile.SetDataSource(manageDatatable(DS1.Tables[0]));
                }
                else
                {
                    ReportFile.SetDataSource(DS1.Tables[0]);
                }



                CrystalDecisions.Shared.ExportOptions myExportOptions;
                CrystalDecisions.Shared.DiskFileDestinationOptions myDiskFileDestinationOptions;

                string myExportFile;

                myExportFile = "C:\\Temp\\" + session.SessionID.ToString() + ".pdf";
                myDiskFileDestinationOptions = new CrystalDecisions.Shared.DiskFileDestinationOptions();
                myDiskFileDestinationOptions.DiskFileName = myExportFile;
                myExportOptions = ReportFile.ExportOptions;

                myExportOptions.DestinationOptions = myDiskFileDestinationOptions;
                myExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
                myExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
                ReportFile.Export();
                //Sabir Khan 9255 05/06/2011 Need to close and dispose Report Document Object.
                ReportFile.Close();
                ReportFile.Dispose();

                response.ClearContent();
                response.ClearHeaders();
                response.ContentType = "application/pdf";
                response.WriteFile(myExportFile);
                response.Flush();
                response.Close();
                System.IO.File.Delete(myExportFile);
            }
        }
        // Muhammad Ali 8253 09/15/2010 --> XML comments..
        /// <summary>
        /// This function is use for creating sub report for trail docket.
        /// </summary>
        /// <param name="filename">File Name</param>
        /// <param name="MainStoredProcedure">Procedure Name</param>
        /// <param name="keyMain">SP Parameters names</param>
        /// <param name="valueMain">SP parameters values</param>
        /// <param name="subreportname">Sub report name</param>
        /// <param name="ds">Data set for sub report generation</param>
        /// <param name="LetterType">letter type</param>
        /// <param name="session">Session object</param>
        /// <param name="response">Response object</param>
        /// <param name="path">path for file export</param>
        /// <param name="newfilename">New file name</param>
        public void CreateSubReports(string filename, string MainStoredProcedure, string[] keyMain, object[] valueMain, string[] subreportname, DataSet ds, int LetterType, System.Web.SessionState.HttpSessionState session, System.Web.HttpResponse response, string path, string newfilename)
        {
            DataSet DataSetMain = null;
            DataSet DataSetSub = null;


            try
            {
                DataSetMain = Cls_DAB.Get_DS_BySPArr(MainStoredProcedure, keyMain, valueMain);
                DataSetSub = ds;
            }
            catch (System.Exception ex)
            {
                response.Write("Exception...! " + ex.Message);
            }
            finally
            {
                //	System.IO.Directory dir; 
                if (!(Directory.Exists("C:\\Temp")))
                {
                    Directory.CreateDirectory("C:\\Temp");
                }
                ReportDocument ReportFile = new ReportDocument();
                try
                {
                    ReportFile.Load(filename);
                    ReportFile.SetDataSource(DataSetMain.Tables[0]);
                    for (int i = 0; i < DataSetSub.Tables.Count; i++)
                    {
                        ReportFile.OpenSubreport(subreportname[i]).SetDataSource(DataSetSub.Tables[i]);
                    }

                    //Sabir Khan 6033 06/25/2009 Supress Arraignment Waiting and Judge report for Criminal and civil Court locations...
                    //-----------------------------------------------------------------------------------------------------------------
                    if (MainStoredProcedure == "usp_hts_get_trial_docket_report_CR" && (valueMain[0].ToString().Equals("3037") || valueMain[0].ToString().Equals("3077") || valueMain[0].ToString().Equals("Civil") || valueMain[0].ToString().Equals("Criminal")))
                    {
                        foreach (Section sec in ReportFile.ReportDefinition.Sections)
                        {
                            if (sec.Name.ToLower() == "reportheadersection4" || sec.Name.ToLower() == "reportheadersection5")
                            {
                                sec.SectionFormat.EnableSuppress = true;

                            }
                        }
                    }
                    //Muhammad Ali 8253 09/15/2010 --> after Arraignment Waiting Report next reports show from next page..
                    else if (MainStoredProcedure == "usp_hts_get_trial_docket_report_CR" && valueMain[0].ToString().Equals("None") || valueMain[0].ToString().Equals("3037"))
                    {
                        foreach (Section sec in ReportFile.ReportDefinition.Sections)
                        {
                            if (sec.Name.ToLower() == "reportheadersection5")
                            {
                                sec.SectionFormat.EnableNewPageBefore = true;
                            }
                        }
                    }
                    //-----------------------------------------------------------------------------------------------------------------

                }



                catch (Exception ex)
                {
                    response.Write("Exception...! " + ex.Message);
                }



                CrystalDecisions.Shared.ExportOptions myExportOptions;
                CrystalDecisions.Shared.DiskFileDestinationOptions myDiskFileDestinationOptions;

                string myExportFile;

                //myExportFile = "C:\\Temp\\" + session.SessionID.ToString() + ".pdf";

                //DateTime dt = DateTime.Today;
                //string st = dt.Month.ToString() + "-" + dt.Day.ToString() + "-" + dt.Year.ToString();
                myExportFile = path + newfilename.ToString();
                myDiskFileDestinationOptions = new CrystalDecisions.Shared.DiskFileDestinationOptions();
                myDiskFileDestinationOptions.DiskFileName = myExportFile;
                myExportOptions = ReportFile.ExportOptions;

                myExportOptions.DestinationOptions = myDiskFileDestinationOptions;
                myExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
                myExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
                ReportFile.Export();
                //file created
                //Sabir Khan 9255 05/06/2011 Need to close and dispose Report Document Object.
                ReportFile.Close();
                ReportFile.Dispose();

                response.ClearContent();
                response.ClearHeaders();
                response.ContentType = "application/pdf";

                response.Buffer = false; // Adil 5985 08/13/2009 Fixing remote host closed issue
                response.WriteFile(myExportFile);

                response.Flush();
                response.Close();

                // Afaq 7154 07/28/2010 use for deleting pdf.
                deleteTempTrialDocket(myExportFile);




            }
        }

        /// <summary>
        /// CreateSubReports create PDF document and return a created PDF document path.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="MainStoredProcedure"></param>
        /// <param name="keyMain"></param>
        /// <param name="valueMain"></param>
        /// <param name="subreportname"></param>
        /// <param name="ds"></param>
        /// <param name="LetterType"></param>
        /// <param name="path"></param>
        /// <param name="newfilename"></param>
        public string CreateSubReports(string filename, string MainStoredProcedure, string[] keyMain, object[] valueMain, string[] subreportname, DataSet ds, int LetterType, string path, string newfilename)
        {
            DataSet DataSetMain = null;
            DataSet DataSetSub = null;
            string myExportFile = null;

            try
            {
                DataSetMain = Cls_DAB.Get_DS_BySPArr(MainStoredProcedure, keyMain, valueMain);
                DataSetSub = ds;
            }
            catch
            {
                throw;
            }
            finally
            {
                //	System.IO.Directory dir; 
                if (!(Directory.Exists("C:\\Temp")))
                {
                    Directory.CreateDirectory("C:\\Temp");
                }
                ReportDocument ReportFile = new ReportDocument();
                try
                {
                    ReportFile.Load(filename);
                    ReportFile.SetDataSource(DataSetMain.Tables[0]);
                    for (int i = 0; i < DataSetSub.Tables.Count; i++)
                    {
                        ReportFile.OpenSubreport(subreportname[i]).SetDataSource(DataSetSub.Tables[i]);
                    }
                }
                catch
                {
                    throw;
                }
                CrystalDecisions.Shared.ExportOptions myExportOptions;
                CrystalDecisions.Shared.DiskFileDestinationOptions myDiskFileDestinationOptions;

                myExportFile = path + newfilename.ToString();
                myDiskFileDestinationOptions = new CrystalDecisions.Shared.DiskFileDestinationOptions();
                myDiskFileDestinationOptions.DiskFileName = myExportFile;
                myExportOptions = ReportFile.ExportOptions;

                myExportOptions.DestinationOptions = myDiskFileDestinationOptions;
                myExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
                myExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
                ReportFile.Export();
                //Sabir Khan 9255 05/06/2011 Need to close and dispose Report Document Object.
                ReportFile.Close();
                ReportFile.Dispose();

                // Afaq 7154 07/28/2010 use for deleting pdf.
                deleteTempTrialDocket(myExportFile);
            }
            return myExportFile;
        }


        public void CreateReport(string filename, string StoredProcedure, string[] key, object[] value1, System.Web.SessionState.HttpSessionState session, System.Web.HttpResponse response, string path)
        {//This method is created by Azwer for bond and arraignment summary reports..
            //clsENationWebComponents Cls_DAB = new clsENationWebComponents();//zeeshan
            System.Data.DataSet DS1 = null;
            try
            {
                DS1 = Cls_DAB.Get_DS_BySPArr(StoredProcedure, key, value1);
            }
            catch (System.Exception ex)
            {
                response.Write("Exception...! " + ex.Message);
            }
            finally
            {
                //	System.IO.Directory dir; 

                ReportDocument ReportFile = new ReportDocument();

                ReportFile.Load(filename);
                ReportFile.SetDataSource(DS1.Tables[0]);

                CrystalDecisions.Shared.ExportOptions myExportOptions;
                CrystalDecisions.Shared.DiskFileDestinationOptions myDiskFileDestinationOptions;

                string myExportFile;

                //myExportFile = "C:\\Temp\\" + session.SessionID.ToString() + ".pdf";
                myExportFile = path;
                myDiskFileDestinationOptions = new CrystalDecisions.Shared.DiskFileDestinationOptions();
                myDiskFileDestinationOptions.DiskFileName = myExportFile;
                myExportOptions = ReportFile.ExportOptions;

                myExportOptions.DestinationOptions = myDiskFileDestinationOptions;
                myExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
                myExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
                ReportFile.Export();
                //Sabir Khan 9255 05/06/2011 Need to close and dispose Report Document Object.
                ReportFile.Close();
                ReportFile.Dispose();

                //response.ClearContent();
                //response.ClearHeaders();
                //response.ContentType = "application/pdf";
                //response.WriteFile(myExportFile);
                //response.Flush();
                //response.Close();

                //System.IO.File.Delete(myExportFile);
            }
        }
        public string CreateReport(string filename, string StoredProcedure, string[] key, object[] value1, System.Web.HttpResponse response, string path, string newfilename)
        {//This method is created by Azwer for bond and arraignment summary reports..
            //clsENationWebComponents Cls_DAB = new clsENationWebComponents();//zeeshan
            System.Data.DataSet DS1 = null;
            string myExportFile = null;
            try
            {
                DS1 = Cls_DAB.Get_DS_BySPArr(StoredProcedure, key, value1);
            }
            catch (System.Exception ex)
            {
                response.Write("Exception...! " + ex.Message);
            }
            finally
            {
                //	System.IO.Directory dir; 

                ReportDocument ReportFile = new ReportDocument();

                ReportFile.Load(filename);
                ReportFile.SetDataSource(DS1.Tables[0]);

                CrystalDecisions.Shared.ExportOptions myExportOptions;
                CrystalDecisions.Shared.DiskFileDestinationOptions myDiskFileDestinationOptions;



                //myExportFile = "C:\\Temp\\" + session.SessionID.ToString() + ".pdf";
                myExportFile = path + newfilename;
                myDiskFileDestinationOptions = new CrystalDecisions.Shared.DiskFileDestinationOptions();
                myDiskFileDestinationOptions.DiskFileName = myExportFile;
                myExportOptions = ReportFile.ExportOptions;

                myExportOptions.DestinationOptions = myDiskFileDestinationOptions;
                myExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
                myExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
                ReportFile.Export();
                //Sabir Khan 9255 05/06/2011 Need to close and dispose Report Document Object.
                ReportFile.Close();
                ReportFile.Dispose();

                //response.ClearContent();
                //response.ClearHeaders();
                //response.ContentType = "application/pdf";
                //response.WriteFile(myExportFile);
                //response.Flush();
                //response.Close();

                //System.IO.File.Delete(myExportFile);
            }
            return myExportFile;
        }
        public void CreateReport(string filename, string StoredProcedure, System.Web.SessionState.HttpSessionState session, System.Web.HttpResponse response, string path)
        {//This method is created by Azwer for bond and arraignment summary reports..
            //clsENationWebComponents Cls_DAB = new clsENationWebComponents();//zeeshan
            System.Data.DataSet DS1 = null;
            try
            {
                DS1 = Cls_DAB.Get_DS_BySP(StoredProcedure);
            }
            catch (System.Exception ex)
            {
                response.Write("Exception...! " + ex.Message);
            }
            finally
            {
                //	System.IO.Directory dir; 

                ReportDocument ReportFile = new ReportDocument();

                ReportFile.Load(filename);
                ReportFile.SetDataSource(DS1.Tables[0]);

                CrystalDecisions.Shared.ExportOptions myExportOptions;
                CrystalDecisions.Shared.DiskFileDestinationOptions myDiskFileDestinationOptions;

                string myExportFile;

                //myExportFile = "C:\\Temp\\" + session.SessionID.ToString() + ".pdf";
                myExportFile = path;
                myDiskFileDestinationOptions = new CrystalDecisions.Shared.DiskFileDestinationOptions();
                myDiskFileDestinationOptions.DiskFileName = myExportFile;
                myExportOptions = ReportFile.ExportOptions;

                myExportOptions.DestinationOptions = myDiskFileDestinationOptions;
                myExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
                myExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
                ReportFile.Export();
                //Sabir Khan 9255 05/06/2011 Need to close and dispose Report Document Object.
                ReportFile.Close();
                ReportFile.Dispose();

                //response.ClearContent();
                //response.ClearHeaders();
                //response.ContentType = "application/pdf";
                //response.WriteFile(myExportFile);
                //response.Flush();
                //response.Close();

                //System.IO.File.Delete(myExportFile);
            }
        }

        public void CreateReportTrialDocket(string filename, string StoredProcedure, string[] key, object[] value1, System.Web.SessionState.HttpSessionState session, System.Web.HttpResponse response)
        {
            //clsENationWebComponents Cls_DAB = new clsENationWebComponents();//zeeshan
            System.Data.DataSet DS1 = null;
            try
            {
                DS1 = Cls_DAB.Get_DS_BySPArr(StoredProcedure, key, value1);
            }
            catch (System.Exception ex)
            {
                response.Write("Exception...! " + ex.Message);
            }
            finally
            {
                //	System.IO.Directory dir; 
                if (!(Directory.Exists("C:\\Temp")))
                {
                    Directory.CreateDirectory("C:\\Temp");
                }
                ReportDocument ReportFile = new ReportDocument();

                ReportFile.Load(filename);
                ReportFile.Subreports[0].SetDataSource(DS1.Tables[0]);
                ReportFile.Subreports[1].SetDataSource(DS1.Tables[1]);

                ReportFile.SetDataSource(DS1.Tables[0]);

                CrystalDecisions.Shared.ExportOptions myExportOptions;
                CrystalDecisions.Shared.DiskFileDestinationOptions myDiskFileDestinationOptions;

                string myExportFile;

                myExportFile = "C:\\Temp\\" + session.SessionID.ToString() + ".pdf";
                myDiskFileDestinationOptions = new CrystalDecisions.Shared.DiskFileDestinationOptions();
                myDiskFileDestinationOptions.DiskFileName = myExportFile;
                myExportOptions = ReportFile.ExportOptions;

                myExportOptions.DestinationOptions = myDiskFileDestinationOptions;
                myExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
                myExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
                ReportFile.Export();
                //Sabir Khan 9255 05/06/2011 Need to close and dispose Report Document Object.
                ReportFile.Close();
                ReportFile.Dispose();

                response.ClearContent();
                response.ClearHeaders();
                response.ContentType = "application/pdf";
                response.WriteFile(myExportFile);
                response.Flush();
                response.Close();

                System.IO.File.Delete(myExportFile);
            }
        }


        public void CreateReportWord(string filename, DataSet DataSet1, string StoredProcedure, string[] key, object[] value1, System.Web.SessionState.HttpSessionState session, System.Web.HttpResponse response)
        {
            //clsENationWebComponents Cls_DAB = new clsENationWebComponents();
            try
            {
                DataSet1 = Cls_DAB.Get_DS_BySPArr(StoredProcedure, key, value1);
            }
            catch (System.Exception ex)
            {
                response.Write("Exception...! " + ex.Message);
            }
            finally
            {
                //	System.IO.Directory dir; 
                if (!(Directory.Exists("C:\\Temp")))
                {
                    Directory.CreateDirectory("C:\\Temp");
                }
                ReportDocument ReportFile = new ReportDocument();
                ReportFile.Load(filename);
                ReportFile.SetDataSource(DataSet1.Tables[0]);

                CrystalDecisions.Shared.ExportOptions myExportOptions;
                CrystalDecisions.Shared.DiskFileDestinationOptions myDiskFileDestinationOptions;

                string myExportFile;

                myExportFile = "C:\\Temp\\" + session.SessionID.ToString() + ".Doc";
                myDiskFileDestinationOptions = new CrystalDecisions.Shared.DiskFileDestinationOptions();
                myDiskFileDestinationOptions.DiskFileName = myExportFile;
                myExportOptions = ReportFile.ExportOptions;

                myExportOptions.DestinationOptions = myDiskFileDestinationOptions;
                myExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
                myExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.WordForWindows;
                ReportFile.Export();
                //Sabir Khan 9255 05/06/2011 Need to close and dispose Report Document Object.
                ReportFile.Close();
                ReportFile.Dispose();

                response.ClearContent();
                response.ClearHeaders();
                response.ContentType = "application/msword";
                response.WriteFile(myExportFile);
                response.Flush();
                response.Close();
                System.IO.File.Delete(myExportFile);
            }
        }

        // Rab Nawaz 10729 04/19/2013 Added for NISI Cases to Export the file to Word. . .
        ///<summary>
        /// Used to Get the Records to Print Answers for the NISI cases
        ///</summary>
        ///<param name="dsReport">DataSet ds</param>
        ///<param name="physicalPathWithFileName">Physical Path of File to Export</param>
        ///<param name="targetFolder">Path of file to be save</param>
        ///<param name="session">HTTP Seesion</param>
        ///<param name="response">HTTP Response</param>
        ///<returns></returns>
        public void CreateReportWord(DataSet dsReport, string physicalPathWithFileName, string targetFolder, System.Web.SessionState.HttpSessionState session, System.Web.HttpResponse response)
        {

            ReportDocument reportFile = new ReportDocument();
            reportFile.Load(physicalPathWithFileName);
            reportFile.SetDataSource(dsReport.Tables[0]);
            string fileNameToSave = session.SessionID + ".doc";
            DiskFileDestinationOptions myDiskFileDestinationOptions = new DiskFileDestinationOptions();
            myDiskFileDestinationOptions.DiskFileName = targetFolder + fileNameToSave;
            ExportOptions myExportOptions = reportFile.ExportOptions;
            myExportOptions.DestinationOptions = myDiskFileDestinationOptions;
            myExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
            myExportOptions.ExportFormatType = ExportFormatType.WordForWindows;
            reportFile.Export();
            //session["DocID"] = myExportFile;
            //return myExportFile;
            reportFile.Close();
            reportFile.Dispose();

            response.ClearContent();
            response.ClearHeaders();

            response.Clear();
            response.ContentType = "application/ms-word";
            response.AddHeader("Content-disposition", "inline; attachment; filename=" + fileNameToSave);
            response.WriteFile(targetFolder + fileNameToSave);
            response.Flush();
            response.Close();
        }

        ///<summary>
        ///</summary>
        ///<param name="dsReport"></param>
        ///<param name="physicalPathWithFileName"></param>
        ///<param name="targetFolder"></param>
        ///<param name="session"></param>
        ///<param name="response"></param>
        ///<returns></returns>
        public string CreateReportWordForNISI(DataSet dsReport, string physicalPathWithFileName, string targetFolder, int ticketid)
        {

            ReportDocument reportFile = new ReportDocument();
            reportFile.Load(physicalPathWithFileName);
            reportFile.SetDataSource(dsReport.Tables[0]);
            string fileNameToSave = ticketid + "_" + DateTime.Now.ToString("mm-dd-yyyy_ssss") + ".doc";
            DiskFileDestinationOptions myDiskFileDestinationOptions = new DiskFileDestinationOptions();
            myDiskFileDestinationOptions.DiskFileName = targetFolder + fileNameToSave;
            ExportOptions myExportOptions = reportFile.ExportOptions;
            myExportOptions.DestinationOptions = myDiskFileDestinationOptions;
            myExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
            myExportOptions.ExportFormatType = ExportFormatType.WordForWindows;
            reportFile.Export();
            //session["DocID"] = myExportFile;
            //return myExportFile;
            reportFile.Close();
            reportFile.Dispose();

            return fileNameToSave;
        }


        #region When coming by clicking cancel on paymentinfo page
        //Another overloaded by Maria                  //When coming by clicking cancel on paymentinfo page
        public void CreateReport(string filename, DataSet DataSet1, string StoredProcedure, string[] key, object[] value1, string CheckBatch, int LetterType, System.Web.SessionState.HttpSessionState session, System.Web.HttpResponse response)
        {
            try
            {
                DataSet1 = new DataSet();
                DataSet1 = Cls_DAB.Get_DS_BySPArr(StoredProcedure, key, value1);
            }
            catch (System.Exception ex)
            {
                response.Write("Exception...! " + ex.Message);
            }
            finally
            {
                //	System.IO.Directory dir; 
                if (!(Directory.Exists("C:\\Temp")))
                {
                    Directory.CreateDirectory("C:\\Temp");
                }
                ReportDocument ReportFile = new ReportDocument();

                ReportFile.Load(filename);
                ReportFile.SetDataSource(DataSet1.Tables[0]);

                CrystalDecisions.Shared.ExportOptions myExportOptions;
                CrystalDecisions.Shared.DiskFileDestinationOptions myDiskFileDestinationOptions;

                string myExportFile;

                myExportFile = "C:\\Temp\\" + session.SessionID.ToString() + ".pdf";
                myDiskFileDestinationOptions = new CrystalDecisions.Shared.DiskFileDestinationOptions();
                myDiskFileDestinationOptions.DiskFileName = myExportFile;
                myExportOptions = ReportFile.ExportOptions;

                myExportOptions.DestinationOptions = myDiskFileDestinationOptions;
                myExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
                myExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
                ReportFile.Export();//file created
                //Sabir Khan 9255 05/06/2011 Need to close and dispose Report Document Object.
                ReportFile.Close();
                ReportFile.Dispose();

                response.ClearContent();
                response.ClearHeaders();
                response.ContentType = "application/pdf";
                if (CheckBatch == "false")
                {

                    response.WriteFile(myExportFile);
                }
                response.Flush();
                response.Close();

                InsertIntoBatch(Convert.ToInt32(value1[0]), Convert.ToInt32(value1[1]), LetterType, myExportFile, CheckBatch, ".pdf", filename);
            }
        }

        /// <summary>
        /// ozair 3643 on 05/01/2008 added overloaded method of CreateReport to implement document tracking changes
        /// </summary>
        /// <param name="filename">Report file name with path</param>
        /// <param name="StoredProcedure">Procedure name</param>
        /// <param name="key">Procedures parameters</param>
        /// <param name="value1">Procedures parameters values</param>        
        /// <param name="LetterType">Type of Letter/Report</param>
        /// <param name="session">Current session</param>
        /// <param name="response">Current response</param>
        /// <param name="documentBatchID">Document id to print</param>
        public void CreateReport(string filename, string StoredProcedure, string[] key, object[] value1, int LetterType, System.Web.SessionState.HttpSessionState session, System.Web.HttpResponse response, int documentBatchID)
        {
            DataSet DataSet1 = null;
            try
            {
                DataSet1 = Cls_DAB.Get_DS_BySPArr(StoredProcedure, key, value1);
            }
            catch (System.Exception ex)
            {
                response.Write("Exception...! " + ex.Message);
            }
            finally
            {
                if (!(Directory.Exists("C:\\Temp")))
                {
                    Directory.CreateDirectory("C:\\Temp");
                }
                ReportDocument ReportFile = new ReportDocument();

                ReportFile.Load(filename);
                ReportFile.SetDataSource(DataSet1.Tables[0]);
                CrystalDecisions.Shared.ExportOptions myExportOptions;
                CrystalDecisions.Shared.DiskFileDestinationOptions myDiskFileDestinationOptions;

                string myExportFile;

                myExportFile = "C:\\Temp\\" + session.SessionID.ToString() + ".pdf";
                myDiskFileDestinationOptions = new CrystalDecisions.Shared.DiskFileDestinationOptions();
                myDiskFileDestinationOptions.DiskFileName = myExportFile;
                myExportOptions = ReportFile.ExportOptions;

                myExportOptions.DestinationOptions = myDiskFileDestinationOptions;
                myExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
                myExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
                ReportFile.Export();
                //Sabir Khan 9255 05/06/2011 Need to close and dispose Report Document Object.
                ReportFile.Close();
                ReportFile.Dispose();

                response.ClearContent();
                response.ClearHeaders();
                response.ContentType = "application/pdf";

                response.WriteFile(myExportFile);

                response.Flush();
                response.Close();

                InsertIntoBatch(Convert.ToInt32(value1[0]), Convert.ToInt32(value1[1]), LetterType, myExportFile, ".pdf", filename, documentBatchID);
            }
        }

        //Without Dataset		
        public void CreateReport(string filename, string StoredProcedure, string[] key, object[] value1, string CheckBatch, int LetterType, System.Web.SessionState.HttpSessionState session, System.Web.HttpResponse response)
        {
            DataSet DataSet1 = null;
            try
            {
                DataSet1 = Cls_DAB.Get_DS_BySPArr(StoredProcedure, key, value1);
            }
            catch (System.Exception ex)
            {
                response.Write("Exception...! " + ex.Message);
            }
            finally
            {
                //	System.IO.Directory dir; 
                if (!(Directory.Exists("C:\\Temp")))
                {
                    Directory.CreateDirectory("C:\\Temp");
                }
                ReportDocument ReportFile = new ReportDocument();

                ReportFile.Load(filename);
                ReportFile.SetDataSource(DataSet1.Tables[0]);
                CrystalDecisions.Shared.ExportOptions myExportOptions;
                CrystalDecisions.Shared.DiskFileDestinationOptions myDiskFileDestinationOptions;

                string myExportFile;

                myExportFile = "C:\\Temp\\" + session.SessionID.ToString() + ".pdf";
                myDiskFileDestinationOptions = new CrystalDecisions.Shared.DiskFileDestinationOptions();
                myDiskFileDestinationOptions.DiskFileName = myExportFile;
                myExportOptions = ReportFile.ExportOptions;

                myExportOptions.DestinationOptions = myDiskFileDestinationOptions;
                myExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
                myExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
                ReportFile.Export();//file created
                //Sabir Khan 9255 05/06/2011 Need to close and dispose Report Document Object.
                ReportFile.Close();
                ReportFile.Dispose();

                response.ClearContent();
                response.ClearHeaders();
                response.ContentType = "application/pdf";
                if (CheckBatch == "false")
                {

                    response.WriteFile(myExportFile);
                }
                response.Flush();
                response.Close();

                InsertIntoBatch(Convert.ToInt32(value1[0]), Convert.ToInt32(value1[1]), LetterType, myExportFile, CheckBatch, ".pdf", filename);
            }
        }
        /// <summary>
        /// //Fahadddd
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="StoredProcedure"></param>
        /// <param name="key"></param>
        /// <param name="value1"></param>
        /// <param name="CheckBatch"></param>
        /// <param name="LetterType"></param>
        /// <param name="session"></param>
        /// <param name="response"></param>
        /// <param name="Filepath"></param>
        public void CreateReport(string filename, string StoredProcedure, string[] key, object[] value1, string CheckBatch, int LetterType, System.Web.SessionState.HttpSessionState session, System.Web.HttpResponse response, object[] value2, string FilePath)
        {
            DataSet DataSet1 = null;
            try
            {
                DataSet1 = Cls_DAB.Get_DS_BySPArr(StoredProcedure, key, value1);
            }
            catch (System.Exception ex)
            {
                response.Write("Exception...! " + ex.Message);
            }
            finally
            {
                //	System.IO.Directory dir; 
                if (!(Directory.Exists("C:\\Temp")))
                {
                    Directory.CreateDirectory("C:\\Temp");
                }
                ReportDocument ReportFile = new ReportDocument();

                ReportFile.Load(filename);
                ReportFile.SetDataSource(DataSet1.Tables[0]);
                CrystalDecisions.Shared.ExportOptions myExportOptions;
                CrystalDecisions.Shared.DiskFileDestinationOptions myDiskFileDestinationOptions;

                string myExportFile;
                //myExportFile = FilePath;

                myExportFile = FilePath + session.SessionID.ToString() + "-ContinuanceDoc" + ".pdf";
                myDiskFileDestinationOptions = new CrystalDecisions.Shared.DiskFileDestinationOptions();
                myDiskFileDestinationOptions.DiskFileName = myExportFile;
                myExportOptions = ReportFile.ExportOptions;

                myExportOptions.DestinationOptions = myDiskFileDestinationOptions;
                myExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
                myExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
                ReportFile.Export();//file created
                //Sabir Khan 9255 05/06/2011 Need to close and dispose Report Document Object.
                ReportFile.Close();
                ReportFile.Dispose();

                response.ClearContent();
                response.ClearHeaders();
                response.ContentType = "application/pdf";


                response.WriteFile(myExportFile);



                string[] key_con = { "@ticketid", "@empid", "@docpath" };
                //string[] value_con = { value2, myExportFile };
                value2.SetValue(myExportFile, 2);
                Cls_DAB.ExecuteSP("Usp_hts_Insert_BatchPrint_Continuance", key_con, value2);

                response.Flush();
                response.Close();


            }
        }

        /// <summary>
        /// ozair 3643 on 05/01/2008 added overloaded method of CreateBondReport to implement document tracking changes
        /// </summary>
        /// <param name="filename">Report file name with path</param>
        /// <param name="MainStoredProcedure">Main report procedure name</param>
        /// <param name="SubStoredProcedure">Sub report procedures parameters</param>
        /// <param name="keyMain">Main report procedures parameters</param>
        /// <param name="valueMain">Main report procedures parameters values</param>
        /// <param name="keySub">Sub report procedures parameters</param>
        /// <param name="valueSub">Sub report procedures parameters values</param>
        /// <param name="LetterType">Type of Letter/Report</param>
        /// <param name="session">Current session</param>
        /// <param name="response">Current response</param>
        /// <param name="documentBatchID">Document id to print</param>
        public void CreateBondReport(string filename, string MainStoredProcedure, string SubStoredProcedure, string[] keyMain, object[] valueMain, string[] keySub, object[] valueSub, int LetterType, System.Web.SessionState.HttpSessionState session, System.Web.HttpResponse response, int documentBatchID)
        {
            DataSet DataSetMain = null;
            DataSet DataSetSub = null;

            try
            {
                DataSetMain = Cls_DAB.Get_DS_BySPArr(MainStoredProcedure, keyMain, valueMain);
                DataSetSub = Cls_DAB.Get_DS_BySPArr(SubStoredProcedure, keySub, valueSub);
            }
            catch (System.Exception ex)
            {
                response.Write("Exception...! " + ex.Message);
            }
            finally
            {
                //	System.IO.Directory dir; 
                if (!(Directory.Exists("C:\\Temp")))
                {
                    Directory.CreateDirectory("C:\\Temp");
                }
                ReportDocument ReportFile = new ReportDocument();

                ReportFile.Load(filename);

                ReportFile.SetDataSource(DataSetMain.Tables[0]);
                ReportFile.OpenSubreport("PleaOfNotGuilty_Bond.rpt").SetDataSource(DataSetSub.Tables[0]);
                // Rab Nawz Khan 8921 02/22/2011 added subreport in Bond.rpt
                DataTable dt = DataSetMain.Tables[0].Copy();
                dt.Rows.RemoveAt(Convert.ToInt32(dt.Rows.Count - 1));
                ReportFile.OpenSubreport("NoContentPlea.rpt").SetDataSource(dt);
                // END 8921

                CrystalDecisions.Shared.ExportOptions myExportOptions;
                CrystalDecisions.Shared.DiskFileDestinationOptions myDiskFileDestinationOptions;

                string myExportFile;

                myExportFile = "C:\\Temp\\" + session.SessionID.ToString() + ".pdf";
                myDiskFileDestinationOptions = new CrystalDecisions.Shared.DiskFileDestinationOptions();
                myDiskFileDestinationOptions.DiskFileName = myExportFile;
                myExportOptions = ReportFile.ExportOptions;

                myExportOptions.DestinationOptions = myDiskFileDestinationOptions;
                myExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
                myExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
                ReportFile.Export();//file created
                //Sabir Khan 9255 05/06/2011 Need to close and dispose Report Document Object.
                ReportFile.Close();
                ReportFile.Dispose();

                response.ClearContent();
                response.ClearHeaders();
                response.ContentType = "application/pdf";

                response.WriteFile(myExportFile);

                response.Flush();
                response.Close();

                InsertIntoBatch(Convert.ToInt32(valueMain[0]), Convert.ToInt32(valueMain[1]), LetterType, myExportFile, ".pdf", filename, documentBatchID);
            }
        }

        public void CreateBondReport(string filename, string MainStoredProcedure, string SubStoredProcedure, string[] keyMain, object[] valueMain, string[] keySub, object[] valueSub, string CheckBatch, int LetterType, System.Web.SessionState.HttpSessionState session, System.Web.HttpResponse response)
        {
            DataSet DataSetMain = null;
            DataSet DataSetSub = null;
            try
            {
                DataSetMain = Cls_DAB.Get_DS_BySPArr(MainStoredProcedure, keyMain, valueMain);
                DataSetSub = Cls_DAB.Get_DS_BySPArr(SubStoredProcedure, keySub, valueSub);
            }
            catch (System.Exception ex)
            {
                response.Write("Exception...! " + ex.Message);
            }
            finally
            {
                //	System.IO.Directory dir; 
                if (!(Directory.Exists("C:\\Temp")))
                {
                    Directory.CreateDirectory("C:\\Temp");
                }
                ReportDocument ReportFile = new ReportDocument();

                ReportFile.Load(filename);
                ReportFile.SetDataSource(DataSetMain.Tables[0]);

                ReportFile.OpenSubreport("PleaOfNotGuilty_Bond.rpt").SetDataSource(DataSetSub.Tables[0]);

                CrystalDecisions.Shared.ExportOptions myExportOptions;
                CrystalDecisions.Shared.DiskFileDestinationOptions myDiskFileDestinationOptions;

                string myExportFile;

                myExportFile = "C:\\Temp\\" + session.SessionID.ToString() + ".pdf";
                myDiskFileDestinationOptions = new CrystalDecisions.Shared.DiskFileDestinationOptions();
                myDiskFileDestinationOptions.DiskFileName = myExportFile;
                myExportOptions = ReportFile.ExportOptions;

                myExportOptions.DestinationOptions = myDiskFileDestinationOptions;
                myExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
                myExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
                ReportFile.Export();//file created
                //Sabir Khan 9255 05/06/2011 Need to close and dispose Report Document Object.
                ReportFile.Close();
                ReportFile.Dispose();

                response.ClearContent();
                response.ClearHeaders();
                response.ContentType = "application/pdf";
                if (CheckBatch == "false")
                {

                    response.WriteFile(myExportFile);
                }
                response.Flush();
                response.Close();

                InsertIntoBatch(Convert.ToInt32(valueMain[0]), Convert.ToInt32(valueMain[1]), LetterType, myExportFile, CheckBatch, ".pdf", filename);
            }
        }
        //CODE ADDED BY FAHAD FOR SUB REPORTS 
        public void CreateSubReports(string filename, string MainStoredProcedure, string[] keyMain, object[] valueMain, string[] subreportname, DataSet ds, int LetterType, System.Web.SessionState.HttpSessionState session, System.Web.HttpResponse response, string path, string ticketID, bool reportopen, string newfilename)
        {
            DataSet DataSetMain = null;
            DataSet DataSetSub = null;


            try
            {
                DataSetMain = Cls_DAB.Get_DS_BySPArr(MainStoredProcedure, keyMain, valueMain);
                DataSetSub = ds;
            }
            catch (System.Exception ex)
            {
                response.Write("Exception...! " + ex.Message);
            }
            finally
            {
                //	System.IO.Directory dir; 
                if (!(Directory.Exists("C:\\Temp")))
                {
                    Directory.CreateDirectory("C:\\Temp");
                }
                ReportDocument ReportFile = new ReportDocument();

                ReportFile.Load(filename);
                ReportFile.SetDataSource(DataSetMain.Tables[0]);
                for (int i = 0; i < DataSetSub.Tables.Count; i++)
                {
                    ReportFile.OpenSubreport(subreportname[i]).SetDataSource(DataSetSub.Tables[i]);
                }



                CrystalDecisions.Shared.ExportOptions myExportOptions;
                CrystalDecisions.Shared.DiskFileDestinationOptions myDiskFileDestinationOptions;

                string myExportFile;

                //myExportFile = "C:\\Temp\\" + session.SessionID.ToString() + ".pdf";

                //DateTime dt = DateTime.Today;
                //string st = dt.Month.ToString() + "-" + dt.Day.ToString() + "-" + dt.Year.ToString();
                myExportFile = path + newfilename.ToString();
                myDiskFileDestinationOptions = new CrystalDecisions.Shared.DiskFileDestinationOptions();
                myDiskFileDestinationOptions.DiskFileName = myExportFile;
                myExportOptions = ReportFile.ExportOptions;

                myExportOptions.DestinationOptions = myDiskFileDestinationOptions;
                myExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
                myExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
                ReportFile.Export();
                //file created
                //Sabir Khan 9255 05/06/2011 Need to close and dispose Report Document Object.
                ReportFile.Close();
                ReportFile.Dispose();

                if (reportopen == true)
                {
                    response.ClearContent();
                    response.ClearHeaders();
                    response.ContentType = "application/pdf";


                    response.WriteFile(myExportFile);

                    response.Flush();
                    response.Close();
                }




            }
        }

        //Code Added By M.Azwar Alam for Arraignment docket...
        public void CreateSubReports(string filename, string MainStoredProcedure, string[] subreportname, DataSet ds, System.Web.SessionState.HttpSessionState session, System.Web.HttpResponse response, string Path, int j)
        {
            DataSet DataSetMain = null;
            DataSet DataSetSub = null;


            try
            {
                DataSetMain = Cls_DAB.Get_DS_BySP(MainStoredProcedure);
                DataSetSub = ds;
            }
            catch (System.Exception ex)
            {
                response.Write("Exception...! " + ex.Message);
            }
            finally
            {

                ReportDocument ReportFile = new ReportDocument();

                ReportFile.Load(filename);
                if (j == 1)
                    ReportFile.SetDataSource(DataSetMain.Tables[0]);

                for (int i = 0; i < DataSetSub.Tables.Count; i++)
                {
                    ReportFile.OpenSubreport(subreportname[i]).SetDataSource(DataSetSub.Tables[i]);
                }



                CrystalDecisions.Shared.ExportOptions myExportOptions;
                CrystalDecisions.Shared.DiskFileDestinationOptions myDiskFileDestinationOptions;

                string myExportFile;


                myExportFile = Path;
                myDiskFileDestinationOptions = new CrystalDecisions.Shared.DiskFileDestinationOptions();
                myDiskFileDestinationOptions.DiskFileName = myExportFile;
                myExportOptions = ReportFile.ExportOptions;

                myExportOptions.DestinationOptions = myDiskFileDestinationOptions;
                myExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
                myExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
                ReportFile.Export();
                //Sabir Khan 9255 05/06/2011 Need to close and dispose Report Document Object.
                ReportFile.Close();
                ReportFile.Dispose();



            }
        }
        //Code Added By M.Azwar Alam for Arraignment docket...
        public void CreateSubReports(string filename, string MainStoredProcedure, string[] subreportname, string[] key, object[] values, DataSet ds, System.Web.SessionState.HttpSessionState session, System.Web.HttpResponse response, string Path)
        {
            DataSet DataSetMain = null;
            DataSet DataSetSub = null;


            try
            {
                DataSetMain = Cls_DAB.Get_DS_BySPArr(MainStoredProcedure, key, values);
                DataSetSub = ds;
            }
            catch (System.Exception ex)
            {
                response.Write("Exception...! " + ex.Message);
            }
            finally
            {

                ReportDocument ReportFile = new ReportDocument();

                ReportFile.Load(filename);
                ReportFile.SetDataSource(DataSetMain.Tables[0]);
                for (int i = 0; i < DataSetSub.Tables.Count; i++)
                {
                    ReportFile.OpenSubreport(subreportname[i]).SetDataSource(DataSetSub.Tables[i]);
                }



                CrystalDecisions.Shared.ExportOptions myExportOptions;
                CrystalDecisions.Shared.DiskFileDestinationOptions myDiskFileDestinationOptions;

                string myExportFile;


                myExportFile = Path;
                myDiskFileDestinationOptions = new CrystalDecisions.Shared.DiskFileDestinationOptions();
                myDiskFileDestinationOptions.DiskFileName = myExportFile;
                myExportOptions = ReportFile.ExportOptions;

                myExportOptions.DestinationOptions = myDiskFileDestinationOptions;
                myExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
                myExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
                ReportFile.Export();

                //Sabir Khan 9255 05/06/2011 Need to close and dispose Report Document Object.
                ReportFile.Close();
                ReportFile.Dispose();


            }
        }

        public void CreateBondReport(string filename, string MainStoredProcedure, string SubStoredProcedure, string[] keyMain, object[] valueMain, string[] keySub, object[] valueSub, System.Web.SessionState.HttpSessionState session, System.Web.HttpResponse response, string path)
        {//Added by Azee for PrintAllBonds options on bondsettings report in docket settings..
            DataSet DataSetMain = null;
            DataSet DataSetSub = null;
            try
            {
                DataSetMain = Cls_DAB.Get_DS_BySPArr(MainStoredProcedure, keyMain, valueMain);
                DataSetSub = Cls_DAB.Get_DS_BySPArr(SubStoredProcedure, keySub, valueSub);
            }
            catch (System.Exception ex)
            {
                response.Write("Exception...! " + ex.Message);
            }
            finally
            {
                //	System.IO.Directory dir; 
                /*if (!(Directory.Exists("C:\\Temp")))
                {
                    Directory.CreateDirectory("C:\\Temp");
                }*/
                ReportDocument ReportFile = new ReportDocument();

                ReportFile.Load(filename);
                ReportFile.SetDataSource(DataSetMain.Tables[0]);

                ReportFile.OpenSubreport("PleaOfNotGuilty_Bond.rpt").SetDataSource(DataSetSub.Tables[0]);

                CrystalDecisions.Shared.ExportOptions myExportOptions;
                CrystalDecisions.Shared.DiskFileDestinationOptions myDiskFileDestinationOptions;

                string myExportFile;

                // myExportFile = "C:\\Temp\\" + session.SessionID.ToString() + ".pdf";
                myExportFile = path;
                myDiskFileDestinationOptions = new CrystalDecisions.Shared.DiskFileDestinationOptions();
                myDiskFileDestinationOptions.DiskFileName = myExportFile;
                myExportOptions = ReportFile.ExportOptions;

                myExportOptions.DestinationOptions = myDiskFileDestinationOptions;
                myExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
                myExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
                ReportFile.Export();//file created
                //Sabir Khan 9255 05/06/2011 Need to close and dispose Report Document Object.
                ReportFile.Close();
                ReportFile.Dispose();

                /* response.ClearContent();
                 response.ClearHeaders();
                 response.ContentType = "application/pdf";
                 if (CheckBatch == "false")
                 {

                     response.WriteFile(myExportFile);
                 }
                 response.Flush();
                 response.Close();*/

                //InsertIntoBatch(Convert.ToInt32(valueMain[0]), Convert.ToInt32(valueMain[1]), LetterType, myExportFile, CheckBatch, ".pdf", filename);
            }
        }


        //Added by Azee for Export to word
        public void CreateReportWord(string filename, string MainStoredProcedure, string SubStoredProcedure, string[] keyMain, object[] valueMain, string[] keySub, object[] valueSub, string CheckBatch, int LetterType, System.Web.SessionState.HttpSessionState session, System.Web.HttpResponse response)
        {

            DataSet DataSetMain = null;
            DataSet DataSetSub = null;
            try
            {
                DataSetMain = Cls_DAB.Get_DS_BySPArr(MainStoredProcedure, keyMain, valueMain);
                DataSetSub = Cls_DAB.Get_DS_BySPArr(SubStoredProcedure, keySub, valueSub);
            }
            catch (System.Exception ex)
            {
                response.Write("Exception...! " + ex.Message);
            }
            finally
            {
                //	System.IO.Directory dir; 
                if (!(Directory.Exists("C:\\Temp")))
                {
                    Directory.CreateDirectory("C:\\Temp");
                }
                ReportDocument ReportFile = new ReportDocument();

                ReportFile.Load(filename);

                // Rab Nawz Khan 8921 02/22/2011 Included new Bond letter in Bond.rpt file
                //ReportFile.ParameterFields[Convert.ToString(keyMain[0])].CurrentValues.AddValue(valueMain[0]);
                //ReportFile.ParameterFields[Convert.ToString(keyMain[1])].CurrentValues.AddValue(valueMain[1]);

                ReportFile.SetDataSource(DataSetMain.Tables[0]);
                ReportFile.OpenSubreport("PleaOfNotGuilty_Bond.rpt").SetDataSource(DataSetSub.Tables[0]);
                // Rab Nawz Khan 8921 02/22/2011 added subreport in Bond.rpt
                DataTable dt = DataSetMain.Tables[0].Copy();
                dt.Rows.RemoveAt(Convert.ToInt32(dt.Rows.Count - 1));
                ReportFile.OpenSubreport("NoContentPlea.rpt").SetDataSource(dt);
                // END 8921

                CrystalDecisions.Shared.ExportOptions myExportOptions;
                CrystalDecisions.Shared.DiskFileDestinationOptions myDiskFileDestinationOptions;

                string myExportFile;

                myExportFile = "C:\\Temp\\" + session.SessionID.ToString() + ".Doc";
                myDiskFileDestinationOptions = new CrystalDecisions.Shared.DiskFileDestinationOptions();
                myDiskFileDestinationOptions.DiskFileName = myExportFile;
                myExportOptions = ReportFile.ExportOptions;

                myExportOptions.DestinationOptions = myDiskFileDestinationOptions;
                myExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
                myExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.WordForWindows;
                ReportFile.Export();//file created
                //Sabir Khan 9255 05/06/2011 Need to close and dispose Report Document Object.
                ReportFile.Close();
                ReportFile.Dispose();

                response.ClearContent();
                response.ClearHeaders();
                response.ContentType = "application/msword";
                if (CheckBatch == "false")
                {

                    response.WriteFile(myExportFile);
                }
                response.Flush();
                response.Close();

                InsertIntoBatch(Convert.ToInt32(valueMain[0]), Convert.ToInt32(valueMain[1]), LetterType, myExportFile, CheckBatch, ".pdf", filename);
            }
        }//


        /// <summary>
        /// ozair 3643 on 05/01/2008 added over loaded method
        /// </summary>
        /// <param name="TicketID"></param>
        /// <param name="EmpID"></param>
        /// <param name="LetterID"></param>
        /// <param name="myExportFile"></param>
        /// <param name="Doctype"></param>
        /// <param name="filename"></param>
        /// <param name="documentBatchID"></param>
        private void InsertIntoBatch(int TicketID, int EmpID, int LetterID, string myExportFile, string Doctype, string filename, int documentBatchID)
        {
            try
            {
                string vpat = ConfigurationSettings.AppSettings["NTPATHBatchPrint"].ToString();

                string[] keys = { "@TicketID_FK", "@BatchDate", "@PrintDate", "@IsPrinted", "@LetterID_FK", "@EmpID", "@DocPath", "@PageCount", "@DocumentBatchID" };
                DataSet RptInfo = GetReportName(LetterID);

                string fname = vpat;
                string Datetime = Convert.ToString(DateTime.Now).Replace(":", "").Replace("/", "");
                string filenameonly = Datetime + "-" + TicketID + "-" + RptInfo.Tables[0].Rows[0]["LetterName"].ToString() + Doctype;
                string FilePath = fname + filenameonly;

                fname = FilePath;

                Doc doc = new Doc();
                doc.Read(myExportFile);
                int pagecount = doc.PageCount;
                doc.Dispose();

                object[] values = { TicketID, System.DateTime.Now.Date, System.DateTime.Now.Date, 1, LetterID, EmpID, filenameonly, pagecount, documentBatchID };
                Cls_DAB.InsertBySPArr("USP_HTS_Insert_BatchPrintLetter", keys, values);

                if (System.IO.File.Exists(FilePath) == true)
                {
                    System.IO.File.Delete(FilePath);
                    System.IO.File.Copy(myExportFile, FilePath);
                }
                else
                {
                    System.IO.File.Copy(myExportFile, FilePath);
                }

                System.IO.File.Delete(myExportFile);
            }
            catch (System.Exception ex)
            {
                string exKeep = ex.Message.ToString();
            }
        }

        //Yasir Kamal 7590 03/29/2010 trial letter modifications
        /// <summary>
        /// This method is used to insert batch Info
        /// </summary>
        /// <param name="TicketID">Ticket ID</param>
        /// <param name="EmpID">Employee ID</param>
        /// <param name="LetterID">Letter ID</param>
        /// <param name="myExportFile">Path of file created in Temp folder</param>
        /// <param name="BatchType">Batch Type</param>
        /// <param name="Doctype">Document Type</param>
        /// <param name="filename">Report File name</param>
        /// <returns>File Path</returns>
        public string InsertIntoBatch(int TicketID, int EmpID, int LetterID, string myExportFile, string BatchType, string Doctype, string filename)
        {
            try
            {
                string vpat = ConfigurationSettings.AppSettings["NTPATHBatchPrint"].ToString();

                string[] keys = { "@TicketID_FK", "@BatchDate", "@PrintDate", "@IsPrinted", "@LetterID_FK", "@EmpID", "@DocPath", "@PageCount" };
                DataSet RptInfo = GetReportName(LetterID);
                //string fname=filename.Substring(0,filename.LastIndexOf(@"\"));		
                //fname=fname+@"\BatchPrint\";
                string fname = vpat;
                string Datetime = Convert.ToString(DateTime.Now).Replace(":", "").Replace("/", "");
                string filenameonly = Datetime + "-" + TicketID + "-" + RptInfo.Tables[0].Rows[0]["LetterName"].ToString() + Doctype;
                string FilePath = fname + filenameonly;
                //commented by ozair.
                //FilePath=FilePath.Replace(" ","");		

                //fname=FilePath.Substring(FilePath.IndexOf(@"Reports\")-1);
                fname = FilePath;

                //added by ozair
                Doc doc = new Doc();
                doc.Read(myExportFile);
                int pagecount = doc.PageCount;
                doc.Dispose();
                //

                if (BatchType == "true")
                {
                    //Enter values in batch Letter.

                    //Insert Values in tblBatch.
                    //object[] values={TicketID,System.DateTime.Now,"1/1/1900",0,LetterID,EmpID,fname};
                    object[] values = { TicketID, System.DateTime.Now, "1/1/1900", 0, LetterID, EmpID, filenameonly, pagecount };
                    Cls_DAB.InsertBySPArr("USP_HTS_Insert_BatchPrintLetter", keys, values);

                }
                else //when batch false 
                {
                    //object[] values={TicketID,System.DateTime.Now.Date,System.DateTime.Now.Date,1,LetterID,EmpID,fname};
                    object[] values = { TicketID, System.DateTime.Now.Date, System.DateTime.Now.Date, 1, LetterID, EmpID, filenameonly, pagecount };
                    Cls_DAB.InsertBySPArr("USP_HTS_Insert_BatchPrintLetter", keys, values);

                }
                //added by zee
                if (System.IO.File.Exists(FilePath) == true)
                {
                    System.IO.File.Delete(FilePath);
                    System.IO.File.Copy(myExportFile, FilePath);
                }
                else
                {
                    System.IO.File.Copy(myExportFile, FilePath);
                }
                //
                System.IO.File.Delete(myExportFile);
                return FilePath;
            }
            catch
            {
                return "";
                //string exKeep = ex.Message.ToString();
            }
        }
        public string CreateReport_Retfname(string filename, string StoredProcedure, string[] key, object[] value1, string CheckBatch, int LetterType, System.Web.SessionState.HttpSessionState session, System.Web.HttpResponse response)
        {
            DataSet DataSet1 = null;
            string filepath = "";
            try
            {
                DataSet1 = Cls_DAB.Get_DS_BySPArr(StoredProcedure, key, value1);
            }
            catch (System.Exception ex)
            {
                response.Write("Exception...! " + ex.Message);
            }
            finally
            {
                //	System.IO.Directory dir; 
                if (!(Directory.Exists("C:\\Temp")))
                {
                    Directory.CreateDirectory("C:\\Temp");
                }
                ReportDocument ReportFile = new ReportDocument();
                


                  ReportFile.Load(filename);
                ReportFile.SetDataSource(DataSet1.Tables[0]);
                //Farrukh 10148 04/19/2012 Added DataSource for TrialNotification sub report
                if (filename.ToLower().Contains("trial_notification"))
                {
                    ReportFile.Subreports["Trial_Notification_batch1.rpt"].SetDataSource(DataSet1.Tables[0]);
                }
                // Mohammad Ali 9949 12/30/2011 if Report Name is receipt then load receipt.rpt report when case is criminal then CriminalReceipt.rpt 
                if (filename.IndexOf("receipt") > 1)
                {
                    System.Data.DataSet dsTest = null;
                    dsTest = Cls_DAB.Get_DS_BySPArr("USP_HTS_ESignature_Get_Payments", key, value1);

                    ReportFile.Subreports["RptFirstThreePayments.rpt"].SetDataSource(dsTest.Tables[0]);
                    ReportFile.Subreports["RptRestOfThePayments.rpt"].SetDataSource(dsTest.Tables[0]);

                    dsTest = null;
                    string[] key2 = { "@TicketID" };
                    object[] value2 = { value1.GetValue(0) };
                    dsTest = Cls_DAB.Get_DS_BySPArr("USP_HTS_ESignature_Get_PaymentPlan", key2, value2);
                    ReportFile.Subreports["SubReport_PaymentPlans.rpt"].SetDataSource(dsTest.Tables[0]);
                }
                else if (filename.IndexOf("CriminalReceipt") > 1)
                {
                    DataSet dsTest = null;
                    dsTest = Cls_DAB.Get_DS_BySPArr("USP_HTS_ESignature_Get_Payments", key, value1);

                    ReportFile.Subreports["RptFirstThreePayments.rpt"].SetDataSource(dsTest.Tables[0]);
                    //ReportFile.Subreports["RptRestOfThePayments.rpt"].SetDataSource(dsTest);

                    dsTest = null;
                    string[] key2 = { "@TicketID" };
                    object[] value2 = { value1.GetValue(0) };
                    dsTest = Cls_DAB.Get_DS_BySPArr("USP_HTS_ESignature_Get_PaymentPlan", key2, value2);
                    //  ReportFile.Subreports["SubReport_PaymentPlans.rpt"].SetDataSource(dsTest);


                    dsTest = null;
                    dsTest = Cls_DAB.Get_DS_BySPArr("usp_htp_get_alrCaseInformation", key, value1);
                    ReportFile.Subreports["subRpt_PaymentPlan_AlrCaseInfo.rpt"].SetDataSource(dsTest.Tables[0]);
                    ReportFile.Subreports["subRpt_Agreement_AlrCaseInfo.rpt"].SetDataSource(dsTest.Tables[0]);
                    ReportFile.Subreports["SubRpt_R_Agreement_AlrCaseInfo.rpt"].SetDataSource(dsTest.Tables[0]);
                    ReportFile.Subreports["SubRpt_Contract_AlrCaseInfo.rpt"].SetDataSource(dsTest.Tables[0]);



                    dsTest = null;
                    // Providing one object to the Procedure 
                    string[] keyofPlanes = { key[0] };
                    object[] ticketIdForPayments = { value1[0] };
                    dsTest = Cls_DAB.Get_DS_BySPArr("USP_HTS_Get_New_SchedulePaymentDetail", keyofPlanes, ticketIdForPayments);
                    ReportFile.Subreports["SubRpt_PaymentPlan.rpt"].SetDataSource(dsTest.Tables[0]);
                }

                CrystalDecisions.Shared.ExportOptions myExportOptions;
                CrystalDecisions.Shared.DiskFileDestinationOptions myDiskFileDestinationOptions;

                string myExportFile;

                myExportFile = "C:\\Temp\\" + session.SessionID.ToString() + ".pdf";
                myDiskFileDestinationOptions = new CrystalDecisions.Shared.DiskFileDestinationOptions();
                myDiskFileDestinationOptions.DiskFileName = myExportFile;
                myExportOptions = ReportFile.ExportOptions;

                myExportOptions.DestinationOptions = myDiskFileDestinationOptions;
                myExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
                myExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
                ReportFile.Export();//file created

                //Sabir Khan 9255 05/06/2011 Need to close and dispose Report Document Object.
                ReportFile.Close();
                ReportFile.Dispose();

                response.ClearContent();
                response.ClearHeaders();
                response.ContentType = "application/pdf";
                if (CheckBatch == "false")
                {

                    response.WriteFile(myExportFile);
                }
                response.Flush();
                response.Close();

                filepath = InsertIntoBatch1(Convert.ToInt32(value1[0]), Convert.ToInt32(value1[1]), LetterType, myExportFile, CheckBatch, ".pdf", filename);
            }
            return filepath;
        }
        private string InsertIntoBatch1(int TicketID, int EmpID, int LetterID, string myExportFile, string BatchType, string Doctype, string filename)
        {
            //Code by Maria    //zee //Modified by ozair
            try
            {

                string vpat = ConfigurationSettings.AppSettings["NTPATHBatchPrint"].ToString();

                string[] keys = { "@TicketID_FK", "@BatchDate", "@PrintDate", "@IsPrinted", "@LetterID_FK", "@EmpID", "@DocPath", "@PageCount" };
                DataSet RptInfo = GetReportName(LetterID);
                //string fname=filename.Substring(0,filename.LastIndexOf(@"\"));				
                //fname=fname+@"\BatchPrint\";
                string fname = vpat;
                //if (!Directory.Exists(fname))
                //	Directory.CreateDirectory(fname);

                string Datetime = Convert.ToString(DateTime.Now).Replace(":", "").Replace("/", "");
                string filenameonly = Datetime + "-" + TicketID + "-" + RptInfo.Tables[0].Rows[0]["LetterName"].ToString() + Doctype;
                string FilePath = fname + filenameonly;
                //commented by ozair.
                //FilePath=FilePath.Replace(" ","");				

                //fname=FilePath.Substring(FilePath.IndexOf(@"Reports\")-1);
                fname = FilePath;
                //added by ozair
                Doc doc = new Doc();
                doc.Read(myExportFile);
                int pagecount = doc.PageCount;
                doc.Dispose();
                //
                if (BatchType == "true")
                {
                    //Insert Values in tblBatch.
                    object[] values = { TicketID, System.DateTime.Now.Date, "1/1/1900", 0, LetterID, EmpID, filenameonly, pagecount };
                    Cls_DAB.InsertBySPArr("USP_HTS_Insert_BatchPrintLetter", keys, values);
                }
                else //when batch false 
                {
                    object[] values = { TicketID, System.DateTime.Now.Date, System.DateTime.Now.Date, 1, LetterID, EmpID, filenameonly, pagecount };
                    Cls_DAB.InsertBySPArr("USP_HTS_Insert_BatchPrintLetter", keys, values);

                }
                //added by zee
                if (System.IO.File.Exists(FilePath) == true)
                {
                    System.IO.File.Delete(FilePath);
                    System.IO.File.Copy(myExportFile, FilePath);
                }
                else
                {
                    System.IO.File.Copy(myExportFile, FilePath);
                }
                //
                System.IO.File.Delete(myExportFile);
                return FilePath;
            }
            catch (System.Exception ex)
            {
                string exKeep = ex.Message.ToString();
                return "";
            }
        }

        public void CreateReportWord(string filename, DataSet DataSet1, string StoredProcedure, string[] key, object[] value1, string CheckBatch, int LetterType, System.Web.SessionState.HttpSessionState session, System.Web.HttpResponse response)
        {
            //clsENationWebComponents Cls_DAB = new clsENationWebComponents();

            try
            {
                DataSet1 = Cls_DAB.Get_DS_BySPArr(StoredProcedure, key, value1);
            }
            catch (System.Exception ex)
            {
                response.Write("Exception...! " + ex.Message);
            }
            finally
            {
                //	System.IO.Directory dir; 
                if (!(Directory.Exists("C:\\Temp")))
                {
                    Directory.CreateDirectory("C:\\Temp");
                }
                ReportDocument ReportFile = new ReportDocument();
                ReportFile.Load(filename);
                ReportFile.SetDataSource(DataSet1);

                if (filename == "receipt")
                {
                    System.Data.DataTable dtTest = null;
                    dtTest = Cls_DAB.Get_DS_BySPArr("USP_HTS_ESignature_Get_Payments", key, value1).Tables[0];

                    ReportFile.Subreports["RptFirstThreePayments.rpt"].SetDataSource(dtTest);
                    ReportFile.Subreports["RptRestOfThePayments.rpt"].SetDataSource(dtTest);

                    //Added by khalid for bug date 26-1-08 precautionary after 2861
                    DataSet ds = null;
                    string[] key2 = { "@TicketID" };
                    object[] value2 = { value1.GetValue(0) };
                    ds = Cls_DAB.Get_DS_BySPArr("USP_HTS_ESignature_Get_PaymentPlan", key2, value2);
                    if (ds.Tables.Count > 0)
                        ReportFile.Subreports["SubReport_PaymentPlans.rpt"].SetDataSource(ds.Tables[0]);
                    //end khalid   

                }

                CrystalDecisions.Shared.ExportOptions myExportOptions;
                CrystalDecisions.Shared.DiskFileDestinationOptions myDiskFileDestinationOptions;

                string myExportFile;

                myExportFile = "C:\\Temp\\" + session.SessionID.ToString() + ".Doc";
                myDiskFileDestinationOptions = new CrystalDecisions.Shared.DiskFileDestinationOptions();
                myDiskFileDestinationOptions.DiskFileName = myExportFile;
                myExportOptions = ReportFile.ExportOptions;

                myExportOptions.DestinationOptions = myDiskFileDestinationOptions;
                myExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
                myExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.WordForWindows;
                ReportFile.Export();
                //Sabir Khan 9255 05/06/2011 Need to close and dispose Report Document Object.
                ReportFile.Close();
                ReportFile.Dispose();

                response.ClearContent();
                response.ClearHeaders();
                response.ContentType = "application/msword";
                if (CheckBatch == "false")
                {
                    response.WriteFile(myExportFile);
                }
                response.Flush();
                response.Close();

                //Waqas 5933 05/22/2009 Deleting file
                System.IO.File.Delete(myExportFile);
                //InsertIntoBatch(Convert.ToInt32(value1[0]),Convert.ToInt32(value1[1]),LetterType,myExportFile,CheckBatch,".Doc",filename);
            }
        }
        public void CreateReportWord(string filename, string StoredProcedure, string[] key, object[] value1, string CheckBatch, int LetterType, System.Web.SessionState.HttpSessionState session, System.Web.HttpResponse response)
        {
            //clsENationWebComponents Cls_DAB = new clsENationWebComponents();
            DataSet DataSet1 = null;
            try
            {
                DataSet1 = Cls_DAB.Get_DS_BySPArr(StoredProcedure, key, value1);
            }
            catch (System.Exception ex)
            {
                response.Write("Exception...! " + ex.Message);
            }
            finally
            {
                //	System.IO.Directory dir; 
                if (!(Directory.Exists("C:\\Temp")))
                {
                    Directory.CreateDirectory("C:\\Temp");
                }
                ReportDocument ReportFile = new ReportDocument();
                ReportFile.Load(filename);
                ReportFile.SetDataSource(DataSet1.Tables[0]);

                CrystalDecisions.Shared.ExportOptions myExportOptions;
                CrystalDecisions.Shared.DiskFileDestinationOptions myDiskFileDestinationOptions;

                string myExportFile;

                myExportFile = "C:\\Temp\\" + session.SessionID.ToString() + ".Doc";
                myDiskFileDestinationOptions = new CrystalDecisions.Shared.DiskFileDestinationOptions();
                myDiskFileDestinationOptions.DiskFileName = myExportFile;
                myExportOptions = ReportFile.ExportOptions;

                myExportOptions.DestinationOptions = myDiskFileDestinationOptions;
                myExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
                myExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.WordForWindows;
                ReportFile.Export();
                //Sabir Khan 9255 05/06/2011 Need to close and dispose Report Document Object.
                ReportFile.Close();
                ReportFile.Dispose();

                response.ClearContent();
                response.ClearHeaders();
                response.ContentType = "application/msword";
                if (CheckBatch == "false")
                {
                    response.WriteFile(myExportFile);
                }
                response.Flush();
                response.Close();

                //Waqas 5933 05/22/2009 Deleting file
                System.IO.File.Delete(myExportFile);
                //InsertIntoBatch(Convert.ToInt32(value1[0]),Convert.ToInt32(value1[1]),LetterType,myExportFile,CheckBatch,".Doc",filename);
            }
        }
        #endregion
        #region letter OF Rep
        //Noufil Khan 3999 05/22/2008 overloaded function with parameter return
        public string CreateReport(string filename, string StoredProcedure, string[] key, object[] value1, string CheckBatch, int LetterType, int EmpID, System.Web.SessionState.HttpSessionState session, System.Web.HttpResponse response)
        {
            DataSet DataSet1 = null;
            string returnFile = "";
            try
            {
                DataSet1 = Cls_DAB.Get_DS_BySPArr(StoredProcedure, key, value1);
            }
            catch (System.Exception ex)
            {
                response.Write("Exception...! " + ex.Message);
            }
            finally
            {
                string myExportFile = "";
                //	System.IO.Directory dir; 
                if (!(Directory.Exists("C:\\Temp")))
                {
                    Directory.CreateDirectory("C:\\Temp");
                }
                ReportDocument ReportFile = new ReportDocument();

                ReportFile.Load(filename);
                ReportFile.SetDataSource(DataSet1.Tables[0]);

                CrystalDecisions.Shared.ExportOptions myExportOptions;
                CrystalDecisions.Shared.DiskFileDestinationOptions myDiskFileDestinationOptions;



                myExportFile = "C:\\Temp\\" + session.SessionID.ToString() + ".pdf";

                myDiskFileDestinationOptions = new CrystalDecisions.Shared.DiskFileDestinationOptions();
                myDiskFileDestinationOptions.DiskFileName = myExportFile;
                myExportOptions = ReportFile.ExportOptions;

                myExportOptions.DestinationOptions = myDiskFileDestinationOptions;
                myExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
                myExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
                ReportFile.Export();
                //Sabir Khan 9255 05/06/2011 Need to close and dispose Report Document Object.
                ReportFile.Close();
                ReportFile.Dispose();

                response.ClearContent();
                response.ClearHeaders();
                response.ContentType = "application/pdf";
                if (CheckBatch == "false")
                {
                    response.WriteFile(myExportFile);
                }
                response.Flush();
                response.Close();
                returnFile = InsertIntoBatch(Convert.ToInt32(value1[0]), EmpID, LetterType, myExportFile, CheckBatch, ".pdf", filename);

            }
            return returnFile;
        }
        //Nasir 6181 07/22/2009 overloaded function with parameter return
        public string CreateReport(string filename, string StoredProcedure, string[] key, object[] value1, int LetterType, int EmpID, System.Web.SessionState.HttpSessionState session, System.Web.HttpResponse response)
        {
            DataSet DataSet1 = null;
            string returnFile = "";
            try
            {
                DataSet1 = Cls_DAB.Get_DS_BySPArr(StoredProcedure, key, value1);
            }
            catch (System.Exception ex)
            {
                response.Write("Exception...! " + ex.Message);
            }
            finally
            {
                string myExportFile = "";
                //	System.IO.Directory dir; 
                if (!(Directory.Exists("C:\\Temp")))
                {
                    Directory.CreateDirectory("C:\\Temp");
                }
                ReportDocument ReportFile = new ReportDocument();

                ReportFile.Load(filename);
                ReportFile.SetDataSource(DataSet1.Tables[0]);

                CrystalDecisions.Shared.ExportOptions myExportOptions;
                CrystalDecisions.Shared.DiskFileDestinationOptions myDiskFileDestinationOptions;



                myExportFile = "C:\\Temp\\" + session.SessionID.ToString() + ".pdf";

                myDiskFileDestinationOptions = new CrystalDecisions.Shared.DiskFileDestinationOptions();
                myDiskFileDestinationOptions.DiskFileName = myExportFile;
                myExportOptions = ReportFile.ExportOptions;

                myExportOptions.DestinationOptions = myDiskFileDestinationOptions;
                myExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
                myExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
                ReportFile.Export();
                //Sabir Khan 9255 05/06/2011 Need to close and dispose Report Document Object.
                ReportFile.Close();
                ReportFile.Dispose();

                returnFile = InsertIntoBatch(Convert.ToInt32(value1[0]), EmpID, LetterType, myExportFile, "false", ".pdf", filename);

            }
            return returnFile;
        }

        //Word letter of Rep
        public void CreateReportWord(string filename, string StoredProcedure, string[] key, object[] value1, string CheckBatch, int LetterType, int EmpID, System.Web.SessionState.HttpSessionState session, System.Web.HttpResponse response)
        {
            DataSet DataSet1 = null;
            try
            {
                DataSet1 = Cls_DAB.Get_DS_BySPArr(StoredProcedure, key, value1);
            }
            catch (System.Exception ex)
            {
                response.Write("Exception...! " + ex.Message);
            }
            finally
            {
                //	System.IO.Directory dir; 
                if (!(Directory.Exists("C:\\Temp")))
                {
                    Directory.CreateDirectory("C:\\Temp");
                }
                ReportDocument ReportFile = new ReportDocument();
                ReportFile.Load(filename);
                ReportFile.SetDataSource(DataSet1.Tables[0]);

                CrystalDecisions.Shared.ExportOptions myExportOptions;
                CrystalDecisions.Shared.DiskFileDestinationOptions myDiskFileDestinationOptions;

                string myExportFile;

                myExportFile = "C:\\Temp\\" + session.SessionID.ToString() + ".Doc";
                myDiskFileDestinationOptions = new CrystalDecisions.Shared.DiskFileDestinationOptions();
                myDiskFileDestinationOptions.DiskFileName = myExportFile;
                myExportOptions = ReportFile.ExportOptions;

                myExportOptions.DestinationOptions = myDiskFileDestinationOptions;
                myExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
                myExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.WordForWindows;
                ReportFile.Export();
                //Sabir Khan 9255 05/06/2011 Need to close and dispose Report Document Object.
                ReportFile.Close();
                ReportFile.Dispose();

                response.ClearContent();
                response.ClearHeaders();
                response.ContentType = "application/msword";
                if (CheckBatch == "false")
                {
                    response.WriteFile(myExportFile);
                }
                response.Flush();
                response.Close();

                //Waqas 5933 05/22/2009 Deleting file
                System.IO.File.Delete(myExportFile);

                //InsertIntoBatch(value1[0].ToString(),EmpID,LetterType,myExportFile,CheckBatch,".Doc");
            }
        }

        //Waqas 5864 07/02/2009 ALR Letters
        /// <summary>
        /// This method is used to generate word copy of the report.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="StoredProcedure"></param>
        /// <param name="key"></param>
        /// <param name="value1"></param>
        /// <param name="CheckBatch"></param>
        /// <param name="LetterType"></param>
        /// <param name="EmpID"></param>
        /// <param name="session"></param>
        /// <param name="response"></param>
        public void CreateReportWordWithName(string Docfilename, string filename, string StoredProcedure, string[] key, object[] value1, string CheckBatch, int LetterType, int EmpID, System.Web.SessionState.HttpSessionState session, System.Web.HttpResponse response)
        {
            DataSet DataSet1 = null;
            try
            {
                DataSet1 = Cls_DAB.Get_DS_BySPArr(StoredProcedure, key, value1);
            }
            catch (System.Exception ex)
            {
                response.Write("Exception...! " + ex.Message);
            }
            finally
            {
                //	System.IO.Directory dir; 
                if (!(Directory.Exists("C:\\Temp")))
                {
                    Directory.CreateDirectory("C:\\Temp");
                }
                ReportDocument ReportFile = new ReportDocument();
                ReportFile.Load(filename);
                ReportFile.SetDataSource(DataSet1.Tables[0]);

                CrystalDecisions.Shared.ExportOptions myExportOptions;
                CrystalDecisions.Shared.DiskFileDestinationOptions myDiskFileDestinationOptions;

                string myExportFile;

                myExportFile = "C:\\Temp\\" + session.SessionID.ToString() + ".Doc";
                myDiskFileDestinationOptions = new CrystalDecisions.Shared.DiskFileDestinationOptions();
                myDiskFileDestinationOptions.DiskFileName = myExportFile;
                myExportOptions = ReportFile.ExportOptions;

                myExportOptions.DestinationOptions = myDiskFileDestinationOptions;
                myExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
                myExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.WordForWindows;
                ReportFile.Export();
                //Sabir Khan 9255 05/06/2011 Need to close and dispose Report Document Object.
                ReportFile.Close();
                ReportFile.Dispose();

                response.ClearContent();
                response.ClearHeaders();
                response.ContentType = "application/msword";

                response.AddHeader("Content-Disposition", "attachment; filename=" + Docfilename + ".doc");

                if (CheckBatch == "false")
                {
                    response.WriteFile(myExportFile);
                }
                response.Flush();
                response.Close();

                //Waqas 5933 05/22/2009 Deleting file
                System.IO.File.Delete(myExportFile);

                //InsertIntoBatch(value1[0].ToString(),EmpID,LetterType,myExportFile,CheckBatch,".Doc");
            }
        }

        // Abid Ali 5359 1/23/2009  Created Certified Letter of Rep sub reports
        public string CreateCertifiedLORSubReports(string filename, string MainStoredProcedure, string[] keyMain, object[] valueMain, string[] subreportname, string CheckBatch, int EmpID, DataSet ds, System.Web.SessionState.HttpSessionState session, System.Web.HttpResponse response)
        {
            DataSet DataSetMain = null;
            DataSet DataSetSub = null;
            string returnFile = "";

            try
            {
                DataSetMain = Cls_DAB.Get_DS_BySPArr(MainStoredProcedure, keyMain, valueMain);
                DataSetSub = ds;
            }
            catch (System.Exception ex)
            {
                response.Write("Exception...! " + ex.Message);
            }
            finally
            {
                //	System.IO.Directory dir; 
                if (!(Directory.Exists("C:\\Temp")))
                {
                    Directory.CreateDirectory("C:\\Temp");
                }
                ReportDocument ReportFile = new ReportDocument();
                try
                {
                    ReportFile.Load(filename);
                    ReportFile.SetDataSource(DataSetMain.Tables[0]);
                    for (int i = 0; i < DataSetSub.Tables.Count; i++)
                    {
                        ReportFile.OpenSubreport(subreportname[i]).SetDataSource(DataSetSub.Tables[i]);
                    }
                }
                catch (Exception ex)
                {
                    response.Write("Exception...! " + ex.Message);
                }

                CrystalDecisions.Shared.ExportOptions myExportOptions;
                CrystalDecisions.Shared.DiskFileDestinationOptions myDiskFileDestinationOptions;

                string myExportFile = "C:\\Temp\\" + session.SessionID.ToString() + ".pdf";

                myDiskFileDestinationOptions = new CrystalDecisions.Shared.DiskFileDestinationOptions();
                myDiskFileDestinationOptions.DiskFileName = myExportFile;
                myExportOptions = ReportFile.ExportOptions;

                myExportOptions.DestinationOptions = myDiskFileDestinationOptions;
                myExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
                myExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
                ReportFile.Export();
                //Sabir Khan 9255 05/06/2011 Need to close and dispose Report Document Object.
                ReportFile.Close();
                ReportFile.Dispose();

                response.ClearContent();
                response.ClearHeaders();
                response.ContentType = "application/pdf";

                if (CheckBatch == "false")
                {
                    response.WriteFile(myExportFile);
                }
                response.Flush();
                response.Close();
                returnFile = InsertIntoLORBatch(valueMain[0].ToString(), EmpID, myExportFile, CheckBatch, ".pdf", filename);
            }

            return returnFile;
        }

        public string CreateReportCertifiedLOR(string filename, string StoredProcedure, string[] key, object[] value1, string CheckBatch, int EmpID, System.Web.SessionState.HttpSessionState session, System.Web.HttpResponse response)
        {
            DataSet DataSet1 = null;
            string returnFile = "";
            try
            {
                DataSet1 = Cls_DAB.Get_DS_BySPArr(StoredProcedure, key, value1);
            }
            catch (System.Exception ex)
            {
                response.Write("Exception...! " + ex.Message);
            }
            finally
            {
                string myExportFile = "";
                //	System.IO.Directory dir; 
                if (!(Directory.Exists("C:\\Temp")))
                {
                    Directory.CreateDirectory("C:\\Temp");
                }
                ReportDocument ReportFile = new ReportDocument();

                ReportFile.Load(filename);
                ReportFile.SetDataSource(DataSet1.Tables[0]);

                CrystalDecisions.Shared.ExportOptions myExportOptions;
                CrystalDecisions.Shared.DiskFileDestinationOptions myDiskFileDestinationOptions;



                myExportFile = "C:\\Temp\\" + session.SessionID.ToString() + ".pdf";

                myDiskFileDestinationOptions = new CrystalDecisions.Shared.DiskFileDestinationOptions();
                myDiskFileDestinationOptions.DiskFileName = myExportFile;
                myExportOptions = ReportFile.ExportOptions;

                myExportOptions.DestinationOptions = myDiskFileDestinationOptions;
                myExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
                myExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
                ReportFile.Export();
                //Sabir Khan 9255 05/06/2011 Need to close and dispose Report Document Object.
                ReportFile.Close();
                ReportFile.Dispose();

                response.ClearContent();
                response.ClearHeaders();
                response.ContentType = "application/pdf";
                if (CheckBatch == "false")
                {
                    response.WriteFile(myExportFile);
                }
                response.Flush();
                response.Close();
                returnFile = InsertIntoLORBatch(value1[0].ToString(), EmpID, myExportFile, CheckBatch, ".pdf", filename);

            }
            return returnFile;
        }

        private string InsertIntoLORBatch(string trackingNumber, int EmpID, string myExportFile, string BatchType, string Doctype, string filename)
        {
            try
            {
                string vpat = ConfigurationSettings.AppSettings["NTPATHLORPrint"].ToString();
                string fname = vpat;
                string Datetime = Convert.ToString(DateTime.Now).Replace(":", "").Replace("/", "");
                string filenameonly = trackingNumber + "_Printed" + Doctype;
                string FilePath = fname + filenameonly;
                fname = FilePath;

                Doc doc = new Doc();
                doc.Read(myExportFile);
                int pagecount = doc.PageCount;
                doc.Dispose();

                if (System.IO.File.Exists(FilePath))
                {
                    System.IO.File.Delete(FilePath);
                    System.IO.File.Copy(myExportFile, FilePath);
                }
                else
                {
                    System.IO.File.Copy(myExportFile, FilePath);
                }

                System.IO.File.Delete(myExportFile);
                return FilePath;
            }
            catch
            {
                return "";
                //string exKeep = ex.Message.ToString();
            }
        }
        #endregion

        //just creating the file to physical location not displaying it //When OK is clicked on Payment info page
        public void CreateReportForEntry(string filename, string StoredProcedure, string[] key, object[] value1, string CheckBatch, int LetterType, System.Web.SessionState.HttpSessionState session, System.Web.HttpResponse response)
        {
            DataSet DataSet1 = null;
            try
            {
                DataSet1 = Cls_DAB.Get_DS_BySPArr(StoredProcedure, key, value1);
            }
            catch (System.Exception ex)
            {
                response.Write("Exception...! " + ex.Message);
            }
            finally
            {
                //	System.IO.Directory dir; 
                if (!(Directory.Exists("C:\\Temp")))
                {
                    Directory.CreateDirectory("C:\\Temp");
                }
                ReportDocument ReportFile = new ReportDocument();

                ReportFile.Load(filename);
                ReportFile.SetDataSource(DataSet1.Tables[0]);

                CrystalDecisions.Shared.ExportOptions myExportOptions;
                CrystalDecisions.Shared.DiskFileDestinationOptions myDiskFileDestinationOptions;

                string myExportFile;

                myExportFile = "C:\\Temp\\" + session.SessionID.ToString() + ".pdf";
                myDiskFileDestinationOptions = new CrystalDecisions.Shared.DiskFileDestinationOptions();
                myDiskFileDestinationOptions.DiskFileName = myExportFile;
                myExportOptions = ReportFile.ExportOptions;

                myExportOptions.DestinationOptions = myDiskFileDestinationOptions;
                myExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
                myExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
                ReportFile.Export();
                //Sabir Khan 9255 05/06/2011 Need to close and dispose Report Document Object.
                ReportFile.Close();
                ReportFile.Dispose();

                InsertIntoBatch(Convert.ToInt32(value1[0]), Convert.ToInt32(value1[1]), LetterType, myExportFile, CheckBatch, ".pdf", filename);
            }
        }

        public void CreateReportForEntry(string filename, string StoredProcedure, string[] key, object[] value1, string CheckBatch, int LetterType, int EmpID, System.Web.SessionState.HttpSessionState session, System.Web.HttpResponse response)
        {
            DataSet DataSet1 = null;

            try
            {
                DataSet1 = Cls_DAB.Get_DS_BySPArr(StoredProcedure, key, value1);
            }
            catch (System.Exception ex)
            {
                response.Write("Exception...! " + ex.Message);
            }
            finally
            {
                if (!(Directory.Exists("C:\\Temp")))
                {
                    Directory.CreateDirectory("C:\\Temp");
                }
                ReportDocument ReportFile = new ReportDocument();

                ReportFile.Load(filename);
                ReportFile.SetDataSource(DataSet1.Tables[0]);

                CrystalDecisions.Shared.ExportOptions myExportOptions;
                CrystalDecisions.Shared.DiskFileDestinationOptions myDiskFileDestinationOptions;

                string myExportFile;

                myExportFile = "C:\\Temp\\" + session.SessionID.ToString() + ".pdf";
                myDiskFileDestinationOptions = new CrystalDecisions.Shared.DiskFileDestinationOptions();
                myDiskFileDestinationOptions.DiskFileName = myExportFile;
                myExportOptions = ReportFile.ExportOptions;

                myExportOptions.DestinationOptions = myDiskFileDestinationOptions;
                myExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
                myExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
                ReportFile.Export();
                //Sabir Khan 9255 05/06/2011 Need to close and dispose Report Document Object.
                ReportFile.Close();
                ReportFile.Dispose();

                InsertIntoBatch(Convert.ToInt32(value1[0]), EmpID, LetterType, myExportFile, CheckBatch, ".pdf", filename);
            }
        }

        private DataSet GetReportName(int LetterID)
        {
            string[] keys = { "@LetterID" };
            object[] LetterID1 = { LetterID };
            DataSet ds_rpt = Cls_DAB.Get_DS_BySPArr("USP_HTS_GET_BatchLetterInfo", keys, LetterID1);
            return ds_rpt;
        }

        //simple
        public void CreateReportWord(string filename, string StoredProcedure, string[] key, object[] value1, System.Web.SessionState.HttpSessionState session, System.Web.HttpResponse response)
        {
            //clsENationWebComponents Cls_DAB = new clsENationWebComponents();
            System.Data.DataSet DS1 = null;

            try
            {
                DS1 = Cls_DAB.Get_DS_BySPArr(StoredProcedure, key, value1);
            }
            catch (System.Exception ex)
            {
                response.Write("Exception...! " + ex.Message);
            }
            finally
            {
                //	System.IO.Directory dir; 
                if (!(Directory.Exists("C:\\Temp")))
                {
                    Directory.CreateDirectory("C:\\Temp");
                }
                ReportDocument ReportFile = new ReportDocument();
                ReportFile.Load(filename);
                ReportFile.SetDataSource(DS1.Tables[0]);
                // Rab Nawaz Khan 10346 08/01/2012 Fixed the Export to Word parameter missing issue. . . 
                if (filename.ToLower().Contains("trial_notification"))
                {
                    ReportFile.Subreports["Trial_Notification_batch1.rpt"].SetDataSource(DS1.Tables[0]);
                }
                if (filename.IndexOf("receipt") > 1)
                {
                    System.Data.DataSet dsTest = null;
                    dsTest = Cls_DAB.Get_DS_BySPArr("USP_HTS_ESignature_Get_Payments", key, value1);

                    ReportFile.Subreports["RptFirstThreePayments.rpt"].SetDataSource(dsTest.Tables[0]);
                    ReportFile.Subreports["RptRestOfThePayments.rpt"].SetDataSource(dsTest.Tables[0]);

                    //Added by khalid for bug 2681 date 26-1-08
                    DataSet ds = null;
                    string[] key2 = { "@TicketID" };
                    object[] value2 = { value1.GetValue(0) };
                    ds = Cls_DAB.Get_DS_BySPArr("USP_HTS_ESignature_Get_PaymentPlan", key2, value2);
                    if (ds.Tables.Count > 0)
                        ReportFile.Subreports["SubReport_PaymentPlans.rpt"].SetDataSource(ds.Tables[0]);
                    //end 2681


                }
                else if (filename.IndexOf("CriminalReceipt") > 1)
                {
                    DataSet dsTest = null;
                    dsTest = Cls_DAB.Get_DS_BySPArr("USP_HTS_ESignature_Get_Payments", key, value1);

                    ReportFile.Subreports["RptFirstThreePayments.rpt"].SetDataSource(dsTest.Tables[0]);
                    //ReportFile.Subreports["RptRestOfThePayments.rpt"].SetDataSource(dsTest);

                    dsTest = null;
                    string[] key2 = { "@TicketID" };
                    object[] value2 = { value1.GetValue(0) };
                    dsTest = Cls_DAB.Get_DS_BySPArr("USP_HTS_ESignature_Get_PaymentPlan", key2, value2);
                    //  ReportFile.Subreports["SubReport_PaymentPlans.rpt"].SetDataSource(dsTest);


                    dsTest = null;
                    dsTest = Cls_DAB.Get_DS_BySPArr("usp_htp_get_alrCaseInformation", key, value1);
                    ReportFile.Subreports["subRpt_PaymentPlan_AlrCaseInfo.rpt"].SetDataSource(dsTest.Tables[0]);
                    ReportFile.Subreports["subRpt_Agreement_AlrCaseInfo.rpt"].SetDataSource(dsTest.Tables[0]);
                    ReportFile.Subreports["SubRpt_R_Agreement_AlrCaseInfo.rpt"].SetDataSource(dsTest.Tables[0]);
                    ReportFile.Subreports["SubRpt_Contract_AlrCaseInfo.rpt"].SetDataSource(dsTest.Tables[0]);



                    dsTest = null;
                    // Providing one object to the Procedure 
                    string[] keyofPlanes = { key[0] };
                    object[] ticketIdForPayments = { value1[0] };
                    dsTest = Cls_DAB.Get_DS_BySPArr("USP_HTS_Get_New_SchedulePaymentDetail", keyofPlanes, ticketIdForPayments);
                    ReportFile.Subreports["SubRpt_PaymentPlan.rpt"].SetDataSource(dsTest.Tables[0]);
                }

                CrystalDecisions.Shared.ExportOptions myExportOptions;
                CrystalDecisions.Shared.DiskFileDestinationOptions myDiskFileDestinationOptions;

                string myExportFile;

                myExportFile = "C:\\Temp\\" + session.SessionID.ToString() + ".Doc";
                myDiskFileDestinationOptions = new CrystalDecisions.Shared.DiskFileDestinationOptions();
                myDiskFileDestinationOptions.DiskFileName = myExportFile;
                myExportOptions = ReportFile.ExportOptions;

                myExportOptions.DestinationOptions = myDiskFileDestinationOptions;
                myExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
                myExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.WordForWindows;
                ReportFile.Export();
                //Sabir Khan 9255 05/06/2011 Need to close and dispose Report Document Object.
                ReportFile.Close();
                ReportFile.Dispose();

                response.ClearContent();
                response.ClearHeaders();
                response.ContentType = "application/msword";
                response.WriteFile(myExportFile);
                response.Flush();
                response.Close();
                System.IO.File.Delete(myExportFile);
            }
        }


        public void CreateReportNone(CrystalDecisions.CrystalReports.Engine.ReportDocument ReportFile, string path, DataSet DataSet1, string StoredProcedure, string[] key, object[] value1, System.Web.SessionState.HttpSessionState session, System.Web.HttpResponse response)
        {
            //clsENationWebComponents Cls_DAB = new clsENationWebComponents();
            try
            {
                DataSet1 = Cls_DAB.Get_DS_BySPArr(StoredProcedure, key, value1);
            }
            catch (System.Exception ex)
            {
                response.Write("Exception...! " + ex.Message);
            }
            finally
            {
                //	System.IO.Directory dir; 
                if (!(Directory.Exists("C:\\Temp")))
                {
                    Directory.CreateDirectory("C:\\Temp");
                }
                ReportFile.SetDataSource(DataSet1.Tables[0]);

                CrystalDecisions.Shared.ExportOptions myExportOptions;
                CrystalDecisions.Shared.DiskFileDestinationOptions myDiskFileDestinationOptions;

                string myExportFile;

                myExportFile = "C:\\Temp\\" + session.SessionID.ToString() + ".pdf";
                myDiskFileDestinationOptions = new CrystalDecisions.Shared.DiskFileDestinationOptions();
                myDiskFileDestinationOptions.DiskFileName = myExportFile;
                myExportOptions = ReportFile.ExportOptions;

                myExportOptions.DestinationOptions = myDiskFileDestinationOptions;
                myExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
                myExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
                ReportFile.Export();
                //Sabir Khan 9255 05/06/2011 Need to close and dispose Report Document Object.
                ReportFile.Close();
                ReportFile.Dispose();

                response.ClearContent();
                response.ClearHeaders();
                response.ContentType = "application/pdf";
                response.WriteFile(myExportFile);
                response.Flush();
                response.Close();

                System.IO.File.Delete(myExportFile);
            }
        }


        public string CreateReport(DataSet dsReport, string PhysicalPathWithFileName, string TargetFolder, System.Web.SessionState.HttpSessionState session, System.Web.HttpResponse response)
        {
            string myExportFile;
            try
            {
                //DataSet  dsReport =	Cls_DAB.Get_DS_BySPArr(StoredProcedure,keys,values);

                //Server.MapPath 
                if (!(Directory.Exists(TargetFolder)))
                {
                    Directory.CreateDirectory(TargetFolder);
                }

                ReportDocument ReportFile = new ReportDocument();
                ReportFile.Load(PhysicalPathWithFileName);
                ReportFile.SetDataSource(dsReport.Tables[0]);

                CrystalDecisions.Shared.ExportOptions myExportOptions;
                CrystalDecisions.Shared.DiskFileDestinationOptions myDiskFileDestinationOptions;



                myExportFile = TargetFolder + "\\" + session.SessionID.ToString() + ".pdf";
                myDiskFileDestinationOptions = new CrystalDecisions.Shared.DiskFileDestinationOptions();
                myDiskFileDestinationOptions.DiskFileName = myExportFile;
                myExportOptions = ReportFile.ExportOptions;

                myExportOptions.DestinationOptions = myDiskFileDestinationOptions;
                myExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
                myExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
                ReportFile.Export();
                //Sabir Khan 9255 05/06/2011 Need to close and dispose Report Document Object.
                ReportFile.Close();
                ReportFile.Dispose();


                //				response.ClearContent(); 
                //				response.ClearHeaders(); 
                //				response.ContentType = "application/pdf"; 
                //				response.WriteFile(myExportFile); 

                session["DocID"] = myExportFile;
                //response.Redirect("LoadPDF.aspx");


                return myExportFile;
                //response.Flush(); 
                //response.Close(); 

                //System.IO.File.Delete(myExportFile); 


            }
            catch (System.Exception ex)
            {
                response.Write("Exception...! " + ex.Message);
                return myExportFile = "";
            }
            finally
            {
                //	System.IO.Directory dir; 



            }
        }
        //For Email
        public string CreateEmail(string filename, string StoredProcedure, string[] key, object[] value1, System.Web.SessionState.HttpSessionState session, System.Web.HttpResponse response)
        {

            System.Data.DataSet DS1 = null;
            string myExportFile;
            try
            {
                DS1 = Cls_DAB.Get_DS_BySPArr(StoredProcedure, key, value1);
            }
            catch (System.Exception ex)
            {
                response.Write("Exception...! " + ex.Message);
            }
            finally
            {
                //	System.IO.Directory dir; 
                if (!(Directory.Exists("C:\\Temp")))
                {
                    Directory.CreateDirectory("C:\\Temp");
                }
                ReportDocument ReportFile = new ReportDocument();

                ReportFile.Load(filename);
                ReportFile.SetDataSource(DS1.Tables[0]);

                CrystalDecisions.Shared.ExportOptions myExportOptions;
                CrystalDecisions.Shared.DiskFileDestinationOptions myDiskFileDestinationOptions;

                //				string myExportFile;

                myExportFile = "C:\\Temp\\" + session.SessionID.ToString() + ".pdf";
                myDiskFileDestinationOptions = new CrystalDecisions.Shared.DiskFileDestinationOptions();
                myDiskFileDestinationOptions.DiskFileName = myExportFile;
                myExportOptions = ReportFile.ExportOptions;

                myExportOptions.DestinationOptions = myDiskFileDestinationOptions;
                myExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
                myExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
                ReportFile.Export();
                //Sabir Khan 9255 05/06/2011 Need to close and dispose Report Document Object.
                ReportFile.Close();
                ReportFile.Dispose();

                response.ClearContent();
                response.ClearHeaders();
                response.ContentType = "application/pdf";
                response.WriteFile(myExportFile);
                response.Flush();
                response.Close();

                //Waqas 5933 05/22/2009 Deleting file
                System.IO.File.Delete(myExportFile);
            }
            return myExportFile;
        }
        // Task Id 3595 05/21/2008 Agha Usman
        private DataTable manageDatatable(DataTable dt)
        {
            string folderPath = ConfigurationManager.AppSettings["NTPATHScanImage"];

            int i = 0;
            for (i = 0; i <= dt.Rows.Count - 1; i++)
            {
                if (Convert.IsDBNull(dt.Rows[i]["filename"]) == false)
                {
                    string ImageFile = Path.Combine(folderPath, dt.Rows[i]["filename"].ToString());

                    FileStream fs = new FileStream(ImageFile, FileMode.Open);
                    BinaryReader br = new BinaryReader(fs);
                    dt.Rows[i]["imgdata"] = br.ReadBytes(int.Parse(br.BaseStream.Length.ToString()));
                    br = null;
                    fs.Close();
                    fs = null;
                }
            }
            return dt;
        }

        //Nasir 6181 07/25/2009 create report for lor        
        public void CreateApprovalReportLOR(int ticketno, int empid, int lettertype, string FileName, HttpSessionState session, HttpResponse response)
        {

            string[] key = { "@ticketid", "@empid" };
            object[] value1 = { ticketno, empid };
            string filepath;

            if (lettertype == 6)
            {
                filepath = CreateReport(FileName, "USP_HTS_LETTER_OF_REP", key, value1, lettertype, empid, session, response);
                session["reportpath"] = filepath;
            }

        }

        //Yasir Kamal 7590 03/29/2010 trial letter modifications
        /// <summary>
        /// This method is used to insert batch info.
        /// </summary>
        /// <param name="TicketID">Ticket ID</param>
        /// <param name="EmpID">Employee ID</param>
        /// <param name="LetterID">Letter ID</param>
        /// <param name="myExportFile">Path of File created in Temp folder</param>
        /// <param name="BatchType">Batch Type</param>
        /// <param name="Doctype">Document type</param>
        /// <param name="filename">Report File name</param>
        /// <param name="TicketIDBatch">Ticket ID Batch</param>
        /// <returns>File Path</returns>
        private string InsertIntoBatch(string TicketID, int EmpID, int LetterID, string myExportFile, string BatchType, string Doctype, string filename, string TicketIDBatch)
        {
            try
            {
                clsBatch batch = new clsBatch();
                string vpat = ConfigurationSettings.AppSettings["NTPATHBatchPrint"].ToString();

                BatchLetterType batchLetterType = (BatchLetterType)(LetterID);

                DataSet RptInfo = GetReportName(LetterID);

                string fname = vpat;
                string Datetime = Convert.ToString(DateTime.Now).Replace(":", "").Replace("/", "");
                string ReportName = RptInfo.Tables[0].Rows[0]["LetterName"].ToString() + Doctype;
                string filenameonly = string.Empty;

                filenameonly = batch.updateBatchPrintInformation(batchLetterType, TicketID.ToString(), EmpID, TicketIDBatch, Datetime, ReportName);

                string FilePath = fname + filenameonly;


                Doc doc = new Doc();
                doc.Read(myExportFile);
                int pagecount = doc.PageCount;
                doc.Dispose();


                if (System.IO.File.Exists(FilePath) == true)
                {
                    System.IO.File.Delete(FilePath);
                    System.IO.File.Copy(myExportFile, FilePath);
                }
                else
                {
                    System.IO.File.Copy(myExportFile, FilePath);
                }

                System.IO.File.Delete(myExportFile);
                return FilePath;
            }
            catch
            {
                return "";

            }
        }

        /// <summary>
        /// This method is used to create report
        /// </summary>
        /// <param name="filename">Report File Name</param>
        /// <param name="StoredProcedure">Name of the procedure to be executed</param>
        /// <param name="key">String Array Containing the parameter list</param>
        /// <param name="value1">Object Array Containing the parameter values</param>
        /// <param name="session">Session State</param>
        /// <param name="response">Response</param>
        /// <param name="LetterType">Letter Type</param>
        /// <param name="TicketIDBatch">TicketIDBatch</param>
        /// <param name="Batch">Batch</param>
        /// <param name="TicketID">Ticket ID</param>
        public void CreateReport(string filename, string StoredProcedure, string[] key, object[] value1, System.Web.SessionState.HttpSessionState session, System.Web.HttpResponse response, string LetterType, string TicketIDBatch, string Batch, string TicketID)
        {

            System.Data.DataSet DS1 = null;
            try
            {
                DS1 = Cls_DAB.Get_DS_BySPArr(StoredProcedure, key, value1);

            }
            catch (System.Exception ex)
            {
                response.Write("Exception...! " + ex.Message);
            }
            finally
            {

                if (!(Directory.Exists("C:\\Temp")))
                {
                    Directory.CreateDirectory("C:\\Temp");
                }
                ReportDocument ReportFile = new ReportDocument();

                ReportFile.Load(filename);

                ReportFile.SetDataSource(DS1.Tables[0]);

                //Farrukh 10148 04/19/2012 Added DataSource for TrialNotification sub report
                if (filename.ToLower().Contains("trial_notification"))
                {
                    ReportFile.Subreports["Trial_Notification_batch1.rpt"].SetDataSource(DS1.Tables[0]);
                }
                CrystalDecisions.Shared.ExportOptions myExportOptions;
                CrystalDecisions.Shared.DiskFileDestinationOptions myDiskFileDestinationOptions;

                string myExportFile;

                myExportFile = "C:\\Temp\\" + session.SessionID.ToString() + ".pdf";
                myDiskFileDestinationOptions = new CrystalDecisions.Shared.DiskFileDestinationOptions();
                myDiskFileDestinationOptions.DiskFileName = myExportFile;
                myExportOptions = ReportFile.ExportOptions;

                myExportOptions.DestinationOptions = myDiskFileDestinationOptions;
                myExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
                myExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
                ReportFile.Export();
                //Sabir Khan 9255 05/06/2011 Need to close and dispose Report Document Object.
                ReportFile.Close();
                ReportFile.Dispose();

                response.ClearContent();
                response.ClearHeaders();
                response.ContentType = "application/pdf";
                response.WriteFile(myExportFile);
                response.Flush();
                response.Close();

                InsertIntoBatch(TicketID, Convert.ToInt32(value1[0]), Convert.ToInt32(LetterType), myExportFile, "True", ".pdf", filename, TicketIDBatch);

                System.IO.File.Delete(myExportFile);
            }
        }

        /// <summary>
        /// This method is used to insert batch info
        /// </summary>
        /// <param name="TicketID">Ticket ID</param>
        /// <param name="EmpID">Employee ID</param>
        /// <param name="LetterID">Letter ID</param>
        public void InsertIntoBatch(int TicketID, int EmpID, int LetterID)
        {
            try
            {

                string[] keys = { "@TicketID_FK", "@BatchDate", "@PrintDate", "@IsPrinted", "@LetterID_FK", "@EmpID", "@DocPath" };

                object[] values = { TicketID, System.DateTime.Now, "1/1/1900", 0, LetterID, EmpID, "" };
                Cls_DAB.InsertBySPArr("USP_HTS_Insert_BatchPrintLetter", keys, values);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        // Afaq 7154 07/28/2010 
        /// <summary>
        /// Use this method for deleting temporary generated trial docket pdf.
        /// </summary>
        /// <param name="path">path of file</param>
        public static void deleteTempTrialDocket(string path)
        {
            if (!string.IsNullOrEmpty(path))
            {
                FileInfo info = new FileInfo(path);
                DirectoryInfo dir = new DirectoryInfo(info.DirectoryName);
                FileInfo[] files = dir.GetFiles();
                foreach (FileInfo k in files)
                {
                    if (k.CreationTime.Date != DateTime.Now.Date)
                    {
                        try
                        {
                            k.Delete();
                        }
                        catch
                        {

                        }

                    }
                }
            }
            else
            {
                return;
            }

        }


        #endregion
    }

}

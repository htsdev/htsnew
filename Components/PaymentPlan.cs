using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;


namespace lntechNew.Components
{

    public class PaymentPlan
    {
        clsENationWebComponents ClsDb = new clsENationWebComponents("Connection String");

        #region " User defined Variables"

        private DateTime dsdate;
        private DateTime dedate;
        private int courttype = 0;
        private int critaria = 0;
        private int chk;
        private int ticketid;
        private string generalcomments = string.Empty;
        #endregion

        #region "Properties"

        public DateTime DsDate
        {
            set
            {
                dsdate = value;
            }
            get
            {
                return dsdate;
            }
        }

        public DateTime DeDate
        {
            set
            {
                dedate = value;
            }
            get
            {
                return dedate;
            }
        }
        public int CourtType
        {
            set
            {
                courttype = value;
            }
            get
            {
                return courttype;
            }
        }

        public int Criteria
        {
            set
            {
                critaria = value;
            }
            get
            {
                return critaria;
            }
        }

        public int CHK
        {
            set
            {
                chk = value;
            }
            get
            {
                return chk;
            }
        }

        public int TicketID
        {
            set
            {
                ticketid = value;
            }
            get
            {
                return ticketid;
            }
        }

        public string GeneralComments
        {
            set
            {
                generalcomments = value;
            }
            get
            {
                return generalcomments;
            }
        }
        #endregion

        #region " Methods"

        public DataSet GetPaymentPlans()
        {
            try
            {
                string[] keys = { "@dsdata", "@dedate", "@critaria", "@chk", "@CourtType" };
                object[] values = { dsdate, dedate, critaria, chk, courttype };

                DataSet ds = ClsDb.Get_DS_BySPArr("USP_HTS_GET_PAYMENTPLANREPORT", keys, values);

                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetPaymentDetail()
        {
            try
            {
                string[] keys = { "@TicketID" };
                object[] values = { ticketid };

                DataSet ds = new DataSet();

                //if (ticketid == null)
                //    return ds;
                if (ticketid <= 0)
                    return ds;

                ds = ClsDb.Get_DS_BySPArr("usp_hts_get_PaymentDetail", keys, values);

                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetPPlan()
        {
            try
            {
                string[] keys = { "@TicketID" };
                object[] values = { ticketid };

                DataSet ds = new DataSet();

                //if (ticketid == null)
                //    return ds;
                if (ticketid <= 0)
                    return ds;

                ds = ClsDb.Get_DS_BySPArr("USP_HTS_Get_PaymentPlan", keys, values);

                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet UpdatComments()
        {
            try
            {
                string[] keys = { "@TicketID", "@GeneralComments" };
                object[] values = { ticketid, generalcomments };

                DataSet ds = new DataSet();

                //if (ticketid == null)
                //    return ds;
                if (ticketid <= 0)
                    return ds;

                ds = ClsDb.Get_DS_BySPArr("USP_HTS_UPDATE_GeneralComments", keys, values);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int UpdatePaymentFollowUpdate(int ticketid, DateTime followupdate, int empid)
        {
            string[] keys = { "@ticketid_pk", "@paymentfollowupdate", "@EmpID" };
            object[] values = { ticketid, followupdate, empid };
            return Convert.ToInt32(ClsDb.ExecuteScalerBySp("USP_HTP_Update_PaymentFollowupdate", keys, values));
        }


        //Fahad 6638 11/10/2009 parameter Criteria added    
        /// <summary>
        /// This method returns clients records whose payment follow update has been passed
        /// </summary>
        /// <param name="Criteria">This is Criteria for the CaseType.</param>
        /// <returns>DataTable</returns>
        public DataTable GetPastDueReports(int Criteria)
        {
            //Sabir Khan 7497 06/24/2010 parameter added for validation report...
            string[] key = { "@Criteria","@validation" };//Fahad 6638 11/10/2009 parameter Criteria added
            object[] value1 = { Criteria,false };//Fahad 6638 11/10/2009 parameter Criteria added
            return (ClsDb.Get_DT_BySPArr("USP_HTP_Get_Past_Due_Calls",key,value1));
        }

        /// <summary>
        ///     Noufil 5618 04/01/2009 Get Payment Due report
        ///     This method returns clients records whose payment follow update is in Future
        /// </summary>
        /// <returns>DataTable</returns>
        public DataTable GetPaymentDueReports()
        {
            return (ClsDb.Get_DT_BySPArr("USP_HTP_Get_Payment_Due_Calls"));
        }


        #endregion
    }
}

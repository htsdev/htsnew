﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using FrameWorkEnation.Components;

namespace lntechNew.Components
{
    // Noufil 3999 05/20/2008 Class for fax report
    public class faxclass
    {
        clsENationWebComponents clsdb = new clsENationWebComponents();

        /// <summary>
        /// This function add new entry in the faxletter table
        /// </summary>
        /// <param name="ticketid"></param>
        /// <param name="empid"></param>
        /// <param name="ename"></param>
        /// <param name="eemail"></param>
        /// <param name="faxno"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <param name="status"></param>
        /// <param name="faxdate"></param>
        /// <param name="docpath"></param>
        /// <returns></returns>

        public int addlfaxletter(int ticketid, int empid, string ename, string eemail, string faxno, string subject, string body, string status, DateTime faxdate,string docpath)
        {
            // Noufil 3999 05/20/2008 Add fax entry into database

            string[] keys = { "@ticketid", "@empid", "@ename", "@eemail", "@faxno", "@subject", "@body", "@status", "@faxdate", "@docpath", "@output" };
            object[] values = { ticketid, empid, ename, eemail, faxno, subject, body, status, faxdate, docpath,0 };
            //clsdb.ExecuteSP("Usp_Htp_insertintofaxletter", keys, values);
            //Sabir Khan 9917 11/18/2011 Procedure name has been changed.
            object[] obj = clsdb.InsertBySPArrRet("Usp_Htp_insertintofaxletter_Faxmaker", keys, values, 1);
            int outp = int.Parse(obj[0].ToString());
            return outp;            
        }

        /// <summary>
        /// This fuction returns number of rows thats exist in the table faxletter
        /// </summary>
        /// <returns>count</returns>
        public string getfaxcount(int ticketid)
        {
            // Noufil 3999 05/20/2008 get the maximum id for the fax
            string[] keys = { "@ticketid" };
            object[] values = { ticketid };
            //string faxcountt = 
            object result=clsdb.ExecuteScalerBySp("USP_HTP_faxcount", keys, values);
            return result.ToString();
            //faxcountt;
        }

        /// <summary>
        /// This function returns records for fax report
        /// </summary>
        /// <param name="status"></param>
        /// <param name="fromdate"></param>
        /// <param name="todate"></param>
        /// <returns>DataTable with or without records</returns>
        public DataTable GetFaxReport(int status,DateTime fromdate,DateTime todate)
        {
            // Noufil 3999 05/20/2008 Return fax report 
            string[] keys = { "@statusid", "@fromdate", "@todate" };
            object[] values = { status, fromdate.ToShortDateString(), todate.ToShortDateString() };
            //Sabir Khan 9917 11/18/2011 Procedure name has been changed.
            DataTable dt = clsdb.Get_DT_BySPArr("Usp_Htp_Faxreport_faxmaker",keys,values);
             return dt;
        }
        /// <summary>
        /// This function returns records for Jp Appearance Report
        /// </summary>
        /// <returns>DataTable with or without records</returns>
        public DataTable GetJpAppearancereport()
        {
            // Noufil 3999 05/20/2008 Return JP Appearance Fax Report
            DataTable dt = clsdb.Get_DT_BySPArr("USP_HTP_JpAppFaxreport");
            return dt;
        }

    }
}

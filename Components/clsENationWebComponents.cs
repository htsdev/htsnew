using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using Microsoft.VisualBasic;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using System.Web.UI.WebControls;



namespace FrameWorkEnation.Components
{
    /// <summary>
    /// CLSEnation Web Component is as database component used to execute Stored Procedures from various type of parameters
    /// and return Dataset , Datatables 
    /// 
    /// HOW TO USE :
    /// 
    /// Step 1:
    ///      
    /// 
    /// Step 2 :
    ///      Create an object of clsEnationWebComponent in your class 
    ///      eg : 
    /// 
    /// Step 3: 
    ///      Now you can use any method of clsEnationWebComponent depending upon requirement 
    /// 
    ///  
    /// 
    /// </summary>


    public enum TableOption
    {
        /// <summary>
        /// None
        /// </summary>
        [Description("None")]
        None = 0,
        /// <summary>
        /// Add
        /// </summary>
        [Description("Add")]
        Add = 1,
        /// <summary>
        /// Edit
        /// </summary>
        [Description("Edit")]
        Edit = 2,
        /// <summary>
        /// Delete
        /// </summary>
        [Description("Delete")]
        Delete = 3,
    }

    /// <summary>
    /// This Class is worked as a Data Access Layer.
    /// </summary>
    /// <remarks>
    /// To Use This Class follow the following steps
    /// 
    /// 1. Add the Namespace lntechNew.Components in your file;
    /// 2. Create Object of the Class
    ///     clsEnationWebComponent clsdb = new clsEnationWebComponent();
    /// </remarks>
    public class clsENationWebComponents
    {
        /// <summary>
        /// Generic Database Object
        /// </summary>
        Database db;

        /// <summary>
        /// Variable Used for SQL command
        /// </summary>
        string sqlCommand = "";

        /// <summary>
        /// Generic DataReader Object
        /// </summary>
        IDataReader DR;

        /// <summary>
        /// Generic Connection Object
        /// </summary>
        private DbConnection connection;
        /// <summary>
        /// Generic Transaction Object
        /// </summary>
        private DbTransaction transaction;

        /// <summary>
        /// Transaction Object 
        /// </summary>
        private bool istransaction = false;

        /// <summary>
        /// Type DataSet Object
        /// </summary>
        lntechNew.Database.DSintechNew dstCustom = new lntechNew.Database.DSintechNew();

        /// <summary>
        /// DataTable Variable used by the class
        /// </summary>
        DataTable dtlTarget;

        /// <summary>
        /// DataRow Variable used by the class
        /// </summary>
        DataRow dtwTarget;

        /// <summary>
        /// Default Constructor
        /// </summary>
        /// <example>
        /// <code>
        /// //Creates A Database Connection Object with the default database
        /// clsENationWebComponents ClsDb = new clsENationWebComponents();
        /// </code>
        /// </example>
        public clsENationWebComponents()
            : this("", false)
        {
            //    db = DatabaseFactory.CreateDatabase();
        }

        /// <summary>
        /// Create Database By Database Name
        /// </summary>
        /// <param name="DatabaseName">Name of the database which is used for the database operations</param>
        /// <example>
        /// <code>
        /// //Creates A Database Connection Object with the specified database object
        /// clsENationWebComponents ClsDb = new clsENationWebComponents("traffic");
        /// </code>
        /// </example>
        public clsENationWebComponents(string DatabaseName)
            : this(DatabaseName, false)
        {
            //db = DatabaseFactory.CreateDatabase(DatabaseName);
        }

        /// <summary>
        /// Create Database and enable transaction
        /// </summary>
        /// <param name="Transaction">Take boolean value indicating the transaction to be enabled on the database.</param>
        /// <example>
        /// <code>
        /// //Creates A Database Connection Object with the default database and enable a transaction
        /// clsENationWebComponents ClsDb = new clsENationWebComponents(true);
        /// </code>
        /// </example>
        public clsENationWebComponents(bool Transaction)
            : this("", Transaction)
        {
            //db = DatabaseFactory.CreateDatabase();
            //this.IsTransaction = true;

            //connection = db.CreateConnection();
            //connection.Open();
            //transaction = connection.BeginTransaction();

            /*
            using (connection = db.GetConnection())
            {
                connection.Open();
                transaction = connection.BeginTransaction();
            }*/

        }

        /// <summary>
        /// Create Database By Database Name and Enable a transaction
        /// </summary>
        /// <param name="DatabaseName">Name of the database which is used for the database operations</param>
        /// <param name="Transaction">Take boolean value indicating the transaction to be enabled on the database.</param>
        /// <example>
        /// <code>
        /// //Creates A Database Connection Object with the specified database and enable a transaction
        /// clsENationWebComponents ClsDb = new clsENationWebComponents("Houston",true);
        /// </code>
        /// </example>
        public clsENationWebComponents(string DatabaseName, bool Transaction)
        {
            if (string.IsNullOrEmpty(DatabaseName))
                db = DatabaseFactory.CreateDatabase();
            else
                db = DatabaseFactory.CreateDatabase(DatabaseName);

            if (Transaction)
            {
                this.IsTransaction = true;
                connection = db.CreateConnection();
                connection.Open();
                transaction = connection.BeginTransaction();
            }
        }

        /// <summary>
        /// Release resources held by the object
        /// </summary>
        ~clsENationWebComponents()
        {
            db = null;
            dstCustom = null;
            connection = null;
            transaction = null;
            DR = null;
        }

        /// <summary>
        /// Sets or Gets the IsTransaction Variable. 
        /// </summary>
        public bool IsTransaction
        {
            get
            {
                return istransaction;	// return the value from privte field.
            }
            set
            {
                istransaction = value;	// save value into private field.
            }
        }


        # region BeginTransaction

        /// <summary>
        /// Create a connection with database and begin the transaction.
        /// </summary>
        public void DBBeginTransaction()
        {
            this.IsTransaction = true;

            connection = db.CreateConnection();
            connection.Open();
            transaction = connection.BeginTransaction();

        }

        /// <summary>
        /// Create a connection with the database and returns the transaction oject
        /// </summary>
        /// <returns></returns>
        public DbTransaction DBBeginAndReturnTransaction()
        {
            this.IsTransaction = true;

            connection = db.CreateConnection();
            connection.Open();
            transaction = connection.BeginTransaction();
            return transaction;
        }

        /// <summary>
        /// Used to Commit the transaction
        /// </summary>
        public void DBTransactionCommit()
        {
            transaction.Commit();
        }

        /// <summary>
        /// Used to Rollback the transaction
        /// </summary>
        public void DBTransactionRollback()
        {
            transaction.Rollback();
        }

        #endregion

        // Miscellaneous Functions ********************* 

        /// <summary>
        /// Get the type of parameter
        /// </summary>
        /// <param name="strType">string value representing a specified type ( i.e. boolean , int32 etc )</param>
        /// <returns>Return the system type represented by the string</returns>
        private System.Data.DbType GetDBType(string strType)
        {
            switch (strType.Trim())
            {
                case "Binary":
                    return System.Data.DbType.Binary;

                case "Boolean":
                    return System.Data.DbType.Boolean;

                case "Byte":
                    return System.Data.DbType.Byte;

                case "Currency":
                    return System.Data.DbType.Currency;

                case "Date":
                    return System.Data.DbType.Date;

                case "DateTime":
                    return System.Data.DbType.DateTime;

                case "Decimal":
                    return System.Data.DbType.Decimal;

                case "Double":
                    return System.Data.DbType.Double;

                case "Int16":
                    return System.Data.DbType.Int16;

                case "Int32":
                    return System.Data.DbType.Int32;

                case "Int64":
                    return System.Data.DbType.Int64;

                case "SByte":
                    return System.Data.DbType.SByte;

                case "Single":
                    return System.Data.DbType.Single;

                case "String":
                    return System.Data.DbType.String;

                case "StringFixedLength":
                    return System.Data.DbType.StringFixedLength;

                case "Time":
                    return System.Data.DbType.Time;

                case "UInt16":
                    return System.Data.DbType.UInt16;

                case "UInt32":
                    return System.Data.DbType.UInt32;

                case "UInt64":
                    return System.Data.DbType.UInt64;

                case "VarNumeric":
                    return System.Data.DbType.VarNumeric;
                case "Byte[]":
                    return System.Data.DbType.Binary;   //SAEED 7931 06/23/2010, reutrn binary, if type is Image in db. 
                //return (System.Data.DbType)(System.Data.SqlDbType.Image);   
                default:
                    return System.Data.DbType.Int64;

            }
        }

        /// <summary>
        /// This Method simply executes the query written in basic sql syntax.
        /// </summary>
        /// <param name="strQuery">
        /// Represents an SQL query 
        /// </param>
        /// <returns>
        /// Returns DataReader Object containing the selected records.
        /// </returns>
        /// <example>
        /// <code>
        /// //Create Database Connection
        /// clsENationWebComponents ClsDb = new clsENationWebComponents();
        /// 
        /// //Execute Query
        /// DataReader dr = ClsDb.Get_All_RecordsByQuery("Select * from tbltickets");
        /// </code>
        /// </example>
        public IDataReader Get_All_RecordsByQuery(string strQuery)
        {

            //	DBCommandWrapper dbCommandWrapper  = db.GetStoredProcCommandWrapper(strQuery);			
            DbCommand dbCommand = db.GetSqlStringCommand(strQuery);
            DR = (IDataReader)db.ExecuteReader(dbCommand);
            return DR;
        }

        #region Functions For Fetching With SP and DataReader

        /// <summary>
        ///  This Method execute the Stored Procedure having no parameter and return the DataReader Object
        /// </summary>
        /// <param name="spName">
        /// Name of the procedure to be executed.
        /// </param>
        /// <returns> Returns DataReader Object representing the selected records</returns>
        /// <example>
        /// <code>
        /// //Create Database Connection
        /// clsENationWebComponents ClsDb = new clsENationWebComponents();
        /// 
        /// //Execute Stored Procedure
        /// DataReader dr = ClsDb.Get_DR_BySP("Procedure_Name_Having_No_Parameter");
        /// </code>
        /// </example>
        public IDataReader Get_DR_BySP(string spName)
        {
            //System.Data.DataSet 
            //DBCommandWrapper dbCommandWrapper  = db.GetStoredProcCommandWrapper(spName);			
            DbCommand dbCommand = db.GetStoredProcCommand(spName);
            return (IDataReader)db.ExecuteReader(dbCommand);
        }

        /// <summary>
        /// This Method execute the Stored Procedure having one or more parameter and return the DataReader Object
        /// </summary>
        /// <param name="spName">
        /// Name of the procedure to be executed
        /// </param>
        /// <param name="key">
        /// String Array Containing the parameter list
        /// </param>
        /// <param name="value1">
        /// Object Array Containing the parameter values
        /// </param>
        /// <returns>
        /// DataReader Object representing the selected records
        /// </returns>
        /// <example>
        /// <code>
        /// //Create Database Connection
        /// clsENationWebComponents ClsDb = new clsENationWebComponents();
        /// 
        /// //Paremeter List
        /// string[] keys = {"Parameter1" ,"Parameter2"};
        /// 
        /// //Parameter Values 
        /// object[] values = {"Parameter1 Value","Parameter2 Value"};
        /// 
        /// //Execute Stored Procedure
        /// DataReader dr = ClsDb.Get_DR_BySPArr("Procedure_Name_Having_Multiple_Parameters",keys,values);
        /// </code>
        /// </example>
        public IDataReader Get_DR_BySPArr(string spName, string[] key, object[] value1)
        {
            DbCommand dbCommand = db.GetStoredProcCommand(spName);
            // try
            //{
            for (int i = 0; i <= key.Length - 1; i++)
            {
                db.AddInParameter(dbCommand, (string)key.GetValue(i), GetDBType(value1.GetValue(i).GetType().Name), value1.GetValue(i));
            }
            return (IDataReader)db.ExecuteReader(dbCommand);
            //}
            //catch (Exception ex)
            //{
            //    string msg = ex.Message + ex.StackTrace;
            //    IDataReader dr = null;
            //    throw;
            //}
        }

        #endregion

        /// <summary>
        /// This method fill the DataGrid with data returned by the specified query.
        /// </summary>
        /// <param name="WebControlx">
        /// Object Variable representing the DataGrid
        /// </param>
        /// <param name="strQuery">
        /// Represents an SQL query 
        /// </param>
        /// <returns>
        /// Returns true if control is successfully filled with the data
        /// </returns>
        /// <example>
        /// <code>
        /// //Create Database Connection
        /// clsENationWebComponents ClsDb = new clsENationWebComponents();
        /// 
        /// //Creating DataGrid
        /// DataGrid dgResults;
        /// 
        /// if ( ClsDb.FetchValuesInWebControl(dgResults,"Select * from tbltickets"))
        /// {
        ///     //Perform task if datagrid loads with the data
        /// }
        /// 
        /// </code>
        /// </example>
        public bool FetchValuesInWebControl(object WebControlx, string strQuery)
        {
            //FetchValuesInWebControl(WebControlx, strQuery,""); 
            sqlCommand = strQuery;
            DbCommand dbCommandWrapper = db.GetSqlStringCommand(sqlCommand);

            if (WebControlx is DataGrid)
            {
                ((DataGrid)WebControlx).DataSource = db.ExecuteReader(dbCommandWrapper);
                ((DataGrid)WebControlx).DataBind();
                return true;
            }
            return false;
        }

        /// <summary>
        /// This method is used to fill DropDownList or  RadioButtonList web controls by sql query.
        /// </summary>
        /// <param name="WebControlx">Name of the List Control</param>
        /// <param name="strQuery">Represents an SQL query used to get the values for the List</param>
        /// <param name="strTextField">Represented Text Field to be set in the control</param>
        /// <param name="strValueField">Represented Value Field to be set in the control</param>
        /// <returns>Returns true if control is populated </returns>
        /// <example>
        /// <code>
        /// //Create Database Connection
        /// clsENationWebComponents ClsDb = new clsENationWebComponents();
        /// 
        /// DropDownList ddlUsers; 
        /// 
        /// //Execute Stored Procedure
        /// DataReader dr = ClsDb.FetchValuesInWebControl(ddlUsers,"Select * from tblUsers","UserName","UserID");
        /// </code>
        /// </example>
        public bool FetchValuesInWebControl(object WebControlx, string strQuery, string strTextField, string strValueField)
        {
            //FetchValuesInWebControl(WebControlx, strQuery, strTextField, strValueField, -1); 			
            sqlCommand = strQuery;
            DbCommand dbCommandWrapper = db.GetSqlStringCommand(sqlCommand);

            if (WebControlx is DropDownList)
            {
                ((DropDownList)WebControlx).DataTextField = strTextField;
                ((DropDownList)WebControlx).DataValueField = strValueField;

                ((DropDownList)WebControlx).DataSource = db.ExecuteReader(dbCommandWrapper);
                ((DropDownList)WebControlx).DataBind();
                return true;
            }
            if (WebControlx is RadioButtonList)
            {
                ((RadioButtonList)WebControlx).DataTextField = strTextField;
                ((RadioButtonList)WebControlx).DataValueField = strValueField;

                ((RadioButtonList)WebControlx).DataSource = db.ExecuteReader(dbCommandWrapper);
                ((RadioButtonList)WebControlx).DataBind();
                return true;
            }
            return false;
        }

        /// <summary>
        /// This method is used to fill DropDownList or  RadioButtonList web controls by sql query and selects a specified value in the control.
        /// </summary>
        /// <param name="WebControlx">Name of the List Control</param>
        /// <param name="strQuery">Represents an SQL query used to get the values for the List</param>
        /// <param name="strTextField">Represented Text Field to be set in the control</param>
        /// <param name="strValueField">Represented Value Field to be set in the control</param>
        /// <param name="intSelectedItem">Value to be selected in the control</param>
        /// <returns>Returns true if control is populated</returns>
        /// <example>
        /// <code>
        /// //Create Database Connection
        /// clsENationWebComponents ClsDb = new clsENationWebComponents();
        /// 
        /// DropDownList ddlUsers; 
        /// 
        /// //Execute Stored Procedure 
        /// //Fill The User List and Selects The Fist User
        /// DataReader dr = ClsDb.FetchValuesInWebControl(ddlUsers,"Select * from tblUsers","UserName","UserID",1);
        /// </code>
        /// </example>
        public bool FetchValuesInWebControl(object WebControlx, string strQuery, string strTextField, string strValueField, int intSelectedItem)
        {
            sqlCommand = strQuery;
            DbCommand dbCommandWrapper = db.GetSqlStringCommand(sqlCommand);

            if (WebControlx is DropDownList)
            {
                ((DropDownList)WebControlx).DataTextField = strTextField;
                ((DropDownList)WebControlx).DataValueField = strValueField;

                ((DropDownList)WebControlx).DataSource = db.ExecuteReader(dbCommandWrapper);
                ((DropDownList)WebControlx).DataBind();

                if ((((DropDownList)WebControlx).Items.Count >= intSelectedItem) && (intSelectedItem != -1))
                {
                    //((DropDownList)WebControlx).Items.FindByValue(intSelectedItem.ToString()).Selected  = true;					
                    ((DropDownList)WebControlx).SelectedValue = intSelectedItem.ToString();
                }
                return true;
            }

            if (WebControlx is RadioButtonList)
            {
                ((RadioButtonList)WebControlx).DataTextField = strTextField;
                ((RadioButtonList)WebControlx).DataValueField = strValueField;

                ((RadioButtonList)WebControlx).DataSource = db.ExecuteReader(dbCommandWrapper);
                ((RadioButtonList)WebControlx).DataBind();

                if ((((RadioButtonList)WebControlx).Items.Count >= intSelectedItem) && (intSelectedItem != -1))
                {

                    ((RadioButtonList)WebControlx).Items.FindByValue(intSelectedItem.ToString()).Selected = true;
                }
                return true;
            }

            return false;
        }

        /// <summary>
        /// This method fill the DataGrid with data returned by the specified procedure.
        /// </summary>
        /// <param name="WebControlx">
        /// Object Variable representing the DataGrid.
        /// </param>
        /// <param name="spName">
        /// Stored Procedure name which is used to fetch data.
        /// </param>
        /// <returns>Returns true if control is successfully filled with the data</returns> 
        /// <example>
        /// <code>
        /// //Create Database Connection
        /// clsENationWebComponents ClsDb = new clsENationWebComponents();
        /// 
        /// //Creating DataGrid
        /// DataGrid dgResults;
        /// 
        /// if ( ClsDb.FetchValuesInWebControl(dgResults,"ProcdureName") )
        /// {
        ///     //Perform task if datagrid loads with the data
        /// }
        /// 
        /// </code>
        /// </example>
        public bool FetchValuesInWebControlBysp(object WebControlx, string spName)
        {
            //FetchValuesInWebControl(WebControlx, strQuery,""); 
            //sqlCommand =strQuery;
            //DBCommandWrapper dbCommandWrapper  = db.GetSqlStringCommandWrapper(sqlCommand);
            DbCommand dbCommandWrapper = db.GetStoredProcCommand(spName);

            if (WebControlx is DataGrid)
            {
                ((DataGrid)WebControlx).DataSource = db.ExecuteReader(dbCommandWrapper);
                ((DataGrid)WebControlx).DataBind();
                return true;
            }
            return false;
        }

        /// <summary>
        /// This method fill the DataGrid with data returned by the specified procedure.
        /// </summary>
        /// <param name="WebControlx">
        /// Object Variable representing the DataGrid.
        /// </param>
        /// <param name="spName">
        /// Stored Procedure name which is used to fetch data.
        /// </param>
        /// <returns>Returns true if control is successfully filled with the data</returns> 
        /// <example>
        /// <code>
        /// //Create Database Connection
        /// clsENationWebComponents ClsDb = new clsENationWebComponents();
        /// 
        /// //Creating DataGrid
        /// DataGrid dgResults;
        /// 
        /// if ( ClsDb.FetchValuesInWebControlBySPWithDS(dgResults,"ProcdureName") )
        /// {
        ///     //Perform task if datagrid loads with the data
        /// }
        /// 
        /// </code>
        /// </example>
        public bool FetchValuesInWebControlBySPWithDS(object WebControlx, string spName)
        {
            //FetchValuesInWebControl(WebControlx, strQuery,""); 
            //sqlCommand =strQuery;
            //DBCommandWrapper dbCommandWrapper  = db.GetSqlStringCommandWrapper(sqlCommand);
            DbCommand dbCommandWrapper = db.GetStoredProcCommand(spName);

            if (WebControlx is DataGrid)
            {
                ((DataGrid)WebControlx).DataSource = db.ExecuteDataSet(dbCommandWrapper);
                ((DataGrid)WebControlx).DataBind();
                return true;
            }
            return false;
        }

        /// <summary>
        /// This method is used to fill DropDownList or  RadioButtonList web controls by a stored procedure.
        /// </summary>
        /// <param name="WebControlx">Name of the List Control</param>
        /// <param name="spName">Represents an SQL query used to get the values for the List</param>
        /// <param name="strTextField">Represented Text Field to be set in the control</param>
        /// <param name="strValueField">Represented Value Field to be set in the control</param>
        /// <returns>Returns true if control is populated </returns>
        /// <example>
        /// <code>
        /// //Create Database Connection
        /// clsENationWebComponents ClsDb = new clsENationWebComponents();
        /// 
        /// DropDownList ddlUsers; 
        /// 
        /// //Execute Stored Procedure
        /// DataReader dr = ClsDb.FetchValuesInWebControlBysp(ddlUsers,"Procedure_Name","UserName","UserID");
        /// </code>
        /// </example>
        public bool FetchValuesInWebControlBysp(object WebControlx, string spName, string strTextField, string strValueField)
        {

            DbCommand dbCommandWrapper = db.GetStoredProcCommand(spName);


            if (WebControlx is DropDownList)
            {
                ((DropDownList)WebControlx).DataTextField = strTextField;
                ((DropDownList)WebControlx).DataValueField = strValueField;

                ((DropDownList)WebControlx).DataSource = db.ExecuteReader(dbCommandWrapper);
                ((DropDownList)WebControlx).DataBind();
                return true;
            }
            if (WebControlx is RadioButtonList)
            {
                ((RadioButtonList)WebControlx).DataTextField = strTextField;
                ((RadioButtonList)WebControlx).DataValueField = strValueField;

                ((RadioButtonList)WebControlx).DataSource = db.ExecuteReader(dbCommandWrapper);
                ((RadioButtonList)WebControlx).DataBind();
                return true;
            }
            if (WebControlx is ListBox)
            {
                ((ListBox)WebControlx).DataTextField = strTextField;
                ((ListBox)WebControlx).DataValueField = strValueField;

                ((ListBox)WebControlx).DataSource = db.ExecuteReader(dbCommandWrapper);
                ((ListBox)WebControlx).DataBind();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Description Not Available
        /// </summary>
        /// <param name="ctlControl">DataGrid Control to be filled with the data</param>
        /// <param name="strTable">Name of the DataTable</param>
        /// <param name="intColumnNo">Column Number</param>
        public void InitGrid(DataGrid ctlControl, string strTable, int intColumnNo)
        {
            dtlTarget = dstCustom.Tables[strTable];
            dtwTarget = dtlTarget.NewRow();
            dtlTarget.Rows.Add(dtwTarget);
            dstCustom.AcceptChanges();
            ctlControl.DataSource = dtlTarget;
            ctlControl.DataBind();
        }

        /// <summary>
        /// This method is used to fill DropDownList or  RadioButtonList web controls by a stored procedure and selects a specified value in the control.
        /// </summary>
        /// <param name="WebControlx">Name of the List Control</param>
        /// <param name="spName">Represents an SQL query used to get the values for the List</param>
        /// <param name="strTextField">Represented Text Field to be set in the control</param>
        /// <param name="strValueField">Represented Value Field to be set in the control</param>
        /// <param name="intSelectedItem">Value to be selected in the control</param>
        /// <returns>Returns true if control is populated</returns>
        /// <example>
        /// <code>
        /// //Create Database Connection
        /// clsENationWebComponents ClsDb = new clsENationWebComponents();
        /// 
        /// DropDownList ddlUsers; 
        /// 
        /// //Execute Stored Procedure 
        /// //Fill The User List and Selects The Fist User
        /// DataReader dr = ClsDb.FetchValuesInWebControlBysp(ddlUsers,"Procedure_Name","UserName","UserID",1);
        /// </code>
        /// </example>
        public bool FetchValuesInWebControlBysp(object WebControlx, string spName, string strTextField, string strValueField, int intSelectedItem)
        {

            DbCommand dbCommandWrapper = db.GetStoredProcCommand(spName);

            if (WebControlx is DropDownList)
            {
                ((DropDownList)WebControlx).DataTextField = strTextField;
                ((DropDownList)WebControlx).DataValueField = strValueField;

                ((DropDownList)WebControlx).DataSource = db.ExecuteReader(dbCommandWrapper);
                ((DropDownList)WebControlx).DataBind();

                if ((((DropDownList)WebControlx).Items.Count >= intSelectedItem) && (intSelectedItem != -1))
                {
                    //((DropDownList)WebControlx).Items.FindByValue(intSelectedItem.ToString()).Selected  = true;					
                    ((DropDownList)WebControlx).SelectedValue = intSelectedItem.ToString();
                }
                return true;
            }

            if (WebControlx is RadioButtonList)
            {
                ((RadioButtonList)WebControlx).DataTextField = strTextField;
                ((RadioButtonList)WebControlx).DataValueField = strValueField;

                ((RadioButtonList)WebControlx).DataSource = db.ExecuteReader(dbCommandWrapper);
                ((RadioButtonList)WebControlx).DataBind();

                if ((((RadioButtonList)WebControlx).Items.Count >= intSelectedItem) && (intSelectedItem != -1))
                {

                    ((RadioButtonList)WebControlx).Items.FindByValue(intSelectedItem.ToString()).Selected = true;
                }
                return true;
            }
            if (WebControlx is ListBox)
            {
                ((ListBox)WebControlx).DataTextField = strTextField;
                ((ListBox)WebControlx).DataValueField = strValueField;

                ((ListBox)WebControlx).DataSource = db.ExecuteReader(dbCommandWrapper);
                ((ListBox)WebControlx).DataBind();

                if ((((ListBox)WebControlx).Items.Count >= intSelectedItem) && (intSelectedItem != -1))
                {

                    ((ListBox)WebControlx).Items.FindByValue(intSelectedItem.ToString()).Selected = true;
                }
                return true;
            }

            return false;
        }

        /// <summary>
        /// This method gets DataReader Object returned by the specified procedure having a single parameter.
        /// </summary>
        /// <param name="spName">
        /// Name of the Stored Procedure
        /// </param>
        /// <param name="key">
        /// Name of the Procedure Parameter
        /// </param>
        /// <param name="value1">
        /// Paremeter Value
        /// </param>
        /// <returns>
        ///DataReader Object representing the selected records
        /// </returns>
        /// <example>
        /// <code>
        /// //Create Database Connection
        /// clsENationWebComponents ClsDb = new clsENationWebComponents();
        /// 
        /// //Get Records
        /// DataReader dr = ClsDb.Get_DR_BySPByOneParmameter("ProcedureName","ParameterName","ParameterValue");
        /// 
        /// while ( dr.Read())
        /// {
        ///     //Process Records
        /// }
        /// 
        /// </code>
        /// </example>
        public IDataReader Get_DR_BySPByOneParmameter(string spName, string key, object value1)
        {
            //try
            //{
            DbCommand dbCommand = db.GetStoredProcCommand(spName);
            db.AddInParameter(dbCommand, (string)key, GetDBType(value1.GetType().Name), value1);
            return (IDataReader)db.ExecuteReader(dbCommand);
            //}
            //catch (Exception ex)
            //{
            //    string exmsg = ex.Message;
            //    IDataReader DR = null;
            //    return DR;
            //}
        }

        /// <summary>
        /// This method gets DataReader Object returned by the specified procedure having two parameters.
        /// </summary>
        /// <param name="spName">
        /// Name of the Stored Procedure
        /// </param>
        /// <param name="ParmName">
        /// Name of the First Procedure Parameter 
        /// </param>
        /// <param name="ParmValue">
        /// First Paremeter  Value
        /// </param>
        /// <param name="ParmName2">Name of the Second Procedure Parameter</param>
        /// <param name="ParmValue2">Second Parameter Value</param>
        /// <returns>
        ///DataReader Object representing the selected records
        /// </returns>
        /// <example>
        /// <code>
        /// //Create Database Connection
        /// clsENationWebComponents ClsDb = new clsENationWebComponents();
        /// 
        /// //Get Records
        /// DataReader dr = ClsDb.Get_DR_BySPByTwoParmameter("ProcedureName","FirstParameterName","FirstParameterValue","SecondParameterName","SecondParameterValue");
        /// 
        /// while ( dr.Read())
        /// {
        ///     //Process Records
        /// }
        /// 
        /// </code>
        /// </example>
        public IDataReader Get_DR_BySPByTwoParmameter(string spName, string ParmName, object ParmValue, string ParmName2, object ParmValue2)
        {

            DbCommand dbCommand = db.GetStoredProcCommand(spName);
            db.AddInParameter(dbCommand, ParmName, GetDBType(ParmValue.GetType().Name), ParmValue);
            db.AddInParameter(dbCommand, ParmName2, GetDBType(ParmValue2.GetType().Name), ParmValue2);

            DR = (IDataReader)db.ExecuteReader(dbCommand);
            return DR;
        }

        /// <summary>
        /// This method gets delete records by specified procedure having a single parameter.
        /// </summary>
        /// <param name="spName">
        /// Name of the Stored Procedure
        /// </param>
        /// <param name="ParmName">
        /// Name of the Procedure Parameter
        /// </param>
        /// <param name="ParmValue">
        /// Paremeter Value
        /// </param>
        /// <returns>Return true if delete operation performed successfully</returns>
        /// <example>
        /// <code>
        /// //Create Database Connection
        /// clsENationWebComponents ClsDb = new clsENationWebComponents();
        /// 
        /// //Get Records
        ///  if ( ClsDb.DeleteBySPByOneParmameter("ProcedureName","ParameterName","ParameterValue"))
        ///  {
        ///     //Record has been deleted
        ///  }
        /// 
        /// </code>
        /// </example>
        public bool DeleteBySPByOneParmameter(string spName, string ParmName, object ParmValue)
        {
            DbCommand dbCommand = db.GetStoredProcCommand(spName);
            db.AddInParameter(dbCommand, ParmName, GetDBType(ParmValue.GetType().Name), ParmValue);

            db.ExecuteNonQuery(dbCommand);
            return true;
        }

        #region DATASET OPERATIONS

        /// <summary>
        ///  This Method execute the Stored Procedure having no parameter and return the DataSet Object
        /// </summary>
        /// <param name="spName">
        /// Name of the procedure to be executed.
        /// </param>
        /// <returns> Returns DataSet Object representing the selected records</returns>
        /// <example>
        /// <code>
        /// //Create Database Connection
        /// clsENationWebComponents ClsDb = new clsENationWebComponents();
        /// 
        /// //Execute Stored Procedure
        /// DataSet ds = ClsDb.Get_DS_BySP("Procedure_Name_Having_No_Parameter");
        /// </code>
        /// </example>
        public DataSet Get_DS_BySP(string spName)
        {
            //try
            //{
            //DBCommandWrapper dbCommandWrapper  = db.GetStoredProcCommandWrapper(spName);
            DbCommand dbCommand = db.GetStoredProcCommand(spName);
            dbCommand.CommandTimeout = 600;
            return (DataSet)db.ExecuteDataSet(dbCommand);
            //}
            //catch (Exception ex)
            //{
            //    string exmsg = ex.Message;
            //    DataSet ds = new DataSet();
            //    return ds;
            //}
        }

        /// <summary>
        ///  This Method execute the Stored Procedure having no parameter and return the DataSet Object
        /// </summary>
        /// <param name="spName">
        /// Name of the procedure to be executed.
        /// </param>
        /// <returns> Returns DataSet Object representing the selected records</returns>
        /// <example>
        /// <code>
        /// //Create Database Connection
        /// clsENationWebComponents ClsDb = new clsENationWebComponents();
        /// 
        /// //Execute Stored Procedure
        /// DataSet dr = ClsDb.Get_DS_BySP("Procedure_Name_Having_No_Parameter");
        /// </code>
        /// </example>
        public DataSet Get_DS_BySPArr(string spName)
        {
            //try
            //{
            //DBCommandWrapper dbCommandWrapper  = db.GetStoredProcCommandWrapper(spName);
            DbCommand dbCommand = db.GetStoredProcCommand(spName);
            dbCommand.CommandTimeout = 600;
            return (DataSet)db.ExecuteDataSet(dbCommand);
            //}
            //catch (Exception ex)
            //{
            //    string exmsg = ex.Message;
            //    DataSet ds = new DataSet();
            //    return ds;
            //}
        }

        /// <summary>
        /// This Method execute the Stored Procedure having one or more parameter and return the DataSet Object
        /// </summary>
        /// <param name="spName">
        /// Name of the procedure to be executed
        /// </param>
        /// <param name="key">
        /// String Array Containing the parameter list
        /// </param>
        /// <param name="value1">
        /// Object Array Containing the parameter values
        /// </param>
        /// <returns>
        /// DataSet Object representing the selected records
        /// </returns>
        /// <example>
        /// <code>
        /// //Create Database Connection
        /// clsENationWebComponents ClsDb = new clsENationWebComponents();
        /// 
        /// //Paremeter List
        /// string[] keys = {"Parameter1" ,"Parameter2"};
        /// 
        /// //Parameter Values 
        /// object[] values = {"Parameter1 Value","Parameter2 Value"};
        /// 
        /// //Execute Stored Procedure
        /// DataSet ds = ClsDb.Get_DS_BySPArr("Procedure_Name_Having_Multiple_Parameters",keys,values);
        /// </code>
        /// </example>
        public DataSet Get_DS_BySPArr(string spName, string[] key, object[] value1)
        {
            //Saeed 7931 7/5/2010 remove unreachable code.
            DataSet ds = new DataSet();
            try
            {
                //	DBCommandWrapper dbCommandWrapper  = db.GetStoredProcCommandWrapper(spName);
                DbCommand dbCommand = db.GetStoredProcCommand(spName);
                dbCommand.CommandTimeout = 2000;    // Fahad 4742,4743 02/06/2009 timeout increases from 1000 to 2000
                for (int i = 0; i <= key.Length - 1; i++)
                {
                    db.AddInParameter(dbCommand, (string)key.GetValue(i), GetDBType(value1.GetValue(i).GetType().Name), value1.GetValue(i));
                }

                //CommandTimeout = 600;
                ds = db.ExecuteDataSet(dbCommand);
            }
            catch (SqlException ex)
            {
                // string exmsg = ex.Message;
                //  throw ex.Message;
                throw new Exception(ex.Message);
            }
            catch (Exception ex)
            {
                string exmsg = ex.Message;
                // throw ex.Message; 
            }
            return ds;
            //Saeed 7931 7/5/2010 End
        }

        /// <summary>
        /// This method gets DataSet Object returned by the specified procedure having a single parameter.
        /// </summary>
        /// <param name="spName">
        /// Name of the Stored Procedure
        /// </param>
        /// <param name="key">
        /// Name of the Procedure Parameter
        /// </param>
        /// <param name="value1">
        /// Paremeter Value
        /// </param>
        /// <returns>
        ///DataSet Object representing the selected records
        /// </returns>
        /// <example>
        /// <code>
        /// //Create Database Connection
        /// clsENationWebComponents ClsDb = new clsENationWebComponents();
        /// 
        /// //Get Records
        /// DataSet ds = ClsDb.Get_DS_BySPByOneParmameter("ProcedureName","ParameterName","ParameterValue");
        /// 
        /// 
        /// </code>
        /// </example>
        public DataSet Get_DS_BySPByOneParmameter(string spName, string key, object value1)
        {
            //try
            //{
            DbCommand dbCommand = db.GetStoredProcCommand(spName);
            db.AddInParameter(dbCommand, (string)key, GetDBType(value1.GetType().Name), value1);
            return (DataSet)db.ExecuteDataSet(dbCommand);
            //}
            //catch (Exception ex)
            //{
            //    string exmsg = ex.Message;
            //    DataSet ds = new DataSet();
            //    return ds;
            //}
        }

        /// <summary>
        /// This method gets DataSet returned by the specified procedure having two parameters.
        /// </summary>
        /// <param name="spName">
        /// Name of the Stored Procedure
        /// </param>
        /// <param name="ParmName">
        /// Name of the First Procedure Parameter 
        /// </param>
        /// <param name="ParmValue">
        /// First Paremeter  Value
        /// </param>
        /// <param name="ParmName2">Name of the Second Procedure Parameter</param>
        /// <param name="ParmValue2">Second Parameter Value</param>
        /// <returns>
        ///DataSet Object representing the selected records
        /// </returns>
        /// <example>
        /// <code>
        /// //Create Database Connection
        /// clsENationWebComponents ClsDb = new clsENationWebComponents();
        /// 
        /// //Get Records
        /// DataSet dr = ClsDb.Get_DS_BySPByTwoParmameter("ProcedureName","FirstParameterName","FirstParameterValue","SecondParameterName","SecondParameterValue");
        /// 
        /// </code>
        /// </example>
        public DataSet Get_DS_BySPByTwoParmameter(string spName, string ParmName, object ParmValue, string ParmName2, object ParmValue2)
        {

            //DBCommandWrapper dbCommandWrapper = db.GetStoredProcCommandWrapper(spName);
            DbCommand dbCommand = db.GetStoredProcCommand(spName);
            db.AddInParameter(dbCommand, ParmName, GetDBType(ParmValue.GetType().Name), ParmValue);
            db.AddInParameter(dbCommand, ParmName2, GetDBType(ParmValue2.GetType().Name), ParmValue2);
            //dbCommandWrapper.CommandTimeout = 600;
            return (DataSet)db.ExecuteDataSet(dbCommand);

        }

        /// <summary>
        /// This Method execute the Stored Procedure having one or more parameter and return the DataTable Object
        /// </summary>
        /// <param name="spName">
        /// Name of the procedure to be executed
        /// </param>
        /// <param name="key">
        /// String Array Containing the parameter list
        /// </param>
        /// <param name="value1">
        /// Object Array Containing the parameter values
        /// </param>
        /// <returns>
        /// DataTable Object representing the selected records
        /// </returns>
        /// <example>
        /// <code>
        /// //Create Database Connection
        /// clsENationWebComponents ClsDb = new clsENationWebComponents();
        /// 
        /// //Paremeter List
        /// string[] keys = {"Parameter1" ,"Parameter2"};
        /// 
        /// //Parameter Values 
        /// object[] values = {"Parameter1 Value","Parameter2 Value"};
        /// 
        /// //Execute Stored Procedure
        /// DataTable dt = ClsDb.Get_DT_BySPArr("Procedure_Name_Having_Multiple_Parameters",keys,values);
        /// </code>
        /// </example>
        public DataTable Get_DT_BySPArr(string spName, string[] key, object[] value1)
        {
            DbCommand dbCommand = db.GetStoredProcCommand(spName);
            dbCommand.CommandTimeout = 600;
            for (int i = 0; i <= key.Length - 1; i++)
            {
                db.AddInParameter(dbCommand, (string)key.GetValue(i), GetDBType(value1.GetValue(i).GetType().Name), value1.GetValue(i));
            }
            DataSet Ds = (DataSet)db.ExecuteDataSet(dbCommand);
            return (DataTable)Ds.Tables[0];
        }

        /// <summary>
        ///  This Method execute the Stored Procedure having no parameter and return the DataTable Object
        /// </summary>
        /// <param name="spName">
        /// Name of the procedure to be executed.
        /// </param>
        /// <returns> Returns DataTable Object representing the selected records</returns>
        /// <example>
        /// <code>
        /// //Create Database Connection
        /// clsENationWebComponents ClsDb = new clsENationWebComponents();
        /// 
        /// //Execute Stored Procedure
        /// DataTable dt = ClsDb.Get_DT_BySPArr("Procedure_Name_Having_No_Parameter");
        /// </code>
        /// </example>
        public DataTable Get_DT_BySPArr(string spName)
        {
            DbCommand dbCommand = db.GetStoredProcCommand(spName);
            dbCommand.CommandTimeout = 20000;
            DataSet Ds = (DataSet)db.ExecuteDataSet(dbCommand);
            return (DataTable)Ds.Tables[0];
        }

        #endregion

        #region INSERT OPERATIONS


        /// <summary>
        /// This Method execute the Insert Stored Procedure having five parameter.
        /// </summary>
        /// <param name="spName">Name of the procedure to be executed</param>
        /// <param name="ParmName">Name of the First Procedure Parameter </param>
        /// <param name="ParmValue">First Paremeter  Value</param>
        /// <param name="ParmName2">Name of the Second Procedure Parameter </param>
        /// <param name="ParmValue2">Second Paremeter  Value</param>
        /// <param name="ParmName3">Name of the Third Procedure Parameter </param>
        /// <param name="ParmValue3">Third Paremeter  Value</param>
        /// <param name="ParmName4">Name of the Fourth Procedure Parameter </param>
        /// <param name="ParmValue4">Fourth Paremeter  Value</param>
        /// <param name="ParmName5">Name of the Fifth Procedure Parameter </param>
        /// <param name="ParmValue5">Fifth Paremeter  Value</param>
        /// <returns>Returns true if values inserted successfully</returns>
        /// <example>
        /// <code>
        /// //Create Database Connection
        /// clsENationWebComponents ClsDb = new clsENationWebComponents();
        /// 
        /// //Get Records
        /// ClsDb.InsertBySP("ProcedureName","FirstParameterName","FirstParameterValue","SecondParameterName","SecondParameterValue","ThirdParameterName","ThirdParameterValue","FourthParameterName","FourthParameterValue","FifthParameterName","FifthParameterValue");
        /// </code>
        /// </example>
        public bool InsertBySP(string spName, string ParmName, object ParmValue, string ParmName2, object ParmValue2, string ParmName3, object ParmValue3, string ParmName4, object ParmValue4, string ParmName5, object ParmValue5)
        {
            //DBCommandWrapper dbCommandWrapper = db.GetStoredProcCommandWrapper(spName);
            DbCommand dbCommand = db.GetStoredProcCommand(spName);
            db.AddInParameter(dbCommand, ParmName, GetDBType(ParmValue.GetType().Name), ParmValue);
            db.AddInParameter(dbCommand, ParmName2, GetDBType(ParmValue2.GetType().Name), ParmValue2);
            db.AddInParameter(dbCommand, ParmName3, GetDBType(ParmValue3.GetType().Name), ParmValue3);
            db.AddInParameter(dbCommand, ParmName4, GetDBType(ParmValue4.GetType().Name), ParmValue4);
            db.AddInParameter(dbCommand, ParmName5, GetDBType(ParmValue5.GetType().Name), ParmValue5);

            db.ExecuteNonQuery(dbCommand);
            return true;
        }

        /// <summary>
        /// This Method execute the Insert Stored Procedure having one or more parameter.
        /// </summary>
        /// <param name="spName">
        /// Name of the procedure to be executed
        /// </param>
        /// <param name="key">
        /// String Array Containing the parameter list
        /// </param>
        /// <param name="value1">
        /// Object Array Containing the parameter values
        /// </param>
        /// <example>
        /// <code>
        /// //Create Database Connection
        /// clsENationWebComponents ClsDb = new clsENationWebComponents();
        /// 
        /// //Paremeter List
        /// string[] keys = {"Parameter1" ,"Parameter2"};
        /// 
        /// //Parameter Values 
        /// object[] values = {"Parameter1 Value","Parameter2 Value"};
        /// 
        /// //Execute Stored Procedure
        /// ClsDb.InsertBySPArr("Procedure_Name_Having_Multiple_Parameters",keys,values);
        /// </code>
        /// </example>
        public void InsertBySPArr(string spName, string[] key, object[] value1)
        {
            DbCommand dbCommand = db.GetStoredProcCommand(spName);
            // Function Modified By Fahad set Command timeout = 2000 BUG# 1370 ( Fahad 25-jan-2008 ) 
            dbCommand.CommandTimeout = 2000;
            for (int i = 0; i <= key.Length - 1; i++)
            {
                db.AddInParameter(dbCommand, (string)key.GetValue(i), GetDBType(value1.GetValue(i).GetType().Name), value1.GetValue(i));
            }
            db.ExecuteNonQuery(dbCommand);
            return;
            //End Fahad
        }

        /// <summary>
        /// This Method execute the Stored Procedure having one or more parameter in the specified transaction. 
        /// </summary>
        /// <param name="spName">
        /// Name of the procedure to be executed.
        /// </param>
        /// <param name="key">
        /// String Array Containing the parameter list.
        /// </param>
        /// <param name="value1">
        /// Object Array Containing the parameter values.
        /// </param>
        /// <param name="trans">
        /// The transaction object in which the procedure executes.
        /// </param>
        /// <example>
        /// <code>
        /// //Create Database Connection
        /// clsENationWebComponents ClsDb = new clsENationWebComponents();
        /// 
        /// IDBTransaction Trans = ClsDb.BeginTransaction();
        /// 
        /// //Paremeter List
        /// string[] keys = {"Parameter1" ,"Parameter2"};
        /// 
        /// //Parameter Values 
        /// object[] values = {"Parameter1 Value","Parameter2 Value"};
        /// 
        /// //Execute Stored Procedure in the specifed transaction
        /// ClsDb.InsertBySPArr("Procedure_Name_Having_Multiple_Parameters",keys,values,Trans);
        /// </code>
        /// </example>
        public void InsertBySPArr(string spName, string[] key, object[] value1, DbTransaction trans)
        {
            DbCommand dbCommand = db.GetStoredProcCommand(spName);
            dbCommand.CommandTimeout = 600;

            for (int i = 0; i <= key.Length - 1; i++)
            {
                db.AddInParameter(dbCommand, (string)key.GetValue(i), GetDBType(value1.GetValue(i).GetType().Name), value1.GetValue(i));
            }
            db.ExecuteNonQuery(dbCommand, trans);
            return;
        }

        /// <summary>
        /// This Method execute the Stored Procedure having one or more parameter in the specified transaction. 
        /// </summary>
        /// <param name="spName">
        /// Name of the procedure to be executed.
        /// </param>
        /// <param name="key">
        /// String Array Containing the parameter list.
        /// </param>
        /// <param name="value1">
        /// Object Array Containing the parameter values.
        /// </param>
        /// <param name="trans">
        /// The transaction object in which the procedure executes.
        /// </param>
        /// <example>
        /// <code>
        /// //Create Database Connection
        /// clsENationWebComponents ClsDb = new clsENationWebComponents();
        /// 
        /// IDBTransaction Trans = ClsDb.BeginTransaction();
        /// 
        /// //Paremeter List
        /// string[] keys = {"Parameter1" ,"Parameter2"};
        /// 
        /// //Parameter Values 
        /// object[] values = {"Parameter1 Value","Parameter2 Value"};
        /// 
        /// //Execute Stored Procedure in the specifed transaction
        /// ClsDb.InsertBySPArrRet("Procedure_Name_Having_Multiple_Parameters",keys,values,Trans);
        /// </code>
        /// </example>
        public object InsertBySPArrRet(string spName, string[] key, object[] value1, DbTransaction trans)
        {
            DbCommand dbCommand = db.GetStoredProcCommand(spName);
            //dbCommandWrapper.CommandTimeout = 600;		
            dbCommand.CommandTimeout = 1000;
            int i = 0;
            for (i = 0; i <= key.Length - 2; i++)
            {
                db.AddInParameter(dbCommand, (string)key.GetValue(i), GetDBType(value1.GetValue(i).GetType().Name), value1.GetValue(i));
            }
            db.AddOutParameter(dbCommand, (string)key.GetValue(i), GetDBType(value1.GetValue(i).GetType().Name), 18);
            db.ExecuteNonQuery(dbCommand, trans);
            return db.GetParameterValue(dbCommand, (string)key.GetValue(i));
        }

        /// <summary>
        /// This Method execute the Insert Stored Procedure having one or more parameter.
        /// </summary>
        /// <param name="spName">
        /// Name of the procedure to be executed
        /// </param>
        /// <param name="key">
        /// String Array Containing the parameter list
        /// </param>
        /// <param name="value1">
        /// Object Array Containing the parameter values
        /// </param>
        /// <example>
        /// <code>
        /// //Create Database Connection
        /// clsENationWebComponents ClsDb = new clsENationWebComponents();
        /// 
        /// //Paremeter List
        /// string[] keys = {"Parameter1" ,"Parameter2"};
        /// 
        /// //Parameter Values 
        /// object[] values = {"Parameter1 Value","Parameter2 Value"};
        /// 
        /// //Execute Stored Procedure
        /// ClsDb.InsertBySPArrRet("Procedure_Name_Having_Multiple_Parameters",keys,values);
        /// </code>
        /// </example>
        public object InsertBySPArrRet(string spName, string[] key, object[] value1)
        {
            DbCommand dbCommand = db.GetStoredProcCommand(spName);
            dbCommand.CommandTimeout = 1000;
            // dbCommandWrapper.CommandTimeout = 600;
            int i = 0;
            for (i = 0; i <= key.Length - 2; i++)
            {
                db.AddInParameter(dbCommand, (string)key.GetValue(i), GetDBType(value1.GetValue(i).GetType().Name), value1.GetValue(i));
            }
            db.AddOutParameter(dbCommand, (string)key.GetValue(i), GetDBType(value1.GetValue(i).GetType().Name), 18);
            db.ExecuteNonQuery(dbCommand);
            return db.GetParameterValue(dbCommand, (string)key.GetValue(i));
        }

        //Yasir Kamal 7150 01/01/2010 method to convert byte array into binary.
        /// <summary>
        /// This Method execute the Insert Stored Procedure having one or more parameter.
        /// </summary>
        /// <param name="spName">
        /// Name of the procedure to be executed
        /// </param>
        /// <param name="key">
        /// String Array Containing the parameter list
        /// </param>
        /// <param name="value1">
        /// Object Array Containing the parameter values
        /// </param>
        /// <param name="convertByte">
        /// boolean parameter to convert Byte array into binary. 
        /// </param>
        /// <example>
        /// <code>
        /// //Create Database Connection
        /// clsENationWebComponents ClsDb = new clsENationWebComponents();
        /// 
        /// //Paremeter List
        /// string[] keys = {"Parameter1" ,"Parameter2","Parameter3"};
        /// 
        /// //Parameter Values 
        /// object[] values = {"Parameter1 Value","Parameter2 Value","Parameter3 Value"};
        /// 
        /// //Execute Stored Procedure
        /// ClsDb.InsertBySPArrRet("Procedure_Name_Having_Multiple_Parameters",keys,values,convertByte);
        /// </code>
        /// </example>
        public object InsertBySPArrRet(string spName, string[] key, object[] value1, bool convertByte)
        {
            DbCommand dbCommand = db.GetStoredProcCommand(spName);
            dbCommand.CommandTimeout = 1000;
            // dbCommandWrapper.CommandTimeout = 600;
            int i = 0;
            for (i = 0; i <= key.Length - 2; i++)
            {
                if (value1.GetValue(i) == null)
                {
                    db.AddInParameter(dbCommand, (string)key.GetValue(i), DbType.Binary, value1.GetValue(i));
                }
                else if (value1.GetValue(i).GetType().Name == "Byte[]" && convertByte)
                {
                    db.AddInParameter(dbCommand, (string)key.GetValue(i), DbType.Binary, value1.GetValue(i));
                }
                else
                    db.AddInParameter(dbCommand, (string)key.GetValue(i), GetDBType(value1.GetValue(i).GetType().Name), value1.GetValue(i));
            }
            db.AddOutParameter(dbCommand, (string)key.GetValue(i), GetDBType(value1.GetValue(i).GetType().Name), 18);
            db.ExecuteNonQuery(dbCommand);
            return db.GetParameterValue(dbCommand, (string)key.GetValue(i));
        }

        /// <summary>
        /// This Method execute the Insert Stored Procedure having one or more parameter and return array object of output parameters.
        /// </summary>
        /// <param name="spName">
        /// Name of the procedure to be executed
        /// </param>
        /// <param name="key">
        /// String Array Containing the parameter list
        /// </param>
        /// <param name="value1">
        /// Object Array Containing the parameter values
        /// </param>
        /// <param name="OutPutparameterCount">
        /// No of output parameter should returned. 
        /// </param>
        /// <returns>
        /// Object Array containing output parameters
        /// </returns>
        /// <example>
        /// <code>
        /// //Create Database Connection
        /// clsENationWebComponents ClsDb = new clsENationWebComponents();
        /// 
        /// //Paremeter List
        /// string[] keys = {"Parameter1" ,"Parameter2"};
        /// 
        /// //Parameter Values 
        /// object[] values = {"Parameter1 Value","Parameter2 Value"};
        /// 
        /// //Execute Stored Procedure
        /// object[] resultValues = ClsDb.InsertBySPArrRet("Procedure_Name_Having_Multiple_Parameters",keys,values,5);
        /// </code>
        /// </example>
        public object[] InsertBySPArrRet(string spName, string[] key, object[] value1, int OutPutparameterCount)
        {
            DbCommand dbCommand = db.GetStoredProcCommand(spName);
            dbCommand.CommandTimeout = 1000;
            // dbCommandWrapper.CommandTimeout = 600;
            int i = 0;
            OutPutparameterCount++;

            for (i = 0; i <= key.Length - OutPutparameterCount; i++)
            {
                db.AddInParameter(dbCommand, (string)key.GetValue(i), GetDBType(value1.GetValue(i).GetType().Name), value1.GetValue(i));
            }

            for (int j = i; j <= key.Length - 1; j++)
            {
                db.AddOutParameter(dbCommand, (string)key.GetValue(j), GetDBType(value1.GetValue(j).GetType().Name), 18);
            }

            db.ExecuteNonQuery(dbCommand);

            object[] parm;
            parm = new object[OutPutparameterCount - 1];
            int IntCounter = 0;
            for (int j = i; j <= key.Length - 1; j++)
            {

                parm.SetValue(db.GetParameterValue(dbCommand, (string)key.GetValue(j)), IntCounter);
                IntCounter++;
            }
            return parm;
        }

        #endregion

        #region SCALER FUNCTION

        /// <summary>
        ///  This Method execute the Stored Procedure having no parameter and return a Scalar value as a Object.
        /// </summary>
        /// <param name="spName">
        /// Name of the procedure to be executed.
        /// </param>
        /// <returns> Returns A Single object value returned by the procedure.</returns>
        /// <example>
        /// <code>
        /// //Create Database Connection
        /// clsENationWebComponents ClsDb = new clsENationWebComponents();
        /// 
        /// //Execute Stored Procedure
        ///  double sum = Convert.ToDouble(ClsDb.ExecuteScalerBySp("Procedure_Name_Having_No_Parameter"));
        /// </code>
        /// </example>
        public object ExecuteScalerBySp(string spName)
        {
            DbCommand dbCommand = db.GetStoredProcCommand(spName);
            dbCommand.CommandTimeout = 600;
            object obj = db.ExecuteScalar(dbCommand);
            return obj;
        }

        /// <summary>
        /// This Method execute the Stored Procedure having one or more parameter and return a Scalar value as a Object.
        /// </summary>
        /// <param name="spName">
        /// Name of the procedure to be executed
        /// </param>
        /// <param name="key">
        /// String Array Containing the parameter list
        /// </param>
        /// <param name="value1">
        /// Object Array Containing the parameter values
        /// </param>
        /// <returns>
        ///  Returns A Single object value returned by the procedure.
        /// </returns>
        /// <example>
        /// <code>
        /// //Create Database Connection
        /// clsENationWebComponents ClsDb = new clsENationWebComponents();
        /// 
        /// //Paremeter List
        /// string[] keys = {"Parameter1" ,"Parameter2"};
        /// 
        /// //Parameter Values 
        /// object[] values = {"Parameter1 Value","Parameter2 Value"};
        /// 
        /// //Execute Stored Procedure
        /// double sum = Convert.ToDouble(ClsDb.ExecuteScalerBySp("Procedure_Name_Having_Multiple_Parameters",keys,values));
        /// </code>
        /// </example>
        public object ExecuteScalerBySp(string spName, string[] key, object[] value1)
        {
            DbCommand dbCommand = db.GetStoredProcCommand(spName);
            dbCommand.CommandTimeout = 600;
            for (int i = 0; i <= key.Length - 1; i++)
            {
                db.AddInParameter(dbCommand, (string)key.GetValue(i), GetDBType(value1.GetValue(i).GetType().Name), value1.GetValue(i));
            }
            object obj = (object)db.ExecuteScalar(dbCommand);
            return obj;
        }

        /// <summary>
        ///  This method interacts with DataBase to execute the Stored Procedure accordingly.
        /// </summary>
        /// <param name="spName">Name of the Stored Procedure</param>
        /// <param name="key">Array of the Parameters Name used by Stored Procedure</param>
        /// <param name="value1">Array of the Parameters Value used by Stored Procedure</param>
        /// <param name="timeout">DataBase Commmad Time Out Set by Parameter</param>
        /// <returns>Object on the basis of Result Set</returns>
        public object ExecuteScalerBySp(string spName, string[] key, object[] value1, int timeout)
        {
            //Fahad 7791 06/30/2010 Method Added
            DbCommand dbCommand = db.GetStoredProcCommand(spName);
            dbCommand.CommandTimeout = timeout;
            for (int i = 0; i <= key.Length - 1; i++)
            {
                db.AddInParameter(dbCommand, (string)key.GetValue(i), GetDBType(value1.GetValue(i).GetType().Name), value1.GetValue(i));
            }
            object obj = db.ExecuteScalar(dbCommand);
            return obj;
        }


        /// <summary>
        /// This Method execute the Stored Procedure having one or more parameter in the specified transaction and return a Scalar value as a Object.
        /// </summary>
        /// <param name="spName">
        /// Name of the procedure to be executed.
        /// </param>
        /// <param name="key">
        /// String Array Containing the parameter list.
        /// </param>
        /// <param name="value1">
        /// Object Array Containing the parameter values.
        /// </param>
        /// <param name="Trans">
        /// The transaction object in which the procedure executes.
        /// </param>
        /// <returns>
        ///  Returns A Single object value returned by the procedure.
        /// </returns>
        /// <example>
        /// <code>
        /// //Create Database Connection
        /// clsENationWebComponents ClsDb = new clsENationWebComponents();
        /// 
        /// IDBTransaction Trans = ClsDb.BeginTransaction();
        /// 
        /// //Paremeter List
        /// string[] keys = {"Parameter1" ,"Parameter2"};
        /// 
        /// //Parameter Values 
        /// object[] values = {"Parameter1 Value","Parameter2 Value"};
        /// 
        /// //Execute Stored Procedure in the specifed transaction
        /// double sum = Convert.ToDouble(ClsDb.ExecuteScalerBySp("Procedure_Name_Having_Multiple_Parameters",keys,values,Trans));
        /// </code>
        /// </example>
        public object ExecuteScalerBySp(string spName, string[] key, object[] value1, DbTransaction Trans)
        {
            object obj;
            DbCommand dbCommand = db.GetStoredProcCommand(spName);
            dbCommand.CommandTimeout = 600;
            for (int i = 0; i <= key.Length - 1; i++)
            {
                db.AddInParameter(dbCommand, (string)key.GetValue(i), GetDBType(value1.GetValue(i).GetType().Name), value1.GetValue(i));
            }
            obj = (object)db.ExecuteScalar(dbCommand, Trans);
            return obj;
        }

        #endregion


        /// <summary>
        ///  This Method execute the Stored Procedure having no parameter and returns value as a string.
        /// </summary>
        /// <param name="spName">
        /// Name of the procedure to be executed.
        /// </param>
        /// <returns> Returns A string value indicating that procedure is executed successfully.</returns>
        /// <example>
        /// <code>
        /// //Create Database Connection
        /// clsENationWebComponents ClsDb = new clsENationWebComponents();
        /// 
        /// //Execute Stored Procedure
        ///  ClsDb.ExecuteSP("Procedure_Name_Having_No_Parameter");
        /// </code>
        /// </example>
        public string ExecuteSP(string spName)
        {
            try
            {
                DbCommand dbCommand = db.GetStoredProcCommand(spName);
                //dbCommandWrapper.CommandTimeout = 600;				
                //if (IsTransaction)
                //db.ExecuteNonQuery(dbCommandWrapper, transaction);						
                //else
                db.ExecuteNonQuery(dbCommand);

                return "true";
            }
            catch (Exception ex)
            {

                //if (IsTransaction)
                //transaction.Rollback();
                return ex.Message.ToString();
            }
            finally
            {
                //				if (IsTransaction)
                //					connection.Close();				
            }
        }

        /// <summary>
        /// This Method execute the Stored Procedure having one or more parameter.
        /// </summary>
        /// <param name="spName">
        /// Name of the procedure to be executed
        /// </param>
        /// <param name="key">
        /// String Array Containing the parameter list
        /// </param>
        /// <param name="value1">
        /// Object Array Containing the parameter values
        /// </param>
        /// <example>
        /// <code>
        /// //Create Database Connection
        /// clsENationWebComponents ClsDb = new clsENationWebComponents();
        /// 
        /// //Paremeter List
        /// string[] keys = {"Parameter1" ,"Parameter2"};
        /// 
        /// //Parameter Values 
        /// object[] values = {"Parameter1 Value","Parameter2 Value"};
        /// 
        /// //Execute Stored Procedure
        /// ClsDb.ExecuteSP("Procedure_Name_Having_Multiple_Parameters",keys,values);
        /// </code>
        /// </example>
        public void ExecuteSP(string spName, string[] key, object[] value1)
        {
            //try
            //{
            DbCommand dbCommand = db.GetStoredProcCommand(spName);
            //	dbCommandWrapper.CommandTimeout = 600;		
            dbCommand.CommandTimeout = 600;
            for (int i = 0; i <= key.Length - 1; i++)
            {
                db.AddInParameter(dbCommand, (string)key.GetValue(i), GetDBType(value1.GetValue(i).GetType().Name), value1.GetValue(i));
            }
            db.ExecuteNonQuery(dbCommand);
            //}
            //catch (Exception ex)
            //{
            //    ex.Message.ToString();
            //}
        }

        /// <summary>
        /// This method interacts with DataBase to execute the Stored Procedure accordingly.
        /// </summary>
        /// <param name="timeout">DataBase Commmad Time Out Set by Parameter</param>
        /// <param name="spName">Name of the Stored Procedure</param>
        /// <param name="key">Array of the Parameters Name used by Stored Procedure</param>
        /// <param name="value1">Array of the Parameters Value used by Stored Procedure</param>
        public void ExecuteSP(int timeout, string spName, string[] key, object[] value1)
        {
            //Fahad 7791 06/30/2010 Method Modified(Increased TimeOut time to runing query)
            DbCommand dbCommand = db.GetStoredProcCommand(spName);
            dbCommand.CommandTimeout = timeout;
            for (int i = 0; i <= key.Length - 1; i++)
            {
                db.AddInParameter(dbCommand, (string)key.GetValue(i), GetDBType(value1.GetValue(i).GetType().Name), value1.GetValue(i));
            }
            db.ExecuteNonQuery(dbCommand);

        }

        //Waqas 6342 08/13/2009 Sending Keys, Values with check of allowing null values
        /// <summary>
        /// This Procedure is to add parameters, execute procedure. If it finds null values then pass through string Type.
        /// </summary>
        /// <param name="spName"></param>
        /// <param name="key"></param>
        /// <param name="value1"></param>
        /// <param name="AllowNullValues"></param>
        public void ExecuteSP(string spName, string[] key, object[] value1, bool AllowNullValues)
        {
            if (AllowNullValues)
            {
                DbCommand dbCommand = db.GetStoredProcCommand(spName);
                dbCommand.CommandTimeout = 600;
                for (int i = 0; i <= key.Length - 1; i++)
                {
                    if (value1.GetValue(i) != null)
                    {
                        db.AddInParameter(dbCommand, (string)key.GetValue(i), GetDBType(value1.GetValue(i).GetType().Name), value1.GetValue(i));
                    }
                    else
                    {
                        db.AddInParameter(dbCommand, (string)key.GetValue(i), DbType.String, value1.GetValue(i));
                    }

                }
                db.ExecuteNonQuery(dbCommand);
            }
            else
            {
                DbCommand dbCommand = db.GetStoredProcCommand(spName);
                dbCommand.CommandTimeout = 600;
                for (int i = 0; i <= key.Length - 1; i++)
                {
                    db.AddInParameter(dbCommand, (string)key.GetValue(i), GetDBType(value1.GetValue(i).GetType().Name), value1.GetValue(i));
                }
                db.ExecuteNonQuery(dbCommand);
            }

        }


        /// <summary>
        /// This Method execute the Stored Procedure having one or more parameter in a specified transaction.
        /// </summary>
        /// <param name="spName">
        /// Name of the procedure to be executed
        /// </param>
        /// <param name="key">
        /// String Array Containing the parameter list
        /// </param>
        /// <param name="value1">
        /// Object Array Containing the parameter values
        /// </param>
        /// <param name="trans">
        /// The transaction object in which the procedure executes.
        /// </param>
        /// <example>
        /// <code>
        /// //Create Database Connection
        /// clsENationWebComponents ClsDb = new clsENationWebComponents();
        /// 
        /// IDBTransaction Trans = ClsDb.BeginTransaction(); 
        /// 
        /// //Paremeter List
        /// string[] keys = {"Parameter1" ,"Parameter2"};
        /// 
        /// //Parameter Values 
        /// object[] values = {"Parameter1 Value","Parameter2 Value"};
        /// 
        /// //Execute Stored Procedure
        /// ClsDb.ExecuteSP("Procedure_Name_Having_Multiple_Parameters",keys,values,Trans);
        /// </code>
        /// </example>
        public void ExecuteSP(string spName, string[] key, object[] value1, DbTransaction trans)
        {
            try
            {
                DbCommand dbCommand = db.GetStoredProcCommand(spName);
                //		dbCommandWrapper.CommandTimeout = 600;		

                for (int i = 0; i <= key.Length - 1; i++)
                {
                    db.AddInParameter(dbCommand, (string)key.GetValue(i), GetDBType(value1.GetValue(i).GetType().Name), value1.GetValue(i));
                }

                //				if (IsTransaction)
                //				{
                db.ExecuteNonQuery(dbCommand, trans);
                //				}
                //				else
                //				{
                //					db.ExecuteNonQuery(dbCommandWrapper);	
                //				}				
            }

            catch (Exception)   //Saeed 7931 7/5/2010 remove ex, declare but never use.
            {
                transaction.Rollback();
            }

        }

        /// <summary>
        ///  This Method execute the SQL query.
        /// </summary>
        /// <param name="strQuery">
        /// SQL query to be executed.
        /// </param>
        /// <example>
        /// <code>
        /// //Create Database Connection
        /// clsENationWebComponents ClsDb = new clsENationWebComponents();
        /// 
        /// //Execute Stored Procedure
        /// ClsDb.ExecuteQuery("Insert into temptable values (1,1,1,1)");
        /// </code>
        /// </example>
        public void ExecuteQuery(string strQuery)
        {
            //try
            //{
            DbCommand dbCommand = db.GetSqlStringCommand(strQuery);
            dbCommand.Connection = db.CreateConnection();
            dbCommand.Connection.Open();
            dbCommand.ExecuteNonQuery();
            //}
            //catch (Exception Ex)
            //{
            //    ///transaction.Rollback();  
            //}
        }

        /// <summary>
        /// This Method execute the Stored Procedure having one or more parameter.
        /// </summary>
        /// <param name="spName">
        /// Name of the procedure to be executed
        /// </param>
        /// <param name="key">
        /// String Array Containing the parameter list
        /// </param>
        /// <param name="value1">
        /// Object Array Containing the parameter values
        /// </param>
        /// <param name="a">
        /// Useless Parameter
        /// </param>
        /// <example>
        /// <code>
        /// //Create Database Connection
        /// clsENationWebComponents ClsDb = new clsENationWebComponents();
        /// 
        /// //Paremeter List
        /// string[] keys = {"Parameter1" ,"Parameter2"};
        /// 
        /// //Parameter Values 
        /// object[] values = {"Parameter1 Value","Parameter2 Value"};
        /// 
        /// //Execute Stored Procedure
        /// ClsDb.ExecuteSP("Procedure_Name_Having_Multiple_Parameters",keys,values);
        /// </code>
        /// </example>
        public void ExecuteSP(string spName, string[] key, object[] value1, int a)
        {
            //	try
            //	{

            DbCommand dbCommandWrapper = db.GetStoredProcCommand(spName);
            dbCommandWrapper.CommandTimeout = 600;

            for (int i = 0; i <= key.Length - 1; i++)
            {
                db.AddInParameter(dbCommandWrapper, (string)key.GetValue(i), GetDBType(value1.GetValue(i).GetType().Name), value1.GetValue(i));
            }

            db.ExecuteNonQuery(dbCommandWrapper, transaction);
            //	}
            //	catch(Exception ex)
            //	{
            //string err= ex.Message;
            //	}
        }


        //Yasir Kamal 7590 03/29/2010 trial letter modifications
        /// <summary>
        /// This Method execute the Insert Stored Procedure having one or more parameter.
        /// </summary>
        /// <param name="spName">
        /// Name of the procedure to be executed
        /// </param>
        /// <param name="key">
        /// String Array Containing the parameter list
        /// </param>
        /// <param name="value1">
        /// Object Array Containing the parameter values
        /// </param>
        /// <param name="outputSize">
        /// Output Parameter size 
        /// </param>
        /// <returns>Document Path</returns>
        /// <example>
        /// <code>
        /// //Create Database Connection
        /// clsENationWebComponents ClsDb = new clsENationWebComponents();
        /// 
        /// //Paremeter List
        /// string[] keys = {"Parameter1" ,"Parameter2"};
        /// 
        /// //Parameter Values 
        /// object[] values = {"Parameter1 Value","Parameter2 Value"};
        /// 
        /// //Execute Stored Procedure
        /// ClsDb.InsertBySPArrRetOutputSize("Procedure_Name_Having_Multiple_Parameters",keys,values,outputSize);
        /// </code>
        /// </example>
        public object InsertBySPArrRetOutputSize(string spName, string[] key, object[] value1, int outputSize)
        {
            DbCommand dbCommand = db.GetStoredProcCommand(spName);
            dbCommand.CommandTimeout = 1000;
            // dbCommandWrapper.CommandTimeout = 600;
            int i = 0;
            for (i = 0; i <= key.Length - 2; i++)
            {
                db.AddInParameter(dbCommand, (string)key.GetValue(i), GetDBType(value1.GetValue(i).GetType().Name), value1.GetValue(i));
            }
            db.AddOutParameter(dbCommand, (string)key.GetValue(i), GetDBType(value1.GetValue(i).GetType().Name), outputSize);
            db.ExecuteNonQuery(dbCommand);
            return db.GetParameterValue(dbCommand, (string)key.GetValue(i));
        }
    }
}

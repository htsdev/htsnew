using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using LNHelper;

namespace HTP.Components
{
    public class SullolawReports
    {
        static DBComponent ClsDb = new DBComponent("Sullolaw");
        public enum Sulloereporttype
        {
            Houston = 1,
            Dallas = 2
        };

        public static DataTable GetTransactionDetail(string FromDate, string ToDate, string ClientType)
        {
            string[] keys = { "@FromDate", "@Todate", "@ClientType" };
            object[] values = { FromDate, ToDate, ClientType };
            return ClsDb.GetDTBySPArr("usp_PS_Get_TicketTransaction_New", keys, values);
        }

        public static DataTable GetPubicSiteVisitorsDetail(string FromDate, string ToDate, string ClientType)
        {
            string[] keys = { "@StartDate", "@EndDate", "@ClientType" };
            object[] values = { FromDate, ToDate, ClientType };
            return ClsDb.GetDTBySPArr("USP_PS_GetDailyVisitors", keys, values);
        }

        public static DataTable GetCallQuoteDetail(string FromDate, string ToDate, string ClientType)
        {
            string[] keys = { "@StartDate", "@EndDate", "@ClientType" };
            object[] values = { FromDate, ToDate, ClientType };
            return ClsDb.GetDTBySPArr("USP_PS_QuoteCall_Report", keys, values);
        }

        // Noufil 4461 08/28/2008 

        //Yasir Kamal 7372 02/12/2010 allow user to set follow Up date.
        /// <summary>
        /// This function gets Legal consultation records from Public site
        /// </summary>
        /// <param name="siteid">Site Id</param>
        /// <param name="showall">Display All Records</param>
        /// <param name="date">Date</param>
        /// <param name="callstat">Callback Status</param>
        /// <param name="AllFollowUpDate">Display All Follow Up date</param>
        /// <returns>Legal consultation Report Records</returns>
        public static DataTable GetLegalConsultationReport(int siteid, int showall, DateTime date, int callstat, DateTime enddate, bool AllFollowUpDate, int practiceArea)
        {
            if (siteid > 0 && showall >= 0 && callstat >= 0)
            {
                //Noufil 4661 08/30/2008 Get Legal Consultation Report
                // Noufil 4919 10/08/2008 End date COlumn added
                String[] parameters = { "@siteid", "@showall", "@date", "@callback", "@enddate", "@AllFollowUpDate", "@practiceArea" };
                object[] values = { siteid, showall, date, callstat, enddate, AllFollowUpDate, practiceArea };
                return ClsDb.GetDTBySPArr("USP_PS_Get_LegalConsultation", parameters, values);
            }
            else
                throw new ArgumentException("Parameter not in correct format.");

        }


        /// <summary>
        ///  This function gets all those records who has filled the signup information from public site but didn't hire any one
        /// </summary>
        /// <param name="siteid">Site Id</param>
        /// <param name="showall">Display All Records</param>
        /// <param name="date">Date</param>
        /// <param name="callstat">Callback Status</param>
        /// <returns>Online Tracking Report Records</returns>
        public static DataTable GetOnlineTrackingReport(int siteid, int showall, DateTime startdate, DateTime enddate, int callstat)
        {
            if (siteid > 0 && showall >= 0 && callstat >= 0)
            {
                //Noufil 4661 08/30/2008 Get Online Tracking Report
                // Noufil 5524 03/16/2009 New parameter for @enddate added
                String[] parameters = { "@siteype", "@showall", "@date", "@enddate", "@callback" };
                object[] values = { siteid, showall, startdate, enddate, callstat };
                return ClsDb.GetDTBySPArr("USP_PS_Get_OnlineTracking", parameters, values);
            }
            else
                throw new ArgumentException("Parameter not in correct format.");

        }


        /// <summary>
        /// This function update comments for legal consultation and visitor report
        /// </summary>
        /// <param name="customerid">Customer Id</param>
        /// <param name="empid">Rep Id</param>
        /// <param name="comments">Contact Comments</param>
        /// <param name="commenttype">Comment Type</param>
        /// <param name="callback">Callback Status Type</param>
        public static void UpdateComments(int customerid, int empid, string comments, int commenttype, int callback)
        {
            //Noufil 4661 08/30/2008 Add Procedure To Update Call Back Comments
            String[] parameters = { "@customerid", "@empid", "@GeneralComments", "@commenttype", "@callback" };
            object[] values = { customerid, empid, comments, commenttype, callback };
            ClsDb.ExecuteSP("USP_PS_UpdateRepComments", parameters, values);
        }

        //Yasir Kamal 7372 02/12/2010 allow user to set follow Up date.
        /// <summary>
        /// This function update comments for legal consultation and visitor report
        /// </summary>
        /// <param name="customerid">Customer Id</param>
        /// <param name="empid">Rep Id</param>
        /// <param name="comments">Contact Comments</param>
        /// <param name="commenttype">Comment Type</param>
        /// <param name="callback">Callback Status Type</param>
        /// <param name="FollowUpDate">Follow Up date</param>
        public static void UpdateComments(int customerid, int empid, string comments, int commenttype, int callback, DateTime FollowUpDate)
        {
            String[] parameters = { "@customerid", "@empid", "@GeneralComments", "@commenttype", "@callback", "@FollowUpDate" };
            object[] values = { customerid, empid, comments, commenttype, callback, FollowUpDate };
            ClsDb.ExecuteSP("USP_PS_UpdateRepComments", parameters, values);
        }

        /// <summary>
        ///  This function gets all those records who has filled the contact us information from public site
        /// </summary>
        /// <param name="siteid">Site Id</param>
        /// <param name="showall">Display All Records</param>
        /// <param name="date">Date</param>
        /// <param name="callstat">Callback Status</param>
        /// <param name="enddate">End Date</param>
        /// <returns>Contact Us Report Records</returns>
        public static DataTable GetContactUsReport(int siteid, int showall, DateTime date, int callstat, DateTime enddate)
        {
            //Zeeshan 4709 09/26/2008 Get Contact Us Report
            String[] parameters = { "@siteid", "@showall", "@date", "@callback", "@enddate" };
            object[] values = { siteid, showall, date, callstat, enddate };
            return ClsDb.GetDTBySPArr("USP_PS_Get_Online_ContactUs_Report", parameters, values);

        }

        /// <summary>
        /// Get data for POLM report
        /// </summary>
        /// <param name="showall"></param>
        /// <param name="sdate"></param>
        /// <param name="edate"></param>
        /// <param name="RefAttor"></param>
        /// <param name="CaseType"></param>
        /// <returns></returns>
        public static DataTable GetPOLMData(Boolean showall, DateTime sdate, DateTime edate, int RefAttor, int CaseType)
        {
            String[] parameters = { "@showall", "@StartDate", "@EndDate", "@RefAttorney", "@CaseType", "@siteid" };
            object[] values = { showall, sdate, edate, "", "", 1 };
            return ClsDb.GetDTBySPArr("USP_PS_Get_POLM_LegalConsultation", parameters, values);
        }


    }
}

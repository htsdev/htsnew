using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;

namespace HTP.Components
{
    public class clsEDeliverySettings : ConfigurationSection
    {
        clsENationWebComponents clsDb = new clsENationWebComponents();
        
        public clsEDeliverySettings() 
        {

        }
        /// <summary>
        /// It is getting value of "userId" from web.config which is assign there and seting its value here
        /// </summary>
        [ConfigurationProperty("userId", DefaultValue = "Sullow & Sullow", IsRequired = true)]
        public string UserId
        {
            get { return (string)this["userId"]; }
            set { this["userId"] = value; }
        }

        /// <summary>
        /// It is getting value of "fromName" from web.config which is assign there and seting its value here
        /// </summary>
        [ConfigurationProperty("fromName", DefaultValue = "Sullow & Sullow", IsRequired = true)]
        public string FromName 
        {
            get { return (string)this["fromName"]; }
            set { this["fromName"] = value; }
        }

        /// <summary>
        /// It is getting value of "fromAddress1" from web.config which is assign there and seting its value here
        /// </summary>
        [ConfigurationProperty("fromAddress1", DefaultValue = "2020 Southwest Freeway(US 59)", IsRequired = true)]
        public string FromAddress1 
        {
            get { return (string)this["fromAddress1"]; }
            set { this["fromAddress1"] = value; }
        }

        /// <summary>
        /// It is getting value of "fromAddress2" from web.config which is assign there and seting its value here
        /// </summary>
        [ConfigurationProperty("fromAddress2", DefaultValue = ", Suite 300", IsRequired = true)]
        public string FromAddress2
        {
            get { return (string)this["fromAddress2"]; }
            set { this["fromAddress2"] = value; }
        }

        /// <summary>
        /// It is getting value of "fromCity" from web.config which is assign there and seting its value here
        /// </summary>
        [ConfigurationProperty("fromCity", DefaultValue = "Houston", IsRequired = true)]
        public string FromCity
        {
            get { return (string)this["fromCity"]; }
            set { this["fromCity"] = value; }
        }

        /// <summary>
        /// It is getting value of "fromState" from web.config which is assign there and seting its value here
        /// </summary>
        [ConfigurationProperty("fromState", DefaultValue = "TX", IsRequired = true)]
        public string FromState
        {
            get { return (string)this["fromState"]; }
            set { this["fromState"] = value; }
        }

        /// <summary>
        /// It is getting value of "fromZip5" from web.config which is assign there and seting its value here
        /// </summary>
        [ConfigurationProperty("fromZip5", DefaultValue = "77098", IsRequired = true)]
        public string FromZip5
        {
            get { return (string)this["fromZip5"]; }
            set { this["fromZip5"] = value; }
        }

        /// <summary>
        /// It is getting value of "fromZip4" from web.config which is assign there and seting its value here
        /// </summary>
        [ConfigurationProperty("fromZip4", DefaultValue = "", IsRequired = true)]
        public string FromZip4
        {
            get { return (string)this["fromZip4"]; }
            set { this["fromZip4"] = value; }
        }

        /// <summary>
        /// This table returns dataTable with infromation about tracking number and tracking response and their delivery status 
        /// </summary>
        /// <param name="status"></param>
        /// <returns>Datatable with either records or no records</returns>
        public DataTable getdelconfirmation(int status)
        {
            DataTable ds;
            string[] key = { "@status"};
            object[] value = { status };
            ds = clsDb.Get_DT_BySPArr("Usp_Hts_DeliveryConfrimation", key, value);           
            return ds;
        }
    }
}

﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.Office.Interop.Outlook;
using lntechNew.Components.ClientInfo;
using System.Diagnostics;


//Waqas 5864 07/06/2009 Outlook Mail Class
namespace HTP.Components
{
    public class OutlookClass
    {
        #region Common Variables
        clsLogger bugTracker = new clsLogger();
        #endregion

        #region Private Properties
        #endregion

        #region Public Properties

        #endregion

        #region Constructor

        public OutlookClass()
        {

        }
        
        #endregion

        #region Public Methods

        /// <summary>
        /// This method is used to Create New Outlook Mail and save in Msg format at specified path.
        /// </summary>
        /// <param name="To">It represents distination email address of the mail</param>
        /// <param name="Subject">It represents subject of the mail</param>
        /// <param name="HTMLBody">It represents the body of the mail</param>
        /// <param name="DestinationPath">It represents the destination path of the mail</param>
        /// <param name="FileName">It represents the file name of the mail</param>
        public void CreateNewMailSaveMsg(string To, string Subject, string HTMLBody, string DestinationPath, string FileName)
        {
            try
            {
                Process[] processlist = Process.GetProcesses();

                bool isAvailable = false;
                foreach (Process theprocess in processlist)
                {
                    if (theprocess.ProcessName.ToLower() == "OUTLOOK")
                    { isAvailable = true; }
                }

                if (!isAvailable)
                {
                    Process.Start("OUTLOOK");
                }

                Microsoft.Office.Interop.Outlook.Application App = new Microsoft.Office.Interop.Outlook.Application();
                Microsoft.Office.Interop.Outlook.MailItem Item = (Microsoft.Office.Interop.Outlook.MailItem)(App.CreateItem(Microsoft.Office.Interop.Outlook.OlItemType.olMailItem));
                Item.To = To;
                Item.Subject = Subject;
                Item.HTMLBody = HTMLBody;

                Item.SaveAs(DestinationPath + FileName, Microsoft.Office.Interop.Outlook.OlSaveAsType.olMSG);
                Item.Close(Microsoft.Office.Interop.Outlook.OlInspectorClose.olSave);                
                

            }
            catch (System.Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
                
        #endregion

        #region Private Methods

        #endregion 
    }

    
}

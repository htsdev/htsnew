using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NUnit.Framework;
using NUnit.Extensions.Asp;
using NUnit.Extensions.Asp.AspTester;
using NUnit.Extensions.Asp.HtmlTester;

namespace lntechNew.backroom
{
    [TestFixture]
    public class TestClass : WebFormTestCase
    {
        [Test]
        
        public void TestLoginPage()
        { 
            string strUrl = "http://localhost/TroubleTicketManagement/Login.aspx";
            
            Browser.GetPage(strUrl);

            ButtonTester btnlogin = new ButtonTester("btn_Login", CurrentWebForm);
            TextBoxTester txtUserName = new TextBoxTester("txtUsrId", CurrentWebForm);
            TextBoxTester txtPassw = new TextBoxTester("txt_Password", CurrentWebForm);
            

            string Userid="";
            string Passw="";
            btnlogin.Click();
            txtUserName.Text="";
            txtPassw.Text="";
            
            WebAssertion.AssertEquals(Userid,txtUserName.Text);
            WebAssertion.AssertEquals(Passw, txtPassw.Text);

        }
        [Test]
        public void TestViewBugs()
        {
            string strUrl = "http://localhost/TroubleTicketManagement/ViewBugs.aspx";
            Browser.GetPage(strUrl);

            DataGridTester dgView = new DataGridTester("dg_Viewbug", CurrentWebForm);
            string count=dgView.RowCount.ToString();

        }
        [Test]
        public void TestUpdateDeveloper()
        {  
             
            string strUrl = "http://localhost/TroubleTicketManagement/UpdateDeveloper.aspx";
            Browser.GetPage(strUrl);

            DropDownListTester ddlPriority = new DropDownListTester("ddl_priority", CurrentWebForm);
            DropDownListTester ddluser = new DropDownListTester("ddl_users", CurrentWebForm);
            ButtonTester btnSubmit = new ButtonTester("btnSubmit", CurrentWebForm);
            LabelTester Message = new LabelTester("lblMessage",CurrentWebForm);
            LinkButtonTester lnkComments = new LinkButtonTester("lnk_comments", CurrentWebForm);

            string Priority = ddlPriority.Items.Count.ToString();
            string User = ddluser.Items.Count.ToString();
            string Msg = Message.Text.ToString();
            string Comments = lnkComments.Text.ToString();

            btnSubmit.Click();

        }
        [Test]
        public void TestUpdateBug()
        {
            string strUrl = "http://localhost/TroubleTicketManagement/UpdateBug.aspx";
            Browser.GetPage(strUrl);

            DropDownListTester ddlPriority = new DropDownListTester("ddl_priority", CurrentWebForm);
            TextBoxTester txtFileDesc = new TextBoxTester("txt_filedesc", CurrentWebForm);
            LabelTester Message = new LabelTester("lblMessage",CurrentWebForm);
            ButtonTester submit = new ButtonTester("btnSubmit", CurrentWebForm);
            LinkButtonTester lnkAddComments = new LinkButtonTester("lnk_comments", CurrentWebForm);


            string Priority = ddlPriority.Items.Count.ToString();
            string FileDesc = txtFileDesc.Text;
            string Msg = Message.Text.ToString();
            string Comments = lnkAddComments.Text.ToString();

            submit.Click();
        }
        [Test]
        public void TestGeneralView()
        {
            string strUrl = "http://localhost/TroubleTicketManagement/GeneralView.aspx";
            Browser.GetPage(strUrl);

            LabelTester Message = new LabelTester("lblMessage", CurrentWebForm);
            string Msg = Message.Text.ToString();

        }
        [Test]
        public void TestGeneralBug()
        {
            string strUrl = "http://localhost/TroubleTicketManagement/GeneralBug.aspx";
            Browser.GetPage(strUrl);
            TextBoxTester txtShortDesc = new TextBoxTester("txt_shortdesc", CurrentWebForm);
            TextBoxTester txtPageUrl = new TextBoxTester("txt_pageurl", CurrentWebForm);
            TextBoxTester txtTicketNo = new TextBoxTester("txt_ticketno", CurrentWebForm);
            TextBoxTester txtDescription = new TextBoxTester("txt_Desc", CurrentWebForm);
            TextBoxTester txtFileDescription = new TextBoxTester("txt_filedesc", CurrentWebForm);

            DropDownListTester Priority = new DropDownListTester("ddl_priority", CurrentWebForm);
            DropDownListTester Status = new DropDownListTester("ddl_status", CurrentWebForm);
            ButtonTester Submit = new ButtonTester("btnSubmit", CurrentWebForm);
            LabelTester Message = new LabelTester("lblMessage", CurrentWebForm);

            HtmlAnchorTester HPDeveloperLink = new HtmlAnchorTester("Hyperlink1", CurrentWebForm, false);
            HtmlAnchorTester ViewIssues = new HtmlAnchorTester("hp_Viewissues", CurrentWebForm, false);
            
            

            string ShortDesc = txtShortDesc.Text.ToString();
            string PageUrl = txtPageUrl.Text.ToString();
            string TicketNo = txtTicketNo.Text.ToString();
            string Desc = txtDescription.Text.ToString();
            string FileDesc = txtFileDescription.Text.ToString();
            string dPriority = Priority.Items.Count.ToString();
            string dStatus = Status.Items.Count.ToString();
            string Msg = Message.Text.ToString();
            string DeveloperLink = HPDeveloperLink.HRef.ToString();
            string IssueLink = ViewIssues.HRef.ToString();
            
            Submit.Click();

        }
        [Test]
        public void TestDefault()
        {
            string strUrl = "http://localhost/TroubleTicketManagement/Default.aspx";
            Browser.GetPage(strUrl);
            TextBoxTester txtShortDesc = new TextBoxTester("txt_shortdesc", CurrentWebForm);
            TextBoxTester txtPageUrl = new TextBoxTester("txt_pageurl", CurrentWebForm);
            TextBoxTester txtTicketNo = new TextBoxTester("txt_ticketno", CurrentWebForm);
            TextBoxTester txtDescription = new TextBoxTester("txt_Desc", CurrentWebForm);
            TextBoxTester txtFileDescription = new TextBoxTester("txt_filedesc", CurrentWebForm);

            DropDownListTester Priority = new DropDownListTester("ddl_priority", CurrentWebForm);
            DropDownListTester Status = new DropDownListTester("ddl_status", CurrentWebForm);
            ButtonTester Submit = new ButtonTester("btnSubmit", CurrentWebForm);
            //LabelTester Message = new LabelTester("lblMessage", CurrentWebForm);

            
            string ShortDesc = txtShortDesc.Text.ToString();
            string PageUrl = txtPageUrl.Text.ToString();
            string TicketNo = txtTicketNo.Text.ToString();
            string Desc = txtDescription.Text.ToString();
            string FileDesc = txtFileDescription.Text.ToString();
            string dPriority = Priority.Items.Count.ToString();
            string dStatus = Status.Items.Count.ToString();
            //string Msg = Message.Text.ToString();
            Submit.Click();

            
        }
        [Test]
        public void TestAddComments()
        {
            string strUrl = "http://localhost/TroubleTicketManagement/AddComments.aspx";
            Browser.GetPage(strUrl);
            TextBoxTester txtDescription = new TextBoxTester("txt_Desc", CurrentWebForm);
            ButtonTester Submit = new ButtonTester("btnSubmit", CurrentWebForm);
            string Desc = txtDescription.Text.ToString();
            Submit.Click();
        }
        [Test]
        public void TestBugProperties()
        {
            lntechNew.Components.Bug NewBug = new lntechNew.Components.Bug();
            NewBug.BugID = 4566;
            NewBug.BugDate = System.DateTime.Today;
            NewBug.BugDescription = "This is test case description";
            NewBug.DeveloperID = 78554545;
            NewBug.EmployeeID = 42454545;
            NewBug.PageUrl = "www.test.aspx";
            NewBug.PostedDate = System.DateTime.Today;
            NewBug.PriorityID = 7878;
            NewBug.StatusID = 4545;
            NewBug.TicketID = "54454";
            
        }
        [Test]
        public void TestAttachmentProperties()
        {
            lntechNew.Components.Attachment NewAttach = new lntechNew.Components.Attachment();
            NewAttach.BugID = 4525;
            NewAttach.ContentType = "Text";
            NewAttach.Description = "Test Class description";
            NewAttach.EmployeeID = 4524;
            NewAttach.FileName = "TestClassFileNAme";
            NewAttach.Size = 545;
            NewAttach.UploadDate = System.DateTime.Today;

        }
        [Test]
        public void TestCommentProperties()
        {
            lntechNew.Components.Comments NewComments = new lntechNew.Components.Comments();
            NewComments.BugID = 45454;
            NewComments.CommentDate = System.DateTime.Today;
            NewComments.Description = "Test Class Description";
            NewComments.EmployeeID = 3991;
            
        }


    }
}

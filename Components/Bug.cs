using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;
using System.Data.SqlClient;
using System.Data.Common;


namespace lntechNew.Components
{
    /// <summary>
    /// Classes Created BY Fahad For Trouble Ticket EMAIL All Methods are defined in that class
    /// This component will be used from now in trouble ticket all business logic is written here (12/15/2007-Fahad)
    /// </summary>
    /// 

    public enum DropdownType
    {
        Fillstatus,
        FillPriority,
        FillUser

    }



    public class Bug
    {
        clsENationWebComponents ClsDb = new clsENationWebComponents("TroubleTicketDB");
        clsLogger bugTracker = new clsLogger();




        #region Methods

        public int AddNewBug(string BugDescription, int EmployeeID, int StatusID, int PriorityID, string PageUrl, string TicketID, string CauseNo, string desc)
        {
            int bugid = 0;
            try
            {

                string[] key ={ "@ShortDescription", "@EmployeeID", "@StatusID", "@PriorityID", "@PageUrl", "@TicketID", "@CauseNo", "@Web", "@bugdesc", "@databaseid", "BugID" };
                object[] value1 ={ BugDescription, EmployeeID, StatusID, PriorityID, PageUrl, TicketID, CauseNo, 2, desc, 2, 0 };
                bugid = (int)ClsDb.InsertBySPArrRet("usp_Bug_InsertNewBug", key, value1);
                return bugid;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return bugid;
            }
        }
        public bool UpdateBug(int PriorityID, int DeveloperID, int BugID, int StatusID)
        {
            try
            {
                string[] key ={ "@PriorityID", "@DeveloperID", "@BugID", "@StatusID", "@Web" };
                object[] value1 ={ PriorityID, DeveloperID, BugID, StatusID, 2 }; //for dallas webi
                ClsDb.ExecuteSP("usp_Bug_UpdateBug", key, value1);
                return true;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return false;
            }
        }
        public void UpdateBugs(int PriorityID, int DeveloperID, int BugID)
        {
            try
            {
                string[] key ={ "@PriorityID", "@DeveloperID", "@BugID", "@Web" };
                object[] value1 ={ PriorityID, DeveloperID, BugID, 2 };
                ClsDb.ExecuteSP("usp_Bug_UpdateBugs", key, value1);
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        public DataSet ViewBugs()
        {
            DataSet ds = null;
            try
            {
                ds = ClsDb.Get_DS_BySP("usp_bug_GetAllBug");
                return ds;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return ds;
            }
        }
        public DataSet GetEmail(int DeveloperID)
        {
            DataSet ds = null;
            try
            {
                string[] key ={ "@DeveloperID" };
                object[] value1 ={ DeveloperID };
                ds = ClsDb.Get_DS_BySPArr("usp_bug_getEmailAddress", key, value1);
                return ds;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return ds;
            }
        }
        public DataSet DisplayBug(int CaseNumber)
        {
            DataSet Ds = new DataSet();
            try
            {
                string[] key ={ "@ticketID" };
                object[] value1 ={ CaseNumber };
                Ds = ClsDb.Get_DS_BySPArr("usp_bug_getcaseinfo", key, value1);
                return Ds;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return Ds;
            }
        }
        /// <summary>
        /// ADDED BY FAHAD FOR TROUBLE TICKET EMAIL When adding a new trouble ticket 
        /// </summary>
        /// <param name="ticketid"></param>
        /// <param name="PageUrl"></param>
        /// <param name="Bugid"></param>
        /// <param name="priority"></param>
        /// <param name="Description"></param>
        /// <param name="status"></param>
        /// <param name="URL"></param>
        public void SendOPenTroubleTicketEmail(string ticketid, string PageUrl, string Bugid, string priority, string Description, string status, string URL, Boolean grid)
        {
            try
            {
                string subject = "New Trouble";
                if (ticketid.Length > 0)
                {
                    subject += "  TicketID [" + ticketid + "]";
                }
                else
                {
                    subject += " Ticket";
                }

                string msg = BugEmail(Bugid, priority, status, URL, grid);
                MailClass.SendOpenCloseTroubleTicketEmail("<b><center><font size=\"5\">New Trouble Ticket</font><center></b><br />" + "<br /><br />\n" + msg, subject, "");

            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

            }

        }

        /// <summary>
        /// Generalize Method to Filll Drop Down ( 12/15/2007-Fahad)
        /// </summary>
        /// <param name="ddl"> Drop Down List</param>
        /// <param name="dropdowntype"> Enum Type </param>

        public void FillDropdown(DropDownList ddl, DropdownType dropdowntype)
        {
            try
            {
                DataSet DS = new DataSet();
                switch (dropdowntype)
                {
                    case DropdownType.Fillstatus: DS = ClsDb.Get_DS_BySP("usp_bug_getstatus"); break;
                    case DropdownType.FillPriority: DS = ClsDb.Get_DS_BySP("usp_bug_getpriorities"); break;
                    case DropdownType.FillUser: DS = ClsDb.Get_DS_BySP("usp_bug_getusers"); break;
                }
                if (DS.Tables[0].Rows.Count > 0)
                {
                    ddl.Items.Clear();
                    for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                    {
                        string Name = DS.Tables[0].Rows[i]["ename"].ToString().Trim();
                        string ID = DS.Tables[0].Rows[i]["id"].ToString().Trim();
                        ddl.Items.Add(new ListItem(Name, ID));
                    }
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        public DataSet BindGridGeneralView(int Showall)
        {
            DataSet DS_GetBugs = new DataSet();
            try
            {
                string[] key ={ "@Showall", "@Web" };
                object[] value1 ={ Showall, 2 };
                DS_GetBugs = ClsDb.Get_DS_BySPArr("usp_Bug_GetBugs", key, value1);
                return DS_GetBugs;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return DS_GetBugs;
            }
        }
        public DataSet DisplayBugFromBugid(int Bugid)
        {
            DataSet DS_DisplayBug = new DataSet();
            try
            {
                string[] key ={ "@Bugid", "@Web" };
                object[] value1 ={ Bugid, 2};
                DS_DisplayBug = ClsDb.Get_DS_BySPArr("usp_bug_get_bugs", key, value1);
                return DS_DisplayBug;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return DS_DisplayBug;
            }
        }
        public DataSet BindGridUpdateBug(int Bugid)
        {
            DataSet DS_Attachment = new DataSet();
            try
            {
                string[] key ={ "@Bugid" };
                object[] value1 ={ Bugid };
                DS_Attachment = ClsDb.Get_DS_BySPArr("usp_Bug_GetAttachment", key, value1);
                return DS_Attachment;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return DS_Attachment;
            }
        }
        public DataSet DisplayCommentsForUpdateBug(string Bugid, DataList dl_comments)
        {
            DataSet DS_DisplayComments = new DataSet();
            try
            {
                string[] key ={ "@Bugid" };
                object[] value1 ={ Bugid };
                DS_DisplayComments = ClsDb.Get_DS_BySPArr("usp_bug_get_comments", key, value1);

                if (DS_DisplayComments.Tables[0].Rows.Count > 0)
                {
                    dl_comments.DataSource = DS_DisplayComments;
                    dl_comments.DataBind();
                    return DS_DisplayComments;
                }
                else
                {
                    throw new Exception("Error in Procedure Dataset is null in method DisplayCommentsForUpdateBug");
                }
                return DS_DisplayComments;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return DS_DisplayComments;
            }
        }
        public void DisplayDescription(string bugid, DataList dl_des)
        {
            try
            {
                string[] key = { "@bugid" };
                object[] value = { bugid };
                DataSet ds = ClsDb.Get_DS_BySPArr("usp_hts_getdescription", key, value);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    dl_des.DataSource = ds;
                    dl_des.DataBind();
                }
                else
                {
                    throw new Exception("Error In Procedure (usp_hts_getdescription).Dataset is null in method DisplayDescription(string bugid, DataList dl_des) ");
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        /// <summary>
        /// Added By Fahad For Trouble Ticket EMAIL
        /// </summary>
        /// <param name="Developerid"></param>
        /// <param name="Bugid"></param>
        /// <param name="ServerName"></param>
        /// <param name="priority"></param>
        /// <param name="status"></param>
        /// <param name="ticketid"></param>
        /// <param name="URL"></param>
        public void SendMail(int Developerid, string Bugid, string ServerName, string priority, string status, string ticketid, string URL, bool showgrid, string title)
        {
            DataSet DS_Email;
            string EmailAddress = "";
            string DeveloperName = "";
            try
            {
                DS_Email = GetEmail(Developerid);

                if (DS_Email.Tables[0].Rows.Count > 0)
                {
                    EmailAddress = DS_Email.Tables[0].Rows[0]["EmailAddress"].ToString();
                    DeveloperName = DS_Email.Tables[0].Rows[0]["UserName"].ToString();
                    string subject = title + "  BugID[ " + Bugid + " ]";

                    string msg = BugEmail(Bugid, priority, status, URL, showgrid);
                    MailClass.SendOpenCloseTroubleTicketEmail(msg, subject, EmailAddress);
                }
                else
                {
                    throw new NullReferenceException("Dataset in Null");
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        /// <summary>
        /// ADDED By Fahad For Trouble Ticket EMAIL
        /// </summary>
        /// <param name="PageUrl"></param>
        /// <param name="DevName"></param>
        /// <param name="Bugid"></param>
        /// <param name="priority"></param>
        /// <param name="status"></param>
        /// <param name="ticketid"></param>
        /// <param name="URL"></param>
        public void SendCloseTroubleTicketEmail(string PageUrl, string DevName, string Bugid, string priority, string status, string ticketid, string URL, Boolean grid)
        {
            try
            {
                string subject = "";
                if ((ticketid != "") || (ticketid.ToUpper() != "N/A"))
                {
                    subject = "Closed Trouble TicketID [ " + ticketid + "]";
                }
                else
                {
                    subject = "Closed Trouble TicketID";
                }
                string msg = BugEmail(Bugid, priority, status, URL, grid);
                MailClass.SendOpenCloseTroubleTicketEmail(msg, subject, "");
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        /// <summary>
        /// Added By Fahad For the content of trouble tikcet email (12/10/2007 - Fahad)
        /// </summary>
        /// <param name="bugid"></param>
        /// <param name="priority"></param>
        /// <param name="status"></param>
        /// <param name="URL"></param>
        /// <returns></returns>
        public string BugEmail(string bugid, string priority, string status, string URL, Boolean grd)
        {
            DataSet DS_DisplayComments = ClsDb.Get_DS_BySPByOneParmameter("usp_bug_get_comments", "@Bugid", bugid);
            string description = "";
            string shortdescription = "";
            string ticketid2 = "";
            string bugdesc = "";
            string table = "";


            try
            {
                foreach (DataRow dr in DS_DisplayComments.Tables[0].Rows)
                {
                    description = dr["comments"].ToString();
                    shortdescription = dr["title"].ToString();
                    bugdesc = dr["bugdescription"].ToString();
                    ticketid2 = dr["ticketid"].ToString();

                }

                table = "<table border=0 cellpadding=0 cellspacing=0 width=\"600\"><tr><td><table width=\"100%\"><tr><td width=\"13%\" class=\"label\" ><strong>Bug ID:</strong></td>";
                table += "<td width=\"10%\" align=\"left\" class=\"label\" >&nbsp;" + bugid + "&nbsp;</td>";
                table += "<td width=\"17%\" class=label  ><strong>Project: </strong></td>";
                table += "<td width=\"25%\" class=label >&nbsp;Houston Traffic&nbsp;</td>";
                table += "<td width=\"16%\" class=label  ><strong>Status : </strong></td>";
                table += "<td width=\"19%\" class=label>&nbsp;" + status + "&nbsp;</td>";
                table += "</tr></table></td></tr>\n";

                table += "<tr><td><table width=\"100%\"><tr>";
                table += "<td width=\"13%\" class=label ><strong>Priority : </strong></td>";
                table += "<td width=\"10%\" align=left class=label >&nbsp;" + priority + "</td>";
                table += "<td width=\"17%\" class=label ><strong>Ticket No : </strong></td>";
                table += "<td width=\"18%\" align=left class=label >&nbsp;" + ticketid2 + "</td>";
                table += "<td width=\"10%\" class=label ><strong>Date :</strong></td>";
                table += "<td width=\"32%\" class=label>&nbsp;" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + "&nbsp;</td>";
                table += "</tr></table></td></tr>";

                table += "<tr><td><table width=\"100%\"><tr>";
                table += "<td width=\"13%\" class=label ><strong>Title : </strong></td>";
                table += "<td width=\"87%\" class=label >&nbsp;" + shortdescription + "</td>";
                table += "</tr></table></td></tr>";

                table += "<tr><td><table width=\"100%\"><tr>";
                table += "<td width=\"13%\" class=label ><strong> URL : </strong></td>";
                table += "<td width=\"87%\"  class=label >&nbsp;<a href=" + URL + ">Please Click Here</a></td>";
                table += "</tr></table></td></tr>";

                table += "<tr><td class=label >&nbsp;</td></tr><tr><td class=label ><b>Description:</b> </td></tr><tr><td class=label>" + bugdesc + "</td></tr>";

                table += "</table>\n\n\n\n";
                if ((grd == false) && (description != "N/A") && (description != null))
                {
                    string grid = "\n<b>Comments</b><br /><table cellpadding=\"0\" cellspacing=\"0\" border=1 width=\"100%\"><tr><td class=label><b>Date</b></td><td class=label><b>Name</b></td><td class=label style=\"width: 65%\"><b>Comments</b></td></tr>\n";
                    if (DS_DisplayComments.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in DS_DisplayComments.Tables[0].Rows)
                        {
                            grid += "<tr><td class=label>" + Convert.ToDateTime(dr["commentsdate"]).ToShortDateString() + "&nbsp;" + Convert.ToDateTime(dr["commentsdate"]).ToShortTimeString() + "</td>" + "<td class=label>" + Convert.ToString(dr["username"]) + "</td>" + "<td class=label align=\"left\" style=\"width: 60%\">" + Convert.ToString(dr["comments"]) + "</td> </tr>\n";
                        }
                        grid += "</table>";
                    }
                    table = table + grid;
                }



                return table;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return table;
            }
        }


        #endregion

        public Bug()
        {

        }
    }
}

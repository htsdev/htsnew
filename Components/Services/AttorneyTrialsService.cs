﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using HTP.Components.Entities;

namespace HTP.Components.Services
{
    public class AttorneyTrialsService
    {
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        clsLogger bugTracker = new clsLogger();


        /// <summary>
        /// Gets Court names
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetCourtName()
        {
            return ClsDb.Get_DS_BySP("usp_Get_CourtName");
        }

        /// <summary>
        /// Gets Attorney names
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetAttorney()
        {
            return ClsDb.Get_DS_BySP("usp_Get_Attorney");
        }

        /// <summary>
        /// Gets Attorney trial information by rowID. 
        /// </summary>
        /// <param name="AtrnyTrail">Object of Attorney Trials</param>
        /// <returns>DataTable</returns>
        public DataTable GetJuryTrialById(AttorneyTrials AtrnyTrail)
        {
            string[] key = { "@RowId" };
            object[] value = { AtrnyTrail.RowId };

            return ClsDb.Get_DT_BySPArr("USP_HTS_GET_JuryTrial_ByID", key, value);
        }

        /// <summary>
        /// Insert Attorney Trial information
        /// </summary>
        /// <param name="AtrnyTrail">Object of Attorney Trials</param>
        public void InsertJuryTrial(AttorneyTrials AtrnyTrail)
        {
            string[] key = { "@Date", "@Attorney", "@Prosecutor", "@Officer", "@Defendant", "@Caseno", "@Ticketno", "@Courtid", "@Judge", "@Brieffacts", "@Verdictid", "@Verdict", "@Fine", "@Courtreport", "@Rowid"};
            object[] value = { AtrnyTrail.Date, AtrnyTrail.Attorney, AtrnyTrail.Prosecutor, AtrnyTrail.Officer, AtrnyTrail.Defendant, AtrnyTrail.CaseNo, AtrnyTrail.TicketNo, AtrnyTrail.CourtId, AtrnyTrail.Judge, AtrnyTrail.BriefFacts, AtrnyTrail.VerdictId, AtrnyTrail.Verdict, AtrnyTrail.Fine, AtrnyTrail.CourtReport, AtrnyTrail.RowId };

            ClsDb.InsertBySPArrRet("USP_HTS_Insert_JuryTrial", key, value);

        }

        /// <summary>
        /// Update Attorney Trial Information
        /// </summary>
        /// <param name="AtrnyTrail">Object of Attorney Trials</param>
        public void UpdateJuryTrial(AttorneyTrials AtrnyTrail)
        {
            string[] key = { "@Rowid", "@Date", "@Attorney", "@Prosecutor", "@Officer", "@Defendant", "@Caseno", "@Ticketno", "@Courtid", "@Judge", "@Brieffacts", "@Verdictid", "@Verdict", "@Fine", "@Courtreport" };
            object[] value = { AtrnyTrail.RowId, AtrnyTrail.Date, AtrnyTrail.Attorney, AtrnyTrail.Prosecutor, AtrnyTrail.Officer, AtrnyTrail.Defendant, AtrnyTrail.CaseNo, AtrnyTrail.TicketNo, AtrnyTrail.CourtId, AtrnyTrail.Judge, AtrnyTrail.BriefFacts, AtrnyTrail.VerdictId, AtrnyTrail.Verdict, AtrnyTrail.Fine, AtrnyTrail.CourtReport };

            ClsDb.ExecuteSP("USP_HTS_Update_JuryTrial", key, value);

        }
    }
}

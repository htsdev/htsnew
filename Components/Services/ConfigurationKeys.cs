﻿using System;
using Configuration.Client;
using Configuration.Helper;

namespace HTP.Components.Services
{
    /// <summary>
    /// This class represents the all Configuration keys which is getting keys by configuration Service.
    /// </summary>
    public class ConfigurationKeys
    {
        readonly IConfigurationClient _client = ConfigurationClientFactory.GetConfigurationClient();

        #region POLM Configuration Keys

        /// <summary>
        ///  Polm Email To Address
        /// </summary>
        public string EmailTo
        {
            get
            {

                return _client.GetValueOfKey(Division.COMMON, Module.POLM, SubModule.EMAIL, Key.EMAIL_TO_ADDRESS);
            }

        }

        /// <summary>
        ///  Polm Email From Address
        /// </summary>
        public string EmailFrom
        {
            get
            {
                return _client.GetValueOfKey(Division.COMMON, Module.POLM, SubModule.EMAIL, Key.EMAIL_FROM_ADDRESS);
            }

        }
        /// <summary>
        ///  Polm Email Subject
        /// </summary>
        public string EmailSubject
        {
            get
            {
                return _client.GetValueOfKey(Division.COMMON, Module.POLM, SubModule.EMAIL, Key.EMAIL_SUBJECT);
            }
        }
        public string WebisteAddress
        {
            get
            {
                return _client.GetValueOfKey(Division.COMMON, Module.POLM, SubModule.GENERAL, Key.SERVER_ADDRESS);
            }
        }

        /// <summary>
        ///  Polm File Save Location Path
        /// </summary>
        public string FileSaveLocation
        {
            get
            {
                return _client.GetValueOfKey(Division.HOUSTON, Module.POLM, SubModule.GENERAL, Key.REPORT_FILE_PATH);
            }
        }

        /// <summary>
        ///  Address of SMTP Server
        /// </summary>
        public string SmtpServer
        {
            get
            {
                return _client.GetValueOfKey(Division.COMMON, Module.SMTP, SubModule.OUTGOING_SERVER, Key.SERVER_ADDRESS);
            }
        }

        /// <summary>
        ///  User name of the SMTP Server
        /// </summary>
        public string SmtpUser
        {
            get
            {
                return _client.GetValueOfKey(Division.COMMON, Module.SMTP, SubModule.OUTGOING_SERVER, Key.USER_ID);
            }
        }

        /// <summary>
        ///  Password of the SMTP Server
        /// </summary>
        public string SmtpPassword
        {
            get
            {
                return _client.GetValueOfKey(Division.COMMON, Module.SMTP, SubModule.OUTGOING_SERVER, Key.PASSWORD);
            }
        }

        /// <summary>
        ///  SMTP email Sender Name
        /// </summary>
        public string SmtpSenderName
        {
            get
            {
                return _client.GetValueOfKey(Division.COMMON, Module.SMTP, SubModule.EMAIL, Key.EMAIL_SENDER_NAME);
            }
        }

        /// <summary>
        ///  Reply To Address of the SMTP server.
        /// </summary>
        public string SmtpReplyToAddress
        {
            get
            {
                return _client.GetValueOfKey(Division.COMMON, Module.SMTP, SubModule.OUTGOING_SERVER, Key.EMAIL_REPLYTO_ADDRESS);
            }
        }

        /// <summary>
        ///  Use SSL for smtp server
        /// </summary>
        public bool EnableSsl
        {
            get
            {
                return string.IsNullOrEmpty(_client.GetValueOfKey(Division.COMMON, Module.SMTP, SubModule.OUTGOING_SERVER, Key.USE_SSL)) ? false : Convert.ToBoolean(_client.GetValueOfKey(Division.COMMON, Module.SMTP, SubModule.OUTGOING_SERVER, Key.USE_SSL));
            }
        }

        /// <summary>
        ///  Smtp Port for smtp server
        /// </summary>
        public int SmtpPort
        {
            get
            {
                return string.IsNullOrEmpty(_client.GetValueOfKey(Division.COMMON, Module.SMTP, SubModule.OUTGOING_SERVER, Key.PORT)) ? 25 : Convert.ToInt32(_client.GetValueOfKey(Division.COMMON, Module.SMTP, SubModule.OUTGOING_SERVER, Key.PORT));
            }
        }

        /// <summary>
        ///  Subject of the Password Recovery Subject.
        /// </summary>
        public string PasswordRecoverySubject
        {
            get
            {
                return _client.GetValueOfKey(Division.COMMON, Module.POLM, SubModule.PASSWORD_RECOVERY, Key.EMAIL_SUBJECT);
            }
        }

        /// <summary>
        ///  From Email Address of the Password Recovery email.
        /// </summary>
        public string PasswordRecoveryEmailFrom
        {
            get
            {
                return _client.GetValueOfKey(Division.COMMON, Module.POLM, SubModule.PASSWORD_RECOVERY, Key.EMAIL_FROM_ADDRESS);
            }
        }

        /// <summary>
        ///  Polm File/Image Save Location Path
        /// </summary>
        public string NtpathScanImage
        {
            get
            {
                return _client.GetValueOfKey(Division.COMMON, Module.POLM, SubModule.GENERAL, Key.DOCSTORAGE_IMAGE_PATH);
            }
        }

        /// <summary>
        ///  Email Footer legal note
        /// </summary>
        public string EmailFooter
        {
            get
            {
                return _client.GetValueOfKey(Division.COMMON, Module.POLM, SubModule.EMAIL, Key.Footer);
            }
        }
        #endregion

    }
}

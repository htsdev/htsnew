﻿using System;
using System.Data;

namespace HTP.Components
{
    // Abid Ali 4912 11/13/2008 -- Traffic Follow Up Date setting
    /// <summary>
    /// FollowUpType enum helps to idenitify which follow up date is selected
    /// </summary>
    public enum FollowUpType
    {
        /// <summary>
        /// 
        /// </summary>
        NOSFollowUpDate,

        /// <summary>
        /// 
        /// </summary>
        ArrWaitingFollowUpDate,

        /// <summary>
        /// 
        /// </summary>
        BondFollowUpDate,

        /// <summary>
        /// 
        /// </summary>
        CriminalFollowUpDate,

        /// <summary>
        /// 
        /// </summary>
        BondWaitingFollowUpDate,

        /// <summary>
        /// 
        /// </summary>
        TrafficWaitingFollowUpDate,

        // Abid Ali 5018 12/28/2008 Famil waiting follow up
        /// <summary>
        /// 
        /// </summary>
        FamilyWaitingFollowUpDate,

        //Fahad 02/01/2009 5098
        /// <summary>
        /// 
        /// </summary>
        ContractFollowUpDate,

        //Waqas 5653 03/21/2009 For NO LOR Update
        /// <summary>
        /// 
        /// </summary>
        LORFollowUpDate,

        //Waqas 5697 03/261/2009 For HMC FTA Follow Up date
        /// <summary>
        /// 
        /// </summary>
        HMCFTAFollowUpDate,

        //Fahad 5722 04/01/2009 For NonHMC Follow Up date
        /// <summary>
        /// 
        /// </summary>
        NonHMCFollowUpDate,

        // Noufil 5691 04/06/2009 For past court date HMC follow update
        /// <summary>
        /// 
        /// </summary>
        PastCourtDateHMCFollowupdate,

        /// <summary>
        /// 
        /// </summary>
        PastCourtDateNonHMCFollowupdate,

        //Fahad 5753 04/10/2009 For Split Follow Up date
        /// <summary>
        /// 
        /// </summary>
        SplitCaseFollowUpDate,

        // Noufil 5819 05/13/2009 ALR Hearinng Follow Up date
        /// <summary>
        /// 
        /// </summary>
        ALRHearingFollowUpdate,

        //Sabir Khan 5977 07/01/2009 HMC Setting Discrepancy Follow Up Date
        /// <summary>
        /// 
        /// </summary>
        HMCSettingDiscrepancyFollowUpDate,

        //Nasir 7234 01/08/2010 HMC Same Day Arr.
        /// <summary>
        /// 
        /// </summary>
        HMCSameDayArrFollowUpDate,

        //Afaq 7752 05/04/2010 Dispose Alert 
        /// <summary>
        /// 
        /// </summary>
        DisposeAlert,

        /// <summary>
        /// Bad Email Address Followup Date. 
        /// </summary>
        BadEmailAddress, //SAEED 7844 06/23/2010 for Bad Email Address

        /// <summary>
        /// Address Validation Followup Date. 
        /// </summary>
        AddressValidationReport, //SAEED 7844 06/23/2010 for Address Validation report

        /// <summary>
        /// Missing Cause Follow update.
        /// </summary>
        MissingCauseFollowUpdate, //Muhammad Ali 7747 06/23/2010 for Missing Cause Follow update.

        //Saeed 8101 09/24/2010  HCJP Auto Update Alert Follow up Date added.
        /// <summary>
        /// HCJP Auto Update Alert Follow up Date
        /// </summary>
        HCJPAutoAlertFollowUpDate,
        //Sabir Khan 8488 12/24/2010 Bad Addresses Follow Up Date Added
        /// <summary>
        /// Bad Addresses Follow up Date
        /// </summary>
        BadAddressFollowUpDate,

        //Muhammad Nadir Siddiqui 9134 4/28/2011 Added No LOR Comfirmation Follow Up date
        /// <summary>
        /// No LOR Comfirmation Follow up Date
        /// </summary>
        NoLORConfirmationFollowUpDate,


        //Rab Nawaz 10729 4/16/2013 Added Nisi Follow Up date
        /// <summary>
        /// No LOR Comfirmation Follow up Date
        /// </summary>
        NisiFollowUpDate

    }
}

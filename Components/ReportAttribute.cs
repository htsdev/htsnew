﻿using System;
using System.Data;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace HTP.Components
{
    //Saeed 8101 09/29/2010 class created.
    /// <summary>
    /// ReportAttribute:Class written to handle report attribute.
    /// </summary>
    public class ReportAttribute
    {
        //Create an Instance of Data Access Class
        readonly clsENationWebComponents _clsDb = new clsENationWebComponents();

        //Creating the new instance og bug logger
        readonly clsLogger _bugTracker = new clsLogger();

        #region Variables
        private string _attributeName = String.Empty;
        private string _shortName = String.Empty;
        private string _description = String.Empty;
        private DateTime _insertdate = DateTime.Now;
        private int _insertby;
        private DateTime _lastupdatedate = DateTime.Now;
        private int _lastupdateby;
        private string _attributetype = String.Empty;
        #endregion

        #region Properties


        /// <summary>
        /// Get or Set attribute name   
        /// </summary>
        public string AttributeName
        {
            get
            {
                return _attributeName;
            }
            set
            {
                _attributeName = value;
            }
        }

        /// <summary>
        /// Get or Set short name   
        /// </summary>

        public string ShortName
        {
            get
            {
                return _shortName;
            }
            set
            {
                _shortName = value;
            }
        }

        /// <summary>
        /// Get or Set description   
        /// </summary>

        public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
            }

        }

        /// <summary>
        /// Get or Set the insert date   
        /// </summary>
        public DateTime InsertDate
        {
            get
            {
                return _insertdate;
            }
            set
            {
                _insertdate = value;
            }
        }

        /// <summary>
        /// Get or Set the user id who is inserting    
        /// </summary>
        public int InsertBy
        {
            get
            {
                return _insertby;
            }
            set
            {
                _insertby = value;
            }
        }

        /// <summary>
        /// Get or Set the Last Update Date   
        /// </summary>
        public DateTime LastUpdateDate
        {
            get
            {
                return _lastupdatedate;
            }
            set
            {
                _lastupdatedate = value;
            }
        }


        /// <summary>
        /// Get or Set the user id who is last inserting
        /// </summary>
        public int LastUpdateBy
        {
            get
            {
                return _lastupdateby;
            }
            set
            {
                _lastupdateby = value;
            }
        }

        /// <summary>
        /// Get or Set attribute type   
        /// </summary>
        public string AttributeType
        {
            get
            {
                return _attributetype;
            }
            set
            {
                _attributetype = value;
            }
        }


        #endregion

        /// <summary>
        /// This Function return all the Attributes
        /// </summary>
        /// <returns> Dataset with all the attribute records</returns>
        public DataTable ListAllAttribute()
        {

            var dt = _clsDb.Get_DT_BySPArr("USP_ReportSetting_Get_ReportAttribute");
            return dt;
        }

        /// <summary>
        /// This Function return information about ReportAttribute
        /// </summary>
        /// <param name="attributeId">Attribute Id</param>
        /// <returns>ReportAttribute Object</returns>
        public ReportAttribute GetAttributeInfo(int attributeId)
        {
            var attribute = new ReportAttribute();
            try
            {

                string[] keys = { "@attributeid" };
                object[] values = { attributeId };
                var dT = _clsDb.Get_DT_BySPArr("USP_ReportSetting_GetByID_ReportAttribute", keys, values);

                if (dT.Rows.Count > 0)
                {
                    var dr = dT.Rows[0];
                    attribute.AttributeName = dr["AttributeName"].ToString();
                    attribute.ShortName = dr["ShortName"].ToString();
                    attribute.Description = dr["Description"].ToString();
                    attribute.AttributeType = dr["AttributeType"].ToString();
                }
            }
            catch (Exception ex)
            {
                _bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

            }
            return attribute;
        }

        /// <summary>
        /// This Function is used to insert or update the record in the ReportAttribute table
        /// </summary>
        /// <param name="attributeId">Attribute Id</param>
        /// <returns>Returns string acknowledge message</returns>
        public string InsertUpdateAttributeInfo(int attributeId)
        {
            var result = String.Empty;
            try
            {
                if (attributeId == 0)
                {

                    string[] keys = { "@attributeid", "@attributename", "@shortname", "@description", "@insertby", "@lastupdateby", "@attributetype" };
                    object[] values = { attributeId, _attributeName, _shortName, _description, _insertby, _lastupdateby, _attributetype };
                    result = _clsDb.ExecuteScalerBySp("USP_ReportSetting_Insert_ReportAttribute", keys, values).ToString();
                }
                else
                {
                    string[] keys = { "@attributeid", "@attributename", "@shortname", "@description", "@lastupdateby", "@attributetype" };
                    object[] values = { attributeId, _attributeName, _shortName, _description, _lastupdateby, _attributetype };
                    result = _clsDb.ExecuteScalerBySp("USP_ReportSetting_Update_ReportAttribute", keys, values).ToString();

                }
            }
            catch (Exception ex)
            {
                _bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

            }
            return result;

        }

        /// <summary>
        /// This Function is used to delete the record in the ReportAttribute table when it is no
        /// present in the ReportSetting table
        /// </summary>
        /// <param name="attributeId">Attribute Id</param>
        /// <returns>Returns True if process completed Successfully else False</returns>
        public bool DeleteAttributeInfo(int attributeId)
        {
            var result = false;
            try
            {
                if (attributeId != 0)
                {
                    string[] keys = { "@attributeid" };
                    object[] values = { attributeId };
                    result = Convert.ToBoolean(_clsDb.ExecuteScalerBySp("USP_ReportSetting_Delete_ReportAttribute", keys, values));
                }
            }
            catch (Exception ex)
            {
                _bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
            return result;
        }
    }
}

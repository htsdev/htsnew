﻿using System;
using System.Data;
using System.Data.Common;
using FrameWorkEnation.Components;
using System.Web;
using System.Configuration;
using System.Collections;

namespace HTP.Components
{
    public class Attorneys
    {
        clsENationWebComponents ClsDb = new clsENationWebComponents("Connection String");

        private int promoID;
        private string attorneyName;
        private int mailerName;
        private string latterMsg;
        private string htpMsg;
        private int violCount;

        public int PromoID
        {
            get
            {
                return promoID;
            }
            set
            {
                promoID = value;
            }
        }

        public string AttorneyName
        {
            get
            {
                return attorneyName;
            }
            set
            {
                attorneyName = value;
            }
        }

        public int MailerName
        {
            get { return mailerName; }
            set { mailerName = value; }
        }
        public string LatterMessage
        {
            get { return latterMsg; }
            set { latterMsg = value; }
        }
        public string MatterMessage
        {
            get { return htpMsg; }
            set { htpMsg = value; }
        }
        public int ViolationCount
        {
            get { return violCount; }
            set { violCount = value; }
        }

        /// <summary>
        /// Get Promotional information of Attorneys
        /// </summary>       
        public DataTable GetPromoInfo()
        {
            DataTable dt = ClsDb.Get_DT_BySPArr("usp_Get_AttorneyPromoTemplate");            
            return dt;
        }

        public void DeletePromoScheme()
        { 
            string[] key1 = { "@PromoID"};
            object[] value1 = { PromoID};
            ClsDb.ExecuteSP("usp_HTP_DeletePromoScheme", key1, value1);
        }
        /// <summary>
        /// Get Promotional information of Attorneys
        /// </summary>       
        public DataTable GetAttorneyNames()
        {
            DataTable dt = ClsDb.Get_DT_BySPArr("usp_Get_AttorneyNames");
            return dt;
        }

        public DataTable GetSelectedPromoScheme()
        {
            DataTable dt = ClsDb.Get_DS_BySPByOneParmameter("usp_Get_PromoScheme", "@PromoID", PromoID).Tables[0];
            return dt;
        }

        public void AddPromoScheme()
        {
            string[] key1 = { "@MailerID", "@AttorneyName", "@PromoMessageForLatter", "@PromoMessageForHTP", "@ViolationCount" };
            object[] value1 = { MailerName, AttorneyName, LatterMessage, MatterMessage, ViolationCount };
            ClsDb.ExecuteScalerBySp("usp_HTP_AddPromoScheme", key1, value1);
        }

        /// <summary>
        /// Get Promotional information of Attorneys
        /// </summary>       
        public DataTable GetPromoMailersNames()
        {
            DataTable dt = ClsDb.Get_DT_BySPArr("usp_Get_PromoMailers");
            return dt;
        }

        public void UpdatePromoInfo()
        {
            string[] key1 = { "@PromoID", "@AttorneyName", "@MailerName", "@LatterMsg", "@MatterMsg", "@ViolationCount" };
            object[] value1 = { PromoID,AttorneyName,MailerName,LatterMessage,MatterMessage,ViolationCount };
            ClsDb.ExecuteScalerBySp("usp_HTP_Update_AttorneyPromoInfo", key1, value1);
        }

    }
}

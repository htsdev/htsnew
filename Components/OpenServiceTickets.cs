using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;
using System.Data.SqlClient;
using System.Data.Common;
using LNHelper;

namespace lntechNew.Components
{

    /// <summary>
    /// Service Type Enumerated Data Type
    /// </summary>
    public enum ServiceTicketType
    {
        Assigned,
        Closed,
        Open,
        Updated,
        None
    }

    /// <summary>
    /// Service Categories Enumerated Data Type
    /// </summary>
    public enum ServiceTicketCategories
    {
        Continuance = 4,
        AngryClient = 2,
        Miscellaneous = 3,
        BouncedCheck = 5,
        BondForfeiture = 6,
        CheckCourtSetting = 7,
        AttorneyCallBack = 8,
        None
    }


    public class OpenServiceTickets
    {

        #region Variables

        DBComponent ClsDb = new DBComponent("Connection String");
        clsLogger bugTracker = new clsLogger();
        private DateTime _duedate;
        private DataTable _dt = null;
        private object[] _values;
        private object[] _updatevalues;
        //khalid 14-9-07
        private int per_completion;
        

       
        #endregion

        #region Properties

        //Added By Khalid 14-9-07
        public int Per_Completion
        {
            get { return per_completion; }
            set { per_completion = value; }
        }


        public DateTime DueDate
        {
            get { return _duedate; }
            set { _duedate = value; }
        }

        public object[] Values
        {
            get { return _values; }
            set { _values = value; }
        }

        //public DataSet DS
        //{
        //    get { return _ds; }
        //    set { _ds = value; }
        //}

        public DataTable DT
        {
            get { return _dt; }
            set { _dt = value; }
        }

        public object[] UpdateValues
        {
            get
            {
                return _updatevalues;
            }
            set
            {
                if (value == null || value.Length < 3)
                {
                    throw new ArgumentException("Parameter Value may not be null or blank or less than 3");
                }
                _updatevalues = value;
            }
        }
        

        #endregion

        #region Service Ticket Variables

        int ticketid = 0;
        int fid = 0;
        int empid = 0;
        string generalcomments = "";
        string percentage = "0";
        string category = "0";
        string priority = "0";
        int showontrialdocket = 0;
        bool showserviceinstruction = false;
        string serviceinstruction = string.Empty;
        int assignto = 0;
        bool showgeneralcomments = false;
        string firstname = string.Empty;
        string lastname = string.Empty;
        //Fahad 5815 04/28/2009 
        string enteredcomments = string.Empty;
        string priorityDescription = string.Empty;
        string assignedToName = string.Empty;
        string serviceTicketCategoryName = string.Empty;
        // Zahoor 4770 09/12/08
        int _ContinuanceOption = 0;

        string Followupdate = string.Empty;

        

        #endregion

        #region Service Ticket Properties

        public int TicketID
        {
            set
            {
                ticketid = value;
            }
        }

        public int FID
        {
            set
            {
                fid = value;
            }
        }

        public int EmpID
        {
            set
            {
                empid = value;
            }

        }

        public int ShowOnTrialDocket
        {
            set
            {
                showontrialdocket = value;
            }
            get
            {
                return showontrialdocket;
            }
        }

        public string GeneralComments
        {
            set
            {
                generalcomments = value;
            }
            get
            {
                return generalcomments;
            }
        }

        public string Percentage
        {
            set
            {
                percentage = value;
            }
            get
            {
                return percentage;
            }
        }

        public string Category
        {
            set
            {
                category = value;
            }
            get
            {
                return category;
            }
        }

        public string Priority
        {
            set
            {
                priority = value;
            }
            get
            {
                return priority;
            }

        }

        public string ServiceInstruction
        {
            set
            {
                serviceinstruction = value;
            }

            get
            {
                return serviceinstruction;
            }
        }
        

        public bool ShowServiceTicketInstruction
        {
            set
            {
                showserviceinstruction = value;
            }
            get
            {
                return showserviceinstruction;
            }
        }

        public int AssignTo
        {
            set
            {
                assignto = value;
            }
            get
            {
                return assignto;
            }
        }

        public bool ShowGeneralComments
        {

            set
            {
                showgeneralcomments = value;
            }
            get
            {
                return showgeneralcomments;
            }
        }

        public string FirstName
        {
            get
            {
                return firstname;
            }
            set
            {
                firstname = value;
            }
        }

        public string LastName
        {
            get
            {
                return lastname;
            }
            set
            {
                lastname = value;
            }
        }

        public int ContinuanceOption
        {
            get { return _ContinuanceOption; }
            set { _ContinuanceOption = value; }
        }
        //Fahad 5815 04/28/2009
        public string EnteredComments
        {
            set
            {
                enteredcomments = value;
            }

            get
            {
                return enteredcomments;
            }
        }

        public string ServiceTicketFollowUpdate
        {
            set
            {
                Followupdate = value;
            }
            get 
            {
                return Followupdate;
            }
        }

        #endregion

        #region Methods

        // Agha Usman 4271 06/25/2008
        // Noufil 5069 11/05/2008  Add new parameter date range.
        //Sabir Khan 5738 05/25/2009 code commented...
        //------------------------------------------------
        //public DataTable GetRecords(string dateFrom, string dateTo, int empId, string ticketType, string showOnlyIncomplete, string showAllMyTickets, string showAllUserTickets, int CaseTypeId, int isdaterange, int showAllfollowup)
        //{

        //    try
        //    {
        //        //Sabir Khan
        //        string[] keys = { "@StartDate", "@EndDate", "@EmpID", "@TicketType", "@ShowOnlyIncomplete", "@ShowAllMyTickets", "@ShowAllUserTickets", "@CaseTypeId", "@isdaterange", "@showAllfollowup" };
        //        object[] values = { dateFrom, dateTo, empId, ticketType, showOnlyIncomplete, showAllMyTickets, showAllUserTickets, CaseTypeId, isdaterange, showAllfollowup };
        //        DT = ClsDb.GetDTBySPArr("Usp_Hts_Get_OpenServiceTicketsRecords", keys, values);
        //        return DT;
        //    }
        //    catch (Exception ex)
        //    {
        //        bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
        //        return null;
        //    }
        //}
        //Sabir Khan 5738 05/25/2009 Get records...
        public DataTable GetRecords(string dateFrom, string dateTo, int empId, string ticketType, string showOnlyIncomplete, int CaseTypeId, int showAllfollowup)
        {

            try
            {
                //Sabir Khan
                string[] keys = { "@StartDate", "@EndDate", "@EmpID", "@TicketType", "@ShowOnlyIncomplete", "@CaseTypeId", "@showAllfollowup" };
                object[] values = { dateFrom, dateTo, empId, ticketType, showOnlyIncomplete, CaseTypeId,  showAllfollowup };
                DT = ClsDb.GetDTBySPArr("Usp_Hts_Get_OpenServiceTicketsRecords", keys, values);
                return DT;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return null;
            }
        }
        //---------------------------------------------------------------------------------------
        public DataTable GetGeneralComments()
        {
            try
            {
                string[] keys = { "@TicketID", "@Fid" };
                DT = ClsDb.GetDTBySPArr("usp_hts_get_generalcomments", keys, Values);

                return DT;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return DT;
            }
        }

        public bool UpdateServiceTicketInformation()
        {

            try
            {
                // Zahoor 4770 09/12/08 Add ContinuanceOption parameter
                //Fahad 5815 Add parameter @enterdcomments
                //Sabir Khan 5738 06/01/2009 Update modified for follow up date date...
                string[] keys ={ "@TicketID", "@Fid", "@EmpID", "@per_completion", "@Category", "@Priority", "@ShowTrialDocket", "@ShowServiceInstruction", "@ServiceInstruction", "@AssignedTo", "@ShowGeneralComments", "@ContinuanceOption","@EnteredComments","@Followupdate" };
                object[] values = { ticketid, fid, empid, percentage, category, priority, showontrialdocket, showserviceinstruction, serviceinstruction, assignto, showgeneralcomments, _ContinuanceOption,enteredcomments,DateTime.Parse(Followupdate) };
                ClsDb.ExecuteSP("usp_hts_updategeneralcomments_FromOpenServieTicketReport", keys, values);
                return true;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return false;
            }

        }

        //Aziz 2382 12/19/2007
        //Adding of service ticket option
        public bool AddServiceTicket()
        {            
            try
            {

                // Zahoor 4770 09/12/08 Add ContinuanceOption parameter
                //Sabir Khan 5738 06/01/2009 Add method modified for follow up date date...
                string[] keys ={ "@TicketID", "@EmpID", "@per_completion", "@Category", "@Priority", "@ShowTrialDocket", "@ShowServiceInstruction", "@ServiceInstruction", "@AssignedTo", "@ShowGeneralComments", "@ContinuanceOption","@ServiceTicketfollowup" };
                object[] values = { ticketid, empid, Convert.ToInt32(percentage),Convert.ToInt32(category),Convert.ToInt32(priority),Convert.ToInt32(showontrialdocket),Convert.ToInt32(showserviceinstruction), serviceinstruction, Convert.ToInt32(assignto), Convert.ToInt32(showgeneralcomments), Convert.ToInt32(_ContinuanceOption), DateTime.Parse(Followupdate) };
                ClsDb.ExecuteSP("usp_hts_AddServiceTicket", keys, values);
                return true;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return false;
            }
        }

        //Method Added By Fahad to get EmailAddress Fahad(11-1-08)
        public string GetEmail(int employeeid)
        {
            string[] keys ={ "@employeeid" };
            object[] values = { employeeid };
            DataSet ds = ClsDb.GetDSBySPArr("usp_hts_get_email", keys, values);
            string email = Convert.ToString(ds.Tables[0].Rows[0]["email"]);
            return email;

        }
        //End Fahad

        public void CloseServiceTicket(int ticketid, int Fid)
        {
            try
            {
                string[] keys ={ "@TicketID", "@Fid" };
                object[] values = { ticketid, Fid };
                ClsDb.ExecuteSP("USP_HTS_Close_Service_Ticket", keys, values);
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        public void RemoveDocket(int ticketid, int Fid)
        {
            try
            {
                string[] keys ={ "@TicketID", "@Fid" };
                object[] values = { ticketid, Fid };
                ClsDb.ExecuteSP("USP_HTS_Remove_From_Docket", keys, values);
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        public DataSet GetServiceTicketAlerts()
        {
            try
            {
                string[] keys = { "@EmpID" };
                return ClsDb.GetDSBySPArr("usp_hts_get_ServiceTicketAlerts", keys, Values);

                //return DT;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return null;
            }
        }


        public bool AssignTicketToUser(int ticketid, int employeeid, int fid)
        {

            try
            {
                string[] keys ={ "@TicketID", "@EmpId", "@Fid" };
                object[] values = { ticketid, employeeid, fid };
                ClsDb.ExecuteSP("USP_HTS_Assign_Service_Ticket", keys, values);
                return true;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return false;
            }

        }

        public void Update()
        {
            try
            {
                string[] keys ={ "@EmpID" };
                ClsDb.ExecuteScalerBySP("Usp_Hts_AlertUpdateDate", keys, Values);


            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

            }
        }

        /// <summary>
        /// This function gets information about the service ticket.
        /// </summary>
        /// <param name="ticketid">Case Number</param>
        /// <param name="fid">Primary key of the service ticket record</param>
        /// <returns>Return DataSet that contain the detail information about the service ticket</returns>
        public bool GetServiceTicketInformation(int ticketid, int fid)
        {
            string[] key ={ "@Ticketid", "@Fid" };
            object[] value ={ ticketid, fid };
            DataSet ServiceTicketInfo = ClsDb.GetDSBySPArr("usp_hts_Get_ServiceTicket_Information", key, value);


            if (ServiceTicketInfo.Tables.Count > 0)
            {
                // Zahoor 4770 09/12/08 : To handle zero rows case.
                if (ServiceTicketInfo.Tables[0].Rows.Count > 0)
                {
                    showgeneralcomments = Convert.ToBoolean(ServiceTicketInfo.Tables[0].Rows[0]["ShowGeneralComments"]);
                    showserviceinstruction = Convert.ToBoolean(ServiceTicketInfo.Tables[0].Rows[0]["ShowServiceInstruction"]);
                    showontrialdocket = Convert.ToInt32(ServiceTicketInfo.Tables[0].Rows[0]["ShowTrailDocket"]);

                    //Select Category
                    category = Convert.ToString(ServiceTicketInfo.Tables[0].Rows[0]["ServiceTicketCategory"]);

                    //Select Priority
                    priority = Convert.ToString(ServiceTicketInfo.Tables[0].Rows[0]["Priority"]);

                    //Select Completion Percentage
                    percentage = Convert.ToString(ServiceTicketInfo.Tables[0].Rows[0]["ContinuanceStatus"]);

                    //If User Exist In List Than Select
                    assignto = Convert.ToInt32(Convert.ToInt32(ServiceTicketInfo.Tables[0].Rows[0]["AssignedTo"]));

                    //Display General Comments
                    generalcomments = Convert.ToString(ServiceTicketInfo.Tables[0].Rows[0]["GeneralComments"]);

                    //Display Service Ticket Instruction
                    serviceinstruction = Convert.ToString(ServiceTicketInfo.Tables[0].Rows[0]["ServiceTicketInstruction"]);

                    //Display Client Name
                    lastname = Convert.ToString(ServiceTicketInfo.Tables[0].Rows[0]["LastName"]);

                    firstname = Convert.ToString(ServiceTicketInfo.Tables[0].Rows[0]["FirstName"]);

                    //Get Priority
                    priorityDescription = Convert.ToString(ServiceTicketInfo.Tables[0].Rows[0]["priorityDescription"]);

                    //Get Assigned User Name
                    assignedToName = Convert.ToString(ServiceTicketInfo.Tables[0].Rows[0]["AssignedToName"]);

                    //Get Service Ticket Category Name
                    serviceTicketCategoryName = Convert.ToString(ServiceTicketInfo.Tables[0].Rows[0]["ServiceTicketCategoryName"]);

                    // Zahoor 4770 09/16/08 
                    // Get Service ticket Continuance Option
                    _ContinuanceOption = int.Parse(ServiceTicketInfo.Tables[0].Rows[0]["ContinuanceOption"].ToString());

                    //Sabir Khan 5738 06/01/2009 Get follow update date...
                    Followupdate  = ServiceTicketInfo.Tables[0].Rows[0]["servicefollowupdate"].ToString();
                }

            }
            else
            {
                return false;
            }

            return true;
        }

        public DataSet GetServiceTicketCategories()
        {
            return ClsDb.GetDSBySP("USP_HTS_GET_SERVICETICKET_CATEGORIES");
        }

        // Zahoor 4770 09/12/08
        /// <summary>
        /// This method returns Continuance options in case of service ticket category is Continuance.
        /// </summary>
        /// <returns> Data Table</returns>
        public DataTable GetServiceTicketContinueanceOptions()
        {

            DataTable options = ClsDb.GetDTBySPArr("USP_HTS_GETCONTINOUSOPTIONS");

            return options;
        }

        /// <summary>
        /// Return Service Ticket Category By Division
        /// </summary>
        /// <param name="CaseTypeId">Case Type (Division) Id</param>
        /// <returns></returns>
        public DataTable GetServiceTicketCategories(int CaseTypeId)
        {
            // Parameterized procedure created
            // Agha Usman 4271 06/27/2008
            string[] key = { "@CaseTypeId" };
            object[] value = { CaseTypeId };
            DataTable violations = ClsDb.GetDTBySPArr("USP_HTS_GET_SERVICETICKET_CATEGORIES", key, value);

            return violations;//ClsDb.Get_DS_BySP("USP_HTS_GET_SERVICETICKET_CATEGORIES");
        }

        //Created By Zeeshan Ahmed On 1/22/2008
        /// <summary>
        /// Send Service Ticket Notification Email
        /// </summary>
        /// <param name="serviceTicketType">Service Ticket Type</param>
        /// <param name="serviceTicketCategories">Service Ticket Category Type</param>
        /// <param name="url">URL of Page</param>
        /// <param name="category">Service Category Name</param>
        /// <param name="priority">Priority</param>
        /// <param name="empName">Rep Name</param>
        /// <param name="generalComments">General Comments</param>
        /// <param name="ticketid">Ticket ID</param>
        /// <param name="assignedTo">Assigned To</param>
        /// <param name="assignedUserEmail">Assigned User Email</param>
        public void SendServiceTicketNotificationEmail(ServiceTicketType serviceTicketType, ServiceTicketCategories serviceTicketCategories, string url, string category, string priority, string empName, string generalComments, int ticketid, string assignedTo, string percentage, string assignedUserEmail, int CaseTypeId)
        {


            this.ticketid = ticketid;

            DataTable violations = GetCaseInformation();

            if (violations.Rows.Count > 0)
            {
                this.ticketid = ticketid;
                string firstName = Convert.ToString(violations.Rows[0]["FirstName"]);
                string lastName = Convert.ToString(violations.Rows[0]["LastName"]);
                string courtLocation = Convert.ToString(violations.Rows[0]["courtLocation"]);
                // Noufil 4787 09/21/2008 Add ticket number
                string ticketnumber = Convert.ToString(violations.Rows[0]["ref"]);
                //Kazim 3826 04/23/2008
                int bond = Convert.ToInt16(violations.Rows[0]["bondflag"].ToString());
                // Noufil 4447 08/18/2008 Active flag added to modify email subject
                int activeflag = Convert.ToInt32(violations.Rows[0]["activeflag"]);

                url = "http://" + url + "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + Convert.ToString(ticketid);

                string serviceTicketDateText = "";
                string subject = "";
                string to = "";
                string body = "";
                string name = "<a href=" + url + ">" + lastName + " " + firstName + "</a>";

                CaseType objCaseType = new CaseType();
                objCaseType.CaseTypeId = CaseTypeId;
                DataTable dt = objCaseType.GetCaseTypeInfo();
                
                string OpenEmail = dt.Rows[0]["ServiceTicketEmailAlert"].ToString();
                string CloseEmail = dt.Rows[0]["ServiceTicketEmailClose"].ToString();
                string AngryOpenEmail = dt.Rows[0]["ServiceTicketAngryEmailOpen"].ToString();
                string AngryCloseEmail = dt.Rows[0]["ServiceTicketAngryEmailClose"].ToString();
                string caseTypeName = dt.Rows[0]["CaseTypeName"].ToString();
                // Zahoor 4757 09/09/2008
                string UpdateEmail = dt.Rows[0]["ServiceTicketUpdateEmail"].ToString();
                // Noufil 4447 08/18/2008 active flag added to decide whether case is for prospect (activeflag 0) or client (activeflag 1)
                string type = (activeflag == 0 ? "Prospect " : "Client");


                switch (serviceTicketType)
                {
                    case ServiceTicketType.Assigned:
                        subject = "Service Ticket Assigned To  � " + assignedTo + " - " + type;
                        serviceTicketDateText = "Assign D/T";
                        //Asad Ali 7781 5/11/2010 (Send Service Ticket Email at Assigned User Email Address).
                        //to = OpenEmail;
                        to = assignedUserEmail;
                        //End 7781
                        break;

                    case ServiceTicketType.Open:

                        // Noufil 4787 09/21/2008 Add ticket number
                        subject = "Service Ticket Open � " + lastName + "," + firstName + " - " + ticketnumber + " - " + category + " - " + type;
                        //Modified by Ozair for Task 2710 on 01/26/2008 [changed  text]
                        serviceTicketDateText = "Open D/T";

                        switch (serviceTicketCategories)
                        {
                            //TODO ADD Application Section For Service Ticket
                            case ServiceTicketCategories.AngryClient: to = AngryOpenEmail; break;
                            //TODO ADD Application Section For Service Ticket

                            default: 
                                to = OpenEmail;
                                //Sabir Khan 6670 10/05/2009 Get email address of associated user...
                                string associatedEmailto = GetAssociatedEmailAddress(category);
                                to = to + ',' + associatedEmailto; 
                                break;
                        }
                        break;

                    case ServiceTicketType.Closed:

                        // Noufil 4787 09/21/2008 Add ticket number
                        subject = "Service Ticket Close - " + lastName + "," + firstName + " - " + ticketnumber + " - " + category + " - " + type;
                        //Modified by Ozair for Task 2710 on 01/26/2008 [changed  text]
                        serviceTicketDateText = "Close D/T";

                        switch (serviceTicketCategories)
                        {
                            //TODO ADD Application Section For Service Ticket

                            //Kazim 3573 3/31/2008 
                            // Send mail to CloserServiceUser when Service Ticket closed, If client is Angry client
                            // then mail also send to AngryClientClosedEmail.

                            case ServiceTicketCategories.AngryClient: to = AngryCloseEmail + " ," + CloseEmail; break;
                            default: to = CloseEmail; break;
                        }
                        break;
                    case ServiceTicketType.Updated:
                        // Noufil 4787 09/21/2008 Add ticket number
                        subject = "Service Ticket Updated � " + lastName + "," + firstName + " - " + ticketnumber + " - " + category + " - " + type;
                        //Modified by Ozair for Task 2710 on 01/26/2008 [changed  text]
                        serviceTicketDateText = "Update D/T";
                        // Zahoor 4757 09/09/2008
                        to = UpdateEmail;
                        break;
                }

                #region Service Ticket Email Body

                //Format HTML Of Case Violations
                string grid = "<table width='438' border=0 cellpedding=0 cellspacing =0><tr><td width='111'><b>Ticket#</b></td><td width='98' ><b>Court date</b></td><td width='89'><b>Time</b></td><td width='64'><b>Court#</b></td><td width='66'><b>Status</b></td></tr>";

                foreach (DataRow dr in violations.Rows)
                    grid += "<tr><td class=label>" + dr["refcasenumber"] + "</td><td class=label>" + dr["courtDate"] + "</td>" + "<td class=label>" + dr["courtTime"] + "</td><td class=label>&nbsp;&nbsp;" + dr["courtnumbermain"] + "</td><td class=label>" + dr["Courtviolationstatus"] + "</td></tr>\n";

                //Kazim 3826 4/23/2008 Add BondFlag in the Email body

                grid += "</table>";
                //Modified by Ozair for Task 2710 on 01/26/2008 [added percentage also included some more \n,reduced width of 1st col for better html]
                //Format Service Ticket Information
                body = "<center><font size=\"5\"><b>Service Ticket</b></font></center><br />" + "<br /><br /> <table width='460'><tr><td align=left><table class=clsLeftPaddingTable width='440' border='0' cellpadding='0' cellspacing='0'>\n<tr><td width='100' align='left' NOWRAP><b>Client Name: </b> </td><td width='340' align='left'>" + name;

                if (bond == 1)
                {
                    body += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><font color=\"red\">BOND</font></b>";
                }
                body += "</td></tr><tr><td><b>Division Type:</b></td><td>" + caseTypeName + "</td></tr><tr><td><b>Sales Rep:</b></td><td>" + empName + "</td></tr><tr><td><b>Assigned To:</b></td><td>" + assignedTo + "<tr><td><b>" + serviceTicketDateText + ":</b></td>\n<td>" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + "</td></tr>\n<tr><td><b>Crt Loc:</b></td><td>" + courtLocation + "</td></tr><tr><td><b>Priority:</b></td><td>" + priority + "</td></tr><tr><td><b>% Comp:</b></td><td>" + percentage + "</td></tr><td><b>Category:</b></td>\n<td>" + category + "</td></tr><tr><td><b>Comments:</b></td><td>" + generalComments + "</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td></tr></table></td>\n</tr><tr><td><center><font size=\"4\"><b>Case Information</b></font></center>" + grid + "</td></tr></table>";

                string test = body;
                #endregion

                //send Open Service Notification Email Email
                MailClass.SendOpenServiceAssign(body, subject, to, "");

            }
        }

        //Created By Zeeshan Ahmed On 1/22/2008
        /// <summary>
        /// Send Service Ticket Notification Email
        /// </summary>
        /// <param name="serviceTicketType">Service Ticket Type</param>
        /// <param name="ticketId">Ticket ID</param>
        /// <param name="fId">FId</param>
        /// <param name="empName">Rep Name</param>
        /// <param name="url">Page URL</param>
        /// <param name="CaseTypeId">Case Type (Division)</param>
        public void SendServiceTicketNotificationEmail(ServiceTicketType serviceTicketType, int ticketId, int fId, string empName, string url, int CaseTypeId)
        {
            //If Service Ticket Information Found
            if (GetServiceTicketInformation(ticketId, fId))
            {
                //Modified by Ozair for Task 2710 on 01/26/2008 [added percentage]
                SendServiceTicketNotificationEmail(serviceTicketType, (ServiceTicketCategories)Convert.ToInt32(category), url, serviceTicketCategoryName, priorityDescription, empName, generalcomments, ticketId, assignedToName, percentage, "", CaseTypeId);
            }

        }

        /// <summary>
        /// Get Case Information ( Client Name , Violations )
        /// </summary>
        /// <returns>Return DataTable Containing Violations</returns>
        public DataTable GetCaseInformation()
        {
            string[] key ={ "@Ticketid" };
            object[] value ={ ticketid };
            DataTable violations = ClsDb.GetDTBySPArr("usp_hts_get_ServiceTicketEmail", key, value);
            return violations;
        }


        //Sabir Khan 6670 10/05/2009
        /// <summary>
        /// Get associated email address....
        /// </summary>
        /// <returns>email address of the user associated to the selected category</returns>
        public string GetAssociatedEmailAddress(string Category)
        {
            string[] key = { "@assCategory" };
            object[] value = { Category };
            DataTable EmailTo = ClsDb.GetDTBySPArr("USP_HTP_Get_AssociatedEmailAddress", key, value);
            return EmailTo.Rows[0][0].ToString(); 
        }

        // Afaq 8213 11/01/2010 Method Added
        /// <summary>
        /// Return count of given service ticket against a ticketId.
        /// </summary>
        /// <returns>No of Service Tickets</returns>
        public int CheckServiceTicket()
        {
            string[] keys = { "@catId", "@ticketId" };
            object[] values = { Category,ticketid};
            return Convert.ToInt32(ClsDb.ExecuteScalerBySP("Usp_Htp_CheckServiceTicket", keys, values));
        }
        #endregion

    }
}

﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using HTP.Components;

namespace HTP.Components
{
    public class BussinessLogic
    {
        #region Variables

        private int helpdocid = 0;
        private string docname = String.Empty;
        private string busineslogic = string.Empty;
        private string menuid = String.Empty;
        private DateTime uploadeddate;
        private int empid = 0;
        private bool isdeleted = false;
        private int rowscount = 0;
        clsENationWebComponents ClsDb = new clsENationWebComponents("Connection String");
        DataTable dtBusinessLogic = new DataTable();



        #endregion

        #region Properties
        public int HelpDocID
        {
            get
            {
                return helpdocid;
            }
            set
            {
                helpdocid = value;
            }
        }
        public string DocName
        {
            get
            {
                return docname;
            }
            set
            {
                docname = value;
            }
        }
        public string BusinesLogic
        {
            get
            {
                return busineslogic;
            }
            set
            {
                busineslogic = value;
            }
        }
        public string MenuID
        {
            get
            {
                return menuid;
            }
            set
            {
                menuid = value;
            }
        }
        public DateTime UploadedDate
        {
            get
            {
                return uploadeddate;
            }
            set
            {
                uploadeddate = value;
            }
        }
        public int EmpID
        {
            get
            {
                return empid;
            }
            set
            {
                empid = value;
            }
        }
        public int RowsCount
        {
            get
            {
                return rowscount;
            }
            set
            {
                rowscount = value;
            }
        }
        public bool IsDeleted
        {
            get
            {
                return isdeleted;
            }
            set
            {
                isdeleted = value;
            }
        }
        #endregion

        #region Methods

        /// <summary>
        /// Fahad 01/16/2009 5376
        /// This method retrive all the uploaded documents and show in grid.
        /// </summary>
        /// <param name="MenuID"></param>
        /// <returns></returns>
        public DataTable GetUploadedDocument(int MenuID)
        {

            if (MenuID > 0)
            {
                string[] key = { "@MenuID" };
                object[] value = { MenuID };
                dtBusinessLogic = ClsDb.Get_DT_BySPArr("USP_HTP_Get_AllUploadedDocs", key, value);
                return dtBusinessLogic;

            }
            return dtBusinessLogic;

        }
        /// <summary>
        /// Fahad 01/16/2009 5376
        /// This method get the business logic 
        /// </summary>
        /// <param name="MenuID"></param>
        /// <returns></returns>
        public DataTable GetBusinessLogicByMenuID(int MenuID)
        {
            if (MenuID > 0)
            {
                string[] keys = { "@MenuID" };
                object[] values = { MenuID };
                dtBusinessLogic = ClsDb.Get_DT_BySPArr("Usp_Get_BusinessLogicByMenuID", keys, values);
                return dtBusinessLogic;
            }
            return dtBusinessLogic;
        }

        /// <summary>
        /// Fahad 01/16/2009 5376
        /// this method delete the Uploaded Documents on the basis of Menuid and Document Id
        /// </summary>
        /// <param name="MenuID"></param>
        /// <param name="DocID"></param>
        /// <returns></returns>
        public bool DeleteUploadedDocs(int MenuID, int DocID)
        {
            if (MenuID > 0 && DocID > 0)
            {
                string[] key = { "@MenuID", "@DocID" };
                object[] value1 = { MenuID, DocID };
                RowsCount = Convert.ToInt32(ClsDb.ExecuteScalerBySp("USP_HTP_Delete_UploadedDocs", key, value1));
                if (RowsCount >= 0)
                    return true;

            }
            return false;


        }

        /// <summary>
        ///     Fahad 01/16/2009 5376
        ///     This method Upload the document
        ///     Noufil 5376 01/27/2009 Document included 
        /// </summary>
        /// <param name="MenuID"></param>
        /// <param name="DocName"></param>
        /// <param name="EmpID"></param>
        /// <returns></returns>
        public bool UploadNewDocument(int MenuID, string DocName, int EmpID, string SaveDoc)
        {

            if ((MenuID > 0 && DocName.Trim() != " ") && (DocName.Trim() != "" && EmpID > 0) && (SaveDoc.Trim() != "" && SaveDoc != null) && (DocName.Trim() != "" && DocName != null))
            {
                string[] key = { "@SubMenuID_FK", "@DocName", "@EmpID", "@SavedDocName" };
                object[] values = { MenuID, DocName, EmpID, SaveDoc };
                RowsCount = Convert.ToInt32(ClsDb.ExecuteScalerBySp("USP_HTP_Insert_AllUploadedDocs", key, values));
                if (RowsCount >= 0)
                    return true;
                else
                    return false;
            }
            else
            {
                return false;

            }


        }

        /// <summary>
        /// Fahad 01/16/2009 5376
        /// this method Update the BusinessLogic
        /// </summary>
        /// <param name="MenuID"></param>
        /// <param name="busineslogic"></param>
        /// <returns></returns>
        public bool UpdateNewBussinessLogic(int MenuID, string busineslogic)
        {

            if (busineslogic != null && MenuID > 0 && busineslogic.Trim() != "" && busineslogic.Trim() != "")
            {
                string[] key = { "@MenuID", "@BussinessLogic" };
                object[] value1 = { MenuID, busineslogic };
                RowsCount = Convert.ToInt32(ClsDb.ExecuteScalerBySp("USP_HTP_Update_BussinessLogic", key, value1));
                if (RowsCount >= 0)
                    return true;
                else
                    return false;
            }

            else
            {
                return false;
            }

        }

        /// <summary>
        /// Fahad 01/16/2009 5376
        /// this method is getting Accesstype of a user
        /// </summary>
        /// <param name="EmpID"></param>
        /// <returns></returns>
        public int GetAccessType(int EmpID)
        {
            if (EmpID > 0)
            {
                string[] keys = { "@EmpID" };
                object[] values = { EmpID };
                int type = Convert.ToInt32(ClsDb.ExecuteScalerBySp("USP_HTP_Get_UserAccessType", keys, values));
                return type;
            }
            else
            {
                return 0;
            }

        }


        /// <summary>
        ///     // Noufil 5691 04/07/2009 
        ///     This methd is use to Search business logic information 
        /// </summary>
        /// <param name="URL">URL of Page</param>
        /// <param name="Attribute">"Days"</param>
        /// <returns></returns>
        public DataTable GetBusinessLogicInformationByURL(string URL, string Attribute)
        {
            if (URL != null && Attribute != null)
            {
                DataTable dtDays = new DataTable();
                string[] key = { "@Report_Url", "@Attribute_key" };
                object[] value = { URL, Attribute };
                dtDays = ClsDb.Get_DT_BySPArr("usp_htp_get_reportByUrl", key, value);
                if (dtDays != null)
                    return dtDays;
                else
                    throw new Exception("Result set is not in corect format");
            }
            else
                throw new ArgumentNullException();
        }

        /// <summary>
        ///     // Noufil 5691 04/07/2009 
        ///     Get Information about Set business Logic days 
        /// </summary>
        /// <param name="Attribute">"Days"</param>
        /// <param name="ReportID">Report ID in Business Logic Table</param>
        /// <returns></returns>
        public DataTable GetBusinessLogicSetDaysInformation(string Attribute, string ReportID)
        {
            if (ReportID != null && Attribute != null)
            {
                DataTable dtDays = new DataTable();
                string[] key1 = { "@Attribute_Key", "@Report_Id" };
                string[] value1 = { Attribute, ReportID };
                dtDays = ClsDb.Get_DT_BySPArr("usp_hts_get_BusinessLogicDay", key1, value1);
                if (dtDays != null)
                    return dtDays;
                else
                    throw new Exception("Result set is not in corect format");
            }
            else
                throw new ArgumentNullException();
        }
        #endregion

    }
}

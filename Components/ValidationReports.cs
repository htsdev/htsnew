using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;

namespace lntechDallasNew.Components
{

    public enum ValidationReport
    {
        OutSideCourtAppearance,
        Priceadjustment,
        NoCourtLocation,
        BondAlert,
        Filed2714butNeedConfirmation,
        AppealBondFiledbutNeedConfirmation,
        Not2714Filed,
        Appbondnotfiled,

        
       
    }

    public class ValidationReports
    {

        #region Variables

        clsENationWebComponents clsdb = new clsENationWebComponents();
        DataSet records;
        bool hasParameter = false;
        public string[] keys;
        public object[] values;
        
        #endregion
        
        #region Properties

        public bool HasParameter
        {
            get  { return hasParameter; }
            set  { hasParameter = value;}
        }

        #endregion

        public void getReportData(GridView gv_records, Label errormessage, ValidationReport reporttype)
        {

            switch (reporttype)
            {
                case ValidationReport.OutSideCourtAppearance: getRecords("USP_DTS_OUTSIDECOURTAPPEARANCE"); break;
                case ValidationReport.Priceadjustment: getRecords("USP_DTS_PRICEADJUSTMENT"); break;
                case ValidationReport.NoCourtLocation: getRecords("USP_HTS_Get_NoCourtLocation"); break;
                case ValidationReport.BondAlert: getRecords("USP_DTS_BONDREPORT"); break;
                case ValidationReport.Filed2714butNeedConfirmation: getRecords("USP_DTS_jp2714Confirmation"); break;
                case ValidationReport.AppealBondFiledbutNeedConfirmation: getRecords("USP_DTS_AppealConfirmation"); break;
               
                case ValidationReport.Not2714Filed: getRecords("USP_DTS_2714_NotSelected "); break;
                case ValidationReport.Appbondnotfiled: getRecords("USP_DTS_APPEALBONDNOTFILED"); break;
              
               
               
         
                
            }
            displayRecords(gv_records, errormessage);
        }

        private void getRecords(string procedurename)
        {
            try
            {
                records = new DataSet();

                if (HasParameter)
                {
                    records = clsdb.Get_DS_BySPArr(procedurename, keys, values);
                }
                else
                {
                    records = clsdb.Get_DS_BySP(procedurename);
                }
            }
            catch (Exception ex)
            {
                records = new DataSet();
                records.Tables.Add();
            }
        }

        private void displayRecords(GridView gv_records, Label errormessage)
        {
            if (records.Tables[0].Rows.Count > 0)
            {
                errormessage.Text = "";
                generateSerialNo();
                gv_records.DataSource = records.Tables[0];
                gv_records.DataBind();
            }
            else
            {
                errormessage.Text = "No Record Found";
                gv_records.DataSource = records.Tables[0];
                gv_records.DataBind();
            }
        }

        private void generateSerialNo()
        {
            if (records.Tables[0].Columns.Contains("sno") == false)
            {
                records.Tables[0].Columns.Add("sno");
                int sno = 1;
                records.Tables[0].Rows[0]["sno"] = 1;

                for (int i = 1; i < records.Tables[0].Rows.Count; i++)
                {
                    if (records.Tables[0].Rows[i - 1]["ticketid_pk"].ToString() != records.Tables[0].Rows[i]["ticketid_pk"].ToString())
                    {
                           records.Tables[0].Rows[i]["sno"] = ++sno;
                    }

                }

            }

        }


    }
}

using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.Components
{
    /// <summary>
    /// This Class is written by Azwer Alam for No Trial Letter Report...
    /// </summary>
    public class PendingTrialLetter
    {
        clsENationWebComponents ClsDb = new clsENationWebComponents("Connection String");
        clsLogger clog = new clsLogger();
        public DataSet DS=null;
        private object[] _values = null;

        #region Constructor

        public PendingTrialLetter()
        {
            
        }

        public PendingTrialLetter(object[] val)
        {
            this.Value = val;
        }

        #endregion

        #region Properties

        public object[] Value
        {
            get
            {
                return _values;
            }
            set
            {
                if (value == null || value.Length < 2)
                {
                    throw new ArgumentException("Parameter Value may not be null or blank");
                }
                _values=value;
            }
        }

        #endregion 

        #region Methods

        public DataSet GetData()
        {
            try
            {
                DS = ClsDb.Get_DS_BySP("Usp_Hts_Get_NoTrialLetter");
                return DS; 
            }

            catch (Exception ex)
            {
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return DS;  
            }
        }

        public int UpdateFlag()
        {
            int result = 0;
            try
            {
                string[] keys={"@TicketID", "@EmpID" };
                ClsDb.ExecuteSP("Usp_Hts_UpdateNoTrialLetter",keys , this.Value);
                result = 1;
                return result;
            }
            catch (Exception ex)
            {

                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
               return result;
            }
        }

        #endregion 

    }

}

﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using MODI;

namespace lntechNew.Components.ClientInfo
{
    public class NewOCR
    {
        public string OCRImage(string fileToOCR)
        {
            MODI.Document md = new MODI.Document();
            md.Create(fileToOCR);
            md.OCR(MODI.MiLANGUAGES.miLANG_ENGLISH, true, true);
            MODI.Image img = (MODI.Image)md.Images[0];
            MODI.Layout layout = img.Layout;
            layout = img.Layout;
            string result = layout.Text;
            md.Close(false);          
            return result;



        }

    }
}

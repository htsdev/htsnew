using System;
using System.Data;
using FrameWorkEnation.Components;
using System.Configuration;
using MSXML2;
using System.Net;
using System.IO;

namespace lntechNew.Components.ClientInfo
{
    /// <summary>
    /// Summary description for clsCreditCard.
    /// </summary>
    public class clsCreditCard
    {
        //Create an Instance of Data Access Class
        clsENationWebComponents clstran = new clsENationWebComponents(true);
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        clsLogger bugTracker = new clsLogger();
        AppSettingsReader asReader = new AppSettingsReader();
        #region Variables
        int TicketID;
        int ScheduleID;
        int invoiceNumber;
        //		private string fullName=String.Empty;
        //		private string creditCardNo=String.Empty;
        //		private string cardType=String.Empty;
        //		private string cin=String.Empty;
        //		private DateTime expirationDate=DateTime.Now;
        //		private double amount=0;
        //		private string status=String.Empty;
        //		private string transactionNumber=String.Empty;
        //		private string returnCode=String.Empty;
        //		private string errorMessage=String.Empty;
        //		private int empID=0;
        //        private int approvedFlag=0;
        //		private int clearFlag=0;
        //		private string responseSubCode=String.Empty;
        //		private string responseReasonCode=String.Empty;
        //		private string responseReasonText=String.Empty;
        //		private string authorizationCode=String.Empty;
        //		private string avsCode=String.Empty;

        #endregion

        #region Properties

        public int InvoiceNumber
        {
            get
            {
                return invoiceNumber;
            }
            set
            {
                invoiceNumber = value;
            }
        }
        //
        //		public string FullName
        //		{
        //			get
        //			{
        //				return fullName;
        //			}
        //			set
        //			{
        //				fullName = value;
        //			}
        //		}
        //
        //		public string CreditCardNo
        //		{
        //			get
        //			{
        //				return creditCardNo;
        //			}
        //			set
        //			{
        //				creditCardNo = value;
        //			}
        //		}
        //
        //		public string CardType
        //		{
        //			get
        //			{
        //				return cardType;
        //			}
        //			set
        //			{
        //				cardType = value;
        //			}
        //		}
        //
        //		public string CIN
        //		{
        //			get
        //			{
        //				return cin;
        //			}
        //			set
        //			{
        //				cin = value;
        //			}
        //		}
        //
        //		public DateTime ExpirationDate
        //		{
        //			get
        //			{
        //				return expirationDate;
        //			}
        //			set
        //			{
        //				expirationDate = value;
        //			}
        //		}
        //
        //		public double Amount
        //		{
        //			get
        //			{
        //				return amount;
        //			}
        //			set
        //			{
        //				amount = value;
        //			}
        //		}
        //
        //		public string Status
        //		{
        //			get
        //			{
        //				return status;
        //			}
        //			set
        //			{
        //				status = value;
        //			}
        //		}
        //
        //		public string TransactionNumber
        //		{
        //			get
        //			{
        //				return transactionNumber;
        //			}
        //			set
        //			{
        //				transactionNumber = value;
        //			}
        //		}
        //
        //		public string ReturnCode
        //		{
        //			get
        //			{
        //				return returnCode;
        //			}
        //			set
        //			{
        //				returnCode = value;
        //			}
        //		}
        //
        //		public string ErrorMessage
        //		{
        //			get
        //			{
        //				return errorMessage;
        //			}
        //			set
        //			{
        //				errorMessage = value;
        //			}
        //		}
        //
        //		public int EmpID
        //		{
        //			get
        //			{
        //				return empID;
        //			}
        //			set
        //			{
        //				empID = value;
        //			}
        //		}
        //
        //		public int ApprovedFlag
        //		{
        //			get
        //			{
        //				return approvedFlag;
        //			}
        //			set
        //			{
        //				approvedFlag = value;
        //			}
        //		}
        //
        //		public int ClearFlag
        //		{
        //			get
        //			{
        //				return clearFlag;
        //			}
        //			set
        //			{
        //				clearFlag = value;
        //			}
        //		}
        //
        //		public string ResponseSubCode
        //		{
        //			get
        //			{
        //				return responseSubCode;
        //			}
        //			set
        //			{
        //				responseSubCode = value;
        //			}
        //		}
        //
        //		public string ResponseReasonCode
        //		{
        //			get
        //			{
        //				return responseReasonCode;
        //			}
        //			set
        //			{
        //				responseReasonCode = value;
        //			}
        //		}
        //
        //		public string ResponseReasonText
        //		{
        //			get
        //			{
        //				return responseReasonText;
        //			}
        //			set
        //			{
        //				responseReasonText = value;
        //			}
        //		}
        //
        //		public string AuthorizationCode
        //		{
        //			get
        //			{
        //				return authorizationCode;
        //			}
        //			set
        //			{
        //				authorizationCode = value;
        //			}
        //		}
        //
        //		public string AVSCode
        //		{
        //			get
        //			{
        //				return avsCode;
        //			}
        //			set
        //			{
        //				avsCode = value;
        //			}
        //		}

        #endregion

        #region Methods

        /// <summary>
        /// Method To Authenticate & Process Credit Card
        /// </summary>
        /// <param name="InvoiceNum">Payment Invoice num</param>
        /// <param name="Scheduleid">If schedule payment then its ID esle ID:0 </param>
        /// <param name="cardnum">Credit card number</param>
        /// <param name="processflag">0:if schedule payment selected or schedule count=0  else 1 </param>
        public void ProcessCreditCard(int InvoiceNum, int Scheduleid, string cardnum, int processflag)
        {

            string PostData = "";
            //PostData = "x_Login=ant481205930";        //test account for Authorize.Net
            PostData = "x_Login=" + asReader.GetValue("Login", typeof(string)).ToString();
            //PostData += "&x_tran_key=83gSdU2xvkXBTwtg";
            PostData += "&x_tran_key=" + asReader.GetValue("Tran_Key", typeof(string)).ToString();
            PostData += "&x_Version=3.0";
            //PostData += "&x_Test_Request=False";  //   ' since we are testing
            //PostData += "&x_Test_Request=True";  //   'True since we are testing
            PostData += "&x_Test_Request=" + asReader.GetValue("Test_Request", typeof(string)).ToString();
            PostData += "&x_ADC_Delim_Data=TRUE";  //'we want comma delimited
            PostData += "&x_ADC_URL=FALSE";  // 'do not want to redirect
            //Now add the form fields

            //Getting Info of client credit card for processing by invoice number
            string[] key1 ={ "@invnumber" };
            object[] value1 ={ InvoiceNum };
            DataSet ds_cardinfo = ClsDb.Get_DS_BySPArr("USP_HTS_GET_CREDITCARD_BY_INVOICENUM", key1, value1);
            TicketID = Convert.ToInt32(ds_cardinfo.Tables[0].Rows[0]["ticketid"]);

            //PostData += "&x_invoice_num="+ds_cardinfo.Tables[0].Rows[0]["invnumber"].ToString();		
            PostData += "&x_invoice_num=" + InvoiceNum;
            PostData += "&x_Card_Num=" + cardnum;
            PostData += "&x_Exp_Date=" + ds_cardinfo.Tables[0].Rows[0]["expdate"].ToString();
            PostData += "&x_amount   =" + ds_cardinfo.Tables[0].Rows[0]["amount"].ToString();
            PostData += "&x_card_code=" + ds_cardinfo.Tables[0].Rows[0]["cin"].ToString();

            PostData += "&x_Description=Traffic Tickets";
            PostData += "&x_First_Name=" + ds_cardinfo.Tables[0].Rows[0]["firstname"].ToString();
            PostData += "&x_Last_Name =" + ds_cardinfo.Tables[0].Rows[0]["lastname"].ToString();
            PostData += "&x_Address    =" + ds_cardinfo.Tables[0].Rows[0]["Address"].ToString();
            PostData += "&x_City      =" + ds_cardinfo.Tables[0].Rows[0]["city"].ToString();
            PostData += "&x_State     =" + ds_cardinfo.Tables[0].Rows[0]["state"].ToString();
            PostData += "&x_ZIP        =" + ds_cardinfo.Tables[0].Rows[0]["zip"].ToString();

            ////
            //string connection="https://secure.authorize.net/gateway/transact.dll";
            string connection = asReader.GetValue("ConnectionURL", typeof(string)).ToString();

            char[] sep = { ',' };
            //XMLHTTP objHTTP = new XMLHTTP();
            ////ServerXMLHTTP objHTTP = new ServerXMLHTTP();
            //objHTTP.open("POST", connection, false, null, null);
            //objHTTP.send(PostData);
            ////string response =objHTTP.responseText;

            //if (objHTTP.status == 200)
            //{
            //    string res = objHTTP.responseText;

            //    string[] alag = res.Split(sep);
            //    ScheduleID = Scheduleid;
            //    UpdateCreditCardTransaction(alag, processflag);
            //}
            //else
            //{
            //    throw new Exception("Cannot Retrieve Response From Server");
            //}

            StreamWriter myWriter = null;
            string[] alag;

            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(connection);
            objRequest.Method = "POST";
            objRequest.ContentLength = PostData.Length;
            objRequest.ContentType = "application/x-www-form-urlencoded";

            try
            {
                myWriter = new StreamWriter(objRequest.GetRequestStream());
                myWriter.Write(PostData);

                HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
                using (StreamReader sr = new StreamReader(objResponse.GetResponseStream()))
                {
                    string res = sr.ReadToEnd();
                    alag = res.Split(sep);

                    sr.Close();
                }
                ScheduleID = Scheduleid;
                UpdateCreditCardTransaction(alag, processflag);
            }
            catch (Exception e)
            {
                throw new ApplicationException("Cannot Retrieve Response From Server - " + e.Message);
            }
            finally
            {
                myWriter.Close();
            }

        }
        /// <summary>
        /// Update credit card table and payments table	
        /// </summary>							
        private void UpdateCreditCardTransaction(string[] array, int processflag)
        {
            try
            {
                string x_response_code = array[0];
                //Response Code:
                //1 = This transaction has been approved.
                //2 = This transaction has been declined.
                //3 = There has been an error processing this transaction.

                string x_response_subcode = array[1];
                string x_response_reason_code = array[2];
                string x_response_reason_text = array[3];
                string x_auth_code = array[4];   //6 digit approval code
                string x_avs_code = array[5];
                /*
                    'Address Verification system:
                'A = Address (street) matches, Zip does not
                'E = AVS error
                'N = No match on address or zip
                'P = AVS Not Applicable
                'R = Retry, system unavailable or timed out
                'S = service not supported by issuer
                'U = address information is unavailable
                'W = 9 digit Zip matches, address does not
                'X = exact AVS match
                'Y = address and 5 digit zip match
                'Z = 5 digit zip matches, address does not
                */

                string x_trans_id = array[6];  //  'transaction id
                string x_invoice_num = array[7];
                string x_description = array[8];
                string x_amount = array[9];
                string x_method = array[10];
                string x_type = array[11];
                string x_cust_id = array[12];
                string x_first_name = array[13];
                string x_last_name = array[14];
                string x_company = array[15];
                string x_address = array[16];
                string x_city = array[17];
                string x_state = array[18];
                string x_zip = array[19];
                string x_country = array[20];
                string x_phone = array[21];
                string x_fax = array[22];
                string x_email = array[23];
                string x_ship_to_first_name = array[24];
                string x_ship_to_last_name = array[25];
                string x_ship_to_company = array[26];
                string x_ship_to_address = array[27];
                string x_ship_to_city = array[28];
                string x_ship_to_state = array[29];
                string x_ship_to_zip = array[30];
                string x_ship_to_country = array[31];
                string x_tax = array[32];
                string x_duty = array[33];
                string x_freight = array[34];
                string x_tax_exempt = array[35];
                string x_po_num = array[36];
                string x_md5_hash = array[37];


                if (x_response_code == "1")
                {
                    clstran.DBBeginTransaction();
                    string[] key1 ={ "@ticketid", "@invnumber", "@status", "@errmsg", "@transnum", "@response_subcode", "@response_reason_code", "@response_reason_text", "@auth_code", "@avs_code", "@scheduleid", "@processflag" };
                    object[] value1 ={ TicketID, x_invoice_num, "0", "", x_trans_id, x_response_subcode, x_response_reason_code, x_response_reason_text, x_auth_code, x_avs_code, ScheduleID, processflag };
                    clstran.ExecuteSP("USP_HTS_UPDATE_CREDITCARD_INFO", key1, value1, 1);
                    clstran.DBTransactionCommit();

                }
                else if (x_response_code == "2")
                {
                    clstran.DBBeginTransaction();
                    string[] key1 ={ "@ticketid", "@invnumber", "@status", "@errmsg", "@transnum", "@response_subcode", "@response_reason_code", "@response_reason_text", "@auth_code", "@avs_code", "@scheduleid", "@processflag" };
                    object[] value1 ={ TicketID, x_invoice_num, "", "", x_trans_id, x_response_subcode, x_response_reason_code, x_response_reason_text, x_auth_code, x_avs_code, ScheduleID, processflag };
                    clstran.ExecuteSP("USP_HTS_UPDATE_CREDITCARD_INFO", key1, value1, 1);
                    clstran.DBTransactionCommit();
                    throw new Exception("The transaction was not approved: " + x_response_reason_text);
                }
                else
                {
                    clstran.DBBeginTransaction();
                    string[] key1 ={ "@ticketid", "@invnumber", "@status", "@errmsg", "@transnum", "@response_subcode", "@response_reason_code", "@response_reason_text", "@auth_code", "@avs_code", "@scheduleid", "@processflag" };
                    object[] value1 ={ TicketID, x_invoice_num, "", "", x_trans_id, x_response_subcode, x_response_reason_code, x_response_reason_text, x_auth_code, x_avs_code, ScheduleID, processflag };
                    clstran.ExecuteSP("USP_HTS_UPDATE_CREDITCARD_INFO", key1, value1, 1);
                    clstran.DBTransactionCommit();
                    throw new Exception("An error occured during processing. " + x_response_reason_text);
                }

            }
            catch (Exception ex)
            {
                if (ex.Message.StartsWith("An error occured during processing.") || ex.Message.StartsWith("The transaction was not approved:"))
                {
                    throw new Exception(ex.Message);
                }
                else
                {
                    clstran.DBTransactionRollback();
                    bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

                }
            }
        }

        /// <summary>
        /// Voids Credit card transaction
        /// </summary>

        public void VoidCreditCardTransaction(int invnum, int empid)
        {
            string[] key1 ={ "@invnumber" };
            object[] value1 ={ invnum };
            DataSet ds_voidcardinfo = ClsDb.Get_DS_BySPArr("USP_HTS_GET_CREDITCARD_BY_INVOICENUM_VOID", key1, value1);
            TicketID = Convert.ToInt32(ds_voidcardinfo.Tables[0].Rows[0]["ticketid"]);
            InvoiceNumber = invnum;

            string PostData = "";
            //PostData = "x_Login=ant481205930";        //test account for Authorize.Net
            PostData = "x_Login=" + asReader.GetValue("Login", typeof(string)).ToString();
            //PostData += "&x_tran_key=83gSdU2xvkXBTwtg";
            PostData += "&x_tran_key=" + asReader.GetValue("Tran_Key", typeof(string)).ToString();
            PostData += "&x_Version=3.0";
            //PostData += "&x_Test_Request=False";  //   'since we are not testing
            //PostData += "&x_Test_Request=True";  //   'since we are testing
            PostData += "&x_Test_Request=" + asReader.GetValue("Test_Request", typeof(string)).ToString();
            PostData += "&x_ADC_Delim_Data=TRUE";  //'we want comma delimited
            PostData += "&x_ADC_URL=FALSE";  // 'do not want to redirect
            PostData += "&x_TYPE=VOID";
            PostData += "&x_TRANS_ID=" + ds_voidcardinfo.Tables[0].Rows[0]["transnum"].ToString();

            //string connection="https://secure.authorize.net/gateway/transact.dll";
            string connection = asReader.GetValue("ConnectionURL", typeof(string)).ToString();

            char[] sep = { ',' };
            //XMLHTTP objHTTP = new XMLHTTP();
            //objHTTP.open("POST", connection, false, null, null);
            //objHTTP.send(PostData);
            ////string response =objHTTP.responseText;

            //if (objHTTP.status == 200)
            //{
            //    string res = objHTTP.responseText;

            //    string[] alag = res.Split(sep);
            //    UpdateCreditCardTransaction_Void(alag, empid);
            //}
            //else
            //{
            //    throw new Exception("Cannot Retrieve Response From Server");
            //}

            StreamWriter myWriter = null;
            string[] alag;

            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(connection);
            objRequest.Method = "POST";
            objRequest.ContentLength = PostData.Length;
            objRequest.ContentType = "application/x-www-form-urlencoded";

            try
            {
                myWriter = new StreamWriter(objRequest.GetRequestStream());
                myWriter.Write(PostData);

                HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
                using (StreamReader sr = new StreamReader(objResponse.GetResponseStream()))
                {
                    string res = sr.ReadToEnd();
                    alag = res.Split(sep);

                    sr.Close();
                }
                UpdateCreditCardTransaction_Void(alag, empid);
            }
            catch (Exception e)
            {
                throw new ApplicationException("Cannot Retrieve Response From Server - " + e.Message);
            }
            finally
            {
                myWriter.Close();
            }


        }

        private void UpdateCreditCardTransaction_Void(string[] array, int empid)
        {

            string x_response_code = array[0];
            //Response Code:
            //1 = This transaction has been approved.
            //2 = This transaction has been declined.
            //3 = There has been an error processing this transaction.

            string x_response_subcode = array[1];
            string x_response_reason_code = array[2];
            string x_response_reason_text = array[3];
            string x_auth_code = array[4];   //6 digit approval code
            string x_avs_code = array[5];
            /*
                'Address Verification system:
            'A = Address (street) matches, Zip does not
            'E = AVS error
            'N = No match on address or zip
            'P = AVS Not Applicable
            'R = Retry, system unavailable or timed out
            'S = service not supported by issuer
            'U = address information is unavailable
            'W = 9 digit Zip matches, address does not
            'X = exact AVS match
            'Y = address and 5 digit zip match
            'Z = 5 digit zip matches, address does not
            */

            string x_trans_id = array[6];  //  'transaction id
            /*
            string x_invoice_num             = array[7];
            string x_description             = array[8];
            string x_amount                  = array[9];
            string x_method                  = array[10];
            string x_type                    = array[11];
            string x_cust_id                 = array[12];
            string x_first_name              = array[13];
            string x_last_name               = array[14];
            string x_company                 = array[15];
            string x_address                 = array[16];
            string x_city                    = array[17];
            string x_state                   = array[18];
            string x_zip                     = array[19];
            string x_country                 = array[20];
            string x_phone                   = array[21];
            string x_fax                     = array[22];
            string x_email                   = array[23];
            string x_ship_to_first_name      = array[24];
            string x_ship_to_last_name       = array[25];
            string x_ship_to_company         = array[26];
            string x_ship_to_address         = array[27];
            string x_ship_to_city            = array[28];
            string x_ship_to_state           = array[29];
            string x_ship_to_zip             = array[30];
            string x_ship_to_country         = array[31];
            string x_tax                     = array[32];
            string x_duty                    = array[33];
            string x_freight                 = array[34];
            string x_tax_exempt              = array[35];
            string x_po_num                  = array[36];
            string x_md5_hash                = array[37];
            */

            if (x_response_code == "1")
            {
                string[] key1 ={ "@employeeid", "@invoicenumber", "@status", "@errmsg", "@transnum", "@response_subcode", "@response_reason_code", "@response_reason_text", "@auth_code", "@avs_code" };
                object[] value1 ={ empid, InvoiceNumber, "0", "", x_trans_id, x_response_subcode, x_response_reason_code, x_response_reason_text, x_auth_code, x_avs_code };
                ClsDb.ExecuteSP("USP_HTS_UPDATE_CREDITCARD_INFO_FOR_VOID", key1, value1);
            }
            else if (x_response_code == "2")
            {
                throw new Exception("The transaction was not approved: " + x_response_reason_text);
            }
            else
            {
                throw new Exception("An error occured during processing. " + x_response_reason_text);
            }
        }
        public DataTable GetManualCCDetail()
        {
            DataTable dt = ClsDb.Get_DT_BySPArr("sp_Get_TestingData");
            return dt;
        }



        #endregion

        public clsCreditCard()
        {
            //
            // TODO: Add constructor logic here
            //
        }
    }
}

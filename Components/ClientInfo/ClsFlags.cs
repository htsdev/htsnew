using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
//updated by fahad
namespace lntechNew.Components.ClientInfo
{
    // Noufil 3498 04/05/2008 setting wrong number enum
    // Agha 4004 12/05/2008 setting Bad email enum
    // Agha 4347 07/21/2008 setting Problem client flag
    //ozair 4846 11/14/2008 renamed ProblemClient_Becareful to ProblemClient_AllowHire
    public enum FlagType
    {
        WrongNumber = 33,
        bademail = 34,
        SOL = 35,
        NotOnSystem = 15,
        //Sabir Khan 5442 02/10/2009 NoLetter Flag added...
        NoLetter = 7,
        ProblemClient_AllowHire = 36,
        ProblemClient_NoHire = 13,
        //Fahad 6054 07/23/2009 OutsideFirmClient Flag added...
        OutsideFirmClient=8,
        //Yasir Kamal 6480 08/28/2009 AllowFaxingLor flag added.
        AllowFaxingLOR = 39,
        //Sabir Khan 8623 12/13/2010 Pre Trial Diversion and Motion Hiring flag added.
        Pre_Trial_Diversion = 42,
        Motion_Hiring = 43,
        //Sabir Khan 8862 02/15/2011 JUVNILE Flag...
        JUVENILE = 44,

        //Sabir Khan 11509 11/05/2013 Return Court Notice flag added.
        ReturnedCourtNotice = 46,
        //Sabir Khan 11509 11/05/2013 Do Not Bond flag added.
        DoNotBond = 47,
        //Sabir Khan 11509 11/05/2013 Bond Forfeiture flag added.
        BondForfeiture = 48


    };
    public class ClsFlags
    {

        ServiceTicketComponent service = new ServiceTicketComponent();
        clsENationWebComponents clsdb = new clsENationWebComponents();
        clsLogger bugTracker = new clsLogger();
        int ticketid = 0;
        int flagid = 0;
        int empid = 0;
        int serviceticketcategory = 0;
        int priority = 0;
        private int trialdocket = 0;
        int AssignTo;
        private int firmId = 0;
        DateTime followupdate = new DateTime(1900, 01, 01);
        string _Description = string.Empty;

        public int TicketID
        {
            set
            {
                ticketid = value;
            }
            get
            {
                return ticketid;
            }

        }

        public DateTime Followupdt
        {
            set
            {
                followupdate = value;
            }
            get
            {
                return followupdate;
            }

        }

        public int Assign
        {
            set
            {
                AssignTo = value;
            }
            get
            {
                return AssignTo;
            }

        }

        public int TrialDocket
        {
            set
            {
                trialdocket = value;
            }
            get
            {
                return trialdocket;
            }
        }

        public int FlagID
        {
            set
            {
                flagid = value;
                // tahir 4777 09/10/2008 getting flag description....
                this._Description =  GetFlagDescription( (FlagType)flagid);
            }
        }
        public int EmpID
        {
            set
            {
                empid = value;
            }
            get
            {
                return empid;
            }
        }
        public int ServiceTicketCategory
        {
            set
            {
                serviceticketcategory = value;
            }
        }
        public int Priority
        {
            set
            {
                priority = value;
            }
        }

        public int FirmId
        {
            get { return this.firmId; }
            set { this.firmId = value; }
        }

        public string Description
        {
            get
            {
                return _Description;
            }
        }

        public bool AddNewFlag()
        {
            try
            {
                string[] keys = { "@TicketID", "@FlagID", "@EmpID", "@ServiceTicketCategory", "@Priority", "@ShowOnTrialDocket" };
                object[] values = { ticketid, flagid, empid, serviceticketcategory, priority, Convert.ToInt32(TrialDocket) };
                clsdb.ExecuteSP("USP_HTS_Insert_flag_by_ticketnumber_and_flagID", keys, values);
                return true;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return false;
            }
        }

        public void FillServiceTicketcategories(DropDownList ddl_category)
        {
            try
            {
                DataTable dt = service.GetCategories();
                ddl_category.DataSource = dt;
                ddl_category.DataTextField = "Description";
                ddl_category.DataValueField = "ID";
                ddl_category.DataBind();
                ddl_category.Items.Insert(0, new ListItem("--Choose--", "0"));
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        // Noufil 3498 04/05/2008 check whether flag exists or not
        /// <summary>This function Checks weather the flag already exist or not</summary>
        /// <returns> return true or false if count exist or not in database </returns>
        /// <example>
        /// <code>
        /// //Create Database Connection
        /// clsENationWebComponents ClsDb = new clsENationWebComponents();
        /// //Execute Stored Procedure
        /// clsdb.ExecuteScalerBySp("USP_HTP_Get_FlagCount_By_TicketNumber_And_FlagID", keys, values);
        /// if object count>0 return true
        /// else false
        /// </code>
        /// </example>
        /// 
        public Boolean HasFlag()
        {

            string[] keys = { "@TicketID", "@FlagID" };
            object[] values = { this.TicketID, this.flagid };
            object count = clsdb.ExecuteScalerBySp("USP_HTP_Get_FlagCount_By_TicketNumber_And_FlagID", keys, values);
            return (Convert.ToInt32(count) > 0 ? true : false);

        }

        //Sabir Khan 5260 11/27/2008 Check existance of not on system flag...
        /// <summary>
        /// This function is used to return number of count of not on system by passing ticketid and flag id
        /// </summary>
        /// <param name="ticketid">Ticket Id</param>
        /// <returns>Total count of record</returns>
        public Boolean HasFlag(int flagid)
        {
            string[] keys = { "@TicketID", "@FlagID" };
            object[] values = { this.ticketid, flagid };
            object count = clsdb.ExecuteScalerBySp("USP_HTP_Get_FlagCount_By_TicketNumber_And_FlagID", keys, values);
            return (Convert.ToInt32(count) > 0 ? true : false);
        }

        // Rab Nawaz Khan 01/13/2015 Added to improve the page performance by Calling the all the Flages once instead of separatly. . .
        public DataTable GetFlages(int ticketId)
        {
            string[] keys = { "@TicketID" };
            object[] values = { ticketId };
            return clsdb.Get_DT_BySPArr("USP_HTP_Get_FlagCount_By_TicketNumber_And_FlagID", keys, values);
        }

        // Noufil 4215 06/28/2008 Get flag id
        /// <summary>
        /// This procedure returns the flagid of the input flag
        /// </summary>
        /// <returns>Flagid</returns>
        /// <code>
        /// clsENationWebComponents ClsDb = new clsENationWebComponents();
        /// //Execute Stored Procedure
        /// clsdb.ExecuteScalerBySp("USP_HTS_Getflagid_byticketnumber", keys, values);
        /// return comse value
        /// </code>
        public int GetFlagId(int ticketId, FlagType flagType)
        {
            string[] keys = { "@ticketid", "@flagid" };
            object[] values = { this.TicketID, Convert.ToInt32(flagType) };
            return Convert.ToInt32(clsdb.ExecuteScalerBySp("USP_HTP_Getflagid_byticketnumber", keys, values));
        }

        // Tahir 4777  09/10/2008 Get flag description by Id
        /// <summary>
        /// This procedure returns description of the flag
        /// </summary>
        /// <param name="flagType">Flag Type</param>
        /// <returns>Description</returns>
        public string GetFlagDescription(FlagType flagType)
        {
            string[] keys = { "@flagid" };
            object[] values = { Convert.ToInt32(flagType) };
            return Convert.ToString(clsdb.ExecuteScalerBySp("USP_HTP_Get_FlagDescription", keys, values));
        }

        /// <summary>
        /// Delete Flag by Fid
        /// </summary>
        /// <param name="ticketId">Case Number</param>
        /// <param name="flagType">Flag Type</param>
        /// <param name="empId">EmpId</param>
        /// <param name="fid">Fid</param>
        /// <returns>Return true if procedure executed successfully</returns>
        public bool DeleteFlagbyFId(int ticketId, FlagType flagType, int empId, int fid)
        {
            string[] key12 = { "@TicketID", "flagid", "empid", "@fid" };
            object[] value12 = { ticketId, Convert.ToInt32(flagType), empId, fid };
            clsdb.ExecuteSP("USP_HTS_Delete_EventFlag_by_Ticketnumber_and_FlagID", key12, value12);
            return true;
        }

        /// <summary>
        /// Delete Flag by Fid
        /// </summary>
        /// <param name="ticketId">Case Number</param>
        /// <param name="flagType">Flag Type</param>
        /// <param name="empId">EmpId</param>
        /// <returns>Return true if procedure executed successfully</returns>
        public bool DeleteFlagbyFId(int ticketId, FlagType flagType, int empid)
        {
            return DeleteFlagbyFId(ticketId, flagType, empid, GetFlagId(ticketId, flagType));
        }

    }

}

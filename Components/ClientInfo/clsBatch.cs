using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
//TODO:Use New Namespace
using FrameWorkEnation.Components;
using lntechNew.Components;
using System.Collections;
using MSXML2;
using System.Data.SqlClient;



namespace HTP.Components
{
    //Zeeshan Ahmed 3486 04/04/2008

    /// <summary>
    /// Represent Batch Letter Type
    /// </summary>
    /// 
    public enum BatchLetterType
    {
        MissedCourtLetter = 11,
        PledOutLetter = 12,
        TrialNotificationLetter = 2,
        //Noufil 3498 04/17/2008 for letter type for set call 
        SetCallLetter = 13,
        Edelivery = 14,
        //noufil 4215 06/16/2008 SOL flag added
        SOL = 15,
        // Abid Ali 5359 12/30/2008 LOR flag added
        LOR = 6,
        // Haris Ahmed 10381 08/31/2012 Immigration Letter
        ImmigrationLetter = 34

    }


    /// <summary>
    /// Class Represent Batch Report
    /// </summary>
    public class BatchReport
    {
        public string ReportName = "";
        public string ReportProcedure = "";

        public BatchReport(string ReportName, string ReportProcedure)
        {
            this.ReportName = ReportName;
            this.ReportProcedure = ReportProcedure;
        }
    }

    /// <summary>
    /// This class contains all the logic related to batch print.
    /// </summary>
    public class clsBatch
    {
        clsENationWebComponents clsDB = new clsENationWebComponents();
        clsCrsytalComponent batchRpt = new clsCrsytalComponent();

        /// <summary>
        /// Get Letters in the batch
        /// </summary>
        /// <param name="batchLetterType">Batch Letter Type</param>
        /// <param name="empId">Employee ID</param>
        /// <param name="courtId">Court Location</param>
        /// <param name="isPrinted">Is Printed Document</param>
        /// <param name="fromDate">Printed From Date </param>
        /// <param name="toDate">Printed To Date</param>
        /// <returns></returns>
        public DataSet getBatchbyLetterType(BatchLetterType batchLetterType, string empId, int courtId, bool isPrinted, DateTime fromDate, DateTime toDate, int bond)
        {

            string[] key1 = { "@LType", "@emp_id", "CourtId", "@isprinted", "@fromdate", "@todate", "@bondflag" };
            object[] Value1 = { Convert.ToInt32(batchLetterType), empId, courtId, Convert.ToInt32(isPrinted), fromDate, toDate, bond };
            return clsDB.Get_DS_BySPArr("USP_HTS_GET_BatchTicketID_New_v1", key1, Value1);
        }

        //Nasir 5681 05/15/2009 for getting count only
        /// <summary>
        /// Get Count of all letters
        /// </summary>
        /// <param name="isPrinted"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        public DataSet getBatchPrintCount(bool isPrinted, DateTime StartDate, DateTime EndDate, string empId, int courtId)
        {

            string[] key1 = { "@IsPrinted", "@StartDate", "@EndDate", "@court", "@EMPID" };
            object[] Value1 = { Convert.ToInt32(isPrinted), StartDate, EndDate, courtId, empId };
            return clsDB.Get_DS_BySPArr("USP_HTP_Get_BatchPrint_Count", key1, Value1);
        }

        /// <summary>
        /// Delete Letter from the batch.
        /// </summary>
        /// <param name="batchLetterType">Batch Letter Type</param>
        /// <param name="batchIdList">Batch ID List</param>
        /// <returns>Return true if batch deleted successfully</returns>
        public bool deleteLetterFromBatch(BatchLetterType batchLetterType, string batchIdList)
        {
            string[] Key2 = { "@LetterID", "@BatchIDList" };
            object[] value2 = { Convert.ToInt32(batchLetterType), batchIdList };
            clsDB.ExecuteSP("USP_HTS_Delete_LetterBatch_New", Key2, value2);
            return true;
        }

        /// <summary>
        /// Delete Letter from Batch print against Ticket Id
        /// </summary>
        /// <param name="batchLetterType"></param>
        /// <param name="batchIdList"></param>
        /// <returns></returns>
        public bool deleteLetterFromBatch(BatchLetterType batchLetterType, int TicketId)
        {
            //Agha Usman 4165 06/03/2008
            string[] Key2 = { "@LetterID", "@TicketId" };
            object[] value2 = { Convert.ToInt32(batchLetterType), TicketId };
            clsDB.ExecuteSP("USP_HTP_Delete_LetterTicket", Key2, value2);
            return true;
        }

        /// <summary>
        /// Delete Printed Letter From the batch.
        /// </summary>
        /// <param name="batchLetterType">Batch Letter Type</param>
        /// <returns>Return true if batch deleted successfully</returns>
        public bool deletePrintedLetterFromBatch(BatchLetterType batchLetterType)
        {
            String[] Key2 = { "@LetterID" };
            object[] value2 = { Convert.ToInt32(batchLetterType) };
            clsDB.ExecuteSP("USP_HTS_Batch_Delete_AlreadyPrinted", Key2, value2);
            return true;
        }

        /// <summary>
        /// This Procedure Email Batch Report
        /// </summary>
        /// <param name="batchLetterType">Batch Report Type</param>
        /// <param name="Ticketlist">Case Numbers</param>
        /// <returns>Return true if email send successfully</returns>
        public DataTable emailBatchReport(BatchLetterType batchLetterType, string Ticketlist)
        {

            // Agha Usman - 3886 - 05/12/2008
            string[] Key2 = { "@Ticketid_pk" };
            object[] value2 = { Ticketlist };

            DataTable dt = new DataTable();

            switch (batchLetterType)
            {
                case BatchLetterType.TrialNotificationLetter:
                    dt = clsDB.Get_DT_BySPArr("USP_HTS_Send_EmailTrialLetter_BatchReport", Key2, value2);
                    break;
                default:
                    break;
            }
            return dt;
        }

        /// <summary>
        /// This Function Get Batch Report Name and Procedure
        /// </summary>
        /// <param name="batchLetterType">Batch Letter Type</param>
        /// <returns>Return BatchReport containing Report and Procedure Name</returns>
        public BatchReport getReportName(BatchLetterType batchLetterType)
        {
            String[] keys = { "@LetterID" };
            object[] values = { Convert.ToInt32(batchLetterType) };
            DataSet ds = clsDB.Get_DS_BySPArr("USP_HTS_GET_BatchLetterInfo", keys, values);

            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                return new BatchReport(ds.Tables[0].Rows[0]["ReportName"].ToString(), ds.Tables[0].Rows[0]["StoredProc"].ToString());

            return new BatchReport("", "");
        }

        //Yasir Kamal 7590 03/29/2010 trial letter modifications
        /// <summary>
        /// This Method Update Print Information About The Batch
        /// </summary>
        /// <param name="batchLetterType">Batch Letter Type</param>
        /// <param name="TicketID">Case Number</param>
        /// <param name="EmpID">Employee ID</param>
        /// <param name="TicketIDBatch">Batch ID of Case</param>
        /// <param name="Datetime">Current Date Time</param>
        /// <param name="ReportName">Name of Report</param>
        /// <returns>Docpath</returns>
        public string updateBatchPrintInformation(BatchLetterType batchLetterType, string TicketID, int EmpID, string TicketIDBatch, string Datetime, string ReportName)
        {
            string Docpath = String.Empty;
            int outputSize = 120;

            String[] Key2 = { "@LetterID", "@ticketidlist", "@PrintEmpId", "@TicketIDBatch", "@Datetime", "@ReportName", "@Docpath" };
            object[] value2 = { Convert.ToInt32(batchLetterType), TicketID, EmpID, TicketIDBatch, Datetime, ReportName, "" };
            Docpath = Convert.ToString(clsDB.InsertBySPArrRetOutputSize("USP_HTS_UPDATE_LetterBatch", Key2, value2, outputSize));
            return Docpath;
        }

        /// <summary>
        /// This Method Update Print Information About The Batch
        /// </summary>
        /// <param name="batchLetterType">Batch Letter Type</param>
        /// <param name="TicketID">Case Number</param>
        /// <param name="EmpID">Employee ID</param>
        /// <param name="TicketIDBatch">Batch ID of Case</param>
        /// <returns>Return true if update operation performed successfully</returns>
        public bool updateBatchPrintInformation(BatchLetterType batchLetterType, string TicketID, int EmpID, string TicketIDBatch)
        {
            String[] Key2 = { "@LetterID", "@ticketidlist", "@PrintEmpId", "@TicketIDBatch" };
            object[] value2 = { Convert.ToInt32(batchLetterType), TicketID, EmpID, TicketIDBatch };
            clsDB.ExecuteSP("USP_HTS_UPDATE_LetterBatch", Key2, value2);
            return true;


        }
        //
        /// <summary>
        /// Fahad 5071 11/24/2008
        /// this method is used to update the isemail flag
        /// </summary>
        /// <param name="batchLetterType"></param>
        /// <param name="TicketID"></param>
        /// <param name="TicketIDBatch"></param>
        /// <returns></returns>
        public bool UpdateBatchPrintEmail(BatchLetterType batchLetterType, string TicketID, string TicketIDBatch)
        {
            String[] Key2 = { "@LetterID", "@ticketidlist", "@TicketIDBatch" };
            object[] value2 = { Convert.ToInt32(batchLetterType), TicketID, TicketIDBatch };
            clsDB.ExecuteSP("USP_HTP_Update_BatchPrintEmail", Key2, value2);
            return true;
        }



        /// <summary>
        ///This Function Send Missed Court Cases To Batch
        /// </summary>
        /// <param name="TicketIds">Case Numbers</param>
        /// <param name="batchLetterType">Batch Letter Type</param>
        public void SendLetterToBatch(Hashtable TicketIds, BatchLetterType batchLetterType, int empid)
        {
            //int empid = 0;
            string ReportName = "";
            string ProcedureName = "";

            //Set Report Name and Procedure
            switch (batchLetterType)
            {
                case BatchLetterType.MissedCourtLetter:
                    ReportName = "\\MissedCourtLetter.rpt";
                    ProcedureName = "USP_HTP_MissedCourt_Letter";

                    break;
                case BatchLetterType.PledOutLetter:
                    ReportName = "\\PledOutLetter.rpt";
                    ProcedureName = "USP_HTP_PledOut_Letter";
                    break;
                // Noufil 3498 04/17/2008 define procedure name and report name for set call
                case BatchLetterType.SetCallLetter:
                    ReportName = "\\SetcallLetter.rpt";
                    ProcedureName = "USP_HTP_SetCall_Letter";
                    break;

                case BatchLetterType.SOL:
                    ReportName = "\\SOLreport.rpt";
                    ProcedureName = "USP_HTP_SOLreport";
                    break;

            }
            //Send Letters To Batch
            foreach (DictionaryEntry key in TicketIds)
            {
                string[] keys = { "@TicketID" };
                object[] value1 = { key.Value };
                string filename = HttpContext.Current.Server.MapPath("..") + "\\Reports" + ReportName;
                batchRpt.CreateReportForEntry(filename, ProcedureName, keys, value1, "true", Convert.ToInt32(batchLetterType), empid, HttpContext.Current.Session, HttpContext.Current.Response);
            }
        }

        public DataTable getinfo(int ticketid)
        {
            String[] keys = { "@ticketid" };
            object[] values = { ticketid };
            DataTable dt = clsDB.Get_DT_BySPArr("Usp_Hts_getinfobyticketid", keys, values);
            return dt;
        }

        public string GetdataFromJims(string url)
        {
            XMLHTTP ObjHttp = new XMLHTTP();
            ObjHttp.open("GET", url, false, null, null);
            ObjHttp.send(null);
            if (ObjHttp.status.ToString() == "200")
            {
                return ObjHttp.responseText.Replace("UTF-8", "UTF-16");
            }
            else
            {
                return "";
            }
        }
        // Noufil 3650 04/30/2008 
        /// <summary>
        /// This procedure update client response and traking number
        /// </summary>
        /// <param name="printempid"></param>
        /// <param name="USPSTrackingNumber"></param>
        /// <param name="USPSResponse"></param>
        /// <param name="BatchId"></param>
        /// <param name="eflag"></param>
        /// <returns>True/False</returns>
        public bool updateBatchPrintInformation(int printempid, string USPSTrackingNumber, string USPSResponse, int BatchId, int eflag)
        {
            String[] Key2 = { "@PrintEmpId", "@USPSTrackingNumber", "@USPSResponse", "@BatchId_pk", "@eflag" };
            object[] value2 = { printempid, USPSTrackingNumber, USPSResponse, BatchId, eflag };
            clsDB.ExecuteSP("Usp_Hts_Edelivery_Update", Key2, value2);
            return true;
        }

        /// <summary>
        /// This method updates client information when letters sent from batch through electronic email.
        /// </summary>
        /// <param name="printempid">Printed letter id</param>
        /// <param name="USPSTrackingNumber">USPS Tracking number</param>
        /// <param name="USPSResponse">Response from USPS service</param>
        /// <param name="BatchId">Batch ID</param>
        /// <param name="eflag">Electronic Flag</param>
        /// <param name="img">Letter in image format </param>
        /// <returns>true/false</returns>
        public bool updateBatchPrintInformation(int printempid, string USPSTrackingNumber, string USPSResponse, int BatchId, int eflag, Byte[] img)
        {
            //SAEED 7931 06/23/2010 method refactored.
            String[] Key2 = { "@PrintEmpId", "@USPSTrackingNumber", "@USPSResponse", "@BatchId_pk", "@eflag", "@DeliveryConfirmationLabel" };
            object[] value2 = { printempid, USPSTrackingNumber, USPSResponse, BatchId, eflag, img };
            int numRowsAffected = Convert.ToInt32(clsDB.ExecuteScalerBySp("Usp_Hts_Edelivery_Update", Key2, value2));
            if (numRowsAffected > 0) return true;
            else return false;

        }

        /// <summary>
        /// This function shows the information about those client those traking number has beeen generated seccuessfully
        /// </summary>
        /// <param name="datetime"></param>
        /// <param name="check"></param>
        /// <returns>DataTable</returns>
        public DataTable getlabels(DateTime datetime, bool check)
        {
            String[] Key2 = { "@datefrom", "@selectall" };
            object[] value2 = { datetime, check };
            DataTable dt = clsDB.Get_DT_BySPArr("Usp_Hts_labelreport", Key2, value2);
            return dt;
        }

        /// <summary>
        /// This procedure returns Lables 
        /// </summary>
        /// <param name="batchid"></param>
        /// <returns>True/False</returns>
        public bool GetEdeliveryImage(string batchid)
        {
            String[] Key2 = { "@BatchId" };
            object[] value2 = { batchid };
            clsDB.ExecuteSP("Usp_Hts_Edelivery_Get", Key2, value2);
            return true;
        }

        /// <summary>
        /// This Method is used to delete letter from the batch
        /// </summary>
        /// <param name="batchLetterType">Batch Letter Type</param>
        /// <param name="TicketId">Case Number</param>
        /// <param name="EmpId">Employee ID</param>
        public void DeleteLetterFromBatch(BatchLetterType batchLetterType, int TicketId, int EmpId)
        {
            //Zeeshan 4163 06/03/2008
            string[] keys = { "@LetterType", "@TicketId", "@EmpId" };
            object[] values = { Convert.ToInt32(batchLetterType), TicketId, EmpId };
            clsDB.ExecuteSP("USP_HTP_Delete_Letter_From_Batch", keys, values);
        }

        /// <summary>
        /// This method insert the LOR Batch History
        /// </summary>
        /// <param name="TicketIds">List of comma separted Ticket IDs</param>
        /// <param name="EmpId">Employee ID</param>
        /// <param name="CertifiedMailNumber">Certified Mail Number</param>
        public void InsertLORBatchHistory(string TicketIds, string BatchIds, int EmpId, string CertifiedMailNumber)
        {
            string[] keys = { "@TicketIDs", "@EmpID", "@CertifiedMailNumber", "@BatchIDs" };
            object[] values = { TicketIds, EmpId, CertifiedMailNumber, BatchIds };
            clsDB.ExecuteSP("USP_HTP_INSERT_LetterOfRepBatchHistory", keys, values);
        }
        //Fahad 9022 03/14/2011 Method Added
        /// <summary>
        /// This method used to Send LOR(Letter Of Rep)to teh Batch Print.
        /// </summary>
        /// <param name="ticketno">Ticket Id of the Client</param>
        /// <param name="empid">Employee Id of the Login User</param>
        /// <param name="lettertype">Type of the Letter(Type of LOR is 6 )</param>
        /// <param name="isBatch">Either send to Batch Or Not.</param>
        public void SendLORToBatch(int ticketno, int empid, int lettertype, bool isBatch)
        {
            if (lettertype == 6 && isBatch)
            {
                string[] keys = { "@TicketID_FK", "@BatchDate", "@PrintDate", "@IsPrinted", "@LetterID_FK", "@EmpID", "@DocPath", "@PageCount" };
                object[] values = { ticketno, System.DateTime.Now, "1/1/1900", 0, lettertype, empid, "batchLOR.pdf", 1 };
                clsDB.InsertBySPArr("USP_HTS_Insert_BatchPrintLetter", keys, values);
            }

        }

    }

}

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.WebControls;
using System.IO;
using WebSupergoo.ABCpdf6;



namespace lntechNew.Components.ClientInfo
{
    /// <summary>
    /// Summary description for clsPayments.
    /// </summary>
    public class NewClsPayments
    {
        //Create an Instance of Data Access Class
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        clsCase ClsCase = new clsCase();
        NewClsCreditCard clscard = new NewClsCreditCard();
        clsLogger bugTracker = new clsLogger();
        clsSession ClsSession = new clsSession();

        #region Variables

        private int invoiceNumber = 0;
        private int ticketID = 0;
        private int paymentType = 0;
        private int empID = 0;
        private int paymentVoid = 0;
        private int voidInitial = 0;
        private int pretrialStatus = 0;
        private int scheduleid = 0;  //newly added  
        private int processflag = 0;//added to track the process scenerio
        private int mainStatus = 0;
        private string cardType = String.Empty;
        private string cardHolderName = String.Empty;
        private string cardNumber = String.Empty;
        private string cin = String.Empty;
        private string authCode = String.Empty;
        private string refNumber = String.Empty;
        private string certifiedMail = String.Empty;
        private string paymentsComments = String.Empty;
        private string cardExpirationDate = String.Empty;
        private double chargeAmount = 0;
        private double totalfee = 0;
        private double owes = 0;
        private DateTime recDate = DateTime.Now;
        private DateTime dueDate = DateTime.Now;
        private DateTime processDate = DateTime.Now;
        //added by ozair for andrew's modification point 2 
        private int toProcess = 0;
        bool TransactionType;

        //Added By Azee for walk in client flag modification
        private string walkinclientflag = String.Empty;
        //

        //
        //added by azee for newpaymentinfo page..
        private int paymentstatus = 0;
        //
        //added by azwar for instant if status is in FTA then restrict payment.
        private string status = String.Empty;
        //

        //Added by Azee
        private string _logfilepath = string.Empty;
        // Noufil 5618 04/02/2009 PaymentFollowUpdate string added
        private string Paymentfollowupdate = string.Empty;
        //Fahad 6054 07/21/2009 New Variable Added paymentReference
        private string paymentreference = string.Empty;
        // Afaq 8213 11/01/2010 used in payment identifier property.
        private int paymentIdentifier = 0;

        private int branchId;

        private int howHired;


        //
        #endregion

        #region Properties

        //Added by Azee
        public string LogFilePath
        {
            get
            {
                return _logfilepath;
            }
            set
            {
                _logfilepath = value;
            }
        }

        //

        public string WalkInClientFlag
        {
            get
            {
                return walkinclientflag;
            }
            set
            {
                walkinclientflag = value;
            }
        }

        //added by azwar for instant if status is in FTA then restrict payment.
        public string Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }
        //


        //added by ozair for andrew's modification point 2 
        public int ToProcess
        {
            get
            {
                return toProcess;
            }
            set
            {
                toProcess = value;
            }
        }

        public int HowHired
        {
            get
            {
                return howHired;
            }
            set
            {
                howHired = value;
            }
        }

        //

        public int Scheduleid
        {
            get
            {
                return scheduleid;
            }
            set
            {
                scheduleid = value;
            }
        }

        public int Processflag
        {
            get
            {
                return processflag;
            }
            set
            {
                processflag = value;
            }
        }

        public int InvoiceNumber
        {
            get
            {
                return invoiceNumber;
            }
            set
            {
                invoiceNumber = value;
            }
        }


        public int PreTrialStatus
        {
            get
            {
                return pretrialStatus;
            }
            set
            {
                pretrialStatus = value;
            }
        }

        public int TicketID
        {
            get
            {
                return ticketID;
            }
            set
            {
                ticketID = value;
            }
        }

        public int PaymentType
        {
            get
            {
                return paymentType;
            }
            set
            {
                paymentType = value;
            }
        }

        public DateTime RecDate
        {
            get
            {
                return recDate;
            }
            set
            {
                recDate = value;
            }
        }

        public string RefNumber
        {
            get
            {
                return refNumber;
            }
            set
            {
                refNumber = value;
            }
        }

        public int MainStatus
        {
            get
            {
                return mainStatus;
            }
            set
            {
                mainStatus = value;
            }
        }

        public DateTime DueDate
        {
            get
            {
                return dueDate;
            }
            set
            {
                dueDate = value;
            }
        }

        public string CardNumber
        {
            get
            {
                return cardNumber;
            }
            set
            {
                cardNumber = value;
            }
        }

        public string CardExpirationDate
        {
            get
            {
                return cardExpirationDate;
            }
            set
            {
                cardExpirationDate = value;
            }
        }


        public string PaymentsComments
        {
            get
            {
                return paymentsComments;
            }
            set
            {
                paymentsComments = value;
            }
        }

        public string Cin
        {
            get
            {
                return cin;
            }
            set
            {
                cin = value;
            }
        }
        public string AuthCode
        {
            get
            {
                return authCode;
            }
            set
            {
                authCode = value;
            }
        }


        public string CardHolderName
        {
            get
            {
                return cardHolderName;
            }
            set
            {
                cardHolderName = value;
            }
        }

        public string CertifiedMail
        {
            get
            {
                return certifiedMail;
            }
            set
            {
                certifiedMail = value;
            }
        }

        public double ChargeAmount
        {
            get
            {
                return chargeAmount;
            }
            set
            {
                chargeAmount = value;
            }
        }

        public double Totalfee
        {
            get
            {
                return totalfee;
            }
            set
            {
                totalfee = value;
            }
        }

        public double Owes
        {
            get
            {
                return owes;
            }
            set
            {
                owes = value;
            }
        }

        public int EmpID
        {
            get
            {
                return empID;
            }
            set
            {
                empID = value;
            }
        }

        public int PaymentVoid
        {
            get
            {
                return paymentVoid;
            }
            set
            {
                paymentVoid = value;
            }
        }

        public int VoidInitial
        {
            get
            {
                return voidInitial;
            }
            set
            {
                voidInitial = value;
            }
        }

        public string CardType
        {
            get
            {
                return cardType;
            }
            set
            {
                cardType = value;
            }
        }

        public int PaymentStatus
        {
            get
            {
                return paymentstatus;
            }
            set
            {
                paymentstatus = value;
            }
        }
        public bool TransType
        {
            get
            {
                return TransactionType;
            }
            set
            {
                TransactionType = value;

            }
        }
        // Afaq 8213 11/01/2010 Create property.
        public int PaymentIdentifier
        {
            get
            {
                return paymentIdentifier;
            }
            set
            {
                paymentIdentifier = value;

            }
        }

        // Noufil 5618 04/02/2009 PaymentFollowUpdate property added       

        public string PaymentFollowUpdate
        {
            get { return Paymentfollowupdate; }
            set { Paymentfollowupdate = value; }
        }
        //Fahad 6054 07/21/2009 New Property PaymentReference Added
        public string PaymentReference
        {
            get
            {
                return paymentreference;
            }
            set
            {
                paymentreference = value;
            }
        }
        public int BranchID
        {
            get { return branchId; }
            set { branchId = value; }
        }       
        public string IPAddress
        {
            get { return ClsSession.GetCookie("LocalIPAddress", System.Web.HttpContext.Current.Request); }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Method To Process Payment
        /// </summary>		
        public bool ProcessPayment()
        {

            clscard.TransactionsMode = TransactionType;
            bool processcc = false;

            //InvoiceNumber= GetNewInvoiceNum();
            //int invno = 0;
            int rowid = 0;
            //object for transaction begin
            clsENationWebComponents clstran = new clsENationWebComponents(true);
            try
            {
                // Afaq 8213 11/01/2010 add @paymentIdentifier for storing payment identifier.
                //If Payment Type is not credit card
                if (PaymentType != 5)
                {
                    clstran.DBBeginTransaction();
                    // Sabir Khan 10920 05/27/2013 Added Machine name, ip address and branch info.
                    string[] key1 ={"@TicketID",  "@PaymentType", "@ChargeAmount",
									  "@EmployeeID", "@scheduleid", "@processflag","@PaymentStatusID","@PaymentReference","@PaymentIdentifier","@branchID","@IPAddress","@HowHired"};
                    object[] value1 = { TicketID, PaymentType, ChargeAmount, EmpID, Scheduleid, Processflag, PaymentStatus, PaymentReference, PaymentIdentifier, BranchID, IPAddress, HowHired };//Fahad 6054 07/21/2009 New Paremeter PaymentReference Added
                    clstran.ExecuteSP("USP_HTS_Process_Payment_New", key1, value1, 1);
                    clstran.DBTransactionCommit();
                    return true;
                }
                else
                {	//Only inserting in Credit Card Table
                    //In order to hide credit card number
                    //string tempcardnum=CardNumber.Substring(0,1)+"*****"+CardNumber.Substring(12,4);
                    string tempcardnum = "";
                    if (CardNumber.Length > 0)
                        tempcardnum = CardNumber.Substring(0, 1) + "*****" + CardNumber.Substring(CardNumber.Length - 4, 4);
                    clstran.DBBeginTransaction();
                    string[] key1 ={"@TicketID","@InvoiceNumber","@ChargeAmount","@EmployeeID","@CardNumber",
									  "@CardExpdate","@name","@cin","@cardtype", "@rowid"};
                    object[] value1 ={TicketID,InvoiceNumber,ChargeAmount,EmpID,tempcardnum,
										CardExpirationDate,CardHolderName,Cin,Convert.ToInt32(CardType),rowid};
                    //clstran.ExecuteSP("USP_HTS_Process_CreditCardPayment",key1,value1,1);

                    // tahir 4554 08/12/2008 fixed the credit card payment bug
                    //rowid = Convert.ToInt16(clstran.InsertBySPArrRet("USP_HTS_Process_CreditCardPayment_New", key1, value1));
                    rowid = Convert.ToInt32(clstran.InsertBySPArrRet("USP_HTS_Process_CreditCardPayment_New", key1, value1));
                    // end 4554
                    clstran.DBTransactionCommit();
                    processcc = true;

                }
            }
            catch (Exception ex)
            {
                clstran.DBTransactionRollback();
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                throw new Exception(ex.Message);
            }

            try
            {
                if (processcc)
                {
                    clscard.EmpId = empID;
                    //Afaq 8312 11/01/2010 set property Value.
                    clscard.PaymentIdentifier = PaymentIdentifier;
                    clscard.ProcessCreditCard(rowid, Scheduleid, CardNumber, Processflag, PaymentStatus, BranchID, IPAddress,HowHired); //providing cardno as it get hidden in DB
                    return true;
                }
            }
            catch (Exception ex)
            {
                //Zeeshan 3961 07/16/2008 Check If Transaction Approved Or Not. If Not Approved Than Display Message Otherwise Void It.
                if (clscard.ResponseCode != "1" || ex.Message.StartsWith("Cannot Retrieve Response From Server") || ex.Message.StartsWith("The transaction was not approved: ") || ex.Message.StartsWith("An error occured during processing."))
                {
                    throw new ApplicationException(ex.Message);
                }
                else
                {
                    try
                    {
                        clscard.InvoiceNumber = rowid;
                        clscard.VoidCreditCardTransaction(EmpID, clscard.TransactionNo);
                    }
                    catch (Exception EX)
                    {
                        string date = DateTime.Now.ToShortDateString();

                        if (EX.Message.StartsWith("Cannot Retrieve Response From Server For The Following Transaction No :") || EX.Message.StartsWith("The transaction was not approved: ") || EX.Message.StartsWith("An error occured during processing. "))
                        {
                            string Message = "DATE: " + date + ", Transaction has not been void because error occurs during credit card processing, Original Exception : " + EX.Message.ToString() + ", Transaction No: " + clscard.TransactionNo.ToString() + ", EmployeeID : " + EmpID.ToString();
                            WriteToLogFile(Message);
                            MailClass.SendMailToAttorneys(Message, "Transaction Issue :", string.Empty, ConfigurationManager.AppSettings["ToEmail"].ToString(), ConfigurationManager.AppSettings["CCEmail"].ToString());
                        }
                        else
                        {
                            string Message = "DATE: " + date + ", Transaction has been void because error occurs during credit card processing but not Updated in Database, Original Exception : " + EX.Message.ToString() + ", Transaction No: " + clscard.TransactionNo.ToString() + ", EmployeeID : " + EmpID.ToString();
                            WriteToLogFile(Message);
                            MailClass.SendMailToAttorneys(Message, "Transaction Issue :", string.Empty, ConfigurationManager.AppSettings["ToEmail"].ToString(), ConfigurationManager.AppSettings["CCEmail"].ToString());
                        }
                        //throw new ApplicationException(ex.Message + innerEx.Message);
                        throw new ApplicationException("Error occurred in credit card processing. Please try re-processing the transaction or contact MIS department if problem persist.");
                    }
                }

                throw new ApplicationException(ex.Message);
            }

            return false;
        }

        /// <summary>
        /// Information regarding Payment  
        /// </summary>
        /// <param name="TicketID">ID for which to get Payment Details</param>		
        public bool GetPaymentInfo(int TicketID)
        {
            try
            {
                string[] key1 = { "@TicketID" };
                object[] value1 = { TicketID };
                //ClsDb.DBBeginTransaction();
                DataSet ds = ClsDb.Get_DS_BySPArr("USP_HTS_GET_NEW_PAYMENTSINFO", key1, value1);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Totalfee = Convert.ToDouble(ds.Tables[0].Rows[0]["Totalfee"]);
                    string actflag = ds.Tables[0].Rows[0]["Activeflag"].ToString();

                    if (actflag == "1")
                    {
                        ChargeAmount = Convert.ToDouble(ds.Tables[0].Rows[0]["Paid"]);
                        Owes = Convert.ToDouble(ds.Tables[0].Rows[0]["Owes"]);
                    }
                    else
                    {
                        Owes = Totalfee;
                        ChargeAmount = 0;
                    }
                    CertifiedMail = ds.Tables[0].Rows[0]["CertifiedMailNumber"].ToString().Trim();
                    PreTrialStatus = Convert.ToInt32(ds.Tables[0].Rows[0]["PretrialStatus"]);
                    PaymentsComments = ds.Tables[0].Rows[0]["PayComments"].ToString().Trim();
                    //added by ozair for andrew's modification point 2 
                    ToProcess = Convert.ToInt32(ds.Tables[0].Rows[0]["toProcess"].ToString().Trim());
                    //
                    //added by Azwar for Instant if case is in FTA then restrict Payment

                    Status = ds.Tables[0].Rows[0]["status"].ToString();
                    //Added by Azee for client walk in flag
                    WalkInClientFlag = ds.Tables[0].Rows[0]["WalkInClientFlag"].ToString();
                    // Noufil 5618 04/02/2009 Payment follow Update column added in property.
                    PaymentFollowUpdate = ds.Tables[0].Rows[0]["PaymentFollowUpDate"].ToString();

                    //
                    //
                    //	MainStatus=Convert.ToInt32(ds.Tables[0].Rows[0]["MainStatus"]);				
                    //ClsDb.DBTransactionCommit();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                //ClsDb.DBTransactionRollback();
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

                return false;
            }

        }
        /// <summary>
        /// Updates Payment Comments,Certified Mail, Pretrialstatus on Payment Info Page 
        /// </summary>		
        public bool UpdatePaymentInfo()
        {
            try
            {
                string[] key1 = { "@TicketID", "@pretrialstatus", "@comments", "@certifiedmailnumber", "@Empid" };
                object[] value1 = { TicketID, PreTrialStatus, PaymentsComments, CertifiedMail, EmpID };
                ClsDb.ExecuteSP("USP_HTS_Update_PaymentInfo", key1, value1);
                return true;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

                return false;
            }
        }

        /// <summary>
        /// Get DataSet For Schedule Payment Details 
        /// </summary>
        /// <param name="TicketID">ID for which to get Schedule Payment(s)</param>		
        public DataSet GetSchedulePaymentInfo(int TicketID)
        {
            string[] key1 = { "@TicketID" };
            object[] value1 = { TicketID };
            //DataSet ds=ClsDb.Get_DS_BySPArr("USP_HTS_Get_SchedulePaymentDetail",key1,value1);
            //return ds;
            return ClsDb.Get_DS_BySPArr("USP_HTS_Get_New_SchedulePaymentDetail", key1, value1);
        }

        /// <summary>
        /// Get DataSet For Payment Details 
        /// </summary>
        /// <param name="TicketID">ID for which to get Schedule Payment(s)</param>		
        public DataSet GetPaymentDetail(int TicketID)
        {
            //ClsDb.DBBeginTransaction();
            string[] key1 = { "@TicketID" };
            object[] value1 = { TicketID };
            //DataSet ds=ClsDb.Get_DS_BySPArr("USP_HTS_GET_PAYMENTSDetail",key1,value1);
            //ClsDb.DBTransactionCommit();
            //return ds;
            return ClsDb.Get_DS_BySPArr("USP_HTS_GET_PAYMENTSDetail", key1, value1);

        }
        /// <summary>
        /// Calls when To void the transaction  
        /// </summary>		
        /// <param name="InvoiceNum">Provide the invoice num of the payment</param>
        public bool VoidPayment(int InvoiceNum, string PayMethod)
        {
            try
            {
                clscard.TransactionsMode = TransactionType;

                string[] key2 = { "@invoicenumber" };
                object[] value2 = { InvoiceNum };
                string transnum = ClsDb.ExecuteScalerBySp("USP_HTS_Get_CreditCardTransNum", key2, value2).ToString();

                //if(PayMethod!="Credit Card")
                if (transnum.Length == 0 || transnum == "0")
                {
                    string[] key1 = { "@invoicenumber", "@employeeid" };
                    object[] value1 = { InvoiceNum, EmpID };
                    ClsDb.ExecuteSP("USP_HTS_Process_VoidTransaction", key1, value1);
                }

                else
                {
                    clscard.InvoiceNumber = InvoiceNum;
                    clscard.TicketId = TicketID;
                    clscard.VoidCreditCardTransaction(InvoiceNum, EmpID);
                }
                return true;
            }
            catch (Exception ex)
            {
                //Zeeshan 4525 08/26/2008 Used to Avoid Time Out Message In The Bug Tracker 
                if (ex.Message.StartsWith("An error occured during processing.") || ex.Message.StartsWith("The transaction was not approved:") || ex.Message.StartsWith("Payment process service is not responding at the moment"))
                {
                    throw new Exception(ex.Message);
                }
                else
                {
                    bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                    throw new Exception(ex.Message);
                }
            }
        }

        /// <summary>
        /// Returns Dataset Contanning Available CardType
        /// </summary>
        /// <returns></returns>
        public DataSet GetCardType()
        {
            //			ClsDb.DBBeginTransaction();
            //			DataSet ds=ClsDb.Get_DS_BySP("USP_HTS_Get_CardType");
            //			ClsDb.DBTransactionCommit();
            //			return ds;
            return ClsDb.Get_DS_BySP("USP_HTS_Get_CardType");
        }

        /// <summary>
        /// Returns DataSet Containing Available Payments Type 
        /// </summary>
        /// <returns></returns>
        public DataSet GetPaymentType()
        {
            //ClsDb.DBBeginTransaction();
            //DataSet ds=ClsDb.Get_DS_BySP("USP_HTS_Get_PaymentType");
            //ClsDb.DBTransactionCommit();
            //return ds;
            return ClsDb.Get_DS_BySP("USP_HTS_Get_PaymentType");
        }
        //Afaq 9398 06/20/2011
        /// <summary>
        /// Returns DataSet Containing Available Payments Identifier Type 
        /// </summary>
        /// <returns></returns>
        public DataTable GetPaymentIdentifierType()
        {
            //ClsDb.DBBeginTransaction();
            //DataSet ds=ClsDb.Get_DS_BySP("USP_HTS_Get_PaymentType");
            //ClsDb.DBTransactionCommit();
            //return ds;
            return ClsDb.Get_DT_BySPArr("USP_HTP_GetPaymentIdentifier");
        }
        /// <summary>
        /// Add Schedule Payment
        /// </summary>
        /// <param name="dsPaymentInfo"></param>
        /// <returns></returns>
        public bool AddSchedulePayment(double Amount, DateTime Date, int Scheduleid)
        {
            try
            {
                //ClsDb.DBBeginTransaction();
                string[] key1 = { "@TicketID", "Amount", "PayDate", "EmpID", "@Scheduleid" };
                object[] value1 = { TicketID, Amount, Date, EmpID, Scheduleid };
                ClsDb.ExecuteSP("USP_HTS_Insert_Edit_SchedulePayment", key1, value1);
                //	ClsDb.DBTransactionCommit();
                return true;
            }
            catch (Exception ex)
            {
                //ClsDb.DBTransactionRollback();
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

                return false;
            }
        }
        /// <summary>
        /// Return New Invoice No
        /// </summary>		
        public int GetNewInvoiceNum()
        {
            try
            {
                int ret = 0;
                string[] key1 = { "@InvoiceNumber" };
                object[] value1 = { 0 };
                object obj = ClsDb.InsertBySPArrRet("USP_HTS_Generate_InvoiceNo", key1, value1);
                return ret = Convert.ToInt32(obj);
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

                return 0;
            }
        }

        /// <summary>
        /// Returns available credit card of the client
        /// </summary>		
        public DataSet GetCreditCardInfo()
        {
            string[] key1 = { "@TicketID" };
            object[] value1 = { TicketID };
            return ClsDb.Get_DS_BySPArr("USP_HTS_GET_CreditCardInfo", key1, value1);
        }
        /// <summary>
        /// When To Delete Schedule Payment
        /// </summary>
        /// <param name="scheduleid">scheduleID to make it inactive</param>
        public void InactiveSchedulePayment(int scheduleid)
        {
            try
            {
                string[] key1 = { "@ScheduleID" };
                object[] value1 = { scheduleid };
                ClsDb.ExecuteSP("USP_HTS_New_Inactive_SchedulePayment", key1, value1);

            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// Get schedule Amount of ID 
        /// </summary>
        /// <param name="scheduleid">ID to get amount</param>
        /// <returns>Schedule Amount</returns>
        /// 
        //Change by Ajmal
        //public System.Data.SqlClient.SqlDataReader GetScheduleAmount(int scheduleid)
        public IDataReader GetScheduleAmount(int scheduleid)
        {
            try
            {
                string[] key1 = { "@ScheduleID" };
                object[] value1 = { scheduleid };

                //object amount=ClsDb.ExecuteScalerBySp("USP_HTS_GET_Payment_By_ScheduleID",key1,value1);
                //System.Data.SqlClient.SqlDataReader dr= ClsDb.Get_DR_BySPArr("USP_HTS_GET_Payment_By_ScheduleID",key1,value1);

                //return dr;
                return ClsDb.Get_DR_BySPArr("USP_HTS_GET_Payment_By_ScheduleID", key1, value1);
            }
            catch (Exception ex)
            {
                //ClsDb.DBTransactionRollback();
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// For Batch Print Letter
        /// </summary>
        /// <returns>DataSet having Batch Print Records</returns>

        public DataSet GetBatchPrintInfo()
        {
            string[] keys = { "@TicketID" };
            object[] values = { TicketID };
            //DataSet ds_Print =   ClsDb.Get_DS_BySPArr("USP_HTS_GET_BATCH_PaymentInfo",keys,values);
            //return ds_Print;			
            return ClsDb.Get_DS_BySPArr("USP_HTS_GET_BATCH_PaymentInfo", keys, values);
        }

        public DataTable GetAuthorizeNetReport(bool showTestTransactions, DateTime startDate, DateTime endDate, string database, string source, string transtype)
        {
            string[] keys = { "@ShowTestTrans", "@Database", "@StartDate", "@EndDate", "@Source", "@TransactionType" };
            object[] values = { Convert.ToInt16(showTestTransactions), database, startDate, endDate, source, transtype };
            return ClsDb.Get_DT_BySPArr("USP_HTS_AUTHORIZE_NET_REPORT", keys, values);
        }

        //Method to check courtid null if any of violation courtid is null then restrict payment..
        public bool CourtIDNullCheck()
        {
            try
            {
                string[] key1 = { "@TicketID" };
                object[] value1 = { ticketID };

                DataTable dt = ClsDb.Get_DT_BySPArr("Usp_Hts_Check_CourtidNull", key1, value1);
                if (dt.Rows.Count > 0)
                {
                    if (Convert.ToInt32(dt.Rows[0][0]) > 0)
                    {
                        return false;
                    }
                    else
                        return true;
                }
                else
                    return true;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                throw new Exception(ex.Message);
            }
        }

        //Waqas 5864 06/26/2009 Get Payment Date
        /// <summary>
        /// This method is used to get max date in schedule payment
        /// </summary>
        /// <param name="TicketID"></param>
        /// <returns></returns>
        public string GetMaxSchedulePaymentDate(int TicketId)
        {
            string[] key1 = { "@TicketID" };
            object[] value1 = { TicketId };
            return ClsDb.ExecuteScalerBySp("USP_HTP_Get_Max_SchedulePaymentDate", key1, value1).ToString();
        }

        //Fahad 6054 07/21/2009 Method Returns True Or False to Check the existing Client having Same CourtLocation and CourtDate
        /// <summary>
        /// This Method Returns True Or False to Check the existing Client having Same CourtLocation and CourtDate
        /// </summary>
        /// <param name="TicketId"></param>
        /// <returns></returns>
        public int GetExistingClientByCourtDateandLocation(int TicketId)
        {
            string[] keys = {"@TicketID"};
            object[] values = {TicketId};
            DataTable dtResult = ClsDb.Get_DT_BySPArr("USP_HTP_GetDefendentByCourtDate", keys, values);
            if (dtResult.Rows.Count > 0)
                return 1;
            else
                return 0;
        }
        //Fahad 6054 07/21/2009 Method Returns True Or False to Check the existing Client having Same CourtLocation and CourtDate
        /// <summary>
        /// This method used for insert Trial Comments in DataBase
        /// </summary>
        /// <param name="scheduleid"></param>
        public void InsertPaymentTrialComments(int TicketId,int EmployeeId,int CommentId,string Comments)
        {
            try
            {
                string[] key1 = { "@TicketID","@EmployeeID","@CommentID","@Comments" };
                object[] value1 = {TicketId,EmployeeId,CommentId,Comments };
                ClsDb.ExecuteSP("USP_HTS_Insert_Comments", key1, value1);

            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                throw new Exception(ex.Message);
            }
        }

        //Fahad 6054 07/24/2009 Method Returns 1 Or 0 to Check the existing Client having PartenerFirmCredit,FreindCredit and AttorneyCredit PaymentType
        /// <summary>
        /// This method Returns 1 Or 0 to Check the existing Client having PartenerFirmCredit,FreindCredit and AttorneyCredit PaymentType
        /// </summary>
        /// <param name="scheduleid"></param>
        public int GetSpecificPaymentType(int TicketId)
        {
            try
            {
                string[] key1 = { "@TicketID" };
                object[] value1 = { TicketId };
                DataTable dt=ClsDb.Get_DT_BySPArr("USP_HTP_GetPaymentMethod", key1, value1);
                if (dt.Rows.Count > 0)
                    return 1;
                else
                    return 0;

            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

                throw new Exception(ex.Message);
            }
        }




        //Abbas Qamar 9957 03/01/2012 Method Returns the All Payment Types.
        /// <summary>
        /// This method used for Return All Payment types.
        /// </summary>
        
        public DataSet GetAllPaymentTypes()
        {
            DataSet ds=new DataSet();
            try
            {
               ds=ClsDb.Get_DS_BySP("USP_HTP_Get_AllPaymentTypes");

            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                throw new Exception(ex.Message);
            }
            return ds;
        }

        //Abbas Qamar 9957 03/01/2012 Method Returns the All Payment Types.
        /// <summary>
        /// This method used to update the payment type record
        /// </summary>
        /// <param name="paymentId">scheduleID to make it inactive</param>
        /// <param name="Description">scheduleID to make it inactive</param>
        /// <param name="shortDesc">scheduleID to make it inactive</param>
        /// <param name="isActive">scheduleID to make it inactive</param>
        public string UpdatePaymentType(int paymentId,string Description,string shortDesc ,bool isActive)
        {
            string effectedRow = string.Empty;
            try
            {
                string[] key1 = { "@PaymentId", "@Description", "@ShortDescription","@IsActive" };
                object[] value1 = { paymentId, Description, shortDesc, isActive };
                effectedRow = ClsDb.ExecuteScalerBySp("USP_HTP_Update_PaymentTypes", key1, value1).ToString();

            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                throw new Exception(ex.Message);
            }
            return effectedRow;
        }

        //Abbas Qamar 9957 03/01/2012 Method Delete the selected Payment Type.
        /// <summary>
        /// Delete the payment Type
        /// </summary>
        /// <param name="PaymenttypeId">scheduleID to make it inactive</param>
        public string DeletePaymentType(int PaymenttypeId)
        {
            string rowcount = string.Empty;
            try
            {
                string[] key1 = { "@Paymenttype_PK" };
                object[] value1 = { PaymenttypeId };
                rowcount=ClsDb.ExecuteScalerBySp("USP_HTP_Delete_PaymentTypes", key1, value1).ToString();

            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

                throw new Exception(ex.Message);
            }

            return rowcount;
        }

        #endregion

        public NewClsPayments()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public NewClsPayments(HttpRequest request)
        {
            ///// Set Test mode according to webconfig value

            //Code Commented by Azwar..
            /*if (ConfigurationSettings.AppSettings["Test_Request"].ToString() == "True")
                TransType = true;
            else
                TransType = false;*/


            TransType = false;


            string localserveripaddress = ConfigurationManager.AppSettings["LocalServerIPAddress"].ToString();
            if (localserveripaddress == request.ServerVariables["REMOTE_HOST"].ToString())
            {
                if (ConfigurationManager.AppSettings["Test_Request"].ToString() == "True")
                    TransType = true;
                else
                    TransType = false;
            }
            else
            {
                TransType = false;
            }

        }




        public void WriteToLogFile(string message)
        {
            LogFilePath = ConfigurationManager.AppSettings["PaymentLogFilePath"].ToString();

            string filename = ConfigurationManager.AppSettings["PaymentLogFileName"].ToString();



            FileStream fs = new FileStream(LogFilePath + filename,
                FileMode.Open, FileAccess.Write);
            StreamWriter m_streamWriter = new StreamWriter(fs);
            m_streamWriter.BaseStream.Seek(0, SeekOrigin.End);
            string[] arr = message.Split(',');
            if (arr.Length >= 0)
            {
                for (int i = 0; i < arr.Length; i++)
                {
                    m_streamWriter.WriteLine(arr[i].ToString());

                }
            }
            m_streamWriter.WriteLine("--------------------------------------------------------------------------------------------");
            m_streamWriter.Flush();
            m_streamWriter.Close();


        }
        /// <summary>
        /// Task Id: 10920
        /// Date: 05/27/2013
        /// This method is used to get branch ID.Avoided changes to GetBranchID(machineName) as it has too much references.
        /// </summary>
        /// <param name="branchName">Take Branch Name</param>
        /// <returns>Branch ID</returns>
        public int GetBranchByName(string branchName)
        {

            int newbranchID = 2;

            DataTable dtBranches = ClsDb.Get_DT_BySPArr("USP_HTP_Get_BranchesName");
            if (dtBranches.Rows.Count > 0)
            {
                foreach (DataRow dr in dtBranches.Rows)
                {

                    if (branchName.StartsWith(dr["BranchName"].ToString(), true, null))
                    {
                        newbranchID = Convert.ToInt32(dr["BranchId"]);
                    }
                }
            }

            return newbranchID;
        }
    }
}

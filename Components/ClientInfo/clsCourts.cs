using System;
using System.Data;
using FrameWorkEnation.Components;
using System.Collections;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Data.Common;
using HTP.Components.ClientInfo;
using HTP.Components.Entities;

namespace lntechNew.Components.ClientInfo
{
    /// <summary>
    /// Summary description for clsCourts.
    /// </summary>
    public class clsCourts
    {
        //Create an Instance of Data Access Class
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        clsLogger bugTracker = new clsLogger();
        CourtsFiles files = new CourtsFiles();
        static clsENationWebComponents ClsDbb = new clsENationWebComponents();
        // Noufil 4747 09/09/2008 Enumeration added
        public enum CourtsName
        {
            HarrisCountyTitle4D = 3058,
            Pasadena = 3004
        }
        #region Variables

        private int courtID = 0;
        private string courtName = String.Empty;
        private string address1 = String.Empty;
        private string address2 = String.Empty;
        private string city = String.Empty;
        private int stateID = 0;
        private int courtType = 0;
        private int settingRequestType = 0;
        private string judgeName = String.Empty;
        private string zipCode = String.Empty;
        private string courtContact = String.Empty;
        private string phone = String.Empty;
        private string fax = String.Empty;
        private string courtDropDownName = String.Empty;
        private int bondType = 0;
        private int precinctID = 0;
        private int inActiveFlag = 0;
        private int isCriminalCourt = 0;
        private string shortName = String.Empty;
        private double visitCharges = 0;
        private DateTime Repdate;
        private bool duplicatecauseno = false; //added by khalid 2-1-08 
        private int isletterofrep;
        //Sabir Khan 5763 04/10/2009 variables for LOR Subpoena and LOR Motion for Discovery...
        private int islorsubpoena;
        private int islorMOD;
        //Muhammad Muneer 8227 09/27/2010 added new variables for the LOR Dates
        private DateTime lorrepdate2;
        private DateTime lorrepdate3;
        private DateTime lorrepdate4;
        private DateTime lorrepdate5;
        private DateTime lorrepdate6;

        //Asad Ali 8153 09/20/2010 declare variable for ALR Associatd court ID
        private int associatedALRCourtID = 0;


        //Kazim 3697 5/6/2008 declare more fields in the clscourts 

        private int caseIdentifiaction;
        private int acceptAppeals;
        private string appealLocation;
        private int initialSetting;
        private int lor;
        private string appearnceHire;
        private string judgeTrialHire;
        private string juryTrialHire;
        private int handwritingAllowed;
        private int additionalFTAIssued;
        private int gracePeriod;
        //Nasir 5310 12/29/2008 Declare aLRProcess
        private int aLRProcess;
        private string continuance;
        private int supportingDocument;
        private string comments;
        private int casetype;
        //Sabir Khan 5941 07/24/2009 refrence county object....
        private County _County;
        // Rab Nawaz Khan 8997 08/02/2011 Added for allow or restrict the court room numbers for the outside courts
        private bool allowCourtNumbers;

        //default false means duplicate is not allowed;

        #endregion

        #region Properties
        //Asad Ali 8153 09/20/2010 declare variable for ALR Associatd court ID
        public int AssociatedALRCourtID
        {
            get
            {
                return associatedALRCourtID;
            }
            set
            {
                associatedALRCourtID = value;
            }
        }
        public int Casetype
        {
            get { return casetype; }
            set { casetype = value; }
        }

        // Noufil 4945 10/31/2008 New property added
        public int IsLetterofRep
        {
            get { return isletterofrep; }
            set { isletterofrep = value; }
        }

        //Sabir Khan 5763 04/10/2009 Properties for LORSubpoena and LORMotion for Discovery...
        //--------------------------
        public int IsLORSubpoena
        {
            get { return islorsubpoena; }
            set { islorsubpoena = value; }
        }
        public int IsLORMOD
        {
            get { return islorMOD; }
            set { islorMOD = value; }
        }

        //---------------------------
        public int CourtID
        {
            get
            {
                return courtID;
            }
            set
            {
                courtID = value;
            }
        }

        public string CourtName
        {
            get
            {
                return courtName;
            }
            set
            {
                courtName = value;
            }
        }

        public string Address1
        {
            get
            {
                return address1;
            }
            set
            {
                address1 = value;
            }
        }

        public string Address2
        {
            get
            {
                return address2;
            }
            set
            {
                address2 = value;
            }
        }

        public string City
        {
            get
            {
                return city;
            }
            set
            {
                city = value;
            }
        }

        public int StateID
        {
            get
            {
                return stateID;
            }
            set
            {
                stateID = value;
            }
        }

        public int CourtType
        {
            get
            {
                return courtType;
            }
            set
            {
                courtType = value;
            }
        }

        public int SettingRequestType
        {
            get
            {
                return settingRequestType;
            }
            set
            {
                settingRequestType = value;
            }
        }

        public string JudgeName
        {
            get
            {
                return judgeName;
            }
            set
            {
                judgeName = value;
            }
        }

        public string ZipCode
        {
            get
            {
                return zipCode;
            }
            set
            {
                zipCode = value;
            }
        }

        public string CourtContact
        {
            get
            {
                return courtContact;
            }
            set
            {
                courtContact = value;
            }
        }

        public string Phone
        {
            get
            {
                return phone;
            }
            set
            {
                phone = value;
            }
        }

        public string Fax
        {
            get
            {
                return fax;
            }
            set
            {
                fax = value;
            }
        }

        public string CourtDropDownName
        {
            get
            {
                return courtDropDownName;
            }
            set
            {
                courtDropDownName = value;
            }
        }

        public int BondType
        {
            get
            {
                return bondType;
            }
            set
            {
                bondType = value;
            }
        }

        public int PrecinctID
        {
            get
            {
                return precinctID;
            }
            set
            {
                precinctID = value;
            }
        }

        public int InActiveFlag
        {
            get
            {
                return inActiveFlag;
            }
            set
            {
                inActiveFlag = value;
            }
        }


        public string ShortName
        {
            get
            {
                return shortName;
            }
            set
            {
                shortName = value;
            }
        }
        public int IsCriminalCourt
        {
            get
            {
                return isCriminalCourt;
            }
            set
            {
                isCriminalCourt = value;
            }

        }

        public double VisitCharges
        {
            get
            {
                return visitCharges;
            }
            set
            {
                visitCharges = value;
            }
        }

        public DateTime LetterOfRep
        {
            get
            {
                return Repdate;
            }
            set
            {
                Repdate = value;
            }
        }

        //Muhammad Muneer 8227 09/27/2010 adding the new properties to store new LOR Dates


        public DateTime LORRepDate2
        {
            get
            {
                return lorrepdate2;
            }
            set
            {
                lorrepdate2 = value;
            }
        }

        public DateTime LORRepDate3
        {
            get
            {
                return lorrepdate3;
            }
            set
            {
                lorrepdate3 = value;
            }
        }

        public DateTime LORRepDate4
        {
            get
            {
                return lorrepdate4;
            }
            set
            {
                lorrepdate4 = value;
            }
        }

        public DateTime LORRepDate5
        {
            get
            {
                return lorrepdate5;
            }
            set
            {
                lorrepdate5 = value;
            }
        }

        public DateTime LORRepDate6
        {
            get
            {
                return lorrepdate6;
            }
            set
            {
                lorrepdate6 = value;
            }
        }


        public bool DuplicateCauseno
        {
            get
            {
                return duplicatecauseno;
            }
            set
            {
                duplicatecauseno = value;
            }
        }

        public int CaseIdentifiaction
        {
            get
            {
                return caseIdentifiaction;
            }
            set
            {
                caseIdentifiaction = value;
            }
        }

        public int AcceptAppeals
        {
            get
            {
                return acceptAppeals;
            }
            set
            {
                acceptAppeals = value;
            }
        }
        public string AppealLocation
        {
            get
            {
                return appealLocation;
            }
            set
            {
                appealLocation = value;
            }
        }
        public int InitialSetting
        {
            get
            {
                return initialSetting;
            }
            set
            {
                initialSetting = value;
            }
        }
        public int LOR
        {
            get
            {
                return lor;
            }
            set
            {
                lor = value;
            }
        }
        public string AppearnceHire
        {
            get
            {
                return appearnceHire;
            }
            set
            {
                appearnceHire = value;
            }
        }
        public string JudgeTrialHire
        {
            get
            {
                return judgeTrialHire;
            }
            set
            {
                judgeTrialHire = value;
            }
        }
        public string JuryTrialHire
        {
            get
            {
                return juryTrialHire;
            }
            set
            {
                juryTrialHire = value;
            }
        }
        public int HandwritingAllowed
        {
            get
            {
                return handwritingAllowed;
            }
            set
            {
                handwritingAllowed = value;
            }
        }
        public int AdditionalFTAIssued
        {
            get
            {
                return additionalFTAIssued;
            }
            set
            {
                additionalFTAIssued = value;
            }
        }
        public int GracePeriod
        {
            get
            {
                return gracePeriod;
            }
            set
            {
                gracePeriod = value;
            }
        }
        //Nasir 5310 12/24/2008 ADD Property for new column in Court
        public int ALRProcess
        {
            get
            {
                return aLRProcess;
            }
            set
            {
                aLRProcess = value;
            }
        }

        public string Continuance
        {
            get
            {
                return continuance;
            }
            set
            {
                continuance = value;
            }
        }
        public int SupportingDocument
        {
            get
            {
                return supportingDocument;
            }
            set
            {
                supportingDocument = value;
            }
        }
        public string Comments
        {
            get
            {
                return comments;
            }
            set
            {
                comments = value;
            }
        }
        //Sabir Khan 5941 07/24/2009 setting value for county object...
        public County County
        {
            get { return _County; }
            set { _County = value; }
        }

        // Rab Nawaz Khan 8997 08/02/2011 Added for allow or restrict the court room numbers for the outside courts
         public bool  AllowCourtNumbers

        {
            get { return  allowCourtNumbers; }
            set {  allowCourtNumbers = value; }
        }
        
        //Nasir 7369 02/19/2010 added AllowOnlineSignUp
        public bool AllowOnlineSignUp { get; set; }
        
        #endregion

        #region Methods

        //Noufil 4237 06/28/2008 casetype parameter added
        //Nasir 5310 12/24/2008 New parameter ALRProcess
        /// <summary>
        ///  This function is use to add court
        /// </summary>
        /// <param name="trans">take DB transaction object</param>
        /// <returns>return integer</returns>
        public int AddCourt(DbTransaction trans)
        {
            try
            {
                //Asad Ali 8153 09/20/2010 add Associated court ID and Remove Code of Online Signup b/c this task code rollback 
                //Sabir Khan 5763 04/16/2009 Parameter added for LOR subpoena and LOR Motion of discovery...
                //Sabir Khan 5941 07/24/2009 countyid parameter has been added...
                //Nasir 7369 02/19/2010 added AllowOnlineSignUp
                // Rab Nawaz Khan 8997 10/18/2011 Allow Court Room Numbers added in insert Query
                string[] key = { "@courtname", "@address", "@address2", "@shortname", "@city", "@state", "@visitcharges", "@courttype", "@settingrequesttype", "@judgename", "@zip", "@courtcontact", "@phone", "@fax", "@shortcourtname", "@bondtype", "@activeflag", "@IsCriminalCourt", "@RepDate", "@CausenoDuplicate", "@CourtId", "@CaseIdentifiaction", "@AcceptAppeals", "@AppealLocation", "@InitialSetting", "@LOR", "@AppearnceHire", "@JudgeTrialHire", "@JuryTrialHire", "@HandwritingAllowed", "@AdditionalFTAIssued", "@GracePeriod", "@ALRProcess", "@Continuance", "@SupportingDocument", "@Comments", "@CaseTypeId", "@isletterofrep", "@islorsubpoena", "@islormod", "@CountyId", "@AssociatedALRCourtID", "@AllowCourtNumbers" };
                object[] value1 = { CourtName, Address1, Address2, ShortName, City, StateID, VisitCharges, CourtType, SettingRequestType, JudgeName, ZipCode, CourtContact, Phone, Fax, CourtDropDownName, BondType, InActiveFlag, IsCriminalCourt, Repdate, DuplicateCauseno, courtID, caseIdentifiaction, acceptAppeals, appealLocation, initialSetting, lor, appearnceHire, judgeTrialHire, juryTrialHire, handwritingAllowed, additionalFTAIssued, gracePeriod, ALRProcess, continuance, supportingDocument, comments, casetype, isletterofrep, islorsubpoena, islorMOD, _County.CountyID, (AssociatedALRCourtID != 0 ? AssociatedALRCourtID.ToString() : DBNull.Value.ToString()), allowCourtNumbers };

                int court = Convert.ToInt32(ClsDb.ExecuteScalerBySp("usp_HTS_Insert_CourtInfo", key, value1, trans));
                return court;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return 0;
            }
        }
        //Noufil 4237 06/28/2008 casetype parameter added
        /// <summary>
        /// This procedure 
        /// </summary>
        /// <returns>return updation status</returns>
        public bool UpdateCourt()
        {
            try
            {
                //Asad Ali 8153 09/20/2010 add Associated court ID and Remove Code of Online Signup b/c this task code rollback 
                //Sabir Khan 5763 04/16/2009 Parameter added for LOR subpoena and LOR Motion of discovery...
                //Sabir Khan 5941 07/24/2009 countyid parameter has been added...
                //Nasir 7369 02/19/2010 added AllowOnlineSignUp
                //Muhammad Muneer 8227 09/28/2010 added the new functionality to support new LOR Dates
                ArrayList SortLOR = new ArrayList();
                if (CourtID == 3004)
                {
                    SortLOR.Insert(0, Repdate);
                    SortLOR.Insert(1, LORRepDate2);
                    SortLOR.Insert(2, LORRepDate3);
                    SortLOR.Insert(3, LORRepDate4);
                    SortLOR.Insert(4, LORRepDate5);
                    SortLOR.Insert(5, LORRepDate6);
                    SortLOR.Sort();

                    Repdate = Convert.ToDateTime(SortLOR[0].ToString());
                    LORRepDate2 = Convert.ToDateTime(SortLOR[1].ToString());
                    LORRepDate3 = Convert.ToDateTime(SortLOR[2].ToString());
                    LORRepDate4 = Convert.ToDateTime(SortLOR[3].ToString());
                    LORRepDate5 = Convert.ToDateTime(SortLOR[4].ToString());
                    LORRepDate6 = Convert.ToDateTime(SortLOR[5].ToString());
                    string[] key = { "@courtid", "@courtname", "@address", "@address2", "@shortname", "@city", "@state", "@visitcharges", "@courttype", "@settingrequesttype", "@judgename", "@zip", "@courtcontact", "@phone", "@fax", "@shortcourtname", "@bondtype", "@activeflag", "@IsCriminalCourt", "@RepDate", "@CausenoDuplicate", "@CaseIdentifiaction", "@AcceptAppeals", "@AppealLocation", "@InitialSetting", "@LOR", "@AppearnceHire", "@JudgeTrialHire", "@JuryTrialHire", "@HandwritingAllowed", "@AdditionalFTAIssued", "@GracePeriod", "@ALRProcess", "@Continuance", "@SupportingDocument", "@Comments", "@CaseTypeId", "@isletterofrep", "@islorsubpoena", "@islormod", "@CountyId", "@AssociatedALRCourtID", "@LORrepdate2", "@LORrepdate3", "@LORrepdate4", "@LORrepdate5", "@LORrepdate6", "@AllowCourtNumbers" };

                    object[] value1 = { CourtID, CourtName, Address1, Address2, ShortName, City, StateID, VisitCharges, CourtType, SettingRequestType, JudgeName, ZipCode, CourtContact, Phone, Fax, CourtDropDownName, BondType, InActiveFlag, IsCriminalCourt, Repdate, DuplicateCauseno, caseIdentifiaction, acceptAppeals, appealLocation, initialSetting, lor, appearnceHire, judgeTrialHire, juryTrialHire, handwritingAllowed, additionalFTAIssued, gracePeriod, ALRProcess, continuance, supportingDocument, comments, casetype, isletterofrep, islorsubpoena, islorMOD, _County.CountyID, (AssociatedALRCourtID != 0 ? AssociatedALRCourtID.ToString() : DBNull.Value.ToString()), LORRepDate2, LORRepDate3, LORRepDate4, LORRepDate5, LORRepDate6, AllowCourtNumbers };
                    ClsDb.ExecuteSP("usp_HTS_Update_CourtInfo", key, value1);
                    return true;

                }
                else
                {
                    string[] key = { "@courtid", "@courtname", "@address", "@address2", "@shortname", "@city", "@state", "@visitcharges", "@courttype", "@settingrequesttype", "@judgename", "@zip", "@courtcontact", "@phone", "@fax", "@shortcourtname", "@bondtype", "@activeflag", "@IsCriminalCourt", "@RepDate", "@CausenoDuplicate", "@CaseIdentifiaction", "@AcceptAppeals", "@AppealLocation", "@InitialSetting", "@LOR", "@AppearnceHire", "@JudgeTrialHire", "@JuryTrialHire", "@HandwritingAllowed", "@AdditionalFTAIssued", "@GracePeriod", "@ALRProcess", "@Continuance", "@SupportingDocument", "@Comments", "@CaseTypeId", "@isletterofrep", "@islorsubpoena", "@islormod", "@CountyId", "@AssociatedALRCourtID", "@LORrepdate2", "@LORrepdate3", "@LORrepdate4", "@LORrepdate5", "@LORrepdate6", "@AllowCourtNumbers" };

                    object[] value1 = { CourtID, CourtName, Address1, Address2, ShortName, City, StateID, VisitCharges, CourtType, SettingRequestType, JudgeName, ZipCode, CourtContact, Phone, Fax, CourtDropDownName, BondType, InActiveFlag, IsCriminalCourt, Repdate, DuplicateCauseno, caseIdentifiaction, acceptAppeals, appealLocation, initialSetting, lor, appearnceHire, judgeTrialHire, juryTrialHire, handwritingAllowed, additionalFTAIssued, gracePeriod, ALRProcess, continuance, supportingDocument, comments, casetype, isletterofrep, islorsubpoena, islorMOD, _County.CountyID, (AssociatedALRCourtID != 0 ? AssociatedALRCourtID.ToString() : DBNull.Value.ToString()), LORRepDate2, LORRepDate3, LORRepDate4, LORRepDate5, LORRepDate6, AllowCourtNumbers };
                    ClsDb.ExecuteSP("usp_HTS_Update_CourtInfo", key, value1);
                    return true;
                }
                }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return false;
            }

        }

        public int DeleteCourt(int CourtID)
        {
            try
            {
                string[] key = { "@courtid", "@isDeleted" };

                object[] value1 = { CourtID, 0 };

                int ccount = Convert.ToInt32(ClsDb.InsertBySPArrRet("usp_HTS_Delete_CourtInfo", key, value1));

                //Kazim 3697 5/6/2008 Delete all the uploaded files of a court automatically when user delete a court. 

                files.CourtId = CourtID;
                files.DeleteAllCourtFiles();
                return ccount;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return 0;
            }

        }

        public DataSet GetCourtInfo()
        {
            DataSet ds = ClsDb.Get_DS_BySP("usp_HTS_Get_CourtInfo");
            return ds;
        }

        //Asad Ali 8153 09/20/2010 get All Associated ALR Courts
        /// <summary>
        /// this method return Associated courts
        /// </summary>
        /// <returns>All Associated courts as a DataSet</returns>
        public DataSet GetAssociatedCourts()
        {
            DataSet ds = ClsDb.Get_DS_BySP("USP_HTP_GET_AssociatedALRCourts");
            return ds;
        }


        public DataSet GetCourtInfo(int CourtID)
        {
            DataSet ds = ClsDb.Get_DS_BySPByOneParmameter("usp_HTS_Get_CourtInfo", "@courtid", CourtID);
            return ds;
        }
        /*public DataSet GetCourt(int CourtID)
        {
            DataSet ds=ClsDb.Get_DS_BySPByOneParmameter("usp_HTS_GetCourt","@CourtID",CourtID);
            return ds;
        }*/

        public DataSet GetShortCourtName()
        {
            DataSet ds = ClsDb.Get_DS_BySP("usp_HTS_GetShortCourtName");
            return ds;
        }

        public DataSet GetAllActiveCourtName()
        {
            return GetAllActiveCourtName(1, 0, Convert.ToInt32(CaseType.Traffic));
        }

        //Change by Ajmal
        //public SqlDataReader GetAllCourtName()

        public IDataReader GetAllCourtName()
        {
            IDataReader rdr = ClsDb.Get_DR_BySP("usp_HTS_GetShortCourtName");
            return rdr;
        }

        // Added By Zeeshan Ahmed
        public bool CheckCriminalCourt(int courtid)
        {
            try
            {
                string[] keys = { "@courtid" };
                object[] values = { courtid };
                return Convert.ToBoolean(ClsDb.ExecuteScalerBySp("USP_HTS_Check_Criminal_Court", keys, values));
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return false;
            }
        }
        // Rab Nawaz Khan 8997 10/18/2011 Allow Court Room numbers logic Added 
        /// <summary>
        /// This method is used to check the allow court room numbers
        /// </summary>
        /// <param name="courtid">courtid</param>
        /// <returns>return boolean</returns>
        public DataTable CheckCourtAllowanceStatus(int courtid)
        {
            try
            {
                string[] keys = { "@courtid" };
                object[] values = { courtid };
                return (DataTable)ClsDb.Get_DT_BySPArr("usp_HTP_GetCourtRoomNumberAllowanceStatus", keys, values);
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return null;
            }
        }

        // Rab Nawaz Khan 8997 10/18/2011 Allow Court Room numbers logic Added 
        /// <summary>
        /// Used to get the Allowed Court Room Numbers 
        /// </summary>
        /// <param name="courtid">Court Id</param>
        /// <returns>Boolean</returns>
        public bool GetAllowedCourtRoomNumbers(int courtid)
        {
            try
            {
                string[] keys = { "@courtid" };
                object[] values = { courtid };
                return Convert.ToBoolean(ClsDb.ExecuteScalerBySp("usp_HTP_GetAllowedCourtRoomNumbers", keys, values));
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return false;
            }
        }


        //Kazim 3533 4/1/2008 used to get the court detail of specific court

        public DataTable GetCourtDetails(int courtid)
        {
            string[] keys = { "@CourtID" };
            object[] values = { courtid };
            DataTable dtcourtdetail = ClsDb.Get_DT_BySPArr("USP_HTP_Get_CourtDetails", keys, values);
            return dtcourtdetail;
        }


        /*public SqlDataReader GetAllCourtCategories()
        {
            SqlDataReader  rdr=ClsDb.Get_DR_BySP("usp_HTS_Mailer_GetShortCourtCategories");
            return rdr;
        }*/


        /// <summary>
        /// Get List of Courts Related To Particular Case Type 
        /// </summary>
        /// <param name="Active">Active Courts</param>
        /// <param name="FilterCourt">Filter Court</param>
        /// <param name="caseType">Case Type</param>
        /// <returns></returns>
        public DataSet GetAllActiveCourtName(int Active, int FilterCourt, int caseType) //Agha Usman 4271 07/02/2008 - Change the type of casetype parameter
        {
            //Zeeshan Ahmed 3979 05/20/2008
            string[] keys = { "@active", "@FilterCourt", "@CaseType" };
            object[] values = { Active, FilterCourt, (int)caseType };
            return ClsDb.Get_DS_BySPArr("usp_HTS_GetShortCourtName", keys, values);
        }
        /// <summary>
        /// Fahad 5172 26/11/2008
        /// Method which identify the LOR method
        /// </summary>
        /// 
        public static int GetLORMethodByCase(int TcktID)
        {
            int Count = 0;
            string[] keys = { "@TicketId" };
            object[] values = { TcktID };
            DataTable dt = ClsDbb.Get_DT_BySPArr("USP_HTP_GET_LORMethod", keys, values);
            if (dt.Rows.Count > 0)
            {
                Count = Convert.ToInt32(dt.Rows[0][0].ToString());
            }
            return Count;
        }
        #endregion

        public clsCourts()
        {

            _County = new County();
        }


        public DataTable GetCourtRoomNumbers()
        {
            return ClsDbb.Get_DT_BySPArr("USP_HTP_GET_CourtRoomNumber");
        }

        public DataTable GetCourtTime()
        {
            return ClsDbb.Get_DT_BySPArr("USP_HTP_GET_CourtRoomTime");
        }

        public DataTable GetCourtAssociation(int courtId)
        {
            string[] key = {"@CourtID"};
            object[] value = {courtId};
            return ClsDbb.Get_DT_BySPArr("USP_HTP_Get_CourtAssociationByCourt",key,value);
        }

        public void InsertCourtAssociation(int courtId,int statusId, int roomId, int timeId)
        {
            string[] key = { "@CourtID","StatusID","RoomID","TimeID"};
            object[] value = { courtId,statusId,roomId,timeId };
            ClsDbb.InsertBySPArr("USP_HTP_Insert_CourtAssociation",key,value);
        }

        public void DeleteCourtAssociation(int courtId, int statusId, int roomId, int timeId)
        {
            string[] key = { "@CourtID", "StatusID", "RoomID", "TimeID" };
            object[] value = { courtId, statusId, roomId, timeId };
            ClsDbb.InsertBySPArr("USP_HTP_Insert_CourtAssociation",key,value);
        }

        //Sabir Khan 9941 12/14/2011
        ///<summary> 
        /// Check PMC LOR Dates for Past and for Full.
        ///</summary>
        ///<returns></returns>
        public int CheckLORDatesForPMC(int TicketID)
        {
            DataTable dt = ClsDbb.Get_DT_BySPArr("USP_HTP_Check_LORDatesForPMC");

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    return Convert.ToInt32(dt.Rows[0][0].ToString());
                }

            }

            // Zeeshan Haider 11306 08/12/2013 PMC Jury-Trial Cases(Do not modify case setting for sending LOR)
            if (!string.IsNullOrEmpty(Convert.ToString(TicketID)) && TicketID > 0)
            {
                clsCase ClsCase = new clsCase();
                if (ClsCase.HasViolationByCourID(TicketID, 3004, 26))
                    return 1;
                
            }
            return 0;
        }

        //Zeeshan Haider 10699 04/18/2013
        /// <summary> 
        /// Compare Case Court Date with LOR Dates for PMC
        /// </summary>
        /// <param name="ticketId"></param>
        /// <returns>True/False</returns>
        public string CompareCourtDateLORDatesPMC(int ticketId)
        {
            string[] key = { "@TicketID" };
            object[] value = { ticketId };
            DataTable dt = ClsDbb.Get_DT_BySPArr("USP_HTP_Compare_CaseCourtDate_LORDatesPMC", key, value);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    return dt.Rows[0][0].ToString();
                }
            }
            return "0";
        }
        //Faique Ali 11491 10/21/2013 add method to get Active/InActive Court..
        public DataTable IsActiveCourtCase(int TicketID)
        {
            string[] key = { "@TicketId" };
            object[] value = { TicketID };
            DataTable dt = ClsDbb.Get_DT_BySPArr("USP_HTP_Check_ActiveCourt", key, value);
            return dt;
            //return Convert.ToBoolean(Convert.ToInt16(ClsDb.ExecuteScalerBySp("USP_HTP_Check_ActiveCourt", key, value)));
        }
    }
}

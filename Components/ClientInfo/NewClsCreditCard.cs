using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.WebControls;
using System.IO;
using WebSupergoo.ABCpdf6;
using MSXML2;
using System.Net;
using lntechNew.ServiceReference1;

namespace lntechNew.Components.ClientInfo
{
    /// <summary>
    /// Summary description for clsCreditCard.
    /// </summary>
    public class NewClsCreditCard
    {
        #region Variables

        //Create an Instance of Data Access Class
        clsENationWebComponents clstran = new clsENationWebComponents(true);
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        clsLogger bugTracker = new clsLogger();
        AppSettingsReader asReader = new AppSettingsReader();

        int TicketID;
        int ScheduleID;
        int invoiceNumber;
        public bool TransType;
        string LServerIPAddress;

        // Added By Zeeshan Ahmed
        string transactionno = "0";
        string x_response_code = "";
        string x_response_subcode = "";
        string x_response_reason_code = "";
        string x_response_reason_text = "";
        string x_auth_code = "";
        string x_avs_code = "";
        string x_trans_id = "";
        string x_invoice_num = "";
        string fullname = "";
        string cnum = "";
        string cin = "";
        string expdate = "";
        string cardtype = "";
        double amount = 0;
        int empid = 0;
        int paymenttype = 0;
        bool isVoid = false;
        string reftransactionnumber = "";
        // Afaq 8213 11/01/2010 used in PaymentIdentifier property.
        private int paymentIdentifier;
        #endregion

        #region Properties

        public int InvoiceNumber
        {
            get
            {
                return invoiceNumber;
            }
            set
            {
                invoiceNumber = value;
            }
        }

        public bool TransactionsMode
        {
            get
            {
                return TransType;
            }
            set
            {
                TransType = value;

            }
        }

        public string TransactionNo
        {
            get
            {
                return transactionno;
            }
            set
            {
                transactionno = value;
            }
        }

        // Added By Zeeshan Ahmed

        public int TicketId
        {
            set
            {
                TicketID = value;
            }
        }

        public string CardNumber
        {
            set
            {
                cnum = value;
            }
        }

        public string CVN
        {
            set
            {
                cin = value;
            }
        }

        public string CardType
        {
            set
            {
                cardtype = value;
            }
        }

        public string ExpDate
        {
            set
            {
                expdate = value;
            }
        }

        public int EmpId
        {
            set
            {
                empid = value;
            }
        }

        public int PaymentType
        {
            set
            {
                paymenttype = value;
            }
        }

        public string ResponseCode
        {
            get
            {
                return x_response_code;
            }
        }
        // Afaq 8213 11/01/2010 Create property.
        public int PaymentIdentifier
        {
            get
            {
                return paymentIdentifier;
            }
            set
            {
                paymentIdentifier = value;

            }
        }

        #endregion

        #region Methods


        public void  ProcessCreditCard(int InvoiceNum, int Scheduleid, string cardnum, int processflag, int PaymentStatusID, int branchId, string IPAddress, int HowHired)
        {
            
            
            string PostData = "";
            //PostData = "x_Login=ant481205930";        //test account for Authorize.Net
            PostData = "x_Login=" + asReader.GetValue("Login", typeof(string)).ToString();
            PostData += "&x_tran_key=9ee3TdyZz4k7U66E";
            //The following line is commented for test transection.. Sabir Khan 04/07/2018
            //PostData += "&x_tran_key=" + asReader.GetValue("Tran_Key", typeof(string)).ToString();
            PostData += "&x_Version=3.0";
            //PostData += "&x_Test_Request=False";  //   ' since we are testing
            //PostData += "&x_Test_Request=True";  //   'True since we are testing

            ///////////////////PostData += "&x_Test_Request=" + TransType;
            //PostData += "&x_Test_Request=" + asReader.GetValue("Test_Request", typeof(string)).ToString();
            PostData += "&x_ADC_Delim_Data=TRUE";  //'we want comma delimited
            PostData += "&x_ADC_URL=FALSE";  // 'do not want to redirect
            //Now add the form fields

            //Getting Info of client credit card for processing by invoice number
            string[] key1 ={ "@invnumber" };
            object[] value1 ={ InvoiceNum };
            DataSet ds_cardinfo = ClsDb.Get_DS_BySPArr("USP_HTS_GET_CREDITCARD_BY_INVOICENUM", key1, value1);
            TicketID = Convert.ToInt32(ds_cardinfo.Tables[0].Rows[0]["ticketid"]);

            //PostData += "&x_invoice_num="+ds_cardinfo.Tables[0].Rows[0]["invnumber"].ToString();		
            PostData += "&x_invoice_num=" + InvoiceNum;
            PostData += "&x_Card_Num=" + cardnum;
            PostData += "&x_Exp_Date=" + ds_cardinfo.Tables[0].Rows[0]["expdate"].ToString();
            PostData += "&x_amount=" + ds_cardinfo.Tables[0].Rows[0]["amount"].ToString();
            PostData += "&x_card_code=" + ds_cardinfo.Tables[0].Rows[0]["cin"].ToString();

            PostData += "&x_Description=Traffic Tickets, Case No: " + ds_cardinfo.Tables[0].Rows[0]["caseno"].ToString() + ", Ticket Number: " + ds_cardinfo.Tables[0].Rows[0]["ticketnumber"].ToString();
            PostData += "&x_First_Name=" + ds_cardinfo.Tables[0].Rows[0]["firstname"].ToString();
            PostData += "&x_Last_Name =" + ds_cardinfo.Tables[0].Rows[0]["lastname"].ToString();
            PostData += "&x_Address=" + ds_cardinfo.Tables[0].Rows[0]["Address"].ToString();
            PostData += "&x_City=" + ds_cardinfo.Tables[0].Rows[0]["city"].ToString();
            PostData += "&x_State=" + ds_cardinfo.Tables[0].Rows[0]["state"].ToString();
            PostData += "&x_ZIP=" + ds_cardinfo.Tables[0].Rows[0]["zip"].ToString();
            PostData += "&x_PHONE=" + ds_cardinfo.Tables[0].Rows[0]["phoneno"].ToString();
            PostData += "&x_FAX=" + ds_cardinfo.Tables[0].Rows[0]["phoneno1"].ToString();
            PostData += "&x_DRIVERS_LICENSE_NUM=" + ds_cardinfo.Tables[0].Rows[0]["dlnumber"].ToString();


            // Assign Information For Tracking User
            fullname = ds_cardinfo.Tables[0].Rows[0]["firstname"].ToString() + " " + ds_cardinfo.Tables[0].Rows[0]["lastname"].ToString();
            cnum = ds_cardinfo.Tables[0].Rows[0]["ccnum"].ToString();
            cin = ds_cardinfo.Tables[0].Rows[0]["cin"].ToString();
            expdate = ds_cardinfo.Tables[0].Rows[0]["expdate"].ToString();
            amount = Convert.ToDouble(ds_cardinfo.Tables[0].Rows[0]["amount"].ToString());
            cardtype = ds_cardinfo.Tables[0].Rows[0]["cardtype"].ToString();

            ////
            //string connection = "https://secure.authorize.net/gateway/transact.dll";
            string connection = asReader.GetValue("ConnectionURL", typeof(string)).ToString();

            char[] sep = { ',' };
            //XMLHTTP objHTTP = new XMLHTTP();
            //objHTTP.open("POST", connection, false, null, null);
            //objHTTP.send(PostData);
            ////string response =objHTTP.responseText;

            //if (objHTTP.status == 200)
            //{
            //    string res = objHTTP.responseText;

            //    string[] alag = res.Split(sep);
            //    ScheduleID = Scheduleid;
            //    UpdateCreditCardTransaction(alag, processflag,PaymentStatusID);
            //}
            //else
            //{
            //    throw new Exception("Cannot Retrieve Response From Server");
            //}

            LNServiceClient lnService = new LNServiceClient();

            var ticketcybercash = lnService.ProcessCreditCardTransactionHTP(PostData,connection);

            StreamWriter myWriter = null;
            string[] alag;

            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(connection);
            objRequest.Method = "POST";
            objRequest.ContentLength = PostData.Length;
            objRequest.ContentType = "application/x-www-form-urlencoded";
            x_response_code = "";
            //Zeeshan 4525 08/26/2008 Set Void Payment TimeOut 60 Seconds
            objRequest.Timeout = 60000;

            try
            {
                myWriter = new StreamWriter(objRequest.GetRequestStream());
                myWriter.Write(PostData);
                myWriter.Close();

                HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
                using (StreamReader sr = new StreamReader(objResponse.GetResponseStream()))
                {
                    string res = sr.ReadToEnd();
                    alag = res.Split(sep);
                    
                    sr.Close();
                }
                ScheduleID = Scheduleid;
                UpdateCreditCardTransaction(alag, processflag, PaymentStatusID, branchId, IPAddress, HowHired);
            }
            catch (WebException webEx)
            {
                if (webEx.Status == WebExceptionStatus.Timeout)
                    throw new ApplicationException("Payment process service is not responding at the moment please try later in few minutes or contact your system administrator.");
                else
                    throw new ApplicationException("Cannot Retrieve Response From Server - " + webEx.Message);
            }
            catch (Exception e)
            {
                if (x_response_code != "")
                    throw new ApplicationException(e.Message);
                else
                    throw new ApplicationException("Cannot Retrieve Response From Server - " + e.Message);
            }
            finally
            {
                if (myWriter != null)
                    myWriter.Close();
            }

        }
        /// <summary>
        /// Update credit card table and payments table	
        /// </summary>							
        private void UpdateCreditCardTransaction(string[] array, int processflag, int PaymentStatusID, int branchId, string IPAddress, int HowHired)
        {
            string Auth_Code = string.Empty;
            try
            {
                x_response_code = array[0];
                //Response Code:
                //1 = This transaction has been approved.
                //2 = This transaction has been declined.
                //3 = There has been an error processing this transaction.

                x_response_subcode = array[1];
                x_response_reason_code = array[2];
                x_response_reason_text = array[3];
                x_auth_code = array[4];   //6 digit approval code
                x_avs_code = array[5];
                Auth_Code = x_auth_code;
                /*
                    'Address Verification system:
                'A = Address (street) matches, Zip does not
                'E = AVS error
                'N = No match on address or zip
                'P = AVS Not Applicable
                'R = Retry, system unavailable or timed out
                'S = service not supported by issuer
                'U = address information is unavailable
                'W = 9 digit Zip matches, address does not
                'X = exact AVS match
                'Y = address and 5 digit zip match
                'Z = 5 digit zip matches, address does not
                */

                x_trans_id = array[6];  //  'transaction id
                x_invoice_num = array[7];
                string x_description = array[8];
                string x_amount = array[9];
                string x_method = array[10];
                string x_type = array[11];
                string x_cust_id = array[12];
                string x_first_name = array[13];
                string x_last_name = array[14];
                string x_company = array[15];
                string x_address = array[16];
                string x_city = array[17];
                string x_state = array[18];
                string x_zip = array[19];
                string x_country = array[20];
                string x_phone = array[21];
                string x_fax = array[22];
                string x_email = array[23];
                string x_ship_to_first_name = array[24];
                string x_ship_to_last_name = array[25];
                string x_ship_to_company = array[26];
                string x_ship_to_address = array[27];
                string x_ship_to_city = array[28];
                string x_ship_to_state = array[29];
                string x_ship_to_zip = array[30];
                string x_ship_to_country = array[31];
                string x_tax = array[32];
                string x_duty = array[33];
                string x_freight = array[34];
                string x_tax_exempt = array[35];
                string x_po_num = array[36];
                string x_md5_hash = array[37];

                TransactionNo = x_trans_id;

                // Log Transaction ( Added By Zeeshan Ahmed )
                isVoid = false;
                reftransactionnumber = x_invoice_num;
                TrackTransactionLog("");
                // Afaq 8213 11/01/2010 add @paymentIdentifier for storing payment identifier.
                if (x_response_code == "1")
                {
                    clstran.DBBeginTransaction();
                    string[] key1 = { "@ticketid", "@invnumber", "@status", "@errmsg", "@transnum", "@response_subcode", "@response_reason_code", "@response_reason_text", "@auth_code", "@avs_code", "@scheduleid", "@processflag", "@PaymentStatusID", "@responsecode", "@paymentIdentifier", "@branchid", "@IPAddress", "@HowHired" };
                    object[] value1 = { TicketID, x_invoice_num, "0", "", x_trans_id, x_response_subcode, x_response_reason_code, x_response_reason_text, x_auth_code, x_avs_code, ScheduleID, processflag, PaymentStatusID, Convert.ToInt32(x_response_code), PaymentIdentifier, branchId, IPAddress, HowHired };
                    clstran.ExecuteSP("USP_HTS_UPDATE_CREDITCARD_INFO", key1, value1, 1);
                    clstran.DBTransactionCommit();

                }
                else if (x_response_code == "2")
                {
                    clstran.DBBeginTransaction();
                    string[] key1 = { "@ticketid", "@invnumber", "@status", "@errmsg", "@transnum", "@response_subcode", "@response_reason_code", "@response_reason_text", "@auth_code", "@avs_code", "@scheduleid", "@processflag", "@PaymentStatusID", "@responsecode", "@branchid", "@IPAddress", "@HowHired" };
                    object[] value1 = { TicketID, x_invoice_num, "", "", x_trans_id, x_response_subcode, x_response_reason_code, x_response_reason_text, x_auth_code, x_avs_code, ScheduleID, processflag, PaymentStatusID, Convert.ToInt32(x_response_code), branchId, IPAddress, HowHired };
                    clstran.ExecuteSP("USP_HTS_UPDATE_CREDITCARD_INFO", key1, value1, 1);
                    clstran.DBTransactionCommit();

                    // Noufil 3691 07/16/2008 Check the new notes from database.If is doesnot exist then return the Old notes
                    string ReasonText = GetNotesForResponse(Convert.ToInt32(x_response_code), x_response_reason_code);
                    throw new Exception(ReasonText == "" ? (x_response_reason_text) : (ReasonText));

                }
                else
                {
                    clstran.DBBeginTransaction();
                    string[] key1 = { "@ticketid", "@invnumber", "@status", "@errmsg", "@transnum", "@response_subcode", "@response_reason_code", "@response_reason_text", "@auth_code", "@avs_code", "@scheduleid", "@processflag", "@PaymentStatusID", "@responsecode", "@branchid", "@IPAddress", "@HowHired" };
                    object[] value1 = { TicketID, x_invoice_num, "", "", x_trans_id, x_response_subcode, x_response_reason_code, x_response_reason_text, x_auth_code, x_avs_code, ScheduleID, processflag, PaymentStatusID, Convert.ToInt32(x_response_code), branchId, IPAddress, HowHired };
                    clstran.ExecuteSP("USP_HTS_UPDATE_CREDITCARD_INFO", key1, value1, 1);
                    clstran.DBTransactionCommit();

                    // Noufil 3691 07/16/2008 Check the new notes from database.If is doesnot exist then return the Old notes
                    string ReasonText = GetNotesForResponse(Convert.ToInt32(x_response_code), x_response_reason_code);
                    throw new Exception(ReasonText == "" ? (x_response_reason_text) : (ReasonText));

                }

            }
            catch (Exception ex)
            {
                if (x_response_code != "1")
                {
                    throw new Exception(ex.Message);
                }
                else
                {
                    bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                    throw new Exception(ex.Message + "Auth_Code: " + Auth_Code);
                }
            }
        }

        /// <summary>
        /// Voids Credit card transaction
        /// </summary>

        public void VoidCreditCardTransaction(int invnum, int empid)
        {


            string[] key1 ={ "@invnumber" };
            object[] value1 ={ invnum };
            DataSet ds_voidcardinfo = ClsDb.Get_DS_BySPArr("USP_HTS_GET_CREDITCARD_BY_INVOICENUM_VOID", key1, value1);
            TicketID = Convert.ToInt32(ds_voidcardinfo.Tables[0].Rows[0]["ticketid"]);
            InvoiceNumber = invnum;

            string PostData = "";
            PostData = "x_Login=" + asReader.GetValue("Login", typeof(string)).ToString();
            PostData += "&x_tran_key=" + asReader.GetValue("Tran_Key", typeof(string)).ToString();
            PostData += "&x_Version=3.0";
            PostData += "&x_Test_Request=" + TransType;

            PostData += "&x_ADC_Delim_Data=TRUE";  //'we want comma delimited
            PostData += "&x_ADC_URL=FALSE";  // 'do not want to redirect
            PostData += "&x_TYPE=VOID";
            PostData += "&x_TRANS_ID=" + ds_voidcardinfo.Tables[0].Rows[0]["transnum"].ToString();
            transactionno = ds_voidcardinfo.Tables[0].Rows[0]["transnum"].ToString();
            string connection = asReader.GetValue("ConnectionURL", typeof(string)).ToString();

            char[] sep = { ',' };
            //XMLHTTP objHTTP = new XMLHTTP();
            //objHTTP.open("POST", connection, false, null, null);
            //objHTTP.send(PostData);

            //if (objHTTP.status == 200)
            //{
            //    string res = objHTTP.responseText;

            //    string[] alag = res.Split(sep);
            //    UpdateCreditCardTransaction_Void(alag, empid);
            //}
            //else
            //{
            //    throw new Exception("Cannot Retrieve Response From Server");
            //}

            StreamWriter myWriter = null;
            string[] alag;

            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(connection);
            objRequest.Method = "POST";
            objRequest.ContentLength = PostData.Length;
            objRequest.ContentType = "application/x-www-form-urlencoded";
            //Zeeshan 4525 08/26/2008 Set Void Payment TimeOut 60 Seconds
            objRequest.Timeout = 60000;
            try
            {
                myWriter = new StreamWriter(objRequest.GetRequestStream());
                myWriter.Write(PostData);
                //ozair 4488 07/30/2008 streamwriter must be closed before getting the response
                myWriter.Close();
                //endozair 4488
                HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
                using (StreamReader sr = new StreamReader(objResponse.GetResponseStream()))
                {
                    string res = sr.ReadToEnd();
                    alag = res.Split(sep);

                    sr.Close();
                }
                UpdateCreditCardTransaction_Void(alag, empid);
            }
            catch (WebException webEx)
            {
                if (webEx.Status == WebExceptionStatus.Timeout)
                    throw new ApplicationException("Payment process service is not responding at the moment please try later in few minutes or contact your system administrator.");
                else
                    throw new ApplicationException("Cannot Retrieve Response From Server - " + webEx.Message);
            }
            catch (Exception e)
            {
                throw new ApplicationException("Cannot Retrieve Response From Server - " + e.Message);
            }
            finally
            {
                if (myWriter != null)
                    myWriter.Close();
            }
        }

        // Added By Zeeshan Ahmed
        public void VoidCreditCardTransaction(int empid, string transnum)
        {

            if (transnum != "0" && transnum != "")
            {
                //Zeeshan 4525 08/27/2008 User HttpWebRequest Object For Transaction
                string PostData = "";
                PostData = "x_Login=" + asReader.GetValue("Login", typeof(string)).ToString();
                PostData += "&x_tran_key=" + asReader.GetValue("Tran_Key", typeof(string)).ToString();
                PostData += "&x_Version=3.0";
                PostData += "&x_Test_Request=" + TransType;
                PostData += "&x_ADC_Delim_Data=TRUE";  //'we want comma delimited
                PostData += "&x_ADC_URL=FALSE";  // 'do not want to redirect
                PostData += "&x_TYPE=VOID";
                PostData += "&x_TRANS_ID=" + transnum;

                //ds_voidcardinfo.Tables[0].Rows[0]["transnum"].ToString();
                transactionno = transnum;
                string connection = asReader.GetValue("ConnectionURL", typeof(string)).ToString();
                StreamWriter myWriter = null;
                char[] sep = { ',' };
                string[] alag;

                HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(connection);
                objRequest.Method = "POST";
                objRequest.ContentLength = PostData.Length;
                objRequest.ContentType = "application/x-www-form-urlencoded";
                //Zeeshan 4525 08/26/2008 Set Void Payment TimeOut 60 Seconds
                objRequest.Timeout = 60000;
                try
                {
                    myWriter = new StreamWriter(objRequest.GetRequestStream());
                    myWriter.Write(PostData);
                    //ozair 4488 07/30/2008 streamwriter must be closed before getting the response
                    myWriter.Close();
                    //endozair 4488
                    HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
                    using (StreamReader sr = new StreamReader(objResponse.GetResponseStream()))
                    {
                        string res = sr.ReadToEnd();
                        alag = res.Split(sep);
                        sr.Close();
                    }
                    UpdateCreditCardTransaction_Void(alag, empid);
                }
                catch 
                {
                    throw new Exception("Cannot Retrieve Response From Server For The Following Transaction No :" + transnum);
                }
                finally
                {
                    if (myWriter != null)
                        myWriter.Close();
                }
            }
            else
            {
                string[] key1 ={ "@invoicenumber", "@employeeid" };
                object[] value1 ={ transnum, empid };
                ClsDb.ExecuteSP("USP_HTS_Process_VoidTransaction", key1, value1);
            }

        }

        private void UpdateCreditCardTransaction_Void(string[] array, int empid)
        {

            x_response_code = array[0];
            //Response Code:
            //1 = This transaction has been approved.
            //2 = This transaction has been declined.
            //3 = There has been an error processing this transaction.

            x_response_subcode = array[1];
            x_response_reason_code = array[2];
            x_response_reason_text = array[3];
            x_auth_code = array[4];   //6 digit approval code
            x_avs_code = array[5];
            /*
                'Address Verification system:
            'A = Address (street) matches, Zip does not
            'E = AVS error
            'N = No match on address or zip
            'P = AVS Not Applicable
            'R = Retry, system unavailable or timed out
            'S = service not supported by issuer
            'U = address information is unavailable
            'W = 9 digit Zip matches, address does not
            'X = exact AVS match
            'Y = address and 5 digit zip match
            'Z = 5 digit zip matches, address does not
            */

            x_trans_id = array[6];  //  'transaction id
            /*
            string x_invoice_num             = array[7];
            string x_description             = array[8];
            string x_amount                  = array[9];
            string x_method                  = array[10];
            string x_type                    = array[11];
            string x_cust_id                 = array[12];
            string x_first_name              = array[13];
            string x_last_name               = array[14];
            string x_company                 = array[15];
            string x_address                 = array[16];
            string x_city                    = array[17];
            string x_state                   = array[18];
            string x_zip                     = array[19];
            string x_country                 = array[20];
            string x_phone                   = array[21];
            string x_fax                     = array[22];
            string x_email                   = array[23];
            string x_ship_to_first_name      = array[24];
            string x_ship_to_last_name       = array[25];
            string x_ship_to_company         = array[26];
            string x_ship_to_address         = array[27];
            string x_ship_to_city            = array[28];
            string x_ship_to_state           = array[29];
            string x_ship_to_zip             = array[30];
            string x_ship_to_country         = array[31];
            string x_tax                     = array[32];
            string x_duty                    = array[33];
            string x_freight                 = array[34];
            string x_tax_exempt              = array[35];
            string x_po_num                  = array[36];
            string x_md5_hash                = array[37];
            */

            // Get Card Information
            ClearCardInformation();
            // Log Transaction
            isVoid = true;
            this.empid = empid;
            reftransactionnumber = transactionno;
            TrackTransactionLog(invoiceNumber.ToString());

            if (x_response_code == "1")
            {
                string[] key1 = { "@employeeid", "@invoicenumber", "@status", "@errmsg", "@transnum", "@response_subcode", "@response_reason_code", "@response_reason_text", "@auth_code", "@avs_code", "@responsecode" };
                object[] value1 ={ empid, InvoiceNumber, "0", "", x_trans_id, x_response_subcode, x_response_reason_code, x_response_reason_text, x_auth_code, x_avs_code, Convert.ToInt32(x_response_code) };
                ClsDb.ExecuteSP("USP_HTS_UPDATE_CREDITCARD_INFO_FOR_VOID", key1, value1);
            }
            else if (x_response_code == "2")
            {
                // Noufil 3691 07/16/2008 Check the new notes from database.If is doesnot exist then return the Old notes
                string ReasonText = GetNotesForResponse(Convert.ToInt32(x_response_code), x_response_reason_code);
                throw new Exception(ReasonText == "" ? (x_response_reason_text) : (ReasonText));
            }
            else
            {
                // Noufil 3691 07/16/2008 Check the new notes from database.If is doesnot exist then return the Old notes
                string ReasonText = GetNotesForResponse(Convert.ToInt32(x_response_code), x_response_reason_code);
                throw new Exception(ReasonText == "" ? (x_response_reason_text) : (ReasonText));
            }
        }
        public DataTable GetManualCCDetail()
        {
            DataTable dt = ClsDb.Get_DT_BySPArr("sp_Get_TestingData");
            return dt;
        }

        // Added By Zeeshan Ahmed For Logging Credit Card Transaction
        // Added By Zeeshan Ahmed To Save Transaction Log
        public void TrackTransactionLog(string x_invoice_num)
        {
            try
            {
                string[] key_log = { "@Ticketid", "@invnumber", "@status", "@errmsg", "@transnum", "@response_subcode", "@response_reason_code", "@response_reason_text", "@auth_code", "@avs_code", "@approvedflag", "@amount", "@ccnum", "@expdate", "@name", "@cin", "@cardtype", "@paymentmethod", "@empid", "@RetCode", "@ClearFlag", "@TransType", "@isVoid", "@RefTransNum", "@responsecode" };
                object[] value_log ={ TicketID, x_invoice_num, "0", "", x_trans_id, x_response_subcode, x_response_reason_code, x_response_reason_text, x_auth_code, x_avs_code, 1, amount, cnum, expdate, fullname, cin, cardtype, paymenttype, empid, "", 0, 1, isVoid, reftransactionnumber, Convert.ToInt32(x_response_code) };
                clstran.ExecuteScalerBySp("USP_HTS_INSERT_CREDITCARD_INFO_FOR_LOG", key_log, value_log);
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }


        // Get Information For Void Transaction
        private void ClearCardInformation()
        {
            //Getting Info of client credit card  by invoice number
            try
            {
                string[] key1 ={ "@invnumber" };
                object[] value1 ={ invoiceNumber };
                DataSet ds_cardinfo = ClsDb.Get_DS_BySPArr("USP_HTS_GET_CREDITCARD_BY_INVOICENUM_VOID", key1, value1);
                x_invoice_num = invoiceNumber.ToString();
                fullname = ds_cardinfo.Tables[0].Rows[0]["firstname"].ToString() + " " + ds_cardinfo.Tables[0].Rows[0]["lastname"].ToString();
                cnum = ds_cardinfo.Tables[0].Rows[0]["ccnum"].ToString();
                cin = ds_cardinfo.Tables[0].Rows[0]["cin"].ToString();
                expdate = ds_cardinfo.Tables[0].Rows[0]["expdate"].ToString();
                amount = Convert.ToDouble(ds_cardinfo.Tables[0].Rows[0]["amount"].ToString());
                cardtype = ds_cardinfo.Tables[0].Rows[0]["cardtype"].ToString();
            }
            catch
            {
                x_invoice_num = invoiceNumber.ToString();
                fullname = "";
                cnum = "";
                cin = "";
                expdate = "";
                amount = 0;
                cardtype = "0";
            }
        }

        /// <summary>
        ///     This Methods Return the Response Notes Which we have written in the database for user friendly reading
        /// </summary>
        /// <param name="responseCode">Response Code</param>
        /// <param name="responseReasionCode">Response Reason Code</param>
        /// <returns>Notes</returns>
        public string GetNotesForResponse(int responseCode, string responseReasionCode)
        {
            // Noufil 3961 07/16/2008 This function return Notes
            string[] key_code = { "@Response_code", "@responsereasoncode" };
            object[] value_code = { responseCode, responseReasionCode };
            return Convert.ToString(clstran.ExecuteScalerBySp("USP_HTP_GetResponseNotes", key_code, value_code));
        }

        #endregion

        public NewClsCreditCard()
        {

        }
    }
}

﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Specialized;
using System.Xml;
using System.IO;
using FrameWorkEnation.Components;
using LNHelper;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace lntechNew.Components.ClientInfo
{
    public class ProblemClientLookup
    {

        #region Variables
        private decimal _Id;
        private string _FirstName;
        private string _MiddleName;
        private string _Lastname;
        private DateTime _DOB;
        private bool _IsManual;
        private bool _isNew;
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        #endregion

        #region Constructors

        public ProblemClientLookup()
        {
            _Id = 0;
        }

        public ProblemClientLookup(decimal Id): this()
        {
            _Id = Id;
        }

        public ProblemClientLookup(decimal Id, string FirstName, string MiddleName, string Lastname, DateTime DOB, bool IsManual): this()
        {
            _Id = Id;
            _FirstName = FirstName;
            _MiddleName = MiddleName;
            _Lastname = Lastname;
            _DOB = DOB;
            _IsManual = IsManual;
        }

        #endregion

        #region Properties

        public decimal Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        public string FirstName
        {
            get { return _FirstName; }
            set { _FirstName = value; }
        }

        public string MiddleName
        {
            get { return _MiddleName; }
            set { _MiddleName = value; }
        }

        public string Lastname
        {
            get { return _Lastname; }
            set { _Lastname = value; }
        }

        public DateTime DOB
        {
            get { return _DOB; }
            set { _DOB = value; }
        }

        public bool IsManual
        {
            get { return _IsManual; }
            set { _IsManual = value; }
        }

        public bool IsNew
        {
            get
            {
                _isNew = (_Id == 0);
                return _isNew;
            }
            set { _isNew = value; }
        }

        #endregion

        #region Functions

        /// <summary>
        /// Populates a dataset with info from the database, based on the requested primary key.
        /// </summary>
        /// <param name="Id"></param>
        /// <returns>A DataSet containing the results of the query</returns>
        private DataSet LoadByPrimaryKey(decimal Id)
        {
            string sqlCommand = "usp_hts_GetProblemClientLookup";

            // Add in parameters
            string[] key = { "@Id" };
            object[] value1 = { Id };

            // DataSet that will hold the returned results		
            // Note: connection closed by ExecuteDataSet method call 
            DataSet ds = ClsDb.Get_DS_BySPArr(sqlCommand, key, value1);
            return ds;
        }

        /// <summary>
        /// Populates a dataset with info from the database, based on the requested Personal Info.
        /// </summary>
        /// <param name=""></param>
        /// <returns>A DataSet containing the results of the query</returns>
        private DataSet LoadByPersonalInfo(String FirstName, String MiddleName, String LastName, DateTime DOB)
        {
            string sqlCommand = "usp_hts_GetProblemClientLookup";

            // Add in parameters
            string[] key = { "@FirstName", "@MiddleName", "@Lastname", "@DOB"};
            object[] value1 = { FirstName, MiddleName, LastName, DOB };

            // DataSet that will hold the returned results		
            // Note: connection closed by ExecuteDataSet method call 
            DataSet ds = ClsDb.Get_DS_BySPArr(sqlCommand, key, value1);
            return ds;
        }

        /// <summary>
        /// Populates current instance of the object with info from the database, based on the requested primary key.
        /// </summary>
        /// <param name="Id"></param>
        public virtual void Load(decimal Id)
        {
            // DataSet that will hold the returned results		
            DataSet ds = this.LoadByPrimaryKey(Id);

            // Load member variables from datarow
            DataRow row = ds.Tables[0].Rows[0];
            _Id = (decimal)row["Id"];
            _FirstName = (string)row["FirstName"];
            _MiddleName = (string)row["MiddleName"];
            _Lastname = (string)row["Lastname"];
            _DOB = (DateTime)row["DOB"];
            _IsManual = (bool)row["IsManual"];
        }


        /// <summary>
        /// Populates current instance of the object with info from the database, based on the requested Personal Info.
        /// </summary>
        /// <param name="Id"></param>
        public virtual void Load(String FirstName, String MiddleName, String LastName, DateTime DOB)
        {
            // DataSet that will hold the returned results		
            DataSet ds = this.LoadByPersonalInfo(FirstName, MiddleName, LastName, DOB);

            // Load member variables from datarow
            DataRow row = ds.Tables[0].Rows[0];
            _Id = (decimal)row["Id"];
            _FirstName = (string)row["FirstName"];
            _MiddleName = (string)row["MiddleName"];
            _Lastname = (string)row["Lastname"];
            _DOB = (DateTime)row["DOB"];
            _IsManual = (bool)row["IsManual"];
        }

        /// <summary>
        /// Adds or updates information in the database depending on the primary key stored in the object instance.
        /// </summary>
        /// <returns>Returns True if saved successfully, False otherwise.</returns>
        public bool Save()
        {
            if (this.IsNew)
                return Insert();
            else
                return Update();
        }

        /// <summary>
        /// Inserts the current instance data into the database.
        /// </summary>
        /// <returns>Returns True if saved successfully, False otherwise.</returns>
        private bool Insert()
        {
            
            string sqlCommand = "usp_hts_AddProblemClientLookup";

            // Add in parameters
            string[] key = { "@Id", "@FirstName", "@MiddleName", "@Lastname", "@DOB", "@IsManual" };
            object[] value1 = { 0, _FirstName, _MiddleName, _Lastname, _DOB, _IsManual };

            // Save output parameter values
            object param;
            param = ClsDb.InsertBySPArrRet(sqlCommand, key, value1);
            if (param == DBNull.Value) return false;
            _Id = (decimal)param;

            return true;
        }

        /// <summary>
        /// Updates the current instance data in the database.
        /// </summary>
        /// <returns>Returns True if saved successfully, False otherwise.</returns>
        public bool Update()
        {
            
            string sqlCommand = "usp_hts_UpdateProblemClientLookup";

            
            string[] key = { "@Id", "@FirstName", "@MiddleName", "@Lastname", "@DOB", "@IsManual" };
            object[] value1 = { 0, _FirstName, _MiddleName, _Lastname, _DOB, _IsManual };

            try
            {
                // Save output parameter values
                object param;
                param = ClsDb.InsertBySPArrRet(sqlCommand, key, value1);
                if (param == DBNull.Value) return false;
                _Id = (decimal)param;

                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Removes info from the database, based on the requested primary key.
        /// </summary>
        /// <param name="Id"></param>
        public static void Remove(decimal Id)
        {
            clsENationWebComponents ClsDb1 = new clsENationWebComponents();

            string sqlCommand = "usp_hts_DeleteProblemClientLookup";

            // Add in parameters
            string[] key = { "@Id" };
            object[] value1 = { Id };


            ClsDb1.InsertBySPArr(sqlCommand, key, value1);
            //db.ExecuteNonQuery(dbCommandWrapper);
        }

        /// <summary>
        /// Serializes the current instance data to an Xml string.
        /// </summary>
        /// <returns>A string containing the Xml representation of the object.</returns>
        virtual public string ToXml()
        {
            // DataSet that will hold the returned results		
            DataSet ds = this.LoadByPrimaryKey(_Id);
            StringWriter writer = new StringWriter();
            ds.WriteXml(writer);
            return writer.ToString();
        }

        /// <summary>
        /// Utility function that returns a DBNull.Value if requested. The comparison is done inline
        /// in the Insert() and Update() functions.
        /// </summary>
        private object SetNullValue(bool isNullValue, object value)
        {
            if (isNullValue)
                return DBNull.Value;
            else
                return value;
        }

        #endregion
    }
}



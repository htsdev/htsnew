using System;
using System.Data;
using System.Data.SqlClient;
using FrameWorkEnation.Components;
using LNHelper;

namespace lntechNew.Components.ClientInfo
{
    /// <summary>
    /// clsUser:Class to authenticate users.
    /// </summary>
    public class clsUser
    {
        //Create an Instance of Data Access Class
        DBComponent ClsDb = new DBComponent("Houston");
        //Zeeshan Ahmed 4703 08/28/2008
        DBComponent ClsTicketDesk = new DBComponent("TicketDesk");

        #region Variables

        private int empID = 0;
        private int CloseOutAccessDays = 0;
        private int status = 0;
        private int isattorney = 0;
        private int iservicetadmin = 0;
        // Zahoor 4676 09/17/08
        private int userLocation = 0;
        //Fahad 5196 11/28/2008
        private int associatedFirmID = 0;

        private string loginID = String.Empty;
        private string password = String.Empty;
        private string firstName = String.Empty;
        private string lastName = String.Empty;
        private string abbreviation = String.Empty;
        private string userNameSPN = String.Empty;
        private string passwordSPN = String.Empty;
        private bool isSPNUser = false;
        private int accessType = 0;
        private bool isSalesRep = false;
        private bool canupdateCloseOut = false;
        private string NTuserID = String.Empty;
        private string _email = String.Empty;
        #endregion

        #region Properties

        /// <summary>
        /// Get or Set User(s) EmpID   
        /// </summary>
        public int EmpID
        {
            get
            {
                return empID;
            }
            set
            {
                empID = value;
            }
        }

        /// <summary>
        /// Get or Set User(s) LoginID
        /// </summary>
        public string LoginID
        {
            get
            {
                return loginID;
            }
            set
            {
                loginID = value;
            }
        }
        /// <summary>
        /// Get or Set User(s) Password
        /// </summary>
        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                password = value;
            }
        }

        public int Status
        {
            get { return status; }
            set { status = value; }
        }

        public int Iservicetadmin
        {
            get { return iservicetadmin; }
            set { iservicetadmin = value; }
        }

        /// <summary>
        /// Get or Set User(s) FirstName
        /// </summary>
        public string FirstName
        {
            get
            {
                return firstName;
            }
            set
            {
                firstName = value;
            }
        }

        public int Isattorney
        {
            get { return isattorney; }
            set { isattorney = value; }
        }

        /// <summary>
        /// Get or Set User(s) LastName
        /// </summary>
        public string LastName
        {
            get
            {
                return lastName;
            }
            set
            {
                lastName = value;
            }
        }
        /// <summary>
        /// Get or Set User(s) Abbreviation
        /// </summary>
        public string Abbreviation
        {
            get
            {
                return abbreviation;
            }
            set
            {
                abbreviation = value;
            }
        }
        /// <summary>
        /// Get or Set User(s) AccessType 
        /// </summary>
        public int AccessType
        {
            get
            {
                return accessType;
            }
            set
            {
                accessType = value;
            }
        }
        /// <summary>
        /// Get or Set User(s) IsSalesRep 
        /// </summary>
        public bool IsSalesRep
        {
            get
            {
                return isSalesRep;
            }
            set
            {
                isSalesRep = value;
            }
        }

        /// <summary>
        /// Get or Set User(s) canUpdateCloseOut 
        /// </summary>
        public bool canUpdateCloseOut
        {
            get
            {
                return canupdateCloseOut;
            }
            set
            {
                canupdateCloseOut = value;
            }
        }

        /// <summary>
        /// Get or Set User's spn used id
        /// </summary>
        public string UserNameSPN
        {
            get { return userNameSPN; }
            set { userNameSPN = value; }
        }

        /// <summary>
        /// Get or set user's spn password
        /// </summary>
        public string PasswordSPN
        {
            get { return passwordSPN; }
            set { passwordSPN = value; }
        }

        ///<summary>
        /// Get or Set user's spn search flag
        ///</summary>
        public bool IsSPNUser
        {
            get { return isSPNUser; }
            set { isSPNUser = value; }
        }

        ///<summary>
        /// Get or Set user's NTUserID field
        ///</summary>
        public string NTUserID
        {
            get { return NTuserID; }
            set { NTuserID = value; }
        }
        /// <summary>
        /// khalid 3188 2/19/08 represents id of serviceticket Admin
        /// </summary>
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        //Nasir 6968 12/04/2009
        /// <summary>
        /// Get and set User Contact Number
        /// </summary>
        public string ContactNumber { get; set; }

        public int CloseOutAccessDay
        {
            get { return CloseOutAccessDays; }
            set { CloseOutAccessDays = value; }
        }

        public int UserLocation
        {
            get { return userLocation; }
            set { userLocation = value; }
        }

        /// <summary>
        /// Fahad 5196 11/28/2008
        /// 
        /// </summary>
        public int AssociatedFirmID
        {
            get { return associatedFirmID; }
            set { associatedFirmID = value; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Method to authenticate user
        /// </summary>
        /// <param name="LoginID">Specify UserID</param>
        /// <param name="Password">Specify Password</param>		


        public int AuthenticateUser()
        {
            //Change by Ajmal
            //SqlDataReader dr_autuser = null;
            IDataReader dr_autuser = null;
            //Checking user existance in DB
            try
            {
                string[] key1 = { "@LoginID", "@Password" };
                object[] value1 = { LoginID, Password };
                dr_autuser = ClsDb.GetDRBySPArr("USP_HTS_Get_UserInfo", key1, value1);
                DataTable dt = new DataTable();
                dt.Load(dr_autuser);
                if(dt != null && dt.Rows.Count>0)
                {
                    DataRow dr = dt.Rows[0];
                    EmpID = Convert.ToInt32(dr["EmpId"]);
                    LoginID = dr["UserName"].ToString();
                    Password = dr["Password"].ToString();
                    LastName = dr["LastName"].ToString();
                    FirstName = dr["FirstName"].ToString();
                    Abbreviation = dr["Abbreviation"].ToString();
                    AccessType = Convert.ToInt32(dr["AccessType"].ToString());
                    canUpdateCloseOut = Convert.ToBoolean(dr["canupdatecloseoutlog"]);
                    IsSPNUser = Convert.ToBoolean(dr["flagCanSPNSearch"]);
                    UserNameSPN = dr["SPNUserName"].ToString();
                    PasswordSPN = dr["SPNPassword"].ToString();
                    // Zahoor 4676 09/17/08
                    userLocation = Convert.ToInt32(dr["UserLocation"]);
                    
                    //Turk: This distrubing change password functionality commented for time being
                    //EmpID = 3992;

                    return EmpID;
                }
                //if (dr_autuser.Read() == true)
                //{
                //    EmpID = Convert.ToInt32(dr_autuser["EmpId"]);
                //    LoginID = dr_autuser["UserName"].ToString();
                //    Password = dr_autuser["Password"].ToString();
                //    LastName = dr_autuser["LastName"].ToString();
                //    FirstName = dr_autuser["FirstName"].ToString();
                //    Abbreviation = dr_autuser["Abbreviation"].ToString();
                //    AccessType = Convert.ToInt32(dr_autuser["AccessType"].ToString());
                //    canUpdateCloseOut = Convert.ToBoolean(dr_autuser["canupdatecloseoutlog"]);
                //    IsSPNUser = Convert.ToBoolean(dr_autuser["flagCanSPNSearch"]);
                //    UserNameSPN = dr_autuser["SPNUserName"].ToString();
                //    PasswordSPN = dr_autuser["SPNPassword"].ToString();
                //    // Zahoor 4676 09/17/08
                //    userLocation = Convert.ToInt32(dr_autuser["UserLocation"]);
                //    return EmpID;
                //}
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                string[] key12 = { "@Message", "@Source", "@TargetSite", "@StackTrace" };
                object[] value11 = { ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace };
                ClsDb.InsertBySPArr("usp_hts_ErrorLog", key12, value11);
                return 0;
            }
            finally
            {
                dr_autuser.Close();
            }
        }

        public DataSet getAllUsersList()
        {
            try
            {
                DataSet ds = ClsDb.GetDSBySPArr("USp_HTS_GET_All_Users");
                return ds;
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                return new DataSet();
            }
        }

        public int AuthenticateUserByNTCredentials(string NTUserID)
        {
            DataTable dtRecord = null;
            try
            {
                string[] key1 = { "@NTUserID" };
                object[] value1 = { NTUserID };
                dtRecord = ClsDb.GetDTBySPArr("USP_HTS_Get_NTUserInfo", key1, value1);
                if (dtRecord.Rows.Count > 0)
                {
                    EmpID = Convert.ToInt32(dtRecord.Rows[0]["EmpId"]);
                    LoginID = dtRecord.Rows[0]["UserName"].ToString();
                    Password = dtRecord.Rows[0]["Password"].ToString();
                    LastName = dtRecord.Rows[0]["LastName"].ToString();
                    FirstName = dtRecord.Rows[0]["FirstName"].ToString();
                    Abbreviation = dtRecord.Rows[0]["Abbreviation"].ToString();
                    AccessType = Convert.ToInt32(dtRecord.Rows[0]["AccessType"].ToString());
                    canUpdateCloseOut = Convert.ToBoolean(dtRecord.Rows[0]["canupdatecloseoutlog"]);
                    IsSPNUser = Convert.ToBoolean(dtRecord.Rows[0]["flagCanSPNSearch"]);
                    UserNameSPN = dtRecord.Rows[0]["SPNUserName"].ToString();
                    PasswordSPN = dtRecord.Rows[0]["SPNPassword"].ToString();
                    NTUserID = dtRecord.Rows[0]["NTUserID"].ToString();
                    return EmpID;
                }
                else
                    return 0;
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                return 0;
            }
        }

        public void UpdateNTUserID(int EmpID, string WindowsUserID)
        {
            try
            {
                string[] key12 = { "@EmpID", "@NtLoginID" };
                object[] value11 = { EmpID, WindowsUserID };
                ClsDb.ExecuteSP("USP_HTS_UPDATE_NTLOGIN_ID", key12, value11);
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
            }
        }


        /// <summary>
        /// Get the list of Attorneys  //0=not attorneys, 1=attorneys, 2=all
        /// </summary>
        /// <returns>DataSet Containing List Of Attorneys</returns>
        public DataSet GetAttorneyList(int IsAttorney)
        {
            //Aziz 2382 12/19/2007
            //IsAttorney Flag added
            // Code Modified BY Fahad add new condition if IsAttorney = 0 then IsAttorney = null (Fahad 1/12/08)
            object att;
            if ((IsAttorney == 2) || (IsAttorney == 0))
            {
                att = null;
                return ClsDb.GetDSBySP("usp_Get_Attorney");
            }
            else
            {
                att = IsAttorney;
                return ClsDb.GetDSBySPByOneParmameter("usp_Get_Attorney", "@IsAttorney", att);
            }
        }


        #endregion

        public static clsUser GetServiceTicketAdmin()
        {

            clsUser Admin = new clsUser();
            DBComponent dB = new DBComponent();
            DataTable dt_admininfo = new DataTable();

            try
            {
                DataSet ds = dB.GetDSBySP("USP_HTS_Get_ServiceTicketAdminInfo");
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    dt_admininfo = ds.Tables[0];


                    Admin.EmpID = Convert.ToInt32(dt_admininfo.Rows[0]["EmpId"]);
                    Admin.LoginID = Convert.ToString(dt_admininfo.Rows[0]["UserName"]);
                    Admin.Password = Convert.ToString(dt_admininfo.Rows[0]["Password"]);
                    Admin.LastName = Convert.ToString(dt_admininfo.Rows[0]["LastName"]);
                    Admin.FirstName = Convert.ToString(dt_admininfo.Rows[0]["FirstName"]);
                    Admin.Abbreviation = Convert.ToString(dt_admininfo.Rows[0]["Abbreviation"]);
                    Admin.Abbreviation = Convert.ToString(dt_admininfo.Rows[0]["Abbreviation"]);
                    Admin.AccessType = Convert.ToInt32(dt_admininfo.Rows[0]["AccessType"]);
                    Admin.canUpdateCloseOut = Convert.ToBoolean(dt_admininfo.Rows[0]["canupdatecloseoutlog"]);
                    Admin.IsSPNUser = Convert.ToBoolean(dt_admininfo.Rows[0]["flagCanSPNSearch"]);
                    Admin.UserNameSPN = Convert.ToString(dt_admininfo.Rows[0]["SPNUserName"]);
                    Admin.PasswordSPN = Convert.ToString(dt_admininfo.Rows[0]["SPNPassword"]);
                    Admin.Email = Convert.ToString(dt_admininfo.Rows[0]["Email"]);

                }
                return Admin;

            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                return Admin;
            }

        }




        /// <summary>
        /// This method is used to get System User Information
        /// </summary>
        /// <returns>DataTable conatins System User Info</returns>

        //kazim 3791 4/22/2008 Get System User Information

        public DataTable GetSystemUserInfo()
        {
            string[] keys = { "@employeeid" };
            object[] values = { empID };
            return ClsDb.GetDTBySPArr("USP_HTS_get_system_user_info", keys, values);
        }

        // Zahoor 4676 09/17/08
        // <summary>
        /// This Function return information about employee
        /// </summary>
        /// <returns> DataTable with information about employee</returns>        
        public clsUser GetSystemUserInfo(int empId)
        {
            clsUser User = new clsUser();
            string[] keys = { "@employeeid" };
            object[] values = { empId };
            DataTable dT = ClsDb.GetDTBySPArr("USP_HTS_get_system_user_info", keys, values);


            if (dT.Rows.Count > 0)
            {
                DataRow dr = dT.Rows[0];
                //ozair 09/06/2008 setting empid,loginid for user object
                User.empID = empId;
                User.loginID = dr["username"].ToString();
                User.firstName = dr["Firstname"].ToString();
                User.lastName = dr["lastname"].ToString();
                User.abbreviation = dr["abbreviation"].ToString();
                User.password = dr["password"].ToString();
                User.userNameSPN = dr["username"].ToString();
                User.accessType = Convert.ToInt32(dr["accesstype"].ToString());
                User.Email = dr["Email"].ToString();
                User.canupdateCloseOut = Convert.ToBoolean(Convert.ToInt32(dr["CanUpdateCloseOutLog"].ToString()));

                //Waqas Javed 5057 03/17/2009 
                //User.CloseOutAccessDay = Convert.ToInt32(dr["CloseOutAceessDays"].ToString());
                //User.status = int.Parse(dr["status"].ToString());
                //User.userLocation = Convert.ToInt32(dr["UserLocation"].ToString());
                //User.isattorney = Convert.ToInt32(dr["isattorney"].ToString());

                User.IsSPNUser = Convert.ToBoolean(dr["flagCanSPNSearch"]);
                User.CloseOutAccessDay = Convert.ToInt32(dr["CloseOutAccessDays"].ToString());
                User.status = Convert.ToInt32(dr["status"]);
                User.isattorney = Convert.ToInt32(dr["isattorney"]);
            }

            return User;
        }

        public clsUser()
        {

        }
        // Agha Usman 4271 07/02/2008
        // Create New Constructor
        //Noufil 4232 07/07/2008 now column "CloseOutAceessDays" added in data table 
        public clsUser(int UserId)
        {
            DBComponent dB = new DBComponent();
            DataTable dt_admininfo = new DataTable();

            try
            {
                string[] keys = { "@employeeid" };
                object[] values = { UserId };
                DataTable dt = ClsDb.GetDTBySPArr("USP_HTS_get_system_user_info", keys, values);

                if (dt.Rows.Count > 0)
                {
                    dt_admininfo = dt;

                    this.EmpID = Convert.ToInt32(dt_admininfo.Rows[0]["EmpId"]);
                    this.LoginID = Convert.ToString(dt_admininfo.Rows[0]["UserName"]);
                    this.Password = Convert.ToString(dt_admininfo.Rows[0]["Password"]);
                    this.LastName = Convert.ToString(dt_admininfo.Rows[0]["LastName"]);
                    this.FirstName = Convert.ToString(dt_admininfo.Rows[0]["FirstName"]);
                    this.Abbreviation = Convert.ToString(dt_admininfo.Rows[0]["Abbreviation"]);
                    this.Abbreviation = Convert.ToString(dt_admininfo.Rows[0]["Abbreviation"]);
                    this.AccessType = Convert.ToInt32(dt_admininfo.Rows[0]["AccessType"]);
                    this.canUpdateCloseOut = Convert.ToBoolean(dt_admininfo.Rows[0]["canupdatecloseoutlog"]);
                    this.IsSPNUser = Convert.ToBoolean(dt_admininfo.Rows[0]["flagCanSPNSearch"]);
                    this.UserNameSPN = Convert.ToString(dt_admininfo.Rows[0]["SPNUserName"]);
                    this.PasswordSPN = Convert.ToString(dt_admininfo.Rows[0]["SPNPassword"]);
                    this.Email = Convert.ToString(dt_admininfo.Rows[0]["Email"]);
                    this.CloseOutAccessDay = Convert.ToInt32(dt_admininfo.Rows[0]["CloseOutAccessDays"]);
                    this.status = Convert.ToInt32(dt_admininfo.Rows[0]["status"]);
                    this.NTuserID = Convert.ToString(dt_admininfo.Rows[0]["NTUserID"]);
                    this.isattorney = Convert.ToInt32(dt_admininfo.Rows[0]["IsAttorney"]);
                    this.iservicetadmin = Convert.ToInt32(dt_admininfo.Rows[0]["isServiceTicketAdmin"]);
                    //Fahad 5196 11/28/2008
                    this.associatedFirmID = Convert.ToInt32(dt_admininfo.Rows[0]["FirmID"]);
                    //Nasir 6968 12/04/2009 set contact number
                    this.ContactNumber =dt_admininfo.Rows[0]["ContactNumber"] != null ? dt_admininfo.Rows[0]["ContactNumber"].ToString():null;


                }

            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
            }
        }
        /// <summary>
        /// This method returns all users
        /// </summary>
        /// <param name="status"></param>
        /// <param name="orderby"></param>
        /// <returns>Dataset with all users</returns>
        public DataSet GetAllUserList(string status, string orderby)
        {
            string[] keys = { "@status", "@orderby" };
            object[] values = { status, orderby };
            DataSet ds = ClsDb.GetDSBySPArr("USP_HTS_list_all_users", keys, values);
            return ds;
        }
        /// <summary>
        ///  This method insert User records in the database
        /// </summary>
        /// <param name="fname"></param>
        /// <param name="lname"></param>
        /// <param name="abbrev"></param>
        /// <param name="uname"></param>
        /// <param name="passwrd"></param>
        /// <param name="accesstype"></param>
        /// <param name="closeout"></param>
        /// <param name="empid"></param>
        /// <param name="email"></param>
        /// <param name="canspnsearch"></param>
        /// <param name="SPNUserName"></param>
        /// <param name="SPNPassword"></param>
        /// <param name="status"></param>
        /// <param name="NTUserID"></param>
        /// <param name="check"></param>
        /// <param name="stadmin"></param>
        /// <param name="noofdays"></param>
        /// <param name="FirmId"></param>

        //ozair 4759 09/13/2008 changed the parameter name spnsearch to canspnsearch
        //also changed it's type to bool instead of string as the procedure has this parameter as bit.
        //Fahad 5196 11/28/2008/   FirmID added//
        //Nasir 6968 12/04/2009 added contact number
        /// <summary>
        /// Add new user
        /// </summary>
        /// <param name="fname">client first name</param>
        /// <param name="lname">client Last name</param>
        /// <param name="abbrev">client abbreviation</param>
        /// <param name="uname">client user name</param>
        /// <param name="passwrd">client password</param>
        /// <param name="accesstype">client access type</param>
        /// <param name="closeout">client close out</param>
        /// <param name="empid">employee identification</param>
        /// <param name="email">client email</param>
        /// <param name="canspnsearch">can spn search</param>
        /// <param name="SPNUserName">SPNUserName</param>
        /// <param name="SPNPassword">SPNPassword</param>
        /// <param name="status">client status</param>
        /// <param name="NTUserID">Network UserID</param>
        /// <param name="check">check</param>
        /// <param name="stadmin">stadmin</param>
        /// <param name="noofdays">noofdays</param>
        /// <param name="FirmId">FirmId</param>
        /// <param name="ContactNumber">client Contact Number</param>
        public void Adduser(string fname, string lname, string abbrev, string uname, string passwrd, string accesstype, string closeout, string empid, string email, bool canspnsearch, string SPNUserName, string SPNPassword, string status, string NTUserID, bool check, bool stadmin, string noofdays, string FirmId, string ContactNumber)
        {
            //Nasir 6968 12/04/2009 add contact number
            string[] keys = { "@firstname", "@lastname", "@Abbreviation", "@username", "@password", "@accesstype", "@CanUpdateCloseOutLog", "@employeeid", "@email", "@FlagCanSPNSearch", "@SPNUserName", "@SPNPassword", "@Status", "@NTUserID", "@IsAttorney", "@IsSTAdmin", "@CloseOutAccessDays", "@FirmID", "@ContactNumber" };
            object[] values = { fname, lname, abbrev, uname.Trim(), passwrd, accesstype, closeout, empid, email.Trim(), canspnsearch, SPNUserName, SPNPassword, status, NTUserID.Trim(), check, stadmin, noofdays, FirmId, ContactNumber };
            ClsDb.ExecuteSP("USP_HTS_Add_users", keys, values);
            
            //JZTurk commented by Jahanzeb, because Database not found on server, and application not working properly due to this line
            //UpdateTicketDeskPassword(uname.Trim(), passwrd, email.Trim());
        }

        /// <summary>
        /// This Function check the user
        /// </summary>
        /// <param name="username"></param>
        /// <param name="empid"></param>
        /// <returns>Dataset with User records</returns>
        public DataSet CheckUser(string username, string empid)
        {
            string[] keys1 = { "@username", "@employeeid" };
            object[] values1 = { username.Trim(), empid };
            DataSet ds_check = ClsDb.GetDSBySPArr("USP_HTS_Check_users", keys1, values1);
            return ds_check;
        }

        /// <summary>
        ///         This function Update employee password
        /// </summary>
        /// <param name="newpassword"> new password which will be updated</param>
        /// <returns>Row count to chekc if record update or not</returns>
        /// 
        // Noufil 4378 07/23/2008 This function update the employee password
        public bool ChangeEmployeePassword(string newpassword)
        {
            string[] keys = { "@empid", "@oldpasspword", "@newpassword" };
            object[] values = { empID, password, newpassword };
            return Convert.ToBoolean(Convert.ToInt32(ClsDb.ExecuteScalerBySP("USP_HTP_ChangeEmployeePassword", keys, values).ToString()));
        }

        /// <summary>
        /// This function is used to update user password in support ticket system.
        /// </summary>
        /// <param name="userName">User Name</param>
        /// <param name="password">Password</param>
        /// <param name="email">User Email Address</param> 
        /// <returns>Return true if password update successfully</returns>
        public bool UpdateTicketDeskPassword(string userName, string password, string email)
        {
            //Zeeshan Ahmed 4703 08/28/2008 Add Function
            string[] keys = { "@UserName", "@Password", "@SiteId", "@Email" };
            object[] values = { userName + "-HTP", password, 3, email };
            return Convert.ToBoolean(Convert.ToInt32(ClsTicketDesk.ExecuteScalerBySP("USP_TD_Update_UserPassword", keys, values)));
        }

    }
}

using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections;
//TODO:Use New Namespace
using FrameWorkEnation.Components;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;


namespace HTP.Components
{
    //Zeeshan Ahmed 3486 04/04/2008

    /// <summary>
    /// Contains All Logic Related To Docket Close Out Report
    /// </summary>
    public class clsDocketCloseOut
    {

        int ticketviolationid = 0;
        clsENationWebComponents clsDB = new clsENationWebComponents();
        clsCrsytalComponent batchRpt = new clsCrsytalComponent();
        private clsLogger cLog = new clsLogger();

        public int TicketViolationId
        {
            get { return ticketviolationid; }
            set { ticketviolationid = value; }
        }

        int empid = 0;

        public int EmpId
        {
            get { return empid; }
            set { empid = value; }
        }
        //Zeeshan 4163 06/03/2008 Add Missed Court Type Property
        BatchLetterType missedCourtType;

        public BatchLetterType MissedCourtType
        {
            get { return missedCourtType; }
            set { missedCourtType = value; }
        }

        /// <summary>
        /// This Method set status to missed court
        /// </summary>
        public void SetMissedCourtStatus()
        {
            //Zeeshan 4163 06/03/2008 Save Missed Court Status Type
            string[] keys = { "@TicketsViolationID", "@EmpID", "@MissedCourtType" };
            object[] values = { ticketviolationid, empid, missedCourtType };
            clsDB.ExecuteSP("USP_HTS_DOCKETCLOSEOUT_UPDATE_MissedCourt_Selected", keys, values);
        }

        /// <summary>
        ///This Function Send Missed Court Cases To Batch
        /// </summary>
        /// <param name="TicketIds">Case Numbers</param>
        /// <param name="batchLetterType">Batch Letter Type</param>
        public void SendMissedCourtLetterToBatch(Hashtable TicketIds, BatchLetterType batchLetterType)
        {
            string ReportName = "";
            string ProcedureName = "";

            //Set Report Name and Procedure
            switch (batchLetterType)
            {
                case BatchLetterType.MissedCourtLetter:
                    ReportName = "\\MissedCourtLetter.rpt";
                    ProcedureName = "USP_HTP_MissedCourt_Letter";

                    break;
                case BatchLetterType.PledOutLetter:
                    ReportName = "\\PledOutLetter.rpt";
                    ProcedureName = "USP_HTP_PledOut_Letter";
                    break;
            }
            //Send Letters To Batch
            foreach (DictionaryEntry key in TicketIds)
            {
                //Zeeshan 4163 06/03/2008 Remove Missed Court Letter From 
                clsBatch batch = new clsBatch();
                //batch.DeleteLetterFromBatch((batchLetterType == BatchLetterType.MissedCourtLetter ? BatchLetterType.PledOutLetter : BatchLetterType.MissedCourtLetter), Convert.ToInt32(key.Value), empid);

                string[] keys = { "@TicketID" };
                object[] value1 = { key.Value };
                string filename = HttpContext.Current.Server.MapPath("..") + "\\Reports" + ReportName;
                batchRpt.CreateReportForEntry(filename, ProcedureName, keys, value1, "true", Convert.ToInt32(batchLetterType), empid, HttpContext.Current.Session, HttpContext.Current.Response);
            }
        }


        /// <summary>
        /// Get Docket Records For Docket Close Out Report
        /// </summary>
        /// <param name="CourtDate">Court Date</param>
        /// <param name="CourtName">Court Name</param>
        /// <param name="Status">Case Status</param>
        /// <param name="ShowClosedDocket">Show Closed Docket</param>
        /// <param name="RedX">Display Records With Discrepencies</param>
        /// <param name="QuestionMark">Display Cases having discrepencies but makes a logical sense</param>
        /// <param name="YelloCheck">Display Cases having auto and verified status match but court date is in past</param>
        /// <param name="GreenCheck">Display Records having no Discrepencies</param>
        /// <returns></returns>
        public DataSet GetDocketCloseOutReport(DateTime CourtDate, string CourtName, string Status, int ShowClosedDocket, bool RedX, bool QuestionMark, bool YelloCheck, bool GreenCheck)
        {
            string[] keys = { "@CourtDate", "@CourtName", "@Status", "@ShowClosedDocket", "@ShowRedX", "@ShowQuestionMark", "@ShowYellowCheck", "@ShowGreenCheck" };
            object[] values = { CourtDate, CourtName, Status, ShowClosedDocket, RedX, QuestionMark, YelloCheck, GreenCheck };
            return clsDB.Get_DS_BySPArr("USP_HTS_DOCKETCLOSEOUT_Filter_RECORDS", keys, values);
        }

        /// <summary>
        /// This method returns all pending tickets(Cases) having Auto Status in DLQ/Warrant.
        /// </summary>
        /// <returns></returns>
        public DataTable GetAllPendingTicket()
        {
            return clsDB.Get_DT_BySPArr("USP_HTP_PendingTicket");
        }

        /// <summary>
        /// Zeeshan Haider
        /// Date: 06/17/2013
        /// Task: 11008
        /// This method removes existing trial letter in batch of a given ticket.
        /// </summary>
        /// <param name="TicketID"></param>
        public void RemoveExistingTrialLetterInBatch(int TicketID)
        {
            try
            {
                string[] keys = { "@TicketID" };
                object[] values = { TicketID };
                clsDB.ExecuteSP("USP_HTS_BATCHLETTERS_DELETE_EXISTING", keys, values);
            }
            catch (Exception ex)
            {
                cLog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        /// <summary>
        /// Zeeshan Haider
        /// Date: 06/17/2013
        /// Task: 11008
        /// This method sends trial letter to batch of a given ticket.
        /// </summary>
        /// <param name="EmpID"></param>
        /// <param name="TicketID"></param>
        public void SendTrialLetterToBatch(int TicketID, int EmpID)
        {
            try
            {
                string[] keys = { "@TicketID_FK", "@BatchDate", "@PrintDate", "@IsPrinted", "@LetterID_FK", "@EmpID", "@DocPath" };
                object[] values = { TicketID, DateTime.Now, "1/1/1900", 0, 2, EmpID, "" };
                clsDB.InsertBySPArr("USP_HTS_Insert_BatchPrintLetter", keys, values);
            }
            catch (Exception ex)
            {
                cLog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
    }
}

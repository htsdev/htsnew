using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

//****************************************Class Writtten by Azwar Alam***********************************************
//****************************************For Financial Report*******************************************************

//Parameters Updated by Agha Usman Task Id 3210 - 02/23/2008
namespace lntechNew.Components.ClientInfo
{
    public class FinancialReports
    {
        clsENationWebComponents ClsDb = new clsENationWebComponents("Connection String");
        clsLogger bugTracker = new clsLogger();
        clsSession ClsSession = new clsSession();
        #region Variables

        private DateTime _recFromDate;
        private DateTime _recToDate;
        private int _FirmID;
        private DataSet _ds = null;
        private DataTable _dt = null;

        private object[] _values;


        #endregion

        #region Properties

        public DateTime RecFromDate
        {
            get { return _recFromDate; }
            set { _recFromDate = value; }
        }
        public DateTime RecToDate
        {
            get { return _recToDate; }
            set { _recToDate = value; }
        }
        public int FirmID
        {
            get { return _FirmID; }
            set { _FirmID = value; }
        }
        public object[] Values
        {
            get
            {
                return _values;
            }
            set
            {
                if (value == null || value.Length != 4)
                {
                    throw new ArgumentException("Parameter Value may not be null or blank or length of Parameter is not Equal to 2");
                }
                _values = value;
            }
        }
        public DataSet DS
        {
            get
            {
                return _ds;
            }
            set
            {
                _ds = value;
            }
        }
        public DataTable DT
        {
            get
            {
                return _dt;
            }
            set
            {
                _dt = value;
            }
        }


        #endregion

        #region Methods

        public DataSet GetData()
        {
            try
            {


                string[] keys = { "@RecDate", "@RecToDate", "@firmId", "@BranchId" };

                DS = ClsDb.Get_DS_BySPArr("usp_Get_All_VoidPaymentRecords", keys, Values);

                return DS;


            }
            catch
            {
                return DS;
            }
        }


        public DataSet GetAttorneyCredit()
        {
            try
            {


                string[] keys = { "@RecDate", "@RecToDate", "@firmId", "@BranchId" };

                DS = ClsDb.Get_DS_BySPArr("usp_Get_All_AttorneyCreditPayments", keys, Values);

                return DS;


            }
            catch
            {
                return DS;
            }
        }

        public DataSet GetFeeWeived()
        {
            try
            {

                // Sabir Khan 10920 05/27/2013 Branch ID added
                string[] keys = { "@RecDate", "@RecToDate", "@firmId", "@BranchId" };

                DS = ClsDb.Get_DS_BySPArr("usp_Get_CreditPayments", keys, Values);

                return DS;


            }
            catch
            {
                return DS;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DataTable GetDataForCriminalPaymentReport(int CoveringFirmId, DateTime FromDate, DateTime ToDate)
        {
            try
            {
                string[] keys = { "@CoveringFirmId", "@FromDate", "@ToDate" };
                DT = ClsDb.Get_DT_BySPArr("usp_HTP_Get_DataForCriminalPaymentReport", keys, Values);
                return DT;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return DT;
            }
        }

        //Sabir Khan 5418 01/26/2009
        ///<summary>
        ///This method will return Category Type by date
        ///</summary>
        ///<returns>It returns DataSet for all Category Type</returns>
        /// <param name="sDate"></param>
        /// <param name="eDate"></param>
        /// <param name="firmId"></param>
        public DataSet FillCategoryType()
        {
            try
            {
                // Sabir Khan 10920 05/27/2013 Branch ID added
                string[] key1 = { "@sDate", "@eDate", "@firmId", "@BranchId" };
                DS = ClsDb.Get_DS_BySPArr("USP_HTS_Get_FinReport_BalancePaid_Ver2", key1, Values);
                return DS;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return DS;
            }

        }
        //Sabir Khan 5418 01/26/2009
        ///<summary>
        ///This method will return Credit Type by date
        ///</summary>
        ///<returns>It returns DataSet for all Credit Type</returns>
        /// <param name="sDate"></param>
        /// <param name="eDate"></param>
        /// <param name="firmId"></param>
        public DataSet FillCreditType()
        {
            try
            {
                // Sabir Khan 10920 05/27/2013 Branch ID added
                string[] key1 = { "@sDate", "@eDate", "@firmId", "@BranchID" };
                DS = ClsDb.Get_DS_BySPArr("USP_HTS_Get_FinReport_CreditType", key1, Values);
                return DS;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return DS;
            }

        }
        //Sabir Khan 5418 01/26/2009
        ///<summary>
        ///This method will return Total payment type by date
        ///</summary>
        ///<returns>It returns DataSet for Total payment Type</returns>
        /// <param name="sDate"></param>
        /// <param name="eDate"></param>
        /// <param name="firmId"></param>
        public DataSet GetPaymentTypeSumByDate()
        {
            try
            {
                // Sabir Khan 10920 05/27/2013 Branch ID added
                string[] key1 = { "@RecDate", "@RecTo", "@firmId", "@BranchID" };
                DS = ClsDb.Get_DS_BySPArr("usp_Get_All_PaymentSumByRecDaterange", key1, Values);
                return DS;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return DS;
            }

        }
        //Sabir Khan 5418 01/26/2009
        ///<summary>
        ///This method will return Court Summary by date
        ///</summary>
        ///<returns>It returns DataSet for Court Summary</returns>
        /// <param name="sDate"></param>
        /// <param name="eDate"></param>
        /// <param name="firmId"></param>
        public DataSet GetCourtSummary()
        {
            try
            {
                // Sabir Khan 10920 05/27/2013 Branch ID added
                string[] key1 = { "@RecDate", "@RecTo", "@firmId", "@BranchID" };
                DS = ClsDb.Get_DS_BySPArr("usp_Get_All_CourtInfoByRecDateRange", key1, Values);
                return DS;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return DS;
            }

        }        
    
        
        public void DeletePaymentDetailByDate(DateTime calQueryDate, int branchId)
        {
            // Sabir Khan 10920 05/27/2013 Branch ID added
            string[] keys = { "@TransDate", "@BranchID" };
            object[] values = { calQueryDate, branchId };

            ClsDb.ExecuteSP("usp_Del_tblPaymentDetailWeeklyByDate", keys, values);
        }

        public void InsertPaymentDetailByDate(int EmployeeID, DateTime calQueryDate, decimal dCash, decimal dCheck , string Note, int branchId)
        {
            // Sabir Khan 10920 05/27/2013 Branch ID added
            string[] keys = { "@EmployeeID", "@TransDate", "@ActualCash", "@ActualCheck", "@Notes", "@branchID" };
            object[] values = { EmployeeID, calQueryDate, dCash, dCheck, Note, branchId };
            ClsDb.InsertBySPArr("usp_Add_tblPaymentDetailWeekly", keys, values);
            //ClsDb.InsertBySP("usp_Add_tblPaymentDetailWeekly", "EmployeeID", EmployeeID, "TransDate", calQueryDate, "ActualCash", dCash, "ActualCheck", dCheck, "Notes", Note, "branchID", branchId);
        }

        //Waqas 5110 01/22/2009
        ///<summary>
        ///This method will return payment details by date
        ///</summary>
        ///<returns>It returns DataSet for all payment details</returns>
        /// <param name="QueryDate"></param>
        /// <param name="QueryDateTo"></param>
        public DataSet GetPaymentDetailByDateByRep(DateTime QueryDate, DateTime QueryDateTo, int branchId)
        {
            try
            {
                // Sabir Khan 10920 05/27/2013 Branch ID added
                string[] key1 = { "@RecDate", "@RecDateTo", "@BranchID" };
                object[] value1 = { QueryDate, QueryDateTo, branchId };
                DataSet dsPaymentInfo = ClsDb.Get_DS_BySPArr("usp_Get_All_PaymentDetailByRecDateRange", key1, value1);
                return dsPaymentInfo;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                throw new Exception(ex.Message);
            }
        }

        ///<summary>
        ///This method will return Weekly payment details by date
        ///</summary>
        ///<returns>It returns DataSet for all weekly payment details</returns>
        /// <param name="QueryDate"></param>
        public DataSet GetWeeklyPaymentDetailByDateByRep(DateTime QueryDate, int branchId)
        {
            try
            {
                // Sabir Khan 10920 05/27/2013 Branch ID added
                DataSet dsWeeklyPaymentDetailsByRep = ClsDb.Get_DS_BySPByTwoParmameter("usp_Get_All_tblPaymentDetailWeeklyByDate", "TransDate", QueryDate, "branchID", branchId);
                return dsWeeklyPaymentDetailsByRep;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                throw new Exception(ex.Message);
            }
        }

        ///<summary>
        ///This method will return payment summary by date
        ///</summary>
        ///<returns>It returns DataSet for all  payment summary </returns>
        /// <param name="QueryDate"></param>
        public DataSet GetPaymentSummaryByDate(DateTime QueryDate)
        {
            try
            {
                DataSet dsPaymentInfo = ClsDb.Get_DS_BySPByOneParmameter("usp_Get_All_PaymentInfoByRecDate", "RecDate", QueryDate);
                return dsPaymentInfo;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                throw new Exception(ex.Message);
            }
        }

        ///<summary>
        ///This method will return payment details of credit cards following parameters.
        ///</summary>
        ///<returns>It returns DataSet for all payment summary </returns>
        /// <param name="QueryDate"></param>
        /// <param name="QueryDateTo"></param>
        /// <param name="RepID"></param>
        /// <param name="PayID"></param>
        /// <param name="CourtID"></param>        
        public DataSet GetPaymentDetailsForCreditCards(DateTime QueryDate, DateTime QueryDateTo, int RepID, int PayID, int CourtID, int branchId)
        {
            try
            {
                // Sabir Khan 10920 05/27/2013 Branch ID added
                string[] key1 = { "@RecDate", "@RecDateTo", "@EmployeeID", "@PaymentType", "@CourtID", "@BranchID" };
                object[] value1 = { QueryDate, QueryDateTo, RepID, PayID, CourtID, branchId };
                DataSet dsFinReport = ClsDb.Get_DS_BySPArr("usp_Get_All_PaymentDetailOfCCByCriteriaRange", key1, value1);
                return dsFinReport;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                throw new Exception(ex.Message);
            }
        }

        ///<summary>
        ///This method will return all payment details with specified criteria
        ///</summary>
        ///<returns>It returns DataSet for all payment details </returns>
        /// <param name="QueryDate"></param>
        /// <param name="QueryDateTo"></param>
        /// <param name="RepID"></param>
        /// <param name="PayID"></param>
        /// <param name="CourtID"></param>        
        public DataSet GetPaymentDetailsForByCriteriaRange(DateTime QueryDate, DateTime QueryDateTo, int RepID, int PayID, int CourtID, int branchId)
        {
            try
            {
                // Sabir Khan 10920 05/27/2013 Branch ID added
                string[] key1 = { "@RecDate", "@RecDateTo", "@EmployeeID", "@PaymentType", "@CourtID", "@BranchID" };
                object[] value1 = { QueryDate, QueryDateTo, RepID, PayID, CourtID, branchId };
                DataSet dsFinReport = ClsDb.Get_DS_BySPArr("usp_Get_All_PaymentDetailByCriteriaRange", key1, value1);
                return dsFinReport;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                throw new Exception(ex.Message);
            }
        }

        ///<summary>
        ///This method will return payment type summary by date
        ///</summary>
        ///<returns>It returns DataSet for payment type summary</returns>
        /// <param name="QueryDate"></param>
        /// <param name="QueryDateTo"></param>
        public DataSet GetAllPaymentTypeSumByDate(DateTime QueryDate, DateTime QueryDateTo, int branchId)
        {
            try
            {
                // Sabir Khan 10920 05/27/2013 Branch ID added
                string[] key1 = { "@RecDate", "@RecTo", "@BranchID" };
                object[] value1 = { QueryDate, QueryDateTo, branchId };
                DataSet dsPaymentInfo = ClsDb.Get_DS_BySPArr("usp_Get_All_PaymentSumByRecDateRange", key1, value1);
                return dsPaymentInfo;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                throw new Exception(ex.Message);
            }
        }

        ///<summary>
        ///This method will return Court Information by date ranges
        ///</summary>
        ///<returns>It returns DataSet for Court Information </returns>
        /// <param name="QueryDate"></param>
        /// <param name="QueryDateTo"></param>
        public DataSet GetAllCourtsInfoByDateRange(DateTime QueryDate, DateTime QueryDateTo, int branchId)
        {
            try
            {
                // Sabir Khan 10920 05/27/2013 Branch ID added
                string[] key1 = { "@RecDate", "@RecTo", "@BranchID" };
                object[] value1 = { QueryDate, QueryDateTo, branchId };
                DataSet dsPaymentInfo = ClsDb.Get_DS_BySPArr("usp_Get_All_CourtInfoByRecDateRange", key1, value1);
                return dsPaymentInfo;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                throw new Exception(ex.Message);
            }
        }

        ///<summary>
        ///This method will return Mailer Summary.
        ///</summary>
        ///<returns>It returns DataSet for Mailer summary </returns>
        /// <param name="QueryDate"></param>
        /// <param name="QueryDateTo"></param>
        /// <param name="RepID"></param>
        /// <param name="PayID"></param>
        /// <param name="CourtID"></param>        
        public DataSet GetAllMailerSummary(DateTime QueryDate, DateTime QueryDateTo, int RepID, int PayID, int CourtID, int branchId)
        {
            try
            {
                // Sabir Khan 10920 05/27/2013 Branch ID added
                string[] key1 = { "@RecDate", "@RecDateTo", "@EmployeeID", "@PaymentType", "@CourtID", "@BranchID" };
                object[] value1 = { QueryDate, QueryDateTo, RepID, PayID, CourtID, branchId };
                //Sabir Khan 6047 06/25/2009 sp has been saparate for Mailer summary from transaction detail....
                DataSet dsFinReport = ClsDb.Get_DS_BySPArr("USP_HTP_Get_MailerSummaryDetail", key1, value1);
                return dsFinReport;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                throw new Exception(ex.Message);
            }
        }

        ///<summary>
        ///This method will return payment detail report by court id 
        ///</summary>
        ///<returns>It returns DataSet for payment detail  </returns>
        /// <param name="QueryDate"></param>
        /// <param name="CourtID"></param>
        public DataSet GetPaymentDetailByCourtID(DateTime QueryDate, string CourtId)
        {
            try
            {
                DataSet dsFinReport = ClsDb.Get_DS_BySPByTwoParmameter("usp_Get_All_PaymentDetailByCourtID", "RecDate", QueryDate, "CourtID", CourtId);
                return dsFinReport;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                throw new Exception(ex.Message);
            }
        }

        ///<summary>
        ///This method will return courts  
        ///</summary>
        ///<returns>It returns DataSet for courts  </returns>
        public DataSet GetAllCourtsForCloseOut()
        {
            try
            {
                DataSet dsCourts = ClsDb.Get_DS_BySP("usp_Get_All_Court");
                return dsCourts;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                throw new Exception(ex.Message);
            }
        }

        ///<summary>
        ///This method will return all rep list by date criteria
        ///</summary>
        ///<returns>It returns DataSet for rep list</returns>
        /// <param name="QueryDate"></param>
        /// <param name="QueryDateTo"></param>
        public DataSet GetAllRepList(DateTime QueryDate, DateTime QueryDateTo)
        {
            try
            {
                string[] key1 = { "@sDate", "@eDate" };
                object[] value1 = { QueryDate, QueryDateTo };
                DataSet dsRepList = ClsDb.Get_DS_BySPArr("usp_Get_All_RepList", key1, value1);
                return dsRepList;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                throw new Exception(ex.Message);
            }
        }

        ///<summary>
        ///This method will return Payment Types  
        ///</summary>
        ///<returns>It returns DataSet for Payment Types  </returns>
        public DataSet GetPaymentTypeForCloseOut()
        {
            try
            {
                DataSet dsPaymentType = ClsDb.Get_DS_BySP("usp_Get_All_CCType");
                return dsPaymentType;

            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                throw new Exception(ex.Message);
            }
        }

        ///<summary>
        ///This method will delete payment details by date
        ///</summary>
        ///<returns>It returns true if deletion performed successfully </returns>
        /// <param name="TransDate"></param>
        public bool DeleteWeeklyPaymentDetailsByDate(DateTime TransDate, int branchId)
        {

            try
            {
                // Sabir Khan 10920 05/27/2013 Branch ID added
                string[] key1 = { "@TransDate", "@branchID" };
                object[] value1 = { TransDate, branchId };
                ClsDb.ExecuteSP("usp_Del_tblPaymentDetailWeeklyByDate", key1, value1);
                return true;

            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                throw new Exception(ex.Message);
            }
        }

        ///<summary>
        ///This method will insert payment details 
        ///</summary>
        ///<returns>It returns true on insertion successfully and false otherwise </returns>
        /// <param name="EmployeeID"></param>
        /// <param name="TransDate"></param>
        /// <param name="Cash"></param>
        /// <param name="Check"></param>
        /// <param name="Notes"></param>
        public bool InsertWeeklyPaymentDetails(int EmployeeID, DateTime TransDate, decimal Cash, decimal Check, string Notes, int branchId)
        {

            try
            {
                // Sabir Khan 10920 05/27/2013 Branch ID added
                string[] key1 = { "EmployeeID", "@TransDate", "ActualCash", "ActualCheck", "Notes", "@branchID" };
                object[] value1 = { EmployeeID, TransDate, Cash, Check, Notes, branchId };
                ClsDb.InsertBySPArr("usp_Add_tblPaymentDetailWeekly", key1, value1);
                return true;

            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                throw new Exception(ex.Message);
            }
        }

        ///<summary>
        ///This method will return Email Addresses
        ///</summary>
        ///<returns>It returns DataSet having email addresses</returns>
        public DataSet GetEmailSetting()
        {
            try
            {
                DataSet dsCourts = ClsDb.Get_DS_BySP("usp_Get_All_EMailSetting");
                return dsCourts;

            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// Get Branch Info
        /// Task ID: 10920
        /// Created by: Sabir Khan
        /// Date: 05/27/2013 
        /// </summary>
        /// <param name="Request">HttpRequest Request</param>
        /// <returns>Return DataTable of Branches.</returns>
        public DataTable GetBranchInfo(HttpRequest Request)
        {
            string localmachinename = "";
            DataTable dtBranches = ClsDb.Get_DT_BySPArr("USP_HTP_Get_BranchesName");
            DataTable dtBranchInfo = new DataTable();
            dtBranchInfo.Columns.Add("BranchId");
            dtBranchInfo.Columns.Add("BranchName");
            localmachinename = ClsSession.GetCookie("BranchName", Request);
            if (dtBranches.Rows.Count > 0)
            {
                foreach (DataRow dr in dtBranches.Rows)
                {
                    //Ozair 8376 10/14/2010 ignore case implemented for current default culture
                    if (localmachinename.StartsWith(dr["BranchName"].ToString(), true, null))
                    {
                        dtBranchInfo.Rows.Add(Convert.ToInt32(dr["BranchId"]), dr["BranchName"].ToString());
                    }

                }
                //Asad Ali 7755 06/14/2010 setting Default Branch to 2 i.e Dallas 
                if (dtBranchInfo.Rows.Count == 0)
                {
                    dtBranchInfo.Rows.Add(2, "Houston 2020");
                }

            }
            else
            {
                //Asad Ali 7755 06/14/2010 setting Default Branch to 2 i.e Dallas 
                dtBranchInfo.Rows.Add(2, "Houston 2020");
            }
            return dtBranchInfo;
        }
        #endregion


    }
    
}

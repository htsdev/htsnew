﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using FrameWorkEnation.Components;
using lntechNew.Components;

namespace lntechNew.Components.ClientInfo
{
    public class ClsJuryOutCome
    {
        clsENationWebComponents clsDB = new clsENationWebComponents();

        public DataSet GetJuryOutComeReport(DateTime DocketDate)
        {
            string[] keys = { "@DocketDate"};
            object[] values = {DocketDate};
            return clsDB.Get_DS_BySPArr("USP_HTP_Get_JuryOutCome", keys, values);
        }
    }
}

﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using FrameWorkEnation.Components;


namespace lntechNew.Components.ClientInfo
{
    ///<summary>
    ///</summary>
    public class ClsNisiCaseHandler
    {
        private clsENationWebComponents _clsDb;
        private DataTable _dt;
        readonly clsLogger _bugTracker;
        private DataSet _ds;
        ///<summary>
        /// Empty Contructore
        ///</summary>
        public ClsNisiCaseHandler ()
        {
            _clsDb = new clsENationWebComponents();
            _dt = new DataTable();
            _bugTracker = new clsLogger();
            _ds = new DataSet();
        }

        ///<summary>
        /// Use to get the Data from DB for NISI Clienst.
        ///</summary>
        ///<returns>Data Table</returns>
        public DataTable GetNisiDataForReport (int isActive, string status)
        {
            try
            {
                string[] key = { "@isActive", "@status" };
                object[] value = { isActive, status };

                return _clsDb.Get_DT_BySPArr("USP_HTP_GET_NISI_ClientsInfo", key, value);
            }
            catch (Exception ex)
            {
                _bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return _dt;
            }
        }

        ///<summary>
        ///</summary>
        ///<param name="rowIds"></param>
        ///<param name="employeeId"></param>
        ///<returns></returns>
        public DataSet GetSelectClientsToPrintLetters(string rowIds, int employeeId, bool isPrintedFromPrintAnswerButton, string filePath)
        {
            try
            {
                string[] keys = { "@RowId", "@EmployeeId", "@btn_printAnswerClicked", "@filePath" };
                object[] values = { rowIds, employeeId, isPrintedFromPrintAnswerButton, filePath };
                return _clsDb.Get_DS_BySPArr("USP_HTP_PrintAnswers_For_NisiCases", keys, values);
            }
            catch (Exception ex)
            {
                _bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return _ds;
            }
        }


        ///<summary>
        /// Used to get the records to print the answers of NISI for clients
        ///</summary>
        ///<param name="ticketId">Ticket ID</param>
        ///<param name="employeeId"></param>
        ///<returns>Data Set</returns>
        public DataSet GetClientLettersToPrint(int ticketId, int employeeId, string multiNisi, string MultiUnderlyingCause)
        {
            try
            {
                string[] keys = { "@TicketId", "@employeeId", "@multiNisi", "@multiUnderlyingCause" };
                object[] values = { ticketId, employeeId, multiNisi, MultiUnderlyingCause};
                return _clsDb.Get_DS_BySPArr("USP_HTS_Get_NisiCase_ClientLetters", keys, values);
            }
            catch (Exception ex)
            {
                _bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return _ds;
            }
        }
        ///<summary>
        /// Used to add history note for nisi client latter
        ///</summary>
        ///<param name="ticketId">Ticket ID</param>
        ///<param name="employeeId"></param>
        ///<returns>Data Set</returns>
        public void InsertHistoryNoteForNisiCllientLatter(int ticketId, int employeeId, string filepath)
        {
            try
            {
                string[] keys = { "@TicketID", "@EmployeeId", "@FilePath" };
                object[] values = { ticketId, employeeId, filepath };
                _clsDb.ExecuteScalerBySp("USP_HTP_InsertHistoryNoteForNISIClientLetter", keys, values);
            }
            catch (Exception ex)
            {
                _bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
    }
}

using System;
using System.Data;
using FrameWorkEnation.Components;

namespace lntechNew.Components.ClientInfo
{
    /// <summary>
    /// Summary description for clsPayments.
    /// </summary>
    public class clsPayments
    {
        //Create an Instance of Data Access Class
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        clsCase ClsCase = new clsCase();
        clsCreditCard clscard = new clsCreditCard();
        clsLogger bugTracker = new clsLogger();
        #region Variables

        private int invoiceNumber = 0;
        private int ticketID = 0;
        private int paymentType = 0;
        private int empID = 0;
        private int paymentVoid = 0;
        private int voidInitial = 0;
        private int pretrialStatus = 0;
        private int scheduleid = 0;  //newly added  
        private int processflag = 0;//added to track the process scenerio
        private int mainStatus = 0;
        private string cardType = String.Empty;
        private string cardHolderName = String.Empty;
        private string cardNumber = String.Empty;
        private string cin = String.Empty;
        private string authCode = String.Empty;
        private string refNumber = String.Empty;
        private string certifiedMail = String.Empty;
        private string paymentsComments = String.Empty;
        private string cardExpirationDate = String.Empty;
        private double chargeAmount = 0;
        private double totalfee = 0;
        private double owes = 0;
        private DateTime recDate = DateTime.Now;
        private DateTime dueDate = DateTime.Now;
        private DateTime processDate = DateTime.Now;
        //added by ozair for andrew's modification point 2 
        private int toProcess = 0;
        //
        #endregion

        #region Properties

        //added by ozair for andrew's modification point 2 
        public int ToProcess
        {
            get
            {
                return toProcess;
            }
            set
            {
                toProcess = value;
            }
        }
        //

        public int Scheduleid
        {
            get
            {
                return scheduleid;
            }
            set
            {
                scheduleid = value;
            }
        }

        public int Processflag
        {
            get
            {
                return processflag;
            }
            set
            {
                processflag = value;
            }
        }

        public int InvoiceNumber
        {
            get
            {
                return invoiceNumber;
            }
            set
            {
                invoiceNumber = value;
            }
        }


        public int PreTrialStatus
        {
            get
            {
                return pretrialStatus;
            }
            set
            {
                pretrialStatus = value;
            }
        }

        public int TicketID
        {
            get
            {
                return ticketID;
            }
            set
            {
                ticketID = value;
            }
        }

        public int PaymentType
        {
            get
            {
                return paymentType;
            }
            set
            {
                paymentType = value;
            }
        }

        public DateTime RecDate
        {
            get
            {
                return recDate;
            }
            set
            {
                recDate = value;
            }
        }

        public string RefNumber
        {
            get
            {
                return refNumber;
            }
            set
            {
                refNumber = value;
            }
        }

        public int MainStatus
        {
            get
            {
                return mainStatus;
            }
            set
            {
                mainStatus = value;
            }
        }

        public DateTime DueDate
        {
            get
            {
                return dueDate;
            }
            set
            {
                dueDate = value;
            }
        }

        public string CardNumber
        {
            get
            {
                return cardNumber;
            }
            set
            {
                cardNumber = value;
            }
        }

        public string CardExpirationDate
        {
            get
            {
                return cardExpirationDate;
            }
            set
            {
                cardExpirationDate = value;
            }
        }


        public string PaymentsComments
        {
            get
            {
                return paymentsComments;
            }
            set
            {
                paymentsComments = value;
            }
        }

        public string Cin
        {
            get
            {
                return cin;
            }
            set
            {
                cin = value;
            }
        }
        public string AuthCode
        {
            get
            {
                return authCode;
            }
            set
            {
                authCode = value;
            }
        }


        public string CardHolderName
        {
            get
            {
                return cardHolderName;
            }
            set
            {
                cardHolderName = value;
            }
        }

        public string CertifiedMail
        {
            get
            {
                return certifiedMail;
            }
            set
            {
                certifiedMail = value;
            }
        }

        public double ChargeAmount
        {
            get
            {
                return chargeAmount;
            }
            set
            {
                chargeAmount = value;
            }
        }

        public double Totalfee
        {
            get
            {
                return totalfee;
            }
            set
            {
                totalfee = value;
            }
        }

        public double Owes
        {
            get
            {
                return owes;
            }
            set
            {
                owes = value;
            }
        }

        public int EmpID
        {
            get
            {
                return empID;
            }
            set
            {
                empID = value;
            }
        }

        public int PaymentVoid
        {
            get
            {
                return paymentVoid;
            }
            set
            {
                paymentVoid = value;
            }
        }

        public int VoidInitial
        {
            get
            {
                return voidInitial;
            }
            set
            {
                voidInitial = value;
            }
        }

        public string CardType
        {
            get
            {
                return cardType;
            }
            set
            {
                cardType = value;
            }
        }


        #endregion

        #region Methods


        /// <summary>
        /// Method To Process Payment
        /// </summary>		
        public bool ProcessPayment()
        {
            //object for transaction begin
            clsENationWebComponents clstran = new clsENationWebComponents(true);
            try
            {
                //InvoiceNumber= GetNewInvoiceNum();
                int invno = 0;
                int rowid = 0;
                //If Payment Type is not credit card
                if (PaymentType != 5)
                {
                    clstran.DBBeginTransaction();
                    string[] key1 ={"@TicketID",  "@PaymentType", "@ChargeAmount",
									  "@EmployeeID", "@scheduleid", "@processflag"};
                    object[] value1 ={ TicketID, PaymentType, ChargeAmount, EmpID, Scheduleid, Processflag };
                    clstran.ExecuteSP("USP_HTS_Process_Payment", key1, value1, 1);
                    clstran.DBTransactionCommit();
                }
                else
                {	//Only inserting in Credit Card Table
                    //In order to hide credit card number
                    //string tempcardnum=CardNumber.Substring(0,1)+"*****"+CardNumber.Substring(12,4);
                    string tempcardnum = "";
                    if (CardNumber.Length > 0)
                        tempcardnum = CardNumber.Substring(0, 1) + "*****" + CardNumber.Substring(CardNumber.Length - 4, 4);
                    clstran.DBBeginTransaction();
                    string[] key1 ={"@TicketID","@InvoiceNumber","@ChargeAmount","@EmployeeID","@CardNumber",
									  "@CardExpdate","@name","@cin","@cardtype", "@rowid"};
                    object[] value1 ={TicketID,InvoiceNumber,ChargeAmount,EmpID,tempcardnum,
										CardExpirationDate,CardHolderName,Cin,Convert.ToInt32(CardType), rowid};
                    //clstran.ExecuteSP("USP_HTS_Process_CreditCardPayment",key1,value1,1);
                    rowid = Convert.ToInt16(clstran.InsertBySPArrRet("USP_HTS_Process_CreditCardPayment", key1, value1));
                    clstran.DBTransactionCommit();

                    //To Validate/Process CreditCard   //ScheduleID for checking if normal payment is being paid or schedule payment
                    clscard.ProcessCreditCard(rowid, Scheduleid, CardNumber, Processflag); //providing cardno as it get hidden in DB

                }

                return true;
            }
            catch (Exception ex)
            {

                if (ex.Message.StartsWith("An error occured during processing.") || ex.Message.StartsWith("The transaction was not approved:"))
                {
                    throw new Exception(ex.Message);
                }
                else
                {
                    clstran.DBTransactionRollback();
                    bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

                    return false;
                }
            }
        }

        /// <summary>
        /// Information regarding Payment  
        /// </summary>
        /// <param name="TicketID">ID for which to get Payment Details</param>		
        public bool GetPaymentInfo(int TicketID)
        {
            try
            {
                string[] key1 ={ "@TicketID" };
                object[] value1 ={ TicketID };
                //ClsDb.DBBeginTransaction();
                DataSet ds = ClsDb.Get_DS_BySPArr("USP_HTS_GET_PAYMENTSINFO", key1, value1);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Totalfee = Convert.ToDouble(ds.Tables[0].Rows[0]["Totalfee"]);
                    string actflag = ds.Tables[0].Rows[0]["Activeflag"].ToString();

                    if (actflag == "1")
                    {
                        ChargeAmount = Convert.ToDouble(ds.Tables[0].Rows[0]["Paid"]);
                        Owes = Convert.ToDouble(ds.Tables[0].Rows[0]["Owes"]);
                    }
                    else
                    {
                        Owes = Totalfee;
                    }
                    CertifiedMail = ds.Tables[0].Rows[0]["CertifiedMailNumber"].ToString().Trim();
                    PreTrialStatus = Convert.ToInt32(ds.Tables[0].Rows[0]["PretrialStatus"]);
                    PaymentsComments = ds.Tables[0].Rows[0]["PayComments"].ToString().Trim();
                    //added by ozair for andrew's modification point 2 
                    ToProcess = Convert.ToInt32(ds.Tables[0].Rows[0]["toProcess"].ToString().Trim());
                    //
                    //	MainStatus=Convert.ToInt32(ds.Tables[0].Rows[0]["MainStatus"]);				
                    //ClsDb.DBTransactionCommit();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                //ClsDb.DBTransactionRollback();
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

                return false;
            }

        }
        /// <summary>
        /// Updates Payment Comments,Certified Mail, Pretrialstatus on Payment Info Page 
        /// </summary>		
        public bool UpdatePaymentInfo()
        {
            try
            {
                string[] key1 ={ "@TicketID", "@pretrialstatus", "@comments", "@certifiedmailnumber", "@Empid" };
                object[] value1 ={ TicketID, PreTrialStatus, PaymentsComments, CertifiedMail, EmpID };
                ClsDb.ExecuteSP("USP_HTS_Update_PaymentInfo", key1, value1);
                return true;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

                return false;
            }
        }

        /// <summary>
        /// Get DataSet For Schedule Payment Details 
        /// </summary>
        /// <param name="TicketID">ID for which to get Schedule Payment(s)</param>		
        public DataSet GetSchedulePaymentInfo(int TicketID)
        {
            string[] key1 ={ "@TicketID" };
            object[] value1 ={ TicketID };
            //DataSet ds=ClsDb.Get_DS_BySPArr("USP_HTS_Get_SchedulePaymentDetail",key1,value1);
            //return ds;
            return ClsDb.Get_DS_BySPArr("USP_HTS_Get_SchedulePaymentDetail", key1, value1);
        }

        /// <summary>
        /// Get DataSet For Payment Details 
        /// </summary>
        /// <param name="TicketID">ID for which to get Schedule Payment(s)</param>		
        public DataSet GetPaymentDetail(int TicketID)
        {
            //ClsDb.DBBeginTransaction();
            string[] key1 ={ "@TicketID" };
            object[] value1 ={ TicketID };
            //DataSet ds=ClsDb.Get_DS_BySPArr("USP_HTS_GET_PAYMENTSDetail",key1,value1);
            //ClsDb.DBTransactionCommit();
            //return ds;
            return ClsDb.Get_DS_BySPArr("USP_HTS_GET_PAYMENTSDetail", key1, value1);

        }
        /// <summary>
        /// Calls when To void the transaction  
        /// </summary>		
        /// <param name="InvoiceNum">Provide the invoice num of the payment</param>
        public bool VoidPayment(int InvoiceNum, string PayMethod)
        {
            try
            {
                string[] key2 = { "@invoicenumber" };
                object[] value2 = { InvoiceNum };
                string transnum = ClsDb.ExecuteScalerBySp("USP_HTS_Get_CreditCardTransNum", key2, value2).ToString();

                //if(PayMethod!="Credit Card")
                if (transnum.Length == 0)
                {
                    string[] key1 ={ "@invoicenumber", "@employeeid" };
                    object[] value1 ={ InvoiceNum, EmpID };
                    ClsDb.ExecuteSP("USP_HTS_Process_VoidTransaction", key1, value1);
                }
                else
                {
                    clscard.VoidCreditCardTransaction(InvoiceNum, EmpID);
                }
                return true;
            }
            catch (Exception ex)
            {
                if (ex.Message.StartsWith("An error occured during processing.") || ex.Message.StartsWith("The transaction was not approved:"))
                {
                    throw new Exception(ex.Message);
                }
                else
                {
                    bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

                    return false;
                }
            }
        }

        /// <summary>
        /// Returns Dataset Contanning Available CardType
        /// </summary>
        /// <returns></returns>
        public DataSet GetCardType()
        {
            //			ClsDb.DBBeginTransaction();
            //			DataSet ds=ClsDb.Get_DS_BySP("USP_HTS_Get_CardType");
            //			ClsDb.DBTransactionCommit();
            //			return ds;
            return ClsDb.Get_DS_BySP("USP_HTS_Get_CardType");
        }

        /// <summary>
        /// Returns DataSet Containing Available Payments Type 
        /// </summary>
        /// <returns></returns>
        public DataSet GetPaymentType()
        {
            //ClsDb.DBBeginTransaction();
            //DataSet ds=ClsDb.Get_DS_BySP("USP_HTS_Get_PaymentType");
            //ClsDb.DBTransactionCommit();
            //return ds;
            return ClsDb.Get_DS_BySP("USP_HTS_Get_PaymentType");
        }
        /// <summary>
        /// Add Schedule Payment
        /// </summary>
        /// <param name="dsPaymentInfo"></param>
        /// <returns></returns>
        public bool AddSchedulePayment(double Amount, DateTime Date, int Scheduleid)
        {
            try
            {
                //ClsDb.DBBeginTransaction();
                string[] key1 ={ "@TicketID", "Amount", "PayDate", "EmpID", "@Scheduleid" };
                object[] value1 ={ TicketID, Amount, Date, EmpID, Scheduleid };
                ClsDb.ExecuteSP("USP_HTS_Insert_Edit_SchedulePayment", key1, value1);
                //	ClsDb.DBTransactionCommit();
                return true;
            }
            catch (Exception ex)
            {
                //ClsDb.DBTransactionRollback();
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

                return false;
            }
        }
        /// <summary>
        /// Return New Invoice No
        /// </summary>		
        public int GetNewInvoiceNum()
        {
            try
            {
                int ret = 0;
                string[] key1 ={ "@InvoiceNumber" };
                object[] value1 ={ 0 };
                object obj = ClsDb.InsertBySPArrRet("USP_HTS_Generate_InvoiceNo", key1, value1);
                return ret = Convert.ToInt32(obj);
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

                return 0;
            }
        }

        /// <summary>
        /// Returns available credit card of the client
        /// </summary>		
        public DataSet GetCreditCardInfo()
        {
            string[] key1 ={ "@TicketID" };
            object[] value1 ={ TicketID };
            return ClsDb.Get_DS_BySPArr("USP_HTS_GET_CreditCardInfo", key1, value1);
        }
        /// <summary>
        /// When To Delete Schedule Payment
        /// </summary>
        /// <param name="scheduleid">scheduleID to make it inactive</param>
        public void InactiveSchedulePayment(int scheduleid)
        {
            try
            {
                string[] key1 ={ "@ScheduleID" };
                object[] value1 ={ scheduleid };
                ClsDb.ExecuteSP("USP_HTS_Inactive_SchedulePayment", key1, value1);

            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// Get schedule Amount of ID 
        /// </summary>
        /// <param name="scheduleid">ID to get amount</param>
        /// <returns>Schedule Amount</returns>
        /// 
        //Change by Ajmal
        //public System.Data.SqlClient.SqlDataReader GetScheduleAmount(int scheduleid)
        public IDataReader GetScheduleAmount(int scheduleid)
        {
            try
            {
                string[] key1 ={ "@ScheduleID" };
                object[] value1 ={ scheduleid };

                //object amount=ClsDb.ExecuteScalerBySp("USP_HTS_GET_Payment_By_ScheduleID",key1,value1);
                //System.Data.SqlClient.SqlDataReader dr= ClsDb.Get_DR_BySPArr("USP_HTS_GET_Payment_By_ScheduleID",key1,value1);

                //return dr;
                return ClsDb.Get_DR_BySPArr("USP_HTS_GET_Payment_By_ScheduleID", key1, value1);
            }
            catch (Exception ex)
            {
                //ClsDb.DBTransactionRollback();
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// For Batch Print Letter
        /// </summary>
        /// <returns>DataSet having Batch Print Records</returns>

        public DataSet GetBatchPrintInfo()
        {
            string[] keys = { "@TicketID" };
            object[] values = { TicketID };
            //DataSet ds_Print =   ClsDb.Get_DS_BySPArr("USP_HTS_GET_BATCH_PaymentInfo",keys,values);
            //return ds_Print;			
            return ClsDb.Get_DS_BySPArr("USP_HTS_GET_BATCH_PaymentInfo", keys, values);
        }


        #endregion

        public clsPayments()
        {
            //
            // TODO: Add constructor logic here
            //
        }
    }
}

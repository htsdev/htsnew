using System;
using System.Data;
using System.Data.SqlClient;
using FrameWorkEnation.Components;

namespace lntechNew.Components.ClientInfo
{
    /// <summary>
    /// Summary description for clsCaseDetail.
    /// </summary>
    public class clsCaseDetail : clsCase
    {
        clsCase ClsCase = new clsCase();
        //Create an Instance of Data Access Class
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        clsLogger bugTracker = new clsLogger();
        #region Variables
        private int ticketViolationID = 0;
        //Fahad 5299 02/10/2009 new added UpdateAll field 
        private bool updateAll = false;
        private string ticketViolationIDs = String.Empty;
        private int planID = 0;
        private bool updateMain = false;
        private bool bondFlagMain = false;
        private bool matchAutoVerified = false;
        //private int ticketID=0;
        private int violationNumber = 0;
        private int sequencNumber = 0;
        private double fineAmount = 0;
        private double bondAmount = 0;
        private string refCaseNumber = String.Empty;
        private string causeNumber = String.Empty;
        private string caseNumberAssignedByCourt = String.Empty;
        private int violationStatusID = 0;
        private string violationDescription = String.Empty;
        private DateTime courtDate = DateTime.Now;
        private string courtNumber = String.Empty;
        private int courtViolationStatusID = 0;
        private int automatedInsertFlag = 0;
        private DateTime updateDate = DateTime.Now;
        private int courtID = 0;
        private DateTime ticketViolationDate = DateTime.Now;
        private string ticketOfficerNumber = String.Empty;
        private DateTime courtDateMain = DateTime.Now;
        private int courtViolationStatusIDMain = 0;
        private string courtNumberMain = String.Empty;
        private DateTime courtDateScan = DateTime.Now;
        private int courtViolationStatusIDScan = 0;
        private string courtNumberScan = String.Empty;
        private DateTime scanUpdateDate = DateTime.Now;
        private DateTime verifiedStatusUpdateDate = DateTime.Now;
        private int verifiedEmpID = 0;
        private int scanDocID = 0;
        private int scanDocNumber = 0;
        private double dscProbationAmount = 0;
        private int dscProbationTime = 0;
        private DateTime dscProbationDate = DateTime.Now;
        private int timeToPay = 0;
        private DateTime timeToPayDate = DateTime.Now;
        private int isDSCSelected = 0;
        private int isRETSelected = 0;
        private int isMISCSelected = 0;
        private DateTime showCauseDate = DateTime.Now;
        private string miscComments = String.Empty;
        private int showcausecourtnumber = 0;
        private DateTime retdate = DateTime.Now;
        private int retcourtnumber = 0;
        private int isInsSelected = 0;
        private int coursecompletiondays = 0;
        private int drivingrecordsdays = 0;
        private int isShowCauseSelected = 0;
        private string CourtDetail; //added by asghar for oscare court
        private bool arrignmentwaitingdate;
        private bool bondwaitingdate;
        // Added By Zeeshan Ahmed for Criminal Courts
        private int chargelevel = 0;
        private string cdi = "";
        //Zeeshan Ahmed 3724 05/13/2008 Add Arrest Date Property 
        private DateTime arrestDate;

        public DateTime ArrestDate
        {
            get { return arrestDate; }
            set { arrestDate = value; }
        }

        //Zeeshan Ahmed 4163 06/02/2008 Add MissedCourtType Property
        private int missedCourtType = -1;

        public int MissedCourtType
        {
            get { return missedCourtType; }
            set { missedCourtType = value; }
        }

        #endregion

        #region Properties

        public int TicketViolationID
        {
            get
            {
                return ticketViolationID;
            }
            set
            {
                ticketViolationID = value;
            }
        }

        public string TicketViolationIDs
        {
            get
            {
                return ticketViolationIDs;
            }
            set
            {
                ticketViolationIDs = value;
            }
        }
        public int PlanID
        {
            get
            {
                return planID;
            }
            set
            {
                planID = value;
            }
        }
        public bool UpdateMain
        {
            get
            {
                return updateMain;
            }
            set
            {
                updateMain = value;
            }
        }
        //Fahad 5299 02/10/2009 new added UpdateAll field 
        public bool UpdateAll
        {
            get
            {
                return updateAll;
            }
            set
            {
                updateAll = value;
            }
        }

        public bool BondFlagMain
        {
            get
            {
                return bondFlagMain;
            }
            set
            {
                bondFlagMain = value;
            }
        }

        public bool MatchAutoVerified
        {
            get
            {
                return matchAutoVerified;
            }
            set
            {
                matchAutoVerified = value;
            }
        }

        /*public int TicketID
        {
            get
            {
                return ticketID;
            }
            set
            {
                ticketID = value;
            }
        }*/

        public int ViolationNumber
        {
            get
            {
                return violationNumber;
            }
            set
            {
                violationNumber = value;
            }
        }

        public int SequencNumber
        {
            get
            {
                return sequencNumber;
            }
            set
            {
                sequencNumber = value;
            }
        }

        public double FineAmount
        {
            get
            {
                return fineAmount;
            }
            set
            {
                fineAmount = value;
            }
        }

        public double BondAmount
        {
            get
            {
                return bondAmount;
            }
            set
            {
                bondAmount = value;
            }
        }

        public string RefCaseNumber
        {
            get
            {
                return refCaseNumber;
            }
            set
            {
                refCaseNumber = value;
            }
        }
        public string CauseNumber
        {
            get
            {
                return causeNumber;
            }
            set
            {
                causeNumber = value;
            }
        }

        public string CaseNumberAssignedByCourt
        {
            get
            {
                return caseNumberAssignedByCourt;
            }
            set
            {
                caseNumberAssignedByCourt = value;
            }
        }

        public int ViolationStatusID
        {
            get
            {
                return violationStatusID;
            }
            set
            {
                violationStatusID = value;
            }
        }

        public string ViolationDescription
        {
            get
            {
                return violationDescription;
            }
            set
            {
                violationDescription = value;
            }
        }

        public DateTime CourtDate
        {
            get
            {
                return courtDate;
            }
            set
            {
                courtDate = value;
            }
        }
        public bool ArrignmentWaitingDate
        {
            get
            {
                return arrignmentwaitingdate;
            }
            set
            {
                arrignmentwaitingdate = value;
            }
        }
        public bool BondWaitingDate
        {
            get
            {
                return bondwaitingdate;
            }
            set
            {
                bondwaitingdate = value;
            }
        }
        public string CourtNumber
        {
            get
            {
                return courtNumber;
            }
            set
            {
                courtNumber = value;
            }
        }

        public int CourtViolationStatusID
        {
            get
            {
                return courtViolationStatusID;
            }
            set
            {
                courtViolationStatusID = value;
            }
        }

        public int AutomatedInsertFlag
        {
            get
            {
                return automatedInsertFlag;
            }
            set
            {
                automatedInsertFlag = value;
            }
        }

        public DateTime UpdateDate
        {
            get
            {
                return updateDate;
            }
            set
            {
                updateDate = value;
            }
        }

        public int CourtID
        {
            get
            {
                return courtID;
            }
            set
            {
                courtID = value;
            }
        }

        public DateTime TicketViolationDate
        {
            get
            {
                return ticketViolationDate;
            }
            set
            {
                ticketViolationDate = value;
            }
        }

        public string TicketOfficerNumber
        {
            get
            {
                return ticketOfficerNumber;
            }
            set
            {
                ticketOfficerNumber = value;
            }
        }

        public DateTime CourtDateMain
        {
            get
            {
                return courtDateMain;
            }
            set
            {
                courtDateMain = value;
            }
        }

        public int CourtViolationStatusIDMain
        {
            get
            {
                return courtViolationStatusIDMain;
            }
            set
            {
                courtViolationStatusIDMain = value;
            }
        }

        public string CourtNumberMain
        {
            get
            {
                return courtNumberMain;
            }
            set
            {
                courtNumberMain = value;
            }
        }

        public DateTime CourtDateScan
        {
            get
            {
                return courtDateScan;
            }
            set
            {
                courtDateScan = value;
            }
        }

        public int CourtViolationStatusIDScan
        {
            get
            {
                return courtViolationStatusIDScan;
            }
            set
            {
                courtViolationStatusIDScan = value;
            }
        }

        public string CourtNumberScan
        {
            get
            {
                return courtNumberScan;
            }
            set
            {
                courtNumberScan = value;
            }
        }

        public DateTime ScanUpdatDate
        {
            get
            {
                return scanUpdateDate;
            }
            set
            {
                scanUpdateDate = value;
            }
        }

        public DateTime VerifiedStatusUpdateDate
        {
            get
            {
                return verifiedStatusUpdateDate;
            }
            set
            {
                verifiedStatusUpdateDate = value;
            }
        }

        public int VerifiedEmpID
        {
            get
            {
                return verifiedEmpID;
            }
            set
            {
                verifiedEmpID = value;
            }
        }

        public int ScanDocID
        {
            get
            {
                return scanDocID;
            }
            set
            {
                scanDocID = value;
            }
        }

        public int ScanDocNumber
        {
            get
            {
                return scanDocNumber;
            }
            set
            {
                scanDocNumber = value;
            }
        }

        public double DSCProbationAmount
        {
            get
            {
                return dscProbationAmount;
            }
            set
            {
                dscProbationAmount = value;
            }
        }

        public int DSCProbationTime
        {
            get
            {
                return dscProbationTime;
            }
            set
            {
                dscProbationTime = value;
            }
        }

        public DateTime DSCProbationDate
        {
            get
            {
                return dscProbationDate;
            }
            set
            {
                dscProbationDate = value;
            }
        }

        public int TimeToPay
        {
            get
            {
                return timeToPay;
            }
            set
            {
                timeToPay = value;
            }
        }

        public DateTime TimeToPayDate
        {
            get
            {
                return timeToPayDate;
            }
            set
            {
                timeToPayDate = value;
            }
        }

        public int IsDSCSelected
        {
            get
            {
                return isDSCSelected;
            }
            set
            {
                isDSCSelected = value;
            }
        }

        public int IsRETSelected
        {
            get
            {
                return isRETSelected;
            }
            set
            {
                isRETSelected = value;
            }
        }

        public int IsMISCSelected
        {
            get
            {
                return isMISCSelected;
            }
            set
            {
                isMISCSelected = value;
            }
        }

        public DateTime ShowCauseDate
        {
            get
            {
                return showCauseDate;
            }
            set
            {
                showCauseDate = value;
            }
        }

        public string MISCComments
        {
            get
            {
                return miscComments;
            }
            set
            {
                miscComments = value;
            }
        }
        public int ShowCauseCourtNumber
        {
            get
            {
                return showcausecourtnumber;
            }
            set
            {
                showcausecourtnumber = value;
            }
        }
        public DateTime RetDate
        {
            get
            {
                return retdate;
            }
            set
            {
                retdate = value;
            }
        }
        public int RetCourtNumber
        {
            get
            {
                return retcourtnumber;
            }
            set
            {
                retcourtnumber = value;
            }
        }
        public int IsInsSelected
        {
            get
            {
                return isInsSelected;
            }
            set
            {
                isInsSelected = value;
            }
        }
        public int CourseCompletionDays
        {
            get
            {
                return coursecompletiondays;
            }
            set
            {
                coursecompletiondays = value;
            }
        }
        public int DrivingRecordsDays
        {
            get
            {
                return drivingrecordsdays;
            }
            set
            {
                drivingrecordsdays = value;
            }
        }
        public int IsShowCauseSelected
        {
            get
            {
                return isShowCauseSelected;
            }
            set
            {
                isShowCauseSelected = value;
            }
        }

        public string OscareCourtDetail
        {
            get
            {
                return CourtDetail;
            }
            set
            {
                CourtDetail = value;
            }
        }
        public int ChargeLevel
        {
            set
            {
                chargelevel = value;
            }
        }


        public string CDI
        {
            set
            {
                cdi = value;
            }
            get
            {
                return cdi;
            }
        }


        #endregion

        #region Methods
        /// <summary>
        /// Get Case Detail Info to display in grid
        /// </summary>
        /// <param name="TicketID">TicketID Whose Detail case info to get</param>
        public DataSet GetCaseDetail(int TicketID)
        {
            DataSet ds = ClsDb.Get_DS_BySPByOneParmameter("usp_HTS_GetCaseInfoDetail", "@TicketID", TicketID);
            return ds;
        }

        /// <summary>
        /// Get max ticketsviolationid by ticketid
        /// </summary>
        /// <param name="TicketID"></param>
        public DataSet GetMaxTVIDByTicketID(int TicketID)
        {
            DataSet ds = ClsDb.Get_DS_BySPByOneParmameter("usp_hts_get_tvidbyticketid", "@ticketid", TicketID);
            return ds;
        }
        /// <summary>
        /// Get Case Detail Info to display in update Violation page
        /// </summary>
        /// <param name="TicketViolationID">TicketViolationID Whose Detail case info to get</param>
        public DataSet GetCaseDetailByViolationID(int TicketViolationID)
        {
            DataSet ds = ClsDb.Get_DS_BySPByOneParmameter("usp_HTS_Get_ViolationUpdateInfo", "@TicketsViolationID", TicketViolationID);
            return ds;
        }

        //Waqas 5173 01/20/2009
        /// <summary>
        /// Get CaseTypeId and Covering firmId against TicketID, or TicketViolationID
        /// </summary>
        /// <param name="TicketID">Ticket ID</param>
        /// <param name="TicketViolationID">TicketViolationID</param>
        public DataSet GetCaseTypeCoveringFirm(int TicketID, int TicketViolationID)
        {
            string[] key = { "@TicketId", "@TicketsViolationID" };
            object[] values = { TicketID, TicketViolationID };
            DataSet ds = ClsDb.Get_DS_BySPArr("USP_HTP_Get_CaseTypeCoveringFirmForSharepoint", key, values);
            return ds;
        }
        /// <summary>
        /// Check for ticket number existance.
        /// </summary>
        /// <param name="tvid">Ticketviolationid</param>
        /// <param name="ticketNumber">Ticketnumber Whose existance to be checked</param>
        /// <param name="SeqNo">Sequence Number Whose existance to be checked</param>
        /// <param name="ticketid">Ticketid Whose existance to be checked</param>
        /// <param name="courtid">Couret location Whose existance to be checked</param>
        /*public DataSet CheckTicketNumberExistance(int tvid, string ticketnumber,string SeqNo,int ticketid,int courtid)
        {
			
                string[] key    = {"@tvid","@ticketnumber","@SeqNo","@ticketid","@courtid","@existingticket"};

                object[] value1 = {tvid,ticketnumber,SeqNo,ticketid,courtid,"0"};
					
                DataSet ds=ClsDb.Get_DS_BySPArr("usp_HTS_Check_TicketNumber_Existance",key,value1);
                return ds;				
			
        }*/

        /// <summary>
        /// Check for Cause number existance.
        /// </summary>
        /// <param name="tvid">Ticketviolationid</param>
        /// <param name="causenumber">Ticketnumber Whose existance to be checked</param>
        /// <param name="courtid">Couret location Whose existance to be checked</param>
        public DataSet CheckCauseNumberExistance(int tvid, string causenumber, int courtid)
        {

            string[] key = { "@tvid", "@causenumber", "@courtid", "@existingcausenumber" };

            object[] value1 = { tvid, causenumber, courtid, "0" };

            DataSet ds = ClsDb.Get_DS_BySPArr("usp_HTS_Check_CauseNumber_Existance", key, value1);
            return ds;

        }

        /// <summary>
        /// Nasir 5310 12/24/2008 Check ISARLProcess 
        /// </summary>
        /// <param name="CourtID"></param>
        /// <returns></returns>
        public bool CheckIsARLProcess(int CourtIDq)
        {

            try
            {
                string[] Key = { "@CourtID" };
                object[] Value = { CourtIDq };
                return Convert.ToBoolean(ClsDb.ExecuteScalerBySp("USP_HTP_Check_IsALRProcess", Key, Value));
            }
            catch (Exception ex)
            {

                clsLogger.ErrorLog(ex);
                return false;
            }

        }

        /// <summary>
        /// Updates Case Detail Info on Update Violation page
        /// </summary>
        public bool UpdateCaseDetail()
        {
            //Zeeshan Ahmed 3724 05/13/2008 Add Arrest Date Parameter in the procedure
            //Zeeshan Ahmed 4163 06/03/2008 Add Missed Court Type Parameter In Procedure
            //string[] key = { "@TicketsViolationID", "@TicketsViolationIDs", "@RefCaseNumber", "@CauseNumber", "@ViolationId", "@FineAmount", "@BondAmount", "@BondFlag", "@PlanId", "@VerifiedStatusId", "@VerifiedCourtDate", "@VerifiedCourtNumber", "@CourtId", "@UpdateMain", "@EmployeeId", "@OscarCourtDetail", "@ArrignmentWaitingdate", "@BondWaitingdate", "@ChargeLevel", "@CDI", "@ArrestDate", "@MissedCourtType" };
            //object[] value1 = { TicketViolationID, TicketViolationIDs, RefCaseNumber, CauseNumber, ViolationNumber, FineAmount, BondAmount, BondFlag, PlanID, CourtViolationStatusID, CourtDate, CourtNumber, CourtID, UpdateMain, EmpID, CourtDetail, arrignmentwaitingdate, bondwaitingdate, chargelevel, cdi, arrestDate, missedCourtType };
            //Fahad 5299 02/10/2009 If clause added and one paramete updateall added
            if (TicketViolationID > 0 && RefCaseNumber != null && CauseNumber != null && CourtViolationStatusID > 0 && CourtNumber != null && CourtID > 0 && EmpID > 0 && CourtDetail != null && cdi != null)
            {
                string[] key = { "@TicketsViolationID", "@RefCaseNumber", "@CauseNumber", "@ViolationId", "@FineAmount", "@BondAmount", "@BondFlag", "@PlanId", "@VerifiedStatusId", "@VerifiedCourtDate", "@VerifiedCourtNumber", "@CourtId", "@UpdateMain", "@EmployeeId", "@OscarCourtDetail", "@ArrignmentWaitingdate", "@BondWaitingdate", "@ChargeLevel", "@CDI", "@ArrestDate", "@MissedCourtType", "@UpdateAll" };
                object[] value1 = { TicketViolationID, RefCaseNumber, CauseNumber, ViolationNumber, FineAmount, BondAmount, BondFlag, PlanID, CourtViolationStatusID, CourtDate, CourtNumber, CourtID, UpdateMain, EmpID, CourtDetail, arrignmentwaitingdate, bondwaitingdate, chargelevel, cdi, arrestDate, missedCourtType, UpdateAll };

                try
                {
                    ClsDb.ExecuteSP("USP_HTS_Update_ViolationUpdateInfo", key, value1);
                    return true;
                }
                catch (Exception ex)
                {
                    bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                    return false;
                }
            }
            else
            {
                return false;

            }

        }

        /// <summary>
        /// Insert New Violation Case Detail Info on Update Violation page
        /// </summary>
        public int InsertCaseDetail()
        {
            //Aziz 1974 05/08/08 Change the return type.SP is now returning newly created Id
            //Zeeshan Ahmed 3724 05/13/2008 Add Arrest Date Parameter in the procedure
            //Zeeshan Ahmed 4163 06/03/2008 Add Missed Court Type Parameter In Procedure
            int TicketsViolationID = 0;
            string[] key = { "@TicketId_pk", "@TicketsViolationIDs", "@RefCaseNumber", "@CauseNumber", "@ViolationNumber_pk", "@CourtNumberAuto", "@FineAmount", "@BondAmount", "@BondFlag", "@PlanId", "@CaseStatusId", "@CourtDateAuto", "@CourtId", "@EmployeeId", "@UpdateMain", "@OscarCourtDetail", "@ChargeLevel", "@CDI", "@ArrestDate", "@MissedCourtType" };
            object[] value1 = { TicketID, TicketViolationIDs, RefCaseNumber, CauseNumber, ViolationNumber, CourtNumber, FineAmount, BondAmount, BondFlag, PlanID, CourtViolationStatusID, CourtDate, CourtID, EmpID, UpdateMain, CourtDetail, chargelevel, cdi, arrestDate, missedCourtType };

            try
            {
                object oTicketsViolationID = ClsDb.ExecuteScalerBySp("USP_HTS_Insert_NewViolation", key, value1);
                int.TryParse(oTicketsViolationID.ToString(), out TicketsViolationID);
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
            return TicketsViolationID;
        }


        public DataSet GetDispositionInfoDetail(int TicketID)
        {
            DataSet ds = ClsDb.Get_DS_BySPByOneParmameter("USP_HTS_Get_CaseDispositionDetail", "@TicketID_PK", TicketID);
            return ds;
        }

        //public bool UpdateDispositionInfoDetail(IDbTransaction trans)
        public bool UpdateDispositionInfoDetail()
        {
            string[] key = { "@Ticketsviolationid", "@MISCComments", "@ShowCauseDate", "@IsMISCSelected", "@IsRETSelected", "@IsDSCSelected", "@TimeToPayDate", "@TimeToPay", "@DSCProbationDate", "@DSCProbationTime", "@DSCProbationAmount", "@ViolationStatusID", "@EmployeeID", "@ShowCauseCourtNumber", "@RetDate", "@RetCourtNumber", "@IsInsSelected", "@CourseCompletionDays", "@DrivingRecordsDays", "@IsShowCauseSelected", "@CoveringFirmID" };

            object[] value1 = { TicketViolationIDs, MISCComments, ShowCauseDate, IsMISCSelected, IsRETSelected, IsDSCSelected, TimeToPayDate, TimeToPay, DSCProbationDate, DSCProbationTime, DSCProbationAmount, ViolationStatusID, EmpID, ShowCauseCourtNumber, RetDate, RetCourtNumber, IsInsSelected, CourseCompletionDays, DrivingRecordsDays, IsShowCauseSelected, FirmID };
            try
            {
                ClsDb.ExecuteSP("usp_HTS_Update_casedispositiondetail", key, value1);
                return true;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return false;
            }

        }

        public DataSet GetSummaryInfoDetail(int TicketID)
        {
            string[] key2 = { "@TicketId_pk" };
            object[] value1 = { TicketID };
            DataSet ds = ClsDb.Get_DS_BySPArr("USP_HTS_GET_SummaryInfoDetail ", key2, value1);
            return ds;
        }
        //change by Ajmal
        //public SqlDataReader GetSummaryInfo(int TicketID)
        public IDataReader GetSummaryInfo(int TicketID)
        {
            string[] key1 = { "@TicketId_pk" };
            object[] value1 = { TicketID };

            IDataReader objSummaryInfo = ClsDb.Get_DR_BySPArr("USP_HTS_GET_SummaryInfo", key1, value1);
            return objSummaryInfo;
        }

        public DataSet GetSummaryInformation(int TicketID)
        {
            string[] key1 = { "@TicketId_pk" };
            object[] value1 = { TicketID };

            DataSet objSummaryInfo = ClsDb.Get_DS_BySPArr("USP_HTS_GET_SummaryInfo", key1, value1);
            return objSummaryInfo;
        }

        /// <summary>
        /// This procedure violation amount of case by cause number.
        /// </summary>
        /// <param name="TicketID">Case Number</param>
        /// <param name="CauseNumber">Cause Number</param>
        /// <param name="FineAmount">Fine Amount</param>
        /// <returns>Return true if violation amount updated successfully</returns>
        public bool UpdateViolationFineAmount(int TicketID, string CauseNumber, string FineAmount)
        {
            //Zeeshan Ahmed 3535 04/15/2008
            string[] keys = { "@TicketID", "@CauseNumber", "@FineAmount" };
            object[] values = { TicketID, CauseNumber, FineAmount };
            ClsDb.ExecuteSP("USP_HTP_UPDATE_VIOLATION_FINEAMOUNT", keys, values);
            return true;
        }

        /// <summary>
        /// This function Update the firmid using ticket id 
        /// </summary>
        /// <param name="ticketid">Case Number</param>
        /// <param name="firmid">Firm Id</param>        
        public void UpdateCoverageFirm(int firmid, int ticketid)
        {
            //Noufil 4747 on 09/09/2008
            string[] key2 = { "@CoveringFirmID", "@ticketid_pk" };
            object[] value2 = { firmid, ticketid };
            ClsDb.ExecuteSP("usp_HTP_Update_CoverageFirm", key2, value2);
        }

        //Nasir 6483 10/07/2009
        /// <summary>
        /// set hidden field if secondary user and client last payment passess 3 week 
        /// if not internet client else two month
        /// </summary>
        /// <param name="TicketID"></param>
        public string SetForBondReport(int TicketID)
        {
            string value = "0";

            DataTable dt = new DataTable();
            dt = ClsCase.CheckForBondReport(TicketID);
            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0][0].ToString() != "0" && dt.Rows[0][1].ToString() != "0")
                {
                    value = dt.Rows[0][0].ToString();
                }
            }
            return value;
        }

        #endregion

        public clsCaseDetail()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        //Sabir Khan 5260 11/28/2008 Check ID in Ticket Number ....
        /// <summary>
        /// This function is used to get all RefCaseNumber by passing ticketid 
        /// </summary>
        /// <param name="ticketid">Ticket Id</param>
        /// <returns>All RefCaseNumber associated with the passing ticket ID</returns>
        public bool HasIDTicket(int ticketid, DataSet DSIDTicket)
        {
            //DataSet DSIDTicket = GetCaseDetail(ticketid);

            foreach (DataRow row in DSIDTicket.Tables[0].Rows)
            {
                //ozair 5260 12/23/2008 fixed issue of alphabate case
                if (row["RefCaseNumber"].ToString().ToUpper().StartsWith("ID"))
                { return true; }

            }
            return false;

        }

        //Sabir Khan 5763 04/17/2009 Check for same court date ....
        /// <summary>
        /// This function is used to compare the courtdate of all voilation of a client.
        /// </summary>
        /// <param name="ticketid">Ticket Id</param>
        /// <returns>True if all courtdate are same otherwise false</returns>
        public bool HasSameCourtDate(int ticketid)
        {
            DataSet ds = GetCaseDetail(ticketid);
            //Yasir Kamal 6603 09/18/2009 Lor same court date bug fixed.
            string strExp;
            strExp = "ulstatus <> 50";
            bool same = true;
            DataRow[] rows = ds.Tables[0].Select(strExp);

            // Noufil 6621 09/25/2009 Check existance of row.
            if (rows != null && rows.Length > 0)
            {
                DateTime firstdate = Convert.ToDateTime(rows[0]["CourtDateMain"].ToString());
                foreach (DataRow row in rows)
                {
                    // Noufil 6551 09/09/2009 Don't consider dispose and No-Hire cases (both category id 50) when matching different court date.
                    if (firstdate != Convert.ToDateTime(row["CourtdateMain"].ToString()))
                        same = false;

                }
            }
            else
                same = false;

            return same;
        }

        //Sabir Khan 5763 04/17/2009 Check for more speeding violation....
        /// <summary>
        /// This function is used to check if a cllient has more then one speeding violation.
        /// </summary>
        /// <param name="ticketid">Ticket Id</param>
        /// <returns>false if more then one speeding violation  otherwise true</returns>
        public int HasMoreSpeedingViolation(int ticketid)
        {
            DataSet ds = GetCaseDetail(ticketid);
            int court = 0;
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                if ((row["violationDescription"].ToString().Contains("SPEEDING")) || (row["violationDescription"].ToString().Contains("SPEED")))
                    court = court + 1;

            }
            return court;

        }

        //Waqas 5864 06/24/2009 Fetching Attorneys against case.
        /// <summary>
        /// This method is used to get attorneys by ticket id
        /// </summary>
        /// <param name="ticketid"></param>
        /// <returns></returns>
        public DataTable GetAttorneysByTickets(int ticketid)
        {
            string[] key2 = { "@TicketID" };
            object[] value2 = { ticketid };
            DataTable dt = ClsDb.Get_DT_BySPArr("usp_htp_get_AttorneysByTicketID", key2, value2);
            return dt;
        }


        //Sabir 8058 07/23/2010 -->Fetching total number of cases having status in pretrail for selected court date.
        /// <summary>
        /// This methos is used to get total number of cases having status in pretrail for selected court date
        /// </summary>
        /// <param name="crtDate">Court Date</param>
        /// <param name="CourtID">Court ID</param>
        /// <returns>get total number of cases having status in pretrail for selected court date</returns>
        public DataTable GetTotalPMCPRreTrialCasesForSelectedDate(DateTime crtDate, int CourtID)
        {
            string[] key2 = { "@crtDate", "@CourtID" };
            object[] value2 = { crtDate, CourtID };
            DataTable dt = ClsDb.Get_DT_BySPArr("USP_HTP_GET_PMCPretrialforSelectedDate", key2, value2);
            return dt;
        }
        //Afaq 8521 01/04/2011 method added.
        /// <summary>
        /// This method is used to get attorneys by ticket id against each violation.
        /// </summary>
        /// <param name="ticketid"></param>
        /// <param name="violationId"></param>
        /// <returns></returns>
        public DataTable GetAttorneysByViolation(string ticketid, string violationId)
        {
            string[] key2 = { "@TicketID", "@ViolationId" };
            object[] value2 = { ticketid, violationId };
            DataTable dt = ClsDb.Get_DT_BySPArr("usp_htp_get_AttorneysByViolations", key2, value2);
            return dt;
        }

    }
}

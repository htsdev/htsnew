﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using FrameWorkEnation.Components;

namespace lntechNew.Components.ClientInfo
{
    // Noufil 5894 06/08/2009 Class created.
    public class clsQuoteCallBack
    {
        clsENationWebComponents ClsDB = new clsENationWebComponents();

        /// <summary>
        ///     This function returns Quote call back report.
        /// </summary>
        /// <param name="Criteria">1.Day Before Court Date List or 2.Call back Date on</param>
        /// <param name="Datefrom">Date range from for criterea 1</param>
        /// <param name="DateTo">Date range to for criterea 1</param>
        /// <param name="CallDate">Date for criterea 2.</param>
        /// <param name="casetype">Case type 1.Traffic 2.Crminal 3.Civil 4.Family 0.All</param>
        /// <returns>DataTable having rows >= 0</returns>
        public DataTable GetQuoteCallBack(int Criteria, DateTime Datefrom, DateTime DateTo, DateTime CallDate, int casetype)
        {
            if (Criteria > 0 && (casetype == -1 || casetype > 0) && Datefrom != null && DateTo != null && CallDate != null)
            {
                DataTable dt_result = new DataTable();
                switch (Criteria)
                {
                    case 1:
                        string[] key1 = { "@DateFrom", "@DateTo", "@casetypeid" };
                        object[] value1 = { Datefrom.ToString("MM/dd/yyyy"), DateTo.ToString("MM/dd/yyyy"), casetype };
                        dt_result = ClsDB.Get_DT_BySPArr("usp_HTS_Get_All_DayBeforeCourtDate", key1, value1);
                        break;
                    case 2:
                        string[] key2 = { "@CallDate", "@casetypeid" };
                        object[] value2 = { CallDate.ToString(), casetype };
                        dt_result = ClsDB.Get_DT_BySPArr("usp_HTS_Get_All_ThreeDayAfterFirstQuote_New", key2, value2);
                        break;
                }
                return dt_result;
            }
            else
                throw new ArgumentNullException("Parameter cannot be null.");
        }

        /// <summary>
        /// This function returns all Quote call back query type
        /// </summary>
        /// <returns>DataTable having rows >= 0</returns>
        public DataTable GetAllQuoteCallBackQuery()
        {
            return ClsDB.Get_DT_BySPArr("usp_HTS_Get_All_QueryType");
        }

        //Fahad 6934 12/10/2009
        /// <summary>
        /// This function returns all Quote call back query type
        /// </summary>
        /// <returns>DataTable having rows >= 0</returns>
        public DataTable GetQuoteCallBackTicketIDs()
        {
            return ClsDB.Get_DT_BySPArr("USP_DTP_GET_TicketIDs");
        }

        //Fahad 6934 12/10/2009
        /// <summary>
        /// This function returns Case Violation of the Client on the basis of Ticket Id.
        /// </summary>
        /// <param name="TicketID">TicketId on which Client all information based.</param>
        /// <returns>DataTable having rows >= 0</returns>
        public DataTable GetCaseInfoDetailByTicketId(int TicketID)
        {
            string[] key1 = { "@TicketID" };
            object[] value1 = { TicketID };
            return ClsDB.Get_DT_BySPArr("usp_HTS_GetCaseInfoDetail", key1, value1);

        }

        //Fahad 6934 12/10/2009
        /// <summary>
        /// This function Returns all Quote call back Info on the basis of TicketId
        /// </summary>
        /// <param name="TicketID">TicketId on which Client all information based.</param>
        /// <returns>DataTable having rows >= 0</returns>
        public DataTable GetQuoteCallBackDataByTicketId(int TicketID)
        {
            string[] key1 = { "@TicketID" };
            object[] value1 = { TicketID };
            return ClsDB.Get_DT_BySPArr("USP_HTP_Get_QuoteCallBackByTicketID", key1, value1);

        }

        //Fahad 6934 12/17/2009
        /// <summary>
        /// This function Update all Quote call back Information on basis of TicketId
        /// </summary>
        /// <param name="TicketID">TicketId on which Client all information based.</param>
        /// <param name="FollowUpId">Quote call back Follow Up Status Id.</param>
        /// <param name="FollowUp">Option of Follow Up either 1 OR 0.</param>
        /// <param name="CallBackDate">Call Back Date</param>
        /// <param name="EmpID">ID through which Employee logged in.</param>
        public void UpdateQuoteCallBackInfoByTicketId(int TicketID, int FollowUpId, int FollowUp, DateTime CallBackDate, int EmpID)
        {
            string[] key = { "@TicketID_FK", "@FollowUPID", "@FollowUpYN", "@CallBackDate", "@EmpID" };
            object[] value1 = { TicketID, FollowUpId, FollowUp, CallBackDate.ToString("MM/dd/yyyy"), EmpID };
            ClsDB.ExecuteSP("USP_HTP_Update_QuoteCallBackInfo", key, value1);
        }

        //Fahad 6934 01/13/2010
        /// <summary>
        /// This method Update the Tracking of the Report
        /// </summary>
        /// <param name="TicketID">TicketId on which Client all information based.</param>
        /// <param name="EventDescription">Which event has occured Clicked Or Updated</param>
        /// <param name="EmpID">ID through which Employee logged in.</param>
        public void UpdateQuoteCallTrackingInfoByTicketId(int TicketID, string EventDescription, int EmpID)
        {
            string[] key = { "@TicketID", "@EventDescription", "@EmployeeID" };
            object[] value1 = { TicketID, EventDescription, EmpID };
            ClsDB.ExecuteSP("USP_HTP_SetQuoteCallTrackingReport", key, value1);

        }

    }
}

using System;
using System.Data;
using System.Data.SqlClient;
using FrameWorkEnation.Components;

namespace lntechNew.Components.ClientInfo
{
    /// <summary>
    /// Summary description for clsCaseStatus.
    /// </summary>
    public class clsCaseStatus
    {
        //Create an Instance of Data Access Class
        clsENationWebComponents ClsDb = new clsENationWebComponents();

        #region Variables

        private int typeID = 0;
        private string description = String.Empty;
        private double sameDayCharged = 0;
        private double chargeWithIn24 = 0;
        private double chargeWithIn48 = 0;
        private int orderField = 0;

        #endregion

        #region Properties

        public int TypeID
        {
            get
            {
                return typeID;
            }
            set
            {
                typeID = value;
            }
        }

        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }

        public double SameDayCharged
        {
            get
            {
                return sameDayCharged;
            }
            set
            {
                sameDayCharged = value;
            }
        }

        public double ChargeWithIn24
        {
            get
            {
                return chargeWithIn24;
            }
            set
            {
                chargeWithIn24 = value;
            }
        }

        public double ChargeWithIn48
        {
            get
            {
                return chargeWithIn48;
            }
            set
            {
                chargeWithIn48 = value;
            }
        }

        public int OrderField
        {
            get
            {
                return orderField;
            }
            set
            {
                orderField = value;
            }
        }

        #endregion

        #region Methods

        public DataSet GetAllCaseStatus()
        {
            DataSet ds = ClsDb.Get_DS_BySP("usp_HTS_GetAllCaseStatus");
            return ds;
        }
        public DataSet GetAllCourtCaseStatus()
        {
            DataSet ds = ClsDb.Get_DS_BySP("usp_HTS_GetAllCourtCaseStatus");
            return ds;
        }


        //New overloaded method to get case statuses for a specific court house.
        /// <summary>
        /// Get case statuses for specific court house.
        /// if isdisposition is true, the DISPOSED will also be returned with other statuses
        /// </summary>
        /// <param name="courtid"></param>
        /// <param name="isdisposition"></param>
        public DataSet GetAllCourtCaseStatus(int courtid, bool isdisposition)
        {
            return GetAllCourtCaseStatus(courtid, isdisposition, CaseType.Traffic);
        }

        public DataSet GetALRCourtCaseStatus(bool isdisposition)
        {
            return GetAllCourtCaseStatus(3047, isdisposition, CaseType.Traffic);
        }
        //ozair 4442 07/22/2008 ALR Courts(Houston, Fort bend, Conroe)
        public DataSet GetALRCourtCaseStatus(int courtid, bool isdisposition)
        {
            return GetAllCourtCaseStatus(courtid, isdisposition, CaseType.Criminal);
        }
        //end ozair 4442
        public DataSet GetDispositionCaseStatus()
        {
            DataSet ds = ClsDb.Get_DS_BySP("usp_HTS_GetDispositionCaseStatus");
            return ds;
        }


        /// <summary>
        /// This function is used to get status related to the case type
        /// </summary>
        /// <param name="courtid">Court ID </param>
        /// <param name="isdisposition">IsDisposition</param>
        /// <param name="caseType">Case Type</param>
        /// <returns>Return Court Status List</returns>
       public DataSet GetAllCourtCaseStatus(int courtid, bool isdisposition, int caseType) //Agha Usman 4271 07/02/2008 - Change The caseType parameter type

        {
            //Zeeshan Ahmed 3979 05/20/2008
            string[] keys = { "@courtid", "@isDisposition", "@CaseType" };
            object[] values = { courtid, isdisposition, caseType };
            DataSet ds = ClsDb.Get_DS_BySPArr("usp_HTS_GetAllCourtCaseStatus", keys, values);
            return ds;
        }



        /*public SqlDataReader GetAllCaseStatusReader()
        {
            SqlDataReader rdr=ClsDb.Get_DR_BySP("usp_HTS_GetAllCaseStatus");
            return rdr;
        }*/
        #endregion
        public clsCaseStatus()
        {
            //
            // TODO: Add constructor logic here
            //
        }
    }
}

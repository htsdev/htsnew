using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ExpertPdf.HtmlToPdf;
using ExpertPdf.MergePdf;

namespace lntechNew.Components.ClientInfo
{
    // This Class Is Used To Generate and Merge PDF Files
    public class clsPdfGenerator
    {        
        
        #region Variables

        PDFMerge Merge = new PDFMerge();
        PdfConverter pdfConverter;
        string filepath = String.Empty;
        string url = String.Empty;
        bool ispotrait = true;
        int sequence = 0;
        string errormessage = String.Empty;
        
        #endregion
        
        #region Properties


        public string URL
        {
            get
            {
                return url;
            }

            set
            {
                url = value;
            }
        }

        public string FilePath
        {
            get
            {
                return filepath;
            }

            set
            {
                filepath = value;
            }
        }

        public bool IsPotrait
        {
            get
            {
                return ispotrait;
            }

            set
            {
                ispotrait = value;
            }
        }

        public string Error
        {
            get
            {
                return errormessage;
            }

            set
            {
                errormessage = value;
            }
        }

        public int Sequence
        {
            get
            {
                return sequence;
            }

            set
            {
                sequence = value;
            }
        }

        #endregion
        
        #region Methods


        public bool GeneratePDF()
        {
            try
            {
                CreatePDF(url, filepath, ispotrait);
                return true;
            }
            catch (Exception ex)
            {
                errormessage = ex.Message;
                return false;
            }
        }

        public bool CreateMultiplePdfAndMerge()
        {

            try
            {
                CreateAndMergePDF(url, filepath, ispotrait, sequence);
                return true;
            }
            catch(Exception ex)
            {
                errormessage=  ex.Message;
                return false;
            }
        }
                        
        private void CreatePDF(string url, string filenamepath, bool portrait)
        {
           pdfConverter = new PdfConverter();

            pdfConverter.PdfDocumentOptions.PdfPageSize = ExpertPdf.HtmlToPdf.PdfPageSize.Letter;
            pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
            
            if (!portrait)
            {
                pdfConverter.PdfDocumentOptions.PdfPageOrientation = ExpertPdf.HtmlToPdf.PDFPageOrientation.Landscape;
            }
            
            SetPdfDocumentSettings(pdfConverter);
            pdfConverter.PdfDocumentOptions.PdfPageSize = ExpertPdf.HtmlToPdf.PdfPageSize.A4;
            pdfConverter.SavePdfFromUrlToFile(url, filenamepath);
           
        }

        private void CreateAndMergePDF(string url, string filenamepath, bool portrait,int seq)
        {
            pdfConverter = new PdfConverter();

            pdfConverter.PdfDocumentOptions.PdfPageSize = ExpertPdf.HtmlToPdf.PdfPageSize.Letter;
            pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
            if (!portrait)
            {
                pdfConverter.PdfDocumentOptions.PdfPageOrientation = ExpertPdf.HtmlToPdf.PDFPageOrientation.Landscape;
            }
                                
            // Set PDF Page Size
            SetPdfDocumentSettings(pdfConverter);
            // Save Pdf File
            pdfConverter.SavePdfFromUrlToFile(url, @filenamepath);
            
            //Merge PDF Files
            Merge.AppendPDFFile(filenamepath);
            
            for (int i = 0; i < seq-1; i++)
                Merge.AppendPDFFile(filenamepath);

            Merge.SaveMergedPDFToFile(filenamepath);
                        
        }


        public void MergePDF()
        {
            pdfConverter = new PdfConverter();
            pdfConverter.PdfDocumentOptions.PdfPageSize = ExpertPdf.HtmlToPdf.PdfPageSize.Letter;
            pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
            if (!ispotrait)
            {
                pdfConverter.PdfDocumentOptions.PdfPageOrientation = ExpertPdf.HtmlToPdf.PDFPageOrientation.Landscape;
            }
            // Set PDF Page Size
            SetPdfDocumentSettings(pdfConverter);
            //Merge PDF Files
            Merge.AppendPDFFile(filepath);
            // Save Pdf File
            pdfConverter.SavePdfFromUrlToFile(url, filepath);
            //Apend File
            Merge.AppendPDFFile(filepath);
            // Save Merge File
            Merge.SaveMergedPDFToFile(filepath);
          
        }



        private void SetPdfDocumentSettings(PdfConverter pdfConverter)
        {
            pdfConverter.PdfDocumentOptions.ShowHeader = false;
            pdfConverter.PdfDocumentOptions.ShowFooter = false;
            pdfConverter.PdfDocumentOptions.LeftMargin = 5 ;
            pdfConverter.PdfDocumentOptions.RightMargin = 5;
            pdfConverter.PdfDocumentOptions.TopMargin = 5;
            pdfConverter.PdfDocumentOptions.BottomMargin = 5;
            pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
        }
   
        #endregion

    }
}

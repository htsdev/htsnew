using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.Security;

namespace lntechNew.Components.ClientInfo
{
	/// <summary>
	/// Class which will maintain session and cookie information 
	/// </summary>
	public class clsSession
	{
		clsLogger bugTracker = new clsLogger();
#region Variables
		
		private static string sessionID=String.Empty;
		//private int empID=0;
		//private string abbreviation=String.Empty;
		private static int sessionTimeOut=0;

#endregion
		
#region Properties

		/// <summary>
		/// Get or Set SessionID value 
		/// </summary>
		public static string SessionID
		{
			get
			{
				return sessionID;
			}
			set
			{
				sessionID = value;
			}
		}

	/*	public int EmpID
		{
			get
			{
				return empID;
			}
			set
			{
				empID = value;
			}
		}
      
		public string Abbreviation
		{
			get
			{
				return abbreviation;
			}
			set
			{
				abbreviation = value;
			}
		}
*/
		/// <summary>
		/// Get or Set SessionTimeOut Value
		/// </summary>
		public static int SessionTimeOut
		{
			get
			{
				return sessionTimeOut;
			}
			set
			{
				sessionTimeOut = value;
			}
		}
#endregion

#region Methods

		/// <summary>
		/// Check Session exist or not
		/// </summary>
		/// <param name="sessionstat">Current Page.session Object</param>		
		//public bool IsValidSession(HttpSessionState sessionstat)
		public bool IsValidSession(HttpRequest httprequest)
		{
//			if (sessionstat.Count==0)			
//			{
//				return false;
//			}
		 return	IsCookieExist("sEmpID",httprequest);
			 

			
		}
        /// <summary>
        /// Zahoor 4676 09/17/2008
        /// Check for valid session by checking the employee id in cookie first, 
        /// if exist in cookie then return true otherwise it checks the employee id 
        /// in session; if found then recreate the required cookies and returns true
        /// otherwise returns false
        /// </summary>        
        /// <param name="httprequest">Current request</param>
        /// <param name="httpresponse">Current response</param> 
        /// <param name="sessionstat">Current Page.session Object</param>	
        public bool IsValidSession(HttpRequest httprequest, HttpResponse httpresponse, HttpSessionState sessionstat)
        {
            if (httprequest.AppRelativeCurrentExecutionFilePath.Contains("ViolationFeeold.aspx"))
            {
                if (!IsCookieExist("sEmpID", httprequest))
                {

                    int empID;
                    if (!int.TryParse(GetSessionVariable("sEmpID", sessionstat), out empID))
                    {
                        return false;
                    }
                    else
                    {
                        return RestoreCookies(empID, httprequest, httpresponse);
                    }
                }
                else
                {
                    return true;
                }
            }
            else
            {
                int empID;
                if (!int.TryParse(GetSessionVariable("sEmpID", sessionstat), out empID))
                {
                    return false;
                }
                else
                {
                    return RestoreCookies(empID, httprequest, httpresponse);
                }
            }
        }

		/*

		public void SetSessionTimeOut(int TimeInMinutes)
		{
		
		}

		public int GetSessionTimeOut(string SessionID)
		{
			return 0;
		}
		*/
		/// <summary>
		/// Create Session on start of the application
		/// </summary>
		/// <param name="login">Save UserID value into session</param>
		/// <param name="empid">Save EmpID value into session</param>
		/// <param name="Abbreviation">Save Abbreviation value into session</param>
		/// <param name="sessionstat">Current Page.session Object</param>		
		public bool SetSessionVariable(string login,int empid,string Abbreviation,int AccessType, HttpRequest httprequest, HttpResponse httpresponse)
		{
			try
			{				
//				HttpSessionState uSession= sessionstat;
//				uSession.Add("sUserID",login);
//				uSession.Add("sEmpID",empid);
//				uSession.Add("sAbbreviation",Abbreviation);
//				uSession.Add("sAccessType",AccessType);
				bool bTemp=false;
				bTemp = CreateCookie("sUserId", login, httprequest,httpresponse);
				bTemp = CreateCookie("sEmpID", empid.ToString(), httprequest,httpresponse);
				//commented by ozair not used
				//bTemp = CreateCookie("sAbbreviation", Abbreviation, httprequest,httpresponse);
				bTemp = CreateCookie("sAccessType", AccessType.ToString(), httprequest,httpresponse);
				
				return bTemp;
			}
			catch
			{
				//bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
				return false;
			}					
		}
		/// <summary>
		/// Individual Seesion Variables
		/// </summary>
		/// <param name="sName">Session Variable Name</param>
		/// <param name="sValue">Session Variable Value</param>
		/// <param name="sessionstat">Current Page.session Object</param>
		/// <returns></returns>
		public bool SetSessionVariable(string sName,string sValue,HttpSessionState sessionstat) 
		{
			try
			{
				HttpSessionState uSession= sessionstat;
				uSession.Add(sName,sValue);				
				return true;
			}
			catch
			{
				//bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
				return false;
			}
		}

		/// <summary>
		/// Individual Objects
		/// </summary>
		/// <param name="sName">Session Variable Name</param>
		/// <param name="oValue">object Value</param>
		/// <param name="sessionstat">Current Page.session Object</param>
		/// <returns></returns>
		public bool SetSessionVariable(string sName, object oValue, HttpSessionState sessionstat)
		{
			try
			{				
				HttpSessionState uSession= sessionstat;
				uSession.Add(sName, oValue);
				return true;
			}
			catch
			{
		//		bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
				return false;
			}					
		}

		/// <summary>
		/// Get Session variable value
		/// </summary>
		/// <param name="sName">Session Variable Name</param>
		/// <param name="sessionstat">Current Page.session Object</param>		
		public string GetSessionVariable (string sName,HttpSessionState sessionstat)
		{
			try
			{
				HttpSessionState uSession= sessionstat;			
				return uSession[sName].ToString();
			}
			catch
			{
		//		bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
				return null ;
			}
		}

		/// <summary>
		/// Get object from session variable
		/// </summary>
		/// <param name="sName">Session Variable Name</param>
		/// <param name="sessionstat">Current Page.session Object</param>		
		public object GetSessionObject (string sName,HttpSessionState sessionstat)
		{
			try
			{
				HttpSessionState uSession= sessionstat;			
				return uSession[sName];
			}
			catch
			{
			//	bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
				return null ;
			}
		}

		/// <summary>
		/// Create Cookie 
		/// </summary>
		/// <param name="CookieName">Specify Cookie Variable name</param>
		/// <param name="CookieValue">Specify Cookie Variable Value</param>
		/// <param name="httprequest">Current Page.Request Object</param>
		/// <param name="htpresponse">Current Page.Response Object</param>		
		public bool CreateCookie(string CookieName,string CookieValue,HttpRequest httprequest,HttpResponse htpresponse)
		{	
			try
			{				
				if (IsCookieExist(CookieName,httprequest)==false)
				{					
					HttpCookie uCookie= new HttpCookie(CookieName);
					uCookie.Value=CookieValue;
					uCookie.Expires=DateTime.Now.AddDays(1);
					htpresponse.Cookies.Add(uCookie);										
					return true;
				}
				else
				{
					httprequest.Cookies.Remove(CookieName);
					HttpCookie uCookie= new HttpCookie(CookieName);
					uCookie.Value=CookieValue;
					uCookie.Expires=DateTime.Now.AddDays(1);
					htpresponse.Cookies.Add(uCookie);										
					return true;
				}
			}
			catch(Exception ex)
			{
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
				return false;
			}
		}
		//----------------Added By Ozair--------------------
		/// <summary>
		/// Delete Cookie 
		/// </summary>
		/// <param name="CookieName">Specify Cookie Variable name</param>
		/// <param name="htpresponse">Current Page.Response Object</param>		
		public void DeleteCookie(string cookieName,HttpResponse htpresponse)
		{	
			try
			{
				htpresponse.Cookies[cookieName].Expires = DateTime.Now.AddDays(-1);
				//htpresponse.Cookies.Remove(cookieName);
			}
			catch(Exception ex)
			{
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}

		//-------------------------------------------------------
		/// <summary>
		/// Call by create cookie function to check either cookie already exist or not
		/// </summary>
		/// <param name="CheckCookie">Specify Cookie Name To Check</param>
		/// <param name="htprequest">Current Page.Request Object</param>		
		public bool IsCookieExist(string CheckCookie, HttpRequest httprequest)
		{
			HttpCookieCollection cookies = httprequest.Cookies;
			string[] cookiearr=cookies.AllKeys;			

			for(int i=0;i<cookiearr.Length;i++)
				if (cookiearr[i]==CheckCookie)
					return true;					

			return false;
		}

		/// <summary>
		/// Get cookie value by cookie name 
		/// </summary>
		/// <param name="CookieName">Specify Cookie Name To Get Its Value</param>
		/// <param name="httpreq">Current Page.Request Object</param>		
		public string GetCookie(string CookieName,HttpRequest httpreq)
		{
			if(IsCookieExist(CookieName,httpreq)==true)
			{					
				return httpreq.Cookies.Get(CookieName).Value;            			
			}
			return"";			
		}

		/// <summary>
		/// Method To Encode String
		/// </summary>
		/// <param name="pStr">Specify String To Encode</param>
		public string Hashed_MD5(string pStr)
		{
			return FormsAuthentication.HashPasswordForStoringInConfigFile(pStr, "MD5");
		}

        /// <summary>
        /// Zahoor 4676 09/17/2008
        /// creates the cookies by fetching data by employee id 
        /// </summary>
        /// <param name="empID"></param>
        /// <param name="httprequest"></param>
        /// <param name="htpresponse"></param>
        public bool RestoreCookies(int empID, HttpRequest httprequest, HttpResponse htpresponse)
        {
            // Zahoor 4676 For getting user info 
            clsUser user = new clsUser();
            //user.EmpID = empID;
            clsUser cUser = user.GetSystemUserInfo(empID);

            //In order to create employee cookie
            if (!CreateCookie("sEmpID", cUser.EmpID.ToString(), httprequest, htpresponse))
            {
                return false;
            }
            if (!CreateCookie("sAccessType", cUser.AccessType.ToString(), httprequest, htpresponse))
            {
                return false;
            }
            //Waqas Javed 5057 03/17/2009 UserLocation cookie was not being used
            //if (!CreateCookie("sUserLocation", cUser.UserLocation.ToString(), httprequest, htpresponse))
            //{
            //    return false;
            //}
            if (!CreateCookie("sIsSPNUser", cUser.IsSPNUser.ToString(), httprequest, htpresponse))
            {
                return false;
            }

            //In order to save userid, password
            if (!CreateCookie("sUserID", cUser.LoginID, httprequest, htpresponse))
            {
                return false;
            }
            if (!CreateCookie("Password", cUser.Password, httprequest, htpresponse))
            {
                return false;
            }
            //Creating Session variables On Start 		            
            if (!CreateCookie("sEmpName", cUser.LastName + ", " + cUser.FirstName, httprequest, htpresponse))
            {
                return false;
            }
            if (!CreateCookie("sCanUpdateCloseOut", cUser.canUpdateCloseOut.ToString(), httprequest, htpresponse))
            {
                return false;
            }
            return true;
        }
#endregion

		public clsSession()
		{
			
		}
	}
}

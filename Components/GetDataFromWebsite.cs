using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using MSXML2;
using System.Text.RegularExpressions;
//using HCM.Services;

namespace lntechNew.Components
{
    public class GetDataFromWebsite
    {
        string Response;

        private string GetdataFromWebsite(string TicketNum)
        {
            try
            {
               // string url = "http://www.jp.hctx.net/PublicAccess/SearchValidator";
                string url = "https://www.texasonline.state.tx.us/NASApp/rap/BaseRap";  
                XMLHTTP ObjHttp = new XMLHTTP();
                //Connecting to site
                ObjHttp.open("post", url, false, null, null);
                //Sending parameters
                //ObjHttp.send("case=" + TicketNum + "&dl= &lname= &dob= &court=All Courts&language=en");
                ObjHttp.send("GXHC_gx_session_id_=GTvhWCnxpxm64fqMLH1hfpgjP0cvGdk4J2BHyyW72dNC8PZ88TTp!-247518291!NONE!1184080442577" +
                    "&_instname= &_currpage=logon &_pageid=1 &_sign=open &_pageclass=RapLogon &_selector= &_nextpage=summary.jsp");

                if (ObjHttp.status.ToString() == "200")
                {
                    return ObjHttp.responseText;
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
              //  ErrorLogger.Log("GetdataFromWebsite", ex.Message);
                return "";
            }
        }

        public string StartProcessing(string TicketNum)
        {
            Response = GetdataFromWebsite(TicketNum);

            //Parse Data
            if (Response.ToString() != "")
            {
               // DT = ResponseParser(Response);
              //  SaveTicketNumInfoToDB(DT);
            }
            return Response;
        }
    }
}

using System;
using System.Data;
using System.Collections;
//using mailer.localhost;
using lntechNew.com.legalhouston.zp4;
using System.Configuration;

namespace lntechNew.Components
{
	/// <summary>
	/// Summary description for ZP4Wrapper.
	/// </summary>
	public class ZP4Wrapper
	{		
		Hashtable ht = new Hashtable();		
		Service1 cadd1 = new Service1();	
		public ZP4Wrapper()
		{	
			ht.Add("YDSADDRESS","");
			ht.Add("YDSCITY","");
			ht.Add("YDSSTATE","");
			ht.Add("YDSZIP","");
			ht.Add("YDSBARCODE","");
			ht.Add("YDS","");
			ht.Add("YDSSUCCESS","");
            ht.Add("YDSAPPARTMENT", "");
            cadd1.Url = ConfigurationSettings.AppSettings["ZP4Path"].ToString();
		}
		public Hashtable ZP4YDS (string ADDRESS,string CITY, string STATE, string ZIP)
		{
			try
			{			
				//DataSet DSYDS= (DataSet) cadd1.getYDS(ADDRESS,CITY,STATE,ZIP);
                ht["YDSADDRESS"] = ADDRESS;
                ht["YDSCITY"] = CITY;
                ht["YDSSTATE"] = STATE;
                ht["YDSZIP"] = ZIP;
                ht["YDSBARCODE"] = "998";
                ht["YDS"] = "Y";
                ht["YDSSUCCESS"] = "true";
                ht["YDSAPPARTMENT"] = "";



                #region Sabir Khan 28-12-2014 Code commented
                //if (DSYDS.Tables[0].Rows[0]["success"].ToString()== "true")		

                //{

                //    if (DSYDS.Tables[0].Rows[0]["YDSstrdpv"].ToString() != "N")	
                //    {

                //        if(DSYDS.Tables[0].Rows[0]["YDSstraddress"].ToString() !="")
                //        {
                //            ht["YDSADDRESS"]=DSYDS.Tables[0].Rows[0]["YDSstraddress"].ToString(); 
                //        }
                //        if(DSYDS.Tables[0].Rows[0]["YDSstrcity"].ToString()!="")
                //        {
                //            ht["YDSCITY"]=DSYDS.Tables[0].Rows[0]["YDSstrcity"].ToString(); 
                //        }
                //        if(DSYDS.Tables[0].Rows[0]["YDSstrstate"].ToString()!="")
                //        {
                //            ht["YDSSTATE"]=DSYDS.Tables[0].Rows[0]["YDSstrstate"].ToString(); 
                //        }
                //        if(DSYDS.Tables[0].Rows[0]["YDSstrzip"].ToString()!="")
                //        {
                //            ht["YDSZIP"]=DSYDS.Tables[0].Rows[0]["YDSstrzip"].ToString(); 
                //        }
                //        ht["YDSBARCODE"]=DSYDS.Tables[0].Rows[0]["YDSstrbarcode"].ToString();				
                //        ht["YDS"]=DSYDS.Tables[0].Rows[0]["YDSstrdpv"].ToString();
                //        ht["YDSSUCCESS"]=DSYDS.Tables[0].Rows[0]["success"].ToString();
                //        ht["YDSAPPARTMENT"] = DSYDS.Tables[0].Rows[0]["YDSstrapt"].ToString();


                //        return ht;
                //    }
                //    ht["YDSSUCCESS"]=DSYDS.Tables[0].Rows[0]["success"].ToString();
                //    ht["YDS"]=DSYDS.Tables[0].Rows[0]["YDSstrdpv"].ToString();								 
                //    return ht;
                //}
                //ht["YDSSUCCESS"]=DSYDS.Tables[0].Rows[0]["success"].ToString();
                #endregion
                
				return ht;
			}
			catch
			{
				ht["YDSSUCCESS"]="false";
				return ht;
				
			}
		}
	}
}
	


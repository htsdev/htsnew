<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ScanDockets.aspx.cs" Inherits="lntechNew.ReturnedDockets.ScanDockets" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<HEAD>
		<title>Scan Dockets</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<SCRIPT src="../Scripts/Dates.js" type="text/javascript"></SCRIPT>
		<style type="text/css">A:link { COLOR: #0066cc; TEXT-DECORATION: none }
	A:visited { COLOR: #0066cc; TEXT-DECORATION: none }
	A:active { TEXT-DECORATION: none }
	A:hover { COLOR: #0066cc; TEXT-DECORATION: underline }
	.style1 { COLOR: #ffffff }
	.style2 { FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-STYLE: normal; FONT-FAMILY: Tahoma }
	.style4 { FONT-SIZE: 8pt }
		</style>
		<%=Session["objTwain"].ToString()%>
		
	</HEAD>
<body>
        <form id="Form1" runat="server" method="post">
            <table id="TableMain" align="center" border="0" cellpadding="0" cellspacing="0" width="780">
                <tr>
                    <td colspan="2" width=100%><uc1:activemenu id="ActiveMenu1" runat="server"></uc1:activemenu>
                    </td>
                </tr>
            </table>
            <table id="Table1" align="center" border="0" cellpadding="0" cellspacing="0" width="780">
                <tr>
                    <td background="../Images/separator_repeat.gif" colspan="2" height="11" width="780">
                    </td>
                </tr>
                <tr>
                    <td background="../Images/subhead_bg.gif" class="clssubhead" colspan="2" style="width: 780px;
                        height: 35px">
                        &nbsp; Scan Dockets</td>
                </tr>
                <tr>
                    <td align="left" class="clsLeftPaddingTable" colspan="2" height="11">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td id="new" colspan="2">
                        <table width="780">
                            <tr>
                                <td class="clsLeftPaddingTable" style="width: 909px; height: 22px">
                                    Date of Docket:&nbsp;<ew:calendarpopup id="cal_DocketDate" runat="server" allowarbitrarytext="False"
                                        calendarlocation="Bottom" controldisplay="TextBoxImage" culture="(Default)" enablehidedropdown="True"
                                        font-names="Tahoma" font-size="8pt" imageurl="../images/calendar.gif" padsingledigits="True"
                                        showcleardate="True" showgototoday="True" text=" " tooltip="Call Back Date" upperbounddate="12/31/9999 23:59:00"
                                        width="80px"> <TEXTBOXLABELSTYLE CssClass="clstextarea" /><WEEKDAYSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black" BackColor="White" /><MONTHHEADERSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black" BackColor="Yellow" /><OFFMONTHSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray" BackColor="AntiqueWhite" /><GOTOTODAYSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black" BackColor="White" /><TODAYDAYSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black" BackColor="LightGoldenrodYellow" /><DAYHEADERSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black" BackColor="Orange" /><WEEKENDSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black" BackColor="LightGray" /><SELECTEDDATESTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black" BackColor="Yellow" /><CLEARDATESTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black" BackColor="White" /><HOLIDAYSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black" BackColor="White" /></ew:calendarpopup>
                                    &nbsp;&nbsp; Attorney:
                                    <asp:DropDownList ID="ddl_Attorney" runat="server" CssClass="clsinputcombo">
                                    </asp:DropDownList></td>
                                <td align="left" class="clsLeftPaddingTable" style="width: 380px; height: 22px">
                                    <input id="btnScn" runat="server" class="clsbutton" name="Button1" onclick="JavaScript: StartScan();"
                                        onserverclick="btnScn_ServerClick1" style="width: 72px; height: 20px" type="button"
                                        value="Scan Now" />
                                    &nbsp;&nbsp;
                                    <input id="Adf" runat="server" checked="checked" name="Adf" type="checkbox" />Use
                                    Feeder
                                </td>
                            </tr>
                            <tr>
                                <td align="center" class="clsLeftPaddingTable" colspan="2">
                                    &nbsp;<asp:Label ID="lblMessage" runat="server" ForeColor="Red" Height="16px"></asp:Label></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        &nbsp;
                        </td>
                </tr>
                <tr>
                    <td background="../Images/separator_repeat.gif" colspan="2" height="11" width="780">
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="2" style="width: 760px">
                        <uc1:footer id="Footer1" runat="server"></uc1:footer>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="visibility: hidden">
                        <asp:TextBox ID="txtImageCount" runat="server"></asp:TextBox><asp:TextBox ID="txtQuery"
                            runat="server"></asp:TextBox><asp:TextBox ID="txtempid" runat="server"></asp:TextBox><asp:TextBox
                                ID="txtsessionid" runat="server"></asp:TextBox>
                        <asp:TextBox ID="txtSrv" runat="server"></asp:TextBox>
                        <asp:TextBox ID="txtbID" runat="server"></asp:TextBox>
                        <asp:TextBox ID="txtnoofscandoc" runat="server">1</asp:TextBox></td>
                </tr>
            </table>
            
            </form>             
        
    	<DIV id="ErrorString"></DIV>
		<IMG height="1" src="" width="1" name="Img1"> 
		<!-- added by ozair-->
		<SCRIPT language="JavaScript">
function StartScan()
{
	if(document.getElementById("ddl_Attorney").value=="-1")
	{
		alert("Plese Select Attorney.");
		document.getElementById("ddl_Attorney").focus();
		return false;
	}

    var sid= document.getElementById("txtsessionid").value;
	var eid= document.getElementById("txtempid").value;
	var path='<%=ViewState["vNTPATHScanDocketTemp"]%>';
	var Type='network';		
	var AutoFeed=1;
	var sel=OZTwain1.SelectScanner();
	if(sel=="Success")
	{
	    if (document.Form1.Adf.checked == true)
	    {
	        AutoFeed=-1;
	    }	    
	    
	    OZTwain1.Acquire(sid,eid,path,Type,AutoFeed);
	    //
	    if (document.Form1.Adf.checked == false)
	      {
		    var x= confirm("Do you want to scan more pages [OK] Yes [Cancel] NO.");
		    if(x)
		    {
			    //OZTwain1.Acquire(sid,eid,path,Type);
			    StartScan();
		    }	
		    else
		    {
		        document.Form1.Adf.checked = true;
		    }		
	      }
    }
    else if(sel=="Cancel")
    {
        alert("Operation canceled by user!");
        return false;
    }
    else 
    {
        alert("Scanner not installed.");
	    return false;
    }
    
}
		</SCRIPT>
		<!-- added by ozair-->
</body>
</html>

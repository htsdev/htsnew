using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.ReturnedDockets
{
    public partial class DocketsReports : System.Web.UI.Page
    {
        clsSession ClsSession = new clsSession();
        clsLogger clog = new clsLogger();
        clsENationWebComponents ClsDB = new clsENationWebComponents();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                  if (ClsSession.IsValidSession(this.Request) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else //To stop page further execution
                {

                    lbl_error.Text = "";

                    if (!IsPostBack)
                    {
                        btnSearch.Attributes.Add("OnClick", "return validatepage();");
                        BindControls();
                        try
                        {
                            DateTime dt= Convert.ToDateTime(Request.QueryString["dt"].ToString());
                            cal_StartDate.SelectedDate = dt;
                            cal_EndDate.SelectedDate = dt;
                            ddl_SearchBy.SelectedIndex = 1;
                            BindGrid();
                        }
                        catch
                        {
                        }
                        
                        if (ClsSession.GetCookie("sAccessType", this.Request).ToString() != "2")
                        {
                            dgresult.Columns[5].Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lbl_error.Text = ex.ToString();
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        void BindControls()
        {
            try
            {
                cal_StartDate.SelectedDate = DateTime.Today;
                cal_EndDate.SelectedDate = DateTime.Today;
                DataSet ds = ClsDB.Get_DS_BySP("USP_HTS_GET_ATTORNEYNAME");
                ddl_Attorney.Items.Clear();
                ddl_Attorney.Items.Add(new ListItem("Select", "0"));
                string Name = String.Empty;
                string ID = String.Empty;
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Name = ds.Tables[0].Rows[i]["Name"].ToString().Trim();
                    ID = ds.Tables[0].Rows[i]["ID"].ToString().Trim();
                    ddl_Attorney.Items.Add(new ListItem(Name, ID));
                }
                //Added by Ozair For Task 2691 on 01/25/2008
                Name = "Other Attorney Docket";
                ID = "99";
                ddl_Attorney.Items.Add(new ListItem(Name, ID));
                //end task 2691

                Name = String.Empty;
                ID = String.Empty;

                DataSet ds1 = ClsDB.Get_DS_BySP("USP_HTS_GET_EMPLOYEENAME");
                ddl_Emp.Items.Clear();
                ddl_Emp.Items.Add(new ListItem("Select", "0"));                
                for (int i = 0; i < ds1.Tables[0].Rows.Count; i++)
                {
                    Name = ds1.Tables[0].Rows[i]["Name"].ToString().Trim();
                    ID = ds1.Tables[0].Rows[i]["ID"].ToString().Trim();
                    ddl_Emp.Items.Add(new ListItem(Name, ID));
                }
            }
            catch (Exception ex)
            {
                lbl_error.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        private void BindGrid()
        {
            string[] keys ={ "@Type", "@DateFrom", "@DateTo", "@AttorneyID", "@EmpID" };
            object[] values ={ Convert.ToInt32(ddl_SearchBy.SelectedValue), cal_StartDate.SelectedDate.ToShortDateString(), cal_EndDate.SelectedDate.ToShortDateString(), Convert.ToInt32(ddl_Attorney.SelectedValue), Convert.ToInt32(ddl_Emp.SelectedValue) };


            // Getting List of Dockets	

            DataSet ds = ClsDB.Get_DS_BySPArr("usp_hts_search_scandocket", keys, values);

            if (ds.Tables.Count > 0)
            {

                if (ds.Tables[0].Rows.Count > 0)
                {
                    dgresult.DataSource = ds.Tables[0];
                    dgresult.DataBind();
                    GenerateSerial();
                }
                else
                {
                    dgresult.DataSource = "";
                    dgresult.DataBind();
                    lbl_error.Text = "No Record Found";
                }

            }
            else
                lbl_error.Text = "No Record Found";
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid();                
            }

            catch (Exception ex)
            {
                lbl_error.Text = ex.ToString();
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        private void GenerateSerial()
        {
            // Procedure for writing serial numbers in "S.No." column in data grid.......	

            long sNo = (dgresult.CurrentPageIndex) * (dgresult.PageSize);
            try
            {
                // looping for each row of record.............
                foreach (DataGridItem ItemX in dgresult.Items)
                {
                    sNo += 1;
                    // setting text of hyper link to serial number after bouncing of hyper link...
                    ((Label)(ItemX.FindControl("lbl_sno"))).Text = (string)Convert.ChangeType(sNo, typeof(string));

                    Label docid = (Label)ItemX.FindControl("lbl_docid");

                    ((LinkButton)ItemX.FindControl("lnkbtn_docdate")).Attributes.Add("OnClick", "return PopUpCallBack(" + docid.Text + "  );");
                    ((ImageButton)ItemX.FindControl("img_Delete")).Attributes.Add("OnClick", "return CheckDel();");
                    //((Label)(ItemX.FindControl("lbl_pageno"))).Text += "  Pages";

                }
            }
            catch (Exception ex)
            {
                lbl_error.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void dgresult_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Del")
                {
                    string[] key ={ "@ID" };
                    object[] value1 ={ Convert.ToInt32(e.CommandArgument.ToString()) };
                    ClsDB.InsertBySPArr("usp_hts_delete_scandocket", key, value1);
                    BindGrid();
                }
            }
            catch(Exception ex)
            {
                lbl_error.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }	
    }
}

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DocketsReports.aspx.cs" Inherits="lntechNew.ReturnedDockets.DocketsReports" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Scanned Dockets</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <SCRIPT src="../Scripts/dates.js" type="text/javascript"></SCRIPT>
    <script type="text/javascript">
		

function isDate()
{
     
	var startdate = document.getElementById("cal_StartDate").value;
	var enddate = document.getElementById("cal_EndDate").value;
	
	if(validate_date(enddate)==true && validate_date(startdate)==true)
	 {
	    return true;
	 }
	 
	 else
	 {
	    return false;
	 }
	
}
function validate_date(inputwidget)
{
    
	var datestring = inputwidget;
	var first = datestring.indexOf('/');
	var last = datestring.lastIndexOf('/');

    if ( first == -1 ) 
    {
        alert("The date entered is not valid and date must be in the mm/dd/yyyy format.");
		return false;
    }
    if ( last == -1 ) 
    {
        alert("The date entered is not valid and date must be in the mm/dd/yyyy format.");
	    return false;
    }
    

	    var daypart= datestring.substring(0, first);
	    var monthpart  = datestring.substring(first + 1, last);
	    var yearpart = datestring.substring(last + 1, datestring.length);
	   
    	
	    if ((datestring.length > 0) && 
	         ((yearpart.length < 4) || (yearpart.length>4) || (yearpart<1900) || !isvaliddate(yearpart, monthpart, daypart)))  
	        
	    {
		   
		    inputwidget.value = "";
		    alert("The date entered is not valid and date must be in the mm/dd/yyyy format.");
		    return false;
	    }
	    else if(isNaN(yearpart))
	    {
	        alert("The date entered is not valid and date must be in the mm/dd/yyyy format.");
	        return false;
	    }
	    else if(isNaN(monthpart))
	    {
	        alert("The date entered is not valid and date must be in the mm/dd/yyyy format.");
	        return false;
	    }
	    else if(isNaN(daypart))
	    {
	        alert("The date entered is not valid and date must be in the mm/dd/yyyy format.");
	        return false;
	    }
	    else
	    {
	
		    return true;
	    }
}

function isvaliddate(year, month, day)
{
	//return (month >= 1) && (month <= 12) && (day >= 1) && (day <= mostdaysinmonth(month, year));
	return (day >= 1) && (day <= 12) && (month >= 1) && (month <= mostdaysinmonth(day, year));
}
//End of Date validation

		function PopUpCallBack(DocID)
			{
				
				window.open ("PreviewPDF.aspx?docid="+ DocID);
				return false;
			
			}
			function CheckDel()
			{
				
				var x=confirm("Are you sure you want to delete this docket! \r\n Click [OK] for yes or [Cancel] for No.");
				if(x==false)
				{
				    return false;
				}				
			}
		function validatepage()
		{
			//a;
			var d1 = document.getElementById("cal_StartDate").value;
			var d2 = document.getElementById("cal_EndDate").value;			
			
			if (d1 == d2)
				return true;
			if (compareDates(d2,'MM/dd/yyyy',d1,'MM/dd/yyyy')==false)
				{
					alert("Please Enter start date less than end date");
					document.getElementById("cal_EndDate").focus(); 
					return false;
				}
				return true;
		}
		function CalendarValidation()
		{ 
			var dtStartdate=document.getElementById("cal_StartDate").value;
			var dtEndDate=document.getElementById("cal_EndDate").value;
			if(dtStartdate > dtEndDate)
			{ 
				alert("PLease Specify Valid Future Date!");
				return false;
			}
		}
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <TABLE id="TableMain" 
				cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
				<TR>
					<TD style="WIDTH: 815px" colSpan="4">
						<uc1:ActiveMenu id="ActiveMenu1" runat="server"></uc1:ActiveMenu></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 816px" colSpan="4">
						<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
							<TR>
								<TD width="100%" background="../Images/separator_repeat.gif" colSpan="4" height="10"></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 46px" colSpan="4">
									<TABLE id="Table2" style="WIDTH: 784px; HEIGHT: 14px" cellSpacing="1" cellPadding="1" width="784"
										border="0">
										<TR>
											<TD class="clsLeftPaddingTable" ><STRONG>Start Date:</STRONG></TD>
											<TD class="clsLeftPaddingTable" >
												<ew:calendarpopup id="cal_StartDate" runat="server" PadSingleDigits="True"
													ShowClearDate="True" Culture="(Default)" AllowArbitraryText="True" ShowGoToToday="True" CalendarLocation="Left"
													ImageUrl="../images/calendar.gif" ControlDisplay="TextBoxImage" Width="99px" Font-Size="8pt" Font-Names="Tahoma" EnableHideDropDown="True" DisableTextboxEntry="False">
													<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
													<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="White"></WeekdayStyle>
													<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="Yellow"></MonthHeaderStyle>
													<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
														BackColor="AntiqueWhite"></OffMonthStyle>
													<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="White"></GoToTodayStyle>
													<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="LightGoldenrodYellow"></TodayDayStyle>
													<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="Orange"></DayHeaderStyle>
													<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="LightGray"></WeekendStyle>
													<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="Yellow"></SelectedDateStyle>
													<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="White"></ClearDateStyle>
													<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="White"></HolidayStyle>
												</ew:calendarpopup></TD>
											<TD class="clsLeftPaddingTable" style="WIDTH: 131px; HEIGHT: 23px">
                                                <strong>End Date:</strong></TD>
											<TD class="clsLeftPaddingTable" style="WIDTH: 128px; HEIGHT: 23px"><ew:calendarpopup id="cal_EndDate" runat="server" PadSingleDigits="True" ShowClearDate="True"
													Culture="(Default)" AllowArbitraryText="True" ShowGoToToday="True" CalendarLocation="Left" ImageUrl="../images/calendar.gif" ControlDisplay="TextBoxImage"
													Width="102px" Font-Size="8pt" Font-Names="Tahoma" EnableHideDropDown="True" DisableTextboxEntry="False">
													<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
													<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="White"></WeekdayStyle>
													<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="Yellow"></MonthHeaderStyle>
													<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
														BackColor="AntiqueWhite"></OffMonthStyle>
													<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="White"></GoToTodayStyle>
													<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="LightGoldenrodYellow"></TodayDayStyle>
													<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="Orange"></DayHeaderStyle>
													<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="LightGray"></WeekendStyle>
													<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="Yellow"></SelectedDateStyle>
													<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="White"></ClearDateStyle>
													<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="White"></HolidayStyle>
												</ew:calendarpopup></TD>
											<TD class="clsLeftPaddingTable" style="WIDTH: 106px; HEIGHT: 23px">
                                                <strong>Search By:</strong></TD>
											<TD class="clsLeftPaddingTable" style="HEIGHT: 23px"><asp:dropdownlist id="ddl_SearchBy" runat="server" Width="127px" CssClass="ClsInputCombo" >
                                                <asp:ListItem Selected="True" Value="1">Scanned Date</asp:ListItem>
                                                <asp:ListItem Value="2">Docket Date</asp:ListItem>
                                            </asp:DropDownList></TD>
										</TR>
										<TR>
											<TD class="clsLeftPaddingTable" style="WIDTH: 131px"><STRONG>Attorney Name:</STRONG></TD>
											<TD class="clsLeftPaddingTable" ><asp:dropdownlist id="ddl_Attorney" runat="server" Width="127px" CssClass="ClsInputCombo">
                                            </asp:DropDownList></TD>
											<TD class="clsLeftPaddingTable" style="WIDTH: 131px">
                                                <strong>Employee Name:</strong></TD>
											<TD class="clsLeftPaddingTable" style="WIDTH: 128px"><STRONG><asp:dropdownlist id="ddl_Emp" runat="server" Width="127px" CssClass="ClsInputCombo">
                                            </asp:DropDownList></STRONG></TD>
											<TD class="clsLeftPaddingTable" style="WIDTH: 106px"><asp:button OnClientClick="return isDate();" id="btnSearch" runat="server" CssClass="clsbutton" Text="Submit" OnClick="btnSearch_Click"></asp:button></TD>
											<TD class="clsLeftPaddingTable"></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 11px" align="left" background="../Images/separator_repeat.gif" colSpan="4">&nbsp;
								</TD>
							</TR>
							<TR>
								<TD align="left" colSpan="4"><asp:datagrid id="dgresult" runat="server" Width="784px" CssClass="clsleftpaddingtable" AutoGenerateColumns="False"
										BorderColor="White" BorderStyle="None" OnItemCommand="dgresult_ItemCommand">
										<Columns>
											<asp:TemplateColumn HeaderText="S.No" >
												<HeaderStyle HorizontalAlign="Left" Width=25px CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id="lbl_sno" runat="server" CssClass="label"></asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Scan Date">
												<HeaderStyle HorizontalAlign="Left"  CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id=lblScanDate runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.ScanDate") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Docket Date">
												<HeaderStyle HorizontalAlign="Left"  CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:LinkButton id=lnkbtn_docdate runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DocketDate", "{0:d}") %>'>
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Attorney Name">
												<HeaderStyle HorizontalAlign="Left"  CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id=lbl_attname runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.Attorney") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Pages">
												<HeaderStyle HorizontalAlign="Left"  CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id=lbl_pageno runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.DocCount") %>'>
													</asp:Label>
													<asp:Label id=lbl_docid runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.bookid") %>' Visible="False">
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Delete">
                                            <HeaderStyle HorizontalAlign="Left"  CssClass="clsaspcolumnheader"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="img_Delete" ImageUrl="~/Images/DELETE.jpg" runat="server" CommandName="Del" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.bookid") %>' />
												</ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Emp Abb">
                                            <HeaderStyle HorizontalAlign="Left"  CssClass="clsaspcolumnheader"></HeaderStyle>
                                                <ItemTemplate>
													<asp:Label id=lbl_EmpAbb runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.Abbreviation") %>'>
													</asp:Label>
												</ItemTemplate>
                                            </asp:TemplateColumn>
										</Columns>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD width="100%" background="../Images/separator_repeat.gif" colSpan="4" height="11"></TD>
							</TR>
							<TR>
								<TD align="center" width="100%" colSpan="4" height="11">
									<asp:label id="lbl_error" runat="server" ForeColor="Red"></asp:label></TD>
							</TR>
							
							<TR>
								<TD style="WIDTH: 760px" align="left" colSpan="4">
									<uc1:Footer id="Footer1" runat="server"></uc1:Footer></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
    </div>
    </form>
</body>
</html>

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using System.IO;

namespace lntechNew.ReturnedDockets
{
    public partial class ScanDockets : System.Web.UI.Page
    {
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        clsSession ClsSession = new clsSession();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (ClsSession.IsValidSession(this.Request) == false)
                {
                    Response.Redirect("../frmlogin.aspx");
                }
                else //To stop page further execution
                {

                    if (!(Page.IsPostBack))
                    {
                        //network path
                        ViewState["vNTPATHScanDocketTemp"] = ConfigurationSettings.AppSettings["NTPATHScanDocketTemp"].ToString();
                        ViewState["vNTPATHScanDocketImage"] = ConfigurationSettings.AppSettings["NTPATHScanDocketImage"].ToString();
                        //
                        string strServer;
                        strServer = "http://" + Request.ServerVariables["SERVER_NAME"];
                        txtSrv.Text = strServer;
                        Session["objTwain"] = "<OBJECT id='OZTwain1' classid='" + strServer + "/OZTwain_1.dll#OZTwain.OZTwain' height='1' width='1' VIEWASTEXT> </OBJECT>";//for scanning function call in javascript.

                        BindControls();
                        txtsessionid.Text = Session.SessionID.ToString();
                        txtempid.Text = ClsSession.GetCookie("sEmpID", this.Request);
                    }
                    
                }


            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }
        void BindControls()
        {
            try
            {
                cal_DocketDate.SelectedDate = DateTime.Today;
                DataSet ds = ClsDb.Get_DS_BySP("USP_HTS_GET_ATTORNEYNAME");
                ddl_Attorney.Items.Clear();
                ddl_Attorney.Items.Add(new ListItem("Select", "-1"));
                string Name = String.Empty;
                string ID = String.Empty;
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Name = ds.Tables[0].Rows[i]["Name"].ToString().Trim();
                    ID = ds.Tables[0].Rows[i]["ID"].ToString().Trim();
                    ddl_Attorney.Items.Add(new ListItem(Name, ID));                    
                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }
        private string[] sortfilesbydate(string[] fileName)
        {
            for (int i = 0; i < fileName.Length; i++)
            {
                DateTime cDateTime = File.GetCreationTime(fileName[i]);
                for (int j = i + 1; j < fileName.Length; j++)
                {
                    DateTime cDateTime1 = File.GetCreationTime(fileName[j]);
                    if (DateTime.Compare(cDateTime1, cDateTime) < 0)
                    {
                        string fname = fileName[j];
                        fileName[j] = fileName[i];
                        fileName[i] = fname;
                        i = -1;
                        break;
                    }
                }
            }
            return fileName;
        }
        protected void btnScn_ServerClick1(object sender, EventArgs e)
        {
            try
            {
                string searchpat = "*" + Session.SessionID + txtempid.Text + "*.jpg";
                string[] fileName = Directory.GetFiles(ViewState["vNTPATHScanDocketTemp"].ToString(), searchpat);
                //
                if (fileName.Length > 1)
                {
                    fileName = sortfilesbydate(fileName);
                }
                //
                int empid = Convert.ToInt32(txtempid.Text); 
                int attorney=Convert.ToInt32(ddl_Attorney.SelectedValue);
                int BookId = 0, picID = 0;
                if (txtbID.Text.Length > 0)
                {
                    BookId = Convert.ToInt32(txtbID.Text);
                }
                string picName, picDestination;

                int DocCount = 1;
                for (int i = 0; i < fileName.Length; i++)
                {
                    string[] key = { "@ScanDate", "@DocketDate", "@AttorneyID", "@EmpID", "@Extension", "@Count", "@Book", "@BookID" };
                    object[] value1 = { DateTime.Now, cal_DocketDate.SelectedDate, attorney, empid, "JPG", DocCount, BookId, "" };
                    //call sP and get the book ID back from sP
                    picName = ClsDb.InsertBySPArrRet("usp_hts_insert_ScanDocket", key, value1).ToString();
                    string BookI = picName.Split('-')[0];
                    string picI = picName.Split('-')[1];
                    BookId = (int)Convert.ChangeType(BookI, typeof(int)); ; //
                    picID = (int)Convert.ChangeType(picI, typeof(int)); ; //
                    DocCount = DocCount + 1;
                    //Move file
                    picDestination = ViewState["vNTPATHScanDocketImage"].ToString() + picName + ".jpg"; //DestinationImage
                    System.IO.File.Copy(fileName[i].ToString(), picDestination);
                    System.IO.File.Delete(fileName[i].ToString());
                }

                ddl_Attorney.ClearSelection();
                cal_DocketDate.SelectedDate = DateTime.Today;
                
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }
    }
}

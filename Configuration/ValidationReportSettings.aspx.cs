﻿using System;
using System.Web.UI.WebControls;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;

namespace HTP.Configuration
{
    public partial class ValidationReportSettings : System.Web.UI.Page
    {
        readonly ValidationReports _reports = new ValidationReports();
        readonly clsLogger _bugTracker = new clsLogger();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack == false)
            {

                gv_Records.PageIndex = 0;
                GridViewSortDirection = SortDirection.Descending;
                GridViewSortExpression = "Title";
                FillGrid();
            }
            Pagingctrl.PageIndexChanged += Pagingctrl_PageIndexChanged;
            Pagingctrl.PageSizeChanged += Pagingctrl_PageSizeChanged;
            Pagingctrl.GridView = gv_Records;
        }
        private void FillGrid()
        {
            _reports.getRecords("usp_HTS_Get_ValidationReportSettings");
            var ds = _reports.records;
            if (ds.Tables[0].Rows.Count > 0)
            {
                ds.Tables[0].Columns.Add("sno");
                var dv = ds.Tables[0].DefaultView;
                dv.Sort = GridViewSortExpression + " " + (GridViewSortDirection == SortDirection.Ascending ? "ASC" : "DESC");
                var dt = dv.ToTable();
                //GenerateSerialNo(dt);
                Pagingctrl.GridView = gv_Records;
                gv_Records.DataSource = dt;
                gv_Records.DataBind();
            }
            else    //SAEED-7752-05/06/2010, if no record found set gridview to null, so that it will display "No Records Found"  message.
            {
                gv_Records.DataSource = null;
                gv_Records.DataBind();
            }

            Pagingctrl.PageCount = gv_Records.PageCount;
            Pagingctrl.PageIndex = gv_Records.PageIndex;
            Pagingctrl.SetPageIndex();
        }
        protected void gv_Records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_Records.PageIndex = e.NewPageIndex;
                FillGrid();
            }
            catch (Exception ex)
            {

                _bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        
        protected void gv_Records_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                var sortExpression = e.SortExpression;

                if (GridViewSortDirection == SortDirection.Ascending && sortExpression == GridViewSortExpression)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                else
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }

                GridViewSortExpression = sortExpression;
                FillGrid();
                Pagingctrl.PageCount = gv_Records.PageCount;
                Pagingctrl.PageIndex = gv_Records.PageIndex;
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
            }
        }

        private SortDirection GridViewSortDirection
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Ascending;
                return (SortDirection)ViewState["sortDirection"];
            }
            set { ViewState["sortDirection"] = value; }

        }

        private string GridViewSortExpression
        {
            get
            {
                if (ViewState["sortExpression"] == null)
                    ViewState["sortExpression"] = "";
                return ViewState["sortExpression"].ToString();
            }
            set { ViewState["sortExpression"] = value; }

        }
        
        void Pagingctrl_PageSizeChanged(int pageSize)
        {

            {
                if (pageSize > 0)
                {
                    gv_Records.PageIndex = 0;
                    gv_Records.PageSize = pageSize;
                    gv_Records.AllowPaging = true;
                }
                else
                {
                    gv_Records.AllowPaging = false;
                }
                FillGrid();
            }
        }
        void Pagingctrl_PageIndexChanged()
        {

            {
                gv_Records.PageIndex = Pagingctrl.PageIndex - 1;
                FillGrid();
            }
        }
        protected void gv_Records_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "btnclick")
            {
                var rowId = Convert.ToInt32(e.CommandArgument);
                var rptName = ((LinkButton)gv_Records.Rows[rowId].Cells[0].FindControl("img_Add")).Text;
                var cat = ((Label)gv_Records.Rows[rowId].Cells[1].FindControl("lbl_category")).Text;
                var primUser = ((Label)gv_Records.Rows[rowId].Cells[2].FindControl("lbl_primaryUser")).Text;
                var secUser = ((Label)gv_Records.Rows[rowId].Cells[3].FindControl("lbl_secondryUser")).Text;
                var isActive = ((Label)gv_Records.Rows[rowId].Cells[4].FindControl("lbl_isActive")).Text == "Yes";
                mpeTrafficwaiting.Show();
                Pagingctrl.Visible = true;

            }
        }
        protected void gv_Records_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ((LinkButton)e.Row.FindControl("img_Add")).CommandArgument = e.Row.RowIndex.ToString();
                }
            }

            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                lbl_message.Text = ex.ToString();
            }
        }

    }
    
}

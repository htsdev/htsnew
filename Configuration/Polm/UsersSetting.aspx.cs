﻿using System;
using System.Web.UI.WebControls;
using HTP.ClientController;

namespace HTP.Configuration.Polm
{
    public partial class UsersSetting : System.Web.UI.Page
    {
        PolmController htpController = new PolmController();

        #region Method
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                FillGrid();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                switch (btnAdd.Text)
                {
                    case "Add":
                        if (Add())
                        {
                            SetEmptyControl();
                            FillGrid();
                            lbl_Message.Text = "Record added successfully.";
                        }
                        else
                            lbl_Message.Text = "Record not added successfully.";
                        break;
                    case "Update":
                        if (Update())
                        {
                            SetEmptyControl();
                            FillGrid();
                            lbl_Message.Text = "Record added successfully.";
                        }
                        else
                            lbl_Message.Text = "Record not added successfully.";
                        break;
                }

            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
            }

        }

        protected void gvDivision_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                //if (e.CommandName == "image")
                //{
                //    int associatedCases = Convert.ToInt32((((HiddenField)gvUsers.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("hfcases")).Value));
                //    if (associatedCases == 0)
                //    {
                //        int ID = Convert.ToInt32((((HiddenField)gvUsers.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("hfValueId")).Value));
                //        htpController.DeleteUser(ID);
                //        FillGrid();
                //    }
                //    else
                //        lbl_Message.Text = "Bodily Damage Cannot be deleted as it is associated with case. Please update case first then remove again.";
                //}
                //else if (e.CommandName == "lnkbutton")
                //{
                //    txtUsers.Text = ((LinkButton)gvUsers.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("lnkbtnValue")).Text;
                //    ViewState["ValueID"] = ((HiddenField)gvUsers.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("hfValueId")).Value;
                //    btnAdd.Text = "Update";
                //}
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
            }
        }

        protected void gvLanguage_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.DataRow) return;
            ((LinkButton)e.Row.FindControl("lnkbtnValue")).CommandArgument = Convert.ToString(e.Row.RowIndex);
            ((ImageButton)e.Row.FindControl("ImgDelete")).CommandArgument = Convert.ToString(e.Row.RowIndex);
        }
        #endregion

        #region Method

        private void SetEmptyControl()
        {
            txtUsers.Text = string.Empty;
            chkIsActive.Checked = false;
        }

        private void FillGrid()
        {
            //DataTable dt = new DataTable();
            //dt = htpController.GetAllUser();
            //if (dt.Rows.Count > 0)
            //{
            //    gvUsers.DataSource = dt;
            //    gvUsers.DataBind();
            //}
            //else
            //{
            //    gvUsers.DataSource = null;
            //    gvUsers.DataBind();
            //}
        }

        private bool Add()
        {
            return false;
            //return htpController.AddUser(Server.HtmlEncode(txtUsers.Text), chkIsActive.Checked);
        }

        private bool Update()
        {
            return false;
            //return htpController.UpdateUser(Convert.ToInt32(ViewState["ValueID"]), Server.HtmlEncode(txtUsers.Text), chkIsActive.Checked);
        }

        #endregion
    }
}

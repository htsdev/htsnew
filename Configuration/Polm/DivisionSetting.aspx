﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DivisionSetting.aspx.cs"
    Inherits="HTP.Configuration.Polm.DivisionSetting" %>

<%@ Register Src="~/WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="~/WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Division Setting</title>
    <link href="../../Styles.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
        function ShowDeleteMessage()
        {
            if (confirm("Are you sure you want to delete?"))
                return true;
            return false;
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <aspnew:ScriptManager ID="ScriptManager1" runat="server">
        <Scripts>
            <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="~/Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
        </Scripts>
    </aspnew:ScriptManager>
    <aspnew:UpdatePanel ID="pnl_main" runat="server">
        <ContentTemplate>
            <div>
                <table id="TableMain" cellspacing="0" cellpadding="0" width="780px" align="center"
                    border="0">
                    <tr>
                        <td>
                            <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td background="../../Images/separator_repeat.gif" height="9" width="750px">
                        </td>
                    </tr>
                    <tr>
                        <td class="clsLeftPaddingTable" align="center">
                            <table style="width: 60%">
                                <tr>
                                    <td align="right">
                                        <asp:Label ID="lblDivision" runat="server" CssClass="clssubhead" Text="Division :"></asp:Label>
                                    </td>
                                    <td align="left" valign="middle">
                                        <asp:TextBox ID="txtDivision" runat="server" CssClass="clsInputadministration" ValidationGroup="contact"
                                            MaxLength="20" Width="200px"></asp:TextBox>&nbsp;
                                        <asp:CheckBox ID="chkIsActive" Visible="false" runat="server" Text="IsActive" CssClass="clsLeftPaddingTable" />&nbsp;
                                        <asp:Button ID="btnAdd" runat="server" CssClass="clsbutton" ValidationGroup="contact"
                                            OnClick="btnAdd_Click" Text="Add" Width="60px" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td align="left" valign="middle">
                                        <asp:ValidationSummary ID="ValidationSummary1" CssClass="clssubhead" DisplayMode="List"
                                            runat="server" ValidationGroup="contact" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" CssClass="clssubhead" ControlToValidate="txtDivision"
                                            runat="server" Display="None" ErrorMessage="Please enter division." ValidationGroup="contact"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" CssClass="clssubhead"
                                            runat="server" ValidationGroup="contact" Display="None" ErrorMessage="Please insert valid division."
                                            ControlToValidate="txtDivision" ValidationExpression="^[ a-zA-Z]+$" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../../Images/separator_repeat.gif" height="9" width="750px">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="819px" height="34px" background="../../Images/subhead_bg.gif" class="clssubhead">
                                <tr>
                                    <td align="left" style="width: 50%" class="clssubhead">
                                        &nbsp;Division &nbsp;&nbsp;
                                    </td>
                                    <td align="right" style="width: 50%" class="clssubhead">
                                        <asp:LinkButton ID="lnk_AddNewRecord" runat="server" OnClick="lnk_AddNewRecord_Click">Add New Record</asp:LinkButton>&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../../Images/separator_repeat.gif" height="9" width="750px">
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="width: 819px">
                            <asp:Label ID="lbl_Message" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                            <br />
                            <asp:GridView ID="gvDivision" runat="server" AutoGenerateColumns="False" CssClass="clsLeftPaddingTable"
                                CellPadding="3" CellSpacing="0" OnRowCommand="gvDivision_RowCommand" OnRowDataBound="gvDivision_RowDataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="S#" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="LblSno" runat="server" CssClass="clssubhead" Text='<%# Container.DataItemIndex+1 %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Division Type" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LnkbtnValue" runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'
                                                CommandName="lnkbutton"></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Active" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="LblIsActive" runat="server" CssClass="clsLabel" Text='<%# (Convert.ToInt32(Eval("IsActive")) == 0) ? "No" : "Yes" %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImgDelete" runat="server" ImageUrl="~/Images/remove2.gif" CommandName="ImgDelete"
                                                OnClientClick="return ShowDeleteMessage();" />
                                            <asp:HiddenField ID="HfValueId" runat="server" Value='<%# Eval("Id") %>' />
                                            <asp:HiddenField ID="HfIsactive" runat="server" Value='<%# Eval("IsActive") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                        <td align="center">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td background="../../Images/separator_repeat.gif" height="9" width="750px">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc1:Footer ID="Footer1" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
        <Triggers>
            <aspnew:AsyncPostBackTrigger ControlID="btnAdd" EventName="Click" />
        </Triggers>
    </aspnew:UpdatePanel>
    </form>
</body>
</html>

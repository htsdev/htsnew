﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UsersSetting.aspx.cs" Inherits="HTP.Configuration.Polm.UsersSetting" %>

<%@ Register Src="~/WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="~/WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Users Setting</title>
    <link href="../../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="TableMain" cellspacing="0" cellpadding="0" width="780px" align="center"
            border="0">
            <tr>
                <td>
                    <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td background="../../Images/separator_repeat.gif" height="9" width="750px">
                </td>
            </tr>
            <tr>
                <td class="clsLeftPaddingTable">
                    <table style="width: 100%">
                        <tr>
                            <td align="center" valign="middle">
                                <asp:Label ID="lblContactType" runat="server" CssClass="clssubhead" Text="User Role :"></asp:Label>
                                &nbsp;
                                <asp:TextBox ID="txtUsers" runat="server" CssClass="clsInputadministration" Width="200px"></asp:TextBox>&nbsp;
                                <asp:CheckBox ID="chkIsActive" runat="server" Text="IsActive" CssClass="clsLeftPaddingTable" />
                                <asp:Button ID="btnAdd" runat="server" CssClass="clsbutton" OnClick="btnAdd_Click"
                                    Text="Add" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td background="../../Images/separator_repeat.gif" height="9" width="750px">
                </td>
            </tr>
            <tr>
                <td background="../../Images/subhead_bg.gif" class="clssubhead" align="left" style="height: 34px;
                    width: 819px;">
                    &nbsp;Users
                </td>
            </tr>
            <tr>
                <td background="../../Images/separator_repeat.gif" height="9" width="750px">
                </td>
            </tr>
            <tr>
                <td align="center" style="width: 819px">
                    <asp:Label ID="lbl_Message" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                    <br />
                    <asp:GridView ID="gvUsers" runat="server" AutoGenerateColumns="False" CssClass="clsLeftPaddingTable"
                        CellPadding="3" CellSpacing="0" OnRowCommand="gvUsers_RowCommand">
                        <Columns>
                            <asp:TemplateField HeaderText="SNo">
                                <ItemTemplate>
                                    <asp:Label ID="lblSno" runat="server" CssClass="clssubhead" Text='<%# Eval("SNo") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Contact Type">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkbtnValue" runat="server" Text='<%# Eval("Damagesvalue") %>'
                                        ToolTip='<%# Eval("Damagesvalue") %>' CommandName="lnkbutton"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImgDelete" runat="server" ImageUrl="~/Images/remove2.gif" CommandArgument='<%# Eval("ID") %>'
                                        CommandName="Delete" />
                                    <asp:HiddenField ID="hfValueId" runat="server" Value='<%# Eval("DamagesvalueID") %>' />
                                    <asp:HiddenField ID="hfcases" runat="server" Value='<%# Eval("Asscociatedcase") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
                <td align="center">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td background="../../Images/separator_repeat.gif" height="9" width="750px">
                </td>
            </tr>
            <tr>
                <td>
                    <uc1:Footer ID="Footer1" runat="server" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

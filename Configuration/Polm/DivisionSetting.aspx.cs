﻿using System;
using System.Web.UI.WebControls;
using lntechNew.Components.ClientInfo;
using HTP.ClientController;

namespace HTP.Configuration.Polm
{
    /// <summary>
    /// This class represents all information about the Division Setting.
    /// </summary>
    public partial class DivisionSetting : WebComponents.BasePage
    {
        #region Variables

        //Variable Declared for POLM controller class
        readonly PolmController _htpController = new PolmController();

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Checking Access Type if Not Primary then redirecting to Login Error Page
                if (AccessType != 2)
                {
                    Response.Redirect("~/LoginAccesserror.aspx", false);
                }
                else if (!IsPostBack)
                    FillGrid();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                switch (btnAdd.Text)
                {
                    case "Add":
                        if (Add())
                        {
                            SetEmptyControl();
                            FillGrid();
                            lbl_Message.Text = "Record added successfully.";
                        }
                        else
                            lbl_Message.Text = "Record not added successfully.";
                        break;
                    case "Update":
                        if (Update())
                        {
                            SetEmptyControl();
                            FillGrid();
                            lbl_Message.Text = "Record updated successfully.";
                        }
                        else
                            lbl_Message.Text = "Record not updated successfully.";
                        break;
                }

            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        protected void gvDivision_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                lbl_Message.Text = string.Empty;
                switch (e.CommandName)
                {
                    case "ImgDelete":
                        {
                            //Ozair 7673 04/19/2010 Control Name Renamed as per standards
                            // Noufil 7673 04/19/2010  Remove association code and move logic at BO layer
                            var id = Convert.ToInt32((((HiddenField)gvDivision.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("HfValueId")).Value));

                            //Call the Polm Controller method to Delete Division.
                            if (_htpController.DeleteDivision(id))
                            {
                                SetEmptyControl();
                                lbl_Message.Text = "Record deleted successfully";
                                FillGrid();
                            }
                            // Noufil 7673 04/19/2010 Added else clause to show not deleted successfully message.
                            else
                            {
                                lbl_Message.Text = "Record not deleted successfully. Please try again later.";
                            }
                        }
                        break;
                    case "lnkbutton":
                        //Ozair 7673 04/19/2010 Control Name Renamed as per standards
                        txtDivision.Text = ((LinkButton)gvDivision.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("LnkbtnValue")).Text;
                        ViewState["ValueID"] = ((HiddenField)gvDivision.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("HfValueId")).Value;
                        chkIsActive.Checked = Convert.ToBoolean(((HiddenField)gvDivision.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("HfIsactive")).Value);
                        btnAdd.Text = "Update";
                        //Fahad 7673 04/13/2010 setting IsActive checkbox visibility to True
                        chkIsActive.Visible = true;
                        break;
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void gvDivision_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    //Ozair 7673 04/19/2010 Control Name Renamed as per standards
                    ((LinkButton)e.Row.FindControl("LnkbtnValue")).CommandArgument = Convert.ToString(e.Row.RowIndex);
                    ((ImageButton)e.Row.FindControl("ImgDelete")).CommandArgument = Convert.ToString(e.Row.RowIndex);
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        protected void lnk_AddNewRecord_Click(object sender, EventArgs e)
        {
            try
            {
                SetEmptyControl();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        #endregion

        #region Method

        /// <summary>
        /// This method used to set the default view of all the controls on the page.
        /// </summary>
        private void SetEmptyControl()
        {
            lbl_Message.Text = string.Empty;
            txtDivision.Text = string.Empty;
            chkIsActive.Checked = false;
            if (ViewState != null) ViewState["ValueID"] = String.Empty;
            btnAdd.Text = "Add";
            //Fahad 7673 04/13/2010 setting IsActive checkbox visibility to False
            chkIsActive.Visible = false;
        }

        /// <summary>
        /// This method is used to Fill Data into the Grid.
        /// </summary>
        private void FillGrid()
        {
            //Call POLM Controller method to Get All Division.
            var divisionlist = _htpController.GetAllDivision(null);

            //Checking if list is not empty or Null then bind with Grid.
            if (divisionlist != null)
            {
                gvDivision.DataSource = divisionlist;
                gvDivision.DataBind();
            }
            else
            {
                gvDivision.DataSource = null;
                gvDivision.DataBind();
            }
        }

        /// <summary>
        /// This method is used to Add/Insert New Division setting.
        /// </summary>
        /// <returns>True/False</returns>
        private bool Add()
        {
            //Fahad 7673 04/13/2010 setting IsActive to True
            //Call POLM Controller method to Add Division, , by default it is set to true.
            return _htpController.AddDivision(Server.HtmlEncode(txtDivision.Text), true);
        }

        /// <summary>
        /// This Method used to Update the existing Division setting.
        /// </summary>
        /// <returns>True/False</returns>
        private bool Update()
        {
            //Call POLM Controller method to Update Division.
            return _htpController.UpdateDivision((Convert.ToInt32(ViewState["ValueID"])), Server.HtmlEncode(txtDivision.Text), chkIsActive.Checked);
        }

        #endregion
    }
}


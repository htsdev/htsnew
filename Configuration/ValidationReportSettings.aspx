﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ValidationReportSettings.aspx.cs" Inherits="HTP.Configuration.ValidationReportSettings" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register Src="../WebControls/UpdateFollowUpInfo.ascx" TagName="UpdateFollowUpInfo"
    TagPrefix="uc3" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Validation Report Settings</title>

    <script src="../Scripts/Dates.js" type="text/javascript"></script>

    <script src="../Scripts/jsDate.js" type="text/javascript"></script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .modalBackground
        {
            background-color: Gray;
            filter: alpha(opacity=70);
            opacity: 0.7;
        }
        .modalPopup
        {
            background-color: #FFD9D5;
            border-width: 3px;
            border-style: solid;
            border-color: Gray;
            padding: 3px;
            width: 250px;
            position: static;
            left: 200px;
            top: -65px;
        }
        A:link
        {
            font-size: 8pt;
            font-family: Verdana,Arial, Helvetica, sans-serif;
            color: #4169E1;
            text-decoration: none;
            font-weight: bold;
        }
        A:Visited
        {
            font-size: 8pt;
            font-family: Verdana,Arial, Helvetica, sans-serif;
            color: #4169E1;
            text-decoration: none;
            font-weight: bold;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <aspnew:ScriptManager ID="ScriptManager1" runat="server">
            <Scripts>
                <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
            </Scripts>
        </aspnew:ScriptManager>
        <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline">
            <ContentTemplate>
                <table id="TableMain" align="center" border="0" cellpadding="0" cellspacing="0" style="z-index: 101"
                    width="800">
                    <tbody>
                        <tr>
                            <td colspan="4" style="height: 14px">
                                <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <tr>
                                    <td background="../images/separator_repeat.gif" height="11" align="center">
                                    </td>
                                </tr>
                                <tr>
                                    <td background="../Images/subhead_bg.gif" class="clssubhead" align="right" style="height: 34px">
                                        <table style="width: 100%">
                                            <tr>
                                                <td align="left" class="clssubhead" width="50%">
                                                    Validation Report Settings
                                                </td>
                                                <td style="text-align: right">
                                                     <uc3:PagingControl ID="Pagingctrl" runat="server"/>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td background="../images/separator_repeat.gif" height="11" align="center">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table id="TableGrid" bgcolor="white" border="0" cellpadding="0" cellspacing="0"
                                            width="100%">
                                            <tr>
                                                <td align="center" style="width: 100%">
                                                    <asp:Label ID="lbl_message" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="2" valign="top">
                                                    <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlResult">
                                                        <ProgressTemplate>
                                                            <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl1" runat="server" Text="Please Wait ......"
                                                                CssClass="clsLabel"></asp:Label>
                                                        </ProgressTemplate>
                                                    </aspnew:UpdateProgress>
                                                    <asp:GridView ID="gv_Records" runat="server" AutoGenerateColumns="False" Width="100%" DataKeyNames="ID"
                                                        AllowPaging="True" OnPageIndexChanging="gv_Records_PageIndexChanging" PageSize="20"
                                                        CssClass="clsLeftPaddingTable"  OnRowDataBound="gv_Records_RowDataBound"
                                                        AllowSorting="True" OnSorting="gv_Records_Sorting" EmptyDataText="No Record Found" OnRowCommand="gv_Records_RowCommand">
                                                        <Columns>
                                                            <%--<asp:TemplateField HeaderText="S#">
                                                                <ItemTemplate>
                                                                    &nbsp;<asp:HyperLink ID="lbl_sno" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                                        Text='<%# DataBinder.Eval(Container, "DataItem.sno") %>'></asp:HyperLink>
                                                                    <asp:HiddenField ID="hf_TicketID" runat="server" Value="<%# Bind('TicketID_PK') %>" />
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                            </asp:TemplateField>--%>
                                                            <asp:TemplateField HeaderText="<u>Report Name</u>" SortExpression="Title">
                                                                <ItemTemplate>
                                                                <asp:LinkButton Text='<%# Bind("Title") %>' ID="img_Add" CommandName="btnclick" runat="server"  />
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<u>Category</u>" SortExpression="Category">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_category" runat="server" CssClass="clsLeftPaddingTable" Text='<%# Bind("Category") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<u>Primary User</u>" SortExpression="PrimaryUserID">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_primaryUser" runat="server" CssClass="clsLeftPaddingTable" Text='<%# Bind("PrimaryUserID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<u>Secondry User</u>" SortExpression="SecondaryUserID">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_secondryUser" runat="server" CssClass="clsLeftPaddingTable" Text='<%# Bind("SecondaryUserID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<u>InActive</u>" SortExpression="IsActive">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_isActive" runat="server" CssClass="clsLeftPaddingTable" Text='<%# Bind("IsActive") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton Text="<img src='../Images/add.gif' border='0'/>" ID="img_Add1" CommandName="btnclick"
                                                                        runat="server" />
                                                                    <%--<asp:HiddenField ID="hf_criminal_fname" runat="server" Value='<%#Eval("FIRSTNAME") %>' />
                                                                    <asp:HiddenField ID="hf_criminal_lname" runat="server" Value='<%#Eval("LASTNAME") %>' />
                                                                    <asp:HiddenField ID="hf_criminal_ticketno" runat="server" Value='<%#Eval("TICKETNUMBER") %>' />
                                                                    <asp:HiddenField ID="hf_criminal_causeno" runat="server" Value='<%#Eval("CAUSENUMBER") %>' />
                                                                    <asp:HiddenField ID="hf_criminal_loc" runat="server" Value='<%#Eval("CourtLoc") %>' />
                                                                    <asp:HiddenField ID="hf_criminal_courtid" runat="server" Value='<%#Eval("Status") %>' />
                                                                    <asp:HiddenField ID="hf_NextFollowUpdate" runat="server" Value='<%#Eval("NextFollowUpDate") %>' />--%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <PagerStyle HorizontalAlign="Center" />
                                                        <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                            FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td background="../images/separator_repeat.gif" height="11" width="780">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Panel ID="pnlFollowup" runat="server">
                                                        <uc4:ValidationControl ID="ValidationControl" runat="server"  />
                                                    </asp:Panel>
                                                    <ajaxToolkit:ModalPopupExtender ID="mpeTrafficwaiting" runat="server" BackgroundCssClass="modalBackground"
                                                        PopupControlID="pnlFollowup" TargetControlID="btn">
                                                    </ajaxToolkit:ModalPopupExtender>
                                                    <asp:Button ID="btn" runat="server" Style="display: none;" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <uc1:Footer ID="Footer1" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </ContentTemplate>
        </aspnew:UpdatePanel>
    </div>
    </form>

    <script language="javascript" type="text/javascript">
        document.getElementById("UpdateFollowUpInfo2_cal_followupdate_div").style.zIndex = 100003;
        document.getElementById("UpdateFollowUpInfo2_cal_followupdate_monthYear").style.zIndex = 100004;
    </script>

</body>
</html>

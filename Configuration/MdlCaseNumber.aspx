﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MdlCaseNumber.aspx.cs"
    Inherits="HTP.Configuration.MdlCaseNumber" %>

<%@ Register Src="~/WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="~/WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>MDL Case Numbers</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
        function ShowDeleteMessage() {
            if (confirm("Are you sure you want to delete?"))
                return true;
            return false;
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <aspnew:ScriptManager ID="ScriptManager1" runat="server">
        <Scripts>
            <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="~/Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
        </Scripts>
    </aspnew:ScriptManager>
    <aspnew:UpdatePanel ID="pnl_main" runat="server">
        <ContentTemplate>
            <div>
                <table id="TableMain" cellspacing="0" cellpadding="0" width="780px" align="center"
                    border="0">
                    <tr>
                        <td>
                            <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td background="../Images/separator_repeat.gif" height="9" width="750px">
                        </td>
                    </tr>
                    <tr>
                        <td class="clsLeftPaddingTable">
                            <table style="width: 100%">
                                <tr>
                                    <td align="center" valign="middle">
                                        <asp:Label ID="lblCompany" runat="server" CssClass="clssubhead" Text="MDL Number :"></asp:Label>
                                        &nbsp;
                                        <asp:TextBox ID="txtMDLNumber" runat="server" CssClass="clsInputadministration" Width="200px"
                                            MaxLength="50"></asp:TextBox>&nbsp;
                                        <asp:CheckBox ID="chkIsActive" runat="server" Text="IsActive" CssClass="clsLeftPaddingTable" />&nbsp;
                                        <asp:Button ID="btnAdd" runat="server" CssClass="clsbutton" OnClick="btnAdd_Click"
                                            Text="Add" Width="60px" ValidationGroup="contact" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" valign="middle">
                                        <asp:ValidationSummary ID="ValidationSummary1" CssClass="clssubhead" DisplayMode="List"
                                            runat="server" ValidationGroup="contact" />
                                        <asp:RequiredFieldValidator ID="rfvValue" CssClass="clssubhead" ControlToValidate="txtMDLNumber"
                                            runat="server" Display="None" ErrorMessage="Please enter MDL Number." ValidationGroup="contact"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../Images/separator_repeat.gif" height="9" width="750px">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="819px" height="34px" background="../Images/subhead_bg.gif" class="clssubhead">
                                <tr>
                                    <td align="left" style="width: 100%;" class="clssubhead">
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../Images/separator_repeat.gif" height="9" width="750px">
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="width: 819px">
                            <asp:Label ID="lbl_Message" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                            <br />
                            <asp:GridView ID="gvMDLNumber" runat="server" AutoGenerateColumns="False" CssClass="clsLeftPaddingTable"
                                CellPadding="3" CellSpacing="0" OnRowCommand="gvMDLNumberRowCommand" OnRowDataBound="gvMDLNumberRowDataBound"
                                Width="100%">
                                <Columns>
                                    <asp:TemplateField Visible="false" HeaderText="S#">
                                        <ItemTemplate>
                                            <asp:Label ID="hl_Sno" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SNo") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="" HeaderStyle-Width="40%" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="MDLNumber" runat="server" Text="MDL Number" ToolTip="MDL Number"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="MDL Number" HeaderStyle-Width="30%" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMDLNumber" runat="server" Text='<%# Eval("CaseNumber") %>' ToolTip='<%# Eval("CaseNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="IsActive" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="LblIsActive" runat="server" CssClass="clsLabel" Text='<%# (Convert.ToInt32(Eval("isActive")) == 0) ? "No" : "Yes" %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-Width="20%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lblbtnEdit" runat="server" Text="Edit" CommandArgument='<%# Eval("SNo") %>'
                                                CommandName="editing" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-Width="20%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkbtnDelete" runat="server" Text="Delete" CommandArgument='<%# Eval("SNo") %>'
                                                CommandName="deleting" OnClientClick="return ShowDeleteMessage();" />
                                            <asp:HiddenField ID="HfValueId" runat="server" Value='<%# Eval("Id") %>' />
                                            <asp:HiddenField ID="HfIsactive" runat="server" Value='<%# Eval("isActive") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                        <td align="center">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td background="../Images/separator_repeat.gif" height="9" width="750px">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc1:Footer ID="Footer1" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
        <Triggers>
            <aspnew:AsyncPostBackTrigger ControlID="btnAdd" EventName="Click" />
        </Triggers>
    </aspnew:UpdatePanel>
    </form>
</body>
</html>

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;

namespace HTP.Configuration
{
    public partial class PromotionalPrices : System.Web.UI.Page
    {
        IDataReader dr = null;

        #region Declaration

        clsENationWebComponents ClsDb = new clsENationWebComponents();
        clsViolations ClsViolations = new clsViolations();
        clsFirms clsfirm = new clsFirms();
        clsSession cSession = new clsSession();
        clsLogger clog = new clsLogger();
        DataView dv;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (cSession.GetCookie("sAccessType", this.Request).ToString() != "2")
                {
                    Response.Redirect("../LoginAccesserror.aspx", false);
                }

                if (!IsPostBack)
                {
                    GetCompetitorList();
                }
                btn_Submit.Attributes.Add("onclick", "return ValidateControls();");
                FillGrid();
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        private void GetCompetitorList()
        {
            try
            {
                DataTable dt = clsfirm.GetAllCompetitorFirmName(true);

                if (dt.Rows.Count > 0)
                    ddl_Competitors.DataSource = dt;
                ddl_Competitors.DataBind();
                ddl_Competitors.Items.Insert(0, new ListItem("--- Choose ---", "-1"));

            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

            }
        }

        protected void btn_Submit_Click(object sender, EventArgs e)
        {
            try
            {
                int Empid = 0;
                int ActiveFirm = 1;    //By Default Firms will be considered as active.

                if (cb_Active.Checked == false)
                    ActiveFirm = 0;
                if (cSession.IsValidSession(this.Request, this.Response, this.Session) == false ||
                        (!int.TryParse(cSession.GetCookie("sEmpID", this.Request), out Empid)))
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                string[] key1 = {
                                    "@FirmID", "@SchemeID", "@IsActive", "@Empid", "@MatterMessage",
                                    "@RegularPrice", "@BondPrice"
                                };

      

                object[] value1 = {
                                      ddl_Competitors.SelectedValue,0,ActiveFirm, Empid, txt_MatterMsg.Text, txt_RegularPrice.Text,
                                      txt_BondPrice.Text
                                  };
                

                ClsDb.InsertBySPArr("usp_htp_update_PromotionalPrices", key1, value1);
                FillGrid();
         
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }



        }

        private void FillGrid()
        {
            try
            {


                DataSet tempDs = ClsDb.Get_DS_BySPArr("usp_htp_get_Competitors");
                dv = new DataView(tempDs.Tables[0]);
                gvResult.DataSource = dv;
                gvResult.DataBind();
                if (gvResult.Rows.Count < 1)
                {
                    lbl_message.Text = "No Record Found";
                }
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void lnkbtnInsertScheme_Click(object sender, EventArgs e)
        {
            Divadd.Visible = true;
            btn_Submit.Text = "Add";
            pnlCompetitor.Visible = true;
            pnlShowZipCode.Visible = false;
        }

     


      

        
        public void gvResult_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            try
            {       
                if (e.CommandName == "Delete")
                {
    
                    string[] keys = { "@FirmID" };
                    object[] values = { ddl_Competitors.SelectedValue };
                    ClsDb.ExecuteSP("usp_htp_delete_promotionalscheme", keys, values);
                }


                if (e.CommandName == "FNameClicked")
                {
                    Divadd.Visible = true;
                    pnlShowZipCode.Visible = false;
                    btn_Submit.Text = "Update";
                    GridView gridview = (GridView)sender;
                    //GridView gv = (GridView)sender;
                    //gridview.SelectedIndex = Convert.ToInt32(e.CommandArgument) - 1;
                    //ViewState["UpdateFlag"] = 1;
                    //gv.SelectedIndex = Convert.ToInt32(e.CommandArgument) - 1;
                    ViewState["FirmName"] = ((LinkButton)gridview.SelectedRow.FindControl("lnkbtnFirmName")).Text;
                    
                    //ddl_Competitors.Text = dr["Firm Name"].ToString();
                    //ddl_Competitors.Text = ((HiddenField)gvResult.SelectedRow.FindControl("lnkbtnFirmName")).Text;
                    //ViewState["UpdateFlag"] = 1;
                    ddl_Competitors.SelectedItem.Text = ((LinkButton)gvResult.SelectedRow.FindControl("lnkbtnFirmName")).ToString();
                    //ddl_Competitors.SelectedItem.Text = ((HiddenField)gv.SelectedRow.FindControl("lbl_FirmID")).Value;
                    //ViewState["FirmID"] = ((HiddenField)gv.SelectedRow.FindControl("lbl_FirmID")).Value;
                    //ViewState["Description"] = ((LinkButton)gv.SelectedRow.FindControl("lbtn_Desc")).Text;
                    //ViewState["AssignedUser"] = ((Label)gv.SelectedRow.FindControl("lbl_AssignedUser")).Text;//Fahad 6054 07/25/2009 Add AssignTo field through dropdown selected value
                    //MultiView1.SetActiveView(v_Update);
                }
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message.ToString();
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }







        }

        protected void btn_delete_Click(object sender, EventArgs e)
        {
            try
            {
                string[] keys = { "@FirmID" };
                object[] values = { ddl_Competitors.SelectedValue };
                ClsDb.ExecuteSP("usp_htp_delete_promotionalscheme", keys, values);
                FillGrid();



            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void btn_Clear_Click(object sender, EventArgs e)
        {
            try
            {
                ddl_Competitors.SelectedIndex = 0;
                txt_RegularPrice.Text = "";
                txt_BondPrice.Text = "";
                txt_MatterMsg.Text = "";
                cb_Active.Checked = false;
                lbl_message.Text = "";
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }


        }

        protected void lnkbtnZipCode_Click(object sender, EventArgs e)
        {

            Divadd.Visible = true;
            pnlShowZipCode.Visible = true;
            pnlCompetitor.Visible = false;
        }



   
    }
}
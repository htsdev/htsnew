﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AttorneyPayout.aspx.cs"
    Inherits="HTP.Activities.AttorneyPayoutConfiguration" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="~/WebControls/ActiveMenu.ascx" TagPrefix="uc1" TagName="activemenu" %>
<%@ Register Src="~/WebControls/PagingControl.ascx" TagName="pagingcontrol" TagPrefix="uc3" %>
<%@ Register Src="~/WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Attorney Payout</title>

    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <meta http-equiv="X-UA-Compatible" content="IE=5; IE=8; IE=7.5;" />
    <meta http-equiv="X-UA-Compatible" content="chrome=1">

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />




    <%--<link href="../Styles.css" type="text/css" rel="Stylesheet" />--%>

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
        // Noufil 6869 10/30/2009 Validate Text Box inside grid
        function ValidateInput()
        {        
        
           //Nasir 6869 01/27/2010 Hide label
            var lbl_Message = document.getElementById("lbl_Message");
            lbl_Message.style.display = 'none';
            var grid = document.getElementById("gv_Records");
            var number = "";
            for (var i= 2 ; i <= grid.rows.length ; i++ )
            {
                if (i < 10)
                    number = "0"+i;
                else
                    number = i;                    
                
                var Text = trim(document.getElementById("gv_Records_ctl" + number + "_txt_price").value);
                
                if (Text.length == 0 )
                {  
                    
                    alert("Please enter Amount.");                  
                    return false;
                }
                else if (isNaN(Text))
                {
                    alert("Please enter numeric value in Amount");               
                    return false;
                }
            }
        }
        
        // Noufil 6869 10/30/2009 Validate Firm
        function Validate()
        {
            if (document.getElementById("ddlAttorneys").value == "-1")
            {
                alert("Please select Attorney.");
                return false;
            }
        }
        
         //Nasir 6869 01/27/2010 Hide when drop down change
        function HideGrid()
        {
            var grid = document.getElementById("gv_Records");
            var tr_seperator = document.getElementById("tr_seperator");
            var btnupdate = document.getElementById("btnupdate");
            var lbl_Message = document.getElementById("lbl_Message");
            if(grid!=null)
            {
                grid.style.display = 'none';
            }                        
            tr_seperator.style.display = 'none';                        
            btnupdate.style.display = 'none';
            lbl_Message.style.display = 'none';
        }
        
    </script>

</head>

<body class=" ">

    <form id="form1" runat="server">

        <aspnew:ScriptManager ID="ScriptManager1" runat="server" />

        <!-- START CONTAINER -->
        <div class="page-container row-fluid container-fluid">

            <asp:Panel ID="pnl" runat="server">
                <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
            </asp:Panel>

            <!-- START CONTENT -->
            <section id="main-content" class="attorneyPayoutPage">
                <section class="wrapper main-wrapper row" style=''>
                    <div class="col-md-12">
<div id="LblSucessdiv" visible="false" runat="server" >
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
<asp:Label runat="server" ID="LblSucesstext"></asp:Label>
</div>
</div>
                <div class='col-xs-12'>
                    <div class="page-title">

                        <div class="pull-left">
                            <!-- PAGE HEADING TAG - START -->
                            <h1 class="title">Attorney Payout</h1>
                            <!-- PAGE HEADING TAG - END -->          
                        </div>                                       
                                
                    </div>
                </div>
                <div class="clearfix"></div>

                <!-- MAIN CONTENT AREA STARTS -->
    
                <div class="col-xs-12">
                    <section class="box ">
                        <div class="content-body">

                            <div class="row">
                                <div class="col-md-6 col-sm-7 col-xs-8">

                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Attorneys</label>
                                        <div class="controls">
                                            <asp:DropDownList ID="ddlAttorneys" CssClass="form-control m-bot15" runat="server" DataTextField="FirmName" DataValueField="CoveringFirmID" onchange="HideGrid();">
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-6 col-sm-7 col-xs-8">

                                    <div class="form-group">
                                        <label class="form-label" for="field-1">&nbsp;</label>
                                        <div class="controls">
                                            <asp:Button ID="btnSearch" Text="Search" OnClick="btnSearch_Click" CssClass="btn btn-primary pull-right" runat="server" OnClientClick="return Validate();" />
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12">

                                    <div class="form-group">
                                        <asp:Label ID="lbl_Message" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                                    </div>

                                </div>

                            </div>

                            <aspnew:UpdatePanel ID="updatepnlpaging" runat="server" RenderMode="Inline">
                                <ContentTemplate>
                                    <uc3:pagingcontrol ID="Pagingctrl" runat="server" />
                                </ContentTemplate>
                            </aspnew:UpdatePanel>

                            <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlResult">
                                <ProgressTemplate>
                                    <img src="../Images/plzwait.gif" /><asp:Label ID="lbl1" runat="server" Text="Please Wait ......" CssClass="form-label"></asp:Label>
                                </ProgressTemplate>
                            </aspnew:UpdateProgress>

                        </div>
                    </section>
                </div>

                    <table>
                        <tr id="tr_seperator" runat="server" style="display: none;">
                            <td>

                            </td>
                        </tr>
                    </table>
                <aspnew:UpdatePanel ID="upnlResult" runat="server">
                    <ContentTemplate>

                        <div class="col-lg-12">
                            <section class="box ">
                                <header class="panel_header">
                                    <h2 class="title pull-left">&nbsp;</h2>
                                </header>
                                <div class="content-body">
                                    <div class="row">
                                        <div class="col-xs-12">

                                            <asp:GridView ID="gv_Records" runat="server" AutoGenerateColumns="False" CssClass="table" CellPadding="0" CellSpacing="0" AllowPaging="True" PageSize="20">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="S#">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_sno" runat="server" CssClass="form-label" Text='<%# Eval("sno") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Charge Level" HeaderStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_chargeLevel" runat="server" CssClass="form-label" Text='<%# Eval("LevelCode") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Price" HeaderStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <span class="GridItemStyle pull-left">$</span>
                                                            <asp:TextBox ID="txt_price" runat="server" MaxLength="8" CssClass="form-control pull-left" Text='<%#  Convert.ToDouble( Eval("amount")) %>'></asp:TextBox>
                                                            <asp:HiddenField ID="hf_chargelevelID" runat="server" Value='<%# Eval("ID") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <PagerStyle HorizontalAlign="Center" />
                                                <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                    FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                            </asp:GridView>
                                            <asp:HiddenField ID="hf_NoteIDs" runat="server" />
                                            <asp:HiddenField ID="hf_ticketviolationids" runat="server" />
                                            <asp:Button ID="btnupdate" runat="server" style="display:none" Text="Update" CssClass="btn btn-primary pull-right" OnClick="btnupdate_Click" OnClientClick="return ValidateInput();" />
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>

                    </ContentTemplate>
                </aspnew:UpdatePanel>

                <!-- MAIN CONTENT AREA ENDS -->

                </section>
            </section>
            <!-- END CONTENT -->

        </div>
        <!-- END CONTAINER -->

    </form>

    <!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->


    <!-- CORE JS FRAMEWORK - START --> 
    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script> 
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>  
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>  
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE JS FRAMEWORK - END --> 


    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 

    <script src="../assets/plugins/autosize/autosize.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>
    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


    <!-- CORE TEMPLATE JS - START --> 
    <script src="../assets/js/scripts.js" type="text/javascript"></script> 
    <!-- END CORE TEMPLATE JS - END --> 

</body>



































<%--<body>
    <form id="form1" runat="server">
    <div>
        <aspnew:ScriptManager ID="ScriptManager1" runat="server" />
        <table cellpadding="0" cellspacing="0" width="900px" align="center" border="0">
            <tr>
                <td>
                    <uc1:activemenu ID="ActiveMenu1" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="center" style="width: 100%">
                    <asp:Label ID="lblMessage" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 100%" background="../Images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table cellpadding="0" cellpadding="0" width="55%">
                        <tr style="width: 100%">
                            <td style="width: 8%">
                                <span class="clssubhead">Attorneys :</span>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlAttorneys" CssClass="clsInputCombo" runat="server" DataTextField="FirmName"
                                    DataValueField="CoveringFirmID" onchange="HideGrid();">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:Button ID="btnSearch" Text="Search" OnClick="btnSearch_Click" CssClass="clsbutton"
                                    runat="server" OnClientClick="return Validate();" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td background="../Images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td background="../Images/subhead_bg.gif" height="34" class="clssubhead" align="right">
                    <aspnew:UpdatePanel ID="updatepnlpaging" runat="server" RenderMode="Inline">
                        <ContentTemplate>
                            <uc3:pagingcontrol ID="Pagingctrl" runat="server" />
                        </ContentTemplate>
                    </aspnew:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td>
                    <table id="TableGrid" bgcolor="white" border="0" cellpadding="0" cellspacing="0"
                        width="100%">
                        <tr>
                            <td background="../../images/separator_repeat.gif" colspan="5" height="11" style="height: 11px"
                                width="780">
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Label ID="lbl_Message" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2" valign="top">
                                <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlResult">
                                    <ProgressTemplate>
                                        <img src="../Images/plzwait.gif" /><asp:Label ID="lbl1" runat="server" Text="Please Wait ......"
                                            CssClass="clsLabel"></asp:Label>
                                    </ProgressTemplate>
                                </aspnew:UpdateProgress>
                                <aspnew:UpdatePanel ID="upnlResult" runat="server">
                                    <ContentTemplate>
                                        <asp:GridView ID="gv_Records" runat="server" AutoGenerateColumns="False" Width="30%"
                                            CssClass="clsLeftPaddingTable" CellPadding="0" CellSpacing="0" AllowPaging="True"
                                            PageSize="20">
                                            <Columns>
                                                <asp:TemplateField HeaderText="S#">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_sno" runat="server" CssClass="GridItemStyle" Text='<%# Eval("sno") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Charge Level" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_chargeLevel" runat="server" CssClass="GridItemStyle" Text='<%# Eval("LevelCode") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Price" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <span class="GridItemStyle">$</span>
                                                        <asp:TextBox ID="txt_price" runat="server" Width="50px" MaxLength="8" CssClass="clsInputadministration"
                                                            Text='<%#  Convert.ToDouble( Eval("amount")) %>'></asp:TextBox>
                                                        <asp:HiddenField ID="hf_chargelevelID" runat="server" Value='<%# Eval("ID") %>' />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerStyle HorizontalAlign="Center" />
                                            <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                        </asp:GridView>
                                    </ContentTemplate>
                                </aspnew:UpdatePanel>
                                <asp:HiddenField ID="hf_NoteIDs" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td background="../../images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="30%">
                        <tr>
                            <td align="right">
                                <asp:Button ID="btnupdate" runat="server" style="display:none" Text="Update" CssClass="clsbutton"
                                    OnClick="btnupdate_Click" OnClientClick="return ValidateInput();" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="tr_seperator" runat="server" style="display: none;">
                <td background="../../images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td>
                    <uc1:Footer ID="Footer1" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:HiddenField ID="hf_ticketviolationids" runat="server" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>--%>
</html>

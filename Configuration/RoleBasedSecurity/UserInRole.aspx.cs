﻿using System;
using System.Web.UI.WebControls;
using HTP.ClientController;
using lntechNew.Components.ClientInfo;

using RoleBasedSecurity.DataTransferObjects;

namespace HTP.Configuration.RoleBasedSecurity
{
    /// <summary>
    /// This class represents all information about the UserInRole.
    /// </summary>
    public partial class UserInRole : WebComponents.BasePage
    {

        #region Variables

        //Variable Declared for role based security controller class
        readonly RoleBasedSecurityController _roleBasedSecurityController = new RoleBasedSecurityController();

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lbl_Message.Text = string.Empty;
                //Checking Access Type if Not Primary then redirecting to Login Error Page
                if (AccessType != 2)
                {
                    Response.Redirect("~/LoginAccesserror.aspx", false);
                }
                else if (!IsPostBack)
                {
                    //Call Role Based Security Controller method to get User by Traffic Program ID and assigned to the List.
                    var tpuserList = _roleBasedSecurityController.GetUserByTpId(new UserDto { TpUserId = EmpId });

                    //Setting Userid in View State for further Use.
                    ViewState["LoginEmployeeId"] = tpuserList[0].UserId;
                    //Populating the drop down list having active values
                    Populateddl(true);
                    FillGrid();

                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {

                lbl_Message.Text = string.Empty;
                switch (btnAdd.Text)
                {
                    case "Add":
                        if (Add())
                        {
                            SetEmptyControl();
                            FillGrid();
                            lbl_Message.Text = "Record added successfully.";
                        }
                        else
                            lbl_Message.Text = "Record not added successfully.";
                        break;
                    case "Update":
                        if (Update())
                        {
                            SetEmptyControl();
                            FillGrid();
                            lbl_Message.Text = "Record updated successfully.";
                        }
                        else
                            lbl_Message.Text = "Record not updated successfully.";
                        break;
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        protected void lnk_AddNewRecord_Click(object sender, EventArgs e)
        {
            try
            {   
                SetEmptyControl();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void gvUserInRole_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                lbl_Message.Text = string.Empty;

                switch (e.CommandName)
                {
                    case "image":
                        {
                            var id = Convert.ToInt32((((HiddenField)gvUserInRole.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("HfValueId")).Value));

                            //Call the Role Based Security Controller method to Delete User in Role.
                            if (_roleBasedSecurityController.DeleteUserInRole(new UserInRolesDto { UserRoleId = id }))
                            {
                                SetEmptyControl();
                                lbl_Message.Text = "Record deleted successfully";
                                FillGrid();
                            }
                            else
                                lbl_Message.Text = "Record not deleted successfully";
                        }
                        break;
                    case "lnkbutton":
                        //Re-Populating the drop down list having all active and inactive values
                        Populateddl(null);
                        //Assigning Directly Drop Dwons Selected Value through Label control.
                        ddlCompany.SelectedValue =
                            ddlCompany.Items.FindByText(
                                ((Label)gvUserInRole.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("LblCompany"))
                                    .Text).Value;

                        ddlRole.SelectedValue =
                            ddlRole.Items.FindByText(
                                ((Label)gvUserInRole.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("LblRole"))
                                    .Text).Value;

                        ddlUser.SelectedValue =
                            ddlUser.Items.FindByText(
                                ((LinkButton)gvUserInRole.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("LnkbtnUser"))
                                    .Text).Value;

                        ViewState["ValueID"] = ((HiddenField)gvUserInRole.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("HfValueId")).Value;
                        chkIsActive.Checked = Convert.ToBoolean(((HiddenField)gvUserInRole.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("HfIsactive")).Value);
                        btnAdd.Text = "Update";
                        break;
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        protected void gvUserInRole_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ((LinkButton)e.Row.FindControl("LnkbtnUser")).CommandArgument = Convert.ToString(e.Row.RowIndex);
                    ((ImageButton)e.Row.FindControl("ImgDelete")).CommandArgument = Convert.ToString(e.Row.RowIndex);
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        #endregion

        #region Method

        /// <summary>
        /// This method used to set the default view of all the controls on the page.
        /// </summary>
        private void SetEmptyControl()
        {
            //Re-Populating the drop down list having active values
            Populateddl(true);
            chkIsActive.Checked = false;
            ddlCompany.SelectedValue = "0";
            ddlRole.SelectedValue = "0";
            ddlUser.SelectedValue = "0";
            lbl_Message.Text = string.Empty;
            if (ViewState != null) ViewState["ValueID"] = String.Empty;
            btnAdd.Text = "Add";
        }

        /// <summary>
        /// This method is used to Fill Data into the Grid.
        /// </summary>
        private void FillGrid()
        {
            //Call the Role Based Security Controller method to get all User in Roles and assigned to the List.
            var roleist = _roleBasedSecurityController.GetAllUserInRole(new UserInRolesDto { IsActive = null });

            //Checking if list is not empty then bind with drop down list.
            if (roleist != null)
            {
                gvUserInRole.DataSource = roleist;
                gvUserInRole.DataBind();
            }
            else
            {
                gvUserInRole.DataSource = null;
                gvUserInRole.DataBind();
            }
        }

        /// <summary>
        /// This method is used to Add/Insert New UserInRole.
        /// </summary>
        /// <returns>True/False</returns>
        private bool Add()
        {
            //Call the Role Based Security Controller method to Add User in Role.
            return _roleBasedSecurityController.AddUserInRole(new UserInRolesDto
            {
                UserId = Convert.ToInt32(ddlUser.SelectedValue),
                CompanyId = Convert.ToInt32(ddlCompany.SelectedValue),
                RoleId = Convert.ToInt32(ddlRole.SelectedValue),
                IsActive = chkIsActive.Checked,
                InsertedBy = Convert.ToInt32(ViewState["LoginEmployeeId"])
            });
        }

        /// <summary>
        /// This Method used to Update the existing UserInRole.
        /// </summary>
        /// <returns>True/False</returns>
        private bool Update()
        {
            //Call the Role Based Security Controller method to Update User in Role.
            return _roleBasedSecurityController.UpdateUserInRole(new UserInRolesDto
            {
                UserRoleId = Convert.ToInt32(ViewState["ValueID"]),
                UserId = Convert.ToInt32(ddlUser.SelectedValue),
                CompanyId = Convert.ToInt32(ddlCompany.SelectedValue),
                RoleId = Convert.ToInt32(ddlRole.SelectedValue),
                IsActive = chkIsActive.Checked,
                LastUpdatedBy = Convert.ToInt32(ViewState["LoginEmployeeId"])

            });


        }

        /// <summary>
        /// This method is using for populating the drop down lists of Role, Company and User.
        /// </summary>
        private void Populateddl(bool? isActive)
        {
            //Call the Role Based Security Controller method to get all Roles and assigned to the List.
            var rolelist = _roleBasedSecurityController.GetAllRole(new RoleDto { IsActive = isActive });

            //Checking if list is not empty then bind with drop down list.
            if (rolelist.Count > 0)
            {
                ddlRole.DataSource = rolelist;
                ddlRole.DataTextField = "RoleName";
                ddlRole.DataValueField = "RoleId";
                ddlRole.DataBind();
                ddlRole.Items.Insert(0, new ListItem("--Select--", "0"));
            }

            //Call the Role Based Security Controller method to get all Companies and assigned to the List.
            var compnaylist = _roleBasedSecurityController.GetAllCompany(new CompanyDto { IsActive = isActive });

            //Checking if list is not empty then bind with drop down list.
            if (compnaylist.Count > 0)
            {
                ddlCompany.DataSource = compnaylist;
                ddlCompany.DataTextField = "CompanyName";
                ddlCompany.DataValueField = "CompanyId";
                ddlCompany.DataBind();
                ddlCompany.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            //Call the Role Based Security Controller method to get all Companies and assigned to the List.
            var userlist = _roleBasedSecurityController.GetAllUser(new UserDto { IsActive = isActive });

            //Checking if list is not empty then bind with drop down list.
            if (userlist.Count > 0)
            {
                ddlUser.DataSource = userlist;
                ddlUser.DataTextField = "UserName";
                ddlUser.DataValueField = "UserId";
                ddlUser.DataBind();
                ddlUser.Items.Insert(0, new ListItem("--Select--", "0"));
            }

        }

        #endregion


    }
}


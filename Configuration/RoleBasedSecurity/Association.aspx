﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Association.aspx.cs" Inherits="HTP.Configuration.RoleBasedSecurity.Association" %>

<%@ Register Src="~/WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="~/WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Association</title>
    <link href="../../Styles.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
     function ShowHide()
     {
        if ( document.getElementById('rbProcess').checked == true )					 
            {
                document.getElementById('ddlMenu').disabled=true;
                document.getElementById('ddlProcess').disabled=false;
            }
         else if( document.getElementById('rbMenu').checked == true)	
			{
			 document.getElementById('ddlProcess').disabled=true;
			 document.getElementById('ddlMenu').disabled=false;
			}		
     }
    </script>

    <style type="text/css">
        .style1
        {
            width: 219px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="TableMain" cellspacing="0" cellpadding="0" width="780px" align="center"
            border="0">
            <tr>
                <td>
                    <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td background="../../Images/separator_repeat.gif" height="9" width="750px">
                </td>
            </tr>
            <tr>
                <td class="clsLeftPaddingTable">
                    <table style="width: 100%">
                        <tr>
                            <td id="tdProcess" valign="middle" class="style1">
                                <asp:RadioButton ID="rbProcess" CssClass="clssubhead" GroupName="Type" onclick="ShowHide();"
                                    Text="Process Name :" runat="server" />
                                &nbsp;
                                <asp:DropDownList ID="ddlProcess" runat="server" CssClass="clsInputadministration"
                                    Width="100px">
                                </asp:DropDownList>
                            </td>
                            <td valign="middle">
                                <asp:RadioButton ID="RadioButton1" GroupName="Type" CssClass="clssubhead" onclick="ShowHide();"
                                    Text="Menu Name :" runat="server" />
                                &nbsp;
                                <asp:DropDownList ID="ddlMenu" runat="server" CssClass="clsInputadministration" Width="100px">
                                </asp:DropDownList>
                                &nbsp;
                                <asp:CheckBox ID="chkIsActive" runat="server" Text="IsActive" CssClass="clsLeftPaddingTable" />&nbsp;
                                <asp:Button ID="btnAdd" runat="server" CssClass="clsbutton" OnClick="btnAdd_Click"
                                    Text="Add" Width="60px" ValidationGroup="contact" />
                            </td>
                        </tr>                      
                    </table>
                </td>
            </tr>
            <tr>
                <td background="../../Images/separator_repeat.gif" height="9" width="750px">
                </td>
            </tr>
            <tr>
                <td>
                    <table width="819px" height="34px" background="../../Images/subhead_bg.gif" class="clssubhead">
                        <tr>
                            <td align="left" style="width: 410px" class="clssubhead">
                                &nbsp;Association &nbsp;&nbsp;
                            </td>
                            <td align="right" style="width: 409px" class="clssubhead">
                                <asp:LinkButton ID="lnk_AddNewRecord" runat="server" OnClick="lnk_AddNewRecord_Click">Add New Record</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td background="../../Images/separator_repeat.gif" height="9" width="750px">
                </td>
            </tr>
            <tr>
                <td align="center" style="width: 819px">
                    <asp:Label ID="lbl_Message" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                    <br />
                    <asp:GridView ID="gvAssociation" runat="server" AutoGenerateColumns="False" CssClass="clsLeftPaddingTable"
                        CellPadding="3" CellSpacing="0" OnRowCommand="gvAssociation_RowCommand" OnRowDataBound="gvAssociation_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="S#">
                                <ItemTemplate>
                                    <asp:Label ID="lblSno" runat="server" CssClass="GridItemStyle" Text='<%# Eval("SerialNumber") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Process Name">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkbtnProcess" runat="server" Text='<%# Eval("ProcessName") %>'
                                        ToolTip='<%# Eval("ProcessName") %>' CommandName="lnkbutton"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Menu Name">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkbtnMenu" runat="server" Text='<%# Eval("MenuId") %>' ToolTip='<%# Eval("MenuId") %>'
                                        CommandName="lnkbutton"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Active">
                                <ItemTemplate>
                                    <asp:Label ID="lblIsActive" runat="server" CssClass="clsLabel" Text='<%# (Convert.ToInt32(Eval("IsActive")) == 0) ? "No" : "Yes" %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Inserted By">
                                <ItemTemplate>
                                    <asp:Label ID="lblInserted" runat="server" CssClass="clsLabel" Text='<%# Eval("InsertedUserName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Updated By">
                                <ItemTemplate>
                                    <asp:Label ID="lblupdated" runat="server" CssClass="clsLabel" Text='<%# Eval("LastUpDateUserName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImgDelete" runat="server" ImageUrl="~/Images/remove2.gif" CommandArgument='<%# Eval("AssociationId") %>'
                                        CommandName="image" />
                                    <asp:HiddenField ID="hfValueId" runat="server" Value='<%# Eval("AssociationId") %>' />
                                    <asp:HiddenField ID="hfcases" runat="server" Value='<%# Eval("asscociatedprocess") %>' />
                                    <%-- <asp:HiddenField ID="hfmenu" runat="server" Value='<%# eval("asscociatedmenu") %>' />--%>
                                    <asp:HiddenField ID="hfIsactive" runat="server" Value='<%# Eval("IsActive") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
                <td align="center">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td background="../../Images/separator_repeat.gif" height="9" width="750px">
                </td>
            </tr>
            <tr>
                <td>
                    <uc1:Footer ID="Footer1" runat="server" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

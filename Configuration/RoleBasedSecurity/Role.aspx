﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Role.aspx.cs" Inherits="HTP.Configuration.RoleBasedSecurity.Role" %>

<%@ Register Src="~/WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="~/WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Role</title>
    <link href="../../Styles.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
        function ShowDeleteMessage()
        {
            if (confirm("Are you sure you want to delete?"))
                return true;
            return false;
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <aspnew:ScriptManager ID="ScriptManager1" runat="server">
        <Scripts>
            <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="~/Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
        </Scripts>
    </aspnew:ScriptManager>
    <aspnew:UpdatePanel ID="pnl_main" runat="server">
        <ContentTemplate>
            <div>
                <table id="TableMain" cellspacing="0" cellpadding="0" width="780px" align="center"
                    border="0">
                    <tr>
                        <td>
                            <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td background="../../Images/separator_repeat.gif" height="9" width="750px">
                        </td>
                    </tr>
                    <tr>
                        <td class="clsLeftPaddingTable">
                            <table style="width: 100%">
                                <tr>
                                    <td align="center" valign="middle">
                                        <asp:Label ID="lblRole" runat="server" CssClass="clssubhead" Text="Role :"></asp:Label>
                                        &nbsp;
                                        <%-- Fahad 7783 05/10/2010 Company Name Length increases 60 to 100--%>
                                        <asp:TextBox ID="txtRole" runat="server" CssClass="clsInputadministration" Width="200px"
                                            MaxLength="100"></asp:TextBox>
                                        &nbsp;
                                        <asp:CheckBox ID="chkAllowAdmin" runat="server" Text="Allow Admin Rights" CssClass="clsLeftPaddingTable" />&nbsp;
                                        <asp:CheckBox ID="chkIsActive" runat="server" Text="IsActive" CssClass="clsLeftPaddingTable" />&nbsp;
                                        <asp:Button ID="btnAdd" runat="server" CssClass="clsbutton" OnClick="btnAdd_Click"
                                            Text="Add" Width="60px" ValidationGroup="contact" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" valign="middle">
                                        <asp:ValidationSummary ID="ValidationSummary1" CssClass="clssubhead" DisplayMode="List"
                                            runat="server" ValidationGroup="contact" />
                                        <asp:RequiredFieldValidator ID="rfvValue" CssClass="clssubhead" ControlToValidate="txtRole"
                                            runat="server" Display="None" ErrorMessage="Please enter role" ValidationGroup="contact"></asp:RequiredFieldValidator>
                                        <%-- Fahad 7783 05/10/2010 Corrected the spelling of Alphabets in Error message and allow @,',? character in Name--%>
                                        <asp:RegularExpressionValidator ID="regexpName" CssClass="clssubhead" runat="server"
                                            ValidationGroup="contact" ErrorMessage="Please enter valid role." Display="None"
                                            ControlToValidate="txtRole" ValidationExpression="^[ a-zA-Z!”$%&’()@'?*\+,+-\/;\[\\\]\^_`{|}~]+$" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../../Images/separator_repeat.gif" height="9" width="750px">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="819px" height="34px" background="../../Images/subhead_bg.gif" class="clssubhead">
                                <tr>
                                    <td align="left" style="width: 410px" class="clssubhead">
                                        &nbsp;Role &nbsp;&nbsp;
                                    </td>
                                    <td align="right" style="width: 409px" class="clssubhead">
                                        <asp:LinkButton ID="lnk_AddNewRecord" runat="server" OnClick="lnk_AddNewRecord_Click">Add New Record</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../../Images/separator_repeat.gif" height="9" width="750px">
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="width: 819px">
                            <asp:Label ID="lbl_Message" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                            <br />
                            <asp:GridView ID="gvRole" runat="server" AutoGenerateColumns="False" CssClass="clsLeftPaddingTable"
                                CellPadding="3" CellSpacing="0" OnRowCommand="gvRole_RowCommand" OnRowDataBound="gvRole_RowDataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="S#" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="LblSno" runat="server" CssClass="GridItemStyle" Text='<%# Eval("SerialNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Role Name" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LnkbtnValue" runat="server" Text='<%# Eval("RoleName") %>' ToolTip='<%# Eval("RoleName") %>'
                                                CommandName="lnkbutton"></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Allow Admin Rights" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="LblAllowRightsAdmin" runat="server" CssClass="clsLabel" Text='<%# (Convert.ToInt32(Eval("AllowRightsAdmin")) == 0) ? "No" : "Yes" %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Active" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="LblIsActive" runat="server" CssClass="clsLabel" Text='<%# (Convert.ToInt32(Eval("IsActive")) == 0) ? "No" : "Yes" %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Inserted By" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="LblInserted" runat="server" CssClass="clsLabel" Text='<%# Eval("InsertedUserName") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Updated By" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="Lblupdated" runat="server" CssClass="clsLabel" Text='<%# Eval("LastUpDateUserName") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImgDelete" runat="server" ImageUrl="~/Images/remove2.gif" CommandArgument='<%# Eval("RoleId") %>'
                                                CommandName="image" OnClientClick="return ShowDeleteMessage();" />
                                            <asp:HiddenField ID="HfValueId" runat="server" Value='<%# Eval("RoleId") %>' />
                                            <asp:HiddenField ID="HfIsActive" runat="server" Value='<%# Eval("IsActive") %>' />
                                            <asp:HiddenField ID="HfAllowAdmin" runat="server" Value='<%# Eval("AllowRightsAdmin") %>' />
                                            <asp:HiddenField ID="HfIsAssociated" runat="server" Value='<%# Eval("IsAssociated") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                        <td align="center">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td background="../../Images/separator_repeat.gif" height="9" width="750px">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc1:Footer ID="Footer1" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
        <Triggers>
            <aspnew:AsyncPostBackTrigger ControlID="btnAdd" EventName="Click" />
        </Triggers>
    </aspnew:UpdatePanel>
    </form>
</body>
</html>

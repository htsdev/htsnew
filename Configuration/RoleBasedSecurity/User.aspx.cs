using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using lntechNew.Components.ClientInfo;
//Waqas 5057 03/19/2009 Changed to HTP

namespace lntechNew.Configuration.RoleBasedSecurity
{
    /// <summary>
    /// Summary description for Admin.
    /// </summary>
    public partial class User : System.Web.UI.Page
    {

        clsSession ClsSession = new clsSession();
        protected CheckBox chkCanView;

        clsLogger clog = new clsLogger();
        clsUser cUser = new clsUser();
        clsFirms ClsFirms = new clsFirms();


        //private void Page_Load(object sender, System.EventArgs e)
        //{
        //    TD1.Style["visibility"] = "hidden";
        //    TD2.Style["visibility"] = "hidden";
        //    //Checking employee info in session
        //    if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false)
        //    {
        //        Response.Redirect("../frmlogin.aspx", false);
        //    }
        //    else //To stop page further execution
        //    {
        //        if (ClsSession.GetCookie("sAccessType", this.Request).ToString() != "2")
        //        {
        //            Response.Redirect("../LoginAccesserror.aspx", false);
        //            Response.End();
        //        }
        //        else //To stop page further execution
        //        {

        //            if (!IsPostBack)
        //            {
        //                FillddlFirms();
        //                ddl_spnsearch.Attributes.Add("onchange", "return TD1TD2();");
        //                try
        //                {
        //                    btn_update.Attributes.Add("onclick", "return formSubmit();");
        //                    //Sabir Khan 5193 11/21/2008 viewstate has been set empty for new user...
        //                    ViewState["EmployeeID"] = "";
        //                    // Display User Infomation In The Grid
        //                    bindGrid();
        //                }
        //                catch (Exception ex)
        //                {
        //                    lblMessage.Text = "Cannot Find Record";
        //                    clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
        //                }
        //            }
        //        }
        //    }
        //}




        
        ///Fahad 5196 26/11/2008
        /// <summary>
        /// This method is popualting the ddl by firm abbreviation and firm id.
        /// use to associate the firm with user.
        /// </summary>
        private void FillddlFirms()
        {
            try
            {
                //ozair 5306 12/11/2008 binding full name in firm ddl
                DataSet ds_Firms = ClsFirms.GetAllFullFirmName();

                ddl_Firm.DataSource = ds_Firms.Tables[0];
                //ozair 5306 12/05/2008 binding full name in firm ddl
                ddl_Firm.DataTextField = "FirmFullName";
                ddl_Firm.DataValueField = "FirmID";
                ddl_Firm.DataBind();
                ddl_Firm.SelectedValue = "3000";
            }
            catch (Exception ex)
            {
                lblMessage.Visible = true;
                lblMessage.Text = ex.Message;


            }
        }
        private void btn_clear_Click(object sender, System.EventArgs e)
        {
            //ClearValues();

        }

        protected void btn_update_Click1(object sender, EventArgs e)
        {
            //try
            //{


            //    // Update Value of User
            //    //Sabir Khan 5193 11/21/2008 set hidden field value to empployee id variable ...
            //    // string empid = employeeid;

            //    string empid = Convert.ToString(ViewState["EmployeeID"]);
            //    // Check If No User Is Selected Than Set Employee ID to 0

            //    if (empid == "")
            //        empid = "0";

            //    //check for user existance.
            //    int exist = 0;
            //    DataSet ds_check = cUser.CheckUser(tb_uname.Text.Trim(), empid);
            //    if (ds_check.Tables[0].Rows.Count > 0)
            //    {
            //        exist = Convert.ToInt32(ds_check.Tables[0].Rows[0]["exist"].ToString());
            //    }
            //    if (exist == 0)
            //    {
            //        //ozair 4759 09/13/2008 implemented bool variable for canspnsearh
            //        bool canSPNSearch;
            //        string SPNUserName;
            //        string SPNPassword;
            //        if (ddl_spnsearch.SelectedValue == "1")
            //        {
            //            //ozair 4759 09/13/2008 implemented bool variable for canspnsearh
            //            canSPNSearch = true;
            //            SPNUserName = txt_SPNUserName.Text;
            //            SPNPassword = txt_SPNPassword.Text;
            //        }
            //        else
            //        {
            //            //ozair 4759 09/13/2008 implemented bool variable for canspnsearh
            //            canSPNSearch = false;
            //            SPNUserName = "";
            //            SPNPassword = "";
            //        }

            //        //Nasir 6968 12/04/2009 concadinate contact number
            //        string ContactNumber = txt_CC11.Text + txt_CC12.Text + txt_CC13.Text + txt_CC14.Text;


            //        // Noufil 4232 07/09/2008 add user function switch to clsuser class
            //        //ozair 4759 09/13/2008 implemented bool variable for canspnsearh
            //        //Fahad 5196 11/27/2008 add ddl value to add firm

            //        //Nasir 6968 12/04/2009 add contact number
            //        cUser.Adduser(tb_fname.Text, tb_lname.Text, tb_abbrev.Text, tb_uname.Text.Trim(), tb_password.Text, ddl_accesstype.SelectedValue, ddl_closeout.SelectedValue, empid, txt_email.Text.Trim(), canSPNSearch, SPNUserName, SPNPassword, ddl_status.SelectedValue, txt_NTUserID.Text.Trim(), this.chkAttorney.Checked, this.chk_StAdmin.Checked, tb_NoofDays.Text, ddl_Firm.SelectedValue.ToString(), ContactNumber);

            //        //Zeeshan Ahmed 4703 08/28/2008 If Login User & Update User Are Same Update The Password In The Cookie
            //        if (Convert.ToString(ClsSession.GetCookie("sUserID", this.Request)) == tb_uname.Text.Trim())
            //        {
            //            ClsSession.CreateCookie("Password", tb_password.Text.Trim(), this.Request, this.Response);
            //            //Redirect To Update Ticket Desk Password In the URL
            //            Response.Redirect("Admin.aspx", false);
            //        }
            //        else
            //        {
            //            bindGrid();
            //        }

            //        //Sabir Khan 5193 11/21/2008
            //        ClearValues();

            //    }
            //    else
            //    {
            //        //Sabir Khan 5193 11/21/2008 when user name already exist...

            //        HttpContext.Current.Response.Write("<script language='javascript'> alert('User name already exist! Please specify another user name.'); </script>");
            //    }
            //}
            //catch (Exception ex)
            //{
            //    clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            //}
        }

        protected void rbl_status_SelectedIndexChanged(object sender, EventArgs e)
        {
           // bindGrid();
        }

        protected void ddl_sort_SelectedIndexChanged(object sender, EventArgs e)
        {
            //bindGrid();
        }

       

        protected void Page_Load(object sender, EventArgs e)
        {
            //bindGrid();
        }
    }
}
﻿using System;
using System.Web.UI.WebControls;
using HTP.ClientController;
using lntechNew.Components.ClientInfo;
using RoleBasedSecurity.DataTransferObjects;

namespace HTP.Configuration.RoleBasedSecurity
{
    public partial class Association : System.Web.UI.Page
    {

        #region Variables

        readonly RoleBasedSecurityController _roleBasedSecurityController = new RoleBasedSecurityController();

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lbl_Message.Text = string.Empty;
                if (!IsPostBack)
                {
                    FillGrid();
                    Populateddl();

                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void lnk_AddNewRecord_Click(object sender, EventArgs e)
        {

            try
            {
                SetEmptyControl();
                btnAdd.Text = "Add";
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                //if (Page.IsValid)
                //{
                lbl_Message.Text = string.Empty;
                switch (btnAdd.Text)
                {
                    case "Add":
                        if (Add())
                        {
                            SetEmptyControl();
                            FillGrid();
                            lbl_Message.Text = "Record added successfully.";
                        }
                        else
                            lbl_Message.Text = "Record not added successfully.";
                        break;
                    case "Update":
                        if (Update())
                        {
                            SetEmptyControl();
                            FillGrid();
                            lbl_Message.Text = "Record updated successfully.";
                        }
                        else
                            lbl_Message.Text = "Record not updated successfully.";
                        break;
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        protected void gvAssociation_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ((ImageButton)e.Row.FindControl("ImgDelete")).CommandArgument = Convert.ToString(e.Row.RowIndex);
                    ((LinkButton)e.Row.FindControl("lnkbtnProcess")).CommandArgument = Convert.ToString(e.Row.RowIndex);
                    ((LinkButton)e.Row.FindControl("lnkbtnMenu")).CommandArgument = Convert.ToString(e.Row.RowIndex);

                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        protected void gvAssociation_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                lbl_Message.Text = string.Empty;

                switch (e.CommandName)
                {
                    case "image":
                        {
                            var associatedCases = Convert.ToInt32((((HiddenField)gvAssociation.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("hfcases")).Value));
                            if (associatedCases == 0)
                            {
                                var id = Convert.ToInt32((((HiddenField)gvAssociation.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("hfValueId")).Value));
                                if (_roleBasedSecurityController.DeleteAssociation(new AssociationDto { AssociationId = id }))
                                {
                                    lbl_Message.Text = "Record deleted successfully";
                                    FillGrid();
                                }
                                else
                                {
                                    lbl_Message.Text = "Record not deleted successfully";
                                }
                            }
                            else
                            {
                                lbl_Message.Text = "This Association cannot be deleted as it is associated with case. Please update case information first then try again.";
                            }

                        }
                        break;
                    case "lnkbutton":
                        ddlProcess.SelectedItem.Text = ((LinkButton)gvAssociation.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("lnkbtnProcess")).Text;
                        ddlMenu.SelectedValue = ((LinkButton)gvAssociation.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("lnkbtnMenu")).Text;
                        ViewState["ValueID"] = ((HiddenField)gvAssociation.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("hfValueId")).Value;
                        chkIsActive.Checked = Convert.ToBoolean(((HiddenField)gvAssociation.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("hfIsactive")).Value);
                        btnAdd.Text = "Update";
                        break;
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }
        #endregion

        #region Method

        /// <summary>
        /// This method used to set the default view of all the controls on the page.
        /// </summary>
        private void SetEmptyControl()
        {
            //t.Text = string.Empty;
            //chkIsActive.Checked = false;
            //chkCanExecute.Checked = false;
            //lbl_Message.Text = string.Empty;
            //if (ViewState != null) ViewState["ValueID"] = String.Empty;
            //btnAdd.Text = "Add";
        }

        /// <summary>
        /// This method is used to Fill Data into the Grid.
        /// </summary>
        private void FillGrid()
        {
            var associationlist = _roleBasedSecurityController.GetAllAssociation(new AssociationDto { IsActive = null });
            if (associationlist != null)
            {
                gvAssociation.DataSource = associationlist;
                gvAssociation.DataBind();
            }
            else
            {
                gvAssociation.DataSource = null;
                gvAssociation.DataBind();
            }
        }

        /// <summary>
        /// This method is used to Add/Insert New Case Status.
        /// </summary>
        /// <returns>The Object of Type Polm Controller.</returns>
        private bool Add()
        {
            return _roleBasedSecurityController.AddAssociation(new AssociationDto
            {
                ProcessId = Convert.ToInt32(ddlProcess.SelectedValue),
                MenuId = Convert.ToInt32(ddlMenu.SelectedValue),
                IsActive = chkIsActive.Checked,
                InsertedBy = 1
            });
        }

        /// <summary>
        /// This Method used to Update the existing Case Status.
        /// </summary>
        /// <returns>The Object of Type Polm Controller.</returns>
        private bool Update()
        {
            return _roleBasedSecurityController.UpdateAssociation(new AssociationDto
            {
                AssociationId = Convert.ToInt32(ViewState["ValueID"]),
                ProcessId = Convert.ToInt32(ddlProcess.SelectedValue),
                MenuId = Convert.ToInt32(ddlMenu.SelectedValue),
                IsActive = chkIsActive.Checked,
                LastUpdatedBy = 1

            });
        }

        /// <summary>
        /// 
        /// </summary>
        private void Populateddl()
        {
            var processlist = _roleBasedSecurityController.GetAllProcess(new ProcessDto { IsActive = null });
            if (processlist != null)
            {
                ddlProcess.DataSource = processlist;
                ddlProcess.DataTextField = "ProcessName";
                ddlProcess.DataValueField = "ProcessId";
                ddlProcess.DataBind();
                ddlProcess.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            ddlMenu.Items.Insert(0, new ListItem("--Select--", "0"));
            ddlMenu.DataBind();
        }

        #endregion


    }
}


﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using lntechNew.Components.ClientInfo;
using ListItem = System.Web.UI.WebControls.ListItem;

namespace lntechNew.Configuration
{

    public partial class CourtAssociation : System.Web.UI.Page
    {
        clsCourts clscourt = new clsCourts();
        public static Dictionary<int, string> Hstatus = new Dictionary<int, string>();
        public static Dictionary<int, string>.KeyCollection Keys;
        public static DataTable DtCourtAssociat;
        public static DataTable DtStatusRoom;

        protected void Page_Load(object sender, EventArgs e)
        {


            if (!IsPostBack)
            {
                FillCourt();


            }

        }

        private void FillCourt()
        {
            ddlCourt.DataTextField = "shortname";
            ddlCourt.DataValueField = "courtid";
            ddlCourt.DataSource = clscourt.GetAllActiveCourtName();
            ddlCourt.DataBind();
        }

        protected void wizCourtAssociation_NextButtonClick(object sender, WizardNavigationEventArgs e)
        {


            if (wizCourtAssociation.ActiveStep.ID == "Step1")
            {

                lblDivision.Text = ddlDivision.SelectedItem.Text;
                lblCourt.Text = ddlCourt.SelectedItem.Text;




                clsCaseStatus caseStatus = new clsCaseStatus();
                ChkStatus.DataTextField = "description";
                ChkStatus.DataValueField = "id";
                ChkStatus.DataSource = caseStatus.GetAllCourtCaseStatus();
                ChkStatus.DataBind();

                DtCourtAssociat = clscourt.GetCourtAssociation(Convert.ToInt32(ddlCourt.SelectedValue));




                if (DtCourtAssociat.Rows.Count > 0)
                {
                    var disstatus = from d in DtCourtAssociat.AsEnumerable()
                                    select d.Field<int>("CourtStatusID");


                    for (int i = 0; i < ChkStatus.Items.Count; i++)
                    {


                        if (DtCourtAssociat.Select("CourtStatusID=" + ChkStatus.Items[i].Value).Count() > 0)
                        {
                            ChkStatus.Items[i].Selected = true;
                        }

                        //var vstatus = new { Status = ChkStatus.Items[i].Value };
                        //Hstatus.Add(Convert.ToInt32(ChkStatus.Items[i].Value), ChkStatus.Items[i].Text.ToString());
                        //lstStatus.Add(ChkStatus.Items[i].Value);            

                    }
                }

            }

            //List<string> lstStatus=new List<string>();            

            if (wizCourtAssociation.ActiveStep.ID == "Step2")
            {
                BindStep2();

            }

            if (wizCourtAssociation.ActiveStep.ID == "Step3")
            {
                BindStep3();
            }


            mpCourtAssociation.Show();
        }

        protected void wizCourtAssociation_PreviousButtonClick(object sender, WizardNavigationEventArgs e)
        {
            if (wizCourtAssociation.ActiveStep.ID == "Step1")
            {

                lblDivision.Text = ddlDivision.SelectedItem.Text;
                lblCourt.Text = ddlCourt.SelectedItem.Text;
                clsCaseStatus caseStatus = new clsCaseStatus();
                ChkStatus.DataTextField = "description";
                ChkStatus.DataValueField = "id";
                ChkStatus.DataSource = caseStatus.GetAllCourtCaseStatus();
                ChkStatus.DataBind();

            }

            //List<string> lstStatus=new List<string>();            

            if (wizCourtAssociation.ActiveStep.ID == "Step4")
            {

                BindStep2();
            }

            //if (wizCourtAssociation.ActiveStep.ID == "Step4")
            //{
            //    BindStep3();
            //}


            mpCourtAssociation.Show();
        }

        protected void wizCourtAssociation_FinishButtonClick(object sender, WizardNavigationEventArgs e)
        {
            var commasepstatusroomTime = hfStatusRoomTime.Value.ToString().Split(',');

            string[] str = null;
            var statusRoomNumber =
                           new { StatusID = 0, RoomID = 0, TimeId = 0, IsDeleted = 0 };


            var lststatusroomtime = MakeList(statusRoomNumber);


            foreach (var s in commasepstatusroomTime)
            {
                str = s.Split('-');
                if (!string.IsNullOrEmpty(s))
                {
                    var st =
                        new
                            {
                                StatusID = Convert.ToInt32(str[0]),
                                RoomID = Convert.ToInt32(str[1]),
                                TimeId = Convert.ToInt32(str[2]),
                                IsDeleted = Convert.ToInt32(str[3])
                            };
                    lststatusroomtime.Add(st);
                }

            }

            foreach (var o in lststatusroomtime)
            {
                if (o.IsDeleted == 0)
                {
                    clscourt.InsertCourtAssociation(Convert.ToInt32(ddlCourt.SelectedValue), Convert.ToInt32(o.StatusID),
                                                    Convert.ToInt32(o.RoomID), Convert.ToInt32(o.TimeId)
                                                    );
                }
                else if (o.IsDeleted == 1)
                {
                    clscourt.DeleteCourtAssociation(Convert.ToInt32(ddlCourt.SelectedValue), Convert.ToInt32(o.StatusID),
                                                    Convert.ToInt32(o.RoomID), Convert.ToInt32(o.TimeId)
                                                    );
                }
            }

            mpCourtAssociation.Hide();
        }


        private DataTable GetCourtRoomNumbers()
        {
            return clscourt.GetCourtRoomNumbers();
        }

        private void BindStep2()
        {
            Hstatus = new Dictionary<int, string>();
            hfStatusRoom.Value = "";
            for (int i = 0; i < ChkStatus.Items.Count; i++)
            {
                if (ChkStatus.Items[i].Selected)
                {
                    //var vstatus = new { Status = ChkStatus.Items[i].Value };
                    Hstatus.Add(Convert.ToInt32(ChkStatus.Items[i].Value), ChkStatus.Items[i].Text.ToString());
                    //lstStatus.Add(ChkStatus.Items[i].Value);            
                }
            }


            Keys = Hstatus.Keys;

            foreach (var key in Keys)
            {
                HtmlTableRow trstatus = new HtmlTableRow();
                HtmlTableCell tdstatus = new HtmlTableCell();
                Label lblstatus = new Label();
                lblstatus.Text = Hstatus[key].ToString();
                tdstatus.Controls.Add(lblstatus);
                trstatus.Controls.Add(tdstatus);
                tbl_room.Controls.Add(trstatus);
                DataTable dtRoomNumber = GetCourtRoomNumbers();

                HtmlTableRow trcourtroom = new HtmlTableRow();

                CheckBoxList chklstRoom = new CheckBoxList();

                chklstRoom.RepeatDirection = RepeatDirection.Horizontal;
                chklstRoom.RepeatLayout = RepeatLayout.Flow;
                chklstRoom.ID = "chklstRoom" + key.ToString();

                foreach (DataRow row in dtRoomNumber.Rows)
                {


                    //CheckBox chkroom = new CheckBox();                        
                    //chkroom.ID = "chkroom" + row["CourtRoomNumberID"].ToString();
                    //chkroom.Text = row["RoomNumber"].ToString();                        
                    ListItem lstRoom = new ListItem();
                    lstRoom.Text = row["RoomNumber"].ToString();
                    lstRoom.Value = row["CourtRoomNumberID"].ToString();
                    lstRoom.Attributes.Add("onclick", "SetRoomValue(" + key + "," + row["CourtRoomNumberID"].ToString() + ",this)");


                    if (DtCourtAssociat.Select("CourtRoomNumberID=" + row["CourtRoomNumberID"].ToString() + " and CourtStatusID=" + key).Count() > 0)
                    {
                        lstRoom.Selected = true;
                        hfStatusRoom.Value = hfStatusRoom.Value + key + '-' + row["CourtRoomNumberID"].ToString() +
                                             '-' + '2' + ',';
                    }

                    if (DtStatusRoom != null)
                    {
                        DataRow[] dr = DtStatusRoom.Select("RoomID=" + row["CourtRoomNumberID"].ToString() + " and StatusID=" + key + " and IsDeleted<>2");
                        if (dr.Count() > 0)
                        {
                            lstRoom.Selected = true;
                            hfStatusRoom.Value = hfStatusRoom.Value + key + '-' + row["CourtRoomNumberID"].ToString() +
                                             '-' + dr[0]["IsDeleted"] + ',';
                        }
                    }

                    //chkStatus.Items.Add(new ListItem());       
                    //lstRoom.Selected = true;
                    chklstRoom.Items.Add(lstRoom);
                }

                HtmlTableCell tdcourtroom = new HtmlTableCell();
                HtmlTableCell tdcourtroomText = new HtmlTableCell();
                tdcourtroom.Controls.Add(chklstRoom);
                trcourtroom.Controls.Add(tdcourtroom);
                tbl_room.Controls.Add(trcourtroom);
                //CheckBoxList1.Items.Add(new ListItem());
            }
        }

        private void BindStep3()
        {
            //Control c = Page.FindControl("wizCourtAssociation_chklstRoom3");
            DtStatusRoom = null;
            var commasepstatusroom = hfStatusRoom.Value.ToString().Split(',');

            string[] str = null;
            object statusRoomNumber =
                           new { StatusID = 0, RoomID = 0, IsDeleted = 0 };
            var lststatusroom = MakeList(statusRoomNumber);

            foreach (var s in commasepstatusroom)
            {
                str = s.Split('-');
                if (!string.IsNullOrEmpty(s))
                    lststatusroom.Add(new { StatusID = Convert.ToInt32(str[0]), RoomID = Convert.ToInt32(str[1]), IsDeleted = Convert.ToInt32(str[2]) });
            }


            DtStatusRoom = LINQToDataTable(lststatusroom);



            //var groupstatusRoomNumber = from lsr in dt.AsEnumerable()
            //                            orderby lsr.Field<int>("StatusID"), lsr.Field<int>("RoomID")
            //                            select new { StatusID = lsr.Field<int>("StatusID"), RoomID = lsr.Field<int>("RoomID"), IsDeleted = lsr.Field<int>("IsDeleted") };

            var groupstatusRoomNumber = from lsr in DtStatusRoom.AsEnumerable()
                                        group lsr by lsr.Field<int>("StatusID");
            //orderby lsr.Field<int>("StatusID"), lsr.Field<int>("RoomID")
            //select new { StatusID = lsr.Field<int>("StatusID"), RoomID = lsr.Field<int>("RoomID"), IsDeleted = lsr.Field<int>("IsDeleted") };

            DataTable dtCourtTime = clscourt.GetCourtTime();

            foreach (var groups in groupstatusRoomNumber)
            {
                //Status 

                HtmlTableRow trstatus = new HtmlTableRow();
                HtmlTableCell tdstatus = new HtmlTableCell();
                tdstatus.Attributes.Add("class", "clsLeftPaddingTable");
                Label lblstatus = new Label();
                lblstatus.Text = Hstatus[groups.Key].ToString();
                tdstatus.Controls.Add(lblstatus);
                trstatus.Controls.Add(tdstatus);
                tbl_CourtTime.Controls.Add(trstatus);

                var rooms = from room in DtStatusRoom.AsEnumerable()
                            where room.Field<int>("StatusID") == groups.Key
                            select new { roomnumber = room.Field<int>("RoomID") };

                //Court Room


                foreach (var roomnumber in rooms)
                {
                    HtmlTableRow trroom = new HtmlTableRow();
                    HtmlTableCell tdroom = new HtmlTableCell();
                    Label lblroom = new Label();
                    lblroom.Text = (roomnumber.roomnumber).ToString();
                    tdroom.Controls.Add(lblroom);
                    trroom.Controls.Add(tdroom);
                    tbl_CourtTime.Controls.Add(trroom);



                    //time
                    HtmlTableRow trcourtTime = new HtmlTableRow();
                    CheckBoxList chklstTime = new CheckBoxList();

                    chklstTime.RepeatDirection = RepeatDirection.Horizontal;
                    chklstTime.RepeatLayout = RepeatLayout.Table;
                    foreach (DataRow row in dtCourtTime.Rows)
                    {
                        //CheckBox chkroom = new CheckBox();                        
                        //chkroom.ID = "chkroom" + row["CourtRoomNumberID"].ToString();
                        //chkroom.Text = row["RoomNumber"].ToString();                        
                        ListItem lstRoom = new ListItem();
                        lstRoom.Text = row["Time"].ToString();
                        lstRoom.Value = row["CourtTimeID"].ToString();
                        lstRoom.Attributes.Add("onclick", "SetTimeValue(" + groups.Key + "," + roomnumber.roomnumber + "," + row["CourtTimeID"].ToString() + ",this)");
                        //chkStatus.Items.Add(new ListItem());       
                        //lstRoom.Selected = true;
                        chklstTime.Items.Add(lstRoom);
                    }
                    HtmlTableCell tdcourtTimeText = new HtmlTableCell();
                    tdcourtTimeText.InnerHtml = "Room Number";
                    HtmlTableCell tdcourtTime = new HtmlTableCell();
                    tdcourtTime.Controls.Add(chklstTime);
                    trcourtTime.Controls.Add(tdcourtTimeText);
                    trcourtTime.Controls.Add(tdcourtTime);
                    tbl_CourtTime.Controls.Add(trcourtTime);

                }
            }
        }

        private List<T> MakeList<T>(T itemOfType)
        {
            List<T> newlist = new List<T>();
            return newlist;
        }

        public DataTable LINQToDataTable<T>(IEnumerable<T> varlist)
        {
            DataTable dtReturn = new DataTable();

            // column names 
            PropertyInfo[] oProps = null;

            if (varlist == null) return dtReturn;

            foreach (T rec in varlist)
            {
                // Use reflection to get property names, to create table, Only first time, others will follow 
                if (oProps == null)
                {
                    oProps = ((Type)rec.GetType()).GetProperties();
                    foreach (PropertyInfo pi in oProps)
                    {
                        Type colType = pi.PropertyType;

                        if ((colType.IsGenericType) && (colType.GetGenericTypeDefinition()
                        == typeof(Nullable<>)))
                        {
                            colType = colType.GetGenericArguments()[0];
                        }

                        dtReturn.Columns.Add(new DataColumn(pi.Name, colType));
                    }
                }

                DataRow dr = dtReturn.NewRow();

                foreach (PropertyInfo pi in oProps)
                {
                    dr[pi.Name] = pi.GetValue(rec, null) == null ? DBNull.Value : pi.GetValue(rec, null);
                }

                dtReturn.Rows.Add(dr);
            }
            return dtReturn;
        }






    }
}

﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using HTP.Components;
using lntechNew.Components.ClientInfo;

namespace HTP.Configuration
{
    //Saeed 8101 09/29/2010 class created.
    /// <summary>
    /// ReportSettingAttribute class
    /// </summary>
    public partial class ReportSettingAttribute : Page
    {
        readonly clsSession _clsSession = new clsSession();
        readonly clsLogger _clog = new clsLogger();
        readonly ReportAttribute _cAttribute = new ReportAttribute();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!_clsSession.IsValidSession(Request, Response, Session))
            {
                Response.Redirect("../frmlogin.aspx", false);
            }
            else //To stop page further execution
            {
                if (_clsSession.GetCookie("sAccessType", Request) != "2")
                {

                    Response.Redirect("../LoginAccesserror.aspx", false);
                    Response.End();
                }
                else //To stop page further execution
                {

                    if (!IsPostBack)
                    {
                        try
                        {
                            btn_update.Attributes.Add("onclick", "return formSubmit();");
                            ViewState["IsNewAttribute"] = "-1";
                            BindGrid();


                        }
                        catch (Exception ex)
                        {
                            lbl_Message.Text = "Cannot Find Record";
                            _clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                        }
                    }
                }
            }
        }


        /// <summary>
        /// This method is used to bind attributenames, attributeid, shortname, description to the Grid View.
        /// </summary>
        public void BindGrid()
        {

            // Getting List of All Users            
            lbl_Message.Text = "";
            var ds = _cAttribute.ListAllAttribute();

            // Displaying List of Users In DataGrid
            if (ds.Rows.Count > 0)
            {
                dg_results.DataSource = ds;
                dg_results.DataBind();
            }
            else
            {
                dg_results.DataBind();
                lbl_Message.Text = "No Record Found";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dg_results_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            if (e.CommandName == "click")
            {
                var aid = (Label)e.Item.FindControl("lbl_aid");
                ViewState["IsNewAttribute"] = aid.Text;
                var attribute = _cAttribute.GetAttributeInfo(int.Parse(ViewState["IsNewAttribute"].ToString()));

                try
                {
                    btn_update.Text = "Update";
                    tb_aname.Text = attribute.AttributeName;
                    tb_sname.Text = attribute.ShortName;
                    tb_description.Text = attribute.Description;
                    ddl_attributetype.SelectedIndex = -1;
                    ddl_attributetype.Items.FindByText(attribute.AttributeType).Selected = true;
                }
                catch (Exception ex)
                {
                    lbl_Message.Text = "Cannot Display Record";
                    _clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                }
            }
            else if (e.CommandName == "image")
            {

                var aid = (Label)e.Item.FindControl("lbl_aid");
                ViewState["IsNewAttribute"] = aid.Text;
                var attribute = new ReportAttribute();
                var result = attribute.DeleteAttributeInfo(int.Parse(ViewState["IsNewAttribute"].ToString()));

                BindGrid();
                if (result)
                {

                    ScriptManager.RegisterStartupScript(Page, GetType(), "opnewin", "alert('Cannot delete this record. It is used by Reports in our system')", true);
                }
                else
                {
                    ClearValues();
                    lbl_Message.Text = "Record Deleted Successfully";
                }


            }

        }

        /// <summary>
        /// this method is used to clear all the values from the text boxes
        /// </summary>
        private void ClearValues()
        {
            btn_update.Text = "Add";
            tb_aname.Text = "";
            tb_sname.Text = "";
            tb_description.Text = "";
            ddl_attributetype.SelectedIndex = -1;
            ViewState["IsNewAttribute"] = "-1";
            lbl_Message.Text = string.Empty;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_update_Click(object sender, EventArgs e)
        {
            var attributeid = Convert.ToString(ViewState["IsNewAttribute"]);
            var cattribute = new ReportAttribute();
            if (attributeid == "-1")
            {
                attributeid = "0";
            }
            cattribute.AttributeName = Server.HtmlEncode(tb_aname.Text.Trim());
            cattribute.ShortName = Server.HtmlEncode(tb_sname.Text);
            cattribute.Description = Server.HtmlEncode(tb_description.Text);
            cattribute.InsertDate = DateTime.Now;
            cattribute.InsertBy = int.Parse(_clsSession.GetCookie("sEmpID", Request));
            cattribute.LastUpdateDate = DateTime.Now;
            cattribute.LastUpdateBy = int.Parse(_clsSession.GetCookie("sEmpID", Request));
            cattribute.AttributeType = ddl_attributetype.SelectedItem.Text;
            var result = cattribute.InsertUpdateAttributeInfo(int.Parse(attributeid));
            if (result == "true")
            {
                ScriptManager.RegisterStartupScript(Page, GetType(), "opnewin",
                                      "alert('Invalid attribute name: This attribute name is already exists')", true);
            }
            else
            {
                lbl_Message.Text = "Record Updated Successfully";
            }
            BindGrid();
            ClearValues();

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            ClearValues();
        }
    }
}

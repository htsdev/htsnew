﻿<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="PromotionalPrices.aspx.cs" Inherits="HTP.Configuration.PromotionalPrices" %>

<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>Promotional Prices</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>

    <link href="../Styles.css" type="text/css" rel="stylesheet">

    <script language="javascript">
    function ValidateControls()
    {  
  		
			if (document.frmcompetitor.ddl_Competitors.value == "-1")
			{
				alert("Please specify Competitor Name");
				document.frmcompetitor.ddl_Competitors.focus();
				return false;
			}
			if (document.frmcompetitor.txt_RegularPrice.value == "")
			{
			    alert("Please enter Regular Price");
			    document.frmcompetitor.txt_RegularPrice.focus();
			    return false;
			
			}
			if (document.frmcompetitor.txt_BondPrice.value == "")
			{
			    alert("Please enter Bond Price");
			    document.frmcompetitor.txt_BondPrice.focus();
			    return false;
			
			}

			if (isNumeric(document.getElementById("txt_RegularPrice").value) == false)
			{
			    alert("Sorry, invalid Regular price amount.")
			    frmcompetitor.txt_RegularPrice.focus();
			    return false;
			}
			if (isNumeric(document.getElementById("txt_BondPrice").value) == false)
			{
			    alert("Sorry, invalid Bond price amount.")
			    frmcompetitor.txt_BondPrice.focus();
			    return false;
			}
			if (document.getElementById("txt_RegularPrice").value == 0 || document.getElementById("txt_RegularPrice").value >= 1000)
			{
			    alert("Value should be between 1-999")
			    frmcompetitor.txt_RegularPrice.focus();
			    return false;
			}
			if (document.getElementById("txt_BondPrice").value == 0 || document.getElementById("txt_BondPrice").value >= 1000)
			{
			    alert("Value should be between 1-999")
			    frmcompetitor.txt_BondPrice.focus();
			    return false;
			}

	}
			
    function isNumeric(value) 
	{
        if (value == null || !value.toString().match(/^[-]?\d*\.?\d*$/)) 
        return false;
        return true;
    }
				

    
    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>

<body>
    <form id="frmcompetitor" method="post" runat="server">
    <table id="TableMain" style="z-index: 101" cellspacing="0" cellpadding="0" width="780"
        align="center" border="0">
        <tr>
            <td>
                <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
            </td>
        </tr>
        <tr>
            <td width="780" background="../../images/separator_repeat.gif" colspan="5" height="11">
            </td>
        </tr>
        <tr>
            <td>
                <div id="Divadd" visible="false" runat="server">
                    <table class="clsleftpaddingtable" width="100%" border="1" bordercolor="white">
                    <tr class="clsleftpaddingtable">
                        <asp:Panel ID="pnlCompetitor" runat="server">
                        <td style="width: 120px" class="clsleftpaddingtable">
                            Competitor:
                        </td>
                        <td width="210">
                            <asp:DropDownList ID="ddl_Competitors"
                                    runat="server" Width="206px" CssClass="clsinputcombo" DataTextField="FirmFullName" DataValueField="FirmID">
                                </asp:DropDownList></asp:Panel>
                        </td>
                        
                        
                    </tr>
                    <tr class="clsleftpaddingtable">
                    <asp:Panel ID="pnlShowZipCode" runat="server" Visible="false">
                        <td style="width: 120px" class="clsleftpaddingtable">
                            Zip Code (Comma Separated)</td>
                        <td width="210">
                            <asp:TextBox ID="txt_ZipCode" runat="server" Width="206px" CssClass="clsinputadministration"
                                MaxLength="30"></asp:TextBox>
                        </td>
                        </asp:Panel>
                        
                    </tr>
                    <tr class="clsleftpaddingtable">
                        <td style="width: 100px; height: 25px;" class="clsleftpaddingtable">
                            Promotional Price $:
                        </td>
                        <td style="width: 215px; height: 25px;">
                            <asp:TextBox ID="txt_RegularPrice" runat="server" Width="206px" CssClass="clsinputadministration"
                                MaxLength="30"></asp:TextBox>
                        </td>
                        
                    </tr>
                    <tr class="clsleftpaddingtable">
                        <td style="width: 100px; height: 23px" class="clsleftpaddingtable">
                            Bond Price $:
                        </td>
                        <td style="width: 215px; height: 23px">
                                <asp:TextBox ID="txt_BondPrice" runat="server" Width="206px" CssClass="clsinputadministration"
                                MaxLength="30"></asp:TextBox>
                        
                        </td>
                        
                    </tr>
                 
                    <tr class="clsleftpaddingtable">
                        <td style="width: 140px; height: 25px;" class="clsleftpaddingtable">
                            Matter Message:
                        </td>
                        <td style="width: 215px; height: 25px;">
                            <asp:TextBox ID="txt_MatterMsg" runat="server" Width="206px" CssClass="clsinputadministration"
                                MaxLength="20" TextMode="MultiLine"></asp:TextBox>
                        </td>
                        
                    </tr>
                    <tr class="clsleftpaddingtable">
                        <td style="width: 140px; height: 25px;" class="clsleftpaddingtable">
                            Active:
                        </td>
                        <td style="width: 215px; height: 25px;">
                            <asp:CheckBox ID="cb_Active" runat="server" CssClass="label" Checked="true" />
                        </td>
                        
                    </tr>

                    
                    
                   
                     
                    <tr runat="server" id="tr_textmessage" class="clsleftpaddingtable" style="display:none">
                        
                        <td class="clsleftpaddingtable" colspan="2">
                            
                        </td>
                    </tr>
                    <tr class="clsleftpaddingtable">
                        <td style="height: 1px" class="clsleftpaddingtable" colspan="2">
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            <asp:Label ID="lbl_message" runat="server" CssClass="label" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    <tr class="clsleftpaddingtable">
                        <td style="height: 25px" class="clsleftpaddingtable" colspan="2" align="center">
                            <asp:Button ID="btn_Clear" runat="server" CssClass="clsbutton" Text="Clear" 
                                onclick="btn_Clear_Click"></asp:Button>
                            &nbsp;<asp:Button ID="btn_Submit" runat="server" CssClass="clsbutton" Text="Add" 
                                onclick="btn_Submit_Click"></asp:Button>&nbsp;<asp:Button
                                ID="btn_delete" runat="server" CssClass="clsbutton" Text="Delete" 
                                onclick="btn_delete_Click"></asp:Button>
                        </td>
                    </tr>
                </table>
        
                </div>
            </td>
        </tr>  
        <tr>
            <td background="../../images/separator_repeat.gif" style="height: 10px">
            </td>
        </tr>
        <tr>
            <td background="../Images/subhead_bg.gif" class="clssubhead" align="right" style="height: 34px;">
                <table style="width: 100%;">
                    <tr>

                        <td style="text-align: right;">
                         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                         
                         <asp:LinkButton ID="lnkbtnZipCode" runat="server" 
                                OnClick="lnkbtnZipCode_Click">Insert Zip Code Pricing</asp:LinkButton>
                           
                        &nbsp;
                         <asp:LinkButton ID="lnkbtnInsertScheme" runat="server" OnClick="lnkbtnInsertScheme_Click">Insert Scheme</asp:LinkButton>
                          
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table id="tblgrid" cellspacing="0" cellpadding="0" width="780" align="center" border="0">
                    <tr>
                        <td align="center">
                       
                        <asp:GridView ID="gvResult" runat="server" AutoGenerateColumns="False" 
                                CssClass="clsLeftPaddingTable" CellPadding="3" Width="780px" 
                                AllowPaging="True" PageSize="20" 
                                OnRowCommand="gvResult_RowCommand" DataKeyNames="FirmId,SchemeId">
                        <Columns>
                                <asp:TemplateField SortExpression="FirmId" Visible="False"></asp:TemplateField>
                                <asp:TemplateField></asp:TemplateField>
                                <asp:TemplateField HeaderText="Firm Name">
                                        <HeaderStyle Width="25%" CssClass="clsaspcolumnheader"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkbtnFirmName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.firmname") %>' CommandArgument='<%# DataBinder.Eval(Container, "DataItem.firmname") %>' 
                                                CommandName="FNameClicked">
                                            </asp:LinkButton>
                                            <asp:Label ID="lbl_FirmID" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.firmid") %>'
                                                Visible="False">
                                            </asp:Label>
                                            </ItemTemplate>
                                 </asp:TemplateField>

                                    
                                    <asp:TemplateField HeaderText="Promotional Price $">
                               <ItemTemplate>
                                                              
                                     
                                            <asp:Label ID="lblPromoPrice" runat="server" Text='<%# Eval("Promotional Price $","{0:C0}") %>'
                                                CssClass="Label"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Bond Price $">
                                     <ItemTemplate>
                                            
                         
                                       
                                            <asp:Label ID="lblBondPrice" runat="server" CssClass="Label" Text='<%# Eval("Bond Price $","{0:C0}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Message for Matter's Page">
                                    <ItemTemplate>
                                            
                   
                                   
                                            <asp:Label ID="lblMatterMessage" runat="server" Width="100px" CssClass="clsLabel" Text='<%# Eval("Message for Matter Page","{0:C0}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                   <ItemTemplate>
                                   <asp:ImageButton ID="DeleteButton" runat="server" ImageUrl="~/Images/remove2.gif"
                                   CommandArgument='<%# DataBinder.Eval(Container, "DataItem.firmname") %>' CommandName="Delete" />
                                   </ItemTemplate>
                                   </asp:TemplateField>
                                    
                                                        </Columns>
                                                    </asp:GridView>
                        
                        
                        
                            
                        </td>
                    </tr>
                </table>
            </td>
        </tr>


      
        <tr>
            <td background="../../images/separator_repeat.gif" colspan="6" height="11">
            </td>
        </tr>
        <tr>
            <td colspan="5" style="height: 48px">
                <asp:HiddenField ID="HfSchemeID" runat="server" />
                <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>

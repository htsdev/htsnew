<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ConfigurationSettingReport.aspx.cs"
    Inherits="HTP.Configuration.ConfigurationSettingReport" %>

<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc1" %>
<%@ Register TagPrefix="uc5" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Configuration Settings </title>
    <link href="../Styles.css" type="text/css" rel="stylesheet" />

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
     // Fahad 7287 01/23/2010 Hide modal popup
        function HideAddPopup() 
        {
            var modalPopupBehavior = $find('mpeConfigAdd');
            modalPopupBehavior.hide();
            return false;
        }
        // Fahad 7287 01/23/2010 Hide modal popup
         function HideEditPopup() 
        {
            var modalPopupBehavior = $find('mpeConfigEdit');
            modalPopupBehavior.hide();
            return false;
        }
        // Fahad 7287 01/23/2010 To Validate Data
        function CheckAddData()
        {
               var txtboxdesc=document.getElementById("txtAddDescription").value;
               var txtboxvalue=document.getElementById("txtAddValue").value;
               txtboxdesc=trim(txtboxdesc);
               txtboxvalue=trim(txtboxvalue);
               
                   if (document.getElementById("ddlprogramPop").value == "NONE")
                {
                    alert("Please select program.");
                    return false;
                }
                else if (document.getElementById("ddlModulePop").value == "NONE")
                {
                    alert("Please select module.");
                    return false;
                }
                   else if (document.getElementById("ddlSubModulePop").value == "NONE")
                {
                    alert("Please select sub-module.");
                    return false;
                }
                else if (document.getElementById("ddlKeyPop").value == "NONE")
                {
                    alert("Please select key.");
                    return false;
                }
              
			   else if(txtboxdesc=="")
			   {
				alert ("Please enter the description of the key")		
				document.getElementById("txtAddDescription").focus(); 
				return false;				
			   }
			   else if(txtboxdesc=="")        
			   {
				alert ("Please enter the value of the key")		
				document.getElementById("txtAddValue").focus(); 
				return false;				
			   }
		
            return true;
        }
        // Fahad 7287 01/23/2010 To Validate Data
         function CheckEditData()
        {
               var txtboxdesc=document.getElementById("txtEditDesc").value;
               var txtboxvalue=document.getElementById("txtEditValue").value;
               txtboxdesc=trim(txtboxdesc);
               txtboxvalue=trim(txtboxvalue);
			   if(txtboxdesc=="")
			   {
				alert ("Please enter the description of the key")		
				document.getElementById("txtEditDesc").focus(); 
				return false;				
			   }
			   else if(txtboxdesc=="")        
			   {
				alert ("Please enter the value of the key")		
				document.getElementById("txtEditValue").focus(); 
				return false;				
			   }
		
            return true;
        }
        
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <aspnew:ScriptManager ID="ScriptManager" runat="server" />
        <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline">
            <ContentTemplate>
                <table cellspacing="0" cellpadding="0" width="900px" align="center" border="0">
                    <tr>
                        <td>
                            <uc1:ActiveMenu ID="ActiveMenu" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td background="../Images/separator_repeat.gif" height="11">
                        </td>
                    </tr>
                    <tr align="right">
                        <td>
                            <asp:LinkButton ID="lnkbtnAddNewRecord" runat="server" OnClick="lnkbtnAddNewRecord_Click">Add New Record</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100%" background="../Images/separator_repeat.gif" height="11">
                        </td>
                    </tr>
                    <tr>
                        <td class="clsLeftPaddingTable">
                            <table border="0" width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <span class="clssubhead">Program Type :</span>&nbsp;
                                        <asp:DropDownList ID="ddlDivison" runat="server" CssClass="clsInputadministration"
                                            Width="100px" AutoPostBack="true" OnSelectedIndexChanged="ddlDivison_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <span class="clssubhead">Module Type :</span>
                                        <asp:DropDownList ID="ddlModule" runat="server" CssClass="clsInputadministration"
                                            Width="200px" AutoPostBack="true" OnSelectedIndexChanged="ddlModule_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <span class="clssubhead">Sub-Module Type :</span>
                                        <asp:DropDownList ID="ddlSubModule" runat="server" CssClass="clsInputadministration"
                                            Width="100px" AutoPostBack="true" OnSelectedIndexChanged="ddlSubModule_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <span class="clssubhead">Key Type :</span>
                                        <asp:DropDownList ID="ddlKey" runat="server" CssClass="clsInputadministration" AutoPostBack="true"
                                            Width="100px" OnSelectedIndexChanged="ddlKey_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../Images/separator_repeat.gif" height="11">
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td background="../Images/subhead_bg.gif" class="clssubhead" align="left" style="height: 34px">
                            <table width="100%">
                                <tr>
                                    <td align="left" class="clssubhead">
                                        Configuration Settings
                                    </td>
                                    <td align="right" class="clssubhead">
                                        <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../../images/separator_repeat.gif" height="11" style="height: 11px;
                            width: 100%">
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Label ID="lblMessage" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table id="TableGrid" bgcolor="white" border="0" cellpadding="0" cellspacing="0"
                                width="100%">
                                <tr>
                                    <td align="center" colspan="2" valign="top">
                                        <aspnew:UpdateProgress ID="updateprogress" runat="server" AssociatedUpdatePanelID="upnlResult">
                                            <ProgressTemplate>
                                                <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl1" runat="server" Text="Please Wait ......"
                                                    CssClass="clsLabel"></asp:Label>
                                            </ProgressTemplate>
                                        </aspnew:UpdateProgress>
                                        <asp:GridView ID="gv_Records" runat="server" AutoGenerateColumns="False" Width="100%"
                                            CssClass="clsLeftPaddingTable" OnRowDataBound="gv_Records_RowDataBound" AllowPaging="True"
                                            OnPageIndexChanging="gv_Records_PageIndexChanging" PageSize="20" OnRowCommand="gv_Records_RowCommand">
                                            <Columns>
                                                <asp:TemplateField HeaderText="S#">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_Sno" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.SNo") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Division" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_Division" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.DivisionName") %>'></asp:Label>
                                                        <asp:HiddenField ID="hf_Program" runat="server" Value='<%#Eval("DivisionID") %>' />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Module Name" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_Module" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.ModuleName") %>'></asp:Label>
                                                        <asp:HiddenField ID="hf_Module" runat="server" Value='<%#Eval("ModuleID") %>' />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Sub-Module Name" HeaderStyle-CssClass="clssubhead"
                                                    HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_SubModuleName" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.SubModuleName") %>'></asp:Label>
                                                        <asp:HiddenField ID="hf_SubModule" runat="server" Value='<%#Eval("SubModuleID") %>' />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Key" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_Key" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.KeyName") %>'></asp:Label>
                                                        <asp:HiddenField ID="hf_Key" runat="server" Value='<%#Eval("KeyID") %>' />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Value" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_Value" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.Value") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Description" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_Description" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.Description") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Modify" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkEdit" runat="server" Text="Edit" CommandName="btnEditclick"></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerStyle HorizontalAlign="Center" />
                                            <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td background="../../images/separator_repeat.gif" colspan="5" style="height: 11px;
                                        width: 100%">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <uc5:Footer ID="Footer" runat="server"></uc5:Footer>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ajaxToolkit:ModalPopupExtender ID="mpeConfigAdd" runat="server" BackgroundCssClass="modalBackground"
                                PopupControlID="pnlAdd" TargetControlID="btn123">
                            </ajaxToolkit:ModalPopupExtender>
                            <asp:Panel ID="pnlAdd" runat="server" Width="400px" Style="display: none;">
                                <table id="table7" style="border-top: black thin solid; border-left: black thin solid;
                                    border-bottom: black thin solid; border-right: black thin solid;" cellpadding="0"
                                    cellspacing="0" width="100%" class="clsLeftPaddingTable">
                                    <tr>
                                        <td background="../Images/subhead_bg.gif" valign="bottom" width="100%" style="height: 32px">
                                            <table border="0" width="100%" style="height: 100%">
                                                <tr>
                                                    <td align="left">
                                                        <span class="clssubhead">Add Configuration Settings</span>
                                                    </td>
                                                    <td align="right">
                                                        <asp:LinkButton ID="lbtn_close" OnClientClick="return HideAddPopup();" runat="server">X</asp:LinkButton>&nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td align="center" colspan="2" valign="middle">
                                                        <asp:Label ID="lblAddmsg" Visible="false" runat="server" Text="" Font-Names="Arial"
                                                            Font-Size="X-Small" ForeColor="Red"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        <span class="clssubhead">Program Type :</span>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlprogramPop" runat="server" CssClass="clsInputadministration"
                                                            Width="200px">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        <span class="clssubhead">Module Type :</span>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlModulePop" runat="server" CssClass="clsInputadministration"
                                                            Width="200px">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        <span class="clssubhead">Sub-Module Type :</span>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlSubModulePop" runat="server" CssClass="clsInputadministration"
                                                            Width="200px">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        <span class="clssubhead">Key Type :</span>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlKeyPop" runat="server" CssClass="clsInputadministration"
                                                            Width="200px">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        <span class="clssubhead">Value Type :</span>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtAddValue" TextMode="MultiLine" Wrap="true" Width="200px" CssClass="clsInputadministration"
                                                            runat="server"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        <span class="clssubhead">Description :</span>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtAddDescription" runat="server" CssClass="clsInputadministration"
                                                            TextMode="MultiLine" Width="200px" Wrap="true"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnAdd" runat="server" CssClass="clsbutton" OnClientClick="return CheckAddData();"
                                                            Text="Add" OnClick="btnAdd_Click" />
                                                        <asp:Button ID="btnClose" runat="server" CssClass="clsbutton" OnClientClick="return HideAddPopup();"
                                                            Text="Close" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Button ID="btn123" runat="server" Style="display: none;" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ajaxToolkit:ModalPopupExtender ID="mpeConfigEdit" runat="server" BackgroundCssClass="modalBackground"
                                PopupControlID="pnlEdit" TargetControlID="btn">
                            </ajaxToolkit:ModalPopupExtender>
                            <asp:Panel ID="pnlEdit" runat="server" Width="400px" Style="display: none;">
                                <table id="table1" style="border-top: black thin solid; border-left: black thin solid;
                                    border-bottom: black thin solid; border-right: black thin solid;" cellpadding="0"
                                    cellspacing="0" width="100%" class="clsLeftPaddingTable">
                                    <tr>
                                        <td background="../Images/subhead_bg.gif" valign="bottom" width="100%" style="height: 32px">
                                            <table border="0" width="100%" style="height: 100%">
                                                <tr>
                                                    <td align="left">
                                                        <span class="clssubhead">Update Configuration Settings</span>
                                                    </td>
                                                    <td align="right">
                                                        <asp:LinkButton ID="LinkButton1" OnClientClick="return HideEditPopup();" runat="server">X</asp:LinkButton>&nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td align="center" colspan="2" valign="middle">
                                                        <asp:Label ID="lbleditmsg" Visible="false" runat="server" Text="" Font-Names="Arial"
                                                            Font-Size="X-Small" ForeColor="Red"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        <span class="clssubhead">Program Type :</span>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblProgramType" runat="server" class="clsLeftPaddingTable" Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        <span class="clssubhead">Module Type :</span>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblModuleType" runat="server" class="clsLeftPaddingTable" Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        <span class="clssubhead">Sub-Module Type :</span>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblSubModuleType" runat="server" class="clsLeftPaddingTable" Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        <span class="clssubhead">Key Type :</span>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblKey" runat="server" class="clsLeftPaddingTable" Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        <span class="clssubhead">Value Type :</span>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtEditValue" TextMode="MultiLine" Wrap="true" Width="200px" CssClass="clsInputadministration"
                                                            runat="server"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        <span class="clssubhead">Description :</span>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtEditDesc" runat="server" CssClass="clsInputadministration" TextMode="MultiLine"
                                                            Width="200px" Wrap="true"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnEdit" runat="server" CssClass="clsbutton" OnClientClick="return CheckEditData();"
                                                            OnClick="btnUpdate_Click" Text="Update" />
                                                        <asp:Button ID="btnExit" runat="server" CssClass="clsbutton" OnClientClick="return HideEditPopup();"
                                                            Text="Close" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Button ID="btn" runat="server" Style="display: none;" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </aspnew:UpdatePanel>
    </div>
    </form>
</body>
</html>

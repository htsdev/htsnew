﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;

namespace HTP.Activities
{
    public partial class AttorneyPayoutConfiguration : System.Web.UI.Page
    {
        #region Declaration

        clsENationWebComponents ClsDb = new clsENationWebComponents();
        clsViolations ClsViolations = new clsViolations();
        clsFirms clsfirm = new clsFirms();
        clsSession cSession = new clsSession();

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (cSession.GetCookie("sAccessType", this.Request).ToString() != "2")
                {
                    Response.Redirect("../LoginAccesserror.aspx", false);
                }

                if (!IsPostBack)
                {
                    FillCriminalAttorney();
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {

                DataTable dtchargelevel = clsfirm.GetFirmChargeLevelAmount(Convert.ToInt32(ddlAttorneys.SelectedValue));
                if (dtchargelevel.Rows.Count > 0)
                {
                    gv_Records.DataSource = dtchargelevel;
                    gv_Records.DataBind();
                    //Nasir 6869 01/27/2010 display when serach button click
                    btnupdate.Style[HtmlTextWriterStyle.Display] = "block";
                    tr_seperator.Style[HtmlTextWriterStyle.Display] = "block";
                    gv_Records.Style[HtmlTextWriterStyle.Display] = "block";
                    lbl_Message.Text = "";
                }
                else
                {
                    //Nasir 6869 01/27/2010 Hide when no record found
                   // lbl_Message.Text = "No Record Found";
                    LblSucessdiv.Attributes.Add("class", "alert alert-danger alert-dismissable fade in");
                    LblSucesstext.Text = "No Record Found";
                    LblSucessdiv.Visible = true;
                    tr_seperator.Style[HtmlTextWriterStyle.Display] = "none";
                    btnupdate.Style[HtmlTextWriterStyle.Display] = "none";
                    gv_Records.Style[HtmlTextWriterStyle.Display] = "none";
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void btnupdate_Click(object sender, EventArgs e)
        {
            try
            {
                string amount = string.Empty;
                string chargelevelid = string.Empty;
                foreach (GridViewRow gvr in gv_Records.Rows)
                {
                    amount = ((TextBox)(gvr.FindControl("txt_price"))).Text;
                    chargelevelid = ((HiddenField)(gvr.FindControl("hf_chargelevelID"))).Value;
                    clsfirm.UpdateChargeAmount(Convert.ToInt32(ddlAttorneys.SelectedValue), Convert.ToDouble(amount), Convert.ToInt32(chargelevelid));
                    lbl_Message.Text = "Records updated successfully";
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
            }
        }

        #endregion

        #region Method

        private void FillCriminalAttorney()
        {
            DataTable dt = clsfirm.GetCriminalAttorney();
            // Noufil 6869 11/02/2009 implement check if data not empty
            if (dt.Rows.Count > 0)
                ddlAttorneys.DataSource = dt;
            
            ddlAttorneys.DataBind();
            // Noufil 6869 10/30/2009 Insert Choose option in dropdown
            ddlAttorneys.Items.Insert(0, new ListItem("--- Choose ---", "-1"));
        }

        #endregion
    }
}

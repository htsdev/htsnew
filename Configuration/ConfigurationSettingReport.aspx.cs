using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;
using HTP.Components.Services;
using Configuration.Helper;
using Configuration.Client;
using MetaBuilders.WebControls;
using Configuration.Common;

namespace HTP.Configuration
{
    public partial class ConfigurationSettingReport : System.Web.UI.Page
    {
        // Fahad Muhammad Qureshi 7287 01/22/2010  grouping variables
        #region Variables

        clsSession ClsSession = new clsSession();
        clsLogger clog = new clsLogger();
        clsCase cCase = new clsCase();
        clsLogger bugTracker = new clsLogger();
        DataSet DS;
        ValidationReports reports = new ValidationReports();
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        ConfigurationService configurationService = new ConfigurationService();

        #endregion

        // Fahad Muhammad Qureshi 7287 01/22/2010   grouping Event Handlers
        #region Event Handler

        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                // Zeeshan Haider 11387 09/09/2013 Added access to configuration setting report page
                bool accConfigSetting = ConfigurationManager.AppSettings["ConfigurationSettingReportAccess"]
                    .Contains(ClsSession.GetCookie("sUserID", this.Request).Trim());

                accConfigSetting = accConfigSetting
                                       ? true
                                       : (ClsSession.GetCookie("sAccessType", this.Request) == "2");

                if (!accConfigSetting)
                {
                    Response.Redirect("../LoginAccesserror.aspx", false);
                }
                else if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else
                {
                    if (!IsPostBack)
                    {
                        FillComboBoxValues();
                        FillGrid();
                        gv_Records.Visible = false;
                    }
                    Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
                    Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
                    Pagingctrl.GridView = gv_Records;

                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }
        }

        protected void gv_Records_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ((LinkButton)e.Row.FindControl("lnkEdit")).CommandArgument = e.Row.RowIndex.ToString();
                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }
        }

        protected void gv_Records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_Records.PageIndex = e.NewPageIndex;
                FillGrid();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }
        }

        protected void gv_Records_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "btnEditclick")
                {

                    int RowID = Convert.ToInt32(e.CommandArgument);

                    ViewState["ProgramType"] = (((HiddenField)gv_Records.Rows[RowID].FindControl("hf_Program")).Value);
                    ViewState["ModuleType"] = (((HiddenField)gv_Records.Rows[RowID].FindControl("hf_Module")).Value);
                    ViewState["SubModuleType"] = (((HiddenField)gv_Records.Rows[RowID].FindControl("hf_SubModule")).Value);
                    ViewState["Key"] = (((HiddenField)gv_Records.Rows[RowID].FindControl("hf_Key")).Value);

                    lblProgramType.Text = Convert.ToString((Division)Convert.ToInt32(ViewState["ProgramType"]));
                    lblModuleType.Text = Convert.ToString((Module)Convert.ToInt32(ViewState["ModuleType"]));
                    lblSubModuleType.Text = Convert.ToString((SubModule)Convert.ToInt32(ViewState["SubModuleType"]));
                    lblKey.Text = Convert.ToString((Key)Convert.ToInt32(ViewState["Key"]));

                    // Rab Nawaz Khan 10914 06/13/2013 Added for the SMS Text to Display proper format. . . 
                    //if ((Convert.ToInt32(Division.HOUSTON) == 1 && Convert.ToInt32(Module.SMS_WORKFLOW) == 1 && Convert.ToInt32(SubModule.SMS) == 1)
                    //    && ((Convert.ToInt32(Key.SMS_REMINDER_TEXT) == 37) || Convert.ToInt32(Key.SMS_SET_TEXT) == 38))
                    //{
                    //    txtEditValue.Text = (((Label)gv_Records.Rows[RowID].FindControl("lbl_Value")).Text).Replace("&lt;", "<");
                    //    txtEditValue.Text = txtEditValue.Text.Replace("&gt;", ">");
                    //}
                    //else
                    //{
                    //    txtEditValue.Text = (((Label)gv_Records.Rows[RowID].FindControl("lbl_Value")).Text);
                    //}
                    //txtEditValue.Text = (((Label)gv_Records.Rows[RowID].FindControl("lbl_Value")).Text);
                    txtEditDesc.Text = (((Label)gv_Records.Rows[RowID].FindControl("lbl_Description")).Text);
                    mpeConfigEdit.Show();

                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }

        }

        // Fahad Muhammad Qureshi 7287 01/22/2010 Filter family report
        /// <summary>
        /// Event handler to filter report according to criteria
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnFamilySubmit_Click(object sender, EventArgs e)
        {
            try
            {
                gv_Records.PageIndex = 0;
                FillGrid();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }

        }

        protected void ddlDivison_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                FillGrid();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }

        }

        protected void ddlModule_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                FillGrid();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }
        }

        protected void ddlSubModule_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                FillGrid();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }
        }

        protected void ddlKey_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                FillGrid();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                configurationService.SetKeyValue(ViewState["ProgramType"].ToString(), ViewState["ModuleType"].ToString(), ViewState["SubModuleType"].ToString(), ViewState["Key"].ToString(), txtEditValue.Text, txtEditDesc.Text);
                FillGrid();
                lbleditmsg.Text = "Value Updated successfully.";
                txtEditValue.Text = string.Empty;
                txtEditDesc.Text = string.Empty;
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }
        }

        protected void lnkbtnAddNewRecord_Click(object sender, EventArgs e)
        {
            try
            {
                mpeConfigAdd.Show();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                configurationService.SetKeyValue(ddlprogramPop.SelectedItem.ToString(), ddlModulePop.SelectedItem.ToString(), ddlSubModulePop.SelectedItem.ToString(), ddlKeyPop.SelectedItem.ToString(), txtAddValue.Text, txtAddDescription.Text);
                FillGrid();
                lblAddmsg.Text = "Value Added successfully.";
                txtAddValue.Text = string.Empty;
                txtAddDescription.Text = string.Empty;
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }

        }

        #endregion

        // Fahad Muhammad Qureshi 7287 01/22/2010  grouping Methods
        #region Methods

        private void FillGrid()
        {
            // Fahad Muhammad Qureshi 7287 01/22/2010 Show Record according to report selection
            try
            {
                lblMessage.Text = string.Empty;
                gv_Records.Visible = true;
                
                DataTable dt = configurationService.GetSelectedData(ddlDivison.SelectedValue.ToString(), ddlModule.SelectedValue.ToString(), ddlSubModule.SelectedValue.ToString(), ddlKey.SelectedValue.ToString());
                if (dt.Rows.Count > 0)
                {
                    // Fahad Muhammad Qureshi 7287 01/22/2010 Set grivview page size 
                    dt.Columns.Add("sno");
                    // Fahad Muhammad Qureshi 7287 01/22/2010 GridView Name Changed in PaggingControl
                    Pagingctrl.GridView = gv_Records;
                    gv_Records.DataSource = dt;
                    gv_Records.DataBind();
                    Pagingctrl.PageCount = gv_Records.PageCount;
                    Pagingctrl.PageIndex = gv_Records.PageIndex;
                    Pagingctrl.SetPageIndex();
                }
                else
                {
                    // Fahad Muhammad Qureshi 7287 01/22/2010 Set grivview page size
                    Pagingctrl.PageCount = 0;
                    Pagingctrl.PageIndex = 0;
                    Pagingctrl.SetPageIndex();
                    lblMessage.Text = "No Records Found";
                    gv_Records.Visible = false;
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        void Pagingctrl_PageIndexChanged()
        {

            gv_Records.PageIndex = Pagingctrl.PageIndex - 1;
            FillGrid();

        }

        // Fahad Muhammad Qureshi 7287 01/22/2010  page size change event handler
        /// <summary>
        /// Page Size change event handler
        /// </summary>
        /// <param name="pageSize"></param>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            if (pageSize > 0)
            {
                gv_Records.PageIndex = 0;
                gv_Records.PageSize = pageSize;
                gv_Records.AllowPaging = true;
            }
            else
            {
                gv_Records.AllowPaging = false;
            }
            FillGrid();
        }

        private void FillComboBoxValues()
        {
            PopulateList(ddlDivison, Enum.GetNames(typeof(Division)));
            PopulateList(ddlModule, Enum.GetNames(typeof(Module)));
            PopulateList(ddlSubModule, Enum.GetNames(typeof(SubModule)));
            PopulateList(ddlKey, Enum.GetNames(typeof(Key)));

            PopulateList(ddlprogramPop, Enum.GetNames(typeof(Division)));
            PopulateList(ddlModulePop, Enum.GetNames(typeof(Module)));
            PopulateList(ddlSubModulePop, Enum.GetNames(typeof(SubModule)));
            PopulateList(ddlKeyPop, Enum.GetNames(typeof(Key)));


        }

        private void PopulateList(DropDownList cmb, string[] values)
        {
            cmb.DataSource = values;
            cmb.DataBind();
        }

        #endregion

    }
}

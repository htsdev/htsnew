﻿<%@ Register TagPrefix="uc1" TagName="LoginMenu" Src="WebControls/LoginMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="WebControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuTop" Src="WebControls/MenuTop.ascx" %>

<%@ Page Language="c#" AutoEventWireup="false" Inherits="HTP.frmLogin" CodeBehind="frmLogin.aspx.cs" %>

<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie ie9" lang="en" class="no-js"> <![endif]-->
<!--[if !(IE)]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->

<head>
   <%-- <title>Login - Enation</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <link href="Styles.css" type="text/css" rel="stylesheet" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />

    <!-- CSS -->
	<link href="assets/css/bootstrap.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/ionicons.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/main.css" rel="stylesheet" type="text/css" />
	<!-- Google Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,300,400,700' rel='stylesheet' type='text/css' />--%>
    <head>
        <!-- 
         * @Package: Complete Admin - Responsive Theme
         * @Subpackage: Bootstrap
         * @Version: 2.2
         * This file is part of Complete Admin Theme.
        -->
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        <meta charset="utf-8" />
        <title>Traffic Ticket CRM : Login Page</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        
        <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon" />    <!-- Favicon -->
        <link rel="apple-touch-icon-precomposed" href="assets/images/apple-touch-icon-57-precomposed.png">	<!-- For iPhone -->
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/images/apple-touch-icon-114-precomposed.png">    <!-- For iPhone 4 Retina display -->
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/images/apple-touch-icon-72-precomposed.png">    <!-- For iPad -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/images/apple-touch-icon-144-precomposed.png">    <!-- For iPad Retina display -->




        <!-- CORE CSS FRAMEWORK - START -->
        <link href="assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen"/>
        <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/animate.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css"/>
        <!-- CORE CSS FRAMEWORK - END -->

        <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START --> 
        
        
<link href="assets/plugins/icheck/skins/all.css" rel="stylesheet" type="text/css" media="screen"/>

        <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END --> 


        <!-- CORE CSS TEMPLATE - START -->
        <link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/responsive.css" rel="stylesheet" type="text/css"/>
        <!-- CORE CSS TEMPLATE - END -->

    </head>
    <style type="text/css">
        label {
            padding-left: 5px;
        }
    </style>

    <script type="text/javascript" language="javascript">
        // Noufil 4378 08/06/2008 Allow only alphabets and number in password n user name
        //$(document).ready (function(){
        //    $(document).ready (function(){
        //        window.setTimeout(function () { 
        //            $(".alert alert-danger").alert('close');}, 2000);
        //    });  
        //});  
       // });

		function CheckName(name)
        {       
            for ( i = 0 ; i < name.length ; i++)
            {
                var asciicode =  name.charCodeAt(i)
                //If not valid alphabet 
                if (  ! ((asciicode >= 64 && asciicode <=90) || ( asciicode >= 97 && asciicode <=122)||(asciicode >= 48 && asciicode <=57)))
                return false;
           }
            return true;
        }
        
		function ValidateLogin()
		{		
			if (document.LoginForm.txtUsrId.value=='')  
			{
			    $("#dvUserID").attr("style", "display:block");
			    $(document).ready(function () {
			        window.setTimeout(function () {
			            $(".alert").attr("style", "display:none");
			        }, 2000);
			    });
				//alert ("Please enter User ID.");
				document.LoginForm.txtUsrId.focus(); 
				return(false);					
			}				
			if (document.LoginForm.txt_Password.value=='')  
			{
			    $("#dvPassword").attr("style", "display:block");
			    $(document).ready(function () {
			        window.setTimeout(function () {
			            $(".alert").attr("style", "display:none");
			        }, 2000);
			    });
				document.LoginForm.txt_Password.focus(); 
				return(false);					
			}
			
			if (document.getElementById("ddl_Branch").selectedIndex==0) 
			{
			    
			    $("#dvBranch").attr("style", "display:block");
			    $(document).ready(function () {
			        window.setTimeout(function () {
			            $(".alert").attr("style", "display:none");
			        }, 2000);
			    });
		        document.getElementById("ddl_Branch").focus();
		        return false;
			}
			
			if (CheckName(document.getElementById("txtUsrId").value)== false)
			{
			    $("#dvAlpha").attr("style", "display:block");
			    $(document).ready(function () {
			        window.setTimeout(function () {
			            $(".alert").attr("style", "display:none");
			        }, 2000);
			    });
			    document.getElementById("txtUsrId").focus();
			    return false;
			}
			if (CheckName(document.getElementById("txt_Password").value)== false)
			{
			   $("#dvPass").attr("style", "display:block");
			   $(document).ready(function () {
			       window.setTimeout(function () {
			           $(".alert").attr("style", "display:none");
			       }, 2000);
			   });
			    document.getElementById("txt_Password").focus();
			    return false;
			}
		}
				
    </script>

</head>
<body onload="LoginForm.txtUsrId.focus()" class="login_page  pace-done">
       
<div class="container-fluid">
    <div class="login-wrapper row">
        
        <div id="login" class="login loginpage col-lg-offset-4 col-md-offset-3 col-sm-offset-3 col-xs-offset-0 col-xs-12 col-sm-6 col-lg-4">
            <h1><a href="#" title="Login Page" tabindex="-1">Traffic Ticket CRM</a></h1>
            
            <form name="loginform" id="LoginForm" method="post" runat="server">
                <div class="col-xs-12">
            <div class="alert alert-danger fade in" id="dvUserID" style="display:none;">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            Please enter User ID.
        </div>
            <div class="alert alert-danger fade in" id="dvPassword" style="display:none;">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                Please enter Password.
            </div>
            <div class="alert alert-danger fade in" id="dvBranch" style="display:none;">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                Please select your branch location to proceed
            </div>
            <div class="alert alert-danger fade in" id="dvAlpha" style="display:none;">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                Username must contain alphabets and numbers
            </div>
               <div class="alert alert-danger fade in" id="dvPass" style="display:none;">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                Password must contain alphabets and numbers
            </div>
        </div>
                <p>
                    <label for="user_login">Username<br />
                        <asp:TextBox ID="txtUsrId" runat="server" CssClass="input" placeholder="User ID" MaxLength="20"></asp:TextBox>
                        <!--<input type="text" name="log" id="user_login" class="input" value="demo" size="20" />--></label>
                </p>
                <p>
                    <label for="user_pass">Password<br />
                        <asp:TextBox ID="txt_Password" runat="server" CssClass="input" placeholder="Password" MaxLength="20" TextMode="Password"></asp:TextBox>
                        <!--<input type="password" name="pwd" id="user_pass" class="input" value="demo" size="20" />--></label>
                </p>
                <p>
                    <label for="">
                        Branch<br />
                        <asp:DropDownList ID="ddl_Branch" runat="server" CssClass="input"></asp:DropDownList>
                    </label>
                </p>
                <p class="forgetmenot">
                    <asp:CheckBox ID="chk_SPassword" runat="server" Checked="True" CssClass="icheck-minimal-aero" Text="Save Password" />
                    <%--<label class="icheck-label form-label" for="rememberme"><!--<input name="rememberme" type="checkbox" id="rememberme" value="forever" class="icheck-minimal-aero" checked>--> Remember me</label>--%>
                </p>



                <p class="submit">
                    <asp:Button ID="btn_Login" runat="server" CssClass="btn btn-accent btn-block" Text="Login" OnClientClick="return ValidateLogin();" />
                    <%--<input type="submit" name="wp-submit" id="wp-submit" class="btn btn-accent btn-block" value="Sign In" />--%>
                </p>
                <p class="submit">
                    <asp:Label ID="lblMsg" runat="server" style="color: #ffffff;padding-left: 30%;"
                               Visible="False"></asp:Label>
                </p>
            </form>

            


        </div>
    </div>
</div>


          

<!-- MAIN CONTENT AREA ENDS -->
<!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->


<!-- CORE JS FRAMEWORK - START --> 
<script src="assets/js/jquery-1.11.2.min.js" type="text/javascript"></script> 
<script src="assets/js/jquery.easing.min.js" type="text/javascript"></script> 
<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
<script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>  
<script src="assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script> 
<script src="assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>  
<script>window.jQuery||document.write('<script src="assets/js/jquery-1.11.2.min.js"><\/script>');</script>
<!-- CORE JS FRAMEWORK - END --> 


<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 

<script src="assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


<!-- CORE TEMPLATE JS - START --> 
<script src="assets/js/scripts.js" type="text/javascript"></script> 
<!-- END CORE TEMPLATE JS - END --> 


<!-- General section box modal start -->
<div class="modal" id="section-settings" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog animated bounceInDown">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Section Settings</h4>
            </div>
            <div class="modal-body">

                Body goes here...

            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                <button class="btn btn-success" type="button">Save changes</button>
            </div>
        </div>
    </div>
</div>
<!-- modal end -->
</body>
</html>

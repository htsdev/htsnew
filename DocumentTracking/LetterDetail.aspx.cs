using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using HTP.Components;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;

namespace HTP.DocumentTracking
{
    public partial class LetterDetail : System.Web.UI.Page
    {
        #region Variables

        clsDocumentTracking clsDT = new clsDocumentTracking();    
        clsLogger BugTracker = new clsLogger();
        DataView DV;
        clsENationWebComponents clsDb = new clsENationWebComponents();
        clsSession cSession = new clsSession();
        
        #endregion 

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (cSession.IsValidSession(this.Request) == false)
            {
                Response.Redirect("../frmlogin.aspx", false);
            }
            else
            {
                if (!IsPostBack)
                {   
                    lbl_Message.Visible = false;
                    if (Request.QueryString["DBID"] != "" & Request.QueryString["DBID"] != null)
                    {
                        ViewState["DBID"] = Request.QueryString["DBID"].ToString();
                        ViewState["SBID"] = Request.QueryString["SBID"].ToString();
                        BindGrid();
                    } 
                    
                }
            }
        }

        protected void gv_Scan_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_LetterDetail.PageIndex = e.NewPageIndex;
                DV = (DataView)Session["DV"];
                gv_LetterDetail.DataSource = DV;
                gv_LetterDetail.DataBind();
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;
                lbl_Message.Visible = true;
            }
        }

        #endregion

        #region Methods

        private void BindGrid()
        {
            try
            {
                clsDT.DocumentBatchID = Convert.ToInt32(ViewState["DBID"]);
                clsDT.ScanBatchID = Convert.ToInt32(ViewState["SBID"]);
                DataSet ds_LetterDetail = clsDT.GetLetterDetail();

                if (ds_LetterDetail.Tables[0].Rows.Count > 0)
                {
                    gv_LetterDetail.DataSource = ds_LetterDetail;
                    DV = new DataView(ds_LetterDetail.Tables[0]);
                    Session["DV"] = DV;
                    gv_LetterDetail.DataBind();
                }
                else
                {
                    lbl_Message.Text = "No Record Found";
                }
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;
                lbl_Message.Visible = true;
            }

        }

        #endregion
    }
}

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text.RegularExpressions;
using FrameWorkEnation.Components;

namespace HTP.DocumentTracking
{
    public partial class TestOcr : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string ocr_data = String.Empty;
            int documentBatchID = 0;
            int pageCount = 0;
            int pageNo = 0;
            string temp = String.Empty;
            clsENationWebComponents clsDB = new clsENationWebComponents();

            DataSet ds = clsDB.Get_DS_BySP("usp_test_ocrdata");

            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Response.Write("\n\rScanBatchDetailID: " + ds.Tables[0].Rows[i]["ScanBatchDetailID"].ToString()+" ");
                    ocr_data = ds.Tables[0].Rows[i]["ocrdata"].ToString().Trim();
                    if (ocr_data.Length > 0)
                    {
                        char[] lit = { '\r', '\n' };
                        string[] data = ocr_data.Split(lit);
                        try
                        {
                            for (int j = data.Length - 1; j >= 0; j--)
                            {
                                temp = data[j].ToString().Trim();
                                temp = temp.Replace(" ", "");

                                Regex pat = new Regex(@"^\d*-\d*-\d*$");
                                Match m = pat.Match(data[j].ToString().Trim());
                                if (m.Success)
                                {
                                    temp = data[j].ToString().Trim();
                                    Response.Write(temp);
                                    documentBatchID = Convert.ToInt32(temp.Substring(0, temp.IndexOf("-")));
                                    Response.Write(documentBatchID.ToString());
                                    temp = temp.Substring(temp.IndexOf("-") + 1);
                                    pageNo = Convert.ToInt32(temp.Substring(0, temp.IndexOf("-")));
                                    Response.Write(pageNo.ToString());
                                    temp = temp.Substring(temp.IndexOf("-") + 1);
                                    pageCount = Convert.ToInt32(temp);
                                    Response.Write(pageCount.ToString());
                                    break;
                                }
                            }
                        }
                        catch
                        {
                            Response.Write("Error processing scan batch detail id:" + ds.Tables[0].Rows[i]["ScanBatchDetailID"].ToString());
                        }
                    }
                }
            }
        }
    }
}

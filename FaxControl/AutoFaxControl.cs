﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using FaxControl.Classes;
using System.Xml;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Collections;
using System.Drawing.Design;

//Zeeshan 4734 09/22/2008 Remove Warnings From The Code
namespace FaxControl
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:AutofaxControl runat=server></{0}:AutofaxControl>")]
    [Designer(typeof(FaxControlDesigner))]
    [ParseChildren(true)]
    [PersistChildren(false)]
    public class AutofaxControl : WebControl, INamingContainer
    {
        #region Controls Declaration
        protected Panel pnlMain = new Panel();
        protected Table tblMain = new Table();

        protected TableRow trHeader = new TableRow();
        protected TableRow trMessage = new TableRow();
        protected TableRow trTo = new TableRow();
        protected TableRow trSubject = new TableRow();
        protected TableRow trAttachment = new TableRow();
        protected TableRow trBody = new TableRow();
        protected TableRow trCommand = new TableRow();

        protected TableCell tdHeader = new TableCell();
        protected TableCell tdMessage = new TableCell();
        protected TableCell tdToLabel = new TableCell();
        protected TableCell tdToText = new TableCell();
        protected TableCell tdSubjectLabel = new TableCell();
        protected TableCell tdSubjectText = new TableCell();
        protected TableCell tdAttachmentLabel = new TableCell();
        protected TableCell tdAttachmentText = new TableCell();
        protected TableCell tdBodyText = new TableCell();
        protected TableCell tdCommandText = new TableCell();

        protected TextBox txtTo = new TextBox();
        protected TextBox txtSubject = new TextBox();
        protected TextBox txtAttachment = new TextBox();
        protected TextBox txtBody = new TextBox();

        protected Button btnSend = new Button();
        protected HiddenField Hf_closepopup = new HiddenField();
        protected HiddenField Hf_showalert = new HiddenField();


        protected Label lblMessage = new Label();

        protected System.Web.UI.HtmlControls.HtmlGenericControl divWait = new System.Web.UI.HtmlControls.HtmlGenericControl();
        protected System.Web.UI.WebControls.Image imgWait = new System.Web.UI.WebControls.Image();
        protected Label lblWaitMessage = new Label();




        #endregion

        #region Constructors
        public AutofaxControl()
        {
            _viewState = new StateBag();
            _viewState.Add("ticketId", _ticketId);
        }
        #endregion

        #region Variable Declaration
        string Empname = string.Empty;
        string Courtfax = string.Empty;
        string Empemail = string.Empty;
        string refno = string.Empty;
        int _ticketId = 0;

        const string status = "pending";
        #endregion

        #region Properties
        private StateBag _viewState;
        private bool _isTrackingViewState;

        private Settings.ProgramSource source = FaxControl.Settings.ProgramSource.Houston;

        [Bindable(true)]
        [Category("Data")]
        [DefaultValue(0)]
        [Localizable(true)]

        public int Empid
        {
            get
            {

                int empId = int.Parse(ViewState["empid"] != null ? ViewState["empid"].ToString() : "0");
                return empId;
            }
            set { ViewState["empid"] = value; }
        }


        [Bindable(true)]
        [Category("Data")]
        [DefaultValue(0)]
        [Localizable(true)]

        public int Ticketid
        {
            get
            {

                int ticketId = int.Parse(ViewState["ticketId"] != null ? ViewState["ticketId"].ToString() : "0");
                return ticketId;
            }
            set { ViewState["ticketId"] = value; }
        }

        [Bindable(true)]
        [Category("Data")]
        [DefaultValue("")]
        [Localizable(true)]

        public string Subject
        {
            get
            {
                string subject = ViewState["subject"] != null ? ViewState["subject"].ToString() : string.Empty;
                return subject;
            }
            set { ViewState["subject"] = value; }
        }

        [Bindable(true)]
        [Category("Data")]
        [DefaultValue("")]
        [Localizable(true)]


        public string Attachment
        {
            get
            {
                string attachment = ViewState["attachment"] != null ? ViewState["attachment"].ToString() : string.Empty;
                return attachment;
            }
            set { ViewState["attachment"] = value; }
        }

        [Bindable(true)]
        [Category("Data")]
        [DefaultValue("")]
        [Localizable(true)]

        public string ToAddress
        {
            get
            {
                string toAddress = ViewState["toAddress"] != null ? ViewState["toAddress"].ToString() : string.Empty;
                return toAddress;
            }
            set { ViewState["toAddress"] = value; }
        }

        [Bindable(true)]
        [Category("Data")]
        [DefaultValue("")]
        [Localizable(true)]

        public string Body
        {
            get
            {
                string body = ViewState["body"] != null ? ViewState["body"].ToString() : string.Empty;
                return body;
            }
            set { ViewState["body"] = value; }
        }

        [Bindable(true)]
        [Category("Data")]
        [DefaultValue("")]
        [Localizable(true)]

        public string SenderName
        {
            get
            {
                string senderName = ViewState["senderName"] != null ? ViewState["senderName"].ToString() : string.Empty;
                return senderName;
            }
            set { ViewState["senderName"] = value; }
        }

        [Bindable(true)]
        [Category("Data")]
        [DefaultValue("")]
        [Localizable(true)]

        public string SenderEmail
        {
            get
            {
                string senderEmail = ViewState["senderEmail"] != null ? ViewState["senderEmail"].ToString() : string.Empty;
                return senderEmail;
            }
            set { ViewState["senderEmail"] = value; }
        }



        [Bindable(true)]
        [Category("Behavior")]
        [DefaultValue("")]
        [Localizable(true)]

        public bool SetHiddenField
        {
            get
            {
                bool setHiddenField = ViewState["setHiddenField"] != null ? bool.Parse(ViewState["setHiddenField"].ToString()) : false;
                return setHiddenField;
            }
            set { ViewState["setHiddenField"] = value; }
        }

        [Bindable(true)]
        [Category("Behavior")]
        [DefaultValue("")]
        [Localizable(true)]

        public Settings.ProgramSource Source
        {
            get
            {
                Settings.ProgramSource source = (Settings.ProgramSource)(ViewState["source"] != null ? ViewState["source"] : Settings.ProgramSource.Houston);
                return source;
            }
            set { ViewState["source"] = value; }
        }


        [Category("Behavior")]
        [Localizable(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public HistoryNote PendingNote
        {
            get
            {
                HistoryNote tempoint = new HistoryNote();

                tempoint.FirstText = "Letter of Rep : Ref#";
                tempoint.SecondText = "- Sent to fax Number :";
                tempoint.ThirdText = "For CourtHouse [HCJP 4-1]- Waiting confirmation";

                HistoryNote note = ViewState["pendingNote"] != null ? (HistoryNote)ViewState["pendingNote"] : tempoint;
                return note;

            }
            set { ViewState["pendingNote"] = value; }
        }


        [Category("Behavior")]
        [Localizable(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public HistoryNote ConfirmNote
        {
            get
            {
                HistoryNote tempoint = new HistoryNote();

                tempoint.FirstText = "Letter of Rep : Ref#";
                tempoint.SecondText = "- Confirm to fax Number :";
                tempoint.ThirdText = "For CourtHouse [HCJP 4-1]- Waiting confirmation";

                HistoryNote note = ViewState["confirmNote"] != null ? (HistoryNote)ViewState["confirmNote"] : tempoint;
                return note;

            }
            set { ViewState["confirmNote"] = value; }
        }


        [Category("Behavior")]
        [Localizable(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public HistoryNote DeclineNote
        {
            get
            {
                HistoryNote tempoint = new HistoryNote();

                tempoint.FirstText = "Letter of Rep : Ref#";
                tempoint.SecondText = "- Decline from fax Number :";
                tempoint.ThirdText = "For CourtHouse [HCJP 4-1]- Waiting confirmation";

                HistoryNote note = ViewState["declineNote"] != null ? (HistoryNote)ViewState["declineNote"] : tempoint;
                return note;

            }
            set { ViewState["declineNote"] = value; }
        }

        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [Localizable(true)]

        public string WaitMessage
        {
            get
            {
                string waitMessage = ViewState["waitMessage"] != null ? ViewState["waitMessage"].ToString() : string.Empty;
                return waitMessage;
            }
            set { ViewState["waitMessage"] = value; }
        }


        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [Localizable(true)]
        [UrlProperty("*.gif")]
        [System.ComponentModel.Editor(typeof(System.Web.UI.Design.ImageUrlEditor), typeof(UITypeEditor))]
        public string WaitImage
        {
            get
            {
                string waitImage = ViewState["waitImage"] != null ? ViewState["waitImage"].ToString() : string.Empty;
                return waitImage;
            }
            set { ViewState["waitImage"] = value; }
        }


        #endregion

        #region Method
        protected override void OnInit(EventArgs e)
        {
            btnSend.Click += new EventHandler(btnSend_Click);
            base.OnInit(e);
        }
        protected override void CreateChildControls()
        {
            //if (!this.Page.IsPostBack) 
            //{
            this.Controls.Clear();
            const int txtboxWidth = 415;
            const int txtBodyWidth = 475;
            const int btnWidth = 100;
            const string btnClass = "clsbutton";
            const string txtClass = "clsInputadministration";
            const string lblClass = "clsLabel";



            // Setting For Error Message
            lblMessage.Text = "";
            lblMessage.ForeColor = Color.Red;
            tdMessage.Controls.Add(lblMessage);


            // Setting for TO Row
            tdToLabel.CssClass = lblClass;
            tdToLabel.Style.Add("align", "left");
            tdToLabel.Text = "To";
            txtTo.Width = txtboxWidth;
            txtTo.CssClass = txtClass;
            txtTo.ReadOnly = true;
            txtTo.Text = ToAddress + "@Faxmaker.com";
            tdToText.Controls.Add(txtTo);

            // Setting for Subject Row
            tdSubjectLabel.CssClass = lblClass;
            tdSubjectLabel.Style.Add("align", "left");
            tdSubjectLabel.Text = "Subject";
            txtSubject.Width = txtboxWidth;
            txtSubject.ReadOnly = true;
            txtSubject.CssClass = txtClass;
            txtSubject.Text = Subject;
            tdSubjectText.Controls.Add(txtSubject);


            // Setting for Attachment Row
            tdAttachmentLabel.CssClass = lblClass;
            tdAttachmentLabel.Style.Add("align", "left");
            tdAttachmentLabel.Text = "Attachment";
            txtAttachment.Width = txtboxWidth;
            txtAttachment.CssClass = txtClass;
            txtAttachment.ReadOnly = true;
            txtAttachment.Text = Path.GetFileName(Attachment); ;
            tdAttachmentText.Controls.Add(txtAttachment);


            // Setting for Body Row
            txtBody.TextMode = TextBoxMode.MultiLine;
            txtBody.Width = txtBodyWidth;
            txtBody.CssClass = txtClass;
            txtBody.Height = Unit.Pixel(100);
            txtBody.Text = Body;
            tdBodyText.Controls.Add(txtBody);

            // Setting for Command Row
            btnSend.Text = "Send";
            btnSend.Width = btnWidth;
            btnSend.CssClass = btnClass;
            btnSend.OnClientClick = "ShowDiv()";
            tdCommandText.Controls.Add(btnSend);
            Hf_closepopup.Value = "0";
            Hf_closepopup.ID = "hf_closepopup";
            Hf_showalert.Value = "0";
            Hf_showalert.ID = "hf_showalert";


            // Setting Please wait message 
            imgWait.ImageUrl = WaitImage;
            lblWaitMessage.Text = WaitMessage;
            lblWaitMessage.CssClass = "clssubhead";
            divWait.Controls.Add(imgWait);
            divWait.Controls.Add(lblWaitMessage);
            divWait.Style.Add("Display", "none");
            divWait.ID = "waitdiv";
            divWait.Style.Add("width", "100%");




            tdCommandText.Controls.Add(Hf_closepopup);
            tdCommandText.Controls.Add(Hf_showalert);
            tdCommandText.Controls.Add(divWait);



            tblMain.Controls.Add(trHeader);
            tblMain.Controls.Add(trMessage);
            tblMain.Controls.Add(trTo);
            tblMain.Controls.Add(trSubject);
            tblMain.Controls.Add(trAttachment);
            tblMain.Controls.Add(trBody);
            tblMain.Controls.Add(trCommand);


            tdHeader.ColumnSpan = 2;
            trHeader.Controls.Add(tdHeader);
            tdMessage.ColumnSpan = 2;
            trMessage.Controls.Add(tdMessage);

            trTo.Controls.Add(tdToLabel);
            trTo.Controls.Add(tdToText);

            trSubject.Controls.Add(tdSubjectLabel);
            trSubject.Controls.Add(tdSubjectText);

            trSubject.Controls.Add(tdSubjectLabel);
            trSubject.Controls.Add(tdSubjectText);

            trAttachment.Controls.Add(tdAttachmentLabel);
            trAttachment.Controls.Add(tdAttachmentText);


            tdBodyText.ColumnSpan = 2;
            trBody.Controls.Add(tdBodyText);

            tdCommandText.ColumnSpan = 2;
            trCommand.Controls.Add(tdCommandText);





            this.Page.ClientScript.RegisterStartupScript(typeof(string), "DivShow", "<script language='javascript'> function ShowDiv() { document.getElementById('" + this.ClientID + "_" + divWait.ClientID + "').style.display = 'block' } </script>");
            this.Page.ClientScript.RegisterStartupScript(typeof(string), "SuccessShow", "<script language='javascript'>function ShowSuccess() { alert('Fax has been sent successfully') }</script>");



            this.Controls.Add(tblMain);
            base.CreateChildControls();

        }

        void btnSend_Click(object sender, EventArgs e)
        {
            try
            {
                int ticketId = Ticketid;
                int empid = Empid;

                if (ticketId != 0)
                {
                    lblMessage.Visible = false;
                    if (empid == 0)
                    {
                        lblMessage.Text = "Employee Id is not provided";
                    }
                    else
                    {
                        //add fax data
                        FaxControl.Settings.AutofaxSettings config = (FaxControl.Settings.AutofaxSettings)System.Configuration.ConfigurationManager.GetSection("AutofaxSettings");
                        faxclass fclass = new faxclass();

                        if (Attachment.Contains("\\\\\\") == true)
                        {
                            Attachment = Attachment.Replace("\\\\", "\\");
                        }

                        string vpat = config.NTPATHfax;
                        string newfilepath = vpat + txtAttachment.Text;
                        string oldfilepath = Attachment;

                        if (System.IO.File.Exists(newfilepath) == false)
                        {
                            System.IO.File.Copy(oldfilepath, newfilepath);
                        }


                        int output = 0;
                        output = fclass.addfaxletter(ticketId, empid, SenderName, SenderEmail, ToAddress, Subject, txtBody.Text, status, DateTime.Now, ConfirmNote.ToString(), DeclineNote.ToString(), Source, PendingNote.ToString(), newfilepath);
                        string emailTo = string.Empty;
                        if (config.Testkey != "")
                        {
                            if (config.Faxto.Contains("@") == true)
                            {
                                emailTo = config.Testkey + config.Faxto;
                            }
                            else
                            {
                                emailTo = config.Testkey + "@" + config.Faxto;
                            }

                        }
                        else
                        {
                            if (config.Faxto.Contains("@") == true)
                            {
                                emailTo = ToAddress + config.Faxto;
                            }
                            else
                            {
                                emailTo = ToAddress + "@" + config.Faxto;
                            }
                        }

                        LNHelper.MailClass.SendMail(SenderEmail, emailTo, Subject + " ref # " + output, txtBody.Text, "", "", oldfilepath);

                        if (SetHiddenField == true)
                        {
                            Hf_closepopup.Value = "1";
                        }
                        else
                        {
                            Hf_closepopup.Value = "0";
                        }

                        Hf_showalert.Value = "1";




                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }
        #endregion

        #region Viewstate Maintainance

        protected StateBag ViewState
        {
            get
            {

                if (_viewState == null)
                {

                    _viewState = new StateBag(false);

                    if (_isTrackingViewState)
                    {

                        ((IStateManager)_viewState).TrackViewState();

                    }

                }

                return _viewState;

            }
        }

        protected override object SaveViewState()
        {

            object[] allStates = new object[15];

            allStates[0] = Ticketid;
            allStates[1] = Empid;
            allStates[2] = Subject;
            allStates[3] = Attachment;
            allStates[4] = ToAddress;
            allStates[5] = Body;
            allStates[6] = SenderName;
            allStates[7] = SenderEmail;
            allStates[8] = Source;
            allStates[9] = PendingNote;
            allStates[10] = ConfirmNote;
            allStates[11] = DeclineNote;
            allStates[12] = WaitMessage;
            allStates[13] = WaitImage;
            allStates[14] = SetHiddenField;



            return allStates;

        }



        protected override void LoadViewState(object savedState)
        {

            if (savedState != null)
            {
                // Load State from the array of objects that was saved at ;
                // SavedViewState.
                object[] myState = (object[])savedState;
                if (myState[0] != null)
                {
                    //base.LoadViewState(myState[0]);
                    this.Ticketid = int.Parse(myState[0].ToString());
                }
                if (myState[1] != null)
                {
                    this.Empid = int.Parse(myState[1].ToString());
                }
                if (myState[2] != null)
                {
                    this.Subject = myState[2].ToString();
                    this.txtSubject.Text = this.Subject;
                }
                if (myState[3] != null)
                {
                    this.Attachment = myState[3].ToString();
                    this.txtAttachment.Text = this.Attachment;
                }
                if (myState[4] != null)
                {
                    this.ToAddress = myState[4].ToString();
                    this.txtTo.Text = this.ToAddress;
                }
                if (myState[5] != null)
                {
                    this.Body = myState[5].ToString();
                    this.txtBody.Text = this.Body;
                }

                if (myState[6] != null)
                {
                    this.SenderName = myState[6].ToString();
                }

                if (myState[7] != null)
                {
                    this.SenderEmail = myState[7].ToString();
                }

                if (myState[8] != null)
                {
                    this.Source = (Settings.ProgramSource)myState[8];
                }
                if (myState[9] != null)
                {
                    this.PendingNote = (HistoryNote)myState[9];
                }

                if (myState[10] != null)
                {
                    this.ConfirmNote = (HistoryNote)myState[10];
                }

                if (myState[11] != null)
                {
                    this.DeclineNote = (HistoryNote)myState[11];
                }
                if (myState[12] != null)
                {
                    this.WaitMessage = myState[12].ToString();
                }
                if (myState[13] != null)
                {
                    this.WaitImage = myState[13].ToString();
                }
                if (myState[14] != null)
                {
                    this.SetHiddenField = bool.Parse(myState[14].ToString());
                }
            }

            //base.LoadViewState(savedState);

        }
        protected override void TrackViewState()
        {
            base.TrackViewState();

            if (_viewState != null)
                ((IStateManager)_viewState).TrackViewState();
        }
        #endregion

    }
}


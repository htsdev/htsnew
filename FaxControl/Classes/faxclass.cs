﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using LNHelper;
using System.Xml;

namespace FaxControl.Classes
{
    // Noufil 3999 05/20/2008 Class for fax report
    public class faxclass
    {
        //Zeeshan 4734 09/22/2008 Modify Code To Remove Warnings
        DBComponent clsdb;

        public faxclass() 
        {
            FaxControl.Settings.AutofaxSettings config = (FaxControl.Settings.AutofaxSettings)System.Configuration.ConfigurationManager.GetSection("AutofaxSettings");
            clsdb = new DBComponent(config.Connection);
        }

        /// <summary>
        /// This function add new entry in the faxletter table
        /// </summary>
        /// <param name="ticketid"></param>
        /// <param name="empid"></param>
        /// <param name="ename"></param>
        /// <param name="eemail"></param>
        /// <param name="faxno"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <param name="status"></param>
        /// <param name="faxdate"></param>
        /// <param name="docpath"></param>
        /// <returns></returns>

        public int addfaxletter(int ticketid, int empid, string ename, string eemail, string faxno, string subject, string body, string status, DateTime faxdate,string confirmNote , string declineNote, Settings.ProgramSource src, string subjectNote,string Docpath)
        {
            
            // Noufil 3999 05/20/2008 Add fax entry into database

            string[] keys = { "@ticketid", "@empid", "@ename", "@eemail", "@faxno", "@subject", "@body", "@status", "@faxdate", "@ConfirmNote", "@DeclineNote", "@SystemId", "@SubjectNote", "@Docpath", "@output" };
            object[] values = { ticketid, empid, ename, eemail, faxno, subject, body, status, faxdate, confirmNote, declineNote, (int)src, subjectNote, Docpath, 0 };
            //clsdb.ExecuteSP("Usp_Htp_insertintofaxletter", keys, values);
            object[] obj = clsdb.InsertBySPArrRet("Usp_Htp_insertintofaxletter", keys, values, 1);
            int outp = int.Parse(obj[0].ToString());
            return outp;
        }

        /// <summary>
        /// This fuction returns number of rows thats exist in the table faxletter
        /// </summary>
        /// <returns>count</returns>
        public string getfaxcount(int ticketid)
        {
            // Noufil 3999 05/20/2008 get the maximum id for the fax
            string[] keys = { "@ticketid" };
            object[] values = { ticketid };
            //string faxcountt = 
            object result = clsdb.ExecuteScalerBySP("USP_HTP_faxcount", keys, values);
            return result.ToString();
            //faxcountt;
        }

        /// <summary>
        /// This function returns records for fax report
        /// </summary>
        /// <param name="status"></param>
        /// <param name="fromdate"></param>
        /// <param name="todate"></param>
        /// <returns>DataTable with or without records</returns>
        public DataTable GetFaxReport(Settings.ProgramSource src,int status, DateTime fromdate, DateTime todate)
        {
            // Noufil 3999 05/20/2008 Return fax report 
            string[] keys = { "@SystemId","@statusid", "@fromdate", "@todate" };
            object[] values = { (int) src,status, fromdate.ToShortDateString(), todate.ToShortDateString() };
            DataTable dt = clsdb.GetDTBySPArr("Usp_Htp_Faxreport", keys, values);
            return dt;
        }
        /// <summary>
        /// This function returns records for Jp Appearance Report
        /// </summary>
        /// <returns>DataTable with or without records</returns>
        public DataTable GetJpAppearancereport()
        {
            // Noufil 3999 05/20/2008 Return JP Appearance Fax Report
            DataTable dt = clsdb.GetDTBySPArr("USP_HTP_JpAppFaxreport");
            return dt;
        }

        public bool AddNote(int EmpID, string Subject, string Note, int TicketID,Settings.ProgramSource src)
        {
            string[] key = { "@EmpID", "@Subject", "@Note", "@TicketID" };

            DBComponent clsdb1; 

            switch (src)
            {
                case  FaxControl.Settings.ProgramSource.Houston:
                    clsdb1 = new DBComponent("TrafficTickets");
                    break;
                case  FaxControl.Settings.ProgramSource.Dallas:
                    clsdb1 = new DBComponent("DallasTrafficTickets");
                    break;
                default:
                    clsdb1 = new DBComponent("TrafficTickets");
                    break;
            }

            
            object[] value1 = { EmpID, Subject, Note, TicketID };
            try
            {
                clsdb1.ExecuteSP("usp_HTS_Insert_CaseHistoryInfo", key, value1);
                return true;
            }
            catch (Exception ex)
            {
                
                return false;
            }
        }

    }
}

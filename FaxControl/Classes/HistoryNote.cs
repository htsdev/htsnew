﻿using System.ComponentModel;
using System.ComponentModel.Design.Serialization;
using System;
using System.Reflection;
using System.Web.UI;


namespace FaxControl
{
    public interface IStateManagedItem : IStateManager
    {
        void SetDirty();
    }


    public abstract class StateManagedItem : IStateManagedItem
    {
        #region Fields

        private bool trackingViewState = false;
        private StateBag viewState;

        #endregion

        #region Constructor

        public StateManagedItem()
        {
            viewState = new StateBag();
        }

        #endregion

        #region Properties

        protected StateBag ViewState
        {
            get { return this.viewState; }
        }

        #endregion

        #region IStateManager Members

        [Browsable(false)]
        public bool IsTrackingViewState
        {
            get { return trackingViewState; }
        }

        public void LoadViewState(object state)
        {
            ((IStateManager)this.ViewState).LoadViewState(state);
        }

        public object SaveViewState()
        {
            return ((IStateManager)this.ViewState).SaveViewState();
        }

        public void TrackViewState()
        {
            this.trackingViewState = true;
            ((IStateManager)this.ViewState).TrackViewState();
        }

        #endregion

        #region IStateManager Members

        [Browsable(false)]
        bool IStateManager.IsTrackingViewState
        {
            get { return this.IsTrackingViewState; }
        }

        void IStateManager.LoadViewState(object state)
        {
            this.LoadViewState(state);
        }

        object IStateManager.SaveViewState()
        {
            return this.SaveViewState();
        }

        void IStateManager.TrackViewState()
        {
            this.TrackViewState();
        }

        #endregion

        #region Methods

        public void SetDirty()
        {
            viewState.SetDirty(true);
        }

        #endregion
        
    }

    [TypeConverter(typeof(HistoryNoteConverter))]
    public class HistoryNote : StateManagedItem
    {
        public string FirstText { 
        get 
        {
            object o = ViewState["_firstText"];
            return o == null ? null : (string)o;
        } 
        set { 
                ViewState["_firstText"] = value; 
            } 
        }


        public string SecondText
        {
            get
            {
                object o = ViewState["_secondText"];
                return o == null ? null : (string)o;
            }
            set
            {
                ViewState["_secondText"] = value;
            }
        }


        public string ThirdText
        {
            get
            {
                object o = ViewState["_thirdText"];
                return o == null ? null : (string)o;
            }
            set
            {
                ViewState["_thirdText"] = value;
            }
        }

        public override string ToString()
        {
            return FirstText + " {Key} " + SecondText + " {Number}" + ThirdText;
        }

    }


    public class HistoryNoteConverter : TypeConverter
    {

        public HistoryNoteConverter()
        {

        }

        

        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourcetype)
        {
            if (sourcetype == typeof(string))
                return true;
            else
                return base.CanConvertFrom(context, sourcetype);
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationtype)
        {
            if (destinationtype == typeof(InstanceDescriptor) || destinationtype == typeof(string))
                return true;
            else
                return base.CanConvertTo(context, destinationtype);
        }

        public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object src, Type destinationtype)
        {

            //convert to a string
            if (destinationtype == typeof(InstanceDescriptor))
            {
                ConstructorInfo ci = typeof(HistoryNote).GetConstructor(new Type[] { });
                if (ci != null)
                {
                    return new InstanceDescriptor(ci, null, false);
                }

            }
            else if (destinationtype == typeof(string))
            {
                HistoryNote p3 = (HistoryNote)src;
                string result = p3.FirstText + " {key} " + p3.SecondText + " {number} " + p3.ThirdText;
                return result;
            }
            return base.ConvertTo(context, culture, src, destinationtype);
        }
        public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object src)
        {
            {
                //convert from a string
                if (src is string)
                {
                    HistoryNote p3 = new HistoryNote();
                    string[] tempstr = src.ToString().Split('}');
                    p3.FirstText = tempstr[0].Replace("{key", "").Trim();
                    p3.SecondText= tempstr[1].Replace("{number", "").Trim();
                    p3.ThirdText= tempstr[2];
                    return p3;
                }
                return base.ConvertFrom(context, culture, src);
            }
        }


        public override bool GetPropertiesSupported(ITypeDescriptorContext context)
        {
            return true;
        }

        public override PropertyDescriptorCollection GetProperties(ITypeDescriptorContext context, object src, Attribute[] attributes)
        {
            return TypeDescriptor.GetProperties(src, attributes);
        }
    }

    
    //public struct HistoryNote
    //{
    //    private string _firstText;
    //    private string _secondText;
    //    private string _thirdText;
      
    //    public string FirstText { get { return _firstText; } set { _firstText = value; } }
      
    //    public string SecondText { get { return _secondText; } set { _secondText = value; } }
      
    //    public string ThirdText { get { return _thirdText; } set { _thirdText = value; } }

    //    public override string ToString()
    //    {
    //        string result = _firstText + " {Key} " + _secondText + " {Number} " + _thirdText;
    //        return result;
    //        //return base.ToString();
    //    }
    //}
}
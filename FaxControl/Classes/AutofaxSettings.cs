﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using LNHelper;


namespace FaxControl.Settings
{
    public enum ProgramSource
    {
        Houston = 1,
        Dallas = 2
    }
    public class AutofaxSettings : ConfigurationSection
    {
        //Zeeshan 4734 09/22/2008 Modify Code To Remove Warnings
        DBComponent clsDb = new DBComponent();

        public AutofaxSettings()
        {

        }
        /// <summary>
        /// It is getting value of "Fax Path" from web.config which is assign there and seting its value here
        /// </summary>
        [ConfigurationProperty("faxPath", DefaultValue = "", IsRequired = true)]
        public string NTPATHfax
        {
            get { return (string)this["faxPath"]; }
            set { this["faxPath"] = value; }
        }

        /// <summary>
        /// It is getting value of "Fax To" from web.config which is assign there and seting its value here
        /// </summary>
        [ConfigurationProperty("faxto", DefaultValue = "", IsRequired = true)]
        public string Faxto
        {
            get { return (string)this["faxto"]; }
            set { this["faxto"] = value; }
        }

        /// <summary>
        /// It is getting value of "testKey" from web.config which is assign there and seting its value here
        /// </summary>
        [ConfigurationProperty("testkey", DefaultValue = "", IsRequired = false)]
        public string Testkey
        {
            get { return (string)this["testkey"]; }
            set { this["testkey"] = value; }
        }

        /// <summary>
        /// It is getting value of "Database" from web.config which is assign there and seting its value here
        /// </summary>
        [ConfigurationProperty("database", IsRequired = false)]
        public string Connection
        {
            get { return (string)this["database"]; }
            set { this["database"] = value; }
        }
    }
}

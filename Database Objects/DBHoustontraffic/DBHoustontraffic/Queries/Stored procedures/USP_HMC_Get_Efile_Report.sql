SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HMC_Get_Efile_Report]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HMC_Get_Efile_Report]
GO



CREATE procedure [dbo].[USP_HMC_Get_Efile_Report]    
    
@Empid int ,   
@RecordIDs varchar(200)  
  
  
as     
  
declare @Records  table(ID int)          
insert into @Records select * from dbo.sap_string_param(@RecordIDs)      
    
SELECT     dbo.tbl_HMC_Users.Firm, dbo.tbl_HMC_Users.Fname, dbo.tbl_HMC_Users.Lname, ef.Empid, ta.FirstName, ta.LastName, ta.MidNumber, ta.DOB,     
                      f.FirmName, ef.ID, tva.CourtDate, tva.Courtnumber, cvs.ShortDescription, ef.EFileName, ef.EFileDate, ef.Flag, tva.CauseNumber,     
                      tva.TicketNumber_PK,  tva.ViolationNumber_PK,  tva.RecordID    
FROM         dbo.tblCourtViolationStatus AS cvs INNER JOIN    
                      dbo.tblTicketsViolationsArchive AS tva INNER JOIN    
                      dbo.tblTicketsArchive AS ta ON tva.RecordID = ta.RecordID INNER JOIN    
                      dbo.tbl_HMC_EFile AS ef ON tva.TicketNumber_PK = ef.TicketNumber_Fk AND tva.ViolationNumber_PK = ef.ViolationNumber_FK ON     
                      cvs.CourtViolationStatusID = tva.violationstatusid LEFT OUTER JOIN    
                      dbo.tblFirm AS f INNER JOIN    
                      dbo.tbl_HMC_Users ON f.FirmID = dbo.tbl_HMC_Users.Firm ON ef.Empid = dbo.tbl_HMC_Users.s_no    
WHERE    /* (ef.Flag = 1) AND*/ (ef.Empid = @Empid)    
AND    ef.recordid in ( select id from @Records)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


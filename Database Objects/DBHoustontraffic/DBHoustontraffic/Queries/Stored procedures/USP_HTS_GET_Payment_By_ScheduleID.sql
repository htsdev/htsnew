SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_GET_Payment_By_ScheduleID]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_GET_Payment_By_ScheduleID]
GO


         CREATE procedure [dbo].[USP_HTS_GET_Payment_By_ScheduleID]    
    
@Scheduleid as int      
    
as    
    
select convert (varchar,amount)as amount,paymentdate     
from   tblschedulepayment    
where scheduleid=@Scheduleid  
  
  
  
  
  
  
  
        

----------------------------------------------------------------------------------------------      

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


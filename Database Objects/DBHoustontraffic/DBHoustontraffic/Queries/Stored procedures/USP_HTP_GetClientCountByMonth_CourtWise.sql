USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTP_GetClientCountByMonth_CourtWise]    Script Date: 08/26/2010 06:03:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Created By: Muhammad Adil Aleem
Created date : Jul-19th-2010
Business Logic: To get number of cases hired per month for specific court.
Parameters: 
	@CourtID: Specific court ID.
*/
Alter PROCEDURE [dbo].[USP_HTP_GetClientCountByMonth_CourtWise]
@CourtID INT
AS
select upper(dbo.fn_DateFormat(ttp.RecDate, 32, '-', '', 0)) as [Hired Month],
COUNT(DISTINCT tt.TicketID_PK) as [Client Count]
FROM tblTicketsViolations ttv
INNER JOIN tblTickets tt
ON tt.TicketID_PK = ttv.TicketID_PK
INNER JOIN tblTicketsPayment ttp
ON tt.TicketID_PK = ttp.TicketID
WHERE ttp.PaymentVoid = 0 
AND ttp.PaymentType NOT IN (8, 9, 11, 99, 100, 103)
AND ttv.CourtID = @CourtID
AND ttp.RecDate = (SELECT TOP 1 ttp2.RecDate FROM tblTicketsPayment ttp2 WHERE ttp2.TicketID = tt.TicketID_PK ORDER BY ttp2.RecDate DESC)
AND ttv.CourtViolationStatusIDmain not in (80, 105)
GROUP BY dbo.fn_DateFormat(ttp.RecDate, 32, '-', '', 0)
order by year('1/' + dbo.fn_DateFormat(ttp.RecDate, 32, '-', '', 0)), month('1/' + dbo.fn_DateFormat(ttp.RecDate, 32, '-', '', 0))

GO

GRANT EXECUTE ON dbo.USP_HTP_GetClientCountByMonth_CourtWise TO dbr_webuser
GO
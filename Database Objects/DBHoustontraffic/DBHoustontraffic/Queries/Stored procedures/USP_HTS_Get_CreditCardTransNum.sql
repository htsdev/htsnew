SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Get_CreditCardTransNum]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Get_CreditCardTransNum]
GO

        
--  USP_HTS_Get_CreditCardTransNum  81692  
  
  
create procedure [dbo].[USP_HTS_Get_CreditCardTransNum]   
 (  
 @InvoiceNumber int  
 )  
  
as  
  
declare @transnum varchar(20)  
set @transnum =  ''  
  
select @transnum =  isnull(transnum,'')  from tblticketscybercash  
where invnumber = @invoicenumber  
  
select @transnum  
  
  
----------------------------------------------------------------------------------------------      

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


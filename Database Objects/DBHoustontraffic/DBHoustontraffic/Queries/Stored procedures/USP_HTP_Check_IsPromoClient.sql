USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTP_Check_IsPromoClient]    Script Date: 01/02/2014 02:18:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** 
-- =============================================
-- Author:        Sabir Khan
-- Task ID:       11460
-- Create date: 09/24/2013
-- Description:   This procedure is used to check for promo price
-- =============================================
*******/
--[dbo].[USP_HTP_Check_IsPromoClient] 500342
ALTER PROCEDURE [dbo].[USP_HTP_Check_IsPromoClient]
@TicketID INT
AS
BEGIN 
SET NOCOUNT ON;
IF EXISTS(
              SELECT tn.recordid 
              FROM tbltickets t 
              INNER JOIN tblticketsviolations tv 
              ON tv.TicketID_PK = t.TicketID_PK 
              INNER JOIN tblLetterNotes tn 
              ON tn.RecordID = tv.RecordID 
              AND ISNULL(tn.isPromo,0) = 1
              WHERE t.TicketID_PK = @TicketID
              AND tn.LetterType IN (9,12)
            )
BEGIN
      SELECT top 1 CONVERT(BIT,1) AS IsPromo, 
              CASE ISNULL(tn.PromoID,0) WHEN 0 THEN 'SPECIAL PROMO' ELSE a.PromoMessageForHTP END AS PromoMessageForHTP
              FROM tbltickets t 
              INNER JOIN tblticketsviolations tv 
              ON tv.TicketID_PK = t.TicketID_PK 
              INNER JOIN tblLetterNotes tn 
              ON tn.RecordID = tv.RecordID
			  LEft outer join  AttorneyPromoTemplates a on a.PromoID = isnull(tn.PromoID,0)              
              
      WHERE ISNULL(tn.isPromo,0) = 1
      AND t.TicketID_PK = @TicketID
      AND tn.LetterType IN (9,12)
	  order by tn.RecordLoadDate desc
 
            
      
END
ELSE
BEGIN
      SELECT CONVERT(BIT,0) AS IsPromo, '' AS PromoMessageForHTP
END
END


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Usp_Hts_UpdateNoTrialLetterForJury]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Usp_Hts_UpdateNoTrialLetterForJury]
GO



CREATE procedure Usp_Hts_UpdateNoTrialLetterForJury      
      
@TicketID  int   
    
      
as      
      
      
update tbltickets set jurystatus=4 where ticketid_pk=@TicketID  --4 value is use when first time payment and status is in jury or pretrial  
       
          



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


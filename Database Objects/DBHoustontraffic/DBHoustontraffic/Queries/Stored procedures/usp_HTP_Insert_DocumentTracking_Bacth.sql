﻿/******   
Create by:  Syed Muhammad Ozair  
  
Business Logic : this procedure is used to Insert the information in tbl_htp_documenttracking_batch table and   
				 gets the inserted id.  
  
List of Parameters:   
 @BatchDate : Batch date for this document/report  
 @DocumentID_FK : docuemnt/Report id from tbl_HTP_DocumentTracking_Documents   
 @EmployeeID : employee id of current logged in user  
  
List of Columns:    
 DocumentBatchID : currently inserted ID, this field will be returned to the calling method.   
   
******/  
  
CREATE Procedure dbo.usp_HTP_Insert_DocumentTracking_Bacth
  
@BatchDate datetime,  
@DocumentID_FK int,  
@EmployeeID int  
  
as  
  
Declare @BID int  
  
set @BID = 0  
  
Insert into tbl_htp_documenttracking_batch (BatchDate, DocumentID_FK, EmployeeID)  
Values (@BatchDate, @DocumentID_FK, @EmployeeID)  
  
Select @BID = Scope_Identity()  
  
select @BID as DocumentBatchID  

go

grant exec on dbo.usp_HTP_Insert_DocumentTracking_Bacth to dbr_webuser
go
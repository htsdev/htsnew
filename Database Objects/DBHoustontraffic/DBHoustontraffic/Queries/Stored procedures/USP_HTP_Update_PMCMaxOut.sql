USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTP_Update_PMCMaxOut]    Script Date: 01/21/2011 16:24:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*
-- =============================================
Muhammad Muneer 8227 09/30/2010 Added this procedure to support the new functionality for LOR
Business Logic: This stored procedure is used to get total number of cases set for the selected date and having status in pre trail

Perameter: @crtDate: Selected Court Date

Return Columns: totalcases
-- =============================================
*/
--  USP_HTP_Update_PMCMaxOut 235613
ALTER PROCEDURE [dbo].[USP_HTP_Update_PMCMaxOut]
	@ticketId INT,
	@faxId int
AS
BEGIN
    
	DECLARE @EmployeeID   INT

	SET @EmployeeID=3992	
			
	--Check for Pasadena Waiting cases which is not bond....
	IF EXISTS (SELECT * FROM tblticketsviolations WHERE CourtViolationStatusIDmain = 104 AND UnderlyingBondFlag = 0  AND TicketID_PK = @ticketId)
	BEGIN  
	
	 DECLARE @selectedCourtDate DATETIME
	 --Sabir Khan 9917 11/18/2011 Faxmaker DB has been removed.
	 select @selectedCourtDate = PMCRepDate from FaxLetter where FaxletterID = @faxId and TicketID = @ticketId
	 DECLARE @temp TABLE (RowId INT IDENTITY(1, 1), TicketsViolationID INT)                                
	
	 INSERT INTO @temp
	  (
	    TicketsViolationID
	  )
	 SELECT TicketsViolationID  FROM tblTicketsViolations WHERE CourtViolationStatusIDmain = 104 AND UnderlyingBondFlag = 0  AND TicketID_PK = @ticketId and Courtid=3004
	 DECLARE @idx INT,@violid INT
	 DECLARE @RecCount INT
	 SELECT @idx = 1,@RecCount = COUNT(TicketsViolationID)
	 FROM   @temp 
	 WHILE @idx <= @RecCount
	 BEGIN  
	 SELECT @violid = TicketsViolationID
	 FROM   @temp
	 WHERE  rowid = @idx 
	 --Update into Pre Trail and Court date for Pasadena Pre Trial Cases...  	
	 UPDATE tblTicketsViolations SET    CourtDate = @selectedCourtDate,CourtDateMain = @selectedCourtDate,CourtDateScan = @selectedCourtDate,courtviolationstatusidmain = 101, CourtViolationStatusID = 101,CourtViolationStatusIDScan  = 101,VEmployeeID = @EmployeeID
	 WHERE TicketsViolationID = @violid and Ticketid_pk = @ticketId and Courtid = 3004
	 
	 SET @idx = @idx + 1
	 END
	 --Send Trail Notification Letter into Batch Print...
	 DECLARE @currDate DATETIME
	 SET @currDate = GETDATE() 
	 EXEC USP_HTS_Insert_BatchPrintLetter @ticketId,@currDate,'1/1/1900',0,2,@EmployeeID,''
	END
END   


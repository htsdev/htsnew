USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[usp_mailer_Send_Jersey_Appearance]    Script Date: 11/14/2011 23:35:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/****** 
Created by:		Sabir Khan
Business Logic:	The procedure is used by LMS application to get data for Jersey Village Appearance letter
				and to create LMS history for this letter for the selected date range.
				
List of Parameters:
	@catnum:		Category number of the selected letter type, if printing for 
					all courts of the selected court category
	@LetterType:	Selected letter type in LMS
	@CrtId:			Court id of the selected court category if printing for individual court hosue.
	@startListdate:	Comma separated dates selected to print letter.
	@PrintType:		Flag to just preview the letter or to mark letters as sent. If 1 then LMS letter history will be created.
	@SearchType:	Flag that identifies if searching by court date or by list date. 0= list date, 1 = court date
	@isPrinted:		Flag to include already printed letters in the batch.

List of Columns:	
	LetterId:		Letter ID generated for each letter. It will be "NOT PRINTABLE"	if @PrintType = 0
	RecordId:		Identity value to look up the case in non-clients. used to group the records in report file.
	FirstName:		Person's first name that will appear on the letter
	LastName:		Person's last name that will appear on the letter
	Address:		Person's Home address that will appear on the letter
	City:			Person's Home City that will appear on the letter
	State:			Person's Home State that will appear on the letter
	FineAmount:		Fine amount for each violation that will appear on the letter
	ViolationDescritpion: violation description that will appear on the letter
	Court Name:		Name of court with which the violation is associated. Used in report file
	DPC:			Address status that is used in barcode font on the letter
	DPTwo:			Address status that is used in barcode font on the letter
	TicketNumber_pk:Case number for the violation
	ZipCode:		Person's Home Zip code.
	Abb:			Short abbreviation of employee who is printing the letter


******/

-- exec usp_mailer_Send_Jersey_Appearance 33, 126, 33, '10/26/2011,' , 3991,  0 ,0   , 0
ALTER procedure [dbo].[usp_mailer_Send_Jersey_Appearance]
@catnum int=1,                                    
@LetterType int=126,                                    
@crtid int=1, 
@startListdate varchar (500) ,                                    
@empid int=2006,                              
@printtype int =0 ,
@searchtype int = 0 ,
@isprinted bit                            
                                    
                                    
as

-- DECLARING LOCAL VARIABLES FOR THE FILTERS....
declare @officernum varchar(50),                                    
@officeropr varchar(50),                                    
@tikcetnumberopr varchar(50),                                    
@ticket_no varchar(50),                                                                    
@zipcode varchar(50),                                             
@zipcodeopr varchar(50),                                    
                                    
@fineamount money,                                            
@fineamountOpr varchar(50) ,                                            
@fineamountRelop varchar(50),                   
              
@singleviolation money,                                            
@singlevoilationOpr varchar(50) ,                                            
@singleviolationrelop varchar(50),                                    
                                    
@doubleviolation money,                                            
@doublevoilationOpr varchar(50) ,                                            
@doubleviolationrelop varchar(50),                                  
@count_Letters int,                  
@violadesc varchar(500),                  
@violaop varchar(50)  


declare @tempDate datetime, @mailbarry bit
select @tempDate = getdate(), @mailbarry = 1
if @tempdate >= '8/11/08' and @Tempdate <= '8/24/08'
	set @mailbarry = 0 
else set @mailbarry = 1     
                                   
                                    
declare @sqlquery varchar(max), @sqlquery2 varchar(max), @lettername varchar(50) -- Rab Nawaz 9954 12/24/2011 Jersey Village Mailer added in the Global Template mailers of HMC
SET @lettername = 'APPEARANCE'
                                            
-- ASSIGNING VALUES TO THE VARIABLES 
-- BY GETTTING INFORMATION FROM LMS FILTER STRUCTURE...
-- FOR THIS LETTER TYPE....
Select @officernum=officernum,                                    
@officeropr=oficernumOpr,                                    
@tikcetnumberopr=tikcetnumberopr,                                    
@ticket_no=ticket_no,                                                                      
@zipcode=zipcode,                                    
@zipcodeopr=zipcodeLikeopr,                                    
@singleviolation =singleviolation,                                    
@singlevoilationOpr =singlevoilationOpr,                                    
@singleviolationrelop =singleviolationrelop,                                    
@doubleviolation = doubleviolation,                                    
@doublevoilationOpr=doubleviolationOpr,                                            
@doubleviolationrelop=doubleviolationrelop,                  
@violadesc=violationdescription,                  
@violaop=violationdescriptionOpr ,              
@fineamount=fineamount ,                                            
@fineamountOpr=fineamountOpr ,                                            
@fineamountRelop=fineamountRelop              
                                 
from tblmailer_letters_to_sendfilters                                    
where courtcategorynum=@catnum                                    
and Lettertype=@LetterType                                  

-- INITIALIZING THE VARIABLE FOR DYNAMIC SQL...
Select @sqlquery =''  , @sqlquery2 = ''
  
-- GETTING THE SHORT ABBREVIATION OF THE CURRENTLY LOGGED IN 
-- EMPLOYEE IN THE LOCAL VARIABLE....
declare @user varchar(10)
select @user = upper(abbreviation) from tblusers where employeeid = @empid
                                  
-- CREATING DYNAMIC SQL FOR THE MAIN QUERY...
set @sqlquery=@sqlquery+'                                    
Select	distinct ta.recordid, 
	convert(varchar(10),ta.listdate,101) as listdate, 
	count(tva.ViolationNumber_PK) as ViolCount,
	isnull(sum(isnull(tva.fineamount,0)),0) as Total_Fineamount 
into #temptable                                          
FROM    dbo.tblTicketsViolationsArchive tva 
INNER JOIN
	dbo.tblTicketsArchive ta 
ON 	tva.RecordID = ta.RecordID 
INNER JOIN
	dbo.tblCourtViolationStatus tcvs 
ON 	tva.violationstatusid = tcvs.CourtViolationStatusID 
-- GET ONLY ARRAIGNMENT (PRIMARY STATUS) CASES....
where  tcvs.CategoryID=2   

-- ONLY VERIFIED AND VALID ADDRESSES
and 	Flag1 in (''Y'',''D'',''S'')

and LEN(ISNULL(RTRIM(LTRIM(ta.lastname)),''''))<>0
and LEN(ISNULL(RTRIM(LTRIM(ta.firstname)),''''))<>0 

-- NOT MARKED AS STOP MAILING...
and 	donotmailflag = 0 

-- SHOULD BE NON-CLIENT
and 	ta.clientflag=0

-- PERSONS ADDRESS IS NOT CHANGED IN PAST 48 MONTHS (ACCUZIP)...
and isnull(ta.ncoa48flag,0) = 0
and datediff(day, tva.courtdate, getdate()) < 0
and len(isnull(tva.attorneyname, '''')+isnull(tva.barcardnumber,'''')) = 0 
and CHARINDEX(''KUBOSH'',isnull(tva.BondingCompanyName,''''))=0

'               
          
--Address status that is used in barcode font on the letter
if(@crtid=33 )              
	set @sqlquery=@sqlquery+' 
and 	tva.courtlocation In (Select courtid from tblcourts where courtcategorynum =  '+ convert(varchar(10),@crtid) +' )'                  

-- ELSE PRINTING FOR THE INDIVIDUAL SELECTED COURT OF THE CATEGORY....
if(@crtid <> 33  )              
	set @sqlquery =@sqlquery +'                                  
and 	tva.courtlocation =  '+ convert(varchar(10),@crtid)

-- GET RECORDS RELATED ONLY TO THE SELECTED DATE RANGE.....
set @sqlquery=@sqlquery+'
and   ' + dbo.Get_String_Concat_With_DatePart_ver2 ( ''+  @startlistdate + '', 'ta.listdate' ) 


-- LMS FILTERS SECTION:
-----------------------------------------------------------------------------------------------------------------------------------------------------
-- IF THERE IS ANY FILTER SPECIFIED TO INCLUDE/EXCLUDE ONLY PARTICULAR OFFICER NUMBERS....
if(@officernum<>'')                                    
	set @sqlquery =@sqlquery +'  
	and 	ta.officerNumber_Fk '+@officeropr +'('+ @officernum+')'                                     
              
-- FINE AMOUNT FILTER.....
-- INCLUDE ONLY THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT....
if(@fineamount<>0 and @fineamountOpr<>'not'  )              
	set @sqlquery =@sqlquery+ '   
	and tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                   

-- EXCLUDE THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT.....
if(@fineamount<>0 and @fineamountOpr = 'not'  )              
	set @sqlquery =@sqlquery+ ' 
    and not tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                   
                                    
-- IF THERE IS ANY FILTER DEFINED FOR INCLUDE/EXCLUDE TICKET NUMBER...
if(@ticket_no<>'')                                    
set @sqlquery=@sqlquery+ '
and  	('+ dbo.Get_String_Concat_With_op(''+@ticket_no+'' , 'tva.TicketNumber_PK' ,+ ''+ @tikcetnumberopr)+')'                                          
                                  
-- ZIP CODE FILTER......
-- INCLUDE/EXCLUDE THE SPECIFIED ZIP CODES.....
if(@zipcode<>'')                                                          
	set @sqlquery=@sqlquery+ ' 
	and	('+  dbo.Get_String_Concat_With_op(''+@zipcode+'' , 'left(ta.zipcode,3)' ,+ ''+ @zipcodeopr) +')'             

                               
set @sqlquery =@sqlquery+'      
group by  
	ta.recordid, convert(varchar(10),ta.listdate,101)
having 1 =1 '              

-- SINGLE VIOLATION FINE AMOUNT FILTER....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE SINGLE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT
if(@singleviolation<>0 and @doubleviolation=0)                                
	set @sqlquery =@sqlquery+ '
	and (  '+ @singlevoilationOpr+  '  (isnull(sum(isnull(tva.fineamount,0)),0) '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and count(tva.ViolationNumber_PK)=1))
      '                                

-- DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...
if(@doubleviolation<>0 and @singleviolation=0 )                                
	set @sqlquery =@sqlquery + '
	and ('+ @doublevoilationOpr+  '   (isnull(sum(isnull(tva.fineamount,0)),0) '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ')and count(tva.ViolationNumber_PK)=2) )
      '                                                    

-- BOTH SINGLE & DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE AND SINGLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...                                      
if(@doubleviolation<>0 and @singleviolation<>0)                                
	set @sqlquery =@sqlquery+ '
	and ('+ @singlevoilationOpr+  '  (isnull(sum(isnull(tva.fineamount,0)),0) '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and count(tva.ViolationNumber_PK)=1))
    and ('+  @doublevoilationOpr+  '   (isnull(sum(isnull(tva.fineamount,0)),0) '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ') and count(tva.ViolationNumber_PK)=2))
        '                                  

-- GETTING THE CLIENT'S PERSONAL & VIOLATION INFORMATION 
-- IN TEMP TABLE FOR RECORDS FILTERED IN THE ABOVE QUERY...
set @sqlquery=@sqlquery+'                                    
Select	distinct ta.recordid, 
	convert(varchar(10),ta.listdate,101) as listdate,
	tva.courtdate,
	ta.courtid, 
	flag1 = isnull(ta.Flag1,''N'') ,
	ta.officerNumber_Fk,                                          
	tva.TicketNumber_PK,
	ta.donotmailflag,
	ta.clientflag,
	upper(firstname) as firstname,
	upper (lastname ) as lastname,
	upper(address1) + '''' + isnull(ta.address2,'''') as address,                      
	upper(ta.city) as city,
	S.state as state,
	dbo.tblCourts.CourtName,
	zipcode,
	midnumber,
	dp2 as dptwo,
	dpc,                
	violationdate,
	violationstatusid,
	left(zipcode,5) + rtrim(ltrim(midnumber)) as zipmid,
	tva.ViolationDescription, 
	-- Rab Nawaz Khan 9715 10/12/2011 Default Fine amount 100 code commented . . 
	-- case when isnull(tva.fineamount,0)=0 then 100 else tva.fineamount end as FineAmount,
	isnull(tva.fineamount,0) as FineAmount,
	a.ViolCount,
	a.Total_Fineamount 
into #temp                                          
FROM    dbo.tblTicketsViolationsArchive tva 
INNER JOIN
	dbo.tblTicketsArchive ta 
ON 	tva.RecordID = ta.RecordID 
INNER JOIN
	dbo.tblCourts 
ON 	ta.CourtID = dbo.tblCourts.Courtid 
INNER JOIN
	dbo.tblCourtViolationStatus tcvs 
ON 	tva.violationstatusid = tcvs.CourtViolationStatusID 
INNER JOIN
tblstate S ON
ta.stateid_fk = S.stateID
inner join #temptable a on a.recordid = ta.recordid 
where  1=1 
AND tcvs.CategoryID <> 50  -- Abbas 9991 01/16/2012 excluded disposed violations.
and len(isnull(tva.attorneyname, '''')+isnull(tva.barcardnumber,'''')) = 0 
and CHARINDEX(''KUBOSH'',isnull(tva.BondingCompanyName,''''))=0

'               

-- VIOLATION DESCRIPTION FILTER.....
-- EXCLUDE THE LETTER IF SPECIFIED VIOLATION IS THE ONLY VIOLATION IN THE LETTER....
if(@violadesc <> '')
	BEGIN
		-- NOT LIKE FILTER.......
		if(charindex('not',@violaop)<> 0 )              
			BEGIN
				set @sqlquery=@sqlquery+' 
				and	 not ( ('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ 'like')+'))'           	
			END
		-- INCLUDE ONLY THOSE LETTER THAT SINGLE VIOLATION AND MATCHES THE SPECIFIED VIOLATION DESCRIPTION...
		-- LIKE FILTER.......
		--if(charindex('not',@violaop) =  0 )              
		ELSE
			BEGIN
				set @sqlquery=@sqlquery+' 
				and	 (  ('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ 'like')+'))'           
			END
	END

-- GETTING THE FINAL RESULSTS IN ANOTHER TEMP TABLE.....
set @sqlquery=@sqlquery+'

Select distinct recordid, violationstatusid,ViolCount,Total_Fineamount,  listdate as mdate, FirstName,LastName,address,                              
 city,state,FineAmount,violationdescription,CourtName,dpc,dptwo,Flag1,officerNumber_Fk,TicketNumber_PK,                                     
 courtid,zipmid,donotmailflag,clientflag, zipcode
 , dbo.GetPromoPriceTemplate(zipcode,'+convert(Varchar,@lettertype)+') AS promotemplate  
 into #temp1  from #temp                                    
 '              

-- ECLUDE THE LETTERS THAT HAVE BEEN ALREADY PRINTED......  
set @sqlquery =@sqlquery + '                                                  

delete from #temp1 
from #temp1 a inner join tblletternotes V on a.recordid = V.recordid  and v.lettertype = '+convert(varchar,@lettertype)+'
   
'   

if @mailbarry = 0
	set @sqlquery=@sqlquery+ 
	'
	insert into #temp1 
	select ''1'', 3, 2, 150, '''+ convert(Varchar(10), @tempdate, 101) +''', ''BARRY'', ''BOBBIT'', ''822 TENISON MEMORIAL'',
	''DALLAS'', ''TX'', 100, ''Printed on '+ convert(varchar, getdate()) +''', ''TESTING'', 0,00, ''Y'', 0, ''TESTING 1'', 3049, ''75223'', 0, 0, ''75223'', ''''
	'           

-- Farrukh 9715 10/11/2011 Added section to hide the entire columm in mailer if fine amount is zero agains any violation of a recored . . . 
set @sqlquery2 =@sqlquery2 + '  
 alter table #temp1 
 ADD isfineamount BIT NOT NULL DEFAULT 1  
 
 select recordid into #fine from #temp1   WHERE ISNULL(FineAmount,0) = 0  
 
 update t   
 set t.isfineAmount = 0  
 from #temp1 t inner join #fine f on t.recordid = f.recordid '



-- IF "MARK LETTER AS SENT" OPTION IS SELECTED....
-- CREATE LETTER HISTORY FOR EACH PERSON....
if( @printtype<>0)         
set @sqlquery2 =@sqlquery2 + '
	Declare @ListdateVal DateTime, @totalrecs int, @count int, @p_EachLetter money, @recordid varchar(50),
		@zipcode   varchar(12), @maxbatch int  ,@lCourtId int  ,@dptwo varchar(10),@dpc varchar(10)
	declare @tempBatchIDs table (batchid int)
	-- GETTING TOTAL LETTERS AND COSTING ......
	Select @totalrecs =count(*) from #temp1 where mdate = @ListdateVal                                 
	Select @count=Count(distinct recordid) from #temp1                                
	if @count>=500                                
		set @p_EachLetter=convert(money,0.3)                                
	if @count<500                                
		set @p_EachLetter=convert(money,0.39)                                

	-- DECLARING CURSOR TO INSERT THE RECORDS IN LETTER HISTORY ONE BY ONE.....
	-- THERE WILL A SEPARATE BATCH FOR EACH LIST/COURT DATE VALUE ......
	Declare ListdateCur Cursor for                                  
	Select distinct mdate  from #temp1                                
	open ListdateCur                                   
	Fetch Next from ListdateCur into @ListdateVal                                                      
	while (@@Fetch_Status=0)                              
		begin                                
			Select @totalrecs =count(distinct recordid) from #temp1 where mdate = @ListdateVal  

			-- INSERTING RECORD IN BATCH TABLE......                               
			insert into tblBatchLetter(Empid, Lettertype,Listdate,CourtId,ParentID,LCount,PExpense) values                                  
			( '+ convert(varchar(10),@empid) +'  ,  '+ convert(varchar(10),@lettertype) +'  , @ListdateVal,  '+ convert(varchar(10),@catnum) +', 0, @totalrecs, @p_EachLetter)                                   

			-- GETTING BATCH ID OF THE INSERTED RECORD.....
			Select @maxbatch=Max(BatchId) from tblBatchLetter 
				insert into @tempBatchIDs select @maxbatch                                                     

				-- CREATING CURSOR TO INSERT RECORDS FOR EACH LETTER FOR THE NEWLY INSERTED BATCH....
				Declare RecordidCur Cursor for                               
				Select distinct recordid,zipcode, courtid, dptwo, dpc from #temp1 where datediff(day,  mdate , @ListdateVal ) = 0                              
				open RecordidCur                                                         
				Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc                                          
				while(@@Fetch_Status=0)                              
				begin                              

					-- INSERTING LETTERS IN LETTER HISTORY FOR THE SPECIFIED BATCH...
					insert into tblLetterNotes(BatchId_Fk,listdate,ZipCode,RecordID,PCost,LetterType, courtid, dp2, dpc)                              
					values (@maxbatch,@ListdateVal,@zipcode,@recordid,@p_EachLetter,'+ convert(varchar(10),@lettertype) +' ,@lCourtId, @dptwo, @dpc)                              
					Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc                          
				end                              
				close RecordidCur 
				deallocate RecordidCur                                                               
			Fetch Next from ListdateCur into @ListdateVal                              
		end                                            
	close ListdateCur  deallocate ListdateCur 

	-- OUTPUTTING THE DATA TO THE APPLICATION FOR REPROT FILE.....
	 Select distinct convert(varchar(20),n.noteid) as letterid, t.recordid,  t.FirstName,                              
	 t.LastName,t.address,t.city, t.state,FineAmount,violationdescription,CourtName, t.dpc,                              
	 dptwo, TicketNumber_PK, t.zipcode, '''+@user+''' as Abb , promotemplate, isfineamount,  
	 -- Rab Nawaz 9954 12/24/2011 Jersey Village Mailer added in the Global Template mailers of HMC 
	 '''+@lettername +''' as lettername, t.courtid, t.mdate as courtdate, t.zipmid, 0 as midnumber, 0 as midnum	
	 from #temp1 t inner join tblletternotes n
	 on t.recordid = n.recordid and t.courtid = n.courtid and n.lettertype = '+ convert(varchar(10),@lettertype) +' 
	 order by T.zipcode 

-- GENERATING AND SENDING EMAIL FOR LETTER PRINTING NOTIFICATION........
declare @lettercount int, @subject varchar(200), @body varchar(400) , @sql varchar(500)
select @lettercount = count(distinct recordid) from #temp1
select @subject = convert(varchar(20), @lettercount) + '' LMS Jersey Village Appearance Letters Printed''
select @body = convert(varchar(20), @lettercount) + '' LETTERS ('' + CONVERt(VARCHAR(12), GETDATE()) + '') - ''+ upper( '''+@user+''')
exec usp_mailer_send_Email @subject, @body

-- OUTPUTING THE PDF FILE NAME TO THE APPLICATION...
declare @batchIDs_filname varchar(500)
set @batchIDs_filname = ''''
select @batchIDs_filname = @batchIDs_filname + convert(varchar,batchid) + '',''  from @tempBatchIDs
select isnull(@batchIDs_filname,'''') as batchid

'

else
	-- IF "MARK LETTER AS SENT" OPTION IS NOT SELECTED....
	-- THEN DO NOT CREATE LETTER HISTORY. JUST OUTPUT THE DATA FOR REPORT FILE 
	-- TO DISPLAY THE LETTER...
	set @sqlquery2 = @sqlquery2 + '
	 Select distinct ''NOT PRINTABLE'' as letterId,  recordid,  FirstName, LastName,address,city, state, 
	FineAmount,violationdescription,CourtName, dpc, dptwo, TicketNumber_PK, zipcode, '''+@user+''' as Abb , promotemplate, isfineamount,
	-- Rab Nawaz 9954 12/24/2011 Jersey Village Mailer added in the Global Template mailers of HMC
	 '''+@lettername +''' as lettername, courtid, mdate as courtdate, zipmid, 0 as midnumber, 0 as midnum	
  
	 from #temp1 order by zipcode 
 '
-- DROPPING TEMPORARY TABLES ....
set @sqlquery2 = @sqlquery2 + '
drop table #temp 
drop table #temp1'

--print @sqlquery + @sqlquery2
-- EXECUTING THE DYNAMIC SQL ....
exec (@sqlquery + @sqlquery2)




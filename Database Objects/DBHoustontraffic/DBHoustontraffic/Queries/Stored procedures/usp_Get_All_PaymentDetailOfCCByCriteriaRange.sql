/******   
  
Altered by:  Sabir Khan
Business Logic : this procedure is used to get the Credit payment Detail within the specific date of range.  
   
  
List of Parameters :   
  
 @RecDate     : From Search Date                           
 @RecDateTo   : To Search Date                            
 @EmployeeID  : Employee ID                             
 @PaymentType : Payment Type                            
 @CourtID  : COurt Location      
 @firmId  : Firm ID    

*****/      
ALTER procedure [dbo].[usp_Get_All_PaymentDetailOfCCByCriteriaRange]                 
                
@RecDate DateTime,                
@RecDateTo DateTime,                
@EmployeeID int,                
@PaymentType Int,                
@CourtID Int = 0,  
@firmId int = NULL,
@BranchID INT =1   -- Sabir Khan 10920 05/27/2013 Branch Id added.                
                
AS                
                
set @RecDateTo = @RecDateTo + '23:59:59.999'              
               
IF (@EmployeeID=0 AND @CourtID = 0)                
 BEGIN                
   SELECT p.ChargeAmount AS ChargeAmount,           
  p.EmployeeID,                 
                p.PaymentType,           
  CONVERT(Varchar(12), p.RecDate, 101) AS RecDate,           
  u.Lastname,                 
                CASE  wHEN (LEN(UPPER(t.Lastname) + ', ' + UPPER(t.Firstname))>19) THEN  UPPER(t.Lastname) + ', ' +UPPER(t.Firstname) + '...'                
     ELSE UPPER(t.Lastname) + ', ' + UPPER(t.Firstname)			--Sabir khan 6467 10/14/2009 client name should be in upper case...              
           END AS CustomerName,                
  c.ShortName AS Court,                
                pt.Description AS CCType,            
  p.TicketID,          
  case t.bondflag when 1 then 'B' else '' end AS BondFlag,                 
  p.CardType,           
  CONVERT(Varchar(12), t.MailDate, 101) AS ListDate,                
  dbo.Fn_FormateTime(p.RecDate) As RecTime  ,          
  p.PaymentType        ,    
 p.recdate as sorttime ,       
  case when isnull(t.initialadjustment,0) > 0 then 'YES'  ELSE '' END as Adj1,          
  CASE WHEN ISNULL(t.adjustment,0) > 0 THEN 'YES' ELSE '' END as Adj2,      
(case convert(varchar,isnull(tl.LetterID_PK,0))         
when  9 then 'ARR 1'         
when  12 then 'D ARR'      
when  13 then 'JUD'      
when  19 then 'DLQ'      
when  20 then 'APP'      
when  21 then 'WAR'      
when  22 then 'APP'         
when  24 then 'CRIM'        
when  25 then 'WAR'       
when  26 then 'CRIM'       
when  27 then 'APP'        
when  28 then 'ARR'        
when  32 then 'ARR'        
when  28 then 'ARR'        
when  32 then 'ARR'      
when  33 then 'WAR'       
when  34 then 'NCOA'       
when  35 then 'MCOA'      
when  36 then 'WAR'       
when  40 then 'ARR 1(N)'       
else 'N/A' end) +      
(case isnull(ta.NCOA18Flag,0)      
 when 1 then '-(18)'      
 else ''      
end)      
  as MailType,        
Convert(varchar,isnull(ln.RecordLoadDate,'1/1/1900'),101) as MailDate , IPAddress	-- sabir Khan 10920 05/27/2013 Getting IP address                    
/*   FROM         dbo.tblPaymenttype pt RIGHT OUTER JOIN                
  dbo.tblTicketsPayment p ON pt.Paymenttype_PK = p.PaymentType LEFT OUTER JOIN                
  dbo.tblCourts c RIGHT OUTER JOIN                
  dbo.tblTickets t ON c.Courtid = t.CurrentCourtloc ON                 
  p.TicketID = t.TicketID_PK LEFT OUTER JOIN                
  dbo.tblUsers u ON p.EmployeeID = u.EmployeeID                
*/          
 FROM dbo.tblTickets t           
 INNER JOIN          
  dbo.tblCourts c           
 INNER JOIN          
  dbo.tblTicketsViolations v           
 ON  c.Courtid = v.CourtID           
 and v.ticketsviolationid = (          
   select top 1 ticketsviolationid from tblticketsviolations          
   where courtid is not null and ticketid_pk = v.ticketid_pk          
   )          
 ON  t.TicketID_PK = v.TicketID_PK           
 RIGHT OUTER JOIN          
  dbo.tblPaymenttype pt           
 RIGHT OUTER JOIN          
  dbo.tblTicketsPayment p           
 ON  pt.Paymenttype_PK = p.PaymentType           
 ON  t.TicketID_PK = p.TicketID           
 LEFT OUTER JOIN          
  dbo.tblUsers u           
 ON  p.EmployeeID = u.EmployeeID  left outer join        
 tblletternotes ln on t.MailerID = ln.NoteID left outer join        
 tblletter tl on tl.LetterID_PK = ln.LetterType  left outer join      
 tblticketsarchive ta on ln.RecordID = ta.RecordID         
 Where        p.RecDate  Between  @RecDate  AND  @RecDateTo          
 And   PaymentVoid <> 1                
 AND   p.PaymentType = 5                  
 AND  p.CardType = Right(@PaymentType,1)    
 AND (v.coveringFirmId = coalesce(@firmId,v.coveringFirmId))               
 AND  p.Branchid IN (SELECT branchid FROM fn_get_branchIDs(@branchID)) -- Sabir Khan 10920 05/27/2013 Branch Id check added.                  
                
 END                
--*******                
Else IF (@EmployeeID<>0 AND @CourtID = 0)                
BEGIN                  
--  SELECT     CONVERT(Varchar(20), dbo.tblTicketsPayment.ChargeAmount, 101) AS ChargeAmount, dbo.tblTicketsPayment.EmployeeID,                 
 SELECT p.ChargeAmount AS ChargeAmount,          
  p.EmployeeID,                 
  p.PaymentType,           
  CONVERT(Varchar(12), p.RecDate, 101) AS RecDate,           
  u.Lastname,                 
  CASE WHEN (LEN(t.Lastname + ', ' + t.Firstname)>19)           
   THEN  t.Lastname + ', ' + t.Firstname + '...'                
       ELSE t.Lastname + ', ' + t.Firstname                
         END AS CustomerName,                
  c.ShortName AS Court,                
  pt.Description AS CCType,            
  p.TicketID,          
  case t.bondflag when 1 then 'B' else '' end AS BondFlag,                 
  p.CardType,           
  CONVERT(Varchar(12), t.MailDate, 101) AS ListDate,                
  dbo.Fn_FormateTime(p.RecDate) As RecTime      ,          
  p.PaymentType      ,        
 p.recdate as sorttime    ,    
  case when isnull(t.initialadjustment,0) > 0 then 'YES'  ELSE '' END as Adj1,          
  CASE WHEN ISNULL(t.adjustment,0) > 0 THEN 'YES' ELSE '' END as Adj2,      
(case convert(varchar,isnull(tl.LetterID_PK,0))         
when  9 then 'ARR 1'         
when  12 then 'D ARR'      
when  13 then 'JUD'      
when  19 then 'DLQ'      
when  20 then 'APP'      
when  21 then 'WAR'      
when  22 then 'APP'         
when  24 then 'CRIM'        
when  25 then 'WAR'       
when  26 then 'CRIM'       
when  27 then 'APP'        
when  28 then 'ARR'        
when  32 then 'ARR'        
when  28 then 'ARR'        
when  32 then 'ARR'      
when  33 then 'WAR'       
when  34 then 'NCOA'       
when  35 then 'MCOA'      
when  36 then 'WAR'       
when  40 then 'ARR 1(N)'       
else 'N/A' end) +      
(case isnull(ta.NCOA18Flag,0)      
 when 1 then '-(18)'      
 else ''      
end)      
  as MailType,       
Convert(varchar,isnull(ln.RecordLoadDate,'1/1/1900'),101) as MailDate ,IPAddress -- sabir Khan 10920 05/27/2013 Getting IP address  	                 
 FROM dbo.tblTickets t           
 INNER JOIN          
  dbo.tblCourts c           
 INNER JOIN          
  dbo.tblTicketsViolations v           
 ON  c.Courtid = v.CourtID           
 and v.ticketsviolationid = (          
   select top 1 ticketsviolationid from tblticketsviolations          
   where courtid is not null and ticketid_pk = v.ticketid_pk          
   )          
 ON  t.TicketID_PK = v.TicketID_PK           
 RIGHT OUTER JOIN          
  dbo.tblPaymenttype pt           
 RIGHT OUTER JOIN          
  dbo.tblTicketsPayment p           
 ON  pt.Paymenttype_PK = p.PaymentType           
 ON  t.TicketID_PK = p.TicketID           
 LEFT OUTER JOIN          
  dbo.tblUsers u           
 ON  p.EmployeeID = u.EmployeeID    left outer join        
 tblletternotes ln on t.MailerID = ln.NoteID left outer join        
 tblletter tl on tl.LetterID_PK = ln.LetterType left outer join      
 tblticketsarchive ta on ln.RecordID = ta.RecordID            
   Where        p.RecDate  Between  @RecDate  AND  @RecDateTo          
   And   p.PaymentVoid <> 1                 
   AND   p.PaymentType = 5                  
   And   p.EmployeeID= @EmployeeID                
   AND   p.CardType = Right(@PaymentType,1)  
   AND (v.coveringFirmId = coalesce(@firmId,v.coveringFirmId))  
   AND	p.Branchid IN (SELECT branchid FROM fn_get_branchIDs(@branchID))   -- Sabir Khan 10920 05/27/2013 Branch Id check added.                  
END                
                
Else IF (@EmployeeID=0 AND @CourtID > 2)                
BEGIN                  
   SELECT p.ChargeAmount AS ChargeAmount,           
  p.EmployeeID,                 
            p.PaymentType,           
  CONVERT(Varchar(12), p.RecDate, 101) AS RecDate,           
  u.Lastname,                 
                CASE  wHEN (LEN(t.Lastname + ', ' + t.Firstname)>19) THEN  t.Lastname + ', ' +t.Firstname + '...'                
     ELSE t.Lastname + ', ' + t.Firstname                
           END AS CustomerName,                
  c.ShortName AS Court,                
                pt.Description AS CCType,            
  p.TicketID,          
  case t.bondflag when 1 then 'B' else '' end AS BondFlag,                 
  p.CardType,           
  CONVERT(Varchar(12), t.MailDate, 101) AS ListDate,                
  dbo.Fn_FormateTime(p.RecDate) As RecTime  ,          
  p.PaymentType     ,      
 p.recdate as sorttime ,         
  case when isnull(t.initialadjustment,0) > 0 then 'YES'  ELSE '' END as Adj1,          
  CASE WHEN ISNULL(t.adjustment,0) > 0 THEN 'YES' ELSE '' END as Adj2,      
(case convert(varchar,isnull(tl.LetterID_PK,0))         
when  9 then 'ARR 1'         
when  12 then 'D ARR'      
when  13 then 'JUD'      
when  19 then 'DLQ'      
when  20 then 'APP'      
when  21 then 'WAR'      
when  22 then 'APP'         
when  24 then 'CRIM'        
when  25 then 'WAR'       
when  26 then 'CRIM'       
when  27 then 'APP'        
when  28 then 'ARR'        
when  32 then 'ARR'        
when  28 then 'ARR'        
when  32 then 'ARR'      
when  33 then 'WAR'       
when  34 then 'NCOA'       
when  35 then 'MCOA'      
when  36 then 'WAR'       
when  40 then 'ARR 1(N)'       
else 'N/A' end) +      
(case isnull(ta.NCOA18Flag,0)      
 when 1 then '-(18)'      
 else ''      
end)      
  as MailType,        
Convert(varchar,isnull(ln.RecordLoadDate,'1/1/1900'),101) as MailDate , IPAddress                 
 FROM dbo.tblTickets t           
 INNER JOIN          
  dbo.tblCourts c           
 INNER JOIN          
  dbo.tblTicketsViolations v           
 ON  c.Courtid = v.CourtID           
 and v.ticketsviolationid = (          
   select top 1 ticketsviolationid from tblticketsviolations          
   where courtid is not null and ticketid_pk = v.ticketid_pk          
   )          
 ON  t.TicketID_PK = v.TicketID_PK           
 RIGHT OUTER JOIN          
  dbo.tblPaymenttype pt           
 RIGHT OUTER JOIN          
  dbo.tblTicketsPayment p           
 ON  pt.Paymenttype_PK = p.PaymentType           
 ON  t.TicketID_PK = p.TicketID           
 LEFT OUTER JOIN          
  dbo.tblUsers u           
 ON  p.EmployeeID = u.EmployeeID   left outer join        
 tblletternotes ln on t.MailerID = ln.NoteID left outer join        
 tblletter tl on tl.LetterID_PK = ln.LetterType left outer join      
 tblticketsarchive ta on ln.RecordID = ta.RecordID             
   Where        p.RecDate  Between  @RecDate  AND  @RecDateTo          
   And   p.PaymentVoid <> 1                 
   AND   p.PaymentType = 5                  
   AND   c.Courtid = @CourtID                
   AND   p.CardType = Right(@PaymentType,1)  
   AND (v.coveringFirmId = coalesce(@firmId,v.coveringFirmId)) 
   AND	 p.Branchid IN (SELECT branchid FROM fn_get_branchIDs(@branchID))                 
END                
---Added By Zeeshan Ahmed------------        
Else IF (@EmployeeID=0 AND @CourtID = 1)                
BEGIN                  
   SELECT p.ChargeAmount AS ChargeAmount,           
  p.EmployeeID,                 
            p.PaymentType,           
  CONVERT(Varchar(12), p.RecDate, 101) AS RecDate,           
  u.Lastname,                 
                CASE  wHEN (LEN(t.Lastname + ', ' + t.Firstname)>19) THEN  t.Lastname + ', ' +t.Firstname + '...'                
     ELSE t.Lastname + ', ' + t.Firstname                
           END AS CustomerName,                
  c.ShortName AS Court,                
                pt.Description AS CCType,            
  p.TicketID,          
  case t.bondflag when 1 then 'B' else '' end AS BondFlag,                 
  p.CardType,           
  CONVERT(Varchar(12), t.MailDate, 101) AS ListDate,                
  dbo.Fn_FormateTime(p.RecDate) As RecTime  ,          
  p.PaymentType     ,     
 p.recdate as sorttime    ,       
  case when isnull(t.initialadjustment,0) > 0 then 'YES'  ELSE '' END as Adj1,          
  CASE WHEN ISNULL(t.adjustment,0) > 0 THEN 'YES' ELSE '' END as Adj2,      
(case convert(varchar,isnull(tl.LetterID_PK,0))         
when  9 then 'ARR 1'         
when  12 then 'D ARR'      
when  13 then 'JUD'      
when  19 then 'DLQ'      
when  20 then 'APP'      
when  21 then 'WAR'      
when  22 then 'APP'         
when  24 then 'CRIM'        
when  25 then 'WAR'       
when  26 then 'CRIM'       
when  27 then 'APP'        
when  28 then 'ARR'        
when  32 then 'ARR'        
when  28 then 'ARR'        
when  32 then 'ARR'      
when  33 then 'WAR'       
when  34 then 'NCOA'       
when  35 then 'MCOA'      
when  36 then 'WAR'       
when  40 then 'ARR 1(N)'       
else 'N/A' end) +      
(case isnull(ta.NCOA18Flag,0)      
 when 1 then '-(18)'      
 else ''      
end)      
  as MailType,        
Convert(varchar,isnull(ln.RecordLoadDate,'1/1/1900'),101) as MailDate , IPAddress	                   
         
 FROM dbo.tblTickets t           
 INNER JOIN          
  dbo.tblCourts c           
 INNER JOIN          
  dbo.tblTicketsViolations v           
 ON  c.Courtid = v.CourtID           
 and v.ticketsviolationid = (          
   select top 1 ticketsviolationid from tblticketsviolations          
   where courtid is not null and ticketid_pk = v.ticketid_pk          
   )         ON  t.TicketID_PK = v.TicketID_PK           
 RIGHT OUTER JOIN          
  dbo.tblPaymenttype pt           
 RIGHT OUTER JOIN          
  dbo.tblTicketsPayment p           
 ON  pt.Paymenttype_PK = p.PaymentType           
 ON  t.TicketID_PK = p.TicketID           
 LEFT OUTER JOIN          
  dbo.tblUsers u           
 ON  p.EmployeeID = u.EmployeeID    left outer join        
 tblletternotes ln on t.MailerID = ln.NoteID left outer join        
 tblletter tl on tl.LetterID_PK = ln.LetterType left outer join      
 tblticketsarchive ta on ln.RecordID = ta.RecordID            
   Where        p.RecDate  Between  @RecDate  AND  @RecDateTo          
   And   p.PaymentVoid <> 1                 
  AND   p.PaymentType = 5                  
   AND   c.Courtid In ( 3001 , 3002 , 3003)                
   AND   p.CardType = Right(@PaymentType,1)      
  AND (v.coveringFirmId = coalesce(@firmId,v.coveringFirmId))
  AND	 p.Branchid IN (SELECT branchid FROM fn_get_branchIDs(@branchID))              
END                
ELSE  IF (@EmployeeID <> 0 AND @CourtID = 1)               
Begin                
   SELECT p.ChargeAmount AS ChargeAmount,           
  p.EmployeeID,                 
                p.PaymentType,           
  CONVERT(Varchar(12), p.RecDate, 101) AS RecDate,           
  u.Lastname,                 
                CASE  wHEN (LEN(t.Lastname + ', ' + t.Firstname)>19) THEN  t.Lastname + ', ' +t.Firstname + '...'                
     ELSE t.Lastname + ', ' + t.Firstname                
           END AS CustomerName,                
  c.ShortName AS Court,                
                pt.Description AS CCType,            
  p.TicketID,          
  case t.bondflag when 1 then 'B' else '' end AS BondFlag,                 
  p.CardType,           
  CONVERT(Varchar(12), t.MailDate, 101) AS ListDate,                
  dbo.Fn_FormateTime(p.RecDate) As RecTime  ,          
  p.PaymentType         ,      
 p.recdate as sorttime    ,      
  case when isnull(t.initialadjustment,0) > 0 then 'YES'  ELSE '' END as Adj1,          
  CASE WHEN ISNULL(t.adjustment,0) > 0 THEN 'YES' ELSE '' END as Adj2,      
(case convert(varchar,isnull(tl.LetterID_PK,0))         
when  9 then 'ARR 1'         
when  12 then 'D ARR'      
when  13 then 'JUD'      
when  19 then 'DLQ'      
when  20 then 'APP'      
when  21 then 'WAR'      
when  22 then 'APP'         
when  24 then 'CRIM'        
when  25 then 'WAR'       
when  26 then 'CRIM'       
when  27 then 'APP'        
when  28 then 'ARR'        
when  32 then 'ARR'        
when  28 then 'ARR'        
when  32 then 'ARR'      
when  33 then 'WAR'       
when  34 then 'NCOA'       
when  35 then 'MCOA'      
when  36 then 'WAR'       
when  40 then 'ARR 1(N)'       
else 'N/A' end) +      
(case isnull(ta.NCOA18Flag,0)      
 when 1 then '-(18)'      
 else ''      
end)      
  as MailType,      
Convert(varchar,isnull(ln.RecordLoadDate,'1/1/1900'),101) as MailDate, IPAddress              
        
 FROM dbo.tblTickets t           
 INNER JOIN          
  dbo.tblCourts c           
 INNER JOIN          
  dbo.tblTicketsViolations v           
 ON  c.Courtid = v.CourtID           
 and v.ticketsviolationid = (          
   select top 1 ticketsviolationid from tblticketsviolations          
   where courtid is not null and ticketid_pk = v.ticketid_pk          
   )          
 ON  t.TicketID_PK = v.TicketID_PK           
 RIGHT OUTER JOIN          
  dbo.tblPaymenttype pt           
 RIGHT OUTER JOIN          
  dbo.tblTicketsPayment p           
 ON  pt.Paymenttype_PK = p.PaymentType           
 ON  t.TicketID_PK = p.TicketID           
 LEFT OUTER JOIN          
  dbo.tblUsers u           
 ON  p.EmployeeID = u.EmployeeID   left outer join        
 tblletternotes ln on t.MailerID = ln.NoteID left outer join        
 tblletter tl on tl.LetterID_PK = ln.LetterType left outer join      
 tblticketsarchive ta on ln.RecordID = ta.RecordID             
   Where        p.RecDate  Between  @RecDate  AND  @RecDateTo          
   And   p.PaymentVoid <> 1                 
   AND   p.PaymentType = 5                  
   And   p.EmployeeID= @EmployeeID                
   AND   p.CardType = Right(@PaymentType,1)                 
   AND   c.Courtid In ( 3001 , 3002 , 3003)  
   AND (v.coveringFirmId = coalesce(@firmId,v.coveringFirmId)) 
   AND	 p.Branchid IN (SELECT branchid FROM fn_get_branchIDs(@branchID))                  
End                
-------------------------------------      
ELSE  IF (@EmployeeID <> 0 AND @CourtID = 2)               
Begin                
   SELECT p.ChargeAmount AS ChargeAmount,           
  p.EmployeeID,                 
                p.PaymentType,           
  CONVERT(Varchar(12), p.RecDate, 101) AS RecDate,           
  u.Lastname,                 
                CASE  wHEN (LEN(t.Lastname + ', ' + t.Firstname)>19) THEN  t.Lastname + ', ' +t.Firstname + '...'                
     ELSE t.Lastname + ', ' + t.Firstname                
           END AS CustomerName,                
  c.ShortName AS Court,                
                pt.Description AS CCType,            
  p.TicketID,          
  case t.bondflag when 1 then 'B' else '' end AS BondFlag,                 
  p.CardType,           
  CONVERT(Varchar(12), t.MailDate, 101) AS ListDate,                
  dbo.Fn_FormateTime(p.RecDate) As RecTime  ,          
  p.PaymentType         ,    
 p.recdate as sorttime    ,        
  case when isnull(t.initialadjustment,0) > 0 then 'YES'  ELSE '' END as Adj1,          
  CASE WHEN ISNULL(t.adjustment,0) > 0 THEN 'YES' ELSE '' END as Adj2,      
(case convert(varchar,isnull(tl.LetterID_PK,0))         
when  9 then 'ARR 1'         
when  12 then 'D ARR'      
when  13 then 'JUD'      
when  19 then 'DLQ'      
when  20 then 'APP'      
when  21 then 'WAR'      
when  22 then 'APP'         
when  24 then 'CRIM'        
when  25 then 'WAR'       
when  26 then 'CRIM'       
when  27 then 'APP'        
when  28 then 'ARR'        
when  32 then 'ARR'        
when  28 then 'ARR'        
when  32 then 'ARR'      
when  33 then 'WAR'       
when  34 then 'NCOA'       
when  35 then 'MCOA'      
when  36 then 'WAR'       
when  40 then 'ARR 1(N)'       
else 'N/A' end) +      
(case isnull(ta.NCOA18Flag,0)      
 when 1 then '-(18)'      
 else ''      
end)      
  as MailType,        
Convert(varchar,isnull(ln.RecordLoadDate,'1/1/1900'),101) as MailDate , IPAddress             
        
 FROM dbo.tblTickets t           
 INNER JOIN          
  dbo.tblCourts c           
 INNER JOIN          
  dbo.tblTicketsViolations v           
 ON  c.Courtid = v.CourtID           
 and v.ticketsviolationid = (          
   select top 1 ticketsviolationid from tblticketsviolations          
   where courtid is not null and ticketid_pk = v.ticketid_pk          
   )          
 ON  t.TicketID_PK = v.TicketID_PK           
 RIGHT OUTER JOIN          
  dbo.tblPaymenttype pt           
 RIGHT OUTER JOIN          
  dbo.tblTicketsPayment p           
 ON  pt.Paymenttype_PK = p.PaymentType           
 ON  t.TicketID_PK = p.TicketID           
 LEFT OUTER JOIN          
  dbo.tblUsers u           
 ON  p.EmployeeID = u.EmployeeID   left outer join        
 tblletternotes ln on t.MailerID = ln.NoteID left outer join        
 tblletter tl on tl.LetterID_PK = ln.LetterType left outer join      
 tblticketsarchive ta on ln.RecordID = ta.RecordID             
   Where        p.RecDate  Between  @RecDate  AND  @RecDateTo          
   And  p.PaymentVoid <> 1                 
   AND   p.PaymentType = 5                  
   And   p.EmployeeID= @EmployeeID                
   AND   p.CardType = Right(@PaymentType,1)                 
   AND   c.Courtid Not In ( 3001 , 3002 , 3003)  
   AND (v.coveringFirmId = coalesce(@firmId,v.coveringFirmId))  
   AND	 p.Branchid IN (SELECT branchid FROM fn_get_branchIDs(@branchID))                
End                
-------------------------------------      
      
Else IF (@EmployeeID=0 AND @CourtID = 2)                
BEGIN                  
   SELECT p.ChargeAmount AS ChargeAmount,           
  p.EmployeeID,                 
            p.PaymentType,           
  CONVERT(Varchar(12), p.RecDate, 101) AS RecDate,           
  u.Lastname,                 
                CASE  wHEN (LEN(t.Lastname + ', ' + t.Firstname)>19) THEN  t.Lastname + ', ' +t.Firstname + '...'                
     ELSE t.Lastname + ', ' + t.Firstname                
           END AS CustomerName,                
  c.ShortName AS Court,                
                pt.Description AS CCType,            
  p.TicketID,          
  case t.bondflag when 1 then 'B' else '' end AS BondFlag,                 
  p.CardType,           
  CONVERT(Varchar(12), t.MailDate, 101) AS ListDate,                
  dbo.Fn_FormateTime(p.RecDate) As RecTime  ,          
  p.PaymentType     ,      
 p.recdate as sorttime    ,      
  case when isnull(t.initialadjustment,0) > 0 then 'YES'  ELSE '' END as Adj1,          
  CASE WHEN ISNULL(t.adjustment,0) > 0 THEN 'YES' ELSE '' END as Adj2,      
(case convert(varchar,isnull(tl.LetterID_PK,0))         
when  9 then 'ARR 1'         
when  12 then 'D ARR'      
when  13 then 'JUD'      
when  19 then 'DLQ'      
when  20 then 'APP'      
when  21 then 'WAR'      
when  22 then 'APP'         
when  24 then 'CRIM'        
when  25 then 'WAR'       
when  26 then 'CRIM'       
when  27 then 'APP'        
when  28 then 'ARR'        
when  32 then 'ARR'        
when  28 then 'ARR'        
when  32 then 'ARR'      
when  33 then 'WAR'       
when  34 then 'NCOA'       
when  35 then 'MCOA'      
when  36 then 'WAR'       
when  40 then 'ARR 1(N)'       
else 'N/A' end) +      
(case isnull(ta.NCOA18Flag,0)      
 when 1 then '-(18)'      
 else ''      
end)      
  as MailType,      
Convert(varchar,isnull(ln.RecordLoadDate,'1/1/1900'),101) as MailDate , IPAddress                
         
 FROM dbo.tblTickets t           
 INNER JOIN          
  dbo.tblCourts c           
 INNER JOIN          
  dbo.tblTicketsViolations v           
 ON  c.Courtid = v.CourtID           
 and v.ticketsviolationid = (          
   select top 1 ticketsviolationid from tblticketsviolations          
   where courtid is not null and ticketid_pk = v.ticketid_pk          
   )          
 ON  t.TicketID_PK = v.TicketID_PK           
 RIGHT OUTER JOIN       
  dbo.tblPaymenttype pt           
 RIGHT OUTER JOIN          
  dbo.tblTicketsPayment p           
 ON  pt.Paymenttype_PK = p.PaymentType           
 ON  t.TicketID_PK = p.TicketID           
 LEFT OUTER JOIN          
  dbo.tblUsers u           
 ON  p.EmployeeID = u.EmployeeID    left outer join        
 tblletternotes ln on t.MailerID = ln.NoteID left outer join        
 tblletter tl on tl.LetterID_PK = ln.LetterType left outer join      
 tblticketsarchive ta on ln.RecordID = ta.RecordID            
   Where        p.RecDate  Between  @RecDate  AND  @RecDateTo          
   And   p.PaymentVoid <> 1                 
  AND   p.PaymentType = 5                  
   AND   c.Courtid Not In ( 3001 , 3002 , 3003)                
   AND   p.CardType = Right(@PaymentType,1)  
 AND (v.coveringFirmId = coalesce(@firmId,v.coveringFirmId))
 AND	 p.Branchid IN (SELECT branchid FROM fn_get_branchIDs(@branchID))                  
END                
       
-------------------------------------              
ELSE                
Begin                
   SELECT p.ChargeAmount AS ChargeAmount,           
  p.EmployeeID,                 
                p.PaymentType,           
  CONVERT(Varchar(12), p.RecDate, 101) AS RecDate,           
  u.Lastname,                 
                CASE  wHEN (LEN(t.Lastname + ', ' + t.Firstname)>19) THEN  t.Lastname + ', ' +t.Firstname + '...'                
     ELSE t.Lastname + ', ' + t.Firstname                
           END AS CustomerName,                
  c.ShortName AS Court,                
                pt.Description AS CCType,            
  p.TicketID,          
  case t.bondflag when 1 then 'B' else '' end AS BondFlag,                 
  p.CardType,           
  CONVERT(Varchar(12), t.MailDate, 101) AS ListDate,                
  dbo.Fn_FormateTime(p.RecDate) As RecTime  ,          
  p.PaymentType         ,       
 p.recdate as sorttime    ,     
  case when isnull(t.initialadjustment,0) > 0 then 'YES'  ELSE '' END as Adj1,          
  CASE WHEN ISNULL(t.adjustment,0) > 0 THEN 'YES' ELSE '' END as Adj2,      
(case convert(varchar,isnull(tl.LetterID_PK,0))         
when  9 then 'ARR 1'         
when  12 then 'D ARR'      
when  13 then 'JUD'      
when  19 then 'DLQ'      
when  20 then 'APP'      
when  21 then 'WAR'      
when  22 then 'APP'         
when  24 then 'CRIM'        
when  25 then 'WAR'       
when  26 then 'CRIM'       
when  27 then 'APP'        
when  28 then 'ARR'        
when  32 then 'ARR'        
when  28 then 'ARR'        
when  32 then 'ARR'      
when  33 then 'WAR'       
when  34 then 'NCOA'       
when  35 then 'MCOA'      
when  36 then 'WAR'       
when  40 then 'ARR 1(N)'       
else 'N/A' end) +      
(case isnull(ta.NCOA18Flag,0)      
 when 1 then '-(18)'      
 else ''      
end)      
  as MailType,       
Convert(varchar,isnull(ln.RecordLoadDate,'1/1/1900'),101) as MailDate, IPAddress            
 FROM dbo.tblTickets t           
 INNER JOIN          
  dbo.tblCourts c           
 INNER JOIN          
  dbo.tblTicketsViolations v           
 ON  c.Courtid = v.CourtID           
 and v.ticketsviolationid = (          
   select top 1 ticketsviolationid from tblticketsviolations          
   where courtid is not null and ticketid_pk = v.ticketid_pk          
   )          
 ON  t.TicketID_PK = v.TicketID_PK           
 RIGHT OUTER JOIN          
  dbo.tblPaymenttype pt           
 RIGHT OUTER JOIN          
  dbo.tblTicketsPayment p           
 ON  pt.Paymenttype_PK = p.PaymentType           
 ON  t.TicketID_PK = p.TicketID           
 LEFT OUTER JOIN          
  dbo.tblUsers u           
 ON  p.EmployeeID = u.EmployeeID    left outer join        
 tblletternotes ln on t.MailerID = ln.NoteID left outer join        
 tblletter tl on tl.LetterID_PK = ln.LetterType  left outer join      
 tblticketsarchive ta on ln.RecordID = ta.RecordID           
   Where        p.RecDate  Between  @RecDate  AND  @RecDateTo          
   And   p.PaymentVoid <> 1                 
   AND   p.PaymentType = 5                  
   And   p.EmployeeID= @EmployeeID                
   AND   p.CardType = Right(@PaymentType,1)                 
   AND   c.Courtid = @CourtID    
   AND (v.coveringFirmId = coalesce(@firmId,v.coveringFirmId)) 
   AND	 p.Branchid IN (SELECT branchid FROM fn_get_branchIDs(@branchID))              
End                



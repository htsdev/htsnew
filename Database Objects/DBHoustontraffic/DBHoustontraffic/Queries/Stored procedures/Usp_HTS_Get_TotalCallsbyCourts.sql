ALTER procedure Usp_HTS_Get_TotalCallsbyCourts  
@ReportStartDate datetime ,  
@ReportEndDate datetime   
as  
  
select C.courtid,C.courtname,count(t.ticketid_pk) as TotalCalls,sum(Activeflag) as TotalHired  
  
from tbltickets T, tblcourts C ,tblticketsviolations tv 
where 
T.ticketId_pk = tv.ticketId_pk 
and tv.courtId = C.courtid  
and recdate between @ReportStartDate and dateadd(day,1,@ReportEndDate)  
  
group by C.courtid,C.courtname  
order by C.courtname  

go 
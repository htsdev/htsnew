﻿USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[usp_HTS_Update_CourtInfo]    Script Date: 09/29/2010 07:37:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

  
  
  
  
/******   
Create by:  Sarim Ghnai  
Business Logic : This procedure is used to update the court information of existing courts.  
   
List of Parameters:       
      @CaseIdentification: represnts Case Identification of court  
      @Accept Appeals:     represents Accept Appeals of court  
      @Appeallocation:     represents Appeal Location of court  
   @InitialSetting:  represents InitialSetting of court  
      @LOR:     represents LO Method of court  
      @AppearanceHire:  represents AppearanceHire of court  
      @JudgeTrialHire:  represents Judge Trial Hire of court  
   @JuryTrialHire:  represents Jury Trial Hire of court   
   @Bond Type:   represents Bond Type of court  
   @HandWritingAllowed: specify that handwriting is allowed or not   
   @AdditionalFTAIssues: specify additional FTA Issues  
      @GracePeriod:   represents Grace Period of the court  
   @Continuance   represents continuance of the court  
   @SupportingDocuments: specify that whether supporing documents are allowed or not.  
      @Comments:   represents comments about the court.   
  
******/  
  
--updated by khalid 2520 2-1-08  
-- Noufil 4237 06/24/2008 Casetype field added to Update the nature of court i-e civil,criminal or traffic

ALTER  procedure [dbo].[usp_HTS_Update_CourtInfo]        
@courtname varchar(50) = null,        
@address varchar(50) = null,        
@address2 varchar(20) = null,        
@shortname varchar(9) = null,        
@city varchar(20) = null,        
@state int = 0,        
@visitcharges money = 0,        
@courttype tinyint = 0,        
@settingrequesttype tinyint = 0,        
@judgename varchar(50) = null,        
@zip varchar(12) = null,        
@courtcontact varchar(50) = null,        
@phone varchar(20) = null,        
@fax varchar(20) = null,        
@shortcourtname varchar(50)= null,        
@courtid int = 0,        
@bondtype int = 0,        
@activeflag int = 0,  
@IsCriminalCourt int = 0,        
@RepDate datetime,  
@CausenoDuplicate bit,  
@CaseIdentifiaction int,  
@AcceptAppeals int,  
@AppealLocation varchar(200),  
@InitialSetting int,  
@LOR int,  
@AppearnceHire varchar(200),  
@JudgeTrialHire varchar(200),  
@JuryTrialHire varchar(200),  
@HandwritingAllowed int,  
@AdditionalFTAIssued int,  
@GracePeriod int,  
--Nasir 5310 12/29/2008 take parameter @ALRProcess
@ALRProcess int,  
@Continuance varchar(200),  
@SupportingDocument int,  
@Comments varchar(1000),
@CaseTypeId int,
@isletterofrep INT,
--Sabir Khan 5763 04/10/2009 
@islorsubpoena INT,
@islormod INT,
--Sabir Khan 5941 07/24/2009 County Id parameter has been added for association of county with court location...
@CountyId INT,
--Nasir 7369 02/19/2010 added AllowOnlineSignUp
--@AllowOnlineSignUp BIT,
--Asad Ali 8153 09/20/2010 Associated Court ID 
@AssociatedALRCourtID INT,
--Muhammad Muneer 8227 09/27/2010 added five LOR dates
@LORrepdate2 DATETIME,
@LORrepdate3 DATETIME,
@LORrepdate4 DATETIME,
@LORrepdate5 DATETIME,
@LORrepdate6 DATETIME,
@AllowCourtNumbers AS BIT -- Rab Nawaz Khan 8997 08/02/2011 Added for allow or restrice the court Numbers for the outside courts

as        

IF @AssociatedALRCourtID=0
BEGIN
	SET @AssociatedALRCourtID=NULL
END    
    
    
update tblcourts        
 set address = @address,        
 address2 = @address2,        
 city = @city,        
 state = @state,        
 visitcharges = @visitcharges,        
 courttype = @courttype,        
 settingrequesttype = @settingrequesttype,        
 judgename = @judgename,        
 zip = @zip,        
 courtcontact = @courtcontact,        
 phone = @phone,        
 fax = @fax ,        
 shortcourtname = @shortcourtname,        
 courtname = @courtname,        
 bondtype = @bondtype,        
 inactiveflag= @activeflag,        
 shortname = @shortname,  
 IsCriminalCourt=@IsCriminalCourt,  
 Repdate = dbo.fn_DateFormat(@RepDate,28,'/',':',1)  ,  
 CausenoDuplicate=@CausenoDuplicate,    
 CaseIdentifiaction=@CaseIdentifiaction,  
 AcceptAppeals=@AcceptAppeals,  
 AppealLocation=@AppealLocation,  
 InitialSetting=@InitialSetting,  
 LOR=@LOR,  
 AppearnceHire=@AppearnceHire,  
 JudgeTrialHire=@JudgeTrialHire,  
 JuryTrialHire=@JuryTrialHire,  
 HandwritingAllowed=@HandwritingAllowed,  
 AdditionalFTAIssued=@AdditionalFTAIssued,  
 GracePeriod=@GracePeriod,  
 ALRProcess=@ALRProcess,  --Nasir 5310 12/29/2008	SET column ALRProcess 
 Continuance=@Continuance,  
 SupportingDocument=@SupportingDocument,  
 Comments=@Comments,
CaseTypeId=@CaseTypeId,
IsletterofRep=@isletterofrep, -- Noufil 4945 10/27/2008 New column added 
--Sabir Khan 5763 04/10/2009 
IsLORSubpoena = @islorsubpoena,
IsLORMOD = @islormod,
--Sabir Khan 5941 07/24/2009 County Id has been added...
CountyId = @CountyId,
--Nasir 7369 02/19/2010 added AllowOnlineSignUp
--AllowOnlineSignUp =@AllowOnlineSignUp,
--Asad Ali 8153 09/20/2010 Associated Court ID 
AssociatedALRCourtID=@AssociatedALRCourtID,
--Muhammad Muneer 8227 09/27/2010 updating the already added five LOR Dates
LORRepDate2 = @LORrepdate2,
LORRepDate3 = @LORrepdate3,
LORRepDate4 = @LORrepdate4,
LORRepDate5 = @LORrepdate5,
LORRepDate6 = @LORrepdate6,
-- Rab Nawaz Khan 8997 08/02/2011 Added for allow or restrice the court Numbers for the outside courts
AllowCourtNumbers = @AllowCourtNumbers
 where courtid = @courtid        
  
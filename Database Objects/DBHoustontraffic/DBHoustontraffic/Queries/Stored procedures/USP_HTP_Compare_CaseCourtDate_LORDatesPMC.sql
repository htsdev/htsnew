﻿USE TrafficTickets
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
 
/****** Object:  StoredProcedure [dbo].[USP_HTP_Compare_CaseCourtDate_LORDatesPMC]    Script Date: 08/13/2013 14:21:43 ******/
/*  
Created By : Zeeshan Haider
Business Logic : This procedure compare LOR dates for PMC with Case court date.
Task ID: 10699
*/
ALTER PROCEDURE [dbo].[USP_HTP_Compare_CaseCourtDate_LORDatesPMC]
	@TicketID INT
AS
BEGIN
    DECLARE @LORDate1 DATETIME
    DECLARE @LORDate2 DATETIME
    DECLARE @LORDate3 DATETIME
    DECLARE @LORDate4 DATETIME
    DECLARE @LORDate5 DATETIME
    DECLARE @LORDate6 DATETIME
    DECLARE @CourtDate DATETIME    
    DECLARE @DateIsInFuture INT
    SET @DateIsInFuture = 0
    
    SELECT @CourtDate = MAX(ttv.CourtDateMain)
    FROM   tblTicketsViolations ttv
    WHERE  ttv.TicketID_PK = @TicketID
    
    SELECT @LORDate1 = RepDate,
           @LORDate2 = LORRepDate2,
           @LORDate3 = LORRepDate3,
           @LORDate4 = LORRepDate4,
           @LORDate5 = LORRepDate5,
           @LORDate6 = LORRepDate6
    FROM   tblCourts
    WHERE  Courtid = 3004
    
    
    
    IF DATEDIFF(DAY, @CourtDate, @LORDate1) > 0
       OR DATEDIFF(DAY, @CourtDate, @LORDate2) > 0
       OR DATEDIFF(DAY, @CourtDate, @LORDate3) > 0
       OR DATEDIFF(DAY, @CourtDate, @LORDate4) > 0
       OR DATEDIFF(DAY, @CourtDate, @LORDate5) > 0
       OR DATEDIFF(DAY, @CourtDate, @LORDate6) > 0
    BEGIN
        SET @DateIsInFuture = 1
    END
    --Zeeshan Haider 11306 08/12/2013 PMC Jury-Trial Cases(Do not modify case setting for sending LOR)
    IF EXISTS (
           SELECT ttv.TicketID_PK
           FROM   tblTicketsViolations ttv
           WHERE  ttv.TicketID_PK = @TicketID
                  AND ttv.CourtViolationStatusIDmain = 26
                  AND ttv.CourtID = 3004
       )
    BEGIN
        SET @DateIsInFuture = 1
    END
    
    SELECT @DateIsInFuture AS [DateIsInFuture]
END

GO
GRANT EXECUTE ON [dbo].[USP_HTP_Compare_CaseCourtDate_LORDatesPMC] TO dbr_webuser
GO
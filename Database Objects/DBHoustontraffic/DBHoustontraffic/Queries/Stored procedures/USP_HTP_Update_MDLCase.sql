﻿USE TrafficTickets
GO

/****** 
Create by		: Zeeshan Haider
Created Date	: 10/02/2012
TasK			: 10437

Business Logic : 
			This procedure update MDL Cases
			
List of Parameters:	
			@CaseNumber	MDL Case Number
			@Date		Date of insertion/modification
			@EmployeeID	Employee identification
			@isActive	active/inactive flag					
******/

CREATE PROCEDURE [dbo].[USP_HTP_Update_MDLCase]
	@CaseNumber VARCHAR(20),
	@Date DATETIME = '01/01/1900',
	@EmployeeID VARCHAR(50),
	@isActive BIT,
	@Id INT
AS
BEGIN
        UPDATE [TrafficTickets].[dbo].[tblMDLCases]
        SET    [ModifiedDate] = @Date,
               [ModifiedBy] = @EmployeeID,
               [isActive] = @isActive,
               CaseNumber = @CaseNumber
        WHERE  Id = @Id
END
GO

GRANT EXECUTE ON [dbo].[USP_HTP_Update_MDLCase] TO dbr_webuser
GO 
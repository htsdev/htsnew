/******  
* Created By :	  Afaq Ahmed
* Create Date :   6/20/2011 4:10:08 PM 
* Task ID :		  9398
* Business Logic :  Return all payment identifier.
* List of Parameter :N/A
* Column Return :   Id,Name  
******/
Alter PROCEDURE [dbo].[USP_HTP_GetPaymentIdentifier]
AS
	SELECT id,[Name]
	FROM   paymentIdentifier 
	-- Abbas Qamar 9957 04-01-2011 to select all records except Bond
	WHERE id  != 3
	


﻿/****** 
Created by:  Syed Muhammad Ozair
Business Logic : this procedure is used to get all the firms with full firm name
 
List of Columns: 
		FirmID, 
		FullName as FirmFullname  
 
******/ 


Create PROCEDURE  dbo.usp_HTP_Get_AllFullFirmsName  
AS  
  
Select  FirmID, 		
		FirmName as FirmFullname  
From	tblFirm  
order by	FirmName  

go

Grant exec on dbo.usp_HTP_Get_AllFullFirmsName to dbr_webuser
go
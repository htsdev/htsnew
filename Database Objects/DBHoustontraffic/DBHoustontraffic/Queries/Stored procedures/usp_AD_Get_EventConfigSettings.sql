/****** 
Created by:		Syed Muhammad Ozair
Business Logic:	The Procedure is used to get the event config settings				
	
	
List of Columns:
	EventTypeID,
	EventTypeName,
	EventTypeDays,
	CourtID,
	SettingDateRange,
	EventState,
	DialTime
			
*******/

ALTER Procedure dbo.usp_AD_Get_EventConfigSettings

as

select 
	EventTypeID,
	EventTypeName,
	EventTypeDays,
	CourtID,
	SettingDateRange,
	EventState,
	DialTime,
	case CourtID when 0 then 'All Courts' when 1 then 'HMC Courts' when 2 then 'Non HMC Courts' else (select c.shortname from tblcourts c where c.courtid=s.CourtID) end as CourtName
from AutoDialerEventConfigSettings s

--Farrukh 9332 07/29/2011 Multiple Event Call Time implmented
	SELECT EventTypeID,DialTime
	FROM   AutoDialerEventDialTime
	ORDER BY EventTypeID

go

grant exec on dbo.usp_AD_Get_EventConfigSettings to dbr_webuser
go
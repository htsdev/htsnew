﻿/**  
* Waqas 5771 04/17/2009  
* Business Logic : This procedure is used to update contact information if not available
* Only those column will be update who have null values or default values  
*
Parameter List : 
  
ContactID
FirstName
MiddleName
LastName
DLNumber
DLState
NoDL
CDLFlag
DOB
Address1
Address2
Email
City
StateID_FK
Zip
LanguageSpeak
Race
Gender
Height
Weight
HairColor
Eyes
Phone1
PhoneType1
Phone2
PhoneType2
Phone3
PhoneType3
EmailNotAvailable
MidNumber
NODL
*   
**/  
  
--usp_htp_update_contactinformation @ContactID = 168133, @PhoneType1 = 1, @Phone1  = '111111111', @LanguageSpeak = 'SPANISH', @CDLFlag = 0  
  
Create PROC dbo.usp_htp_update_contactinformation  
  
@ContactID INT,  
@MiddleName VARCHAR(6) = null,   
@DLNumber VARCHAR(30) = NULL,   
@DLState int = NULL,  
@CDLFlag tinyint = 3,  
@DOB datetime = NULL,  
@Address1 varchar(50) = NULL,  
@Address2 varchar(50) = NULL,  
@Email varchar(100) = NULL,  
@City varchar(50) = NULL,  
@StateID_FK int = NULL,  
@Zip varchar(20) = NULL,  
@LanguageSpeak varchar(30) = NULL,  
@Race varchar(20) = NULL,  
@Gender varchar(10) = NULL,  
@Height varchar(10) = NULL,  
@Weight varchar(10) = NULL,  
@HairColor varchar(10) = NULL,  
@Eyes VARCHAR(10) = NULL,  
@Phone1 varchar(15) = NULL,  
@PhoneType1 int = NULL,  
@Phone2 VARCHAR(15) = NULL,  
@PhoneType2 int = NULL,  
@Phone3 VARCHAR(15) = NULL,  
@PhoneType3 int = NULL,  
@MidNumber VARCHAR(20) = NULL ,
@NoDL INT = null 
  
AS  
  
BEGIN  
   
DECLARE @QUERY VARCHAR(MAX)  
  
  
  
declare @_MiddleName VARCHAR   
declare @_DLNumber VARCHAR   
DECLARE @_DLState int   
DECLARE @_CDLFlag tinyint   
DECLARE @_DOB datetime   
DECLARE @_Address1 varchar   
DECLARE @_Address2 varchar   
DECLARE @_Email varchar   
DECLARE @_City varchar   
DECLARE @_StateID_FK int   
DECLARE @_Zip varchar   
DECLARE @_LanguageSpeak VARCHAR  
DECLARE @_Race varchar   
DECLARE @_Gender varchar  
DECLARE @_Height varchar   
DECLARE @_Weight varchar   
DECLARE @_HairColor varchar   
DECLARE @_Eyes varchar   
DECLARE @_Phone1 varchar   
DECLARE @_PhoneType1 int   
DECLARE @_Phone2 varchar   
DECLARE @_PhoneType2 int   
DECLARE @_Phone3 varchar   
DECLARE @_PhoneType3 int   
DECLARE @_MidNumber varchar   
DECLARE @_NoDL int
  
SELECT   
@_DLNumber = DLNumber,   
@_MiddleName = MiddleName,  
@_DLState = DLState,   
@_CDLFlag = CDLFlag,   
@_DOB = DOB,   
@_Address1 = Address1,   
@_Address2 = Address2,   
@_Email = Email,   
@_City = City,   
@_StateID_FK = StateID_FK,   
@_Zip = Zip,   
@_LanguageSpeak = LanguageSpeak,   
@_Race = Race,   
@_Gender = Gender,   
@_Height = Height,   
@_Weight = Weight,   
@_HairColor = HairColor,   
@_Eyes = Eyes,   
@_Phone1 = Phone1,   
@_PhoneType1 = PhoneType1,   
@_Phone2 = Phone2,   
@_PhoneType2 = PhoneType2,   
@_Phone3 = Phone3,   
@_PhoneType3 = PhoneType3,   
@_MidNumber = MidNumber,  
@_NoDL = NoDL
FROM Contact c WHERE c.ContactID = @ContactID  
  
SET @QUERY = ' '  
IF(@CDLFlag <> 3 and @_CDLFlag = 3) BEGIN set @QUERY = @QUERY + ' CDLFlag = ' + CONVERT(VARCHAR, @CDLFlag) + ', ' END  
IF(@LanguageSpeak IS NOT NULL AND @_LanguageSpeak IS NULL) BEGIN set @QUERY = @QUERY + ' LanguageSpeak = ''' + @LanguageSpeak + ''', ' END  
IF(@Phone1 IS NOT NULL AND @_Phone1 IS NULL) BEGIN set @QUERY = @QUERY + ' Phone1 = ''' + CONVERT(VARCHAR, @Phone1) + ''', ' END  
IF(@PhoneType1 IS NOT NULL AND @_PhoneType1 IS NULL) BEGIN set @QUERY = @QUERY + ' PhoneType1 = ' + CONVERT(VARCHAR, @PhoneType1) + ', ' END   
IF(@MiddleName IS not NULL and  @_MiddleName is NULL) BEGIN set @QUERY = @QUERY + ' MiddleName = ''' + @MiddleName + ''', ' END   
IF(@DLNumber IS not NULL and  @_DLNumber is NULL) BEGIN set @QUERY = @QUERY + ' DLNumber = ''' + @DLNumber + ''' , DLState = ' + CONVERT(VARCHAR, @DLState) + ', NODL = ' + CONVERT(VARCHAR, @NoDL) + ', ' END 
IF(@DOB IS NOT NULL and  @_DOB IS NULL) BEGIN set @QUERY = @QUERY + ' DOB = ''' + @DOB + ''', ' END  
IF(@Address1 IS NOT NULL and  @_Address1 IS NULL) BEGIN set @QUERY = @QUERY + ' Address1 = ''' + @Address1 + ''', ' END  
IF(@Address2 IS NOT NULL and  @_Address2 IS NULL) BEGIN set @QUERY = @QUERY + ' Address2 = ''' + @Address2 + ''', ' END  
IF(@Email IS NOT NULL and  @_Email IS NULL) BEGIN set @QUERY = @QUERY + ' Email = ''' + @Email + ''', ' END  
IF(@City IS NOT NULL and  @_City IS NULL) BEGIN set @QUERY = @QUERY + ' City = ''' + @City + ''', ' END  
IF(@StateID_FK IS NOT NULL and  @_StateID_FK IS NULL) BEGIN set @QUERY = @QUERY + ' StateID_FK = ' + CONVERT(VARCHAR, @StateID_FK) + ', ' END  
IF(@Zip IS NOT NULL and  @_Zip IS NULL) BEGIN set @QUERY = @QUERY + ' Zip = ''' + @Zip + ''', ' END  
IF(@Race IS NOT NULL and  @_Race IS NULL) BEGIN set @QUERY = @QUERY + ' Race = ''' + @Race + ''', ' END  
IF(@Gender IS NOT NULL and  @_Gender IS NULL) BEGIN set @QUERY = @QUERY + ' Gender = ''' + @Gender + ''', ' END  
IF(@Height IS NOT NULL and  @_Height IS NULL) BEGIN set @QUERY = @QUERY + ' Height = ''' + @Height + ''', ' END  
IF(@Weight IS NOT NULL and  @_Weight IS NULL) BEGIN set @QUERY = @QUERY + ' Weight = ''' + @Weight + ''', ' END  
IF(@HairColor IS NOT NULL and  @_HairColor IS NULL) BEGIN set @QUERY = @QUERY + ' HairColor = ''' + @HairColor + ''', ' END  
IF(@Eyes IS NOT NULL and  @_Eyes IS NULL) BEGIN set @QUERY = @QUERY + ' Eyes = ''' + @Eyes + ''', ' END  
IF(@Phone2 IS NOT NULL and @_Phone2 IS NULL) BEGIN set @QUERY = @QUERY + ' Phone2 = ''' + @Phone2 + ''', ' END  
IF(@PhoneType2 IS NOT NULL AND @_PhoneType2 IS NULL) BEGIN set @QUERY = @QUERY + ' PhoneType2 = ' + CONVERT(VARCHAR, @PhoneType2) + ', ' END  
IF(@Phone3 IS NOT NULL and @_Phone3 IS NULL) BEGIN set @QUERY = @QUERY + ' Phone3 = ''' + @Phone3 + ''', ' END  
IF(@PhoneType3 IS NOT NULL and @_PhoneType3 IS NULL) BEGIN set @QUERY = @QUERY + ' PhoneType3 = ' + CONVERT(VARCHAR, @PhoneType3) + ', ' END  
IF(@MidNumber IS NOT NULL ) BEGIN set @QUERY = @QUERY + ' MidNumber = ''' + @MidNumber + ''', ' END  
  
  
  
PRINT (@QUERY)  
  
IF( LEN(LTRIM(RTRIM(@QUERY))) <> 0)  
begin   
 SET @QUERY  = SUBSTRING(@QUERY, 1, LEN(RTRIM(@QUERY))-1)  
 set @QUERY = 'UPDATE Contact SET ' + @QUERY + ' where contactid = ' + convert(VARCHAR, @ContactID) + ' '  
 PRINT (@QUERY)  
 EXEC(@QUERY)  
end  
  
--PRINT (@QUERY)  
  
  
END 

go

grant execute on [dbo].[usp_htp_update_contactinformation] to dbr_webuser
go

    
 
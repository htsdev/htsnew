IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'USP_HTS_Get_FinReport_BalancePaid')
	BEGIN
		DROP  Procedure  USP_HTS_Get_FinReport_BalancePaid
	END

GO


-- Last Modified By : Zeeshan Ahmed
-- Last Modified Date : 1/18/2008
-- Modify Procedure For Bounce Check Payment Type
CREATE procedure [dbo].[USP_HTS_Get_FinReport_BalancePaid]
	(
	@sDate	datetime,
	@eDate	datetime
	)
as 

select	@eDate = @eDate + '23:59:59.999'

declare	@temp	table (
	ticketid	int,
	clienttype	varchar(20)
	)

insert into @temp
select ticketid,
	-- IF MININUM OF PAYMENT DATE FOR A TICKET ID LIES BETWEEN THE SELECTED DATE RANGE... 
	case when   ( min(recdate)  >= @sDate and min(recdate) <= @eDate )

		-- THEN CONSIDER THIS TICKET AS 'NEW CLIENT' 
		then 'New Client' 

	     -- ELSE AS 'BALANCE PAID'
	     else 'BP' 
	end as clienttype
from tblticketspayment 
where ticketid in (
	select 	ticketid from tblticketspayment 
	where 	recdate between @sDate and @eDate

	-- EXCLUDING BALANCE DUE, CHEQUES IN MAIL, FREIND CREDIT AND ATTORNEY CREDIT PAYEMNT TYPES...
	and	paymenttype not in (99,100,9, 11 ) 
	and 	paymentvoid = 0
	) 
group by 
	ticketid  

declare	@temp1	table (
	ticketid	int,
	clienttype	varchar(20),
	bondflag 	bit,
	chargeamount	money	,
	courtid 	int,
	paymenttype	int
	)

insert into @temp1
select	a.ticketid,
	a.clienttype, 
	t.bondflag,
	isnull(p.chargeamount,0),
	v.courtid,
	p.paymenttype
from 	tbltickets T , tblticketspayment P,  @temp a , tblticketsviolations v
where a.ticketid = T.ticketid_pk 
and T.ticketid_pk = P.ticketid 
and t.ticketid_pk = v.ticketid_pk
and P.recdate between @sDate and @eDate
and p.paymentvoid = 0
and p.paymenttype not in (99,100,9,11)
and v.ticketsviolationid = (
		select top 1 ticketsviolationid from tblticketsviolations
		where ticketid_pk  = v.ticketid_pk and (courtid is not null and courtid <> 0)
		)

--select * from @temp1 order by courtid, clienttype, bondflag


-- CREATING TEMPORARY TABLE......
declare	@temp3 table (
	categorytype varchar(50),
	totalcount int,
	totalamount money,
	DisplayCol int
	)



insert 	into @temp3 select 'Tickets',0,0,0


-- GETTING HOUSTON REGULAR TICKETS.......
insert 	into @temp3 
select	'Houston Regular Tickets' , count(ticketid), isnull(sum(chargeamount),0),1
from	@temp1 
where	clienttype <> 'BP'
and	bondflag = 0
and	courtid in (3001,3002,3003)
and	paymenttype not in (8,103)


-- GETTING OUTSIDE COURTS REGULAR TICKETS.......
insert 	into @temp3 
select	'Outside Court Regular Tickets' , count(ticketid),  isnull(sum(chargeamount),0),1
from	@temp1 
where	clienttype <> 'BP'
and	bondflag = 0
and	courtid not  in (3001,3002,3003, 3043,3047,3048)
and 	courtid not  in ( select courtid from tblcourts where courtcategorynum in ( 2,4) )
and	paymenttype not in (8,103)


insert into @temp3 select 'Bonds',0,0,0

-- GETTING HOUSTON BOND TICKETS....
insert 	into @temp3 
select	'Houston Bonds' , count(ticketid),  isnull(sum(chargeamount),0),1
from	@temp1 
where	clienttype <> 'BP'
and	bondflag = 1
and	courtid in (3001,3002,3003)
and	paymenttype not in (8,103)

-- GETTING OUTSIDE COURTS BOND TICKETS.......
insert 	into @temp3 
select	'Outside Bonds' , count(ticketid),  isnull(sum(chargeamount),0),1
from	@temp1 
where	clienttype <> 'BP'
and	bondflag = 1
and	courtid not  in (3001,3002,3003, 3043,3047,3048)
and 	courtid not  in ( select courtid from tblcourts where courtcategorynum in ( 2,4) )
and	paymenttype not in (8,103)


insert into @temp3 select 'Criminal Division',0,0,0

-- GETTING  Harris County Criminal TICKETS
insert 	into @temp3 
select	'Harris County Criminal' , count(ticketid),  isnull(sum(chargeamount),0),1
from	@temp1 
where	clienttype <> 'BP'
and	courtid   in ( select courtid from tblcourts where courtcategorynum = 2)
and	courtid not in (3001,3002,3003)
and	paymenttype not in (8,103)


-- GETTING Fort Bend County Court TICKETS.....
insert 	into @temp3 
select	'Fort Bend County' , count(ticketid),  isnull(sum(chargeamount),0),1
from	@temp1 
where	clienttype <> 'BP'
and	courtid   in ( 3043)
and	paymenttype not in (8,103)

-- GETTING  Montgomery County Criminal TICKETS
insert 	into @temp3 
select	'Montgomery County Criminal' , count(ticketid),  isnull(sum(chargeamount),0),1
from	@temp1 
where	clienttype <> 'BP'
and	courtid   in ( select courtid from tblcourts where courtcategorynum = 4)
and	courtid not in (3001,3002,3003)
and	paymenttype not in (8,103)

-- GETTING  ALR TICKETS
insert 	into @temp3 
select	'ALR' , count(ticketid), isnull(sum(chargeamount),0),1
from	@temp1 
where	clienttype <> 'BP'
and	courtid   in ( 3047, 3048)
and	paymenttype not in (8,103)

-- GETTING  REFUND TRANSACTIONS..............
insert 	into @temp3 
select	'Refunds' , count(ticketid), isnull(sum(chargeamount),0),1
from	@temp1 
where	paymenttype IN (8,103)


-- GETTING BALANCE PAID COUNTS & AMOUNT ...........
insert	into @temp3
select	'Balance Paid', count(ticketid), isnull(sum(chargeamount),0),1
from 	@temp1
where	clienttype = 'BP'
and 	paymenttype not in (8,103)


-- RETURNING THE FINAL RESULT SET.......
select * from @temp3




/***********

Business Logic : Thsi procedure is used to update case history.

Parameter : 
		@ticketid : Case Number
		@subject  : Subject 
		@notes    : History Notes
		@employeeid : Employee Id

Column Return :
		
	@@rowcount : return the number of rows update

************/  
  
    
alter procedure [dbo].[usp_PS_Add_Notes]  
@ticketid int,  
@subject varchar(4000),  
@notes varchar(4000),  
@employeeid int  
as  
insert into tblticketsnotes(ticketid,subject,notes,employeeid) values(@TicketID,@subject,@notes,@employeeid)  
select @@rowcount  


go

grant execute on dbo.usp_PS_Add_Notes to dbr_webuser


go  


  
 
-- Alter By Zeeshan Ahmed 
-- Check Main Status 
ALTER procedure [dbo].[usp_Varify_CourtStatus] --6890,26    
@ticketid  int,     
@Status  int     
as    
SET NOCOUNT ON;    
select TicketID_PK,tv.CourtViolationstatusIDmain,ts.Description     
         from tblTicketsViolations tv WITH(NOLOCK) join tblCourtViolationStatus ts WITH(NOLOCK) on    
           tv.CourtViolationstatusIDmain=ts.CourtViolationstatusID    
where (TicketID_PK=@ticketid) and (ts.CourtViolationstatusID=@Status) 
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_UpdateManualCC]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_UpdateManualCC]
GO


    
-- =============================================    
-- Author:  <Asghar>    
-- Create date: <Feb/15/2007>    
-- Description: <It will update tblmanualpayment by info which is given by getway after transacion completed >    
-- =============================================    
    
--USP_HTS_UpdateManualCC 69,1,'',1606082620,1,1,'This transaction has been approved',246357,'P',0

CREATE PROCEDURE [dbo].[USP_HTS_UpdateManualCC]     
@paymentid int,            
@status bit,            
@errmsg varchar(255),            
@transnum varchar(50),            
@response_subcode  varchar(20),                
@response_reason_code   varchar(20),             
@response_reason_text varchar(100),             
@auth_code   varchar(6),                       
@avs_code varchar(1),    
@voidpayment BIT         
    
AS    
           
declare @approvedflag int      
set  @approvedflag = 0                    
if  @status = '1'    
begin    
set  @approvedflag = 1            
end    
else    
begin    
set  @status=0     
end    
    
BEGIN     
 update tblmanualpayment set    
    
  status =@status,    
  errmsg =@errmsg,    
  transnum =@transnum,    
  response_subcode =@response_subcode,    
  response_reason_code =@response_reason_code,    
  response_reason_text =@response_reason_text,    
  auth_code =@auth_code,    
  approveflag =@approvedflag,    
  avs_code =@avs_code,    
  voidpayment =@voidpayment      
 where PaymentID =@paymentid    

-- Added By Zeeshan Ahmed For Credit Card Transaction logging
       
Update tblticketscybercashlog set invnumber = Convert(varchar,@paymentid) , RefTransnum =''
 where RefTransnum = convert(varchar(30),@paymentid  )
  
END   



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


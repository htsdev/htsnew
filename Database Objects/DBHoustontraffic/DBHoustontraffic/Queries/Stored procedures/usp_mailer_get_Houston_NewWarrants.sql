

/****** 
Altered by:		Sabir Khan
Business Logic:	The procedure is used by LMS application to get data for selected date range.
				
List of Parameters:
	@catnum:		Category number.
	@lettertype:	Type of letter.     
	@startListdate	Start list date            
	@crtid :		Court ID 
	@endListdate	End list date.	
	@searchtype:	Search Type	
	
*******/
--  usp_mailer_Get_Houston_NewWarrants  1, 41, 1, '07/18/12', '07/17/12', 0
ALTER Procedure [dbo].[usp_mailer_Get_Houston_NewWarrants]
	(
	@catnum		int,
	@LetterType 	int,
	@crtid 		int   ,                                              
	@startListdate 	Datetime,
	@endListdate 	DateTime,                                                      
	@SearchType     int
	)

as                                                              
          
SET NOCOUNT ON;
                                                  
declare	@officernum varchar(50), 
	@officeropr varchar(50), 
	@tikcetnumberopr varchar(50), 
	@ticket_no varchar(50), 
	@zipcode varchar(50), 
	@zipcodeopr varchar(50), 
	@fineamount money,
	@fineamountOpr varchar(50) ,
	@fineamountRelop varchar(50),
	@singleviolation money, 
	@singlevoilationOpr varchar(50) , 
	@singleviolationrelop varchar(50), 
	@doubleviolation money,
	@doublevoilationOpr varchar(50) , 
	@doubleviolationrelop varchar(50), 
	@count_Letters int,
	@violadesc varchar(500),
	@violaop varchar(50),
	@sqlquery nvarchar(MAX)  ,
	@sqlquery2 nvarchar(MAX)  ,
	@sqlParam nvarchar(1000)


select @sqlparam = '@catnum int, @LetterType int, @crtid int, @startListdate Datetime,
		    @endListdate DateTime, @SearchType int'
                                                             
                                                      
select 	@endlistdate = @endlistdate+'23:59:59.000' 
--select 	@endlistdate

	select @sqlquery ='', @sqlquery2 = ''
                                                              
Select	@officernum=officernum,
	@officeropr=oficernumOpr,
	@tikcetnumberopr=tikcetnumberopr,
	@ticket_no=ticket_no,
	@zipcode=zipcode,
	@zipcodeopr=zipcodeLikeopr,
	@singleviolation =singleviolation,
	@singlevoilationOpr =singlevoilationOpr, 
	@singleviolationrelop =singleviolationrelop, 
	@doubleviolation = doubleviolation,
	@doublevoilationOpr=doubleviolationOpr,
	@doubleviolationrelop=doubleviolationrelop,
	@violadesc=violationdescription,
	@violaop=violationdescriptionOpr , 
	@fineamount=fineamount ,
	@fineamountOpr=fineamountOpr ,
	@fineamountRelop=fineamountRelop  
from 	tblmailer_letters_to_sendfilters WITH(NOLOCK) 
where 	courtcategorynum=@catnum 
and 	Lettertype=@LetterType 
                        


                              
set 	@sqlquery=@sqlquery+'                                          
 Select distinct convert(datetime , convert(varchar(10),@startListdate,101)) as mdate,
 flag1 = isnull(ta.Flag1,''N'') , 
 ta.donotmailflag, TA.recordid AS RECORDID,
left(TA.zipcode,5) + ta.midnumber as zipmid,
 count(tva.ViolationNumber_PK) as ViolCount, 
 isnull(sum(isnull(tva.fineamount,0)),0) as Total_Fineamount,
 tva.violationnumber_pk,tva.violationdescription
into #temp FROM dbo.tblTicketsViolationsArchive TVA WITH(NOLOCK) 
INNER JOIN 
dbo.tblTicketsArchive TA WITH(NOLOCK) ON TVA.recordid = TA.recordid
where TVA.violationstatusid = 186
and charindex(''FTA'',tva.causenumber) <> 0
and isnull(ta.ncoa48flag,0) = 0
--Muhammad Muneer 8465 11/11/2010 added the new functionality that is the null checks for the first name and the last name
and LEN(ISNULL(RTRIM(LTRIM(ta.lastname)),''''))<>0
and LEN(ISNULL(RTRIM(LTRIM(ta.firstname)),''''))<>0 
and TVA.FTALinkID is not null

--Rab Nawaz Khan 9119 04/04/2011 added attorney association logic.
and len(isnull(tva.attorneyname, '''')+isnull(tva.barcardnumber,'''')) = 0 
and CHARINDEX(''KUBOSH'',isnull(tva.BondingCompanyName,''''))=0
-- Sabir Khan 10245 05/03/2012
and isnull(ta.IsIncorrectMidNum,0) = 0
'

if @lettertype = 41
	set 	@sqlquery=@sqlquery+'
	and datediff(day, ftaissuedate, getdate()) between 30 and 45  '
else if @lettertype = 42
	set 	@sqlquery=@sqlquery+'
	and datediff(day, ftaissuedate, getdate()) between 60 and 75  '
else if @lettertype = 43
	set 	@sqlquery=@sqlquery+'
	and datediff(day, ftaissuedate, getdate()) between 90 and 105  '
--Sabir Khan 6410 08/19/2009 Three new warrant letter added.
--------------------------------------------------------------------
--Sabir Khan 6503 09/01/2009 The warrant letter (180 - 270) has been changed into Warrant (180 - 220)
ELSE IF @LetterType = 65
	SET @sqlquery = @sqlquery+'
	and datediff(day, ftaissuedate, getdate()) between 180 and 220  '
ELSE IF @LetterType = 66
	SET @sqlquery = @sqlquery+'
	and datediff(day, ftaissuedate, getdate()) between 360 and 390  '  
ELSE IF @LetterType = 67
	SET @sqlquery = @sqlquery+'
	and datediff(day, ftaissuedate, getdate()) between 720 and 750  '
--------------------------------------------------------------------- 
	
if(@crtid=1  )                                    
	set @sqlquery=@sqlquery+' 
	and tva.courtlocation In (Select courtid from tblcourts WITH(NOLOCK) where courtcategorynum = @crtid )' 

if( @crtid >= 3000 )                                    
	set @sqlquery =@sqlquery +' 
	and tva.courtlocation = @crtid' 
 
if(@officernum<>'')                                                        
	set @sqlquery =@sqlquery + '  and ta.officerNumber_Fk '  + @officeropr  +'('+ @officernum+')' 
                                                       
if(@ticket_no<>'') 
	set @sqlquery=@sqlquery + ' and ('+  dbo.Get_String_Concat_With_op(''+@ticket_no+'' , 'tva.TicketNumber_PK' ,+ ''+ @tikcetnumberopr) +')' 
 
if(@zipcode<>'')                                                                              
	set @sqlquery=@sqlquery+ ' and ('+ dbo.Get_String_Concat_With_op(''+@zipcode+'' , 'left(ta.zipcode,3)' ,+ ''+ @zipcodeopr) +')' 

if(@violadesc<>'') 
	set @sqlquery=@sqlquery+ ' and ('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ @violaop) +')'

if(@fineamount<>0 and @fineamountOpr<>'not'  ) 
	set @sqlquery =@sqlquery+ '
        and tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')' 

if(@fineamount<>0 and @fineamountOpr = 'not'  ) 
	set @sqlquery =@sqlquery+ '
        and not  tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')' 

                                                      

set @sqlquery =@sqlquery+ ' 
and ta.recordid not in ( 
	select	d.recordid from tblletternotes n WITH(NOLOCK), tblletternotes_detail d WITH(NOLOCK)
	where 	lettertype = @LetterType and   n.noteid = d.noteid
	union
	-- exclude DLQ letters printed today or yesterday...
	select d.recordid from tblletternotes n WITH(NOLOCK) inner join tblletternotes_detail d WITH(NOLOCK)
	on d.noteid = n.noteid  where lettertype = 19 
	and datediff(day, recordloaddate, getdate()) in (0,1)
	)'



	set @sqlquery =@sqlquery+ ' group by    tva.violationnumber_pk,tva.violationdescription, 
 --convert(datetime , convert(varchar(10),@startListdate,101)) ,
 ta.Flag1,ta.donotmailflag , TA.recordid,
left(TA.zipcode,5) + ta.midnumber
'

                  
--print @sqlquery
                                    
set @sqlquery=@sqlquery + '
Select distinct  zipmid,ViolCount, Total_Fineamount, mdate ,Flag1, donotmailflag,recordid,violationnumber_pk,violationdescription into #temp1  from #temp 
 '                                  
if(@singleviolation<>0 and @doubleviolation=0) 
	set @sqlquery =@sqlquery+ 'Where  '+ @singlevoilationOpr+ 
      '  (Total_FineAmount '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and ViolCount=1) 
         or violcount>3 
      ' 
                                  
                                       
if(@doubleviolation<>0 and @singleviolation=0 )                                  
	set @sqlquery =@sqlquery + 'where   '+ @doublevoilationOpr+ 
       '   (Total_FineAmount '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ')and ViolCount=2) 
      or violcount>3 

--Yasir Kamal 7119 12/10/2009 send warrant letters to existing clients.
--Sabir Khan 7099 12/08/2009 Exclude client cases... 
--set @sqlquery=@sqlquery + 
--delete from #temp1 where recordid in (
--select v.recordid from tblticketsviolations v inner join tbltickets t 
--on t.ticketid_pk = v.ticketid_pk and t.activeflag = 1 
--inner join #temp1 a on a.recordid = v.recordid
--)

--Rab Nawaz Khan 9685 09/14/20211 Remove cases having attorney association...
select tva.recordid,tva.violationnumber_pk,tva.violationdescription into #tempAttorney 
from #temp1 t inner join tblticketsviolationsarchive tva WITH(NOLOCK) on tva.ftalinkid = t.ftalinkid 
where len(isnull(tva.attorneyname, '''')+isnull(tva.barcardnumber,'''')) <> 0
and tva.violationstatusid <> 80 -- Abbas 9991 01/16/2012 Excluded disposed cases.

delete  f 
from #temp1 f
inner join #tempAttorney a on a.recordid = f.recordid and a.violationnumber_pk = f.violationnumber_pk
and a.violationdescription = f.violationdescription

'



set @sqlquery2=@sqlquery2 +'
Select count(distinct zipmid) as Total_Count, mdate 
into #temp2 
from #temp1 
group by mdate 
Select count(distinct zipmid) as Good_Count, mdate 
into #temp3
from #temp1 
where Flag1 in (''Y'',''D'',''S'')   
and donotmailflag = 0    
group by mdate 
Select count(distinct zipmid) as Bad_Count, mdate  
into #temp4  
from #temp1 
where Flag1  not in (''Y'',''D'',''S'') 
and donotmailflag = 0 
group by mdate 
Select count(distinct zipmid) as DonotMail_Count, mdate  
into #temp5  from #temp1 where donotmailflag=1 
group by mdate
create table #letterdates (mdate datetime)
while datediff(day, @startListdate, @endlistdate)>=0
begin
	insert into #letterdates (mdate) select convert(varchar(10),@startlistdate,101)
	set @startlistdate = dateadd(day, 1, @startlistdate)
end
'
	
set @sqlquery2 = @sqlquery2 +' 
Select 	l.mdate as listdate,
        isnull(Total_Count,0) as Total_Count,
        isnull(Good_Count,0) Good_Count,  
        isnull(Bad_Count,0)Bad_Count,  
        isnull(DonotMail_Count,0)DonotMail_Count  
from #letterdates l left outer join #Temp2 on #temp2.mdate = l.mdate left outer join #temp3 
on #temp2.mdate=#temp3.mdate  
left outer join #Temp4  
on #temp2.mdate=#Temp4.mdate    
left outer join #Temp5  
on #temp2.mdate=#Temp5.mdate 
order by l.mdate
Select count(distinct zipmid) from #temp1 
where Flag1 in (''Y'',''D'',''S'') and donotmailflag = 0  
drop table #temp drop table #temp1 drop table #temp2  drop table #temp3  drop table #temp4  drop table #temp5  drop table #letterdates
' 
                                                  
print @sqlquery  + @sqlquery2                                       

--exec( @sqlquery)
set @sqlquery = @sqlquery + @sqlquery2
exec sp_executesql @sqlquery , @sqlparam, @catnum, @LetterType, @crtid, @startListdate, @endListdate, @SearchType


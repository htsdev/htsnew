﻿/******     
Created by:  Syed Muhammad Ozair   
Business Logic : this procedure is used to get bond client ,reguler client and All client of set/reminder/fta   
    calls depend on the mode parameter.     
         
List of Parameters:     
    @showreport  -- Show report = 0 for reminder calls; 1 for set calls; 2 for FTA calls    
 @reminderdate    
 @mode   
 @status  
 @TicketIDs  
 @ReminderCourtDateStart  
 @ReminderCourtDateEnd   
******/   
-- usp_AD_Get_Calls 0,'03/05/2009',3,0,'164638,196926,'  
  
ALTER PROCEDURE [dbo].[usp_AD_Get_Calls]  
 @showreport INT, -- Show report = 0 for reminder calls; 1 for set calls; 2 for FTA calls  
 @reminderdate DATETIME,  
 @mode INT,  
 @status INT,  
 @TicketIDs VARCHAR(MAX),  
 @ReminderCourtDateStart DATETIME,  
 @ReminderCourtDateEnd DATETIME   
AS  
 --select  @reminderdate='08/15/2008',@status=0,@showall=0,@showreport=1,@mode=3  
   
 DECLARE @eventTypeID INT  
 DECLARE @eventTypeName VARCHAR(50)  
 DECLARE @SDR INT  
 DECLARE @EventTypeDays INT  
 DECLARE @CourtID INT  
   
 SET @eventTypeID = 0  
   
 IF @showreport = 0  
     SET @eventTypeID = 2  
 ELSE   
 IF @showreport = 1  
     SET @eventTypeID = 1  
 ELSE   
 IF @showreport = 2  
     SET @eventTypeID = 3  
   
 DECLARE @Startdate DATETIME,  
         @EndDate DATETIME  
   
 SELECT @EventTypeDays = EventTypeDays,  
        @SDR = settingdaterange,  
        @eventTypeName = EventTypeName,  
        @CourtID = CourtID  
 FROM   autodialereventconfigsettings  
 WHERE  eventtypeid = @eventTypeID  
   
 SET @Startdate = @reminderdate + @EventTypeDays  
   
 SET @Startdate = @Startdate + '23:59:58'    
   
 SET @EndDate = @Startdate + @SDR  
   
   
 DECLARE @str VARCHAR(MAX)      
   
 SET @str =   
     '  
     SELECT DISTINCT       
  t.TicketID_PK as ticketid,        
  t.Firstname,       
  t.Lastname,       
  isnull(t.LanguageSpeak,''N/A'') as LanguageSpeak,      
  t.BondFlag,        
  '  
   
 IF @showreport = 0  
 BEGIN  
     SET @str = @str +  
         '  
         isnull(tv.ReminderCallStatus,0) as status,   
         tv.autodialerremindercallstatus as autodialerstatus, '  
 END  
   
 IF @showreport = 1 -- for set call  
 BEGIN  
     SET @str = @str +  
         '  
         isnull(tv.setCallStatus,0) as status,   
         tv.autodialersetcallstatus as autodialerstatus, '  
 END  
 ELSE   
 IF @showreport = 2  
 BEGIN  
     SET @str = @str +  
         '  
         isnull(tv.FTACallStatus,0) as status,   
         0 as autodialerstatus, '  
 END   
 -- ended 3977    
 SET @str = @str +  
     '  
     convert(varchar(20),ISNULL(dbo.formatphonenumbers(T.Contact1),''''))+''(''+ISNULL(LEFT(Ct1.Description, 1),'''')+'')'' as contact1,  
     convert(varchar,Ct1.autodialerpriority) as  contact1Priority,  
  convert(varchar,isnull(T.ContactType1,0))as ContactType1,                                            
  convert(varchar(20),ISNULL(dbo.formatphonenumbers(T.Contact2),''''))+''(''+ISNULL(LEFT(Ct2.Description, 1),'''')+'')'' as contact2,                                                
  convert(varchar,Ct2.autodialerpriority) as  contact2Priority,                                        
  convert(varchar,isnull(T.ContactType2,0))as ContactType2,                                                
  convert(varchar(20),ISNULL(dbo.formatphonenumbers(T.Contact3),''''))+''(''+ISNULL(LEFT(Ct3.Description, 1),'''')+'')'' as contact3,                                                 
  convert(varchar,Ct3.autodialerpriority) as  contact3Priority,                                        
  convert(varchar,isnull(T.ContactType3,0))as ContactType3,                                                  
  tv.CourtDateMain          
  into #temp4      
  FROM  tblTicketsViolations TV   
   INNER JOIN dbo.tblTickets t ON  TV.TicketID_PK = t.TicketID_PK                              
   inner join dbo.tblCourtViolationStatus cvs ON TV.CourtViolationStatusIDMain = cvs.CourtViolationStatusID AND cvs.CategoryID <> 50 --Haris Ahmed 10335 06/20/2012 Issue fix for picking dispose client                                  
   LEFT OUTER JOIN dbo.tblContactstype ct2 ON  t.ContactType2 = ct2.ContactType_PK and t.autodialercancallflag2=1 and ct2.ContactType_PK in (1,4)  
   LEFT OUTER JOIN dbo.tblContactstype ct1 ON  t.ContactType1 = ct1.ContactType_PK and t.autodialercancallflag1=1 and ct1.ContactType_PK in (1,4)  
   LEFT OUTER JOIN dbo.tblContactstype ct3 ON  t.ContactType3 = ct3.ContactType_PK and t.autodialercancallflag3=1 and ct3.ContactType_PK in (1,4)  
  WHERE   
   (Ct1.autodialerpriority<>0 or Ct2.autodialerpriority<>0 or Ct3.autodialerpriority<>0)  
   AND  
   isnull(t.bondflag,0)=0  
   AND   
   cvs.categoryid not in (50,7)    
   AND  
   t.ticketid_pk in (select * from dbo.Sap_String_Param(''' + @TicketIDs + '''))   
   '  
     
 --Testing Purpose at live  
 if @eventTypeID=1  
 SET @str = @str +  
     '  
   --AND t.ticketid_pk = 9999999  
   --OR   
   --t.ticketid_pk in (13446,5065,85292,125054,8790,74861)  
   --t.ticketid_pk = 8790  
   --t.ticketid_pk in (13446,5065)     
  '   
    
  if @eventTypeID=2  
 SET @str = @str +  
     '  
   --AND   
   --t.ticketid_pk = 9999999  
  '   
    
   
    
    
    
 if @CourtID=1  
 SET @str = @str +  
     '  
      AND  
   TV.courtid in (3001,3002,3003)   
     '  
   
 if @CourtID=2  
 SET @str = @str +  
     '  
   AND  
   TV.courtid not in (3001,3002,3003)      
     '  
     --Reminder Call  
 if @eventTypeID=2  
 SET @str = @str +  
     '  
   AND  
   datediff(day,TV.courtdatemain,'''+Convert(varchar,@ReminderCourtDateStart,101)+''')<=0  
   AND datediff(day,TV.courtdatemain,'''+Convert(varchar,@ReminderCourtDateEnd,101)+''')>=0  
     '  
       
 SET @str = @str +  
     '  
     select distinct         
  ticketid,         
  firstname,      
  lastname,       
  languagespeak,      
  case when bondflag=1 then ''B'' else '''' end as bondflag,      
  ISNULL(status,0) as status,  
  autodialerstatus,                         
  Contact1,  
  contact1Priority ,      
  ContactType1,  
  Contact2,       
  contact2Priority,  
  ContactType2,  
  Contact3,       
  contact3Priority,  
  ContactType3,  
  T.CourtDateMain,         
  case dbo.tblTicketsFlag.flagid       
  when 5 then 1  else 2  end as flagid,          
  rs.Description as callback          
  into #temp1       
  from #temp4 T       
  left OUTER JOIN      
  dbo.tblTicketsFlag ON T.TicketID = dbo.tblTicketsFlag.TicketID_PK       
  and dbo.tblTicketsFlag.FlagID in (5)         
  left outer join tblReminderStatus rs on rs.Reminderid_pk = T.status                                                   
  order by T.CourtDateMain  
      
  --newly added to call function in order to get distinct violations                                                                      
  select       
  T.*,  
  dbo.Fn_AD_Get_TicketsViolationsWithCourts (ticketid,CourtDateMain) as trialdesc ,      
  WrongNumberFlag = (select Count(*) from tblticketsflag where ticketid_pk = T.TicketId and FlagID=33),  
  dbo.fn_hts_get_TicketsViolationIds (ticketid,CourtDateMain) as TicketViolationIds       
  into #temp5       
  from #temp1 T         
  order by t.lastname, t.firstname      
  '   
   
 SET @str = @str + 'select * into #temp6 from #temp5 where status = ' +  
     CONVERT(VARCHAR, @status)   
   
   
 SET @str = @str + '  
  select * into #temp7 from   
  (select   
  ticketid,' +  
  CONVERT(VARCHAR, @eventTypeID) + ' as EventTypeID,''' + CONVERT(VARCHAR, @eventtypename) + ''' as EventTypeName,        
  lastname,  
  firstname,                
  languagespeak,      
  bondflag,      
  ISNULL(status,0) as status,  
  autodialerstatus,                         
  Contact1 as contact,  
  contact1Priority as Priority,  
  ContactType1 as ContactType,        
  flagid,     
  callback,  
  trialdesc,  
  wrongnumberflag,  
  TicketViolationIds,   
  isnull(responsetype,''Pending'') as Response,  
  convert(varchar,getdate(),101) as eventdatetime         
  from #temp6 t left outer join AutoDialerResponse r   
  on t.autodialerstatus=r.responseid  
  where    
  charindex(''('',Contact1)=13  
  and   
  len(contact1)>5  
  and   
  contact1Priority<>0  
   
  union  
    
  select   
  ticketid,' +  
     CONVERT(VARCHAR, @eventTypeID) + ' as EventTypeID,''' + CONVERT(VARCHAR, @eventtypename) + ''' as EventTypeName,        
  lastname,  
  firstname,                 
  languagespeak,      
  bondflag,      
  ISNULL(status,0) as status,  
  autodialerstatus,                         
  Contact2 as contact,  
  contact2Priority as Priority,  
  ContactType2 as ContactType,            
  flagid,          
  callback,  
  trialdesc,  
  wrongnumberflag,  
  TicketViolationIds,  
  isnull(responsetype,''Pending'') as  Response ,  
  convert(varchar,getdate(),101) as eventdatetime          
  from #temp6 t left outer join AutoDialerResponse r  
  on t.autodialerstatus=r.responseid  
  where    
  charindex(''('',Contact2)=13  
  and   
  len(contact2)>5  
  and  
  contact2Priority<>0  
    
  union  
    
  select   
  ticketid,' +  
     CONVERT(VARCHAR, @eventTypeID) + ' as EventTypeID,''' + CONVERT(VARCHAR, @eventtypename) + ''' as EventTypeName,                 
  lastname,  
  firstname,            
  languagespeak,      
  bondflag,      
  ISNULL(status,0) as status,  
  autodialerstatus,                         
  Contact3 as contact,  
  contact3Priority as Priority,  
  ContactType3 as ContactType,             
  flagid,          
  callback,  
  trialdesc,  
  wrongnumberflag,  
  TicketViolationIds,  
  isnull(responsetype,''Pending'') as Response,  
  convert(varchar,getdate(),101) as eventdatetime         
  from #temp6 t left outer join AutoDialerResponse r   
  on t.autodialerstatus=r.responseid  
  where    
  charindex(''('',Contact3)=13  
  and   
  len(contact3)>5  
  and   
  contact3Priority<>0  
  ) a  
  order by ticketid,Priority '  
   
   
 SET @str = @str +'   
 SELECT ticketid,  
       EventTypeID,  
       EventTypeName,  
       lastname,  
       firstname,  
       languagespeak,  
       bondflag,  
       STATUS,  
       autodialerstatus,  
       contact,  
       Priority,  
       ContactType,  
       flagid,  
       callback,  
       MIN(trialdesc) AS trialdesc,  
       wrongnumberflag,  
       TicketViolationIds,  
       Response,  
       eventdatetime         
FROM   #temp7  
GROUP BY  
       ticketid,  
       EventTypeID,  
       EventTypeName,  
       lastname,  
       firstname,  
       languagespeak,  
       bondflag,  
       STATUS,  
       autodialerstatus,  
       contact,  
       Priority,  
       ContactType,  
       flagid,  
       callback,         
       wrongnumberflag,  
       TicketViolationIds,  
       Response,  
       eventdatetime  
ORDER BY  
ticketid,Priority  
       '  
   
 SET @str = @str +  
     '                                
  drop table #temp1                                                      
  drop table #temp4                                                   
  drop table #temp5  
  drop table #temp6  
  drop table #temp7         
  '   
   
 --print @str   
   
 EXEC (@str)  
GO

Grant exec on dbo.usp_AD_Get_Calls to dbr_webuser
go  

/****** 
Created by:		Rab Nawaz Khan
Task ID   :     10401
Date      :     08/15/2012 

Business Logic:	The procedure is used by LMS application to get data for Missouri City Arraignmnet 
				and Warrant letters and to create LMS history for this letter for the selected date range.
				
List of Parameters:
	@catnum:		Category number of the selected letter type, if printing for 
					all courts of the selected court category
	@LetterType:	Selected letter type in LMS
	@CrtId:			Court id of the selected court category if printing for individual court hosue.
	@startListdate:	Comma separated dates selected to print letter.
	@EmpId:			Employee id for the currently logged in user
	@PrintType:		Flag to just preview the letter or to mark letters as sent. If 1 then LMS letter history will be created.
	@SearchType:	Flag that identifies if searching by court date or by list date. 0= list date, 1 = court date
	@isPrinted:		Flag to include already printed letters in the batch.

List of Columns:	
	LetterId:		Letter ID generated for each letter. It will be "NOT PRINTABLE"	if @PrintType = 0
	RecordId:		Identity value to look up the case in non-clients. used to group the records in report file.
	FirstName:		Person's first name that will appear on the letter
	LastName:		Person's last name that will appear on the letter
	Address:		Person's Home address that will appear on the letter
	City:			Person's Home City that will appear on the letter
	State:			Person's Home State that will appear on the letter
	FineAmount:		Fine amount for each violation that will appear on the letter
	ViolationDescritpion: violation description that will appear on the letter
	CourtName:		Name of court with which the violation is associated. Used in report file
	DPC:			Address status that is used in barcode font on the letter
	DPTwo:			Address status that is used in barcode font on the letter
	TicketNumber:	Case number for the violation
	ZipCode:		Person's Home Zip code.
	MidNumber:		MID Number associated with the person
	ViolationDate:	Date of the violation
	ZipMid:			First five characters of the zipcode plus mid number
	Abb:			Short abbreviation of employee who is printing the letter

******/

-- USP_MAILER_Send_Immigration_Data 35, 130, 35, '10/01/2012,' , 3992, 1
CREATE PROCEDURE [dbo].[USP_MAILER_Send_Immigration_Data]  
@catnum int=35, 
@LetterType int=130,                                      
@crtid int=35,
@startListdate varchar (500) ='01/04/2006',                                      
@empid int=3992,
@printtype int =0                                   
                                      
AS                                      
-- DECLARING LOCAL VARIABLES FOR THE FILTERS.... 

-- DECLARING & INITIALIZING LOCAL VARIABLES FOR DYNAMIC SQL.. 
declare @sqlquery varchar(max), @sqlquery2 varchar(max)
Select @sqlquery =''  , @sqlquery2 = ''    

-- GETTING THE SHORT ABBREVIATION OF THE CURRENTLY LOGGED IN 
-- EMPLOYEE IN THE LOCAL VARIABLE....  
declare @user varchar(10)  
select @user = upper(abbreviation) from tblusers where employeeid = @empid  


-- CREATING DYNAMIC SQL FOR THE MAIN QUERY...
set @sqlquery=@sqlquery+' 
Select distinct ta.recordid,
convert(varchar(10),ta.listdate,101) as listdate, 
DATEDIFF(DAY, ISNULL(ta.DOB, ''01/01/1900''), GETDATE()) / 365.25 AS ageLimit, 
DATEDIFF(DAY, ISNULL(ta.DOB, ''01/01/1900''), GETDATE()) / 365 AS AgeYears  
into #MainQuery  
FROM    dbo.tblTicketsViolationsArchive tva INNER JOIN  dbo.tblTicketsArchive ta 
ON  tva.RecordID = ta.RecordID INNER JOIN  dbo.tblCourtViolationStatus tcvs   
ON  tva.violationstatusid = tcvs.CourtViolationStatusID   

-- ONLY VERIFIED AND VALID ADDRESSES
where   Flag1 in (''Y'',''D'',''S'')  

-- NOT MARKED AS STOP MAILING...
and donotmailflag = 0   

-- SHOULD BE NON-CLIENT
and ISNULL(ta.clientflag, 0) = 0   

-- Only violations which contains "license", "insurance", OR any thing related to "Driver''s license" in the violation Description. . . 
AND (tva.ViolationDescription LIKE ''%license%'' OR tva.ViolationDescription LIKE ''%insurance%'' 
	OR tva.ViolationDescription LIKE ''%drivers license%'' OR tva.ViolationDescription LIKE ''%driver''''s license%'') 

-- PERSONS ADDRESS IS NOT CHANGED IN PAST 48 MONTHS (ACCUZIP)...
and isnull(ta.ncoa48flag,0) = 0

-- First Name last should not be null or empty. . . 
and ISNULL(TA.FirstName, '''') <> '''' 
and ISNULL(TA.LastName, '''') <> '''' 
'                    

if(@startListdate<>'')                                      
	set @sqlquery=@sqlquery+'  
	and   ' + dbo.Get_String_Concat_With_DatePart_ver2 ( ''+  @startlistdate + '', 'ta.listdate' )             

-- ECLUDE THE LETTERS THAT HAVE BEEN ALREADY PRINTED......
set @sqlquery =@sqlquery+ '        
group by    
 ta.recordid, 
 convert(varchar(10),ta.listdate,101), 
 DATEDIFF(DAY, ISNULL(ta.DOB, ''01/01/1900''), GETDATE()) / 365.25,
 DATEDIFF(DAY, ISNULL(ta.DOB, ''01/01/1900''), GETDATE()) / 365
   

-- GETTING THE FINAL RESULSTS IN ANOTHER TEMP TABLE.....
Select distinct ta.recordid,   
 a.listdate,  
 ta.courtdate,
 ISNULL(ta.courtid, 3001) AS courtid,
 ta.donotmailflag,  
 ta.clientflag,  
 upper(firstname) as firstname,  
 upper(lastname) as lastname,  
 upper(address1) + '''' + isnull(ta.address2,'''') as address,                        
 upper(ta.city) as city,  
 s.state,    
 zipcode,           
 tva.ticketviolationdate as violationdate,  
 ViolationDescription,
 a.ageLimit, 
 a.AgeYears, 
 dp2 as dptwo, 
 convert(varchar(10),ta.DOB,101) as DOB   
into #secondQuery 
from tblticketsarchive ta, tblticketsviolationsarchive tva, #MainQuery a, tblstate s 
where ta.recordid = tva.recordid  and ta.stateid_fk = s.stateid 
and ta.recordid = a.recordid 


-- Excluding already printed letters. . . 
Delete from #secondQuery
FROM #secondQuery a INNER JOIN tblletternotes n ON n.RecordID = a.recordid  
WHERE 	lettertype = '+convert(varchar(10),@lettertype)+'  



-- Deleting the Records which are greater on less then age limit 16 to 30. . . 
DELETE FROM #secondQuery
WHERE ageLimit < 16
OR ageLimit > 30


-- GETTING THE FINAL RESULSTS IN ANOTHER TEMP TABLE.....
Select distinct recordid, listdate, FirstName,LastName,address, city, state, violationdescription, courtid, zipcode, courtdate, DOB, dptwo, AgeYears, 130 AS LetterType  
into #finalQuery  from #secondQuery 
where 1=1
AND  (ViolationDescription LIKE ''%license%'' OR ViolationDescription LIKE ''%insurance%'' 
	OR ViolationDescription LIKE ''%drivers license%'' OR ViolationDescription LIKE ''%driver''''s license%'') 
' 



-- IF "MARK LETTER AS SENT" OPTION IS SELECTED....
-- CREATE LETTER HISTORY FOR EACH PERSON....                   
if( @printtype = 1 )
	begin
		set @sqlquery2 =@sqlquery2 +                                      
		'  
		Declare @ListdateVal DateTime,                                    
		@totalrecs int,                                  
		@count int,                                  
		@p_EachLetter money,                                
		@recordid varchar(50),                                
		@zipcode   varchar(12),                                
		@maxbatch int  ,  
		@lCourtId int  ,  
		@dptwo varchar(10),  
		@dpc varchar(10)  

		declare @tempBatchIDs table (batchid int)  

		-- GETTING TOTAL LETTERS AND COSTING ......
		Select @totalrecs =count(*) from #finalQuery where listdate = @ListdateVal 
		set @p_EachLetter=convert(money,0.45)  

		-- DECLARING CURSOR TO INSERT THE RECORDS IN LETTER HISTORY ONE BY ONE.....
		-- THERE WILL A SEPARATE BATCH FOR EACH LIST/COURT DATE VALUE ......
		Declare ListdateCur Cursor for                                    
		Select distinct listdate  from #finalQuery                                  
		open ListdateCur                                     
		Fetch Next from ListdateCur into @ListdateVal                                                        
		while (@@Fetch_Status=0)                                
			begin                                  

				-- GETTING TOTAL LETTERS FOR THE LIST/COURT DATE...
				Select @totalrecs =count(distinct recordid) from #finalQuery where listdate = @ListdateVal                                   

				-- INSERTING RECORD IN BATCH TABLE......                               
				insert into tblBatchLetter(Empid, Lettertype,Listdate,CourtId,ParentID,LCount,PExpense) values                                    
				('+convert(varchar(10),@empid)+' ,'+convert(varchar(10),@lettertype)+', @ListdateVal, '+convert(varchar(10),@catnum)+', 0, @totalrecs, @p_EachLetter)                                     

				-- GETTING BATCH ID OF THE INSERTED RECORD.....
				Select @maxbatch=Max(BatchId) from tblBatchLetter   
				insert into @tempBatchIDs select @maxbatch                                                       

				-- CREATING CURSOR TO INSERT RECORDS FOR EACH LETTER FOR THE NEWLY INSERTED BATCH....
				Declare RecordidCur Cursor for                                 
				Select distinct recordid,zipcode, courtid, dptwo from #finalQuery where listdate = @ListdateVal                                 
				open RecordidCur                                                           
				Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo                                         
				while(@@Fetch_Status=0)                                
					begin                                

						-- INSERTING LETTERS IN LETTER HISTORY FOR THE SPECIFIED BATCH...
						insert into tblLetterNotes(BatchId_Fk,listdate,ZipCode,RecordID,PCost,LetterType, courtid, dp2)                                
						values (@maxbatch,@ListdateVal,@zipcode,@recordid,@p_EachLetter,'+convert(varchar(10),@lettertype)+' ,@lCourtId, @dptwo)                                
						Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo                         
					end                                
				close RecordidCur   
				deallocate RecordidCur                                                                 
				Fetch Next from ListdateCur into @ListdateVal                                
			end                                              

		close ListdateCur    
		deallocate ListdateCur   

		-- OUTPUTTING THE DATA TO THE APPLICATION FOR REPROT FILE.....		 
		Select distinct convert(varchar(20),n.noteid) as letterid, t.recordid, t.courtdate, t.FirstName, 
		t.LastName,t.address,t.city, t.state, t.zipcode, '''+@user+''' as Abb, DOB, dptwo, t.courtid, AgeYears, t.LetterType
		from #finalQuery t , tblletternotes n, @tempBatchIDs tb  
		where t.recordid = n.recordid and t.courtid = n.courtid and n.batchid_fk = tb.batchid and n.lettertype = '+convert(varchar(10),@lettertype)+'   
		order by t.zipcode  

		-- GENERATING AND SENDING EMAIL FOR LETTER PRINTING NOTIFICATION........		
		declare @lettercount int, @subject varchar(200), @body varchar(400) ,  @sql varchar(500)  
		select @lettercount = count(distinct n.noteid) from tblletternotes n, @tempBatchIDs b where n.batchid_fk = b.batchid  
		select @subject = convert(varchar(20), @lettercount) +  '' Immigration Letters printed ''  
		select @body = convert(varchar(20), @lettercount) + '' LETTERS ('' + CONVERt(VARCHAR(12), GETDATE()) + '') - ''+ upper( '''+@user+''')  
		exec usp_mailer_send_Email @subject, @body  

		-- OUTPUTING THE PDF FILE NAME TO THE APPLICATION...
		declare @batchIDs_filname varchar(500)
		set @batchIDs_filname = ''''
		select @batchIDs_filname = @batchIDs_filname + convert(varchar,batchid) + '',''  from @tempBatchIDs
		select isnull(@batchIDs_filname,'''') as batchid

		'  
	end
  
else  if( @printtype = 0 )

	-- IF "MARK LETTER AS SENT" OPTION IS NOT SELECTED....
	-- THEN DO NOT CREATE LETTER HISTORY. JUST OUTPUT THE DATA FOR REPORT FILE 
	-- TO DISPLAY THE LETTER...
	begin
		set @sqlquery2 = @sqlquery2 + '  		
		
		Select distinct ''NOT PRINTABLE'' as letterId, t.recordid, t.courtdate, t.FirstName, 
		t.LastName,t.address,t.city, t.state, t.zipcode, '''+@user+''' as Abb, DOB, dptwo, courtid, AgeYears, LetterType 
		from #finalQuery t
		order by t.zipcode '  
	end


-- DROPPING TEMPORARY TABLES ....  
set @sqlquery2 = @sqlquery2 + '  
  
drop table #MainQuery   
drop table #finalQuery
drop table #secondQuery
'  
                                      
--print @sqlquery + @sqlquery2  

-- CONCATENATING THE DYNAMIC SQL..
set @sqlquery = @sqlquery + @sqlquery2  

-- EXECUTING THE DYNAMIC SQL ....
exec (@sqlquery)  
  
  
GO 
GRANT EXECUTE ON [USP_MAILER_Send_Immigration_Data] TO webuser;
Go
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


-- =============================================
-- Author:		Afaq Ahmed
-- Create date: 05/25/2010
-- Business Logic:	This SP will return the already processed email messages' unique IDs
-- =============================================

CREATE PROCEDURE [dbo].[USP_HTP_Get_ProcessedEmail]
AS

SELECT * FROM ProcessedEmails ORDER BY InsertDate DESC

GO
GRANT EXECUTE ON [dbo].[USP_HTP_Get_ProcessedEmail] TO dbr_webuser
GO




USE TrafficTickets
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rab Nawaz Khan
-- Task ID:		11473
-- Create date: 10/23/2013
-- Description:	This Store Procedure is used to insert the caller Id information against the clients.
-- =============================================
ALTER PROCEDURE USP_HTP_InsertLeadCallerIdInfo 
	@callerId VARCHAR(100) = NULL,
	@isUnknonwn BIT = 0,
	@ticketId INT,
	@empId INT
AS
BEGIN
	SET NOCOUNT ON;
	
	IF (@isUnknonwn = 1)
	BEGIN
		SET @callerId = NULL
	END
	-- If ticketId not exits then insert the record
	IF((SELECT COUNT(RowID) FROM tblCallerIdsFromClients WHERE ticketId = @ticketId) = 0)
	BEGIN
		INSERT INTO tblCallerIdsFromClients( CallerId,isUnknown, recdate, ticketId, insertedBy )
		VALUES (@callerId, @isUnknonwn, GETDATE(), @ticketId, @empId)
		
		IF (@isUnknonwn = 1)
		BEGIN
			INSERT INTO tblTicketsNotes (TicketID,[Subject],Recdate,EmployeeID)
			VALUES(@ticketId,'Caller ID added � Unknown',GETDATE(),@empId)
		END
		ELSE
		BEGIN
			INSERT INTO tblTicketsNotes (TicketID,[Subject],Recdate,EmployeeID)
			VALUES(@ticketId,'Caller ID added - ' + @callerId,GETDATE(),@empId)
		END
	END
	ELSE
	BEGIN
		DECLARE @existingCallerId VARCHAR(150)
		DECLARE @existingIsUnknonwn BIT
		
		SELECT @existingCallerId = ISNULL(CallerId, ''), @existingIsUnknonwn = ISNULL(isUnknown, 0)
		FROM tblCallerIdsFromClients WHERE ticketId = @ticketId
		
		IF (@existingIsUnknonwn = 1)
		BEGIN
			SET @existingCallerId = 'Unknown'
		END
		-- If record exists but but recdate is null then update all fields 
		IF((SELECT COUNT(RowID) FROM tblCallerIdsFromClients WHERE ticketId = @ticketId AND recdate IS NULL) > 0)
		BEGIN
				UPDATE tblCallerIdsFromClients
				SET CallerId = @callerId, isUnknown = @isUnknonwn, insertedBy = @empId, recdate = GETDATE()
				WHERE ticketId = @ticketId
		END
		ELSE
			BEGIN	
				UPDATE tblCallerIdsFromClients
				SET CallerId = @callerId, isUnknown = @isUnknonwn
				WHERE ticketId = @ticketId
			END
			
			IF ((@existingIsUnknonwn = 1 AND @isUnknonwn = 0) OR (@existingCallerId NOT LIKE @callerId))
			BEGIN
				INSERT INTO tblTicketsNotes (TicketID,[Subject],Recdate,EmployeeID)
				VALUES(@ticketId,'Caller ID changed From ['+ @existingCallerId +'] To ['+ @callerId +']',GETDATE(),@empId)	
			END
			IF(@existingIsUnknonwn = 0 AND @isUnknonwn = 1)
			BEGIN
				INSERT INTO tblTicketsNotes (TicketID,[Subject],Recdate,EmployeeID)
				VALUES(@ticketId,'Caller ID changed From ['+ @existingCallerId +'] To [Unknown]',GETDATE(),@empId)	
			END
	END
END
GO
	GRANT EXECUTE ON USP_HTP_InsertLeadCallerIdInfo TO dbr_webuser
GO


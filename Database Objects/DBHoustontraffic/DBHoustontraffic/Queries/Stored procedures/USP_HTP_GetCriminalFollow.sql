﻿/****** Object:  StoredProcedure [dbo].[USP_HTP_GetCriminalFollow]    Script Date: 05/15/2008 11:44:33 ******/      
/*        
Created By : Noufil Khan        
Business Logic : This procedure display information about all those clients whose cases are in criminal courts      
    and their flag is active and their status must be waiting.      
Parameter:       
   @courtid  : searching criterea with respect to court      
   @datefrom : Date range that starts from      
   @dateto   : Date range that end to      
   @showAll  : For showing all records in validation email      
      
*/      
  --[USP_HTP_GetCriminalFollow] -1,' ',' ',1
  -- Noufil 4432 08/05/2008 html serial number added added for validation report  
  
ALTER PROCEDURE [dbo].[USP_HTP_GetCriminalFollow]
	@courtid INT,
	@datefrom DATETIME,
	@dateto DATETIME,
	@showAll BIT = 0,
	@ValidationCategory int=0 -- Saeed 7791 07/10/2010 @ValidationCategory=1 for alert & @ValidationCategory=2 for report
	
AS
	SELECT DISTINCT 
	       t.TicketID_PK,
	       tv.ticketsviolationid,
	       tv.casenumassignedbycourt AS CAUSENUMBER,
	       tv.RefCaseNumber AS TICKETNUMBER,
	       t.Lastname AS LASTNAME,
	       t.Firstname AS FIRSTNAME,
	       dbo.fn_dateformat(t.DOB, 27, '/', ':', 1) AS DOB,
	       t.DLNumber AS DL,
	       dbo.fn_dateformat(tv.CourtDateMain, 30, '/', ':', 1) AS COURTDATE,
	       tv.CourtnumberMain AS COURTNO,
	       vs.ShortDescription AS STAT,
	       c.ShortName AS LOC,
	       F.FirmAbbreviation AS COV,
	       tv.CourtID AS courtid,
	       CASE 
	            WHEN t.bondflag = 1 THEN 'B'
	            ELSE ''
	       END AS bflag,
	       dbo.fn_dateformat(t.Criminalfollowupdate, 27, '/', ':', 1) AS 
	       FollowUpDate,
	       CASE 
	            WHEN t.ActiveFlag = 1 THEN 0
	            ELSE 1
	       END AS ActiveFlag,
	       '<a href="http://ln.legalhouston.com/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber='
	       + CONVERT(VARCHAR(30), t.TicketID_PK) + '" >' + CONVERT(VARCHAR(30), ROW_NUMBER() OVER(ORDER BY t.TicketID_PK))
	       + '</a>' AS link,
	       	--Muhammad Ali 8031 07/16/2010 --> Add four Columns.. two for sorting and 2 for info
			CONVERT(VARCHAR,tv.CourtDateMain,101)	AS CrtDate,
			tv.CourtDateMain AS sortcourtdate,
			 CASE
			 WHEN CONVERT(INT,DATEDIFF(DAY,tv.CourtDateMain,GETDATE()))< 0 THEN '  ' 
			 ELSE CONVERT(VARCHAR, DATEDIFF(DAY,tv.CourtDateMain,GETDATE()) )
			END AS 	pastdue	,
			DATEDIFF(DAY,tv.CourtDateMain,GETDATE()) AS sortedPastDue
	FROM   dbo.tblTickets AS t
	       INNER JOIN dbo.tblTicketsViolations AS tv
	            ON  t.TicketID_PK = tv.TicketID_PK
	       INNER JOIN dbo.tblCourtViolationStatus AS vs
	            ON  tv.CourtViolationStatusIDmain = vs.CourtViolationStatusID
	       INNER JOIN dbo.tblCourts AS c
	            ON  tv.CourtID = c.Courtid
	       LEFT OUTER JOIN tblfirm F
	            ON  F.FirmID = ISNULL(tv.CoveringFirmID, 3000)
	       LEFT OUTER JOIN tblticketsextensions tx
	            ON  t.ticketid_pk = tx.ticketid_pk
	WHERE  c.iscriminalcourt = 1
	       AND t.Activeflag = 1
	       AND (@CourtId = -1 OR tv.CourtId = @CourtId)
	       AND (
	               (
	                   (@showAll = 1 OR @ValidationCategory=2)   -- Saeed 7791 07/10/2010 display all records if showAll=1 or validation category is 'Report'
	                   AND DATEDIFF(DAY, ISNULL(t.Criminalfollowupdate, '1/1/1900'), GETDATE()) 
	                       >= 0
	               )
	               OR -- ozair 4389 07/12/2008 chnaged court date to Follow Up Date   
	                  (
	                  	  --Sarim 7066 02/26/2010 included @showAll check
	                      (@showAll = 0 OR @ValidationCategory=1)	-- Saeed 7791 07/09/2010 display  if showAll=0 or validation category is 'Alert' then display records which has 'follow up date' null,past or today.
	                      AND DATEDIFF(DAY, t.Criminalfollowupdate, @datefrom) <
	                          = 0
	                      AND DATEDIFF(DAY, t.Criminalfollowupdate, @dateto) >= 
	                          0
	                  ) 
	                  -- end ozair 4389
	           )
	       AND tv.CourtViolationStatusIDmain = 104
	GROUP BY
	       t.ticketid_pk,
	       tv.casenumassignedbycourt,
	       tv.refcasenumber,
	       t.lastname,
	       t.firstname,
	       t.dob,
	       t.dlnumber,
	       tv.courtdatemain,
	       tv.courtnumbermain,
	       vs.shortdescription,
	       c.shortname,
	       t.activeflag,
	       tv.ticketsviolationid,
	       t.bondflag,
	       F.FirmAbbreviation,
	       t.Criminalfollowupdate,
	       tv.CourtID 
	       --Sabir Khan 5297 12/03/2008 sort by follow up date...
	ORDER BY
	       FollowUpDate  
    
    
    
  
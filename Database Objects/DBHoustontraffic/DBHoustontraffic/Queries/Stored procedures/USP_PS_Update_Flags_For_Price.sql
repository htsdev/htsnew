SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_PS_Update_Flags_For_Price]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_PS_Update_Flags_For_Price]
GO





Create Procedure [dbo].[USP_PS_Update_Flags_For_Price]
(

@ticketID int,
@AccFlag int,
@CDLFlag int

)

as

Update tbltickets 
set 
AccidentFlag = @AccFlag,
CDLFlag = @CDLFlag
where ticketid_pk = @ticketID





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


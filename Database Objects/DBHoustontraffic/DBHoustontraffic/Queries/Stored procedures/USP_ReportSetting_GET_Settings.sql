/************************************************************
 * Code formatted by SoftTree SQL Assistant © v4.0.34
 * Time: 9/27/2010 12:29:18 PM
 ************************************************************/

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/*  
* Created By		: SAEED AHMED
* Task ID			: 8101  
* Created Date		: 09/27/2010  
* Business logic	: This procedure is used get report setting based on report id.
*					: 
* Parameter List  
* @Report_ID		: Report ID
* 
* USP_ReportSetting_GET_Settings 52
*/
CREATE PROC [dbo].[USP_ReportSetting_GET_Settings]
@Report_ID INT
AS
SELECT ReportSetting.ReportSettingId,
       tbl_Report.Report_ID AS ReportId,
       tbl_Report.Report_Name AS ReportName,
       tblCourts.Courtid,
       tblCourts.CourtName,
       ReportAttribute.ShortName,
       ReportAttribute.AttributeID,
       ReportAttribute.AttributeName,
       ReportSetting.AttributeValue,
       ReportAttribute.AttributeType
FROM   ReportAttribute
       INNER JOIN ReportSetting
            ON  ReportAttribute.AttributeID = ReportSetting.AttributeId
       INNER JOIN tblCourts
            ON  ReportSetting.CourtID = tblCourts.Courtid
       INNER JOIN tbl_Report
            ON  ReportSetting.ReportID = tbl_Report.Report_ID
WHERE  tbl_Report.Report_ID = @Report_ID
GO

GRANT EXECUTE ON [dbo].[USP_ReportSetting_GET_Settings] TO dbr_webuser
GO


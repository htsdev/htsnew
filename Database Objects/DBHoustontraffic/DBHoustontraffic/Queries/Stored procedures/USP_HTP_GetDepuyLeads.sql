﻿set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


/************************************************************************************************
--Created By   : Afaq Ahmed
--Creation Data: 12/09/2010
--Task ID :8612
--Parameter List	
--@showAllRecord : If true than show all record. 
--@FromDate : From Date
--@ToDate : To Date
--Return Column:
--Serial Number, First Name,Last Name,Address1,Phone1,Email,DOB,SurgeryDate,
--Manufacturer, ProblemDescription,RecDate
--Business Logic : Display depuy leads contact us information on the basis of contact date range.
--Updated By: Muhammad Nadir Siddiqui 8759 01/06/2010
--Added Columns: Manufacturer[Referrenced from Manufacturers table], IsRecalled, IsPain  
 ************************************************************************************************/



ALTER PROCEDURE [dbo].[USP_HTP_GetDepuyLeads]
	@fromDate DATETIME = '01/01/1900',
	@toDate DATETIME = '12/30/2010',
	@ShowAllRecords BIT = 0
AS
	SELECT ROW_NUMBER() OVER(ORDER BY dpcu.RecDate DESC) AS 'SNo',
	       dpcu.FirstName,
	       dpcu.LastName,
	       dpcu.Address1,
	       --Ozair 8759 01/07/2011 modified clause
	       case when isnull(dpcu.Phone1,'')='' THEN '' ELSE dbo.fn_FormatContactNumber_new(dpcu.Phone1, -1) + CASE WHEN ISNULL(dpcu.Phone1Type,'')='' THEN '' ELSE  '(' + dpcu.Phone1Type + ')' END  end AS 'Phone1',
	       dpcu.Email,
	       CASE CONVERT(VARCHAR(12), dpcu.DOB, 101)
	            WHEN '01/01/1900' THEN ''
	            ELSE CONVERT(VARCHAR(12), dpcu.DOB, 101)
	       END AS 'DOB',
	       CASE CONVERT(VARCHAR(12), dpcu.SurgeryDate, 101)
	            WHEN '01/01/1900' THEN ''
	            ELSE CONVERT(VARCHAR(12), dpcu.SurgeryDate, 101)
	       END AS 'SurgeryDate',
	       isnull(dpcu.Manufacturer,'') AS Manufacturer,
	       dpcu.ProblemDescription,
	       dpcu.RecDate,
	       --Ozair 8759 01/07/2011 added two more column
	       isnull(dpcu.IsRecalled,'') AS Recalled,
           isnull(dpcu.IsPain,'') AS Pain,
           -- Muhammad Nadir Siddiqui 8961 03/16/2011 added two columns
           isnull(dpcu.PracticeAreaId,'') AS PracticeAreaId,
           isnull(dpcu.PainLevel,'') AS PainLevel,
           pa.PracticeAreaDescription AS PracticeArea
	FROM   SulloLaw.dbo.DePuyContactUs dpcu
	LEFT JOIN SulloLaw.dbo.PracticeAreas pa
	ON pa.PracticeAreaId = dpcu.PracticeAreaId
	
	
	WHERE  @ShowAllRecords = 1
	       OR  (
	               @ShowAllRecords = 0
	               AND DATEDIFF(DAY, @FromDate, RecDate) >= 0
	               AND DATEDIFF(DAY, @toDate, RecDate) <= 0
	           )
	
	
	



SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_WebScan_InsertPending_NEW]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_WebScan_InsertPending_NEW]
GO


CREATE procedure dbo.usp_WebScan_InsertPending_NEW        
                        
@PicID as int,                        
@CheckStatus as int   ,        
@Location as varchar(50),      
@Status as varchar(50),    
@OcrData as nvarchar(4000),
@TypeScan as varchar(50)=''    
                  
as                        
                        
Insert into tbl_WebScan_OCR                        
(                         
 PicID,                        
 CheckStatus ,      
 Location,      
 Status,    
 OCRData,
 TypeScan           
                  
)                        
values                        
(                         
 @PicID ,                        
 @CheckStatus,      
 @Location,      
 @Status,    
 @OCRData,
 @TypeScan        
)       


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


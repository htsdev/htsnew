﻿/***************************************************************************************
 * CREATED BY		: Abid Ali
 * CREATED DATE		: 01/06/2009
  * Task Id			: 5359
 * 
 * Business Logic	: Return list of all lor batch history records group by ticket id
 * 
 * Parameter List	:
 *						@CertifiedMailNumber	: Certified Mail Number	
 *						
 * Columns Return	:
 *						SNo
 *						TicketID_PK
 *						FullName
 *						refcasenumber
 *						languagespeak
 *						attn
 *						Attorneyname
 *						judgename
 *						courtid
 *						courtname
 *						address
 *						cityzip
 *						causenumbers
 *						USPSTrackingNumber
 *						FormattedTrackingNumber
 *********************************************************************************/

ALTER PROCEDURE [dbo].[USP_HTP_GET_LORBatchHistoryGroupByTicketID]
(@CertifiedMailNumber VARCHAR(20))
AS
	SELECT CONVERT(VARCHAR, ROW_NUMBER() OVER(ORDER BY TicketID_PK ASC)) + ')' AS 
	       SNo,
	       t.TicketID_PK,
	       t.FullName,
	       t.BatchID_FK,
	       t.languagespeak,
	       t.attn,
	       t.Attorneyname,
	       t.firmsubtitle, -- abid 5544 02/17/2009 column for subtitle
	       t.judgename,
	       t.courtid,
	       t.courtname,
	       t.address,
	       t.cityzip,
	       t.USPSTrackingNumber,
	       t.FormattedTrackingNumber,
	       dbo.FN_GET_ConcatedRefCaseNumbersFromLORBatchHistory(t.BatchID_FK) AS 
	       refcasenumber
	FROM   (
	           SELECT DISTINCT dbo.tblLORBatchHistory.TicketID_PK,
	                  dbo.tblLORBatchHistory.FullName,
	                  dbo.tblLORBatchHistory.BatchID_FK,
	                  dbo.tblLORBatchHistory.languagespeak,
	                  dbo.tblLORBatchHistory.attn,
	                  dbo.tblLORBatchHistory.Attorneyname,
	                  dbo.tblLORBatchHistory.firmsubtitle, -- abid 5544 02/17/2009 column for subtitle
	                  dbo.tblLORBatchHistory.judgename,
	                  dbo.tblLORBatchHistory.courtid,
	                  dbo.tblLORBatchHistory.courtname,
	                  dbo.tblLORBatchHistory.address,
	                  dbo.tblLORBatchHistory.cityzip,
	                  dbo.tblHTSBatchPrintLetter.USPSTrackingNumber,
	                  (
	                      SUBSTRING(dbo.tblHTSBatchPrintLetter.USPSTrackingNumber, 1, 4) 
	                      +
	                      '-' + SUBSTRING(dbo.tblHTSBatchPrintLetter.USPSTrackingNumber, 5, 4) 
	                      + '-' + SUBSTRING(dbo.tblHTSBatchPrintLetter.USPSTrackingNumber, 9, 4)
	                      + '-' + SUBSTRING(dbo.tblHTSBatchPrintLetter.USPSTrackingNumber, 13, 4) 
	                      + '-' + SUBSTRING(dbo.tblHTSBatchPrintLetter.USPSTrackingNumber, 17, 4)
	                  ) AS FormattedTrackingNumber
	           FROM   dbo.tblLORBatchHistory
	                  LEFT OUTER JOIN dbo.tblHTSBatchPrintLetter
	                       ON  dbo.tblLORBatchHistory.BatchID_FK = dbo.tblHTSBatchPrintLetter.BatchID_PK
	                  LEFT OUTER JOIN tblUsers tu
	                       ON  dbo.tblHTSBatchPrintLetter.PrintEmpID = tu.EmployeeID
	           WHERE  dbo.tblHTSBatchPrintLetter.USPSTrackingNumber = @CertifiedMailNumber
	       ) t
	       --Faique Ali 11023 7/11/2013 to set ordering in rpt
	ORDER BY t.BatchID_FK ASC
GO

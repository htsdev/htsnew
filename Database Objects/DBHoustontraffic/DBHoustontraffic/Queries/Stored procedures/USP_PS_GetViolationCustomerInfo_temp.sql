        
alter procedure [dbo].[USP_PS_GetViolationCustomerInfo_temp]    
                               
@RecordId int                  
                  
as                               
                          
Select  T.TicketNumber_pk as TicketNumber ,                           
    TV.midnumber,                                   
    TV.DLNumber,                                   
    T.ViolationNumber_PK,                                   
    T.ViolationCode,                                   
    T.ViolationDescription,                                   
    T.FineAmount,                                   
    TV.FirstName,                                   
    TV.LastName,                                   
    TV.Address1,                                    
    TV.Address2,                                   
	t.courtlocation as courtid,              
    TV.Gender,                                   
    TV.officerNumber_Fk,                                   
    TV.City,                                   
    TV.StateID_FK,                              
    S.State,                                    
    TV.ZipCode,                                  
    TV.PhoneNumber,                                   
    TV.DOB,                                   
    TV.ViolationDate,                                   
    isnull(T.CourtDate,'1/1/1900') as CourtDate ,          
	TV.OfficerName,                                   
    TV.Race,                                   
    TV.AssociatedCaseNumber,                                   
    T.CourtNumber,                                   
    TV.Courttime,                              
    TV.ListDate ,                              
    TV.RecordID ,                             
	PlanId = (                              
		select top 1 planid from tblpriceplans p                               
		where tv.listdate between p.effectivedate and p.enddate                              
		and p.isactiveplan = 1                               
		and p.courtid = tv.courtid                              
		) ,                            
    BondFlag = ( case when t.courtdate < getdate() then 1 else 0 end ),                    
	isnull(T.BondAmount,0) as BondAmount  ,            
	dbo.tblDateType.TypeID,
	isnull(t.ftalinkid,0) as ftalinkid            
 into #temp                            
from             
   dbo.tblState AS S RIGHT OUTER JOIN            
         dbo.tblTicketsViolationsArchive AS T INNER JOIN            
         dbo.tblTicketsArchive AS TV ON TV.RecordID = T.RecordID INNER JOIN            
         dbo.tblDateType INNER JOIN            
         dbo.tblCourtViolationStatus AS CVS ON dbo.tblDateType.TypeID = CVS.CategoryID ON T.violationstatusid = CVS.CourtViolationStatusID ON             
         S.StateID = TV.StateID_FK            
 where t.recordid = @recordid                         
------------------------------------------------                            
-- Finding Other FTA Cases Associated with the current case --  
  
  
Declare @FTALinkID as int   
Set @FTALinkID=0  
  
Select Top 1 @FTALinkID = fTALinkID  from #temp where ftalinkid > 0
  
  
if @FTALinkID > 0   
Begin  
 -- Find Associated FTA Tickets   
   
insert into  #temp   
  
 Select  T.TicketNumber_pk as TicketNumber ,                           
    TV.midnumber,                                   
    TV.DLNumber,        
    T.ViolationNumber_PK,                                   
    T.ViolationCode,                                   
    T.ViolationDescription,                                   
    T.FineAmount,                                   
    TV.FirstName,                                   
    TV.LastName,                                   
    TV.Address1,                                    
    TV.Address2,                                   
	t.courtlocation as courtid,              
    TV.Gender,                                   
    TV.officerNumber_Fk,                                   
    TV.City,                                   
    TV.StateID_FK,                              
    S.State,                                    
    TV.ZipCode,                                  
    TV.PhoneNumber,                                   
    TV.DOB,                                   
    TV.ViolationDate,                                   
    isnull(T.CourtDate,'1/1/1900') as CourtDate ,          
	TV.OfficerName,                                   
    TV.Race,                                   
    TV.AssociatedCaseNumber,                                   
    T.CourtNumber,                                   
    TV.Courttime,                              
    TV.ListDate ,                              
    TV.RecordID ,                             
	PlanId = (                              
		select top 1 planid from tblpriceplans p                               
		where tv.listdate between p.effectivedate and p.enddate                              
		and p.isactiveplan = 1                               
		and p.courtid = tv.courtid                              
		) ,                            
    BondFlag = ( case when t.courtdate < getdate() then 1 else 0 end ),                    
	isnull(T.BondAmount,0) as BondAmount  ,            
	dbo.tblDateType.TypeID ,
	isnull(t.ftalinkid,0) as ftalinkid              
from             
	dbo.tblState AS S RIGHT OUTER JOIN            
    dbo.tblTicketsViolationsArchive AS T INNER JOIN            
	dbo.tblTicketsArchive AS TV ON TV.RecordID = T.RecordID INNER JOIN            
	dbo.tblDateType INNER JOIN            
	dbo.tblCourtViolationStatus AS CVS ON dbo.tblDateType.TypeID = CVS.CategoryID ON T.violationstatusid = CVS.CourtViolationStatusID ON             
	S.StateID = TV.StateID_FK            
where t.recordid <>  @recordid  and FTALinkID = @FTALinkID  and  TV.ClientFlag = 0 
  
End  
  
  
  
--------------------------------Added By Zeeshan Ahmed-----------------------     
------------------------------- Modify On 31st August 2007--------------------                          
Select Distinct  t.TicketNumber , t.ViolationDate,  t.CourtDate,  t.CourtNumber,  t.Courttime , t.ViolationDescription , tc.courtname , tc.address  from #temp t       
join tblcourts  tc       
on t.courtid = tc.courtid      
------------------------------------------------------------------------------      
      
                          
select Distinct * from #temp                            
drop table #temp                          




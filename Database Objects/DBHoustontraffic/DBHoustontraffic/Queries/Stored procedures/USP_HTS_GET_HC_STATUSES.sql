SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_GET_HC_STATUSES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_GET_HC_STATUSES]
GO


CREATE PROCEDURE dbo.USP_HTS_GET_HC_STATUSES      
AS      
SELECT distinct status FROM tbl_HTS_HCStatuses

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


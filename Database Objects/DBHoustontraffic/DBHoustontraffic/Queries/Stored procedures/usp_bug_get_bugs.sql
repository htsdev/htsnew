SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_bug_get_bugs]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_bug_get_bugs]
GO

  
  
CREATE procedure [dbo].[usp_bug_get_bugs] --43      
      
@bugid as int      
      
as      
      
select b.shortdescription,      
    b.statusid,      
    b.priorityid,      
    isnull(b.pageurl,'')as pageurl,      
    isnull(b.ticketid,'') as ticketid,  
 b.developerid,
	b.bug_id      
        
        
from tbl_bug b      
      
where       
b.bug_id=@bugid      
      
  
  
  
  
 

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


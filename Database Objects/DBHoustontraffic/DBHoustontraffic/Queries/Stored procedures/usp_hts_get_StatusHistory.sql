
/************************
Business Logic: This stored procedure is used to get all records from tblticketsviolationlog which are loged against given ticketid.

Input Parameters:
@TicketID: Ticketid Associated with case.

*************************/  
--	

ALTER Procedure [dbo].[usp_hts_get_StatusHistory]  

@TicketID int  
  
AS  

set nocount ON
SELECT     tv.RefCaseNumber, tvl.newverifiedDate, tvl.oldverifiedDate, tvl.Recdate, tcs1.Description AS newverifiedstatus, c.ShortName, 
                      tcs2.Description AS oldverifiedstatus, tvl.newscanDate, tvl.oldscanDate, tcs3.Description AS newscanstatus, tcs4.Description AS oldscanstatus, 
                      tvl.newautoDate, tvl.oldautoDate, tsc6.Description AS newautostatus, tcs5.Description AS oldautostatus, tvl.NewverifiedRoom, tvl.oldverifiedRoom, 
                      tvl.newscanroom, tvl.oldscanroom, tvl.newautoroom, tvl.oldautoroom
FROM         dbo.tblTicketsViolationLog AS tvl WITH(NOLOCK) INNER JOIN
                      dbo.tblTicketsViolations AS tv WITH(NOLOCK) ON tvl.TicketviolationID = tv.TicketsViolationID LEFT OUTER JOIN
                      dbo.tblCourtViolationStatus AS tcs1 WITH(NOLOCK) ON tvl.newverifiedstatus = tcs1.CourtViolationStatusID INNER JOIN
                      --Yasir Kamal 6029 06/16/2209 tblticketsviolationlog column courtid has been renamed to newcourtid
                      dbo.tblCourts AS c WITH(NOLOCK) ON tvl.newCourtid = c.Courtid AND tv.TicketID_PK = @TicketID INNER JOIN
                      dbo.tblCourtViolationStatus AS tcs5 WITH(NOLOCK) ON tvl.oldautostatus = tcs5.CourtViolationStatusID LEFT OUTER JOIN
                      dbo.tblCourtViolationStatus AS tcs3 WITH(NOLOCK) ON tvl.newscanstatus = tcs3.CourtViolationStatusID LEFT OUTER JOIN
                      dbo.tblCourtViolationStatus AS tsc6 WITH(NOLOCK) ON tvl.newautostatus = tsc6.CourtViolationStatusID LEFT OUTER JOIN
                      dbo.tblCourtViolationStatus AS tcs2 WITH(NOLOCK) ON tvl.oldverifiedstatus = tcs2.CourtViolationStatusID LEFT OUTER JOIN
                      dbo.tblCourtViolationStatus AS tcs4 WITH(NOLOCK) ON tvl.oldscanstatus = tcs4.CourtViolationStatusID
ORDER BY tvl.Recdate DESC
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_msmq_insertmessage_ver1]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_msmq_insertmessage_ver1]
GO

  
CREATE procedure [dbo].[usp_msmq_insertmessage_ver1]    
    
@Batchid int,    
@Reset  varchar(100),    
@CauseNo varchar(50),    
@Message varchar(200)
    
as    
    
insert into tbl_webscan_msmqlog(batchid,message,msgdate,reset,causeno)    
values(@Batchid,@Message,getdate(),@Reset,@CauseNo)    
    

    
  

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


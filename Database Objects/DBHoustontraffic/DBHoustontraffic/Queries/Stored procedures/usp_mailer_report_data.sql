SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_mailer_report_data]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_mailer_report_data]
GO








CREATE proc [dbo].[usp_mailer_report_data]
	(
	@crtid int,                
	@lettertype int,                
	@startListdate varchar(10),                
	@endListdate varchar(10)          
	)
as                
                
SELECT  distinct                 
	tbl.BatchID,                
	ta.MidNumber,                
	ta.FirstName,                 
	ta.LastName,                
	ta.Address1,                
	tln.RecordID,                 
	tbl.PExpense,                
	tbl.LCount,                 
	ta.Clientflag,                 
	isnull(ta.donotmailflag,0)donotmailflag,                
	tbl.BatchSentDate,                
	tln.LetterType,                
	tln.listdate,                
	ta.ticketnumber                
into #temp                
FROM    tblTicketsArchive ta 
INNER JOIN                
        tblLetterNotes tln 
ON 	ta.RecordID = tln.RecordID 
INNER JOIN                
        tblBatchLetter tbl 
ON 	tln.BatchId_Fk = tbl.BatchID                
where 	ta.ClientFlag=0                
and  	isnull(Donotmailflag,0)=0                
and 	tln.LetterType=@lettertype                
and 	tbl.courtid in( 
		Select courtid from tblcourts where courtcategorynum= @crtid
		)                
and 	tbl.listdate  between convert (datetime,@startListdate) and convert ( datetime,@endListdate) 
                
                
Select distinct 
	tt.TicketID_PK, 
	tt.ticketnumber_pk, 
	tt.Midnum,
	tt.FirstName,
	tt.LastName,
	tt.Address1,
	activeflag,
	BatchSentDate,
	donotmailflag                
into 	#temp1                
from 	Tbltickets tt 
inner join 
	#temp t                
on 	tt.firstname=t.firstname                
and 	tt.lastname=t.lastname                
and 	tt.address1=t.address1                
where 	tt.currentcourtloc in( 
		Select courtid from tblcourts where courtcategorynum= @crtid
		)                
                
                
----This Is For Quote Clients------------                
Select distinct count( distinct recordid) QuoteClients ,#temp.batchSentdate                
into #temp2                
from	#temp 
inner join 
	#temp1                
on 	#temp.BatchSentDate=#temp1.BatchSentDate                
where   clientflag=1                
and 	activeflag=0                
group by 
	#temp.batchSentdate;                
                
----Clients------------                
Select distinct count( distinct recordid) Clients ,#temp.batchSentdate                
into	#temp3                
from 	#temp 
inner join 
	#temp1                
on 	#temp.BatchSentDate=#temp1.BatchSentDate                
where   clientflag=1                
and  	activeflag=1                
group by 
	#temp.batchSentdate;                
                
----NonClients------------                
Select count( distinct recordid) NonClients ,#temp.batchSentdate                
into #temp4                
from 	#temp 
inner join 
	#temp1                
on 	#temp.BatchSentDate=#temp1.BatchSentDate                
where   clientflag=0                
group by 
	#temp.batchSentdate;                
                
SELECT	distinct  BatchSentDate,                        
        SUM(dbo.tblTicketsPayment.ChargeAmount) TotalAmountCharged        
into #temp5                
FROM	#temp1 
INNER JOIN                
       tblTicketsPayment 
ON	#temp1.TicketID_PK = dbo.tblTicketsPayment.TicketID                
WHERE   tblTicketsPayment.PaymentVoid = 0 
AND 	dbo.tblTicketsPayment.PaymentType NOT IN (99, 100)                 
AND 	#temp1.Activeflag = 1                
GROUP BY 
	BatchSentDate
                
Select 	distinct 
	convert ( varchar(10),#temp.Listdate,101) Listdate ,                
       	#temp.LCount,                
      	convert ( varchar(10), #temp.batchsentdate,101) BatchSentdate,                
       	isnull(#temp3.Clients,0) Clients,                
       	isnull (#temp2.QuoteClients,0)QuoteClients,                
       	isnull ( #temp4.NonClients,0)NonClients,                
      	cast ( convert (varchar (50),PExpense*LCount ) as  Decimal)as TotalMailingCost,                
      	cast( convert (varchar (50), isnull(#temp5.TotalAmountCharged , 0)) as Decimal ) RevenueGenerated,                
      	cast ( convert (varchar(50), isnull (TotalAmountCharged- PExpense*LCount,0))as Decimal )  ProfitGenerated                
from	#temp 
left outer join 
	#temp2                
on 	#temp.batchsentdate=#temp2.batchsentdate                
left outer join 
	#temp3                
on 	#temp.batchsentdate=#temp3.batchsentdate                
left outer join 
	#temp4                
on 	#temp.batchsentdate=#temp4.batchsentdate                
left outer join 
	#temp5                
on 	#temp.batchsentdate=#temp5.batchsentdate                
                
                
                
drop table #temp;                
drop table #temp1;                
drop table #temp2;                
drop table #temp3;                
drop table #temp4;                
drop table #temp5;                
              
            
          
        
      
                
                
              
            
          
        
      
        
      
      
              
              
              
              
              
              
              
              
              
              
            
          
        
      
    
  
  

        
      
    
  
    
  
  










GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


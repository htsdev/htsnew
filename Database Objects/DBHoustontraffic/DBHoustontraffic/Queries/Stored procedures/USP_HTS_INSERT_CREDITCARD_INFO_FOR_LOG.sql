/*******

Created By: Noufil Khan 3961 07/16/2008 

Business Logic: This procedure is used to save credit card transacition
 
Parameter : 
			@responsereasoncode : Save Response Reason Code
 
*******/

ALTER procedure [dbo].[USP_HTS_INSERT_CREDITCARD_INFO_FOR_LOG]  --1067,'xxccbbddffee','0','','0101','','','','',''                    
          
@Ticketid int,                        
@invnumber varchar(200),                        
@status varchar(2),                        
@errmsg varchar(255),                        
@transnum varchar(50),                        
@response_subcode  varchar(20),                            
@response_reason_code   varchar(20),                         
@response_reason_text varchar(100),                         
@auth_code   varchar(6),                                   
@avs_code varchar(1),          
@approvedflag int,                        
@amount money,                        
@ccnum varchar(20),          
@expdate varchar(8),                        
@name varchar(50),                        
@cin varchar(4),                
@cardtype varchar(20),                       
@paymentmethod as varchar(50),                              
@empid int,          
@RetCode int,          
@ClearFlag int,          
@TransType int,                           
@IsVoid bit = 0,    
@RefTransNum varchar(50) = '',
@responsecode int=0  -- Noufil 3961 New parameter Response_Code added 
as          
                         
begin                
                
declare @crtype as int          
          
select @crtype=case @cardtype                
when 'Visa' then 1                
when 'MasterCard' then 2                
when 'American Express' then 3                
when 'Discover' then 4        
else  convert(int , @cardtype)              
end                 
             
-- insert into tblticketscybercashlog                        
-- (ticketid_pk,invnumber,amount,ccnum,expdate,name,cin,cardtype,status,transnum,errmsg,approvedflag,response_subcode,                
-- response_reason_text,auth_code,avs_code,transType )                          
          
INSERT INTO tblTicketsCybercashLog          
           (TicketID ,invnumber ,amount ,ccnum ,expdate ,name ,Recdate,status,transnum,retcode,errmsg,EmployeeID,          
   Approvedflag,clearflag,cin,CardNumber,response_subcode,response_reason_code,response_reason_text,auth_code,avs_code,CardType,transType,IsVoid,RefTransNum,Response_Code)          
           
values(@Ticketid,@invnumber,convert(money,@amount),@ccnum,@expdate,@name, getdate(),@status,@transnum,@RetCode,@errmsg,@empid,          
  @approvedflag,@ClearFlag,@cin,@ccnum,@response_subcode,@response_reason_code,@response_reason_text,@auth_code,@avs_code,@crtype,@TransType,@IsVoid,@RefTransNum,@responsecode )          
          
end              
                
      
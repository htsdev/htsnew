set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


/*************************
Business Logic: This stored procedure is used by HTP/Matter/History to display Auto Info History of case.

Input Parameters: 
@TicketId_PK : Ticketid associated with case.

*************************/
--USP_HTP_Get_AutoStatusHistory 4369
ALTER procedure [dbo].[USP_HTP_Get_AutoStatusHistory] --125017

@TicketId_PK int              
                 
AS             

--Yasir Kamal 6029 06/16/2009 display user LastName and courtShortName in AutoInfoHistory Section               
select 
tv.TicketId_Pk, tv.RefCaseNumber, tv.TicketsViolationID, 
--Yasir Kamal 6277 08/03/2009 add cause Number Column
tvLog.RecDate, 
-- Adil 8303 09/23/2010 Sugarland Events Court Status Code added.
ISNULL(cvs.Description, '') + CASE WHEN ISNULL(tvlog.SugarlandEvtDataFileStatus, '') = '' THEN '' ELSE '(' + tvlog.SugarlandEvtDataFileStatus + ')' END AS Description,
tvLog.NewAutoDate,tvLog.NewAutoRoom,tu.Lastname,tc.ShortName,tv.casenumassignedbycourt

from tblTicketsViolationLog tvLog
inner join tblTicketsViolations tv on tvLog.TicketViolationID = tv.TicketsViolationID
inner join tblCourtViolationStatus cvs on tvLog.NewAutoStatus = cvs.CourtViolationStatusId
LEFT OUTER JOIN tblUsers tu ON tvlog.EmployeeID = tu.EmployeeID
LEFT OUTER JOIN tblCourts tc ON tvlog.newCourtid = tc.Courtid
Where (tv.CourtID = 3060 OR (NewAutoStatus != OldAutoStatus) -- Adil 09/01/2010 8221 for Sugar land data do not check New and Old comparison.
	OR (NewAutoDate != OldAutoDate)
	OR (NewAutoRoom != OldAutoRoom)
	)
AND tv.TicketId_Pk = @TicketId_PK
order by RecDate DESC

﻿USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTP_GetAllCriminalViolationsForSharePoint]    Script Date: 06/24/2008 15:34:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******     
Created by: Aziz    
Business Logic : Get all the violations which can be added in calender provided     
     case is active    
     violation belongs to a criminical court    
     violation status is not "NO hire" and "Dispose"    
     Court Date should be in future    
     
List of Columns:    
 CourtName - Short court name    
 FirmName - Short Covering firm name    
 ClientLanguage - Abbreviation of Client Language    
 TotalFee - Total fee of the case    
 PaidFee - Total amount paid by client till now    
 Lastname - Last name of the client    
 TicketsViolationID     
 CourtDateMain - Verified court date    
******/    
    
ALTER PROCEDURE [dbo].[USP_HTP_GetAllCriminalViolationsForSharePoint]    
    
AS     
SELECT      
c.ShortName as CourtName, f.FirmAbbreviation as FirmName,    
ClientLanguage = case when t.LanguageSpeak = 'SPANISH' then 'S' else'E'end ,    
t.calculatedtotalfee as TotalFee,     
PaidFee =  (                                                   
 select isnull( SUM(ISNULL(p.ChargeAmount, 0)),0) from tblticketspayment p                                                  
 where p.ticketid = t.ticketid_pk                                                  
 and  p.paymentvoid = 0                                                  
 ) ,    
t.Lastname, tv.TicketsViolationID, tv.CourtDateMain, t.TicketId_pk as TicketId    
FROM tblTickets t     
INNER JOIN tblTicketsViolations tv ON t.TicketID_PK = tv.TicketID_PK    
inner join tblCourts c on c.CourtId = tv.CourtID    
Inner Join tblFirm f on f.FirmID = tv.CoveringFirmId    
    
WHERE     
--Sabir Khan 4662 09/12/2008 add casetypeid for family law    
----------------------------    
--Waqas Javed  5173 12/20/2008 also add event if covering firm is not SULL  
(t.CaseTypeID in (2,4) or isnull(tv.CoveringFirmId,0) <> 3000 )  
     
and t.ActiveFlag = 1    
and tv.CourtViolationStatusId not IN (80,236)    
and datediff(day, tv.CourtDateMain, getDate())<=0    
    
    

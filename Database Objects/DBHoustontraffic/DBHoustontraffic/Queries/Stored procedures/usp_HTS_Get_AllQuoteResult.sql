SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_HTS_Get_AllQuoteResult]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_HTS_Get_AllQuoteResult]
GO

          CREATE PROCEDURE [dbo].[usp_HTS_Get_AllQuoteResult] AS        
SELECT     QuoteResultID, QuoteResultDescription        
FROM         dbo.tblQuoteResult            

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_DOCKETCLOSEOUT_UPDATEALL_SNAPSHOT]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_DOCKETCLOSEOUT_UPDATEALL_SNAPSHOT]
GO

CREATE PROCEDURE USP_HTS_DOCKETCLOSEOUT_UPDATEALL_SNAPSHOT  
@Date datetime,  
@LastUpdate nvarchar(50),  
@TicketID int  
as    
UPDATE tbl_hts_docketcloseout_snapshot    
 set LastUpdate = @LastUpdate    
where    
 RecordID IN (SELECT RecordID from tbl_HTS_DocketCloseOut_Snapshot where Datediff(day,CourtDate,@Date)=0 and TicketID_PK = @TicketID )  

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


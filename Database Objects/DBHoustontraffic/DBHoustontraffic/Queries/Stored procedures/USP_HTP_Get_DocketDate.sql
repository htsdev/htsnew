USE [TrafficTickets]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Sabir Khan 4970 10/17/2008
-- Business Logic : this procedure is used to get current date for docket date.			
CREATE PROCEDURE dbo.USP_HTP_Get_DocketDate 
	
AS
BEGIN
select getdate() as docketdate
END
GO
grant execute on dbo.USP_HTP_Get_DocketDate to dbr_webuser
GO
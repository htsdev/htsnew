USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[usp_HTP_Get_HowHiredOptions]    Script Date: 01/02/2014 02:20:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** 
Create by:  Sabir Khan
Task ID: 11509
Business Logic : This procedure is used to get find by description

******/
ALTER Procedure [dbo].[usp_HTP_Get_HowHiredOptions]

as

SELECT ID,HiredVia FROM HowHired


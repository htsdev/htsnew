set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go



/****** 

Alter by:  Zeeshan Ahmed

Business Logic : This procedure is used to get case information for missed court letter
				 Display Violations which have missed court status		
List of Parameters:     

     @TicketID : Ticket Id of the case. (i.e. primary key of the case). 

List of Columns: 

	TicketID_PK : TicketId
	FirstName : First Name 
	LastName : Last Name
	Address1 : Address 
	City : City
	Zip : Zip 
	State : State
	RefcaseNumber : Ref case numbers     
	CourtDate : Court Date
	CourtPhoneNumber : Court Phone Number
	
******/
     
ALTER procedure [dbo].[USP_HTP_MissedCourt_Letter]     
(     
@TicketID int     
)     
as     
    
declare @CaseNumber varchar(1000)    
set @CaseNumber = ''    
    
select @CaseNumber = @CaseNumber + TV.RefcaseNumber + ', '    
from     
--tblHTSBatchPrintLetter BP     
--join
 tbltickets T     
--on T.TicketID_Pk = BP.Ticketid_FK     
join tblticketsviolations TV     
On T.TicketID_Pk = TV.Ticketid_pk     
join tblcourtviolationstatus CVS     
On CVS.courtviolationstatusid = TV.CourtViolationStatusidmain     
join tblcourts C     
on C.courtID = TV.CourtID     
join tblState S1     
on S1.Stateid = t.StateID_fk     
join tblState S2     
on S2.Stateid = C.State     
where     
T.Ticketid_pk = @TicketID   
and    TV.CourtViolationStatusidmain      = 105  
group by TV.RefcaseNumber    
    
if Len(@CaseNumber) > 1   
set @CaseNumber = left(@CaseNumber, len(@CaseNumber)- 1)    
    
select Distinct T.TicketID_PK ,  T.FirstName , T.LastName , T.Address1 ,T.City , T.Zip ,S1.State, @CaseNumber as RefcaseNumber,     
 max(TV.CourtDateMain) CourtDate , dbo.fn_FormatContactNumber_new(isnull(c.Phone,''),-1) CourtPhoneNumber from       

--tblHTSBatchPrintLetter BP     
--join
 tbltickets T     
--on T.TicketID_Pk = BP.Ticketid_FK     
join tblticketsviolations TV     
On T.TicketID_Pk = TV.Ticketid_pk     
join tblcourtviolationstatus CVS     
On CVS.courtviolationstatusid = TV.CourtViolationStatusidmain     
join tblcourts C     
on C.courtID = TV.CourtID     
join tblState S1     
on S1.Stateid = t.StateID_fk     
join tblState S2     
on S2.Stateid = C.State     
where     
T.Ticketid_pk = @TicketID  
and    TV.CourtViolationStatusidmain      = 105
group by T.TicketID_PK ,  T.FirstName , T.LastName , T.Address1 ,T.City , T.Zip ,S1.State, c.Phone 




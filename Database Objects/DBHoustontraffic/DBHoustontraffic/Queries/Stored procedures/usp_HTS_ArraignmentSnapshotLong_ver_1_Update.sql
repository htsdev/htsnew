﻿ ALTER Procedure [dbo].[usp_HTS_ArraignmentSnapshotLong_ver_1_Update]        
@ReportWeekDay int                  
AS                  
declare @DayValue varchar(10)                  
if @ReportWeekday = 2                  
 set @DayValue = 'Monday'                  
if @ReportWeekday = 3                  
 set @DayValue = 'Tuesday'                  
if @ReportWeekday = 4                  
 set @DayValue = 'Wednesday'                  
if @ReportWeekday = 5                  
 set @DayValue = 'Thursday'                  
if @ReportWeekday = 6                  
 set @DayValue = 'Friday'                  
if @ReportWeekday = 99                  
 set @DayValue = 'N/A'                  
                  
SET NOCOUNT ON                  
 create TABLE [#tblTickets] (                  
 [TicketID_PK] [int] NOT NULL ,                  
 [TicketNumber_PK] [varchar] (20) NOT NULL ,                  
 [Lastname] [varchar] (20) NULL ,                  
 [Firstname] [varchar] (20)  NULL ,                  
 [Violations] [varchar] (50) NULL,                  
 [DLNumber] [varchar] (30) NULL ,                  
 [DOB] [datetime] NULL ,                  
 [MID] [varchar] (20) NULL ,                  
 [BondFlag] [int] NULL ,                    
 [TrialdateTime] [datetime]  NULL ,                  
 [TrialDesireddateTime] [datetime] NULL,                  
 [SEQ] [INT] NULL)                  
                  
                  
INSERT INTO #tblTickets (TicketID_PK,  TicketNumber_PK, Lastname, Firstname, DLNumber, DOB, MID,  BondFlag, TrialDateTime, TrialDesireddateTime)                  
SELECT DISTINCT TT.TicketID_PK,         
--TV.Refcasenumber        
(case         
when (tv.casenumassignedbycourt is not null)         
 and (tv.refcasenumber not like 'ID%')         
 and (tv.casenumassignedbycourt not like '')        
then tv.casenumassignedbycourt         
else TV.Refcasenumber end)        
,        
TT.Lastname,                   
TT.Firstname, TT.DLNumber, TT.DOB,TT.MidNum , TT.BondFlag, TV.courtdatemain , null                  
FROM dbo.tblTickets TT INNER JOIN dbo.tblTicketsViolations TV ON                  
TT.TicketID_PK = TV.TicketID_PK INNER JOIN dbo.tblViolations TRV ON                  
TV.ViolationNumber_PK = TRV.ViolationNumber_PK  LEFT OUTER JOIN                  
                      tblOfficer O ON TT.OfficerNumber = O.OfficerNumber_PK                  
                  
                  
WHERE TRV.ViolationType not IN (1)                  
AND (TV.courtviolationstatusidmain in (select courtviolationstatusid from tblcourtviolationstatus where categoryid in  (2,12,51)))                   
AND isnull(officertype,'N/A') = @DayValue               
AND (TT.Activeflag = 1)                   
and TV.courtviolationstatusidmain <> 80                
AND TV.courtviolationstatusidmain <> 162 --added by ozair for bug # 1432                  
and (TV.courtid IN(3001, 3002, 3003)) ORDER BY TT.ticketid_pk                   
                  
--select * from tblcourtviolationstatus                  
DECLARE @TicketID int                  
DECLARE @TicketNumber varchar(12)                  
DECLARE @Violations varchar(50)                  
DECLARE @SEQ int                  
DECLARE @TicketID2 varchar(50)                  
                  
                  
SELECT @SEQ =1                  
                  
DECLARE CurWeekDays CURSOR READ_ONLY FOR                   
Select TicketID_PK, TicketNumber_PK From #tblTickets ORDER BY                   
convert(datetime,convert(char,TrialDateTime,101)) ASC ,                  
convert(datetime,convert(char, TrialDesireddateTime,101)) ASC ,                  
Lastname ASC ,                   
Firstname ASC ,                  
TicketNumber_PK ASC ,                  
TicketID_PK ASC                  
                  
OPEN CurWeekDays                  
FETCH NEXT FROM CurWeekDays INTO @TicketID , @TicketNumber                   
                  
Select @TicketID2 = @TicketID                  
--select * from tbldatetype                  
WHILE (@@fetch_status <> -1)                  
BEGIN                  
 SELECT @Violations = coalesce(@Violations + ', ',' ') --+ Convert(varchar(2),TV.SequenceNumber) // Agha usman 2664 06/13/2008
 FROM dbo.tblTicketsViolations TV INNER JOIN dbo.tblViolations TRV ON                  
 TV.ViolationNumber_PK = TRV.ViolationNumber_PK                   
 WHERE TRV.ViolationType not IN (1)                  
 AND TV.courtViolationStatusIDmain in (select  courtViolationStatusID from tblcourtviolationstatus where categoryid in (2,12,51))                  
 AND TV.TicketID_PK =  @TicketID                   
 AND TV.RefCaseNumber = @TicketNumber                  
                  
                  
  UPDATE #tblTickets Set #tblTickets.Violations= @Violations ,                  
  SEQ = @SEQ                    
  WHERE  #tblTickets.TicketID_PK = @TicketID AND                  
  #tblTickets.TicketNumber_PK = @TicketNumber                  
                    
  SELECT @TicketNumber = NULL                  
  SELECT @Violations = NULL                   
  SELECT @TicketID = NULL                  
                    
  FETCH NEXT FROM CurWeekDays INTO @TicketID , @TicketNumber                   
                  
  IF (@TicketID2 <> @TicketID)                  
  BEGIN                  
   SELECT @SEQ = @SEQ + 1                  
  END                  
END                  
                  
CLOSE CurWeekDays                  
DEALLOCATE CurWeekDays                  
IF (@ReportWeekDay=99)                  
BEGIN                  
 SELECT t.TicketID_PK, t.TicketNumber_PK , t.Lastname, t.Firstname , Violations =                   
 CASE LEFT(UPPER(t.TicketNumber_PK),1)                  
 WHEN 'F' THEN NULL                  
 ELSE Violations                   
 END , t.DLNumber , t.DOB ,t.MID ,  BondFlag =                   
 CASE BondFlag                   
 WHEN 1 THEN 'b'                  
 ELSE NULL                  
 END , t.TrialdateTime, t.TrialDesireddateTime , t.SEQ                  
      
 --Added by M.Azwar Alam on 29 August 2007 --      
 --If Do Not Reset Flag added from GeneralInfo then dont show that record on Not Assigned Report...      
      
 FROM #tblTickets t  where t.TicketID_PK not in ( select TicketID_PK  from tblticketsflag  where flagid =25  )               
--from #tblTickets t      
 ORDER BY                   
 convert(datetime,convert(char, TrialDateTime,101)) ASC,                  
 convert(datetime,convert(char, TrialDesireddateTime,101)) ASC ,                  
 t.Lastname ASC ,                   
 t.Firstname ASC ,                  
 t.TicketNumber_PK ASC ,                  
 t.TicketID_PK ASC                  
END                  
ELSE                  
BEGIN                  
 SELECT TicketID_PK, TicketNumber_PK , Lastname, Firstname , Violations =                   
 CASE LEFT(UPPER(TicketNumber_PK),1)                  
 WHEN 'F' THEN NULL                  
 ELSE Violations                   
 END , DLNumber , DOB , MID , BondFlag =                   
 CASE BondFlag                   
 WHEN 1 THEN 'b'                  
 ELSE NULL                  
 END , TrialdateTime, TrialDesireddateTime,  SEQ                  
 FROM #tblTickets                   
 ORDER BY                   
 convert(datetime,convert(char,TrialDateTime,101)) ASC ,                  
 convert(datetime,convert(char, TrialDesireddateTime,101)) ASC ,                  
 Lastname ASC ,                   
 Firstname ASC ,                  
 TicketNumber_PK ASC ,                  
 TicketID_PK ASC                  
END        
GO 
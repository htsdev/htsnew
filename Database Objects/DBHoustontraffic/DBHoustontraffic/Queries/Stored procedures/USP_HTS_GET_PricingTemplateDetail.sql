SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_GET_PricingTemplateDetail]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_GET_PricingTemplateDetail]
GO










CREATE PROCEDURE USP_HTS_GET_PricingTemplateDetail 
 ( 
 
	 @templateid int=0 -- 0 for Adding purpose Otherwise for update purpose   
--         @priceid	int = 0

 )  
   
AS  
  
--if ( @templateid>0 )  
-- begin  

  SELECT

	  TD.DetailId,   
        TD.TemplateID,   
    VC.CategoryId,  
     VC.CategoryName,   
    ISNull(TD.BasePrice,0) as BasePrice,  
    ISNull(TD.SecondaryPrice,0) as SecondaryPrice,    
    IsNull(TD.BasePercentage,0) as BasePercentage,   
    ISNull(TD.SecondaryPercentage,0) as SecondaryPercentage,    
    ISNull(TD.BondBase,0) as BondBase,   
    ISNull(TD.BondSecondary,0) as BondSecondary,   
    ISNull(TD.BondBasePercentage,0) as  BondBasePercentage,  
    ISNull(TD.BondAssumption,0) as BondAssumption,   
    IsNull(TD.BondSecondaryPercentage,0) as BondSecondaryPercentage,   
    ISNull(TD.FineAssumption,0) as FineAssumption  
  FROM         dbo.tblViolationCategory VC LEFT OUTER JOIN  
       dbo.tblPricingTemplateDetail TD ON TD.CategoryId = VC.CategoryId AND TD.TemplateID = @templateid  
/*
 end  

else  
 begin  
  SELECT     TD.DetailId,   
   TD.TemplateID,  
    VC.CategoryId,  
    VC.CategoryName,   
   ISNull(TD.BasePrice,0) as BasePrice,  
    ISNull(TD.SecondaryPrice,0) as SecondaryPrice,    
    IsNull(TD.BasePercentage,0) as BasePercentage,   
    ISNull(TD.SecondaryPercentage,0) as SecondaryPercentage,    
    ISNull(TD.BondBase,0) as BondBase,   
    ISNull(TD.BondSecondary,0) as BondSecondary,   
    ISNull(TD.BondBasePercentage,0) as  BondBasePercentage,  
    ISNull(TD.BondAssumption,0) as BondAssumption,   
    IsNull(TD.BondSecondaryPercentage,0) as BondSecondaryPercentage,   
    ISNull(TD.FineAssumption,0) as FineAssumption  
  FROM         dbo.tblViolationCategory VC LEFT OUTER JOIN  
     dbo.tblPricingTemplateDetail TD ON TD.CategoryId = VC.CategoryId AND TD.TemplateID = @templateid  
 end  
  
*/  
/*  
SELECT DetailId,   
  TemplateID,   
  BasePrice,   
  SecondaryPrice,   
  BasePercentage,   
  SecondaryPercentage,   
  BondBase,   
  BondSecondary,   
  BondBasePercentage,   
                    BondAssumption,   
  BondSecondaryPercentage,   
  FineAssumption  
FROM          dbo.tblPricingTemplateDetail  
where  templateid = @templateid  
*/







GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


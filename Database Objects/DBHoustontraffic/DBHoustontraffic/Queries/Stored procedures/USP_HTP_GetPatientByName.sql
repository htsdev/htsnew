﻿/******  
* Created By :	Fahad Muhammad Qureshi.
* Create Date :   10/21/2010 5:49:46 PM
* Task ID :				6766
* Business Logic :  This procedure is used to Get .
* List of Parameter :
* Column Return : 	      
******/


ALTER PROCEDURE [dbo].[USP_HTP_GetPatientByName]
	@FirstName VARCHAR(50) = '',
	@LastName VARCHAR(50) = '',
	@DOB DATETIME = NULL,
	@email VARCHAR(255) -- Noufil Khan 8469 10/29/2010 email searching involve
AS
	IF DATEDIFF(DAY, @DOB, '01/01/1900') = 0
	    SET @DOB = NULL
	
	DECLARE @DefaultSearch BIT
	-- Noufil Khan 8469 10/29/2010 set default search to zero for blank email
	IF (@FirstName = '' AND @LastName = '' AND @DOB IS NULL AND @email = '')
	BEGIN
	    SET @DefaultSearch = 0
	END
	ELSE
	BEGIN
	    SET @DefaultSearch = 1
	END	
	
	SELECT MAX(tt.TicketID_PK) AS TicketID_PK,
	       UPPER(ISNULL(tt.Firstname, '')) AS Firstname,
	       UPPER(ISNULL(tt.Lastname, '')) AS Lastname,
	       ISNULL(tt.DOB, '') AS DOB,
	       ISNULL(tt.Address1, '') + ISNULL(tt.Address2, '') AS [ADDRESS],
	       tt.Email,
	       UPPER(ISNULL(tt.City, '')) AS City,
	       ISNULL(tt.Stateid_FK, 45) AS StateId,
	       ISNULL(tt.Zip, '') AS Zip,
	       ISNULL(ts.[State], 'TX') AS [State]
	FROM   tblTickets tt
	       LEFT OUTER JOIN tblState ts
	            ON  ts.StateID = tt.Stateid_FK
	WHERE  (@FirstName = '' OR ((@FirstName <> '') AND CHARINDEX(@FirstName, tt.Firstname, 0) = 1))
	       AND (@LastName = '' OR ((@LastName <> '') AND CHARINDEX(@LastName, tt.Lastname, 0) = 1))
	       AND (@DOB IS NULL OR ((@DOB IS NOT NULL) AND (DATEDIFF(DAY, @DOB, tt.DOB) = 0)))
	       -- Noufil Khan 8469 10/29/2010 search for input email
	       AND (@email = '' OR (@email <> '' AND (CHARINDEX(@email, tt.Email, 0)> 0)))
	       AND (tt.Activeflag = 1)
	       AND tt.TicketID_PK NOT IN (SELECT ISNULL(p.TicketId,0) FROM Patient p)
	       AND @DefaultSearch = 1
	GROUP BY
	       Firstname,
	       Lastname,
	       DOB,
	       ISNULL(tt.Address1, '') + ISNULL(tt.Address2, ''),
	       Email,
	       City,
	       ISNULL(tt.Stateid_FK, 45),
	       Zip,
	       ISNULL(ts.[State], 'TX')
	ORDER BY
	       tt.Firstname,
	       tt.Lastname  
	

GO
GRANT EXECUTE ON [dbo].[USP_HTP_GetPatientByName] TO dbr_webuser
GO 
﻿
/***********************************************************************************
 * Created By		: Abid Ali
 * Created Date		: 12/31/2008
 * Task Id			: 5359
 * 
 * Business Logic	: To insert letter of rep into batch history
 * 
 * Parameter List	:
 *						@TicketIDs			: List of comma separted ticket IDs
 *						@EmpID				: Employee ID
 *						@CertifiedMailNumber: Certified Mail Number
 *						@BatchIDs			: Batch IDs
 ********************************************************************************/

ALTER PROCEDURE [dbo].[USP_HTP_INSERT_LetterOfRepBatchHistory]
(
    @TicketIDs            VARCHAR(MAX),
    @EmpID                INT,
    @CertifiedMailNumber  VARCHAR(20),
    @BatchIDs             VARCHAR(MAX)
)
AS
	-- if there is no record found	
	SELECT DISTINCT CONVERT(INT, sValue) AS TicketId
	       INTO #Tickets
	FROM   dbo.SplitString(@TicketIDs, ',')
	
	IF NOT EXISTS(
	       SELECT *
	       FROM   #Tickets
	   )
	    RETURN 
	
	CREATE TABLE #BatchTable
	(
		BatchID   INT,
		TicketID  INT
	)
	
	INSERT INTO #BatchTable
	  (
	    BatchID,
	    TicketID
	  )
	SELECT CONVERT(INT, SUBSTRING(sValue, 0, CHARINDEX('=', t.sValue))),
	       CONVERT(
	           INT,
	           SUBSTRING(
	               sValue,
	               CHARINDEX('=', t.sValue) + 1,
	               LEN(sValue) - CHARINDEX('=', t.sValue)
	           )
	       )
	FROM   (
	           SELECT DISTINCT *
	           FROM   dbo.SplitString(@BatchIDs, ',')
	       ) t	       
	
	
	DECLARE @strVacationDates VARCHAR(2000)
	SET @strVacationDates = dbo.fn_getvacation()
	
	-- abid  5563 02/21/2009 remove old feature and modifiy quer
	UPDATE tblhtsbatchprintletter
	SET    ISPrinted = 1,
	       PrintDate = GETDATE(),
	       PrintEmpID = @EmpID,
	       USPSTrackingNumber = @CertifiedMailNumber
	WHERE  BatchId_pk IN (SELECT BatchID
	                      FROM   #BatchTable)
	       AND deleteflag <> 1
	
	-- abid  5563 02/21/2009 remove old feature and update history	
	INSERT INTO tblticketsnotes
	  (
	    TicketID,
	    SUBJECT,
	    EmployeeID,
	    Notes
	  )
	SELECT TicketId,
	       '<a href="../docstorage/Letter Of Rep/' + @CertifiedMailNumber +
	       '_Printed.pdf" target="_blank">Letter of Rep printed through Batch print – CM – ' 
	       + @CertifiedMailNumber + '</a>',	-- abid 5544 02/17/2009 comment updated
	       @EmpID,
	       ''
	FROM   #Tickets
	
	--ozair 5767 05/06/2009 updating IsLORPrinted flag
	UPDATE tbltickets
	SET    IsLORPrinted = 1
	WHERE  TicketID_PK IN (SELECT TicketId
	                       FROM   #Tickets)
	
	
	--Nasir 6013 07/14/2009 status change
	DECLARE @CourtID INT
	SELECT @CourtID = courtid
	FROM   tblTicketsViolations
	WHERE  TicketID_PK = (
	           SELECT TOP 1 TicketId
	           FROM   #Tickets
	       )
	--court HCJP 4-1 5-2 and PMC
	
	
	    DECLARE @TempTickets TABLE (LoopCount INT IDENTITY(1, 1) NOT NULL, TicketID INT)		
	    INSERT INTO @TempTickets
	    SELECT TicketId
	    FROM   #Tickets
	    
	    DECLARE @LoopCounter INT
	    DECLARE @Rec INT
	    DECLARE @tickID INT
	    SET @LoopCounter = (
	            SELECT COUNT(*)
	            FROM   #Tickets
	        )
	    
	    WHILE (@LoopCounter >= 0)
	    BEGIN
	        SELECT @tickID = TicketID
	        FROM   @TempTickets
	        WHERE  LoopCount = @LoopCounter
	        
	        SELECT @rec = COUNT(*)
	        FROM   tblTicketsViolations ttv
	               INNER JOIN tblCourtViolationStatus tcvs
	                    ON  tcvs.CourtViolationStatusID = ttv.CourtViolationStatusIDmain
	        WHERE  ttv.TicketID_PK = @tickID
	               AND DATEDIFF(
	                       DAY,
	                       dbo.fn_getnextbusinessday(GETDATE(), 2),
	                       ttv.CourtDateMain
	                   ) > 0
	               AND (
	                       (ttv.CourtID IN (3013, 3016) AND tcvs.CategoryID = 3)
	                       OR (ttv.CourtID = 3004 AND tcvs.CategoryID = 5)
	                   )
	        
	        IF (@Rec <> 0)
	        BEGIN
	            EXEC usp_hts_update_violationstatus @tickID,@EmpID
	        END
	        ELSE 
	        IF EXISTS (
	               --Nasir 6013 07/08/2009 set status waiting if status is appearence	
	               SELECT *
	               FROM   tblTicketsViolations ttv
	                      INNER JOIN tblCourtViolationStatus tcvs
	                           ON  tcvs.CourtViolationStatusID = ttv.CourtViolationStatusIDmain
	               WHERE  tcvs.CategoryID = 2
	                      AND ttv.CourtViolationStatusIDmain NOT IN (80, 236)
	                      AND ttv.TicketID_PK = @tickID
	           )
	        BEGIN
	           EXEC usp_hts_update_violationstatus @tickID,@EmpID
	        END
	        
	        SET @LoopCounter = @LoopCounter -1
	    END
	
	
	
	
	
	
	-- abid  5563 02/21/2009 removed
	-- DROP TABLE #temp 
	DROP TABLE #Tickets
	DROP TABLE #BatchTable
GO








GRANT EXEC ON [dbo].[USP_HTP_INSERT_LetterOfRepBatchHistory] TO dbr_webuser

GO
/************************************************************
 * Code formatted by SoftTree SQL Assistant © v4.0.34
 * Time: 9/27/2010 12:32:34 PM
 ************************************************************/

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/*  
* Created By		: SAEED AHMED
* Task ID			: 8101  
* Created Date		: 09/27/2010  
* Business logic	: This procedure is used get report id of the page which is pass as an input parameter.
*					: 
* Parameter List	
  @Report_Url		: Page url.
  
* 
*/
CREATE PROCEDURE [dbo].[USP_ReportSetting_GetReportId]
	@Report_Url VARCHAR(2000)
AS
BEGIN
    SELECT Report_ID
    FROM   tbl_Report
    WHERE  Report_URl = @Report_Url
END
GO


GRANT EXECUTE ON [dbo].[USP_ReportSetting_GetReportId] TO dbr_webuser
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Delete_VacationDate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Delete_VacationDate]
GO




--[USP_HTS_Delete_VacationDate] 3
CREATE procedure [dbo].[USP_HTS_Delete_VacationDate]
	(
	@Id int
	)


as 

update tblvacationdates set isactivevacation = 0 where RowId = @Id

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


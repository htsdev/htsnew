SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Continuance_Letters_Printing]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Continuance_Letters_Printing]
GO

          
          
-- USP_HTS_Continuance_Letters_Printing 1,'11/20/2007 ','12/07/2007',3001,3991,1             
Create Procedure [dbo].[USP_HTS_Continuance_Letters_Printing]                            
@showPrinted int,                            
@startdate datetime,                            
@enddate datetime,                            
@courtid int,                            
@rep int,          
@ShowSacnDocument int                            
                            
                            
AS                            
                            
declare @sql varchar(max)                            
                            
set @sql = 'SELECT DISTINCT           
                      T.TicketID_PK AS ticketid_pk, T.Firstname,    T.Lastname + '', '' + T.Firstname AS ClientName, TCS.Description AS ContinuanceStatus,           
                      (CASE WHEN CONVERT(varchar, isnull(T .ContinuanceDate, ''01/01/1900''), 101) = ''01/01/1900'' THEN '''' ELSE CONVERT(varchar(10), T .ContinuanceDate,101) END) AS ContinuanceDate,T.ContinuanceDate AS Expr1, dbo.fn_DateFormat(ISNULL(bpl.PrintDate, CONVERT(datetime, ''1/1/1900'')), 28, ''/'', '':'', 1)           
                      AS PrintDate, tcvs.ShortDescription AS STATUS, bpl.IsPrinted, bpl.BatchID_PK AS batchid, ttf.Empid,          
                          (SELECT     TOP (1) Abbreviation          
                            FROM          tblUsers          
                            WHERE      (EmployeeID = ttf.Empid)) AS SRep, dbo.fn_DateFormat(ISNULL(tv.CourtDateMain, CONVERT(datetime, ''1/1/1900'')), 28, ''/'', '':'', 1)           
                      AS courtDatemain, TU.Abbreviation AS Rep, tc.ShortName, CONVERT(int, tv.CourtNumber) AS courtnumber, bpl.PrintedRep AS PrintEmpId,           
                      dbo.fn_DateFormat(ISNULL(ttf.RecDate, CONVERT(datetime, ''1/1/1900'')), 28, ''/'', '':'', 1) AS RecDate, ttf.RecDate as recdatesort, CONVERT(datetime, tv.CourtDateMain) AS courtdatemainsort,  
                       CONVERT(datetime,T .ContinuanceDate) as Contsort,        
                          (SELECT     TOP (1) CONVERT(Varchar, tsc.ScanBookID) + ''-'' + CONVERT(varchar, tsp.DOCID) + ''.'' + tsp.DocExtension AS Doc          
                            FROM          tblScanBook AS tsc INNER JOIN          
                                                   tblTickets AS T ON T.TicketID_PK = tsc.TicketID RIGHT OUTER JOIN          
                                                   tblScanPIC AS tsp ON tsc.ScanBookID = tsp.ScanBookID          
                            WHERE      (T.ContinuanceStatus > 0) AND (tsc.DocTypeID = 2) AND (tsc.TicketID = tv.TicketID_PK)) AS Doc          
FROM tblTickets AS T INNER JOIN          
          
          
          
                      tblTicketsViolations AS tv ON T.TicketID_PK = tv.TicketID_PK LEFT OUTER JOIN          
                      tblTicketsFlag AS ttf ON tv.TicketID_PK = ttf.TicketID_PK LEFT OUTER JOIN          
                      tblContinousStatus AS TCS ON T.ContinuanceStatus = TCS.StatusID LEFT OUTER JOIN          
                      tblUsers AS TU ON T.EmployeeID = TU.EmployeeID LEFT OUTER JOIN          
                      tblCourtViolationStatus AS tcvs ON tv.CourtViolationStatusIDmain = tcvs.CourtViolationStatusID LEFT OUTER JOIN          
                      tblCourts AS tc ON tv.CourtID = tc.Courtid LEFT OUTER JOIN          
                      tblHTSBatchPrintLetter_Continuance AS bpl ON T.TicketID_PK = bpl.TicketID_FK          
WHERE    (ttf.Flagid = 9) AND (T.ContinuanceStatus > 0) AND (ISNULL(T.ContinuanceFlag, 0) <> 0) AND (tv.TicketsViolationID =          
                          (SELECT     TOP (1) TicketsViolationID          
                            FROM          tblTicketsViolations          
                            WHERE      (TicketID_PK = T.TicketID_PK))) AND (tv.CourtViolationStatusIDmain <> 80)                  
and t.ticketid_pk '        
                            
if (@showPrinted=1)                            
begin                        
 set @sql =@sql + ' in (select distinct ticketid_fk from tblHTSBatchPrintLetter_Continuance where IsDeleted <> 1)'                            
               
 set @sql = @sql +  ' and bpl.printdate between ''' + Convert(varchar(12),@startdate,101) + ' 00:00:00''' + ' and ''' +  Convert(varchar(12),@enddate,101) + ' 23:59:59 ''' 

if(@rep <> -1)                            
     set @sql = @SQL + ' and bpl.PrintEmpID=' +convert(varchar,@rep)+'' 

if(@courtid <> -1)                            
     set @sql = @SQL + ' and tv.courtid =' +convert(varchar,@courtid)+' '   
                      
 end           
                       
else                            
 Begin            
  set @sql =@sql + ' not in (select distinct ticketid_fk from tblHTSBatchPrintLetter_Continuance) '                            
            
  if(@ShowSacnDocument=0)          
  set @sql = @SQL + ' and ( Select Count(*) from tblscanbook where doctypeid=2 and ticketid=t.ticketid_pk  and isnull(isdeleted,0) = 0 ) = 0'          
  else          
  set @sql = @SQL + ' and ( Select Count(*) from tblscanbook where doctypeid=2 and ticketid=t.ticketid_pk and isnull(isdeleted,0) = 0 ) > 0'          
          
 End              
                        
if(@courtid <> -1 and @showPrinted=0)                            
     set @sql = @SQL + ' and tv.courtid =' +convert(varchar,@courtid)+' '                            
                            
if(@rep <> -1 and @showPrinted=0)                            
     set @sql = @SQL + ' and ttf.empid  =' +convert(varchar,@rep)+'' 

        
           
           
                     
                           
--print @sql                            
exec(@sql) 


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


﻿/************************************************************
 * Created By : Noufil Khan 
 * Created Date: 10/30/2010 4:02:54 PM
 * Business Logic : This prcedure returns patient file and its information against the appropiate patient
 * Parameters :
 *			@patientId : Primary Id of table
 * Column Return :
 *			   Id,
			   PatientId,
			   OriginalFileName,
			   StoreFileLocation,
			   Description,
			   UserName,
			   UploadDate
 ************************************************************/

CREATE PROCEDURE [dbo].[USP_HTP_GetAllPatientFile]
	@patientId INT
AS
	SELECT puf.Id,
	       puf.PatientId,
	       puf.OriginalFileName,
	       puf.StoreFileLocation,
	       puf.[Description],
	       u.UserName,
	       puf.UploadDate
	FROM   PatientsUploadedFile puf
	LEFT OUTER JOIN users.dbo.[User] u ON u.UserId = puf.UploadedBy
	WHERE  puf.PatientId = @patientId
GO

 GRANT EXECUTE ON [dbo].[USP_HTP_GetAllPatientFile] TO dbr_webuser
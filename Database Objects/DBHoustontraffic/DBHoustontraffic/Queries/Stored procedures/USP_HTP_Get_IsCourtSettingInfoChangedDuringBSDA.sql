﻿/****** 
Created by:		Syed Muhammad Ozair 5799 04/14/2009
Business Logic:	this procedure is used to get IsCourtSettingInfoChangedDuringBSDA by ticketid

List of Parameters:				
	@TicketID :
	
List of Column:
	IsCourtSettingInfoChangedDuringBSDA
	
*******/
CREATE PROCEDURE dbo.USP_HTP_Get_IsCourtSettingInfoChangedDuringBSDA
	@ticketID INT
AS
	SELECT tt.IsCourtSettingInfoChangedDuringBSDA
	FROM   tblTickets tt
	WHERE  tt.TicketID_PK = @ticketID
GO


GRANT EXECUTE ON dbo.USP_HTP_Get_IsCourtSettingInfoChangedDuringBSDA TO dbr_webuser
GO



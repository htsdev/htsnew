﻿
     
/*            
Created By     : Muhammad Nasir    
Created Date : 01/09/2010      
TasK   : 7234            
Business Logic  : This procedure Updates HMC Same Day Arr Follow Up Date .          
               
Parameter:           
   @TicketID  : Updating criteria with respect to TicketId          
   @FollowUpDate : Date of follow Up which has to be set         
   @empid :Employee ID
         
          
*/          
      
CREATE PROCEDURE [dbo].[USP_HTP_Update_HMCSameDayArrFollowUpDate]
	@FollowUpDate DATETIME,
	@TicketID INT,
	@empid INT
AS
	UPDATE tblTickets
	SET    HMCSameDayArrFollowUpdate = @FollowUpDate,
	       EmployeeID = @empid
	WHERE  TicketID_PK = @TicketID
GO

   
   GRANT EXECUTE ON dbo.[USP_HTP_Update_HMCSameDayArrFollowUpDate] TO 
dbr_webuser
   
   GO 
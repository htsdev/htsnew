
-- exec sp_UploadLubbockDLQFiles '3/27/09'
ALTER procedure [dbo].[sp_UploadLubbockDLQFiles]  

 @listdate varchar(12)    
as    


SET NOCOUNT ON

if len(isnull(@listdate,''))=0 or isdate(@listdate) = 0 or datediff(day, isnull(@listdate,'1/1/1900') ,'1/1/1900') = 0 
	set @listdate = CONVERT(VARCHAR(10), getdate() ,101)



DECLARE @LoaderId tinyint, @rowcount int, @CurrDate datetime, @empid int    
    
SElecT @LoaderId = 3, @currdate = getdate(), @empid = 3992
    
-------------------------------------------------------------------------------    
-----  FORMATTING GARBAGE DATA .......       
-------------------------------------------------------------------------------    
update tblDLQ set courtnum = 0 where isnumeric(courtnum) = 0    
update tblDLQ set officernum = 0 where isnumeric(officernum) = 0    
update tblDLQ set misc2 = replace(misc2,' ','') 

-----------------------------------------------------------------------------
--Sabir Khan 4413 02/27/2009 Replace unll state with TX...
--update tblDLQ set State = 'TX' where len(isnull(State,'')) = 0
-----------------------------------------------------------------------------   
    
-- IF TICKET NUMBER NOT PRESENT THEN UPDATE TICKET NUMBER BY CAUSE NUMBER    
update tbldlq    
set ticketnumber = misc2    
where len(isnull(ticketnumber,'')) = 0    
    
-------------------------------------------------------------------------------    
-----  INSERTING NEW VIOLATIONS..........    
-------------------------------------------------------------------------------    
select distinct violation as violations,Violationcode into #temp     
from tblDLQ WITH(NOLOCK) where violationcode not in(    
select distinct T.violationcode  from tblDLQ T WITH(NOLOCK), tblviolations V WITH(NOLOCK)   
where ltrim(T.Violation) = V.description    
and T.violationcode = V.violationcode    
and V.violationtype = 2 )    
    
insert into tblviolations    
(description,sequenceorder,IndexKey,IndexKeys,ChargeAmount,BondAmount,    
Violationtype,ChargeonYesflag,ShortDesc,ViolationCode,AdditionalPrice,courtloc)    
select violations,null,2,100,140,0,2,0,'None',violationcode,0,0 from #temp     
    
-------------------------------------------------------------------------------    
-----  UPDATING BOND AMOUNT IN TBLVIOLATIONS..........    
-----  ADDED BY TAHIR AHMED DT: 02/16/2007    
-------------------------------------------------------------------------------    
update v    
set v.newbondamount  = convert(money, c.misc3)     
from tbldlq c, tblviolations v    
where v.violationcode = c.violationcode    
and isnumeric(c.misc3) = 1    
    

--Sabir Khan 4413 03/03/2009 all records into temp table
-------------------------------------------------------------------------------    
-----  GETTING TICKETS IN TEMP TABLE FOR INSERTION OF NEW TICKETS......    
------------------------------------------------------------------------------- 
select  ticketnumber,midnumber,    
left(upper(firstname),20) as firstname, left(upper(lastname),20) as lastname,    
left(upper(initial),5) as initial,    
upper(address) as address,upper(CITY) as city,isnull(s.stateid,45) as state, -- phone, Rab Nawaz Khan 10196 04/23/2012 Phone Numbers Excluded from uploading
ZIP, dbo.convertstringtodate(dob) as dob,null as dlnumber, 
dbo.convertstringtodate(violationdate)  as violationdate,    
dbo.convertstringtodate(courtdate)     
+ ' ' + dbo.fn_convert_timestring_to_time(time) as courtdate ,    
'                                                             ' as officername,    
race= case race    
when 'H' then 'Hispanic'    
when 'W' then 'White'    
when 'B' then 'Black'    
else 'Asian'    
end,gender=case gender    
when 'M' then 'Male'    
else 'Female'    
end,    
left(height,1) + ' ft ' + right(Height,2) as height, associatedcasenumber,    
@listdate as listdate,        
dbo.fn_convert_timestring_to_time(time) as courttime,    
substring(barcode,10,2) as dp2,right(barcode,1) as dpc,    
null as crrt,    
courtid = case courtnum     
when 13 then 3002    
when 14 then 3002    
when 18 then 3003    
else 3001    
end,    
DPV AS FLAG1,officernum ,
0 as violationnumber,
convert(money,price) as fineamount,
violation, violationcode,status, convert(int,dbo.fn_getviolationstatus(status)) as violationstatusid, courtnum, misc3 as bondamount,
misc2 as causenumber, associatedcasenumber as bonddate 
into #temptblDLQ    
from tblDLQ L WITH(NOLOCK)  LEFT OUTER JOIN tblState s WITH(NOLOCK) ON s.[State] = L.[State]

--Sabir Khan 4413 03/03/2009 set officername in temp table...
update #temptblDLQ set officername = ltrim(officername)   
 
update #temptblDLQ     
set officername = isnull(O.firstname,'N/A') + ' ' + isnull(O.lastname,'N/A')
from #temptblDLQ T, tblofficer O    
where convert(int,T.Officernum) = O.officernumber_pk

--Sabir Khan 4413 03/03/2009 insert rowid field into temp table...
alter table #temptbldlq add rowid int identity(1,1)

select causenumber, min(rowid) as rowid into #duplicate
from #temptbldlq group by causenumber having count(causenumber)> 1

-- delete duplicate cause numbers...
delete from #temptbldlq
from #temptbldlq a inner join #duplicate b on a.causenumber = b.causenumber and a.rowid > b.rowid

alter table #temptbldlq drop column rowid

--Sabir Khan 4413 03/03/2009 sort records by midnumber into temp table...
select * into #temptblDLQFinal from #temptblDLQ order by midnumber

ALTER TABLE #temptbldlqfinal ADD isExists BIT DEFAULT(0)

UPDATE a
SET a.isExists = 1 
FROM #temptbldlqfinal a INNER JOIN dbo.tblticketsviolationsarchive v ON v.CauseNumber = a.causenumber
INNER JOIN dbo.tblticketsarchive t ON t.RecordID = v.RecordID  and  a.midnumber = t.MidNumber


UPDATE ta
SET ta.IsIncorrectMidNum = 1
FROM tblTicketsArchive ta INNER JOIN tblTicketsViolationsArchive tva ON tva.RecordID = ta.recordid
INNER JOIN #temptbldlqfinal a ON a.causenumber = tva.CauseNumber AND a.midnumber <> ta.MidNumber
WHERE ISNULL(a.isExists,0) = 0

ALTER TABLE #temptbldlqfinal ADD IsAttornyAss BIT NOT NULL DEFAULT(0)

UPDATE a
SET a.IsAttornyAss = 1
FROM #temptbldlqfinal a INNER JOIN tblTicketsViolationsArchive tva ON tva.CauseNumber = a.causenumber
INNER JOIN tblTicketsArchive ta ON ta.RecordID = tva.RecordID AND ta.MidNumber <> a.midnumber
WHERE ISNULL(a.isExists,0) = 0 AND len(isnull(tva.attorneyname, '')+isnull(tva.barcardnumber,'')) > 0
AND ISNULL(a.IsAttornyAss,0) = 0


alter table #temptblDLQFinal add rowid int identity(1,1)
--Sabir Khan 4413 03/03/2009 declare veriable for loop...
declare @idx int, @reccount int, @midnumber varchar(20), @tempMid varchar(20), @RecordId INT, @isExists bit
declare @cuase varchar(30), @addedrec int, @updatedrec int, @tempupdate int
set @cuase = null
select @idx = 1, @midnumber = '', @tempmid= '', @recordid = 0, @isExists = 0, @reccount = count(rowid) from #temptblDLQFinal
select @addedrec = 0, @updatedrec = 0, @tempupdate = 0

While @idx <= @reccount
BEGIN
	 select @isExists = isexists from #temptblDLQFinal where rowid = @idx
	 --Sabir Khan 4413 03/03/2009 check cuase number in database ...

	if ISNULL(@isExists,0) = 0 

		BEGIN
			--insert ticket numbers that don't exist in dbo.tblticketsArchive    
			insert into dbo.tblticketsArchive(TicketNumber, MidNumber, FirstName, LastName,     
			Initial, Address1, City, StateID_FK, -- PhoneNumber, Rab Nawaz Khan 10196 04/23/2012 Phone Numbers Excluded from uploading
			ZipCode, DOB, DLNumber, ViolationDate, CourtDate, OfficerName,Race,Gender,Height ,AssociatedCaseNumber,listdate    
			,CourtNumber,Courttime,DP2 ,DPC,CRRT,courtid,flag1,OfficerNumber_FK,InsertionLoaderId)    
			 
			select distinct 
			left(ticketnumber,20), left(midnumber,20) ,left(firstname,20),left(lastname,20),left(initial,5),left(address,50),left(city,50),state,
			-- left(phone,15)	, Rab Nawaz Khan 10196 04/23/2012 Phone Numbers Excluded from uploading
			left(ZIP,12),dob,dlnumber,violationdate,courtdate,left(officername,50),left(race,10),left(gender,10),left(height,10),
			left(associatedcasenumber,20),listdate,courtnum,left(courttime,10),left(dp2,10),left(dpc,10),left(crrt,10),courtid,FLAG1,officernum,@LoaderId
			from #temptblDLQFinal  where	rowid = @idx and firstname is not null and lastname is not null

			--Sabir Khan 4413 03/03/2009 insertion into tblticketsviolationarchive...
			INSERT INTO dbo.tblticketsViolationsArchive(TicketNumber_PK, ViolationNumber_PK, FineAmount,    
			    
			ViolationDescription,ViolationCode,violationstatusid,courtdate,courtnumber,courtlocation,    
			bondamount,ticketviolationdate,ticketofficernumber,RecordID,CauseNumber,bonddate,updateddate, InsertionLoaderId,BarCardNumber )	

			select ticketnumber,convert(int,violationnumber),fineamount,violation,left(violationcode,10),violationstatusid,    
			courtdate,courtnum,courtid,    
			convert(money,bondamount) as bondamount,violationdate,officernum,0, causenumber,bonddate,getdate(),@LoaderId, CASE ISNULL(IsAttornyAss,0) WHEN 1 THEN '999999999' ELSE NULL END    
			from #temptblDLQFinal where rowid = @idx
 
			set @addedrec = @addedrec + 1
		END   
	ELSE
		BEGIN

			-------------------------------------------------------------------------------    
			-----  UPDATE CAUSE NUMBER IN NON CLIENTS BY MATCHING TICKET NUMBER & MID NUMBER......    
			-------------------------------------------------------------------------------    
			update V    
			set causenumber = L.causenumber,    
			 UpdationLoaderId = @LoaderId    
			from #temptblDLQFinal L, dbo.tblticketsviolationsarchive V, dbo.tblticketsArchive a    
			where L.ticketnumber = V.ticketnumber_pk    
			and a.recordid  =  v.recordid    
			and a.midnumber = l.midnumber    
			and  len(isnull(v.causenumber,'')) = 0 
			and L.rowid = @idx

			-------------------------------------------------------------------------------    
			-----  UPDATE CAUSE NUMBER IN QUOTE/CLIENTS BY MATCHING TICKET NUMBER & MID NUMBER......    
			-------------------------------------------------------------------------------    
			update V    
			set casenumassignedbycourt = L.causenumber, vemployeeid =  @empid   
			from #temptblDLQFinal L, tblticketsviolations V, tbltickets a    
			where L.ticketnumber = V.refcasenumber    
			and a.midnum = l.midnumber    
			and a.ticketid_pk   =  v.ticketid_pk    
			and  len(isnull(v.casenumassignedbycourt,'')) = 0  
			and L.rowid = @idx

			-------------------------------------------------------------------------------    
			-----  UPDATE CASE STATUS, BOND DATE, BOND AMOUNT & UPDATE DATE IN NON-CLIENTS     
			-----  FOR EXISTING TICKETS BY MATCHING CAUSE NUMBER .......    
			-------------------------------------------------------------------------------    
			update V    
			set V.violationstatusid = L.violationstatusid,    
			V.bonddate=L.associatedcasenumber ,    
			V.bondamount=convert(money,L.bondamount) ,    
			v.updateddate = L.associatedcasenumber ,  
			V.status = ISNULL(L.status,'N/A'),    
			 UpdationLoaderId = @LoaderId    
			from     
			#temptblDLQFinal L, dbo.tblticketsviolationsarchive V    
			where L.causenumber = V.causenumber    
			and V.violationstatusid not in (80,145,146,147,148)    
			and datediff(day, isnull(v.bonddate,'1/1/1900'), L.associatedcasenumber) > 0 and L.rowid = @idx

			set @tempupdate = @@rowcount
			set @updatedrec = @updatedrec + @tempupdate
			set @tempupdate = 0

			---Update bond amount for all --------------
			update V    
			set  
			 V.bondamount=convert(money,L.bondamount)       
			from     
			#temptblDLQFinal L, dbo.tblticketsviolationsarchive V    
			where L.causenumber = V.causenumber and L.rowid = @idx

			 -------------------------------------------------------------------------------    
			 -----  UPDATE CASE STATUS, BOND DATE, BOND AMOUNT & UPDATE DATE IN QUOTE/CLIENT     
			 -----  FOR EXISTING TICKETS BY MATCHING CAUSE NUMBER .......    
			 -------------------------------------------------------------------------------  
			 update V    
			 set courtviolationstatusid = T.violationstatusid,    
				v.updateddate = t.associatedcasenumber,
				v.vemployeeid = @empid
			 from     
			 #temptblDLQFinal T, tblticketsviolations V    
			 where T.causenumber = V.casenumassignedbycourt    
			 and courtviolationstatusid not in (80,145,146,147,148) and T.rowid = @idx
			 -- tahir 5718 03/27/2009 fixed the dlq date issue.
			 and datediff(day, isnull(v.updateddate, '1/1/1900'), t.associatedcasenumber) >= 0

			 set @tempupdate = @@rowcount
			 set @updatedrec = @updatedrec + @tempupdate
			 set @tempupdate = 0


		END

set @idx = @idx + 1

END


--Sabir Khan 4413 03/03/2009 drop all temp table...
drop table #temp
drop table #duplicate
drop table #temptblDLQ
drop table #temptblDLQFinal



-- UPDATE DO NOT MAIL FLAG FOR NEWLY INSERTED RECORDS..    
-- IF PERSON'S NAME & ADDRESS MARKED AS DO NOT MAIL...    
EXEC USP_HTS_UPDATE_NOMAILFLAG @CurrDate, '3001,3002,3003,'    
    
-- SENDING UPLOADED NO OF RECORDS THROUGH EMAIL   

EXEC usp_sendemailafterloaderexecutes  @addedrec,@LoaderId,'houston' ,'', @updatedrec


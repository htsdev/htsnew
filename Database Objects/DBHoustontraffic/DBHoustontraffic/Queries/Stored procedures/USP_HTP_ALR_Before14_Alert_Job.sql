set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go
/*
Author: Muhammad Adil Aleem
Description: To get ALR records 14 days before the court date
Parameters: NIL
Type : Report
Created date : Jul-1st-2008
*/
ALTER  procedure [dbo].[USP_HTP_ALR_Before14_Alert_job]
as
declare @ThisResultSet varchar(max)
--Sabir Khan 6433 09/07/2009 Declare varibale...
declare @SerialHyperlink varchar(1000)
declare @ThisPendingCount varchar(5)  
declare @ThisAlertCount varchar(5)  
declare @ThisTotalCount varchar(5) 
set @SerialHyperlink = 'ln.legalhouston.com' 
--Stored procedure for genrating serial number has been changed.
--execute USP_HTP_Generics_Get_XMLSet 'traffictickets.dbo.USP_HTP_ALR_Before14_Alert', ' ', 'ALR Subpeona Alert', @ResultSet = @ThisResultSet output  
execute USP_HTP_Generics_Get_XMLSet_For_Validation   
	   'traffictickets.dbo.USP_HTP_ALR_Before14_Alert',   
	   ' ',   
	   'ALR Subpeona Alert',  
	   'TicketID_PK,  TICKETNO as [Ticket No], LNAME as [L.NAME], FNAME as [F.NAME],CRTDATE as [CRT.DATE],CRTNO as [CRT.NO], CRTLOC as [CRT.LOC],TRIALCATEGORY as [TRIAL.CATEGORY] ',  
	   1,'',@SerialHyperlink,@ResultSet = @ThisResultSet output, @PendingCount = @ThisPendingCount OutPut,@AlertCount =@ThisAlertCount output  ,@TotalCount=@ThisTotalCount output
--&lt; has been replaced with < and &gt; has been replaced with >...
set @ThisResultSet = replace(@ThisResultSet, '&lt;','<')
set @ThisResultSet= replace(@ThisResultSet, '&gt;','>')    

if isnull(@ThisResultSet,'') <> ''

Begin  
  
EXEC msdb.dbo.sp_send_dbmail  
    @profile_name = 'ALR Alerts',            
    @recipients = 'ALRalert@sullolaw.com',      
    --@recipients = 'sabir@lntechnologies.com',  
    @subject = 'ALR Subpeona Alert',  
    @body = @ThisResultSet,  
    @body_format = 'HTML';  
End  
  
/*    
Created By: Haris Ahmed    
Date:   08/30/2012    
    
Business Logic:    
 The stored procedure is used to Update Immigration Status
    
List of Input Parameters:    
 @TicketId: Case identification for the client. 
 @StatusID: Case status of the client. 
*/

CREATE PROCEDURE [dbo].[USP_HTS_Update_ImmigrationCaseStatus]
	@TicketID INT,
	@StatusID INT
AS
	
	UPDATE tbl_HTS_ImmigrationComments
	SET	
		StatusID = @StatusID,
		ModifiedDate = GETDATE()
	WHERE 
		TicketID = @TicketID

GO
GRANT EXECUTE ON USP_HTS_Update_ImmigrationCaseStatus TO dbr_webuser;
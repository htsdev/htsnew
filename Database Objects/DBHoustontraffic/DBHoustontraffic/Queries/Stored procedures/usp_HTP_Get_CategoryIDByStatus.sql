﻿/****** 
Create by		: Syed Muhammad Ozair
Created Date	: 11/19/2008
Task ID			: 5171
Business Logic : This procedure simply return the category id associated with the provided status

Column Return : 
				categoryid
******/

Create procedure dbo.usp_HTP_Get_CategoryIDByStatus

@status	int

as

select isnull(categoryid,0) as categoryid
from tblcourtviolationstatus
where courtviolationstatusid=@status

go

Grant exec on dbo.usp_HTP_Get_CategoryIDByStatus to dbr_webuser
go
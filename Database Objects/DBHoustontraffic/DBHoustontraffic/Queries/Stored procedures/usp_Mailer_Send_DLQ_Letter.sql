set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


/****** 
Created by:		Tahir Ahmed
Business Logic:	The procedure is used by LMS application to get data for HMC Judge letter
				and to create LMS history for this letter for the selected date range.
				
List of Parameters:
	@catnum:		Category number of the selected letter type, if printing for 
					all courts of the selected court category
	@LetterType:	Selected letter type in LMS
	@CrtId:			Court id of the selected court category if printing for individual court hosue.
	@startListdate:	Comma separated dates selected to print letter.
	@EmpId:			Employee information who is printing the letter.
	@PrintType:		Flag to just preview the letter or to mark letters as sent. If 1 then LMS letter history will be created.
	@SearchType:	Flag that identifies if searching by court date or by list date. 0= list date, 1 = court date
	@isPrinted:		Flag to include already printed letters in the batch.

List of Columns:	
	LetterId:		Letter ID generated for each letter. It will be "NOT PRINTABLE"	if @PrintType = 0
	RecordId:		Identity value to look up the case in non-clients. used to group the records in report file.
	FirstName:		Person's first name that will appear on the letter
	LastName:		Person's last name that will appear on the letter
	Address:		Person's Home address that will appear on the letter
	City:			Person's Home City that will appear on the letter
	State:			Person's Home State that will appear on the letter
	FineAmount:		Fine amount for each violation that will appear on the letter
	ViolationDescritpion: violation description that will appear on the letter
	Court Name:		Name of court with which the violation is associated. Used in report file
	DPC:			Address status that is used in barcode font on the letter
	DPTwo:			Address status that is used in barcode font on the letter
	TicketNumber_pk:Case number for the violation
	ZipCode:		Person's Home Zip code.
	CourtDate:		Court date associated with the violation.
	MidNum:			MidNumber of the person associated with the case.
	ZipMid:			First five characters of zip code and midnumber for grouping of data in report file.
	Abb:			Short abbreviation of employee who is printing the letter

*******/

  
-- usp_Mailer_Send_DLQ_Letter 1, 19, 1, '1/1/2010,10/29/2010' , 3991, 1 , 0 , 0  
ALTER proc [dbo].[usp_Mailer_Send_DLQ_Letter]  
@catnum int=1,                                        
@LetterType int=9,                                        
@crtid int=1,                                        
@startListdate varchar (500) ='01/04/2006',                                        
@empid int=3991,                                  
@printtype int = 0 ,  
@searchtype int = 0,  
@isprinted bit  
                                        
as  

-- DECLARING LOCAL VARIABLES FOR THE FILTERS....                                          
declare @officernum varchar(50),                                        
@officeropr varchar(50),                                        
@tikcetnumberopr varchar(50),                                        
@ticket_no varchar(50),                                                                        
@zipcode varchar(50),                                                 
@zipcodeopr varchar(50),                                        
@fineamount money,                                                
@fineamountOpr varchar(50) ,                                                
@fineamountRelop varchar(50),                       
@singleviolation money,                                                
@singlevoilationOpr varchar(50) ,                                                
@singleviolationrelop varchar(50),                                        
@doubleviolation money,                                                
@doublevoilationOpr varchar(50) ,                                                
@doubleviolationrelop varchar(50),                                      
@count_Letters int,                      
@violadesc varchar(500),                      
@violaop varchar(50)                                         

-- DECLARING & INITIALIZING THE VARIABLE FOR DYNAMIC SQL...                                        
declare @sqlquery varchar(max), @sqlquery2 varchar(max) 
Select @sqlquery =''  , @sqlquery2 = ''   

-- ASSIGNING VALUES TO THE VARIABLES 
-- BY GETTTING INFORMATION FROM LMS FILTER STRUCTURE...
-- FOR THIS LETTER TYPE....
Select @officernum=officernum,                                        
@officeropr=oficernumOpr,                                        
@tikcetnumberopr=tikcetnumberopr,                                        
@ticket_no=ticket_no,                                                                          
@zipcode=zipcode,                                        
@zipcodeopr=zipcodeLikeopr,                                        
@singleviolation =singleviolation,                                        
@singlevoilationOpr =singlevoilationOpr,                                        
@singleviolationrelop =singleviolationrelop,                                        
@doubleviolation = doubleviolation,                                        
@doublevoilationOpr=doubleviolationOpr,                                                
@doubleviolationrelop=doubleviolationrelop,                      
@violadesc=violationdescription,                      
@violaop=violationdescriptionOpr ,                  
@fineamount=fineamount ,                                                
@fineamountOpr=fineamountOpr ,                                                
@fineamountRelop=fineamountRelop                  
                                     
from tblmailer_letters_to_sendfilters                                        
where courtcategorynum=@catnum                                        
and Lettertype=@LetterType                                      

-- GETTING THE SHORT ABBREVIATION OF THE CURRENTLY LOGGED IN 
-- EMPLOYEE IN THE LOCAL VARIABLE....
declare @user varchar(10)  
select @user = upper(abbreviation) from tblusers where employeeid = @empid  

-- CREATING DYNAMIC SQL FOR THE MAIN QUERY...                                  
set @sqlquery=@sqlquery+'                                        
Select distinct 
	ta.recordid,   
	convert(varchar(10),tva.bonddate,101) as listdate,  
	left(ta.zipcode,5) + ltrim(ta.midnumber) as zipmid,  
	tva.CourtDate, -- Rab Nawaz 9951 12/22/2011 Added Court date insted of violation date 
	tva.courtlocation as courtid, 
	flag1 = isnull(ta.Flag1,''N'') , 
	tva.violationnumber_pk,   
	ta.officerNumber_Fk, 
	tva.TicketNumber_PK,  
	ta.donotmailflag,  
	ta.clientflag,  
	upper(firstname) as  firstname, 
	upper(lastname)  as lastname,   
	upper(address1) + '''' + isnull(ta.address2,'''') as address,                          
	upper(ta.city) as city,  
	s.state,  
	c.CourtName,  
	zipcode,  
	midnumber,  
	dp2 as dptwo,  
	dpc, 
	violationdate, 
	violationstatusid,    
	ta.midnumber as midnum,  
	ViolationDescription, 
	case when isnull(tva.fineamount,0)=0 then 100 else tva.fineamount end as FineAmount  
into #temptable                                              
FROM tblTicketsViolationsArchive tva, tblTicketsArchive ta  , tblstate s, tblcourts c  
where  tva.RecordID = ta.RecordID     
and ta.stateid_Fk = s.stateid  
and c.courtid = tva.courtlocation  

-- ONLY DLQ CASES....
and  tva.violationstatusid =  146  

-- ONLY VERIFIED AND VALID ADDRESSES
and  Flag1 in (''Y'',''D'',''S'')   

-- PERSONS ADDRESS IS NOT CHANGED IN PAST 48 MONTHS (ACCUZIP)...
and isnull(ta.ncoa48flag,0) = 0   

--Muhammad Muneer 8465 11/11/2010 added the new functionality that is null checks for the first name and the last name
and LEN(ISNULL(RTRIM(LTRIM(ta.lastname)),''''))<>0
and LEN(ISNULL(RTRIM(LTRIM(ta.firstname)),''''))<>0


-- NOT MARKED AS STOP MAILING...
and  donotmailflag = 0  

--Rab Nawaz Khan 9119 04/04/2011 added attorney association logic.
and len(isnull(tva.attorneyname, '''')+isnull(tva.barcardnumber,'''')) = 0 
and CHARINDEX(''KUBOSH'',isnull(tva.BondingCompanyName,''''))=0
-- Sabir Khan 10245 05/03/2012
and isnull(ta.IsIncorrectMidNum,0) = 0   
'                                        

-- FOR THE SELECTED DATE RANGE.....
set @sqlquery=@sqlquery+'    
and   ' + dbo.Get_String_Concat_With_DatePart_ver2 ( ''+  @startlistdate + '', 'tva.bonddate' )     


-- IF PRINTING FOR ALL COURTS OF THE SELECTED CATEGORY.....
if(@crtid = @catnum )                  
	set @sqlquery=@sqlquery+'     
	and  tva.courtlocation In (Select courtid from tblcourts where courtcategorynum = '+CONVERT(vARCHAR,@CATNUM)+' )'                      

-- IF PRINTING FOR AN INDIVIDUAL COURT OF THE SELECTED CATEGORY...
ELSE 
	set @sqlquery =@sqlquery +'                                      
	and  tva.courtlocation =  ' +convert(varchar(10),@crtid)                                     
                                      
                                        
-- LMS FILTERS SECTION:
-----------------------------------------------------------------------------------------------------------------------------------------------------
-- IF THERE IS ANY FILTER SPECIFIED TO INCLUDE/EXCLUDE ONLY PARTICULAR OFFICER NUMBERS....
if(@officernum<>'')                                        
	set @sqlquery =@sqlquery + '      
	and  ta.officerNumber_Fk '+@officeropr +'('+ @officernum+')'                                         
                                        
-- IF THERE IS ANY FILTER DEFINED FOR INCLUDE/EXCLUDE TICKET NUMBER...                                    
if(@ticket_no<>'')                                        
	set @sqlquery=@sqlquery+ '    
	and   ('+ dbo.Get_String_Concat_With_op(''+@ticket_no+'' , 'tva.TicketNumber_PK' ,+ ''+ @tikcetnumberopr)+')'                                              
                                        
-- ZIP CODE FILTER......
-- INCLUDE/EXCLUDE THE SPECIFIED ZIP CODES.....                       
if(@zipcode<>'')                                                              
	set @sqlquery=@sqlquery+ '     
	and ('+  dbo.Get_String_Concat_With_op(''+@zipcode+'' , 'left(ta.zipcode,3)' ,+ ''+ @zipcodeopr) +')'                    
  
-- FINE AMOUNT FILTER.....
-- INCLUDE ONLY THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT....
if(@fineamount<>0 and @fineamountOpr<>'not'  )                  
	set @sqlquery =@sqlquery+ ' 
    and tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                       
  
-- EXCLUDE THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT.....
if(@fineamount<>0 and @fineamountOpr = 'not'  )                  
	set @sqlquery =@sqlquery+ '   
	and not tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                       

-- ECLUDE THE LETTERS THAT HAVE BEEN ALREADY PRINTED......
-- OR FTA LETTER PRINTED IN PAST WEEK FOR THE SAME RECORD...  
set @sqlquery =@sqlquery+ '  
-- Rab Nawaz 9951 12/21/2011 Exclude already printed letter 
and ta.recordid not in (                                                                      
 select n.recordid from tblletternotes n 
 where  n.lettertype ='+convert(varchar(10),@lettertype)+'     
 union  
 -- exclude FTA letters printed between last seven days...  
 -- Rab Nawaz 9951 12/21/2011 Exclude the letters which prited in last 3 days
 select nts.recordid from tblletternotes nts
 where lettertype IN (9, 12, 13, 25, 41, 58) -- HMC Arraignment 1, HMC Week Before Arraignment, HMC Judge Letter, HMC Warrant, HMC Warrant (30-45), HMC Jury Letter 
 AND datediff(day, nts.recordloaddate, dbo.fn_getprevbusinessday(GETDATE(),3)) <=0
 ) 
  
  
-- GETTING VIOL COUNT & TOTAL FINE AMOUNT FOR EACH CLIENT .....
select left(zipcode,5)+ltrim(midnumber) as zipmid ,   
 count(violationnumber_pk) as violcount,  
 isnull(sum(isnull(fineamount,0)),0) as total_fineamount  
into #temp6  
from #temptable    
group by left(zipcode,5)+ltrim(midnumber)  
  
alter table #temptable add violcount int , total_fineamount money  
  
update a   
set a.violcount = b.violcount,   
 a.total_fineamount = b.total_fineamount  
from #temptable a, #temp6 b where a.zipmid = b.zipmid  

-- GETTING RECORDS IN TEMP TABLE...
select  * into #temp from #temptable where 1 = 1  
'  

-- SINGLE VIOLATION FINE AMOUNT FILTER....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE SINGLE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT
if(@singleviolation<>0 and @doubleviolation=0)                                  
	set @sqlquery =@sqlquery+ '  
	and (  '+ @singlevoilationOpr+  '  (total_fineamount '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and violcount=1))  
      '                                  
-- DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...
if(@doubleviolation<>0 and @singleviolation=0 )                                  
	set @sqlquery =@sqlquery + '  
	and ('+ @doublevoilationOpr+  '   (total_fineamount '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ')and violcount=2) )  
      '                                                      

-- BOTH SINGLE & DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE AND SINGLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...                                                                        
if(@doubleviolation<>0 and @singleviolation<>0)                                  
	set @sqlquery =@sqlquery+ '  
	and ('+ @singlevoilationOpr+  '  (total_fineamount '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and violcount=1))                                  
    and ('+ @doublevoilationOpr+  '  (total_fineamount '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ') and violcount=2))  
        '                                    
                                
-- VIOLATION DESCRIPTION FILTER.....
-- EXCLUDE THE LETTER IF SPECIFIED VIOLATION IS THE ONLY VIOLATION IN THE LETTER....
if(@violadesc <> '')  
	BEGIN
		-- NOT LIKE FILTER.......  
		if(charindex('not',@violaop)<> 0 )                
			BEGIN
				set @sqlquery=@sqlquery+' 
				-- Rab Nawaz Khan 9659 09/06/2011 Filter Setting has been changed  
				and  not (   ( '+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  violationdescription  ' ,+ ''+ 'like')+'))'             	
			END
		-- INCLUDE ONLY THOSE LETTER THAT SINGLE VIOLATION AND MATCHES THE SPECIFIED VIOLATION DESCRIPTION...
		-- LIKE FILTER.......
		--if(charindex('not',@violaop) =  0 )                
		ELSE
			BEGIN
				set @sqlquery=@sqlquery+'  
				-- Rab Nawaz Khan 9659 09/06/2011 Filter Setting has been changed 
				and  (  ( '+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  violationdescription  ' ,+ ''+ 'like')+'))'            		
			END	
	END
                
  
-- GETTING RECORDS IN TEMP TABLE.....                            
set @sqlquery=@sqlquery+'    
    
Select distinct recordid, violationstatusid,ViolCount,Total_Fineamount,  listdate as mdate,FirstName,LastName,address,                                  
 city,state,FineAmount,violationdescription,CourtName,dpc,dptwo,Flag1,officerNumber_Fk,TicketNumber_PK, courtdate,                                        
 courtid,donotmailflag,clientflag, zipcode, midnumber as midnum, left(zipcode,5) + midnumber as zipmid into #temp1  from #temp                                        


-- tahir 4624 09/01/2008 exclude client cases
delete from #temp1 where recordid in (
select v.recordid from tblticketsviolations v inner join tbltickets t 
on t.ticketid_pk = v.ticketid_pk and t.activeflag = 1 
inner join #temp1 a on a.recordid = v.recordid
)
-- end 4624
 '                  

-- Rab Nawaz Khan 10331 11/12/2012 added to hide the entire columm in mailer if fine amount is zero agains any violation . . .  
set @sqlquery=@sqlquery+'
ALTER TABLE #temp1 
ADD isfineamount BIT NOT NULL DEFAULT 1  
 
SELECT recordid INTO #fine FROM #temp1  WHERE ISNULL(FineAmount,0) = 0 
 
UPDATE t   
SET t.isfineAmount = 0  
FROM #temp1 t INNER JOIN #fine f ON t.recordid = f.recordid '                  

             
-- IF "MARK LETTER AS SENT" OPTION IS SELECTED....
-- CREATE LETTER HISTORY FOR EACH PERSON....
If( @printtype<>0) 
	BEGIN            
		set @sqlquery2 =@sqlquery2 +    
		'    
		select * into #temp2 from #temp1                                       

		Declare @ListdateVal DateTime,                                      
		@totalrecs int,                                    
		@count int,                                    
		@p_EachLetter money,                                  
		@recordid varchar(50),                                  
		@zipcode   varchar(12),                                  
		@maxbatch int  ,    
		@lCourtId int  ,    
		@dptwo varchar(10),    
		@dpc varchar(10)  ,  @tempzipmid varchar(50), @midnum  varchar(20),  @tempidentity  varchar(50), @zipmid varchar(50)  
		declare @tempBatchIDs table (batchid int)  

		-- GETTING TOTAL LETTERS AND COSTING ......
		Select @totalrecs =count(zipmid) from #temp1 where mdate = @ListdateVal                                     
		Select @count=Count(zipmid) from #temp1                                    

		if @count>=500                                    
			set @p_EachLetter=convert(money,0.3)                                    
		                                
		if @count<500                                    
			set @p_EachLetter=convert(money,0.39)                                    

		-- DECLARING CURSOR TO INSERT THE RECORDS IN LETTER HISTORY ONE BY ONE.....
		-- THERE WILL A SEPARATE BATCH FOR EACH LIST/COURT DATE VALUE ......                                  
		Declare ListdateCur Cursor for                                      
		Select distinct mdate  from #temp1                                    
		open ListdateCur                                        
		Fetch Next from ListdateCur into @ListdateVal                                                          
		while (@@Fetch_Status=0)                                  
			begin                                    
				Select @totalrecs =count(distinct zipmid) from #temp1 where mdate = @ListdateVal                                     

				-- INSERTING RECORD IN BATCH TABLE......                                   
				insert into tblBatchLetter(Empid, Lettertype,Listdate,CourtId,ParentID,LCount,PExpense) values                                      
				('+convert(varchar(10),@empid)+' ,'+convert(varchar(10),@lettertype)+', @ListdateVal, '+convert(varchar(10),@catnum)+', 0, @totalrecs, @p_EachLetter)                                       

				-- GETTING BATCH ID OF THE INSERTED RECORD.....
				Select @maxbatch=Max(BatchId) from tblBatchLetter    
				insert into @tempBatchIDs select @maxbatch                                                         

				-- CREATING CURSOR TO INSERT RECORDS FOR EACH LETTER FOR THE NEWLY INSERTED BATCH....                                                 
				Declare RecordidCur Cursor for                                   
				Select  recordid,zipcode, courtid, dptwo, dpc,midnum, zipmid from #temp2 where mdate = @ListdateVal order by zipmid   
				open RecordidCur                                                             
				Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc,@midnum, @zipmid                                              

				-- INSERTING LETTERS IN LETTER HISTORY FOR THE SPECIFIED BATCH...
				while(@@Fetch_Status=0)                                  
					begin    
						if(@zipmid = @tempzipmid)   
							begin    
								insert into tblletternotes_detail (noteid, recordid) select @tempidentity, @recordid  
							end  
						else  
							begin                           
								insert into tblLetterNotes(BatchId_Fk,listdate,ZipCode,RecordID,PCost,LetterType, courtid, dp2, dpc,midnumber)                                  
								values (@maxbatch,@ListdateVal,@zipcode,@recordid,@p_EachLetter,'+convert(varchar(10),@lettertype)+' ,@lCourtId, @dptwo, @dpc, @midnum)    
								set @tempidentity =@@identity  
								set @tempzipmid = @zipmid   
								insert into tblletternotes_detail (noteid, recordid) select @tempidentity, @recordid  
							end   
						Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc,@midnum , @zipmid   
					end  
		                                     
				close RecordidCur     
				deallocate RecordidCur                                                                   
				Fetch Next from ListdateCur into @ListdateVal                                  
			end                                                

		close ListdateCur      
		deallocate ListdateCur     

		-- OUTPUTTING THE DATA TO THE APPLICATION FOR REPROT FILE.....
		Select distinct convert(varchar(20),n.noteid) as letterid, t.recordid,  t.FirstName,                                  
		t.LastName,t.address,t.city, t.state,FineAmount,violationdescription,CourtName, t.dpc,                                  
		dptwo, TicketNumber_PK, n.zipcode , courtdate , n.midnumber as midnum, n.midnumber, left(n.zipcode,5) + n.midnumber as zipmid, '''+@user+''' as Abb, t.isfineamount                                  
		from #temp1 t , tblletternotes n, @tempBatchIDs tb, tblletternotes_detail d  
		where t.recordid = d.recordid and t.courtid = n.courtid and n.batchid_fk = tb.batchid and n.lettertype = '+convert(varchar(10),@lettertype)+'   
		and d.noteid = n.noteid  
		order by [zipmid]  

		-- GENERATING AND SENDING EMAIL FOR LETTER PRINTING NOTIFICATION........
		declare @lettercount int, @subject varchar(200), @body varchar(400) , @sql varchar(500)  
		select @lettercount = count(distinct left(zipcode,5)+midnumber) from tblletternotes n, @tempBatchIDs b where n.batchid_fk = b.batchid  
		select @subject = convert(varchar(20), @lettercount) +  '' LMS HMC DLQ Letters Printed''  
		select @body = convert(varchar(20), @lettercount) + '' LETTERS ('' + CONVERt(VARCHAR(12), GETDATE()) + '') - ''+ upper( '''+@user+''')  
		exec usp_mailer_send_Email @subject, @body  

		-- OUTPUTING THE PDF FILE NAME TO THE APPLICATION...
		declare @batchIDs_filname varchar(500)  
		set @batchIDs_filname = ''''  
		select @batchIDs_filname = @batchIDs_filname + convert(varchar,batchid) + '',''  from @tempBatchIDs  
		select isnull(@batchIDs_filname,'''') as batchid  

		drop table #temp2  
		'    
	END    
else  
	-- IF "MARK LETTER AS SENT" OPTION IS NOT SELECTED....
	-- THEN DO NOT CREATE LETTER HISTORY. JUST OUTPUT THE DATA FOR REPORT FILE 
	-- TO DISPLAY THE LETTER...  
	BEGIN
		set @sqlquery2 = @sqlquery2 + '    
		Select distinct ''NOT PRINTABLE'' as letterId,  recordid,  FirstName,                         
		LastName,address,city, state,FineAmount,violationdescription,CourtName, dpc, dptwo, TicketNumber_PK, zipcode , courtdate  ,  
		midnum , left(zipcode,5)+midnum as zipmid, '''+@user+''' as Abb, isfineamount 
		from #temp1     
		order by [zipmid]  
		'    
	END

-- DROPPING TEMPORARY TABLES ....
set @sqlquery2 = @sqlquery2 + '    
    
drop table #temp     
drop table #temp1 drop table #temp6 drop table #temptable'    
                                        
--print @sqlquery + @sqlquery2   

-- CONCATENATING THE DYNAMIC QUERY.... 
set @sqlquery = @sqlquery + @sqlquery2    

-- EXECUTING THE DYNAMIC SQL ....
exec (@sqlquery)  


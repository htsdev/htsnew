﻿/*
*  Created By : Abid Ali
*  Created Date : 02/09/2009
*  Task ID : 5359
*  
*  Busines Logic : Check certified mail number is exist on particular date
*  
*  Parameter List:
*				@CertifiedMailNumber : Certified mail number
*				@CourtId : Court Id
*				
*	Column Reture :
*				CMNExistsOnCurrentDate
*				IsCMNExist
*/

CREATE PROCEDURE [dbo].[USP_HTP_Check_CertifiedMailNumber]
(@CertifiedMailNumber VARCHAR(20),
@CourtId AS INT)
AS
	DECLARE @PrintDate AS DATETIME	
	
	SELECT TOP 1 @PrintDate = printdate
	FROM   tblHTSBatchPrintLetter thpl
	INNER JOIN tblTicketsViolations ttv ON thpl.TicketID_FK = ttv.TicketID_PK
	WHERE  thpl.USPSTrackingNumber = @CertifiedMailNumber AND  ttv.CourtID = @CourtId
	
	DECLARE @IsSameDay BIT 
	DECLARE @IsNotExist BIT
	
	IF @PrintDate IS NULL
	BEGIN
		SELECT TOP 1 @PrintDate = printdate
		FROM   tblHTSBatchPrintLetter thpl
		INNER JOIN tblTicketsViolations ttv ON thpl.TicketID_FK = ttv.TicketID_PK
		WHERE  thpl.USPSTrackingNumber = @CertifiedMailNumber
		
		IF @PrintDate IS NULL
			SET @IsNotExist = 0
		ELSE
			SET @IsNotExist = 1
			
	    SET @IsSameDay = 0
	END
	ELSE
	BEGIN
		SET @IsNotExist = 1
		IF CONVERT(VARCHAR, @PrintDate, 101) = CONVERT(VARCHAR, GETDATE(), 101)
			SET @IsSameDay = 1
		ELSE
			SET @IsSameDay = 0
	END    
	
	
	
	SELECT @IsSameDay AS CMNExistsOnCurrentDate,
	       @IsNotExist AS IsCMNExist
GO






GRANT EXEC ON [dbo].[USP_HTP_Check_CertifiedMailNumber] TO dbr_webuser

GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_GET_EMPLOYEE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_GET_EMPLOYEE]
GO


Create Procedure USP_HTS_GET_EMPLOYEE
@employeeid varchar(100)
as

select lastname + ',' + firstname as clientname from tblusers where employeeid = @employeeid


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


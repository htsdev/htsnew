


/****************
Noufil 5772 04/10/2009
Business Logic : This Stored Procedure is used to Judge report in Trial Docket Report 
all HMC clients whose Who are in judge status. CLients will be fliter out throught court date.
List of Parameters :
@courtdate 
@URL 
******/
-- USP_HTP_GET_JUDGEREPORT_FOR_TRIALDOCKET '4/15/2009', '545'
ALTER PROCEDURE [dbo].[USP_HTP_GET_JUDGEREPORT_FOR_TRIALDOCKET]
	@courtdate DATETIME,
	@url VARCHAR(MAX),
	@Courtloc VARCHAR(200)
AS
--Fahad 10378 01/09/2013 HMC-W also excluded along other HMC Courts
if @Courtloc!='0' AND @Courtloc!='Traffic' and @Courtloc!='none' and @Courtloc!='OU' AND @Courtloc!='IN' and @Courtloc!='3001' and @Courtloc!='3002' and @Courtloc!='3003' and @Courtloc!='3075' 
	BEGIN
		EXEC usp_hts_get_trial_docket_report_CR @Courtloc, '01/01/1900', 'NONE', '-1', '5,2', 0, @url, 1,0,1 -- Noufil 5895 05/11/2009 new parameter added -- Afaq 7577 03/25/2010 Arraignment category included too.
	END
ELSE
	BEGIN
		EXEC usp_hts_get_trial_docket_report_CR 'IN', @courtdate, 'TRIAL', '-1', '5,2', 0, @url, 1,0,1 -- Noufil 5895 05/11/2009 new parameter added -- Afaq 7577 03/25/2010 Arraignment category included too. -- Adil 7635 04/07/2010 CourtLoc "IN" to display only inside court cases.
	END





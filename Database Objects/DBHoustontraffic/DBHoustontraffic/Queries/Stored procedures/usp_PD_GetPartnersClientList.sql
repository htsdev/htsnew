USE TrafficTickets
GO
/****** Object:  StoredProcedure [dbo].[usp_PD_GetPartnersClientList]    Script Date: 09/15/2011 15:03:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[usp_PD_GetPartnersClientList]    
  
 @Partner INT, 
 @Status INT,     
 @Active INT     
AS    
EXEC  ProductDefects.dbo.usp_PD_GetPartnersClientList @Partner,@Status,@Active 
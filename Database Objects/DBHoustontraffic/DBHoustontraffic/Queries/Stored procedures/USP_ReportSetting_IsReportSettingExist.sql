set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

 
/*  
* Created By		: SAEED AHMED
* Task ID			: 8101  
* Created Date		: 09/27/2010  
* Business logic	: This procedure check the existance of duplicate reoced based on input paramter.
*					: 
* Parameter List	
  @ReportID			: Report Id.
  @CourtID			: Copurt Id
  @AttributeId		: Attribute Id.
* 
* USP_ReportSetting_IsReportSettingExist 52,3051,1
*/ 
CREATE PROC [dbo].[USP_ReportSetting_IsReportSettingExist]
@ReportID INT,
@CourtID INT,
@AttributeId INT
AS

IF EXISTS(SELECT ReportSettingId FROM   ReportSetting WHERE  ReportID = @ReportID AND CourtID = @CourtID AND AttributeId = @AttributeId)
BEGIN
SELECT 'true'
END
ELSE
	BEGIN
SELECT 'false'
	END

GO
GRANT EXECUTE ON [dbo].[USP_ReportSetting_IsReportSettingExist] TO dbr_webuser
GO

/******   
Business Logic : 
display only those records that meet the following criteria 
1) Is Active Client 
2) Case is not disposed 
3) Should be HMC courts
******/     
alter Procedure [dbo].[usp_HTS_ArraignmentSnapshotLongFooterJury_ver_1_Update]                
AS                
SET NOCOUNT ON                  
 create TABLE [#tblTickets] (                  
 [TicketID_PK] [int] NOT NULL ,  
 [refcasenumber][varchar] (20) NULL,                    
 [Lastname] [varchar] (20) NULL ,                  
 [Firstname] [varchar] (20)  NULL ,                  
 [Violations] [varchar] (50) NULL,                  
 [DLNumber] [varchar] (30) NULL ,                  
 [DOB] [datetime] NULL ,                  
 [MID] [varchar] (20) NULL ,                  
 [BondFlag] [int] NULL ,                    
 [CurrentCourtNum] varchar(3) NULL,                  
 [CurrentDateSet] [datetime] NULL,                  
 [OfficerDay]  [varchar] (30) NULL,                  
 [SEQ] int NULL,          
 [CauseNumber] [varchar] (20) NULL ,
 --7629 Asad Ali 04/01/2010 
 IsRstFlagId  INT NULL,
 TicketsViolationID INT ,
 --Muhammad Ali 7629 06/17/2010 -- add New Flag
 PRFlag SMALLINT,
 --Muhammad Ali 8237 09/08/2010 --> Add new Flags..
 accidentflag TINYINT,
 HTMLaccidentflag VARCHAR(50),
 HTMLPRFlag VARCHAR(50),
 HTMLBondFlag VARCHAR(50),
 HTMLIsRstFlagId VARCHAR(50)
 
)                  
---   DROP TABLE  #tblTickets
 --Muhammad Ali 7629 06/17/2010 -- Add PRFlag into insert, select statment and innerjoin add   
 --Muhammad Ali 8237 09/08/2010 --Add AccidentFlag...           
 INSERT INTO #tblTickets (TicketID_PK, refcasenumber, Lastname, Firstname, DLNumber, DOB, MID,  BondFlag, CurrentCourtNum, CurrentDateSet, OfficerDay,CauseNumber ,IsRstFlagId,TicketsViolationID,accidentflag)                  
 SELECT  distinct TT.TicketID_PK, TV.Refcasenumber,  TT.Lastname,                   
 TT.Firstname, TT.DLNumber, TT.DOB,TT.MidNum , TT.BondFlag,  TV.CourtNumbermain , TV.CourtDateMain, O.OfficerType, TV.casenumassignedbycourt,NULL,tv.TicketsViolationID,tt.AccidentFlag
 FROM         tblTickets TT 
 INNER JOIN           tblTicketsViolations TV ON TT.TicketID_PK = TV.TicketID_PK INNER JOIN                  
                      tblViolations TRV ON TV.ViolationNumber_PK = TRV.ViolationNumber_PK INNER JOIN
                      tblCourtViolationStatus CV ON TV.CourtViolationStatusIDmain = CV.CourtViolationStatusID  LEFT OUTER JOIN                  
                      tblOfficer O ON TT.OfficerNumber = O.OfficerNumber_PK            
   WHERE     (CV.CourtViolationStatusID in (select courtviolationstatusid from tblcourtviolationstatus where categoryid = 5))               
 --Sabir Khan 7629 06/18/2010 Flag column is removed from select list added by ali....
 --Asad Ali  7629 04/07/2010 Should be today or past date check
 --Sabir Khan 7995 07/07/2010 All cases having court date in past, today or in future....
 --AND ((DATEDIFF([day],GETDATE(), TV.courtdatemain) <= 0) )                  
 and TRV.ViolationType not IN (1)                  
 AND (TT.Activeflag = 1) AND                   
 CV.CategoryID <> 50 and                   
 (TV.CourtID IN(3001, 3002, 3003, 3075)) -- Rab Nawaz Khan 10327 06/14/2012 Added the HMC-W Court in Judge Trail Report. . .
 ORDER BY TV.CourtDateMain ASC                  
  
                  
--Asad Ali  7629 04/07/2010 Updating 
UPDATE #tblTickets SET IsRstFlagId=ttf.FlagID FROM 
tblTicketsFlag ttf WHERE  ttf.TicketID_PK=#tblTickets.TicketID_PK AND ttf.FlagID=25
               
--Muhammad Ali 7629 06/18/2010  --> Update
UPDATE #tblTickets SET   PRFlag = ttf.FlagID FROM 
tblTicketsFlag ttf WHERE   ttf.TicketID_PK=#tblTickets.TicketID_PK AND ttf.FlagID=20
               
                  
 DECLARE @TicketID int                  
 DECLARE @TicketNumber varchar(12)                  
 DECLARE @Violations varchar(50)                  
 DECLARE @SEQ int                  
 DECLARE @TicketID2 varchar(50)                  
                  
 SELECT @SEQ =1                  
                  
 DECLARE CurWeekDays CURSOR READ_ONLY FOR                   
 Select TicketID_PK, refcasenumber From #tblTickets                   
 ORDER BY                  
 CurrentDateSet ASC  ,                  
 TicketID_PK ASC ,                  
 refcasenumber ASC                  
                  
 OPEN CurWeekDays                  
 FETCH NEXT FROM CurWeekDays INTO @TicketID , @TicketNumber                   
                  
 Select @TicketID2 = @TicketID                  
                  
 WHILE (@@fetch_status <> -1)                  
 BEGIN                  
  SELECT @Violations = coalesce(@Violations + ', ',' ') + Convert(varchar,TV.refcasenumber)                   
  FROM dbo.tblTicketsViolations TV INNER JOIN dbo.tblViolations TRV ON                  
  TV.ViolationNumber_PK = TRV.ViolationNumber_PK                   
  WHERE TRV.ViolationType IN (0,2,8)                  
  AND TV.ViolationStatusID = 1                  
  AND TV.TicketID_PK =  @TicketID                   
  AND TV.RefCaseNumber = @TicketNumber                  
   UPDATE #tblTickets                   
   Set #tblTickets.Violations= @Violations ,                  
   SEQ = @SEQ                  
   WHERE  #tblTickets.TicketID_PK = @TicketID AND                  
   #tblTickets.refcasenumber = @TicketNumber                  
                     
   SELECT @TicketNumber = NULL               
   SELECT @Violations = NULL                   
   SELECT @TicketID = NULL                  
                     
   FETCH NEXT FROM CurWeekDays INTO @TicketID , @TicketNumber                   
                  
  IF (@TicketID2 <> @TicketID)                 
  BEGIN                  
   SELECT @SEQ = @SEQ + 1                  
  END                  
 END                  
 CLOSE CurWeekDays                  
 DEALLOCATE CurWeekDays           
--Muhammad Ali 8237 09/08/2010 -->  update *, PR, B, NO RST Flags text have been added....into Temp table coloumn...                 
  UPDATE #tblTickets SET HTMLaccidentflag = (CASE WHEN ISNULL(accidentflag,0)=1 THEN '*; ' ELSE '' END),
 HTMLPRFlag = (CASE WHEN PRFlag=20 THEN '&nbsp<font color=red>PR</font>' ELSE '' END),
 HTMLBondFlag = (CASE WHEN ISNULL(BondFlag,0)=1 THEN 'b; ' ELSE '' END ),
 HTMLIsRstFlagId = (CASE WHEN ISNULL (IsRstFlagId,0)=25 THEN 'NO RST; ' ELSE '' END )                 
                  
 SELECT  t.TicketID_PK,(CASE isnull(t.CauseNumber,'') when '' then t.refcasenumber else t.CauseNumber end) as TicketNumber_PK , t.Lastname, t.Firstname , Violations =                   
 CASE LEFT(UPPER(t.refcasenumber),1)           
 WHEN 'F' THEN NULL                  
 ELSE Violations                   
 END , t.DLNumber , t.DOB ,t.MID , 
 --Muhammad Ali 8237 09/08/2010 Remove cases and optimize logic...
 BondFlag =   HTMLaccidentflag + HTMLBondFlag + HTMLIsRstFlagId + HTMLPRFlag                 

 --Asad Ali 7629 04/01/2010 add flag 
 --Muhammad Ali 7629 06/17/2010 Add Flag and Check all Conditions for All 3 Flags and Create Column for Report that contain Simple text not in HTML format 
 --CASE  WHEN ISNULL(BondFlag,0)=1 AND IsRstFlagId=25 AND t.PRFlag=20 THEN 'b; NO RST; &nbsp;<font color=red>PR</font>' 
 --WHEN ISNULL(BondFlag,0)=0 AND IsRstFlagId=25 AND t.PRFlag=20 THEN 'NO RST; &nbsp;<font color=red>PR</font>'
 --WHEN ISNULL(BondFlag,0)=1 AND IsRstFlagId=25 THEN 'b; NO RST'
 --WHEN BondFlag=1  AND t.PRFlag=20 THEN 'b; &nbsp;<font color=red>PR</font>' 
--WHEN ISNULL(BondFlag,0)=0 AND IsRstFlagId=25 THEN 'NO RST'
--WHEN BondFlag=1 THEN 'b' 
--WHEN t.PRFlag=20 THEN '&nbsp;<font color=red>PR</font>'
 --ELSE NULL
 --END
 --Implement logic to show RED 'PR' in print report
 ,RepBoundFlagWithoutRed=  HTMLaccidentflag+HTMLBondFlag + HTMLIsRstFlagId
  --CASE  WHEN ISNULL(BondFlag,0)=1 AND IsRstFlagId=25 AND t.PRFlag=20 THEN 'b; NO RST; '
 --WHEN ISNULL(BondFlag,0)=0 AND IsRstFlagId=25 AND t.PRFlag=20 THEN 'NO RST; '
 --WHEN ISNULL(BondFlag,0)=1 AND IsRstFlagId=25 THEN 'b; NO RST'
 --WHEN BondFlag=1  AND t.PRFlag=20 THEN 'b; ' 
--WHEN ISNULL(BondFlag,0)=0 AND IsRstFlagId=25 THEN 'NO RST'
--WHEN BondFlag=1 THEN 'b' 
 --ELSE NULL
  --END
  ,RepBoundFlagRed=
  CASE WHEN t.PRFlag=20 THEN 'PR' ELSE '' 
  END
  -- END 8237
 --Muhammad Ali END-->Add Flag and Check all Conditions for All 3 Flags and Create Column for Report that contain Simple text not in HTML format 
 ,  t.CurrentCourtNum, t.CurrentDateSet, t.OfficerDay, t.SEQ, t.CauseNumber
 FROM #tblTickets t  
 ORDER BY                    
 CurrentDateSet ASC  ,                  
 TicketID_PK ASC ,                  
 refcasenumber ASC    
 
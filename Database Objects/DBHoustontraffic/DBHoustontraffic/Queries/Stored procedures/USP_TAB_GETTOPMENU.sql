/****** 
	Sabir Khan 5440 01/23/2009
	Business Logic: This procedure is used to Get active and selected Top menu with associated URL.
	Parameters : 
	Column Returns : IDMAIN, ID,TITLE,ORDERING,ACTIVE,URL,
******/
ALTER PROCEDURE [DBO].[USP_TAB_GETTOPMENU]
AS
	--Sabir Khan 5440 01/23/2009 Get spacific fields from tbl_tab_submenu...
	--SELECT * from  TBL_TAB_MENU where active = 1
	--ORDER BY TBL_TAB_MENU.ORDERING
	--Sabir Khan 5519 02/09/2009 add main menu id column...
	SELECT a.id AS idmain,
	       b.id,
	       a.TITLE,
	       a.ORDERING,
	       a.active,
	       b.url,
	       --Ozair 7077 03/10/2010 IsSubMenuHidden column added in select list
	       a.IsSubMenuHidden
	FROM   TBL_TAB_MENU a
	       INNER JOIN tbl_TAB_SUBMENU b
	            ON  a.id = b.menuid
	WHERE  a.active = 1
	       AND b.[active] = 1
	       AND b.SELECTED = 1
	ORDER BY
	       a.ORDERING



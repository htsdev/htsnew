SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Usp_Get_CourtDate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Usp_Get_CourtDate]
GO

      
--Usp_Get_CourtDate '10/5/2007'

CREATE PROCEDURE [dbo].[Usp_Get_CourtDate]            
@Date Datetime            
AS            
BEGIN            
        
select tva.TicketNumber_PK,tva.Courtdate, tva.ViolationStatusID from tblticketsarchive ta inner join tblticketsviolationsarchive tva            
on ta.RecordID = tva.RecordID            
WHERE     (tva.CourtLocation BETWEEN 3007 AND 3022) AND 
(DateDiff(day,ta.RecLoadDate,@Date)=0)
    and len(tva.TicketNumber_PK)=12      -- and tva.ViolationStatusID = 3
END 


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


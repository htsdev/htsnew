
/******   
Business Logic : 
display only those records that meet the following criteria 
1) Is Active Client 
2) Case is not disposed 
3) Probation Request is Active
******/     
ALTER  PROCEDURE [dbo].[USP_HTS_GET_PR_POA_REPORT]          
as          
 --Muhammad Ali 7629 06/23/2010 -->Remove Temp Table
          
SELECT distinct            
 t.TicketID_PK,            
 t.Lastname,           
 t.Firstname,           
 tv.RefCaseNumber AS TicketNumber,           
 tv.casenumassignedbycourt AS CauseNumber,           
 v.Description AS ViolationDescription,           
 t.TrialComments,           
 tblCourtViolationStatus.ShortDescription AS Status,           
 tv.CourtDateMain AS CourtDate,           
 tv.CourtNumbermain AS CourtRoom,         
 --Muhammad Ali 7629 06/17/2010 -- Comment Flag Column 
 --Muhammad Ali 7629 06/23/2010 -- Retrive All Flag Except PR flag as per new Requirements and Remove extra feilds from temp table
Flag=
CASE
	 when tv.UnderlyingBondFlag = 1 AND sb.DocTypeID = 14 then 'B; POA'
	when tv.UnderlyingBondFlag = 1  then 'B'
		when sb.DocTypeID = 14  	then 'POA'
	ELSE ''
END,
--Asad Ali 7995 07/23/2010  add columns 
ISNULL(tv.UnderlyingBondFlag,0) UnderlyingBondFlag,
ISNULL(sb.DocTypeID,0) DocTypeID
 ----------------------Previouse Code related to 'Muhammad Ali 7629 06/17/2010 Comment Flag Column' ------------------------------------------------  
-- case  	when tf.FlagID = 20  then '&nbsp;<font color=red>PR</font>' 													
--   else ' ' end as PRFlag,          

--case  when sb.DocTypeID = 14  	then ' POA' else ' ' end as POAFlag     --sb.DocTypeID,

 --case  when tv.UnderlyingBondFlag = 1   then 'B'   else ' '   end as rBondFlag,           

-- case  	when tf.FlagID = 20  	then 'PR'  
-- 	else ' '   end as rPRFlag,          
 --case when sb.DocTypeID = 14 then ';POA' else ' ' end as rPOAFlag --,tf.FlagID
 ------------------------------------------------------------------------------------------
--End Muhammad Ali-- Retrive All Flag Except PR flag as per new Requirements and Remove extra feilds from temp table
      
FROM         tblTickets AS t INNER JOIN          
                      tblTicketsViolations AS tv ON t.TicketID_PK = tv.TicketID_PK INNER JOIN          
                      tblViolations AS v ON tv.ViolationNumber_PK = v.ViolationNumber_PK INNER JOIN          
                      tblCourtViolationStatus ON tv.CourtViolationStatusIDmain = tblCourtViolationStatus.CourtViolationStatusID INNER JOIN          
       tblTicketsFlag tf ON t.TicketID_PK = tf.TicketID_PK LEFT OUTER JOIN          
       tblScanBook sb ON t.TicketID_PK = sb.TicketID 
       AND sb.DocTypeID = 14 --Muhammad Ali 7629 06/23/2010 --> 'AND sb.DocTypeID = 14' added for only select POA 
       --- Asad Ali 7995 07/22/2010 get Document that are not Deleted
       AND ISNULL(IsDeleted,0)=0
         
WHERE         

  -- Asad Ali 7629  03/30/2010 --
  isnull(t.Activeflag,0)=1 --Active clients --          
  AND tv.CourtViolationStatusIDmain<>80 --Case is not disposed 
  AND  tf.FlagID =20
 --- 7629 End
      
ORDER BY          
--Muhammad Ali 8237 09/07/2010 --> Sort by Court date and Court Number..
 --t.LastName    
 tv.CourtDateMain, tv.CourtNumbermain 
-- Asad Ali 7629  03/30/2010 --

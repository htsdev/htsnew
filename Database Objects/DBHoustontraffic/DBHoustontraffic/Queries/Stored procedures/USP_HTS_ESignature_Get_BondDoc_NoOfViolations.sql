﻿  
  
/*declare @TotalViolations int  
exec USP_HTS_ESignature_Get_BondDoc_NoOfViolations 124546, @NoOfViolations = @TotalViolations output  
select @TotalViolations  
*/  
alter PROCEDURE [dbo].[USP_HTS_ESignature_Get_BondDoc_NoOfViolations]   
@TicketIDList int,  
@NoOfViolations int Output  
  
AS                    
                    
SET NOCOUNT ON                      
Declare @SQLString nvarchar(4000)                      
                    
SELECT  dbo.tblTicketsViolations.TicketID_PK, SUM(dbo.tblTicketsViolations.BondAmount) as BondAmount                      
 into #temp1                       
FROM dbo.tblTicketsViolations INNER JOIN                       
dbo.tblViolations ON dbo.tblTicketsViolations.violationnumber_pk = dbo.tblViolations.ViolationNumber_PK                       
  WHERE     (dbo.tblViolations.Violationtype  not in (1))                       
 and (dbo.tblTicketsViolations.underlyingbondflag=1 or dbo.tblTicketsViolations.courtviolationstatusid=135) --added by ozair for bug#1224                  
  and dbo.tblTicketsViolations.TicketID_PK = @TicketIDList                  
                  
 --AND  refcasenumber like 'f%'                     
 GROUP BY dbo.tblTicketsViolations.TicketID_PK                    
                      
                     
 SELECT  DISTINCT   dbo.tblTickets.TicketID_PK, dbo.tblTicketsViolations.RefCaseNumber, dbo.tblViolations.Description,                       
  dbo.tblTicketsViolations.BondAmount, dbo.tblTickets.Firstname, dbo.tblTickets.MiddleName, dbo.tblTickets.Lastname,                       
  dbo.tblTickets.Address1, dbo.tblTickets.Address2, dbo.tblTickets.Midnum, dbo.tblTickets.City, tblState_1.State, dbo.tblTickets.Zip,                       
  dbo.tblTickets.DLNumber, dbo.tblState.State AS DLState, dbo.tblTickets.DOB, dbo.tblTickets.SSNumber, dbo.tblTickets.Race,                       
  dbo.tblTickets.gender, dbo.tblTickets.Height, dbo.tblTickets.Weight, dbo.tblTickets.HairColor, dbo.tblTickets.Eyes,  dbo.tblTickets.languagespeak,                       
  dbo.tblCourts.CourtName as Crt_CourtName , dbo.tblCourts.Address as Crt_Address , dbo.tblCourts.Address2 as Crt_Address2,                       
  dbo.tblCourts.City as Crt_City , tblState_2.State as Crt_State , dbo.tblCourts.Zip as Crt_Zip, dbo.tblTicketsViolations.casenumassignedbycourt                       
  into #tbltemp                       
  FROM dbo.tblTicketsViolations INNER JOIN                       
  dbo.tblViolations ON dbo.tblViolations.ViolationNumber_PK = dbo.tblTicketsViolations.ViolationNumber_PK LEFT OUTER JOIN                       
  dbo.tblTickets ON dbo.tblTicketsViolations.TicketID_PK = dbo.tblTickets.TicketID_PK LEFT OUTER JOIN   dbo.tblState ON dbo.tblTickets.DLState = dbo.tblState.StateID  LEFT OUTER JOIN                       
  dbo.tblState tblState_1 ON dbo.tblTickets.Stateid_FK = tblState_1.StateID LEFT OUTER JOIN                       
  dbo.tblCourts ON dbo.tblCourts.courtid = dbo.tblTicketsViolations.courtid LEFT OUTER JOIN                       
  dbo.tblState tblState_2 ON dbo.tblCourts.State = tblState_2.StateID                       
  WHERE (dbo.tblViolations.ViolationType not in (1))                      
  and dbo.tblTickets.TicketID_PK = @TicketIDList                    
  and (dbo.tblTicketsViolations.underlyingbondflag=1 or dbo.tblTicketsViolations.courtviolationstatusid=135) --added by ozair for bug#1224                  
         and dbo.tblTicketsViolations.courtviolationstatusid<>80   --newly added                    
 --AND  refcasenumber like 'f%'                     
  ORDER BY dbo.tblTickets.TicketID_PK, dbo.tblTicketsViolations.RefCaseNumber  
                      
select  a.TicketID_PK, a.RefCaseNumber, a.[Description],  casenumassignedbycourt  ,                   
  a.BondAmount, a.Firstname, a.MiddleName, a.Lastname,  a.Address1, a.Address2, a.Midnum, a.City, a.State, a.Zip,                       
  a.DLNumber, a.DLState, a.DOB, a.SSNumber, a.Race, a.gender, a.Height, a.Weight, a.HairColor, a.Eyes,  a.languagespeak,                       
  a.Crt_CourtName , a.Crt_Address , a.Crt_Address2, a.Crt_City , a.Crt_State , a.Crt_Zip, b.BondAmount as totalBondAmount                    
  into #TbltempBond                    
 from #tbltemp a, #temp1 b where a.TicketID_PK = b.TicketID_PK                    
                    
alter table #tbltempbond                    
add dup varchar(25)                    
                    
alter table #tbltempbond                    
add Counter int                    
                    
                  
                    
                    
select * into #tbltempbond2                    
from #tbltempbond                    
                    
select * into #tbltempbond1                    
from #tbltempbond2                    
                    
                    
update #tbltempbond2 set dup = '*'                    
                    
select Top 1 *                     
into #tbltempbond3                    
from #tbltempbond2                    
                    
update #tbltempbond3 set dup = ''                    
                    
update #tbltempbond2 set Counter = 2                    
update #tbltempbond3 set  Counter = 3 ,  Description = '' ,  BondAmount = 0 , totalBondAmount = 0, TicketID_PK = Null, RefCaseNumber = Null                   
                    
update #tbltempbond1 set Counter = 1                    
                    
                    
insert into #tbltempbond2 (TicketID_PK, RefCaseNumber, [Description],  casenumassignedbycourt,                     
  BondAmount, Firstname, MiddleName, Lastname,  Address1, Address2, Midnum, City, State, Zip,                       
  DLNumber, DLState, DOB, SSNumber, Race, gender, Height, Weight, HairColor, Eyes,  languagespeak,                       
  Crt_CourtName , Crt_Address , Crt_Address2, Crt_City , Crt_State , Crt_Zip, totalBondAmount, dup,Counter)                    
select TicketID_PK, RefCaseNumber, [Description],   casenumassignedbycourt ,                   
  BondAmount, Firstname, MiddleName, Lastname,  Address1, Address2, Midnum, City, State, Zip,                       
  DLNumber, DLState, DOB, SSNumber, Race, gender, Height, Weight, HairColor, Eyes,  languagespeak,                       
  Crt_CourtName , Crt_Address , Crt_Address2, Crt_City , Crt_State , Crt_Zip, totalBondAmount, dup,Counter from  #tbltempbond1                    
union all                    
select TicketID_PK, RefCaseNumber, [Description],    casenumassignedbycourt ,                  
  BondAmount, Firstname, MiddleName, Lastname,  Address1, Address2, Midnum, City, State, Zip,                       
  DLNumber, DLState, DOB, SSNumber, Race, gender, Height, Weight, HairColor, Eyes,  languagespeak,                       
  Crt_CourtName , Crt_Address , Crt_Address2, Crt_City , Crt_State , Crt_Zip, totalBondAmount, dup, Counter from #tbltempbond3                    
                    
       
Set @NoOfViolations = (select (count(Counter) - 1) / 2 as VCount from #tbltempbond2)  
return @NoOfViolations  
  
  
  
  
  
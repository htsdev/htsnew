SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_MID_Insert_TicketPCCRStatus]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_MID_Insert_TicketPCCRStatus]
GO

  
-- PROCEDURE TO UPDATE THE QUOTE STATUS...                       
CREATE procedure [dbo].[usp_MID_Insert_TicketPCCRStatus]                       
 (                      
 @RecordIds varchar(200),   -- CASE NUMBERS THAT WILL HAVE TO BE UPDATED....                       
 @PCCRStatus int,  -- NEW QUOTE STATUS                        
 @Comments varchar(1980),  -- UPDATED COMMENTS...                      
 @Employeeid varchar(25),    -- EMPLOYEE IDENTIFICATION WHO UPDATED THE CASE....                      
 @IsCDL  int,  -- OTHER INPUT FLAGS....                      
 @PriceShopping bit,                      
 @HiredAnother  bit,                      
 @HandleMySelf bit,                      
 @PaidTicket bit,                      
 @DSCProb bit,                      
 @NoMoney bit,                      
 @TooExpensive  bit,                      
 @PoorService bit,                      
 @isAccident  bit,                      
 @isStatusChanged int,  -- FLAG TO CHECK IF QUOTE STATUS CHANGED OR NOT....                      
 @IsSignUp bit, -- FLAG FOR INSERTING ENTRY IF CLIENT SIGNUPs....              
 @NoContactFlag bit,                      
 @LanguageSpeak nvarchar(30) -- Language information                    
 )                      
         
    
as                      
                      
declare @RecCount int,                      
 @strabbr varchar(3),                      
 @strcomments varchar(1980),                      
 @strComm varchar(100),                      
 @strStatus varchar(50),                  
 @newCommentsFlag bit,
 @noContactUpdated bit                      
                  
set @noContactUpdated = 0
 set @newCommentsFlag = 0                      
 set @strcomments = ''                 
-- GETTING SHORT NAME FOR THE EMPLOYEE WHO UPDATED THE CASE....                      
SELECT @strabbr = Abbreviation FROM TBLUSERS where employeeid = @employeeid                      
                      
-- GETTING QUOTE STATUS DESCRIPTION.......                      
select @strStatus = statusdescription from tblpccrstatus where statusid = @pccrstatus                      
                      
-- FORMATTING COMMENTS AND DATE TIME FORMAT IN COMMENTS...                      
--select @strComm = '(' + @strstatus + ' - '+dbo.formatdateandtimeintoshortdateandtime(getdate())+ ' - ' + @strabbr + ')'                      
select @strComm = '(' +dbo.formatdateandtimeintoshortdateandtime(getdate()) +' - ' + @strabbr + ' - ALERT STATUS -' + @strstatus + ')'                
                      
                      
-- IF COMMENTS HAVE BEEN CHANGED..... THEN ADD AN ENTRY TO THE COMMENTS...                       
if @Comments <> '' and @Comments is not null and right(@Comments,1) <> ')'                      
 begin                
  if @NoContactFlag = 1              
   begin              
    set @strComments =  'NO CONTACT BOX SELECTED ' + @strComments; 
	set @noContactUpdated =1               
   end                    
  set @strcomments  = @comments + ' ' + @strComments+ ' (' + dbo.formatdateandtimeintoshortdateandtime(getdate()) + ' - ' + @strabbr + ' - ALERT STATUS - '+ @strstatus + ')'          
  set @newCommentsFlag = 1                      
 end                      
-- OTHER WISE DO NOT CHANGE THE ACTUAL COMMENTS....                      
else                      
 begin                      
  set @strcomments  = @comments                
 end                      
                      
                      
-- IF QUOTE STATUS HAS BEEN CHANGED FROM THE PREVIOUS STATUS THAN ADD ENTRY IN THE COMMENTS...                      
if @isStatusChanged = 1                      
 begin                      
 if @newCommentsFlag=1                  
  begin                  
               
   set @strcomments =  @comments --+ ' (' + dbo.formatdateandtimeintoshortdateandtime(getdate()) + ' - ' + @strabbr + '- ALERT STATUS - ' +@strStatus +')'                  
 if @NoContactFlag = 1              
   begin              
    set @strComments =  @strComments + ' NO CONTACT BOX SELECTED ' 
	set @noContactUpdated =1             
   end              
  set @strComments = @strComments + ' (' + dbo.formatdateandtimeintoshortdateandtime(getdate()) + ' - ' + @strabbr + '- ALERT STATUS - ' +@strStatus +')'            
  end                  
 else                 
  begin                
if @NoContactFlag = 1              
   begin              
    set @strComments =  @strComments + ' NO CONTACT BOX SELECTED ' 
	set @noContactUpdated =1            
   end                
     set @strcomments = @strComments + ' ' + @strComm                      
  end                  
 end                      
                      
              
--IF NO CONTACT IS SELETED             
/*if @NoContactFlag = 1              
 begin              
  set @strComments =  'No contact box selected ' + @strComments;              
 end  */            
                      
-- MAINTAINING SIGN UP HISTORY.......                      
if @IsSignUp = 1                      
 begin                      
  set @strcomments = @strComments + ' ( ALERT TO QUOTE' + ' - '+dbo.formatdateandtimeintoshortdateandtime(getdate())+ ' - ' + @strabbr + ')'                    
 end                      
                      
if @NoContactFlag =1 and @NoContactUpdated = 0
	begin
	set @strcomments = @strComments + ' ( NO CONTACT BOX SELECTED' + ' - '+dbo.formatdateandtimeintoshortdateandtime(getdate())+ ' - ' + @strabbr + ')'                    
	set @noContactUpdated =1    
	end  
  
declare @recIDs table   
(  
RecordID int,  
UpdateFlag bit  
)  
  
insert into @recIDs (RecordID) select * from dbo.sap_string_param(@RecordIds)  
  
update @recIDs  
 set UpdateFlag = 1   
where   
 RecordID IN (SELECT Recordid_fk from tblticketsarchivepccr where recordid_fk IN (select * from dbo.sap_string_param(@RecordIds)))  
  
    
  
update tblticketsarchivepccr                      
  set PCCRStatusId = @PCCRStatus,                      
   comments  = @strcomments,                      
   EmployeeId = @EmployeeId,                      
   IsCDL  = @IsCDL,                      
   PriceShopping = @priceShopping,                    
   HiredAnother  = @HiredAnother,                      
   LastUpdateDate  = getdate(),                      
   PaidTicket = @paidticket,                      
   HandleMySelf  = @HandleMySelf,                      
   dscprob  = @dscprob,                      
   nomoney  = @nomoney,                      
   tooexpensive = @tooexpensive,                      
   PoorService = @PoorService,                      
   isaccident = @isaccident,                      
   IsSignUp = @IsSignUp,                    
   LanguageSpeak = @LanguageSpeak                     
  where RecordId_fk in  ( select RecordID from @recIDs where UpdateFlag = 1)  
  
insert into tblticketsarchivepccr                      
   ( recordid_fk, pccrstatusid, comments, employeeid, IsCDL, priceshopping, hiredanother, handlemyself, paidticket, dscprob, nomoney, tooexpensive, poorservice, isaccident, issignup, LanguageSpeak , lastupdatedate)                      
  select RecordID, @pccrstatus, @strcomments, convert(int,@employeeid), @IsCDL ,@PriceShopping, @HiredAnother, @HandleMySelf, @PaidTicket, @DSCProb, @NoMoney, @TooExpensive, @PoorService, @isAccident, @IsSignUp, @LanguageSpeak, getdate() from @recIDs where isnull(UpdateFlag,0) <> 1  
  
  
/*  
                      
-- CHECK THE EXISTANCE OF THE RECORD.....                      
select  @RecCount =  (                      
   select count(*) from tblticketsarchivepccr                       
   where recordid_fk  in ( select * from dbo.sap_string_param(@RecordIds))                      
   )                      
  
  
                      
-- IF SELECTED RECORD ALREADY EXISTS IN THE TABLE THEN UPDATE IT WITH THE CURRENT INFORMATION....                      
if @RecCount > 0                      
 begin                      
  update tblticketsarchivepccr                      
  set PCCRStatusId = @PCCRStatus,                      
   comments  = @strcomments,                      
   EmployeeId = @EmployeeId,                      
   IsCDL  = @IsCDL,                      
   PriceShopping = @priceShopping,                    
   HiredAnother  = @HiredAnother,                      
   LastUpdateDate  = getdate(),                      
   PaidTicket = @paidticket,                      
   HandleMySelf  = @HandleMySelf,                      
   dscprob  = @dscprob,                      
   nomoney  = @nomoney,                     
   tooexpensive = @tooexpensive,                      
   PoorService = @PoorService,                      
   isaccident = @isaccident,                      
   IsSignUp = @IsSignUp,                    
   LanguageSpeak = @LanguageSpeak                     
  where RecordId_fk in  ( select * from dbo.sap_string_param(@RecordIds))                      
 end          
                
                      
-- OTHER WISE ADD A NEW ENTRY IN THE DATABASE .......                         
else                      
                      
 begin                      
  select @recordids = (select  distinct top 1 maincat from dbo.sap_string_param(@recordids))                      
  insert into tblticketsarchivepccr                      
   ( recordid_fk, pccrstatusid, comments, employeeid, IsCDL, priceshopping, hiredanother, handlemyself, paidticket, dscprob, nomoney, tooexpensive, poorservice, isaccident, issignup, LanguageSpeak )                      
  select recordid, @recordids, @pccrstatus, @strcomments, convert(int,@employeeid), @IsCDL ,@PriceShopping, @HiredAnother, @HandleMySelf, @PaidTicket, @DSCProb, @NoMoney, @TooExpensive, @PoorService, @isAccident, @IsSignUp, @LanguageSpeak from   
 end                      
                    */  
  

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


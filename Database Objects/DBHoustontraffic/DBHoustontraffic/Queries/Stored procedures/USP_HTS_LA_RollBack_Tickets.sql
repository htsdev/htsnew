SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_LA_RollBack_Tickets]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_LA_RollBack_Tickets]
GO



CREATE PROCEDURE USP_HTS_LA_RollBack_Tickets
@GroupID as Int
AS  
	Delete From tblTicketsViolationsArchive Where RecordID IN (Select RecordID From tblTicketsArchive Where GroupID = @GroupID)
	Delete From tblTicketsArchive Where GroupID = @GroupID


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


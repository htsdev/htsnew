﻿/******   
Created by:  Syed Muhammad Ozair 
Business Logic : this procedure is used to get records from usp_AD_Get_Calls by providing specific datetime  
       
List of Parameters:   
    @CallDate
    @TicketIDs
    @CallType
	@CourtDateStart
	@CourtDateEnd
     
******/ 


ALTER PROCEDURE dbo.usp_AD_Get_AllCalls
	@CallDate as DATETIME,
	--Ozair 7975 09/16/2010 Parameters modifid
	@TicketIDs as VARCHAR(MAX),
	@CallType AS INT,
	@CourtDateStart as DateTime,
	@CourtDateEnd as DateTime
AS
	SET NOCOUNT ON 
	
	CREATE TABLE #temp
	(
		ticketid            INT,
		EventTypeID         INT,
		EventTypeName       VARCHAR(50),
		lastname            VARCHAR(20),
		firstname           VARCHAR(20),
		languagespeak       VARCHAR(20),
		bondflag            VARCHAR(1),
		STATUS              INT,
		autodialerstatus    INT,
		contact             VARCHAR(50),
		Priority            INT,
		ContactType         INT,
		flagid              INT,
		callback            VARCHAR(50),
		trialdesc           VARCHAR(MAX),
		wrongnumberflag     INT,
		TicketViolationIds  VARCHAR(MAX),
		Response            VARCHAR(50),
		EventDateTime       VARCHAR(50)
	) 
	
	--Ozair 7975 09/16/2010 parameter @CallType used
	INSERT INTO #temp
	EXEC usp_AD_Get_Calls @CallType,
	     @CallDate,
	     3,
	     0,
	     @TicketIDs,
	     @CourtDateStart,
	     @CourtDateEnd
	     
	
	SELECT ticketid,
	       bondflag,
	       EventTypeName,
	       EventDateTime,
	       lastname,
	       firstname,
	       contact,
	       trialdesc,
	       Response,
	       ContactType,
	       EventTypeID,
	       languagespeak,
	       STATUS,
	       autodialerstatus,
	       Priority,	
	       flagid,
	       callback,
	       wrongnumberflag,
	       TicketViolationIds
	FROM   #temp
	
	DROP TABLE #temp
GO


GRANT EXEC ON dbo.usp_AD_Get_AllCalls TO dbr_webuser
GO
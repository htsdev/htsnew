SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Update_Awardee]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Update_Awardee]
GO




CREATE PROCEDURE [usp_Update_Awardee]
	(
	 @NAME 	[varchar](200),
	 @ADDRESS 	[varchar](200),
	 @CITY 	[varchar](50),
	 @STATE 	[varchar](50),
	 @ZIP 	             [varchar](50),
              @RecordKey [int],
@YDS VARCHAR(10),
@BARCODE VARCHAR(50),
@AWDNBR  VARCHAR(100)
--@Mail  VARCHAR(50)
) 
AS UPDATE [dbo].[AWARDS] 

SET  
	 [AWDNAME]	  = @NAME,
	 [AWDADDRESS] = @ADDRESS,
	 [AWDCITY]	 = @CITY,
	 [AWDSTATE]	 = @STATE,
	 [AWDZIP]	 = @ZIP,
               YDS                  = @YDS,
              BARCODE=@BARCODE,             
              AWDNBR=@AWDNBR,
--               Mail=@Mail,
              [LastUpdateDate]=getdate()

WHERE
              RecordKey=@RecordKey

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_GET_PricingPlans]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_GET_PricingPlans]
GO










--Getting Plan Description by Effective Date Ranges ,End Date Ranges,Court ID, PlanStatus  
CREATE procedure USP_HTS_GET_PricingPlans    
 (  
 @EffectiveDateFrom datetime, -- Effective Date Ranges  
 @EffectiveDateTo datetime, -- Effective Date Ranges  
 @EndDateFrom  datetime, -- End Date Ranges  
 @EndDateTo  datetime, -- End Date Ranges  
 @CourtId  int , -- Court ID  
 @ShowAll  bit  -- Plan Status   
 )  
  
  
as  
  
declare @strSQL  nvarchar(2000),   -- Query  
 @strParam nvarchar(500) -- Parameters  
  
select @strParam = '   
 @EffectiveDateFrom datetime,  
 @EffectiveDateTo datetime,  
 @EndDateFrom  datetime,  
 @EndDateTo  datetime,  
 @CourtId  int  
'  
  
  
select @strSQL ='  
SELECT pp.PlanId,
 pp.PlanShortName,   
 pp.PlanDescription,   
 pp.EffectiveDate,   
 pp.EndDate,   
 c.ShortName,   
 pp.IsActivePlan  
FROM    dbo.tblPricePlans pp   
INNER JOIN  
 dbo.tblCourts c   
ON  pp.CourtId = c.Courtid  
where 1=1  
'  
  
if @courtid <> 0  -- if not All Court is selected or no court is seleceted  
 begin  
  select @strSQL= @strSQL + '  
  and pp.courtid = @CourtId  
  '  
 end  
  
if @ShowAll = 0 -- setting the plan status   
 begin  
  select @strSQL = @strSQL + '  
   and pp.effectivedate between  @EffectiveDateFrom and @EffectiveDateTo  
   and pp.enddate between @EndDateFrom and @EndDateTo  
   '  
 end  
else  
 begin  
  select @strSQL = @strSQL + '  
  and pp.IsActivePlan = 1  
  '  
 end  
  
  
--print @strsql  
exec sp_executesql @strSQL, @strParam, @EffectiveDateFrom, @EffectiveDateTo, @EndDateFrom, @EndDateTo, @CourtId







GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


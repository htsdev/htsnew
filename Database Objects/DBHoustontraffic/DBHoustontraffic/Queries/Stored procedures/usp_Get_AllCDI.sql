SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_AllCDI]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_AllCDI]
GO

create PROCEDURE   [dbo].[usp_Get_AllCDI]
as
BEGIN
	
	SELECT  Code , '(' + Code + ') ' + Description as Description  from tblCDICodes
END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


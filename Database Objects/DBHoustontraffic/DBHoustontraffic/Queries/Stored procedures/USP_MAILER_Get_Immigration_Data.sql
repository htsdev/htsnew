
/****** 
Created by:		Rab Nawaz Khan
Task ID   :     10391
Date      :     08/29/2012
Business Logic:	The procedure is used by LMS application to get the number of printable letter
				count for the Immigration Letters for the selected date range for the Non-Clients.
				
List of Parameters:
	@catnum:		Category number of the selected letter type, if printing for 
					all courts of the selected court category
	@LetterType:	Selected letter type in LMS
	@CrtId:			Court id of the selected court category if printing for individual court hosue.
	@startListdate:	Starting list date for Arraignment letter.
	@endListDAte:	Ending list date for Arraignment letter.
	@SearchType:	Flag that identifies if searching by court date or by list date. 0= list date, 1 = court date

List of Columns:	
	Total_Count:	Total number of letters exists for the selected date range.
	Good_Count:		Total number of letters that have valid addresses
	Bad_Count:		Total Number of letter that have undeliverable addresses
	DonotMail_Count:Total number of letter for addresses that had been marked to stop sending letters.
	

******/

  -- [dbo].[USP_MAILER_Get_Immigration_Data]  130, '08/29/2011', '08/29/2012'
CREATE PROCEDURE [dbo].[USP_MAILER_Get_Immigration_Data] 
 (  
 @LetterType  int,  
 @startListdate  Datetime,  
 @endListdate  DateTime
 )  
  
as                                                                

Select distinct 
convert(datetime , convert(varchar(10),ta.listdate,101)) as mdate,   
flag1 = isnull(ta.Flag1,'N') , 
ta.donotmailflag, ta.recordid, 
DATEDIFF(DAY, ISNULL(ta.DOB, '01/01/1900'), GETDATE()) / 365.25 AS ageLimit
into #temp 
FROM dbo.tblTicketsViolationsArchive TVA  ,dbo.tblTicketsArchive TA    
WHERE  TVA.recordid = TA.recordid   

-- First Name last should not be null or empty. . . 
and ISNULL(TA.FirstName, '') <> ''  
and ISNULL(TA.LastName, '') <> '' 

-- Only Non Clients 
and ta.clientflag=0 
 
-- NOT MOVED IN PAST 48 MONTHS (ACCUZIP PROCESSED)... 
and isnull(ta.ncoa48flag,0) = 0 

-- FOR THE SELECTED LIST DATE RANGE ONLY... 
and datediff(Day,  ta.listdate , @startListdate)<=0 
and datediff(Day,  ta.listdate , @endlistdate )>=0


-- Only violations which contains "license", "insurance", OR any thing related to "Driver's license" in the violation Description. . . 
AND (tva.ViolationDescription LIKE '%license%' OR tva.ViolationDescription LIKE '%insurance%' 
	OR tva.ViolationDescription LIKE '%drivers license%' OR tva.ViolationDescription LIKE '%driver''s license%') 


-- Deleting the Records which were already printed . . . 
Delete from #temp
FROM #temp a INNER JOIN tblletternotes n ON n.RecordID = a.recordid 
INNER JOIN tblletter l ON l.LetterID_PK = n.LetterType 
WHERE 	lettertype = @LetterType


-- Deleting the Records which are greater on less then age limit 16 to 30. . . 
DELETE FROM #temp
WHERE ageLimit < 16
OR ageLimit > 30

-- SELECT * FROM #temp ORDER BY mdate


Select count(distinct recordid) as Total_Count, mdate   
into #temp2   
from #temp   
group by mdate   
    
-- GETTTING LIST DATE WISE TOTAL NUMBER OF PRINTABLE LETTERS...
Select count(distinct recordid) as Good_Count, mdate   
into #temp3  
from #temp   
where Flag1 in ('Y','D','S')     
and donotmailflag = 0      
group by mdate     

-- GETTING LIST DATE WISE TOTAL NUMBER OF NON-PRINTABLE LETTERS 
Select count(distinct recordid) as Bad_Count, mdate    
into #temp4    
from #temp   
where Flag1  not in ('Y','D','S')   
and donotmailflag = 0   
group by mdate   

-- GETTING LIST DATE WISE TOTAL NUMBER OF LETTERS THAT MARKED 
-- AS BLOCKED ADDRESSES.....   
Select count(distinct recordid) as DonotMail_Count, mdate    
into #temp5     
from #temp   
where donotmailflag=1   
group by mdate

                                          
-- OUTPUTTING THE REQURIED INFORMATION........
Select   #temp2.mdate as listdate,  
        Total_Count,  
        isnull(Good_Count,0) Good_Count,    
        isnull(Bad_Count,0)Bad_Count,    
        isnull(DonotMail_Count,0)DonotMail_Count    
from #Temp2 left outer join #temp3     
on #temp2.mdate=#temp3.mdate    
left outer join #Temp4    
on #temp2.mdate=#Temp4.mdate      
left outer join #Temp5    
on #temp2.mdate=#Temp5.mdate                                                         
where Good_Count > 0 order by #temp2.mdate desc  

-- DROPPING THE TEMPORARY TABLES USED ABOVE.....
drop table #temp drop table #temp2  drop table #temp3  drop table #temp4  drop table #temp5   

GO
grant exec on dbo.USP_MAILER_Get_Immigration_Data to dbr_webuser
GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_LA_Get_Summary_Support]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_LA_Get_Summary_Support]
GO



CREATE PROCEDURE USP_HTS_LA_Get_Summary_Support
@Group_ID as int

AS

Select * From Tbl_HTS_LA_Summary_Support Where GroupID = @Group_ID Order By RecordID


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


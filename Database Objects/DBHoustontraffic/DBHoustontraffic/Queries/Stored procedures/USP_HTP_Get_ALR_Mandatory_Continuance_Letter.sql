/**  
* Nasir 5864 06/25/2009  
* Business Logic: This procedure is use to get data for Mandatory Continuance Letter and Continuance Letter

* List of parameter:
* First Name, 
* Last name,
* Case number,
* Court Date
* Driving License Number
* AttorenyFirstName,
* AttorneyLastName,
* AttorneyBarCardNumber,
* AttorneySignatureImage


**/  

alter PROCEDURE dbo.USP_HTP_Get_ALR_Mandatory_Continuance_Letter --75371
	@TicketID INT
AS
	SELECT DISTINCT tt.Firstname
	      ,tt.Lastname
	      ,tt.DLNumber
	      ,ttv.CourtDateMain
	      ,CASE ttv.casenumassignedbycourt WHEN '' THEN ttv.RefCaseNumber ELSE ttv.casenumassignedbycourt end AS CaseNumber
	      ,tf.AttorenyFirstName AttorenyFirstName
	      ,tf.AttorneyLastName AS AttorneyLastName
	      ,tf.AttorneyBarCardNumber AS AttorneyBarCardNumber
	      --Yasir Kamal 7150 01/01/2010 get attorney signature image.
	      ,tf.FirmID
	      INTO #temp
	FROM   tblTickets tt
	       INNER JOIN tblTicketsViolations ttv
	            ON  ttv.TicketID_PK = tt.TicketID_PK
	       INNER JOIN tblFirm tf
	            ON  tf.FirmID = tt.CoveringFirmID
	WHERE  tt.TicketID_PK = @TicketID
	AND ttv.ViolationNumber_PK=16159

--Yasir Kamal 7150 01/01/2010 get attorney signature image.
SELECT t.*,tf.AttorneySignatureImage
  FROM tblFirm tf INNER JOIN  #temp t ON t.firmid = tf.FirmID
  

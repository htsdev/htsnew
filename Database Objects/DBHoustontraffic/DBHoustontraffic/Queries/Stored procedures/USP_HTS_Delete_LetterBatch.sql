SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Delete_LetterBatch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Delete_LetterBatch]
GO


--USP_HTS_Delete_LetterBatch 2,2,'06/03/2006'     
        
CREATE  procedure USP_HTS_Delete_LetterBatch              
                        
@LetterID int,                        
--@chk int,                        
--@batchDate datetime = '1/1/1900',                        
@ticketidlist varchar(1000) = '0,'                        
                        
as                               
/*        
                        
 if (@chk = 1)       --To delete by letter type                 
 begin                                   
  update tblhtsbatchprintletter                         
    set DeleteFlag=1            
    where LetterID_Fk = @LetterID                
end               
              
 if (@chk = 2)                    --To delete by filedate    
  Begin                                         
  update tblhtsbatchprintletter                             
     set DeleteFlag=1                          
    where                       
    LetterID_Fk = @LetterID                         
    And datediff(day, BatchDate , @batchDate) = 0                          
  End                        
      
                      
 if (@chk = 3)                    --To delete by ticketid ,lettertype, filedate    
*/  
 Begin              
            
   update tblhtsbatchprintletter                         
    set DeleteFlag=1                        
    where                       
    LetterID_Fk = @LetterID                         
--    And datediff(day, BatchDate , @batchDate ) = 0                        
    and TicketID_FK  IN (select * from dbo.Sap_String_Param( @ticketidlist ))                           
 End                        
    
  
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


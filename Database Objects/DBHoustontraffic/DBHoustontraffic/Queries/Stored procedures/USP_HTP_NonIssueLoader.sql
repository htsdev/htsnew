-- exec  USP_HTP_NonIssueLoader
alter procedure [dbo].[USP_HTP_NonIssueLoader]

as

declare @LoaderID int, 
		@LoaderName varchar(100)

select @Loaderid = LoaderId, @LoaderName = LoaderDescription
from tblloaders where Loaderid = 8

-- getting required records in temp table...
select distinct  v.ticketid_pk, v.ticketsviolationid, v.courtid,
	v.courtdate, v.courtnumber, v.courtviolationstatusid,
	v.courtdatemain, v.courtnumbermain, v.courtviolationstatusidmain
INTO #Events
from tblticketsviolations v inner join tbltickets t on t.ticketid_pk = v.ticketid_pk
inner join tblcourtviolationstatus sa on sa.courtviolationstatusid = v.courtviolationstatusidmain 
INNER join tblcourts c on c.courtid = v.courtid

-- only active clients..
where t.activeflag = 1

-- only cases having auto status Non Issue (primary status)
-- only specific case statuses...
and v.courtviolationstatusid in (
	208,215,198,216,151,177,79,18,21,172,168,124,96,67,138,153,154,23,22,178,65,
	155,102,13,20,81,170,9,161,15,36,90,6,10,12,210,211,235,212,213,214,239,195,243
	)

-- only cases having verified primary status JURY TRIAL..
and sa.categoryid = 4

-- ONLY CASES HAVING VERIFIED COURT DATE IN PAST..
and datediff(day, v.courtdatemain, getdate()) > 0

-- ONLY HMC & HCJP COURT CASES..
-- Sabir Khan 8909 02/09/10/2011 remove HCJP courts from auto disposed alert update.
and c.courtcategorynum in (1)

order by v.ticketid_pk 

 
alter table #Events add rowid int identity(1,1), violcount int

-- getting violation count for each client...
select ticketid_pk, count(ticketsviolationid) violcount
into #violcount from #Events group by ticketid_pk

update b set b.violcount= a.violcount
from #Events b, #violcount a where a.ticketid_pk = b.ticketid_pk

--select * from #events

declare @idx int, @reccount int,  @ticketid int , @ticketsviolationid int,  @empid int  ,@senddate datetime,
		@IsTrial int , @ViolCount int, @IdxViol int, @TempTicketId int, @SendTrialLetter bit, @lettercount int

DECLARE @Client table(ticketsviolationid int, ticketid_pk int);

select @ViolCount = 0 , @IdxViol = 0, @TempTicketId = 0, @SendTrialLetter = 0 ,@senddate = getdate(), @empid = 3992
select @idx = 1, @reccount = count(rowid), @ticketid = 0, @ticketsviolationid = 0, @lettercount = 0
from #Events

While @idx <= @reccount
begin
	select	@ticketid = ticketid_pk, @ticketsviolationid = ticketsviolationid , 
			@ViolCount = violcount 
	from #Events where rowid = @idx

	if @ticketid != @TempTicketId
		begin
			set @TempTicketId = 0
			set @IdxViol = 1
			set @SendTrialLetter = 0
		end

	-- UPDATE CASE SETTING INFORMATION IN CLIENTS....
	update V    
	set v.courtviolationstatusidmain = 80,
	v.courtdatemain = c.courtdate,
	v.courtnumbermain = c.courtnumber,
	v.courtid = case when v.courtid in (3001,3002,3003) then 
						 case c.courtnumber     
							when '13' then 3002    
							when '14' then 3002    
							when '18' then 3003    
							else 3001    
						 end 
				else v.courtid end,
	V.updateddate = getdate(),
	v.vemployeeid = @empid
	OUTPUT INSERTED.ticketsviolationid, inserted.ticketid_pk  into @Client 
	from tblticketsviolations v inner join #Events c on v.ticketsviolationid = c.ticketsviolationid
	and c.ticketsviolationid = @ticketsviolationid

	-- add note in case history..
	if @ViolCount =  @IdxViol 
		begin
			insert into tblticketsnotes (ticketid, subject , employeeid)
			select @ticketid, 'Non Issue Event Disposed', @empid
		end

	select	@TempTicketId = @ticketid,
			@ticketid = 0 , 
			@ticketsviolationid = 0, 
			@IsTrial = null,
			@ViolCount = null

	set @IdxViol = @IdxViol + 1
	set @idx = @idx + 1
End

-- dump data in A/W loader update history table...

insert into LoaderResetHistory 
	(
	ticketid_pk, ticketsviolationid, courtid, 
	courtdate, courtnumber,	courtviolationstatusid, 
	courtdatemain, courtnumbermain, courtviolationstatusidmain, 
	vemployeeid, LoaderUpdateDate, loaderid
	)
	
select ticketid_pk, ticketsviolationid, courtid,
	courtdate, courtnumber, 80,
	courtdatemain, courtnumbermain, courtviolationstatusidmain, 
	@empid, getdate(), @LoaderId
from #events 

-- sending email to management with the statistics...
declare @message varchar(1000), @subject varchar(100), @totalclients int, @totalviolations int, 
		@compTime varchar(50) 
select	@compTime = dbo.fn_DateFormat(getdate(),28,'/',':',1),
		@totalclients = count(distinct ticketid_pk) ,
		@totalviolations = count(distinct ticketsviolationid)
from @Client

set @subject = @LoaderName +  ' Completed - ' + convert(varchar(10), getdate(),101)

set @message  = 
@LoaderName +  ' completed at: ' +  @compTime  + '.<br>
Following are the details<br><br>
<table width="300px" border="1" cellpading="0" cellspacing="0" style="bordercolor: black">
<tr><td width="75%">Total number of clients updated</td><td align="right" width="20%">' + convert(varchar,@totalclients)       + '</td></tr>
<tr><td width="75%">Total number of violations disposed</td><td align="right" width="20%">' + convert(varchar,@totalviolations)       + '</td></tr>
</table><br><br>
This is a system generated email!<br>
'

EXEC msdb.dbo.sp_send_dbmail                           
    @profile_name = 'Data Loader',                           
    @recipients =   'dataloaders@sullolaw.com',            
	@copy_recipients  = 'ozair@lntechnologies.com',            
    @subject = @Subject,                           
    @body = @Message,                         
    @body_format = 'HTML' ;

drop table #events
drop table #violcount

go

grant execute on [dbo].[USP_HTP_NonIssueLoader] to dbr_webuser
go

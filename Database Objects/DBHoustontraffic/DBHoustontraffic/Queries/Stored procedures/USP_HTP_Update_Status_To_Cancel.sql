USE TrafficTickets
GO

/****** Object:  StoredProcedure [dbo].[USP_HTP_Update_Status_To_Cancel]    Script Date: 11/15/2011 19:14:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************************************************
 * Created By		: Muhammad Nasir	
 * Created Date		: 07/10/2009
 * Task Id			: 6013
 * 
 * Business Logic	: update status in faxletter to cancel
 * 
 * Parameter List	:
 *						@FaxID			: FaxID 
 ********************************************************************************/

CREATE PROCEDURE [dbo].[USP_HTP_Update_Status_To_Cancel] 

@FaxID INT

AS
UPDATE faxLetter
SET [status] = 'Cancelled'
WHERE FaxLetterid = @FaxID
GO

GRANT EXECUTE ON [dbo].[USP_HTP_Update_Status_To_Cancel]  TO dbr_webuser
Go
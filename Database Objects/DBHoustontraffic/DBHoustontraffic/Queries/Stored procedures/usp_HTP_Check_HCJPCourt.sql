/****** 
Alter by:  Noufil Khan
Business Logic : this procedure is used to check for any HCJP court in the given case excluding disposed and no hire violations. 

List of Parameters:	
	@Ticketid : Ticket Id of the case

List of Columns: 	
	isfound : if found then 1 else 0

******/

     
Alter Procedure [dbo].[usp_HTP_Check_HCJPCourt]      
@Ticketid int,  
@showAll int = -1  
as      
      
declare @count int      
      
      
select @count=count(*) from tblticketsviolations      
where ticketid_pk=@Ticketid      
and courtid between 3007 and 3022     
and (@showAll=1 or courtviolationstatusidmain not in (80,236))  
      
if @count>0      
 select 1 as isfound      
else      
 select 0 as isfound      

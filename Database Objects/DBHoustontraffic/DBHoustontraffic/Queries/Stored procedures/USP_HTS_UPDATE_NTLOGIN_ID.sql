SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_UPDATE_NTLOGIN_ID]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_UPDATE_NTLOGIN_ID]
GO


CREATE PROCEDURE USP_HTS_UPDATE_NTLOGIN_ID    
@EmpID int,    
@NtLoginID varchar(20)    
as    
UPDATE tblusers    
 set     
  NTUserID = @NtLoginID    
 where    
  EmployeeID = @EmpID


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


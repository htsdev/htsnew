ALTER Procedure [dbo].[usp_hts_reportmanager_client]      
      
      
@fromdate  datetime,      
@todate datetime       
      
as      
      
SELECT     (Select top 1 refcasenumber from tblticketsviolations where ticketId_pk = t1.ticketId_pk) as 'Ticket Number', t1.Firstname as 'First Name', t1.Lastname as 'Last Name',    
  Convert(varchar,(Select top 1 courtdatemain from tblticketsviolations where ticketId_pk = t1.ticketId_pk),101) as 'Main Court Date', t2.Description as 'Main Case Status'     
FROM       tblTickets t1 INNER JOIN
			tblticketsviolations TV on t1.ticketId_pk = TV.ticketId_Pk INNER JOIN
           tblDateType t2 ON TV.CourtViolationStatusIdMain = t2.TypeID INNER JOIN      
           tblTicketsPayment t3 ON t1.TicketID_PK = t3.TicketID      
      
      
WHERE     (t3.Recdate BETWEEN @fromdate AND @todate ) AND (t1.Activeflag = 1)       
 AND (t3.PaymentType NOT IN (100, 99)) AND (t3.PaymentVoid = 0)      
 AND t3.recdate in       
   (select min(t4.recdate)as date from tblticketspayment t4,tbltickets t5       
   where t4.ticketid=t5.ticketid_pk      
   group by t4.ticketid)      
GROUP BY  t1.Firstname, t1.Lastname, t2.Description, t3.RecDate,t1.ticketId_pk  
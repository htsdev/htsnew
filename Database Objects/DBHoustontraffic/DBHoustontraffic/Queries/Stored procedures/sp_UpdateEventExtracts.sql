USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateEventExtracts]    Script Date: 04/26/2012 22:50:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/****** 
Create by		: Sabir Khan
Created Date	: 06/03/2009
TasK ID			: 4413

Business Logic : This procedure is used to update all records of Non Clients and Quote clients 
				 -- By Matching Cause Number 
				 -- By Matching Ticket Number
				 -- By Matching Ticket Number & Violation Number
				 -- By Matching Ticket Number & Sequence Number
				 
				
Column Return : 
				@updatedrec: Number of updated records.
			
******/


-- sp_UpdateEventExtracts    
ALTER PROCEDURE [dbo].[sp_UpdateEventExtracts]    
as 

DECLARE @violationid int ,    
@status int,    
@courtdate datetime,    
@courtnum as varchar(3),    
@courtdatemain datetime,     
@LoaderId tinyint,    
@currDate datetime    

--SET NOCOUNT ON    
    
SElect @LoaderId = 2, @currdate = getdate()    
    
-----------------------------------------------------------------------------------------------------------------------    
-- INSERTING NEW CASE STATUSES IN TBLCOURTVIOLATIONSTATUS...........    
-----------------------------------------------------------------------------------------------------------------------    
insert into tblcourtviolationstatus(Description,violationcategory,ShortDescription,CategoryID,SortOrder,CourtStatusUpdateDate)    
select distinct status,0,left(status,3),0,0,getdate() from tblEventExtractTemp    
where status not in (    
select distinct status from tblEventExtractTemp T, tblcourtviolationstatus S    
where T.Status = S.description and violationcategory = 0)    

    
-----------------------------------------------------------------------------------------------------------------------    
-- INSERTING NISI CASES IN A SEPARATE TABLE.............    
-----------------------------------------------------------------------------------------------------------------------    
insert into tbleventdataanlysis (causenumber, ticketnumber, status, courtnumber, courtdate, courttime, updatedate,
		violationstatusid, assocmidnumber, barnumber, bondingnumber, attorneyname, bondingcompanyname
	)
select distinct t.causenumber , t.ticketnumber, t.status, t.courtnumber, t.courtdate, t.courttime, t.updatedate,
		t.violationstatusid, t.assocmidnumber, t.barnumber, t.bondingnumber, t.attorneyname, t.bondingcompanyname
from tblEventExtractTemp t
where (charindex('nisi', t.causenumber) <> 0  or charindex('nisi', status) <> 0 )
and not exists 
	(select * from tbleventdataanlysis where causenumber = t.causenumber)
	

-----------------------------------------------------------------------------------------------------------------------    
-- UPDATE VIOLATIONSTATUSID IN SOURCE TEMP TABLE.......    
-----------------------------------------------------------------------------------------------------------------------    
update T    
set violationstatusid = courtviolationstatusid    
 from tblEventExtractTemp T, tblcourtviolationstatus S    
where T.Status = S.description and violationcategory = 0    
    

-----------------------------------------------------------------------------------------------------------------------    
-- FORMATTING CAUSENUMBER, COURT DATE AND COURT NUMBER IN SOURCE TEMP TABLE    
-----------------------------------------------------------------------------------------------------------------------    
update tblEventExtractTemp set causenumber = replace(causenumber,' ',''),courtdate = courtdate + ' ' + dbo.formatTime(courttime)    

-- tahir 5921 05/19/2009 fixed the court number bug...
update tblEventExtractTemp set Courtnumber = 0 
--where courtnumber = 'ADMIN'    
where isnumeric(isnull(courtnumber,''))=0

--print 'test'    

-- FIRST UPDATE THE COMPETITORS DATABASE.....
EXEC dbo.USP_HTS_UpdateCompetitorsDB

--Sabir Khan 4413 05/03/2009
-- IF TICKET NUMBER NOT PRESENT THEN UPDATE TICKET NUMBER BY CAUSE NUMBER    
update tblEventExtractTemp    
set TicketNumber = CauseNumber    
where len(isnull(TicketNumber,'')) = 0

-----------------------------------------------------------------------------------------------------------------------    
-- ADDED FTA PROCESS    
-----------------------------------------------------------------------------------------------------------------------    
--Sabir Khan 4413 05/03/2009 Add all records into temp table...
select       
causenumber as causenumber ,TicketNumber, assocmidnumber,       
updatedate as violationdate,    
courtdate, ViolationStatusID,getdate() as listdate,Status,    
courtnumber,courttime,    
--------------------------------------------------------------------------------
--case c.courtnumber when '13' then 3002 when '14' then 3002 when '18' then 3003 else 3001 end as courtid,    -- Adil 7685 04/09/2010 commented
--case when c.courtnumber IN ('13', '14') then 3002 when c.courtnumber IN ('18', '20') then 3003 ELSE 3001 END as courtid,    -- Adil 7685 04/09/2010 When CRT is 18 or 20 then assume Dairy Ashford as court
----------------------------------------------------------------------
-- Sabir Khan 10037 02/09/2012 Court Number logic has been implemented.
-- If Case Status is Jury Trial
case when ViolationStatusID = 26 then
				case when courtnumber in ('18') then 3003 -- If Court number is 18 then set court location as HMC-D
				when courtnumber in ('20') then 3075 -- Fahad 11371 08/30/2013 Added HMC-W check
			    else 3001 -- Otherwise set court location as HMC-L
				end
-- If Case Status is not in Jury Trial				
when ViolationStatusID <> 26 then
				Case when courtnumber in ('18') then 3003 -- If Court number is 18 then set court location as HMC-D
					 when courtnumber in ('13','14') then 3002 -- If Court number is 13 or 14 then set court location as HMC-M
					 when courtnumber in ('20') then 3075 -- Abbas Qamar 10088 04/24/2012 Adding court number against HMC-W 
					 else 3001 -- Otherwise set court location as HMC-L
				end
end as courtid,
-----------------------------------------------------------------------
updatedate, barnumber, bondingnumber, attorneyname, bondingcompanyname    
    
into #temp
   
from tblEventExtractTemp C 


--Sabir Khan 4413 05/03/2009 add column into temp table...
ALTER TABLE #temp 
	ADD RowId INT PRIMARY KEY identity(1,1),
		lookupid INT NOT NULL DEFAULT(0),
		ticketviolationid INT NOT NULL DEFAULT(0),
		IncorrectMid INT NOT NULL DEFAULT(0)

--update newly added column if cause number is exists in database...
UPDATE a SET  a.lookupid = v.rowid
FROM  #temp a INNER JOIN tblticketsviolationsarchive v ON v.CauseNumber = a.causenumber
inner join tblticketsarchive t on t.recordid = v.recordid AND t.MidNumber = a.assocmidnumber
where V.courtlocation in (3001,3002,3003,3075) -- Abbas Qamar 10088 04-24-2012 Adding court HMC-w
AND ISNULL(a.lookupid,0) = 0      


-- Update IncorrectMid for all those records if cuase number is exists and mid number is different in database
UPDATE a SET  a.IncorrectMid = 1
FROM  #temp a INNER JOIN dbo.tblticketsviolationsarchive v ON v.CauseNumber = a.causenumber
inner join tblticketsarchive t on t.recordid = v.recordid AND t.MidNumber <> a.assocmidnumber
WHERE ISNULL(a.lookupid,0) = 0
AND v.courtlocation IN (3001,3002,3003,3075)

--update newly added column if ticket number is exists in database...
UPDATE a SET  a.lookupid = v.rowid
FROM  #temp a INNER JOIN tblticketsviolationsarchive v ON v.TicketNumber_PK = a.TicketNumber
inner join tblticketsarchive t on t.recordid = v.recordid AND t.MidNumber = a.assocmidnumber
where v.courtlocation in (3001,3002,3003,3075) -- Abbas Qamar 10088 04-24-2012 Adding court HMC-w
AND ISNULL(a.lookupid,0) = 0 

-- Update IncorrectMid for all those records if Ticket number is exists and mid number is different in database
UPDATE a SET  a.IncorrectMid = 1
FROM  #temp a INNER JOIN dbo.tblticketsviolationsarchive v ON v.TicketNumber_PK = a.ticketnumber
inner join tblticketsarchive t on t.recordid = v.recordid AND t.MidNumber <> a.assocmidnumber
where ISNULL(a.lookupid,0) = 0
AND a.IncorrectMid = 0
AND v.courtlocation IN (3001,3002,3003,3075)  


--update column into temp table...
UPDATE a SET  a.lookupid = v.rowid
FROM  #temp a INNER JOIN tblticketsviolationsarchive v ON a.TicketNumber = V.ticketnumber_pk + convert(varchar(2),v.violationnumber_pk)
inner join tblticketsarchive t on t.recordid = v.recordid AND t.MidNumber = a.assocmidnumber
where v.courtlocation in (3001,3002,3003,3075)    -- Abbas Qamar 10088 04-24-2012 Adding court HMC-w
AND ISNULL(a.lookupid,0) = 0   

-- Update IncorrectMid for all those records if ticket number + violation number is exists and mid number is different in database
UPDATE a SET  a.IncorrectMid = 1
FROM #temp a INNER JOIN tblticketsviolationsarchive V ON a.ticketnumber =  v.TicketNumber_PK + CONVERT(VARCHAR, v.ViolationNumber_PK)
inner join tblticketsarchive t on t.recordid = v.recordid AND t.MidNumber <> a.assocmidnumber
WHERE ISNULL(a.lookupid,0) = 0
AND a.IncorrectMid = 0
AND V.courtlocation IN (3001,3002,3003,3075)

--update newly added column if cause number is exists in Quote client...
UPDATE a SET  a.ticketviolationid = v.TicketsViolationID
FROM  #temp a INNER JOIN tblticketsviolations v ON a.causenumber = v.casenumassignedbycourt
where V.courtid in (3001,3002,3003,3075)    -- Abbas Qamar 10088 04-24-2012 Adding court HMC-w 
AND ISNULL(a.ticketviolationid,0) = 0   


--update newly added column if ticket number is exists in Quote client...
UPDATE a SET  a.ticketviolationid = v.TicketsViolationID 
FROM  #temp a INNER JOIN tblticketsviolations v ON a.TicketNumber = v.refcasenumber
where V.courtid in (3001,3002,3003,3075) -- Abbas Qamar 10088 04-24-2012 Adding court HMC-w
AND ISNULL(a.ticketviolationid,0) = 0       


--update column into temp table...
UPDATE a SET  a.ticketviolationid = v.TicketsViolationID
FROM  #temp a INNER JOIN tblticketsviolations v ON a.TicketNumber = V.refcasenumber + convert(varchar(2),V.sequencenumber)
where V.courtid in (3001,3002,3003,3075)  -- Abbas Qamar 10088 04-24-2012 Adding court HMC-w
AND ISNULL(a.ticketviolationid,0) = 0   


--insert all incorrect mid number records into a mid number discrepancy table.
INSERT INTO Mid_Discrepancy_Event(CauseNumber,TicketNumber,STATUS,CourtNumber,CourtDate,CourtTime,
UpdateDate,ViolationStatusID,assocmidnumber,BarNumber,BondingNumber,AttorneyName,BondingCompanyName)
SELECT causenumber,TicketNumber,Status,courtnumber,courtdate,courttime,updatedate,ViolationStatusID,
assocmidnumber,barnumber, bondingnumber,attorneyname, bondingcompanyname 
FROM #temp WHERE IncorrectMid = 1
--------------------------------------------------------------------
declare @idx int, @reccount int, @updatedrec int, @tempupdate INT
select	@idx = 1,@updatedrec = 0, @reccount = count(rowid) from #temp
-- insert or update records one by one....

--Sabir Khan 4413 03/11/2009 Create temp table table for getting updated records rowid and violationstatusid...
DECLARE @NonClient table(RowId int);
DECLARE @QuoteClient table(ViolationstatusId int);


While @idx <= @reccount
BEGIN
	
			-----------------------------------------------------------------------------------------------------------------------    
			-- UPDATE ATTORNEY NAMES / BONDING COMPANY NAMES & INITIAL EVENT DATES....  
			-- BY MATCHING CAUSE NUMBER    
			----------------------------------------------------------------------------------------------------------------------- 
			-- sabir khan 4413 05/03/2009 UPDATE ATTORNEY INFORMATION ....
			update V    
			set  v.barcardnumber =  c.barnumber ,    
			v.attorneyname =  isnull(c.attorneyname,c.barnumber) ,
			v.InitialEventDate_Attorney =	c.updatedate	
			from #temp C
			INNER JOIN  tblticketsviolationsarchive V on c.lookupid = v.rowid    
			where LEN(ISNULL(c.barnumber,'') + ISNULL(c.attorneyname,'') ) > 0
			and datediff(day, isnull(v.InitialEventDate_Attorney,'1/1/1900'), '1/1/1900') =0 
			and C.rowid = @idx and isnull(c.lookupid,0) > 0  --Sarim Ghani 03/13/2009   Add lookupid filter on Tahir Request
			AND C.IncorrectMid = 0
			

			-- sabir khan 4413 05/03/2009 UPDATE BONDING COMPANY INFORMATION....
			update V    
			set    v.bondingnumber =   c.bondingnumber ,   
			v.bondingcompanyname =  isnull(c.bondingcompanyname, c.bondingnumber)  ,
			v.InitialEventDate_BondingCompany =  c.updatedate
			from #temp C 
			INNER JOIN  tblticketsviolationsarchive V on c.lookupid = v.rowid 
			where 				    
			LEN(ISNULL(c.bondingnumber,'') + ISNULL(c.bondingcompanyname,'') ) > 0
			and datediff(day, isnull(v.InitialEventDate_BondingCompany,'1/1/1900'), '1/1/1900') =0 
			and C.rowid = @idx and isnull(c.lookupid,0) > 0  --Sarim Ghani 03/13/2009   Add lookupid filter on Tahir Request
			AND C.IncorrectMid = 0
										
			----------------------------------------------------------------------------------------------------------------------- 
			-- sabir khan 4413 05/03/2009   
			-- UPDATE STATUS, COURTDATE, COURT NUMBER FOR EXISTING REGULAR TICKETS IN NON CLIENTS    
			-- BY MATCHING CAUSE NUMBER    
			-----------------------------------------------------------------------------------------------------------------------    
			update V    
			set violationstatusid = C.violationstatusid,    
			V.courtdate = C.courtdate ,V.updateddate = C.updatedate,    
			V.courtnumber = C.courtnumber,    
			v.ftaissuedate = case when c.violationstatusid = 186  AND DATEDIFF(DAY, '1/1/1900', ISNULL(v.ftaissuedate,'1/1/1900') ) = 0  then c.updatedate else v.ftaissuedate  end,    
			v.courtlocation = c.courtid,
			UpdationLoaderId =  @LoaderId 
			OUTPUT INSERTED.rowid into @NonClient    
			from #temp C 
			INNER JOIN  tblticketsviolationsarchive V on c.lookupid = v.rowid 
			where (datediff(day,ISNULL(V.updateddate,'1/1/1900') , C.updatedate) >= 0)
			and C.rowid = @idx   and isnull(c.lookupid,0) > 0 --Sarim Ghani 03/13/2009   Add lookupid filter on Tahir Request
			AND C.IncorrectMid = 0	

			-----------------------------------------------------------------------------------------------------------------------
			-- sabir khan 4413 05/03/2009    
			-- UPDATE STATUS, COURTDATE, COURT NUMBER FOR EXISTING TICKETS IN QUOTE/CLIENT    
			------------------------------------------------------------------------------------------------------------------------    
			update V    
			set courtviolationstatusid = C.violationstatusid,    
			V.courtdate = C.courtdate ,V.updateddate = C.updatedate,    
			V.courtnumber = C.courtnumber,    
			-- v.courtid =c.courtid, -- Sabir Khan 9073 03/24/2011 No need to update courtid with auto information		
			v.VEmployeeID = 3992, -- Sabir Khan 9073 03/24/2011 Need to update vemployee ID also	
			--Sabir 4640 08/21/2008 set bondreminderdate to today's date + 2 business days when verified status is  "A/W" or "Arraignment" and 
			--Auto Status is "FTA Warrant" or "Warrant" otherwise nothing to do with Bondreminderdate
			---------------------------------------------------------------------------------------------------------------------------------------------------
			V.BondReminderDate=case when V.CourtViolationStatusIDmain in (3,201) -- 3 = Arraignment, 201 = Arr/Waiting
			AND C.ViolationStatusID in (186,274) -- 186 = FTA Warrant, 274 = Warrant
			then 
			dbo.fn_getnextbusinessday(getdate(),2) 
			else 
			v.bondreminderdate 
			end  
			--------------------------------------------------------------------------------------------------------------------------------------------------- 
			OUTPUT INSERTED.TicketsViolationID into @QuoteClient 
			from    
			#temp C    
			INNER JOIN  tblticketsviolations V on c.ticketviolationid = v.TicketsViolationID 
			where (datediff(day, ISNULL(V.updateddate,'1/1/1900') , C.updatedate) >= 0)  
			and C.rowid = @idx  and  ISNULL(c.ticketviolationid,0) > 0   --Sarim Ghani 03/13/2009   Add ticketviolationid filter on Tahir Request
	
	set @idx = @idx + 1
END

Drop table #temp

--Sabir Khan 4413 03/11/2009 get total count of updated records...
SELECT @updatedrec = 
		ISNULL((select COUNT(Distinct rowid) from @NonClient ),0) 
		+ 
		ISNULL((select COUNT(Distinct ViolationstatusId) from @QuoteClient),0)

  
-- SENDING UPLOADED NO OF RECORDS THROUGH EMAIL   
-- sabir khan 4413 05/03/2009
EXEC usp_sendemailafterloaderexecutes  0,@LoaderId,'houston','',@updatedrec 





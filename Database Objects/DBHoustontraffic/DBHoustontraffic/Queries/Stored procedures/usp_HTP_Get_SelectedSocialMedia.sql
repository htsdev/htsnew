USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[usp_HTP_Get_SelectedSocialMedia]    Script Date: 01/02/2014 02:22:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** 
Create by:  Sabir Khan
Task ID: 11509
Business Logic : This procedure is used to get find by description

******/
ALTER Procedure [dbo].[usp_HTP_Get_SelectedSocialMedia]
@TicketID INT
as

SELECT fd.MediaID,fb.DESCRIPTION FROM SocialMediaDetail fd 
INNER JOIN SocialMedia fb ON fb.ID = fd.MediaID 
WHERE fd.TicketID = @TicketID


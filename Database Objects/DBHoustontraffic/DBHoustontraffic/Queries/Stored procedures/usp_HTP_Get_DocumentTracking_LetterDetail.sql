/****** 
Create by:  Syed Muhammad Ozair

Business Logic : this procedure is used to display the details of a document which is scanned and processed by Document Tracking.

List of Parameters:	
	@DocumentBatchID : document id of the for which records will be fetched

List of Columns: 	
	LetterID : Document batch id
	PageCount : total no of pages of the document
	PageNo : page no of the document	

******/

Create Procedure dbo.usp_HTP_Get_DocumentTracking_LetterDetail

@DocumentBatchID int,
@ScanBatchID int

as

select	documentbatchid as LetterID, 
	documentbatchpagecount as PageCount,
	documentbatchpageno as PageNo 
from	tbl_htp_documenttracking_scanbatch_detail
where	documentbatchid=@DocumentBatchID
	and	ScanBatchID_FK=@ScanBatchID
order by	documentbatchpageno

go

grant exec on dbo.usp_HTP_Get_DocumentTracking_LetterDetail to dbr_webuser
go
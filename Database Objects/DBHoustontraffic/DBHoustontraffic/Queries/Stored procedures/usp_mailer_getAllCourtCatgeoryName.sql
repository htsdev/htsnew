﻿USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[usp_mailer_getAllCourtCatgeoryName]    Script Date: 10/12/2011 12:10:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_mailer_getAllCourtCatgeoryName]
AS
	SELECT tcc.CourtCategorynum AS courtid,
	       tcc.CourtCategoryName AS courtname
	FROM   tblCourtCategories tcc
	WHERE  tcc.IsActive = 1
	ORDER BY
	       tcc.CourtCategoryName

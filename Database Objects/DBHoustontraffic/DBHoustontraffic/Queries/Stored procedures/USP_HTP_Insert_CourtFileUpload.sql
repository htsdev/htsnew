set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


/****** 
Create by:  Muhammad Kazim
Business Logic : This procedure is used to insert the uploaded files information of the courts.
 
List of Parameters:     
      @Courtid:			represnts id of a court
      @DocumentName:	represents name of uploaded document
      @UploadedDate:	represents the date on which the file is uploaded on server
	  @Rep:				represents the employee id of the rep which has uploaded the file
	  @DocumentPath:	represents the path of uploaded document 
  
******/


create PROCEDURE [dbo].[USP_HTP_Insert_CourtFileUpload]
@CourtId int,
@DocumentName varchar(100),
@UploadedDate datetime,
@Rep varchar(100),
@DocumentPath varchar(300)

AS

insert into Courtfiles values(@courtid,@DocumentName,@UploadedDate,@Rep,@DocumentPath)
go

grant execute on dbo.[USP_HTP_Insert_CourtFileUpload] to [dbr_webuser]
go




/****** 
Alter by:  Noufil Khan
Business Logic : This Procedure select 1 reocrd from database with repsect to ticketid and date about clint information and call status and call comments 

List of Parameters:	
	@ticketid   : Search with Ticketid 
	@courtdate	: start date to search

******/   




--[dbo].[usp_HTS_Get_ReminderCall_comments_Update] 174687,'03/27/2008'

ALTER PROCEDURE [dbo].[usp_HTS_Get_ReminderCall_comments_Update] -- 102623,'04/05/2006'
	@ticketid INT , 
	 --@TicketsViolationID int,
	@courtdate DATETIME
AS
	SET NOCOUNT ON;
	SELECT TOP 1 
	       TV.ticketid_pk,
	       TV.TicketsViolationID,
	       T.Firstname,
	       T.Lastname,
	       T.LanguageSpeak,
	       T.BondFlag,
	       --ozair 4837 10/08/2008 court address commented
	       --U.Address + ' ' + U.City + ', ' + 'TX' + ' ' + U.Zip AS courtaddress,
	       tv.ReminderComments,
	       tv.SetCallComments,
	       tv.ReminderCallStatus,
	       tv.SetCallStatus,
	       dbo.fn_hts_get_TicketsViolations (@ticketid, @courtdate) AS trialdesc,	--calling func in order to get distinct violations
	       I.FirmAbbreviation,
	       T.GeneralComments,	-- Add Zahoor 3977 
	       tv.FTACallStatus,	-- Add Zahoor 3977 
	       tv.FTACallComments,
	       tv.HMCAWDLQCallStatus,	--Nasir 5690 03/25/2009 add
	                             	-- I.Phone, I.Fax,
	                             	-- TV.CourtDateMain, TV.CourtNumbermain,
	       CONVERT(
	           VARCHAR(20),
	           ISNULL(dbo.formatphonenumbers(T.Contact1), '')
	       ) + '(' + ISNULL(LEFT(Ct1.Description, 1), '') + ')' AS contact1,
	       CONVERT(
	           VARCHAR(20),
	           ISNULL(dbo.formatphonenumbers(T.Contact2), '')
	       ) + '(' + ISNULL(LEFT(Ct2.Description, 1), '') + ')' AS contact2,
	       CONVERT(
	           VARCHAR(20),
	           ISNULL(dbo.formatphonenumbers(T.Contact3), '')
	       ) + '(' + ISNULL(LEFT(Ct3.Description, 1), '') + ')' AS contact3
	FROM   dbo.tblFirm I WITH(NOLOCK)
	       RIGHT OUTER JOIN dbo.tblContactstype CT1 WITH(NOLOCK)
	       RIGHT OUTER JOIN dbo.tblTicketsViolations TV WITH(NOLOCK)
	       INNER JOIN dbo.tblTickets T WITH(NOLOCK)
	            ON  TV.TicketID_PK = T.TicketID_PK
	       INNER JOIN dbo.tblCourts U WITH(NOLOCK)
	            ON  TV.CourtID = U.Courtid
	       INNER JOIN dbo.tblCourtViolationStatus CVS WITH(NOLOCK)
	            ON  TV.CourtViolationStatusID = CVS.CourtViolationStatusID
	                --    INNER JOIN dbo.tblDateType F ON CVS.CategoryID = F.TypeID
	                
	       LEFT OUTER JOIN dbo.tblContactstype ct3 WITH(NOLOCK)
	            ON  T.ContactType3 = ct3.ContactType_PK
	       LEFT OUTER JOIN dbo.tblContactstype ct2 WITH(NOLOCK)
	            ON  T.ContactType2 = ct2.ContactType_PK
	            ON  CT1.ContactType_PK = T.ContactType1
	            ON  I.FirmID = T.FirmID
	WHERE  -- (TV.TicketsViolationID = @TicketsViolationID) 
	       (TV.ticketid_pk = @ticketid)
	       AND courtviolationstatusidmain NOT IN (80, 236) 
	           --ozair 4837 10/08/2008 commented not needed
	           --and datediff(day, courtdatemain, @courtdate) = 0
	           
	               
	           








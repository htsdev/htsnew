SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_GET_BatchTicketID]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_GET_BatchTicketID]
GO


create PROCEDURE [dbo].[USP_HTS_GET_BatchTicketID]               
@LType  int ,                                      
@emp_id varchar(10),          
@isprinted int,  
@fromdate datetime,  
@todate datetime          
          
AS                                      
          
declare @tbl1 table        
(        
ticketid_pk int,        
[Name] varchar(100),
Tooltip varchar(100),    
CourtShortName varchar(20),  
ShortDescription varchar(20),  
batchdate datetime,  
b_Emp varchar(10),        
courtdate datetime,  
CourtNumber int,        
printdate datetime,  
p_Emp varchar(10),        
isprinted int,        
Countp  int        
)        
          
set @todate = @todate + '23:59:59.999'  
          
          
insert into @tbl1           
select  t.ticketid_pk,
( 
case 
	when len(t.lastname +', '+ t.firstName)>15
	then substring (t.lastname +', '+ t.firstName,0,15) + '...'
    else
     t.lastname +', '+ t.firstName
end
) as [Name],  
t.lastname +', '+ t.firstName as Tooltip,
   (SELECT  top 1 C1.ShortName  
      FROM         dbo.tblCourts AS C1 INNER JOIN  
      dbo.tblTicketsViolations AS TV2 ON C1.Courtid = TV2.CourtID  
      where (tv2.ticketid_pk = t.ticketid_pk  )) CourtShortName,  
   (  select top 1 t4.ShortDescription       
     from tblticketsviolations tv2,tblcourtviolationstatus t4            
     where  tv2.courtviolationstatusidmain = t4.courtviolationstatusid                       
      and (tv2.ticketid_pk = t.ticketid_pk)          
      and  (t4.categoryid in (1,2,3,4,5))) ShortDescription,              
(            
 case             
  when convert(varchar(10), b.batchDate, 101)  = '01/01/1900' then null            
  --else convert(varchar(20), b.batchDate , 109)          
  else  b.batchDate   
 end   )  as batchdate ,    
 b_User.Abbreviation as BatchEmp,           
  courtdate =isnull(  
    (select min(t2.CourtDateMain)           
       from tblticketsviolations t2,tblcourtviolationstatus t4            
      where  t2.courtviolationstatusidmain = t4.courtviolationstatusid        
        and (datediff(day, t2.courtdatemain, getdate()) <= 0 )              
        and (t2.ticketid_pk = t.ticketid_pk)          
        and  (t4.categoryid in (1,2,3,4,5))),  
          (select max(tt2.CourtDateMain)         
            from tblticketsviolations tt2,tblcourtviolationstatus tt4        
            where tt2.courtviolationstatusidmain = tt4.courtviolationstatusid      
             and (datediff(day, tt2.courtdatemain, getdate()) >= 0)             
             and (tt2.ticketid_pk = t.ticketid_pk)        
             and  (tt4.categoryid in (1,2,3,4,5)) )),    
 courtNumber=isnull(  
    (select min(t2.courtnumbermain)           
      from tblticketsviolations t2,tblcourtviolationstatus t4            
       where  t2.courtviolationstatusidmain = t4.courtviolationstatusid        
        and (datediff(day, t2.courtdatemain, getdate()) <= 0 )              
        and (t2.ticketid_pk = t.ticketid_pk)          
        and  (t4.categoryid in (1,2,3,4,5)) ),  
         (select max(tt2.courtnumbermain)         
           from tblticketsviolations tt2,tblcourtviolationstatus tt4        
            where tt2.courtviolationstatusidmain = tt4.courtviolationstatusid      
              and (datediff(day, tt2.courtdatemain, getdate()) >= 0)             
              and (tt2.ticketid_pk = t.ticketid_pk)        
              and  (tt4.categoryid in (1,2,3,4,5)) )),          
 case          
  when b.printdate='01/01/1900' then null else  b.printdate end as printdate,           
  p_User.Abbreviation  as PrintEmp,           
  b.isprinted,          
    (select count(bb.batchid_pk)   
      from tblhtsbatchprintletter bb   
       where letterid_fk=@ltype and isprinted=0 and deleteflag<>1              
        and bb.batchid_pk=  
         (select max(tbv.batchID_pk) from tblhtsbatchprintletter tbv           
           where  tbv.ticketid_fk=bb.ticketid_fk   
             and tbv.LetterID_Fk = @LType    
             and deleteflag<>1)              
    ) as countp                             
FROM         dbo.tblHTSBatchPrintLetter b INNER JOIN          
                      dbo.tblTickets t ON t.TicketID_PK = b.TicketID_FK AND b.BatchID_PK =          
                          (SELECT     MAX(tbv.batchID_pk)          
                            FROM          tblhtsbatchprintletter tbv          
                            WHERE      tbv.ticketid_fk = b.ticketid_fk AND tbv.LetterID_Fk = @LType AND deleteflag <> 1)   
     LEFT OUTER JOIN          
                      dbo.tblUsers b_User ON b.EmpID = b_User.EmployeeID LEFT OUTER JOIN          
          
                      dbo.tblUsers p_User ON b.PrintEmpID = p_User.EmployeeID          
  
  
WHERE     (b.LetterID_FK = @LType)          
ORDER BY b.BatchDate DESC          
            
--select * from @tbl1  where b_emp like @emp_id and isprinted = @isprinted     
  
if(@isprinted=1)   
 begin  
  
 select ticketid_pk,   
  [name],    
  CourtShortName,  
  ShortDescription,  
  Tooltip,
  dbo.formatdateandtimeintoshortdateandtime(batchdate) as batchdate  ,   
  b_emp,    
  case when courtdate is null then '' else dbo.formatdateandtimeintoshortdateandtime(courtdate) end as courtdate,   
  courtnumber,   
  case when printdate is null then '' else dbo.formatdateandtimeintoshortdateandtime(printdate) end as printdate ,   
  p_emp,   
  isprinted,   
  countp,  
  convert(Datetime, courtdate) as trialdate, courtnumber as trilroom,   
  convert(DateTime,batchdate) as batchdate1,   
  b_emp as batchemp,   
  convert(DateTime,printdate) as printdate1,    
  p_emp as printemp   
  
 from @tbl1  where b_emp like @emp_id and isprinted = 1    
 and printdate between @fromdate and @todate  
 order by [batchdate1] desc                      
  
 end  
  
else  
 begin  
  select ticketid_pk,   
  [name],    
  CourtShortName,  
  ShortDescription,  
Tooltip,
  dbo.formatdateandtimeintoshortdateandtime(batchdate) as batchdate  ,   
  b_emp,    
  case when courtdate is null then '' else dbo.formatdateandtimeintoshortdateandtime(courtdate) end as courtdate,   
  courtnumber,   
  case when printdate is null then '' else dbo.formatdateandtimeintoshortdateandtime(printdate) end as printdate ,   
  p_emp,   
  isprinted,   
  countp,  
  convert(Datetime, courtdate) as trialdate, courtnumber as trilroom,   
  convert(DateTime,batchdate) as batchdate1,   
  b_emp as batchemp,   
  convert(DateTime,printdate) as printdate1,    
  p_emp as printemp   
  
 from @tbl1  where b_emp like @emp_id and isprinted = 0      
 order by [batchdate1] desc       
                 
 end  

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


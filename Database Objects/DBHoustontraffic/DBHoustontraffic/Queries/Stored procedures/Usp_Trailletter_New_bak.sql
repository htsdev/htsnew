SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Usp_Trailletter_New_bak]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Usp_Trailletter_New_bak]
GO



--select * from tblticketsviolations where refcasenumber = 'TR81C0087086'


--Usp_Trailletter_New 103104,3000
--select * from tbldatetype


create     Procedure Usp_Trailletter_New_bak

@TicketIDList	 int,
@empid	int

as

SELECT     dbo.tblTickets.TicketID_PK, dbo.tblTickets.MiddleName, dbo.tblTicketsViolations.RefCaseNumber AS TicketNumber_PK, dbo.tblTickets.Firstname, 
                      dbo.tblTickets.Lastname, dbo.tblTickets.Address1, dbo.tblTickets.Address2, dbo.tblTickets.City AS Ccity, dbo.tblState.State AS cState, 
                      dbo.tblTickets.Zip AS Czip, dbo.tblTicketsViolations.CourtDate, dbo.tblTicketsViolations.CourtNumber, dbo.tblCourts.Address, dbo.tblCourts.City, 
                      dbo.tblCourts.Zip, dbo.tblTickets.Datetypeflag, dbo.tblTickets.Activeflag, dbo.tblTickets.LanguageSpeak, dbo.tblTicketsViolations.SequenceNumber, 
                      tblState_1.State, dbo.tblCourtViolationStatus.CategoryID, dbo.tblTicketsViolations.TicketViolationDate, tblTickets.languagespeak
FROM         dbo.tblTickets INNER JOIN
                      dbo.tblTicketsViolations ON dbo.tblTickets.TicketID_PK = dbo.tblTicketsViolations.TicketID_PK INNER JOIN
                      dbo.tblCourts ON dbo.tblTickets.CurrentCourtloc = dbo.tblCourts.Courtid INNER JOIN
                      dbo.tblState ON dbo.tblTickets.Stateid_FK = dbo.tblState.StateID INNER JOIN
                      dbo.tblState tblState_1 ON dbo.tblCourts.State = tblState_1.StateID INNER JOIN
                      dbo.tblCourtViolationStatus ON dbo.tblTicketsViolations.CourtViolationStatusID = dbo.tblCourtViolationStatus.CourtViolationStatusID
WHERE     (dbo.tblTickets.Datetypeflag IN (3, 4, 5)) AND (dbo.tblTickets.Activeflag = 1)-- AND (DATEDIFF([day], dbo.tblTicketsViolations.CourtDate, GETDATE()) <= 0) 
                      AND (dbo.tblCourtViolationStatus.CategoryID <> 80) AND (NOT (dbo.tblTicketsViolations.RefCaseNumber LIKE 'ID%')) AND 
                      (dbo.tblTickets.TicketID_PK = @TicketIDList)

if @@rowcount >=1 
	exec sp_Add_Notes @TicketIDList,'Trial Letter Printed',null,@empid





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


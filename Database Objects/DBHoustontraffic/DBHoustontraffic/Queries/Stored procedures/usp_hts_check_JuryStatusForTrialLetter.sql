SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_hts_check_JuryStatusForTrialLetter]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_hts_check_JuryStatusForTrialLetter]
GO


CREATE procedure usp_hts_check_JuryStatusForTrialLetter   
    
@TicketID int    
    
as    
    
Select top 1 isnull(t.activeflag,0) as ActiveFlag,    
  isnull(c.categoryid,0) as categoryid    
     
    
from     
 tbltickets t  inner join tblticketsviolations tv on     
 t.ticketid_pk=tv.ticketid_pk inner join     
 tblcourtviolationstatus c on tv.courtviolationstatusidmain=c.courtviolationstatusid    
     
 where    
      
  t.ticketid_pk=@TicketID    
    
     
    

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_webscan_get_msmqlog_NewII]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_webscan_get_msmqlog_NewII]
GO

    
CREATE procedure [dbo].[usp_webscan_get_msmqlog_NewII]  --1145,'12/19/2007'     
      
@Batchid int,    
@sentdate  datetime    
      
as      
      
select top 5 message,    
msgdate,    
isnull(reset,'') as reset,    
isnull(causeno,'') as causeno,    
isnull(scanstatus,'') as status,    
(select count(id)    
from tbl_webscan_msmqlog    
where reset is not null    
and batchid=@Batchid    
and isnull(scanstatus,'')<>'Sending'    
and isnull(message,'')<>'No Cause'    
and isnull(message,'')<>'Quote Client'    
and msgdate>@sentdate)as processed,  
(select count(id)    
from tbl_webscan_msmqlog    
where reset is not null    
and batchid=@Batchid    
and isnull(message,'')='No Cause'    
and msgdate>@sentdate)as NoCause,  
(select count(id)    
from tbl_webscan_msmqlog    
where reset is not null    
and batchid=@Batchid    
and isnull(message,'')='Quote Client'    
and msgdate>@sentdate)as QuoteClient      
from tbl_webscan_msmqlog      
where batchid=@Batchid      
and msgdate>@sentdate    
order by id desc    



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_HTS_Get_AllViolationQuote]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_HTS_Get_AllViolationQuote]
GO










CREATE PROCEDURE usp_HTS_Get_AllViolationQuote  
    
@TicketID numeric    
    
AS    
    
SELECT     dbo.tblViolationQuote.QuoteID,   
dbo.tblViolationQuote.QuoteResultID,   
Convert(varchar,dbo.tblViolationQuote.QuoteResultDate,101) as QuoteResultDate,   
dbo.tblViolationQuote.FollowUPDate,     
dbo.tblViolationQuote.FollowUpYN,   
dbo.tblViolationQuote.QuoteComments,   
Convert(varchar,dbo.tblViolationQuote.CallBackDate,101) as  CallBackDate,  
Convert(varchar,dbo.tblViolationQuote.AppointmentDate,101) as    AppointmentDate,  
dbo.tblViolationQuote.AppointmentTime,   
dbo.tblViolationQuote.TicketID_FK,   
CONVERT(varchar(12), dbo.tblViolationQuote.LastContactDate, 101)  AS LastContactDate,   
dbo.tblQuoteResult.QuoteResultDescription As FollowUpStatus ,   
dbo.tblViolationQuote.FollowUPID    
FROM         dbo.tblViolationQuote LEFT OUTER JOIN    
                      dbo.tblQuoteResult ON dbo.tblViolationQuote.FollowUPID = dbo.tblQuoteResult.QuoteResultID    
    
WHERE     (TicketID_FK = @TicketID)    
Order By QuoteID Desc







GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


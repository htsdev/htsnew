SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_ESignature_Get_ImageForSubReport]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_ESignature_Get_ImageForSubReport]
GO




-- =============================================
-- Author:		Muhammad Adil Aleem
-- Create date: 14-Nov-06
-- Description:	To Get Images from table
-- =============================================
--[USP_HTS_ESignature_Get_ImageForSubReport] '124546', 'ImageStream_Bond_ClientSign1',4
CREATE PROCEDURE [dbo].[USP_HTS_ESignature_Get_ImageForSubReport] 
	@TicketID varchar(25),
	@ImageTrace varchar(100)
AS
Declare @RecordCount int
Set @RecordCount = 0
BEGIN
Set @RecordCount = (SELECT count(TicketID) from TBL_HTS_ESignature_Images Where TicketID = @TicketID And ImageTrace = @ImageTrace)
If @RecordCount <> 0
	Begin
		SELECT * from TBL_HTS_ESignature_Images Where TicketID = @TicketID And ImageTrace = @ImageTrace Order By PageNumber
	End
Else
	Begin
		Select @TicketID as TicketID, null as ImageStream, @ImageTrace as ImageTrace, 1 as PageNumber
	End
END





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


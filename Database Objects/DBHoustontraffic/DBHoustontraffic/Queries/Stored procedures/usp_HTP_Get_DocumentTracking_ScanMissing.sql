/****** 
Create by:  Syed Muhammad Ozair

Business Logic : this procedure is used to get the batch detail by document/report batch id.

List of Parameters:	
	@DocumentBatchID : document id of the for which records will be fetched

List of Columns: 	
	BatchID : Batch id  
	LetterID : Document batch id
	PageCount : total no of pages of the document	

******/

Create Procedure dbo.usp_HTP_Get_DocumentTracking_ScanMissing

@DocumentBatchID int,
@ScanBatchID int

as

select top 1 scanbatchid_fk as BatchID, 
	documentbatchid as LetterID, 
	documentbatchpagecount as PageCount 
from	tbl_htp_documenttracking_scanbatch_detail
where	documentbatchid=@DocumentBatchID
	and ScanBatchID_fk=@ScanBatchID

go

grant exec on dbo.usp_HTP_Get_DocumentTracking_ScanMissing to dbr_webuser
go


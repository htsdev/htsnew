set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go




/*********

Created By		: Zeeshan Ahmed

Business Logic	: This Procedure is used to get list of child custody case status.

List Of Columns :

		ChildCustodyCaseStatusID 
		CaseStatusName

*********/

Create PROCEDURE [dbo].[USP_CC_GET_CaseStatus]
AS


SELECT * FROM [dbo].[ChildCustodyCaseStatus]
Order by ChildCustodyCaseStatusID 


Go

Grant Execute on [dbo].[USP_CC_GET_CaseStatus] to dbr_webuser

Go
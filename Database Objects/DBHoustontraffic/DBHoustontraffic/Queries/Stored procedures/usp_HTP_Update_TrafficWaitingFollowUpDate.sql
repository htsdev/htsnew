﻿/****** 
Create by:  Abid Ali

Business Logic : this procedure is used to update or insert traffic waiting follow up date in tblTickets table.

List of Parameters:	
	@TicketID : id against which records are updated
	@EmployeeID : employee id of current logged in user
	
******/
-- Abid Ali 5425 1/20/2009 undo changes of 5098 due to wrong data updation which was not part of build of 5098

ALTER PROCEDURE [dbo].[usp_HTP_Update_TrafficWaitingFollowUpDate]
(
    @TicketID              INT,
    @EmployeeID            INT,
    @FollowUpDate          DATETIME = NULL,
    @IsUpdateFollowUpDate  BIT = FALSE
)
AS	
	-- declare variable to check client is active or not
	DECLARE @ActiveFlag AS BIT
	
	SELECT @ActiveFlag = ActiveFlag
	FROM   tblTickets
	WHERE  TicketID_PK = @TicketID
	
	IF @ActiveFlag = 1
	BEGIN
	    -- If @IsUpdateFollowUpDate = 0 then update by default follow up date
	    IF @IsUpdateFollowUpDate = 0
	    BEGIN
	        -- Declaring variable to count number records who meet the criteria
	        -- of Court can not be HMC and HCJP, case type must be traffic
	        -- and finally has waiting status
	        DECLARE @TrafficWaitingRecordsCount AS INT
	        
	        -- Abid Ali 5018 12/24/2008 Update Family and traffic waiting
	        DECLARE @CaseType INT
	        
	        SELECT @CaseType = CaseTypeId
	        FROM   tbltickets
	        WHERE  ticketid_pk = @TicketID
	        
	        -- Getting record count
	        IF @CaseType = 4
	        BEGIN
	            -- Getting record count
	            SELECT @TrafficWaitingRecordsCount = COUNT(*)
	            FROM   tblTicketsViolations
	                   INNER JOIN tblCourts
	                        ON  tblTicketsViolations.CourtID = tblCourts.CourtID
	            WHERE  tblTicketsViolations.TicketID_PK = @TicketID -- passed parameter
	                   AND tblTicketsViolations.courtviolationstatusidmain = 104 -- waiting status
	                   AND tblCourts.CaseTypeId = 4 -- Family law
	        END
	        ELSE
	        BEGIN
	            SELECT @TrafficWaitingRecordsCount = COUNT(*)
	            FROM   tblTicketsViolations
	                   INNER JOIN tblCourts
	                        ON  tblTicketsViolations.CourtID = tblCourts.CourtID
	            WHERE  tblTicketsViolations.TicketID_PK = @TicketID -- passed parameter
	                   AND tblTicketsViolations.courtviolationstatusidmain = 104 -- waiting status
	                   AND -- Sabir Khan 5183 To Exclude HMC and HCJP courts...
	                       tblCourts.CourtCategoryNum NOT IN (1, 2) -- HMC and HCJP
	                   AND tblCourts.CaseTypeId = 1 -- Traffic case
	        END 
	        
	        -- checking if there is atleast one record found
	        -- if found then update the follow up date
	        -- Traffic Waiting Follow Up date must be equal to Today + 7 days
	        IF @TrafficWaitingRecordsCount > 0
	        BEGIN
	            DECLARE @OldTrafficFollowUpDate DATETIME
	            
	            SELECT @OldTrafficFollowUpDate = trafficWaitingFollowUpDate
	            FROM   tblTickets
	            WHERE  TicketID_PK = @TicketID
	            
	            DECLARE @TrafficFollowUpDate AS DATETIME
	            --Waqas 6342 08/20/2009 Follow up date set to coming friday for family
	            if(@CaseType = 4)
	            begin
		            SET @TrafficFollowUpDate = dbo.fn_get_next_coming_day(getdate(),6)
	            end
	            else
	            begin
					SET @TrafficFollowUpDate = dbo.fn_getnextbusinessday(GETDATE(), 5)
	            end
	            
	            UPDATE tblTickets
	            SET    trafficWaitingFollowUpDate = @TrafficFollowUpDate,
	                   employeeidupdate = @EmployeeID
	            WHERE  TicketID_PK = @TicketID
	            
	            -- Abid Ali 5425 1/20/2009 setting waiting follow up date comment
	            -- insert into hisotry
	            DECLARE @Subject AS VARCHAR(4000)
	            IF @OldTrafficFollowUpDate IS NULL
	            BEGIN
	                SET @Subject = 'Waiting Follow Up Date has changed from N/A to ' + CONVERT(VARCHAR, @TrafficFollowUpDate, 101)
	                
	                EXECUTE [dbo].[sp_Add_Notes] @TicketID, @Subject , '', @EmployeeID
	            END
	            ELSE 
	            IF CONVERT(VARCHAR, @OldTrafficFollowUpDate, 101) <> CONVERT(VARCHAR, @TrafficFollowUpDate, 101)
	            BEGIN
	                SET @Subject = 'Waiting Follow Up Date has changed from ' + CONVERT(VARCHAR, @OldTrafficFollowUpDate, 101) + ' to ' + CONVERT(VARCHAR, @TrafficFollowUpDate, 101)
	                
	                EXECUTE [dbo].[sp_Add_Notes] @TicketID, @Subject , '', @EmployeeID
	            END
	        END
	    END
	    ELSE
	    BEGIN
	        DECLARE @OldFollowUpDate DATETIME
	        
	        SELECT @OldFollowUpDate = trafficWaitingFollowUpDate
	        FROM   tblTickets
	        WHERE  TicketID_PK = @TicketID
	        
	        UPDATE tblTickets
	        SET    trafficWaitingFollowUpDate = @FollowUpDate,
	               employeeidupdate = @EmployeeID
	        WHERE  TicketID_PK = @TicketID
	        
	        -- Abid Ali 5425 1/20/2009 setting waiting follow up date comment
	        -- insert into hisotry
	        DECLARE @Message AS VARCHAR(4000)
	        IF @OldFollowUpDate IS NULL
	        BEGIN
	            SET @Message = 'Waiting Follow Up Date has changed from  N/A to ' + CONVERT(VARCHAR, @FollowUpDate, 101)
	            
	            EXECUTE [dbo].[sp_Add_Notes] @TicketID, @Message , '', @EmployeeID
	        END
	        ELSE IF CONVERT(VARCHAR, @OldFollowUpDate, 101) <> CONVERT(VARCHAR, @FollowUpDate, 101)
	        BEGIN   
	        	
	        	SET @Message = 'Waiting Follow Up Date has changed from ' + CONVERT(VARCHAR, @OldFollowUpDate, 101)  + ' to ' + CONVERT(VARCHAR, @FollowUpDate, 101)
	            
	            EXECUTE [dbo].[sp_Add_Notes] @TicketID, @Message , '', @EmployeeID
	        END
	    END
	END
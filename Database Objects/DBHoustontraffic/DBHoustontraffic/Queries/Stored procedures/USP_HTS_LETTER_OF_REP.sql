USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTS_LETTER_OF_REP]    Script Date: 07/16/2013 09:46:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
/******    
ALTER by:  Sabir Khan  
  
Business Logic : this procedure is used to get the details which will be displayed on letter of rep.  
  
List of Parameters:   
 @ticketid : ticket id of the case  
 @empid : employee id of currently logged in user  
  
******/     

--EXEC [dbo].[USP_HTS_LETTER_OF_REP] 17239401,4177 

-- USP_HTS_LETTER_OF_REP 166314,3991 ,10596, 3991   7961
--USP_HTS_LETTER_OF_REP 197050,3991
--Sabir Khan 5424 Implement proper case logic on retrived fields...
ALTER PROCEDURE [dbo].[USP_HTS_LETTER_OF_REP] -- 124164,3991
	@ticketid INT ,
	@empid INT
AS
	DECLARE @strVacationDates VARCHAR(2000)
	SET @strVacationDates = dbo.fn_getvacation()
	-----------------------------------------------------------
	--Sabir Khan 8227 01/21/2011 Associating PMC Rep Date for Faxes....
	-----------------------------------------------------------
	--Sabir Khan 8899 7/14/2011 Replace the repeated code in function for Getting spacefic date for PMC court location...
	DECLARE @selectedCourtDate DATETIME	
	SET @selectedCourtDate = [dbo].[fn_GetRepDateForPMC](@TicketID)
	-----------------------------------------------------------
	-----------------------------------------------------------
	
	SELECT DISTINCT
	       --Sabir Khan 5424 02/19/2009 set text in proper case...
	       dbo.Fn_htp_get_InitCap(firstname) AS firstname,
	       dbo.Fn_htp_get_InitCap(lastname) AS lastname,
	       dbo.formatdate(dob) AS dob,
	       dlnumber,
	       @strVacationDates AS vacdate,
	       ticketsviolationid,
	       dbo.Fn_htp_get_InitCap(a.description) AS viol_desc,
	       CASE 
	            WHEN LEN(ISNULL(casenumassignedbycourt, '')) = 0 THEN 
	                 refcasenumber
	            ELSE casenumassignedbycourt
	       END AS casenumassignedbycourt,
	       refcasenumber,
	       languagespeak,	--added on 03/29/2006    zee                                
	       dbo.formatdateandtime_with_at_sign(V.courtdatemain) AS currentdateset,
	       dbo.Fn_htp_get_InitCap(firstname + ' ' + lastname) AS fullname,
	       --Yasir kamal 5621 03/05/2009 Lor Format Change
	       --Sabir Khan 5634 09/03/2009 'e' has been removed from Atten...                               
	       'Attn: ' + C.courtcontact AS attn,
	       C.judgename AS judgename,
	       -- 5621 end                               
	       'Tel: ' + dbo.formatphonenumbers(CONVERT(VARCHAR(20), C.phone)) AS 
	       phone,
	       C.courtname AS courtname,
	       --Sabir Khan 5424 02/19/2009 Insert comma if address2 is exist...
	       CASE 
	            WHEN LEN(ISNULL(C.address2, '')) = 0 THEN C.address + ' ' + C.address2
	            ELSE C.address + ', ' + C.address2
	       END AS ADDRESS,
	       C.city + ', ' + E.state + ' ' + C.zip AS cityzip,
	       'Fax: ' + dbo.formatphonenumbers(CONVERT(VARCHAR(20), C.fax)) AS 
	       FAXNO,
	       --Zeeshan Haider 11306 08/12/2013 PMC Jury-Trial Cases(Do not modify case setting for sending LOR)
	       CASE 
	            WHEN v.CourtID = 3004 AND v.CourtViolationStatusIDmain = 26 THEN 
	                 'Jury-Trial'
	            ELSE S.description
	       END AS DESCRIPTION,
	       firmname AS firmname,
	       F.address AS firmaddress,
	       F.address2 AS firmaddress2,
	       F.city AS firmcity,
	       'TX' AS firmstate,
	       F.zip AS firmzip,
	       'Tel: ' + dbo.formatphonenumbers(CONVERT(VARCHAR(20), F.phone)) AS 
	       firmphone,
	       'Fax: ' + dbo.formatphonenumbers(CONVERT(VARCHAR(20), F.fax)) AS 
	       firmfax,
	       condition1flag,
	       dbo.Fn_htp_get_InitCap(Attorneyname) AS Attorneyname,
	       dbo.Fn_htp_get_InitCap(firmsubtitle) AS firmsubtitle,
	       --Zeeshan Haider 11306 08/12/2013 PMC Jury-Trial Cases(Do not modify case setting for sending LOR)
	       CASE 
	            WHEN v.CourtID = 3004 AND v.CourtViolationStatusIDmain = 26 THEN 
	                 'Jury-Trial'
	            ELSE (
	                     SELECT TOP 1 dbo.tblCourtSettingRequest.Description
	                     FROM   dbo.tblCourts
	                            INNER JOIN dbo.tblCourtSettingRequest
	                                 ON  dbo.tblCourts.SettingRequestType = dbo.tblCourtSettingRequest.SettingType
	                            INNER JOIN dbo.tblTicketsViolations
	                                 ON  dbo.tblCourts.Courtid = dbo.tblTicketsViolations.CourtID
	                     WHERE  (dbo.tblTicketsViolations.TicketID_PK = t.TicketID_PK)
	                 )
	       END AS courtDesc,
	       (
	           SELECT MAX(dbo.tblTicketsViolations.CourtDateMain) AS Expr1
	           FROM   dbo.tblTicketsViolations
	                  INNER JOIN dbo.tblCourtViolationStatus
	                       ON  dbo.tblTicketsViolations.CourtViolationStatusIDmain = 
	                           dbo.tblCourtViolationStatus.CourtViolationStatusID
	           WHERE  (dbo.tblCourtViolationStatus.CategoryID IN (3, 4, 5))
	           HAVING (
	                      MAX(dbo.tblTicketsViolations.CourtDateMain) > GETDATE() + 21
	                  )
	       ) AS NewCourtDate,	--Convert(varchar(12),C.RepDate) as RepDate, --Noufil 4945 Date comment due to extract time also.
	       CASE 
	            WHEN V.Courtid = 3004 THEN CASE 
												--Zeeshan Haider 11306 08/12/2013 PMC Jury-Trial Cases(Do not modify case setting for sending LOR)
	                                            WHEN v.UnderlyingBondFlag = 0 AND v.CourtViolationStatusIDmain <> 26 THEN 
	                                                 dbo.fn_DateFormat(@selectedCourtDate, 31, '/', ':', 1)
	                                            --Zeeshan Haider 11306 08/12/2013 PMC Jury-Trial Cases(Do not modify case setting for sending LOR)
	                                            WHEN v.CourtViolationStatusIDmain = 26 THEN
	                                            	dbo.fn_DateFormat(v.CourtDateMain, 31, '/', ':', 1)
	                                            ELSE dbo.fn_DateFormat(C.RepDate, 31, '/', ':', 1)
	                                       END
	            ELSE dbo.fn_DateFormat(C.RepDate, 31, '/', ':', 1)
	       END AS RepDate,
	       V.courtid,
	       --Zeeshan Haider 11306 08/12/2013 PMC-Jury Trial Cases(Do not modify case setting for sending LOR)
	       CASE 
	            WHEN v.CourtID = 3004 AND v.CourtViolationStatusIDmain = 26 THEN 
	                 'Jury-Trial'
	            ELSE (
	                     SELECT s.description
	                     FROM   tblCourtSettingRequest s,
	                            tblcourts co
	                     WHERE  co.settingrequesttype = s.settingtype
	                            AND co.courtid = V.courtid
	                 )
	       END AS SettingRequest,
	       T.email,
	       C.IsLORSubpoena,
	       C.IsLORMOD,
	       C.PrecinctID,
	       C.courtcategorynum,
	       mc.CountyName,	--Sabir Khan 5763 04/17/2009 LOR Subpoena and LOR Motion for Discovery...
	                     	-- Noufil 6551 09/09/2009 Court Violation Status category added
	       u.categoryid 
	       --Faique Ali 11023 07/16/2013 adding new fields for FAX rpt..
	       ,
	       t.gender
	       
	       INTO #temp
	       --Ozair 5763 05/06/2009 Joins included
	FROM   tbltickets T
	       INNER JOIN tblticketsviolations v
	            ON  t.TicketID_PK = v.TicketID_PK
	       INNER JOIN tblcourts C
	            ON  V.courtid = C.courtid
	       INNER JOIN tblviolations a
	            ON  v.violationnumber_pk = a.violationnumber_pk
	       INNER JOIN tblcourtsettingrequest S
	            ON  C.settingrequesttype = S.settingtype
	       INNER JOIN tblstate E
	            ON  c.state = E.stateid
	       INNER JOIN tblfirm F
	            ON  T.firmid = F.firmid
	       INNER JOIN tblcourtviolationstatus U
	            ON  V.courtviolationstatusidmain = U.courtviolationstatusid
	       LEFT OUTER JOIN tblCourtCategories tc --Sabir Khan 5763 05/06/2009 Inner join has been changed into Left outer join...
	            ON  C.courtcategorynum = tc.CourtCategorynum
	       LEFT OUTER JOIN tbl_Mailer_County mc
	            ON  tc.CountyID_FK = mc.CountyID
	WHERE  activeflag = 1
	       AND settingrequesttype NOT IN (0)
	       AND V.courtid NOT IN (3001, 3002, 3003)
	       AND V.courtviolationstatusid <> 80
	       AND T.ticketid_pk = @ticketid
	ORDER BY
	       [casenumassignedbycourt] DESC 
	
	--Yasir Kamal 5948 06/09/2009 RowNo column added.
	ALTER TABLE #temp ADD RowNo INT NOT NULL IDENTITY(1, 1)                                     
	
	DECLARE @Causenumbers VARCHAR(200)            
	SET @Causenumbers = ''            
	
	SELECT @Causenumbers = @Causenumbers + casenumassignedbycourt + ' / '
	FROM   #temp
	WHERE  UPPER(viol_desc) LIKE '%SPEED%' --Sabir Khan 6359 08/10/2009 Cause number has been get only for speeding violations... 
	
	--Nasir 6181 07/22/2009 if @Causenumbers is blank 
	IF (@Causenumbers <> '')
	BEGIN
	    SELECT *,
	           LEFT(@Causenumbers, LEN(@Causenumbers) -1) AS causenumbers
	    FROM   #temp
	END
	ELSE
	    SELECT *,
	           @Causenumbers causenumbers
	    FROM   #temp 
	
	DROP TABLE #temp  

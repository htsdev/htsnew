﻿   
  
  
  
            
 ALTER Procedure [dbo].[usp_HTS_ArraignmentSnapshotLongFooterOutsideArraignment_ver_1_Update]              
AS                
SET NOCOUNT ON                
 create TABLE [#tblTickets] (                
 [TicketID_PK] [int] NOT NULL ,                
 [TicketNumber_PK] [varchar] (20) NOT NULL ,                
 [Violations] [varchar] (50) NULL, 
 [Lastname] [varchar] (20) NULL ,                
 [Firstname] [varchar] (20)  NULL ,                
 [DLNumber] [varchar] (30) NULL ,                
 [DOB] [datetime] NULL ,                
 [MID] [varchar] (20) NULL ,                
 [BondFlag] [int] NULL ,                  
 [CurrentCourtNum] varchar(3) NULL,                
 [CurrentDateSet] [datetime])            
                
INSERT INTO #tblTickets (TicketID_PK, TicketNumber_PK, Lastname, Firstname, DLNumber, DOB, MID,  BondFlag, CurrentCourtNum, CurrentDateSet,Violations)                
SELECT DISTINCT             
                      TOP 100 PERCENT TT.TicketID_PK, TV.RefCaseNumber, TT.Lastname, TT.Firstname, TT.DLNumber, TT.DOB, TT.Midnum, TT.BondFlag,             
                      TV.CourtNumbermain, TV.CourtDateMain , TV.refcasenumber --, TV.SequenceNumber // Agha Usman 2664 06/13/2008             
FROM         dbo.tblTickets TT INNER JOIN            
                      dbo.tblTicketsViolations TV ON TT.TicketID_PK = TV.TicketID_PK INNER JOIN            
                      dbo.tblViolations TRV ON TV.ViolationNumber_PK = TRV.ViolationNumber_PK INNER JOIN            
                      dbo.tblCourtViolationStatus CV ON TV.CourtViolationStatusIDmain = CV.CourtViolationStatusID LEFT OUTER JOIN            
                      dbo.tblOfficer O ON TT.OfficerNumber = O.OfficerNumber_PK            
WHERE     (CV.CourtViolationStatusID IN            
                          (SELECT     courtviolationstatusid            
                            FROM          tblcourtviolationstatus            
                            WHERE      --description LIKE '%arraignment%'          
--         categoryid in (2,5,12,51)          
         categoryid in (2)          
       ))             
 AND (DATEDIFF([day], TV.CourtDateMain, GETDATE()) <= 0)             
 AND (TRV.Violationtype IN (0, 2,8))            
 AND            (TT.Activeflag = 1)             
 AND (TV.CourtID NOT IN (3001, 3002, 3003))             
 AND (CV.CategoryID <> 50)            
ORDER BY TV.CourtDateMain            
            
            
DECLARE @TicketID int                
DECLARE @TicketNumber varchar(12)                
DECLARE @Violations varchar(50)                
DECLARE CurWeekDays CURSOR READ_ONLY FOR                 
Select TicketID_PK, TicketNumber_PK From #tblTickets Order by TicketID_PK ASC , TicketNumber_PK ASC                
OPEN CurWeekDays                
FETCH NEXT FROM CurWeekDays INTO @TicketID , @TicketNumber                 
WHILE (@@fetch_status <> -1)                
BEGIN                
 SELECT @Violations = coalesce(@Violations + ', ',' ') + Convert(varchar,TV.refcasenumber) -- Agha Usman 2664 06/13/2008                 
 FROM dbo.tblTicketsViolations TV INNER JOIN dbo.tblViolations TRV ON                
 TV.ViolationNumber_PK = TRV.ViolationNumber_PK                 
 WHERE TRV.ViolationType IN (0,2,8)                
 AND TV.ViolationStatusID = 1                
 AND TV.TicketID_PK =  @TicketID                 
 AND TV.RefCaseNumber = @TicketNumber                
  UPDATE #tblTickets Set #tblTickets.Violations= @Violations                 
  WHERE  #tblTickets.TicketID_PK = @TicketID AND                
  #tblTickets.TicketNumber_PK = @TicketNumber                
                  
  SELECT @TicketNumber = NULL                
  SELECT @Violations = NULL                 
  SELECT @TicketID = NULL                
                  
  FETCH NEXT FROM CurWeekDays INTO @TicketID , @TicketNumber                 
END                
CLOSE CurWeekDays                
DEALLOCATE CurWeekDays                
                
 SELECT t.TicketID_PK, t.TicketNumber_PK , t.Lastname, t.Firstname , Violations =  CASE LEFT(UPPER(t.TicketNumber_PK),1) WHEN 'F' THEN NULL  ELSE Violations   END   
,   
  t.DLNumber ,t.DOB ,t.MID ,  BondFlag =                 
 CASE BondFlag                 
 WHEN 1 THEN 'b'                
 ELSE NULL                
 END , t.CurrentCourtNum, t.CurrentDateSet                
      
      
--Added by M.Azwar Alam on 29 August 2007 --          
 --If Do Not Reset Flag added from GeneralInfo then dont show that record on Outside Trial Report...          
          
 FROM #tblTickets t  where t.TicketID_PK not in ( select TicketID_PK  from tblticketsflag  where flagid =25  )                   
--from #tblTickets t       
 ORDER BY                  
 CurrentDateSet ASC  ,                
 TicketID_PK ASC ,                
 TicketNumber_PK ASC                
                 
 -------------------------------Proc End-----------------------------------------------------            
  
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_hts_get_TrialDocket_Updates]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_hts_get_TrialDocket_Updates]
GO



CREATE procedure [dbo].[usp_hts_get_TrialDocket_Updates] --126130                      
                      
@Ticketid int,      
@Fid int                      
                      
as                       
select   
TF.ticketid_pk as ticketid_pk,  
TF.Flagid as Flagid,  
TF.RecDate as RecDate,  
isnull(TF.IsPaid,0) as IsPaid,  
isnull(TF.Fid,0) as Fid,  
TF.ContinuanceDate as ContinuanceDate,  
isnull(TF.ContinuanceStatus,0) as ContinuanceStatus,  
isnull(TF.Priority,-1)as Priority,  
isnull(TF.empid,0) as EmpID,  
isnull(TF.ServiceTicketCategory,-1)as ServiceTicketCategory,  
isnull(TF.AssignedTo,0) as AssignedTo,  
isnull(TF.ShowTrailDocket,0) as ShowTrailDocket,  
TF.ClosedDate as ClosedDate,  
isnull(TF.DeleteFlag,0)as DeleteFlag,
isnull ( TF.ShowServiceInstruction ,0) as ShowServiceInstruction,
T.GeneralComments ,
TF.ServiceTicketInstruction as ServiceTicketInstruction,
T.firstname, 
T.lastname,
isnull(TF.ShowGeneralComments,0) as ShowGeneralComments
from tblticketsflag TF
join tbltickets T 
on T.TicketId_pk = TF.ticketid_pk 

 where TF.ticketid_pk = @Ticketid and Fid = @Fid and Flagid = 23 


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


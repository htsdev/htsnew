SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_NOMAILFLAG_BY_LETTERID]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_NOMAILFLAG_BY_LETTERID]
GO


CREATE PROCEDURE [dbo].[USP_HTS_NOMAILFLAG_BY_LETTERID]      
@LetterID int      
as      
INSERT INTO tbl_hts_donotmail_records  (FirstName, LastName, Address, Zip, City, StateID_FK)       
select FirstName,LastName,Address1,case when len(ZipCode)>5 then substring(ZipCode,1,5) else ZipCode end,City,StateID_FK from tblticketsarchive where recordid = @LetterID    
    
update tblticketsarchive       
 set DoNotMailFlag =1      
WHERE  recordid = @LetterID 

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO



/**
* Business Logic : This method use to delete schedule payment
* 
**/

ALTER PROCEDURE [dbo].[USP_HTS_New_Inactive_SchedulePayment]
	@ScheduleID INT
AS
	UPDATE tblschedulepayment
	SET    scheduleflag = 0
	WHERE  scheduleid = @ScheduleID 
	
	-------------if no payment is paid and schedule count is zero the set active flag=0      
	DECLARE @chargeamount MONEY,
	        @ticketid INT
	
	SELECT @ticketid = ticketid_pk
	FROM   tblschedulepayment
	WHERE  scheduleid = @ScheduleID 
	--Cheking if charge amount in null              
	
	SELECT @chargeamount = ISNULL(SUM(ISNULL(chargeamount, 0)), 0)
	FROM   tblticketspayment
	WHERE  ticketid = @ticketid
	       AND paymenttype NOT IN (99, 100)
	       AND paymentvoid = 0
	-- Noufil 5618 04/09/2009 Update payment follow update and add note in history.
	DECLARE @oldfollowupdate DATETIME
	SET @oldfollowupdate = (
	        SELECT ISNULL(tt.PaymentDueFollowUpDate, '01/01/1900')
	        FROM   tblTickets tt
	        WHERE  tt.TicketID_PK = @ticketid
	    )
	
	DECLARE @oldfollowvarchar VARCHAR(20) 
	SET @oldfollowvarchar = CASE @oldfollowupdate
	                             WHEN '01/01/1900' THEN 'N/A'
	                             ELSE CONVERT(VARCHAR(20), @oldfollowupdate, 101)
	                        END
	
	DECLARE @NewPaymentFollowupdate DATETIME
	SET @NewPaymentFollowupdate = (
	        SELECT MIN(TSP.PaymentDate)
	        FROM   tblSchedulePayment tsp
	        WHERE  TSP.TicketID_PK = @ticketid
	               AND DATEDIFF(DAY, TSP.PaymentDate, GETDATE()) < 0
	               AND TSP.ScheduleFlag = 1
	    )
	
	IF (@NewPaymentFollowupdate <> @oldfollowupdate)
	BEGIN
	    UPDATE tblTickets
	    SET    PaymentDueFollowUpDate = @NewPaymentFollowupdate
	    WHERE  TicketID_PK = @ticketid
	    
	    INSERT INTO tblticketsnotes
	      (
	        ticketid,
	        SUBJECT,
	        employeeid,
	        Notes
	      )
	    SELECT @TicketID,
	           'Payment Follow-Up Date has been changed from ' + @oldfollowvarchar
	           + ' to ' + CONVERT(VARCHAR(12), @NewPaymentFollowupdate, 101),
	           (
	               SELECT TOP 1 ttv.VEmployeeID
	               FROM   tblTicketsViolations ttv
	               WHERE  ttv.TicketID_PK = @ticketid
	           ),
	           NULL
	    FROM   tblusers U
	    WHERE  employeeid = (
	               SELECT TOP 1 ttv.VEmployeeID
	               FROM   tblTicketsViolations ttv
	               WHERE  ttv.TicketID_PK = @ticketid
	           )
	END--checking if schedule payment already exist          
	   /*      
	   select scheduleid from tblschedulepayment      
	   where ticketid_pk= @ticketid and scheduleflag<>0      
	   
	   if ( @chargeamount > 0  and @@rowcount =0  )      
	   begin              
	   update tbltickets set activeflag = 0 where ticketid_pk = @ticketid              
	   end                
	   */ 
	   
	   /*if not exists (      
	   select scheduleid from tblschedulepayment      
	   where  ticketid_pk = @ticketid and scheduleflag <> 0      
	   and  @chargeamount <=0      
	   
	   )      
	   update tbltickets set activeflag = 0 where ticketid_pk = @ticketid            */ 
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   ----------------------------------------------------------------------------------------------      
	   

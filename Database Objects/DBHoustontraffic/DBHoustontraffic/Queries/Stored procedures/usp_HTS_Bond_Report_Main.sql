﻿/******   
Alter by:  Syed Muhammad Ozair  
  
Business Logic : this procedure is used to Insert the information in document tracking batch tickets table and
				 also insert the entry in case history for the related cases. it also gets the required information 
				 that will be displayed under bond summary report.			  
  
List of Parameters:   
 @TVIDS : ticketsviilationids
 @DocumentBatchID : docuemnt id 
 @updateflag : 
 @employee : employee id of current logged in user  
  
List of Columns:    
 ticketid :
 causeno :
 ticketno : 
 lastname : 
 firstname :
 dlnumber : 
 dob : 
 courtdate : 
 midnum: 
 Time : 
 status :  
 bonddate : 
 ticketsviolationid :
 docid : document id
 bondflag :
   
******/

ALTER procedure [dbo].[usp_HTS_Bond_Report_Main]             
                                
@TVIDS as varchar(max),                        
@DocumentBatchID as int,            
@updateflag as int ,    
@employee as int                      
                                
AS                                
                                
                    
DECLARE @TBLTEMPTVID TABLE (TICKETVIOLATIONID INT)                                
declare @temp table            
(ticketid int,causeno varchar(20),ticketno varchar(20), lastname varchar(20), firstname varchar(20), dlnumber varchar(30), dob varchar(12),            
 midnum varchar(20), [Time] int, status varchar(20), bonddate varchar(12), bondflag varchar(2), ticketsviolationid int,            
docid int)            
                        
if  @TVIDS=''                          
begin      
	insert into @temp                       
	select  distinct                                                  
	t.ticketid_pk as ticketid,                               
	tv.casenumassignedbycourt as causeno,                                
	tV.REFCASENUMBER AS TICKETNO,                         
	upper(t.lastname) as lastname,                                
	upper(t.firstname) as firstname,                                
	t.dlnumber,                                
	convert(varchar(12),t.dob,101) as dob,                                
	t.midnum,                           
    isnull(datediff(day,tvl.recdate,getdate()),0) as Time,                            
    ( case                             
	when   tcv.shortdescription='BOND' then 'BND'                            
	else tcv.shortdescription                            
    end )as status,                            
    Convert(varchar(12),tvl.recdate,101) as BondDate ,                
    (case            
	when t.bondflag=1 then 'B'            
	else ''            
	end) as BondFlag  ,            
    tv.ticketsviolationid,            
	@DocumentBatchID as DOCID                                
	from tbltickets t inner join                                 
    tblticketsviolations tv on                                 
    t.ticketid_pk=tv.ticketid_pk inner join                                 
    tblcourtviolationstatus tcv on                                 
    tv.courtviolationstatusidmain=tcv.courtviolationstatusid left outer join                   
    tblticketsviolationlog tvl on                   
    tv.ticketsviolationid=tvl.ticketviolationid and                  
    tvl.recdate=(              
    select max(recdate) from tblticketsviolationlog where ticketviolationid= tvl.ticketviolationid                 
    and newverifiedstatus <> oldverifiedstatus              
    )                         
    where                                                       
    tV.courtviolationstatusidmain = 202  
	order by lastname  ,firstname                            
    
	if @updateflag =1          
	begin          
		declare @temp2 table             
		(              
		counter int IDENTITY(1,1) NOT NULL,              
		ticketsviolationid int    
		)     
 
		insert into @temp2 select ticketsviolationid from #Temp    
		declare @loopcounter as int            
		set @loopcounter=(SELECT count(*) FROM @temp2)    
		while @loopcounter >=1     
		begin    
			update tblticketsviolations set courtviolationstatusidmain=202 where ticketsviolationid = (SELECT ticketsviolationid FROM @temp2 where counter=@loopcounter)          
			set @loopcounter=@loopcounter-1    
		end         
	end              
                 
	select * from @temp            
         
 end                  
 else                  
 begin                          
	insert into @TBLTEMPTVID select * from dbo.sap_string_param(@TVIDS)                           
                          
    insert into @temp                        
	select                                
    t.ticketid_pk as ticketid,                               
    tv.casenumassignedbycourt as causeno,                                
    tV.REFCASENUMBER AS TICKETNO,                                
    upper (t.lastname) as lastname,                                
    upper(t.firstname) as firstname,                
    t.dlnumber,                                
    convert(varchar(12),t.dob,101) as dob,                                
    t.midnum,      
	0 as time,                              
    ( case                             
    when   tcv.shortdescription='BOND' then 'BND'                            
    else tcv.shortdescription                            
    end )as status,                            
    Convert(varchar(12),tv.bonddate,101) as BondDate ,                  
	(case            
	when t.bondflag=1 then 'B'            
	else ''            
	end) as BondFlag,              
	tv.ticketsviolationid    ,                      
    @DocumentBatchID as DOCID                                
	from tbltickets t inner join                                 
    tblticketsviolations tv on                                 
    t.ticketid_pk=tv.ticketid_pk inner join                                 
    tblcourtviolationstatus tcv on                                 
    tv.courtviolationstatusidmain=tcv.courtviolationstatusid inner join                        
	tblcourts tc on                         
	tv.courtid=tc.courtid                        
	where                                                         
    TV.TICKETSVIOLATIONID IN (SELECT TICKETVIOLATIONID FROM @TBLTEMPTVID)                           
	and t.activeflag=1  and tv.courtid in (3001,3002,3003, 3075) -- Rab Nawaz Khan 10327 06/14/2012 Added the HMC-W Court in Arraignment Setting Report. . . 
	and tv.courtdatemain is not null                                
	and tV.courtviolationstatusidmain = 202                                            
	order by t.lastname  ,t.firstname                           
                          
    
	if @updateflag =1          
	begin        
		 declare @temp1          table    
		(              
		counter int IDENTITY(1,1) NOT NULL,              
		TICKETVIOLATIONID int    
		)     
 
		insert into @temp1 SELECT TICKETVIOLATIONID FROM @TBLTEMPTVID    
		declare @loopcounter1 as int            
		set @loopcounter1=(SELECT count(*) FROM @temp1)    
		while @loopcounter1 >=1     
		begin    
			update tblticketsviolations set courtviolationstatusidmain=202 where ticketsviolationid = (SELECT TICKETVIOLATIONID FROM @temp1 where counter=@loopcounter1)          
			set @loopcounter1=@loopcounter1-1    
		end         
	end          
     
	select * from @temp            

end                   
                       
IF @DocumentBatchID <> 0            
begin      
	declare @docid as varchar(20)        
	set @docid=convert(varchar(20),@DocumentBatchID,101)               
   
	Insert into tbl_htp_documenttracking_batchTickets (BatchID_FK, TicketID_FK)
	select distinct DOCID, ticketid from @temp   
    
	insert into tblTicketsNotes (TicketID,Subject,EmployeeID)
	-- Noufil 8039 07/22/2010 Rename report tile to Arraignment Setting from Arraingment waiting.        
	select TicketID_FK, '<a href="javascript:window.open(''../quickentrynew/PreviewPrintHistory.aspx?filename='+'BOND_WAITING-'+@docid+''');void('''');"''>Bond Settings Summary DOC ID : ' + @docid+'</a>', @employee        
	from tbl_htp_documenttracking_batchTickets 
	where BatchID_FK=@DocumentBatchID       
        
end              
                     
    
    
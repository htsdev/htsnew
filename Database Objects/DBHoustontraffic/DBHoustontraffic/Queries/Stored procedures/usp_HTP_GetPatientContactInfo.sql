﻿/******  
* Created By :	  Syed Muhammad Ozair.
* Create Date :   11/05/2010 
* Task ID :		  8501
* Business Logic :  This procedure is used to Get get patient contact information.
* List of Parameter :
* Column Return :     
******/

ALTER PROCEDURE dbo.usp_HTP_GetPatientContactInfo
	@PatientId INT
AS
	SELECT p.LastName,
	       p.FirstName,
	       p.[Address],
	       p.City,
	       p.StateId,
	       p.Zip,
	       p.Contact1,
	       p.ContactType1,
	       p.Contact2,
	       p.ContactType2,
	       p.Email,
	       p.Email2,
	       CONVERT(VARCHAR,p.DOB,101) AS DOB,
	       p.Occupation,
	       p.Height,
	       p.[Weight],
	       ISNULL(p.SpanishSpeaker,2) AS SpanishSpeaker, -- Saeed 8585 12/04/2010 spanish speaker field added in select statement.
	       ISNULL(p.SsnNo,'') AS SsnNo -- Saeed 8585 12/04/2010 SsnNo added in select statement.
	FROM   Patient p
	WHERE  p.Id = @PatientId
	
GO

GRANT EXECUTE ON dbo.usp_HTP_GetPatientContactInfo TO dbr_webuser
GO
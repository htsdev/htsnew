/****** 
Altered by:		Syed Muhammad Ozair
Business Logic:	The Procedure is used to get the Error Logs by Date Range for a specific DB Type				

List of Parameters:
			@dtFrom	:	Date From 
			@dtTo	:	Date To
			@type	:	Data Base Type
	
List of Columns:
			EventID
			[Message]
			Source
			TargetSite
			StackTrace
			[Timestamp]
			
*******/
ALTER PROC [dbo].[usp_hts_get_errorlog_Esig_Loader_range]     
@dtFrom DATETIME,      
@dtTo DATETIME,  
@type INT       
      
AS      
SET @dtFrom = @dtfrom + '12:00:00 AM'    
SET @dtTo = @dtto + '11:59:59:998 PM'    
  
IF (@type = 0)--all error logs
BEGIN
    SELECT EventID,
           [Message],
           Source,
           TargetSite,
           StackTrace,
           [Timestamp]
    FROM   Tbl_HTS_ErrorLog
    WHERE  (
               [Timestamp] BETWEEN CONVERT(DATETIME, @dtFrom) AND CONVERT(DATETIME, @dtTo)
           )
    ORDER BY
           EventID DESC
END  
-------  
IF (@type = 1) --for esignature errolog
BEGIN
    SELECT EventID,
           [Message],
           Source,
           TargetSite,
           StackTrace,
           [Timestamp]
    FROM   Tbl_HTS_ErrorLog
    WHERE  (
               [Timestamp] BETWEEN CONVERT(DATETIME, @dtFrom) AND CONVERT(DATETIME, @dtTo)
               AND Source LIKE 'Esignature%'
           )
    ORDER BY
           EventID DESC
END  
--------  
IF (@type = 2) --for loader errolog
BEGIN
    SELECT EventID,
           [Message],
           Source,
           TargetSite,
           StackTrace,
           [Timestamp]
    FROM   Tbl_HTS_ErrorLog
    WHERE  (
               [Timestamp] BETWEEN CONVERT(DATETIME, @dtFrom) AND CONVERT(DATETIME, @dtTo)
               AND Source LIKE 'Loader%'
           )
    ORDER BY
           EventID DESC
END  
--------
IF (@type = 3) --for loader errolog
BEGIN
    SELECT EventID,
           [Message],
           Source,
           TargetSite,
           StackTrace,
           [Timestamp]
    FROM   Tbl_HTS_ErrorLog
    WHERE  (
               [Timestamp] BETWEEN CONVERT(DATETIME, @dtFrom) AND CONVERT(DATETIME, @dtTo)
               AND Source LIKE 'outlook%'
           )
    ORDER BY
           EventID DESC
END
-------
--Ozair 6766 04/29/2010 Added PolM option
IF (@type = 4) --for POLM errolog
BEGIN
    SELECT EventID,
           [Message],
           Source,
           TargetSite,
           StackTrace,
           [Timestamp]
    FROM   POLM.dbo.Tbl_HTS_ErrorLog
    WHERE  (
               [Timestamp] BETWEEN CONVERT(DATETIME, @dtFrom) AND CONVERT(DATETIME, @dtTo)               
           )
    ORDER BY
           EventID DESC
END
--Ozair 8739 01/31/2011 Added productdefects option
IF (@type = 5) --for productdefects errolog
BEGIN
    SELECT EventID,
           [Message],
           Source,
           TargetSite,
           StackTrace,
           [Timestamp]
    FROM   productdefectsstaging.dbo.Tbl_HTS_ErrorLog
    WHERE  (
               [Timestamp] BETWEEN CONVERT(DATETIME, @dtFrom) AND CONVERT(DATETIME, @dtTo)               
           )
    ORDER BY
           EventID DESC
END
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_RPT_LMS_Financials_MailerID_Analysis]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_RPT_LMS_Financials_MailerID_Analysis]
GO

CREATE procedure dbo.USP_RPT_LMS_Financials_MailerID_Analysis

as

-- HOUSTON......
create table #result (hire_year int, hire_month int, categoryname varchar(100), categorynum int,
					hire_count int, mailerID_count int )

select distinct 
	ticketid,
	min(recdate) as HireDate ,
	isnull(sum(isnull(chargeamount,0)),0) as Revenue
into #temp 
from tblticketspayment
where paymentvoid  =0 
group by ticketid having isnull(sum(isnull(chargeamount,0)),0)>0

select t.ticketid_pk,
		v.courtid,
		isnull(mailerid,0) as MailerId,
		p.hiredate
into #temp2
from tbltickets t , tblticketsviolations v, #temp p
where t.ticketid_pk = v.ticketid_pk
and t.ticketid_pk = p.ticketid
and datediff(month, p.hiredate, dateadd(month,-6, getdate())   )<=0

select distinct ticketid_pk, courtid, mailerid, hiredate into #temp3 from #temp2

insert into #result (hire_year, hire_month, categoryname, categorynum, hire_count , mailerID_count)
select year(hiredate)as  hire_year, month(hiredate)as hire_month, cc.courtcategoryname, c.courtcategorynum,
	count(distinct ticketid_pk) as hire_count,
	sum(case when mailerid = 0 then 0 else 1 end) as mailerID_count
from #temp3 t, tblcourts c, tblcourtcategories cc
where t.courtid = c.courtid
and c.courtcategorynum = cc.courtcategorynum
group by year(hiredate), month(hiredate), cc.courtcategoryname, c.courtcategorynum
order by c.courtcategorynum, year(hiredate) desc, month(hiredate) desc


-- DALLAS.....

select distinct 
	ticketid,
	min(recdate) as HireDate ,
	isnull(sum(isnull(chargeamount,0)),0) as Revenue
into #temp4
from dallastraffictickets.dbo.tblticketspayment
where paymentvoid  =0 
--and datediff(day, recdate, '1/1/07')<=0
group by ticketid having isnull(sum(isnull(chargeamount,0)),0)>0


select t.ticketid_pk,
		v.courtid,
		isnull(mailerid,0) as MailerId,
		p.hiredate
into #temp5
from dallastraffictickets.dbo.tbltickets t , dallastraffictickets.dbo.tblticketsviolations v, #temp4 p
where t.ticketid_pk = v.ticketid_pk
and t.ticketid_pk = p.ticketid
and datediff(month, p.hiredate, dateadd(month,-6, getdate())   )<=0

select distinct ticketid_pk, courtid, mailerid, hiredate into #temp6 from #temp5

insert into #result (hire_year, hire_month, categoryname, categorynum, hire_count , mailerID_count)
select year(hiredate)as  hire_year, month(hiredate)as hire_month, cc.courtcategoryname, c.courtcategorynum,
	count(distinct ticketid_pk) as hire_count,
	sum(case when mailerid = 0 then 0 else 1 end) as mailerID_count
from #temp6 t, dallastraffictickets.dbo.tblcourts c, traffictickets.dbo.tblcourtcategories cc
where t.courtid = c.courtid
and c.courtcategorynum = cc.courtcategorynum
group by year(hiredate), month(hiredate), cc.courtcategoryname, c.courtcategorynum
order by c.courtcategorynum, year(hiredate) desc, month(hiredate) desc

select	convert(Datetime, convert(Varchar, hire_month)+'/1/'+convert(varchar, hire_year) , 101) as hire_Date,
		categoryname,
		categorynum,
		hire_count,
		mailerid_count,
		case hire_count when 0 then 0 else round(  (  cast (mailerid_count as float) /  cast(hire_count as float)*100),2) end  as Percentage,
		case when categorynum in (6,11) then 1 else 0 end as [dbid] 
into #final
from #result

select  left( datename( month, hire_date ), 3 )+ '-' + right( datepart( year, hire_date ), 2 ) as [Hire Month],
	categoryname as [Court Category],
	hire_count as [Hire Count],
	mailerid_count as [Have Mailer IDs],
	convert(Varchar, percentage)+'%' as Percentage
from #final 
order by [dbid], categorynum, hire_date desc


drop table #temp
drop table #temp2
drop table #temp3
drop table #temp4
drop table #temp5
drop table #temp6
drop table #result
drop table #final

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


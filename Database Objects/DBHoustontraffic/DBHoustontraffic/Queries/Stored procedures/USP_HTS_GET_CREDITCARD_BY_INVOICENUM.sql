SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_GET_CREDITCARD_BY_INVOICENUM]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_GET_CREDITCARD_BY_INVOICENUM]
GO


 CREATE procedure [dbo].[USP_HTS_GET_CREDITCARD_BY_INVOICENUM]                  
@invnumber int                
--@ticketid int    --Getting credit card info by providing invoicenumber                
as                    
select                 
invnumber,                  
amount,                  
ccnum,                  
expdate,                  
cin,                  
name,                  
C.employeeid,                  
address1 + ' ' + isnull(address2,'') as Address,                  
city,                  
S.state,                  
zip,                  
firstname,                  
lastname  ,                  
ticketid  ,               
transnum, --if to void transaction of credit card            
isnull(t.dlnumber,'') as dlnumber,            
isnull(t.contact1+'('+p1.description+')','') as phoneno,            
isnull(t.contact2+'('+p2.description+')','') as phoneno1,            
min(isnull(v.refcasenumber,'')) as ticketnumber,            
min(isnull(v.casenumassignedbycourt,'')) as caseno,
cardtype            
from tblticketscybercash C,tbltickets T,tblstate S,tblticketsviolations V,            
tblcontactstype p1,tblcontactstype p2                    
where T.ticketid_pk = C.ticketid  and T.stateid_fk=S.stateid                  
and t.ticketid_pk=v.ticketid_pk            
and isnull(t.contacttype1,0)=p1.contacttype_pk            
and isnull(t.contacttype2,0)=p2.contacttype_pk            
and rowid = @invnumber            
group by invnumber,amount,ccnum,expdate,cin,name,C.employeeid,address1,address2,city,            
S.state,zip,firstname,lastname,ticketid,transnum,t.dlnumber,t.contact1,t.contact2,            
p1.description,p2.description ,cardtype


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


USE [TrafficTickets]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--[USP_HTP_get_flag_by_ticketnumber_and_flagID] 162381,33,3001
CREATE PROCEDURE [dbo].[USP_HTP_Get_FlagCount_By_TicketNumber_And_FlagID] 
	@TicketID  int,                                                  
	@FlagID    int                                                	
as

select Count(*) from tblticketsflag 
where ticketid_pk=@TicketID and FlagID=@FlagID

GO
grant execute on USP_HTP_Get_FlagCount_By_TicketNumber_And_FlagID to dbr_webuser

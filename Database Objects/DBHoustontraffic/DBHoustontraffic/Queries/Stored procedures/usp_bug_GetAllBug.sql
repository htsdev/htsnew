SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_bug_GetAllBug]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_bug_GetAllBug]
GO



--usp_bug_GetAllBug 'ss'           
CREATE procedure [dbo].[usp_bug_GetAllBug]            
    
as            
            
select b.bug_id,            
  b.shortdescription,            
  convert(varchar(12),b.posteddate,101) as posteddate,           
  u.username,            
  p.priority_name,            
  s.status_name,      
  b.Ticketid as TicketNumber   ,      
  isnull(bu.UserName,'NotAssigned') as DeveloperName  
            
  from tbl_bug b             
            
  inner join tblusers u on            
  b.EmployeeID=u.EmployeeID             
  inner join tbl_bug_priority p on             
  b.priorityid=p.priority_id             
  inner join tbl_bug_status s on             
  b.statusid=s.status_id  
 left outer  join tbl_bug_users bu on  
 b.developerid=bu.userid  
    
  order by b.bug_id desc   
  






GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[usp_hts_get_trial_docket_report_CR]    Script Date: 08/26/2013 09:12:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--exec usp_hts_get_trial_docket_report_CR @courtloc=N'None',@courtdate='2013-08-24 15:15:12:000',@page=N'1',@courtnumber=N'',@datetype=N'0',@singles=N'0',@URL=N'qa.houston.com',@ShowOwesDetail=N'1'






/******       
Business Logic : This procedure is used for Houston trial docket report.
      
Parameters :  
------------       
@courtloc : This is Court Location   
@courtdate : This is Start date of selection criteria  
@page : For what page SP will run
@courtnumber : This is Court room number
@datetype :  violation category
@singles : Either to generate single dockets or not
@URL : Will show on cases' hyperlinks
@ShowOwesDetail : either show or don't show owes details
@ShowOtherResultSets : Either show other results or not
@showjudgereport : To show results for judge report
@isdatafound : Will return 1 if records found else returns 0.
******/  
-- [usp_hts_get_trial_docket_report_CR] 'none', '12/13/2012', '1','','0',1, 'newpakhouston.legalhouston.com', 0
ALTER  procedure [dbo].[usp_hts_get_trial_docket_report_CR]   
@courtloc varchar(10) = '3037',   
@courtdate varchar(12) = '08/11/2009',   
@page varchar(12) = 'TRIAL',   
@courtnumber varchar(6) = '-1',   
@datetype varchar(20) = '0',   
@singles int = 0 ,  
@URL varchar(200) = 'ln.legalhouston.com',  
@ShowOwesDetail bit = 1, 
@ShowOtherResultSets BIT = 1, -- Adil 5772 04/15/2009 : Parameter added
@showjudgereport INT = 0, -- Noufil 5895 05/11/2009 : Parameter added
@IsNotForRuiz BIT = 0,  -- Abbas Shahid Khwaja 9334 05/31/2011 : Parameter added 
@isdatafound int =0 output      -- 07/08/08 Sabir 4349 
  
as 
SET NOCOUNT ON;

create table #TempExists -- 07/08/08 Sabir 4349
(TableName varchar(20),
IsExists int) 
 
Declare @NameDiscrepancy int   
set @NameDiscrepancy =19   
if @courtnumber = ''   
 begin   
 set @courtnumber ='-1'   
 end   
   
set nocount on   
 --Muhammad Muneer 8240 09/08/2010 Added the new column Statutenumber  
SELECT dbo.tblTickets.TicketID_PK, dbo.tblTickets.Firstname, isnull(dbo.tblTickets.MiddleName,'')as MiddleName, dbo.tblTickets.Lastname, dbo.tblTicketsViolations.Statutenumber AS StatuteNumber,   
Ofirstname =   
case   
when left(dbo.tblcourtviolationstatus.Description,3) = 'ARR' then dbo.FN_GetCaseSequences(dbo.tblTickets.ticketid_pk)   
else   
--Afaq 8608 12/28/2010 Remove officer name having first name 'Unkown', '?' , 'UNK', '-UNK','NA' or 'N/A'
-- Babar Ahmad 9184 05/09/2011 Added three more keywords 'Unknwn', 'unknon', '??'
case when isnull(dbo.tblOfficer.FirstName,'N/A') IN ('Unkown', '?' , 'UNK', '-UNK', 'N/A','NA', 'Unknwn', 'unknon', '??') THEN '' ELSE dbo.tblOfficer.FirstName END   
end   
,   
Olastname = case   
when left(dbo.tblcourtviolationstatus.Description,3) = 'ARR' then ' '   
else   
--Afaq 8608 12/28/2010 Remove officer name having last name 'Unkown', '?' , 'UNK', '-UNK','NA' or 'N/A'  
-- Babar Ahmad 9184 05/09/2011 Added three more keywords 'Unknwn', 'unknon', '??'
case when isnull(dbo.tblOfficer.LastName,'N/A') IN ('Unkown', '?' , 'UNK', '-UNK', 'N/A','NA', 'Unknwn', 'unknon', '??') THEN '' ELSE dbo.tblOfficer.LastName END   
end,   
   
 dbo.tblTickets.CaseTypeId as CaseType, --Sabir Khan 4527 08/25/2008  
 -- Sabir Khan 11108 08/23/2013 Short description logic for speeding in pearland court has been implemented.  
 case when (tblTicketsViolations.CourtID = 3086 AND tblTicketsViolations.ViolationDescription LIKE '%Speeding%') THEN [dbo].[fn_GetPearLandShortDesc](tblTicketsViolations.RefCaseNumber,tblTicketsViolations.ViolationDescription) ELSE dbo.tblViolations.ShortDesc END AS ShortDesc, 
 dbo.tblTicketsViolations.courtdatemain as currentdateset,   
dbo.tblTicketsViolations.courtid as currentcourtloc,
CASE 
      WHEN dbo.tblTicketsViolations.JuryTrialCount > 1 THEN CONVERT(VARCHAR, dbo.tblTicketsViolations.JuryTrialCount) 
           + 'JR'
      ELSE ''
 END AS JuryCounter, --hafiz 10241 02/08/2012 added the Jury trial counter     
-- tahir 5141 11/14/2008 fixed the spaces in court num.
--convert(int,dbo.tblTicketsViolations.courtnumbermain) as currentcourtnum,   
--Afaq 7577 03/24/2010 Include tblcourtviolationstatus.courtviolationstatusid column
--Sabir Khan 7979 07/12/2010 fix trial docket issue... 
case when len(ltrim(rtrim(isnull(dbo.tblTicketsViolations.courtnumbermain,'')))) = 0 then '0' else convert(VARCHAR,dbo.tblTicketsViolations.courtnumbermain) end as currentcourtnum,
dbo.tblcourtviolationstatus.ShortDescription as description,activeflag,dbo.tblcourtviolationstatus.courtviolationstatusid as courtviolationstatusid1,
--Yasir Kamal 6434 09/02/2009 display comma separated ChargeLevels   
dbo.tblcourts.courtname, dbo.tblcourts.Address,case when dbo.tbltickets.CaseTypeId = 2 THEN ISNULL(dbo.tblChargelevel.levelcode,'N/A') 
ELSE 
''
end	AS levelcode,dbo.tblViolations.description AS violationdescription,   
trialcomments=   
isnull(case   
   
when dbo.tblTickets.trialcomments <> '' then '<font color=black><b>TRI:</b></font><font color=red>' + isnull(dbo.tblTickets.trialcomments,' ') + '</font>'  
end, ' ') +   
   
isnull(case when (select count(FlagID) from tblticketsflag where FlagID=@NameDiscrepancy and TicketID_PK =dbo.tblTickets.ticketid_pk) > 0   
then '<font color=black><b>Name Discrepancy</b></font>' else '' end,'')
-- Abid Ali 4946 When Case is jury trial and has bonds then notification trial letter not send then add "No Trial Letter" and also add reminder status
-- tahir 5489 02/03/2009 for trial letters in batch print.
+ ISNULL(CASE WHEN dbo.tblcourtviolationstatus.categoryid = 4 AND dbo.tblTickets.jurystatus IN(0,2) AND dbo.tblTickets.bondflag = 1  THEN '<font color=black>; <b>No Trial Letter</b></font>' ELSE '' END, '')
--Sabir Khan 5660 03/16/2009 Get date from reminder comments and display...
--+ ISNULL(CASE WHEN dbo.tblcourtviolationstatus.categoryid = 4 AND dbo.tblTickets.bondflag = 1 THEN '<font color=black>; <b>Call Status : ' + tblReminderstatus.Description + '</b></font>' ELSE '' END, '')
--Sabir Khan 5742 04/02/2009 RM has been changed into RC...
-- CASE WHEN LEN(ISNULL(dbo.tblticketsviolations.ReminderComments,''))> 0 THEN '<font color=black> <b>RC:' + dbo.fn_htp_Get_Max_ReminderCommentDate(dbo.tblticketsviolations.TicketID_PK) + '</b></font>' ELSE '<font color=black> <b>RC:N/A</b></font>' END -- Adil 7606 03/24/10 code commented
--Yasir Kamal 7230 02/04/2010 display court apointed flag in trial docket.
+ isnull(case when (select count(FlagID) from tblticketsflag where FlagID=40 and TicketID_PK =dbo.tblTickets.ticketid_pk) > 0   
then '<font color=black> <b>Court Appointed Case</b></font>' else '' end,'') ,
bondflag,firmabbreviation,courttype,   
case accidentflag   
 when 1 then 1   
 else 0   
end as accflag,   
speeding = dbo.getviolationspeed_ver1(0,refcasenumber,fineamount,violationcode),   
convert(varchar(2),month(dbo.tblTicketsViolations.courtdatemain)) + '/' + convert(varchar(2),day(dbo.tblTicketsViolations.courtdatemain)) + '/' + convert(varchar(4),year(dbo.tblTicketsViolations.courtdatemain)) as courtarrdate,   
   
   
CONVERT(FLOAT,(CONVERT(VARCHAR(20), datepart(hour,dbo.tblTicketsViolations.courtdatemain)) + '.' + CONVERT(VARCHAR(20), datepart(minute,dbo.tblTicketsViolations.courtdatemain)))) as courttime,dbo.tblCaseDispositionstatus.description as ViolationStatusID,   -- Adil 8634 12/13/2010 Made court time sort more accurate by adding minutes.
dbo.tblTicketsViolations.violationnumber_pk as violationnumber,address1,   
case   
when dbo.tblTicketsViolations.courtid in (3001, 3002,3003,3075) then midnum  --Fahad 10378 01/09/2013 HMC-W also included along other HMC Courts 
when dbo.tblTicketsViolations.courtid not in (3001, 3002,3003,3075) and (casenumassignedbycourt not like '' and not (casenumassignedbycourt is null)) then casenumassignedbycourt   --Fahad 10378 01/09/2013 HMC-W also excluded along other HMC Courts
when dbo.tblTicketsViolations.courtid not in (3001, 3002,3003,3075) and (casenumassignedbycourt like ' ' or casenumassignedbycourt like '' or casenumassignedbycourt is null) then RefCasenumber   --Fahad 10378 01/09/2013 HMC-W also excluded along other HMC Courts
else ' ' end as midnum,   

--Nasir 10507 10/24/2012 added new column to get case number
(select 
top 1 isnull(tbl01.casenumassignedbycourt, '')
from dbo.tblTicketsViolations tbl01
inner join dbo.tblViolations tbl02 on tbl02.ViolationNumber_PK = tbl01.ViolationNumber_PK
where tbl02.description <> 'failure to appear'
and tbl01.ticketID_PK = dbo.tblTicketsViolations.ticketID_PK
AND isnull(tbl01.casenumassignedbycourt, '') <> '' AND tbl01.casenumassignedbycourt <> 'N/A') VCaseNumber,  
   
violationtype, dbo.tblCourts.Address as courtaddress,   
dbo.tblcourtviolationstatus.categoryid as courtviolationstatusid   
,refcasenumber,   
-- Adil 7606 03/24/10 add reminder call into offense location
CASE WHEN LEN(ISNULL(dbo.tblticketsviolations.ReminderComments,''))> 0 THEN '<font color=black> <b>RC:' + dbo.fn_DateFormat(dbo.fn_htp_Get_Max_ReminderCommentDate(dbo.tblticketsviolations.TicketID_PK),1,'/',':',0) + '</b></font>' ELSE '<font color=black> <b>RC:N/A</b></font>' END -- Adil 7606 03/29/2010 date format changed 'yy' used instead of 'yyyy'
AS ReminderCallDate,
(select top 1 isnull(tvv.OffenseLocation,'')  from tblticketsviolations tvv where tvv.ticketid_pk = tblTicketsViolations.ticketid_pk ) 
as OffenseLocation,

-- Noufil 6340 08/10/2009 Substring all languages
SUBSTRING (ISNULL(dbo.tblTickets.LanguageSpeak,'English'),1,3) AS LanguageSpeak
--case when LanguageSpeak = 'English' then '<b> ENG </b>' Else '<b> SPA </b>' end LanguageSpeak -- Add Language Speaks Column Task # 3602 Shekhani
into #temp   
FROM dbo.tblTickets WITH(NOLOCK) INNER JOIN   
 dbo.tblTicketsViolations WITH(NOLOCK) ON dbo.tblTickets.TicketID_PK = dbo.tblTicketsViolations.TicketID_PK INNER JOIN   
 dbo.tblCaseDispositionstatus WITH(NOLOCK) ON dbo.tblTicketsViolations.violationstatusid = dbo.tblCaseDispositionstatus.casestatusid_pk INNER JOIN   
 dbo.tblfirm WITH(NOLOCK) ON dbo.tblTickets.firmid = dbo.tblfirm.firmid INNER JOIN   
 dbo.tblViolations WITH(NOLOCK) ON dbo.tblTicketsViolations.ViolationNumber_PK = dbo.tblViolations.ViolationNumber_PK INNER JOIN   
 dbo.tblcourtviolationstatus WITH(NOLOCK) ON dbo.tblTicketsviolations.courtviolationstatusidmain = dbo.tblcourtviolationstatus.courtviolationstatusID INNER JOIN   
 dbo.tblCourts WITH(NOLOCK) ON dbo.tblTicketsViolations.courtid = dbo.tblCourts.Courtid LEFT OUTER JOIN   
 dbo.tblOfficer WITH(NOLOCK) ON isnull(dbo.tblTicketsViolations.ticketOfficerNumber, dbo.tblTickets.OfficerNumber) = dbo.tblOfficer.OfficerNumber_PK   
--dbo.tblOfficer ON isnull(dbo.tblTicketsViolations.ticketOfficerNumber, isnull(dbo.tblTickets.OfficerNumber, 
--(select top 1 tvf.ticketOfficerNumber from dbo.tblTicketsViolations tvf 
--where tvf.ticketid_pk = dbo.tblTicketsViolations.ticketid_pk and tvf.ticketOfficerNumber is not null))) = dbo.tblOfficer.OfficerNumber_PK   
  -- Abid Ali 4946 for reminder status
 LEFT OUTER JOIN tblReminderstatus ON tblTicketsViolations.ReminderCallStatus = tblReminderstatus.Reminderid_pk  
 LEFT OUTER JOIN tblChargelevel ON tblTicketsViolations.chargelevel = tblchargelevel.id 
WHERE (dbo.tblViolations.Violationtype not IN (1) or dbo.tblViolations.violationnumber_pk in (11,12) ) AND (dbo.tblcourtviolationstatus.categoryid IN (select distinct categoryid from tblcourtviolationstatus where violationcategory in (0,2)   
-- Abbas Shahid Khwaja 9334 05/31/2011 Include only mike monk cases... 
AND ((@IsNotForRuiz = 0) OR (@IsNotForRuiz = 1 AND tblTicketsViolations.CoveringFirmID = 3041)) 
-- Muhammad Nasir 10507 12/12/2012 Added categoryID 2 (ARRAIGNMENT) to exlude results from select list
and categoryid not in (2,7,50) and courtviolationstatusid not in (104, 201) ))   
AND (dbo.tblTickets.Activeflag = 1)   
and dbo.tblTicketsViolations.courtid <> 0   
and datediff(DAY,dbo.tblTicketsViolations.courtdatemain,@courtdate) = 0 and datediff(DAY,'1/1/1900',@courtdate) <> 0
  
select DISTINCT ticketid,ISNULL( T.totalfeecharged - SUM( A.ChargeAmount), 0) as dueamount , t.continuanceamount as continuance , continuanceStatus   
 ,T.ContinuanceDate   
 into #temp2   
 from tblticketspayment A WITH(NOLOCK),tbltickets T WITH(NOLOCK) where   
 T.ticketid_pk = A.ticketid   
 and activeflag = 1   
 and paymentvoid=0   
 and paymenttype not in (99,100)   
 group by ticketid,totalfeecharged, t.continuanceamount ,continuanceStatus , T.ContinuanceDate   
   
 declare @sqlquery varchar(max)   
 declare @sqlquery1 varchar(max)   
   
 set @sqlquery = ' '   
   
 select T.*,T2.continuance ,T2.continuanceStatus, T2.ContinuanceDate,   
 dueamount into #temp3   
 FROM #temp T   
 left outer join #temp2 T2 on T.ticketid_pk = T2.ticketid   
   
   
 SELECT #temp3.*,count(*) as clientcount into #temp4   
 FROM #temp3 RIGHT OUTER JOIN   
 tblTickets T ON #temp3.Firstname = T.Firstname AND #temp3.Lastname = T.Lastname AND   
 (isnull(#temp3.Address1,'N/A') = isnull(T.Address1,'N/A') or #temp3.midnum = T.midnum)   
 where t.activeflag = 1   
 GROUP BY   
 #temp3.TicketID_PK, #temp3.Firstname, #temp3.MiddleName, #temp3.Lastname, #temp3.Ofirstname,#temp3.courtviolationstatusid1,   
 #temp3.Olastname, #temp3.ShortDesc, #temp3.currentdateset, #temp3.currentcourtloc,#temp3.levelcode,#temp3.violationdescription,   
 #temp3.currentcourtnum, #temp3.Description, #temp3.activeflag, #temp3.courtname, #temp3.Address,   
 #temp3.trialcomments, #temp3.bondflag, #temp3.firmabbreviation, #temp3.courttype, #temp3.accflag,   
 #temp3.speeding, #temp3.continuance, #temp3.dueamount, #temp3.courtarrdate, #temp3.courttime, #temp3.ViolationStatusID,   
 #temp3.violationnumber, #temp3.address1, #temp3.midnum, #temp3.violationtype, #temp3.courtaddress,   
 #temp3.courtviolationstatusid, #temp3.refcasenumber , #temp3.continuanceStatus ,#temp3.continuanceDate,
 #temp3.OffenseLocation, #temp3.LanguageSpeak, #temp3.ReminderCallDate,#temp3.CaseType, -- Sabir Khan 4527 08/25/2008   
 #temp3.StatuteNumber, --Muhammad Muneer 8240 09/08/2010 Added the new column Statutenumber
 #temp3.JuryCounter, --hafiz 10241 02/08/2012 added the Jury trial counter   
 #temp3.VCaseNumber	--Nasir 10507 10/24/2012 added new column to get case number
   
																										--Sabir Khan 4527 08/25/2008
 set @sqlquery = 'select P.TicketID_PK,P.Firstname,P.MiddleName,P.Lastname,P.Ofirstname,P.Olastname,P.CaseType,P.courtviolationstatusid1,    
 replace(P.ShortDesc,''None'',''Other'') as shortdesc,P.currentdateset,p.levelcode,p.violationdescription,   
 P.currentcourtloc,
 --hafiz 10241 02/08/2012 added the Jury trial counter  
 P.JuryCounter,  
 P.VCaseNumber, --Nasir 10507 10/24/2012 added new column to get case number
 P.currentcourtnum,P.Description,P.activeflag,P.courtname,   
 P.Address,P.trialcomments,P.bondflag,P.firmabbreviation,P.courttype,P.accflag,P.speeding,   
 P.continuance,p.continuancestatus,p.ContinuanceDate, P.dueamount,P.courtarrdate,P.courttime,P.ViolationStatusID,   
 P.violationnumber,P.clientcount,T.coveringfirmid,dbo.fn_HTS_GetCoveringFirm(p.ticketid_pk,' + Convert(varchar,@IsNotForRuiz) + ') as coveringfirm,   
 P.violationtype,courtaddress,cdlflag,courtviolationstatusid,P.StatuteNumber,--Muhammad Muneer 8240 09/08/2010 Added the new column Statutenumber  
 convert(varchar(20),P.TicketID_PK) + isnull(convert(varchar(4),courtviolationstatusid),0) as ticketstatusid,
-- pretrialstatus = case   
-- when pretrialstatus = 1 and T.currentcourtloc between 3001 and 3022 and left(Description,3) <> ''JUR'' then ''RFT''   
-- when pretrialstatus =2 and left(Description,3) <> ''JUR'' then ''WBT''   
-- when pretrialstatus =4 and left(Description,3) <> ''JUR'' then ''DA''   
-- when pretrialstatus =5 and left(Description,3) <> ''JUR'' then ''DSC''   
-- else '' '' end, 
AccidentFlag, p.midnum, p.refcasenumber, t.dob ,convert(int,( dbo.Fn_CheckCaseDiscrepency(p.TicketID_pk , '''+ @courtdate+'''))) as Discrepency, 
 p.OffenseLocation, p.LanguageSpeak, p.ReminderCallDate into #temp5   
 from #temp4 P,dbo.tbltickets T,dbo.tblfirm F where P.ticketid_pk = T.ticketid_pk and 
 case when T.coveringfirmid = 0 then 3000 else T.coveringfirmid end = F.firmid and P.ticketid_pk is not null '   

  --Sabir Khan 4527 08/25/2008 Check rule for Case Type.
 ------------------------------------------------------
 if @Courtloc ='Traffic' or @courtloc = 'Criminal' or @courtloc = 'Civil'
 Begin
	 if @courtloc = 'Traffic'
		 begin
			 set @sqlquery = @sqlquery + ' and P.CaseType = 1 '
		 end
	 if @courtloc = 'Criminal'
		 begin
			set @sqlquery = @sqlquery + ' and P.CaseType = 2 '
		 end
	 if @courtloc = 'Civil'
		 begin
			set @sqlquery = @sqlquery + ' and P.CaseType = 3 '
		 end
 End
------------------------------------------------------
 ELSE
	 Begin
	 if @courtloc is not null and @courtloc <> 'None'   
		 begin   
		 if @courtloc = 'JP'
			 BEGIN
	 			set @sqlquery = @sqlquery + ' and P.currentcourtloc not in (3001,3002,3003,3075)'   --Fahad 10378 01/09/2013 HMC-W also excluded along other HMC Courts
			 END 
			 -- Afaq 7154 07/28/2010 To display only inside court.
		 ELSE if @courtloc = 'IN' OR @courtloc = 'OU' -- Adil 7635 04/07/2010 To display only inside court cases.
			 BEGIN 
	 			set @sqlquery = @sqlquery + ' and P.currentcourtloc in (3001,3002,3003,3075)'   --Fahad 10378 01/09/2013 HMC-W also included along other HMC Courts
			 END 
		 ELSE   
			 begin   
				set @sqlquery = @sqlquery + ' and P.currentcourtloc = ' + @courtloc   
			 end   
		 end
	 END
	  
 if @courtdate is not null and @courtdate <> '01/01/1900'   
 begin   
 set @sqlquery = @sqlquery + ' and datediff(day,P.currentdateset,''' + @courtdate + ''') = 0'   
 end   
 if @courtnumber <> '-1'   
 begin   
 set @sqlquery = @sqlquery + ' and P.currentcourtnum = ' + @courtnumber   
 end   
 if @datetype <> '0'   
 begin   
 set @sqlquery = @sqlquery + ' and courtviolationstatusid in (' + @datetype + ')'   
 end
--Afaq 7577 03/24/2010 Include record for arraignment
if @datetype = '5,2'
begin --Sabir Khan 9467 7/12/2011 Fixed Judge Trial / Arrignment Report for Arrignment Cases...
set @sqlquery = @sqlquery + '   and (courtviolationstatusid in (2,5))'
end   
 set @sqlquery = @sqlquery + ' and ((P.ShortDesc not in (''CDL'',''ACC'',''PROB'')) or (P.ShortDesc is null)) order by P.courtarrdate,   
 P.currentcourtloc,P.Address,convert(varchar,P.currentcourtnum) asc,P.courttime,P.lastname,P.firstname,P.middlename   
 --select * from #temp5 where ticketid_pk = 102979   
 '   
   
 set @sqlquery = @sqlquery + 'select * into #violations from tblviolations where description like ''%speeding%'' and violationtype <> 0 '   
   
 set @sqlquery = @sqlquery + 'select T.*, isnull(V.violationnumber_pk,0) as speedingvnum   
 into #temp6 from #temp5 T   
 left outer join #violations V on T.violationnumber = V.violationnumber_pk   
 order by case T.courtviolationstatusid when 9 then 9   
 when 0 then 9 else 1 end   
 ,T.courtarrdate,T.currentcourtloc,T.Address,convert(varchar,T.currentcourtnum) asc,   
 T.courttime,T.lastname,T.firstname,T.middlename,T.courtviolationstatusid,T.StatuteNumber  --Muhammad Muneer 8240 09/08/2010 Added the new column Statutenumber   
 drop table #temp5   
 declare @ticketID int   
 declare @tbl1 table ( ticketid int, shortdesc1 varchar(1000),levelcode1 varchar(200),statutenumber varchar(50))   --Muhammad Muneer 8240 09/08/2010 Added the new column Statutenumber  
 declare @strVaue varchar(200)
 declare @strVaue1 varchar(200)
 declare @strVaueStatute varchar(50)  
 declare Crtemp cursor for   
 select distinct ticketID_pk from #temp6   
 --tahir   
 open Crtemp   
 fetch next from Crtemp   
 into @ticketid   
 WHILE @@FETCH_STATUS = 0   
 BEGIN   
 --set @strVaue = ''&nbsp;&nbsp;'' 
 set @strVaue = '''' 
 set @strVaue1 = ''''
 set @strVaueStatute=''''  
-- select @strVaue = @strVaue+ case isnull(shortdesc,'''') when ''spd'' then ''spd, '' else isnull(shortdesc,'''')+'', '' end from #temp6 where ticketID_pk = @ticketid   
-- select @strVaue1 = @strVaue1 + case when len(isnull(levelcode,'''')) > 0 then violationdescription +'' (''+  levelcode + ''),''  else @strVaue1 end  from #temp6 where ticketID_pk = @ticketid 
-- select @strVaueStatute=@strVaueStatute+ isnull(StatuteNumber,'''')  +'', '' end from #temp6 where ticketID_pk = @ticketid

select  @strVaue = @strVaue+ case isnull(shortdesc,'''') when ''spd'' then ''spd, '' when '''' then '''' else isnull(shortdesc,'''')+'', '' end  , --Sabir Khan 8762 01/05/2010 removed Unnecessary comma''s
        @strVaue1 = @strVaue1 + case when len(isnull(levelcode,'''')) > 0 then violationdescription +'' (''+  levelcode + ''),''  else @strVaue1 end ,
		@strVaueStatute=@strVaueStatute+ case isnull(StatuteNumber,'''')  when '''' then '''' else +'', '' end --Sabir Khan 8762 01/05/2010 removed Unnecessary comma''s   
from #temp6 where ticketID_pk = @ticketid


 --SELECT @strVaue1 = CASE WHEN SUBSTRING(@strVaue1,LEN(@strVaue1),1) = '','' THEN LEFT(@strVaue1, LEN(@strVaue1)-1) ELSE @strVaue1 END
 IF len(replace(REPLACE(@strVaue1, ''N/A'',''''),'','','''')) = 0 set @strVaue1 = ''''
 select distinct @strVaue = case @strVaue when ''spd,'' then ''spd,'' else @strVaue end +case abs(speeding)when '' '' then '' '' else ''(''+convert(varchar(20),abs(speeding))+ '')'' end from #temp6 where ticketID_pk = @ticketid   
 if len(@strvaue) > 50   
 set @strvaue = stuff(@strvaue,charindex('','',@strvaue,50)+1,0,'' '')   
 insert into @tbl1 values(@ticketid, @strVaue,@strVaue1,@strVaueStatute)   
 fetch next from Crtemp   
 into @ticketid   
 END   
 close Crtemp   
 DEALLOCATE Crtemp   
 SELECT TicketId_Pk as CaseNumber,   
 '':''+lastName Client_LastName , t2.shortdesc1,t2.levelcode1,TicketID_PK ,   
 Firstname+'',''+MiddleName as FMname,   
 (case isnull(Olastname,'''') when '''' then '''' when ''N/A'' then '''' else Olastname + '', '' end
 + case isnull(Ofirstname,'''') when '''' then '''' when ''N/A'' then '''' else Ofirstname end) OLFNAME,
 currentcourtloc,
 --hafiz 10241 02/08/2012 added the Jury trial counter  
 JuryCounter,        
 VCaseNumber, --Nasir 10507 10/24/2012 added new column to get case number
 currentcourtnum, Description, courtname+''-''+Address+''-''+   
 (convert(varchar(10),DatePart(m,currentdateset))+''/''+convert(varchar(10),DatePart(d,currentdateset))+''/''+convert(varchar(10),DatePart(yyyy,currentdateset))) as CourtName_Address_DateSet,   
 bondflag,convert(varchar(10),cast (dueamount as decimal)) dueamount ,firmabbreviation, courttime,   
 case clientcount when 1 then '''' else ''*''+convert(varchar(5),clientcount) end ClientCount,   
 -- Added by Ozair on 12/18/2007 Ttask 2383  
 case when ( select count(TicketID_PK) from tblticketsflag tf where tf.ticketid_pk = t1.TicketId_Pk and flagid = 8 ) > 0 then firmabbreviation+''-CL''+'' '' else '''' end +   
 --additionend  
 case clientcount when 1 then '''' else ''*''+convert(varchar(5),clientcount) end+ ''''+   
   
 ltrim( case AccidentFlag   
 when 0 then '''' else ''ACC'' end +'' ''+   
 case cdlflag   
 when 0 then '''' else ''CDL'' end +'' ''+   
   
case when ( select count(TicketID_PK) from tblticketsflag tf where tf.ticketid_pk = t1.TicketId_Pk and flagid = 1 ) > 0 then ''&nbsp;P'' else '''' end +   
 case when ( select count(TicketID_PK) from tblticketsflag tf where tf.ticketid_pk = t1.TicketId_Pk and flagid = 2 ) > 0 then ''&nbsp;Attn'' else '''' end +   
 case when ( select count(TicketID_PK) from tblticketsflag tf where tf.ticketid_pk = t1.TicketId_Pk and flagid = 9 ) > 0 then
 --Muhammad Ali 8236 09/06/2010 --> Get and Concanate all Continuance Date i.e Cont-09/06/2010;Cont-09/19/2010.......
 ''&nbsp;''+(dbo.fn_get_ConcatenateContinuanceDates(t1.TicketId_Pk)) else '''' end +   
 case when ( select count(TicketID_PK) from tblticketsflag tf where tf.ticketid_pk = t1.TicketId_Pk and flagid = 15 ) > 0 then ''&nbsp;NOS'' else '''' end +   
 case when ( select count(TicketID_PK) from tblticketsflag tf where tf.ticketid_pk = t1.TicketId_Pk and flagid = 21 ) > 0 then ''&nbsp;W'' else '''' end +   
 case when ( select count(TicketID_PK) from tblticketsflag tf where tf.ticketid_pk = t1.TicketId_Pk and flagid = 22 ) > 0 then ''&nbsp;U'' else '''' end +   
 case when ( select count(TicketID_PK) from tblticketsflag tf where tf.ticketid_pk = t1.TicketId_Pk and flagid = 20 ) > 0 then ''&nbsp;<font color=red>PR</font>'' else '''' end +   
 case when ( select count(TicketID) from tblScanBook ts where ts.ticketid = t1.TicketId_Pk and DocTypeID = 14 ) > 0 then ''&nbsp;POA'' else '''' end +   
 case   when datediff(year,isnull(dob, ''1/1/1900''),currentdateset) <= 25 then ''<25''  else '' ''   
 end )   as ClientCount_PreTrialStatus_CDLFLAG, 
coveringfirm,dbo.formattime(t1.currentdateset) as currentdateset,'' '' as space,t1.accflag,   
 (trialcomments +'' ''+''<font color=black>''+ case when Convert(varchar,ContinuanceDate,101) = '''+ convert(varchar,convert(datetime,@courtdate),101) + ''' then   
 (select shortdescription from tblcontinousstatus where statusid = isnull(continuancestatus,1)) else '''' end   
 + ''</font>'') as trialcomments   
 , midnum, refcasenumber , Discrepency, OffenseLocation,   -- Adil 7606 03/24/10 add language into offense location for trial docket report only.
 LanguageSpeak, ReminderCallDate,
 ''Yes'' as BondDocuments,CaseType,convert(varchar,courtarrdate,101) as courtarrdate, --Nasir 6968 12/14/2009 add courtarrdate,
 t2.statutenumber as StatueNumber
 into #temp7   
 FROM #temp6 t1, @tbl1 t2   
 where t2.ticketid = t1.TicketID_PK   
 order by currentcourtnum   
   
 SELECT distinct casenumber, count(ticketid_pk) as violationCount ,Client_LastName, isnull(shortdesc1,'''') shortdesc1,levelcode1,TicketID_PK ,   
 FMname,OLFNAME , currentcourtloc,
  --hafiz 10241 02/08/2012 added the Jury trial counter  
 JuryCounter,    
 VCaseNumber, --Nasir 10507 10/24/2012 added new column to get case number
case currentcourtnum   
 when '''' then '''' else currentcourtnum end   
currentcourtnum  
, Description, CourtName_Address_DateSet,  
'  
--updated by adil and khalid bug 2625 18-1-08  
if @ShowOwesDetail = 1   
 begin  
 set @sqlquery = @sqlquery + 'case bondflag when 0 then   
 case dueamount when 0 then '''' else ''$'' + dueamount end   
 else  
 case BondDocuments when ''No'' then ''B?'' else ''B'' end + case dueamount when 0 then '''' else ''$'' + dueamount end end as BondFlag,'  
 End  
Else  
 Begin  
 set @sqlquery = @sqlquery + 'case bondflag when 0 then   
   case dueamount when 0 then '''' else ''$'' end   
 else  
 case BondDocuments when ''No'' then ''B?'' else ''B'' end + case dueamount when 0 then '''' else ''$'' end end as BondFlag,'  
 End  
  
 set @sqlquery = @sqlquery +' firmabbreviation, courttime,   
 min(clientcount) as clientcount,ClientCount_PreTrialStatus_CDLFLAG as ClientCount_PreTrialStatus_CDLFLAG ,coveringfirm as CoveringFirm,currentdateset,space,accflag,trialcomments,midnum ,refcasenumber,Discrepency, OffenseLocation, LanguageSpeak, ReminderCallDate,CaseType ,courtarrdate,  --Nasir 6968 12/14/2009 add courtarrdate --Afaq
StatueNumber 
 into #temp8 FROM #temp7   
   
 group by currentcourtloc, currentcourtnum, CourtName_Address_DateSet,accflag,   
 TicketID_PK, shortdesc1,levelcode1,FMname, Client_LastName, OLFNAME, midnum, refcasenumber,   
 Description, bondflag, firmabbreviation,   
 courttime, ClientCount_PreTrialStatus_CDLFLAG,coveringfirm, currentdateset, casenumber,space, 
 --Nasir 6968 12/14/2009 add courtarrdate  
 trialcomments, dueamount,Discrepency, OffenseLocation, LanguageSpeak, ReminderCallDate,BondDocuments,CaseType,courtarrdate 
 , StatueNumber
 --hafiz 10241 02/08/2012 added the Jury trial counter  
 ,JuryCounter
 ,VCaseNumber --Nasir 10507 10/24/2012 added new column to get case number
 order by currentcourtloc   
   
 '   
 --print @sqlquery     
	 set @sqlquery1='';   
	 -- Noufil 6340 09/03/2009 Alias added for table #temp8 and use alias with columns
	 set @sqlquery1=@sqlquery1+'   
	 SELECT distinct a.casenumber,   
	 sum(a.violationCount) as violationCount, min(a.Client_LastName)Client_LastName ,
	 min(isnull(a.shortdesc1,'''')) shortdesc1,levelcode1, 
	 min(a.TicketID_PK)TicketID_PK ,   
	 min(a.OLFNAME) OLFNAME , 
	 a.currentcourtloc,
	  --hafiz 10241 02/08/2012 added the Jury trial counter 
	  a.JuryCounter,      
	  a.VCaseNumber, --Nasir 10507 10/24/2012 added new column to get case number
	 min(a.FMname) as FMname,
	 a.currentcourtnum, 
	 a.Description, 
	 a.accflag, 
	 a.CourtName_Address_DateSet, 
	 min(a.BondFlag) BondFlag,   
	 min(a.firmabbreviation) firmabrevation, 
	 min(a.courttime) courttime,   
	 min(a.clientcount) as clientcount,
	 min(a.ClientCount_PreTrialStatus_CDLFLAG)ClientCount_PreTrialStatus_CDLFLAG, 
	 min(a.CoveringFirm) as CoveringFirm,
	 min(a.currentdateset) currentdateset,
	 min(a.space) as Space,   	 
	 -- Noufil 6340 08/10/2009 "In custody" added in TrialComments. and case type added
	 (case when (SELECT count(ttf.FlagID) FROM tblTicketsFlag ttf  WHERE ttf.TicketID_PK = a.TicketID_PK AND ttf.FlagID=38 and isnull(ttf.DeleteFlag,0)=0 and CaseType=2 ) > 0 then  min(trialcomments)+ '' <font color=red><b>(In Custody)</b></font>'' else min(trialcomments)  end ) as trialcomments,
	 min(a.midnum)as midnum, 
	 min(a.refcasenumber) as refcasenumber , 
	 --Nasir 6968 12/14/2009 add courtarrdate
	 a.Discrepency, a.OffenseLocation, a.LanguageSpeak, a.ReminderCallDate, a.CaseType,a.courtarrdate , a.StatueNumber
	 into #temp9 FROM #temp8 a
	 group by casenumber,currentcourtloc,currentcourtnum,levelcode1,CourtName_Address_DateSet,Description , Discrepency, OffenseLocation, LanguageSpeak, 
	 ReminderCallDate,CaseType,a.TicketID_PK,a.courtarrdate,a.accflag, 
	 a.StatueNumber--hafiz 10241 02/08/2012 added the Jury trial counter  
	 , JuryCounter     --Nasir 6968 12/14/2009 add courtarrdate
	 , VCaseNumber --Nasir 10507 10/24/2012 added new column to get case number
	 
	 --Zeeshan Haider 11081 06/13/2013 FTCS
	 update #temp9
	 set shortdesc1 = replace(shortdesc1, ''Basic Rep'',''ftcs'')
	 where accflag = 1
	 
	  --Fahad 7635 04/08/2010 updated from "left(OLFNAME,13)" to "left(OLFNAME,30)"
	 SELECT distinct casenumber, violationCount ,Client_LastName,case when len(shortdesc1) > 1 then left(shortdesc1, len(shortdesc1) - 1) else '''' end as shortdesc1,case when len(levelcode1) > 1 then left(levelcode1, len(levelcode1) - 1) else '''' end as levelcode1,TicketID_PK ,   
	 left(FMname,13) as FMname ,left(OLFNAME,30) as OLFNAME , currentcourtloc,currentcourtnum, left(Description, 3) as Description, CourtName_Address_DateSet, BondFlag,   
	 firmabrevation, courttime,clientcount,ClientCount_PreTrialStatus_CDLFLAG ,--hafiz 10241 02/08/2012 added the Jury trial counter   
	 JuryCounter, 
	 VCaseNumber, --Nasir 10507 10/24/2012 added new column to get case number
	 CoveringFirm ,currentdateset,Space,   
	 trialcomments, dbo.USP_HTS_Get_offenceDates(ticketid_pk) as offenceDate, isnull(midnum, right(refcasenumber,7)) as midnum ,Discrepency , OffenseLocation, LanguageSpeak, ReminderCallDate,CaseType,  
	 0 as GroupID,courtarrdate,accflag, --Nasir 6968 12/14/2009 add courtarrdate
	 StatueNumber
	 into #temp10  
	 from #temp9 where Description Not In(''BOND'',''COM'',''DSCD'', ''DAR'',''DPX'',''CLO'',''B/W'',''WAIT'''   
	   
	 if @singles = 1 --this will only add if trial docket prints singles Document.   
	 begin   
	 set @sqlquery1=@sqlquery1+' ,''SS'',''DEF'' '   
	 end   
	   
	 set @sqlquery1=@sqlquery1+ ' ) and Description in (''ARR'',''PRE'',''JUR'',''JUR2'',''JUD'',''BF'',''A/W'',''TRI'') order by currentcourtloc, currentcourtnum, courttime , Client_LastName ;   
	 insert into #temp10   --Fahad 7635 04/08/2010 updated from "left(OLFNAME,13)" to "left(OLFNAME,30)"
	 SELECT distinct casenumber, violationCount ,Client_LastName,case when len(shortdesc1) > 1 then left(shortdesc1, len(shortdesc1) - 1) else '''' end as shortdesc1,case when len(levelcode1) > 1 then left(levelcode1, len(levelcode1) - 1) else '''' end as levelcode1,TicketID_PK ,   
	 left(FMname,13) as FMname ,left(OLFNAME,30) as OLFNAME , currentcourtloc,currentcourtnum, left(Description, 3) as Description, CourtName_Address_DateSet, BondFlag,   
	 firmabrevation, courttime,clientcount,ClientCount_PreTrialStatus_CDLFLAG
	 --hafiz 10241 02/08/2012 added the Jury trial counter  
	 ,JuryCounter
	 ,VCaseNumber --Nasir 10507 10/24/2012 added new column to get case number
	 ,CoveringFirm, currentdateset,Space,   
	 trialcomments, dbo.USP_HTS_Get_offenceDates(ticketid_pk) as offenceDate, isnull(midnum, right(refcasenumber,7)) as midnum ,Discrepency, OffenseLocation, LanguageSpeak, ReminderCallDate,CaseType, 
	 1 as GroupID ,courtarrdate,accflag, StatueNumber --Nasir 6968 12/14/2009 add courtarrdate
	 from #temp9 where Description Not In(''BOND'',''COM'',''JUR2'',''DSCD'',''DAR'',''DPX'',''CLO'',''B/W'',''WAIT'' '   
	  
	 if @singles = 1 --this will only add if trial docket prints singles Document.   
	 begin   
	 set @sqlquery1=@sqlquery1+' ,''SS'',''DEF'' '   
	 end   
	   
	 set @sqlquery1=@sqlquery1+ ' ) and Description not in (''ARR'',''PRE'',''JUR'',''JUD'',''BF'',''A/W'',''TRI'') order by currentcourtloc, currentcourtnum, courttime , Client_LastName ;'   
		-- 07/08/08 Sabir 4349
	
	 --Zeeshan Haider 11081 06/13/2013 FTCS
	 set @sqlquery1=@sqlquery1+' update #temp10
	 set shortdesc1 = replace(shortdesc1, ''Basic Rep'',''ftcs'')
	 where accflag = 1 '
		
	IF (@showjudgereport=0) -- Noufil 5895 05/11/2009 Show trial docket report if parameter has value 0 else show judge report.
	BEGIN
		-- Adil 8021 07/15/2010 CrtOrder added for court room number sorting.
		SET @sqlquery1 = @sqlquery1 +  ' if exists(select ''temp9'', * from #temp9) begin insert into #TempExists values(''temp9'', 1) end 
		--Nasir 6968 12/14/2009 add Attorney name and contact number
		select *, ''' + @URL + ''' as URL,tu.Firstname + '' '' + tu.Lastname AS OnCallAtorneyName,ISNULL(dbo.formatphonenumbers(as1.ContactNumber),'''') as AttorneyContactNumber,
		CASE WHEN ISNUMERIC(currentcourtnum) = 1 THEN currentcourtnum ELSE (ASCII(CASE WHEN SUBSTRING(currentcourtnum, 1, 1) = '''' THEN ''0'' ELSE SUBSTRING(currentcourtnum, 1, 1) END)) END
		as CrtOrder from #temp10 
		left join AttorneysSchedule as1 on CONVERT(VARCHAR, (CONVERT(DATETIME, #temp10.courtarrdate)),101) = convert(varchar, as1.DocketDate ,101)
		left join tblUsers tu on as1.AttorneyID= tu.EmployeeID
		 order by GroupID, currentcourtloc, currentcourtnum, courttime , Client_LastName ;   
		if exists(select ''temp10'', * from #temp10) begin insert into #TempExists values(''temp10'', 1) end '	
	END
	ELSE
	BEGIN
		-- Adil 5895 05/26/2009 fixing duplicate records issue
		-- Adil 8021 07/15/2010 CrtOrder added for court room number sorting.
		SET @sqlquery1 = @sqlquery1 +  ' if exists(select ''temp9'', * from #temp9) begin insert into #TempExists values(''temp9'', 1) end 
		select (Select top 1 casenumassignedbycourt from tblticketsviolations ttv1 where ttv1.ticketid_pk =  #temp10.ticketid_pk and datediff(day,ttv1.courtdate, ''' + @courtdate + ''') = 0 and ttv1.CourtNumbermain = #temp10.currentcourtnum) as casenumassignedbycourt, *, ''' + @URL + ''' as URL,
		CASE WHEN ISNUMERIC(currentcourtnum) = 1 THEN currentcourtnum ELSE (ASCII(CASE WHEN SUBSTRING(currentcourtnum, 1, 1) = '''' THEN ''0'' ELSE SUBSTRING(currentcourtnum, 1, 1) END)) END
		as CrtOrder from #temp10 
		order by GroupID, currentcourtloc, currentcourtnum, courttime , Client_LastName ;   
		if exists(select ''temp10'', * from #temp10) begin insert into #TempExists values(''temp10'', 1) end '

	END

	 if @ShowOtherResultSets = 1 -- Adil 5772 04/15/2009
	 BEGIN
 		set @sqlquery1=@sqlquery1+ '
		select distinct currentcourtloc from #temp9   
		where Description Not In(''BOND'',''COM'',''JUR2'',''DSCD'',''DAR'',''DPX'',''CLO'',''B/W'',''WAIT'' ) ;   
		'
	 end
  
 -- 07/08/08 Sabir 4349
 
 if @ShowOtherResultSets = 1 -- Adil 5772 04/15/2009
 BEGIN
 	set @sqlquery1=@sqlquery1+ '
	select distinct currentcourtloc, currentcourtnum from #temp9   
	where Description Not In(''BOND'',''COM'',''JUR2'',''DSCD'',''DAR'',''DPX'',''CLO'',''B/W'',''WAIT'' );   
	'
 END
 

   
 set @sqlquery1=@sqlquery1+ 'drop table #temp6;drop table #temp7; drop table #temp8; drop table #temp9;drop table #temp10;'    
   
--select len(@sqlquery) + len(@sqlquery1)   
   
 print @sqlquery  
 print @sqlquery1
 exec (@sqlquery+ ' '+@sqlquery1)   
 
 
   
 
 
 
if exists(select * from  #TempExists where tablename in ('temp9', 'temp10'))-- 07/08/08 Sabir 4349
BEGIN
set @isdatafound=1
END
else
BEGIN
set @isdatafound=0
END
  
 drop table #temp2   
 drop table #temp3   
 drop table #temp4   
   
--end   
drop table #temp   




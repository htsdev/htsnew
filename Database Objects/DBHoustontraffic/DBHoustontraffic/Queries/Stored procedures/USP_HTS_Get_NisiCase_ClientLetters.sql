USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTS_Get_NisiCase_ClientLetters]    Script Date: 05/15/2013 05:48:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*    
Created By: Rab Nawaz    
Date:   04/16/2013
Task ID :	10729    
    
Business Logic:    
 The stored procedure is used to get Nisi cases Client information to Print the letters for the clients     
*/
-- [dbo].[USP_HTS_Get_NisiCase_ClientLetters] 413568, 4123, '2012NISI0010240,2012NISI0010206,2012NISI0010207,2012NISI0010208,', '2012FTA0021982,2010TR0877252,2010TR0877254,2010TR0877255,'
-- [dbo].[USP_HTS_Get_NisiCase_ClientLetters] 430576, 4123, '2013NISI0001015,', '2012TR0413024,'
ALTER PROCEDURE [dbo].[USP_HTS_Get_NisiCase_ClientLetters]
	@TicketId INT,
	@employeeId INT = 3992,
	@multiNisi VARCHAR(500),
	@multiUnderlyingCause VARCHAR(500)
AS             
BEGIN  
	SELECT DISTINCT tt.TicketID_PK,
		   tt.Firstname,
		   tt.Lastname,
		   tt.Address1 + ' ' + ISNULL(tt.Address2, '') AS ClientAddress,
		   tt.City,
		   ts.STATE AS ClientSTATE,
		   tt.Zip,
		   --ttv.CourtViolationStatusIDmain,
		   --ISNULL(ttv.CourtDate, '01/01/1900') AS CourtDate,
		   ISNULL(ttv.CourtDateMain, '01/01/1900') AS CourtDateMain,
		   nisi.UnderlyingTicketNumber,
		   nisi.UnderlyingCauseNumber,
		   nisi.NisiCauseNumber,
		   CASE WHEN tt.LanguageSpeak = 'SPANISH' THEN 'SPANISH' ELSE 'ENGLISH' END AS languageSpeak  
	INTO #temp
	FROM   TrafficTickets.dbo.tblTickets tt INNER JOIN TrafficTickets.dbo.tblTicketsViolations ttv
			ON tt.TicketID_PK = ttv.TicketID_PK
			INNER JOIN TrafficTickets.dbo.tblNISI_ClientsUploadedByLoader nisi
			ON ISNULL(nisi.TicketId, 0) = tt.TicketID_PK
		   INNER JOIN TrafficTickets.dbo.tblState ts 
		   ON  ts.StateID = ISNULL(tt.Stateid_FK, 45) -- if state is not exists against client the set the Texas as default state. . . 
	WHERE tt.TicketID_PK = @TicketId
	AND nisi.NisiCauseNumber IN (SELECT * FROM [dbo].[Sap_String_Param_forstring](@multiNisi))
	AND nisi.UnderlyingCauseNumber IN (SELECT * FROM [dbo].[Sap_String_Param_forstring](@multiUnderlyingCause))
	--AND CHARINDEX('NISI', ttv.casenumassignedbycourt) > 0 -- Only Print Letters for the NISI Cause Numbers. . . 
	
	-- Updating Court Date on Underlying violation Cause number basis. . . 
	UPDATE t
	SET t.CourtDateMain = v.CourtDateMain
	FROM #temp t INNER JOIN TrafficTickets.dbo.tblTicketsViolations v ON t.TicketID_PK = v.TicketID_PK
	AND v.casenumassignedbycourt IN (SELECT * FROM [dbo].[Sap_String_Param_forstring](@multiUnderlyingCause))
	
	SELECT DISTINCT * FROM #temp
	
END

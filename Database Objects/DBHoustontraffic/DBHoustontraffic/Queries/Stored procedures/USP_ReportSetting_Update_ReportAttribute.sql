﻿
/*  
* Created By		: Saeed Ahmed
* Task ID			: 8101  
* Created Date		: 09/27/2010  
* Business logic	: This procedure is used to update the already available record in the ReportAttribute table 
* Parameter List  
* @attributeid 		: This parameter contains the AttributeID
* @attributename    : This parameter contains the AttributeName
* @shortname        : This parameter contains the ShortName
* @description      : This parameter contains the Description
* @insertdate       : This parameter contains the insert date
* @insertby         : This parameter contains the inserted user id
* @lastupdatedate   : This parameter contains the last update date
* @lastupdateby     : This parameter contains the last update user id
* @attributetype    : This parameter contains attribute type
*/

CREATE PROCEDURE [dbo].[USP_ReportSetting_Update_ReportAttribute]
(
    @attributeid    INT,
    @attributename  VARCHAR(300),
    @shortname      VARCHAR(50),
    @description    VARCHAR(500),
    @lastupdateby   INT,
    @attributetype  VARCHAR(20)
)
AS
BEGIN
    IF EXISTS(
           SELECT AttributeID
           FROM   ReportAttribute
           WHERE  (
                      AttributeID <> @attributeid
                      AND LTRIM(RTRIM(AttributeName)) = LTRIM(RTRIM(@attributename))
                  )
       )
    BEGIN
        SELECT 'true' AS isExist
    END   
    ELSE
    BEGIN
        UPDATE ReportAttribute
        SET    AttributeName = @attributename,
               ShortName = @shortname,
               [Description] = @description,
               LastUpdateDate = GETDATE(),
               LastUpdateBy = @lastupdateby,
               AttributeType = @attributetype
        WHERE  (AttributeID = @attributeid)
        SELECT 'false' AS isExist
    END
END 
GO
GRANT EXECUTE ON [dbo].[USP_ReportSetting_Update_ReportAttribute] TO dbr_webuser
GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_GET_BatchTicketID_Rep]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_GET_BatchTicketID_Rep]
GO


CREATE  PROCEDURE USP_HTS_GET_BatchTicketID_Rep -- 3015      
@courtid  int                    
               
AS                    
                    
select distinct  t.ticketid_pk, t.firstName +' '+t.lastname as [Name], b.batchDate,(select top 1 tv.courtdatemain                
        from tblticketsviolations tv                 
        where tv.ticketid_pk=t.ticketid_pk )as courtdate,b.printdate,b.isprinted ,ttv.courtid          
from tblhtsbatchprintletter b                        
inner join                    
 tbltickets t                    
on  t.ticketID_pk =  b.ticketID_fK     and b.batchid_pk=(  select top 1 tbv.batchID_pk from tblhtsbatchprintletter tbv                                         
  where  tbv.ticketid_fk=b.ticketid_fk and tbv.LetterID_Fk = 6  and deleteflag<>1  )         
inner join tblticketsviolations ttv on ttv.ticketid_pk=b.ticketid_fk    
where b.LetterID_Fk = 6                     
and deleteflag<>1       
and ttv.courtid=@courtid        
order by b.batchDate  desc  
    
      
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO



GO
/****** Object:  StoredProcedure [dbo].[USP_HTP_Update_FollowUpdate]    Script Date: 05/15/2008 11:52:20 ******/

/*  
Created By : Noufil Khan  
Business Logic : This procedure Update FollowUpdate Coloumn in Tbltikcets
Parameter:
			@follow   : Follow Up Date set by User
			@ticketid : Ticket id of user		

*/
--[USP_HTP_Update_FollowUpdate]'05/16/2008',184167
Create Procedure [dbo].[USP_HTP_Update_FollowUpdate]
@follow datetime,
@ticketid int
as
update tbltickets 
set Criminalfollowupdate =@follow
where TicketID_PK = @ticketid

go
GRANT EXEC ON [dbo].[USP_HTP_Update_FollowUpdate] to dbr_webuser
go

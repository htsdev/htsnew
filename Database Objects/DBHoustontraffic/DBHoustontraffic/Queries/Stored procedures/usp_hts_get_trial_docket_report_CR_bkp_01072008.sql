SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_hts_get_trial_docket_report_CR_bkp_01072008]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_hts_get_trial_docket_report_CR_bkp_01072008]
GO



-- exec usp_hts_get_trial_docket_report_CR @courtloc='None', @courtdate='1/2/2008' , @ShowOwesDetail = 0
CREATE procedure [dbo].[usp_hts_get_trial_docket_report_CR_bkp_01072008] 
@courtloc varchar(10) = 'None', 
@courtdate varchar(12) = '01/01/1900', 
@page varchar(12) = 'TRIAL', 
@courtnumber varchar(6) = '-1', 
@datetype varchar(20) = '0', 
@singles int = 0 ,
@URL varchar(100) = 'ln.legalhouston.com',
@ShowOwesDetail bit = 1  
as 
 
Declare @NameDiscrepancy int 
set @NameDiscrepancy =19 
if @courtnumber = '' 
 begin 
 set @courtnumber ='-1' 
 end 
 
set nocount on 
 
SELECT dbo.tblTickets.TicketID_PK, dbo.tblTickets.Firstname, isnull(dbo.tblTickets.MiddleName,'')as MiddleName, dbo.tblTickets.Lastname, 
Ofirstname = 
case 
when left(dbo.tblcourtviolationstatus.Description,3) = 'ARR' then dbo.FN_GetCaseSequences(dbo.tblTickets.ticketid_pk) 
else 
isnull(dbo.tblOfficer.FirstName,'N/A') 
end 
, 
Olastname = case 
when left(dbo.tblcourtviolationstatus.Description,3) = 'ARR' then ' ' 
else 
 
isnull(dbo.tblOfficer.LastName,'N/A') 
end, 
 
 
dbo.tblViolations.ShortDesc, dbo.tblTickets.CourtDate, 
 dbo.tblTicketsViolations.courtdatemain as currentdateset, 
dbo.tblTicketsViolations.courtid as currentcourtloc, 
convert(int,dbo.tblTicketsViolations.courtnumbermain) as currentcourtnum, 
dbo.tblcourtviolationstatus.ShortDescription as description,activeflag, 
dbo.tblcourts.courtname, dbo.tblcourts.Address, 
trialcomments= 
 
isnull(case 
 
when dbo.tblTickets.pretrialcomments <> '' then '<font color=black><b>PRE:</b></font><font color=red>' + isnull(dbo.tblTickets.pretrialcomments,' ') + '</font>'
end,' ') + 
 
isnull(case 
 
when dbo.tblTickets.trialcomments <> '' then '<font color=black><b>TRI:</b></font><font color=red>' + isnull(dbo.tblTickets.trialcomments,' ') + '</font>'
end, ' ') + 
 
isnull(case when (select count(FlagID) from tblticketsflag where FlagID=@NameDiscrepancy and TicketID_PK =dbo.tblTickets.ticketid_pk) > 0 
then '<font color=black><b>Name Discrepancy</b></font>' else '' end,'') 
 
, 
bondflag,firmabbreviation,courttype, 
case accidentflag 
 when 1 then 1 
 else 0 
end as accflag, 
speeding = dbo.getviolationspeed_ver1(sequencenumber,refcasenumber,fineamount,violationcode), 
convert(varchar(2),month(dbo.tblTicketsViolations.courtdatemain)) + '/' + convert(varchar(2),day(dbo.tblTicketsViolations.courtdatemain)) + '/' + convert(varchar(4),year(dbo.tblTicketsViolations.courtdatemain)) as courtarrdate, 
 
 
datepart(hour,dbo.tblTicketsViolations.courtdatemain) as courttime,dbo.tblCaseDispositionstatus.description as ViolationStatusID, 
dbo.tblTicketsViolations.violationnumber_pk as violationnumber,address1, 
case 
when dbo.tblTicketsViolations.courtid in (3001, 3002,3003) then midnum 
when dbo.tblTicketsViolations.courtid not in (3001, 3002,3003) and (casenumassignedbycourt not like '' and not (casenumassignedbycourt is null)) then casenumassignedbycourt 
when dbo.tblTicketsViolations.courtid not in (3001, 3002,3003) and (casenumassignedbycourt like ' ' or casenumassignedbycourt like '' or casenumassignedbycourt is null) then RefCasenumber 
else ' ' end as midnum, 
 
violationtype, dbo.tblCourts.Address as courtaddress, 
dbo.tblcourtviolationstatus.categoryid as courtviolationstatusid 
,refcasenumber, 
(select top 1 isnull(tvv.OffenseLocation,'N/A')  from tblticketsviolations tvv where tvv.ticketid_pk = tblTicketsViolations.ticketid_pk ) as OffenseLocation 
into #temp 
FROM dbo.tblTickets INNER JOIN 
 dbo.tblTicketsViolations ON dbo.tblTickets.TicketID_PK = dbo.tblTicketsViolations.TicketID_PK INNER JOIN 
 dbo.tblCaseDispositionstatus ON dbo.tblTicketsViolations.violationstatusid = dbo.tblCaseDispositionstatus.casestatusid_pk INNER JOIN 
 dbo.tblfirm ON dbo.tblTickets.firmid = dbo.tblfirm.firmid INNER JOIN 
 dbo.tblViolations ON dbo.tblTicketsViolations.ViolationNumber_PK = dbo.tblViolations.ViolationNumber_PK INNER JOIN 
 dbo.tblcourtviolationstatus ON dbo.tblTicketsviolations.courtviolationstatusidmain = dbo.tblcourtviolationstatus.courtviolationstatusID INNER JOIN 
 dbo.tblCourts ON dbo.tblTicketsViolations.courtid = dbo.tblCourts.Courtid LEFT OUTER JOIN 
 dbo.tblOfficer ON dbo.tblTickets.OfficerNumber = dbo.tblOfficer.OfficerNumber_PK 
WHERE (dbo.tblViolations.Violationtype not IN (1) or dbo.tblViolations.violationnumber_pk in (11,12) ) AND (dbo.tblcourtviolationstatus.categoryid IN (select distinct categoryid from tblcourtviolationstatus where violationcategory in (0,2) 

and categoryid not in (7,50) and courtviolationstatusid not in (104, 201) )) 
AND (dbo.tblTickets.Activeflag = 1) 
and dbo.tblTicketsViolations.courtid <> 0 
and datediff(DAY,dbo.tblTicketsViolations.courtdatemain,@courtdate) = 0 
 --ISNULL(SUM(DISTINCT T.totalfeecharged) - SUM(TP.ChargeAmount), 0) AS Owes 
 

select DISTINCT ticketid,ISNULL( T.totalfeecharged - SUM( A.ChargeAmount), 0) as dueamount , t.continuanceamount as continuance , continuanceStatus 
 ,T.ContinuanceDate 
 --totalfeecharged-sum(ChargeAmount) as dueamount 
 into #temp2 
 from tblticketspayment A,tbltickets T where 
 T.ticketid_pk = A.ticketid 
 and activeflag = 1 
 and paymentvoid=0 
 and paymenttype not in (99,100) 
 group by ticketid,totalfeecharged, t.continuanceamount ,continuanceStatus , T.ContinuanceDate 
 
 declare @sqlquery varchar(8000) 
 declare @sqlquery1 varchar(8000) 
 
 set @sqlquery = ' ' 
 
 select T.*,T2.continuance ,T2.continuanceStatus, T2.ContinuanceDate, 
 dueamount into #temp3 
 FROM #temp T 
 left outer join #temp2 T2 on T.ticketid_pk = T2.ticketid 
 
 
 SELECT #temp3.*,count(*) as clientcount into #temp4 
 FROM #temp3 RIGHT OUTER JOIN 
 tblTickets T ON #temp3.Firstname = T.Firstname AND #temp3.Lastname = T.Lastname AND 
 (isnull(#temp3.Address1,'N/A') = isnull(T.Address1,'N/A') or #temp3.midnum = T.midnum) 
 where t.activeflag = 1 
 GROUP BY 
 #temp3.TicketID_PK, #temp3.Firstname, #temp3.MiddleName, #temp3.Lastname, #temp3.Ofirstname, 
 #temp3.Olastname, #temp3.ShortDesc, #temp3.CourtDate, #temp3.currentdateset, #temp3.currentcourtloc, 
 #temp3.currentcourtnum, #temp3.Description, #temp3.activeflag, #temp3.courtname, #temp3.Address, 
 #temp3.trialcomments, #temp3.bondflag, #temp3.firmabbreviation, #temp3.courttype, #temp3.accflag, 
 #temp3.speeding, #temp3.continuance, #temp3.dueamount, #temp3.courtarrdate, #temp3.courttime, #temp3.ViolationStatusID, 
 #temp3.violationnumber, #temp3.address1, #temp3.midnum, #temp3.violationtype, #temp3.courtaddress, 
 #temp3.courtviolationstatusid, #temp3.refcasenumber , #temp3.continuanceStatus ,#temp3.continuanceDate,#temp3.OffenseLocation 
 
 -- select * from #temp4 
 
 set @sqlquery = 'select P.TicketID_PK,P.Firstname,P.MiddleName,P.Lastname,P.Ofirstname,P.Olastname, 
 replace(P.ShortDesc,''None'',''Other'') as shortdesc,P.CourtDate,P.currentdateset, 
 P.currentcourtloc,P.currentcourtnum,P.Description,P.activeflag,P.courtname, 
 P.Address,P.trialcomments,P.bondflag,P.firmabbreviation,P.courttype,P.accflag,P.speeding, 
 P.continuance,p.continuancestatus,p.ContinuanceDate, P.dueamount,P.courtarrdate,P.courttime,P.ViolationStatusID, 
 P.violationnumber,P.clientcount,T.coveringfirmid,F.FirmAbbreviation as coveringfirm, 
 P.violationtype,courtaddress,cdlflag,courtviolationstatusid, 
 convert(varchar(20),P.TicketID_PK) + isnull(convert(varchar(4),courtviolationstatusid),0) as ticketstatusid, pretrialstatus = case 
 when pretrialstatus = 1 and T.currentcourtloc between 3001 and 3022 and left(Description,3) <> ''JUR'' then ''RFT'' 
 when pretrialstatus =2 and left(Description,3) <> ''JUR'' then ''WBT'' 
 when pretrialstatus =4 and left(Description,3) <> ''JUR'' then ''DA'' 
 when pretrialstatus =5 and left(Description,3) <> ''JUR'' then ''DSC'' 
 else '' '' end, AccidentFlag, p.midnum, p.refcasenumber, t.dob ,convert(int,( dbo.Fn_CheckCaseDiscrepency(p.TicketID_pk , '''+ @courtdate+'''))) as Discrepency, p.OffenseLocation into #temp5 
 from #temp4 P,dbo.tbltickets T,dbo.tblfirm F where P.ticketid_pk = T.ticketid_pk and case when T.coveringfirmid = 0 then 3000 else T.coveringfirmid end = F.firmid and P.ticketid_pk is not null ' 
 if @courtloc is not null and @courtloc <> 'None' 
 begin 
 if @courtloc = 'JP' or @courtloc = 'OU' 
 if @courtloc = 'JP' 
 begin 
 set @sqlquery = @sqlquery + ' and P.currentcourtloc not in (3001,3002,3003)' 
 end 
 else 
 begin 
 set @sqlquery = @sqlquery + ' and P.currentcourtloc in (3001,3002,3003)' 
 end 
 ELSE 
 
 begin 
 set @sqlquery = @sqlquery + ' and P.currentcourtloc = ' + @courtloc 
 end 
 end 
 if @courtdate is not null and @courtdate <> '01/01/1900' 
 begin 
 set @sqlquery = @sqlquery + ' and datediff(day,P.currentdateset,''' + @courtdate + ''') = 0' 
 end 
 if @courtnumber <> '-1' 
 begin 
 set @sqlquery = @sqlquery + ' and P.currentcourtnum = ' + @courtnumber 
 end 
 if @datetype <> '0' 
 begin 
 set @sqlquery = @sqlquery + ' and courtviolationstatusid in (' + @datetype + ')' 
 end 
 set @sqlquery = @sqlquery + ' and ((P.ShortDesc not in (''CDL'',''ACC'',''PROB'')) or (P.ShortDesc is null)) order by P.courtarrdate, 
 P.currentcourtloc,P.Address,convert(int,P.currentcourtnum) asc,P.courttime,P.lastname,P.firstname,P.middlename 
 --select * from #temp5 where ticketid_pk = 102979 
 ' 
 
 set @sqlquery = @sqlquery + 'select * into #violations from tblviolations where description like ''%speeding%'' and violationtype <> 0 ' 
 
 set @sqlquery = @sqlquery + 'select T.*, isnull(V.violationnumber_pk,0) as speedingvnum 
 into #temp6 from #temp5 T 
 left outer join #violations V on T.violationnumber = V.violationnumber_pk 
 order by case T.courtviolationstatusid when 9 then 9 
 when 0 then 9 else 1 end 
 ,T.courtarrdate,T.currentcourtloc,T.Address,convert(int,T.currentcourtnum) asc, 
 T.courttime,T.lastname,T.firstname,T.middlename,T.courtviolationstatusid 
 drop table #temp5 
 declare @ticketID int 
 declare @tbl1 table ( ticketid int, shortdesc1 varchar(1000)) 
 declare @strVaue varchar(200) 
 declare Crtemp cursor for 
 select distinct ticketID_pk from #temp6 
 --tahir 
 open Crtemp 
 fetch next from Crtemp 
 into @ticketid 
 WHILE @@FETCH_STATUS = 0 
 BEGIN 
 set @strVaue = ''&nbsp;&nbsp;'' 
 select @strVaue = @strVaue+ case isnull(shortdesc,'''') when ''spd'' then ''spd,'' else isnull(shortdesc,'''')+'','' end from #temp6 where ticketID_pk = @ticketid 
 select distinct @strVaue = case @strVaue when ''spd,'' then ''spd,'' else @strVaue end +case abs(speeding)when '' '' then '' '' else ''(''+convert(varchar(20),abs(speeding))+ '')'' end from #temp6 where ticketID_pk = @ticketid 
 if len(@strvaue) > 50 
 set @strvaue = stuff(@strvaue,charindex('','',@strvaue,50)+1,0,'' '') 
 insert into @tbl1 values(@ticketid, @strVaue) 
 fetch next from Crtemp 
 into @ticketid 
 END 
 close Crtemp 
 DEALLOCATE Crtemp 
 SELECT TicketId_Pk as CaseNumber, 
 '':''+lastName Client_LastName , t2.shortdesc1, TicketID_PK , 
 Firstname+'',''+MiddleName as FMname, 
 case Olastname+'',''+Ofirstname when ''N/A,N/A'' then '''' else Olastname+'',''+Ofirstname end OLFNAME , 
 currentcourtloc, 
 currentcourtnum, Description, courtname+''-''+Address+''-''+ 
 (convert(varchar(10),DatePart(m,currentdateset))+''/''+convert(varchar(10),DatePart(d,currentdateset))+''/''+convert(varchar(10),DatePart(yyyy,currentdateset))) as CourtName_Address_DateSet, 
 bondflag,convert(varchar(10),cast (dueamount as decimal)) dueamount ,firmabbreviation, courttime, 
 case clientcount when 1 then '''' else ''*''+convert(varchar(5),clientcount) end ClientCount, 
 -- Added by Ozair on 12/18/2007 Ttask 2383
 case when ( select count(TicketID_PK) from tblticketsflag tf where tf.ticketid_pk = t1.TicketId_Pk and flagid = 8 ) > 0 then firmabbreviation+''-CL''+'' '' else '''' end + 
 --additionend
 case clientcount when 1 then '''' else ''*''+convert(varchar(5),clientcount) end+ ''''+ 
 
 ltrim( case AccidentFlag 
 when 0 then '''' else ''ACC'' end +'' ''+ 
 case cdlflag 
 when 0 then '''' else ''CDL'' end +'' ''+ 
 
 case when ( select count(TicketID_PK) from tblticketsflag tf where tf.ticketid_pk = t1.TicketId_Pk and flagid = 1 ) > 0 then ''&nbsp;P'' else '''' end + 
 case when ( select count(TicketID_PK) from tblticketsflag tf where tf.ticketid_pk = t1.TicketId_Pk and flagid = 2 ) > 0 then ''&nbsp;Attn'' else '''' end + 
 case when ( select count(TicketID_PK) from tblticketsflag tf where tf.ticketid_pk = t1.TicketId_Pk and flagid = 9 ) > 0 then ''&nbsp;Cont'' else '''' end + 
 case when ( select count(TicketID_PK) from tblticketsflag tf where tf.ticketid_pk = t1.TicketId_Pk and flagid = 15 ) > 0 then ''&nbsp;NOS'' else '''' end + 
 case when ( select count(TicketID_PK) from tblticketsflag tf where tf.ticketid_pk = t1.TicketId_Pk and flagid = 21 ) > 0 then ''&nbsp;W'' else '''' end + 
 case when ( select count(TicketID_PK) from tblticketsflag tf where tf.ticketid_pk = t1.TicketId_Pk and flagid = 22 ) > 0 then ''&nbsp;U'' else '''' end + 
 case when ( select count(TicketID_PK) from tblticketsflag tf where tf.ticketid_pk = t1.TicketId_Pk and flagid = 20 ) > 0 then ''&nbsp;<font color=red>PR</font>'' else '''' end + 
 case when ( select count(TicketID) from tblScanBook ts where ts.ticketid = t1.TicketId_Pk and DocTypeID = 14 ) > 0 then ''&nbsp;POA'' else '''' end + 
 case pretrialstatus 
 when ''RFT'' then ''(RFT)'' else pretrialstatus end ) + 
 case 
 when datediff(year,isnull(dob, ''1/1/1900''),currentdateset) <= 25 then ''<25'' 
 else '' '' 
 end 
 as ClientCount_PreTrialStatus_CDLFLAG, coveringfirm,dbo.formattime(t1.currentdateset) as currentdateset,'' '' as space, 
 (trialcomments +'' ''+''<font color=black>''+ case when Convert(varchar,ContinuanceDate,101) = '''+ convert(varchar,convert(datetime,@courtdate),101) + ''' then 
 (select shortdescription from tblcontinousstatus where statusid = isnull(continuancestatus,1)) else '''' end 
 + ''</font>'') as trialcomments 
 , midnum, refcasenumber , Discrepency, OffenseLocation 
 into #temp7 
 FROM #temp6 t1, @tbl1 t2 
 where t2.ticketid = t1.TicketID_PK 
 order by currentcourtnum 
 
 SELECT distinct casenumber, count(ticketid_pk) as violationCount ,Client_LastName, isnull(shortdesc1,'''') shortdesc1, TicketID_PK , 
 FMname,OLFNAME , currentcourtloc,case currentcourtnum when '''' then '''' else currentcourtnum end currentcourtnum, Description, CourtName_Address_DateSet, case bondflag when 0 then '' '' else ''B'' + case dueamount when 0 then ''''else 
'

if @ShowOwesDetail = 1 
begin
set @sqlquery = @sqlquery + '''$''+ dueamount end '
end
Else
Begin
set @sqlquery = @sqlquery + '''$'' end '

End

 set @sqlquery = @sqlquery +' end BondFlag, firmabbreviation, courttime, 
 min(clientcount) as clientcount,ClientCount_PreTrialStatus_CDLFLAG as ClientCount_PreTrialStatus_CDLFLAG ,coveringfirm as CoveringFirm,currentdateset,space,trialcomments,midnum ,refcasenumber,Discrepency, OffenseLocation 
 into #temp8 FROM #temp7 
 
 group by currentcourtloc, currentcourtnum, CourtName_Address_DateSet, 
 TicketID_PK, shortdesc1, FMname, Client_LastName, OLFNAME, midnum, refcasenumber, 
 Description, bondflag, firmabbreviation, 
 courttime, ClientCount_PreTrialStatus_CDLFLAG,coveringfirm, currentdateset, casenumber,space, 
 trialcomments, dueamount,Discrepency, OffenseLocation order by currentcourtloc 
 
 ' 
 --print @sqlquery 
 
 set @sqlquery1=''; 
 set @sqlquery1=@sqlquery1+' 
 SELECT distinct casenumber, 
 sum(violationCount) as violationCount, min(Client_LastName)Client_LastName,min(isnull(shortdesc1,'''')) shortdesc1, min(TicketID_PK)TicketID_PK , 
 min(FMname) FMname,min(OLFNAME) OLFNAME , currentcourtloc, 
 currentcourtnum, Description, CourtName_Address_DateSet, min(BondFlag) BondFlag, 
 min(firmabbreviation) firmabrevation, min(courttime) courttime, 
 min(clientcount) as clientcount,min(ClientCount_PreTrialStatus_CDLFLAG)ClientCount_PreTrialStatus_CDLFLAG, min(CoveringFirm) as CoveringFirm,min(currentdateset) currentdateset,min(space) as Space, 
 min(trialcomments) as trialcomments,min(midnum)as midnum, min(refcasenumber) as refcasenumber , Discrepency, OffenseLocation into #temp9 FROM #temp8 
 group by casenumber,currentcourtloc,currentcourtnum,CourtName_Address_DateSet,Description , Discrepency, OffenseLocation ;
 
 SELECT distinct casenumber, violationCount ,Client_LastName,shortdesc1,TicketID_PK , 
 left(FMname,13) as FMname ,left(OLFNAME,13) as OLFNAME , currentcourtloc,currentcourtnum, left(Description, 3) as Description, CourtName_Address_DateSet, BondFlag, 
 firmabrevation, courttime,clientcount,ClientCount_PreTrialStatus_CDLFLAG ,CoveringFirm ,currentdateset,Space, 
 trialcomments, dbo.USP_HTS_Get_offenceDates(ticketid_pk) as offenceDate, isnull(midnum, right(refcasenumber,7)) as midnum ,Discrepency, OffenseLocation ,
 0 as GroupID 
 into #temp10
 from #temp9 where Description Not In(''BOND'',''COM'',''DSCD'', ''DAR'',''DPX'',''CLO'',''B/W'',''WAIT''' 
 
 if @singles = 1 --this will only add if trial docket prints singles Document. 
 begin 
 set @sqlquery1=@sqlquery1+' ,''SS'',''DEF'' ' 
 end 
 
 set @sqlquery1=@sqlquery1+ ' ) and Description in (''ARR'',''PRE'',''JUR'',''JUR2'',''JUD'',''BF'',''A/W'',''TRI'') order by currentcourtloc, currentcourtnum, courttime , Client_LastName ; 
 insert into #temp10 
 SELECT distinct casenumber, violationCount ,Client_LastName,shortdesc1,TicketID_PK , 
 left(FMname,13) as FMname ,left(OLFNAME,13) as OLFNAME , currentcourtloc,currentcourtnum, left(Description, 3) as Description, CourtName_Address_DateSet, BondFlag, 
 firmabrevation, courttime,clientcount,ClientCount_PreTrialStatus_CDLFLAG ,CoveringFirm, currentdateset,Space, 
 trialcomments, dbo.USP_HTS_Get_offenceDates(ticketid_pk) as offenceDate, isnull(midnum, right(refcasenumber,7)) as midnum ,Discrepency, OffenseLocation ,
 1 as GroupID 
 from #temp9 where Description Not In(''BOND'',''COM'',''JUR2'',''DSCD'',''DAR'',''DPX'',''CLO'',''B/W'',''WAIT'' ' 

 if @singles = 1 --this will only add if trial docket prints singles Document. 
 begin 
 set @sqlquery1=@sqlquery1+' ,''SS'',''DEF'' ' 
 end 
 
 set @sqlquery1=@sqlquery1+ ' ) and Description not in (''ARR'',''PRE'',''JUR'',''JUD'',''BF'',''A/W'',''TRI'') order by currentcourtloc, currentcourtnum, courttime , Client_LastName ; 
 select *, ''' + @URL + ''' as URL from #temp10 order by GroupID, currentcourtloc, currentcourtnum, courttime , Client_LastName ; 
 select distinct currentcourtloc from #temp9 
 where Description Not In(''BOND'',''COM'',''JUR2'',''DSCD'',''DAR'',''DPX'',''CLO'',''B/W'',''WAIT'' ) ; 
 
 select distinct currentcourtloc, currentcourtnum from #temp9 
 where Description Not In(''BOND'',''COM'',''JUR2'',''DSCD'',''DAR'',''DPX'',''CLO'',''B/W'',''WAIT'' ); 
 
 drop table #temp6;drop table #temp7;drop table #temp8;drop table #temp9;drop table #temp10;' 
 
--select len(@sqlquery1) 
 
 print @sqlquery+' '+@sqlquery1 
 exec (@sqlquery+ ' '+@sqlquery1) 


 drop table #temp2 
 drop table #temp3 
 drop table #temp4 
 
--end 
drop table #temp 




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


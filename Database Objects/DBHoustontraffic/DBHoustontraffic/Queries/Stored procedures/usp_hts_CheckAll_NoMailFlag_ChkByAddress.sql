

-- =============================================
-- Author:		Abbas Qamar 
-- Create date: 29/10/2010
-- Business Logic : The stored procedure is used by HTP /Activies /Donot Mail Update application.
--				It is used to search record by name & address. User can mark the records appearing
--				in the results to remove the person from our mailing lists 
--
-- List Of Parameters:
--	@Address:		mailing address of the person
--	@Zip:			zip code associated with the mailing address
--	@City:			city associated with the mailing address
--	@stateid_fk:	state id related to the mailing address

-- List of output columns:

--	address1:		mailing address of the person 
--	city:			city associated with the mailing address
--	state:			name of the state associated with the mailing address
--	zipcode:		zipcode associated with the mailing address
--	nomailflag		flag if already marked as do not mail flag

--
--
-- =============================================
-- usp_hts_CheckAll_NoMailFlag_ChkByAddress '3522 GOLDEN TEE LN','77459-4008','MISSOURI CITY',45
 
CREATE procedure [dbo].[usp_hts_CheckAll_NoMailFlag_ChkByAddress]
                
@Address varchar(50) ,                   
@Zip varchar(12)   ,                       
@City varchar(50),                
@stateid_fk int                 
                
as                

declare @DataSet table(Sno int identity, dbid int,DoNotMailUpdateDate VARCHAR(10),recordid int, nomailflag bit)                              

insert into @dataset(dbid,DoNotMailUpdateDate, recordid, nomailflag)              
select  distinct  1,ISNULL(CONVERT(VARCHAR(10),t.DoNotMailUpdateDate,101),'Not Set'), t.recordid, isnull(t.donotmailflag,0) 
from tblticketsarchive t inner join tblticketsviolationsarchive v 
on v.recordid = t.recordid 
inner join tblstate s on s.stateid = t.stateid_fk 
where                        
     t.address1 like @Address                        
   AND                        
  t.ZipCode Like(@Zip+'%')                        
  AND                      
  t.City = @City                      
  AND                    
 t.StateID_FK = @stateid_fk                 
                
-- searching matching records in DallasTrafficTickets database....

insert into @dataset(dbid,DoNotMailUpdateDate, recordid, nomailflag)              
select distinct 2,ISNULL(CONVERT(VARCHAR(10),t.DoNotMailUpdateDate,101),'Not Set'), t.recordid, isnull(t.donotmailflag,0)        
 from dallastraffictickets.dbo.tblticketsarchive   t 
inner join Dallastraffictickets.dbo.tblticketsviolationsarchive v 
on v.recordid = t.recordid
inner join dallastraffictickets.dbo.tblstate s
on s.stateid = t.stateid_fk
                        
where                        
                        
     t.address1 like @Address                        
   AND                        
  t.ZipCode Like(@Zip+'%')                        
  AND                      
  t.City = @City                      
  AND                    
 t.StateID_FK = @stateid_fk                 
                
                
insert into @dataset(dbid,DoNotMailUpdateDate, recordid,nomailflag)              
select distinct 5,ISNULL(CONVERT(VARCHAR(10),t.DoNotMailUpdateDate,101),'Not Set'), t.childcustodypartyid, isnull(t.donotmailflag,0)    
 from civil.dbo.childcustodyparties  t 
inner join civil.dbo.childcustody v 
on v.childcustodyid = t.childcustodyid
inner join civil.dbo.tblstate s
on s.stateid = t.stateid     
                        
where                        
     t.address = @Address                        
   AND                        
  t.ZipCode Like(@Zip+'%')                        
  AND                      
  t.City = @City                      
  AND                    
 t.StateID = @stateid_fk  
                
                
                
-- out put the results...
select * from @dataset order by dbid


﻿/****** 
Alter by	: Syed Muhammad Ozair
Altered Date	: 11/14/2008
TasK		: 5138

Business Logic : This procedure return the data on which we display/implements check on repoprt buttons under billing page

List of Parameters:	
	@TicketID			: id against which records are fetched


Column Return : 
		CourtID,       
		CourtDateMain,       
		CategoryId,       
		bondflag,    
		ContinuanceAmount,       
		email,
		caseTypeID,
		violationnumber_pk,
		LOR,
		ViolationCategoryID
******/
ALTER procedure [dbo].[USP_HTS_Get_PaymentInfo_ButtonStatus] 
 (      
 @TicketID  INT
 )      
         
as         
    SET NOCOUNT ON ;        
	SELECT 
	--Yasir Kamal 6073 06/24/2009 Null Courtid Bug fixed.
			ISNULL(tv.CourtID,0) AS CourtID,       
			isnull(tv.CourtDateMain,'01/01/1900') as CourtDateMain,       
			isnull( cvs.CategoryID,0) as CategoryId, 
			ISNULL(cvs.CourtViolationStatusID,0) AS ViolationStatus, --Sabir Khan 8862 02/23/2011 violation status id added.      
			--t.BondFlag,       
			isnull(tv.underlyingbondflag,0) as bondflag,    
			ISNULL(t.ContinuanceAmount, 0) AS ContinuanceAmount,       
			ISNULL(t.Email, '') AS email,
			-- Abid Ali 5138 11/17/2008
			t.caseTypeID,
			tv.violationnumber_pk,
			-- Abid Ali 5359 12/30/2008 for LOR Method
			tc.LOR,
			-- Abid Ali 5505 02/06/2009 for Violation category  
			v.CategoryID AS ViolationCategoryID
			
	FROM	
							tblTickets t WITH(NOLOCK)       
			INNER JOIN		tblTicketsViolations tv WITH(NOLOCK)       
					ON		t.TicketID_PK = tv.TicketID_PK       
			LEFT OUTER JOIN tblCourtViolationStatus cvs WITH(NOLOCK)       
					ON		tv.CourtViolationStatusIDmain = cvs.CourtViolationStatusID     
			-- Abid Ali 5359 12/30/2008 for LOR Method 
			LEFT OUTER JOIN tblCourts tc WITH(NOLOCK)
					ON		tv.CourtID = tc.Courtid 
			-- Abid Ali 5505 02/06/2009 for Violation category 
			LEFT OUTER JOIN tblViolations v WITH(NOLOCK)
					ON		tv.ViolationNumber_PK = v.ViolationNumber_PK
	where		tv.ticketid_pk=@ticketid --Fahad 5908 05/16/2009 Remove Filter for Disposed Case
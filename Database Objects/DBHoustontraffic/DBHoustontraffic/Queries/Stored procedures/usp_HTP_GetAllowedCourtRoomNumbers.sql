/****** 
Created by:		Rab Nawaz Khan
Task Id:		8997
Business Logic:	This procedure is used by HTP application to get status to display the court room numbers text box for all out side traffic courts and 
				display the court room numbers text box in application if the particular traffic court setting allow to display the court room numbers
				
List of Parameters:
	@courtId: select the court on the court ID base
				
********/
CREATE PROCEDURE usp_HTP_GetAllowedCourtRoomNumbers 
(
	@courtId AS INT	
)
AS
IF EXISTS (SELECT courtid 
           FROM tblCourts t
           WHERE Courtid = @courtId 
           -- Either a Criminal court 
           AND (IsCriminalCourt = 1 
           -- Either inside court of houston 
				OR courtcategorynum = 1 
				-- Either civil or family law case type
				OR t.CaseTypeID IN (3,4))
		   )
BEGIN
	SELECT CONVERT(BIT, 1) AS AllowCourtNumbers FROM tblCourts tc
	WHERE tc.Courtid = @courtId
END
ELSE
	BEGIN
		SELECT CONVERT(BIT, ISNULL(tc.AllowCourtNumbers, 0)) AS AllowCourtNumbers FROM tblCourts tc
		WHERE tc.Courtid = @courtId
	END

GO
GRANT EXECUTE ON [dbo].[usp_HTP_GetAllowedCourtRoomNumbers] TO dbr_webuser
GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_HTS_Get_All_tblChoice]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_HTS_Get_All_tblChoice]
GO

          create PROCEDURE [dbo].[usp_HTS_Get_All_tblChoice]      
      
 AS      
Select * from       
tblChoice      
Order By ChoiceID Desc      

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


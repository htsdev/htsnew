SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_Get_TrialLetterNotPrintedReport]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_Get_TrialLetterNotPrintedReport]
GO

CREATE procedure USP_Get_TrialLetterNotPrintedReport
@DiffDate int = 14
as
select refcasenumber as TicketNumber,casenumassignedbycourt as CauseNumber,courtdatemain as CourtDate
from tbltickets T, tblticketsViolations V
where T.ticketid_pk = V.ticketid_pk
and datediff(day,courtdatemain,getdate()) <=@DiffDate
and activeflag = 1
and courtviolationstatusidmain in(
select courtviolationstatusid from tblcourtviolationstatus where categoryid in (
select categoryid from tbldatetype where typeid in (3,4,5)))
and T.ticketid_pk not in (
select ticketid from tblticketsnotes where subject like '%Tr%letter%printed%')
and courtid in (3001,3002,3003)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


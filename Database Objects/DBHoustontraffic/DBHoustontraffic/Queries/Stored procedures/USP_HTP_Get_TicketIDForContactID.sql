﻿/************************************************************
 * Code formatted by SoftTree SQL Assistant © v4.0.34
 * Time: 4/20/2009 11:20:29 AM
 ************************************************************/

/****  
* Created BY : Nasir  
* Business Logic: select top 1 ticket of given Contact ID  
* Parameter:@ContactID  
* Selection:TicketID  
* Task ID: Nasir 5771 04/17/2009
****/  
--USP_HTP_Get_TicketIDForContactID 80175  
  
  
  
CREATE PROCEDURE dbo.USP_HTP_Get_TicketIDForContactID
	@ContactID INT
AS
	DECLARE @TicketID INT  
	
	SET @TicketID = (
	        SELECT TOP 1 TicketID_PK
	        FROM   tbltickets
	        WHERE  ContactID_FK = @ContactID
	    )  
	
	IF @TicketID IS NULL
	    SELECT '0'
	ELSE
	    SELECT @TicketID
GO

GRANT EXECUTE ON dbo.USP_HTP_Get_TicketIDForContactID TO dbr_webuser

GO
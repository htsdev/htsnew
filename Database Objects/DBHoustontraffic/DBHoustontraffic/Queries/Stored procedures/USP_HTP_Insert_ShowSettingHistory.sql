﻿/****** 
Create by		: Muhammad Nasir Mamda
Created Date	: 12/14/2009
TasK			: 6968

Business Logic : 
			This procedure maintain logs changes occurs in report.
			
List of Parameters:	
			@ReportID	report identification
			@Note		notes of change	occurs in report
			@EmployeeID	Employee identification
					
******/

Create PROCEDURE dbo.USP_HTP_Insert_ShowSettingHistory
@ReportID INT,
@Note VARCHAR(MAX),
@EmployeeID INT
AS

INSERT INTO ShowSettingHistory
(
	ReportID,
	Note,
	RecDate,
	UpdatedByEmployeeID
	
)
VALUES
(

	@ReportID/* ReportID	*/,
	@Note/* Note	*/,
	GETDATE()/* RecDate	*/,
	@EmployeeID/* EmployeeID	*/
) 

GO

GRANT EXECUTE ON dbo.USP_HTP_Insert_ShowSettingHistory TO dbr_webuser

 
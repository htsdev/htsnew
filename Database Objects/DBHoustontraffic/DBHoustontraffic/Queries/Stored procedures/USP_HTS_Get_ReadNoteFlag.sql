
ALTER procedure [dbo].[USP_HTS_Get_ReadNoteFlag]       
    
@ticketid as int     
    
as     
SET NOCOUNT ON;    
    
select count(*) as checkreadnotes   from tblticketsflag WITH(NOLOCK) where flagid = 31 and ticketid_pk = @ticketid   
  
select isnull(readcomments,'NA') as ReadComments from tbltickets WITH(NOLOCK) where ticketid_pk = @ticketid

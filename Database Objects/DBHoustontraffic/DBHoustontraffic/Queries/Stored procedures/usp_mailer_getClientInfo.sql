/*
*  Business Logic:	The procedure is used search record for Houston and dallas client that we have send letters..
				
List of Parameters:
@ticketNumber : Ticket Number.
@FirstName First NAme of Client..
@LastName Last Name.
@Branch  : 1 for houston and 2 for Dallas..
List of Columns:	
	Ticket ID : ticket id for Client
	Mailer ID : Mailer Id for Letter that we send to that Client.
	First Name: First Name of Client.
	Last Name : Last Name of Client.
	Recdate	: Mailer Date.
	Project : 1 for hoston and 2 for Dallas 
	Causenumber: Case Number of Client.
	Hire DAte: Hire Date for Client,
	

******/

--usp_mailer_getClientInfo '','','ADAME',2	
-- branch 1	for Houston, and 2 for Dallas
-- dbo.usp_mailer_getClientInfo '077782896','','',1
CREATE PROCEDURE dbo.usp_mailer_getClientInfo
@ticketNumber VARCHAR(50),
@FirstName VARCHAR(MAX),
@LastName VARCHAR(MAX),
@Branch INT=2
AS
DECLARE @Query VARCHAR(MAX)

IF(@Branch=1)
BEGIN
--Abbas Shahid Khwaja 4900 07/07/2011 Include RecordId and isactive condition 
  	SET @Query=' SELECT  DISTINCT tn.noteid, tt.firstname,tt.lastname,  tv.TicketNumber_PK AS ticketNumber,
tc.CourtCategoryName + '' - '' + l.lettername AS MailerType,
convert(varchar,tn.ListDate,101) as UploadDate ,convert(varchar,tn.RecordLoadDate,101) as MailDate,tt.recordid,'+CONVERT(VARCHAR,@Branch)+ '  as BranchId  
into #temp
FROM tblTicketsArchive  tt 
INNER JOIN  tblTicketsViolationsArchive tv ON tv.RecordID = tt.RecordID        
INNER JOIN tblLetterNotes tn ON tn.RecordID = tv.RecordId    
INNER JOIN tblletter l ON l.LetterID_PK = tn.LetterType and isnull(l.isactive,0) = 1  
 INNER JOIN tblCourtCategories tc ON tc.CourtCategorynum = l.courtcategory '
 

END
ELSE
	--Abbas Shahid Khwaja 4900 07/07/2011 Include RecordId and isactive condition 
	BEGIN
	SET @Query='SELECT  DISTINCT tn.noteid, tt.firstname,tt.lastname,  tv.TicketNumber_PK AS ticketNumber,
tc.CourtCategoryName + '' - '' + l.lettername AS MailerType,
 convert(varchar,tn.ListDate,101) as UploadDate ,convert(varchar,tn.RecordLoadDate,101) as MailDate,tt.recordid,'+CONVERT(VARCHAR,@Branch)+ ' as BranchId  
into #temp
FROM DallasTrafficTickets.dbo.tblTicketsArchive  tt 
INNER JOIN  DallasTrafficTickets.dbo.tblTicketsViolationsArchive tv ON tv.RecordID = tt.RecordID       
INNER JOIN tblLetterNotes tn ON tn.RecordID = tv.RecordId    
INNER JOIN tblletter l ON l.LetterID_PK = tn.LetterType  and isnull(l.isactive,0) = 1   
 INNER JOIN tblCourtCategories tc ON tc.CourtCategorynum = l.courtcategory '
	END
	
IF(@FirstName<>'' AND @LastName<>'' AND @ticketNumber<>'')
BEGIN
	SET @Query=@Query+' WHERE tt.FirstName='''+@FirstName+''' AND tt.LastName='''+@LastName+''' AND tv.TicketNumber_PK='''+@ticketNumber
END
ELSE IF(@ticketNumber<>'' AND @FirstName<>'')
BEGIN
SET @Query=@Query+' WHERE tv.TicketNumber_PK='''+@ticketNumber+''' AND tt.FirstName='''+@FirstName
END 
ELSE IF(@ticketNumber<>'' AND @LastName<>'')
BEGIN
	SET @Query=@Query+'WHERE tv.TicketNumber_PK='''+@ticketNumber+''' AND tt.LastName='''+@LastName
END
ELSE IF(@FirstName<>'' AND @LastName<>'')
BEGIN
		SET @Query=@Query+'WHERE tt.FirstName='''+@FirstName+''' AND tt.LastName='''+@LastName
END
ELSE IF(@ticketNumber<>'') 
	BEGIN
		SET @Query=@Query+'WHERE tv.TicketNumber_PK='''+@ticketNumber
	END
ELSE IF(@FirstName<>'')
BEGIN
		SET @Query=@Query+'WHERE  tt.FirstName ='''+@FirstName
END
ELSE IF(@LastName<>'')
BEGIN
	SET @Query=@Query+'WHERE  tt.LastName ='''+@LastName
END

SET @Query=@Query +'''ORDER BY convert(varchar,tn.RecordLoadDate,101) DESC'


SET @Query=@Query+' ALTER TABLE #temp ADD TicketID_PK INT DEFAULT NULL'
IF(@Branch=1)
--Abbas Shahid Khwaja 4900 07/07/2011 change the condition now it joins with the tblticketsviolations instead of tbltickets on recordid
BEGIN
SET @Query=@Query+' UPDATE #temp
SET    TicketID_PK = tv.TicketID_PK
FROM   #temp t
       LEFT OUTER JOIN tblticketsviolations tv
            ON  tv.RecordID = t.RecordID'
END
ELSE
--Abbas Shahid Khwaja 4900 07/07/2011 change the condition now it joins with the tblticketsviolations instead of tbltickets on recordid	
	BEGIN
		SET @Query=@Query+'
		 UPDATE #temp
			SET    TicketID_PK = tv.TicketID_PK
			FROM   #temp t
			LEFT OUTER JOIN DallasTrafficTickets.dbo.tblticketsviolations tv
            ON  tv.RecordID = t.RecordID '
	END
		SET @Query=@Query+' select * from #temp'
PRINT (@Query)

EXEC (@Query)

GO

GRANT EXECUTE ON dbo.usp_mailer_getClientInfo TO dbr_webuser

GO
/*    
Created By: Haris Ahmed    
Date:   08/30/2012    
    
Business Logic:    
 The stored procedure is used to get Potential Visa Client History
    
List of Input Parameters:    
 @DOB: Date of Birth
 @DOBFilter: Date of Birth Filter
 @InquiryDate: Inquiry Date
 @IDFilter: Inquiry Date Filter
 @CaseStatus: Case Status
*/

CREATE PROCEDURE [dbo].[USP_HTS_Get_PotentialVisaClientHistory]
	@DOB DATETIME,
	@DOBFilter BIT,
	@InquiryDate DATETIME,
	@IDFilter BIT,
	@CaseStatus INT
AS
SELECT tt.TicketID_PK,
	   tt.Firstname,
       tt.Lastname,
       CONVERT(VARCHAR,tt.DOB,101) AS DOB,
       thic.Comments,
       tt.Address1 + ' ' + ISNULL(tt.Address2, '') AS ADDRESS,
       tt.City,
       ts.STATE AS STATE,
       tt.Zip,
       CONVERT(
           VARCHAR(20),
           ISNULL(dbo.formatphonenumbers(tt.Contact1), '')
       ) + '(' + ISNULL(LEFT(ct1.Description, 1), '') + ')' AS contact1,
       CONVERT(
           VARCHAR(20),
           ISNULL(dbo.formatphonenumbers(tt.Contact2), '')
       ) + '(' + ISNULL(LEFT(ct2.Description, 1), '') + ')' AS contact2,
       CONVERT(
           VARCHAR(20),
           ISNULL(dbo.formatphonenumbers(tt.Contact3), '')
       ) + '(' + ISNULL(LEFT(ct3.Description, 1), '') + ')' AS contact3,
       CONVERT(VARCHAR,thic.InsertDate,101) AS InquiryDate,
       thics.[Status],
       thpl.PrintDate,
       th.LetterName,
       '<a href="javascript:window.open(''../ClientInfo/frmPreview.aspx?RecordID='+ convert(varchar(20),tn.RecordID)  +''');void('''');">View Image</a>' AS ViewImage       
FROM   tblTickets tt
       INNER JOIN tbl_HTS_ImmigrationComments thic
            ON  thic.TicketID = tt.TicketID_PK
       INNER JOIN tbl_HTS_ImmigrationCommentsStatus thics
            ON  thics.StatusID = thic.StatusID
       INNER JOIN tblState ts
            ON  ts.StateID = tt.Stateid_FK       
       INNER JOIN tblHTSBatchPrintLetter thpl 
			ON thpl.TicketID_FK = tt.TicketID_PK
	   INNER JOIN tblHTSNotes tn 
			ON tn.BatchID_PK = thpl.BatchID_PK
       INNER JOIN tblHTSLetters th 
			ON th.LetterID_pk = thpl.LetterID_FK
	   LEFT OUTER JOIN dbo.tblContactstype ct2
            ON  tt.ContactType2 = ct2.ContactType_PK
       LEFT OUTER JOIN dbo.tblContactstype ct1
            ON  tt.ContactType1 = ct1.ContactType_PK
       LEFT OUTER JOIN dbo.tblContactstype ct3
            ON  tt.ContactType3 = ct3.ContactType_PK
WHERE 
	thpl.LetterID_FK = 34 AND
	(@DOBFilter = 0 OR DATEDIFF(DAY, tt.DOB, @DOB) = 0) AND 
	(@IDFilter = 0 OR DATEDIFF(DAY, thic.InsertDate, @InquiryDate) = 0) AND 
	((@CaseStatus = 0 AND thic.StatusID <> 2) OR thic.StatusID = @CaseStatus)
ORDER BY thpl.PrintDate DESC
	
GO
GRANT EXECUTE ON USP_HTS_Get_PotentialVisaClientHistory TO dbr_webuser;
/******************************************************  
Created By : Waqas Javed  
Business Logic: This procedure is used to login the user after matching the user id and password and return user information.  
  
List of Parameters:  
--------------------  
@LoginId : This is User's Login ID  
@Password : this is user's password  
@IsAttorney : This is to mention that whether logged in user is attorney or not.
Column Return: EmpId, UserName,[Password],LastName,FirstName,Abbreviation,AccessType,canupdatecloseoutlog,UserLocation,flagCanSPNSearch,
			   SPNUserName,SPNPassword,FirmID,FirmName,isattorney,isServiceTicketAdmin
  
******************************************************/  
--Waqas 5373 01/05/2009   
ALTER PROCEDURE [dbo].[USP_HTS_Get_UserInfo] 
(
    @LoginId     VARCHAR(25),
    @Password    VARCHAR(25),
    @IsAttorney  BIT = NULL -- Afaq 7914 07/12/2010 Add parameter
)
AS
	SELECT employeeid AS EmpId,
	       LTRIM(RTRIM(UserName)) AS UserName,
	       --Waqas 5373 01/05/2009           
	       tblusers.[Password],
	       tblusers.LastName,
	       tblusers.FirstName,
	       tblusers.Abbreviation,
	       tblusers.AccessType,
	       ---5173         
	       ISNULL(canupdatecloseoutlog, 0) AS canupdatecloseoutlog,
	       ISNULL(UserLocation, 1) AS UserLocation,
	       0 AS flagCanSPNSearch,
	       --Waqas 5373 01/05/2009 
	       '' AS SPNUserName,
	       '' AS SPNPassword,
	       tblusers.FirmID,
	       tblFirm.FirmName,
	       ISNULL(isattorney, 0) AS isattorney
	       -- Asad Ali 7799 05/18/2010 get value of isServiceTicketAdmin
	       ,
	       isServiceTicketAdmin
	FROM   tblusers
	       INNER JOIN tblFirm
	            ON  tblusers.FirmID = tblFirm.FirmID
	WHERE  tblusers.username = @LoginId
	       AND tblusers.[password] = @password
	       AND tblusers.status = 1
	           -- Afaq 7914 07/12/2010 Add check for attorney used in attorney docket
	       AND (@IsAttorney IS NULL OR tblusers.IsAttorney = @IsAttorney)
	       
	           
  
    


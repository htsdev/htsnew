use TrafficTickets
go
/******         
Create by:  Agha Usman Ahmed
Modified By	: Muhammad Nasir 10071      
Business Logic : this procedure is used to insert bad email flag to the tickets who have the email address that is specified in parameters        
        
List of Parameters:         
    @email : Email Address to search the ticket id         
    @subject : parameter for email subject -- Afaq 8162 08/17/2010   
    -- Afaq 8162 08/17/2010 remove parmeter @BounceBackType  
      
    
List of Columns:     
    ticketNumber : show ticket number of a ticket    
        
       
******/           
    
ALTER PROCEDURE dbo.usp_htp_insertflagbyemail     
 @email VARCHAR(1000),    
 @subject VARCHAR(1000)    
    
AS    
BEGIN    
    DECLARE @flagId TINYINT    
        
    -- Fahad 09/17/2009 Declare Table Variable    
    -- Afaq 8162 08/23/2010 add ticket Id column  
    DECLARE @temp1 TABLE    
            (refcasenumber VARCHAR(20), SiteID TINYINT,TicketId_pk INT )    
                
    /*-----------------------------------------------------------    
    * Houston Traffic Program    
    -------------------------------------------------------------*/    
    --HOuston BADEmail Flag ID    
    SET @flagId = 34     
        
    -- save ticket Ids in temp table against specified email adddress        
    SELECT t.ticketId_Pk,    
           tv.refcasenumber INTO #tickets    
    FROM   tbltickets t    
           INNER JOIN tblticketsviolations tv    
                ON  t.ticketId_Pk = tv.ticketId_Pk    
    -- Babar Ahmad 9676 11/04/2011 Excluded records whose reviewed email flag is set.  
    WHERE  email = @email AND ISNULL(t.ReviewedEmailStatus,0) = 0   
    GROUP BY    
           t.ticketId_Pk,    
           tv.refcasenumber    
        
    --Muhammad Nasir 10071 12/20/2012 Mark deleteFlag to null to remove temporary hiding from bad email report
	update tblTickets
	SET ReviewedEmailStatus = 0
	,ReviewedEmailStatusID = 0
	WHERE  email = @email AND ISNULL(ReviewedEmailStatus,0) = 0

    --Muhammad Nasir 10071 12/20/2012 Mark deleteFlag to null to remove temporary hiding from bad email report
    Update tblTicketsFlag
	SET deleteFlag = 0
    FROM   tblTicketsFlag tf
	INNER JOIN #tickets t ON  t.ticketId_pk = tf.ticketId_Pk    
    AND tf.flagId = @flagId
         
    -- Removing records from temp table that have already specified flag        
    DELETE #tickets    
    FROM   #tickets t    
           INNER JOIN tblticketsflag tf    
                ON  t.ticketId_pk = tf.ticketId_Pk    
                AND tf.flagId = @flagId     
                 
        
    -- Fahad 6429 09/17/2009 Declare Table Variable    
    DECLARE @temp TABLE    
            (SNo INT IDENTITY(1, 1), TicketID INT)    
        
    INSERT INTO @temp    
      (    
        TicketID    
      )    
    SELECT DISTINCT ticketId_Pk    
    FROM   #tickets    
        
        
    DECLARE @rowcount INT    
    DECLARE @rowno INT    
        
    SET @rowno = 1    
        
    SELECT @rowcount = COUNT(*)    
    FROM   @temp     
        
    WHILE (@rowno <= @rowcount)    
    BEGIN    
        -- Inserting only those records whos flag is not inserted        
        INSERT INTO tblticketsflag    
          (    
            TicketId_Pk,    
            FlagId,    
            --Fahad 6429 09/17/2009 EmailBounceBackType added    
            EmailBounceBackType    
          )    
        SELECT TicketID,    
               @flagId,    
               0  -- Afaq 8162 08/17/2010  set 0 for all bounce email  
        FROM   @temp    
        WHERE  SNo = @rowno    
            
		--Muhammad Nasir 10071 12/24/2012 mark reviewed status
		update tblTickets
		SET ReviewedEmailStatus = 0
		,ReviewedEmailStatusID = 0
		where ticketID_PK = (SELECT TicketID FROM @temp WHERE  SNo = @rowno) 

        --Fahad 6429 09/07/2009 Varibale dcalare to check the Counce Back Type and all cases of IF ,Else-If and Else as following.    
        INSERT INTO tblticketsnotes    
          (    
            ticketid,    
            SUBJECT,    
            notes,    
            employeeid    
          )    
          --Fahad 6429 09/07/2009 History Notes Updated Email Address and Source Added    
        SELECT TicketID,    
               'Flag - Bad Email  ['+ @email+' ] Added: Email Subject [' + @subject + ']',  -- Afaq 8162 08/17/2010  Modify the comment   
               '',    
               3992    
        FROM   @temp    
        WHERE  SNo = @rowno    
            
        SET @rowno = @rowno + 1    
    END    
    -- Inserting RefCaseNumber and Site ID for Houston    
    INSERT INTO @temp1    
      (    
        refcasenumber,    
        SiteID,  
        TicketId_pk -- Afaq 8162 08/23/2010 add ticket number column.  
      )    
    SELECT DISTINCT '', -- Afaq 8162 08/23/2010 Remove ref case number column    
           1, --Houston             
			ticketId_Pk   
    FROM   #tickets    
        
    DROP TABLE #tickets    
        
    /*-----------------------------------------------------------    
    * Dallas Traffic Program    
    -------------------------------------------------------------*/    
    --Excluding Trial Notification EmailType    
        
    BEGIN    
        --Dallas BADEmail Flag ID    
        SET @flagId = 38     
            
        -- save ticket Ids in temp table against specified email adddress        
        SELECT t.ticketId_Pk,    
               tv.refcasenumber INTO #dtickets    
        FROM   dallastraffictickets.dbo.tbltickets t    
               INNER JOIN dallastraffictickets.dbo.tblticketsviolations tv    
                    ON  t.ticketId_Pk = tv.ticketId_Pk    
         -- Rab Nawaz 9676 12/02/2011 Excluded records whose reviewed email flag is set.  
  WHERE  email = @email AND ISNULL(t.ReviewedEmailStatus,0) = 0   
        GROUP BY    
               t.ticketId_Pk,    
               tv.refcasenumber    
            

		--Muhammad Nasir 10071 12/20/2012 Mark deleteFlag to null to remove temporary hiding from bad email report
		update dallastraffictickets.dbo.tblTickets
		SET ReviewedEmailStatus = 0
		,ReviewedEmailStatusID = 1
		WHERE  email = @email AND ISNULL(ReviewedEmailStatus,0) = 0         
    
		--Muhammad Nasir 10071 12/20/2012 Mark deleteFlag to null to remove temporary hiding from bad email report
		Update dallastraffictickets.dbo.tblTicketsFlag
		SET deleteFlag = 0
		FROM   dallastraffictickets.dbo.tblTicketsFlag tf
		INNER JOIN #dtickets t ON  t.ticketId_pk = tf.ticketId_Pk    
		AND tf.flagId = @flagId

        -- Removing records from temp table that have already specified flag        
        DELETE #dtickets    
        FROM   #dtickets t    
               INNER JOIN dallastraffictickets.dbo.tblticketsflag tf    
                    ON  t.ticketId_pk = tf.ticketId_Pk    
                    AND tf.flagId = @flagId     
            
        -- Fahad 09/17/2009 Declare Table Variable    
        DECLARE @dtemp TABLE    
                (SNo INT IDENTITY(1, 1), TicketID INT)    
            
        INSERT INTO @dtemp    
          (    
            TicketID    
          )    
        SELECT DISTINCT ticketId_Pk    
        FROM   #dtickets    
            
            
        DECLARE @drowcount INT    
        DECLARE @drowno INT    
            
        SET @drowno = 1    
            
        SELECT @drowcount = COUNT(*)    
        FROM   @dtemp     
            
        WHILE (@drowno <= @drowcount)    
        BEGIN    
            -- Inserting only those records whos flag is not inserted        
            INSERT INTO dallastraffictickets.dbo.tblticketsflag    
              (    
                TicketId_Pk,    
                FlagId,    
                --Fahad 6429 09/17/2009 EmailBounceBackType added    
                EmailBounceBackType    
              )    
            SELECT TicketID,    
                   @flagId,    
                   0  -- Afaq 8162 08/17/2010  set 0 for all bounce email  
            FROM   @dtemp    
            WHERE  SNo = @drowno

			--Muhammad Nasir 10071 12/24/2012 mark reviewed status
			update dallastraffictickets.dbo.tblTickets
			SET ReviewedEmailStatus = 0
			,ReviewedEmailStatusID = 1
			where ticketID_PK = (SELECT TicketID FROM @dtemp WHERE SNo = @drowno)     
                
            --Fahad 6429 09/07/2009 Varibale dcalare to check the Counce Back Type and all cases of IF ,Else-If and Else as following.    
            INSERT INTO dallastraffictickets.dbo.tblticketsnotes    
              (    
                ticketid,    
                SUBJECT,    
                notes,    
                employeeid    
              )    
            SELECT TicketID,    
                   'Flag - Bad Email  ['+ @email+' ] Added: Email Subject [' + @subject + ']',  -- Afaq 8162 08/17/2010  Modify the comment  
                   '',    
                   3992    
            FROM   @dtemp    
            WHERE  SNo = @drowno    
                
            SET @drowno = @drowno + 1    
        END    
            
        -- Inserting RefCaseNumber and Site ID for Dallas    
        INSERT INTO @temp1    
          (    
            refcasenumber,    
            SiteID,  
            TicketId_pk  -- Afaq 8162 08/23/2010 add ticket id number column.  
          )    
        SELECT DISTINCT '', -- Afaq 8162 08/23/2010 Remove ref case number column  
               2, --Dallas  
               ticketId_Pk    
        FROM   #dtickets    
            
        DROP TABLE #dtickets    
    END    
 Select * from  @temp1     
END 
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_All_Disposition_Codes]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_All_Disposition_Codes]
GO

CREATE PROCEDURE   [dbo].[usp_Get_All_Disposition_Codes]
as
BEGIN
	SELECT  Code , '(' + Code + ') ' + Description as Description   from tblDispositionCodes
END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


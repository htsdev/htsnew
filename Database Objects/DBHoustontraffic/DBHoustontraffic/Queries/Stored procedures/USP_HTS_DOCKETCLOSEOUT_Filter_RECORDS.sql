/* 
* Business Logic:This sp is used on NewDocketCloseOutOptimize.aspx.
* It will return: 
 Cases having no discrepancies.
 Cases having discrepancies.
 Case having auto and verified status match but not disposed and court date is in past.
 Cases on the basis of court location.
 Parameter:
@CourtDate                                        
@CourtName                    
@Status                 
@ShowClosedDocket 
@ShowRedX 
@ShowQuestionMark
@ShowYellowCheck 
@ShowGreenCheck 

* */
         
                     
Alter PROCEDURE [dbo].[USP_HTS_DOCKETCLOSEOUT_Filter_RECORDS]  --'12/11/2006'                                      
@CourtDate datetime ,                                       
@CourtName varchar(20),                    
@Status varchar(20),                  
@ShowClosedDocket int,
@ShowRedX bit,
@ShowQuestionMark bit,
@ShowYellowCheck bit,
@ShowGreenCheck bit
AS                                        
                      
                    
Declare @Query varchar(8000)          
          
set @query = ' declare @temp1 table (           
 ticketid_pk int,          
 lastname varchar(50) ,          
 firstname varchar(50),          
 bondflag tinyint,          
 courtdateSnapShot datetime,          
 courtnumber varchar(3), ---- Afaq 8059 09/24/2010 change datatype     
 shortdescription varchar(20),          
 courtname varchar(100),          
 trialcomments varchar(1000),          
 Midnum varchar(20),          
 ticketsViolationId int,          
 courtnamecomplete varchar(100),          
 lastupdate varchar(50),          
 recordid int,          
 courtid int,          
 shortname varchar(20),          
 causeno varchar(30),          
 ticketnumber varchar(30),          
 autostatus varchar(100),          
 autostatusid int,          
 autocourtdate datetime,           
 autocourtnumber varchar(3), -- Afaq 8059 09/24/2010 change datatype      
 verifiedstatus varchar(100),          
 courtdatemain datetime,          
 verifiedcourtnumber varchar(3), -- Afaq 8059 09/24/2010 change datatype        
 verifiedstatusid int,          
 snapshotstatus varchar(100),          
 Discrepency int ,        
 AutoCategoryId int,        
 VerifiedCategoryId int ,      
 showquestionmark bit       
 )           
        
        
 insert into @temp1 ( ticketid_pk, lastname, firstname,  bondflag, courtdatesnapshot, courtnumber,           
    shortdescription, courtname, trialcomments, midnum, ticketsviolationid, courtnamecomplete, lastupdate,           
    recordid, courtid, shortname, causeno, ticketnumber, autostatus, autostatusid, autocourtdate, autocourtnumber,          
    verifiedstatus, courtdatemain, verifiedcourtnumber, verifiedstatusid, snapshotstatus,Discrepency, AutoCategoryId, VerifiedCategoryId, showquestionmark)   
   
       
          
 SELECT s.TicketID_PK, s.LastName as Lastname,                                         
   s.FirstName,  s.BondFlag, s.CourtDate , isnull(s.CourtNumber,''''), -- Afaq 8059 09/24/2010 change datatype                                     
   s.ShortDescription,                                         
   s.CourtName,                                        
   s.TrialComments,                                         
   s.Midnum,                                         
   s.TicketsViolationID,                                        
   s.CourtName + ''-''+isnull(tc.address,'' '') +''-'' +CONVERT(varchar, s.CourtDate, 101) as CourtNameComplete,                                  
   isnull(s.LastUpdate,'' '') as LastUpdate,                                    
   s.RecordID,                                
   s.CourtID,                                
 tc.shortname  ,                    
 tv.casenumassignedbycourt as causeno,                   
 tv.refcasenumber as ticketnumber,                   
tblCourtViolationStatus_1.ShortDescription +'' ''+ CONVERT(varchar, TV.CourtDate,101) + '' '' + SUBSTRING(CONVERT(varchar, TV.CourtDate), 13, 7) + '' '' +    
isnull(TV.CourtNumber,'''') AS AutoStatus, -- Afaq 8059 09/24/2010 change datatype                                        
tv.courtviolationstatusid, tv.courtdate, tv.courtnumber,           
tblCourtViolationStatus_2.ShortDescription + '' '' + CONVERT(varchar, TV.CourtDateMain, 101) + '' '' + SUBSTRING(CONVERT(varchar,TV.CourtDateMain), 13, 7) +  
'' '' + isnull(TV.CourtNumbermain,'''') AS VerifiedStatus, -- Afaq 8059 09/24/2010 change datatype                                                               
tv.courtdatemain, tv.courtnumbermain,  tv.courtviolationstatusidmain,           
s.ShortDescription + '' '' + CONVERT(varchar, s.CourtDate, 101) + '' '' + SUBSTRING(CONVERT(varchar,s.CourtDate), 13, 7) + '' '' + isnull(s.CourtNumber,'''')                              AS SnapShotStatus,-- Afaq 8059 09/24/2010 change datatype         
dbo.Fn_CheckDiscrepency_1(s.TicketsViolationID) ,        
tblCourtViolationStatus_1.categoryid, tblCourtViolationStatus_2.categoryid ,       
0 as showquestionmark      
from tbl_HTS_DocketCloseOut_Snapshot  s                           
join tblcourts tc                  
on tc.courtid = isnull(s.courtId,3001)                  
join tblticketsviolations tv                
on tv.ticketsviolationid = s.ticketsviolationid                
LEFT OUTER JOIN                                                            
dbo.tblCourtViolationStatus tblCourtViolationStatus_1 ON                                               
TV.CourtViolationStatusID = tblCourtViolationStatus_1.CourtViolationStatusID                           
LEFT OUTER JOIN                                                            
dbo.tblCourtViolationStatus tblCourtViolationStatus_2 ON                                                             
TV.CourtViolationStatusIDmain = tblCourtViolationStatus_2.CourtViolationStatusID                            
where DateDiff(day,s.CourtDate,'''+Convert(varchar(10),@CourtDate,101)+''')=0        
      
update @temp1 set showquestionmark = 1      
where courtdatemain < getdate() and VerifiedCategoryId not in (50,12,7,1)      
  
'                   



          
-- Filtering Records On Searching Criteria           
Set @Query = @Query + ' Select * Into #temp2 from @temp1 where 1=1'          
          
if @CourtName !=''                  
 Set @Query = @Query + ' and ShortName='''+@CourtName + ''''                  
           
if @Status = '1'                  
 set @Query = @Query + ' and RTrim(Substring(AutoStatus,0,4)) in (''DLQ'',''FTA'')'                   
          
else if @Status = '2'                  
 --Set @Query = @Query + ' and RTrim(Substring(AutoStatus,0,4)) in (''ARR'',''PRE'',''JUR'',''JUD'',''WAIT'') and autocourtdate  > getdate()'                 
  
 Set @Query = @Query + ' and AutoCategoryId in (1,2,3,4,5) and autocourtdate  > getdate()'                   
          
else if @Status = '3'                  
 --Set @Query = @Query + ' and RTrim(Substring(AutoStatus,0,4)) in (''DISP'')'                   
 Set @Query = @Query + ' and AutoCategoryId = 50'                   
          
else if @Status = '4' --i.e  Show All NIR Cases                  
 --Set @Query = @Query + ' and RTrim(Substring(AutoStatus,0,4)) Not in (''ARR'',''PRE'',''JUR'',''JUD'',''WAIT'') and autocourtdate > getdate()'              
   
     
 Set @Query = @Query + ' and AutoCategoryId Not in (1,2,3,4,5,50) and autocourtdate > getdate()'                   
              
else if @Status = '5'                  
 Set @Query = @Query + '  and datediff(day,  isnull(autocourtdate,''01/01/1900'') , ''01/01/1900'') = 0       '            
          
if @ShowClosedDocket = 1                   
 Set @Query = @Query + ' and ( autoCourtNumber != verifiedCourtNumber  Or autoCourtDate ! = CourtDatemain or autoCourtNumber != verifiedCourtNumber)'         
else
begin
	-- Filtering selected checks
	set @Query = @Query + ' and Discrepency IN ('

	if @ShowRedX = 1
		set @Query = @Query + ' 1,'

	if @ShowQuestionMark = 1
		set @Query = @Query + ' 3,'
	   
	if @ShowYellowCheck =1
		set @Query = @Query + ' 2,'

	if @ShowGreenCheck =1
		set @Query = @Query + ' 0,'

	set @query = left(@query,len(@query)-1)

	set @query = @query + ') '
end

--Set @Query = @Query + ' and Discrepency = 1 '                
          
Set @Query = @Query + '         
        
Select ticketid_pk,  lastname, firstname, bondflag, courtdatesnapshot, courtnumber,           
    shortdescription, courtname, trialcomments, midnum, ticketsviolationid, courtnamecomplete, lastupdate,           
    recordid, courtid, shortname, causeno, ticketnumber, autostatus, autostatusid, autocourtdate, autocourtnumber,          
    verifiedstatus, courtdatemain, verifiedcourtnumber, verifiedstatusid, snapshotstatus,Discrepency ,showquestionmark       
 from #temp2 where shortdescription In (''ARR'',''PRE'',''JUR'',''JUR2'',''JUD'',''BF'',''A/W'',''TRI'')         
 order by courtid, courtnumber, courtdatesnapshot, lastname        
         
'           
        
Set @Query = @Query + ' Select * from #temp2 where shortdescription Not In (''ARR'',''PRE'',''JUR'',''JUR2'',''JUD'',''BF'',''A/W'',''TRI'')         
and shortdescription Not In(''BOND'',''COM'',''JUR2'',''DSCD'',''DAR'',''DPX'',''CLO'',''B/W'',''WAIT'')         
order by  courtid , courtnumber, courtdatesnapshot ,lastname drop table #temp2'           
          
exec ( @Query)           
print  @Query        
        
        
     
--------------------------------------------------------------------------------------------------------------------------------------------------  


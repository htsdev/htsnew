﻿ /******  
* Created By :	  Saeed Ahmed.
* Create Date :   11/10/2010 
* Task ID :		  8501
* Business Logic :  This procedure is used to update patient correspondence.
* List of Parameter : @PatientId 
* Column Return :     
******/
create PROCEDURE dbo.usp_HTP_UpdateCorrespondence
@PatientId INT,
@ContractSentDate DATETIME=NULL,
@ContractReceivedDate DATETIME=NULL,
@ContractSignedSentDate DATETIME=NULL,
@HippaSentDate DATETIME=NULL,
@HippaReceivedDate DATETIME=NULL,
@IsRejectionLetterSent BIT	

AS
UPDATE patient SET 
ContractSentDate=@ContractSentDate,
ContractReceivedDate=@ContractReceivedDate,
ContractSignedSentDate=@ContractSignedSentDate,
HippaSentDate=@HippaSentDate,
HippaReceivedDate=@HippaReceivedDate,
IsRejectionLetterSent=@IsRejectionLetterSent
WHERE Id=@PatientId

GO
GRANT EXECUTE ON dbo.usp_HTP_UpdateCorrespondence TO dbr_webuser
GO

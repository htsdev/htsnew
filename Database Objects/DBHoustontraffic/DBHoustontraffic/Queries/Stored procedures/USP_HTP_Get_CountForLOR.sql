﻿
/***********************************************************************************
 * Created By		: Muhammad Nasir	
 * Created Date		: 07/10/2009
 * Task Id			: 6013
 * 
 * Business Logic	: Get violation count if court HCJP 4-1 5-2 with status pretrial and pasadena court with status judge and  Court date is in future with more than two business days 
 * 
 * Parameter List	:
 *						@TicketID			: ticket ID  
 ********************************************************************************/




Alter PROCEDURE USP_HTP_Get_CountForLOR 
@TicketID int
as
SELECT COUNT(*) AS counts
FROM   tblTicketsViolations ttv
INNER JOIN tblCourtViolationStatus tcvs
ON tcvs.CourtViolationStatusID=ttv.CourtViolationStatusIDmain
WHERE  ttv.TicketID_PK = @TicketID
       AND DATEDIFF(
               DAY,
               dbo.fn_getnextbusinessday(GETDATE(), 2),
               ttv.CourtDateMain
           ) > 0
       AND (
               (
                   ttv.CourtID IN (3013, 3016)
                   AND tcvs.CategoryID = 3
               )
               OR (
                      ttv.CourtID = 3004
                      AND tcvs.CategoryID = 5
                  )
           )

GO
GRANT EXECUTE ON USP_HTP_Get_CountForLOR TO dbr_webuser
GO
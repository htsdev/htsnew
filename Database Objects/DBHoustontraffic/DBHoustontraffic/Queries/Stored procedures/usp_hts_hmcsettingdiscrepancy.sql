/*
* Modified by: Saeed
* Task ID : 7791
* Business logic : This procedure ised to get HMC cases having dispcrepencies 
* Parameter :     
* @showall : if @showall=0 then display records whose followup date is null,past or today, otherwise display all records.
* @ValidationCategory : Report category; 1=Alert & 2=Report
* */
Alter PROCEDURE [dbo].[usp_hts_hmcsettingdiscrepancy]
	@showall BIT = 0, --Sabir Khan 5977 07/03/2009 showall parameter added for displaying all cases...
	@ValidationCategory INT = 0 -- Saeed 7791 07/10/2010 @ValidationCategory=1 for alert & @ValidationCategory=2 for report
AS
	SELECT DISTINCT
	       --ROW_NUMBER() OVER(ORDER BY TT.TicketID_PK ASC),
	       TT.TicketID_PK,
	       ttv.casenumassignedbycourt AS causenumber,
	       ttv.RefCaseNumber,	-- Sabir Khan 5977 07/01/2009 ticket number added...
	       tt.lastname,
	       tt.firstname,
	       ISNULL(c1.shortdescription, 'N/A') AS VDesc,
	       CONVERT(VARCHAR(11), TTV.courtdatemain, 101) AS courtdatemain,
	       REPLACE(
	           SUBSTRING(CONVERT(VARCHAR(20), TTV.courtdatemain), 13, 18),
	           '12:00AM',
	           '0:00AM'
	       ) AS courttimemain,
	       ISNULL(TTV.courtnumbermain, 0) AS courtnumbermain, -- Afaq 8351 10/05/2010 Remove INT conversion.
	       ISNULL(c2.shortdescription, 'N/A') AS ADesc,
	       CONVERT(VARCHAR(11), TTV.courtdate, 101) AS courtdate,
	       REPLACE(
	           SUBSTRING(CONVERT(VARCHAR(20), TTV.courtdate), 13, 18),
	           '12:00AM',
	           '0:00AM'
	       ) AS courttime,
	       ISNULL(TTV.courtnumber, 0) AS courtnumber,-- Afaq 8351 10/05/2010 Remove INT conversion.
	       tt.activeflag,
	       '<a href="http://ln.legalhouston.com/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber='
	       + CONVERT(VARCHAR(30), TT.TicketID_PK) + '" >' + CONVERT(
	           VARCHAR(30),
	           ROW_NUMBER() OVER(ORDER BY TT.TicketID_PK ASC)
	       ) + '</a>' AS link,
	       CONVERT(VARCHAR(11), te.SettingDiscrepancyFollowupDate, 101) AS 
	       followupdate,	-- Sabir Khan 5977 07/03/2009 Follow up date field added...
	       te.SettingDiscrepancyFollowupDate AS sortfollowupdate
	FROM   tbltickets tt -- Sabir Khan 5977 07/23/2009 Join issue has been fixed...
	       
	       INNER JOIN tblticketsviolations ttv
	            ON  tt.TicketID_PK = ttv.TicketID_PK
	       INNER JOIN tblCourtViolationStatus c1
	            ON  ttv.CourtViolationStatusIDmain = c1.CourtViolationStatusID
	       INNER JOIN tblCourtViolationStatus c2
	            ON  ttv.CourtViolationStatusID = c2.courtviolationstatusid
	       LEFT OUTER JOIN tblticketsextensions te
	            ON  tt.TicketID_PK = te.ticketid_pk
	WHERE  1 = 1 
	       -- FILTERS FOR DISCREPANCY LOGIC.....
	       AND (
	               (
	                   ttv.courtnumber <> ttv.courtnumbermain
	                   OR ttv.courtdate <> ttv.courtdatemain
	                   OR c1.categoryid <> c2.categoryid
	               )
	           )
	           -- FILTER CLAUSES......
	           -- Verified Status in Jury, Judge
	       AND (
	               --Yasir Kamal 7011 11/19/2009 verified status B/W or A/W and auto primary status ARRAIGNMNET. 
	               (c1.categoryid IN (4, 5))
	               OR --Sabir Khan 7073 12/02/2009 use secondry auto status for arrainnment reset...
	                  --(c1.CourtViolationStatusID IN (201,202) AND c2.CategoryID = 2)
	                  (
	                      c1.CourtViolationStatusID IN (201, 202)
	                      AND c2.CourtViolationStatusID = 5
	                  )
	           )
	           -- Only Future Verified Date
	       AND DATEDIFF(DAY, ttv.courtdatemain, GETDATE()) < 0
	           --Sabir Khan 5977 07/03/2009 Add show all logic and also logic for null, past and today follow up date....
	       AND (
	               (@showall = 1 OR @ValidationCategory = 2) -- Saeed 7791 07/10/2010 display all records if showAll=1 or validation category is 'Report'
	               OR (
	                      (@showall = 0 OR @ValidationCategory = 1)
	                      AND DATEDIFF(
	                              DAY,
	                              ISNULL(te.SettingDiscrepancyFollowupDate, '1/1/1900'),
	                              GETDATE()
	                          ) >= 0
	                  )
	           ) -- Saeed 7791 07/10/2010 display  if showAll=0 or validation category is 'Alert' then display records which has 'follow up date' null,past or today.
	       AND tt.activeflag = 1
	       AND ttv.courtid IN (3001, 3002, 3003,3075) -- Abbas Qamar 10088 5/18/2012 adding hmc court
	ORDER BY
	       sortfollowupdate  




set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

-- Noufil 4487 08/05/2008 courtlocation field added and new parameter casetype added to get record filter with casetype
-- usp_HTS_Get_All_ThreeDaysBeforeCourtDate '04/04/2005','08/04/2008',-1
ALTER PROCEDURE [dbo].[usp_HTS_Get_All_ThreeDaysBeforeCourtDate]             
            
@DateFrom datetime,            
@DateTo datetime,
@casetypeid int            
            
AS            
SELECT DISTINCT             
                      dbo.tblViolationQuote.QuoteID, CONVERT(varchar(12), dbo.tblViolationQuote.LastContactDate, 101) AS LastContactDate, CONVERT(Int, GETDATE()             
                      - dbo.tblViolationQuote.LastContactDate) AS Days, 
                      dbo.tblTickets.Lastname + ', ' +dbo.tblTickets.Firstname  AS Customer, /* CONVERT(varchar(12), dbo.tblTickets.ContactDate, 101) AS ContactDate, Agha Usman 2664 06/17/2008 */             
                      CONVERT(decimal(8,0), dbo.tblTickets.calculatedtotalfee) as calculatedtotalfee,tblQuoteResult_2.QuoteResultDescription, dbo.tblViolationQuote.FollowUpYN,             
                      dbo.tblTickets.TicketID_PK, REPLACE(tblQuoteResult_1.QuoteResultDescription, '---------------------', ' ') AS FollowUpStatus, CONVERT(varchar(12),             
                      dbo.tblTicketsViolations.CourtDateMain, 101) AS CourtDate,            
					  dbo.tblViolationQuote.CallBackDate, dbo.tblViolationQuote.AppointmentDate,             
                      dbo.tblViolationQuote.CallBackTime, dbo.tblViolationQuote.AppointmentTime,tblTickets.casetypeid,c.shortname as crt
            
FROM         dbo.tblViolationQuote LEFT OUTER JOIN            
                      dbo.tblQuoteResult tblQuoteResult_1 ON dbo.tblViolationQuote.FollowUPID = tblQuoteResult_1.QuoteResultID RIGHT OUTER JOIN            
                      dbo.tblTicketsViolations RIGHT OUTER JOIN            
                      dbo.tblTickets ON dbo.tblTicketsViolations.TicketID_PK = dbo.tblTickets.TicketID_PK ON             
                      dbo.tblViolationQuote.TicketID_FK = dbo.tblTickets.TicketID_PK LEFT OUTER JOIN            
                      dbo.tblUsers ON dbo.tblTickets.EmployeeIDUpdate = dbo.tblUsers.EmployeeID LEFT OUTER JOIN            
                      dbo.tblQuoteResult tblQuoteResult_2 ON dbo.tblViolationQuote.QuoteResultID = tblQuoteResult_2.QuoteResultID            
						inner join dbo.tblCourts c on tblTicketsViolations.CourtID = c.Courtid
WHERE     (dbo.tblTickets.Activeflag = 0)  AND (dbo.tblViolationQuote.FollowUpYN = 1)             
			And  (Convert(datetime, dbo.tblViolationQuote.LastContactDate,101) <= Convert(datetime, (GETDATE() - 3) ,101) )            
			AND ( (CONVERT(datetime, dbo.tblTicketsViolations.CourtDateMain, 101) >= CONVERT(datetime, @DateFrom, 101) )            
			And (CONVERT(datetime, dbo.tblTicketsViolations.CourtDateMain, 101) <= CONVERT(datetime, (@DateTo+1), 101))             
			And
				dbo.tblTicketsViolations.CourtDateMain IN            
							  (SELECT MAX(dbo.tblTicketsViolations.CourtDateMain)FROM dbo.tblTicketsViolations            
									WHERE dbo.tblTicketsViolations.TicketID_PK = dbo.tblTickets.TicketID_PK))
			And  ((@casetypeid=-1 and 1=1) or (@casetypeid<> -1 and tblTickets.casetypeid= @casetypeid))
  
  


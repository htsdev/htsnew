set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


/****** Object:  StoredProcedure [dbo].[Usp_Htp_Faxreport]    Script Date: 05/23/2008 16:48:40 ******/
/*  
Created By : Noufil Khan  
Business Logic : This Procedure return those client whose fax letter had been sent.
Parametr:
			@statusid : fax status
			@fromdate : Date from which searching starts
			@todate   : Date to which searching ends
			@status   : for validation email report

column : ticketid,faxid,faxdate,faxstatus,firstname,lastname,causenumber,courtinfo,status,courtShortname,docpath

*/
-- Noufil 3999 06/19/2008 This Procedure return those client whose fax letter had been sent.
 -- [Usp_Htp_Faxreport]  -1,'','',1
-- [dbo].[Usp_Htp_Faxreport]  2,'07/09/2008','08/09/2008'


-- Noufil 4432 08/05/2008 html serial number added added for validation report


ALTER procedure [dbo].[Usp_Htp_Faxreport]   
@statusid int,  
@fromdate datetime,  
@todate datetime,  
@status bit=0  
  
as  
select distinct  
f.ticketid as ticketid,  
 f.faxletterid as faxid,  
 dbo.fn_DateFormat(isnull(f.faxdate,Convert(datetime,'1/1/1900')),30,'/',':',1) as faxdate,  
 case f.status when 'SUCCESS' then 'Confirmed' when 'PENDING' then 'Queued' else 'Declined' end as faxstatus,  
 t.firstname as fname,  
 t.lastname as lname,  
 tv.casenumassignedbycourt as causenumber,  
 dbo.fn_DateFormat(isnull(tv.CourtDateMain,Convert(datetime,'1/1/1900')),30,'/',':',1)+' '+tv.courtnumber as courtinfo,  
 cvs.shortdescription as status,  
 c.ShortName as CRT ,
f.docpath as docpath

from faxletter f  

 left outer join  tbltickets t  
  on t.ticketid_pk=f.ticketid  
 left outer join tblticketsviolations tv  
  on t.ticketid_pk=tv.ticketid_pk  
 left outer join dbo.tblCourtViolationStatus cvs                                   
  ON  tv.CourtViolationStatusIDMain = cvs.CourtViolationStatusID   
 left outer join tblcourts c    
  on tv.CourtID=c.Courtid 

  
where 
 ((@status=1 and f.status in ('PENDING','DECLINE')) or (@status=0 and f.status= (case @statusid when 0 then 'PENDING' when 1 then 'SUCCESS' when 2 then 'DECLINE' else f.status  end  )))
and ((@status=1 and datediff(day,f.faxdate,getdate()) = 0) or (@status=0 and (f.faxdate between @fromdate + ' 00:00:00'  and @todate+ ' 23:59:59')))
and tv.CourtViolationStatusIDMain in (104,3)
and tv.CourtID not in (3001,3002,3003)



SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_webscan_autoverify_bkp_29mar07]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_webscan_autoverify_bkp_29mar07]
GO

  
--select * from tbl_webscan_ocr where picid in (571,572)        
        
-- usp_webscan_autoverify 161,3991        
        
CREATE Procedure [dbo].[usp_webscan_autoverify_bkp_29mar07]         
        
@batchid int,        
@empid  int        
        
        
as        
        
declare @loopcounter as int        
        
create table #temp        
(        
 counter int IDENTITY(1,1) NOT NULL,        
 PicID int,                   
 CauseNo varchar(50),                   
 CourtDate datetime,                   
 CourtTime  varchar(15),                   
 CourtNo int,                   
 Status varchar(50),                   
 Location varchar(50),                   
 CourtDateScan  datetime    ,              
 EmployeeID int        
)        
        
insert into #temp select o.PicID,o.CauseNo,o.newcourtdate as courtdate,o.Time as courttime,o.CourtNo,o.Status,o.Location,o.newcourtdate+' '+o.Time as courtdatescan,@empid as employeeid        
from tbl_webscan_ocr o inner join         
  tbl_webscan_pic p on         
  o.picid=p.id inner join         
  tbl_webscan_batch b on        
  b.batchid=p.batchid        
where o.checkstatus=3 and        
  b.batchid=@batchid        
        
set @loopcounter=(select count(*) from #temp)        
        
while @loopcounter >=1        
begin        
        
  declare @PicID int        
  declare @CauseNo varchar(50)        
  declare @CourtDate as datetime        
  declare @CourtTime as varchar(15)        
  declare @CourtNo as int          
  declare @Status as varchar(50)        
  declare @Location as varchar(50)        
  declare @CourtDateScan as datetime        
  declare @EmployeeID as int        
  declare @Msg int      
 ---       
  declare @tempC table (                  
  causeno varchar(50),      
  picid int       
 )       
  declare @causenos varchar(50)         
 ---      
          
  select @PicID=PicID,@CauseNo=CauseNo,@CourtDate=courtdate,@CourtTime=courttime,@CourtNo=CourtNo,@Status=Status,@Location=Location,@courtdatescan=courtdatescan,@employeeid=employeeid        
  from #temp        
  where counter = @loopcounter        
        
  declare @TempCauseNo as varchar(50)                  
  select @TempCauseNo =casenumassignedbycourt from tblticketsviolations where casenumassignedbycourt=@CauseNo                  
  set @Msg=1              
                
  if    @TempCauseNo <> 'NULL'                  
   begin                  
                    
                         
    declare @TempCID as varchar(3)                      
    declare @CourtNumScan as varchar(50)                      
                          
    set @TempCID=@CourtNo                      
                          
    if @TempCID='13' or @TempCID='14'                      
   begin                      
    set @CourtNumScan='3002'                      
   end                      
    else if @TempCID='18'                      
   begin                      
    set @CourtNumScan='3003'                      
   end                      
    else                      
   begin                      
    set @CourtNumScan='3001'                      
   end                      
                        
    declare @tempCVID int                      
                  
    --Select @tempCVID=courtviolationstatusid from tblcourtviolationstatus where description=@Status                      
    if @Status='JURY TRIAL'     
  set @tempCVID=26    
    
 else if @Status='JUDGE'     
  set @tempCVID=103    
    
 else if @Status='PRETRIAL'     
  set @tempCVID=101    
   
 else if @Status='ARRAIGNMENT'     
 set @tempCVID=3   
    
    declare @TodayDate as datetime                      
    set @TodayDate=getdate()              
                
    begin                      
     update tblticketsviolations                      
                           
     set                      
                           
     CourtDateMain        =@CourtDateScan,                      
     CourtNumberMain        =@CourtNo,                      
     CourtViolationStatusIDmain     =@tempCVID,     
     VerifiedStatusUpdateDate     =@TodayDate,--@ScanUpdateDate,                      
     CourtID          =@CourtNumScan,               
     CourtDate         =@CourtDateScan,              
     CourtNumber         =@CourtNo,              
     CourtViolationStatusID      =@tempCVID,           
     CourtDateScan        =@CourtDateScan,              
     CourtViolationStatusIDScan     =@tempCVID,              
     CourtNumberScan        =@CourtNo,              
     ScanUpdatedate        =@TodayDate,              
     CourtIDScan         =@CourtNumScan,            
     Updateddate  =   @TodayDate ,                   
     vemployeeid         =@EmployeeID,        
     BSDAUpdateDate =getdate()                       
                            
                           
     where                       
  casenumassignedbycourt=@CauseNo                       
  and  CourtID in (3001,3002,3003)                      
         
 --      
 insert into @tempC select @CauseNo,@PicID      
 --      
                       
     update   tbl_webscan_ocr                          
   set                          
   causeno   =@CauseNo,                          
   Status   =@Status,                          
   NewCourtDate  =convert(varchar(12),@CourtDate,101),                          
   CourtNo   =@CourtNo,                          
   Location   =@Location,                          
                                
   Time    =@CourtTime ,              
   checkstatus  =4               
                   
   where                          
   picid=@PicID              
                   
     update tbl_webscan_data              
   set                         
   CauseNo    =@CauseNo,                        
   CourtDate   =@CourtDate,                        
   CourtTime   =@CourtTime,                        
   CourtRoomNo   =@CourtNo,                        
   Status    =@Status,                        
   CourtLocation  =@Location                        
   where                        
   PicID=@PicID               
                   
    set @Msg=2               
    end              
                
   end          
            
  if @Msg=1          
  begin          
   update   tbl_webscan_ocr                          
   set                          
   causeno   =@CauseNo,                          
   Status    =@Status,                          
   NewCourtDate  =convert(varchar(12),@CourtDate,101),                          
   CourtNo   =@CourtNo,                          
   Location   =@Location,                          
                               
   Time    =@CourtTime ,              
   checkstatus  =5               
                 
   where                          
    picid=@PicID              
                  
   update tbl_webscan_data              
   set                         
   CauseNo    =@CauseNo,                        
   CourtDate   =@CourtDate,                        
   CourtTime   =@CourtTime,                        
   CourtRoomNo   =@CourtNo,                    
   Status    =@Status,                        
   CourtLocation  =@Location                        
   where                        
    PicID=@PicID              
  end         
 set @loopcounter=@loopcounter-1        
end        
      
select causeno,picid from @tempC    
  
  
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


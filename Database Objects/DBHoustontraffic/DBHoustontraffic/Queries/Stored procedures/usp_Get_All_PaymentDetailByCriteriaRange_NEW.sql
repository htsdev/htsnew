SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_All_PaymentDetailByCriteriaRange_NEW]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_All_PaymentDetailByCriteriaRange_NEW]
GO










-- ACTUAL PROCEDURE.....



CREATE procedure dbo.usp_Get_All_PaymentDetailByCriteriaRange_NEW

@RecDate DateTime,
@RecDateTo DateTime,
@EmployeeID int,
@PaymentType Int,
@CourtID int = 0

AS

If (@EmployeeID=0 And @PaymentType=0  AND @CourtID = 0)
Begin
	SELECT     CONVERT(Varchar(20), dbo.tblTicketsPayment.ChargeAmount, 101) AS ChargeAmount, dbo.tblTicketsPayment.EmployeeID, 
                      dbo.tblTicketsPayment.PaymentType, CONVERT(Varchar(12), dbo.tblTicketsPayment.RecDate, 101) AS RecDate, dbo.tblUsers.Lastname, 
                      CASE 
		WHEN (LEN(dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname)>19) THEN  dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname + '...'
		ELSE dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname
	         END AS CustomerName,
		--LEFT(dbo.tblCourts.shortcourtname,20) + '...'  AS Court, 
		dbo.tblCourts.ShortName AS Court,
                      dbo.tblPaymenttype.Description AS CCType,  dbo.tblTicketsPayment.TicketID, REPLACE(dbo.tblTickets.BondFlag, 1, 'B') AS BondFlag,
		dbo.tblTicketsPayment.CardType, CONVERT(Varchar(12), MailDate, 101) AS ListDate,
		dbo.Fn_FormateTime(dbo.tblTicketsPayment.RecDate) As RecTime

	FROM         dbo.tblPaymenttype RIGHT OUTER JOIN
                      dbo.tblTicketsPayment ON dbo.tblPaymenttype.Paymenttype_PK = dbo.tblTicketsPayment.PaymentType LEFT OUTER JOIN
                      dbo.tblCourts RIGHT OUTER JOIN
                      dbo.tblTickets ON dbo.tblCourts.Courtid = dbo.tblTickets.CurrentCourtloc ON 
                      dbo.tblTicketsPayment.TicketID = dbo.tblTickets.TicketID_PK LEFT OUTER JOIN
                      dbo.tblUsers ON dbo.tblTicketsPayment.EmployeeID = dbo.tblUsers.EmployeeID
	Where 
		/*( CONVERT(varchar(12),tblTicketsPayment.RecDate, 101)   >=  CONVERT(varchar(12), @RecDate, 101)  
		     And CONVERT(varchar(12),tblTicketsPayment.RecDate, 101)   <=  CONVERT(varchar(12), @RecDateTo, 101)  )  */
		( tblTicketsPayment.RecDate  Between  @RecDate  AND  @RecDateTo+1)   And  PaymentVoid <> 1 
			AND  dbo.tblTicketsPayment.PaymentType IN (1,2,4,5,6,7,9) 
End

Else If (@EmployeeID<>0 And @PaymentType=0   AND @CourtID = 0)
Begin
	SELECT     CONVERT(Varchar(20), dbo.tblTicketsPayment.ChargeAmount, 101) AS ChargeAmount, dbo.tblTicketsPayment.EmployeeID, 
                      dbo.tblTicketsPayment.PaymentType, CONVERT(Varchar(12), dbo.tblTicketsPayment.RecDate, 101) AS RecDate, dbo.tblUsers.Lastname, 
                      CASE 
		WHEN (LEN(dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname)>19) THEN  dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname + '...'
		ELSE dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname
	         END AS CustomerName,
		dbo.tblCourts.ShortName AS Court,
                      dbo.tblPaymenttype.Description AS CCType,  dbo.tblTicketsPayment.TicketID, REPLACE(dbo.tblTickets.BondFlag, 1, 'B') AS BondFlag,
		dbo.tblTicketsPayment.CardType, CONVERT(Varchar(12), MailDate, 101) AS ListDate,
		dbo.Fn_FormateTime(dbo.tblTicketsPayment.RecDate) As RecTime
FROM         dbo.tblPaymenttype RIGHT OUTER JOIN
                      dbo.tblTicketsPayment ON dbo.tblPaymenttype.Paymenttype_PK = dbo.tblTicketsPayment.PaymentType LEFT OUTER JOIN
                      dbo.tblCourts RIGHT OUTER JOIN
                      dbo.tblTickets ON dbo.tblCourts.Courtid = dbo.tblTickets.CurrentCourtloc ON 
                      dbo.tblTicketsPayment.TicketID = dbo.tblTickets.TicketID_PK LEFT OUTER JOIN
                      dbo.tblUsers ON dbo.tblTicketsPayment.EmployeeID = dbo.tblUsers.EmployeeID
	Where 
		( tblTicketsPayment.RecDate  Between  @RecDate  AND  @RecDateTo+1)  AND  PaymentVoid <> 1 
		And  dbo.tblTicketsPayment.EmployeeID= @EmployeeID  AND  dbo.tblTicketsPayment.PaymentType IN (1,2,4,5,6,7,9) 
End

Else If (@EmployeeID=0 And @PaymentType<>0    AND @CourtID = 0)
Begin	
	IF (@PaymentType=200)
		Begin
		SELECT     CONVERT(Varchar(20), dbo.tblTicketsPayment.ChargeAmount, 101) AS ChargeAmount, dbo.tblTicketsPayment.EmployeeID, 
                      	dbo.tblTicketsPayment.PaymentType, CONVERT(Varchar(12), dbo.tblTicketsPayment.RecDate, 101) AS RecDate, dbo.tblUsers.Lastname, 
	CASE 

			WHEN (LEN(dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname)>19) THEN  dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname + '...'
			ELSE dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname
	         	END AS CustomerName,
		dbo.tblCourts.ShortName AS Court,
                      	dbo.tblPaymenttype.Description AS CCType,  dbo.tblTicketsPayment.TicketID, REPLACE(dbo.tblTickets.BondFlag, 1, 'B') AS BondFlag,
		dbo.tblTicketsPayment.CardType, CONVERT(Varchar(12), MailDate, 101) AS ListDate,
		dbo.Fn_FormateTime(dbo.tblTicketsPayment.RecDate) As RecTime
		FROM         dbo.tblPaymenttype RIGHT OUTER JOIN
                      	dbo.tblTicketsPayment ON dbo.tblPaymenttype.Paymenttype_PK = dbo.tblTicketsPayment.PaymentType LEFT OUTER JOIN
                      	dbo.tblCourts RIGHT OUTER JOIN
                      	dbo.tblTickets ON dbo.tblCourts.Courtid = dbo.tblTickets.CurrentCourtloc ON 
                      	dbo.tblTicketsPayment.TicketID = dbo.tblTickets.TicketID_PK LEFT OUTER JOIN
                      	dbo.tblUsers ON dbo.tblTicketsPayment.EmployeeID = dbo.tblUsers.EmployeeID
		Where 
			( tblTicketsPayment.RecDate  Between  @RecDate  AND  @RecDateTo+1)  AND  PaymentVoid <> 1 
			--And  dbo.tblTicketsPayment.EmployeeID= @EmployeeID
			--And  dbo.tblTicketsPayment.PaymentType <> 9		
			AND  dbo.tblTicketsPayment.PaymentType IN (1,2,4,5,6,7) 
		End
	
	ELSE IF (@PaymentType=300)
		Begin
		SELECT     CONVERT(Varchar(20), dbo.tblTicketsPayment.ChargeAmount, 101) AS ChargeAmount, dbo.tblTicketsPayment.EmployeeID, 
                      	dbo.tblTicketsPayment.PaymentType, CONVERT(Varchar(12), dbo.tblTicketsPayment.RecDate, 101) AS RecDate, dbo.tblUsers.Lastname, 
                      	CASE 
			WHEN (LEN(dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname)>19) THEN  dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname + '...'
			ELSE dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname
	         	END AS CustomerName,
		dbo.tblCourts.ShortName AS Court,
                      	dbo.tblPaymenttype.Description AS CCType,  dbo.tblTicketsPayment.TicketID, REPLACE(dbo.tblTickets.BondFlag, 1, 'B') AS BondFlag,
		dbo.tblTicketsPayment.CardType, CONVERT(Varchar(12), MailDate, 101) AS ListDate,
		dbo.Fn_FormateTime(dbo.tblTicketsPayment.RecDate) As RecTime
		FROM         dbo.tblPaymenttype RIGHT OUTER JOIN
                      	dbo.tblTicketsPayment ON dbo.tblPaymenttype.Paymenttype_PK = dbo.tblTicketsPayment.PaymentType LEFT OUTER JOIN
                      	dbo.tblCourts RIGHT OUTER JOIN
                      	dbo.tblTickets ON dbo.tblCourts.Courtid = dbo.tblTickets.CurrentCourtloc ON 
                      	dbo.tblTicketsPayment.TicketID = dbo.tblTickets.TicketID_PK LEFT OUTER JOIN
                      	dbo.tblUsers ON dbo.tblTicketsPayment.EmployeeID = dbo.tblUsers.EmployeeID
		Where 
			( tblTicketsPayment.RecDate  Between  @RecDate  AND  @RecDateTo+1)   AND  PaymentVoid <> 1 
			And  dbo.tblTicketsPayment.PaymentType IN (5,6)
		End
	
	ELSE IF (@PaymentType=301)
		Begin
		SELECT     CONVERT(Varchar(20), dbo.tblTicketsPayment.ChargeAmount, 101) AS ChargeAmount, dbo.tblTicketsPayment.EmployeeID, 
                      	dbo.tblTicketsPayment.PaymentType, CONVERT(Varchar(12), dbo.tblTicketsPayment.RecDate, 101) AS RecDate, dbo.tblUsers.Lastname, 
                      	CASE 
			WHEN (LEN(dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname)>19) THEN  dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname + '...'
			ELSE dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname
	         END AS CustomerName,
		dbo.tblCourts.ShortName AS Court,
                      	dbo.tblPaymenttype.Description AS CCType,  dbo.tblTicketsPayment.TicketID, REPLACE(dbo.tblTickets.BondFlag, 1, 'B') AS BondFlag,
		dbo.tblTicketsPayment.CardType, CONVERT(Varchar(12), MailDate, 101) AS ListDate,
		dbo.Fn_FormateTime(dbo.tblTicketsPayment.RecDate) As RecTime
		FROM         dbo.tblPaymenttype RIGHT OUTER JOIN
                      	dbo.tblTicketsPayment ON dbo.tblPaymenttype.Paymenttype_PK = dbo.tblTicketsPayment.PaymentType LEFT OUTER JOIN
                      	dbo.tblCourts RIGHT OUTER JOIN
                      	dbo.tblTickets ON dbo.tblCourts.Courtid = dbo.tblTickets.CurrentCourtloc ON 
                      	dbo.tblTicketsPayment.TicketID = dbo.tblTickets.TicketID_PK LEFT OUTER JOIN
                      	dbo.tblUsers ON dbo.tblTicketsPayment.EmployeeID = dbo.tblUsers.EmployeeID
		Where 
			( tblTicketsPayment.RecDate  Between  @RecDate  AND  @RecDateTo+1)   AND  PaymentVoid <> 1 
			--And  dbo.tblTicketsPayment.EmployeeID= @EmployeeID
			And  dbo.tblTicketsPayment.PaymentType IN (5)
		End	

	ELSE
		Begin
		SELECT     CONVERT(Varchar(20), dbo.tblTicketsPayment.ChargeAmount, 101) AS ChargeAmount, dbo.tblTicketsPayment.EmployeeID, 
                      	dbo.tblTicketsPayment.PaymentType, CONVERT(Varchar(12), dbo.tblTicketsPayment.RecDate, 101) AS RecDate, dbo.tblUsers.Lastname, 
                      	CASE 
			WHEN (LEN(dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname)>19) THEN  dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname + '...'
			ELSE dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname
	         END AS CustomerName,
		dbo.tblCourts.ShortName AS Court,
                      	dbo.tblPaymenttype.Description AS CCType,  dbo.tblTicketsPayment.TicketID, REPLACE(dbo.tblTickets.BondFlag, 1, 'B') AS BondFlag,
		dbo.tblTicketsPayment.CardType, CONVERT(Varchar(12), MailDate, 101) AS ListDate,
		dbo.Fn_FormateTime(dbo.tblTicketsPayment.RecDate) As RecTime
		FROM         dbo.tblPaymenttype RIGHT OUTER JOIN
                      	dbo.tblTicketsPayment ON dbo.tblPaymenttype.Paymenttype_PK = dbo.tblTicketsPayment.PaymentType LEFT OUTER JOIN
                      	dbo.tblCourts RIGHT OUTER JOIN
                      	dbo.tblTickets ON dbo.tblCourts.Courtid = dbo.tblTickets.CurrentCourtloc ON 
                      	dbo.tblTicketsPayment.TicketID = dbo.tblTickets.TicketID_PK LEFT OUTER JOIN
                      	dbo.tblUsers ON dbo.tblTicketsPayment.EmployeeID = dbo.tblUsers.EmployeeID
		Where 
			( tblTicketsPayment.RecDate  Between  @RecDate  AND  @RecDateTo+1)    AND  PaymentVoid <> 1 
			--And  dbo.tblTicketsPayment.EmployeeID= @EmployeeID
			And  dbo.tblTicketsPayment.PaymentType = @PaymentType
		End
End

Else If (@EmployeeID<>0 And @PaymentType<>0    AND @CourtID = 0)
Begin
	IF (@PaymentType=200)
		Begin
		SELECT     CONVERT(Varchar(20), dbo.tblTicketsPayment.ChargeAmount, 101) AS ChargeAmount, dbo.tblTicketsPayment.EmployeeID, 
                      	dbo.tblTicketsPayment.PaymentType, CONVERT(Varchar(12), dbo.tblTicketsPayment.RecDate, 101) AS RecDate, dbo.tblUsers.Lastname, 
	             CASE 
		WHEN (LEN(dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname)>19) THEN  dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname + '...'
		ELSE dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname
	         END AS CustomerName,
		dbo.tblCourts.ShortName AS Court,
	             dbo.tblPaymenttype.Description AS CCType,  dbo.tblTicketsPayment.TicketID, REPLACE(dbo.tblTickets.BondFlag, 1, 'B') AS BondFlag,
		dbo.tblTicketsPayment.CardType, CONVERT(Varchar(12), MailDate, 101) AS ListDate,
		dbo.Fn_FormateTime(dbo.tblTicketsPayment.RecDate) As RecTime
		FROM         dbo.tblPaymenttype RIGHT OUTER JOIN
                      	dbo.tblTicketsPayment ON dbo.tblPaymenttype.Paymenttype_PK = dbo.tblTicketsPayment.PaymentType LEFT OUTER JOIN
                      	dbo.tblCourts RIGHT OUTER JOIN
                      	dbo.tblTickets ON dbo.tblCourts.Courtid = dbo.tblTickets.CurrentCourtloc ON 
                      	dbo.tblTicketsPayment.TicketID = dbo.tblTickets.TicketID_PK LEFT OUTER JOIN
                      	dbo.tblUsers ON dbo.tblTicketsPayment.EmployeeID = dbo.tblUsers.EmployeeID
		Where 
			( tblTicketsPayment.RecDate  Between  @RecDate  AND  @RecDateTo+1)    AND  PaymentVoid <> 1 
		And  dbo.tblTicketsPayment.EmployeeID= @EmployeeID
		--And  dbo.tblTicketsPayment.PaymentType <> 9
		AND  dbo.tblTicketsPayment.PaymentType IN (1,2,4,5,6,7) 
		End

	IF (@PaymentType=300)
		Begin
		SELECT     CONVERT(Varchar(20), dbo.tblTicketsPayment.ChargeAmount, 101) AS ChargeAmount, dbo.tblTicketsPayment.EmployeeID, 
                      	dbo.tblTicketsPayment.PaymentType, CONVERT(Varchar(12), dbo.tblTicketsPayment.RecDate, 101) AS RecDate, dbo.tblUsers.Lastname, 
	            CASE 
		WHEN (LEN(dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname)>19) THEN  dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname + '...'
		ELSE dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname
	         END AS CustomerName,
		dbo.tblCourts.ShortName AS Court,
	             dbo.tblPaymenttype.Description AS CCType,  dbo.tblTicketsPayment.TicketID, REPLACE(dbo.tblTickets.BondFlag, 1, 'B') AS BondFlag,
		dbo.tblTicketsPayment.CardType, CONVERT(Varchar(12), MailDate, 101) AS ListDate,
		dbo.Fn_FormateTime(dbo.tblTicketsPayment.RecDate) As RecTime
		FROM         dbo.tblPaymenttype RIGHT OUTER JOIN
                      	dbo.tblTicketsPayment ON dbo.tblPaymenttype.Paymenttype_PK = dbo.tblTicketsPayment.PaymentType LEFT OUTER JOIN
                      	dbo.tblCourts RIGHT OUTER JOIN
                      	dbo.tblTickets ON dbo.tblCourts.Courtid = dbo.tblTickets.CurrentCourtloc ON 
                      	dbo.tblTicketsPayment.TicketID = dbo.tblTickets.TicketID_PK LEFT OUTER JOIN
                      	dbo.tblUsers ON dbo.tblTicketsPayment.EmployeeID = dbo.tblUsers.EmployeeID
		Where 
			( tblTicketsPayment.RecDate  Between  @RecDate  AND  @RecDateTo+1)   AND  PaymentVoid <> 1 
		And  dbo.tblTicketsPayment.EmployeeID= @EmployeeID
		And  dbo.tblTicketsPayment.PaymentType IN (5,6)
		End

	IF (@PaymentType=301)
		Begin
		SELECT     CONVERT(Varchar(20), dbo.tblTicketsPayment.ChargeAmount, 101) AS ChargeAmount, dbo.tblTicketsPayment.EmployeeID, 
                      	dbo.tblTicketsPayment.PaymentType, CONVERT(Varchar(12), dbo.tblTicketsPayment.RecDate, 101) AS RecDate, dbo.tblUsers.Lastname, 
	             CASE 
		WHEN (LEN(dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname)>19) THEN  dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname + '...'
		ELSE dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname
	         END AS CustomerName,
		dbo.tblCourts.ShortName AS Court,
	             dbo.tblPaymenttype.Description AS CCType,  dbo.tblTicketsPayment.TicketID, REPLACE(dbo.tblTickets.BondFlag, 1, 'B') AS BondFlag,
		dbo.tblTicketsPayment.CardType, CONVERT(Varchar(12), MailDate, 101) AS ListDate,
		dbo.Fn_FormateTime(dbo.tblTicketsPayment.RecDate) As RecTime
		FROM         dbo.tblPaymenttype RIGHT OUTER JOIN
                      	dbo.tblTicketsPayment ON dbo.tblPaymenttype.Paymenttype_PK = dbo.tblTicketsPayment.PaymentType LEFT OUTER JOIN
                      	dbo.tblCourts RIGHT OUTER JOIN
                      	dbo.tblTickets ON dbo.tblCourts.Courtid = dbo.tblTickets.CurrentCourtloc ON 
                      	dbo.tblTicketsPayment.TicketID = dbo.tblTickets.TicketID_PK LEFT OUTER JOIN
                      	dbo.tblUsers ON dbo.tblTicketsPayment.EmployeeID = dbo.tblUsers.EmployeeID
		Where 
			( tblTicketsPayment.RecDate  Between  @RecDate  AND  @RecDateTo+1)    AND  PaymentVoid <> 1 
		And  dbo.tblTicketsPayment.EmployeeID= @EmployeeID
		And  dbo.tblTicketsPayment.PaymentType IN (5)
		End

	ELSE
		Begin
		SELECT     CONVERT(Varchar(20), dbo.tblTicketsPayment.ChargeAmount, 101) AS ChargeAmount, dbo.tblTicketsPayment.EmployeeID, 
                      	dbo.tblTicketsPayment.PaymentType, CONVERT(Varchar(12), dbo.tblTicketsPayment.RecDate, 101) AS RecDate, dbo.tblUsers.Lastname, 
	            CASE 
		WHEN (LEN(dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname)>19) THEN  dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname + '...'
		ELSE dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname
	         END AS CustomerName,
		dbo.tblCourts.ShortName AS Court,
	             dbo.tblPaymenttype.Description AS CCType,  dbo.tblTicketsPayment.TicketID, REPLACE(dbo.tblTickets.BondFlag, 1, 'B') AS BondFlag,
		dbo.tblTicketsPayment.CardType, CONVERT(Varchar(12), MailDate, 101) AS ListDate,
		dbo.Fn_FormateTime(dbo.tblTicketsPayment.RecDate) As RecTime
		FROM         dbo.tblPaymenttype RIGHT OUTER JOIN
                      	dbo.tblTicketsPayment ON dbo.tblPaymenttype.Paymenttype_PK = dbo.tblTicketsPayment.PaymentType LEFT OUTER JOIN
                      	dbo.tblCourts RIGHT OUTER JOIN
                      	dbo.tblTickets ON dbo.tblCourts.Courtid = dbo.tblTickets.CurrentCourtloc ON 
                      	dbo.tblTicketsPayment.TicketID = dbo.tblTickets.TicketID_PK LEFT OUTER JOIN
                      	dbo.tblUsers ON dbo.tblTicketsPayment.EmployeeID = dbo.tblUsers.EmployeeID
		Where 
			( tblTicketsPayment.RecDate  Between  @RecDate  AND  @RecDateTo+1)   AND  PaymentVoid <> 1 
		And  dbo.tblTicketsPayment.EmployeeID= @EmployeeID
		And  dbo.tblTicketsPayment.PaymentType = @PaymentType
		End
END

Else If (@EmployeeID=0 And @PaymentType=0    AND @CourtID <>0)
	BEGIN
		SELECT     CONVERT(Varchar(20), dbo.tblTicketsPayment.ChargeAmount, 101) AS ChargeAmount, dbo.tblTicketsPayment.EmployeeID, 
		                      dbo.tblTicketsPayment.PaymentType, CONVERT(Varchar(12), dbo.tblTicketsPayment.RecDate, 101) AS RecDate, dbo.tblUsers.Lastname, 
             			         CASE 
		WHEN (LEN(dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname)>19) THEN  dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname + '...'
		ELSE dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname
	         	END AS CustomerName,
			dbo.tblCourts.ShortName AS Court,
	             		         dbo.tblPaymenttype.Description AS CCType,  dbo.tblTicketsPayment.TicketID, REPLACE(dbo.tblTickets.BondFlag, 1, 'B') AS BondFlag, 
				dbo.tblTicketsPayment.CardType, CONVERT(Varchar(12), MailDate, 101) AS ListDate,
		dbo.Fn_FormateTime(dbo.tblTicketsPayment.RecDate) As RecTime
			FROM         dbo.tblPaymenttype RIGHT OUTER JOIN
		 dbo.tblTicketsPayment ON dbo.tblPaymenttype.Paymenttype_PK = dbo.tblTicketsPayment.PaymentType LEFT OUTER JOIN
		                      dbo.tblCourts RIGHT OUTER JOIN
		                      dbo.tblTickets ON dbo.tblCourts.Courtid = dbo.tblTickets.CurrentCourtloc ON 
		                      dbo.tblTicketsPayment.TicketID = dbo.tblTickets.TicketID_PK LEFT OUTER JOIN
		                      dbo.tblUsers ON dbo.tblTicketsPayment.EmployeeID = dbo.tblUsers.EmployeeID
		Where 		
				( tblTicketsPayment.RecDate  Between  @RecDate  AND  @RecDateTo+1)   And  PaymentVoid <> 1 
				AND dbo.tblCourts.Courtid = @CourtID						
				--AND  dbo.tblTicketsPayment.PaymentType IN (6)  
				--And  dbo.tblTicketsPayment.EmployeeID= @EmployeeID
				--AND dbo.tblTicketsPayment.CardType = Right(@PaymentType,1)			
	END


Else If (@EmployeeID<>0 And @PaymentType=0    AND @CourtID <>0)
Begin
	SELECT     CONVERT(Varchar(20), dbo.tblTicketsPayment.ChargeAmount, 101) AS ChargeAmount, dbo.tblTicketsPayment.EmployeeID, 
                      dbo.tblTicketsPayment.PaymentType, CONVERT(Varchar(12), dbo.tblTicketsPayment.RecDate, 101) AS RecDate, dbo.tblUsers.Lastname, 
                      CASE 
		WHEN (LEN(dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname)>19) THEN  dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname + '...'
		ELSE dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname
	         END AS CustomerName,
		dbo.tblCourts.ShortName AS Court,
                      dbo.tblPaymenttype.Description AS CCType,  dbo.tblTicketsPayment.TicketID, REPLACE(dbo.tblTickets.BondFlag, 1, 'B') AS BondFlag,
		dbo.tblTicketsPayment.CardType, CONVERT(Varchar(12), MailDate, 101) AS ListDate,
		dbo.Fn_FormateTime(dbo.tblTicketsPayment.RecDate) As RecTime
	FROM         dbo.tblPaymenttype RIGHT OUTER JOIN
                      dbo.tblTicketsPayment ON dbo.tblPaymenttype.Paymenttype_PK = dbo.tblTicketsPayment.PaymentType LEFT OUTER JOIN
                      dbo.tblCourts RIGHT OUTER JOIN
                      dbo.tblTickets ON dbo.tblCourts.Courtid = dbo.tblTickets.CurrentCourtloc ON 
                      dbo.tblTicketsPayment.TicketID = dbo.tblTickets.TicketID_PK LEFT OUTER JOIN
                      dbo.tblUsers ON dbo.tblTicketsPayment.EmployeeID = dbo.tblUsers.EmployeeID
	Where 
		( tblTicketsPayment.RecDate  Between  @RecDate  AND  @RecDateTo+1)  AND  PaymentVoid <> 1 
		And  dbo.tblTicketsPayment.EmployeeID= @EmployeeID  
		AND dbo.tblCourts.Courtid = @CourtID						
		AND  dbo.tblTicketsPayment.PaymentType IN (1,2,4,5,6,7,9) 
End

Else If (@EmployeeID=0 And @PaymentType<>0    AND @CourtID <>0)
Begin
	IF (@PaymentType=200)
		Begin
		SELECT     CONVERT(Varchar(20), dbo.tblTicketsPayment.ChargeAmount, 101) AS ChargeAmount, dbo.tblTicketsPayment.EmployeeID, 
                      	dbo.tblTicketsPayment.PaymentType, CONVERT(Varchar(12), dbo.tblTicketsPayment.RecDate, 101) AS RecDate, dbo.tblUsers.Lastname, 
	             CASE 
		WHEN (LEN(dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname)>19) THEN  dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname + '...'
		ELSE dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname
	         END AS CustomerName,
		dbo.tblCourts.ShortName AS Court,
	             dbo.tblPaymenttype.Description AS CCType,  dbo.tblTicketsPayment.TicketID, REPLACE(dbo.tblTickets.BondFlag, 1, 'B') AS BondFlag,
		dbo.tblTicketsPayment.CardType, CONVERT(Varchar(12), MailDate, 101) AS ListDate,
		dbo.Fn_FormateTime(dbo.tblTicketsPayment.RecDate) As RecTime
		FROM         dbo.tblPaymenttype RIGHT OUTER JOIN
                      	dbo.tblTicketsPayment ON dbo.tblPaymenttype.Paymenttype_PK = dbo.tblTicketsPayment.PaymentType LEFT OUTER JOIN
                      	dbo.tblCourts RIGHT OUTER JOIN
                      	dbo.tblTickets ON dbo.tblCourts.Courtid = dbo.tblTickets.CurrentCourtloc ON 
                      	dbo.tblTicketsPayment.TicketID = dbo.tblTickets.TicketID_PK LEFT OUTER JOIN
                      	dbo.tblUsers ON dbo.tblTicketsPayment.EmployeeID = dbo.tblUsers.EmployeeID
		Where 
			( tblTicketsPayment.RecDate  Between  @RecDate  AND  @RecDateTo+1)    AND  PaymentVoid <> 1 
		--And  dbo.tblTicketsPayment.EmployeeID= @EmployeeID
		--And  dbo.tblTicketsPayment.PaymentType <> 9
		AND dbo.tblCourts.Courtid = @CourtID
		AND  dbo.tblTicketsPayment.PaymentType IN (1,2,4,5,6,7) 
		End

	IF (@PaymentType=300)
		Begin
		SELECT     CONVERT(Varchar(20), dbo.tblTicketsPayment.ChargeAmount, 101) AS ChargeAmount, dbo.tblTicketsPayment.EmployeeID, 
                      	dbo.tblTicketsPayment.PaymentType, CONVERT(Varchar(12), dbo.tblTicketsPayment.RecDate, 101) AS RecDate, dbo.tblUsers.Lastname, 
	            CASE 
		WHEN (LEN(dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname)>19) THEN  dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname + '...'
		ELSE dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname
	         END AS CustomerName,
		dbo.tblCourts.ShortName AS Court,
	             dbo.tblPaymenttype.Description AS CCType,  dbo.tblTicketsPayment.TicketID, REPLACE(dbo.tblTickets.BondFlag, 1, 'B') AS BondFlag,
		dbo.tblTicketsPayment.CardType, CONVERT(Varchar(12), MailDate, 101) AS ListDate,
		dbo.Fn_FormateTime(dbo.tblTicketsPayment.RecDate) As RecTime
		FROM         dbo.tblPaymenttype RIGHT OUTER JOIN
                      	dbo.tblTicketsPayment ON dbo.tblPaymenttype.Paymenttype_PK = dbo.tblTicketsPayment.PaymentType LEFT OUTER JOIN
                      	dbo.tblCourts RIGHT OUTER JOIN
                      	dbo.tblTickets ON dbo.tblCourts.Courtid = dbo.tblTickets.CurrentCourtloc ON 
                      	dbo.tblTicketsPayment.TicketID = dbo.tblTickets.TicketID_PK LEFT OUTER JOIN
                      	dbo.tblUsers ON dbo.tblTicketsPayment.EmployeeID = dbo.tblUsers.EmployeeID
		Where 
			( tblTicketsPayment.RecDate  Between  @RecDate  AND  @RecDateTo+1)   AND  PaymentVoid <> 1 
		--And  dbo.tblTicketsPayment.EmployeeID= @EmployeeID
		AND dbo.tblCourts.Courtid = @CourtID
		And  dbo.tblTicketsPayment.PaymentType IN (5,6)
		End

	IF (@PaymentType=301)
		Begin
		SELECT     CONVERT(Varchar(20), dbo.tblTicketsPayment.ChargeAmount, 101) AS ChargeAmount, dbo.tblTicketsPayment.EmployeeID, 
                      	dbo.tblTicketsPayment.PaymentType, CONVERT(Varchar(12), dbo.tblTicketsPayment.RecDate, 101) AS RecDate, dbo.tblUsers.Lastname, 
	             CASE 
		WHEN (LEN(dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname)>19) THEN  dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname + '...'
		ELSE dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname
	         END AS CustomerName,
		dbo.tblCourts.ShortName AS Court,
	             dbo.tblPaymenttype.Description AS CCType,  dbo.tblTicketsPayment.TicketID, REPLACE(dbo.tblTickets.BondFlag, 1, 'B') AS BondFlag,
		dbo.tblTicketsPayment.CardType, CONVERT(Varchar(12), MailDate, 101) AS ListDate,
		dbo.Fn_FormateTime(dbo.tblTicketsPayment.RecDate) As RecTime
		FROM         dbo.tblPaymenttype RIGHT OUTER JOIN
                      	dbo.tblTicketsPayment ON dbo.tblPaymenttype.Paymenttype_PK = dbo.tblTicketsPayment.PaymentType LEFT OUTER JOIN
                      	dbo.tblCourts RIGHT OUTER JOIN
                      	dbo.tblTickets ON dbo.tblCourts.Courtid = dbo.tblTickets.CurrentCourtloc ON 
                      	dbo.tblTicketsPayment.TicketID = dbo.tblTickets.TicketID_PK LEFT OUTER JOIN
                      	dbo.tblUsers ON dbo.tblTicketsPayment.EmployeeID = dbo.tblUsers.EmployeeID
		Where 
			( tblTicketsPayment.RecDate  Between  @RecDate  AND  @RecDateTo+1)    AND  PaymentVoid <> 1 
		--And  dbo.tblTicketsPayment.EmployeeID= @EmployeeID
		AND dbo.tblCourts.Courtid = @CourtID
		And  dbo.tblTicketsPayment.PaymentType IN (5)
		End

	ELSE
		Begin
		SELECT     CONVERT(Varchar(20), dbo.tblTicketsPayment.ChargeAmount, 101) AS ChargeAmount, dbo.tblTicketsPayment.EmployeeID, 
                      	dbo.tblTicketsPayment.PaymentType, CONVERT(Varchar(12), dbo.tblTicketsPayment.RecDate, 101) AS RecDate, dbo.tblUsers.Lastname, 
	            CASE 
		WHEN (LEN(dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname)>19) THEN  dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname + '...'
		ELSE dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname
	         END AS CustomerName,
		dbo.tblCourts.ShortName AS Court,
	             dbo.tblPaymenttype.Description AS CCType,  dbo.tblTicketsPayment.TicketID, REPLACE(dbo.tblTickets.BondFlag, 1, 'B') AS BondFlag,
		dbo.tblTicketsPayment.CardType, CONVERT(Varchar(12), MailDate, 101) AS ListDate,
		dbo.Fn_FormateTime(dbo.tblTicketsPayment.RecDate) As RecTime
		FROM         dbo.tblPaymenttype RIGHT OUTER JOIN
                      	dbo.tblTicketsPayment ON dbo.tblPaymenttype.Paymenttype_PK = dbo.tblTicketsPayment.PaymentType LEFT OUTER JOIN
                      	dbo.tblCourts RIGHT OUTER JOIN
                      	dbo.tblTickets ON dbo.tblCourts.Courtid = dbo.tblTickets.CurrentCourtloc ON 
                      	dbo.tblTicketsPayment.TicketID = dbo.tblTickets.TicketID_PK LEFT OUTER JOIN
                      	dbo.tblUsers ON dbo.tblTicketsPayment.EmployeeID = dbo.tblUsers.EmployeeID
		Where 
			( tblTicketsPayment.RecDate  Between  @RecDate  AND  @RecDateTo+1)   AND  PaymentVoid <> 1 
		--And  dbo.tblTicketsPayment.EmployeeID= @EmployeeID

		AND dbo.tblCourts.Courtid = @CourtID
		And  dbo.tblTicketsPayment.PaymentType = @PaymentType
		End
END

Else If (@EmployeeID<>0 And @PaymentType<>0    AND @CourtID <>0)
Begin
	IF (@PaymentType=200)
		Begin
		SELECT     CONVERT(Varchar(20), dbo.tblTicketsPayment.ChargeAmount, 101) AS ChargeAmount, dbo.tblTicketsPayment.EmployeeID, 
                      	dbo.tblTicketsPayment.PaymentType, CONVERT(Varchar(12), dbo.tblTicketsPayment.RecDate, 101) AS RecDate, dbo.tblUsers.Lastname, 
	             CASE 
		WHEN (LEN(dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname)>19) THEN  dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname + '...'
		ELSE dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname
	         END AS CustomerName,
		dbo.tblCourts.ShortName AS Court,
	             dbo.tblPaymenttype.Description AS CCType,  dbo.tblTicketsPayment.TicketID, REPLACE(dbo.tblTickets.BondFlag, 1, 'B') AS BondFlag,
		dbo.tblTicketsPayment.CardType, CONVERT(Varchar(12), MailDate, 101) AS ListDate,
		dbo.Fn_FormateTime(dbo.tblTicketsPayment.RecDate) As RecTime
		FROM         dbo.tblPaymenttype RIGHT OUTER JOIN
                      	dbo.tblTicketsPayment ON dbo.tblPaymenttype.Paymenttype_PK = dbo.tblTicketsPayment.PaymentType LEFT OUTER JOIN
                      	dbo.tblCourts RIGHT OUTER JOIN
                      	dbo.tblTickets ON dbo.tblCourts.Courtid = dbo.tblTickets.CurrentCourtloc ON 
                      	dbo.tblTicketsPayment.TicketID = dbo.tblTickets.TicketID_PK LEFT OUTER JOIN
                      	dbo.tblUsers ON dbo.tblTicketsPayment.EmployeeID = dbo.tblUsers.EmployeeID
		Where 
			( tblTicketsPayment.RecDate  Between  @RecDate  AND  @RecDateTo+1)    AND  PaymentVoid <> 1 
		And  dbo.tblTicketsPayment.EmployeeID= @EmployeeID
		--And  dbo.tblTicketsPayment.PaymentType <> 9
		AND dbo.tblCourts.Courtid = @CourtID
		AND  dbo.tblTicketsPayment.PaymentType IN (1,2,4,5,6,7) 
		End

	IF (@PaymentType=300)
		Begin
		SELECT     CONVERT(Varchar(20), dbo.tblTicketsPayment.ChargeAmount, 101) AS ChargeAmount, dbo.tblTicketsPayment.EmployeeID, 
                      	dbo.tblTicketsPayment.PaymentType, CONVERT(Varchar(12), dbo.tblTicketsPayment.RecDate, 101) AS RecDate, dbo.tblUsers.Lastname, 
	            CASE 
		WHEN (LEN(dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname)>19) THEN  dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname + '...'
		ELSE dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname
	         END AS CustomerName,
		dbo.tblCourts.ShortName AS Court,
	             dbo.tblPaymenttype.Description AS CCType,  dbo.tblTicketsPayment.TicketID, REPLACE(dbo.tblTickets.BondFlag, 1, 'B') AS BondFlag,
		dbo.tblTicketsPayment.CardType, CONVERT(Varchar(12), MailDate, 101) AS ListDate,
		dbo.Fn_FormateTime(dbo.tblTicketsPayment.RecDate) As RecTime
		FROM         dbo.tblPaymenttype RIGHT OUTER JOIN
                      	dbo.tblTicketsPayment ON dbo.tblPaymenttype.Paymenttype_PK = dbo.tblTicketsPayment.PaymentType LEFT OUTER JOIN
                      	dbo.tblCourts RIGHT OUTER JOIN
                      	dbo.tblTickets ON dbo.tblCourts.Courtid = dbo.tblTickets.CurrentCourtloc ON 
                      	dbo.tblTicketsPayment.TicketID = dbo.tblTickets.TicketID_PK LEFT OUTER JOIN
                      	dbo.tblUsers ON dbo.tblTicketsPayment.EmployeeID = dbo.tblUsers.EmployeeID
		Where 
			( tblTicketsPayment.RecDate  Between  @RecDate  AND  @RecDateTo+1)   AND  PaymentVoid <> 1 
		And  dbo.tblTicketsPayment.EmployeeID= @EmployeeID
		AND dbo.tblCourts.Courtid = @CourtID
		And  dbo.tblTicketsPayment.PaymentType IN (5,6)
		End

	IF (@PaymentType=301)
		Begin
		SELECT     CONVERT(Varchar(20), dbo.tblTicketsPayment.ChargeAmount, 101) AS ChargeAmount, dbo.tblTicketsPayment.EmployeeID, 
                      	dbo.tblTicketsPayment.PaymentType, CONVERT(Varchar(12), dbo.tblTicketsPayment.RecDate, 101) AS RecDate, dbo.tblUsers.Lastname, 
	             CASE 
		WHEN (LEN(dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname)>19) THEN  dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname + '...'
		ELSE dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname
	         END AS CustomerName,
		dbo.tblCourts.ShortName AS Court,
	             dbo.tblPaymenttype.Description AS CCType,  dbo.tblTicketsPayment.TicketID, REPLACE(dbo.tblTickets.BondFlag, 1, 'B') AS BondFlag,
		dbo.tblTicketsPayment.CardType, CONVERT(Varchar(12), MailDate, 101) AS ListDate,
		dbo.Fn_FormateTime(dbo.tblTicketsPayment.RecDate) As RecTime
		FROM         dbo.tblPaymenttype RIGHT OUTER JOIN
                      	dbo.tblTicketsPayment ON dbo.tblPaymenttype.Paymenttype_PK = dbo.tblTicketsPayment.PaymentType LEFT OUTER JOIN
                      	dbo.tblCourts RIGHT OUTER JOIN
                      	dbo.tblTickets ON dbo.tblCourts.Courtid = dbo.tblTickets.CurrentCourtloc ON 
                      	dbo.tblTicketsPayment.TicketID = dbo.tblTickets.TicketID_PK LEFT OUTER JOIN
                      	dbo.tblUsers ON dbo.tblTicketsPayment.EmployeeID = dbo.tblUsers.EmployeeID
		Where 
			( tblTicketsPayment.RecDate  Between  @RecDate  AND  @RecDateTo+1)    AND  PaymentVoid <> 1 
		And  dbo.tblTicketsPayment.EmployeeID= @EmployeeID
		AND dbo.tblCourts.Courtid = @CourtID
		And  dbo.tblTicketsPayment.PaymentType IN (5)
		End

	ELSE
		Begin
		SELECT     CONVERT(Varchar(20), dbo.tblTicketsPayment.ChargeAmount, 101) AS ChargeAmount, dbo.tblTicketsPayment.EmployeeID, 
                      	dbo.tblTicketsPayment.PaymentType, CONVERT(Varchar(12), dbo.tblTicketsPayment.RecDate, 101) AS RecDate, dbo.tblUsers.Lastname, 
	            CASE 
		WHEN (LEN(dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname)>19) THEN  dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname + '...'
		ELSE dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname
	         END AS CustomerName,
		dbo.tblCourts.ShortName AS Court,
	             dbo.tblPaymenttype.Description AS CCType,  dbo.tblTicketsPayment.TicketID, REPLACE(dbo.tblTickets.BondFlag, 1, 'B') AS BondFlag,
		dbo.tblTicketsPayment.CardType, CONVERT(Varchar(12), MailDate, 101) AS ListDate,
		dbo.Fn_FormateTime(dbo.tblTicketsPayment.RecDate) As RecTime
		FROM         dbo.tblPaymenttype RIGHT OUTER JOIN
                      	dbo.tblTicketsPayment ON dbo.tblPaymenttype.Paymenttype_PK = dbo.tblTicketsPayment.PaymentType LEFT OUTER JOIN
                      	dbo.tblCourts RIGHT OUTER JOIN
                      	dbo.tblTickets ON dbo.tblCourts.Courtid = dbo.tblTickets.CurrentCourtloc ON 
                      	dbo.tblTicketsPayment.TicketID = dbo.tblTickets.TicketID_PK LEFT OUTER JOIN
                      	dbo.tblUsers ON dbo.tblTicketsPayment.EmployeeID = dbo.tblUsers.EmployeeID
		Where 
			( tblTicketsPayment.RecDate  Between  @RecDate  AND  @RecDateTo+1)   AND  PaymentVoid <> 1 
		And  dbo.tblTicketsPayment.EmployeeID= @EmployeeID
		AND dbo.tblCourts.Courtid = @CourtID
		And  dbo.tblTicketsPayment.PaymentType = @PaymentType
		End
END








GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


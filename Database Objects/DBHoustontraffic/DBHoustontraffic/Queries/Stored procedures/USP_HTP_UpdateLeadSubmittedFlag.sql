USE TrafficTickets
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rab Nawaz Khan
-- Task Id:		11473
-- Create date: 10/30/2013
-- Description:	This Procedure is used to update the isLeadsDetailsSubmitted flag against clients who provid the Lead Details
-- =============================================
CREATE PROCEDURE USP_HTP_UpdateLeadSubmittedFlag 
	@TicketId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON
	IF ((SELECT COUNT(ticketId) FROM TrafficTickets.dbo.tblCallerIdsFromClients WHERE ticketId = @TicketId) = 0)
		BEGIN
			INSERT INTO [TrafficTickets].[dbo].[tblCallerIdsFromClients] ([ticketId], [isUnknown] ,[isLeadsDetailsSubmitted])
			VALUES (@TicketId, 0, 1)
		END
	ELSE
		BEGIN
			UPDATE TrafficTickets.dbo.tblCallerIdsFromClients
			SET isLeadsDetailsSubmitted = 1
			WHERE ticketId = @TicketId
		END
END
GO
	GRANT EXECUTE ON USP_HTP_UpdateLeadSubmittedFlag TO dbr_webuser
GO

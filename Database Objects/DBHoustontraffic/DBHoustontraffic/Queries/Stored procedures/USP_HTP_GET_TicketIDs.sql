﻿/****** 
Created by:		Fahad Muhammad Qureshi.
TaskID:			6934
Created Date:	12/08/2009
Business Logic:	The procedure is used by Houston Traffic Program to get Ticket ID's. 
******/

CREATE PROCEDURE dbo.USP_HTP_GET_TicketIDs
AS
SELECT tvq.TicketID_FK FROM tblViolationQuote tvq LEFT OUTER JOIN tblTickets tt ON tvq.TicketID_FK=tt.TicketID_PK 
GO
GRANT EXECUTE ON dbo.USP_HTP_GET_TicketIDs TO dbr_webuser
GO 
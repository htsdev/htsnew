﻿   
/******   
Alter by:  Farrukh Iftikhar
Business Logic : this procedure is used to get bond client ,reguler client and All client of reminder calls depend on the mode parameter.        
     Note: this same procedure is also used for auto dialer; when the optional auto dialer parameters are passed; 
			incase of auto dialer it only returns ticket ids
       
List of Parameters:   
    @reminderdate : date specify by user  
    @status       : call status  
       0 Pending  
       1 Contacted  
       2 Left Message  
       3 No Answer  
       4 Wrong Telephone Number  
       5 All  
    @showall    : show all record or not   
    @showreport   : remindercall or set call report  
       if showreport=0  
         display reminder call record with respect to date specify by user and the remindercall status it check all select then it shows the remindercall information for the next five days  
       if showreport=1  
         display setcall with respect to date and status specify by user in Judge,Jury or Pretrail   
  
    @mode     : for getting spacific record.  
        if mode=1  
        display all bond clients.  
        if mode=2  
        display all regular clients.  
        if mode=3  
        display all clients.  
    
    optional parameter for auto dialer; make sure that these parameters should be at last in this list
    @isAD               BIT = 0, if 1 than means comming from auto dialer and show all should be 0
    @reminderstartdate  DATETIME = '01/01/1900', start date from which searching should be made for autodialer
    @reminderenddate    DATETIME = '01/01/1900'  end date to which seaching should be made for autodialer
******/     
    
  
   
-- [USP_HTS_Get_remainderSMS_Update] '6/30/2009',5,1,0,-1,3,0,'01/01/1900','01/01/1900',1
    
ALTER PROCEDURE [dbo].[USP_HTS_Get_remainderSMS_Update] --'04/28/2006',2                        
(
    @reminderdate       DATETIME,
    @status             INT = 0,
    @showall            INT = 0,
    @showreport         INT = 0,
    @language           INT,
    @mode               INT,	/* parameter has been added for getting spacific record. */ 
    --ozair 4194 03/13/2009 optional parameter for auto dialer; make sure that these parameters should be at last in this list
    @isAD               BIT = 0,
    @reminderstartdate  DATETIME = '01/01/1900',
    @reminderenddate    DATETIME = '01/01/1900',
    -- Noufil 5884 06/30/2009 Caluse added for sms 
    @ReminderCallForSms BIT=0
)
AS
	SET @reminderdate = @reminderdate + '23:59:58'  
	--ozair 08/05/2009 length set to max from 8000 to resolve query execution prob
	DECLARE @str VARCHAR(max)        
	
	SET @str = 
	    '    
SELECT DISTINCT     
 t.TicketID_PK as ticketid,     
 --TV.TicketsViolationID as ticketViolationId,     
 t.Firstname,     
 t.Lastname,     
 isnull(t.LanguageSpeak,''N/A'') as LanguageSpeak,    
 t.BondFlag,    
 -- add by zahoor 3977  
 t.GeneralComments,      
'
	
	IF @showreport = 0
	BEGIN
	    SET @str = @str + 
	        '    
   tv.ReminderComments as comments,     
   isnull(tv.ReminderCallStatus,0) as status, ' -- changes made for Task Id 3882 04/28/2008
	END 
	----Nasir 6475 09/05/2009 remove FTA Call
	
	----Nasir 6475 09/05/2009 remove Set Call
	
	SET @str = @str + 
	    '    
 convert(varchar(20),ISNULL(dbo.formatphonenumbers(T.Contact1),''''))+''(''+ISNULL(LEFT(Ct1.Description, 1),'''')+'')'' as contact1,                                              
 convert(varchar(20),ISNULL(dbo.formatphonenumbers(T.Contact2),''''))+''(''+ISNULL(LEFT(Ct2.Description, 1),'''')+'')'' as contact2,                                              
 convert(varchar(20),ISNULL(dbo.formatphonenumbers(T.Contact3),''''))+''(''+ISNULL(LEFT(Ct3.Description, 1),'''')+'')'' as contact3,                                               
 T.TotalFeeCharged,    
 f.FirmAbbreviation,    
 f.Phone,    
 f.Fax,    
 --ozair 4837 10/08/2008 court address commented  
 --c.Address + '' '' + c.City + '', TX  '' + c.Zip AS courtaddress,    
 --Insurance=(case when v.shortdesc=''ins'' then v.shortdesc+''(''+isnull(convert(varchar,tv.ticketviolationdate,101),'''')+'')'' else '''' end),              
 --Child=(case when v.shortdesc=''chld blt'' then v.shortdesc else '''' end)    
 Insurance=(select top 1 vv.shortdesc+''(''+isnull(convert(varchar,tvv.ticketviolationdate,101),'''')+'')''    
   from tblviolations vv inner join tblticketsviolations tvv    
    on vv.violationnumber_pk=tvv.violationnumber_pk     
   where tvv.ticketid_pk=t.ticketid_pk            
   and vv.shortdesc=''ins''),    
 Child=(select top 1 vv.shortdesc            
   from tblviolations vv inner join tblticketsviolations tvv     
    on vv.violationnumber_pk=tvv.violationnumber_pk    
   where tvv.ticketid_pk=t.ticketid_pk            
   and vv.shortdesc=''chld blt''),    
--ozair 4837 10/16/2008 courtdate onverted to mm/dd/yyyy format   
Convert(datetime,convert(varchar,CourtDateMain,101)) as CourtDateMain,
--Ozair 4194 07/07/2009 added dialer status
DialerStatus = CASE WHEN tv.AutoDialerReminderCallStatus=0 THEN ''Pending'' ELSE adr.ResponseType END,
-- Noufil 5884 07/14/2009  courtdate,courtroom,courtshortname,SendSMSFlag1 ,SendSMSFlag2 ,SendSMSFlag3 columns added.
TV.CourtDateMain AS courtdate,TV.CourtNumbermain AS courtroom,
-- Noufil 6177 08/17/2009 Short name replace with Court name and Adress + Zip
 --Muhammad Ali 8370 10/08/2010 add Court Address with court name via "-" seperator, for SMS service
CONVERT(VARCHAR(50),c.CourtName)+''-''+CONVERT(VARCHAR(50) ,c.Address)  AS courtshortname,
isnull(t.SendSMSFlag1,0) as SendSMSFlag1 ,isnull(t.SendSMSFlag2,0) as SendSMSFlag2 ,isnull(t.SendSMSFlag3,0) as SendSMSFlag3,
--Abbas Shahid Khwaja 9231 07/01/2011 include zip, address2, city,state
c.Zip,
c.Address2 AS CourtAddress2,
c.City,
ts.[State]
into #temp4    
FROM  tblTicketsViolations TV --added in order to get single ticketid          
 INNER JOIN dbo.tblTickets t ON  TV.TicketID_PK = t.TicketID_PK                            
 inner join dbo.tblCourtViolationStatus cvs ON TV.CourtViolationStatusIDMain = cvs.CourtViolationStatusID AND cvs.CategoryID <> 50    --Haris Ahmed 10335 06/20/2012 Issue fix for picking dispose client 
 INNER JOIN dbo.tblCourts c ON  TV.CourtID = c.Courtid               
 LEFT OUTER JOIN dbo.tblFirm f ON t.FirmID = f.FirmID and  isnull(t.firmid,3000) = 3000                 
 LEFT OUTER JOIN dbo.tblContactstype ct2 ON  t.ContactType2 = ct2.ContactType_PK                                 
 LEFT OUTER JOIN dbo.tblContactstype ct1 ON  t.ContactType1 = ct1.ContactType_PK                                 
 LEFT OUTER JOIN dbo.tblContactstype ct3 ON  t.ContactType3 = ct3.ContactType_PK               
 INNER JOIN tblviolations v on v.violationnumber_pk = isnull(tv.violationnumber_pk,0)                                                
 --Sabir Khan 4760 09/08/2008  
 INNER JOIN dbo.tblticketspayment TP ON TV.TicketID_PK =TP.TicketID
 --Abbas Shahid 9231 07/13/2011 added state
 LEFT OUTER JOIN tblState ts ON ts.StateID = c.[State]
 --Ozair 4194 07/07/2009 added dialer status
 LEFT OUTER JOIN AutoDialerResponse adr
 ON	adr.ResponseID=tv.AutoDialerReminderCallStatus  
 WHERE isnull(t.firmid,3000) = 3000    
   
 --Sabir Khan 4876  09/29/2008  
 and t.CaseTypeId=1 
   
-- Show report = 0 for reminder calls    
-- Show report = 1 for set calls    
-- Show report = 2 for FTA calls  3977  
    
-- and activeflag=1 // Comment by Zahoor 3977  
AND     
    
' 
	--  

	
	----Nasir 6475 09/05/2009 remove Set Call
	
	IF @showreport = 0
	BEGIN
	    --Farrukh 10367 09/05/2012  Excluding Cases having call back Status as Contacted, SMS Reply Confirmed & AutoDialer-Confirmed
	    --Sabir 4272 07/12/2008  For Reminder Call  
	    SET @str = @str + 
	        ' activeflag=1 and TV.ticketID_PK NOT IN (select ticketID_PK from tblticketsviolations where remindercallstatus in (1, 7, 8) and  convert(datetime,dbo.GetDateFromComments(ReminderComments)) >= (getdate()-(case when datepart(weekday,(getdate()))=6 then 5 else 7 end)))  AND AutoDialerReminderCallStatus <> 1 AND ' 
	    --Sabir Khan 4760 09/08/2008  
	    SET @str = @str + 
	        ' dateDiff(day,TP.Recdate,TV.CourtDateMain) <> 0 and dateDiff(day,TP.Recdate,dbo.fn_getlastbusinessday(TV.CourtDateMain, 1))  <> 0 and TP.paymentvoid =0 AND  '
	    -- Rab Nawaz 9912 11/15/2011 Exclude case having status in bond forfeiture.
	    SET @str = @str + 
	        ' TV.ticketID_PK NOT IN(SELECT DISTINCT ticketID_PK from tblticketsviolations WHERE CourtViolationStatusID  = 196) AND  '
	END 
	-- Comment by Zahoor: for getting contacted status records.  
	/*if @showreport=2 --Zahoor 3977 08/13/2008  For FTA Call  
	BEGIN  
	set @str=@str+ ' isnull(tv.FTACallStatus,0)<>1 and '  
	END*/ 
	-- Modified By Noufil Filter searching by language    
	IF @language = 0
	BEGIN
	    SET @str = @str + ' t.LanguageSpeak = ''ENGLISH'' AND'
	END
	ELSE 
	IF @language = 1
	BEGIN
	    SET @str = @str + ' t.LanguageSpeak = ''SPANISH'' AND'
	END
	ELSE 
	IF @language = -1
	BEGIN
		--Fahad 7975 08/27/2010 Added In clause to get both Spanish and English Language
		IF @isAD=1
			SET @str = @str + ' t.LanguageSpeak in (''ENGLISH'',''SPANISH'') AND'	
		ELSE
			SET @str = @str + ''
	END  
	IF @showreport = 0
	BEGIN
		IF @showall = 0
		BEGIN
			IF @isAD=1
			--Ozair 4194 03/13/2009 For Auto Dialer Reminder calls...  
			SET @str = @str + ' DATEDIFF([day], TV.CourtDateMain, ''' + CONVERT(VARCHAR(20), @reminderstartdate) + ''')<=0 
				and DATEDIFF([day], TV.CourtDateMain, ''' + CONVERT(VARCHAR(20), @reminderenddate) + ''')>=0
				and ticketsviolationid not in (select ticketviolationid from tblticketsviolationlog where newverifiedstatus in (26,103) AND newverifiedstatus <> oldverifiedstatus AND recdate >= (getdate()-(case when datepart(weekday,getdate()) in (5,6) then 4 else 6 end))) AND '
			ELSE
			--Sabir Khan 5403 01/12/2009 Reminder calls...  
			SET @str = @str + ' DATEDIFF([day], TV.CourtDateMain, ''' + CONVERT(VARCHAR(20), @reminderdate) 
				+ ''')=0 and ticketsviolationid not in (select ticketviolationid from tblticketsviolationlog where newverifiedstatus in (26,103) AND newverifiedstatus <> oldverifiedstatus AND recdate >= (getdate()-(case when datepart(weekday,getdate()) in (5,6) then 4 else 6 end))) AND '            
		END 
		ELSE
    		BEGIN
    		IF @ReminderCallForSms = 0		
				SET @str = @str + ' datediff(day,tv.courtdatemain,getdate()) between datediff(day, getdate(), getdate()-5) and 0  and '         
			-- Noufil 5884 06/30/2009 Reminder call sms clause added. Show records having court date 3 business days for today's date
			ELSE 
			BEGIN
				SET @str = @str + ' DATEDIFF(DAY,[dbo].[fn_getprevbusinessday] (tv.courtdatemain,3),GETDATE()) = 0
					-- Rab Nawaz 9961 01/03/2012 cases which contains NISI in cause Number will be Excluded. . 
					AND NOT EXISTS ( SELECT ttv.TicketID_PK
 					FROM tblTicketsViolations ttv 
 					WHERE t.TicketID_PK = ttv.TicketID_PK
 					AND CHARINDEX(''NISI'', ISNULL (ttv.casenumassignedbycourt, '''')) > 0) AND   -- Should contain NISI in casenumassignedbycourt 
					-- END 9961 '
				END 
        		
    		END
    	   	
    	
        	
    --  set @str = @str + ' DATEDIFF([day], TV.CourtDateMain, getdate())<=0 AND DATEDIFF([day], TV.CourtDateMain,getdate()) >= -5 AND '--DATEDIFF([day], '04/22/2008',getdate())<= 0 and DATEDIFF([day], '04/22/2008',getdate()) >= -5  
    
    SET @str = @str + 
        '    
  (     
   cvs.categoryid IN (3, 4)      
  OR      
   (    
    cvs.categoryid = 5 and TV.CourtID NOT IN (3001, 3002, 3003, 3075) AND TV.CourtID <> 3037                                                  
    --------------------------Added By Zeeshan Ahmed----------------------      
    --Dont Display Cases Having Judge Status and HMC Court ---------------      
    AND  NOT ((cvs.categoryid = 5 and TV.CourtID IN (3001, 3002, 3003, 3075)))    
   )    
  )    
  '
   IF @mode = 1 --Nasir 6475 09/05/2009 add bond flag for reminder call
	    BEGIN	      
	        SET @str = @str + 
	            ' and isnull(t.BondFlag,0) = 1'
	    END
	    ELSE IF @mode = 2 --Nasir 6475 09/05/2009 add none bond flag for reminder call
	    BEGIN
	        SET @str = @str + ' and isnull(t.BondFlag,0) = 0 '
	    END
	   
END
	
----Nasir 6475 09/05/2009 remove FTA Call
	
	----Nasir 6475 09/05/2009 remove Set Call  
	
	
	
	SET @str = @str + 
	    '    
                                            
select distinct       
 ticketid,     
 --ticketViolationId,     
 firstname,    
 lastname,     
 languagespeak,    
 bondflag,    
 comments,    
 GeneralComments,  
 ISNULL(status,0) as status,     
                  
 Contact1,     
 Contact2,     
 Contact3,     
T.CourtDateMain,     
 totalfeecharged,    
-- courtnumber,    
 FirmAbbreviation,     
 phone,     
 fax,     
-- trialdesc,    
--ozair 4837 10/08/2008 court address commented  
 --courtaddress,              
 Insurance,              
 Child,         
 case dbo.tblTicketsFlag.flagid     
 when 5 then 1  else 2  end as flagid,        
  rs.Description as callback,
 --Ozair 4194 07/07/2009 added dialer status
 DialerStatus,
 -- Noufil 5884 07/14/2009  courtdate,courtroom,courtshortname,SendSMSFlag1 ,SendSMSFlag2 ,SendSMSFlag3 columns added
 courtdate,courtroom,courtshortname,SendSMSFlag1,SendSMSFlag2,SendSMSFlag3,
--Abbas Shahid Khwaja 9231 07/01/2011 include zip, address2, city,state in gropuby clause.
 Zip,
CourtAddress2,
 City,
[State]             
  into #temp1     
 from #temp4 T     
  left OUTER JOIN    
  dbo.tblTicketsFlag ON T.TicketID = dbo.tblTicketsFlag.TicketID_PK     
  and dbo.tblTicketsFlag.FlagID not in (5)       
 left outer join tblReminderStatus rs on rs.Reminderid_pk = T.status                                                 
  --  order by T.CourtDateMain                                         
                                          
                                         
                                          
select                                                   
 T.*,  owedamount = t.totalfeecharged -     
  (     
  select isnull(sum(isnull(chargeamount,0)),0) from tblticketspayment p     
  where p.ticketid = t.ticketid            
  and   p.paymenttype not in (99,100)     
  and   p.paymentvoid<>1     
  )     
    
  into #temp2     
  from #temp1 T     
order by T.CourtDateMain     
                                  
--newly added to call function in order to get distinct violations                                 
                                  
select     
T.*,dbo.fn_hts_get_TicketsViolations (ticketid,CourtDateMain) as trialdesc ,    
WrongNumberFlag = (select Count(*) from tblticketsflag where ticketid_pk = T.TicketId and FlagID=33)     
,dbo.fn_hts_get_TicketsViolationIds (ticketid,CourtDateMain) as TicketViolationIds   
into #temp5     
from #temp2 T     
-- added by tahir dt: 4/25/08 task 3878  
--by default, sort the records on last name and then first name.....  
order by t.lastname, t.firstname    
--Yasir Kamal 5623 03/06/2009 new sp created for missed court date calls report
--order by CourtDateMain
--5623 end
' 
	--if @status=null
	--begin
	--@status=0
	--end  
	--Ozair 4194 03/13/2009 for Auto Dialer Reminder Call
	IF @isAD = 1
	BEGIN
		SET @str = @str + 'DECLARE @result VARCHAR(MAX)
		SET @result = ''''
		SELECT @result = @result + CONVERT(VARCHAR,ticketid) + '',''
		FROM   #temp5 
		WHERE  STATUS = '+ CONVERT(VARCHAR, @status)+'
	    ORDER BY
	           lastname,
	           firstname
		
		SELECT  @result AS ticketids'
	END		
	ELSE     
	IF @status = 5
	BEGIN
	    SET @str = @str + 'select * from #temp5 '
	END
	ELSE
	BEGIN
	    SET @str = @str + 'select * from #temp5 where status = ' + CONVERT(VARCHAR, @status)
	END                                                    
	
	SET @str = @str + 
	    '                              
drop table #temp1                                                    
drop table #temp4                                                 
drop table #temp2                                                
drop table #temp5' 

	--PRINT @str    
	EXEC (@str)  
	
	

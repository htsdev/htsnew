﻿USE TrafficTickets
GO 
 
/****** 
Created by:		Zeeshan Haider
Task ID:		10595
Date:			01/01/2013
Business Logic:	The procedure is used by LMS application to get data for AMC Appearance Day Of letter
				and to create LMS history for this letter for the selected date range.
				
List of Parameters:
	@catnum:		Category number of the selected letter type, if printing for 
					all courts of the selected court category
	@BondFlag:		Flag that identifies if searching for warrant letters or for appearance.
	@CrtId:			Court id of the selected court category if printing for individual court hosue.
	@startListdate:	Comma separated dates selected to print letter.
	@EmpId:			Employee information who is printing the letter.
	@PrintType:		Flag to just preview the letter or to mark letters as sent. If 1 then LMS letter history will be created.
	@language:		Flag if need to print single sided (English) or double sided (both English & Spanish) letters.
	@isPrinted:		Flag to include already printed letters in the batch.
	
List of Columns:	
	LetterId:		Letter ID generated for each letter. It will be "NOT PRINTABLE"	if @PrintType = 0
	RecordId:		Identity value to look up the case in non-clients. used to group the records in report file.
	FirstName:		Person's first name that will appear on the letter
	LastName:		Person's last name that will appear on the letter
	Address:		Person's Home address that will appear on the letter
	City:			Person's Home City that will appear on the letter
	State:			Person's Home State that will appear on the letter
	FineAmount:		Fine amount for each violation that will appear on the letter
	ViolationDescritpion: violation description that will appear on the letter
	CourtName:		Name of court with which the violation is associated. Used in report file
	DPC:			Address status that is used in barcode font on the letter
	DPTwo:			Address status that is used in barcode font on the letter
	TicketNumber_pk:Case number for the violation
	ZipCode:		Person's Home Zip code.
	Language:		Flag if need to print single sided (English) or double sided (both English & Spanish) letters.
	CourtDate:		court date associated with the violation.
	Abb:			Short abbreviation of employee who is printing the letter
	ChkGroup:		Recordid plus language flag for grouping of records on the report file.
	
*******/
 
CREATE PROCEDURE [dbo].[USP_MAILER_Send_Austin_App_DayOf] 
(
    @catnum         INT = 37,
    @bondFlag       INT = 0,
    @courtid        INT = 37,
    @startListdate  VARCHAR(500) = '01/01/2013,',
    @empid          INT = 3991,
    @printtype      INT = 0,
    @language       INT = 0,	-- by default print  English letters   ,  
    @isprinted      BIT
)
AS
SET NOCOUNT ON; 
	-- DECLARING LOCAL VARIABLES FOR FILTERS...                                            
	DECLARE @officernum VARCHAR(50),
	        @officeropr VARCHAR(50),
	        @tikcetnumberopr VARCHAR(50),
	        @ticket_no VARCHAR(50),
	        @zipcode VARCHAR(50),
	        @zipcodeopr VARCHAR(50),
	        @fineamount MONEY,
	        @fineamountOpr VARCHAR(50),
	        @fineamountRelop VARCHAR(50),
	        @singleviolation MONEY,
	        @singlevoilationOpr VARCHAR(50),
	        @singleviolationrelop VARCHAR(50),
	        @doubleviolation MONEY,
	        @doublevoilationOpr VARCHAR(50),
	        @doubleviolationrelop VARCHAR(50),
	        @count_Letters INT,
	        @violadesc VARCHAR(500),
	        @violaop VARCHAR(50),
	        @LetterType INT,
	        @lettername VARCHAR(50),
	        @nextdate DATETIME
	
	-- GETTING NEXT THREE BUSINESS DAYS DATE.
	SET @nextdate = dbo.fn_getprevbusinessday(GETDATE(), 3)
	
	-- SETTING LETTER TYPE AND LETTER NAME..  
	SET @LetterType = 140
	SET @lettername = 'Austin Appearance Day Of'
	
	-- SEND LETTER TO BARRY FOR TWO WEEKS.
	DECLARE @tempDate DATETIME,
	        @mailbarry BIT
	
	SELECT @tempDate = GETDATE(),
	       @mailbarry = 1
	-- STOP SENDING LETTERS TO BARRY
	IF @tempdate >= '8/11/08'
	   AND @Tempdate <= '8/24/08'
	    SET @mailbarry = 0
	ELSE 
	IF NOT EXISTS (
	       SELECT batchid
	       FROM   tblbatchletter
	       WHERE  lettertype = @lettertype
	              AND DATEDIFF(MONTH, batchsentdate, GETDATE()) = 0
	   )
	    SET @mailbarry = 0
	ELSE
	    SET @mailbarry = 1 
	
	-- DECLARING & INITIALIZING THE VARIABLE FOR DYNAMIC SQL...                                      
	DECLARE @sqlquery VARCHAR(MAX),
	        @sqlquery2 VARCHAR(MAX),
	        @sqlquery3 VARCHAR(MAX)
	
	SELECT @sqlquery = '',
	       @sqlquery2 = '',
	       @sqlquery3 = '' 
	
	-- ASSIGNING VALUES TO THE VARIABLES
	-- BY GETTTING INFORMATION FROM LMS FILTER STRUCTURE...
	-- FOR THIS LETTER TYPE....
	SELECT @officernum = officernum,
	       @officeropr = oficernumOpr,
	       @tikcetnumberopr = tikcetnumberopr,
	       @ticket_no = ticket_no,
	       @zipcode = zipcode,
	       @zipcodeopr = zipcodeLikeopr,
	       @singleviolation = singleviolation,
	       @singlevoilationOpr = singlevoilationOpr,
	       @singleviolationrelop = singleviolationrelop,
	       @doubleviolation = doubleviolation,
	       @doublevoilationOpr = doubleviolationOpr,
	       @doubleviolationrelop = doubleviolationrelop,
	       @violadesc = violationdescription,
	       @violaop = violationdescriptionOpr,
	       @fineamount = fineamount,
	       @fineamountOpr = fineamountOpr,
	       @fineamountRelop = fineamountRelop
	FROM   tblmailer_letters_to_sendfilters
	WHERE  courtcategorynum = @catnum
	       AND lettertype = @LetterType 
	
	
	-- GETTING THE SHORT ABBREVIATION OF THE CURRENTLY LOGGED IN
	-- EMPLOYEE IN THE LOCAL VARIABLE....
	DECLARE @user VARCHAR(10)  
	SELECT @user = UPPER(abbreviation)
	FROM   tblusers
	WHERE  employeeid = @empid 
	
	-- CREATING DYNAMIC SQL FOR THE MAIN QUERY...                                      
	SET @sqlquery = @sqlquery + 
	    '  declare @recCount int    
                                  
Select distinct tva.recordid,
mdate = convert(datetime,convert(varchar(10), tva.Courtdate, 101)),  
count(tva.ViolationNumber_PK) as ViolCount,  
isnull(sum(isnull(tva.fineamount,0)),0) as Total_Fineamount   
into #temprary                                           
FROM    dallastraffictickets.dbo.tblTicketsViolationsArchive tva WITH(NOLOCK)    
INNER JOIN   
 dallastraffictickets.dbo.tblTicketsArchive ta WITH(NOLOCK)    
on  tva.recordid = ta.recordid  
INNER JOIN  dallastraffictickets.dbo.tblcourts tc  on  ta.courtid = tc.courtid  
INNER JOIN  
tblstate S ON  
ta.stateid_fk = S.stateID  

-- FIRST NAME & LAST NAME SHOULD NOT BE EMPTY..
and isnull(ta.lastname,'''') <> '''' and isnull(ta.firstname,'''') <> '''' 

-- ONLY VERIFIED AND VALID ADDRESSES
and  Flag1 in (''Y'',''D'',''S'')

-- NOT MARKED AS STOP MAILING...
and  isnull(donotmailflag,0)  =0  

-- EXCLUDE CASES TO WHICH ATTORNEY IS ASSIGNED
AND isnull(tva.AttorneyID,0) = 0

-- PERSONS ADDRESS IS NOT CHANGED IN PAST 48 MONTHS (ACCUZIP)...
and isnull(ta.ncoa48flag,0) = 0

-- EXCLUDE V TICKETS....
and charindex(''v'',tva.ticketnumber_pk)<> 1  

-- Exclude Disposed Violations
and tva.violationstatusid <> 80

--EXCLUDE CASE HAVING STATUS IN (Motion ,Motion: see problem ,Set for Court ,Work Release)
and isnull(tva.WebSiteCourtStatus,0) not in ((SELECT wscs.ID  FROM DallasTrafficTickets.dbo.WebSiteCourtStatus wscs WHERE (wscs.MailAllowed=0 AND wscs.WebSite=''DMC'')))
'
	
	
	SET @sqlquery = @sqlquery + 
	    ' 
-- SEND AMC APPEARANCE AND AMC APPEARANCE DAY OF LETTERS  ONLY TO APPEARANCE CASES...
and tva.violationstatusid = 116                                   
' 
	
	
	-- IF PRINTING FOR ALL ACTIVE COURTS OF THE SELECTED CATEGORY.....
	IF (@courtid = @CATNUM)
	    SET @sqlquery = @sqlquery + 
	        ' and tva.courtlocation In (Select courtid from dallastraffictickets.dbo.tblcourts where courtcategorynum  = '
	        + CONVERT(VARCHAR, @CATNUM) + ')' 
	        
	        -- PRINTING FOR AN INDIVIDUAL COURT OF THE SELECTED CATEGORY...
	ELSE
	    SET @sqlquery = @sqlquery + ' and tva.courtlocation=' + CONVERT(VARCHAR, @courtid) 
	
	
	IF (@startListdate = '-1')
	BEGIN
	    SET @sqlquery = @sqlquery + 
	        '   
	and datediff(day, getdate(), tva.courtdate) = 1 '
	END-- ONLY FOR THE SELECTED DATE RANGE.....
	ELSE 
	IF (@startListdate <> '')
	    SET @sqlquery = @sqlquery + '   
	and ' + dbo.Get_String_Concat_With_DatePart_ver2('' + @startListdate + '', 'tva.courtdate') 
	
	-- LMS FILTERS SECTION:
	-----------------------------------------------------------------------------------------------------------------------------------------------------
	-- IF THERE IS ANY FILTER SPECIFIED TO INCLUDE/EXCLUDE ONLY PARTICULAR OFFICER NUMBERS....
	IF (@officernum <> '')
	    SET @sqlquery = @sqlquery + '    
	and  ta.officerNumber_Fk ' + @officeropr + '(' + @officernum + ')' 
	
	-- IF THERE IS ANY FILTER DEFINED FOR INCLUDE/EXCLUDE TICKET NUMBER...
	IF (@ticket_no <> '')
	    SET @sqlquery = @sqlquery + '  
	and   (' + dbo.Get_String_Concat_With_op(
	            '' + @ticket_no + '',
	            'tva.TicketNumber_PK',
	            + '' + @tikcetnumberopr
	        ) + ')' 
	
	-- ZIP CODE FILTER......
	-- INCLUDE/EXCLUDE THE SPECIFIED ZIP CODES.....
	IF (@zipcode <> '')
	    SET @sqlquery = @sqlquery + '   
	and (' + dbo.Get_String_Concat_With_op(
	            '' + @zipcode + '',
	            'left(ta.zipcode,2)',
	            + '' + @zipcodeopr
	        ) + ')' 
	
	-- FINE AMOUNT FILTER.....
	-- INCLUDE ONLY THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT....
	IF (@fineamount <> 0 AND @fineamountOpr <> 'not')
	    SET @sqlquery = @sqlquery + '  
    and tva.fineamount' + CONVERT(VARCHAR(10), @fineamountRelop) + 
	        'Convert(Money,' + CONVERT(VARCHAR(10), @fineamount) + ')' 
	
	-- EXCLUDE THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT.....  
	IF (@fineamount <> 0 AND @fineamountOpr = 'not')
	    SET @sqlquery = @sqlquery + '  
    and  not tva.fineamount' + CONVERT(VARCHAR(10), @fineamountRelop) + 
	        'Convert(Money,' + CONVERT(VARCHAR(10), @fineamount) + ')'                                       
	
	SET @sqlquery = @sqlquery + 
	    '                                           
group by   tva.recordid,  convert(datetime,convert(varchar(10), tva.Courtdate  ,101))
having 1=1
' 
	
	-- SINGLE VIOLATION FINE AMOUNT FILTER....
	-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE SINGLE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT
	IF (@singleviolation <> 0 AND @doubleviolation = 0)
	    SET @sqlquery = @sqlquery + '
	and (  ' + @singlevoilationOpr + 
	        '  (isnull(sum(isnull(tva.fineamount,0)),0) ' + CONVERT(VARCHAR(10), @singleviolationrelop) 
	        + 'Convert(Money,' + CONVERT(VARCHAR(10), @singleviolation) + 
	        ') and count(tva.ViolationNumber_PK)=1))
      ' 
	
	-- DOUBLE VIOLATION FINE AMOUNT FILTER.....
	-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE VIOLATIONS
	-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...
	IF (@doubleviolation <> 0 AND @singleviolation = 0)
	    SET @sqlquery = @sqlquery + '
	and (' + @doublevoilationOpr + 
	        '   (isnull(sum(isnull(tva.fineamount,0)),0) ' + CONVERT(VARCHAR(10), @doubleviolationrelop) 
	        + 'convert (Money,' + CONVERT(VARCHAR(10), @doubleviolation) + 
	        ')and count(tva.ViolationNumber_PK)=2) )
      ' 
	
	-- SINGLE & DOUBLE VIOLATION FINE AMOUNT FILTER.....
	-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE VIOLATIONS
	-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...
	IF (@doubleviolation <> 0 AND @singleviolation <> 0)
	    SET @sqlquery = @sqlquery + '
	and (' + @singlevoilationOpr + '  (isnull(sum(isnull(tva.fineamount,0)),0) '
	        + CONVERT(VARCHAR(10), @singleviolationrelop) + 'Convert(Money,' + 
	        CONVERT(VARCHAR(10), @singleviolation) + 
	        ') and count(tva.ViolationNumber_PK)=1))
    and (' + @doublevoilationOpr + '  (isnull(sum(isnull(tva.fineamount,0)),0) '
	        + CONVERT(VARCHAR(10), @doubleviolationrelop) + 'convert(Money,' + 
	        CONVERT(VARCHAR(10), @doubleviolation) + 
	        ') and count(tva.ViolationNumber_PK)=2))
        ' 
	
	
	
	-- GETTING THE CLIENT'S PERSONAL & VIOLATION INFORMATION
	-- IN TEMP TABLE FOR RECORDS FILTERED IN THE ABOVE QUERY...
	SET @sqlquery = @sqlquery + 
	    ' 


-- DUE TO CHANGES IN DTP MAILERS, NOW DTP MAILER DISPLAYS VIOLATIONDATE INSETD OF COURTDATE ON APP DAY OF MAILERS
SELECT recordid, MAX(mdate) AS mdate, SUM(violCount) AS violcount, SUM(total_fineamount) AS total_fineamount
INTO #temptable
FROM #temprary
GROUP BY recordid
                                  
Select distinct a.recordid,   
 a.mdate,  
 ta.ViolationDate as courtdate, ta.courtid,  flag1 = isnull(ta.Flag1,''N'') , ta.officerNumber_Fk,   
 tva.TicketNumber_PK, isnull(ta.donotmailflag,0) as donotmailflag,  
 ta.clientflag, upper( firstname) as firstname, upper(lastname) as lastname, upper(address1) + '''' + isnull(ta.address2,'''') as address,
 upper(ta.city) as city, s.state,  
 tc.CourtName, zipcode,  midnumber,  dp2 as dptwo, dpc,  ta.ViolationDate,  violationstatusid,   
 left(zipcode,5) + rtrim(ltrim(midnumber)) as zipmid,  
 ViolationDescription, case when isnull(tva.fineamount,0)=0 then 100 else convert(numeric(10,0),tva.fineamount) end as FineAmount, 
 a.ViolCount,  
 a.Total_Fineamount   
into #temp                                            
FROM    dallastraffictickets.dbo.tblTicketsViolationsArchive tva  WITH(NOLOCK)   
inner join   
 dallastraffictickets.dbo.tblTicketsArchive ta  WITH(NOLOCK)   
on  tva.recordid = ta.recordid  
inner join  dallastraffictickets.dbo.tblcourts tc  on  ta.courtid = tc.courtid  
INNER JOIN  
tblstate S ON  
ta.stateid_fk = S.stateID
inner join #temptable a on a.recordid= ta.recordid where  1=1 

'
	
	IF (@startListdate <> '-1')
	    SET @sqlquery = @sqlquery + 
	        ' 
	-- EXCLUDE FUTURE JUDGE, JURY, SHOW CAUSE CASES.....
	and datediff(day, getdate(), tva.courtdate) >= 0
	'
	
	SET @sqlquery = @sqlquery + 
	    ' 
-- SEND AMC APPEARANCE AND AMC APPEARANCE DAY OF LETTERS  ONLY TO APPEARANCE CASES...
and tva.violationstatusid = 116
' 
	
	-- VIOLATION DESCRIPTION FILTER.....
	-- EXCLUDE THE LETTER IF SPECIFIED VIOLATION IS THE ONLY VIOLATION IN THE LETTER....
	IF (@violadesc <> '')
	BEGIN
	    -- NOT LIKE FILTER.......
	    IF (CHARINDEX('not', @violaop) <> 0)
	        SET @sqlquery = @sqlquery + ' 
		and	 not ( a.violcount =1  and (' + dbo.Get_String_Concat_With_op(
	                '' + @violadesc + '',
	                '  tva.violationdescription  ',
	                + '' + 'like'
	            ) + '))' 
	    
	    -- INCLUDE ONLY THOSE LETTER THAT SINGLE VIOLATION AND MATCHES THE SPECIFIED VIOLATION DESCRIPTION...
	    -- LIKE FILTER.......
	    IF (CHARINDEX('not', @violaop) = 0)
	        SET @sqlquery = @sqlquery + ' 
		and	 ( a.violcount =1  and (' + dbo.Get_String_Concat_With_op(
	                '' + @violadesc + '',
	                '  tva.violationdescription  ',
	                + '' + 'like'
	            ) + '))'
	END
	
	
	-- GETTING THE FINAL RESULSTS IN ANOTHER TEMP TABLE.....                 
	SET @sqlquery = @sqlquery +
	    'Select distinct recordid, violationstatusid,ViolCount,Total_Fineamount, mdate, FirstName,LastName,address,                                
 city,state,FineAmount,violationdescription,CourtName,dpc,dptwo,Flag1,officerNumber_Fk,TicketNumber_PK,                                       
 courtid,zipmid,donotmailflag,clientflag, zipcode, midnumber, courtdate,
 --TODO: GET PROMOTIONAL PRICE SCHEME...
 dbo.GetPromoPriceTemplate(zipcode,' + CONVERT(VARCHAR, @lettertype) + ') AS promotemplate
 into #temp1  from #temp                                      

-- EXCLUDE CLIENT CASES
delete from #temp1 where recordid in (
select v.recordid from dallastraffictickets.dbo.tblticketsviolations v WITH(NOLOCK) inner join dallastraffictickets.dbo.tbltickets t WITH(NOLOCK)  
on t.ticketid_pk = v.ticketid_pk and t.activeflag = 1 
inner join #temp1 a on a.recordid = v.recordid
)

-- EXCLUDE THE LETTERS THAT HAVE BEEN ALREADY PRINTED......
delete from #temp1 
from #temp1 a inner join tblletternotes n on n.recordid = a.recordid
where (	
		-- DAY OF LETTER ALREADY PRINTED...
		(n.lettertype = 140) OR

		-- ANY AUSTIN LETTER PRINTED IN PAST FIVE BUSINESS DAYS..
		(n.lettertype in (139, 141, 142) and datediff(day, n.recordloaddate, dbo.fn_getprevbusinessday(GETDATE(),5)) <=0)		
	)
 '
	
--	IF @mailbarry = 0
--	    SET @sqlquery = @sqlquery +
--	        '
--	insert into #temp1 
--	select ''1'', 116, 2, 150, ''' + CONVERT(VARCHAR(10), @tempdate, 101) + ''', ''BARRY'', ''BOBBIT'', ''7 Butterfly Place'',
--	''Austin'', ''TX'', 100, ''Printed on ' + CONVERT(VARCHAR, GETDATE()) + ''', ''TESTING'', 1,22, ''Y'', 0, ''TESTING 1'', 3140, ''78738-1343'', 0, 0, ''78738-1343'', '''', ''2/20/2008'',''0,35.00''' 
--	
	
	-- IF "MARK LETTER AS SENT" OPTION IS SELECTED....
	-- CREATE LETTER HISTORY FOR EACH PERSON....  
	IF (@printtype = 1)
	BEGIN
	    SET @sqlquery2 = @sqlquery2 +
	        '  
		Declare @ListdateVal DateTime, @totalrecs int,@count int, @p_EachLetter money,@recordid varchar(50),                                
		@zipcode   varchar(12),@maxbatch int  ,@lCourtId int  ,@dptwo varchar(10),@dpc varchar(10)  
		declare @tempBatchIDs table (batchid int)  

		-- GETTING TOTAL LETTERS AND COSTING ......
		Select @totalrecs =count(*) from #temp1 where mdate = @ListdateVal                                   
		Select @count=Count(*) from #temp1                                  

		set @p_EachLetter=convert(money,0.50)                                  

		-- DECLARING CURSOR TO INSERT THE RECORDS IN LETTER HISTORY ONE BY ONE.....
		-- THERE WILL A SEPARATE BATCH FOR EACH LIST/COURT DATE VALUE ......
		Declare ListdateCur Cursor for                                    
		Select distinct mdate  from #temp1                    
		open ListdateCur                                     
		Fetch Next from ListdateCur into @ListdateVal                                                        
		while (@@Fetch_Status=0)                                
			begin                                  

				-- GETTING TOTAL LETTERS FOR THE LIST/COURT DATE ......	                     
				Select @totalrecs =count(distinct recordid) from #temp1 where mdate = @ListdateVal                                   

				-- INSERTING RECORD IN BATCH TABLE......                               
				insert into tblBatchLetter(Empid, Lettertype,Listdate,CourtId,ParentID,LCount,PExpense) values                                    
				(' + CONVERT(VARCHAR(10), @empid) + ', ' + CONVERT(VARCHAR(10), @lettertype) 
	        + ' , @ListdateVal, ' + CONVERT(VARCHAR(10), @catnum) + 
	        ', 0, @totalrecs, @p_EachLetter)                                     

				-- GETTING BATCH ID OF THE INSERTED RECORD.....
				Select @maxbatch=Max(BatchId) from tblBatchLetter   
				insert into @tempBatchIDs select @maxbatch                                                         

				-- CREATING CURSOR TO INSERT RECORDS FOR EACH LETTER FOR THE NEWLY INSERTED BATCH....
				Declare RecordidCur Cursor for                                 
				Select distinct recordid,zipcode, courtid, dptwo, dpc from #temp1 where mdate = @ListdateVal                                 
				open RecordidCur                                                           
				Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc                                            
				while(@@Fetch_Status=0)                                
					begin                                

					-- INSERTING LETTERS IN LETTER HISTORY FOR THE SPECIFIED BATCH...					
					insert into tblLetterNotes(BatchId_Fk,listdate,ZipCode,RecordID,PCost,LetterType, courtid, dp2, dpc)                                
					values (@maxbatch,@ListdateVal,@zipcode,@recordid,@p_EachLetter,'
	        + CONVERT(VARCHAR(10), @lettertype) + 
	        ',@lCourtId, @dptwo, @dpc)                                
					Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc                            
					end                                
				close RecordidCur   
				deallocate RecordidCur                                                                 
				Fetch Next from ListdateCur into @ListdateVal                                
			end                                              

		close ListdateCur    
		deallocate ListdateCur   


		-- OUTPUTTING THE DATA TO THE APPLICATION FOR REPROT FILE.....
		Select distinct convert(varchar(20),n.noteid) as letterid, t.recordid,  t.FirstName,                                
		t.LastName,t.address,t.city, t.state,FineAmount,violationdescription,CourtName, t.dpc,                                
		dptwo, TicketNumber_PK, t.zipcode  , courtdate , ''' + @user + ''' as Abb, promotemplate                                
		into #temp2  
		from #temp1 t , tblletternotes n, @tempBatchIDs tb  
		where t.recordid = n.recordid and t.courtid = n.courtid and n.batchid_fk = tb.batchid and n.lettertype = '
	        + CONVERT(VARCHAR(10), @lettertype) + 
	        '   
		order by T.zipcode   
		select @recCount = count(distinct recordid) from #temp2   


		-- GENERATING AND SENDING EMAIL FOR LETTER PRINTING NOTIFICATION........
		declare @lettercount int, @subject varchar(200), @body varchar(400) , @user varchar(10), @sql varchar(500)  
		select @user = abbreviation from tblusers where employeeid = ' + CONVERT(VARCHAR(10), @empid) 
	        + 
	        '  
		select @lettercount = count(distinct n.noteid) from tblletternotes n, @tempBatchIDs b where n.batchid_fk = b.batchid  
		select @subject = convert(varchar(20), @lettercount) + '' ''  + ''' + @lettername + ' Letters Printed''  
		select @body = convert(varchar(20), @lettercount) + '' LETTERS ('' + CONVERt(VARCHAR(12), GETDATE()) + '') - ''+ upper( ''' + @user + ''')  
		exec usp_mailer_send_Email @subject, @body  


		-- OUTPUTING THE PDF FILE NAME TO THE APPLICATION...
		declare @batchIDs_filname varchar(500)
		set @batchIDs_filname = ''''
		select @batchIDs_filname = @batchIDs_filname + convert(varchar,batchid) + '',''  from @tempBatchIDs  
		'
	END
	ELSE  
	IF (@printtype = 0)
	   -- IF "MARK LETTER AS SENT" OPTION IS NOT SELECTED....
	   -- THEN DO NOT CREATE LETTER HISTORY. JUST OUTPUT THE DATA FOR REPORT FILE
	   -- TO DISPLAY THE LETTER...
	BEGIN
	    SET @sqlquery2 = @sqlquery2 + '  
		Select distinct ''NOT PRINTABLE'' as letterId,  recordid,  FirstName, LastName,address,city, state,FineAmount,
		violationdescription,CourtName, dpc, dptwo, TicketNumber_PK, zipcode , courtdate, ''' + @user + ''' as Abb , promotemplate
		into #temp3  
		from #temp1   
		order by zipcode   
		select @recCount = count(distinct recordid) from #temp3  
		'
	END 
	
	
	-- IF PRINTING SINGLE SIDED LETTERS....
	IF @language = 0
	BEGIN
	    -- IF ONLY PREVIEWING THE LETTER....
	    -- OUT PUT THE DATA FOR REPORT FILE...
	    IF @printtype = 0
	    BEGIN
	        SELECT @sqlquery3 = @sqlquery3 + 
	               '   
				select t1.*, 0 as language, convert(varchar(15), t1.recordid) + ''0'' as ChkGroup from #temp3 t1 order by t1.zipcode'
	    END-- IF SENDING LETTERS TO BATCH...
	       -- OUT PUT THE DATA TO REPORT FILE WITH BATCH FILE NAME..
	    ELSE 
	    IF @printtype = 1
	    BEGIN
	        SELECT @sqlquery3 = @sqlquery3 + 
	               '   
				select t2.*, 0 as language, convert(varchar(15), t2.recordid) + ''0'' as ChkGroup from #temp2 t2
				order by [zipcode] 
				
				select isnull(@batchIDs_filname,'''') as batchid 
				'
	    END
	END-- FOR DOUBLE SIDED PRINTING....
	ELSE  
	IF @language = 1
	BEGIN
	    -- IF ONLY PREVIEWING THE LETTER....
	    IF @printtype = 0
	    BEGIN
	        SELECT @sqlquery3 = @sqlquery3 + 
	               '   
				declare @table4 TABLE  (  
				[letterId] [varchar] (13)  ,  
				[recordid] [int] ,  
				[FirstName] [varchar] (20)  ,  
				[LastName] [varchar] (20)  ,  
				[address] [varchar] (100) ,  
				[city] [varchar] (50) ,  
				[state] [varchar] (2)  ,  
				[FineAmount] [money]  ,  
				[violationdescription] [varchar] (200) ,  
				[CourtName] [varchar] (50) ,  
				[dpc] [varchar] (10)  ,  
				[dptwo] [varchar] (10)  ,  
				[TicketNumber_PK] [varchar] (20)  ,  
				[zipcode] [varchar] (15) ,  
				[language] [int],  
				[courtdate] [datetime],  
				[Abb] [varchar] (10) ,
				[promotemplate] [varchar] (100)
				)  

				insert into @table4 (letterid, recordid, firstname, lastname, address, city, state, fineamount,  
				violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb, promotemplate)    

				select letterid, recordid, firstname, lastname, address, city, state, fineamount+60,  
				violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 0, courtdate, Abb, promotemplate from #temp3    

				insert into @table4 (letterid, recordid, firstname, lastname, address, city, state, fineamount,  
				violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb, promotemplate)    

				select letterid, recordid, firstname, lastname, address, city, state, fineamount+60,  
				violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 1, courtdate, Abb, promotemplate from #temp3 ;   


				select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table4 order by zipcode, language ;  
				'
	    END-- IF SENDING LETTERS TO BATCH...
	       -- OUT PUT THE DATA TO REPORT FILE WITH BATCH FILE NAME..
	    ELSE 
	    IF @printtype = 1
	    BEGIN
	        SELECT @sqlquery3 = @sqlquery3 + 
	               '   
				declare @table4 TABLE  (  
				[letterId] [varchar] (13)  ,  
				[recordid] [int] ,  
				[FirstName] [varchar] (20)  ,  
				[LastName] [varchar] (20)  ,  
				[address] [varchar] (100) ,  
				[city] [varchar] (50) ,  
				[state] [varchar] (2)  ,  
				[FineAmount] [money]  ,  
				[violationdescription] [varchar] (200) ,  
				[CourtName] [varchar] (50) ,  
				[dpc] [varchar] (10)  ,  
				[dptwo] [varchar] (10)  ,  
				[TicketNumber_PK] [varchar] (20)  ,  
				[zipcode] [varchar] (15) ,  
				[language] [int],   
				[courtdate] [datetime],  
				[Abb] [varchar] (10) ,
				[promotemplate] [varchar] (100)
				)  

				insert into @table4 (letterid, recordid, firstname, lastname, address, city, state, fineamount,  
				violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb, promotemplate)    

				select letterid, recordid, firstname, lastname, address, city, state, fineamount+60,  
				violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 0 , courtdate, Abb, promotemplate from #temp2    

				insert into @table4 (letterid, recordid, firstname, lastname, address, city, state, fineamount,  
				violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb, promotemplate)    

				select letterid, recordid, firstname, lastname, address, city, state, fineamount+60,  
				violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 1 , courtdate, Abb, promotemplate from #temp2 ;   


				select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table4   
				order by [zipcode], [language]  

				select isnull(@batchIDs_filname,'''') as batchid 
				'
	    END
	END
	
	-- EXECUTING THE DYNAMIC SQL ....
	EXEC (@sqlquery + @sqlquery2 + @sqlquery3)  
--PRINT (@sqlquery + @sqlquery2 + @sqlquery3)

GO
GRANT EXECUTE ON [dbo].[USP_MAILER_Send_Austin_App_DayOf] TO dbr_webuser
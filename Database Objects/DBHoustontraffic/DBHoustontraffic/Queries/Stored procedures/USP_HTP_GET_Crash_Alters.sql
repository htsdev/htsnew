﻿ USE TrafficTickets
GO

/****** 
Created by		:		Farrukh Iftikhar
Task ID			:		10447 
Business Logic	:		The Procedure is used to get Texas Department of Transportation Information. 
						
				
List of Parameters:	
	@downloaddate			 
	@status
	
*******/

--EXEC [dbo].[USP_HTP_GET_Crash_Alters] '09/25/2012',4

CREATE PROCEDURE [dbo].[USP_HTP_GET_Crash_Alters]
	@downloaddate DATETIME,	
	@status INT = -1
AS
BEGIN
	
SET NOCOUNT ON;

SELECT DISTINCT TPO.OperatorLastName AS LastName,
       TPO.OperatorFirstName AS FirstName,
       ISNULL(TP.Prsn_Age, '') AS Age,
       (
           CASE 
                WHEN ISNULL(TP.Prsn_Age, 0) = 0 THEN ''
                ELSE DATEPART(yy, TP.InsertDate) - ISNULL(TP.Prsn_Age, 0)
           END
       ) AS DOB,
       TC.Case_ID AS CaseId,
       TC.Crash_ID AS CrashId,
       CONVERT(VARCHAR(10),TP.InsertDate,101) AS DownloadDate,
        CONVERT(VARCHAR(10),TP.CrashDate,101) AS CrashDate 
       INTO #tempCrash
FROM   tblTexasTransportationPersonOperators TPO
       INNER JOIN tbl_Texas_Transportation_Person TP
            ON  TPO.CrashID = TP.Crash_ID
       INNER JOIN tbl_Texas_Transportation_Crash TC
            ON  TC.Crash_ID = TP.Crash_ID
            AND TC.Case_ID = TPO.IncidentNumber       
WHERE  ISNULL(TPO.OperatorFirstName, '') <> ''
       AND ISNULL(TPO.OperatorLastName, '') <> ''
	   AND DATEDIFF(DAY,TP.InsertDate,@downloaddate) = 0


IF @status = 1 --Client (F,L,YR)
BEGIN
    SELECT DISTINCT Cr.*,
		   (CASE WHEN ISNULL(CT.CaseTypeName,'') <> '' THEN 'Client' ELSE 'N/A' END ) AS [Status],
		   CT.CaseTypeName,
           E.AttorneyName
           INTO #temp1
    FROM   #tempCrash Cr
           INNER JOIN tblTickets T WITH(NOLOCK)
                ON  Cr.LastName = T.Lastname 
                AND Cr.FirstName = T.Firstname 
                AND (
                         Cr.DOB = DATEPART(YEAR, T.DOB)
                        OR (Cr.DOB - 1) = DATEPART(YEAR, T.DOB)
                        OR (Cr.DOB + 1) = DATEPART(YEAR, T.DOB)
                )
                AND ISNULL(T.Activeflag,0) = 1
           INNER JOIN tblTicketsViolations TV WITH(NOLOCK)
                ON  T.Recordid = TV.RecordID
           LEFT OUTER JOIN tblEventExtractTemp E WITH(NOLOCK)
                ON  TV.casenumassignedbycourt = E.TicketNumber
           LEFT OUTER JOIN tblCourts C
				ON  TV.CourtID = C.Courtid
		   LEFT OUTER JOIN CaseType CT
				ON C.CaseTypeID = Ct.CaseTypeId	
		   
    
END
ELSE IF @status = 2 --Client (L,YR)
BEGIN
    SELECT DISTINCT Cr.*,
		  (CASE WHEN ISNULL(CT.CaseTypeName,'') <> '' THEN 'Client' ELSE 'N/A' END ) AS [Status],
		   CT.CaseTypeName,
           E.AttorneyName
           INTO #temp2
    FROM   #tempCrash Cr
           INNER JOIN tblTickets T WITH(NOLOCK)
                ON  Cr.LastName =T.Lastname
                AND (
                         Cr.DOB = DATEPART(YEAR, T.DOB)
                        OR (Cr.DOB - 1) = DATEPART(YEAR, T.DOB)
                        OR (Cr.DOB + 1) = DATEPART(YEAR, T.DOB)
                )
                AND ISNULL(T.Activeflag,0) = 1
           INNER JOIN tblTicketsViolations TV WITH(NOLOCK)
                ON  T.Recordid = TV.RecordID
           LEFT OUTER JOIN tblEventExtractTemp E WITH(NOLOCK)
                ON   TV.casenumassignedbycourt = E.TicketNumber
           LEFT OUTER JOIN tblCourts C
				ON TV.CourtID = C.Courtid
		   LEFT OUTER JOIN CaseType CT
				ON C.CaseTypeID = Ct.CaseTypeId
    
END
ELSE IF @status = 3 --Quote
BEGIN
    SELECT DISTINCT Cr.*,
		  (CASE WHEN ISNULL(CT.CaseTypeName,'') <> '' THEN 'Quote' ELSE 'N/A' END ) AS [Status],
		   CT.CaseTypeName,	
           E.AttorneyName
           INTO #temp3
    FROM   #tempCrash Cr
           INNER JOIN tblTickets T WITH(NOLOCK)
                ON  Cr.LastName = T.Lastname 
                AND Cr.FirstName = T.Firstname 
                AND (
                         Cr.DOB = DATEPART(YEAR, T.DOB)
                        OR (Cr.DOB - 1) = DATEPART(YEAR, T.DOB)
                        OR (Cr.DOB + 1) = DATEPART(YEAR, T.DOB)
                )
                AND ISNULL(T.Activeflag,0) = 0
           INNER JOIN tblTicketsViolations TV WITH(NOLOCK)
                ON  T.Recordid = TV.RecordID
           LEFT OUTER JOIN tblEventExtractTemp E WITH(NOLOCK)
                ON TV.casenumassignedbycourt =  E.TicketNumber
           LEFT OUTER JOIN tblCourts C
				ON TV.CourtID =  C.Courtid 
		   LEFT OUTER JOIN CaseType CT
				ON C.CaseTypeID = Ct.CaseTypeId 
    
END
ELSE IF @status = 4 --Non Client
BEGIN
    SELECT DISTINCT Cr.*,
		   (CASE WHEN ISNULL(CT.CaseTypeName,'') <> '' THEN 'Non Client' ELSE 'N/A' END ) AS [Status],
			CT.CaseTypeName,
           TV.AttorneyName
           INTO #temp4
    FROM   #tempCrash Cr
           INNER JOIN tblTicketsArchive T WITH(NOLOCK)
                ON  Cr.LastName = T.Lastname 
                AND Cr.FirstName = T.Firstname 
                AND (
                         Cr.DOB = DATEPART(YEAR, T.DOB)
                        OR (Cr.DOB - 1) = DATEPART(YEAR, T.DOB)
                        OR (Cr.DOB + 1) = DATEPART(YEAR, T.DOB)
                )
                AND ISNULL(T.Clientflag,0) = 0
                AND DATEPART(YEAR,T.RecLoadDate) = 2012
           INNER JOIN tblTicketsViolationsArchive TV WITH(NOLOCK)
                ON  T.Recordid = TV.RecordID
           LEFT OUTER JOIN tblCourts C
				ON TV.CourtLocation =  C.Courtid 
		   LEFT OUTER JOIN CaseType CT
				ON C.CaseTypeID = Ct.CaseTypeId 
     
END


IF @status = 1
BEGIN
   	SELECT DISTINCT * FROM  #temp1
   	DROP TABLE #temp1
END
ELSE IF @status = 2
BEGIN
   	SELECT DISTINCT * FROM  #temp2
   	DROP TABLE #temp2
END
ELSE IF @status = 3
BEGIN
   	SELECT DISTINCT * FROM  #temp3
   	DROP TABLE #temp3
END
ELSE IF @status = 4
BEGIN
   	SELECT DISTINCT * FROM  #temp4
   	DROP TABLE #temp4
END
ELSE 
BEGIN
   	SELECT DISTINCT Cr.*,
		   (CASE WHEN ISNULL(CT.CaseTypeName,'') <> '' THEN 'Client' ELSE 'N/A' END ) AS [Status],
		   CT.CaseTypeName,
           E.AttorneyName
           INTO #temp5
    FROM   #tempCrash Cr
           LEFT OUTER JOIN tblTickets T WITH(NOLOCK)
                ON  Cr.LastName = T.Lastname 
                AND Cr.FirstName = T.Firstname 
                AND (
                         Cr.DOB = DATEPART(YEAR, T.DOB)
                        OR (Cr.DOB - 1) = DATEPART(YEAR, T.DOB)
                        OR (Cr.DOB + 1) = DATEPART(YEAR, T.DOB)
                )
                AND ISNULL(T.Activeflag,0) = 1
           LEFT OUTER JOIN tblTicketsViolations TV WITH(NOLOCK)
                ON  T.Recordid = TV.RecordID
           LEFT OUTER JOIN tblEventExtractTemp E WITH(NOLOCK)
                ON  TV.casenumassignedbycourt = E.TicketNumber
           LEFT OUTER JOIN tblCourts C
				ON  TV.CourtID = C.Courtid
		   LEFT OUTER JOIN CaseType CT
				ON C.CaseTypeID = Ct.CaseTypeId		   
    
    
    SELECT DISTINCT Cr.*,
			(CASE WHEN ISNULL(CT.CaseTypeName,'') <> '' THEN 'Client' ELSE 'N/A' END ) AS [Status],
		   CT.CaseTypeName,
           E.AttorneyName
           INTO #temp6
   FROM   #tempCrash Cr
           LEFT OUTER JOIN tblTickets T WITH(NOLOCK)
                ON  Cr.LastName =T.Lastname
                AND (
                         Cr.DOB = DATEPART(YEAR, T.DOB)
                        OR (Cr.DOB - 1) = DATEPART(YEAR, T.DOB)
                        OR (Cr.DOB + 1) = DATEPART(YEAR, T.DOB)
                )
                AND ISNULL(T.Activeflag,0) = 1
           LEFT OUTER JOIN tblTicketsViolations TV WITH(NOLOCK)
                ON  T.Recordid = TV.RecordID
           LEFT OUTER JOIN tblEventExtractTemp E WITH(NOLOCK)
                ON   TV.casenumassignedbycourt = E.TicketNumber
           LEFT OUTER JOIN tblCourts C
				ON TV.CourtID = C.Courtid
		   LEFT OUTER JOIN CaseType CT
				ON C.CaseTypeID = Ct.CaseTypeId
    
    SELECT DISTINCT Cr.*,
			(CASE WHEN ISNULL(CT.CaseTypeName,'') <> '' THEN 'Quote' ELSE 'N/A' END ) AS [Status],
		   CT.CaseTypeName,	
           E.AttorneyName
           INTO #temp7
     FROM   #tempCrash Cr
           LEFT OUTER JOIN tblTickets T WITH(NOLOCK)
                ON  Cr.LastName = T.Lastname 
                AND Cr.FirstName = T.Firstname 
                AND (
                         Cr.DOB = DATEPART(YEAR, T.DOB)
                        OR (Cr.DOB - 1) = DATEPART(YEAR, T.DOB)
                        OR (Cr.DOB + 1) = DATEPART(YEAR, T.DOB)
                )
                AND ISNULL(T.Activeflag,0) = 0
           LEFT OUTER JOIN tblTicketsViolations TV WITH(NOLOCK)
                ON  T.Recordid = TV.RecordID
           LEFT OUTER JOIN tblEventExtractTemp E WITH(NOLOCK)
                ON TV.casenumassignedbycourt =  E.TicketNumber
           LEFT OUTER JOIN tblCourts C
				ON TV.CourtID =  C.Courtid 
		   LEFT OUTER JOIN CaseType CT
				ON C.CaseTypeID = Ct.CaseTypeId 
    
    
    SELECT DISTINCT Cr.*,
			(CASE WHEN ISNULL(CT.CaseTypeName,'') <> '' THEN 'Non Client' ELSE 'N/A' END ) AS [Status],
			CT.CaseTypeName,
           TV.AttorneyName
           INTO #temp8
    FROM   #tempCrash Cr
           LEFT OUTER JOIN tblTicketsArchive T WITH(NOLOCK)
                ON  Cr.LastName = T.Lastname 
                AND Cr.FirstName = T.Firstname 
                AND (
                         Cr.DOB = DATEPART(YEAR, T.DOB)
                        OR (Cr.DOB - 1) = DATEPART(YEAR, T.DOB)
                        OR (Cr.DOB + 1) = DATEPART(YEAR, T.DOB)
                )
                AND ISNULL(T.Clientflag,0) = 0
                AND DATEPART(YEAR,T.RecLoadDate) = 2012
           LEFT OUTER JOIN tblTicketsViolationsArchive TV WITH(NOLOCK)
                ON  T.Recordid = TV.RecordID
           LEFT OUTER JOIN tblCourts C
				ON TV.CourtLocation =  C.Courtid 
		   LEFT OUTER JOIN CaseType CT
				ON C.CaseTypeID = Ct.CaseTypeId 
    
    SELECT DISTINCT * FROM #temp5
    UNION ALL SELECT DISTINCT * FROM #temp6
    UNION ALL SELECT DISTINCT * FROM #temp7
    UNION ALL SELECT DISTINCT * FROM #temp8
    
    DROP TABLE #temp5
    DROP TABLE #temp6
    DROP TABLE #temp7
    DROP TABLE #temp8
END




DROP TABLE #tempCrash

END




GO
GRANT EXECUTE ON [dbo].[USP_HTP_GET_Crash_Alters] TO dbr_webuser
GO
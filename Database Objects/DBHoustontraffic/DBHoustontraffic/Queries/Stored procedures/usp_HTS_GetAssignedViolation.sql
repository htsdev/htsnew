SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_HTS_GetAssignedViolation]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_HTS_GetAssignedViolation]
GO










CREATE PROCEDURE  usp_HTS_GetAssignedViolation

@CategoryId int
AS
Select ViolationNumber_PK,Description from tblviolations
where CategoryId=@CategoryId





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


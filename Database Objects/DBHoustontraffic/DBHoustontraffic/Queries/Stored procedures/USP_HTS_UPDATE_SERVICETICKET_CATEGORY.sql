﻿/****** Object:  StoredProcedure [dbo].[USP_DTP_Update_NonHMCFollowUpdate]  ******/     
/*        
Created By     : Fahad Muhammad Qureshi.
Created Date   : 07/25/2009  
TasK		   : 6054        
Business Logic : This procedure is used to Upadte the Service Ticket Information on the basis of category id and others.out Firm Id on then basis of Client TicketID in Billing Section.
Parameter: 
	@CategoryID : Defines the Category for the Service Ticket .
	@Description : Defines the Category Description in Wordings.
	@casetypetraffic : Integer value returns 0 OR 1 which check that Category related to Traffic case or not.
	@iscasetypecivil : Integer value returns 0 OR 1 which check that Category related to Civil case or not.
	@iscasetypecriminal : Integer value returns 0 OR 1 which check that Category related to Criminal case or not.
	@iscasetypeFamily  : Integer value returns 0 OR 1 which check that Category related to Family case or not.
	@AssignTo : this field showing that Ticket is assign 

        
*/
  
  
ALTER PROCEDURE [dbo].[USP_HTS_UPDATE_SERVICETICKET_CATEGORY]
	@CategoryID INT,
	@Description VARCHAR(50),
	@casetypetraffic INT,
	@iscasetypecivil INT,
	@iscasetypecriminal INT,
	@iscasetypeFamily INT,
	@AssignTo INT    --Fahad 6054 07/25/2009 @AssignTo parameter Added
AS					
	IF EXISTS (				--Fahad 6054 07/25/2009 Not excluded from the check
	       SELECT *
	       FROM   tblServiceTicketCategories
	       WHERE  --DESCRIPTION = @Description --Sabir Khan 7229 01/06/2010 Fixed category name issue...
	              ID = @CategoryID
	   )
	BEGIN
	    UPDATE tblServiceTicketCategories
	    SET    DESCRIPTION = @Description,
	           AssignedTo = @AssignTo --Fahad 6054 07/25/2009 @AssignTo parameter Added
	    WHERE  ID = @CategoryID
	END  
	
	IF (@casetypetraffic = 1)
	BEGIN
	    IF NOT EXISTS (
	           SELECT *
	           FROM   CaseTypeServiceCategory
	           WHERE  casetypeid = 1
	                  AND servicecategoryid = @CategoryID
	       )
	    BEGIN
	        INSERT INTO CaseTypeServiceCategory
	          (
	            casetypeid,
	            servicecategoryid
	          )
	        VALUES
	          (
	            1,
	            @CategoryID
	          ) 
	        PRINT 'traffic insert'
	    END
	    
	    PRINT 'traffic'
	END
	ELSE
	BEGIN
	    DELETE 
	    FROM   CaseTypeServiceCategory
	    WHERE  casetypeid = 1
	           AND servicecategoryid = @CategoryID
	    
	    PRINT 'traffic delete'
	END  
	
	IF (@iscasetypecivil = 1)
	BEGIN
	    IF NOT EXISTS (
	           SELECT *
	           FROM   CaseTypeServiceCategory
	           WHERE  casetypeid = 3
	                  AND servicecategoryid = @CategoryID
	       )
	    BEGIN
	        INSERT INTO CaseTypeServiceCategory
	          (
	            casetypeid,
	            servicecategoryid
	          )
	        VALUES
	          (
	            3,
	            @CategoryID
	          ) 
	        PRINT 'Civil insert'
	    END
	END
	ELSE
	BEGIN
	    DELETE 
	    FROM   CaseTypeServiceCategory
	    WHERE  casetypeid = 3
	           AND servicecategoryid = @CategoryID
	    
	    PRINT 'Civil delete'
	END  
	
	IF (@iscasetypecriminal = 1)
	BEGIN
	    IF NOT EXISTS (
	           SELECT *
	           FROM   CaseTypeServiceCategory
	           WHERE  casetypeid = 2
	                  AND servicecategoryid = @CategoryID
	       )
	    BEGIN
	        INSERT INTO CaseTypeServiceCategory
	          (
	            casetypeid,
	            servicecategoryid
	          )
	        VALUES
	          (
	            2,
	            @CategoryID
	          ) 
	        PRINT 'Criminal insert'
	    END
	END
	ELSE
	BEGIN
	    DELETE 
	    FROM   CaseTypeServiceCategory
	    WHERE  casetypeid = 2
	           AND servicecategoryid = @CategoryID
	    
	    PRINT 'Criminal delete'
	END
	
	-- Noufil 4782 Family case type added
	IF (@iscasetypeFamily = 1)
	BEGIN
	    IF NOT EXISTS (
	           SELECT *
	           FROM   CaseTypeServiceCategory
	           WHERE  casetypeid = 4
	                  AND servicecategoryid = @CategoryID
	       )
	    BEGIN
	        INSERT INTO CaseTypeServiceCategory
	          (
	            casetypeid,
	            servicecategoryid
	          )
	        VALUES
	          (
	            4,
	            @CategoryID
	          ) 
	        PRINT 'Family insert'
	    END
	END
	ELSE
	BEGIN
	    DELETE 
	    FROM   CaseTypeServiceCategory
	    WHERE  casetypeid = 4
	           AND servicecategoryid = @CategoryID
	    
	    PRINT 'Family delete'
	END
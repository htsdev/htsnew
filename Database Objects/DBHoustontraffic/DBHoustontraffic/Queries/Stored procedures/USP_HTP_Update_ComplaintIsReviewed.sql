﻿/************************************************************
 * Code formatted by SoftTree SQL Assistant © v4.0.34
 * Time: 9/1/2009 12:39:55 PM
 ************************************************************/


/****** 
Created by:		Muhammad Nasir
Task ID :		6098
Business Logic:	The procedure is used by houston to thoses comments which are review by reps.
				
List of Parameters:	
	@NoteIDs		note ids	
	
******/



Create PROCEDURE USP_HTP_Update_ComplaintIsReviewed
	@NoteIDs VARCHAR(1000),
	@EmployeeID int
AS
BEGIN
	DECLARE @EmpName VARCHAR(100)
	SELECT @EmpName=tu.UserName FROM tblUsers tu WHERE tu.EmployeeID=@EmployeeID
	
    UPDATE tblTicketsNotes
    SET    IsReviewed = 1
    WHERE  NotesID_PK IN (SELECT maincat
                          FROM   dbo.Sap_String_Param(@NoteIDs))
    
    
    DECLARE @temp TABLE (id INT IDENTITY(1, 1) NOT NULL, ticketid INT)
    INSERT INTO @temp
      (
        ticketid
      )(
           SELECT TicketID
           FROM   tblTicketsNotes
           WHERE  NotesID_PK IN (SELECT maincat
                                 FROM   dbo.Sap_String_Param(@NoteIDs))
       )             
    
    DECLARE @count INT
    DECLARE @item INT 
    DECLARE @TicketID INT 
    
    SET @item = 1
    SELECT @count = COUNT(*)
    FROM   @temp
    
    WHILE @count >= @item
    BEGIN
        SELECT @TicketID = ticketid
        FROM   @temp
        WHERE  id = @item
        
        INSERT INTO tblticketsnotes
          (
            TicketID,
            [Subject],
            EmployeeID,
            Recdate
          )
        VALUES
          (
            @TicketID,
            @EmpName + ' has reviewed the complaint',
            @EmployeeID,
            GETDATE()
          )
        SET @item = @item + 1
    END
END
GO


 GRANT EXECUTE ON dbo.USP_HTP_Update_ComplaintIsReviewed TO dbr_webuser
 GO
 
 
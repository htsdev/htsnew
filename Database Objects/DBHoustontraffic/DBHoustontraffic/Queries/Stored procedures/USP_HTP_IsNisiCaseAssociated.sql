USE TrafficTickets
GO
/****** 
Create by:  Rab Nawaz
Date:		01/03/2012

Business Logic : The stored procedure is to get information about the 
				 NISI cases of particular client

List of Parameters:	
	@TicketId : Identification of case to find the NISI cases through cause number 

List of Columns: 	
	Column0: Returns true if find NISI in the cause number else return false

******/


CREATE PROCEDURE USP_HTP_IsNisiCaseAssociated
(
	@TicketId AS INT
)
AS
BEGIN
	IF EXISTS (SELECT ttv.TicketID_PK
	           FROM tblTicketsViolations ttv 
	           WHERE ttv.TicketID_PK = @TicketId 	           
	           AND CHARINDEX('NISI', ISNULL (ttv.casenumassignedbycourt, '')) > 0  -- Should contain NISI in casenumassignedbycourt
	)
	BEGIN
		SELECT CONVERT(BIT, 1) 
	END
		
	
	ELSE
		BEGIN
			SELECT CONVERT(BIT, 0)
		END
END

GO
GRANT EXECUTE ON dbo.USP_HTP_IsNisiCaseAssociated TO dbr_webuser
GO

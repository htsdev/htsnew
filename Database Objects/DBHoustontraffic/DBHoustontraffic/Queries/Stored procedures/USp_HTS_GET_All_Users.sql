﻿/*        
Created By     : Fahad Muhammad Qureshi.
Created Date   : 07/27/2009  
TasK		   : 6054        
Business Logic : This procedure is used to fetching out EmployeeID and UserName Data for All Users.
     
*/

ALTER Procedure dbo.USp_HTS_GET_All_Users  
as 
--Sabir Khan 6670 10/02/2009 Getting First Name and Last Name of the users instead of User Name... 
--select Employeeid , upper(UserName)as UserName  from tblusers where username is not null and status = 1   
select Employeeid , upper(Firstname) + ' ' + UPPER(Lastname) as UserName  from tblusers where username is not null and status = 1   


  
  


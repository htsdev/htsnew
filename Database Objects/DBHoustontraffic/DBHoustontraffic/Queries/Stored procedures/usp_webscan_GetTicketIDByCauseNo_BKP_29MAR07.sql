SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_webscan_GetTicketIDByCauseNo_BKP_29MAR07]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_webscan_GetTicketIDByCauseNo_BKP_29MAR07]
GO

  
CREATE procedure [dbo].[usp_webscan_GetTicketIDByCauseNo_BKP_29MAR07]      
      
@CauseNo varchar(50)      
      
as      
      
select tv.ticketid_pk as TicketID,tv.courtdatemain,tv.courtid,cvs.categoryid       
from tblticketsviolations  tv inner join    
 tblcourtviolationstatus cvs on     
 cvs.courtviolationstatusid=tv.courtviolationstatusidmain    
where casenumassignedbycourt=@CauseNo      
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


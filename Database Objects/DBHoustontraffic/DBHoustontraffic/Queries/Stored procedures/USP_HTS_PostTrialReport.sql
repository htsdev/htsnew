SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_PostTrialReport]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_PostTrialReport]
GO

                           
CREATE PROCEDURE [dbo].[USP_HTS_PostTrialReport] -- '12/11/2006','','',0                                              
@CourtDate datetime ,                                               
@CourtName varchar(20),                            
@Status varchar(20),                          
@ShowClosedDocket int                            
AS                                                
                              
                            
Declare @Query varchar(8000)                  
                  
set @query = ' declare @temp1 table (
 ticketid_pk int,
 lastname varchar(50) ,
 firstname varchar(50),
 bondflag tinyint,
 courtdateSnapShot datetime,
 courtnumber int,
 shortdescription varchar(20),
 courtname varchar(100),
 trialcomments varchar(1000),
 Midnum varchar(20),
 ticketsViolationId int,
 courtnamecomplete varchar(100),
 lastupdate varchar(50),
 recordid int,
 courtid int,
 shortname varchar(20),
 causeno varchar(30),
 ticketnumber varchar(30),
 autostatus varchar(100),
 autostatusid int,
 autocourtdate datetime,
 autocourtnumber int,
 verifiedstatus varchar(100),
 courtdatemain datetime,
 verifiedcourtnumber int,
 verifiedstatusid int,
 snapshotstatus varchar(100),
 Discrepency bit ,
 AutoCategoryId int,
 VerifiedCategoryId int ,
 ContactNo  varchar(5000),
 Comments varchar(8000),
 showquestionmark bit
 )
 
 insert into @temp1 ( ticketid_pk, lastname, firstname,  bondflag, courtdatesnapshot, courtnumber,
    shortdescription, courtname, trialcomments, midnum, ticketsviolationid, courtnamecomplete, lastupdate,
    recordid, courtid, shortname, causeno, ticketnumber, autostatus, autostatusid, autocourtdate, autocourtnumber,
    verifiedstatus, courtdatemain, verifiedcourtnumber, verifiedstatusid, snapshotstatus,Discrepency, AutoCategoryId, VerifiedCategoryId, ContactNo , Comments, showquestionmark)

 SELECT s.TicketID_PK, s.LastName as Lastname,
   s.FirstName,  s.BondFlag, s.CourtDate , Convert(int,s.CourtNumber) ,
   s.ShortDescription,
   s.CourtName,
   s.TrialComments,
   s.Midnum,
   s.TicketsViolationID,
   s.CourtName + ''-''+isnull(tc.address,'' '') +''-'' +CONVERT(varchar, s.CourtDate, 101) as CourtNameComplete,
   isnull(s.LastUpdate,'' '') as LastUpdate,
   s.RecordID,
   s.CourtID,
   tc.shortname,
 tv.casenumassignedbycourt as causeno,
 tv.refcasenumber as ticketnumber,
tblCourtViolationStatus_1.ShortDescription +'' ''+ CONVERT(varchar, TV.CourtDate,101) + '' '' + SUBSTRING(CONVERT(varchar, TV.CourtDate), 13, 7) + '' '' + case
when  isnumeric(isnull(TV.CourtNumber,'''')) = 1 then convert(varchar(4),convert(int,TV.CourtNumber))
when  isnumeric(isnull(TV.CourtNumber,'''')) = 0 then convert(varchar(4),isnull(TV.CourtNumber,''''))
end  AS AutoStatus,
   tv.courtviolationstatusid, tv.courtdate, tv.courtnumber,
tblCourtViolationStatus_2.ShortDescription + '' '' + CONVERT(varchar, TV.CourtDateMain, 101) + '' '' + SUBSTRING(CONVERT(varchar,TV.CourtDateMain), 13, 7) + '' '' + case
when  isnumeric(isnull(TV.CourtNumbermain,'''')) = 1 then convert(varchar(4),convert(int,TV.CourtNumbermain))
when  isnumeric(isnull(TV.CourtNumbermain,'''')) = 0 then convert(varchar(4),isnull(TV.CourtNumbermain,''''))
end  AS VerifiedStatus,
tv.courtdatemain, tv.courtnumbermain,  tv.courtviolationstatusidmain,
s.ShortDescription + '' '' + CONVERT(varchar, s.CourtDate, 101) + '' '' + SUBSTRING(CONVERT(varchar,s.CourtDate), 13, 7) + '' '' + case
when  isnumeric(isnull(s.CourtNumber,'''')) = 1 then convert(varchar(4),convert(int,s.CourtNumber))
when  isnumeric(isnull(s.CourtNumber,'''')) = 0 then convert(varchar(4),isnull(s.CourtNumber,''''))
end  AS SnapShotStatus,
dbo.Fn_CheckDiscrepency(s.TicketsViolationID) ,
tblCourtViolationStatus_1.categoryid, tblCourtViolationStatus_2.categoryid ,
isnull(dbo.fn_FormatPhoneNumber(tct1.Description,tt.Contact1) + ''<BR>'' +dbo.fn_FormatPhoneNumber(tct2.Description,tt.Contact2) + ''<BR>'' + dbo.fn_FormatPhoneNumber(tct3.Description,tt.Contact3),'''') as ContactNo,
isnull(s.Comments,''''),
0 as showquestionmark
from tbl_HTS_DocketCloseOut_Snapshot  s
join tblcourts tc
on tc.courtid = isnull(s.courtId,3001)
join tbltickets tt
on tt.TicketID_pk = s.TicketID_pk
join tblticketsviolations tv
on tv.ticketsviolationid = s.ticketsviolationid
left outer join tblcontactstype tct1
on tt.contacttype1 = tct1.contacttype_pk
left outer join tblcontactstype tct2
on tt.contacttype2 = tct2.contacttype_pk
left outer join tblcontactstype tct3
on tt.contacttype3 = tct3.contacttype_pk
LEFT OUTER JOIN
dbo.tblCourtViolationStatus tblCourtViolationStatus_1 ON
TV.CourtViolationStatusID = tblCourtViolationStatus_1.CourtViolationStatusID
LEFT OUTER JOIN
dbo.tblCourtViolationStatus tblCourtViolationStatus_2 ON
TV.CourtViolationStatusIDmain = tblCourtViolationStatus_2.CourtViolationStatusID
where DateDiff(day,s.CourtDate,'''+Convert(varchar(10),@CourtDate,101)+''')=0

update @temp1 set showquestionmark = 1
where courtdatemain < getdate() and VerifiedCategoryId not in (50,12,7)
'                           

-- Filtering Records On Searching Criteria                   
Set @Query = @Query + ' Select * Into #temp2 from @temp1 where 1=1'                  
                  
if @CourtName !=''                          
 Set @Query = @Query + ' and ShortName='''+@CourtName + ''''                          
                   
if @Status = '1'                          
 set @Query = @Query + ' and RTrim(Substring(AutoStatus,0,4)) in (''DLQ'',''FTA'')'                           
                  
else if @Status = '2'                          
 --Set @Query = @Query + ' and RTrim(Substring(AutoStatus,0,4)) in (''ARR'',''PRE'',''JUR'',''JUD'',''WAIT'') and autocourtdate  > getdate()'                           
 Set @Query = @Query + ' and AutoCategoryId in (1,2,3,4,5) and autocourtdate  > getdate()'                           
                  
else if @Status = '3'                          
 --Set @Query = @Query + ' and RTrim(Substring(AutoStatus,0,4)) in (''DISP'')'                           
 Set @Query = @Query + ' and AutoCategoryId = 50'                    
                  
else if @Status = '4' --i.e  Show All NIR Cases                          
 --Set @Query = @Query + ' and RTrim(Substring(AutoStatus,0,4)) Not in (''ARR'',''PRE'',''JUR'',''JUD'',''WAIT'') and autocourtdate > getdate()'                           
 Set @Query = @Query + ' and AutoCategoryId Not in (1,2,3,4,5,50) and autocourtdate > getdate()'                           
                      
else if @Status = '5'                          
 Set @Query = @Query + '  and datediff(day,  isnull(autocourtdate,''01/01/1900'') , ''01/01/1900'') = 0       '                    
                  
if @ShowClosedDocket = 1                           
 Set @Query = @Query + ' and ( autoCourtNumber != verifiedCourtNumber  Or autoCourtDate ! = CourtDatemain or autoCourtNumber != verifiedCourtNumber)'                        
--Set @Query = @Query + ' and Discrepency = 1 '                        
                  
Set @Query = @Query + '
 
Select  ticketid_pk,  lastname, firstname, bondflag, courtdatesnapshot, courtnumber,
    shortdescription, courtname, trialcomments, midnum, ticketsviolationid, courtnamecomplete, lastupdate,
    recordid,courtid, shortname, causeno, ticketnumber, autostatus, autostatusid, autocourtdate, autocourtnumber,
    verifiedstatus, courtdatemain, verifiedcourtnumber, verifiedstatusid, snapshotstatus,Discrepency , ContactNo, Comments, showquestionmark
 from #temp2 where shortdescription In (''ARR'',''PRE'',''JUR'',''JUR2'',''JUD'',''BF'',''A/W'',''TRI'')
 order by courtid, courtnumber, courtdatesnapshot, lastname
 
'                   
                
Set @Query = @Query + ' Select * from #temp2 where shortdescription Not In (''ARR'',''PRE'',''JUR'',''JUR2'',''JUD'',''BF'',''A/W'',''TRI'')
and shortdescription Not In(''BOND'',''COM'',''JUR2'',''DSCD'',''DAR'',''DPX'',''CLO'',''B/W'',''WAIT'')
order by  courtid , courtnumber, courtdatesnapshot ,lastname drop table #temp2'

--print  @Query                     
exec ( @Query)                   

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


 /********************************************************************      
Created By     : Syed Muhammad Ozair  
Modified Date   : 08/18/2009        
Business Logic : This procedure is used in Misc Reports which displays the summary of mailers sent for specific date range.  
  
Parameter:             
 @StartDate,  
 @EndDate  
  
Columns:  
 MailerDate,   
 MailerCount,  
 MailerScanned,  
 MailerLost,  
 PendingCount,  
 ReturnedCount,  
 DeliveredCount,  
 Unknown    
   
*********************************************************************/
ALTER PROCEDURE [dbo].[usp_HTP_GET_MailerUSPSReport]  
 @StartDate DATETIME,  
 @EndDate DATETIME  ,
 @LetterType int = 55   
 AS  


-- TAHIR 7917 07/23/2010 REDISGNED THE STORED PROCEDURE FOR PERFORMANCE TUNING

-- GET ALL MAILERS DATA PRINTED DURING THE SPECIFIED DATE RANGE....
SELECT CONVERT(DATETIME, CONVERT(VARCHAR(10), currentdate, 101)) AS currentdate, noteid
INTO #Mailers
FROM tblletternotes   
WHERE LetterType = @lettertype
AND DATEDIFF(DAY, CurrentDate, @startdate ) <= 0
AND DATEDIFF(DAY, CurrentDate, @enddate) >= 0   

-- GET ALL PACKAGE DATA FOR THE MAILERS PRINTED DURING SPECIFIED DATE RANGE...
SELECT p.* INTO #PackageData 
FROM MailerUSPSInfo.dbo.USPSPackageData p
INNER JOIN #Mailers a ON a.noteid = p.LetterID

-- CREATING INDEX ON TEMP TABLE FOR PERFORMANCE   
BEGIN TRY   

	CREATE NONCLUSTERED INDEX IX_Mailers_LetterId ON #Mailers
	(LetterID) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

	CREATE NONCLUSTERED INDEX IX_PackageData_LetterId ON #PackageData
	(LetterID) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	
END TRY

BEGIN CATCH
	-- DO NOTHING
END CATCH
	

CREATE TABLE #temp (
	MailerDate DATETIME, MailerCount INT, 
	MailerScanned INT,MailerLost INT
	)   

-- GETTING COUNT FOR MAILERS PRINTED AND SCANNED   
insert INTO #temp (mailerdate, mailercount, MailerScanned)   
SELECT	CONVERT(VARCHAR(10), a.currentdate, 101), 
		COUNT(DISTINCT a.noteid),
		COUNT (DISTINCT u.letterid)   
FROM #Mailers a 
LEFT OUTER JOIN MailerUSPSInfo.dbo.uspspackagedata u 
ON u.letterid = a.NoteId  
GROUP BY CONVERT(VARCHAR(10), a.currentdate, 101)


-- GETTING COUNT FOR MAILERS LOST
UPDATE #temp SET mailerlost = ISNULL(mailercount,0) - ISNULL(mailerscanned,0)

-- GETTING PENDING COUNT
SELECT CONVERT(VARCHAR(10), a.currentdate, 101) AS mailerdate,
COUNT(DISTINCT t1.letterid) AS PendingCount
INTO #PendingCount
FROM   #PackageData t1
INNER JOIN #Mailers a ON a.noteid = t1.LetterID
AND  NOT EXISTS 
	(
		SELECT id FROM #PackageData
		WHERE LetterID = t1.LetterID
		AND OperationCode IN (
				'828', '878', '898', '908','912', '914','918', '146','266', '276','286', '336',
				'366', '376','396', '406','416', '426','446', '466','486', '496','506', '806', 
				'816', '826', '829', '836','846', '856','866', '876','879', '886','896', '899', 
				'905', '906','909', '911','913', '915','919', '966','976', '091', '099'
				)
	)
GROUP BY CONVERT(VARCHAR(10), a.currentdate, 101)
                

-- GETTING COUNT FOR 091 - RETURNED
SELECT CONVERT(VARCHAR(10), a.currentdate, 101) AS mailerdate,
	COUNT(DISTINCT t1.letterid) AS ReturnedCount_091
INTO #ReturnedCount_091
FROM   #PackageData t1
INNER JOIN #Mailers a ON a.noteid = t1.LetterID
AND  EXISTS 
	(
		SELECT m.id FROM #PackageData m
		WHERE m.LetterID = t1.LetterID
		AND m.OperationCode = '091'
		AND NOT EXISTS 
			(
				SELECT o.id FROM #PackageData o
				WHERE o.LetterID = m.LetterID
				AND o.OperationCode = '099'
			)	
	)
GROUP BY CONVERT(VARCHAR(10), a.currentdate, 101)              

-- GETTING COUNT FOR 099 - RETURNED
SELECT CONVERT(VARCHAR(10), a.currentdate, 101) AS mailerdate,
	COUNT(DISTINCT t1.letterid) AS ReturnedCount_099
INTO #ReturnedCount_099
FROM   #PackageData t1
INNER JOIN #Mailers a ON a.noteid = t1.LetterID
AND  t1.OperationCode = '099'
GROUP BY CONVERT(VARCHAR(10), a.currentdate, 101)   

-- GETTING COUNT FOR 1ST PASS DELIVERED
SELECT CONVERT(VARCHAR(10), a.currentdate, 101) AS mailerdate,
	COUNT(DISTINCT t1.letterid) AS First_Pass_DeliveredCount
INTO #First_Pass_DeliveredCount
FROM   #PackageData t1
INNER JOIN #Mailers a ON a.noteid = t1.LetterID
AND  EXISTS 
	(
		SELECT m.id FROM #PackageData m
		WHERE m.LetterID = t1.LetterID
		AND m.OperationCode IN ('828', '878', '898', '908', '912', '914', '918')
		AND NOT EXISTS 
				(
				SELECT o.id FROM #PackageData o
				WHERE o.LetterID = t1.LetterID
				AND o.OperationCode IN (
						'146','266','276','286','336','366','376','396', 
						'406','416','426','446','466','486','496','506','806','816','826','829','836','846','856', 
						'866', '876','879','886','896','899','905','906','909','911','913','915','919','966','976'
						)
				)
		AND NOT EXISTS 
				(
				SELECT p.id FROM #PackageData p
				WHERE p.LetterID = t1.LetterID
				AND p.OperationCode IN ('091','099')
				)				
	)
GROUP BY CONVERT(VARCHAR(10), a.currentdate, 101)   

-- GETTING COUNT FOR 2ND PASS DELIVERED
SELECT CONVERT(VARCHAR(10), a.currentdate, 101) AS mailerdate,
	COUNT(DISTINCT t1.letterid) AS Second_Pass_DeliveredCount
INTO #Second_Pass_DeliveredCount
FROM   #PackageData t1
INNER JOIN #Mailers a ON a.noteid = t1.LetterID
AND  EXISTS 
	(
		SELECT m.id FROM #PackageData m 
		WHERE m.LetterID = t1.LetterID
		AND m.OperationCode IN 
				(
				'146', '266','276', '286','336', '366','376', '396','406', '416','426', '446', 
				'466', '486','496', '506','806', '816','826', '829','836', '846','856', '866', 
				'876', '879','886', '896','899', '905','906', '909','911', '913','915', '919', 
				'966', '976'
				)
		AND NOT EXISTS 
				(
				SELECT p.id FROM #PackageData p
				WHERE p.LetterID = t1.LetterID
				AND p.OperationCode IN ('091','099')
				)				
	)
GROUP BY CONVERT(VARCHAR(10), a.currentdate, 101)       

-- SUMMARIZING THE FIGURES     
SELECT	a.MailerDate, 
		isnull(a.MailerCount,0) AS MailerCount, 
		isnull(a.MailerScanned,0) AS MailerScanned, 
		isnull(a.MailerLost,0) AS MailerLost, 
		isnull(b.pendingcount,0) AS PendingCount, 
		isnull(c.ReturnedCount_091,0) AS ReturnedCount_091, 
		isnull(d.ReturnedCount_099,0) AS ReturnedCount_099,
		isnull(e.First_Pass_DeliveredCount,0) AS First_Pass_DeliveredCount, 
		isnull(f.Second_Pass_DeliveredCount,0) AS Second_Pass_DeliveredCount
INTO #Result		
FROM #temp a
LEFT OUTER JOIN #PendingCount b ON DATEDIFF(DAY, a.MailerDate, b.mailerdate) =0 
LEFT OUTER JOIN #ReturnedCount_091 c  ON DATEDIFF(DAY, a.MailerDate, c.mailerdate) =0 
LEFT OUTER JOIN #ReturnedCount_099 d ON DATEDIFF(DAY, a.MailerDate, d.mailerdate) =0 
LEFT OUTER JOIN #First_Pass_DeliveredCount e ON DATEDIFF(DAY, a.MailerDate, e.mailerdate) =0
LEFT OUTER JOIN #Second_Pass_DeliveredCount f ON DATEDIFF(DAY, a.MailerDate, f.mailerdate) =0
 
-- GETTING FINAL RESULTS FOR THE REPORT
SELECT	CONVERT(VARCHAR(10),MailerDate,101) AS MailerDate, 
		MailerCount, 
		MailerScanned, 
		MailerLost, 
		pendingcount, 
		ReturnedCount_091 AS [091_ReturnedCount], 
		ReturnedCount_099 AS [099_ReturnedCount],
		First_Pass_DeliveredCount AS [1st_Pass_DeliveredCount], 
		Second_Pass_DeliveredCount AS [2nd_Pass_DeliveredCount],
		UnKnown = ISNULL( MailerScanned - ( pendingcount + ReturnedCount_091 + 
											ReturnedCount_099 + First_Pass_DeliveredCount + Second_Pass_DeliveredCount
											),0)
FROM #Result
ORDER BY MailerDate

BEGIN TRY
	DROP TABLE #temp  
	DROP TABLE #Mailers
	DROP TABLE #PackageData
	DROP TABLE #PendingCount  
	DROP TABLE #ReturnedCount_091  
	DROP TABLE #ReturnedCount_099
	DROP TABLE #First_Pass_DeliveredCount
	DROP TABLE #Second_Pass_DeliveredCount
	DROP TABLE #Result
END TRY
BEGIN CATCH
	-- DO NOTHING
END CATCH
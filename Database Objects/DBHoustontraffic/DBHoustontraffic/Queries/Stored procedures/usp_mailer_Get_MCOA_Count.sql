SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_mailer_Get_MCOA_Count]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_mailer_Get_MCOA_Count]
GO


--  usp_mailer_Get_MCOA_Count 1, 9, 1, '11/15/2006'
CREATE Procedure [dbo].[usp_mailer_Get_MCOA_Count]
	(
	@catnum		int,
	@LetterType 	int,
	@crtid 		int,                                                 
	@startListdate	datetime,
	@endListdate	datetime,
	@SearchType  int = 0                                    
	)

as
declare	@officernum varchar(50), 
	@officeropr varchar(50), 
	@tikcetnumberopr varchar(50), 
	@ticket_no varchar(50), 
	@zipcode varchar(50), 
	@zipcodeopr varchar(50), 
	@fineamount money,
	@fineamountOpr varchar(50) ,
	@fineamountRelop varchar(50),
	@singleviolation money, 
	@singlevoilationOpr varchar(50) , 
	@singleviolationrelop varchar(50), 
	@doubleviolation money,
	@doublevoilationOpr varchar(50) , 
	@doubleviolationrelop varchar(50), 
	@count_Letters int,
	@violadesc varchar(500),
	@violaop varchar(50),
	@sqlquery nvarchar(4000)  ,
	@sqlquery2 nvarchar(4000)  ,
	@sqlParam nvarchar(1000)


select @sqlparam = '@catnum int, @LetterType int, @crtid int, @startListdate datetime, @endListdate datetime, @SearchType int '

                                                             
                                                      
select 	@sqlquery ='',
	@sqlquery2 = '',
	@endListdate = @endListdate + ' 23:59:59.998'
                                                              
Select	@officernum=officernum,
	@officeropr=oficernumOpr,
	@tikcetnumberopr=tikcetnumberopr,
	@ticket_no=ticket_no,
	@zipcode=zipcode,
	@zipcodeopr=zipcodeLikeopr,
	@singleviolation =singleviolation,
	@singlevoilationOpr =singlevoilationOpr, 
	@singleviolationrelop =singleviolationrelop, 
	@doubleviolation = doubleviolation,
	@doublevoilationOpr=doubleviolationOpr,
	@doubleviolationrelop=doubleviolationrelop,
	@violadesc=violationdescription,
	@violaop=violationdescriptionOpr , 
	@fineamount=fineamount ,
	@fineamountOpr=fineamountOpr ,
	@fineamountRelop=fineamountRelop  
from 	tblmailer_letters_to_sendfilters  
where 	courtcategorynum=@catnum 
and 	Lettertype=@LetterType 
                                                      
set 	@sqlquery=@sqlquery+'                                          
 Select distinct                                                         
 convert(datetime , convert(varchar(10), isnull(ta.mcoa_updatedate, ''01/01/1900'')  ,101)) as mdate, 
 flag1 = isnull(ta.Flag1,''N'') ,                                                      
 ta.donotmailflag, ta.recordid,                                                    
 count(tva.ViolationNumber_PK) as ViolCount,                                                      
 sum(tva.fineamount)as Total_Fineamount
into #temp                                                            
FROM dbo.tblCourtViolationStatus tcvs                      
INNER JOIN dbo.tblTicketsViolationsArchive TVA                       
ON tcvs.CourtViolationStatusID = TVA.violationstatusid INNER JOIN                       
dbo.tblTicketsArchive TA ON TVA.recordid = TA.recordid
--and ta.listdate between @startlistdate and @endlistdate
and datediff(day, ta.mcoa_updatedate, @startlistdate) <=0
and datediff(day, ta.mcoa_updatedate, @endlistdate )>= 0
and tcvs.CategoryID=2 
and ta.clientflag=0 
and isnull(ta.ncoa48flag,0) = 0
'
  
if(@crtid=1  )                                    
	set @sqlquery=@sqlquery+' and ta.courtid In (Select courtid from tblcourts where courtcategorynum = @crtid )'                                        

if( @crtid >= 3000 )                                    
	set @sqlquery =@sqlquery +' and ta.courtid = @crtid' 
                                                        
	                                                
                                                      
if(@officernum<>'')                                                        
	set @sqlquery =@sqlquery + '  and ta.officerNumber_Fk '  + @officeropr  +'('+ @officernum+')'                                                         
                                                       
if(@ticket_no<>'')                        
	set @sqlquery=@sqlquery + ' and ('+  dbo.Get_String_Concat_With_op(''+@ticket_no+'' , 'tva.TicketNumber_PK' ,+ ''+ @tikcetnumberopr) +')'                                                              
                                    
if(@zipcode<>'')                                                                              
	set @sqlquery=@sqlquery+ ' and ('+ dbo.Get_String_Concat_With_op(''+@zipcode+'' , 'left(ta.zipcode,3)' ,+ ''+ @zipcodeopr) +')'                                   

if(@violadesc<>'')            
	set @sqlquery=@sqlquery+ ' and ('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ @violaop) +')'
                                                      
set @sqlquery =@sqlquery+ '                          
and ta.recordid not in (                                                                    
	select	recordid from tblletternotes 
	where 	lettertype =@LetterType   
--	and    	datediff(day, listdate, ta.listdate) = 0
	)'


set @sqlquery =@sqlquery+ ' group by                                                       
 convert(datetime , convert(varchar(10), isnull(ta.mcoa_updatedate, ''01/01/1900'')  ,101)),
  ta.Flag1,      
  ta.donotmailflag    , ta.recordid                                                  
'                                  


if(@fineamount<>0 and @fineamountOpr<>'not'  )                                  
	set @sqlquery =@sqlquery+ '
        having sum(tva.fineamount)'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                                       
                                                      
set @sqlquery=@sqlquery + ' 
Select distinct  ViolCount, Total_Fineamount, mdate ,Flag1, donotmailflag, recordid into #temp1  from #temp                                                          
 '                                  
if(@singleviolation<>0 and @doubleviolation=0)                                  
	set @sqlquery =@sqlquery+ 'Where  '+ @singlevoilationOpr+                                                    
      '  (Total_FineAmount '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and ViolCount=1)                                  
         or violcount>3                                                               
      '                                  
                                  
                                       
if(@doubleviolation<>0 and @singleviolation=0 )                                  
begin                                  

set @sqlquery=@sqlquery+                                         
'Select distinct   mdate ,  flag1, donotmailflag, recordid into #temp1  from #temp                                                        
 '
                                  
set @sqlquery =@sqlquery + 'where   '+ @doublevoilationOpr+                                                         
       '   (Total_FineAmount '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ')and ViolCount=2)                                                        
      or violcount>3 '                                                      
end                              
                                  
if(@doubleviolation<>0 and @singleviolation<>0)                                  
begin                                  
set @sqlquery =@sqlquery+ 'Where  '+ @singlevoilationOpr+                                                        
      '  (Total_FineAmount '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and ViolCount=1)                                  
         or   '                                  
set @sqlquery =@sqlquery +  @doublevoilationOpr+                                                         
       '   (Total_FineAmount '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ')and ViolCount=2)                                                        
      or violcount>3 '                                    
                                  
end                       


set @sqlquery2=@sqlquery2 +'
Select count(distinct recordid) as Total_Count, mdate 
into #temp2 
from #temp1 
group by mdate 
  
Select count(distinct recordid) as Good_Count, mdate 
into #temp3
from #temp1 
where Flag1 in (''Y'',''D'',''S'')   
and donotmailflag = 0    
group by mdate   
                                                      
Select count(distinct recordid) as Bad_Count, mdate  
into #temp4  
from #temp1 
where Flag1  not in (''Y'',''D'',''S'') 
and donotmailflag = 0 
group by mdate 
                   
Select count(distinct recordid) as DonotMail_Count, mdate  
into #temp5   
from #temp1 
where donotmailflag=1 
group by mdate'
	
         
set @sqlquery2 = @sqlquery2 +'                                                        
Select   #temp2.mdate as listdate,
        Total_Count,
        isnull(Good_Count,0) Good_Count,  
        isnull(Bad_Count,0)Bad_Count,  
        isnull(DonotMail_Count,0)DonotMail_Count  
from #Temp2 left outer join #temp3   
on #temp2.mdate=#temp3.mdate  
left outer join #Temp4  
on #temp2.mdate=#Temp4.mdate    
left outer join #Temp5  
on #temp2.mdate=#Temp5.mdate                                                       
where Good_Count > 0 order by #temp2.mdate desc

Select count(distinct recordid) from #temp1 
where Flag1 in (''Y'',''D'',''S'') and donotmailflag = 0  

drop table #temp drop table #temp1 drop table #temp2  drop table #temp3  drop table #temp4  drop table #temp5 
'                                                      
                                                      
--print @sqlquery  + @sqlquery2                                       
--exec( @sqlquery)
set @sqlquery = @sqlquery + @sqlquery2
exec sp_executesql @sqlquery , @sqlparam, @catnum, @LetterType, @crtid, @startListdate, @endListdate, @SearchType



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


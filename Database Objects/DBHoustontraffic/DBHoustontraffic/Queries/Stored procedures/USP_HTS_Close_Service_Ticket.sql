SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Close_Service_Ticket]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Close_Service_Ticket]
GO

Create procedure dbo.USP_HTS_Close_Service_Ticket --125136          
(          
@ticketid int, 
@Fid int          
)          
as          
          
update tblticketsflag           
Set           
ContinuanceStatus = 100 ,      
ClosedDate = getdate(),    
DeleteFlag = 1          
where Ticketid_pk = @ticketid 
and Flagid = 23          
--and Fid = ( Select top 1 Fid from tblticketsflag where Ticketid_pk = @ticketid  and Flagid = 23 order by recdate desc )
and Fid = @Fid


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


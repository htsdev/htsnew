/****** 

Business Logic:	The procedure is used by LMS application to get courts by court category number.
				
List of Parameters:
	@courtcatgorynum : court category number associated with the court.
	


******/

----  usp_mailer_getcoutnames_percourtcategory 27  
ALTER PROC [dbo].[usp_mailer_getcoutnames_percourtcategory]
@courtcatgorynum  INT = 1
AS                
          
DECLARE @temp TABLE (courtid INT, courtname VARCHAR(200))
--Yasir Kamal 7218 01/13/2010 lms structure changed. 
DECLARE @LocationId_Fk INT 

SELECT @LocationId_Fk = CASE ISNULL(tcc.CourtCategorynum,0) WHEN 26 THEN 5 ELSE ISNULL(tcc.LocationId_FK ,0) END
FROM tblCourtCategories tcc WHERE tcc.CourtCategorynum = @courtcatgorynum

SELECT @LocationId_Fk = ISNULL(@LocationId_Fk,0)
      
-- DALLAS COURTS          
IF @LocationId_Fk = 2  
	BEGIN  
		SELECT courtid,  
			   courtname  
		FROM   Dallastraffictickets.dbo.tblcourts  
		WHERE  CourtCategorynum = @courtcatgorynum  
			   AND inactiveflag = 0  
		ORDER BY  
			   courtname          
	END  
ELSE   
	BEGIN  
		SELECT courtid,  
			   CASE @courtcatgorynum WHEN 2 THEN shortcourtname ELSE courtname END AS courtname
		FROM   tblcourts  
		WHERE  CourtCategorynum = @courtcatgorynum  
			   AND inactiveflag = 0  
	END 
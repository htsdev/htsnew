﻿
/****** 
Created by:		Muhammad Nasir
Task ID :		6098
Business Logic:	The procedure is used by houston to Get all comment types
				
  
List of Columns:   
 CommentID:	Comment ID
 Comment Type: Comment Type Name
	
******/
 

Create PROCEDURE dbo.USP_HTP_Get_AllCommentsType
AS
	SELECT c.CommentID
	      ,c.CommentType
	FROM   Comments c
	WHERE c.CommentID IN (1,3,4,6,9,10)
	
	GO 
	
	GRANT  EXECUTE ON dbo.USP_HTP_Get_AllCommentsType TO dbr_webuser
	
	GO
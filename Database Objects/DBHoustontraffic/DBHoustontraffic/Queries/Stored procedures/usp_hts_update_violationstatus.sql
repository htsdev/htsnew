USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[usp_hts_update_violationstatus]  Script Date: 11/15/2011 19:14:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[usp_hts_update_violationstatus]
	@ticketid INT ,
	@empid INT
AS
BEGIN
    -- CHANGED BY TAHIR AHMED DT:2/27/08 BUG# 3275.....
    -- NEED TO UPDATE THE UNDERLYING VIOLATIONS ONE BY ONE SO ALL THE
    -- SETTING INFORMATION CHANGES LOGGED INTO CASE HISTORY.....
    DECLARE @temp TABLE (rowid INT IDENTITY, ticketsviolationid INT)
    
    INSERT INTO @temp
      (
        ticketsviolationid
      )
    SELECT ticketsviolationid
    FROM   tblticketsviolations
    WHERE  ticketid_pk = @ticketid 
           -- excluding disposed & no hire cases.....
           AND courtviolationstatusidmain NOT IN (80, 236)
           -- Zeeshan Haider 11306 08/15/2013 PMC Jury-Trial Cases(Do not modify case setting for sending LOR)
           AND courtviolationstatusidmain NOT IN (SELECT v.courtviolationstatusidmain
                                                  FROM   tblTicketsViolations v
                                                  WHERE  v.TicketID_PK = @ticketid
                                                         AND v.CourtID = 3004
                                                         AND v.CourtViolationStatusIDmain = 26)
    ORDER BY
           ticketsviolationid
    
    DECLARE @COUNT INT,
            @IDX INT,
            @TicketViolationId INT
    
    SELECT @idx = 1,
           @count = COUNT(ticketsviolationid)
    FROM   @temp
    
    WHILE @idx <= @count
    BEGIN
        SELECT @TicketViolationId = ticketsviolationid
        FROM   @temp
        WHERE  rowid = @idx
        
        UPDATE tblticketsviolations
        SET    courtviolationstatusidmain = 104,
               courtviolationstatusidscan = 104,
               courtviolationstatusid = 104,
               vemployeeid = @empid
        WHERE  ticketsviolationid = @TicketViolationId  
        
        SET @idx = @idx + 1
    END
    
    -- Abid Ali 4912 - Update traffic follow up date on waiting status
    EXECUTE [dbo].[usp_HTP_Update_TrafficWaitingFollowUpDate] @ticketid, @empid
END
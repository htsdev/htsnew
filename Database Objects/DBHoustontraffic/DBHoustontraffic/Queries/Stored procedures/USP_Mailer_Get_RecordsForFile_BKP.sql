SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_Mailer_Get_RecordsForFile_BKP]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_Mailer_Get_RecordsForFile_BKP]
GO

-- USP_Mailer_Get_RecordsForFile
CREATE procedure [dbo].[USP_Mailer_Get_RecordsForFile_BKP]

as

declare @sDate datetime

set @sDate = getdate()

-- CASES UPLOADED TODAY IN TRAFFIC TICKETS DATABASE....
select distinct 
	isnull(replace(lastname,',',' '),'') as last_name, 
	isnull(replace(firstname,',', ' '),'') as first_name,
	isnull(replace(initial,',',' '),'') as middle_initial,
	isnull(replace (t.address1 + isnull(' '+t.address2,''),',',' '),'') as address,
	isnull(replace(city,',',' '),'') as city,
	isnull(replace(isnull(s.state,''),',',' '),'') as state,
	isnull(replace(zipcode,',',' '),'') as zip_code, 
	v.recordid as record_id,
	1 as case_type  
from tblticketsarchive t inner join tblticketsviolationsarchive v
on v.recordid = t.recordid  left outer join tblstate s
on t.stateid_fk = s.stateid
where datediff(Day, t.recloaddate, @sDate)= 0
and t.AccuZipProcessDate is null
and len(isnull(t.address1,'')) > 0

-- CASES UPLOADED TODAY IN DALLAS TRAFFIC TICKETS DATABASE....
union
select distinct 
	isnull(replace(lastname,',',' '),'') as last_name, 
	isnull(replace(firstname,',', ' '),'') as first_name,
	isnull(replace(initial,',',' '),'') as middle_initial,
	isnull(replace (t.address1 + isnull(' '+t.address2,''),',',' '),'') as address,
	isnull(replace(city,',',' '),'') as city,
	isnull(replace(isnull(s.state,''),',',' '),'') as state,
	isnull(replace(zipcode,',',' '),'') as zip_code, 
	v.recordid as record_id,
	2 as case_type  
from dallastraffictickets.dbo.tblticketsarchive t inner join dallastraffictickets.dbo.tblticketsviolationsarchive v
on v.recordid = t.recordid  left outer join dallastraffictickets.dbo.tblstate s
on t.stateid_fk = s.stateid
where datediff(Day, t.recloaddate, @sDate)= 0
and t.AccuZipProcessDate is null
and len(isnull(t.address1,'')) > 0

-- CASES UPLOADED TODAY IN JIMS DATABASE....
union
select distinct 
	isnull(replace(lastname,',',' '),'') as last_name, 
	isnull(replace(firstname,',', ' '),'') as first_name,
	isnull(replace(middlename,',',' '),'') as middle_initial,
	isnull(replace (t.address ,',',' '),'') as address,
	isnull(replace(t.city,',',' '),'') as city,
	isnull(replace(isnull(t.state,''),',',' '),'') as state,
	isnull(replace(t.zip,',',' '),'') as zip_code, 
	t.clientid as record_id,
	3 as case_type  
from jims.dbo.criminalcases  t inner join jims.dbo.criminal503 v
on   v.bookingnumber = t.bookingnumber
where datediff(Day, t.recdate, @sDate)= 0
and t.AccuZipProcessDate is null
and len(isnull(t.address,'')) > 0


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_WebScan_InsertInLog]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_WebScan_InsertInLog]
GO


CREATE procedure [dbo].[usp_WebScan_InsertInLog]

@PicID as int,
@Status as varchar(50)

as

Insert into tbl_WebScan_Log
( 
	PicID,
	Status
)
values
( 
	@PicID,
	@Status
)


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


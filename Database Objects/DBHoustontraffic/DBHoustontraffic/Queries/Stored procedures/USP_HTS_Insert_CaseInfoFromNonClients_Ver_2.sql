USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTS_Insert_CaseInfoFromNonClients_Ver_2]    Script Date: 01/02/2014 04:07:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** 
Create by:  Sarim Ghani
Business Logic : This procedure is used to migrate case from non client to quote client.
List of Parameters:
     TicketNumber : Ticket Number of case
     Midnumber : Midnumber of case
	 Address : Address of case
	 ZipCode : Zip code of case 
	 EmpId : Sales Rep Id who migrate the non client case to quote client 
	 MailerID : Mailer id assicociated with the case.
	 sOutPut [Output Parameter] : Return case id comma separated with active flag
     @IsTrafficClient: pass one for traffic client and zero for non traffic

List of Columns:
IsTrafficAlertSignUp:	  Reprensts that client hire from TrafficAlert page 
Kazim 3828 4/25/2008 Migrate Arresting Agency information form nonclient to quote client
******/

ALTER PROCEDURE [dbo].[USP_HTS_Insert_CaseInfoFromNonClients_Ver_2] 
(
    @TicketNumber     VARCHAR(20),
    @midnumber        VARCHAR(20),
    @address          VARCHAR(50) = '',
    @ZipCode          VARCHAR(12) = '',
    @EmpId            INT = 3992,
    @mailerID         INT = 0,
    @IsTrafficClient  INT,
    @sOutPut          VARCHAR(20) = '' OUTPUT
)
AS
	SET NOCOUNT ON; 
	
	DECLARE @Officernumber INT,
	        @courtid INT,
	        @courtdate DATETIME,
	        @violationdate DATETIME,
	        @MailDate DATETIME,
	        @PlanId INT,
	        @RecCount INT,
	        @listdate DATETIME,
	        @TicketId INT,
	        @iFlag INT,
	        @RecordId INT,
	        @ErrorNumber INT,
	        @FTALinkId INT,
	        @CauseNumber VARCHAR(30),
	        @CaseTypeID INT,
	        @officer_FirstName VARCHAR(MAX), -- Sabir Khan 10089 03/05/2012 Fixed missing violation bug.
	        @officer_LastName VARCHAR(MAX)
	
	
	-- CREATING TEMP TABLES.................................. 
	DECLARE @tblTicketsArchive TABLE (
	            [TicketNumber] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	            [MidNumber] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	            [FirstName] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	            [LastName] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	            [Initial] [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	            [Address1] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	            [Address2] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	            [City] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	            [StateID_FK] [int] NULL,
	            [ZipCode] [varchar](12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	            [PhoneNumber] [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	            [WorkPhoneNumber] [varchar](12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	            [DOB] [datetime] NULL,
	            [DLNumber] [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	            [ViolationDate] [datetime] NULL,
	            [Race] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	            [Gender] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	            [Height] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	            [Weight] [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	            [ListDate] [datetime] NOT NULL,
	            [LetterCode] [smallint] NULL,
	            [MailDateREGULAR] [datetime] NULL,
	            [MailDateFTA] [datetime] NULL,
	            [Bondflag] [tinyint] NULL,
	            [officerNumber_Fk] [int] NULL,
	            [RecordID] [int],
	            [DP2] [varchar](10),
	            [DPC] [varchar](10),
	            [Flag1] [varchar](1),
	            --Waqas 6791 11/23/2009  new field added to migrate
	            [Eyes] [varchar] (10) NULL,
	            [HairColor] [varchar] (10) NULL,
	            [EmployerID] [int] NULL,
	            [DLStateID] [int] NULL
	        ) 
	
	DECLARE @tblTicketsViolationsArchive TABLE (
	            [TicketNumber_PK] [varchar](20) COLLATE 
	            SQL_Latin1_General_CP1_CI_AS NOT NULL,
	            [ViolationNumber_PK] [int] NULL,--Haris 9709 09/29/2011 datatype changed to int from tinyint
	            [FineAmount] [money] NULL,
	            [ViolationDescription] [varchar](200) COLLATE 
	            SQL_Latin1_General_CP1_CI_AS NULL,
	            --Waqas 6791 11/23/2009 ViolationCode length from 10 to 20.
	            [ViolationCode] [varchar](20) COLLATE 
	            SQL_Latin1_General_CP1_CI_AS NULL,
	            [violationstatusid] [int] NULL,
	            [CourtDate] [datetime] NULL,
	            [Courtnumber] [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	            [BondAmount] [money] NULL,
	            [TicketOfficerNumber] [varchar](20) COLLATE 
	            SQL_Latin1_General_CP1_CI_AS NULL,
	            [RecordID] [int] NULL,
	            [CauseNumber] [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	            [CourtLocation] [int] NULL,
	            [OffenseLocation] [varchar](200) COLLATE 
	            SQL_Latin1_General_CP1_CI_AS NULL,
	            [OffenseTime] [varchar] (20) COLLATE 
	            SQL_Latin1_General_CP1_CI_AS NULL,
	            [ArrestingAgencyName] [varchar] (50) NULL,
	            --Waqas 6791 11/20/2009 getting arrest date also 
	            [ArrestDate] DATETIME NULL,
	            [ChargeLevelID] [int] NULL,
	            --Waqas 7249 01/09/2010 added ticketviolationsdate
	            [TicketViolationDate] DATETIME NULL,
	            --Muhammad Muneer 8240 09/08/2010 added column Statutenumber
	            [Statutenumber] VARCHAR(50) NULL,
	            --Sabir Khan 11601 12/31/2013 added HCJP warrant fields
	            [LastOpenWarrType] VARCHAR(20),
	            [LastOpenWarrDate] DATETIME
	        )
	
	-- FIRST GET THE RECORD ID & VIOLATION DATE OF THE TICKET ....... 
	SELECT @RecordId = t.recordid,
	       @violationdate = ISNULL(t.violationdate, '01/01/1900'),
	       @Officernumber = ISNULL(T.officernumber_fk, 962528),
	       @officer_FirstName = ISNULL(o.FirstName,''), -- Sabir Khan 10089 03/05/2012 Fixed missing violation bug.
	       @officer_LastName = ISNULL(o.LastName,''),
	       @listdate = t.listdate,
	       --@courtid = t.courtid, -- Sarim 2664 06/06/2008   
	       @courtid = v.courtlocation,
	       @FTALinkId = v.ftalinkid,
	       @courtdate = t.courtdate,
	       @Causenumber = v.causenumber
	FROM   tblticketsarchive t WITH(NOLOCK)
	       INNER JOIN tblticketsviolationsarchive v WITH(NOLOCK)
	            ON  t.recordid = v.recordid
	       LEFT OUTER JOIN tblOfficer o WITH(NOLOCK)
	            ON o.OfficerNumber_PK = t.officerNumber_Fk
	WHERE  v.ticketnumber_pk = @ticketnumber 
	AND ISNULL(t.IsIncorrectMidNum,0) = 0 --Sabir Khan 10245 05/07/2012 Check added for bad mid number cases.
	AND ISNULL(t.Address1, '') = ISNULL(@address, '') -- Rab Nawaz Khan 10656 01/10/2013 Fix the issue of Wrong Court Loading in Client Profile during migration. . . 
	-- Adil 06/30/2008 Getting criminal identity of the case
	SELECT @CaseTypeID = ISNULL(CaseTypeID, 1)
	FROM   tblcourts WITH(NOLOCK)
	WHERE  courtid = @CourtID
	
	
	-- CHECK IF CAUSE NUMBER ALREADY EXISTS IN CLIENT/QUOTE DATABASE........ 
	SELECT @ticketid = t.ticketid_pk,
	       --Yasir Kamal 5143 03/04/2009 Index was outside the bounds of the array Bug Fixed.
	       @iFlag = ISNULL(t.activeflag, 0) 
	       --5143 end
	FROM   tbltickets t WITH(NOLOCK)
	       INNER JOIN tblticketsviolations v WITH(NOLOCK)
	            ON  t.ticketid_pk = v.ticketid_pk
	            AND v.casenumassignedbycourt LIKE @causenumber 
	
	SELECT @TICKETID = ISNULL(@TICKETID, 0) 
	
	-- IF NOT EXISTS THEN........ 
	IF @ticketid = 0
	BEGIN
	    SELECT @iFlag = 2 
	    
	    -- ROUTINES FOR INSIDE COURTS................................. 
	    IF @courtid IN (SELECT c.courtid FROM tblCourts c WITH(NOLOCK) WHERE c.courtcategorynum = 1) -- Rab Nawaz Khan 10394 07/30/2012 Added the all inside courts Dynamically including newly added inside court HMC-W. . . 
	    BEGIN
	        -- CONDITION # 1: WHEN FTA LINK ID IS NOT NULL 
	        IF @ftalinkid IS NOT NULL
	        BEGIN
	            INSERT INTO @tblTicketsArchive
	              (
	                TicketNumber,
	                MidNumber,
	                FirstName,
	                LastName,
	                Initial,
	                Address1,
	                Address2,
	                City,
	                StateID_FK,
	                ZipCode,
	                PhoneNumber,
	                WorkPhoneNumber,
	                DOB,
	                DLNumber,
	                ViolationDate,
	                Race,
	                Gender,
	                Height,
	                WEIGHT,
	                ListDate,
	                MailDateREGULAR,
	                MailDateFTA,
	                Bondflag,
	                officerNumber_Fk,
	                RecordID,
	                DPC,
	                DP2,
	                FLAG1
	              )
	            SELECT t.TicketNumber,
	                   t.MidNumber,
	                   t.FirstName,
	                   t.LastName,
	                   t.Initial,
	                   t.Address1,
	                   t.Address2,
	                   t.City,
	                   t.StateID_FK,
	                   t.ZipCode,
	                   t.PhoneNumber,
	                   t.WorkPhoneNumber,
	                   t.DOB,
	                   t.DLNumber,
	                   t.ViolationDate,
	                   t.Race,
	                   t.Gender,
	                   t.Height,
	                   t.Weight,
	                   t.ListDate,
	                   t.MailDateREGULAR,
	                   t.MailDateFTA,
	                   t.Bondflag,
	                   t.officerNumber_Fk,
	                   t.RecordID,
	                   t.DPC,
	                   t.DP2,
	                   t.FLAG1
	            FROM   tblticketsarchive t WITH(NOLOCK),
	                   tblticketsviolationsarchive v WITH(NOLOCK)
	            WHERE  t.recordid = v.recordid
	                   AND t.clientflag = 0
	                   AND ISNULL(t.IsIncorrectMidNum,0) = 0 --Sabir Khan 10245 05/07/2012 Check added for bad mid number cases.
	                   AND v.ftalinkid = @ftalinkid 
	            
	            INSERT INTO @tblTicketsViolationsArchive
	              (
	                TicketNumber_PK,
	                ViolationNumber_PK,
	                FineAmount,
	                ViolationDescription,
	                ViolationCode,
	                violationstatusid,
	                CourtDate,
	                Courtnumber,
	                BondAmount,
	                TicketOfficerNumber,
	                RecordID,
	                CauseNumber,
	                courtlocation,
	                OffenseLocation,
	                OffenseTime,
	                ArrestingAgencyName,
	                TicketViolationDate,
	                Statutenumber,
	                LastOpenWarrType,
					LastOpenWarrDate
	              )
	            --Waqas 7249 01/09/2010 added ticketviolationsdate
	            --Muhammad Muneer 8240 09/08/2010 added the column Statutenumber                 
	            SELECT v.TicketNumber_PK,
	                   v.ViolationNumber_PK,
	                   v.FineAmount,
	                   v.ViolationDescription,
	                   v.ViolationCode,
	                   v.violationstatusid,
	                   v.CourtDate,
	                   v.Courtnumber,
	                   v.BondAmount,
	                   v.TicketOfficerNumber,
	                   v.RecordID,
	                   v.CauseNumber,
	                   v.courtlocation,
	                   v.OffenseLocation,
	                   v.OffenseTime,
	                   v.ArrestingAgencyName,
	                   v.TicketViolationDate,
	                   v.Statutenumber, --Muhammad Muneer 8240 09/08/2010 added the column Statutenumber
	                   v.LastOpenWarrType,
	                   v.LastOpenWarrDate
	            FROM   tblticketsviolationsarchive v WITH(NOLOCK),
	                   @tblticketsarchive t
	            WHERE  t.recordid = v.recordid
	        END 
	        
	        
	        
	        -- CONDITION # 2: WHEN FTA LINK ID IS NULL AND VIOLATION DATE IS NULL 
	        IF @ftalinkid IS NULL
	           AND DATEDIFF(DAY, @violationdate, '01/01/1900') = 0
	        BEGIN
	            INSERT INTO @tblTicketsArchive
	              (
	                TicketNumber,
	                MidNumber,
	                FirstName,
	                LastName,
	                Initial,
	                Address1,
	                Address2,
	                City,
	                StateID_FK,
	                ZipCode,
	                PhoneNumber,
	                WorkPhoneNumber,
	                DOB,
	                DLNumber,
	                ViolationDate,
	                Race,
	                Gender,
	                Height,
	                WEIGHT,
	                ListDate,
	                MailDateREGULAR,
	                MailDateFTA,
	                Bondflag,
	                officerNumber_Fk,
	                RecordID,
	                DPC,
	                DP2,
	                FLAG1
	              )
	            SELECT t.TicketNumber,
	                   t.MidNumber,
	                   t.FirstName,
	                   t.LastName,
	                   t.Initial,
	                   t.Address1,
	                   t.Address2,
	                   t.City,
	                   t.StateID_FK,
	                   t.ZipCode,
	                   t.PhoneNumber,
	                   t.WorkPhoneNumber,
	                   t.DOB,
	                   t.DLNumber,
	                   t.ViolationDate,
	                   t.Race,
	                   t.Gender,
	                   t.Height,
	                   t.weight,
	                   t.ListDate,
	                   t.MailDateREGULAR,
	                   t.MailDateFTA,
	                   t.Bondflag,
	                   t.officerNumber_Fk,
	                   t.RecordID,
	                   t.DPC,
	                   t.DP2,
	                   t.FLAG1
	            FROM   tblticketsarchive t WITH(NOLOCK)
	            WHERE  t.clientflag = 0
					   AND ISNULL(t.IsIncorrectMidNum,0) = 0 --Sabir Khan 10245 05/07/2012 Check added for bad mid number cases.
	                   AND t.recordid = @recordid 
	            
	            INSERT INTO @tblTicketsViolationsArchive
	              (
	                TicketNumber_PK,
	                ViolationNumber_PK,
	                FineAmount,
	                ViolationDescription,
	                ViolationCode,
	                violationstatusid,
	                CourtDate,
	                Courtnumber,
	                BondAmount,
	                TicketOfficerNumber,
	                RecordID,
	                CauseNumber,
	                courtlocation,
	                OffenseLocation,
	                OffenseTime,
	                ArrestingAgencyName,
	                TicketViolationDate,
	                LastOpenWarrType,
					LastOpenWarrDate
	              )
	            --Waqas 7249 01/09/2010 added ticketviolationsdate
	            
	            SELECT v.TicketNumber_PK,
	                   v.ViolationNumber_PK,
	                   v.FineAmount,
	                   v.ViolationDescription,
	                   v.ViolationCode,
	                   v.violationstatusid,
	                   v.CourtDate,
	                   v.Courtnumber,
	                   v.BondAmount,
	                   v.TicketOfficerNumber,
	                   v.RecordID,
	                   v.CauseNumber,
	                   v.courtlocation,
	                   v.OffenseLocation,
	                   v.OffenseTime,
	                   v.ArrestingAgencyName,
	                   v.TicketViolationDate,
	                   v.LastOpenWarrType,
	                   v.LastOpenWarrDate
	            FROM   tblticketsviolationsarchive v WITH(NOLOCK),
	                   @tblticketsarchive t
	            WHERE  t.recordid = v.recordid
	        END 
	        
	        
	        
	        -- CONDITION # 3: WHEN FTA LINK ID IS NULL , VIOLATION DATE IS NOT NULL AND OFFICER NUMBER IS NULL 
	        IF @ftalinkid IS NULL
	           AND DATEDIFF(DAY, @violationdate, '01/01/1900') <> 0
	           AND @officernumber = 962528
	        BEGIN
	            -- ADD RECORDS IN TEMP TABLES BY MATCHING VIOLATION DATE & MID NUMMBER ONLY.... 
	            INSERT INTO @tblTicketsArchive
	              (
	                TicketNumber,
	                MidNumber,
	                FirstName,
	                LastName,
	                Initial,
	                Address1,
	                Address2,
	                City,
	                StateID_FK,
	                ZipCode,
	                PhoneNumber,
	                WorkPhoneNumber,
	                DOB,
	                DLNumber,
	                ViolationDate,
	                Race,
	                Gender,
	                Height,
	                WEIGHT,
	                ListDate,
	                MailDateREGULAR,
	                MailDateFTA,
	                Bondflag,
	                officerNumber_Fk,
	                RecordID,
	                DPC,
	                DP2,
	                FLAG1
	              )
	            SELECT t.TicketNumber,
	                   t.MidNumber,
	                   t.FirstName,
	                   t.LastName,
	                   t.Initial,
	                   t.Address1,
	                   t.Address2,
	                   t.City,
	                   t.StateID_FK,
	                   t.ZipCode,
	                   t.PhoneNumber,
	                   t.WorkPhoneNumber,
	                   t.DOB,
	                   t.DLNumber,
	                   t.ViolationDate,
	                   t.Race,
	                   t.Gender,
	                   t.Height,
	                   t.Weight,
	                   t.ListDate,
	                   t.MailDateREGULAR,
	                   t.MailDateFTA,
	                   t.Bondflag,
	                   t.officerNumber_Fk,
	                   t.RecordID,
	                   t.DPC,
	                   t.DP2,
	                   t.FLAG1
	            FROM   tblticketsarchive t WITH(NOLOCK)
	            WHERE  t.clientflag = 0
	                   AND DATEDIFF(DAY, t.violationdate, @violationdate) = 0
	                   AND t.midnumber = @midnumber
	                   AND ISNULL(t.IsIncorrectMidNum,0) = 0  --Sabir Khan 10245 05/07/2012 Check added for bad mid number cases. 
	            
	            INSERT INTO @tblTicketsViolationsArchive
	              (
	                TicketNumber_PK,
	                ViolationNumber_PK,
	                FineAmount,
	                ViolationDescription,
	                ViolationCode,
	                violationstatusid,
	                CourtDate,
	                Courtnumber,
	                BondAmount,
	                TicketOfficerNumber,
	                RecordID,
	                CauseNumber,
	                courtlocation,
	                OffenseLocation,
	                OffenseTime,
	                ArrestingAgencyName,
	                TicketViolationDate,
	                LastOpenWarrType,
					LastOpenWarrDate
	              )
	            --Waqas 7249 01/09/2010 added ticketviolationsdate
	            
	            SELECT v.TicketNumber_PK,
	                   v.ViolationNumber_PK,
	                   v.FineAmount,
	                   v.ViolationDescription,
	                   v.ViolationCode,
	                   v.violationstatusid,
	                   v.CourtDate,
	                   v.Courtnumber,
	                   v.BondAmount,
	                   v.TicketOfficerNumber,
	                   v.RecordID,
	                   v.CauseNumber,
	                   v.courtlocation,
	                   v.OffenseLocation,
	                   v.OffenseTime,
	                   v.ArrestingAgencyName,
	                   v.TicketViolationDate,
	                   v.LastOpenWarrType,
	                   v.LastOpenWarrDate
	            FROM   tblticketsviolationsarchive v WITH(NOLOCK),
	                   @tblticketsarchive t
	            WHERE  t.recordid = v.recordid
	        END 
	        
	        
	        
	        -- CONDITION # 4: WHEN FTA LINK ID IS NULL , VIOLATION DATE IS NOT NULL AND OFFICER NUMBER IS NOT NULL 
	        IF @ftalinkid IS NULL
	           AND DATEDIFF(DAY, @violationdate, '01/01/1900') <> 0
	           AND @officernumber <> 962528
	        BEGIN
	            INSERT INTO @tblTicketsArchive
	              (
	                TicketNumber,
	                MidNumber,
	                FirstName,
	                LastName,
	                Initial,
	                Address1,
	                Address2,
	                City,
	                StateID_FK,
	                ZipCode,
	                PhoneNumber,
	                WorkPhoneNumber,
	                DOB,
	                DLNumber,
	                ViolationDate,
	                Race,
	                Gender,
	                Height,
	                WEIGHT,
	                ListDate,
	                MailDateREGULAR,
	                MailDateFTA,
	                Bondflag,
	                officerNumber_Fk,
	                RecordID,
	                DPC,
	                DP2,
	                FLAG1
	              )
	            SELECT t.TicketNumber,
	                   t.MidNumber,
	                   t.FirstName,
	                   t.LastName,
	                   t.Initial,
	                   t.Address1,
	                   t.Address2,
	                   t.City,
	                   t.StateID_FK,
	                   t.ZipCode,
	                   t.PhoneNumber,
	                   t.WorkPhoneNumber,
	                   t.DOB,
	                   t.DLNumber,
	                   t.ViolationDate,
	                   t.Race,
	                   t.Gender,
	                   t.Height,
	                   t.Weight,
	                   t.ListDate,
	                   t.MailDateREGULAR,
	                   t.MailDateFTA,
	                   t.Bondflag,
	                   t.officerNumber_Fk,
	                   t.RecordID,
	                   t.DPC,
	                   t.DP2,
	                   t.FLAG1
	            FROM   tblticketsarchive t WITH(NOLOCK)
	            WHERE  t.clientflag = 0
	                   AND DATEDIFF(DAY, t.violationdate, @violationdate) = 0
	                   AND ISNULL(t.IsIncorrectMidNum,0) = 0 --Sabir Khan 10245 05/07/2012 Check added for bad mid number cases.
	                   AND t.midnumber = @midnumber
	                   -- Sabir Khan 10089 03/05/2012 Fixed missing violation bug.	
	                   -- Rab Nawaz Khan 04/20/2012 Fixed the  Client info not pulling up bug 			  
	                   AND 
						   (
							 ISNULL(t.officerNumber_Fk,'') in (SELECT OfficerNumber_PK FROM tblOfficer WITH(NOLOCK) WHERE FirstName = @officer_FirstName AND LastName = @officer_LastName)
							 OR
							 ISNULL(t.officerNumber_Fk,'')  = ISNULL(@officernumber,'')							
						   )
	                   --AND t.officernumber_fk = @officernumber 
	            
	            INSERT INTO @tblTicketsViolationsArchive
	              (
	                TicketNumber_PK,
	                ViolationNumber_PK,
	                FineAmount,
	                ViolationDescription,
	                ViolationCode,
	                violationstatusid,
	                CourtDate,
	                Courtnumber,
	                BondAmount,
	                TicketOfficerNumber,
	                RecordID,
	                CauseNumber,
	                courtlocation,
	                OffenseLocation,
	                OffenseTime,
	                ArrestingAgencyName,
	                TicketViolationDate,
	                Statutenumber,
	                LastOpenWarrType,
					LastOpenWarrDate
	              )
	            --Muhammad Muneer 8240 09/08/2010 added column Statutenumber --Waqas 7249 01/09/2010 added ticketviolationsdate 
	            
	            SELECT v.TicketNumber_PK,
	                   v.ViolationNumber_PK,
	                   v.FineAmount,
	                   v.ViolationDescription,
	                   v.ViolationCode,
	                   v.violationstatusid,
	                   v.CourtDate,
	                   v.Courtnumber,
	                   v.BondAmount,
	                   v.TicketOfficerNumber,
	                   v.RecordID,
	                   v.CauseNumber,
	                   v.courtlocation,
	                   v.OffenseLocation,
	                   v.OffenseTime,
	                   v.ArrestingAgencyName,
	                   v.TicketViolationDate,
	                   v.Statutenumber, --Muhammad Muneer 8240 09/08/2010 added column Statutenumber
	                   v.LastOpenWarrType,
	                   v.LastOpenWarrDate
	            FROM   tblticketsviolationsarchive v WITH(NOLOCK),
	                   @tblticketsarchive t
	            WHERE  t.recordid = v.recordid
	        END
	    END-- ROUTINES FOR OUTSIDE COURTS.................................
	    ELSE
	    BEGIN
	        INSERT INTO @tblTicketsArchive
	          (
	            TicketNumber,
	            MidNumber,
	            FirstName,
	            LastName,
	            Initial,
	            Address1,
	            Address2,
	            City,
	            StateID_FK,
	            ZipCode,
	            PhoneNumber,
	            WorkPhoneNumber,
	            DOB,
	            DLNumber,
	            ViolationDate,
	            Race,
	            Gender,
	            Height,
	            WEIGHT,
	            ListDate,
	            MailDateREGULAR,
	            MailDateFTA,
	            Bondflag,
	            officerNumber_Fk,
	            RecordID,
	            DPC,
	            DP2,
	            FLAG1,
	            Eyes,
	            HairColor,
	            EmployerID,
	            DLStateID
	          )
	        --Waqas 6791 11/23/2009  new field added to migrate
	        SELECT t.TicketNumber,
	               t.MidNumber,
	               t.FirstName,
	               t.LastName,
	               t.Initial,
	               t.Address1,
	               t.Address2,
	               t.City,
	               t.StateID_FK,
	               t.ZipCode,
	               t.PhoneNumber,
	               t.WorkPhoneNumber,
	               t.DOB,
	               t.DLNumber,
	               t.ViolationDate,
	               t.Race,
	               t.Gender,
	               t.Height,
	               t.weight,
	               t.ListDate,
	               t.MailDateREGULAR,
	               t.MailDateFTA,
	               t.Bondflag,
	               t.officerNumber_Fk,
	               t.RecordID,
	               t.DPC,
	               t.DP2,
	               t.FLAG1,
	               t.Eyes,
	               t.HairColor,
	               t.EmployerID,
	               t.DLStateID --Waqas 6791 11/23/2009  new field added to migrate
	        FROM   tblticketsarchive t WITH(NOLOCK)
	        WHERE  t.midnumber = @midnumber
	               AND t.listdate = @listdate
	               AND t.clientflag = 0 
	        
	        --Waqas 6791 11/20/2009 getting arrest date also
	        --Waqas 7249 01/09/2010 added ticketviolationsdate
	        INSERT INTO @tblTicketsViolationsArchive
	          (
	            TicketNumber_PK,
	            ViolationNumber_PK,
	            FineAmount,
	            ViolationDescription,
	            ViolationCode,
	            violationstatusid,
	            CourtDate,
	            Courtnumber,
	            BondAmount,
	            TicketOfficerNumber,
	            RecordID,
	            CauseNumber,
	            courtlocation,
	            ArrestingAgencyName,
	            ArrestDate,
	            ChargeLevelID,
	            TicketViolationDate,
	            Statutenumber,
	            LastOpenWarrType,
				LastOpenWarrDate
	          )
	        --Muhammad Muneer 09/08/2010 added column Statutenumber     
	        SELECT v.TicketNumber_PK,
	               v.ViolationNumber_PK,
	               v.FineAmount,
	               v.ViolationDescription,
	               v.ViolationCode,
	               v.violationstatusid,
	               v.CourtDate,
	               v.Courtnumber,
	               v.BondAmount,
	               v.TicketOfficerNumber,
	               v.RecordID,
	               v.CauseNumber,
	               v.courtlocation,
	               v.ArrestingAgencyName,
	               v.ArrestDate,
	               v.ChargeLevel,
	               v.TicketViolationDate,
	               v.Statutenumber, --Waqas 7111 12/09/2009 using arrestdate field
	               v.LastOpenWarrType,
	               v.LastOpenWarrDate
	        FROM   tblticketsviolationsarchive v WITH(NOLOCK),
	               @tblticketsarchive t --Muhammad Muneer 09/08/2010 added column Statutenumber
	        WHERE  t.recordid = v.recordid
	    END 
	    
	    ------------------------------------------------------------------------------------------------------------------------
	    -- INSERTING MAIN INFORMATION INTO TBLTICKETS FROM TBLTICKETSARCHIVE
	    ------------------------------------------------------------------------------------------------------------------------ 
	    BEGIN TRAN 
	    INSERT INTO tbltickets
	      (
	        midnum,
	        firstname,
	        lastname,
	        MiddleName,
	        Address1,
	        Address2,
	        City,
	        Stateid_FK,
	        Zip,
	        ClientCourtNumber,
	        --Contact1, -- Abbas Qamar 10093 04/11/2012 No need to migrate phone numbers.
	        --ContactType1,
	        --Contact2,
	        --ContactType2,
	        DOB,
	        DLNumber,
	        ViolationDate,
	        OfficerNumber,
	        employeeid,
	        Race,
	        Gender,
	        Height,
	        WEIGHT,	/* courtdate, Sarim 2664 06/06/2008*/ maildate,
	        bondflag,
	        BondsRequiredflag,
	        employeeidupdate,	/*currentcourtloc Sarim 2664 06/06/2008 ,*/ RecordId,
	        MailerID,
	        DP2,
	        DPC,
	        IsTrafficAlertSignup,
	        Flag1,
	        CaseTypeId,	-- Adil 06/30/2008 Inserting CaseTypeID
	        Eyes,
	        HairColor,
	        EmployerID,
	        DLState
	      )
	    --Waqas 6791 11/23/2009  new field added to migrate
	    
	    SELECT DISTINCT TOP 1 
	           MidNumber,
	           UPPER(FirstName),
	           UPPER(LastName),
	           UPPER(Initial),
	           UPPER(Address1),
	           UPPER(Address2),
	           UPPER(City),
	           StateID_FK,
	           ZipCode,
	           --- Comment By Sarim Ghani now data will move in New field "Clientcourtnum" 
	           phonenumber,
--	           CASE  -- Abbas Qamar 10093 04/11/2012 No need to migrate phone numbers.
--	                WHEN ISNULL(PhoneNumber, '') = '' THEN NULL
--	                ELSE PhoneNumber
--	           END,	-- Adil 7990 07/31/2010 Migrating phone number into ContactI and work phone number into ContactII.
--	           CASE 
--	                WHEN ISNULL(PhoneNumber, '') = '' THEN NULL
--	                ELSE 1
--	           END,
--	           CASE 
--	                WHEN ISNULL(WorkPhoneNumber, '') = '' THEN NULL
--	                ELSE WorkPhoneNumber
--	           END,
--	           CASE 
--	                WHEN ISNULL(WorkPhoneNumber, '') = '' THEN NULL
--	                ELSE 2
--	           END,
	           DOB,
	           DLNumber,
	           ViolationDate,
	           OfficerNumber_FK,
	           @EmpId,
	           UPPER(Race),
	           Gender,
	           Height,
	           WEIGHT,	/* @courtdate, Agha  2664 06/06/2008 */ 
	           CASE 
	                WHEN ticketnumber LIKE 'F%' THEN maildatefta
	                ELSE maildateregular
	           END,
	           bondflag,
	           3,
	           @EmpId,	/*@courtid,   Sarim 2664 06/06/2008   ,*/ RecordId,
	           @mailerID,
	           DP2,
	           DPC,
	           @IsTrafficClient,
	           Flag1,
	           @CaseTypeID -- Adil 06/30/2008 Inserting CaseTypeID
	           ,
	           Eyes,
	           HairColor,
	           EmployerID,
	           DLStateID --Waqas 6791 11/23/2009  new field added to migrate
	    FROM   @tblTicketsArchive
	    WHERE  recordid = @recordid 
	    
	    -- Noufil Khan 8136 10/21/2010 Move comments from archive table (This code is shifted from trigger "[trg_tblTickets_Events]")
	    UPDATE	tt
	    SET		tt.generalcomments = ISNULL(tt.generalcomments, '') + ' ' + ISNULL(ttap.Comments,''),
				tt.cdlflag = ISNULL(ttap.IsCDL, 3),
				tt.accidentflag = ISNULL(ttap.IsAccident, 3),
				tt.LanguageSpeak = ISNULL(ttap.LanguageSpeak, '')
	    FROM	tbltickets tt
				INNER JOIN tblTicketsArchivePCCR ttap WITH(NOLOCK) ON ttap.RecordID_FK = tt.recordid
	    WHERE	tt.recordid = @recordid AND LEN(ISNULL(ttap.Comments,'')) > 0
	    
	    
	    IF @@error = 0
	    BEGIN
	        --ozair Task 4767 09/11/2008 replace @@Identity with Scope_Identity
	        --as @@Identity returns the last inserted identity value instead of the insert statement executed in current scope
	        --select @TicketID=@@identity 
	        SELECT @TicketID = SCOPE_IDENTITY() 
	        SET @ErrorNumber = 0
	    END
	    ELSE
	    BEGIN
	        SET @ErrorNumber = @@error
	    END 
	    
	    ------------------------------------------------------------------------------------------------------------------------
	    -- INSERTING DETAILED INFORMATION INTO TBLTICKETSVIOLATIONS FROM TBLTICKETSVIOLATIONSARCHIVE
	    ------------------------------------------------------------------------------------------------------------------------ 
	    IF @TicketId <> 0
	    BEGIN
	        DECLARE @temp1 TABLE 
	                (
	                    violationnumber INT,
	                    bondamount MONEY,
	                    FineAmount MONEY,
	                    TicketNumber VARCHAR(20),
	                    --ozair 4775 09/16/2008 datatype changed to int from tinyint as the data/field inserted in this is of type smallint
	                    sequenceNumber INT,
	                    Violationcode VARCHAR(10),
	                    ViolationStatusID INT,
	                    CourtNumber VARCHAR(10),
	                    CourtDate DATETIME,
	                    ViolationDescription VARCHAR(200),
	                    courtid INT,
	                    ticketviolationdate DATETIME,
	                    ticketofficernum VARCHAR(20),
	                    CauseNumber VARCHAR(25),
	                    OffenseLocation VARCHAR(200),
	                    OffenseTime VARCHAR(20),
	                    RecordId INT,
	                    ArrestingAgencyName VARCHAR(50),
	                    ArrestDate DATETIME,	--Waqas 6791 11/20/2009 getting arrest date also
	                    ChargeLevelID INT,
	                    Statutenumber VARCHAR(50), -- Muhammad Muneer 8240 09/08/2010 added column Statutenumber
	                    LastOpenWarrType VARCHAR(20),
						LastOpenWarrDate DATETIME
	                ) 
	        
	        DECLARE @temp2 TABLE 
	                (
	                    violationnumber INT,
	                    fineamount MONEY,
	                    ticketnumber VARCHAR(25),
	                    sequencenummber INT,
	                    violationcode VARCHAR(10),
	                    bondamount MONEY,
	                    violationstatusid INT,
	                    courtnumber VARCHAR(3),	-- Sabir Khan 7979 07/02/2010 type has been changed from int into varchar. 
	                    courtdate DATETIME,
	                    courtid INT,
	                    CauseNumber VARCHAR(25),
	                    OffenseLocation VARCHAR(200),
	                    OffenseTime VARCHAR(20),
	                    recordid INT,
	                    ArrestingAgencyName VARCHAR(50),
	                    ArrestDate DATETIME,	--Waqas 6791 11/20/2009 getting arrest date also
	                    ChargeLevelID INT
	                ) 
	        
	        IF @courtid IN (SELECT c.courtid FROM tblcourts c WITH (NOLOCK) WHERE c.courtcategorynum = 1)  -- Rab Nawaz Khan 10394 07/30/2012 Added the all inside courts Dynamically including newly added inside court HMC-W. . .
	        BEGIN
	            INSERT INTO @temp1
	            SELECT DISTINCT 
	                   ISNULL(L.ViolationNumber_PK, 0) AS ViolationNumber,
	                   ISNULL(V.bondamount, 0) AS BondAmount,
	                   V.FineAmount,
	                   v.ticketnumber_pk,
	                   V.ViolationNumber_PK AS sequenceNumber,
	                   V.violationcode AS Violationcode,
	                   v.ViolationStatusID,
	                   v.CourtNumber,
	                   V.CourtDate,
	                   ISNULL(v.ViolationDescription, 'N/A') AS 
	                   violationdescription,
	                   --@CourtId, 
	                   v.courtlocation,
	                   --Waqas 7249 01/09/2010 added ticketviolationsdate
	                   v.TicketViolationDate,
	                   ISNULL(v.TicketOfficerNumber, 962528),
	                   v.CauseNumber,
	                   v.OffenseLocation,
	                   v.OffenseTime,
	                   v.recordid,
	                   v.ArrestingAgencyName,
	                   NULL,	--Waqas 6791 11/20/2009 getting arrest date as null 
	                   NULL,
	                   v.Statutenumber, --Muhammad Muneer 8240 added the column Statutenumber
	                   v.LastOpenWarrType,
	                   v.LastOpenWarrDate
	            FROM   @tblTicketsArchive t
	                   INNER JOIN @tblticketsviolationsarchive V
	                        ON  (t.recordid = v.recordid)
	                   LEFT OUTER JOIN tblViolations L WITH(NOLOCK)
	                        ON  V.violationcode = L.violationcode
	                        AND v.violationdescription = l.description
	        END
	        ELSE
	        BEGIN
	            INSERT INTO @temp1
	            --Waqas 6791 11/20/2009 getting arrest date also
	            --Waqas 7249 01/09/2010 added ticketviolationsdate
	            SELECT DISTINCT(
	                       SELECT TOP 1 ISNULL(ViolationNumber_PK, 0)
	                       FROM   tblViolations WITH(NOLOCK)
	                       WHERE  DESCRIPTION = V.ViolationDescription
	                   ) AS ViolationNumber,
	                   ISNULL(V.BondAmount, 0) AS BondAmount,
	                   V.FineAmount,
	                   V.TicketNumber_PK,
	                   '0' AS sequenceNumber,
	                   V.ViolationCode AS Violationcode,
	                   V.violationstatusid,
	                   V.Courtnumber,
	                   V.CourtDate,
	                   ISNULL(V.ViolationDescription, 'N/A') AS 
	                   violationdescription,
	                   V.CourtLocation,
	                   v.TicketViolationDate,
	                   ISNULL(V.TicketOfficerNumber, 962528) AS Expr1,
	                   V.CauseNumber,
	                   V.OffenseLocation,
	                   V.OffenseTime,
	                   V.RecordID,
	                   v.ArrestingAgencyName,
	                   v.ArrestDate,
	                   v.ChargeLevelID,
	                   v.Statutenumber, --Muhammad Muneer 8240 09/08/2010 Added the column Statutenumber
	                   v.LastOpenWarrType,
	                   v.LastOpenWarrDate
	            FROM   @tblTicketsArchive AS t
	                   INNER JOIN @tblticketsviolationsarchive AS V
	                        ON  t.RecordID = V.RecordID
	                   INNER JOIN tblViolations AS L WITH(NOLOCK)
	                        ON  V.ViolationDescription = L.Description
	            WHERE  (
	                       L.Violationtype =(
	                           SELECT ViolationType
	                           FROM   tblCourtViolationType WITH(NOLOCK)
	                           WHERE  (CourtId = @courtId)
	                       )
	                   )
	        END 
	        
	        
	        INSERT INTO @temp2
	        SELECT V.ViolationNumber_PK,
	               t1.fineamount,
	               t1.TicketNumber,
	               t1.sequencenumber,
	               t1.violationcode,
	               t1.bondamount,
	               ViolationStatusID,
	               CourtNumber,
	               CourtDate,
	               courtid,
	               t1.CauseNumber,
	               t1.OffenseLocation,
	               t1.OffenseTime,
	               t1.recordid,
	               t1.ArrestingAgencyName,
	               t1.ArrestDate,	--Waqas 6791 11/20/2009 getting arrest date also
	               t1.ChargeLevelID
	        FROM   @temp1 t1
	               INNER JOIN tblViolations V WITH(NOLOCK)
	                    ON  t1.sequencenumber = V.sequenceorder
	        WHERE  t1.violationnumber = 0 
	        
	        UPDATE t1
	        SET    t1.violationnumber = t2.violationnumber
	        FROM   @temp1 t1,
	               @temp2 t2
	        WHERE  t1.violationcode = t2.violationcode 
	        
	        SELECT @PlanId = (
	                   SELECT TOP 1 planid
	                   FROM   tblpriceplans WITH(NOLOCK)
	                   WHERE  courtid = @Courtid
	                          AND effectivedate <= @listdate
	                          AND enddate >  = @listdate
	                          AND isActivePlan = 1
	                   ORDER BY
	                          planid
	               ) 
	        
	        
	        INSERT INTO tblticketsviolations
	          (
	            TicketID_PK,
	            ViolationNumber_PK,
	            FineAmount,
	            refcasenumber,
	            -- sequencenumber,  -- Sarim 2664 06/06/2008    
	            bondamount,
	            CourtViolationStatusID,
	            CourtNumber,
	            CourtDate,
	            violationdescription,
	            courtid,
	            ticketviolationdate,
	            TicketOfficerNumber,
	            vemployeeid,
	            CourtViolationStatusIDmain,
	            CourtNumbermain,
	            CourtDatemain,
	            CourtViolationStatusIDscan,
	            CourtNumberscan,
	            CourtDatescan,
	            casenumassignedbycourt,
	            PlanId,
	            OffenseLocation,
	            OffenseTime,
	            recordid,
	            ArrestingAgencyName,
	            ArrestDate,	--Waqas 6791 11/20/2009 getting arrest date also
	            ChargeLevel,
	            Statutenumber, --Muhammad Muneer 8240 added the column
	            LastOpenWarrType,
				LastOpenWarrDate
	          )
	        SELECT DISTINCT 
	               @TicketID,
	               violationNumber,
	               FineAmount,
	               TicketNumber,
	               -- sequencenumber,  -- Sarim 2664 06/06/2008  
	               bondamount,
	               --Zeeshan Ahmed 4847 09/24/2008 Set Defauld Pre Trial Status For Criminal Case
	               CASE 
	                    WHEN @CaseTypeID = 2 THEN 101
	                    ELSE ViolationStatusID
	               END,
	               CourtNumber,
	               CourtDate,
	               violationdescription,
	               --@courtid, 
	               courtid,
	               ticketviolationdate,
	               ticketofficernum,
	               @empid,
	               --Zeeshan Ahmed 4847 09/24/2008 Set Defauld Pre Trial Status For Criminal Case
	               CASE 
	                    WHEN @CaseTypeID = 2 THEN 101
	                    ELSE ViolationStatusID
	               END,
	               CourtNumber,
	               CourtDate,
	               --Zeeshan Ahmed 4847 09/24/2008 Set Defauld Pre Trial Status For Criminal Case 
	               CASE 
	                    WHEN @CaseTypeID = 2 THEN 101
	                    ELSE ViolationStatusID
	               END,
	               CourtNumber,
	               CourtDate,
	               CauseNumber,
	               ISNULL(@PlanId, 45),
	               OffenseLocation,
	               OffenseTime,
	               recordid,
	               ArrestingAgencyName,
	               CASE 
	                    WHEN courtid = 3036 THEN ArrestDate
	                    ELSE NULL
	               END --Waqas 6791 11/20/2009 getting arrest date also
	               ,
	               ChargeLevelID,
	               Statutenumber, --Muhammad Muneer 8240 added the column
	               LastOpenWarrType,
	               LastOpenWarrDate
	        FROM   @temp1 
	        
	        
	        IF @@error <> 0
	            SET @ErrorNumber = @@error 
	        --------------------------------------------------------------------------------------------------------------------
	        -- INSERTING HISTORY NOTES
	        --------------------------------------------------------------------------------------------------------------------
	        --insert into tblticketsnotes(ticketid, subject, employeeid) values (@TicketID, 'Initial Inquiry', @empid ) 
	        
	        DECLARE @strNote VARCHAR(100) 
	        SELECT @strNote = 'INITIAL INQUIRY ' + UPPER(ISNULL(tc.shortname, 'N/A')) 
	               + ':' + UPPER(ISNULL(c.ShortDescription, 'N/A')) + ':' + 
	               CONVERT(VARCHAR(10), ISNULL(v.CourtDateMain, '1/1/1900'), 101) 
	               + ' #' + ISNULL(v.courtnumbermain, '0') + ' @ ' + UPPER(dbo . formattime(ISNULL(v.CourtDateMain, '1/1/1900')))
	        FROM   tblTicketsViolations v WITH(NOLOCK)
	               LEFT OUTER JOIN tblCourtViolationStatus c WITH(NOLOCK)
	                    ON  v.CourtViolationStatusIDmain = c.CourtViolationStatusID
	               LEFT OUTER JOIN tblcourts tc WITH(NOLOCK)
	                    ON  v.courtid = tc.courtid
	        WHERE  v.ticketid_pk = @TicketID 
	        
	        
	        IF @strNote IS NOT NULL
	            INSERT INTO tblticketsnotes
	              (
	                ticketid,
	                SUBJECT,
	                employeeid
	              )
	            VALUES
	              (
	                @TicketID,
	                @strNote,
	                @empid
	              ) 
	              
	              -- Rab Nawaz Khan 10195 04/24/2012 Commented the Code to insert the history note in the database of "Contact From Court"
	              -- Abbas Qamar  10093 04-04-2012 Inserting History notes when user data migrated 
	         --------------------------------------------------------------------------------------------------------------------
	        -- INSERTING HISTORY NOTES
	        --------------------------------------------------------------------------------------------------------------------
	        --DECLARE @Contact VARCHAR(100) 
	        --DECLARE @workphone VARCHAR(100) 
		    --Set @Contact = (
			--		    	SELECT PhoneNumber FROM  tblTicketsArchive
			--				WHERE     Recordid = @RecordId
		    --)
		    --Set @workphone = (
			--		    	SELECT WorkPhoneNumber FROM  tblTicketsArchive
			--				WHERE     Recordid = @RecordId
			--				)
	        --IF LEN(ISNULL(@Contact,''))>0
	        --BEGIN
	        --INSERT INTO tblticketsnotes
	        --     (
	        --        ticketid,
	        --        SUBJECT,
	        --        employeeid
	        --      )
	        --    VALUES
	        --      (
	        --        @TicketID,
	        --        'Contact from court: Home: '+@Contact,
	        --        @empid
	        --      ) 
	        --END
	        --IF LEN(ISNULL(@workphone,''))>0
	        --BEGIN
	        --     INSERT INTO tblticketsnotes
	        --      (
	        --        ticketid,
	        --        SUBJECT,
	        --        employeeid
	        --      )
	        --    VALUES
	        --      (
	        --        @TicketID,
	        --        'Contact from court: Work: '+@workphone,
	        --        @empid
	        --     ) 
	        --END
	        --------------------------------------------------------------------------------------------------------------------
	        -- End 10093
	        --------------------------------------------------------------------------------------------------------------------
	        -- END 10195
	        
	        -- Rab Nawaz Khan 11108 08/06/2013 Added for the pearland court. . . 
	        IF (@courtid = 3086)
	        BEGIN
	        	SELECT ttva.CauseNumber INTO #vioForServiceTickets FROM TrafficTickets.dbo.tblTicketsViolationsArchive ttva WITH(NOLOCK)
	        	WHERE ttva.CourtLocation =  @courtid AND ttva.RecordID = @RecordId AND ttva.violationstatusid = 26 -- Jury Trial
	        	
	        	IF ((SELECT COUNT(*) FROM #vioForServiceTickets) > 0)
	        	BEGIN
	        		DECLARE @Violation_Status VARCHAR(50)
	        		SET @Violation_Status = NULL
	        		
	        		SELECT TOP 1 @Violation_Status = pa.ViolationStatus
					FROM LoaderFilesArchive.dbo.tblPearland_ArrignmentOne_DataArchive pa WITH(NOLOCK) INNER JOIN #vioForServiceTickets v
					ON v.CauseNumber = REPLACE(pa.CauseNumber, '-', '')
					ORDER BY pa.InsertDate DESC
					
					IF (@Violation_Status = 'TRJYCNTCT')
					BEGIN
						DECLARE @folloupDate VARCHAR(50)
						SET @folloupDate = CONVERT(VARCHAR(50), [TrafficTickets].[dbo].[fn_getnextbusinessday] (GETDATE(),1), 101)
						EXEC TrafficTickets.dbo.usp_hts_AddServiceTicket @TicketID=@TicketID,@EmpID=3992,@per_completion=0,@Category=4,@Priority=1,@ShowTrialDocket=1,@ShowServiceInstruction=1,@ServiceInstruction=N'Court data indicates court is filing for continuance',@AssignedTo=4014,@ShowGeneralComments=1,@ContinuanceOption=1,@ServiceTicketfollowup = @folloupDate
						
						exec USP_HTS_Insert_Comments @TicketID=@TicketID,@EmployeeID=3992,@CommentID=N'1',@Comments=N'Court or DA filed for continuance ',@IsComplaint=0
	        		END
	        	END
	        END
	        
	        -- END 11108
	        -- Agha Usman 4347 07/22/2008  - Problem Client Detection
	        DECLARE @Firstname VARCHAR(20)                                         
	        DECLARE @Lastname VARCHAR(20)                                         
	        DECLARE @MiddleName VARCHAR(6)                                          
	        DECLARE @DOB DATETIME
	        
	        
	        SELECT @FirstName = FirstName,
	               @LastName = LastName,
	               @MiddleName = Initial,
	               @DOB = DOB
	        FROM   @tblTicketsArchive
	        
	        EXEC dbo.USP_HTP_ProblemClient_Manager @TicketId,
	             @FirstName,
	             @LastName,
	             @MiddleName,
	             @DOB,
	             NULL 
	        
	        
	        
	        IF @@error <> 0
	            SET @ErrorNumber = @@error 
	        --------------------------------------------------------------------------------------------------------------------
	        -- UPDATING CLIENT FLAG IN TBLTICKETSARHCIVE
	        -------------------------------------------------------------------------------------------------------------------- 
	        UPDATE t
	        SET    clientflag = 1
	        FROM   tblticketsarchive t WITH(NOLOCK)
	               INNER JOIN @tblTicketsArchive t2
	                    ON  t.recordid = t2.recordid 
	        
	        IF @@error <> 0
	            SET @ErrorNumber = @@error
	    END 
	    
	    IF @ErrorNumber = 0
	        COMMIT TRAN
	    ELSE
	        ROLLBACK TRAN
	END 
	
	-- UPDATE MAILER ID IN TBLTICKETS...... 
	EXEC dbo.USP_HTS_Update_Mailer_id @TicketID,
	     @empid,
	     1 
	
	--Yasir Kamal 5143 03/04/2009 Index was outside the bounds of the array Bug Fixed.
	SET @ticketid = ISNULL(@ticketid, 0)
	SET @iFlag = ISNULL(@iFlag, 0)
	--5143 end
	SELECT @sOutPut = CONVERT(VARCHAR(100), @ticketid) + ',' + CONVERT(VARCHAR(3), @iFlag) 
	---------------------------------------------------------------------- 
	
	
	--Nasir 5771 04/14/2009  if court is HMC then copy non client potential contact id to client table 
	IF @courtid IN (SELECT c.courtid FROM tblcourts c WITH (NOLOCK) WHERE c.courtcategorynum = 1) -- Rab Nawaz Khan 10394 07/30/2012 Added the all inside courts Dynamically including newly added inside court HMC-W. . .
	BEGIN
	    DECLARE @ContactID INT 
	    --check contact in contact table against last name,first initial DOB and min num or zip code
	    SELECT @ContactID = c.ContactID
	    FROM   Contact c WITH(NOLOCK)
	    WHERE  c.LastName = @Lastname
	           AND LEFT(c.FirstName, 1) = LEFT(@Firstname, 1)
	           AND c.MidNumber = @midnumber
	           AND c.DOB = @DOB  
	    
	    IF @ContactID IS NOT NULL
	    BEGIN
	        UPDATE tblTickets
	        SET    ContactID_FK = @ContactID
	        WHERE  TicketID_PK = @TicketID  
	        
	        UPDATE tblTicketsArchive
	        SET    PotentialContactID_FK = @ContactID
	        WHERE  RecordID = @RecordId
	               AND RecordID > 0
	        
	        --Maintain History
	        DECLARE @Name1 AS VARCHAR(50)    
	        SELECT @Name1 = (
	                   SELECT ISNULL(tu.Firstname, '') + ' ' + ISNULL(tu.Lastname, '')
	                   FROM   tblUsers tu WITH(NOLOCK)
	                   WHERE  tu.EmployeeID = @EMPID
	               ) 
	        
	        INSERT INTO tblTicketsNotes
	          (
	            TicketID,
	            [Subject],
	            Notes,
	            EmployeeID
	          )
	        VALUES
	          (
	            @TicketID,
	            'Contact ID Assigned = <a href="javascript:window.open(''/ClientInfo/AssociatedMatters.aspx?search=0&caseNumber=' + 
	            CONVERT(VARCHAR, @TicketID) + ''');void('''');">' + CONVERT(VARCHAR, @ContactID) 
	            + '</a> - ' + @Name1 + ' ',
	            'Contact ID Assigned = <a href="javascript:window.open(''/ClientInfo/AssociatedMatters.aspx?search=0&caseNumber=' + 
	            CONVERT(VARCHAR, @TicketID) + ''');void('''');">' + CONVERT(VARCHAR, @ContactID) 
	            + '</a> - ' + @Name1 + ' ',
	            @EMPID
	          )
	    END
	END
﻿ set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go
/******************************************************  
	Created By	   : Farrukh Iftikhar
	Created Date   : 07/28/2011
	Task Id		   : 9332   
	Business Logic : This Procedure Deletes Event Call Time
	Parameter Used : 
						@eventtypeid   Event id			

 ******************************************************/



CREATE PROCEDURE [dbo].[USP_AD_DELETE_EventCallTime]
@eventtypeid TINYINT
AS

DELETE autodialerEventdialtime WHERE EventTypeID = @eventtypeid

GO
GRANT EXECUTE ON [USP_AD_DELETE_EventCallTime] TO dbr_webuser




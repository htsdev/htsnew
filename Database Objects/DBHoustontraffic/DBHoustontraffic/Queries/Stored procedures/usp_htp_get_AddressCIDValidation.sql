/**********        
        
Created By : Waqas Javed 5770 04/09/2009 Address CID validation Report
        
Business Logic : This Report display client cases with following logics      
* Case should be active.
* Verified status should be in Arraignment, Appearance, waiting, Arrainment or Bond waiting, Pre trial, Judge Trial or Jury Trial.
* whose CID is same but address in different.
        
List Of Parameters :         
@Validation : if 1 then follow up date will be checked for validation email  

List Of Columns :      
        
ticketid_pk
ContactID 
Firstname
LastName  
DOB
FirstInitial 
CauseNumber
RefCaseNumber
Address
ContactAddress

       
**********/       
 -- Afaq 7918 06/22/2010 Change the whole procedure remove subqueries and apply selfjoin.
 -- usp_htp_get_AddressCIDValidation 0,1
CREATE PROCEDURE [dbo].[usp_htp_get_AddressCIDValidation]
--@Validation INT = 0, 7844 SAEED 06/03/2010, this paramter was not using in sp therefore comment this out.
	@ShowAll BIT = 0, --7844 SAEED 06/01/2010, Display all records if this parameter is 1
	@ValidationCategory INT = 0 -- Saeed 7791 07/10/2010 @ValidationCategory=1 for alert & @ValidationCategory=2 for report
AS
BEGIN
	SET NOCOUNT ON;
    SELECT DISTINCT tt.ContactID_FK AS ContactID,
           tt.TicketID_PK,
           tt.Lastname,
           tt.Firstname,
           tt.DOB,
           tt.AddressValidationFollowupDate AS FollowUpDate,
           SUBSTRING(tt.Firstname, 1, 1) AS FirstNameInitial,
           MAX(ttv.casenumassignedbycourt) AS CauseNumber,
           MAX(ttv.RefCaseNumber) AS RefCaseNumber,
           (
               ISNULL(tt.Address1, '') + ISNULL(tt.Address2, '') + ISNULL(tt.City, '') 
               + ISNULL(s.[State], '') + ISNULL(tt.Zip, '')
           ) AS ADDRESS,
           (
               c.Address1 + ' ' + c.Address2 + ', ' + c.City + ', ' + s.[State] 
               + ' ' + c.Zip
           ) AS ContactAddress
    FROM   tblTickets tt WITH(NOLOCK)
           INNER JOIN tblTickets tt2
                ON  tt.ContactID_FK = tt2.ContactID_FK
           INNER JOIN tblState s WITH(NOLOCK)
                ON  tt.Stateid_FK = s.StateID
           INNER JOIN tblState s1 WITH(NOLOCK)
                ON  tt.Stateid_FK = s1.StateID
           INNER JOIN tblTicketsViolations ttv WITH(NOLOCK)
                ON  tt.TicketID_PK = ttv.TicketID_PK
           INNER JOIN tblCourtViolationStatus tcvs WITH(NOLOCK)
                ON  ttv.CourtViolationStatusIDmain = tcvs.CourtViolationStatusID
           INNER JOIN tblTicketsViolations ttv2 WITH(NOLOCK)
                ON  tt2.TicketID_PK = ttv2.TicketID_PK
           INNER JOIN tblCourtViolationStatus tcvs2 WITH(NOLOCK)
                ON  ttv2.CourtViolationStatusIDmain = tcvs2.CourtViolationStatusID
           INNER JOIN Contact c WITH(NOLOCK)
                ON  tt.ContactID_FK = c.ContactID
    WHERE  --Ozair 7791 07/26/2010  where clause optimized  
           (
               (@ShowAll = 1 OR @ValidationCategory = 2) -- Saeed 7791 07/09/2010 display all records if showAll=1 or validation category is 'Report'  --7844 SAEED 06/01/2010 if show all is selected then display all records without checking followup date.
               OR (
                      (@ShowAll = 0 OR @ValidationCategory = 1)
                      AND DATEDIFF(
                              DAY,
                              ISNULL(tt.AddressValidationFollowupDate, '1/1/1900'),
                              GETDATE()
                          ) >= 0
                  ) -- Saeed 7791 07/09/2010 display  if showAll=0 or validation category is 'Alert' then display records which has 'follow up date' null,past or today.  --7844 SAEED 06/01/2010, if show all is not selected then bydefualt display records which have followup date of past,today or null.
           )
           AND ISNULL(tt.Address1, '') + ISNULL(tt.Address2, '') + ISNULL(tt.City, '') 
               + ISNULL(s.[State], '') + ISNULL(tt.Zip, '') <>
               ISNULL(tt2.Address1, '') + ISNULL(tt2.Address2, '') + ISNULL(tt2.City, '') 
               + ISNULL(s1.[State], '') + ISNULL(tt2.Zip, '')
           AND tcvs.CategoryID <> 50
           AND tcvs2.CategoryID <> 50
           AND tt2.Activeflag = 1
           AND tt.Activeflag = 1
           AND tt.TicketID_PK <> tt2.TicketID_PK
    GROUP BY
           tt.ContactID_FK,
           tt.Lastname,
           tt.Firstname,
           tt.DOB,
           tt.Address1,
           tt.Address2,
           tt.City,
           s.[State],
           tt.Zip,
           tt.TicketID_PK,
           tt.AddressValidationFollowupDate,
           c.Address1,
           c.Address2,
           c.City,
           c.Zip
    ORDER BY
           tt.ContactID_FK,
           tt.TicketID_PK,
           tt.Lastname
END



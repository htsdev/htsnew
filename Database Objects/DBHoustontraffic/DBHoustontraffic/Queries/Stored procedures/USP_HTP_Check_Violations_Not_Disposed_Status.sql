/**************************************
 Author: Yasir Kamal
 Create date: 06/04/2009
 Task Id : 5948
 
Business Logic: This Stored procedure is used to check violations having status other than disposed.

List Of Input Parameters :  
@ticketid : Ticketid Associated with case.


***************************************/

ALTER procedure [dbo].[USP_HTP_Check_Violations_Not_Disposed_Status] 
@ticketid int

as
--Sabir Khan 7313 01/22/2010 No Hire has been included...
--if ( Select Count(*) from tblticketsviolations where (courtviolationstatusidmain !=80) and (ticketid_pk = @ticketid )) > 0
IF EXISTS (
	SELECT TicketsViolationID 
	FROM tblticketsviolations 
	WHERE	ISNULL(CourtViolationStatusIDmain,0) NOT IN (80,236) 
	AND TicketID_PK = @ticketid
	)
	Select 1 
Else
	Select 0 




﻿/******  
* Created By :	  Syed Muhammad Ozair.
* Create Date :   11/05/2010 
* Task ID :		  8501
* Business Logic :  This procedure is used to Get get patient name and status and source information.
* List of Parameter :
* Column Return :     
******/
ALTER PROCEDURE dbo.USP_HTP_GetPatientInfoStatus
	@PatientId INT
AS
	SELECT p.FirstName + ' ' + p.LastName AS PatientName,
	       p.PatientStatusId,
	       p.SourceID,
	       p.OtherSource,
	       p.ReferralAttorneyId --Saeed 8585 12/03/2010 new filed added in select statement to get referral attorney.
	FROM   Patient p
	WHERE  p.Id = @PatientId
GO

GRANT EXECUTE ON dbo.USP_HTP_GetPatientInfoStatus TO dbr_webuser
GO


USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[usp_mailer_Get_Stafford_Warrant]    Script Date: 11/03/2010 06:22:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--SET QUOTED_IDENTIFIER ON 
--GO
--SET ANSI_NULLS ON 
--GO

--if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_mailer_Get_Stafford_Warrant]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
--drop procedure [dbo].[usp_mailer_Get_Stafford_Warrant]
--GO

/****** 
Business Logic:	The procedure is used by LMS application to get the number of printable letter
				count for the Stafford Warrant letter for the selected date range.
				
List of Parameters:
	@startdate:	Starting list date for Warrant letter.
	@enddate:	Ending list date for Warrant letter.
	@courtid:	Court id of the selected court category if printing for individual court hosue.
	@bondflag:	Flag if printing bond letters else appearance letter.
	@catnum:	Category number of the selected letter type, if printing for 
				all courts of the selected court category

List of Columns:	
	Total_Count:	Total number of letters exists for the selected date range.
	Good_Count:		Total number of letters that have valid addresses
	Bad_Count:		Total Number of letter that have undeliverable addresses
	DonotMail_Count:Total number of letter for addresses that had been marked to stop sending letters.
	

******/


-- usp_mailer_Get_Stafford_Warrant 12,36,12, '7/1/07'  , '7/30/07', 1
ALTER Procedure [dbo].[usp_mailer_Get_Stafford_Warrant]  
 (  
 @catnum  int,  
 @LetterType  int,  
 @crtid   int,                                                   
 @startListdate  Datetime,  
 @endListdate  DateTime,                                                        
 @SearchType     int                                               
 )  
  
as                                                                
                                                              
declare @officernum varchar(50),   
 @officeropr varchar(50),   
 @tikcetnumberopr varchar(50),   
 @ticket_no varchar(50),   
 @zipcode varchar(50),   
 @zipcodeopr varchar(50),   
 @fineamount money,  
 @fineamountOpr varchar(50) ,  
 @fineamountRelop varchar(50),  
 @singleviolation money,   
 @singlevoilationOpr varchar(50) ,   
 @singleviolationrelop varchar(50),   
 @doubleviolation money,  
 @doublevoilationOpr varchar(50) ,   
 @doubleviolationrelop varchar(50),   
 @count_Letters int,  
 @violadesc varchar(500),  
 @violaop varchar(50),  
 @sqlquery nvarchar(4000)  ,  
 @sqlquery2 nvarchar(4000)  ,  
 @sqlParam nvarchar(1000)  
  
  
select @sqlparam = '@catnum int, @LetterType int, @crtid int, @startListdate Datetime,    
      @endListdate DateTime, @SearchType int'  
  
                                                               
                                                        
select  @endlistdate = @endlistdate+'23:59:59.000',   
 @sqlquery ='',  
 @sqlquery2 = ''  
                                                                
Select @officernum=officernum,  
 @officeropr=oficernumOpr,  
 @tikcetnumberopr=tikcetnumberopr,  
 @ticket_no=ticket_no,  
 @zipcode=zipcode,  
 @zipcodeopr=zipcodeLikeopr,  
 @singleviolation =singleviolation,  
 @singlevoilationOpr =singlevoilationOpr,   
 @singleviolationrelop =singleviolationrelop,   
 @doubleviolation = doubleviolation,  
 @doublevoilationOpr=doubleviolationOpr,  
 @doubleviolationrelop=doubleviolationrelop,  
 @violadesc=violationdescription,  
 @violaop=violationdescriptionOpr ,   
 @fineamount=fineamount ,  
 @fineamountOpr=fineamountOpr ,  
 @fineamountRelop=fineamountRelop    
from  tblmailer_letters_to_sendfilters    
where  courtcategorynum=@catnum   
and  Lettertype=@LetterType   
                                                        
set  @sqlquery=@sqlquery+'                                            
 Select distinct '  
      
if @searchtype = 0   
 set  @sqlquery=@sqlquery+'        
 convert(datetime , convert(varchar(10),ta.listdate,101)) as mdate,   '  
else  
 set  @sqlquery=@sqlquery+' 
 --Sabir Khan 7483 03/05/2010 using updated date instead of bond date.                                                          
 convert(datetime , convert(varchar(10),tva.updateddate,101)) as mdate, '  -- Adil 7483 03/03/2010 use bondate from TVA instead of TA.
  
  
set  @sqlquery=@sqlquery+'        
 flag1 = isnull(ta.Flag1,''N'') ,                                                        
 ta.donotmailflag,  ta.recordid, -- Adil 8481 11/03/2010 Picked recordid instead of zipmid
 --ta.zipcode + ta.midnumber as recordid,
 count(tva.ViolationNumber_PK) as ViolCount,                                                        
 sum(tva.fineamount)as Total_Fineamount  
into #temp                                                              
FROM   dbo.tblTicketsViolationsArchive TVA  ,dbo.tblTicketsArchive TA                       
where TVA.recordid = TA.recordid  
and ta.clientflag=0 
and isnull(ta.ncoa48flag,0) = 0

--Sabir Khan 7483 03/05/2010 Useing loader id and checking bond date.
and (tva.bonddate is not null or tva.dlqupdatedate is not null)

--Adil 8349 10/29/2010 Dispose cases should not mail OUT.
and tva.violationstatusid <> 80
'
  
if @SearchType = 0   
 set @sqlquery =@sqlquery+ ' 
		and datediff(Day,  ta.listdate , @startListdate)<=0
		and datediff(Day,  ta.listdate , @endlistdate )>=0'  
else if @searchtype = 1   
  set @sqlquery =@sqlquery+ ' 
		and datediff(Day,  tva.updateddate , @startListdate)<=0  -- Sabir Khan 7483 03/05/2010 using updated date instead of bond date.
		and datediff(Day,  tva.updateddate , @endlistdate )>=0'  -- Adil 7483 03/03/2010 use bondate from TVA instead of TA. 
  
if(@crtid=@catnum  )                                      
 set @sqlquery=@sqlquery+' and tva.courtlocation In (Select courtid from tblcourts where courtcategorynum = @crtid )'                                          
else  
 set @sqlquery =@sqlquery +' and tva.courtlocation = @crtid'   
                                                       
if(@officernum<>'')                           
 set @sqlquery =@sqlquery + '  and ta.officerNumber_Fk '  + @officeropr  +'('+ @officernum+')'                                                           
                                                         
if(@ticket_no<>'')                          
 set @sqlquery=@sqlquery + ' and ('+  dbo.Get_String_Concat_With_op(''+@ticket_no+'' , 'tva.TicketNumber_PK' ,+ ''+ @tikcetnumberopr) +')'                                                                
                                      
if(@zipcode<>'')                                                                                
 set @sqlquery=@sqlquery+ ' and ('+ dbo.Get_String_Concat_With_op(''+@zipcode+'' , 'left(ta.zipcode,3)' ,+ ''+ @zipcodeopr) +')'                                     
  
if(@violadesc<>'')              
 set @sqlquery=@sqlquery+ ' and ('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ @violaop) +')'  

if(@fineamount<>0 and @fineamountOpr<>'not'  )                                    
 set @sqlquery =@sqlquery+ '  
        and tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                                         

if(@fineamount<>0 and @fineamountOpr  = 'not'  )                                    
 set @sqlquery =@sqlquery+ '  
        and not tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                                         
  
set @sqlquery =@sqlquery+ '                
and ta.recordid not in (                                                                      
 select n.recordid from tblletternotes n inner join  tblticketsarchive t on t.recordid = n.recordid
 where  lettertype =  @LetterType
 )'  
  
                                                        
if @searchtype = 0   
 set @sqlquery =@sqlquery+ ' group by                                                         
 convert(datetime , convert(varchar(10),ta.listdate,101)) ,  
 ta.Flag1,        
 ta.donotmailflag    ,ta.recordid --ta.zipcode + ta.midnumber 
'-- Adil 8481 11/03/2010 grouping on recordid instead of zipmid

else  
 set @sqlquery =@sqlquery+ ' group by                                                         
  convert(datetime , convert(varchar(10),tva.updateddate,101)),  
  ta.Flag1,        
  ta.donotmailflag     , ta.recordid -- ta.zipcode + ta.midnumber                                            
'                                    
  
  
                                                        
set @sqlquery=@sqlquery + '   
Select distinct  ViolCount, Total_Fineamount, mdate ,Flag1, donotmailflag, recordid into #temp1  from #temp                                                            
 '                                    
if(@singleviolation<>0 and @doubleviolation=0)                                    
 set @sqlquery =@sqlquery+ 'Where  '+ @singlevoilationOpr+                                                      
      '  (Total_FineAmount '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and ViolCount=1)                                    
         or violcount>3                                                                 
      '                                    
                                    
                                         
if(@doubleviolation<>0 and @singleviolation=0 )                                    
begin                                    
  
set @sqlquery=@sqlquery+                                           
'Select distinct   mdate ,  flag1, donotmailflag, recordid into #temp1  from #temp                                                          
 '  
                                    
set @sqlquery =@sqlquery + 'where   '+ @doublevoilationOpr+                                                           
       '   (Total_FineAmount '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ')and ViolCount=2)                                                          
      or violcount>3 '                                                        
end                                
                                    
if(@doubleviolation<>0 and @singleviolation<>0)                                    
begin                                    
set @sqlquery =@sqlquery+ 'Where  '+ @singlevoilationOpr+                                                          
      '  (Total_FineAmount '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and ViolCount=1)                                    
         or   '                                    
set @sqlquery =@sqlquery +  @doublevoilationOpr+                                                           
       '   (Total_FineAmount '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ')and ViolCount=2)                                                          
      or violcount>3 '                                      
                                    
end                         
  
  
set @sqlquery2=@sqlquery2 +'  
Select count(distinct recordid) as Total_Count, mdate   
into #temp2   
from #temp1   
group by mdate   
    
Select count(distinct recordid) as Good_Count, mdate   
into #temp3  
from #temp1   
where Flag1 in (''Y'',''D'',''S'')     
and donotmailflag = 0      
group by mdate     
                                                        
Select count(distinct recordid) as Bad_Count, mdate    
into #temp4    
from #temp1   
where Flag1  not in (''Y'',''D'',''S'')   
and donotmailflag = 0   
group by mdate   
                     
Select count(distinct recordid) as DonotMail_Count, mdate    
into #temp5     
from #temp1   
where donotmailflag=1   
group by mdate'  
   
           
set @sqlquery2 = @sqlquery2 +'                                                          
Select   #temp2.mdate as listdate,  
        Total_Count,  
        isnull(Good_Count,0) Good_Count,    
        isnull(Bad_Count,0)Bad_Count,    
        isnull(DonotMail_Count,0)DonotMail_Count    
from #Temp2 left outer join #temp3     
on #temp2.mdate=#temp3.mdate    
left outer join #Temp4    
on #temp2.mdate=#Temp4.mdate      
left outer join #Temp5    
on #temp2.mdate=#Temp5.mdate                                                         
where Good_Count > 0 order by #temp2.mdate desc  
drop table #temp drop table #temp1 drop table #temp2  drop table #temp3  drop table #temp4  drop table #temp5   
'                                                        

--print @sqlquery  + @sqlquery2                                         
--exec( @sqlquery)                                                          
set @sqlquery = @sqlquery + @sqlquery2  
--print @sqlquery 
exec sp_executesql @sqlquery , @sqlparam, @catnum, @LetterType, @crtid, @startListdate, @endListdate, @SearchType  
  

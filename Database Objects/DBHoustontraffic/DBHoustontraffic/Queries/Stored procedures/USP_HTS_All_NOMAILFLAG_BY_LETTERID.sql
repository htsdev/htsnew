SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_All_NOMAILFLAG_BY_LETTERID]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_All_NOMAILFLAG_BY_LETTERID]
GO



CREATE PROCEDURE [dbo].[USP_HTS_All_NOMAILFLAG_BY_LETTERID]                      
@LetterID int                      
as             
          
          
declare @cnt int          
          
select @cnt=count(ta.firstname) from tblticketsarchive ta          
inner join tblletternotes ln on ta.recordid=ln.recordid          
where ln.noteid = @LetterID and ln.lettertype not in (24,17,18)          
          
------------------for houston traffic system----------------------------------------------                   
          
if @cnt > 0          
begin          
 INSERT INTO tbl_hts_donotmail_records  (FirstName, LastName, Address, Zip, City, StateID_FK)                       
 select ta.FirstName,ta.LastName,ta.Address1+' '+ta.address2,case when len(ta.ZipCode)>5 then substring(ta.ZipCode,1,5) else ta.ZipCode end,ta.City,ta.StateID_FK from tblticketsarchive ta          
 inner join tblletternotes ln on ta.recordid=ln.recordid          
 where ln.noteid = @LetterID and ln.lettertype not in (24,17,18)             
          
          
 update ta          
  set DoNotMailFlag =2 from tblticketsarchive ta          
 inner join tblletternotes ln on ta.recordid=ln.recordid          
 where ln.noteid=@LetterID  and ln.lettertype not in (24,17,18)         
end          
          
          
          
          
----------------------------------for dallas------------------------------------------          
select @cnt=count(dallastraffictickets.dbo.tblticketsarchive.FirstName) from dallastraffictickets.dbo.tblticketsarchive           
inner join tblletternotes ln on dallastraffictickets.dbo.tblticketsarchive.recordid=ln.recordid          
where ln.noteid = @LetterID  and ln.lettertype  in (17,18)                   
          
if @cnt > 0          
begin          
          
 INSERT INTO tbl_hts_donotmail_records  (FirstName, LastName, Address, Zip, City, StateID_FK)                       
 select dallastraffictickets.dbo.tblticketsarchive.FirstName,          
 dallastraffictickets.dbo.tblticketsarchive.LastName,          
 dallastraffictickets.dbo.tblticketsarchive.Address1 +' '+  dallastraffictickets.dbo.tblticketsarchive.Address2 ,          
 case when len(dallastraffictickets.dbo.tblticketsarchive.ZipCode)>5 then substring(dallastraffictickets.dbo.tblticketsarchive.ZipCode,1,5) else dallastraffictickets.dbo.tblticketsarchive.ZipCode end,          
 dallastraffictickets.dbo.tblticketsarchive.City,          
 dallastraffictickets.dbo.tblticketsarchive.StateID_FK from dallastraffictickets.dbo.tblticketsarchive           
 inner join tblletternotes ln on dallastraffictickets.dbo.tblticketsarchive.recordid=ln.recordid          
 where ln.noteid = @LetterID    and ln.lettertype  in (17,18)                
          
          
          
          
 update td          
  set DoNotMailFlag =2 from dallastraffictickets.dbo.tblticketsarchive td          
 inner join tblletternotes ln on td.recordid=ln.recordid          
 where ln.noteid=@LetterID  and ln.lettertype  in (17,18)        
          
end          
          
----------------------------------------for JIMS-----------------------------------------------          
          
          
select @cnt=count(cc.firstname) FROM         JIMS.dbo.CriminalCases AS cc INNER JOIN              
              tblState AS st ON cc.State = st.State inner join           
    tblLetterNotes ln on cc.BookingNumber= ln.RecordID            
   where ln.noteid = @LetterID  and ln.lettertype  in (24)         
          
          
if @cnt > 0          
          
begin          
          
 INSERT INTO tbl_hts_donotmail_records  (FirstName, LastName, Address, Zip, City, StateID_FK)                         
 SELECT                   
 cc.FirstName,               
 cc.LastName,               
 cc.Address,               
 CASE WHEN len(cc.Zip) > 5 THEN substring(cc.Zip, 1, 5) ELSE cc.Zip END AS Zip,               
 cc.City,               
 st.StateID              
 FROM         JIMS.dbo.CriminalCases AS cc INNER JOIN              
      tblState AS st ON cc.State = st.State inner join           
     tblLetterNotes ln on cc.BookingNumber= ln.RecordID            
          
   where ln.noteid = @LetterID   and ln.lettertype  in (24)        
                       
 update jims.dbo.CriminalCases              
 set DoNotMailFlag = 2              
               
 FROM         JIMS.dbo.CriminalCases AS jcc INNER JOIN                
       tblLetterNotes AS ln ON jcc.BookingNumber = ln.RecordID INNER JOIN                
        tblState AS st ON jcc.State = st.State                
 WHERE                
  ln.noteid = @LetterID   and ln.lettertype  in (24)         
end          
          
          


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


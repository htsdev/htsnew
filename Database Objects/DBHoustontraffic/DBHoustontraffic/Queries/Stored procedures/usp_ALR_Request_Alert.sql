/*  
Created By: Unknown  
  
Business Logic:  
 The procedure is used to get records for ALR Request Alert report  
   
Parameters:  
 None  
   
Output Columns:  
 TicketId_pk:  
 TicketNumber:  
 FName:  
 LName:  
 CourtDateMain:  
 CourtNumber:  
 Status:  
 ShortName:  
 CourtDate:  
  
*/  
  
-- Noufil 4432 08/05/2008 html serial number added added for validation report


ALTER procedure [dbo].[usp_ALR_Request_Alert]
as    
SELECT DISTINCT 
       tv.TicketID_PK AS ticketid_pk,
       tv.RefCaseNumber AS ticketnumber,
       t.Firstname AS fname,
       t.Lastname AS lname,
       dbo.fn_DateFormat(tv.CourtDateMain, 24, '/', ':', 1) AS courtdatemain,
       CONVERT(INT, tv.CourtNumbermain) AS courtnumber,
       tcvs.ShortDescription AS STATUS,
       c.ShortName AS shortname,
       tv.CourtDateMain AS CourtDate,
       '<a href="http://ln.legalhouston.com/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber='+ CONVERT(VARCHAR(30), tv.TicketID_PK) + '" >' + CONVERT(VARCHAR(30),ROW_NUMBER() OVER(ORDER BY tv.TicketID_PK, CourtDate)) + '</a>' AS link
FROM   tblTickets AS t
       INNER JOIN tblTicketsViolations AS tv
            ON  t.TicketID_PK = tv.TicketID_PK
       INNER JOIN tblCourtViolationStatus AS tcvs
            --Ozair 7791 07/24/2010 join corrected to verified status
            ON  tcvs.CourtViolationStatusID = tv.CourtViolationstatusIDmain
       INNER JOIN tblCourts AS c
            ON  tv.CourtID = c.Courtid
WHERE  --Ozair 7791 07/24/2010 Where clause optimized  
       (
           SELECT COUNT(*)
           FROM   tblTicketsFlag
           WHERE  ticketid_pk = t.ticketid_pk
                  AND FlagID = 27
       ) = 0
       -- violation description should be ALR HEARING....
       AND tv.violationnumber_pk = 16159
           --case status should be WAITING or ALR HEARING....
       AND tcvs.courtviolationstatusid IN (104, 237)
           --Only Client Cases
       AND (t.Activeflag = 1)
ORDER BY
       ticketid_pk,
       CourtDate 

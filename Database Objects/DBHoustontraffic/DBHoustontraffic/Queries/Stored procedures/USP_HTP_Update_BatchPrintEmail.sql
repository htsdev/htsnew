﻿    
/******     
Create by:  Fahad Qureshi    
    
Business Logic : This procedure simply Update Batch Trial Print email record's history     
    
******/    
    
ALTER Procedure [dbo].[USP_HTP_Update_BatchPrintEmail]     
     
  @LetterID int,                                                  
  @ticketidlist varchar(8000) = '0,',              
  @TicketIDBatch varchar(8000)           
    AS    
    
--select top 200 * from tbltickets    
    
 UPDATE   b  
 SET   b.IsEmail=1       
from tbltickets t inner join tblhtsbatchprintletter b   
on t.ticketid_pk = b.ticketid_fk  
WHERE     
  b.LetterID_Fk = @LetterID AND b.deleteflag<>1 AND     
  b.ticketid_fk not in (select distinct ticketid_pk from tblticketsflag where flagid = 7)      
--Sabir Khan 5303 12/04/2008 must have a valid email address...  
and len(isnull(t.email,'')) > 0 and  charindex('@',t.Email,2)>0  
  
  
  
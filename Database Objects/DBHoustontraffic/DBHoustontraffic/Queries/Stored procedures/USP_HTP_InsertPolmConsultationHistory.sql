﻿/************************************************************
 * Created By : Noufil Khan
 * Create Date : 2/20/2010 5:49:40 PM
 * Business Logic : This Procedure is used to insert POLM Consultation History n Traffic Program
 * Parameter :
 *		@TicketID
 *		@QuickLegalMatter 
 *		@LegalMatterDescription 
 *		@Damage
 *		@BodilyDamage
 *		@AttorneyID
 *		@POLMCaseID
 * Column Return : 
 *		N/A
 ************************************************************/

ALTER PROCEDURE [dbo].[USP_HTP_InsertPolmConsultationHistory]
	@TicketID INT,
	@QuickLegalMatter VARCHAR(40),
	@LegalMatterDescription VARCHAR(2000),
	@Damage VARCHAR(50),
	@BodilyDamage VARCHAR(40),
	@AttorneyID INT,
	@POLMCaseID INT
AS
	INSERT INTO [TrafficTickets].[dbo].[POLMConsultationHistory]
	  (
	    [TicketID],
	    [QuickLegalMatter],
	    [LegalMatterDescription],
	    [Damage],
	    [BodilyDamage],
	    [AttorneyID],
	    [POLMCaseID]
	  )
	VALUES
	  (
	    @TicketID,
	    @QuickLegalMatter,
	    @LegalMatterDescription,
	    @Damage,
	    @BodilyDamage,
	    @AttorneyID,
	    @POLMCaseID
	  )
	SELECT @@ROWCOUNT
	
GO

GRANT EXECUTE ON [dbo].[USP_HTP_InsertPolmConsultationHistory] TO dbr_webuser
GO
	  
 
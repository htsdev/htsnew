SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_WebScan_GetBatchRecords]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_WebScan_GetBatchRecords]
GO



CREATE procedure [dbo].[usp_WebScan_GetBatchRecords]  
  
as  
  
declare @main table( batchid int , TotalCount int )  
  
Insert into @main Select batchid , count(id) as TotalCount from tbl_WebScan_Pic group by batchid  
  
Select distinct b.batchid,b.scantype , convert(varchar(12),b.scandate,101) as ScanDate,c.TotalCount  
  
from tbl_WebScan_Batch b inner join   
  
@main c on   
  
b.batchid=c.batchid  
  
  
  


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_RPT_Get_LMSObjects]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_RPT_Get_LMSObjects]
GO

CREATE procedure [dbo].[USP_RPT_Get_LMSObjects]

as


SELECT c.courtcategoryname [Court], l.lettername [Letter Name],
 '\\Loader\C$\inetpub\wwwroot\LMSRunTimeFiles\ReportFiles\' + l.reportfilename as [Report File Name]
FROM TBLLETTER l, tblcourtcategories c
where l.courtcategory = c.courtcategorynum
and l.isactive =1 
order by l.courtcategory, l.sortorder
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


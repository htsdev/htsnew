USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTS_GetCaseinfo_Fax]    Script Date: 11/16/2011 20:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
   
  
  
  
-- USP_HTS_GetLetterOfRep 124164    
/*      
Created By : Noufil Khan      
Business Logic : This procedure return information about client regarding fax    
Parameter :  @ticketid  display records with respect to ticket id entered    
Column :firstname,lastname,refcasenumber,email,fax    
    
*/    
--[USP_HTS_GetCaseinfo_Fax] 61982,3991    
CREATE procedure [dbo].[USP_HTS_GetCaseinfo_Faxmaker]    
@SystemId int,  
@ticketid int ,    
@FaxID INT,
@empid int      
    
as    
  
declare @dbName varchar(50)   
select @dbName = DbName from System where SystemId = @SystemId  
  
if @dbName = 'TrafficTickets'  
Begin    
  
select Top 1        
  tc.firstname as firstname,    
  tc.lastname as lastname,  
  -- Fahad Q. 5419 1/26/2009 Getting cause number/ticket number for Auto Fax  
  case when ltrim(rtrim(ISNULL(casenumassignedbycourt, ''))) != '' then ltrim(rtrim(ISNULL(casenumassignedbycourt,''))) else ltrim(rtrim(ISNULL(RefCaseNumber,''))) end AS refcasenumber,  
  t.email,  
  t.FirstName as [EFname],  
  t.LastName as [ELname],  
  C.fax as FAXNO, 
  --Nasir 6013 07/10/2009 add body
  fl.Body,
  C.ShortName  
       
from TrafficTickets.dbo.tblusers t      
  INNER JOIN TrafficTickets.dbo.tblTicketsViolations tv       
   ON tv.VEmployeeID = t.employeeid    
  INNER JOIN TrafficTickets.dbo.tblcourts C      
   On C.Courtid = tv.CourtID       
  inner join TrafficTickets.dbo.tbltickets tc    
   on tv.ticketid_pk=tc.ticketid_pk
   --Nasir 6013 07/10/2009 faxletter inner join
   Left OUTER join faxLetter fl
   ON fl.Ticketid=tc.TicketID_PK    
    
WHERE 
--Yasir Kamal 7012 11/19/2009 exclude disposed and no hire cases.
tv.CourtViolationStatusIDmain NOT IN (80,236)
and
(
	((@FaxID = 0) and tv.TicketID_PK=@ticketid  )
	or
	fl.FaxLetterid = @FaxID
)	

ORDER BY ISNULL(c.LOR,0) 
    
   
End  
  
else if @dbName = 'DallasTrafficTickets'  
Begin  
select Top 1        
  tc.firstname as firstname,    
  tc.lastname as lastname,     
 -- Fahad Q. 5419 1/26/2009 Getting cause number/ticket number for Auto Fax  
  case when ltrim(rtrim(ISNULL(casenumassignedbycourt, ''))) != '' then ltrim(rtrim(ISNULL(casenumassignedbycourt,''))) else ltrim(rtrim(ISNULL(RefCaseNumber,''))) end AS refcasenumber,  
  t.email,  
  t.FirstName as [EFname],  
  t.LastName as [ELname],  
  C.fax as FAXNO,  
  --Nasir 6013 07/10/2009 add body
  fl.Body,
  C.ShortName    
       
from DallasTrafficTickets.dbo.tblusers t      
  INNER JOIN DallasTrafficTickets.dbo.tblTicketsViolations tv       
   ON tv.VEmployeeID = t.employeeid    
  INNER JOIN DallasTrafficTickets.dbo.tblcourts C      
   On C.Courtid = tv.CourtID       
  inner join DallasTrafficTickets.dbo.tbltickets tc    
   on tv.ticketid_pk=tc.ticketid_pk 
   --Nasir 6013 07/10/2009 faxletter inner join
   INNER JOIN faxLetter fl
   ON fl.Ticketid=tc.TicketID_PK       
    
WHERE 
((@FaxID = 0) and tv.TicketID_PK=@ticketid  )
or
fl.FaxLetterid = @FaxID
  
End  
  

Go
GRANT EXECUTE ON [dbo].[USP_HTS_GetCaseinfo_Faxmaker] TO dbr_webuser

GO
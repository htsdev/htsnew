/*    
Created By: Haris Ahmed    
Date:   08/30/2012    
    
Business Logic:    
 The stored procedure is used to insert Immigration Comments
    
List of Input Parameters:    
 @TicketId: Case identification for the client. 
 @StatusID: Case status of the client. 
 @ImmigrationComments: Immigration Comments for the client. 
*/

ALTER PROCEDURE [dbo].[USP_HTS_Insert_ImmigrationComments]
	@TicketID INT,
	@StatusID INT,
	@ImmigrationComments VARCHAR(MAX),
	@IsImmigration BIT --Farrukh 11180 06/28/2013 Added "Isimmigration" field
AS
	IF NOT EXISTS (SELECT * FROM tbl_HTS_ImmigrationComments WHERE TicketID = @TicketID)
		BEGIN
			INSERT INTO tbl_HTS_ImmigrationComments
			(
				TicketID,
				StatusID,
				Comments,
				ModifiedDate,
				InsertDate,
				IsImmigration
			)
			VALUES
			(
				@TicketID,
				@StatusID,
				@ImmigrationComments,
				GETDATE(),
				GETDATE(),
				@IsImmigration
			)
		END
	ELSE
		BEGIN
			UPDATE tbl_HTS_ImmigrationComments
			SET	
				StatusID = @StatusID,
				Comments = @ImmigrationComments,
				ModifiedDate = GETDATE(),
				IsImmigration = @IsImmigration
			
			WHERE 
				TicketID = @TicketID
		END
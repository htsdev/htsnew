﻿ USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[sp_search_last_six_months_clients]    Script Date: 09/29/2011 11:56:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO



ALTER       procedure [dbo].[sp_search_last_six_months_clients]
@Inputticketnumber varchar(12)  ,
@InputDLNumber varchar(30) =null ,
@InputLastName varchar(20) =null,
@InputFirstname varchar(20)=null,
@InputMidnum varchar(20) =null ,
@InputDOB datetime =null,
@InputPhoneNumber varchar(20)=null,
@InputTrialDate datetime =null,
@InputArraignDate datetime = null,
@InputcontactDate datetime = null
as
set nocount on
Set rowcount 100
DECLARE @SQLString NVARCHAR(1000)
declare @ParmDefinition NVARCHAR(500)
DECLARE @ticketnumber varchar(12),
@violationNumber int, --Haris 9709 09/29/2011 datatype changed to int from tinyint
@tempticketnumber varchar(12),
@tempviolationNumber varchar(50),
@firstname varchar(20),
@lastname varchar(20),
@midnum varchar(20),
@fineamount money,
@dateset datetime,
@courtnum smallint,
@category varchar(10),
@sumfineamount money,
@TotalFeeCharged money,
@violationdate datetime,
@dob datetime,
@tempfirstname varchar(20),
@templastname varchar(20),
@tempmidnum varchar(20),
@tempdateset datetime,
@tempcourtnum smallint,
@tempcategory varchar(10),
@tempTotalFeeCharged money,
@tempviolationdate datetime,
@tempdob datetime,
@tempticketid int,
@tempaddress1 varchar(30),
@ticketid int,
@address1 varchar(30)
create TABLE #temp (
ticketnumber varchar(12),
violationNumber varchar(50), 
firstname varchar(20),
lastname varchar(20),
midnum varchar(20),
fineamount money,
dateset datetime,
courtnum smallint,
category varchar(10),
TotalFeeCharged money,
violationdate datetime,
dob datetime,
ticketid int,
address1 varchar(30),
activeflag int
)
SET @SQLString =
     N' DECLARE tickets_cursor CURSOR FOR  
select distinct 
firstname,lastname,midnumber,t.ticketnumber,v.violationNumber_pk,fineamount,courtdate,0,''Non Client'',null as ''TotalFeeCharged'',violationdate,DOB,0,address1
from tblticketsarchive t,tblticketsviolationsarchive v
where t.ticketnumber = v.ticketnumber_pk
and clientflag = 0'
if @Inputticketnumber <> ''
	begin
	set @sqlstring = @sqlstring + N' and t.ticketnumber = @Inputticketnumber'
	end
if @InputDLNumber <> ''
	begin
	set @sqlstring = @sqlstring + N' and t.DLNumber = @InputDLNumber'
	end
if @InputLastName <> ''
	begin
	set @sqlstring = @sqlstring + N' and Rtrim(t.lastname) like @InputLastName + ''%'''  
	end
if @InputFirstname <> ''
	begin
	set @sqlstring = @sqlstring + N' and rtrim(t.firstname) like @InputFirstname + ''%'''    
	end
if @InputMidnum <> ''
	begin
	set @sqlstring = @sqlstring + N' and t.midnumber = @InputMidnum' 
	end
if @InputDOB <> ''
	begin
	set @sqlstring = @sqlstring + N' and t.DOB = @InputDOB' 
	end
if @InputPhoneNumber <> ''
	begin
	set @sqlstring = @sqlstring + N' and t.phonenumber = @InputPhoneNumber' 
	end
if @InputTrialDate <> ''
	begin
	set @sqlstring = @sqlstring + N' and t.courtdate = @InputTrialDate' 
	end
--if @InputArraignDate <> ''
	--begin
	--set @sqlstring =@sqlstring + N' and A.courtdate = @InputArraignDate ' 
	--end
set @sqlstring =@sqlstring + N' order by t.ticketnumber' 
set @ParmDefinition = N'@Inputticketnumber varchar(12),@InputDLNumber varchar(30),@InputLastName varchar(20),
			@InputFirstname varchar(20),@InputMidnum varchar(20) ,@InputDOB datetime ,
			@InputPhoneNumber varchar(20),@InputTrialDate datetime,@InputArraignDate datetime'
EXECUTE sp_executesql @SQLString,@ParmDefinition ,@Inputticketnumber,@InputDLNumber,@InputLastName,@InputFirstname,
		@InputMidnum,@InputDOB,@InputPhoneNumber,@InputTrialDate,@InputArraignDate
 
		
OPEN tickets_cursor
  FETCH NEXT FROM tickets_cursor INTO 
	@firstname,@lastname,@midnum,@ticketnumber,@violationNumber,@fineamount,@dateset,@courtnum,@category,@TotalFeeCharged,@violationdate,@dob,@ticketid,@address1
IF @@FETCH_STATUS = 0     
begin
set @tempticketnumber = @ticketnumber
set @sumfineamount = 0
WHILE @@FETCH_STATUS = 0
BEGIN
	if (@tempticketnumber) = @ticketnumber
		begin	
			set @tempviolationNumber = isnull(@tempviolationNumber,' ') + ' ' + convert(varchar(5),@violationNumber)
			set @sumfineamount = @sumfineamount + @fineamount 
		end
	else
		begin
			print rtrim(@tempticketnumber) + ' ' + @tempviolationNumber
			insert into #temp(ticketnumber,violationNumber , firstname ,lastname ,midnum ,
				fineamount ,dateset ,courtnum ,category,TotalFeeCharged,violationdate,dob,ticketid,address1)
				 values(rtrim(@tempticketnumber),@tempviolationNumber,@tempfirstname,@templastname,@tempmidnum,
				@sumfineamount,@tempdateset,@tempcourtnum,@tempcategory,@tempTotalFeeCharged,@tempviolationdate,@tempdob,@ticketid,@address1)
			set @tempviolationNumber = ' '
			set @sumfineamount = 0
			set @tempviolationNumber = isnull(@tempviolationNumber,' ') + ' ' + convert(varchar(5),@violationNumber)
			set @sumfineamount = @sumfineamount + @fineamount 
		end
		set @tempticketnumber = @ticketnumber
		set @tempfirstname = @firstname
		set @templastname = @lastname
		set @tempmidnum = @midnum
		set @tempdateset = @dateset
		set @tempcourtnum = @courtnum
		set @tempcategory = @category
		set @tempTotalFeeCharged = @TotalFeeCharged
		set @tempviolationdate = @violationdate
		set @tempdob = @dob
		set @tempticketid = @ticketid
		set @tempaddress1 = @address1
	FETCH NEXT FROM tickets_cursor INTO 
	@firstname,@lastname,@midnum,@ticketnumber,@violationNumber,@fineamount,@dateset,@courtnum,@category,@TotalFeeCharged,@violationdate,@dob,@ticketid,@address1
END 
insert into #temp(ticketnumber,violationNumber , firstname ,lastname ,midnum ,
				fineamount ,dateset ,courtnum ,category,TotalFeeCharged,violationdate,dob,ticketid,address1)
				 values(rtrim(@tempticketnumber),@tempviolationNumber,@firstname,@lastname,@midnum,
				@sumfineamount,@dateset,@courtnum,@category,@tempTotalFeeCharged,@violationdate,@dob,@ticketid,@address1) 
end 
CLOSE tickets_cursor
DEALLOCATE tickets_cursor
select * from #temp
drop table #temp
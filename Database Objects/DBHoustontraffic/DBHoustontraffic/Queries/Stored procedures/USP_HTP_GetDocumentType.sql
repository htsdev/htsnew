﻿ /******  
* Created By :	  Saeed Ahmed.
* Create Date :   11/10/2010 
* Task ID :		  8501
* Business Logic :  This procedure is used to get list of all docuemnt types.
* List of Parameter :
* Column Return :     
******/
CREATE PROCEDURE dbo.USP_HTP_GetDocumentType
AS
SELECT id,description FROM DocuemntType dt
	
GO
GRANT EXECUTE ON dbo.USP_HTP_GetDocumentType TO dbr_webuser
GO



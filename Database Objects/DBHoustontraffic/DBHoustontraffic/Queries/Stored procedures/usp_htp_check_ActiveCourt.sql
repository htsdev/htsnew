

/****** 
Create by		: Syed Faique Ali	
Created Date	: 10/22/2013
Task ID			: 11491
Business Logic : This procedure is used to determin the court house wheather Active or Inactive against selected ticket id

Parameters : 
				@ticketid
******/

USE TrafficTickets
GO

ALTER procedure USP_HTP_Check_ActiveCourt 
@ticketid int  
AS
BEGIN
	SELECT tc.InActiveFlag FROM tblTicketsViolations ttv 
	INNER JOIN tblCourts tc ON tc.Courtid=ttv.CourtID
	WHERE ttv.TicketID_PK =@ticketid
	
END  
GO

GRANT EXECUTE ON USP_HTP_Check_ActiveCourt TO dbr_webuser

GO






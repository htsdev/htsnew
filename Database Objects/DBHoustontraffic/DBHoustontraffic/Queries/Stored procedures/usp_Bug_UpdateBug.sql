SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Bug_UpdateBug]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Bug_UpdateBug]
GO



CREATE procedure [dbo].[usp_Bug_UpdateBug]              
              
@PriorityID int,              
@DeveloperID int,      
@BugID int,  
@StatusID int             
              
as              
              
update tbl_bug  set             
            
 PriorityID=@PriorityID,      
 Developerid=@DeveloperID,               
 StatusID=@StatusID             
where             
 bug_id=@BugID         
  
            
     




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


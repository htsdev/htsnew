﻿/************************************************************
 * Created By : Noufil Khan 
 * Created Date: 10/30/2010 3:58:06 PM
 * Business Logic : This procedure insert new file information against the appropiate patient
 * Parameters :
 *				@PatientId,
				@OriginalFileName,
				@StoreFileLocation,
				@Description,
				@userId
 * Column Return :
 *				Identity column
 ************************************************************/

ALTER PROCEDURE [dbo].[USP_HTP_InsertPatientFile]
	@PatientId INT,
	@OriginalFileName VARCHAR(300),
	@StoreFileLocation VARCHAR(500),
	@Description VARCHAR(2000),
	@userId INT,
	@DocumentTypeId INT,
	@DocumentsubTypeId INT
AS
	INSERT INTO PatientsUploadedFile
	  (
	    PatientId,
	    OriginalFileName,
	    StoreFileLocation,
	    [Description],
	    UploadedBy,
	    DocumentTypeId ,
		DocumentsubTypeId 
	  )
	VALUES
	  (
	    @PatientId,
	    @OriginalFileName,
	    @StoreFileLocation,
	    @Description,
	    @userId,
	    @DocumentTypeId ,
		@DocumentsubTypeId 
	  )
	  
	  select SCOPE_IDENTITY()
	  
	  GO
	  GRANT EXECUTE ON [dbo].[USP_HTP_InsertPatientFile] TO dbr_webuser
/******       
Created by: Tahir      
Business Logic : Get all the violations against a ticket id provided       
     violation belongs to a criminical court      
     violation status is not "NO hire" and "Dispose"      
       
List of Parameters:      
 @TicketId      
 @TicketsViolationID        
        
       
List of Columns:      
 CourtName - Short court name      
 FirmName - Short Covering firm name      
 ClientLanguage - Abbreviation of Client Language      
 TotalFee - Total fee of the case      
 PaidFee - Total amount paid by client till now      
 Lastname - Last name of the client      
 TicketsViolationID       
 CourtDateMain - Verified court date      
******/      
--  grant execute on [dbo].[USP_HTP_GetCriminalViolationsForSharePoint] to dbr_webuser      
 -- [USP_HTP_GetCriminalViolationsForSharePoint] -1, 1381336      
 -- [USP_HTP_GetCriminalViolationsForSharePoint] 196225, -1      
ALTER PROCEDURE [dbo].[USP_HTP_GetCriminalViolationsForSharePointDeletion]      
(      
 @TicketId int = NULL,      
 @TicketsViolationID int = NULL      
)      
AS       
SELECT        
--Waqas Javed  5173 01/20/2009
c.ShortName as CourtName, ISNULL(f.FirmAbbreviation,'SULL') as FirmName,      
ClientLanguage = case when t.LanguageSpeak = 'SPANISH' then 'S' else'E'end ,      
t.calculatedtotalfee as TotalFee,       
PaidFee =  (                                                     
 select isnull( SUM(ISNULL(p.ChargeAmount, 0)),0) from tblticketspayment p                                                    
 where p.ticketid = t.ticketid_pk                                                    
 and  p.paymentvoid = 0                                                    
 ) ,      
t.Lastname, tv.TicketsViolationID, tv.CourtDateMain, t.TicketId_pk as TicketId      
--, tv.CourtID, t.TicketID_PK, t.Firstname, t.MiddleName,       
 --t.Midnum, tv.RefCaseNumber,tv.CourtViolationStatusId,       
FROM tblTickets t       
INNER JOIN tblTicketsViolations tv ON t.TicketID_PK = tv.TicketID_PK      
inner join tblCourts c on c.CourtId = tv.CourtID      
--Waqas Javed  5173 01/20/2009
LEFT OUTER JOIN tblFirm f on f.FirmID = tv.CoveringFirmId      
      
WHERE     
--Sabir Khan 4662 09/12/2008 add casetypeid for family law    
----------------------------    
--Waqas Javed  5173 01/20/2009 also add event if covering firm is not SULL  
--(t.CaseTypeID in (2,4) or isnull(tv.CoveringFirmId,0) <> 3000 )  
--and tv.CourtViolationStatusId not IN (80,236)      
--Waqas Javed  5173 01/20/2009 also add event if covering firm is not SULL        
 (@TicketId=-1 OR t.TicketId_pk = @TicketId )       
AND (@TicketsViolationID=-1 OR tv.TicketsViolationID = @TicketsViolationID )       
      
      
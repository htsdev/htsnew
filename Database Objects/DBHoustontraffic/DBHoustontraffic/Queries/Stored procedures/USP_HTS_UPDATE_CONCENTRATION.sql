SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_UPDATE_CONCENTRATION]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_UPDATE_CONCENTRATION]
GO


Create PROCEDURE [dbo].[USP_HTS_UPDATE_CONCENTRATION]    
@CourtDate datetime,    
@CourtID int,    
@CourtRoom int,    
@ViolationID int,    
@FirmID int    
as    
    
declare @sql varchar(3000)  
  
  
if @CourtID IN (3001,3002,3003)   
begin  
  
if @ViolationID = 0
begin
UPDATE tblticketsviolations    
 set CoveringFirmID = @FirmID  
  from tblticketsviolations tv inner join tblcourtviolationstatus cvs on    
 tv.CourtViolationStatusIDMain = cvs.CourtViolationStatusID    
 where    
  DateDiff(day,tv.CourtDateMain,@CourtDate)=0 and    
  tv.CourtID = @CourtID and  
  tv.CourtNumbermain = @CourtRoom 

 UPDATE tbltickets    
 set CoveringFirmID = @FirmID  
 from tbltickets t inner join tblticketsviolations tv on    
 t.ticketid_pk = tv.ticketid_pk inner join tblcourtviolationstatus cvs on    
 tv.CourtViolationStatusIDMain = cvs.CourtViolationStatusID 
 where    
  DateDiff(day,tv.CourtDateMain,@CourtDate)=0 and    
  tv.CourtID = @CourtID and  
  tv.CourtNumbermain = @CourtRoom    
end
else
UPDATE tblticketsviolations    
 set CoveringFirmID = @FirmID  
  from tblticketsviolations tv inner join tblcourtviolationstatus cvs on    
 tv.CourtViolationStatusIDMain = cvs.CourtViolationStatusID    
 where    
  DateDiff(day,tv.CourtDateMain,@CourtDate)=0 and    
  tv.CourtID = @CourtID and  
  tv.CourtNumbermain = @CourtRoom and    
  cvs.CategoryID = @ViolationID  

 UPDATE tbltickets    
 set CoveringFirmID = @FirmID  
 from tbltickets t inner join tblticketsviolations tv on    
 t.ticketid_pk = tv.ticketid_pk inner join tblcourtviolationstatus cvs on    
 tv.CourtViolationStatusIDMain = cvs.CourtViolationStatusID    
    
 where    
  DateDiff(day,tv.CourtDateMain,@CourtDate)=0 and    
  tv.CourtID = @CourtID and  
  tv.CourtNumbermain = @CourtRoom and    
  cvs.CategoryID = @ViolationID  
  
  
end  
  
else  
begin  
UPDATE tblticketsviolations    
 set CoveringFirmID = @FirmID    
 from tblticketsviolations tv inner join tblcourtviolationstatus cvs on    
 tv.CourtViolationStatusIDMain = cvs.CourtViolationStatusID    
 where    
  tv.CourtDateMain = @CourtDate and    
  tv.CourtID = @CourtID and    
  tv.CourtNumbermain = @CourtRoom and    
  cvs.CategoryID = @ViolationID   
   
    
    
 UPDATE tbltickets    
 set CoveringFirmID = @FirmID    
 from tbltickets t inner join tblticketsviolations tv on    
 t.ticketid_pk = tv.ticketid_pk inner join tblcourtviolationstatus cvs on    
 tv.CourtViolationStatusIDMain = cvs.CourtViolationStatusID    
    
 where    
  tv.CourtDateMain = @CourtDate and    
  tv.CourtID = @CourtID and    
  tv.CourtNumbermain = @CourtRoom and    
  cvs.CategoryID = @ViolationID    
    
    
  end  


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


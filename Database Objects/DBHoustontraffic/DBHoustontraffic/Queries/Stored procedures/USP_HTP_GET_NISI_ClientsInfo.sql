USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTP_GET_NISI_ClientsInfo]    Script Date: 05/15/2013 01:07:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** 
Created by:		Rab Nawaz
Task ID :		10729
Date :			03/28/2013

Business Logic:	The procedure is used by HTP to get the all the Clients which are rebonded (court issue the NISI against them).
				
List of Parameters:	
	@startdate:		Starting date of complaint flag add
	@endListDAte:	Ending date of complaint flag add
	@CaseType		Civil, Criminal, Traffic, Family
	@Attorney		Attorney who was covering the case
	@ShowAll		By default all non reviewed cases will be displayed

List of Columns:	
	TicketID_PK:	Case number.
	FullName:		FullName of client
	Area			 case type Civil, Criminal, Traffic, Family
	AttorneyName:	Attorney who was covering the case
	CDate:			complaint date and time
	Complaint:		system shows the full complaint comment

******/

--[dbo].[USP_HTP_GET_NISI_ClientsInfo] 1, 'Rebonded'

ALTER PROCEDURE [dbo].[USP_HTP_GET_NISI_ClientsInfo]

@isActive INT = 1,
@status VARCHAR(20) = 'Pending'

AS
BEGIN
	-- Searching for Closed cases who has Disposed court Status or Cases having past Court Date. . . 
	IF (@isActive = 0)
	BEGIN
		
		SELECT nisi.RowID, ISNULL(nisi.TicketId, 0) AS TicketID, CONVERT(VARCHAR(50), nisi.[NISI Issue Date], 101) AS NISIGeneratedDate, 
		case when replace(isnull(nisi.[First Name],'') + isnull(nisi.[Middle Name],'') + isnull(nisi.[Last Name],''),' ','') = '' then t.Firstname
		else nisi.[First Name] end AS [FirstName], 
		case when replace(isnull(nisi.[First Name],'') + isnull(nisi.[Middle Name],'') + isnull(nisi.[Last Name],''),' ','') = '' then t.MiddleName
		else nisi.[Middle Name] end AS [MidName],
		case when replace(isnull(nisi.[First Name],'') + isnull(nisi.[Middle Name],'') + isnull(nisi.[Last Name],''),' ','') = '' then t.Lastname
		else nisi.[Last Name] end AS [LastName], 		  
		ISNULL(nisi.BondForfeitureAmount, 0.00) AS [BondAmount], 
		ltrim(rtrim(REPLACE(nisi.NisiCauseNumber,' ',''))) AS [NISICause], CONVERT(VARCHAR(100), '') AS [NISIStatus], 
		ltrim(rtrim(replace(nisi.UnderlyingCauseNumber,' ','')))  AS [UnderlyingViolationCase],
		CASE WHEN ISNULL(nisi.UnderlyingTicketNumber, '') = '' THEN nisi.UnderlyingCauseNumber ELSE nisi.UnderlyingTicketNumber END AS TicketNumber, 
		CONVERT(VARCHAR(200), '') AS [ViolationDescription],
		CONVERT(VARCHAR (150), '') AS [ViolationCaseStatusInfo], 
		CONVERT(VARCHAR(50), ISNULL(nisi.PrintAnswerDate, '01/01/1900'), 101) AS [PrintAnswer],	
		CONVERT(VARCHAR(100),'--Choose--') AS [ContactStatus], ISNULL(t.NisiNotes, 'N/A') AS [Notes], 
		TrafficTickets.dbo.fn_HTP_Get_ClientNumbers(t.TicketID_PK) AS [ContactNumbers], t.LanguageSpeak AS [Language], 
		case ltrim(rtrim(len(ISNULL(t.Email, '')))) when 0 then 'N/A' ELSE t.Email END AS EmailAddress,
		CONVERT(VARCHAR(100), ISNULL(CONVERT(VARCHAR(50), t.NisiFollowUpDate, 101), 'N/A')) AS [FollowUpDate], 
		ISNULL(nisi.ContactTypeId, -1) AS ContactTypeId, 0 AS IsLetterPrinted, t.IsNisiFolloupDateFreezed
		INTO #tempClosed
    	FROM TrafficTickets.dbo.tblNISI_ClientsUploadedByLoader nisi
    	INNER JOIN TrafficTickets.dbo.tblTickets t ON nisi.TicketId = t.TicketID_PK
		WHERE t.Activeflag = 1 
		AND ISNULL(nisi.TicketId, 0) <> 0 
		
		CREATE NONCLUSTERED INDEX IX_TempCauseNumber ON #tempClosed(nisicause); 
		CREATE NONCLUSTERED INDEX IX_TempUnderlyingCause ON #tempClosed([UnderlyingViolationCase]); 
		
		-- Getting all records from event table having NISI cause number.
		SELECT REPLACE (e.causenumber, ' ', '') AS TempNISICause, e.* INTO #tempNISI
		FROM #tempClosed t INNER JOIN LoaderFilesArchive.dbo.tblEventExtractTemp e ON t.NisiCause	= REPLACE (e.causenumber, ' ', '')
		
		-- Getting all records from event table having underlying cause number.
		SELECT REPLACE (e.causenumber, ' ', '') AS TempUnderlineCause, e.* INTO #tempUnderlying
		FROM #tempClosed t INNER JOIN LoaderFilesArchive.dbo.tblEventExtractTemp e ON t.UnderlyingViolationCase	= REPLACE (e.causenumber, ' ', '')

		-- Getting the most recent records from nisi temp table
		SELECT MAX(e.RowId) AS RowId, e.TempNISICause AS CauseNum
		INTO #TempMaxByNISICause
		FROM #tempClosed t INNER JOIN #tempNISI e ON t.NisiCause	= e.TempNISICause		
		GROUP BY e.TempNISICause	
		-- Adding field in temp table for disposed cases.
		ALTER TABLE #tempClosed ADD isdisposed INT DEFAULT 0
		
		
		
		-- update is Disposed field for all cases which are present in disposed loader.
		UPDATE t
		SET t.isdisposed = 1
		FROM #tempClosed t INNER JOIN LoaderFilesArchive.dbo.tblClosedCases d
		ON replace(d.causenumber,' ','') = t.UnderlyingViolationCase
		
		
		-- Delete all the recs which has future NISI court date and letter is also printed. . . 
		DELETE t
		FROM #tempClosed t INNER JOIN #TempMaxByNISICause p ON t.NisiCause	= p.CauseNum
		INNER JOIN #tempNISI e ON e.RowId = p.rowId					
		WHERE
		isnull(t.isdisposed,0) = 0 AND
		(
		DATEDIFF(DAY,e.courtdate,GETDATE()) <= 0
		OR
		DATEDIFF(DAY,t.[PrintAnswer],'01/01/1900') = 0
		)
		
		
		
		--Delete all records which are not exist in event file based on NISI cause.. these should be no court date case.
		DELETE t
		FROM #tempClosed t WHERE t.nisicause NOT IN (SELECT TempNISICause FROM #tempNISI)
		
		-- Delete all those cases whose are present in event file with court date 1/1/1900.
		DELETE t FROM #tempClosed t INNER JOIN #TempMaxByNISICause p ON p.CauseNum = t.nisicause
		INNER JOIN #tempNISI e ON e.RowId = p.rowId	
		WHERE DATEDIFF(DAY,e.courtdate,'1/1/1900') = 0 
		
		
		
		-- Below logic is only for cases whose latter are printed.
		------------------------------------------------------------------------------------
		--Lookup NISI Cause numbers in HMC Event loader and get the most recent Records. . . 
		-- Updating NISI Status for those cases which are exists in event file and their answer is printed
		UPDATE t
		SET t.NISIStatus = 'Hearing ' + TrafficTickets.dbo.fn_DateFormat(e.[courtdate], 1, '/', '-', 0) + ' Ct #'+ REPLACE(e.[courtnumber],'ADMIN',' 0') + ' @ ' + TrafficTickets.dbo.Fn_FormateTime(e.[courttime])
		FROM #tempClosed t INNER JOIN #tempNISI  e ON e.TempNISICause = t.nisicause
		INNER JOIN #TempMaxByNISICause p ON p.rowid = e.RowId
		WHERE DATEDIFF(DAY,[PrintAnswer],'1/1/1900') <> 0	
		
		
		
		
		--Getting all the information of Underlying violation cause number for the NISI case. . .
		SELECT MAX(e.RowId) AS RowId, e.TempUnderlineCause AS UnderlyingCause
		INTO #TempMaxByUnderlyingCause
		FROM #tempUnderlying e INNER JOIN #tempClosed t ON t.UnderlyingViolationCase = e.TempUnderlineCause
		GROUP BY e.TempUnderlineCause
		
		-- Updating Underlying violation cause number voilation informaion
		UPDATE t
		SET t.[ViolationCaseStatusInfo] = vs.ShortDescription + ' ' + TrafficTickets.dbo.fn_DateFormat(e.CourtDate, 1, '/', '-', 0) + ' Ct #'+REPLACE(e.CourtNumber, 'ADMIN', ' 0') + ' @ ' + TrafficTickets.dbo.Fn_FormateTime(e.courttime)
		FROM #tempClosed t INNER JOIN #tempUnderlying e ON e.TempUnderlineCause = t.UnderlyingViolationCase
		INNER JOIN #TempMaxByUnderlyingCause p ON p.rowid = e.RowId
		INNER JOIN trafficTickets.dbo.tblCourtViolationStatus vs ON vs.[Description] = e.status
		
		DROP TABLE #TempMaxByUnderlyingCause
				
		-- Getting the violation description of Underlying cause number from HMC Arrignement loader. . . 
		SELECT  MAX(tl.rowId) AS RowId, Replace(tl.causenumber, ' ', '') AS UnderlyingCause, tl.violation
		INTO #violationsClosed
		FROM LoaderFilesArchive.dbo.tblLubbock tl  INNER JOIN #tempClosed t ON t.UnderlyingViolationCase = Replace(tl.causenumber, ' ', '')
		GROUP BY Replace(tl.causenumber, ' ', ''), tl.violation
		
		--Updating the violtion description which we get from HMC arrignment loader for the underlying cause number . . . 
		UPDATE t
		SET ViolationDescription = v.violation
		FROM #violationsClosed v INNER JOIN #tempClosed t ON t.UnderlyingViolationCase = v.UnderlyingCause
		
		DROP TABLE #violationsClosed
		-- Updating the contact info. . . 
		UPDATE t
		SET t.ContactStatus = ct.[Description]
		FROM #tempClosed t INNER JOIN TrafficTickets.dbo.tblNisiContactTypes ct
		ON ct.ContactTypeid = t.ContactTypeId
		
		
		ALTER TABLE #tempClosed
		ADD NISIGeneratedDateSortable DATETIME DEFAULT '01/01/1900' NOT NULL,
		PrintAnswerSortable DATETIME DEFAULT '01/01/1900' NOT NULL,
		FollowUpDateSortable DATETIME DEFAULT '01/01/1900' NOT NULL
		
		
		UPDATE #tempClosed
		SET NISIGeneratedDateSortable = CAST(NISIGeneratedDate AS DATETIME)
		WHERE ISDATE(NISIGeneratedDate) = 1
		
		UPDATE #tempClosed
		SET PrintAnswerSortable = CAST(PrintAnswer AS DATETIME)
		WHERE ISDATE(PrintAnswer) = 1
		
		UPDATE #tempClosed
		SET FollowUpDateSortable = CAST(FollowUpDate AS DATETIME)
		WHERE ISDATE(FollowUpDate) = 1
	
		-- Getting Closed + Pending cases
		IF (@status = 'Pending')
		BEGIN
			
			DELETE t		
			FROM #tempClosed t INNER JOIN LoaderFilesArchive.dbo.tblClosedCases disposed
			ON t.NISICause = LTRIM(RTRIM(REPLACE(disposed.causenumber,' ','')))
			
			SELECT t.* INTO #tempClosedFinal  
			FROM #tempClosed t INNER JOIN #tempUnderlying e 
			ON e.TempUnderlineCause = t.UnderlyingViolationCase
			
			INNER JOIN tblCourtViolationStatus cv ON cv.[Description] = e.[status]
			WHERE (cv.CategoryID = 7)-- AND DATEDIFF(DAY, t.NISIGeneratedDate, e.CourtDate) < 0)
			
			SELECT MIN(RowId) AS RowId, [LastName], [FirstName]
			INTO #PendingClosed
			FROM #tempClosedFinal
			GROUP BY [LastName], [FirstName]
			
			UPDATE #tempClosedFinal
			SET ContactStatus = '*****', [Notes] = '*****',  [ContactNumbers] = '*****',[Language] = '*****', [EmailAddress] = '*****', [FollowUpDate] = '*****'
			WHERE RowId NOT IN (SELECT m.RowId FROM #PendingClosed m)
			
			SELECT DISTINCT * FROM #tempClosedFinal
			ORDER BY [FirstName], [LastName], RowId
			
			DROP TABLE #tempClosedFinal
			DROP TABLE #PendingClosed
			DROP TABLE #tempClosed
			
			DROP TABLE #tempNISI
			DROP TABLE #tempUnderlying
			DROP TABLE #TempMaxByNISICause
		END
		-- Getting Closed + Rebonded cases
		IF (@status = 'Rebonded')
		BEGIN
			
			SELECT t.* into #tempRebondedClosed FROM #tempClosed t 
			INNER JOIN #tempUnderlying e 
			ON e.TempUnderlineCause = t.UnderlyingViolationCase
			INNER JOIN tblCourtViolationStatus cv ON cv.[Description] = e.[status]
			WHERE (
					(cv.CategoryID IN (2,5,4)
				  ) 
				  AND 
				  (
				  	cv.CourtViolationStatusID IN (5,21,26,47,103,151,153,155,156,157,158,159,161,162,163,167,170,171,177,183,198,216,223,274)
				  )
			) 
			OR
			(isnull(t.isdisposed,0) = 1)
			--AND (DATEDIFF(DAY, t.NISIGeneratedDate, e.CourtDate) < 0))
			
			
			SELECT MIN(RowId) AS RowId, [LastName], [FirstName]
			INTO #RebondedClosed
			FROM #tempRebondedClosed
			GROUP BY [LastName], [FirstName]
			
			UPDATE #tempRebondedClosed
			SET ContactStatus = '*****', [Notes] = '*****',  [ContactNumbers] = '*****',[Language] = '*****', [EmailAddress] = '*****', [FollowUpDate] = '*****'
			WHERE RowId NOT IN (SELECT m.RowId FROM #RebondedClosed m)
			
			SELECT DISTINCT * FROM #tempRebondedClosed
			ORDER BY [FirstName], [LastName], RowId
			
			DROP TABLE #tempRebondedClosed
			DROP TABLE #tempClosed
			DROP TABLE #RebondedClosed
			DROP TABLE #tempNISI
			DROP TABLE #tempUnderlying
			DROP TABLE #TempMaxByNISICause
		END
		-- Getting All Closed cases
		IF (@status = 'All')
		BEGIN
			SELECT t.* INTO #tempClosedAll 
			FROM #tempClosed t INNER JOIN #tempUnderlying e 
			ON e.TempUnderlineCause = t.UnderlyingViolationCase
			INNER JOIN tblCourtViolationStatus cv ON cv.[Description] = e.[status]
			WHERE 
			(
				(cv.CategoryID IN (2,5,4,7)
			) 
			AND 
			(				
				cv.CourtViolationStatusID IN (5,21,26,47,103,146,151,153,155,156,157,158,159,161,162,163,167,170,171,177,183,198,216,223,274)
			)
			) -- Arr, Judge and Jury and courtdate is greater then nisi generated date
			 OR
			(isnull(t.isdisposed,0) = 1)			
			SELECT MIN(RowId) AS RowId, [LastName], [FirstName]
			INTO #AllClosed
			FROM #tempClosedAll
			GROUP BY [LastName], [FirstName]
			
			UPDATE #tempClosedAll
			SET ContactStatus = '*****', [Notes] = '*****',  [ContactNumbers] = '*****',[Language] = '*****', [EmailAddress] = '*****', [FollowUpDate] = '*****'
			WHERE RowId NOT IN (SELECT m.RowId FROM #AllClosed m)
			
			SELECT DISTINCT * FROM #tempClosedAll
			ORDER BY [FirstName], [LastName], RowId
			
			DROP TABLE #tempClosedAll
			DROP TABLE #AllClosed
			DROP TABLE #tempClosed
			DROP TABLE #tempNISI
			DROP TABLE #tempUnderlying
			DROP TABLE #TempMaxByNISICause
		END	
	END
	-- Searching for Active clients who has Blank NISI Status or Cases have future Court Date. . . 
	ELSE
	BEGIN
		-- Getting all the NISI cases which uploade by our loader. . . 
		SELECT nisi.RowID, ISNULL(nisi.TicketId, 0) AS TicketID, CONVERT(VARCHAR(50), nisi.[NISI Issue Date], 101) AS NISIGeneratedDate, 
		case when replace(isnull(nisi.[First Name],'') + isnull(nisi.[Middle Name],'') + isnull(nisi.[Last Name],''),' ','') = '' then t.Firstname
		else nisi.[First Name] end AS [FirstName], 
		case when replace(isnull(nisi.[First Name],'') + isnull(nisi.[Middle Name],'') + isnull(nisi.[Last Name],''),' ','') = '' then t.MiddleName
		else nisi.[Middle Name] end AS [MidName],
		case when replace(isnull(nisi.[First Name],'') + isnull(nisi.[Middle Name],'') + isnull(nisi.[Last Name],''),' ','') = '' then t.Lastname
		else nisi.[Last Name] end AS [LastName], 
		ISNULL(nisi.BondForfeitureAmount, 0.00) AS [BondAmount], 
		ltrim(rtrim(Replace(nisi.NisiCauseNumber,' ',''))) AS [NISICause], CONVERT(VARCHAR(100), '') AS [NISIStatus], 
		ltrim(rtrim(Replace(nisi.UnderlyingCauseNumber,' ',''))) AS [UnderlyingViolationCase],
		CASE WHEN ISNULL(nisi.UnderlyingTicketNumber, '') = '' THEN nisi.UnderlyingCauseNumber ELSE nisi.UnderlyingTicketNumber END AS TicketNumber, 
		CONVERT(VARCHAR(200), '') AS [ViolationDescription],
		CONVERT(VARCHAR (150), '') AS [ViolationCaseStatusInfo], 
		CONVERT(VARCHAR(50), ISNULL(nisi.PrintAnswerDate, '01/01/1900'), 101) AS [PrintAnswer],	
		CONVERT(VARCHAR(100),'--Choose--') AS [ContactStatus], ISNULL(t.NisiNotes, 'N/A') AS [Notes], 
		TrafficTickets.dbo.fn_HTP_Get_ClientNumbers(t.TicketID_PK) AS [ContactNumbers], t.LanguageSpeak AS [Language], 
		case ltrim(rtrim(len(ISNULL(t.Email, '')))) when 0 then 'N/A' ELSE t.Email END AS EmailAddress,
		CONVERT(VARCHAR(100), ISNULL(CONVERT(VARCHAR(50), t.NisiFollowUpDate, 101), 'N/A')) AS [FollowUpDate], 
		ISNULL(nisi.ContactTypeId, -1) AS ContactTypeId, 0 AS IsLetterPrinted, t.IsNisiFolloupDateFreezed
		INTO #temp
    	FROM TrafficTickets.dbo.tblNISI_ClientsUploadedByLoader nisi
    	INNER JOIN TrafficTickets.dbo.tblTickets t ON nisi.TicketId = t.TicketID_PK
		WHERE t.Activeflag = 1 
		AND ISNULL(nisi.TicketId, 0) <> 0 
		
		CREATE NONCLUSTERED INDEX IX_TempCauseNumber ON #temp(nisicause); 
		CREATE NONCLUSTERED INDEX IX_TempUnderlyingCause ON #temp([UnderlyingViolationCase]); 
		
		-- Deleting all the case which are marked as disposed by our loader. . . 
		DELETE t		
		FROM #temp t INNER JOIN LoaderFilesArchive.dbo.tblClosedCases disposed
		ON t.NISICause = ltrim(rtrim(replace(disposed.causenumber,' ','')))
		
		DELETE t		
		FROM #temp t INNER JOIN LoaderFilesArchive.dbo.tblClosedCases disposed
		ON t.UnderlyingViolationCase = ltrim(rtrim(replace(disposed.causenumber,' ','')))
		
		-- Getting all records from event table having NISI cause number.
		SELECT REPLACE (e.causenumber, ' ', '') as tmpNISIActiveCause, e.* INTO #tempNISIActive
		FROM #temp t INNER JOIN LoaderFilesArchive.dbo.tblEventExtractTemp e ON t.NisiCause	= REPLACE (e.causenumber, ' ', '')
		
		-- Getting all records from event table having underlying cause number.
		SELECT REPLACE (e.causenumber, ' ', '') AS tmpUnderlyingCause,e.* INTO #tempUnderlyingActive
		FROM #temp t INNER JOIN LoaderFilesArchive.dbo.tblEventExtractTemp e ON t.UnderlyingViolationCase	= REPLACE (e.causenumber, ' ', '')
		
		-- Updating the field for clients who has printed the letters. . . 
		UPDATE t
		SET t.IsLetterPrinted = 1
		FROM #temp t 
		WHERE DATEDIFF(day, t.PrintAnswer, '01/01/1900') <> 0
				
		-- Below logic is only for cases whose latter are printed.
		------------------------------------------------------------------------------------
		--Lookup NISI Cause numbers in HMC Event loader and get the most recent Records. . . 
		SELECT MAX(e.RowId) AS RowId, e.tmpNISIActiveCause AS CauseNum
		INTO #TempMaxNISICauseActive
		FROM #temp t INNER JOIN #tempNISIActive e ON t.NisiCause	= e.tmpNISIActiveCause
		AND t.IsLetterPrinted = 1
		GROUP BY e.tmpNISIActiveCause
		
		-- Delete all the recs which has past NISI court date and letter is also printed. . . 
		DELETE t
		FROM #temp t INNER JOIN #TempMaxNISICauseActive p ON t.NisiCause	= p.CauseNum
		INNER JOIN #tempNISIActive e ON e.RowId = p.rowId
		AND t.IsLetterPrinted = 1	
		WHERE DATEDIFF(DAY,e.courtdate,GETDATE())>0 AND DATEDIFF(DAY,e.courtdate,'1/1/1900') <>0
		
		UPDATE t
		SET t.NISIStatus = 'No Court Date'
		FROM #temp t 
		WHERE t.IsLetterPrinted = 1
		AND t.nisicause NOT IN (SELECT tmpNISIActiveCause FROM #tempNISIActive)
		
		UPDATE t
		SET t.NISIStatus = 'Hearing ' + TrafficTickets.dbo.fn_DateFormat(e.[courtdate], 1, '/', '-', 0) + ' Ct #'+ REPLACE(e.[courtnumber],'ADMIN',' 0') + ' @ ' + TrafficTickets.dbo.Fn_FormateTime(e.[courttime])
		FROM #temp t INNER JOIN #tempNISIActive  e ON e.tmpNISIActiveCause = t.nisicause
		INNER JOIN #TempMaxNISICauseActive p ON p.rowid = e.RowId
		WHERE t.IsLetterPrinted = 1
		AND DATEDIFF(DAY, e.courtdate, '01/01/1900') <> 0
		
		
		UPDATE t
		SET t.NISIStatus = 'No Court Date'
		FROM #temp t INNER JOIN #tempNISIActive  e ON e.tmpNISIActiveCause = t.nisicause
		INNER JOIN #TempMaxNISICauseActive p ON p.rowid = e.RowId
		WHERE t.IsLetterPrinted = 1
		AND DATEDIFF(DAY, e.courtdate, '01/01/1900') = 0
		
		SELECT MAX(e.RowId) AS RowId, e.tmpUnderlyingCause AS UnderlyingCause
		INTO #TempMaxUnderlyingCauseActive
		FROM #tempUnderlyingActive e INNER JOIN #temp t ON t.UnderlyingViolationCase = e.tmpUnderlyingCause
		GROUP BY e.tmpUnderlyingCause
		
		
		UPDATE t
		SET t.[ViolationCaseStatusInfo] = vs.ShortDescription + ' ' + TrafficTickets.dbo.fn_DateFormat(e.CourtDate, 1, '/', '-', 0) + ' Ct #'+REPLACE(e.CourtNumber, 'ADMIN', ' 0') + ' @ ' + TrafficTickets.dbo.Fn_FormateTime(e.courttime)
		FROM #temp t INNER JOIN #tempUnderlyingActive e ON e.tmpUnderlyingCause = t.UnderlyingViolationCase
		INNER JOIN #TempMaxUnderlyingCauseActive p ON p.rowid = e.RowId
		INNER JOIN trafficTickets.dbo.tblCourtViolationStatus vs ON vs.[Description] = e.status
		
		--DROP TABLE #TempMaxUnderlyingCauseActive
		
		
		SELECT  MAX(tl.rowId) AS RowId, Replace(tl.causenumber, ' ', '') AS UnderlyingCause, tl.violation
		INTO #violations
		FROM LoaderFilesArchive.dbo.tblLubbock tl  INNER JOIN #temp t ON t.UnderlyingViolationCase = Replace(tl.causenumber, ' ', '')
		GROUP BY Replace(tl.causenumber, ' ', ''), tl.violation
		
		
		UPDATE t
		SET ViolationDescription = v.violation
		FROM #violations v INNER JOIN #temp t ON t.UnderlyingViolationCase = v.UnderlyingCause
		
		UPDATE t
		SET t.ContactStatus = ct.[Description]
		FROM #temp t INNER JOIN TrafficTickets.dbo.tblNisiContactTypes ct
		ON ct.ContactTypeid = t.ContactTypeId
		
		DROP TABLE #violations
		
		
		ALTER TABLE #temp
		ADD NISIGeneratedDateSortable DATETIME DEFAULT '01/01/1900' NOT NULL,
		PrintAnswerSortable DATETIME DEFAULT '01/01/1900' NOT NULL,
		FollowUpDateSortable DATETIME DEFAULT '01/01/1900' NOT NULL
		
		
		UPDATE #temp
		SET NISIGeneratedDateSortable = CAST(NISIGeneratedDate AS DATETIME)
		WHERE ISDATE(NISIGeneratedDate) = 1
		
		UPDATE #temp
		SET PrintAnswerSortable = CAST(PrintAnswer AS DATETIME)
		WHERE ISDATE(PrintAnswer) = 1
		
		UPDATE #temp
		SET FollowUpDateSortable = CAST(FollowUpDate AS DATETIME)
		WHERE ISDATE(FollowUpDate) = 1
		
		-- Getting Active + Pending cases
		IF (@status = 'Pending')
		BEGIN
			SELECT t.* INTO #ActivePending 
			FROM #temp t INNER JOIN #TempMaxUnderlyingCauseActive a 
			ON a.UnderlyingCause = t.UnderlyingViolationCase
			inner join #tempUnderlyingActive e on t.UnderlyingViolationCase = e.tmpUnderlyingCause and a.rowid = e.rowid
			INNER JOIN tblCourtViolationStatus cv ON cv.[Description] = e.[status]
			WHERE (cv.CategoryID = 7)
			
			
			SELECT MIN(RowId) AS RowId, [LastName], [FirstName]
			INTO #ActivePendingFormating
			FROM #ActivePending
			GROUP BY [LastName], [FirstName]
			
			UPDATE #ActivePending
			SET ContactStatus = '*****', [Notes] = '*****',  [ContactNumbers] = '*****',[Language] = '*****', [EmailAddress] = '*****', [FollowUpDate] = '*****'
			WHERE RowId NOT IN (SELECT m.RowId FROM #ActivePendingFormating m)
			
			SELECT DISTINCT * FROM #ActivePending
			ORDER BY [FirstName], [LastName], RowId
			
			DROP TABLE #ActivePending
			DROP TABLE #ActivePendingFormating
			DROP TABLE #temp
			
			DROP TABLE #tempNISIActive
			DROP TABLE #tempUnderlyingActive
			DROP TABLE #TempMaxNISICauseActive
			DROP TABLE #TempMaxUnderlyingCauseActive
					
		END
		-- Getting Active + Rebonded cases
		IF (@status = 'Rebonded')
		BEGIN
			SELECT t.* INTO #activeRebonded 
			FROM #temp t INNER JOIN #TempMaxUnderlyingCauseActive a 
			ON a.UnderlyingCause = t.UnderlyingViolationCase
			inner join #tempUnderlyingActive e 
			on t.UnderlyingViolationCase = e.tmpUnderlyingCause and a.rowid = e.rowid
			INNER JOIN tblCourtViolationStatus cv ON cv.[Description] = e.[status]
			WHERE 
			cv.CategoryID IN (2,5,4)
				
				-- Arr, Judge and Jury and courtdate is greater then nisi generated date
			AND (cv.CourtViolationStatusID IN (5,21,26,47,151,153,155,156,157,158,159,161,162,163,167,170,171,177,183,198,216,274)) 
		
			
			
			SELECT MIN(RowId) AS RowId, [LastName], [FirstName]
			INTO #ActiveRebondedFormating
			FROM #activeRebonded
			GROUP BY [LastName], [FirstName]
			
			UPDATE #activeRebonded
			SET ContactStatus = '*****', [Notes] = '*****',  [ContactNumbers] = '*****',[Language] = '*****', [EmailAddress] = '*****', [FollowUpDate] = '*****'
			WHERE RowId NOT IN (SELECT m.RowId FROM #ActiveRebondedFormating m)
			
			SELECT DISTINCT * FROM #activeRebonded
			ORDER BY [FirstName], [LastName], RowId
			
			DROP TABLE #activeRebonded
			DROP TABLE #ActiveRebondedFormating
			DROP TABLE #temp
			
			DROP TABLE #tempNISIActive
			DROP TABLE #tempUnderlyingActive
			DROP TABLE #TempMaxNISICauseActive
			DROP TABLE #TempMaxUnderlyingCauseActive
			
			
		END
		-- Getting All Active cases
		IF (@status = 'All')
		BEGIN
			SELECT t.* INTO #activeAll 
			FROM #temp t INNER JOIN #TempMaxUnderlyingCauseActive a 
			ON a.UnderlyingCause = t.UnderlyingViolationCase
			inner join #tempUnderlyingActive e 
			on t.UnderlyingViolationCase = e.tmpUnderlyingCause and a.rowid = e.rowid
			INNER JOIN tblCourtViolationStatus cv ON cv.[Description] = e.[status]
			WHERE 
			cv.CategoryID IN (2,5,4,7)		
			OR 
			cv.CourtViolationStatusID IN (5,21,26,47,151,153,155,156,157,158,159,161,162,163,167,170,171,177,183,198,216,274)		
			
			
			
			SELECT MIN(RowId) AS RowId, [LastName], [FirstName]
			INTO #ActiveAllFormating
			FROM #activeAll
			GROUP BY [LastName], [FirstName]
			
			UPDATE #activeAll
			SET ContactStatus = '*****', [Notes] = '*****',  [ContactNumbers] = '*****',[Language] = '*****', [EmailAddress] = '*****', [FollowUpDate] = '*****'
			WHERE RowId NOT IN (SELECT m.RowId FROM #ActiveAllFormating m)
			
			SELECT DISTINCT * FROM #activeAll
			ORDER BY [FirstName], [LastName], RowId 
			
			DROP TABLE #activeAll
			DROP TABLE #ActiveAllFormating
			DROP TABLE #temp
			
			DROP TABLE #tempNISIActive
			DROP TABLE #tempUnderlyingActive
			DROP TABLE #TempMaxNISICauseActive
			DROP TABLE #TempMaxUnderlyingCauseActive
		END				
END
END

GO
	GRANT EXECUTE ON [dbo].[USP_HTP_GET_NISI_ClientsInfo] TO dbr_webuser
GO
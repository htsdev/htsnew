﻿set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


/****** Object:  StoredProcedure [dbo].[USP_HTP_GET_CaseStatusByCourtId]  ******/     
/*        
Created By     : Fahad Muhammad Qureshi.
Created Date   : 06/16/2009  
TasK		   : 6036        
Business Logic : This procedure Get the Case Status by Court ID and Ticket ID.
           
Parameter:       
   @TicketID : Getting records criteria with respect to TicketId   
   @CourtId	 : Getting records criteria with respect to CourtId 
   @Status   : Getting records criteria with respect to Status 
*/      

CREATE PROCEDURE [dbo].[USP_HTP_GET_CaseStatusByCourtId]--140394,3004,103
	@Ticketid INT,
	@CourtId INT,
	@Status INT
AS
if ( Select Count(*) from tblticketsviolations tv where (tv.TicketID_PK = @Ticketid) AND (tv.CourtViolationstatusIDmain = @Status) AND (tv.CourtID=@CourtId)) > 0
Select 1 
Else
Select 0 
GO
GRANT EXEC ON [dbo].[USP_HTP_GET_CaseStatusByCourtId] TO dbr_webuser
GO  





/****** Object:  StoredProcedure [dbo].[usp_HTS_Bond_Report]    Script Date: 10/31/2008 15:43:39 ******/
/******   
Alter by:  Syed Muhammad Ozair  
  
Business Logic : this procedure is used to Insert the information in document tracking batch tickets table and
				 also insert the entry in case history for the related cases. it also gets the required information 
				 that will be displayed under bond summary report.			  
  
List of Parameters:   
 @TVIDS : ticketsviilationids
 @DocumentBatchID : docuemnt id 
 @updateflag : 
 @employee : employee id of current logged in user  
  
List of Columns:    
 ticketid :
 causeno :
 ticketno : 
 lastname : 
 firstname :
 dlnumber : 
 dob : 
 courtdate : 
 midnum: 
 Time : 
 status :  
 bonddate : 
 ticketsviolationid :
 docid : document id
 bondflag :
   
******/

-- [dbo].[usp_HTS_Bond_Report] '161065,118124,',1115,1,3991
ALTER PROCEDURE [dbo].[usp_HTS_Bond_Report]
	@TVIDS
AS
	VARCHAR(MAX), 
	@DocumentBatchID AS INT, 
	@updateflag AS INT , 
	@employee AS INT 
	
	AS                                
	
	DECLARE @FLAG AS VARCHAR(50)    
	
	DECLARE @TBLTEMPTVID TABLE (TICKETVIOLATIONID INT)            
	DECLARE @temp TABLE 
	        (
	            ticketid INT,
	            causeno VARCHAR(20),
	            ticketno VARCHAR(20),
	            lastname VARCHAR(20),
	            firstname VARCHAR(20),
	            dlnumber VARCHAR(30),
	            dob VARCHAR(12),
	            courtdate VARCHAR(12),
	            midnum VARCHAR(20),
	            [Time] INT,
	            STATUS VARCHAR(20),
	            bonddate VARCHAR(12),
	            bondflag VARCHAR(6),
	            ticketsviolationid INT,
	            docid INT,
	            needsign VARCHAR(3)
	        )          
	
	IF @TVIDS = ''
	BEGIN
	    INSERT INTO @temp
	    SELECT DISTINCT
	           t.ticketid_pk AS ticketid,
	           tv.casenumassignedbycourt AS causeno,
	           tV.REFCASENUMBER AS TICKETNO,
	           UPPER(t.lastname) AS lastname,
	           UPPER(t.firstname) AS firstname,
	           t.dlnumber,
	           CONVERT(VARCHAR(12), t.dob, 101) AS dob,
	           CONVERT(VARCHAR(12), TV.COURTDATEMAIN, 101) AS COURTDATE,
	           t.midnum,
	           ISNULL(DATEDIFF(DAY, tvl.recdate, GETDATE()), 0) AS TIME,
	           (
	               CASE 
	                    WHEN tcv.shortdescription = 'BOND' THEN 'BND'
	                    ELSE tcv.shortdescription
	               END
	           ) AS STATUS,
	           CONVERT(VARCHAR(12), tvl.recdate, 101) AS BondDate,
	           (CASE WHEN t.bondflag = 1 THEN 'B;' ELSE '' END) + (CASE WHEN TF.FlagID IS NULL THEN '' ELSE 'N;' END) AS 
	           bondflag,
	           tv.ticketsviolationid,
	           @DocumentBatchID AS DOCID,
	           NeedSign = ISNULL(
	               (
	                   SELECT TOP 1 CASE 
	                                     WHEN ISNULL(sb.scanbookid, 0) = 0 THEN 
	                                          'S;'
	                                     ELSE ''
	                                END
	                   FROM   tblscanbook sb
	                   WHERE  sb.ticketid = t.ticketid_pk
	                          AND sb.doctypeid = 8
	                          AND ISNULL(sb.isdeleted, 0) = 0
	               ),
	               'S;'
	           )
	    FROM   tbltickets t
	           INNER JOIN tblticketsviolations tv
	                ON  t.ticketid_pk = tv.ticketid_pk
	           INNER JOIN tblcourtviolationstatus tcv
	                ON  tv.courtviolationstatusidmain = tcv.courtviolationstatusid
	           LEFT OUTER JOIN tblticketsviolationlog tvl
	                ON  tv.ticketsviolationid = tvl.ticketviolationid
	                AND tvl.recdate = (
	                        SELECT MAX(recdate)
	                        FROM   tblticketsviolationlog
	                        WHERE  ticketviolationid = tvl.ticketviolationid
	                               AND newverifiedstatus <> oldverifiedstatus
	                    )
	           LEFT OUTER JOIN tblTicketsFlag TF
	                ON  tv.TICKETID_PK = TF.TICKETID_PK
	                AND TF.FlagID = 17
	    WHERE  TCV.CATEGORYID = 12
	           AND t.activeflag = 1
	           AND tv.courtid IN (3001, 3002, 3003, 3075) -- Rab Nawaz Khan 10327 06/14/2012 Added the HMC-W Court in Bond Setting Report. . .
	           AND tv.courtdatemain IS NOT NULL
	           AND tV.courtviolationstatusidmain <> 80
	           AND tV.courtviolationstatusidmain <> 162
	    ORDER BY
	           [lastname], -- Sadaf Aziz 10657 01/08/2013 Added Time in Order By
	           [firstname],
	           [Time] DESC    
	    
	    
	    IF @updateflag = 1
	    BEGIN
	        DECLARE @temp2 TABLE 
	                (
	                    COUNTER INT IDENTITY(1, 1) NOT NULL,
	                    ticketsviolationid INT
	                )     
	        
	        INSERT INTO @temp2
	        SELECT ticketsviolationid
	        FROM   @temp    
	        
	        DECLARE @loopcounter AS INT            
	        SET @loopcounter = (
	                SELECT COUNT(*)
	                FROM   @temp2
	            )
	        
	        WHILE @loopcounter >= 1
	        BEGIN
	            -- Noufil 7939 06/24/2010 Update VemployeeId with Employee ID to get Correct username in client history 
	            UPDATE tblticketsviolations
	            SET    courtviolationstatusidmain = 202,
	                   VEmployeeID = @employee
	            WHERE  ticketsviolationid = (
	                       SELECT ticketsviolationid
	                       FROM   @temp2
	                       WHERE  COUNTER = @loopcounter
	                   )
	            
	            SET @loopcounter = @loopcounter -1
	        END
	    END              
	    
	    
	    SELECT ticketid,
	           causeno,
	           ticketno,
	           lastname,
	           firstname,
	           dlnumber,
	           dob,
	           courtdate,
	           midnum,
	           [Time],
	           STATUS,
	           bonddate,
	           ticketsviolationid,
	           docid,
	           bondflag + needsign AS bondflag
	   FROM   @temp
	    ORDER BY [lastname], -- Sadaf Aziz 10657 01/08/2012 Added Order By
	           [firstname] ,
	           [Time] DESC
	    
	END
	ELSE
	BEGIN
	    INSERT INTO @TBLTEMPTVID
	    SELECT *
	    FROM   dbo.sap_string_param(@TVIDS)                           
	    
	    INSERT INTO @temp
	    SELECT t.ticketid_pk AS ticketid,
	           tv.casenumassignedbycourt AS causeno,
	           tV.REFCASENUMBER AS TICKETNO,
	           UPPER(t.lastname) AS lastname,
	           UPPER(t.firstname) AS firstname,
	           t.dlnumber,
	           CONVERT(VARCHAR(12), t.dob, 101) AS dob,
	           CONVERT(VARCHAR(12), TV.COURTDATEMAIN, 101) AS COURTDATE,
	           t.midnum,
	           ISNULL(DATEDIFF(DAY, tv.bonddate, GETDATE()), 0) AS TIME,
	           (
	               CASE 
	                    WHEN tcv.shortdescription = 'BOND' THEN 'BND'
	                    ELSE tcv.shortdescription
	               END
	           ) AS STATUS,
	           CONVERT(VARCHAR(12), tv.bonddate, 101) AS BondDate,
	           (CASE WHEN t.bondflag = 1 THEN 'B' ELSE '' END) AS BondFlag,
	           tv.ticketsviolationid,
	           @DocumentBatchID AS DOCID,
	           NeedSign = ISNULL(
	               (
	                   SELECT TOP 1 CASE 
	                                     WHEN ISNULL(sb.scanbookid, 0) = 0 THEN 
	                                          'S;'
	                                     ELSE ''
	                                END
	                   FROM   tblscanbook sb
	                   WHERE  sb.ticketid = t.ticketid_pk
	                          AND sb.doctypeid = 8
	                          AND ISNULL(sb.isdeleted, 0) = 0
	               ),
	               'S;'
	           )
	    FROM   tbltickets t
	           INNER JOIN tblticketsviolations tv
	                ON  t.ticketid_pk = tv.ticketid_pk
	           INNER JOIN tblcourtviolationstatus tcv
	                ON  tv.courtviolationstatusidmain = tcv.courtviolationstatusid
	           INNER JOIN tblcourts tc
	                ON  tv.courtid = tc.courtid
	    WHERE  TV.TICKETSVIOLATIONID IN (SELECT TICKETVIOLATIONID
	                                     FROM   @TBLTEMPTVID)
	           AND t.activeflag = 1
	           AND tv.courtid IN (3001, 3002, 3003, 3075) -- Rab Nawaz Khan 10327 06/14/2012 Added the HMC-W Court in Bond Setting Report. . .
	           AND TCV.CATEGORYID = 12
	           AND tv.courtdatemain IS NOT NULL
	           AND tV.courtviolationstatusidmain <> 80
	           AND tV.courtviolationstatusidmain <> 162
	    ORDER BY
	          [lastname],
	           [firstname] , -- Sadaf Aziz 10657 01/08/2013 Added Time in Order By
	           [Time] DESC 
	                                      
	    
	    
	    IF @updateflag = 1
	    BEGIN
	        DECLARE @temp1 TABLE 
	                (COUNTER INT IDENTITY(1, 1) NOT NULL, TICKETVIOLATIONID INT)     
	        
	        INSERT INTO @temp1
	        SELECT TICKETVIOLATIONID
	        FROM   @TBLTEMPTVID
	        
	        DECLARE @loopcounter1 AS INT            
	        SET @loopcounter1 = (
	                SELECT COUNT(*)
	                FROM   @temp1
	            )
	        
	        WHILE @loopcounter1 >= 1
	        BEGIN
	            -- Noufil 7939 06/24/2010 Update VemployeeId with Employee ID to get Correct username in client history 
	            UPDATE tblticketsviolations
	            SET    courtviolationstatusidmain = 202,
	                   VEmployeeID = @employee
	            WHERE  ticketsviolationid = (
	                       SELECT TICKETVIOLATIONID
	                       FROM   @temp1
	                       WHERE  COUNTER = @loopcounter1
	                   )
	            
	            SET @loopcounter1 = @loopcounter1 -1
	        END
	        -- Noufil 4980 10/31/2008 update bondwaiting followupdate on moving record from bond setting.
	        --Sabir Khan 5676 03/19/2009 bondwaitingFollowUpdate logic has been changed from 5 business days to 6 business days...
	        UPDATE tbltickets
	        SET    bondwaitingFollowUpdate = dbo.fn_getnextbusinessday(GETDATE(), 6)
	        WHERE  ticketid_pk IN (SELECT ticketid
	                               FROM   @temp)
	        
	        DECLARE @loopcounter21 AS INT		
	        SET @loopcounter21 = (
	                SELECT COUNT(ticketid)
	                FROM   @temp
	            )
	        
	        -- Noufil 8123 08/04/2010 Code commented becuase history note was inserting multiple times.
	        --WHILE @loopcounter21 >= 1
	        --BEGIN
	            INSERT INTO tblticketsnotes
	              (
	                ticketid,
	                [Subject],
	                employeeid
	              )
	            --Sabir Khan 5676 03/19/2009 bondwaitingFollowUpdate logic has been changed from 5 business days to 6 business days...
	            SELECT DISTINCT ticketid,
	                   'Bond waiting follow up date changed to ' + CONVERT(VARCHAR, dbo.fn_getnextbusinessday(GETDATE(), 6), 101),
	                   @employee
	            FROM   @temp
	            
	            SET @loopcounter21 = @loopcounter21 -1
	        --END
	    END          
	    
	    SELECT ticketid,
	           causeno,
	           ticketno,
	           lastname,
	           firstname,
	           dlnumber,
	           dob,
	           courtdate,
	           midnum,
	           [Time],
	           STATUS,
	           bonddate,
	           ticketsviolationid,
	           docid,
	           bondflag + needsign AS bondflag
	    FROM   @temp
	    ORDER BY [lastname], -- Sadaf Aziz 10657 01/08/2012 Added Order By
	           [firstname] ,
	           [Time] DESC
	    
	END          
	
	IF @DocumentBatchID <> 0
	BEGIN
	    DECLARE @docid AS VARCHAR(20)      
	    SET @docid = CONVERT(VARCHAR(20), @DocumentBatchID, 101)      
	    
	    INSERT INTO tbl_htp_documenttracking_batchTickets
	      (
	        BatchID_FK,
	        TicketID_FK
	      )
	    SELECT DISTINCT DOCID,
	           ticketid
	    FROM   @temp    
	    
	    INSERT INTO tblTicketsNotes
	      (
	        TicketID,
	        SUBJECT,
	        EmployeeID
	      )
	    SELECT TicketID_FK,
	           '<a href="javascript:window.open(''../quickentrynew/PreviewPrintHistory.aspx?filename=' + 'BOND-' + 
	           @docid + ''');void('''');"''>Bond Setting Summary DOC ID : ' + @docid
	           + '</a>',
	           @employee
	    FROM   tbl_htp_documenttracking_batchTickets
	    WHERE  BatchID_FK = @DocumentBatchID
	END
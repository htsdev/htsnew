﻿ /******  
* Created By :	  Saeed Ahmed
* Create Date :   11/05/2010 
* Task ID :		  8501
* Business Logic :  This procedure is used to Get get list of all available doctors.
* List of Parameter :
* Column Return :     
******/
CREATE PROCEDURE dbo.usp_HTP_GetDoctors
AS
SELECT d.Id,firstname + ' ' + d.LastName AS doctorName FROM Doctor d
GO

GRANT EXECUTE ON dbo.usp_HTP_GetDoctors TO dbr_webuser
GO
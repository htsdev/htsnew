 
/****** Object:  StoredProcedure [dbo].[USP_HTS_Get_EMailTrialNotification]    Script Date: 07/16/2008 19:59:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

  
ALTER procedure [dbo].[USP_HTS_Get_EMailTrialNotification]  
as  
  
create table #TempTable  
(  
 ID int identity, TicketID int,ClientName varchar(250),CustomerName varchar(250), EmployeeID int, EMailAddress varchar(250)  
)  
  
insert into #TempTable  
--- Geting those records which has future date and status in JURRY,ARR and courts are out side courts  
SELECT    distinct 
 t.TicketID_PK As TicketID,  
 isnull(t.LastName,'') + ', ' + isnull(t.FirstName,'') As ClientName,  
 isnull(t.LastName,'') + ' ' + isnull(t.FirstName,'') As CustomerName,  
 t.EmployeeID,  
 t.Email AS EMailAddress   
    
FROM    tblTickets t     
INNER JOIN    
 tblTicketsViolations tv     
ON  t.TicketID_PK = tv.TicketID_PK     
LEFT OUTER JOIN    
 tblCourtViolationStatus cvs     
ON  tv.CourtViolationStatusIDmain = cvs.CourtViolationStatusID  
where (charindex('@',isnull(t.Email,''),1)>0) and (datediff (day,getdate(),isnull(tv.CourtDateMain,'01/01/1900')) >= 7 )  
and cvs.CategoryID in (2,3,4,5) and tv.CourtID not in (3001,3002,3003) And isnull(t.IsEMailedTrialLetter,0) = 0  
and t.ticketid_pk not in (select ticketid_pk from tblticketsflag where ticketid_pk =t.ticketid_pk and flagid=7)--Checking No Letters  
and t.ActiveFlag=1  
and tv.courtviolationstatusidmain not in(50,80)-- Case should not be disposed  
and tv.CourtDateMain = (Select min(CourtDateMain) from tblticketsviolations where ticketid_pk = t.ticketid_pk)  
--	Status of all violations shuld be same
and dbo.fn_get_split_records_count(t.ticketid_pk)=0 
--and tv.ticketid_pk=125616
  
insert into #TempTable  
--- Geting those records which has future date and status in JURRY and courts are inside courts  
SELECT  distinct
   
 t.TicketID_PK As TicketID,   
 isnull(t.LastName,'') + ', ' + isnull(t.FirstName,'') As ClientName,  
 isnull(t.LastName,'') + ' ' + isnull(t.FirstName,'') As CustomerName,  
 t.EmployeeID,  
 t.Email AS EMailAddress   
FROM    tblTickets t     
INNER JOIN    
 tblTicketsViolations tv     
ON  t.TicketID_PK = tv.TicketID_PK     
LEFT OUTER JOIN    
 tblCourtViolationStatus cvs     
ON  tv.CourtViolationStatusIDmain = cvs.CourtViolationStatusID  
where (charindex('@',isnull(t.Email,''),1)>0) and (datediff (day,getdate(),isnull(tv.CourtDateMain,'01/01/1900')) >= 7 )  
and cvs.CategoryID = 4 and tv.CourtID in (3001,3002,3003) And isnull(t.IsEMailedTrialLetter,0) = 0  
and t.ticketid_pk not in (select ticketid_pk from tblticketsflag where ticketid_pk =t.ticketid_pk and flagid=7)--Checking No Letters  
and t.ActiveFlag=1  
and tv.courtviolationstatusidmain not in(50,80)-- Case should not be disposed  
and tv.CourtDateMain = (Select min(CourtDateMain) from tblticketsviolations where ticketid_pk = t.ticketid_pk)  
--	Status of all violations shuld be same
and dbo.fn_get_split_records_count(t.ticketid_pk)=0
--and tv.ticketid_pk=125616

  
create table #TempTable2  
(  
 ID int identity, TicketID int,ClientName varchar(250),CustomerName varchar(250), EmployeeID int, EMailAddress varchar(250)  
)  
-- Inserting both query records to single temp table for geting distinct records  
insert into #TempTable2 select distinct TicketID, ClientName,CustomerName, EmployeeID, EMailAddress from #TempTable order by TicketID  
  
  
  
------- Sending Email To Clients -------------  
  
-- Local Variables  
Declare @Body varchar(max)  
Declare @email varchar(250)  
Declare @TicketID varchar(100)  
Declare @Subject varchar(250)  
Declare @count int  
Declare @no int  
Declare @EmpID int  
Declare @ClientName varchar(250)  
  
--- Subject Of Email   
select @Subject ='Sullo & Sullo Attorneys � Trial Notification Email'  
  
-- Set count with total numbers of records  
select @count =count(ID) from #TempTable2  
-- no at the starting of while loop  
set @no =1  
  
while @no <= @count  
begin  
  
-- Geting Current Email,TicketID,ClientName and EmpID from temp table  
select @email =EMailAddress from #TempTable2 where ID =@no  
select @TicketID =TicketID from #TempTable2 where ID =@no  
select @ClientName =CustomerName from #TempTable2 where ID =@no  
select @EmpID =3992-- System ID  
  
   
-- Genrate EmailID by removing spactioal chracters from datetime  
Declare @EmailID varchar(250)  
Declare @CurrentDateTime varchar(100)  
set @CurrentDateTime =convert(varchar(30),getdate()) + convert(varchar(2),DatePart(s, getdate()))  
set @CurrentDateTime = replace(@CurrentDateTime,'-','')  
set @CurrentDateTime = replace(@CurrentDateTime,' ','')  
set @CurrentDateTime = replace(@CurrentDateTime,'.','')  
set @CurrentDateTime = replace(@CurrentDateTime,':','')  
set @EmailID =@TicketID + @CurrentDateTime -- Combining TicketID and CurrentDateTime  
--------------------  
  
-- Page HTML  
set @Body ='<style type="text/css">
.Label{ FONT-WEIGHT: normal;  COLOR: #000000; FONT-STYLE: normal; FONT-FAMILY: Verdana; FONT-SIZE: 8.5pt; border-style:none; border-width:0px; }
.SmallLabel {FONT-WEIGHT: normal;  COLOR:Gray; FONT-STYLE: normal; FONT-FAMILY: Verdana; FONT-SIZE: 7pt; border-style:none; border-width:0px;}</style>
<table border="0" bgcolor="#D9D9D9" cellpadding="0" cellspacing="0" align="center" height="100%" width="100%">
<tr><td class="SmallLabel" align="center"><br /> If you are having problems viewing this email, you can view it online at<br />
<a href="http://test.legalhouston2.com/Reports/TroubleViewingLink.aspx?casenumber='+dbo.Encrypt(@TicketID)+'&ClientName='+dbo.Encrypt(@ClientName)+'&EmailID='+dbo.Encrypt(@EmailID)+'">http://test.legalhouston2.com/EmailLink/TroubleViewingLink?RefrenceCode='+dbo.Encrypt(@TicketID)+' </a></td></tr>
<tr> <td width="100%" style="height: 100%"> <br>
<table border="1" bgcolor="white" bordercolor="Silver" cellpadding="8" cellspacing="0" align="center" height="100%" width="500">
<tr> <td align="center" style="height: 5px; border:1;border-color:Silver;"><img src="http://www.sullolaw.com/images/SulloLawlogo.jpg"/> </td></tr>
<tr> <td valign="top" width="100%" style="height: 400px"> <table border="0" align="center" cellpadding="0" cellspacing="0" width="100%"><tr>
<td class="Label" > </td><td class="Label"> <br /><br /> DEAR : '+upper(@ClientName)+',<br /><br />
Thank you for hiring Sullo & Sullo, LLP. Please click on the link below to review important information regarding your case(s)
including court date(s), time(s), location(s), and items you must bring to court with you.
</td></tr><tr> <td align="center" style="width: 13px"> </td><td align="center" class="Label">
<br /><br /><a href="http://test.legalhouston2.com/Reports/frmTrialNotification.aspx?casenumber='+dbo.Encrypt(@TicketID)+'&EmailID='+dbo.Encrypt(@EmailID)+'">
<span style=color:#4F81BD>Please click here to view your Trial Notification Letter</span> </a>
</td></tr><tr> </tr><tr> <td class="Label" style="width: 13px"></td><td class="Label"><br /><br />Thank you,<br /><br /><br /></td></tr> <tr>
<td style="width: 13px"></td><td> <font size=5 face=Calibri><span style=font-size:16.0pt;font-family:Calibri;> Sullo <o:p></o:p></span></font>
<font size=4 face="Lucida Fax"><span style="font-size:14.0pt;font-family: Lucida Fax";>&</span></font>
<font size=5 face=Calibri><span style=font-size:16.0pt;font-family:Calibri;> Sullo,</span></font>
<font size=2 face=Calibri><span style=font-size:9.0pt;font-family:Calibri;><b>LLP</b></span></font>
</td></tr> <tr><td style="width: 13px; height: 19px"></td> <td class="Label"> <b>Tel #:</b> 713.839.9026</td></tr><tr>
<td style="width: 13px"></td><td class="Label"> <b>Fax #:</b> 713.523.6634</td></tr>
<tr> <td style="width: 13px"> </td> <td class="Label">http://test.legalhouston2.com<br><br></td></tr>
</table></td></tr></table></td></tr><tr> <td><table border="0" cellpadding="0" cellspacing="0" align="center" style="width: 452px; height:1px;">
<tr><td valign="top" align="center"> <table style="width: 500px"><tr><td><hr /> </td></tr></table></td></tr></table></td></tr></table>'
-- Rab Nawaz 10914 07/30/2013 Removed Disclimer. . .  
-- <tr><td align="center" width="100%" class="SmallLabel">
-- This e-mail and the files transmitted with it are the property of Sullo & Sullo <span style=font-size:6pt;>LLP</span> and/or its affiliates, are confidential, and are intended solely for use of the individual or entity to whom this e-mail is addressed. If
-- you are not one of the named recipients or otherwise have reason to believe that you have received this message in error, please notify the sender at 713-839-9026 and delete this message immediately from your computer.&nbsp; Any other use, retention, dis
-- semination, forwarding, printing, or copying of this e-mail is strictly prohibited.<br /><br /></td></tr>
-- </table> </td></tr><tr><td align="center" class="SmallLabel">Copyright� 2007, Sullo & Sullo, <span style=font-size:6pt;>LLP</span> All Rights Reserved.<br /><br /><br/>

  
if((select count(email) from tblnoemailflag where email = @email) = 0) --The clients email which are exist in tblnoemailflag wont send email  
 BEGIN  
   
 if((select IsEmailFlag from tblConfigurationSetting where id=1) = 1) -- Checking the IsEmailFlag flag if flag is 1 email will be send    
  BEGIN     
   EXEC msdb.dbo.sp_send_dbmail     
     @profile_name = 'Email Trial Letter',      
     @recipients = @email,     
     @subject = @Subject,     
     @body = @Body ,    
     @body_format = 'HTML' ;    
    
  END    
  -- Sending Note to Case history    
  insert into tblticketsnotes (ticketid,subject,employeeid) values(@TicketID,'Trial Letter sent to ('+@email+')' ,@EmpID)    
      
  -- Add entry in tblemailtrialnotification with EmailID    
  Insert into tblemailtrialnotification(TicketID_pk,EmailDate,EmailID)    
  values(@TicketID,getdate(),@EmailID)      
  
  -- update trial letter date and isemailedtriallter field even email is not send    
  update tbltickets set IsEmailedTrialLetter =1,TrialLetterEmailedDate =getdate() where ticketid_pk = @TicketID    
       
  ---- It will make a gap between bulk sending emails     
  waitfor delay '0:0:20'    
 END    
    
set @no = @no +1    
    
end    
drop table #TempTable    
drop table #TempTable2    
    
--------------------------------------------------------------------------------------------------------------------------    
/*********************

Alter By:	Tahir Ahmed
Date:		09/29/2008

Business Logic:
	The procedure is by HTP /Activities /Partial Pay report to highlight the balance dues.

Input Parameters:
	@dsdata:		Balance Due start date
	@dedate:		Balance due end date
	@cSdate:		Court start date
	@cEdate:		court end date
	@status:		payment status
	@chk:			Flag to display all past dues irrespective of selected due date range
	@CourtType:		Case type of the case (All Courts,Traffic, Criminal, Civil) 
	@CaseStatus:	Case status type (All, Open Cases, Disposed Cases)

Output Columns:
ClientName:		Name of the client (First name & Last name)
TicketID_PK:	Case identification ID            
PaymentDate:	Payment Due date
Amount:			Balance Due amount
PmytComments:	Schedule payment comments
scheduleid:		Schedude payment ID
contact1:		Contact 1 of the cleint
contact2:		Contact 2 of the client
contact3:		Contact 3 of the client
Activeflag:		Active flag (Quote/Client)	
TotalFeeCharged:Total Fee
CourtDate:		Court Date for the violation
crt:			Court location

*******************/



--USP_HTS_GET_PARTIALPAYREPORTNEW  '2/1/2007', '2/1/2007', '11/01/2006','03/31/2007',0,1,0          
ALTER procedure [dbo].[USP_HTS_GET_PARTIALPAYREPORTNEW]            
            
@dsdata datetime ,             
@dedate datetime ,             
@cSdate datetime ,             
@cEdate datetime ,             
@status int,            
@chk int,            
@CourtType int,
@CaseStatus int = 0
            
as             
            
select @dedate = @dedate + '23:59:59.000'             
                    
Declare @Cstatus varchar(12)            
            
if @status = 0            
 begin            
  set @Cstatus = '1,'            
 end
 --ozair task 3198 on 20/2/2008 [check for default and waived plans added, vaue 2 and 3]
else if @status = 2            
 begin            
  set @Cstatus = '2,'            
 end
else if @status = 3            
 begin            
  set @Cstatus = '3,'            
 end
else
 begin            
  set @Cstatus = '2,3,'            
 end            
            
declare @strQuery nvarchar(4000)            
declare @strParamDef nvarchar(200)            
            
set @strParamDef = '@dsdata datetime , @dedate datetime , @cSdate datetime , @cEdate datetime ,            
@status int, @chk int,  @CourtType int, @Cstatus varchar(12)'            
            
            
set @strQuery = '            
SELECT        
 t.Firstname + '' '' + t.Lastname AS ClientName,            
 t.TicketID_PK,            
 convert(varchar(12),sp.PaymentDate,101) as  PaymentDate ,         
 sp.Amount ,        
 '''' as PmytComments,             -- Agha Usman 2664 06/17/2008
 sp.scheduleid,          
   convert(varchar(20),ISNULL(dbo.formatphonenumbers(T.Contact1),''''))+''(''+ISNULL(LEFT(tblContactstype_1.Description, 1),'''')+'')'' as contact1,                                
         convert(varchar(20),ISNULL(dbo.formatphonenumbers(T.Contact2),''''))+''(''+ISNULL(LEFT(dbo.tblContactstype.Description, 1),'''')+'')'' as contact2,                                
         convert(varchar(20),ISNULL(dbo.formatphonenumbers(T.Contact3),''''))+''(''+ISNULL(LEFT(tblContactstype_2.Description, 1),'''')+'')'' as contact3,             
 t.Activeflag,            
 sp.ScheduleFlag, t.TotalFeeCharged -             
  (            
   select sum(chargeamount) from tblticketspayment p            
    where p.ticketid = t.ticketid_pk            
     and p.paymentvoid = 0            
     and p.paymenttype not in (99,100)            
  ) as Owes,            
 t.TotalFeeCharged,            
  
 convert(varchar(12),(isnull((select  min(tv.courtdatemain) from tblticketsviolations tv            
    where tv.ticketid_pk = t.ticketid_pk            
     and datediff(day, getdate(), tv.courtdatemain) >=0),            
    (select  max(tv1.courtdate) from tblticketsviolations tv1 where tv1.ticketid_pk = t.ticketid_pk))),101) as CourtDate,             
  
 (select  top 1 c.shortname from tblticketsviolations tv ,tblcourts c         
 where tv.courtid = c.courtid and tv.ticketid_pk = t.ticketid_pk) as crt            
FROM      dbo.tblTickets AS t INNER JOIN            
                      dbo.tblSchedulePayment AS sp ON t.TicketID_PK = sp.TicketID_PK LEFT OUTER JOIN            
                      dbo.tblContactstype AS tblContactstype_2 ON t.ContactType3 = tblContactstype_2.ContactType_PK LEFT OUTER JOIN            
                      dbo.tblContactstype ON t.ContactType2 = dbo.tblContactstype.ContactType_PK LEFT OUTER JOIN            
                      dbo.tblContactstype AS tblContactstype_1 ON t.ContactType1 = tblContactstype_1.ContactType_PK            
WHERE (t.Activeflag = 1) AND (sp.ScheduleFlag = 1)  
-- tahir 4266 06/19/2008
and isnull(t.paymentstatus,1) in (select maincat from dbo.sap_dates(  @Cstatus )) 
-- end 4266

--and isnull(t.paymentstatus,1) in (select maincat from dbo.sap_dates(  @Cstatus ))   -- Agha Usman 2664 06/17/2008
and (select top 1  tv.courtid from tblticketsviolations tv where        
 tv.ticketid_pk =t.ticketid_pk) in (select distinct(CourtID) from tblcourts where courtid <> 0 and InActiveFlag = 0)   
/*  
and t.ticketid_pk in(select distinct ticketid_pk from tblticketsviolations where courtdatemain between  @cSdate   and @cEdate  )       
*/  
'            
            
if @chk = 0            
 begin            
  set @strQuery = @strQuery + ' AND (sp.PaymentDate between  @dsdata and  @dedate ) '            
 end             
  
if @chk = 1   
 begin   
  set @strQuery = @strQuery + ' AND (datediff(day, sp.PaymentDate, Getdate()) >0  )'  
 end   
            
-- tahir 4842 09/29/2008 display Traffic, Criminal, or Family Courts....

--if @CourtType = 1 -- Criminal Court            
--begin             
--  set @strQuery = @strQuery + ' and (select top 1  tv.courtid from tblticketsviolations tv where        
-- tv.ticketid_pk =t.ticketid_pk) in (select distinct(CourtID) from tblcourts where iscriminalcourt = 1) '            
--end             
--          
--if @CourtType = 2 -- Non-Criminal Court            
--begin             
--  set @strQuery = @strQuery + ' and (select top 1  tv.courtid from tblticketsviolations tv where        
-- tv.ticketid_pk =t.ticketid_pk) in (select distinct(CourtID) from tblcourts where iscriminalcourt = 0) '            
--end        

if @CourtType <> 0
begin             
  set @strQuery = @strQuery + ' and t.casetypeid = @CourtType '            
end        	

if @CaseStatus = 1
begin
	set @strQuery = @strQuery + ' 
	and  t.ticketid_pk in (select ticketid_pk from tblticketsviolations v 
	inner join tblcourtviolationstatus s on s.courtviolationstatusid = v.courtviolationstatusidmain
	where s.categoryid in (2,3,4,5) and v.ticketid_pk = t.ticketid_pk
	)'
end

if @CaseStatus = 2
begin
	set @strQuery = @strQuery + '
	and  t.ticketid_pk in (select ticketid_pk from tblticketsviolations v 
	inner join tblcourtviolationstatus s on s.courtviolationstatusid = v.courtviolationstatusidmain
	where s.categoryid = 50 and v.ticketid_pk = t.ticketid_pk
	)'
end


-- end 4842
        
set @strQuery = @strQuery + ' order by ClientName '  
  
EXECUTE sp_executesql @strQuery, @strParamDef, @dsdata , @dedate , @cSdate , @cEdate ,  @status , @chk , @CourtType, @Cstatus             
    
                 
    
go 
/****** 
Create by:  Syed Muhammad Ozair

Business Logic : this procedure is used to display the details of a scanned batch.

List of Parameters:	
	@ScanBatchID : scan batch id for which records will be fetched

List of Columns: 	
	BatchID : scan batch id
	PageCount : total no of pages of the batch
	ScanDate : page no of the document	
	NoLetterID : count of no letter ids

******/

Create Procedure dbo.usp_HTP_Get_DocumentTracking_UpdateBatch 

@ScanBatchID int

as

declare @cnt int

select @cnt=count(documentbatchid) 
from tbl_htp_documenttracking_scanbatch_detail 
where documentbatchid=0
and scanbatchid_fk=@ScanBatchID

select	sbd.scanbatchid_fk as BatchID,
	count(sbd.scanbatchid_fk) as PageCount,
	sb.scanbatchdate as ScanDate, 
	@cnt as NoLetterID 
from	tbl_htp_documenttracking_scanbatch_detail sbd inner join tbl_htp_documenttracking_scanbatch sb
on	sbd.scanbatchid_fk=sb.scanbatchid 
where	sbd.scanbatchid_fk=@ScanBatchID
group by	sbd.scanbatchid_fk, sb.scanbatchdate

go

grant exec on dbo.usp_HTP_Get_DocumentTracking_UpdateBatch to dbr_webuser
go


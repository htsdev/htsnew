USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTP_GET_ListofActiveAndClosedCases]    Script Date: 02/10/2012 15:28:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sabir Khan
-- Work Item ID: 10031 
-- Create date: 02/08/2012
-- Business Logic: This stored procedure is used to display list of active/closed cases for both dallas and houston
-- Input Parameters: 
-- @DBID: 1 - for Houston database, 2 - for Dallas database.
-- @IsActive : 1 - for Active cases, 0 - for closed cases.

-- =============================================
-- USP_HTP_GET_ListofActiveAndClosedCases 2,0
ALTER PROCEDURE [dbo].[USP_HTP_GET_ListofActiveAndClosedCases]
	@DBID INT,
	@IsActive INT
AS
IF @DBID = 1
	BEGIN
		IF @IsActive = 0
		BEGIN
			-- Section of Houston Disposed cases
			-- Get all disposed cases
			SELECT tv.TicketID_PK,MAX(tv.TicketsViolationID) AS TicketsViolationID 
			INTO #tempHoustonDispo FROM tbltickets t 
			INNER JOIN tblTicketsViolations tv ON tv.TicketID_PK = t.TicketID_PK
			INNER JOIN tblCourtViolationStatus csv ON csv.CourtViolationStatusID = tv.CourtViolationStatusIDmain
			INNER JOIN tblTicketsPayment tp ON tp.TicketID = t.TicketID_PK
			WHERE csv.CategoryID = 50
			AND ISNULL(t.Activeflag,0) = 1
			AND ISNULL(tp.PaymentVoid,0) = 0
			AND LEN(ISNULL(t.Firstname,'') + ISNULL(t.Lastname,'')) > 0
			AND t.Firstname NOT LIKE 'Test' AND t.Lastname NOT LIKE 'Test'
			and t.ticketid_pk not in (4485,6362,6774,8271,8810,9090,9212,13446,107104,141748,180922,184109,190178,190179,191713,
			192536,204470,209388,216998,218215,218216,236436,241094,242807,256869,75545,294170,299221,301694,322718,332203,
			340280,394850,395013,395237,398204,275545,194549,242817,380322,380323)			
			GROUP BY tv.TicketID_PK
			
			-- Add Fields in Temp table for case informations
			ALTER TABLE #tempHoustonDispo ADD firstname VARCHAR(500),lastname VARCHAR(500),CaseNumber VARCHAR(200),ClientDate DATETIME,
			CaseStatus VARCHAR(200),Courtdate DATETIME,RoomNo VARCHAR(10),CourtLocation VARCHAR(500)
			
			-- Update First Name and Last Name of Clients
			UPDATE t 
			SET t.firstname = tt.firstname, t.lastname = tt.lastname
			FROM #tempHoustonDispo t INNER JOIN tbltickets tt ON tt.TicketID_PK = t.ticketid_pk
			
			-- Update Client Case informations
			UPDATE t 
			SET t.CaseNumber = CASE WHEN LEN(ISNULL(tv.casenumassignedbycourt,''))>0 THEN tv.casenumassignedbycourt ELSE tv.RefCaseNumber END ,
			t.CaseStatus = csv.[Description] ,
			t.Courtdate = tv.CourtDateMain ,
			t.RoomNo = tv.CourtNumbermain ,
			t.CourtLocation = c.CourtName
			FROM #tempHoustonDispo t INNER JOIN tbltickets tt ON tt.TicketID_PK = t.ticketid_pk
			INNER JOIN tblticketsviolations tv ON tv.TicketsViolationID = t.TicketsViolationID
			INNER JOIN tblCourtViolationStatus csv ON csv.CourtViolationStatusID = tv.CourtViolationStatusIDmain
			INNER JOIN tblTicketsPayment tp ON tp.TicketID = t.ticketid_pk
			INNER JOIN tblCourts c ON c.Courtid = tv.CourtID
			
			-- Update All Null Room No with 0
			UPDATE #tempHoustonDispo SET RoomNo = 0 WHERE RoomNo IS NULL
			
			-- Get Hire date
			SELECT t.ticketid_pk,MIN(tp.recdate) AS Recdate
			INTO #tempHoustonDispoPay FROM #tempHoustonDispo t 			
			INNER JOIN tblTicketsPayment tp ON tp.TicketID = t.ticketid_pk
			GROUP BY t.ticketid_pk
			
			-- Update Hire date in Temp table
			UPDATE t
			SET t.ClientDate = ttp.Recdate
			FROM #tempHoustonDispo t 
			INNER JOIN #tempHoustonDispoPay ttp ON ttp.ticketid_pk = t.ticketid_pk			
			INNER JOIN tblTicketsPayment tp ON tp.TicketID = t.ticketid_pk
			
			-- display the final result
			SELECT CASENumber AS [Case Number],firstName AS [First Name],LastName AS [Last Name],
			CONVERT(VARCHAR,ClientDate,101) AS [Client Date],Casestatus AS [Case Status],
			dbo.fn_DateFormat(CourtDate,24,'/',':',1) AS [Court Date],RoomNo AS [Room Number],CourtLocation AS [Court Location]
			FROM #tempHoustonDispo
			
			-- Drop Temp Table
			DROP TABLE #tempHoustonDispo
			DROP TABLE #tempHoustonDispoPay
		END
ELSE
		BEGIN
			-- Section of Houston Active Cases
			-- Get all disposed cases
			SELECT tv.TicketID_PK,MAX(tv.TicketsViolationID) AS TicketsViolationID 
			INTO #tempHoustonActive FROM tbltickets t 
			INNER JOIN tblTicketsViolations tv ON tv.TicketID_PK = t.TicketID_PK
			INNER JOIN tblCourtViolationStatus csv ON csv.CourtViolationStatusID = tv.CourtViolationStatusIDmain
			INNER JOIN tblTicketsPayment tp ON tp.TicketID = t.TicketID_PK
			WHERE csv.CategoryID <> 50
			AND ISNULL(t.Activeflag,0) = 1
			AND ISNULL(tp.PaymentVoid,0) = 0
			AND LEN(ISNULL(t.Firstname,'') + ISNULL(t.Lastname,'')) > 0
			AND t.Firstname NOT LIKE 'Test' AND t.Lastname NOT LIKE 'Test'
			and t.ticketid_pk not in (4485,6362,6774,8271,8810,9090,9212,13446,107104,141748,180922,184109,190178,190179,191713,
			192536,204470,209388,216998,218215,218216,236436,241094,242807,256869,75545,294170,299221,301694,322718,332203,
			340280,394850,395013,395237,398204,275545,194549,242817,380322,380323)	
			GROUP BY tv.TicketID_PK
			-- Add Fields in Temp table for case informations
			ALTER TABLE #tempHoustonActive ADD firstname VARCHAR(500),lastname VARCHAR(500),CaseNumber VARCHAR(200),ClientDate DATETIME,
			CaseStatus VARCHAR(200),Courtdate DATETIME,RoomNo VARCHAR(10),CourtLocation VARCHAR(500)
			
			-- Update First Name and Last Name of Clients
			UPDATE t 
			SET t.firstname = tt.firstname, t.lastname = tt.lastname
			FROM #tempHoustonActive t INNER JOIN tbltickets tt ON tt.TicketID_PK = t.ticketid_pk
			
			-- Update Client Case informations
			UPDATE t 
			SET t.CaseNumber = CASE WHEN LEN(ISNULL(tv.casenumassignedbycourt,''))>0 THEN tv.casenumassignedbycourt ELSE tv.RefCaseNumber END ,
			t.CaseStatus = csv.[Description] ,
			t.Courtdate = tv.CourtDateMain ,
			t.RoomNo = tv.CourtNumbermain ,
			t.CourtLocation = c.CourtName
			FROM #tempHoustonActive t INNER JOIN tbltickets tt ON tt.TicketID_PK = t.ticketid_pk
			INNER JOIN tblticketsviolations tv ON tv.TicketsViolationID = t.TicketsViolationID
			INNER JOIN tblCourtViolationStatus csv ON csv.CourtViolationStatusID = tv.CourtViolationStatusIDmain
			INNER JOIN tblTicketsPayment tp ON tp.TicketID = t.ticketid_pk
			INNER JOIN tblCourts c ON c.Courtid = tv.CourtID
			
			-- Update All Null Room No with 0
			UPDATE #tempHoustonActive SET RoomNo = 0 WHERE RoomNo IS NULL
			
			-- Get Hire date
			SELECT t.ticketid_pk,MIN(tp.recdate) AS Recdate
			INTO #tempHoustonActivePay FROM #tempHoustonActive t 			
			INNER JOIN tblTicketsPayment tp ON tp.TicketID = t.ticketid_pk
			GROUP BY t.ticketid_pk
			
			-- Update Hire date in Temp table
			UPDATE t
			SET t.ClientDate = ttp.Recdate
			FROM #tempHoustonActive t 
			INNER JOIN #tempHoustonActivePay ttp ON ttp.ticketid_pk = t.ticketid_pk			
			INNER JOIN tblTicketsPayment tp ON tp.TicketID = t.ticketid_pk
			
			-- display the final result
			SELECT CASENumber AS [Case Number],firstName AS [First Name],LastName AS [Last Name],
			CONVERT(VARCHAR,ClientDate,101) AS [Client Date],Casestatus AS [Case Status],
			dbo.fn_DateFormat(CourtDate,24,'/',':',1) AS [Court Date],RoomNo AS [Room Number],CourtLocation AS [Court Location]
			FROM #tempHoustonActive
			
			-- Drop Temp Table
			DROP TABLE #tempHoustonActive
			DROP TABLE #tempHoustonActivePay
		END	
	END
ELSE
	BEGIN
		IF @IsActive = 0
		BEGIN
			-- Section of Dallas Disposed Cases
			SELECT tv.TicketID_PK,MAX(tv.TicketsViolationID) AS TicketsViolationID 
			INTO #tempDallasDispo FROM DallasTrafficTickets.dbo.tbltickets t
			INNER JOIN DallasTrafficTickets.dbo.tblTicketsViolations tv ON tv.TicketID_PK = t.TicketID_PK
			INNER JOIN DallasTrafficTickets.dbo.tblCourtViolationStatus csv ON csv.CourtViolationStatusID = tv.CourtViolationStatusIDmain
			INNER JOIN DallasTrafficTickets.dbo.tblTicketsPayment tp ON tp.TicketID = t.TicketID_PK
			WHERE csv.CategoryID = 50
			AND ISNULL(t.Activeflag,0) = 1
			AND ISNULL(tp.PaymentVoid,0) = 0
			AND LEN(ISNULL(t.Firstname,'') + ISNULL(t.Lastname,'')) > 0
			AND t.Firstname NOT LIKE 'Test' AND t.Lastname NOT LIKE 'Test'
			and t.ticketid_pk not in (78608,158339,158342,178799,179085,179086,166209)
			GROUP BY tv.TicketID_PK
			
			-- Add Fields in Temp table for case informations
			ALTER TABLE #tempDallasDispo ADD firstname VARCHAR(500),lastname VARCHAR(500),CaseNumber VARCHAR(200),ClientDate DATETIME,
			CaseStatus VARCHAR(200),Courtdate DATETIME,RoomNo VARCHAR(10),CourtLocation VARCHAR(500)
			
			-- Update First Name and Last Name of Clients
			UPDATE t 
			SET t.firstname = tt.firstname, t.lastname = tt.lastname
			FROM #tempDallasDispo t INNER JOIN DallasTrafficTickets.dbo.tbltickets tt ON tt.TicketID_PK = t.ticketid_pk
		
			-- Update Client Case informations
			UPDATE t 
			SET t.CaseNumber = CASE WHEN LEN(ISNULL(tv.casenumassignedbycourt,''))>0 THEN tv.casenumassignedbycourt ELSE tv.RefCaseNumber END ,
			t.CaseStatus = csv.[Description] ,
			t.Courtdate = tv.CourtDateMain ,
			t.RoomNo = tv.CourtNumbermain ,
			t.CourtLocation = c.CourtName
			FROM #tempDallasDispo t INNER JOIN DallasTrafficTickets.dbo.tbltickets tt ON tt.TicketID_PK = t.ticketid_pk
			INNER JOIN DallasTrafficTickets.dbo.tblticketsviolations tv ON tv.TicketsViolationID = t.TicketsViolationID
			INNER JOIN DallasTrafficTickets.dbo.tblCourtViolationStatus csv ON csv.CourtViolationStatusID = tv.CourtViolationStatusIDmain
			INNER JOIN DallasTrafficTickets.dbo.tblTicketsPayment tp ON tp.TicketID = t.ticketid_pk
			INNER JOIN DallasTrafficTickets.dbo.tblCourts c ON c.Courtid = tv.CourtID
			
			-- Update All Null Room No with 0
			UPDATE #tempDallasDispo SET RoomNo = 0 WHERE RoomNo IS NULL
			
			-- Get Hire date
			SELECT t.ticketid_pk,MIN(tp.recdate) AS Recdate
			INTO #tempDallasDispoPay FROM #tempDallasDispo t			
			INNER JOIN DallasTrafficTickets.dbo.tblTicketsPayment tp ON tp.TicketID = t.ticketid_pk
			GROUP BY t.ticketid_pk
			
			-- Update Hire date in Temp table
			UPDATE t
			SET t.ClientDate = ttp.Recdate
			FROM #tempDallasDispo t 
			INNER JOIN #tempDallasDispoPay ttp ON ttp.ticketid_pk = t.ticketid_pk			
			INNER JOIN DallasTrafficTickets.dbo.tblTicketsPayment tp ON tp.TicketID = t.ticketid_pk
			
			-- display the final result
			SELECT CASENumber AS [Case Number],firstName AS [First Name],LastName AS [Last Name],
			CONVERT(VARCHAR,ClientDate,101) AS [Client Date],Casestatus AS [Case Status],
			dbo.fn_DateFormat(CourtDate,24,'/',':',1) AS [Court Date],RoomNo AS [Room Number],CourtLocation AS [Court Location]
			FROM #tempDallasDispo
			
			-- Drop Temp Table
			DROP TABLE #tempDallasDispo
			DROP TABLE #tempDallasDispoPay
		END
ELSE
		BEGIN
			-- Section of Dallas Active Cases
			SELECT tv.TicketID_PK,MAX(tv.TicketsViolationID) AS TicketsViolationID 
			INTO #tempDallasActive FROM DallasTrafficTickets.dbo.tbltickets t
			INNER JOIN DallasTrafficTickets.dbo.tblTicketsViolations tv ON tv.TicketID_PK = t.TicketID_PK
			INNER JOIN DallasTrafficTickets.dbo.tblCourtViolationStatus csv ON csv.CourtViolationStatusID = tv.CourtViolationStatusIDmain
			INNER JOIN DallasTrafficTickets.dbo.tblTicketsPayment tp ON tp.TicketID = t.TicketID_PK
			WHERE csv.CategoryID <> 50
			AND ISNULL(t.Activeflag,0) = 1
			AND ISNULL(tp.PaymentVoid,0) = 0
			AND LEN(ISNULL(t.Firstname,'') + ISNULL(t.Lastname,'')) > 0
			AND t.Firstname NOT LIKE 'Test' AND t.Lastname NOT LIKE 'Test'
			and t.ticketid_pk not in (78608,158339,158342,178799,179085,179086,166209)
			GROUP BY tv.TicketID_PK
			
			-- Add Fields in Temp table for case informations
			ALTER TABLE #tempDallasActive ADD firstname VARCHAR(500),lastname VARCHAR(500),CaseNumber VARCHAR(200),ClientDate DATETIME,
			CaseStatus VARCHAR(200),Courtdate DATETIME,RoomNo VARCHAR(10),CourtLocation VARCHAR(500)
			
			-- Update First Name and Last Name of Clients
			UPDATE t 
			SET t.firstname = tt.firstname, t.lastname = tt.lastname
			FROM #tempDallasActive t INNER JOIN DallasTrafficTickets.dbo.tbltickets tt ON tt.TicketID_PK = t.ticketid_pk
			
			-- Update Client Case informations
			UPDATE t 
			SET t.CaseNumber = CASE WHEN LEN(ISNULL(tv.casenumassignedbycourt,''))>0 THEN tv.casenumassignedbycourt ELSE tv.RefCaseNumber END ,
			t.CaseStatus = csv.[Description] ,
			t.Courtdate = tv.CourtDateMain ,
			t.RoomNo = tv.CourtNumbermain ,
			t.CourtLocation = c.CourtName
			FROM #tempDallasActive t 
			INNER JOIN DallasTrafficTickets.dbo.tbltickets tt ON tt.TicketID_PK = t.ticketid_pk
			INNER JOIN DallasTrafficTickets.dbo.tblticketsviolations tv ON tv.TicketsViolationID = t.TicketsViolationID
			INNER JOIN DallasTrafficTickets.dbo.tblCourtViolationStatus csv ON csv.CourtViolationStatusID = tv.CourtViolationStatusIDmain
			INNER JOIN DallasTrafficTickets.dbo.tblCourts c ON c.Courtid = tv.CourtID
				
			-- Update All Null Room No with 0
			UPDATE #tempDallasActive SET RoomNo = 0 WHERE RoomNo IS NULL
			
			-- Get Hire date
			SELECT t.ticketid_pk,MIN(tp.recdate) AS Recdate
			INTO #tempDallasActivePay FROM #tempDallasActive t 			
			INNER JOIN DallasTrafficTickets.dbo.tblTicketsPayment tp ON tp.TicketID = t.ticketid_pk
			GROUP BY t.ticketid_pk
			
			-- Update Hire date in Temp table
			UPDATE t
			SET t.ClientDate = ttp.Recdate
			FROM #tempDallasActive t 
			INNER JOIN #tempDallasActivePay ttp ON ttp.ticketid_pk = t.ticketid_pk			
			INNER JOIN DallasTrafficTickets.dbo.tblTicketsPayment tp ON tp.TicketID = t.ticketid_pk
					
			-- display the final result
			SELECT CASENumber AS [Case Number],firstName AS [First Name],LastName AS [Last Name],
			CONVERT(VARCHAR,ClientDate,101) AS [Client Date],Casestatus AS [Case Status],
			dbo.fn_DateFormat(CourtDate,24,'/',':',1) AS [Court Date],RoomNo AS [Room Number],CourtLocation AS [Court Location]
			FROM #tempDallasActive
			
			-- Drop Temp Table
			DROP TABLE #tempDallasActive
			DROP TABLE #tempDallasActivePay
		END	
	END

	
	

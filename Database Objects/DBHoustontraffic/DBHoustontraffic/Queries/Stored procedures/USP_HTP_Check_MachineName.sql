USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTP_Check_MachineName]    Script Date: 06/03/2013 17:13:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
* This stored procedure is used to get all company name
*/

CREATE Procedure [dbo].[USP_HTP_Check_MachineName]
@IPAddress varchar(100),
@BranchId int
as
select * 
from branch 
WHERE 
BranchId = @BranchId 
AND 
SUBSTRING(@IPAddress,0,11) = substring(Branchstartwith,0,11)


GO

GRANT EXECUTE ON [dbo].[USP_HTP_Check_MachineName] TO dbr_webuser

GO
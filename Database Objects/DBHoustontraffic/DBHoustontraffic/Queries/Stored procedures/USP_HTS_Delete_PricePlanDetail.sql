SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Delete_PricePlanDetail]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Delete_PricePlanDetail]
GO










--Used to delete all the rows of plan ID in tblPricePlanDetail
CREATE PROCEDURE [USP_HTS_Delete_PricePlanDetail]
	(@PlanId 	[int])

AS DELETE [tblPricePlanDetail] 

WHERE 
	( [PlanId]	 = @PlanId)





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


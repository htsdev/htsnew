SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_SAVE_HC_STATUSES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_SAVE_HC_STATUSES]
GO

CREATE PROCEDURE USP_HTS_SAVE_HC_STATUSES  
@status varchar(99)  
AS  
insert into tbl_HTS_HCStatuses values(@status)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


/****** 
Created by:		Tahir Ahmed
Business Logic:	The procedure is used by LMS application to get the letter information to display on document
				history page.
				
List of Parameters:
	@LetterType:	Letter type selected in selection criteria. Default is "All"
	@empId:			Employee select in selection criteira who printed the mailer.
	@startDate:		Mailer date from.
	@endDate:		Mailer date to.
	@location:		location of the currently logged in employee. For dallas it will be 2 else 1.

List of Columns:	
	batchid			Batch id of the mailer.
	courtid			court category id of the mailer
	lettertype		mailer type id
	ListDate		date for which the mailer was printed. it may be court, upload, warrant, bond or loader update date
					depending on the associated mailer type.
	LCount			number of mailers printed for the batch
	Letter			full letter name (court category name plus letter name)
	Abbreviation	employee abbreviation who printed the mailer
	batchdate		mailer printing date
	fileName		PDF file name generated for the batch.
	

******/

-- USP_Mailer_Get_BatchFileName null,null,'4/8/09','4/8/09' ,2     
alter PROCEDURE [dbo].[USP_Mailer_Get_BatchFileName]      
 @letterType int  = null,      
 @empId int = null,      
 @startDate datetime,      
 @endDate datetime,
 @location int = null 
AS      
BEGIN      
 SET NOCOUNT ON;      

    
Select distinct tbl.batchid, tbl.courtid, tbl.lettertype,tbl.ListDate, LCount,   
 (tc.CourtCategoryName + '-' +  tl.LetterName)as Letter ,    
 tu.Abbreviation ,    
 batchsentdate ,    
 convert(datetime, convert(varchar(10),batchsentdate, 101 )) as batchdate,    
 isnull(tbl.FileName,'') fileName     
from tblbatchLetter tbl      
inner join     
 tblLetter tl     
on tbl.LetterType = tl.LetterId_PK      
inner join     
 tblcourtCategories tc     
on tl.courtcategory = tc.CourtCategoryNum      
inner join     
 tblusers tu     
on tbl.empId = tu.EmployeeId      
where datediff(day,tbl.batchSentDate, @startDate) <=0     
and datediff(day,tbl.batchSentDate, @endDate)   >=0    
and (isnull(@lettertype,0)=0  or tbl.lettertype = @lettertype)
and ( isnull(@empid,0) = 0   or tbl.empid = @empid   )
-- tahir 5764 04/08/2009 allowed dallas users to access..
and (isnull(@location,0) != 2 
--Yasir Kamal 7218 01/13/2010 lms structure changed. 
     --or tbl.courtid in (6,11,16,25,27,28) -- tahir 7196 12/29/09 added arlington mailers
     OR tc.LocationId_FK = 2
    ) 

END      


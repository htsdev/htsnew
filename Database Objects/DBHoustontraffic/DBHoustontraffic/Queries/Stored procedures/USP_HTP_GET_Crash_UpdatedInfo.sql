USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTP_GET_Crash_UpdatedInfo]    Script Date: 08/27/2013 00:58:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** 
Created by		:		Sabir Khan Miani
Task ID			:		Not yet created 
Business Logic	:		The Procedure is used to get updated Texas Department of Transportation Information. 					
				
List of Parameters:	
	@downloaddate			 	
*******/

-- Connections: Client (F,L,YR,Zip), Client (F,L,YR), Client (L,Zip), Quote (F,L,YR,Zip), Quote (F,L,YR), Quote (L,Zip), Non Client (F,L,YR,Zip)
-- Non Client (F,L,YR), Non Client (L,Zip), All Clients, All Quote Clients, All Non Clients 

-- EXEC [dbo].[USP_HTP_GET_Crash_UpdatedInfo] '08/19/2013', 'Client (F,L,YR,Zip)'
ALTER PROCEDURE [dbo].[USP_HTP_GET_Crash_UpdatedInfo]
	@downloaddate DATETIME,
	@status VARCHAR(200)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT DISTINCT CONVERT(VARCHAR(10), TP.CrashDate, 101) AS CrashDate,CONVERT(VARCHAR(10), TP.InsertDate, 101) AS DownloadDate,TPO.IncidentNumber,
	       TPO.OperatorLastName AS LastName,TPO.OperatorFirstName AS FirstName,ISNULL(TP.Prsn_Age, '') AS Age,(CASE WHEN ISNULL(TP.Prsn_Age, 0) = 0 THEN ''
	       ELSE DATEPART(yy, TP.InsertDate) - ISNULL(TP.Prsn_Age, 0) END) AS DOB,UF.Tot_Injry_Cnt,UF.Poss_Injry_Cnt,TC.Thousand_Damage_Fl,TC.Crash_Sev_ID,UF.Veh_Dmag_Scl_1_ID,UF.Veh_Dmag_Scl_2_ID,TC.Medical_Advisory_Fl,
	       TC.Crash_Fatal_Fl,TC.Death_Cnt,TC.Cmv_Involv_Fl,UF.Veh_CMV_Fl,UF.Drvr_Lic_Cls_ID,UF.VIN,UF.Drvr_Zip,UF.Ownr_Zip,TC.Crash_ID,lkp.INSURANCE_PROOF_DESC, TC.Case_ID AS CaseId, TC.UpdateDate AS RecordCreation,TC.RecordID, g.GNDR_DESC AS Gender
	       INTO #tempCrash 
	FROM   Crash.dbo.tblTexasTransportationPersonOperators TPO INNER JOIN Crash.dbo.tbl_Person TP ON  TPO.CrashID = TP.Crash_ID
	       INNER JOIN Crash.dbo.tbl_Crash TC ON  TC.Crash_ID = TP.Crash_ID AND TC.Case_ID = TPO.IncidentNumber
	       LEFT OUTER JOIN Crash.dbo.tbl_Unit UF ON  UF.Crash_ID = TC.Crash_ID LEFT OUTER JOIN Crash.dbo.INSURANCE_PROOF_LKP LKP ON  UF.Fin_Resp_Proof_ID = LKP.INSURANCE_PROOF_ID
	       LEFT OUTER JOIN Crash.dbo.GNDR_LKP g ON g.GNDR_ID = tp.Prsn_Gndr_ID
	WHERE  ISNULL(TPO.OperatorFirstName, '') <> '' AND ISNULL(TPO.OperatorLastName, '') <> '' AND DATEDIFF(DAY, TP.InsertDate, @downloaddate) = 0 AND isnull(TC.Rpt_City_ID,0) = 208
	 	
	CREATE NONCLUSTERED INDEX idx_LastName ON #tempCrash(LastName)
	CREATE NONCLUSTERED INDEX idx_FirstName ON #tempCrash(FirstName)
	CREATE NONCLUSTERED INDEX idx_DOB ON #tempCrash(DOB)
	CREATE NONCLUSTERED INDEX idx_Drvr_Zip ON #tempCrash(Drvr_Zip)
	CREATE NONCLUSTERED INDEX idx_CrashID ON #tempCrash(Crash_Id)
	CREATE NONCLUSTERED INDEX idx_Recordid ON #tempCrash(Recordid)
	CREATE NONCLUSTERED INDEX idx_IncidentNumber ON #tempCrash(IncidentNumber)

	SELECT MAX(t.ticketid_pk) AS ticketid,t.Lastname,t.Firstname, ISNULL(t.Contact1,'') AS Contact1, ISNULL(t.Contact2,'') AS Contact2, ISNULL(t.Contact3,'') AS Contact3,t.Address1 + isnull(t.Address2,'') AS Address1,t.Email AS EmailAddress, t.gender AS Gender INTO #tbltickets
	FROM   tblTickets T   INNER JOIN tblTicketsViolations TV   ON  tv.TicketID_PK = t.TicketID_PK GROUP BY   t.Lastname,  t.Firstname,  ISNULL(t.Contact1,''), ISNULL(t.Contact2,''), ISNULL(t.Contact3,''),t.Address1 + isnull(t.Address2,''),t.Email, t.gender	
	    CREATE NONCLUSTERED INDEX idx_LastName ON #tbltickets(LastName)
	    CREATE NONCLUSTERED INDEX idx_FirstName ON #tbltickets(firstname)	    
	    SELECT DISTINCT REPLACE(causenumber, ' ', '') AS causenumber,attorneyname INTO #eventTemp
	    FROM   loaderfilesarchive.dbo.tblEventExtractTemp WITH(NOLOCK)	    
	    CREATE NONCLUSTERED INDEX idx_CauseNumber ON #eventTemp(causenumber)
	IF (@status = 'Client (F, L, YR, G, Zip)' OR @status = 'Client (F,L,YR,Zip)' OR @status = 'Client (F,L,YR)'  OR @status = 'Client (L,Zip)' OR @status ='Quote (F, L, YR, G, Zip)' OR @status = 'Quote (F,L,YR,Zip)' OR @status = 'Quote (F,L,YR)' OR @status = 'Quote (L,Zip)')
	BEGIN
		if(@status = 'Client (F, L, YR, G, Zip)' OR @status = 'Client (F,L,YR,Zip)' or @status = 'Client (L,Zip)' OR  @status ='Quote (F, L, YR, G, Zip)' or @status = 'Quote (F,L,YR,Zip)' or @status ='Quote (L,Zip)')
		BEGIN
			SELECT t.lastname,t.firstname,t.zip,t.dob,t.Midnum,ISNULL(t.Contact1,'') AS Contact1,ISNULL(t.Contact2,'') AS Contact2,ISNULL(t.Contact3,'') AS Contact3,E.AttorneyName,t.TicketID_PK,CT.CaseTypeName,T.Activeflag,Cr.RecordCreation, TT.Address1, TT.EmailAddress,tt.gender AS gender INTO #tempClient  
			FROM   #tempCrash Cr
			INNER JOIN #tbltickets TT WITH(NOLOCK) ON  TT.Lastname  = Cr.LastName 
			INNER JOIN tbltickets t	ON  t.TicketID_PK = tt.ticketid	
			AND t.Lastname = Cr.LastName
			AND left(t.Zip,5)  = left(CR.Drvr_Zip,5)  
			INNER JOIN tblTicketsViolations TV WITH(NOLOCK) ON  t.TicketID_PK = tv.TicketID_PK  
			LEFT OUTER JOIN #eventTemp E WITH(NOLOCK)
			ON  TV.casenumassignedbycourt = E.causenumber  
			LEFT OUTER JOIN tblCourts C	ON  TV.CourtID = C.Courtid  
			LEFT OUTER JOIN CaseType CT ON  C.CaseTypeID = Ct.CaseTypeId			
			CREATE NONCLUSTERED INDEX idx_LastName ON #tempClient(LastName)
			CREATE NONCLUSTERED INDEX idx_firstname ON #tempClient(firstname)
			CREATE NONCLUSTERED INDEX idx_zip ON #tempClient(zip)
			CREATE NONCLUSTERED INDEX idx_dob ON #tempClient(dob)
				
		END
		IF(@status = 'Client (F,L,YR)' OR @status = 'Quote (F,L,YR)')
		BEGIN
			SELECT t.lastname, t.firstname,t.zip, t.dob,t.Midnum, ISNULL(t.Contact1,'') AS Contact1, ISNULL(t.Contact2,'') AS Contact2,ISNULL(t.Contact3,'') AS Contact3,E.AttorneyName, t.TicketID_PK,CT.CaseTypeName, T.Activeflag ,Cr.RecordCreation, TT.Address1, TT.EmailAddress INTO #tempClientFirst
			FROM   #tempCrash Cr  
			INNER JOIN #tbltickets TT WITH(NOLOCK) ON  TT.Lastname   = Cr.LastName   
			AND TT.Firstname   = Cr.FirstName    
			INNER JOIN tbltickets t	ON  t.TicketID_PK = tt.ticketid
			AND t.Lastname   = Cr.LastName   
			AND t.Firstname   = Cr.FirstName  	
			--(DATEPART(YEAR, T.DOB) = Cr.DOB	OR DATEPART(YEAR, T.DOB) = (Cr.DOB - 1) OR DATEPART(YEAR, T.DOB) = (Cr.DOB + 1))
			AND DATEPART(YEAR, T.DOB) IN (Cr.DOB,Cr.DOB+1,Cr.DOB-1)
			INNER JOIN tblTicketsViolations TV WITH(NOLOCK)	ON  t.TicketID_PK = tv.TicketID_PK  
			LEFT OUTER JOIN #eventTemp E WITH(NOLOCK) ON  TV.casenumassignedbycourt = E.causenumber
			LEFT OUTER JOIN tblCourts C	ON  TV.CourtID = C.Courtid  LEFT OUTER JOIN CaseType CT	ON  C.CaseTypeID = Ct.CaseTypeId 			
			CREATE NONCLUSTERED INDEX idx_LastName ON #tempClientFirst(LastName)
			CREATE NONCLUSTERED INDEX idx_firstname ON #tempClientFirst(firstname)
			CREATE NONCLUSTERED INDEX idx_zip ON #tempClientFirst(zip)
			CREATE NONCLUSTERED INDEX idx_dob ON #tempClientFirst(dob)
		END
		IF (@status = 'Client (F, L, YR, G, Zip)')
	    BEGIN	    	
	    	--Client (F, L, YR, G, Zip)
	    	SELECT DISTINCT Cr.*,(CASE WHEN ISNULL(t.CaseTypeName, '') <> '' THEN 'Client' ELSE 'N/A' END) AS [Status],t.CaseTypeName,t.Midnum AS midnumber,
			t.Contact1 AS Tel1,t.Contact2 AS Tel2,t.Contact3 AS Tel3,t.Zip AS ZipCode,t.AttorneyName,'1' AS flag,'Client (F, L, YR, G, Zip)' AS connection,
			t.TicketID_PK AS ticketid,Address1,EmailAddress INTO #tempFLYGZ 
	    	FROM   #tempCrash Cr  
	    	INNER JOIN #tempClient t ON  t.lastname  = cr.lastname 
	    	AND t.firstname = cr.firstname 			  
			AND DATEPART(YEAR, T.DOB)  IN (Cr.DOB,Cr.DOB+1,Cr.DOB-1) 
			AND left(t.Zip,5) = left(CR.Drvr_Zip,5)  
			AND ISNULL(T.Activeflag, 0) = 1
			AND Lower(t.gender) = LOWER(Cr.Gender) 
			
			SELECT MAX(ticketid) AS ticketid,LastName,FirstName,Age,DOB,Tel1,Tel2,Tel3 INTO #tmpGFinal FROM   #tempFLYGZ	GROUP BY  LastName,FirstName,Age,DOB,Tel1,Tel2,Tel3
			
			ALTER TABLE #tmpGFinal ADD CrashDate DATETIME,DownloadDate DATETIME,IncidentNumber VARCHAR(200),connection VARCHAR(200),CaseTypeName VARCHAR(200),AttorneyName VARCHAR(200), midnumber VARCHAR(200), 
			ZipCode VARCHAR(200),Tot_Injry_Cnt VARCHAR(100), Poss_Injry_Cnt VARCHAR(100), Thousand_Damage_Fl VARCHAR(100), Crash_Sev_ID VARCHAR(100),Veh_Dmag_Scl_1_ID VARCHAR(100), Veh_Dmag_Scl_2_ID VARCHAR(100),
			Medical_Advisory_Fl VARCHAR(100), Crash_Fatal_Fl VARCHAR(100), Death_Cnt VARCHAR(100), Cmv_Involv_Fl VARCHAR(100),Veh_CMV_Fl VARCHAR(100),INSURANCE_PROOF_DESC VARCHAR(200),Drvr_Lic_Cls_ID	VARCHAR(100), 
			VIN VARCHAR(100), Drvr_Zip VARCHAR(100), Ownr_Zip VARCHAR(100),Crash_ID	VARCHAR(100), RecordCreation DATETIME ,Address1 VARCHAR(300),EmailAddress VARCHAR(200)
			
			UPDATE t
			SET    t.CrashDate = tt.CrashDate,t.DownloadDate = tt.DownloadDate,t.IncidentNumber = tt.IncidentNumber,t.connection = tt.connection,
				   t.CaseTypeName = tt.CaseTypeName,t.AttorneyName = tt.AttorneyName,t.midnumber = tt.midnumber,t.ZipCode = tt.ZipCode,
				   t.Tot_Injry_Cnt = tt.Tot_Injry_Cnt,t.Poss_Injry_Cnt = tt.Poss_Injry_Cnt,t.Thousand_Damage_Fl = tt.Thousand_Damage_Fl,
				   t.Crash_Sev_ID = tt.Crash_Sev_ID,t.Veh_Dmag_Scl_1_ID = tt.Veh_Dmag_Scl_1_ID,t.Veh_Dmag_Scl_2_ID = tt.Veh_Dmag_Scl_2_ID,
				   t.Medical_Advisory_Fl = tt.Medical_Advisory_Fl,t.Crash_Fatal_Fl = tt.Crash_Fatal_Fl,t.Death_Cnt = tt.Death_Cnt,
				   t.Cmv_Involv_Fl = tt.Cmv_Involv_Fl,t.Veh_CMV_Fl = tt.Veh_CMV_Fl,t.INSURANCE_PROOF_DESC = tt.INSURANCE_PROOF_DESC,
				   t.Drvr_Lic_Cls_ID = tt.Drvr_Lic_Cls_ID,t.VIN = tt.VIN,t.Drvr_Zip = tt.Drvr_Zip,t.Ownr_Zip = tt.Ownr_Zip,t.Crash_ID = tt.Crash_ID,
				   t.RecordCreation = tt.RecordCreation, t.Address1 = tt.Address1, t.EmailAddress = tt.EmailAddress
			FROM   #tmpGFinal t  INNER JOIN #tempFLYGZ tt ON  ISNULL(tt.LastName, '') = ISNULL(t.LastName, '') AND ISNULL(tt.FirstName, '') = ISNULL(t.FirstName, '') AND ISNULL(tt.Age, '') = ISNULL(t.Age, '')     AND ISNULL(tt.DOB, '') = ISNULL(t.DOB, '')
	               AND ISNULL(tt.Tel1, '') = ISNULL(t.Tel1, '') AND ISNULL(tt.Tel2, '') = ISNULL(t.Tel2, '') AND ISNULL(tt.Tel3, '') = ISNULL(t.Tel3, '')
	       if not exists(select Crash_ID from Crash.dbo.tempClientFLYGZ where datediff(day,updatedate,getdate()) = 0)
			BEGIN
				--truncate table Crash.dbo.tempClientFLYGZ
				
				INSERT INTO Crash.dbo.tempClientFLYGZ(ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,Connection,dataset,AttyLink,
				LastName,FirstName,Age,[DOB(YR)],[Mid(X-ref)],Tel1,Tel2,Tel3,EmailAddress,Address1,zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
				Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,
				Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID)
				
				SELECT DISTINCT ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,connection AS Connection,CaseTypeName AS dataset,AttorneyName AS AttyLink,
				LastName,FirstName,Age,DOB AS [DOB(YR)],midnumber AS [Mid(X-ref)],Tel1,Tel2,Tel3,EmailAddress,Address1,ZipCode AS zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
				Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,
				Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID FROM   #tmpGFinal ORDER BY RecordCreation DESC
			END 	      
	       SELECT DISTINCT ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,connection AS Connection,dataset,AttyLink,
	       LastName,FirstName,Age,[DOB(YR)],[Mid(X-ref)],Tel1,Tel2,Tel3,EmailAddress,Address1,zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
	       Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,
	       Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID FROM   Crash.dbo.tempClientFLYGZ ORDER BY RecordCreation DESC
	    	
	       DROP TABLE #tmpGFinal  DROP TABLE #tempFLYGZ   DROP TABLE #tempCrash DROP TABLE #tempClient DROP TABLE #eventTemp DROP TABLE #tbltickets
	    						
	    END
	    ELSE	
		IF (@status = 'Client (F,L,YR,Zip)')
	    BEGIN
	    	--Client (F,L,YR,Zip)
	    	SELECT DISTINCT Cr.*,(CASE WHEN ISNULL(t.CaseTypeName, '') <> '' THEN 'Client' ELSE 'N/A' END) AS [Status],t.CaseTypeName,t.Midnum AS midnumber,
			t.Contact1 AS Tel1,t.Contact2 AS Tel2,t.Contact3 AS Tel3,t.Zip AS ZipCode,t.AttorneyName,'1' AS flag,'Client (F,L,YR,Zip)' AS connection,
			t.TicketID_PK AS ticketid,Address1,EmailAddress INTO #tempFLYZ 
	    	FROM   #tempCrash Cr  
	    	INNER JOIN #tempClient t ON  t.lastname  = cr.lastname 
	    	AND t.firstname = cr.firstname 
			--AND (DATEPART(YEAR, T.DOB) = Cr.DOB OR DATEPART(YEAR, T.DOB) = (Cr.DOB - 1) OR DATEPART(YEAR, T.DOB) = (Cr.DOB + 1))  
			AND DATEPART(YEAR, T.DOB)  IN (Cr.DOB,Cr.DOB+1,Cr.DOB-1) 
			AND left(t.Zip,5) = left(CR.Drvr_Zip,5)  
			AND ISNULL(T.Activeflag, 0) = 1
			
			SELECT MAX(ticketid) AS ticketid,LastName,FirstName,Age,DOB,Tel1,Tel2,Tel3 INTO #tmpFinal FROM   #tempFLYZ	GROUP BY  LastName,FirstName,Age,DOB,Tel1,Tel2,Tel3
			
			ALTER TABLE #tmpFinal ADD CrashDate DATETIME,DownloadDate DATETIME,IncidentNumber VARCHAR(200),connection VARCHAR(200),CaseTypeName VARCHAR(200),AttorneyName VARCHAR(200), midnumber VARCHAR(200), 
			ZipCode VARCHAR(200),Tot_Injry_Cnt VARCHAR(100), Poss_Injry_Cnt VARCHAR(100), Thousand_Damage_Fl VARCHAR(100), Crash_Sev_ID VARCHAR(100),Veh_Dmag_Scl_1_ID VARCHAR(100), Veh_Dmag_Scl_2_ID VARCHAR(100),
			Medical_Advisory_Fl VARCHAR(100), Crash_Fatal_Fl VARCHAR(100), Death_Cnt VARCHAR(100), Cmv_Involv_Fl VARCHAR(100),Veh_CMV_Fl VARCHAR(100),INSURANCE_PROOF_DESC VARCHAR(200),Drvr_Lic_Cls_ID	VARCHAR(100), 
			VIN VARCHAR(100), Drvr_Zip VARCHAR(100), Ownr_Zip VARCHAR(100),Crash_ID	VARCHAR(100), RecordCreation DATETIME ,Address1 VARCHAR(300),EmailAddress VARCHAR(200)
			
			UPDATE t
			SET    t.CrashDate = tt.CrashDate,t.DownloadDate = tt.DownloadDate,t.IncidentNumber = tt.IncidentNumber,t.connection = tt.connection,
				   t.CaseTypeName = tt.CaseTypeName,t.AttorneyName = tt.AttorneyName,t.midnumber = tt.midnumber,t.ZipCode = tt.ZipCode,
				   t.Tot_Injry_Cnt = tt.Tot_Injry_Cnt,t.Poss_Injry_Cnt = tt.Poss_Injry_Cnt,t.Thousand_Damage_Fl = tt.Thousand_Damage_Fl,
				   t.Crash_Sev_ID = tt.Crash_Sev_ID,t.Veh_Dmag_Scl_1_ID = tt.Veh_Dmag_Scl_1_ID,t.Veh_Dmag_Scl_2_ID = tt.Veh_Dmag_Scl_2_ID,
				   t.Medical_Advisory_Fl = tt.Medical_Advisory_Fl,t.Crash_Fatal_Fl = tt.Crash_Fatal_Fl,t.Death_Cnt = tt.Death_Cnt,
				   t.Cmv_Involv_Fl = tt.Cmv_Involv_Fl,t.Veh_CMV_Fl = tt.Veh_CMV_Fl,t.INSURANCE_PROOF_DESC = tt.INSURANCE_PROOF_DESC,
				   t.Drvr_Lic_Cls_ID = tt.Drvr_Lic_Cls_ID,t.VIN = tt.VIN,t.Drvr_Zip = tt.Drvr_Zip,t.Ownr_Zip = tt.Ownr_Zip,t.Crash_ID = tt.Crash_ID,
				   t.RecordCreation = tt.RecordCreation, t.Address1 = tt.Address1, t.EmailAddress = tt.EmailAddress
			FROM   #tmpFinal t  INNER JOIN #tempFLYZ tt ON  ISNULL(tt.LastName, '') = ISNULL(t.LastName, '') AND ISNULL(tt.FirstName, '') = ISNULL(t.FirstName, '') AND ISNULL(tt.Age, '') = ISNULL(t.Age, '')     AND ISNULL(tt.DOB, '') = ISNULL(t.DOB, '')
	               AND ISNULL(tt.Tel1, '') = ISNULL(t.Tel1, '') AND ISNULL(tt.Tel2, '') = ISNULL(t.Tel2, '') AND ISNULL(tt.Tel3, '') = ISNULL(t.Tel3, '')
	       
	       if not exists(select Crash_ID from Crash.dbo.tempClientFLYZ where datediff(day,updatedate,getdate()) = 0)
			BEGIN
				--truncate table Crash.dbo.tempClientFLYZ
				
				INSERT INTO Crash.dbo.tempClientFLYZ(ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,Connection,dataset,AttyLink,
				LastName,FirstName,Age,[DOB(YR)],[Mid(X-ref)],Tel1,Tel2,Tel3,EmailAddress,Address1,zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
				Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,
				Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID)
				
				SELECT DISTINCT ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,connection AS Connection,CaseTypeName AS dataset,AttorneyName AS AttyLink,
				LastName,FirstName,Age,DOB AS [DOB(YR)],midnumber AS [Mid(X-ref)],Tel1,Tel2,Tel3,EmailAddress,Address1,ZipCode AS zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
				Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,
				Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID FROM   #tmpFinal ORDER BY RecordCreation DESC
			END 	      
	       SELECT DISTINCT ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,connection AS Connection,dataset,AttyLink,
	       LastName,FirstName,Age,[DOB(YR)],[Mid(X-ref)],Tel1,Tel2,Tel3,EmailAddress,Address1,zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
	       Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,
	       Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID FROM   Crash.dbo.tempClientFLYZ ORDER BY RecordCreation DESC
	        	      
--	       SELECT DISTINCT ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,connection AS Connection,CaseTypeName AS dataset,AttorneyName AS AttyLink,
--	       LastName,FirstName,Age,DOB AS [DOB(YR)],midnumber AS [Mid(X-ref)],Tel1,Tel2,Tel3,EmailAddress,Address1,ZipCode AS zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
--	       Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,
--	       Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID FROM   #tmpFinal ORDER BY RecordCreation DESC
	    	
	       DROP TABLE #tmpFinal  DROP TABLE #tempFLYZ   DROP TABLE #tempCrash DROP TABLE #tempClient DROP TABLE #eventTemp DROP TABLE #tbltickets
	    						
	    END
	    ELSE 
	    IF (@status = 'Client (F,L,YR)')
	    BEGIN
	    	--Client (F,L,YR)
	    	SELECT DISTINCT Cr.*,(CASE WHEN ISNULL(t.CaseTypeName, '') <> '' THEN 'Client' ELSE 'N/A' END) AS [Status],t.CaseTypeName,t.Midnum AS midnumber,t.Contact1 AS Tel1,
	        t.Contact2 AS Tel2,t.Contact3 AS Tel3,t.Zip AS ZipCode,t.AttorneyName,'1' AS flag,'Client (F,L,YR)' AS connection,t.TicketID_PK AS ticketid,t.Address1,t.EmailAddress INTO #tempFLY
	        FROM   #tempCrash Cr  
	        INNER JOIN #tempClientFirst t ON  t.lastname   = cr.lastname   
	        AND t.firstname   = cr.firstname    
	        --AND (DATEPART(YEAR, T.DOB) = Cr.DOB OR DATEPART(YEAR, T.DOB) = (Cr.DOB - 1) OR DATEPART(YEAR, T.DOB) = (Cr.DOB + 1)) 
	        AND DATEPART(YEAR, T.DOB) IN (Cr.DOB,Cr.DOB+1,Cr.DOB-1)
	        AND ISNULL(T.Activeflag, 0) = 1
			SELECT MAX(ticketid) AS ticketid,LastName,FirstName,Age,DOB,Tel1,Tel2,Tel3 INTO #tmpFLY FROM   #tempFLY	GROUP BY  LastName,FirstName,Age,DOB,Tel1,Tel2,Tel3
			ALTER TABLE #tmpFLY ADD CrashDate DATETIME,DownloadDate DATETIME,IncidentNumber VARCHAR(200),connection VARCHAR(200),CaseTypeName VARCHAR(200),AttorneyName VARCHAR(200), midnumber VARCHAR(200), 
			ZipCode VARCHAR(200),Tot_Injry_Cnt VARCHAR(100), Poss_Injry_Cnt VARCHAR(100), Thousand_Damage_Fl VARCHAR(100), Crash_Sev_ID VARCHAR(100),Veh_Dmag_Scl_1_ID VARCHAR(100), Veh_Dmag_Scl_2_ID VARCHAR(100),
			Medical_Advisory_Fl VARCHAR(100), Crash_Fatal_Fl VARCHAR(100), Death_Cnt VARCHAR(100), Cmv_Involv_Fl VARCHAR(100),Veh_CMV_Fl VARCHAR(100),INSURANCE_PROOF_DESC VARCHAR(200),Drvr_Lic_Cls_ID	VARCHAR(100),VIN VARCHAR(100), Drvr_Zip VARCHAR(100), Ownr_Zip VARCHAR(100),Crash_ID	VARCHAR(100),
			RecordCreation DATETIME ,Address1 VARCHAR(300),EmailAddress VARCHAR(200)			
			UPDATE t
			SET    t.CrashDate = tt.CrashDate,t.DownloadDate = tt.DownloadDate,t.IncidentNumber = tt.IncidentNumber,t.connection = tt.connection,t.CaseTypeName = tt.CaseTypeName,t.AttorneyName = tt.AttorneyName,t.midnumber = tt.midnumber,t.ZipCode = tt.ZipCode,
				   t.Tot_Injry_Cnt = tt.Tot_Injry_Cnt,t.Poss_Injry_Cnt = tt.Poss_Injry_Cnt,t.Thousand_Damage_Fl = tt.Thousand_Damage_Fl,t.Crash_Sev_ID = tt.Crash_Sev_ID,t.Veh_Dmag_Scl_1_ID = tt.Veh_Dmag_Scl_1_ID,t.Veh_Dmag_Scl_2_ID = tt.Veh_Dmag_Scl_2_ID,
				   t.Medical_Advisory_Fl = tt.Medical_Advisory_Fl,t.Crash_Fatal_Fl = tt.Crash_Fatal_Fl,t.Death_Cnt = tt.Death_Cnt,t.Cmv_Involv_Fl = tt.Cmv_Involv_Fl,t.Veh_CMV_Fl = tt.Veh_CMV_Fl,t.INSURANCE_PROOF_DESC = tt.INSURANCE_PROOF_DESC,
				   t.Drvr_Lic_Cls_ID = tt.Drvr_Lic_Cls_ID,t.VIN = tt.VIN,t.Drvr_Zip = tt.Drvr_Zip,t.Ownr_Zip = tt.Ownr_Zip,t.Crash_ID = tt.Crash_ID,
				   t.RecordCreation = tt.RecordCreation, t.Address1 = tt.Address1, t.EmailAddress = tt.EmailAddress
			FROM   #tmpFLY t
				   INNER JOIN #tempFLY tt	ON  ISNULL(tt.LastName, '') = ISNULL(t.LastName, '') AND ISNULL(tt.FirstName, '') = ISNULL(t.FirstName, '')	AND ISNULL(tt.Age, '') = ISNULL(t.Age, '')
				   AND ISNULL(tt.DOB, '') = ISNULL(t.DOB, '') AND ISNULL(tt.Tel1, '') = ISNULL(t.Tel1, '') AND ISNULL(tt.Tel2, '') = ISNULL(t.Tel2, '')	AND ISNULL(tt.Tel3, '') = ISNULL(t.Tel3, '')				   
			
		   if not exists(select Crash_ID from Crash.dbo.tempClientFLY where datediff(day,updatedate,getdate()) = 0)
			BEGIN
				--truncate table Crash.dbo.tempClientFLY
				
				INSERT INTO Crash.dbo.tempClientFLY(ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,Connection,dataset,AttyLink,
				LastName,FirstName,Age,[DOB(YR)],[Mid(X-ref)],Tel1,Tel2,Tel3,EmailAddress,Address1,zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
				Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,
				Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID)
				
				SELECT DISTINCT ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,connection AS Connection,CaseTypeName AS dataset,AttorneyName AS AttyLink,
				LastName,FirstName,Age,DOB AS [DOB(YR)],midnumber AS [Mid(X-ref)],Tel1,Tel2,Tel3,EmailAddress,Address1,ZipCode AS zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
				Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,
				Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID FROM   #tmpFLY ORDER BY RecordCreation DESC
			END 	      
	       SELECT DISTINCT ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,connection AS Connection,dataset,AttyLink,
	       LastName,FirstName,Age,[DOB(YR)],[Mid(X-ref)],Tel1,Tel2,Tel3,EmailAddress,Address1,zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
	       Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,
	       Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID FROM   Crash.dbo.tempClientFLY ORDER BY RecordCreation DESC
	          
--		   SELECT DISTINCT ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,connection AS Connection,CaseTypeName AS dataset,AttorneyName AS AttyLink,LastName,FirstName,Age,DOB AS [DOB(YR)],midnumber AS [Mid(X-ref)],Tel1,Tel2,Tel3,EmailAddress,Address1,ZipCode AS zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
--	       Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,
--	       Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID FROM   #tmpFLY	ORDER BY RecordCreation DESC	    
--		   DROP TABLE #tmpFLY	DROP TABLE #tempFLY	DROP TABLE #tempCrash DROP TABLE #tempClientFirst DROP TABLE #eventTemp DROP TABLE #tbltickets				   				        
	    END	
	    ELSE 
	    IF (@status = 'Client (L,Zip)')
	    BEGIN
			--Client (L,Zip)
			SELECT DISTINCT Cr.*,(CASE WHEN ISNULL(ct.CaseTypeName, '') <> '' THEN 'Client' ELSE 'N/A' END) AS [Status],ct.CaseTypeName,t.Midnum AS midnumber,t.Contact1 AS Tel1,
			t.Contact2 AS Tel2,t.Contact3 AS Tel3,t.Zip AS ZipCode,e.AttorneyName,'2' AS flag,'Client (L,Zip)' AS connection,t.TicketID_PK AS ticketid,t.Email AS EmailAddress,(t.Address1 + ISNULL(t.Address2,'')) AS Address1   INTO #tempLZ
			FROM   #tempCrash Cr  INNER JOIN tbltickets t ON  t.lastname  = cr.lastname   
			AND left(t.Zip,5)  = left(CR.Drvr_Zip,5)   
			AND ISNULL(T.Activeflag, 0) = 1 
			INNER JOIN tblTicketsViolations tv ON tv.TicketID_PK = t.TicketID_PK
			LEFT OUTER JOIN #eventTemp E ON e.causenumber = tv.casenumassignedbycourt  LEFT OUTER JOIN tblCourts C  ON  TV.CourtID = C.Courtid	LEFT OUTER JOIN CaseType CT ON  C.CaseTypeID = Ct.CaseTypeId
			SELECT MAX(ticketid) AS ticketid,LastName,FirstName,Age,DOB,Tel1,Tel2,Tel3 INTO #tmpLZ FROM   #tempLZ GROUP BY  LastName,FirstName,Age,DOB,Tel1,Tel2,Tel3
			ALTER TABLE #tmpLZ ADD CrashDate DATETIME,DownloadDate DATETIME, IncidentNumber VARCHAR(200),connection VARCHAR(200),CaseTypeName VARCHAR(200),AttorneyName VARCHAR(200), midnumber VARCHAR(200),ZipCode VARCHAR(200),Tot_Injry_Cnt VARCHAR(100), Poss_Injry_Cnt VARCHAR(100), 
			Thousand_Damage_Fl VARCHAR(100), Crash_Sev_ID VARCHAR(100),Veh_Dmag_Scl_1_ID VARCHAR(100), Veh_Dmag_Scl_2_ID VARCHAR(100),Medical_Advisory_Fl VARCHAR(100),Crash_Fatal_Fl VARCHAR(100), Death_Cnt VARCHAR(100), Cmv_Involv_Fl VARCHAR(100),Veh_CMV_Fl VARCHAR(100),INSURANCE_PROOF_DESC VARCHAR(200),Drvr_Lic_Cls_ID 
			VARCHAR(100),VIN VARCHAR(100), Drvr_Zip VARCHAR(100), Ownr_Zip VARCHAR(100),Crash_ID VARCHAR(100), RecordCreation DATETIME ,Address1 VARCHAR(300),EmailAddress VARCHAR(200)
			UPDATE t
			SET    t.CrashDate = tt.CrashDate,t.DownloadDate = tt.DownloadDate,t.IncidentNumber = tt.IncidentNumber,t.connection = tt.connection,t.CaseTypeName = tt.CaseTypeName,t.AttorneyName = tt.AttorneyName,t.midnumber = tt.midnumber,t.ZipCode = tt.ZipCode,
				   t.Tot_Injry_Cnt = tt.Tot_Injry_Cnt,t.Poss_Injry_Cnt = tt.Poss_Injry_Cnt,t.Thousand_Damage_Fl = tt.Thousand_Damage_Fl,t.Crash_Sev_ID = tt.Crash_Sev_ID,t.Veh_Dmag_Scl_1_ID = tt.Veh_Dmag_Scl_1_ID,t.Veh_Dmag_Scl_2_ID = tt.Veh_Dmag_Scl_2_ID,
				   t.Medical_Advisory_Fl = tt.Medical_Advisory_Fl,t.Crash_Fatal_Fl = tt.Crash_Fatal_Fl,t.Death_Cnt = tt.Death_Cnt,t.Cmv_Involv_Fl = tt.Cmv_Involv_Fl,t.Veh_CMV_Fl = tt.Veh_CMV_Fl,t.INSURANCE_PROOF_DESC = tt.INSURANCE_PROOF_DESC,
				   t.Drvr_Lic_Cls_ID = tt.Drvr_Lic_Cls_ID,t.VIN = tt.VIN,t.Drvr_Zip = tt.Drvr_Zip,t.Ownr_Zip = tt.Ownr_Zip,t.Crash_ID = tt.Crash_ID,
				   t.RecordCreation = tt.RecordCreation, t.Address1 = tt.Address1, t.EmailAddress = tt.EmailAddress
			FROM   #tmpLZ t
				   INNER JOIN #tempLZ tt ON  ISNULL(tt.LastName, '') = ISNULL(t.LastName, '') AND ISNULL(tt.FirstName, '') = ISNULL(t.FirstName, '') AND ISNULL(tt.Age, '') = ISNULL(t.Age, '')  AND ISNULL(tt.DOB, '') = ISNULL(t.DOB, '') AND ISNULL(tt.Tel1, '') = ISNULL(t.Tel1, '') AND ISNULL(tt.Tel2, '') = ISNULL(t.Tel2, '') AND ISNULL(tt.Tel3, '') = ISNULL(t.Tel3, '')
		   if not exists(select Crash_ID from Crash.dbo.tempClientLZ where datediff(day,updatedate,getdate()) = 0)
			BEGIN
				--truncate table Crash.dbo.tempClientLZ
				
				INSERT INTO Crash.dbo.tempClientLZ(ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,Connection,dataset,AttyLink,
				LastName,FirstName,Age,[DOB(YR)],[Mid(X-ref)],Tel1,Tel2,Tel3,EmailAddress,Address1,zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
				Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,
				Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID)
				
				SELECT DISTINCT ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,connection AS Connection,CaseTypeName AS dataset,AttorneyName AS AttyLink,
				LastName,FirstName,Age,DOB AS [DOB(YR)],midnumber AS [Mid(X-ref)],Tel1,Tel2,Tel3,EmailAddress,Address1,ZipCode AS zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
				Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,
				Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID FROM   #tmpLZ ORDER BY RecordCreation DESC
			END 	      
	       SELECT DISTINCT ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,connection AS Connection,dataset,AttyLink,
	       LastName,FirstName,Age,[DOB(YR)],[Mid(X-ref)],Tel1,Tel2,Tel3,EmailAddress,Address1,zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
	       Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,
	       Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID FROM   Crash.dbo.tempClientLZ ORDER BY RecordCreation DESC
	       
--		   SELECT DISTINCT ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,connection AS Connection,CaseTypeName AS dataset,AttorneyName AS AttyLink,LastName,FirstName,Age,DOB AS [DOB(YR)],midnumber AS [Mid(X-ref)],Tel1,Tel2,Tel3,EmailAddress,Address1,ZipCode AS zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
--	       Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,
--	       Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID FROM   #tmpLZ ORDER BY RecordCreation DESC
--		   DROP TABLE #tmpLZ  DROP TABLE #tempLZ  DROP TABLE #tempCrash  DROP TABLE #tempClient  DROP TABLE #eventTemp  DROP TABLE #tbltickets   
	    END
	    ELSE
	    IF (@status = 'Quote (F, L, YR, G, Zip)')
	    BEGIN
	        --Quote (F,L,YR,Zip)
	        SELECT DISTINCT Cr.*,(CASE WHEN ISNULL(t.CaseTypeName, '') <> '' THEN 'Quote'  ELSE 'N/A' END) AS [Status],t.CaseTypeName,t.Midnum AS midnumber,t.Contact1 AS Tel1,
	        t.Contact2 AS Tel2,t.Contact3 AS Tel3,t.Zip AS ZipCode,t.AttorneyName,'3' AS flag,'Quote (F, L, YR, G, Zip)' AS connection,t.TicketID_PK AS ticketid,t.Address1, t.EmailAddress INTO #tempQLFYGZ
	        FROM   #tempCrash Cr 
	        INNER JOIN #tempClient t  ON  t.lastname   = cr.lastname   
	        AND t.firstname   = cr.firstname    	        
	        AND DATEPART(YEAR, T.DOB) IN (Cr.DOB,Cr.DOB+1,Cr.DOB-1)
	        AND left(t.Zip,5)   = left(CR.Drvr_Zip,5)   AND ISNULL(T.Activeflag, 0) = 0
	        AND Lower(t.gender) = LOWER(Cr.Gender) 
	        
			SELECT MAX(ticketid) AS ticketid,LastName,FirstName,Age,DOB,Tel1,Tel2,Tel3 INTO #tmpQLFYGZ FROM   #tempQLFYGZ	GROUP BY  LastName,FirstName,Age,DOB,Tel1,Tel2,Tel3
			ALTER TABLE #tmpQLFYGZ ADD CrashDate DATETIME,DownloadDate DATETIME,IncidentNumber VARCHAR(200),connection VARCHAR(200),CaseTypeName VARCHAR(200),AttorneyName VARCHAR(200), midnumber VARCHAR(200),ZipCode VARCHAR(200),Tot_Injry_Cnt VARCHAR(100), Poss_Injry_Cnt VARCHAR(100),Thousand_Damage_Fl VARCHAR(100), Crash_Sev_ID VARCHAR(100), 
			Veh_Dmag_Scl_1_ID VARCHAR(100), Veh_Dmag_Scl_2_ID VARCHAR(100),Medical_Advisory_Fl VARCHAR(100),Crash_Fatal_Fl VARCHAR(100), Death_Cnt VARCHAR(100), Cmv_Involv_Fl VARCHAR(100),Veh_CMV_Fl VARCHAR(100),INSURANCE_PROOF_DESC VARCHAR(200),Drvr_Lic_Cls_ID VARCHAR(100),VIN VARCHAR(100), Drvr_Zip VARCHAR(100), Ownr_Zip VARCHAR(100),Crash_ID VARCHAR(100),
			RecordCreation DATETIME ,Address1 VARCHAR(300),EmailAddress VARCHAR(200)
			UPDATE t
			SET    t.CrashDate = tt.CrashDate,t.DownloadDate = tt.DownloadDate,t.IncidentNumber = tt.IncidentNumber,t.connection = tt.connection,t.CaseTypeName = tt.CaseTypeName,t.AttorneyName = tt.AttorneyName,t.midnumber = tt.midnumber,t.ZipCode = tt.ZipCode,
				   t.Tot_Injry_Cnt = tt.Tot_Injry_Cnt,t.Poss_Injry_Cnt = tt.Poss_Injry_Cnt,t.Thousand_Damage_Fl = tt.Thousand_Damage_Fl,t.Crash_Sev_ID = tt.Crash_Sev_ID,t.Veh_Dmag_Scl_1_ID = tt.Veh_Dmag_Scl_1_ID,t.Veh_Dmag_Scl_2_ID = tt.Veh_Dmag_Scl_2_ID,
				   t.Medical_Advisory_Fl = tt.Medical_Advisory_Fl,t.Crash_Fatal_Fl = tt.Crash_Fatal_Fl,t.Death_Cnt = tt.Death_Cnt,t.Cmv_Involv_Fl = tt.Cmv_Involv_Fl,t.Veh_CMV_Fl = tt.Veh_CMV_Fl,t.INSURANCE_PROOF_DESC = tt.INSURANCE_PROOF_DESC,
				   t.Drvr_Lic_Cls_ID = tt.Drvr_Lic_Cls_ID,t.VIN = tt.VIN,t.Drvr_Zip = tt.Drvr_Zip,t.Ownr_Zip = tt.Ownr_Zip,t.Crash_ID = tt.Crash_ID,
					t.RecordCreation = tt.RecordCreation, t.Address1 = tt.Address1, t.EmailAddress = tt.EmailAddress

			FROM   #tmpQLFYGZ t
				   INNER JOIN #tempQLFYGZ tt	ON  ISNULL(tt.LastName, '') = ISNULL(t.LastName, '') AND ISNULL(tt.FirstName, '') = ISNULL(t.FirstName, '')	AND ISNULL(tt.Age, '') = ISNULL(t.Age, '') AND ISNULL(tt.DOB, '') = ISNULL(t.DOB, '') AND ISNULL(tt.Tel1, '') = ISNULL(t.Tel1, '') AND ISNULL(tt.Tel2, '') = ISNULL(t.Tel2, '') AND ISNULL(tt.Tel3, '') = ISNULL(t.Tel3, '')
		   
		   if not exists(select Crash_ID from Crash.dbo.tempQuoteFLYGZ where datediff(day,updatedate,getdate()) = 0)
			BEGIN
				--truncate table Crash.dbo.tempQuoteFLYGZ
				
				INSERT INTO Crash.dbo.tempQuoteFLYGZ(ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,Connection,dataset,AttyLink,
				LastName,FirstName,Age,[DOB(YR)],[Mid(X-ref)],Tel1,Tel2,Tel3,EmailAddress,Address1,zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
				Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,
				Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID)
				
				SELECT DISTINCT ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,connection AS Connection,CaseTypeName AS dataset,AttorneyName AS AttyLink,
				LastName,FirstName,Age,DOB AS [DOB(YR)],midnumber AS [Mid(X-ref)],Tel1,Tel2,Tel3,EmailAddress,Address1,ZipCode AS zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
				Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,
				Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID FROM   #tmpQLFYGZ ORDER BY RecordCreation DESC
			END 	      
	       SELECT DISTINCT ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,connection AS Connection,dataset,AttyLink,
	       LastName,FirstName,Age,[DOB(YR)],[Mid(X-ref)],Tel1,Tel2,Tel3,EmailAddress,Address1,zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
	       Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,
	       Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID FROM   Crash.dbo.tempQuoteFLYGZ ORDER BY RecordCreation DESC
		   
--		   SELECT DISTINCT ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,connection AS Connection,CaseTypeName AS dataset,AttorneyName AS AttyLink,LastName,FirstName,Age,DOB AS [DOB(YR)],midnumber AS [Mid(X-ref)],Tel1,Tel2,Tel3,EmailAddress,Address1,ZipCode AS zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
--	       Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID
--	       FROM   #tmpQLFYGZ ORDER BY  RecordCreation DESC
		   DROP TABLE #tmpQLFYGZ   DROP TABLE #tempQLFYGZ  DROP TABLE #tempCrash DROP TABLE #tempClient  DROP TABLE #eventTemp   DROP TABLE #tbltickets
	    END
	    ELSE 
	    IF (@status = 'Quote (F,L,YR,Zip)')
	    BEGIN
	        --Quote (F,L,YR,Zip)
	        SELECT DISTINCT Cr.*,(CASE WHEN ISNULL(t.CaseTypeName, '') <> '' THEN 'Quote'  ELSE 'N/A' END) AS [Status],t.CaseTypeName,t.Midnum AS midnumber,t.Contact1 AS Tel1,
	        t.Contact2 AS Tel2,t.Contact3 AS Tel3,t.Zip AS ZipCode,t.AttorneyName,'3' AS flag,'Quote (F,L,YR,Zip)' AS connection,t.TicketID_PK AS ticketid,t.Address1, t.EmailAddress INTO #tempQLFYZ
	        FROM   #tempCrash Cr 
	        INNER JOIN #tempClient t  ON  t.lastname   = cr.lastname   
	        AND t.firstname   = cr.firstname    	        
	        AND DATEPART(YEAR, T.DOB) IN (Cr.DOB,Cr.DOB+1,Cr.DOB-1)
	        AND left(t.Zip,5)   = left(CR.Drvr_Zip,5)   AND ISNULL(T.Activeflag, 0) = 0
			SELECT MAX(ticketid) AS ticketid,LastName,FirstName,Age,DOB,Tel1,Tel2,Tel3 INTO #tmpQLFYZ FROM   #tempQLFYZ	GROUP BY  LastName,FirstName,Age,DOB,Tel1,Tel2,Tel3
			ALTER TABLE #tmpQLFYZ ADD CrashDate DATETIME,DownloadDate DATETIME,IncidentNumber VARCHAR(200),connection VARCHAR(200),CaseTypeName VARCHAR(200),AttorneyName VARCHAR(200), midnumber VARCHAR(200),ZipCode VARCHAR(200),Tot_Injry_Cnt VARCHAR(100), Poss_Injry_Cnt VARCHAR(100),Thousand_Damage_Fl VARCHAR(100), Crash_Sev_ID VARCHAR(100), 
			Veh_Dmag_Scl_1_ID VARCHAR(100), Veh_Dmag_Scl_2_ID VARCHAR(100),Medical_Advisory_Fl VARCHAR(100),Crash_Fatal_Fl VARCHAR(100), Death_Cnt VARCHAR(100), Cmv_Involv_Fl VARCHAR(100),Veh_CMV_Fl VARCHAR(100),INSURANCE_PROOF_DESC VARCHAR(200),Drvr_Lic_Cls_ID VARCHAR(100),VIN VARCHAR(100), Drvr_Zip VARCHAR(100), Ownr_Zip VARCHAR(100),Crash_ID VARCHAR(100),
			RecordCreation DATETIME ,Address1 VARCHAR(300),EmailAddress VARCHAR(200)
			UPDATE t
			SET    t.CrashDate = tt.CrashDate,t.DownloadDate = tt.DownloadDate,t.IncidentNumber = tt.IncidentNumber,t.connection = tt.connection,t.CaseTypeName = tt.CaseTypeName,t.AttorneyName = tt.AttorneyName,t.midnumber = tt.midnumber,t.ZipCode = tt.ZipCode,
				   t.Tot_Injry_Cnt = tt.Tot_Injry_Cnt,t.Poss_Injry_Cnt = tt.Poss_Injry_Cnt,t.Thousand_Damage_Fl = tt.Thousand_Damage_Fl,t.Crash_Sev_ID = tt.Crash_Sev_ID,t.Veh_Dmag_Scl_1_ID = tt.Veh_Dmag_Scl_1_ID,t.Veh_Dmag_Scl_2_ID = tt.Veh_Dmag_Scl_2_ID,
				   t.Medical_Advisory_Fl = tt.Medical_Advisory_Fl,t.Crash_Fatal_Fl = tt.Crash_Fatal_Fl,t.Death_Cnt = tt.Death_Cnt,t.Cmv_Involv_Fl = tt.Cmv_Involv_Fl,t.Veh_CMV_Fl = tt.Veh_CMV_Fl,t.INSURANCE_PROOF_DESC = tt.INSURANCE_PROOF_DESC,
				   t.Drvr_Lic_Cls_ID = tt.Drvr_Lic_Cls_ID,t.VIN = tt.VIN,t.Drvr_Zip = tt.Drvr_Zip,t.Ownr_Zip = tt.Ownr_Zip,t.Crash_ID = tt.Crash_ID,
					t.RecordCreation = tt.RecordCreation, t.Address1 = tt.Address1, t.EmailAddress = tt.EmailAddress

			FROM   #tmpQLFYZ t
				   INNER JOIN #tempQLFYZ tt	ON  ISNULL(tt.LastName, '') = ISNULL(t.LastName, '') AND ISNULL(tt.FirstName, '') = ISNULL(t.FirstName, '')	AND ISNULL(tt.Age, '') = ISNULL(t.Age, '') AND ISNULL(tt.DOB, '') = ISNULL(t.DOB, '') AND ISNULL(tt.Tel1, '') = ISNULL(t.Tel1, '') AND ISNULL(tt.Tel2, '') = ISNULL(t.Tel2, '') AND ISNULL(tt.Tel3, '') = ISNULL(t.Tel3, '')
		   
		   if not exists(select Crash_ID from Crash.dbo.tempQuoteFLYZ where datediff(day,updatedate,getdate()) = 0)
			BEGIN
				--truncate table Crash.dbo.tempQuoteFLYZ
				
				INSERT INTO Crash.dbo.tempQuoteFLYZ(ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,Connection,dataset,AttyLink,
				LastName,FirstName,Age,[DOB(YR)],[Mid(X-ref)],Tel1,Tel2,Tel3,EmailAddress,Address1,zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
				Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,
				Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID)
				
				SELECT DISTINCT ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,connection AS Connection,CaseTypeName AS dataset,AttorneyName AS AttyLink,
				LastName,FirstName,Age,DOB AS [DOB(YR)],midnumber AS [Mid(X-ref)],Tel1,Tel2,Tel3,EmailAddress,Address1,ZipCode AS zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
				Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,
				Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID FROM   #tmpQLFYZ ORDER BY RecordCreation DESC
			END 	      
	       SELECT DISTINCT ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,connection AS Connection,dataset,AttyLink,
	       LastName,FirstName,Age,[DOB(YR)],[Mid(X-ref)],Tel1,Tel2,Tel3,EmailAddress,Address1,zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
	       Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,
	       Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID FROM   Crash.dbo.tempQuoteFLYZ ORDER BY RecordCreation DESC
		   
--		   SELECT DISTINCT ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,connection AS Connection,CaseTypeName AS dataset,AttorneyName AS AttyLink,LastName,FirstName,Age,DOB AS [DOB(YR)],midnumber AS [Mid(X-ref)],Tel1,Tel2,Tel3,EmailAddress,Address1,ZipCode AS zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
--	       Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID
--	       FROM   #tmpQLFYZ ORDER BY  RecordCreation DESC
		   DROP TABLE #tmpQLFYZ   DROP TABLE #tempQLFYZ  DROP TABLE #tempCrash DROP TABLE #tempClient  DROP TABLE #eventTemp   DROP TABLE #tbltickets
	    END
	    ELSE 
	    IF (@status = 'Quote (F,L,YR)')
	    BEGIN
	        --Quote (F,L,YR)
	        SELECT DISTINCT Cr.*,(CASE WHEN ISNULL(t.CaseTypeName, '') <> '' THEN 'Quote' ELSE 'N/A' END) AS [Status],t.CaseTypeName,t.Midnum AS midnumber,t.Contact1 AS Tel1,
	        t.Contact2 AS Tel2,t.Contact3 AS Tel3,t.Zip AS ZipCode,t.AttorneyName,'3' AS flag,'Quote (F,L,YR)' AS connection,t.TicketID_PK AS ticketid,t.Address1, t.EmailAddress INTO #tempQLFY
	        FROM   #tempCrash Cr 
	        INNER JOIN #tempClientFirst t  ON  t.lastname   = cr.lastname   
	        AND t.firstname   = cr.firstname   
	        --AND (DATEPART(YEAR, T.DOB) = Cr.DOB OR DATEPART(YEAR, T.DOB) = (Cr.DOB - 1) OR DATEPART(YEAR, T.DOB) = (Cr.DOB + 1)) 
	        AND DATEPART(YEAR, T.DOB) IN (Cr.DOB,Cr.DOB+1,Cr.DOB-1)
	        AND ISNULL(T.Activeflag, 0) = 0
			SELECT MAX(ticketid) AS ticketid,LastName,FirstName,Age,DOB,Tel1,Tel2,Tel3 INTO #tmpQLFY FROM   #tempQLFY GROUP BY  LastName,FirstName,Age,DOB,Tel1,Tel2,Tel3
			ALTER TABLE #tmpQLFY ADD CrashDate DATETIME,DownloadDate DATETIME,IncidentNumber VARCHAR(200),connection VARCHAR(200),CaseTypeName VARCHAR(200),AttorneyName VARCHAR(200), midnumber VARCHAR(200),ZipCode VARCHAR(200),Tot_Injry_Cnt VARCHAR(100), Poss_Injry_Cnt VARCHAR(100),Thousand_Damage_Fl VARCHAR(100), Crash_Sev_ID VARCHAR(100),Veh_Dmag_Scl_1_ID VARCHAR(100), Veh_Dmag_Scl_2_ID VARCHAR(100),Medical_Advisory_Fl VARCHAR(100),Crash_Fatal_Fl VARCHAR(100), Death_Cnt VARCHAR(100), Cmv_Involv_Fl VARCHAR(100),Veh_CMV_Fl VARCHAR(100),INSURANCE_PROOF_DESC VARCHAR(200),Drvr_Lic_Cls_ID 
			VARCHAR(100),VIN VARCHAR(100), Drvr_Zip VARCHAR(100), Ownr_Zip VARCHAR(100),Crash_ID VARCHAR(100), RecordCreation DATETIME ,Address1 VARCHAR(300),EmailAddress VARCHAR(200)
			UPDATE t
			SET    t.CrashDate = tt.CrashDate,t.DownloadDate = tt.DownloadDate,t.IncidentNumber = tt.IncidentNumber,t.connection = tt.connection,t.CaseTypeName = tt.CaseTypeName,t.AttorneyName = tt.AttorneyName,t.midnumber = tt.midnumber,t.ZipCode = tt.ZipCode,
				   t.Tot_Injry_Cnt = tt.Tot_Injry_Cnt,t.Poss_Injry_Cnt = tt.Poss_Injry_Cnt,t.Thousand_Damage_Fl = tt.Thousand_Damage_Fl,t.Crash_Sev_ID = tt.Crash_Sev_ID,t.Veh_Dmag_Scl_1_ID = tt.Veh_Dmag_Scl_1_ID,t.Veh_Dmag_Scl_2_ID = tt.Veh_Dmag_Scl_2_ID,
				   t.Medical_Advisory_Fl = tt.Medical_Advisory_Fl,t.Crash_Fatal_Fl = tt.Crash_Fatal_Fl,t.Death_Cnt = tt.Death_Cnt,t.Cmv_Involv_Fl = tt.Cmv_Involv_Fl,t.Veh_CMV_Fl = tt.Veh_CMV_Fl,t.INSURANCE_PROOF_DESC = tt.INSURANCE_PROOF_DESC,
				   t.Drvr_Lic_Cls_ID = tt.Drvr_Lic_Cls_ID,t.VIN = tt.VIN,t.Drvr_Zip = tt.Drvr_Zip,t.Ownr_Zip = tt.Ownr_Zip,t.Crash_ID = tt.Crash_ID,
					t.RecordCreation = tt.RecordCreation, t.Address1 = tt.Address1, t.EmailAddress = tt.EmailAddress		   
			FROM   #tmpQLFY t
				   INNER JOIN #tempQLFY tt ON  ISNULL(tt.LastName, '') = ISNULL(t.LastName, '')	AND ISNULL(tt.FirstName, '') = ISNULL(t.FirstName, '') AND ISNULL(tt.Age, '') = ISNULL(t.Age, '') AND ISNULL(tt.DOB, '') = ISNULL(t.DOB, '') AND ISNULL(tt.Tel1, '') = ISNULL(t.Tel1, '') AND ISNULL(tt.Tel2, '') = ISNULL(t.Tel2, '') AND ISNULL(tt.Tel3, '') = ISNULL(t.Tel3, '')		   
		   
		   if not exists(select Crash_ID from Crash.dbo.tempQuoteFLY where datediff(day,updatedate,getdate()) = 0)
			BEGIN
				--truncate table Crash.dbo.tempQuoteFLY
				
				INSERT INTO Crash.dbo.tempQuoteFLY(ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,Connection,dataset,AttyLink,
				LastName,FirstName,Age,[DOB(YR)],[Mid(X-ref)],Tel1,Tel2,Tel3,EmailAddress,Address1,zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
				Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,
				Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID)
				
				SELECT DISTINCT ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,connection AS Connection,CaseTypeName AS dataset,AttorneyName AS AttyLink,
				LastName,FirstName,Age,DOB AS [DOB(YR)],midnumber AS [Mid(X-ref)],Tel1,Tel2,Tel3,EmailAddress,Address1,ZipCode AS zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
				Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,
				Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID FROM   #tmpQLFY ORDER BY RecordCreation DESC
			END 	      
	       SELECT DISTINCT ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,connection AS Connection,dataset,AttyLink,
	       LastName,FirstName,Age,[DOB(YR)],[Mid(X-ref)],Tel1,Tel2,Tel3,EmailAddress,Address1,zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
	       Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,
	       Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID FROM   Crash.dbo.tempQuoteFLY ORDER BY RecordCreation DESC
		   
--		   SELECT DISTINCT ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,connection AS Connection,CaseTypeName AS dataset,AttorneyName AS AttyLink,LastName,FirstName,Age,DOB AS [DOB(YR)],midnumber AS [Mid(X-ref)],Tel1,Tel2,Tel3,EmailAddress,Address1,ZipCode AS zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
--	       Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID
--	       FROM   #tmpQLFY ORDER BY   RecordCreation DESC
		   DROP TABLE #tmpQLFY  DROP TABLE #tempQLFY  DROP TABLE #tempCrash  DROP TABLE #tempClientFirst DROP TABLE #eventTemp  DROP TABLE #tbltickets
	    END
	    ELSE 
	    IF (@status = 'Quote (L,Zip)')
	    BEGIN
	        --Quote (L,Zip)
			  SELECT DISTINCT Cr.*,(CASE WHEN ISNULL(t.CaseTypeName, '') <> '' THEN 'Quote' ELSE 'N/A' END) AS [Status],t.CaseTypeName,t.Midnum AS midnumber,t.Contact1 AS Tel1,
			  t.Contact2 AS Tel2,t.Contact3 AS Tel3,t.Zip AS ZipCode,t.AttorneyName,'3' AS flag,'Quote (L,Zip)' AS connection,t.TicketID_PK AS ticketid,Address1, EmailAddress INTO #tempQLZ
			  FROM   #tempCrash Cr   
			  INNER JOIN #tempClient t   ON  t.lastname   = cr.lastname   
			  AND left(t.Zip,5)   = left(CR.Drvr_Zip,5)   AND ISNULL(T.Activeflag, 0) = 0
			  SELECT MAX(ticketid) AS ticketid,LastName,FirstName,Age,DOB,Tel1,Tel2,Tel3 INTO #tmpQLZ FROM   #tempQLZ GROUP BY  LastName,FirstName,Age,DOB,Tel1,Tel2,Tel3
			  ALTER TABLE #tmpQLZ ADD CrashDate DATETIME,DownloadDate DATETIME,IncidentNumber VARCHAR(200),connection VARCHAR(200),CaseTypeName VARCHAR(200),AttorneyName VARCHAR(200), midnumber VARCHAR(200),ZipCode VARCHAR(200),Tot_Injry_Cnt VARCHAR(100), Poss_Injry_Cnt VARCHAR(100), 
			  Thousand_Damage_Fl VARCHAR(100), Crash_Sev_ID VARCHAR(100),Veh_Dmag_Scl_1_ID VARCHAR(100), Veh_Dmag_Scl_2_ID VARCHAR(100),Medical_Advisory_Fl VARCHAR(100),Crash_Fatal_Fl VARCHAR(100), Death_Cnt VARCHAR(100), Cmv_Involv_Fl VARCHAR(100), 
			  Veh_CMV_Fl VARCHAR(100),INSURANCE_PROOF_DESC VARCHAR(200),Drvr_Lic_Cls_ID	VARCHAR(100),VIN VARCHAR(100), Drvr_Zip VARCHAR(100), Ownr_Zip VARCHAR(100),Crash_ID VARCHAR(100),
			  RecordCreation DATETIME ,Address1 VARCHAR(300),EmailAddress VARCHAR(200)
			UPDATE t
			SET    t.CrashDate = tt.CrashDate,t.DownloadDate = tt.DownloadDate,t.IncidentNumber = tt.IncidentNumber,t.connection = tt.connection,t.CaseTypeName = tt.CaseTypeName,t.AttorneyName = tt.AttorneyName,t.midnumber = tt.midnumber,t.ZipCode = tt.ZipCode,
				   t.Tot_Injry_Cnt = tt.Tot_Injry_Cnt,t.Poss_Injry_Cnt = tt.Poss_Injry_Cnt,t.Thousand_Damage_Fl = tt.Thousand_Damage_Fl,t.Crash_Sev_ID = tt.Crash_Sev_ID,t.Veh_Dmag_Scl_1_ID = tt.Veh_Dmag_Scl_1_ID,t.Veh_Dmag_Scl_2_ID = tt.Veh_Dmag_Scl_2_ID,
				   t.Medical_Advisory_Fl = tt.Medical_Advisory_Fl,t.Crash_Fatal_Fl = tt.Crash_Fatal_Fl,t.Death_Cnt = tt.Death_Cnt,t.Cmv_Involv_Fl = tt.Cmv_Involv_Fl,t.Veh_CMV_Fl = tt.Veh_CMV_Fl,t.INSURANCE_PROOF_DESC = tt.INSURANCE_PROOF_DESC,
				   t.Drvr_Lic_Cls_ID = tt.Drvr_Lic_Cls_ID,t.VIN = tt.VIN,t.Drvr_Zip = tt.Drvr_Zip,t.Ownr_Zip = tt.Ownr_Zip,t.Crash_ID = tt.Crash_ID,
				   t.RecordCreation = tt.RecordCreation, t.Address1 = tt.Address1, t.EmailAddress = tt.EmailAddress
			FROM   #tmpQLZ t
				   INNER JOIN #tempQLZ tt ON  ISNULL(tt.LastName, '') = ISNULL(t.LastName, '') AND ISNULL(tt.FirstName, '') = ISNULL(t.FirstName, '') AND ISNULL(tt.Age, '') = ISNULL(t.Age, '')
				   AND ISNULL(tt.DOB, '') = ISNULL(t.DOB, '') AND ISNULL(tt.Tel1, '') = ISNULL(t.Tel1, '')	AND ISNULL(tt.Tel2, '') = ISNULL(t.Tel2, '') AND ISNULL(tt.Tel3, '') = ISNULL(t.Tel3, '')
		   
		   
		   if not exists(select Crash_ID from Crash.dbo.tempQuoteLZ where datediff(day,updatedate,getdate()) = 0)
			BEGIN
				--truncate table Crash.dbo.tempQuoteLZ
				
				INSERT INTO Crash.dbo.tempQuoteLZ(ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,Connection,dataset,AttyLink,
				LastName,FirstName,Age,[DOB(YR)],[Mid(X-ref)],Tel1,Tel2,Tel3,EmailAddress,Address1,zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
				Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,
				Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID)
				
				SELECT DISTINCT ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,connection AS Connection,CaseTypeName AS dataset,AttorneyName AS AttyLink,
				LastName,FirstName,Age,DOB AS [DOB(YR)],midnumber AS [Mid(X-ref)],Tel1,Tel2,Tel3,EmailAddress,Address1,ZipCode AS zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
				Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,
				Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID FROM   #tmpQLZ ORDER BY RecordCreation DESC
			END 	      
	       SELECT DISTINCT ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,connection AS Connection,dataset,AttyLink,
	       LastName,FirstName,Age,[DOB(YR)],[Mid(X-ref)],Tel1,Tel2,Tel3,EmailAddress,Address1,zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
	       Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,
	       Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID FROM   Crash.dbo.tempQuoteLZ ORDER BY RecordCreation DESC
	       
--		   SELECT DISTINCT ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,connection AS Connection,CaseTypeName AS dataset,AttorneyName AS AttyLink,LastName,FirstName,Age,DOB AS [DOB(YR)],midnumber AS [Mid(X-ref)],Tel1,Tel2,Tel3,EmailAddress,Address1,ZipCode AS zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
--	       Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID
--	       FROM   #tmpQLZ ORDER BY RecordCreation DESC
		   DROP TABLE #tmpQLZ DROP TABLE #tempQLZ  DROP TABLE #tempCrash DROP TABLE #tempClient  DROP TABLE #eventTemp   DROP TABLE #tbltickets
	    END
	END
	IF (@status = 'Non Client (F,L,YR,G,Zip)' OR @status = 'Non Client (F,L,YR,Zip)' OR @status = 'Non Client (F,L,YR)' OR @status = 'Non Client (L,Zip)')
	BEGIN
		SELECT MAX(t.recordid) AS recordid,t.LastName,t.FirstName,t.PhoneNumber, t.WorkPhoneNumber INTO #tempMaxNonClient
	    FROM   tblTicketsArchive T WITH(NOLOCK) INNER JOIN tblTicketsViolationsArchive TV WITH(NOLOCK) ON  T.Recordid = TV.RecordID AND ISNULL(T.Clientflag, 0) = 0 AND DATEPART(YEAR, T.RecLoadDate) = 2012
	    LEFT OUTER JOIN tblCourts C  ON  TV.CourtLocation = C.Courtid LEFT OUTER JOIN CaseType CT ON  C.CaseTypeID = Ct.CaseTypeId GROUP BY  t.LastName,t.FirstName,t.PhoneNumber,t.WorkPhoneNumber
		CREATE NONCLUSTERED INDEX idx_LastName ON #tempMaxNonClient(LastName)
	    CREATE NONCLUSTERED INDEX idx_firstname ON #tempMaxNonClient(firstname)
	    CREATE NONCLUSTERED INDEX idx_Recordid ON #tempMaxNonClient(recordid)
	    IF(@status = 'Non Client (F,L,YR,G,Zip)' OR @status = 'Non Client (F,L,YR,Zip)' OR @status = 'Non Client (L,Zip)')
	    BEGIN
	    	SELECT t.LastName,t.FirstName,t.DOB,CT.CaseTypeName, t.MidNumber,t.PhoneNumber,t.WorkPhoneNumber,t.ZipCode,tv.AttorneyName, Cr.RecordCreation, '' AS EmailAddress, (t.Address1 + ISNULL(t.Address2,'')) AS Address1, ISNULL(t.Gender,'') AS Gender   INTO #tempNonClientZ
			FROM   #tempCrash Cr 
			INNER JOIN tblTicketsArchive T WITH(NOLOCK) ON  T.Lastname   = Cr.LastName    
			AND left(t.ZipCode,5)   = left(CR.Drvr_Zip,5)   AND ISNULL(T.Clientflag, 0) = 0
			AND DATEPART(YEAR, T.RecLoadDate) = 2012 INNER JOIN #tempMaxNonClient tm  ON  tm.recordid = t.RecordID AND tm.lastname = t.LastName INNER JOIN tblTicketsViolationsArchive TV WITH(NOLOCK)
			ON  T.Recordid = TV.RecordID LEFT OUTER JOIN tblCourts C ON  TV.CourtLocation = C.Courtid LEFT OUTER JOIN CaseType CT ON  C.CaseTypeID = Ct.CaseTypeId
			CREATE NONCLUSTERED INDEX idx_LastName ON #tempNonClientZ(LastName)
			CREATE NONCLUSTERED INDEX idx_firstname ON #tempNonClientZ(firstname)
			CREATE NONCLUSTERED INDEX idx_zip ON #tempNonClientZ(ZipCode)
			CREATE NONCLUSTERED INDEX idx_dob ON #tempNonClientZ(dob)	    
	    END
	    IF(@status = 'Non Client (F,L,YR)')
	    BEGIN
	    	SELECT t.LastName,t.FirstName,t.DOB,CT.CaseTypeName,t.MidNumber,t.PhoneNumber,t.WorkPhoneNumber,t.ZipCode,tv.AttorneyName, Cr.RecordCreation, '' AS EmailAddress, (t.Address1 + ISNULL(t.Address2,'')) AS Address1 INTO #tempNonClientF
			FROM   #tempCrash Cr 
			INNER JOIN tblTicketsArchive T WITH(NOLOCK) ON  T.Lastname    = Cr.LastName    
			AND T.Lastname   = Cr.LastName    
			--AND (DATEPART(YEAR, T.DOB) = Cr.DOB OR DATEPART(YEAR, T.DOB) = (Cr.DOB - 1) OR DATEPART(YEAR, T.DOB) = (Cr.DOB + 1)) 
			 AND DATEPART(YEAR, T.DOB) IN (Cr.DOB,Cr.DOB+1,Cr.DOB-1)
			AND ISNULL(T.Clientflag, 0) = 0 AND DATEPART(YEAR, T.RecLoadDate) = 2012 INNER JOIN #tempMaxNonClient tm  ON  tm.recordid = t.RecordID          AND tm.lastname = t.LastName  INNER JOIN tblTicketsViolationsArchive TV WITH(NOLOCK)
			ON  T.Recordid = TV.RecordID LEFT OUTER JOIN tblCourts C ON  TV.CourtLocation = C.Courtid LEFT OUTER JOIN CaseType CT ON  C.CaseTypeID = Ct.CaseTypeId
			CREATE NONCLUSTERED INDEX idx_LastName ON #tempNonClientF(LastName)
			CREATE NONCLUSTERED INDEX idx_firstname ON #tempNonClientF(firstname)
			CREATE NONCLUSTERED INDEX idx_zip ON #tempNonClientF(ZipCode)
			CREATE NONCLUSTERED INDEX idx_dob ON #tempNonClientF(dob)
	    END
	    IF (@status = 'Non Client (F,L,YR,G,Zip)')
	    BEGIN
	        --Non Client (F,L,YR,Zip)
	        SELECT DISTINCT Cr.*,(CASE WHEN ISNULL(t.CaseTypeName, '') <> '' THEN 'Non Client' ELSE 'N/A' END) AS [Status],t.CaseTypeName,t.MidNumber AS midnumber,t.PhoneNumber AS Tel1,t.WorkPhoneNumber AS Tel2,
	        '' AS Tel3,t.ZipCode AS ZipCode,t.AttorneyName,'4' AS flag,'Non Client (F,L,YR,G,Zip)' AS connection,0 AS ticketid, t.EmailAddress, t.Address1 
	        INTO #tempNLFYGZ FROM   #tempCrash Cr 
	        INNER JOIN #tempNonClientZ t ON  t.lastname    = cr.lastname                
	        AND t.firstname    = cr.firstname   	        
	        AND DATEPART(YEAR, T.DOB) IN (Cr.DOB,Cr.DOB+1,Cr.DOB-1)
	        AND left(t.ZipCode,5)    = left(CR.Drvr_Zip,5) 
	        AND Lower(t.gender) = LOWER(Cr.Gender) 
	        
			SELECT MAX(ticketid) AS ticketid,LastName,FirstName,Age,DOB,Tel1,Tel2,Tel3,RecordCreation,Address1, EmailAddress INTO #tmpNLFYGZ FROM   #tempNLFYGZ	GROUP BY  LastName,FirstName,Age,DOB,Tel1,Tel2,Tel3,RecordCreation,Address1, EmailAddress
			ALTER TABLE #tmpNLFYGZ ADD CrashDate DATETIME,DownloadDate DATETIME,IncidentNumber VARCHAR(200),connection VARCHAR(200),CaseTypeName VARCHAR(200),AttorneyName VARCHAR(200), midnumber VARCHAR(200), 
			ZipCode VARCHAR(200),Tot_Injry_Cnt VARCHAR(100), Poss_Injry_Cnt VARCHAR(100),Thousand_Damage_Fl VARCHAR(100), Crash_Sev_ID VARCHAR(100),Veh_Dmag_Scl_1_ID VARCHAR(100), Veh_Dmag_Scl_2_ID VARCHAR(100),
			Medical_Advisory_Fl VARCHAR(100), Crash_Fatal_Fl VARCHAR(100), Death_Cnt VARCHAR(100), Cmv_Involv_Fl VARCHAR(100),Veh_CMV_Fl VARCHAR(100),INSURANCE_PROOF_DESC VARCHAR(200),Drvr_Lic_Cls_ID	VARCHAR(100), 
			VIN VARCHAR(100), Drvr_Zip VARCHAR(100), Ownr_Zip VARCHAR(100),Crash_ID	VARCHAR(100)			
			UPDATE t
			SET    t.CrashDate = tt.CrashDate,t.DownloadDate = tt.DownloadDate,t.IncidentNumber = tt.IncidentNumber,t.connection = tt.connection,t.CaseTypeName = tt.CaseTypeName,t.AttorneyName = tt.AttorneyName,t.midnumber = tt.midnumber,t.ZipCode = tt.ZipCode,
				   t.Tot_Injry_Cnt = tt.Tot_Injry_Cnt,t.Poss_Injry_Cnt = tt.Poss_Injry_Cnt,t.Thousand_Damage_Fl = tt.Thousand_Damage_Fl,t.Crash_Sev_ID = tt.Crash_Sev_ID,t.Veh_Dmag_Scl_1_ID = tt.Veh_Dmag_Scl_1_ID,t.Veh_Dmag_Scl_2_ID = tt.Veh_Dmag_Scl_2_ID,
				   t.Medical_Advisory_Fl = tt.Medical_Advisory_Fl,t.Crash_Fatal_Fl = tt.Crash_Fatal_Fl,t.Death_Cnt = tt.Death_Cnt,t.Cmv_Involv_Fl = tt.Cmv_Involv_Fl,t.Veh_CMV_Fl = tt.Veh_CMV_Fl,t.INSURANCE_PROOF_DESC = tt.INSURANCE_PROOF_DESC,
				   t.Drvr_Lic_Cls_ID = tt.Drvr_Lic_Cls_ID,t.VIN = tt.VIN,t.Drvr_Zip = tt.Drvr_Zip,t.Ownr_Zip = tt.Ownr_Zip,t.Crash_ID = tt.Crash_ID
			FROM   #tmpNLFYGZ t INNER JOIN #tempNLFYGZ tt	ON  ISNULL(tt.LastName, '') = ISNULL(t.LastName, '') AND ISNULL(tt.FirstName, '') = ISNULL(t.FirstName, '')	AND ISNULL(tt.Age, '') = ISNULL(t.Age, '') AND ISNULL(tt.DOB, '') = ISNULL(t.DOB, '') AND ISNULL(tt.Tel1, '') = ISNULL(t.Tel1, '')
			AND ISNULL(tt.Tel2, '') = ISNULL(t.Tel2, '') AND ISNULL(tt.Tel3, '') = ISNULL(t.Tel3, '')
		   
		   if not exists(select Crash_ID from Crash.dbo.tempNonClientFLYGZ where datediff(day,updatedate,getdate()) = 0)
			BEGIN
				--truncate table Crash.dbo.tempNonClientFLYGZ
				
				INSERT INTO Crash.dbo.tempNonClientFLYGZ(ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,Connection,dataset,AttyLink,
				LastName,FirstName,Age,[DOB(YR)],[Mid(X-ref)],Tel1,Tel2,Tel3,EmailAddress,Address1,zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
				Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,
				Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID)
				
				SELECT DISTINCT ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,connection AS Connection,CaseTypeName AS dataset,AttorneyName AS AttyLink,
				LastName,FirstName,Age,DOB AS [DOB(YR)],midnumber AS [Mid(X-ref)],Tel1,ISNULL(Tel2,''),ISNULL(Tel3,''),EmailAddress,Address1,ZipCode AS zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
				Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,
				Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID FROM   #tmpNLFYGZ ORDER BY RecordCreation DESC
			END 	      
	       SELECT DISTINCT ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,connection AS Connection,dataset,AttyLink,
	       LastName,FirstName,Age,[DOB(YR)],[Mid(X-ref)],Tel1,Tel2,Tel3,EmailAddress,Address1,zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
	       Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,
	       Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID FROM   Crash.dbo.tempNonClientFLYGZ ORDER BY RecordCreation DESC
		   
--		   SELECT DISTINCT ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,connection AS Connection,CaseTypeName AS dataset,AttorneyName AS AttyLink,LastName,FirstName,Age,DOB AS [DOB(YR)],midnumber AS [Mid(X-ref)],Tel1,Tel2,Tel3,EmailAddress,Address1,ZipCode AS zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
--	       Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID
--	       FROM   #tmpNLFYGZ	ORDER BY  RecordCreation DESC	    
	       DROP TABLE #tmpNLFYGZ DROP TABLE #tempNLFYGZ  DROP TABLE #tempCrash DROP TABLE #tempNonClientZ DROP TABLE #tempMaxNonClient       
	    END
	    ELSE 
	    IF (@status = 'Non Client (F,L,YR,Zip)')
	    BEGIN
	        --Non Client (F,L,YR,Zip)
	        SELECT DISTINCT Cr.*,(CASE WHEN ISNULL(t.CaseTypeName, '') <> '' THEN 'Non Client' ELSE 'N/A' END) AS [Status],t.CaseTypeName,t.MidNumber AS midnumber,t.PhoneNumber AS Tel1,t.WorkPhoneNumber AS Tel2,
	        '' AS Tel3,t.ZipCode AS ZipCode,t.AttorneyName,'4' AS flag,'Non Client (F,L,YR,Zip)' AS connection,0 AS ticketid, t.EmailAddress, t.Address1 INTO #tempNLFYZ FROM   #tempCrash Cr 
	        INNER JOIN #tempNonClientZ t ON  t.lastname    = cr.lastname                
	        AND t.firstname    = cr.firstname   
	        --AND (DATEPART(YEAR, T.DOB) = Cr.DOB  OR DATEPART(YEAR, T.DOB) = (Cr.DOB - 1) OR DATEPART(YEAR, T.DOB) = (Cr.DOB + 1)) 
	        AND DATEPART(YEAR, T.DOB) IN (Cr.DOB,Cr.DOB+1,Cr.DOB-1)
	        AND left(t.ZipCode,5)    = left(CR.Drvr_Zip,5)   
			SELECT MAX(ticketid) AS ticketid,LastName,FirstName,Age,DOB,Tel1,Tel2,Tel3,RecordCreation,Address1, EmailAddress INTO #tmpNLFYZ FROM   #tempNLFYZ	GROUP BY  LastName,FirstName,Age,DOB,Tel1,Tel2,Tel3,RecordCreation,Address1, EmailAddress
			ALTER TABLE #tmpNLFYZ ADD CrashDate DATETIME,DownloadDate DATETIME,IncidentNumber VARCHAR(200),connection VARCHAR(200),CaseTypeName VARCHAR(200),AttorneyName VARCHAR(200), midnumber VARCHAR(200), 
			ZipCode VARCHAR(200),Tot_Injry_Cnt VARCHAR(100), Poss_Injry_Cnt VARCHAR(100),Thousand_Damage_Fl VARCHAR(100), Crash_Sev_ID VARCHAR(100),Veh_Dmag_Scl_1_ID VARCHAR(100), Veh_Dmag_Scl_2_ID VARCHAR(100),
			Medical_Advisory_Fl VARCHAR(100), Crash_Fatal_Fl VARCHAR(100), Death_Cnt VARCHAR(100), Cmv_Involv_Fl VARCHAR(100),Veh_CMV_Fl VARCHAR(100),INSURANCE_PROOF_DESC VARCHAR(200),Drvr_Lic_Cls_ID	VARCHAR(100), 
			VIN VARCHAR(100), Drvr_Zip VARCHAR(100), Ownr_Zip VARCHAR(100),Crash_ID	VARCHAR(100)			
			UPDATE t
			SET    t.CrashDate = tt.CrashDate,t.DownloadDate = tt.DownloadDate,t.IncidentNumber = tt.IncidentNumber,t.connection = tt.connection,t.CaseTypeName = tt.CaseTypeName,t.AttorneyName = tt.AttorneyName,t.midnumber = tt.midnumber,t.ZipCode = tt.ZipCode,
				   t.Tot_Injry_Cnt = tt.Tot_Injry_Cnt,t.Poss_Injry_Cnt = tt.Poss_Injry_Cnt,t.Thousand_Damage_Fl = tt.Thousand_Damage_Fl,t.Crash_Sev_ID = tt.Crash_Sev_ID,t.Veh_Dmag_Scl_1_ID = tt.Veh_Dmag_Scl_1_ID,t.Veh_Dmag_Scl_2_ID = tt.Veh_Dmag_Scl_2_ID,
				   t.Medical_Advisory_Fl = tt.Medical_Advisory_Fl,t.Crash_Fatal_Fl = tt.Crash_Fatal_Fl,t.Death_Cnt = tt.Death_Cnt,t.Cmv_Involv_Fl = tt.Cmv_Involv_Fl,t.Veh_CMV_Fl = tt.Veh_CMV_Fl,t.INSURANCE_PROOF_DESC = tt.INSURANCE_PROOF_DESC,
				   t.Drvr_Lic_Cls_ID = tt.Drvr_Lic_Cls_ID,t.VIN = tt.VIN,t.Drvr_Zip = tt.Drvr_Zip,t.Ownr_Zip = tt.Ownr_Zip,t.Crash_ID = tt.Crash_ID
			FROM   #tmpNLFYZ t INNER JOIN #tempNLFYZ tt	ON  ISNULL(tt.LastName, '') = ISNULL(t.LastName, '') AND ISNULL(tt.FirstName, '') = ISNULL(t.FirstName, '')	AND ISNULL(tt.Age, '') = ISNULL(t.Age, '') AND ISNULL(tt.DOB, '') = ISNULL(t.DOB, '') AND ISNULL(tt.Tel1, '') = ISNULL(t.Tel1, '')
			AND ISNULL(tt.Tel2, '') = ISNULL(t.Tel2, '') AND ISNULL(tt.Tel3, '') = ISNULL(t.Tel3, '')
		   
		   if not exists(select Crash_ID from Crash.dbo.tempNonClientFLYZ where datediff(day,updatedate,getdate()) = 0)
			BEGIN
				--truncate table Crash.dbo.tempNonClientFLYZ
				
				INSERT INTO Crash.dbo.tempNonClientFLYZ(ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,Connection,dataset,AttyLink,
				LastName,FirstName,Age,[DOB(YR)],[Mid(X-ref)],Tel1,Tel2,Tel3,EmailAddress,Address1,zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
				Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,
				Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID)
				
				SELECT DISTINCT ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,connection AS Connection,CaseTypeName AS dataset,AttorneyName AS AttyLink,
				LastName,FirstName,Age,DOB AS [DOB(YR)],midnumber AS [Mid(X-ref)],Tel1,ISNULL(Tel2,''),ISNULL(Tel3,''),EmailAddress,Address1,ZipCode AS zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
				Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,
				Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID FROM   #tmpNLFYZ ORDER BY RecordCreation DESC
			END 	      
	       SELECT DISTINCT ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,connection AS Connection,dataset,AttyLink,
	       LastName,FirstName,Age,[DOB(YR)],[Mid(X-ref)],Tel1,Tel2,Tel3,EmailAddress,Address1,zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
	       Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,
	       Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID FROM   Crash.dbo.tempNonClientFLYZ ORDER BY RecordCreation DESC
		   
		   
--		   SELECT DISTINCT ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,connection AS Connection,CaseTypeName AS dataset,AttorneyName AS AttyLink,LastName,FirstName,Age,DOB AS [DOB(YR)],midnumber AS [Mid(X-ref)],Tel1,Tel2,Tel3,EmailAddress,Address1,ZipCode AS zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
--	       Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID
--	       FROM   #tmpNLFYZ	ORDER BY  RecordCreation DESC	    
	       DROP TABLE #tmpNLFYZ DROP TABLE #tempNLFYZ  DROP TABLE #tempCrash DROP TABLE #tempNonClientZ DROP TABLE #tempMaxNonClient       
	    END
	    ELSE 
	    IF (@status = 'Non Client (F,L,YR)')
	    BEGIN
	        --Non Client (F,L,YR)	
	        SELECT DISTINCT Cr.*,(CASE WHEN ISNULL(t.CaseTypeName, '') <> '' THEN 'Non Client' ELSE 'N/A' END) AS [Status],t.CaseTypeName,t.MidNumber AS midnumber,t.PhoneNumber AS Tel1,t.WorkPhoneNumber AS Tel2,
	        '' AS Tel3,t.ZipCode AS ZipCode,t.AttorneyName,'4' AS flag,'Non Client (F,L,YR)' AS connection,0 AS ticketid,t.Address1, t.EmailAddress  INTO #tempNFLY FROM   #tempCrash Cr INNER JOIN #tempNonClientF t
	        ON  t.lastname    = cr.lastname    AND t.firstname    = cr.firstname    
	        --AND (DATEPART(YEAR, T.DOB) = Cr.DOB OR DATEPART(YEAR, T.DOB) = (Cr.DOB - 1) OR DATEPART(YEAR, T.DOB) = (Cr.DOB + 1))
	         AND DATEPART(YEAR, T.DOB) IN (Cr.DOB,Cr.DOB+1,Cr.DOB-1)
	        SELECT MAX(ticketid) AS ticketid,LastName,FirstName,Age,DOB,Tel1,Tel2,Tel3,RecordCreation,Address1, EmailAddress INTO #tmpNFLY FROM   #tempNFLY	GROUP BY  LastName,FirstName,Age,DOB,Tel1,Tel2,Tel3,RecordCreation,Address1, EmailAddress
			ALTER TABLE #tmpNFLY ADD CrashDate DATETIME,DownloadDate DATETIME,IncidentNumber VARCHAR(200),connection VARCHAR(200),CaseTypeName VARCHAR(200),AttorneyName VARCHAR(200), midnumber VARCHAR(200), 
			ZipCode VARCHAR(200),Tot_Injry_Cnt VARCHAR(100), Poss_Injry_Cnt VARCHAR(100), Thousand_Damage_Fl VARCHAR(100), Crash_Sev_ID VARCHAR(100),Veh_Dmag_Scl_1_ID VARCHAR(100), Veh_Dmag_Scl_2_ID VARCHAR(100),
			Medical_Advisory_Fl VARCHAR(100), Crash_Fatal_Fl VARCHAR(100), Death_Cnt VARCHAR(100), Cmv_Involv_Fl VARCHAR(100),Veh_CMV_Fl VARCHAR(100),INSURANCE_PROOF_DESC VARCHAR(200),Drvr_Lic_Cls_ID 
			VARCHAR(100), VIN VARCHAR(100), Drvr_Zip VARCHAR(100), Ownr_Zip VARCHAR(100),Crash_ID VARCHAR(100)
			UPDATE t
			SET    t.CrashDate = tt.CrashDate,t.DownloadDate = tt.DownloadDate,t.IncidentNumber = tt.IncidentNumber,t.connection = tt.connection,  t.CaseTypeName = tt.CaseTypeName,t.AttorneyName = tt.AttorneyName,t.midnumber = tt.midnumber,t.ZipCode = tt.ZipCode,
				   t.Tot_Injry_Cnt = tt.Tot_Injry_Cnt,t.Poss_Injry_Cnt = tt.Poss_Injry_Cnt,t.Thousand_Damage_Fl = tt.Thousand_Damage_Fl,t.Crash_Sev_ID = tt.Crash_Sev_ID,t.Veh_Dmag_Scl_1_ID = tt.Veh_Dmag_Scl_1_ID,t.Veh_Dmag_Scl_2_ID = tt.Veh_Dmag_Scl_2_ID,
				   t.Medical_Advisory_Fl = tt.Medical_Advisory_Fl,t.Crash_Fatal_Fl = tt.Crash_Fatal_Fl,t.Death_Cnt = tt.Death_Cnt,t.Cmv_Involv_Fl = tt.Cmv_Involv_Fl,t.Veh_CMV_Fl = tt.Veh_CMV_Fl,t.INSURANCE_PROOF_DESC = tt.INSURANCE_PROOF_DESC,
				   t.Drvr_Lic_Cls_ID = tt.Drvr_Lic_Cls_ID,t.VIN = tt.VIN,t.Drvr_Zip = tt.Drvr_Zip,t.Ownr_Zip = tt.Ownr_Zip,t.Crash_ID = tt.Crash_ID FROM   #tmpNFLY t
				   INNER JOIN #tempNFLY tt ON  ISNULL(tt.LastName, '') = ISNULL(t.LastName, '')	AND ISNULL(tt.FirstName, '') = ISNULL(t.FirstName, '') AND ISNULL(tt.Age, '') = ISNULL(t.Age, '')
				   AND ISNULL(tt.DOB, '') = ISNULL(t.DOB, '') AND ISNULL(tt.Tel1, '') = ISNULL(t.Tel1, '')	AND ISNULL(tt.Tel2, '') = ISNULL(t.Tel2, '') AND ISNULL(tt.Tel3, '') = ISNULL(t.Tel3, '')
			
			if not exists(select Crash_ID from Crash.dbo.tempNonClientFLY where datediff(day,updatedate,getdate()) = 0)
			BEGIN
				--truncate table Crash.dbo.tempNonClientFLY
				
				INSERT INTO Crash.dbo.tempNonClientFLY(ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,Connection,dataset,AttyLink,
				LastName,FirstName,Age,[DOB(YR)],[Mid(X-ref)],Tel1,Tel2,Tel3,EmailAddress,Address1,zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
				Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,
				Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID)
				
				SELECT DISTINCT ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,connection AS Connection,CaseTypeName AS dataset,AttorneyName AS AttyLink,
				LastName,FirstName,Age,DOB AS [DOB(YR)],midnumber AS [Mid(X-ref)],ISNULL(Tel1,''),ISNULL(Tel2,''),ISNULL(Tel3,''),EmailAddress,Address1,ZipCode AS zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
				Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,
				Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID FROM   #tmpNFLY ORDER BY RecordCreation DESC
			END 	      
	       SELECT DISTINCT ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,connection AS Connection,dataset,AttyLink,
	       LastName,FirstName,Age,[DOB(YR)],[Mid(X-ref)],Tel1,Tel2,Tel3,EmailAddress,Address1,zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
	       Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,
	       Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID FROM   Crash.dbo.tempNonClientFLY ORDER BY RecordCreation DESC
	       
--			SELECT DISTINCT ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,connection AS Connection,CaseTypeName AS dataset,AttorneyName AS AttyLink,LastName,FirstName,Age,DOB AS [DOB(YR)],midnumber AS [Mid(X-ref)],Tel1,Tel2,Tel3,EmailAddress,Address1,ZipCode AS zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
--	        Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID
--	        FROM   #tmpNFLY ORDER BY  RecordCreation DESC
			DROP TABLE #tmpNFLY  DROP TABLE #tempNFLY  DROP TABLE #tempCrash DROP TABLE #tempNonClientF DROP TABLE #tempMaxNonClient
	    END
	    ELSE 
	    IF (@status = 'Non Client (L,Zip)')
	    BEGIN
	        --Non Client (L,Zip)	
	        SELECT DISTINCT Cr.*,(CASE WHEN ISNULL(t.CaseTypeName, '') <> '' THEN 'Non Client' ELSE 'N/A' END) AS [Status],t.CaseTypeName,t.MidNumber AS midnumber,t.PhoneNumber AS Tel1,t.WorkPhoneNumber AS Tel2,
	        '' AS Tel3,t.ZipCode AS ZipCode,t.AttorneyName,'4' AS flag,'Non Client (L,Zip)' AS connection,0 AS ticketid,t.Address1, t.EmailAddress INTO #tempNLZ FROM   #tempCrash Cr 
	        INNER JOIN #tempNonClientZ t ON  t.lastname    = cr.lastname    
	        AND left(t.ZipCode,5)    = left(CR.Drvr_Zip,5)   	    
			SELECT MAX(ticketid) AS ticketid,LastName,FirstName,Age,DOB,Tel1,Tel2,Tel3,RecordCreation,Address1, EmailAddress INTO #tmpNLZ FROM   #tempNLZ	GROUP BY  LastName,FirstName,Age,DOB,Tel1,Tel2,Tel3,RecordCreation,Address1, EmailAddress
			ALTER TABLE #tmpNLZ ADD CrashDate DATETIME,DownloadDate DATETIME, IncidentNumber VARCHAR(200),connection VARCHAR(200),CaseTypeName VARCHAR(200),AttorneyName VARCHAR(200), midnumber VARCHAR(200), 
			ZipCode VARCHAR(200),Tot_Injry_Cnt VARCHAR(100), Poss_Injry_Cnt VARCHAR(100), Thousand_Damage_Fl VARCHAR(100), Crash_Sev_ID VARCHAR(100),Veh_Dmag_Scl_1_ID VARCHAR(100), Veh_Dmag_Scl_2_ID VARCHAR(100),
			Medical_Advisory_Fl VARCHAR(100), Crash_Fatal_Fl VARCHAR(100), Death_Cnt VARCHAR(100), Cmv_Involv_Fl VARCHAR(100),Veh_CMV_Fl VARCHAR(100),INSURANCE_PROOF_DESC VARCHAR(200),Drvr_Lic_Cls_ID	VARCHAR(100), 
			VIN VARCHAR(100), Drvr_Zip VARCHAR(100), Ownr_Zip VARCHAR(100),Crash_ID	VARCHAR(100)
			UPDATE t
			SET    t.CrashDate = tt.CrashDate,t.DownloadDate = tt.DownloadDate,t.IncidentNumber = tt.IncidentNumber,t.connection = tt.connection,t.CaseTypeName = tt.CaseTypeName,t.AttorneyName = tt.AttorneyName,t.midnumber = tt.midnumber,t.ZipCode = tt.ZipCode,
				   t.Tot_Injry_Cnt = tt.Tot_Injry_Cnt,t.Poss_Injry_Cnt = tt.Poss_Injry_Cnt,t.Thousand_Damage_Fl = tt.Thousand_Damage_Fl,t.Crash_Sev_ID = tt.Crash_Sev_ID,t.Veh_Dmag_Scl_1_ID = tt.Veh_Dmag_Scl_1_ID,t.Veh_Dmag_Scl_2_ID = tt.Veh_Dmag_Scl_2_ID,
				   t.Medical_Advisory_Fl = tt.Medical_Advisory_Fl,t.Crash_Fatal_Fl = tt.Crash_Fatal_Fl,t.Death_Cnt = tt.Death_Cnt,t.Cmv_Involv_Fl = tt.Cmv_Involv_Fl,t.Veh_CMV_Fl = tt.Veh_CMV_Fl,t.INSURANCE_PROOF_DESC = tt.INSURANCE_PROOF_DESC,
				   t.Drvr_Lic_Cls_ID = tt.Drvr_Lic_Cls_ID,t.VIN = tt.VIN,t.Drvr_Zip = tt.Drvr_Zip,t.Ownr_Zip = tt.Ownr_Zip,t.Crash_ID = tt.Crash_ID	FROM   #tmpNLZ t  INNER JOIN #tempNLZ tt
						ON  ISNULL(tt.LastName, '') = ISNULL(t.LastName, '') AND ISNULL(tt.FirstName, '') = ISNULL(t.FirstName, '')	AND ISNULL(tt.Age, '') = ISNULL(t.Age, '') AND ISNULL(tt.DOB, '') = ISNULL(t.DOB, '') AND ISNULL(tt.Tel1, '') = ISNULL(t.Tel1, '')	AND ISNULL(tt.Tel2, '') = ISNULL(t.Tel2, '') AND ISNULL(tt.Tel3, '') = ISNULL(t.Tel3, '')
		   
		   if not exists(select Crash_ID from Crash.dbo.tempNonClientLZ where datediff(day,updatedate,getdate()) = 0)
			BEGIN
				--truncate table Crash.dbo.tempNonClientLZ
				
				INSERT INTO Crash.dbo.tempNonClientLZ(ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,Connection,dataset,AttyLink,
				LastName,FirstName,Age,[DOB(YR)],[Mid(X-ref)],Tel1,Tel2,Tel3,EmailAddress,Address1,zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
				Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,
				Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID)
				
				SELECT DISTINCT ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,connection AS Connection,CaseTypeName AS dataset,AttorneyName AS AttyLink,
				LastName,FirstName,Age,DOB AS [DOB(YR)],midnumber AS [Mid(X-ref)],ISNULL(Tel1,''),ISNULL(Tel2,''),ISNULL(Tel3,''),EmailAddress,Address1,ZipCode AS zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
				Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,
				Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID FROM   #tmpNLZ ORDER BY RecordCreation DESC
			END 	      
	       SELECT DISTINCT ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,connection AS Connection,dataset,AttyLink,
	       LastName,FirstName,Age,[DOB(YR)],[Mid(X-ref)],Tel1,Tel2,Tel3,EmailAddress,Address1,zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
	       Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,
	       Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID FROM   Crash.dbo.tempNonClientLZ ORDER BY RecordCreation DESC
	       
--		   SELECT DISTINCT ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,connection AS Connection,CaseTypeName AS dataset,AttorneyName AS AttyLink,LastName,FirstName,Age,DOB AS [DOB(YR)],midnumber AS [Mid(X-ref)],Tel1,Tel2,Tel3,Address1, EmailAddress,ZipCode AS zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
--	       Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID
--	       FROM   #tmpNLZ	ORDER BY RecordCreation DESC
	       DROP TABLE #tmpNLZ  DROP TABLE #tempNLZ  DROP TABLE #tempCrash  DROP TABLE #tempNonClientZ  DROP TABLE #tempMaxNonClient
	    END
	END
IF(@status = 'All Clients')
BEGIN
SELECT DISTINCT * into #tmpFinAll FROM Crash.dbo.tempClientFLYGZ 
UNION ALL SELECT DISTINCT *  FROM Crash.dbo.tempClientFLYZ    
UNION ALL SELECT DISTINCT * FROM Crash.dbo.tempClientFLY    
UNION ALL SELECT DISTINCT * FROM Crash.dbo.tempClientLZ

SELECT DISTINCT ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,connection AS Connection,dataset,AttyLink,
LastName,FirstName,Age,[DOB(YR)],[Mid(X-ref)],Tel1,Tel2,Tel3,EmailAddress,Address1,zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,
Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID FROM   #tmpFinAll ORDER BY RecordCreation DESC

DROP TABLE #tmpFinAll
TRUNCATE TABLE Crash.dbo.tempClientFLYGZ
TRUNCATE TABLE Crash.dbo.tempClientFLYZ
TRUNCATE TABLE Crash.dbo.tempClientFLY
TRUNCATE TABLE Crash.dbo.tempClientLZ
END
IF(@status = 'All Quote Clients')
BEGIN
SELECT DISTINCT * into #tmpQuoteAll FROM Crash.dbo.tempQuoteFLYGZ 
UNION ALL SELECT DISTINCT *  FROM Crash.dbo.tempQuoteFLYZ    
UNION ALL SELECT DISTINCT * FROM Crash.dbo.tempQuoteFLY    
UNION ALL SELECT DISTINCT * FROM Crash.dbo.tempQuoteLZ

SELECT DISTINCT ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,connection AS Connection,dataset,AttyLink,
LastName,FirstName,Age,[DOB(YR)],[Mid(X-ref)],Tel1,Tel2,Tel3,EmailAddress,Address1,zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,
Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID FROM   #tmpQuoteAll ORDER BY RecordCreation DESC

DROP TABLE #tmpQuoteAll


TRUNCATE TABLE Crash.dbo.tempQuoteFLYGZ
TRUNCATE TABLE Crash.dbo.tempQuoteFLYZ
TRUNCATE TABLE Crash.dbo.tempQuoteFLY
TRUNCATE TABLE Crash.dbo.tempQuoteLZ
END

END
IF(@status = 'All Non Clients')
BEGIN
SELECT DISTINCT * into #tmpNonClientAll FROM Crash.dbo.tempNonClientFLYGZ 
UNION ALL SELECT DISTINCT *  FROM Crash.dbo.tempNonClientFLYZ    
UNION ALL SELECT DISTINCT * FROM Crash.dbo.tempNonClientFLY    
UNION ALL SELECT DISTINCT * FROM Crash.dbo.tempNonClientLZ

SELECT DISTINCT ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,connection AS Connection,dataset,AttyLink,
LastName,FirstName,Age,[DOB(YR)],[Mid(X-ref)],Tel1,Tel2,Tel3,EmailAddress,Address1,zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,
Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID FROM   #tmpNonClientAll ORDER BY RecordCreation DESC

DROP TABLE #tmpNonClientAll

TRUNCATE TABLE Crash.dbo.tempNonClientFLYGZ
TRUNCATE TABLE Crash.dbo.tempNonClientFLYZ
TRUNCATE TABLE Crash.dbo.tempNonClientFLY
TRUNCATE TABLE Crash.dbo.tempNonClientLZ
END

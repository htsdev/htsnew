SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_GET_LETTERBATCH]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_GET_LETTERBATCH]
GO


CREATE PROCEDURE [USP_HTS_GET_LETTERBATCH]  --to get ist letters count    
@LType int      
 AS      
      
select  l.LetterName,count(distinct ticketid_fk) as [Count] ,b.batchdate as FileDate ,sum(isprinted) as printed   
from tblhtsbatchprintletter b        
inner join      
tblhtsletters l      
on  l.letterid_pk = b.letterid_fk  and b.batchid_pk=(  select top 1 tbv.batchID_pk from tblhtsbatchprintletter tbv                                 
  where  tbv.ticketid_fk=b.ticketid_fk and tbv.LetterID_Fk = @LType and deleteflag<>1  )   --and tbv.ISPrinted = 0  
         
where b.LetterID_Fk = @LType --and b.ISPrinted = 0    
and deleteflag<>1     
    
      
group by b.batchdate,l.LetterName     
  



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


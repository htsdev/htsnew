SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_hts_insert_clientemailinfo]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_hts_insert_clientemailinfo]
GO



CREATE procedure usp_hts_insert_clientemailinfo

@ticketid int,
@lettertype int,
@attchfile varchar(200),
@To varchar(100),
@From varchar(50),
@CC varchar(100),
@BCC varchar(50),
@subject varchar(200),
@body varchar(500)
as

insert into tblemailnotes
(
ticketid,
lettertype,
[filename],
emailto,
emailfrom,
emailcc,
emailbcc,
emailsubject,
emailmessage
)
values
(
@ticketid,
@lettertype ,
@attchfile ,
@To ,
@From ,
@CC ,
@BCC ,
@subject ,
@body 
)
select max(emailid) from tblemailnotes


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


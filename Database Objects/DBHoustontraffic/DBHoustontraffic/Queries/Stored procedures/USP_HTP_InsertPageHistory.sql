

/************************************************

Created By: Aziz-ur_Rehman
BuisnessLogic: This procedure is used to insert information like (opendate,username) that open the ticketdesk site  
 
List of Parameters:

@Submenuid: represents the menuid from wher user open this report. 
@Employeeid: reprensent employeeid who open this report

List of Columns:

ViewDate: this is the date on which user open the ticketdesk site.

*************************************************/
  
create PROCEDURE [dbo].[USP_HTP_InsertPageHistory]  
(  
 @SubMenuId int,  
 @EmployeeId int
)  
 
as  
  
insert into PageHistory (SubMenuId, EmployeeId, ViewDate) 
values(@SubMenuId,  @EmployeeId, getDate())
go
 
grant execute on [USP_HTP_InsertPageHistory] to [dbr_webuser]
go 

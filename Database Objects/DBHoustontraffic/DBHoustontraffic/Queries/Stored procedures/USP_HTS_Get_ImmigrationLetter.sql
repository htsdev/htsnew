/*    
Created By: Haris Ahmed    
Date:   08/30/2012    
    
Business Logic:    
 The stored procedure is used to get Immigration Comment Status    
*/

ALTER PROCEDURE [dbo].[USP_HTS_Get_ImmigrationLetter]
	@empid INT,
	@LetterType INT,
	@TicketIDList VARCHAR(MAX)
AS

DECLARE @tickets TABLE (ticketid NUMERIC)             
    
    INSERT INTO @tickets
    SELECT *
    FROM   dbo.Sap_String_Param_New(@TicketIDList) 
    
SELECT tt.TicketID_PK,
	   tt.Firstname,
       tt.Lastname,
       tt.DOB,
       CAST(DATEDIFF(DAY, tt.DOB, GETDATE())/365.00 AS INT) Age,
       thic.Comments,
       tt.Address1 + ' ' + ISNULL(tt.Address2, '') AS ADDRESS,
       tt.City,
       ts.STATE AS STATE,
       tt.Zip,
       CONVERT(
           VARCHAR(20),
           ISNULL(dbo.formatphonenumbers(tt.Contact1), '')
       ) + '(' + ISNULL(LEFT(ct1.Description, 1), '') + ')' AS contact1,
       CONVERT(
           VARCHAR(20),
           ISNULL(dbo.formatphonenumbers(tt.Contact2), '')
       ) + '(' + ISNULL(LEFT(ct2.Description, 1), '') + ')' AS contact2,
       CONVERT(
           VARCHAR(20),
           ISNULL(dbo.formatphonenumbers(tt.Contact3), '')
       ) + '(' + ISNULL(LEFT(ct3.Description, 1), '') + ')' AS contact3,
       thic.InsertDate AS InquiryDate,
       thics.[Status],
       ISNULL(tt.LanguageSpeak,'ENGLISH') AS [Language] --Farrukh 11180 06/26/2013 added Language field for both "ENGLSH" and "SPANISH" clients
FROM   tblTickets tt
       INNER JOIN tbl_HTS_ImmigrationComments thic
            ON  thic.TicketID = tt.TicketID_PK
       INNER JOIN tbl_HTS_ImmigrationCommentsStatus thics
            ON  thics.StatusID = thic.StatusID
       INNER JOIN tblState ts
            ON  ts.StateID = tt.Stateid_FK
       LEFT OUTER JOIN dbo.tblContactstype ct2
            ON  tt.ContactType2 = ct2.ContactType_PK
       LEFT OUTER JOIN dbo.tblContactstype ct1
            ON  tt.ContactType1 = ct1.ContactType_PK
       LEFT OUTER JOIN dbo.tblContactstype ct3
            ON  tt.ContactType3 = ct3.ContactType_PK
WHERE 
	tt.TicketID_PK IN (SELECT ticketid
               FROM   @tickets)

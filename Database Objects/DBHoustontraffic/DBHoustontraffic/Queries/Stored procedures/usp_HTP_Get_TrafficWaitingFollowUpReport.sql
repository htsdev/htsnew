﻿

/****** 
Create by:  Abid Ali

Business Logic : This procedure simply return the traffic waiting follow up record from table

-- Abid Ali 5018 12/24/2008 Traffic records display

Parameters List:
		@ShowAllRecords		: To show all records
		@ShowAllPastRecords : Show all past or today's records ( default set )
		@FromFollowUpDate	: Show according to From follow up date
		@ToFollowUpDate		: Show according to To follow up date

Column Return : 
				TicketID_PK
				Firstname
				Lastname
				TrafficWaitingFollowUpDate
				TrafficFollowUpDate
				TicketsViolationID
				CourtViolationStatusIDmain
				RefCaseNumber
				CauseNumber
				Courtnumber
				ShortName
				VerifiedCourtStatus
				CourtDate
				VerifiedCourtDate
				Criminalfollowupdate
				TrafficWaitingFollowUp
				CrtDate
				sortcourtdater
				pastdue	
******/

ALTER Procedure [dbo].[usp_HTP_Get_TrafficWaitingFollowUpReport] 
-- Abid Ali 5018 12/24/2008 Traffic records display
(
	@ShowAllRecords BIT = 0,
	@ShowAllPastRecords BIT = 1,
	@FromFollowUpDate DATETIME = '01/01/1900',
	@ToFollowUpDate DATETIME = '01/01/2012',
	@ValidationCategory int=0 -- Saeed 7791 07/09/2010 @ValidationCategory=1 for alert & @ValidationCategory=2 for report
)
AS

	-- Abid Ali 5018 12/24/2008 Traffic records display
	SET @FromFollowUpDate = convert(varchar,@FromFollowUpDate,101) + ' 00:00:00'
	SET @ToFollowUpDate = convert(varchar,@ToFollowUpDate,101) + ' 23:59:58'
	
	-- Haris Ahmed 10286 10/10/2012  Traffic Waiting Follow Up
	/***********************************/
	DECLARE @ReportID INT 	
	SELECT @ReportID = tr.Report_ID
	FROM   tbl_Report tr
	WHERE  tr.Report_Url = '/Reports/WaitingFollowUpTraffic.aspx'
	
	DECLARE @EndDate DATETIME
	SET @EndDate = GETDATE()
	/**********************************/
	
	IF (@ShowAllRecords = 1 OR @ValidationCategory=2)  -- Saeed 7791 07/10/2010 display all records if @ShowAllRecords=1 or validation category is 'Report'
	BEGIN

select t1.* from (
		SELECT	
			tblTickets.TicketID_PK,
			tblTickets.Firstname,
			tblTickets.Lastname,
			tblTickets.TrafficWaitingFollowUpDate,
			dbo.fn_DateFormat(trafficWaitingFollowUpDate, 25, '/', ':', 1) as TrafficFollowUpDate,
			tblTicketsViolations.TicketsViolationID,
			tblTicketsViolations.CourtViolationStatusIDmain,
			tblticketsViolations.RefCaseNumber,
			tblticketsViolations.casenumassignedbycourt as CauseNumber,
			tblticketsViolations.Courtnumber, 
			tblCourts.ShortName,
			tblTickets.DLNumber AS DL,
			-- Abid Ali 12/26/2008 5018 for Cov column			
			tblFirm.FirmAbbreviation  AS COV,
			tblCourtViolationStatus.ShortDescription as VerifiedCourtStatus,
			dbo.fn_DateFormat(tblticketsViolations.Courtdate, 25, '/', ':', 1) as CourtDate,
			dbo.fn_DateFormat(tblticketsViolations.CourtDateMain, 25, '/', ':', 1) as  VerifiedCourtDate,
			tblTickets.Criminalfollowupdate AS followupdate,
			case	convert(varchar,isnull(tblTickets.trafficWaitingFollowUpDate,' '),101) 
					when '01/01/1900' then ' ' 
					else convert(varchar,tblTickets.trafficWaitingFollowUpDate,101) 
					end as TrafficWaitingFollowUp,
					--Muhammad Ali 8031 07/16/2010 --> Add four Columns.. two for sorting and 2 for info
			CONVERT(VARCHAR,tblticketsViolations.CourtDateMain,101) AS CrtDate,
			tblticketsViolations.CourtDateMain AS sortcourtdate,
			 CASE
			 WHEN CONVERT(INT,DATEDIFF(DAY,tblticketsViolations.CourtDateMain,GETDATE()))< 0 THEN '  ' 
			 ELSE CONVERT(VARCHAR, DATEDIFF(DAY,tblticketsViolations.CourtDateMain,GETDATE()) )
			END AS 	pastdue	,
			DATEDIFF(DAY,tblticketsViolations.CourtDateMain,GETDATE()) AS sortedPastDue, 
			tblCourts.CourtCategoryNum, tblCourts.CourtID
		FROM	
				tblTickets
				INNER JOIN	tblTicketsViolations 
						ON	tblTickets.TicketID_PK = tblTicketsViolations.TicketID_PK
				INNER JOIN	tblCourts 
						ON	tblTicketsViolations.CourtID = tblCourts.CourtID
				INNER JOIN	tblCourtViolationStatus 
						ON	tblTicketsViolations.CourtViolationStatusID = tblCourtViolationStatus.CourtViolationStatusID
							AND 
							tblTicketsViolations.CourtViolationStatusIDmain = tblCourtViolationStatus.CourtViolationStatusID
				-- Abid Ali 5018 for Cov column
				LEFT OUTER JOIN tblFirm
						ON	Isnull(tblTicketsViolations.CoveringFirmID,3000) = tblFirm.FirmID
				INNER JOIN ReportSetting rs -- Haris Ahmed 10286 10/10/2012 No of business day on or before for Traffic Waiting Follow Up
	                    ON  rs.CourtID = tblTicketsViolations.CourtID AND rs.AttributeId = 1
				
		WHERE	
				tblTicketsViolations.courtviolationstatusidmain = 104 -- waiting status
--				AND
--				tblCourts.CourtCategoryNum not in (1,2) -- HMC and HCJP
				AND
				tblCourts.CaseTypeId = 1 -- Traffic Case
				AND
				tblTickets.ActiveFlag = 1 -- Active client
				--order by  tblTickets.trafficWaitingFollowUpDate, tblTickets.TicketID_PK -- Adil Aleem 5857 05/15/2009 Include Ticket ID in order by clause
				--Haris Ahmed 10286 10/10/2012 No of business day on or before for Traffic Waiting Follow Up
				AND (ISNULL(rs.ReportID, 0) = @ReportID
				AND DATEDIFF(
					   DAY,
					   (
						   SELECT MAX(ttvl.Recdate)
						   FROM   tblTicketsViolationLog ttvl
						   WHERE  ttvl.TicketviolationID = tblTicketsViolations.TicketsViolationID AND ttvl.newverifiedstatus = 104 AND ttvl.oldverifiedstatus <> 104
					   ),
					   dbo.fn_getprevbusinessday(@EndDate, ISNULL(rs.AttributeValue, 0))
				) >= 0)
) as t1 
WHERE 

t1.CourtCategoryNum not in (1,2)
OR
--Muhammad Nadir Siddiqui 8423 04/27/2011 Cases in HCJP Court with more than 180 days.
(t1.CourtCategoryNum =2
and
DATEDIFF(DAY,(SELECT MAX(ttvl.Recdate) FROM tblTicketsViolationLog ttvl WHERE ttvl.TicketviolationID=t1.TicketsViolationID ), GETDATE())> 180
)
order by  t1.trafficWaitingFollowUpDate, t1.TicketID_PK
	END
	ELSE IF (@ShowAllPastRecords = 1 OR @ValidationCategory=1) -- Saeed 7791 07/10/2010 display  if @ShowAllPastRecords=1 or validation category is 'Alert' then display records which has 'follow up date' null,past or today.
	BEGIN

 	
		select t1.* from (
SELECT	

			tblTickets.TicketID_PK,
			tblTickets.Firstname,
			tblTickets.Lastname,
			tblTickets.TrafficWaitingFollowUpDate,
			dbo.fn_DateFormat(trafficWaitingFollowUpDate, 25, '/', ':', 1) as TrafficFollowUpDate,
			tblTicketsViolations.TicketsViolationID,
			tblTicketsViolations.CourtViolationStatusIDmain,
			tblticketsViolations.RefCaseNumber,
			tblticketsViolations.casenumassignedbycourt as CauseNumber,
			tblticketsViolations.Courtnumber, 
			tblCourts.ShortName,
			tblTickets.DLNumber AS DL,
			-- Abid Ali 12/26/2008 5018 for Cov column
			tblFirm.FirmAbbreviation  AS COV,
			tblCourtViolationStatus.ShortDescription as VerifiedCourtStatus,
			dbo.fn_DateFormat(tblticketsViolations.Courtdate, 25, '/', ':', 1) as CourtDate,
			dbo.fn_DateFormat(tblticketsViolations.CourtDateMain, 25, '/', ':', 1) as  VerifiedCourtDate,
			tblTickets.Criminalfollowupdate AS followupdate,
			case	convert(varchar,isnull(tblTickets.trafficWaitingFollowUpDate,' '),101) 
					when '01/01/1900' then ' ' 
					else convert(varchar,tblTickets.trafficWaitingFollowUpDate,101) 
					end as TrafficWaitingFollowUp,
					--Muhammad Ali 8031 07/16/2010 --> Add four Columns.. two for sorting and 2 for info
			CONVERT(VARCHAR,tblticketsViolations.CourtDateMain,101)	AS CrtDate,
			tblticketsViolations.CourtDateMain AS sortcourtdate,
			 CASE
			 WHEN CONVERT(INT,DATEDIFF(DAY,tblticketsViolations.CourtDateMain,GETDATE()))< 0 THEN '  ' 
			 ELSE CONVERT(VARCHAR, DATEDIFF(DAY,tblticketsViolations.CourtDateMain,GETDATE()) )
			END AS 	pastdue	,
			DATEDIFF(DAY,tblticketsViolations.CourtDateMain,GETDATE()) AS sortedPastDue,
			tblCourts.CourtCategoryNum,tblCourts.CourtID
		FROM	
				tblTickets
				INNER JOIN	tblTicketsViolations 
						ON	tblTickets.TicketID_PK = tblTicketsViolations.TicketID_PK
				INNER JOIN	tblCourts 
						ON	tblTicketsViolations.CourtID = tblCourts.CourtID
				INNER JOIN	tblCourtViolationStatus 
						ON	tblTicketsViolations.CourtViolationStatusID = tblCourtViolationStatus.CourtViolationStatusID
							AND 
							tblTicketsViolations.CourtViolationStatusIDmain = tblCourtViolationStatus.CourtViolationStatusID
				-- Abid Ali 5018 for Cov column
				LEFT OUTER JOIN tblFirm
						ON	Isnull(tblTicketsViolations.CoveringFirmID,3000) = tblFirm.FirmID
				INNER JOIN ReportSetting rs -- Haris Ahmed 10286 10/10/2012 No of business day on or before for Traffic Waiting Follow Up
	                    ON  rs.CourtID = tblTicketsViolations.CourtID AND rs.AttributeId = 1
				
		WHERE	
				tblTicketsViolations.courtviolationstatusidmain = 104 -- waiting status
				AND
				--Sabir Khan 5183 11/20/2008 To Exclude all HMC HCJP courts
				--Nadir 8423 04/27/2011 Commit Condition
				--tblCourts.CourtCategoryNum not in (1,2) -- HMC and HCJP
				--AND
				tblCourts.CaseTypeId = 1 -- Traffic Case
				AND
				tblTickets.ActiveFlag = 1 -- Active client
				AND 
				-- Abid Ali 5018 12/24/2008 Trafic all till today
				DATEDIFF(DAY,ISNULL(tblTickets.TrafficWaitingFollowUpDate, GETDATE()) , GETDATE()) >= 0 -- today or in past
				--Sabir Khan 5297 12/03/2008 sort by follow up date...
				--order by  tblTickets.trafficWaitingFollowUpDate, tblTickets.TicketID_PK -- Adil Aleem 5857 05/15/2009 Include Ticket ID in order by clause
				--Haris Ahmed 10286 10/10/2012 No of business day on or before for Traffic Waiting Follow Up
				AND (ISNULL(rs.ReportID, 0) = @ReportID
				AND DATEDIFF(
					   DAY,
					   (
						   SELECT MAX(ttvl.Recdate)
						   FROM   tblTicketsViolationLog ttvl
						   WHERE  ttvl.TicketviolationID = tblTicketsViolations.TicketsViolationID AND ttvl.newverifiedstatus = 104 AND ttvl.oldverifiedstatus <> 104
					   ),
					   dbo.fn_getprevbusinessday(@EndDate, ISNULL(rs.AttributeValue, 0))
				) >= 0)
) as t1
where

t1.CourtCategoryNum not in (1,2)

OR
--Muhammad Nadir Siddiqui 8423 04/27/2011 Cases in HCJP Court with more than 180 days.
(t1.CourtCategoryNum =2
and
DATEDIFF(DAY,(SELECT MAX(ttvl.Recdate) FROM tblTicketsViolationLog ttvl WHERE ttvl.TicketviolationID=t1.TicketsViolationID ), GETDATE())> 180
)

 order by  t1.trafficWaitingFollowUpDate, t1.TicketID_PK
	END
	ELSE
	BEGIN

		select t1.* from (
SELECT	
			tblTickets.TicketID_PK,
			tblTickets.Firstname,
			tblTickets.Lastname,
			tblTickets.TrafficWaitingFollowUpDate,
			dbo.fn_DateFormat(trafficWaitingFollowUpDate, 25, '/', ':', 1) as TrafficFollowUpDate,
			tblTicketsViolations.TicketsViolationID,
			tblTicketsViolations.CourtViolationStatusIDmain,
			tblticketsViolations.RefCaseNumber,
			tblticketsViolations.casenumassignedbycourt as CauseNumber,
			tblticketsViolations.Courtnumber, 
			tblCourts.ShortName,
			tblTickets.DLNumber AS DL,
			-- Abid Ali 12/26/2008 5018 for Cov column
			tblFirm.FirmAbbreviation  AS COV,
			tblCourtViolationStatus.ShortDescription as VerifiedCourtStatus,
			dbo.fn_DateFormat(tblticketsViolations.Courtdate, 25, '/', ':', 1) as CourtDate,
			dbo.fn_DateFormat(tblticketsViolations.CourtDateMain, 25, '/', ':', 1) as  VerifiedCourtDate,
			tblTickets.Criminalfollowupdate AS followupdate,
			case	convert(varchar,isnull(tblTickets.trafficWaitingFollowUpDate,' '),101) 
					when '01/01/1900' then ' ' 
					else convert(varchar,tblTickets.trafficWaitingFollowUpDate,101) 
					end as TrafficWaitingFollowUp,
					--Muhammad Ali 8031 07/16/2010 --> Add four Columns.. two for sorting and 2 for info
			CONVERT(VARCHAR,tblticketsViolations.CourtDateMain,101)	AS CrtDate,
			tblticketsViolations.CourtDateMain AS sortcourtdate,
			 CASE
			 WHEN CONVERT(INT,DATEDIFF(DAY,tblticketsViolations.CourtDateMain,GETDATE()))< 0 THEN '  ' 
			 ELSE CONVERT(VARCHAR, DATEDIFF(DAY,tblticketsViolations.CourtDateMain,GETDATE()) )
			END AS 	pastdue	,
			DATEDIFF(DAY,tblticketsViolations.CourtDateMain,GETDATE()) AS sortedPastDue,
			tblCourts.CourtCategoryNum,tblCourts.CourtID
		FROM	
				tblTickets
				INNER JOIN	tblTicketsViolations 
						ON	tblTickets.TicketID_PK = tblTicketsViolations.TicketID_PK
				INNER JOIN	tblCourts 
						ON	tblTicketsViolations.CourtID = tblCourts.CourtID
				INNER JOIN	tblCourtViolationStatus 
						ON	tblTicketsViolations.CourtViolationStatusID = tblCourtViolationStatus.CourtViolationStatusID
							AND 
							tblTicketsViolations.CourtViolationStatusIDmain = tblCourtViolationStatus.CourtViolationStatusID
				-- Abid Ali 5018 for Cov column
				LEFT OUTER JOIN tblFirm
						ON	Isnull(tblTicketsViolations.CoveringFirmID,3000) = tblFirm.FirmID
				INNER JOIN ReportSetting rs -- Haris Ahmed 10286 10/10/2012 No of business day on or before for Traffic Waiting Follow Up
	                    ON  rs.CourtID = tblTicketsViolations.CourtID AND rs.AttributeId = 1
				
		WHERE	
				tblTicketsViolations.courtviolationstatusidmain = 104 -- waiting status
				AND
				--tblCourts.CourtCategoryNum not in (1,2) -- HMC and HCJP
				--AND
				tblCourts.CaseTypeId = 1 -- Traffic Case
				AND
				tblTickets.ActiveFlag = 1 -- Active client
				AND 
				( -- filter by date
					convert(varchar,tblTickets.trafficWaitingFollowUpDate,101) BETWEEN @FromFollowUpDate AND @ToFollowUpDate
				) 				
				--order by  tblTickets.trafficWaitingFollowUpDate, tblTickets.TicketID_PK -- Adil Aleem 5857 05/15/2009 Include Ticket ID in order by clause
				--Haris Ahmed 10286 10/10/2012 No of business day on or before for Traffic Waiting Follow Up
				AND (ISNULL(rs.ReportID, 0) = @ReportID
				AND DATEDIFF(
					   DAY,
					   (
						   SELECT MAX(ttvl.Recdate)
						   FROM   tblTicketsViolationLog ttvl
						   WHERE  ttvl.TicketviolationID = tblTicketsViolations.TicketsViolationID AND ttvl.newverifiedstatus = 104 AND ttvl.oldverifiedstatus <> 104
					   ),
					   dbo.fn_getprevbusinessday(@EndDate, ISNULL(rs.AttributeValue, 0))
				) >= 0)
	
) as t1 

WHERE 		

t1.CourtCategoryNum not in (1,2)

OR
--Muhammad Nadir Siddiqui 8423 04/27/2011 Cases in HCJP Court with more than 180 days.
(t1.CourtCategoryNum =2 
and
DATEDIFF(DAY,(SELECT MAX(ttvl.Recdate) FROM tblTicketsViolationLog ttvl WHERE ttvl.TicketviolationID=t1.TicketsViolationID ), GETDATE())> 180
)	
order by  t1.trafficWaitingFollowUpDate, t1.TicketID_PK


END

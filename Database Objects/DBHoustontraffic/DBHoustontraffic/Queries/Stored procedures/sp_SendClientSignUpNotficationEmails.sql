set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

/****** 
Alter by		: Syed Muhammad Ozair
Created Date	: 01/21/2009
Task ID			: 5434
Business Logic : This procedure is used to send email to useres checked/selected under text message page
				 for inside courts client having vioation status in Arrignment or Judge and court date between 
				 notice start/end date

Parameters : 
				@MsgId
				@empid

******/ 
ALTER PROCEDURE [dbo].[sp_SendClientSignUpNotficationEmails]
	@ticketid INT,
	@Resendby VARCHAR(10) = ''
AS
	--ozair 5434 01/21/2009 increase the length of @name and @email and @emailalias as precaution
	DECLARE @email VARCHAR(1000),
	        @notice TINYINT,
	        @ticketnumber VARCHAR(20),
	        @causenumber VARCHAR(20), --   --Sabir Khan 6478 08/28/2009 
	        @firstname VARCHAR(20),
	        @lastname VARCHAR(40),
	        @subject VARCHAR(100),
	        @name VARCHAR(500),
	        @emailid INT,
	        @emailalias VARCHAR(30),
	        @CourtDate DATETIME,
	        @CourtNumber VARCHAR(6),
	        @ClientName VARCHAR(50),
	        @employeeinitial VARCHAR(2),
	        @NoticeStartDay INT,
	        @NoticeEndDay INT
	
	DECLARE @body VARCHAR(1000) -- Rab Nawaz 10914 06/26/2013 increased the length of SMS text. . . 
     
	DECLARE @emailpos INT      
	SET @name = ' '      
	SET @email = ' '      
	SELECT @name = NAME + ',' + @name,
		   -- tahir 5434 01/26/2008 fixed the sp_send_dbmail issue...
		   --@email = Email + ', ' + @email,
	       @email = Email + '; ' + @email,
	       @Notice = notice,
	       @NoticeStartDay = NoticeStartDay,
	       @NoticeEndDay = NoticeEndDay
	FROM   emailsettings
	WHERE  defaultflag = 1      

	-- tahir 6514 09/02/2009 
	IF @NoticeStartDay != 0
		SET @NoticeStartDay = -1 * @NoticeStartDay
		
	IF @NoticeEndDay != 0 
		SET @NoticeEndDay = -1 * @NoticeEndDay 
		
	
	
	SET @name = LTRIM(@name)      
	SET @email = LTRIM(@email)      
	SET @email = LEFT(@email, LEN(@email) -1)      
	
	
	SELECT 
		    --Sabir Khan 6478 08/28/2009 Need to send ticket number and also (cause number if exists)...
	        @ticketnumber = CASE 
							WHEN LEN(ISNULL(v.RefCaseNumber,'')) = 0 
							THEN 'N/A'
							ELSE
								v.RefCaseNumber
						    END,	
	
			@causenumber =  CASE 
	                            WHEN LEN(ISNULL(v.casenumassignedbycourt, '')) = 0 
	                            THEN ''
	                            ELSE v.casenumassignedbycourt
	                       END,
	       @firstname = T.firstname,
	       @lastname = T.lastname,
	       @emailalias = dbo.Fn_formatinfoforemailalias(courtdatemain, officertype, categoryid),
	       @body = dbo.Fn_formatinfoforbody(courtdatemain, courtnumbermain),
	       @CourtDate = courtdatemain,
	       @CourtNumber = courtnumbermain
	FROM   dbo.tblTickets T
	       LEFT OUTER JOIN dbo.tblOfficer O
	            ON  T.OfficerNumber = O.OfficerNumber_PK
	       INNER JOIN tblticketsViolations V
	            ON  T.ticketid_pk = V.ticketid_pk
	       INNER JOIN tblcourtviolationStatus S
	            ON  V.courtviolationstatusidmain = S.courtviolationstatusid
	WHERE  activeflag = 1
	       AND S.categoryid IN (2, 5)
	       AND (
	               
	             DATEDIFF(DAY, courtdatemain, GETDATE()) BETWEEN @NoticeEndDay AND @NoticeStartDay
	             -- tahir 6514 09/02/2009 no need to hard code..
	             --OR (DATEDIFF(DAY, courtdatemain, GETDATE()) = 0)
	           )
	       AND T.ticketid_pk = @ticketid
	       AND courtid IN (3001, 3002, 3003, 3075)--Fahad 10378 01/11/2013 HMC-W Court added to send sms       
	
	
	IF @@rowcount > 0
	BEGIN
	    DECLARE @violationcount VARCHAR(10)      
	    
	    
	    SELECT @violationcount = CASE 
	                                  WHEN COUNT(TicketId_pk) = 1 THEN CONVERT(CHAR, COUNT(TicketId_pk))
	                                  ELSE '+' + CONVERT(VARCHAR(10), (COUNT(TicketId_pk) -1))
	                             END
	    FROM   tblticketsviolations
	    WHERE  TicketID_Pk = @ticketid      
	    
	    
	    SELECT TOP 1 @employeeinitial = abbreviation
	    FROM   tblticketspayment T,
	           tblusers E
	    WHERE  T.employeeid = E.employeeid
	           AND ticketid = @ticketid
	    ORDER BY
	           T.recdate 
	    
	    --CHANGED BY TAHIR AHMED DT: 01/18/07 AS PER ANDREW'S REQUEST.......
	    --set @subject = @emailalias + '-' + @ticketnumber+ +' ('+RTrim(@violationcount)  + ') '+left(@lastname,10) + ',' + left(@firstname,5) +
	    -- ' ct#'+ @CourtNumber + '@' + substring(convert(varchar,@CourtDate,100), patindex('%:%', convert(varchar,@CourtDate,100)) - 2, 7) + '/' + @employeeinitial
	    --set @subject = @emailalias + '-' + @ticketnumber +' ('+RTrim(@violationcount)  + ')'
	    --set @body = left(@lastname,10) + ',' + left(@firstname,5) + ' ' + @body + '/' + @employeeinitial       
	    --Sabir Khan 6478 08/28/2009 
	    SET @subject = @emailalias       
	    SET @body = @ticketnumber + ' ' + @causenumber +  ' (' + RTRIM(@violationcount) + ') ' + LEFT(@lastname, 10) 
	        + ',' + LEFT(@firstname, 5) + ' ' + @body + '/' + @employeeinitial       
	    
	    
	    
	    IF @violationcount IS NOT NULL
	    BEGIN
	        INSERT INTO tblemailhistory
	          (
	            emailto,
	            client,
	            SUBJECT,
	            sentinfo,
	            TicketID,
	            CourtDate,
	            CourtNumber,
	            resendby
	          )
	        SELECT @email,
	               @emailalias,
	               @subject,
	               @body,
	               @ticketid,
	               @CourtDate,
	               @CourtNumber,
	               @Resendby
	        
	        SELECT @emailid = @@identity      
	        DECLARE @rc INT 
	        
	        EXEC msdb.dbo.sp_send_dbmail 
	             @profile_name = 'admin',
	             @recipients = @email,
	             @subject = @subject,
	             @body = @body,
	             @body_format = 'TEXT' , -- TAHIR 05/13/2009 DO NOT SEND IN HTML FORMAT....
	             -- tahir 5434 01/26/2008 fixed the sp_send_dbmail issue...
	             @mailitem_id = @rc OUTPUT   
	        
	        IF ISNULL(@rc,0) <> 0
	        BEGIN
	            UPDATE tblemailhistory
	            SET    sentflag = 1
	            WHERE  id = @emailid
	        END
	    END
	END

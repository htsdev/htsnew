USE TrafficTickets
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
 
/****** Object:  StoredProcedure [dbo].[Usp_Htp_insertintofaxletter]    Script Date: 05/22/2008 14:21:43 ******/
/*  
Created By : Noufil Khan  
Business Logic : This procedure insert records in fax Letter Table
Column : faxletterid

*/

-- Agha Usman 08/13/2008 4491  
ALTER PROCEDURE [dbo].[Usp_Htp_insertintofaxletter_Faxmaker]
	@ticketid INT = 22220,
	@empid INT = 3991,
	@ename VARCHAR(200),
	@eemail VARCHAR(200),
	@faxno VARCHAR(50),
	@subject VARCHAR(200),
	@body VARCHAR(500),
	@status VARCHAR(500),
	@faxdate DATETIME,
	@ConfirmNote VARCHAR(1000),
	@DeclineNote VARCHAR(1000),
	@SystemId INT,
	@SubjectNote VARCHAR(1000),
	@Docpath VARCHAR(200),
	@output INT = 0 OUTPUT
AS
	IF NOT EXISTS 
	   (
	       SELECT *
	       FROM   faxletter
	       WHERE  Ticketid = @ticketid
	              AND Employeeid = @empid
	              AND EmployeeName = @ename
	              AND EmployeeEmail = @eemail
	              AND FaxNumber = @faxno
	              AND SUBJECT = @subject
	              AND Body = @body
	              AND STATUS = @status
	              AND FaxDate = @faxdate
	              AND docpath = @Docpath
	   )
	BEGIN
	    --------------------------------------------------------------------------------------------------------------------
	    --Sabir Khan 8899 7/14/2011 Replace the repeated code in function for Getting spacefic date for PMC court location...
	    DECLARE @selectedCourtDate DATETIME	
	    SET @selectedCourtDate = TrafficTickets.[dbo].[fn_GetRepDateForPMC](@ticketid) 
	    -----------------------------------------------------------------------------------------------------------------------	
	    
	    INSERT INTO faxletter
	      (
	        Ticketid,
	        Employeeid,
	        EmployeeName,
	        EmployeeEmail,
	        FaxNumber,
	        SUBJECT,
	        Body,
	        STATUS,
	        FaxDate,
	        ConfirmNote,
	        DeclineNote,
	        SystemId,
	        DocPath,
	        PMCRepDate
	      )
	    VALUES
	      (
	        @ticketid,
	        @empid,
	        @ename,
	        @eemail,
	        @faxno,
	        @subject,
	        @body,
	        @status,
	        @faxdate,
	        @ConfirmNote,
	        @DeclineNote,
	        @SystemId,
	        @Docpath,
	        @selectedCourtDate
	      )  
	    
	    SET @output = SCOPE_IDENTITY()
	    
	    DECLARE @faxnumber VARCHAR(MAX)
	    DECLARE @court VARCHAR(MAX)
	    DECLARE @dbName VARCHAR(20)
	    
	    SELECT @dbName = dbName
	    FROM   SYSTEM
	    WHERE  SystemId = @SystemId
	    
	    --set @faxnumber= (select top 1 fax from tblcourts inner join tblTicketsViolations tv On tblcourts.Courtid = tv.CourtID where tv.ticketid_pk=@ticketid and fax is not null )
	    --set @court=(select top 1 shortname from tblcourts inner join tblTicketsViolations tv On tblcourts.Courtid = tv.CourtID where tv.ticketid_pk=@ticketid and shortname is not null)
	    --set @output=(select max(faxletterid)from faxletter where ticketid=@ticketid)
	    --select @Subjectnote='Letter of Rep : Ref# ' + convert(varchar(20),@output) + '- Sent to fax Number : ' + traffictickets.dbo.formatphonenumbers(@faxnumber) + ' For CourtHouse '+ @court +' - Waiting confirmation'
	    SET @SubjectNote = REPLACE(@SubjectNote, '{key}', @output)
	    SET @SubjectNote = REPLACE(@SubjectNote, '{number}', @faxno)
	    
	    EXEC (
	             'insert into ' + @dbName + 
	             '.dbo.tblticketsnotes (TicketID,Subject,EmployeeID,Notes) values (' + @ticketid + ',''' + @Subjectnote + ''',' + @empid + ','''')'
	         )
	    
	    -- Zeeshan Haider 10699 5/13/2013 Change case status to WAIT when Fax LOR is sent.
	    -- Zeeshan Haider 11306 08/12/2013 PMC Jury-Trial Cases(Do not modify case setting for sending LOR)
	    IF EXISTS (
	           SELECT ttv.TicketID_PK
	           FROM   tblTicketsViolations ttv
	           WHERE  ttv.CourtID = 3004
	                  AND ttv.CourtViolationStatusIDmain <> 26
	                  AND ttv.TicketID_PK = @ticketid
	       )
	    BEGIN
	        EXEC [dbo].[usp_hts_update_violationstatus] @ticketid,@empid
	    END
	END
GO

GRANT EXECUTE ON [dbo].[Usp_Htp_insertintofaxletter_Faxmaker] TO dbr_webuser
GO



/****** 
Create By:		Faique Ali 10882
create date:	03/07/2013
Business Logic:	This procedure is used by HTP application in HTP/Backroom that allows user to Fetch 
				records of those client who missed their court.   
				
Parameters:		N/A.
*******/

USE TrafficTickets
GO

CREATE PROCEDURE USP_HTP_MissedClient_Email_List
--Main Query 
AS BEGIN
SELECT DISTINCT t.TicketID_PK, t.Firstname, t.Lastname, vs.[Description]  , ISNULL(convert(varchar(30), v.CourtDateMain,101), '01/01/1900') AS CourtDate, 
	ISNULL(t.Address1, NULL) AS Address1,
	ISNULL(t.Address2, NULL) AS Address2, t.City, ts.[State], t.Zip, t.Email, ISNULL(t.Contact1, NULL) AS [Contact No 1],
	ISNULL(t.Contact2, NULL) AS [Contact No 2], ISNULL(t.Contact3, NULL) AS [Contact No 3], ISNULL(t.LanguageSpeak, NULL) AS [Language],
	CASE WHEN v.casenumassignedbycourt IS NULL OR v.casenumassignedbycourt = '' THEN 'N/A' ELSE v.casenumassignedbycourt END AS [Case No 1], 
	CASE WHEN v.RefCaseNumber IS NULL OR v.RefCaseNumber = '' THEN 'N/A' ELSE v.RefCaseNumber END AS [Ticket No 1],   
	CASE WHEN tv.[Description] IS NULL OR tv.[Description] = '' THEN 'N/A' ELSE tv.[Description] END AS [Violation No 1]
	INTO #temp
  FROM tbltickets t INNER JOIN tblticketsviolations v ON t.TicketID_PK = v.TicketID_PK
	INNER JOIN tblCourtViolationStatus vs ON vs.CourtViolationStatusID = v.CourtViolationStatusIDmain
	INNER JOIN tblViolations tv ON tv.ViolationNumber_PK = v.ViolationNumber_PK
	INNER JOIN tblState ts ON ts.StateID = t.Stateid_FK
	INNER JOIN tblTicketsPayment ttp ON ttp.TicketID = t.TicketID_PK
WHERE ISNULL(t.Activeflag, 0) = 1 -- Only Clients 
AND ISNULL(v.CourtViolationStatusIDmain, 0) = 105 -- Missed Court Status
AND ISNULL(t.Email, '') <> ''
AND ISNULL(t.CoveringFirmID, 0) = 3000 -- Only Sullo and Sullo Firm
AND ISNULL(t.Firstname, 'Test') NOT LIKE 'Test'
AND ISNULL(t.Lastname, 'Test') NOT LIKE 'Test'
AND ISDATE(v.CourtDateMain) = 1
AND ISNULL(ttp.PaymentVoid, 0) <> 1
AND ttp.PaymentType NOT IN (0, 8, 10, 98, 99)
ORDER BY t.TicketID_PK

ALTER TABLE #temp
ADD [Case No 2] VARCHAR(30), [Case No 3] VARCHAR(30), [Case No 4] VARCHAR(30), [Ticket No 2] VARCHAR(30), [Ticket No 3] VARCHAR(30), 
[Ticket No 4] VARCHAR(30),[Violation No 2] VARCHAR(300), [Violation No 3] VARCHAR(300), [Violation No 4] VARCHAR(300) 

SELECT *, ROW_NUMBER () OVER ( ORDER BY ticketID_PK)  AS RowNUM  INTO #formatting FROM #temp ORDER BY ticketID_PK


SELECT *, ROW_NUMBER () OVER ( PARTITION BY ticketid_pk ORDER BY ticketid_pk) AS [PARTITION] INTO #partition FROM #formatting


SELECT Distinct ticketid_pk INTO #singleVio FROM #partition GROUP BY ticketid_pk HAVING COUNT(ticketid_pk) = 1

UPDATE prt
SET prt.[Violation No 2] = p.[Violation No 1], prt.[Case No 2] = p.[Case No 1], prt.[Ticket No 2] = p.[Ticket No 1]
FROM #partition p INNER JOIN  #partition u ON p.ticketid_pk = u.ticketid_pk INNER JOIN #partition prt ON prt.ticketid_pk = u.ticketid_pk
WHERE p.[Violation No 1] <> u.[Violation No 1]
AND u.[PARTITION] = 1 AND p.[PARTITION] = 2
AND u.ticketid_pk NOT IN (SELECT * FROM #singleVio)


SELECT Distinct ticketid_pk INTO #ThreeVio FROM #partition GROUP BY ticketid_pk HAVING COUNT(ticketid_pk) = 2 OR COUNT(ticketid_pk) = 1 

UPDATE prt
SET prt.[Violation No 3] = p.[Violation No 1], prt.[Case No 3] = p.[Case No 1], prt.[Ticket No 3] = p.[Ticket No 1]
 
FROM #partition p INNER JOIN  #partition u ON p.ticketid_pk = u.ticketid_pk INNER JOIN #partition prt ON prt.ticketid_pk = u.ticketid_pk
WHERE p.[Violation No 1] <> u.[Violation No 1]
AND u.[PARTITION] = 1 AND p.[PARTITION] = 3
AND u.ticketid_pk NOT IN (SELECT * FROM #ThreeVio)

SELECT Distinct ticketid_pk INTO #FourVio FROM #partition GROUP BY ticketid_pk HAVING COUNT(ticketid_pk) = 3 OR  COUNT(ticketid_pk) = 2 OR COUNT(ticketid_pk) = 1 




UPDATE prt
SET prt.[Violation No 4] = p.[Violation No 1], prt.[Case No 4] = p.[Case No 1], prt.[Ticket No 4] = p.[Ticket No 1]

FROM #partition p INNER JOIN  #partition u ON p.ticketid_pk = u.ticketid_pk INNER JOIN #partition prt ON prt.ticketid_pk = u.ticketid_pk
WHERE p.[Violation No 1] <> u.[Violation No 1]
AND u.[PARTITION] = 1 AND p.[PARTITION] = 4
AND u.ticketid_pk NOT IN (SELECT * FROM #FourVio)


SELECT MIN(RowNUM) AS ID INTO #DeleteDuplicate FROM #partition GROUP BY ticketid_pk

--remove duplicated profiles..
DELETE FROM #partition WHERE RowNUM NOT IN (SELECT ID FROM #DeleteDuplicate)

select rowNUM into #deleteMultipuleProfiles from #partition 
where rowNUM not in (
select MIN(RowNUM) from #partition
group by Firstname, Lastname, Address1, Email, city, state, zip) 

delete from #partition where rownum in (select * from #deleteMultipuleProfiles)

--display desierd ressult
 SELECT distinct Firstname, Lastname, [Description] as [Case Status], CourtDate, 
	Address1, 
	CASE WHEN Address2 IS NULL OR address2 = '' THEN 'N/A' ELSE Address2 END AS Address2, City, [State], Zip, Email, [Contact No 1],
	CASE WHEN [Contact No 2] IS NULL OR [Contact No 2] = '' THEN 'N/A' ELSE [Contact No 2] END AS [Contact No 2], 
	CASE WHEN [Contact No 3] IS NULL OR [Contact No 3] = '' THEN 'N/A' ELSE [Contact No 3] END AS [Contact No 3], 
	[Language],	
	CASE WHEN [Case No 1] IS NULL OR [Case No 1] = '' THEN 'N/A' ELSE [Case No 1] END AS [Case No 1],	
	CASE WHEN [Case No 2] IS NULL OR [Case No 2] = '' THEN 'N/A' ELSE [Case No 2] END AS [Case No 2],	
	CASE WHEN [Case No 3] IS NULL OR [Case No 3] = '' THEN 'N/A' ELSE [Case No 3] END AS [Case No 3],	
	CASE WHEN [Case No 4] IS NULL OR [Case No 4] = '' THEN 'N/A' ELSE [Case No 4] END AS [Case No 4], 
	CASE WHEN [Ticket No 1] IS NULL OR [Ticket No 1] = '' THEN 'N/A' ELSE [Ticket No 1] END AS [Ticket No 1],
	CASE WHEN [Ticket No 2] IS NULL OR [Ticket No 2] = '' THEN 'N/A' ELSE [Ticket No 2] END AS [Ticket No 2],
	CASE WHEN [Ticket No 3] IS NULL OR [Ticket No 3] = '' THEN 'N/A' ELSE [Ticket No 3] END AS [Ticket No 3],
	CASE WHEN [Ticket No 4] IS NULL OR [Ticket No 4] = '' THEN 'N/A' ELSE [Ticket No 4] END AS [Ticket No 4],
	CASE WHEN [Violation No 1] IS NULL OR [Violation No 1] = '' THEN 'N/A' ELSE [Violation No 1] END AS [Violation No 1],
	CASE WHEN [Violation No 2] IS NULL OR [Violation No 2] = '' THEN 'N/A' ELSE [Violation No 2] END AS [Violation No 2],
	CASE WHEN [Violation No 3] IS NULL OR [Violation No 3] = '' THEN 'N/A' ELSE [Violation No 3] END AS [Violation No 3],
	CASE WHEN [Violation No 4] IS NULL OR [Violation No 4] = '' THEN 'N/A' ELSE [Violation No 4] END AS [Violation No 4]

FROM #partition
 ORDER BY firstname, lastname


drop table  #temp
drop table #formatting
DROP TABLE #partition
DROP TABLE #ThreeVio
DROP TABLE #singleVio
DROP TABLE #FourVio
DROP TABLE #DeleteDuplicate
drop table #deleteMultipuleProfiles

END

GO
GRANT EXECUTE ON [dbo].[USP_HTP_MissedClient_Email_List] TO dbr_webuser
GO



 
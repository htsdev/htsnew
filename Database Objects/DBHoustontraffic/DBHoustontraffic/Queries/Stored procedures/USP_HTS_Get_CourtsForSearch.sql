USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTS_Get_CourtsForSearch]    Script Date: 03/08/2008 16:15:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_HTS_Get_CourtsForSearch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_HTS_Get_CourtsForSearch]

go

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go



create procedure [dbo].[USP_HTS_Get_CourtsForSearch]


as



select courtid, shortcourtname as courtname from tblcourts where courtid != 0  and inactiveflag=0
order by courtid,courtname


go


go

GRANT EXECUTE ON [dbo].[USP_HTS_Get_CourtsForSearch] TO [dbr_webuser]


go
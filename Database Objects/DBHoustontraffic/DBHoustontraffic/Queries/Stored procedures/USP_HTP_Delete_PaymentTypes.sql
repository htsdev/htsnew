/*  
Created By : Abbas Qamar
Task Id    : 9957 
Creation Date : 04-01-2011
Business Logic : This procedure Delete the payment type if not record associated with payment type
Parameter:
			@Paymenttype_PK : ID of payment type
			

*/



CREATE PROCEDURE [dbo].[USP_HTP_Delete_PaymentTypes]  

@Paymenttype_PK int


AS		

BEGIN

DECLARE @PaymentId int
Set @PaymentId =  (SELECT COUNT(PaymentType) FROM tblTicketsPayment ttp WHERE ttp.PaymentType=@Paymenttype_PK)

IF(@PaymentId <= 0)
BEGIN


DELETE from tblPaymenttype 
WHERE Paymenttype_PK=@Paymenttype_PK
END


END

select @@ROWCOUNT


GO
GRANT EXECUTE ON [dbo].[USP_HTP_Delete_PaymentTypes] TO dbr_webuser
GO 

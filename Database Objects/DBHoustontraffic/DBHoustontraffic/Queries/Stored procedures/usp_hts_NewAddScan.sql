/****************  
Alter by: Syed Muhammad Ozair  
Date:  08/04/2009  
  
Business Logic:  
 The stored procedure is used by Houtson Traffic Program to insert document in docstorage against a case    
  
Input Parameters:  
 @updated   
 @extension   
 @Description   
 @DocType  
 @SubDocTypeID   
 @Employee  
 @TicketID  
 @Count  
 @Book   
 @Events   
 @FromAutoFax  
 @UseDescriptionFormat  
 @BookID   
  
  
Output Columns:  
 @BookID  
  
***************/      
-- Kazim 3980 05/27/2008 Correct the Scan Document Hyperlink      
-- Ozair 3302 on 02/29/2008 [Typo error: Scannedddd changed to Scanned]          
-- [usp_hts_NewAddScan] '2/27/2008 3:37:49 PM','TXT','(Uploaded File) TESTTESTESTESTESTTESTTEST','Continuance',0,3991,3872,1,0,'Upload',''      
ALTER PROCEDURE [dbo].[usp_hts_NewAddScan]  
 @updated DATETIME,  
 @extension NVARCHAR(4),  
  --ozair 5864 08/04/2009 length increased  
 @Description VARCHAR(max),  -- kashif jawed 9876 11/29/2011 change length from 250 to max  
 @DocType NVARCHAR(50),  
 @SubDocTypeID INT,  
 @Employee INT,  
 @TicketID INT,  
 @Count INT,  
 @Book INT,  
 @Events VARCHAR(50),   
  --Ozair 5665 03/27/2009 optional parameter for Autofax Service  
 @FromAutoFax BIT = 0,  
  --Ozair 7673 04/14/2010 added parameter for whether to use Description Format or not  
  --By default it is true  
 @UseDescriptionFormat BIT = 1,  
 @BookID NVARCHAR(50) OUTPUT  
AS  
 DECLARE @docID  INT,  
         @picID  NVARCHAR(10)                          
   
 SELECT @BookID = @Book                          
   
 IF (@Book = 0)  
 BEGIN  
     SELECT @docID = (SELECT DocTypeID FROM tblDocType WHERE  LOWER(DocType) = LOWER(@DocType)) --Get Document id of a given document type  
       
     INSERT INTO tblScanBook  
       (  
         EmployeeID,  
         TicketID,  
         DocTypeID,  
         SubDocTypeID,  
         updatedatetime,  
         DocCount,  
         ResetDesc,  
         Events  
       )  
     VALUES  
       (  
         @employee,  
         @TicketID,  
         @docID,  
         @SubDocTypeID,  
         NULL,  
         0,  
         @Description,  
         @Events  
       ) -- insert into book                          
       
     SELECT @BookID = SCOPE_IDENTITY() --Get Book Id of the latest inserted  
       
     -- Noufil Khan 8425 10/26/2010 Check if client is of court 'pasedena'   
  --        and inserted document is 'Lor Confirmation' and request is from Auto Fax Service
     IF EXISTS (SELECT ttv.TicketID_PK FROM tblTicketsViolations ttv WHERE ttv.CourtID =3004   
      AND ttv.TicketID_PK = @TicketID AND @docID = 18 AND @FromAutoFax = 1
      -- Zeeshan Haider 10699 05/15/2013 If the case already has a "Pre-Trial" or any other status than "WAITING" status, then system shouldn't change date or status
      AND ttv.CourtViolationStatusIDmain = 104)
		  BEGIN  
		   -- Noufil Khan 8425 10/26/2010 Update court date to Lor Court date and set court status to 'Pre-Trial'  
		   UPDATE ttv   
		   SET  ttv.CourtDateMain = tlh.CourtDate, ttv.CourtViolationStatusIDmain = 101  
		   FROM tblTicketsViolations ttv  
			 INNER JOIN tblLORBatchHistory tlh ON ttv.TicketID_PK = tlh.TicketID_PK  
		   WHERE   ttv.TicketID_PK = @TicketID
		   --Zeeshan Haider 11306 08/12/2013 PMC Jury-Trial Cases(Do not modify case setting for sending LOR)
		   AND ttv.CourtViolationStatusIDmain <> 26
		  END
       
     --ozair 5864 08/04/2009 length increased  
     DECLARE @temp_description  VARCHAR(150)               
     DECLARE @typeofdoc         VARCHAR(50)                      
     DECLARE @rectype           VARCHAR(1)                      
       
       
     SELECT @temp_description = @Description              
       
     SELECT @temp_description = CASE   
                                     WHEN @temp_description <> '' THEN '[' + @temp_description + ']'  
                                     ELSE @temp_description  
                                END               
       
     SELECT @typeofdoc = doctype  
     FROM   tbldoctype  
     WHERE  doctypeid = @docID  
       
     IF @SubDocTypeID <> 0  
         SET @typeofdoc = @typeofdoc + ' - ' + (  
                 SELECT subdoctype  
                 FROM   tblsubdoctype  
                 WHERE  subdoctypeid = @SubDocTypeID  
             )  
       
     --Ozair 7673 04/14/2010 setting Subject based on @UseDescriptionFormat parameter  
     DECLARE @Subject VARCHAR(1000)  
     IF @UseDescriptionFormat = 1  AND @Events = 'Outlook Save' -- Kashif Jawed 9876 03/05/2012 change Note History description for outlook email uploading
         SET @Subject = 'Upload Document - ' + @typeofdoc + ' ' + @temp_description  
     ELSE IF @UseDescriptionFormat = 1 
         SET @Subject = 'Scanned Document - ' + @typeofdoc + ' ' + @temp_description  
     ELSE  
         SET @Subject = @Description  
           
     SET @rectype = '1'   
     -- Sarim 5419 1/26/2009 For MSG File.  
     -- Ozair 5665 03/27/2009 note should be inserted for LOR Confirmation report from documents page
     IF @FromAutoFax = 0  
     BEGIN  
      -- Abbas Qamar 9195 11/1/2011   
      if @docID <> 21 -- NO need to add bad email link in case history  
         INSERT INTO tblTicketsNotes  
           (  
             TicketID,  
             SUBJECT,  
             EmployeeID  
           )  
         VALUES  
           (  
             @TicketID,  
             --Ozair 7673 04/14/2010 Used @subject variable  
             '<a href="javascript:window.open(''../paperless/PreviewMain.aspx?DocID=' + @BookID + '&RecType=' + @rectype + '&DocNum=0&DocExt=' + @extension + ''','''',''height=505,width=600,resizable=yes,status=no,toolbar=no,scrollbars=yes,menubar=no,location=no'');void('''');"''>' + @Subject + '</a>',  
             @employee  
           )   
         --updating edelivery flag for LOR certified mail confirmation report  
         IF @docID = 18  
         BEGIN  
             UPDATE tblhtsbatchprintletter  
             SET    EDeliveryFlag = 1,  
                    USPSConfirmDate = GETDATE()  
             WHERE  IsPrinted = 1  
                    AND DeleteFlag <> 1  
                    AND EDeliveryFlag = 0  
                    AND LetterID_FK = 6  
                    AND TicketID_FK = @TicketID  
         END  
     END  
 END                          
   
 UPDATE tblScanBook  
 SET    updatedatetime = GETDATE(),  
        DocCount = @Count  
 WHERE  ScanBookID = @BookID -- Update the book table                          
   
 INSERT INTO tblScanPIC  
   (  
     ScanBookID,  
     updatedatetime,  
     DocExtension  
   )  
 VALUES  
   (  
     @BookID,  
     @updated,  
     @extension  
   ) --insert new image into database                          
   
 SELECT @picID = SCOPE_IDENTITY() --Get Book Id of the latest inserted  
   
 --------------------------------------------------------------------------khalid-----------        
 IF (@SubDocTypeID = 20)--15 ALR Request Confirmation  
 BEGIN       
     --parameters required for inserting flag        
     EXEC USP_HTS_Insert_flag_by_ticketnumber_and_flagID @TicketID,  
          27,  
          @employee,  
          0,  
          0,  
          0  
 END   
 --------------------------------------------------------------------------------------khalid--------                          
 SELECT @BookID = @BookID + '-' + @picID   
 RETURN   
 ----------------------------------------------------------------------------------------------          

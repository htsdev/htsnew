﻿/******   
Waqas 5864 06/30/2009
Business Logic : This report display all cases that we have not been sent a case summary to their attorneys.
All Family and criminal case except alr violations will be displayed.    
  
Columns:  
  
 ticketid_pk,  
 LastName,  
 FirstName,  
 Cause Number
 CourtDate,  
 CourtTime,  
 CourtRoom
  
******/  
  
  
ALTER PROCEDURE [dbo].[USP_HTP_Get_CaseEmailSummary]
AS
	SELECT DISTINCT 
	       t.ticketid_pk AS ticketid_pk,
	       ISNULL(tv.casenumassignedbycourt, '') AS CauseNumber,
	       t.Lastname AS LastName,
	       t.firstname AS FirstName,
	       CONVERT(VARCHAR(20), tv.CourtDateMain, 101) AS CourtDate,
	       dbo.Fn_FormateTime(tv.CourtDateMain) AS CourtTime,
	       ISNULL(tv.CourtNumbermain, '0') AS CourtRoom
	FROM   tblTickets t
	       INNER JOIN tblTicketsViolations tv
	            ON  t.ticketid_pk = tv.ticketid_pk
	       INNER JOIN tblcourtviolationstatus cvs
	            ON  tv.courtviolationstatusidmain = cvs.CourtViolationStatusID
	WHERE  --Ozair 7791 07/24/2010  where clause optimized
	       --Waqas 6342 08/24/2009 For criminal and family cases now
	       tv.violationnumber_pk <> 16159
	       --AND tv.CourtViolationStatusID <> 80
	       AND cvs.CategoryID <> 50
	           --and tv.courtid in (SELECT Courtid FROM tblCourts WHERE ALRProcess = 1 ) 
	           
	           -- tahir 6408 08/18/2009 only active clients...
	       AND ISNULL(t.EmailSentFlag, 0) = 0
	       AND t.CaseTypeId IN (2, 4)
	       AND ISNULL(t.Activeflag, 0) = 1
	ORDER BY
	       t.Lastname,
	       t.ticketid_pk
GO


  
  
  
   
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Mailer_Send_DallasCriminal]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Mailer_Send_DallasCriminal]
GO

  
  
  
  
-- usp_Mailer_Send_DallasCriminal 16, 37, 16, '10/25/07,' , 3992, 0,0 ,0  
        
CREATE proc [dbo].[usp_Mailer_Send_DallasCriminal]  
@catnum int=1,                                      
@LetterType int=9,                                      
@crtid int=1,                                      
@startListdate varchar (500) ='01/04/2006',                                      
@empid int=2006,                                
@printtype int =0 ,  
@searchtype int    ,  
@isprinted  bit                               
                                      
                                      
as                                      
                                            
declare @officernum varchar(50),                                      
@officeropr varchar(50),                                      
@tikcetnumberopr varchar(50),                                      
@ticket_no varchar(50),                                                                      
@zipcode varchar(50),                                               
@zipcodeopr varchar(50),                                      
                                      
@fineamount money,                                              
@fineamountOpr varchar(50) ,                                              
@fineamountRelop varchar(50),                     
                
@singleviolation money,                                              
@singlevoilationOpr varchar(50) ,                                              
@singleviolationrelop varchar(50),                                      
                                      
@doubleviolation money,                                              
@doublevoilationOpr varchar(50) ,                                              
@doubleviolationrelop varchar(50),                                    
@count_Letters int,                    
@violadesc varchar(500),                    
@violaop varchar(50)                                       
                                      
declare @sqlquery varchar(8000),  
 @sqlquery2 varchar(8000)  
                                              
Select @officernum=officernum,                                      
@officeropr=oficernumOpr,                                      
@tikcetnumberopr=tikcetnumberopr,                                      
@ticket_no=ticket_no,                                                                        
@zipcode=zipcode,                                      
@zipcodeopr=zipcodeLikeopr,                                      
@singleviolation =singleviolation,                                      
@singlevoilationOpr =singlevoilationOpr,                                      
@singleviolationrelop =singleviolationrelop,                                      
@doubleviolation = doubleviolation,                                      
@doublevoilationOpr=doubleviolationOpr,                                              
@doubleviolationrelop=doubleviolationrelop,                    
@violadesc=violationdescription,                    
@violaop=violationdescriptionOpr ,                
@fineamount=fineamount ,                                              
@fineamountOpr=fineamountOpr ,                                              
@fineamountRelop=fineamountRelop                
                                   
from tblmailer_letters_to_sendfilters                                      
where courtcategorynum=@catnum                                      
and Lettertype=@LetterType                                    
  
Select @sqlquery =''  , @sqlquery2 = ''  
  
declare @user varchar(10)  
select @user = upper(abbreviation) from tblusers where employeeid = @empid  
                                      
set @sqlquery=@sqlquery+'                                      
Select distinct tva.recordid as recordid,   
 ta.midnumber as span,   
 tva.ticketnumber_pk as casenumber ,   
 0 as clientflag,  
 left(ta.zipcode,5) + rtrim(ltrim(ta.midnumber)) as zipspan,  
 convert(varchar(10),ta.recloaddate,101) as listdate,   
 flag1 = isnull(ta.Flag1,''N'') ,  
 0 as donotmailflag,  
 upper(ta.firstname + '' ''+ ta.lastname) as name,  
 ta.address1 + isnull('' '' + ta.address2,'''') as address,  
 ta.city,  
 isnull(s.state,''TX'') as state,  
 ta.zipcode,  
 dp2 as dptwo,  
 dpc  
into #temp                                            
FROM    dallastraffictickets.dbo.tblticketsarchive ta inner join 
	dallastraffictickets.dbo.tblticketsviolationsarchive tva
on tva.recordid = ta.recordid
left outer join dallastraffictickets.dbo.tblstate s
on s.stateid = ta.stateid_fk
where Flag1 in (''Y'',''D'',''S'') 
and isnull(ta.ncoa48flag,0) = 0  
and   isnull(ta.donotmailflag,0) = 0
'  
  
if @catnum = @crtid
	set @sqlquery=@sqlquery + ' and tva.courtlocation in (select courtid from dallastraffictickets.dbo.tblcourts where courtcategorynum = '+convert(varchar,@catnum )+')
	'
else
	set @sqlquery=@sqlquery + ' and tva.courtlocation = '+convert(Varchar,@crtid)

  
set @sqlquery=@sqlquery+'  
and   ' + dbo.Get_String_Concat_With_DatePart_ver2 ( ''+  @startlistdate + '', 'ta.recloaddate' )   
  
set @sqlquery =@sqlquery+ '   
and ta.recordid not in (                                                                      
 select n.recordid from tblletternotes n , dallastraffictickets.dbo.tblticketsarchive t
 where  lettertype ='+convert(varchar(10),@lettertype)+'  and t.recordid = n.recordid
 )'  
  
                                      
set @sqlquery=@sqlquery+'  
  
Select distinct recordid,  span,      casenumber, clientflag,  
 zipspan, listdate as mdate,Name,address,                                
 city,state,dpc,dptwo,Flag1,donotmailflag,  zipcode into #temp1  from #temp                                      
 '                
           
if( @printtype<>0)           
set @sqlquery2 =@sqlquery2 +                                      
'  
 Declare @ListdateVal DateTime,                                    
  @totalrecs int,                                  
  @count int,                                  
  @p_EachLetter money,                                
  @recordid varchar(50),                                
  @zipcode   varchar(12),                                
  @maxbatch int  ,  
  @lCourtId int  ,  
  @dptwo varchar(10),  
  @dpc varchar(10)  
 declare @tempBatchIDs table (batchid int)  
 Select @totalrecs =count(*) from #temp1 where mdate = @ListdateVal                                   
 Select @count=Count(*) from #temp1                                  
  
 if @count>=500                                  
  set @p_EachLetter=convert(money,0.3)                                  
                                  
 if @count<500                                  
  set @p_EachLetter=convert(money,0.39)                                  
                                
 Declare ListdateCur Cursor for                                    
 Select distinct mdate  from #temp1                                  
 open ListdateCur                                     
 Fetch Next from ListdateCur into @ListdateVal                                                        
 while (@@Fetch_Status=0)                                
  begin                                  
   Select @totalrecs =count(distinct recordid) from #temp1 where mdate = @ListdateVal                                   
  
   insert into tblBatchLetter(Empid, Lettertype,Listdate,CourtId,ParentID,LCount,PExpense) values                                    
   ( '+convert(varchar(10), @empid)+'  , '+convert(varchar(10),@lettertype)+' , @ListdateVal, '+convert(varchar(10),@crtid)+' , 0, @totalrecs, @p_EachLetter)                                     
  
   Select @maxbatch=Max(BatchId) from tblBatchLetter    
    insert into @tempBatchIDs select @maxbatch                                                       
    Declare RecordidCur Cursor for                                 
    Select distinct recordid,zipcode, 3037, dptwo, dpc from #temp1 where mdate = @ListdateVal                                 
    open RecordidCur                                                           
    Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc                                            
    while(@@Fetch_Status=0)                                
    begin                                
     insert into tblLetterNotes(BatchId_Fk,listdate,ZipCode,RecordID,PCost,LetterType, courtid, dp2, dpc)                                
     values (@maxbatch,@ListdateVal,@zipcode,@recordid,@p_EachLetter,'+convert(varchar(10),@lettertype)+' ,@lCourtId, @dptwo, @dpc)                                
     Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc                            
    end                                
    close RecordidCur   
    deallocate RecordidCur                                                                 
   Fetch Next from ListdateCur into @ListdateVal                                
  end                                              
  
 close ListdateCur    
 deallocate ListdateCur   
  
  Select distinct convert(varchar(20),n.noteid) as letterid, clientid, Name, address, city, state, t.zipcode as zip , t.recordid, span, arrestdate,  
   chargewording, chargelevel, disposition, bookingdate, casenumber, clientflag, t.zipspan, mdate as listdate,  
   dptwo, t.dpc,  clientflag, '''+@user+''' as Abb            
 from #temp1 t , tblletternotes n, @tempBatchIDs tb  
 where t.recordid = n.recordid and  n.courtid = 3037 and n.batchid_fk = tb.batchid and n.lettertype = '+convert(varchar(10),@lettertype)+'   
  order by [ZIPSPAN]  
  
declare @lettercount int, @subject varchar(200), @body varchar(400) , @sql varchar(500)  
select @lettercount = count(distinct n.noteid) from tblletternotes n, @tempBatchIDs b where n.batchid_fk = b.batchid  
select @subject = convert(varchar(20), @lettercount) + '' LMS Dallas County Criminal Letters Printed''  
select @body = convert(varchar(20), @lettercount) + '' LETTERS ('' + CONVERt(VARCHAR(12), GETDATE()) + '') - ''+ upper( '''+@user+''')  
exec usp_mailer_send_Email @subject, @body  

declare @batchIDs_filname varchar(500)
set @batchIDs_filname = ''''
select @batchIDs_filname = @batchIDs_filname + convert(varchar,batchid) + '',''  from @tempBatchIDs
select isnull(@batchIDs_filname,'''') as batchid

'  
  
else  
 set @sqlquery2 = @sqlquery2 + '  
  Select distinct ''NOT PRINTABLE'' as letterId,  Name, address, city, state, zipcode as zip , recordid, span,   
       casenumber, clientflag,  mdate as listdate,  
   dptwo, dpc, zipspan, clientflag, '''+@user+''' as Abb  
  from #temp1   
  order by [zipspan]  
 '  
  
set @sqlquery2 = @sqlquery2 + '  
  
drop table #temp   
drop table #temp1'  
                                      
print @sqlquery + @sqlquery2  
set @sqlquery = @sqlquery + @sqlquery2  
exec (@sqlquery)  
  
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


﻿/****** 
Create by:  Muhammad Nasir
Business Logic : this procedure is used to check the court is ALR Process or not. 

List of Parameters:	
	@CourtID : Court Id

List of Columns: 	
	isfound : if found then 1 else 0

******/


Create PROCEDURE [dbo].[USP_HTP_Check_IsALRProcess ]
@CourtID INT
AS

--IF(EXISTS(SELECT * FROM tblCourts WHERE Courtid=@CourtID AND ALRProcess=1))
-- SELECT 1
--ELSE
-- select	0 


SELECT * FROM tblCourts WHERE Courtid=@CourtID AND ALRProcess=1
SELECT @@ROWCOUNT 

Go
grant exec on dbo.USP_HTP_Check_IsALRProcess to dbr_webuser
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_GET_Address_Infromation_By_MIDNUM]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_GET_Address_Infromation_By_MIDNUM]
GO


create Procedure [dbo].[USP_HTS_GET_Address_Infromation_By_MIDNUM] --'04480726'    
(    
     
 @MidNum varchar(20)    
     
)    
    
as    
    
    
SELECT DISTINCT       
T.TicketID_pk,ISNULL(T.Lastname, '') + ' ' + ISNULL(T.Firstname, '') AS ClientName,     
--------------------------- Added By Zeeshan Ahmed --------------------------------------    
ISNULL(T.Address1, '') as Address1 , ISNULL(T.Address2, '')as Address2, T.City ,TS.State  as State ,T.Zip as zipcode ,      
-----------------------------------------------------------------------------------------    
     
      
        ISNULL(T.Midnum, '') AS MidNum, 
			

	dbo.fn_FormatContactNumber_new(Convert(varchar,isnull(t.Contact1,'')),Convert(varchar,isnull(t.ContactType1,''))) as Contact1,  
	dbo.fn_FormatContactNumber_new(Convert(varchar,isnull(t.Contact2,'')),Convert(varchar,isnull(t.ContactType2,''))) as Contact2,  
	dbo.fn_FormatContactNumber_new(Convert(varchar,isnull(t.Contact3,'')),Convert(varchar,isnull(t.ContactType3,''))) as Contact3 


			     
             
    
      
FROM         dbo.tblTickets AS T      
                       
        join tblstate ts On T.StateId_fk = ts.stateid    left outer join

		dbo.tblContactstype AS tblContactstype_2 ON t.ContactType3 = tblContactstype_2.ContactType_PK LEFT OUTER JOIN                          
        dbo.tblContactstype ON t.ContactType2 = dbo.tblContactstype.ContactType_PK LEFT OUTER JOIN                          
        dbo.tblContactstype AS tblContactstype_1 ON t.ContactType1 = tblContactstype_1.ContactType_PK                 
              
    
where midnum = @MidNum    
    
ORDER BY Midnum   
  
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


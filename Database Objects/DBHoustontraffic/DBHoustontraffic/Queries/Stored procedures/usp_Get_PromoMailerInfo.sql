USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[usp_Get_PromoMailerInfo]    Script Date: 01/02/2014 02:17:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- [dbo].[usp_Get_PromoMailerInfo] '12/5/2013','12/6/2013',9
ALTER PROCEDURE [dbo].[usp_Get_PromoMailerInfo]
@startDate DATETIME,
@endDate DATETIME,
@letterType INT                               
                                    
as   

SELECT DISTINCT t.ticketid_pk, ln.promoAttorney, l.lettername,ln.recordid,isnull(t.Activeflag,3) active into #temp
FROM tblLetterNotes ln INNER JOIN tblLetter l ON l.LetterID_PK = ln.LetterType
LEFT OUTER JOIN tblTicketsViolations tv ON tv.RecordID = ln.RecordID
LEFT OUTER JOIN tbltickets t ON t.TicketID_PK = tv.TicketID_PK
WHERE ISNULL(ln.isPromo,0) = 1
AND DATEDIFF(DAY,ln.RecordLoadDate,@startDate)<=0
AND DATEDIFF(DAY,ln.RecordLoadDate,@endDate)>=0
AND ln.LetterType = @letterType
and ln.PromoID is not null
and datediff(day,ln.RecordLoadDate,'12/02/2013') <> 0

alter table #temp add Quote int, Hires int, nonClient int, revenue money

select tp.ticketid, sum(tp.ChargeAmount) as TotAmount into #tempPay
from tblticketspayment tp inner join #temp t on t.ticketid_pk = tp.ticketid
inner join tbltickets tt on tt.ticketid_pk = t.ticketid_pk and isnull(tt.Activeflag,0) = 1
where isnull(tp.PaymentVoid,0) = 0
group by tp.ticketid


update t set t.revenue = tp.TotAmount
from #temp t 
inner join #tempPay tp on tp.ticketid = t.ticketid_pk

update #temp set Quote = 1 where active = 0
update #temp set Hires = 1 where active = 1
update #temp set nonClient = 1 where active = 3
update #temp set Quote = 0 where isnull(Quote,0) = 0
update #temp set Hires = 0 where isnull(Hires,0) = 0 
update #temp set revenue = 0 where isnull(revenue,0) = 0
update #temp set nonClient = 0 where isnull(nonClient,0) = 0 

select promoAttorney, LetterName, Count(distinct recordid) MailerCount,
sum(Quote) Quote, sum(Hires) Hired, sum(nonClient) NonClient, sum(revenue)  Revenue 
into #temp1 from #temp group by promoAttorney,LetterName
order by Count(distinct recordid) desc

ALTER TABLE #temp1 ADD ID INT IDENTITY(1,1) NOT NULL

INSERT INTO #temp1(promoAttorney,LetterName,MailerCount,Quote,Hired,NonClient,Revenue)
SELECT 'Total Promotional Mailers','',SUM(MailerCount), SUM(Quote),SUM(Hired),SUM(NonClient), sum(Revenue)  FROM #temp1

SELECT promoAttorney [Attorney Name],LetterName  Mailer,MailerCount [Mailer Count],Quote,Hired,NonClient [Non Client],Convert(varchar,Round(Revenue,2)) Revenue FROM #temp1 order by ID asc

drop table #temp1
drop table #temp


alter procedure USP_HTS_delete_firm_info    
 (  
 @firmid int,  
 @Ret  int=0   Output  
 )  
as    
  
if exists ( select ticketid_pk from tbltickets where FirmID = @firmid or CoveringFirmID = @firmid )  
 set @Ret = 0  
else  
Begin  
 delete from tblfirm where firmid = @firmid     
 set @Ret = 1  
End  
  
go
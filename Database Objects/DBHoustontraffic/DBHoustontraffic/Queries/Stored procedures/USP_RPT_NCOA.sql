SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_RPT_NCOA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_RPT_NCOA]
GO

CREATE procedure [dbo].[USP_RPT_NCOA]

as

declare @tickets int

select distinct recordid into #temp from tblletternotes where lettertype = 34

select @tickets = count( distinct t.mailerid)
from tbltickets t
inner join 
	tblticketsviolations v
on t.ticketid_pk = v.ticketid_pk
inner join 
	tblletternotes n
on n.noteid = t.mailerid
and n.lettertype = 34

--select @tickets

declare  @others int
select @others = count( distinct t.mailerid)
from tbltickets t
inner join 
	tblticketsviolations v
on t.ticketid_pk = v.ticketid_pk
inner join 
	tblletternotes n
on n.noteid = t.mailerid
and n.lettertype <> 34
inner join 
	#temp a
on a.recordid = v.recordid

--select @others

select 
	convert(varchar(10), recordloaddate, 101) as [Letter Date], 
	count(noteid) as [Letter Count],
	@tickets as [Quote by NCOA Letter],
	@others as [Quote by Other Letters]
from tblletternotes where lettertype = 34
group by convert(varchar(10), recordloaddate, 101)
	
	
drop table #temp
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


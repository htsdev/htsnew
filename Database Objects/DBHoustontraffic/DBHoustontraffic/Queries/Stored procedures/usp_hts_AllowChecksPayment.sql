 /****** 
ALTER by:  Syed Muhammad Ozair for Task 3993

Business Logic : this procedure is used to check for min court date of a criminal case to allow check payments or not;
				 this is only for QUOTE CLIENT, and then checks if it is within 0(current date) to 7 days or not. 
				 if it is then returns 1 else 0. 0 means do not allow check payment. 1 means allow check payment.

List of Parameters:	
	@ticketid : ticket id of the case to check

List of Columns: 	
	allowChecks : 0 or 1; 
				  0 means do not allow check payment. 
				  1 means allow check payment.

******/

ALTER procedure dbo.usp_hts_AllowChecksPayment      
      
@ticketid int      
      
as      
      
declare @mindate datetime      
      
select @mindate=min(tv.courtdatemain)      
from tblticketsviolations tv inner join tbltickets t
on tv.ticketid_pk=t.ticketid_pk inner join tblcourtviolationstatus cvs      
on cvs.courtviolationstatusid=tv.courtviolationstatusidmain      
group by tv.ticketid_pk,cvs.categoryid,tv.courtid,t.activeflag      
having tv.ticketid_pk=@ticketid  
and tv.courtid in(3037,3036,3043)
and isnull(t.activeflag,0)=0      
      
      
if @mindate is null      
 select 1 as allowChecks      
else if datediff(day,@mindate,getdate())>0      
 select 1 as allowChecks      
else if datediff(day,@mindate,getdate())<=-7      
 select 1 as allowChecks      
else       
 select 0 as allowChecks      
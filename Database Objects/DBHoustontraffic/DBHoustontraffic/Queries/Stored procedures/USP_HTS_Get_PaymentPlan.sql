﻿/**
* Business Logic : This method returns clients payment plan information based on ticket id
**/

--	[USP_HTS_Get_PaymentPlan]  47402
ALTER PROCEDURE [dbo].[USP_HTS_Get_PaymentPlan]
	@TicketID
AS
	INT 
	AS            
	SELECT ts.scheduleid,
	       ts.PaymentDate,
	       CONVERT(VARCHAR(20), ts.Amount) AS Amount,
	       DATEDIFF(DAY, GETDATE(), ts.paymentdate) AS daysleft,
	       (
	           SELECT ISNULL(
	                      SUM(DISTINCT t.totalfeecharged) - ISNULL(SUM(p.ChargeAmount), 0),
	                      0
	                  )
	           FROM   tbltickets t
	                  INNER JOIN tblticketspayment p
	                       ON  t.ticketid_pk = p.ticketid
	                       AND t.ticketid_pk = @TicketID
	                       AND p.paymentvoid = 0
	                       AND p.paymenttype NOT IN (99, 100)
	       ) AS Owes,
	       -- Noufil 5618 04/02/2009 Payment FOllow Update column added
	       CONVERT(VARCHAR(200),tt.PaymentDueFollowUpDate,101) AS PaymentFollowUpdate
	FROM   tblschedulepayment ts
	INNER JOIN tblTickets tt ON tt.TicketID_PK = ts.TicketID_PK
	WHERE  ts.ticketid_pk = @TicketID
	       AND scheduleflag <> 0
	ORDER BY
	       paymentdate 
	       
	       

 

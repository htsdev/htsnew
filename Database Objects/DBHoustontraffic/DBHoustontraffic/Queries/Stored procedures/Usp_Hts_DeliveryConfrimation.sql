GO


/****** Object:  StoredProcedure [dbo].[Usp_Hts_DeliveryConfrimation]    Script Date: 04/30/2008 17:27:10 ******/
/******    
Create by:  Noufil khan    
Business Logic : This procedure return Trackreponse and tracking number when mail is delivered at client end as well as their delivery status

List of Parameters: 
	@status 
			-1 for all stauts
			0 for pending
			1 for delivered
******/  
  

--dbo.[Usp_Hts_DeliveryConfrimation] -1
CREATE Procedure [dbo].[Usp_Hts_DeliveryConfrimation]
@status smallint
as

SELECT     TOP (100) PERCENT           
    dbo.tblHTSBatchPrintLetter.ticketid_fk as ticketid,
    dbo.tblHTSBatchPrintLetter.BatchID_Pk,
    dbo.tbl_hts_batch_trialletter.LastName +', '+ dbo.tbl_hts_batch_trialletter.FirstName as [Name],         
    c.ShortName +' '+ isnull(cs.ShortDescription,'N/A')+' '+case when dbo.tbl_hts_batch_trialletter.CourtDate is null then '' else dbo.fn_dateformat(dbo.tbl_hts_batch_trialletter.CourtDate,29,'/',':',1)+' '+ '&'+' '+dbo.tbl_hts_batch_trialletter.CourtNumber  end as courtdate, 
	case when tblHTSBatchPrintLetter.DeliveryStatus=0 then 'Pending'else 'Delivered'end as DeliveryStatus,
    dbo.tblHTSBatchPrintLetter.USPSTrackingNumber as TrackingNumber,
	(select top 1 case when USPSResponseDelivery is not null then  USPSResponseDelivery.query('for $s in TrackResponse return $s/TrackInfo/TrackSummary') else '<TrackSummary> Not Tracked as yet </TrackSummary>' end  from tblHTSBatchPrintLetter tbp where BatchID_PK = dbo.tblHTSBatchPrintLetter.BatchID_Pk) as trackResponse
	
	

FROM dbo.tblHTSBatchPrintLetter 
		LEFT OUTER JOIN   dbo.tblUsers 
				ON dbo.tblHTSBatchPrintLetter.PrintEmpID = dbo.tblUsers.EmployeeID 
		LEFT OUTER JOIN   dbo.tblUsers AS tblUsers_1 
				ON dbo.tblHTSBatchPrintLetter.EmpID = tblUsers_1.EmployeeID 
		RIGHT OUTER JOIN  dbo.tbl_hts_batch_trialletter 
				ON dbo.tblHTSBatchPrintLetter.BatchID_PK = dbo.tbl_hts_batch_trialletter.BatchID AND           
				   dbo.tblHTSBatchPrintLetter.TicketID_FK = dbo.tbl_hts_batch_trialletter.ticketid_pk 
		Inner Join dbo.tbltickets AS tbltickets 
				on dbo.tbl_hts_batch_trialletter.ticketid_pk = tbltickets.ticketid_pk 
		Inner Join tblcourts c 
				ON dbo.tbl_hts_batch_trialletter.CourtID = c.CourtID  
		left outer join tblcourtviolationstatus cs 
				on cs.CourtViolationStatusID = tbl_hts_batch_trialletter.courtviolationstatusid and cs.categoryid = 4      
WHERE     
      
		  isnull(dbo.tblHTSBatchPrintLetter.deleteflag ,0) = 0      
	 and  isnull(dbo.tblHTSBatchPrintLetter.isprinted ,0) = 1
	 and  dbo.tblHTSBatchPrintLetter.USPSTrackingNumber is not null 
	 and  dbo.tblHTSBatchPrintLetter.USPSTrackingNumber <> '0'
	 and dbo.tblHTSBatchPrintLetter.USPSResponseDelivery is not null
	 and  (tblHTSBatchPrintLetter.DeliveryStatus=case when @status = -1 then 0 else @status end
	 or tblHTSBatchPrintLetter.DeliveryStatus=case when @status = -1 then 1 else @status end)
    
GROUP BY dbo.tblHTSBatchPrintLetter.ticketid_fk, dbo.tblHTSBatchPrintLetter.BatchID_Pk, 
		 dbo.tbl_hts_batch_trialletter.FirstName,dbo.tbl_hts_batch_trialletter.MiddleName, 
		 dbo.tbl_hts_batch_trialletter.LastName,c.ShortName, dbo.tbl_hts_batch_trialletter.CaseStatus,
	     dbo.tblUsers.Abbreviation,dbo.tbl_hts_batch_trialletter.CourtNumber,cs.ShortDescription,
		 dbo.tblHTSBatchPrintLetter.USPSTrackingNumber,dbo.tbl_hts_batch_trialletter.CourtDate,
		 tblHTSBatchPrintLetter.DeliveryStatus,dbo.tbl_hts_batch_trialletter.BatchID
             
ORDER BY [Name]


go
GRANT EXEC ON [dbo].[Usp_Hts_DeliveryConfrimation] to dbr_webuser
go 

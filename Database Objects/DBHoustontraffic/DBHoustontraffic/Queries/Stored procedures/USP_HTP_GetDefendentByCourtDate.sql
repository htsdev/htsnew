﻿/************************************************************
 * Code formatted by SoftTree SQL Assistant © v4.0.34
 * Time: 8/19/2009 5:12:18 PM
 ************************************************************/

--USP_HTP_GetDefendentByCourtDate 5126
/*        
Created By     : Fahad Muhammad Qureshi.
Created Date   : 07/20/2009  
TasK		   : 6054        
Business Logic : This procedure is used to fetching out Data for All Outside Firms.
     
*/

ALTER PROCEDURE [dbo].[USP_HTP_GetDefendentByCourtDate]
	@TicketID INT
AS
	SET NOCOUNT ON ;
	DECLARE @CourtDate DATETIME
	DECLARE @CourtID INT
	
	IF EXISTS (
	       SELECT ttv.*
	       FROM   tblTicketsViolations ttv WITH(NOLOCK)
	              INNER JOIN tblCourtViolationStatus tcvs WITH(NOLOCK)
	                   ON  ttv.CourtViolationStatusIDmain = tcvs.CourtViolationStatusID
	       WHERE  ttv.TicketID_PK = @TicketID
	              AND tcvs.CategoryID IN (3, 4, 5)
	   )
	BEGIN
	    SELECT @CourtDate = MIN(ttv.CourtDateMain),
	           @CourtID = ttv.CourtID
	    FROM   tblTicketsViolations ttv WITH(NOLOCK)
	    WHERE  ttv.TicketID_PK = @TicketID
	    GROUP BY
	           ttv.CourtID
	    
	    SELECT a.CourtDateMain
	    FROM   tblTicketsViolations a WITH(NOLOCK)
	           INNER JOIN tblCourtViolationStatus tcvs WITH(NOLOCK)
	                ON  tcvs.CourtViolationStatusID = a.CourtViolationStatusIDmain
	           INNER JOIN tblTickets tt WITH(NOLOCK)
	                ON  tt.TicketID_PK = a.TicketID_PK
	    WHERE  a.TicketID_PK <> @TicketID
	           AND a.CourtID = @CourtID
	           AND tcvs.CategoryID <> 50
	           AND tt.Activeflag = 1
	           AND DATEDIFF(DAY, a.CourtDateMain, @CourtDate) = 0
	END
	ELSE
	BEGIN
	    SELECT GETDATE()
	END


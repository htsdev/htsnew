  
      
              
/******************

CREATED BY : SARIM GHANI

BUSINESS LOGIC : This procedure is used to get cases which have discrepancies.

LIST OF INPUT PARAMETERS :

	@startdate = Start Date             
	@enddate = End Date      
	@showall = Show All Records               
	@courtlocation = Court Location

LIST OF COLUMNS :
	
	CoveringFirmID : Covering Firm Id Which Cover The Case	
	Arrest Date : Arrest Date Associated With The Violation
	CaseTypeId  : Case Type Of Case
	MissedCourtType : Violation Missed Court Type
*******************/
--Zeeshan Ahmed 3757 04/17/2007
--Add HasNOS Column in the report for Update Violation Control
--usp_discrepancyReport '3/29/2006','4/28/2009'        
Alter PROCEDURE [dbo].[usp_discrepancyReport]   --'1/1/2006','1/1/2007' ,0  ,0            
                  
@startdate as datetime,                  
@enddate as datetime,                  
@showall as int=0,                  
@courtlocation as int = 0                  
AS                  
     
               
set @enddate = @enddate + '23:59:59'                
                
if @showall  = 1                 
 select @startdate = '1/1/1900', @enddate = '12/31/2100'                
                
 select  distinct                
  TT.TicketID_PK, refcasenumber as refcasenumberseq,                  
  convert(varchar(11),TTV.courtdate,101)+'@'+replace(substring(convert(varchar(20),TTV.courtdate),13,18),'12:00AM','0:00AM')    AS courtdate,         --upload--auto                  
   convert(int, isnull(TTV.courtnumber,0)) as courtnumber,                 
  convert(varchar(11),TTV.courtdatescan,101)+'@'+replace(substring(convert(varchar(20),TTV.courtdatescan),13,18),'12:00AM','0:00AM')    AS courtdatescan, --scan                   
   convert(int, isnull(TTV.courtnumberscan,0) ) as courtnumberscan,                          
    replace(TC.shortname,'--Choose','N/A')as shortname  ,                
                 ticketsviolationid,                   
 replace(dt1.description,'---Choose----','N/A') as SDesc,                 
 replace(dt2.description,'---Choose----','N/A') as ADesc,                
 tt.activeflag                 
              
-- Added By Zeeshan Ahmed For Update Panel              
              
,              
ttv.bondamount,              
ttv.fineamount,              
ttv.casenumassignedbycourt as causenumber,              
ttv.planid,              
ttv.violationnumber_pk,              
--ttv.ticketsviolationid ,              
ttv.courtviolationstatusidmain,              
ttv.courtnumbermain,              
ttv.Courtdatemain,              
ttv.oscarcourtdetail,              
ttv.underlyingbondflag as bondflag,              
isnull(tblCourtViolationStatus_2.ShortDescription,'') AS VerCourtDesc ,              
dbo.tblViolations.Description +                                       
(case                                      
when ttv.violationnumber_pk = 0 then ''                                      
else  ' ' + '(' + ISNULL(vc.ShortDescription, 'N/A')+ ')'                                       
end ) as violationDescription,              
ttv.courtid,              
CVS.CategoryID ,           
isnull(TTV.ChargeLevel,0) as ChargeLevel,          
Convert ( int ,tc.IsCriminalCourt) as IsCriminalCourt,        
isnull ( TTV.CDI  ,'0' ) as CDI  ,                   
dbo.fn_CheckFTAViolationInCase( TT.TicketID_PK)  as hasFTAViolations,    
dbo.fn_CheckFTAViolation(ticketsviolationid)  as isFTAViolation ,
case when ( Select Count(*) from tblticketsflag where flagid=15 and ticketid_pk = tt.ticketid_pk) =0 then 0 else 1 end  as hasNOS,
TTV.CoveringFirmID ,          
TTV.ArrestDate  ,
isnull(tt.casetypeid ,0) as  casetypeid,
isnull(TTV.MissedCourtType,-1) as  MissedCourtType            
into #temp                
FROM                    
                
tbltickets tt, tblticketsviolations ttv, tblCourtViolationStatus c1,                
 tblCourtViolationStatus c2,                 
tbldatetype dt1,tblcourts tc,tbldatetype dt2                
              
-- Added By Zeeshan Ahmed ------------------------------------------------------              
,tblCourtViolationStatus tblCourtViolationStatus_2 ,tblViolations ,                    
   tblViolationCategory vc ,tblCourtViolationStatus CVS               
              
              
--------------------------------------------------------------------------------              
              
where tt.ticketid_pk = ttv.ticketid_pk                
              
---------Added By Zeeshan Ahmed              
and tblViolations.CategoryID = vc.CategoryId                                                         
and TTV.ViolationNumber_PK = dbo.tblViolations.ViolationNumber_PK                      
and TTV.CourtViolationStatusIDmain = tblCourtViolationStatus_2.CourtViolationStatusID              
and TTV.CourtViolationStatusID = CVS.CourtViolationStatusID              
-------------------------------------------------------------------------------------              
              
and ttv.courtviolationstatusid = c2.courtviolationstatusid                
and c2.categoryid = dt2.typeid                
and ttv.courtviolationstatusidscan = c1.courtviolationstatusid                
and c1.categoryid = dt1.typeid                
and ttv.courtId = tc.courtid                
and  dt1.typeid!=50      --dont need records with 'disposed'status for auto                    
and (                 
 ttv.courtdate <> ttv.courtdatescan                
 or                
 ttv.courtnumberscan <> ttv.courtnumber                
        or                   
 dt1.typeid <> dt2.typeid                
 )                 
 and dt2.typeid in (2,3,4,5)                    
 and activeflag = 1
 -- Noufil 5059 11/06/2008 Scan status should not be missed Court
 and ttv.courtviolationstatusidscan !=105
 and ttv.courtdate between @startdate and @enddate
order by TT.ticketid_pk                      
                
--select * from #temp                
--where (                
-- courtnumber <> courtnumberscan                
-- or                
-- courtdate <> courtdatescan                
-- or                
-- sdesc <> adesc                
-- )                
--                
               
-- Added By Zeeshan Ahmed On 23rd August 2007            
-- Show Records from All Courts         
         
if  @courtlocation = 0             
Begin            
            
 select * from #temp                
      where (                
       courtnumber <> courtnumberscan                
       or                
       courtdate <> courtdatescan                
       or                
       sdesc <> adesc                
       )                
            
End            
            
-- Show records of Inside courts         
         
else if @courtlocation = 1             
Begin            
              
 select * from #temp                
      where (                
       courtnumber <> courtnumberscan                
       or                
       courtdate <> courtdatescan                
       or                
       sdesc <> adesc                
       )                
       and courtid in ( 3001,3002,3003)            
            
             
end            
-- Show records of Outside courts            
else if @courtlocation = 2            
Begin            
             
 select * from #temp                
      where (                
       courtnumber <> courtnumberscan                
       or                
       courtdate <> courtdatescan                
       or                
       sdesc <> adesc                
       )                
       and courtid not in ( 3001,3002,3003)            
             
End             
            
drop table #temp       
      
      
      
  
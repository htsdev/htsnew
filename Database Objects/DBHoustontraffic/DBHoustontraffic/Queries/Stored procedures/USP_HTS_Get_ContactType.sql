SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Get_ContactType]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Get_ContactType]
GO










CREATE PROCEDURE  USP_HTS_Get_ContactType

 AS

Select  ContactType_PK as ID,Description  From tblContactsType
where [description] NOT LIKE '%CHOOSE%'





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


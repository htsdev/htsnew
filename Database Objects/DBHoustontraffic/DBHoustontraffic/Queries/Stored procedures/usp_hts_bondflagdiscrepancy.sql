/*
* Modified by: Ozair
* Task ID : 7791
* Business logic : This procedure is used to get all cases having bond flag discrepency 
* Parameter : N/A     

* */
ALTER PROCEDURE [dbo].[usp_hts_bondflagdiscrepancy]
AS
	-- Noufil 4432 08/05/2008 html serial number added added for validation report
	
	SELECT *,
	       '<a href="http://ln.legalhouston.com/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber='
	       + CONVERT(VARCHAR(30), main.TicketID_PK) + '" >' + CONVERT(VARCHAR(30), ROW_NUMBER() OVER(ORDER BY main.TicketID_PK))
	       + '</a>' AS link
	FROM   (
	           SELECT DISTINCT
	                  TT.TicketID_PK,
	                  ISNULL(ttv.casenumassignedbycourt, '') AS CauseNumber,
	                  tt.lastname,
	                  tt.firstname,
	                  ISNULL(c1.shortdescription, 'N/A') AS STATUS,
	                  --Ozair 7979 07/14/2010 remove conversion to int from courtnumbermain            
	                  CONVERT(VARCHAR(11), TTV.courtdatemain, 101) + ' @ ' + 
	                  REPLACE(
	                      SUBSTRING(CONVERT(VARCHAR(20), TTV.courtdatemain), 13, 18),
	                      '12:00AM',
	                      '0:00AM'
	                  ) + ' # ' + ISNULL(TTV.courtnumbermain, 0) AS courtinfo,
	                  CASE 
	                       WHEN ISNULL(tt.Bondflag, 0) = 0 THEN 'No'
	                       ELSE 'Yes'
	                  END AS bondflag,
	                  CASE 
	                       WHEN (
	                                SELECT COUNT(sb.ticketid)
	                                FROM   tblscanbook sb WITH(NOLOCK)
	                                WHERE  sb.ticketid = tt.TicketId_Pk
	                                       AND sb.doctypeid IN (8, 12)
	                            ) > 0 THEN 'Yes'
	                       ELSE CASE 
	                                 WHEN (
	                                          SELECT COUNT(hn.ticketid_fk)
	                                          FROM   tblHTSNotes hn WITH(NOLOCK)
	                                          WHERE  hn.ticketid_fk = tt.TicketId_Pk
	                                                 AND hn.LetterID_FK = 4
	                                      ) > 0 THEN 'Yes'
	                                 ELSE CASE 
	                                           WHEN (
	                                                    SELECT COUNT(tn.ticketid)
	                                                    FROM   tblticketsnotes 
	                                                           tn WITH(NOLOCK)
	                                                    WHERE  tn.ticketid = tt.TicketId_Pk
	                                                           AND tn.subject = 
	                                                               'bond letter printed'
	                                                ) > 0 THEN 'Yes'
	                                           ELSE 'No'
	                                      END
	                            END
	                  END AS BondDocuments,
	                  tt.activeflag,
	                  tt.Criminalfollowupdate AS followupdate
	           FROM   dbo.tblTickets AS tt WITH(NOLOCK)
	                  INNER JOIN dbo.tblTicketsViolations AS ttv WITH(NOLOCK)
	                       ON  tt.TicketID_PK = ttv.TicketID_PK
	                  INNER JOIN dbo.tblCourtViolationStatus AS c1 WITH(NOLOCK)
	                       ON  ttv.CourtViolationStatusIDmain = c1.CourtViolationStatusID
	                  INNER JOIN dbo.tblTicketsPayment AS p WITH(NOLOCK)
	                       ON  tt.TicketID_PK = p.TicketID
	           WHERE  --Ozair 7791 07/24/2010  where clause optimized    
	                  tt.ticketid_pk NOT IN (SELECT DISTINCT ticketid_pk
	                                         FROM   tblticketsflag WITH(NOLOCK)
	                                         WHERE  flagid = 32)
	                  AND p.paymentvoid <> 1
	                  AND p.paymenttype <> 8
	                  AND c1.categoryid IN (1, 2, 3, 4, 5, 12)
	                  AND tt.activeflag = 1
	       ) main
	WHERE  main.bondflag <> main.bonddocuments
	ORDER BY
	       main.TicketID_PK -- Adil Aleem 5857 05/15/2009 Include Ticket ID in order by clause
	       
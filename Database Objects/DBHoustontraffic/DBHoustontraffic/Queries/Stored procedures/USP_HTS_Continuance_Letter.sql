



/*******************************
Created by: unknow
Create date: 

Business Logic:
	The procedure is used to display the Continuance letter contents on HTP /Matter / Billing /Continuance letter

Input Parameters:
	@TicketId:	Record identification
	@EmployeeId:Who is printing the Continuance letter

Output Columns:
	ticketid_pk:		record identification
	refcasenumber:		ticket number
	description:		line delimited cause/ticket number + violation description
	firstname:			first name of the client
	lastname:			last name of the client
	currentdateset:		verified court date
	currentcourtnum:	verified court number
	Address:			mailing address of the client
	city:				city associated with the mailing address
	state:				state associated with the mailing address
	zip:				zip code associated with the mailing address
	contrequestnotes:	continuance comments
	courtid:			court id associated with the violation
	pct_pl:				precint & place of the court; for HCJP courts.

************************/

--147611

--EXEC dbo.USP_HTS_Continuance_Letter 251254 ,4177
ALTER procedure dbo.USP_HTS_Continuance_Letter
          
@Ticketid int,          
@employeeid int           
       
as          



select T.ticketid_pk,      
 
	O.description,      
	 firstname,       
	 lastname,      
	 --Faique Ali 11422 09/20/2013 adding requierd fileds
	 CONVERT(varchar(20),courtdatemain,101) AS courtdate, 
	 LTRIM(RIGHT(CONVERT(VARCHAR(20), courtdatemain, 100), 7)) AS courttime,
	 ISNULL (T.LanguageSpeak,'ENGLISH') AS LanguageSpeak,	 
	 CASE WHEN LEN( isnull(V.casenumassignedbycourt,''))>0 THEN  V.casenumassignedbycourt ELSE ISNULL(V.RefCaseNumber,'')  END AS causenumber,
	 v.RefCaseNumber,v.casenumassignedbycourt,
	 upper(isnull(M.CountyName,'')) as CountyName,
	 CASE WHEN c.Courtid=3006 THEN upper (SUBSTRING(c.CourtName,0,25)+'Court' ) ELSE UPPER(	c.CourtName ) END  AS CourtName ,	 
	 courtdatemain,      
	 courtnumbermain,      
	 Address,      
	 C.city,      
	 UPPER( isnull(S.StateName ,''))StateName, 
	 ISNULL( c.courtcategorynum,0) AS courtcategorynum,
	 C.zip,      
	 T.ContinuanceComments as contrequestnotes,      
	 isnull(v.courtid,0) AS courtid,  
	 --ozair task 3196 on 16/2/2008    
	 isnull(c.precinctid,0) as pct_pl          
into #tblcontinuance          
from tbltickets T, tblticketsviolations V, tblcourts C, tblstate S, tblviolations O
,tbl_Mailer_County M
where  T.ticketid_pk = V.ticketid_pk          
	--Faique Ali 11422 09/27/2013 get only active courts
	and  v.courtid = C.courtid  AND c.InActiveFlag=0         
	and  C.state  = S.stateid          
	and  v.violationnumber_pk = O.violationnumber_pk     
	and  violationtype <> 1          
	and  activeflag = 1          
	and  continuanceamount > 0          
	and  T.ticketid_pk = @Ticketid   
	--Faique Ali 11422 09/23/2013 Exclude dispose cases      
	AND c.CountyId=M.CountyID AND M.IsActive=1
	AND v.CourtViolationStatusIDmain <>80
ORDER BY V.RefCaseNumber ASC


declare @temp varchar(2000)      
select @temp = ''
DECLARE @caseNum INT,@refcasenum INT   

select        
	 ticketid_pk,
	 @temp as [description],      
	 firstname,       
	 lastname,      
	 courtdatemain as currentdateset,      
	 courtnumbermain as currentcourtnum,      
	 Address,      
	 city,      
	 stateName,      
	 zip,      
	 contrequestnotes,      
	 courtid,  
	 MAX(courtdate) AS courtdate, --Faique Ali 11422 09/20/2013 adding requierd fileds
	 CourtName,
	 courttime,
	 causenumber,
	 CountyName,
	 courtcategorynum,
	 LanguageSpeak,
	 RefCaseNumber,
	 casenumassignedbycourt,
	 --ozair task 3196 on 16/2/2008  
	 pct_pl 
	 
INTO #temp        
from  #tblcontinuance
GROUP BY --Faique Ali 11422 09/23/2013 Exclude dispose cases and get single record
	 ticketid_pk,

	 [description],firstname,lastname,courtdatemain,courtnumbermain,Address,city,stateName,zip,      
	 contrequestnotes,courtid,courttime,
	 causenumber,
	 LanguageSpeak,pct_pl,CountyName,courtcategorynum,CourtName
	 ,RefCaseNumber
	 ,casenumassignedbycourt 

ORDER BY courtdate ASC


	IF( (SELECT COUNT(casenumassignedbycourt) FROM #temp WHERE LEN(casenumassignedbycourt) > 0) > 0)
	BEGIN
		SELECT TOP 1 * FROM #temp t WHERE LEN(casenumassignedbycourt) > 0
	END
	ELSE
		BEGIN
			SELECT TOP 1 * FROM #temp t
		END


drop table #temp        
drop table #tblcontinuance	
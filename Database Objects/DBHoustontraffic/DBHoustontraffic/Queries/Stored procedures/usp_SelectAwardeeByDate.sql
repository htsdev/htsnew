SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_SelectAwardeeByDate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_SelectAwardeeByDate]
GO






CREATE PROCEDURE usp_SelectAwardeeByDate 

@sdate datetime,
@edate datetime

AS

         SELECT    
                             AWDNAME, AWDADDRESS, AWDCITY, AWDSTATE, AWDZIP, AWARDEE,RecordKey,AWDNBR 
         FROM         
                             [dbo].[AWARDS]
        WHERE                             mergeDate between @sdate and @edate	and  yds = 'N'
         and
                       awdaddress+awdcity+awdstate+awdzip NOT IN                                                     
                            (SELECT     dbo.AwardeeBlockAddresses.AWDADDRESS+dbo.AwardeeBlockAddresses.AWDcity+dbo.AwardeeBlockAddresses.AWDstate+dbo.AwardeeBlockAddresses.AWDzip
                            FROM          dbo.AwardeeBlockAddresses)
order by RecordKey

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


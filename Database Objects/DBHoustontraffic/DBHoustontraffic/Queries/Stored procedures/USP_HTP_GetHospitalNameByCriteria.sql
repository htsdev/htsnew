﻿/******  
* Created By :	Fahad Muhammad Qureshi.
* Create Date :   10/19/2010 12:44:00 PM
* Task ID :				6766
* Business Logic :  This procedure is used to Get .
* List of Parameter :
* Column Return : 
      
******/
 
CREATE PROCEDURE dbo.USP_HTP_GetHospitalNameByCriteria 
@Criteria VARCHAR(MAX) = ''
AS      
      
BEGIN
    SELECT DISTINCT TOP 15 h.[Name] AS Hospital
    FROM Hospital h
    WHERE  h.[Name] LIKE @criteria + '%'
           AND h.[Name] IS NOT NULL
           AND LEN(h.[Name]) > 0
    ORDER BY
           h.[Name]
END 
GO
GRANT EXECUTE ON dbo.USP_HTP_GetHospitalNameByCriteria TO dbr_webuser
GO 	  
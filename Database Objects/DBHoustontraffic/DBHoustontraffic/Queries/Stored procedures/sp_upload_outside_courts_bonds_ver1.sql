if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_upload_outside_courts_bonds_ver1]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_bug_getstatus]
GO

create procedure [dbo].[sp_upload_outside_courts_bonds_ver1]            
as            
            
declare @state varchar(2)            
select distinct VIOLATION as violations into #temp             
from tblOutsideCourtsBondtemp            
            
            
insert into dbo.tblviolations(description,sequenceorder,IndexKey,IndexKeys,ChargeAmount,BondAmount,            
Violationtype,ChargeonYesflag,ShortDesc,ViolationCode,AdditionalPrice)            
select violations,null,2,100,100,140,3,1,'None','None',0 from #temp             
where violations not in (            
select description from dbo.tblviolations where violationtype = 3)            
            
            
select firstname,lastname,ADD1,ADD2,T.CITY,isnull(T.STATE,'TX') as state,T.ZIP,T.PHONE,CASENO,            
Status,ENTRYDATE,VIOLATION,outsidemisc1,outsidemisc2,DP2,DPC,ERRSTAT,DPV,listdate,            
Officernumber,MidNumber,courtid            
 into #temp2             
from tblOutsideCourtsBondtemp T, tblcourts C            
where substring(T.caseno,3,2) = precinctid            
and CASENO not in (            
select distinct TicketNumber from dbo.tblTicketsArchive )            
            
select firstname,lastname,ADD1,ADD2,T.CITY,isnull(T.STATE,'TX') as state,T.ZIP,T.PHONE,CASENO,            
Status,ENTRYDATE,VIOLATION,outsidemisc1,outsidemisc2,DP2,DPC,ERRSTAT,DPV,listdate,            
Officernumber,MidNumber,courtid            
 into #temp3             
from tblOutsideCourtsBondtemp T, tblcourts C            
where substring(T.caseno,3,2) = precinctid            
and CASENO not in (            
select distinct TicketNumber_pk from dbo.tblTicketsviolationsArchive )            
            
            
            
            
--select * from dallasdata            
--select * from dbo.tblTicketsArchive            
select distinct CASENO into #duplicatecases            
from #temp2,dbo.tblstate S            
where #temp2.state = S.state            
and caseno not in (            
select distinct TicketNumber from dbo.tblTicketsArchive )            
group by caseno            
having count( CASENO) > 1            
            
            
select distinct CASENO into #duplicatecasesviolations            
from #temp3,dbo.tblstate S            
where #temp3.state = S.state            
and caseno not in (            
select distinct TicketNumber_pk from dbo.tblTicketsviolationsArchive )            
group by caseno            
having count( CASENO) > 1            
            
            
            
INSERT INTO dbo.tblTicketsArchive(TicketNumber,  FirstName, LastName,             
Initial, Address1, Address2,City, StateID_FK,  ZipCode,PhoneNumber,violationdate, listdate            
,DP2 ,DPC,flag1,courtid,officerNumber_Fk,officername,midnumber,outsidemisc1,outsidemisc2)            
            
select distinct CASENO as ticketnumber,firstname,lastname,'' as initial,            
ADD1 as address1 , ADD2 as address2,            
 CITY as city,            
stateid,ZIP as zip,phone ,            
dbo.convertstringtodate(entrydate),listdate,dp2,dpc,DPV,#temp2.courtid as courtid,            
962528,'N/A',midnumber,outsidemisc1,outsidemisc2            
from #temp2,dbo.tblstate S            
where #temp2.state = S.state            
and caseno not in (            
select distinct TicketNumber from dbo.tblTicketsArchive )            
and caseno not in (            
select distinct caseno from #duplicatecases)            
--select * from #temp2            
--select * from tblcourtviolationstatus            
            
insert into tblcourtviolationstatus(description,violationcategory,SortOrder)            
select distinct status,1,0  from tblOutsideCourtsBondtemp            
where status not in (select description from tblcourtviolationstatus)            
            
DECLARE @temp_ticketnumber varchar(20),            
@temp_violationnmber int,            
@temp_fineamount money,            
@temp_violation varchar(100),            
@tenp_violationcode varchar(10),            
@temp_status int,            
@temp_courtdate datetime,            
@temp_courtnum varchar(10),            
@temp_courtid int,            
@temp_bondamount money,            
@temp_violationdate datetime,            
@temp_officernum varchar(20)            
            
            
--select * from #tempticketsviolationsarchive             
      
declare @Count  int      
set @Count=0     
    
---------------------------------For Court Wise Count------------------------------    
select distinct CASENO as ticketnumber,violationnumber_pk,chargeamount as fineamount,            
VIOLATION  as description,violationnumber_pk as violationcode,            
CourtViolationStatusID,'01/01/1900' as courtdate ,courtid    
into #CountCourtWise    
from #temp3 O, dbo.tblviolations V, tblcourtviolationstatus C            
where ltrim(O.violation) = V.description            
and isnull(O.status,'N/A') = C.description            
and caseno not in (            
select distinct TicketNumber_pk from dbo.tblTicketsviolationsArchive )            
and violationtype = 3            
and caseno not in (            
select distinct caseno from #duplicatecasesviolations)    
    
declare @Count_new table(    
id int identity(1,1),    
courtid int,    
courtname varchar(50),    
reccount int    
)    
    
insert into @Count_new(courtid,courtname,reccount)    
select v.courtid,c.shortname,count(v.courtid) as recordcount     
from #CountCourtWise v inner join tblcourts c    
on c.courtid=v.courtid    
group by v.courtid,c.shortname     
    
declare @rec int    
declare @idx int    
set @idx=1    
select @rec=max(id) from @Count_new    
    
declare @tab varchar(max)    
set @tab='<table border="1" cellpadding="0" cellspacing="0" bordercolor="black" style="border-collapse: collapse"><tr><td>Court</td><td>Record Uploaded</td></tr>'    
while @idx<=@rec    
begin    
 select @tab=@tab+'<tr><td>'+convert(varchar,courtname)+'</td><td>'+convert(varchar,reccount)+'</td></tr>' from @Count_new where id=@idx    
 set @idx = @idx + 1    
end    
    
set @tab=@tab+'</table>'    
-----------------------------------------------------------------------------------    
            
DECLARE mycursor CURSOR FOR             
select distinct CASENO as ticketnumber,violationnumber_pk,chargeamount as fineamount,            
VIOLATION  as description,violationnumber_pk as violationcode,            
CourtViolationStatusID,'01/01/1900' ,courtid            
from #temp3 O, dbo.tblviolations V, tblcourtviolationstatus C            
where ltrim(O.violation) = V.description            
and isnull(O.status,'N/A') = C.description            
and caseno not in (            
select distinct TicketNumber_pk from dbo.tblTicketsviolationsArchive )            
and violationtype = 3            
and caseno not in (            
select distinct caseno from #duplicatecasesviolations)            
            
            
OPEN mycursor             
            
FETCH NEXT FROM mycursor             
INTO             
@temp_ticketnumber ,            
@temp_violationnmber ,            
@temp_fineamount ,            
@temp_violation ,            
@tenp_violationcode ,            
@temp_status ,            
@temp_courtdate ,@temp_courtid            
            
WHILE @@FETCH_STATUS = 0             
BEGIN             
            
            
INSERT INTO dbo.tblticketsViolationsArchive(TicketNumber_PK, ViolationNumber_PK, FineAmount,ViolationDescription,ViolationCode,            
 violationstatusid,courtdate,Recordid,courtlocation, causenumber)            
select @temp_ticketnumber ,            
@temp_violationnmber ,            
@temp_fineamount ,            
@temp_violation ,            
@tenp_violationcode ,            
@temp_status ,            
@temp_courtdate ,0 ,@temp_courtid, @temp_ticketnumber            
            
set @Count=@Count+1      
            
FETCH NEXT FROM mycursor             
INTO             
@temp_ticketnumber ,            
@temp_violationnmber ,            
@temp_fineamount ,            
@temp_violation ,            
@tenp_violationcode ,            
@temp_status ,            
@temp_courtdate,@temp_courtid            
            
END             
            
CLOSE mycursor             
DEALLOCATE mycursor             
            
            
            
           
/*            
INSERT INTO dbo.tblticketsViolationsArchive(TicketNumber_PK, ViolationNumber_PK, FineAmount,ViolationDescription,ViolationCode,violationstatusid,courtdate)            
select distinct CASENO as ticketnumber,violationnumber_pk,chargeamount as fineamount,            
VIOLATION  as description,violationnumber_pk as violationcode,            
CourtViolationStatusID,'01/01/1900'             
from #temp3 O, dbo.tblviolations V, tblcourtviolationstatus C            
where ltrim(O.violation) = V.description            
and isnull(O.status,'N/A') = C.description            
and caseno not in (            
select distinct TicketNumber_pk from dbo.tblTicketsviolationsArchive )            
and violationtype = 3            
and caseno not in (            
select distinct caseno from #duplicatecasesviolations)            
*/            
            
            
/*            
            
update tblTicketsArchive             
set bondflag = 1, bonddate = getdate()            
where ticketnumber in (            
select distinct caseno from tblOutsideCourtsBondtemp            
)            
and bonddate is null            
            
*/            
            
            
            
declare @updatedCount int  
set @updatedCount=0  
  
update T            
set bondflag = 1, bonddate = getdate()            
from tblTicketsArchive T, tblOutsideCourtsBondtemp E,tblcourts C            
where T.ticketnumber = E.caseno            
and substring(E.caseno,3,2) = precinctid            
and T.courtid = C.courtid            
and bonddate is null  

set @updatedCount=@@Rowcount 

------------------------------------------------------------------------------
-- ADDED BY TAHIR AHMED DT:3/7/08 TASK 3382
-- NEED TO UPDATE BOND DATE IN TBLTICKETSVIOLATIONSARCHIVE TABLE TOO...
------------------------------------------------------------------------------
UPDATE v
set v.bonddate = getdate()
from tblticketsviolationsarchive v, tblOutsideCourtsBondtemp E,tblcourts C            
where v.ticketnumber_pk = E.caseno            
and substring(E.caseno,3,2) = c.precinctid            
and v.courtlocation = C.courtid            
and v.bonddate is null 
------------------------------------------------------------------------------

--set @updatedCount=@@Rowcount 
--print @updatedcount           
            
            
            
drop table #temp            
drop table #temp2            
drop table #duplicatecases            
drop table #duplicatecasesviolations            
            
            
          
-----------------------------------------------------------------------------------------------------------------------            
-- UPDATE DO NOT MAIL FLAG FOR NEWLY INSERTED RECORDS..            
-- IF PERSON'S NAME & ADDRESS MARKED AS DO NOT MAIL...            
-----------------------------------------------------------------------------------------------------------------------            
declare @Courtids varchar(8000)          
set @Courtids=''          
select @Courtids=@Courtids+convert(varchar,courtid)+',' from tblcourts          
where courtcategorynum=2          
          
declare @CurrDate datetime          
set @CurrDate=getdate()          
EXEC USP_HTS_UPDATE_NOMAILFLAG @CurrDate, @Courtids         
        
-- SENDING UPLOADED NO OF RECORDS THROUGH EMAIL         
      
EXEC usp_sendemailafterloaderexecutes  @Count,4,'harris county jp',@tab,@updatedCount   
go

grant execute on [dbo].[sp_upload_outside_courts_bonds_ver1] to dbr_webuser
go
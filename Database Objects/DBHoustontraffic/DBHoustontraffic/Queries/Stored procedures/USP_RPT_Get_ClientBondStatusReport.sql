alter procedure dbo.USP_RPT_Get_ClientBondStatusReport

as 

-- first get the hiredate, last payment date and payment count for each client...
select ticketid, min(recdate) hiredate, max(recdate) lastpaydate,  count(distinct invoicenumber_pk) paycount
into #hires
from tblticketspayment
where isnull(paymentvoid,0) = 0
group by ticketid
order by ticketid


-- now get all payment transactions....
select distinct  a.*, p.invoicenumber_pk, p.recdate paydate
into #payments
from tblticketspayment p inner join #hires a on a.ticketid = p.ticketid
where isnull(paymentvoid,0) = 0
order by a.ticketid

-- not get the max date when case went into bond or bond waiting status...
select distinct v.ticketid_pk, min(l.recdate) bonddate
into #bonds
from tblticketsviolations v 
inner join  tblticketsviolationlog l on l.ticketviolationid = v.ticketsviolationid
inner join tbltickets t on t.ticketid_pk = v.ticketid_pk
where newverifiedstatus in (202,135,258)
and oldverifiedstatus <> newverifiedstatus
and t.activeflag = 1
group by v.ticketid_pk
order by v.ticketid_pk

-- dump all records in temp table....
select distinct p.ticketid, p.paycount, p.invoicenumber_pk, p.hiredate, p.lastpaydate,  p.paydate, b.bonddate
into #result
from #payments p inner join #bonds b on p.ticketid = b.ticketid_pk
order by p.ticketid, p.invoicenumber_pk


-- list only those cases having bond set date after last payment...
select distinct 
'<a href="../ClientInfo/ViolationFeeold.aspx?sMenu=61&search=0&casenumber=' + convert(Varchar,t.ticketid_pk) + '" target="_blank">' + t.lastname + isnull(', '+t.firstname,'') + '</a>' as [Client Name],
 p.lastpaydate as [Last Payment Date], 
 p.bonddate as [Bond Set Date]
from #result p inner join tbltickets t on t.ticketid_pk = p.ticketid
where bonddate > lastpaydate 
and t.activeflag = 1
order by [Bond Set Date] desc, [Client Name]

drop table #hires
drop table #payments
drop table #bonds
drop table #result





-- =============================================  
-- Created By:  Sabir Khan 
-- Task ID:		5977 
-- Create Date:	07/02/2009
-- Business Logic: This stored procedure is used to insert or update follow up date of HMC Setting Discrepancy cases.  
-- List of Parameters: 
--		@TicketID_PK:		unique identifier
--		@FollowUpDate:	    Updated Follow Up Date
-- List of Columns: 
--		TicketID_PK:		This field is the unique identifier from tblTickets table.
--		FollowUpDate:		Follow Update to be insert/update.   



CREATE PROCEDURE [dbo].[USP_HTP_Update_HMCSettingDiscrepancyFollowUpDate]

@TicketID_PK int,
@FollowUpDate datetime=NULL
AS

BEGIN

declare @L_Exist int 
set @L_Exist =0

select  @L_Exist = Count(TicketID_PK) from tblTicketsExtensions
where TicketID_PK = @TicketID_PK

if(@L_Exist=0)
	begin
	insert into tblTicketsExtensions(TicketID_PK ,SettingDiscrepancyFollowupDate)
	values(@TicketID_PK ,@FollowUpDate)


	end
else
	begin			
			update tblTicketsExtensions
			set SettingDiscrepancyFollowupDate = @FollowUpDate			
			where TicketID_PK = @TicketID_PK
	
	end

END


GO

GRANT EXECUTE ON [dbo].[USP_HTP_Update_HMCSettingDiscrepancyFollowUpDate] TO dbr_webuser

GO
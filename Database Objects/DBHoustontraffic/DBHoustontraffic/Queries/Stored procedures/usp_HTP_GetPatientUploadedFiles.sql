﻿/******  
* Created By :	  Syed Muhammad Ozair.
* Create Date :   11/08/2010 
* Task ID :		  8501
* Business Logic :  This procedure is used to Get patient uploaded files.
* List of Parameter :
* Column Return :     
******/
ALTER PROCEDURE dbo.usp_HTP_GetPatientUploadedFiles
	@PatientId INT,
	@DocumentTypeId INT = -1
AS
	SELECT puf.Id,
	       puf.UploadDate,
	       puf.OriginalFileName,
	       puf.[Description] fileDescription,
	       dt.[Description] documentType,
	       dst.[Description] documentSubType,
	       u.LastName +', '+u.FirstName AS username,
	       puf.StoreFileLocation
	FROM   PatientsUploadedFile puf
	       LEFT OUTER JOIN DocuemntType dt
	            ON  dt.Id = puf.DocumentTypeId
	       LEFT OUTER JOIN DocumentSubType dst
	            ON  dst.Id = puf.DocumentSubTypeId
	       INNER JOIN Users.dbo.[User] u
	            ON  u.UserId = puf.UploadedBy
	WHERE  puf.PatientId = @PatientId
	       AND isnull(puf.DocumentTypeId,0) = case when @DocumentTypeId=-1 THEN isnull(puf.DocumentTypeId,0) ELSE @DocumentTypeId end 
	       

GO

GRANT EXECUTE ON dbo.usp_HTP_GetPatientUploadedFiles TO dbr_webuser
GO 
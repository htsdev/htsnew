USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTP_GetAllBadMarkedRecords]    Script Date: 12/11/2013 10:07:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:			Rab Nawaz Khan
-- Create date:		09/18/2013
-- Business Logic:	This Procedure is Used to Get all the records which are marked as bad mail/not delivered.
-- =============================================
ALTER PROCEDURE [dbo].[USP_HTP_GetAllBadMarkedRecords] 
	@letterType INT = 138,
	@badMarkedDateFrom DATETIME = '07/04/2013',
	@badMarkedDateTo DATETIME = '10/08/2013',
	@showAll BIT = 0,
	@isValidationReport BIT = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON; 
	
	-- Getting the Deployment Date of task. . . 
	DECLARE @menuId INT
	DECLARE @SubMenuId INT
	DECLARE @deploymentDate DATETIME
	SELECT @menuId = ttm.ID FROM tbl_TAB_MENU ttm WHERE ttm.TITLE LIKE 'VALIDATIONS'
	SELECT @SubMenuId = tts.ID FROM tbl_TAB_SUBMENU tts WHERE tts.TITLE LIKE 'LMS Address Check' AND tts.MENUID = @menuId
	BEGIN
		SELECT @deploymentDate = CONVERT(VARCHAR(50), MIN(InsertDate), 101) FROM ValidationReportSettings v WHERE v.ReportID = @SubMenuId	
	END
	 
	 SET @deploymentDate = '05/05/2013'

	
	DECLARE  @tblBadRecs AS TABLE
	(
		NoteId INT,
		RecordID INT,
		LetterType VARCHAR(150) NULL,
		LetterPrintDate VARCHAR(50) NULL,
		FirstName VARCHAR(100),
		LastName VARCHAR(100),
		ExistingAddress VARCHAR (1000) NULL,
		CompleteNewAddress1 VARCHAR(1000) NULL,
		CompleteNewAddress2 VARCHAR(1000) NULL,
		BadMarkDate VARCHAR(50) NULL,
		RecDate DATETIME,
		LetterID_PK INT,
		NewAddress1 VARCHAR(500) NULL,
		NewCity1 VARCHAR(200) NULL,
		NewState1 INT,
		NewZipCode1 VARCHAR(200) NULL,
		NewAddress2 VARCHAR(500) NULL,
		NewCity2 VARCHAR(200) NULL,
		NewState2 INT,
		NewZipCode2 VARCHAR(200) NULL,
		IsNoNewAddressMarked BIT,
		IsNewAddress2Updated BIT,
		CauseNumbers VARCHAR(2000) NULL,
		CauseNumber VARCHAR(50) NULL,
		AddressStatus1 VARCHAR(5) NULL,
		AddressStatus2 VARCHAR(5) NULL
	)
	
	INSERT INTO @tblBadRecs
    SELECT DISTINCT tln.NoteId, tta.RecordID, tc.ShortName + '-' + REPLACE(tl.LetterName, 'Criminal ', '') AS LetterType, CONVERT(VARCHAR(100),  tln.RecordLoadDate, 101) AS LetterPrintDate, 
    tta.FirstName, tta.LastName, dbo.fn_HTP_GetCompleteAddress(tta.RecordID, 0, 0) AS ExistingAddress,
    dbo.fn_HTP_GetCompleteAddress(tta.RecordID, 1, 1) AS CompleteNewAddress1,
    dbo.fn_HTP_GetCompleteAddress(tta.RecordID, 1, 2) AS CompleteNewAddress2,
	REPLACE(CONVERT(VARCHAR(100), tbm.RecDate, 110), '-', '/') AS BadMarkDate, tbm.RecDate, tl.LetterID_PK,
	ISNULL(nd.Address1, 'N/A') AS NewAddress1, ISNULL(nd.City1, 'N/A') AS NewCity1, ISNULL(nd.State1, 0) AS NewState1, ISNULL(nd.ZipCode1, 'N/A') AS NewZipCode1,
	ISNULL(nd.Address2, 'N/A') AS NewAddress2, ISNULL(nd.City2, 'N/A') AS NewCity2, ISNULL(nd.State2, 0) AS NewState2, ISNULL(nd.ZipCode2, 'N/A') AS NewZipCode2,
	ISNULL(nd.IsNoNewAddressMarked, 0) AS IsNoNewAddressMarked, CONVERT(BIT, 0) AS IsNewAddress2Updated, 
	dbo.Fn_HTP_Get_AllCauseNumbersCommaSeperated(0, 1, tta.RecordID) AS CauseNumbers, CONVERT(VARCHAR(100), '') AS CauseNumber,
	ISNULL(nd.AddressStatus1, 'N') AS AddressStatus1, ISNULL(nd.AddressStatus2, 'N') AS AddressStatus2
	FROM tblLetterNotes tln 
	INNER JOIN tblTicketsArchive tta ON tln.RecordID = tta.RecordID
	INNER JOIN tblLetter tl ON tl.LetterID_PK = tln.LetterType
	INNER JOIN tblcourts tc ON tc.Courtid = tln.CourtId
	INNER JOIN tblState ts ON ts.StateID = ISNULL(tta.StateID_FK, 45)
	LEFT OUTER JOIN tblNewAddresses nd ON tta.RecordID = nd.RecordId
	LEFT OUTER JOIN tblBadMailers tbm ON tbm.MailerID = tln.NoteId 
    WHERE tln.LetterType = @letterType
	AND tl.isactive = 1
	AND tc.InActiveFlag = 0
	AND 
		(
			-- Within Selected Date Rang if not selected show all. . . 
			(DATEDIFF(DAY, tbm.RecDate, @badMarkedDateFrom) <=0 AND DATEDIFF(DAY, tbm.RecDate, @badMarkedDateTo) >=0 AND @showAll = 0)
			-- Marked as show All  then show the records after the deployment date of this task
			OR (@showAll = 1 AND DATEDIFF(DAY, @deploymentDate, tbm.RecDate) >= 0)
			-- Mailer being sent three days before and no confirmation has been recievied. . . 
			OR (
				DATEDIFF(DAY, tln.RecordLoadDate, GETDATE()) > 3 
				AND tln.USPSStopTheClockDate IS NULL 
				AND tln.USPSReturnedDate IS NULL 
				AND tln.USPSReturnedFacilityID IS NULL
				AND DATEDIFF(DAY, @deploymentDate, tln.RecordLoadDate) >= 0
			)
		)
	UPDATE @tblBadRecs
	SET IsNewAddress2Updated = 1
	WHERE LEN(NewAddress2) > 0 AND LEN(NewCity2) > 0
	AND NewAddress2 NOT LIKE 'N/A' AND NewCity2 NOT LIKE 'N/A'
	
	
	DECLARE @causeNum AS TABLE
	(
		RecordID INT, CauseNumber VARCHAR(50) NULL
	)
	
	INSERT INTO @causeNum
	SELECT DISTINCT ttva.RecordID, 
	(SELECT TOP 1 CASE WHEN LEN(ISNULL(v.CauseNumber, '')) > 0 THEN v.CauseNumber ELSE v.TicketNumber_PK END 
	FROM tblTicketsViolationsArchive v WHERE v.RecordID = ttva.RecordID) AS CauseNumber
	FROM tblTicketsViolationsArchive ttva INNER JOIN @tblBadRecs r ON ttva.RecordID = r.RecordId
	
	UPDATE b
	SET b.CauseNumber = c.CauseNumber
	FROM @tblBadRecs b INNER JOIN @causeNum c ON b.Recordid = c.RecordId
	
	
	IF (@isValidationReport = 1)
	BEGIN
		SELECT ROW_NUMBER() OVER (ORDER BY RecDate DESC) AS ID, * 
		FROM @tblBadRecs
		WHERE (NewAddress1 LIKE 'N/A' AND NewCity1 LIKE 'N/A' AND NewAddress2 LIKE 'N/A' AND NewCity2 LIKE 'N/A') AND IsNoNewAddressMarked = 0
		AND ((DATEDIFF(DAY, @deploymentDate, RecDate) >= 0) OR (DATEDIFF(DAY, @deploymentDate, CONVERT(DATETIME, LetterPrintDate)) >= 0))
		ORDER BY RecDate DESC	
	END
	
	ELSE
	BEGIN
		SELECT ROW_NUMBER() OVER (ORDER BY RecDate DESC) AS ID, * 
		FROM @tblBadRecs
		WHERE DATEDIFF(DAY, @deploymentDate, RecDate) >= 0 OR DATEDIFF(DAY, @deploymentDate, CONVERT(DATETIME, LetterPrintDate)) >= 0
		ORDER BY RecDate DESC
	END
END



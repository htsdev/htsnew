﻿ 
/****** Object:  StoredProcedure [dbo].[USP_HTP_GetAGCasesForValidation]    Script Date: 09/15/2008 10:25:29 ******/
-- Noufil 4747
/******
	Business Logic : This procedure display all client that are in Civil and their auto and verified court date are different.
	
	Columns: 
		TicketID_PK
		lastname
		firstname
		refcasenumber
		ShortName
		ShortDescription
		ShortDescription
		CourtDate
		tv.CourtDateMain
*****/


CREATE Procedure [dbo].[USP_HTP_GetAGCasesForValidation]
as
SELECT  distinct
		t.TicketID_PK as ticketid_pk,
		lastname= case when len(isnull(t.Lastname,''))>10 then substring(t.Lastname,0,10)+'...'  else t.Lastname end,
		firstname= case when len(isnull(t.Firstname,''))>10 then substring(t.Firstname,0,10)+'...'  else t.Firstname end,		
		refcasenumber = case when len(isnull(tv.casenumassignedbycourt,'')) = 0 then tv.refcasenumber else tv.casenumassignedbycourt end ,		
		c.ShortName as shortname,
		cvs.ShortDescription as autostatus,
		tcv2.ShortDescription AS verifiedcourtstatus,
		dbo.fn_DateFormat(tv.CourtDate, 25, '/', ':', 1) as autocourtdate,
		dbo.fn_DateFormat(tv.CourtDateMain, 25, '/', ':', 1) as  verifiedcourtdate
   
          
FROM         dbo.tblTickets AS t 
				INNER JOIN dbo.tblTicketsViolations AS tv ON t.TicketID_PK = tv.TicketID_PK 
				INNER JOIN dbo.tblCourts AS c ON tv.CourtID = c.Courtid 
				INNER JOIN dbo.tblCourtViolationStatus AS cvs ON tv.CourtViolationStatusID = cvs.CourtViolationStatusID 
				INNER JOIN dbo.tblCourtViolationStatus AS tcv2 ON tv.CourtViolationStatusIDmain = tcv2.CourtViolationStatusID          

WHERE 
	(tv.CourtViolationStatusIDmain <> 80)
	AND t.activeflag=1
	AND datediff(day,tv.CourtDate,tv.CourtDateMain)<>0
	AND t.CaseTypeId=3

order by ticketid_pk , refcasenumber

go
grant exec on [dbo].[USP_HTP_GetAGCasesForValidation] to dbr_webuser

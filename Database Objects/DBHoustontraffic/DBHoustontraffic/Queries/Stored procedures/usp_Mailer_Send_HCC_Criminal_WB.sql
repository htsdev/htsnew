﻿USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[usp_Mailer_Send_HCC_Criminal_WB]    Script Date: 09/24/2013 01:59:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/****** 
Created by:		Farrukh Iftikhar
Task:			10438
Business Logic:	The procedure is used by LMS application to get data for Harris County Criminal New letter
				and to create LMS history for this letter for the selected date range.
				Filters structure is not implemented for this letters.
				
List of Parameters:
	@catnum:		Category number of the selected letter type, if printing for 
					all courts of the selected court category
	@LetterType:	Selected letter type in LMS
	@CrtId:			Court id of the selected court category if printing for individual court hosue.
	@startListdate:	Comma separated dates selected to print letter.
	@empid:			Employee information of the logged in employee.
	@PrintType:		Flag to just preview the letter or to mark letters as sent. If 1 then LMS letter history will be created.
	@SearchType:	Flag that identifies if searching by court date or by list date. 0= list date, 1 = court date
	@isPrinted:		Flag to include already printed letters in the batch.

List of Columns:	
	LetterId:		Letter ID generated for each letter. It will be "NOT PRINTABLE"	if @PrintType = 0
	ClientId:		Identity value to look up the case in non-clients. used to group the records in report file.
	Name:			Person's first and last name that will appear on the letter
	Address:		Person's Home address that will appear on the letter
	City:			Person's Home City that will appear on the letter
	State:			Person's Home State that will appear on the letter
	Zip:			Zip code associated with person home address
	RecordId:		Booking Number of the case
	Span:			SPN number associated with the person
	ArrestDate:		Date when person arrested
	ChargeWording:	Description of charge on the person
	ChargeLevel:	Charge level associated with the charge wording
	Disposition:	Disposition status of the case
	BookingDate:	Booking date of the case
	CaseNumber:		Case number charged on the person
	ClientFlag:		Flag to identify if it is hired/contacted
	ListDate:		Upload date of the case
	DPTwo:			Address status that is used in barcode font on the letter
	DPC:			Address status that is used in barcode font on the letter
	ZipSpan:		First five characters of zip code and SPN number
	Abb:			Short abbreviation of employee who is printing the letter

******/

-- usp_Mailer_Send_HCC_Criminal_WB 8, 137, 8, '5/28/2009,' , 3992, 0,0 ,0
      
ALTER PROC [dbo].[usp_Mailer_Send_HCC_Criminal_WB]
@catnum int=1,                                    
@LetterType int=9,                                    
@crtid int=1,                                    
@startListdate varchar (500) ='01/04/2006',                                    
@empid int=2006,                              
@printtype int =0 ,
@searchtype int    ,
@isprinted  bit                             
                                    
as       

SET NOCOUNT ON;                         
                          
-- DECLARING LOCAL VARIABLES FOR THE FILTERS....                
declare @officernum varchar(50),                                    
@officeropr varchar(50),                                    
@tikcetnumberopr varchar(50),                                    
@ticket_no varchar(50),                                                                    
@zipcode varchar(50),                                             
@zipcodeopr varchar(50),                                    
                                    
@fineamount money,                                            
@fineamountOpr varchar(50) ,                                            
@fineamountRelop varchar(50),                   
              
@singleviolation money,                                            
@singlevoilationOpr varchar(50) ,                                            
@singleviolationrelop varchar(50),                                    
                                    
@doubleviolation money,                                            
@doublevoilationOpr varchar(50) ,                                            
@doubleviolationrelop varchar(50),                                  
@count_Letters int,                  
@violadesc varchar(500),                  
@violaop varchar(50)                                     
                                    
-- DECLARING & INITIALIZING THE VARIABLE FOR DYNAMIC SQL...
declare @sqlquery varchar(max), @sqlquery2 varchar(max)
Select @sqlquery =''  , @sqlquery2 = ''
              
-- ASSIGNING VALUES TO THE VARIABLES 
-- BY GETTTING INFORMATION FROM LMS FILTER STRUCTURE...
-- FOR THIS LETTER TYPE....
Select @officernum=officernum,                                    
@officeropr=oficernumOpr,                                    
@tikcetnumberopr=tikcetnumberopr,                                    
@ticket_no=ticket_no,                                                                      
@zipcode=zipcode,                                    
@zipcodeopr=zipcodeLikeopr,                                    
@singleviolation =singleviolation,                                    
@singlevoilationOpr =singlevoilationOpr,                                    
@singleviolationrelop =singleviolationrelop,                                    
@doubleviolation = doubleviolation,                                    
@doublevoilationOpr=doubleviolationOpr,                                            
@doubleviolationrelop=doubleviolationrelop,                  
@violadesc=violationdescription,                  
@violaop=violationdescriptionOpr ,              
@fineamount=fineamount ,                                            
@fineamountOpr=fineamountOpr ,                                            
@fineamountRelop=fineamountRelop              
                                 
from tblmailer_letters_to_sendfilters WITH(NOLOCK)                                   
where courtcategorynum=@catnum                                    
and Lettertype=@LetterType                                  

-- GETTING THE SHORT ABBREVIATION OF THE CURRENTLY LOGGED IN 
-- EMPLOYEE IN THE LOCAL VARIABLE....
declare @user varchar(10)
select @user = upper(abbreviation) from tblusers WITH(NOLOCK) where employeeid = @empid
                                    
-- CREATING DYNAMIC SQL FOR THE MAIN QUERY...
--causenumber is fetched instead of ticketnumber
--address2 is used instead of addres1(adress1 is fetched twice)
set @sqlquery=@sqlquery+'                                    
Select	distinct tva.recordid, 
	ta.midnumber as span, 
	tva.violationdescription as chargewording, 
	tva.ticketviolationdate as bookingdate, 
	tva.causenumber as casenumber , 
	ta.clientflag,
	--left(ta.zipcode,5) + rtrim(ltrim(ta.midnumber)) as zipspan,
	left(ta.zipcode,5) + rtrim(ltrim(ta.recordid)) as zipspan,
	convert(varchar(10),ta.recloaddate,101) as listdate, 
	flag1 = isnull(ta.Flag1,''N'') ,
	ta.donotmailflag,	
	upper(ltrim(rtrim(ta.firstname)) + '' ''+ ltrim(rtrim(ta.lastname))) as name,
	upper(ta.address1) + isnull('' '' + upper(ta.address2)  ,'''') as address,
	upper(ta.city) as city,
	s.state,
	ta.zipcode,
	ta.dp2 as dptwo,
	ta.dpc,
	count(tva.ViolationNumber_PK) as ViolCount,                                                      
	isnull(sum(isnull(tva.fineamount,0)),0) as Total_Fineamount,
	ISNULL(tva.ChargeLevel, 0) AS ChargeLevel
into #temp                                          
FROM   dbo.tblticketsarchive ta WITH(NOLOCK)
inner join dbo.tblticketsviolationsarchive tva WITH(NOLOCK) on tva.recordid = ta.recordid
inner join dbo.tblstate s on s.stateid = ta.stateid_fk

-- ONLY VERIFIED AND VALID ADDRESSES
where	Flag1 in (''Y'',''D'',''S'') 
and tva.violationstatusid <> 80 
-- NOT MARKED AS STOP MAILING...
and		isnull(ta.donotmailflag,0) = 0

-- EXCLUDE CERTAIN DISPOSITION STATUSES.....
and ISNULL(tva.hccc_dst,'''') not in (''A'',''D'',''P'')

and (ta.insertionloaderId in (26,69,70) or ta.UpdationLoaderID in (71,72,73))
and isnull(tva.cad,'''') = ''''
and isnull(tva.attorneyname,'''') = ''''
and (ISNULL(tva.rea,'''') not in (''DISM'',''DADD'', ''DADH'',''DFPD'',''MCH'',''MCHJ'',''MCHR'',''ADGM'',''ADMP'',''APRB'',''DISP'',''EXHR'',''MCCA'',''NGIH'',''PFCS'',''PNDC'',''PNGJ'',''SFBF''))
and datediff(day,getdate(),isnull(tva.CourtDate,''01/01/1900'')) > 0 
--and datediff(day,DBO.FN_GETNEXTBUSINESSDAY(GETDATE(),5),isnull(tva.CourtDate,''01/01/1900'')) <= 0
-- LAST NAME SHOULD NOT BE EMPTY..
and isnull(ta.lastname,'''') <> ''''

-- FIRST NAME SHOULD NOT BE EMPTY
and isnull(ta.firstname,'''') <> ''''

-- EXCLUDE RECORDS THAT HAVE BEEN ALREADY PRINTED OR ANY HCCC LETTER PRINTED IN PAST 5 DAYS.
AND  TA.RECORDID NOT IN (                                                                              
SELECT N.RECORDID FROM TBLLETTERNOTES N WITH(NOLOCK) INNER JOIN DBO.TBLTICKETSARCHIVE A WITH(NOLOCK)          
ON A.RECORDID = N.RECORDID WHERE LETTERTYPE = ' + CONVERT(VARCHAR, @LETTERTYPE) + 
	    '
UNION 
SELECT L.RECORDID FROM TBLLETTERNOTES L WITH(NOLOCK) WHERE (L.LETTERTYPE in (55, 136, 138) AND DATEDIFF(DAY, L.RECORDLOADDATE, DBO.FN_GETPREVBUSINESSDAY(GETDATE(),5)) <=0)         
) 
-- PERSONS ADDRESS IS NOT CHANGED IN PAST 48 MONTHS (ACCUZIP)...
and		isnull(ta.ncoa48flag,0) = 0'


-- ONLY FOR SELECTED DATE RANGE...
set @sqlquery=@sqlquery+'
and   ' + dbo.Get_String_Concat_With_DatePart_ver2 ( ''+  @startlistdate + '', 'ta.recloaddate' ) 

set @sqlquery=@sqlquery+'
and   ' + dbo.Get_String_Concat_With_DatePart_ver2 ( ''+  @startlistdate + '', 'tva.courtdate' ) 


-- IF PRINTING FOR ALL COURTS OF THE SELECTED COURT CATEGORY...  
if(@crtid= @catnum  )                                    
	set @sqlquery=@sqlquery+' 
	and tva.courtlocation In (Select courtid from tblcourts WITH(NOLOCK) where courtcategorynum = '+convert(varchar,@crtid)+' )'                                        

-- ELSE PRINTING FOR THE INDIVIDUAL SELECTED COURT OF THE CATEGORY....
else 
	set @sqlquery =@sqlquery +' 
	and tva.courtlocation = '+convert(varchar,@crtid) 
                                                        
-- LMS FILTERS SECTION:
-----------------------------------------------------------------------------------------------------------------------------------------------------
-- IF THERE IS ANY FILTER SPECIFIED TO INCLUDE/EXCLUDE ONLY PARTICULAR OFFICER NUMBERS....
if(@officernum<>'')                                                        
	set @sqlquery =@sqlquery + '  
	and ta.officerNumber_Fk '  + @officeropr  +'('+ @officernum+')'                                                         


-- IF THERE IS ANY FILTER DEFINED FOR INCLUDE/EXCLUDE VIOLATION DESCRIPTIONS...
if(@ticket_no<>'')                        
	set @sqlquery=@sqlquery + ' 
	and ('+  dbo.Get_String_Concat_With_op(''+@ticket_no+'' , 'tva.TicketNumber_PK' ,+ ''+ @tikcetnumberopr) +')'                                                              
                                    
-- ZIP CODE FILTER......
-- INCLUDE/EXCLUDE THE SPECIFIED ZIP CODES.....
if(@zipcode<>'')                                                                              
	set @sqlquery=@sqlquery+ ' 
	and ('+ dbo.Get_String_Concat_With_op(''+@zipcode+'' , 'left(ta.zipcode,3)' ,+ ''+ @zipcodeopr) +')'                                   

-- FINE AMOUNT FILTER.....
-- INCLUDE ONLY THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT....
if(@fineamount<>0 and @fineamountOpr<>'not'  )                                  
	set @sqlquery =@sqlquery+ '
    and tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                                       
                                                      
-- EXCLUDE THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT.....
if(@fineamount<>0 and @fineamountOpr = 'not'  )                                  
	set @sqlquery =@sqlquery+ '
    and not tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                                       
-----------------------------------------------------------------------------------------------------------------------------------------------------



set @sqlquery =@sqlquery+ ' 
group by                                                       
tva.recordid, 
	ta.midnumber, 
	tva.violationdescription , 
	tva.ticketviolationdate , 
	tva.causenumber , 
	ta.clientflag,
	--left(ta.zipcode,5) + rtrim(ltrim(ta.midnumber)) ,
	left(ta.zipcode,5) + rtrim(ltrim(ta.recordid)),
	convert(varchar(10),ta.recloaddate,101), 
	isnull(ta.Flag1,''N'') ,
	ta.donotmailflag,
	upper(ltrim(rtrim(ta.firstname)) + '' ''+ ltrim(rtrim(ta.lastname))) ,
	upper(ta.address1) + isnull('' '' + upper(ta.address2)  ,'''') ,
	upper(ta.city) ,
	s.state,
	ta.zipcode,
	ta.dp2 ,
	ta.dpc,
	tva.ChargeLevel
having 1 =1 
'                                  

-- SINGLE VIOLATION FINE AMOUNT FILTER....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE SINGLE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT
if(@singleviolation<>0 and @doubleviolation=0)                                
	set @sqlquery =@sqlquery+ '
	and (  '+ @singlevoilationOpr+  '  (isnull(sum(isnull(tva.fineamount,0)),0) '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and count(tva.ViolationNumber_PK)=1))
      '                                

-- DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...
if(@doubleviolation<>0 and @singleviolation=0 )                                
	set @sqlquery =@sqlquery + '
	and ('+ @doublevoilationOpr+  '   (isnull(sum(isnull(tva.fineamount,0)),0) '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ')and count(tva.ViolationNumber_PK)=2) )
      '                                                    

-- BOTH SINGLE & DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE AND SINGLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...                                
if(@doubleviolation<>0 and @singleviolation<>0)                                
	set @sqlquery =@sqlquery+ '
	and ('+ @singlevoilationOpr+  '  (isnull(sum(isnull(tva.fineamount,0)),0) '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and count(tva.ViolationNumber_PK)=1))                                
    and ('+ @doublevoilationOpr+  '  (isnull(sum(isnull(tva.fineamount,0)),0) '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ') and count(tva.ViolationNumber_PK)=2))
        '              


-- GETTING REOCORDS IN TEMP TABLE...
set @sqlquery=@sqlquery+'
select distinct recordid, span, chargewording, bookingdate, casenumber , clientflag,zipspan,listdate as mdate, flag1 ,donotmailflag,
name,address,city,state,zipcode,dptwo,dpc,ViolCount, Total_Fineamount, ChargeLevel
into #temp1  from #temp where 1=1 
'              
        
-- VIOLATION DESCRIPTION FILTER.....
-- EXCLUDE THE LETTER IF SPECIFIED VIOLATION IS THE ONLY VIOLATION IN THE LETTER....
if(@violadesc <> '')
	BEGIN
		-- NOT LIKE FILTER.......
		if(charindex('not',@violaop)<> 0 )              
			BEGIN
				set @sqlquery=@sqlquery+' 
				and	 not ( violcount =1  and ('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  chargewording  ' ,+ ''+ 'like')+'))'           			
			END
		-- INCLUDE ONLY THOSE LETTER THAT SINGLE VIOLATION AND MATCHES THE SPECIFIED VIOLATION DESCRIPTION...
		-- LIKE FILTER.......
		--if(charindex('not',@violaop) =  0 )              
		ELSE
			BEGIN
				set @sqlquery=@sqlquery+' 
				and	 ( violcount =1  and ('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  chargewording  ' ,+ ''+ 'like')+'))'           	
			END	
	END


set @sqlquery=@sqlquery+'
delete from #temp1 where recordid in (
select v.recordid from tblticketsviolations v WITH(NOLOCK) inner join tbltickets t WITH(NOLOCK)
on t.ticketid_pk = v.ticketid_pk and t.activeflag = 1 
inner join #temp1 a on a.recordid = v.recordid
)

-- Rab Nawaz Khan 10349 09/05/2013 Added Max Fine Amount and Max Jail Time Columns values. . . 
 ALTER TABLE #temp1 
 ADD isMaxFineAmountNotfound BIT NOT NULL DEFAULT 1, HcccMailerVioDesc Varchar(500), MaxFineAmount Money, MaxJailTimePeriod Varchar(100)
 
 -- Rab Nawaz Khan  11459 09/24/2013 Setting the default values to null for the mailer violations . . . 
 update #temp1 set HcccMailerVioDesc = ''''
  
 Update t
 SET t.HcccMailerVioDesc = UPPER(hc.MailerViolationDescription), t.MaxFineAmount = hc.Maxfine, t.MaxJailTimePeriod = UPPER(hc.MaxJail), isMaxFineAmountNotfound = 0
 FROM #temp1 t INNER JOIN tblHCCCViolationsInfo hc on REPLACE(t.chargewording, '' '', '''') = REPLACE(hc.ViolationDescription, '' '', '''') 
 WHERE t.ChargeLevel = hc.ChargeLevel
 
 -- Rab Nawaz Khan  11459 09/24/2013 Updating the matched Records on violation description basis from HCCC table
 Update t
 SET t.HcccMailerVioDesc = UPPER(hc.MailerViolationDescription)
 FROM #temp1 t INNER JOIN tblHCCCViolationsInfo hc on REPLACE(t.chargewording, '' '', '''') = REPLACE(hc.ViolationDescription, '' '', '''') 
 
 
 -- Getting the the Record if multiplie violations and any of the violation not found in HCCC table
 SELECT tt.Recordid INTO #tempFineAmount FROM #temp1 tt
 WHERE tt.Recordid IN (SELECT t.Recordid FROM #temp1 t WHERE t.isMaxFineAmountNotfound = 0)
 AND tt.isMaxFineAmountNotfound = 1
 
 -- Updating the Records
 UPDATE #temp1
 SET isMaxFineAmountNotfound = 1
 WHERE Recordid in (SELECT t.Recordid FROM #tempFineAmount t)
 -- END 10349
' 

-- IF "MARK LETTER AS SENT" OPTION IS SELECTED....
-- CREATE LETTER HISTORY FOR EACH PERSON....
if( @printtype = 1 )         
	BEGIN
		set @sqlquery2 =@sqlquery2 +                                    
		'
			Declare @ListdateVal DateTime,                                  
				@totalrecs int,                                
				@count int,                                
				@p_EachLetter money,                              
				@recordid varchar(50),                              
				@zipcode   varchar(12),                              
				@maxbatch int  ,
				@lCourtId int  ,
				@dptwo varchar(10),
				@dpc varchar(10)
			declare @tempBatchIDs table (batchid int)

			-- GETTING TOTAL LETTERS AND COSTING ......
			Select @totalrecs =count(*) from #temp1 where mdate = @ListdateVal 
			
			set @p_EachLetter=convert(money,0.45) 
		                              
			-- DECLARING CURSOR TO INSERT THE RECORDS IN LETTER HISTORY ONE BY ONE.....
			-- THERE WILL A SEPARATE BATCH FOR EACH LIST/COURT DATE VALUE ......
			Declare ListdateCur Cursor for                                  
			Select distinct mdate  from #temp1                                
			open ListdateCur                                   
			Fetch Next from ListdateCur into @ListdateVal                                                      
			while (@@Fetch_Status=0)                              
				begin                                

					-- GET TOTAL LETTERS FOR THE LIST DATE
					Select @totalrecs =count(distinct recordid) from #temp1 where mdate = @ListdateVal                                 

					-- INSERTING RECORD IN BATCH TABLE......
					insert into tblBatchLetter(Empid, Lettertype,Listdate,CourtId,ParentID,LCount,PExpense) values                                  
					( '+convert(varchar(10), @empid)+'  , '+convert(varchar(10),@lettertype)+' , @ListdateVal, '+convert(varchar(10),@crtid)+' , 0, @totalrecs, @p_EachLetter)                                   


					-- GETTING BATCH ID OF THE INSERTED RECORD.....
					Select @maxbatch=Max(BatchId) from tblBatchLetter  WITH(NOLOCK)
					insert into @tempBatchIDs select @maxbatch                                                     

					-- CREATING CURSOR TO INSERT RECORDS FOR EACH LETTER FOR THE NEWLY INSERTED BATCH....
					Declare RecordidCur Cursor for                               
					Select distinct recordid,zipcode, 3037, dptwo, dpc from #temp1 where mdate = @ListdateVal                               
					open RecordidCur                                                         
					Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc                                          
					while(@@Fetch_Status=0)                              
					begin                              

						-- INSERTING LETTERS IN LETTER HISTORY FOR THE SPECIFIED BATCH...
						insert into tblLetterNotes(BatchId_Fk,listdate,ZipCode,RecordID,PCost,LetterType, courtid, dp2, dpc)                              
						values (@maxbatch,@ListdateVal,@zipcode,@recordid,@p_EachLetter,'+convert(varchar(10),@lettertype)+' ,@lCourtId, @dptwo, @dpc)                              
						Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc                          
					end                              
					close RecordidCur 
					deallocate RecordidCur                                                               
					Fetch Next from ListdateCur into @ListdateVal                              
				end                                            

			close ListdateCur  
			deallocate ListdateCur 

		-- OUTPUTTING THE DATA TO THE APPLICATION FOR REPROT FILE.....
		Select distinct convert(varchar(20),n.noteid) as letterid, Name, address, city, state, t.zipcode as zip , t.recordid, span, 
		chargewording,  bookingdate, casenumber, clientflag, t.zipspan, mdate as listdate,
		dptwo, t.dpc,  '''+@user+''' as Abb, ''137'' as lettertype, t.isMaxFineAmountNotfound, t.HcccMailerVioDesc, t.MaxFineAmount, t.MaxJailTimePeriod 		
		from #temp1 t , tblletternotes n, @tempBatchIDs tb
		where t.recordid = n.recordid and  n.courtid = 3037 and n.batchid_fk = tb.batchid and n.lettertype = '+convert(varchar(10),@lettertype)+' 
		order by [ZIPSPAN]

		-- GENERATING AND SENDING EMAIL FOR LETTER PRINTING NOTIFICATION........
		declare @lettercount int, @subject varchar(200), @body varchar(400) , @sql varchar(500)
		select @lettercount = count(distinct n.noteid) from tblletternotes n WITH(NOLOCK), @tempBatchIDs b where n.batchid_fk = b.batchid
		select @subject = convert(varchar(20), @lettercount) + '' Harris County Criminal - Criminal week Before Letters Printed''
		select @body = convert(varchar(20), @lettercount) + '' LETTERS ('' + CONVERt(VARCHAR(12), GETDATE()) + '') - ''+ upper( '''+@user+''')
		exec usp_mailer_send_Email @subject, @body

		-- OUTPUTING THE PDF FILE NAME TO THE APPLICATION...
		declare @batchIDs_filname varchar(500)
		set @batchIDs_filname = ''''
		select @batchIDs_filname = @batchIDs_filname + convert(varchar,batchid) + '',''  from @tempBatchIDs
		select isnull(@batchIDs_filname,'''') as batchid

		'

	END
else

	-- IF "MARK LETTER AS SENT" OPTION IS NOT SELECTED....
	-- THEN DO NOT CREATE LETTER HISTORY. JUST OUTPUT THE DATA FOR REPORT FILE 
	-- TO DISPLAY THE LETTER...
	BEGIN
		set @sqlquery2 = @sqlquery2 + '
		Select distinct ''NOT PRINTABLE'' as letterId,  Name, address, city, state, zipcode as zip , recordid, span, 
		chargewording,  bookingdate, casenumber, clientflag,  mdate as listdate,
		dptwo, dpc, zipspan,  '''+@user+''' as Abb, ''137'' as lettertype, isMaxFineAmountNotfound, HcccMailerVioDesc, MaxFineAmount, MaxJailTimePeriod		
		from #temp1 
		order by [zipspan]
		'
	END

-- DROPPING TEMPORARY TABLES ....
set @sqlquery2 = @sqlquery2 + '

drop table #temp 
drop table #temp1
DROP TABLE #tempFineAmount
'
 
                                    
--print  @sqlquery2 --@sqlquery +

-- CONCATENATING THE DYNAMIC SQL
set @sqlquery = @sqlquery + @sqlquery2

-- EXECUTING THE DYNAMIC SQL ....
exec (@sqlquery)



GO
GRANT EXECUTE ON [dbo].[usp_Mailer_Send_HCC_Criminal_WB] TO dbr_webuser
GO
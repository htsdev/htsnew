﻿ 
 /****** Object:  StoredProcedure [dbo].[USP_HTP_GetResponseNotes]    Script Date: 07/16/2008 12:18:48 ******/
/*
 Created By: Noufil Khan 3961 07/16/2008 
 Business Logic: This procedure Returns User Friendly Notes in Reponse of any Error in paying Payments
 Parameter : @Response_code      : Take Response Code Number
			 @responsereasoncode : Take Response Reason Code
 Column : 
			Note : Returns Notes 
*/
GO
--[USP_HTP_GetResponseNotes] 1,'1'
Create procedure [dbo].[USP_HTP_GetResponseNotes] 
@Response_code int,
@responsereasoncode varchar(20)
as

declare @note varchar(5000)

set @note = (Select notes from AuthorizeRSNDetail where Response_code =@Response_code and response_reason_code =@responsereasoncode)


Select isnull(@note,'')as note



go
grant exec on [dbo].[USP_HTP_GetResponseNotes] to dbr_webuser


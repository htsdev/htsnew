 /******
 Business Logic : This report is used to get Continuance flag clients.
    
******/   

ALTER PROCEDURE [dbo].[USP_HTS_CONCENTRATION_REPORT]        
@StartDate datetime,        
@EndDate datetime,        
@Court int,    
@ShowDetail bit,  
@WithoutStatus bit    --khalid 24-10-07  
AS        
        
set @StartDate = convert(varchar,@startdate,101) + ' 00:00:00'        
set @EndDate = convert(varchar,@enddate,101) + ' 23:59:59'        
        
declare @sql varchar(4000)        
        
if(@ShowDetail = 1 and @WithoutStatus=0)    
begin    
set @sql = '        
SELECT     case when tv.CourtID IN(3001,3002,3003) then convert(datetime,convert(varchar,tv.courtdatemain,101)+'' 7:45:00'')    
else tv.courtdatemain end as CourtDateMain, c.ShortName, case len(tv.CourtNumbermain) when 1 then ''0''+tv.CourtNumbermain else tv.CourtNumbermain end as CourtNumbermain, cvs.ShortDescription, COUNT(distinct t.TicketID_PK) AS ClientCount,         
case isnull(f.FirmAbbreviation,''SULL'') when ''SULL'' then ''?'' else f.FirmAbbreviation end as FirmAbbreviation,        
tv.CourtID, cvs.CategoryID        
FROM         tblCourtViolationStatus AS cvs RIGHT OUTER JOIN        
                      tblTicketsViolations AS tv ON cvs.CourtViolationStatusID = tv.CourtViolationStatusIDmain LEFT OUTER JOIN        
                      tblCourts AS c ON tv.CourtID = c.Courtid LEFT OUTER JOIN        
                      tblTickets AS t ON tv.TicketID_PK = t.TicketID_PK LEFT OUTER JOIN        
                      tblFirm AS f ON isnull(tv.CoveringFirmID,3000)  = f.FirmID        
WHERE tv.CourtdateMain BETWEEN '''+convert(varchar,@startdate)+''' AND '''+convert(varchar,@enddate)+''' and t.ActiveFlag = 1        
and (cvs.CategoryID IN (3,4,5))        
GROUP BY  case when tv.CourtID IN(3001,3002,3003) then convert(datetime,convert(varchar,tv.courtdatemain,101)+'' 7:45:00'')    
else tv.courtdatemain end, c.ShortName, tv.CourtNumbermain, cvs.ShortDescription, cvs.CategoryID, f.FirmAbbreviation, tv.CourtID        
 '       
end    
else if(@ShowDetail = 1 and @WithoutStatus=1)    
begin    
set @sql = '        
SELECT     case when tv.CourtID IN(3001,3002,3003) then convert(datetime,convert(varchar,tv.courtdatemain,101)+'' 7:45:00'')    
else tv.courtdatemain end as CourtDateMain, c.ShortName, case len(tv.CourtNumbermain) when 1 then ''0''+tv.CourtNumbermain else tv.CourtNumbermain end as CourtNumbermain, '' '' as ShortDescription, COUNT(distinct t.TicketID_PK) AS ClientCount,0 as CategoryID,          
case isnull(f.FirmAbbreviation,''SULL'') when ''SULL'' then ''?'' else f.FirmAbbreviation end as FirmAbbreviation,        
tv.CourtID, tv.courtnumber         
FROM         tblCourtViolationStatus AS cvs RIGHT OUTER JOIN        
                      tblTicketsViolations AS tv ON cvs.CourtViolationStatusID = tv.CourtViolationStatusIDmain LEFT OUTER JOIN        
                      tblCourts AS c ON tv.CourtID = c.Courtid LEFT OUTER JOIN        
                      tblTickets AS t ON tv.TicketID_PK = t.TicketID_PK LEFT OUTER JOIN        
                      tblFirm AS f ON isnull(tv.CoveringFirmID,3000)  = f.FirmID        
WHERE tv.CourtdateMain BETWEEN '''+convert(varchar,@startdate)+''' AND '''+convert(varchar,@enddate)+''' and t.ActiveFlag = 1        
and (cvs.CategoryID IN (3,4,5))        
GROUP BY  case when tv.CourtID IN(3001,3002,3003) then convert(datetime,convert(varchar,tv.courtdatemain,101)+'' 7:45:00'')    
else tv.courtdatemain end, c.ShortName, tv.CourtNumbermain, f.FirmAbbreviation, tv.CourtID, tv.courtnumber        
 '       
end    
   else  
begin     --Sabir Khan 7979 07/07/2010 check for sugarland court for couurt room number
set @sql = '    
SELECT     case when tv.CourtID IN(3001,3002,3003) then convert(datetime,convert(varchar,tv.courtdatemain,101)+'' 7:45:00'')    
else tv.courtdatemain end as CourtDateMain, c.ShortName, 0 as CourtNumbermain, '' '' as ShortDescription, COUNT(distinct t.TicketID_PK) AS ClientCount,         
'' '' as FirmAbbreviation,        
tv.CourtID, 0 as CategoryID            
FROM         tblCourtViolationStatus AS cvs RIGHT OUTER JOIN        
                      tblTicketsViolations AS tv ON cvs.CourtViolationStatusID = tv.CourtViolationStatusIDmain LEFT OUTER JOIN        
                      tblCourts AS c ON tv.CourtID = c.Courtid LEFT OUTER JOIN        
                      tblTickets AS t ON tv.TicketID_PK = t.TicketID_PK LEFT OUTER JOIN        
                      tblFirm AS f ON isnull(tv.CoveringFirmID,3000)  = f.FirmID        
WHERE tv.CourtdateMain BETWEEN '''+convert(varchar,@startdate)+''' AND '''+convert(varchar,@enddate)+''' and t.ActiveFlag = 1        
and (cvs.CategoryID IN (3,4,5))        
GROUP BY  case when tv.CourtID IN(3001,3002,3003) then convert(datetime,convert(varchar,tv.courtdatemain,101)+'' 7:45:00'')    
else tv.courtdatemain end, c.ShortName,  tv.CourtID         
 '    
end     
        
if(@Court = 1) -- HMC Courts        
 set @sql = @sql + ' HAVING (tv.CourtID IN (3001,3002,3003)) '        
        
else if(@Court = 2) -- HCJP Courts        
 set @sql = @sql + ' HAVING (tv.CourtID BETWEEN 3007 and 3022) '        
        
else if(@Court = 3) -- All non HMC and HCJP        
 set @sql = @sql + ' HAVING (tv.CourtID NOT IN (3001,3002,3003)) and (tv.CourtID NOT BETWEEN 3007 and 3022)  '  

else if(@Court = 4) -- All non HMC  Aziz Task 1711         
 set @sql = @sql + ' HAVING (tv.CourtID NOT IN (3001,3002,3003))   '  

else if(@Court = 5) -- Not HMC & HCCC  Aziz Task 2120         
 set @sql = @sql + ' HAVING (tv.CourtID NOT IN (3001,3002,3003,3037))   '  
      
else if(@Court <> 0) -- Individual Courts        
 set @sql = @sql + ' HAVING (tv.CourtID = '+convert(varchar,@Court) +') '        
        
if(@ShowDetail = 1)    
 set @sql = @sql + ' ORDER BY  case when tv.CourtID IN(3001,3002,3003) then convert(datetime,convert(varchar,tv.courtdatemain,101)+'' 7:45:00'')    
else tv.courtdatemain end, tv.CourtID, convert(varchar,tv.CourtNumbermain) '        
else    
 set @sql = @sql + ' ORDER BY case when tv.CourtID IN(3001,3002,3003) then convert(datetime,convert(varchar,tv.courtdatemain,101)+'' 7:45:00'')    
else tv.courtdatemain end, tv.CourtID '    
        
        
        
--print @sql       
exec (@sql)    





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


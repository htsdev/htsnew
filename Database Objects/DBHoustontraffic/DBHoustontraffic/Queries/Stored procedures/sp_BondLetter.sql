﻿/******    
ALTER by:  Syed Muhammad Ozair  
  
Business Logic : this procedure is used to get the details which will be displayed on Bond letter.  
  
List of Parameters:   
 @TicketIDList : ticke id of the case  
 @empid : employee id of currently logged in user  
 @DocumentBatchID : document/Report id generated to print  
  
List of Columns:    
 Counter :   
 TicketID_PK :   
 RefCaseNumber :   
 SequenceNumber :   
 Description :   
 casenumassignedbycourt :                     
 BondAmount :   
 Firstname :   
 MiddleName :   
 Lastname :   
 Address1 :   
 Address2 :   
 Midnum :   
 City :   
 State :   
 Zip :                        
 DLNumber :   
 DLState :   
 DOB :   
 SSNumber :   
 Race :   
 gender :   
 Height :   
 Weight :   
 HairColor :   
 Eyes :   
 languagespeak :   
 Crt_CourtName :   
 Crt_Address :   
 Crt_Address2 :   
 Crt_City :   
 Crt_State :   
 Crt_Zip :   
 totalBondAmount :   
 dup :   
 DBID : document batch id
 violcnt: total number of violations  
  
******/     
    
      
ALTER PROCEDURE [dbo].[sp_BondLetter]  --249045,3991 
  
@TicketIDList int,                    
@empid int,  
@DocumentBatchID int = 0  
                    
AS                    
                    
SET NOCOUNT ON                      
Declare @SQLString nvarchar(4000)                      
                    
                    
SELECT  dbo.tblTicketsViolations.TicketID_PK,
--Yasir Kamal 5627 03/06/2009 Bond Letter Changes     
SUM(isnull(case when dbo.tblTicketsViolations.courtid in (3001,3002,3003) then 0 else dbo.tblTicketsViolations.bondamount end,0)) as BondAmount                      
--5627 end
 into #temp1                       
FROM dbo.tblTicketsViolations INNER JOIN                       
dbo.tblViolations ON dbo.tblTicketsViolations.violationnumber_pk = dbo.tblViolations.ViolationNumber_PK                       
  WHERE     (dbo.tblViolations.Violationtype  not in (1))                       
 and (dbo.tblTicketsViolations.underlyingbondflag=1 or dbo.tblTicketsViolations.courtviolationstatusid=135) --added by ozair for bug#1224                  
  and dbo.tblTicketsViolations.TicketID_PK = @TicketIDList                  
                  
 --AND  refcasenumber like 'f%'                     
 GROUP BY dbo.tblTicketsViolations.TicketID_PK                    
                      
                     
-- Agha Usman 2664 06/04/2008
--Yasir Kamal 7058 07/12/2009 get violation type.
 SELECT  DISTINCT   dbo.tblTickets.TicketID_PK, dbo.tblviolations.violationtype,
 -- tahir 4281 06/20/2008
 -- to display cause numbers for HCJP courts...
  dbo.tblCourts.CourtCategorynum,
  dbo.tblTicketsViolations.RefCaseNumber, 
   -- Babar Ahmad 9660 09/07/2011 added ClientPhoneNumber, CountyName, PrecinctID and causenumbers fields.
  [dbo].[fn_HTP_Get_ContactNumber](dbo.tblTickets.TicketID_PK) AS ClientPhoneNumber, 
  mc.CountyName,
   dbo.tblCourts.PrecinctID,
  [dbo].[Fn_HTP_Get_AllCauseNumbersWithComma](dbo.tblTicketsViolations.TicketID_PK) AS causenumbers,
 '0' as SequenceNumber, dbo.tblViolations.Description,                       
--Yasir Kamal 5627 03/06/2009 Bond Letter Changes     
 isnull(case when dbo.tblTicketsViolations.courtid in (3001,3002,3003) then  0 else dbo.tblTicketsViolations.bondamount end,0) as BondAmount,
--5627 end
    dbo.tblTickets.Firstname, dbo.tblTickets.MiddleName, dbo.tblTickets.Lastname,                       
       
 --added by asghar      
  (case when dbo.tblTickets.FirmID =3000 then dbo.tblTickets.Address1 else (select Address from dbo.tblfirm where dbo.tblfirm.FirmID = dbo.tblTickets.FirmID) end) as Address1,       
  (case when dbo.tblTickets.FirmID =3000 then dbo.tblTickets.Address2 else (select Address2 from dbo.tblfirm where dbo.tblfirm.FirmID = dbo.tblTickets.FirmID) end) as Address2,       
  --dbo.tblTickets.Address1, dbo.tblTickets.Address2,       
  dbo.tblTickets.Midnum,        
      
  (case when dbo.tblTickets.FirmID =3000 then dbo.tblTickets.City else (select City from dbo.tblfirm where FirmID =dbo.tblTickets.FirmID) end) as city,      
  (case when dbo.tblTickets.FirmID =3000 then tblState_1.State else(select tblState_1.State where tblState_1.StateID =(select State from tblfirm where FirmID =dbo.tblTickets.FirmID)) end ) as State,          
  (case when dbo.tblTickets.FirmID =3000 then dbo.tblTickets.Zip else (select Zip from tblfirm where FirmID =dbo.tblTickets.FirmID) end ) as Zip,         
      
  --dbo.tblTickets.City, tblState_1.State, dbo.tblTickets.Zip,                       
  dbo.tblTickets.DLNumber, dbo.tblState.State AS DLState, dbo.tblTickets.DOB, dbo.tblTickets.SSNumber, dbo.tblTickets.Race,                       
  dbo.tblTickets.gender, dbo.tblTickets.Height, dbo.tblTickets.Weight, dbo.tblTickets.HairColor, dbo.tblTickets.Eyes,  dbo.tblTickets.languagespeak,                       
  dbo.tblCourts.CourtName as Crt_CourtName , dbo.tblCourts.Address as Crt_Address , dbo.tblCourts.Address2 as Crt_Address2,                       
  dbo.tblCourts.City as Crt_City , tblState_2.State as Crt_State , dbo.tblCourts.Zip as Crt_Zip, dbo.tblTicketsViolations.casenumassignedbycourt                       
  into #tbltemp                       
  FROM dbo.tblTicketsViolations INNER JOIN                       
  dbo.tblViolations ON dbo.tblViolations.ViolationNumber_PK = dbo.tblTicketsViolations.ViolationNumber_PK LEFT OUTER JOIN                       
  dbo.tblTickets ON dbo.tblTicketsViolations.TicketID_PK = dbo.tblTickets.TicketID_PK LEFT OUTER JOIN   dbo.tblState ON dbo.tblTickets.DLState = dbo.tblState.StateID  LEFT OUTER JOIN                       
  dbo.tblState tblState_1 ON dbo.tblTickets.Stateid_FK = tblState_1.StateID LEFT OUTER JOIN                       
  dbo.tblCourts ON dbo.tblCourts.courtid = dbo.tblTicketsViolations.courtid LEFT OUTER JOIN 
  tblCourtCategories cc ON dbo.tblcourts.courtcategorynum = cc.CourtCategorynum LEFT OUTER JOIN
  tbl_Mailer_County mc ON cc.CountyID_FK = mc.CountyID LEFT OUTER JOIN                     
  dbo.tblState tblState_2 ON dbo.tblCourts.State = tblState_2.StateID                          
  WHERE (dbo.tblViolations.ViolationType not in (1))         
  and dbo.tblTickets.TicketID_PK = @TicketIDList                    
  and (dbo.tblTicketsViolations.underlyingbondflag=1 or dbo.tblTicketsViolations.courtviolationstatusid=135) --added by ozair for bug#1224                  
         and dbo.tblTicketsViolations.courtviolationstatusid<>80   --newly added                    
 --AND  refcasenumber like 'f%'                     
  ORDER BY dbo.tblTickets.TicketID_PK, [RefCaseNumber]
--Agha Usman 2664 06/04/2008                       
-- Babar Ahmad 9660 09/07/2011 added ClientPhoneNumber, CountyName, PrecinctID and causenumbers fields.                      
select  a.TicketID_PK,a.violationtype, a.courtcategorynum, a.RefCaseNumber,  a.ClientPhoneNumber,a.CountyName, a.PrecinctID, a.causenumbers, a.SequenceNumber, a.[Description],  casenumassignedbycourt  ,                   
  a.BondAmount, a.Firstname, a.MiddleName, a.Lastname,  a.Address1, a.Address2, a.Midnum, a.City, a.State, a.Zip,                       
  a.DLNumber, a.DLState, a.DOB, a.SSNumber, a.Race, a.gender, a.Height, a.Weight, a.HairColor, a.Eyes,  a.languagespeak,                       
  a.Crt_CourtName , a.Crt_Address , a.Crt_Address2, a.Crt_City , a.Crt_State , a.Crt_Zip, b.BondAmount as totalBondAmount                    
  into #TbltempBond                    
 from #tbltemp a, #temp1 b where a.TicketID_PK = b.TicketID_PK                    
                    
alter table #tbltempbond                    
add dup varchar(25)                    
                    
alter table #tbltempbond                    
add Counter int                    
                    
                  
                    
                    
select * into #tbltempbond2                    
from #tbltempbond                    
                    
select * into #tbltempbond1                    
from #tbltempbond2                    
                    
                    
update #tbltempbond2 set dup = '*'                    
                    
select Top 1 *                     
into #tbltempbond3                    
from #tbltempbond2                    
                    
update #tbltempbond3 set dup = ''                    
                    
update #tbltempbond2 set Counter = 2                    
update #tbltempbond3 set  Counter = 3 ,  Description = '' ,  BondAmount = 0 , totalBondAmount = 0, TicketID_PK = Null, RefCaseNumber = Null ,      
 casenumassignedbycourt = null                  
                    
update #tbltempbond1 set Counter = 1                    
                    
-- Babar Ahmad 9660 09/07/2011 added ClientPhoneNumber, CountyName, PrecinctID and causenumbers fields.                    
insert into #tbltempbond2 (TicketID_PK,violationtype, courtcategorynum, RefCaseNumber, ClientPhoneNumber,CountyName, PrecinctID, causenumbers,  SequenceNumber, [Description],  casenumassignedbycourt,                     
  BondAmount, Firstname, MiddleName, Lastname,  Address1, Address2, Midnum, City, State, Zip,                       
  DLNumber, DLState, DOB, SSNumber, Race, gender, Height, Weight, HairColor, Eyes,  languagespeak,                       
  Crt_CourtName , Crt_Address , Crt_Address2, Crt_City , Crt_State , Crt_Zip, totalBondAmount, dup,Counter)  
  -- Babar Ahmad 9660 09/07/2011 added ClientPhoneNumber, CountyName, PrecinctID and causenumbers fields.                  
select TicketID_PK,violationtype, courtcategorynum, RefCaseNumber, ClientPhoneNumber,CountyName, PrecinctID, #tbltempbond1.causenumbers, SequenceNumber, [Description],   casenumassignedbycourt ,                   
  BondAmount, Firstname, MiddleName, Lastname,  Address1, Address2, Midnum, City, State, Zip,                       
  DLNumber, DLState, DOB, SSNumber, Race, gender, Height, Weight, HairColor, Eyes,  languagespeak,                       
  Crt_CourtName , Crt_Address , Crt_Address2, Crt_City , Crt_State , Crt_Zip, totalBondAmount, dup,Counter from  #tbltempbond1                    
union all   
-- Babar Ahmad 9660 09/07/2011 added ClientPhoneNumber, CountyName, PrecinctID and causenumbers fields.                 
select TicketID_PK,violationtype, courtcategorynum, RefCaseNumber, ClientPhoneNumber,CountyName, PrecinctID, #tbltempbond3.causenumbers, SequenceNumber, [Description],    casenumassignedbycourt ,                  
  BondAmount, Firstname, MiddleName, Lastname,  Address1, Address2, Midnum, City, State, Zip,                       
  DLNumber, DLState, DOB, SSNumber, Race, gender, Height, Weight, HairColor, Eyes,  languagespeak,            --Yasir Kamal 7058 07/12/2009 dont display empty page if court is HCJP.           
  Crt_CourtName , Crt_Address , Crt_Address2, Crt_City , Crt_State , Crt_Zip, totalBondAmount, dup, Counter from #tbltempbond3 WHERE courtcategorynum <> 2                    
                    
-- Babar Ahmad 9660 09/07/2011 added ClientPhoneNumber, CountyName, PrecinctID and causenumbers fields.      
select Counter, TicketID_PK,violationtype, courtcategorynum, RefCaseNumber, ClientPhoneNumber,CountyName, PrecinctID, causenumbers, SequenceNumber, [Description],   casenumassignedbycourt,                    
  BondAmount, Firstname, MiddleName, Lastname,  Address1, Address2, Midnum, City, State, Zip,                       
  DLNumber, DLState, DOB, SSNumber, Race, gender, Height, Weight, (case HairColor when '--Choose--' then 'N/A' else HairColor  end) as HairColor, (case Eyes when '--Choose--' then 'N/A' else Eyes  end) as Eyes,  languagespeak,           
  Crt_CourtName , Crt_Address , Crt_Address2, Crt_City , Crt_State , Crt_Zip, totalBondAmount, dup  
  --ozair 3643 on 05/01/2008  
  ,@DocumentBatchID as DBID  
  --end ozair 3643  
  --ozair 4343 on 07/07/2008
  ,(select count(te.RefCaseNumber) from #tbltempbond2 te where te.counter=1) as violcnt
  --end ozair 4343
from #tbltempbond2
--Fahad 5820 04/24/2009 where Clause is added
WHERE ISNULL(dup,'') <> '*'                                    
ORDER BY COUNTER                    
      
  
  
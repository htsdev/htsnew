/************************************************************
 * Code formatted by SoftTree SQL Assistant © v4.0.34
 * Time: 7/24/2012 5:06:02 PM
 ************************************************************/

/****************

Alter by:	Hafiz Khalid Nawaz
Date:		07/24/2012

Business Logic:
	The stored procedure is used by Houtson Traffic Program to update the 
	Cleint's/Prospect's personal, contact, fee and other information....

Input Parameters:
	@TicketID_PK:
	@Firstname:
	@MiddleName:
	@Lastname:
	@DOB:
	@Contact1:
	@ContactType1:
	@LanguageSpeak:
	@GeneralComments:
	@BondFlag:
	@AccidentFlag:
	@CDLFlag:
	@Activeflag:
	@SpeedingID:
	@FirmID:
	@EmpID:
	@LateFlag:
	@CaseTypeID:
	@HasPaymentPlan:
	@HaveALRPriorAttorney: -- Waqas 5864 07/08/2009 ALR Criminal changes
	@ALRPriorAttorneyType:
	@ALRPriorAttorneyName:
	@CaseSummaryComments
	@ALROfficerName
    @ALROfficerbadge
    @ALRPrecinct
    @ALROfficerAddress
    @ALROfficerCity
    @ALROfficerStateID
    @ALROfficerZip
    @ALROfficerContactNumber
    @ALRIsIntoxilyzerTakenFlag
    @ALRIntoxilyzerResult
    @ALRBTOFirstName
    @ALRBTOLastName
    @ALRBTOBTSSameFlag
    @ALRBTSFirstName
    @ALRBTSLastName
    @ALRArrestingAgency
    @ALRMileage
    @ALRObservingOfficerName,
	@ALRObservingOfficerBadgeNo,
	@ALRObservingOfficerPrecinct,
	@ALRObservingOfficerAddress,
	@ALRObservingOfficerCity,
	@ALRObservingOfficerState,
	@ALRObservingOfficerZip,
	@ALRObservingOfficerContact,
	@ALRObservingOfficerArrestingAgency,
	@ALRObservingOfficerMileage,
	@IsALRHearingRequired,
	@ALRHearingRequiredAnswer
	@Occupation
	@Employer
	@IsUnemployed
	@VehicleType 

Output Columns:
	N/A

***************/    
                  
ALTER PROCEDURE [dbo].[Usp_HTS_UpdateCaseInfo]
	@TicketID_PK
AS
	INT, 
	@Firstname AS VARCHAR(20), 
	@MiddleName AS VARCHAR(1), 
	@Lastname AS VARCHAR(20), 
	@DOB AS DATETIME, 
	@Contact1 AS VARCHAR(15), 
	@ContactType1 AS INT, 
	@LanguageSpeak VARCHAR(30), 
	@GeneralComments VARCHAR(MAX), -- Adil - For 2585    
	@BondFlag TINYINT, 
	@AccidentFlag TINYINT, 
	@CDLFlag TINYINT, 
	--@AdsourceID tinyint,                    
	@Activeflag TINYINT, 
	@SpeedingID INT, 
	@FirmID INT, 
	@EmpID INT, 
	@LateFlag TINYINT, 
	@CaseTypeID INT, --Sabir Khan 4636 08/26/2008  Update Case Type               
	@HasPaymentPlan BIT = 0, -- tahir 4786 10/08/2008,
	                         --waqas 6342 08/25/2009 changes for criminal and family
	@HavePriorAttorney BIT, -- Waqas 5864 06/26/2009 ALR Criminal changes
	@PriorAttorneyType VARCHAR(MAX),
	@PriorAttorneyName VARCHAR(MAX),
	@CaseSummaryComments VARCHAR(MAX),
	@ALROfficerName VARCHAR(MAX),
	@ALROfficerbadge VARCHAR(MAX),
	@ALRPrecinct VARCHAR(MAX),
	@ALROfficerAddress VARCHAR(MAX),
	@ALROfficerCity VARCHAR(MAX),
	@ALROfficerStateID INT,
	@ALROfficerZip VARCHAR(MAX),
	@ALROfficerContactNumber VARCHAR(MAX),
	@ALRIsIntoxilyzerTakenFlag TINYINT,
	@ALRIntoxilyzerResult VARCHAR(MAX),
	@ALRBTOFirstName VARCHAR(MAX),
	@ALRBTOLastName VARCHAR(MAX),
	@ALRBTOBTSSameFlag BIT = NULL,
	@ALRBTSFirstName VARCHAR(MAX),
	@ALRBTSLastName VARCHAR(MAX),
	@ALRArrestingAgency VARCHAR(MAX),
	@ALRMileage FLOAT,
	--waqas 6342 08/13/2009 alr observing officer
	--@ALRObservingOfficerName VARCHAR(MAX),
	--@ALRObservingOfficerBadgeNo VARCHAR(MAX),
	--@ALRObservingOfficerPrecinct VARCHAR(MAX),
	--@ALRObservingOfficerAddress VARCHAR(MAX),
	--@ALRObservingOfficerCity VARCHAR(MAX),
	--@ALRObservingOfficerState int,
	--@ALRObservingOfficerZip VARCHAR(MAX),
	--@ALRObservingOfficerContact VARCHAR(MAX),
	--@ALRObservingOfficerArrestingAgency VARCHAR(MAX),
	--@ALRObservingOfficerMileage FLOAT,
	--@IsALRHearingRequired bil = null,
	--@ALRHearingRequiredAnswer  VARCHAR(MAX)
	@IsALRArrestingObservingSame BIT = NULL,
	@ALRObservingOfficerName VARCHAR(MAX) = NULL,
	@ALRObservingOfficerBadgeNo VARCHAR(MAX) = NULL,
	@ALRObservingOfficerPrecinct VARCHAR(MAX) = NULL,
	@ALRObservingOfficerAddress VARCHAR(MAX) = NULL,
	@ALRObservingOfficerCity VARCHAR(MAX) = NULL,
	@ALRObservingOfficerState INT = NULL,
	@ALRObservingOfficerZip VARCHAR(MAX) = NULL,
	@ALRObservingOfficerContact VARCHAR(MAX) = NULL,
	@ALRObservingOfficerArrestingAgency VARCHAR(MAX) = NULL,
	@ALRObservingOfficerMileage FLOAT = NULL,
	@IsALRHearingRequired BIT = NULL,
	@ALRHearingRequiredAnswer VARCHAR(MAX) = NULL,
	
	--Waqas 6599 09/19/2009
	@Occupation VARCHAR(100) = NULL,
	@Employer VARCHAR(50) = NULL,
	@IsUnemployed BIT = NULL,
	@VehicleType TINYINT = NULL,
	-- Abbas Qamar 04-10-2012 10114 Declaring the varaibles for HMCCC,Montgomery,Fortbend
	@DLNumber VARCHAR(30),
	@NoDl INT,
	@SSNumber VARCHAR(12),
	@SSNTypeId INT,
	@SSNOtherQuestion VARCHAR(200),
	@DLState INT, 
	-- End 10114
	-- Rab Nawaz Khan 10330 07/03/2012 parameter Added for the Arr status and same court date
	@ArrStatusFlag BIT
	
	AS                     
	
	
	
	DECLARE @dFirstname VARCHAR(20)                                             
	DECLARE @dLastname VARCHAR(20)                                             
	DECLARE @dMiddleName VARCHAR(6)                                              
	DECLARE @dDOB DATETIME    
	DECLARE @isManual BIT    
	
	SELECT @isManual = isProblemClientManual,
	       @dFirstName = FirstName,
	       @dLastname = LastName,
	       @dMiddleName = MiddleName,
	       @dDOB = DOB
	FROM   tbltickets
	WHERE  ticketId_Pk = @TicketID_PK 
	
	--Waqas 5864 07/16/2009 ALR Changes
	DECLARE @OldCaseSummaryComments VARCHAR(MAX)
	SET @OldCaseSummaryComments = (
	        SELECT ISNULL(CaseSummaryComments, '')
	        FROM   tbltickets
	        WHERE  ticketid_pk = @TicketID_PK
	    )
	
	--Waqas 6894 10/30/2009 Inserting Occupation and employer.
	DECLARE @OccID INT
	IF EXISTS(
	       SELECT *
	       FROM   Occupation
	       WHERE  Occupation = @Occupation
	   )
	BEGIN
	    SET @OccID = (
	            SELECT TOP 1 OccupationID
	            FROM   Occupation
	            WHERE  Occupation = @Occupation
	        )
	END
	ELSE
	BEGIN
	    INSERT INTO Occupation
	      (
	        Occupation
	      )
	    VALUES
	      (
	        @Occupation
	      )
	    SET @OccID = (
	            SELECT TOP 1 OccupationID
	            FROM   Occupation
	            WHERE  Occupation = @Occupation
	        )
	END
	
	DECLARE @EmplrID INT
	IF EXISTS(
	       SELECT *
	       FROM   Employer
	       WHERE  Employer = @Employer
	   )
	BEGIN
	    SET @EmplrID = (
	            SELECT TOP 1 EmployerID
	            FROM   Employer
	            WHERE  Employer = @Employer
	        )
	END
	ELSE
	BEGIN
	    INSERT INTO Employer
	      (
	        Employer
	      )
	    VALUES
	      (
	        @Employer
	      )
	    SET @EmplrID = (
	            SELECT TOP 1 EmployerID
	            FROM   Employer
	            WHERE  Employer = @Employer
	        )
	END
	
	--- Abbas Qamar 10114 Checking the Criminal Courts for HMCC,Fortbend,Montgomrey
	DECLARE @IsCriminalCount INT
	SET @IsCriminalCount = (
	        SELECT COUNT(*)
	        FROM   tblticketsviolations v
	               JOIN tblcourts c
	                    ON  v.courtid = c.courtid
	        WHERE  c.iscriminalcourt = 1
	               AND c.Courtid IN (3037, 3043, 3036)
	               AND c.InActiveFlag = 0
	               AND v.ticketid_pk = @TicketID_PK
	    ) 
	
	IF (@IsCriminalCount > 0)
	BEGIN
	    UPDATE tbltickets
	    SET    DLNumber = @DLNumber,
	           NoDL = @NoDl,
	           SSNumber = @SSNumber,
	           SSNTypeId = @SSNTypeId,
	           SSNOtherQuestion = @SSNOtherQuestion,
	           DLState = @DLState
	    WHERE  TicketID_PK = @TicketID_PK
	END
	
	
	-- End 10114
	
	--Hafiz 10288 07/24/2012 added if condition
	IF @IsALRHearingRequired = 1
	BEGIN
	    UPDATE tblTickets
	    SET    Firstname = @Firstname,
	           Middlename = @MiddleName,
	           Lastname = @Lastname,
	           DOB = @DOB,
	           Contact1 = @Contact1,
	           ContactType1 = @ContactType1,
	           LanguageSpeak = @LanguageSpeak,
	           BondFlag = @BondFlag,
	           BondsRequiredflag = @BondFlag,
	           AccidentFlag = @AccidentFlag,
	           CDLFlag = @CDLFlag,
	           SpeedingID = @SpeedingID,
	           FirmID = @FirmID,
	           EmployeeIDUpdate = @EmpID,
	           LateFlag = @LateFlag,
	           CaseTypeID = @CaseTypeID,	--Sabir Khan 4636 08/26/2008  Update Case Type             
	           HasPaymentPlan = @HasPaymentPlan,	-- tahir 4786 10/08/2008
	                                            	--Waqas 6342 08/24/2009 New changes for all criminal and family cases
	           HavePriorAttorney = @HavePriorAttorney,	-- Waqas 5864 06/26/2009 ALR Criminal Changes.
	           PriorAttorneyType = @PriorAttorneyType,
	           PriorAttorneyName = @PriorAttorneyName,
	           CaseSummaryComments = @CaseSummaryComments,
	           --Hafiz 10288 07/23/2012 On matter page we don't update these info moved to contact page
	           --	       ALROfficerName = @ALROfficerName,
	           --	       ALROfficerbadge = @ALROfficerbadge,
	           --	       ALRPrecinct = @ALRPrecinct,
	           --	       ALROfficerAddress = @ALROfficerAddress,
	           --	       ALROfficerCity = @ALROfficerCity,
	           --	       ALROfficerStateID = @ALROfficerStateID,
	           --	       ALROfficerZip = @ALROfficerZip,
	           --	       ALROfficerContactNumber = @ALROfficerContactNumber,
	           ALRIsIntoxilyzerTakenFlag = @ALRIsIntoxilyzerTakenFlag,
	           --	       ALRIntoxilyzerResult = @ALRIntoxilyzerResult,
	           --	       ALRBTOFirstName = @ALRBTOFirstName,
	           --	       ALRBTOLastName = @ALRBTOLastName,
	           --	       ALRBTOBTSSameFlag = @ALRBTOBTSSameFlag,
	           --	       ALRBTSFirstName = @ALRBTSFirstName,
	           --	       ALRBTSLastName = @ALRBTSLastName,
	           --	       ALRMileage = @ALRMileage,
	           --waqas 6342 08/13/2009 alr observing officer
	           --	       IsALRArrestingObservingSame = @IsALRArrestingObservingSame,
	           --	       ALRObservingOfficerName = @ALRObservingOfficerName,
	           --	       ALRObservingOfficerBadgeNo = @ALRObservingOfficerBadgeNo,
	           --	       ALRObservingOfficerPrecinct = @ALRObservingOfficerPrecinct,
	           --	       ALRObservingOfficerAddress = @ALRObservingOfficerAddress,
	           --	       ALRObservingOfficerCity = @ALRObservingOfficerCity,
	           --	       ALRObservingOfficerState = @ALRObservingOfficerState,
	           --	       ALRObservingOfficerZip = @ALRObservingOfficerZip,
	           --	       ALRObservingOfficerContact = @ALRObservingOfficerContact,
	           --	       ALRObservingOfficerArrestingAgency = @ALRObservingOfficerArrestingAgency,
	           --	       ALRObservingOfficerMileage = @ALRObservingOfficerMileage,
	           IsALRHearingRequired = @IsALRHearingRequired,
	           --	       ALRHearingRequiredAnswer = @ALRHearingRequiredAnswer,
	           --End 10288
	           --Waqas 6599 09/19/2009 occupation, employer, vehicle type
	           --Waqas 6894 10/30/2009 inserting records into occupation id and employer id
	           OccupationID = @OccID,
	           EmployerID = @EmplrID,
	           IsUnemployed = @IsUnemployed,
	           VehicleType = @VehicleType,
			   ArraignmentStatusFlag = @ArrStatusFlag -- Rab Nawaz Khan 10330 07/03/2012 Flag Added against the Client if client hire us on same court date with Arr status. . . 
	    WHERE  ticketid_pk = @ticketid_pk
	END
	ELSE
	BEGIN
	    UPDATE tblTickets
	    SET    Firstname = @Firstname,
	           Middlename = @MiddleName,
	           Lastname = @Lastname,
	           DOB = @DOB,
	           Contact1 = @Contact1,
	           ContactType1 = @ContactType1,
	           LanguageSpeak = @LanguageSpeak,
	           BondFlag = @BondFlag,
	           BondsRequiredflag = @BondFlag,
	           AccidentFlag = @AccidentFlag,
	           CDLFlag = @CDLFlag,
	           SpeedingID = @SpeedingID,
	           FirmID = @FirmID,
	           EmployeeIDUpdate = @EmpID,
	           LateFlag = @LateFlag,
	           CaseTypeID = @CaseTypeID,	--Sabir Khan 4636 08/26/2008  Update Case Type             
	           HasPaymentPlan = @HasPaymentPlan,	-- tahir 4786 10/08/2008
	                                            	--Waqas 6342 08/24/2009 New changes for all criminal and family cases
	           HavePriorAttorney = @HavePriorAttorney,	-- Waqas 5864 06/26/2009 ALR Criminal Changes.
	           PriorAttorneyType = @PriorAttorneyType,
	           PriorAttorneyName = @PriorAttorneyName,
	           CaseSummaryComments = @CaseSummaryComments,
	           ALROfficerName = @ALROfficerName,
	           ALROfficerbadge = @ALROfficerbadge,
	           ALRPrecinct = @ALRPrecinct,
	           ALROfficerAddress = @ALROfficerAddress,
	           ALROfficerCity = @ALROfficerCity,
	           ALROfficerStateID = @ALROfficerStateID,
	           ALROfficerZip = @ALROfficerZip,
	           ALROfficerContactNumber = @ALROfficerContactNumber,
	           ALRIsIntoxilyzerTakenFlag = @ALRIsIntoxilyzerTakenFlag,
	           ALRIntoxilyzerResult = @ALRIntoxilyzerResult,
	           ALRBTOFirstName = @ALRBTOFirstName,
	           ALRBTOLastName = @ALRBTOLastName,
	           ALRBTOBTSSameFlag = NULL, --@ALRBTOBTSSameFlag,
	           ALRBTSFirstName = @ALRBTSFirstName,
	           ALRBTSLastName = @ALRBTSLastName,
	           ALRMileage = @ALRMileage,
	           --waqas 6342 08/13/2009 alr observing officer
	           IsALRArrestingObservingSame = @IsALRArrestingObservingSame,
	           ALRObservingOfficerName = @ALRObservingOfficerName,
	           ALRObservingOfficerBadgeNo = @ALRObservingOfficerBadgeNo,
	           ALRObservingOfficerPrecinct = @ALRObservingOfficerPrecinct,
	           ALRObservingOfficerAddress = @ALRObservingOfficerAddress,
	           ALRObservingOfficerCity = @ALRObservingOfficerCity,
	           ALRObservingOfficerState = @ALRObservingOfficerState,
	           ALRObservingOfficerZip = @ALRObservingOfficerZip,
	           ALRObservingOfficerContact = @ALRObservingOfficerContact,
	           ALRObservingOfficerArrestingAgency = @ALRObservingOfficerArrestingAgency,
	           ALRObservingOfficerMileage = @ALRObservingOfficerMileage,
	           IsALRHearingRequired = @IsALRHearingRequired,
	           ALRHearingRequiredAnswer = @ALRHearingRequiredAnswer,
	           --Waqas 6599 09/19/2009 occupation, employer, vehicle type
	           --Waqas 6894 10/30/2009 inserting records into occupation id and employer id
	           OccupationID = @OccID,
	           EmployerID = @EmplrID,
	           IsUnemployed = @IsUnemployed,
	           VehicleType = @VehicleType
	    WHERE  ticketid_pk = @ticketid_pk
	END
	--END 10288 07/24/2012 
	--Haris Ahmed 10291 07/23/2012 Change name in Non Client Database as well
	UPDATE tblTicketsArchive
	SET    FirstName = @Firstname,
	       LastName = @Lastname
	WHERE  FirstName = @dFirstname
	       AND LastName = @dLastname
	       AND DOB = @dDOB
	
	
	--Asad Ali 8153 09/09/2010 checking if AlR hearing  is required then insert blank violation 
	IF (@IsALRHearingRequired = 1)
	BEGIN
	    DECLARE @CourtID INT 
	    DECLARE @AssociatedALRCourtID INT 
	    DECLARE @Note VARCHAR(MAX)
	    DECLARE @ArrestDate DATETIME
	    
	    SELECT TOP 1 @CourtID = ttv.CourtID,
	           @ArrestDate = ttv.ArrestDate
	    FROM   tblTicketsViolations ttv
	    WHERE  ttv.TicketID_PK = @ticketid_pk
	    ORDER BY
	           ttv.ViolationRecDate DESC
	    
	    SELECT @AssociatedALRCourtID = tc.AssociatedALRCourtID
	    FROM   tblCourts tc
	    WHERE  tc.Courtid = @CourtID --AND tc.ALRProcess=0
	    
	    
	    IF (@AssociatedALRCourtID IS NOT NULL)
	    BEGIN
	        IF NOT EXISTS(
	               SELECT *
	               FROM   tblTicketsViolations ttv
	               WHERE  ttv.TicketID_PK = @ticketid_pk
	                      AND ttv.CourtViolationStatusID = 237
	                      AND ttv.CourtID = @AssociatedALRCourtID
	           )
	        BEGIN
	            INSERT INTO tblTicketsViolations
	              (
	                [TicketID_PK],
	                ViolationNumber_PK,
	                [casenumassignedbycourt],
	                [CourtViolationStatusID],
	                CourtViolationStatusIDmain,
	                CourtViolationStatusIDScan,
	                [CourtID],
	                CourtDate,
	                CourtDateMain,
	                CourtDateScan,
	                [VEmployeeID],
	                ViolationRecDate,
	                CourtNumber,
	                CourtNumbermain,
	                CourtNumberscan,
	                ArrestDate
	              )
	            VALUES
	              (
	                @ticketid_pk,	--TicketID_PK
	                16159,	--ViolationNumber_PK
	                NULL,	--[casenumassignedbycourt]
	                237,	--[CourtViolationStatusID]
	                237,	--CourtViolationStatusIDmain
	                237,	--CourtViolationStatusIDScan
	                @AssociatedALRCourtID,	--[CourtID]
	                CONVERT(VARCHAR(10), GETDATE(), 101) + ' 8:00AM',	--CourtDate
	                CONVERT(VARCHAR(10), GETDATE(), 101) + ' 8:00AM',	--CourtDateMain
	                CONVERT(VARCHAR(10), GETDATE(), 101) + ' 8:00AM',	--CourtDateScan
	                @EmpID,	--VEmployeeID
	                GETDATE(),	--ViolationRecDate
	                0,	--CourtNumber
	                0,	--CourtNumbermain
	                0,	--CourtNumberscan
	                @ArrestDate --ArrestDate
	              )
	            
	            
	            --Convert(varchar(10), GETDATE(),101) + ' 8:00AM'
	            /*CourtDate,
	            CourtDateMain,
	            CourtDateScan,
	            CourtViolationStatusID,
	            CourtViolationStatusIDmain,
	            CourtViolationStatusIDScan,
	            CourtNumber,
	            CourtNumbermain,
	            CourtNumberscan
	            */
	            
	            SET @Note = (
	                    SELECT UPPER('New Violation Added: Case # ') +
	                           ISNULL(ttv.casenumassignedbycourt, '[N/A]') +
	                           ': ' +
	                           UPPER(tc.ShortName) + ': ' +
	                           UPPER(tv.ShortDesc) + ': ' +
	                           CONVERT(VARCHAR(10), ISNULL(ttv.CourtDateMain, ''), 101) 
	                           + ' #' + CONVERT(VARCHAR(10), ISNULL(CourtNumber, '')) 
	                           +
	                           ' @' + LTRIM(
	                               RIGHT(
	                                   CONVERT(VARCHAR(30), ISNULL(ttv.CourtDateMain, ''), 100),
	                                   8
	                               )
	                           ) +
	                           ' (' + UPPER(ISNULL(tcvs.ShortDescription, '')) +
	                           ')'
	                    FROM   tblTicketsViolations ttv
	                           INNER JOIN tblViolations tv
	                                ON  tv.ViolationNumber_PK = ttv.ViolationNumber_PK
	                           INNER JOIN tblCourts tc
	                                ON  tc.Courtid = ttv.CourtID
	                           INNER JOIN tblCourtViolationStatus tcvs
	                                ON  tcvs.CourtViolationStatusID = ttv.CourtViolationStatusID
	                    WHERE  ttv.TicketsViolationID = SCOPE_IDENTITY()
	                           --SCOPE_IDENTITY()
	                )
	            
	            
	            
	            INSERT INTO tblTicketsNotes
	              (
	                TicketID,
	                [Subject],
	                RecDate,
	                EmployeeID
	              )
	            VALUES
	              (
	                @ticketid_pk,
	                @Note,
	                GETDATE(),
	                @EmpID
	              )
	        END
	    END
	END
	
	
	--Waqas 5864 07/16/2009 ALR Changes 
	IF LEN(@CaseSummaryComments) <> LEN(@OldCaseSummaryComments)
	BEGIN
	    INSERT INTO tblTicketsNotes
	      (
	        TicketID,
	        [Subject],
	        Notes,
	        EmployeeID
	      )
	    VALUES
	      (
	        @TicketID_PK,
	        'Case Summary Comments Updated',
	        'Case Summary Comments Updated',
	        @EmpID
	      )
	END  
	
	UPDATE tblticketsviolations
	SET    ArrestingAgencyName = @ALRArrestingAgency
	WHERE  TicketID_PK = @TicketID_PK
	
	
	-- Agha Usman 4347 07/21/2008 - Problem client managment    
	EXEC USP_HTP_ProblemClient_Manager @TicketID_PK,
	     @Firstname,
	     @Lastname,
	     @MiddleName,
	     @DOB,
	     @isManual,
	     @dFirstname,
	     @dLastname,
	     @dMiddleName,
	     @dDOB 
	
	-----------------------------------------------------------------
	-- Update at 30/August 2007 By Asghar
	-- if Bond = yes then update "1" in bondflag field , update "1" all underlinebondflags in tblticketsviolation table
	-----------------------------------------------------------------            
	IF (@BondFlag = 1)
	BEGIN
	    UPDATE tbltickets
	    SET    bondflag = 1,
	           employeeidupdate = @EmpID
	    WHERE  ticketid_pk = @TicketID_PK    
	    
	    UPDATE tblticketsviolations
	    SET    underlyingbondflag = 1
	    WHERE  ticketid_pk = @TicketID_PK
	END 
	
	-- if Bond = yes then update "0" in bondflag field , update "0" all underlinebondflags in tblticketsviolation table   
	IF (@BondFlag = 0)
	BEGIN
	    UPDATE tbltickets
	    SET    bondflag = 0,
	           employeeidupdate = @EmpID
	    WHERE  ticketid_pk = @TicketID_PK    
	    
	    UPDATE tblticketsviolations
	    SET    underlyingbondflag = 0
	    WHERE  ticketid_pk = @TicketID_PK
	END----------------------------------------    
	   
	
	GO

grant execute on dbo.[Usp_HTS_UpdateCaseInfo] to dbr_webuser
go      
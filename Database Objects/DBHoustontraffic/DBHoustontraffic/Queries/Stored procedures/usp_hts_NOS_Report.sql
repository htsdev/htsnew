﻿

 /**********      
      
Created By : Nasir 5938 05/27/2009 Add Comments In Procedure      
      
Business Logic : This Report display client cases with have not on system flag is active    

      
List Of Parameters :       
      @Validation : if 1 then follow up date will be checked for validation email
List Of Columns :    
      

LastName
FirstName
StatusID
CourtDate
CourtRoom
TicketNumber
HireDate
DOB
hiredatesort
Ref
Status
TicketID_PK
CourtID
CourtTime
FollowUpDate
followupdatesort
generalcomments
CourtID
CaseTypeId
     
**********/      
 
 
 set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


  -- [usp_hts_NOS_Report] 1,1
  
ALTER Procedure [dbo].[usp_hts_NOS_Report] --0
  ----Procedure For NOS Report this procedure is used in both reports (Fahad - 01/04/2007)    
@validation as INT,
@ValidationCategory int=0 -- Saeed 7791 07/10/2010 @ValidationCategory=1 for alert & @ValidationCategory=2 for report
as    



SELECT DISTINCT t.TicketID_PK, t.Lastname, t.Firstname,tv.casenumassignedbycourt  as causenumber, tv.RefCaseNumber, c.ShortName,         
dbo.fn_DateFormat(ISNULL((SELECT MIN(RecDate) AS Expr1 FROM tblTicketsPayment  WHERE  (ticketid = tf.TicketID_PK) AND   (tf.FlagID = 15)), '1/1/1900'), 27, '/', ':', 0)AS HireDate,     
dbo.fn_DateFormat(ISNULL(tte.NOSFollowUpDate,'1/1/1900'), 27, '/', ':', 0) AS Followupdate,tte.NOSFollowupDate as followupdatesort,     
t.GeneralComments AS generalcomments ,(select min(recdate) from tblticketspayment where ticketid = tf.ticketid_pk and tf.flagid = 15) as hiredatesort,
'<a href="http://ln.legalhouston.com/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber='+convert(varchar(30), t.TicketID_PK)+'" >'+convert(varchar(30), ROW_NUMBER() OVER (order by tte.NOSFollowupDate asc ))+'</a>' as link,
t.Criminalfollowupdate AS followupdatee,
c.CourtID, -- Agha Usman - 4426 - 08/05/2008
T.CaseTypeId--Nasir 5938 05/26/2009 for business day check
         
FROM         tblTickets AS t INNER JOIN          
                      tblTicketsViolations AS tv ON t.TicketID_PK = tv.TicketID_PK LEFT OUTER JOIN          
                      tblticketsextensions AS tte ON t.TicketID_PK = tte.Ticketid_pk INNER JOIN          
                      tblCourts AS c ON tv.CourtID = c.Courtid INNER JOIN          
                      tblTicketsFlag AS tf ON tv.TicketID_PK = tf.TicketID_PK          
WHERE     (tf.FlagID = 15) AND (t.activeflag = 1)
and	(
		(	(@validation = 1 OR @ValidationCategory=1) and (datediff (day,tte.NOSFollowUpDate,getdate() ) >= 0)) -- Saeed 7791 07/10/2010 display  if call from validation sp or validation category is 'Alert' then display records which has 'follow up date' null,past or today.
		or (@validation = 0 OR @ValidationCategory=2) -- Saeed 7791 07/09/2010 display all records if @validation=0 or validation category is 'Report'
	)
order by followupdatesort, t.TicketID_PK -- Adil Aleem 5857 05/15/2009 Include Ticket ID in order by clause
    
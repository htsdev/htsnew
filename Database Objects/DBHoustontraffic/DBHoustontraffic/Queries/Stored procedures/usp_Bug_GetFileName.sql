SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Bug_GetFileName]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Bug_GetFileName]
GO



create procedure [dbo].[usp_Bug_GetFileName]
@bugid as int

as

select  a.attachfilename
		from tbl_bug_attachment a
		where
		bug_id=@bugid





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


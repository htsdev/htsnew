SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_All_ScanDocuments_New_Ver_2]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_All_ScanDocuments_New_Ver_2]
GO








create   PROCEDURE [dbo].[usp_Get_All_ScanDocuments_New_Ver_2]     
    
@TicketID varchar(30),    
@DocType varchar(50)    
    
AS    
    
    
IF (@DocType='All')    
Begin    
/*     
 */    
     
     
      
 SELECT  dbo.tblTicketsViolations.ScanDocID AS DOC_ID, 'Resets        ' AS DOCREF,     
                      dbo.tblTicketsViolations.UpdatedDate AS UPDATEDATETIME, dbo.tblTicketsViolations.ScanDocNum AS DOC_Num,    
 LTRIM(dbo.tblTicketsViolations.RefCaseNumber)    
                      + ' - ' + LTRIM(STR(dbo.tblTicketsViolations.SequenceNumber)) + '  - ' + dbo.tblCourtViolationStatus.ShortDescription + '  - ' + CONVERT(varchar(20),     
                      dbo.tblTicketsViolations.CourtDate, 101) + ' ' +  SUBSTRING(SUBSTRING(CONVERT(char(26), dbo.tblTicketsViolations.CourtDate, 109), 13, 14), 1, 5)     
                      + ' ' + SUBSTRING(SUBSTRING(CONVERT(char(26), dbo.tblTicketsViolations.CourtDate, 109), 13, 14), 13, 2)     
                      + '  - #' + dbo.tblTicketsViolations.CourtNumber + '  - ' + ISNULL(dbo.tblViolations.Description, '') AS ResetDesc, dbo.tblTicketsViolations.TicketsViolationID AS RefCaseNumber, '0' AS RecordType, 1 AS ImageCount,0 as did, 0 as ocrid    
into #ScanDoc     
FROM         dbo.tblTicketsViolations LEFT OUTER JOIN    
                      dbo.tblViolations ON dbo.tblTicketsViolations.ViolationNumber_PK = dbo.tblViolations.ViolationNumber_PK LEFT OUTER JOIN    
                      dbo.tblCourtViolationStatus ON dbo.tblTicketsViolations.CourtViolationStatusID = dbo.tblCourtViolationStatus.CourtViolationStatusID    
 WHERE     (dbo.tblTicketsViolations.ScanDocID IS NOT NULL)    AND  TicketID_PK = @TicketID    
 Order By dbo.tblTicketsViolations.RefCaseNumber    
    
/**  **/    
 Insert into #ScanDoc    
 SELECT   TrafficDocs.traffic.DOCMAIN.DOC_ID, TrafficDocs.traffic.DOCMAIN.DOCREF, TrafficDocs.traffic.DOCPIC.UPDATEDATETIME,  '0' as DOC_Num, 'No Description' AS ResetDesc, ' ' AS RefCaseNumber, '0' AS RecordType, 1 AS ImageCount,0 as did ,0 as ocrid       
    
 FROM         TrafficDocs.traffic.DOCMAIN LEFT OUTER JOIN    
                        TrafficDocs.traffic.DOCPIC ON TrafficDocs.traffic.DOCMAIN.DOC_ID = TrafficDocs.traffic.DOCPIC.DOC_ID    
 WHERE     (TrafficDocs.traffic.DOCMAIN.DOCSOURCE = @TicketID )    
/** Tahir **/    
 Insert into #ScanDoc    
 SELECT tblScanBook.ScanBookID AS DOC_ID, tblDocType.DocType, tblScanBook.UPDATEDATETIME,  '0' as DOC_Num, 
	ResetDesc, ' '  AS RefCaseNumber, '1' AS RecordType,tblscanbook.doccount AS ImageCount,  
	tblscanpic.docid as did,  isnull(tblscandata.DOC_ID,0) as ocrid
FROM         dbo.tblScanBook INNER JOIN
              dbo.tblDocType ON dbo.tblScanBook.DocTypeID = dbo.tblDocType.DocTypeID INNER JOIN
              dbo.tblScanPIC ON dbo.tblScanBook.ScanBookID = dbo.tblScanPIC.ScanBookID LEFT OUTER JOIN
               dbo.tblscandata ON dbo.tblScanPIC.DOCID = dbo.tblscandata.docnum AND dbo.tblScanBook.ScanBookID = dbo.tblscandata.DOC_ID
 	where     tblScanBook.TicketID = @TicketID    
    
-- select * from  #ScanDoc    
select DOC_ID, min(DOCREF) as DOCREF , min(UPDATEDATETIME) as UPDATEDATETIME, 
	min(doc_num) as DOC_Num, min (ResetDesc) as ResetDesc  , min (RefCaseNumber) as RefCaseNumber,
	min (RecordType) as RecordType , min (ImageCount) as ImageCount, min(did) as did, min (ocrid) as ocrid
into #temp4
from #scandoc
group by DOC_ID

select * from #temp4
drop table #temp4

end    
    
ELSE IF (@DocType<> '<choose>')    
begin    
     
 SELECT     TrafficDocs.traffic.DOCMAIN.DOC_ID, TrafficDocs.traffic.DOCMAIN.DOCREF, TrafficDocs.traffic.DOCPIC.UPDATEDATETIME,  '0' as DOC_Num, 'No Description'  AS ResetDesc, ' ' AS RefCaseNumber, '0' AS RecordType, 1 AS ImageCount,0 as did ,0 as ocrid     
 into #ScanDoc2    
 FROM         TrafficDocs.traffic.DOCMAIN LEFT OUTER JOIN    
                        TrafficDocs.traffic.DOCPIC ON TrafficDocs.traffic.DOCMAIN.DOC_ID = TrafficDocs.traffic.DOCPIC.DOC_ID    
 WHERE     (TrafficDocs.traffic.DOCMAIN.DOCSOURCE = @TicketID )    
   And  Lower(TrafficDocs.traffic.DOCMAIN.DOCREF) = Lower(@DocType)    
/** Tahir **/    
 Insert into #ScanDoc2    
 SELECT tblScanBook.ScanBookID AS DOC_ID, tblDocType.DocType, tblScanBook.UPDATEDATETIME,  '0' as DOC_Num, 
	isnull(ResetDesc,'') as ResetDesc, ' '  AS RefCaseNumber, '1' AS RecordType,tblscanbook.doccount AS ImageCount,  
	tblscanpic.docid as did,  isnull(tblscandata.DOC_ID,0) as ocrid
FROM         dbo.tblScanBook INNER JOIN
                      dbo.tblDocType ON dbo.tblScanBook.DocTypeID = dbo.tblDocType.DocTypeID 
INNER JOIN
                      dbo.tblScanPIC ON dbo.tblScanBook.ScanBookID = dbo.tblScanPIC.ScanBookID LEFT OUTER JOIN
                      dbo.tblscandata ON dbo.tblScanPIC.DOCID = dbo.tblscandata.docnum AND dbo.tblScanBook.ScanBookID = dbo.tblscandata.DOC_ID
where     tblScanBook.TicketID = @TicketID    
 And       Lower(tblDocType.DocType) = Lower(@DocType) 	
    
-- select * from  #ScanDoc2    

select DOC_ID, min(DOCREF) as DOCREF , min(UPDATEDATETIME) as UPDATEDATETIME, 
	min(doc_num) as DOC_Num, min (ResetDesc) as ResetDesc  , min (RefCaseNumber) as RefCaseNumber,
	min (RecordType) as RecordType , min (ImageCount) as ImageCount--, min(did) as did, min (ocrid) as ocrid
into #temp3
from #scandoc2
group by DOC_ID

select * from #temp3
drop table #temp3
end





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


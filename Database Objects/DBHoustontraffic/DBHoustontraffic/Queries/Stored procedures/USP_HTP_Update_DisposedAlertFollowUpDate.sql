
     
/*            
Created By     : Afaq Ahmed  
Created Date : 05/03/2010      
TasK ID   : 7752            
Business Logic  : This procedure Updates Disposed Alert Follow Up Date .          
               
Parameter:           
   @TicketID  : Updating criteria with respect to TicketId          
   @FollowUpDate : Date of follow Up which has to be set         
   @empid :Employee ID
         
          
*/          
      
CREATE PROCEDURE [dbo].[USP_HTP_Update_DisposedAlertFollowUpDate]
	@FollowUpDate DATETIME,
	@TicketID INT,
	@empid INT
AS
	UPDATE tblTickets
	SET    DisposedAlertFollowUpDate = @FollowUpDate,
	       EmployeeID = @empid
	WHERE  TicketID_PK = @TicketID
GO

GRANT EXECUTE ON [dbo].[USP_HTP_Update_DisposedAlertFollowUpDate] to DBR_WEBUSER
GO
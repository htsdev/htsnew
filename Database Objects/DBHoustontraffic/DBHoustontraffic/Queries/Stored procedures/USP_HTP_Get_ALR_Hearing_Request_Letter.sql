/**  
* Nasir 5864 06/25/2009  
* Business Logic: This procedure is use to get data for ALR Hearing Request Letter

* List of parameter:
* First Name, 
* Last name,
* DOB
* Driving License Number
* Address
* City
* Zip
* State
* Contact No
* Arrest Date
* Couty Name
* ALR Arresting Agency
* ALR Officer Name
* Test Result
* AttorenyFirstName,
* AttorneyLastName,
* AttorneySignatureImage



**/  

--dbo.USP_HTP_Get_ALR_Hearing_Request_Letter 75371
alter PROCEDURE dbo.USP_HTP_Get_ALR_Hearing_Request_Letter
	@TicketID INT
AS
	SELECT distinct tt.Firstname
	      ,tt.Lastname
	      ,tt.DOB
	      ,tt.DLNumber
	      ,tt.Address1+' '+tt.Address2 AS ADDRESS
	      ,tt.City
	      ,tt.Zip
	      ,(SELECT State FROM tblState WHERE StateID =  tt.Stateid_FK) AS state
	      ,convert(varchar(20),ISNULL(dbo.formatphonenumbers(tt.Contact1),'')) as contact1
	      ,ttv.ArrestDate
	      ,tmc.CountyName
	      --,tt.ALRArrestingAgency AS ALRArrestingAgency --tt.ALRArrestingAgency
	      ,ISNULL((Select TOP 1 tbtv.ArrestingAgencyName from tblticketsviolations tbtv where tbtv.ticketid_pk = @TicketID), '') AS ALRArrestingAgency
	      ,tt.ALROfficerName
	      ,CASE WHEN tt.ALRIsIntoxilyzerTakenFlag = 1 then upper(tt.ALRIntoxilyzerResult)
			WHEN tt.ALRIsIntoxilyzerTakenFlag <> 1 THEN 'No' 
			END
			as ALRIntoxilyzerResult
	      ,tf.AttorenyFirstName AS AttorenyFirstName  --tf.AttorenyFirstName
	      ,tf.AttorneyLastName AS AttorneyLastName--tf.AttorneyLastName
	      --Yasir Kamal 7150 01/01/2010 get attorney signature image.
	      ,tf.FirmID
	      INTO #temp
	FROM   tblTickets tt
	       INNER JOIN tblTicketsViolations ttv
	            ON  ttv.TicketID_PK = tt.TicketID_PK
	       INNER JOIN tblCourts tc
	            ON  ttv.CourtID = tc.Courtid	       
	       --Nasir 7150 02/25/2010 remove join as now we get countyid from tblcourt
			LEFT OUTER JOIN tbl_Mailer_County tmc
	            ON  tc.CountyId = tmc.CountyID	       
	       INNER JOIN tblFirm tf
	            ON  tt.CoveringFirmID = tf.FirmID
	WHERE  tt.TicketID_PK = @TicketID
	AND ttv.ViolationNumber_PK=16159

--Yasir Kamal 7150 01/01/2010 get attorney signature image.
SELECT t.*,tf.AttorneySignatureImage
  FROM tblFirm tf INNER JOIN  #temp t ON t.firmid = tf.FirmID
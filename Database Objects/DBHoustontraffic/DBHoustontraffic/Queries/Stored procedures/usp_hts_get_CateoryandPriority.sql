SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_hts_get_CateoryandPriority]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_hts_get_CateoryandPriority]
GO

Create procedure [dbo].[usp_hts_get_CateoryandPriority]        
@TicketId int      
as        
select isnull(ServiceTicketCategory,2) as ServiceTicketCategory, 
isnull(Priority,0) as Priority,Fid ,FirmID

from tblticketsflag where ticketid_pk=@TicketId      

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


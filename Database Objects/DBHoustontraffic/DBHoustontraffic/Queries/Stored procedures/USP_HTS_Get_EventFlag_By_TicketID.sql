/************************************************************
 * Code formatted by SoftTree SQL Assistant � v4.0.34
 * Time: 8/22/2011 12:42:59 PM
 ************************************************************/




/****** 
Modified by:		Muhammad Nasir
Business Logic:	The procedure is used by houston to get the events associated with given ticket.
				
List of Parameters:	
	@TicketID:		ticket id of client	

List of Columns:	
	description :		event name.
	isPaid :			payment occured
	fid:				flag id
	continuancedate:	date of Continuance flag
	ContinuanceStatus:	status of Continuance flag
	ContinuanceOption:	Option of Continuance 

  
******/ 
    -- execute USP_HTS_Get_EventFlag_By_TicketID 330873
ALTER PROCEDURE [dbo].[USP_HTS_Get_EventFlag_By_TicketID]
	@TicketID
AS
	INT 
	
	AS         
	DECLARE @tbl1 TABLE
	        (
	            RowNo INT IDENTITY,
	            TicketID_PK INT,
	            FlagID INT,
	            RecDate DATETIME,
	            IsPaid INT,
	            Fid INT,
	            ContinuanceDate DATETIME,
	            ContinuanceStatus INT,
	            priority INT,
	            empid INT,
	            ServiceTicketCategory INT,
	            ContinuanceOption INT,
	            Options VARCHAR(50),
	            Category VARCHAR(50)
	        )
	--Fahad 9584 08/22/2011 Re-arranged all the Order of the Fields
	INSERT INTO @tbl1
	  (
	    TicketID_PK,
	    FlagID,
	    RecDate,
	    IsPaid,
	    Fid,
	    ContinuanceDate,
	    ContinuanceStatus,
	    ContinuanceOption,
	    priority,
	    empid,
	    ServiceTicketCategory,
	    Options,
	    Category
	  )
	SELECT tf.TicketID_PK,
	       tf.FlagID,
	       tf.RecDate,
	       tf.IsPaid,
	       tf.Fid,
	       tf.ContinuanceDate,
	       tf.ContinuanceStatus,
	       tf.CONTINUANCEOPTION,
	       ISNULL(tf.Priority, 0) AS priority,
	       tf.Empid,
	       tf.ServiceTicketCategory,
	       ISNULL(
	           (
	               SELECT co.Description
	               FROM   ContinuanceOptions co
	               WHERE  co.OptionID = tf.CONTINUANCEOPTION
	           ),
	           ''
	       ) AS Options,
	       ISNULL(
	           (
	               SELECT stc.Description
	               FROM   tblServiceTicketCategories stc
	               WHERE  stc.ID = tf.ServiceTicketCategory
	           ),
	           ''
	       ) AS category
	FROM   tblTicketsFlag AS tf
	       INNER JOIN tblTickets
	            ON  tf.TicketID_PK = tblTickets.TicketID_PK
	WHERE  (tf.TicketID_PK = @TicketID)
	ORDER BY
	       tf.RecDate DESC      
	
	DECLARE @tblTemp TABLE 
	        (TicketID_PK INT, FlagID INT, RecDate DATETIME, RowNo INT)
	
	INSERT INTO @tbltemp
	SELECT ticketid_pk,
	       flagid,
	       RecDate,
	       RowNo
	FROM   @tbl1
	GROUP BY
	       ticketid_pk,
	       flagid,
	       RecDate,
	       RowNo 
	       
	DELETE 
	FROM   @tbl1
	WHERE  rowno NOT IN (SELECT rowno
	                     FROM   @tbltemp)         
	
	SELECT DESCRIPTION,
	       ISNULL(ispaid, 0) AS isPaid,
	       fid,
	       continuancedate,
	       ISNULL(continuancestatus, 0) AS ContinuanceStatus,
	       ISNULL(continuanceoption, 0) AS ContinuanceOption,
	       CASE ttf.flagid
	            WHEN 37 THEN CONVERT(VARCHAR, RecDate, 101)
	            ELSE Options
	       END AS Options,
	       Category,	-- Zahoor 4770 09/11/2008
	       
	       flagid,
	       priority,
	       ISNULL(ServiceTicketCategory, 0) AS ServiceTicketCategory
	FROM   @Tbl1 ttf,
	       tblEventflags tef
	WHERE  tef.flagid_pk = ttf.flagid
	       AND ticketid_pk = @TicketID 
	           --Added By Zeeshan Ahmed On 1/8/2007
	           -- Do Not Change This Order Otherwise It Will Create Problem For Continuance Flag
	ORDER BY
	       flagid ASC,
	       ispaid ASC,
	       Fid DESC  
    




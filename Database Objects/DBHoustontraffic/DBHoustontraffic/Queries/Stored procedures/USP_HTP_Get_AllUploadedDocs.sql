﻿/****** 
Created by:   Fahad Muhammad Qureshi.
Created Date: 10/01/2009
Task ID:	  5376

Business Logic :
			 This procedure is used to get the details of documents that were uploaded by the Primary user.

List of Parameters:	
			@MenuID : Menu id 

List of Columns:
			HelpDocumend ID: primary key by which document save and delete and retrieve.
			UploadedDate   : date on which Document get upload.
			DocName		   : Doument name by which it Upload
			SubMenuID_FK   : Menu id to find iut which page having which menu.
			EmpID		   : Employee id 
			IsDeleted	   : To show Deleted document By the User.
			UserName	   : By which user logged in to the system. 
			Accesstype	   : to find out Secondry User and Primary User.	
	

******/   
CREATE PROCEDURE [dbo].[USP_HTP_Get_AllUploadedDocs]
@MenuID int  
as  
SELECT 
			hp.HelpDocID_PK,
			hp.UploadedDate,
			hp.DocName,
			hp.SubMenuID_FK,
			hp.EmpID,
			hp.IsDeleted,
			(tu.Firstname)AS UserName,
			tu.Accesstype,hp.SavedDocName
			

FROM		dbo.tblHelpDocument hp 
	INNER JOIN                          
			dbo.tblUsers tu              
ON  
			hp.EmpID = tu.EmployeeID 
WHERE 
			hp.SubMenuID_FK =@MenuID
		AND
			hp.IsDeleted=0
		
			
			GO
			GRANT EXECUTE ON [dbo].[USP_HTP_Get_AllUploadedDocs]TO dbr_webuser
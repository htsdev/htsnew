SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/****** 
Created by:		Tahir Ahmed
Business Logic:	The procedure is used by LMS application to get data for Dallas County Criminal 
				Booking List letter and to create LMS history for this letter for the selected date range.
				
List of Parameters:
	@catnum:		Category number of the selected letter type, if printing for 
					all courts of the selected court category
	@LetterType:	Selected letter type in LMS
	@CrtId:			Court id of the selected court category if printing for individual court hosue.
	@startListdate:	Comma separated dates selected to print letter.
	@Empid:			Employee information of the currently logged in employee
	@PrintType:		Flag to just preview the letter or to mark letters as sent. If 1 then LMS letter history will be created.
	@SearchType:	Flag that identifies if searching by court date or by list date. 0= list date, 1 = court date
	@isPrinted:		Flag to include already printed letters in the batch.
	@Language:		Flag if need to print double sided or single sided letters.

List of Columns:	
	LetterId:		Letter ID generated for each letter. It will be "NOT PRINTABLE"	if @PrintType = 0
	RecordId:		Identity value to look up the case in non-clients. used to group the records in report file.
	FirstName:		Person's first name that will appear on the letter
	LastName:		Person's last name that will appear on the letter
	Address:		Person's Home address that will appear on the letter
	City:			Person's Home City that will appear on the letter
	State:			Person's Home State that will appear on the letter
	ViolationDescritpion: violation description that will appear on the letter
	Court Name:		Name of court with which the violation is associated. Used in report file
	DPC:			Address status that is used in barcode font on the letter
	DPTwo:			Address status that is used in barcode font on the letter
	TicketNumber_pk:Case number for the violation
	ZipCode:		Person's Home Zip code.
	Language:		Flag if need to print double sided or single sided letters
	CourtDate:		Court date associated with the violation
	Abb:			Short abbreviation of employee who is printing the letter


******/
    
--  usp_Mailer_Send_DallasCriminal_Sheriff 16, 45, 16, '08/18/2010', 3992, 0 ,0, 0,0    
ALTER procedure [dbo].[usp_Mailer_Send_DallasCriminal_Sheriff]                                     
 (          
 @catnum int=1,                                          
@LetterType int=9,                                          
@crtid int=1,                                          
@startListdate varchar (500) ='01/04/2006',                                          
@empid int=2006,                                    
@printtype int =0 ,      
@searchtype int,  
@isprinted  bit,  
@language int = 0   -- by default print  English letters   
 )          
as                                              
                                                    

-- DECLARING LOCAL VARIABLES FOR THE FILTERS....
declare @officernum varchar(50),                                              
 @officeropr varchar(50),                                              
 @tikcetnumberopr varchar(50),                                              
 @ticket_no varchar(50),                                                                              
 @zipcode varchar(50),                                                       
 @zipcodeopr varchar(50),                                              
 @fineamount money,                                                      
 @fineamountOpr varchar(50) ,                                                      
 @fineamountRelop varchar(50),                             
 @singleviolation money,                                                      
 @singlevoilationOpr varchar(50) ,                                                      
 @singleviolationrelop varchar(50),                                              
 @doubleviolation money,                                                      
 @doublevoilationOpr varchar(50) ,                                                      
 @doubleviolationrelop varchar(50),                                            
 @count_Letters int,                            
 @violadesc varchar(500),                            
 @violaop varchar(50),          
 @lettername varchar(50)          
          
     
 set @lettername = 'Dallas County Criminal Sheriff'          
      
-- DECLARING & INITIALIZING THE VARIABLE FOR DYNAMIC SQL...                                              
declare @sqlquery varchar(max),@sqlquery2 varchar(max),@sqlquery3 varchar(max)  
Select @sqlquery ='', @sqlquery2 =''  , @sqlquery3 = ''                                                  

--Sabir Khan 5450 01/22/2009 Send Criminal mailer to barry...
------------------------------------------------------------
declare @tempDate datetime, @mailbarry bit
select @tempDate = '1/23/09', @mailbarry = 0
if DATEDIFF(DAY,@tempdate,GETDATE())=0
	set @mailbarry = 1   
-------------------------------------------------------------
-- ASSIGNING VALUES TO THE VARIABLES 
-- BY GETTTING INFORMATION FROM LMS FILTER STRUCTURE...
-- FOR THIS LETTER TYPE....                                                      
Select @officernum=officernum,                                              
 @officeropr=oficernumOpr,                                              
 @tikcetnumberopr=tikcetnumberopr,                                              
 @ticket_no=ticket_no,                                                                                
 @zipcode=zipcode,                                              
 @zipcodeopr=zipcodeLikeopr,                                              
 @singleviolation =singleviolation,                                              
 @singlevoilationOpr =singlevoilationOpr,                                              
 @singleviolationrelop =singleviolationrelop,                                              
 @doubleviolation = doubleviolation,                                              
 @doublevoilationOpr=doubleviolationOpr,                                                      
 @doubleviolationrelop=doubleviolationrelop,                            
 @violadesc=violationdescription,                            
 @violaop=violationdescriptionOpr ,                        
 @fineamount=fineamount ,                                                      
 @fineamountOpr=fineamountOpr ,        
 @fineamountRelop=fineamountRelop                        
from  tblmailer_letters_to_sendfilters                                              
where  courtcategorynum=@catnum            
and lettertype = @lettertype                                            
                                           
-- GETTING THE SHORT ABBREVIATION OF THE CURRENTLY LOGGED IN 
-- EMPLOYEE IN THE LOCAL VARIABLE....          
declare @user varchar(10)          
select @user = upper(abbreviation) from tblusers where employeeid = @empid          
                                          

-- CREATING DYNAMIC SQL FOR THE MAIN QUERY...
set @sqlquery=@sqlquery+'  
declare @recCount int                                            

Select distinct tva.recordid,           
mdate = convert(datetime,convert(varchar(10),ta.listdate,101)),    
count(tva.ViolationNumber_PK) as ViolCount,          
isnull(sum(isnull(tva.fineamount,0)),0) as Total_Fineamount           
into #temptable                                                    
FROM    dallastraffictickets.dbo.tblTicketsViolationsArchive tva           
inner join           
dallastraffictickets.dbo.tblTicketsArchive ta           
on  tva.recordid = ta.recordid          
inner join  dallastraffictickets.dbo.tblcourts tc  on  ta.courtid = tc.courtid        
INNER JOIN        
tblstate S ON        
ta.stateid_fk = S.stateID          

-- ONLY VERIFIED AND VALID ADDRESSES
where Flag1 in (''Y'',''D'',''S'')  

-- NOT MARKED AS STOP MAILING...
and  isnull(donotmailflag,0)  =0           

-- PERSON NAME MUST NOT BE EMPTY...
and len(isnull(ta.firstname,'''') )> 0 and len(isnull(ta.lastname,'''') ) > 0        

-- PERSONS ADDRESS IS NOT CHANGED IN PAST 48 MONTHS (ACCUZIP)...
and isnull(ta.ncoa48flag,0) = 0    

-- ONLY BOOKING LIST RECORDS...
--and tva.insertionloaderid = 18

--Rab Nawaz Khan 9326 05/30/2011 Only Booking List and felony cases  
--Zeeshan Haider 11483 11/01/2013 Added DCC Book In New loader "86" for SUB1*.MRG files 
AND tva.insertionloaderid in (18, 35, 86)  
-- end 9326 

--Yasir Kamal 6831 10/21/2009 
-- EXCLUDE RECORDS THAT HAVE BEEN ALREADY PRINTED OR PASS TO HIRE LETTER PRINTED IN PAST 7 DAYS.
-- Babar Ahmad 8597	07/14/2011 Changed number of days from 7 to 5 business days and included Bonded Out mailers.
and  ta.recordid not in (                                                                              
select n.recordid from tblletternotes n  inner join dallastraffictickets.dbo.tblticketsarchive a           
on a.recordid = n.recordid where lettertype = '+convert(varchar, @lettertype)+'
union 
select l.recordid from tblletternotes l where (l.Lettertype in (46,73) and datediff(day, l.recordloaddate, dbo.fn_getprevbusinessday(GETDATE(),5)) <=0)         
)          
'          
                    
-- IF PRINTING FOR ALL COURTS OF THE SELECTED COURT CATEGORY....          
if(@crtid=@catnum)          
	set @sqlquery=@sqlquery+' 
	and tva.courtlocation In (Select courtid from dallastraffictickets.dbo.tblcourts where courtcategorynum  = '+convert(varchar,@catnum)+')'                    

-- ELSE PRINTING FOR THE INDIVIDUAL SELECTED COURT OF THE CATEGORY....
else          
	set @sqlquery=@sqlquery+' 
	and tva.courtlocation=' + convert(varchar,@crtid)          
          
                                   
-- FOR THE SELECTED DATE RANGE ONLY....
if(@startListdate<>'')                                              
	set @sqlquery=@sqlquery+ '           
	and '+dbo.Get_String_Concat_With_DatePart_ver2(''+@startListdate +'', 'ta.listdate')     
                                       
-- LMS FILTERS SECTION:
-----------------------------------------------------------------------------------------------------------------------------------------------------
-- IF THERE IS ANY FILTER SPECIFIED TO INCLUDE/EXCLUDE ONLY PARTICULAR OFFICER NUMBERS....      
if(@officernum<>'')                                              
	set @sqlquery =@sqlquery +'            
	and  ta.officerNumber_Fk '+@officeropr +'('+ @officernum+')'                                               
                                              
-- IF THERE IS ANY FILTER DEFINED FOR INCLUDE/EXCLUDE TICKET NUMBER...
if(@ticket_no<>'')                                              
	set @sqlquery=@sqlquery+ '          
	and   ('+ dbo.Get_String_Concat_With_op(''+@ticket_no+'' , 'tva.TicketNumber_PK' ,+ ''+ @tikcetnumberopr)+')'                                                    
                          
-- ZIP CODE FILTER......
-- INCLUDE/EXCLUDE THE SPECIFIED ZIP CODES.....
if(@zipcode<>'')                                                                    
	set @sqlquery=@sqlquery+ '           
	and ('+  dbo.Get_String_Concat_With_op(''+@zipcode+'' , 'left(ta.zipcode,2)' ,+ ''+ @zipcodeopr) +')'                        
           
-- FINE AMOUNT FILTER.....
-- INCLUDE ONLY THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT....
if(@fineamount<>0 and @fineamountOpr<>'not'  )                                          
	set @sqlquery =@sqlquery+ '          
    and tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                                               
          
-- EXCLUDE THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT.....
if(@fineamount<>0 and @fineamountOpr = 'not'  )                                          
	set @sqlquery =@sqlquery+ '          
    and  not tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                                               
          
                                                                  
set @sqlquery =@sqlquery+'                                                   
group by   tva.recordid,  convert(datetime,convert(varchar(10),ta.listdate,101))     
having 1=1       
'        
    
-- SINGLE VIOLATION FINE AMOUNT FILTER....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE SINGLE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT
if(@singleviolation<>0 and @doubleviolation=0)                                    
	set @sqlquery =@sqlquery+ '    
	and (  '+ @singlevoilationOpr+  '  (isnull(sum(isnull(tva.fineamount,0)),0) '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and count(tva.ViolationNumber_PK)=1))    
      '                                    

-- DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...
if(@doubleviolation<>0 and @singleviolation=0 )                                    
	set @sqlquery =@sqlquery + '    
	and ('+ @doublevoilationOpr+  '   (isnull(sum(isnull(tva.fineamount,0)),0) '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ')and count(tva.ViolationNumber_PK)=2) )    
      '                                                        
                                    
-- BOTH SINGLE & DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE AND SINGLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...
if(@doubleviolation<>0 and @singleviolation<>0)                                    
	set @sqlquery =@sqlquery+ '    
	and ('+ @singlevoilationOpr+  '  (isnull(sum(isnull(tva.fineamount,0)),0) '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and count(tva.ViolationNumber_PK)=1))    
    and ('+  @doublevoilationOpr+  '   (isnull(sum(isnull(tva.fineamount,0)),0) '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ') and count(tva.ViolationNumber_PK)=2))    
        '                                      
    
-- GETTING THE CLIENT'S PERSONAL & VIOLATION INFORMATION 
-- IN TEMP TABLE FOR RECORDS FILTERED IN THE ABOVE QUERY...    
set @sqlquery=@sqlquery+'                                            
Select distinct tva.recordid,           
a.mdate ,tva.courtdate, ta.courtid,  flag1 = isnull(ta.Flag1,''N'') , ta.officerNumber_Fk, ta.violationdate as bookingdate,    
isnull(tva.causenumber, tva.ticketnumber_pk) as TicketNumber_PK, isnull(ta.donotmailflag,0) as donotmailflag,          
ta.clientflag,  upper(firstname) as firstname, upper(lastname) as lastname, upper(address1) + '''' + isnull(ta.address2,'''') as address,                                
upper(ta.city) as city, s.state,        
tc.CourtName, zipcode,  midnumber,  dp2 as dptwo, dpc,  violationdate,  violationstatusid,           
left(zipcode,5) + rtrim(ltrim(midnumber)) as zipmid,     
ViolationDescription, case when isnull(tva.fineamount,0)=0 then 100 else convert(numeric(10,0),tva.fineamount) end as FineAmount,     
a.ViolCount,          
a.Total_Fineamount           
into #temp                                                    
FROM    dallastraffictickets.dbo.tblTicketsViolationsArchive tva           
inner join           
dallastraffictickets.dbo.tblTicketsArchive ta           
on  tva.recordid = ta.recordid          
inner join  dallastraffictickets.dbo.tblcourts tc  on  tva.courtlocation = tc.courtid        
INNER JOIN        
tblstate S ON        
ta.stateid_fk = S.stateID     
inner join #temptable a on a.recordid = ta.recordid 
where  
--Rab Nawaz Khan 9326 05/30/2011 Only Booking List and felony cases  
--Zeeshan Haider 11483 11/01/2013 Added DCC Book In New loader "86" for SUB1*.MRG files  
tva.insertionloaderid in (18, 35, 86)


'         

-- VIOLATION DESCRIPTION FILTER.....
-- EXCLUDE THE LETTER IF SPECIFIED VIOLATION IS THE ONLY VIOLATION IN THE LETTER....
if(@violadesc <> '')    
begin    
	-- NOT LIKE FILTER.......    
	if(charindex('not',@violaop)<> 0 )                  
	 set @sqlquery=@sqlquery+'     
	 and  not ( a.violcount =1  and ('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ 'like')+'))'               
	    
	-- INCLUDE ONLY THOSE LETTER THAT SINGLE VIOLATION AND MATCHES THE SPECIFIED VIOLATION DESCRIPTION...
	-- LIKE FILTER.......
	if(charindex('not',@violaop) =  0 )                  
	 set @sqlquery=@sqlquery+'     
	 and  ( a.violcount =1  and ('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ 'like')+'))'               
end    


set @sqlquery=@sqlquery+'    
-- tahir 6327 08/07/2009 need to rollback 4456 changes..
-- tahir ahmed 4456 07/23/2008 
-- get only those letters that have DRVIING WHILE INTOXICATED like violations...

--select distinct recordid into #templetter from dallastraffictickets.dbo.tblticketsviolationsarchive
--where violationdescription like ''driving while intoxicated%''

--delete from #temp where recordid not in (select recordid from #templetter)

'  

    
-- EXCLUDE 'JUSTICE OF THE PEARCE' VIOLATIONS....
set @sqlquery=@sqlquery+'      

select distinct recordid into #ViolFilter from dallastraffictickets.dbo.tblticketsviolationsarchive    
where charindex(''justice of the peace'',violationdescription)>0     
delete from #temp where recordid in (select recordid from #violfilter)    
'               
                                      
-- GETTING THE FINAL RESULSTS IN ANOTHER TEMP TABLE.....
set @sqlquery=@sqlquery+'

-- Rab Nawaz Khan 8754 01/21/2011 Exclude Disposed Violations
	Delete from #temp
	where #temp.violationstatusid = 80
-- End 8754

Select distinct recordid, violationstatusid,ViolCount,Total_Fineamount, mdate, FirstName,LastName,address,                                        
city,state,FineAmount,violationdescription ,CourtName,dpc,dptwo,Flag1,officerNumber_Fk,TicketNumber_PK,                                            
courtid,zipmid,donotmailflag,clientflag, zipcode, midnumber, courtdate, bookingdate   into #temp1  from #temp   

		'          
--Sabir Khan 5450 01/22/2009 Send Crminal mailer to barry...
-- Abbas Shahid Khwaja 8799 4/19/2011 Change the values of dpc and dptwo in the #temp1 for barcode values and change zip code.
------------------------------------------------------------
if @mailbarry = 1
	set @sqlquery=@sqlquery+ 

'insert into #temp1 
select ''1'', 116, 2, 150, '''+ convert(Varchar(10), @tempdate, 101) +''', ''BARRY'', ''BOBBIT'', ''822 Tenison Memorial'',
''DALLAS'', ''TX'', 100, ''Printed on '+ convert(varchar, getdate()) +''', ''TESTING'', ''1'',''22'', ''Y'', 0, ''TESTING 1'', 3049, ''75223-1140'', 0, 0, ''75223-1140'', '''', ''2/20/2008'', ''2/20/2008''
'   
------------------------------------------------------------                   
 set @sqlquery=@sqlquery+                       
' -- Sabir Khan 7099 12/07/2009 exclude client cases
delete from #temp1 where recordid in (
select v.recordid from dallastraffictickets.dbo.tblticketsviolations v inner join dallastraffictickets.dbo.tbltickets t 
on t.ticketid_pk = v.ticketid_pk and t.activeflag = 1 
inner join #temp1 a on a.recordid = v.recordid
) 
 '                      
          

-- IF "MARK LETTER AS SENT" OPTION IS SELECTED....
-- CREATE LETTER HISTORY FOR EACH PERSON....
if( @printtype = 1)                   
	BEGIN
		set @sqlquery2 =@sqlquery2 +                                              
		'          
		Declare @ListdateVal DateTime, @totalrecs int,@count int, @p_EachLetter money,@recordid varchar(50),                                        
		@zipcode   varchar(12),@maxbatch int  ,@lCourtId int  ,@dptwo varchar(10),@dpc varchar(10)          
		declare @tempBatchIDs table (batchid int)          

		-- GETTING TOTAL LETTERS AND COSTING ......
		Select @totalrecs =count(*) from #temp1 where mdate = @ListdateVal                                           
		Select @count=Count(*) from #temp1                                          
		set @p_EachLetter=convert(money,0.50)                                          

		-- DECLARING CURSOR TO INSERT THE RECORDS IN LETTER HISTORY ONE BY ONE.....
		-- THERE WILL A SEPARATE BATCH FOR EACH LIST/COURT DATE VALUE ......
		Declare ListdateCur Cursor for                                            
		Select distinct mdate  from #temp1                                          
		open ListdateCur                                             
		Fetch Next from ListdateCur into @ListdateVal                                                           
		while (@@Fetch_Status=0)                                        
			begin                                          

				-- GETTING TOTAL LETTERS FOR THE LIST DATE....
				Select @totalrecs =count(distinct recordid) from #temp1 where mdate = @ListdateVal                                           

				-- INSERTING RECORD IN BATCH TABLE......                               
				insert into tblBatchLetter(Empid, Lettertype,Listdate,CourtId,ParentID,LCount,PExpense) values                                            
				('+ convert(varchar(10),@empid) +', '+ convert(varchar(10),@lettertype) +' , @ListdateVal, '+ convert(varchar(10),@catnum) +', 0, @totalrecs, @p_EachLetter)                                             

				-- GETTING BATCH ID OF THE INSERTED RECORD.....
				Select @maxbatch=Max(BatchId) from tblBatchLetter           
				insert into @tempBatchIDs select @maxbatch                                                                 

				-- CREATING CURSOR TO INSERT RECORDS FOR EACH LETTER FOR THE NEWLY INSERTED BATCH....
				Declare RecordidCur Cursor for                                         
				Select distinct recordid,zipcode, courtid, dptwo, dpc from #temp1 where mdate = @ListdateVal                                         
				open RecordidCur                                                                   
				Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc                                                    
				while(@@Fetch_Status=0)                                        
					begin                                        

						-- INSERTING LETTERS IN LETTER HISTORY FOR THE SPECIFIED BATCH...
						insert into tblLetterNotes(BatchId_Fk,listdate,ZipCode,RecordID,PCost,LetterType, courtid, dp2, dpc)                                        
						values (@maxbatch,@ListdateVal,@zipcode,@recordid,@p_EachLetter,'+ convert(varchar(10),@lettertype) +',@lCourtId, @dptwo, @dpc)                                        
						Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc                                    
					end                                        
				close RecordidCur           
				deallocate RecordidCur                                                                         
				Fetch Next from ListdateCur into @ListdateVal                                        
			end                                                      

		close ListdateCur            
		deallocate ListdateCur           
		
		-- OUTPUTTING THE DATA TO THE APPLICATION FOR REPROT FILE.....
		Select distinct convert(varchar(20),n.noteid) as letterid, t.recordid, t.FirstName , t.LastName, clientflag,    
		t.address,t.city, t.state, violationdescription ,CourtName, t.dpc,    
		dptwo, TicketNumber_PK , t.zipcode , courtdate , '''+@user+''' as Abb      
		into #temp2  
		from #temp1 t , tblletternotes n, @tempBatchIDs tb          
		where t.recordid = n.recordid and t.courtid = n.courtid and n.batchid_fk = tb.batchid and n.lettertype = '+convert(varchar(10),@lettertype)+'           
		order by T.zipcode           
		select @recCount = count(distinct recordid) from #temp1           

		-- GENERATING AND SENDING EMAIL FOR LETTER PRINTING NOTIFICATION........
		declare @lettercount int, @subject varchar(200), @body varchar(400) , @sql varchar(500)          
		select @lettercount = count(distinct n.noteid) from tblletternotes n, @tempBatchIDs b where n.batchid_fk = b.batchid          
		select @subject =  convert(varchar(20), @lettercount) + '' '' +'''+ @lettername +' Letters Printed''          
		select @body = convert(varchar(20), @lettercount) + '' LETTERS ('' + CONVERt(VARCHAR(12), GETDATE()) + '') - ''+ upper( '''+@user+''')          
		exec usp_mailer_send_Email @subject, @body,1          

		-- OUTPUTING THE PDF FILE NAME TO THE APPLICATION...
		declare @batchIDs_filname varchar(500)    
		set @batchIDs_filname = ''''    
		select @batchIDs_filname = @batchIDs_filname + convert(varchar,batchid) + '',''  from @tempBatchIDs    

		'          
	END
          
else   if( @printtype = 0)     
   
	-- IF "MARK LETTER AS SENT" OPTION IS NOT SELECTED....
	-- THEN DO NOT CREATE LETTER HISTORY. JUST OUTPUT THE DATA FOR REPORT FILE 
	-- TO DISPLAY THE LETTER...
	BEGIN
		set @sqlquery2 = @sqlquery2 + '          
		Select distinct ''NOT PRINTABLE'' as letterId,  recordid,  FirstName, lastname, clientflag,                                       
		address,city, state,violationdescription,CourtName, dpc, dptwo, TicketNumber_PK ,     
		zipcode, courtdate , '''+@user+''' as Abb   
		into #temp3   
		from #temp1           
		order by zipcode  
		select @recCount = count(distinct recordid) from #temp3                 
		'      
	END    
--Sabir Khan 6995 11/23/2009 Letter Type added...
-- IF SINGLE SIDED LETTER IS SELECTED.....
if @language = 0        
	begin        
		-- IF ONLY PREVIEWING THE LETTERS....
		if @printtype = 0        
			BEGIN
				select @sqlquery3 = @sqlquery3 + '         
				select t1.*, ''0'' as language, convert(varchar(15), t1.recordid) + ''0'' as ChkGroup, '+ CONVERT(VARCHAR,@LetterType) +' as LetterType from #temp3 t1 order by t1.zipcode
				'        
			END
		      
		-- IF SENDING LETTERS TO THE LETTER HISTORY....
		ELSE if @printtype = 1        
			BEGIN
				select @sqlquery3 = @sqlquery3 + '         
				select t2.*, ''0'' as language, convert(varchar(15), t2.recordid) + ''0'' as ChkGroup, '+ CONVERT(VARCHAR,@LetterType) +' as LetterType from #temp2 t2         
				order by [zipcode]       
				select isnull(@batchIDs_filname,'''') as batchid       
				'        
			END
	end        

             

-- print @sqlquery     
-- print @sqlquery2      
-- print @sqlquery3          

-- EXECUTING THE DYNAMIC SQL ....
exec  ( @sqlquery + @sqlquery2 + @sqlquery3 )          
    


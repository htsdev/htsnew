SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_HTS_Get_CaseHistoryInfo]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_HTS_Get_CaseHistoryInfo]
GO










CREATE procedure usp_HTS_Get_CaseHistoryInfo

@TicketID   int


as 

begin
	SELECT   
		 b.Abbreviation,
		 a.Subject, a.Recdate
	FROM    
		 dbo.tblUsers b INNER JOIN
                 dbo.tblTicketsNotes a ON b.EmployeeID = a.EmployeeID
	
	where a.TicketID = @TicketID  
	order by a.recdate desc


end






GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


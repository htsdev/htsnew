SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_GET_contactNos_by_Ticketid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_GET_contactNos_by_Ticketid]
GO

CREATE PROCEDURE USP_HTS_GET_contactNos_by_Ticketid  

@TicketID_pk int  

as  

declare @tabs varchar(200)
set @tabs='&nbsp;&nbsp;&nbsp;&nbsp;'
Select case when c1.contacttype_pk<>0 then '<BR>'+@tabs+c1.description+': '+t.contact1 else '' end +
case when c2.contacttype_pk<>0 then '<BR>'+@tabs+c2.description+': '+t.contact2 else '' end+
case when c3.contacttype_pk<>0 then '<BR>'+@tabs+c3.description+': '+t.contact3 else '' end as ContactNo
from tbltickets t,tblcontactstype c1,tblcontactstype c2,tblcontactstype c3
where c1.contacttype_pk=t.contacttype1
and c2.contacttype_pk=t.contacttype2
and c3.contacttype_pk=t.contacttype3
and TicketID_PK = @TicketID_pk  


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_mailer_get_savedfilter]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_mailer_get_savedfilter]
GO









CREATE   proc [dbo].[usp_mailer_get_savedfilter] --1             
@courtcategorynum int,            
@LetterType int            
             
as            
declare @lettername varchar(25)            
            
Select  isnull(officernum,'') officernum ,isnull(oficernumOpr,'') oficernumOpr ,            
isnull(ticket_no,'')  ticket_no, isnull(tikcetnumberopr,'') tikcetnumberopr,isnull(convert(varchar(10),fineamount),'') fineamount,isnull(fineamountrelop,'') fineamountrelop, isnull(fineamountOpr,'') fineamountOpr,isnull(violationdescription,'')violationdescription ,            
isnull(violationdescriptionOpr,'') violationdescriptionOpr ,isnull( zipcode,'') zipcode, isnull(zipcodeLikeopr,'')zipcodeLikeopr ,isnull(Lettertype,'')  Lettertype,            
isnull(convert(varchar (10),singleviolation),'') singleviolation,            
isnull(singlevoilationOpr,'') singlevoilationOpr,        
isnull(singleviolationrelop,'') singleviolationrelop,        
isnull(convert(varchar ( 10),doubleviolation),'') doubleviolation ,isnull(doubleviolationOpr,'') doubleviolationOpr,            
isnull(doubleviolationrelop,'') doubleviolationrelop        
from tblmailer_letters_to_sendfilters            
where courtcategorynum=@courtcategorynum             
and LetterType=@LetterType








GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


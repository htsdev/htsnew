SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_MAILER_GET_HarrisCountyUploads]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_MAILER_GET_HarrisCountyUploads]
GO



-- USP_MAILER_GET_HarrisCountyUploads '8/1/07', '9/30/07', 0
CREATE procedure [dbo].[USP_MAILER_GET_HarrisCountyUploads]
	(
	@sdate datetime,
	@edate datetime,
	@uploadtype int = 0
	)

as


--declare @sdate datetime, @edate datetime
--set @sdate = '9/10/07'
--set @edate = '9/30/07'
--


-- FIRST GET THE DIFFERENT TYPES OF UPLOADS......
create table #uploads (listdate datetime, categoryid int, courtid int, shortname varchar(50), uploadtype varchar(20),  uploadcount int, violcount int)


-- APPEARANCE/ARRAIGNMENT CASES......
insert into #uploads (listdate, categoryid, courtid, shortname, uploadtype, uploadcount, violcount)
select convert(varchar(10),  t.listdate,101),
	c.courtcategorynum,
	c.courtid ,
	c.shortname,
	'APP/ARR',
	count(distinct t.recordid) ,
	count(v.ticketnumber_pk) 
from tblticketsarchive t, tblticketsviolationsarchive v, tblcourts c
where t.recordid= v.recordid 
and v.courtlocation = c.courtid
and datediff(day, t.listdate, @sdate)<=0
and datediff(day, t.listdate, @edate)>=0
and datediff(day, isnull(v.bonddate, '1/1/1900'),'1/1/1900') =0 
and datediff(day, isnull(T.bonddate, '1/1/1900'),'1/1/1900') =0 
and datediff(day, isnull(v.ftaissuedate, '1/1/1900'),'1/1/1900') =0 
group by c.courtcategorynum, c.courtid, c.shortname,convert(varchar(10),  t.listdate,101)

-- HMC WARRANTS (FTA)......
insert into #uploads (listdate, categoryid, courtid, shortname, uploadtype, uploadcount, violcount)
select convert(varchar(10),  v.ftaissuedate,101),
	c.courtcategorynum,
	c.courtid ,
	c.shortname,
	'WARRANT',
	count(distinct left(t.zipcode,5)+t.midnumber) ,
	count(v.ticketnumber_pk) 
from tblticketsarchive t, tblticketsviolationsarchive v, tblcourts c
where t.recordid= v.recordid 
and v.courtlocation = c.courtid
and datediff(day, v.ftaissuedate, @sdate)<=0
and datediff(day, v.ftaissuedate, @edate)>=0
group by c.courtcategorynum, c.courtid, c.shortname,convert(varchar(10),  v.ftaissuedate,101)

-- HMC DLQ......
insert into #uploads (listdate, categoryid, courtid, shortname, uploadtype, uploadcount, violcount)
select convert(varchar(10),  v.bonddate,101),
	c.courtcategorynum,
	c.courtid ,
	c.shortname,
	'DLQ',
	count(distinct left(t.zipcode,5)+t.midnumber) ,
	count(v.ticketnumber_pk) 
from tblticketsarchive t, tblticketsviolationsarchive v, tblcourts c
where t.recordid= v.recordid 
and v.courtlocation = c.courtid
and datediff(day, v.bonddate, @sdate)<=0
and datediff(day, v.bonddate, @edate)>=0
group by c.courtcategorynum, c.courtid, c.shortname,convert(varchar(10),  v.bonddate,101)

-- OTHER COURT WARRANT CASES.......
insert into #uploads (listdate, categoryid, courtid, shortname, uploadtype, uploadcount, violcount)
select convert(varchar(10),  v.bonddate,101),
	c.courtcategorynum,
	c.courtid ,
	c.shortname,
	'WARRANT',
	count(distinct T.recordid) ,
	count(v.ticketnumber_pk) 
from tblticketsarchive t, tblticketsviolationsarchive v, tblcourts c
where t.recordid= v.recordid 
and v.courtlocation = c.courtid
and c.courtcategorynum <> 1
and datediff(day, v.bonddate, @sdate)<=0
and datediff(day, v.bonddate, @edate)>=0
group by c.courtcategorynum, c.courtid, c.shortname,convert(varchar(10),  v.bonddate,101)

-- DALLAS APPEARANCE......
insert into #uploads (listdate, categoryid, courtid, shortname, uploadtype, uploadcount, violcount)
select convert(varchar(10),  t.listdate,101),
	c.courtcategorynum,
	c.courtid ,
	c.shortname,
	'APP/ARR',
	count(distinct t.recordid) ,
	count(v.ticketnumber_pk) 
from dallastraffictickets.dbo.tblticketsarchive t, dallastraffictickets.dbo.tblticketsviolationsarchive v, dallastraffictickets.dbo.tblcourts c
where t.recordid= v.recordid 
and v.courtlocation = c.courtid
and datediff(day, t.listdate, @sdate)<=0
and datediff(day, t.listdate, @edate)>=0
and datediff(day, isnull(v.violationbonddate, '1/1/1900'),'1/1/1900') =0 
group by c.courtcategorynum, c.courtid, c.shortname,convert(varchar(10),  t.listdate,101)

-- DALLAS WARRANT......
insert into #uploads (listdate, categoryid, courtid, shortname, uploadtype, uploadcount, violcount)
select convert(varchar(10),  v.violationbonddate,101),
	c.courtcategorynum,
	c.courtid ,
	c.shortname,
	'WARRANT',
	count(distinct t.recordid) ,
	count(v.ticketnumber_pk) 
from dallastraffictickets.dbo.tblticketsarchive t, dallastraffictickets.dbo.tblticketsviolationsarchive v, dallastraffictickets.dbo.tblcourts c
where t.recordid= v.recordid 
and v.courtlocation = c.courtid
and datediff(day, v.violationbonddate, @sdate)<=0
and datediff(day, v.violationbonddate, @edate)>=0
group by c.courtcategorynum, c.courtid, c.shortname,convert(varchar(10), v.violationbonddate,101)

-- JIMS CASES......
insert into #uploads (listdate, categoryid, courtid, shortname, uploadtype, uploadcount, violcount)
select convert(varchar(10),  T.recdate,101),
	c.courtcategorynum,
	3037 as courtid ,
	c.shortname,
	'APP/ARR',
	count(distinct t.bookingnumber) ,
	count(t.casenumber) 
from jims.dbo.criminalcases T, tblcourts c
where c.courtid = 3037
and datediff(day, T.recdate, @sdate)<=0
and datediff(day, T.recdate, @edate)>=0
group by c.courtcategorynum, c.shortname,convert(varchar(10), T.recdate,101)


-- NOW GET THE LETTERS INFORMATION.....
create table #letters (listdate datetime, categoryid int, courtid int, shortname varchar(50), uploadtype varchar(20),  lettercount int, violcount int)

-- LETTERS FROM TRAFFIC TICKETS DATABASE FOR ARR/APP CASES......
INSERT into #letters (listdate, categoryid, courtid, shortname, uploadtype, lettercount, violcount)
select convert(varchar(10),  n.listdate,101) ,	
	b.courtid,
	n.courtid,
	c.shortname,
	'APP/ARR',
	count(distinct n.noteid) as LetterCount,
	count(v.ticketnumber_pk) as LetterViolCount
from tblletternotes n, tblcourts c, tblticketsviolationsarchive v, tblbatchletter b
where n.courtid= c.courtid
and b.batchid = n.batchid_fk
and n.recordid  = v.recordid 
and b.courtid not in (6,8,11)
--AND n.lettertype not in (19,25,21,18,33,36)
and n.lettertype in (9,20,22,24,27,28,32)
and datediff(Day, n.listdate, @sdate)<=0
and datediff(day, n.listdate, @edate)>=0
group by 	convert(varchar(10),  n.listdate,101), b.courtid,	N.courtid,	c.shortname	

-- LETTERS FROM TRAFFIC TICKETS DATABASE FOR DLQ CASES......
INSERT into #letters (listdate, categoryid, courtid, shortname, uploadtype, lettercount, violcount)
select convert(varchar(10),  n.listdate,101) ,	
	b.courtid,
	n.courtid,
	c.shortname,
	'DLQ',
	count(distinct n.noteid) as LetterCount,
	count(v.ticketnumber_pk) as LetterViolCount
from tblletternotes n, tblcourts c, tblticketsviolationsarchive v, tblbatchletter b, tblletternotes_detail d
where n.courtid= c.courtid
and n.noteid=  d.noteid
and b.batchid = n.batchid_fk
and d.recordid  = v.recordid 
AND N.lettertype = 19
and datediff(Day, n.listdate, @sdate)<=0
and datediff(day, n.listdate, @edate)>=0
group by 	convert(varchar(10),  n.listdate,101), b.courtid,	N.courtid,	c.shortname	

-- LETTERS FROM TRAFFIC TICKETS DATABASE FOR FTA CASES......
INSERT into #letters (listdate, categoryid, courtid, shortname, uploadtype, lettercount, violcount)
select convert(varchar(10),  n.listdate,101) ,	
	b.courtid,
	n.courtid,
	c.shortname,
	'WARRANT',
	count(distinct n.noteid) as LetterCount,
	count(v.ticketnumber_pk) as LetterViolCount
from tblletternotes n, tblcourts c, tblticketsviolationsarchive v, tblbatchletter b, tblletternotes_detail d
where n.courtid= c.courtid
and n.noteid=  d.noteid
and b.batchid = n.batchid_fk
and d.recordid  = v.recordid 
AND N.lettertype = 25
and datediff(Day, n.listdate, @sdate)<=0
and datediff(day, n.listdate, @edate)>=0
group by 	convert(varchar(10),  n.listdate,101), b.courtid,	N.courtid,	c.shortname	

-- LETTERS FROM DALLAS TRAFFIC TICKETS DATABASE REGULAR CASES......
INSERT into #letters (listdate, categoryid, courtid, shortname, uploadtype, lettercount, violcount)
select convert(varchar(10),  n.listdate,101) ,	
	b.courtid,
	n.courtid,
	c.shortname,
	'APP/ARR',
	count(distinct n.noteid) as LetterCount,
	count(v.ticketnumber_pk) as LetterViolCount
from tblletternotes n,  dallastraffictickets.dbo.tblcourts c, dallastraffictickets.dbo.tblticketsviolationsarchive v, tblbatchletter b
where n.courtid= c.courtid
and b.batchid = n.batchid_fk
and n.recordid  = v.recordid 
and b.courtid  in (6,11)
AND n.lettertype IN (29,31)
and datediff(Day, n.listdate, @sdate)<=0
and datediff(day, n.listdate, @edate)>=0
group by 	convert(varchar(10),  n.listdate,101), b.courtid,	N.courtid,	c.shortname	

-- LETTERS FROM DALLAS TRAFFIC TICKETS DATABASE BOND CASES......
INSERT into #letters (listdate, categoryid, courtid, shortname, uploadtype, lettercount, violcount)
select convert(varchar(10),  n.listdate,101) ,	
	b.courtid,
	n.courtid,
	c.shortname,
	'WARRANT',
	count(distinct n.noteid) as LetterCount,
	count(v.ticketnumber_pk) as LetterViolCount
from tblletternotes n,  dallastraffictickets.dbo.tblcourts c, dallastraffictickets.dbo.tblticketsviolationsarchive v, tblbatchletter b
where n.courtid= c.courtid
and b.batchid = n.batchid_fk
and n.recordid  = v.recordid 
and n.lettertype = 18
and datediff(Day, n.listdate, @sdate)<=0
and datediff(day, n.listdate, @edate)>=0
group by 	convert(varchar(10),  n.listdate,101), b.courtid,	N.courtid,	c.shortname	


-- LETTERS FROM JIMS DATABASE......
INSERT into #letters (listdate, categoryid, courtid, shortname, uploadtype, lettercount, violcount)
select convert(varchar(10),  n.listdate,101) ,	
	b.courtid,
	n.courtid,
	c.shortname,
	'APP/ARR',
	count(distinct n.noteid) as LetterCount,
	count(v.casenumber) as LetterViolCount
from tblletternotes n, tblcourts c, jims.dbo.criminalcases v, tblbatchletter b
where n.courtid= c.courtid
and b.batchid = n.batchid_fk
and n.recordid  = v.bookingnumber 
and b.courtid  = 8 
and c.courtid = 3037
and datediff(Day, n.listdate, @sdate)<=0
and datediff(day, n.listdate, @edate)>=0
group by 	convert(varchar(10),  n.listdate,101), b.courtid,	N.courtid,	c.shortname	


-- GETTING OUTPUT ........		
select 
	a.listdate, a.categoryid, a.courtid, case a.categoryid when 11 then 'D'+a.shortname else a.shortname end as shortname,  a.uploadtype, 
	a.uploadcount, a.violcount as uploadviolcount, 
	isnull(b.lettercount,0) as lettercount, 
	isnull(b.ViolCount,0) as letterviolcount,
	round((convert(float, isnull(b.lettercount,0)) * 100) / convert(float, a.uploadcount),2) as coveragepercentage
from #uploads a left outer join  #letters b 
on  a.categoryid = b.categoryid
and	a.courtid = b.courtid 
and a.listdate = b.listdate
and a.uploadtype = b.uploadtype
--where a.categoryid in (6,11)
order by  a.listdate desc, a.categoryid, a.courtid, a.shortname, a.uploadtype


 drop table #uploads
 drop table #letters

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


﻿  
/******    
* Created By :   Fahad Muhammad Qureshi.  
* Create Date :   10/15/2010 7:22:45 PM  
* Task ID :    8422  
* Business Logic :  This procedure is used to Get .  
* List of Parameter :  
* Column Return :       
******/  
ALTER PROCEDURE [dbo].[USP_HTP_AddDoctor]
	@FirstName VARCHAR(50),
	@LastName VARCHAR(50),
	@Address VARCHAR(50),
	@City VARCHAR(50),
	@State INT,
	@Zip VARCHAR(20),
	@DOB DATETIME,
	@DoctorId INT
AS
	IF (@DoctorId = 0)
	BEGIN
	    IF NOT EXISTS (
	           SELECT id
	           FROM   doctor
	           WHERE  LastName = @LastName
	                  AND FirstName = @FirstName
	                  AND ADDRESS = @Address
	                  AND City = @City
	                  AND [State] = @State
	                  AND Zip = @Zip
	                  AND DOB = @DOB
	       )
	    BEGIN
	        INSERT INTO Doctor
	          (
	            LastName,
	            FirstName,
	            [Address],
	            City,
	            [State],
	            Zip,
	            DOB
	          )
	        VALUES
	          (
	            @LastName,
	            @FirstName,
	            @Address,
	            @City,
	            @State,
	            @Zip,
	            @DOB
	          )  
	        SELECT SCOPE_IDENTITY()
	    END
	    ELSE
	    BEGIN
	        SELECT id
	        FROM   doctor
	        WHERE  LastName = @LastName
	               AND FirstName = @FirstName
	               AND ADDRESS = @Address
	               AND City = @City
	               AND [State] = @State
	               AND Zip = @Zip
	               AND DOB = @DOB
	    END
	END
	ELSE
	BEGIN
	    UPDATE Doctor
	    SET    FirstName = @FirstName,
	           LastName = @LastName,
	           [Address] = @Address,
	           City = @City,
	           [State] = @State,
	           Zip = @Zip,
	           DOB = @DOB
	    WHERE  id = @DoctorId  
	    
	    SELECT @DoctorId
	END  
   
   
GO
GRANT EXECUTE ON [dbo].[USP_HTP_AddDoctor]  TO dbr_webuser
GO 	  
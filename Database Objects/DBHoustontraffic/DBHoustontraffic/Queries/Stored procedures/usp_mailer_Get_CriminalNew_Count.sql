/****** 
Created by:		Tahir Ahmed
Business Logic:	The procedure is used by LMS application to get the number of printable letter
				count for the Harris County Criminal letter for the selected date range.
				Filters structure is not implemented for this procedure.
				
List of Parameters:
	@catnum:		Category number of the selected letter type, if printing for 
					all courts of the selected court category
	@LetterType:	Selected letter type in LMS
	@CrtId:			Court id of the selected court category if printing for individual court hosue.
	@startListdate:	Starting list date for Arraignment letter.
	@endListDAte:	Ending list date for Arraignment letter.
	@SearchType:	Flag that identifies if searching by court date or by list date. 0= list date, 1 = court date

List of Columns:	
	Total_Count:	Total number of letters exists for the selected date range.
	Good_Count:		Total number of letters that have valid addresses
	Bad_Count:		Total Number of letter that have undeliverable addresses
	DonotMail_Count:Total number of letter for addresses that had been marked to stop sending letters.
	

******/


--  usp_mailer_Get_CriminalNew_Count 8, 26, 8, '5/6/2008', '5/6/2008', 0
ALTER  Procedure [dbo].[usp_mailer_Get_CriminalNew_Count]
	(
	@catnum		int,
	@LetterType 	int,
	@crtid 		int,                                                 
	@startListdate 	Datetime,
	@endListdate 	DateTime,                                                      
	@SearchType     int                                             
	)

as                                                              
                               
-- DECLARING LOCAL VARIABLES FOR FILTERS....                             
declare	@officernum varchar(50), 
	@officeropr varchar(50), 
	@tikcetnumberopr varchar(50), 
	@ticket_no varchar(50), 
	@zipcode varchar(50), 
	@zipcodeopr varchar(50), 
	@fineamount money,
	@fineamountOpr varchar(50) ,
	@fineamountRelop varchar(50),
	@singleviolation money, 
	@singlevoilationOpr varchar(50) , 
	@singleviolationrelop varchar(50), 
	@doubleviolation money,
	@doublevoilationOpr varchar(50) , 
	@doubleviolationrelop varchar(50), 
	@count_Letters int,
	@violadesc varchar(500),
	@violaop varchar(50),
	@sqlquery nvarchar(MAX)  ,
	@sqlquery2 nvarchar(max)  ,
	@sqlParam nvarchar(1000)


-- SETTING PARAMETER LIST AND ORDER FOR DYNAMIC SQL.....
select @sqlparam = '@catnum int, @LetterType int, @crtid int, @startListdate Datetime, 	
		    @endListdate DateTime, @SearchType int'

-- INITIALIZING LOCAL VARIABLE FOR DYNAMIC SQL...                                                             
select @sqlquery ='', @sqlquery2 = ''
                                                              

-- GETTING DIFFERENT FILTER TYPES AND THIER VALUES IN THE LOCAL VARIABLES
-- IF ASSOCIACTED WITH THE LETTER.......
Select	@officernum=officernum,
	@officeropr=oficernumOpr,
	@tikcetnumberopr=tikcetnumberopr,
	@ticket_no=ticket_no,
	@zipcode=zipcode,
	@zipcodeopr=zipcodeLikeopr,
	@singleviolation =singleviolation,
	@singlevoilationOpr =singlevoilationOpr, 
	@singleviolationrelop =singleviolationrelop, 
	@doubleviolation = doubleviolation,
	@doublevoilationOpr=doubleviolationOpr,
	@doubleviolationrelop=doubleviolationrelop,
	@violadesc=violationdescription,
	@violaop=violationdescriptionOpr , 
	@fineamount=fineamount ,
	@fineamountOpr=fineamountOpr ,
	@fineamountRelop=fineamountRelop  
from 	tblmailer_letters_to_sendfilters  
where 	courtcategorynum=@catnum 
and 	Lettertype=@LetterType 

-- MAIN QUERY....
-- LIST DATE WISE NUMBER OF DISTINCT NON-CLIENTS
-- FOR THE SELECTED DATE RANGE ONLY 
-- INSERTING VALUES IN A TEMP TABLE....                                                      
set 	@sqlquery=@sqlquery+'                                          
 Select distinct      
	convert(datetime , convert(varchar(10),c.recdate,101)) as mdate,     
 flag1 = isnull(c.Flag1,''N'') ,                                                      
 isnull(c.donotmailflag,0) as donotmailflag, c.bookingnumber as recordid
into #temp                                                            
FROM jims.dbo.criminalcases c

-- EXCLUDE CERTAIN DISPOSITION TYPES....
where	disposition NOT in 
	(''CABS'',''CEXP'',''COMM'',''COTH'',''CREJ'',''CREQ'',''DADJ'',''DERR'',''DISM'',''DISP'',''GILT'',''NOB'',''NOTG'',''NPCF'',''PART'',''PDFC'',''PROB'',''USTP'') 

-- FOR SELECTED DATE RANGE ONLY...
and datediff(day, c.recdate , @startListdate)<=0
and datediff(day, c.recdate, @endlistdate) >=0

-- NOT MOVED IN PAST 48 MONTHS (ACCUZIP PROCESSED)...
and isnull(c.ncoa48flag,0) = 0

-- EXCLUDE LETTERS THAT HAVE ALREADY PRINTED...
and c.bookingnumber not in (                                                                    
	select	recordid from tblletternotes 
	where 	lettertype =  @LetterType   
	)



-- GETTING THE RESULT SET IN TEMP TABLE.....
Select distinct   mdate ,Flag1, donotmailflag, recordid into #temp1  from #temp                                                          
 '                                  

-- SUMMARIZING THE TOTAL NUMBER OF LETTERS......
-- GETTING LIST DATE WISE TOTAL NUMBER OF LETTERS....
set @sqlquery2=@sqlquery2 +'
Select count(distinct recordid) as Total_Count, mdate 
into #temp2 
from #temp1 
group by mdate 
  
-- GETTTING LIST DATE WISE TOTAL NUMBER OF PRINTABLE LETTERS...
Select count(distinct recordid) as Good_Count, mdate 
into #temp3
from #temp1 
where Flag1 in (''Y'',''D'',''S'')   
and donotmailflag = 0    
group by mdate   
                                                      
-- GETTING LIST DATE WISE TOTAL NUMBER OF NON-PRINTABLE LETTERS
Select count(distinct recordid) as Bad_Count, mdate  
into #temp4  
from #temp1 
where Flag1  not in (''Y'',''D'',''S'') 
and donotmailflag = 0 
group by mdate 
                   
-- GETTING LIST DATE WISE TOTAL NUMBER OF LETTERS THAT MARKED 
-- AS BLOCKED ADDRESSES.....
Select count(distinct recordid) as DonotMail_Count, mdate  
into #temp5   
from #temp1 
where donotmailflag=1 
group by mdate

-- GETTING ALL THE DATES IN TEMP TABLE THAT LIES BETWEEN THE 
-- SELECTED DATE RANGE.......
create table #letterdates (mdate datetime)
while datediff(day, @startListdate, @endlistdate)>=0
begin
	insert into #letterdates (mdate) select convert(varchar(10),@startlistdate,101)
	set @startlistdate = dateadd(day, 1, @startlistdate)
end
'
	
-- OUTPUTTING THE REQURIED INFORMATION........         
set @sqlquery2 = @sqlquery2 +'                                                        
Select 	l.mdate as listdate,
        isnull(Total_Count,0) as Total_Count,
        isnull(Good_Count,0) Good_Count,  
        isnull(Bad_Count,0)Bad_Count,  
        isnull(DonotMail_Count,0)DonotMail_Count  
from #letterdates l left outer join #Temp2 on #temp2.mdate = l.mdate left outer join #temp3 
on #temp2.mdate=#temp3.mdate  
left outer join #Temp4  
on #temp2.mdate=#Temp4.mdate    
left outer join #Temp5  
on #temp2.mdate=#Temp5.mdate                                                       
order by l.mdate

-- DROPPING THE TEMPORARY TABLES USED ABOVE.....
drop table #temp drop table #temp1 drop table #temp2  drop table #temp3  drop table #temp4  drop table #temp5  drop table #letterdates
'                                                      
                                                      
--print @sqlquery  + @sqlquery2                                       

-- CONCATENATING THE QUERY ....
set @sqlquery = @sqlquery + @sqlquery2

-- EXECUTING THE DYNAMIC SQL QUERY...
exec sp_executesql @sqlquery , @sqlparam, @catnum, @LetterType, @crtid, @startListdate, @endListdate, @SearchType

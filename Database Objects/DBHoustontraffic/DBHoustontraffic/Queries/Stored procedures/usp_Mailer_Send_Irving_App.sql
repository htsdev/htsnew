USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[usp_Mailer_Send_Irving_App]    Script Date: 01/13/2011 00:37:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** 
Business Logic:	The procedure is used by LMS application to get data for Irving Appearance 
				and to create LMS history for this letter for the selected date range.
				
List of Parameters:
	@catnum:		Category number of the selected letter type, if printing for 
					all courts of the selected court category
	@courtid:			Court id of the selected court category if printing for individual court hosue.
	@startListdate:	Comma separated dates selected to print letter.
	@EmpId:			Employee information who is printing the letter.
	@PrintType:		Flag to just preview the letter or to mark letters as sent. If 1 then LMS letter history will be created.
	@language:      Language i.e. english or spanish.
	@bondFlag:      Bond flag is set or not.
	@isPrinted:		Flag to include already printed letters in the batch.

List of Columns:	
	LetterId:		Letter ID generated for each letter. It will be "NOT PRINTABLE"	if @PrintType = 0
	RecordId:		Identity value to look up the case in non-clients. used to group the records in report file.
	FirstName:		Person's first name that will appear on the letter
	LastName:		Person's last name that will appear on the letter
	Address:		Person's Home address that will appear on the letter
	City:			Person's Home City that will appear on the letter
	State:			Person's Home State that will appear on the letter
	FineAmount:		Fine amount for each violation that will appear on the letter
	ViolationDescritpion: violation description that will appear on the letter
	CourtName:		Name of court with which the violation is associated. Used in report file
	DPC:			Address status that is used in barcode font on the letter
	DPTwo:			Address status that is used in barcode font on the letter
	TicketNumber_pk:Case number for the violation
	ZipCode:		Person's Home Zip code.
	MidNumber:		MidNumber of the person associated with the case.
	Abb:			Short abbreviation of employee who is printing the letter

*******/
--  usp_Mailer_Send_Irving_App 6, 0, 6, '2/1/2008,' , 3991, 0, 0, 0
ALTER procedure [dbo].[usp_Mailer_Send_Irving_App]                           
	(
	@catnum		int=6,                                    
	@bondFlag 	int=0,                                    
	@courtid 	int=6,                                    
	@startListdate 	varchar (500) = '03/24/2006,' ,                                    
	@empid 		int=3991,                              
	@printtype 	int =0 ,
	@language	int = 1  ,  -- by default print both English & Spanish letters   ,
	@isprinted bit                              
	)
as                                    
                                          
declare	@officernum varchar(50),                                    
	@officeropr varchar(50),                                    
	@tikcetnumberopr varchar(50),                                    
	@ticket_no varchar(50),                                                                    
	@zipcode varchar(50),                                             
	@zipcodeopr varchar(50),                                    
	@fineamount money,                                            
	@fineamountOpr varchar(50) ,                                            
	@fineamountRelop varchar(50),                   
	@singleviolation money,                                            
	@singlevoilationOpr varchar(50) ,                                            
	@singleviolationrelop varchar(50),                                    
	@doubleviolation money,                                            
	@doublevoilationOpr varchar(50) ,                                            
	@doubleviolationrelop varchar(50),                                  
	@count_Letters int,                  
	@violadesc varchar(500),                  
	@violaop varchar(50),
	@LetterType int,
	@lettername varchar(50)

if @bondflag = 0
begin
	set @lettertype = 49
	set @lettername = 'Irving Appearance'
end

-- 6 copies of the letter will be mailed to Greg on this date for testing purposes
declare @tempDate datetime
set @tempDate = '2/4/2008'
-----------------------
                                    
declare @sqlquery varchar(8000)  ,
	@sqlquery2 varchar(8000)  ,
	@sqlquery3 varchar(8000)                                
                                        
                                            
Select	@officernum=officernum,                                    
	@officeropr=oficernumOpr,                                    
	@tikcetnumberopr=tikcetnumberopr,                                    
	@ticket_no=ticket_no,                                                                      
	@zipcode=zipcode,                                    
	@zipcodeopr=zipcodeLikeopr,                                    
	@singleviolation =singleviolation,                                    
	@singlevoilationOpr =singlevoilationOpr,                                    
	@singleviolationrelop =singleviolationrelop,                                    
	@doubleviolation = doubleviolation,                                    
	@doublevoilationOpr=doubleviolationOpr,                                            
	@doubleviolationrelop=doubleviolationrelop,                  
	@violadesc=violationdescription,                  
	@violaop=violationdescriptionOpr ,              
	@fineamount=fineamount ,                                            
	@fineamountOpr=fineamountOpr ,                                            
	@fineamountRelop=fineamountRelop              
from 	tblmailer_letters_to_sendfilters                                    
where 	courtcategorynum=@catnum 
and lettertype =   @lettertype                                 
                                 



Select @sqlquery ='', @sqlquery2 =''  , @sqlquery3 = ''

declare @dateVar varchar(25)

declare @user varchar(10)
select @user = upper(abbreviation) from tblusers where employeeid = @empid
set @dateVar = ' ta.recloaddate '                                    
set @sqlquery=@sqlquery+'  declare @recCount int                                  

Select	distinct 
	tva.recordid, 
	mdate = convert(datetime,convert(varchar(10),ta.recloaddate,101)),
	count(tva.ViolationNumber_PK) as ViolCount,
	sum(isnull(tva.fineamount,0))as Total_Fineamount 
into #temptable                                          
FROM   	dallastraffictickets.dbo.tblTicketsViolationsArchive tva 
inner join 
	dallastraffictickets.dbo.tblTicketsArchive ta 
on 	tva.recordid = ta.recordid
inner join  dallastraffictickets.dbo.tblcourts tc  on 	ta.courtid = tc.courtid
where	ta.lastname is not null  and ta.firstname is not null
and isnull(ta.ncoa48flag,0) = 0 	and  tva.violationbonddate is null and charindex(''v'',tva.ticketnumber_pk)<>1
and 	Flag1 in (''Y'',''D'',''S'')  and 	isnull(donotmailflag,0)  =0 '
          

if(@courtid= @catnum)
	set @sqlquery=@sqlquery+' and tva.courtlocation In (Select courtid from dallastraffictickets.dbo.tblcourts where courtcategorynum  = '+convert(varchar(10),@catnum ) +')'          
else
	set @sqlquery=@sqlquery+' and tva.courtlocation=' + convert(varchar,@courtid)
                                    
if(@startListdate<>'')                                    
	set @sqlquery=@sqlquery+ ' 
	and '+dbo.Get_String_Concat_With_DatePart_ver2(''+@startListdate +'', @dateVar ) 
                                    
if(@officernum<>'')                                    
	set @sqlquery =@sqlquery +'  
	and 	ta.officerNumber_Fk '+@officeropr +'('+ @officernum+')'                                     
                                    
if(@ticket_no<>'')                                    
	set @sqlquery=@sqlquery+ '
	and  	('+ dbo.Get_String_Concat_With_op(''+@ticket_no+'' , 'tva.TicketNumber_PK' ,+ ''+ @tikcetnumberopr)+')'                                          
                
if(@zipcode<>'')                                                          
	set @sqlquery=@sqlquery+ ' 
	and	('+  dbo.Get_String_Concat_With_op(''+@zipcode+'' , 'left(ta.zipcode,2)' ,+ ''+ @zipcodeopr) +')'              

if(@fineamount<>0 and @fineamountOpr<>'not'  )                                
set @sqlquery =@sqlquery+ '
      and tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                                     

if(@fineamount<>0 and @fineamountOpr = 'not'  )                                
set @sqlquery =@sqlquery+ '
      and not tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                                     

--Yasir Kamal 7365 02/01/2010 exclude letters
-- EXCLUDE THE LETTERS THAT HAVE BEEN ALREADY PRINTED......
-- OR APP DAY OF OR WARRANT LETTER PRINTED IN PAST 3 DAYS FOR THE SAME RECORD.. 
set @sqlquery =@sqlquery + '                                              
	and ta.recordid  not in   
	(                                                                    
 	select n.recordid FROM dbo.tblLetterNotes n inner join dallastraffictickets.dbo.tblticketsarchive t
		on t.recordid  = n.recordid  where lettertype = 49
    union
	select n.recordid FROM dbo.tblLetterNotes n inner join dallastraffictickets.dbo.tblticketsarchive t
	on t.recordid  = n.recordid  where lettertype in  (50,51)
	-- Babar Ahmad 8597 07/12/2011 Changed number of days from 3 to 5
	and datediff(day, n.recordloaddate, dbo.fn_getprevbusinessday(GETDATE(),5)) <=0    
	)     
'


set @sqlquery =@sqlquery+'                                         
group by   tva.recordid,  convert(datetime,convert(varchar(10),ta.recloaddate,101))
having 1=1 
'            

if(@singleviolation<>0 and @doubleviolation=0)                                
	set @sqlquery =@sqlquery+ '
	and (  '+ @singlevoilationOpr+  '  (isnull(sum(isnull(tva.fineamount,0)),0) '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and count(tva.ViolationNumber_PK)=1))
      '                                
if(@doubleviolation<>0 and @singleviolation=0 )                                
	set @sqlquery =@sqlquery + '
	and ('+ @doublevoilationOpr+  '   (isnull(sum(isnull(tva.fineamount,0)),0) '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ')and count(tva.ViolationNumber_PK)=2) )
      '                                                    
                                
if(@doubleviolation<>0 and @singleviolation<>0)                                
	set @sqlquery =@sqlquery+ '
	and ('+ @singlevoilationOpr+  '  (isnull(sum(isnull(tva.fineamount,0)),0) '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and count(tva.ViolationNumber_PK)=1))
    and ('+  @doublevoilationOpr+  '   (isnull(sum(isnull(tva.fineamount,0)),0) '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ') and count(tva.ViolationNumber_PK)=2))
        '                                  
  

set @sqlquery =@sqlquery+'                             
Select	distinct 
	a.recordid, 
	a.mdate ,
	tva.courtdate,	ta.courtid,  flag1 = isnull(ta.Flag1,''N'') , ta.officerNumber_Fk, 
	tva.TicketNumber_PK, isnull(ta.donotmailflag,0) as donotmailflag,
	ta.clientflag, 	upper(firstname) as firstname, upper(lastname) as lastname, upper(address1) + '''' + isnull(ta.address2,'''') as address,                      
	upper(ta.city) as city, s.state,
	tc.CourtName, zipcode, 	midnumber,  dp2 as dptwo, dpc, 	violationdate, 	violationstatusid, 
	left(zipcode,5) + rtrim(ltrim(midnumber)) as zipmid,
	ViolationDescription, case when isnull(tva.fineamount,0)=0 then 100 else convert(numeric(10,0),tva.fineamount) end as FineAmount, 
	a.ViolCount, a.Total_Fineamount 
into #temp                                          
FROM   	dallastraffictickets.dbo.tblTicketsViolationsArchive tva 
inner join 
	dallastraffictickets.dbo.tblTicketsArchive ta 
on 	tva.recordid = ta.recordid
inner join  dallastraffictickets.dbo.tblcourts tc  on 	ta.courtid = tc.courtid
INNER JOIN
tblstate S ON
ta.stateid_fk = S.stateID
inner join #temptable a on a.recordid = ta.recordid where	ta.lastname is not null  and ta.firstname is not null
and isnull(ta.ncoa48flag,0) = 0 	and  tva.violationbonddate is null and charindex(''v'',tva.ticketnumber_pk)<>1
and 	Flag1 in (''Y'',''D'',''S'')  and 	isnull(donotmailflag,0)  =0 
'

-- VIOLATION DESCRIPTION FILTER.....
if(@violadesc <> '')
begin
-- NOT LIKE FILTER.......
if(charindex('not',@violaop)<> 0 )              
	set @sqlquery=@sqlquery+' 
	and	 not ( a.violcount =1  and ('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ 'like')+'))'           

-- LIKE FILTER.......
if(charindex('not',@violaop) =  0 )              
	set @sqlquery=@sqlquery+' 
	and	 ( a.violcount =1  and ('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ 'like')+'))'           
end
                 
                            
set @sqlquery=@sqlquery+                     
'
-- Rab Nawaz Khan 8754 01/12/2011 Exclude Disposed Violations
DELETE FROM #temp
WHERE #temp.violationstatusid = 80
-- END 8754

Select distinct recordid, violationstatusid,ViolCount,Total_Fineamount, mdate, FirstName,LastName,address,                              
 city,state,FineAmount,violationdescription,CourtName,dpc,dptwo,Flag1,officerNumber_Fk,TicketNumber_PK,  
  --Yasir Kamal 7226 01/20/2010 get promotional price                                         
 courtid,zipmid,donotmailflag,clientflag, zipcode, midnumber, courtdate,dbo.GetPromoPriceTemplate(zipcode,'+convert(Varchar,@lettertype)+') AS promotemplate into #temp1  from #temp                                    
 '            
 
 if datediff(Day, @tempdate, getdate()) = 0
set @sqlquery=@sqlquery+ 
'
insert into #temp1 
select ''1'', 116, 2, 150, '''+ convert(Varchar(10), @tempdate, 101) +''', ''GREG'', ''SULLO'', ''6023 Fordham'',
''Houston'', ''TX'', 100, ''TESTING'', ''TESTING'', 0,00, ''Y'', 0, ''TESTING 1'', 3049, ''77005'', 0, 0, ''77005'', '''', ''2/20/2008'',''0,35.00''
UNION ALL
select ''2'', 116, 2, 150, '''+ convert(Varchar(10), @tempdate, 101) +''', ''GREG'', ''SULLO'', ''6023 Fordham'',
''Houston'', ''TX'', 100, ''TESTING'', ''TESTING'', 0,00, ''Y'', 0, ''TESTING 2'', 3049, ''77005'', 0, 0, ''77005'', '''', ''2/20/2008'',''0,35.00''
UNION ALL
select ''3'', 116, 2, 150, '''+ convert(Varchar(10), @tempdate, 101) +''', ''GREG'', ''SULLO'', ''6023 Fordham'',
''Houston'', ''TX'', 100, ''TESTING'', ''TESTING'', 0,00, ''Y'', 0, ''TESTING 3'', 3049, ''77005'', 0, 0, ''77005'', '''', ''2/20/2008'',''0,35.00''
UNION ALL
select ''4'', 116, 2, 150, '''+ convert(Varchar(10), @tempdate, 101) +''', ''GREG'', ''SULLO'', ''6023 Fordham'',
''Houston'', ''TX'', 100, ''TESTING'', ''TESTING'', 0,00, ''Y'', 0, ''TESTING 4'', 3049, ''77005'', 0, 0, ''77005'', '''', ''2/20/2008'',''0,35.00''
UNION ALL
select ''5'', 116, 2, 150, '''+ convert(Varchar(10), @tempdate, 101) +''', ''GREG'', ''SULLO'', ''6023 Fordham'',
''Houston'', ''TX'', 100, ''TESTING'', ''TESTING'', 0,00, ''Y'', 0, ''TESTING 5'', 3049, ''77005'', 0, 0, ''77005'', '''', ''2/20/2008'',''0,35.00''
UNION ALL
select ''6'', 116, 2, 150, '''+ convert(Varchar(10), @tempdate, 101) +''', ''GREG'', ''SULLO'', ''6023 Fordham'',
''Houston'', ''TX'', 100, ''TESTING'', ''TESTING'', 0,00, ''Y'', 0, ''TESTING 6'', 3049, ''77005'', 0, 0, ''77005'', '''', ''2/20/2008'',''0,35.00''
'  

  set @sqlquery=@sqlquery+                       
' -- Sabir Khan 7099 12/07/2009 exclude client cases
delete from #temp1 where recordid in (
select v.recordid from dallastraffictickets.dbo.tblticketsviolations v inner join dallastraffictickets.dbo.tbltickets t 
on t.ticketid_pk = v.ticketid_pk and t.activeflag = 1 
inner join #temp1 a on a.recordid = v.recordid
) 
 '                       
              
if( @printtype<>0)         
set @sqlquery2 =@sqlquery2 +                                    
'
	Declare @ListdateVal DateTime, @totalrecs int,@count int, @p_EachLetter money,@recordid varchar(50),                              
		@zipcode   varchar(12),@maxbatch int  ,@lCourtId int  ,@dptwo varchar(10),@dpc varchar(10)
	declare @tempBatchIDs table (batchid int)
	Select @totalrecs =count(*) from #temp1 where mdate = @ListdateVal                                 
	Select @count=Count(*) from #temp1                                

	set @p_EachLetter=convert(money,0.50)                                
                              
	Declare ListdateCur Cursor for                                  
	Select distinct mdate  from #temp1                                
	open ListdateCur                                   
	Fetch Next from ListdateCur into @ListdateVal                                                      
	while (@@Fetch_Status=0)                              
		begin                                
			Select @totalrecs =count(distinct recordid) from #temp1 where mdate = @ListdateVal                                 

			insert into tblBatchLetter(Empid, Lettertype,Listdate,CourtId,ParentID,LCount,PExpense) values                                  
			('+ convert(varchar(10),@empid) +', '+ convert(varchar(10),@lettertype) +' , @ListdateVal, '+ convert(varchar(10),@catnum) +', 0, @totalrecs, @p_EachLetter)                                   

			Select @maxbatch=Max(BatchId) from tblBatchLetter 
				insert into @tempBatchIDs select @maxbatch                                                       
				Declare RecordidCur Cursor for                               
				Select distinct recordid,zipcode, courtid, dptwo, dpc from #temp1 where mdate = @ListdateVal                               
				open RecordidCur                                                         
				Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc                                          
				while(@@Fetch_Status=0)                              
				begin                              
					insert into tblLetterNotes(BatchId_Fk,listdate,ZipCode,RecordID,PCost,LetterType, courtid, dp2, dpc)                              
					values (@maxbatch,@ListdateVal,@zipcode,@recordid,@p_EachLetter,'+ convert(varchar(10),@lettertype) +',@lCourtId, @dptwo, @dpc)                              
					Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc                          
				end                              
				close RecordidCur 
				deallocate RecordidCur                                                               
			Fetch Next from ListdateCur into @ListdateVal                              
		end                                            

	close ListdateCur  
	deallocate ListdateCur 

	 Select distinct convert(varchar(20),n.noteid) as letterid, t.recordid,  t.FirstName,                              
	 t.LastName,t.address,t.city, t.state,FineAmount,violationdescription,CourtName, t.dpc,   
	  --Yasir Kamal 7226 01/20/2010 get promotional price                                 
	 dptwo, TicketNumber_PK, t.zipcode  , courtdate, '''+@user+''' as Abb,promotemplate                                   
	 into #temp2
	from #temp1 t , tblletternotes n, @tempBatchIDs tb
	where t.recordid = n.recordid and t.courtid = n.courtid and n.batchid_fk = tb.batchid and n.lettertype = '+convert(varchar(10),@lettertype)+' 
	 order by T.zipcode 
 select @recCount = count(distinct recordid) from #temp2 

declare @lettercount int, @subject varchar(200), @body varchar(400) , @sql varchar(500)
select @lettercount = count(distinct n.noteid) from tblletternotes n, @tempBatchIDs b where n.batchid_fk = b.batchid
select @subject =  convert(varchar(20), @lettercount) + '' ''  +  '''+ @lettername +' Letters Printed''
select @body = convert(varchar(20), @lettercount) + '' LETTERS ('' + CONVERt(VARCHAR(12), GETDATE()) + '') - ''+ upper( '''+@user+''')
exec usp_mailer_send_Email @subject, @body

declare @batchIDs_filname varchar(500)
set @batchIDs_filname = ''''
select @batchIDs_filname = @batchIDs_filname + convert(varchar,batchid) + '',''  from @tempBatchIDs


'

else
	set @sqlquery2 = @sqlquery2 + '
	 Select distinct ''NOT PRINTABLE'' as letterId,  recordid,  FirstName,                              
	 LastName,address,city, state,FineAmount,violationdescription,CourtName, dpc,dptwo, TicketNumber_PK, zipcode , courtdate, '''+@user+''' as Abb,promotemplate                                    
	 into #temp3
	 from #temp1 
	 order by zipcode 
 select @recCount = count(distinct recordid) from #temp3
 '
	

if @language = 0
	begin
		if @printtype = 0
			select @sqlquery3 = @sqlquery3 + ' 
			select t1.*, 0 as language, convert(varchar(15), t1.recordid) + ''0'' as ChkGroup from #temp3 t1 order by t1.zipcode'
		
		if @printtype = 1
			select @sqlquery3 = @sqlquery3 + ' 
			select t2.*, 0 as language, convert(varchar(15), t2.recordid) + ''0'' as ChkGroup from #temp2 t2 
			order by [zipcode]
			
			select isnull(@batchIDs_filname,'''') as batchid
 '
	end
else
	begin
		if @printtype = 0
			select @sqlquery3 = @sqlquery3 + ' 
			declare @table4 TABLE  (
				[letterId] [varchar] (13)  ,
				[recordid] [int] ,
				[FirstName] [varchar] (20)  ,
				[LastName] [varchar] (20)  ,
				[address] [varchar] (100) ,
				[city] [varchar] (50) ,
				[state] [varchar] (2)  ,
				[FineAmount] [money]  ,
				[violationdescription] [varchar] (200) ,
				[CourtName] [varchar] (50) ,
				[dpc] [varchar] (10)  ,
				[dptwo] [varchar] (10)  ,
				[TicketNumber_PK] [varchar] (20)  ,
				[zipcode] [varchar] (15) ,
				[language] [int],
				[courtdate] [datetime],
				[Abb] [varchar] (10),
				 --Yasir Kamal 7226 01/20/2010 get promotional price      
				[promotemplate] [varchar] (100)
			)

			insert into @table4 (letterid, recordid, firstname, lastname, address, city, state, fineamount,
					violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb,promotemplate)		

			select letterid, recordid, firstname, lastname, address, city, state, fineamount,
					violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 0, courtdate, Abb,promotemplate from #temp3 	

			insert into @table4 (letterid, recordid, firstname, lastname, address, city, state, fineamount,
					violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb,promotemplate)		

			select letterid, recordid, firstname, lastname, address, city, state, fineamount,
					violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 1, courtdate, Abb,promotemplate from #temp3 ;	


			select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table4 order by zipcode, language ;
'

		if @printtype = 1 	
			select @sqlquery3 = @sqlquery3 + ' 
			declare @table4 TABLE  (
				[letterId] [varchar] (13)  ,
				[recordid] [int] ,
				[FirstName] [varchar] (20)  ,
				[LastName] [varchar] (20)  ,
				[address] [varchar] (100) ,
				[city] [varchar] (50) ,
				[state] [varchar] (2)  ,
				[FineAmount] [money]  ,
				[violationdescription] [varchar] (200) ,
				[CourtName] [varchar] (50) ,
				[dpc] [varchar] (10)  ,
				[dptwo] [varchar] (10)  ,
				[TicketNumber_PK] [varchar] (20)  ,
				[zipcode] [varchar] (15) ,
				[language] [int], 
				[courtdate] [datetime],
				[Abb] [varchar] (10),
				[promotemplate] [varchar] (100)
			)

			insert into @table4 (letterid, recordid, firstname, lastname, address, city, state, fineamount,
					violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb,promotemplate)		

			select letterid, recordid, firstname, lastname, address, city, state, fineamount,
					violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 0 , courtdate, Abb,promotemplate from #temp2 	

			insert into @table4 (letterid, recordid, firstname, lastname, address, city, state, fineamount,
					violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb,promotemplate)		

			select letterid, recordid, firstname, lastname, address, city, state, fineamount,
					violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 1 , courtdate, Abb,promotemplate from #temp2 ;	

                        
			select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table4 
			order by [zipcode], [language]

			select isnull(@batchIDs_filname,'''') as batchid 
'
	END
	--7226 end      
                                    
--select  len(@sqlquery), len(@sqlquery2) , len(@sqlquery3)
--print @sqlquery + @sqlquery2 + @sqlquery3
exec  ( @sqlquery + @sqlquery2 + @sqlquery3 )


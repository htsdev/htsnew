
-- =============================================
-- Author:		Tahir Ahmed
-- Create date: 07/21/2008
-- Description:	The stored procedure is used in HTP /Activies /Donot Mail Update application.
--				It is used to set donot mail flag for a person to exclude the person's name  
--				and address from our mailing lists. 
--
-- List Of Parameters:
--		@DBId:		Database Id related to the record.
--		@RecordID:	Unique identified for the case in above specified database
--
-- List of output columns: none
--
-- Dependent Procedure : 
-- Procedure usp_htp_update_usps_donotmailflag from USPSTracking service is using this procedure.
--
-- =============================================


-- USP_HTP_Update_SetNoMailFlag   1,5212762
ALTER procedure dbo.USP_HTP_Update_SetNoMailFlag 
	(
	@DBId int,
	@RecordId int
	)


as

-- houston traffic
if @dbid = 1
	begin
		INSERT INTO tbl_hts_donotmail_records  (FirstName, LastName, Address, Zip, City, StateID_FK)
		select firstname, lastname, address1 + isnull(' ' + address2,''), case when len(ZipCode)>5 then substring(ZipCode,1,5) else ZipCode end, city, stateid_fk
		from tblticketsarchive where recordid  = @recordid

		update tblticketsarchive 
		set donotmailflag = 1 
		where recordid= @recordid
		
	end

-- dallas traffic
if @dbid = 2
	begin
		INSERT INTO tbl_hts_donotmail_records  (FirstName, LastName, Address, Zip, City, StateID_FK)
		select firstname, lastname, address1 + isnull(' ' + address2,''), case when len(ZipCode)>5 then substring(ZipCode,1,5) else ZipCode end, city, stateid_fk
		from dallasTrafficTickets.dbo.tblticketsarchive where recordid  = @recordid

		update dallasTrafficTickets.dbo.tblticketsarchive 
		set donotmailflag = 1 
		where recordid= @recordid
	end

-- harris criminal
if @dbid = 3
	begin
		INSERT INTO tbl_hts_donotmail_records  (FirstName, LastName, Address, Zip, City, StateID_FK)
		select firstname, lastname, address, case when len(Zip)>5 then substring(Zip,1,5) else Zip end, city, s.stateid
		from jims.dbo.criminalcases c inner join tblstate s on s.state = c.state
		where c.bookingnumber  = @recordid

		update jims.dbo.criminalcases  
		set donotmailflag = 1 
		where bookingnumber = @recordid
	end

-- harris civil
if @dbid = 5
	begin
		INSERT INTO tbl_hts_donotmail_records  (FirstName, LastName, Address, Zip, City, StateID_FK)
		select firstname, lastname, address, case when len(Zipcode)>5 then substring(Zipcode,1,5) else Zipcode end, city, stateid
		from civil.dbo.childcustodyparties
		where childcustodypartyid = @recordid

		update civil.dbo.childcustodyparties
		set donotmailflag = 1 
		where childcustodypartyid = @recordid
	end

--Sabir Khan 9921 12/08/2011 Added Assumed Database 
if @dbid = 6
	begin
		INSERT INTO tbl_hts_donotmail_records  (FirstName, LastName, Address, Zip, City, StateID_FK)
		SELECT LEFT(BusinessName,20) AS Businessname, NULL, [Address] , case when len(Zip)>5 then substring(Zip,1,5) else Zip end, city, ISNULL(ts.StateID,0)
		FROM AssumedNames.dbo.business b
		LEFT OUTER JOIN tblState ts ON ts.[State] = b.[State] 
		WHERE BusinessID  = @recordid

		update AssumedNames.dbo.business
		SET NoMailflag  = 1 
		WHERE BusinessID = @recordid
		
		update AssumedNames.dbo.BusinessOwner
		SET NoMailflag  = 1 
		WHERE BusinessID = @recordid
		
	end


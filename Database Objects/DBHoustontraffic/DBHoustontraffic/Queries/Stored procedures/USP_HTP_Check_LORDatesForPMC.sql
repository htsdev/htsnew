
/****** 
Created By:		Sabir Khan
Business Logic:	This stored procedure is used to check and return true if all LOR dates is in past or full else return false.
*******/
ALTER procedure [dbo].[USP_HTP_Check_LORDatesForPMC]            
          
as  

DECLARE @LORDate1 DATETIME
DECLARE @LORDate2 DATETIME
DECLARE @LORDate3 DATETIME
DECLARE @LORDate4 DATETIME
DECLARE @LORDate5 DATETIME
DECLARE @LORDate6 DATETIME
DECLARE @Maxout INT
DECLARE @DateIsInPastOrFull INT

SELECT @LORDate1= RepDate,@LORDate2 =LORRepDate2,@LORDate3 =LORRepDate3,
@LORDate4 =LORRepDate4,@LORDate5=LORRepDate5,@LORDate6=LORRepDate6
FROM tblCourts WHERE Courtid = 3004

SET @Maxout = (SELECT v.[Value]  FROM   Configuration.dbo.[Value] v WHERE  v.DivisionID = 1 AND v.ModuleID = 4
	               AND v.SubModuleID = 14 AND v.KeyID = 26)

IF DATEDIFF(DAY,@LORDate1,GETDATE())>0 AND DATEDIFF(DAY,@LORDate2,GETDATE())>0 AND DATEDIFF(DAY,@LORDate3,GETDATE())>0 AND DATEDIFF(DAY,@LORDate4,GETDATE())>0 AND DATEDIFF(DAY,@LORDate5,GETDATE())>0 AND DATEDIFF(DAY,@LORDate6,GETDATE())>0
BEGIN
	SET @DateIsInPastOrFull = 1
END
ELSE
BEGIN
	SET @DateIsInPastOrFull = 0
END

IF @DateIsInPastOrFull = 0
BEGIN
	DECLARE @TotalRepDate1 INT
	DECLARE @TotalRepDate2 INT
	DECLARE @TotalRepDate3 INT
	DECLARE @TotalRepDate4 INT
	DECLARE @TotalRepDate5 INT
	DECLARE @TotalRepDate6 INT
	
   
   SELECT @TotalRepDate1 = COUNT(DISTINCT t.TicketID_PK)
   FROM   tblTicketsViolations tv INNER JOIN tbltickets t  ON  t.TicketID_PK = tv.TicketID_PK	   
   INNER JOIN faxLetter fl ON fl.Ticketid = t.TicketID_PK		   
   WHERE  t.Activeflag = 1 AND tv.CourtID = 3004 AND DATEDIFF(DAY, fl.PMCRepDate, @LORDate1) =0
   SELECT @TotalRepDate1 = @TotalRepDate1 + COUNT(DISTINCT t.TicketID_PK)
   FROM   tblTicketsViolations tv INNER JOIN tbltickets t  ON  t.TicketID_PK = tv.TicketID_PK
   INNER JOIN tblLORBatchHistory tlh ON tlh.TicketID_PK = t.TicketID_PK	      
   WHERE  t.Activeflag = 1 AND tv.CourtID = 3004 AND DATEDIFF(DAY,tlh.RepDate,@LORDate1) = 0
   
   
   
   SELECT @TotalRepDate2 = COUNT(DISTINCT t.TicketID_PK)
   FROM   tblTicketsViolations tv INNER JOIN tbltickets t  ON  t.TicketID_PK = tv.TicketID_PK	   
   INNER JOIN faxLetter fl ON fl.Ticketid = t.TicketID_PK		   
   WHERE  t.Activeflag = 1 AND tv.CourtID = 3004 AND DATEDIFF(DAY, fl.PMCRepDate, @LORDate2) =0
   SELECT @TotalRepDate2 = @TotalRepDate2 + COUNT(DISTINCT t.TicketID_PK)
   FROM   tblTicketsViolations tv INNER JOIN tbltickets t  ON  t.TicketID_PK = tv.TicketID_PK
   INNER JOIN tblLORBatchHistory tlh ON tlh.TicketID_PK = t.TicketID_PK	      
   WHERE  t.Activeflag = 1 AND tv.CourtID = 3004 AND DATEDIFF(DAY,tlh.RepDate,@LORDate2) = 0
   
   
   SELECT @TotalRepDate3 = COUNT(DISTINCT t.TicketID_PK)
   FROM   tblTicketsViolations tv INNER JOIN tbltickets t  ON  t.TicketID_PK = tv.TicketID_PK	   
   INNER JOIN faxLetter fl ON fl.Ticketid = t.TicketID_PK		   
   WHERE  t.Activeflag = 1 AND tv.CourtID = 3004 AND DATEDIFF(DAY, fl.PMCRepDate, @LORDate3) =0
   SELECT @TotalRepDate3 = @TotalRepDate3 + COUNT(DISTINCT t.TicketID_PK)
   FROM   tblTicketsViolations tv INNER JOIN tbltickets t  ON  t.TicketID_PK = tv.TicketID_PK
   INNER JOIN tblLORBatchHistory tlh ON tlh.TicketID_PK = t.TicketID_PK	      
   WHERE  t.Activeflag = 1 AND tv.CourtID = 3004 AND DATEDIFF(DAY,tlh.RepDate,@LORDate3) = 0
   
   SELECT @TotalRepDate4 = COUNT(DISTINCT t.TicketID_PK)
   FROM   tblTicketsViolations tv INNER JOIN tbltickets t  ON  t.TicketID_PK = tv.TicketID_PK	   
   INNER JOIN faxLetter fl ON fl.Ticketid = t.TicketID_PK		   
   WHERE  t.Activeflag = 1 AND tv.CourtID = 3004 AND DATEDIFF(DAY, fl.PMCRepDate, @LORDate4) =0
   SELECT @TotalRepDate4 = @TotalRepDate4 + COUNT(DISTINCT t.TicketID_PK)
   FROM   tblTicketsViolations tv INNER JOIN tbltickets t  ON  t.TicketID_PK = tv.TicketID_PK
   INNER JOIN tblLORBatchHistory tlh ON tlh.TicketID_PK = t.TicketID_PK	      
   WHERE  t.Activeflag = 1 AND tv.CourtID = 3004 AND DATEDIFF(DAY,tlh.RepDate,@LORDate4) = 0
   
   SELECT @TotalRepDate5 = COUNT(DISTINCT t.TicketID_PK)
   FROM   tblTicketsViolations tv INNER JOIN tbltickets t  ON  t.TicketID_PK = tv.TicketID_PK	   
   INNER JOIN faxLetter fl ON fl.Ticketid = t.TicketID_PK		   
   WHERE  t.Activeflag = 1 AND tv.CourtID = 3004 AND DATEDIFF(DAY, fl.PMCRepDate, @LORDate5) =0
   SELECT @TotalRepDate5 = @TotalRepDate5 + COUNT(DISTINCT t.TicketID_PK)
   FROM   tblTicketsViolations tv INNER JOIN tbltickets t  ON  t.TicketID_PK = tv.TicketID_PK
   INNER JOIN tblLORBatchHistory tlh ON tlh.TicketID_PK = t.TicketID_PK	      
   WHERE  t.Activeflag = 1 AND tv.CourtID = 3004 AND DATEDIFF(DAY,tlh.RepDate,@LORDate5) = 0
   
   SELECT @TotalRepDate6 = COUNT(DISTINCT t.TicketID_PK)
   FROM   tblTicketsViolations tv INNER JOIN tbltickets t  ON  t.TicketID_PK = tv.TicketID_PK	   
   INNER JOIN faxLetter fl ON fl.Ticketid = t.TicketID_PK		   
   WHERE  t.Activeflag = 1 AND tv.CourtID = 3004 AND DATEDIFF(DAY, fl.PMCRepDate, @LORDate6) =0
   SELECT @TotalRepDate6 = @TotalRepDate6 + COUNT(DISTINCT t.TicketID_PK)
   FROM   tblTicketsViolations tv INNER JOIN tbltickets t  ON  t.TicketID_PK = tv.TicketID_PK
   INNER JOIN tblLORBatchHistory tlh ON tlh.TicketID_PK = t.TicketID_PK	      
   WHERE  t.Activeflag = 1 AND tv.CourtID = 3004 AND DATEDIFF(DAY,tlh.RepDate,@LORDate6) = 0
   IF((@TotalRepDate1>=@Maxout OR datediff(day,@LORDate1,getdate())>=0) AND (@TotalRepDate2>=@Maxout OR datediff(day,@LORDate2,getdate())>=0) AND (@TotalRepDate3>=@Maxout OR datediff(day,@LORDate3,getdate())>=0) AND (@TotalRepDate4>=@Maxout OR datediff(day,@LORDate4,getdate())>=0) AND (@TotalRepDate5>=@Maxout OR datediff(day,@LORDate5,getdate())>=0) AND (@TotalRepDate6>=@Maxout OR datediff(day,@LORDate6,getdate())>=0))
	BEGIN
		SET @DateIsInPastOrFull = 1
	END
	ELSE
	BEGIN
			SET @DateIsInPastOrFull = 0
	END
END

SELECT @DateIsInPastOrFull AS DateIsInPastOrFull


GO

GRANT EXECUTE ON [dbo].[USP_HTP_Check_LORDatesForPMC]  TO dbr_webuser

Go



SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_MID_Get_NewTicketsInfo]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_MID_Get_NewTicketsInfo]
GO


-- PROCEDURE FOR GETTING CASE DETAILS OF NEW CASES OF THE SELECTED CLIENT.....      
CREATE procedure [dbo].[usp_MID_Get_NewTicketsInfo]      
 (      
 @MIDNum  varchar(20),  -- INPUT PARAMETER ON WHICH THE NEW CASES WILL BE RETRIEVED....      
 @listdate datetime      -- FOR AN SPECIFIC LIST DATE.......      
 )      
      
as      
      
      
-- VARIABLE DECLARATION....      
declare @SqlScript  as nvarchar(4000)           
declare @SQLParam   as nvarchar(200)          
      
set @SqlScript = '      
SELECT distinct      
 upper(convert(varchar(25),tva.TicketNumber_PK) +''-''+       
 convert(varchar(10),tva.ViolationNumber_PK)) as CaseNo,      
      
 -- GETTING VIOLATION DESCRIPTION....      
 upper((case when isnull(tva.violationdescription,'''') like ''%------%-----%'' then ''-NA-''      
  else (case when isnull(tva.violationdescription,'''') = ''---Choose----'' then ''-NA-''      
        else isnull(tva.violationdescription,'''')      
        end)         
 end)) AS ViolDesc,      
 tva.FineAmount as FineAmount,       
      
 -- GETTING CASE STATUS      
 upper(isnull(cvs.shortdescription,''N/A'')) as CaseStatus,       
 tva.CourtDate as CourtDate,      
 upper(c.ShortName) as CourtName ,      
 ta.recordid as RecordId,      
 ta.midnumber midnum,      
 ta.zipcode as zip,      
 ta.address1 as address,  
ta.CourtNumber as CourtNumber  
      
FROM    dbo.tblViolations v       
INNER JOIN      
 dbo.tblTicketsViolationsArchive tva       
INNER JOIN      
 dbo.tblCourts c       
INNER JOIN      
 dbo.tblTicketsArchive ta       
ON  c.Courtid = ta.CourtID       
--ON  tva.TicketNumber_PK = ta.TicketNumber       
on tva.recordid = ta.recordid
ON  v.ViolationNumber_PK = tva.ViolationNumber_PK       
LEFT OUTER JOIN      
 dbo.tblTicketsArchivePCCR tp       
ON  ta.recordID = tp.RecordID_FK       
LEFT OUTER JOIN      
 dbo.tblDateType dt       
INNER JOIN      
 dbo.tblCourtViolationStatus cvs       
ON  dt.TypeID = cvs.CategoryID       
ON  tva.violationstatusid = cvs.CourtViolationStatusID      
      
      
-- FILTERING RECORDS ON THE SELECTED CRITERIA......      
where ta.midnumber = @MIDNum      
and ta.clientflag=0      
and v.violationtype <> 1      
and convert(varchar(10),ta.listdate,101) = @listdate       
and tva.ticketnumber_pk <> ''0''      
'      
      
set @SQLparam = '@MIDNum varchar(20), @listdate datetime'          
exec sp_executesql @SqlScript ,@SQLparam, @midnum, @listdate      
      

    


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_All_PaymentInfoByRecDate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_All_PaymentInfoByRecDate]
GO









CREATE procedure [dbo].[usp_Get_All_PaymentInfoByRecDate]   
  
@RecDate DateTime  
  
AS  
  
  
Select	PaymentType, 
	Count(ticketID) As TotalCount, 
	Sum(ChargeAmount) As Amount 
from 	tblTicketsPayment  
Where  	PaymentVoid <> 1   
And 	datediff(day,recdate, @recdate) = 0  --( RecDate  Between  @RecDate AND @RecDate+1)  
  
Group By PaymentType  
Order By PaymentType  
  
  
  
  









GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


USE TrafficTickets
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rab Nawaz Khan
-- Create date: 10/03/2013
-- Task ID:		11431
-- Description:	This Procedure is used to maintain the history of updated addresses agains non-clients
-- =============================================
ALTER PROCEDURE USP_HTP_GetUpdatedAddressesHistory 
	@recordId INT
AS
BEGIN
	SET NOCOUNT ON;
    SELECT [AddressHistory_Id], [Activity], h.[RecordId], [Insertdate], [UserId], tu.UserName, UPPER(tu.Lastname) AS Lastname, 
      tu.Firstname, tta.FirstName AS ClientFirstName, tta.LastName AS ClientLastName
	FROM [TrafficTickets].[dbo].[tblNewAddressesHisotry] h INNER JOIN tblUsers tu
	ON h.UserId = tu.EmployeeID
	INNER JOIN tblTicketsArchive tta ON tta.RecordID = h.RecordId
	WHERE h.[RecordId] = @recordId
    ORDER BY [Insertdate] DESC
END
GO
	GRANT EXECUTE ON USP_HTP_GetUpdatedAddressesHistory  TO dbr_webuser
GO

/*****************************  
* Author : Abbas Qamar
* Task Id: 9726 
* Creation Date : 11/03/2011
* 
* Business Logic : This Stored procedure is used by HTP/Activites/DoNotMailUpdate/NoMailFlagRecords. to get list of all
*					persons added in our no-mailing list according to search criteria.
*					
* Input Parameters:
* @Address = address of a person.
* @Zip = zip code of city.
* @StateID = id of state.
* @City = city of a peroson.
* 
* Output Columns:
* ADDRESS
* ZIP
* City
* State
*************************/  

CREATE PROCEDURE [dbo].[USP_HTS_Get_NoMailFlagByAddress] --'','','','','','','1','07/21/2009','07/21/2009'     
AS

SELECT distinct top 250 convert(varchar,m.insertdate,101) as insertdate,
	m.ADDRESS, 
	m.ZIP, 
	m.City, 
	s.State,
	m.InsertDate,
Case m.UpdateSource when 3 then 'Bad Mail By Address' 
					else 'N/A'
					end as UpdateSource  
FROM tbl_hts_donotmail_records m     
join tblstate s    
on m.StateID_FK = s.StateID    
WHERE m.UpdateSource=3
ORDER BY m.InsertDate DESC

GO

GRANT EXECUTE ON [dbo].[USP_HTS_Get_NoMailFlagByAddress] TO dbr_webuser

GO
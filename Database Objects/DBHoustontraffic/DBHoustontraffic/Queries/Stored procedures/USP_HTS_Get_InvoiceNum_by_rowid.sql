SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Get_InvoiceNum_by_rowid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Get_InvoiceNum_by_rowid]
GO

create procedure [dbo].[USP_HTS_Get_InvoiceNum_by_rowid]     
 (    
 @InvoiceNumber int    
 )    
    
as    
    
declare @transnum int    
set @transnum =  0    
    
select @transnum = invnumber  from tblticketscybercash    
where rowid = @invoicenumber    
    
select @transnum      
  
  
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


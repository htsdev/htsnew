/*
* Business Logic : This Store procedure returns clients information base on ticket id.
* Parameter : @TicketID 
*/

-- usp_hts_get_PaymentDetail 17844
ALTER PROCEDURE [dbo].[usp_hts_get_PaymentDetail]
	@TicketID
AS
	INT
	
	AS
	-- Noufil 5618 04/09/2009 Join were not correctly implemented. Joins corrected
	SELECT     TP.InvoiceNumber_PK, TP.PaymentVoid AS voidflag, TP.TicketID, CONVERT(VARCHAR(20), TP.ChargeAmount) AS amount, TP.RecDate AS paymentdate, 
                      PT.Description AS types, UPPER(u.Abbreviation) AS rep, ISNULL(tsc.transnum, '') AS transnum, UPPER(tsc.name) AS NAME, cct.CCType, tsc.auth_code, 
                      UPPER(ISNULL(t.Firstname, '')) + ' ' + UPPER(ISNULL(t.Lastname, '')) AS clientname, ISNULL(t.GeneralComments, '') AS generalcomments, 
                      ISNULL(tsc.response_reason_text, '') AS response, 
                      -- Noufil 5618 04/02/2009 Contact number of Client Added
                      CASE t .Contact1 WHEN '' THEN '' ELSE dbo.formatphonenumbers(t .Contact1) + ' ( ' + tc.[Description] + ' ) ' END AS contact1, 
                      CASE t .Contact2 WHEN '' THEN '' ELSE dbo.formatphonenumbers(t .Contact2) + ' ( ' + tcc.[Description] + ' ) ' END AS contact2, 
                      CASE t .Contact3 WHEN '' THEN '' ELSE dbo.formatphonenumbers(t .Contact3) + ' ( ' + tccc.[Description] + ' ) ' END AS contact3
FROM         tblContactstype AS tccc RIGHT OUTER JOIN
                      tblTickets AS t ON tccc.ContactType_PK = t.ContactType3 LEFT OUTER JOIN
                      tblContactstype AS tc ON t.ContactType1 = tc.ContactType_PK LEFT OUTER JOIN
                      tblContactstype AS tcc ON tcc.ContactType_PK = t.ContactType2 LEFT OUTER JOIN
                      tblTicketsPayment AS TP INNER JOIN
                      tblPaymenttype AS PT ON TP.PaymentType = PT.Paymenttype_PK INNER JOIN
                      tblUsers AS u ON TP.EmployeeID = u.EmployeeID ON t.TicketID_PK = TP.TicketID LEFT OUTER JOIN
                      tblTicketsCybercash AS tsc ON TP.InvoiceNumber_PK = tsc.invnumber LEFT OUTER JOIN
                      tblCCType AS cct ON tsc.CardType = cct.CCTypeID
WHERE     (TP.TicketID = @TicketID) AND (TP.PaymentType <> 99)
ORDER BY paymentdate
	
	/*SELECT TP.InvoiceNumber_PK,
	       TP.paymentvoid AS voidflag,
	       TP.TicketID,
	       CONVERT(VARCHAR(20), TP.ChargeAmount) AS amount,
	       -- convert(varchar(10),TP.RecDate,101)as recdate,                   
	       TP.RecDate AS paymentdate,
	       PT.Description AS types,
	       UPPER(u.abbreviation) AS rep,
	       ISNULL(tsc.transnum, '') AS transnum,
	       UPPER(tsc.name) AS NAME,
	       cct.CCType,
	       tsc.auth_code,
	       (
	           UPPER(ISNULL(t.firstname, '')) + ' ' + UPPER(ISNULL(t.lastname, ''))
	       ) AS clientname,
	       ISNULL(t.GeneralComments, '') AS generalcomments,
	       ISNULL(tsc.response_reason_text, '') AS response,
	       -- Noufil 5618 04/02/2009 Contact number of Client Added
	       CASE t.Contact1 WHEN '' THEN '' ELSE dbo.formatphonenumbers(t.Contact1)+' ( '+ tc.[Description]+' ) ' END AS contact1,
	       CASE t.Contact2 WHEN '' THEN '' ELSE dbo.formatphonenumbers(t.Contact2) +' ( '+ tcc.[Description]+' ) ' END AS  contact2,
	       CASE t.Contact3 WHEN '' THEN '' ELSE dbo.formatphonenumbers(t.Contact3) +' ( '+ tccc.[Description]+' ) ' END AS contact3
	       
	FROM   tblTicketsPayment TP
	       INNER JOIN tblPaymenttype PT
	            ON  TP.PaymentType = PT.Paymenttype_PK
	       INNER JOIN tblusers u
	            ON  TP.employeeid = u.employeeid
	       INNER JOIN tbltickets t
	            ON  TP.ticketid = t.ticketid_pk
	       LEFT OUTER JOIN tblticketscybercash tsc
	            ON  TP.invoicenumber_pk = tsc.invnumber
	       LEFT OUTER JOIN dbo.tblCCType cct
	            ON  tsc.cardtype = cct.CCTypeID
	       INNER JOIN tblContactstype tc
	            ON  tc.ContactType_PK = t.ContactType1
	       INNER JOIN tblContactstype tcc
	            ON  tcc.ContactType_PK = t.ContactType2
	       INNER JOIN tblContactstype tccc
	            ON  tccc.ContactType_PK = t.ContactType3
	WHERE  (TP.TicketID = @TicketID)
	       AND TP.paymenttype <> 99
	ORDER BY
	       tp.recdate*/

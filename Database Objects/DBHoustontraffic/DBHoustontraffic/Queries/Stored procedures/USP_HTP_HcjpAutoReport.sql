﻿set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go










/******************************************
Created By:		Noufil Khan
Create Date:	09/08/2007
Task:			4748

Business Logic:
	This report display all records 
			1. Whose Verified status is WAIT or Pretrial or Appearance, Jury Trial or Judge Trial.  
			2. Courtdatemain is of future within 60 days

Input Parameters:
	None.

Output Columns:
	ticketid_pk:		identification of the case in database    
	lastname:			last name of the client
	firstname:			first name of the client
	refcasenumber:		ticket number for the case	
	shortname:			short court name
	autostatus:			case status updated by the loader
	verifiedcourtstatus:verified case status udpated by the user
	autocourtdate:		court date updated by the loader
	verifiedcourtdate:	court date updated by the user	
	WaitTime:			time in days for which the case is in discrepancy	
	bondflag:			flag for bond status if client needs bonds
	LORPrintDate:		date when last time Letter of Rep printed
	LORSortDate:		max. LOR print date

********************************************/

ALTER PROCEDURE [dbo].[USP_HTP_HcjpAutoReport]
AS
	SELECT DISTINCT 
	       t.TicketID_PK AS ticketid_pk,
	       lastname = CASE 
	                       WHEN LEN(ISNULL(t.Lastname, '')) > 10 THEN SUBSTRING(t.Lastname, 0, 10)
	                            + '...'
	                       ELSE t.Lastname
	                  END,
	       firstname = CASE 
	                        WHEN LEN(ISNULL(t.Firstname, '')) > 10 THEN 
	                             SUBSTRING(t.Firstname, 0, 10) + '...'
	                        ELSE t.Firstname
	                   END,
	       refcasenumber = CASE 
	                            WHEN LEN(ISNULL(tv.casenumassignedbycourt, '')) 
	                                 = 0 THEN tv.refcasenumber
	                            ELSE tv.casenumassignedbycourt
	                       END,
	       c.CourtName AS courtname,
	       c.ShortName AS shortname,
	       cvs.ShortDescription AS autostatus,
	       tcv2.ShortDescription AS verifiedcourtstatus,
	       dbo.fn_DateFormat(tv.CourtDate, 25, '/', ':', 1) AS autocourtdate,
	       dbo.fn_DateFormat(tv.CourtDateMain, 25, '/', ':', 1) AS 
	       verifiedcourtdate,
	       tv.CourtNumbermain AS courtnumber,
	       WaitTime = ISNULL(
	           DATEDIFF(
	               DAY,
	               (
	                   SELECT MIN(recdate)
	                   FROM   tblticketsviolationlog
	                   WHERE  ticketviolationid = tv.ticketsviolationid
	                          AND oldverifiedstatus <> newverifiedstatus
	                          AND newverifiedstatus = tv.courtviolationstatusidmain
	               ),
	               GETDATE()
	           ),
	           0
	       ),
	       tv.CourtDate,
	       tv.CourtDateMain,
	       (CASE WHEN tv.underlyingbondflag = 1 THEN 'B' ELSE '' END) AS 
	       bondflag,
	       LORPrintDate = (
	           SELECT CONVERT(VARCHAR(12), MAX(printdate), 103)
	           FROM   tblHTSNotes n
	           WHERE  t.ticketid_pk = n.ticketid_fk
	                  AND n.letterid_fk = 6
	       ),
	       LORSortDate = (
	           SELECT CONVERT(VARCHAR(12), MAX(printdate), 103)
	           FROM   tblHTSNotes n
	           WHERE  t.ticketid_pk = n.ticketid_fk
	                  AND n.letterid_fk = 6
	       )
	FROM   dbo.tblTickets AS t
	       INNER JOIN dbo.tblTicketsViolations AS tv
	            ON  t.TicketID_PK = tv.TicketID_PK
	       INNER JOIN dbo.tblCourts AS c
	            ON  tv.CourtID = c.Courtid
	       INNER JOIN dbo.tblCourtViolationStatus AS cvs
	            ON  tv.CourtViolationStatusID = cvs.CourtViolationStatusID
	       INNER JOIN dbo.tblCourtViolationStatus AS tcv2
	            ON  tv.CourtViolationStatusIDmain = tcv2.CourtViolationStatusID
	WHERE  --Ozair 7791 07/24/2010  where clause optimized
	       --Nasir 6273 08/03/2009 Verified status is WAIT or Pretrial or Appearance, Jury Trial or Judge Trial.
	       (
	           tv.CourtViolationStatusIDmain = 104
	           OR tcv2.categoryid IN (2, 3, 4, 5)
	       )
	       AND DATEDIFF(DAY, GETDATE(), courtdate) BETWEEN 0 AND 60
	           -- tahir 4965 10/14/2008 do not display record if verified & auto status is waiting....
	       AND ((tcv2.CategoryID <> cvs.CategoryID AND tv.CourtID <> 3067)
	       -- Adil 8454 10/28/2010 For Stafford court Appearance and Pretrial cases will be ignored if they are against each other in Auto Vs Verified but their court date/time must be matched.
	       OR
		   (tv.CourtID = 3067 AND
		   
		   ((CASE WHEN (tcv2.CategoryID IN (2, 3)) THEN 116 ELSE tv.courtviolationstatusidmain END 
		   = CASE WHEN (cvs.CategoryID IN (2, 3)) THEN 116 ELSE tv.courtviolationstatusid END AND tv.CourtDate <> tv.CourtDateMain) 
		   OR 
		   (tv.CourtDate <> tv.CourtDateMain)
		   OR
		   (CASE WHEN (tcv2.CategoryID IN (2, 3)) THEN 116 ELSE tv.courtviolationstatusidmain END 
		   <> CASE WHEN (cvs.CategoryID IN (2, 3)) THEN 116 ELSE tv.courtviolationstatusid END)
		   )))
	       
	       AND t.activeflag = 1
	       --Sabir Khan 8174 08/17/2010 Case should not be associated with HMC Courts.
	       --AND isnull(c.courtcategorynum,0) <> 1--Fahad 11417 09/12/2013 included HMC courts as well
	ORDER BY
	       ticketid_pk,
	       refcasenumber
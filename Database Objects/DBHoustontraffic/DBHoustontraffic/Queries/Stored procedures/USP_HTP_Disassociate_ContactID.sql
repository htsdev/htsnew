﻿/***    
* Waqas Javed 5771 04/16/2009  
* Business Logic : This procedure is used to disassociate the contact id of the client    
* 1. Active clients can not be dis associated.    
* 2. Contact Id will be disassociated in non client database if client was a potential client  
* 3. History note will be added " CID [CID] Disassocited - [Rep Name] "  
* Parameters : 
* @TicketID
* @EMPID
***/    
        
      
CREATE PROCEDURE dbo.USP_HTP_Disassociate_ContactID
	@TicketID INT,
	@EMPID INT
AS
BEGIN
    DECLARE @Name AS VARCHAR(30),
            @CID AS INT 
    
    --Getting CID by Ticket ID
    SELECT @CID = (
               SELECT contactid_fk
               FROM   tblTickets tt
               WHERE  tt.TicketID_PK = @TicketID
           ) 
    --Disassociate Contact ID in tbltickets
    UPDATE tbltickets
    SET    ContactID_FK = NULL
    WHERE  TicketID_PK = @TicketID    
    
    --Getting CASE type ID
    DECLARE @Casetype INT
    SELECT @Casetype = casetypeid
    FROM   tblTickets tt
    WHERE  tt.TicketID_PK = @TicketID
    
    --For Traffic and Criminal 
    IF @Casetype = 1
       OR @Casetype = 2
    BEGIN
        IF EXISTS (
               SELECT TOP 1 ttv.Recordid
               FROM   tblTicketsViolations ttv
               WHERE  ttv.RecordID > 0
                      AND ticketid_pk = @TicketID
           )
        BEGIN
            DECLARE @Recordid INT  
            SELECT @Recordid = (
                       SELECT TOP 1 ttv.Recordid
                       FROM   tblTicketsviolations ttv
                       WHERE  ticketid_pk = @TicketID
                   )  
            
            UPDATE tblticketsarchive
            SET    PotentialContactID_FK = NULL
            WHERE  RecordID = @Recordid
        END
    END
    ELSE --For Family and Civil
    IF @Casetype = 4
       OR @Casetype = 3
    BEGIN
        IF EXISTS (
               SELECT TOP 1 ttv.Recordid
               FROM   tblTicketsViolations ttv
               WHERE  ttv.RecordID > 0
                      AND ticketid_pk = @TicketID
           )
        BEGIN
            DECLARE @Partyid INT  
            SELECT @Partyid = (
                       SELECT TOP 1 ttv.Recordid
                       FROM   tblTicketsviolations ttv
                       WHERE  ticketid_pk = @TicketID
                   )  
            
            UPDATE civil.dbo.ChildCustodyParties
            SET    PotentialContactID_FK = NULL
            WHERE  ChildCustodyPartyId = @Partyid
        END
    END
    
    --Getting Rep Name
    SELECT @Name = (
               SELECT ISNULL(tu.Firstname, '') + ' ' + ISNULL(tu.Lastname, '')
               FROM   tblUsers tu
               WHERE  tu.EmployeeID = @EMPID
           )    
    
    --Inserting Note in History
    INSERT INTO tblticketsnotes
      (
        ticketid,
        SUBJECT,
        employeeid
      )
    VALUES
      (
        @TicketID,
        'CID <a href="javascript:window.open(' +
        '''/ClientInfo/AssociatedMatters.aspx?cid=' + CONVERT(VARCHAR(20), @CID) 
        + ''');void('''');">' + CONVERT(VARCHAR(20), @CID) +
        '</a> Disassociated - ' + @Name + ' ',
        @EMPID
      )
END
GO

GRANT EXECUTE ON [dbo].[USP_HTP_Disassociate_ContactID] TO dbr_webuser

GO

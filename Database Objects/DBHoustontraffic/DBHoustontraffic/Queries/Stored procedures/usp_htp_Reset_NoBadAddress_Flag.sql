/*
* Created By: Farrukh Iftikhar
* Task : 10003
* Date: 01/20/2012
* 
* Business logic: This procedure is used to set the No Bad Address flag against Ticket ID
* 
* Input Parameters:
*			@ticketid = Ticket ID of record
*			@IsNoBadAddressFlag = Ticket ID of record
*/


-- [dbo].[usp_htp_Reset_NoBadAddress_Flag] 8790,1 

CREATE PROCEDURE [dbo].[usp_htp_Reset_NoBadAddress_Flag]
	@ticketid INT,
	@IsNoBadAddressFlag BIT
AS
BEGIN

UPDATE tbltickets                                            
SET NoBadAddressFlag = @IsNoBadAddressFlag
WHERE TicketID_PK=@ticketid

     
END


GO
GRANT EXECUTE ON usp_htp_Reset_NoBadAddress_Flag TO dbr_webuser
GO

﻿ /************************************************************
 * Created By: Syed Muhammad Ozair 6557 09/10/2009
 * 
 * Business Logic:	This procedure is used to retunrned detailed information related to mailers sent via USPS IMB code 
 *					to track it delivery or returned.
 * 
 * Prameter List:
 *		@Status : Pending, 091_Returned, 099_Returned, 1st_Pass_Delivered and 2nd_Pass_Delivered
 *		@StartDate 
 *		@EndDate	
 *		
 * Column List:
 *		SNo
 *		LetterID 
 *		FCLT_ID 
 *		OperationCode 
 *		OperationDateTime 
 *		ZipCode 
 *		DP2 
 * 
 * 
 ************************************************************/

CREATE PROCEDURE dbo.usp_HTP_GET_DetailMailerUSPSReportByStatus
	@Status VARCHAR(50),
	@StartDate DATETIME,
	@EndDate DATETIME
AS
	DECLARE @table TABLE
	        (
	            SNo VARCHAR(10),
	            LetterID VARCHAR(9),
	            FCLT_ID VARCHAR(5),
	            OperationCode VARCHAR(5),
	            OperationDateTime DATETIME,
	            ZipCode VARCHAR(9),
	            DP2 VARCHAR(2)
	        )
	
	DECLARE @rowcount INT
	DECLARE @rowno INT
	DECLARE @LID VARCHAR(9)
	DECLARE @GroupCount INT
	
	--PendingCount
	IF @Status = 'Pending'
	BEGIN
	    SELECT ROW_NUMBER() OVER(ORDER BY ud.letterid, ud.operationdatetime) AS 
	           SNo,
	           ud.LetterID,
	           ud.FCLT_ID,
	           ud.OperationCode,
	           ud.OperationDateTime,
	           ud.ZipCode,
	           ud.DP2
	           INTO #temp
	    FROM   MailerUSPSInfo.dbo.USPSPackageData ud
	    WHERE  ud.LetterID IN (SELECT DISTINCT t1.letterid
	                           FROM   MailerUSPSInfo.dbo.uspspackagedata t1
	                           WHERE  t1.letterid NOT IN (SELECT DISTINCT t2.letterid
	                                                      FROM   MailerUSPSInfo.dbo.uspspackagedata 
	                                                             t2
	                                                             --Ozair 6405 08/27/2009 New Delivered Returned Code check implemented
	                                                      WHERE  t2.operationcode IN (
	                                                                                 '828', 
	                                                                                 '878', 
	                                                                                 '898', 
	                                                                                 '908', 
	                                                                                 '912', 
	                                                                                 '914', 
	                                                                                 '918', 
	                                                                                 '146', 
	                                                                                 '266', 
	                                                                                 '276', 
	                                                                                 '286', 
	                                                                                 '336', 
	                                                                                 '366', 
	                                                                                 '376', 
	                                                                                 '396', 
	                                                                                 '406', 
	                                                                                 '416', 
	                                                                                 '426', 
	                                                                                 '446', 
	                                                                                 '466', 
	                                                                                 '486', 
	                                                                                 '496', 
	                                                                                 '506', 
	                                                                                 '806', 
	                                                                                 '816', 
	                                                                                 '826', 
	                                                                                 '829', 
	                                                                                 '836', 
	                                                                                 '846', 
	                                                                                 '856', 
	                                                                                 '866', 
	                                                                                 '876', 
	                                                                                 '879', 
	                                                                                 '886', 
	                                                                                 '896', 
	                                                                                 '899', 
	                                                                                 '905', 
	                                                                                 '906', 
	                                                                                 '909', 
	                                                                                 '911', 
	                                                                                 '913', 
	                                                                                 '915', 
	                                                                                 '919', 
	                                                                                 '966', 
	                                                                                 '976')
	                                                             OR  (CONVERT(INT, t2.operationcode) IN (91, 99)))
	                                  AND t1.letterid IN (SELECT DISTINCT b.noteid
	                                                      FROM   tblletternotes 
	                                                             b
	                                                      WHERE  DATEDIFF(DAY, '08/01/2009', b.currentdate) 
	                                                             >= 0
	                                                             AND DATEDIFF(DAY, '09/01/2009', b.currentdate) 
	                                                                 <= 0
	                                                             AND courtid = 
	                                                                 3037
	                                                             AND lettertype = 
	                                                                 55))
	    ORDER BY
	           ud.LetterID,
	           ud.OperationDateTime
	    
	    
	    SET @GroupCount = 1
	    SET @rowno = 1
	    
	    SELECT @rowcount = COUNT(letterid)
	    FROM   #temp
	    
	    WHILE (@rowno <= @rowcount)
	    BEGIN
	        IF (@rowno = 1)
	        BEGIN
	            SELECT @LID = letterid
	            FROM   #temp
	            WHERE  SNo = @rowno
	            
	            INSERT INTO @table
	            SELECT @GroupCount,
	                   letterid,
	                   FCLT_ID,
	                   operationcode,
	                   operationDatetime,
	                   zipcode,
	                   dp2
	            FROM   #temp
	            WHERE  SNo = @rowno
	        END
	        ELSE
	        BEGIN
	            IF @LID <> (
	                   SELECT letterid
	                   FROM   #temp
	                   WHERE  SNo = @rowno
	               )
	            BEGIN
	                SET @GroupCount = @GroupCount + 1
	                INSERT INTO @table
	                SELECT @GroupCount,
	                       letterid,
	                       FCLT_ID,
	                       operationcode,
	                       operationDatetime,
	                       zipcode,
	                       dp2
	                FROM   #temp
	                WHERE  SNo = @rowno
	                
	                SELECT @LID = letterid
	                FROM   #temp
	                WHERE  SNo = @rowno
	            END
	            ELSE
	            BEGIN
	                INSERT INTO @table
	                SELECT '',
	                       letterid,
	                       FCLT_ID,
	                       operationcode,
	                       operationDatetime,
	                       zipcode,
	                       dp2
	                FROM   #temp
	                WHERE  SNo = @rowno
	            END
	        END
	        SET @rowno = @rowno + 1
	    END
	    
	    SELECT *
	    FROM   @table 
	    
	    DROP TABLE #temp
	END
	--"091_ReturnedCount"
	IF @Status = '091_Returned'
	BEGIN
	    SELECT ROW_NUMBER() OVER(ORDER BY ud.letterid, ud.operationdatetime) AS 
	           SNo,
	           ud.LetterID,
	           ud.FCLT_ID,
	           ud.OperationCode,
	           ud.OperationDateTime,
	           ud.ZipCode,
	           ud.DP2
	           INTO #temp1
	    FROM   MailerUSPSInfo.dbo.USPSPackageData ud
	    WHERE  ud.LetterID IN (SELECT DISTINCT t1.letterid
	                           FROM   MailerUSPSInfo.dbo.uspspackagedata t1
	                           WHERE  t1.letterid  IN (SELECT DISTINCT t2.letterid
	                                                   FROM   MailerUSPSInfo.dbo.uspspackagedata 
	                                                          t2 
	                                                          --Ozair 6405 08/18/2009 New Returned Code check implemented
	                                                   WHERE  CONVERT(INT, t2.operationcode) = 
	                                                          91
	                                                          AND t2.LetterID 
	                                                              NOT IN (SELECT 
	                                                                             letterid
	                                                                      FROM   
	                                                                             MailerUSPSInfo.dbo.uspspackagedata
	                                                                      WHERE  
	                                                                             CONVERT(INT, operationcode) = 
	                                                                             99))
	                                  AND t1.letterid IN (SELECT DISTINCT b.noteid
	                                                      FROM   tblletternotes 
	                                                             b
	                                                      WHERE  DATEDIFF(DAY, '08/01/2009', b.currentdate) 
	                                                             >= 0
	                                                             AND DATEDIFF(DAY, '09/01/2009', b.currentdate) 
	                                                                 <= 0
	                                                             AND courtid = 
	                                                                 3037
	                                                             AND lettertype = 
	                                                                 55))
	    ORDER BY
	           ud.LetterID,
	           ud.OperationDateTime
	    
	    
	    
	    SET @GroupCount = 1
	    SET @rowno = 1
	    
	    SELECT @rowcount = COUNT(letterid)
	    FROM   #temp1
	    
	    WHILE (@rowno <= @rowcount)
	    BEGIN
	        IF (@rowno = 1)
	        BEGIN
	            SELECT @LID = letterid
	            FROM   #temp1
	            WHERE  SNo = @rowno
	            
	            INSERT INTO @table
	            SELECT @GroupCount,
	                   letterid,
	                   FCLT_ID,
	                   operationcode,
	                   operationDatetime,
	                   zipcode,
	                   dp2
	            FROM   #temp1
	            WHERE  SNo = @rowno
	        END
	        ELSE
	        BEGIN
	            IF @LID <> (
	                   SELECT letterid
	                   FROM   #temp1
	                   WHERE  SNo = @rowno
	               )
	            BEGIN
	                SET @GroupCount = @GroupCount + 1
	                INSERT INTO @table
	                SELECT @GroupCount,
	                       letterid,
	                       FCLT_ID,
	                       operationcode,
	                       operationDatetime,
	                       zipcode,
	                       dp2
	                FROM   #temp1
	                WHERE  SNo = @rowno
	                
	                SELECT @LID = letterid
	                FROM   #temp1
	                WHERE  SNo = @rowno
	            END
	            ELSE
	            BEGIN
	                INSERT INTO @table
	                SELECT '',
	                       letterid,
	                       FCLT_ID,
	                       operationcode,
	                       operationDatetime,
	                       zipcode,
	                       dp2
	                FROM   #temp1
	                WHERE  SNo = @rowno
	            END
	        END
	        SET @rowno = @rowno + 1
	    END
	    
	    SELECT *
	    FROM   @table 
	    
	    DROP TABLE #temp1
	END
	
	IF @Status = '099_Returned'
	BEGIN
	    --"099_ReturnedCount"
	    SELECT ROW_NUMBER() OVER(ORDER BY ud.letterid, ud.operationdatetime) AS 
	           SNo,
	           ud.LetterID,
	           ud.FCLT_ID,
	           ud.OperationCode,
	           ud.OperationDateTime,
	           ud.ZipCode,
	           ud.DP2
	           INTO #temp2
	    FROM   MailerUSPSInfo.dbo.USPSPackageData ud
	    WHERE  ud.LetterID IN (SELECT DISTINCT t1.letterid
	                           FROM   MailerUSPSInfo.dbo.uspspackagedata t1
	                           WHERE  t1.letterid  IN (SELECT DISTINCT t2.letterid
	                                                   FROM   MailerUSPSInfo.dbo.uspspackagedata 
	                                                          t2 
	                                                          --Ozair 6405 08/18/2009 New Returned Code check implemented
	                                                   WHERE  CONVERT(INT, t2.operationcode) = 
	                                                          99)
	                                  AND t1.letterid IN (SELECT DISTINCT b.noteid
	                                                      FROM   tblletternotes 
	                                                             b
	                                                      WHERE  DATEDIFF(DAY, '08/01/2009', b.currentdate) 
	                                                             >= 0
	                                                             AND DATEDIFF(DAY, '09/01/2009', b.currentdate) 
	                                                                 <= 0
	                                                             AND courtid = 
	                                                                 3037
	                                                             AND lettertype = 
	                                                                 55))
	    ORDER BY
	           ud.LetterID,
	           ud.OperationDateTime
	    
	    
	    
	    SET @GroupCount = 1
	    SET @rowno = 1
	    
	    SELECT @rowcount = COUNT(letterid)
	    FROM   #temp2
	    
	    WHILE (@rowno <= @rowcount)
	    BEGIN
	        IF (@rowno = 1)
	        BEGIN
	            SELECT @LID = letterid
	            FROM   #temp2
	            WHERE  SNo = @rowno
	            
	            INSERT INTO @table
	            SELECT @GroupCount,
	                   letterid,
	                   FCLT_ID,
	                   operationcode,
	                   operationDatetime,
	                   zipcode,
	                   dp2
	            FROM   #temp2
	            WHERE  SNo = @rowno
	        END
	        ELSE
	        BEGIN
	            IF @LID <> (
	                   SELECT letterid
	                   FROM   #temp2
	                   WHERE  SNo = @rowno
	               )
	            BEGIN
	                SET @GroupCount = @GroupCount + 1
	                INSERT INTO @table
	                SELECT @GroupCount,
	                       letterid,
	                       FCLT_ID,
	                       operationcode,
	                       operationDatetime,
	                       zipcode,
	                       dp2
	                FROM   #temp2
	                WHERE  SNo = @rowno
	                
	                SELECT @LID = letterid
	                FROM   #temp2
	                WHERE  SNo = @rowno
	            END
	            ELSE
	            BEGIN
	                INSERT INTO @table
	                SELECT '',
	                       letterid,
	                       FCLT_ID,
	                       operationcode,
	                       operationDatetime,
	                       zipcode,
	                       dp2
	                FROM   #temp2
	                WHERE  SNo = @rowno
	            END
	        END
	        SET @rowno = @rowno + 1
	    END
	    
	    SELECT *
	    FROM   @table 
	    
	    DROP TABLE #temp2
	END
	
	--"1st_Pass_DeliveredCount"
	IF @Status = '1st_Pass_Delivered'
	BEGIN
	    SELECT ROW_NUMBER() OVER(ORDER BY ud.letterid, ud.operationdatetime) AS 
	           SNo,
	           ud.LetterID,
	           ud.FCLT_ID,
	           ud.OperationCode,
	           ud.OperationDateTime,
	           ud.ZipCode,
	           ud.DP2
	           INTO #temp3
	    FROM   MailerUSPSInfo.dbo.USPSPackageData ud
	    WHERE  ud.LetterID IN (SELECT DISTINCT t1.letterid
	                           FROM   MailerUSPSInfo.dbo.uspspackagedata t1
	                           WHERE  t1.letterid  IN (SELECT DISTINCT t2.letterid
	                                                   FROM   MailerUSPSInfo.dbo.uspspackagedata 
	                                                          t2 
	                                                          --Ozair 6405 08/27/2009 New Delivered Returned Code check implemented
	                                                   WHERE  t2.operationcode IN ('828', '878', '898', '908', '912', '914', '918')
	                                                          AND t2.LetterID 
	                                                              NOT IN (SELECT 
	                                                                             letterid
	                                                                      FROM   
	                                                                             MailerUSPSInfo.dbo.uspspackagedata
	                                                                      WHERE  
	                                                                             operationcode IN (
	                                                                                              '146', 
	                                                                                              '266', 
	                                                                                              '276', 
	                                                                                              '286', 
	                                                                                              '336', 
	                                                                                              '366', 
	                                                                                              '376', 
	                                                                                              '396', 
	                                                                                              '406', 
	                                                                                              '416', 
	                                                                                              '426', 
	                                                                                              '446', 
	                                                                                              '466', 
	                                                                                              '486', 
	                                                                                              '496', 
	                                                                                              '506', 
	                                                                                              '806', 
	                                                                                              '816', 
	                                                                                              '826', 
	                                                                                              '829', 
	                                                                                              '836', 
	                                                                                              '846', 
	                                                                                              '856', 
	                                                                                              '866', 
	                                                                                              '876', 
	                                                                                              '879', 
	                                                                                              '886', 
	                                                                                              '896', 
	                                                                                              '899', 
	                                                                                              '905', 
	                                                                                              '906', 
	                                                                                              '909', 
	                                                                                              '911', 
	                                                                                              '913', 
	                                                                                              '915', 
	                                                                                              '919', 
	                                                                                              '966', 
	                                                                                              '976'))
	                                                          AND t2.LetterID 
	                                                              NOT IN (SELECT 
	                                                                             letterid
	                                                                      FROM   
	                                                                             MailerUSPSInfo.dbo.uspspackagedata
	                                                                      WHERE  
	                                                                             CONVERT(INT, operationcode) IN (91, 99)))
	                                  AND t1.letterid IN (SELECT DISTINCT b.noteid
	                                                      FROM   tblletternotes 
	                                                             b
	                                                      WHERE  DATEDIFF(DAY, '08/01/2009', b.currentdate) 
	                                                             >= 0
	                                                             AND DATEDIFF(DAY, '09/01/2009', b.currentdate) 
	                                                                 <= 0
	                                                             AND courtid = 
	                                                                 3037
	                                                             AND lettertype = 
	                                                                 55))
	    ORDER BY
	           ud.LetterID,
	           ud.OperationDateTime
	    
	    
	    
	    SET @GroupCount = 1
	    SET @rowno = 1
	    
	    SELECT @rowcount = COUNT(letterid)
	    FROM   #temp3
	    
	    WHILE (@rowno <= @rowcount)
	    BEGIN
	        IF (@rowno = 1)
	        BEGIN
	            SELECT @LID = letterid
	            FROM   #temp3
	            WHERE  SNo = @rowno
	            
	            INSERT INTO @table
	            SELECT @GroupCount,
	                   letterid,
	                   FCLT_ID,
	                   operationcode,
	                   operationDatetime,
	                   zipcode,
	                   dp2
	            FROM   #temp3
	            WHERE  SNo = @rowno
	        END
	        ELSE
	        BEGIN
	            IF @LID <> (
	                   SELECT letterid
	                   FROM   #temp3
	                   WHERE  SNo = @rowno
	               )
	            BEGIN
	                SET @GroupCount = @GroupCount + 1
	                INSERT INTO @table
	                SELECT @GroupCount,
	                       letterid,
	                       FCLT_ID,
	                       operationcode,
	                       operationDatetime,
	                       zipcode,
	                       dp2
	                FROM   #temp3
	                WHERE  SNo = @rowno
	                
	                SELECT @LID = letterid
	                FROM   #temp3
	                WHERE  SNo = @rowno
	            END
	            ELSE
	            BEGIN
	                INSERT INTO @table
	                SELECT '',
	                       letterid,
	                       FCLT_ID,
	                       operationcode,
	                       operationDatetime,
	                       zipcode,
	                       dp2
	                FROM   #temp3
	                WHERE  SNo = @rowno
	            END
	        END
	        SET @rowno = @rowno + 1
	    END
	    
	    SELECT *
	    FROM   @table 
	    
	    DROP TABLE #temp3
	END
	--"2nd_Pass_DeliveredCount"  
	IF @Status = '2nd_Pass_Delivered'
	BEGIN
	    SELECT ROW_NUMBER() OVER(ORDER BY ud.letterid, ud.operationdatetime) AS 
	           SNo,
	           ud.LetterID,
	           ud.FCLT_ID,
	           ud.OperationCode,
	           ud.OperationDateTime,
	           ud.ZipCode,
	           ud.DP2
	           INTO #temp4
	    FROM   MailerUSPSInfo.dbo.USPSPackageData ud
	    WHERE  ud.LetterID IN (SELECT DISTINCT t1.letterid
	                           FROM   MailerUSPSInfo.dbo.uspspackagedata t1
	                           WHERE  t1.letterid  IN (SELECT DISTINCT t2.letterid
	                                                   FROM   MailerUSPSInfo.dbo.uspspackagedata 
	                                                          t2 
	                                                          --Ozair 6405 08/27/2009 New Delivered Returned Code check implemented
	                                                   WHERE  t2.operationcode IN (
	                                                                              '146', 
	                                                                              '266', 
	                                                                              '276', 
	                                                                              '286', 
	                                                                              '336', 
	                                                                              '366', 
	                                                                              '376', 
	                                                                              '396', 
	                                                                              '406', 
	                                                                              '416', 
	                                                                              '426', 
	                                                                              '446', 
	                                                                              '466', 
	                                                                              '486', 
	                                                                              '496', 
	                                                                              '506', 
	                                                                              '806', 
	                                                                              '816', 
	                                                                              '826', 
	                                                                              '829', 
	                                                                              '836', 
	                                                                              '846', 
	                                                                              '856', 
	                                                                              '866', 
	                                                                              '876', 
	                                                                              '879', 
	                                                                              '886', 
	                                                                              '896', 
	                                                                              '899', 
	                                                                              '905', 
	                                                                              '906', 
	                                                                              '909', 
	                                                                              '911', 
	                                                                              '913', 
	                                                                              '915', 
	                                                                              '919', 
	                                                                              '966', 
	                                                                              '976')
	                                                          AND t2.LetterID 
	                                                              NOT IN (SELECT 
	                                                                             letterid
	                                                                      FROM   
	                                                                             MailerUSPSInfo.dbo.uspspackagedata
	                                                                      WHERE  
	                                                                             CONVERT(INT, operationcode) IN (91, 99)))
	                                  AND t1.letterid IN (SELECT DISTINCT b.noteid
	                                                      FROM   tblletternotes 
	                                                             b
	                                                      WHERE  DATEDIFF(DAY, @StartDate, b.currentdate) 
	                                                             >= 0
	                                                             AND DATEDIFF(DAY, @EndDate, b.currentdate) 
	                                                                 <= 0
	                                                             AND courtid = 
	                                                                 3037
	                                                             AND lettertype = 
	                                                                 55))
	    ORDER BY
	           ud.LetterID,
	           ud.OperationDateTime
	    
	    
	    
	    SET @GroupCount = 1
	    SET @rowno = 1
	    
	    SELECT @rowcount = COUNT(letterid)
	    FROM   #temp4
	    
	    WHILE (@rowno <= @rowcount)
	    BEGIN
	        IF (@rowno = 1)
	        BEGIN
	            SELECT @LID = letterid
	            FROM   #temp4
	            WHERE  SNo = @rowno
	            
	            INSERT INTO @table
	            SELECT @GroupCount,
	                   letterid,
	                   FCLT_ID,
	                   operationcode,
	                   operationDatetime,
	                   zipcode,
	                   dp2
	            FROM   #temp4
	            WHERE  SNo = @rowno
	        END
	        ELSE
	        BEGIN
	            IF @LID <> (
	                   SELECT letterid
	                   FROM   #temp4
	                   WHERE  SNo = @rowno
	               )
	            BEGIN
	                SET @GroupCount = @GroupCount + 1
	                INSERT INTO @table
	                SELECT @GroupCount,
	                       letterid,
	                       FCLT_ID,
	                       operationcode,
	                       operationDatetime,
	                       zipcode,
	                       dp2
	                FROM   #temp4
	                WHERE  SNo = @rowno
	                
	                SELECT @LID = letterid
	                FROM   #temp4
	                WHERE  SNo = @rowno
	            END
	            ELSE
	            BEGIN
	                INSERT INTO @table
	                SELECT '',
	                       letterid,
	                       FCLT_ID,
	                       operationcode,
	                       operationDatetime,
	                       zipcode,
	                       dp2
	                FROM   #temp4
	                WHERE  SNo = @rowno
	            END
	        END
	        SET @rowno = @rowno + 1
	    END
	    
	    SELECT *
	    FROM   @table 
	    
	    DROP TABLE #temp4
	END
GO

GRANT EXECUTE ON dbo.usp_HTP_GET_DetailMailerUSPSReportByStatus TO dbr_webuser
GO
﻿
/******  
* Created By :	  Syed Muhammad Ozair.
* Create Date :   11/08/2010 
* Task ID :		  8501
* Business Logic :  This procedure is used to Update patient questionaire information along with source and status by pateint id.
* List of Parameter :
* Column Return :     
******/
ALTER PROCEDURE dbo.usp_HTP_UpdatePatientQuestionaire
	@PatientId INT,
	@DoctorContacted BIT,
	@SurgeryMonth VARCHAR(2),
	@SurgeryDay VARCHAR(2),
	@SurgeryYear VARCHAR(4),
	@ContactComment VARCHAR(MAX),
	@HadBloodTest BIT,
	@BloodTestDetail VARCHAR(MAX),
	@IsProblem BIT,
	@ProblemComments VARCHAR(MAX),
	@PainSeverity INT,
	@isManufactrurer BIT,
	@ManufacturerComments VARCHAR(MAX),
	@WereScrewImplanted BIT,
	@ImplantMaterialId INT,
	@isDeviceDefected BIT,
	@DeviceDefectedDetail VARCHAR(MAX),
	@HasImplantRemoved BIT,
	@IsSchedule BIT,
	@ScheduleDate DATETIME,
	@ScheduleComments VARCHAR(MAX),
	@ObserverComments VARCHAR(MAX),
	@ManufacturerId INT=0,
	@NewManufacturer VARCHAR(200)='',
	@ImplantedHip BIT,
	@ImplantedHipComments VARCHAR(MAX)='',
	@OtherDeviceImplanted BIT,
	@OtherDeviceImplantedId INT=0,
	@OtherDeviceImplantedComments VARCHAR(MAX)='',
	@NewOtherDeviceImplanted VARCHAR(200),
	@ProductId VARCHAR(MAX),
	@WhyHispImplanted VARCHAR(MAX)
	
	
AS
	--Saeed 8585 12/03/2010 code add new manufactruer id if dont exist and update manufactruerid in patient table.
	DECLARE @manufacId INT
	SET @manufacId=NULL
	
	IF @NewManufacturer<>''
	BEGIN
		IF NOT EXISTS(SELECT [Name] FROM Manufacturer WHERE [name]=@NewManufacturer)
		BEGIN
			INSERT INTO Manufacturer([name]) VALUES(@NewManufacturer)
		SET @manufacId=SCOPE_IDENTITY()			
		END
	END
	ELSE IF @ManufacturerId>0
	BEGIN
		SET @manufacId=@ManufacturerId
	END
	------------------------------------------------------------
	
	--Saeed 8585 12/03/2010 code add new other device implanted if dont exist, and update respective fields in patient table.
	DECLARE @otherdeviceId INT
	SET @otherdeviceId=NULL
	
	IF @NewOtherDeviceImplanted<>''
	BEGIN
		IF NOT EXISTS(SELECT description FROM OtherImplantedDevice WHERE [description]=@NewOtherDeviceImplanted)
		BEGIN
			INSERT INTO OtherImplantedDevice(description) VALUES(@NewOtherDeviceImplanted)
		SET @otherdeviceId=SCOPE_IDENTITY()			
		END
	END
	ELSE IF @OtherDeviceImplantedId>0
	BEGIN
		SET @otherdeviceId=@OtherDeviceImplantedId
	END
	------------------------------------------------------------
	
	
	
	UPDATE Patient
	SET    DoctorContacted = @DoctorContacted,
	       SurgeryMonth = @SurgeryMonth,
	       SurgeryDay = @SurgeryDay,
	       SurgeryYear = @SurgeryYear,
	       ContactComment = @ContactComment,
	       HadBloodTest = @HadBloodTest,
	       BloodTestDetail = @BloodTestDetail,
	       IsProblem = @IsProblem,
	       ProblemComments = @ProblemComments,
	       PainSeverity = @PainSeverity,
	       isManufactrurer = @isManufactrurer,
	       ManufacturerComments = @ManufacturerComments,
	       WereScrewImplanted = @WereScrewImplanted,
	       ImplantMaterialId = @ImplantMaterialId,
	       isDeviceDefected = @isDeviceDefected,
	       DeviceDefectedDetail = @DeviceDefectedDetail,
	       HasImplantRemoved = @HasImplantRemoved,
	       IsSchedule = @IsSchedule,
	       ScheduleDate = @ScheduleDate,
	       ScheduleComments = @ScheduleComments,
	       ObserverComments = @ObserverComments,
	       ManufactuerId = @manufacId,
	       ImplantedHip=@ImplantedHip,
	       ImplantedHipComments=@ImplantedHipComments,
	       OtherDeviceImplanted=@OtherDeviceImplanted,
	       OtherDeviceImplantedId=@otherdeviceId,
	       OtherDeviceImplantedComments=@OtherDeviceImplantedComments,
	       ProductId=@ProductId,
	       WhyHispImplanted=@WhyHispImplanted
	       
	       
	       
	       
	WHERE  Id = @PatientId
GO


GRANT EXECUTE ON dbo.usp_HTP_UpdatePatientQuestionaire TO dbr_webuser
GO
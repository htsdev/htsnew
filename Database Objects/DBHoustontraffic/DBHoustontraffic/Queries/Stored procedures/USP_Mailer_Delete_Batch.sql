SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_Mailer_Delete_Batch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_Mailer_Delete_Batch]
GO

-- USP_Mailer_Delete_Batch 4000
CREATE procedure dbo.USP_Mailer_Delete_Batch	
	(
	@BatchID int,
	@isDeleted tinyint = 0  
	)


as

set nocount on 
declare @isExists tinyint, @isTodaysBatch tinyint

-- CHECK IF ANY MAILER ID RELATED TO THE BATCH TAGGED WITH ANY CASE.....
if exists (
		select t.mailerid from tbltickets t inner join tblletternotes n
		on n.noteid = t.mailerid and n.batchid_fk = @batchid
		union
		select t.mailerid from dallastraffictickets.dbo.tbltickets t inner join tblletternotes n
		on n.noteid = t.mailerid and n.batchid_fk = @batchid
		) 
	set @isExists  = 1
else
	set @isExists  =0

-- CHECK IF THE BATCH IS TODAY'S BATCH....
if exists (
	select batchid from tblbatchletter where batchid = @batchid 
	and datediff(day, batchsentdate, getdate()) = 0
	)

	set @isTodaysBatch = 1
else
	set @isTodaysBatch = 0

-- IF BATCH IS NOT TODAY'S BATCH OR TAGGED WITH ANY CASE
-- DO NOT DELETE AND RETURN FALSE.....
IF @isTodaysBatch = 0 or @isExists = 1
	begin
		set @isDeleted = 0
	end

-- ELSE DELETE THE BATCH ....
if @isTodaysBatch = 1 and @isExists = 0
begin
		delete from tblletternotes_detail 
		where noteid in (
			select noteid from tblletternotes where batchid_fk = @batchid
			)

		delete from tblletternotes where batchid_fk = @batchid

		delete from tblbatchletter where batchid = @batchid

		set @isDeleted = 1
end

select @isDeleted as IsDeleted
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


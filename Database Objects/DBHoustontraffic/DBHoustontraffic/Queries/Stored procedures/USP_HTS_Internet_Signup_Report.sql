/*************
Created By : Zeeshan Ahmed 

Business Logic : This procedure is used to view Public Site Clients that are contacted or not contacted
 
List of Parameters:     
	@StartDate : Start Date
	@EndDate   : End Date
	@AllDates  : Show All records
	@ContactType : Contact Type 0: All, 1: Contacted,2 : Not Contacted 

List of Columns: 
    
      TicketID_pk
      FirstName
      LastName
      Recdate
      FileName 
      
*************/ 
--USP_HTS_Internet_Signup_Report  '09/15/2008','09/15/2008',1,1
Alter  procedure [dbo].[USP_HTS_Internet_Signup_Report]      
 (     

@StartDate datetime,    
@EndDate datetime,    
@AllDates bit,    
@ContactType int --Sabir Khan 4749 09/15/2208
)    
as      
    
select Distinct  
	t.TicketID_pk ,FirstName , LastName , ttp.Recdate  , Convert(varchar,sp.scanbookid )+'-'+ Convert(varchar,sp.DocID) + '.' +  sp.DocExtension  as FileName   
	,CONVERT(VARCHAR(10),ttp.Recdate,111) as [SignupDate],SUBSTRING(CONVERT(varchar(24), ttp.Recdate , 121), 12, 12) as [SignupTime]
from tblticketspayment ttp      
	join tbltickets t on t.ticketid_pk = ttp.ticketid      
	join tblscanbook sb on t.ticketid_pk = sb.ticketid      
	join tblscanpic sp on sb.scanbookid = sp.scanbookid      

where ttp.paymenttype = 7       
		and doctypeid = 11
		-- Afaq 8093 07/28/2010 get internet signup receipt only.
		AND CHARINDEX('INTERNET CLIENT SIGNUP RECEIPT', sb.ResetDesc) > 0    
		and sb.employeeid = 3992    
		and ((@ContactType =2 and t.ticketid_pk not in ( Select ticketid_pk from tblticketsflag where flagid = 18)) --Sabir Khan 4749 09/15/2008
				or (@ContactType =1 and t.ticketid_pk in ( Select ticketid_pk from tblticketsflag where flagid = 18 )))
        and ((@AllDates=1) or (@AllDates=0 and datediff(day,@StartDate,ttp.recdate) >0 and  datediff(day,ttp.recdate,@EndDate)>0))

order by ttp.recdate desc
﻿/****** 
Created by:		Syed Muhammad Ozair
Business Logic:	The Procedure is used to get Auto Dialer OutCome Details. 
				
				
List of Parameters:
	@Court
	@EventType 
	@StartDate 
	@EndDate
	@ResponseTypeIDs
	@ShowAll	

List of Columns:	
	callid:
	ticketid_fk:
	Bond:	 
	eventtypename:
	calldatetime:
	calldateforsorting:
	lastname:
	firstname: 
	contactnumber:
	responsetype:
	settinginfo:
	recordingpath:
*******/

ALTER procedure dbo.usp_AD_Get_CallOutCome 

@Court int,
@EventType tinyint,
@StartDate datetime,
@EndDate datetime,
@ResponseTypeIDs varchar(100),
@ShowAll bit

as

declare @sqlstring nvarchar(max)

set @sqlstring='
select distinct
o.callid,
o.ticketid_fk,
case when o.isbond=1 then ''B'' else '''' end as Bond, 
s.eventtypename, 
dbo.fn_DateFormat(o.calldatetime,30,''/'','':'',1) as calldatetime,
o.calldatetime as calldateforsorting, 
t.lastname, 
t.firstname, 
o.contactnumber,
r.responsetype,
[dbo].[FN_AD_Get_Outcome_SettingsInfo](o.callid) as settinginfo,
case when (o.responseid_fk =1 or o.responseid_fk=4 or o.responseid_fk=7) then o.recordingpath else '''' end as recordingpath --Farrukh 9538 06/01/2012 Recording added for Voicemail message
from autodialercalloutcome o inner join autodialercalloutcomedetail d 
on o.callid=d.callid_fk inner join autodialereventconfigsettings s
on o.eventtypeid_fk=s.eventtypeid inner join tbltickets t
on o.ticketid_fk=t.ticketid_pk inner join autodialerresponse r
on o.responseid_fk=r.responseid
where 1=1 '

if @ShowAll=0
	set @sqlstring=@sqlstring+' and datediff(day, o.calldatetime, '''+ convert(varchar, @StartDate, 101) + ''' ) <= 0  
	and datediff(day, o.calldatetime, '''+ convert(varchar, @EndDate, 101) + ''' ) >= 0'   

if @EventType > 0
		set @sqlstring= @sqlstring + ' and o.eventtypeid_fk='+convert(varchar,@EventType)

if @Court = 1
	set @sqlstring= @sqlstring + ' and d.courtid in (3001,3002,3003)'
else if @Court = 2
	set @sqlstring= @sqlstring + ' and d.courtid not in (3001,3002,3003)'

set @sqlstring= @sqlstring + ' and o.responseid_fk in (select * from dbo.sap_string_param('''+@ResponseTypeIDs+'''))'

set @sqlstring= @sqlstring + ' order by calldatetime'

exec (@sqlstring)

--print (@sqlstring)


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_ESignature_Get_PrestoredSignature]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_ESignature_Get_PrestoredSignature]
GO


CREATE PROCEDURE [dbo].[USP_HTS_ESignature_Get_PrestoredSignature]
@Entity as varchar(15),
@SignID as varchar(20)
As
Select Top 1 SignData From Tbl_HTS_ESignature_DefaultSignatures Where  Entity = @Entity And SignID = @SignID

	

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


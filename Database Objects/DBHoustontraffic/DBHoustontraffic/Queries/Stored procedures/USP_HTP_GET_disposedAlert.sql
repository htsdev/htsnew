
 -- ====================================================================================================================================================================================    
 -- Author:  Afaq Ahmed    
 -- Create date: 04/29/2010    
 -- Bussiness Logic: Get all client cases of HMC and HCJP with Auto status of 'Disposed' but an Verified status of 'Jury, Judge, PreTrial and Arraignment' with future court date    
 -- Parameters:
 -- @showall: Will display All records when set to 1 else display only due follow-up cases. 
 -- TasK ID  : 7752  
 -- ====================================================================================================================================================================================    
ALTER PROCEDURE [dbo].[USP_HTP_GET_disposedAlert]
@showall bit = 1,
@ValidationCategory int=0 -- Saeed 7791 07/09/2010 @ValidationCategory=1 for alert & @ValidationCategory=2 for report
 AS    
 SELECT tt.TicketID_PK ,    
 ttv.casenumassignedbycourt AS 'CauseNumber',    
 tt.Firstname,    
 tt.Lastname,     
 ttv.RefCaseNumber,    
 tc.ShortName,    
 (SELECT tcvs.[Description]    
  FROM tblCourtViolationStatus tcvs     
  WHERE tcvs.CourtViolationStatusID=ttv.CourtViolationStatusIDmain) AS 'VerifiedStatus',    
 dbo.fn_DateFormat(ttv.CourtDateMain, 25, '/', ':', 1) AS 'VerifiedCourtDate',    
 (SELECT tcvs.[Description]    
  FROM tblCourtViolationStatus tcvs     
  WHERE tcvs.CourtViolationStatusID=ttv.CourtViolationStatusID) AS 'AutoStatus',
 dbo.fn_DateFormat(ttv.CourtDate, 25, '/', ':', 1) AS 'AutoCourtDate',    
 CONVERT(VARCHAR(10),tt.DisposedAlertFollowUpDate,101) AS 'FollowUpDate',
 tt.DisposedAlertFollowUpDate AS sortfollowupdate
 FROM tblTicketsViolations ttv    
 INNER JOIN tblTickets tt    
 ON tt.TicketID_PK = ttv.TicketID_PK    
 INNER JOIN tblCourts tc    
 ON ttv.CourtID= tc.Courtid    
 WHERE 
 --Active client
 tt.Activeflag = 1
 --Sabir Khan 8909 02/09/2011 Auto Status Dispose
 AND ttv.CourtViolationStatusID = 80
 --Sabir Khan 8909 02/09/2011 Covring Firm Sullo Law
 AND ISNULL(ttv.CoveringFirmID,3000) = 3000
 --Sabir Khan 8909 02/09/2011 Verified Status in Arr, Jury, Judge or Pre Trial and Verified Court Date in future.
 AND (ttv.CourtViolationStatusIDmain IN (SELECT CourtViolationStatusID FROM tblCourtViolationStatus WHERE CategoryID IN (2,3,4,5)) AND DATEDIFF(DAY,ttv.CourtDateMain,GETDATE())<0)
 --AND ttv.CourtViolationStatusID IN (3,101,103,26)   -- 3 = Arraignment, 101 = PreTrial, 103 = JUDGE & 26 = Jury
 AND ttv.CourtID IN (3007, 3008, 3009, 3010, 3011, 3012, 3013, 3014, 3015, 3016, 3017, 3018, 3019, 3020, 3021, 3022, 3001, 3002, 3003) -- Adil 7752 07/10/2010 Only HCJP and HMC cases.
 AND (SELECT SUM(ISNULL(ttp.ChargeAmount, 0)) -- Paid amount must be greater than zero
        FROM tblTicketsPayment ttp WHERE ttp.PaymentType <> 8 AND ttp.PaymentVoid = 0 AND ttp.TicketID = ttv.TicketID_PK)>0
 --AND DATEDIFF(DAY,ISNULL(ttv.CourtDate, '1/1/1900'), GETDATE()) < 0
  AND 
  (
  	(@showall = 1 OR @ValidationCategory=2) -- Saeed 7791 07/10/2010 display all records if showAll=1 or validation category is 'Report'
  	OR 
  	( (@showall = 0 OR @ValidationCategory=1) AND DATEDIFF(DAY,ISNULL(tt.DisposedAlertFollowUpDate,'1/1/1900'),GETDATE())>=0) -- Saeed 7791 07/09/2010 display  if showAll=0 or validation category is 'Alert' then display records which has 'follow up date' null,past or today.
  )
 ORDER BY sortfollowupdate
 



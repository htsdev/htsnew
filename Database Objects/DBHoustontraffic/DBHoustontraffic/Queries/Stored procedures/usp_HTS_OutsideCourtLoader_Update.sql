if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_HTS_OutsideCourtLoader_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_HTS_OutsideCourtLoader_Update]
GO


CREATE PROCEDURE [dbo].[usp_HTS_OutsideCourtLoader_Update]  
@TicketID int,  
@TicketNumber varchar(20),  
@CourtDate datetime,  
@EmpID int,
@CourtStatus int
as  
UPDATE tblticketsviolations  
 set  
  CourtDate = @CourtDate,
  CourtviolationstatusID = @CourtStatus, 
  VEmployeeID = @EmpID  
 where  
  TicketID_PK = @TicketID and  
  casenumassignedbycourt = @TicketNumber    
   
  
GO
GRANT EXEC ON [dbo].[usp_HTS_OutsideCourtLoader_Update] TO [dbr_webuser]
GO

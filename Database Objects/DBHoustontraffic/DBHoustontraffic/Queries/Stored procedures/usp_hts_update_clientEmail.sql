SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_hts_update_clientEmail]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_hts_update_clientEmail]
GO




create procedure usp_hts_update_clientEmail
@ticketid int,
@email varchar(255)
as
update tbltickets 
set email =@email
where ticketid_pk=@ticketid






GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_LA_InsertOrUpdate_Summary]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_LA_InsertOrUpdate_Summary]
GO



CREATE PROCEDURE USP_HTS_LA_InsertOrUpdate_Summary
@Loader_ID as int,
@Record_Date as datetime,
@List_Date as datetime,
@Reliable_Records as int,
@NonReliable_Records as int,
@AddressVerified_Records_Y as int,
@AddressVerified_Records_D as int,
@AddressVerified_Records_S as int,
@AddressVerified_Records_N as int,
@Inserted_Records as int,
@Updated_Records as int,
@GroupID as int

AS
Declare @Record_ID as int

Set @Record_ID = (Select GroupID From Tbl_HTS_LA_Summary Where GroupID = @GroupID)

If @Record_ID is null
	Begin
		Insert Into Tbl_HTS_LA_Summary (Loader_ID, Record_Date, List_Date, Reliable_Records, NonReliable_Records, AddressVerified_Records_Y, AddressVerified_Records_D, AddressVerified_Records_S, AddressVerified_Records_N, Inserted_Records, Updated_Records, GroupID) Values (@Loader_ID, @Record_Date, @List_Date, @Reliable_Records, @NonReliable_Records, @AddressVerified_Records_Y, @AddressVerified_Records_D, @AddressVerified_Records_S, @AddressVerified_Records_N, @Inserted_Records, @Updated_Records, @GroupID)
	End
Else
	Begin
		Update Tbl_HTS_LA_Summary Set Loader_ID = @Loader_ID, Record_Date = @Record_Date, List_Date = @List_Date, Reliable_Records = @Reliable_Records, NonReliable_Records = @NonReliable_Records, AddressVerified_Records_Y = @AddressVerified_Records_Y, @AddressVerified_Records_D = @AddressVerified_Records_D, AddressVerified_Records_S = @AddressVerified_Records_S, AddressVerified_Records_N = @AddressVerified_Records_N, Inserted_Records = @Inserted_Records, Updated_Records = @Updated_Records Where GroupID = @GroupID

	End


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


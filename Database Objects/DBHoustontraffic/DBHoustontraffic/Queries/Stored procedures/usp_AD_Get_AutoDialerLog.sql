﻿ /****** 
Created by:		Muhammad Nasir
Business Logic:	The Procedure is used to get Auto Dialer Log Details. 
				
				
List of Parameters:	
	@EventType 
	@StartDate 
	@EndDate	

List of Columns:	
	ID:	
	eventtypename:
	calldatetime:
	Note:
*******/

Create procedure dbo.usp_AD_Get_AutoDialerLog 

@EventType TINYINT,
@StartDate datetime,
@EndDate datetime


as

declare @sqlstring nvarchar(max)

set @sqlstring='
select distinct
L.ID,
L.eventtype, 
dbo.fn_DateFormat(L.TimeStamp,30,''/'','':'',1) as calldatetime,
L.TimeStamp,
L.note
from AutoDialerLog L
where 1=1 '

	set @sqlstring=@sqlstring+' and datediff(day, L.TimeStamp, '''+ convert(varchar, @StartDate, 101) + ''' ) <= 0  
	and datediff(day, L.TimeStamp, '''+ convert(varchar, @EndDate, 101) + ''' ) >= 0'   
IF(@EventType=1)
	set @sqlstring= @sqlstring + ' and L.eventtype like ''set call'''
else
    set @sqlstring= @sqlstring + ' and L.eventtype like ''reminder call'''

set @sqlstring= @sqlstring + ' order by L.TimeStamp' 

exec (@sqlstring)

--print (@sqlstring)

go

grant exec on dbo.usp_AD_Get_AutoDialerLog to dbr_webuser
go
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_GET_SubReports]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_GET_SubReports]
GO

CREATE procedure [dbo].[USP_HTS_GET_SubReports]       
 (                   
 @TicketID int    
                
 ) 
              
                  
as  

 exec  usp_hts_GetNotes_CR @TicketID
 exec  usp_hts_get_SummaryInfoDetail @TicketID


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


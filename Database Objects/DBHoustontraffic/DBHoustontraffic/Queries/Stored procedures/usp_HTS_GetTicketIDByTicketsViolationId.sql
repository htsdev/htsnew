SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_HTS_GetTicketIDByTicketsViolationId]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_HTS_GetTicketIDByTicketsViolationId]
GO

-- usp_HTS_GetTicketIDByTicketsViolationId 783908
      
CREATE procedure [dbo].[usp_HTS_GetTicketIDByTicketsViolationId]          
          
@ticketsViolationID int
          
as          
          
declare @ticketid int  
declare @courtdate datetime  
declare @courtid int  
declare @categoryid int  
declare @activeflag int  
set @ticketid=0  
  
  
  
select @ticketid=tv.ticketid_pk,@courtdate=tv.courtdatemain,@courtid=tv.courtid,@categoryid=cvs.categoryid,@activeflag=1           
from tblticketsviolations  tv inner join     
 tbltickets t on     
 t.ticketid_pk=tv.ticketid_pk inner join        
 tblcourtviolationstatus cvs on         
 cvs.courtviolationstatusid=tv.courtviolationstatusidmain        
where t.activeflag=1    
and ticketsviolationid=@ticketsViolationID      
  
if @ticketid =0  
begin  
 select @ticketid=tv.ticketid_pk,@courtdate=tv.courtdatemain,@courtid=tv.courtid,@categoryid=cvs.categoryid,@activeflag=0           
from tblticketsviolations  tv inner join     
 tbltickets t on     
 t.ticketid_pk=tv.ticketid_pk inner join        
 tblcourtviolationstatus cvs on         
 cvs.courtviolationstatusid=tv.courtviolationstatusidmain        
where t.activeflag=0    
and ticketsviolationid=@ticketsViolationID      
end  
  
if @ticketid<>0  
 select @ticketid as TicketID,@courtdate as courtdatemain,@courtid as courtid,@categoryid as categoryid,@activeflag as client  
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


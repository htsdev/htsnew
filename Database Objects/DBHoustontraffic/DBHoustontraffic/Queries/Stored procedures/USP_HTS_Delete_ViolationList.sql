SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Delete_ViolationList]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Delete_ViolationList]
GO



CREATE procedure [dbo].[USP_HTS_Delete_ViolationList]
	(
	@ViolId int
	)
as 

update tblviolations set violationtype = 0 where violationnumber_pk = @violid

declare @temp table(
	RowId	int identity(0,1),
	ViolId	int
	)

insert into @temp (violid)
select	violationnumber_pk
from	tblviolations where violationtype = 8 order by sequenceorder

update	v
set		v.sequenceorder  = t.rowid
from	tblviolations v inner join @temp t
on		t.violid = v.violationnumber_pk






GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


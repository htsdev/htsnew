    
Alter procedure usp_hts_update_PaymentStaus    
    
@PaymentStatusID int,    
@tiketid int    
    
as    
    
-- COMMENT THE WHOLE CODE  
  --Agha Usman 2664 06/10/2008
-- Noufil 4270 06/23/2008 Code Uncomment as on this task's Requirement
 
  
  update tbltickets  
	set paymentstatus=@PaymentStatusID  
	where ticketid_pk=@tiketid  

go
GRANT EXEC ON [dbo].[usp_hts_update_PaymentStaus] to dbr_webuser
go



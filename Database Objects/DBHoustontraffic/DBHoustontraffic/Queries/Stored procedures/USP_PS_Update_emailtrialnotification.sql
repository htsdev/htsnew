--- This procedue update tblemailtrialnotification and insert Confirmation date when client click on Confirmation link     
--USP_PS_Update_emailtrialnotification 108597Aug242007339PM4    
alter procedure [dbo].[USP_PS_Update_emailtrialnotification]    
    
@EmailID varchar(250)    
    
AS    
    
Update tblemailtrialnotification set ConfirmationDate = getdate()    
where EmailID = @EmailID    
select @@rowcount    
    
    
------------------------------------------------------------------------------------------------------------------------------------    
    
  
go  
grant exec on   [dbo].[USP_PS_Update_emailtrialnotification] to dbr_webuser
go

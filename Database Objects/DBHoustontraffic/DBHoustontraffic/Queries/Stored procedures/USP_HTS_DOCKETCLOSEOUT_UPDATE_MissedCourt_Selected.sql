/************  

Created By : Anonymous 

Business Logic : This Procedure is used to set status of violations to missed court

List of Paremeters :

	@TicketsViolationID : Ticket Violation Id
	@EmpID : Employee ID 
	@MissedCourtType : Missed Court Type 

************/
Alter procedure dbo.USP_HTS_DOCKETCLOSEOUT_UPDATE_MissedCourt_Selected      
 (      
 @TicketsViolationID int,      
 @EmpID int  ,    
 @MissedCourtType int = -1	 
)      
      
      
as      
      
      
update tblticketsviolations       
set  
  courtviolationstatusidmain = 105,      
  courtviolationstatusid = 105,      
  courtviolationstatusidscan = 105,      
  courtnumber = courtnumbermain,      
  courtnumberscan = courtnumbermain,      
  courtdate = courtdatemain,      
  courtdatescan = courtdatemain,      
  vemployeeid = @EmpID,
  MissedCourtType = Case When  @MissedCourtType = 11 then 1 when  @MissedCourtType = 12 then 2 else -1 end    
where ticketsviolationid = @TicketsViolationID      
    
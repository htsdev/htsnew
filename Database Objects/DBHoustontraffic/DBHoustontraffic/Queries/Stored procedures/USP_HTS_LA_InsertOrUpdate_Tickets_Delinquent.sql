SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_LA_InsertOrUpdate_Tickets_Delinquent]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_LA_InsertOrUpdate_Tickets_Delinquent]
GO




CREATE PROCEDURE USP_HTS_LA_InsertOrUpdate_Tickets_Delinquent

@Record_Date datetime,  
@List_Date datetime,  
@Cause_Number as Varchar(20),  
@Ticket_Number as Varchar(20),  
@MID_Number as varchar(20),  
@Name_First as varchar(50),  
@Name_Middle as varchar(50),  
@Name_Last as varchar(50),  
@Address as varchar(50),  
@Address2 as varchar(50),  
@City as varchar(35),  
@State as Varchar(10),  
@ZIP as varchar(10),  
@Telephone_Number as varchar(20),  
@Race as varchar(20),  
@Height as varchar(10),  
@Gender as char(1),  
@Birth_Date as smalldatetime,  
@Violation_Date as smalldatetime,
@Updated_Date as smalldatetime,
@Violation_Code as Varchar(20),
@Violation_Description as Varchar(2000),
@Court_Room_Number as varchar(10),  
@Court_Date Smalldatetime,
@Court_Time Varchar(10),
@Fine_Amount as float,
@Officer_Number as Int,
@GroupID as Int,
@Bond_Amount as float,
@AddressStatus Char(1),
@Flag_SameAsPrevious as bit,
@PreviousRecordID as int,
@CurrentRecordID int output,
@IOU_Mode int Output

AS  
Declare @OfficerName as Varchar(40)
Declare @StateID as Int
Declare @CourtID as Int
Declare @Gender_Support as Varchar(6)
Declare @ViolationID as int
Declare @PlanID as int
Declare @IDFound as int
Declare @Ticket_NumberFound as varchar(20)
Declare @TicketNumber_LA as varchar(20)

Set @Cause_Number = Left(RTrim(LTrim(@Cause_Number)), 30)
Set @Ticket_Number = Left(RTrim(LTrim(@Ticket_Number)), 20)
Set @MID_Number = Left(RTrim(LTrim(@MID_Number)), 20)
Set @Name_Middle = Left(RTrim(LTrim(@Name_Middle)),5)
Set @Name_Last = Left(RTrim(LTrim(@Name_Last)),20)
Set @Name_First = Left(RTrim(LTrim(@Name_First)),20)
Set @Address = Left(RTrim(LTrim(@Address)), 50)
Set @Address2 = Left(RTrim(LTrim(@Address2)), 50)
Set @City = Left(RTrim(LTrim(@City)), 50)
Set @ZIP = Left(RTrim(LTrim(@ZIP)), 12)
Set @Telephone_Number = Left(RTrim(LTrim(@Telephone_Number)), 15)
Set @Race = Left(RTrim(LTrim(@Race)), 10)
Set @Height = Left(RTrim(LTrim(@Height)), 10)
Set @Gender = Left(RTrim(LTrim(@Gender)), 10)
Set @Violation_Code = Left(RTrim(LTrim(@Violation_Code)), 10)
Set @Violation_Description = Left(RTrim(LTrim(@Violation_Description)), 200)
Set @AddressStatus = Left(RTrim(LTrim(@AddressStatus)), 1)

Set @Height = Replace(@Height,'"','')
Set @Court_Date = @Court_Date + Convert(datetime, @Court_Time)
Set @ViolationID = 0
Set @PlanID = 0
Set @Cause_Number = Replace(@Cause_Number, ' ', '')
Set @IOU_Mode = 0
Set @TicketNumber_LA = @Ticket_Number + '0'

If Len(@Ticket_Number) < 1
	Begin
		Set @Ticket_Number = @Cause_Number
		Set @TicketNumber_LA = @Cause_Number
	End

Select @Race = Case Upper(Left(@Race,1))
	when 'A' then 'Asian'
	when 'B' then 'Black'
	when 'H' then 'Hispanic'
	when 'W' then 'White'
	else 'Other'
End

If @Court_Room_Number = '13' or @Court_Room_Number = '14'
	Begin
		Set @CourtID = 3002
	End
Else If @Court_Room_Number = '18'
	Begin
		Set @CourtID = 3003
	End
Else If Ltrim(Rtrim(Upper(@Court_Room_Number))) = 'ADMIN'
	Begin
		Set @Court_Room_Number = '0'
		Set @CourtID = 3003
	End
Else 
	Begin
		Set @CourtID = 3001
	End

Set @ViolationID = (Select Top 1 ViolationNumber_PK From TblViolations Where (ViolationCode = @Violation_Code))

If @ViolationID Is Null
	Begin
		Insert Into TblViolations (Description, ViolationCode) Values(@Violation_Description, @Violation_Code)
		Set @ViolationID = Scope_Identity()
	End

Set @OfficerName = (Select Top 1 FirstName + ' ' + LastName From TblOfficer Where OfficerNumber_PK = @Officer_Number)

Set @StateID = (Select Top 1 StateID From TblState Where State = @State)

Set @PlanID = (Select Top 1 PlanID From tblPricePlans Where CourtID = @CourtID)

If @Gender like 'M%'
	Begin
		Set @Gender_Support = 'Male'
	End
Else If @Gender like 'F%'
	Begin
		Set @Gender_Support = 'Female'
	End


If @Flag_SameAsPrevious = 0
	Begin
		Set @IDFound = (SELECT Top 1 RecordID From tblTicketsArchive Where ViolationDate = @Violation_Date And MidNumber = @MID_Number And CourtID = @CourtID)
		If @IDFound Is Null
			Begin
				Insert Into tblTicketsArchive (RecLoadDate, ListDate, TicketNumber, MidNumber, FirstName, Initial, LastName, Address1, Address2, City, StateID_FK, ZipCode, PhoneNumber, Race, Height, Gender, DOB, ViolationDate, CRRT,CourtDate, CourtTime, OfficerName, officerNumber_Fk, CourtID, GroupID, Flag1)   
				Values(@Record_Date, @List_Date, @Ticket_Number, @MID_Number, @Name_First, @Name_Middle, @Name_Last, @Address, @Address2, @City, @StateID, @ZIP, @Telephone_Number, @Race, @Height, @Gender_Support, @Birth_Date, @Violation_Date, @Court_Room_Number, @Court_Date, @Court_Time, @OfficerName, @Officer_Number, @CourtID, @GroupID, @AddressStatus)	

				Set @CurrentRecordID = scope_identity()
			End
		Else
			Begin
				Set @CurrentRecordID = @IDFound
				Update tblTicketsArchive Set GroupID = @GroupID Where (RecordID = @CurrentRecordID)
			End
	End
Else
	Begin
		Set @CurrentRecordID = @PreviousRecordID
	End

Set @Ticket_NumberFound = (Select top 1 TicketNumber_LA From tblTicketsViolationsArchive
			Where 
			(CauseNumber = @Cause_Number  Or (TicketNumber_LA = @Ticket_Number And TicketNumber_LA <> '' And TicketNumber_LA Is Not Null)))

If @Ticket_NumberFound Is Null
	Begin
		Insert Into tblTicketsViolationsArchive (TicketNumber_PK, ViolationNumber_PK, TicketNumber_LA, FineAmount, ViolationDescription, ViolationCode, CourtDate, UpdatedDate, Courtnumber, CourtLocation, TicketViolationDate, TicketOfficerNumber, RecordID, CauseNumber, BondAmount, ViolationStatusID, LoaderUpdateDate, DLQUpdateDate)
		Values(@Ticket_Number, 0, @TicketNumber_LA, @Fine_Amount, @Violation_Description, @Violation_Code, @Court_Date, @Updated_Date, @Court_Room_Number, @CourtID, @Violation_Date, @Officer_Number, @CurrentRecordID, @Cause_Number, @Bond_Amount, 146, GetDate(), GetDate())
		Set @IOU_Mode = 1
	End
Else
	Begin
		Set @IDFound = (Select top 1 RecordID From tblTicketsViolationsArchive Where (TicketNumber_LA = @Ticket_NumberFound And (UpdatedDate < @Updated_Date Or UpdatedDate Is Null)))
		If @IDFound Is Not Null
			Begin
				Update tblTicketsViolationsArchive Set ViolationStatusID = 146, UpdatedDate = @Updated_Date, LoaderUpdateDate = GetDate(), DLQUpdateDate = @Updated_Date Where (TicketNumber_LA = @Ticket_NumberFound)
				Update tblTicketsViolations Set CourtViolationStatusID = 146, UpdatedDate = @Updated_Date, PlanID = @PlanID Where (CaseNumAssignedByCourt = @Cause_Number And (UpdatedDate < @Updated_Date Or UpdatedDate Is Null))
				Set @IOU_Mode = 2
			End
	End

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


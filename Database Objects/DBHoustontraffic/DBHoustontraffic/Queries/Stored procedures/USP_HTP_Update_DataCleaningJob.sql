/******   
Create by:  Tahir Ahmed 
Create DAte: 06/26/2009

Business Logic :This Procedure is used by Houston Data Cleaning Job scheduled on database server.
				It updates the data in non-clients and in quote/client databases for HTP.
  
List of Parameters:   
	N/A
  
List of Columns:    
	N/A
  
******/  

-- exec USP_HTP_Update_DataCleaningJob
alter procedure dbo.USP_HTP_Update_DataCleaningJob

as


create table #results (
	TicketId int,
	Recordid int,
	UpdateType tinyint
	)

/* update type legend....

1 = court location updated in nonclients	-recordid
2 =	recordid updated in nonclients	-recordid
3 =	recordid updated in clients	-ticketid
4 =	clientflag updated in nonclients	-recordid
5 =	mailerid updated in clients	-ticketid
6 =	midnumber updated in clients	-ticketid
7 =	officer number updated in clients	-ticketid

*/


/*
STEP 1:
	SCRIPT TO UPDATE COURT LOCATION IN TBLTICKETSVIOLATIONSARCHIVE BY TICKET NUMBER
*/

update	v
set	v.courtlocation =t.courtid
output null, inserted.recordid, 1 into #results
from	tblticketsarchive t, tblticketsviolationsarchive v
where	t.ticketnumber=v.ticketnumber_pk
and	isnull(v.courtlocation,0)=0
and isnull(t.courtid,0) > 0
and	t.clientflag=0


/*
STEP 2:
	SCRIPT TO UPDATE RECORD ID IN TBLTICKETSVIOLATIONSARCHIVE TABLE 
*/
 

set nocount on

declare 	@TicketNum varchar(20)
declare 	@RecordID int
declare 	@CourtId1 int
declare 	@midNumber varchar(20)
declare 	@listDate datetime,
	@idx1 int,
	@rec1 int,
	@violno int
declare @OfficerNumber int
declare @ViolationDate datetime



CREATE TABLE #temptable1(
	Rowid int primary key IDENTITY(1,1) NOT NULL,
	Ticketnumber_pk varchar(30),
	Violationnumber_pk smallint,
	courtlocation int
) 


insert into #temptable1 (ticketnumber_pk, violationnumber_pk, courtlocation)
Select	tva.ticketnumber_pk, tva.violationnumber_pk, tva.courtlocation
FROM	dbo.tblTicketsViolationsArchive TVA, dbo.tblTicketsArchive TA 
where	tva.ticketnumber_pk = ta.ticketnumber
and	ta.clientflag=0  
and	isnull(tva.recordid,0) = 0
	
select @RecordID = 0, @idx1 = 1, @rec1 = count(rowid) from #temptable1

while @idx1 <= @rec1
begin

	Select 	@TicketNum = TicketNumber_pk ,
		@ViolNo = violationnumber_pk,
		@CourtId1 = CourtLocation 
	from 	#temptable1
	where	rowid = @idx1

	-- Abbas Qamar 05-26-2012 10088 Adding HMC-W Court ID
	if @courtid1 in (3001,3002,3003,3075)
		begin
			-- Pick Midnumber & list date for selection of recordid 
			select	@midNumber = midnumber, 
				@OfficerNumber = officernumber_fk, 
				@violationdate = violationdate
			FROM	tblTicketsArchive 
			where	ticketnumber =  @TicketNum 
			and	isnull(CourtID,0)  = @CourtId1
	
			SELECT  top 1  @RecordID = isnull(recordid,0) FROM tblTicketsArchive 
			where	midnumber = @midNumber  
			and	officernumber_fk = @officernumber
			and	datediff(day, violationdate , @violationdate) =0 
	
		end
	else
		begin
			-- Pick Midnumber & list date for selection of recordid 
			select	@midNumber = midnumber, 
				@listdate = listdate
			FROM	tblTicketsArchive 
			where	ticketnumber =  @TicketNum 
			and	isnull(CourtID,0)  = @CourtId1
	
			SELECT  top 1  @RecordID = isnull(recordid,0) FROM tblTicketsArchive 
			where	midnumber = @midNumber  
			and	datediff(day,listdate , @listdate) = 0
		end
	
	--- Update Recordid 
	update	tblTicketsViolationsArchive set recordID = @RecordID 
	output null, inserted.recordid, 2 into #results
	where	TicketNumber_pk = @TicketNum and (isnull(CourtLocation,0) =  @CourtID1)

	set @idx1 = @idx1 + 1
end 

Drop table #temptable1


/*
STEP 3 a:
	SCRIPT TO UPDATE RECORD ID IN TBLTICKETSVIOLATIONS TABLE 
*/

---------------------------------------------------------------------
-- TAHIR AHMED 5318 01/01/2009 FIXED THE RECORDID UPDATE ISSUE FOR HMC COURTS....
---------------------------------------------------------------------

CREATE TABLE #Tickets (ticketsviolationid int, recordid int)

-- HMC CASES......

-- FIRST GET THE RECORDS FROM NON-CLIENTS BY MATCHIGN CAUSE NUMBERS.......
INSERT INTO #Tickets (ticketsviolationid, recordid)
select distinct v.ticketsviolationid, va.recordid
from tblticketsviolations v inner join tblticketsviolationsarchive va
on va.causenumber = v.casenumassignedbycourt
where len(isnull(v.casenumassignedbycourt,'')) > 0
-- Abbas Qamar 05-26-2012 10088 Adding HMC-W Court ID
and isnull(v.recordid,0) =0 and v.courtid in (3001,3002,3003,3075)
and isnull(va.recordid,0) > 0

-- FIRST GET THE RECORDS FROM NON-CLIENTS BY COMPLETE TICKET NUMBER.......
INSERT INTO #Tickets (ticketsviolationid, recordid)
select distinct v.ticketsviolationid, va.recordid
from tblticketsviolations v 
inner join tbltickets t on t.ticketid_pk = v.ticketid_pk
inner join tblticketsviolationsarchive va on va.ticketnumber_pk  = v.refcasenumber
inner join tblticketsarchive ta on ta.recordid = va.recordid
where len(isnull(v.refcasenumber,'')) > 0
-- Abbas Qamar 05-26-2012 10088 Adding HMC-W Court ID
and isnull(v.recordid,0) =0 and v.courtid in (3001,3002,3003,3075)
and isnull(va.recordid,0) > 0
and t.lastname = ta.lastname 
and left(t.firstname,1) = left(ta.firstname,1)
and v.ticketsviolationid not in (select ticketsviolationid from #tickets)

-- FIRST GET THE RECORDS FROM NON-CLIENTS BY COMPLETE TICKET NUMBER.......
INSERT INTO #Tickets (ticketsviolationid, recordid)
select distinct v.ticketsviolationid, va.recordid
from tblticketsviolations v 
inner join tbltickets t on t.ticketid_pk = v.ticketid_pk
inner join tblticketsviolationsarchive va on v.refcasenumber =  left(va.ticketnumber_pk , len(va.ticketnumber_pk )-1 )
inner join tblticketsarchive ta on ta.recordid= va.recordid
where len(isnull(v.refcasenumber,'')) > 0
-- Abbas Qamar 05-26-2012 10088 Adding HMC-W Court ID
and isnull(v.recordid,0) =0 and v.courtid in (3001,3002,3003,3075)
and t.lastname = ta.lastname 
and left(t.firstname,1) = left(ta.firstname,1)
and isnull(va.recordid,0) > 0
and v.ticketsviolationid not in (select ticketsviolationid from #tickets)


--UPDATING RECORD ID IN TBLTICKETSVIOLATIONS
begin tran

alter table tblTicketsViolations DISABLE TRIGGER trg_tblTicketsViolations_Events 
alter table tblTicketsViolations DISABLE TRIGGER TRig_Violations 

update	v
set	v.recordid=u.recordid
output inserted.ticketid_pk, null, 3 into #results
from	tblticketsviolations v inner join #Tickets u
on	v.ticketsviolationid=u.ticketsviolationid

alter table tblTicketsViolations ENABLE TRIGGER trg_tblTicketsViolations_Events 
alter table tblTicketsViolations ENABLE TRIGGER TRig_Violations 

commit tran


drop table #tickets

---------------------------------------------------------------------
-- END 5318
---------------------------------------------------------------------


--FOR NON CLIENTS--
--GETTING DUPLICATE TICKET NUMBERS FROM TBLTICKETSVIOLATIONS
select	refcasenumber,  count(ticketsviolationid) as tvcount
into	#temp
from	tblticketsviolations v, tbltickets t
where	len(isnull(refcasenumber,'')) > 3
and	t.ticketid_pk = v.ticketid_pk
and	datediff(Day, t.recdate, '1/1/07')<=0
and	isnull(v.recordid,0)=0
--and	courtid<>3037
group by	refcasenumber
having	count(ticketsviolationid) >1
order by	refcasenumber

--select * from #temp
--GETTING ALL NON DUPLICATE TICKET NUMBERS FROM TBLTICKETSVIOLATIONS
select	refcasenumber,  v.ticketid_pk,ticketsviolationid
into	#temp1
from	tblticketsviolations v, tbltickets t
where	len(isnull(refcasenumber,'')) > 3
and	t.ticketid_pk = v.ticketid_pk
and	datediff(Day, t.recdate, '1/1/07')<=0
and	isnull(v.recordid,0)=0
and	refcasenumber not in (select refcasenumber from #temp)
--and	courtid<>3037
order by	refcasenumber

--select * from #temp1
--GETTING ALL TICKET NUMBERS AND THIER RECORD ID FROM TBLTICKETSVIOLATIONSARCHIVE
--FOR ALL NON DUPLICATE TICKET NUMBERS FROM TBLTICKETSVIOLATIONS
select	v.recordid ,v.ticketnumber_pk
into	#temp2
from	tblticketsarchive a, tblticketsviolationsarchive v
where	a.recordid=v.recordid
and	v.ticketnumber_pk in (select refcasenumber from #temp1)
and	v.recordid<>0

--select * from #temp2

--GETTING DUPLICATE TICKET NUMBERS FROM TBLTICKETSVIOLATIONSARCHIVE
select	ticketnumber_pk,count(recordid) as con
into	#temp3
from	#temp2
group by	ticketnumber_pk
having	count(recordid)>1
order by	ticketnumber_pk

--select * from #temp3

--GETTING ALL NON DUPLICATE TICKET NUMBERS FROM TBLTICKETSVIOLATIONS ARCHIVE 
select	ticketnumber_pk,recordid
into	#temp4
from	#temp2
where	ticketnumber_pk not in (select ticketnumber_pk from #temp3)

--select * from #temp4

--GETTING ALL THE TICKETNUMBERS AND THEIR RECORD ID TO UPDATE 
select	refcasenumber, ticketid_pk,ticketsviolationid,recordid
into	#temp5
from	#temp4 a, #temp1 v
where	a.ticketnumber_pk=v.refcasenumber

--select * from #temp5

--UPDATING RECORD ID IN TBLTICKETSVIOLATIONS
begin tran

alter table tblTicketsViolations disable trigger trg_tblTicketsViolations_Events
alter table tblTicketsViolations disable trigger TRig_Violations
--DISABLE TRIGGER trg_tblTicketsViolations_Events ON tblTicketsViolations
--DISABLE TRIGGER TRig_Violations ON tblTicketsViolations

update	v
set	v.recordid=u.recordid
output inserted.ticketid_pk, null, 3 into #results
from	tblticketsviolations v inner join #temp5 u
on	v.ticketsviolationid=u.ticketsviolationid

alter table tblTicketsViolations enable trigger trg_tblTicketsViolations_Events
alter table tblTicketsViolations enable trigger TRig_Violations


--ENABLE TRIGGER trg_tblTicketsViolations_Events ON tblTicketsViolations
--ENABLE TRIGGER TRig_Violations ON tblTicketsViolations

commit tran

--DROPPING TEMP TABLES
drop table #temp
drop table #temp1
drop table #temp2
drop table #temp3
drop table #temp4
drop table #temp5


/*
STEP 4 a:
	SCRIPT TO UPDATE CLIENT FLAG IN NON CLIENTS FOR MANUALLY ADDED CASES
*/
update	a
set	a.clientflag=1
output null, inserted.recordid, 4 into #results
from	tblticketsarchive a,tblticketsviolations v
where	a.recordid=v.recordid
and	isnull(v.recordid,0)<>0
and	a.clientflag=0


/*
STEP 5:
	SCRIPT TO UPDATE MAILER ID 
*/

SET NOCOUNT ON   
  
declare @rec int, @idx int, @courtid int, @ticketid int , @UpdateCount int, @cnt int 

create table #updated (ticketid int, IsUpdated int)
  
CREATE TABLE #tickets2(  
 rowid int primary key identity(1,1),  
 ticketid int  NOT NULL,  
 courtid [int] NULL,  
 )

insert into #tickets2 (ticketid , courtid)  
select distinct  t.ticketid_pk, isnull(v.courtid,0) as courtid  
from tbltickets t inner join tblticketsviolations v  
on t.ticketid_pk = v.ticketid_pk  
where isnull(t.mailerid, 0) = 0  
and datediff(day, t.recdate,'1/1/2007') <= 0  
order by t.ticketid_pk, [courtid]  
  
  
select @idx = 1, @rec = count(rowid) from #tickets2  
set @updatecount = 0
  
while @idx <=  @rec  
begin  
 select @ticketid = ticketid, @courtid = courtid   
 from #tickets2 where rowid = @idx  
  
if @courtid = 3037  
	-- jims......  
	begin
		insert into #updated (ticketid, isupdated)
		--select @ticketid, 1
		EXEC dbo.USP_HTS_Update_Mailer_id @TicketID, 3992, 3  
	end

 -- civil..
 else if @courtid = 3077
	begin
		insert into #updated (ticketid, isupdated)
		--select @ticketid, 1
		EXEC dbo.USP_HTS_Update_Mailer_id @TicketID, 3992, 5
	end
 else  
  -- houston....  
	begin
		insert into #updated (ticketid, isupdated)
		--select @ticketid, 1
	    EXEC dbo.USP_HTS_Update_Mailer_id @TicketID, 3992, 1  
	end
   
 set @idx = @idx + 1  
end  

insert into #results (ticketid, updatetype)
select distinct ticketid ,5 from #updated where isupdated = 1

drop table #tickets2
drop table #updated


/*
STEP 6 a:
	SCRIPT TO UPDATE MID NUMBER IN TBLTICKETS TABLE FROM NON CLIENTS FOR CASES THAT
ARE MANUALLY ADDED IN QUOTE/CLIENT.
*/
--FOR NON_CLIENTS
begin tran 
alter table tblTickets disable trigger trg_tblTickets_Events
--DISABLE TRIGGER trg_tblTickets_Events ON tblTickets

update	t
set	t.midnum=a.midnumber
output inserted.ticketid_pk, null, 6 into #results
from	tbltickets t,tblticketsviolations v,tblticketsarchive a
where	a.recordid=v.recordid
and	v.ticketid_pk=t.ticketid_pk
and	isnull(v.recordid,0)<>0
and	len(isnull(t.midnum,''))= 0
and 	v.courtid<>3037

--ENABLE TRIGGER trg_tblTickets_Events ON tblTickets
alter table tblTickets enable trigger trg_tblTickets_Events

commit tran

begin tran
--DISABLE TRIGGER trg_tblTickets_Events ON tblTickets
alter table tblTickets disable trigger trg_tblTickets_Events

-- tahir TT 351 04/13/2009 update mid number in quote/client if it is non numeric for HMC cases.
update t set t.midnum = a.midnumber
output inserted.ticketid_pk, null, 6 into #results
from tblticketsviolations v inner join tbltickets t
on t.ticketid_pk = v.ticketid_pk
inner join tblticketsviolationsarchive b on b.recordid= v.recordid
inner join tblticketsarchive a on a.recordid = b.recordid
where isnull(v.recordid,0) > 0
and isnumeric(isnull(t.midnum,'')) = 0
-- Abbas Qamar 05-26-2012 10088 Adding HMC-W Court ID
and v.courtid in (3001,3002,3003,3075)
and b.courtlocation in (3001,3002,3003,3075)
and isnumeric(isnull(a.midnumber,'')) = 1

--ENABLE TRIGGER trg_tblTickets_Events ON tblTickets
alter table tblTickets enable trigger trg_tblTickets_Events

commit tran


/*
STEP 10 a:
	SCRIPT TO UPDATE Officer NUMBER IN TBLTICKETSVIOLATIONS TABLE FROM NON CLIENTS.
*/


--Get All records for updating Officer number...
select  Distinct  V.TicketID_PK, V.ticketsviolationid, TVA.RecordID,V.RefCaseNumber, TVA.TicketOfficerNumber
INTO #tempOfficer
FROM tblTicketsViolationsArchive TVA INNER JOIN tblTicketsViolations V ON TVA.RecordID = V.RecordID
INNER JOIN tblTicketsArchive TA ON TVA.RecordID = TA.RecordID
INNER JOIN tblTickets T ON V.TicketID_PK = T.TicketID_PK
WHERE ISNULL(V.RecordID,0) > 0
-- Abbas Qamar 05-26-2012 10088 Adding HMC-W Court ID
AND V.courtid IN (3001,3002,3003,3075)
AND TVA.courtlocation in (3001,3002,3003,3075)
AND ISNUMERIC(ISNULL(V.TicketOfficerNumber,'')) = 0
AND ISNUMERIC(ISNULL(TVA.TicketOfficerNumber,'')) = 1 
ORDER BY v.ticketid_pk 

-- Add identity field for loop...
alter table #tempOfficer add rowid int identity(1,1)

--Variable declaration...
declare @idx3 int, @reccount int, @ticketid3 int , @ticketsviolationid int,  @empid INT, @tempupdate INT, @TicketNumber VARCHAR(50),@OfficerNumber3 INT
		
select @idx3 = 1, @reccount = count(rowid), @ticketid3 = 0, @ticketsviolationid = 0, @tempupdate = 0 , @empid = 3992, @TicketNumber = NULL, @OfficerNumber3 = 0
FROM #tempOfficer
While @idx3 <= @reccount
begin
	select	@ticketid3 = ticketid_pk, @ticketsviolationid = ticketsviolationid, @TicketNumber = RefCaseNumber, @OfficerNumber3 =  TicketOfficerNumber		
	from #tempOfficer where rowid = @idx3
	
	begin tran

	--Update Officer Number in client from non-clients...
	UPDATE V SET V.TicketOfficerNumber = @OfficerNumber3,
			v.vemployeeid = @empid
	output inserted.ticketid_pk, null, 7 into #results
	FROM tblTicketsViolations V 
	INNER JOIN tblTickets T ON V.TicketID_PK = T.TicketID_PK
	
	WHERE V.TicketID_PK = @ticketid3
	AND V.ticketsviolationid = @ticketsviolationid
	
	-- Add note into history...
	set @tempupdate = @@rowcount
	IF(ISNULL(@tempupdate,0)) > 0
	BEGIN
			insert into tblticketsnotes (ticketid, subject , employeeid)
			select @ticketid3, @TicketNumber + ':Officer number updated to ' + CONVERT(VARCHAR(50),@OfficerNumber3), @empid			
	END 
	
	commit tran

set @idx3 = @idx3 + 1
END
DROP TABLE #tempOfficer


-- SUMMARIZING AND SENDING EMAIL......
declare @UpdateCount1 int, @UpdateCount2 int, @UpdateCount3 int, @UpdateCount4 int, @UpdateCount5 int, @UpdateCount6 int, @UpdateCount7 int,
		@body varchar(max)

SELECT @UpdateCount1 = COUNT(DISTINCT recordid) FROM #results WHERE UpdateType  = 1 AND ISNULL(recordid,0) > 0
SELECT @UpdateCount2 = COUNT(DISTINCT recordid) FROM #results WHERE UpdateType  = 2 AND ISNULL(recordid,0) > 0
SELECT @UpdateCount3 = COUNT(DISTINCT ticketid) FROM #results WHERE UpdateType  = 3 AND ISNULL(ticketid,0) > 0
SELECT @UpdateCount4 = COUNT(DISTINCT recordid) FROM #results WHERE UpdateType  = 4 AND ISNULL(recordid,0) > 0
SELECT @UpdateCount5 = COUNT(DISTINCT ticketid) FROM #results WHERE UpdateType  = 5 AND ISNULL(ticketid,0) > 0
SELECT @UpdateCount6 = COUNT(DISTINCT ticketid) FROM #results WHERE UpdateType  = 6 AND ISNULL(ticketid,0) > 0
SELECT @UpdateCount7 = COUNT(DISTINCT ticketid) FROM #results WHERE UpdateType  = 7 AND ISNULL(ticketid,0) > 0

DROP TABLE #results
	
SELECT @body = 'The Houston Data Cleaning Job has been completed and following are the results:<br><br>
<table width=450 border=1 cellpadding= 0 cellspacing=0>
<tr><td>Court location in non-clients database is updated for </td><td>' + CONVERT(VARCHAR, isnull(@UpdateCount1,0)) +  ' profiles</td></tr>
<tr><td>Record Id in non-clients database is updated for</td><td>' + CONVERT(VARCHAR, isnull(@UpdateCount2,0)) +  ' profiles</td></tr>
<tr><td>Record Id in quote/clients database is updated for </td><td>' + CONVERT(VARCHAR, isnull(@UpdateCount3,0)) +  ' profiles</td></tr>
<tr><td>Client Flag in non-clients database is updated for </td><td>' + CONVERT(VARCHAR, isnull(@UpdateCount4,0)) +  ' profiles</td></tr>
<tr><td>Mailer Id in quote/clients database is updated for </td><td>' + CONVERT(VARCHAR, isnull(@UpdateCount5,0)) +  ' profiles</td></tr>
<tr><td>Mid Number in quote/clients database is updated for </td><td>' + CONVERT(VARCHAR, isnull(@UpdateCount6,0)) +  ' profiles</td></tr>
<tr><td>Officer Number in quote/clients database is updated for </td><td>' + CONVERT(VARCHAR, isnull(@UpdateCount7,0)) +  ' profiles</td></tr>
</table>
'	
	
EXEC msdb.dbo.sp_send_dbmail 
	@profile_name = 'Data Loader', 
	@recipients = 'dataloaders@sullolaw.com',  
	@copy_recipients = '',
	@blind_copy_recipients = 'tahir@lntechnologies.com',
	@subject = 'Data Cleaning Job Completed',
	@body = @body, 
	@body_format = 'HTML'

go

grant execute on dbo.USP_HTP_Update_DataCleaningJob to dbr_webuser
go
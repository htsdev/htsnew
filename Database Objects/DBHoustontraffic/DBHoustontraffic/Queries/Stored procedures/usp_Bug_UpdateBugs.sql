SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Bug_UpdateBugs]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Bug_UpdateBugs]
GO



create procedure [dbo].[usp_Bug_UpdateBugs]

@PriorityID int,              
   
@BugID int  
           
              
as              
              
update tbl_bug  set             
            
 PriorityID=@PriorityID      
        
 
where             
 bug_id=@BugID         
  





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTP_GetAllWrongResponsedBySMS_Clients]    Script Date: 05/24/2013 15:27:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/****** 
Created by		:		Rab Nawaz Khan
Task ID			:		10914
Business Logic	:		The Procedure is used to get all the clients who responsed Wrong 'any thing else then 'YES'' for the system sended SMS. 
	
*******/

--[dbo].[USP_HTP_GetAllWrongResponsedBySMS_Clients] 
						
  
ALTER PROCEDURE [dbo].[USP_HTP_GetAllWrongResponsedBySMS_Clients]
	
AS
BEGIN
	SELECT [ID], [Url], [APIID], [FromNumber], [ToNumber] ,[ReceivedDate], [Text], [UDH], [MessageId], [InsertDate], [TicketNumber], [RecordID],
	[IsSMSReplyConfirmed], [TicketID], [IsWrongReplySmsSend]
    FROM [ArticleManagementSystem].[dbo].[SMSDetails]
	WHERE IsSMSReplyConfirmed = 0
	AND ISNULL(TicketID, 0) <> 0
	AND IsWrongReplySmsSend = 0 -- Should not used ISNULL function else result will be wrong and all the previous clients will also be fetched by SP.
END

GO
GRANT EXECUTE ON [dbo].[USP_HTP_GetAllWrongResponsedBySMS_Clients] TO dbr_webuser
GO





 ALTER procedure [dbo].[USP_HTS_Create_Customer_Profile]                    
 (                    
         
--Customer Information Parameter         
 @customerid int,              
 @firstname [varchar] (100) ,                
 @lastname [varchar] (100) ,                
 @telephone [varchar] (20) ,                
 @alttelephone [varchar] (20) ,                
 @alttelephone2 [varchar] (20),                
 @height [varchar] (20) ,                
 @weight [varchar] (20) ,                
 @HairColor [varchar] (20) ,                
 @EyeColor [varchar] (20) ,           
 @email [varchar] (50) ,                
 @contact [varchar] (50) ,                
 @homeaddress [varchar] (100) ,                
 @city [varchar] (50) ,                
 @zip [varchar] (12) ,                
 @dob datetime ,                
 @driverlicense [varchar] (20) ,                
 @comments [text] ,                
 @crname [varchar] (50) ,                
 @crnumber [varchar] (20) ,                
 @trafficticket [varchar] (20) ,                
 @csc [varchar] (5) ,                
 @crmonth [tinyint] ,                
 @crtype [varchar] (20) ,                
 @cryear [varchar] (4) ,                
 @amount [numeric](18, 3),                
 @SessionID [varchar](100) ,                
 @Recdate [varchar] (30) ,                
 @ApprovedFlag [tinyint] = 0,            
 @IsProcessCompleted bit,              
 @StateID_Fk      [int]=0  ,              
 @ClientType [tinyint],           
 @dlstate int ,                      
 @Gender char(6),                    
 @Race varchar(20)    ,                  
 @TelType int,                
 @MailerID int =0,                
 @TicketNo int,              
 @ReceiptFileName varchar(200) ,              
 @LastPageIndex  int,            
 @siteid int ,          
 @recordid int,                      
-- Case Setting Information        
@accident bit,          
@school bit,          
@bondflag bit,          
@cdl bit,          
@CourtSettingID int ,         
@CourtNumber int,        
-- Case Violation Information        
@fineamount money,            
@bondamount money,            
@courtid int ,          
@courtdate datetime  ,          
@violationids varchar(2000),        
-- Payment Information        
@invnumber varchar(10),                      
@status varchar(2),                      
@errmsg varchar(255),                      
@transnum varchar(50),                      
@response_subcode  varchar(20),                          
@response_reason_code   varchar(20),                       
@response_reason_text varchar(100),                       
@auth_code   varchar(6),             
@avs_code varchar(1)                  
)                    
                    
as            
 -- VARIABLE DECLARATIONS.......                    
 declare        
 --Used For Court Search Case        
 @RecCount  int,                    
 @TicketId  int,                    
 @InvoiceNumber int,                    
 @empid int,                 
 @empdesc varchar(10),                
 --Used For Non Client Case s           
 @FTALinkID int     ,           
 @Officernumber  int  ,        
 @violationdate  datetime,        
 @listdate datetime,            
 @MailDate datetime             
        
         
 --Start Transaction               
 begin transaction                    
         
        
 --Get Employee Abbreviation        
 select @empid = 3992, @empdesc = abbreviation from dbo.tblusers where employeeid = 3992                 
        
 --If Case Is Non Client         
 if @TicketNo = 0 and @Recordid <> 0         
 Begin        
         
  --Update Court Date In Archive Table If Not Available         
  Update tblticketsviolationsarchive Set CourtDate = @CourtDate, CourtNumber = @CourtNumber                      
  where recordid = @RecordId and courtdate = '1/1/1900'           
         
  --Retrive Case Information From Archive Tables        
  select * into #tblTicketsArchive           
  from tblticketsarchive           
  where recordid =@RecordId                          
        
  --Retrive Violation Infromation From Archive Table           
  select * into #tblTicketsViolationsArchive          
  from tblticketsviolationsarchive          
  where recordid = @RecordId        
          
  -- Getting FTA Link ID To Add Other FTA Related Tickets In The Case             
  Set @FTALinkID = 0            
  Select Top 1 @FTALinkID = isnull(FTALinkID,0) from tblticketsviolationsarchive where recordid = @RecordId                      
        
  -- Getting Related FTA Tickets        
  if @FTALinkID <> 0             
  Begin            
   Insert into #tblTicketsViolationsArchive select * from tblticketsviolationsarchive         
   where recordid <>  @RecordId  and FTALinkID = @FTALinkID            
  End                
                  
  -- Set Information In Variables                      
  select          
  @officernumber = isnull(officernumber_fk,962528),          
  @violationdate = isnull(violationdate,'01/01/1900') ,          
  @courtdate = t.courtdate,          
  @courtid = t.courtid,          
  @listdate = t.listdate          
  from #tblTicketsArchive t inner join #tblTicketsViolationsArchive v          
  on t.recordid = v.recordid and t.recordid = @RecordId           
         
  --Declare Temp Table        
  declare @Temp table ( TicketNumber varchar(20),CourtDate datetime,MailDate datetime,ViolationDate datetime,recordid int)            
           
  insert into @Temp              
  select  distinct  TicketNumber,            
  courtdate,maildate = case when ticketnumber like  'F%' then maildatefta  else maildateregular  end,            
  isnull(violationdate,'01/01/1900') , recordid           
  from   #tblTicketsArchive             
  where recordid = @recordid           
             
  -- Retrive Mail Date                            
  select @maildate = isnull(max(maildate),'01/01/1900') from @temp              
         
  ------------------------------------------------------------------------------------------------------------------------            
  -- INSERTING MAIN INFORMATION INTO TBLTICKETS FROM TBLTICKETSARCHIVE            
  ------------------------------------------------------------------------------------------------------------------------            
          
     insert into tbltickets(            
  midnum,  firstname,  lastname,  Address1,  Address2,  City,  Stateid_FK,  Zip,  Contact1, contactType1,          
  contact2, Contacttype2, contact3, contacttype3, height, weight, haircolor, eyes, DOB,  DLNumber,           
  dlstate,  ViolationDate,  OfficerNumber,  employeeid,  Race,   Gender,  courtdate,  maildate,            
  employeeidupdate, currentcourtloc, RecordId, email, generalcomments,          
  accidentflag, cdlflag, bondflag,  BondsRequiredflag  , languagespeak, MailerID,WalkInClientFlag          
  )           
             
  select distinct  MidNumber,t.firstname,t.lastname,t.address1,t.address2,t.city,          
  isnull(t.stateid_fk,45), t.zipcode, @telephone,case when len(@telephone)> 0 then 1 else 0 end,          
  @alttelephone, case when len(@alttelephone)> 0 then 4 else 0 end,          
  @alttelephone2,case when len(@alttelephone2)> 0 then isnull(@TelType,0) else 0 end,          
  @height,@weight,@haircolor,@eyecolor,@dob,@driverlicense,          
  @dlstate, ViolationDate,            
  OfficerNumber_FK, @EmpId, @Race, @Gender,            
  @courtdate,   @maildate,   @EmpId,   @courtid,    RecordId, @email,         
  case when len(isnull(convert(varchar(1900),@comments),'')) > 0           
  then convert(varchar(1900),@comments) + ' ('+dbo.formatdateandtimeintoshortdateandtime(getdate()) + ' - ' +  @empdesc + ')' + ' '           
  else '' end ,          
  @accident, @cdl, @bondflag, @bondflag, 'ENGLISH', @MailerID,0         
  from   #tblTicketsArchive t where t.recordid = @recordid          
               
  if @@error = 0           
  select @TicketID=@@identity                    
        
  ------------------------------------------------------------------------------------------------------------------------            
  --  INSERTING DETAILED INFORMATION INTO TBLTICKETSVIOLATIONS FROM TBLTICKETSVIOLATIONSARCHIVE             
  ------------------------------------------------------------------------------------------------------------------------            
  IF @TicketId <> 0           
  begin          
            
    -- Declare Temporary Tables        
    declare @temp1 table             
   (            
    violationnumber  int,            
    bondamount  money,            
    FineAmount   money,            
    TicketNumber   varchar(20),            
    sequenceNumber   tinyint,            
    Violationcode   varchar(10),            
    ViolationStatusID  int,            
    CourtNumber   varchar(10),            
    CourtDate   datetime,            
    ViolationDescription  varchar(200),            
    courtid   int,            
    ticketviolationdate  datetime,            
    ticketofficernum  varchar(20)  ,          
    CauseNumber varchar(25),          
    RecordId  int          
   )          
             
   declare @temp2 table            
   (            
    violationnumber  int,            
    fineamount  money,            
    ticketnumber  varchar(25),            
    sequencenummber  int,            
    violationcode  varchar(10),            
    bondamount  money,            
    violationstatusid int,            
    courtnumber  int,            
    courtdate  datetime,            
    courtid   int  ,          
    CauseNumber varchar(25),           
    recordid int          
   )          
                   
  --If Inside Courts         
  if @courtid in (3001,3002,3003)          
   begin          
    insert into @temp1            
    SELECT  distinct             
    isnull(L.ViolationNumber_PK,0) as ViolationNumber,            
    isnull(V.bondamount,0) as BondAmount,             
    V.FineAmount,            
    v.ticketnumber_pk,          
    V.ViolationNumber_PK as sequenceNumber,            
    V.violationcode as Violationcode,             
    case v.ViolationStatusID when 186 then 135 when 146 then 135 else v.ViolationStatusID end,            
    v.CourtNumber,            
    V.CourtDate,            
    isnull(v.ViolationDescription,'N/A') as violationdescription,             
    --@CourtId,             
    courtlocation,                            
    t.violationdate,            
    isnull(v.TicketOfficerNumber,962528)  ,          
    v.CauseNumber,          
    v.recordid          
    from  #tblticketsarchive t            
    INNER JOIN             
    #tblticketsviolationsarchive V             
    on ( t.recordid = v.recordid )          
    LEFT OUTER JOIN             
    tblViolations  L             
    ON   V.violationcode = L.violationcode             
   end          
  -- If Out Side Courts        
  else          
  begin          
   insert into @temp1            
   SELECT  distinct             
   isnull(L.ViolationNumber_PK,0) as ViolationNumber,            
   isnull(V.bondamount,0) as BondAmount,             
   V.FineAmount,            
   v.ticketnumber_pk,          
   '0' as sequenceNumber,            
   V.violationcode as Violationcode,             
   v.ViolationStatusID,            
   v.CourtNumber,            
   V.CourtDate,            
   isnull(v.ViolationDescription,'N/A') as violationdescription,             
   --@CourtId,             
   courtlocation,                            
   t.violationdate,            
   isnull(v.TicketOfficerNumber,962528)  ,          
   v.CauseNumber,          
   v.recordid          
   from  #tblticketsarchive t            
   INNER JOIN             
   #tblticketsviolationsarchive V             
   on ( t.recordid = v.recordid )          
   LEFT OUTER JOIN             
   tblViolations  L                  
   ON   V.violationdescription  = L.description          
   and  l.violationtype = (          
   select violationtype from tblcourtviolationtype          
   where courtid = @courtid )          
  end          
          
  -- Save Violation Information                    
  insert into @temp2            
     select V.ViolationNumber_PK , t1.fineamount,t1.TicketNumber,t1.sequencenumber,t1.violationcode,            
  t1.bondamount,ViolationStatusID, CourtNumber, CourtDate,  courtid  , t1.CauseNumber,t1.recordid          
  from @temp1 t1            
  inner join             
  tblViolations V             
  on  t1.sequencenumber = V.sequenceorder            
  where  t1.violationnumber = 0               
             
  update  t1            
  set t1.violationnumber = t2.violationnumber            
  from   @temp1 t1,             
 @temp2 t2            
  where   t1.violationcode = t2.violationcode            
              
  insert into tblticketsviolations            
  (            
  TicketID_PK,ViolationNumber_PK,FineAmount,refcasenumber,sequencenumber,bondamount,CourtViolationStatusID,            
  CourtNumber,CourtDate,violationdescription,courtid,ticketviolationdate, TicketOfficerNumber,            
  vemployeeid,CourtViolationStatusIDmain,CourtNumbermain,CourtDatemain,CourtViolationStatusIDscan,CourtNumberscan,            
  CourtDatescan ,casenumassignedbycourt,underlyingbondflag,PlanId,RecordID)            
               
    SELECT  distinct              
 @TicketID,            
  violationNumber,            
  FineAmount,            
  TicketNumber,            
  sequencenumber,            
  bondamount,            
  ViolationStatusID,            
  CourtNumber,            
  CourtDate,            
  violationdescription,            
  courtid,                            
  ticketviolationdate,            
  ticketofficernum,            
  @empid,            
  ViolationStatusID,            
  CourtNumber,            
  CourtDate,            
  ViolationStatusID,                     
  CourtNumber,            
  CourtDate,          
  CauseNumber,          
  case when isnull(courtdate, '1/1/1900') < getdate() then 1 else 0 end,          
  (select  top 1 planid from  tblpriceplans where  courtid =@Courtid          
  and  effectivedate <= @listdate and enddate > = @listdate          
  and isActivePlan = 1  order by planid          
  )  ,                  
  @RecordId          
   from  @temp1            
 End        
End        
        
-- If Client Is A Court Search Client         
if @ClientType = 0 and @recordid = 0 and @ticketno = 0         
Begin        
 ------------------------------------------------                     
 -- FIRST INSERTING RECORDS IN TBLTICKETS        
 ------------------------------------------------                     
 insert into dbo.tbltickets (                    
 firstname, lastname, contact1, contacttype1,  contact2, contacttype2, contact3,         
 contacttype3, height, weight,haircolor, eyes, email, address1, city, stateid_fk,        
 zip, dob, dlnumber, dlstate,calculatedtotalfee, totalfeecharged, recdate, accidentflag,         
 cdlflag, bondsrequiredflag, bondflag, employeeid,employeeidupdate, activeflag, lockflag,        
 firmid, basefeecharge, languagespeak  , generalcomments  , gender, race,WalkInClientFlag)                    
        
 values (        
 left(ltrim(@firstname),20),                   
 left(ltrim(@lastname),20),                     
 left(ltrim(@telephone),15),                     
 case when len(@telephone)> 0 then 1 else 0 end,                
 left(ltrim(@alttelephone),15),                     
 case when len(@alttelephone)> 0 then 4 else 0 end,                
 left(ltrim(@alttelephone2),15),                     
 case when len(@alttelephone2)> 0 then isnull(@TelType,0) else 0 end,                
 case when @height = '-NA-' then null else left(ltrim(@height),10) end ,                      
 case when @weight = '-NA-' then null else left(ltrim(@weight),10) end ,                     
 case when @HairColor = '-NA-' then null else left(ltrim(@HairColor),10) end ,                     
 case when @EyeColor = '-NA-' then null else left(ltrim(@EyeColor),10) end ,                     
 @email,left(ltrim(@homeaddress),50),@city, @stateid_fk,                  
 @zip,convert(datetime, @dob,101),                     
 @driverlicense,                   
 @dlstate,                  
 @amount,                     
 @amount,                      
 @Recdate,                     
 @accident   ,                
 @cdl ,                
 @bondFlag ,@bondFlag ,@empid,@empid,1,1,3000,                    
 @amount,                     
 'ENGLISH'  ,                  
 case when len(isnull(convert(varchar(1900),@comments),'')) > 0                 
 then convert(varchar(1900),@comments) + ' ('+dbo.formatdateandtimeintoshortdateandtime(getdate()) + ' - ' +  @empdesc + ')' + ' '                 
 else '' end ,                
 @gender,@race,0              
 )           
                      
 set @TicketId  = @@identity          
                  
 if @TicketId is not null                    
    begin            
        -------------------------------------------------        
  -- NOW INSERTING RECORDS IN TBLTICKETSVIOLATIONS                   
        -------------------------------------------------        
          
  --Declare Temp Table For Court Search Violations         
  declare @violations table ( violationnumber_pk int  )         
         
  --Get ViolationIds from CSV format        
  Insert into @violations Select * from dbo.Sap_String_Param(@violationids)         
        
  -- FIRST GETTING RECORDS IN A TEMP TABLE.....                    
  declare @tempCourt table                    
  (                    
  RowId int identity(1,1),                    
  violationnumber_pk int,                    
  refcasenumber varchar(30),                    
  courtdate datetime,                    
  courtid int,                    
  courtviolationstatusidmain int  ,                
  courtnumber int                 
  )                    
                      
  insert into @tempCourt ( violationnumber_pk, refcasenumber, courtdate, courtid, courtviolationstatusidmain, courtnumber)                    
           
  SELECT V.ViolationNumber_PK,'ID'+ convert(varchar(15), @TicketId),@CourtDate ,@CourtID,                     
  case @courtsettingid when 5 then 103 when 4 then 26 else 3 end , @courtnumber                
  from @violations V         
  INNER JOIN                    
  dbo.tblViolations v2                     
  On v.ViolationNumber_PK =  v2.ViolationNumber_PK         
  --AND  v2.Violationtype = 10                    
                     
  -- UPDATING SEQUENCE NUMBER IN TEMP TABLE......                    
  select @reccount = count(*) from @tempCourt                    
                      
  -- NOW INSERTING RECORDS FROM TEMP TABLE TO TBLTICKETSVIOLATIONS....                    
  insert into tblticketsviolations (                    
  ticketid_pk, violationnumber_pk,  refcasenumber, courtdate, courtnumber,                     
  courtviolationstatusid, courtdatemain, courtnumbermain, courtviolationstatusidmain,                    
  courtdatescan, courtnumberscan, courtviolationstatusidscan, courtid, planid  , underlyingbondflag                   
  )                    
                      
    select @ticketid,                     
  t.violationnumber_pk,                     
  t.refcasenumber,                    
  t.courtdate,                     
  t.courtnumber,                    
  t.courtviolationstatusidmain,                    
  t.courtdate,                    
  t.courtnumber,                    
  t.courtviolationstatusidmain,                    
  t.courtdate,                    
  t.courtnumber,                    
  t.courtviolationstatusidmain,                    
  t.courtid,                    
  planid = (                    
   select  top 1 planid                     
   from  tblpriceplans                     
   where  courtid =t.Courtid                    
   and  effectivedate <= getdate()                    
   and enddate > = getdate()                    
   and isActivePlan = 1                    
   )   ,                
  @bondflag                
    from @tempCourt t                    
 End        
End                     
                 
        
--If Quote Client         
if @TicketNo <> 0         
Begin        
        
 Update tbltickets                     
                   
Set                     
                    
 firstname = @FirstName ,                    
 lastname =@LastName ,                    
 Address1 = @homeaddress,                    
 City = @city,                    
 Stateid_FK = @StateID_Fk,                    
 Zip = @zip,                    
 Contact1 = @telephone,                    
 contactType1 =case when len(@telephone)> 0 then 1 else 0 end,                                            
 contact2 = @alttelephone,                     
 Contacttype2 =case when len(@alttelephone)> 0 then 4 else 0 end,                                 
 contact3 = @alttelephone2,                    
 contacttype3 =case when len(@alttelephone2)> 0 then isnull(@TelType,0) else 0 end,                                
 height = @height,                    
 weight = @weight,                    
 haircolor = @haircolor,                    
 eyes = @eyecolor,                    
 DOB = @dob,                    
 DLNumber = @driverlicense,                    
 dlstate = @dlstate,                      
 employeeid = 3992,                    
 Race = @race,                    
 Gender = @gender,                    
 email = @Email,                    
 bondflag = @bondflag ,                        
 bondsrequiredflag = @bondflag,                        
 accidentflag = @accident ,                        
 cdlflag =@cdl,                        
 lockflag = 1,                        
 TotalFeeCharged = CalculatedTotalFee ,            
 EmailNotAvailable = 0  ,                   
 NoDl = case when len(@driverlicense)> 0 then 0 else 1 end,            
 WalkInClientFlag=0,        
 ActiveFlag = 1                             
          
where ticketid_pk = @TicketNo                        
                    
 update dbo.tblticketsviolations                         
 set underlyingbondflag = @bondflag                      
 where ticketid_pk = @TicketNo           
         
        
set @TicketId = @TicketNo        
        
End        
        
        
        
-----------------------------------------------        
--Common Quries For Non / Quote / Court Clients        
-----------------------------------------------        
        
        
        
        
        
         
-------------------------------------------------------------------------------------------------------          
-- NOW INSERTING PAYMENT INFORMATION..................          
-------------------------------------------------------------------------------------------------------             
 -- INSERTING A NOTE IN CASE HISTORY.....                        
        
 insert into tblticketsnotes (ticketid, subject , employeeid)                     
 values ( @TicketId, 'Internet Client Signup'  ,@empid )                    
            
 -- NOW INSERTING FEE INFORMATION.............          
 update tbltickets           
 set basefeecharge = @amount,          
 calculatedtotalfee = @amount,          
 totalfeecharged = @amount,          
 feeinitial = @empdesc,          
 initialadjustmentinitial = @empdesc,          
 lockflag = 1,     initialadjustment = 0,          
 adjustment = 0,          
 ContinuanceAmount = 0,          
 InitialTotalFeeCharged = @amount,          
 employeeidupdate = 3992          
 where ticketid_pk = @ticketid                    
        
--------------------------------------------------------------------------------------------------------------------                      
 --    UPDATING CASE STATUS & COURT INFO IN TBLTICKETS....                    
 --------------------------------------------------------------------------------------------------------------------                      
           
        
update t                    
   set t.datetypeflag = c.CategoryID,                 
    t.CurrentDateset = v.courtdatemain,                    
    t.CurrentCourtNum = v.courtnumbermain,                    
    t.CurrentCourtloc = v.courtid                    
   FROM    tblTickets t                     
   INNER JOIN                    
                tblTicketsViolations v                     
   ON  t.TicketID_PK = v.TicketID_PK                     
   INNER JOIN                    
                tblCourtViolationStatus c                     
   ON  v.CourtViolationStatusIDmain = C.CourtViolationStatusID              
   where v.courtdatemain = (                    
     select min(courtdatemain) from tblticketsviolations v2                    
     where v2.ticketid_pk = t.ticketid_pk                     
     )                    
   and t.ticketid_pk = @TicketID                    
        
--------------------------------------------------------------------------------------------------------------------                      
--    INSERTING PAYMENT RECORDS.........                    
--------------------------------------------------------------------------------------------------------------------                      
                      
insert into tblticketspayment (paymenttype, ticketid,  chargeamount, paymentvoid , recdate, employeeid )          
    values                    (7,@TicketId,@amount,0,getdate(),@empid )                    
            
select @InvoiceNumber = scope_identity()                  
        
 --Added By Zeeshan Ahmed For TrackPayment            
 update tblticketscybercashlog set invnumber = Convert(varchar,@InvoiceNumber) , reftransnum =''             
 where ticketid = @customerid and transtype = 0             
            
 -- INSERTING CREDIT CARD INFORMATION....                    
   insert into dbo.tblticketscybercash (                    
    ticketid, invnumber, amount, ccnum, expdate, name, recdate, status,                     
    transnum, retcode, errmsg, employeeid, approvedflag, clearflag, cin, cardnumber, response_subcode,                    
    response_reason_code, response_reason_text , auth_code, avs_code, cardtype                    
    )                    
   values  ( @ticketid, @InvoiceNumber, @amount, @crnumber, Convert(varchar,@crmonth) + '/' +Convert(varchar,@cryear) , @crname, getdate(), @status,                     
    @transnum,         
        
  '',--@retcode,--NA Comment By zeeshan        
        
 @errmsg, @empid, @approvedflag,         
  0,--@clearflag, --NA        
        
 @csc, @crnumber, @response_subcode,                    
    @response_reason_code, @response_reason_text, @auth_code, @avs_code, @crtype                    
   )             
                
        
--------------------------------------------------------------------------------------------------------------------                      
--    INSERTING INTERNET CLIENT RECEIPT IMAGE TO SCANNED DOCS                
--------------------------------------------------------------------------------------------------------------------                      
         
 DECLARE @BookID int, @picID int , @cmd varchar(255)  , @notes varchar(500)                      
  insert into tblScanBook (EmployeeID,TicketID,DocTypeID,updatedatetime,DocCount,ResetDesc)                 
  values(@empid,@ticketid,11,getdate(),1,'INTERNET CLIENT SIGNUP RECEIPT') -- insert into book                          
                      
  select @BookID =  Scope_Identity() --Get Book Id of the latest inserted                          
                
  insert into tblScanPIC (ScanBookID,updatedatetime,DocExtension)                 
  values( @BookID,getdate(),'PDF') --insert new image into database                          
        select @picID =  Scope_Identity() --Get Book Id of the latest inserted                          
  --Docpic = @image          
                
  set @notes = '<a href="javascript:window.open(''../paperless/PreviewMain.aspx?DocID='+ convert(varchar(10), @BookID)+'&RecType=1&DocNum=0&DocExt=PDF'');void('''');"''>SCANNED DOCUMENT - OTHER [INTERNET CLIENT SIGNUP RECEIPT]</a>'                       
  insert into tblTicketsNotes (TicketID,Subject,EmployeeID)       
  values (@TicketID, @notes ,@EMPID)                     
                
 set @ReceiptFileName = convert(varchar(10), @bookid) +'-'+ convert(varchar(10), @picid) + '.PDF'               
        
 --------------------------------------------------------------------------------------------------------------------                      
 --    INSERTING HISTORY NOTES                      
 --------------------------------------------------------------------------------------------------------------------                      
                      
declare @strNote varchar(100)                
 select  @strNote = 'INITIAL INQUIRY '+ upper(isnull(tc.shortname,'N/A')) + ':' + upper(isnull(c.ShortDescription,'N/A')) + ':' + CONVERT(varchar(10), isnull(v.CourtDateMain,'1/1/1900'), 101) + ' #' +  isnull(v.courtnumbermain ,'0')+ ' @ '         
+ upper(dbo.formattime(isnull(v.CourtDateMain,'1/1/1900')))                
 FROM tblTicketsViolations v                 
 left outer join                
  tblCourtViolationStatus c                 
 ON  v.CourtViolationStatusIDmain = c.CourtViolationStatusID                
 left outer  join                
  tblcourts tc                
on  v.courtid=tc.courtid                
 where  v.ticketid_pk = @TicketID                
                 
 if @strNote is not null                
    insert into tblticketsnotes(ticketid, subject, employeeid) values (@TicketID, @strNote, @empid )                      
        
        
--------------------------------------------------------------------------------------------------------------------            
--    UPDATING CLIENT FLAG IN TBLTICKETSARHCIVE          
--------------------------------------------------------------------------------------------------------------------            
   if @recordid <> 0         
    Begin        
     update  tblticketsarchive set clientflag = 1 where recordid  = @Recordid              
    End                  
        
--------------------------------------------------------------------------------------------------------------------            
--     Added By Zeeshan For Mailer ID for 2nd June 2007 Mailer ID Modifications                  
--------------------------------------------------------------------------------------------------------------------            
   if @MailerID <> 0         
  Begin        
   exec USP_HTS_Update_Mailer_id @TicketID, @empid, 1          
  End        
           
  
  
---Update Transaction Log Information  
Update tblTicketsCybercashLog set TicketID = @ticketid ,  invnumber = @InvoiceNumber    
where invnumber = @customerid  and transnum = @transnum   
  
--Commit Transaction        
commit transaction         
        
        
Select @ReceiptFileName as  ReceiptFileName  , 1 as ApprovedFlag , @ticketid as TicketID  , @InvoiceNumber as InvoiceNumber  
go

GRANT EXEC on dbo.USP_HTS_Create_Customer_Profile to dbr_webuser
go 
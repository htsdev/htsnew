/*    
* Created By  : SAEED AHMED  
* Task ID   : 7791    
* Created Date  : 05/29/2010    
* Business logic : This procedure is used to get all the available timings on which primary/secondary user can be scheduled to receive emails.  
*     : There are two type of timings: Weekday Timings and Saturday Timings.  
*   
*  
*/  
CREATE PROC dbo.USP_HTP_GET_EmailTimings  
AS  
SELECT TimingID AS id,TimingValue AS timings FROM ValidationEmailTimings 

GO
GRANT EXECUTE ON dbo.USP_HTP_GET_EmailTimings TO dbr_webuser
GO

USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[usp_hts_get_NewPaymentDetails]    Script Date: 01/02/2014 03:47:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*        
Created By     : Fahad Muhammad Qureshi.
Created Date   : 07/27/2009  
TasK		   : 6054        
Business Logic : This procedure is used to Fetch the Payment Detail Information on the basis of Ticket id.
Parameter: 
	@TicketID : Defines the Category for the Service Ticket .
	       
*/

ALTER PROCEDURE [dbo].[usp_hts_get_NewPaymentDetails] 
	@TicketID AS INT 
	AS      
	SELECT TP.InvoiceNumber_PK,
	       TP.paymentvoid AS voidflag,
	       TP.TicketID,
	       TP.ChargeAmount AS Paid,
	       TP.RecDate,
	       PT.Description + CASE --Fahad 6054 07/27/2009 Case added on Payemt Description
	                             WHEN LEN(ISNULL(tp.PaymentReference, '')) = 0 THEN 
	                                  ''
	                             WHEN tp.PaymentType = 104 AND LEN(ISNULL(tp.PaymentReference, ''))-- 104-Partener Firm Credit
	                                  > 0 THEN ' ['+(
	                                      SELECT TOP 1 tf.FirmAbbreviation
	                                      FROM   tblFirm tf
	                                      WHERE  tf.FirmID = tp.PaymentReference
	                                  )+']'
	                             WHEN tp.PaymentType = 11 AND LEN(ISNULL(tp.PaymentReference, ''))-- 11-Attorney Credit
	                                  > 0 THEN ' [C/O '+(
	                                      SELECT TOP 1 tu.UserName
	                                      FROM   tblusers tu
	                                      WHERE  tu.EmployeeID = tp.PaymentReference
	                                  )+']'
	                             -- Abbas Qamar 9957 10/02/2012 for Free wavied payment type description
	                             WHEN tp.PaymentType =102 AND LEN(ISNULL(tp.PaymentReference, ''))-- 11-Attorney Credit
	                                  > 0 THEN ' [C/O '+(
	                                      SELECT TOP 1 tu.UserName
	                                      FROM   tblusers tu
	                                      WHERE  tu.EmployeeID = tp.PaymentReference
	                                  )+']'     
	                                  
	                             WHEN tp.PaymentType = 9 AND LEN(ISNULL(tp.PaymentReference, '')) -- 9-Friend Credit
	                                  > 0 THEN ' ['+convert(varchar,tp.PaymentReference)+']'
	                             ELSE ''
	                        END AS Description,
	       UPPER(u.abbreviation) AS abbreviation,
	       ISNULL(tsc.transnum, '') AS transnum,
	       tsc.rowid,
	       tsc.ccnum,
	       tsc.expdate,
	       UPPER(tsc.name) AS NAME,
	       tsc.cin,
	       cct.CCType,
	       tsc.auth_code,
	       CASE 
	            WHEN tsc.response_reason_text IS NOT NULL AND ISNULL(response_subcode, 0) 
	                 = 1 THEN 'Approved'
	            WHEN tsc.response_reason_text IS NOT NULL AND ISNULL(response_subcode, 0) 
	                 <> 1 THEN 'Decline'
	            ELSE ''
	       END AS response_reason_text,
	       CASE WHEN PId.Id=0 THEN ''ELSE PId.Name END  AS 'PaymentIdentifier', -- Afaq 9398 06/20/2011 Add payment Identifier.
	       b.BranchName, -- Sabir Khan 10920 05/27/2013 display Branch Name.
	       hh.HiredVia -- Sabir Khan 11509 11/13/2013 how hired column added.
	FROM   tblTicketsPayment TP
	       INNER JOIN tblPaymenttype PT
	            ON  TP.PaymentType = PT.Paymenttype_PK
	       INNER JOIN tblusers u
	            ON  TP.employeeid = u.employeeid
	       INNER JOIN paymentIdentifier PId
				ON tp.paymentIdentifier=PId.Id
		   INNER JOIN Branch b 
				ON b.BranchId=TP.BranchID
	       LEFT OUTER JOIN tblticketscybercash tsc
	            ON  TP.invoicenumber_pk = tsc.invnumber
	       LEFT OUTER JOIN dbo.tblCCType cct
	            ON  tsc.cardtype = cct.CCTypeID
	       LEFT OUTER JOIN HowHiredClientDetail hhd 
				ON hhd.TicketID = tp.TicketID AND hhd.InvoiceNumber_fk = tp.InvoiceNumber_PK
		   LEFT OUTER JOIN HowHired hh 
				ON hh.ID = hhd.HowHiredID
	WHERE  (TP.TicketID = @TicketID)
	       AND TP.paymenttype <> 99
	ORDER BY
	       tp.recdate DESC 
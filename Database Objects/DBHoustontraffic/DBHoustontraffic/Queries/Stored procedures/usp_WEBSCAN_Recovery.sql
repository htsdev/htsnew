SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_WEBSCAN_Recovery]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_WEBSCAN_Recovery]
GO

CREATE Procedure usp_WEBSCAN_Recovery  
  
as  
  
--SELECT    distinct    O.CauseNo, P.BatchID, P.ID AS picid  
--FROM         dbo.tblTicketsViolations AS TV INNER JOIN  
--                      dbo.tbl_WebScan_OCR AS O ON TV.casenumassignedbycourt = O.CauseNo INNER JOIN  
--                      dbo.tbl_WebScan_Pic AS P ON O.PicID = P.ID INNER JOIN  
--                      dbo.tblTickets ON TV.TicketID_PK = dbo.tblTickets.TicketID_PK  
--WHERE     (TV.ScanUpdateDate >= CONVERT(DATETIME, '2007-02-12 00:00:00', 102)) AND (O.CheckStatus = 4) AND (dbo.tblTickets.Activeflag = 1)  
--order by batchid,picid

select CauseNo, BatchID,  picid from ozbsda
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


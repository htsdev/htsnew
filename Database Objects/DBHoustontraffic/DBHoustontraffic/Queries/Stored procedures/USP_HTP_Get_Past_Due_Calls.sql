﻿/************************************************************
 * Code formatted by SoftTree SQL Assistant © v4.0.34
 * Time: 11/10/2009 3:48:50 PM
 ************************************************************/

 /**
* Business Logic : This method returns Client's information whose follow update has been passed and he/she doesn't owes money
*					and Payment status must not be Waved or defalut
		*	Column selected 
		*	TicketID_PK
		*	ClientName
		*	crt
		*	CourtDate
		*	TotalFeeCharged
		*	Owes
		*	PaymentDate
		*	PaymentFollowUpdate
		*	Remainingdays
		*	CrtType
**/

ALTER PROCEDURE [dbo].[USP_HTP_Get_Past_Due_Calls] 
	@Criteria TINYINT, --Fahad 6638 11/10/2009 @Criteria parameter added
	@validation INT,		--Sabir Khan 7497 06/24/2010 Parameter added for validation report.
	@ValidationCategory int=0 -- Saeed 7791 07/10/2010 @ValidationCategory=1 for alert & @ValidationCategory=2 for report
AS
	SELECT t.TicketID_PK,
	       t.Firstname + ' ' + t.Lastname AS ClientName,
	       (
	           SELECT TOP 1 c.shortname
	           FROM   tblticketsviolations tv,
	                  tblcourts c
	           WHERE  tv.courtid = c.courtid
	                  AND tv.ticketid_pk = t.ticketid_pk
	       ) AS crt,
	     
	               ISNULL(
	                   (
	                       SELECT MIN(tv.courtdatemain)
	                       FROM   tblticketsviolations tv
	                       WHERE  tv.ticketid_pk = t.ticketid_pk
	                              AND DATEDIFF(DAY, GETDATE(), tv.courtdatemain) 
	                                  >= 0
	                   ),
	                   (
	                       SELECT MAX(tv1.courtdate)
	                       FROM   tblticketsviolations tv1
	                       WHERE  tv1.ticketid_pk = t.ticketid_pk
	                   )
	               )
	           AS CourtDate,
	       t.TotalFeeCharged,
	       ISNULL(t.TotalFeeCharged, 0) -(
	           SELECT SUM(ISNULL(chargeamount, 0))
	           FROM   tblticketspayment p
	           WHERE  p.ticketid = t.ticketid_pk
	                  AND p.paymentvoid = 0
	                  AND p.paymenttype NOT IN (99, 100)
	       ) AS Owes,
	       --ozair 5618 05/08/2009    
	       sp.PaymentDate AS PaymentDate,
	       t.PaymentDueFollowUpDate AS PaymentFollowUpdate,
	       DATEDIFF(DAY, GETDATE(), t.PaymentDueFollowUpDate) AS Pastdays,
	       t.paymentstatus,
	       --Nasir 6049 07/10/2009 add case Type
	       (
	           SELECT ct.CaseTypeName
	           FROM   CaseType ct
	           WHERE  ct.CaseTypeId = t.CaseTypeId
	       ) AS CrtType
	FROM   dbo.tblTickets AS t
	       INNER JOIN dbo.tblSchedulePayment AS sp
	            ON  t.TicketID_PK = sp.TicketID_PK
	WHERE  --Ozair 7791 07/24/2010  where clause optimized    
	       (
	           SELECT TOP 1 tv.courtid
	           FROM   tblticketsviolations tv
	           WHERE  tv.ticketid_pk = t.ticketid_pk
	       ) IN (SELECT DISTINCT(CourtID)
	             FROM   tblcourts
	             WHERE  courtid <> 0
	                    AND InActiveFlag = 0)
	       AND (
	               ISNULL(t.TotalFeeCharged, 0) -(
	                   SELECT SUM(ISNULL(chargeamount, 0))
	                   FROM   tblticketspayment p
	                   WHERE  p.ticketid = t.ticketid_pk
	                          AND p.paymentvoid = 0
	                          AND p.paymenttype NOT IN (99, 100)
	               )
	           ) > 0
	           --Fahad 6638 11/10/2009 Case Type Filter Added
	       AND (
	               (
	                   (
	                       (@validation = 1 OR @ValidationCategory = 1) -- Saeed 7791 07/10/2010 display  if call from validation sp or validation category is 'Alert' then display records which has 'follow up date' null,past or today.
	                   )
	                   AND (
	                           (@Criteria = 0)
	                           OR ((@Criteria = 1) AND (t.CaseTypeId = 1))--Case Type Traffic
	                           OR ((@Criteria = 2) AND (t.CaseTypeId IN (2, 3)))--Sabir Khan 7497 06/24/2010 Case Type Criminal
	                           OR ((@Criteria = 4) AND (t.CaseTypeId = 4))--Case Type Family
	                           OR ((@Criteria = 5) AND (t.CaseTypeId NOT IN (1, 2, 4)))--Case Type not Traffic not Criminal and not in Family
	                       )
	               )
	               OR (
	                      (
	                          (@validation = 0 OR @ValidationCategory = 2) -- Saeed 7791 07/09/2010 display all records if @validation=0 or validation category is 'Report'
	                      )
	                      AND (
	                              (@Criteria = 0)
	                              OR ((@Criteria = 1) AND (t.CaseTypeId = 1))--Case Type Traffic
	                              OR ((@Criteria = 2) AND (t.CaseTypeId = 2))--Case Type Criminal
	                              OR ((@Criteria = 4) AND (t.CaseTypeId = 4))--Case Type Family
	                              OR ((@Criteria = 5) AND (t.CaseTypeId NOT IN (1, 2, 4)))--Case Type not Traffic not Criminal and not in Family
	                          )
	                  )
	           )
	           --ozair 5618 05/08/2009 only past follow up dates
	       AND DATEDIFF(
	               DAY,
	               ISNULL(t.PaymentDueFollowUpDate, '01/01/1900'),
	               GETDATE()
	           ) > 0
	       AND ISNULL(t.Paymentstatus, 1) NOT IN (2, 3)
	       AND (t.Activeflag = 1)
	GROUP BY
	       t.paymentstatus,
	       t.Firstname,
	       t.Lastname,
	       t.TicketID_PK,
	       t.TotalFeeCharged,
	       t.PaymentDueFollowUpDate,
	       t.CaseTypeId,
	       sp.PaymentDate
	ORDER BY
	       t.PaymentDueFollowUpDate,
	       t.TicketID_PK

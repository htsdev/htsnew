SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_LA_Get_FilesForRollback]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_LA_Get_FilesForRollback]
GO



CREATE PROCEDURE USP_HTS_LA_Get_FilesForRollback
@StartDate As datetime,
@EndDate As datetime,
@Mode As int

 AS

SELECT     dbo.tblTicketsArchive.ListDate, dbo.tblTicketsArchive.RecLoadDate
FROM         dbo.tblTicketsArchive LEFT OUTER JOIN
                      dbo.tblTicketsViolationsArchive ON dbo.tblTicketsArchive.RecordID = dbo.tblTicketsViolationsArchive.RecordID
WHERE     (dbo.tblTicketsViolationsArchive.violationstatusid = @Mode) AND (dbo.tblTicketsArchive.GroupID > 0)
GROUP BY dbo.tblTicketsArchive.ListDate, dbo.tblTicketsArchive.RecLoadDate
HAVING      (Datediff(Day,ListDate,GetDate()) <= DateDiff(Day,@StartDate, @EndDate))


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


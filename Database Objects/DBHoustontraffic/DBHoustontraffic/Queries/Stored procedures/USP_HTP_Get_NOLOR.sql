﻿

/****** 
Object:  StoredProcedure [dbo].[USP_HTP_Get_NOLOR]    Script Date: 10/15/2008 17:26:09 
// Noufil 4840 10/14/2008
Business Logic : This report display all outside case records what are in waiting status with no LOR cofirmation document.
		This report display all the records after october 1st 2008
		--Fahad 5172 11/25/2008-- 
		this report also display one week old records only for those which LOR Method is Mail or other methods 
		else display records immidialtly which have LOR Method as Fax
		
		
--Yasir Kamal 5336 12/19/2008--
   This report also display the case which has not previously had a pre-trial
  

Columns:  
	ticketid_pk,
	ticketnumber,
	CauseNumber,
	FirstName,
	LastName,
	StatusID,
	CourtDate,
	CourtRoom,
	Status,
	CourtTime


******/


alter PROCEDURE [dbo].[USP_HTP_Get_NOLOR]
--Muhammad Nadir Siddiqui 9134 04/29/2011 added showall parameter.
@showAll BIT =0
AS
	SELECT DISTINCT
	       t.ticketid_pk AS ticketid_pk,
	       ISNULL(tv.refcasenumber, '') AS ticketnumber,
	       ISNULL(tv.casenumassignedbycourt, '') AS CauseNumber,
	       t.firstname AS FirstName,
	       t.Lastname AS LastName,
	       tv.CourtViolationStatusIDmain AS StatusID,
	       CONVERT(VARCHAR(20), tv.CourtDateMain, 101) AS CourtDate,
	       ISNULL(tv.CourtNumbermain, '0') AS CourtRoom,
	       cvs.ShortDescription AS STATUS,
	       dbo.Fn_FormateTime(tv.CourtDateMain) AS CourtTime,
		   --Muhammad Nadir Siddiqui 9134 04/28/2011 added No LOR confirmation followup date and courtid.  
		   CASE CONVERT(VARCHAR, ISNULL(t.NoLORConfirmFollowUpDate, ' '), 101)
	            WHEN '01/01/1900' THEN ' '
	            ELSE CONVERT(VARCHAR, t.NoLORConfirmFollowUpDate, 101)
	       END AS NoLORConfirmFollowUpDate,
		   c.CourtID
	FROM   tblTickets t
	       INNER JOIN tblTicketsViolations tv
	            ON  t.ticketid_pk = tv.ticketid_pk
	       INNER JOIN tblCourts c
	            ON  tv.Courtid = c.CourtID
	       LEFT OUTER JOIN tblTicketsViolationLog vl
	            ON  tv.TicketsViolationID = vl.TicketviolationID
	       INNER JOIN dbo.tblCourtViolationStatus AS cvs
	            ON  tv.CourtViolationStatusIDmain = cvs.CourtViolationStatusID
	WHERE  
			
		   --Ozair 8005 07/23/2010 where clause modified to optmize query
	       --Fahad 5172 11/25/2008 included one week old records check along with LOR method type--
	       (
	           (
	               --Sabir Khan 5728 03/31/2009 Include cases which are pending from last 5 business days...
	               t.ticketid_pk IN (SELECT TOP 1 ticketid_pk
	                                 FROM   tblLORBatchHistory LBH
	                                        INNER JOIN dbo.tblHTSBatchPrintLetter 
	                                             BPL
	                                             ON  LBH.BatchID_FK = BPL.BatchID_PK
	                                 WHERE  BPL.USPSResponse IS NULL
	                                        AND BPL.EDeliveryFlag = 0
	                                        AND bpl.TicketID_FK = t.TicketID_PK
	                                        AND BPL.IsPrinted = 1
	                                        AND DATEDIFF(
	                                                DAY,
	                                                dbo.fn_getnextbusinessday(BPL.PrintDate, 5),
	                                                GETDATE()
	                                            ) >= 0
	                                 ORDER BY
	                                        bpl.PrintDate DESC) 
	               --datediff(day,(select top 1 tvl.recdate from tblTicketsViolationLog tvl where tvl.TicketviolationID=vl.TicketviolationID),getdate())>7
	               AND c.lor <> 0
	           )
	           OR (c.lor = 0)
	       )
	       --Fahad 5172 11/20/2008 included isdeleted check--
	       AND (
	               SELECT COUNT(DocTypeID)
	               FROM   tblScanBook
	               WHERE  TicketID = t.TicketID_PK
	                      AND doctypeid = 18
	                      AND isdeleted = 0
	           ) = 0
	           --Yasir Kamal 5336 12/19/2008 check if the case has not previously had a pre-trial--
	       AND tv.TicketsViolationID NOT IN (SELECT TicketviolationID
	                                         FROM   tblTicketsViolationLog
	                                         WHERE  newverifiedstatus = 101
	                                                AND ticketviolationid = tv.ticketsviolationid)
	       AND (DATEDIFF(DAY, '10/1/2008', vl.recdate) > 0)
	           --only clients
	       AND t.activeflag = 1
	           -- Noufil 06/24/2010 7942 Allow bond status to show in report.
	       AND cvs.courtviolationstatusid IN (104, 135) 
	           --only HCJP Courts
	       AND c.courtid BETWEEN 3007 AND 3022
		   --Muhammad Nadir Siddiqui 9134 04/29/2011 added show all
		   AND (
	               (@showall = 1)
	               OR ( @showall = 0 AND DATEDIFF(DAY, GETDATE(), ISNULL(t.NoLORConfirmFollowUpDate, '1/1/1900')) <= 0)
	           )







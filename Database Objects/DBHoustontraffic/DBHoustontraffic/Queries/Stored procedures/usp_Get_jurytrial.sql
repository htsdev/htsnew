
-- =============================================
-- Author: Asghar
-- Create date: 02/03/2007
-- Business Logic: This stored procedure is used to get trial cases information for tried cases report.
-- Input Parameters: 
-- @stadate: start date selected in date selection criteria.
-- @enddate : end date selected in date selection criteria.
-- @attorney : id associated with attorney. 
-- @court : id associated with court.
-- =============================================

---   usp_Get_jurytrial '01/02/2007','05/03/2007','4005','3003'
ALTER  PROCEDURE [dbo].[usp_Get_jurytrial] 
	
@stdate		datetime,
@enddate	datetime,
@attorney	int,
@Court	int

AS

select @enddate =@enddate +'23:59:59.000'

BEGIN

if(@attorney != 0 and @Court != 0)
 begin
		select	rowid,
				[date] as recdate,
				(select lastname+' '+firstname from tblusers where employeeid=attorney)as attorney,
				verdict,
				caseno,
				abs(fine) as fine,
				brieffacts 
		from tbljurytrial
		--Yasir Kamal 6279 08/05/2009 use datediff instead of between.
		--Farrukh 11233 06/25/2013 Changed lookup date from "Recdate" to "[date]"
		where	datediff(day,[date],@stdate) <=0 and datediff(day,[date],@enddate) >=0			
		and		attorney = @attorney
		and		Courtid	=	@court

end
	if(@attorney =0  and @Court != 0)
	 begin
		
		select	rowid,
				[date] as recdate,
				(select lastname+' '+firstname from tblusers where employeeid=attorney)as attorney,
				verdict,
				caseno,
			abs(fine) as fine,
				brieffacts 
		from tbljurytrial
		--Yasir Kamal 08/05/2009 use datediff instead of between.
		--Farrukh 11233 06/25/2013 Changed lookup date from "Recdate" to "[date]"
		where	datediff(day,[date],@stdate) <=0 and datediff(day,[date],@enddate) >=0			
		and		Courtid	=	@court
     end

	if(@attorney !=0  and @Court = 0)
	 begin
	
		select	rowid,
				[date] as recdate,
				(select lastname+' '+firstname from tblusers where employeeid=attorney)as attorney,
				verdict,
				caseno,
				abs(fine) as fine,
				brieffacts 
		from tbljurytrial
		--Yasir Kamal 08/05/2009 use datediff instead of between.
		--Farrukh 11233 06/25/2013 Changed lookup date from "Recdate" to "[date]"
		where	datediff(day,[date],@stdate) <=0 and datediff(day,[date],@enddate) >=0			
		and		attorney = @attorney
     end

	
	else
	 begin
		
			select	rowid,
					[date] as recdate,
					(select lastname+' '+firstname from tblusers where employeeid=attorney)as attorney,
					verdict,
					caseno,
					abs(fine) as fine,
					brieffacts 
			from tbljurytrial
			--Yasir Kamal 08/05/2009 use datediff instead of between.
			--Farrukh 11233 06/25/2013 Changed lookup date from "Recdate" to "[date]"
			where	datediff(day,[date],@stdate) <=0 and datediff(day,[date],@enddate) >=0				
	end


END




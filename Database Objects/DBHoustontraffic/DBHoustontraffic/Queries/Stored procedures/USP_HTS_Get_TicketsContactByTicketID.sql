SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Get_TicketsContactByTicketID]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Get_TicketsContactByTicketID]
GO

  
CREATE PROCEDURE [dbo].[USP_HTS_Get_TicketsContactByTicketID]             
@TicketID_PK     int          
          
AS            
            
declare @temp table(          
 phno varchar(25),    
 contacttype varchar(15)          
 )          
          
insert into @temp           
Select Contact1,c.description    
from tbltickets t    
inner join     
tblcontactstype as c    
on t.ContactType1 = c.ContactType_PK             
Where t.TicketID_PK = @TicketID_PK            
          
insert into @temp           
Select Contact2,c.description    
from tbltickets t    
inner join     
tblcontactstype as c    
on t.ContactType2 = c.ContactType_PK             
Where t.TicketID_PK = @TicketID_PK             
          
insert into @temp           
Select Contact3,c.description    
from tbltickets t    
inner join     
tblcontactstype as c    
on t.ContactType3 = c.ContactType_PK             
Where t.TicketID_PK = @TicketID_PK          
          
select * from @temp   

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


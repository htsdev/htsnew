﻿ /*  
* Created By		: SAEED AHMED
* Task ID			: 7791  
* Created Date		: 05/29/2010  
* Business logic	: This procedure is used get Alert summary report.
*					: 
* Parameter List  
* @ReportIds		: Represents comma seperated report ids.
*
*/
CREATE PROC USP_HTS_GetReportsummary 
@ReportIds VARCHAR(100)
AS 

BEGIN
DECLARE @isScheduleParam VARCHAR(5)
DECLARE @ThisResultalertsummary VARCHAR(MAX)
declare @ThisPendingCount varchar(5)    
declare @ThisAlertCount varchar(5)    
declare @ThisTotalCount varchar(5)   


SET @isScheduleParam ='1,1'
 execute USP_HTP_Generics_Get_XMLSet_For_Validation     
    'traffictickets.dbo.USP_HTP_GetSummaryReport',     
    @isScheduleParam,     
    'Alert Summary',  
    '[Report Name],[Alert] as Due,[Pending]' ,-- Noufil 5900 06/09/2009 Remove * and use column name to show only that column at report  
    0,'',1,@ResultSet = @ThisResultalertsummary output, @PendingCount = @ThisPendingCount OutPut,@AlertCount =@ThisAlertCount output  ,@TotalCount=@ThisTotalCount output    	
    
SELECT  @ThisResultalertsummary
END


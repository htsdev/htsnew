/****** 
Business Logic:	The procedure is used by HTP to get number of cases having status in bond waiting.
List of Parameters:
	@Validation:	Used for bond waiting alert under validation email.
	@ShowAll:		Used for displaying specific follow up date cases or all follow up date cases having status in bond waiting.

 
******/


-- Noufil 4432 08/05/2008 html serial number added added for validation report
-- Noufil 4980 10/31/2008 bondwaitingfollowupdate column added and bond flag case edit
--USP_HTS_Get_BondWaitingViolations 1,1
ALTER PROCEDURE [dbo].[USP_HTS_Get_BondWaitingViolations] 
--Yasir 5423 02/11/2009 cases having follow up date in next 6 business days
	@Validation INT = 0,
	@ShowAll INT = 0,
	@ValidationCategory INT = 0 -- Saeed 7791 07/09/2010 @ValidationCategory=1 for alert & @ValidationCategory=2 for report
AS
	DECLARE @bd        INT,
	        @idx       INT,
	        @tempdate  DATETIME
	
	SET @bd = ABS(
	        (
	            SELECT [dbo].[fn_GetReportValue] ('days', 93)
	        )
	    )
	
	SET @idx = 0
	WHILE @idx <= @bd
	BEGIN
	    --Yasir Kamal 5734 03/31/2009 followUpdate logic set for 2 business days
	    IF DATEPART(dw, DATEADD(DAY, @idx, GETDATE())) IN (1, 7)
	        SET @bd = @bd + 1
	    
	    SET @idx = @idx + 1
	END
	
	SET @tempdate = DATEADD(DAY, @bd, GETDATE())
	
	--5423 end
	
	SELECT dbo.fn_DateFormat(BondWaitingDate, 25, '/', ':', 1) AS BondDate,
	       DATEDIFF(dd, BondWaitingDate, GETDATE()) AS DaysOver,
	       *
	FROM   (
	           SELECT (
	                      CASE 
	                           WHEN (ISNULL(tblTicketsViolations.BondWaitingdate, 0) <> 0) THEN 
	                                tblTicketsViolations.BondWaitingdate
	                           ELSE (
	                                    SELECT MAX(RecDate)
	                                    FROM   tblticketsviolationlog
	                                    WHERE  tblticketsViolations.ticketsviolationid = 
	                                           tblticketsviolationlog.ticketviolationid
	                                           AND newverifiedstatus = 202
	                                           AND newverifiedstatus <>
	                                               oldverifiedstatus
	                                )
	                      END
	                  ) AS BondWaitingDate,
	                  BondFlag = CASE 
	                                  WHEN BondFlag = 1 THEN 'B'
	                                  ELSE ' '
	                             END,
	                  tblticketsViolations.RefCaseNumber,
	                  tblticketsViolations.casenumassignedbycourt AS causenumber,
	                  tblTickets.LastName,
	                  tblTickets.FirstName,
	                  tblTickets.ticketid_pk,
	                  tblticketsViolations.Courtnumber,
	                  tblCourts.ShortName,
	                  AStatus.ShortDescription AS AutoStatus,
	                  dbo.fn_DateFormat(tblticketsViolations.Courtdate, 25, '/', ':', 1) AS 
	                  CourtDate,
	                  VStatus.ShortDescription AS VerifiedCourtStatus,
	                  dbo.fn_DateFormat(tblticketsViolations.CourtDateMain, 25, '/', ':', 1) AS 
	                  VerifiedCourtDate,
	                  tblTickets.Criminalfollowupdate AS followupdate,	--convert(varchar,isnull(tblTickets.bondwaitingFollowUpdate,' '),101) as bondwaitingfollow,
	                  CASE CONVERT(
	                           VARCHAR,
	                           ISNULL(tblTickets.bondwaitingFollowUpdate, ' '),
	                           101
	                       )
	                       WHEN '01/01/1900' THEN ' '
	                       ELSE CONVERT(VARCHAR, tblTickets.bondwaitingFollowUpdate, 101)
	                  END AS bondwaitingfollow,
	                  tblTickets.bondwaitingFollowUpdate
	           FROM   tblticketsViolations,
	                  tblTickets,
	                  tblCourts,
	                  tblCourtViolationStatus AS AStatus,
	                  tblCourtViolationStatus AS VStatus
	           WHERE  --Ozair 7791 07/24/2010  where clause optimized  
	                  tblTicketsViolations.CourtViolationStatusID = AStatus.CourtViolationStatusID
	                  AND tblTicketsViolations.CourtViolationStatusIDmain = 
	                      VStatus.CourtViolationStatusID 
	                      ---SAbir Khan 7607 03/24/2010 No need to check auto court date in past...
	                      --AND DATEDIFF(dd, tblticketsviolations.courtdate, getdate()) > 0 and
	                  AND tblticketsviolations.CourtViolationStatusIDMain = 202
	                  AND tblTicketsViolations.CourtId = tblCourts.CourtId
	                  AND tblTicketsViolations.TicketID_PK = tblTickets.TicketID_PK
	       ) AS main
	       
	       -- tahir 5165 11/18/2008 to show all bond waiting cases
	       --where  ((@Validation=1) or (@Validation=0 and datediff(day, dbo.fn_getnextbusinessday(BondWaitingDate,5), getdate()) > 0))
	       --and ((@Validation=0) or (@Validation=1 and datediff(day,getdate(), isnull(bondwaitingfollow,'')) <= 0))
	WHERE  --Yasir 5423 02/11/2009 cases having follow up date in next 6 business days
	       
	       (
	           (@Validation = 0 OR @ValidationCategory = 2) -- Saeed 7791 07/09/2010 display all records if @validation=0 or validation category is 'Report'
	           OR (
	                  (@Validation = 1 OR @ValidationCategory = 1)
	                  AND DATEDIFF(DAY, GETDATE(), ISNULL(bondwaitingfollow, '')) 
	                      <= 0
	              ) -- Saeed 7791 07/10/2010 display  if call from validation sp or validation category is 'Alert' then display records which has 'follow up date' null,past or today.
	       ) 
	       --Yasir Kamal 5734 03/31/2009 default records logic changed.
	       AND (
	               (@ShowAll = 1 OR @ValidationCategory = 2) -- Saeed 7791 07/10/2010 display all records if showAll=1 or validation category is 'Report'
	               OR (
	                      (@ShowAll = 0 OR @ValidationCategory = 1)
	                      AND DATEDIFF(DAY, GETDATE(), ISNULL(bondwaitingfollow, BondWaitingDate)) 
	                          <= @bd
	                  ) -- Saeed 7791 07/10/2010 display  if showAll=0 or validation category is 'Alert' then display records which has 'follow up date' null,past or today.
	           ) 
	           --Sabir Khan 5297 12/03/2008 sort by follow update...
	ORDER BY
	       bondwaitingFollowUpdate,
	       ticketid_pk --Sabir Khan 03/26/2010 ticketid_pk has been added in order by for fixing serial no bug...
	       






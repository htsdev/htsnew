SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_PCS_GetCaseInfo]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_PCS_GetCaseInfo]
GO

      
CREATE Procedure         
--        [USP_PCS_GetCaseInfo] 184666          
                  
 [dbo].[USP_PCS_GetCaseInfo] -- 124229                              
@ticketid int                                                                                      
                                                                                      
as                                                                                      
  
  
declare @ContinuanceAmount as money   
--Calculate Continuance Amount   
Set @ContinuanceAmount = isnull (( Select Count(*) from tblticketsflag where ticketid_pk= @ticketid and flagid = 9 ) * (Select top 1  Amount from tblspeeding where Description = 'Continuance'),0)  
  
  
  
  
SELECT         
    
 ISNULL(T.AccidentFlag,0) AS AccidentFlag,                                                           
 ISNULL(T.CDLFlag, 0) AS CDLFlag,                                                           
                                                          
 ISNULL(Convert(varchar,T.Adjustment), 0) AS FinalAdjustment,                                                           
  ( Select Count(*) from tblticketsviolations where TicketID_PK = t.ticketid_pk) AS ViolationCount,                                                           
 ISNULL(T.LockFlag, 0) AS LockFlag,                                                           
 isnull(T.SpeedingID,0) as SpeedingID,                                                           
 ISNULL(T.InitialAdjustment,0) as InitialAdjustment,                           
 case    
  when isnull(t.lateflag,0) = 0 then 0     
  else isnull( (Select top 1 Amount from tblspeeding  where Description = 'Late Fee') ,0) end as LateFee,              
 isnull(@ContinuanceAmount,0) as ContinuanceAmount      
                                                         
FROM     tblTickets T       
where                                                    
T.TicketID_PK = @ticketid    
    
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HMC_Get_pleaguily_Report]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HMC_Get_pleaguily_Report]
GO



CREATE procedure [dbo].[USP_HMC_Get_pleaguily_Report]    
    
@Empid int  ,  
@RecordIDs varchar(200)  
as     
  
declare @Records  table(ID int)          
insert into @Records select * from dbo.sap_string_param(@RecordIDs)     
  
SELECT     ta.FirstName, ta.LastName, ta.MidNumber, ta.DOB, ef.Flag, tva.CauseNumber, tva.TicketNumber_PK, tva.ViolationNumber_PK, tva.RecordID,     
                      tva.ViolationDescription, ta.Initial    
    
FROM         dbo.tblTicketsViolationsArchive AS tva INNER JOIN    
                      dbo.tblTicketsArchive AS ta ON tva.RecordID = ta.RecordID INNER JOIN    
                      dbo.tbl_HMC_EFile AS ef ON tva.TicketNumber_PK = ef.TicketNumber_Fk AND tva.ViolationNumber_PK = ef.ViolationNumber_FK    
WHERE     
 ef.recordid in ( select id from @Records)  
/*AND (ef.Flag = 0) */  AND (ef.Empid = @Empid)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


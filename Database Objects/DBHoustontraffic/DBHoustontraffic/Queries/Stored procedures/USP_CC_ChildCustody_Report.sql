set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


/*************************************************
Created By:Zeeshan Ahmed

List of parameters:
-------------------

@courtid: represents courtid of the court
@settingtype: represnts setting type
@casestatus:represents status of the case
@DocketDateFrom:represnts the start range of Docket Date
@DocketDateTo: represnts End range of Docket Date
@LoadDateFrom: represents Load Date starting range
@LoadDateTo: represents Load Date Ending range 

**************************************************/
--USP_CC_ChildCustody_Report 0,0,0,'04/28/2008','04/28/2008','',''

create Procedure [dbo].[USP_CC_ChildCustody_Report]      
(      
      
 @CourtId int,      
 @SettingType int,      
 @CaseStatus int,      
 @DocketDateFrom  varchar(12)='', 
 @DocketDateTo  varchar(12)='', 
 @LoadDateFrom  varchar(12)='',  
 @LoadDateTo  varchar(12)=''    
       
)      
as      
      
      
declare @Query  varchar(8000)                      
                      
                      
Set @Query = '        
 


Select       
CC.ChildCustodyID,   
CC.Style ,       
 CC.CaseNumber ,      
 CST.SettingTypeName as SettingType ,       
 CCS.CaseStatusName as CaseStatus  ,      
 Right( Convert(varchar,ChildCustodyCourtId ),1) as CourtRoom,      
 CC.NextSettingDate,      
 CC.RESULT,      
 isnull(RECLOADDATE,''1/1/1900'') as RECDATE       
      
from       
      
ChildCustody CC      
      
Join      
ChildCustodySettingType CST       
 ON CC.ChildCustodySettingTypeID = CST.ChildCustodySettingTypeID      
Join       
ChildCustodyCaseStatus CCS      
 On CC.ChildCustodyCaseStatusId = CCS.ChildCustodyCaseStatusId      
      
Where   1 = 1   '


if ( @CourtId != 0 )
Set @Query =  @Query + ' and  CC.ChildCustodyCourtID  = ' + Convert(varchar,@CourtId)     

if ( @CaseStatus != 0 )
Set @Query =  @Query + ' and CC.ChildCustodyCaseStatusID  = ' + Convert(varchar,@CaseStatus)  

if ( @SettingType != 0 )
Set @Query =  @Query + ' and CC.ChildCustodySettingTypeID  = ' + Convert(varchar,@SettingType)  

if ( @CaseStatus != 0 )
Set @Query =  @Query + ' and CC.ChildCustodyCaseStatusID  = ' + Convert(varchar,@CaseStatus)     


if @DocketDateFrom !='' and @DocketDateTo !=''                      
 begin  
	 Set @Query = @Query  + ' and datediff(day, SettingDate ,  '''+@DocketDateFrom+''' ) <= 0 '                      
	Set @Query = @Query  + ' and datediff(day, SettingDate ,  '''+@DocketDateTo+''' ) >= 0'                      
 end 


if @LoadDateFrom !='' and @LoadDateTo !=''                      
 begin  
	 Set @Query = @Query  + ' and datediff(day, RecLoadDate ,  '''+@LoadDateFrom+''' ) <= 0 '                      
	Set @Query = @Query  + ' and datediff(day, RecLoadDate ,  '''+@LoadDateTo+''' ) >= 0'                      
 end 

exec (@Query)

go


grant execute on [USP_CC_ChildCustody_Report] to [dbr_webuser]
go

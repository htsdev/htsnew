SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_HTS_GetReminderCalls]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_HTS_GetReminderCalls]
GO




















--dbo.usp_HTS_GetReminderCalls '11/09/2005', '11/09/2005', 2
CREATE           procedure dbo.usp_HTS_GetReminderCalls 
	(
	@FromDate	datetime,
	@ToDate		datetime,
	@ClientType	int		--  '0' = QUOTE
	)				--  '1' = CLIENT
as					--  '2' = ALL


--select @fromdate = @fromdate + '00:00:00'
--select @todate   = @todate   + '23:59:59'


create table #temp
	(
	ticketid	int,
	clientname	varchar(100),
	courtdate	varchar(25),
	casestatus	varchar(50),
	courtaddress	varchar(100),
	contact		varchar(200),
	language	varchar(50),
	comments	varchar(500)
	)

-- IF 'QUOTE' OR 'CLIENT' IS SELECTED IN CLIENT TYPE CRITERIA..........
if @ClientType <> 2
	begin
		insert into #temp (ticketid, clientname, courtdate, casestatus, courtaddress, contact, language, comments )
		SELECT	distinct
			t.ticketid_pk as ticketid,
			t.Lastname+', '+t.Firstname as clientname, 
			convert(varchar(20),t.currentdateset) as CourtDate ,
			(select description from tbldatetype 
			where typeid = t.datetypeflag
			) as casestatus, 
			c.Address as courtaddress, 
			dbo.fn_Get_Contacts(t.ticketid_pk) as Contact,
			isnull(t.LanguageSpeak,'-NA-') as language,
			isnull(t.generalcomments,'-NA-') as comments

			FROM	dbo.tblCourts c 
			INNER JOIN
				dbo.tblTickets t 
			INNER JOIN
				dbo.tblTicketsViolations v 
			ON	t.TicketID_PK = v.TicketID_PK 
			ON 	c.Courtid = t.CurrentCourtloc 
			LEFT OUTER JOIN
				dbo.tblDateType dt 
			INNER JOIN
				dbo.tblCourtViolationStatus cvs
			ON 	dt.TypeID = cvs.CategoryID 
			ON 	v.CourtViolationStatusIDmain = cvs.CourtViolationStatusID		
		
	
		
					-- FILTERING RECORDS AS PER REQUIREMENT......
	
		-- RECORDS RELATED TO INSIDE COURTS ONLY....
		where	c.courtid in (3001,3002,3003)     
		
		-- ONLY F TICKETS RELATED RECORDS.....
		and	upper(v.refcasenumber) like 'F%'  

		--CASE STATUS NULL , ARRAIGNMENT, PRETRIAL, TRIAL OR JUDGE TRIAL....
		and	( dt.typeid is null or dt.typeid in (2,3,4,5) )

		-- RECORDS THAT ARE RELATED TO GIVEN DATE RANGE.....
--		and 	datediff(day,v.recdate,getdate()) <= datediff(day,@fromdate,@todate)
		and	convert(varchar(10),v.recdate,101) between @fromdate and @todate
		
		-- RECORDS HAVING RECDATE LESS THAN THE F TICKETS RECDATE
		and	v.ticketid_pk in (
				select 	b.ticketid_pk 
				from	tblticketsviolations b
				where	v.ticketid_pk = b.ticketid_pk
				and	b.refcasenumber not like 'F%'
				and	v.recdate > b.recdate
				)
		
		-- CASES THAT MARKED AS "QUOTE" OR "CLIENT"....
		and	t.activeflag = @clienttype
	end

-- IF 'ALL' IS SELECTED IN CLIENT TYPE CRITERIA..........
else
	begin
		insert into #temp (ticketid, clientname, courtdate, casestatus, courtaddress, contact, language, comments )
		SELECT	distinct
			t.ticketid_pk as ticketid,
			t.Lastname+', '+t.Firstname as clientname, 
			convert(varchar(20),t.currentdateset) as CourtDate ,
--			isnull(dt.Description,'-NA-') as casestatus, 
			(select description from tbldatetype 
			where typeid = t.datetypeflag
			) as casestatus, 
			c.Address as courtaddress, 
			dbo.fn_Get_Contacts(t.ticketid_pk) as Contact,
			isnull(t.LanguageSpeak,'NA') as language,
			isnull(t.generalcomments,'NA') as comments

			FROM	dbo.tblCourts c 
			INNER JOIN
				dbo.tblTickets t 
			INNER JOIN
				dbo.tblTicketsViolations v 
			ON	t.TicketID_PK = v.TicketID_PK 
			ON 	c.Courtid = t.CurrentCourtloc 
			LEFT OUTER JOIN
				dbo.tblDateType dt 
			INNER JOIN
				dbo.tblCourtViolationStatus cvs
			ON 	dt.TypeID = cvs.CategoryID 
			ON 	v.CourtViolationStatusIDmain = cvs.CourtViolationStatusID		
		
		
					-- FILTERING RECORDS AS PER REQUIREMENT......
	
		-- RECORDS RELATED TO INSIDE COURTS ONLY....
		where	c.courtid in (3001,3002,3003)     
		
		-- ONLY F TICKETS RELATED RECORDS.....
		and	upper(v.refcasenumber) like 'F%'  

		--CASE STATUS NULL , ARRAIGNMENT, PRETRIAL, TRIAL OR JUDGE TRIAL....
		and	( dt.typeid is null or dt.typeid in (2,3,4,5) )

		-- RECORDS THAT ARE RELATED TO GIVEN DATE RANGE.....
--		and 	datediff(day,v.recdate,getdate()) <= datediff(day,@fromdate,@todate)
		and	convert(varchar(10),v.recdate,101) between @fromdate and @todate

		
		-- RECORDS HAVING RECDATE LESS THAN THE F TICKETS RECDATE
		and	v.ticketid_pk in (
				select 	b.ticketid_pk 
				from	tblticketsviolations b
				where	v.ticketid_pk = b.ticketid_pk
				and	b.refcasenumber not like 'F%'
				and	v.recdate > b.recdate
				)


	end
	


select * from #temp
where	( contact is not null and len(contact) > 5 )

drop table #temp


















GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


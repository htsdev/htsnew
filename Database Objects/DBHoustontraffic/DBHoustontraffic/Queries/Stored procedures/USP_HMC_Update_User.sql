SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HMC_Update_User]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HMC_Update_User]
GO



CREATE Procedure [dbo].[USP_HMC_Update_User]        
 @Uid varchar(20),      
 @AccountType int,      
 @Status int,      
 @FName varchar(25),      
 @LName varchar(25),      
 @Email varchar(50),      
 @Description varchar(200),
 @Firm varchar(50)
         
as      
         
 update tbl_HMC_Users   
 set  
  AccountType = @AccountType,  
  Status = @Status,  
  Fname = @FName,   
  Lname = @LName,   
  Email = @Email,   
  Description = @Description,    
  Firm = @Firm
 where uid = @uid      


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


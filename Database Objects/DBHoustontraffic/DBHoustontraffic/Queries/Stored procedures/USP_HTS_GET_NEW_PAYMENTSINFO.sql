﻿/**
* Business Logic : This method returns payment's information for client's and (nonclient's or quotes) base on ticket id
* Parameters : @ticketID
**/
              
  --[USP_HTS_GET_NEW_PAYMENTSINFO] 17844    
ALTER Procedure [dbo].[USP_HTS_GET_NEW_PAYMENTSINFO]                
        (                                      
 @ticketID int                                              
 )                                              
as                                              
                                    
declare @status as int                                    
                          
select @status= activeflag from tbltickets where ticketid_pk=@ticketID                                    
                                    
if (@status=1)    --if activeflag ==1 i.e client                                
 begin                                             
 SELECT                                         
  T.TicketID_PK,                                    
                  
  ISNULL(SUM(DISTINCT T.totalfeecharged), 0) AS TotalFee,                                    
  ISNULL(SUM(TP.ChargeAmount), 0) AS Paid,                                       
--ISNULL(SUM(DISTINCT T.calculatedtotalfee) - SUM(TP.ChargeAmount), 0) AS Owes,           REMARKED TO GET LOCK AMOUNT...                          
  ISNULL(SUM(DISTINCT T.totalfeecharged) - isnull (SUM(TP.ChargeAmount),0), 0) AS Owes,                                    
/*                  
  ISNULL(SUM( isnull(T.totalfeecharged,0) ), 0) AS TotalFee,                                    
  ISNULL(SUM( isnull(TP.ChargeAmount,0)), 0) AS Paid,                                       
  ISNULL(SUM( isnull(T.totalfeecharged,0) ) - isnull (SUM( isnull(TP.ChargeAmount,0) ),0), 0) AS Owes,                                    
*/                
 --Agha Usman 2664 06/04/2008  
  '' AS PayComments,                        
  '0' as CertifiedMailNumber,                                       
  T.Activeflag,                        
  '0' Pretrialstatus,          
      
/*  (select count(tv.ticketid_pk) from tblticketsviolations tv                
 left outer join tblcourtviolationstatus cvs on                 
 tv.courtviolationstatusidmain=cvs.courtviolationstatusid                
 where (cvs.violationcategory in(0,2) and isnull(statustype,0) <>1)                   
  and tv.ticketid_pk=@ticketID                
 ) as toProcess                
*/                
(select count(v.ticketid_pk)              
FROM    dbo.tblCourtViolationStatus AS c INNER JOIN              
  dbo.tblDateType AS d ON c.CategoryID = d.TypeID RIGHT OUTER JOIN              
  dbo.tblTicketsViolations AS v ON c.CourtViolationStatusID = v.CourtViolationStatusIDmain              
where v.ticketid_pk = @ticketID              
and  isnull(d.isactive,0) = 0              
) as toprocess      ,        
       
(select count(v.ticketid_pk) from  tblTicketsViolations v where       
       
      
ticketid_pk=@ticketID and courtviolationstatusidmain in (186)) as status,--added by azwar for restrict Payment when case is in FTA        
   
(select WalkInClientFlag from tbltickets where ticketid_pk=@ticketID) as  WalkInClientFlag,
-- Noufil 5618 04/02/2009 PaymentFollowUpdate column has been added
CONVERT(VARCHAR(20),T.PaymentDueFollowUpDate,101) AS PaymentFollowUpdate
              
FROM dbo.tblTicketsPayment TP                   
right OUTER JOIN                                      
 dbo.tblTickets T                   
ON  TP.TicketID = T.TicketID_PK                   
AND  (TP.PaymentVoid <> 1)                    
and  TP.paymenttype <>99                 
                
WHERE (T.TicketID_PK = @ticketID)  --not to include balance due amount                   
GROUP BY                   
--Agha Usman 2664 06/04/2008
 T.TicketID_PK, T.Activeflag,T.PaymentDueFollowUpDate
--T.TicketID_PK, PymtComments, CertifiedMailNumber, T.Activeflag, Pretrialstatus                                  
end                                    
                                    
else          --if quote or non-client                          
 begin                                     
 SELECT                                         
  T.TicketID_PK,                                    
  ISNULL(T.totalfeecharged, 0) AS TotalFee,                                     
  '' AS PayComments,          
  '0' as CertifiedMailNumber,                                       
  T.Activeflag,                        
  '0' as Pretrialstatus,          
     
 (select count(v.ticketid_pk) from  tblTicketsViolations v where       
       
      
ticketid_pk=@ticketID and courtviolationstatusidmain in (186)) as status,--added by azwar for restrict Payment when case is in FTA        
        
/*  (select count(tv.ticketid_pk) from tblticketsviolations tv                
 left outer join tblcourtviolationstatus cvs on                 
 tv.courtviolationstatusidmain=cvs.courtviolationstatusid                
 where (cvs.violationcategory in(0,2) and isnull(statustype,0) <>1)                  
  and tv.ticketid_pk=@ticketID                
 ) as toProcess                                    
*/              
(select count(v.ticketid_pk)              
FROM    dbo.tblCourtViolationStatus AS c INNER JOIN              
  dbo.tblDateType AS d ON c.CategoryID = d.TypeID RIGHT OUTER JOIN              
  dbo.tblTicketsViolations AS v ON c.CourtViolationStatusID = v.CourtViolationStatusIDmain              
where v.ticketid_pk = @ticketID              
and  isnull(d.isactive,0) = 0              
) as toprocess  ,  
  
(select WalkInClientFlag from tbltickets where ticketid_pk=@ticketID) as  WalkInClientFlag,
-- Noufil 5618 04/02/2009 PaymentFollowUpdate column has been added
CONVERT(VARCHAR(20),T.PaymentDueFollowUpDate,101) AS PaymentFollowUpdate  
    
    
              
 FROM                                     
                       dbo.tblTickets T                                                            
 WHERE     (T.TicketID_PK = @ticketID)                                       
 end                        
                    
                    
                    
                    
                  
                  
                  
                  
                  
          
----------------------------------------------------------------------------------------------     
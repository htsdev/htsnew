ALTER procedure [dbo].[USP_HTS_UPDATE_ViolationPrices]          
 (          
 @TicketId  int,          
 @BaseFeeCharge  money,          
 @ContinuanceAmount money,          
 @InitialBaseFee  money,          
 @InitialAdjustment money,          
 @FinalAdjustment money,          
 @InitialTotalFeeCharged money,          
 @CalculatedTotalFee money,          
 @LockFlag  bit,          
 @empid  int       
 )          
as          
          
    
---------------- edited by fahad------------------------------------------------------    
--Modified By Zeeshan Ahmed On 1/4/2007  
--Donot Update Lock Flag If Case Is Criminal Case    
if  (@FinalAdjustment is not null) and (@FinalAdjustment <> '')  and (dbo.fn_check_criminal_case( @TicketId) = 0)    
begin    
 update tbltickets set lockflag=0 where ticketid_pk=@TicketId    
end    
    
----------------------------------------------------------------------    
    
    
    
declare @strFeeInitial   varchar(10),          
 @strInitialAdjustmentInintial varchar(10),          
 @CurrCalculatedTotalFee  money,          
 @strTemp1   varchar(10),          
 @strTemp2   varchar(10)          
 
          
          
select @strTemp2 = initialadjustmentinitial from tbltickets          
where ticketid_pk = @TicketId          
          
--if @SalesEmployeeId is null          
-- set @SalesEmployeeId = @empid          
          
select @strFeeinitial = abbreviation from  tblusers where employeeid = @empid          
set  @strInitialAdjustmentInintial = @strFeeInitial          
          
if not (@strTemp2 is null or len(@strTemp2) <= 0 )          
 set @strInitialAdjustmentInintial = @strTemp2          
          
set  @CalculatedTotalFee = @BaseFeeCharge + @InitialAdjustment + @FinalAdjustment + @ContinuanceAmount          
select  @CurrCalculatedTotalFee = calculatedtotalfee from tbltickets where ticketid_pk = @TicketId          
          
begin tran          
           
update tbltickets          
set BaseFeeCharge = @BaseFeeCharge,          
 ContinuanceAmount = @ContinuanceAmount,          
 InitialBaseFee = @InitialBaseFee,          
 InitialAdjustment = @InitialAdjustment,          
 adjustment = @FinalAdjustment,          
 InitialTotalFeeCharged = @CalculatedTotalFee,          
 CalculatedTotalFee = @CalculatedTotalFee,          
  employeeidupdate = @empid          
          
where ticketid_pk = @TicketId          
          
if @LockFlag = 1          
 begin          
  update tbltickets          
  set TotalFeeCharged = @CalculatedTotalFee,          
   InitialTotalFeeCharged = @CalculatedTotalFee,          
   CalculatedTotalFee = @CalculatedTotalFee,          
   lockflag = 1,          
   employeeidupdate = @empid,          
   feeinitial = @strFeeInitial,          
   initialAdjustmentInitial = @strInitialAdjustmentInintial          
  where  ticketid_pk =  @ticketid          
          
            
 end          
else           
 if @CurrCalculatedTotalFee <> @CalculatedTotalFee          
  begin          
   insert into tblticketsnotes (ticketid, subject, recdate, employeeid)          
   values (@ticketid, 'PRICE QUOTE $' + convert(varchar(10),  convert(int, @CalculatedTotalFee )  ) , getdate(), @empid )          
  end          
          
commit tran      
      
          
if @CurrCalculatedTotalFee <> @CalculatedTotalFee          
 select 1          
else          
 select 0       

go
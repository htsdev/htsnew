﻿ /******  
* Created By :	  Saeed Ahmed.
* Create Date :   11/05/2010 
* Task ID :		  8501
* Business Logic :  This procedure is used to update hosptial detial.
* List of Parameter  @id
*					: @Name,
					: @City,
					: @State,
					: @ZipCode
* Column Return :    
******/
-- USP_HTP_UpdateHospitalDetail
CREATE PROC  dbo.USP_HTP_UpdateHospitalDetail
@id INT,
	@name VARCHAR(100),
	@City VARCHAR(50),
	@State INT,
	@ZipCode VARCHAR(10),
	@address VARCHAR(200)
as

UPDATE Hospital
SET
	[Name] = @name,
	City = @City,
	[State] = @State,
	ZipCode = @ZipCode,
	ADDRESS=@address
	
WHERE Id=@id	
GO

GRANT EXECUTE ON dbo.USP_HTP_UpdateHospitalDetail TO dbr_webuser
GO
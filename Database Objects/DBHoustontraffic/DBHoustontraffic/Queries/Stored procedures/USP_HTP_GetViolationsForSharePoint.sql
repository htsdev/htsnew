USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTP_GetViolationsForSharePoint]    Script Date: 11/07/2008 01:18:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** 
Created by: Aziz
Business Logic : Get all the violations against a ticket id provided 
					case is active
					violation belongs to a criminical court
					violation status is not "NO hire" and "Dispose"
 
List of Parameters:
	@TicketId
	@TicketsViolationID		
	 
 
List of Columns:
	CourtName - Short court name
	FirmName - Short Covering firm name
	ClientLanguage - Abbreviation of Client Language
	TotalFee - Total fee of the case
	PaidFee - Total amount paid by client till now
	Lastname - Last name of the client
	TicketsViolationID 
	CourtDateMain - Verified court date
******/

 -- [USP_HTP_GetCriminalViolationsForSharePoint] -1, 1381336
 -- [USP_HTP_GetViolationsForSharePoint] 196890, -1
ALTER PROCEDURE [dbo].[USP_HTP_GetViolationsForSharePoint]
(
	@TicketId int = NULL,
	@TicketsViolationID int = NULL
)
AS 

SELECT  
c.ShortName as CourtName, f.FirmAbbreviation as FirmName,
ClientLanguage = case when t.LanguageSpeak = 'SPANISH'	then 'S' else'E'end ,
t.calculatedtotalfee as TotalFee, 
PaidFee =  (                                               
 select isnull( SUM(ISNULL(p.ChargeAmount, 0)),0) from tblticketspayment p                                              
 where p.ticketid = t.ticketid_pk                                              
 and  p.paymentvoid = 0                                              
 ) ,
t.Lastname, tv.TicketsViolationID, tv.CourtDateMain, t.TicketId_pk as TicketId,
isnull(t.CaseTypeID,0) as CaseType --Sabir Khan 4662 09/12/2008
--, tv.CourtID, t.TicketID_PK, t.Firstname, t.MiddleName, 
 --t.Midnum, tv.RefCaseNumber,tv.CourtViolationStatusId, 
FROM tblTickets t 
INNER JOIN tblTicketsViolations tv ON t.TicketID_PK = tv.TicketID_PK
inner join tblCourts c on c.CourtId = tv.CourtID
Inner Join tblFirm f on f.FirmID = tv.CoveringFirmId

WHERE 
--Sabir Khan 4662 09/12/2008
----------------------------
t.CaseTypeID in (2,4) 
--(c.IsCriminalCourt=1)
----------------------------
and t.ActiveFlag = 1
and tv.CourtViolationStatusId not IN (80,236)

AND (@TicketId=-1 OR t.TicketId_pk = @TicketId ) 
AND (@TicketsViolationID=-1 OR tv.TicketsViolationID = @TicketsViolationID ) 

--GO
--Grant Execute on [dbo].[USP_HTP_GetViolationsForSharePoint] to dbr_webuser
--
--GO

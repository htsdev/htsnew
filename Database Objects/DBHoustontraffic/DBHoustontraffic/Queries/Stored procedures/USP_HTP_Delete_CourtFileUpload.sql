set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


/****** 
Create by:  Muhammd Kazim
Business Logic : this procedure is used to delete the particular file of a court.
 
List of Parameters:     
      @id : represents file id of file
  
******/


Create PROCEDURE [dbo].[USP_HTP_Delete_CourtFileUpload]
@id int

AS

delete from Courtfiles where id=@id
select @@rowcount

go


grant execute on dbo.[USP_HTP_Delete_CourtFileUpload] to [dbr_webuser]
go








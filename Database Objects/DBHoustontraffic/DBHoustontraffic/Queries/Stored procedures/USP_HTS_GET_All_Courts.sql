SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_GET_All_Courts]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_GET_All_Courts]
GO











CREATE        procedure USP_HTS_GET_All_Courts 
as

select	courtid,
	shortcourtname
from tblcourts
WHERE (courtid <> 0 and len(shortcourtname) > 1)






GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


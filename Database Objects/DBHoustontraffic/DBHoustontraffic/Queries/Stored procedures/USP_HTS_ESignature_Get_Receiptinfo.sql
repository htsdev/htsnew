﻿set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go




/*        
Altered By     : Fahad Muhammad Qureshi.
Created Date   : 04/23/2009  
TasK		   : 5806        
Business Logic : This procedure retrived all the information about the Reciept Report's Documnets,No of pages etc .  
           
Parameter:       
	@ticketID   : identifiable key  TicketId      
	@employeeid		: Employee which have Right to access the system
	@SessionID	: Session identifier 

     
      
*/  


-- [USP_HTS_ESignature_Get_ReceiptInfo] 124546,3992, '5527555lk87944' 
ALTER procedure [dbo].[USP_HTS_ESignature_Get_Receiptinfo] 
@ticketID int , 
@employeeid int , 
@SessionID varchar(200) 
as 
 
declare @paymentamount money 
declare @casenumbers varchar(500), 
@contactinfo varchar(100), 
@batchid int 
 
 
set @batchid=0 
 
select @paymentamount = sum(chargeamount) 
from tblticketspayment where ticketid = @ticketID 
and paymenttype not in (99,100) 
and paymentvoid not in (1) 
 
 
set @casenumbers = ' ' 
select @casenumbers = @casenumbers + (case when casenumassignedbycourt is null or casenumassignedbycourt ='' or left(casenumassignedbycourt, 2) = 'ID' then isnull(refcasenumber,' ') else casenumassignedbycourt end) + ',' 
 
from tblticketsviolations 
where ticketid_pk = @ticketid 
set @casenumbers = ltrim(left(@casenumbers,len(@casenumbers)-1)) 
 
declare @Address1 as varchar(50) 
declare @Address2 as varchar(50) 
-- Fahad 5806 05/06/2009 change the contact comments
set @Contactinfo = 
	(
	select  dbo.formatphonenumbers(t.contact1)  + char(13) + 
			dbo.formatphonenumbers(t.contact2) + char(13) +
			dbo.formatphonenumbers(t.contact3) 
	from tbltickets t where ticketid_pk = @ticketid
	) 
 
create table #ResultSet 
(ticketid_pk int, 
firstname varchar(50), 
MiddleName varchar(50), 
lastname varchar(50), 
Address1 varchar(100), 
Address2 varchar(100), 
ticketnumber_pk varchar(1000), 
languagespeak varchar(50), 
batchid int, 
paymentamount money, 
city varchar(50), 
state varchar(10), 
zip varchar(20), 
Address varchar(100), 
currentdateset datetime, 
totalfeecharged money, 
dueamount int, 
casenumbers varchar(300), 
settintype varchar(50), 
contactinfo varchar(200), 
FirmName varchar(50), 
ctno varchar(50), 
empName varchar(50), 
CaseNumber varchar(100), 
Status varchar(50), 
CourtDateTime datetime, 
CourtNo varchar(50), 
Location varchar(50), 
RowNo int NOT NULL IDENTITY (1, 1), 
----Fahad 5806 04/21/2009 Commented all ClientSign except ClientSign2
--ClientSign1 image, 
ClientSign2 image, 
--ClientSign3 image,
--ClientSign4 image, 
ViolDesc varchar(100) ,
hasBond INT)
 
declare @empName varchar(50) 
declare @numPayment int 
declare @numPlan int
 
set @empName=(select upper(firstname)+' '+upper(lastname) from tblusers where employeeid=@employeeid ) 

Set @numpayment = (Select Count(TicketID) from tblticketspayment where ticketid = @ticketid and paymenttype not in (99,100) and paymentvoid not in (1))

Set @numPlan = (Select Count(TicketID_PK) from tblSchedulePayment where ticketid_pk = @ticketid and ScheduleFlag <> 0)

insert into #ResultSet (ticketid_pk, firstname, MiddleName, lastname, Address1, Address2, ticketnumber_pk, languagespeak, batchid, paymentamount, city, state, zip, Address, currentdateset, totalfeecharged, dueamount, casenumbers, settintype, 
						contactinfo, FirmName, ctno, empName, CaseNumber, Status, CourtDateTime, CourtNo, Location, ViolDesc, hasBond) 
select T.ticketid_pk, firstname, MiddleName, lastname, 
 
(case when T.FirmID =3000 then T.Address1 else (Select Address from tblfirm where Firmid = T.FirmId) end) as Address1, 
(case when T.FirmID =3000 then T.Address2 else (Select Address2 from tblfirm where Firmid = T.FirmId) end ) as Address2, 
 
 @casenumbers as ticketnumber_pk, 
 t.languagespeak, @batchid as batchid, @paymentamount as paymentamount, 
 
 (case when T.FirmID = 3000 then T.city else (select city from tblfirm where FirmID =T.FirmID) end) as city, 
 (case when T.FirmID =3000 then S.state else (select S.state where S.StateID =(select state from tblfirm where FirmID =T.FirmID))end ) as state, 
 (case when T.FirmID =3000 then T.zip else (select Zip from tblfirm where FirmID =T.FirmID) end )as zip, 
 
 C.Address + ' ' + C.Address2 + ', ' + C.city + ', ' + 'TX ' + C.zip as Address, 
 v.courtdatemain as currentdateset, totalfeecharged, Convert(int,totalfeecharged-@paymentamount) as dueamount, 
 @casenumbers as casenumbers, o.description as settintype,
(case when T.FirmID =3000 then @contactinfo else (select dbo.formatphonenumbers(Phone) from tblfirm where FirmID =T.FirmID) end) as contactinfo, 
(case when T.FirmID = 3000 then (Firstname +' ' + MiddleName + ' ' + Lastname) else (select FirmName from tblfirm where tblfirm.FirmID =T.FirmID) end) as FirmName, 
 v.courtnumbermain as ctno , 
 @empName as empName , 
(case when V.casenumassignedbycourt is null or V.casenumassignedbycourt ='' or left(V.casenumassignedbycourt, 2) = 'ID' then isnull(V.refcasenumber,' ') else V.casenumassignedbycourt end) as CaseNumber, 
 o.shortdescription as Status, 
 v.courtdatemain as CourtDateTime, 
 v.courtnumbermain as CourtNo, 
 c.shortname as Location, 
 UPPER(case when len(isnull(vd.description,''))>25 then left(vd.description,25)+'....' else isnull(vd.description,'') END) as ViolDesc, -- Fahad 5908 05/16/2009 use upper case
 t.BondFlag 
from tbltickets T, tblcourts C, tbldatetype D ,tblstate S , tblticketsviolations V , tblcourtviolationstatus O, tblviolations vd 
 
where 
T.stateid_fk=S.stateid 
and T.ticketid_pk = V.ticketid_pk 
and V.courtviolationstatusidmain = O.courtviolationstatusid 
and O.categoryid = D.typeid 
and T.ticketid_pk in (@ticketID) 
and activeflag = 1 
and c.courtid = v.courtid 
and v.ViolationNumber_PK=vd.ViolationNumber_PK 
 
update #ResultSet Set 
----Fahad 5806 04/21/2009 Commented all ClientSign except ClientSign2
--ClientSign1 = (Select top 1 convert(varbinary(8000),ImageStream) From Tbl_HTS_ESignature_Images Where ImageTrace = 'ImageStream_Receipt_ClientSign1' and PageNumber = 1 and TicketID = @ticketID and SessionID = @SessionID), 
 ClientSign2 = (Select top 1 convert(varbinary(8000),ImageStream) From Tbl_HTS_ESignature_Images Where ImageTrace = 'ImageStream_Receipt_ClientSign1' and TicketID = @ticketID and SessionID = @SessionID)
-- ClientSign3 = (Select top 1 convert(varbinary(8000),ImageStream) From Tbl_HTS_ESignature_Images Where ImageTrace = 'ImageStream_Receipt_ClientSign1' and PageNumber = 3 and TicketID = @ticketID and SessionID = @SessionID), 
-- ClientSign4 = (Select top 1 convert(varbinary(8000),ImageStream) From Tbl_HTS_ESignature_Images Where ImageTrace = 'ImageStream_Receipt_ClientSign1' and PageNumber = 4 and TicketID = @ticketID and SessionID = @SessionID) --Fahad 5806 04/21/2009 ClientSign4 is Added
select *, @numpayment as NoOfPayments, @numPlan as NoOfPaymentPlans from #ResultSet 
 
drop table #ResultSet 


﻿USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTP_GET_SMSClientReplies]    Script Date: 07/04/2013 11:08:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/****** 
Created by		:		Farrukh Iftikhar
Task ID			:		10367 
Business Logic	:		The Procedure is used to get Client SMS replies. 
						
				
List of Parameters:	
	@fromreceiveddate		
	@toreceiveddate 
	@smsoutcome
	
*******/

--EXEC [dbo].[USP_HTP_GET_SMSClientReplies] '07/03/2013','07/03/2013',2

ALTER PROCEDURE [dbo].[USP_HTP_GET_SMSClientReplies]
	@fromreceiveddate DATETIME,
	@toreceiveddate DATETIME,
	@smsoutcome INT = -1
AS
BEGIN
	--WHERE DATEDIFF(DAY, GETDATE(), s.ReceivedDate) = 0
	SELECT DISTINCT s.ID, s.TicketNumber, s.TicketID, texttype
	INTO #uniqueRecs
	FROM SulloLaw.dbo.SMSDetails s  
	WHERE ISNULL(s.TicketNumber, '') <> ''
	AND DATEDIFF(DAY, s.ReceivedDate, @fromreceiveddate) <= 0
    AND DATEDIFF(DAY, s.ReceivedDate, @toreceiveddate) >= 0
	
    SELECT DISTINCT T.Lastname,
           T.Firstname,
           t.TicketID_PK AS TicketId,
           S.FromNumber,
           s.[Text],
           (
               CASE 
                    WHEN ISNULL(t.BondFlag, 0) = 0 THEN 'No'
                    ELSE 'Yes'
               END
           ) AS BondFlag,
           (
               SELECT tr.[Description]
               FROM   TrafficTickets.dbo.tblReminderstatus tr
               WHERE  tr.Reminderid_PK = TV.ReminderCallStatus
           ) AS CallBackStatus,
           (
               SELECT CASE 
                           WHEN Tv.AutoDialerReminderCallStatus = 0 THEN 
                                'Pending'
                           ELSE (
                                    SELECT adr.ResponseType
                                    FROM   TrafficTickets.dbo.AutoDialerResponse 
                                           adr
                                    WHERE  adr.ResponseID = Tv.AutoDialerReminderCallStatus
                                )
                      END
           ) AS AutoDialerStatus,
           (SELECT TOP 1 ttv.RefCaseNumber FROM tblTicketsViolations ttv WHERE ttv.TicketID_PK = t.TicketID_PK) AS TicketNumber,
           --s.TicketNumber,
           TV.ReminderCallStatus,
           Tv.AutoDialerReminderCallStatus,
           s.ReceivedDate,
           s.IsSMSReplyConfirmed,
           --ISNULL(tv.CourtDateMain,'1/1/1900') AS CourtDate,
           --dbo.fn_hts_get_TicketsViolationIds (t.TicketID_PK,tv.CourtDateMain) as TicketViolationIds,
           CASE WHEN s.textType = 'Set Call' OR s.textType = 'Manual' THEN 1 ELSE 0 END AS SmsTextType, 'Reminder Text' AS SMSType
           INTO #tempSmsReplies
    FROM   TrafficTickets.dbo.tblTickets T
           INNER JOIN TrafficTickets.dbo.tblTicketsViolations TV
                ON t.TicketID_PK = tv.TicketID_PK
           INNER JOIN SulloLaw.dbo.SMSDetails S
                ON t.TicketID_PK = s.TicketID
    WHERE  ISNULL(T.BondFlag, 0) = 0
           AND DATEDIFF(DAY, s.ReceivedDate, @fromreceiveddate) <= 0
           AND DATEDIFF(DAY, s.ReceivedDate, @toreceiveddate) >= 0
           AND (s.textType LIKE 'Reminder Call' OR ISNULL(s.textType, '') = '')
           AND s.ID IN (SELECT u.ID FROM #uniqueRecs u)
          
  
  
    -- Rab Nawaz 10914 07/02/2013 Added to show the Set call status on SMS client replies report. . . 
    INSERT INTO #tempSmsReplies
    SELECT DISTINCT T.Lastname,
           T.Firstname,
           t.TicketID_PK AS TicketId,
           S.FromNumber,
           s.[Text],
           (
               CASE 
                    WHEN ISNULL(t.BondFlag, 0) = 0 THEN 'No'
                    ELSE 'Yes'
               END
           ) AS BondFlag,
           (
               SELECT tr.[Description]
               FROM   TrafficTickets.dbo.tblReminderstatus tr
               WHERE  tr.Reminderid_PK = TV.setcallstatus
           ) AS CallBackStatus,
           (
               SELECT CASE 
                           WHEN Tv.AutoDialerSetCallStatus = 0 THEN 
                                'Pending'
                           ELSE (
                                    SELECT adr.ResponseType
                                    FROM   TrafficTickets.dbo.AutoDialerResponse 
                                           adr
                                    WHERE  adr.ResponseID = Tv.AutoDialerSetCallStatus
                                )
                      END
           ) AS AutoDialerStatus,
           (SELECT TOP 1 ttv.RefCaseNumber FROM tblTicketsViolations ttv WHERE ttv.TicketID_PK = t.TicketID_PK) AS TicketNumber,
           TV.setcallstatus,
           Tv.AutoDialerSetCallStatus,
           s.ReceivedDate,
           s.IsSMSReplyConfirmed,
           --ISNULL(tv.CourtDateMain,'1/1/1900') AS CourtDate,
           --dbo.fn_hts_get_TicketsViolationIds (t.TicketID_PK,tv.CourtDateMain) as TicketViolationIds,
           CASE WHEN s.textType = 'Set Call' OR s.textType = 'Manual' THEN 1 ELSE 0 END AS SmsTextType, 'Set Text' AS SMSType
    FROM   TrafficTickets.dbo.tblTickets T
           INNER JOIN TrafficTickets.dbo.tblTicketsViolations TV
                ON t.TicketID_PK = tv.TicketID_PK
           INNER JOIN SulloLaw.dbo.SMSDetails S
                ON t.TicketID_PK = s.TicketID
    WHERE  ISNULL(T.BondFlag, 0) = 0
           AND DATEDIFF(DAY, s.ReceivedDate, @fromreceiveddate) <= 0
           AND DATEDIFF(DAY, s.ReceivedDate, @toreceiveddate) >= 0
           AND s.textType LIKE 'Set Call'
           AND s.ID IN (SELECT u.ID FROM #uniqueRecs u)
           
    
    ALTER TABLE #tempSmsReplies
    ADD TicketViolationIds VARCHAR(300), CourtDate DATETIME
    
    UPDATE s
    SET TicketViolationIds = dbo.fn_hts_get_TicketsViolationIds (v.TicketId_pk,v.CourtDateMain), CourtDate = ISNULL(v.CourtDateMain, '01/01/1900') 
    from #tempSmsReplies s INNER JOIN TrafficTickets.dbo.tblTicketsViolations v 
    ON v.TicketID_PK = s.TicketId 
    AND TicketNumber = CASE WHEN LEN(LTRIM(RTRIM(v.RefCaseNumber))) > 0 THEN LTRIM(RTRIM(v.RefCaseNumber)) ELSE v.casenumassignedbycourt END 
    
    
    IF @smsoutcome = 1
    BEGIN
        SELECT DISTINCT *
        FROM   #tempSmsReplies t
        WHERE  t.ReminderCallStatus = 8
    END
    ELSE 
    IF @smsoutcome = 2
    BEGIN
        SELECT DISTINCT *
        FROM   #tempSmsReplies t
        WHERE  t.ReminderCallStatus = 0
        
    END
    ELSE
    BEGIN
        SELECT DISTINCT *
        FROM   #tempSmsReplies t
       
    END
    
    DROP TABLE #tempSmsReplies
END

GO
GRANT EXECUTE ON [dbo].[USP_HTP_GET_SMSClientReplies] TO dbr_webuser
GO
﻿ /******     
Alter by:  Noufil khan    
Business Logic : this procedure is used to get Client Information when a single letter printin batch print    
    
List of Parameters:     
 @TicketID : By ticketID we can seearch our desire result.    
    
******/    
  
Create Procedure [dbo].[USP_HTP_SOLreport]    
(        
@TicketID int        
)        
as        
        
 select Distinct T.TicketID_PK ,  T.FirstName , T.LastName , T.Address1 ,T.City , T.Zip ,S1.State, TV.RefcaseNumber , T.languageSpeak,  TV.CourtDateMain , TV.CourtNumberMain , CVS.ShortDescription as Status ,    
  C.Address + ', ' + C.City + ', '+S2.State +' ' + C.Zip as Location ,

TV.casenumassignedbycourt ,v.Description

from tbltickets T       
join tblticketsviolations TV        
On T.TicketID_Pk = TV.Ticketid_pk         
join tblcourtviolationstatus CVS        
On CVS.courtviolationstatusid = TV.CourtViolationStatusidmain        
inner join tblviolations v 
on tv.ViolationNumber_PK = v.ViolationNumber_PK  
join tblcourts C        
on C.courtID = TV.CourtID        
join tblState S1         
on S1.Stateid = t.StateID_fk        
join tblState S2        
on  S2.Stateid = C.State        
where         
T.Ticketid_pk = @TicketID  
  
  
Go


Grant execute on [dbo].[USP_HTP_SOLreport] to dbr_webuser

GO
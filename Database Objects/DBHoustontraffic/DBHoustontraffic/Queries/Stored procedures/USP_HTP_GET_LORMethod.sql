﻿ set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

/****** 
Create by:  Fahad Qureshi

Business Logic : This procedure simply Update Mode of LOR Method

Parameters:
	@TicketId 

Columns: 
	count or records.

******/
ALTER PROCEDURE  [dbo].USP_HTP_GET_LORMethod
@TicketId int=0
AS 
select count(c.courtid) 
from dbo.tblTicketsViolations TV 
inner join tblCourts C on TV.Courtid=C.Courtid
where 
c.lor=0
and isnull(tv.CourtViolationStatusIDmain,0) NOT IN (80,236) --Sabir Khan 6821 10/20/2009 Exclude dispose and no hire cases...
and TV.ticketid_pk=@TicketId 

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

--[USP_HTS_ESignature_Get_ClientInfo] 124546
/*
Created By     : Adil Aleem.
Created Date   : N/A  
Business Logic : This procedure retrived all the information about the client by Ticket ID.  
           
Parameter:       
	@TicketID   : identifiable key  TicketId      
*/
ALTER PROCEDURE [dbo].[USP_HTS_ESignature_Get_ClientInfo]  
@TicketID int        
        
AS        
--Delete from Tbl_HTS_Esignature_Images Where TicketID = @TicketID And ImageTrace Like '%Client%'  
(        
  
  
SELECT     TOP 1 tblTickets.TicketID_PK, isnull(tblTickets.LastName,'') + ', ' + isnull(tblTickets.MiddleName,'') + ' ' + tblTickets.Firstname as ClientName, tblTickets.LanguageSpeak as ClientLanguage, isnull(tblTickets.Address1,'') + ' ' + isnull(tblTickets.Address2,'') as Address1, isnull(tblTickets.Email,'') as Email, isnull(tblTickets.City,'') as City,   
                      isnull(tblTickets.Zip,'') as Zip, isnull(tblState.State,'') as State, isnull(tblTickets.Contact1,'') as Contact1, isnull(tblTickets.ContinuanceAmount,0) as ContinuanceAmount, isnull(tblTickets.BondFlag,0) as BondFlag, isnull(tblTicketsViolations.CourtID,0) as CourtID,   
                      isnull(tblCourtViolationStatus.CategoryID,0) as CategoryID, tblTickets.Activeflag,  
     isnull(tblTicketsViolations.RefCaseNumber,'') as TicketNumber, isnull(tblTicketsViolations.casenumassignedbycourt, '') as CauseNumber ,  
(Case When isnull(tblTicketsViolations.CourtId,0) in(3001, 3002, 3003) then 'InSide' Else 'OutSide' End) As CourtStatus,
(Case When isnull(tblTicketsViolations.CourtId,0) in(3001, 3002, 3003) OR (isnull(tblTicketsViolations.CourtId,0) >= 3007 AND isnull(tblTicketsViolations.CourtId,0)<= 3022) then 1 Else 0 End) AS HMC_HCJP -- Adil 5806 05/08/2009 Column added to indicate HMC & HCJP cases
FROM         tblTicketsViolations INNER JOIN  
                      tblCourtViolationStatus ON tblTicketsViolations.CourtViolationStatusIDmain = tblCourtViolationStatus.CourtViolationStatusID INNER JOIN  
                      tblTickets ON tblTicketsViolations.TicketID_PK = tblTickets.TicketID_PK INNER JOIN  
                      tblState ON tblTickets.Stateid_FK = tblState.StateID  
WHERE     (tblTickets.Activeflag = 1) AND (tblTicketsViolations.TicketID_PK = @TicketID)  
)  


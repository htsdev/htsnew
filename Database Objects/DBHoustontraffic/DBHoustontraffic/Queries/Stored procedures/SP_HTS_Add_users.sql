IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_HTS_Add_users]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_HTS_Add_users]
  go
--khalid 2/19/08  updated for serviceticket admin
create procedure [dbo].[USP_HTS_Add_users]            
 @firstname varchar(20),                
 @lastname varchar(20),                
 @Abbreviation varchar(3),                
 @username varchar(20),                
 @password varchar(20),                
 @accesstype tinyint,                
 @CanUpdateCloseOutLog int,             
 @employeeid int,              
 @email varchar(100),        
 @FlagCanSPNSearch bit,        
 @SPNUserName varchar(20),        
 @SPNPassword varchar(20),      
 @Status int,    
 @NTUserID varchar(20), 
 @IsAttorney bit,
 @IsSTAdmin bit
   
as 
--keep only one service ticket admin
if @IsSTAdmin=1
   update tblusers set isServiceTicketAdmin=0
               
if @employeeid = 0                
 insert into tblusers (firstname, lastname, abbreviation, username, [password], accesstype,CanUpdateCloseOutLog,Email,FlagCanSPNSearch,SPNUserName,SPNPassword, Status, NTUserID , IsAttorney,isServiceTicketAdmin)              
 values (@firstname, @lastname, @abbreviation, @username, @password, @accesstype,@CanUpdateCloseOutLog,@email,@FlagCanSPNSearch,@SPNUserName,@SPNPassword, @Status, @NTUserID , @IsAttorney,@IsSTAdmin)        
                
else                
 update tblusers                
 set firstname =  @firstname,                
  lastname = @lastname,                
  abbreviation =  @abbreviation,                
  username =  @username,                
  [password] = @password,                
  accesstype =  @accesstype,              
  CanUpdateCloseOutLog = @CanUpdateCloseOutLog,             
  Email = @email,        
  FlagCanSPNSearch = @FlagCanSPNSearch,        
  SPNUserName = @SPNUserName,        
  SPNPassword = @SPNPassword  ,      
  Status = @Status,    
  NTUserID = @NTUserID,
  IsAttorney=@IsAttorney,
isServiceTicketAdmin=@IsSTAdmin    

  where employeeid =  @employeeid   
go
GO
GRANT EXEC ON [dbo].[USP_HTS_Add_users] TO [dbr_webuser]
GO
             
                
     
      
   
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Delete_LetterBatch_New]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Delete_LetterBatch_New]
GO



create  procedure USP_HTS_Delete_LetterBatch_New  
@LetterID int,    
@BatchIDList varchar(1000) = '0,'    
as  
BEGIN  
  
update tblhtsbatchprintletter  
 set deleteflag = 1  
 where letterid_fk = @LetterID  
 and BatchID_PK IN (select * from dbo.Sap_String_Param( @BatchIDList ))  
  
END


--------Added by Azwar when delete from batch print Trial letter than update jurystatus field in tbltickets to 0
if @LetterID=2

begin
	update t  set  jurystatus = 0 from tbltickets t inner join 
	tblhtsbatchprintletter tb on 
	t.ticketid_pk=tb.ticketid_fk 
where
	tb.BatchID_PK IN (select * from dbo.Sap_String_Param( @BatchIDList )) 
end
-----------------------------------------------------------------------------------------------------------------

          
         
          


   

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


/*
* Modified by: Saeed
* Task ID : 7791
* Business logic : This procedure is used to get non HMC cases having past court dates 
* Parameter :     
* @showall : if @showall=0 then display records whose followup date is null,past or today, otherwise display all records.
* @ValidationCategory : Report category; 1=Alert & 2=Report
* */
-- USP_HTP_Get_PastCourtDateForNonHMC 0,1
ALTER PROCEDURE [dbo].[USP_HTP_Get_PastCourtDateForNonHMC] 
@showall INT,
@ValidationCategory int=0 -- Saeed 7791 07/09/2010 @ValidationCategory=1 for alert & @ValidationCategory=2 for report
AS
	SELECT DISTINCT 
	       tv.TicketID_PK AS ticketid_pk,
	       tv.RefCaseNumber AS ticketnumber,
	       tv.casenumassignedbycourt AS causenumber,
	       t.Firstname AS fname,
	       t.Lastname AS lname,
	       dbo.fn_DateFormat(tv.CourtDateMain, 24, '/', ':', 1) AS courtdatemain,
	       --Sabir Khan 7979 07/14/2010 check for sugarland court for couurt room number
	       CONVERT(VARCHAR, tv.CourtNumbermain) AS courtnumber,
	       tcvs.ShortDescription AS STATUS,
	       c.ShortName AS shortname,
	       tv.CourtDateMain AS CourtDate,
	       -- Noufil 5691 04/06/2009 Show HMC followupdate
	       t.PastCourtDateFollowUpDate,
	       tv.CourtID
	       
	FROM   tblTickets AS t
	       INNER JOIN tblTicketsViolations AS tv
	            ON  t.TicketID_PK = tv.TicketID_PK
	       INNER JOIN tblCourtViolationStatus AS tcvs
	            ON  tv.CourtViolationStatusIDmain = tcvs.CourtViolationStatusID
	       INNER JOIN tblCourts AS c
	            ON  tv.CourtID = c.Courtid
	WHERE  --Ozair 7791 07/24/2010  where clause optimized  
	       (
	           (@showall = 1 OR @ValidationCategory = 2) -- Saeed 7791 07/09/2010 display all records if showAll=1 or validation category is 'Report'
	           OR (
	                  (@showall = 0 OR @ValidationCategory = 1)
	                  AND DATEDIFF(
	                          DAY,
	                          ISNULL(t.PastCourtDateFollowUpDate, '01/01/1900'),
	                          GETDATE()
	                      ) >= 0
	              ) -- Saeed 7791 07/09/2010 display  if showAll=0 or validation category is 'Alert' then display records which has 'follow up date' null,past or today.
	       )
	       AND --Don't Display Cases having bond status 
	           (
	               tv.CourtViolationStatusIDmain NOT IN (80, 105, 104, 201, 202, 123, 135)
	           )
	       AND (DATEPART(dw, tv.CourtDateMain) NOT IN (7, 1))
	       AND (tv.CourtDateMain < GETDATE() - 3)
	       AND (tv.CourtDateMain >= '01/01/2007') 
	           -- Noufil 5691 04/06/2009 Cases only in HMC court
	       AND c.CourtID NOT IN (3001, 3002, 3003)
	       AND (t.Activeflag = 1)
	ORDER BY
	       t.PastCourtDateFollowUpDate DESC,
	       tv.TicketID_PK -- Adil Aleem 5857 05/15/2009 Include Ticket ID in order by clause
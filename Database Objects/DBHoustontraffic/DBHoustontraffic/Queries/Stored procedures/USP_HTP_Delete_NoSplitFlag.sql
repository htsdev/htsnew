﻿  
/****** Object:  StoredProcedure [dbo].[USP_HTP_Update_NonHMCFollowUpdate]  ******/     
/*        
Created By     : Fahad Muhammad Qureshi.
Created Date   : 04/10/2009  
TasK			: 5753        
Business Logic  : This procedure delete no split flag record .  
           
Parameter:       
   @TicketID     : Deleting criteria with respect to TicketId      
  
     
      
*/      


Create PROCEDURE USP_HTP_Delete_NoSplitFlag
(
	@TicketID INT
)
AS
DELETE tblTicketsFlag  WHERE FlagID=12 AND TicketID_PK=@TicketID

GO

grant exec on [dbo].[USP_HTP_Delete_NoSplitFlag] to dbr_webuser

GO  
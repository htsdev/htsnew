SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_LA_Get_FilesForDay]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_LA_Get_FilesForDay]
GO



CREATE PROCEDURE USP_HTS_LA_Get_FilesForDay
@Mode as int,
@Load_Date as datetime
AS

Select Distinct(GroupID) From Tbl_HTS_LA_Summary Where Convert(varchar,Record_Date,101) = Convert(Varchar,@Load_Date,101) And Loader_ID = @Mode


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


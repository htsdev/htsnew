SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Created By:		Rab Nawaz Khan
-- Create date:		09/18/2013
-- Business Logic:	This Procedure is used to get all the letter types for LMS Address Check report to dispaly the bad mail letters information
-- =============================================
CREATE PROCEDURE USP_HTP_GetBadMailLetterTypes 
AS
BEGIN
	SET NOCOUNT ON;

    SELECT tl.LetterID_PK AS ID,  tc.ShortName + ' -' + REPLACE(tl.LetterName, 'Criminal', '') AS LetterName
	FROM tblLetter tl INNER JOIN tblCourts tc ON tc.courtcategorynum = tl.courtcategory
	WHERE tl.courtcategory = 8 -- Only HCCCC 
	AND tl.LetterID_PK = 138 -- Only Booked In
	AND tl.isactive = 1
END
GO
	GRANT EXECUTE ON USP_HTP_GetBadMailLetterTypes TO dbr_webuser
GO

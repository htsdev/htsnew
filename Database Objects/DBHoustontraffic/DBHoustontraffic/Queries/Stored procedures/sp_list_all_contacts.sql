﻿ ALTER PROCEDURE [dbo].sp_list_all_contacts
	@isactive BIT = 1
AS
	SELECT tct.contacttype_pk,
	       tct.description,
	       tct.AutoDialerPriority,
	       tct.IsActive
	FROM   tblcontactstype tct
	WHERE  @isactive IS NULL
	       OR  (tct.IsActive = @isactive)
	ORDER BY
	       tct.contacttype_pk
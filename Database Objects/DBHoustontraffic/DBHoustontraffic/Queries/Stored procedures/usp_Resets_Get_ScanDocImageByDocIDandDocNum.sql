SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Resets_Get_ScanDocImageByDocIDandDocNum]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Resets_Get_ScanDocImageByDocIDandDocNum]
GO




--usp_Get_SacanDocImageByDocIDandDocNum 444,165  
  
  
CREATE   PROCEDURE [dbo].[usp_Resets_Get_ScanDocImageByDocIDandDocNum]   
  
@DocID int,  
@DocNum int  
  
  
AS  
  
 SELECT     ScanRepos.dbo.DOCPIC.DOCPIC  
 FROM        ScanRepos.dbo.DOCPIC   
 WHERE     (ScanRepos.dbo.DOCPIC.DOC_ID = @DocID and ScanRepos.dbo.DOCPIC.Docnum =@DocNum )  
  
  
  
  
  
  




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


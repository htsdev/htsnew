﻿/************************************************************
 * Code formatted by SoftTree SQL Assistant © v4.0.34
 * Time: 11/11/2009 4:51:54 PM
 ************************************************************/

/*
 * Business Logic :  This procedure will be used to get last causenumber inserted for montgomery court.
 * Parameter : @IsYearWise
 * Columns : CauseNumber
 */
 
CREATE PROCEDURE dbo.usp_htp_get_MaxCauseNumberForMontgomeryCourt
	@IsYearWise
	 BIT
AS
BEGIN
    SELECT tta.TicketNumber_pk
           FROM   tblTicketsViolationsArchive tta
           WHERE  tta.Courtlocation = 3036
                  AND SUBSTRING(tta.TicketNumber_pk, 1, 2) <> 'MX'
    
END
GO
 
 GRANT EXECUTE ON dbo.usp_htp_get_MaxCauseNumberForMontgomeryCourt TO dbr_webuser
 
 
 
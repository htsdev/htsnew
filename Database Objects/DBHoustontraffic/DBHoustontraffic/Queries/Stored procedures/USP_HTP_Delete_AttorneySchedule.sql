﻿/****** 
Create by		: Muhammad Nasir Mamda
Created Date	: 12/14/2009
TasK			: 6968

Business Logic : 
			This procedure delete attorney schedule by given parameter.
			
List of Parameters:				
			@AttorneyScheduleID attorney schedule identification
					
******/  


Create PROCEDURE dbo.USP_HTP_Delete_AttorneySchedule
	@AttorneyScheduleID INT
AS
BEGIN
    DELETE  FROM AttorneysSchedule    
    WHERE  AttorneyScheduleID = @AttorneyScheduleID
END 

GO 
   
   GRANT EXECUTE ON dbo.USP_HTP_Delete_AttorneySchedule TO dbr_webuser
      
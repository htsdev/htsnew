SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Insert_ReadNoteFlag]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Insert_ReadNoteFlag]
GO

                
CREATE procedure [dbo].[USP_HTS_Insert_ReadNoteFlag]          
@TicketID int,                      
@FlagID int,                   
@EmpID as int          
as                      
insert into tblticketsflag(ticketid_pk,flagid,Empid,recdate) values(@ticketid,@flagid,@EmpID,getdate())          
declare @desc varchar(20)                      
select @desc=description from tbleventflags where flagid_pk=@flagid                    
insert into tblticketsnotes(ticketid,subject,employeeid,Notes) values(@ticketid,'Flag Added -'+ @desc,@empid,null)  
                  
          

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


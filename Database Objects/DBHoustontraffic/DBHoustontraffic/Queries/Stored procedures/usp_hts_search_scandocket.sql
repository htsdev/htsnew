SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_hts_search_scandocket]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_hts_search_scandocket]
GO




    
-- usp_hts_search_scandocket 2,'01/18/2007','01/28/2008',0,0       
CREATE Procedure [dbo].[usp_hts_search_scandocket]        
        
@Type  int,        
@DateFrom datetime,        
@DateTo  datetime,        
@AttorneyID  int,        
@EmpID   int        
        
as        
         


declare @SqlStr varchar(max)
--Almost completely modified for task 2691 on 01/29/2008.
--added the temp table
set @SqlStr= 'declare @temp  table
(
bookid int,
ScanDate datetime,
DocketDate datetime,
Attorney varchar(100),
DocCount int,
Abbreviation varchar(10)
) 
'

-- added the slection for Other Attorney Docket
if(@AttorneyID = 0 or @AttorneyID=99)
begin
	set @SqlStr= @SqlStr + 'insert into @temp (bookid,ScanDate,DocketDate,Attorney,DocCount,Abbreviation)
	(SELECT b.ID AS bookid, b.ScanDate, b.DocketDate, ''Other Attorney Docket'' AS Attorney, b.DocCount, e.Abbreviation        
	FROM    dbo.tblScanDocketBook AS b INNER JOIN        
			dbo.tblUsers AS e ON b.EmpID = e.EmployeeID        
	WHERE (b.isdeleted = 0)
	and b.AttorneyID=99'        
	if  @Type=1        
	set @SqlStr=@SqlStr+' AND (b.ScanDate BETWEEN '''+ Convert(varchar,@datefrom,101) +''' AND '''+ Convert(varchar,@dateto,101)+ ' 23:59:59:998''' +')'         
	if @Type=2        
	set @SqlStr=@SqlStr+' AND (b.DocketDate BETWEEN '''+ Convert(varchar,@datefrom,101) +''' AND '''+ Convert(varchar,@dateto,101)+ ' 23:59:59:998''' +')'         
	if @EmpID>0        
	set @SqlStr=@SqlStr+' AND (b.EmpID = '+Convert(varchar,@EmpID)+')'        
	        
	set @SqlStr = @SqlStr+ ')'

end

-- Modified the slection for attorney's docket
if(@AttorneyID<>99)
begin        
	set @SqlStr=@SqlStr + '
	insert into @temp (bookid,ScanDate,DocketDate,Attorney,DocCount,Abbreviation)
	(SELECT b.ID AS bookid, b.ScanDate, b.DocketDate, a.Lastname + '', '' + a.Firstname AS Attorney, b.DocCount, e.Abbreviation        
	FROM    dbo.tblScanDocketBook AS b INNER JOIN        
			dbo.tblUsers AS a ON b.AttorneyID = a.EmployeeID INNER JOIN        
			dbo.tblUsers AS e ON b.EmpID = e.EmployeeID        
	WHERE (b.isdeleted = 0)'        
	if  @Type=1        
	set @SqlStr=@SqlStr+' AND (b.ScanDate BETWEEN '''+ Convert(varchar,@datefrom,101) +''' AND '''+ Convert(varchar,@dateto,101)+ ' 23:59:59:998''' +')'         
	if @Type=2        
	set @SqlStr=@SqlStr+' AND (b.DocketDate BETWEEN '''+ Convert(varchar,@datefrom,101) +''' AND '''+ Convert(varchar,@dateto,101)+ ' 23:59:59:998''' +')'         
	if @AttorneyID>0        
	set @SqlStr=@SqlStr+' AND (b.AttorneyID = '+Convert(varchar,@AttorneyID)+')'         
	if @EmpID>0        
	set @SqlStr=@SqlStr+' AND (b.EmpID = '+Convert(varchar,@EmpID)+')'        
	        
	set @SqlStr = @SqlStr+ ')'

end

set @SqlStr = @SqlStr+ '
	select * from @temp'

if @Type = 2    
	set @SqlStr = @SqlStr+ ' ORDER BY DocketDate desc, Attorney'    
	    
if @Type = 1    
	set @SqlStr = @SqlStr+ ' ORDER BY ScanDate desc, Attorney'    
    
--print @SqlStr        
        
exec(@SqlStr) 

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

grant exec on [dbo].[usp_hts_search_scandocket] to dbr_webuser
go
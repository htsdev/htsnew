SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_hts_EmailFailed_update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_hts_EmailFailed_update]
GO


CREATE procedure [dbo].[usp_hts_EmailFailed_update]    
    
@emailaddress varchar(50)   
    
AS    
    
Declare @TicketIDs Table
(
	TicketID int
)
    
INSERT INTO @TicketIDs 
select distinct ticketid_pk from tbltickets where email = @emailaddress

if (select count(*) from @TicketIDs) > 0
begin
	INSERT INTO tblnoemailflag
	select TicketID, @emailaddress, getdate() from @TicketIDs
end

  
  
  
  
  
  
  
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


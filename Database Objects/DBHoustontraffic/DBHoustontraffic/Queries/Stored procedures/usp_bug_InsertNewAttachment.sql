SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_bug_InsertNewAttachment]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_bug_InsertNewAttachment]
GO




--usp_bug_InsertNewAttachment  'dmb.doc','test description',3028,'10/20/2006','msword',3991,1  
CREATE procedure [dbo].[usp_bug_InsertNewAttachment]    
@FileName as varchar(50),    
@Description as varchar(100),    
@Size as int,    
@UploadDate as datetime,    
@ContentType as varchar(15),    
@EmployeeID as int,    
@BugID as int,
@Attachmentid int=0 output   
    
as    
    
insert into tbl_bug_attachment    
(     
 AttachFileName,    
 description,    
 file_size,    
 uploadeddate,    
 contenttype,    
 EmployeeID,    
 Bug_id    
)    
values     
(     
 @FileName,    
 @Description,    
 @Size,    
 --@UploadDate,   
 getdate(), 
 @ContentType,    
 @EmployeeID,    
 @BugID    
)    
set @Attachmentid=@@identity






GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


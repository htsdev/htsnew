    
/******     
    
Alter by:  NOufil khan    
Business Logic : procedure use to insert flag in the database     
    
List of Parameters:     
 @TicketID                                                      
 @FlagID    
 @EmpID    
 @ServiceTicketCategory    
 @Priority    
 @ShowOnTrialDocket    
    
******/      
    
    
--     
      --[USP_HTS_Insert_flag_by_ticketnumber_and_flagID] 162601,33,3991    
ALTER procedure [dbo].[USP_HTS_Insert_flag_by_ticketnumber_and_flagID]  
@TicketID int,                                                      
@FlagID int,                                                    
@EmpID as int  ,                                            
@ServiceTicketCategory as int = 0,           
@Priority as int = 0 ,        
@ShowOnTrialDocket int = 0         
                                                    
as      
    
declare @desc varchar(100)                                                      
select @desc=description from tbleventflags where flagid_pk=@flagid    
    
    

-- Rab Nawaz 10941 04/02/2013 Duplicate insertion for the Wrong Telephone Number Flag has been fixed. . . 
DECLARE @isWrongNumberDataInserted BIT
SET @isWrongNumberDataInserted = 0
if @FlagID = 33 -- Wrong Telephone Number Flag
begin    
 declare @counting INT 
 SET @isWrongNumberDataInserted = 1 -- Rab Nawaz 10941 04/02/2013 Setting variable value true if Wrong phone number block executed. . . 
 select @counting =count(ticketid_pk) from tblticketsflag     
  where ticketid_pk=@TicketID and flagid=33    
  if @counting =0
  begin    
   insert into tblticketsflag(ticketid_pk,flagid,empid,assignedto,ServiceTicketCategory,Priority,ShowTrailDocket) values(@ticketid,33,@empid,3996,@ServiceTicketCategory,@Priority,@ShowOnTrialDocket)      
  end  -- Noufil 4369 07/22/2008 return statement remove  
End    
    
    
                                                 
          
 if @FlagID = 23          
   Begin          
   insert into tblticketsflag(ticketid_pk,flagid,empid,assignedto,ServiceTicketCategory,Priority,ShowTrailDocket) values(@ticketid,@flagid,@empid,3996,@ServiceTicketCategory,@Priority,@ShowOnTrialDocket)                                                    
  
   End  
  
 if @FlagID <> 8                                                  
   insert into tblticketsnotes(ticketid,subject,employeeid,Notes) values(@ticketid,'Flag -'+ @desc,@empid,null)                                                     
                                                    
                                                  
 --Added By Zeeshan Ahmed                                                  
 --Used For Continuance Flag                                                  
 if @FlagID = 9                                                  
  Begin                                                  
   Update tbltickets set ContinuanceFlag = 1 where ticketid_pk =@ticketid                                                   
  End                                                  
  
 -- tahir 4777 09/10/2008   
 -- added same functionality for new flag 'Problem Client (No Hire)'  
 --if @FlagID = 36 -- Agha Usman 4347 07/18/2008    
 if @FlagID in (13,36 )  
  begin  
    Declare @FName varchar(20),    
    @MName varchar(6),    
    @LName varchar(20),    
    @refcasenumber varchar(20),    
    @DOB datetime     
       
    select @FName = FirstName,@MName = MiddleName,@LName=LastName,@DOB=dob,@refcasenumber =    
     (select top 1 refcasenumber from tblticketsviolations where ticketId_pk = t.ticketId_pk)     
    from tbltickets t where ticketId_Pk = @TicketID    
       
    -- Agha Usman 4497  07/31/2008 - Remove Middle Name Check     
    select distinct t.ticketId_pk into #temp1 from tbltickets t    
    where FirstName = @Fname    
    and LastName = @LName    
    and datediff(day,isnull(dob,'1-1-1900'),isnull(@dob,'1-1-1900'))= 0    
   -- and isnull(dob,'') = isnull(@DOB,'')    
    and (Select Count(*) from tblticketsFlag  where ticketId_Pk = t.ticketId_pk and FlagId = @FlagID) = 0    
       
    insert into tblticketsflag(ticketid_pk,flagid,empid)    
    select ticketId_pk,@FlagID,@EmpID from #temp1    
       
    insert into tblticketsnotes(ticketid,subject,employeeid)     
    select ticketId_Pk,'Flag -'+ @desc + '- added for ticket number (' + @refcasenumber + ')',3992  -- Haris Ahmed 10290 07/04/2012 Fix bug for Problem Client (No Hire) Flag   
    from #temp1     
    where ticketId_pk != @TicketID    
       
    if not exists(Select * from ProblemClientLookUp     
    where FirstName = @FName    
    and LastName = @LName    
    and datediff(day,isnull(dob,'1-1-1900'),isnull(@dob,'1-1-1900'))= 0)    
    begin     
     INSERT INTO [dbo].[ProblemClientLookup] ([FirstName],[MiddleName],[Lastname],[DOB])VALUES(@FName,@MName,@LName,@DOB)    
    end    
       
    update tbltickets set isProblemClientManual = 1 where ticketId_pk in (select ticketId_pk from #temp1)    
    -------------------------------------------------------------------------  
    -- Sabir Khan 9654 11/24/2011 Add Marks the records as Do Not Mail  
    -------------------------------------------------------------------------  
    insert into tbl_hts_DoNotMail_Records(FirstName ,LastName ,Address ,Zip ,City ,StateID_fk ,insertDate,UpdateSource)                                            
      
    select t.FirstName,t.LastName,isnull(t.Address1,'')+' '+isnull(t.address2,''),t.Zip,t.City,t.StateID_fk,getdate(),10                                 
    from tbltickets t  inner join                                             
    tblticketsflag f on t.ticketid_pk=f.ticketid_pk                                            
    where f.flagid=@FlagID   and t.ticketid_pk  =@TicketID                                         
                                            
    update ta set                                             
    donotmailflag=1                                             
    from tblticketsarchive ta inner join                                            
    tbltickets t on ta.recordid=t.recordid inner join                                             
    tblticketsflag f on t.ticketid_pk=f.ticketid_pk                                            
    where f.flagid=@FlagID and t.ticketid_pk  =@TicketID  
    -------------------------------------------------------------------------   
    drop table #temp1  
   end      
 else 
  Begin          
  IF (@isWrongNumberDataInserted = 0) -- Rab Nawaz 10941 04/02/2013 Duplicate insertion for the Wrong Telephone Number Flag has been fixed. . . 
  BEGIN
   DECLARE @FirmID INT--Fahad 6054 08/19/2009 associate firm with billing page to Contact Page  
   SET @FirmID = 0  
   SELECT top 1 @FirmID = ttp.PaymentReference FROM tblTicketsPayment ttp WHERE ttp.TicketID = @TicketID AND ttp.PaymentType = 104 AND ttp.PaymentVoid = 0 ORDER BY ttp.RecDate DESC  
   insert into tblticketsflag(ticketid_pk, flagid, empid, assignedto) values(@ticketid, @flagid, @empid, case when @FlagID = 23 then 3996 WHEN @FlagID = 8 AND @FirmID <> 0 THEN @FirmID else null end)
   

   --Muhammad Nasir 10071 12/24/2012 set reviewed status to 0 and call back status to pending
   if @flagid = 34 --bad email
	begin
	 update tblTickets
	 SET ReviewedEmailStatus = 0
	 ,ReviewedEmailStatusID = 0
	 where ticketID_PK = @TicketID
	end
   
	END
  
  End     
          
---declare @desc varchar(100)                                                      
--select @desc=description from tbleventflags where flagid_pk=@flagid                                                    
                                               
                                                
  
                                                
-------------------------------------------------Added by Tahir Ahmed -------------------------------------------------------------------------                                            
                                            
            --On Nov 6, 2007        
            --If "No Plea Out" flag is set, put trial notes for the case....        
------------------------------------------------------------------------------------------------------------------------------------------------                                            
if @flagid = 26        
 begin        
  declare @TrialComments varchar(2000)        
  select @TrialComments = isnull(TrialComments,'') from tbltickets where ticketid_pk = @TicketID        
        
  declare @empnm3 as varchar(12)                                                  
                                              
  select @empnm3 = abbreviation from tblusers where employeeid = @empid                                                  
                                         
  select @TrialComments =  @TrialComments + ' NO PLEA OUT Flag set ' + '(' +dbo.formatdateandtimeintoshortdateandtime(getdate()) + ' - ' +  @empnm3 + ')' + ' '                                                  
                           
  update tbltickets set  TrialComments = @TrialComments where ticketid_pk = @TicketID                                                    
 end        
------------------------------------------------------------------------------------------------------------------------------------------------                                            
        
        
                                            
                                            
-------------------------------------------------Added by M.Azwar Alam-------------------------------------------------------------------------                                            
                                            
            --On 29 August 2007                                            
           --If No Advertisement Flag is Selected then update tblticketsarchive and set donotmailflag to 1                           
------------------------------------------------------------------------------------------------------------------------------------------------                                            
if @FlagID =24                                           
 Begin                                            
                                 
                                
declare @courtcnt int                                
                                
select   @courtcnt=count(courtid) from tblticketsviolations where ticketid_pk=  @TicketID and courtid=3037--for Jims                                
                                
if    @courtcnt =0                                   
                                            
 begin                                         
                                            
 insert into tbl_hts_DoNotMail_Records                                            
 (                                            
  FirstName ,                                            
  LastName ,                                            
  Address ,                                            
  Zip ,                                            
  City ,                                            
  StateID_fk ,                                           
  insertDate                                             
 )                                            
 select                                             
  t.FirstName,                                            
  t.LastName,                                            
  isnull(t.Address1,'')+' '+isnull(t.address2,''),                                          
  t.Zip,                                            
  t.City,                                            
  t.StateID_fk,                     
  getdate()                                            
                                            
 from                         
   tbltickets t  inner join                                             
   tblticketsflag f on t.ticketid_pk=f.ticketid_pk                                            
   where f.flagid=@FlagID   and t.ticketid_pk  =@TicketID                                         
                                            
 update ta set                                             
                                            
  donotmailflag=1                                             
                                            
 from tblticketsarchive ta inner join                                            
   tbltickets t on ta.recordid=t.recordid inner join                                             
   tblticketsflag f on t.ticketid_pk=f.ticketid_pk                                            
   where f.flagid=@FlagID and t.ticketid_pk  =@TicketID                   
                
--If No Advertisement flag added then do not show on Traffic alert and Quote Call back---Added by M.Azwar Alam-----                
declare @contact1 varchar(50)                
declare @contact2 varchar(50)                
declare @contact3 varchar(50)                
                
    begin                
                
     select @contact1=contact1 from tbltickets where ticketid_pk=@TicketID                
     select @contact2=contact2 from tbltickets where ticketid_pk=@TicketID                
     select @contact3=contact3 from tbltickets where ticketid_pk=@TicketID                
                
     if (@contact1 is not null and @contact1 <>'')                
      begin                
       insert into tblRestrictedPhoneNumbers                 
       select @contact1                
      end                
                
     if (@contact2 is not null and @contact2 <>'')                
      begin                
       insert into tblRestrictedPhoneNumbers                 
       select @contact2                
      end                
                
     if (@contact3 is not null and @contact3 <>'')                
      begin                
       insert into tblRestrictedPhoneNumbers                 
       select @contact3                
      end                
                     
                
    end                
                
                
   Update tblviolationquote set followupyn=0 where ticketid_fk=@TicketID                
                
                
                
------------------------------------------------------------------------------------------------------------------                
                
                               
 end                       
                                    
                                    
 else                                 
 begin                                   
--------------------Added By M.Azwar Alam on 7 September 2007----------------------------------------                                    
---------------------Update donotmailflag for JIMS cAses---------------------------------------------                                    
                                    
                                    
                                     
                                      
  INSERT INTO tbl_hts_donotmail_records  (FirstName, LastName, Address, Zip, City, StateID_FK)                                                       
  SELECT                                                 
  t.FirstName,                                             
  t.LastName,                
  isnull(t.Address1,'')+' '+isnull(t.address2,''),                                             
  t.Zip,                                             
  t.City,                                    
  t.StateID_fk                                            
  FROM    tbltickets t                         
                                      
                                                
    inner join tblticketsviolations tv on                                     
 t.ticketid_pk=tv.ticketid_pk inner join  tblticketsflag f                                     
 on t.ticketid_pk=f.ticketid_pk                                            
 where f.flagid=@FlagID and tv.courtid=3037 and  t.ticketid_pk  =@TicketID                                    
                                     
                                        
                                         
                                              
                                                      
   update cc set                                             
             
  donotmailflag=2                                             
                                            
 from JIMS.dbo.CriminalCases  cc inner join tblticketsviolations tv                                     
  on cast(cc.bookingnumber as bigint)=tv.recordid inner join tbltickets t on tv.ticketid_pk=t.ticketid_pk inner join                                     
   tblticketsflag f on t.ticketid_pk=f.ticketid_pk                                            
   where f.flagid=@FlagID  and tv.courtid=3037  and t.ticketid_pk  =@TicketID                                        
                                            
                                       
      end                                
                                         
                                              
                                  
 End                                            
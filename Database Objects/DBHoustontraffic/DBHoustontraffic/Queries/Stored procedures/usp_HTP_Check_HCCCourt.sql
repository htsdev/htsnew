/****** 
Create by:  Syed Muhammad Ozair
Business Logic : this procedure is used to check for any HCC (Haris County Criminal) court in the given case excluding disposed and no hire violations. 

List of Parameters:	
	@Ticketid : Ticket Id of the case

List of Columns: 	
	isfound : if found then 1 else 0

******/
CREATE Procedure dbo.usp_HTP_Check_HCCCourt

@Ticketid int

as

declare @count int


select @count=count(*) from tblticketsviolations
where ticketid_pk=@Ticketid
and courtid=3037
and courtviolationstatusidmain not in (80,236)

if @count>0
	select 1 as isfound
else
	select 0 as isfound

go

grant exec on dbo.usp_HTP_Check_HCCCourt to dbr_webuser
go
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

/*
Author: Adil Aleem
Business Logic : Use to insert OR update events for DMC court house through loader service.
It performs following activities.
1. Check violation in Violation's referential table if not exists then insert new violation.
2. Check attorney in Attorney's referential table if not exists then insert new attorney.
3. Check attorney association with defendant if non exists or changed then insert new association info.
4. Check defendant's profile in non-client if not exists then insert new defendant.
5. Check defendant's violations if not exists then insert new violations against defendant.
6. Check defendant's violations if exists but court setting info is changed then update new violations' info.
7. It only treats Judge and Jury cases.

Parameters: 
	@Record_Date: Case record/insert date
	@List_Date: Court list date
	@Ticket_Number:Cause OR Violation number
	@Last_Name: Defendant's last name
	@First_Name: Defendant's first name
	@Middle_Name: Defendant's Middle name
	@DOB: Defendant's Date of Birth
	@Address: Defendant's home address
	@City: Defendant's home city
	@State: Defendant's home state
	@ZIP: Defendant's home zip code
	@PhoneNumber: Defendant's Contact Number
	@DP2: Defendant's barcode part. return by zp4 webservice
	@DPC: Defendant's barcode part. return by zp4 webservice
	@Violation_Description: Violation's title of Defendant
	@violationcode : Violation COde
	@Cause_Number: Defendant's Cause Number
	@Court_Date: Defendant's court date	
	@Court_Time: Defendant's court Time
	@Court_RoomNo:Defendant's court RoomNo
	@FineAmount: Defendant's Fine Amount
	@Court_Status: Defendant's Court Status
	@Officer_Name: Defendant's Officer which Deals the Case
	@Attorney: Defendant's Attorney
	@GroupID: Data file ID
	@AddressStatus: Address status [y/d/s/n] of defendant
	@Insert_Mode int: it Defines the Mode
	@RowID int: It represents the Identity for the record
	@Flag_SameAsPrevious: insure wether this case belongs to previous one or not [It helps to cut-off researching the defendant profile]
	@PreviousRecordID: If this case belongs to previous case then previous case ID will be the value of @PreviousRecordID
	@LoaderID: Insure which loader is executing this case.
	@CurrentRecordID: It returns current Case-ID/Record-ID to calling program
	@Flag_Inserted: It returns status wether the record inserted or deleted
	@Flag_Updated_Clients: It returns status wether the record updated
*/

ALTER PROCEDURE [dbo].[USP_HTS_LA_Insert_Tickets_DallasFTP]
@Record_Date datetime,  
@List_Date datetime,  
@Ticket_Number as varchar(20),
@Last_Name as varchar(50),  
@First_Name as varchar(50),
@Middle_Name as varchar(50),
@DOB as datetime,
@Address as varchar(200),
@City as varchar(35),
@State as Varchar(10),
@ZIP as varchar(10),
@PhoneNumber as varchar(30),
@DP2 varchar(2),
@DPC varchar(1),
@Violation_Description as Varchar(2000),
@Violation_Code as Varchar(50),
@Violation_Date as smalldatetime,
@Cause_Number as varchar(30),
@Court_Date as datetime,
@Court_Time as datetime,
@Court_RoomNo as varchar(10),
@FineAmount as money,
@Court_Status varchar(50),
@Officer_Name varchar(100),
@Attorney	varchar(100),
@GroupID as Int,
@AddressStatus Char(1),
@Insert_Mode int,
@RowID int,
@Flag_SameAsPrevious as bit,
@PreviousRecordID as int,
@LoaderID as int,
@CurrentRecordID int output,
@Flag_Inserted int Output,
@Flag_Updated_Clients int Output
AS
set nocount on  

/* 
@Flag_Inserted
   1: Jury Inserted
   2: Jury Updated
   3: Judge Inserted
   4: Judge Updated

@Flag_Updated_Clients
   1: Jury Updated [Clients]
   2: Judge Updated [Clients]
*/

Declare @StateID as Int
Declare @ViolationID as int
Declare @IDFound as int
Declare @BondAmount as money
Declare @AttorneyID as int
Declare @ClientID as int
Declare @TicketID as int
declare @CourtDateFound datetime
declare @CourtStatusFound varchar(10)
declare @CourtNumberFound int
Declare @IsExistsInAttorneyCase as int
Declare @Attorney_LastName varchar(30)
Declare @Attorney_FirstName varchar(30)

Set @First_Name = left(@First_Name, 20) -- Getting just 20 characters because of field size limitation.
Set @Last_Name = left(@Last_Name, 20) -- Getting just 20 characters because of field size limitation.
Set @Middle_Name = left(@Middle_Name, 1) -- Getting just 1 character because of field size limitation.
Set @PhoneNumber = replace(@PhoneNumber, '-', '')

-- Getting Attorney's First Name
Set @Attorney_FirstName = dallastraffictickets.dbo.FormatName(@Attorney, 1)
-- Getting Attorney's Last Name
Set @Attorney_LastName = dallastraffictickets.dbo.FormatName(@Attorney, 2)

Set @Violation_Code = 'XD' + @Violation_Code

Set @Flag_Inserted = 0

set @Court_Date = @Court_Date + @Court_Time -- Merging court time into court date

If @ZIP = '00000'
	Begin
		Set @ZIP = ''
	End

-- Identifying case status 'T' For Judge & 'J' For Jury Trial
Select @Court_Status = case 
when @Court_Status = 'T' then '103' -- Judge Trial
when @Court_Status = 'J' then '26' -- Jury
end
-- Adil 5939 05/25/2009 commented the code
----Adil 5568 02/21/2009 Hard Fix 8:15 AM court time for Jury
--if @Court_Status = '26'
--	begin
--		Set @Court_Date = convert(datetime,convert(varchar(12),@Court_Date,110) + ' 08:15 AM')
--	END

-- Setting default state as 'Texas'
If Len(@State)<1
	Begin
		Set @State = 'TX'
	End

-- Check and insert new attorney
set @AttorneyID = null
if @Attorney_FirstName is not null or @Attorney_LastName is not null
Begin
	select @AttorneyID = ID from DallasTrafficTickets.dbo.Attorneys where FirstName = @Attorney_FirstName and LastName = @Attorney_LastName
	if @AttorneyID is null
		BEGIN
			insert into DallasTrafficTickets.dbo.Attorneys(FirstName, LastName) values(@Attorney_FirstName, @Attorney_LastName)
			set @AttorneyID = scope_identity()
		END
End

Set @AttorneyID = isnull(@AttorneyID, 0)

-- Check and insert new violation
Set @ViolationID = null
Select Top 1 @ViolationID = ViolationNumber_PK, @FineAmount = isnull(ChargeAmount,0), @BondAmount = isnull(BondAmount,0) From DallasTrafficTickets.dbo.TblViolations Where (Description = @Violation_Description and ViolationCode = @Violation_Code and CourtLoc = 3049 and ViolationType = 4)
If @ViolationID is null
	Begin
		Insert Into DallasTrafficTickets.dbo.TblViolations (Description, Sequenceorder, IndexKey, IndexKeys, ChargeAmount, BondAmount, Violationtype, ChargeonYesflag, ShortDesc, ViolationCode, AdditionalPrice, Attorneyregularprice, Attorneybondprice, CourtLoc)
		Values(@Violation_Description, null, 2, 160, 160, 200, 4, 1, 'None', @Violation_Code, 0, 50, 95, 3049)
		Set @ViolationID = scope_identity()
		Set @FineAmount = 160
		Set @BondAmount = 200
	End

-- Get state ID against state name
Set @StateID = (Select Top 1 StateID From DallasTrafficTickets.dbo.TblState Where State = @State)

If @Flag_SameAsPrevious = 0 and @Insert_Mode = 1
	Begin
	-- Check defendant info for existence
		Set @IDFound = null

		SELECT Top 1 @IDFound = T.RecordID
		From DallasTrafficTickets.dbo.tblTicketsArchive T
		Inner Join DallasTrafficTickets.dbo.tblTicketsViolationsArchive TV
		On T.RecordID = TV.RecordID
		Where FirstName = @First_Name and LastName = @Last_Name and Address1 = @Address and datediff(day,DOB, @DOB) = 0
		and ZipCode = @Zip and datediff(day,ViolationDate, @Violation_Date) = 0 and TV.CourtLocation = 3049

		If @IDFound is null
			Begin
				-- If Ticket Number is not valid then generate Ticket number.
				if Len(@Ticket_Number) < 5
					Begin
						insert into DallasTrafficTickets.dbo.tblIDGenerator(recdate) values(getdate())
						select @Ticket_Number='ID' + convert(varchar(12),@@identity)
					End

				-- Insert defendant's info
				Insert Into DallasTrafficTickets.dbo.tblTicketsArchive (RecLoadDate, ListDate, CourtDate , OfficerName , ViolationDate, TicketNumber, MidNumber, FirstName, LastName, Initial , PhoneNumber, DOB, Address1, City, StateID_FK, ZipCode, CourtID, GroupID, Flag1, DP2, DPC, InsertionLoaderID)   
				Values(@Record_Date, @List_Date, @Court_Date, @Officer_Name , @Violation_Date, @Ticket_Number, @Ticket_Number, @First_Name, @Last_Name, @Middle_Name , @PhoneNumber, @DOB, @Address, @City, @StateID, @ZIP, 3049, @GroupID, @AddressStatus, @DP2, @DPC, @LoaderID)
				
				Set @CurrentRecordID = scope_identity()
			End
		Else
			Begin
				Set @CurrentRecordID = @IDFound
			End
	End
Else
	Begin
		Set @CurrentRecordID = @PreviousRecordID
	End

-- Getting fine amount
Declare @ChargeAmount as money
BEGIN TRY 
	set @ChargeAmount = DallasTrafficTickets.dbo.GetDallasViolationFineamount(@Violation_Description, @FineAmount)
END TRY
BEGIN CATCH
	set @ChargeAmount=@FineAmount
END CATCH
/*
Insert_Mode 1: Insert-able Cases & Defendants
Insert_Mode 4: Existing Defendants + Insert-able Cases
*/
If @Insert_Mode in (1, 4)
	Begin
		-- Insert defendant's violation
		Insert Into DallasTrafficTickets.dbo.tblTicketsViolationsArchive (TicketNumber_PK, ViolationNumber_PK, CauseNumber, CourtDate, Courtnumber , UpdatedDate, ViolationDescription, ViolationCode, FineAmount, CourtLocation, RecordID, ViolationStatusID, LoaderUpdateDate, InsertionLoaderId, GroupID, AttorneyID)
		Values(@Ticket_Number, @ViolationID, @Cause_Number, @Court_Date, @Court_RoomNo , GetDate(), @Violation_Description, @Violation_Code,@ChargeAmount, 3049, @CurrentRecordID, Convert(Int,@Court_Status), getdate(), @LoaderID, @GroupID, @AttorneyID)
		Set @RowID = scope_identity()
		Set @Flag_Inserted = (case when @Court_Status = '26' then 1 else 3 end)
	End
/*
Insert_Mode 2: Existing Updateable Cases
*/
If @Insert_Mode = 2
	Begin
		-- Update defendant's violation
		Update DallasTrafficTickets.dbo.tblTicketsViolationsArchive Set CourtDate = @Court_Date, violationstatusid =Convert(Int , @Court_Status), Courtnumber = @Court_RoomNo,
		LoaderUpdateDate = GetDate() , UpdationLoaderId = @LoaderID, AttorneyID = (case when @AttorneyID = 0 then AttorneyID else @AttorneyID end), GroupID = @GroupID
		where RowID = @RowID

		Set @Flag_Inserted = (case when @Court_Status = '26' then 2 else 4 end)
	End


-- If defendant is client and Auto Court Setting Info is changed then update client Auto info too.
set @ClientID = null
set @TicketID = null
set @Flag_Updated_Clients = 0

select @ClientID = TicketsViolationID, @TicketID = TicketID_PK, @CourtDateFound = CourtDate, @CourtStatusFound = CourtViolationStatusID, @CourtNumberFound = CourtNumber
from DallasTrafficTickets.dbo.tblTicketsViolations
where (casenumassignedbycourt = @Cause_Number or casenumassignedbycourt = @Ticket_Number) and CourtID = 3049 and CourtViolationStatusID <> 80
and ((datediff(day, CourtDate, @Court_Date) <> 0 and datediff(hh, CourtDate, @Court_Date) <> 0) or CourtViolationStatusID <> Convert(Int,@Court_Status) or Convert(Int,CourtNumber) <> Convert(Int,@Court_RoomNo))
if @ClientID is not null
	begin
		update DallasTrafficTickets.dbo.tblTicketsViolations set CourtDate = @Court_Date,
		CourtViolationStatusID = Convert(Int,@Court_Status), CourtNumber = Convert(Int,@Court_RoomNo),
		UpdatedDate = getdate(), GroupID = @GroupID, UpdationLoaderID = @LoaderID
		where TicketsViolationID = @ClientID

		-- Insert note when auto court date time is changed
		if datediff(day, @CourtDateFound, @Court_Date) <> 0 and datediff(hh, @CourtDateFound, @Court_Date) <> 0
		Begin
			insert into DallasTrafficTickets.dbo.tblticketsnotes (ticketid, subject , employeeid)
			select @TicketID, 'Auto Court Date Updated From [' + DallasTrafficTickets.dbo.fn_DateFormat(@CourtDateFound, 5, '/', ':', 1) + '] To [' + DallasTrafficTickets.dbo.fn_DateFormat(@Court_Date, 5, '/', ':', 1) + '] Through Dallas Event Status Loader', 3992
		End

		-- Insert note when auto court status is changed
		if @CourtStatusFound <> Convert(Int, @Court_Status)
		Begin
			Select @Court_Status = case 
			when @Court_Status = '103' then 'JUD' -- Judge Trial
			when @Court_Status = '26' then 'JUR' -- Jury Trial
			end

			Select @CourtStatusFound = ShortDescription From DallasTrafficTickets.dbo.tblcourtviolationstatus where CourtViolationStatusID = convert(int, @CourtStatusFound)

			insert into DallasTrafficTickets.dbo.tblticketsnotes (ticketid, subject , employeeid)
			select @TicketID, 'Auto Court Status Updated From [' + @CourtStatusFound + '] To [' + @Court_Status + '] Through Dallas Event Status Loader', 3992
		End

		-- Insert note when auto court number is changed
		if @CourtNumberFound <> Convert(Int,@Court_RoomNo)
		Begin
			insert into DallasTrafficTickets.dbo.tblticketsnotes (ticketid, subject , employeeid)
			select @TicketID, 'Auto Court Number Updated From [' + convert(varchar(5), @CourtNumberFound) + '] To [' + convert(varchar(5), @Court_RoomNo) + '] Through Dallas Event Status Loader', 3992
		End

		Set @Flag_Updated_Clients = (case when @Court_Status = '26' then 1 else 2 end)
	end

-- Check Association of Defendant with Attorney
If @AttorneyID <> 0
	Begin
		set @IsExistsInAttorneyCase = null
		select @IsExistsInAttorneyCase =  ID from DallasTrafficTickets.dbo.Attorneys_CaseAssociation 
		where RecordID = @CurrentRecordID and isnull(AttorneyID,0) = @AttorneyID and RowID = @RowID

		-- Associate Defendant with Attorney
		if @IsExistsInAttorneyCase is null
			Begin
				Insert into DallasTrafficTickets.dbo.Attorneys_CaseAssociation(RecordID, RowID, AttorneyID, InsertedDate)
				values(@CurrentRecordID, @RowID, @AttorneyID, getdate())
				-- Adil 5470 30/1/2009 Identify updated case type [Jury/Judge] in case of Attorney change only
				Set @Flag_Inserted = (case when @Court_Status = '26' then 2 else 4 end)
			End
	End

--grant execute on dbo.USP_HTS_LA_Insert_Tickets_DallasFTP to dbr_webuser

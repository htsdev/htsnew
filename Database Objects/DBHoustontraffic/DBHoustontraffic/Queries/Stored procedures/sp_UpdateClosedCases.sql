USE [TrafficTickets]
GO
/****** 
Create by		: Sabir Khan
Created Date	: 06/03/2009
TasK ID			: 4413

Business Logic : This procedure is used to update all records of Non Clients and Quote clients 
				 To Dispose the cases:
				 -- By Matching Ticket Number & Violation Number
				 -- By Matching Ticket Number
				 -- By Matching Cause Number	 
				 -- By Matching Ticket Number & Sequence Number 
	
******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER procedure [dbo].[sp_UpdateClosedCases]
as
DECLARE @violationid int ,
@status int,
@courtdate datetime,
@courtnum as varchar(3),
@courtdatemain datetime,
@LoaderId TINYINT

SET NOCOUNT ON

SET @LoaderId = 5

update tblclosedcases set casenumber = replace(casenumber,' ','')

SELECT casenumber,ticketnumber,firstname,initial,lastname,STATUS,CloseDate,CloseComments INTO #temp FROM tblclosedcases

--Sabir Khan 4413 05/03/2009 add column into temp table...
ALTER TABLE #temp 
	ADD RowId INT PRIMARY KEY identity(1,1),
		lookupid INT NOT NULL DEFAULT(0),
		ticketviolationid INT NOT NULL DEFAULT(0)
		
--update newly added column if cause number is exists in database...
UPDATE a SET  a.lookupid = v.rowid
FROM  #temp a INNER JOIN dbo.tblticketsviolationsarchive v ON v.CauseNumber = a.casenumber
WHERE ISNULL(a.lookupid,0) = 0  
AND v.CourtLocation IN (3001,3002,3003)

--update newly added column if ticket number is exists in database...
UPDATE a SET  a.lookupid = v.rowid
FROM  #temp a INNER JOIN dbo.tblticketsviolationsarchive v ON v.TicketNumber_PK = a.TicketNumber
WHERE ISNULL(a.lookupid,0) = 0  
AND v.CourtLocation IN (3001,3002,3003)

--update column into temp table...
UPDATE a SET  a.lookupid = v.rowid
FROM  #temp a INNER JOIN dbo.tblticketsviolationsarchive v ON a.TicketNumber = V.ticketnumber_pk + convert(varchar(2),v.violationnumber_pk)
WHERE ISNULL(a.lookupid,0) = 0  
AND v.CourtLocation IN (3001,3002,3003)

--update newly added column if cause number is exists in Quote client...
UPDATE a SET  a.ticketviolationid = v.TicketsViolationID
FROM  #temp a INNER JOIN tblticketsviolations v ON a.casenumber = v.casenumassignedbycourt
WHERE ISNULL(a.ticketviolationid,0) = 0 
AND v.courtid IN (3001,3002,3003)

--update newly added column if ticket number is exists in Quote client...
UPDATE a SET  a.ticketviolationid = v.TicketsViolationID 
FROM  #temp a INNER JOIN tblticketsviolations v ON a.TicketNumber = v.refcasenumber
WHERE ISNULL(a.ticketviolationid,0) = 0 
AND v.courtid IN (3001,3002,3003)

--update column into temp table...
UPDATE a SET  a.ticketviolationid = v.TicketsViolationID
FROM  #temp a INNER JOIN tblticketsviolations v ON a.TicketNumber = V.refcasenumber + convert(varchar(2),V.sequencenumber)
WHERE ISNULL(a.ticketviolationid,0) = 0 
AND v.courtid IN (3001,3002,3003)

--Sabir Khan 4413 03/11/2009 Create temp table table for getting updated records rowid and violationstatusid...
DECLARE @NonClient table(RowId int);
DECLARE @QuoteClient table(ViolationstatusId int);

declare @reccount INT, @idx INT
declare @addedrec int, @updatedrec int, @tempupdate INT 
select @idx = 1, @reccount = count(rowid) from #temp
select @addedrec = 0, @updatedrec = 0, @tempupdate = 0

While @idx <= @reccount
BEGIN
		--Update non client cases...
		update V
		set violationstatusid = 80,
		courtdate = closedate,V.Status = C.status,updateddate = closedate,
		UpdationLoaderId = @LoaderId
		OUTPUT INSERTED.rowid into @NonClient 
		from #temp C INNER JOIN dbo.tblticketsviolationsArchive V ON C.lookupid = V.RowID
		where C.rowid = @idx AND ISNULL(C.lookupid,0) > 0

		--Update client
		update V
		set courtdate = closedate,updateddate = closedate,
		courtviolationstatusid = 80, vemployeeid = 3992
		OUTPUT INSERTED.TicketsViolationID into @QuoteClient 
		from
		tblticketsviolations V INNER JOIN #temp C ON V.TicketsViolationID = C.ticketviolationid 
		where C.rowid = @idx AND ISNULL(C.ticketviolationid,0) > 0

	SET @idx = @idx + 1

END

DROP TABLE #temp

--Sabir Khan 4413 03/11/2009 get total count of updated records...
SELECT @updatedrec = 
		ISNULL((select COUNT(Distinct rowid) from @NonClient ),0) 
		+ 
		ISNULL((select COUNT(Distinct ViolationstatusId) from @QuoteClient),0)

-- SENDING NO OF RECORDS UPDATED THROUGH EMAIL   
-- sabir khan 4413 05/03/2009
EXEC usp_sendemailafterloaderexecutes  0,@LoaderId,'houston','',@updatedrec 

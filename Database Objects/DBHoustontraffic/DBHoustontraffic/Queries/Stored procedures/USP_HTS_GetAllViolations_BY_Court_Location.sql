SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_GetAllViolations_BY_Court_Location]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_GetAllViolations_BY_Court_Location]
GO

  
Create PROCEDURE [dbo].[USP_HTS_GetAllViolations_BY_Court_Location]  --3001          
(    
    
@courtid int    
    
)            
AS        
    
    
-- If Court Is Criminal Court Display Criminal Court Violations    
If (select IsCriminalCourt from tblcourts where courtid = @courtid   ) = 1    
Begin    
    if @courtid=3047
		begin
			SELECT    ViolationNumber_PK as ID, Description           
			FROM         tblViolations            
			where  ViolationNumber_PK in (0,16159)        
			order by Description asc    
		end
	else
		begin
			SELECT    ViolationNumber_PK as ID, Description           
			FROM         tblViolations            
			where  violationtype=9    --ViolationNumber_PK  NOT in (0,9662,9663,9665,9666,9667,9668,9669)           
			or   ViolationNumber_PK in (0,16227)        
			order by Description asc    
		end
End    
    
else    
Begin    
    
SELECT     ViolationNumber_PK as ID, Description  FROM         tblViolations            
where  violationtype=8 or   ViolationNumber_PK=0  and ViolationNumber_PK  NOT in (0,9662,9663,9665,9666,9667,9668,9669)           
order by SequenceOrder    
    
End     
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


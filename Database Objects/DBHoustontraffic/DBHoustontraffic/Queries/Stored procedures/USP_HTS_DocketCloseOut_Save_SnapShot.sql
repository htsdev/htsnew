SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_DocketCloseOut_Save_SnapShot]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_DocketCloseOut_Save_SnapShot]
GO

        
CREATE PROCEDURE [dbo].[USP_HTS_DocketCloseOut_Save_SnapShot]              
              
as              
              
declare @prev int              
              
set @prev = (        
  SELECT count(RecordID)         
  from tbl_HTS_DocketCloseOut_Snapshot         
  where DateDiff(Day,getdate(),UploadDate)= 0        
  )              
              
if(@prev<>0)              
begin        
 delete from tbl_HTS_DocketCloseOut_Snapshot         
 where DateDiff(Day,getdate(), UploadDate)= 0        
end        
              
INSERT INTO tbl_HTS_DocketCloseOut_Snapshot(        
  TicketID_PK,LastName, FirstName, MiddleName, BondFlag,         
  CourtDate, CourtNumber, ShortDescription, CourtName, TrialComments,         
  Address1, Midnum,ClientCount,UploadDate, TicketsViolationID, CourtID       
  )              
    
SELECT DISTINCT         
  tblTickets.TicketID_PK,         
  tblTickets.Lastname,         
  tblTickets.Firstname,         
  tblTickets.MiddleName,         
  tblTickets.BondFlag,         
  tblTicketsViolations.CourtDateMain,                                 
  tblTicketsViolations.CourtNumbermain,         
  tblCourtViolationStatus.ShortDescription,         
  tblCourts.CourtName,         
  tblTickets.TrialComments,         
  tblTickets.Address1,                                 
  tblTickets.Midnum,         
--tblcourtviolationstatus.categoryid,    
  dbo.fn_HTS_Get_Client_Count(tblTickets.Lastname,tblTickets.Firstname,tblTickets.Address1,tblTickets.Midnum) as clientcount,        
  getdate(),         
--tblCourtViolationStatus.CourtViolationStatusID,    
  tblTicketsViolations.TicketsViolationID,      
--tblTicketsViolations.ViolationNumber_PK, 
  tblCourts.CourtID 
FROM         dbo.tblTickets INNER JOIN                                    
                  dbo.tblTicketsViolations ON dbo.tblTickets.TicketID_PK = dbo.tblTicketsViolations.TicketID_PK INNER JOIN                                    
  dbo.tblCaseDispositionstatus ON dbo.tblTicketsViolations.violationstatusid = dbo.tblCaseDispositionstatus.casestatusid_pk INNER JOIN                                    
   dbo.tblfirm ON dbo.tblTickets.firmid = dbo.tblfirm.firmid INNER JOIN                                    
                      dbo.tblViolations ON dbo.tblTicketsViolations.ViolationNumber_PK = dbo.tblViolations.ViolationNumber_PK INNER JOIN                                    
                      dbo.tblcourtviolationstatus ON dbo.tblTicketsviolations.courtviolationstatusidmain = dbo.tblcourtviolationstatus.courtviolationstatusID INNER JOIN                                    
                      dbo.tblCourts ON dbo.tblTicketsViolations.courtid = dbo.tblCourts.Courtid LEFT OUTER JOIN                                    
                      dbo.tblOfficer ON dbo.tblTickets.OfficerNumber = dbo.tblOfficer.OfficerNumber_PK            
-- TAKING DOCKET'S OF NEXT DAY......        
--WHERE DateDiff(Day, convert(datetime,'1/24/2007'), tblTicketsViolations.CourtDateMain)= 0      
       
    
WHERE     (dbo.tblViolations.Violationtype not IN (1) or dbo.tblViolations.violationnumber_pk in (11,12) ) AND (dbo.tblcourtviolationstatus.categoryid IN (select distinct categoryid from tblcourtviolationstatus where violationcategory in (0,2)            
  
    
and categoryid not in (7,50) and courtviolationstatusid not in (104) ))                                        
AND (dbo.tblTickets.Activeflag = 1)                                     
and dbo.tblTicketsViolations.courtid <> 0                                    
--and DateDiff(Day, convert(datetime,'1/24/2007'), tblTicketsViolations.CourtDateMain)= 0      
and ShortDescription Not In('BOND','COM','DSCD','JUR2' ,'DAR','DPX','CLO' ) /* and ShortDescription  in ('ARR','PRE','JUR','JUR2','JUD') */    
and DateDiff(Day, GetDate(), tblTicketsViolations.CourtDateMain)= 1      
order by   tblCourts.CourtID, tblCourts.CourtName, tblTicketsViolations.CourtNumbermain, tblTicketsViolations.CourtDateMain ,  tblTickets.Lastname 



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


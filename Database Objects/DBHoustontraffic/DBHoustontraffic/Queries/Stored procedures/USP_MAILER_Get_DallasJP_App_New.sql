/****** 
Created by:		Tahir Ahmed
Business Logic:	The procedure is used by LMS application to get the number of printable letter
				count for the Dallas County JP Appearance letter for the selected date range.
				
List of Parameters:
	@startdate:	Starting list date for Arraignment letter.
	@enddate:	Ending list date for Arraignment letter.
	@courtid:	Court id of the selected court category if printing for individual court hosue.
	@bondflag:	Flag if printing bond letters else appearance letter.
	@catnum:	Category number of the selected letter type, if printing for 
				all courts of the selected court category

List of Columns:	
	Total_Count:	Total number of letters exists for the selected date range.
	Good_Count:		Total number of letters that have valid addresses
	Bad_Count:		Total Number of letter that have undeliverable addresses
	DonotMail_Count:Total number of letter for addresses that had been marked to stop sending letters.
	

******/

--  USP_MAILER_Get_DallasJP_App_New '5/1/09', '6/2/09', 11, 0, 11    
ALTER procedure [dbo].[USP_MAILER_Get_DallasJP_App_New]    
 (    
 @startdate datetime,    
 @enddate datetime,    
 @courtid int = 11,    
 @bondflag tinyint = 0,    
 @catnum int=11    
 )    
    
as    
    
-- DECLARING LOCAL VARIABLES FOR FILTERS...
declare @officernum varchar(50),                                                        
 @officeropr varchar(50),                                                        
 @tikcetnumberopr varchar(50),                                                        
 @ticket_no varchar(50),                                                                                                      
 @zipcode varchar(50),                                                                 
 @zipcodeopr varchar(50),                                                        
 @fineamount money,                                                                
 @fineamountOpr varchar(50) ,                                                                
 @fineamountRelop varchar(50),                                       
 @singleviolation money,                                                                
 @singlevoilationOpr varchar(50) ,                                                                
 @singleviolationrelop varchar(50),                                                        
 @doubleviolation money,                                                                
 @doublevoilationOpr varchar(50) ,                                                                
 @doubleviolationrelop varchar(50),                                                      
 @count_Letters int,                                      
 @violadesc varchar(500),                                      
 @violaop varchar(50)                                                      
    

-- DECLARING &  INITIALIZING THE LOCAL VARIABLE FOR DYNAMIC SQL                    
DECLARE @sqlquery varchar(max)
set @sqlquery = ''                                                     
    
-- GETTING DIFFERENT FILTER TYPES AND THIER VALUES IN THE LOCAL VARIABLES
-- IF ASSOCIACTED WITH THE LETTER.......
Select  @officernum=officernum,                                                        
 @officeropr=oficernumOpr,                                                        
 @tikcetnumberopr=tikcetnumberopr,                                                        
 @ticket_no=ticket_no,                                                        
 @zipcode=zipcode,                                                        
 @zipcodeopr=zipcodeLikeopr,                                                
 @singleviolation =singleviolation,                                                   
 @singlevoilationOpr =singlevoilationOpr,                                                        
 @singleviolationrelop =singleviolationrelop,                                                        
 @doubleviolation = doubleviolation,                                                        
 @doublevoilationOpr=doubleviolationOpr,                                                                
 @doubleviolationrelop=doubleviolationrelop,                                      
 @violadesc=violationdescription,                                      
 @violaop=violationdescriptionOpr ,                                  
 @fineamount=fineamount ,                     
 @fineamountOpr=fineamountOpr ,                                                     
 @fineamountRelop=fineamountRelop                     
from  tblmailer_letters_to_sendfilters                                   
where  courtcategorynum=@catnum       
and lettertype = 39                                                  
         
-- MAIN QUERY....
-- LIST DATE WISE NUMBER OF DISTINCT NON-CLIENTS
-- FOR THE SELECTED DATE RANGE ONLY 
-- INSERTING VALUES IN A TEMP TABLE....  
-- tahir 5990 06/02/2009 changed the order of where clauses for performance tuning and made changes in the code styly.  
set @sqlquery=@sqlquery+'                                            
Select distinct tva.recordid,  ta.firstname, ta.lastname,
convert(datetime, convert(varchar(10),ta.listdate,101))as mdate,      
flag1 = isnull(ta.Flag1,''N'') ,                                                        
isnull(ta.donotmailflag,0) as donotmailflag,              
count(tva.ViolationNumber_PK) as ViolCount,                                                        
isnull(sum(isnull(tva.fineamount,0)),0)as Total_Fineamount    
into #temp                                                              
FROM dallastraffictickets.dbo.tblTicketsViolationsArchive TVA                         
INNER JOIN                         
dallastraffictickets.dbo.tblTicketsArchive TA ON TVA.recordid= TA.recordid    

where 
-- ONLY FUTURE COURT COURT DATES....
-- Rab Nawaz Khan 8745  30/12/2010 ONLY FUTURE COURT COURT DATES condition is commented now statement will show the past dates also
--datediff(day,tva.courtdate,getdate())<0
-- End Rab Nawaz Khan 8745
 
-- ozair 3742 on 04/16/2008
-- EXCLUDE FAILURE TO PAY TOLL TICKETS..
--and 
charindex(''Failure to Pay toll'', isnull(tva.violationdescription,'''')) = 0
-- end ozair 3742

-- FOR THE SELECTED DATE RANGE ONLY...
and datediff(day, ta.listdate, ''' + convert(varchar,@startDate,101)  + ''') <= 0    
and datediff(day, ta.listdate, ''' + convert(varchar,@endDate , 101) + ''') >= 0   
and isnull(ta.ncoa48flag,0) = 0 
--Sabir Khan 6784 10/16/2009 EXCLUDE CASES HAVING ANY OF THE FOLLOWING STATUS...
and isnull(TVA.WebSiteCourtStatus,0) not in (9,10,11,13) 

-- Rab Nawaz Khan 8754 02/23/2011 Exclude Disposed Violations
and tva.violationstatusid <> 80

-- Abbas Shahid Khwaja 9296 05/18/2011 Screen out all cases with Offense Date more than 1 year ago and also having DISD in complainant agency
--(charindex(''CONST'', isnull(tva.complainant_agency,'''')) > 0   or charindex(''SHERIFF'', isnull(tva.complainant_agency,'''')) > 0  )
and charindex(''DISD'', isnull(tva.complainant_agency,'''')) = 0 
and [dbo].[fn_CalculateOffenseDate](tva.ticketviolationdate,getdate()) <= 0 
-- Haris Ahmed 10412 09/05/2012 Screen out all cases having DCHD in complainant agency
and charindex(''DCHD'', isnull(tva.complainant_agency,'''')) = 0  
-- End 8754
 
'  
    
-- IF PRINTING FOR ALL COURTS OF THE SELECTED COURT CATEGORY...  
if(@courtid=@catnum)    
	set @sqlquery=@sqlquery+'     
	and tva.courtlocation In (select courtid from dallastraffictickets.dbo.tblcourts where courtcategorynum = '+CONVERT(VARCHAR, @catnum)+' )'              

-- ELSE PRINTING FOR THE INDIVIDUAL SELECTED COURT OF THE CATEGORY....
else    
	set @sqlquery=@sqlquery+'     
	and tva.courtlocation=' + convert(varchar,@courtid)    
    
-- LMS FILTERS SECTION:
-----------------------------------------------------------------------------------------------------------------------------------------------------
-- IF THERE IS ANY FILTER SPECIFIED TO INCLUDE/EXCLUDE ONLY PARTICULAR OFFICER NUMBERS....                                        
if(@officernum<>'')                                                          
	set @sqlquery =@sqlquery + '      
	and  ta.officerNumber_Fk ' +@officeropr  +'('+   @officernum+')'                                                           
                                    
-- IF THERE IS ANY FILTER DEFINED FOR INCLUDE/EXCLUDE TICKET NUMBER...                                                         
if(@ticket_no<>'')                          
	set @sqlquery=@sqlquery + '     
	and ('+  dbo.Get_String_Concat_With_op(''+@ticket_no+'' , 'tva.TicketNumber_PK' ,+ ''+ @tikcetnumberopr) +')'                                                                
                                      
-- ZIP CODE FILTER......
-- INCLUDE/EXCLUDE THE SPECIFIED ZIP CODES.....                                      
if(@zipcode<>'')                                                                                
	set @sqlquery=@sqlquery+ '     
	and  ('+  dbo.Get_String_Concat_With_op(''+@zipcode+'' , 'left(ta.zipcode,2)' ,+ ''+ @zipcodeopr) +')'    
                                   
-- FINE AMOUNT FILTER.....
-- INCLUDE ONLY THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT....   
if(@fineamount<>0 and @fineamountOpr<>'not'  )                                    
	set @sqlquery =@sqlquery+ '    
    and   tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                                         

-- EXCLUDE THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT.....    
if(@fineamount<>0 and @fineamountOpr = 'not'  )                                    
	set @sqlquery =@sqlquery+ '    
    and  not tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                                         
    
set @sqlquery =@sqlquery + '     
group by                                                         
convert(datetime, convert(varchar(10),ta.listdate,101)),    
ta.flag1,                                                        
ta.donotmailflag, ta.firstname, ta.lastname,                                                       
tva.recordid    
having 1=1   
'                                    
      
-- SINGLE VIOLATION FINE AMOUNT FILTER....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE SINGLE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT
if(@singleviolation<>0 and @doubleviolation=0)                                  
	set @sqlquery =@sqlquery+ '  
	and (  '+ @singlevoilationOpr+  '  (isnull(sum(isnull(tva.fineamount,0)),0) '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and count(tva.ViolationNumber_PK)=1))  
      '                                  

-- DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...
if(@doubleviolation<>0 and @singleviolation=0 )             
	set @sqlquery =@sqlquery + '  
	and ('+ @doublevoilationOpr+  '   (isnull(sum(isnull(tva.fineamount,0)),0) '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ')and count(tva.ViolationNumber_PK)=2) )  
      '                                                      

-- BOTH SINGLE & DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE AND SINGLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...
if(@doubleviolation<>0 and @singleviolation<>0)                                  
	set @sqlquery =@sqlquery+ '  
	and ('+ @singlevoilationOpr+  '  (isnull(sum(isnull(tva.fineamount,0)),0) '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and count(tva.ViolationNumber_PK)=1))  
    and ('+  @doublevoilationOpr+  '   (isnull(sum(isnull(tva.fineamount,0)),0) '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ') and count(tva.ViolationNumber_PK)=2))  
        '                                    
                                                      
  
-- GETTING THE RESULT SET IN TEMP TABLE.....  
set @sqlquery=@sqlquery+   
'
-- EXCLUDE RECORDS HAVING LAW OFFICE, LAW FIRM OR ATTORNEY IN NAME FIELDS
delete from #temp 
where charindex(''law office'', isnull(lastname,'''') + isnull(firstname,'''') ) + charindex(''law firm'', isnull(lastname,'''') + isnull(firstname,'''') ) + charindex(''attorney'', isnull(lastname,'''') + isnull(firstname,'''') ) > 0  

delete from #temp where len(isnull(lastname,'''') + isnull(firstname,'''')) = 0
                                      
Select distinct a.recordid, a.ViolCount, a.Total_Fineamount,  a.mDate , a.Flag1, a.donotmailflag   
into #temp1  from #temp a, dallastraffictickets.dbo.tblticketsviolationsarchive tva where a.recordid = tva.recordid   
-- Abbas Shahid Khwaja 9296 05/18/2011 Screen out all cases with Offense Date more than 1 year ago and also having DISD in complainant agency
--(charindex(''CONST'', isnull(tva.complainant_agency,'''')) > 0   or charindex(''SHERIFF'', isnull(tva.complainant_agency,'''')) > 0  )
and charindex(''DISD'', isnull(tva.complainant_agency,'''')) = 0 
and [dbo].[fn_CalculateOffenseDate](tva.ticketviolationdate,getdate()) <= 0 
-- Haris Ahmed 10412 09/05/2012 Screen out all cases having DCHD in complainant agency
and charindex(''DCHD'', isnull(tva.complainant_agency,'''')) = 0  
 '                                  
  
-- VIOLATION DESCRIPTION FILTER.....
-- EXCLUDE THE LETTER IF SPECIFIED VIOLATION IS THE ONLY VIOLATION IN THE LETTER....
if(@violadesc <> '')  
begin  
	-- NOT LIKE FILTER.......  
	if(charindex('not',@violaop)<> 0 )                
		set @sqlquery=@sqlquery+'   
		and  not ( a.violcount =1  and ('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ 'like')+'))'             

	-- INCLUDE ONLY THOSE LETTER THAT SINGLE VIOLATION AND MATCHES THE SPECIFIED VIOLATION DESCRIPTION...
	-- LIKE FILTER.......
	if(charindex('not',@violaop) =  0 )                
		set @sqlquery=@sqlquery+'   
		and  ( a.violcount =1  and ('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ 'like')+'))'             
end  
  
-- EXCLUDE LETTERS THAT HAVE BEEN ALREADY RPINTED 
-- tahir 5067 11/05/2008 query optimization...
set @sqlquery=@sqlquery+'   
delete from #temp1 where recordid in (     
  select n.recordid from tblletternotes n  inner join #temp1 a     
  on a.recordid = n.recordid where lettertype = 39    
  )    

-- Babar Ahmad 8597	07/15/2011 Excluded App Day Of letter printed within previous 5 business days.
delete from #temp1 where recordid in (     
  select n.recordid from tblletternotes n  inner join #temp1 a     
  on a.recordid = n.recordid where lettertype = 38 and datediff(day, n.recordloaddate, dbo.fn_getprevbusinessday(GETDATE(),5)) <=0    
  )  
  
'




set @sqlquery=@sqlquery+                       
' -- Sabir Khan 7099 12/07/2009 exclude client cases
delete from #temp1 where recordid in (
select v.recordid from dallastraffictickets.dbo.tblticketsviolations v inner join dallastraffictickets.dbo.tbltickets t 
on t.ticketid_pk = v.ticketid_pk and t.activeflag = 1 
inner join #temp1 a on a.recordid = v.recordid
)
 ' 

-- SUMMARIZING THE TOTAL NUMBER OF LETTERS......
-- GETTING LIST DATE WISE TOTAL NUMBER OF LETTERS....
set @sqlquery=@sqlquery+'     
  
Select count(distinct recordid) as Total_Count, mDate                                                      
into #temp2                                                    
from #temp1                                                    
group by mDate                                                         
    
-- GETTTING LIST DATE WISE TOTAL NUMBER OF PRINTABLE LETTERS...
Select count(distinct recordid) as Good_Count, mDate                                                                                                  
into #temp3                                                    
from #temp1                                                        
where Flag1 in (''Y'',''D'',''S'')       
and donotmailflag =  0                                                     
group by mDate     
                                                        
-- GETTING LIST DATE WISE TOTAL NUMBER OF NON-PRINTABLE LETTERS
Select count(distinct recordid) as Bad_Count, mDate                                                      
into #temp4                                                        
from #temp1                                                    
where Flag1  not in (''Y'',''D'',''S'')                                                        
and donotmailflag = 0                                                     
group by mDate     
                     
-- GETTING LIST DATE WISE TOTAL NUMBER OF LETTERS THAT MARKED 
-- AS BLOCKED ADDRESSES.....
Select count(distinct recordid) as DonotMail_Count, mDate                                                     
 into #temp5                                                        
from #temp1            
where donotmailflag = 1       
--and flag1  in  (''Y'',''D'',''S'')                                                        
group by mDate     
           
-- OUTPUTTING THE REQURIED INFORMATION........                                                        
Select   listdate = #temp2.mDate ,    
        Total_Count,    
        isnull(Good_Count,0) Good_Count,    
        isnull(Bad_Count,0)Bad_Count,    
        isnull(DonotMail_Count,0)DonotMail_Count    
from #Temp2 left outer join #temp3     
on #temp2.mDate = #temp3.mDate    
                                                       
left outer join #Temp4    
on #temp2.mDate = #Temp4.mDate    
      
left outer join #Temp5    
on #temp2.mDate = #Temp5.mDate                                                     
      
where Good_Count > 0                  
order by #temp2.mDate desc                                                         
              
-- DROPPING THE TEMPORARY TABLES USED ABOVE.....                                                  
drop table #temp                                                    
drop table #temp1                                                    
drop table #temp2                                                        
drop table #temp3                                                    
drop table #temp4    
drop table #temp5    
'                                                        
                                                        
-- print @sqlquery    

-- EXECUTING THE DYNAMIC SQL QUERY...
exec(@sqlquery)    



SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Process_CreditCardPayment_New]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Process_CreditCardPayment_New]
GO

        
 CREATE procedure [dbo].[USP_HTS_Process_CreditCardPayment_New]              
@ticketid int,            
@InvoiceNumber int,            
@EmployeeID int,              
@ChargeAmount float  ,            
@CardNumber varchar(20),            
@CardExpdate varchar(10),            
@name varchar(40),            
@cin varchar(4) ,            
@cardtype int ,    
@rowid int = 0 output    
          
as            
            
          
 insert into tblticketscybercash          
       (ticketid,invnumber,amount,ccnum,expdate,name,employeeid,cin,CARDTYPE)            
 values(@ticketid,@InvoiceNumber,convert(money,@ChargeAmount),@CardNumber,@CardExpdate,@name,@employeeid,@cin,@cardtype)            
    
    
select @rowid = scope_identity()    
return    
    
    
      
  
       
        
        
        
        
        
        
        

----------------------------------------------------------------------------------------------      

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTS_reminderCalls_For_ValidationReport]    Script Date: 02/24/2012 14:59:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************

Alter By:	Tahir Ahmed
Date:		10/10/2008

Business Logic:
	The procedure is used by Houston Traffic Program for reminder call alert report
	under validations and in validation email. It gets all the cases that have not been
	reminded yet for their court dates in next {n} number of business days. This number of 
	business days is specified in "Show Settings" section of the report.

Input Parameters:
	N/A

Output Columns:
	fullname:		first & last name of the client
	languagespeak:	spoken language of the client specified on matter page
	BondFlag:		flag if needs bond
	comments:		reminder call comments
	shortName:		court's short name
	insurance:		insurance comments if it has an insurance violation
	trialdesc:		court setting information
	CourtDateMain:		Court Date


*****************************/


-- Noufil 4432 08/05/2008 html serial number added added for validation report
ALTER PROCEDURE [dbo].[USP_HTS_reminderCalls_For_ValidationReport]        
        
AS        
        
SET NOCOUNT ON; 
	
	-- Adil 4272 08/06/08
DECLARE @CurrentDate  DATETIME
DECLARE @StartDate    DATETIME
DECLARE @EndDate      DATETIME
DECLARE @bd           INT
DECLARE @counter      INT
SET @CurrentDate = GETDATE()
	----------------------------------------------------------
SET @counter = 0
	
SET @StartDate = @CurrentDate  
SET @EndDate = @CurrentDate  
	
SET @bd = ABS(
        (
            SELECT [dbo].[fn_GetReportValue] ('days', 84)
        )
    ) --84 is the report_id of reminder call validation report  
	
	-- tahir 4951 10/10/2008 fixed the OFF days logic.....
WHILE @bd > 0
BEGIN
    SET @enddate = @enddate + 1
    -- add one day to the end  date if current day is sunday
    IF DATEPART(dw, @enddate) = 1
    BEGIN
        SET @enddate = @enddate + 1
    END-- add tow days to the end  date if current day is saturday
    ELSE 
    IF DATEPART(dw, @enddate) = 7
    BEGIN
        SET @enddate = @enddate + 2
    END
    
    SET @bd = @bd - 1
END
	-- end 4951
	--Waqas 6509 09/18/2009 Added courtdatetime columns
SELECT ticketid,
       lastname + ', ' + firstname AS fullname,
       languagespeak,
       CASE bondflag
            WHEN 1 THEN 'Y'
            ELSE 'N'
       END AS BondFlag,
       comments,
       shortName,
       ISNULL(insurance, '') AS insurance,
       trialdesc,
       CourtDateMain,
	   CourtNumbermain, -- Sabir Khan 10012 02/24/2012 fixed record count issue.
       --Farrukh 9332 08/27/2011 ReminderCallStatus added in the report.       
       ReminderCallStatus, 
	   DialerStatus       
FROM   (
           SELECT DISTINCT
                  ticketid,
                  ticketViolationId,
                  firstname,
                  lastname,
                  languagespeak,
                  BondFlag,
                  comments,
                  STATUS,
                  shortName,
                  Insurance,
                  trialdesc,
                  dbo.fn_DateFormat(
                      ISNULL(CourtDateMain, CONVERT(DATETIME, '1/1/1900')),
                      25,
                      '/',
                      ':',
                      1
                  ) AS CourtDateMain,
				  CourtNumbermain, -- Sabir Khan 10012 02/24/2012 fixed record count issue.
                  paymentdate,
                  --Farrukh 9332 08/27/2011 ReminderCallStatus added in the report.       
                  ReminderCallStatus, 
				  DialerStatus
           FROM   (
                      SELECT DISTINCT 
                             t.TicketID_PK AS ticketid,
                             TV.TicketsViolationID AS ticketViolationId,
                             t.Firstname,
                             t.Lastname,
                             t.LanguageSpeak,
                             t.BondFlag,
                             tv.ReminderComments AS comments,
                             tv.ReminderCallStatus AS STATUS,
                             tv.CourtDateMain,
							 tv.CourtNumbermain, -- Sabir Khan 10012 02/24/2012 fixed record count issue.
                             c.ShortName AS ShortName,
                             Insurance = (
                                 SELECT TOP 1 vv.shortdesc + ISNULL(
                                            '(' + CONVERT(VARCHAR, tvv.ticketviolationdate, 101)
                                            + ')',
                                            ''
                                        )
                                 FROM   tblviolations vv
                                        INNER JOIN tblticketsviolations tvv
                                             ON  vv.violationnumber_pk = tvv.violationnumber_pk
                                 WHERE  tvv.ticketid_pk = t.ticketid_pk
                                        AND vv.shortdesc = 'ins'
                             ),
                             trialdesc = STUFF(
                                 dbo.formatdateandtimeintoshortdateandtime(tv.courtdatemain),
                                 CHARINDEX(
                                     ' ',
                                     dbo.formatdateandtimeintoshortdateandtime(tv.courtdatemain)
                                 ),
                                 1,
                                 ' @ '
                             ) + ' ' + ISNULL(cvs.shortdescription, ''),
                             paymentdate = DBO.Fn_GetMinPaymentDate(T.TicketID_PK),
                             cvs.CategoryID,
                             --Farrukh 9332 08/27/2011 ReminderCallStatus added in the report.       
                             tr.[Description] AS ReminderCallStatus, 
							 DialerStatus = CASE WHEN tv.AutoDialerReminderCallStatus=0 THEN 'Pending' ELSE adr.ResponseType END
                             
                      FROM   tblTicketsViolations TV
                             INNER JOIN dbo.tblTickets t
                                  ON  TV.TicketID_PK = t.TicketID_PK
                             INNER JOIN dbo.tblCourtViolationStatus cvs
                                  ON  TV.CourtViolationStatusIDMain = cvs.CourtViolationStatusID
                             INNER JOIN dbo.tblCourts c
                                  ON  TV.CourtID = c.Courtid
                             LEFT OUTER JOIN dbo.tblContactstype ct2
                                  ON  t.ContactType2 = ct2.ContactType_PK
                             LEFT OUTER JOIN dbo.tblContactstype ct1
                                  ON  t.ContactType1 = ct1.ContactType_PK
                             LEFT OUTER JOIN dbo.tblContactstype ct3
                                  ON  t.ContactType3 = ct3.ContactType_PK
                             LEFT OUTER JOIN dbo.tblReminderstatus tr		--Farrukh 9332 08/27/2011 ReminderCallStatus added in the report.  
								 ON tv.ReminderCallStatus = tr.Reminderid_PK
							 LEFT OUTER JOIN AutoDialerResponse adr --Haris Ahmed 10012 01/24/2012 Add Join to separate records on Remainder Call Status
								 ON	adr.ResponseID=tv.AutoDialerReminderCallStatus  
                      WHERE 
                       --Ozair 7791 07/24/2010  where clause optimized  
                             --Sabir Khan 5403 01/13/2009 Old verified status is checked with new verified status...
                             --Asad Ali 7814 05/22/2010 Cases is not displaying on Activities/Reminder Call whose Verified Case Status is updated to Jury or (Judge when court is not HMC) within 4 business days but displaying on Validations/Reminder Call Report.
                             ticketsviolationid NOT IN (SELECT 
                                                                   ticketviolationid
                                                            FROM   
                                                                   tblticketsviolationlog
                                                            WHERE  recdate 
                                                                   >= (
                                                                       @CurrentDate
                                                                       -(
                                                                           CASE 
                                                                                WHEN 
                                                                                     DATEPART(weekday, @CurrentDate) IN (5, 6) THEN 
                                                                                     4
                                                                                ELSE 
                                                                                     6
                                                                           END
                                                                       )
                                                                   )
                                                                   AND oldverifiedstatus<>newverifiedstatus
                                                                   AND newverifiedstatus IN (26, 103))
                             
                                 --Sabir Khan 4973 10/16/2008  check for excluding records whose comment date in past one week.
                             --AND DATEDIFF(DAY,dbo.GetDateFromComments(TV.ReminderComments),GETDATE()) NOT BETWEEN 0 AND 6                                 
                             --AND DATEDIFF(DAY,dbo.GetDateFromComments(TV.setCallComments),GETDATE()) NOT BETWEEN 0 AND 6
							 --Haris Ahmed 10012 01/24/2012 Comment ReminderComments checking as it is not in Activities --> Reminder Call Alert and apply same check for SetCallComments as in Activities --> Reminder Call Alert
							 AND TV.ticketID_PK NOT IN (select ticketID_PK from tblticketsviolations where setcallstatus = 1 and  
							 convert(datetime,dbo.GetDateFromComments(setcallcomments)) >= (getdate()-(case when datepart(weekday,(getdate()))=6 then 5 else 7 end)))
                             AND TV.ReminderCallStatus = 0
                                 --Yasir Kamal  6050 07/06/2009 issue fixed in logic(don't display cases having no call flag set)
                             AND NOT EXISTS (
                                     SELECT ttf.ticketid_pk
                                     FROM   tblTicketsFlag ttf
                                     WHERE  t.TicketID_PK = ttf.TicketID_PK
                                            AND ttf.FlagID = 5
                                 )
                             --9912 Farrukh 11/24/2011 exclude bond forfeiture cases
                             AND TV.TicketID_PK NOT IN (SELECT ttv.TicketID_PK FROM tblTicketsViolations ttv WHERE ttv.CourtViolationStatusID = 196)
                                 --Yasir Kamal 6050 07/10/2009 issue fixed in logic (display judge trial records if court not HMC)
                                 --Yasir Kamal 6215 07/24/2009 remove criminal court check
                             AND (
                                     cvs.categoryid IN (3, 4)
                                     OR (
                                            cvs.CategoryID = 5
                                            AND tv.CourtID NOT IN (3001, 3002, 3003)
                                        )
                             )
                             AND ISNULL(t.firmid, 3000) = 3000                                 
                             AND t.CaseTypeId = 1
                             AND t.Activeflag = 1 
                             AND DATEDIFF(DAY, tv.courtdatemain, @enddate) <= 0 --Haris Ahmed 10012 01/24/2012 @startdate changed with @enddate to display entries of one day
                             AND DATEDIFF(DAY, tv.courtdatemain, @enddate) >= 0
                  ) AS a
                  --Yasir Kamal 6050 06/23/2009 issue fixed (not display the cases hired within 4 business days of their judge or jury trial)
           WHERE  a.ticketViolationId NOT IN (SELECT ttv.TicketsViolationID
                                              FROM   tblticketsviolations 
                                                     ttv
                                              WHERE  a.CategoryID IN (4, 5)
                                                     AND DATEDIFF(DAY,paymentdate,dbo.fn_getlastbusinessday(CourtDateMain, 4)) <= 0)
                  --Yasir Kamal 6050 06/17/2009 exclude cases that hire us within 2 business days to the court date
                  AND DATEDIFF(DAY,paymentdate,dbo.fn_getlastbusinessday(CourtDateMain, 2)) > 0                      
       ) AS b
      

GROUP BY
       CourtDateMain,
	   CourtNumbermain,-- Sabir Khan 10012 02/24/2012 fixed record count issue.
       ticketid,
       lastname,
       firstname,
       languagespeak,
       bondflag,
       comments,
       shortName,
       ISNULL(insurance, ''),
       trialdesc,
       paymentdate,
--Farrukh 9332 08/27/2011 ReminderCallStatus added in the report.       
       ReminderCallStatus, DialerStatus
ORDER BY
       CourtDateMain,
       lastname,
       firstname
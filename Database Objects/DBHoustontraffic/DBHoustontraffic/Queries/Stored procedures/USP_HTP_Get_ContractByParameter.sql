﻿/****** 
Create by		: Fahad Muhammad Qureshi
Created Date	: 12/02/2008
TasK			: 5098

Business Logic : This procedure simply return the violations records for displaying conmtract reports data
				

List of Parameters:	
	@TicketID			: id against which records are updated
	@EmployeeID			: employee id of current logged in user
	@ViolationCategoryID	: For Violation category records
	@CaseTypeID			
	@CategoryID			

Column Return : 
			TicketID_PK
			Firstname
			MiddleName
			Lastname			
			TicketNumber_PK
			FirmName
			Address1
			Address2
			Ccity
			cState
			Czip
			CourtDate
			CourtNumber
			Address
			City
			Zip        
			State   
			Datetypeflag
			Activeflag       
			Languagespeak         
			SequenceNumber
			CategoryID 
			TicketViolationDate
			Description
			CaseStatus    
			courtid
			ViolationNumber_PK
			ViolationDescription
			HasPaymentPlan
			PlanAmount,
	        LumSumAmount,	       
	        @violation (Comma seperated form)
	        @refcasenumber (Comma seperated form)
			
******/
-- [dbo].[USP_HTP_Get_ContractByParameter] 227540,3991,28,2,0

ALTER PROCEDURE [dbo].[USP_HTP_Get_ContractByParameter] 
(
    @TicketID             INT,
    @EmployeeID           INT = 0,
    --Ozair 5505 02/09/2009 Violation Number Replaced with Violation Category ID and removed courtid
    @ViolationCategoryID  INT = 0,
    @CaseTypeID           INT = 0,
    @CategoryID           INT = 0
)
AS
	DECLARE @violation VARCHAR(200)
	DECLARE @refcasenumber VARCHAR(200)
	

	
	SET @violation = ''
	SET @refcasenumber = ''
	
	-- Noufil 6985 11/13/2009 GET all violations and cause number in comma separated form
	SELECT @violation = @violation + tv.[Description] + '; '
	FROM   tblTicketsViolations ttv
	       INNER JOIN tblViolations tv
	            ON  tv.ViolationNumber_PK = ttv.ViolationNumber_PK
	WHERE  ttv.TicketID_PK = @TicketID
	ORDER BY
	       ttv.TicketsViolationID
	
	SELECT @refcasenumber = @refcasenumber + ISNULL(ISNULL(ttv.casenumassignedbycourt,ttv.RefCaseNumber),'N/A') + '; '
	FROM   tblTicketsViolations ttv
	       INNER JOIN tblViolations tv
	            ON  tv.ViolationNumber_PK = ttv.ViolationNumber_PK
	WHERE  ttv.TicketID_PK = @TicketID
	ORDER BY
	       ttv.TicketsViolationID
	
	
	
	SELECT DISTINCT
	       t.TicketID_PK,
	       --Ozair 5505 02/09/2009 Violation Number Replaced with Violation Category ID 
	       dbo.Fn_HTP_Get_SpecificCauseNumber(@TicketID, @ViolationCategoryID, @CaseTypeID, @CategoryID) AS 
	       CaseDescription,
	       dbo.Fn_HTP_Get_CausaeNumberwithViolation(@ticketid) AS 
	       ViolationwithTicket,
	       t.Firstname,
	       t.MiddleName,
	       t.Lastname,
	       ISNULL(t.Address1,'') AS Address1,
	       --Yasir Kamal 7646 04/06/2010 ALR Contract Changes.
	       ISNULL(t.Address2, '') AS Address2,
	       CONVERT(VARCHAR(12),ISNULL(t.DOB,''),101) AS DOB,
	       t.City,
	       t.Zip,
	       ISNULL(t.DLNumber,'') AS DLNumber,
	       t.Calculatedtotalfee,
	       --Ozair 5505 02/09/2009 Violation Number Replaced with Violation Category ID 
	       dbo.Fn_HTP_Get_SpecificViolationDescription(@TicketID, @ViolationCategoryID, @CaseTypeID, @CategoryID) AS 
	       ViolationDescription,
	       s.State,
	       t.Activeflag,
	       ISNULL(t.LanguageSpeak, 'ENGLISH') AS Languagespeak,
	       --v.Description,
	       (
	           SELECT FirstName
	           FROM   TBLUSERS
	           WHERE  EmployeeID = @EmployeeID
	       ) AS RepFirstName,
	       (
	           SELECT LastName
	           FROM   TBLUSERS
	           WHERE  EmployeeID = @EmployeeID
	       ) AS RepLastName,
	       --Ozair 5472 01/30/2009 AG Case Contract Payment Option
	       --ISNULL(t.HasPaymentPlan, 0) AS HasPaymentPlan,
	       -- Noufil 5778 04/16/2009 Plan Base amount added with lum sum amount 
	       --CONVERT(INT, MAX(tpd.BasePrice)) AS PlanAmount,
	       --CONVERT(INT, MAX(tpd.BasePrice)) -100 AS LumSumAmount,
	       --ISNULL (tv.casenumassignedbycourt,tv.RefCaseNumber) AS causenumber,
	       -- Noufil 6985 11/13/2009 Violation and refnumber column added
	       SUBSTRING(@violation, 0, LEN(@violation)) AS violation,
	       SUBSTRING(@refcasenumber, 0, LEN(@refcasenumber)) AS refnumber,
	       --Yasir Kamal 7646 04/06/2010 ALR Contract Changes.
	       dbo.fn_FormatContactNumber_new(Convert(VARCHAR(20),isnull(t.Contact1,'')),'-1') as Contact1,ISNULL(C1.Description, '')   AS ContactType1, 
	       dbo.fn_FormatContactNumber_new(Convert(VARCHAR(20),isnull(t.Contact2,'')),'-1') as Contact2, ISNULL(C2.Description, '') AS ContactType2,    
		   dbo.fn_FormatContactNumber_new(Convert(VARCHAR(20),isnull(t.Contact3,'')),'-1') as Contact3,ISNULL(C3.Description, '') AS ContactType3,
		   ISNULL((select CASE WHEN ISNULL(ttv.ArrestDate,'1/1/1900') = '1/1/1900' THEN '' ELSE convert(varchar(12), ISNULL(ttv.ArrestDate,''),101) + '; ' END from tblTicketsViolations ttv  where ttv.TicketID_PK = t.TicketID_PK  for xml path('')),'') AS ArrestDate,
		   ISNULL((	SELECT     TOP (1) isnull(tmc.CountyName,'')
					FROM         dbo.tbl_Mailer_County AS tmc 
					RIGHT OUTER JOIN   dbo.tblCourts AS tc ON tmc.CountyID = tc.CountyId 
					RIGHT OUTER JOIN   dbo.tblTicketsViolations AS ttv 
					LEFT OUTER JOIN   dbo.tblViolations AS tv ON ttv.ViolationNumber_PK = tv.ViolationNumber_PK AND tv.CategoryID = 28 ON tc.Courtid = ttv.CourtID
					where ttv.ticketid_pk = t.TicketID_PK AND tv.CategoryID = 28),'') AS CountyOfArrest,
		   ISNULL((Select TOP 1 tbtv.ArrestingAgencyName from tblticketsviolations tbtv where tbtv.ticketid_pk = @ticketid), '') AS ALRArrestingAgency,
		   ISNULL(ALROfficerName, '') AS ALROfficerName,
		   ISNULL(ALRIntoxilyzerResult, '') AS ALRIntoxilyzerResult
		
	FROM   dbo.tblTickets t
	       INNER JOIN dbo.tblTicketsViolations tv
	            ON  t.TicketID_PK = tv.TicketID_PK
	       INNER JOIN dbo.tblCourts c
	            ON  tv.courtid = c.Courtid
	       LEFT OUTER JOIN dbo.tblState s
	            ON  t.Stateid_FK = s.StateID
	       INNER JOIN dbo.tblCourtViolationStatus cvs
	            ON  tv.CourtViolationStatusID = cvs.CourtViolationStatusID
	       INNER JOIN dbo.tblViolations v
	            ON  tv.ViolationNumber_PK = v.ViolationNumber_PK
	       INNER JOIN dbo.tblDateType dt
	            ON  cvs.CategoryID = dt.TypeID
	       INNER JOIN tblPricePlanDetail tpd
	            ON  v.CategoryID = tpd.CategoryId
	       LEFT OUTER JOIN dbo.tblContactstype c2
	            ON  t.ContactType2 = c2.ContactType_PK
	       LEFT OUTER JOIN dbo.tblContactstype c1
	            ON  t.ContactType1 = c1.ContactType_PK
	       LEFT OUTER JOIN dbo.tblContactstype c3
	            ON  t.ContactType3 = c3.ContactType_PK     
	WHERE  t.Activeflag = 1
	       AND cvs.CourtViolationStatusID NOT IN (80, 236) 
	           -- Ozair 5505 02/09/2009 removed courtid check
	       AND t.TicketID_PK = @TicketID
	       AND --Ozair 5505 02/09/2009 Violation Number Replaced with Violation Category ID 
	           (
	               (@ViolationCategoryID = 0)
	               OR (
	                      @ViolationCategoryID != 0
	                      AND v.CategoryID = @ViolationCategoryID
	                  )
	           )
	       AND (
	               (@CaseTypeID = 0)
	               OR (@CaseTypeID != 0 AND c.CaseTypeid = @CaseTypeID)
	           )
	       AND (
	               (@CategoryID = 0)
	               OR (
	                      @CategoryID != 0
	                      AND (
	                              (@CategoryID <> 0 OR (@CategoryID = 4 OR @CategoryID = 5))
	                              AND cvs.CategoryID = @CategoryID
	                          )
	                  )
	           )
	GROUP BY
	       t.TicketID_PK,
	       t.Firstname,
	       t.MiddleName,
	       t.Lastname,
	       t.Address1,
	       t.Address2,
	       t.DOB,
	       t.City,
	       t.Zip,
	       t.DLNumber,
	       t.Calculatedtotalfee,
	       s.State,
	       t.Activeflag,
	       t.LanguageSpeak,
	       t.HasPaymentPlan,
	       tv.casenumassignedbycourt,
	       tv.RefCaseNumber,
	       t.Contact1,
	       C1.Description, 
	       t.Contact2,
	       C2.Description,
	       t.Contact3,
	       C3.Description,
	       tv.ArrestDate,
	       t.ALROfficerName,
	       ALRIntoxilyzerResult 
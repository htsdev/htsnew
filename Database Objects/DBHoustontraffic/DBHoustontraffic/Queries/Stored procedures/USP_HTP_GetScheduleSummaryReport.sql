﻿ /*  
* Created By		: SAEED AHMED
* Task ID			: 7791  
* Created Date		: 05/29/2010  
* Business logic	: This procedure is used get Report summary.
*					: 
* Parameter List  
* @report			: Represents report type, 1=Alert,2=Report
* @ReportIds		: Represents comma seperated report ids.
*
*/
Create procedure [dbo].[USP_HTP_GetScheduleSummaryReport]
@report VARCHAR(50) = '1',
@ReportIds VARCHAR(MAX)='',
@SessionId VARCHAR(500)=''		
as 
SELECT id, Reporttitle as [Report Name],alertcount as [Alert],pendingcount as [Pending],totalcount as [Total]
from summaryreport where (@ReportIds <> '' AND sessionid=@SessionId  AND report_type= @report AND isscheduled=1 AND reportid in (select svalue from dbo.splitstring(@ReportIds, ',')))
  
  
GO
GRANT EXECUTE ON dbo.USP_HTP_GetScheduleSummaryReport TO dbr_webuser
GO
  
    
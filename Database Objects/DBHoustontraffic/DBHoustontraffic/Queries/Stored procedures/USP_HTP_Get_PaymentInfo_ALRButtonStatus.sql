﻿/******   
//Waqas 5864 07/03/2009 
Business Logic : This procedure return the data on which we display/implements check on report buttons under billing page for ALR
  
List of Parameters:   
 @TicketID   : Id against which records are fetched  
  
  
Column Return :   
  AttorenyFirstName,
  AttorneyLastName,
  AttorneyBarCardNumber,
  ALRIsIntoxilyzerTakenFlag,
  ALRArrestingAgency
  ALROfficerName
  ALRBTOFirstName
  ALRBTOLastName
  ALRBTSFirstName
  ALRBTSLastName
  ALRObservingOfficerName,
 ALRObservingOfficerAddress, 
 ALRObservingOfficerCity,
 ALRObservingOfficerState, 
 ALRObservingOfficerZip  
  
******/  
--USP_HTP_Get_PaymentInfo_ALRButtonStatus 191204
alter procedure [dbo].[USP_HTP_Get_PaymentInfo_ALRButtonStatus]   
 (        
 @TicketID  INT  
 )        
           
as           
        
 SELECT   DISTINCT
	ISNULL(tf.AttorenyFirstName, '') AS AttorenyFirstName, 
	ISNULL(tf.AttorneyLastName, '') AS AttorneyLastName, 
	ISNULL(tf.AttorneyBarCardNumber, '') AS AttorneyBarCardNumber, 
	ISNULL(tt.ALRIsIntoxilyzerTakenFlag, '') AS ALRIsIntoxilyzerTakenFlag,
	ISNULL(tt.ALRIntoxilyzerResult, '') AS ALRIntoxilyzerResult,
	--ISNULL(tt.ALRArrestingAgency, '') AS ALRArrestingAgency,
	ISNULL((Select TOP 1 tbtv.ArrestingAgencyName from tblticketsviolations tbtv where tbtv.ticketid_pk = @ticketid), '') AS ALRArrestingAgency,
	ISNULL(tt.ALROfficerName, '') AS ALROfficerName,
	ISNULL(tt.ALROfficerAddress, '') AS ALROfficerAddress, -- Waqas 6342 08/13/2009 ALR officer adress
	ISNULL(tt.ALROfficerCity, '') AS ALROfficerCity,
	ISNULL(tt.ALROfficerStateID, '') AS ALROfficerStateID, -- Waqas 6342 08/13/2009 ALR officer state
	ISNULL(tt.ALROfficerZip, '') AS ALROfficerZip,
	ISNULL(tt.ALRBTOFirstName, '') AS ALRBTOFirstName,    
    ISNULL(tt.ALRBTOLastName, '') AS ALRBTOLastName,    
    ISNULL(tt.ALRBTSFirstName, '') AS ALRBTSFirstName,    
    ISNULL(tt.ALRBTSLastName, '') AS ALRBTSLastName,
    -- Waqas 6342 08/13/2009 ALR observing officer 
    ISNULL(tt.IsALRArrestingObservingSame, '0') AS IsALRArrestingObservingSame,
    ISNULL(tt.ALRObservingOfficerName, '') AS ALRObservingOfficerName,
	ISNULL(tt.ALRObservingOfficerAddress, '') AS ALRObservingOfficerAddress, 
	ISNULL(tt.ALRObservingOfficerCity, '') AS ALRObservingOfficerCity,
	ISNULL(tt.ALRObservingOfficerState, '') AS ALRObservingOfficerState, 
	ISNULL(tt.ALRObservingOfficerZip, '') AS ALRObservingOfficerZip     
	
  FROM   tblTickets tt  
        INNER JOIN tblFirm tf  
             ON  tt.CoveringFirmID = tf.FirmID  
 WHERE  tt.TicketID_PK = @TicketID  
 
 GO
 
 GRANT EXECUTE ON dbo.[USP_HTP_Get_PaymentInfo_ALRButtonStatus] TO dbr_webuser
  
 
﻿
/******   
Alter by:  Rab Nawaz Khan  
  
Business Logic : this procedure is used to show All The Reocords that are under the Arraignment Waiting Status and under HMC 	  
  
List of Parameters:   
 @fromDate : Start Date
 @toDate : End Date
 @showAllFlag : 0 or 1
  
List of Columns:    
 ticketid :
 ticketno :  
 lastname : 
 firstname : 
 dlnumber :
 dob : 
 midnum : 
 causeno : 
 courtdate :  
 CRTDATE : 
 status :  
 ticketsviolationid :
 Time :  
 WaitingDate :  
   
******/            

ALter  PROCEDURE [dbo].[USP_HTS_ARRAIGNMENT_WAITING_REPORT]                                         
@fromDate datetime,                        
@toDate datetime,                        
@showAllFlag bit                                        
                                        
as                                  
                                  
begin                            
                        
	declare @sqlQuery varchar(8000)                        
                        
	set @sqlQuery = '                                     
	Select                                         
    t.ticketid_pk as Ticketid,                                      
    tV.REFCASENUMBER AS TICKETNO,                                          
    upper(T.LASTNAME) as lastname,                                        
    upper(T.FIRSTNAME) as firstname,                       
	t.dlnumber,                                     
    T.DOB as DOB,                                        
    T.MIDNUM,                                        
    TV.CASENUMASSIGNEDBYCOURT AS CAUSENO,                                        
    TV.COURTDATEMAIN AS COURTDATE,                                        
    COURTDATEMAIN as CRTDATE,                              
    TCV.SHORTDESCRIPTION as status,                                        
    tv.ticketsviolationid ,      
    (case when isnull((select top 1 ArrignmentWaitingdate from tblticketsviolations where ticketid_pk =tv.ticketid_pk and ViolationNumber_pk =tv.ViolationNumber_pk),0) <> 0     
	then (select top 1 ArrignmentWaitingdate from tblticketsviolations where ticketid_pk =tv.ticketid_pk and ViolationNumber_pk =tv.ViolationNumber_pk)    
	else tvl.recdate end) as WaitingDate,     
    datediff(day,tvl.recdate,getdate()) as Time,              
	--ozair 4073 05/26/2008 incorporating new structure with old one
	(isnull((SELECT COUNT(ticketid)            
	FROM Tblwaitinglogs w inner join tblbatchsummary bs on          
    bs.batchid=w.batchid            
	where t.ticketid_pk = w.ticketid and bs.docpicflag=1           
	GROUP BY  ticketid),0)+isnull((select count(bt.ticketid_fk)
	from tbl_htp_documenttracking_batchtickets bt
	where t.ticketid_pk = bt.ticketid_fk
	and bt.VerifiedDate is not null
	group by bt.ticketid_fk),0))  as doccount ,
	(case when isnull((select top 1 isnull(b.docpicflag,0)             
	from tblwaitinglogs w inner join tblbatchsummary b 
	on w.batchid=b.batchid            
	where ticketid=t.ticketid_pk),0)+ isnull((select count(bt.ticketid_fk)
	from tbl_htp_documenttracking_batchtickets bt
	where t.ticketid_pk = bt.ticketid_fk
	and bt.VerifiedDate is not null
	group by bt.ticketid_fk),0) > 0 then 1 else 0 end) as showscan,   
	--end ozair 4073      
	CASE WHEN TF.FlagID IS NULL THEN '''' ELSE ''N'' END AS FLAGID
	FROM                                       
    TBLTICKETS T INNER JOIN                                         
    TBLTICKETSVIOLATIONS TV ON                                         
    T.TICKETID_PK=TV.TICKETID_PK INNER JOIN                                         
    TBLCOURTVIOLATIONSTATUS TCV ON                                         
    TV.COURTVIOLATIONSTATUSIDMAIN=TCV.COURTVIOLATIONSTATUSID inner join                                       
    tblcourts tc on                                      
    tv.courtid=tc.courtid left outer join                          
    tblticketsviolationlog tvl on                           
    tv.ticketsviolationid=tvl.ticketviolationid                        
    and  tvl.recdate=(select max(recdate) from tblticketsviolationlog where ticketviolationid= tvl.ticketviolationid     
    and tvl.newverifiedstatus = 201 and newverifiedstatus <> oldverifiedstatus)    
	LEFT OUTER   join tblTicketsFlag TF on      
	TV.TICKETID_PK = TF.TICKETID_PK       
	AND TF.FlagID = 15  '        
        
	set @sqlquery = @sqlquery +        
	'WHERE                                        
    tV.courtviolationstatusidmain  = 201 
    and tv.courtid IN(3001,3002,3003,3075) ' -- Rab Nawaz Khan 10327 06/14/2012 Added the HMC-W Court in Arraignment Waiting Setting Report. . .
           
	if @showAllFlag = 0                        
	begin                        
	set @sqlquery = @sqlquery + '         
	AND tvl.recdate BETWEEN '''+ Convert(varchar,@fromDate,101)+ ' 00:00:00:000''' +' AND '''+ Convert(varchar,@ToDate,101)+ ' 23:59:59:998'''                           
	end          
                                          
	set @SqlQuery = @SqlQuery +' order by [time] desc   ,lastname,firstname'                 
    print @SqlQuery                                  
	exec ( @SqlQuery  )                        
end            
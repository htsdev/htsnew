﻿ /***********************************************************************************   
 * Created By	  : Farrukh Iftikhar
 * Created Date   : 07/28/2011  
 * Task Id		  : 9332  
 *   
 * Business Logic :	This procedure is use to get all call back request against a particular ticket 
 *   
 * Parameter List :  
 *      @TicketID :	ticket number.  
       
 ********************************************************************************/ 

ALTER PROCEDURE [dbo].[USP_HTP_Get_Activities] 
	 @TicketID varchar(12)
AS
BEGIN
	SELECT DISTINCT adco.CallID,adco.EventTypeID_FK AS EventTypeID ,adco.CallDateTime,adco.TicketID_FK AS TicketID,
	adco.IsBond,adco.ContactNumber,adco.ResponseID_FK AS ResponseID,adr.ResponseType,
	adecs.EventTypeName,tv.CourtDateMain,
	CASE WHEN adecs.EventTypeID = 2
	THEN (SELECT tr.[Description] FROM tblReminderstatus tr WHERE tr.Reminderid_PK = tv.ReminderCallStatus)
	WHEN adecs.EventTypeID = 1
	THEN (SELECT tr.[Description] FROM tblReminderstatus tr WHERE tr.Reminderid_PK = tv.SetCallStatus)
	END AS CallBackStatus,
	dbo.fn_hts_get_TicketsViolationIds (@TicketID,tv.CourtDateMain) AS TicketViolationIds 
	FROM AutoDialerCallOutcome adco
		INNER JOIN  AutoDialerResponse adr ON 
			adr.ResponseID = adco.ResponseID_FK
		INNER JOIN AutoDialerEventConfigSettings adecs ON 
			adecs.EventTypeID = adco.EventTypeID_FK
		INNER JOIN tblTicketsViolations tv ON
			adco.TicketID_FK = tv.TicketID_PK
		
	WHERE adco.TicketID_FK = @TicketID
	AND tv.TicketsViolationID IN (SELECT TOP 1 TicketsViolationID FROM tblTicketsViolations WHERE TicketID_PK = @TicketID)
	ORDER BY adco.CallDateTime desc

END






GO
GRANT EXECUTE ON [USP_HTP_Get_Activities] TO dbr_webuser
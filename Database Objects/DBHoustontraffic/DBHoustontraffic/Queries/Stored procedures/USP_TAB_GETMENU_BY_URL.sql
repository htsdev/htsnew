SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_TAB_GETMENU_BY_URL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_TAB_GETMENU_BY_URL]
GO


CREATE PROCEDURE [dbo].[USP_TAB_GETMENU_BY_URL]
(@URL varchar(1000),@id int = null)
 AS

	  select id,menuid from tbl_tab_submenu where url=@url and ID =COALESCE(@id,ID) and  active=1    

GO

grant execute on [dbo].[USP_TAB_GETMENU_BY_URL] to dbr_webuser
go


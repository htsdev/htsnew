if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_mailer_send_Email]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_mailer_send_Email]
GO

       
      
    -- usp_mailer_send_Email 'test email', 'test email'  ,1    
CREATE   procedure [dbo].[usp_mailer_send_Email](      
 @subject varchar(200),      
 @body varchar(2000),    
 @SendOthers bit=0      
 )      
      
as       
      
declare @to varchar(200)        
       

select @to = 'LMSLetterPrint@sullolaw.com'        
--select @to = 'tahir@lntechnologies.com'        

EXEC msdb.dbo.sp_send_dbmail                             
		@profile_name = 'Traffic System',                             
		@recipients =  @to ,                            
		@subject = @Subject,                             
		@body = @body,                            
		@body_format = 'HTML' ; 
      
      
      
GO
grant execute on [dbo].[usp_mailer_send_Email] to dbr_webuser
GO


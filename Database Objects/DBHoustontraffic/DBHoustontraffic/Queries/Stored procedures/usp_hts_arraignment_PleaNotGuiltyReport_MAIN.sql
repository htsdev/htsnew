﻿/******   
Alter by:  Syed Muhammad Ozair  
  
Business Logic : this procedure is used to Insert the information in document tracking batch tickets table and
				 also insert the entry in case history for the related cases. it also gets the required information 
				 that will be displayed under Arr summary report.			  
  
List of Parameters:   
 @TVIDS : ticketsviilationids
 @DocumentBatchID : docuemnt id 
 @updateflag : 
 @employee : employee id of current logged in user  
  
List of Columns:    
 ticketid :
 ticketno : 
 lastname : 
 firstname :
 dob : 
 midnum: 
 causeno : 
 courtdate :
 description :  
 status :  
 courdate :
 roomno :
 dlnumber : 
 CRTDATE :  
 ticketsviolationid :
 docid : document id 
   
******/

ALTER PROCEDURE [dbo].[usp_hts_arraignment_PleaNotGuiltyReport_Main]
	@TVIDS
AS
	VARCHAR(MAX),
	@DocumentBatchID AS INT, 
	@UpdateFlag AS INT, 
	@employee AS INT 
	
	AS                            
	
	DECLARE @TBLTEMPTVID TABLE (TICKETVIOLATIONID INT)                   
	DECLARE @temp TABLE 
	        (
	            ticketid INT,
	            ticketno VARCHAR(20),
	            lastname VARCHAR(20),
	            firstname VARCHAR(20),
	            dob VARCHAR(12),
	            midnum VARCHAR(20),
	            causeno VARCHAR(20),
	            DESCRIPTION VARCHAR(100),
	            STATUS VARCHAR(20),
	            courtdate DATETIME,
	            roomno VARCHAR(3),
	            dlnumber VARCHAR(30),
	            crtdate VARCHAR(12),
	            ticketsviolationid INT,
	            docid INT
	        )                 
	
	IF @TVIDS = ''
	BEGIN
	    INSERT INTO @temp
	    SELECT DISTINCT 
	           t.ticketid_pk AS TicketID,
	           tV.REFCASENUMBER AS TICKETNO,
	           UPPER(T.LASTNAME) AS lastname,
	           UPPER(T.FIRSTNAME) AS firstname,
	           CONVERT(VARCHAR(12), T.DOB, 101) AS DOB,
	           T.MIDNUM,
	           TV.CASENUMASSIGNEDBYCOURT AS CAUSENO,
	           UPPER(v.description) AS DESCRIPTION,
	           TCV.SHORTDESCRIPTION AS STATUS,
	           TV.COURTDATEMAIN AS courtdate,
	           tv.courtnumbermain AS roomno,
	           t.dlnumber,
	           CONVERT(VARCHAR(12), TV.COURTDATEMAIN, 101) AS CRTDATE,
	           tv.ticketsviolationid,
	           @DocumentBatchID AS DOCID
	    FROM   TBLTICKETS T
	           INNER JOIN TBLTICKETSVIOLATIONS TV
	                ON  T.TICKETID_PK = TV.TICKETID_PK
	           INNER JOIN TBLCOURTVIOLATIONSTATUS TCV
	                ON  TV.COURTVIOLATIONSTATUSIDMAIN = TCV.COURTVIOLATIONSTATUSID
	           INNER JOIN tblviolations v
	                ON  tv.violationnumber_pk = v.violationnumber_pk
	           LEFT OUTER JOIN tblticketsviolationlog tvl
	                ON  tv.ticketsviolationid = tvl.ticketviolationid
	                AND tvl.recdate = (
	                        SELECT MAX(recdate)
	                        FROM   tblticketsviolationlog
	                        WHERE  ticketviolationid = tvl.ticketviolationid
	                    )
	    WHERE  T.ticketid_pk IN (SELECT DISTINCT 
	                                    t.ticketid_pk
	                             FROM   TBLTICKETS T
	                                    INNER JOIN TBLTICKETSVIOLATIONS TV
	                                         ON  T.TICKETID_PK = TV.TICKETID_PK
	                                    INNER JOIN TBLCOURTVIOLATIONSTATUS TCV
	                                         ON  TV.COURTVIOLATIONSTATUSIDMAIN = 
	                                             TCV.COURTVIOLATIONSTATUSID
	                                    INNER JOIN tblviolations v
	                                         ON  tv.violationnumber_pk = v.violationnumber_pk
	                                    INNER JOIN tblcourts tc
	                                         ON  tv.courtid = tc.courtid
	                                    LEFT OUTER JOIN tblticketsviolationlog 
	                                         tvl
	                                         ON  tv.ticketsviolationid = tvl.ticketviolationid
	                                         AND tvl.recdate = (
	                                                 SELECT MAX(recdate)
	                                                 FROM   
	                                                        tblticketsviolationlog
	                                                 WHERE  ticketviolationid = 
	                                                        tvl.ticketviolationid
	                                             )
	                             WHERE  tV.courtviolationstatusidmain = 201)
	           AND t.activeflag = 1
	           AND tv.courtid IN (3001, 3002, 3003, 3075) -- Rab Nawaz Khan 10327 06/14/2012 Added the HMC-W Court in Arraignment Setting Report. . .
	    ORDER BY
	           courtdate,
	           lastname,
	           firstname                          
	    
	    IF @UpdateFlag = 1
	    BEGIN
	        DECLARE @temp1 TABLE 
	                (
	                    COUNTER INT IDENTITY(1, 1) NOT NULL,
	                    ticketsviolationid INT
	                )     
	        
	        INSERT INTO @temp1
	        SELECT ticketsviolationid
	        FROM   #Temp
	        
	        DECLARE @loopcounter AS INT            
	        SET @loopcounter = (
	                SELECT COUNT(*)
	                FROM   @temp1
	            )
	        
	        WHILE @loopcounter >= 1
	        BEGIN
	            UPDATE tblticketsviolations
	            SET    courtviolationstatusidmain = 201
	            WHERE  ticketsviolationid = (
	                       SELECT ticketsviolationid
	                       FROM   @temp1
	                       WHERE  COUNTER = @loopcounter
	                   )
	            
	            SET @loopcounter = @loopcounter -1
	        END
	    END        
	    
	    SELECT *
	    FROM   @temp
	END
	ELSE
	BEGIN
	    INSERT INTO @TBLTEMPTVID
	    SELECT *
	    FROM   dbo.sap_string_param(@TVIDS)                
	    
	    INSERT INTO @temp
	    SELECT DISTINCT 
	           t.ticketid_pk AS TicketID,
	           tV.REFCASENUMBER AS TICKETNO,
	           UPPER(T.LASTNAME) AS lastname,
	           UPPER(T.FIRSTNAME) AS firstname,
	           CONVERT(VARCHAR(12), T.DOB, 101) AS DOB,
	           T.MIDNUM,
	           TV.CASENUMASSIGNEDBYCOURT AS CAUSENO,
	           UPPER(v.description) AS DESCRIPTION,
	           TCV.SHORTDESCRIPTION AS STATUS,
	           TV.COURTDATEMAIN AS courtdate,
	           tv.courtnumbermain AS roomno,
	           t.dlnumber,
	           CONVERT(VARCHAR(12), TV.COURTDATEMAIN, 101) AS CRTDATE,
	           tv.ticketsviolationid,
	           @DocumentBatchID AS DOCID
	    FROM   TBLTICKETS T
	           INNER JOIN TBLTICKETSVIOLATIONS TV
	                ON  T.TICKETID_PK = TV.TICKETID_PK
	           INNER JOIN TBLCOURTVIOLATIONSTATUS TCV
	                ON  TV.COURTVIOLATIONSTATUSIDMAIN = TCV.COURTVIOLATIONSTATUSID
	           INNER JOIN tblviolations v
	                ON  tv.violationnumber_pk = v.violationnumber_pk
	           LEFT OUTER JOIN tblticketsviolationlog tvl
	                ON  tv.ticketsviolationid = tvl.ticketviolationid
	                AND tvl.recdate = (
	                        SELECT MAX(recdate)
	                        FROM   tblticketsviolationlog
	                        WHERE  ticketviolationid = tvl.ticketviolationid
	                    )
	    WHERE  TV.Ticketsviolationid IN (SELECT DISTINCT 
	                                            tv.Ticketsviolationid
	                                     FROM   TBLTICKETS T
	                                            INNER JOIN TBLTICKETSVIOLATIONS 
	                                                 TV
	                                                 ON  T.TICKETID_PK = TV.TICKETID_PK
	                                            INNER JOIN 
	                                                 TBLCOURTVIOLATIONSTATUS TCV
	                                                 ON  TV.COURTVIOLATIONSTATUSIDMAIN = 
	                                                     TCV.COURTVIOLATIONSTATUSID
	                                            INNER JOIN tblviolations v
	                                                 ON  tv.violationnumber_pk = 
	                                                     v.violationnumber_pk
	                                            INNER JOIN tblcourts tc
	                                                 ON  tv.courtid = tc.courtid
	                                            LEFT OUTER JOIN 
	                                                 tblticketsviolationlog tvl
	                                                 ON  tv.ticketsviolationid = 
	                                                     tvl.ticketviolationid
	                                                 AND tvl.recdate = (
	                                                         SELECT MAX(recdate)
	                                                         FROM   
	                                                                tblticketsviolationlog
	                                                         WHERE  
	                                                                ticketviolationid = 
	                                                                tvl.ticketviolationid
	                                                     )
	                                     WHERE  TV.TICKETSVIOLATIONID IN (SELECT 
	                                                                             TICKETVIOLATIONID
	                                                                      FROM   
	                                                                             @TBLTEMPTVID)
	                                            AND tv.courtviolationstatusidmain = 
	                                                201)
	           AND t.activeflag = 1
	           AND tv.courtid IN (3001, 3002, 3003, 3075) -- Rab Nawaz Khan 10327 06/14/2012 Added the HMC-W Court in Arraignment Setting Report. . .
	    ORDER BY
	           courtdate,
	           lastname,
	           firstname                   
	    
	    IF @UpdateFlag = 1
	    BEGIN
	        DECLARE @temp2 TABLE 
	                (COUNTER INT IDENTITY(1, 1) NOT NULL, TICKETVIOLATIONID INT)     
	        
	        INSERT INTO @temp2
	        SELECT TICKETVIOLATIONID
	        FROM   @TBLTEMPTVID
	        
	        DECLARE @loopcounter1 AS INT            
	        SET @loopcounter1 = (
	                SELECT COUNT(*)
	                FROM   @temp2
	            )
	        
	        WHILE @loopcounter1 >= 1
	        BEGIN
	            UPDATE tblticketsviolations
	            SET    courtviolationstatusidmain = 201
	            WHERE  ticketsviolationid = (
	                       SELECT TICKETVIOLATIONID
	                       FROM   @temp2
	                       WHERE  COUNTER = @loopcounter1
	                   )
	            
	            SET @loopcounter1 = @loopcounter1 -1
	        END
	    END         
	    
	    SELECT *
	    FROM   @temp
	END         
	
	IF @DocumentBatchID <> 0
	BEGIN
	    DECLARE @docid AS VARCHAR(20)        
	    SET @docid = CONVERT(VARCHAR(20), @DocumentBatchID, 101)              
	    
	    INSERT INTO tbl_htp_documenttracking_batchTickets
	      (
	        BatchID_FK,
	        TicketID_FK
	      )
	    SELECT DISTINCT DOCID,
	           ticketid
	    FROM   @temp
	    
	    INSERT INTO tblTicketsNotes
	      (
	        TicketID,
	        SUBJECT,
	        EmployeeID
	      )
	     -- Noufil 8039 07/22/2010 Rename report tile to Arraignment Setting from Arraingment waiting.
	    SELECT TicketID_FK,
	           '<a href="javascript:window.open(''../quickentrynew/PreviewPrintHistory.aspx?filename=' + 'ARR_WAITING-' + @docid + ''');void('''');"''>Arraignment Settings DOC ID : ' + @docid + '</a>',
	           @employee
	    FROM   tbl_htp_documenttracking_batchTickets
	    WHERE  BatchID_FK = @DocumentBatchID
	END
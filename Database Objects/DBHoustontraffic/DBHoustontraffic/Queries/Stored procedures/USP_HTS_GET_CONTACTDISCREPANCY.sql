SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_GET_CONTACTDISCREPANCY]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_GET_CONTACTDISCREPANCY]
GO

CREATE PROCEDURE [dbo].[USP_HTS_GET_CONTACTDISCREPANCY]      
      
AS      
      
      
      
SELECT DISTINCT       
T.TicketID_pk,ISNULL(T.Lastname, '') + ' ' + ISNULL(T.Firstname, '') AS ClientName,     
--------------------------- Added By Zeeshan Ahmed --------------------------------------    
ISNULL(T.Address1,'') +  case when (T.Address2 +',') != ',' then  ',' + T.Address2  else '' end  as Address1 ,    
ISNULL(T.Address2, '')as Address2, T.City ,TS.State as State ,T.Zip as zipcode ,      
-----------------------------------------------------------------------------------------    
ISNULL(T.Midnum, '') AS MidNum, ISNULL(T.Contact1, '') AS Contact1,     
ISNULL(C1.Description, '')   AS ContactType1, ISNULL(T.Contact2, '') AS Contact2, ISNULL(C2.Description, '') AS ContactType2,    
ISNULL(T.Contact3, '') AS Contact3,ISNULL(C3.Description, '') AS ContactType3      
      
FROM         dbo.tblTickets AS T INNER JOIN      
                      dbo.tblTickets AS TT ON T.Midnum = TT.Midnum AND ISNULL(T.Address1, '') + ISNULL(T.Address2, '') <> ISNULL(TT.Address1, '')       
                      + ISNULL(TT.Address2, '') INNER JOIN      
                      dbo.tblTicketsViolations ON T.TicketID_PK = dbo.tblTicketsViolations.TicketID_PK LEFT OUTER JOIN      
                      dbo.tblContactstype AS C1 ON T.ContactType1 = C1.ContactType_PK LEFT OUTER JOIN      
                      dbo.tblContactstype AS C2 ON T.ContactType2 = C2.ContactType_PK LEFT OUTER JOIN      
                      dbo.tblContactstype AS C3 ON T.ContactType3 = C3.ContactType_PK      
        join tblstate ts On T.StateId_fk = ts.stateid inner join tblcourtviolationstatus tcs on  
     dbo.tblTicketsViolations.courtviolationstatusidmain=tcs.courtviolationstatusid inner join tbldatetype dt on  
     tcs.categoryid=dt.typeid   
WHERE     (dbo.tblTicketsViolations.CourtID IN (3001, 3002, 3003)) and (dt.typeid in (2,4,5,3)) and (dbo.tblTicketsViolations.courtdatemain > getdate())  
  
--Added by M.Azwar Alam on 13 Sep 2007---  
--Only Cases in ARR,Jury,Judge,Pretrial and that have future courtdate will show--  
      
ORDER BY Midnum      



  

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


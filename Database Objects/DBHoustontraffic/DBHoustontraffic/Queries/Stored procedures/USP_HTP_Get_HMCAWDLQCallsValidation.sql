﻿/******     
Create by  : Muhammad Nasir  
Created Date : 03/25/2008    
TasK   : 5690   
    
Business Logic :     
   This procedure simply return the records having contact status "Pending" and  which have Verified status in Arraigment, Arraigment waiting and auto status is in DLQ or blank and court should be inside court(HMC) .      
       
List of Parameters:     
   None    
       
Column Return :     
   TicketID    
   Firstname    
   Lastname       
   TicketNumber    
   CourtDate    
   CourtTime    
   CourtRoom    
   CourtLocation    
   ContactStatus          
******/    
CREATE PROCEDURE [dbo].[USP_HTP_Get_HMCAWDLQCallsValidation]
AS
	SELECT DISTINCT 
	       t.TicketID_PK,
	       ISNULL(tv.CaseNumAssignedByCourt, '') AS CauseNumber,
	       ISNULL(tv.refCaseNumber, '') AS TicketNo,
	       t.Lastname,
	       t.Firstname,
	       cvs.shortdescription AS CourtStatus,
	       ISNULL(tv.HMCAWDLQCallStatus, 0) AS STATUS,
	       dbo.fn_DateFormat(tv.CourtDateMain, 1, '/', ':', 1) AS courtdate,
	       dbo.fn_DateFormat(tv.CourtDateMain, 26, '/', ':', 1) AS courttime,
	       ISNULL(tv.CourtNumbermain, '0') AS CourtRoom,
	       c.shortname AS courtloc,
	       rs.Description AS contactstatus
	FROM   tblTicketsViolations TV
	       INNER JOIN dbo.tblTickets t
	            ON  TV.TicketID_PK = t.TicketID_PK
	       INNER JOIN dbo.tblCourtViolationStatus cvs
	            ON  TV.CourtViolationStatusIDMain = cvs.CourtViolationStatusID
	       INNER JOIN dbo.tblCourts c
	            ON  TV.CourtID = c.Courtid
	       INNER JOIN dbo.tblticketspayment TP
	            ON  TV.TicketID_PK = TP.TicketID
	       INNER JOIN tblReminderStatus rs
	            ON  rs.Reminderid_pk = ISNULL(tv.HMCAWDLQCallStatus, 0)
	WHERE  t.CaseTypeId = 1
	       AND (cvs.CategoryID = 2 OR cvs.CourtViolationStatusID IN (201))
	       AND (
	               tv.CourtViolationStatusID = 146
	               OR ISNULL(tv.CourtViolationStatusID, 0) = 0
	           )
	       AND ISNULL(tv.HMCAWDLQCallStatus, 0) = 0
	       AND c.CourtID IN (3001, 3002, 3003)
	       AND t.activeflag = 1
	ORDER BY
	       t.lastname,
	       t.firstname
GO



GRANT EXEC ON dbo.[USP_HTP_Get_HMCAWDLQCallsValidation] TO dbr_webuser
GO 
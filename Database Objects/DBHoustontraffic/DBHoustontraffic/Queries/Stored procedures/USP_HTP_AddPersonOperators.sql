﻿ USE TrafficTickets
GO

/******    
* Created By :   Kamran Shahid
* Create Date :   11/05/2010   
* Task ID :    10447  
* Business Logic :  This procedure is used to Add Texas Transportation Person Operators 
* List of Parameter : @IncidentNumber  
*     : @OperatorFirstName   
     : @OperatorLastName  
    
* Column Return :       
******/
-- USP_HTP_AddDoctorDetail   
CREATE PROC [dbo].[USP_HTP_AddPersonOperators] 
@IncidentNumber    VARCHAR(11),
@OperatorFirstName VARCHAR(50),
@OperatorLastName  VARCHAR(50),
@CrashID           VARCHAR(50)
AS
INSERT INTO tblTexasTransportationPersonOperators
  (
    [IncidentNumber],
    [OperatorFirstName],
    [OperatorLastName],
    [RecordInsertedDate],
    CrashID
  )
VALUES
  (
    @IncidentNumber,
    @OperatorFirstName,
    @OperatorLastName,
    GETDATE(),
    @CrashID
  )

GO
GRANT EXECUTE ON [dbo].[USP_HTP_AddPersonOperators] TO dbr_webuser 

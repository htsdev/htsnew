-- =============================================
-- Author:		Tahir Ahmed
-- Create date: 07/21/2008
-- Description:	The stored procedure is used by HTP /Activies /Donot Mail Update application.
--				It is used to search record by ticket number. User can mark the records appearing
--				in the results to remove the person from our mailing lists 
--
-- List Of Parameters:
--	@TicketNumber:	case number to search for the person.

-- List of output columns:
--	dbid:			Database ID related to the record
--	recordid:		unique identification of record in the above specified database
--	casetype:		description for the matter type (HTP, DTP, Civil, Criminal)
--	ticketnumber:	Ticket number associated with the case
--	firstname:		first name of the person
--	lastname:		last name of the person
--	address1:		mailing address of the person 
--	city:			city associated with the mailing address
--	state:			name of the state associated with the mailing address
--	zipcode:		zipcode associated with the mailing address
--	nomailflag		flag if already marked as do not mail flag
--  Insertdate:     date when case was marked for do not mail

--
--
-- =============================================

 
--   USP_HTS_CheckAll_NOMAILFLAG_ChkByTicNum     '199658123' 
                
ALTER procedure [dbo].[USP_HTS_CheckAll_NOMAILFLAG_ChkByTicNum]   
                    
@TicketNumber varchar(20)                            
                    
as                    

--Yasir Kamal 6144 07/14/2009 Insertdate column added.  
--Sabir Khan 6167 07/17/2009 remove ticketnumber field for removing duplication in clients...  
--Yasir Kamal 6170 08/13/2009 add DoNotMailUpdateDate column
declare @DataSet table(Sno int identity, dbid int,DoNotMailUpdateDate VARCHAR(10),recordid int, casetype varchar(50),FirstName varchar(20),lastname varchar(20),Address1 varchar(50),city varchar(20),state varchar(15),ZipCode varchar(12), nomailflag bit)               



-- GETTING HOUSTON TRAFFIC CASES.....
--Sabir Khan 6167 07/17/2009 remove ticketnumber field for removing duplication in clients...
insert into @DataSet(dbid,DoNotMailUpdateDate, recordid, casetype, firstname,lastname,address1,city,state,zipcode, nomailflag)                 
select distinct 1,ISNULL(CONVERT(VARCHAR(10),ta.DoNotMailUpdateDate,101),'Not Set'), ta.recordid, 'Houston Traffic',
  --ta.ticketnumber,              
   ta.FirstName,               
  ta.LastName,               
  ta.Address1+ isnull(' '+ta.address2,''),               
  ta.City,               
  st.State, case when len(ta.ZipCode)>5 then substring(ta.ZipCode,1,5) else ta.ZipCode end as ZipCode ,
  isnull(ta.donotmailflag,0)              
            
  from tblticketsarchive ta                      
  inner join                      
  tblstate st                       
  on ta.StateID_FK = st.StateID            
  inner join tblticketsviolationsarchive tv
  on tv.recordid = ta.recordid 
--  LEFT OUTER JOIN  tbl_hts_donotmail_records d ON (d.FirstName = ta.FirstName  AND d.LastName = ta.LastName aND d.StateID_FK = ta.StateID_FK AND d.Zip =left(ta.ZipCode,5) AND d.[Address] = ta.address1 + isnull(' '+ta.address2,'') AND d.City = ta.City )         
  where tv.ticketnumber_pk = @TicketNumber              
              

-- GETTING DALLAS TRAFFIC CASES.....
--Sabir Khan 6167 07/17/2009 remove ticketnumber field for removing duplication in clients...
insert into @DataSet(dbid,DoNotMailUpdateDate, recordid, casetype, firstname,lastname,address1,city,state,zipcode, nomailflag)                 
select distinct 2,ISNULL(CONVERT(VARCHAR(10),ta.DoNotMailUpdateDate,101),'Not Set'), ta.recordid, 'Dallas Traffic',
  --ta.ticketnumber,              
  ta.FirstName,               
  ta.LastName,               
  ta.Address1+ isnull(' '+ta.address2,''),             
  ta.City,               
  st.State, case when len(ta.ZipCode)>5 then substring(ta.ZipCode,1,5) else ta.ZipCode end as ZipCode   ,
  isnull(ta.donotmailflag,0)                        
             
  from dallastraffictickets.dbo.tblticketsarchive ta                      
  join                      
  dallastraffictickets.dbo.tblstate st                       
  on ta.StateID_FK = st.StateID  
  inner join  dallastraffictickets.dbo.tblticketsviolationsarchive tv
  on tv.recordid = ta.recordid
 -- LEFT OUTER JOIN  traffictickets.dbo.tbl_hts_donotmail_records d ON (d.FirstName = ta.FirstName  AND d.LastName = ta.LastName aND d.StateID_FK = ta.StateID_FK AND d.Zip =left(ta.ZipCode,5) AND d.[Address] = ta.address1 + isnull(' '+ta.address2,'') AND d.City = ta.City   )
  where tv.ticketnumber_pk = @TicketNumber                  

---- GETTING HARRIS CRIMINAL CASES.....   
---- Sabir Khan 6167 07/17/2009 remove ticketnumber field for removing duplication in clients...           
--insert into @DataSet(dbid,insertdate, recordid, casetype,firstname,lastname,address1,city,state,zipcode, nomailflag)                 
--select distinct 3,ISNULL(CONVERT(VARCHAR(50),d.InsertDate,101),'N/A'), cc.bookingnumber, 'Harris Criminal',                     
--  --cc.casenumber as ticketnumber,
--  cc.FirstName,                   
--  cc.LastName,                   
--  cc.Address,                   
--  cc.City,           
--  cc.State   ,         
--  CASE WHEN len(cc.Zip) > 5 THEN substring(cc.Zip, 1, 5) ELSE cc.Zip END AS Zip ,
--  isnull(cc.donotmailflag,0)                                
--
--FROM    JIMS.dbo.CriminalCases  cc
--LEFT OUTER JOIN  traffictickets.dbo.tbl_hts_donotmail_records d ON (d.FirstName = cc.FirstName AND d.LastName = cc.LastName AND d.Zip = LEFT(cc.Zip,5)  AND d.[Address] = cc.[Address]  AND d.City = cc.City   )  
----where cc.BookingNumber = @TicketNumber                
--where cc.casenumber = @TicketNumber                


-- GETTING HARRIS CIVIL CASES.....
-- tahir 4418 07/21/2008 
-- Sabir Khan 6167 07/17/2009 remove ticketnumber field for removing duplication in clients...  
insert into @DataSet(dbid,DoNotMailUpdateDate, recordid, casetype, firstname,lastname,address1,city,state,zipcode, nomailflag)                 
select distinct 5,ISNULL(CONVERT(VARCHAR(10),cp.DoNotMailUpdateDate,101),'Not Set'), cp.childcustodypartyid, 'Harris Civil',                     
  --cc.casenumber as ticketnumber,
  left(cp.FirstName,20),                   
  left(cp.LastName,20),                   
  left(cp.Address,50),                   
  cp.City,           
  s.State   ,         
  CASE WHEN len(cp.Zipcode) > 5 THEN substring(cp.Zipcode, 1, 5) ELSE cp.Zipcode END AS Zip  ,
  isnull(cp.donotmailflag,0)                               

FROM    civil.dbo.childcustody  cc 
inner join civil.dbo.childcustodyparties cp 
on cp.childcustodyid = cc.childcustodyid
--where cc.BookingNumber = @TicketNumber  
inner join civil.dbo.tblstate s on s.stateid = cp.stateid           
--LEFT OUTER JOIN  traffictickets.dbo.tbl_hts_donotmail_records d ON (d.FirstName = cp.FirstName AND d.LastName = cp.LastName AND d.Zip = LEFT(cp.Zipcode,5)  AND d.[Address] = cp.[Address]  AND d.City = cp.City   )     
where cc.casenumber = @TicketNumber

              
-- out put the results...              
select * from @DataSet    order by dbid          
               



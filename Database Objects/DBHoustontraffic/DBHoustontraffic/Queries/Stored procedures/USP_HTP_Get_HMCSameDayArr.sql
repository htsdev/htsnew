﻿/***  
CREATED BY: Muhammad Nasir
Task ID: 7234
Created Date 01/09/2010
BUSINESS LOGIC:  
 All HMC court cases that have status of Arraignment or Appearance and court date is past or today can be viewed by sales rep(s)  
LIST OF PARAMETERS:  
   @ShowAll 1/0 o display all cases and 1 display past follow up date cases   
LIST OF COLUMNS:  
  TICKETID_PK: Primary key or unique id againts any violation  
  FIRSTNAME: Cutomer's first name  
  LASTNAME: cutomer's Last Name  
  CASENUMASSIGNEDBYCOURT: CAUSENUMBER number assigned by court   
  COURTDATEMAIN: Court date  
  COURTVIOLATIONSTATUSIDMAIN: court Violation Stats Id  
  COURTNUMBERMAIN: ROOM NUMBER Number of court  
  CourtLoc: Court Locations  
  Follow Up Date: follow up date of case
****/

-- USP_HTP_Get_HMCSameDayArr 1,1
ALTER PROCEDURE dbo.USP_HTP_Get_HMCSameDayArr
	@ShowAll BIT = 0,
	@ValidationCategory INT = 0 -- Saeed 7791 07/10/2010 @ValidationCategory=1 for alert & @ValidationCategory=2 for report
AS
BEGIN
    SELECT DISTINCT tt.TicketID_PK,
           tt.Firstname,
           tt.Lastname,
           ttv.casenumassignedbycourt AS CauseNumber,
           ttv.RefCaseNumber AS ticketnumber,
           ISNULL(tc.ShortName, 'N/A') AS CourtLoc,
           tcvs.[Description] AS STATUS,
           ttv.CourtDateMain AS CourtDate,
           ISNULL(
               CONVERT(VARCHAR(20), tt.HMCSameDayArrFollowUpdate, 101),
               ''
           ) AS Followupdate,
           dbo.fn_getnextbusinessday(GETDATE(), 1) AS NextFollowUpDate,
           tt.HMCSameDayArrFollowUpdate
    FROM   tblTickets tt
           INNER JOIN tblTicketsViolations ttv
                ON  tt.TicketID_PK = ttv.TicketID_PK
           INNER JOIN tblCourtViolationStatus tcvs
                ON  ttv.CourtViolationStatusIDmain = tcvs.CourtViolationStatusID
           INNER JOIN tblCourts tc
                ON  ttv.CourtID = tc.Courtid
    WHERE  --Ozair 7791 07/24/2010  where clause optimized  
           DATEDIFF(DAY, ttv.CourtDateMain, GETDATE()) >= 0
           AND tcvs.CategoryID = 2
           AND tt.CaseTypeId = 1
           AND tt.Activeflag = 1
           AND tc.CourtID IN (3001, 3002, 3003)
           AND (
                   (@ShowAll = 0 OR @ValidationCategory = 2) -- Saeed 7791 07/10/2010 display all records if showAll=1 or validation category is 'Report'
                   OR (
                          (@ShowAll = 1 OR @ValidationCategory = 1)
                          AND DATEDIFF(
                                  DAY,
                                  ISNULL(
                                      CONVERT(VARCHAR(20), tt.HMCSameDayArrFollowUpdate, 101),
                                      '01/01/1900'
                                  ),
                                  GETDATE()
                              ) >= 0
                      ) -- Saeed 7791 07/10/2010 display  if showAll=0 or validation category is 'Alert' then display records which has 'follow up date' null,past or today.
               )
    ORDER BY
           tt.HMCSameDayArrFollowUpdate
END

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_Mailer_Get_Courdates]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_Mailer_Get_Courdates]
GO




--  USP_Mailer_Get_Courdates '8/7/06'
CREATE procedure [dbo].[USP_Mailer_Get_Courdates]

@listdate datetime

as

declare @temp table (crtdate datetime)

insert into @temp 
select distinct	convert(varchar(10),tv.courtdate,101)  
from 	tblticketsarchive t
inner join 
	tblticketsviolationsarchive tv 
on 	t.recordid  = tv.recordid
--and	datediff(day,tv.courtdate,getdate()) <=0
and 	datediff(day, t.listdate, @listdate)  = 0
and 	tv.courtdate is not null
and 	tv.courtdate <> '01/01/1900'

select convert(varchar(10), crtdate,101) as courtdate  from @temp order by crtdate



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


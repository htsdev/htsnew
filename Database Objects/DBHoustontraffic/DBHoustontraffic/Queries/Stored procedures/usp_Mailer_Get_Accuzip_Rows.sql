/****** 
Created by:		Tahir Ahmed
Date:			4/29/2008
Business Logic:	The procedure is used to fill a data set through data adapter in	
				Accuzip listener Service. The service inserts the verified/corrected 
				addresses a temp table by using Data Adapter. And to generate the data
				set for this adapter, this procedure is called to create dataset and 
				then new rows added to the dataset by the service code.
				
List of Parameters:	NIL

List of Columns:first, address, city, middle, last, st, zip, record_id, case_type, crrt, barcode, x, 
				status_, errno_, type_, lacs_, company, ocompany, oaddress, ocity, ostate, ozipcode, 
				dpv_, dpvnotes_, ffapplied_, movetype_, movedate_, matchflag_, nxi_, ank_   
******/


Alter procedure [dbo].[usp_Mailer_Get_Accuzip_Rows]

as 


select top 1 
	first, address, city, middle, last, st, zip, record_id, case_type, crrt, barcode, x, 
	status_, errno_, type_, lacs_, company, ocompany, oaddress, ocity, ostate, ozipcode, 
	dpv_, dpvnotes_, ffapplied_, movetype_, movedate_, matchflag_, nxi_, ank_  
from 
	tblmaileraccuziptemp




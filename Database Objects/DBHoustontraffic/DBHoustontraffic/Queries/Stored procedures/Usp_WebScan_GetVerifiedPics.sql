SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Usp_WebScan_GetVerifiedPics]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Usp_WebScan_GetVerifiedPics]
GO


CREATE procedure [dbo].[Usp_WebScan_GetVerifiedPics]     
@BatchID as int         
as        
        
select         
     
 p.id as picID      
     
 from tbl_webscan_pic p inner join     
   tbl_webscan_ocr o on     
   p.id=o.picid    
    
where     
 p.batchid=@BatchID and o.checkstatus=4    
order by picID  
      
          
        
        
      


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Webscan_GetResults]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Webscan_GetResults]
GO


CREATE procedure [dbo].[usp_Webscan_GetResults] --'1/12/2006','5/12/2006'  
  
@startDate as varchar(50),  
@EndDate as varchar(50)  
  
as      
  
--set @startDate = @startDate + '00:00:00'      
--set @EndDate = @EndDate + '23:59:59'          
      
/*declare @main table( batchid int , TotalCount int )      
declare @subtable table(batchid int,badcount int)      
declare @GoodCount table(batchid int,Goodcount int)      
      
Insert into @main Select batchid , count(id) as TotalCount from tbl_WebScan_Pic group by batchid      
insert into @subtable select p.batchid,count(l.picid) from tbl_WebScan_Pic p       
left outer join tbl_WebScan_Log l on p.id=l.picid group by p.batchid      
      
insert into @GoodCount select p.batchid,count(o.picid) from tbl_WebScan_Pic p       
inner join tbl_WebScan_OCR o on p.id=o.picid      
where o.picid not in (Select picid from tbl_WebScan_Log)      
group by p.batchid      
      
      
Select distinct b.batchid,c.TotalCount,b.scantype,convert(varchar(12),b.scandate,101) as scandate,s.badcount,gc.goodcount    
from tbl_WebScan_Batch b inner join       
@main c on       
      
b.batchid=c.batchid inner join @subtable s      
on b.batchid=s.batchid inner join @GoodCount gc      
on b.batchid=gc.batchid   
  
where   
 b.scandate between @startDate and @EndDate*/

Select	
		distinct b.batchid,
		b.ScanType,
		convert(varchar(12),b.scandate,101)as scandate,
		TotalCount=(select count(p.id) from tbl_webscan_pic p where p.batchid=b.batchid )
from	tbl_webscan_batch b 
	where
		 b.scandate between @startDate and @EndDate




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


﻿ /*************************************************************************
*	Created By		: Abid Ali
*	Created Date	: 11/25/2008
*	Task ID			: 5136
*
*	Business Logic	: This stored procedure return records whose verified info is not 
*					  equal to scan info and whose reset we do not received and its two
*					  week from last update by loader
*
*
*	Parameters List	:
*						@StartDate		: courtdate start date
*						@EndDate		: courtdate end date
*						@ShowAll		: To show all records
*						@CourtLocation	: Filter by Court Location
*
*	Columns Return	: 
*						TicketID_PK
*						refcasenumberseq
*						Courtdatemain
*						courtnumbermain
*						courtdatescan
*						courtnumberscan
*						shortname
*						scanDesc
*						mainDesc
*						activeflag
*						bondamount
*						fineamount
*						causenumber
*						planid
*						violationnumber_pk
*						courtviolationstatusidmain
*						oscarcourtdetail
*						bondflag
*						VerCourtDesc
*						violationDescription
*						courtid
*						CategoryID
*						ChargeLevel
*						IsCriminalCourt
*						CDI
*						hasFTAViolations
*						isFTAViolation
*						hasNOS
*						CoveringFirmID
*						ArrestDate
*						casetypeid
*						MissedCourtType
**************************************************************************/
ALTER PROCEDURE [dbo].[USP_HTP_HMCWaitingResetReport](
	@StartDate AS DATETIME,
	@EndDate AS DATETIME,
	@ShowAll AS INT=0,
	@CourtLocation AS INT =0
)
AS                       
               
SET @EndDate = @EndDate + '23:59:59'                
                
IF @ShowAll  = 1                 
	SELECT @StartDate = '1/1/1900', 
			@EndDate = '12/31/2100'                               
                      

	-- Show Records FROM All Courts        
	IF  @CourtLocation = 0             
	BEGIN            
	            
		SELECT	* 
		FROM	(SELECT  DISTINCT	TT.TicketID_PK, 
								refcasenumber AS refcasenumberseq,                  
								-- Courtdatemain
								CONVERT( VARCHAR(11), TTV.Courtdatemain, 101 ) + '@' 
								+ REPLACE( SUBSTRING( CONVERT( VARCHAR(20), TTV.Courtdatemain ), 13, 18 ), '12:00AM', '0:00AM' ) AS Courtdatemain,
								-- courtnumbermain
								CONVERT( INT, ISNULL( TTV.courtnumbermain, 0 ) ) AS courtnumbermain,                
								-- courtdatescan 
								CONVERT( VARCHAR(11), TTV.courtdatescan , 101 ) + '@'
								+ REPLACE( SUBSTRING( CONVERT( VARCHAR(20), TTV.courtdatescan ), 13, 18 ), '12:00AM', '0:00AM' ) AS courtdatescan,
								-- courtnumberscan 
								CONVERT( INT, ISNULL(TTV.courtnumberscan,0) ) AS courtnumberscan,                          
								-- shortname 
								REPLACE( TC.shortname,'--Choose','N/A' )AS shortname,                
								ticketsviolationid,                   
								-- scanDesc 
								REPLACE( dtScan.description, '---Choose----', 'N/A' ) AS scanDesc,
								-- mainDesc
								REPLACE( dtMain.description, '---Choose----', 'N/A' ) AS mainDesc,
								tt.activeflag,              
								ttv.bondamount,
								ttv.fineamount,
								ttv.casenumassignedbycourt AS causenumber,
								ttv.planid,
								ttv.violationnumber_pk,
								ttv.courtviolationstatusidmain,
								ttv.oscarcourtdetail,
								ttv.underlyingbondflag AS bondflag,
								-- VerCourtDesc
								ISNULL( cvsMain.ShortDescription, '' ) AS VerCourtDesc,
								-- violationDescription
								dbo.tblViolations.Description +
								(CASE	WHEN ttv.violationnumber_pk = 0 THEN ''
										ELSE  ' ' + '(' + ISNULL(vc.ShortDescription, 'N/A') + ')'                                       
										END 
								) AS violationDescription,              
								ttv.courtid,
								cvsMain.CategoryID,
								ISNULL( TTV.ChargeLevel, 0 ) AS ChargeLevel,          
								Convert( INT , tc.IsCriminalCourt ) AS IsCriminalCourt,        
								ISNULL( TTV.CDI , '0' ) AS CDI  ,                   
								dbo.fn_CheckFTAViolationInCase( TT.TicketID_PK ) AS hasFTAViolations,    
								dbo.fn_CheckFTAViolation(ticketsviolationid) AS isFTAViolation ,
								-- hasNOS
								CASE WHEN ( Select COUNT(*) FROM tblticketsflag WHERE flagid=15 and ticketid_pk = tt.ticketid_pk) =0 THEN 0 ELSE 1 END  AS hasNOS,
								TTV.CoveringFirmID ,          
								TTV.ArrestDate  ,
								ISNULL( tt.casetypeid , 0 ) AS  casetypeid,
								ISNULL( TTV.MissedCourtType, -1 ) AS  MissedCourtType            
				FROM
									tbltickets tt
						INNER JOIN	tblticketsviolations ttv
								ON	tt.ticketid_pk = ttv.ticketid_pk
						INNER JOIN	tblCourtViolationStatus cvsScan
								ON	ttv.courtviolationstatusidscan = cvsScan.courtviolationstatusid
									AND
									ttv.courtviolationstatusidscan != 105
						INNER JOIN	tblCourtViolationStatus cvsMain
								ON	ttv.CourtViolationStatusIDmain = cvsMain.CourtViolationStatusID
						INNER JOIN	tbldatetype dtScan
								ON	cvsScan.categoryid = dtScan.typeid AND dtScan.typeid != 50
						INNER JOIN	tbldatetype dtMain
								ON	cvsMain.categoryid = dtMain.typeid
						INNER JOIN	tblcourts tc
								ON	ttv.courtId = tc.courtid			
						INNER JOIN	tblViolations
								ON	ttv.ViolationNumber_PK = dbo.tblViolations.ViolationNumber_PK
						INNER JOIN	tblViolationCategory vc
								ON	tblViolations.CategoryID = vc.CategoryId
				WHERE 	
						-- scan info is not same as verified info		
						(
							ttv.courtdatemain <> ttv.courtdatescan 
							or 
							ttv.courtnumbermain <> ttv.courtnumberscan 
							or 
							dtMain.typeid <> dtScan.typeid                
						)                 
						AND dtMain.typeid in (2,3,4,5)                    
						AND activeflag = 1
						-- only Waiting for Reset
						AND ttv.fk_ResetStatusID = 2
						AND ttv.courtdate between @StartDate and @EndDate
						AND ISNULL(loaderupdatedate, GETDATE()) <= DATEADD(DAY, -14, GETDATE())
						AND LastTrialUpdatedBy = 'Events'
				) AS AllCourts                
		WHERE 
				courtnumbermain <> courtnumberscan                
				or                
				Courtdatemain <> courtdatescan                
				or                
				maindesc <> scanDesc
				  
		ORDER BY ticketid_pk              
	END
	-- Show records of Inside courts
	ELSE IF @CourtLocation = 1             
	BEGIN            
	              
		SELECT	* 
		FROM	(SELECT  DISTINCT	TT.TicketID_PK, 
								refcasenumber AS refcasenumberseq,                  
								-- Courtdatemain
								CONVERT( VARCHAR(11), TTV.Courtdatemain, 101 ) + '@' 
								+ REPLACE( SUBSTRING( CONVERT( VARCHAR(20), TTV.Courtdatemain ), 13, 18 ), '12:00AM', '0:00AM' ) AS Courtdatemain,
								-- courtnumbermain
								CONVERT( INT, ISNULL( TTV.courtnumbermain, 0 ) ) AS courtnumbermain,                
								-- courtdatescan 
								CONVERT( VARCHAR(11), TTV.courtdatescan , 101 ) + '@'
								+ REPLACE( SUBSTRING( CONVERT( VARCHAR(20), TTV.courtdatescan ), 13, 18 ), '12:00AM', '0:00AM' ) AS courtdatescan,
								-- courtnumberscan 
								CONVERT( INT, ISNULL(TTV.courtnumberscan,0) ) AS courtnumberscan,                          
								-- shortname 
								REPLACE( TC.shortname,'--Choose','N/A' )AS shortname,                
								ticketsviolationid,                   
								-- scanDesc 
								REPLACE( dtScan.description, '---Choose----', 'N/A' ) AS scanDesc,
								-- mainDesc
								REPLACE( dtMain.description, '---Choose----', 'N/A' ) AS mainDesc,
								tt.activeflag,              
								ttv.bondamount,
								ttv.fineamount,
								ttv.casenumassignedbycourt AS causenumber,
								ttv.planid,
								ttv.violationnumber_pk,
								ttv.courtviolationstatusidmain,
								ttv.oscarcourtdetail,
								ttv.underlyingbondflag AS bondflag,
								-- VerCourtDesc
								ISNULL( cvsMain.ShortDescription, '' ) AS VerCourtDesc,
								-- violationDescription
								dbo.tblViolations.Description +
								(CASE	WHEN ttv.violationnumber_pk = 0 THEN ''
										ELSE  ' ' + '(' + ISNULL(vc.ShortDescription, 'N/A') + ')'                                       
										END 
								) AS violationDescription,              
								ttv.courtid,
								cvsMain.CategoryID,
								ISNULL( TTV.ChargeLevel, 0 ) AS ChargeLevel,          
								Convert( INT , tc.IsCriminalCourt ) AS IsCriminalCourt,        
								ISNULL( TTV.CDI , '0' ) AS CDI  ,                   
								dbo.fn_CheckFTAViolationInCase( TT.TicketID_PK ) AS hasFTAViolations,    
								dbo.fn_CheckFTAViolation(ticketsviolationid) AS isFTAViolation ,
								-- hasNOS
								CASE WHEN ( Select COUNT(*) FROM tblticketsflag WHERE flagid=15 and ticketid_pk = tt.ticketid_pk) =0 THEN 0 ELSE 1 END  AS hasNOS,
								TTV.CoveringFirmID ,          
								TTV.ArrestDate  ,
								ISNULL( tt.casetypeid , 0 ) AS  casetypeid,
								ISNULL( TTV.MissedCourtType, -1 ) AS  MissedCourtType            
				FROM
									tbltickets tt
						INNER JOIN	tblticketsviolations ttv
								ON	tt.ticketid_pk = ttv.ticketid_pk
						INNER JOIN	tblCourtViolationStatus cvsScan
								ON	ttv.courtviolationstatusidscan = cvsScan.courtviolationstatusid
									AND
									ttv.courtviolationstatusidscan != 105
						INNER JOIN	tblCourtViolationStatus cvsMain
								ON	ttv.CourtViolationStatusIDmain = cvsMain.CourtViolationStatusID
						INNER JOIN	tbldatetype dtScan
								ON	cvsScan.categoryid = dtScan.typeid AND dtScan.typeid != 50
						INNER JOIN	tbldatetype dtMain
								ON	cvsMain.categoryid = dtMain.typeid
						INNER JOIN	tblcourts tc
								ON	ttv.courtId = tc.courtid			
						INNER JOIN	tblViolations
								ON	ttv.ViolationNumber_PK = dbo.tblViolations.ViolationNumber_PK
						INNER JOIN	tblViolationCategory vc
								ON	tblViolations.CategoryID = vc.CategoryId
				WHERE 	
						-- scan info is not same as verified info		
						(
							ttv.courtdatemain <> ttv.courtdatescan 
							or 
							ttv.courtnumbermain <> ttv.courtnumberscan 
							or 
							dtMain.typeid <> dtScan.typeid                
						)                 
						AND dtMain.typeid in (2,3,4,5)                    
						AND activeflag = 1
						-- only Waiting for Reset
						AND ttv.fk_ResetStatusID = 2
						AND ttv.courtdate between @StartDate and @EndDate
						AND ISNULL(loaderupdatedate, GETDATE()) <= DATEADD(DAY, -14, GETDATE())
						AND LastTrialUpdatedBy = 'Events'
				) AS InsideCourts           
		WHERE                
				(
					courtnumbermain <> courtnumberscan                
					or                
					Courtdatemain <> courtdatescan                
					or                
					maindesc <> scanDesc               
				)                
				AND courtid in ( 3001,3002,3003)            
	     ORDER BY ticketid_pk       
	             
	END            
	-- Show records of Outside courts            
	ELSE IF @CourtLocation = 2            
	BEGIN            
	             
		SELECT	* 
		FROM	(SELECT  DISTINCT	TT.TicketID_PK, 
								refcasenumber AS refcasenumberseq,                  
								-- Courtdatemain
								CONVERT( VARCHAR(11), TTV.Courtdatemain, 101 ) + '@' 
								+ REPLACE( SUBSTRING( CONVERT( VARCHAR(20), TTV.Courtdatemain ), 13, 18 ), '12:00AM', '0:00AM' ) AS Courtdatemain,
								-- courtnumbermain
								CONVERT( INT, ISNULL( TTV.courtnumbermain, 0 ) ) AS courtnumbermain,                
								-- courtdatescan 
								CONVERT( VARCHAR(11), TTV.courtdatescan , 101 ) + '@'
								+ REPLACE( SUBSTRING( CONVERT( VARCHAR(20), TTV.courtdatescan ), 13, 18 ), '12:00AM', '0:00AM' ) AS courtdatescan,
								-- courtnumberscan 
								CONVERT( INT, ISNULL(TTV.courtnumberscan,0) ) AS courtnumberscan,                          
								-- shortname 
								REPLACE( TC.shortname,'--Choose','N/A' )AS shortname,                
								ticketsviolationid,                   
								-- scanDesc 
								REPLACE( dtScan.description, '---Choose----', 'N/A' ) AS scanDesc,
								-- mainDesc
								REPLACE( dtMain.description, '---Choose----', 'N/A' ) AS mainDesc,
								tt.activeflag,              
								ttv.bondamount,
								ttv.fineamount,
								ttv.casenumassignedbycourt AS causenumber,
								ttv.planid,
								ttv.violationnumber_pk,
								ttv.courtviolationstatusidmain,
								ttv.oscarcourtdetail,
								ttv.underlyingbondflag AS bondflag,
								-- VerCourtDesc
								ISNULL( cvsMain.ShortDescription, '' ) AS VerCourtDesc,
								-- violationDescription
								dbo.tblViolations.Description +
								(CASE	WHEN ttv.violationnumber_pk = 0 THEN ''
										ELSE  ' ' + '(' + ISNULL(vc.ShortDescription, 'N/A') + ')'                                       
										END 
								) AS violationDescription,              
								ttv.courtid,
								cvsMain.CategoryID,
								ISNULL( TTV.ChargeLevel, 0 ) AS ChargeLevel,          
								Convert( INT , tc.IsCriminalCourt ) AS IsCriminalCourt,        
								ISNULL( TTV.CDI , '0' ) AS CDI  ,                   
								dbo.fn_CheckFTAViolationInCase( TT.TicketID_PK ) AS hasFTAViolations,    
								dbo.fn_CheckFTAViolation(ticketsviolationid) AS isFTAViolation ,
								-- hasNOS
								CASE WHEN ( Select COUNT(*) FROM tblticketsflag WHERE flagid=15 and ticketid_pk = tt.ticketid_pk) =0 THEN 0 ELSE 1 END  AS hasNOS,
								TTV.CoveringFirmID ,          
								TTV.ArrestDate  ,
								ISNULL( tt.casetypeid , 0 ) AS  casetypeid,
								ISNULL( TTV.MissedCourtType, -1 ) AS  MissedCourtType            
				FROM
									tbltickets tt
						INNER JOIN	tblticketsviolations ttv
								ON	tt.ticketid_pk = ttv.ticketid_pk
						INNER JOIN	tblCourtViolationStatus cvsScan
								ON	ttv.courtviolationstatusidscan = cvsScan.courtviolationstatusid
									AND
									ttv.courtviolationstatusidscan != 105
						INNER JOIN	tblCourtViolationStatus cvsMain
								ON	ttv.CourtViolationStatusIDmain = cvsMain.CourtViolationStatusID
						INNER JOIN	tbldatetype dtScan
								ON	cvsScan.categoryid = dtScan.typeid AND dtScan.typeid != 50
						INNER JOIN	tbldatetype dtMain
								ON	cvsMain.categoryid = dtMain.typeid
						INNER JOIN	tblcourts tc
								ON	ttv.courtId = tc.courtid			
						INNER JOIN	tblViolations
								ON	ttv.ViolationNumber_PK = dbo.tblViolations.ViolationNumber_PK
						INNER JOIN	tblViolationCategory vc
								ON	tblViolations.CategoryID = vc.CategoryId
				WHERE 	
						-- scan info is not same as verified info		
						(
							ttv.courtdatemain <> ttv.courtdatescan 
							or 
							ttv.courtnumbermain <> ttv.courtnumberscan 
							or 
							dtMain.typeid <> dtScan.typeid                
						)                 
						AND dtMain.typeid in (2,3,4,5)                    
						AND activeflag = 1
						-- only Waiting for Reset
						AND ttv.fk_ResetStatusID = 2
						AND ttv.courtdate between @StartDate and @EndDate
						AND ISNULL(loaderupdatedate, GETDATE()) <= DATEADD(DAY, -14, GETDATE())
						AND LastTrialUpdatedBy = 'Events'
				)   AS outSideCourts                  
		WHERE 
				(                
				   courtnumbermain <> courtnumberscan                
					or                
					Courtdatemain <> courtdatescan                
					or                
					maindesc <> scanDesc                
				)                
				AND courtid not in ( 3001,3002,3003)            
		ORDER BY ticketid_pk
	             
	END
	
	GO
		GRANT EXEC ON [dbo].[USP_HTP_HMCWaitingResetReport] to dbr_webuser
	GO
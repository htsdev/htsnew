﻿/************************************************************
 * Code formatted by SoftTree SQL Assistant © v4.0.34
 * Time: 1/20/2010 6:53:09 PM
 ************************************************************/

/****** 
Created by:		Fahad Muhammad Qureshi.
TaskID:			6934
Created Date:	12/08/2009
Business Logic:	The procedure is used by Houston Traffic Program for backroom to get information about the tracking records of Quote Call Back Date. 
				
List of Parameters:
	@DateFrom :Starting Date of Date range.
	@DateTo : Ending Date of Date range.
	@IsValidation : For Validation email report=1 else 0.

******/
CREATE PROCEDURE dbo.USP_HTP_Get_QuoteCallBackTracking
	@DateFrom DATETIME,
	@DateTo DATETIME
AS
	SELECT tt.Firstname + ' ' + tt.Lastname AS Customer,
	       tu.Firstname + ' ' + tu.Lastname AS RepName,
	       qctd.EventDescription,
	       dbo.fn_DateFormat(qctd.EventDateTime, 30, '/', ':', 1) AS 
	       EventDateTime,
	       CASE 
	            WHEN tt.Activeflag = 1 THEN 'Yes'
	            ELSE 'No'
	       END AS Hired,
	       tt.ticketid_pk
	FROM   QuoteCallbackTracking qct
	       LEFT OUTER JOIN tblTickets tt
	            ON  tt.TicketID_PK = qct.TicketID
	       INNER JOIN QuoteCallbackTrackingDetail qctd
	            ON  qct.QuoteCallbackTrackingID = qctd.QuoteCallbackTrackingID
	       INNER JOIN tblUsers tu
	            ON  tu.EmployeeID = qctd.EmployeeID
	WHERE  --When Called from Report check for Date Range
	       DATEDIFF(DAY, @DateFrom, qctd.EventDateTime) >= 0
	       AND DATEDIFF(DAY, qctd.EventDateTime, @DateTo) >= 0
GO

GRANT EXECUTE ON dbo.USP_HTP_Get_QuoteCallBackTracking TO dbr_webuser
GO 
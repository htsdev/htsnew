﻿/****** Object:  StoredProcedure [dbo].[USP_HTP_GET_LAST_VIOLATION]  ******/     
/*        
 Created By     : Fahad Muhammad Qureshi.
 Created Date   : 05/18/2009  
 TasK		    : 5807        
 Business Logic : This procedure retrives last updated violation from client profile
				
Parameter:       
  @Ticketid		: Retrieving criteria with respect to TicketId      
  
*/ 

-- DROP PROCEDURE USP_HTP_GET_LAST_VIOLATION 74861
Alter PROCEDURE USP_HTP_GET_LAST_VIOLATION
	@Ticketid INT
AS
	SELECT ttv.TicketsViolationID,
	       CASE WHEN (LEN(LTRIM (RTRIM (ttv.casenumassignedbycourt))) > 12) THEN SUBSTRING(LTRIM(RTRIM (ttv.casenumassignedbycourt)), 0, 12)
	            ELSE ISNULL(ttv.casenumassignedbycourt, '')--Fahad 6202 08/05/2009 Cause Number length should be of 12 
	       END AS casenumassignedbycourt,
	       CASE WHEN (LEN(LTRIM (RTRIM (ttv.RefCaseNumber))) > 12) THEN SUBSTRING(ttv.RefCaseNumber, 0, 12)
	            ELSE ISNULL(ttv.RefCaseNumber, '')
	       END AS RefCaseNumber,
	       ttv.casenumassignedbycourt
	FROM   tblTicketsViolations ttv
	WHERE  ttv.TicketsViolationID = (
	           SELECT MAX(tblTicketsViolations.TicketsViolationID)
	           FROM   tblTicketsViolations
	           WHERE  tblTicketsViolations.TicketID_PK = @Ticketid
	       )

GO

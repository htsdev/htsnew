SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_hts_get_trial_docket_report_HCC_Test]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_hts_get_trial_docket_report_HCC_Test]
GO

-- usp_hts_get_trial_docket_report_HCC_Test 3037, '03/15/2007', '03/15/2007'  
  
CREATE   procedure [dbo].[usp_hts_get_trial_docket_report_HCC_Test]                                
@courtloc int = 3037,                                  
@Fromcourtdate datetime = '01/01/1900',      
@Tocourtdate datetime = '01/01/1900',                                
@page varchar(12) = 'TRIAL',                                  
@courtnumber varchar(6) = '-1',                                  
@datetype varchar(20) = '0',  
@singles int = 0   
                            
as  
  
--set @Fromcourtdate = @Fromcourtdate + '00:00:00'  
set @Tocourtdate = @Tocourtdate + '23:59:59'  
  
if @courtnumber = ''  
 begin  
  set @courtnumber ='-1'  
 end  
  
set nocount on                                  
  
SELECT      
 dbo.tblTickets.TicketID_PK ,   
 dbo.tblTickets.Firstname,   
 dbo.tblTickets.Lastname,  
 case   
 when  ltrim(casenumassignedbycourt) = ''  then    refcasenumber      
 else isnull(casenumassignedbycourt,refcasenumber) end as causenumber,  
 ' ' Bond,   
 dbo.tblTicketsViolations.courtdatemain,  
 dbo.tblcourts.shortname,  
 dbo.tblTicketsViolations.courtnumbermain,  
 dbo.tblViolations.ShortDesc,        
 isnull(dbo.tblViolationCategory.ShortDescription,'N/A') as ViolationCategory,              
 dbo.tblcourtviolationstatus.ShortDescription as description,  
 dbo.tblCourts.Courtid   
into #temp   
 FROM            dbo.tblTickets INNER JOIN  
                      dbo.tblTicketsViolations ON dbo.tblTickets.TicketID_PK = dbo.tblTicketsViolations.TicketID_PK INNER JOIN  
                      dbo.tblViolations ON dbo.tblTicketsViolations.ViolationNumber_PK = dbo.tblViolations.ViolationNumber_PK INNER JOIN  
                      dbo.tblCourtViolationStatus ON dbo.tblTicketsViolations.CourtViolationStatusIDmain = dbo.tblCourtViolationStatus.CourtViolationStatusID INNER JOIN  
                      dbo.tblCourts ON dbo.tblTicketsViolations.CourtID = dbo.tblCourts.Courtid left outer JOIN  
                      dbo.tblViolationCategory ON dbo.tblViolations.CategoryID = dbo.tblViolationCategory.CategoryId  
 WHERE     (dbo.tblViolations.Violationtype not IN (1)   
    or dbo.tblViolations.violationnumber_pk in (11,12) )  
    AND (dbo.tblcourtviolationstatus.categoryid   
      IN (select distinct categoryid from tblcourtviolationstatus where violationcategory in (0,2)  
    and categoryid not in (7,50)  
    and courtviolationstatusid not in (104) ))  
    AND (dbo.tblTickets.Activeflag = 1)  
    and dbo.tblTicketsViolations.courtid <> 0    
    and dbo.tblTicketsViolations.courtid = @courtloc  
                                
    --and datediff(DAY,dbo.tblTicketsViolations.courtdatemain,@courtdate) = 0  
    and dbo.tblTicketsViolations.courtdatemain between @Fromcourtdate and @Tocourtdate  
  
  
  
  
if @@rowcount > 0  
begin   
  
select DISTINCT ticketid, totalfeecharged-sum(ChargeAmount) as dueamount   
 into  #temp2  
from tblticketspayment A,tbltickets T where   
 T.ticketid_pk = A.ticketid   
 and activeflag = 1   
 and paymentvoid=0  
 and paymenttype not in (99,100)  
group by ticketid,totalfeecharged                                      
    
declare @sqlquery varchar(8000)              
  
                                
set @sqlquery = ' '                                  
              
  
if @courtnumber = '-1'  
begin                
  select T.*, case when dueamount > 0 then '$$' else '' end as fee   
   FROM #temp T                                 
    left outer join #temp2 T2 on T.ticketid_pk = T2.ticketid                                   
end   
  
else   
begin   
 select T.*, case when dueamount > 0 then '$$' else '' end as fee  
   FROM #temp T                                 
    left outer join #temp2 T2 on T.ticketid_pk = T2.ticketid   
 where courtnumbermain = @courtnumber  
end   
  
end  
  



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


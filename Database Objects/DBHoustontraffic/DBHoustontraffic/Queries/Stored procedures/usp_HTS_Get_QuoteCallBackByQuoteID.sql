set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

 -- Noufil 4487 08/05/2008 courtlocation field added 
    
ALTER PROCEDURE [dbo].[usp_HTS_Get_QuoteCallBackByQuoteID]            
@QuoteID  int                
AS    
SELECT      tblTickets.Lastname + ', ' + tblTickets.Firstname AS Customer, 
			tblViolationQuote.QuoteID, 
			tblViolationQuote.QuoteResultDate,     
            (CASE dbo.tblViolationQuote.FollowUPID WHEN 0 THEN 1 ELSE dbo.tblViolationQuote.FollowUPID END) AS Followupid, 
			tblViolationQuote.FollowUPDate,    
            tblViolationQuote.FollowUpYN, 
			tbltickets.GeneralComments, 
			ISNULL(tblViolationQuote.CallBackDate, '01/01/1900') AS CallBackDate,     
            ISNULL(tblViolationQuote.AppointmentDate, '01/01/1900') AS AppointmentDate, 
			ISNULL(tblViolationQuote.AppointmentTime, '') AS AppointmentTime,     
            tblTicketsViolations.CourtDateMain, 
			tblTickets.calculatedtotalfee, 
			ISNULL(tblTickets.SSNumber, '') + ' ' + ISNULL(tblTicketsViolations.ticketOfficerNumber, '') AS ContactNo,     
            tblTickets.TicketID_PK, tblQuoteResult.QuoteResultID, ISNULL(tblQuoteResult.QuoteResultDescription, '') AS QuoteResultDescription,     
            ISNULL(tblQuoteResult_1.QuoteResultDescription, '') AS QuoteResultDescription2, 
			ISNULL(tblViolationQuote.CallBackTime, '') AS CallBackTime,     
            CONVERT(varchar(12), tblViolationQuote.LastContactDate, 101) AS LastContactDate, 
			tblViolations.Description,
			c.shortname as crt

FROM         tblTicketsViolations 
				INNER JOIN tblTickets ON tblTicketsViolations.TicketID_PK = tblTickets.TicketID_PK 
				INNER JOIN tblViolations ON tblTicketsViolations.ViolationNumber_PK = tblViolations.ViolationNumber_PK 
				RIGHT OUTER JOIN tblViolationQuote 
					LEFT OUTER JOIN tblQuoteResult AS tblQuoteResult_1 ON tblViolationQuote.FollowUPID = tblQuoteResult_1.QuoteResultID 
					LEFT OUTER JOIN tblQuoteResult ON tblViolationQuote.QuoteResultID = tblQuoteResult.QuoteResultID ON tblTickets.TicketID_PK = tblViolationQuote.TicketID_FK
				inner join dbo.tblCourts c on tblTicketsViolations.CourtID = c.Courtid        
                
WHERE     (dbo.tblViolationQuote.QuoteID  = @QuoteID)    


/****** 
Created by:		Syed Muhammad Ozair
Business Logic:	The Procedure is used to get Auto Dialer Response Types 
				
List of Columns:	
	@EventTypeID:		
	@EmployeeID:
	@Notes:
	
*******/

create procedure dbo.usp_AD_Insert_ConfigSettingsHistory 

@EventTypeID	int,
@EmployeeID	int,
@Notes varchar(500)

as

insert into AutoDialerConfigSettingsHistory
(
EventTypeID_Fk,
Notes,
EmployeeID
)
values
(
@EventTypeID,
@Notes,
@EmployeeID
)

go

grant execute on dbo.usp_AD_Insert_ConfigSettingsHistory to dbr_webuser
go
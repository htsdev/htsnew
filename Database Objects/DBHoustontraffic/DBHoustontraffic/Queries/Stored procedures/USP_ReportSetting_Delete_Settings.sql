set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

  /*  
* Created By		: SAEED AHMED
* Task ID			: 8101  
* Created Date		: 09/27/2010  
* Business logic	: This procedure is used delete report setting based on setting id.
*					: 
* Parameter List	
  @ReportSettingId	: Primary key of the table aginst which deletion will perform.	
* 
* USP_ReportSetting_Delete_Settings 17
*/
CREATE PROC [dbo].[USP_ReportSetting_Delete_Settings]
@ReportSettingId INT
AS
DELETE 
FROM   ReportSetting
WHERE  ReportSettingId = @ReportSettingId	
GO
GRANT EXECUTE ON [dbo].[USP_ReportSetting_Delete_Settings] TO dbr_webuser
GO

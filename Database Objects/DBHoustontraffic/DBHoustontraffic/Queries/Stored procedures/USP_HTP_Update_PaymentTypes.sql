

/*  
Created By : Abbas Qamar
Task Id : 9957 
Creation Date : 03-01-2012
Business Logic : This procedure Update FollowUpdate Coloumn in Tbltikcets

Parameter:
			@PaymentId   : Follow Up Date set by User
			@Description : Ticket id of user		

*/
CREATE PROCEDURE [dbo].[USP_HTP_Update_PaymentTypes]  

@PaymentId			INT, 
@Description       varchar(200),
@ShortDescription	varchar(200),
@IsActive			bit

AS		

UPDATE tblPaymenttype
SET Description=@Description,ShortDescription=@ShortDescription,IsActive = @IsActive
WHERE Paymenttype_PK=@PaymentId


select @@ROWCOUNT



GO
GRANT EXECUTE ON [dbo].[USP_HTP_Update_PaymentTypes] TO dbr_webuser
GO 
	

﻿/******        
Created by:  Muhammad Nasir  
Business Logic : This procedure is used By HMC A/W DLQ Calls report to get  client(s)
which have Verified status in Arraigment, Arraigment waiting and auto status is in DLQ or blank and court should be inside court(HMC) .    
sort by courtdate   
Task : 5690  
               
List of Parameters:         
    @startdate : start date specify by user        
    @enddate : end date specify by user        
    @status       : call status        
       0 Pending        
       1 Contacted        
       2 Left Message        
       3 No Answer        
       4 All  
         
    @showall    : show all record or not .        
    @showreport   : not in use  
    @language :    
    @mode  : not in use.      
       
******/           
          
-- [USP_HTP_Get_HMCAWDLQCalls] '01/18/2009','02/18/2009','1','1','2','0','1'       
CREATE PROCEDURE [dbo].[USP_HTP_Get_HMCAWDLQCalls]
	@startdate DATETIME,
	@enddate DATETIME,
	@status INT = 0 ,
	@showall INT = 0,
	@showreport INT = 2,
	@language INT,
	@mode INT
AS
	DECLARE @lng VARCHAR(20)      
	
	SET @startdate = @startdate + '23:59:58'       
	SET @enddate = @enddate + '23:59:58'       
	
	IF @language = 0
	    SET @lng = 'ENGLISH'
	ELSE   
	IF @language = 1
	    SET @lng = 'SPANISH'      
	
	
	SELECT DISTINCT 
	       t.TicketID_PK AS ticketid,
	       t.Firstname,
	       t.Lastname,
	       ISNULL(t.LanguageSpeak, 'N/A') AS LanguageSpeak,
	       t.BondFlag,
	       t.GeneralComments,
	       tv.FTACallComments AS comments,
	       ISNULL(tv.HMCAWDLQCallStatus, 0) AS STATUS,
	       CONVERT(
	           VARCHAR(20),
	           ISNULL(dbo.formatphonenumbers(T.Contact1), '')
	       ) + '(' + ISNULL(LEFT(Ct1.Description, 1), '') + ')' AS contact1,
	       CONVERT(
	           VARCHAR(20),
	           ISNULL(dbo.formatphonenumbers(T.Contact2), '')
	       ) + '(' + ISNULL(LEFT(Ct2.Description, 1), '') + ')' AS contact2,
	       CONVERT(
	           VARCHAR(20),
	           ISNULL(dbo.formatphonenumbers(T.Contact3), '')
	       ) + '(' + ISNULL(LEFT(Ct3.Description, 1), '') + ')' AS contact3,
	       T.TotalFeeCharged,
	       f.FirmAbbreviation,
	       f.Phone,
	       f.Fax,
	       Insurance = (
	           SELECT TOP 1 vv.shortdesc + '(' + ISNULL(CONVERT(VARCHAR, tvv.ticketviolationdate, 101), '') 
	                  + ')'
	           FROM   tblviolations vv
	                  INNER JOIN tblticketsviolations tvv
	                       ON  vv.violationnumber_pk = tvv.violationnumber_pk
	           WHERE  tvv.ticketid_pk = t.ticketid_pk
	                  AND vv.shortdesc = 'ins'
	       ),
	       Child = (
	           SELECT TOP 1 vv.shortdesc
	           FROM   tblviolations vv
	                  INNER JOIN tblticketsviolations tvv
	                       ON  vv.violationnumber_pk = tvv.violationnumber_pk
	           WHERE  tvv.ticketid_pk = t.ticketid_pk
	                  AND vv.shortdesc = 'chld blt'
	       ),
	       CONVERT(DATETIME, CONVERT(VARCHAR, CourtDateMain, 101)) AS 
	       CourtDateMain 
	       INTO #temp4
	FROM   tblTicketsViolations TV --added in order to get single ticketid                
	       
	       INNER JOIN dbo.tblTickets t
	            ON  TV.TicketID_PK = t.TicketID_PK
	       INNER JOIN dbo.tblCourtViolationStatus cvs
	            ON  TV.CourtViolationStatusIDMain = cvs.CourtViolationStatusID
	       INNER JOIN dbo.tblCourts c
	            ON  TV.CourtID = c.Courtid
	       LEFT OUTER JOIN dbo.tblFirm f
	            ON  t.FirmID = f.FirmID
	       LEFT OUTER JOIN dbo.tblContactstype ct2
	            ON  t.ContactType2 = ct2.ContactType_PK
	       LEFT OUTER JOIN dbo.tblContactstype ct1
	            ON  t.ContactType1 = ct1.ContactType_PK
	       LEFT OUTER JOIN dbo.tblContactstype ct3
	            ON  t.ContactType3 = ct3.ContactType_PK
	       INNER JOIN tblviolations v
	            ON  v.violationnumber_pk = ISNULL(tv.violationnumber_pk, 0) 	                
	       INNER JOIN dbo.tblticketspayment TP
	            ON  TV.TicketID_PK = TP.TicketID
	WHERE  t.CaseTypeId = 1
	       AND (@language = -1 OR t.LanguageSpeak = @lng)
	       AND (
	               (cvs.CategoryID = 2 OR cvs.CourtViolationStatusID IN (201))
	               AND (
	                       tv.CourtViolationStatusID = 146
	                       OR ISNULL(tv.CourtViolationStatusID, 0) = 0
	                   )
	           )
	       AND (
	               @showall = 1
	               OR DATEDIFF([day], TV.CourtDatemain, @startdate) <= 0
	               AND DATEDIFF([day], TV.CourtDatemain, @enddate) >= 0
	           )
	       AND TV.CourtID IN (3001, 3002, 3003)
	       AND t.activeflag = 1 
	
	
	
	
	
	SELECT DISTINCT 
	       ticketid,
	       firstname,
	       lastname,
	       languagespeak,
	       bondflag,
	       comments,
	       GeneralComments,
	       ISNULL(STATUS, 0) AS STATUS,
	       Contact1,
	       Contact2,
	       Contact3,
	       T.CourtDateMain,
	       totalfeecharged,
	       FirmAbbreviation,
	       phone,
	       fax,
	       Insurance,
	       Child,
	       rs.Description AS callback 
	       INTO #temp1
	FROM   #temp4 T
	       LEFT OUTER JOIN tblReminderStatus rs
	            ON  rs.Reminderid_pk = T.status   
	
	
	
	
	SELECT T.*,
	       owedamount = t.totalfeecharged -(
	           SELECT ISNULL(SUM(ISNULL(chargeamount, 0)), 0)
	           FROM   tblticketspayment p
	           WHERE  p.ticketid = t.ticketid
	                  AND p.paymenttype NOT IN (99, 100)
	                  AND p.paymentvoid <> 1
	       ) 
	       
	       INTO #temp2
	FROM   #temp1 T 
	
	--Call function in order to get distinct violations                                       	
	SELECT T.*,
	       dbo.fn_hts_get_TicketsViolations (ticketid, CourtDateMain) AS trialdesc,
	       WrongNumberFlag = (
	           SELECT COUNT(*)
	           FROM   tblticketsflag
	           WHERE  ticketid_pk = T.TicketId
	                  AND FlagID = 33
	       ),
	       dbo.fn_hts_get_TicketsViolationIds (ticketid, CourtDateMain) AS TicketViolationIds 
	       INTO #temp5
	FROM   #temp2 T 
	
	
	
	-- For Status All
	IF (@status = 4)
	    SELECT *
	    FROM   #temp5
	    ORDER BY
	           CourtDateMain,
	           lastname,
	           firstname
	ELSE
	    SELECT *
	    FROM   #temp5
	    WHERE  STATUS = CONVERT(VARCHAR, @status)
	    ORDER BY
	           CourtDateMain,
	           lastname,
	           firstname 
	
	
	DROP TABLE #temp1 
	DROP TABLE #temp4 
	DROP TABLE #temp2 
	DROP TABLE #temp5
GO


GRANT EXEC ON dbo.[USP_HTP_Get_HMCAWDLQCalls] TO dbr_webuser
GO 
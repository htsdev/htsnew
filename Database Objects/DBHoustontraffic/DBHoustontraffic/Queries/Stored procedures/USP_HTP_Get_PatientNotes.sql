﻿/******  
* Created By :	  Saeed Ahmed.
* Create Date :   11/08/2010 
* Task ID :		  8501
* Business Logic :  This procedure is used to get patient notes.
* List of Parameter : @PatientID 
* Column Return :     
******/
 create  PROC  dbo.USP_HTP_Get_PatientNotes
 @PatientId INT
 AS
 
SELECT pn.Id,
       pn.PatientId,
       pn.Notes,
       pn.InsertedDate,
       pn.InsertedBy,
       u.LastName + ', ' + u.FirstName AS username
FROM   patientNotes pn
       INNER JOIN Users.dbo.[User] u
            ON  u.UserId = pn.InsertedBy
WHERE  patientid = @PatientId
ORDER BY
       INSERTEDdate DESC 
 
 GO

 GRANT EXECUTE ON dbo.USP_HTP_Get_PatientNotes TO dbr_webuser
 GO
-- =============================================
-- Author: Asghar
-- Create date: 03/03/2007
-- Business Logic: This stored procedure is used to insert attorney trial information in tbljurytrial.
-- =============================================
--   USP_HTS_Insert_JuryTrial '01/03/2007',111,'asghar','khalid','nahid','as876','a234','234','judge','breif','444','verdict','45','sd',1

alter PROCEDURE [dbo].[USP_HTS_Insert_JuryTrial] 
	
@date as	datetime,
@attorney	int,
@prosecutor	varchar(50),
@officer	varchar(50),
@defendant	varchar(50),
@caseno		varchar(30),
@ticketno	varchar(30),
@courtid	int,
@judge		varchar(50),
--Yasir Kamal 6279 08/05/2009 field length increased.
@brieffacts	varchar(MAX),
@verdictid	int,
@verdict	varchar(50),
@fine		money,
@courtreport varchar(50),		
@rowid int = 0 output	

AS
BEGIN

insert into tbljurytrial(recdate,date,attorney,prosecutor,officer,defendant,caseno,ticketno,courtid,judge,brieffacts,verdictid,verdict,fine,courtreport)
	values(getdate(),@date,@attorney,@prosecutor,@officer,@defendant,@caseno,@ticketno,@courtid,@judge,@brieffacts,@verdictid,@verdict,@fine,@courtreport) 

select @rowid = scope_identity()

END




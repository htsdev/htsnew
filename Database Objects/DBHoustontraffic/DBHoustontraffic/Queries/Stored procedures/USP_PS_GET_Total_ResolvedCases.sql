
  /****** 
Created by:		Unknown
Business Logic:	The procedure is used by public site to get the total number of violations
				related to our clients that we have covered.
				
List of Parameters:
	No parameter
	
List of Columns:	
	Column1:	Total number of violations related to our clients.	

******/
  
  
  
ALTER Procedure [dbo].[USP_PS_GET_Total_ResolvedCases]  
as  
select count(distinct tv.ticketsviolationid) 
from tblticketsviolations tv inner join  tbltickets t  
on t.ticketid_pk = tv.ticketid_pk
where activeflag = 1  
  
  
  
  




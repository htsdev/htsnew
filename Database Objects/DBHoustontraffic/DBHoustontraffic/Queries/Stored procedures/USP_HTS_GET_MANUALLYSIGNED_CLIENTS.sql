ALTER PROCEDURE [dbo].[USP_HTS_GET_MANUALLYSIGNED_CLIENTS] --'07/13/2007','08/13/2007',2      
@FromDate datetime,     
@ToDate datetime,   
@DataBase int      
AS      
  
set @FromDate = convert(varchar,@FromDate,101) + ' 00:00:00'  
set @ToDate = convert(varchar,@ToDate,101) + ' 23:59:59'  
      
if(@DataBase = 1)      
begin      
SELECT           
t.TicketID_PK,      
MIN(p.RecDate) as PaymentDate,      
t.Firstname,       
t.Lastname,       
t.TotalFeeCharged as TotalFees,      
(select sum(isnull(ChargeAmount,0)) from tblticketspayment where TicketID = t.TicketID_PK and PaymentVoid = 0) as PaymentAmount,      
isnull(dbo.fn_FormatPhoneNumber(tct1.Description,t.Contact1) + '<BR>' +dbo.fn_FormatPhoneNumber(tct2.Description,t.Contact2) + '<BR>' + dbo.fn_FormatPhoneNumber(tct3.Description,t.Contact3),'') as ContactNo,      
dbo.fn_GetRelatedCaseNumbers(t.TicketID_PK) as CaseNumbers      
FROM         tblTickets AS t INNER JOIN      
tblTicketsPayment AS p ON t.TicketID_PK = p.TicketID INNER JOIN      
tblTicketsViolations AS v ON t.TicketID_PK = v.TicketID_PK       
left outer join tblcontactstype tct1        
on t.contacttype1 = tct1.contacttype_pk        
left outer join tblcontactstype tct2        
on t.contacttype2 = tct2.contacttype_pk        
left outer join tblcontactstype tct3        
on t.contacttype3 = tct3.contacttype_pk       
WHERE     (t.MailerID IS NULL) AND (t.RecordId IS NULL) AND (v.ticketviolationdate IS NULL) AND (p.PaymentVoid = 0)     
GROUP BY t.TicketID_PK, t.Firstname, t.Lastname, t.Midnum, t.TotalFeeCharged,      
tct1.Description,t.Contact1,tct2.Description,t.Contact2,tct3.Description,t.Contact3,p.RecDate   
--HAVING DateDiff(Day,Min(p.RecDate),@FromDate)=0      
HAVING p.RecDate between @FromDate and @ToDate  
ORDER BY p.RecDate desc  
end  
      
else      
begin      
SELECT           
t.TicketID_PK,      
MIN(p.RecDate) as PaymentDate,      
t.Firstname,       
t.Lastname,       
t.TotalFeeCharged as TotalFees,      
(select sum(isnull(ChargeAmount,0)) from dallastraffictickets.dbo.tblticketspayment where TicketID = t.TicketID_PK and PaymentVoid = 0) as PaymentAmount,      
isnull(dbo.fn_FormatPhoneNumber(tct1.Description,t.Contact1) + '<BR>' +dbo.fn_FormatPhoneNumber(tct2.Description,t.Contact2) + '<BR>' + dbo.fn_FormatPhoneNumber(tct3.Description,t.Contact3),'') as ContactNo,      
dallastraffictickets.dbo.fn_GetRelatedCaseNumbers(t.TicketID_PK) as CaseNumbers      
FROM         dallastraffictickets.dbo.tblTickets AS t INNER JOIN      
dallastraffictickets.dbo.tblTicketsPayment AS p ON t.TicketID_PK = p.TicketID INNER JOIN      
dallastraffictickets.dbo.tblTicketsViolations AS v ON t.TicketID_PK = v.TicketID_PK       
left outer join dallastraffictickets.dbo.tblcontactstype tct1        
on t.contacttype1 = tct1.contacttype_pk        
left outer join dallastraffictickets.dbo.tblcontactstype tct2        
on t.contacttype2 = tct2.contacttype_pk        
left outer join dallastraffictickets.dbo.tblcontactstype tct3        
on t.contacttype3 = tct3.contacttype_pk       
WHERE     (t.MailerID IS NULL) AND (t.RecordId IS NULL) AND (v.ticketviolationdate IS NULL) AND (p.PaymentVoid = 0)      
GROUP BY t.TicketID_PK, t.Firstname, t.Lastname, t.Midnum, t.TotalFeeCharged,      
tct1.Description,t.Contact1,tct2.Description,t.Contact2,tct3.Description,t.Contact3,p.RecDate      
--HAVING DateDiff(Day,Min(p.RecDate),@FromDate)=0      
HAVING p.RecDate between @FromDate and @ToDate  
ORDER BY p.RecDate desc  
end   
go 
﻿   
  
/****** Object:  StoredProcedure [dbo].[USP_HTP_Update_NonHMCFollowUpdate]  ******/     
/*        
Created By     : Fahad Muhammad Qureshi.
Created Date   : 04/03/2009  
TasK   : 5653        
Business Logic  : This procedure Updates Non HMC Follow Up Date.      
           
Parameter:       
   @TicketID     : Updating criteria with respect to TicketId      
   @FollowUpDate : Date of follow Up which has to be set     
     
      
*/      
  
CREATE PROCEDURE [dbo].[USP_HTP_Update_NonHMCFollowUpdate]  
 @FollowUpDate DATETIME,  
 @TicketID INT  
AS  
 UPDATE tblTickets  
 SET NonHMCFollowUpDate = @FollowUpDate  
 WHERE  TicketID_PK = @TicketID 
 GO


grant exec on [dbo].[USP_HTP_Update_NonHMCFollowUpdate] to dbr_webuser

GO
 



USE [LoaderFilesArchive]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTP_SearchHCJPData]    Script Date: 05/31/2012 08:21:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*        
Created By     : Muhammad Adil Aleem.
Created Date   : 05/29/2008
Business Logic : This procedure fetches Records which we get from our Loaders from the Court Houses on the basis pf following Paramters 
				 by which we applying filter on it. 
           
Parameter:       
    @LoaderId    :by which identify the Loader  
    @Mode        :Searching Mode: 
				 1:TicketNumber,
				 2:CauseNumber,
				 3:LastName,FirstName,MiddleName,DateofBirth, 
				 4:FileDate,
				 5:CourtDate.
    @CauseNo     :by which we can search CauseNumber or apply mode 2
    @TicketNo    :by which we can search TicketNumber or apply mode 1
    @CrtDate     :by which we can search CourtDate or apply mode 5 
    @LastName    :by which we can search LastName or apply mode 3 and mode 4
    @FirstName   :by which we can search FirstName or apply mode 3
    @MiddleName  :by which we can search MiddleName or apply mode 3
    @DOB         :by which we can search DateOfBirth or apply mode 3
    
*/      

ALTER PROCEDURE [dbo].[USP_HTP_SearchHCJPData] 
(
    @LoaderId    INT = 1,
    @Mode        INT = 1,
    @CauseNo     VARCHAR(20) = '',
    @TicketNo    VARCHAR(20) = '',
    @CrtDate     DATETIME = NULL,
    @LastName    VARCHAR(20) = '',
    @FirstName   VARCHAR(20) = '',
    @MiddleName  VARCHAR(20) = '',
    @DOB         DATETIME = NULL
)
AS
	SET @LastName = '%' + @LastName + '%'               
	SET @FirstName = '%' + @FirstName + '%'    
	SET @MiddleName = '%' + @MiddleName + '%'
	
	--Farrukh 9664 01/09/2012 Added File Number filter
	DECLARE @FileNumber VARCHAR(20)
	SET @FileNumber = '%' + REPLACE(REPLACE(@CauseNo, ' ', ''), '-', '') + '%'
	
	-- Adil 7363 02/02/2010 removing special characters from ticket/cause number check
	SET @TicketNo = '%' + REPLACE(REPLACE((CASE WHEN LEN(@TicketNo) > 2 THEN LEFT(@TicketNo, LEN(@TicketNo)-1) ELSE @TicketNo END) , ' ', ''), '-', '') + '%'
	SET @CauseNo = '%' + REPLACE(REPLACE((CASE WHEN LEN(@CauseNo) > 2 THEN LEFT(@CauseNo, LEN(@CauseNo)-1) ELSE @TicketNo END), ' ', ''), '-', '') + '%'
	
	IF @LoaderId = 1 -- HCJP
	BEGIN
	    -- Noufil 5835 05/06/2009 Use top 1000 to remove out of memory error
	    SELECT TOP 1000 LAGroup.List_Date AS [List Date],
	           tbl_HCJP_DataFile.FiledDate AS [Filed Date],
	           tbl_HCJP_DataFile.NextEventDate AS [Court Date],
	           tbl_HCJP_DataFile.GroupID AS [Group Id],
	           tbl_HCJP_DataFile.CaseNumber AS [Cause Number],
	           tbl_HCJP_DataFile.JPCourtID AS [JP Court Id],
	           tbl_HCJP_DataFile.CitationNumber AS [Ticket Number],
	           tbl_HCJP_DataFile.OffenseCode AS [Offense Code],
	           tbl_HCJP_DataFile.OffenseDescription AS [Offense Description],
	           tbl_HCJP_DataFile.StatutoryCite1 AS [Statutory Cite 1],
	           tbl_HCJP_DataFile.StatutoryCite2 AS [Statutory Cite 2],
	           tbl_HCJP_DataFile.ArrestingAgencyCode AS [Arresting Agency Code],
	           tbl_HCJP_DataFile.ArrestingAgencyName AS [Arresting Agency Name],
	           tbl_HCJP_DataFile.OfficerCode AS [Officer Code],
	           tbl_HCJP_DataFile.OfficerName AS [Officer Name],
	           tbl_HCJP_DataFile.PleaCode AS [Plea Code],
	           tbl_HCJP_DataFile.PleaDate AS [Plea Date],
	           tbl_HCJP_DataFile.DispositionCode AS [Disposition Code],
	           tbl_HCJP_DataFile.DispositionDate AS [Disposition Date],
	           tbl_HCJP_DataFile.JudgmentDate AS [Judgment Date],
	           tbl_HCJP_DataFile.DefFirstName AS [First Name],
	           tbl_HCJP_DataFile.DefMiddleName AS [Middle Name],
	           tbl_HCJP_DataFile.DefLastName AS [Last Name],
	           tbl_HCJP_DataFile.DefSuffix AS [ Suffix],
	           tbl_HCJP_DataFile.DefHomeAddLine1 AS [Home Address 1],
	           tbl_HCJP_DataFile.DefHomeAddLine2 AS [ Home Address 2],
	           tbl_HCJP_DataFile.DefHomeAddCity AS [ City],
	           tbl_HCJP_DataFile.DefHomeAddState AS [ State],
	           tbl_HCJP_DataFile.DefHomeAddZIP1 AS [  ZIP 1],
	           tbl_HCJP_DataFile.DefHomeAreaCode AS [ Home Area Code],
	           tbl_HCJP_DataFile.DefHomePhoneNumber AS [ Phone Number],
	           tbl_HCJP_DataFile.DefWorkAddLine1 AS [ Work Address 1],
	           tbl_HCJP_DataFile.DefWorkAddLine2 AS [ Work Address 2],
	           tbl_HCJP_DataFile.DefWorkAddCity AS [ Work City],
	           tbl_HCJP_DataFile.DefWorkAddState AS [ Work State],
	           tbl_HCJP_DataFile.DefWorkAddZIP1 AS [ Work ZIP 1],
	           tbl_HCJP_DataFile.DefWorkAreaCode AS [ Work Area Code],
	           tbl_HCJP_DataFile.DefWorkPhoneNumber AS [ Phone Number],
	           tbl_HCJP_DataFile.DefDateofBirth AS [ DOB],
	           tbl_HCJP_DataFile.DefSPNNumber AS [ SPN Number],
	           tbl_HCJP_DataFile.DefSPNPointer AS [ SPN Pointer],
	           tbl_HCJP_DataFile.DefHeightFeet AS [ Height Feet],
	           tbl_HCJP_DataFile.DefWeight AS [ Weight],
	           tbl_HCJP_DataFile.DefGender AS [ Gender],
	           tbl_HCJP_DataFile.DefRace AS [ Race],
	           tbl_HCJP_DataFile.TotalFinesAssessed AS [Total Fines Assessed],
	           tbl_HCJP_DataFile.TotalFinesDue AS [Total Fines Due],
	           tbl_HCJP_DataFile.TotalCostsAssessed AS [Total Cost Assessed],
	           tbl_HCJP_DataFile.TotalCostsDue AS [Total Cost Due],
	           tbl_HCJP_DataFile.BalanceDue AS [Balance Due],
	           tbl_HCJP_DataFile.TotalPaid AS [Total Paid],
	           tbl_HCJP_DataFile.LastBondSetDate AS [Last Bond Set Date],
	           tbl_HCJP_DataFile.LastBondPostedDate AS [Last Bond Posted Date],
	           tbl_HCJP_DataFile.LastBondAmount AS [Last Bond Amount],
	           tbl_HCJP_DataFile.NextEventDesc AS [Next Event Desc],
	           tbl_HCJP_DataFile.NextEventTime AS [Next Event Time],
	           tbl_HCJP_DataFile.LastOpenWarrType AS [Last Open Warr Type],
	           tbl_HCJP_DataFile.LastOpenWarrDate AS [Last Open Warr Date],
	           tbl_HCJP_DataFile.rowid AS [Row Id]
	    FROM   tbl_HCJP_DataFile
	           INNER JOIN LA_Houston.dbo.Tbl_HTS_LA_Group AS LAGroup
	                ON  tbl_HCJP_DataFile.GroupID = LAGroup.GroupID
	    WHERE  (@Mode = 1)
	           AND (REPLACE(REPLACE(tbl_HCJP_DataFile.CitationNumber, ' ', ''), '-', '') LIKE @TicketNo) -- Adil 7363 02/02/2010 removing special characters from ticket/cause number check
	           OR  (@Mode = 2)
	           AND (REPLACE(REPLACE(tbl_HCJP_DataFile.CaseNumber, ' ', ''), '-', '') LIKE @CauseNo)
	           OR  (@Mode = 3)
	           AND (tbl_HCJP_DataFile.DefFirstName LIKE @FirstName)
	           AND (tbl_HCJP_DataFile.DefLastName LIKE @LastName)
	           AND (tbl_HCJP_DataFile.DefMiddleName LIKE @MiddleName)
	           AND (ISDATE(tbl_HCJP_DataFile.DefDateofBirth) = 1)
	           AND (DATEDIFF(DAY, tbl_HCJP_DataFile.DefDateofBirth, @DOB) = 0)
	           OR  (@Mode = 3)
	           AND (tbl_HCJP_DataFile.DefFirstName LIKE @FirstName)
	           AND (tbl_HCJP_DataFile.DefLastName LIKE @LastName)
	           AND (tbl_HCJP_DataFile.DefMiddleName LIKE @MiddleName)
	           AND (CONVERT(VARCHAR, @DOB, 101) = '01/01/1900')
	           OR  (@Mode = 4)
	           AND (ISDATE(tbl_HCJP_DataFile.NextEventDate) = 1)
	           AND (
	                   DATEDIFF(DAY, tbl_HCJP_DataFile.NextEventDate, @CrtDate) 
	                   = 0
	               )
	           OR  (@Mode = 5)
	           AND (ISDATE(tbl_HCJP_DataFile.FiledDate) = 1)
	           AND (DATEDIFF(DAY, tbl_HCJP_DataFile.FiledDate, @CrtDate) = 0)
	    ORDER BY tbl_HCJP_DataFile.GroupID, tbl_HCJP_DataFile.rowid
	END
	ELSE 
	IF @LoaderId = 2 -- Arraignment
	BEGIN
	    -- Noufil 5835 05/06/2009 Use top 1000 to remove out of memory error
	    SELECT TOP 1000 RowId AS [Row Id],
	           tdf.FileDate AS [File Date],
	           causenumber AS [Casue Number],
	           midnumber AS [Mid Number],
	           firstname AS [First Name],
	           initial AS M,
	           lastname AS [Last Name],
	           address1 AS [Address 1],
	           address2 AS [Address 2],
	           city AS City,
	           STATE AS STATE,
	           zip AS Zip,
	           phone AS Phone,
	           dob AS DOB,
	           violationdate AS [Violation Date],
	           courtdate AS [Court Date],
	           officernumber AS [Officer Number],
	           fineamount AS [Fine Amount],
	           race AS Race,
	           height AS Height,
	           gender AS Gender,
	           violation AS Violation,
	           violationcode AS [Violation Code],
	           ticketnumber AS [Ticket Number],
	           courtnum AS [Court Number],
	           TIME AS TIME,
	           bondamount AS [Bond Amount],
	           offenselocation AS [Offense Location],
	           offensetime AS [Offense Time],
	           officer_firstname AS [Officer First Name],
	           officer_lastname AS [Officer LastName],
	           tl.InsertDate AS [Insert Date],
	           tl.FileId AS [File Id]
	    FROM   tblLubbock tl
	           INNER JOIN tblDataFiles tdf
	                ON  tdf.FileId = tl.FileId
	    WHERE  (@Mode = 1 AND REPLACE(REPLACE(ticketnumber, ' ', ''), '-', '') LIKE @TicketNo)-- Adil 7363 02/02/2010 removing special characters from ticket/cause number check
	           OR  (@Mode = 2 AND REPLACE(REPLACE(causenumber, ' ', ''), '-', '') LIKE @CauseNo)
	           OR  (
	                   @Mode = 3
	                   AND firstname LIKE @FirstName
	                   AND lastname LIKE @LastName
	                   AND initial LIKE @MiddleName
	                   AND (
	                           (ISDATE(dob) = 1 AND DATEDIFF(DAY, dob, @DOB) = 0)
	                           OR CONVERT(VARCHAR, @DOB, 101) = '01/01/1900'
	                       )
	               )
	           OR  (
	                   @Mode = 4
	                   AND ISDATE(tdf.FileDate) = 1
	                   AND DATEDIFF(DAY, tdf.FileDate, @CrtDate) = 0
	               )
	           OR  (
	                   @Mode = 5
	                   AND ISDATE(courtdate) = 1
	                   AND DATEDIFF(DAY, courtdate, @CrtDate) = 0
	           )
	    ORDER BY tl.FileId, tl.RowId
	END
	ELSE 
	IF @LoaderId = 3 -- DLQ
	BEGIN
	    -- Noufil 5835 05/06/2009 Use top 1000 to remove out of memory error
	    SELECT TOP 1000 RowId AS [Row Id],
	           tdf.FileDate AS [File Date],
	           causenumber AS [Cause Number],
	           midnumber AS [Mid Number],
	           firstname AS [First Name],
	           initial AS M,
	           lastname AS [Last Name],
	           address1 AS [Address 1],
	           address2 AS [Address 2],
	           city AS City,
	           STATE AS STATE,
	           zip AS ZIP,
	           phone AS Phone,
	           dob AS DOB,
	           violationdate AS [Violation Date],
	           courtdate AS [Court Date],
	           officernum AS [Officer Number],
	           fineamount AS [Fine Amount],
	           race AS Race,
	           height AS Height,
	           gender AS Gender,
	           violation AS Violation,
	           violationcode AS [Violation Code],
	           ticketnumber AS [Ticket Number],
	           courtnum AS [Court Number],
	           TIME AS TIME,
	           STATUS AS STATUS,
	           associatedcasenumber AS [Ass. Case Number],
	           bondamount AS [Bond Amount],
	           tdl.InsertDate AS [Insert Date],
	           tdl.FileId AS [File Id]
	    FROM   tblDLQ tdl
	           INNER JOIN tblDataFiles tdf
	                ON  tdf.FileId = tdl.FileId
	    WHERE  (@Mode = 1 AND REPLACE(REPLACE(ticketnumber, ' ', ''), '-', '') LIKE @TicketNo)-- Adil 7363 02/02/2010 removing special characters from ticket/cause number check
	           OR  (@Mode = 2 AND REPLACE(REPLACE(causenumber, ' ', ''), '-', '') LIKE @CauseNo)
	           OR  (
	                   @Mode = 3
	                   AND firstname LIKE @FirstName
	                   AND lastname LIKE @LastName
	                   AND initial LIKE @MiddleName
	                   AND (
	                           (ISDATE(dob) = 1 AND DATEDIFF(DAY, dob, @DOB) = 0)
	                           OR CONVERT(VARCHAR, @DOB, 101) = '01/01/1900'
	                       )
	               )
	           OR  (
	                   @Mode = 4
	                   AND ISDATE(tdf.FileDate) = 1
	                   AND DATEDIFF(DAY, tdf.FileDate, @CrtDate) = 0
	               )
	           OR  (
	                   @Mode = 5
	                   AND ISDATE(courtdate) = 1
	                   AND DATEDIFF(DAY, courtdate, @CrtDate) = 0
	           )
	    ORDER BY tdl.FileId, tdl.RowId
	END
	ELSE 
	IF @LoaderId = 4 -- Events
	BEGIN
	    -- Noufil 5835 05/06/2009 Use top 1000 to remove out of memory error
	    SELECT TOP 1000 RowId AS [Row ID],
	           tdf.FileDate AS [File Date],
	           causenumber AS [Cause Number],
	           ticketnumber AS [Ticket Number],
	           STATUS AS STATUS,
	           courtnumber AS [Court Number],
	           courtdate AS [Court Date],
	           courttime AS [Court Time],
	           updatedate AS [Update Date],
	           associatedcasenumber AS [Ass. Case number],
	           barnumber AS [Bar Number],
	           bondingnumber AS [Bonding Number],
	           attorneyname AS [Attorney Name],
	           bondingcompanyname AS [Bonding Company Name],
	           tem.InsertDate AS [Insert Date],
	           tem.FileId AS [File Id]
	    FROM   tblEventExtractTemp tem
	           INNER JOIN tblDataFiles tdf
	                ON  tdf.FileId = tem.FileId
	    WHERE  (@Mode = 1 AND REPLACE(REPLACE(ticketnumber, ' ', ''), '-', '') LIKE @TicketNo)-- Adil 7363 02/02/2010 removing special characters from ticket/cause number check
	           OR  (@Mode = 2 AND REPLACE(REPLACE(causenumber, ' ', ''), '-', '') LIKE @CauseNo)
	           OR  (
	                   @Mode = 4
	                   AND ISDATE(tdf.FileDate) = 1
	                   AND DATEDIFF(DAY, tdf.FileDate, @CrtDate) = 0
	               )
	           OR  (
	                   @Mode = 5
	                   AND ISDATE(courtdate) = 1
	                   AND DATEDIFF(DAY, courtdate, @CrtDate) = 0
	           )
	    ORDER BY tem.FileId, tem.RowId
	END
	ELSE 
	IF @LoaderId = 5 -- Warrant
	BEGIN
	    -- Noufil 5835 05/06/2009 Use top 1000 to remove out of memory error
	    SELECT TOP 1000 RowId AS [Row Id],
	           tdf.FileDate AS [File Date],
	           linkid AS [Link Id],
	           causenumber AS [Cause Number],
	           midnumber AS [Mid Number],
	           updatedate AS [Update Date],
	           ticketnumber AS [Ticket Number],
	           sequencenumber AS [Sequence Number],
	           tlf.InsertDate AS [Insert Date],
	           tlf.FileId AS [File Id]
	    FROM   tblLubbockFTA tlf
	           INNER JOIN tblDataFiles tdf
	                ON  tdf.FileId = tlf.FileId
	    WHERE  (@Mode = 1 AND REPLACE(REPLACE(ticketnumber, ' ', ''), '-', '') LIKE @TicketNo)
	           OR  (@Mode = 2 AND REPLACE(REPLACE(causenumber, ' ', ''), '-', '') LIKE @CauseNo)
	           OR  (
	                   @Mode = 4
	                   AND ISDATE(tdf.FileDate) = 1
	                   AND DATEDIFF(DAY, tdf.FileDate, @CrtDate) = 0
	           )
	    ORDER BY tlf.FileId, tlf.RowId
	END-- Noufil 5335 12/19/2008 New report for Sugarland added
	ELSE 
	IF @LoaderId = 6 -- Sugarland
	BEGIN
	    -- Noufil 5835 05/06/2009 Use top 1000 to remove out of memory error
	    SELECT DISTINCT TOP 1000
	           sug.ID AS ID,
	           CONVERT(VARCHAR, LAGroup.Load_Date, 101) AS [File Date],
	           Sug.LastName AS [Last Name],
	           Sug.FirstName AS [First Name],
	           Sug.TicketNumber AS [Ticket No],
	           Sug.AddressPartI AS [Address 1],
	           Sug.AddressPartII AS [Adress 2],
	           Sug.City,
	           Sug.[State],
	           Sug.ZipCode,
	           CONVERT(VARCHAR, Sug.Court_Date, 101) AS [Crt.Date],
	           Sug.Violation_Description AS [Description],
	           Sug.DP2,
	           Sug.DPC,
	           Sug.Address_Status AS [Add.Status],
	           Sug.Reliable,
	           CONVERT(VARCHAR, Sug.InsertDate, 101) AS [Insert Date],
	           sug.GroupID
	    FROM   LoaderFilesArchive.dbo.Sugarland_DataArchive Sug
	           INNER JOIN LA_Houston.dbo.Tbl_HTS_LA_Group AS LAGroup
	                ON  Sug.GroupID = LAGroup.GroupID
	    WHERE  (@Mode = 1)
	           AND (REPLACE(REPLACE(sug.TicketNumber, ' ', ''), '-', '') LIKE @TicketNo)-- Adil 7363 02/02/2010 removing special characters from ticket/cause number check
	           OR  (@Mode = 2)
	           AND (REPLACE(REPLACE(sug.TicketNumber, ' ', ''), '-', '') LIKE @CauseNo)
	           OR  (
	                   (@Mode = 3)
	                   AND (
	                           (
	                               RTRIM(LTRIM(@LastName)) = '%'
	                               AND (RTRIM(LTRIM(sug.FirstName)) LIKE @FirstName)
	                           )
	                           OR (
	                                  RTRIM(LTRIM(@FirstName)) = '%'
	                                  AND RTRIM(LTRIM(sug.LastName)) LIKE @LastName
	                              )
	                           OR (
	                                  RTRIM(LTRIM(sug.FirstName)) LIKE @FirstName
	                                  AND RTRIM(LTRIM(sug.LastName)) LIKE @LastName
	                              )
	                       )
	               )
	           OR  (@Mode = 4)
	           AND (ISDATE(LAGroup.Load_Date) = 1)
	           AND (
	                   DATEDIFF(DAY, RTRIM(LTRIM(LAGroup.Load_Date)), @CrtDate) 
	                   = 0
	               )
	           OR  (@Mode = 5)
	           AND (ISDATE(sug.Court_Date) = 1)
	           AND (DATEDIFF(DAY, sug.Court_Date, @CrtDate) = 0)
	    ORDER BY Sug.GroupID, Sug.ID
	END-- Adil 5374 1/1/2009 HMC Dispossed cases raw data search
	ELSE 
	IF @LoaderId = 7 -- Dispossed
	BEGIN
	    -- Noufil 5835 05/06/2009 Use top 1000 to remove out of memory error
	    SELECT TOP 1000 RowId AS [Row Id],
	           tdf.FileDate AS [File Date],
	           causenumber AS [Cause Number],
	           ticketnumber AS [Ticket Number],
	           firstname AS [First Name],
	           initial AS M,
	           lastname AS [Last Name],
	           tl.closedate AS [Dispose Date],
	           tl.closecomments AS [Disposition Comments],
	           tl.InsertDate AS [Insert Date],
	           tl.FileId AS [File Id]
	    FROM   tblClosedCases tl
	           INNER JOIN tblDataFiles tdf
	                ON  tdf.FileId = tl.FileId
	    WHERE  (@Mode = 1 AND REPLACE(REPLACE(ticketnumber, ' ', ''), '-', '') LIKE @TicketNo)-- Adil 7363 02/02/2010 removing special characters from ticket/cause number check
	           OR  (@Mode = 2 AND REPLACE(REPLACE(causenumber, ' ', ''), '-', '') LIKE @CauseNo)
	           OR  (
	                   @Mode = 4
	                   AND ISDATE(tdf.FileDate) = 1
	                   AND DATEDIFF(DAY, tdf.FileDate, @CrtDate) = 0
	           )
	    ORDER BY tl.FileId, tl.RowId
	END--Fahad 5581 02/27/2009 Add data loader for fort worth Court
	ELSE 
	IF @LoaderId = 8 -- Fort Worth
	BEGIN
	    -- Noufil 5835 05/06/2009 Use top 1000 to remove out of memory error
	    SELECT TOP 1000 FW.ID AS [Row ID],
	           CONVERT(VARCHAR, LAGroup.Load_Date, 101) AS [File Date],
	           FW.CauseNumber AS [Cause Number],
	           FW.TicketNumber AS [Ticket Number],
	           FW.First_Name AS [First Name],
	           FW.Last_Name AS [Last Name],
	           traffictickets.dbo.fn_DateFormat(FW.Violation_Date, 4, '/', ':', 1) AS [Violation Date],
	           traffictickets.dbo.fn_DateFormat(FW.Filed_Date, 4, '/', ':', 1) AS Filed_Date, -- Adil 8006 07/14/2010 Filed date added.
	           FW.ViolationCode AS [Violation Code],
	           FW.Violation_Description AS [Violation Desc.],
	           FW.AddressPartI AS [Address],
	           FW.City_State AS [City/State],
	           FW.GroupID AS [Group ID]
	    FROM   dbo.FortWorth_DataArchive FW
	           INNER JOIN LA_Houston.dbo.Tbl_HTS_LA_Group AS LAGroup
	                ON  FW.GroupID = LAGroup.GroupID
	    WHERE  (
	               (@Mode = 1)
	               AND REPLACE(REPLACE(FW.TicketNumber, ' ', ''), '-', '') LIKE @TicketNo-- Adil 7363 02/02/2010 removing special characters from ticket/cause number check
	           )
	           OR  (
	                   (@Mode = 2)
	                   AND REPLACE(REPLACE(FW.CauseNumber, ' ', ''), '-', '') LIKE @CauseNo
	               )
	           OR  (
	                   (@Mode = 3)
	                   AND (
	                           (
	                               RTRIM(LTRIM(@LastName)) = '%'
	                               AND (RTRIM(LTRIM(FW.First_Name)) LIKE @FirstName)
	                           )
	                           OR (
	                                  RTRIM(LTRIM(@FirstName)) = '%'
	                                  AND RTRIM(LTRIM(FW.Last_Name)) LIKE @LastName
	                              )
	                           OR (
	                                  RTRIM(LTRIM(FW.First_Name)) LIKE @FirstName
	                                  AND RTRIM(LTRIM(FW.Last_Name)) LIKE @LastName
	                              )
	                       )
	               )
	           OR  (@Mode = 4)
	           AND (ISDATE(LAGroup.Load_Date) = 1)
	           AND (
	                   DATEDIFF(DAY, RTRIM(LTRIM(LAGroup.Load_Date)), @CrtDate) 
	                   = 0
	           )
	    ORDER BY FW.GroupID, FW.ID
	END--Fahad 5835 06/04/2009 Add follwing cases
	ELSE 
	IF @LoaderId = 9 -- DMC-Arraignment
	BEGIN
	    -- Noufil 5835 05/06/2009 Use top 1000 to remove out of memory error
	    SELECT TOP 1000 RowID AS [Row Id],
	           [Ticket Number] AS [Ticket Number],
	           [Last Name],
	           [First Name],
	           [Address Part I] AS [Address 1],
	           [Address Part II] AS [Address 2],
	           City,
	           [State] AS STATE,
	           [ZIP Code] AS Zip,
	           [Violation Description] AS [Description],
	           [Violation Date],
	           [Violation Code],
	           Race,
	           Gender,
	           CONVERT(
	               VARCHAR,
	               (
	                   CONVERT(
	                       DATETIME,
	                       (
	                           (SUBSTRING(RTRIM(LTRIM(DOB)), 1, 2)) + '/' + (SUBSTRING(RTRIM(LTRIM(DOB)), 3, 2))
	                           + '/' + (SUBSTRING(RTRIM(LTRIM(DOB)), 5, 2))
	                       )
	                   )
	               ),
	               101
	           ) AS DOB,
	           --CONVERT(VARCHAR, CAST(DOB AS DATETIME), 101) AS DOB,
	           dada.GroupID,
	           CONVERT(VARCHAR, Insert_Date, 101) AS [Insert Date],
	           CONVERT(VARCHAR, thlg.Load_Date, 101) AS [File Date]
	    FROM   Dallas_Arraingment_DataArchive dada
	           INNER JOIN LA_Houston.dbo.Tbl_HTS_LA_Group thlg
	                ON  thlg.GroupID = dada.GroupID
	    WHERE  (@Mode = 1 AND REPLACE(REPLACE([Ticket Number], ' ', ''), '-', '') LIKE @TicketNo)-- Adil 7363 02/02/2010 removing special characters from ticket/cause number check
	           OR  (
	                   @Mode = 3
	                   AND [First Name] LIKE @FirstName
	                   AND [Last Name] LIKE @LastName
	                   AND (
	                           --(ISDATE(DOB) = 1 AND DATEDIFF(DAY, DOB, @DOB) = 0)
	                           --OR CONVERT(VARCHAR, @DOB, 101) = '01/01/1900'
	                           (
	                               ISDATE(
	                                   CONVERT(
	                                       VARCHAR,
	                                       (
	                                           CONVERT(
	                                               DATETIME,
	                                               (
	                                                   (SUBSTRING(RTRIM(LTRIM(DOB)), 1, 2))
	                                                   + '/' + (SUBSTRING(RTRIM(LTRIM(DOB)), 3, 2))
	                                                   + '/' + (SUBSTRING(RTRIM(LTRIM(DOB)), 5, 2))
	                                               )
	                                           )
	                                       ),
	                                       101
	                                   )
	                               ) = 1
	                               AND DATEDIFF(
	                                       DAY,
	                                       CONVERT(
	                                           VARCHAR,
	                                           (
	                                               CONVERT(
	                                                   DATETIME,
	                                                   (
	                                                       (SUBSTRING(RTRIM(LTRIM(DOB)), 1, 2))
	                                                       + '/' + (SUBSTRING(RTRIM(LTRIM(DOB)), 3, 2))
	                                                       + '/' + (SUBSTRING(RTRIM(LTRIM(DOB)), 5, 2))
	                                                   )
	                                               )
	                                           ),
	                                           101
	                                       ),
	                                       @DOB
	                                   ) = 0
	                           )
	                           OR CONVERT(VARCHAR, @DOB, 101) = '01/01/1900'
	                       )
	               )
	           OR  (
	                   @Mode = 4
	                   AND ISDATE(thlg.Load_Date) = 1
	                   AND DATEDIFF(DAY, thlg.Load_Date, @CrtDate) = 0
	           )
	    ORDER BY dada.GroupID, dada.RowID
	END
	ELSE 
	IF @LoaderId = 10-- DMC-Bonds
	BEGIN
	    -- Noufil 5835 05/06/2009 Use top 1000 to remove out of memory error
	    SELECT TOP 1000 RowID AS [Row Id],
	           [Ticket Number],
	           [Last NAME],
	           [First Name],
	           [Address Part I] AS [Address 1],
	           [Address Part II] AS [Address 2],
	           City,
	           STATE AS STATE,
	           [ZIP Code] AS Zip,
	           [Violation DESCRIPTION] AS [Description],
	           CONVERT(
	               VARCHAR,
	               (
	                   CONVERT(
	                       DATETIME,
	                       (
	                           (SUBSTRING(RTRIM(LTRIM([Violation Date])), 1, 2))
	                           + '/' + (SUBSTRING(RTRIM(LTRIM([Violation Date])), 3, 2))
	                           + '/' + (SUBSTRING(RTRIM(LTRIM([Violation Date])), 5, 2))
	                       )
	                   )
	               ),
	               101
	           ) AS [Violation Date],
	           [Violation Code],
	           Race,
	           Gender,
	           CONVERT(
	               VARCHAR,
	               (
	                   CONVERT(
	                       DATETIME,
	                       (
	                           (SUBSTRING(RTRIM(LTRIM(DOB)), 1, 2)) + '/' + (SUBSTRING(RTRIM(LTRIM(DOB)), 3, 2))
	                           + '/' + (SUBSTRING(RTRIM(LTRIM(DOB)), 5, 2))
	                       )
	                   )
	               ),
	               101
	           ) AS DOB,
	           dbd.GroupID,
	           CONVERT(VARCHAR, Insert_Date, 101) AS [Insert Date],
	           CONVERT(VARCHAR, thlg.Load_Date, 101) AS [File Date]
	    FROM   Dallas_bond_DataArchive dbd
	           INNER JOIN LA_Houston.dbo.Tbl_HTS_LA_Group thlg
	                ON  thlg.GroupID = dbd.GroupID
	    WHERE  (@Mode = 1 AND REPLACE(REPLACE([Ticket Number], ' ', ''), '-', '') LIKE @TicketNo)-- Adil 7363 02/02/2010 removing special characters from ticket/cause number check
	           OR  (
	                   @Mode = 3
	                   AND [First Name] LIKE @FirstName
	                   AND [Last Name] LIKE @LastName
	                   AND (
	                           (
	                               ISDATE(
	                                   CONVERT(
	                                       VARCHAR,
	                                       (
	                                           CONVERT(
	                                               DATETIME,
	                                               (
	                                                   (SUBSTRING(RTRIM(LTRIM(DOB)), 1, 2))
	                                                   + '/' + (SUBSTRING(RTRIM(LTRIM(DOB)), 3, 2))
	                                                   + '/' + (SUBSTRING(RTRIM(LTRIM(DOB)), 5, 2))
	                                               )
	                                           )
	                                       ),
	                                       101
	                                   )
	                               ) = 1
	                               AND DATEDIFF(
	                                       DAY,
	                                       CONVERT(
	                                           VARCHAR,
	                                           (
	                                               CONVERT(
	                                                   DATETIME,
	                                                   (
	                                                       (SUBSTRING(RTRIM(LTRIM(DOB)), 1, 2))
	                                                       + '/' + (SUBSTRING(RTRIM(LTRIM(DOB)), 3, 2))
	                                                       + '/' + (SUBSTRING(RTRIM(LTRIM(DOB)), 5, 2))
	                                                   )
	                                               )
	                                           ),
	                                           101
	                                       ),
	                                       @DOB
	                                   ) = 0
	                           )
	                           OR CONVERT(VARCHAR, @DOB, 101) = '01/01/1900'
	                       )
	               )
	           OR  (
	                   @Mode = 4
	                   AND ISDATE(thlg.Load_Date) = 1
	                   AND DATEDIFF(DAY, RTRIM(LTRIM(thlg.Load_Date)), @CrtDate) 
	                       = 0
	           )
	    ORDER BY dbd.GroupID, dbd.RowID
	END
	ELSE 
	IF @LoaderId = 12-- DCJP
	BEGIN
	    -- Noufil 5835 05/06/2009 Use top 1000 to remove out of memory error
	    SELECT TOP 1000 RowID AS [Row Id],
	           [Ticket Number],
	           [Cause Number],
	           [Last Name],
	           [First Name],
	           [Middle Name],
	           [Address Part I] AS [Address 1],
	           City,
	           [State],
	           [ZIP Code] AS Zip,
	           PrecinctID,
	           [Violation Description],
	           [Violation Date],
	           [Violation Code],
	           [Fine Amount],
	           [Bond Amount],
	           [Officer Name],
	           [Complainant Agency],
	           dbd.GroupID,
	           CONVERT(VARCHAR, Insert_Date, 101) AS [Insert Date],
	           CONVERT(VARCHAR, thlg.Load_Date, 101) AS [File Date]
	    FROM   Dallas_County_JP_Court_DataArchive dbd
	           INNER JOIN LA_Houston.dbo.Tbl_HTS_LA_Group thlg
	                ON  thlg.GroupID = dbd.GroupID
	    WHERE  (@Mode = 1 AND REPLACE(REPLACE([Ticket Number], ' ', ''), '-', '') LIKE @TicketNo)-- Adil 7363 02/02/2010 removing special characters from ticket/cause number check
	           OR  (@Mode = 2 AND REPLACE(REPLACE([Cause Number], ' ', ''), '-', '') LIKE @CauseNo)
	           OR  (
	                   @Mode = 3
	                   AND [First Name] LIKE @FirstName
	                   AND [Last Name] LIKE @LastName
	                   AND [Middle Name] LIKE @MiddleName
	               )
	           OR  (
	                   @Mode = 4
	                   AND ISDATE(thlg.List_Date) = 1
	                   AND DATEDIFF(DAY, thlg.Load_Date, @CrtDate) = 0
	           )
	    ORDER BY dbd.GroupID, dbd.RowID
	END
	ELSE 
	IF @LoaderId = 13-- DCCC-PTH
	BEGIN
	    -- Noufil 5835 05/06/2009 Use top 1000 to remove out of memory error
	    SELECT TOP 1000 RowID AS [Row Id],
	           [Ticket Number],
	           [Last Name],
	           [First Name],
	           [Address Part I] AS [Address 1],
	           City,
	           [State],
	           [Violation Description],
	           [Setting Status],
	           CONVERT(VARCHAR, CAST([Court Date]AS DATETIME), 101) AS 
	           [Court Date],
	           [Court Time],
	           Location,
	           Race,
	           Gender,
	           --CONVERT(VARCHAR, CAST(DOB AS DATETIME), 101) AS DOB, Afaq 7380 3/25/2010 check for correct date
			   CASE WHEN ISDATE(dob) = 1 THEN CONVERT(VARCHAR, CAST(DOB AS DATETIME), 101) ELSE dob END  AS DOB,
	           Comments,
	           dccc.GroupID,
	           CONVERT(VARCHAR, Insert_Date, 101) AS [Insert Date],
	           CONVERT(VARCHAR, thlg.Load_Date, 101) AS [File Date]
	    FROM   Dallas_County_Criminal_Court_DataArchive dccc
	           INNER JOIN LA_Houston.dbo.Tbl_HTS_LA_Group thlg
	                ON  thlg.GroupID = dccc.GroupID
	    WHERE  (@Mode = 1 AND REPLACE(REPLACE([Ticket Number], ' ', ''), '-', '') LIKE @TicketNo)-- Adil 7363 02/02/2010 removing special characters from ticket/cause number check
	           OR  (
	                   @Mode = 3
	                   AND [First Name] LIKE @FirstName
	                   AND [Last Name] LIKE @LastName
	                   AND (
	                           (ISDATE(DOB) = 1 AND DATEDIFF(DAY, DOB, @DOB) = 0)
	                           OR CONVERT(VARCHAR, @DOB, 101) = '01/01/1900'
	                       )
	               )
	           OR  (
	                   @Mode = 4
	                   AND ISDATE(thlg.Load_Date) = 1
	                   AND DATEDIFF(DAY, thlg.Load_Date, @CrtDate) = 0
	               )
	           OR  (
	                   @Mode = 5
	                   AND ISDATE([Court Date]) = 1
	                   AND DATEDIFF(DAY, [Court Date], @CrtDate) = 0
	           )
	    ORDER BY dccc.GroupID, dccc.RowID
	END
	ELSE 
	IF @LoaderId = 14-- DCCC-BI
	BEGIN
	    -- Noufil 5835 05/06/2009 Use top 1000 to remove out of memory error
	    SELECT TOP 1000 Rowid AS [Row Id],
	           [Violation Date],
	           [Ticket Number],
	           [Last Name],
	           [First Name],
	           [Middle Name], -- Abbas Qamar 9266 07/26/2011 Adding Middle name
	           Race,
	           Gender,
	            --CONVERT(VARCHAR, CAST(DOB AS DATETIME), 101) AS DOB, Afaq 7380 3/25/2010 check for correct date
			   CASE WHEN ISDATE(dob) = 1 THEN CONVERT(VARCHAR, CAST(DOB AS DATETIME), 101) ELSE dob END  AS DOB,
	           [Address Part I] AS [Address 1],
	           [Address Part II] AS [Address 2],
	           City,
	           [State],
	           [Zip Code] AS Zip,
	           DP2,
	           DPC,
	           [Address Status],
	           [Violation Description],
	           Reliable,
	           dccc.GroupID,
	           Recdate,
	           CONVERT(VARCHAR, thlg.Load_Date, 101) AS [File Date]
	    FROM   tbl_DallasCrminal_BookedIn dccc
	           INNER JOIN LA_Houston.dbo.Tbl_HTS_LA_Group thlg
	                ON  thlg.GroupID = dccc.GroupID
	    WHERE  (@Mode = 1 AND REPLACE(REPLACE([Ticket Number], ' ', ''), '-', '') LIKE @TicketNo)-- Adil 7363 02/02/2010 removing special characters from ticket/cause number check
	           OR  (
	                   @Mode = 3
	                   AND [First Name] LIKE @FirstName
	                   AND [Last Name] LIKE @LastName
	                   AND (
	                           (ISDATE(DOB) = 1 AND DATEDIFF(DAY, DOB, @DOB) = 0)
	                           OR CONVERT(VARCHAR, @DOB, 101) = '01/01/1900'
	                       )
	               )
	           OR  (
	                   @Mode = 4
	                   AND ISDATE(thlg.Load_Date) = 1
	                   AND DATEDIFF(DAY, thlg.Load_Date, @CrtDate) = 0
	           )
	    ORDER BY dccc.GroupID, dccc.Rowid
	END
	ELSE
	IF @LoaderId = 15--Irving-Arraignment
	BEGIN
	    -- Noufil 5835 05/06/2009 Use top 1000 to remove out of memory error
	    SELECT TOP 1000 RowID AS [Row Id],
	           [Ticket Number],
	           [Last Name],
	           [Middle Name],
	           [First Name],
	           [Address Part I] AS [Address 1],
	           City,
	           [State],
	           [ZIP Code] AS Zip,
	           [Violation Description],
	           [Violation Date],
	           [Violation Number],
	           [Violation Code],
	           CIT_FILED_DATE,
	           COD_OFF_TYPE,
	           dccc.GroupID,
	           CONVERT(VARCHAR, Insert_Date, 101) AS [Insert Date],
	           CONVERT(VARCHAR, thlg.Load_Date, 101) AS [File Date]
	    FROM   Irving_Arraignment_Datarchive dccc
	           INNER JOIN LA_Houston.dbo.Tbl_HTS_LA_Group thlg
	                ON  thlg.GroupID = dccc.GroupID
	    WHERE  (@Mode = 1 AND REPLACE(REPLACE([Ticket Number], ' ', ''), '-', '') LIKE @TicketNo)-- Adil 7363 02/02/2010 removing special characters from ticket/cause number check
	           OR  (
	                   @Mode = 3
	                   AND [First Name] LIKE @FirstName
	                   AND [Last Name] LIKE @LastName
	                   AND [Middle Name] LIKE @MiddleName
	               )
	           OR  (
	                   @Mode = 4
	                   AND ISDATE(thlg.Load_Date) = 1
	                   AND DATEDIFF(DAY, thlg.Load_Date, @CrtDate) = 0
	           )
	    ORDER BY dccc.RowID
	END
	ELSE
	IF @LoaderId = 16--Irving-Bonds
	BEGIN
	    -- Noufil 5835 05/06/2009 Use top 1000 to remove out of memory error
	    SELECT TOP 1000 RowID AS [Row Id],
	           [Ticket Number],
	           [Last Name],
	           [First Name],
	           [Middle Name],
	           [Address part I] AS [Address 1],
	           City,
	           [State],
	           [ZIP Code] AS Zip,
	           [Violation Description],
	           [Violation Date],
	           [Violation Number],
	           [Violation Code],
	           VIOL_STATUS,
	           WARN_DATE_ISSUED,
	           Officer_Name,
	           dccc.GroupID,
	           CONVERT(VARCHAR, InsertDate, 101) AS [Insert Date],
	           CONVERT(VARCHAR, thlg.Load_Date, 101) AS [File Date]
	    FROM   Irving_DataArchive dccc
	           INNER JOIN LA_Houston.dbo.Tbl_HTS_LA_Group thlg
	                ON  thlg.GroupID = dccc.GroupID
	    WHERE  (@Mode = 1 AND REPLACE(REPLACE([Ticket Number], ' ', ''), '-', '') LIKE @TicketNo)-- Adil 7363 02/02/2010 removing special characters from ticket/cause number check
	           OR  (
	                   @Mode = 3
	                   AND [First Name] LIKE @FirstName
	                   AND [Last Name] LIKE @LastName
	                   AND [Middle Name] LIKE @MiddleName
	               )
	           OR  (
	                   @Mode = 4
	                   AND ISDATE(thlg.Load_Date) = 1
	                   AND DATEDIFF(DAY, thlg.Load_Date, @CrtDate) = 0
	           )
	    ORDER BY dccc.RowID
	END
	ELSE
	IF @LoaderId = 17
	   OR @LoaderId = 18
	   OR @LoaderId = 19--HCJP-Entered or HCJP_EVENTS or HCJP_DISPOSED
	BEGIN
	    DECLARE @searchcriterea VARCHAR(50)
	    IF @LoaderId = 17
	        SET @searchcriterea = 'Entered'
	    
	    IF @LoaderId = 18
	        SET @searchcriterea = 'Events'
	    
	    IF @LoaderId = 19
	        SET @searchcriterea = 'Disposed'
	    -- Noufil 5835 05/06/2009 Use top 1000 to remove out of memory error
	    SELECT TOP 1000 rowid AS [Row Id],
	           CaseNumber AS [Cause Number],
	           CitationNumber AS [Ticket Number],
	           FiledDate AS [List Date],
	           OffenseCode,
	           OffenseDescription,
	           DefLastName AS [Last Name],
	           DefFirstName AS [First Name],
	           DefMiddleName AS [Middle Name],
	           DefDateofBirth AS DOB,
	           DefHomePhoneNumber,
	           DefWorkPhoneNumber,
	           DefHeightFeet,
	           DefWeight,
	           DefGender AS Gender,
	           DefRace AS Race,
	           TotalFinesAssessed,
	           TotalCostsAssessed,
	           StatutoryCite1,
	           StatutoryCite2,
	           OfficerCode,
	           OfficerName,
	           ArrestingAgencyCode,
	           ArrestingAgencyName,
	           PleaCode,
	           PleaDate,
	           DispositionCode,
	           DispositionDate,
	           JudgmentDate,
	           DefHomeAreaCode,
	           DefWorkAddLine1,
	           DefWorkAddLine2,
	           DefWorkAddCity,
	           DefWorkAddState,
	           DefWorkAddZIP1,
	           DefWorkAreaCode,
	           DefSPNNumber,
	           DefSPNPointer,
	           LastOpenWarrType,
	           LastOpenWarrDate,
	           NextEventDesc,
	           NextEventDate AS [Court Date],
	           NextEventTime,
	           LastBondSetDate,
	           LastBondAmount,
	           DefHomeAddLine1,
	           DefHomeAddLine2,
	           DefHomeAddCity,
	           DefHomeAddState,
	           DefHomeAddZIP1,
	           TotalPaid,
	           BalanceDue,
	           TotalFinesDue,
	           TotalCostsDue,
	           LastBondPostedDate,
	           DefSuffix,
	           dccc.GroupID,
	           JPCourtID,
	           CONVERT(VARCHAR, [Insertion Date], 101) AS [Insert Date],
	           CONVERT(VARCHAR, thlg.Load_Date, 101) AS [File Date]
	    FROM   tbl_HCJP_DataFile dccc
	           INNER JOIN LA_Houston.dbo.Tbl_HTS_LA_Group thlg
	                ON  thlg.GroupID = dccc.GroupID
	    WHERE  (
	               (
	                   @Mode = 1
	                   AND REPLACE(REPLACE(dccc.CitationNumber, ' ', ''), '-', '') LIKE @TicketNo-- Adil 7363 02/02/2010 removing special characters from ticket/cause number check
	               )
	               OR (@Mode = 2 AND REPLACE(REPLACE(dccc.CaseNumber, ' ', ''), '-', '') LIKE @CauseNo)
	               OR (
	                      @Mode = 3
	                      AND LTRIM(RTRIM(DefFirstName)) LIKE @FirstName
	                      AND LTRIM(RTRIM(DefLastName)) LIKE @LastName
	                      AND LTRIM(RTRIM(DefMiddleName)) LIKE @MiddleName
	                      AND (
	                              (
	                                  ISDATE(DefDateofBirth) = 1
	                                  AND DATEDIFF(DAY, DefDateofBirth, @DOB) =
	                                      0
	                              )
	                              OR CONVERT(VARCHAR, @DOB, 101) = '01/01/1900'
	                          )
	                  )
	               OR (
	                      @Mode = 4
	                      AND ISDATE(thlg.Load_Date) = 1
	                      AND DATEDIFF(DAY, thlg.Load_Date, @CrtDate) = 0
	                  )
	               OR (
	                      @Mode = 5
	                      AND ISDATE(NextEventDate) = 1
	                      AND DATEDIFF(DAY, NextEventDate, @CrtDate) = 0
	                  )
	           )
	           AND thlg.SourceFileName LIKE '%' + @searchcriterea + '%'
	    ORDER BY dccc.rowid
	END
	ELSE
	IF @LoaderId = 20--Harris County Criminal Court
	BEGIN
	    -- Noufil 5835 05/06/2009 Use top 1000 to remove out of memory error
	    SELECT TOP 1000
	           ID AS [Row Id],
	           CDI,
	           [Ticket Number],
	           [Violation Description],
	           [Court Number],
	           [Last Name],
	           [First Name],
	           [SPN Number],
	           CST,
	           DST,
	           [Bond Amount],
	           [Address Part I] AS [Address 1],
	           City,
	           [Zip Code] AS Zip,
	           Hccc.GroupID,
	           CONVERT(VARCHAR, [Insertion Date], 101) AS [Insert Date],
	           CONVERT(VARCHAR, thlg.Load_Date, 101) AS [File Date]
	    FROM   Harris_County_Criminal_Court_DataArchive Hccc
	           INNER JOIN LA_Houston.dbo.Tbl_HTS_LA_Group thlg
	                ON  thlg.GroupID = Hccc.GroupID
	    WHERE  (@Mode = 1 AND REPLACE(REPLACE([Ticket Number], ' ', ''), '-', '') LIKE @TicketNo)-- Adil 7363 02/02/2010 removing special characters from ticket/cause number check
	           OR  (
	                   (@Mode = 3)
	                   AND (
	                           (
	                               RTRIM(LTRIM(@LastName)) = '%'
	                               AND (RTRIM(LTRIM(hccc.[First Name])) LIKE @FirstName)
	                           )
	                           OR (
	                                  RTRIM(LTRIM(@FirstName)) = '%'
	                                  AND RTRIM(LTRIM(hccc.[Last Name])) LIKE @LastName
	                              )
	                           OR (
	                                  RTRIM(LTRIM(hccc.[First Name])) LIKE @FirstName
	                                  AND RTRIM(LTRIM(hccc.[Last Name])) LIKE @LastName
	                              )
	                       )
	               )
	           OR  (
	                   @Mode = 4
	                   AND ISDATE(thlg.Load_Date) = 1
	                   AND DATEDIFF(DAY, thlg.Load_Date, @CrtDate) = 0
	           )
	    ORDER BY Hccc.ID
	END
	ELSE
	IF @LoaderId = 21--Pasadena Arraignmnet
	BEGIN
	    -- Noufil 5835 05/06/2009 Use top 1000 to remove out of memory error
	    SELECT TOP 1000
	           ID AS [Row Id], 
	           CauseNumber, -- Adil 8336 09/29/2010 Cause number added.
	           [Last Name],
	           [First Name],
				MidNumber,
	           --CONVERT(VARCHAR, CAST([Court Date]AS DATETIME), 101) AS 
	           --[Court Date], Afaq 7380 3/26/2010 check for correct court date.
			   CASE WHEN ISDATE([Court Date]) = 1 THEN CONVERT(VARCHAR, CAST([Court Date] AS DATETIME), 101) ELSE [Court Date] END  AS [Court Date],
	           [Address Part I] AS [Address 1],
	           City,
	           [State],
	           [Zip Code] AS Zip,
	           [Violation Description],
	           [Next Court],
	           [Next am_pm],
	           dccc.GroupID,
	           CONVERT(VARCHAR, [Insertion Date], 101) AS [Insert Date],
	           CONVERT(VARCHAR, dccc.[Insertion Date], 101) AS [File Date]
	    FROM   PasadenaArraignmnet_DataArchive dccc
	           --INNER JOIN LA_Houston.dbo.Tbl_HTS_LA_Group thlg
	           --   ON  thlg.GroupID = dccc.GroupID
	    WHERE  (
	               @Mode = 3
	               AND [First Name] LIKE @FirstName
	               AND [Last Name] LIKE @LastName
	           )
	           OR  (
	                   @Mode = 4
	                   AND ISDATE(dccc.[Insertion Date]) = 1
	                   AND DATEDIFF(DAY, dccc.[Insertion Date], @CrtDate) = 0
	               )
	           OR  (
	                   @Mode = 5
	                   AND ISDATE(dccc.[Court Date]) = 1
	                   AND DATEDIFF(DAY, dccc.[Court Date], @CrtDate) = 0
	           )
	    ORDER BY dccc.ID
	END
	ELSE
	IF @LoaderId = 22--Pasadena Warrant
	BEGIN
	    -- Noufil 5835 05/06/2009 Use top 1000 to remove out of memory error
	    SELECT TOP 1000
	           RowID AS [Row Id],
	           [Last Name],
	           [First Name],
				MidNumber,
	           [Address Part I] AS [Address 1],
	           City,
	           [State],
	           [Zip Code] AS Zip,
	           [Phone Number],
	           [DL Number],
	           Race,
	           Gender,
             --Afaq 7380 3/26/2010 check for correct DOB.
	           Case When ISDATE(CONVERT(
	               VARCHAR,
	               CAST(
	                   (
	                       REPLACE(
	                           REPLACE(SUBSTRING(DOB, (0), (12)), ' ', ''),
	                           '?',
	                           ''
	                       )
	                   )AS DATETIME
	               ),
	               101
	           ))= 1 Then 
				CONVERT(
	               VARCHAR,
	               CAST(
	                   (
	                       REPLACE(
	                           REPLACE(SUBSTRING(DOB, (0), (12)), ' ', ''),
	                           '?',
	                           ''
	                       )
	                   )AS DATETIME
	               ),
	               101
	           ) Else 
				REPLACE(
	                           REPLACE(SUBSTRING(DOB, (0), (12)), ' ', ''),
	                           '?',
	                           '') END
				AS DOB,
				--Afaq 7380 3/26/2010 check for correct [Court Date].
	          Case When ISDATE(CONVERT(
	               VARCHAR,
	               CAST(
	                   (
	                       REPLACE(
	                           REPLACE(SUBSTRING([Court Date], (0), (12)), ' ', ''),
	                           '?',
	                           ''
	                       )
	                   )AS DATETIME
	               ),
	               101
	           ))= 1 Then 
				CONVERT(
	               VARCHAR,
	               CAST(
	                   (
	                       REPLACE(
	                           REPLACE(SUBSTRING([Court Date], (0), (12)), ' ', ''),
	                           '?',
	                           ''
	                       )
	                   )AS DATETIME
	               ),
	               101
	           ) Else 
				REPLACE(
	                           REPLACE(SUBSTRING([Court Date], (0), (12)), ' ', ''),
	                           '?',
	                           '') END
				AS [Court Date],
                --Afaq 7380 3/26/2010 check for correct [Violation Date].
				Case When ISDATE(CONVERT(
	               VARCHAR,
	               CAST(
	                   (
	                       REPLACE(
	                           REPLACE(SUBSTRING([Violation Date], (0), (12)), ' ', ''),
	                           '?',
	                           ''
	                       )
	                   )AS DATETIME
	               ),
	               101
	           ))= 1 Then 
				CONVERT(
	               VARCHAR,
	               CAST(
	                   (
	                       REPLACE(
	                           REPLACE(SUBSTRING([Violation Date], (0), (12)), ' ', ''),
	                           '?',
	                           ''
	                       )
	                   )AS DATETIME
	               ),
	               101
	           ) Else 
				REPLACE(
	                           REPLACE(SUBSTRING([Violation Date], (0), (12)), ' ', ''),
	                           '?',
	                           '') END
				AS [Violation Date],
               --Afaq 7380 3/26/2010 check for correct [BOnd Date].
	           Case When ISDATE(CONVERT(
	               VARCHAR,
	               CAST(
	                   (
	                       REPLACE(
	                           REPLACE(SUBSTRING([Bond Date], (0), (12)), ' ', ''),
	                           '?',
	                           ''
	                       )
	                   )AS DATETIME
	               ),
	               101
	           ))= 1 Then 
				CONVERT(
	               VARCHAR,
	               CAST(
	                   (
	                       REPLACE(
	                           REPLACE(SUBSTRING([Bond Date], (0), (12)), ' ', ''),
	                           '?',
	                           ''
	                       )
	                   )AS DATETIME
	               ),
	               101
	           ) Else 
				REPLACE(
	                           REPLACE(SUBSTRING([Bond Date], (0), (12)), ' ', ''),
	                           '?',
	                           '') END
				AS [Bond Date],
	           [Violation Description],
	           [Officer Name],
	           Docket,
	           Capias,
	           [Officer Badge],
	           dccc.[Group ID] AS GroupID,
	           CONVERT(VARCHAR, [Insertion Date], 101) AS [Insert Date]
	    FROM   PasadenaWarrant_DataArchive dccc
	    WHERE  (
	               @Mode = 3
	               AND dccc.[First Name] LIKE @FirstName
	               AND dccc.[Last Name] LIKE @LastName
	               AND (
	                       (ISNULL(@DOB, '') = '')
	                       OR (
	                              ISDATE(
	                                  CONVERT(
	                                      VARCHAR,
	                                      CAST(
	                                          (
	                                              REPLACE(REPLACE(SUBSTRING([DOB], (0), (12)), ' ', ''), '?', '')
	                                          )AS DATETIME
	                                      ),
	                                      101
	                                  )
	                              ) = 1
	                              AND DATEDIFF(
	                                      DAY,
	                                      CONVERT(
	                                          VARCHAR,
	                                          CAST(
	                                              (
	                                                  REPLACE(REPLACE(SUBSTRING([DOB], (0), (12)), ' ', ''), '?', '')
	                                              )AS DATETIME
	                                          ),
	                                          101
	                                      ),
	                                      @DOB
	                                  ) = 0
	                              OR CONVERT(
	                                     VARCHAR,
	                                     CAST(
	                                         (
	                                             REPLACE(REPLACE(SUBSTRING(dccc.DOB, (0), (12)), ' ', ''), '?', '')
	                                         )AS DATETIME
	                                     ),
	                                     101
	                                 ) = '01/01/1900'
	                          )
	                   )
	           )
	           OR  (
	                   @Mode = 4
	                   AND ISDATE(CAST([Insertion Date] AS DATETIME)) = 1
	                   AND DATEDIFF(DAY, CAST([Insertion Date] AS DATETIME), @CrtDate) 
	                       = 0
	               )
	           OR  (
	                   @Mode = 5
	                   AND ISDATE(
	                           CONVERT(
	                               VARCHAR,
	                               CAST(
	                                   (
	                                       REPLACE(
	                                           REPLACE(SUBSTRING([Court Date], (0), (12)), ' ', ''),
	                                           '?',
	                                           ''
	                                       )
	                                   )AS DATETIME
	                               ),
	                               101
	                           )
	                       ) = 1
	                   AND DATEDIFF(
	                           DAY,
	                           CONVERT(
	                               VARCHAR,
	                               CAST(
	                                   (
	                                       REPLACE(
	                                           REPLACE(SUBSTRING([Court Date], (0), (12)), ' ', ''),
	                                           '?',
	                                           ''
	                                       )
	                                   )AS DATETIME
	                               ),
	                               101
	                           ),
	                           @CrtDate
	                       ) = 0
	           )
	    ORDER BY dccc.RowID
	END
	
	--Sabir Khan 5963 06/02/2009 Get Tickler Extract file....
	IF @LoaderId = 23--Tickler Extract.
	BEGIN
	    -- Noufil 5835 05/06/2009 Use top 1000 to remove out of memory error
	    SELECT TOP 1000
	           RowId AS [Row Id],
	           CauseNumber AS [Cause Number],
	           TicketNumber AS [Ticket Number],
	           TicklerEvent AS [Tickler Event],
	           CONVERT(VARCHAR(10), EVENTDATE, 101) [Event Date],
	           CONVERT(VARCHAR(10), DATE1, 101) [Date 1],
	           UPDATEDATE [Update Date],
	           TDF.FileId AS [File ID]
	    FROM   tblticklerextract te
	           INNER JOIN tblDataFiles tdf
	                ON  tdf.FileId = te.FileId
	    WHERE  tdf.LoaderType = 10
	           AND (
	                   (@MODE = 2 AND REPLACE(REPLACE(TE.CauseNumber, ' ', ''), '-', '') = @CauseNo)-- Adil 7363 02/02/2010 removing special characters from ticket/cause number check
	                   OR (@MODE = 1 AND REPLACE(REPLACE(TE.TicketNumber, ' ', ''), '-', '') LIKE @TicketNo)
	                   OR (@Mode = 4 AND DATEDIFF(DAY, TDF.FileDate, @CrtDate) = 0)
	           )
	    ORDER BY te.RowId
	END
	ELSE --Fahad 6061 06/17/2009 Add Dallas Events Case
	IF @LoaderId = 24--Dallas Events
	BEGIN
	    -- Fahad 6061 06/17/2009 Use top 1000 to remove out of memory error
	    SELECT TOP 1000
	           ID,
	           [Ticket Number],
	           [Cause Number],
	           [Last Name],
	           [First Name],
	           [Middle Name],
	           [DOB Archive],
	           [Address Part I],
	           [Address Part II],
	           City,
	           [State],
	           [Zip Code],
	           [Phone Number],
	           DOCKSUFF,
	           ENTDATE,
	           [Violation Date],
	           [Violation Date Archive],
	           [Violation Description],
	           [Violation Code],
	           [Fine Amount],
	           [Court Status],
	           [Court Date Archive],
	           [Court Time Archive],
	           [Court Room No],
	           [Officer Code],
	           ABTYPE,
	           ABNUMB,
	           ABAMT,
	           ABDATE,
	           CBCODE,
	           CBNUMB,
	           CBAMT,
	           CBDATE,
	           CBRFLAG,
	           CBRAMT,
	           CBRDATE,
	           OCBRFLAG,
	           OCBRAMT,
	           OCBRDATE,
	           CBFCODE,
	           CBFNUMB,
	           CBFAMT,
	           CBFDATE,
	           [Officer Name],
	           Attorney,
	           [Address Status],
	           Reliable,
	           DP2,
	           DPC,
	           dccc.GroupID,
	           InsertDate,
	           Insert_Mode,
	           RecordID,
	           CONVERT(VARCHAR, thlg.Load_Date, 101) AS [File Date],
	           RowID AS [Row Id]
	    FROM   DallasFTP_DataArchive dccc
	           INNER JOIN LA_Houston.dbo.Tbl_HTS_LA_Group thlg
	                ON  thlg.GroupID = dccc.GroupID
	    WHERE  (
	               (
	                   @Mode = 1
	                   AND REPLACE(REPLACE(dccc.[Ticket Number], ' ', ''), '-', '') LIKE @TicketNo
	               )
	               OR (@Mode = 2 AND REPLACE(REPLACE(dccc.[Cause Number], ' ', ''), '-', '') LIKE @CauseNo)
	               OR (
	                      @Mode = 3
	                      AND LTRIM(RTRIM([First Name])) LIKE @FirstName
	                      AND LTRIM(RTRIM([Last Name])) LIKE @LastName
	                      AND LTRIM(RTRIM([Middle Name])) LIKE @MiddleName
	                      AND (
	                              (
	                                  ISDATE([DOB Archive]) = 1
	                                  AND DATEDIFF(DAY, [DOB Archive], @DOB) = 0
	                              )
	                              OR CONVERT(VARCHAR, @DOB, 101) = '01/01/1900'
	                          )
	                  )
	               OR (
	                      @Mode = 4
	                      AND ISDATE(thlg.Load_Date) = 1
	                      AND DATEDIFF(DAY, thlg.Load_Date, @CrtDate) = 0
	                  )
	               OR (
	                      @Mode = 5
	                      AND ISDATE([Court Date Archive]) = 1
	                      AND DATEDIFF(DAY, [Court Date Archive], @CrtDate) = 0
	                  )
	    )
	    ORDER BY dccc.ID
	END
	ELSE
	IF @LoaderId = 25--Arlington Arraignmnet
	BEGIN
	    -- Adil 6592 10/08/2009 Arlington Arraignment Raw Data Search
	    SELECT TOP 1000
	           RowID AS [Row Id],
	           aeda.[Ticket Number],
	           aeda.[Last Name],
	           aeda.[First Name],
	           aeda.[Address Part I] AS [Address 1],
	           --CONVERT(VARCHAR, CAST(aeda.[Violation Date] AS DATETIME), 101) AS 
	           --[Violation Date], Afaq 7694 4/23/2010  Convert Violation format to 'mm/dd/yyyy'
				CONVERT(VARCHAR(10),[Violation Date] ,101)  AS [Violation Date],
	           aeda.[Violation Description],
	           aeda.[Balance Due],
	           CONVERT(VARCHAR, aeda.[Filed Date], 101) as 'Case File Date', --Afaq 7694 04/23/2010 apply aliasing.
	           aeda.[Officer Name],
	           aeda.[Case Status],
	           aeda.GroupID,
	           CONVERT(VARCHAR, aeda.[InsertDate], 101) AS [Insert Date],
	           CONVERT(VARCHAR, aeda.[InsertDate], 101) AS [File Date] INTO #temp
	    FROM   ArlingtonEntered_DataArchive aeda
	    WHERE  (@Mode = 1 AND (REPLACE(REPLACE(aeda.[Ticket Number], ' ', ''), '-', '') LIKE @TicketNo))-- Adil 7363 02/02/2010 removing special characters from ticket/cause number check
	           OR  (
	                   @Mode = 3
	                   AND [First Name] LIKE @FirstName
	                   AND [Last Name] LIKE @LastName
	               )
	           OR  (
	                   @Mode = 4
	                   AND ISDATE(aeda.[InsertDate]) = 1
	                   AND DATEDIFF(DAY, aeda.[InsertDate], @CrtDate) = 0
	           )
	    ORDER BY aeda.RowID
	    IF NOT EXISTS(SELECT * FROM #temp) --Sabir 10289 05/31/2012 If not exist in Arlington Arraignment Pdf file then check in Arlington Arraignment Excel file
		BEGIN
			SELECT TOP 1000	           
	            aeda.[F Name] AS [First Name],
				aeda.[M Name] AS [Middle NAME],
				aeda.[L Name] AS [Last Name],
				aeda.[Sufx] AS [Sufx],
				aeda.[Blk #] AS [Block],
				aeda.[Street Name] AS [Street Name],
				aeda.[Apt #] AS [Apartment],
				aeda.[City] AS [City],
				aeda.[St] AS [STATE],
				aeda.[Zip] AS [Zip Code],
				aeda.[DOB] AS [DOB],
				aeda.[Race] AS [Race],
				aeda.[Sex] AS [Sex],
				aeda.[Age] AS [Age],
				aeda.[Citation] AS [Ticket Number],
				aeda.[Viol #] AS [Violation Number],
				aeda.[Viol Type] AS [Violation Type],
				aeda.[offense statute] AS [Offense Statute],
				aeda.[viol description] AS [Violation Description],
				aeda.[officer agency] AS [Officer Agency],
				aeda.[violation date] AS [Violation Date],
				aeda.[posted speed] AS [Posted Speed],
				aeda.[actual speed] AS [Actual Speed],
				aeda.[accident sw] AS [Accident SW],
				aeda.[date filed  ccyymmdd] AS [Filed Date],
				aeda.[plea] AS [Plea],
				aeda.[attorney] AS [Attorney],
				aeda.[current status description] AS [Current Status Description],
				aeda.[current status date ccyymmdd] AS [Current Status Date],
				aeda.[total fine/fee cost] AS [Fine Amount],
				aeda.[InsertDate] AS [Insert Date]
	    FROM   ArlingtonEntered_DataExcel aeda
	    WHERE  (@Mode = 1 AND (REPLACE(REPLACE(aeda.[Citation], ' ', ''), '-', '') LIKE @TicketNo))
	           OR  (
	                   @Mode = 3
	                   AND [F Name] LIKE @FirstName
	                   AND [L Name] LIKE @LastName
	               )
	           OR  (
	                   @Mode = 4
	                   AND ISDATE(aeda.[InsertDate]) = 1
	                   AND DATEDIFF(DAY, aeda.[InsertDate], @CrtDate) = 0
	           )
	    --ORDER BY aeda.RowID
	 		
		END
		ELSE
			BEGIN
				SELECT * FROM #temp
			END
	    
	END
	ELSE
	IF @LoaderId = 26--Arlington Warrant
	BEGIN
	    -- Adil 6592 10/08/2009 Arlington Warrant Raw Data Search
	    SELECT TOP 1000
	           awda.RowID AS [Row Id],
	           awda.[Ticket Number],
	           awda.[Last Name],
	           awda.[First Name],
	           awda.City,
	           awda.[State],
	           [Violation Date],
	           [Violation Description],
	           awda.[Amount Due],
	           awda.[GroupID] AS GroupID,
	           CONVERT(VARCHAR, [InsertDate], 101) AS [Insert Date]
	    FROM   ArlingtonWarrant_DataArchive awda
	    WHERE  (@Mode = 1 AND (REPLACE(REPLACE(awda.[Ticket Number], ' ', ''), '-', '') LIKE @TicketNo))
	           OR  (
	                   @Mode = 3
	                   AND [First Name] LIKE @FirstName
	                   AND [Last Name] LIKE @LastName
	               )
	           OR  (
	                   @Mode = 4
	                   AND ISDATE(awda.[InsertDate]) = 1
	                   AND DATEDIFF(DAY, awda.[InsertDate], @CrtDate) = 0
	           )
	    ORDER BY awda.RowID
	END
	ELSE
	IF @LoaderId = 27--DCCC CBF
	BEGIN
	    -- Adil 6848 10/27/2009 DCCC CBF raw data search added
	    SELECT TOP 1000 RowID AS [Row Id],
	           TicketNumber,
	           LastName,
	           FirstName,
	           ADDRESS,
	           City,
	           [State],
	           dcda.ViolationDescription
	           [Setting Status],
	           --CONVERT(VARCHAR, CAST(CourtDate AS DATETIME), 101) AS [CourtDate], --Afaq 7380 3/26/2010 correct date for court date.
               CASE WHEN ISDATE(CourtDate) = 1 THEN CONVERT(VARCHAR, CAST(CourtDate AS DATETIME), 101) ELSE CourtDate END  AS CourtDate,
	           [CourtTime],
	           Location,
	           Race,
	           Gender,
                --Afaq 7380 3/26/2010 correct DOB
	           CASE WHEN ISDATE(dob) = 1 THEN CONVERT(VARCHAR, CAST(DOB AS DATETIME), 101) ELSE dob END  AS DOB,
	           Comments,
	           dcda.GroupID,
	           CONVERT(VARCHAR, InsertDate, 101) AS [Insert Date]
	    FROM   DCCC_CBF_DataArchive dcda
	    WHERE  (@Mode = 1 AND REPLACE(REPLACE(TicketNumber, ' ', ''), '-', '') LIKE @TicketNo)-- Adil 7363 02/02/2010 removing special characters from ticket/cause number check
	           OR  (
	                   @Mode = 3
	                   AND FirstName LIKE @FirstName
	                   AND LastName LIKE @LastName
	                   AND (
	                           (ISDATE(DOB) = 1 AND DATEDIFF(DAY, DOB, @DOB) = 0)
	                           OR CONVERT(VARCHAR, @DOB, 101) = '01/01/1900'
	                       )
	               )
	           OR  (
	                   @Mode = 4
	                   AND ISDATE(dcda.InsertDate) = 1
	                   AND DATEDIFF(DAY, dcda.InsertDate, @CrtDate) = 0
	               )
	           OR  (
	                   @Mode = 5
	                   AND ISDATE([CourtDate]) = 1
	                   AND DATEDIFF(DAY, [CourtDate], @CrtDate) = 0
	           )
	    ORDER BY dcda.RowId
	END
	ELSE
	IF @LoaderId = 28--DCCC BN
	BEGIN
	    -- Adil 6850 11/04/2009 DCCC BN raw data search added
	    SELECT TOP 1000 RowID AS [Row Id],
	           [LAST NAME],
	           [FIRST NAME],
	           [Middle Name], -- Abbas Qamar 9266 07/26/2011 Adding Middle name
	           Race,
	           Gender,
	           --Afaq 7380 3/26/2010 correct DOB
	           CASE WHEN ISDATE(dob) = 1 THEN CONVERT(VARCHAR, CAST(DOB AS DATETIME), 101) ELSE dob END  AS DOB,
	           ADDRESS,
	           City,
	           [State],
	           dbda.[Zip Code],
	           dbda.[Violation Description],
	           dbda.[Bond Date],
	           dbda.GroupID,
	           CONVERT(VARCHAR, InsertDate, 101) AS [Insert Date]
	    FROM   DCCC_BN_DataArchive dbda
	    WHERE  (
	               @Mode = 3
	               AND [FIRST NAME] LIKE @FirstName
	               AND [LAST NAME] LIKE @LastName
	               AND (
	                       (ISDATE(DOB) = 1 AND DATEDIFF(DAY, DOB, @DOB) = 0)
	                       OR CONVERT(VARCHAR, @DOB, 101) = '01/01/1900'
	                   )
	           )
	           OR  (
	                   @Mode = 4
	                   AND ISDATE(dbda.InsertDate) = 1
	                   AND DATEDIFF(DAY, dbda.InsertDate, @CrtDate) = 0
	           )
	    ORDER BY dbda.RowId
	END--waqas 6791 11/18/2009 Montgomery
	ELSE
	IF @LoaderId = 29
	BEGIN
	    
	        SELECT TOP 1000 RowID AS [Row ID],
	               REPLACE(TICKET_NUMBER, '-', '') AS [TICKET NUMBER],
	               REPLACE(CAUSE_NUMBER,'-', '') AS [CAUSE NUMBER],
	               Last_Name AS [LAST NAME],
	               First_Name AS [FIRST NAME],
	               ADDRESS,
	               CITY,
	               STATE,
	               ZIP,
	              --Afaq 7380 3/26/2010 correct DOB
	               CASE WHEN ISDATE(dob) = 1 THEN CONVERT(VARCHAR, CAST(DOB AS DATETIME), 101) ELSE dob END  AS DOB,
	               BIRTH_PLACE,
	               RACE,
	               GENDER,
	               HEIGHT,
	               WEIGHT,
	               EYES,
	               HAIR,
	               SKIN,
	               SCARS_MARKS_TATOOS,
	               DL_NUMBER,
	               DLIC_STATE,
	               PIN_NO,
	               NCIC_NO,
	               SID_NO,
	               FBI_NO,
	               EMPLOYER,
	               EMPLOYER_PHONE,
	               VIOLATION_CODE,
	               VIOLATION_DESCRIPTION,
	               COURT,
	               ARREST_DATE,
	               OFFENSE_DATE,
	               FILE_DATE,
	               NBR_WITNESSES,
	               NBR_VICTIMS,
	               NBR_CODEFENDANTS,
	               VIDEO_NBR,
	               PAYMENTS,
	               ARREST_DATE2,
	               TRACKING_NUMBER,
	               BOOKING_NUMBER,
	               OFFENSE2,
	               DEGREE,
	               ASSIGNED_COURT,
	               JUDGE,
	               ARRESTING_OFFICER,
	               ARRESTING_OFFICER_CODE,
	               ARRESTING_AGENCY,
	               DISPOSITION_DATE,
	               ARREST_DDA_DESCRIPTION,
	               NOTES,
	               CHARGE_DATE,
	               DOCKET_SETS_RESETS,
	               DISMISSED_DATE,
	               JUDGEMENT_DATE,
	               JUDGEMENT,
	               CPN_DISPOSITION,
	               DEFENSE_ATTY,
	               DEFENSE_ATTY_CODE,
	               PROSECUTING_ATTY,
	               OFFENSE3,
	               OFFENSE_DEGREE,
	               PLEA_PLEA_DATE,
	               DISPOSED_OFFENSE,
	               OFFENSE_DEGREE2,
	               COMMENTS,
	               BOND_DATE,
	               BOND_NUMBER,
	               BOND_COMPANY,
	               BOND_AMOUNT,
	               BOND_FORFEIT_DATE,
	               BOND_RELEASE_DATE,
	               BOND_COMMENTS,
	               DAYS_CREDIT,
	               YRS_DAYS_PROBATION,
	               RESTITUTION_AMT,
	               LAWYER_FEES,
	               FINE,
	               COURT_COST,
	               DATE_DUE,
	               LAST_ALIAS,
	               LAST_TYPE,
	               LAST_DATE,
	               LAST_PAID,
	               PAID_AMOUNT,
	               BALANCE,
	               PAYMENT_TYPE,
	               HOURS,
	               COMMENTS2,
	               ENTRY_DATE,
	               SET_BY,
	               RESET_DATE,
	               COURT_STATUS,
	               REASON,
	               GroupID,
	               CONVERT(VARCHAR, [Insertion Date], 101) AS [Insert Date]
	        FROM   Montgomery_DataArchive dbda
	        WHERE  (
	                   (
	                       @Mode = 1
	                       AND REPLACE(REPLACE(dbda.Ticket_Number, ' ', ''), '-', '') LIKE @TicketNo-- Adil 7363 02/02/2010 removing special characters from ticket/cause number check
	                   )
	                   OR (@Mode = 2 AND REPLACE(REPLACE(dbda.[Cause_Number], ' ', ''), '-', '') LIKE @CauseNo)
	                   OR (
	                          @Mode = 3
	                          AND LTRIM(RTRIM([First_Name])) LIKE @FirstName
	                          AND LTRIM(RTRIM([Last_Name])) LIKE @LastName
	                          AND (
	                                  (
	                                      ISDATE([DOB]) = 1
	                                      AND DATEDIFF(DAY, [DOB], @DOB) 
	                                          = 0
	                                  )
	                                  OR CONVERT(VARCHAR, @DOB, 101) = 
	                                     '01/01/1900'
	                              )
	                      )
	                   OR (
	                          @Mode = 4
	                          AND ISDATE(dbda.[Insertion Date]) = 1
	                          AND DATEDIFF(DAY, dbda.[Insertion Date], @CrtDate) = 0
	                      )
	                   OR (
	                          @Mode = 5
	                          AND ISDATE(dbda.RESET_DATE) = 1
	                          AND DATEDIFF(DAY, dbda.RESET_DATE, @CrtDate) 
	                              = 0
	                      )
	        )
	        ORDER BY dbda.RowID
	        
	        END
			ELSE
				IF @LoaderId = 30--Fort Worth Warrant
				BEGIN
					-- Adil 7105 12/14/2009 Fort Worth Warrant raw data search added
					SELECT TOP 1000 fwdaw.ID AS [Row Id],
					fwdaw.TicketNumber,
					fwdaw.CauseNumber,
					fwdaw.First_Name,
					fwdaw.Last_Name,
					fwdaw.Middle_Name,
                    --Afaq 7380 3/26/2010 check for correct Warrant Issue Date.
	                CASE WHEN ISDATE(fwdaw.Bond_Date) = 1 THEN CONVERT(VARCHAR, CAST(fwdaw.Bond_Date AS DATETIME), 101) ELSE fwdaw.Bond_Date END  AS [Warrant Issue Date],
					fwdaw.AddressPartI,
					fwdaw.City_State_Zip,
					fwdaw.GroupID,
					fwdaw.Violation_Description AS [Violation Desc.], -- Afaq 7310 05/18/2010 Add Violation Description
						   CONVERT(VARCHAR, InsertDate, 101) AS [Insert Date]
					FROM   FortWorth_DataArchive_Warrant fwdaw
					WHERE  (
	                       @Mode = 1
	                       AND REPLACE(REPLACE(fwdaw.TicketNumber, ' ', ''), '-', '') LIKE @TicketNo-- Adil 7363 02/02/2010 removing special characters from ticket/cause number check
	                   )
	                   OR (@Mode = 2 AND REPLACE(REPLACE(fwdaw.[CauseNumber], ' ', ''), '-', '') LIKE @CauseNo)
	                   or(
							   @Mode = 3
							   AND First_Name LIKE @FirstName
							   AND last_name LIKE @LastName
							   AND (@MiddleName = '' OR @MiddleName <> '' AND fwdaw.Middle_Name LIKE @MiddleName)
						   )
						   OR  (
								   @Mode = 4
								   AND ISDATE(fwdaw.InsertDate) = 1
								   AND DATEDIFF(DAY, fwdaw.InsertDate, @CrtDate) = 0
						   )
						   OR (
	                          @Mode = 5
	                          AND ISDATE(Bond_Date) = 1
	                          AND DATEDIFF(DAY, Bond_Date, @CrtDate) = 0
						   )
					ORDER BY fwdaw.ID
				END
				ELSE
				IF @LoaderId = 31--Grand Prairie Arraignment
				BEGIN
					-- Adil 7205 01/05/2010 Grand Prairie Arraignment raw data search added
					SELECT TOP 1000 gpaa.RowId AS [Row Id],
					gpaa.TicketNumber,
					gpaa.FirstName,
					gpaa.LastName,
					gpaa.ViolationDescription,
                    -- Adil 7796 06/09/2010 displaying offense date into only date format.
					CONVERT(VARCHAR, CAST(gpaa.Violation_Date AS DATETIME), 101) AS [Offense Date],
					CONVERT(VARCHAR, CAST(gpaa.Court_Date AS DATETIME), 101) AS [Court Date], -- Adil 8484 11/11/2010 New file format changes.
					gpaa.[Address],
					gpaa.Fine_Amount,
					gpaa.[Status],-- Adil 7796 06/10/2010 including court status in raw search.
					gpaa.GroupID,
						   CONVERT(VARCHAR, gpaa.InsertDate, 101) AS [Insert Date]
					FROM   GrandPrairie_Arraignment_Archive gpaa
					WHERE  (
	                       @Mode = 1
	                       AND REPLACE(REPLACE(gpaa.TicketNumber, ' ', ''), '-', '') LIKE @TicketNo
	                   )
	                   or(
							   @Mode = 3
							   AND FirstName LIKE @FirstName
							   AND lastname LIKE @LastName
						   )
						   OR  (
								   @Mode = 4
								   AND ISDATE(gpaa.InsertDate) = 1
								   AND DATEDIFF(DAY, gpaa.InsertDate, @CrtDate) = 0
						   )
						   OR (
	                          @Mode = 5
	                          AND ISDATE(Court_Date) = 1
	                          AND DATEDIFF(DAY, Court_Date, @CrtDate) = 0
						   )
					ORDER BY gpaa.RowId
				END
				ELSE IF @LoaderId = 32 --Grand Prairie Warrant
				BEGIN
					-- Adil 7235 01/19/2010 Grand Prairie warrant raw data search added
					SELECT TOP 1000 gpwa.RowId AS [Row Id],
					gpwa.TicketNumber,
					gpwa.FirstName,
					gpwa.LastName,
					gpwa.ViolationDescription,
                    -- Adil 7796 06/09/2010 displaying offense date into only date format.
					CONVERT(VARCHAR, CAST(gpwa.Violation_Date AS DATETIME), 101) AS [Offense Date],
					CONVERT(VARCHAR, CAST(gpwa.Court_Date AS DATETIME), 101) AS [Court Date], -- Adil 8484 11/11/2010 New file format changes.
					gpwa.[Address],
					gpwa.Fine_Amount,
					gpwa.[Status],-- Adil 7796 06/10/2010 including court status in raw search.
					gpwa.GroupID,
					CONVERT(VARCHAR, gpwa.InsertDate, 101) AS [Insert Date]
					FROM   GrandPrairie_Warrant_Archive gpwa
					WHERE  (
	                       @Mode = 1
	                       AND REPLACE(REPLACE(gpwa.TicketNumber, ' ', ''), '-', '') LIKE @TicketNo-- Adil 7363 02/02/2010 removing special characters from ticket/cause number check
	                   )
	                   or(
							   @Mode = 3
							   AND FirstName LIKE @FirstName
							   AND lastname LIKE @LastName
						   )
						   OR  (
								   @Mode = 4
								   AND ISDATE(gpwa.InsertDate) = 1
								   AND DATEDIFF(DAY, gpwa.InsertDate, @CrtDate) = 0
						   )
						   OR (
	                          @Mode = 5
	                          AND ISDATE(gpwa.Court_Date) = 1
	                          AND DATEDIFF(DAY, gpwa.Court_Date, @CrtDate) = 0
						   )
					ORDER BY gpwa.RowId
				END   
				ELSE IF @LoaderId = 33--Plano Arraignment
				BEGIN
					-- Adil 7353 01/29/2010 Plano Arraignment raw data search added
					SELECT TOP 1000 paa.RowId AS [Row Id],
					paa.CauseNumber,
					paa.FirstName,
					paa.LastName,
					CONVERT(VARCHAR, CAST(paa.Violation_Date AS DATETIME), 101) AS [Offense Date],
					--Afaq 7380 3/26/2010 check for correct Offense Date.
	                TrafficTickets.dbo.fn_DateFormat(CASE WHEN ISDATE (paa.Violation_Date) = 1 THEN paa.Violation_Date ELSE '1/1/1900' END, 4, '/', '', 0) AS [Offense Date],
	                --CASE WHEN ISDATE(paa.Violation_Date) = 1 THEN CONVERT(VARCHAR, CAST(paa.Violation_Date AS DATETIME), 101) ELSE paa.Violation_Date END  AS [Offense Date],

					paa.[Address],
					paa.City,
					paa.[State],
					paa.[Zip Code],
					paa.ViolationDescription,
					paa.GroupID,
						   CONVERT(VARCHAR, paa.InsertDate, 101) AS [Insert Date]
					FROM   Plano_Arraignment_Archive paa
					WHERE  (
	                       @Mode = 1
	                       AND REPLACE(REPLACE(paa.CauseNumber, ' ', ''), '-', '') LIKE @TicketNo-- Adil 7363 02/02/2010 removing special characters from ticket/cause number check
	                   )
	                   or(
							   @Mode = 3
							   AND FirstName LIKE @FirstName
							   AND lastname LIKE @LastName
						   )
						   OR  (
								   @Mode = 4
								   AND ISDATE(paa.InsertDate) = 1
								   AND DATEDIFF(DAY, paa.InsertDate, @CrtDate) = 0
						   )
						   OR (
	                          @Mode = 5
	                          AND ISDATE(Violation_Date) = 1
	                          AND DATEDIFF(DAY, Violation_Date, @CrtDate) = 0
						   )
					ORDER BY paa.RowId
				END
				ELSE IF @LoaderId = 34--Plano Warrant
				BEGIN
					-- Adil 7362 02/09/2010 Plano Warrant raw data search added
					SELECT TOP 1000 pwa.RowId AS [Row Id],
					pwa.FirstName,
					pwa.LastName,
					pwa.TicketNumber, -- Adil 8432 10/27/2010 Ticket Number added.
					pwa.CauseNumber,
					TrafficTickets.dbo.fn_DateFormat(pwa.Warrant_Date, 4, '/', ':', 1) AS [Warrant Date], --8432 Adil 10/19/2010 Warrant Date format fixed.
					pwa.[Address],
					pwa.City,
					pwa.[State],
					pwa.[Zip Code],
					pwa.ViolationDescription,
					pwa.GroupID,
						   CONVERT(VARCHAR, pwa.InsertDate, 101) AS [Insert Date]
					FROM   Plano_Warrant_Archive pwa
					WHERE  (
							   @Mode = 3
							   AND FirstName LIKE @FirstName
							   AND lastname LIKE @LastName
						   )
						   OR  (
								   @Mode = 4
								   AND ISDATE(pwa.InsertDate) = 1
								   AND DATEDIFF(DAY, pwa.InsertDate, @CrtDate) = 0
						   )
						   OR (
	                          @Mode = 5
	                          AND ISDATE(Warrant_Date) = 1
	                          AND DATEDIFF(DAY, Warrant_Date, @CrtDate) = 0
						   )
					ORDER BY pwa.RowId
				END
				ELSE IF @LoaderId = 35 --Stafford Arraignment
				BEGIN
					-- Adil 7364 02/17/2010 Stafford Arraignment raw data search added
					SELECT TOP 1000 saa.RowId AS [Row Id],
					saa.FirstName,
					saa.LastName,
					saa.MiddleName,
					saa.TicketNumber,
					saa.CauseNumber, -- Adil 8052 08/01/2010 Cause Number included.
					--CONVERT(VARCHAR, CAST(saa.CourtDate AS DATETIME), 101) AS [Court Date],
					--7380 Afaq 3/23/2010 Check for correct Court Date
					CASE WHEN ISDATE(saa.CourtDate) = 1 THEN CONVERT(VARCHAR, CAST(saa.CourtDate AS DATETIME), 101) ELSE saa.CourtDate END  AS [Court Date],
					TrafficTickets.dbo.fn_DateFormat(saa.CourtTime,26, '/', ':', 1) AS CourtTime,
					saa.Race,
					saa.Sex,
					saa.[Address],
					saa.City,
					saa.[State],
					saa.[Zip Code],
					saa.ViolationDescription,
					saa.[Status],
					saa.Officer,
					saa.GroupID,
						   CONVERT(VARCHAR, saa.InsertDate, 101) AS [Insert Date]
					FROM Stafford_Arraignment_Archive saa
					WHERE  (
	                       @Mode = 1
	                       AND REPLACE(REPLACE(saa.TicketNumber, ' ', ''), '-', '') LIKE @TicketNo
					)
					OR
							(
							   @Mode = 3
							   AND FirstName LIKE @FirstName
							   AND lastname LIKE @LastName
							   AND saa.MiddleName LIKE @MiddleName
						   )
						   OR  (
								   @Mode = 4
								   AND ISDATE(saa.InsertDate) = 1
								   AND DATEDIFF(DAY, saa.InsertDate, @CrtDate) = 0
						   )
						   OR (
	                          @Mode = 5
	                          AND ISDATE(saa.CourtDate) = 1
	                          AND DATEDIFF(DAY, saa.CourtDate, @CrtDate) = 0
						   )
					ORDER BY saa.RowId
				END
				ELSE IF @LoaderId = 36 --Stafford Warrant
				BEGIN
					-- Adil 7420 02/22/2010 Stafford Warrant raw data search added
					SELECT TOP 1000 swa.RowId AS [Row Id],
					swa.[First Name],
					swa.[Last Name],
					swa.[Middle Name],
					swa.suffix,
					swa.[Ticket Number],
					swa.[Cause Number],
					--CONVERT(VARCHAR, CAST(swa.[Court Date] AS DATETIME), 101) AS [Court Date],
					--7380 Afaq 3/23/2010 Check for correct Court Date
					CASE WHEN ISDATE(swa.[Court Date]) = 1 THEN CONVERT(VARCHAR, CAST(swa.[Court Date] AS DATETIME), 101) ELSE swa.[Court Date] END  AS [Court Date],
					TrafficTickets.dbo.fn_DateFormat(swa.[Court Time],26, '/', ':', 1) AS [Court Time],
					swa.Race,
					swa.Sex,
					swa.[Address Part I],
					swa.[Address Part II],
					swa.City,
					swa.[State],
					swa.[Zip Code],
					swa.[DL State],
					swa.[Status], --Afaq 7420 03/04/2010 Status Column Added
					swa.[Violation Description],
					--CONVERT(VARCHAR, CAST(swa.[Violation Date] AS DATETIME), 101) AS [Violation Date],--Afaq 7420 03/04/2010 Cast to DateTime
					--7380 Afaq 3/23/2010 Check for correct Violation Date
					CASE WHEN ISDATE(swa.[Violation Date]) = 1 THEN CONVERT(VARCHAR, CAST(swa.[Violation Date] AS DATETIME), 101) ELSE swa.[Violation Date] END  AS [Violation Date],
					TrafficTickets.dbo.fn_DateFormat(swa.[Violation Time],26, '/', ':', 1) AS [Violation Time],
					--CONVERT(VARCHAR, CAST(swa.[Warrant Date] AS DATETIME), 101) AS [Warrant Date],--Afaq 7420 03/04/2010 Cast to DateTime
					--7380 Afaq 3/23/2010 Check for correct Warrant Date
					CASE WHEN ISDATE(swa.[Warrant Date]) = 1 THEN CONVERT(VARCHAR, CAST(swa.[Warrant Date] AS DATETIME), 101) ELSE swa.[Warrant Date] END  AS [Warrant Date],
					swa.[type],
					swa.[Fine Amount],
					swa.[Bond Amount],
					swa.wcollection,
					swa.newtotal,
					swa.fine_orgbal,
					swa.fine_newbal,
					swa.tax_orgbal,
					swa.tax_newbal,
					swa.lea_orgbal,
					swa.lea_newbal,
					swa.multi_orgbal,
					swa.multi_newbal,
					swa.war_orgbal,
					swa.war_newbal,
					swa.cwar_orgbal,
					swa.cwar_newbal,
					swa.pay_orgbal,
					swa.pay_newbal,
					swa.dl_orgbal,
					swa.dl_newbal,
					swa.col_orgbal,
					swa.col_newbal,
					swa.GroupID,
						   CONVERT(VARCHAR, swa.InsertDate, 101) AS [Insert Date]
					FROM Stafford_Warrant_Archive swa
					WHERE  (
	                       @Mode = 1
	                       AND REPLACE(REPLACE(swa.[Ticket Number], ' ', ''), '-', '') LIKE @TicketNo
					)
					OR
					(
	                       @Mode = 2
	                       AND REPLACE(REPLACE(swa.[Cause Number], ' ', ''), '-', '') LIKE @CauseNo
					)
					OR
							(
							   @Mode = 3
							   AND swa.[First Name] LIKE @FirstName
							   AND swa.[Last Name] LIKE @LastName
							   AND swa.[Middle Name] LIKE @MiddleName
						   )
						   OR  (
								   @Mode = 4
								   AND ISDATE(swa.InsertDate) = 1
								   AND DATEDIFF(DAY, swa.InsertDate, @CrtDate) = 0
						   )
						   OR (
	                          @Mode = 5
	                          AND ISDATE(swa.[Court Date]) = 1
	                          AND DATEDIFF(DAY, swa.[Court Date], @CrtDate) = 0
						   )
					ORDER BY swa.RowId
				END

				ELSE IF @LoaderId IN (37, 38, 39) -- Sugar Land DL, Dispose & Events Loader
				BEGIN
					-- Adil 7988, 7989, 7990 07/22/2010 Sugar Land DL, Dispose & Events raw data search added
					SELECT TOP 1000 seda.RowId AS [Row Id],
					seda.Ticket_Number,
					seda.First_Name, 
					seda.Last_Name, 
					seda.[Status], 
					TrafficTickets.dbo.fn_DateFormat(seda.Court_Date, 2, '/', ':', 1) AS [Court Date], -- Adil 8303 09/23/2010 Court date format changed and court time hided.
					-- seda.Court_Time,
					TrafficTickets.dbo.fn_DateFormat(seda.Conviction_Date, 4, '/', ':', 1) AS [Conviction Date],
					seda.Violation_Description, 
					seda.Fine_Amount, 
					seda.Cost_Due, 
					seda.Bond_Amount, 
					seda.Warrant, 
					seda.Bond, 
					seda.[Type], 
					seda.Race, 
					seda.Sex, 
					TrafficTickets.dbo.fn_DateFormat(seda.DOB, 4, '/', ':', 1) AS [DOB],
					seda.DL_Number, 
					seda.Officer, 
					seda.Agency_Name, 
					seda.Alt_1_Status, 
					TrafficTickets.dbo.fn_DateFormat(seda.Alt_1_Status_Date, 4, '/', ':', 1) AS [Alt_1_Status_Date],
					seda.Alt_1_Status_Time, 
					seda.Alt_2_Status, 
					TrafficTickets.dbo.fn_DateFormat(seda.Alt_2_Status_Date, 4, '/', ':', 1) AS [Alt_2_Status_Date],
					seda.Alt_2_Status_Time, 
					seda.Cause_Number, 
					seda.NDO, 
					seda.Home_Phone, 
					seda.Work_Phone, 
					seda.Cell_Phone,
					seda.Statute, seda.Signed, seda.Bond_Posted, seda.Judge, seda.Review_Attorney, seda.EMail,
					TrafficTickets.dbo.fn_DateFormat(seda.Insert_Date, 4, '/', ':', 1) AS [Insert Date],
					seda.GroupID
					FROM Sugarland_Events_Data_Archive seda
					WHERE  ((
	                       @Mode = 1
	                       AND REPLACE(REPLACE(seda.Ticket_Number, ' ', ''), '-', '') LIKE @TicketNo
					)
					OR  (@Mode = 2)
						   AND (REPLACE(REPLACE(seda.Cause_Number, ' ', ''), '-', '') LIKE @CauseNo)
					OR
							(
							   @Mode = 3
							   AND seda.First_Name LIKE @FirstName
							   AND seda.Last_Name LIKE @LastName
						   )
						   OR  (
								   @Mode = 4
								   AND ISDATE(seda.Insert_Date) = 1
								   AND DATEDIFF(DAY, seda.Insert_Date, @CrtDate) = 0
						   )
						   OR (
	                          @Mode = 5
	                          AND ISDATE(seda.Court_Date) = 1
	                          AND DATEDIFF(DAY, seda.Court_Date, @CrtDate) = 0
	                      )) AND seda.LoaderID = CASE WHEN @LoaderId = 37 THEN 45 WHEN @LoaderId = 38 THEN 46 WHEN @LoaderId = 39 THEN 47 END 
					ORDER BY seda.RowID
				END
				ELSE IF @LoaderId = 40 --Fot Worth DLQ
				BEGIN
					-- Adil 8117 08/24/2010 Fort Worth DLQ raw data search added
					SELECT TOP 1000 fwdad.ID AS [Row Id],
					fwdad.Last_Name,
					fwdad.First_Name,
					fwdad.Middle_Name,
					fwdad.TicketNumber,
					TrafficTickets.dbo.fn_DateFormat(fwdad.List_Date,1,'/',':',1) AS ListDate,
					TrafficTickets.dbo.fn_DateFormat(fwdad.[Violation Date],1,'/',':',1) AS [Violation Date],
					fwdad.ViolationCode,
					fwdad.[Violation Description],
					fwdad.[Status],
					fwdad.[Crt Setting],
					fwdad.Attorney,
					fwdad.Disposition,
					fwdad.[Cause Number],
					fwdad.[Address Part I],
					fwdad.[City State Zip],
					fwdad.GroupID,
					CONVERT(VARCHAR, fwdad.InsertDate, 101) AS [Insert Date]
					FROM FortWorth_DataArchive_DLQ fwdad
					WHERE  (
	                       @Mode = 1
	                       AND REPLACE(REPLACE(fwdad.TicketNumber, ' ', ''), '-', '') LIKE @TicketNo
					)
					OR
					(
	                       @Mode = 2
	                       AND REPLACE(REPLACE(fwdad.[Cause Number], ' ', ''), '-', '') LIKE @CauseNo
					)
					OR
							(
							   @Mode = 3
							   AND fwdad.First_Name LIKE @FirstName
							   AND fwdad.Last_Name LIKE @LastName
							   AND fwdad.Middle_Name LIKE @MiddleName
						   )
						   OR  (
								   @Mode = 4
								   AND ISDATE(fwdad.InsertDate) = 1
								   AND DATEDIFF(DAY, fwdad.InsertDate, @CrtDate) = 0
						   )
					ORDER BY fwdad.ID
						   
				END
				ELSE IF @LoaderId = 41 -- Stafford Events Loader
				BEGIN
					-- Adil 8349 10/18/2010 Stafford Events raw data search added
					SELECT TOP 1000 sea.RowID,
					sea.TicketNumber,
					sea.CauseNumber,
					sea.LastName,
					sea.FirstName,
					sea.MiddleName,
					sea.[Address],
					sea.Address2,
					sea.City,
					sea.[State],
					sea.[Zip Code],
					sea.PhoneNumber,
					TrafficTickets.dbo.fn_DateFormat(sea.DOB, 4, '/', ':', 1) AS [DOB],
					sea.[Status],
					TrafficTickets.dbo.fn_DateFormat(sea.ViolationDate, 4, '/', ':', 1) AS [Violation Date],
					TrafficTickets.dbo.fn_DateFormat(sea.ViolationTime, 26, '/', ':', 1) AS [Violation Time],
					sea.ViolationCode,
					sea.DispositionCode,
					TrafficTickets.dbo.fn_DateFormat(sea.JudgementDate, 4, '/', ':', 1) AS [Judgement Date],
					sea.OfficerName,
					sea.ViolationDescription,
					TrafficTickets.dbo.fn_DateFormat(sea.CourtDate, 4, '/', ':', 1) AS [Court Date],
					sea.CourtTime,
					TrafficTickets.dbo.fn_DateFormat(sea.InsertDate, 4, '/', ':', 1) AS [Insert Date],
					sea.GroupID
					FROM Stafford_Event_Archive sea
					WHERE  ((
	                       @Mode = 1
	                       AND REPLACE(REPLACE(sea.TicketNumber, ' ', ''), '-', '') LIKE @TicketNo
					)
					OR  (@Mode = 2)
						   AND (REPLACE(REPLACE(sea.CauseNumber, ' ', ''), '-', '') LIKE @CauseNo)
					OR
							(
							   @Mode = 3
							   AND sea.FirstName LIKE @FirstName
							   AND sea.LastName LIKE @LastName
							   AND (DATEDIFF(DAY, ISNULL(@DOB, '1/1/1900'), '1/1/1900') = 0 OR DATEDIFF(DAY, ISNULL(@DOB, '1/1/1900'), sea.DOB) = 0)
						   )
						   OR  (
								   @Mode = 4
								   AND ISDATE(sea.InsertDate) = 1
								   AND DATEDIFF(DAY, sea.InsertDate, @CrtDate) = 0
						   )
						   OR (
	                          @Mode = 5
	                          AND ISDATE(sea.CourtDate) = 1
	                          AND DATEDIFF(DAY, sea.CourtDate, @CrtDate) = 0
	                      ))
					ORDER BY sea.RowID					
				END
				ELSE IF @LoaderId = 42 -- PMC Events raw data search added
				BEGIN
					-- Kashif Jawed 8981 03/18/2011 PMC Events raw data search added
					SELECT TOP 1000 pada.RowID,
					pada.[Last Name] LastName,
					pada.[First Name] FirstName,
					pada.Ticket TicketNumber, 
					pada.[Cause Number] CauseNumber,
					pada.[Phone Number] PhoneNumber,
					pada.DOB,
					pada.[Violation Description] ViolationDescription,
					pada.[Officer Name] OfficerName ,
					pada.[Court Date] CourtDate,
					pada.[Court Time] CourtTime,
					pada.[Court AMPM] CourtAP,
					pada.[Address Part I] Address,
					pada.City,
					pada.STATE,
					pada.[Zip Code] ZipCode,
					pada.Hearing,
					pada.[Hearing Code] HearingCode,
					pada.[Initial Disposition] InitDisposition,
					pada.[Initial Disposition Code] InitDispCode,
					pada.[Final Disposition] FinalDisposition,
					pada.[Final Disposition Code] FinalDispCode,
					pada.[Warrant Date] WarranrDate,
					pada.Capias,
					pada.[Balance Due] BalanceDue,
					pada.Bond,
					pada.[Non Disclosure] NonDisclosure,
					pada.[Group ID] GroupID,
					pada.[Insertion Date] InsertionDate
					FROM PasadenaEvents_DataArchive pada 
					WHERE  ((
	                       @Mode = 1
	                       AND REPLACE(REPLACE(pada.Ticket, ' ', ''), '-', '') LIKE @TicketNo
					)
					OR  (@Mode = 2)
						   AND (REPLACE(REPLACE(pada.[Cause Number], ' ', ''), '-', '') LIKE @CauseNo)
					OR
							(
							   @Mode = 3
							   AND pada.[First Name] LIKE @FirstName
							   AND pada.[Last Name] LIKE @LastName
							   AND (DATEDIFF(DAY, ISNULL(@DOB, '1/1/1900'), '1/1/1900') = 0 OR DATEDIFF(DAY, ISNULL(@DOB, '1/1/1900'), pada.DOB) = 0)
						   )
						   OR  (
								   @Mode = 4
								   AND ISDATE(pada.[Insertion Date]) = 1
								   AND DATEDIFF(DAY, pada.[Insertion Date], @CrtDate) = 0
						   )
						   OR (
	                          @Mode = 5
	                          AND ISDATE(pada.[Court Date]) = 1
	                          AND DATEDIFF(DAY, pada.[Court Date], @CrtDate) = 0
	                      ))
					ORDER BY pada.RowID						
				END
				
				--Muhammad Nadir Siddiqui 9303 05/24/2011 Added SOHO- Events raw data Loader 
				
				ELSE IF @LoaderId IN (43,44,45) -- SOHO - Entered, Disposed, and Events Loader 
				BEGIN
					
					SELECT TOP 1000 seda.RowId AS [Row Id],
					seda.Ticket_Number,
					seda.First_Name,					  
					seda.Last_Name, 
					seda.Middle_Name,
					seda.[Status], 
					TrafficTickets.dbo.fn_DateFormat(seda.Court_Date, 2, '/', ':', 1) AS [Court Date], -- Adil 8303 09/23/2010 Court date format changed and court time hided.
					-- seda.Court_Time,
					TrafficTickets.dbo.fn_DateFormat(seda.Conviction_Date, 4, '/', ':', 1) AS [Conviction Date],
					seda.Violation_Description, 
					seda.Fine_Amount, 
					seda.Cost_Due, 
					seda.Bond_Amount, 
					seda.Warrant, 
					seda.Bond, 
					seda.[Type], 
					seda.Race, 
					seda.Sex, 
					TrafficTickets.dbo.fn_DateFormat(seda.DOB, 4, '/', ':', 1) AS [DOB],
					seda.DL_Number, 
					seda.SSN_Number, --Farrukh 9303 06/10/2011 added Social Security Number
					seda.Officer, 
					seda.Agency_Name, 
					seda.Alt_1_Status, 
					TrafficTickets.dbo.fn_DateFormat(seda.Alt_1_Status_Date, 4, '/', ':', 1) AS [Alt_1_Status_Date],
					seda.Alt_1_Status_Date, 
					seda.Alt_2_Status, 
					TrafficTickets.dbo.fn_DateFormat(seda.Alt_2_Status_Date, 4, '/', ':', 1) AS [Alt_2_Status_Date],
					seda.Alt_2_Status_Date, 
					seda.Cause_Number, 
					seda.NDO, 
					seda.Home_Phone, 
					seda.Work_Phone, 
					seda.Cell_Phone,
					seda.Statute, seda.Signed, seda.Bond_Posted, seda.Judge, seda.Review_Attorney, seda.EMail,
					TrafficTickets.dbo.fn_DateFormat(seda.Insert_Date, 4, '/', ':', 1) AS [Insert Date],
					seda.GroupID
					FROM SoHo_Events_Data_Archive  seda
					WHERE  ((
	                       @Mode = 1
	                       AND REPLACE(REPLACE(seda.Ticket_Number, ' ', ''), '-', '') LIKE @TicketNo
					)
					OR  (@Mode = 2)
						   AND (REPLACE(REPLACE(seda.Cause_Number, ' ', ''), '-', '') LIKE @CauseNo)
					OR
							(
							   @Mode = 3
							   AND (seda.First_Name LIKE @FirstName
							   AND seda.Last_Name LIKE @LastName)
						   )
						   OR  (
								   @Mode = 4
								   AND ISDATE(seda.Insert_Date) = 1
								   AND DATEDIFF(DAY, seda.Insert_Date, @CrtDate) = 0
						   )
						   OR (
	                          @Mode = 5
	                          AND ISDATE(seda.Court_Date) = 1
	                          AND DATEDIFF(DAY, seda.Court_Date, @CrtDate) = 0
	                      )) AND seda.LoaderID =  CASE WHEN @LoaderId = 43 THEN 52 WHEN @LoaderId = 44 THEN 53 WHEN @LoaderId = 45 THEN 54 END
	                     
					ORDER BY seda.RowID
				END
				ELSE IF @LoaderId = 46 -- SoHo Arraignment
					BEGIN
						SELECT DISTINCT TOP 1000
							   SoHoArr.ID AS ID,
							   CONVERT(VARCHAR, LAGroup.Load_Date, 101) AS [File Date],
							   SoHoArr.LastName AS [Last Name],
							   SoHoArr.FirstName AS [First Name],
							   SoHoArr.MiddleName AS [Middle Name],
							   SoHoArr.TicketNumber AS [Ticket No],
							   SoHoArr.AddressPartI AS [Address 1],
							   SoHoArr.AddressPartII AS [Adress 2],
							   SoHoArr.City,
							   SoHoArr.[State],
							   SoHoArr.ZipCode,
							   CONVERT(VARCHAR, SoHoArr.Court_Date, 101) AS [Crt.Date],
							   SoHoArr.Violation_Description AS [Description],
							   SoHoArr.DP2,
							   SoHoArr.DPC,
							   SoHoArr.Address_Status AS [Add.Status],
							   SoHoArr.Reliable,
							   CONVERT(VARCHAR, SoHoArr.InsertDate, 101) AS [Insert Date],
							   SoHoArr.GroupID
						FROM   LoaderFilesArchive.dbo.SoHo_DataArchive SoHoArr
							   INNER JOIN LA_Houston.dbo.Tbl_HTS_LA_Group AS LAGroup
									ON  SoHoArr.GroupID = LAGroup.GroupID
						WHERE  (@Mode = 1)
							   AND (REPLACE(REPLACE(SoHoArr.TicketNumber, ' ', ''), '-', '') LIKE @TicketNo)
							   OR  (@Mode = 2)
							   AND (REPLACE(REPLACE(SoHoArr.TicketNumber, ' ', ''), '-', '') LIKE @CauseNo)
							   OR  (
									   (@Mode = 3)
									   AND (
											   (
												   RTRIM(LTRIM(@LastName)) = '%'
												   AND (RTRIM(LTRIM(SoHoArr.FirstName)) LIKE @FirstName)
											   )
											   OR (
													  RTRIM(LTRIM(@FirstName)) = '%'
													  AND RTRIM(LTRIM(SoHoArr.LastName)) LIKE @LastName
												  )
											   OR (
													  RTRIM(LTRIM(SoHoArr.FirstName)) LIKE @FirstName
													  AND RTRIM(LTRIM(SoHoArr.LastName)) LIKE @LastName
												  )
										   )
								   )
							   OR  (@Mode = 4)
							   AND (ISDATE(LAGroup.Load_Date) = 1)
							   AND (
									   DATEDIFF(DAY, RTRIM(LTRIM(LAGroup.Load_Date)), @CrtDate) 
									   = 0
								   )
							   OR  (@Mode = 5)
							   AND (ISDATE(SoHoArr.Court_Date) = 1)
							   AND (DATEDIFF(DAY, SoHoArr.Court_Date, @CrtDate) = 0)
						ORDER BY SoHoArr.GroupID, SoHoArr.ID
					END
					--Farrukh 9618 08/22/2011 Added SMC Arraignment raw data Loader
					ELSE IF @LoaderId = 47 
					BEGIN
						SELECT DISTINCT TOP 1000
							   SMCArr.ID AS ID,
							   traffictickets.dbo.fn_DateFormat(LAGroup.Load_Date, 4, '/', ':', 1) AS [File Date],
							   --CONVERT(VARCHAR, LAGroup.Load_Date, 101) AS [File Date],
							   SMCArr.LastName AS [Last Name],
							   SMCArr.FirstName AS [First Name],
							   SMCArr.MiddleName AS [Middle Name],
							   SMCArr.TicketNumber AS [Ticket No],
							   SMCArr.TicketNumber AS [Cause No],
							   SMCArr.AddressPartI AS [Address 1],
							   SMCArr.AddressPartII AS [Adress 2],
							   SMCArr.City,
							   SMCArr.[State],
							   SMCArr.ZipCode,
							   traffictickets.dbo.fn_DateFormat(SMCArr.Court_Date, 4, '/', ':', 1) AS [Crt.Date],
							   --CONVERT(VARCHAR, SMCArr.Court_Date, 101) AS [Crt.Date],
							   SMCArr.Violation_Description AS [Description],
							   traffictickets.dbo.fn_DateFormat(SMCArr.ViolationDate, 4, '/', ':', 1) AS [Vio.Date],
							   --CONVERT(VARCHAR, SMCArr.ViolationDate, 101) AS [Vio.Date],
							   SMCArr.ViolationLocation AS [vio.Loc],
							   SMCArr.OfficerName AS [Officer Name],
							   SMCArr.VehInfo AS [Veh.Info],
							   traffictickets.dbo.fn_DateFormat(SMCArr.DOB, 4, '/', ':', 1) AS [DOB],
							   --CONVERT(VARCHAR, SMCArr.DOB, 101) AS [DOB],
							   SMCArr.Sex,
							   SMCArr.Race,
							   SMCArr.Radar_Laser AS [Radar Laser],
							   SMCArr.Arrest,
							   SMCArr.Accident,
							   SMCArr.Search,
							   SMCArr.ConstZone AS [Const.Zone],
							   SMCArr.SchoolZone AS [School Zone],
							   SMCArr.PostedSpeed,
							   SMCArr.AllegedSpeed,
							   SMCArr.HomeContactNumber AS [Home Phone],
							   SMCArr.WorkContactNumber AS [Work Phone],							   
							   SMCArr.DP2,
							   SMCArr.DPC,
							   SMCArr.Address_Status AS [Add.Status],
							   SMCArr.Reliable,
							   traffictickets.dbo.fn_DateFormat(SMCArr.InsertDate, 4, '/', ':', 1) AS [Insert Date],
							   --CONVERT(VARCHAR, SMCArr.InsertDate, 101) AS [Insert Date],
							   SMCArr.GroupID
						FROM   LoaderFilesArchive.dbo.SMC_DataArchive SMCArr
							   INNER JOIN LA_Houston.dbo.Tbl_HTS_LA_Group AS LAGroup
									ON  SMCArr.GroupID = LAGroup.GroupID
						WHERE  (@Mode = 1)
							   AND (REPLACE(REPLACE(SMCArr.TicketNumber, ' ', ''), '-', '') LIKE @TicketNo)
							   OR  (@Mode = 2)
							   AND (REPLACE(REPLACE(SMCArr.TicketNumber, ' ', ''), '-', '') LIKE @CauseNo)
							   OR  (@Mode = 3)
							   AND ( SMCArr.[FirstName] LIKE @FirstName
							   AND SMCArr.[LastName] LIKE @LastName
							   AND (DATEDIFF(DAY, ISNULL(@DOB, '1/1/1900'), '1/1/1900') = 0 OR DATEDIFF(DAY, ISNULL(@DOB, '1/1/1900'), SMCArr.DOB) = 0)
								   )
							   OR  (@Mode = 4)
							   AND (ISDATE(LAGroup.Load_Date) = 1)
							   AND (
									   DATEDIFF(DAY, RTRIM(LTRIM(LAGroup.Load_Date)), @CrtDate) 
									   = 0
								   )
							   OR  (@Mode = 5)
							   AND (ISDATE(SMCArr.Court_Date) = 1)
							   AND (DATEDIFF(DAY, SMCArr.Court_Date, @CrtDate) = 0)
							 
						ORDER BY SMCArr.GroupID, SMCArr.ID
					END
					--Adil 9698 09/29/2011 Added Jersey Village Arraignment Data Archive
					ELSE IF @LoaderId = 48 -- Jersey Village
					BEGIN
						SELECT DISTINCT TOP 1000
							   JVArr.ID AS ID,
							   CONVERT(VARCHAR, LAGroup.Load_Date, 101) AS [File Date],
							   JVArr.LastName AS [Last Name],
							   JVArr.FirstName AS [First Name],
							   JVArr.MiddleName AS [Middle Name],
							   JVArr.Age, --Farrukh 9873 11/11/2011 Parameters added (Race,Sex,Officer First Name, Office Last Name,Violation Date & Violation Location)
							   JVArr.Sex,
							   JVArr.Race,
							   JVArr.Officer_First_Name AS [Officer First Name],
							   JVArr.Officer_Last_Name AS [Officer Last Name],
							   JVArr.TicketNumber AS [Ticket No],
							   JVArr.AddressPartI AS [Address 1],
							   JVArr.AddressPartII AS [Adress 2],
							   JVArr.City,
							   JVArr.[State],
							   JVArr.ZipCode,
							   traffictickets.dbo.fn_DateFormat(JVArr.Court_Date, 4, '/', ':', 1) AS [Crt.Date],
							   traffictickets.dbo.fn_DateFormat(JVArr.Violation_Date, 4, '/', ':', 1) AS [Off.Date],
							   JVArr.Violation_Description AS [Description],
							   JVArr.Violation_Location AS [Off.Location],
							   JVArr.DP2,
							   JVArr.DPC,
							   JVArr.Address_Status AS [Add.Status],
							   JVArr.Reliable,
							   traffictickets.dbo.fn_DateFormat(ISNULL(JVArr.DOB, '01/01/1900'), 4, '/', ':', 1) AS [DOB], -- Rab Nawaz Khan 10465 10/09/2012 Added DOB Column in Row Data Search. . . 
							   traffictickets.dbo.fn_DateFormat(JVArr.InsertDate, 4, '/', ':', 1) AS [Insert Date],
							   JVArr.GroupID
						FROM   LoaderFilesArchive.dbo.JerseyVillage_DataArchive JVArr
							   INNER JOIN LA_Houston.dbo.Tbl_HTS_LA_Group AS LAGroup
									ON  JVArr.GroupID = LAGroup.GroupID
						WHERE  (@Mode = 1)
							   AND (REPLACE(REPLACE(JVArr.TicketNumber, ' ', ''), '-', '') LIKE @TicketNo)
							   OR  (@Mode = 2)
							   AND (REPLACE(REPLACE(JVArr.TicketNumber, ' ', ''), '-', '') LIKE @CauseNo)
							   OR  (
									   (@Mode = 3)
									   AND (
											   (
												   RTRIM(LTRIM(@LastName)) = '%'
												   AND (RTRIM(LTRIM(JVArr.FirstName)) LIKE @FirstName)
											   )
											   OR (
													  RTRIM(LTRIM(@FirstName)) = '%'
													  AND RTRIM(LTRIM(JVArr.LastName)) LIKE @LastName
												  )
											   OR (
													  RTRIM(LTRIM(JVArr.FirstName)) LIKE @FirstName
													  AND RTRIM(LTRIM(JVArr.LastName)) LIKE @LastName
												  )
										   )
								   )
							   OR  (@Mode = 4)
							   AND (ISDATE(LAGroup.Load_Date) = 1)
							   AND (
									   DATEDIFF(DAY, RTRIM(LTRIM(LAGroup.Load_Date)), @CrtDate) 
									   = 0
								   )
							   OR  (@Mode = 5)
							   AND (ISDATE(JVArr.Court_Date) = 1)
							   AND (DATEDIFF(DAY, JVArr.Court_Date, @CrtDate) = 0)
						ORDER BY JVArr.GroupID, JVArr.ID
					END
					--Adil 9715 09/29/2011 Added Jersey Village Arraignment Extended Data Archive
					ELSE IF @LoaderId = 49 -- Jersey Village Extended
					BEGIN
						SELECT DISTINCT TOP 1000
						   JVArr.ID AS ID,
						   CONVERT(VARCHAR, LAGroup.Load_Date, 101) AS [File Date],
						   JVArr.LastName AS [Last Name],
						   JVArr.FirstName AS [First Name],
						   JVArr.MiddleName AS [Middle Name],
						   JVArr.TicketNumber AS [Ticket No],
						   JVArr.CauseNumber AS [Cause Number],
							JVArr.LastLetter,
							JVArr.FineDue,
							JVArr.CostDue,
							JVArr.Violation_Description,
							JVArr.TotalDue,
							JVArr.OfficerCode,
							JVArr.OfficerFirstName,
							JVArr.OfficerLastName,
							JVArr.DLState,
							JVArr.Status,
							traffictickets.dbo.fn_DateFormat(JVArr.CourtDate, 4, '/', ':', 1) AS [Crt.Date],
							JVArr.BondSet,
							JVArr.Posted,
							JVArr.TimeDue,
							JVArr.CalendarType,
							JVArr.CourtLocation,
							traffictickets.dbo.fn_DateFormat(JVArr.FiledDate, 4, '/', ':', 1) AS [Filed Date],
							JVArr.Reliable,
							JVArr.GroupID,
							traffictickets.dbo.fn_DateFormat(JVArr.InsertDate, 4, '/', ':', 1) AS [Insert Date]
						FROM   LoaderFilesArchive.dbo.JerseyVillage_Arraignment_Extended_Datarchive JVArr
							   INNER JOIN LA_Houston.dbo.Tbl_HTS_LA_Group AS LAGroup
									ON  JVArr.GroupID = LAGroup.GroupID
						WHERE  
							(@Mode = 1) AND (REPLACE(REPLACE(JVArr.TicketNumber, ' ', ''), '-', '') LIKE @TicketNo)
							   OR  (@Mode = 2) AND (REPLACE(REPLACE(JVArr.CauseNumber, ' ', ''), '-', '') LIKE @CauseNo)
							   OR  (
									   (@Mode = 3)
									   AND (
											   (
												   RTRIM(LTRIM(@LastName)) = '%'
												   AND (RTRIM(LTRIM(JVArr.FirstName)) LIKE @FirstName)
											   )
											   OR (
													  RTRIM(LTRIM(@FirstName)) = '%'
													  AND RTRIM(LTRIM(JVArr.LastName)) LIKE @LastName
												  )
											   OR (
													  RTRIM(LTRIM(JVArr.FirstName)) LIKE @FirstName
													  AND RTRIM(LTRIM(JVArr.LastName)) LIKE @LastName
												  )
										   )
								   )
							   OR  (@Mode = 4)
							   AND (ISDATE(LAGroup.Load_Date) = 1)
							   AND (
									   DATEDIFF(DAY, RTRIM(LTRIM(LAGroup.Load_Date)), @CrtDate) 
									   = 0
								   )
							   OR  (@Mode = 5)
							   AND (ISDATE(JVArr.CourtDate) = 1)
							   AND (DATEDIFF(DAY, JVArr.CourtDate, @CrtDate) = 0)
						ORDER BY JVArr.GroupID, JVArr.ID
					END
					--Adil 9664 11/03/2011 Added Harris Business Startup Data Archive
					ELSE IF @LoaderId = 50 -- Harris Business Startup
					BEGIN
						SELECT DISTINCT TOP 1000
						HCBUDA.ID,
						HCBUDA.FileNumber,
						HCBUDA.BusinessName,
						HCBUDA.BusinessAddress,
						HCBUDA.BusinessCity,
						HCBUDA.BusinessState,
						HCBUDA.BusinessZipCode,
						HCBUDA.OwnerDetails,
						HCBUDA.StatusType,
						traffictickets.dbo.fn_DateFormat(HCBUDA.Date, 4, '/', ':', 1) AS [Reg.Date],
						HCBUDA.FilmCode,
						traffictickets.dbo.fn_DateFormat(HCBUDA.FiledDate, 4, '/', ':', 1) AS [Rec. Date],
						HCBUDA.LoaderID,
						HCBUDA.GroupID
						FROM   LoaderFilesArchive.dbo.HarrisCounty_BusinessStartUp_DataArchive HCBUDA
						WHERE  
							(@Mode = 1) AND (REPLACE(REPLACE(HCBUDA.FileNumber, ' ', ''), '-', '') LIKE @TicketNo)
							--Farrukh 9664 01/09/2012 Added File Number filter
							OR (@Mode = 2) AND (REPLACE(REPLACE(HCBUDA.FileNumber, ' ', ''), '-', '') LIKE @FileNumber)
							   OR  (
									   (@Mode = 3)
									   AND (
												(
												RTRIM(LTRIM(HCBUDA.BusinessName)) LIKE @LastName
												)
												AND
												(
												RTRIM(LTRIM(HCBUDA.OwnerDetails)) LIKE @FirstName
												)
										   )
								   )
							   OR  (@Mode = 4)
							   AND (ISDATE(HCBUDA.FiledDate) = 1)
							   AND (
									   DATEDIFF(DAY, RTRIM(LTRIM(HCBUDA.FiledDate)), @CrtDate) 
									   = 0
								   )
							   OR  (@Mode = 5)
							   AND (ISDATE(HCBUDA.Date) = 1)
							   AND (DATEDIFF(DAY, HCBUDA.Date, @CrtDate) = 0)
						ORDER BY HCBUDA.GroupID, HCBUDA.ID
					END
					--Zeeshan 10304 07/06/2012 Tarrant County Criminal Court
					ELSE IF (@LoaderId = 61 OR @LoaderId = 62) -- Booked-In OR Bond-Out
					BEGIN						
						SELECT DISTINCT TOP 1000 TCCC_BI_BO.Prefix,
						       TCCC_BI_BO.FirstName AS [First Name], TCCC_BI_BO.LastName AS [Last Name],
						       TCCC_BI_BO.Suffix, TCCC_BI_BO.[Address],
						       TCCC_BI_BO.City, TCCC_BI_BO.[State], TCCC_BI_BO.Zip,
						       TCCC_BI_BO.BarCode AS [Bar Code], TCCC_BI_BO.Race, TCCC_BI_BO.CID,
						       CONVERT(VARCHAR(10), TCCC_BI_BO.ListDate, 101) AS [List Date],
						       TCCC_BI_BO.OffenseCode AS [Offense Code],
						       TCCC_BI_BO.ViolationDescription AS [Offense Description]
						FROM TCCC_BookedIn_BondOut_FirstSetting_PassToHire TCCC_BI_BO
						WHERE (TCCC_BI_BO.FirstName <> '' AND TCCC_BI_BO.LoaderId = @LoaderId)
						AND
							(
								((@Mode = 2) AND(REPLACE(TCCC_BI_BO.CID, ' ','') LIKE @FileNumber))
								OR
								((@Mode = 3) AND((RTRIM(LTRIM(@LastName)) = '%' AND (RTRIM(LTRIM(TCCC_BI_BO.FirstName)) LIKE @FirstName))
													OR (RTRIM(LTRIM(@FirstName)) = '%'AND RTRIM(LTRIM(TCCC_BI_BO.LastName)) LIKE @LastName)
													OR (RTRIM(LTRIM(TCCC_BI_BO.FirstName)) LIKE @FirstName AND RTRIM(LTRIM(TCCC_BI_BO.LastName)) LIKE @LastName)
								))
								OR
								((@Mode = 4) AND (DATEDIFF(DAY, TCCC_BI_BO.ListDate, @CrtDate) = 0))
								
							)
						
					END					
					ELSE IF @LoaderId = 63 --First Setting 
					BEGIN
						SELECT DISTINCT TOP 1000 TCCC_FS_PTH.Prefix,
						       TCCC_FS_PTH.FirstName AS [First Name], TCCC_FS_PTH.LastName AS [Last Name],
						       TCCC_FS_PTH.Suffix, TCCC_FS_PTH.[Address],
						       TCCC_FS_PTH.City, TCCC_FS_PTH.[State], TCCC_FS_PTH.Zip,
						       TCCC_FS_PTH.BarCode AS [Bar Code],
						       CONVERT(VARCHAR(10), TCCC_FS_PTH.CourtDate, 101) AS [Court Date],
						       TCCC_FS_PTH.CourtRoom AS [Court Room], 
						       TCCC_FS_PTH.ViolationDescription AS [Offense Description]
						FROM TCCC_BookedIn_BondOut_FirstSetting_PassToHire TCCC_FS_PTH
						WHERE (TCCC_FS_PTH.FirstName <> '' AND TCCC_FS_PTH.LoaderId = @LoaderId)
						AND
						(
								((@Mode = 3) AND((RTRIM(LTRIM(@LastName)) = '%' AND (RTRIM(LTRIM(TCCC_FS_PTH.FirstName)) LIKE @FirstName))
													OR (RTRIM(LTRIM(@FirstName)) = '%'AND RTRIM(LTRIM(TCCC_FS_PTH.LastName)) LIKE @LastName)
													OR (RTRIM(LTRIM(TCCC_FS_PTH.FirstName)) LIKE @FirstName AND RTRIM(LTRIM(TCCC_FS_PTH.LastName)) LIKE @LastName)
								))
								OR
								((@Mode = 4) AND (DATEDIFF(DAY, TCCC_FS_PTH.ListDate, @CrtDate) = 0))
								OR
								((@Mode = 5) AND (DATEDIFF(DAY, TCCC_FS_PTH.CourtDate, @CrtDate) = 0))
						)						 
					END
					ELSE IF @LoaderId = 64 --Pass to Hire
					BEGIN
						SELECT DISTINCT TOP 1000 TCCC_FS_PTH.Prefix,
						       TCCC_FS_PTH.FirstName AS [First Name], TCCC_FS_PTH.LastName AS [Last Name],
						       TCCC_FS_PTH.Suffix, TCCC_FS_PTH.[Address],
						       TCCC_FS_PTH.City, TCCC_FS_PTH.[State], TCCC_FS_PTH.Zip,
						       TCCC_FS_PTH.BarCode AS [Bar Code], TCCC_FS_PTH.Race,
						       CONVERT(VARCHAR(10), TCCC_FS_PTH.ListDate, 101) AS [List Date],
						       CONVERT(VARCHAR(10), TCCC_FS_PTH.CourtDate, 101) AS [Court Date],
						       TCCC_FS_PTH.CourtRoom AS [Court Room], TCCC_FS_PTH.OffenseCode AS [Offense Code],
						       TCCC_FS_PTH.ViolationDescription AS [Offense Description]
						FROM TCCC_BookedIn_BondOut_FirstSetting_PassToHire TCCC_FS_PTH
						WHERE (TCCC_FS_PTH.FirstName <> '' AND TCCC_FS_PTH.LoaderId = @LoaderId)
						AND
						(
								((@Mode = 3) AND((RTRIM(LTRIM(@LastName)) = '%' AND (RTRIM(LTRIM(TCCC_FS_PTH.FirstName)) LIKE @FirstName))
													OR (RTRIM(LTRIM(@FirstName)) = '%'AND RTRIM(LTRIM(TCCC_FS_PTH.LastName)) LIKE @LastName)
													OR (RTRIM(LTRIM(TCCC_FS_PTH.FirstName)) LIKE @FirstName AND RTRIM(LTRIM(TCCC_FS_PTH.LastName)) LIKE @LastName)
								))
								OR
								((@Mode = 4) AND (DATEDIFF(DAY, TCCC_FS_PTH.ListDate, @CrtDate) = 0))
								OR
								((@Mode = 5) AND (DATEDIFF(DAY, TCCC_FS_PTH.CourtDate, @CrtDate) = 0))
						)						 
					END

				ELSE IF @LoaderId = 65 -- Rab Nawaz Khan 10401 Added Missouri Court Loader data. . . 
					BEGIN
						SELECT DISTINCT TOP 1000
							   missoury.ID AS ID,
							   traffictickets.dbo.fn_DateFormat(LAGroup.Load_Date, 4, '/', ':', 1) AS [File Date],
							   missoury.LastName AS [Last Name],
							   missoury.FirstName AS [First Name],
							   missoury.MiddleName AS [Middle Name],
							   missoury.TicketNumber AS [Ticket No],
							   missoury.TicketNumber AS [Cause No],
							   missoury.AddressPartI AS [Address 1],
							   missoury.AddressPartII AS [Adress 2],
							   missoury.City,
							   missoury.[State],
							   missoury.ZipCode,
							   traffictickets.dbo.fn_DateFormat(missoury.Court_Date, 4, '/', ':', 1) AS [Crt.Date],
							   missoury.Violation_Description AS [Description],
							   traffictickets.dbo.fn_DateFormat(missoury.ViolationDate, 4, '/', ':', 1) AS [Vio.Date],
							   missoury.ViolationLocation AS [vio.Loc],
							   missoury.OfficerName AS [Officer Name],
							   missoury.VehInfo AS [Veh.Info],
							   traffictickets.dbo.fn_DateFormat(missoury.DOB, 4, '/', ':', 1) AS [DOB],
							   missoury.Sex,
							   missoury.Race,
							   missoury.Radar_Laser AS [Radar Laser],
							   missoury.Arrest,
							   missoury.Accident,
							   missoury.Search,
							   missoury.ConstZone AS [Const.Zone],
							   missoury.SchoolZone AS [School Zone],
							   missoury.PostedSpeed,
							   missoury.AllegedSpeed,
							   missoury.HomeContactNumber AS [Home Phone],
							   missoury.WorkContactNumber AS [Work Phone],							   
							   missoury.DP2,
							   missoury.DPC,
							   missoury.Address_Status AS [Add.Status],
							   missoury.Reliable,
							   traffictickets.dbo.fn_DateFormat(missoury.InsertDate, 4, '/', ':', 1) AS [Insert Date],
							   missoury.GroupID
						FROM   LoaderFilesArchive.dbo.Missoury_DataArchive missoury
							   INNER JOIN LA_Houston.dbo.Tbl_HTS_LA_Group AS LAGroup
									ON  missoury.GroupID = LAGroup.GroupID
						WHERE  (@Mode = 1)
							   AND (REPLACE(REPLACE(missoury.TicketNumber, ' ', ''), '-', '') LIKE @TicketNo)
							   OR  (@Mode = 2)
							   AND (REPLACE(REPLACE(missoury.TicketNumber, ' ', ''), '-', '') LIKE @CauseNo)
							   OR  (@Mode = 3)
							   AND ( missoury.[FirstName] LIKE @FirstName
							   AND missoury.[LastName] LIKE @LastName
							   AND (DATEDIFF(DAY, ISNULL(@DOB, '1/1/1900'), '1/1/1900') = 0 OR DATEDIFF(DAY, ISNULL(@DOB, '1/1/1900'), missoury.DOB) = 0)
								   )
							   OR  (@Mode = 4)
							   AND (ISDATE(LAGroup.Load_Date) = 1)
							   AND (
									   DATEDIFF(DAY, RTRIM(LTRIM(LAGroup.Load_Date)), @CrtDate) 
									   = 0
								   )
							   OR  (@Mode = 5)
							   AND (ISDATE(missoury.Court_Date) = 1)
							   AND (DATEDIFF(DAY, missoury.Court_Date, @CrtDate) = 0)
							 
						ORDER BY missoury.GroupID, missoury.ID
					END
				ELSE IF @LoaderId = 66 -- Rab Nawaz Khan 10436 09/15/2012 Added Baytown Court Loader data. . . 
					BEGIN
						SELECT DISTINCT TOP 1000
							   baytown.ID AS ID,
							   traffictickets.dbo.fn_DateFormat(LAGroup.Load_Date, 4, '/', ':', 1) AS [File Date],
							   baytown.LastName AS [Last Name],
							   baytown.FirstName AS [First Name],
							   baytown.MiddleName AS [Middle Name],
							   baytown.TicketNumber AS [Ticket No],
							   baytown.TicketNumber AS [Cause No],
							   baytown.AddressPartI AS [Address 1],
							   baytown.AddressPartII AS [Adress 2],
							   baytown.City,
							   baytown.[State],
							   baytown.ZipCode,
							   traffictickets.dbo.fn_DateFormat(baytown.Court_Date, 4, '/', ':', 1) AS [Crt.Date],
							   baytown.Violation_Description AS [Description],
							   traffictickets.dbo.fn_DateFormat(baytown.ViolationDate, 4, '/', ':', 1) AS [Vio.Date],
							   baytown.ViolationLocation AS [vio.Loc],
							   baytown.OfficerName AS [Officer Name],
							   baytown.VehInfo AS [Veh.Info],
							   traffictickets.dbo.fn_DateFormat(baytown.DOB, 4, '/', ':', 1) AS [DOB],
							   baytown.Sex,
							   baytown.Race,
							   baytown.Radar_Laser AS [Radar Laser],
							   baytown.Arrest,
							   baytown.Accident,
							   baytown.Search,
							   baytown.ConstZone AS [Const.Zone],
							   baytown.SchoolZone AS [School Zone],
							   baytown.PostedSpeed,
							   baytown.AllegedSpeed,
							   baytown.HomeContactNumber AS [Home Phone],
							   baytown.WorkContactNumber AS [Work Phone],							   
							   baytown.DP2,
							   baytown.DPC,
							   baytown.Address_Status AS [Add.Status],
							   baytown.Reliable,
							   traffictickets.dbo.fn_DateFormat(baytown.InsertDate, 4, '/', ':', 1) AS [Insert Date],
							   baytown.GroupID
						FROM   LoaderFilesArchive.dbo.Baytown_DataArchive baytown
							   INNER JOIN LA_Houston.dbo.Tbl_HTS_LA_Group AS LAGroup
									ON  baytown.GroupID = LAGroup.GroupID
						WHERE  (@Mode = 1)
							   AND (REPLACE(REPLACE(baytown.TicketNumber, ' ', ''), '-', '') LIKE @TicketNo)
							   OR  (@Mode = 2)
							   AND (REPLACE(REPLACE(baytown.TicketNumber, ' ', ''), '-', '') LIKE @CauseNo)
							   OR  (@Mode = 3)
							   AND ( baytown.[FirstName] LIKE @FirstName
							   AND baytown.[LastName] LIKE @LastName
							   AND (DATEDIFF(DAY, ISNULL(@DOB, '1/1/1900'), '1/1/1900') = 0 OR DATEDIFF(DAY, ISNULL(@DOB, '1/1/1900'), baytown.DOB) = 0)
								   )
							   OR  (@Mode = 4)
							   AND (ISDATE(LAGroup.Load_Date) = 1)
							   AND (
									   DATEDIFF(DAY, RTRIM(LTRIM(LAGroup.Load_Date)), @CrtDate) 
									   = 0
								   )
							   OR  (@Mode = 5)
							   AND (ISDATE(baytown.Court_Date) = 1)
							   AND (DATEDIFF(DAY, baytown.Court_Date, @CrtDate) = 0)
							 
						ORDER BY baytown.GroupID, baytown.ID
					END
				ELSE IF @LoaderId IN (69, 70) -- Farrukh 10438 11/16/2012 HCCC Criminal Filings Daily & HCCC Criminal Filings Monthly
					BEGIN						
						SELECT TOP 1000
						   ID AS [Row Id],
						   CDI,
						   [Ticket Number],
						   [Violation Description],
						   [Court Number],
						   [Last Name],
						   [First Name],
						   [SPN Number],
						   [Race],
						   [Sex],
						   [DOB],
						   CST,
						   DST,
						   [Bond Amount],
						   [Address Part I] AS [Address 1],
						   City,
						   [State],
						   [ZipCode] AS Zip,
						   [Attorney Last Name],
						   [Attorney Middle Name],
						   [Attorney First Name],
						   [Attorney SPN Number],
						   ins,
						   cad,
						   ChargeLevel,
						   cnc,
						   rea,
						   rundate,
						   fda,
						   curr_off,
						   nda,
						   aty_coc,
						   aty_coc_lit,
						   HCCCD.GroupID,
						   CONVERT(VARCHAR, HCCCD.[Insertion Date], 101) AS [Insert Date],
						   CONVERT(VARCHAR, thlg.Load_Date, 101) AS [File Date]
					FROM   LoaderFilesArchive.dbo.HCCC_Criminal_Filings_Daily_DataArchive HCCCD
						   INNER JOIN LA_Houston.dbo.Tbl_HTS_LA_Group thlg
								ON  thlg.GroupID = HCCCD.GroupID
					WHERE  (
							   @Mode = 1
							   AND REPLACE(REPLACE([Ticket Number], ' ', ''), '-', '') LIKE @TicketNo
						   )
						   OR  (
								   (@Mode = 3)
								   AND (
										   (
											   RTRIM(LTRIM(@LastName)) = '%'
											   AND (RTRIM(LTRIM(HCCCD.[First Name])) LIKE @FirstName)
										   )
										   OR (
												  RTRIM(LTRIM(@FirstName)) = '%'
												  AND RTRIM(LTRIM(HCCCD.[Last Name])) LIKE @LastName
											  )
										   OR (
												  RTRIM(LTRIM(HCCCD.[First Name])) LIKE @FirstName
												  AND RTRIM(LTRIM(HCCCD.[Last Name])) LIKE @LastName
											  )
									   )
							   )
						   OR  (
								   @Mode = 4
								   AND ISDATE(thlg.Load_Date) = 1
								   AND DATEDIFF(DAY, thlg.Load_Date, @CrtDate) = 0
							   )
					ORDER BY
						   HCCCD.ID
					END
					ELSE IF @LoaderId = 71  -- Farrukh 10438 11/16/2012 Misdemeanor JIMS-009
					BEGIN						
						SELECT TOP 1000
						   ID AS [Row Id],
						   [Bondsman Name],
						   [JP],
						   [JP NO],						   
						   [Ticket Number],
						   [Violation Description],						   
						   [Court Date],
						   [Court Number],
						   [Last Name],
						   [First Name],
						   [Middle Name],
						   [INST],
						   [Bond Amount],						   
						   JIMS009.GroupID,
						   CONVERT(VARCHAR, JIMS009.[Insertion Date], 101) AS [Insert Date],
						   CONVERT(VARCHAR, thlg.Load_Date, 101) AS [File Date]						   
					FROM   LoaderFilesArchive.dbo.HCCC_Misdemeanor_JIMS_009_DataArchive JIMS009
						   INNER JOIN LA_Houston.dbo.Tbl_HTS_LA_Group thlg
								ON  thlg.GroupID = JIMS009.GroupID
					WHERE  (
							   @Mode = 1
							   AND REPLACE(REPLACE([Ticket Number], ' ', ''), '-', '') LIKE @TicketNo
						   )
						   OR  (
								   (@Mode = 3)
								   AND (
										   (
											   RTRIM(LTRIM(@LastName)) = '%'
											   AND (RTRIM(LTRIM(JIMS009.[First Name])) LIKE @FirstName)
										   )
										   OR (
												  RTRIM(LTRIM(@FirstName)) = '%'
												  AND RTRIM(LTRIM(JIMS009.[Last Name])) LIKE @LastName
											  )
										   OR (
												  RTRIM(LTRIM(JIMS009.[First Name])) LIKE @FirstName
												  AND RTRIM(LTRIM(JIMS009.[Last Name])) LIKE @LastName
											  )
									   )
							   )
						   OR  (
								   @Mode = 4
								   AND ISDATE(thlg.Load_Date) = 1
								   AND DATEDIFF(DAY, thlg.Load_Date, @CrtDate) = 0
						   )
						  OR (@Mode = 5)
							   AND (ISDATE(JIMS009.[Court Date]) = 1)
							   AND (DATEDIFF(DAY, JIMS009.[Court Date], @CrtDate) = 0)
					ORDER BY
						   JIMS009.ID
					END			
					ELSE IF @LoaderId = 72  -- Farrukh 10438 11/16/2012 HCCC Felony 203
					BEGIN						
						SELECT TOP 1000
						   ID AS [Row Id],
						   [Bondsman Name],
						   [JP],
						   [JP NO],						   
						   [Ticket Number],
						   [Violation Description],						   
						   [Court Date],
						   [Last Name],
						   [First Name],
						   [Middle Name],
						   [Reason1],
						   [Future Date],
						   [Reason2],
						   [CRT],
						   [INST],
						   [Bond Amount],						   
						   JIMS203.GroupID,
						   CONVERT(VARCHAR, JIMS203.[Insertion Date], 101) AS [Insert Date],
						   CONVERT(VARCHAR, thlg.Load_Date, 101) AS [File Date]
					FROM   LoaderFilesArchive.dbo.HCCC_Felony_JIMS_023_DataArchive JIMS203
						   INNER JOIN LA_Houston.dbo.Tbl_HTS_LA_Group thlg
								ON  thlg.GroupID = JIMS203.GroupID
					WHERE  (
							   @Mode = 1
							   AND REPLACE(REPLACE([Ticket Number], ' ', ''), '-', '') LIKE @TicketNo
						   )
						   OR  (
								   (@Mode = 3)
								   AND (
										   (
											   RTRIM(LTRIM(@LastName)) = '%'
											   AND (RTRIM(LTRIM(JIMS203.[First Name])) LIKE @FirstName)
										   )
										   OR (
												  RTRIM(LTRIM(@FirstName)) = '%'
												  AND RTRIM(LTRIM(JIMS203.[Last Name])) LIKE @LastName
											  )
										   OR (
												  RTRIM(LTRIM(JIMS203.[First Name])) LIKE @FirstName
												  AND RTRIM(LTRIM(JIMS203.[Last Name])) LIKE @LastName
											  )
									   )
							   )
						   OR  (
								   @Mode = 4
								   AND ISDATE(thlg.Load_Date) = 1
								   AND DATEDIFF(DAY, thlg.Load_Date, @CrtDate) = 0
						   )
						   OR (@Mode = 5)
							   AND (ISDATE(JIMS203.[Court Date]) = 1)
							   AND (DATEDIFF(DAY, JIMS203.[Court Date], @CrtDate) = 0)
					ORDER BY
						   JIMS203.ID
					END
					ELSE IF @LoaderId = 73 -- Farrukh 10438 11/16/2012 HCCC Criminal Dispose Monthly
					BEGIN						
						SELECT TOP 1000
						   ID AS [Row Id],
						   CDI,
						   [Ticket Number],
						   [Violation Description],
						   [Court Number],
						   [Last Name],
						   [First Name],
						   [Middle Name],
						   [SPN Number],
						   [Race],
						   [Sex],
						   [DOB],
						   CST,
						   DST,
						   [Bond Amount],
						   [Address Part I] AS [Address 1],
						   [Address Part II] AS [Address 2],
						   City,
						   [State],
						   [ZipCode] AS Zip,
						   [Attorney Last Name],
						   [Attorney Middle Name],
						   [Attorney First Name],
						   [Attorney SPN Number],
						   ins,
						   cad,
						   ChargeLevel,
						   cnc,
						   rea,
						   rundate,
						   fda,
						   curr_off,
						   nda,
						   aty_coc,
						   aty_coc_lit,
						   dispdt,
						   disposition,
						   sentence,
						   HCCCDM.GroupID,
						   CONVERT(VARCHAR, HCCCDM.[Insertion Date], 101) AS [Insert Date],
						   CONVERT(VARCHAR, thlg.Load_Date, 101) AS [File Date]
					FROM   LoaderFilesArchive.dbo.HCCC_Criminal_Disposed_Monthly_DataArchive HCCCDM
						   INNER JOIN LA_Houston.dbo.Tbl_HTS_LA_Group thlg
								ON  thlg.GroupID = HCCCDM.GroupID
					WHERE  (
							   @Mode = 1
							   AND REPLACE(REPLACE([Ticket Number], ' ', ''), '-', '') LIKE @TicketNo
						   )
						   OR  (
								   (@Mode = 3)
								   AND (
										   (
											   RTRIM(LTRIM(@LastName)) = '%'
											   AND (RTRIM(LTRIM(HCCCDM.[First Name])) LIKE @FirstName)
										   )
										   OR (
												  RTRIM(LTRIM(@FirstName)) = '%'
												  AND RTRIM(LTRIM(HCCCDM.[Last Name])) LIKE @LastName
											  )
										   OR (
												  RTRIM(LTRIM(HCCCDM.[First Name])) LIKE @FirstName
												  AND RTRIM(LTRIM(HCCCDM.[Last Name])) LIKE @LastName
											  )
									   )
							   )
						   OR  (
								   @Mode = 4
								   AND ISDATE(thlg.Load_Date) = 1
								   AND DATEDIFF(DAY, thlg.Load_Date, @CrtDate) = 0
							   )
					ORDER BY
						   HCCCDM.ID
					END
					ELSE IF @LoaderId = 67 -- Farrukh 10447 12/19/2012 Crash Data Texas Dept of Transportation
					BEGIN						
						SELECT TOP 1000
						   RecordId AS [Row Id],
						   Crash_ID,
							Crash_Fatal_Fl,
							Cmv_Involv_Fl,
							Schl_Bus_Fl,
							Rr_Relat_Fl,
							Medical_Advisory_Fl,
							Amend_Supp_Fl,
							Active_School_Zone_Fl,
							Crash_Time,
							Case_ID,
							Local_Use,
							Rpt_CRIS_Cnty_ID,
							Rpt_City_ID,
							Rpt_Outside_City_Limit_Fl,
							Thousand_Damage_Fl,
							Rpt_Latitude,
							Rpt_Longitude,
							Rpt_Rdwy_Sys_ID,
							Rpt_Hwy_Num,
							Rpt_Hwy_Sfx,
							Rpt_Road_Part_ID,
							Rpt_Block_Num,
							Rpt_Street_Pfx,
							Rpt_Street_Name,
							Rpt_Street_Sfx,
							Private_Dr_Fl,
							Toll_Road_Fl,
							Crash_Speed_Limit,
							Road_Constr_Zone_Fl,
							Road_Constr_Zone_Wrkr_Fl,
							Rpt_Street_Desc,
							At_Intrsct_Fl,
							Rpt_Sec_Rdwy_Sys_ID,
							Rpt_Sec_Hwy_Num,
							Rpt_Sec_Hwy_Sfx,
							Rpt_Sec_Road_Part_ID,
							Rpt_Sec_Block_Num,
							Rpt_Sec_Street_Pfx,
							Rpt_Sec_Street_Name,
							Rpt_Sec_Street_Sfx,
							Rpt_Ref_Mark_Offset_Amt,
							Rpt_Ref_Mark_Dist_Uom,
							Rpt_Ref_Mark_Dir,
							Rpt_Ref_Mark_Nbr,
							Rpt_Sec_Street_Desc,
							Rpt_CrossingNumber,
							Wthr_Cond_ID,
							Light_Cond_ID,
							Entr_Road_ID,
							Road_Type_ID,
							Road_Algn_ID,
							Surf_Cond_ID,
							Traffic_Cntl_ID,
							Investigat_Notify_Time,
							Investigat_Notify_Meth,
							Investigat_Arrv_Time,
							Investigat_Comp_Fl,
							ORI_Number,
							Investigat_Agency_ID,
							Investigat_Area_ID,
							Investigat_District_ID,
							Investigat_Region_ID,
							Bridge_Detail_ID,
							Harm_Evnt_ID,
							Intrsct_Relat_ID,
							FHE_Collsn_ID,
							Obj_Struck_ID,
							Othr_Factr_ID,
							Road_Part_Adj_ID,
							Road_Cls_ID,
							Road_Relat_ID,
							Phys_Featr_1_ID,
							Phys_Featr_2_ID,
							Cnty_ID,
							City_ID,
							Latitude,
							Longitude,
							Hwy_Sys,
							Hwy_Nbr,
							Hwy_Sfx,
							Dfo,
							Street_Name,
							Street_Nbr,
							Control,
							[Section],
							Milepoint,
							Ref_Mark_Nbr,
							Ref_Mark_Displ,
							Hwy_Sys_2,
							Hwy_Nbr_2,
							Hwy_Sfx_2,
							Street_Name_2,
							Street_Nbr_2,
							Control_2,
							Section_2,
							Milepoint_2,
							Txdot_Rptable_Fl,
							Onsys_Fl,
							Rural_Fl,
							Crash_Sev_ID,
							Pop_Group_ID,
							Located_Fl,
							Day_of_Week,
							Hwy_Dsgn_Lane_ID,
							Hwy_Dsgn_Hrt_ID,
							Hp_Shldr_Left,
							Hp_Shldr_Right,
							Hp_Median_Width,
							Base_Type_ID,
							Nbr_Of_Lane,
							Row_Width_Usual,
							Roadbed_Width,
							Surf_Width,
							Surf_Type_ID,
							Curb_Type_Left_ID,
							Curb_Type_Right_ID,
							Shldr_Type_Left_ID,
							Shldr_Width_Left,
							Shldr_Use_Left_ID,
							Shldr_Type_Right_ID,
							Shldr_Width_Right,
							Shldr_Use_Right_ID,
							Median_Type_ID,
							Median_Width,
							Rural_Urban_Type_ID,
							Func_Sys_ID,
							Adt_Curnt_Amt,
							Adt_Curnt_Year,
							Adt_Adj_Curnt_Amt,
							Pct_Single_Trk_Adt,
							Pct_Combo_Trk_Adt,
							Trk_Aadt_Pct,
							Curve_Type_ID,
							Curve_Lngth,
							Cd_Degr,
							Delta_Left_Right_ID,
							Dd_Degr,
							Feature_Crossed,
							Structure_Number,
							I_R_Min_Vert_Clear,
							Approach_Width,
							Bridge_Median_ID,
							Bridge_Loading_Type_ID,
							Bridge_Loading_In_1000_Lbs,
							Bridge_Srvc_Type_On_ID,
							Bridge_Srvc_Type_Under_ID,
							Culvert_Type_ID,
							Roadway_Width,
							Deck_Width,
							Bridge_Dir_Of_Traffic_ID,
							Bridge_Rte_Struct_Func_ID,
							Bridge_IR_Struct_Func_ID,
							CrossingNumber,
							RRCo,
							Poscrossing_ID,
							WDCode_ID,
							Standstop,
							Yield,
							Incap_Injry_Cnt,
							Nonincap_Injry_Cnt,
							Poss_Injry_Cnt,
							Non_Injry_Cnt,
							Unkn_Injry_Cnt,
							Tot_Injry_Cnt,
							Death_Cnt,
						   TTC.GroupID,
						   CONVERT(VARCHAR, TTC.[InsertDate], 101) AS [Insert Date],
						   CONVERT(VARCHAR, thlg.Load_Date, 101) AS [File Date]
						FROM Crash.dbo.tbl_Texas_Transportation_Crash TTC
						   INNER JOIN LA_Houston.dbo.Tbl_HTS_LA_Group thlg
								ON  thlg.GroupID = TTC.GroupID
					WHERE  (
							   @Mode = 1
							   AND REPLACE(REPLACE([Crash_ID], ' ', ''), '-', '') LIKE @TicketNo
						   )
						   OR  (
								   (@Mode = 3)
								   AND REPLACE(REPLACE([Case_ID], ' ', ''), '-', '') LIKE @CauseNo
							   )
						   OR  (
								   @Mode = 4
								   AND ISDATE(thlg.Load_Date) = 1
								   AND DATEDIFF(DAY, thlg.Load_Date, @CrtDate) = 0
							   )
					ORDER BY
						   TTC.RecordID
					END
					ELSE IF @LoaderId = 68 -- Farrukh 10447 12/19/2012 Person Data Texas Dept of Transportation
					BEGIN						
						SELECT TOP 1000
						   RecordId AS [Row Id],
						   Crash_ID,
							Unit_Nbr,
							Prsn_Nbr,
							Prsn_Type_ID,
							Prsn_Occpnt_Pos_ID,
							Prsn_Injry_Sev_ID,
							Prsn_Age,
							Prsn_Ethnicity_ID,
							Prsn_Gndr_ID,
							Prsn_Ejct_ID,
							Prsn_Rest_ID,
							Prsn_Airbag_ID,
							Prsn_Helmet_ID,
							Prsn_Sol_Fl,
							Prsn_Alc_Spec_Type_ID,
							Prsn_Alc_Rslt_ID,
							Prsn_Bac_Test_Rslt,
							Prsn_Drg_Spec_Type_ID,
							Prsn_Drg_Rslt_ID,
							Prsn_Death_Time,
							Incap_Injry_Cnt,
							Nonincap_Injry_Cnt,
							Poss_Injry_Cnt,
							Non_Injry_Cnt,
							Unkn_Injry_Cnt,
							Tot_Injry_Cnt,
							Death_Cnt,
							IncidentNumber,
							CrashDate,
							Location,
							TTP.GroupID,
						   CONVERT(VARCHAR, TTP.[InsertDate], 101) AS [Insert Date],
						   CONVERT(VARCHAR, thlg.Load_Date, 101) AS [File Date]
						FROM Crash.dbo.tbl_Texas_Transportation_Person TTP
						   INNER JOIN LA_Houston.dbo.Tbl_HTS_LA_Group thlg
								ON  thlg.GroupID = TTP.GroupID
					WHERE  (
							   @Mode = 1
							   AND REPLACE(REPLACE([Crash_ID], ' ', ''), '-', '') LIKE @TicketNo
						   )
						   OR  (
								   @Mode = 4
								   AND ISDATE(thlg.Load_Date) = 1
								   AND DATEDIFF(DAY, thlg.Load_Date, @CrtDate) = 0
							   )
					ORDER BY
						   TTP.RecordID
					END
					ELSE IF @LoaderId = 74 -- Zeeshan 10595 01/08/2013 Austin Municipal Loader
					BEGIN
						SELECT TOP 1000
							ad.LastName AS [Last Name],
							ad.FirstName AS [First Name],
							ad.MiddleName AS [Middle Name],
							ad.AddressPartI AS [Address],
							ad.Telephone,
							ad.DriverLicenseClass AS [Driver License Class],
							ad.[Ticket Number],
							ad.ViolationCode AS [Violation Code],
							ad.ViolationDescription AS [Violation Description],
							ad.ViolationLocation AS [Violation Location],
							ad.ScheduledEvent AS [Future Scheduled Event],
							ad.Disposition,
							CONVERT(VARCHAR, ad.CourtDate, 101) AS [Court Date],							
							ad.CourtTime AS [Court Time],
							ad.CourtNo AS [Court Number],
							ad.OfficerLastName + ' ' + ad.OfficerFirstName AS [Officer Name],
							ad.OfficerNo AS [Officer Number],
							ad.BalanceDue AS [Balance Due],
							ad.WarrantType AS [Warrant Type],							
							CONVERT(VARCHAR, ad.ViolationDate, 101) AS [Violation Date],
							ad.GroupID,
							CONVERT(VARCHAR, ad.InsertDate, 101) AS [Insert Date]							
						FROM LoaderFilesArchive.dbo.Austin_Municipal_DataArchive ad
						INNER JOIN LA_Houston.dbo.Tbl_HTS_LA_Group thlg
						 ON thlg.GroupID = ad.GroupID
						WHERE  (
						(@Mode = 1 OR @Mode = 2)
							AND (
							(REPLACE(ad.[Ticket Number], ' ', '') LIKE @TicketNo)
							OR (REPLACE(ad.[Ticket Number], ' ', '') LIKE @CauseNo)
						)
						)
						OR (
							(@Mode = 5)
							   AND (ISDATE(ad.CourtDate) = 1)
							   AND (DATEDIFF(DAY, ad.CourtDate, @CrtDate) = 0)
						)
						OR  (							
							(@Mode = 3)
								   AND (
										   (
											   RTRIM(LTRIM(@LastName)) = '%'
											   AND (RTRIM(LTRIM(ad.FirstName)) LIKE @FirstName)
										   )
										   OR (
												  RTRIM(LTRIM(@FirstName)) = '%'
												  AND RTRIM(LTRIM(ad.LastName)) LIKE @LastName
											  )
										   OR (
												  RTRIM(LTRIM(ad.FirstName)) LIKE @FirstName
												  AND RTRIM(LTRIM(ad.LastName)) LIKE @LastName
											  )
									   )
						)
						OR  (
							(@Mode = 4)
							AND(
								ISDATE(ad.InsertDate) = 1
								AND DATEDIFF(DAY, ad.InsertDate, @CrtDate) = 0
							)
						)
					END
					-- Rab Nawaz 10729 03/21/2013 Added NISI Cases Loader. . . 
					ELSE IF @LoaderId = 75
				BEGIN
					SELECT TOP 1000 nisi.RowID AS [Row Id],
					REPLACE(nisi.[NISI Cause Number], ' ', '') AS [NISI Cause Number],
					nisi.[Bonding license number],
					nisi.[First Name],
					nisi.[Middle Name],
					nisi.[Last Name],
					nisi.[Address 1],
					nisi.[Address 2], 
					nisi.City,
					nisi.[State],
					nisi.ZIP,
					nisi.Telephone,
					nisi.Blank,
					CAST(nisi.[Bond forfeiture amount] AS MONEY) AS [Bond forfeiture amount],  
					nisi.Race,
					REPlACE(nisi.Height, '"', '') AS Height,
					nisi.Sex,
					nisi.[NISI Status Short Desc],
					nisi.[NISI Status],
					REPLACE(nisi.[underlying ticket number], ' ', '') AS [underlying ticket number],
					REPLACE(nisi.[underlying cause number], ' ', '') AS [underlying cause number], 
					nisi.[bondsman for bonding company],
					nisi.Bondcompany_Or_attorneyName AS [Attorney NAME],
					TrafficTickets.dbo.fn_DateFormat(nisi.[NISI Issue Date],1,'/',':',1) AS [NISI Issue Date],
					TrafficTickets.dbo.fn_DateFormat(nisi.Insert_Date,1,'/',':',1) AS [Insert Date],
					nisi.GroupID
					FROM LoaderFilesArchive.dbo.NISI_DataArchiveTable nisi
					WHERE  
					(
	                       @Mode = 1
	                       AND REPLACE(REPLACE(nisi.[underlying ticket number], ' ', ''), '-', '') LIKE @TicketNo
					)
					OR
					(
	                       @Mode = 2
	                       AND 
	                       ((REPLACE(REPLACE(nisi.[underlying cause number], ' ', ''), '-', '') LIKE @CauseNo) OR (REPLACE(REPLACE(nisi.[NISI Cause Number], ' ', ''), '-', '') LIKE @CauseNo))
					)
					OR
					(
							@Mode = 3
							AND nisi.[First Name] LIKE @FirstName
							AND nisi.[Last Name] LIKE @LastName
							AND nisi.[Middle Name] LIKE @MiddleName
					)
					OR  
					(
							@Mode = 4
							AND ISDATE(nisi.Insert_Date) = 1
							AND DATEDIFF(DAY, nisi.Insert_Date, @CrtDate) = 0
					)
					ORDER BY nisi.RowID		   
				END
				-- Rab Nawaz 10729 03/21/2013 Added NISI Cases Loader. . . 
					ELSE IF @LoaderId = 76
				BEGIN
					SELECT TOP 1000 docketExtract.RowID AS [Row Id],
					REPLACE(docketExtract.[Cause Number], ' ', '') AS [Cause Number],
					REPLACE(docketExtract.[Ticket Number], ' ', '') AS [Ticket Number],
					CONVERT(VARCHAR, docketExtract.[Transection Court Date], 101) AS [Date Transection Occurred at Court],
					docketExtract.[Docket Entry Violation Description] AS [Docket Entry Viol. DESC],
					CAST (docketExtract.[NISI Amount Owed On Particular NISI] AS MONEY) AS [NISI Amount Owed On Particular NISI],
					CONVERT(VARCHAR, docketExtract.[Docket Extract File Date], 101) AS [Docket Extract File Date],
					CONVERT(VARCHAR, docketExtract.Insert_Date, 101) AS [INSERT DATE], 
					docketExtract.GroupID 
					FROM LoaderFilesArchive.dbo.HoustonDocketExtract_DataArchiveTable docketExtract
					WHERE  
					(
	                       @Mode = 1
	                       AND REPLACE(REPLACE(docketExtract.[Ticket Number], ' ', ''), '-', '') LIKE @TicketNo
					)
					OR
					(
	                       @Mode = 2
	                       AND 
	                       (REPLACE(REPLACE(docketExtract.[Cause Number], ' ', ''), '-', '') LIKE @CauseNo)
					)
					OR  
					(
							@Mode = 4
							AND ISDATE(docketExtract.Insert_Date) = 1
							AND DATEDIFF(DAY, docketExtract.Insert_Date, @CrtDate) = 0
					)
					ORDER BY docketExtract.RowID		   
				END
				ELSE IF @LoaderId = 77 -- Farrukh 11309 08/07/2013 Unit Data Texas Dept of Transportation
					BEGIN						
						SELECT TOP 1000
						    RecordId AS [Row Id],
						    Crash_ID,
							Unit_Nbr,
							Unit_Desc_ID,
							Veh_Parked_Fl,
							Veh_HNR_Fl,
							Veh_Lic_State_ID,
							VIN,
							Veh_Mod_Year,
							Veh_Color_ID,
							Veh_Make_ID,
							Veh_Mod_ID,
							Veh_Body_Styl_ID,
							Emer_Respndr_Fl,
							Drvr_Lic_Type_ID,
							Drvr_Lic_State_ID,
							Drvr_Lic_Cls_ID,
							Drvr_Zip,
							Drvr_Injry_Sev_ID,
							Drvr_Age,
							Drvr_Ethnicity_ID,
							Drvr_Gndr_ID,
							Drvr_Ejct_ID,
							Drvr_Rest_ID,
							Drvr_Airbag_ID,
							Drvr_Helmet_ID,
							Drvr_Sol_Fl,
							Drvr_Alc_Spec_Type_ID,
							Drvr_Alc_Rslt_ID,
							Drvr_Bac_Test_Rslt,
							Drvr_Drg_Spec_Type_ID,
							Drvr_Drg_Rslt_ID,
							Drvr_Drg_Cat_1_ID,
							Ownr_Zip,
							Fin_Resp_Proof_ID,
							Fin_Resp_Type_ID,
							Veh_Dmag_Area_1_ID,
							Veh_Dmag_Scl_1_ID,
							Force_Dir_1_ID,
							Veh_Dmag_Area_2_ID,
							Veh_Dmag_Scl_2_ID,
							Force_Dir_2_ID,
							Veh_Inventoried_Fl,
							Veh_Transp_Name,
							Veh_Transp_Dest,
							Drvr_Death_Time,
							Veh_CMV_Fl,
							Cmv_Fiveton_Fl,
							Cmv_Hazmat_Fl,
							Cmv_Nine_Plus_Pass_Fl,
							Cmv_Veh_Oper_ID,
							Cmv_Carrier_ID_Type_ID,
							Cmv_Carrier_Zip,
							Cmv_Road_Acc_ID,
							Cmv_Veh_Type_ID,
							Cmv_GVWR,
							Cmv_RGVW,
							Cmv_Hazmat_Rel_Fl,
							Hazmat_Cls_1_ID,
							Hazmat_IDNbr_1_ID,
							Hazmat_Cls_2_ID,
							Hazmat_IDNbr_2_ID,
							Cmv_Cargo_Body_ID,
							Trlr1_GVWR,
							Trlr1_RGVW,
							Trlr1_Type_ID,
							Trlr2_GVWR,
							Trlr2_RGVW,
							Trlr2_Type_ID,
							Cmv_Evnt1_ID,
							Cmv_Evnt2_ID,
							Cmv_Evnt3_ID,
							Cmv_Evnt4_ID,
							Cmv_Tot_Axle,
							Cmv_Tot_Tire,
							Contrib_Factr_1_ID,
							Contrib_Factr_2_ID,
							Contrib_Factr_3_ID,
							Contrib_Factr_P1_ID,
							Contrib_Factr_P2_ID,
							Veh_Dfct_1_ID,
							Veh_Dfct_2_ID,
							Veh_Dfct_3_ID,
							Veh_Dfct_P1_ID,
							Veh_Dfct_P2_ID,
							Veh_Trvl_Dir_ID,
							First_Harm_Evt_Inv_ID,
							Incap_Injry_Cnt,
							Nonincap_Injry_Cnt,
							Poss_Injry_Cnt,
							Non_Injry_Cnt,
							Unkn_Injry_Cnt,
							Tot_Injry_Cnt,
							Death_Cnt,
							UFS.GroupID,
							InsertionLoaderID,
							UpdationLoaderID,					
							CONVERT(VARCHAR, UFS.[UpdateDate], 101) AS [Update Date],
						    CONVERT(VARCHAR, UFS.[InsertDate], 101) AS [Insert Date],
						    CONVERT(VARCHAR, thlg.Load_Date, 101) AS [File Date]
						FROM Crash.dbo.Unit_File_Specification UFS
						   INNER JOIN LA_Houston.dbo.Tbl_HTS_LA_Group thlg
								ON  thlg.GroupID = UFS.GroupID
					WHERE  (
							   @Mode = 1
							   AND REPLACE(REPLACE([Crash_ID], ' ', ''), '-', '') LIKE @TicketNo
						   )
						   OR  (
								   @Mode = 4
								   AND ISDATE(thlg.Load_Date) = 1
								   AND DATEDIFF(DAY, thlg.Load_Date, @CrtDate) = 0
							   )
					ORDER BY
						   UFS.RecordID
					END
					ELSE IF @LoaderId = 78 -- Farrukh 11309 08/07/2013 Charges Data Texas Dept of Transportation
					BEGIN						
						SELECT TOP 1000
						    RecordId AS [Row Id],
						    Crash_ID,
							Unit_Nbr,
							Prsn_Nbr,
							Charge_Cat_ID,
							Charge,
							Citation_Nbr,
							CFS.GroupID,
							InsertionLoaderID,
							UpdationLoaderID,				
							CONVERT(VARCHAR, CFS.[UpdateDate], 101) AS [Update Date],
						    CONVERT(VARCHAR, CFS.[InsertDate], 101) AS [Insert Date],
						    CONVERT(VARCHAR, thlg.Load_Date, 101) AS [File Date]
						FROM Crash.dbo.Charges_File_Specification CFS
						   INNER JOIN LA_Houston.dbo.Tbl_HTS_LA_Group thlg
								ON  thlg.GroupID = CFS.GroupID
					WHERE  (
							   @Mode = 1
							   AND REPLACE(REPLACE([Crash_ID], ' ', ''), '-', '') LIKE @TicketNo
						   )
						   OR  (
								   @Mode = 4
								   AND ISDATE(thlg.Load_Date) = 1
								   AND DATEDIFF(DAY, thlg.Load_Date, @CrtDate) = 0
							   )
					ORDER BY
						   CFS.RecordID
					END
					ELSE IF @LoaderId = 79 -- Farrukh 11309 08/07/2013 CDL Endorsements Data Texas Dept of Transportation
					BEGIN						
						SELECT TOP 1000
						    RecordId AS [Row Id],
						    Crash_ID,
							Unit_Nbr,
							Drvr_Lic_Endors_ID,
							InsertionLoaderID,
							UpdationLoaderID,
							EFS.GroupID,			
							CONVERT(VARCHAR, EFS.[UpdateDate], 101) AS [Update Date],
						    CONVERT(VARCHAR, EFS.[InsertDate], 101) AS [Insert Date],
						    CONVERT(VARCHAR, thlg.Load_Date, 101) AS [File Date]
						FROM Crash.dbo.CDL_Endorsements_File_Spec EFS
						   INNER JOIN LA_Houston.dbo.Tbl_HTS_LA_Group thlg
								ON  thlg.GroupID = EFS.GroupID
					WHERE  (
							   @Mode = 1
							   AND REPLACE(REPLACE([Crash_ID], ' ', ''), '-', '') LIKE @TicketNo
						   )
						   OR  (
								   @Mode = 4
								   AND ISDATE(thlg.Load_Date) = 1
								   AND DATEDIFF(DAY, thlg.Load_Date, @CrtDate) = 0
							   )
					ORDER BY
						   EFS.RecordID
					END
					ELSE IF @LoaderId = 81 -- Farrukh 11309 08/07/2013 Damages Data Texas Dept of Transportation
					BEGIN						
						SELECT TOP 1000
						    RecordId AS [Row Id],
						    Crash_ID,
							Damaged_Property,
							InsertionLoaderID,
							UpdationLoaderID,
							DFS.GroupID,			
							CONVERT(VARCHAR, DFS.[UpdateDate], 101) AS [Update Date],
						    CONVERT(VARCHAR, DFS.[InsertDate], 101) AS [Insert Date],
						    CONVERT(VARCHAR, thlg.Load_Date, 101) AS [File Date]
						FROM Crash.dbo.Damages_File_Specification DFS
						   INNER JOIN LA_Houston.dbo.Tbl_HTS_LA_Group thlg
								ON  thlg.GroupID = DFS.GroupID
					WHERE  (
							   @Mode = 1
							   AND REPLACE(REPLACE([Crash_ID], ' ', ''), '-', '') LIKE @TicketNo
						   )
						   OR  (
								   @Mode = 4
								   AND ISDATE(thlg.Load_Date) = 1
								   AND DATEDIFF(DAY, thlg.Load_Date, @CrtDate) = 0
							   )
					ORDER BY
						   DFS.RecordID
					END
					ELSE IF @LoaderId = 83 -- Farrukh 11309 08/07/2013 Driver Data Texas Dept of Transportation
					BEGIN						
						SELECT TOP 1000
						    RecordId AS [Row Id],
						    Crash_ID,
							Unit_Nbr,
							Prsn_Nbr,
							Prsn_Type_ID,
							Prsn_Occpnt_Pos_ID,
							Prsn_Injry_Sev_ID,
							Prsn_Age,
							Prsn_Ethnicity_ID,
							Prsn_Gndr_ID,
							Prsn_Ejct_ID,
							Prsn_Rest_ID,
							Prsn_Airbag_ID,
							Prsn_Helmet_ID,
							Prsn_Sol_Fl,
							Prsn_Alc_Spec_Type_ID,
							Prsn_Alc_Rslt_ID,
							Prsn_Bac_Test_Rslt,
							Prsn_Drg_Spec_Type_ID,
							Prsn_Drg_Rslt_ID,
							Drvr_Drg_Cat_1_ID,
							Prsn_Death_Time,
							Incap_Injry_Cnt,
							Nonincap_Injry_Cnt,
							Poss_Injry_Cnt,
							Non_Injry_Cnt,
							Unkn_Injry_Cnt,
							Tot_Injry_Cnt,
							Death_Cnt,
							Drvr_Lic_Type_ID,
							Drvr_Lic_State_ID,
							[Drvr_Lic_Cls_ID	Drvr_Zip],
							Drvr_Zip,
							InsertionLoaderID,
							UpdationLoaderID,
							DRFS.GroupID,		
							CONVERT(VARCHAR, DRFS.[UpdateDate], 101) AS [Update Date],
						    CONVERT(VARCHAR, DRFS.[InsertDate], 101) AS [Insert Date],
						    CONVERT(VARCHAR, thlg.Load_Date, 101) AS [File Date]
						FROM Crash.dbo.Driver_File_Specification DRFS
						   INNER JOIN LA_Houston.dbo.Tbl_HTS_LA_Group thlg
								ON  thlg.GroupID = DRFS.GroupID
					WHERE  (
							   @Mode = 1
							   AND REPLACE(REPLACE([Crash_ID], ' ', ''), '-', '') LIKE @TicketNo
						   )
						   OR  (
								   @Mode = 4
								   AND ISDATE(thlg.Load_Date) = 1
								   AND DATEDIFF(DAY, thlg.Load_Date, @CrtDate) = 0
							   )
					ORDER BY
						   DRFS.RecordID
					END
					--Rab Nawaz Khan 11108 08/06/2013 Added pearland data Archive. . . 
					ELSE IF @LoaderId = 84 
					BEGIN
						SELECT DISTINCT TOP 1000
						Pearland.RowID,
						Pearland.LastName AS [Defendant Last Name],
						Pearland.FirstName AS [Defendant First Name],
						Pearland.MiddleName AS [Defendant Middle Initial],
						pearland.[Address] AS [Defendant Address],
						Pearland.City AS City,
						Pearland.ZipCode AS Zip,
						pearland.ViolationStatus AS [Case Status],
						Pearland.CauseNumber AS [Cause Number],
						Pearland.TicketNumber AS [Ticket No],
						pearland.ViolationDESCRIPTION AS [Violation Description],
						Pearland.ViolationDate AS [Violation Date],
						Pearland.ViolationCharges +  Pearland.CourtCharges AS [Total Fine Amount],
						Pearland.BondAmount AS [Bond Amount],
						traffictickets.dbo.fn_DateFormat(Pearland.CourtDate, 4, '/', ':', 1) AS [Court Date],
						Pearland.CourtTIME AS [Court Time],
						Pearland.OfficerLastNAME AS [Officer Last Name],
						Pearland.OfficerFirstNAME AS [Officer First Name],
						Pearland.AttorneyNAME AS [Attorney Name], 
						Pearland.AddressSTATUS AS [Address Status],
						Pearland.Reliable, 
						Pearland.GroupID, 
						CONVERT(VARCHAR, LAGroup.Load_Date, 101) AS [File Date]
						--traffictickets.dbo.fn_DateFormat(Pearland.InsertDate, 4, '/', ':', 1) AS [Insert Date]
						
						FROM   LoaderFilesArchive.dbo.tblPearland_ArrignmentOne_DataArchive Pearland
							   INNER JOIN LA_Houston.dbo.Tbl_HTS_LA_Group AS LAGroup
									ON  Pearland.GroupID = LAGroup.GroupID
						WHERE  
							(@Mode = 1) AND (REPLACE(REPLACE(Pearland.TicketNumber, ' ', ''), '-', '') LIKE @TicketNo)
							   OR  (@Mode = 2) AND (REPLACE(REPLACE(Pearland.CauseNumber, ' ', ''), '-', '') LIKE @CauseNo)
							   OR  (
									   (@Mode = 3)
									   AND (
											   (
												   RTRIM(LTRIM(@LastName)) = '%'
												   AND (RTRIM(LTRIM(Pearland.FirstName)) LIKE @FirstName)
											   )
											   OR (
													  RTRIM(LTRIM(@FirstName)) = '%'
													  AND RTRIM(LTRIM(Pearland.LastName)) LIKE @LastName
												  )
											   OR (
													  RTRIM(LTRIM(Pearland.FirstName)) LIKE @FirstName
													  AND RTRIM(LTRIM(Pearland.LastName)) LIKE @LastName
												  )
										   )
								   )
							   OR  (@Mode = 4)
							   AND (ISDATE(LAGroup.Load_Date) = 1)
							   AND (
									   DATEDIFF(DAY, RTRIM(LTRIM(LAGroup.Load_Date)), @CrtDate) = 0
								   )
							   OR  (@Mode = 5)
							   AND (ISDATE(Pearland.CourtDate) = 1)
							   AND (DATEDIFF(DAY, Pearland.CourtDate, @CrtDate) = 0)
						ORDER BY Pearland.RowID
					END
					ELSE IF @LoaderId = 86 --Zeeshan Haider 11483 11/07/2013 Added DCCC Booking List New
					BEGIN
						SELECT DISTINCT TOP 1000
							   dbn.[Last Name],
							   dbn.[First Name],
							   dbn.Gender,
							   dbn.[Address Part I],
							   dbn.City,
							   dbn.[State],
							   dbn.[Zip Code],
							   dbn.[Violation Description],
							   CONVERT(VARCHAR, dbn.Recdate, 101) AS [File Date],
							   dbn.GroupID
						FROM LoaderFilesArchive.dbo.tbl_DallasCrminal_BookedIn_New dbn
						WHERE (
								(@Mode = 3)
									   AND (
											   (
												   RTRIM(LTRIM(@LastName)) = '%'
												   AND (RTRIM(LTRIM(dbn.[First Name])) LIKE @FirstName)
											   )
											   OR (
													  RTRIM(LTRIM(@FirstName)) = '%'
													  AND RTRIM(LTRIM(dbn.[Last Name])) LIKE @LastName
												  )
											   OR (
													  RTRIM(LTRIM(dbn.[First Name])) LIKE @FirstName
													  AND RTRIM(LTRIM(dbn.[Last Name])) LIKE @LastName
												  )
										   )
						) OR  (
							(@Mode = 4)
							AND(
								ISDATE(dbn.Recdate) = 1
								AND DATEDIFF(DAY, dbn.Recdate, @CrtDate) = 0
							)
						)
					END
					
					ELSE IF @LoaderId = 88 -- Rab Nawaz Khan 11530 12/20/2013 Added HCCC Criminal Filings With Future Settings Loader. . . 
					BEGIN						
						SELECT TOP 1000
						   ID AS [Row Id],
						   CDI,
						   [Ticket Number],
						   [Violation Description],
						   [Court Number],
						   [Last Name],
						   [First Name],
						   [SPN Number],
						   [Race],
						   [Sex],
						   [DOB],
						   CST,
						   DST,
						   [Bond Amount],
						   [Address Part I] AS [Address 1],
						   City,
						   [State],
						   [ZipCode] AS Zip,
						   [Attorney Last Name],
						   [Attorney Middle Name],
						   [Attorney First Name],
						   [Attorney SPN Number],
						   ins,
						   cad,
						   ChargeLevel,
						   cnc,
						   rea,
						   rundate,
						   fda,
						   curr_off,
						   nda,
						   aty_coc,
						   aty_coc_lit,
						   HCCCD.GroupID,
						   CONVERT(VARCHAR, HCCCD.[Insertion Date], 101) AS [Insert Date],
						   CONVERT(VARCHAR, thlg.Load_Date, 101) AS [File Date]
					FROM   LoaderFilesArchive.dbo.HCCC_Criminal_Filings_FutureSettings_DataArchive HCCCD
						   INNER JOIN LA_Houston.dbo.Tbl_HTS_LA_Group thlg
								ON  thlg.GroupID = HCCCD.GroupID
					WHERE  (
							   @Mode = 1
							   AND REPLACE(REPLACE([Ticket Number], ' ', ''), '-', '') LIKE @TicketNo
						   )
						   OR  (
								   (@Mode = 3)
								   AND (
										   (
											   RTRIM(LTRIM(@LastName)) = '%'
											   AND (RTRIM(LTRIM(HCCCD.[First Name])) LIKE @FirstName)
										   )
										   OR (
												  RTRIM(LTRIM(@FirstName)) = '%'
												  AND RTRIM(LTRIM(HCCCD.[Last Name])) LIKE @LastName
											  )
										   OR (
												  RTRIM(LTRIM(HCCCD.[First Name])) LIKE @FirstName
												  AND RTRIM(LTRIM(HCCCD.[Last Name])) LIKE @LastName
											  )
									   )
							   )
						   OR  (
								   @Mode = 4
								   AND ISDATE(thlg.Load_Date) = 1
								   AND DATEDIFF(DAY, thlg.Load_Date, @CrtDate) = 0
							   )
					ORDER BY
						   HCCCD.ID
					END
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_HTS_Get_MID_PhoneNumbers]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_HTS_Get_MID_PhoneNumbers]
GO


CREATE   procedure [dbo].[usp_HTS_Get_MID_PhoneNumbers]    
 (    
 @TicketIDs varchar(400),    
 @RecordIDs varchar(400)    
 )    
as    
    
declare @strSQL varchar(8000)    
if right(@ticketids,1) = ','     
 begin     
  set @ticketids = left(@ticketids,len(@ticketids)-1)    
 end    
    
if right(@RecordIDs,1) = ','     
 begin     
  set @RecordIDs = left(@RecordIDs,len(@RecordIDs)-1)    
 end    
    
    
select @strSQL='    
declare @phoneno varchar(500)    
    
declare @temp table    
 (    
 phno varchar(50),    
 type  varchar(20) ,  
ticketid int  
 )    
    
declare @tmpphone table       
 (       
 PhNumber varchar(50) ,    
 type varchar(20) ,  
ticketid int  
 )   '    
    
if len(@ticketids) > 0    
begin    
    
 select @strSQL = @strSQL + '    
 insert into @temp     
 select  t.contact1,     
  ''(''+c1.description+'')'' ,ticketid_pk   
 FROM tblTickets t     
 left OUTER JOIN    
  tblContactstype c1      
 ON   t.ContactType1 =c1.ContactType_PK     
 where ticketid_pk in (' + @ticketids + ')    
     
 insert into @temp     
 select  t.contact2,     
  ''(''+c2.description+'')''  ,ticketid_pk  
 FROM tblTickets t     
 left OUTER JOIN    
  tblContactstype c2    
 ON   t.ContactType2 =c2.ContactType_PK     
 where ticketid_pk in (' + @ticketids + ')    
     
 insert into @temp     
 select  t.contact3,     
  ''(''+c3.description+'')''  ,ticketid_pk  
 FROM tblTickets t     
 left OUTER JOIN    
  tblContactstype c3    
 ON   t.ContactType3 =c3.ContactType_PK     
 where ticketid_pk in (' + @ticketids + ')'    
  
end    
    
--select * from @temp    
    
if len( @RecordIDs ) > 0    
begin    
 select @strSQL = @strSQL + '    
 insert into @tmpphone    
 select distinct ltrim(rtrim(phonenumber)) as PhNo,    
  ''(Other)''  ,0  
 from tblticketsarchive     
 where recordid in (' + @recordids + ')    
 and  phonenumber not like ''%0000000%''      
 and  phonenumber not like ''00000%''      
 and  phonenumber not like ''01000%''  
 and  phonenumber not like ''%CHOOSE%''         
 and  phonenumber is not null      
 and  phonenumber not in (    
   select distinct phonenumber from tblrestrictedphonenumbers    
   )    
 union '    
end    
    
 select @strSQL = @strSQL + '    
 select  distinct ltrim(rtrim(phno)) as PhNo,    
  type ,  ticketid  
 from @temp    
 where phno not like ''00000%''    
 and  phno not like ''%0000000%''    
 and  phno not like ''01000%''    
 and  phno not like ''%CHOOSE%''
 and  phno not in (    
   select distinct phonenumber from tblrestrictedphonenumbers    
   )    
    
--select * from @tmpphone    
  
  
delete from @temp   
  
insert into @temp  
select PhNumber, type, ticketid from @tmpphone  
order by ticketid desc  
  
    
declare @tmpphone2 table       
 (       
 PhNumber varchar(50) ,    
 type varchar(20)      
 )       
    
    
insert into @tmpphone2    
select phno, min(type)  
from @temp    
group by phno    
  
--select * from @tmpphone2    
    
select left(m.phnumber,3)+''-''+substring(m.phnumber,4,3)+''-''+substring(m.phnumber,7,len(m.phnumber)) + type    
from @tmpphone2 m'      
    
    
exec(@strSQL)    

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


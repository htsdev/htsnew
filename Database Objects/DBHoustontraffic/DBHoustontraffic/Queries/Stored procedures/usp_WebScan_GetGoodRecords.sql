SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_WebScan_GetGoodRecords]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_WebScan_GetGoodRecords]
GO


CREATE procedure [dbo].[usp_WebScan_GetGoodRecords]   
@BatchID as int     
as    
    
select     
 o.picid,    
 o.causeno,    
 o.status,
 o.courtno,    
 convert(varchar(12),o.newcourtdate,101) as courtdate,    
 convert(varchar(12),o.todaydate,101) as todaydate,    
 o.location,    
 o.time    
from tbl_WebScan_Batch b    
inner join tbl_WebScan_Pic p on    
b.batchid=p.batchid inner join tbl_WebScan_OCR o on     
p.id=o.picid    
where     
  o.picid not in (Select picid from tbl_WebScan_Log)    
 and b.batchid=@BatchID  
order by o.picid 


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


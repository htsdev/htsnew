/************************************************************************************************
--Created By   : Muhammad Nadir Siddiqui
--Creation Data: 12/23/2010
--Task ID :8699
--Parameter List	
--@showAllRecord : If true than show all record. 
--@FromDate : From Date
--@ToDate : To Date
--Return Column:
--Serial Number, Name,Company,EmailAddress,OfficePhone,CellNumber,Address,CaseComments,PreferredContact
--Business Logic : Display depuy referral attorney leads information on the basis of date range.
 ************************************************************************************************/

CREATE PROCEDURE dbo.USP_HTP_GetDePuyReferralAttorneyLeads
	@fromDate DATETIME,
	@toDate DATETIME,
	@ShowAllRecords BIT = 0
AS
	SELECT ROW_NUMBER() OVER(ORDER BY ra.CreatedDate DESC) AS 'SNo',
	       ra.CreatedDate AS 'Date',
	       ra.FirstName +' '+ ra.LastName AS 'Name',
	       ra.CompanyName AS 'Company',
	       ra.EmailAddress AS 'EmailAddress',
	       dbo.fn_FormatContactNumber_new(ra.OfficePhoneNumber, -1)  AS 'OfficePhone',
	       dbo.fn_FormatContactNumber_new(ra.CellNumber, -1)  AS 'CellNumber',
	       ra.StreetAddress +' '+ra.City +' '+ ra.[State] +' '+ ra.Zip +' '+ ISNULL(ra.Country,'') AS 'Address',
	       ra.[Description] AS 'CaseComments',
	       ra.ContactType AS 'PreferredContact'
	     
	FROM   SulloLaw.dbo.ReferringAttorney ra 
	WHERE  @ShowAllRecords = 1
	       OR  (
	               @ShowAllRecords = 0
	               AND DATEDIFF(DAY, @FromDate, ra.CreatedDate) >= 0
	               AND DATEDIFF(DAY, @toDate, ra.CreatedDate) <= 0
	           )
GO

GRANT EXECUTE ON dbo.USP_HTP_GetDePuyReferralAttorneyLeads TO dbr_webuser
GO
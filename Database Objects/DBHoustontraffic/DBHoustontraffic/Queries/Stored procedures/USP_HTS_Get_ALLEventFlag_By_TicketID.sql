USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTS_Get_ALLEventFlag_By_TicketID]    Script Date: 01/02/2014 03:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
ALTER procedure [dbo].[USP_HTS_Get_ALLEventFlag_By_TicketID]      
    
@TicketID as int      
      
as      
      
select * from tblEventflags        
where flagid_pk NOT IN (1,2,5,16,19,21,30,40,41)
AND flagid_pk        
not in       
(select flagid from  tblticketsflag where ticketid_pk =@TicketID and flagid != 9 and flagid !=23)      
and ActiveStatus = 1  
order by Description
  
  
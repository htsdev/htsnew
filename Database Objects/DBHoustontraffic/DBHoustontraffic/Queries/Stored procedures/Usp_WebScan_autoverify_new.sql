﻿/************************************************************
Created by:		Syed Muhammad Ozair
Business Logic:	this procedure is used to verify (updates setting info from Resets updated from BSDA, BSDAII)
				 records in a batch and then returns a result set for records which are updated through this 
				process.
				Also IsCourtSettingInfoChangedDuringBSDA is reset for provided cases and then 
				 it is set in case of any change in court date and court number	from previous record

List of Parameters:				
	@batchid:
	@empid:
	
List of Columns:
	causeno:
	picid:
	batchid:
	empid:
	
 ************************************************************/

ALTER PROCEDURE [dbo].[usp_webscan_autoverify_new]
	@batchid INT,
	@empid INT
AS
	DECLARE @loopcounter AS INT                            
	
	CREATE TABLE #temp
	(
		COUNTER        INT IDENTITY(1, 1) NOT NULL,
		PicID          INT,
		--ozair 4330 07/02/2008 causeno length increased to 200 from 50                                 
		CauseNo        VARCHAR(200),
		CourtDate      DATETIME,
		CourtTime      VARCHAR(15),
		CourtNo        INT,
		STATUS         VARCHAR(50),
		Location       VARCHAR(50),
		CourtDateScan  DATETIME,
		EmployeeID     INT
	)                            
	
	INSERT INTO #temp
	SELECT o.PicID,
	       o.CauseNo,
	       o.newcourtdate AS courtdate,
	       o.Time AS courttime,
	       o.CourtNo,
	       o.Status,
	       o.Location,
	       o.newcourtdate + ' ' + o.Time AS courtdatescan,
	       @empid AS employeeid
	FROM   tbl_webscan_ocr o
	       INNER JOIN tbl_webscan_pic p
	            ON  o.picid = p.id
	       INNER JOIN tbl_webscan_batch b
	            ON  b.batchid = p.batchid
	WHERE  o.checkstatus = 3
	       AND b.batchid = @batchid 
	
	---
	DECLARE @tempC TABLE (causeno VARCHAR(200), picid INT, batchid INT, empid INT)
	---   
	
	SET @loopcounter = (
	        SELECT COUNT(*)
	        FROM   #temp
	    )                            
	
	WHILE @loopcounter >= 1
	BEGIN
	    DECLARE @PicID INT                            
	    DECLARE @CauseNo VARCHAR(200)                            
	    DECLARE @CourtDate AS DATETIME                            
	    DECLARE @CourtTime AS VARCHAR(15)                            
	    DECLARE @CourtNo AS INT                              
	    DECLARE @Status AS VARCHAR(50)                            
	    DECLARE @Location AS VARCHAR(50)                            
	    DECLARE @CourtDateScan AS DATETIME                            
	    DECLARE @EmployeeID AS INT                            
	    DECLARE @Msg INT 
	    ---                           
	    DECLARE @causenos VARCHAR(200) 
	    ---                          
	    
	    SELECT @PicID = PicID,
	           @CauseNo = CauseNo,
	           @CourtDate = courtdate,
	           @CourtTime = courttime,
	           @CourtNo = CourtNo,
	           @Status = STATUS,
	           @Location = Location,
	           @courtdatescan = courtdatescan,
	           @employeeid = employeeid
	    FROM   #temp
	    WHERE  COUNTER = @loopcounter 
	    
	    --ozair 4330 07/2/2008 modified to implement more then one cause no in one reset
	    SET @Msg = 0 
	    DECLARE @temp TABLE
	            (id INT IDENTITY(1, 1) NOT NULL, causeno VARCHAR(50) NULL)
	    
	    INSERT INTO @temp
	      (
	        causeno
	      )(
	           SELECT sValue
	           FROM   dbo.splitstring(@CauseNo, ',')
	       )
	    
	    DECLARE @count INT
	    DECLARE @item INT 
	    
	    SET @item = (
	            SELECT MIN(id)
	            FROM   @temp
	        )
	    
	    SELECT @count = COUNT(*)
	    FROM   @temp
	    
	    --Ozair 5579 04/14/2009 ReSetting IsCourtSettingInfoChangedDuringBSDA flag
	    DECLARE @count_reset INT
	    DECLARE @item_reset INT
	    SET @item_reset = 1
	    SELECT @count_reset = COUNT(*)
	    FROM   @temp
	    
	    WHILE @count_reset >= @item_reset
	    BEGIN
	        UPDATE tblTickets
	        SET    IsCourtSettingInfoChangedDuringBSDA = 0
	        WHERE  ticketid_pk IN (SELECT DISTINCT t.ticketid_pk
	                               FROM   tbltickets t
	                                      INNER JOIN tblticketsviolations 
	                                           tv
	                                           ON  t.ticketid_pk = tv.ticketid_pk
	                               WHERE  t.activeflag = 1
	                                      AND tv.casenumassignedbycourt = (
	                                              SELECT causeno
	                                              FROM   @temp
	                                              WHERE  id = @item_reset
	                                          ))
	        
	        SET @item_reset = @item_reset + 1
	    END
	    --end Ozair 5579
	    
	    WHILE @count >= 1
	    BEGIN
	        DECLARE @TempCauseNo AS VARCHAR(50)      
	        DECLARE @TempCauseNoCount INT       
	        DECLARE @ProblemMsg VARCHAR(100) 
	        
	        --Ozair 5799 04/14/2009 Getting Old Settings Info before update
	        DECLARE @OldVerifiedStatus INT
	        DECLARE @OldVerifiedCourtDate DATETIME
	        DECLARE @OldVerifiedCourtNumber VARCHAR(3)
	        
	        IF @Location = 'HMC'
	        BEGIN
	            SELECT @TempCauseNo = tv.casenumassignedbycourt,
	                   --Ozair 5799 04/14/2009 Getting Old Settings Info before update
	                   @OldVerifiedStatus = tv.CourtViolationStatusIDmain,
	                   @OldVerifiedCourtDate = tv.CourtDateMain,
	                   @OldVerifiedCourtNumber = LTRIM(RTRIM(ISNULL(tv.CourtNumbermain, '')))
	            FROM   tblticketsviolations tv
	                   INNER JOIN tbltickets t
	                        ON  t.ticketid_pk = tv.ticketid_pk
	            WHERE  tv.casenumassignedbycourt = (
	                       SELECT causeno
	                       FROM   @temp
	                       WHERE  id = @item
	                   )
	                   AND t.activeflag = 1
	                   -- Abbas Qamar 10088 05-11-2012 Adding HMC-W court ID
	                   AND tv.courtid IN (3001, 3002, 3003,3075)      
	            
	            SELECT @TempCauseNoCount = COUNT(tv.casenumassignedbycourt)
	            FROM   tblticketsviolations tv
	                   INNER JOIN tbltickets t
	                        ON  t.ticketid_pk = tv.ticketid_pk
	            WHERE  tv.casenumassignedbycourt = (
	                       SELECT causeno
	                       FROM   @temp
	                       WHERE  id = @item
	                   )
	                    -- Abbas Qamar 10088 05-11-2012 Adding HMC-W court ID
	                   AND tv.courtid IN (3001, 3002, 3003,3075)
	                   AND t.activeflag = 1
	        END
	        ELSE 
	        IF @Location <> 'HMC'
	        BEGIN
	            SELECT @TempCauseNo = tv.casenumassignedbycourt,
	                   --Ozair 5799 04/14/2009 Getting Old Settings Info before update
	                   @OldVerifiedStatus = tv.CourtViolationStatusIDmain,
	                   @OldVerifiedCourtDate = tv.CourtDateMain,
	                   @OldVerifiedCourtNumber = LTRIM(RTRIM(ISNULL(tv.CourtNumbermain, '')))
	            FROM   tblticketsviolations tv
	                   INNER JOIN tbltickets t
	                        ON  t.ticketid_pk = tv.ticketid_pk
	            WHERE  tv.casenumassignedbycourt = (
	                       SELECT causeno
	                       FROM   @temp
	                       WHERE  id = @item
	                   )
	                   AND t.activeflag = 1
	                   -- Abbas Qamar 10088 05-11-2012 Adding HMC-W court ID
	                   AND tv.courtid NOT IN (3001, 3002, 3003,3075)      
	            
	            SELECT @TempCauseNoCount = COUNT(tv.casenumassignedbycourt)
	            FROM   tblticketsviolations tv
	                   INNER JOIN tbltickets t
	                        ON  t.ticketid_pk = tv.ticketid_pk
	            WHERE  tv.casenumassignedbycourt = (
	                       SELECT causeno
	                       FROM   @temp
	                       WHERE  id = @item
	                   )
	                   -- Abbas Qamar 10088 05-11-2012 Adding HMC-W court ID
	                   AND tv.courtid NOT IN (3001, 3002, 3003,3075)
	                   AND t.activeflag = 1
	        END        
	        
	        IF @TempCauseNoCount > 1
	        BEGIN
	            SET @ProblemMsg = 'Duplicate Cause No'      
	            SET @Msg = 1
	        END
	        ELSE 
	        IF @TempCauseNoCount = 0
	        BEGIN
	            SET @ProblemMsg = 'Not in System'      
	            SET @Msg = 1
	        END
	        
	        IF @TempCauseNo <> 'NULL'
	           AND @TempCauseNoCount = 1
	        BEGIN
	            DECLARE @TempCID AS VARCHAR(3)                                          
	            DECLARE @CourtNumScan AS VARCHAR(50)                                          
	            
	            SET @TempCID = @CourtNo                                          
	            
	            IF @TempCID = '13'
	               OR @TempCID = '14'
	            BEGIN
	                SET @CourtNumScan = '3002'
	            END
	            ELSE 
	            IF @TempCID = '18'
	            BEGIN
	                SET @CourtNumScan = '3003'
	            END
	             -- Abbas Qamar 10088 05-12-2012 Adding HMC-W court room no 
	            ELSE 
	            IF @TempCID = '20'
	            BEGIN
	                SET @CourtNumScan = '3075'
	            END
	            ELSE
	            BEGIN
	                SET @CourtNumScan = '3001'
	            END                                        
	            
	            DECLARE @tempCVID INT 
	            
	            --Select @tempCVID=courtviolationstatusid from tblcourtviolationstatus where description=@Status                                          
	            IF @Status = 'JURY TRIAL'
	                SET @tempCVID = 26
	            ELSE 
	            IF @Status = 'JUDGE'
	                SET @tempCVID = 103
	            ELSE 
	            IF @Status = 'PRETRIAL'
	                SET @tempCVID = 101
	            ELSE 
	            IF @Status = 'ARRAIGNMENT'
	                SET @tempCVID = 3
	            ELSE 
	            IF @Status = 'WAITING'
	                SET @tempCVID = 104      
	            
	            DECLARE @TodayDate AS DATETIME                                          
	            SET @TodayDate = GETDATE() 
	            
	            --ozair 5799 04/14/2009	setting IsCourtSettingInfoChanged field	            
	            IF (
	                   @OldVerifiedStatus <> @tempCVID
	                   OR @OldVerifiedCourtDate <> @CourtDateScan
	                   OR @OldVerifiedCourtNumber <> CONVERT(VARCHAR(3), @CourtNo)
	               )
	            BEGIN
	                UPDATE tblTickets
	                SET    IsCourtSettingInfoChangedDuringBSDA = 1
	                WHERE  ticketid_pk IN (SELECT DISTINCT t.ticketid_pk
	                                       FROM   tbltickets t
	                                              INNER JOIN 
	                                                   tblticketsviolations 
	                                                   tv
	                                                   ON  t.ticketid_pk = tv.ticketid_pk
	                                       WHERE  t.activeflag = 1
	                                              AND tv.casenumassignedbycourt = (
	                                                      SELECT causeno
	                                                      FROM   @temp
	                                                      WHERE  id = @item
	                                                  ))
	            END
	            
	            IF @Location = 'HMC'
	            BEGIN
	                UPDATE tblticketsviolations
	                SET    CourtDateMain = @CourtDateScan,
	                       CourtNumberMain = @CourtNo,
	                       CourtViolationStatusIDmain = @tempCVID,
	                       VerifiedStatusUpdateDate = @TodayDate,
	                       CourtID = @CourtNumScan,
	                       CourtDate = @CourtDateScan,
	                       CourtNumber = @CourtNo,
	                       CourtViolationStatusID = @tempCVID,
	                       CourtDateScan = @CourtDateScan,
	                       CourtViolationStatusIDScan = @tempCVID,
	                       CourtNumberScan = @CourtNo,
	                       ScanUpdatedate = @TodayDate,
	                       CourtIDScan = @CourtNumScan,
	                       Updateddate = @TodayDate,
	                       vemployeeid = @EmployeeID,
	                       BSDAUpdateDate = GETDATE()
	                WHERE  casenumassignedbycourt = (
	                           SELECT causeno
	                           FROM   @temp
	                           WHERE  id = @item
	                       )
	                       -- Abbas Qamar 10088 05-12-2012 Adding HMC-W court ID
	                       AND CourtID IN (3001, 3002, 3003,3075)
	                       AND ticketid_pk IN (SELECT DISTINCT t.ticketid_pk
	                                           FROM   tbltickets t
	                                                  INNER JOIN 
	                                                       tblticketsviolations 
	                                                       tv
	                                                       ON  t.ticketid_pk = 
	                                                           tv.ticketid_pk
	                                           WHERE  t.activeflag = 1
	                                                  AND tv.casenumassignedbycourt = (
	                                                          SELECT causeno
	                                                          FROM   @temp
	                                                          WHERE  id = @item
	                                                      ))
	            END
	            ELSE 
	            IF @Location <> 'HMC'
	            BEGIN
	                IF @tempCVID <> 104
	                BEGIN
	                    UPDATE tblticketsviolations
	                    SET    CourtDateMain = @CourtDateScan,
	                           CourtViolationStatusIDmain = @tempCVID,
	                           VerifiedStatusUpdateDate = @TodayDate,
	                           CourtDate = @CourtDateScan,
	                           CourtViolationStatusID = @tempCVID,
	                           CourtDateScan = @CourtDateScan,
	                           CourtViolationStatusIDScan = @tempCVID,
	                           ScanUpdatedate = @TodayDate,
	                           Updateddate = @TodayDate,
	                           vemployeeid = @EmployeeID,
	                           BSDAUpdateDate = GETDATE()
	                    WHERE  casenumassignedbycourt = (
	                               SELECT causeno
	                               FROM   @temp
	                               WHERE  id = @item
	                           )
	                           -- Abbas Qamar 10088 05-12-2012 Adding HMC-W court room no 
	                           AND CourtID NOT IN (3001, 3002, 3003,3075)
	                           AND ticketid_pk IN (SELECT DISTINCT t.ticketid_pk
	                                               FROM   tbltickets t
	                                                      INNER JOIN 
	                                                           tblticketsviolations 
	                                                           tv
	                                                           ON  t.ticketid_pk = 
	                                                               tv.ticketid_pk
	                                               WHERE  t.activeflag = 1
	                                                      AND tv.casenumassignedbycourt = (
	                                                              SELECT causeno
	                                                              FROM   @temp
	                                                              WHERE  id = @item
	                                                          ))
	                END
	                ELSE
	                BEGIN
	                    UPDATE tblticketsviolations
	                    SET    CourtViolationStatusIDmain = @tempCVID,
	                           CourtViolationStatusID = @tempCVID,
	                           CourtViolationStatusIDScan = @tempCVID,
	                           ScanUpdatedate = @TodayDate,
	                           Updateddate = @TodayDate,
	                           vemployeeid = @EmployeeID,
	                           BSDAUpdateDate = GETDATE()
	                    WHERE  casenumassignedbycourt = (
	                               SELECT causeno
	                               FROM   @temp
	                               WHERE  id = @item
	                           )
	                            -- Abbas Qamar 10088 05-12-2012 Adding HMC-W court room no 
	                           AND CourtID NOT IN (3001, 3002, 3003,3075)
	                           AND ticketid_pk IN (SELECT DISTINCT t.ticketid_pk
	                                               FROM   tbltickets t
	                                                      INNER JOIN 
	                                                           tblticketsviolations 
	                                                           tv
	                                                           ON  t.ticketid_pk = 
	                                                               tv.ticketid_pk
	                                               WHERE  t.activeflag = 1
	                                                      AND tv.casenumassignedbycourt = (
	                                                              SELECT causeno
	                                                              FROM   @temp
	                                                              WHERE  id = @item
	                                                          ))
	                END
	            END 
	            
	            --                          
	            INSERT INTO @tempC
	            SELECT @CauseNo,
	                   @PicID,
	                   @batchid,
	                   @empid 
	            --select * from @tempC
	            --
	            IF @Msg = 0
	            BEGIN
	                UPDATE tbl_webscan_ocr
	                SET    causeno = @CauseNo,
	                       STATUS = @Status,
	                       NewCourtDate = CONVERT(VARCHAR(12), @CourtDate, 101),
	                       CourtNo = @CourtNo,
	                       Location = @Location,
	                       TIME = @CourtTime,
	                       checkstatus = 4,
	                       ProblemMsg = ''
	                WHERE  picid = @PicID                                  
	                
	                UPDATE tbl_webscan_data
	                SET    CauseNo = @CauseNo,
	                       CourtDate = @CourtDate,
	                       CourtTime = @CourtTime,
	                       CourtRoomNo = @CourtNo,
	                       STATUS = @Status,
	                       CourtLocation = @Location
	                WHERE  PicID = @PicID                                   
	                
	                SET @Msg = 2
	            END
	        END                              
	        
	        IF @Msg = 1
	        BEGIN
	            UPDATE tbl_webscan_ocr
	            SET    causeno = @CauseNo,
	                   STATUS = @Status,
	                   NewCourtDate = CONVERT(VARCHAR(12), @CourtDate, 101),
	                   CourtNo = @CourtNo,
	                   Location = @Location,
	                   TIME = @CourtTime,
	                   checkstatus = 5,
	                   ProblemMsg = @ProblemMsg
	            WHERE  picid = @PicID                                  
	            
	            UPDATE tbl_webscan_data
	            SET    CauseNo = @CauseNo,
	                   CourtDate = @CourtDate,
	                   CourtTime = @CourtTime,
	                   CourtRoomNo = @CourtNo,
	                   STATUS = @Status,
	                   CourtLocation = @Location
	            WHERE  PicID = @PicID
	        END
	        ELSE
	        BEGIN
	            SET @Msg = 0
	        END
	        
	        SET @item = @item + 1
	        SET @count = @count -1
	    END	
	    DELETE @temp	                                      
	    SET @loopcounter = @loopcounter -1
	END                            
	
	IF @Location = 'HMC'
	    SELECT causeno,
	           picid,
	           batchid,
	           empid
	    FROM   @tempC
	ELSE
	    SELECT DISTINCT causeno,
	           picid,
	           batchid,
	           empid,
	           'reset'
	    FROM   @tempC      

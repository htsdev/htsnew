/****** 
Create by:  Syed Muhammad Ozair

Business Logic : this procedure is used to get the Scan batch detail.

List of Parameters:	
	@ScanBatchID : scan batch id for which records will be fetched

List of Columns: 	
	BatchID : Batch id  
	LetterID : Document batch id
	DocumentType : type of document/report
	ScanDate : when it is scanned in to system
	PageCount : total no of pages of the document
	Missing : missing pages if any
	Verified : is it verified or not

******/

ALTER Procedure dbo.usp_HTP_Get_DocumentTracking_BatchDetail

@ScanBatchID int

as

select sbd.scanbatchid_fk as BatchID,
	sbd.documentbatchid as LetterID,
	d.documentname as DocumentType,
	sb.scanbatchdate as ScanDate,
	sbd.documentbatchpagecount as PageCount,
	sbd.documentbatchpagecount-count(sbd.scanbatchdetailid) as Missing,
	case when sbd.documentbatchpagecount - isnull((select count(scanbatchdetailid) from tbl_htp_documenttracking_scanbatch_detail where Isverified=1 and scanbatchid_fk=sbd.scanbatchid_fk and documentbatchid=sbd.documentbatchid),0) <= 0 then 'Yes' else 'No' end as Verified --Sabir Khan 11/09/2009 Allow verify document when more pages scaned then required....
from	tbl_htp_documenttracking_scanbatch_detail sbd inner join tbl_htp_documenttracking_scanbatch sb
on	sbd.scanbatchid_fk=sb.scanbatchid inner join tbl_HTP_DocumentTracking_Batch b
on	sbd.documentbatchid=b.batchid inner join tbl_HTP_DocumentTracking_Documents d
on	b.documentid_fk=d.documentid
where	sb.scanbatchid=@ScanBatchID
group by sbd.scanbatchid_fk, sbd.documentbatchid, d.documentname, sb.scanbatchdate, sbd.documentbatchpagecount






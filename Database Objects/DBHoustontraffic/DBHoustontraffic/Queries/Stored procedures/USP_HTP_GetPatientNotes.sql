﻿ /************************************************************
 * Created By : Noufil Khan 
 * Created Date: 11/1/2010 3:51:27 PM
 * Business Logic :
 * Parameters :
 * Column Return :
 ************************************************************/

CREATE PROCEDURE [dbo].[USP_HTP_GetPatientNotes]
	@patientId INT
AS
	SELECT ISNULL(p.Notes, '') AS Notes
	FROM   Patient p
	WHERE  p.PatientId = @patientId
GO

GRANT EXECUTE ON [dbo].[USP_HTP_GetPatientNotes] TO dbr_webuser
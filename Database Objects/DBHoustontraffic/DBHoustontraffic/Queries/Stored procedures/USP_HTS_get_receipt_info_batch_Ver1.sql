SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_get_receipt_info_batch_Ver1]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_get_receipt_info_batch_Ver1]
GO


CREATE procedure [dbo].[USP_HTS_get_receipt_info_batch_Ver1] --13128,3992                    
@ticketidlist varchar(8000),                    
@lettertype int,      
@empid int = 3992                    
      
as            
      
declare @paymentamount money                    
declare @casenumbers varchar(500),                    
@paymentinfo varchar(500),                    
@contactinfo varchar(500),            
@batchid     int, --for sorting                
@ticketid int      
      
 declare @tickets table (                        
 ticketid int                            
 )                      
          
--Displaying records sorted when printed          
declare @ticketsort table          
(          
 ticketsort int,          
 batchid int           
)      
      
      
insert into @tickets                     
 select * from dbo.Sap_String_Param(@ticketidlist)           
 insert into @ticketsort           
   select  t.ticketid,hts.batchid_pk from @tickets t inner join tblhtsbatchprintletter hts          
  on t.ticketid=hts.ticketid_fk and hts.batchid_pk=(  select top 1 tbv.batchID_pk from tblhtsbatchprintletter tbv                                             
     where  tbv.ticketid_fk=hts.ticketid_fk and tbv.LetterID_Fk = @letterType and deleteflag<>1   )          
      
      
declare @receiptTable table      
(      
ticketid_pk int,       
firstname        varchar(20),          
MiddleName  varchar(6),      
lastname             varchar(20),          
Address1             varchar(50),          
Address2             varchar(20),          
ticketnumber_pk                varchar(500),                                                                                                                                                                                                                   
  
   
languagespeak                  varchar(30),          
batchid       int,       
paymentamount         int,      
city                      varchar(50),                                   
state                                     varchar(50),                   
zip                  varchar(20),          
Address                              varchar(140),      
currentdateset                  datetime,      
totalfeecharged       int,      
dueamount             int,      
casenumbers              varchar(500),                    
settintype varchar(50),      
paymentinfo varchar(500),          
contactinfo varchar(500)      
)      
      
      
      
declare SetRecepts cursor       
for select ticketsort, batchid from @ticketsort      
      
open SetRecepts      
      
FETCH NEXT FROM SetRecepts      
into @ticketid , @batchid      
      
      
--set @batchid=0        
      
--select * from tblcourts          
      
while @@fetch_status =0      
      
begin       
                
select @paymentamount = sum(chargeamount)                     
from tblticketspayment where ticketid = @ticketid                    
and paymenttype not in (99,100)                    
and paymentvoid not in (1)                    
                    
                    
set @casenumbers = ' '                    
select @casenumbers = @casenumbers + isnull(refcasenumber,' ') + ','                                 
from tblticketsviolations                    
where ticketid_pk = @ticketid                    
set @casenumbers = ltrim(left(@casenumbers,len(@casenumbers)-1))                    
                    
                    
                    
----------newly updated  in order to get contacts from tbltickets --------------                
declare @contact1 as varchar(15)                
declare @contact2 as varchar(15)                
declare @contact3 as varchar(15)                
                
select @contact1 = isnull(contact1,''),                  
@contact2=isnull(contact2,''),                
@contact3=isnull(contact3,'')                        
from tbltickets T                     
where (contact1 is not null or contact2 is not null or contact3 is not null )                 
and ticketid_pk=@ticketid     
                
set @contactinfo=@contact1                
if (@contact2<>'')                
set @contactinfo=@contactinfo+'/'+@contact2                
if (@contact3<>'')                
set @contactinfo=@contactinfo+'/'+@contact3                
if (left(@contactinfo,1)='/')                
begin                
set @contactinfo=right(@contactinfo,len(@contactinfo)-1)                
end                   
---------------                     
            
                    
set @paymentinfo = ' '                    
select @paymentinfo = @paymentinfo + '$' + convert(varchar(20),chargeamount)                      
+ '(' + convert(varchar(12),recdate) + ')' + ';'                    
from tblticketspayment where ticketid = @ticketid                    
and paymenttype not in (99,100)                    
and paymentvoid not in (1)                    
set @paymentinfo = ltrim(left(@paymentinfo,len(@paymentinfo)-1))                    
                    
      
insert into @receiptTable         
select T.ticketid_pk, firstname, MiddleName, lastname, T.Address1, T.Address2, @casenumbers as ticketnumber_pk,      
 t.languagespeak, @batchid as batchid, @paymentamount as paymentamount, T.city,S.state as state, T.zip,                    
 C.Address + ' ' + C.Address2 + ' ' + C.city + ', ' + 'TX ' + C.zip as Address,                    
 v.courtdatemain as currentdateset, totalfeecharged, totalfeecharged-@paymentamount as dueamount,                    
 @casenumbers as casenumbers, o.description as settintype, @paymentinfo as paymentinfo,       
 @contactinfo as contactinfo                
--from tbltickets T,  tblcourts C, tbldatetype D   ,tblstate S                 
from tbltickets T,  tblcourts C, tbldatetype D   ,tblstate S , tblticketsviolations V , tblcourtviolationstatus O           
where --T.ticketid_pk = V.ticketid_pk                    
--and                     
 T.stateid_fk=S.stateid              
and T.ticketid_pk = V.ticketid_pk        
and V.courtviolationstatusidmain = O.courtviolationstatusid        
and O.categoryid = D.typeid                
and T.ticketid_pk in (@ticketid) /*(select MainCat from dbo.Sap_String_Param(@ticketidlist))              */      
and activeflag = 1       and c.courtid = v.courtid            
                    
                  
--if @@rowcount >=1                     
-- exec sp_Add_Notes @ticketidlist,'Receipt Printed',null,@empid      
      
set @paymentinfo = ''      
set @contactinfo = ''      
set @contact1 = ''      
set @casenumbers = ''      
      
FETCH NEXT FROM SetRecepts      
into @ticketid , @batchid      
      
      
end       
      
CLOSE SetRecepts      
DEALLOCATE SetRecepts      
      
select * from @receiptTable      
      
      
    
  

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


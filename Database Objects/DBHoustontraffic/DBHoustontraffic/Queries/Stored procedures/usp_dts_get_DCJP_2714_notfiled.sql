SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_dts_get_DCJP_2714_notfiled]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_dts_get_DCJP_2714_notfiled]
GO

CREATE PROCEDURE [dbo].[usp_dts_get_DCJP_2714_notfiled]          
AS          
BEGIN          
 Select DISTINCT t.TicketID_PK AS ticketid_pk,        
 tv.casenumassignedbycourt as causenumber ,        
 t.Lastname + ' ' + t.Firstname AS FullName,         
 --t.TicketNumber_PK AS ticketnumber,         
 tv.refcasenumber as ticketnumber,        
 dbo.fn_DateFormat(tv.CourtDateMain, 24, '/', ':', 1) AS courtdate,        
 tvs.Description as casestatus,         
 tc.CourtName,      
 tc.CourtId      
from tblTickets t inner join           
 tblTicketsViolations AS tv ON t.TicketID_PK = tv.TicketID_PK LEFT OUTER JOIN           
 tblcourtViolationStatus tvs on tv.CourtViolationStatusIDmain =  tvs.courtviolationStatusId inner join           
 tblCourts tc on tv.CourtId = tc.courtId and tc.courtCategorynum = 11          
 WHERE (t.Activeflag = 1) and not exists(Select * from tblTicketsFlag where TicketId_Pk= t.ticketId_Pk and FlagID=20)   
 order by tc.CourtName          
END 
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


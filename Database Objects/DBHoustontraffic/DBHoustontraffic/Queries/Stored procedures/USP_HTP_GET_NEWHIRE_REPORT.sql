/*
* Created by :- Noufil 6126 07/24/2009
* Business Logic : This procedure return all traffic clients whose new hite status is set to 1 and court status must be out side court except HMC 
* Columns Return : 
*				   TicketID_PK,
				   Firstname,
				   Lastname,
				   paymentdate,
				   Pay,ment Source,
				   NewHireStatus,
				   CourtDate,
				   CourtID,
				   CourtName
*/
ALTER PROCEDURE [dbo].[USP_HTP_GET_NEWHIRE_REPORT]
AS
	SELECT DISTINCT tt.TicketID_PK,
	       tt.Firstname,
	       tt.Lastname,
	       MIN(ttp.RecDate) AS paymentdate,
	       CASE (
	                SELECT MIN(ttp2.PaymentType)
	                FROM   tblTicketsPayment ttp2
	                WHERE  ttp2.TicketID = tt.TicketID_PK
	                       AND DATEDIFF(DAY, MIN(ttp.RecDate), ttp2.RecDate) = 0
	            )
	            WHEN 7 THEN 'Online Sign Up'
	            ELSE 'Traffic Program'
	       END AS Source,
	       tt.NewHireCourtCoverageStatus AS NewHireStatus,
	       ttv.CourtID,
	       tc.CourtName,
	       ISNULL(tt.GeneralComments, '') AS GeneralComments,
	       (
	           ISNULL(
	               (
	                   SELECT MIN(ttv2.CourtDateMain)
	                   FROM   tblTicketsViolations ttv2
	                   WHERE  ttv2.TicketID_PK = tt.TicketID_PK
	                          AND DATEDIFF(DAY, ttv2.CourtDateMain, GETDATE()) >
	                              0
	               ),
	               MIN(ttv.CourtDateMain)
	           )
	       ) AS CourtDateMain
	FROM   tblTickets tt
	       INNER JOIN tblTicketsViolations ttv
	            ON  tt.TicketID_PK = ttv.TicketID_PK
	       INNER JOIN tblTicketsPayment ttp
	            ON  tt.TicketID_PK = ttp.TicketID
	       INNER JOIN tblCourts tc
	            ON  tc.Courtid = ttv.CourtID
	WHERE  --Ozair 7791 07/26/2010  where clause optimized    
	       ttp.PaymentVoid = 0
	       AND tt.Activeflag = 1
	       AND tt.CaseTypeId = 1
	       AND ISNULL(tt.NewHireCourtCoverageStatus, 0) = 1
	       AND tc.CourtID NOT IN (3001, 3002, 3003)
	GROUP BY
	       tt.TicketID_PK,
	       tt.Firstname,
	       tt.Lastname,
	       tt.NewHireCourtCoverageStatus,
	       ttv.CourtID,
	       CourtDateMain,
	       tc.CourtName,
	       tt.GeneralComments
	ORDER BY
	       ttv.CourtID,
	       CourtDateMain ASC
GO


GRANT EXECUTE ON [dbo].[USP_HTP_GET_NEWHIRE_REPORT]  TO dbr_webuser

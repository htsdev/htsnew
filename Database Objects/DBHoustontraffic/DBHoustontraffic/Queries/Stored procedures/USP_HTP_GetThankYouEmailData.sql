﻿/******  
Created by:		    Fahad Muhammad Qureshi
Business Logic :    This procedure is used to Get Data for all those clients which Read Or opened ThankYouEmail.
TaskId:				6429     
List of Parameters: 
					@stEmailedDate    : Starting date to get records for the report.
					@endEmailedDate   : Ending date to get records for the report.
******/

ALTER PROCEDURE dbo.USP_HTP_GetThankYouEmailData
	@stEmailedDate DATETIME,
	@endEmailedDate DATETIME
AS
	--Fahad 12/26/2009 7189 Removed Cloumns and added new one also modified the areas as Dallas and Houston  
	SELECT *
	FROM   (
	           SELECT DISTINCT 
	                  tt.TicketID_PK,
	                  tt.Firstname + ' ' + tt.Lastname AS ClientName,
	                  tt.Email AS emailid,
	                  City = 'Houston',
	                  --Ozair 7405 02/16/2010 Date Format set accordign to DSD
	                  dbo.fn_DateFormat(tt.ThankYouEmailSentDate,24,'/',':',1) AS ThankYouEmailSentDate,
	                  --Fahad 02/15/2010 7405 Changed date format according to DSD
	                  dbo.fn_DateFormat(tt.ThankYouEmailReadDate,24,'/',':',1) AS ThankYouEmailReadDate,	--Fahad 12/26/2009 7189 field added which showing email Open/Read.
	                  RequestedConsultation = CASE --Fahad 7189 01/05/2010 implementeed Blank Check
	                                               WHEN tt.ThankYouEmailReadDate 
	                                                    IS NULL THEN ''
	                                               WHEN (
	                                                        SELECT COUNT(tc.PersonID)
	                                                        FROM   SulloLaw.dbo.tblcontactus 
	                                                               tc
	                                                        WHERE  tc.TicketID = 
	                                                               tt.TicketID_PK
	                                                               AND tc.SiteId = 
	                                                                   1
	                                                    ) > 0 THEN 'Yes'
	                                               ELSE 'No'
	                                          END--Fahad 12/26/2009 7189 field added which showing Client visted our ContactUs page.
	           FROM   tblTickets tt
	           WHERE  DATEDIFF(DAY, @stEmailedDate, tt.ThankYouEmailSentDate) >= 
	                  0
	                  AND DATEDIFF(DAY, tt.ThankYouEmailSentDate, @endEmailedDate) 
	                      >= 0
	           
	           UNION
	           SELECT DISTINCT 
	                  tt.TicketID_PK,
	                  tt.Firstname + ' ' + tt.Lastname AS ClientName,
	                  tt.Email AS emailid,
	                  City = 'Dallas',
	                  --Fahad 02/15/2010 7405 Changed date format according to DSD
	                  dbo.fn_DateFormat(tt.ThankYouEmailSentDate,24,'/',':',1) AS ThankYouEmailSentDate,
	                  dbo.fn_DateFormat(tt.ThankYouEmailReadDate,24,'/',':',1) AS ThankYouEmailReadDate,	--Fahad 12/26/2009 7189 field added which showing email Open/Read.
	                  RequestedConsultation = CASE --Fahad 7189 01/05/2010 implementeed Blank Check
	                                               WHEN tt.ThankYouEmailReadDate 
	                                                    IS NULL THEN ''
	                                               WHEN (
	                                                        SELECT COUNT(tc.PersonID)
	                                                        FROM   SulloLaw.dbo.tblcontactus 
	                                                               tc
	                                                        WHERE  tc.TicketID = 
	                                                               tt.TicketID_PK
	                                                               AND tc.SiteId = 
	                                                                   1
	                                                    ) > 0 THEN 'Yes'
	                                               ELSE 'No'
	                                          END--Fahad 12/26/2009 7189 field added which showing Client visted our ContactUs page.
	           FROM   dallastraffictickets.dbo.tblTickets tt
	           WHERE  DATEDIFF(DAY, @stEmailedDate, tt.ThankYouEmailSentDate) >= 
	                  0
	                  AND DATEDIFF(DAY, tt.ThankYouEmailSentDate, @endEmailedDate) 
	                      >= 0
	       ) a
	ORDER BY
	       a.ThankYouEmailSentDate DESC
GO


GRANT EXECUTE ON dbo.USP_HTP_GetThankYouEmailData TO dbr_webuser
GO 


 
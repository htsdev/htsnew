SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_RM_GetTicketsCountforHMC]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_RM_GetTicketsCountforHMC]
GO


CREATE procedure [dbo].[usp_RM_GetTicketsCountforHMC]
as
select convert(varchar(2),datepart(month,dateadd(month,-datediff(month,recloaddate,getdate()),getdate())))
+ '/' + 
convert(varchar(4),datepart(year,dateadd(month,-datediff(month,recloaddate,getdate()),getdate())))


--convert(varchar(4),datepart(year,dateadd(month,-datediff(month,recloaddate,getdate()),getdate())))
as ReportMonth,count(distinct recordid) as TotalTickets
from tblticketsarchive 
where datediff(month,recloaddate,getdate()) < 12
and courtid in (3001,3002,3003)
group by datediff(month,recloaddate,getdate())
order by datediff(month,recloaddate,getdate())

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


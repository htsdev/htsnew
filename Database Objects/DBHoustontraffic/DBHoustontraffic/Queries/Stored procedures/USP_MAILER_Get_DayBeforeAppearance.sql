SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_MAILER_Get_DayBeforeAppearance]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_MAILER_Get_DayBeforeAppearance]
GO


--  USP_MAILER_Get_DayBeforeAppearance '07/10/2006', '07/18/2006', 6, 0, 6
CREATE procedure [dbo].[USP_MAILER_Get_DayBeforeAppearance]
	(
	@startdate datetime,
	@enddate datetime,
	@courtid int = 6,
	@bondflag tinyint = 0,
	@catnum int=6
	)

as

declare	@officernum varchar(50),                                                    
	@officeropr varchar(50),                                                    
	@tikcetnumberopr varchar(50),                                                    
	@ticket_no varchar(50),                                                                                                  
	@zipcode varchar(50),                                                             
	@zipcodeopr varchar(50),                                                    
	@fineamount money,                                                            
	@fineamountOpr varchar(50) ,                                                            
	@fineamountRelop varchar(50),                                   
	@singleviolation money,                                                            
	@singlevoilationOpr varchar(50) ,                                                            
	@singleviolationrelop varchar(50),                                                    
	@doubleviolation money,                                                            
	@doublevoilationOpr varchar(50) ,                                                            
	@doubleviolationrelop varchar(50),                                                  
	@count_Letters int,                                  
	@violadesc varchar(500),                                  
	@violaop varchar(50), 
	@sqlquery varchar(8000)                                                  

set @enddate =@enddate+'23:59:59.000'                                                         
        
set @sqlquery = ''                                                 

Select 	@officernum=officernum,                                                    
	@officeropr=oficernumOpr,                                                    
	@tikcetnumberopr=tikcetnumberopr,                                                    
	@ticket_no=ticket_no,                                                    
	@zipcode=zipcode,                                                    
	@zipcodeopr=zipcodeLikeopr,                                            
	@singleviolation =singleviolation,                                               
	@singlevoilationOpr =singlevoilationOpr,                                                    
	@singleviolationrelop =singleviolationrelop,                                                    
	@doubleviolation = doubleviolation,                                                    
	@doublevoilationOpr=doubleviolationOpr,                                                            
	@doubleviolationrelop=doubleviolationrelop,                                  
	@violadesc=violationdescription,                                  
	@violaop=violationdescriptionOpr ,                              
	@fineamount=fineamount ,                 
	@fineamountOpr=fineamountOpr ,                                                 
	@fineamountRelop=fineamountRelop                 
from 	tblmailer_letters_to_sendfilters                               
where 	courtcategorynum=@catnum                                                    
					
declare @dateVar varchar(30)


set @sqlquery=@sqlquery+'                                        
        Select distinct tva.recordid, 
	'

if @bondflag = 1 
	begin
		set @dateVar = ' tva.violationbonddate '
		set @sqlquery=@sqlquery+'                                        
			convert(datetime,convert(varchar,tva.violationbonddate,101)) as mdate, '
	end
else
	begin
		set @dateVar = ' ta.recloaddate '
		set @sqlquery=@sqlquery+'                                        
				convert(datetime, convert(varchar,ta.recloaddate,101))as mdate, '	end

set @sqlquery=@sqlquery+'                                        
	 flag1 = isnull(ta.Flag1,''N'') ,                                                    
	 isnull(ta.donotmailflag,0) as donotmailflag,                                                    	
	 count(tva.ViolationNumber_PK) as ViolCount,                                                    
	 isnull(sum(isnull(tva.fineamount,0)),0)as Total_Fineamount
into #temp                                                          
FROM LiveTrafficTickets.dbo.tblTicketsViolationsArchive TVA                     
INNER JOIN                     
	LiveTrafficTickets.dbo.tblTicketsArchive TA ON TVA.recordid= TA.recordid
--	and  tva.TicketNumber_PK  not like ''F%'''            

if(@courtid=6)
	set @sqlquery=@sqlquery+' 
	and ta.courtid In (Select courtid from dallasmailerlive.dbo.tblcourts where courtcategorynum ='+convert(varchar(10),@catnum ) +')'          
else
	set @sqlquery=@sqlquery+' 
	and ta.courtid=' + convert(varchar,@courtid)

                                    
if(@officernum<>'')                                                      
	set @sqlquery =@sqlquery + '  
	and  ta.officerNumber_Fk ' +@officeropr  +'('+   @officernum+')'                                                       
                                
                                                     
if(@ticket_no<>'')                      
	set @sqlquery=@sqlquery + ' 
	and ('+  dbo.Get_String_Concat_With_op(''+@ticket_no+'' , 'tva.TicketNumber_PK' ,+ ''+ @tikcetnumberopr) +')'                                                            
                                  
                                  
if(@zipcode<>'')                                                                            
	set @sqlquery=@sqlquery+ ' 
	and  ('+  dbo.Get_String_Concat_With_op(''+@zipcode+'' , 'ta.zipcode' ,+ ''+ @zipcodeopr) +')'
                               
if(@violadesc<>'')          
	set @sqlquery=@sqlquery+ '  
	and ('+  dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ @violaop)   +')'                                                   

set @sqlquery=@sqlquery +'  
--	and ta.clientflag=0
	and ta.lastname is not null 
	and ta.firstname is not null'                                                        
                                                      
set @sqlquery =@sqlquery + ' 
and ta.recordid  not in 
	(                                                                  
	select recordid FROM dbo.tblLetterNotes n where  n.courtid = ta.courtid
	and n.listdate between  '''+ convert(varchar(10),@startdate,101)+''' and  '''+ convert(varchar(10),@enddate + 1,101)+'''
	and datediff(day, n.recordloaddate, getdate() ) < 30
	)      '

if @bondflag = 1 
	set @sqlquery =@sqlquery + '                                              
	and tva.violationbonddate is not null 
'
else
	set @sqlquery =@sqlquery + '                                              
	and tva.violationbonddate is null --and datediff(day, ta.violationdate, getdate()) between 0 and 18
'


set @sqlquery =@sqlquery + ' 
	and ' + @datevar + ' between ''' + convert(varchar,@startDate,101)  + ''' and  ''' + convert(varchar,@endDate + 1, 101) + '''
 group by                                                     
 convert(datetime, convert(varchar,' + @dateVar + ',101)),
 ta.flag1,                                                    
 ta.donotmailflag,                                                    
 tva.recordid

'                                
if(@fineamount<>0 and @fineamountOpr<>'not'  )                                
set @sqlquery =@sqlquery+ '
      and sum(tva.fineamount)'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                                     
                                                    
set @sqlquery=@sqlquery+                                       
'Select distinct recordid, ViolCount, Total_Fineamount,  mDate ,Flag1, donotmailflag into #temp1  from #temp                                                        
 '                                
if(@singleviolation<>0 and @doubleviolation=0)                                
	set @sqlquery =@sqlquery+ 'Where  '+ @singlevoilationOpr+                                                  
      '  (Total_FineAmount '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and ViolCount=1)                                
         or violcount>3                                                             
      '                                

                                
                                     
if(@doubleviolation<>0 and @singleviolation=0 )                                
begin                                
set @sqlquery=@sqlquery+                                       
'Select distinct recordid, ViolCount,Total_Fineamount,  mDate ,                                            
 FineAmount,Flag1,donotmailflag into #temp1  from #temp                                                      
 '                                
set @sqlquery =@sqlquery + 'where   '+ @doublevoilationOpr+                                                       
       '   (Total_FineAmount '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ')and ViolCount=2)                                                      
      or violcount>3 '                                                    
end                            
                                
if(@doubleviolation<>0 and @singleviolation<>0)                                
begin                                
set @sqlquery =@sqlquery+ 'Where  '+ @singlevoilationOpr+                                                      
      '  (Total_FineAmount '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and ViolCount=1)                                
         or   '                                
set @sqlquery =@sqlquery +  @doublevoilationOpr+                                                       
       '   (Total_FineAmount '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ') and ViolCount=2)                                                      
      or violcount>3 '                                  
                                
end                                
set @sqlquery=@sqlquery+' 
--Select count(distinct recordid) as Total_Count, 
Select count(distinct recordid) as Total_Count, mDate                                                  
into #temp2                                                
from #temp1                                                
group by mDate                                                     

Select count(distinct recordid) as Good_Count, mDate                                                                                              
into #temp3                                                
from #temp1                                                    
where Flag1 in (''Y'',''D'',''S'')   
and donotmailflag =  0                                                 
group by mDate 
                                                    
Select count(distinct recordid) as Bad_Count, mDate                                                  
into #temp4                                                    
from #temp1                                                
where Flag1  not in (''Y'',''D'',''S'')                                                    
and donotmailflag = 0                                                 
group by mDate 
                 
Select count(distinct recordid) as DonotMail_Count, mDate                                                 
 into #temp5                                                    
from #temp1        
where donotmailflag = 1  
--and flag1  in  (''Y'',''D'',''S'')                                                    
group by mDate 
       
                                                    
Select   listdate = #temp2.mDate ,
        Total_Count,
        isnull(Good_Count,0) Good_Count,
        isnull(Bad_Count,0)Bad_Count,
        isnull(DonotMail_Count,0)DonotMail_Count
from #Temp2 left outer join #temp3 
on #temp2.mDate = #temp3.mDate
                                                   
left outer join #Temp4
on #temp2.mDate = #Temp4.mDate
  
left outer join #Temp5
on #temp2.mDate = #Temp5.mDate                                                 
  
where Good_Count > 0              
order by #temp2.mDate desc                                                     
          
                                              
drop table #temp                                                
drop table #temp1                                                
drop table #temp2                                                    
drop table #temp3                                                
drop table #temp4
drop table #temp5
'                                                    
                                                    
--print @sqlquery
exec(@sqlquery)







GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO



/****************
Created By : Zeeshan Ahmed

Business Logic : This procedure is used to get  list of Active Courts for Court Search Option 

List Of Columns :

	Court ID    : Court Id
	CourtName	: Court Name

************/
ALTER Procedure [dbo].[USP_PS_GET_COURT_LIST]  
  
as  
  
Select Distinct  Convert(varchar,CourtID) + '-1' as CourtID ,zip, CourtName  from tblcourts  where inactiveflag = 0 -- Afaq 7822 06/11/2010 add zip column. 
and courtid in  ( 3001,3002,3003,3004,3007,3013,3016 ,3060)
or Courtid in ( Select CourtId from tblcourts where courtcategorynum = 2 and inactiveflag = 0)
-- Zeeshan 4781 09/12/2008 Do Not Display Inactive courts
and courtid not in (Select CourtID from SullolawInActiveCourts)


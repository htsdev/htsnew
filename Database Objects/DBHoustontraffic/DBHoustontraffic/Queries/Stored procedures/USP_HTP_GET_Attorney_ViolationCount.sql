
/******************************************
* Created by : Yasir Kamal
* Date : 08/03/2009 
* Task id: 6056
* 
* Business Logic: This stored procedure is used to highlight the trend for hiring attorneys by people for their multiple violations.it will display top 10 attorneys.
* 
* Input Parameters: N/A
* 
* Output Columns:
*  1)Attorney Name.
*  2)Month.
*  3)Total Client count with Single Violation[Upto 8 violations and each will appear in a separate column]. 
* 
******************************************/




ALTER PROCEDURE [dbo].[USP_HTP_GET_Attorney_ViolationCount]
AS
SELECT distinct  
	eet.associatedcasenumber as midnumber,
	CASE WHEN LEN(ISNULL(p.[Name],'')) =0 THEN 'NO ATTORNEY' ELSE P.[Name] END AS ATTORNEY,
	CONVERT(DATETIME,CONVERT(VARCHAR,MONTH(eet.updatedate)) + '/01/'+ CONVERT(VARCHAR, YEAR(eet.updatedate))) AS eventdate ,
	COUNT(distinct eet.causenumber) AS violcount
into #temp
FROM	LoaderFilesArchive.dbo.tblEventExtractTemp_competitor AS eet 
INNER JOIN  tblCompetitors AS p ON	ISNULL(eet.attorneyname, '') = p.Name  
INNER JOIN  tblCourtViolationStatus c ON eet.status = c.Description AND	c.CategoryID = 4
WHERE	(ISNULL(eet.barnumber, '') <> '' OR ISNULL(eet.attorneyname, '') <> '') 
AND		(ISNULL(eet.bondingnumber, '') = '') 
AND		(ISNULL(eet.bondingcompanyname, '') = '') 
and datediff(day, eet.updatedate, '1/1/2008')<=0 
and datediff(day, eet.updatedate, GETDATE())>=0  
GROUP BY 
	eet.associatedcasenumber , 
	p.name, 
	CONVERT(DATETIME,CONVERT(VARCHAR,MONTH(eet.updatedate)) + '/01/'+ CONVERT(VARCHAR, YEAR(eet.updatedate)))
	
INSERT INTO #temp ( midnumber, attorney, eventdate, violcount)	
SELECT distinct  
	eet.associatedcasenumber ,
	'NO ATTORNEY' AS ATTORNEY,
	CONVERT(DATETIME,CONVERT(VARCHAR,MONTH(eet.updatedate)) + '/01/'+ CONVERT(VARCHAR, YEAR(eet.updatedate))) AS eventdate ,
	COUNT(distinct eet.causenumber) AS violcount
FROM	LoaderFilesArchive.dbo.tblEventExtractTemp_competitor AS eet  
INNER JOIN  tblCompetitors AS p ON	ISNULL(eet.attorneyname, '') = p.Name  
INNER JOIN  tblCourtViolationStatus c ON eet.status = c.Description AND	c.CategoryID = 4
WHERE	datediff(day, eet.updatedate, '1/1/2008')<=0 
and datediff(day, eet.updatedate, GETDATE())>=0  
AND LEN(ISNULL(eet.barnumber,'') + ISNULL(eet.attorneyname,'')  ) = 0

GROUP BY 
	eet.associatedcasenumber, 
	CONVERT(DATETIME,CONVERT(VARCHAR,MONTH(eet.updatedate)) + '/01/'+ CONVERT(VARCHAR, YEAR(eet.updatedate)))	
  
DECLARE  @temp2 TABLE  ( eventdate DATETIME, attorney VARCHAR(200), total INT, viol1 INT, viol2 INT, viol3 INT, viol4 INT, viol5 INT, viol6 INT, viol7 INT, viol8 INT)  

INSERT INTO @temp2 (EVENTdate, attorney, total)
SELECT eventdate, attorney, COUNT (DISTINCT midnumber) FROM #temp   
GROUP BY attorney , eventdate
ORDER BY COUNT (DISTINCT midnumber) DESC 

  
UPDATE a 
SET a.viol1 = (SELECT COUNT(DISTINCT midnumber) FROM #temp WHERE attorney =  a.attorney AND violcount = 1 AND DATEDIFF(DAY, eventdate, a.eventdate) = 0 )
FROM @temp2 a 

UPDATE a 
SET a.viol2 = (SELECT COUNT(DISTINCT midnumber) FROM #temp WHERE attorney = a.attorney AND violcount = 2 AND DATEDIFF(DAY, eventdate, a.eventdate) = 0 )
FROM @temp2 a 

UPDATE a 
SET a.viol3 = (SELECT COUNT(DISTINCT midnumber) FROM #temp WHERE attorney = a.attorney AND violcount = 3 AND DATEDIFF(DAY, eventdate, a.eventdate) = 0 )
FROM @temp2 a 

UPDATE a 
SET a.viol4 = (SELECT COUNT(DISTINCT midnumber) FROM #temp WHERE attorney = a.attorney AND violcount = 4 AND DATEDIFF(DAY, eventdate, a.eventdate) = 0 )
FROM @temp2 a 

UPDATE a 
SET a.viol5 = (SELECT COUNT(DISTINCT midnumber) FROM #temp WHERE attorney = a.attorney AND violcount = 5 AND DATEDIFF(DAY, eventdate, a.eventdate) = 0 )
FROM @temp2 a 

UPDATE a 
SET a.viol6 = (SELECT COUNT(DISTINCT midnumber) FROM #temp WHERE attorney = a.attorney AND violcount = 6 AND DATEDIFF(DAY, eventdate, a.eventdate) = 0 )
FROM @temp2 a 

UPDATE a 
SET a.viol7 = (SELECT COUNT(DISTINCT midnumber) FROM #temp WHERE attorney = a.attorney AND violcount = 7 AND DATEDIFF(DAY, eventdate, a.eventdate) = 0 )
FROM @temp2 a 

UPDATE a 
SET a.viol8 = (SELECT COUNT(DISTINCT midnumber) FROM #temp WHERE attorney = a.attorney AND violcount >= 8 AND DATEDIFF(DAY, eventdate, a.eventdate) = 0 )
FROM @temp2 a 


SELECT DISTINCT eventdate INTO #temp3 FROM @temp2 ORDER BY eventdate
ALTER TABLE #temp3 ADD rowid INT IDENTITY
DECLARE @idx INT 
DECLARE @reccount INT,
	@date DATETIME 

SET @idx = 1
select @reccount = COUNT(rowid) FROM #temp3

DECLARE  @temp4 TABLE  ( eventdate DATETIME, attorney VARCHAR(200), total INT, viol1 INT, viol2 INT, viol3 INT, viol4 INT, viol5 INT, viol6 INT, viol7 INT, viol8 INT)  

WHILE @idx <= @reccount
BEGIN
	
	SELECT  @date = eventdate FROM #temp3 WHERE rowid = @idx 
	
	INSERT INTO @temp4(eventdate,attorney,total,viol1,viol2,viol3,viol4,viol5,viol6,viol7,viol8)
	SELECT TOP 10 * FROM @temp2 WHERE DATEDIFF(DAY, eventdate, @date)=0	
	AND attorney <> 'NO ATTORNEY'
	ORDER BY total DESC 
	
	INSERT INTO @temp4 (eventdate,attorney,total,viol1,viol2,viol3,viol4,viol5,viol6,viol7,viol8)
	SELECT * FROM @temp2 WHERE DATEDIFF(DAY, eventdate, @date) =0 AND attorney = 'NO ATTORNEY'
	
	SET @idx = @idx + 1
END




SELECT * FROM @temp4 ORDER BY eventdate, total DESC
  
drop table #temp

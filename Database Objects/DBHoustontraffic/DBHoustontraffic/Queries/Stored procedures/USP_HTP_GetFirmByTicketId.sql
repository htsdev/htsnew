﻿/****** Object:  StoredProcedure [dbo].[USP_DTP_Update_NonHMCFollowUpdate]  ******/     
/*        
Created By     : Fahad Muhammad Qureshi.
Created Date   : 07/25/2009  
TasK		   : 6054        
Business Logic : This procedure is used to fetching out Firm Id on then basis of Client TicketID in Billing Section.			        
Parameter: 
	@TicketID  : Id on basis of we are fetching information from the database

        
*/

CREATE PROCEDURE dbo.USP_HTP_GetFirmByTicketId
@TicketID AS INT
AS
SELECT top 1 tt.FirmID FROM tblTickets tt WHERE tt.TicketID_PK=@TicketID
GO

GRANT EXEC ON [dbo].[USP_HTP_GetFirmByTicketId] TO dbr_webuser
GO   
/****** 
Create by		: Tahir Ahmed
Created Date	: 5/12/2009
TasK ID			: 5859

Business Logic:
	The stored procedure is by HTP /Backroom /Jury Outcome Report. 
	It displays the summary of violation outcome for Jury Trial Cases
	of Sullo & Sullo clients. The source information is docket close out
	snap shot. The outcome status is updated by "Update Jury Outcome Report"
	SQL job in the source table by getting information from HMC Data files.

	It returns 4 tables for:
	1. Docket Summary
	2. Officer Summary
	3. Court Room Summary
	4. Un-Resulted Cases Summary.
				
List of Parameters:
	@DocketDate: Date of docket. By default it will list last business day's docket.

******/

-- USP_HTP_Get_JuryOutCome '5/5/09'

ALTER PROCEDURE dbo.USP_HTP_Get_JuryOutCome

	@DocketDate DATETIME = null
	

AS

-- setting default docket date...
IF @DocketDate IS NULL	
	SET @DocketDate =  CONVERT(VARCHAR(10), GETDATE()-1, 101)

-- first get the jury violations from docket close out snap shot....
select distinct 
	t.ticketid_pk, 
	v.ticketsviolationid,
	upper(isnull(t.lastname,'') + isnull(', '+t.firstname,'') ) client,
	-- tahir 6133 07/09/2009 fixed the court time issue...
	--convert(int, isnull(v.courtnumbermain,0)) crtNum, 
	convert(int, isnull(s.courtnumber,0)) crtNum, 
	--dbo.fn_dateformat(v.courtdatemain, 26,'/',':',1) crtTime, 
	dbo.fn_dateformat(s.courtdate, 26,'/',':',1) crtTime, 
	case when isnull(t.cdlflag,0)  = 1 then 'CDL' else '' end  as cdlflag,
	case when isnull(t.bondflag,0) = 1 then 'BONDS' else '' end as bondflag,
	isnull(v.ticketofficernumber,0) officernumber,
	upper(isnull(o.firstname,'') + isnull(' '+o.lastname, '')) as officer,
	upper(case when len(isnull(m.shortdesc,'')) =0  then m.description else m.shortdesc end) as viol,
    IsMoving = case when isnull(m.categoryid ,0) = 1 then 1 else 0 end,
	s.outcometype, s.statusid, s.outcomecomments
into #temp
from tbl_HTS_DocketCloseOut_Snapshot s 
inner join tblticketsviolations v on v.ticketsviolationid  = s.ticketsviolationid
inner join tbltickets t on v.ticketid_pk  = t.ticketid_pk
left join tblofficer o on o.officernumber_pk = v.ticketofficernumber
left join tblviolations m on m.violationnumber_pk = v.violationnumber_pk
where datediff(Day, s.courtdate, @DocketDate)=0
and shortdescription = 'jur'
AND v.courtid IN (3001,3002,3003)

-- getting  officer number for all the clients
select ticketid_pk, max(officernumber) officernumber
into #officer 
from #temp 
group by ticketid_pk

update a set a.officernumber = b.officernumber
from #temp a inner join #officer b on a.ticketid_pk = b.ticketid_pk

update a
set officer = (select top 1 officer from #temp where officernumber = a.officernumber )
from #temp a

-- if officer is not available....
update #temp
set officer = 'OFFICER N/A'
WHERE officernumber = 0 or len(officer) =0 or officernumber in (128795 ,962528)

update #temp set outcometype = 5 where ismoving = 0 and outcometype =2

alter table #temp add subtype tinyint, ViolCount int , PleadCount int, IsAllPlead bit

-- settting outcome type 'WARRATN' for dlq cases...
-- warrant...
update a set a.subtype = 3 
from #temp a inner join #temp b on a.ticketid_pk = b.ticketid_pk
where b.outcometype = 3 and isnull(a.subtype,0) = 0 

-- reset...
update a set a.subtype = 4 
from #temp a inner join #temp b on a.ticketid_pk = b.ticketid_pk
where b.outcometype = 4 and isnull(a.subtype,0) = 0

-- plead guilty
update a set a.subtype = 2 
from #temp a inner join #temp b on a.ticketid_pk = b.ticketid_pk
where b.outcometype = 2 and isnull(a.subtype,0) = 0 

-- dadj/dsc
update a set a.subtype = 1
from #temp a inner join #temp b on a.ticketid_pk = b.ticketid_pk
where b.outcometype = 1 and isnull(a.subtype,0) = 0 

-- dismissed/disposed
update a set a.subtype = 5
from #temp a inner join #temp b on a.ticketid_pk = b.ticketid_pk
where b.outcometype = 5 and isnull(a.subtype,0) = 0 

update #temp set subtype = 0 where subtype is null

select ticketid_pk, count(ticketsviolationid) violcount
into #violcount from #temp  group by ticketid_pk

select ticketid_pk, count(ticketsviolationid) pleadcount
into #pleadcount from #temp where outcometype = 2 group by ticketid_pk

update a set a.violcount = b.violcount
from #temp a inner join #violcount b on a.ticketid_pk = b.ticketid_pk

update a set a.pleadcount = b.pleadcount
from #temp a inner join #pleadcount b on a.ticketid_pk = b.ticketid_pk

update #temp set IsAllPlead = 1 where violcount = pleadcount and subtype = 2

-- GETTING VALUES FOR DOCKET SUMMARY IN VARIABLES...
declare @total int, @warrant int, @reset int, @unresulted int, @disposed int, 
		@dsc_dadj int, @plead_all int, @plead_part int, @perc_client_disp float, 
		@perc_dsc_dadj_plead float

select @total = count(distinct ticketid_pk) from #temp
select @warrant = count(distinct ticketid_pk) from #temp where subtype = 3
select @reset = count(distinct ticketid_pk) from #temp where subtype = 4
select @unresulted = count(distinct ticketid_pk) from #temp where subtype = 0
select @disposed = count(distinct ticketid_pk) from #temp where subtype in (1,2,5) 
select @dsc_dadj = count(distinct ticketid_pk) from #temp where subtype = 1
select @plead_all = count(distinct ticketid_pk ) from #temp where subtype = 2 and isnull(isallplead,0) = 1
select @plead_part = count(distinct ticketid_pk ) from #temp where subtype = 2 and isnull(isallplead,0) = 0

select	@total = isnull(@total,0), 
		@warrant = isnull(@warrant,0),
		@reset = isnull(@reset , 0),
		@unresulted = isnull(@unresulted,0),
		@disposed = isnull(@disposed,0),
		@dsc_dadj = isnull(@dsc_dadj,0),
		@Plead_all = isnull(@plead_all,0),
		@plead_part = isnull(@plead_part,0)

-- tahir 6002 06/12/2009 rounding off to 2 digits...
select @perc_client_disp = round( convert(float, case when  @disposed= 0 then 0 else  convert(float, (convert(float,@disposed) - ( convert(float,@dsc_dadj) + convert(float,@plead_part) )) / convert(float,@disposed) ) * 100 end),2)
select @perc_dsc_dadj_plead = round( convert(float, case when @disposed = 0 then 0 else  convert(float, (convert(float,@dsc_dadj) + convert(float,@plead_part)) / convert(float,@disposed)) * 100 END),2)

CREATE TABLE #docket (total INT, warrant INT, reset INT, unresulted INT, disposed INT, dsc_dadj INT ,
		plead_part INT , plead_all INT , perc_client_disp float , perc_dsc_dadj_plead float )

-- RETURNING DOCKET SUMMARY
-- TABLE 1
IF @total > 0 
BEGIN
	INSERT INTO #docket  
	select @total , @warrant , @reset , @unresulted , @disposed ,@dsc_dadj ,
		@plead_part, @plead_all , @perc_client_disp , @perc_dsc_dadj_plead 
END


-- RETURNING DOCKET SUMMARY
-- TABLE 1
SELECT * FROM #docket
		 
-- RETURNING OFFICER SUMMARY 
-- TABLE 2	
select	officer, ticketid_pk, ticketsviolationid, 
		client, cdlflag, bondflag, crtnum, crttime, viol, 
		outcome = case OUTCOMETYPE
					WHEN 0 THEN 'PENDING'
					WHEN 1 THEN 'DSC/DADJ'
					WHEN 2 THEN 'PLEA'
					when 3 then 'WARRANT'
					WHEN 4 THEN 'RESET'
					WHEN 5 THEN 'DISM'
					ELSE 'DISM'
				  END
FROM #TEMP where subtype in (1,2)
order by officer, client, crtnum, crttime, viol, outcome

-- RETURNING COURT ROOM SUMMARY...
-- TABLE 3
select crtnum, count(distinct ticketid_pk) as dsc_dadj_plea
from #temp 
where subtype in (1,2)
group by crtnum
order by crtnum

-- RETURNING UN-RESULTED CASES SUMMARY 
-- TABLE 4
select officer, ticketid_pk, ticketsviolationid, client, crtnum, viol
from #temp where subtype = 0 
order by crtnum, client, officer  -- tahir 5910 05/14/2009 sort by court number...

drop table #temp
drop table #violcount
drop table #pleadcount
DROP TABLE #docket



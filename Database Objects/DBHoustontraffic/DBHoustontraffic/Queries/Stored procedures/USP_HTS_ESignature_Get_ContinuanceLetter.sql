--exec [USP_HTS_ESignature_Get_ContinuanceLetter] 124138, 3991, '995185845767931'  
ALTER procedure [dbo].[USP_HTS_ESignature_Get_ContinuanceLetter]       
        
@Ticketid int,        
@employeeid int,  
@SessionID varchar(200)  
as        
        

--drop table #tblcontinuance        
      -- Agha Usman 2664 06/30/2008 - currentcourtnum    
select T.ticketid_pk,refcasenumber,O.description,firstname, lastname,courtdatemain,Address,C.city,S.state,C.zip,contrequestnotes        
into #tblcontinuance        
from tbltickets T, tblticketsviolations V, tblcourts C, tblstate S, tblviolations O        
where T.ticketid_pk = V.ticketid_pk        
and V.CourtId = C.courtid        
and C.state  = S.stateid        
and v.violationnumber_pk = O.violationnumber_pk        
and violationtype <> 1        
and activeflag = 1        
and continuanceamount > 0        
and T.ticketid_pk = @ticketid          
        
if @@rowcount >=1        
-- exec sp_Add_Notes @TicketID,'Continuance Letter Printed',null,@employeeid        
          
delete from #tblcontinuance where description not in         
(select Top 1 O.description        
from tbltickets T, tblticketsviolations V, tblcourts C, tblstate S, tblviolations O        
where T.ticketid_pk = V.ticketid_pk        
and V.CourtId = C.courtid        
and C.state  = S.stateid        
and v.violationnumber_pk = O.violationnumber_pk        
and violationtype <> 1        
and activeflag = 1        
and continuanceamount > 0        
and T.ticketid_pk = @ticketid )         
        
alter table #tblcontinuance        
alter Column [description] VARCHAR(2000) NOT NULL        
        
        
Declare MergeDescCol cursor for        
select O.description , refcasenumber        
from tbltickets T, tblticketsviolations V, tblcourts C, tblstate S, tblviolations O        
where T.ticketid_pk = V.ticketid_pk        
and V.CourtId = C.courtid        
and C.state  = S.stateid        
and v.violationnumber_pk = O.violationnumber_pk        
and violationtype <> 1        
and activeflag = 1        
and continuanceamount > 0        
and T.ticketid_pk = @ticketid          
        
        
declare @description as varchar(2000)        
declare @refcasenumber as varchar(2000)        
        
OPEN MergeDescCol        
        
-- Get the first row.         
FETCH NEXT FROM MergeDescCol        
INTO @description, @refcasenumber        
        
--          
update #tblcontinuance set [description] = @refcasenumber + ' : ' + @description        
      
FETCH NEXT FROM MergeDescCol        
INTO @description, @refcasenumber        
        
WHILE @@FETCH_STATUS = 0         
BEGIN         
        
update #tblcontinuance set [description] = [description] +CHAR(13)+CHAR(10)+ @refcasenumber + ' : ' + @description        
        
FETCH NEXT FROM MergeDescCol        
INTO @description, @refcasenumber        
        
END         
        
CLOSE MergeDescCol        
DEALLOCATE MergeDescCol        
        
        
        -- Agha Usman 2664 06/30/2008 - currentcourtnum
select  ticketid_pk,refcasenumber,[description],firstname, lastname,courtdatemain,Address,city,state,zip,contrequestnotes,   
(Select top 1 ImageStream From Tbl_HTS_ESignature_Images Where ImageTrace = 'ImageStream_LetterContinuance_Attorney1' and TicketID = @ticketID and SessionID = @SessionID) as AttorneySign1  
from #tblcontinuance        
go 

--USP_HTP_GET_AllUnMatchCourtIDs 76332
/*
Created by:  Sarim  4636  08/26/2008

Business Logic : this procedure is used to check the association  b/w case and court.
				 

List of Parameter:
		@ticketid_pk: 
				To get case type association of a particular case.
List of return values:
				if the case type is associated with court then it will return 0
				and if case type is not associated with court then it will return 1

*/

CREATE procedure [dbo].[USP_HTP_GET_AllUnMatchCourtIDs]

@ticketid_pk int

as

Declare @counts int

select @counts = count(*)  from tbltickets t  
	inner join tblticketsviolations tv on t.ticketid_pk = tv.ticketid_pk	
	inner join tblcourts c on c.courtid = tv.courtid
where  tv.ticketid_pk = @ticketid_pk 
	and  c.casetypeid <> t.casetypeid

if @counts = 0 
	select 0

else 
	select 1

go

grant execute on [dbo].[USP_HTP_GET_AllUnMatchCourtIDs] to dbr_webuser
go
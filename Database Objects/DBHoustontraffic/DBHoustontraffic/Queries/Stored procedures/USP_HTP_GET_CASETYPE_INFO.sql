

/*******    
    
Created By : Agha Usman  
    
Business Logic : This procedure is used to get case type Information .    
    
List of Columns :     
    
  CaseTypeID : Case Type Id    
  CaseTypeName : Case Type Name   
  ServiceTicketEmailAlert:	Open Email 
  ServiceTicketEmailClose: Close Email    
  ServiceTicketAngryEmailOpen : Angry open Email
  ServiceTicketAngryEmailClose : Angry Close Email 
  ServiceTicketUpdateEmail :	Updat Email
  Username : Employee UserName
*******/    
    
ALTER PROCEDURE [dbo].[USP_HTP_GET_CASETYPE_INFO]  
@CaseTypeId int = -1    
AS
BEGIN    
     
Select	CaseTypeId , 
		CaseTypeName,
		ServiceTicketEmpId,
		isnull(ServiceTicketEmailAlert,'N/A') as ServiceTicketEmailAlert,
		isnull(ServiceTicketEmailClose,'N/A') as ServiceTicketEmailClose,
		isnull(ServiceTicketAngryEmailOpen,'N/A') as ServiceTicketAngryEmailOpen,
	    isnull(ServiceTicketAngryEmailClose,'N/A') as ServiceTicketAngryEmailClose,
	    --Zahoor 4757 09/09/2008 for service ticket update email.
	    isnull(ServiceTicketUpdateEmail , 'N/A') as ServiceTicketUpdateEmail 
		, (select Username from tblusers where EmployeeID=CaseType.ServiceTicketEmpId) as Username -- Noufil 4338 07/03/2008 Username field added
From CaseType where @CaseTypeId  =  -1 or CaseTypeId = @CaseTypeId    
-- Agha Usman 4271 06/27/2008    
END
GO



﻿/**  
* Abbas 9223 04/29/2011  
* Business Logic :  This Store procedure display a list of all current clients with FUTURE Verified dates who have a different court date or time or court location or status for the AUTO DATE. 
System shall look at the PREVIOUS auto date IF the autodate was change with respect to Verifed court date update by the system.
If there is NO FUTURE AUTODATE (EITHER BLANK OR IT IS A PAST AUTO DATE) then do not list the case on the report. 
Make sure that Auto Date is in Future


*/

ALTER PROCEDURE [dbo].[USP_HTP_Get_CurrentClientCourtDateChecker]
AS
	SELECT DISTINCT t.TicketID_PK,
	       t.Firstname,
	       t.Lastname,
	       tv.RefCaseNumber AS ticketnumber,
	       tv.RefCasenumber AS refcasenumberseq,
	       -- Babar Ahmad 9223 10/13/2011 Verified court date, time and room number e.g [06/01/2018 @ 08:00 AM #0] 
	       dbo.fn_DateFormat(ISNULL(tv.CourtDateMain, ''), 27, '/', ':', 1) + 
	       ' @ ' + dbo.fn_DateFormat(ISNULL(tv.CourtDateMain, ''), 26, '/', ':', 1) 
	       + ' #' + ISNULL(tv.CourtNumbermain, '0') AS Verified_Court_Info,
	       -- Babar Ahmad 9223 10/06/2011 Verified Court Status e.g Disposed/Waiting/App
	       s_verified.Description AS Verified_status, 
	       -- Babar Ahmad 9223 10/13/2011 Auto court date, time and room number e.g [06/02/2018 @ 08:00 AM #0]
	       (
	           dbo.fn_DateFormat(tvl.newautoDate, 27, '/', ':', 1) + ' @ ' + dbo.fn_DateFormat(tvl.newautoDate, 26, '/', ':', 1) 
	           + ' #' + tvl.newautoroom
	       ) AS Auto_Court_Info,
	       -- Babar Ahmad 9223 10/06/2011 Auto Court Status e.g Disposed/Waiting/App
	       l_verified.[Description] AS Auto_status, 
	       tv.casenumassignedbycourt AS CauseNumber,
	       tv.TicketsViolationID,
	       tv.bondamount,
	       tv.fineamount,
	       c.shortname AS shortname,
	       tv.Planid AS Planid,
	       tv.underlyingbondflag AS bondflag,
	       tv.ViolationNumber_PK AS ViolationNumber_PK,
	       tv.courtviolationstatusid AS Vrs,
	       s_verified.categoryid AS vtypeid,
	       tv.courtid AS courtid,
	       tv.OscarCourtDetail AS OscarCourtDetail,
	       tv.CourtDateMain AS CourtDateMain_1,
	       tv.courtnumbermain AS courtnumbermain,
	       v.Description + (
	           CASE 
	                WHEN tv.violationnumber_pk = 0 THEN ''
	                ELSE ' ' + '(' + ISNULL(vc.ShortDescription, 'N/A') + ')'
	           END
	       ) AS violationDescription,
	       ISNULL(s_verified.shortdescription, 'N/A') AS VDesc,
	       ISNULL(a_verified.shortdescription, 'N/A') AS ADesc,
	       c.iscriminalcourt,
	       ISNULL(tv.chargelevel, 0) AS chargelevel,
	       tv.cdi AS cdi,
	       dbo.fn_CheckFTAViolation(tv.ticketsviolationid) AS isFTAViolation,
	       dbo.fn_CheckFTAViolationInCase(t.TicketID_PK) AS hasFTAViolations,
	       CASE 
	            WHEN (
	                     SELECT COUNT(Fid)
	                     FROM   tblticketsflag
	                     WHERE  flagid = 15
	                            AND ticketid_pk = t.ticketid_pk
	                 ) = 0 THEN 0
	            ELSE 1
	       END AS hasNOs,
	       tv.CoveringFirmId AS CoveringFirmId,
	       tv.ArrestDate AS ArrestDate,
	       ISNULL(t.casetypeid, 0) AS casetypeid,
	       ISNULL(tv.MissedCourtType, -1) AS missedcourttype
	FROM   tblTickets AS t,		   
		   tblTicketsViolations AS tv,
		   tblCourtViolationStatus AS s_verified,
		   tblCourtViolationStatus AS a_verified,
		   tblCourtViolationStatus AS l_verified,
		   tblcourts c,
		   tblViolations v,
		   tblViolationCategory vc ,
	      (
		   		SELECT DISTINCT l.TicketviolationID,MAX(l.Recdate) AS Recdate 
				FROM tblTicketsViolationLog l 
				WHERE l.EmployeeID = 3992
				GROUP BY l.TicketviolationID
	      ) AS tvll,
	      -- Babar Ahmad 9223 10/13/2011 Getting max Verified Court Date and Auto Court Date
	     (
		   		SELECT DISTINCT l.TicketviolationID,MAX(l.newverifiedDate) AS newverifiedDate,l.newverifiedstatus,l.NewverifiedRoom,MAX(l.newautoDate) AS newautoDate,l.newautostatus,l.newautoroom,l.EmployeeID,l.newCourtid , MAX(l.Recdate) AS Recdate 
				FROM tblTicketsViolationLog l 
				WHERE l.EmployeeID = 3992
				GROUP BY l.TicketviolationID,l.newverifiedstatus,l.NewverifiedRoom,l.newautostatus,l.newautoroom,l.EmployeeID,l.newCourtid
	      ) AS tvl
	      
	        
	-- Babar Ahmad 9223 10/03/2011 Retrieve records with future court date.            
	WHERE  ISNULL(t.Activeflag, 0) = 1
		   AND tv.TicketID_PK = t.TicketID_PK
		   AND tv.CourtViolationStatusIDmain = s_verified.CourtViolationStatusID
		   AND tv.CourtViolationStatusID = a_verified.CourtViolationStatusID
		   AND l_verified.CourtViolationStatusID = tvl.newautostatus
		   AND  c.courtid = tv.courtid
		   AND tv.ViolationNumber_PK = v.ViolationNumber_PK
		   AND  v.CategoryID = vc.CategoryId
		   AND tvl.TicketviolationID = tv.TicketsViolationID
		   AND tvl.TicketviolationID = tvll.TicketviolationID
		   AND tvl.Recdate = tvll.Recdate	       
	       AND ISNULL(tv.NoChangeInCourtSetting, 0) <> 1
	       AND DATEDIFF(DAY, tv.CourtDateMain, GETDATE()) < 0
		   AND DATEDIFF(DAY, tvl.newautoDate, GETDATE()) < 0
				AND 
				(
	                    tvl.newautoDate <> tv.CourtDateMain 
	                    OR tvl.newautoroom <> tv.CourtNumbermain
	                    OR tvl.newCourtid <> tv.CourtID
	                    OR tvl.newautostatus <> tv.CourtViolationStatusIDmain
				)
		   -- Babar Ahmad 10/18/2011 Employee should not be SYSTEM	
		   AND tv.VEmployeeID <> 3992

order by t.TicketID_PK





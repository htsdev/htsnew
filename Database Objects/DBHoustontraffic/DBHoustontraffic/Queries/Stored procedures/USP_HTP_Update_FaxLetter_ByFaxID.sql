USE TrafficTickets
GO
/****** Object:  StoredProcedure [dbo].[USP_HTP_Update_FaxLetter_ByFaxID]    Script Date: 11/15/2011 19:14:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*    
Created By : Muhammad Nasir    
Business Logic : This procedure update records in fax Letter Table against FaxID
Parameter	: faxid  
			: @empid
			: @ename
			: @eemail
			: @body
			: @SystemId
Task ID: 6013
			
*/  

ALTER PROCEDURE [dbo].[USP_HTP_Update_FaxLetter_ByFaxID]
	@FaxID INT = 22220,
	@ticketid int=22220,    
	@empid INT = 3991,
	@ename VARCHAR(200),
	@eemail VARCHAR(200),		
	@FaxNo VARCHAR(200),
	@body VARCHAR(500),					
	@SystemId INT,
	@SubjectNote varchar(1000)
	
AS


--------------------------------------------------------------------------------------------------------------------
--Sabir Khan 8227 01/21/2011 Associating PMC Rep Date for Faxes....
--Sabir Khan 8899 7/14/2011 Replace the repeated code in function for Getting spacefic date for PMC court location...
	DECLARE @selectedCourtDate DATETIME
--Zeeshan Haider 11273 07/12/2013 If the PMC Rep date is already assigned to a tickets this shouldn't be updated if LOR Fax is re-sent b/c of any reason.
	DECLARE @existingCourtDate DATETIME	
	SET @existingCourtDate = '1/1/1900'
	SELECT @existingCourtDate = ISNULL(fl.PMCRepDate,'1/1/1900') FROM faxLetter fl
	INNER JOIN tblTicketsViolations v ON v.TicketID_PK = fl.Ticketid
	WHERE fl.FaxLetterid = @FaxID AND v.CourtID = 3004
	IF DATEDIFF(DAY, '1/1/1900', @existingCourtDate) > 0
		BEGIN
			SET @selectedCourtDate = @existingCourtDate
		END
	ELSE
		BEGIN
			SET @selectedCourtDate = TrafficTickets.[dbo].[fn_GetRepDateForPMC](@ticketid)			
		END	
-----------------------------------------------------------------------------------------------------------------------	

	UPDATE faxLetter
	SET    Employeeid = @empid,
	       EmployeeName = @ename,
	       EmployeeEmail = @eemail,	       	       
	       Body = @body,	       	       	       
	       SystemId = @SystemId,
	        ResentCount =  (
	           SELECT ResentCount + 1
	           FROM   faxLetter
	           WHERE  FaxLetterid = @FaxID
	       ),
	       LastResentFaxdate = GETDATE(),
	       PMCRepDate = @selectedCourtDate
	WHERE  FaxLetterid = @FaxID
		
	
set @SubjectNote = Replace(@SubjectNote,'{key}',@FaxID)  
set @SubjectNote = Replace(@SubjectNote,'{number}',@FaxNo)
	
	INSERT INTO TrafficTickets.dbo.tblTicketsNotes(EmployeeID,[Subject],Notes,TicketID)
	VALUES(@empid,@SubjectNote,@SubjectNote,@ticketid)
GO

GRANT EXECUTE ON [dbo].[USP_HTP_Update_FaxLetter_ByFaxID]  TO dbr_webuser
Go
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_GET_OSCARMAILFLAG]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_GET_OSCARMAILFLAG]
GO


CREATE PROCEDURE USP_HTS_GET_OSCARMAILFLAG
@TicketID int
as
Select isnull(OscarMailFlag,0) as OscarMailFlag from tbltickets where ticketid_pk = @TicketID

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTP_Insert_FindByDetail]    Script Date: 01/02/2014 02:23:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** 
Create by:  Sabir Khan
Task ID: 11509
Business Logic : This procedure is used to insert find by detail in client profile.

******/

ALTER PROCEDURE [dbo].[USP_HTP_Insert_FindByDetail]      
@TicketiID INT,
@FindByID INT,
@EmployeeID INT,     
@FindBy VARCHAR(500)
as      

DECLARE @ID INT
SET @ID = NULL

IF NOT EXISTS(SELECT ID FROM FindBy WHERE findby = LTRIM(RTRIM(@FindBy)))
BEGIN
		INSERT INTO FindBy(FindBy,InsertedBy,UpdatedBy)
		VALUES(@FindBy,@EmployeeID,@EmployeeID)	
		SET @ID = SCOPE_IDENTITY()
END 

IF(@ID IS NULL)
BEGIN
	IF @FindByID <> 0
		BEGIN
			SELECT @ID = @FindByID	
		END
	ELSE
		BEGIN
			SELECT @ID = ID FROM FindBy WHERE findby = LTRIM(RTRIM(@FindBy))	
		END	
END

IF NOT EXISTS(SELECT TicketID FROM FindByClientDetail WHERE TicketID = LTRIM(RTRIM(@TicketiID)))
BEGIN
	INSERT INTO FindByClientDetail(TicketID,FindByID,InsertedBy,UpdatedBy)
	VALUES(@TicketiID,@ID,@EmployeeID,@EmployeeID)
	
	INSERT INTO tblTicketsNotes(TicketID,[Subject],EmployeeID)
	VALUES(@TicketiID,'Client find out about Sullo & Sullo LLP through ' + @FindBy,@EmployeeID)
END
ELSE
BEGIN
	IF(@ID <> (SELECT TOP 1 FindByID FROM FindByClientDetail WHERE TicketID = LTRIM(RTRIM(@TicketiID))))
	BEGIN
		UPDATE FindByClientDetail	
		SET	FindByID = @ID,UpdateDate = GETDATE(),UpdatedBy = @EmployeeID 
		WHERE TicketID = @TicketiID
		
		INSERT INTO tblTicketsNotes(TicketID,[Subject],EmployeeID)
		VALUES(@TicketiID,'Client find out about Sullo & Sullo LLP through ' + @FindBy,@EmployeeID)	
	END
	
END  



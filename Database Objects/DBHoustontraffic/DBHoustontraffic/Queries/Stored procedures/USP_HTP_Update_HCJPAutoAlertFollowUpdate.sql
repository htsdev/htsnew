﻿ /************************************************************
 * Code formatted by SoftTree SQL Assistant © v4.0.34
 * Time: 9/24/2010 6:44:03 PM
 ************************************************************/


/****** Object:  StoredProcedure [dbo].[USP_HTP_Update_NonHMCFollowUpdate]  ******/     
/*        
Created By		: Saeed Ahmed.
Created Date	: 09/24/2010  
TasK			: 8101        
Business Logic  : This procedure Updates HCJP Auto Update Alert Follow Up Date.      
           
Parameter:       
   @TicketID     : Updating criteria with respect to TicketId      
   @FollowUpDate : Date of follow Up which has to be set     
     
      
*/      


CREATE PROCEDURE dbo.USP_HTP_Update_HCJPAutoAlertFollowUpdate
	@TicketID INT,
	@FollowUpDate DATETIME
AS
BEGIN
    IF EXISTS(
           SELECT *
           FROM   tblticketsextensions
           WHERE  Ticketid_pk = @TicketID
       )
    BEGIN
        UPDATE tblticketsextensions
        SET    HCJPAutoAlertFollowUpDate = @FollowUpDate
        WHERE  TicketID_PK = @TicketID
    END
    ELSE
    BEGIN
        INSERT INTO tblticketsextensions
          (
            Ticketid_pk,
            HCJPAutoAlertFollowUpDate
          )
        VALUES
          (
            @TicketID,
            @FollowUpDate
          )
    END
END
GO
GRANT EXECUTE ON dbo.USP_HTP_Update_HCJPAutoAlertFollowUpdate TO dbr_webuser
GO

      
CREATE PROCEDURE [dbo].[USP_Mailer_Get_ViolationByCategory]              
@CategoryID int              
              
AS              
              
SELECT     distinct Description              
FROM         dbo.tblViolations              
WHERE     CategoryID =@CategoryID              
--AND  violationtype in(0)  -- 0 means violations to be assigned              
AND       ViolationNumber_PK !=0 -- not select 1 record (--Choose--)            
AND    Violationtype=2       
AND    ISNULL(Description,'') <> ''      
order by   description 

GO

grant exec on [dbo].[USP_Mailer_Get_ViolationByCategory] to dbr_webuser  
Go

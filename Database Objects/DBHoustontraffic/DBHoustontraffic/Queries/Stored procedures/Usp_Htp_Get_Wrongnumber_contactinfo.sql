/****** 
Alter by:  Noufil khan
Business Logic : this procedure is used to get the contact information with the cleint having 
					* firstname,lastname and DOB same
					* midnumber same

******/

  
-- [dbo].[Usp_Htp_Get_Wrongnumber_contactinfo] 61501
CREATE procedure [dbo].[Usp_Htp_Get_Wrongnumber_contactinfo]
-- prameter ticketid through which records will show  
@ticketid int  
as   

-- This procedure show the contact information of the client that have same (firstname,lastname and DOB ) or  same midnumber
select  tCopy.ticketid_pk as ticketid, (tCopy.address1+','+' '+tCopy.city+','+' '+ ts.state+','+' '+tCopy.zip)as Address, ( tCopy.lastname + ',' + ' '+ tCopy.firstname) as fullname,  

	case when isnull(tCopy.Contact1,'') <> '' then convert(varchar(20),ISNULL(dbo.formatphonenumbers(tCopy.Contact1),''))+' '+'('+ISNULL(LEFT(Ct1.Description, 1),'')+')' else '' end as contact1,
	case when isnull(tCopy.Contact2,'') <> '' then convert(varchar(20),ISNULL(dbo.formatphonenumbers(tCopy.Contact2),''))+' '+'('+ISNULL(LEFT(Ct2.Description, 1),'')+')' else '' end as contact2, 
	case when isnull(tCopy.Contact3,'') <> ''then convert(varchar(20),ISNULL(dbo.formatphonenumbers(tCopy.Contact3),''))+' '+'('+ISNULL(LEFT(Ct3.Description, 1),'')+')' else '' end as contact3
  
     
     
from tbltickets tMain, tbltickets tCopy  
inner join   
   tblstate ts  
    on ts.stateid=tCopy.stateid_fk  
-- inner join tblticketsviolations tv
--on tCopy.ticketid_pk = tv.ticketid_pk 
--tv.refcasenumber as TicketNUmber ,
  
LEFT OUTER JOIN   
   dbo.tblContactstype ct2   
    ON tCopy.ContactType2 = ct2.ContactType_PK  
  LEFT OUTER JOIN   
   dbo.tblContactstype ct1   
    ON tCopy.ContactType1 = ct1.ContactType_PK   
  LEFT OUTER JOIN   
   dbo.tblContactstype ct3   
    ON tCopy.ContactType3 = ct3.ContactType_PK   
  
where   
tMain.ticketId_pk =@ticketid  
AND tCopy.ticketId_pk <> @ticketid  
  
And   
(  
(tMain.lastName = tCopy.LastName  
AND tMain.FirstName = tCopy.FirstName  
AND datediff(Day, tMain.DOB,tCopy.DOB) = 0)  
  
or tMain.midnum = tCopy.midnum  
)  


go

GRANT EXEC ON [Usp_Htp_Get_Wrongnumber_contactinfo] to dbr_webuser
go



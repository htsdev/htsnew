/******************

Alter BY : Zeeshan Ahmed
  
BUSINESS LOGIC : This procedure is used to set violation status to Waiting
				 If Criminal Violation status changed to waiting than set Criminal Followup date.

LIST OF INPUT PARAMETERS :

 @TicketViolationIDs : Comma Seperated TicketViolationId's 
 @EmpID : Employee ID 

*******************/  


Alter PROCEDURE usp_hts_docketcloseout_WaitingStatus  
@TicketViolationIDs varchar(2000),  
@EmpID int  
as  


declare @id as int 
declare @table as table ( rowid int identity(1,1) , TicketViolationid int ) 

set @id =1  

Insert into @table ( TicketViolationid ) 
select * from dbo.Sap_String_Param(@TicketViolationIDs) 


while  ( @id <= (Select Count(*) from @table ))
Begin

	declare 
		@Ticketid as int,
		@Statusid as int ,
		@TicketViolationId as int,
		@Courtid as int 

		--Get Ticket Violation ID 
		Select @TicketViolationId = TicketViolationid from @table where rowid = @id

		--Get Ticketid , Previous Court And Status Id , 
		Select @Ticketid = Ticketid_pk , @Statusid = CourtViolationStatusIdmain ,  @Courtid = Courtid
		from tblticketsviolations 
		where TicketsViolationID = @TicketViolationId

		-- If Criminal Violation Than Set Criminal Followup Date
		if ( @Statusid != 104 and (Select IsCriminalCourt From tblcourts  where courtid = @CourtId ) = 1 )
		Begin
				
			declare @followupdate as datetime 
			--Waqas 6342 08/19/2009 Follow up date set to coming friday. And Next friday is today is friday
			Set @followupdate = dbo.fn_get_next_coming_day(getdate(),6)			
						
			update tbltickets set criminalfollowupdate = @followupdate where ticketid_pk = @Ticketid	
			
			declare @note as varchar(100)
			Set @note = 'Criminal Follow Up Date : Follow Up Date has been changed to ' +  Convert(varchar,@Followupdate,101)
			exec usp_HTS_Insert_CaseHistoryInfo @Ticketid ,@note,@EmpID,''
	
		END
		
		-- If Criminal Violation Than Set Criminal Followup Date
		if ( @Statusid != 104 and (Select c.CaseTypeID From tblcourts c where courtid = @CourtId ) = 4 )
		Begin
				
			--Waqas 6342 08/19/2009 Follow up date set to coming friday. And Next friday is today is friday
			Set @followupdate = dbo.fn_get_next_coming_day(getdate(),6)			
						
			update tbltickets set trafficWaitingFollowUpDate = @followupdate where ticketid_pk = @Ticketid	
			
			Set @note = 'Waiting Follow Up Date : Follow Up Date has been changed to ' +  Convert(varchar,@Followupdate,101)
			exec usp_HTS_Insert_CaseHistoryInfo @Ticketid ,@note,@EmpID,''
	
		End
		
		--Update Status To Waiting
		update tblticketsviolations       
		set
			courtviolationstatusidmain = 104,      
		  courtviolationstatusid = 104,      
		  courtviolationstatusidscan = 104,      
		  courtnumber = courtnumbermain,      
		  courtnumberscan = courtnumbermain,      
		  courtdate = courtdatemain,      
		  courtdatescan = courtdatemain,      
		  vemployeeid = @EmpID             
		WHERE          
		  TicketsViolationID = @TicketViolationId

		set @id= @id + 1 
End


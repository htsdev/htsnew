set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


/***
CREATED BY: ZEESHAN ZAHOOR ON 09/11/2008
BUSINESS LOGIC:


LIST OF PARAMETERS:

LIST OF COLUMNS:
****/



CREATE PROCEDURE [dbo].[USP_HTS_GETCONTINOUSOPTIONS]
AS
SELECT * FROM CONTINUANCEOPTIONS      

grant exec on [dbo].[USP_HTS_GETCONTINOUSOPTIONS] to dbr_webuser


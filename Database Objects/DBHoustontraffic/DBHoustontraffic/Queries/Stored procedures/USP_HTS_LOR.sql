set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

/**************

Business Logic:  --Yasir Kamal 5683 03/25/2009 Business Logic added.

The stored procedure is used by HTP /Matter /Billing /Bonds (Bonds Letter).

Input Parameters:  
@TicketID
 
Output Columns:
TicketId
TicketNumber
CauseNumber
ViolationDescription
LastName
FirstName
MidNumber
DOB
BondAmount  

*********/

-- exec [dbo].[USP_HTS_LOR] 124546
ALTER procedure [dbo].[USP_HTS_LOR] 
@TicketID int  
As  
Begin  
  
SELECT  TOP 100 PERCENT 
	dbo.tblTicketsViolations.TicketID_PK as TicketID, 
	dbo.tblTicketsViolations.RefCaseNumber As TicketNumber,   
	dbo.tblTicketsViolations.casenumassignedbycourt AS CauseNumber, 
	UPPER(dbo.tblViolations.Description) AS ViolationDescription, -- Fahad 5908 05/16/2009 change violation description into upper case.   
	dbo.tblTickets.Lastname as LastName, 
	dbo.tblTickets.Firstname as FirstName, 
	dbo.tblTickets.Midnum as MidNumber, 
	dbo.tblTickets.DOB as DOB,
-- tahir 5683 03/20/2009 removed bond amount for hmc courts....
--(case when dbo.tblTicketsViolations.courtid IN (3001, 3002, 3003) then dbo.tblTicketsViolations.FineAmount else dbo.tblTicketsViolations.BondAmount end) as BondAmount  
(case when dbo.tblTicketsViolations.courtid IN (3001, 3002, 3003) then 0 else dbo.tblTicketsViolations.BondAmount end) as BondAmount  
 FROM          dbo.tblTickets RIGHT OUTER JOIN  
                      dbo.tblTicketsViolations ON dbo.tblTickets.TicketID_PK = dbo.tblTicketsViolations.TicketID_PK LEFT OUTER JOIN  
                      dbo.tblCourtViolationStatus ON dbo.tblTicketsViolations.CourtViolationStatusID = dbo.tblCourtViolationStatus.CourtViolationStatusID LEFT OUTER JOIN  
                      dbo.tblViolations ON dbo.tblTicketsViolations.ViolationNumber_PK = dbo.tblViolations.ViolationNumber_PK  
 Where (dbo.tblTicketsViolations.TicketID_PK = @TicketID)  
			and (dbo.tblTicketsViolations.underlyingbondflag=1 or dbo.tblTicketsViolations.courtviolationstatusid=135)  
			and dbo.tblTicketsViolations.courtviolationstatusid<>80  
 ORDER BY dbo.tblTicketsViolations.TicketID_PK, dbo.tblTicketsViolations.CourtDateMain DESC  
  
End  



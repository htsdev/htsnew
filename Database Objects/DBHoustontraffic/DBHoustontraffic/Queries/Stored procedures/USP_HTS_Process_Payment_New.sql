USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTS_Process_Payment_New]    Script Date: 01/02/2014 03:48:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[USP_DTP_Update_NonHMCFollowUpdate]  ******/     
/*        
Created By     : Fahad Muhammad Qureshi.
Created Date   : 07/25/2009  
TasK		   : 6054        
Business Logic : This procedure is used to Process the payment of the client in Billing Section.
				         
Parameter:       
	@PaymentType	: Mean Or method of the Payment by which payment will take place.
	@Ticketid		: Id on basis of we are fetching information from the database
	@ChargeAmount	: Extra Charge amount against of Violations of the clients.
	@EmployeeID		: introduce the employe info
	@scheduleid		: Id by which findout the Schedules of the payment of the Client
	@processflag	: Flag in Client profile
	@PaymentStatusID: what is current status of the Client's payment
	@PaymentReference:There are severl reference of payments we have which identify with the payment method.
        
*/

  
ALTER PROCEDURE [dbo].[USP_HTS_Process_Payment_New] 

	@PaymentType INT,
	@Ticketid INT,
	@ChargeAmount FLOAT,
	@EmployeeID INT,
	@scheduleid INT,
	@processflag INT,
	@PaymentStatusID INT,
	@PaymentReference VARCHAR(50)='', --Fahad 6054 07/25/2009 New parameter @PaymentReference added 
	@PaymentIdentifier INT = 0, --Afaq 8213 11/02/2010 New parameter added.
	@branchID INT, -- Sabir Khan 10920 05/27/2013 Branch Id added.
	@IPAddress VARCHAR(30) ='', -- Sabir Khan 10920 05/27/2013 IP Address of client machine.
	@HowHired INT = 0
	
AS
	DECLARE @refundamt AS MONEY           
	DECLARE @activeflag AS INT        
	DECLARE @owes AS MONEY          
	DECLARE @inviceNo INT
	SELECT @activeflag = activeflag
	FROM   tbltickets
	WHERE  ticketid_pk = @Ticketid 
	
	--declare @empabb as varchar(10)
	--When processing either refund amt or schedule amt or entered amt when schedule count=0   perfect scenerio                          
	IF (@paymenttype = 8 OR @paymenttype = 103)
	BEGIN
	    SET @refundamt = @chargeamount * -1          
	    INSERT INTO tblticketspayment
	      (
	        PaymentType,
	        Ticketid,
	        ChargeAmount,
	        EmployeeID,
	        paymentIdentifier,
	        BranchID,
	        IPAddress	        
	      )
	    VALUES
	      (
	        @PaymentType,
	        @Ticketid,
	        @refundamt,
	        @EmployeeID,
	        @PaymentIdentifier, --Afaq 8213 11/02/2010 New parameter @paymentIdentifier added in the insert statement.  
	        @branchID, 
	        @IPAddress	                                         
	      )	      
	      SET @inviceNo = SCOPE_IDENTITY()                                      
	    
	    IF (@paymenttype = 8)
	    BEGIN
	        INSERT INTO tblticketsnotes
	          (
	            ticketid,
	            SUBJECT,
	            recdate,
	            EmployeeID
	          )
	        VALUES
	          (
	            @Ticketid,
	            'Refund $' + CONVERT(VARCHAR(20), @ChargeAmount),
	            GETDATE(),
	            @EmployeeID
	          )
	    END  
	    
	    IF (@paymenttype = 103)
	    BEGIN
	        INSERT INTO tblticketsnotes
	          (
	            ticketid,
	            SUBJECT,
	            recdate,
	            EmployeeID
	          )
	        VALUES
	          (
	            @Ticketid,
				'Bounce Check $' + CONVERT(VARCHAR(20), @ChargeAmount),
	            GETDATE(),
	            @EmployeeID
	          )
	    END
	END
	ELSE
	    --if amount is not refund
	BEGIN
	    INSERT INTO tblticketspayment
	      (
	        PaymentType,
	        Ticketid,
	        ChargeAmount,
	        EmployeeID,
	        PaymentReference,--Fahad 6054 07/25/2009 New parameter PaymentReference added 
	        paymentIdentifier,
	         BranchID,
	        IPAddress	        
	      )
	    VALUES
	      (
	        @PaymentType,
	        @Ticketid,
	        CONVERT(MONEY, @ChargeAmount),
	        @EmployeeID,
	        @PaymentReference,--Fahad 6054 07/25/2009 New parameter @PaymentReference added 
	        @PaymentIdentifier, --Afaq 8213 11/02/2010 New parameter @paymentIdentifier added in the insert statement.  
	        @branchID, 
	        @IPAddress                                
	      )
	       
	    SET @inviceNo = SCOPE_IDENTITY()
	    
	    --Updating schedule flag to zero of selected scheduleid's                        
	    DECLARE @amt MONEY --cursor variable                      
	    DECLARE @diff AS MONEY                       
	    DECLARE @id AS INT 
	    
	    --In order to set schedule flag=0 of schedule amount with min payment date fall under Charge Amount                       
	    
	    DECLARE abc CURSOR  
	    FOR
	        SELECT amount,
	               scheduleid
	        FROM   tblschedulepayment
	        WHERE  ticketid_pk = @Ticketid
	               AND scheduleflag = 1
	        ORDER BY
	               paymentdate 
	    
	    OPEN abc 
	    
	    FETCH NEXT FROM abc 
	    INTO @amt,@id                       
	    
	    WHILE (@@FETCH_STATUS = 0)
	    BEGIN
	        PRINT ' @ChargeAmount  ' + CONVERT(VARCHAR(10), @ChargeAmount) +
	        ' @amt ' + CONVERT(VARCHAR(10), @amt)        
	        IF (@ChargeAmount >= @amt) --Till ChargeAmount is not < 0
	        BEGIN
	            --print 'andar aagaya hu main  @ChargeAmount>=@amt '        
	            
	            UPDATE tblschedulepayment
	            SET    scheduleflag = 0
	            WHERE  scheduleid = @id
	            
	            SET @ChargeAmount = @ChargeAmount -@amt --setting new charge amt
	        END
	        ELSE 
	        IF (@amt >= @ChargeAmount AND @ChargeAmount > 0)
	        BEGIN
	            UPDATE tblschedulepayment
	            SET    amount = ABS(@ChargeAmount -@amt)
	            WHERE  scheduleid = @id        
	            
	            SET @ChargeAmount = 0
	        END 
	        
	        FETCH NEXT FROM abc 
	        INTO @amt,@id
	    END 
	    CLOSE abc 
	    DEALLOCATE abc        
	    
	    
	    DECLARE @ScheduleDate AS DATETIME        
	    SET @ScheduleDate = GETDATE() + 15        
	    
	    SELECT @owes = ISNULL(
	               SUM(DISTINCT t.TotalFeeCharged) - ISNULL(SUM(tp.ChargeAmount), 0),
	               0
	           ) - ISNULL(
	               (
	                   SELECT SUM(Amount)
	                   FROM   dbo.tblSchedulePayment AS sp
	                   WHERE  (TicketID_PK = t.TicketID_PK)
	                          AND (ScheduleFlag = 1)
	               ),
	               0
	           )
	    FROM   dbo.tblTickets AS t
	           LEFT OUTER JOIN dbo.tblTicketsPayment AS tp
	                ON  t.TicketID_PK = tp.TicketID
	                AND tp.PaymentVoid <> 1
	                AND tp.PaymentType <> 99
	    GROUP BY
	           t.TicketID_PK
	    HAVING (t.TicketID_PK = @Ticketid)        
	    
	    
	    
	    IF @owes > 0
	    BEGIN
	        --print @owes        
	        EXEC USP_HTS_Insert_Edit_SchedulePayment @Ticketid,
	             @owes,
	             @ScheduleDate,
	             @EmployeeID,
	             0
	    END
	END 
	
	--update tbltickets for partial pay report.        
	
	
	--- Set Active Flag to false when whole paid amount is refund         
	DECLARE @totalamount MONEY  
	
	IF (@paymenttype = 8)
	BEGIN
	    SELECT @totalamount = ISNULL(SUM(ISNULL(chargeamount, 0)), 0)
	    FROM   tblticketspayment
	    WHERE  ticketid = @Ticketid
	           AND paymenttype NOT IN (99, 100)
	           AND paymentvoid = 0              
	    
	    IF (@totalamount = 0)
	        UPDATE tbltickets
	        SET    activeflag = 0
	        WHERE  ticketid_pk = @Ticketid
	END 
	-- Noufil 5618 04/09/2009 Update payment follow update and add note in history.
	DECLARE @oldfollowupdate DATETIME
	SET @oldfollowupdate = (
	        SELECT ISNULL(tt.PaymentDueFollowUpDate, '01/01/1900')
	        FROM   tblTickets tt
	        WHERE  tt.TicketID_PK = @Ticketid)
	
	DECLARE @oldfollowvarchar VARCHAR(20) 
	SET @oldfollowvarchar = CASE @oldfollowupdate
	                             WHEN '01/01/1900' THEN 'N/A'
	                             ELSE CONVERT(VARCHAR(20), @oldfollowupdate, 101)
	                        END
	
	
	DECLARE @NewPaymentFollowupdate DATETIME
	SET @NewPaymentFollowupdate = (
	        SELECT MIN(TSP.PaymentDate)
	        FROM   tblSchedulePayment tsp
	        WHERE  TSP.TicketID_PK = @ticketid
	               AND DATEDIFF(DAY, TSP.PaymentDate, GETDATE()) < 0
	               AND TSP.ScheduleFlag = 1
	    )
	
	IF (@NewPaymentFollowupdate <> @oldfollowupdate)
	BEGIN
	    UPDATE tblTickets
	    SET    PaymentDueFollowUpDate = @NewPaymentFollowupdate
	    WHERE  TicketID_PK = @ticketid
	    
	    INSERT INTO tblticketsnotes
	      (
	        ticketid,
	        SUBJECT,
	        employeeid,
	        Notes
	      )
	    SELECT @TicketID,
	           'Payment Follow Update has been changed from ' + @oldfollowvarchar
	           + ' to ' + CONVERT(VARCHAR(12), @NewPaymentFollowupdate, 101),
	           (
	               SELECT TOP 1 ttv.VEmployeeID
	               FROM   tblTicketsViolations ttv
	               WHERE  ttv.TicketID_PK = @ticketid
	           ),
	           NULL
	    FROM   tblusers U
	    WHERE  employeeid = (
	               SELECT TOP 1 ttv.VEmployeeID
	               FROM   tblTicketsViolations ttv
	               WHERE  ttv.TicketID_PK = @ticketid
	           )
	END
	
	IF @PaymentType = 104 --Fahad 6054 08/19/2009 associate firm with billing page to Contact Page
	BEGIN
		DECLARE @FirmID INT
		SET @FirmID = 0
		
		SELECT top 1 @FirmID = ttp.PaymentReference FROM tblTicketsPayment ttp WHERE ttp.TicketID = @TicketID AND ttp.PaymentType = 104 AND ttp.PaymentVoid = 0 ORDER BY ttp.RecDate DESC
		IF @FirmID <> 0
		BEGIN
			UPDATE tbltickets SET FirmID = @FirmID WHERE TicketID_PK = @TicketID --Fahad 6054 08/19/2009 associate firm with billing page to Contact Page
		END 	
	END
	
	--Sabir Khan 11509 11/12/2013
	IF(@HowHired <> 0)
	BEGIN
		DECLARE @HiredVia VARCHAR(100)
		SELECT @HiredVia = HiredVia FROM HowHired WHERE ID = @HowHired
		INSERT INTO HowHiredClientDetail(HowHiredID,TicketID,InsertedBy,UpdatedBy,InvoiceNumber_fk)
		VALUES(@HowHired,@Ticketid,@EmployeeID,@EmployeeID,@inviceNo)
		
		INSERT INTO tblTicketsNotes(TicketID,[Subject],EmployeeID)
		VALUES(@Ticketid,'Hires as' + @HiredVia,@EmployeeID)
	END
	

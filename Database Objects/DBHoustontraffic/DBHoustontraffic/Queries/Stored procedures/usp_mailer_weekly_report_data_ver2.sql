/****** 
Created by:		Tahir Ahmed
Create Date:	18/06/2007

Business Logic:	The procedure is used by LMS application to get the returns on the selected mailer type.

				
List of Parameters:
	@catnum:		Category number of the selected letter type, if printing for 
					all courts of the selected court category
	@LetterType:	Selected letter type in LMS Reports
	@sDate:			Starting date for search criteria.
	@eDate:			Ending date for search criteria.
	@SearchType:	Flag that identifies if searching by upload date or by mailer date. 0= upload date, 1 = mailer date

List of Columns:	
	mdate:			Maier Date or upload date as selected in the search criteria.
	lettercount:	Number of letters printed
	nonclient:		Number of people not hired/contacted us
	client:			Number of people hired us.
	revenue:		Revenue generated from the hired people.
	expense:		Expenses occured on mailing.
	profit:			Profit earned by the maiing.
	NPM:			The profit margin tells you how much profit a company makes for every $1 in generates in revenue.  In our case, the net profit margin tells us how much revenue is generated per dollar of postage spent. NPM = Profits / Revenue
	ROP:			Return on postage tells us how much profit is earned per $1 of postage spent.  ROP = Profit / Expense
	QPMP:			Quotes per Mail Piece tells us how many quotes are given per mail piece sent.  QPMP = Quote / Mailer Count
	HPMP:			Hires per Mail Piece tells us how many clients hire per mail piece sent.  HPMP = Hire / Mailer Count
	PPMP:			Profit per Mail Piece �PPMP� tells us how much money is earned per mail piece sent.  PPMP = Profits / Mailer Count
	CPMP:			It is ratio of CALLS to mailer count and tells us how many people contacting us per mail piece. CPMP = (Hires + Calls) / Mailer Count
	HPQ:			It is ratio of HIRES to CALLS and tells us how many people hired us per call. HPQ = Hires / (Hires + Quote)
	CPL:			CPL stands for �Cost Per Lead� and denotes how much a lead cost for that particular mailer. CPL = Expenses / (Hired + Quotes)
	CPH:			CPH stands for �Cost Per Hire� and denotes how much it cost to get a client for that particular mailer. CPH = Expense / Hired
******/

-- usp_mailer_weekly_report_data_ver2 3037, 26, '05/07/2008', '05/27/2008', 0
ALTER PROCEDURE [dbo].[usp_mailer_weekly_report_data_ver2]
(
    @CatNum      INT,
    @lettertype  INT,
    @sDate       DATETIME,
    @eDate       DATETIME,
    @SearchType  TINYINT
)
AS
	SET NOCOUNT ON 
	--Yasir Kamal 7218 01/19/2010 lms structure changes.
	DECLARE @DBid INT
	-- 1 = Traffic Tickets
	-- 2 = Dallas Traffic Tickets
	
	SELECT @dbid = CASE ISNULL(tl.courtcategory, 0)
	                    WHEN 26 THEN 5
	                    ELSE ISNULL(tcc.LocationId_FK, 0)
	               END
	FROM   tblletter tl
	       INNER JOIN tblCourtCategories tcc
	            ON  tl.courtcategory = tcc.CourtCategorynum
	WHERE  letterid_pk = @lettertype
	
	SELECT @dbid = ISNULL(@dbid, 0)
	
	DECLARE @tblcourts TABLE (courtid INT)
	
	----------------------------------------------------------------------------------------------------
	--		DALLAS DATABASE....
	----------------------------------------------------------------------------------------------------
	IF @dbid = 2
	BEGIN
	    IF @catnum < 1000
	        INSERT INTO @tblcourts
	        SELECT courtid
	        FROM   dallastraffictickets.dbo.tblcourts
	        WHERE  courtcategorynum = @catnum
	    ELSE
	        INSERT INTO @tblcourts
	        SELECT @catnum
	END----------------------------------------------------------------------------------------------------
	   --		TRAFFIC TICKETS DATABASE....
	   ----------------------------------------------------------------------------------------------------
	ELSE
	BEGIN
	    IF @catnum < 1000
	        INSERT INTO @tblcourts
	        SELECT courtid
	        FROM   dbo.tblcourts
	        WHERE  courtcategorynum = @catnum
	    ELSE
	        INSERT INTO @tblcourts
	        SELECT @catnum
	END
	----------------------------------------------------------------------------------------------------		
	
	
	-- GETTING LETTER HISTORY INTO TEMP TABLE AS PER SELECTION CRITERIA.....
	DECLARE @letters TABLE (noteid INT, mdate DATETIME)
	IF @searchtype = 0
	BEGIN
	    INSERT INTO @letters
	    SELECT noteid,
	           CONVERT(VARCHAR(10), listdate, 101)
	    FROM   tblletternotes n,
	           @tblcourts c
	    WHERE  n.courtid = c.courtid
	           AND n.lettertype = @lettertype
	           AND DATEDIFF(DAY, listdate, @sdate) <= 0
	           AND DATEDIFF(DAY, listdate, @edate) >= 0
	END-- Rab Nawaz Khan 10391	09/07/2012 Added the Logic for the Immigraion Letter. . .
	ELSE 
	IF ((@SearchType = 1) AND (@lettertype = 130))
	BEGIN
	    INSERT INTO @letters
	    SELECT noteid,
	           CONVERT(VARCHAR(10), recordloaddate, 101)
	    FROM   tblletternotes n
	    WHERE  n.lettertype = @lettertype
	           AND DATEDIFF(DAY, recordloaddate, @sdate) <= 0
	           AND DATEDIFF(DAY, recordloaddate, @edate) >= 0
	END-- END 10391
	ELSE
	BEGIN
	    INSERT INTO @letters
	    SELECT noteid,
	           CONVERT(VARCHAR(10), recordloaddate, 101)
	    FROM   tblletternotes n,
	           @tblcourts c
	    WHERE  n.courtid = c.courtid
	           AND n.lettertype = @lettertype
	           AND DATEDIFF(DAY, recordloaddate, @sdate) <= 0
	           AND DATEDIFF(DAY, recordloaddate, @edate) >= 0
	END
	-- GETTING COST FOR EACH MAILER/LIST DATE....
	DECLARE @cost TABLE (mdate DATETIME, pcost MONEY)
	INSERT INTO @cost
	SELECT l.mdate,
	       SUM(n.pcost)
	FROM   tblletternotes n
	       INNER JOIN @letters l
	            ON  l.noteid = n.noteid
	GROUP BY
	       l.mdate
	
	-- GETTING INFORMATION FROM CLIENTS/QUOTES......
	DECLARE @QuoteClient TABLE (
	            ticketid INT,
	            mailerid INT,
	            activeflag INT,
	            hiredate DATETIME,
	            quotedate DATETIME,
	            chargeamount MONEY
	        )
	
	IF @dbid = 2
	    INSERT INTO @QuoteClient
	    SELECT t.ticketid_pk,
	           t.mailerid,
	           t.activeflag,
	           MIN(p.recdate),
	           t.recdate,
	           ISNULL(SUM(ISNULL(p.chargeamount, 0)), 0)
	    FROM   dallastraffictickets.dbo.tbltickets t
	           LEFT OUTER JOIN dallastraffictickets.dbo.tblticketspayment p
	                ON  p.ticketid = t.ticketid_pk
	                AND p.paymentvoid = 0
	           INNER JOIN @letters l
	                ON  l.noteid = t.mailerid
	    GROUP BY
	           t.ticketid_pk,
	           t.mailerid,
	           t.activeflag,
	           t.recdate
	ELSE
	    INSERT INTO @QuoteClient
	    SELECT t.ticketid_pk,
	           t.mailerid,
	           t.activeflag,
	           MIN(p.recdate),
	           t.recdate,
	           ISNULL(SUM(ISNULL(p.chargeamount, 0)), 0)
	    FROM   tbltickets t
	           LEFT OUTER JOIN tblticketspayment p
	                ON  p.ticketid = t.ticketid_pk
	                AND p.paymentvoid = 0
	           INNER JOIN @letters l
	                ON  l.noteid = t.mailerid
	    GROUP BY
	           t.ticketid_pk,
	           t.mailerid,
	           t.activeflag,
	           t.recdate
	
	-- SUMMARIZING THE INFORMATION....
	DECLARE @temp2 TABLE
	        (
	            mdate DATETIME,
	            letterCount INT,
	            NonClient INT,
	            Client INT,
	            Quote INT,
	            Revenue MONEY,
	            expense MONEY,
	            weekno INT
	        )
	
	INSERT INTO @temp2
	  (
	    mdate,
	    lettercount,
	    client,
	    Quote,
	    revenue
	  )
	SELECT l.mdate,
	       COUNT(DISTINCT l.noteid),
	       SUM(CASE q.activeflag WHEN 1 THEN 1 END),
	       SUM(CASE q.activeflag WHEN 0 THEN 1 END),
	       SUM(q.chargeamount)
	FROM   @letters l
	       LEFT OUTER JOIN @QuoteClient q
	            ON  l.noteid = q.mailerid
	GROUP BY
	       l.mdate
	
	UPDATE t
	SET    t.expense = p.pcost,
	       nonclient = lettercount -(ISNULL(quote, 0) + ISNULL(client, 0))
	FROM   @temp2 t,
	       @cost p
	WHERE  DATEDIFF(DAY, t.mdate, p.mdate) = 0
	
	--Modify by Mohammad Ali
	--Ids concatenates..
	CREATE TABLE #temp3
	(
		dates       VARCHAR(20),
		ClientsIds  VARCHAR(MAX)
	)
	INSERT INTO #temp3
	  (
	    dates,
	    ClientsIds
	  )
	SELECT CONVERT(VARCHAR, p1.mdate, 101),
	       (
	           SELECT CONVERT(VARCHAR, noteid) + ','
	           FROM   @letters p2
	                  LEFT OUTER JOIN @QuoteClient q
	                       ON  p2.noteid = q.mailerid
	           WHERE  p2.mdate = p1.mdate
	                  AND q.activeflag = 1
	           ORDER BY
	                  mdate
	                  FOR XML PATH('')
	       ) AS ids
	FROM   @letters p1
	       LEFT OUTER JOIN @QuoteClient q
	            ON  p1.noteid = q.mailerid
	WHERE  q.activeflag = 1
	GROUP BY
	       p1.mdate
	
	
	------- Non Client ids...
	
	CREATE TABLE #temp4
	(
		dates          VARCHAR(20),
		nonClientsIds  VARCHAR(MAX)
	)
	INSERT INTO #temp4
	  (
	    dates,
	    nonClientsIds
	  )
	SELECT CONVERT(VARCHAR, p1.mdate, 101),
	       (
	           SELECT CONVERT(VARCHAR, noteid) + ','
	           FROM   @letters p2
	                  LEFT OUTER JOIN @QuoteClient q
	                       ON  p2.noteid = q.mailerid
	           WHERE  p2.mdate = p1.mdate
	                  AND q.activeflag = 0
	           ORDER BY
	                  mdate
	                  FOR XML PATH('')
	       ) AS ids
	FROM   @letters p1
	       LEFT OUTER JOIN @QuoteClient q
	            ON  p1.noteid = q.mailerid
	WHERE  q.activeflag = 0
	GROUP BY
	       p1.mdate
	
	CREATE TABLE #temp5
	(
		date        VARCHAR(20),
		clientsids  VARCHAR(MAX),
		nonclients  VARCHAR(MAX)
	)
	INSERT INTO #temp5
	  (
	    date,
	    clientsids,
	    nonclients
	  )
	SELECT t.mdate,
	       t3.ClientsIds,
	       t4.nonClientsIds
	FROM   @temp2 t
	       LEFT OUTER JOIN #temp3 t3
	            ON  t3.dates = t.mdate
	       LEFT OUTER JOIN #temp4 t4
	            ON  t4.dates = t.mdate
	
	DECLARE @dt1 DATETIME,
	        @dt2 DATETIME
	
	DECLARE @wk INT,
	        @t1 INT,
	        @dw INT
	
	DECLARE @table TABLE (
	            mdate DATETIME,
	            weekday INT,
	            weekno INT,
	            sdate DATETIME,
	            edate DATETIME,
	            week_range VARCHAR(50)
	        )
	
	
	
	--Waqas Javed 5313 12/30/08 breaking in Week Wise
	SELECT @wk = 1,
	       @dt1 = @sdate --, @dt2 = @sdate + 6, 
	
	IF (@edate > DATEADD(wk, DATEDIFF(wk, 0, @sdate), 5))
	BEGIN
	    SELECT @dt2 = DATEADD(wk, DATEDIFF(wk, 0, @sdate), 5),
	           @t1 = 0 --Set last day of Week
	END
	ELSE
	BEGIN
	    SELECT @dt2 = @edate,
	           @t1 = 0 --Set end date
	END
	WHILE DATEDIFF(DAY, @sdate, @edate) >= 0
	BEGIN
	    SELECT @dw = DATEPART(dw, @sDate)
	    IF @dw < @t1
	    BEGIN
	        IF (@edate > @sdate + 6)
	        BEGIN
	            SELECT @wk = @wk + 1,
	                   @dt1 = @sdate,
	                   @dt2 = @sdate + 6
	        END
	        ELSE
	        BEGIN
	            SELECT @wk = @wk + 1,
	                   @dt1 = @sdate,
	                   @dt2 = @edate
	        END
	    END
	    
	    INSERT INTO @table
	      (
	        mdate,
	        [weekday],
	        weekno,
	        sdate,
	        edate
	      )
	    SELECT @sDate,
	           @dw,
	           @wk,
	           @dt1,
	           @dt2
	    
	    SELECT @t1 = @dw
	    SET @sDate = @sDate + 1
	END
	--5313
	UPDATE @table
	SET    week_Range = CONVERT(VARCHAR(10), sdate, 1) + ' - ' + CONVERT(VARCHAR(10), edate, 1)
	
	UPDATE t1
	SET    t1.weekno = t2.weekno
	FROM   @temp2 t1
	       INNER JOIN @table t2
	            ON  t1.mdate = t2.mdate
	
	SELECT b.weekno AS mdate2,
	       b.week_range AS mdate,
	       b.sdate AS sdate,
	       b.edate AS edate,
	       ISNULL(SUM(lettercount), 0) AS lettercount,
	       ISNULL(SUM(ISNULL(nonclient, 0)), 0) AS nonclient,
	       ISNULL(SUM(ISNULL(client, 0)), 0) AS client,
	       ISNULL(SUM(ISNULL(quote, 0)), 0) AS quote,
	       ISNULL(SUM(ISNULL(revenue, 0)), 0) AS revenue,
	       ISNULL(SUM(ISNULL(expense, 0)), 0) AS expense,
	       ISNULL(SUM(ISNULL(revenue, 0) - expense), 0) AS profit,
	       CASE 
	            WHEN @DBid = 1 THEN 1
	            ELSE 2
	       END AS ProjectType
	       INTO #temp
	FROM   @temp2 a
	       INNER JOIN #temp5 t5
	            ON  a.mdate = t5.date
	       INNER JOIN @table b
	            ON  a.mdate = b.mdate
	GROUP BY
	       b.weekno,
	       b.week_range,
	       b.sdate,
	       b.edate
	ORDER BY
	       b.weekno DESC 
	
	--Mohammad Ali 9997 01/13/2012  removing same weeks apearing multiple times (with different mailer numbers).
	
	SELECT mdate2,
	       mdate,
	       sdate,
	       edate,
	       SUM(lettercount) AS lettercount,
	       SUM(nonclient) AS nonclient,
	       SUM(client) AS client,
	       SUM(quote) AS quote,
	       SUM(revenue) AS revenue,
	       SUM(expense) AS expense,
	       SUM(profit) AS profit,
	       NPM = SUM(
	           CONVERT(
	               NUMERIC(10, 3),
	               CASE 
	                    WHEN ISNULL(revenue, 0) = 0 THEN 0
	                    ELSE (ISNULL(revenue, 0) - expense) / revenue
	               END
	           )
	       ),
	       ROP = SUM(
	           CONVERT(
	               NUMERIC(10, 3),
	               CASE 
	                    WHEN ISNULL(expense, 0) = 0 THEN 0
	                    ELSE (ISNULL(revenue, 0) - expense) / expense
	               END
	           )
	       ),
	       QPMP = SUM(
	           CONVERT(
	               NUMERIC(10, 3),
	               CAST(ISNULL(quote, 0) / CAST(lettercount AS FLOAT) AS FLOAT)
	           )
	       ),
	       HPMP = SUM(
	           CONVERT(
	               NUMERIC(10, 3),
	               CAST(ISNULL(client, 0) / CAST(lettercount AS FLOAT) AS FLOAT)
	           )
	       ),
	       PPMP = SUM(
	           CONVERT(
	               NUMERIC(10, 3),
	               (ISNULL(revenue, 0) - expense) / CAST(lettercount AS FLOAT)
	           )
	       ),
	       -- tahir 4465 07/31/2008 added two new columns to the reports...
	       cpmp = SUM(
	           CONVERT(
	               NUMERIC(10, 3),
	               CAST((ISNULL(client, 0) + ISNULL(quote, 0)) AS FLOAT) / CAST(lettercount AS FLOAT)
	           )
	       ),
	       hpq = SUM(
	           CONVERT(
	               NUMERIC(10, 3),
	               CASE 
	                    WHEN (ISNULL(client, 0) + ISNULL(quote, 0)) = 0 THEN 0
	                    ELSE CAST(ISNULL(client, 0) AS FLOAT) / CAST((ISNULL(client, 0) + ISNULL(quote, 0)) AS FLOAT)
	               END
	           )
	       ),
	       --Zeeshan Haider 11426 10/01/2013 Added CPL,CPH ratios
	       cph = SUM(
	           CONVERT(
	               NUMERIC(10, 3),
	               CASE 
	                    WHEN ISNULL(client, 0) = 0 THEN 0
	                    ELSE CAST(ISNULL(expense, 0) AS FLOAT) / CAST(ISNULL(client, 0) AS FLOAT)
	               END
	           )
	       ),
	       cpl = SUM(
	           CONVERT(
	               NUMERIC(10, 3),
	               CASE 
	                    WHEN (ISNULL(client, 0) + ISNULL(quote, 0)) = 0 THEN 0
	                    ELSE CAST(ISNULL(expense, 0) AS FLOAT) / CAST((ISNULL(client, 0) + ISNULL(quote, 0)) AS FLOAT)
	               END
	           )
	       ),
	       COUNT(ProjectType) AS ProjectType 
	       INTO #temp2
	FROM   #temp
	GROUP BY
	       mdate2,
	       mdate,
	       sdate,
	       edate
	
	SELECT mdate,
	       sdate,
	       edate,
	       SUM(nonclient) AS nonclient,
	       SUM(client) AS client,
	       SUM(quote) AS quote,
	       SUM(revenue) AS revenue,
	       SUM(lettercount) AS lettercount,
	       SUM(expense) AS expense,
	       SUM(profit) AS profit,
	       SUM(npm) AS npm,
	       SUM(rop) AS rop,
	       SUM(qpmp) AS qpmp,
	       SUM(hpmp) hpmp,
	       SUM(ppmp) AS ppmp,
	       SUM(cpmp) AS cpmp,
	       SUM(hpq) AS hpq,
	       --Zeeshan Haider 11426 10/01/2013 Added CPL,CPH ratios
	       SUM(cph) AS cph,
	       SUM(cpl) AS cpl,
	       CASE 
	            WHEN @DBid = 1 THEN 1
	            ELSE 2
	       END AS ProjectType
	       INTO #tempFinal
	FROM   #temp2
	GROUP BY
	       mdate,
	       sdate,
	       edate
	
	ALTER TABLE #tempFinal ADD clientsids VARCHAR(MAX), nonclients VARCHAR(MAX)
	
	ALTER TABLE #temp5 ADD rowid INT IDENTITY(1, 1)	
	
	DECLARE @idx INT
	SET @idx = 1
	DECLARE @TotCount INT
	
	SELECT @TotCount = COUNT(DISTINCT Rowid)
	FROM   #temp5
	
	WHILE @idx <= @TotCount
	BEGIN
	    UPDATE TF
	    SET    TF.clientsids = ISNULL(TF.clientsids, '') + ISNULL(t2.clientsids, ''),
	           TF.nonclients = ISNULL(TF.nonclients, '') + ISNULL(t2.nonclients, '')
	    FROM   #tempFinal TF,
	           #temp5 t2
	    WHERE  t2.date >= TF.sdate
	           AND TF.edate >= t2.date
	           AND t2.Rowid = @idx
	    
	    SET @idx = @idx + 1
	END
	
	
	UPDATE #tempFinal
	SET    clientsids = NULL
	WHERE  LEN(clientsids) = 0
	
	UPDATE #tempFinal
	SET    nonclients = NULL
	WHERE  LEN(nonclients) = 0
	
	
	SELECT mdate,
	       nonclient,
	       client,
	       quote,
	       revenue,
	       lettercount,
	       expense,
	       profit,
	       npm,
	       rop,
	       qpmp,
	       hpmp,
	       ppmp,
	       cpmp,
	       hpq,
	       --Zeeshan Haider 11426 10/01/2013 Added CPL,CPH ratios
	       cph,
	       cpl,
	       CASE 
	            WHEN @DBid = 1 THEN 1
	            ELSE 2
	       END AS ProjectType,
	       clientsids,
	       nonclients
	FROM   #tempFinal
	-- End of 9997
	DROP TABLE #temp
	DROP TABLE #temp2

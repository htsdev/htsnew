USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTP_UPDATE_NisiFollowUpDate]    Script Date: 04/17/2013 20:32:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/**
-- Created By : Rab Nawaz
-- Task ID : 10729
-- Created Date : 04/15/2013
Business Logic : This Procedure is used to update the NISI Follow Up Date and NISI notes against clietns 
Parameter : @ticketid_pk : TicketID of Client
			@date : Bond Waiting Follow up Date which will be updated
			@empid: ID of Employee
**/



ALTER PROCEDURE [dbo].[USP_HTP_UPDATE_NisiFollowUpDate]
(
	@ticketid_pk	INT, 
	@date			DATETIME,
	@empid			INT,
	@ContactID		INT,
	@nisiNotes VARCHAR(1500),
	@isFreezed bit
)
AS
BEGIN
	
	DECLARE @OldFollowUpDate DATETIME
	SELECT @OldFollowUpDate = ISNULL(NisiFollowUpDate,'01/01/1900') FROM TrafficTickets.dbo.tbltickets WHERE TicketID_PK = @ticketid_pk
	
	IF ((@OldFollowUpDate IS NOT NULL) AND (DATEDIFF(DAY, @OldFollowUpDate, @date) <> 0))
	BEGIN
		-- Updating Nisi Follow Up Date against Client. 
		UPDATE tbltickets 
		SET NisiFollowUpDate = @date, IsNisiFolloupDateFreezed = @isFreezed
		WHERE ticketid_pk=@ticketid_pk
		
		INSERT INTO TrafficTickets.dbo.tblTicketsNotes (TicketID, [Subject], Recdate, EmployeeID) 
		VALUES (@ticketid_pk, 'NISI Follow up date has been changed to ' + CONVERT(VARCHAR(50), @date, 101), GETDATE(), @empid)
	END	
	
	ELSE
		BEGIN
			IF (DATEDIFF(DAY, @date, '01/01/1900') = 0)
			BEGIN
				UPDATE tbltickets 
				SET NisiFollowUpDate = NULL, IsNisiFolloupDateFreezed = @isFreezed
				WHERE ticketid_pk=@ticketid_pk
				
				
				INSERT INTO TrafficTickets.dbo.tblTicketsNotes (TicketID, [Subject], Recdate, EmployeeID) 
				VALUES (@ticketid_pk, 'NISI Follow up date has been changed to N/A', GETDATE(), @empid)
				
			END
		END
	
	-- Updating Contact Type Id against Client.
	UPDATE TrafficTickets.dbo.tblNISI_ClientsUploadedByLoader
	SET ContactTypeid = @ContactID
	WHERE TicketId = @ticketid_pk
	  
 SET @nisiNotes = LTRIM(RTRIM(@nisiNotes))   

 IF(rtrim(ltrim(@nisiNotes)) <> '')
	BEGIN  
	IF ((RIGHT(@nisiNotes, 1) <> ')')) --if comments are updated  
		BEGIN  
			DECLARE @empnm AS VARCHAR(12) 
			SELECT @empnm = abbreviation  
			FROM   tblusers  
			WHERE  employeeid = @empid   
			IF @empnm IS NULL  
				BEGIN  
					SET @empnm = 'N/A'  
				END  
       
			IF LEN(@nisiNotes) > 0
			SET @nisiNotes = @nisiNotes + ' ' + '(' + dbo.formatdateandtimeintoshortdateandtime(GETDATE()) + ' - ' + UPPER(@empnm) + ')' + ' '
			   
			UPDATE TrafficTickets.dbo.tbltickets  
			SET NisiNotes = ISNULL(NisiNotes, '') + @nisiNotes, employeeidupdate = @empid  
			WHERE  ticketid_pk = @TicketID_PK   
			INSERT INTO tblticketsnotes (ticketid, [subject],  recdate,  EmployeeID,  fk_commentid )  
			VALUES (@ticketid_pk,  'NISI Notes: ' + @nisiNotes,  GETDATE(),  @empid,  1)  
		END      
	END      	
END

GO
GRANT EXECUTE ON [dbo].[USP_HTP_UPDATE_NisiFollowUpDate] TO dbr_webuser
GO
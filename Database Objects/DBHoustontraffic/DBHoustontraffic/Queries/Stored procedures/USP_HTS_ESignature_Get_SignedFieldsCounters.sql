set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go




/*        
Altered By     : Fahad Muhammad Qureshi.
Created Date   : 04/23/2009  
TasK		   : 5806        
Business Logic : This procedure retrived all the Violations about the all Reports .  
           
Parameter:       
	@ticketID   : identifiable key  TicketId 
	@ClientLanguage: language of Client     
	@employeeid		: Employee which have Right to access the system
	@SessionID	: Session identifier 
	@PageCount: No of pages

*/  
--[USP_HTS_ESignature_Get_SignedFieldsCounters] '124546','English', '55777744882996955', 7, 0
ALTER PROCEDURE [dbo].[USP_HTS_ESignature_Get_SignedFieldsCounters] --'124546','English'
@TicketID as varchar(25),
@ClientLanguage as varchar(25),
@SessionID as varchar(50),
@PageCount as INT,
@HMC_HCJP AS INT =0
AS
	Declare @TotalViolations as int
	Declare @Receipt as varchar(40)
	Declare @BondDocs as varchar(40)
	Declare @TrialLetter as varchar(40)
	Declare @LetterOfRep as varchar(40)
	Declare @LetterOfContinuation as varchar(40)

	Exec USP_HTS_ESignature_Get_BondDoc_NoOfViolations @TicketID, @NoOfViolations = @TotalViolations Output
--***************************************************For Bond Report
	Set @BondDocs = (

	select 'Bond Docs ' + convert(varchar,count(ImageTrace)) + '/' + convert(varchar, (
	Select (Count(ImageTrace) * (@TotalViolations + 1) - (CASE WHEN @HMC_HCJP = 1 THEN (@TotalViolations + 1) ELSE 0 END) ) From Tbl_HTS_ESignature_Location -- +1 is for blank page signature
	Where KeyWord = 'Bond' And Entity = 'Client'
	)) 

	from Tbl_HTS_ESignature_Images 
	where imagetrace like '%Bond_Client%' and TicketID = @TicketID and SessionID = @SessionID)
--***************************************************
	--Set @Receipt = (select 'Receipt ' + convert(varchar, count(distinct ImageTrace) * @PageCount) + '/' + convert(varchar,(Select Count(ImageTrace) From Tbl_HTS_ESignature_Location Where KeyWord = 'Receipt' And Entity = 'Client') * @PageCount) from Tbl_HTS_ESignature_Images where imagetrace like '%Receipt_Client%' and TicketID = @TicketID And SessionID = @SessionID)
	Set @Receipt = (select 'Contract/Receipt ' + convert(varchar, count(distinct ImageTrace)) + '/' + convert(varchar,(Select Count(ImageTrace) From Tbl_HTS_ESignature_Location Where KeyWord = 'Receipt' And Entity = 'Client')) from Tbl_HTS_ESignature_Images where imagetrace like '%Receipt_Client%' and TicketID = @TicketID And SessionID = @SessionID)--Fahad 5806 04/24/2009 Column Header changed Reciept to Contract for Employment
--***************************************************
	Set @TrialLetter = (select 'Trial Letter ' + convert(varchar,count(ImageTrace)) + '/' + convert(varchar,(Select Count(ImageTrace) From Tbl_HTS_ESignature_Location Where KeyWord = 'Trial Letter' And Entity = 'Client')) from Tbl_HTS_ESignature_Images where imagetrace like '%LetterTrial_Client%' and TicketID = @TicketID And SessionID = @SessionID)
--***************************************************
	Set @LetterOfRep = (select 'Letter Of Rep ' + convert(varchar,count(ImageTrace)) + '/' + convert(varchar,(Select Count(ImageTrace) From Tbl_HTS_ESignature_Location Where KeyWord = 'Letter Of Rep' And Entity = 'Client')) from Tbl_HTS_ESignature_Images where imagetrace like '%LetterOfRep_Client%' and TicketID = @TicketID And SessionID = @SessionID)
--***************************************************
	Set @LetterOfContinuation = (select 'Letter Of Continuation ' + convert(varchar,count(ImageTrace)) + '/' + convert(varchar,(Select Count(ImageTrace) From Tbl_HTS_ESignature_Location Where KeyWord = 'Letter Of Continuation' And Entity = 'Client')) from Tbl_HTS_ESignature_Images where imagetrace like '%LetterContinuance_Client%' and TicketID = @TicketID And SessionID = @SessionID)
--***************************************************
	Select @Receipt As Receipt, @BondDocs As BondDocs, @TrialLetter As TrialLetter, @LetterOfRep As LetterOfRep, @LetterOfContinuation As LetterOfContinuation


















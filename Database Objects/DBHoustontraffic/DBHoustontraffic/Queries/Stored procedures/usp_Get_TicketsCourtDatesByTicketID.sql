SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_TicketsCourtDatesByTicketID]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_TicketsCourtDatesByTicketID]
GO

















CREATE PROCEDURE [dbo].[usp_Get_TicketsCourtDatesByTicketID] 
@TicketID_PK varchar(20)
AS

SELECT DISTINCT 
                      dbo.tblTicketsViolations.TicketID_PK, dbo.tblTicketsViolations.CourtViolationStatusIDmain, CONVERT(Varchar(20), 
                      dbo.tblTicketsViolations.CourtDateMain) + '  ' + dbo.tblCourtViolationStatus.Description AS CourtDatenStatus
FROM         dbo.tblTicketsViolations LEFT OUTER JOIN
                      dbo.tblCourtViolationStatus ON dbo.tblTicketsViolations.CourtViolationStatusIDmain = dbo.tblCourtViolationStatus.CourtViolationStatusID
WHERE     (dbo.tblTicketsViolations.TicketID_PK = @TicketID_PK)













GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


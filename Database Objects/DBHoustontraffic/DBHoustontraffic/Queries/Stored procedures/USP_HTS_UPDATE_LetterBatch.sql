/******
Business Logic : This procedure is used to Update Print Information About The Batch.    --Yasir Kamal 5489 17/02/2009 Business Logic Updated

List of Parameters:
@LetterID   = Type of  Letter  i.e Trial Notification Letter,Letter of Rep etc
@ticketidlist 
@TicketIDBatch
@PrintEmpId

******/

--USP_HTS_UPDATE_LetterBatch 2,'4550,','4550104120,',3991,'3262010 55634 AM','Trial Notification Letter.pdf'
       
ALTER PROCEDURE [dbo].[USP_HTS_UPDATE_LetterBatch]
	@LetterID INT,
	@ticketidlist VARCHAR(8000) = '0,',
	@TicketIDBatch VARCHAR(8000),
	@PrintEmpId INT,
	 --Yasir Kamal 7590 03/29/2010 trial letter modifications
	@Datetime VARCHAR(120) = '1/1/1900',
	@ReportName VARCHAR(50) = '',
	@Docpath AS	VARCHAR(120) = '' OUTPUT 	
	
	AS
	
	DECLARE @print AS VARCHAR(100),
	        @BatchID INT
	
	SET @ticketidlist = (
			--Farrukh 9885 12/09/2011 added Distinct keyword for getting unique records
	        SELECT dbo.Get_DistinctList(@ticketidlist, ',') DistinctList
	    )
	
	IF (@LetterID = 2)
	BEGIN
	    SET @print = 'Trial Letter Printed' 
	    SELECT @BatchID = MAX(batchid_pk)
	    FROM   tblHTSNotes th
	    
	    SET @BatchID = @BatchID + 1  
	    SELECT @Docpath = @Datetime + '-' + CONVERT(VARCHAR, @BatchID) + '-' + @ReportName
	END                              
	
	IF (@LetterID = 6)
	    SET @print = 'Letter of rep Printed'                              
	
	IF (@LetterID = 5)
	    SET @print = 'Continuance Letter Printed'                              
	
	IF (@LetterID = 3)
	    SET @print = 'Receipt Printed' 
	
	-- Haris Ahmed 10381 09/03/2012 Add Immigration Letter
	IF (@LetterID = 34)
	BEGIN
	    SET @print = 'Immigration Letter Printed'
	    SELECT @Docpath = hb.DocPath
	    FROM   dbo.tblHTSBatchPrintLetter hb
	           INNER JOIN dbo.tblHTSLetters hl
	                ON  hb.LetterID_FK = hl.LetterID_pk
	                AND hb.batchid_pk = (
	                        SELECT TOP 1 tbv.batchID_pk
	                        FROM   tblhtsbatchprintletter tbv
	                        WHERE  tbv.ticketid_fk = hb.ticketid_fk
	                               AND tbv.LetterID_Fk = @LetterID
	                               AND deleteflag <> 1
	                               AND ((@LetterID = 2) OR (@LetterID <> 2 AND tbv.isprinted <> 1))
	                               AND TicketID_FK  IN (SELECT *
	                                                    FROM   dbo.Sap_String_Param(@ticketidlist))
	                        ORDER BY
	                               batchid_pk DESC
	                    )
	END                             
	
	DECLARE @temp TABLE (
	            rowid INT IDENTITY(1, 1),
	            batchid_pk INT,
	            ticketid_fk INT,
	            recdate DATETIME,
	            letterid_fk INT,
	            empid INT,
	            notes VARCHAR(500),
	            docpath VARCHAR(1000),
	            lettername VARCHAR(100)
	        )              
	
	DECLARE @reccount INT,
	        @idx INT,
	        @recordid INT,
	        @lnk VARCHAR(500)
	
	SELECT @reccount = 0,
	       @idx = 1              
	
	
	
	BEGIN
	    INSERT INTO @temp
	    SELECT CASE 
	                WHEN @LetterID = 2 THEN @BatchID
	                ELSE hb.BatchID_PK
	           END,
	           hb.TicketID_FK,
	           GETDATE() AS RecDate,
	           hb.LetterID_FK,
	           hb.EmpID,
	           CASE 
	                WHEN @LetterID = 34 THEN 'Immigration Letter Printed'
	                ELSE 'Batch Print'
	           END AS Notes,
	           CASE 
	                WHEN @LetterID = 2 THEN @Docpath
	                ELSE hb.DocPath
	           END,
	           --7590 end              
	           hl.lettername
	    FROM   dbo.tblHTSBatchPrintLetter hb
	           INNER JOIN dbo.tblHTSLetters hl
	                ON  hb.LetterID_FK = hl.LetterID_pk
	                AND hb.batchid_pk = (
	                        SELECT TOP 1 tbv.batchID_pk
	                        FROM   tblhtsbatchprintletter tbv
	                        WHERE  tbv.ticketid_fk = hb.ticketid_fk
	                               AND tbv.LetterID_Fk = @LetterID
	                               AND deleteflag <> 1 
	                                   --Muhammad Ali 7571 07/28/2010 --> add Conditions for Electrnoic Mail and regular print.....
	                                   --and tbv.isprinted <> 1
	                               AND ((@LetterID = 2) OR (@LetterID <> 2 AND tbv.isprinted <> 1))
	                               AND TicketID_FK  IN (SELECT *
	                                                    FROM   dbo.Sap_String_Param(@ticketidlist))
	                        ORDER BY
	                               batchid_pk DESC
	                    )                   
	    
	    SELECT @recCount = COUNT(rowid)
	    FROM   @temp              
	    
	    WHILE @idx <= @reccount
	    BEGIN
	        -- Haris Ahmed 9885 Add check to view if entry exists in same day for same letter
	        DECLARE @RowCount AS INT
	        
	        -- Haris Ahmed 10381 09/03/2012 Add Immigration Letter
	        IF (@LetterID = 34)
	        BEGIN
	            SELECT @RowCount = COUNT(*)
	            FROM   @temp t
	            WHERE  t.rowid = @idx
	            
	            PRINT @RowCount
	            
	            INSERT INTO tblhtsnotes
	              (
	                BatchID_PK,
	                TicketID_FK,
	                PrintDate,
	                LetterID_FK,
	                EmpID,
	                Note,
	                DocPath
	              )
	            SELECT batchid_pk,
	                   ticketid_fk,
	                   recdate,
	                   letterid_fk,
	                   @PrintEmpId,
	                   notes,
	                   docpath
	            FROM   @temp t
	            WHERE  t.rowid = @idx
	            
	            SET @RecordID = SCOPE_IDENTITY()--@@identity
	        END
	        ELSE
	        BEGIN
	            SELECT @RowCount = COUNT(*)
	            FROM   @temp t
	            WHERE  t.rowid = @idx
	                   AND NOT EXISTS (
	                           SELECT *
	                           FROM   tblhtsnotes
	                           WHERE  tblhtsnotes.TicketID_FK = t.ticketid_fk
	                                  AND DATEDIFF(DAY, tblhtsnotes.PrintDate, t.recdate) = 
	                                      0
	                                  AND tblhtsnotes.LetterID_FK = t.letterid_fk
	                       )
	            
	            PRINT @RowCount
	            
	            INSERT INTO tblhtsnotes
	              (
	                BatchID_PK,
	                TicketID_FK,
	                PrintDate,
	                LetterID_FK,
	                EmpID,
	                Note,
	                DocPath
	              )
	            SELECT batchid_pk,
	                   ticketid_fk,
	                   recdate,
	                   letterid_fk,
	                   @PrintEmpId,
	                   notes,
	                   docpath
	            FROM   @temp t
	            WHERE  t.rowid = @idx 
	                   -- Haris Ahmed 9885 Add check to view if entry exists in same day for same letter
	                   AND NOT EXISTS (
	                           SELECT *
	                           FROM   tblhtsnotes
	                           WHERE  tblhtsnotes.TicketID_FK = t.ticketid_fk
	                                  AND DATEDIFF(DAY, tblhtsnotes.PrintDate, t.recdate) = 
	                                      0
	                                  AND tblhtsnotes.LetterID_FK = t.letterid_fk
	                       )           
	            
	            SET @RecordID = SCOPE_IDENTITY()--@@identity
	        END
	        
	        IF (@RowCount > 0)
	        BEGIN
	            -- Haris Ahmed 10381 09/03/2012 Add Immigration Letter
	            IF (@LetterID = 34)
	            BEGIN
	                INSERT INTO tblticketsnotes
	                  (
	                    ticketid,
	                    SUBJECT,
	                    employeeid
	                  )
	                SELECT TicketID_fk,
	                       '<a href="javascript:window.open(''frmPreview.aspx?RecordID=' + CONVERT(VARCHAR(20), @RecordID) + ''');void('''');">' + @print + '</a>',
	                       @PrintEmpId
	                FROM   @temp
	                WHERE  rowid = @idx
	            END
	            ELSE
	            BEGIN
	                INSERT INTO tblticketsnotes
	                  (
	                    ticketid,
	                    SUBJECT,
	                    employeeid
	                  )
	                SELECT TicketID_fk,
	                       '<a href="javascript:window.open(''frmPreview.aspx?RecordID=' + CONVERT(VARCHAR(20), @RecordID) + ''');void('''');">' + LetterName + ' Printed from Batch Print</a>',
	                       @PrintEmpId
	                FROM   @temp
	                WHERE  rowid = @idx
	            END
	            
	            
	            INSERT INTO tblticketsnotes
	              (
	                ticketid,
	                SUBJECT,
	                employeeid
	              )
	            SELECT tm.TicketID_fk,
						-- '<a href="javascript:window.open(''frmPreview.aspx?RecordID='+ convert(varchar(20),@RecordID)  +''');void('''');">' + tm.LetterName + ' Sent via email</a>',
	                   '<a href="javascript:window.open(''frmPreview.aspx?RecordID=' + CONVERT(VARCHAR(20), @RecordID) + ''');void('''');">' + 'Trial Letter' + '</a>'+ ' sent via email to ' + (SELECT Email FROM tblTickets WHERE TicketID_PK = tm.TicketID_fk),
	                   3992
	            FROM   @temp tm
	                   INNER JOIN tbltickets t
	                        ON  t.TicketID_PK = tm.ticketid_fk
	            WHERE  tm.rowid = @idx
	                   AND ISNULL(t.IsEmailedTrialLetter, 0) = 1
	                   AND DATEDIFF(DAY, t.TrialLetterEmailedDate, GETDATE()) = 
	                       0
	        END
	        
	        SET @idx = @idx + 1
	    END              
	    
	    DECLARE @tickets TABLE (ticketid BIGINT)            
	    
	    INSERT INTO @tickets
	    SELECT *
	    FROM   dbo.Sap_String_Param_bigint(@TicketIDBatch) 
	    
	    -- Haris Ahmed 10381 09/03/2012 Add Immigration Letter
	    IF (@LetterID = 34)
	    BEGIN
	        UPDATE tblhtsbatchprintletter
	        SET    ISPrinted = 1,
	               PrintDate = GETDATE(),
	               PrintEmpID = @PrintEmpId
	        WHERE  LetterID_Fk = @LetterID
	               AND deleteflag <> 1
	               AND ISPrinted = 0 
	               --and  TicketID_FK  IN (select * from dbo.Sap_String_Param( @ticketidlist ))
	               AND CAST(ticketid_fk AS BIGINT) IN (SELECT ticketid
	                                                   FROM   @tickets)
	    END
	    ELSE
	    BEGIN
	        UPDATE tblhtsbatchprintletter
	        SET    ISPrinted = 1,
	               PrintDate = GETDATE(),
	               PrintEmpID = @PrintEmpId
	        WHERE  LetterID_Fk = @LetterID
	               AND deleteflag <> 1 
	               --and  TicketID_FK  IN (select * from dbo.Sap_String_Param( @ticketidlist ))
	               AND CAST(
	                       CONVERT(VARCHAR(12), ticketid_fk) + CONVERT(VARCHAR(12), BatchID_PK) 
	                       AS BIGINT
	                   ) IN (SELECT ticketid
	                         FROM   @tickets)
	    END 
	    
	    --Yasir Kamal 5442 02/19/2009 set Isprintflag=1 of a case having no letter flag.
	    --and tblhtsbatchprintletter.ticketid_fk not in (select distinct ticketid_pk from tblticketsflag where flagid = 7)  --Added by Azwar
	    --5442 end
	    -- tahir 5489 02/03/2009 set jury status 1 for trial letters printed from batch....
	    IF @LetterID = 2
	    BEGIN
	        UPDATE t
	        SET    t.jurystatus = 1
	        FROM   tblhtsbatchprintletter bp
	               INNER JOIN tbltickets t
	                    ON  t.ticketid_pk = bp.ticketid_fk
	        WHERE  CAST(
	                   CONVERT(VARCHAR(12), bp.ticketid_fk) + CONVERT(VARCHAR(12), bp.BatchID_PK) 
	                   AS BIGINT
	               ) IN (SELECT ticketid
	                     FROM   @tickets)
	    END
	END       

USE [TrafficTickets]
GO
/******
* Business Logc: This procedure is used to get weekly payment detail by spacified transaction date and branch. 
* Altered by: Sabir Khan
* Task Id: 10920
* Date: 05/27/2013
******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER PROCEDURE [dbo].[usp_Get_All_tblPaymentDetailWeeklyByDate] 
@TransDate DATETIME,
@branchID INT =0 -- Sabir Khan 10920 05/27/2013 Branch Id added.
AS

IF(@branchID=1) 
BEGIN
	Select EmployeeID, TransDate, SUM(ActualCash) ActualCash , SUM(ActualCheck) ActualCheck,'' AS Notes
	From  tblPaymentDetailWeekly
	Where Convert(varchar(12),TransDate,101) = Convert(varchar(12), @TransDate,101)
	AND Branchid IN (SELECT branchid FROM fn_get_branchIDs(@branchID))
	GROUP BY EmployeeID,TransDate
END
ELSE IF (@branchID=2 OR @branchID=3)
BEGIN
	Select EmployeeID, TransDate, ActualCash,ActualCheck, Notes
	From  tblPaymentDetailWeekly
	Where Convert(varchar(12),TransDate,101) = Convert(varchar(12), @TransDate,101)
	AND Branchid IN (SELECT branchid FROM fn_get_branchIDs(@branchID))
END
ELSE
BEGIN
	Select EmployeeID, TransDate, ActualCash, ActualCheck, Notes
	From  tblPaymentDetailWeekly
	Where Convert(varchar(12),TransDate,101) = Convert(varchar(12), @TransDate,101)
END










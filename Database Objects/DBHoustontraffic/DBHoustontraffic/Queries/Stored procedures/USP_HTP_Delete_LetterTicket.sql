/*
Created By : Agha Usman
Business Logic : Update delete flag in batch print in respect of ticket id and letter id

List of Parameter: 
LetterID : Contain Letter Type
TicketId : Contain Ticket Id 

Created for Task Id 4165

*/



create procedure [dbo].[USP_HTP_Delete_LetterTicket]  
@LetterID int,    
@TicketId int  
as  
BEGIN  
  
update tblhtsbatchprintletter  
 set deleteflag = 1  
 where letterid_fk = @LetterID  
 and TicketId_Fk = @TicketId
  
END

go 

grant exec on [dbo].[USP_HTP_Delete_LetterTicket] to dbr_webuser 
go 


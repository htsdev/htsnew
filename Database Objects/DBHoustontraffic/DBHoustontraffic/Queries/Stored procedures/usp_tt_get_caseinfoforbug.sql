set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

  
create procedure [dbo].[usp_tt_get_caseinfoforbug] --3790 ,1    
@ticketID int  
as          
  
Select  top 1          
  t.ticketid_pk,          
  --t.firstname,          
  --t.middlename,          
  --t.lastname,          
  tv.casenumassignedbycourt,          
  --c.shortname,          
  --tcv.shortdescription,          
  --v.description,        
  tv.refcasenumber           
          
from TrafficTickets.dbo.tbltickets t inner join           
 TrafficTickets.dbo.tblticketsviolations tv on           
 t.ticketid_pk=tv.ticketid_pk       
where           
 t.ticketid_pk=@ticketID   


  
  
 
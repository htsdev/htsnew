

  
--Created By Zeeshan Ahmed
--Business Logic : This Procedure Update Fine Amount Of Violation By  Casuse Number 
    
Create Procedure dbo.USP_HTP_UPDATE_VIOLATION_FINEAMOUNT  
(  
@TicketID int,  
@CauseNumber varchar(20),  
@FineAmount decimal   
)  
  
as  
  
Update tblticketsviolations set  FineAmount = @FineAmount  
where casenumassignedbycourt = @CauseNumber and ticketid_pk = @TicketID  
  


GO


GRANT EXEC ON dbo.USP_HTP_UPDATE_VIOLATION_FINEAMOUNT TO dbr_webuser

GO



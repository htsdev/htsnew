SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Add_ViolationQuote]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Add_ViolationQuote]
GO







CREATE  Procedure usp_Add_ViolationQuote
@TicketID_FK int,
@QuoteResultID int,
@QuoteResultDate varchar(12),
@FollowUPID int,
@FollowUPDate varchar(12),
@FollowUpYN int,
@QuoteComments varchar(4000),
@CallBackDate varchar(12),
@AppointmentDate varchar(12),
@AppointmentTime varchar(12),
--@LastContactDate datetime,
@UNID numeric(18,0)=0 OUTPUT

AS
	BEGIN TRANSACTION
	SELECT @UNID=max(QuoteID)+ 1 from tblViolationQuote
	IF @UNID is NULL
	SET @UNID=1
	--SET @QuoteID= @UNID
SET NOCOUNT ON



Insert into 
tblViolationQuote
--(QuoteID, TicketID_FK,QuoteResultID,QuoteResultDate,FollowUPID, FollowUPDate, FollowUpYN, QuoteComments, CallBackDate, AppointmentDate, AppointmentTime, LastContactDate) 
(QuoteID, TicketID_FK,QuoteResultID,QuoteResultDate,FollowUPID, FollowUPDate, FollowUpYN, QuoteComments, CallBackDate, AppointmentDate, AppointmentTime) 

values
--(@UNID, @TicketID_FK,@QuoteResultID,@QuoteResultDate,@FollowUPID, @FollowUPDate, @FollowUpYN, @QuoteComments, @CallBackDate, @AppointmentDate, @AppointmentTime,@LastContactDate)
(@UNID, @TicketID_FK,@QuoteResultID,@QuoteResultDate,@FollowUPID, @FollowUPDate, @FollowUpYN, @QuoteComments, @CallBackDate, @AppointmentDate, @AppointmentTime)

COMMIT






GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


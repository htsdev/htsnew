﻿ 
  
  
/****** Object:  StoredProcedure [dbo].[USP_HTP_Update_NOLORConfirmationFollowUpdate]  ******/     
/*        
Created By		: Muhammad Nadir Siddiqui
Created Date	: 04/29/2011  
TasK			: 9134      
Business Logic  : This procedure Updates No LOR Confirmation Follow Up Date.      
Parameter:      : @FollowUpDate
				  @TicketID
@TicketID		: Updating criteria with respect to TicketId      
@FollowUpDate	: Date of follow Up which has to be set     
           
*/      
  
CREATE PROCEDURE [dbo].[USP_HTP_Update_NOLORConfirmationFollowUpdate]  
 @FollowUpDate DATETIME,  
 @TicketID INT  
AS  
 UPDATE tblTickets  
 SET    NoLORConfirmFollowUpDate = @FollowUpDate  
 WHERE  TicketID_PK = @TicketID  



GO
GRANT EXECUTE ON USP_HTP_Update_NOLORConfirmationFollowUpdate TO dbr_webuser 
﻿/******  
* Created By :	Fahad Muhammad Qureshi.
* Create Date :   10/19/2010 8:51:00 PM
* Task ID :				6766
* Business Logic :  This procedure is used to Get .
* List of Parameter :
* Column Return : 
******/
ALTER PROCEDURE [dbo].[USP_HTP_AddPatient]
	@TicketId INT,
	@LastName VARCHAR(50),
	@FirstName VARCHAR(50),
	@Address VARCHAR(50),
	@City VARCHAR(50),
	@State INT,
	@Zip VARCHAR(20),
	@DOB DATETIME,
	@Contact1 VARCHAR(15),
	@Contacttype1 INT,
	@Contact2 VARCHAR(15),
	@Contacttype2 INT,
	@Contact3 VARCHAR(15),
	@Contacttype3 INT,
	@Email VARCHAR(250),
	@IsClient BIT,
	@PatientStatus INT,
	@SurgeryDate DATETIME,
	@DoctorContacted BIT,
	@ContactComment VARCHAR(MAX),
	@IsSchedule BIT,
	@ScheduleDate DATETIME,
	@IsProblem BIT,
	@ProblemComments VARCHAR(MAX),
	@PainSeverity INT,
	@ManufacturerComments VARCHAR(MAX),
	@GeneralComments VARCHAR(MAX),
	@DoctorId INT,
	@EmployeeId INT,
	@PatientId INT,
	@HadBloodTest BIT,
	@BloodTestDate DATETIME,
	@BloodTestConductor VARCHAR(200),
	@AbnormalLevelsNoted BIT,
	@AbnormalMetalsFound VARCHAR(200),
	@SurgeryHospitalName VARCHAR(300),
	@SurgeryHospitalCity VARCHAR(100),
	@SurgeryHospitalState VARCHAR(100),
	@ImpactMaterial INT ,
	@WereScrewImplanted BIT,
	@ImplantDefect VARCHAR(500),
	@HasImplantRemoved BIT,
	@ImplantRemoveDate DATETIME,
	@WhyImplantRemoved VARCHAR(500),
	@ImplantRemoveHospName VARCHAR(300),
	@ImplantRemoveHospCity VARCHAR(100),
	@ImplantRemoveHospState VARCHAR(100),
	@ImplantDoctorName VARCHAR(200),
	@ImplantDoctorCity VARCHAR(100),
	@ImplantDoctorState VARCHAR(100),
	@medicalReleaseSent DATETIME,
	@medicalReleaseRecieved DATETIME,
	@clientContractSent DATETIME,
	@clientContractRecieved DATETIME,
	@medicalRecordRequestDate DATETIME,
	@medicalRecordRecievedDate DATETIME,
	@Occupation	varchar(100),
	@IsUnEmployed BIT,
	@LastJobLeftDate DATETIME,
	@LastJobLeftReason varchar(100)
AS
	DECLARE @Employee_ShortName VARCHAR(10)
	
	SELECT @Employee_ShortName = u.Abbreviation
	FROM   Users.dbo.[User] u
	WHERE  u.UserId = @EmployeeId
	
	IF (@PatientId = 0)
	BEGIN
	    IF @ContactComment <> ''
	        SET @ContactComment = @ContactComment + ' (' + dbo.fn_DateFormat(GETDATE(), 2, '/', ':', 1) 
	            + ' - ' + @Employee_ShortName + ')' 
	    
	    IF @ProblemComments <> ''
	        SET @ProblemComments = @ProblemComments + ' (' + dbo.fn_DateFormat(GETDATE(), 2, '/', ':', 1) 
	            + ' - ' + @Employee_ShortName + ')' 
	    
	    IF @ManufacturerComments <> ''
	        SET @ManufacturerComments = @ManufacturerComments + ' (' + dbo.fn_DateFormat(GETDATE(), 2, '/', ':', 1) 
	            + ' - ' + @Employee_ShortName + ')' 
	    
	    IF @GeneralComments <> ''
	        SET @GeneralComments = @GeneralComments + ' (' + dbo.fn_DateFormat(GETDATE(), 2, '/', ':', 1) 
	            + ' - ' + @Employee_ShortName + ')' 
	    
	    INSERT INTO Patient
	      (
	        TicketId,
	        LastName,
	        FirstName,
	        ADDRESS,
	        City,
	        STATE,
	        Zip,
	        DOB,
	        Contact1,
	        ContactType1,
	        Contact2,
	        ContactType2,
	        Contact3,
	        ContactType3,
	        Email,
	        IsClient,
	        PatientStatusId,
	        SurgeryDate,
	        DoctorContacted,
	        ContactComment,
	        IsSchedule,
	        ScheduleDate,
	        IsProblem,
	        ProblemComments,
	        PainSeverity,
	        ManufacturerComments,
	        GeneralComments,
	        DoctorId,
	        EmployeeId,
	        HadBloodTest,
	        BloodTestDate,
	        BloodTestConductor,
	        AbnormalLevelsNoted,
	        AbnormalMetalsFound,
	        p.SurgeryHospitalName,
	        p.SurgeryHospitalCity,
	        p.SurgeryHospitalState,
	        p.ImpactMaterial,
	        p.WereScrewImplanted,
	        p.ImplantDefect,
	        p.HasImplantRemoved,
	        p.ImplantRemoveDate,
	        p.WhyImplantRemoved,
	        p.ImplantRemoveHospName,
	        p.ImplantRemoveHospCity,
	        p.ImplantRemoveHospState,
	        p.ImplantDoctorName,
	        p.ImplantDoctorCity,
	        p.ImplantDoctorState,
	        p.MedicalReleaseSent,
	        p.MedicalReleaseRecieved,
	        p.ClientContractSent,
	        p.ClientContractRecieved,
	        p.MedicalRecordRequestDate,
	        p.MedicalRecordRecievedDate,
	        p.Occupation,
			p.IsUnEmployed,
			p.LastJobLeftDate,
			p.LastJobLeftReason
	      )
	    VALUES
	      (
	        @TicketId,
	        @LastName,
	        @FirstName,
	        @Address,
	        @City,
	        @State,
	        @Zip,
	        @DOB,
	        @Contact1,
	        @Contacttype1,
	        @Contact2,
	        @Contacttype2,
	        @Contact3,
	        @Contacttype3,
	        @Email,
	        @IsClient,
	        @PatientStatus,
	        @SurgeryDate,
	        @DoctorContacted,
	        @ContactComment,
	        @IsSchedule,
	        @ScheduleDate,
	        @IsProblem,
	        @ProblemComments,
	        @PainSeverity,
	        @ManufacturerComments,
	        @GeneralComments,
	        @DoctorId,
	        @EmployeeId,
	        @HadBloodTest,
	        @BloodTestDate,
	        @BloodTestConductor,
	        @AbnormalLevelsNoted,
	        @AbnormalMetalsFound,
	        @SurgeryHospitalName,
	        @SurgeryHospitalCity,
	        @SurgeryHospitalState,
	        @ImpactMaterial,
	        @WereScrewImplanted,
	        @ImplantDefect,
	        @HasImplantRemoved,
	        @ImplantRemoveDate,
	        @WhyImplantRemoved,
	        @ImplantRemoveHospName,
	        @ImplantRemoveHospCity,
	        @ImplantRemoveHospState,
	        @ImplantDoctorName,
	        @ImplantDoctorCity,
	        @ImplantDoctorState,
	        @medicalReleaseSent,
	        @medicalReleaseRecieved,
	        @clientContractSent,
	        @clientContractRecieved,
	        @medicalRecordRequestDate,
	        @medicalRecordRecievedDate,
	        @Occupation,
			@IsUnEmployed,
			@LastJobLeftDate,
			@LastJobLeftReason
	      )
	    
	    SELECT SCOPE_IDENTITY()
	END
	ELSE
	BEGIN
	    DECLARE @OldContactComment VARCHAR(MAX)
	    DECLARE @OldProblemComments VARCHAR(MAX)
	    DECLARE @OldManufacturerComments VARCHAR(MAX)
	    DECLARE @OldGeneralComments VARCHAR(MAX)
	    
	    SELECT @OldContactComment = p.ContactComment,
	           @OldProblemComments = p.ProblemComments,
	           @OldManufacturerComments = p.ManufacturerComments,
	           @OldGeneralComments = p.GeneralComments
	    FROM   Patient p
	    WHERE  p.PatientId = @PatientId
	    
	    IF (LEN(@ContactComment) > 0)
        BEGIN
        	SET @ContactComment = @ContactComment + ' (' + dbo.fn_DateFormat(GETDATE(), 2, '/', ':', 1) 
				+ ' - ' + @Employee_ShortName + ')'
		END
	            
	    IF (LEN(@ProblemComments) > 0)
        BEGIN
        	SET @ProblemComments = @ProblemComments + ' (' + dbo.fn_DateFormat(GETDATE(), 2, '/', ':', 1) 
				+ ' - ' + @Employee_ShortName + ')'
		END

	    IF (LEN(@ManufacturerComments) > 0)
        BEGIN
        	SET @ManufacturerComments = @ManufacturerComments + ' (' + dbo.fn_DateFormat(GETDATE(), 2, '/', ':', 1) 
				+ ' - ' + @Employee_ShortName + ')'
		END
				
	    IF (LEN(@GeneralComments) > 0)
        BEGIN
        	SET @GeneralComments = @GeneralComments + ' (' + dbo.fn_DateFormat(GETDATE(), 2, '/', ':', 1) 
				+ ' - ' + @Employee_ShortName + ')'
        END
	    
	    
	    UPDATE Patient
	    SET    TicketId = @TicketId,
	           LastName = @LastName,
	           FirstName = @FirstName,
	           [Address] = @Address,
	           City = @City,
	           [State] = @State,
	           Zip = @Zip,
	           DOB = @DOB,
	           Contact1 = @Contact1,
	           ContactType1 = @Contacttype1,
	           Contact2 = @Contact2,
	           ContactType2 = @Contacttype2,
	           Contact3 = @Contact3,
	           ContactType3 = @Contacttype3,
	           Email = @Email,
	           IsClient = @IsClient,
	           PatientStatusId = @PatientStatus,
	           SurgeryDate = @SurgeryDate,
	           DoctorContacted = @DoctorContacted,
	           ContactComment =ISNULL(ContactComment,'') + ' '+ ISNULL(@ContactComment,''),
	           IsSchedule = @IsSchedule,
	           ScheduleDate = @ScheduleDate,
	           IsProblem = @IsProblem,
	           ProblemComments = ISNULL(ProblemComments,'') + ' '+ ISNULL(@ProblemComments,''),
	           PainSeverity = @PainSeverity,
	           ManufacturerComments =ISNULL(ManufacturerComments,'') + ' '+ ISNULL(@ManufacturerComments,''), 
	           GeneralComments = ISNULL(GeneralComments,'') + ' '+ ISNULL(@GeneralComments,''),
	           DoctorId = @DoctorId,
	           EmployeeId = @EmployeeId,
	           HadBloodTest = @HadBloodTest,
	           BloodTestDate = @BloodTestDate,
	           BloodTestConductor = @BloodTestConductor,
	           AbnormalLevelsNoted = @AbnormalLevelsNoted,
	           AbnormalMetalsFound = @AbnormalMetalsFound,
	           SurgeryHospitalCity = @SurgeryHospitalCity,
	           SurgeryHospitalState = @SurgeryHospitalState,
	           ImpactMaterial = @ImpactMaterial,
	           WereScrewImplanted = @WereScrewImplanted,
	           ImplantDefect = @ImplantDefect,
	           HasImplantRemoved = @HasImplantRemoved,
	           ImplantRemoveDate = @ImplantRemoveDate,
	           WhyImplantRemoved = @WhyImplantRemoved,
	           ImplantRemoveHospName = @ImplantRemoveHospName,
	           ImplantRemoveHospCity = @ImplantRemoveHospCity,
	           ImplantRemoveHospState = @ImplantRemoveHospState,
	           ImplantDoctorName = @ImplantDoctorName,
	           ImplantDoctorCity = @ImplantDoctorCity,
	           ImplantDoctorState = @ImplantDoctorState,
	           MedicalReleaseSent = @medicalReleaseSent,
	           MedicalReleaseRecieved = @medicalReleaseRecieved,
	           ClientContractSent = @clientContractSent,
	           ClientContractRecieved = @clientContractRecieved,
	           MedicalRecordRequestDate = @medicalRecordRequestDate,
	           MedicalRecordRecievedDate = @medicalRecordRecievedDate,
	           Occupation = @Occupation,
			   IsUnEmployed = @IsUnEmployed,
			   LastJobLeftDate = @LastJobLeftDate,
			   LastJobLeftReason = @LastJobLeftReason,
			   LastContactDate = GETDATE()			  
	    WHERE  PatientId = @PatientId
	    
	    SELECT @PatientId
	END
GO



GRANT EXECUTE ON [dbo].[USP_HTP_AddPatient]  TO dbr_webuser
GO 	  
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Bug_InsertNewBug]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Bug_InsertNewBug]
GO




CREATE procedure [dbo].[usp_Bug_InsertNewBug]        
        
@ShortDescription as varchar(500),        
@EmployeeID int,        
@StatusID int,        
@PriorityID int,        
@PageUrl as varchar(500),        
@PostedDate as datetime ,        
@TicketID as varchar(500),        
@CauseNo as varchar(50),  
@BugID int=0 output        
        
as        
        
insert into tbl_bug        
(         
 ShortDescription,        
 EmployeeID,        
 StatusID,        
 PriorityID,        
 PageUrl,        
 PostedDate,        
 TicketID,        
 CauseNo  
)        
values        
(         
 @ShortDescription,        
 @EmployeeID,        
 @StatusID,        
 @PriorityID,        
 @PageUrl,        
 --@PostedDate,        
 getdate(),
 @TicketID ,       
 @CauseNo      
)        
        
set @BugID=@@identity  
  






GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


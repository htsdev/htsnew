GO
/****** Object:  StoredProcedure [dbo].[Usp_Hts_labelreport]    Script Date: 05/05/2008 09:09:12 ******/
/*  
Created By : Noufil Khan  
Business Logic : This procedure display information about all those clients whose tracking number is generated 
                 and thier XML response has been get and they are printed with Edelivery.

*/
--[dbo].[Usp_Hts_labelreport] '05/02/08' ,0
CREATE procedure [dbo].[Usp_Hts_labelreport]
@datefrom datetime,
@selectall bit
as


SELECT TOP (100) PERCENT           
    dbo.tbl_hts_batch_trialletter.ticketid_pk,
	dbo.tbl_hts_batch_trialletter.BatchID,           
    dbo.tblHTSBatchPrintLetter.LetterID_FK,      
    dbo.tbl_hts_batch_trialletter.LastName +', '+ dbo.tbl_hts_batch_trialletter.FirstName as [name],
    c.ShortName+' '+dbo.tbl_hts_batch_trialletter.CaseStatus+' '+ case when dbo.tbl_hts_batch_trialletter.CourtDate is null then '' else dbo.fn_dateformat(dbo.tbl_hts_batch_trialletter.CourtDate,29,'/',':',1) end+' '+ dbo.tbl_hts_batch_trialletter.CourtNumber as Coutdatetime,
	dbo.fn_dateformat(dbo.tblHTSBatchPrintLetter.BatchDate,29,'/',':',1)+' '+tblUsers_1.Abbreviation as Batchdate,
	case when dbo.tblHTSBatchPrintLetter.PrintDate is null then '' else dbo.fn_dateformat(printdate,29,'/',':',1) end +' '+ dbo.tblUsers.Abbreviation as printdatenew,   
    dbo.tblHTSBatchPrintLetter.USPSTrackingNumber as TrackingNumber,
	dbo.fn_dateformat(dbo.tblHTSBatchPrintLetter.PrintDate,29,'/',':',1) as printdate
      
          
FROM dbo.tblHTSBatchPrintLetter 
		LEFT OUTER JOIN   dbo.tblUsers 
				ON dbo.tblHTSBatchPrintLetter.PrintEmpID = dbo.tblUsers.EmployeeID 
		LEFT OUTER JOIN   dbo.tblUsers AS tblUsers_1 
				ON dbo.tblHTSBatchPrintLetter.EmpID = tblUsers_1.EmployeeID 
		RIGHT OUTER JOIN  dbo.tbl_hts_batch_trialletter 
				ON dbo.tblHTSBatchPrintLetter.BatchID_PK = dbo.tbl_hts_batch_trialletter.BatchID AND           
				   dbo.tblHTSBatchPrintLetter.TicketID_FK = dbo.tbl_hts_batch_trialletter.ticketid_pk 
		Inner Join dbo.tbltickets AS tbltickets 
				on dbo.tbl_hts_batch_trialletter.ticketid_pk = tbltickets.ticketid_pk 
		Inner Join tblcourts c 
				ON dbo.tbl_hts_batch_trialletter.CourtID = c.CourtID  
		left outer join tblcourtviolationstatus cs 
				on cs.CourtViolationStatusID = tbl_hts_batch_trialletter.courtviolationstatusid and cs.categoryid = 4      
WHERE     
      isnull(dbo.tblHTSBatchPrintLetter.deleteflag ,0) = 0      
  and isnull(dbo.tblHTSBatchPrintLetter.isprinted ,0) = 1
  and dbo.tblHTSBatchPrintLetter.USPSTrackingNumber is not null 
  and dbo.tblHTSBatchPrintLetter.USPSTrackingNumber <> '0'
  and (@selectall=1 or datediff(day,dbo.tblHTSBatchPrintLetter.PrintDate,@datefrom) = 0)
 
GROUP BY dbo.tbl_hts_batch_trialletter.ticketid_pk, dbo.tbl_hts_batch_trialletter.BatchID, dbo.tbl_hts_batch_trialletter.FirstName,           
                      dbo.tbl_hts_batch_trialletter.MiddleName, dbo.tbl_hts_batch_trialletter.LastName, dbo.tblHTSBatchPrintLetter.IsPrinted,           
                      dbo.tblHTSBatchPrintLetter.PrintDate, dbo.tblHTSBatchPrintLetter.BatchDate, dbo.tblHTSBatchPrintLetter.DeleteFlag, tblUsers_1.Abbreviation,           
                      c.ShortName, dbo.tbl_hts_batch_trialletter.CaseStatus, dbo.tblHTSBatchPrintLetter.LetterID_FK, dbo.tblUsers.Abbreviation,dbo.tbl_hts_batch_trialletter.CourtNumber,cs.ShortDescription,tbltickets.email,
					  tbltickets.bondflag,dbo.tblHTSBatchPrintLetter.USPSTrackingNumber,dbo.tbl_hts_batch_trialletter.CourtDate
             
ORDER BY dbo.tbl_hts_batch_trialletter.BatchID 

go
GRANT EXEC ON [dbo].[Usp_Hts_labelreport] to dbr_webuser
go 

﻿ /**
* Business Logic : This method returns CLients information whose Payment followupdate is in future
*					 THis method search records on the basis of number of future follow Update days set 
*Selected Columns
*	TicketID_PK
*	ClientName
*	crt
*	CourtDate
*	TotalFeeCharged
*	Owes
*	PaymentDate
*	PaymentFollowUpdate
*	Remainingdays
*	CrtType
**/

ALTER PROCEDURE [dbo].[USP_HTP_Get_Payment_Due_Calls]
AS 

DECLARE @setdays INT
SET @setdays = (
        SELECT TOP 1
        tbld.Attribute_Value
        FROM   Tbl_BusinessLogicDay tbld
        WHERE  tbld.Report_Id = (
                   SELECT tr.Report_ID
                   FROM tbl_Report tr
                   WHERE tr.Report_Url='/reports/PaymentDueCalls.aspx'
               )
    )

SELECT 
	   t.TicketID_PK,
	   -- tahir 6070 06/24/2009 fixed the client name bug.
       t.Firstname + ' ' + t.Lastname AS ClientName,
       (
           SELECT TOP 1 c.shortname
           FROM   tblticketsviolations tv,
                  tblcourts c
           WHERE  tv.courtid = c.courtid
                  AND tv.ticketid_pk = t.ticketid_pk
       ) AS crt,
       CONVERT(VARCHAR(20),
      (
           ISNULL(
               (
                   SELECT MIN(tv.courtdatemain)
                   FROM   tblticketsviolations tv
                   WHERE  tv.ticketid_pk = t.ticketid_pk
                          AND DATEDIFF(DAY, GETDATE(), tv.courtdatemain) >= 0
               ),
               (
                   SELECT MAX(tv1.courtdate)
                   FROM   tblticketsviolations tv1
                   WHERE  tv1.ticketid_pk = t.ticketid_pk
               )
           )
       ),101) AS CourtDate,
       t.TotalFeeCharged,
        ISNULL(t.TotalFeeCharged, 0) -(
           SELECT SUM(ISNULL(chargeamount, 0))
           FROM   tblticketspayment p
           WHERE  p.ticketid = t.ticketid_pk
                  AND p.paymentvoid = 0
                  AND p.paymenttype NOT IN (99, 100)
       ) AS Owes,        
       CONVERT(VARCHAR(200), sp.PaymentDate, 101) AS PaymentDate,
        CONVERT(VARCHAR(200), t.PaymentDueFollowUpDate, 101) AS PaymentFollowUpdate,
       DATEDIFF(DAY, GETDATE(), t.PaymentDueFollowUpDate) AS Remainingdays,
       t.paymentstatus,
       --Nasir 6049 07/10/2009 add case Type
		(SELECT ct.CaseTypeName FROM CaseType ct WHERE ct.CaseTypeId=t.CaseTypeId) as CrtType
      
FROM   dbo.tblTickets AS t
       INNER JOIN dbo.tblSchedulePayment AS sp
            ON  t.TicketID_PK = sp.TicketID_PK
WHERE  --Ozair 7791 07/24/2010  where clause optimized    
       (
           SELECT TOP 1 tv.courtid
           FROM   tblticketsviolations tv
           WHERE  tv.ticketid_pk = t.ticketid_pk
       ) IN (SELECT DISTINCT(CourtID)
             FROM   tblcourts
             WHERE  courtid <> 0
                    AND InActiveFlag = 0)
       AND (
               ISNULL(t.TotalFeeCharged, 0) -(
                   SELECT SUM(ISNULL(chargeamount, 0))
                   FROM   tblticketspayment p
                   WHERE  p.ticketid = t.ticketid_pk
                          AND p.paymentvoid = 0
                          AND p.paymenttype NOT IN (99, 100)
               )
           ) > 0
       AND DATEDIFF(DAY, GETDATE(), t.PaymentDueFollowUpDate) > 0
       AND DATEDIFF(DAY, GETDATE(), t.PaymentDueFollowUpDate) <= @setdays
       AND ISNULL(t.Paymentstatus, 1) NOT IN (2, 3)
       AND (sp.ScheduleFlag = 1)
       AND (t.Activeflag = 1)
ORDER BY
       t.PaymentDueFollowUpDate DESC,
       t.TicketID_PK -- Adil Aleem 5857 05/15/2009 Include Ticket ID in order by clause

      
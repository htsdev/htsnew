

/****** 
Created by:		Rab Nawaz Khan
Task ID   :		10442
Date	  :		09/17/2012

Business Logic:	The procedure is used by LMS application to get data for Sugar Land Warrant letter
				and to create LMS history for this letter for the selected date range.
				
List of Parameters:
	@catnum:		Category number of the selected letter type, if printing for 
					all courts of the selected court category
	@LetterType:	Selected letter type in LMS
	@CrtId:			Court id of the selected court category if printing for individual court hosue.
	@startListdate:	Comma separated dates selected to print letter.
	@EmpId:			Employee information who is printing the letter.
	@PrintType:		Flag to just preview the letter or to mark letters as sent. If 1 then LMS letter history will be created.
	@SearchType:	Flag that identifies if searching by court date or by list date. 0= list date, 1 = court date
	@isPrinted:		Flag to include already printed letters in the batch.

List of Columns:	
	LetterId:		Letter ID generated for each letter. It will be "NOT PRINTABLE"	if @PrintType = 0
	RecordId:		Identity value to look up the case in non-clients. used to group the records in report file.
	FirstName:		Person's first name that will appear on the letter
	LastName:		Person's last name that will appear on the letter
	Address:		Person's Home address that will appear on the letter
	City:			Person's Home City that will appear on the letter
	State:			Person's Home State that will appear on the letter
	FineAmount:		Fine amount for each violation that will appear on the letter
	ViolationDescritpion: violation description that will appear on the letter
	CourtName:		Name of court with which the violation is associated. Used in report file
	DPC:			Address status that is used in barcode font on the letter
	DPTwo:			Address status that is used in barcode font on the letter
	TicketNumber_pk:Case number for the violation
	ZipCode:		Person's Home Zip code.
	ZipMid:			First five characters of zip code and midnumber for grouping of data in report file.
	MidNumber:		MidNumber of the person associated with the case.
	CourtId:		Court identification associated with the violation.
	CourtDate:		Court date associated with the violation.
	Abb:			Short abbreviation of employee who is printing the letter

*******/


/* 
exec  usp_Mailer_Send_Sugarland_Warrant 9, 134, 9, '9/24/2012,' , 3991,0,0,0
*/
        
ALTER PROCEDURE [dbo].[usp_Mailer_Send_Sugarland_Warrant]
@catnum int=1,                                      
@LetterType int=25,                                      
@crtid int=1,                                      
@startListdate varchar (4000) ='12/1/2006',                                      
@empid int=3991,                                
@printtype int = 0 ,
@searchtype int = 0,
@isprinted bit =0
                                      
as
set nocount on                               
                    
-- DECLARING LOCAL VARIABLES FOR THE FILTERS....                                                                  
declare @officernum varchar(50),                                      
@officeropr varchar(50),                                      
@tikcetnumberopr varchar(50),                                      
@ticket_no varchar(50),                                                                      
@zipcode varchar(50),                                               
@zipcodeopr varchar(50),                                      
                                      
@fineamount money,                                              
@fineamountOpr varchar(50) ,                                              
@fineamountRelop varchar(50),                     
                
@singleviolation money,                                              
@singlevoilationOpr varchar(50) ,                                              
@singleviolationrelop varchar(50),                                      
                                      
@doubleviolation money,                                              
@doublevoilationOpr varchar(50) ,                                              
@doubleviolationrelop varchar(50),                                    
@count_Letters int,                    
@violadesc varchar(500),                    
@violaop varchar(50)                                       
                        
-- DECLARING & INITIALIZING THE VARIABLE FOR DYNAMIC SQL...                                                  
declare @sqlquery varchar(max),  @sqlquery2 varchar(max)
Select @sqlquery =''  , @sqlquery2 = ''  
                                              
-- ASSIGNING VALUES TO THE VARIABLES 
-- BY GETTTING INFORMATION FROM LMS FILTER STRUCTURE...
-- FOR THIS LETTER TYPE....
Select @officernum=officernum,                                      
@officeropr=oficernumOpr,                                      
@tikcetnumberopr=tikcetnumberopr,                                      
@ticket_no=ticket_no,                                                                        
@zipcode=zipcode,                                      
@zipcodeopr=zipcodeLikeopr,                                      
@singleviolation =singleviolation,                                      
@singlevoilationOpr =singlevoilationOpr,                                      
@singleviolationrelop =singleviolationrelop,                                      
@doubleviolation = doubleviolation,                                      
@doublevoilationOpr=doubleviolationOpr,                                              
@doubleviolationrelop=doubleviolationrelop,                    
@violadesc=violationdescription,                    
@violaop=violationdescriptionOpr ,                
@fineamount=fineamount ,                                              
@fineamountOpr=fineamountOpr ,                                              
@fineamountRelop=fineamountRelop                
                                   
from tblmailer_letters_to_sendfilters                                      
where courtcategorynum=@catnum                                      
and Lettertype=@LetterType                                    
  
-- GETTING THE SHORT ABBREVIATION OF THE CURRENTLY LOGGED IN 
-- EMPLOYEE IN THE LOCAL VARIABLE....
declare @user varchar(10)
select @user = upper(abbreviation) from tblusers where employeeid = @empid

-- CREATING DYNAMIC SQL FOR THE MAIN QUERY...                                                                      
set @sqlquery=@sqlquery+'                                      
Select distinct ta.recordid, convert(varchar(10), ta.ListDate ,101) as listdate, left(zipcode,5) + rtrim(ltrim(midnumber)) as zipmid,   
tva.ViolationNumber_PK,  ta.violationdate as courtdate,   tva.courtlocation as courtid,    flag1 = isnull(ta.Flag1,''N'') ,                                            
tva.TicketNumber_PK,   ta.donotmailflag,   ta.clientflag, upper(ta.firstname)  firstname,  upper(ta.lastname) lastname,  
upper(ta.address1) + '''' + isnull(ta.address2,'''') as address,                        
upper(ta.city) as city,   s.state,   tc.CourtName,   ta.zipcode,   ta.midnumber, rtrim(ltrim(ta.midnumber)) as midnum,  
ta.dp2 as dptwo,  ta.dpc, violationdate,   violationstatusid,  tva.ViolationDescription, 
isnull(tva.fineamount,0) as FineAmount
into #temptable
FROM tblTicketsViolationsArchive tva 
	INNER JOIN tblTicketsArchive ta ON tva.RecordID = ta.RecordID
	INNER JOIN tblCourts tc ON tc.Courtid = tva.CourtLocation
	INNER JOIN tblState s ON s.StateID = ta.StateID_FK

-- NOT MARKED AS STOP MAILING...
WHERE  donotmailflag = 0

-- ONLY VERIFIED AND VALID ADDRESSES
and  Flag1 in (''Y'',''D'',''S'')  

-- PERSONS ADDRESS IS NOT CHANGED IN PAST 48 MONTHS (ACCUZIP)...
and isnull(ta.ncoa48flag,0) = 0

and LEN(ISNULL(RTRIM(LTRIM(ta.lastname)),''''))<>0
and LEN(ISNULL(RTRIM(LTRIM(ta.firstname)),''''))<>0

-- GET ONLY FTA CASES....
and (violationstatusid = 186)  

-- Only Non-Clients
AND ISNULL(ta.Clientflag, 0) = 0

'

-- GET RECORDS RELATED ONLY TO THE SELECTED DATE RANGE.....
set @sqlquery=@sqlquery+'  
and   ' + dbo.Get_String_Concat_With_DatePart_ver2 ( ''+  @startlistdate + '', 'ta.ListDate' )   

-- IF PRINTING FOR ALL COURTS OF THE SELECTED CATEGORY.....            
if(@crtid = @catnum ) 
	set @sqlquery=@sqlquery+'   
	and  tva.courtlocation In (Select courtid from tblcourts where courtcategorynum = '+ CONVERT(vARCHAR,@catnum) +' )'                    
  
-- ELSE PRINTING FOR AN INDIVIDUAL COURT HOUSE SELECTED.......
ELSE 
	set @sqlquery =@sqlquery +'                                    
	and  tva.courtlocation =  ' +convert(varchar(10),@crtid)                                   
                                    
-- LMS FILTERS SECTION:
-----------------------------------------------------------------------------------------------------------------------------------------------------
-- IF THERE IS ANY FILTER SPECIFIED TO INCLUDE/EXCLUDE ONLY PARTICULAR OFFICER NUMBERS....
if(@officernum<>'')                                      
	set @sqlquery =@sqlquery + '    
	and  ta.officerNumber_Fk '+@officeropr +'('+ @officernum+')'                                       

-- IF THERE IS ANY FILTER DEFINED FOR INCLUDE/EXCLUDE TICKET NUMBER...                                                                          
if(@ticket_no<>'')                                      
	set @sqlquery=@sqlquery+ '  
	and   ('+ dbo.Get_String_Concat_With_op(''+@ticket_no+'' , 'tva.TicketNumber_PK' ,+ ''+ @tikcetnumberopr)+')'                                            
                                      
-- ZIP CODE FILTER......
-- INCLUDE/EXCLUDE THE SPECIFIED ZIP CODES.....                       
if(@zipcode<>'')                                                            
	set @sqlquery=@sqlquery+ '   
	and ('+  dbo.Get_String_Concat_With_op(''+@zipcode+'' , 'left(ta.zipcode,3)' ,+ ''+ @zipcodeopr) +')'                  

-- FINE AMOUNT FILTER.....
-- INCLUDE ONLY THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT....
if(@fineamount<>0 and @fineamountOpr<>'not'  )                
	set @sqlquery =@sqlquery+ '
	and tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                     

-- EXCLUDE THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT.....
if(@fineamount<>0 and @fineamountOpr = 'not'  )                
	set @sqlquery =@sqlquery+ '
	and not tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')' 

-- GETTING RECORDS IN ANOTHER TEMP TABLE.....
set @sqlquery =@sqlquery+ '
select  * into #temp from #temptable where 1 = 1
'

-- SINGLE VIOLATION FINE AMOUNT FILTER....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE SINGLE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT
if(@singleviolation<>0 and @doubleviolation=0)                                
	set @sqlquery =@sqlquery+ '
	and (  '+ @singlevoilationOpr+  '  (total_fineamount '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and violcount=1))
      '                                
-- DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...
if(@doubleviolation<>0 and @singleviolation=0 )                                
	set @sqlquery =@sqlquery + '
	and ('+ @doublevoilationOpr+  '   (total_fineamount '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ')and violcount=2) )
      '                                                    

-- BOTH SINGLE & DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE AND SINGLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...                                      
if(@doubleviolation<>0 and @singleviolation<>0)                                
	set @sqlquery =@sqlquery+ '
	and ('+ @singlevoilationOpr+  '  (total_fineamount '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and violcount=1))                                
    and ('+ @doublevoilationOpr+  '  (total_fineamount '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ') and violcount=2))
        '                                  
                              
-- VIOLATION DESCRIPTION FILTER.....
-- EXCLUDE THE LETTER IF SPECIFIED VIOLATION IS THE ONLY VIOLATION IN THE LETTER....
if(@violadesc <> '')
	BEGIN
		-- NOT LIKE FILTER.......
		if(charindex('not',@violaop)<> 0 )              
		BEGIN
			set @sqlquery=@sqlquery+'
			and	 not (  ( '+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  violationdescription  ' ,+ ''+ 'like')+'))'           			
		END
		-- INCLUDE ONLY THOSE LETTER THAT SINGLE VIOLATION AND MATCHES THE SPECIFIED VIOLATION DESCRIPTION...
		-- LIKE FILTER.......
		--if(charindex('not',@violaop) =  0 )              
		ELSE
			BEGIN
				set @sqlquery=@sqlquery+' 
				and	 ( ( '+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  violationdescription  ' ,+ ''+ 'like')+'))' 		
			END
	END


-- GETTING RECORDS IN ANOTHER TEMP TABLE.....
set @sqlquery =@sqlquery+'
Select distinct recordid, violationstatusid,  listdate as mdate,FirstName,LastName,address,                                
city,state,FineAmount,violationnumber_pk,violationdescription,CourtName,dpc,dptwo,Flag1,TicketNumber_PK,                                       
courtid,courtdate,zipmid,donotmailflag,clientflag, zipcode,midnumber,midnum, ROW_NUMBER() OVER (ORDER BY (SELECT 1)) AS RowNum into #temp1 from #temp

-- ECLUDE THE LETTERS THAT HAVE BEEN ALREADY PRINTED......
delete from #temp1 
from #temp1 a inner join tblletternotes_detail d on d.recordid = a.recordid inner join 
tblletternotes n on n.noteid = d.noteid
where ( n.lettertype  ='+convert(varchar(10),@lettertype)+' ) 


--Rab Nawaz Khan 10442 12/04/2012 Removing the Duplicated entries. . . 
--SELECT DISTINCT t1.FIRSTName, t1.lastName, t1.ADDRESS, MIN(t1.Recordid) AS Recordid
--INTO #duplicationProfiles
--FROM #temp1 t1
--GROUP BY t1.FIRSTName, t1.lastName, t1.ADDRESS
--HAVING COUNT(DISTINCT t1.Recordid) > 1
--ORDER BY t1.FIRSTName, t1.lastName, t1.ADDRESS

--UPDATE t
--SET t.Recordid = d.Recordid
--FROM #temp1 t INNER JOIN  #duplicationProfiles d ON t.Recordid <> d.recordid
--WHERE  t.FIRSTName = d.FIRSTName 
--AND t.lastName = d.lastName
--AND t.ADDRESS = d.ADDRESS

--SELECT DISTINCT Ticketnumber_pk, MAX(rownum) AS RowNum 
--INTO #deleteDuplicate
--FROM #temp1
--WHERE Ticketnumber_pk IN (
--SELECT DISTINCT Ticketnumber_pk
--FROM #temp1 t1
--GROUP BY t1.FIRSTName, t1.lastName, t1.ADDRESS, Ticketnumber_pk, t1.recordid
--HAVING COUNT(t1.Ticketnumber_pk) > 1)
--GROUP BY  Ticketnumber_pk

--DELETE FROM #temp1 WHERE RowNum IN (SELECT d.RowNum FROM #deleteDuplicate d)

SELECT DISTINCT t1.FIRSTName, t1.lastName, t1.ADDRESS, MIN(t1.Recordid) AS Recordid
INTO #duplicationProfiles
FROM #temp1 t1
GROUP BY t1.FIRSTName, t1.lastName, t1.ADDRESS


DELETE FROM t
FROM #temp1 t WHERE t.Recordid NOT IN (SELECT d.Recordid FROM #duplicationProfiles d)  

-- END 10442

' 
             

-- IF "MARK LETTER AS SENT" OPTION IS SELECTED....
-- CREATE LETTER HISTORY FOR EACH PERSON....
if( @printtype<>0)    
	BEGIN       
	set @sqlquery2 =@sqlquery2 +                                      
	'  
	 Declare @ListdateVal DateTime,                                    
	  @totalrecs int,  @count int, @p_EachLetter money, @recordid varchar(50),  @zipcode   varchar(12),  @maxbatch int,   
	  @lCourtId int  , @dptwo varchar(10),  @dpc varchar(10),  @tempmidnum varchar(50), @midnum  varchar(20),  @tempidentity  varchar(50)		
		 
	  declare @tempBatchIDs table (batchid int)

	 -- GETTING TOTAL LETTERS AND COSTING ......
	 Select @totalrecs =count(zipmid) from #temp1 where mdate = @ListdateVal                                   
	 Select @count=Count(zipmid) from #temp1 
	 if @count>=500 
	  set @p_EachLetter=convert(money,0.3) 
	 if @count<500 
	  set @p_EachLetter=convert(money,0.39)                                  

	-- DECLARING CURSOR TO INSERT THE RECORDS IN LETTER HISTORY ONE BY ONE.....
	-- THERE WILL A SEPARATE BATCH FOR EACH LIST/COURT DATE VALUE ......
	 Declare ListdateCur Cursor for 
	 Select distinct mdate  from #temp1                                  
	 open ListdateCur 
	 Fetch Next from ListdateCur into @ListdateVal                                                         
	 while (@@Fetch_Status=0) 
	  begin 
	   Select @totalrecs =count(distinct zipmid) from #temp1 where mdate = @ListdateVal                                   

		-- INSERTING RECORD IN BATCH TABLE......                               
	   insert into tblBatchLetter(Empid, Lettertype,Listdate,CourtId,ParentID,LCount,PExpense) values                                    
	   ('+convert(varchar(10),@empid)+' ,'+convert(varchar(10),@lettertype)+', @ListdateVal, '+convert(varchar(10),@crtid)+', 0, @totalrecs, @p_EachLetter)                                     
	   Select @maxbatch=Max(BatchId) from tblBatchLetter  
		insert into @tempBatchIDs select @maxbatch 

		-- CREATING CURSOR TO INSERT RECORDS FOR EACH LETTER FOR THE NEWLY INSERTED BATCH.... 
		Declare RecordidCur Cursor for 
		Select  recordid,zipcode, courtid, dptwo, dpc,midnum from #temp1 where mdate = @ListdateVal order by zipmid                              
		open RecordidCur 
		Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc,@midnum                                              

		-- INSERTING LETTERS IN LETTER HISTORY FOR THE SPECIFIED BATCH...
		while(@@Fetch_Status=0) 
		begin  
		 if(@midnum = @tempmidnum)	
		begin 
			insert into tblletternotes_detail (noteid, recordid) select @tempidentity, @recordid
		end
		 else
    		begin                         
			 insert into tblLetterNotes(BatchId_Fk,listdate,ZipCode,RecordID,PCost,LetterType, courtid, dp2, dpc,midnumber)                                
			 values (@maxbatch,@ListdateVal,@zipcode,@recordid,@p_EachLetter,'+convert(varchar(10),@lettertype)+' ,@lCourtId, @dptwo, @dpc, @midnum)  
			 set @tempidentity =@@identity
			 set @tempmidnum = @midnum 
			insert into tblletternotes_detail (noteid, recordid) select @tempidentity, @recordid
			end	
		Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc,@midnum  
		end 
		close RecordidCur   
		deallocate RecordidCur                                                                 
	   Fetch Next from ListdateCur into @ListdateVal                               
	  end 
	 close ListdateCur    
	 deallocate ListdateCur   

	-- OUTPUTTING THE DATA TO THE APPLICATION FOR REPROT FILE.....
	Select distinct convert(varchar(20),n.noteid) as letterid, t.recordid,  t.FirstName,                              
	t.LastName,t.address,t.city, t.state,FineAmount,violationdescription,CourtName, t.dpc, 
	dptwo, TicketNumber_PK, n.zipcode,  t.zipmid,t.recordid,t.courtdate,t.courtid,n.midnumber, n.midnumber as midnum, '''+@user+''' as Abb                              
	from #temp1 t , tblletternotes n  , tblletternotes_detail d, 	@tempBatchIDs tb 
	where 	d.RECORDID = T.RECORDID and 	d.noteid = n.noteid --AND N.COURTID = T.COURTID
	and 	TB.BATCHID = N.BATCHID_FK and  n.lettertype = '+convert(varchar(10),@lettertype)+' order by zipmid desc

	-- GENERATING AND SENDING EMAIL FOR LETTER PRINTING NOTIFICATION........
	declare @lettercount int, @subject varchar(200), @body varchar(400) ,  @sql varchar(500)
	select @lettercount = count(distinct n.noteid) from tblletternotes n, @tempBatchIDs b where n.batchid_fk = b.batchid
	select @subject =  convert(varchar(20), @lettercount) + '' Sugar Land Warrant Letters Printed''
	select @body = convert(varchar(20), @lettercount) + '' LETTERS ('' + CONVERt(VARCHAR(12), GETDATE()) + '') - ''+ upper( '''+@user+''')
	exec usp_mailer_send_Email @subject, @body

	-- OUTPUTING THE PDF FILE NAME TO THE APPLICATION...	
	declare @batchIDs_filname varchar(500)
	set @batchIDs_filname = ''''
	select @batchIDs_filname = @batchIDs_filname + convert(varchar,batchid) + '',''  from @tempBatchIDs
	select isnull(@batchIDs_filname,'''') as batchid

	'  
	END  
else  
	-- IF "MARK LETTER AS SENT" OPTION IS NOT SELECTED....
	-- THEN DO NOT CREATE LETTER HISTORY. JUST OUTPUT THE DATA FOR REPORT FILE 
	-- TO DISPLAY THE LETTER...
	BEGIN
	 set @sqlquery2 = @sqlquery2 + '  
	  Select distinct ''NOT PRINTABLE'' as letterId,  recordid,  FirstName, 
	  LastName,address,city, state,FineAmount,violationdescription,CourtName, dpc, dptwo, TicketNumber_PK, 
	  zipcode,zipmid,recordid,courtdate,courtid,midnumber,midnum, '''+@user+''' as Abb
	  from #temp1 order by zipmid desc
	 '  

	END

-- DROPPING TEMPORARY TABLES ....
set @sqlquery2 = @sqlquery2 + ' drop table #temp drop table #temp1 DROP TABLE #temptable Drop Table #duplicationProfiles '  
                                      
--print @sqlquery 
--print @sqlquery2  

-- EXECUTING THE DYNAMIC SQL ....
exec (@sqlquery + @sqlquery2)


GO
GRANT EXECUTE ON [dbo].[usp_Mailer_Send_Sugarland_Warrant] TO dbr_webuser
GO
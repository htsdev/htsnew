﻿  --USP_HTP_Get_Legal_Consultation_Request  '1/1/2003','12/5/2008',0,true      
      
      
/******           
Created by:  Muhammad Nasir        
Business Logic : This Procedure select clients that requested consultation that has not been contacted from Traffic system.

          
List of Parameters:           
 @SDate   : start date to search          
 @EDate : End date to search          
 @Status : Call back Status         
 @ShowAll : Show all record        

List of Columns: 	
	TicketID_PK : Tickets
	ClientName  : Client Name
    Contact Info: Contact no by which company contact
	Recieve Date : Date on which comment was  added. By default,the report will display the data in sorted order by "Recieve Date" in ascending order.     --Yasir 5357 12/25/2008 
    Comments    : Consultation comments when ask for consultation     
         
******/            
  
  --Nasir 5256 12/12/2008  
  --USP_HTP_Get_Legal_Consultation_Request'2/25/2009 12:00:00 AM','2/25/2009 12:00:00 AM',1,1         
        
ALTER PROCEDURE USP_HTP_Get_Legal_Consultation_Request
(      
@SDate AS DATETIME,        
@EDate AS DATETIME,        
@Status AS INT,        
@ShowAll AS BIT        
 )       
as        
        
   
 set @SDate = @SDate + '23:59:58'        
 set @EDate = @EDate + '23:59:58'        
       
 SELECT distinct       
tt.TicketID_PK,tt.Lastname+', '+tt.Firstname AS ClientName,      
 CASE LEN(tt.Contact1)   
  WHEN 0 THEN ''  
  ELSE (convert(varchar(20),ISNULL(dbo.formatphonenumbers(tt.Contact1),''))+ISNULL('('+LEFT(tc1.Description, 1)+')',''))    
 END  
 as contact1,                                                    
 CASE LEN(tt.Contact2)   
  WHEN 0 THEN ''  
  ELSE (convert(varchar(20),ISNULL(dbo.formatphonenumbers(tt.Contact2),''))+ISNULL('('+LEFT(tc2.Description, 1)+')',''))    
 END  
 as contact2,                                                    
 CASE LEN(tt.Contact3)   
  WHEN 0 THEN ''  
  ELSE (convert(varchar(20),ISNULL(dbo.formatphonenumbers(tt.Contact3),''))+ISNULL('('+LEFT(tc3.Description, 1)+')',''))   
 END  
 as contact3,     
convert(varchar(10),thcc.CommentsDate,101) as RecieveDate,thcc.commentsdate as recievedate_sort, --Yasir 5357 12/25/2008 Date on which comment was  added
thcc.Comments,thcc.CallBackStatus,tr.[Description]        
FROM tblTickets tt        
INNER JOIN tbl_hts_ConsultationComments thcc ON tt.TicketID_PK=thcc.TicketID_FK        
INNER JOIN tblTicketsViolations ttv ON tt.TicketID_PK=ttv.TicketID_PK        
INNER JOIN tblReminderstatus tr ON thcc.CallBackStatus=tr.Reminderid_PK        
INNER JOIN tblContactstype tc1 ON tc1.ContactType_PK=tt.ContactType1        
INNER JOIN tblContactstype tc2 ON tc2.ContactType_PK=tt.ContactType2        
INNER JOIN tblContactstype tc3 ON tc3.ContactType_PK=tt.ContactType3        
  
  
  
WHERE tt.hasConsultationComments=1  
  
AND ((@Status != -1 AND thcc.CallBackStatus = CONVERT(VARCHAR(20),@Status))OR (@Status = -1))  --Fahad 5533 02/25/2009 modify for ALL records and when not select ALL   
AND ((@ShowAll != 1 AND  DATEDIFF(DAY,thcc.CommentsDate,@SDate)<=0 AND DATEDIFF(DAY,thcc.CommentsDate,@EDate)>=0) OR (@ShowAll = 1))  
      
ORDER BY recievedate_sort ASC  --Yasir 5357 12/25/2008
       
       

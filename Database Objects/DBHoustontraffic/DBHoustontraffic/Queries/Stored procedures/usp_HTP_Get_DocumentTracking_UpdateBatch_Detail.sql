/****** 
Create by:  Syed Muhammad Ozair

Business Logic : this procedure is used to fetch the details of a perticular non verified scanned batch document.

List of Parameters:	
	@ScanBatchID : scan batch id for which records will be fetched

List of Columns: 	
	PicName : picture name of those scanned docs for which document id is not retrived
	SBDID : scan batch detail id

******/

Create Procedure [dbo].[usp_HTP_Get_DocumentTracking_UpdateBatch_Detail]

@ScanBatchID int

as

select	convert(varchar,scanbatchid_fk) +'-'+convert(varchar,scanbatchdetailid) as PicName,
	scanbatchdetailid as SBDID
from	tbl_htp_documenttracking_scanbatch_detail
where	documentbatchid=0
and scanbatchid_fk=@ScanBatchID

Go

grant exec on [dbo].[usp_HTP_Get_DocumentTracking_UpdateBatch_Detail] to dbr_webuser
go
﻿ --select * from   
--update  
--tblhtsbatchprintletter   
--set isprinted = 0  
--where letterId_Fk = 12  
  
  
              
ALTER procedure [dbo].[USP_HTS_PledOut_Batch_Letter]   --3992,12,'1639843636,18744043519,1619043637,844543507,15885943502,'                        
@empid  int,                        
@LetterType int,                        
@TicketIDList varchar(8000)                        
                        
as                            
begin                            
                       
                          
declare @tickets table (                                                      
 ticketid numeric    
     
 )                             
                            
insert into @tickets                                                   
 select * from dbo.Sap_String_Param_New(@TicketIDList)                                      
    
                     
            
  --SELECT * FROM tblcourts    
      
--select * from tbldoctype      
    
    
declare @temp table (                                                      
 TicketID_PK numeric,    
 FirstName varchar(20),    
 LastName varchar(20),    
 Address1 varchar(50),    
 City varchar(50),    
 Zip varchar(20),    
 State varchar(50),    
 RefcaseNumber varchar(20),    
 languageSpeak varchar(30),    
 CourtDateMain datetime,    
 CourtNumberMain varchar(3),     
 Status varchar(4),    
 Location varchar(500),    
 courtphonenumber varchar(20),    
 [filename] varchar(30),    
 imgdata [image] NULL    
 )       
      
      
// Task Id 3595 05/21/2008 Agha Usman      
insert into  @temp           
select T.TicketID_PK ,  T.FirstName , T.LastName , T.Address1 ,T.City , T.Zip ,S1.State, TV.RefcaseNumber ,     
T.languageSpeak,  TV.CourtDateMain , TV.CourtNumberMain , CVS.ShortDescription as Status ,    
C.Address + ', ' + C.City + ', '+S2.State +' ' + C.Zip  as Location,C.Phone as courtphonenumber,     
convert(varchar(20),sb.ScanBookID) +'-'+ convert(varchar(20),sp.DOCID) + '.'+ sp.DocExtension as [filename],    
null as imgdata    
from           
tblHTSBatchPrintLetter   BP                        
join tbltickets T            
on T.TicketID_Pk = BP.Ticketid_FK            
join tblticketsviolations TV            
On T.TicketID_Pk = TV.Ticketid_pk             
join tblcourtviolationstatus CVS            
On CVS.courtviolationstatusid = TV.CourtViolationStatusidmain            
join tblcourts C            
on C.courtID = TV.CourtID            
join tblState S1             
on S1.Stateid = t.StateID_fk            
join tblState S2            
on  S2.Stateid = C.State      
left outer join  tblScanBook sb on t.TicketId_PK = sb.TicketID and sb.DocTypeID  = 16        
left outer join  tblScanPIC sp      
on sb.ScanBookId = sp.ScanBookId  and sp.DocExtension = 'JPG'      
where       
cast(convert(varchar(12), BP.ticketid_fk) + convert(varchar(12), BP.BatchID_pk) as numeric)                       
IN (select ticketid from @tickets)                       
and letterid_fk = @LetterType  
and TV.ViolationNumber_PK = (select top 1 ViolationNumber_PK from tblticketsviolations where ticketId_Pk = t.ticketId_Pk)  
            
order by BP.BatchID_pk         
    
    
select * from   @temp                 
                      
                            
            
/*            
select T.TicketID_PK ,  T.FirstName , T.LastName , T.Address1 ,T.City , T.Zip ,S1.State, TV.RefcaseNumber ,T.languageSpeak ,   TV.CourtDateMain , TV.CourtNumberMain , CVS.ShortDescription as Status , C.Address + ', ' + C.City + ', '+S2.State +' ' + C.Zip 
  
    
      
        
as Location from           
tblHTSBatchPrintLetter   BP                        
join tbltickets T            
on T.TicketID_Pk = BP.Ticketid_FK            
join tblticketsviolations TV            
On T.TicketID_Pk = TV.Ticketid_pk             
join tblcourtviolationstatus CVS            
On CVS.courtviolationstatusid = TV.CourtViolationStatusidmain            
join tblcourts C            
on C.courtID = TV.CourtID            
join tblState S1             
on S1.Stateid = t.StateID_fk            
join tblState S2            
on  S2.Stateid = C.State            
where             
--cast(convert(varchar(12), BP.ticketid_fk) + convert(varchar(12), BP.BatchID_pk) as numeric)                       
--IN (select ticketid from @tickets)                       
 letterid_fk = 12            
            
order by BP.BatchID_pk              
            
  */          
                    
                          
end 
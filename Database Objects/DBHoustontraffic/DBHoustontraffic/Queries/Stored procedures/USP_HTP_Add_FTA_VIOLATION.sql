﻿/************************************************************
 * Code formatted by SoftTree SQL Assistant © v4.0.34
 * Time: 5/19/2009 4:38:28 PM
 ************************************************************/

/****** Object:  StoredProcedure [dbo].[USP_HTP_Add_FTA_VIOLATION]  ******/     
/*        
 Created By     : Fahad Muhammad Qureshi.
 Created Date   : 05/18/2009  
 TasK		    : 5807        
 Business Logic : This procedure First Check FTA violtaion in Non-Client DataBase 
				if found:
				Then insert that Violation from Non-Client DataBase to Client DataBase and link that Violation to the Client Profile. 
				if Not found:
				Then insert NewViolation containing  OldCauseNumber+Alphabets to the Client Profile.
Parameter:       
  @Ticketid				: Deleting criteria with respect to TicketId      
  @courtviolationstatus	: Status of the Violation
  @empid				: By which we trace out the Employee
  @NewCauseNo			: The new CauseNumber having Alphabet Character which we have to insert
  @Recordid				: The identity for the Non-Client DataBase
  @Midnumber			: Client having multiple violation then have same no based on All violations called MidNumber
  @PlanId				: Trace out the plan of the client on the basis of Violations
      
*/ 

Create PROCEDURE [dbo].[USP_HTP_Add_FTA_VIOLATION]
	@Ticketid INT,
	@courtviolationstatus INT,
	@empid INT,
	@NewCauseNo VARCHAR(100),
	@Recordid INT,
	@Midnumber VARCHAR(100),
	@PlanId INT
AS
	DECLARE @NewTicketViolationID INT
	IF dbo.fn_CheckFTAViolationForNonClient(@Recordid, @Midnumber) = 0
	BEGIN
	    INSERT INTO tblTicketsViolations
	      (
	        TicketID_PK,
	        ViolationNumber_PK,
	        SequenceNumber,
	        FineAmount,
	        BondAmount,
	        RefCaseNumber,
	        casenumassignedbycourt,
	        ViolationStatusID,
	        ViolationDescription,
	        CourtDate,
	        CourtNumber,
	        CourtViolationStatusID,
	        UpdatedDate,
	        CourtID,
	        TicketViolationDate,
	        TicketOfficerNumber,
	        CourtDateMain,
	        CourtViolationStatusIDmain,
	        CourtNumbermain,
	        CourtDateScan,
	        CourtViolationStatusIDScan,
	        CourtNumberscan,
	        ScanUpdateDate,
	        VerifiedStatusUpdateDate,
	        VEmployeeID,
	        ScanDocID,
	        ScanDocNum,
	        ViolationRecDate,
	        ReminderComments,
	        ReminderCallStatus,
	        ReminderCallEmpID,
	        planid,
	        UnderlyingBondFlag,
	        DSCProbationAmount,
	        DSCProbationTime,
	        DSCProbationDate,
	        TimeToPay,
	        TimeToPayDate,
	        IsDSCSelected,
	        IsRETSelected,
	        IsMISCSelected,
	        ShowCauseDate,
	        MISCComments,
	        ViolRecDate,
	        ShowCauseCourtNumber,
	        RetDate,
	        RetCourtNumber,
	        IsIns,
	        CourseCompletionDays,
	        DrivingRecordDays,
	        IsShowCause,
	        GroupID,
	        loaderupdatedate,
	        CourtIDScan,
	        BondDate,
	        bondprintflag,
	        BSDAUpdateDate,
	        RecordID,
	        OscarCourtDetail,
	        arrignmentwaitingdate,
	        bondwaitingdate,
	        OffenseLocation,
	        OffenseTime,
	        CoveringFirmID,
	        ChargeLevel,
	        CDI,
	        setCallComments,
	        setcallstatus,
	        ArrestingAgencyName,
	        ArrestDate,
	        MissedCourtType,
	        BondReminderDate,
	        FTACallComments,
	        FTACallStatus,
	        HMCAWDLQCallStatus
	        --,AutoDialerReminderCallStatus,
	        --AutoDialerSetCallStatus
	      )(
	           SELECT tt.TicketID_PK,
	                  11189,
	                  SequenceNumber,
	                  FineAmount,
	                  BondAmount,
	                  RefCaseNumber,
	                  @NewCauseNo,
	                  ViolationStatusID,
	                  ViolationDescription,
	                  CourtDate,
	                  CourtNumber,
	                  CourtViolationStatusID,
	                  UpdatedDate,
	                  CourtID,
	                  TicketViolationDate,
	                  TicketOfficerNumber,
	                  CourtDateMain,
	                  CourtViolationStatusIDmain,
	                  CourtNumbermain,
	                  CourtDateScan,
	                  CourtViolationStatusIDScan,
	                  CourtNumberscan,
	                  ScanUpdateDate,
	                  VerifiedStatusUpdateDate,
	                  VEmployeeID,
	                  ScanDocID,
	                  ScanDocNum,
	                  ViolationRecDate,
	                  tt.ReminderComments,
	                  ReminderCallStatus,
	                  ReminderCallEmpID,
	                  planid,
	                  UnderlyingBondFlag,
	                  DSCProbationAmount,
	                  DSCProbationTime,
	                  DSCProbationDate,
	                  TimeToPay,
	                  TimeToPayDate,
	                  IsDSCSelected,
	                  IsRETSelected,
	                  IsMISCSelected,
	                  ShowCauseDate,
	                  MISCComments,
	                  ViolRecDate,
	                  ShowCauseCourtNumber,
	                  RetDate,
	                  RetCourtNumber,
	                  IsIns,
	                  CourseCompletionDays,
	                  DrivingRecordDays,
	                  IsShowCause,
	                  GroupID,
	                  loaderupdatedate,
	                  CourtIDScan,
	                  BondDate,
	                  bondprintflag,
	                  BSDAUpdateDate,
	                  tt.RecordID,
	                  OscarCourtDetail,
	                  arrignmentwaitingdate,
	                  bondwaitingdate,
	                  OffenseLocation,
	                  OffenseTime,
	                  tt.CoveringFirmID,
	                  ChargeLevel,
	                  CDI,
	                  setCallComments,
	                  setcallstatus,
	                  ArrestingAgencyName,
	                  ArrestDate,
	                  MissedCourtType,
	                  BondReminderDate,
	                  FTACallComments,
	                  FTACallStatus,
	                  HMCAWDLQCallStatus
	                  --AutoDialerReminderCallStatus,
	                  --AutoDialerSetCallStatus
	           FROM   tblTicketsViolations ttva
						INNER JOIN tblTickets tt ON  tt.TicketID_PK = ttva.TicketID_PK
	           WHERE  ( ttva.RecordID = @Recordid OR tt.Midnum = @Midnumber )
	                  AND ttva.TicketID_PK = @Ticketid
	                  AND ttva.TicketsViolationID = (
	                          SELECT MAX(tblTicketsViolations.TicketsViolationID)
	                          FROM   tblTicketsViolations
	                          WHERE  (tblTicketsViolations.RecordID = @Recordid OR tt.Midnum = @Midnumber)
	                                 AND tblTicketsViolations.TicketID_PK = @Ticketid
	                      )
	       )
	END
	ELSE 
	IF dbo.fn_CheckFTAViolationForNonClient(@Recordid, @Midnumber) > 0
	BEGIN
	    INSERT INTO tblTicketsViolations
	      (
	        TicketID_PK,
	        ViolationNumber_PK,
	        SequenceNumber,
	        FineAmount,
	        BondAmount,
	        RefCaseNumber,
	        casenumassignedbycourt,
	        ViolationStatusID,
	        ViolationDescription,
	        CourtDate,
	        CourtNumber,
	        CourtViolationStatusID,
	        UpdatedDate,
	        CourtID,
	        TicketViolationDate,
	        TicketOfficerNumber,
	        CourtDateMain,
	        CourtViolationStatusIDmain,
	        CourtNumbermain,
	        CourtDateScan,
	        CourtViolationStatusIDScan,
	        CourtNumberscan,
	        ScanUpdateDate,
	        VerifiedStatusUpdateDate,
	        VEmployeeID,
	        ScanDocID,
	        ScanDocNum,
	        ViolationRecDate,
	        ReminderComments,
	        ReminderCallStatus,
	        ReminderCallEmpID,
	        planid,
	        UnderlyingBondFlag,
	        DSCProbationAmount,
	        DSCProbationTime,
	        DSCProbationDate,
	        TimeToPay,
	        TimeToPayDate,
	        IsDSCSelected,
	        IsRETSelected,
	        IsMISCSelected,
	        ShowCauseDate,
	        MISCComments,
	        ViolRecDate,
	        ShowCauseCourtNumber,
	        RetDate,
	        RetCourtNumber,
	        IsIns,
	        CourseCompletionDays,
	        DrivingRecordDays,
	        IsShowCause,
	        GroupID,
	        loaderupdatedate,
	        CourtIDScan,
	        BondDate,
	        bondprintflag,
	        BSDAUpdateDate,
	        RecordID,
	        OscarCourtDetail,
	        arrignmentwaitingdate,
	        bondwaitingdate,
	        OffenseLocation,
	        OffenseTime,
	        CoveringFirmID,
	        ChargeLevel,
	        CDI,
	        setCallComments,
	        setcallstatus,
	        ArrestingAgencyName,
	        ArrestDate,
	        MissedCourtType,
	        BondReminderDate,
	        FTACallComments,
	        FTACallStatus,
	        HMCAWDLQCallStatus
	        -- ,AutoDialerReminderCallStatus,
	        -- AutoDialerSetCallStatus
	      )(
	           SELECT @Ticketid,
	                  11189,
	                  '',
	                  FineAmount,
	                  BondAmount,
	                  TicketNumber_PK,
	                  CauseNumber,
	                  violationstatusid,
	                  ViolationDescription,
	                  ttva.CourtDate,
	                  ttva.Courtnumber,
	                  135,
	                  GETDATE(),
	                  CourtLocation,
	                  TicketViolationDate,
	                  TicketOfficerNumber,
	                  ttva.CourtDate,
	                  135,
	                  ttva.Courtnumber,
	                  ttva.CourtDate,
	                  135,
	                  ttva.Courtnumber,
	                  '',
	                  '',
	                  '',
	                  '',
	                  '',
	                  GETDATE(),
	                  '',
	                  '',
	                  '',
	                  '',
	                  1,
	                  '',
	                  '',
	                  '',
	                  '',
	                  '',
	                  '',
	                  '',
	                  '',
	                  '',
	                  (
	                      SELECT TOP 1 ISNULL(tblTicketsViolations.planid, '')
	                      FROM   tblTicketsViolations
	                      WHERE  tblTicketsViolations.TicketID_PK = @Ticketid
	                  ),
	                  GETDATE(),
	                  '',
	                  '',
	                  '',
	                  '',
	                  '',
	                  '',
	                  '',
	                  '',
	                  '',
	                  '',
	                  '',
	                  '',
	                  '',
	                  '',
	                  '',
	                  '',
	                  '',
	                  '',
	                  '',
	                  '',
	                  '',
	                  '',
	                  '',
	                  '',
	                  '',
	                  '',
	                  '',
	                  '',
	                  '',
	                  '',
	                  ''
	           FROM   tblTicketsArchive tta
	                  INNER JOIN tblTicketsViolationsArchive ttva
	                       ON  tta.RecordID = ttva.RecordID
	           WHERE  (tta.RecordID = @RecordID OR tta.MidNumber = @Midnumber)
	                  AND tta.Clientflag = 0
	                  AND CHARINDEX('FTA', ttva.CauseNumber) > 0
	                  AND ISNULL(ttva.FTALinkId, 0) <> 0
	      )
	      UPDATE tblTicketsArchive
	      SET
	      	Clientflag = 1
	      WHERE tblTicketsArchive.RecordID=@RecordID or MidNumber = @Midnumber
	      	
	END
	
	SET @NewTicketViolationID = SCOPE_IDENTITY()
	IF ISNULL(@NewTicketViolationID, 0) <> 0
	BEGIN
		insert into tblticketsnotes(ticketid,subject,employeeid)
	    SELECT ttv.ticketid_pk,
	           UPPER('New Violation Added: Case # ') +
	           ISNULL(ttv.RefCaseNumber,'') + ': ' +
	           ISNULL(UPPER(tc.shortcourtname),'') + ': ' +
	           ISNULL(UPPER(tcvs.ShortDescription),'') + ': ' +
	           ISNULL(CONVERT(VARCHAR(10), ttv.CourtDateMain, 101),'N/A') + ' #' + ISNULL(CONVERT(VARCHAR(10),  ttv.CourtDateMain),'N/A') +
	           ' @' + ISNULL(LTRIM(RIGHT(CONVERT(VARCHAR(30),ttv.CourtDateMain, 100), 8)) ,'N/A')
	           +
	           ' (' + UPPER(ISNULL(tv.[Description],'')) + ')'				
	           ,@empid
	    FROM   tblTicketsViolations ttv
	           INNER JOIN tblCourts tc
	                ON  ttv.CourtID = tc.Courtid
	           INNER JOIN tblCourtViolationStatus tcvs
	                ON  tcvs.CourtViolationStatusID = ttv.CourtViolationStatusIDmain
	           INNER JOIN tblViolations tv
	                ON  ttv.ViolationNumber_PK = tv.ViolationNumber_PK
	    WHERE  ttv.TicketsViolationID = @NewTicketViolationID
	END
	
	GO
	GRANT EXECUTE ON [dbo].[USP_HTP_Add_FTA_VIOLATION] TO dbr_webuser
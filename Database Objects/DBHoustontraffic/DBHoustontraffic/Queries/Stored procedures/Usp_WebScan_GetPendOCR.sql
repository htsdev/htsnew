SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Usp_WebScan_GetPendOCR]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Usp_WebScan_GetPendOCR]
GO

  
Create procedure [dbo].[Usp_WebScan_GetPendOCR]    
      
@BatchID as int                 
as                
                
      
select picid,      
  CauseNo,      
  Status,      
  convert(varchar(12),NewCourtDate,101) as   NewCourtDate,    
  CourtNo,      
  Location,      
  convert(varchar(12),Time,108) as Time,
  isnull(TypeScan,'') as TypeScan	      
from tbl_webscan_ocr o      
  inner join tbl_webscan_pic p on      
  o.picid=p.id      
  where      
  p.batchid=@BatchID and o.checkstatus=1      
  order by o.picid        
      
              
                
  
  

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


﻿/************************************************************
 * Code formatted by SoftTree SQL Assistant © v4.0.34
 * Time: 1/28/2010 3:44:23 PM
 ************************************************************/

/****** 
Created by:		Fahad Muhammad Qureshi 7287 02/03/2010
   This procedure is used to get value from VALUE table against division, module submodule and key.

List of Parameters: It takes the following parameters
	@Division - Division ID in Value table
	@Module - Module ID in Value table
	@SubModule - SubModule ID in Value table
	@Key - Key ID in Value table.

List of Columns: It returns the following column
	DivisionName,
	ModuleName,
	SubModuleName,
	KeyName,
	Value,
	[Description],
	DivisionID,
	ModuleID,
	SubModuleID,
	KeyID
   
******/ 
CREATE PROCEDURE dbo.usp_config_GetSelectedData 
	@Division INT=0,
	@Module INT=0,
	@SubModule INT=0,
	@Key INT = 0
AS
	SELECT ROW_NUMBER() OVER(ORDER BY Division.DivisionName) AS SNo,
	       Configuration.dbo.Division.DivisionName,
	       Configuration.dbo.Module.ModuleName,
	       Configuration.dbo.SubModule.SubModuleName,
	       Configuration.dbo.[Key].KeyName,
	       Configuration.dbo.VALUE.Value,
	       Configuration.dbo.VALUE.[Description],
	       Configuration.dbo.Division.DivisionID,
	       Configuration.dbo.Module.ModuleID,
	       Configuration.dbo.SubModule.SubModuleID,
	       Configuration.dbo.[Key].KeyID
	FROM   Configuration.dbo.Division
	       INNER JOIN Configuration.dbo.VALUE
	            ON  Configuration.dbo.Division.DivisionID = Configuration.dbo.VALUE.DivisionID
	       INNER JOIN Configuration.dbo.[Key]
	            ON  Configuration.dbo.VALUE.KeyID = Configuration.dbo.[Key].KeyID
	       INNER JOIN Configuration.dbo.Module
	            ON  Configuration.dbo.VALUE.ModuleID = Configuration.dbo.Module.ModuleID
	       INNER JOIN Configuration.dbo.SubModule
	            ON  Configuration.dbo.VALUE.SubModuleID = Configuration.dbo.SubModule.SubModuleID
	WHERE  (
	           (@Division = 0)
	           AND (@Module = 0)
	           AND (@SubModule = 0)
	           AND (@Key = 0)
	           AND 1 != 1
	       )
	       OR  (
	               (
	                   (@Division != 0)
	                   OR (@Module != 0)
	                   OR (@SubModule != 0)
	                   OR (@Key != 0)
	               )
	               AND (
	                       (@Division = 0)
	                       OR (
	                              (@Division <> 0)
	                              AND (Configuration.dbo.Division.DivisionID = @Division)
	                          )
	                   )
	               AND (
	                       (@Module = 0)
	                       OR (
	                              (@Module <> 0)
	                              AND (Configuration.dbo.Module.ModuleID = @Module)
	                          )
	                   )
	               AND (
	                       (@SubModule = 0)
	                       OR (
	                              (@SubModule <> 0)
	                              AND (Configuration.dbo.SubModule.SubModuleID = @SubModule)
	                          )
	                   )
	               AND (
	                       (@Key = 0)
	                       OR ((@Key <> 0) AND (Configuration.dbo.[Key].KeyID = @Key))
	                   )
	           )
	ORDER BY
	       Division.DivisionName
GO
GRANT EXECUTE ON dbo.usp_config_GetSelectedData TO webuser_config 
GO
/******************  
  
CREATED BY : SARIM GHANI  
  
BUSINESS LOGIC : This procedure is used to get cases which have discrepancies.  
  
LIST OF INPUT PARAMETERS :  
   
 @TicketID_PK : Ticket ID   
  
LIST OF COLUMNS :  
    
 Arrest Date : Arrest Date Associated With The Violation  
 CaseTypeId  : Case Type Of Case  
 MissedCourtType : Get Missed Court Type   
*******************/  
Alter procedure [dbo].[usp_HTS_Get_CaseDispositionDetail]                                      
                                              
 @TicketID_PK  int                                              
                                              
 AS                                              
                                              
 begin                                               
  
--Zeeshan Ahmed 3535 04/08/2008  
--FTA Modifications  
declare @HasFTAViolations int   
select @HasFTAViolations = dbo.fn_CheckFTAViolationInCase(@TicketID_PK)  
  
  
-- SELECT tv.RefCaseNumber + '-' + convert(varchar(2),tv.SequenceNumber)  as RefCaseNum,                                              
SELECT CASE WHEN (LEN(RTRIM(LTRIM(ISNULL(tv.RefCaseNumber, 'N/A'))))) > 0 THEN (RTRIM(LTRIM(ISNULL(tv.RefCaseNumber, 'N/A'))))
ELSE 'N/A' END AS RefCaseNum,--Fahad 5368 12/30/2008                                              
 -- convert(varchar(2),tv.SequenceNumber)as SequenceNum, // Agha Usman 2664 13/10/2008  
 v.Description as descriptions,                                              
c.categoryid  as  CrtViolationtype,                                
--c.Description AS CourtViolationStatusID,                                               
c.shortDescription AS CourtViolationStatusID,
CASE 
WHEN (LEN(RTRIM(LTRIM(ISNULL(tv.casenumassignedbycourt, 'N/A'))))) > 0 THEN (RTRIM(LTRIM(ISNULL(tv.casenumassignedbycourt, 'N/A'))))
ELSE 'N/A'END AS CauseNumber,--Fahad 5368 12/30/2008                                                                         
  tv.CourtDateMain as CourtDate,                                               
 tv.CourtNumber as CourtNum,                                               
 tv.ViolationStatusID as ViolationStatusID,                                               
 tv.TicketsViolationID as TicketsViolationID,                                              
 tv.DSCProbationAmount  as DSCProbationAmount,                                              
 tv.[DSCProbationTime] ,                                              
 isnull(tv.[DSCProbationDate],getdate()) as  DSCProbationDate ,                                              
 tv.[TimeToPay],                                              
 isnull(tv.[TimeToPayDate],getdate()) as TimeToPayDate,                                              
 Convert(bit,(isnull(tv.[IsDSCSelected],0))) as IsDSCSelected,                                            
 Convert(bit,(isnull(tv.[IsRETSelected],0))) as IsRETSelected,                                              
 Convert(bit,(isnull(tv.[IsMISCSelected],0))) as IsMISCSelected,                                              
 tv.[ShowCauseDate],                                              
 tv.[MISCComments],
 --Yasir Kamal 6073 04/24/2009 Null Courtid Bug fixed.                                     
 ISNULL(tv.courtid,0) AS courtid,                         
 tv.ShowCauseCourtNumber,                            
 isnull(tv.RetDate,'01/01/1900') as RetDate,                            
 tv.RetCourtNumber,                            
  Convert(bit,(isnull(tv.IsIns,0))) as IsInsSelected,                            
  isnull(tv.CourseCompletionDays,0) as CourseCompletionDays,                            
 isnull(tv.DrivingRecordDays,0) as DrivingRecordsDays,                            
 Convert(bit,(isnull(tv.IsShowCause,0))) as IsShowCauseSelected  ,                                           
 -- Added By Zeeshan Ahmed to Implement Update Panel In case Disposition                        
tv.fineamount,                        
tv.bondamount,                        
tv.planid,                        
tv.courtnumber,                        
tv.ViolationNumber_pk ,                        
 c.CourtViolationStatusID  as courtviolationstatus,                        
--Comment By Zeeshan Ahmed    
--tv.underlyingbondflag as bondflag,                        
      (case                                                                                 
 when isnull(TV.UnderlyingBondFlag,0)  = 1 then 'YES'                                                                                
 when isnull(TV.UnderlyingBondFlag,0)  = 0 then 'NO'                                                                                
 end ) as BondFlag,    
          
--- Added By Zeeshan Ahmed to Use Main Court Information for Violation Update Screen                    
               
tv.courtviolationstatusidmain ,                    
tv.courtdatemain ,                    
tv.courtnumbermain,                  
tblcourts.shortName,                
tv.oscarcourtdetail ,              
c.categoryid  ,                 
isnull(c.ShortDescription,'') AS VerCourtDesc,              
tv.ticketid_pk  ,          
isnull(tv.coveringfirmid,0) as CoveringFirmID,--Added by M.Azwar Alam  on 27 August 2007 for task no 1126          
isnull(TV.ChargeLevel,0) as ChargeLevel,        
Convert ( int ,dbo.tblCourts.IsCriminalCourt) as IsCriminalCourt ,           
isnull ( TV.CDI  ,'0' ) as CDI ,                                 
@HasFTAViolations as hasFTAViolations,  
dbo.fn_CheckFTAViolation(TV.TicketsViolationID) as isFTAViolation,  
TV.ArrestDate,  
isnull(T.CaseTypeID ,0)as CaseTypeID,  
isnull(TV.MissedCourtType,-1) as  MissedCourtType                 
FROM    tblCourtViolationStatus AS c RIGHT OUTER JOIN  
  tblTicketsViolations AS tv INNER JOIN  
  tblTickets AS T ON T.TicketID_PK = tv.TicketID_PK LEFT OUTER JOIN  
  tblViolations AS v ON tv.ViolationNumber_PK = v.ViolationNumber_PK   
    ON c.CourtViolationStatusID = tv.CourtViolationStatusIDmain LEFT OUTER JOIN  
  tblCourts ON tv.CourtID = tblCourts.Courtid  
        
where tv.ticketid_pk =@TicketID_PK   --and                                            
and v.violationtype <> 1                                              
end  -- 16                                 
                                    
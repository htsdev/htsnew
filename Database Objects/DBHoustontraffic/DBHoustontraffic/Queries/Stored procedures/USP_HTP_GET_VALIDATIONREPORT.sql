﻿ /************************************************************
 * Code formatted by SoftTree SQL Assistant © v4.6.12
 * Time: 6/29/2010 1:38:11 PM
 ************************************************************/

/*  
* Created By		: SAEED AHMED
* Task ID			: 7791  
* Created Date		: 05/29/2010  
* Business logic	: This procedure is used to get the list of users who are associated with all active reports which are appearing 
*					: under validation. Each report is associated with two users[Primary & Secondary]. Each primary & secondary user is
*					: also associated with all six days[mon,tue,wed,thu,fri & sat]. If any primary/secondary user is
*					: not associated with any day the procedure will return blank entry for the user for that particular day.
* Parameter List  
* @active			: Represents an active reports in the system, this is an optinal parameter, if not supplied will display all active reports in the system
*					: otherwise will display reports based on the supplied value.
*
*/
CREATE PROC dbo.USP_HTP_GET_VALIDATIONREPORT 
@active AS BIT = TRUE               
AS          
  
  
CREATE TABLE #temp
(
	reportid  INT,
	title     VARCHAR(200),
	pri_mon   VARCHAR(20),
	pri_tue   VARCHAR(20),
	pri_wed   VARCHAR(20),
	pri_thu   VARCHAR(20),
	pri_fri   VARCHAR(20),
	pri_sat   VARCHAR(20),
	sec_mon   VARCHAR(20),
	sec_tue   VARCHAR(20),
	sec_wed   VARCHAR(20),
	sec_thu   VARCHAR(20),
	sec_fri   VARCHAR(20),
	sec_sat   VARCHAR(20)
)  
  
INSERT INTO #temp(reportid,title)
SELECT DISTINCT ID AS ReportID, TITLE
FROM   tbl_TAB_SUBMENU  WHERE  [active] = @active AND MENUID = 18 AND Category IN(1,2)	
  
CREATE TABLE #settings
(
	rowid      INT IDENTITY(1, 1),
	reportid   INT,
	userid     INT,
	username   VARCHAR(20),
	usertype   INT,
	daynumber  NCHAR(10)
)
  
INSERT INTO #settings (reportid, userid, username, usertype, daynumber)
SELECT m.ID AS reportid, u.EmployeeID, u.username, v.usertype, v.DayNumber
FROM   dbo.tblUsers AS u
       RIGHT OUTER JOIN dbo.ValidationReportSettings AS v
            ON  u.EmployeeID = v.UserID
       RIGHT OUTER JOIN dbo.tbl_TAB_SUBMENU AS m
            ON  v.ReportID = m.ID
WHERE  m.[active] = @active
       AND m.MENUID = 18 
       AND m.Category IN(1,2)	
       AND u.[Status]=1

DECLARE @idx       INT,
        @count     INT,
        @username  VARCHAR(20),
        @daynum    INT,
        @usertype  INT,
        @reportid  INT

SELECT @idx = 1,
       @count = COUNT(rowid)
FROM   #settings

WHILE @idx <= @count
BEGIN
    SELECT @username = username,
           @daynum = daynumber,
           @usertype = usertype,
           @reportid = reportid
    FROM   #settings
    WHERE  rowid = @idx 
    
    UPDATE #temp SET pri_mon = @username WHERE  @daynum = 2 AND @usertype = 0 AND reportid = @reportid
    UPDATE #temp SET pri_tue = @username WHERE  @daynum = 3 AND @usertype = 0 AND reportid = @reportid
    UPDATE #temp SET pri_wed = @username WHERE  @daynum = 4 AND @usertype = 0 AND reportid = @reportid
    UPDATE #temp SET pri_thu = @username WHERE  @daynum = 5 AND @usertype = 0 AND reportid = @reportid
    UPDATE #temp SET pri_fri = @username WHERE  @daynum = 6 AND @usertype = 0 AND reportid = @reportid
    UPDATE #temp SET pri_sat = @username WHERE  @daynum = 7 AND @usertype = 0 AND reportid = @reportid
    UPDATE #temp SET sec_mon = @username WHERE  @daynum = 2 AND @usertype = 1 AND reportid = @reportid
    UPDATE #temp SET sec_tue = @username WHERE  @daynum = 3 AND @usertype = 1 AND reportid = @reportid
    UPDATE #temp SET sec_wed = @username WHERE  @daynum = 4 AND @usertype = 1 AND reportid = @reportid
    UPDATE #temp SET sec_thu = @username WHERE  @daynum = 5 AND @usertype = 1 AND reportid = @reportid
    UPDATE #temp SET sec_fri = @username WHERE  @daynum = 6 AND @usertype = 1 AND reportid = @reportid
    UPDATE #temp SET sec_sat = @username WHERE  @daynum = 7 AND @usertype = 1 AND reportid = @reportid

    SELECT @idx = @idx + 1, 
           @username = NULL,
           @daynum = NULL,
           @usertype = NULL,
           @reportid = NULL
END

SELECT DISTINCT reportid, title, 
				pri_mon, pri_tue, pri_wed, pri_thu, pri_fri, pri_sat,
				sec_mon, sec_tue, sec_wed, sec_thu, sec_fri, sec_sat 
FROM   #temp
ORDER BY  title  

DROP TABLE #Temp          
DROP TABLE #settings

GO
GRANT EXECUTE ON dbo.USP_HTP_GET_VALIDATIONREPORT TO dbr_webuser
GO

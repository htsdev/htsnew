/*
Created by: Tahir Ahmed
Create Date: 09/10/2008

Business Logic:
	The stored procedure is used by Houston Traffic Program 
	to get the description of the Flag by flag Id.

List Of Parameters:
	@FlagId:	Id of the flag

List of Output Columns:
	Description: Returns the description of the flag.

*/

create procedure dbo.USP_HTP_Get_FlagDescription
	(
	@FlagId int
	)


as

select top 1 description from tbleventflags where flagid_pk = @FlagId

go

grant execute on dbo.USP_HTP_Get_FlagDescription to dbr_Webuser
go
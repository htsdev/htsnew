/****** 
Create by:  Syed Muhammad Ozair

Business Logic : this procedure is used to get the Scan batch detail for a perticular document id.

List of Parameters:	
	@DocumentBatchID : document id of the for which records will be fetched
	@ScanBatchID : scan batch id

List of Columns: 	
	BatchID : Batch id  
	LetterID : Document batch id
	DocumentType : type of document/report
	ScanDate : when it is scanned in to system
	PageCount : total no of pages of the document

******/

CREATE Procedure dbo.usp_HTP_Get_DocumentTracking_ViewBatchLetters

@DocumentBatchID int,
@ScanBatchID int

as

select sbd.scanbatchid_fk as BatchID,
	sbd.documentbatchid as LetterID,
	d.documentname as DocumentType,
	sb.scanbatchdate as ScanDate,
	sbd.documentbatchpagecount as PageCount
from	tbl_htp_documenttracking_scanbatch_detail sbd inner join tbl_htp_documenttracking_scanbatch sb
on	sbd.scanbatchid_fk=sb.scanbatchid inner join tbl_HTP_DocumentTracking_Batch b
on	sbd.documentbatchid=b.batchid inner join tbl_HTP_DocumentTracking_Documents d
on	b.documentid_fk=d.documentid
where	sbd.documentbatchid=@DocumentBatchID
and	sbd.scanbatchid_fk=@ScanBatchID
group by sbd.scanbatchid_fk, sbd.documentbatchid, d.documentname, sb.scanbatchdate, sbd.documentbatchpagecount

go

grant exec on dbo.usp_HTP_Get_DocumentTracking_ViewBatchLetters to dbr_webuser
go

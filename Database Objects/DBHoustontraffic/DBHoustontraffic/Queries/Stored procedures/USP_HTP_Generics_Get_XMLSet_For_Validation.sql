﻿--USP_HTP_Generics_Get_XMLSet_For_Validation 'traffictickets.dbo.usp_ALR_Request_Alert', ' ', 'HCCC - ALR Request Alert','ticketnumber as [Ticket Number], fname as [First Name], lname as [Last Name],courtdatemain as [Crt.Date/Time],courtnumber as [Crt.No], status as Status,shortname as [Crt.Name]'

--
/*
Author: Noufil Khan
Description: To get XML Schema of any SQL procedure
Business Logic : This procedure return XML on the given procedure.  
Parameters: 
	@ProcedureName : Source procedure name
	@Parameters : Source procedure's parameter values [Comma separated]
	@Subject : Title of XML report
	@ResultSet : Output parameter for generated XML schema
	@ColumnString : Column name which will be seen in the validation report	
	@serialNumber :  Serial number
	@orderby: Order by clause added for sorting.
	@SerialHyperlinkURL varchar(100) : Hyperlink URL for serial number.
	@PendingCount varchar(5) : pending count  
	@AlertCount varchar(5) : alert count
	@TotalCount varchar(5) : total number of rows
	@ShowHyperLink BIT : Hyper Link required at SNo or Not
	@Removecolumn varchar : Column name to be removed from @columnstring. This will be used while sorting by such column that 
							is in @columnstring but we dont need to display in report.

OutPut : 
	@ResultSet : This output parameter returns records in XML format.
	@PendingCount : This output parameter returns count of records 
					1. Follow up date in future.
						a) For No LOR sent report will show count of records leaving records court date with in 2 business days for HCJP and 4 business days for outside courts leaving HCJP
						b) No Certified Mail confirmation will show the count of certified mail number that are not confirmed before 4 days.
					2. Return 0 when there is no follow update in SP.
	@AlertCount :   This output parameter returns count of records 
					1. Follow up date past,today or null
						a) For No LOR sent report will show count of records have court date with in 2 business days for HCJP and 4 business days for outside courts leaving HCJP
						b) No Certified Mail confirmation will show the count of certified mail number that are not confirmed till 4 business days

					2. Return count of all clients when there is no follow update in SP.
	@TotalCount :   This output parameter returns count of clients
Type : Generic
Created date : Aug-1st-2008
Noufil 4432 08/08/2008

*/

/*
declare @ThisResultSetalert varchar(max)  
declare @ThisPendingCount varchar(5)  
declare @ThisAlertCount varchar(5)  
declare @ThisTotalCount varchar(5)
declare @ThisResultSetTrafficWaitingFollowUp varchar(max)
declare @SerialHyperlink varchar(200)
set @SerialHyperlink = 'newpkahouston.legalhouston.com'

declare @ThisResultidServicetraff varchar(max)  
	set @ThisTotalCount = 0   
	declare @Param2 varchar(200) 
	-- Noufil 5069 11/05/2008 New parameter value of date range added 
	--Sabir Khan 5738 05/25/2009  Add followupdate Column to Service ticket report...
	Set @Param2 = '''''' + convert(varchar(20),getdate(), 101) + ''''', ''''' + convert(varchar(20),getdate(), 101) + ''''', 3992,1,0,1,1'  
	  
	execute USP_HTP_Generics_Get_XMLSet_For_Validation   
	   'traffictickets.dbo.Usp_Hts_Get_OpenServiceTicketsRecords ',   
		@Param2,   
	   'Service Ticket Report (Traffic)',  
	   'TicketID_PK,casetypename as [Division],category as [Category],ClientName as [Name],courtdate as [Crt Date],courttime as [Crt Time],shortdescription as [Status],settinginformation as [Loc],recsortdate as [Open D],assignedto as [Open By],rec as [Update D],priority as [Priority],completion as [Comp.%],followupdate as [Follow Up Date]',  
	   1,'[Follow Up Date]',@SerialHyperlink,@ResultSet = @ThisResultidServicetraff output, @PendingCount = @ThisPendingCount OutPut,@AlertCount =@ThisAlertCount output  ,@TotalCount=@ThisTotalCount output  
	   
select @ThisPendingCount
select @ThisAlertCount
select @ThisResultidServicetraff
*/
 
Alter procedure [dbo].[USP_HTP_Generics_Get_XMLSet_For_Validation]
@ProcedureName varchar(150),
@Parameters varchar(500),
@Subject varchar(200) ,
@ColumnString varchar(max) ,
@serialNumber int = 1,
@orderby VARCHAR(200),
@SerialHyperlinkURL varchar(100), -- Noufil 5900 06/19/2009 Parameter added
@ResultSet varchar(max) = '' output,
@PendingCount varchar(5) = '' output,
@AlertCount varchar(5)= '' output,
@TotalCount varchar(5) = '' OUTPUT,
--Ozair 5929 05/21/2009
@ShowHyperLink BIT = 1,
--Waqas 6509 09/17/2009 added removecolumn parameter
@Removecolumn VARCHAR(MAX) = ''


as
begin
declare @execProc varchar(max)
declare @execResultSet varchar(max)
declare @ColumnHeader varchar(max)
declare @sqlString varchar(max)
declare @Session varchar(40)

Set @Session = NewID()

Set @Session = replace(@Session,'-','')

Set @execProc = @ProcedureName + ' ' + @Parameters

-- Noufil 6163 07/20/2009 Reset Count
SET @AlertCount='0'
SET @PendingCount='0'
SET @TotalCount='0'

create table #resultSet_Outer(TempCol varchar(1))
create table #columnNames_Outer(column_name varchar(100), data_type varchar(20), character_maximum_length varchar(50), RowID int)
create table #XML_Outer (XML_String varchar(max), PendingCount varchar(5),AlertCount varchar(5))
insert into #XML_Outer values('','','')
--////////////////////////// Getting Column Header /////////////////////////////////////////////////////

if @serialNumber = 1
	set @ColumnString = ' ''.'' as S#, ' + @ColumnString

-- Saeed 7791 07/12/2010 declare a variable to hold order by clause.
DECLARE @orderbyClause VARCHAR(3000)
SET @orderbyClause =''

-- Saeed 7791 07/12/2010 creating order by clause.
IF (RTRIM(LTRIM(@orderby))<> '' OR CHARINDEX('ticketid_pk',@ColumnString)>0)
BEGIN
	
	-- Rab Nawaz Khan 11431 12/13/2013 Added for LMS Address check report by rec date desc. . . 
	IF (RTRIM(LTRIM(@orderby))<> '' AND LTRIM(RTRIM(@orderby)) LIKE 'RecDate DESC')
		Set @orderbyClause = ' order by ' + @orderby
	-- If order by parameter is not empty and columns contains ticketid_pk then sort with order by parameter + ticketid_pk	
	ELSE IF (RTRIM(LTRIM(@orderby))<> '' AND CHARINDEX('ticketid_pk',@ColumnString)>0)		
		Set @orderbyClause = ' order by ' + CASE WHEN CHARINDEX('date', @orderby) > 0  THEN ' CONVERT(DATETIME, ISNULL('+ @orderby+ ',''01/01/1900''))' ELSE @orderby END + ', ticketid_pk '				
	-- If order by parameter is not empty and columns does not contains ticketid_pk then sort with order by parameter
	ELSE IF (RTRIM(LTRIM(@orderby))<> '' AND CHARINDEX('ticketid_pk',@ColumnString) = -1)
		Set @orderbyClause = ' order by ' + CASE WHEN CHARINDEX('date', @orderby)  = -1  THEN @orderby ELSE ' CONVERT(DATETIME, ISNULL('+ @orderby+ ',''01/01/1900''))' END
	-- If order by parameter is empty and columns contains ticketid_pk then sort with ticketid_pk
	ELSE IF (RTRIM(LTRIM(@orderby)) = '' AND CHARINDEX('ticketid_pk',@ColumnString) > 0)
		Set @orderbyClause = ' order by ticketid_pk '		
END

-- Afaq Ahmed 8213 11/02/2010 Fix general comments data type length issue
Set @execResultSet = '
SELECT ' + @ColumnString + ' into #resultSet_Inner' + @Session + ' FROM OPENQUERY(LinkSrvData,''' + @execProc +''')
select column_name, data_type, Case character_maximum_length WHEN -1 THEN 4000 ELSE character_maximum_length END AS character_maximum_length 
into #columnNames_Inner from tempdb.information_schema.columns where table_name like ''#resultSet_Inner' + @Session + '__%'' 

Alter Table #columnNames_Inner add RowID int identity(1,1)
truncate table #columnNames_Outer
insert into #columnNames_Outer select * from #columnNames_Inner 

declare @ColumnCount int, @LoopCounter int
declare @sqlString varchar(max)

select @ColumnCount = count(column_name) from #columnNames_Inner
Set @LoopCounter = 1
set @sqlString = ''Alter table #resultSet_Outer  add ''
while @LoopCounter <= @ColumnCount
	begin
		
		select @sqlString = @sqlString + '', ['' + column_name + ''] '' + data_type + (case when isnull(character_maximum_length,'''') <> '''' then ''('' + convert(varchar(20),character_maximum_length) + '')'' else '''' end) from #columnNames_Inner where RowID = @LoopCounter
		Set @LoopCounter = @LoopCounter + 1
	end
set @sqlString = replace(@sqlString,''add ,'',''add '')
exec (@sqlString)

DECLARE @avar VARCHAR(100)
Alter Table #resultSet_Outer drop column TempCol 
-- Saeed 7791 07/12/2010 add [follow up date] column in temporary table, if report does not contains that colum. The field is added to support the functionality of changing report category from report to alert and vice versa.
-- Rab Nawaz Khan 11473 11/18/2013 Report has been changed From Online inquiries To Leads. . . 
if (''' + @Subject + '''<> ''Report Summary'' and ''' + @Subject + '''<> ''Alert Summary'' and CHARINDEX(''Leads'',''' + @Subject + ''')<=0 and CHARINDEX(''Online Failed Transaction'',''' + @Subject + ''')<=0 ) AND not exists(SELECT * FROM tempdb.information_schema.columns where table_name LIKE ''#resultSet_Outer%'' AND COLUMN_NAME like ''Follow Up%'')
BEGIN	
	ALTER TABLE #resultSet_Outer ADD [Follow Up Date] varchar(20) DEFAULT('''')	
	INSERT INTO #columnNames_Outer(column_name, data_type, character_maximum_length) VALUES(''Follow Up Date'',''varchar'',20) 
	insert into #resultSet_Outer select *,'''' from #resultSet_Inner' + @Session + @orderbyClause +'
	
END
else
BEGIN
	insert into #resultSet_Outer select * from #resultSet_Inner' + @Session  + @orderbyClause + '
	
END'

---- Noufil 5900/5857 09/06/2009 set order by clause.
---- Waqas 5864 07/15/2009 Issue identified by waqas. Fixed by Adil & Noufil
--IF (RTRIM(LTRIM(@orderby))<> '' OR CHARINDEX('ticketid_pk',@ColumnString)>0)
--BEGIN
--	
--	-- If order by parameter is not empty and columns contains ticketid_pk then sort with order by parameter + ticketid_pk
--	
--	IF (RTRIM(LTRIM(@orderby))<> '' AND CHARINDEX('ticketid_pk',@ColumnString)>0)		
--	BEGIN
--		Set @execResultSet = @execResultSet+ ' order by ' + CASE WHEN CHARINDEX('date', @orderby) > 0  THEN ' CONVERT(DATETIME, ISNULL('+ @orderby+ ',''01/01/1900''))' ELSE @orderby END + ', ticketid_pk '
--		
--	END
--		
--		
--	-- If order by parameter is not empty and columns does not contains ticketid_pk then sort with order by parameter
--	ELSE IF (RTRIM(LTRIM(@orderby))<> '' AND CHARINDEX('ticketid_pk',@ColumnString) = -1)
--		Set @execResultSet = @execResultSet+ ' order by ' + CASE WHEN CHARINDEX('date', @orderby)  = -1  THEN @orderby ELSE ' CONVERT(DATETIME, ISNULL('+ @orderby+ ',''01/01/1900''))' END
--	-- If order by parameter is empty and columns contains ticketid_pk then sort with ticketid_pk
--	ELSE IF (RTRIM(LTRIM(@orderby)) = '' AND CHARINDEX('ticketid_pk',@ColumnString) > 0)
--		Set @execResultSet = @execResultSet+ ' order by ticketid_pk '
--		
--END


Set @execResultSet = @execResultSet + 
' drop table #resultSet_Inner' + @Session + 
' drop table #columnNames_Inner'

Set @execResultSet = Replace(@execResultSet,'[[','[')
Set @execResultSet = Replace(@execResultSet,']]',']')
exec(@execResultSet)



set @ColumnHeader = '<style type="text/css">.clsLeftPaddingTable
			{
				PADDING-LEFT: 5px;
				FONT-SIZE: 8pt;
				COLOR: #123160;
				FONT-FAMILY: Tahoma;
				background-color:EFF4FB;
				border-collapse:collapse;
				border-width:1px;
			}
			
			</style>			
			<H2 align="center"><font color="#3366cc"><STRONG>' + @Subject + '</strong></font></H2>
			<table border="1" cellpadding="0" cellspacing="0" width="100%" bgcolor="#EFF4FB" class="clsLeftPaddingTable" BorderColor="black">
			<tr>'
--////////////////////////// Getting ResultSet /////////////////////////////////////////////////////


if exists (Select * from #resultSet_Outer)
BEGIN
	
   --========================================================
if @serialNumber = 1
BEGIN
	
	DECLARE @isserviceticket BIT
	SET @isserviceticket=0
	IF (@Subject = 'Service Ticket Report (Traffic)' OR @Subject = 'Service Ticket Report (criminal)' OR @Subject = 'Service Ticket Report (Civil)' OR @Subject='Service Ticket Report (Family)') -- Noufil 6107 07/04/2009 Family Law Subject added.
	BEGIN
		SET @isserviceticket=1
		-- Calculate alert count having followupdate null,past or today
		--Sabir Khan 6245 08/12/2009 Fixed Due and pending issue by using fid column intead of ticketid_pk...
		SELECT @AlertCount= ISNULL((SELECT COUNT(DISTINCT FID) FROM #resultSet_Outer 
		                           WHERE DATEDIFF(DAY,GETDATE(),ISNULL([Follow Up Date],'01/01/1900')) <= 0 OR (DATEDIFF(DAY,GETDATE(),ISNULL([Follow Up Date],'01/01/1900')) > 0 AND RTRIM(LTRIM([Comp.%]))='0') ),'0')
			        
		-- Calculate Pending count having followupdate null,past or today
			--Sabir Khan 6245 08/12/2009 Fixed Due and pending issue by using fid column intead of ticketid_pk...
		SELECT @PendingCount= ISNULL((SELECT COUNT(DISTINCT FID) FROM #resultSet_Outer 
		                            WHERE (DATEDIFF(DAY,GETDATE(),ISNULL([Follow Up Date],'01/01/1900')) > 0 AND RTRIM(LTRIM([Comp.%])) <> '0') ),'0')
		                            
		
		DELETE FROM #resultSet_Outer WHERE DATEDIFF(DAY,GETDATE(),ISNULL([Follow Up Date],'01/01/1900')) > 0 AND RTRIM(LTRIM([Comp.%])) <> '0'
		
			--Sabir Khan 6245 08/12/2009 Remove fid column...
		DELETE FROM #columnNames_Outer WHERE COLUMN_NAME LIKE 'FID' 		
	END
	----------------------------------------------------------------------------------------------------------------------------------------
		--Afaq 8521 12/13/2010 Add new report Potential Bond Forfeiture
	ELSE IF (CHARINDEX('Potential Bond Forfeiture',@Subject)>0 )
			BEGIN
				SET @isserviceticket=1
				-- kashif 8771 07/03/2011 change formated [Crt Date & Time] to CONVERT(DATETIME,[Crt Date & Time])
				SELECT @AlertCount = ISNULL((SELECT COUNT(DISTINCT ticketid_pk) FROM #resultSet_Outer Where DATEDIFF(DAY, isnull(CONVERT(DATETIME,[Crt Date & Time]),'01/01/1900'), GETDATE() + 1) <= 0 AND DATEDIFF(DAY, isnull(CONVERT(DATETIME,[Crt Date & Time]),'01/01/1900'),dbo.fn_HTS_GET_Nth_NextBusinessDay(GETDATE(), 3)) >= 0),'0')
				-- kashif 8771 07/03/2011 change formated [Crt Date & Time] to CONVERT(DATETIME,[Crt Date & Time])
				SELECT @PendingCount = ISNULL((SELECT COUNT(DISTINCT ticketid_pk) FROM #resultSet_Outer Where DATEDIFF(DAY, isnull(CONVERT(DATETIME,[Crt Date & Time]),'01/01/1900'),dbo.fn_HTS_GET_Nth_NextBusinessDay(GETDATE(), 4)) <= 0 AND DATEDIFF(DAY, isnull(CONVERT(DATETIME,[Crt Date & Time]),'01/01/1900'),dbo.fn_HTS_GET_Nth_NextBusinessDay(GETDATE(), 5)) >= 0),'0')
				-- kashif 8771 07/03/2011 change formated [Crt Date & Time] to CONVERT(DATETIME,[Crt Date & Time])
				DELETE FROM #resultSet_Outer WHERE DATEDIFF(DAY, isnull(CONVERT(DATETIME,[Crt Date & Time]),'01/01/1900'),dbo.fn_HTS_GET_Nth_NextBusinessDay(GETDATE(), 4)) <= 0 	
			END	
	----------------------------------------------------------------------------------------------------------------------------------------

	
	DELETE FROM #columnNames_Outer WHERE COLUMN_NAME LIKE 'TICKETID_PK' 
	ALTER TABLE #columnNames_Outer 
		DROP COLUMN RowID 

	ALTER TABLE #columnNames_Outer 
		ADD RowID INT IDENTITY(1,1) 

	--DELETE FROM #columnNames_Inner WHERE COLUMN_NAME LIKE 'TICKETID_PK'

	--select @TotalCount = count(*) from #resultSet_Outer
--select Distinct  count(ticketid_pk ) from #resultSet_Outer

 

	alter table #resultSet_Outer 
	add Sno int identity(1,1) 

	alter table #resultSet_Outer 
	add StrSno varchar(300) 


	declare @rowCount int, @chkCount int, @rowNumber int, @SrlNumber int

	set @chkCount = 0 
	set @rowNumber = 1
	set @SrlNumber = 0

	select @chkCount = count(*) from #resultSet_Outer

	declare @Previousticketid int, @CurrentTicketid int 

	set @Previousticketid = 0
	set @CurrentTicketid = 0
	

	while @rowNumber <= @chkCount
	begin 
		select @CurrentTicketid = ticketid_pk from #resultSet_Outer where sno = @rowNumber
		if 	@CurrentTicketid = @Previousticketid 
			begin 
				--Ozair 5929 05/21/2009 show hyperlink S.No else not. 
				IF(@ShowHyperLink=1)
				BEGIN
					update #resultSet_Outer set StrSno = '<a href="http://' + @SerialHyperlinkURL + '/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber='+convert(varchar(30), @CurrentTicketid)+'" > . </a>' where sno = @rowNumber	
				END
				ELSE
				BEGIN
						update #resultSet_Outer set StrSno = '.' where sno = @rowNumber	
				END
			end 
		else 
			begin 				
				set @SrlNumber = @SrlNumber + 1
				--Ozair 5929 05/21/2009 show hyperlink S.No else not.
				IF(@ShowHyperLink=1)
				BEGIN
					update #resultSet_Outer set StrSno = '<a href="http://' + @SerialHyperlinkURL + '/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber='+convert(varchar(30), @CurrentTicketid)+'" > ' + convert(varchar(20),@SrlNumber) + ' </a>'  where sno = @rowNumber
				END
				ELSE
				BEGIN
						update #resultSet_Outer set StrSno = convert(varchar(20),@SrlNumber) where sno = @rowNumber
				END
			end 
		set @rowNumber= @rowNumber + 1
		set @Previousticketid = @CurrentTicketid 

	end 

select @TotalCount=@SrlNumber
	-- Noufil 6096 07/01/2009 serial number issue fixed. Delete records before serial number is generated.
	-- Saeed 8005 07/22/2010 apply CHARINDEX function cause subject contains 'No Lor Sent (Alert)/(Report)'
		 IF (CHARINDEX('NO LOR Sent',@Subject)>0 AND @isserviceticket = 0) -- Noufil 6011 06/12/2009 set alert and pending count.		 		 
			BEGIN							
				-- Calculate alert count having court date with 2 business days for HCJP and 4 business days four outside courts leaving HCJP
				SELECT @AlertCount= ISNULL((SELECT COUNT(DISTINCT ticketid_pk) FROM #resultSet_Outer 
				                            WHERE (CourtID IN (3007,3008,3009,3010,3011,3012,3013,3014,3015,3016,3017,3018,3019,3020,3021,3022) 
													--AND DATEDIFF(DAY,GETDATE(),[Court Date]) >= 0 --Saeed 8005 07/21/2010 clients having appearance date within 2 business days & past for HCJP cases 
													AND DATEDIFF(DAY,[Court Date],dbo.fn_getnextbusinessday(GETDATE(),2)) >= 0)											
				                           --Saeed 8005 07/22/2010 Court id 3004 [Pasadena Municipal Court] removed. In detail section of validation report, records of 3004 is displaying therefore remove 3004 court id from here so that it will also display the count.
				                           OR (CourtID NOT IN (3001,3002,3003,3007,3008,3009,3010,3011,3012,3013,3014,3015,3016,3017,3018,3019,3020,3021,3022) 
													--AND DATEDIFF(DAY,GETDATE(),[Court Date]) >= 0 --Saeed 8005 07/21/2010 clients having appearance date within 4 business days & past for outside court leaving HCJP cases 
													AND DATEDIFF(DAY,[Court Date],dbo.fn_getnextbusinessday(GETDATE(),4)) >= 0)	
										),'0')										

				-- -- Calculate alert count having court date not within 2 business days for HCJP and 4 business days four outside courts leaving HCJP
				SELECT @PendingCount= ISNULL((SELECT COUNT(DISTINCT ticketid_pk) FROM #resultSet_Outer 
				                            WHERE (CourtID IN (3007,3008,3009,3010,3011,3012,3013,3014,3015,3016,3017,3018,3019,3020,3021,3022)
													AND (
														
														--DATEDIFF(DAY,GETDATE(),[Court Date]) < 0 OR  ----Saeed 8005 07/21/2010 clients not having appearance date within 2 business days & past for HCJP cases 
														DATEDIFF(DAY,dbo.fn_getnextbusinessday(GETDATE(),2),[Court Date]) > 0))
				                            --Saeed 8005 07/22/2010 Court id 3004 [Pasadena Municipal Court] removed. In detail section of validation report, records of 3004 is displaying therefore remove 3004 court id from here so that it will also display the count.
				                            OR (CourtID NOT IN (3001,3002,3003,3007,3008,3009,3010,3011,3012,3013,3014,3015,3016,3017,3018,3019,3020,3021,3022) 
												AND (
												--DATEDIFF(DAY,GETDATE(),[Court Date]) < 0 OR  --Saeed 8005 07/21/2010 clients not having appearance date within 4 business days & past for outside court leaving HCJP cases 
												DATEDIFF(DAY,dbo.fn_getnextbusinessday(GETDATE(),4),[Court Date]) > 0))),'0')
				
				DELETE FROM #columnNames_Outer WHERE column_name = 'CourtID'
				ALTER TABLE #resultSet_Outer
					DROP COLUMN CourtID			
			END	
		-- Saeed 8005 07/22/2010 apply CHARINDEX function cause subject contains 'No Certified Mail Confirmation (Alert)/(Report)'
		ELSE IF (CHARINDEX('No Certified Mail Confirmation',@Subject)>0 AND @isserviceticket = 0)  -- Noufil 6011 06/12/2009 set alert and pending count.
			BEGIN
				-- Calculate alert having ceritifend mail confirmation number in pending status and not confirm till 4 business days			
				SELECT @AlertCount= (ISNULL((SELECT COUNT(DISTINCT USPSTrackingNumber) FROM #resultSet_Outer WHERE [Delivery Status] = 'Pending' AND ticketid_pk IN
												(SELECT DISTINCT ticketid_pk FROM #resultSet_Outer GROUP BY ticketid_pk 
												 HAVING DATEDIFF(DAY,dbo.fn_getnextbusinessdayWithoutSunday(MIN([Send Date & Time]),4),GETDATE()) > 0) ),'0'))
				-- Calculate pending having certified mail number in pending status and not confirmation is pending.
				SELECT @PendingCount= (ISNULL((SELECT COUNT(DISTINCT USPSTrackingNumber) FROM #resultSet_Outer WHERE [Delivery Status] = 'Pending' 
										AND ticketid_pk IN (SELECT DISTINCT ticketid_pk FROM #resultSet_Outer GROUP BY ticketid_pk 
										having DATEDIFF(DAY,dbo.fn_getnextbusinessdayWithoutSunday(MIN([Send Date & Time]),4),GETDATE()) <= 0 )),'0'))
										
			END
				-- Noufil 5900 09/06/2009 Calculate alert and pending count if Follow up date column is avalable.
		ELSE IF EXISTS(SELECT * FROM #columnNames_Outer WHERE COLUMN_NAME LIKE 'Follow Up Date') AND @isserviceticket = 0		
			BEGIN
				
				-- Calculate alert count having followupdate null,past or today
				SELECT @AlertCount= ISNULL((SELECT COUNT(DISTINCT ticketid_pk) FROM #resultSet_Outer WHERE DATEDIFF(DAY,GETDATE(),ISNULL([Follow Up Date],'01/01/1900')) <= 0),'0')
				-- Calculate pending count having followupdate future.
				SELECT @PendingCount= ISNULL((SELECT COUNT(DISTINCT ticketid_pk) FROM #resultSet_Outer WHERE DATEDIFF(DAY,GETDATE(),ISNULL([Follow Up Date],'01/01/1900')) > 0),'0')	
				-- Noufil 6096 07/01/2009 Don't delete column for payment due call report.
				-- Saeed 8005 07/22/2010 apply CHARINDEX function cause subject contains 'Payment Due Calls Alert (Alert)/(Report)'
				IF(CHARINDEX('Payment Due Calls Alert',@Subject)<1 )
				BEGIN
					DELETE FROM #resultSet_Outer WHERE DATEDIFF(DAY,GETDATE(),ISNULL([Follow Up Date],'01/01/1900')) > 0	
				END
				
			END
								
			
		-- Noufil 5900 09/06/2009 Calculate alert and pending count if Follow up date column is avalable.		
		ELSE IF (@isserviceticket = 0)
		BEGIN
			--select('Else')
				SELECT @AlertCount= ISNULL(@SrlNumber,'0')
				SELECT @PendingCount='0'
			END
		

	alter table #resultSet_Outer 
		alter column s# varchar(300)


	Update t 
	set t.s# = t1.strSno 
	from #resultSet_Outer t inner join #resultSet_Outer t1
		on t.sno = t1.sno 

	alter table #resultSet_Outer 
		drop column Sno 

	alter table #resultSet_Outer 
		drop column ticketid_pk

	alter table #resultSet_Outer 
		drop column strSno

END
ELSE
	BEGIN 
		
		-- Noufil 5900 09/06/2009 Calculate alert and pending count if Follow up date column is avalable.	
		IF EXISTS(SELECT * FROM #columnNames_Outer WHERE COLUMN_NAME LIKE 'Follow Up Date')
			BEGIN
				
				-- Calculate alert count having followupdate null,past or today				
				SELECT @AlertCount= ISNULL((SELECT COUNT(DISTINCT ticketid_pk) FROM #resultSet_Outer WHERE DATEDIFF(DAY,GETDATE(),ISNULL([Follow Up Date],'01/01/1900')) <= 0),'0')
				-- Calculate pending count having followupdate future.
				SELECT @PendingCount= ISNULL((SELECT COUNT(DISTINCT ticketid_pk) FROM #resultSet_Outer WHERE DATEDIFF(DAY,GETDATE(),ISNULL([Follow Up Date],'01/01/1900')) > 0),'0')	
			END
		-- Noufil 5900 09/06/2009 Calculate alert and pending count if Follow up date column is avalable.
		ELSE IF EXISTS (SELECT * FROM #columnNames_Outer WHERE COLUMN_NAME LIKE 'ticketid_pk')
			BEGIN
				SELECT @AlertCount= ISNULL((SELECT COUNT(DISTINCT ticketid_pk) FROM #resultSet_Outer),'0')
				SELECT @PendingCount='0'
				
				DELETE FROM #columnNames_Outer WHERE column_name = 'ticketid_pk'
				ALTER TABLE #resultSet_Outer
					DROP COLUMN ticketid_pk
			END
		ELSE
			SELECT @AlertCount= ISNULL((SELECT COUNT(*) FROM #resultSet_Outer),'0')
			SELECT @PendingCount='0'	
	
	END
	
END

--Waqas 6509 09/18/2009 Removing columns
--===========================================================
-- Noufil 6163 07/20/2009 If exist check added if Records exists then create HTML else show No records founds.

if exists (Select * from #resultSet_Outer)
Begin 
	
	declare @ColumnCount int, @LoopCounter INT	
	select @ColumnCount = count(column_name) from #columnNames_Outer
	Set @LoopCounter = 1	
	
	while @LoopCounter <= @ColumnCount
	begin
		DECLARE @colName AS VARCHAR(MAX)
		select @ColumnHeader = @ColumnHeader + '<td ><font color="#3366cc"><STRONG>' + column_name + '</strong></font></td>' from #columnNames_Outer where RowID = @LoopCounter 
		AND CHARINDEX(column_name, @Removecolumn) = 0
		Set @LoopCounter = @LoopCounter + 1		
	end
	
	select @sqlString = 'declare @XMLStr varchar(max); Set @XMLStr = Cast ( (  select '
	Set @LoopCounter = 1
	
	while @LoopCounter <= @ColumnCount
	begin
	select @sqlString = @sqlString + 'td= (Case isnull([' + column_name + '],'''') when '''' then '' '' else [' + column_name + '] end) ,'''',' from #columnNames_Outer where RowID = @LoopCounter  
	AND CHARINDEX(column_name, @Removecolumn) = 0
	Set @LoopCounter = @LoopCounter + 1
	end

	--Set @sqlString = @sqlString + ' from #resultSet_Outer FOR XML PATH (''tr''), Type) as varchar(max) )+ ''</table>''; update #XML_Outer set XML_String =@XMLStr'
	-- Noufil 5524 03/26/2009 Set order by when Hyperlink is not use.
	Set @sqlString = @sqlString + ' from #resultSet_Outer '
	
	if (@serialNumber = 0 AND RTRIM(LTRIM(@Subject)) <> 'Alert Summary' AND RTRIM(LTRIM(@Subject)) <> 'Report Summary')	
		begin 
			
			Set @sqlString = @sqlString + ' order by 1'
		end 
	
	Set @sqlString = @sqlString + ' FOR XML PATH (''tr''), Type) as varchar(max) )+ ''</table>''; update #XML_Outer set XML_String =@XMLStr'
	
	
	Set @sqlString = Replace(@sqlString,', from',' from')
	Set @sqlString = Replace(@sqlString,'[[','[')
	Set @sqlString = Replace(@sqlString,']]',']')
	
	exec(@sqlString)
	-- /////////////////////////  ResultSet Returned  //////////////////////////////
	Select @ResultSet = @ColumnHeader + isnull(XML_String, '') from #XML_Outer	
	
	--, @PendingCount  = PendingCount  ,@AlertCount=AlertCount 

End
Else
Begin
	-- /////////////////////////  ResultSet Returned  //////////////////////////////
	Select @ResultSet = @ColumnHeader + '<td align="center" ><font color="#3366cc"> No Record Found </font></td></tr></table>'
	-- Noufil 6163 07/20/2009 code comment and added on top.
	--SELECT @AlertCount ='0'	
	--SELECT @PendingCount ='0'
End

Drop Table #resultSet_Outer
Drop Table #columnNames_Outer
end

﻿ /****************    
Created By : Zeeshan Ahmed    
    
Business Logic : This procedure is used to get case information for sullolaw case search page    
    
List Of Input Parameters :    
    
 @LastName : Last Name    
 @DLNumber : Driver License No.                
 @DOB   : Date Of Birth                
 @ZipCode  : Zip Code    
    
List Of Columns :    
    
 TicketID_PK    
 Firstname    
 Lastname    
 Address1    
 Address2    
 State    
 City    
 zip    
 TicketNumber,      
 CourtNumber,    
 Status,    
 CourtLocation,    
 ViolationDescription    
 CourtDate,    
 CourtAddress,          
 ClientAddress,          
 ViolationStatusID,          
 ShortName,          
 CourtID,          
 ViolationDescriptionToolTip           
    
***************/    
--[usp_ps_get_customer_Status] 'AVILA','34234', '12/23/1969','7708'      
--[usp_ps_get_customer_Status] 'AVILA','', '12/23/1969','7705'          
  
--Waqas 5428 02/16/2009 Replacing dynamic query with modification.       
ALTER PROCEDURE [dbo].[usp_ps_get_customer_Status] --'HACKNEY','09147725','',''
	@LastName VARCHAR(50),
	@DLNumber VARCHAR(20),
	@DOB VARCHAR(15),
	@ZipCode VARCHAR(15)
AS
	SELECT t.TicketID_PK,
	       t.Firstname,
	       t.Lastname,
	       t.Address1,
	       t.Address2,
	       st2.State,
	       st2.StateID
	       t.City,
	       t.zip,
	       CONVERT(NVARCHAR, tv.RefCaseNumber) AS TicketNumber,
	       '# ' + ISNULL(tv.CourtNumbermain, '') AS CourtNumber,
	       cvs.Description AS STATUS,
	       c.shortcourtname AS CourtLocation,
	       CASE 
	            WHEN LEN(v.Description) > 20 THEN (LEFT(v.Description, 20) + '....')
	            ELSE v.Description
	       END AS ViolationDescription,
	       tv.CourtDateMain AS CourtDate,
	       c.Address + ', ' + c.City + ', ' + st.State + ', ' + c.Zip AS CourtAddress,
	       t.Address1 + ', ' + t.City + ', ' + st2.State + ', ' + t.Zip AS ClientAddress,
	       tv.courtviolationstatusidmain AS ViolationStatusID,
	       c.ShortName,
	       c.CourtID,
	       v.Description AS ViolationDescriptionToolTip,
	       
	FROM   tblTickets AS t
	       INNER JOIN tblTicketsViolations AS tv
	            ON  t.TicketID_PK = tv.TicketID_PK
	       INNER JOIN tblCourts AS c
	            ON  tv.CourtID = c.Courtid
	       INNER JOIN tblCourtViolationStatus AS cvs
	            ON  tv.CourtViolationStatusIDmain = cvs.CourtViolationStatusID
	       INNER JOIN tblViolations AS v
	            ON  tv.ViolationNumber_PK = v.ViolationNumber_PK
	       INNER JOIN tblState AS st
	            ON  c.State = st.StateID
	       INNER JOIN tblState AS st2
	            ON  t.Stateid_FK = st2.StateID
	WHERE  t.Activeflag = 1 
	       --Waqas 5428 02/26/2009 Checking Disposed category
	       --AND  tv.CourtViolationStatusIDMain NOT IN (80, 236)
	       AND cvs.CategoryID <> 50
	       AND t.Lastname = @LastName
	       AND (
	               (LEN(@DLNumber) > 0 AND t.DLNumber = @DLNumber)
	               OR (
	                      LEN(@DLNumber) = 0
	                      AND DATEDIFF(DAY, t.DOB, CONVERT(DATETIME, @DOB)) = 0
	                      AND t.zip LIKE(@ZipCode + '%')
	                  )
	           )
	ORDER BY
	       t.TicketID_PK DESC,
	       tv.CourtID ASC,
	       tv.CourtDateMain DESC    
    
    
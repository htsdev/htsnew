SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_WebScan_GetAdminBatch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_WebScan_GetAdminBatch]
GO


CREATE procedure [dbo].[usp_WebScan_GetAdminBatch]          
    
as             
            
select o.picid,            
   o.causeno,            
   o.status,          
   o.courtno,              
   convert(varchar(12),o.newcourtdate,101) + ' ' +o.time +' '+ '#'+o.courtno as courtdate,            
   convert(varchar(12),o.todaydate,101) as todaydate,  
   convert(varchar(12),o.newcourtdate,101) as cd,  
   convert(varchar(12),o.todaydate,101) as td,               
   o.location,            
   o.time,    
   b.batchid,    
   convert(varchar(12),tv.courtdate,101)  + ' ' +o.time +' '+ '#'+o.courtno as autocourtdate,    
   convert(varchar(12),tv.courtdatemain,101)  + ' ' +o.time +' '+ '#'+o.courtno as courtdatemain,    
   tv.courtid     
            
from tbl_WebScan_Batch b            
            
 inner join tbl_WebScan_pic p on    
 b.batchid=p.batchid inner join tbl_webscan_ocr o on     
 p.id=o.picid inner join     
 tblticketsviolations tv on     
 o.causeno=tv.casenumassignedbycourt    
    
            
where             
  o.scanverified=1 and o.adminverified =0           
order by o.picid       


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


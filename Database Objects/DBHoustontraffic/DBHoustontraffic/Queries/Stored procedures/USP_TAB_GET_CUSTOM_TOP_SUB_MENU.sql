/****** 
	Zeeshan Haider 11387 09/09/2013
	Business Logic: This procedure is used to Get active Top and Sub menu with associated URL by Top Menu, Sub Menu ID for custom Access to specific rep.
	Parameters : @TopMenuID, @SubMenuID
	Column Returns : IDMAIN, ID,TITLE,ORDERING,ACTIVE,URL,
******/
CREATE PROCEDURE [dbo].[USP_TAB_GET_CUSTOM_TOP_SUB_MENU]
(@sUserName VARCHAR(20) = NULL)
AS
BEGIN
	IF(@sUserName = 'adominguez')
	BEGIN
		CREATE TABLE #topMenu
		(
			idmain INT,
			id INT,
			title NVARCHAR(100),
			ordering INT,
			[active] BIT,
			[url] NVARCHAR(1000),
			issubmenuhidden BIT
		)
		INSERT INTO #topMenu
		EXEC usp_tab_gettopmenu			
		
		CREATE TABLE #subMenu
		(
			id INT,
			title NVARCHAR(100),
			[url] NVARCHAR(1000),
			ordering INT,
			selected INT,
			menuid INT,
			[active] INT,
			isadminlog BIT,
			applicationid INT,
			category INT,
			iconname VARCHAR(100)
		)
		INSERT INTO #subMenu
		EXEC USP_TAB_GETSUBMENU_by_MENUID
		
		UPDATE #topMenu
		SET [url] = (SELECT [URL]  FROM tbl_TAB_SUBMENU WHERE ID = 260), id = 260
		WHERE idmain = 19
		
		UPDATE #subMenu
		SET selected = 1
		WHERE id = 260
		
		DELETE FROM #subMenu WHERE id <> 260 AND menuid = 19	
		
		SELECT * FROM #topMenu
		SELECT * FROM #subMenu	ORDER BY menuid ASC
			
		DROP TABLE #topMenu
		DROP TABLE #subMenu
	END
	ELSE
		BEGIN
			EXEC USP_TAB_GETTOPMENU
			EXEC USP_TAB_GETSUBMENU_by_MENUID
		END	
END
GO

GRANT EXECUTE ON [dbo].[USP_TAB_GET_CUSTOM_TOP_SUB_MENU] TO dbr_webuser
GO



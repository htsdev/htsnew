

/****** 
Created by:		Rab Nawaz Khan
Task:			11431
BusINess Logic:	The procedure is used by LMS application to get data for Harris County CrimINal Updated Addresses letter
				AND to create LMS history for this letter for the SELECTed date range.
				
List of Parameters:
	@catnum:		Category number of the SELECTed letter type, IF prINTINg for 
					all courts of the SELECTed court category
	@LetterType:	SELECTed letter type IN LMS
	@CrtId:			Court id of the SELECTed court category IF prINTINg for INdividual court hosue.
	@startListdate:	Comma separated dates SELECTed to prINT letter.
	@empid:			Employee INformation of the logged IN employee.
	@PrINTType:		Flag to just preview the letter or to mark letters as sent. IF 1 then LMS letter history will be created.
	@SearchType:	Flag that identIFies IF searchINg by court date or by list date. 0= list date, 1 = court date
	@isPrINTed:		Flag to INclude already prINTed letters IN the batch.

List of Columns:	
	LetterId:		Letter ID generated for each letter. It will be "NOT PRINTABLE"	IF @PrINTType = 0
	ClientId:		Identity value to look up the case IN non-clients. used to group the records IN report file.
	Name:			Person's first AND last name that will appear on the letter
	Address:		Person's Home address that will appear on the letter
	City:			Person's Home City that will appear on the letter
	State:			Person's Home State that will appear on the letter
	Zip:			Zip code associated with person home address
	RecordId:		BookINg Number of the case
	Span:			SPN number associated with the person
	ArrestDate:		Date when person arrested
	ChargeWordINg:	Description of charge on the person
	ChargeLevel:	Charge level associated with the charge wordINg
	Disposition:	Disposition status of the case
	BookINgDate:	BookINg date of the case
	CaseNumber:		Case number charged on the person
	ClientFlag:		Flag to identIFy IF it is hired/contacted
	ListDate:		Upload date of the case
	DPTwo:			Address status that is used IN barcode font on the letter
	DPC:			Address status that is used IN barcode font on the letter
	ZipSpan:		First five characters of zip code AND SPN number
	Abb:			Short abbreviation of employee who is prINTINg the letter

******/

-- usp_mailer_Send_HCC_CrimINal_UpdatedAddress 8, 147, 8, '10/10/2013,' , 3992, 0,0 ,0
      
ALTER PROCEDURE [dbo].[usp_mailer_Send_HCC_Criminal_UpdatedAddress]
	@catnum	INT	=8,                                    
	@LetterType INT=147,                                    
	@crtid INT=8,                                    
	@startListdate VARCHAR (500) ='10/01/2013',                                    
	@empid INT=3992,                              
	@prINTtype INT =0,
	@searchtype INT,
	@isprINTed  BIT                             
                                    
AS       

BEGIN
	SET NOCOUNT ON;                                
	                          
	-- DECLARING LOCAL VARIABLES FOR THE FILTERS....                
	DECLARE @officernum VARCHAR(50),
	@officeropr VARCHAR(50),
	@tikcetnumberopr VARCHAR(50),                                    
	@ticket_no VARCHAR(50),
	@zipcode VARCHAR(50),
	@zipcodeopr VARCHAR(50),                                                                
	@fINeamount MONEY,
	@fINeamountOpr VARCHAR(50),                                            
	@fINeamountRelop VARCHAR(50),
	@sINgleviolation MONEY,
	@sINglevoilationOpr VARCHAR(50) ,
	@sINgleviolationrelop VARCHAR(50),
	@doubleviolation MONEY,                                            
	@doublevoilationOpr VARCHAR(50) ,                                            
	@doubleviolationrelop VARCHAR(50),                                  
	@count_Letters INT,
	@violadesc VARCHAR(500),                  
	@violaop VARCHAR(50)
	                                    
	-- DECLARING & INITIALIZING THE VARIABLE FOR DYNAMIC SQL...
	DECLARE @sqlquery VARCHAR(MAX), @sqlquery2 VARCHAR(MAX)
	SELECT @sqlquery =''  , @sqlquery2 = ''
	              
	-- ASSIGNING VALUES TO THE VARIABLES 
	-- BY GETTTING INFORMATION FROM LMS FILTER STRUCTURE...
	-- FOR THIS LETTER TYPE....
	SELECT @officernum=officernum,                                    
	@officeropr=oficernumOpr,                                    
	@tikcetnumberopr=tikcetnumberopr,                                    
	@ticket_no=ticket_no,                                                                      
	@zipcode=zipcode,                                    
	@zipcodeopr=zipcodeLikeopr,                                    
	@sINgleviolation =sINgleviolation,                                    
	@sINglevoilationOpr =sINglevoilationOpr,                                    
	@sINgleviolationrelop =sINgleviolationrelop,                                    
	@doubleviolation = doubleviolation,                                    
	@doublevoilationOpr=doubleviolationOpr,                                            
	@doubleviolationrelop=doubleviolationrelop,                  
	@violadesc=violationdescription,                  
	@violaop=violationdescriptionOpr ,              
	@fINeamount=fINeamount ,                                            
	@fINeamountOpr=fINeamountOpr ,                                            
	@fINeamountRelop=fINeamountRelop              
	                                 
	FROM tblmailer_letters_to_sendfilters WITH(NOLOCK)                                    
	WHERE courtcategorynum=@catnum                                    
	AND Lettertype=@LetterType                                  

	-- GETTING THE SHORT ABBREVIATION OF THE CURRENTLY LOGGED IN 
	-- EMPLOYEE IN THE LOCAL VARIABLE....
	DECLARE @user VARCHAR(10)
	SELECT @user = UPPER(abbreviation) FROM tblusers WITH(NOLOCK) WHERE employeeid = @empid
	                                    
	-- CREATING DYNAMIC SQL FOR THE MAIN QUERY...
	--causenumber is fetched INstead of ticketnumber
	--address2 is used INstead of addres1(adress1 is fetched twice)
	SET @sqlquery=@sqlquery+'                                    
	SELECT DISTINCT tva.recordid, 
		ta.midnumber as span, 
		tva.violationdescription as chargewordINg, 
		tva.ticketviolationdate as bookingdate, 
		tva.causenumber as casenumber , 
		ta.clientflag,
		left(tn.ZipCode1,5) + rtrim(ltrim(ta.recordid)) as zipspan,
		CONVERT(VARCHAR(10),tn.InsertDate1,101) as listdate, 
		flag1 = ISNULL(tn.AddressStatus1,''N'') ,	
		UPPER(ltrim(rtrim(ta.firstname)) + '' ''+ ltrim(rtrim(ta.lastname))) as name,
		UPPER(tn.Address1) as address,
		UPPER(tn.city1) as city,
		s.state,
		tn.ZipCode1 AS zipcode,
		tn.DP2_1 AS dptwo,
		tn.DPC_1 AS dpc,
		count(tva.ViolationNumber_PK) as ViolCount,                                                      
		ISNULL(sum(ISNULL(tva.fINeamount,0)),0) as Total_FINeamount,
		ISNULL(tva.ChargeLevel, 0) AS ChargeLevel
	INTo #temp 
	FROM  dbo.tblticketsarchive ta WITH(NOLOCK) 
	INNER JOIN dbo.tblticketsviolationsarchive tva WITH(NOLOCK) on tva.recordid = ta.recordid
	INNER JOIN tblNewAddresses tn ON ta.RecordId = tn.RecordId
	INNER JOIN dbo.tblstate s WITH(NOLOCK) on s.stateid = tn.State1

	-- ONLY VERIFIED AND VALID ADDRESSES
	WHERE	tn.AddressStatus1 IN (''Y'',''D'',''S'') 
	AND tva.violationstatusid <> 80 
	
	-- Address Not Marked As No New Address 
	AND ISNULL(tn.IsNoNewAddressMarked, 0) = 0
	
	-- Only Get the Records which have not sent the Mailer for address 1. . . 
	AND ISNULL(tn.isMailerSentOnAddress1, 0) = 0

	-- EXCLUDE CERTAIN DISPOSITION STATUSES.....
	AND tva.hccc_dst NOT IN (''A'',''D'',''P'')
	AND ISNULL(tva.cad,'''') = ''''
	AND ISNULL(tva.attorneyname,'''') = ''''
	AND (ISNULL(tva.rea,'''') NOT IN (''DISM'',''DADD'', ''DADH'',''DFPD'',''MCH'',''MCHJ'',''MCHR'',''ADGM'',''ADMP'',''APRB'',''DISP'',''EXHR'',''MCCA'',''NGIH'',''PFCS'',''PNDC'',''PNGJ'',''SFBF''))

	-- PERSONS ADDRESS IS NOT CHANGED IN PAST 48 MONTHS (ACCUZIP)...
	AND	ISNULL(ta.ncoa48flag,0) = 0'


	-- ONLY FOR SELECTED DATE RANGE...
	SET @sqlquery=@sqlquery+'
	AND   ' + dbo.Get_StrINg_Concat_With_DatePart_ver2 ( ''+  @startlistdate + '', 'tn.InsertDate1' ) 


	-- IF PRINTING FOR ALL COURTS OF THE SELECTED COURT CATEGORY...  
	IF(@crtid= @catnum  )                                    
		SET @sqlquery=@sqlquery+' 
		AND tva.courtlocation IN (SELECT courtid FROM tblcourts WITH(NOLOCK) WHERE courtcategorynum = '+CONVERT(VARCHAR,@crtid)+' )'                                        

	-- ELSE PRINTING FOR THE INDIVIDUAL SELECTED COURT OF THE CATEGORY....
	ELSE 
		SET @sqlquery =@sqlquery +' 
		AND tva.courtlocation = '+CONVERT(VARCHAR,@crtid) 
	                                                        
	-- LMS FILTERS SECTION:
	-----------------------------------------------------------------------------------------------------------------------------------------------------
	-- IF THERE IS ANY FILTER SPECIFIED TO INCLUDE/EXCLUDE ONLY PARTICULAR OFFICER NUMBERS....
	IF(@officernum<>'')                                                        
		SET @sqlquery =@sqlquery + '  
		AND ta.officerNumber_Fk '  + @officeropr  +'('+ @officernum+')'                                                         


	-- IF THERE IS ANY FILTER DEFINED FOR INCLUDE/EXCLUDE VIOLATION DESCRIPTIONS...
	IF(@ticket_no<>'')                        
		SET @sqlquery=@sqlquery + ' 
		AND ('+  dbo.Get_StrINg_Concat_With_op(''+@ticket_no+'' , 'tva.TicketNumber_PK' ,+ ''+ @tikcetnumberopr) +')'                                                              
	                                    
	-- ZIP CODE FILTER......
	-- INCLUDE/EXCLUDE THE SPECIFIED ZIP CODES.....
	IF(@zipcode<>'')                                                                              
		SET @sqlquery=@sqlquery+ ' 
		AND ('+ dbo.Get_StrINg_Concat_With_op(''+@zipcode+'' , 'left(ta.zipcode,3)' ,+ ''+ @zipcodeopr) +')'                                   

	-- FINE AMOUNT FILTER.....
	-- INCLUDE ONLY THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT....
	IF(@fINeamount<>0 AND @fINeamountOpr<>'NOT'  )                                  
		SET @sqlquery =@sqlquery+ '
		AND tva.fINeamount'+ CONVERT (VARCHAR(10),@fINeamountRelop) +'CONVERT(MONEY,'+ CONVERT(VARCHAR(10),@fINeamount) +')'                                       
	                                                      
	-- EXCLUDE THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT.....
	IF(@fINeamount<>0 AND @fINeamountOpr = 'NOT'  )                                  
		SET @sqlquery =@sqlquery+ '
		AND NOT tva.fINeamount'+ CONVERT (VARCHAR(10),@fINeamountRelop) +'CONVERT(MONEY,'+ CONVERT(VARCHAR(10),@fINeamount) +')'                                       
	-----------------------------------------------------------------------------------------------------------------------------------------------------



	SET @sqlquery =@sqlquery+ ' 
	group by                                                       
	tva.recordid, 
		ta.midnumber, 
		tva.violationdescription , 
		tva.ticketviolationdate , 
		tva.causenumber , 
		ta.clientflag,
		left(tn.zipcode1,5) + rtrim(ltrim(ta.recordid)),
		CONVERT(VARCHAR(10),tn.InsertDate1,101), 
		ISNULL(tn.AddressStatus1,''N'') ,
		UPPER(ltrim(rtrim(ta.firstname)) + '' ''+ ltrim(rtrim(ta.lastname))) ,
		UPPER(tn.Address1),
		UPPER(tn.city1) ,
		s.state,
		tn.zipcode1,
		tn.DP2_1,
		tn.DPC_1,
		tva.ChargeLevel                                              
	HAVING 1 =1 
	'                                  

	-- SINGLE VIOLATION FINE AMOUNT FILTER....
	-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE SINGLE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT
	IF(@sINgleviolation<>0 AND @doubleviolation=0)                                
		SET @sqlquery =@sqlquery+ '
		AND (  '+ @sINglevoilationOpr+  '  (ISNULL(sum(ISNULL(tva.fINeamount,0)),0) '+ CONVERT (VARCHAR(10),@sINgleviolationrelop) +'CONVERT(MONEY,'+ CONVERT(VARCHAR(10),@sINgleviolation)+') AND count(tva.ViolationNumber_PK)=1))
		  '                                

	-- DOUBLE VIOLATION FINE AMOUNT FILTER.....
	-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE VIOLATIONS 
	-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...
	IF(@doubleviolation<>0 AND @sINgleviolation=0 )                                
		SET @sqlquery =@sqlquery + '
		AND ('+ @doublevoilationOpr+  '   (ISNULL(sum(ISNULL(tva.fINeamount,0)),0) '+CONVERT (VARCHAR (10),@doubleviolationrelop) +'CONVERT (MONEY,'+ CONVERT(VARCHAR(10),@doubleviolation) + ')AND count(tva.ViolationNumber_PK)=2) )
		  '                                                    

	-- BOTH SINGLE & DOUBLE VIOLATION FINE AMOUNT FILTER.....
	-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE AND SINGLE VIOLATIONS 
	-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...                                
	IF(@doubleviolation<>0 AND @sINgleviolation<>0)                                
		SET @sqlquery =@sqlquery+ '
		AND ('+ @sINglevoilationOpr+  '  (ISNULL(sum(ISNULL(tva.fINeamount,0)),0) '+ CONVERT (VARCHAR(10),@sINgleviolationrelop) +'CONVERT(MONEY,'+ CONVERT(VARCHAR(10),@sINgleviolation)+') AND count(tva.ViolationNumber_PK)=1))                                
		AND ('+ @doublevoilationOpr+  '  (ISNULL(sum(ISNULL(tva.fINeamount,0)),0) '+CONVERT (VARCHAR (10),@doubleviolationrelop) +'CONVERT (MONEY,'+ CONVERT(VARCHAR(10),@doubleviolation) + ') AND count(tva.ViolationNumber_PK)=2))
			'              

	-- GETTING REOCORDS IN TEMP TABLE...
	SET @sqlquery=@sqlquery+'
	SELECT distINct recordid, span, chargewordINg, bookINgdate, casenumber , clientflag,zipspan,listdate as mdate, flag1,
	name,address,city,state,zipcode,dptwo,dpc,ViolCount, Total_Fineamount,ChargeLevel
	INTo #temp1  FROM #temp WHERE 1=1 
	'              
	        
	-- VIOLATION DESCRIPTION FILTER.....
	-- EXCLUDE THE LETTER IF SPECIFIED VIOLATION IS THE ONLY VIOLATION IN THE LETTER....
	IF(@violadesc <> '')
		BEGIN
			-- NOT LIKE FILTER.......
			IF(charINdex('NOT',@violaop)<> 0 )              
				BEGIN
					SET @sqlquery=@sqlquery+' 
					AND	 NOT ( violcount =1  AND ('+ dbo.Get_StrINg_Concat_With_op(''+@violadesc+'' , '  chargewordINg  ' ,+ ''+ 'like')+'))'           			
				END           
			ELSE
				BEGIN
					SET @sqlquery=@sqlquery+' 
					AND	 ( violcount =1  AND ('+ dbo.Get_StrINg_Concat_With_op(''+@violadesc+'' , '  chargewordINg  ' ,+ ''+ 'like')+'))'           	
				END	
		END


	SET @sqlquery=@sqlquery+'
	delete FROM #temp1 WHERE recordid IN (
	SELECT v.recordid FROM tblticketsviolations v WITH(NOLOCK) INNER JOIN tbltickets t 
	on t.ticketid_pk = v.ticketid_pk AND t.activeflag = 1 
	INNER JOIN #temp1 a on a.recordid = v.recordid
	)
	
	INSERT INTO #temp1 (recordid, span, chargewordINg, bookINgdate, casenumber , clientflag,zipspan, mdate, flag1,
		name,address,city,state,zipcode,dptwo,dpc,ViolCount, Total_Fineamount,ChargeLevel)

	SELECT t.recordid, t.span, t.chargewordINg, t.bookINgdate, t.casenumber , t.clientflag,
	LEFT(tna.ZipCode2,5) + rtrim(ltrim(t.recordid)), 
	t.mdate,
	 ISNULL(tna.AddressStatus2,''N''), t.name,
	tna.Address2, tna.city2 ,ts.state, tna.ZipCode2, tna.DP2_2, tna.DPC_2, t.ViolCount, t.Total_Fineamount,t.ChargeLevel
	FROM tblNewAddresses tna INNER JOIN #temp1 t ON tna.RecordId = t.Recordid
			AND tna.Address2 <> t.address
	INNER JOIN tblState ts ON ts.StateID = tna.State2
	WHERE ISNULL(tna.IsNoNewAddressMarked, 0) = 0
	AND ISNULL(tna.AddressStatus2,''N'') IN (''Y'',''D'',''S'')
	AND LEN(ISNULL(tna.Address2, '''')) > 0 
	AND LEN(ISNULL(tna.city2, '''')) > 0 '
	SET @sqlquery=@sqlquery+'
	AND   ' + dbo.Get_StrINg_Concat_With_DatePart_ver2 ( ''+  @startlistdate + '', 'tna.InsertDate2' ) 
	SET @sqlquery=@sqlquery+'
	AND ISNULL(tna.isMailerSentOnAddress2, 0) = 0

	-- Added Max Fine Amount and Max Jail Time Columns values. . . 
	 ALTER TABLE #temp1 
	 ADD isMaxFineAmountNotfound BIT NOT NULL DEFAULT 1, HcccMailerVioDesc Varchar(500), MaxFineAmount Money, MaxJailTimePeriod Varchar(100)
	 
	 -- Setting the default values to null for the mailer violations . . . 
	 update #temp1 set HcccMailerVioDesc = ''''
	  
	 Update t
	 SET t.HcccMailerVioDesc = UPPER(hc.MailerViolationDescription), t.MaxFineAmount = hc.Maxfine, t.MaxJailTimePeriod = UPPER(hc.MaxJail), isMaxFineAmountNotfound = 0
	 FROM #temp1 t INNER JOIN tblHCCCViolationsInfo hc on REPLACE(t.chargewording, '' '', '''') = REPLACE(hc.ViolationDescription, '' '', '''') 
	 WHERE t.ChargeLevel = hc.ChargeLevel
	 
	 -- Updating the matched Records on violation description basis from HCCC table
	 Update t
	 SET t.HcccMailerVioDesc = UPPER(hc.MailerViolationDescription)
	 FROM #temp1 t INNER JOIN tblHCCCViolationsInfo hc on REPLACE(t.chargewording, '' '', '''') = REPLACE(hc.ViolationDescription, '' '', '''') 
	 
	 
	 -- Getting the the Record if multiplie violations and any of the violation not found in HCCC table
	 SELECT tt.Recordid INTO #tempFineAmount FROM #temp1 tt
	 WHERE tt.Recordid IN (SELECT t.Recordid FROM #temp1 t WHERE t.isMaxFineAmountNotfound = 0)
	 AND tt.isMaxFineAmountNotfound = 1
	 
	 -- Updating the Records
	 UPDATE #temp1
	 SET isMaxFineAmountNotfound = 1
	 WHERE Recordid in (SELECT t.Recordid FROM #tempFineAmount t)
	 -- END 10349

	-- Excluding the Records which are marked as Do Not Mail. . . 
	
	ALTER TABLE #temp1
	Add donotmailflag BIT NOT NULL Default 0
	
	DELETE t
	FROM tblNewAddresses tna INNER JOIN #temp t
	ON tna.RecordId = t.RecordId
	INNER JOIN tbl_hts_DoNotMail_Records r
	ON LTRIM(RTRIM(ISNULL(tna.LastName , ''''))) = LTRIM(RTRIM(ISNULL(r.LastName, '''')))
	AND LTRIM(RTRIM(ISNULL(tna.Address1 , ''''))) = LTRIM(RTRIM(ISNULL(r.[Address], '''')))
	AND LTRIM(RTRIM(ISNULL(tna.City1 , ''''))) = LTRIM(RTRIM(ISNULL(r.City, ''''))) 
	AND LEFT(LTRIM(RTRIM(ISNULL(tna.ZipCode1, ''''))),5) = LEFT(LTRIM(RTRIM(ISNULL(r.Zip, ''''))),5)
	AND ISNULL(tna.State1, 0) = ISNULL(r.StateID_FK, 0)
	AND LEN(ISNULL(tna.Address1, '''')) > 0
	AND LEN(ISNULL(tna.City1, '''')) > 0
	
	DELETE t
	FROM tblNewAddresses tna INNER JOIN #temp t
	ON tna.RecordId = t.RecordId
	INNER JOIN tbl_hts_DoNotMail_Records r
	ON LTRIM(RTRIM(ISNULL(tna.LastName , ''''))) = LTRIM(RTRIM(ISNULL(r.LastName, '''')))
	AND LTRIM(RTRIM(ISNULL(tna.Address2 , ''''))) = LTRIM(RTRIM(ISNULL(r.[Address], '''')))
	AND LTRIM(RTRIM(ISNULL(tna.City2 , ''''))) = LTRIM(RTRIM(ISNULL(r.City, ''''))) 
	AND LEFT(LTRIM(RTRIM(ISNULL(tna.ZipCode2, ''''))),5) = LEFT(LTRIM(RTRIM(ISNULL(r.Zip, ''''))),5)
	AND ISNULL(tna.State2, 0) = ISNULL(r.StateID_FK, 0)
	AND LEN(ISNULL(tna.Address2, '''')) > 0
	AND LEN(ISNULL(tna.City2, '''')) > 0
	
	DELETE t
	FROM tblNewAddresses tna INNER JOIN #temp t
	ON tna.RecordId = t.RecordId
	INNER JOIN tbl_hts_DoNotMail_Records r
	ON LTRIM(RTRIM(ISNULL(tna.Address1 , ''''))) = LTRIM(RTRIM(ISNULL(r.[Address], '''')))
	AND LTRIM(RTRIM(ISNULL(tna.City1 , ''''))) = LTRIM(RTRIM(ISNULL(r.City, ''''))) 
	AND LEFT(LTRIM(RTRIM(ISNULL(tna.ZipCode1, ''''))),5) = LEFT(LTRIM(RTRIM(ISNULL(r.Zip, ''''))),5)
	AND ISNULL(tna.State1, 0) = ISNULL(r.StateID_FK, 0) 
	AND LEN(isnull(r.FirstName,'''') + ISNULL(r.LastName,'''')) = 0	
	AND LEN(ISNULL(tna.Address1, '''')) > 0
	AND LEN(ISNULL(tna.City1, '''')) > 0
	
	DELETE t
	FROM tblNewAddresses tna INNER JOIN #temp t
	ON tna.RecordId = t.RecordId
	INNER JOIN tbl_hts_DoNotMail_Records r
	ON LTRIM(RTRIM(ISNULL(tna.Address2 , ''''))) = LTRIM(RTRIM(ISNULL(r.[Address], '''')))
	AND LTRIM(RTRIM(ISNULL(tna.City2 , ''''))) = LTRIM(RTRIM(ISNULL(r.City, ''''))) 
	AND LEFT(LTRIM(RTRIM(ISNULL(tna.ZipCode2, ''''))),5) = LEFT(LTRIM(RTRIM(ISNULL(r.Zip, ''''))),5)
	AND ISNULL(tna.State2, 0) = ISNULL(r.StateID_FK, 0) 
	AND LEN(isnull(r.FirstName,'''') + ISNULL(r.LastName,'''')) = 0	
	AND LEN(ISNULL(tna.Address2, '''')) > 0
	AND LEN(ISNULL(tna.City2, '''')) > 0

	'
	
	IF (@prINTtype = 1)
	BEGIN
		SET @sqlquery2 =@sqlquery2 + '
		
		UPDATE tblNewAddresses
		SET isMailerSentOnAddress1 = 1
		FROM tblNewAddresses nd INNER JOIN #temp1 t ON nd.recordid = t.recordid
		AND nd.Address1 = t.address AND nd.City1 = t.city AND nd.ZipCode1 = t.zipcode 
		
		UPDATE tblNewAddresses
		SET isMailerSentOnAddress2 = 1
		FROM tblNewAddresses nd INNER JOIN #temp1 t ON nd.recordid = t.recordid
		AND nd.Address2 = t.address AND nd.City2 = t.city AND nd.ZipCode2 = t.zipcode ' 
	END 

	-- IF "MARK LETTER AS SENT" OPTION IS SELECTED....
	-- CREATE LETTER HISTORY FOR EACH PERSON....
	IF( @prINTtype = 1 )         
		BEGIN
			SET @sqlquery2 =@sqlquery2 +                                    
			'
				DECLARE @ListdateVal DateTime,                                  
					@totalrecs INT,                                
					@count INT,                                
					@p_EachLetter MONEY,                              
					@recordid VARCHAR(50),                              
					@zipcode   VARCHAR(12),                              
					@MAXbatch INT  ,
					@lCourtId INT  ,
					@dptwo VARCHAR(10),
					@dpc VARCHAR(10)
				DECLARE @tempBatchIDs table (batchid INT)

				-- GETTING TOTAL LETTERS AND COSTING ......
				SELECT @totalrecs =count(*) FROM #temp1 WHERE mdate = @ListdateVal 
				
				SET @p_EachLetter=CONVERT(MONEY,0.45) 
			                              
				-- DECLARING CURSOR TO INSERT THE RECORDS IN LETTER HISTORY ONE BY ONE.....
				-- THERE WILL A SEPARATE BATCH FOR EACH LIST/COURT DATE VALUE ......
				DECLARE ListdateCur Cursor for                                  
				SELECT distINct mdate  FROM #temp1                                
				open ListdateCur                                   
				Fetch Next FROM ListdateCur INTo @ListdateVal                                                      
				while (@@Fetch_Status=0)                              
					begIN                                

						-- GET TOTAL LETTERS FOR THE LIST DATE
						SELECT @totalrecs =count(recordid) FROM #temp1 WHERE mdate = @ListdateVal                                 

						-- INSERTING RECORD IN BATCH TABLE......
						Insert into tblBatchLetter(Empid, Lettertype,Listdate,CourtId,ParentID,LCount,PExpense) values                                  
						( '+CONVERT(VARCHAR(10), @empid)+'  , '+CONVERT(VARCHAR(10),@lettertype)+' , @ListdateVal, '+CONVERT(VARCHAR(10),@crtid)+' , 0, @totalrecs, @p_EachLetter)                                   


						-- GETTING BATCH ID OF THE INSERTED RECORD.....
						SELECT @MAXbatch=MAX(BatchId) FROM tblBatchLetter WITH(NOLOCK) 
						INsert INTo @tempBatchIDs SELECT @MAXbatch                                                     

						-- CREATING CURSOR TO INSERT RECORDS FOR EACH LETTER FOR THE NEWLY INSERTED BATCH....
						DECLARE RecordidCur Cursor for                               
						SELECT distinct recordid,zipcode, 3037, dptwo, dpc FROM #temp1 WHERE mdate = @ListdateVal                               
						open RecordidCur                                                         
						Fetch Next FROM  RecordidCur INTo @recordid,@zipcode, @lCourtId , @dptwo, @dpc                                          
						while(@@Fetch_Status=0)                              
						begIN                              

							-- INSERTING LETTERS IN LETTER HISTORY FOR THE SPECIFIED BATCH...
							INsert INTo tblLetternotes(BatchId_Fk,listdate,ZipCode,RecordID,PCost,LetterType, courtid, dp2, dpc)                              
							values (@MAXbatch,@ListdateVal,@zipcode,@recordid,@p_EachLetter,'+CONVERT(VARCHAR(10),@lettertype)+' ,@lCourtId, @dptwo, @dpc)                              
							Fetch Next FROM  RecordidCur INTo @recordid,@zipcode, @lCourtId , @dptwo, @dpc                          
						end                              
						close RecordidCur 
						deallocate RecordidCur                                                               
						Fetch Next FROM ListdateCur INTo @ListdateVal                              
					end                                            

				close ListdateCur  
				deallocate ListdateCur 

			-- OUTPUTTING THE DATA TO THE APPLICATION FOR REPROT FILE.....
			SELECT distinct CONVERT(VARCHAR(20),n.noteid) as letterid, Name, address, city, state, t.zipcode as zip , t.recordid, span, 
			chargewordINg,  bookINgdate, casenumber, clientflag, t.zipspan, mdate as listdate,
			dptwo, t.dpc,  '''+@user+''' as Abb, ''147'' as lettertype, t.isMaxFineAmountNotfound, t.HcccMailerVioDesc, t.MAXFINeAmount, t.MAXJailTimePeriod 
				
			FROM #temp1 t , tblletterNOTes n, @tempBatchIDs tb
			WHERE t.recordid = n.recordid AND  n.courtid = 3037 AND n.batchid_fk = tb.batchid AND n.lettertype = '+CONVERT(VARCHAR(10),@lettertype)+' 
			order by [ZIPSPAN]

			-- GENERATING AND SENDING EMAIL FOR LETTER PRINTING NOTIFICATION........
			DECLARE @lettercount INT, @subject VARCHAR(200), @body VARCHAR(400) , @sql VARCHAR(500)
			SELECT @lettercount = count(distinct n.noteid) FROM tblletternotes n WITH(NOLOCK), @tempBatchIDs b WHERE n.batchid_fk = b.batchid
			SELECT @subject = CONVERT(VARCHAR(20), @lettercount) + '' Harris County CrimINal - Criminal Book In Updated Addresses Letters Printed''
			SELECT @body = CONVERT(VARCHAR(20), @lettercount) + '' LETTERS ('' + CONVERT(VARCHAR(12), GETDATE()) + '') - ''+ UPPER( '''+@user+''')
			exec usp_mailer_send_Email @subject, @body

			-- OUTPUTING THE PDF FILE NAME TO THE APPLICATION...
			DECLARE @batchIDs_filname VARCHAR(500)
			SET @batchIDs_filname = ''''
			SELECT @batchIDs_filname = @batchIDs_filname + CONVERT(VARCHAR,batchid) + '',''  FROM @tempBatchIDs
			SELECT ISNULL(@batchIDs_filname,'''') as batchid

			'

		END
	ELSE

		-- IF "MARK LETTER AS SENT" OPTION IS NOT SELECTED....
		-- THEN DO NOT CREATE LETTER HISTORY. JUST OUTPUT THE DATA FOR REPORT FILE 
		-- TO DISPLAY THE LETTER...
		BEGIN
			SET @sqlquery2 = @sqlquery2 + '
			SELECT distINct ''NOT PRINTABLE'' as letterId,  Name, address, city, state, zipcode as zip , recordid, span, 
			chargewordINg,  bookINgdate, casenumber, clientflag,  mdate as listdate, 
			dptwo, dpc, zipspan,  '''+@user+''' as Abb, ''147'' as lettertype, isMaxFineAmountNotfound, HcccMailerVioDesc, MAXFINeAmount, MAXJailTimePeriod
			
			FROM #temp1 
			order by [zipspan]
			'
		END

	-- DROPPING TEMPORARY TABLES ....
	SET @sqlquery2 = @sqlquery2 + '

	drop table #temp 
	drop table #temp1
	DROP TABLE #tempFINeAmount'

	-- CONCATENATING THE DYNAMIC SQL
	SET @sqlquery = @sqlquery + @sqlquery2
	--PRINT @sqlquery
	-- EXECUTING THE DYNAMIC SQL ....
	exec (@sqlquery)

END

GO
	GRANT EXECUTE ON usp_mailer_Send_HCC_CrimINal_UpdatedAddress TO dbr_webuser
GO
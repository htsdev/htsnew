/**
* * Created By : Noufil Khan 05/15/2209
* Business Logic : This procedure returns all clients to whom ALR hearing alerts is being sent. Following are the type of Alert
*					5.  ALR Hearing Request
*					6.  ALR DPS Receipt
*  				    7.  ALR Hearing Notification
*  				    8.  ALR Discovery
*  				    9.  ALR Subpoena Application
*  				    10. ALR Subpoena Pick up Alert
*  				    11. ALR Subpoena Return
*  				    12. ALR Hearing Disposition
*  				    14. ALR Request for Appearance of Witness
*  				    15. ALR Continuance Request(Mandatory)
*  				    17. ALR Continuance Request(Discretionary)
*  				    
*  	     
**/

Alter procedure [dbo].[USP_HTP_GET_ALRHEARINGREPORT]
@showvalidation INT = 0
AS
DECLARE @returnqurery5 VARCHAR(5000)
DECLARE @returnqurery6 VARCHAR(5000)
DECLARE @returnqurery7 VARCHAR(5000)
DECLARE @returnqurery8 VARCHAR(5000)
DECLARE @returnqurery9 VARCHAR(5000)
DECLARE @returnqurery10 VARCHAR(5000)
DECLARE @returnqurery11 VARCHAR(5000)
DECLARE @returnqurery12 VARCHAR(5000)
DECLARE @returnqurery14 VARCHAR(5000)
DECLARE @returnqurery15 VARCHAR(5000)
DECLARE @returnqurery17 VARCHAR(5000)
DECLARE @returnresultqurery1 VARCHAR(MAX)
DECLARE @returnresultqurery2 VARCHAR(MAX)
DECLARE @returnresultqurery3 VARCHAR(MAX)
DECLARE @returnresultqurery4 VARCHAR(MAX)
DECLARE @returnresultqurery5 VARCHAR(MAX)
DECLARE @returnresultqurery6 VARCHAR(MAX)

-- ALR Hearing Request
EXEC [dbo].[USP_HTP_GET_ALR_HEARING_RECORDS] 5,0,@showvalidation, @SQLReturnQuery=@returnqurery5 OUTPUT

--ALR DPS Receipt
EXEC [dbo].[USP_HTP_GET_ALR_HEARING_RECORDS] 6,0,@showvalidation, @SQLReturnQuery=@returnqurery6 OUTPUT

-- ALR Hearing Notification
EXEC [dbo].[USP_HTP_GET_ALR_HEARING_RECORDS] 7,0,@showvalidation, @SQLReturnQuery=@returnqurery7 OUTPUT

-- ALR Discovery
EXEC [dbo].[USP_HTP_GET_ALR_HEARING_RECORDS] 8,0,@showvalidation, @SQLReturnQuery=@returnqurery8 OUTPUT 

-- ALR Subpoena Application
EXEC [dbo].[USP_HTP_GET_ALR_HEARING_RECORDS] 9,0,@showvalidation, @SQLReturnQuery=@returnqurery9 OUTPUT 

-- ALR Subpoena Pick up Alert
EXEC [dbo].[USP_HTP_GET_ALR_HEARING_RECORDS] 10,0,@showvalidation, @SQLReturnQuery=@returnqurery10 OUTPUT 

-- ALR Subpoena Return
EXEC [dbo].[USP_HTP_GET_ALR_HEARING_RECORDS] 11,0,@showvalidation, @SQLReturnQuery=@returnqurery11 OUTPUT 

-- ALR Hearing Disposition
EXEC [dbo].[USP_HTP_GET_ALR_HEARING_RECORDS] 12,0,@showvalidation, @SQLReturnQuery=@returnqurery12 OUTPUT 

--Hafiz Khalid 10296  07/04/2012 remove the alr hearing apperance witness 
-- ALR Request for Appearance of Witness 
--EXEC [dbo].[USP_HTP_GET_ALR_HEARING_RECORDS] 14,0,@showvalidation, @SQLReturnQuery=@returnqurery14 OUTPUT

-- ALR Continuance Request(Mandatory)
EXEC [dbo].[USP_HTP_GET_ALR_HEARING_RECORDS] 15,0,@showvalidation, @SQLReturnQuery=@returnqurery15 OUTPUT

-- ALR Continuance Request(Discretionary)
EXEC [dbo].[USP_HTP_GET_ALR_HEARING_RECORDS] 17,0,@showvalidation, @SQLReturnQuery=@returnqurery17 OUTPUT

SET @returnresultqurery1 = 
		@returnqurery5  + ' UNION ' +
		@returnqurery6  + ' UNION ' 		

SET @returnresultqurery2 =
		@returnqurery7  + ' UNION ' +
		@returnqurery8  + ' UNION '
	
SET @returnresultqurery3 =
		@returnqurery9  + ' UNION ' +
		@returnqurery10 + ' UNION '	

SET @returnresultqurery4 =
		@returnqurery11 + ' UNION ' +
		@returnqurery12 + ' UNION '
		
SET @returnresultqurery5 =	
		--Hafiz Khalid 10296  07/04/2012 remove the alr hearing apperance witness subdoctype			
		--@returnqurery14 + ' UNION ' + 
		@returnqurery15 + ' UNION ' 		

SET @returnresultqurery6 = @returnqurery17


exec (@returnresultqurery1 + @returnresultqurery2 + @returnresultqurery3+ @returnresultqurery4 + @returnresultqurery5 + @returnresultqurery6+ ' ORDER BY tt.TicketID_PK,[Title]')
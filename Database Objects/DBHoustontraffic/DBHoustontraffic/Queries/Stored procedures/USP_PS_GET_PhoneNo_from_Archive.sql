SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_PS_GET_PhoneNo_from_Archive]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_PS_GET_PhoneNo_from_Archive]
GO


--- [USP_PS_GET_PhoneNo_from_Archive] 124181

CREATE Procedure [dbo].[USP_PS_GET_PhoneNo_from_Archive]
(
@ticketid_pk int
)
as

declare @phone varchar(50)
Select top 1 @phone=   phonenumber from tblticketsarchive where recordid = ( Select recordid  from tbltickets where ticketid_pk = @ticketid_pk ) 
Select isnull(@phone,'') 
---------------------------------------------------------------------------------------------------------------------------------------------------


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


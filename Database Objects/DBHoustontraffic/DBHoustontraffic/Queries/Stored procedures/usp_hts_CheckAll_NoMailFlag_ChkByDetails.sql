
-- =============================================
-- Author:		Tahir Ahmed
-- Create date: 07/21/2008
-- Business Logic :The stored procedure is used by HTP /Activies /Donot Mail Update application.
--				It is used to search record by name & address. User can mark the records appearing
--				in the results to remove the person from our mailing lists 
--
-- List Of Parameters:
--	@FirstName:		First name of the person
--	@LastName:		last name of the person
--	@Address:		mailing address of the person
--	@Zip:			zip code associated with the mailing address
--	@City:			city associated with the mailing address
--	@stateid_fk:	state id related to the mailing address

-- List of output columns:
--	dbid:			Database ID related to the record
--	recordid:		unique identification of record in the above specified database
--	casetype:		description for the matter type (HTP, DTP, Civil, Criminal)
--	ticketnumber:	Ticket number associated with the case
--	firstname:		first name of the person
--	lastname:		last name of the person
--	address1:		mailing address of the person 
--	city:			city associated with the mailing address
--	state:			name of the state associated with the mailing address
--	zipcode:		zipcode associated with the mailing address
--	nomailflag		flag if already marked as do not mail flag
--  Insertdate:     date when case was marked for do not mail

--
--
-- =============================================

 
ALTER procedure [dbo].[usp_hts_CheckAll_NoMailFlag_ChkByDetails]                
                
@FirstName varchar(20),                
@LastName varchar(20),                
@Address varchar(50) ,                   
@Zip varchar(12)   ,                       
@City varchar(50),                
@stateid_fk int                 
                
as                

--Yasir Kamal 6144 07/14/2009 Insertdate column added.               
--Sabir Khan 6167 07/17/2009 Remove ticket number from temp table...
--Yasir Kamal 6170 08/13/2009 add DoNotMailUpdateDate column
declare @DataSet table(Sno int identity, dbid int,DoNotMailUpdateDate VARCHAR(10),recordid int, casetype varchar(50), FirstName varchar(20),lastname varchar(20),Address1 varchar(50),city varchar(20),state varchar(15),ZipCode varchar(12), nomailflag bit)  
-- searching matching records in TrafficTickets database....
--Sabir Khan 6167 07/17/2009 remove ticketnumber field for removing duplication in clients...
insert into @dataset(dbid,DoNotMailUpdateDate, recordid, casetype, firstname,lastname,address1,city,state,zipcode, nomailflag)              
select  distinct  1,ISNULL(CONVERT(VARCHAR(10),t.DoNotMailUpdateDate,101),'Not Set'), t.recordid, 'Houston Traffic', t.firstname,   t.lastname, t.address1, t.City, s.state, t.ZipCode , isnull(t.donotmailflag,0) 
from tblticketsarchive t inner join tblticketsviolationsarchive v 
on v.recordid = t.recordid 
inner join tblstate s on s.stateid = t.stateid_fk 
--LEFT OUTER JOIN  tbl_hts_donotmail_records d ON (d.FirstName = t.FirstName  AND d.LastName = t.LastName aND d.StateID_FK = t.StateID_FK AND d.Zip =left(t.ZipCode,5) AND d.[Address] = t.address1 + isnull(' '+t.address2,'') AND d.City = t.City )                     
where
-- Abbas Qamar 9726 01/11/2011 making the optional First Name , city and State in Where clause

	 CASE WHEN LEN(LTRIM(RTRIM(@FirstName))) > 0 THEN LTRIM(RTRIM(t.firstname)) ELSE '' END  =  LTRIM(RTRIM(@FirstName))							
     AND                          
     LTRIM(RTRIM(t.lastname)) = LTRIM(RTRIM(@LastName))                        
     AND                          
     LTRIM(RTRIM(t.address1 + ' ' + ISNULL(t.Address2,''))) like Ltrim(Rtrim(@Address))                        
     AND      
	 LEFT(LTRIM(RTRIM(t.ZipCode)),5) = LEFT(LTRIM(RTRIM(@Zip)),5)                       
	 AND                      
     CASE WHEN LEN(LTRIM(RTRIM(@City))) > 0 THEN LTRIM(RTRIM(t.City)) ELSE '' END  =  LTRIM(RTRIM(@City))
     AND                    
     CASE WHEN @stateid_fk > 0 THEN t.StateID_FK ELSE 0 END  = @stateid_fk


                 
-- searching matching records in DallasTrafficTickets database....
--Sabir Khan 6167 07/17/2009 remove ticketnumber field for removing duplication in clients...
insert into @dataset(dbid,DoNotMailUpdateDate, recordid, casetype, firstname,lastname,address1,city,state,zipcode, nomailflag)              
select distinct 2,ISNULL(CONVERT(VARCHAR(10),t.DoNotMailUpdateDate,101),'Not Set'), t.recordid, 'Dallas Traffic', t.firstname,          
   t.lastname,          
   t.address1,          
   t.ZipCode,          
   t.City,          
    s.State  , isnull(t.donotmailflag,0)        
 from dallastraffictickets.dbo.tblticketsarchive   t 
inner join Dallastraffictickets.dbo.tblticketsviolationsarchive v 
on v.recordid = t.recordid
inner join dallastraffictickets.dbo.tblstate s
on s.stateid = t.stateid_fk
--LEFT OUTER JOIN  traffictickets.dbo.tbl_hts_donotmail_records d ON (d.FirstName = t.FirstName  AND d.LastName = t.LastName aND d.StateID_FK = t.StateID_FK AND d.Zip =left(t.ZipCode,5) AND d.[Address] = t.address1 + isnull(' '+t.address2,'') AND d.City = t.City   )                      
WHERE

-- Abbas Qamar 9726 01/11/2011 making the optional First Name , city and State in Where clause

     CASE WHEN LEN(LTRIM(RTRIM(@FirstName))) > 0 THEN LTRIM(RTRIM(t.firstname)) ELSE '' END  =  LTRIM(RTRIM(@FirstName))							
     AND                          
     LTRIM(RTRIM(t.lastname)) = LTRIM(RTRIM(@LastName))                       
     AND                          
     t.address1 like @Address                        
     AND                        
	 LEFT(LTRIM(RTRIM(t.ZipCode)),5) = LEFT(LTRIM(RTRIM(@Zip)),5)                      
	 AND                      
     CASE WHEN LEN(LTRIM(RTRIM(@City))) > 0 THEN LTRIM(RTRIM(t.City)) ELSE '' END  =  LTRIM(RTRIM(@City))
     AND                    
     CASE WHEN @stateid_fk > 0 THEN t.StateID_FK ELSE 0 END  = @stateid_fk

      
                
                
-- searching matching records in Civil database.... 
-- tahir 4418 07/26/2008 added Civil database ....
--Sabir Khan 6167 07/17/2009 remove ticketnumber field for removing duplication in clients...
insert into @dataset(dbid,DoNotMailUpdateDate, recordid, casetype, firstname,lastname,address1,city,state,zipcode, nomailflag)              
select distinct 5,ISNULL(CONVERT(VARCHAR(10),t.DoNotMailUpdateDate,101),'Not Set'), t.childcustodypartyid, 'Harris Civil', t.firstname,          
   t.lastname,          
   t.address,          
   t.ZipCode,          
   t.City,          
    s.State      , isnull(t.donotmailflag,0)    
 from civil.dbo.childcustodyparties  t 
inner join civil.dbo.childcustody v 
on v.childcustodyid = t.childcustodyid
inner join civil.dbo.tblstate s
on s.stateid = t.stateid     
--LEFT OUTER JOIN  traffictickets.dbo.tbl_hts_donotmail_records d ON (d.FirstName = t.FirstName AND d.LastName = t.LastName AND d.Zip = LEFT(t.Zipcode,5)  AND d.[Address] = t.[Address]  AND d.City = t.City   )                   
                        
where                    

-- Abbas Qamar 9726 01/11/2011 making the optional First Name , city and State in Where clause

	 CASE WHEN LEN(LTRIM(RTRIM(@FirstName))) > 0 THEN LTRIM(RTRIM(t.firstname)) ELSE '' END  =  LTRIM(RTRIM(@FirstName))							
     AND                           
     LTRIM(RTRIM(t.lastname)) = LTRIM(RTRIM(@LastName))                       
     AND                          
     LTRIM(RTRIM(t.address)) like LTRIM(RTRIM(@Address))
     AND                        
	 LEFT(LTRIM(RTRIM(t.ZipCode)),5) = LEFT(LTRIM(RTRIM(@Zip)),5)                       
	 AND                      
     CASE WHEN LEN(LTRIM(RTRIM(@City))) > 0 THEN LTRIM(RTRIM(t.City)) ELSE '' END  =  LTRIM(RTRIM(@City))
     AND                    
     CASE WHEN @stateid_fk > 0 THEN t.StateID ELSE 0 END  = @stateid_fk
             
-- out put the results...
select * from @dataset order by dbid
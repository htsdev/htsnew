﻿ /******    
* Created By :   Ozair
* Create Date :   11/05/2010   
* Task ID :    8501  
* Business Logic :  This procedure is used to get patient questionaire.  
*/
create PROCEDURE dbo.usp_HTP_UpdatePatientRecords
	@PatientId INT,
	@SurgeonRequestedDate DATETIME,
	@SurgeonReceivedDate DATETIME,
	@SurgeonSentToClientDate DATETIME,
	@HospitalRequestedDate DATETIME,
	@HospitalReceivedDate DATETIME,
	@HospitalSentToClientDate DATETIME,
	@GPRequestedDate DATETIME,
	@GPReceivedDate DATETIME,
	@GPSentToClientDate DATETIME,	
	@MiscRequestedDate DATETIME,
	@MiscReceivedDate DATETIME,
	@MiscSentToClientDate DATETIME,
	@MiscRecords VARCHAR(MAX)
	
AS
	UPDATE Patient
	SET    SurgeonRequestedDate = @SurgeonRequestedDate,
	       SurgeonReceivedDate = @SurgeonReceivedDate,
	       SurgeonSentToClientDate = @SurgeonSentToClientDate,
	       HospitalRequestedDate = @HospitalRequestedDate,
	       HospitalReceivedDate = @HospitalReceivedDate,
	       HospitalSentToClientDate = @HospitalSentToClientDate,
	       GPRequestedDate = @GPRequestedDate,
	       GPReceivedDate = @GPReceivedDate,
	       GPSentToClientDate = @GPSentToClientDate,
	       MiscRecords = @MiscRecords,
	       MiscRequestedDate = @MiscRequestedDate,
	       MiscReceivedDate = @MiscReceivedDate,
	       MiscSentToClientDate = @MiscSentToClientDate
	WHERE  Id = @PatientId	
GO

GRANT EXECUTE ON dbo.usp_HTP_UpdatePatientRecords TO dbr_webuser
GO
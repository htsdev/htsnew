﻿/************************************************************
	--Sabir Khan 4640 08/20/2008
	 Business Logic : this procedure is used to display all HMC cases which have
		1. verified status = "A/W" or "Arraignment" 
		2. Auto status = "FTA Warrant" or "Warrant"
		3. BondReminderDate is less or current date
		--ozair 4892 10/03/2008				
		4. Must be Client(active flag=1)
	Column List:
	Followupdate
	DaysOver
	BondReminderDate        
	BondFlag 
	RefCaseNumber        
	LastName             
	FirstName            
	ticketid_pk 
	Courtnumber 
	ShortName 
	AutoStatus 
	CourtDate                 
	VerifiedCourtStatus 
	VerifiedCourtDate
 ************************************************************/

		

ALTER PROCEDURE [dbo].[USP_HTS_Get_HMCWarrantAlert]
AS
	-- Noufil 6226 07/28/2009 change follow up date from 01/01/1900 @ 01:01 pm format to 01/01/1900. After dicusiing it with CTO and Ozair to resolve issue in Validation email report.
	SELECT CONVERT(VARCHAR, BondReminderDate, 101) AS Followupdate,
	       DATEDIFF(dd, BondReminderDate, GETDATE()) AS DaysOver,
	       *
	FROM   (
	           SELECT (
	                      CASE 
	                           WHEN (ISNULL(tv.BondReminderDate, 0) <> 0) THEN 
	                                tv.BondReminderDate 
	                                --ozair 4892 10/03/2008 table alias used
	                           ELSE (
	                                    SELECT MAX(RecDate)
	                                    FROM   tblticketsviolationlog tvlog
	                                    WHERE  tv.ticketsviolationid = tvlog.ticketviolationid
	                                           AND newverifiedstatus = 202
	                                           AND newverifiedstatus <> 
	                                               oldverifiedstatus
	                                )
	                      END
	                  ) AS BondReminderDate,
	                  BondFlag = CASE 
	                                  WHEN BondFlag = 1 THEN 'YES'
	                                  ELSE 'No'
	                             END,
	                  tv.RefCaseNumber,
	                  --ozair 4892 10/03/2008 table alias used 
	                  t.LastName,
	                  t.FirstName,
	                  t.ticketid_pk,
	                  tv.Courtnumber,
	                  c.ShortName,
	                  AStatus.ShortDescription AS AutoStatus,
	                  dbo.fn_DateFormat(tv.Courtdate, 25, '/', ':', 1) AS 
	                  CourtDate,
	                  VStatus.ShortDescription AS VerifiedCourtStatus,
	                  dbo.fn_DateFormat(tv.CourtDateMain, 25, '/', ':', 1) AS 
	                  VerifiedCourtDate
	           FROM   tblticketsViolations tv,
	                  --ozair 4892 10/03/2008 alias given
	                  tblTickets t,
	                  tblCourts c,
	                  tblCourtViolationStatus AS AStatus,
	                  tblCourtViolationStatus AS VStatus
	           WHERE  --ozair 4892 10/03/2008 included only clients, table alias used
	                  t.activeflag = 1
	                  AND tv.TicketID_PK = t.TicketID_PK
	                  AND tv.CourtId = c.CourtId
	                  AND tv.CourtViolationStatusID = AStatus.CourtViolationStatusID
	                  AND tv.CourtViolationStatusIDmain = VStatus.CourtViolationStatusID
	                  AND tv.CourtViolationStatusIDmain IN (3, 201)
	                  AND tv.CourtViolationStatusID IN (186, 274)
	                  AND DATEDIFF(DAY, GETDATE(), tv.BondReminderDate) <= 0
	       ) AS main
	       -- Sabir Khan 5297 11/03/2008 sort by follow update...
	       --order by DaysOver desc
	ORDER BY
	       Followupdate,
	       main.ticketid_pk -- Adil Aleem 5857 05/15/2009 Include Ticket ID in order by clause
GO


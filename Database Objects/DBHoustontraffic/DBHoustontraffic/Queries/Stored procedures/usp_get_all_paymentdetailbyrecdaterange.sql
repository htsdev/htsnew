/***************
Business Logic : This procedure is used for getting payment detail by rec date

Parameter:
@RecDate : From date to be filtered.
@RecDateTo :  End date to be filtered.
@firmId : firm id

***************/
--Waqas 5110 01/16/2009
alter procedure [dbo].[usp_Get_All_PaymentDetailByRecDateRange]         
        
@RecDate DateTime,        
@RecDateTo DateTime,    
@firmId int = null ,       
@BranchID INT=1 -- Sabir Khan 10920 05/27/2013 Branch Id added.       
AS        
      
set @RecDateTo = @RecDateTo + '23:59:59.999'      

        
CREATE TABLE #TempFinReportA      
(  
	--Waqas 5110 01/16/2009    
   EmployeeID  int NOT NULL  PRIMARY KEY,      
   CashAmount      money     Not NULL DEFAULT(0),      
)      
    
CREATE TABLE #TempFinReportB      
(      
	--Waqas 5110 01/16/2009
   EmployeeID  int NOT NULL PRIMARY KEY,      
   CheckAmount     money     Not NULL DEFAULT(0)      
)      
       
INSERT INTO #TempFinReportA  (EmployeeID, CashAmount)        
SELECT EmployeeID, Sum(ChargeAmount) As Amount         
FROM         dbo.tblTicketsPayment          
Where   ( RecDate Between  @RecDate   AND   @RecDateTo)        
And PaymentType = 1          
AND  PaymentVoid <> 1         
AND ticketId in (select ticketId_pk from tblticketsviolations where coveringFirmId = coalesce(@firmId,coveringFirmId)) 
AND Branchid IN (SELECT branchid FROM fn_get_branchIDs(@branchID))   -- Sabir Khan 10920 05/27/2013 Branch id check added.   
Group By EmployeeID         
Order By EmployeeID         

        
INSERT INTO #TempFinReportB  (EmployeeID, CheckAmount)        
SELECT EmployeeID, Sum(ChargeAmount) As Amount         
FROM         dbo.tblTicketsPayment         
Where   ( RecDate Between  @RecDate   AND   @RecDateTo)        
And PaymentType in  ( 2 , 4)      
AND  PaymentVoid <> 1         
AND ticketId in (select ticketId_pk from tblticketsviolations where coveringFirmId = coalesce(@firmId,coveringFirmId))  
AND Branchid IN (SELECT branchid FROM fn_get_branchIDs(@branchID))   -- Sabir Khan 10920 05/27/2013 Branch id check added.   
Group By EmployeeID         
Order By EmployeeID         
        
        
SELECT ISNULL(dbo.#TempFinReportA.EmployeeID, dbo.#TempFinReportB.EmployeeID) AS EmpID,       
 ISNULL(CashAmount,0)as CashAmount,       
 ISNULL(CheckAmount,0) as CheckAmount  
FROM dbo.#TempFinReportA       
FULL OUTER JOIN        
 dbo.#TempFinReportB       
ON  dbo.#TempFinReportA.EmployeeID = dbo.#TempFinReportB.EmployeeID        
  
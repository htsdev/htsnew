SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_LA_Update_Driving_License]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_LA_Update_Driving_License]
GO


CREATE PROCEDURE USP_HTS_LA_Update_Driving_License

@Cause_Number as Varchar(20),
@Ticket_Number as Varchar(15),
@License_Number as Varchar(15),
@Plate_Number as Varchar(15),
@GroupID as Int,
@Flag_Updated int Output

AS  
Declare @IDFound as int
Declare @Mid_Number as varchar(15)

Set @Cause_Number = Left(RTrim(LTrim(@Cause_Number)), 30)
Set @Ticket_Number = Left(RTrim(LTrim(@Ticket_Number)), 20)
Set @License_Number = Left(RTrim(LTrim(@License_Number)), 20)
Set @Plate_Number = Left(RTrim(LTrim(@Plate_Number)),5)
Set @Flag_Updated = 0
Set @Cause_Number = Replace(@Cause_Number, ' ', '')
Set @IDFound = 0
Set @Mid_Number = ''


Set @IDFound = (Select top 1 RecordID From tblTicketsViolationsArchive Where (CauseNumber = @Cause_Number Or TicketNumber_LA = @Ticket_Number))

		If @IDFound Is Not Null
			Begin
				Set @Mid_Number = (Select MidNumber From TblTicketsArchive Where (RecordID = @IDFound))
				Update tblTicketsArchive Set DLNumber = @License_Number, PlateNumber = @Plate_Number, GroupID = @GroupID Where (MidNumber = @Mid_Number)
				Set @Flag_Updated = 1
			End

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


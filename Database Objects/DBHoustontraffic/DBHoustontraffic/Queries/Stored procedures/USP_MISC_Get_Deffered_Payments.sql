set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

/*
* Sabir Khan 7723 05/07/2010
* Business Logic:
* 
*/
ALTER PROCEDURE [dbo].[USP_MISC_Get_Deffered_Payments]

@Date DATETIME

AS


select distinct L.FirstName,L.LastName,L.Address1 + isnull(L.Address2,'') as [Address],L.City, L.State,L.Zip, T.Causenumber as CaseNumber,L.Violation,
Convert(varchar,L.Violationdate,101) as ViolationDate,Case L.Gender when 'M' then 'Male' when 'F' then 'Female' else '' end as Gender,
L.FineAmount,Convert(varchar,T.CourtDate,101) as [Courtdate]
from LoaderFilesArchive.dbo.tblEventExtractTemp T 
inner join LoaderFilesArchive.dbo.tblLubbock L on LTrim(Rtrim(L.Causenumber)) = LTrim(RTrim(T.Causenumber))
where T.status in ('DEFERRED PAYMENT', 'DEFERRED PAYMENT NON COMPLIANCE')
and datediff(month,T.Courtdate,@Date) =0
order by Convert(varchar,T.CourtDate,101)



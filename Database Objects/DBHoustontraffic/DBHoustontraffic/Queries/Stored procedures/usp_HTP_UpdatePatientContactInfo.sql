﻿/******  
* Created By :	  Syed Muhammad Ozair.
* Create Date :   11/05/2010 
* Task ID :		  8501
* Business Logic :  This procedure is used to Update patient contact information along with source and status by pateint id.
* List of Parameter :
* Column Return :     
******/

ALTER  PROCEDURE dbo.usp_HTP_UpdatePatientContactInfo
	@PatientId INT,
	@LastName VARCHAR(50),
	@FirstName VARCHAR(50),
	@Address VARCHAR(200),
	@City VARCHAR(50),
	@StateId INT,
	@Zip VARCHAR(10),
	@Contact1 VARCHAR(15),
	@ContactType1 INT,
	@Contact2 VARCHAR(15),
	@ContactType2 INT,
	@Email VARCHAR(255),
	@Email2 VARCHAR(255),
	@DOB DATETIME,
	@Occupation VARCHAR(100),
	@Height VARCHAR(10),
	@Weight VARCHAR(10),
	@EmployeeId INT,
	@SpanishSpeaker TINYINT, --Saeed 8585 12/04/2010 add new paramter.
	@SsnNo VARCHAR(100)
AS
UPDATE Patient
SET
	LastName = @LastName,
	FirstName = @FirstName,
	[Address] = @Address,
	City = @City,
	StateId = @StateId,
	Zip = @Zip,
	Contact1 = @Contact1,
	ContactType1 = @ContactType1,
	Contact2 = @Contact2,
	ContactType2 = @ContactType2,
	Email = @Email,
	Email2 = @Email2,
	DOB = @DOB,
	Occupation = @Occupation,
	Height = @Height,
	[Weight] = @Weight,		
	EmployeeId = @EmployeeId,
	SpanishSpeaker=@SpanishSpeaker, --Saeed 8585 12/04/2010 update spanish speaker field.
	SsnNo=@SsnNo
	WHERE Id=@PatientId
	

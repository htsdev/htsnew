﻿/****** 
Create by:  Syed Muhammad Ozair

Business Logic : this procedure is used to get the picture name stored in docstorage.

List of Parameters:	
	@ScanBatchID : scan batch id for which records will be fetched
	@DocumentBatchID : document batch id for which records will be fetched

List of Columns: 	
	SBID : Scan Batch id  
	SBDID : Scan Batch Detail id
	DBID : document/letter id 	

******/

Create Procedure dbo.usp_HTP_Get_DocumentTracking_ScannedImages

@ScanBatchID int,
@DocumentBatchID int

as


(select scanbatchid_fk as SBID,scanbatchdetailid as SBDID,documentbatchid as DBID, documentbatchpageno  from tbl_htp_documenttracking_scanbatch_detail
where scanbatchid_fk=@ScanBatchID
and DocumentBatchID=@DocumentBatchID
union
select 0 as SBID, 0 as SBDID, 0 as DBID, 999999 as documentbatchpageno)
order by documentbatchpageno

go

grant exec on dbo.usp_HTP_Get_DocumentTracking_ScannedImages to dbr_webuser
go
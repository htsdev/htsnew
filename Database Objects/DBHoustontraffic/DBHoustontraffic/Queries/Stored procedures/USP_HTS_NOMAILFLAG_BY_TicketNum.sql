SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_NOMAILFLAG_BY_TicketNum]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_NOMAILFLAG_BY_TicketNum]
GO


create PROCEDURE [dbo].[USP_HTS_NOMAILFLAG_BY_TicketNum]  
@TicketNumber varchar(20)  
as  
INSERT INTO tbl_hts_donotmail_records 
(
FirstName, LastName, Address,Zip , City, StateID_FK) 
select FirstName,LastName,Address1,case when len(ZipCode)>5 then substring(ZipCode,1,5) else ZipCode end, City,stateid_fk from tblticketsarchive where TicketNumber = @TicketNumber  
Update tblticketsarchive  
set DoNotMailFlag =1  
WHERE  TicketNumber = @TicketNumber

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_All_PaymentInfoByRecDateRange]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_All_PaymentInfoByRecDateRange]
GO










CREATE procedure [dbo].[usp_Get_All_PaymentInfoByRecDateRange]   
  
@RecDate DateTime,  
@RecTo DateTime  
  
AS  

set @RecTo = @RecTo + '23:59:59.999'

  
Select	PaymentType, 
	Count(ticketID) As TotalCount, 
	Sum(ChargeAmount) As Amount   
From 	tblTicketsPayment  
Where  	PaymentVoid <> 1  
And    	( RecDate  Between  @RecDate AND @RecTo)  
/*  
( CONVERT(varchar(12),RecDate, 101)   >= CONVERT(varchar(12), @RecDate, 101)      
And  CONVERT(varchar(12),RecDate, 101)   <=  CONVERT(varchar(12), @RecTo, 101)    )  
*/  

  
Group by PaymentType  
Order By PaymentType  
  
  
  
  









GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


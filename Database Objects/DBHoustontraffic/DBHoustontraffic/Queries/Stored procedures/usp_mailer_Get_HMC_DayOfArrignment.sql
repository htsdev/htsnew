if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_mailer_Get_HMC_DayOfArrignment]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_mailer_Get_HMC_DayOfArrignment]
GO



--  usp_mailer_Get_HMC_DayOfArrignment 1, 48, 1, '2/7/08', '2/10/08', 1
create Procedure [dbo].[usp_mailer_Get_HMC_DayOfArrignment]
	(
	@catnum		int,
	@LetterType 	int,
	@crtid 		int,                                                 
	@startListdate 	Datetime,
	@endListdate 	DateTime,                                                      
	@SearchType     int                                             
	)

as                                                              
                                                            
declare	@officernum varchar(50), 
	@officeropr varchar(50), 
	@tikcetnumberopr varchar(50), 
	@ticket_no varchar(50), 
	@zipcode varchar(50), 
	@zipcodeopr varchar(50), 
	@fineamount money,
	@fineamountOpr varchar(50) ,
	@fineamountRelop varchar(50),
	@singleviolation money, 
	@singlevoilationOpr varchar(50) , 
	@singleviolationrelop varchar(50), 
	@doubleviolation money,
	@doublevoilationOpr varchar(50) , 
	@doubleviolationrelop varchar(50), 
	@count_Letters int,
	@violadesc varchar(500),
	@violaop varchar(50),
	@sqlquery nvarchar(4000)  ,
	@sqlquery2 nvarchar(4000)  ,
	@sqlParam nvarchar(1000)


select @sqlparam = '@catnum int, @LetterType int, @crtid int, @startListdate Datetime, 	
		    @endListdate DateTime, @SearchType int'

                                                             
                                                      
select 	@endlistdate = @endlistdate+'23:59:58.000', 
	@sqlquery ='',
	@sqlquery2 = ''
                                                              
Select	@officernum=officernum,
	@officeropr=oficernumOpr,
	@tikcetnumberopr=tikcetnumberopr,
	@ticket_no=ticket_no,
	@zipcode=zipcode,
	@zipcodeopr=zipcodeLikeopr,
	@singleviolation =singleviolation,
	@singlevoilationOpr =singlevoilationOpr, 
	@singleviolationrelop =singleviolationrelop, 
	@doubleviolation = doubleviolation,
	@doublevoilationOpr=doubleviolationOpr,
	@doubleviolationrelop=doubleviolationrelop,
	@violadesc=violationdescription,
	@violaop=violationdescriptionOpr , 
	@fineamount=fineamount ,
	@fineamountOpr=fineamountOpr ,
	@fineamountRelop=fineamountRelop  
from 	tblmailer_letters_to_sendfilters  
where 	courtcategorynum=@catnum 
and 	Lettertype=@LetterType 
                                                      
set 	@sqlquery=@sqlquery+'                                          
 Select distinct '
				
if @searchtype = 0 
	set 	@sqlquery=@sqlquery+'      
	convert(datetime , convert(varchar(10),ta.listdate,101)) as mdate,   '
else
	set 	@sqlquery=@sqlquery+'                                                         
	convert(datetime , convert(varchar(10),tva.courtdate,101)) as mdate, '

set 	@sqlquery=@sqlquery+'      
 flag1 = isnull(ta.Flag1,''N'') ,                                                      
 ta.donotmailflag, ta.recordid,                                                     
 count(tva.ViolationNumber_PK) as ViolCount,                                                      
 isnull(sum(isnull(tva.fineamount,0)),0) as Total_Fineamount
into #temp                                                            
FROM dbo.tblCourtViolationStatus tcvs,  dbo.tblTicketsViolationsArchive TVA  ,dbo.tblTicketsArchive TA                     
where tcvs.CourtViolationStatusID = TVA.violationstatusid 
and  TVA.recordid = TA.recordid
and tcvs.CategoryID=2
and ta.clientflag=0 
and isnull(ta.ncoa48flag,0) = 0'               

if @SearchType = 0 
		set @sqlquery =@sqlquery+ ' 
		and datediff(day,  ta.listdate , @startListdate)<=0
		and datediff(day, ta.listdate, @endlistdate)>= 0 '
else if @searchtype = 1 
		set @sqlquery =@sqlquery+ ' 
		and datediff(day,  tva.courtdate , @startListdate )<=0 
		and datediff(day, tva.courtdate, @endlistdate )>=0 '

if(@crtid=1 or @crtid=2 or @crtid=3 or @crtid=4 )                                    
	set @sqlquery=@sqlquery+' and tva.courtlocation In (Select courtid from tblcourts where courtcategorynum = @crtid )'                                        

if( @crtid >= 3000 )                                    
	set @sqlquery =@sqlquery +' and tva.courtlocation = @crtid' 
                                                     
if(@officernum<>'')                                                        
	set @sqlquery =@sqlquery + '  and ta.officerNumber_Fk '  + @officeropr  +'('+ @officernum+')'                                                         
                                                       
if(@ticket_no<>'')                        
	set @sqlquery=@sqlquery + ' and ('+  dbo.Get_String_Concat_With_op(''+@ticket_no+'' , 'tva.TicketNumber_PK' ,+ ''+ @tikcetnumberopr) +')'                                                              
                                    
if(@zipcode<>'')                                                                              
	set @sqlquery=@sqlquery+ ' and ('+ dbo.Get_String_Concat_With_op(''+@zipcode+'' , 'left(ta.zipcode,3)' ,+ ''+ @zipcodeopr) +')'                                   

if(@fineamount<>0 and @fineamountOpr<>'not'  )                                  
	set @sqlquery =@sqlquery+ '
        and  tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                                       
                                                      
if(@fineamount<>0 and @fineamountOpr = 'not'  )                                  
	set @sqlquery =@sqlquery+ '
        and not  tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                                       


                                                     
if @searchtype = 0 
	set @sqlquery =@sqlquery+ ' group by                                                       
 convert(datetime , convert(varchar(10),ta.listdate,101)) ,
 ta.Flag1,      
 ta.donotmailflag    , ta.recordid   
having 1 =1                                                
'                                  
else
	set @sqlquery =@sqlquery+ ' group by                                                       
  convert(datetime , convert(varchar(10),tva.courtdate,101)),
  ta.Flag1,      
  ta.donotmailflag     , ta.recordid  
having 1 = 1                                                
'                                  

if(@singleviolation<>0 and @doubleviolation=0)                                
	set @sqlquery =@sqlquery+ '
	and (  '+ @singlevoilationOpr+  '  (isnull(sum(isnull(tva.fineamount,0)),0) '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and count(tva.ViolationNumber_PK)=1))
      '                                
if(@doubleviolation<>0 and @singleviolation=0 )                                
	set @sqlquery =@sqlquery + '
	and ('+ @doublevoilationOpr+  '   (isnull(sum(isnull(tva.fineamount,0)),0) '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ')and count(tva.ViolationNumber_PK)=2) )
      '                                                    
                                
if(@doubleviolation<>0 and @singleviolation<>0)                                
	set @sqlquery =@sqlquery+ '
	and ('+ @singlevoilationOpr+  '  (isnull(sum(isnull(tva.fineamount,0)),0) '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and count(tva.ViolationNumber_PK)=1))                                
    and ('+ @doublevoilationOpr+  '  (isnull(sum(isnull(tva.fineamount,0)),0) '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ') and count(tva.ViolationNumber_PK)=2))
        '                                  

set @sqlquery =@sqlquery+ '              
delete from #temp where recordid  in (                                                                    
	select	n.recordid from tblletternotes n inner join tblticketsarchive t on t.recordid = n.recordid
	where 	lettertype =@LetterType 
	)'


set @sqlquery=@sqlquery + ' 
Select distinct  a.ViolCount, a.Total_Fineamount, a.mdate , a.Flag1, a.donotmailflag, a.recordid 
into #temp1  from #temp a, tblticketsviolationsarchive tva where a.recordid = tva.recordid 

 '                                  

-- VIOLATION DESCRIPTION FILTER.....
if(@violadesc <> '')
begin
-- NOT LIKE FILTER.......
if(charindex('not',@violaop)<> 0 )              
	set @sqlquery=@sqlquery+' 
	and	 not ( a.violcount =1  and ('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ 'like')+'))'           

-- LIKE FILTER.......
if(charindex('not',@violaop) =  0 )              
	set @sqlquery=@sqlquery+' 
	and	 ( a.violcount =1  and ('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ 'like')+'))'           
end
                                  
set @sqlquery2=@sqlquery2 +'
Select count(distinct recordid) as Total_Count, mdate 
into #temp2 
from #temp1 
group by mdate 
  
Select count(distinct recordid) as Good_Count, mdate 
into #temp3
from #temp1 
where Flag1 in (''Y'',''D'',''S'')   
and donotmailflag = 0    
group by mdate   
                                                      
Select count(distinct recordid) as Bad_Count, mdate  
into #temp4  
from #temp1 
where Flag1  not in (''Y'',''D'',''S'') 
and donotmailflag = 0 
group by mdate 
                   
Select count(distinct recordid) as DonotMail_Count, mdate  
into #temp5   
from #temp1 
where donotmailflag=1 
group by mdate

create table #letterdates (mdate datetime)
while datediff(day, @startListdate, @endlistdate)>=0
begin
	insert into #letterdates (mdate) select convert(varchar(10),@startlistdate,101)
	set @startlistdate = dateadd(day, 1, @startlistdate)
end
'
	
         
set @sqlquery2 = @sqlquery2 +'                                                        
Select 	l.mdate as listdate,
        isnull(Total_Count,0) as Total_Count,
        isnull(Good_Count,0) Good_Count,  
        isnull(Bad_Count,0)Bad_Count,  
        isnull(DonotMail_Count,0)DonotMail_Count  
from #letterdates l left outer join #Temp2 on #temp2.mdate = l.mdate left outer join #temp3 
on #temp2.mdate=#temp3.mdate  
left outer join #Temp4  
on #temp2.mdate=#Temp4.mdate    
left outer join #Temp5  
on #temp2.mdate=#Temp5.mdate                                                       
order by l.mdate

Select count(distinct recordid) from #temp1 
where Flag1 in (''Y'',''D'',''S'') and donotmailflag = 0  

drop table #temp drop table #temp1 drop table #temp2  drop table #temp3  drop table #temp4  drop table #temp5  drop table #letterdates
'                                                      
                                                      
print @sqlquery  + @sqlquery2                                       
--exec( @sqlquery)
set @sqlquery = @sqlquery + @sqlquery2
exec sp_executesql @sqlquery , @sqlparam, @catnum, @LetterType, @crtid, @startListdate, @endListdate, @SearchType




go

grant execute on [dbo].[usp_mailer_Get_HMC_DayOfArrignment] to dbr_webuser
go
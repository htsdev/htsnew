/*        
    
Business Logic  : this stored procedure is used to get case summary information for cover sheet report.      
           
Parameter:       
   @TicketId     : Ticket Id of a client
   @Sections	 : Report section value.
   
     
      
*/ 
ALTER PROCEDURE [dbo].[USP_HTS_GET_CaseSummary_Report_Data]    
@TicketId_pk INT,    
@Sections as varchar(6)    
as  
declare @PaidAmount money,     
 @AmountOwes money     
     
select @PaidAmount = (     
 select isnull(sum(isnull(chargeAmount,0)) ,0)     
 from tblticketspayment where ticketid = @TicketId_pk     
 and paymentvoid = 0     
 and paymenttype not in (0,98,99,100)     
 )     
select @AmountOwes = calculatedtotalfee - @paidamount     
from tbltickets where ticketid_pk = @TicketId_pk     
     
-- Extract Flag in a Separate Variable in HTML Format    
    
create table #temp1 (Flags varchar(1000))    
    
declare @F as varchar(50)    
    
    
select     
@F =case when bondflag =1 then 'BOND' else '0' end    
from tbltickets     
where ticketid_pk=@TicketId_pk    
    
if (@F <> '0')    
begin    
insert into #temp1 values (@F)     
end    
    
--- Geting AccidentFlag Flag    
select     
@F =case when AccidentFlag =1 then 'ACC' else '0' end    
from tbltickets     
where ticketid_pk=@TicketId_pk    
if (@F <> '0')    
begin    
insert into #temp1 values (@F)     
end    
    
-- Geting CDLFlag Flag    
select     
@F =case when CDLFlag =1 then 'CDL' else '0' end    
from tbltickets     
where ticketid_pk=@TicketId_pk    
if (@F <> '0')    
begin    
insert into #temp1 values (@F)     
end    
    
--- Geting all Flags which selected to genral info     
insert into #temp1    
select       
 te.Description as Flags    
 from tblticketsflag tf inner join     
   tbleventflags te on tf.flagid = te.flagid_pk    
 where tf.ticketid_pk=@TicketId_pk order by Flags    
    
declare @FlagsHtml varchar(2000)    
    
set @FlagsHtml = ''    
select @FlagsHtml = @FlagsHtml + '<b>  &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;    >> &nbsp; </b> ' + flags + '     ' from #temp1    
    
--select @FlagsHtml    

    
drop table #temp1    
    
     
Select top 1     
@Sections as Sections,     
isnull(t.Lastname,'') as lastName,     
isnull(t.Firstname,'') as FirstName,     
isnull(t.MiddleName,'') as MiddleName,     
isnull(d.Description ,'')AS CaseStatus,     
CONVERT(varchar(20),v.courtDateMain,0) AS CourtDate,     
isnull(v.courtnumbermain,'0') as CourtNum, --Sabir Khan 7979 07/07/2010 type changed      
isnull(c.CourtName,'') as CourtName,     
isnull(c.Address,'') AS CourtAddress,     
FirmAbb = (case when t.firmid = 3000 then '' else isnull(f.FirmAbbreviation,'' ) end ) ,     
isnull(v.remindercomments,'') as ConComments,           
isnull(t.GeneralComments,'') as GenComments,     
isnull(t.SettingComments,'') as SetComments ,     
isnull(t.TrialComments,'') as TriComments,    
(    
convert(varchar(50),isnull(t.Address1,'')) + convert(varchar(50),isnull(', '+t.Address2,'')) +', '+     
case when isnull(t.City,'') <> '' then (convert(varchar(50),isnull(t.City,''))+', ') else '' end+     
case when isnull(ts.State,'') <> '' then (convert(varchar(50),isnull(ts.State,''))+', ') else '' end+     
case when isnull(t.zip,'') <> '' then (convert(varchar(50),isnull(t.zip,''))) else '' end    
) as Address,    
isnull(t.Midnum,'') as MidNumber,     
isnull(t.DLNumber,'') as DLNum,     
convert(varchar(10),isnull(t.DOB,''),101) as DOBirth,    
--Convert(varchar,isnull(t.Contact1,'')) as Contact1, + '',      
 --Convert(varchar,isnull(c3.Description,'')) as Contactype1, + '',     
--Convert(varchar,isnull(t.Contact2,'')) as Contact2,+ '',    
--Convert(varchar,isnull(c1.Description,'')) as Contactype2,+ '',    
--Convert(varchar,isnull(t.Contact3,'')) as Contact3,+ '',    
--Convert(varchar,isnull(c2.Description,'')) as Contactype3, + '',   
dbo.fn_FormatContactNumber_new(Convert(varchar,isnull(t.Contact1,'')),Convert(varchar,isnull(t.ContactType1,''))) as Contact1,  
dbo.fn_FormatContactNumber_new(Convert(varchar,isnull(t.Contact2,'')),Convert(varchar,isnull(t.ContactType2,''))) as Contact2,  
dbo.fn_FormatContactNumber_new(Convert(varchar,isnull(t.Contact3,'')),Convert(varchar,isnull(t.ContactType3,''))) as Contact3,  
  
isnull(t.email,'') as email,    
isnull(left(t.LanguageSpeak,3),'') as Language,     
isnull((case t.CDLFlag     
 when 0 then 'No'     
 when 1 then 'Yes'     
end),'') as CDLFlag,     
isnull((case t.AccidentFlag     
 when 0 then 'No'     
 when 1 then 'Yes'     
end),'') as AccidentFlag,     
isnull(t.calculatedtotalfee,0) as TotFee,     
isnull(t.BaseFeeCharge,0) as Initial,     
isnull(t.RerapAmount,0) as Rerap,     
isnull(t.ContinuanceAmount,0)as Continuance,     
isnull(t.InitialAdjustment,0) as InitialAdjustment,     
isnull(t.Adjustment,0) as Adjust,     
isnull(t.Addition,0) as Addition,     
isnull( c3.Description ,'')AS Description1,     
isnull(c1.Description ,'')AS Description2,     
isnull(c2.Description ,'')AS Description3,     
isnull(@PaidAmount,0) as PaidAmount,     
isnull(@AmountOwes,0) as AmountOwes,     
isnull(t.City,'') as cities,     
isnull(t.Zip,'') as zipcode,     
isnull(ts.State,'') as states,     
isnull(o.firstname +' '+ o.lastname,'N/A') as OfficerName,     
isnull(o.officertype,'') as officertype,     
bond = ( case when t.bondflag = 1 then 'BOND' else '' end ),     
@FlagsHtml as Flag    
FROM dbo.tblEventFlags tf_1 INNER JOIN     
 dbo.tblTicketsFlag tf ON tf_1.FlagID_PK = tf.FlagID RIGHT OUTER JOIN     
 dbo.tblFirm f RIGHT OUTER JOIN     
 dbo.tblCourts c RIGHT OUTER JOIN     
 dbo.tblTickets t LEFT OUTER JOIN     
 dbo.tblticketsviolations v ON t.ticketid_pk=v.ticketid_pk Left OUTER JOIN     
 dbo.tblContactstype c2 ON t.ContactType3 = c2.ContactType_PK LEFT OUTER JOIN     
 dbo.tblContactstype c1 ON t.ContactType2 = c1.ContactType_PK LEFT OUTER JOIN     
 dbo.tblContactstype c3 ON t.ContactType1 = c3.ContactType_PK LEFT OUTER JOIN     
 dbo.tblOfficer o ON t.OfficerNumber = o.OfficerNumber_PK LEFT OUTER JOIN     
 dbo.tblDateType d ON v.courtviolationstatusidmain = d.TypeID ON c.Courtid = v.courtId ON f.FirmID = t.FirmID ON     -- Agha Usman 2664 06/30/2008
 tf.TicketID_PK = t.TicketID_PK LEFT OUTER JOIN     
 dbo.tblState ts ON t.Stateid_FK = ts.StateID     
WHERE t.TicketID_PK = @TicketId_pk  
go
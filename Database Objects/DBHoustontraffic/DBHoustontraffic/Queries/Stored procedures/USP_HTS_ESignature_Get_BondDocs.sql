set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


/*        
Altered By     : Fahad Muhammad Qureshi.
Created Date   : 04/23/2009  
TasK		   : 5806        
Business Logic : This procedure retrived all the information about the Bond Report's Documnets,No of pages .  
           
Parameter:       
	@TicketID   : identifiable key  TicketId      
	@empid		: Employee which have Right to access the system
	@FirstTime	: flag to know that either signature is first time or more than first time
	@SessionID	: Session identifier 
	@DocumentBatchID	: Document identification id 
     
      
*/  

-- USP_HTS_ESignature_Get_BondDocs 61593, 3992, 0, '55777744422335953', 3
ALTER PROCEDURE [dbo].[USP_HTS_ESignature_Get_BondDocs]  
@TicketIDList int,                  
@empid int,  
@FirstTime bit,  
@SessionID varchar(50),
--ozair 3927 on 05/04/2008
@DocumentBatchID int = 0  
--end ozair 3927
  
AS                  
SET NOCOUNT ON  
Declare @SQLString nvarchar(4000)                    
                  --Yasir Kamal 5627 03/06/2009 Bond Letter Changes 
SELECT     tblTicketsViolations.TicketID_PK, (CASE WHEN isnull(tblTicketsViolations.CourtId, 0) IN (3001, 3002, 3003) THEN 0 ELSE SUM(dbo.tblTicketsViolations.BondAmount) END) AS BondAmount  
				  --5627 end
into #temp1  
FROM         tblTicketsViolations INNER JOIN  
                      tblViolations ON tblTicketsViolations.ViolationNumber_PK = tblViolations.ViolationNumber_PK INNER JOIN  
                      tblTickets ON tblTickets.TicketID_PK = tblTicketsViolations.TicketID_PK  
WHERE     (tblViolations.Violationtype NOT IN (1)) AND (tblTicketsViolations.UnderlyingBondFlag = 1) AND (tblTicketsViolations.TicketID_PK = @TicketIDList) OR  
                      (tblViolations.Violationtype NOT IN (1)) AND (tblTicketsViolations.TicketID_PK = @TicketIDList) AND (tblTicketsViolations.CourtViolationStatusID = 135)  
GROUP BY tblTicketsViolations.TicketID_PK, tblTicketsViolations.CourtId  
                    
SELECT DISTINCT   
                      tblTickets.TicketID_PK, tblTicketsViolations.RefCaseNumber, tblViolations.Description, 
				 --Yasir Kamal 5627 03/06/2009 Bond Letter Changes   
                      (CASE WHEN isnull(tblTicketsViolations.CourtId, 0) IN (3001, 3002, 3003) THEN 0 ELSE dbo.tblTicketsViolations.BondAmount END) AS BondAmount, tblTickets.Firstname,   
				 --5627 end 
                      tblTickets.MiddleName, tblTickets.Lastname, tblTickets.Address1, tblTickets.Address2, tblTickets.Midnum, tblTickets.City, tblState_1.State,   
                      tblTickets.Zip, tblTickets.DLNumber, tblState.State AS DLState, tblTickets.DOB, tblTickets.SSNumber, tblTickets.Race, tblTickets.gender,   
                      tblTickets.Height, tblTickets.Weight, tblTickets.HairColor, tblTickets.Eyes, tblTickets.LanguageSpeak, tblCourts.CourtName AS Crt_CourtName,   
                      tblCourts.Address AS Crt_Address, tblCourts.Address2 AS Crt_Address2, tblCourts.City AS Crt_City, tblState_2.State AS Crt_State,   
                      tblCourts.Zip AS Crt_Zip, tblTicketsViolations.casenumassignedbycourt, tblCourts.courtcategorynum 
into #tbltemp   
FROM         tblTicketsViolations INNER JOIN  
                      tblViolations ON tblViolations.ViolationNumber_PK = tblTicketsViolations.ViolationNumber_PK LEFT OUTER JOIN  
                      tblTickets ON tblTicketsViolations.TicketID_PK = tblTickets.TicketID_PK LEFT OUTER JOIN  
                      tblState ON tblTickets.DLState = tblState.StateID LEFT OUTER JOIN  
                      tblState AS tblState_1 ON tblTickets.Stateid_FK = tblState_1.StateID LEFT OUTER JOIN  
                      tblCourts ON tblCourts.Courtid = tblTicketsViolations.CourtID LEFT OUTER JOIN  
                      tblState AS tblState_2 ON tblCourts.State = tblState_2.StateID  
WHERE     (tblViolations.Violationtype NOT IN (1)) AND (tblTickets.TicketID_PK = @TicketIDList) AND (tblTicketsViolations.UnderlyingBondFlag = 1) AND   
                      (tblTicketsViolations.CourtViolationStatusID <> 80) OR  
                      (tblViolations.Violationtype NOT IN (1)) AND (tblTickets.TicketID_PK = @TicketIDList) AND (tblTicketsViolations.CourtViolationStatusID = 135) AND   
                      (tblTicketsViolations.CourtViolationStatusID <> 80)  
ORDER BY tblTickets.TicketID_PK, tblTicketsViolations.RefCaseNumber
  
select  a.TicketID_PK, a.RefCaseNumber,  a.[Description],  casenumassignedbycourt  ,                 
  a.BondAmount, a.Firstname, a.MiddleName, a.Lastname,  a.Address1, a.Address2, a.Midnum, a.City, a.State, a.Zip,                     
  a.DLNumber, a.DLState, a.DOB, a.SSNumber, a.Race, a.gender, a.Height, a.Weight, a.HairColor, a.Eyes,  a.languagespeak,                     
  a.Crt_CourtName , a.Crt_Address , a.Crt_Address2, a.Crt_City , a.Crt_State , a.Crt_Zip, b.BondAmount as totalBondAmount , courtcategorynum                 
into #TbltempBond                  
from #tbltemp a, #temp1 b where a.TicketID_PK = b.TicketID_PK                  
  
alter table #tbltempbond add dup varchar(25)                  
  
alter table #tbltempbond add Counter int                  
               
alter table #tbltempbond add PageNumber int, Serial int NOT NULL IDENTITY (1, 1)  
  
select * into #tbltempbond_Real from #tbltempbond  
  
drop table #tbltempbond  
  
declare @pagenumber int  
 set @pagenumber = 1  
  
declare @Casenum as varchar(20)  
declare @Serial as int  

  
declare setpage cursor for   
select refcasenumber, Serial from #tbltempbond_Real  
open setpage  
FETCH NEXT FROM setpage  
into @Casenum, @Serial  
WHILE @@FETCH_STATUS = 0  
BEGIN  
 update #tbltempbond_Real set pagenumber = @pagenumber  where refcasenumber = @Casenum and Serial = @Serial  
   
 set @pagenumber = @pagenumber + 1   
  
 FETCH NEXT FROM setpage  
 into @Casenum, @Serial  
END  
CLOSE setpage  
DEALLOCATE setpage  
--select '#tbltempbond_Real', * from #tbltempbond_Real  
set @pagenumber = @pagenumber + 3   
declare @agreement int  
declare @contract int  
declare @POA int  
  
set @agreement = @pagenumber -3  
set @contract = @pagenumber -2  
set @POA = @pagenumber -1  
  
select * into #tbltempbond2  
from #tbltempbond_Real  
  
declare setpage2 cursor for   
select refcasenumber, Serial from #tbltempbond2  
open setpage2  
FETCH NEXT FROM setpage2  
into @Casenum, @Serial  
WHILE @@FETCH_STATUS = 0  
BEGIN  
 update #tbltempbond2 set pagenumber = @pagenumber where refcasenumber = @Casenum and Serial = @Serial  
 set @pagenumber = @pagenumber + 1   
 FETCH NEXT FROM setpage2  
 into @Casenum, @Serial  
END  
CLOSE setpage2  
DEALLOCATE setpage2  
  
select * into #tbltempbond1                  
from #tbltempbond_Real  
                  
update #tbltempbond2 set dup = '*'                  
                  
select Top 1 *                   
into #tbltempbond3                  
from #tbltempbond2                  
                  
update #tbltempbond3 set dup = '' , pagenumber = @pagenumber, casenumassignedbycourt = null, midnum = null  
  
update #tbltempbond2 set Counter = 2                  
update #tbltempbond3 set  Counter = 3 ,  Description = '' ,  BondAmount = 0 , totalBondAmount = 0, TicketID_PK = Null, RefCaseNumber = Null                 
update #tbltempbond1 set Counter = 1                  
        
insert into #tbltempbond2 (TicketID_PK, RefCaseNumber, [Description],  casenumassignedbycourt,                   
  BondAmount, Firstname, MiddleName, Lastname,  Address1, Address2, Midnum, City, State, Zip,                     
  DLNumber, DLState, DOB, SSNumber, Race, gender, Height, Weight, HairColor, Eyes,  languagespeak,                     
  Crt_CourtName , Crt_Address , Crt_Address2, Crt_City , Crt_State , Crt_Zip, totalBondAmount, dup,Counter, PageNumber, courtcategorynum)                  
select TicketID_PK, RefCaseNumber, [Description],   casenumassignedbycourt ,                 
  BondAmount, Firstname, MiddleName, Lastname,  Address1, Address2, Midnum, City, State, Zip,                     
  DLNumber, DLState, DOB, SSNumber, Race, gender, Height, Weight, HairColor, Eyes,  languagespeak,                     
  Crt_CourtName , Crt_Address , Crt_Address2, Crt_City , Crt_State , Crt_Zip, totalBondAmount, dup,Counter,PageNumber, courtcategorynum   
from  #tbltempbond1                  
union all                  
select TicketID_PK, RefCaseNumber, [Description],    casenumassignedbycourt ,                
  BondAmount, Firstname, MiddleName, Lastname,  Address1, Address2, Midnum, City, State, Zip,                     
  DLNumber, DLState, DOB, SSNumber, Race, gender, Height, Weight, HairColor, Eyes,  languagespeak,                     
  Crt_CourtName , Crt_Address , Crt_Address2, Crt_City , Crt_State , Crt_Zip, totalBondAmount, dup, Counter, PageNumber, courtcategorynum  
from #tbltempbond3                  
  
alter table #tbltempbond2   
add atr1  image,   
 atr2  image,    
 atr3  image,     
 Client1  image,  
 Thumbnail image,
 --Fahad 5806 04/23/2009 Initial Images Removed from the Bond Report  
-- Initial1 image,  
-- Initial2 image,  
-- Initial3 image,  
-- Initial4 image,  
-- Initial5 image,  
-- Initial6 image,  
-- Initial7 image,  
-- Initial8 image,  
 Client2  image,  
 Client3  image,  
 Client4  image  
  
select * into #tbltempbond2_Real from #tbltempbond2  
  
drop table #tbltempbond2  
declare @SrPg int  
declare setpage cursor for   
select pagenumber from #tbltempbond2_Real order by pagenumber  
open setpage  
FETCH NEXT FROM setpage  
into @SrPg  
WHILE @@FETCH_STATUS = 0  
BEGIN  
 update #tbltempbond2_Real set atr1 =(SELECT  Top 1   convert(varbinary(8000),ImageStream)  
   FROM         Tbl_HTS_ESignature_Images  
    WHERE     (PageNumber = @SrPg) AND (ImageTrace = 'ImageStream_Bond_Attorney1') And (TicketID = @TicketIDList) And (SessionID = @SessionID))  
     WHERE     (PageNumber = @SrPg)  
  
 update #tbltempbond2_Real set atr2 =(SELECT  Top 1   convert(varbinary(8000),ImageStream)  
   FROM         Tbl_HTS_ESignature_Images  
    WHERE     (PageNumber = @SrPg) AND (ImageTrace = 'ImageStream_Bond_Attorney2') And (TicketID = @TicketIDList) And (SessionID = @SessionID))  
     WHERE     (PageNumber = @SrPg)  
  
 update #tbltempbond2_Real set atr3 =(SELECT  Top 1   convert(varbinary(8000),ImageStream)  
   FROM         Tbl_HTS_ESignature_Images  
    WHERE     (PageNumber = @SrPg) AND (ImageTrace = 'ImageStream_Bond_Attorney3') And (TicketID = @TicketIDList) And (SessionID = @SessionID))  
     WHERE     (PageNumber = @SrPg)  
  
 if @FirstTime = 0  
  Begin  
  
   update #tbltempbond2_Real set Client1 =(SELECT Top 1    convert(varbinary(8000),ImageStream)  
     FROM         Tbl_HTS_ESignature_Images  
      WHERE     (PageNumber = @SrPg) AND (ImageTrace = 'ImageStream_Bond_ClientSign1') And (TicketID = @TicketIDList) And (SessionID = @SessionID))  
       --WHERE     (PageNumber = @SrPg)  
   WHERE (Client1 IS NULL)
  
   update #tbltempbond2_Real set Thumbnail =(SELECT Top 1    convert(varbinary(8000),ImageStream)  
     FROM         Tbl_HTS_ESignature_Images  
      WHERE     (PageNumber = @SrPg) AND (ImageTrace = 'ImageStream_Bond_ClientThumbprint1') And (TicketID = @TicketIDList) And (SessionID = @SessionID))   
       WHERE     (Thumbnail Is Null)  
  End  
  
 set @pagenumber = @pagenumber + 1   
  
 FETCH NEXT FROM setpage  
 into @SrPg  
END  
CLOSE setpage  
DEALLOCATE setpage  
 if @FirstTime = 0  
  Begin  
--  Fahad 5806 04/23/2009 Initial Images Removed from the Bond Report  
--   update #tbltempbond2_Real set Initial1 =(SELECT  Top 1   convert(varbinary(8000),ImageStream)  
--     FROM         Tbl_HTS_ESignature_Images  
--      WHERE     (PageNumber = @agreement) AND (ImageTrace = 'ImageStream_Bond_ClientInitial1') And (TicketID = @TicketIDList) And (SessionID = @SessionID))  
--       WHERE  (TicketID_PK Is Not Null)  
--  
--   update #tbltempbond2_Real set Initial2 =(SELECT  Top 1   convert(varbinary(8000),ImageStream)  
--     FROM         Tbl_HTS_ESignature_Images  
--      WHERE     (PageNumber = @agreement) AND (ImageTrace = 'ImageStream_Bond_ClientInitial2') And (TicketID = @TicketIDList) And (SessionID = @SessionID))  
--       WHERE  (TicketID_PK Is Not Null)  
--  
--   update #tbltempbond2_Real set Initial3 =(SELECT  Top 1   convert(varbinary(8000),ImageStream)  
--     FROM         Tbl_HTS_ESignature_Images  
--      WHERE     (PageNumber = @agreement) AND (ImageTrace = 'ImageStream_Bond_ClientInitial3') And (TicketID = @TicketIDList) And (SessionID = @SessionID))  
--       WHERE  (TicketID_PK Is Not Null)  
--  
--   update #tbltempbond2_Real set Initial4 =(SELECT  Top 1   convert(varbinary(8000),ImageStream)  
--     FROM         Tbl_HTS_ESignature_Images  
--      WHERE     (PageNumber = @agreement) AND (ImageTrace = 'ImageStream_Bond_ClientInitial4') And (TicketID = @TicketIDList) And (SessionID = @SessionID))  
--       WHERE  (TicketID_PK Is Not Null)  
--  
--   update #tbltempbond2_Real set Initial5 =(SELECT  Top 1   convert(varbinary(8000),ImageStream)  
--     FROM         Tbl_HTS_ESignature_Images  
--      WHERE     (PageNumber = @agreement) AND (ImageTrace = 'ImageStream_Bond_ClientInitial5') And (TicketID = @TicketIDList) And (SessionID = @SessionID))  
--       WHERE  (TicketID_PK Is Not Null)  
--  
--   update #tbltempbond2_Real set Initial6 =(SELECT  Top 1   convert(varbinary(8000),ImageStream)  
--     FROM         Tbl_HTS_ESignature_Images  
--      WHERE     (PageNumber = @agreement) AND (ImageTrace = 'ImageStream_Bond_ClientInitial6') And (TicketID = @TicketIDList) And (SessionID = @SessionID))  
--       WHERE  (TicketID_PK Is Not Null)  
--  
--   update #tbltempbond2_Real set Initial7 =(SELECT  Top 1   convert(varbinary(8000),ImageStream)  
--     FROM         Tbl_HTS_ESignature_Images  
--      WHERE     (PageNumber = @agreement) AND (ImageTrace = 'ImageStream_Bond_ClientInitial7') And (TicketID = @TicketIDList) And (SessionID = @SessionID))  
--       WHERE  (TicketID_PK Is Not Null)  
--  
--   update #tbltempbond2_Real set Initial8 =(SELECT  Top 1   convert(varbinary(8000),ImageStream)  
--     FROM         Tbl_HTS_ESignature_Images  
--      WHERE     (PageNumber = @agreement) AND (ImageTrace = 'ImageStream_Bond_ClientInitial8') And (TicketID = @TicketIDList) And (SessionID = @SessionID))  
--       WHERE  (TicketID_PK Is Not Null)  
  
   update #tbltempbond2_Real set Client2 =(SELECT   Top 1  convert(varbinary(8000),ImageStream)  
     FROM         Tbl_HTS_ESignature_Images  
      WHERE     (PageNumber = @agreement) AND (ImageTrace = 'ImageStream_Bond_ClientSign2') And (TicketID = @TicketIDList) And (SessionID = @SessionID))  
       WHERE  (TicketID_PK Is Not Null)  
  
   update #tbltempbond2_Real set Client3 =(SELECT   Top 1  convert(varbinary(8000),ImageStream)  
     FROM         Tbl_HTS_ESignature_Images  
      WHERE     (PageNumber = @contract) AND (ImageTrace = 'ImageStream_Bond_ClientSign3') And (TicketID = @TicketIDList) And (SessionID = @SessionID))  
       WHERE  (TicketID_PK Is Not Null)  
  
   update #tbltempbond2_Real set Client4 =(SELECT   Top 1  convert(varbinary(8000),ImageStream)  
     FROM         Tbl_HTS_ESignature_Images  
      WHERE     (PageNumber = @POA) AND (ImageTrace = 'ImageStream_Bond_ClientSign4') And (TicketID = @TicketIDList) And (SessionID = @SessionID))  
       WHERE  (TicketID_PK Is Not Null)  
  
 End  
  
alter table #tbltempbond2_Real drop column Serial  
  
select *,@DocumentBatchID as DBID
--ozair 4370 on 07/07/2008
,(select count(te.RefCaseNumber) from #tbltempbond2_Real te where te.counter=1) as violcnt,
--end ozair 4370 
--into tbl_Temptable  
(CASE WHEN client1 IS NULL THEN 0 ELSE 1 END) AS HasClient1_Sign,
(CASE WHEN thumbnail IS NULL THEN 0 ELSE 1 END) AS HasThumbprint
from #tbltempbond2_Real WHERE ISNULL(dup,'') <> '*' order by pagenumber  
  
drop table #temp1  
drop table #tbltemp  
drop table #tbltempbond1  
drop table #tbltempbond3  
drop table #tbltempbond_Real  
drop table #tbltempbond2_Real  





/******   
created by:  Tahir Ahmed  
Business Logic : The stored procedure is used for HTP /Activities /Set Calls report. It displays all clients that need to be 
				 called for their new court settings. 
				 Note: this same procedure is also used for auto dialer; when the optional auto dialer parameters are passed 
						incase of auto dialer it only returns ticket ids
       
List of Parameters:   
    @reminderdate : date specify by user  
    @status       : call status  
       0 Pending  
       1 Contacted  
       2 Left Message  
       3 No Answer  
       4 Wrong Telephone Number  
       5 All  
    @showall    : show all record or not   
    @showreport   : set call report  
    @mode     : for getting spacific record.  
        if mode=1  
        display all bond clients.  
        if mode=2  
        display all regular clients.  
        if mode=3  
        display all clients.
        
    optional parameter for auto dialer; make sure that these parameters should be at last in this list
    @isAD               BIT = 0, if 1 than means comming from auto dialer and show all should be 0
    @reminderstartdate  DATETIME = '01/01/1900', start date from which searching should be made for autodialer
    @reminderenddate    DATETIME = '01/01/1900'  end date to which seaching should be made for autodialer
******/     
    
   
   
-- USP_HTP_Get_SetCallsActivities '10/05/2009',0,0,1,-1,3,1,'01/18/2012','01/21/2012'
    
ALTER PROCEDURE [dbo].[USP_HTP_Get_SetCallsActivities] --'04/28/2006',2                        
(
    @reminderdate       DATETIME,
    @status             INT = 0,
    @showall            INT = 0,
    @showreport         INT = 1,
    @language           INT,
    @mode               INT,	/* parameter has been added for getting spacific record. */
    --ozair 4194 03/13/2009 optional parameter for auto dialer; make sure that these parameters should be at last in this list
    @isAD               BIT = 0,
    @reminderstartdate  DATETIME = '01/01/1900',
    @reminderenddate    DATETIME = '01/01/1900',
    -- Noufil 5884 06/30/2009 Caluse added for sms 
    @SetCallForSms      BIT = 0
)
AS 
	SET NOCOUNT ON ;
	IF @showall = 1
	BEGIN
	    --Yasir Kamal 5555 03/11/2009 Report_id was not correct.
	    SET @reminderdate = GETDATE() - ABS(
	            (
	                SELECT [dbo].[fn_GetReportValue] ('days', 105)
	            )
	        ) -- 105 is report_id of set call validation report.
	          --5555 end
	    SET @reminderdate = (
	            CASE 
	                 WHEN DATEPART(dw, @reminderdate) = 1 THEN @reminderdate - 2
	                 WHEN DATEPART(dw, @reminderdate) = 7 THEN @reminderdate - 1
	                 ELSE @reminderdate
	            END
	        )
	    
	    SET @reminderdate = CONVERT(VARCHAR(20), @reminderdate, 110) +  ' 00:00:00'
	END	
	
	SELECT DISTINCT 
	       t.TicketID_PK AS ticketid,
	       t.Firstname,
	       t.Lastname,
	       ISNULL(t.LanguageSpeak, 'N/A') AS LanguageSpeak,
	       t.BondFlag,
	       t.GeneralComments,
	       tv.setCallComments AS comments,
	       ISNULL(tv.setCallStatus, 0) AS STATUS,
	       CONVERT(
	           VARCHAR(20),
	           ISNULL(dbo.formatphonenumbers(T.Contact1), '')
	       ) + '(' + ISNULL(LEFT(Ct1.Description, 1), '') + ')' AS contact1,
	       CONVERT(
	           VARCHAR(20),
	           ISNULL(dbo.formatphonenumbers(T.Contact2), '')
	       ) + '(' + ISNULL(LEFT(Ct2.Description, 1), '') + ')' AS contact2,
	       CONVERT(
	           VARCHAR(20),
	           ISNULL(dbo.formatphonenumbers(T.Contact3), '')
	       ) + '(' + ISNULL(LEFT(Ct3.Description, 1), '') + ')' AS contact3,
	       T.TotalFeeCharged,
	       f.FirmAbbreviation,
	       f.Phone,
	       f.Fax,
	       Insurance = (
	           SELECT TOP 1 vv.shortdesc + '(' + ISNULL(CONVERT(VARCHAR, tvv.ticketviolationdate, 101), '')
	                  + ')'
	           FROM   tblviolations vv WITH(NOLOCK)
	                  INNER JOIN tblticketsviolations tvv WITH(NOLOCK)
	                       ON  vv.violationnumber_pk = tvv.violationnumber_pk
	           WHERE  tvv.ticketid_pk = t.ticketid_pk
	                  AND vv.shortdesc = 'ins'
	       ),
	       Child = (
	           SELECT TOP 1 vv.shortdesc
	           FROM   tblviolations vv WITH(NOLOCK)
	                  INNER JOIN tblticketsviolations tvv WITH(NOLOCK)
	                       ON  vv.violationnumber_pk = tvv.violationnumber_pk
	           WHERE  tvv.ticketid_pk = t.ticketid_pk
	                  AND vv.shortdesc = 'chld blt'
	       ),
	       CONVERT(DATETIME, CONVERT(VARCHAR, MIN(CourtDateMain), 101)) AS CourtDateMain,
	       --Ozair 4194 07/07/2009 added dialer status
	       DialerStatus = CASE 
	                           WHEN tv.AutoDialerSetCallStatus = 0 THEN 
	                                'Pending'
	                           ELSE adr.ResponseType
	                      END,
	       -- Noufil 5884 07/14/2009  courtdate,courtroom,courtshortname,SendSMSFlag1 ,SendSMSFlag2 ,SendSMSFlag3 columns added
	       -- Noufil 6523 10/06/2009 Select min future court date for SMS.
	       (
	           SELECT MIN(ttv.CourtDateMain)
	           FROM   tblTicketsViolations ttv WITH(NOLOCK)
	           WHERE  ttv.TicketID_PK = t.TicketID_PK
	                  AND DATEDIFF(DAY, GETDATE(), ttv.CourtDateMain) > 0
	       ) AS courtdate,
	       --tv.CourtDateMain AS courtdate,
	       tv.CourtNumbermain AS courtroom,
	       -- Noufil 6177 08/17/2009 Short name replace with Court name and Adress + Zip
	       --Muhammad Ali 8370 10/08/2010 add Court Address with court name via '-' seperator, for SMS service
		   CONVERT(VARCHAR,c.CourtName)+'-'+CONVERT(VARCHAR ,c.Address) AS courtshortname,
	       ISNULL(t.SendSMSFlag1, 0) AS SendSMSFlag1,
	       ISNULL(t.SendSMSFlag2, 0) AS SendSMSFlag2,
	       ISNULL(t.SendSMSFlag3, 0) AS SendSMSFlag3,
	       --Abbas Shahid Khwaja 9231 07/01/2011 include zip, address2, city,state
	       c.Zip,
	       c.Address2 AS CourtAddress2,
	       c.City,
	       ts.[State] 
	       INTO #temp4
	FROM   tblTicketsViolations TV WITH(NOLOCK) --added in order to get single ticketid          
	       
	       INNER JOIN dbo.tblTickets t WITH(NOLOCK)
	            ON  TV.TicketID_PK = t.TicketID_PK
	       INNER JOIN dbo.tblCourtViolationStatus cvs WITH(NOLOCK)
	            ON  TV.CourtViolationStatusIDMain = cvs.CourtViolationStatusID
	       INNER JOIN dbo.tblCourts c WITH(NOLOCK)
	            ON  TV.CourtID = c.Courtid
	       LEFT OUTER JOIN dbo.tblFirm f WITH(NOLOCK)
	            ON  t.FirmID = f.FirmID
	            AND ISNULL(t.firmid, 3000) = 3000
	       LEFT OUTER JOIN dbo.tblContactstype ct2 WITH(NOLOCK)
	            ON  t.ContactType2 = ct2.ContactType_PK
	       LEFT OUTER JOIN dbo.tblContactstype ct1 WITH(NOLOCK)
	            ON  t.ContactType1 = ct1.ContactType_PK
	       LEFT OUTER JOIN dbo.tblContactstype ct3 WITH(NOLOCK)
	            ON  t.ContactType3 = ct3.ContactType_PK
	       INNER JOIN tblviolations v WITH(NOLOCK)
	            ON  v.violationnumber_pk = ISNULL(tv.violationnumber_pk, 0)
	       INNER JOIN dbo.tblticketspayment TP WITH(NOLOCK)
	            ON  TV.TicketID_PK = TP.TicketID
	            --Abbas Shahid 9231 07/13/2011 added state
	       LEFT OUTER JOIN tblState ts WITH(NOLOCK) ON ts.StateID = c.[State]
	                --Ozair 4194 07/07/2009 added dialer status
	                
	       LEFT OUTER JOIN AutoDialerResponse adr WITH(NOLOCK)
	            ON  adr.ResponseID = tv.AutoDialerSetCallStatus
	WHERE  ISNULL(t.firmid, 3000) = 3000
	       AND ISNULL(t.CaseTypeId, 0) = 1
	       --Zeeshan Haider 10949 04/25/2013 Exclude Disposed violation
	       AND cvs.CategoryID <> 50
	       AND ISNULL(activeflag, 0) = 1
	       --Farrukh 10367 09/05/2012  Excluding Cases having call back Status as Contacted, SMS Reply Confirmed & AutoDialer-Confirmed
	       AND ISNULL(TV.ReminderCallStatus, 0) NOT IN (1, 7, 8)
	       AND (
	       		   --Fahad 7975 08/27/2010 Added In clause to get both Spanish and English Language
	               --((@language = -1 AND @isAD=1) AND(t.LanguageSpeak IN ('ENGLISH','SPANISH'))) 
	               -- Noufil 6523 10/06/2009 Show spanish when Spanish selected. Else for other language then SPANISH show English.
	               (@language = -1 OR (@isAD=1 AND t.LanguageSpeak IN ('ENGLISH','SPANISH')))	               
	               OR (@language = 0 AND ISNULL(t.LanguageSpeak, '') <> 'SPANISH')
	               OR (@language = 1 AND ISNULL(t.LanguageSpeak, '') = 'SPANISH')
	           )
	       AND cvs.categoryid IN (3, 4, 5)
	       AND (
	               @mode = 3
	               OR (@mode = 1 AND ISNULL(t.BondFlag, 0) = 1)
	               OR (@mode = 2 AND ISNULL(t.BondFlag, 0) = 0)
	           )
	       AND TicketsViolationID IN (SELECT ttvl.TicketviolationID
	                                  FROM   tblTicketsViolationLog ttvl
	                                  WHERE  TicketsViolationID = 
	                                         TicketViolationID
	                                         AND (
	                                                 (
	                                                     @showall = 1
	                                                     --Ozair 4194 03/13/2009 for Auto Dialer Set Call
	                                                     AND @isAD = 0
	                                                     -- Noufil 5884 06/30/2009 Caluse added for sms
	                                                     AND @SetCallForSms = 0
	                                                     -- Noufil 6523 10/06/2009 Use Setdate column 
	                                                     AND DATEDIFF(DAY, '07/31/2008', ISNULL(t.SettingDateChanged, '01/01/1900')) 
	                                                         >= 0
	                                                     AND DATEDIFF(DAY, @reminderdate, ISNULL(t.SettingDateChanged, '01/01/1900')) 
	                                                         <= 0
	                                                 )
	                                                 OR (
	                                                        @showall = 0
	                                                        --Ozair 4194 03/13/2009 for Auto Dialer Set Call
	                                                        AND @isAD = 0
	                                                            -- Noufil 5884 06/30/2009 Caluse added for sms
	                                                        AND @SetCallForSms = 0
	                                                        	                                                        
	                                                        -- Noufil 6523 10/06/2009	Use Setdate column 
	                                                        AND DATEDIFF(
	                                                                DAY,
	                                                                ISNULL(t.SettingDateChanged, '01/01/1900'),
	                                                                CONVERT(VARCHAR, @reminderdate, 101)
	                                                            ) = 0
	                                                    )
	                                                    --Ozair 4194 03/13/2009 for Auto Dialer Set Call
	                                                 OR (
	                                                        @isAD = 1
	                                                        AND @showall = 0
	                                                            -- Noufil 5884 06/30/2009 Caluse added for sms
	                                                        AND @SetCallForSms = 0
	                                                        
	                                                        -- Noufil 6523 10/06/2009 Use Setdate column 
	                                                        AND DATEDIFF(
	                                                                DAY,
	                                                                ISNULL(t.SettingDateChanged, '01/01/1900'),
	                                                                CONVERT(VARCHAR, @reminderstartdate, 101)
	                                                            ) <= 0
	                                                        -- Noufil 6523 10/06/2009 Use Setdate column 
	                                                        AND DATEDIFF(
	                                                                DAY,
	                                                                ISNULL(t.SettingDateChanged, '01/01/1900'),
	                                                                CONVERT(VARCHAR, @reminderenddate, 101)
	                                                            ) >= 0
	                                                         
	                                                         
	                                                         -- Abbas Qamar 9999    20-01-2011 Checking the future court date for Set Call status type.
	                                                         AND ( @showreport = 0 OR (@showreport = 1 AND  DATEDIFF(DAY,tv.CourtDateMain ,GETDATE()) > 0) )
	                                                          -- Rab Nawaz 10914 07/02/2013 Checking Set Call Status for Auto Dailer. . . 
	                                                        AND tv.setcallstatus NOT IN (1, 7, 8)
	                                                    )
	                                                    -- Noufil 5884 06/30/2009 Caluse added for sms
	                                                 OR (
	                                                        @SetCallForSms = 1
	                                                        -- Noufil 6523 09/04/2009 Use Setdate column 
	                                                        AND DATEDIFF(
	                                                                DAY,
	                                                                [dbo].[fn_getnextbusinessday] (ISNULL(t.SettingDateChanged, '01/01/1900'), 3),
	                                                                GETDATE()
	                                                            ) = 0
	                                                        --Ozair 7427 02/18/2010 Only Future Court Dates
	                                                        AND DATEDIFF(DAY,GETDATE(),CourtDateMain) >0
	                                                        
	                                                         -- Rab Nawaz 10914 07/02/2013 Checking Set Call Status for SMS service. . . 
	                                                        AND tv.setcallstatus NOT IN (1, 7, 8)
	                                                 )
	                                                 
	                                                 -- Rab Nawaz 10914 07/01/2013 Added for Set call status confirmed. . . 
	                                                  OR (
	                                                        @showreport = 1
	                                                        AND DATEDIFF(
	                                                                DAY,
	                                                                [dbo].[fn_getnextbusinessday] (ISNULL(t.SettingDateChanged, '01/01/1900'), 3),
	                                                                GETDATE()
	                                                            ) = 0
	                                                        AND DATEDIFF(DAY,GETDATE(),CourtDateMain) >0
	                                                        AND (tv.setcallstatus = @status OR @status = 5) -- Set Call SMS confirmed or searched by All
	                                                        AND ISNULL(t.BondFlag, 0) = 0
	                                                        AND @isAD = 0
	                                                        AND @SetCallForSms = 0
	                                                 )
	                                                 
	                                             ))
	GROUP BY
			t.TicketID_PK ,
	       t.Firstname,
	       t.Lastname,
	       t.LanguageSpeak,
	       t.BondFlag,
	       t.GeneralComments,
	       tv.setCallComments,
	       tv.setCallStatus,
	       T.Contact1,Ct1.Description,T.Contact2,Ct2.Description,
	       T.Contact3,Ct3.Description,
	       T.TotalFeeCharged,
	       f.FirmAbbreviation,
	       f.Phone,
	       f.Fax,	       
	       tv.CourtNumbermain,
	       c.CourtName,
			--Muhammad Ali 8370 10/08/2010 Add Court address in group by clause.
	       c.Address,
	       t.SendSMSFlag1,
	       t.SendSMSFlag2,
	       t.SendSMSFlag3,
	       --Abbas Shahid Khwaja 9231 07/01/2011 include zip, address2, city,state in gropuby clause.
	       c.Zip,
	       c.Address2,
	       c.City,
	       ts.[State],
	       tv.AutoDialerSetCallStatus,
	       adr.ResponseType
	                                             
	
	-- Rab Nawaz 9961 01/03/2012 cases which contains NISI in cause Number will be Excluded. .
	IF (@SetCallForSms = 1)
	BEGIN
		DELETE t 
		FROM #temp4 t INNER JOIN tblticketsviolations tv WITH(NOLOCK) ON tv.TicketID_PK = t.ticketid
		WHERE CHARINDEX('NISI', ISNULL (TV.casenumassignedbycourt, '')) > 0
	END                                             
	                                             
	
	SELECT DISTINCT 
	       ticketid,
	       firstname,
	       lastname,
	       languagespeak,
	       bondflag,
	       comments,
	       GeneralComments,
	       ISNULL(STATUS, 0) AS STATUS,
	       Contact1,
	       Contact2,
	       Contact3,
	       T.CourtDateMain,
	       totalfeecharged,
	       FirmAbbreviation,
	       phone,
	       fax,
	       Insurance,
	       Child,
	       CASE ISNULL(dbo.tblTicketsFlag.flagid, 0)
	            WHEN 5 THEN 1
	            ELSE 2
	       END AS flagid,
	       rs.Description AS callback,
	       --Ozair 4194 07/07/2009 added dialer status
	       DialerStatus
	       -- Noufil 5884 07/14/2009  courtdate,courtroom,courtshortname,SendSMSFlag1 ,SendSMSFlag2 ,SendSMSFlag3 columns added
	       ,
	       courtdate,
	       courtroom,
	       courtshortname,
	       SendSMSFlag1,
	       SendSMSFlag2,
	       SendSMSFlag3,
	       --Abbas Shahid Khwaja 9231 07/01/2011 include zip, address2, city,state in gropuby clause.
	       Zip,
	       CourtAddress2,
	       City,
	       [State]
	       INTO #temp1
	FROM   #temp4 T
	       LEFT OUTER JOIN dbo.tblTicketsFlag WITH(NOLOCK)
	            ON  T.TicketID = dbo.tblTicketsFlag.TicketID_PK
	            AND ISNULL(dbo.tblTicketsFlag.FlagID, 0) != (5)
	       LEFT OUTER JOIN tblReminderStatus rs WITH(NOLOCK)
	            ON  rs.Reminderid_pk = T.status 
	
	
	
	-- getting payment information...                                          
	SELECT T.*,
	       owedamount = t.totalfeecharged -(
	           SELECT ISNULL(SUM(ISNULL(chargeamount, 0)), 0)
	           FROM   tblticketspayment p WITH(NOLOCK)
	           WHERE  p.ticketid = t.ticketid
	                  AND p.paymenttype NOT IN (99, 100)
	                  AND p.paymentvoid <> 1
	       ) 
	       
	       INTO #temp2
	FROM   #temp1 T
	ORDER BY
	       T.CourtDateMain 
	
	--newly added to call function in order to get distinct violations                                 
	
	SELECT T.*,
	       dbo.fn_hts_get_TicketsViolations (ticketid, CourtDateMain) AS trialdesc,
	       WrongNumberFlag = (
	           SELECT COUNT(*)
	           FROM   tblticketsflag
	           WHERE  ticketid_pk = T.TicketId
	                  AND FlagID = 33
	       ),
	       dbo.fn_hts_get_TicketsViolationIds (ticketid, CourtDateMain) AS TicketViolationIds 
	       INTO #temp5
	FROM   #temp2 T 
	       -- added by tahir dt: 4/25/08 task 3878
	       -- by default, sort the records on last name and then first name.....
	ORDER BY
	       t.lastname,
	       t.firstname
	
	
	-- getting output...
	--Ozair 4194 03/13/2009 for Auto Dialer Set Call
	IF @isAD = 1
	BEGIN
	    DECLARE @result VARCHAR(MAX)
	    SET @result = ''
	    SELECT @result = @result + CONVERT(VARCHAR, ticketid) + ','
	    FROM   #temp5
	    WHERE  STATUS = @status
	    ORDER BY
	           lastname,
	           firstname
	    
	    SELECT @result AS ticketids
	END       
	ELSE    
	IF @status = 5
	    SELECT *
	    FROM   #temp5
	    ORDER BY
	           lastname,
	           firstname
	ELSE
	    SELECT *
	    FROM   #temp5
	    WHERE  STATUS = @status
	    ORDER BY
	           lastname,
	           firstname
	
	DROP TABLE #temp1 
	DROP TABLE #temp4 
	DROP TABLE #temp2 
	DROP TABLE #temp5

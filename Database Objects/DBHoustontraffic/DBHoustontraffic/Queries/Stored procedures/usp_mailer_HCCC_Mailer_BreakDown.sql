

USE TrafficTickets

/****** Object:  StoredProcedure [dbo].[usp_mailer_HCCC_Mailer_BreakDown ]    Script Date: 04/19/2012 
*		Created By	: - Rab Nawaz Khan
*		Task ID		: - 10191
* 
		Business Logic:	The procedure is used by for the Harris County Criminal Court Mailer Break Down
		
		List of Parameters:
		@Date:	Date for the Harris County Criminal Court Mailer Break Down
				
******/
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- usp_mailer_HCCC_Mailer_BreakDown '04/09/2012'
CREATE PROCEDURE usp_mailer_HCCC_Mailer_BreakDown 
(
	@Date DATETIME 
)

AS
BEGIN
	
	Create Table #summary
	(
		id int identity(1,1),
		TotalLoaderRecords int ,
		date datetime
	)


	-- Getting the Total Records uploaded in the Loader according to the Date
	Declare @LoaderRecs int
	SELECT @LoaderRecs =count(*)  FROM LoaderFilesArchive.dbo.Harris_County_Criminal_Court_DataArchive
	where datediff(day, [Insertion Date],@date) = 0


	insert into #summary
	values(@LoaderRecs, @date)


	-- Getting the Total Uplaoded Recs

	select tva.recordid, tva.hccc_dst ,tva.violationstatusid,  left(ta.zipcode,3) zip, tva.violationdescription,count(tva.ViolationNumber_PK) as ViolCount,
	convert(datetime , convert(varchar(10), ta.recloaddate  ,101)) as recloaddate,  tva.courtlocation, isnull(ta.ncoa48flag,0) as ncoa48flag,
	Flag1, donotmailflag,ta.DoNotMailUpdateDate
	into #MyTemp
	FROM dbo.tblticketsarchive ta
	inner join tblticketsviolationsarchive tva on tva.recordid = ta.recordid
	where datediff(day, ta.recloaddate ,@Date)<=0
	and datediff(day, ta.recloaddate, @Date) >= 0
	and tva.courtlocation In (Select courtid from tblcourts where courtcategorynum = 8 ) 
	group by tva.recordid, tva.hccc_dst ,tva.violationstatusid,  left(ta.zipcode,3), tva.violationdescription,
	convert(datetime , convert(varchar(10), ta.recloaddate  ,101)) ,  tva.courtlocation, isnull(ta.ncoa48flag,0) ,
	Flag1, donotmailflag,ta.DoNotMailUpdateDate


	alter table #summary
	add TotalUploadedRecords int

	declare @uploadedrecs  int
	select @uploadedrecs = count(distinct recordid) from #mytemp

	update #summary
	set TotalUploadedRecords = @uploadedrecs



	alter table #summary
	add TotalClients int,
		DoNotMaill int,
		BadAddress int,
		AddressChangedInPast48 int,
		selectedZipCode int,
		AccedentCases int,
		AlreadyPrinted int,
		TotalPrintable int,
		AlreadyClient int


	--------------------------------------------------------------------------------------------
	Declare @TotalClients int
	Select 
	 @TotalClients =  count(distinct recordid)
	from #MyTemp
	update #summary
	set TotalClients = @TotalClients
	-----------------------------------------------------------------------------------

	Declare @selectedZipCode int
	Select @selectedZipCode =  count(distinct recordid)
	from #MyTemp
	where left(zip,3) not in ('770','773','774','775')
	update #summary
	set selectedZipCode = @selectedZipCode
	delete from #MyTemp where left(zip,3) not in ('770','773','774','775')
	--------------------------------------------------------------------------------

	Declare @BadAddress int
	Select  @BadAddress =  count(distinct recordid)
	from #MyTemp
	where Flag1  not in ('Y','D','S')
	update #summary
	set BadAddress = @BadAddress
	delete from #MyTemp where Flag1  not in ('Y','D','S')

	-----------------------------------------------------------------------------------
	declare @AlreadyClient int
	select @AlreadyClient = Count(distinct tt.recordid) from tblticketsviolations v 
	inner join tbltickets t on t.ticketid_pk = v.ticketid_pk
	inner join  #MyTemp tt on tt.recordid = v.recordid
	where t.activeflag = 1 and datediff(day,t.recdate,@Date)>=0

	update #summary
	set AlreadyClient = @AlreadyClient
	delete from #MyTemp where recordid in (
	select v.recordid from tblticketsviolations v inner join tbltickets t 
	on t.ticketid_pk = v.ticketid_pk and t.activeflag = 1 and datediff(day,t.recdate,@Date)>=0 
	inner join #MyTemp a on a.recordid = v.recordid
	)

	-----------------------------------------------------------------------------------
	Declare @AddressChangedInPast48 int
	Select 
	 @AddressChangedInPast48 =  count(distinct recordid)
	from #MyTemp
	where  isnull(ncoa48flag,0) = 1
	update #summary
	set AddressChangedInPast48 = @AddressChangedInPast48
	delete from #MyTemp where isnull(ncoa48flag,0) = 1

	--------------------------------------------------------------------------------------------

	Declare @DoNotMail int
	Select 
	@DoNotMail =  count(distinct recordid)
	from #MyTemp 
	where donotmailflag=1 and  datediff(day,DoNotMailUpdateDate,@Date)>=0
	update #summary
	set DoNotMaill = @DoNotMail

	delete from #MyTemp where donotmailflag=1 and  datediff(day,DoNotMailUpdateDate,@Date)>=0
	-----------------------------------------------------------------------------------
	alter table #summary
	add ExcludingDisposition int
	Declare @ExcludingDisposition int
	Select 
	@ExcludingDisposition =  count(distinct recordid)
	from #MyTemp
	where hccc_dst in ('A','D','P')
	update #summary
	set ExcludingDisposition = @ExcludingDisposition

	delete from #MyTemp where hccc_dst in ('A','D','P')
	-----------------------------------------------------------------------------------

	Declare @AccedentCases int
	select @AccedentCases = count(distinct recordid) from #MyTemp
	where 1=1 and	 ( violcount =1  and (  violationdescription    like  ('%ACC%' ) or    violationdescription    like  ('%ACCIDENT%' ) ))
	update #summary
	set AccedentCases = @AccedentCases
	delete from #MyTemp where 1=1 and	 ( violcount =1  and (  violationdescription    like  ('%ACC%' ) or    violationdescription    like  ('%ACCIDENT%' ) ))
	---------------------------------------------------------------------------------------------

	select distinct recordid into #tempvar from tblletternotes     
	where  lettertype = 55 and datediff(day,recordloaddate,@Date) >0

	select T.recordid into #tempvarexist from #Mytemp T , #tempvar V  
	where T.recordid = V.recordid
	Declare @AlreadyPrinted int
	select @AlreadyPrinted = count(distinct recordid ) from #tempvarexist

	update #summary
	set AlreadyPrinted = @AlreadyPrinted

	delete from #Mytemp where recordid in (select recordid from #tempvarexist)
	---------------------------------------------------------------------------------------------

	Declare @PrintableCount int

	select @PrintableCount = count(distinct recordid) from #Mytemp

	update #summary
	set TotalPrintable = @PrintableCount


--	select id,TotalLoaderRecords as TotalRecordsInDataFile, Date as MailerPrintDate, TotalUploadedRecords,TotalClients ,
--		DoNotMaill ,
--		BadAddress ,
--		AddressChangedInPast48 ,
--		selectedZipCode ,
--		AccedentCases ,
--		ExcludingDisposition,
--		AlreadyPrinted ,
--		AlreadyClient,
--		TotalPrintable  
--	from #summary


CREATE TABLE #OutPut
(
	filters VARCHAR (MAX),
	TotalCount INT	
)


INSERT INTO #OutPut (filters, TotalCount)
SELECT 'Total records in Data file', TotalLoaderRecords
FROM #summary

DECLARE @calc INT
SELECT @calc = TotalClients FROM #summary

INSERT INTO #OutPut (filters, TotalCount)
SELECT 'After Getting Total number of clients From Uploaded Records', TotalClients
FROM #summary

select @calc = (@calc - AddressChangedInPast48)
from #summary

INSERT INTO #OutPut (filters, TotalCount)
SELECT 'Total Count After Excluding those Clients whom Address Changed In past 48 Months', @calc
FROM #summary


select @calc = ( @calc - ExcludingDisposition)
from #summary

INSERT INTO #OutPut (filters, TotalCount)
SELECT 'Total Count After Filtering According to the Disposition Flag', @calc
FROM #summary


select @calc = ( @calc - selectedZipCode)
from #summary

INSERT INTO #OutPut (filters, TotalCount)
SELECT 'Total clients After Filtered on Zip Codes Basis', @calc
FROM #summary


select @calc = (  @calc - AccedentCases)
from #summary

INSERT INTO #OutPut (filters, TotalCount)
SELECT 'Total Clients Afte Excluding Clients who has Accedent Violations in Violation Description', @calc 
FROM #summary

select @calc = (@calc - AlreadyPrinted)
from #summary


INSERT INTO #OutPut (filters, TotalCount)
SELECT 'Total Count After Removing clients Whom Mailer is Already Printed', @calc 
FROM #summary


select @calc = (@calc - AlreadyClient)
from #summary

INSERT INTO #OutPut (filters, TotalCount)
SELECT 'Total Count After Removing Active Clients (Currently Client)', @calc 
FROM #summary


select @calc = (@calc - DoNotMaill)
from #summary


INSERT INTO #OutPut (filters, TotalCount)
SELECT 'Total Count After Excluding Clients which are Marked as Do Not Mail Flag', @calc 
FROM #summary


select @calc = (@calc - BadAddress)
from #summary


INSERT INTO #OutPut (filters, TotalCount)
SELECT 'Total Count After Excluding Clients Who are Marked as Bad Address', @calc 
FROM #summary


INSERT INTO #OutPut (filters, TotalCount)
SELECT 'Total Printable', @calc
FROM #summary


SELECT filters AS Filters, TotalCount AS [Total Count] FROM #OutPut

	drop table #summary
	drop table #MyTemp
	drop table #tempvar
	drop table #tempvarexist
	DROP TABLE #OutPut

END

GO
	grant execute on [dbo].[usp_mailer_HCCC_Mailer_BreakDown] to dbr_webuser
go
﻿ 
/****
Created By : Muhammad Nasir     
    
Business Logic : This Procedure is used to get count of  printed or non printed Trial Letter,Missed Court,Pled Out,set call,Sol LOR letters in the batch.            

ist of Parameters:             
         
 @emp_id    : Employee Name                 
 @courtid   : Court ID                            
 @isprinted : Used to display Printed records                    
 @fromdate  : Start date     
 @todate    : End Date       
 *****/
--Task ID 5681 05/20/2009

      
--USP_HTP_Get_BatchPrint_Count 0,'05/06/2009','05/13/2009',0,'gs'    
Alter PROCEDURE dbo.USP_HTP_Get_BatchPrint_Count  
 @IsPrinted BIT,  
 @StartDate DATETIME,  
 @EndDate DATETIME ,  
 @court INT=0,  
 @EMPID VARCHAR(50)='%'  
AS  
 IF (@IsPrinted=0)  
 BEGIN  
     SELECT 'Trial Letter' AS LetterType  
           ,COUNT(DISTINCT thpl.TicketID_FK) AS Counts  
     FROM   tblHTSBatchPrintLetter thpl  
            RIGHT OUTER JOIN tbl_HTS_Batch_TrialLetter thbtl  
                 ON  thpl.BatchID_PK = thbtl.BatchID  
            LEFT OUTER JOIN dbo.tblUsers AS tblUsers_1  
                 ON  thpl.EmpID = tblUsers_1.EmployeeID   
                     -- AND thpl.TicketID_FK = dbo.tbl_hts_batch_trialletter.ticketid_pk  
     WHERE  thpl.LetterID_FK = 2   
            AND thpl.IsPrinted = 0  
            AND thpl.DeleteFlag<>1  
            AND ((@court<>0 AND thbtl.CourtID=@court) OR (@court=0))  
            AND (tblUsers_1.Abbreviation LIKE @EMPID)    
       
       
     EXEC USP_HTP_Get_BatchPrint_Count_By_LetterType 'Missed Court'  
         ,11  
         ,0  
         ,@StartDate  
         ,@EndDate  
         ,@court  
         ,@EMPID    
       
     EXEC USP_HTP_Get_BatchPrint_Count_By_LetterType 'Pled Out'  
         ,12  
         ,0  
         ,@StartDate  
         ,@EndDate  
         ,@court  
         ,@EMPID    
       
     EXEC USP_HTP_Get_BatchPrint_Count_By_LetterType 'Pled Out'  
         ,13  
         ,0  
         ,@StartDate  
         ,@EndDate  
         ,@court  
         ,@EMPID    
       
     EXEC USP_HTP_Get_BatchPrint_Count_By_LetterType 'SOL'  
         ,15  
         ,0  
         ,@StartDate  
         ,@EndDate  
         ,@court  
         ,@EMPID    
       
       
     SELECT 'LOR' AS LetterType  
           ,COUNT(DISTINCT thpl.TicketID_FK) AS Counts  
     FROM   tblHTSBatchPrintLetter thpl  
            INNER JOIN dbo.tblticketsviolations ttv  
                 ON  thpl.TicketID_FK = ttv.ticketid_pk  
            INNER JOIN tblLORBatchHistory tlh  
                 ON  thpl.BatchID_PK = tlh.BatchID_FK  
            LEFT OUTER JOIN dbo.tblUsers AS tblUsers_1  
                 ON  thpl.EmpID = tblUsers_1.EmployeeID  
     WHERE  thpl.LetterID_FK = 6   
            AND thpl.IsPrinted = 0  
            AND ISNULL(thpl.deleteflag ,0) = 0   
            AND ((@court<>0 AND ttv.CourtID=@court) OR (@court=0))  
            AND (tblUsers_1.Abbreviation LIKE @EMPID)  
 END  
 ELSE  
 BEGIN  
     SELECT 'Trial Letter' AS LetterType  
           ,COUNT(DISTINCT thpl.TicketID_FK) AS Counts  
     FROM   tblHTSBatchPrintLetter thpl  
            RIGHT OUTER JOIN dbo.tbl_hts_batch_trialletter thbtl  
                 ON  thpl.BatchID_PK = thbtl.BatchID  
            LEFT OUTER JOIN dbo.tblUsers AS tblUsers_1  
                 ON  thpl.EmpID = tblUsers_1.EmployeeID   
                     -- AND thpl.TicketID_FK = dbo.tbl_hts_batch_trialletter.ticketid_pk  
     WHERE  thpl.LetterID_FK = 2   
            AND thpl.IsPrinted = 1  
            AND thpl.DeleteFlag<>1  
            --AND thpl.PrintDate BETWEEN @StartDate AND @EndDate  
            AND (DATEDIFF (DAY,thpl.PrintDate, @StartDate)<=0 AND DATEDIFF (DAY,thpl.PrintDate, @EndDate)>=0)
            AND ((@court<>0 AND thbtl.CourtID=@court) OR (@court=0))  
            AND (tblUsers_1.Abbreviation LIKE @EMPID)    
       
       
       
     EXEC USP_HTP_Get_BatchPrint_Count_By_LetterType 'Missed Court'  
         ,11  
         ,1  
         ,@StartDate  
         ,@EndDate  
         ,@court  
         ,@EMPID    
       
     EXEC USP_HTP_Get_BatchPrint_Count_By_LetterType 'Pled Out'  
         ,12  
         ,1  
         ,@StartDate  
         ,@EndDate  
         ,@court  
         ,@EMPID    
       
     EXEC USP_HTP_Get_BatchPrint_Count_By_LetterType 'Pled Out'  
         ,13  
         ,1  
         ,@StartDate  
         ,@EndDate  
         ,@court  
         ,@EMPID    
       
     EXEC USP_HTP_Get_BatchPrint_Count_By_LetterType 'SOL'  
         ,15  
         ,1  
         ,@StartDate  
         ,@EndDate  
         ,@court  
         ,@EMPID    
       
     SELECT 'LOR' AS LetterType  
           ,COUNT(DISTINCT tlh.ID) AS Counts  
     FROM   tblLORBatchHistory tlh  
            LEFT OUTER JOIN tblHTSBatchPrintLetter thpl  
                 ON  tlh.BatchID_FK = thpl.BatchID_PK  
            LEFT OUTER JOIN dbo.tblUsers AS tblUsers_1  
                 ON  thpl.EmpID = tblUsers_1.EmployeeID  
     WHERE  thpl.IsPrinted = 1  
            AND (  
                    ISNULL(EDeliveryFlag ,0)=0  
                    --AND thpl.PrintDate BETWEEN @StartDate AND @EndDate   
                    AND (DATEDIFF (DAY,thpl.PrintDate, @StartDate)<=0 AND DATEDIFF (DAY,thpl.PrintDate, @EndDate)>=0)
                )  
                --Nasir 6873 10/31/2009 remove EDeliveryFlag 1 as not required         
            AND ((@court<>0 AND tlh.courtid=@court) OR (@court=0))  
            AND (tblUsers_1.Abbreviation LIKE @EMPID)  
 END      
 
 
 GO 
 GRANT EXECUTE ON dbo.USP_HTP_Get_BatchPrint_Count TO dbr_webuser
 Go
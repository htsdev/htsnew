﻿

/****** Object:  StoredProcedure [dbo].[USP_HTP_Update_COntractFollowUpdate]  ******/   
/*      
Created By	    : Fahad Muhammad Qureshi.
Created Date	: 01/04/2009
TasK			: 5098      
Business Logic  : This procedure Update information.   
				     
Parameter:     
   @TicketID	 : Updating criterea with respect to TicketId    
   @FollowUpDate : Date of follow Up which has to be set   
   
    
*/    

CREATE PROCEDURE [dbo].[USP_HTP_Update_COntractFollowUpdate]
	@FollowUpDate DATETIME,
	@TicketID INT
AS
	UPDATE tblTickets
	SET    ContractFollowUpDate = @FollowUpDate
	WHERE  TicketID_PK = @TicketID
GO
 
	GRANT EXECUTE ON [dbo].[USP_HTP_Update_COntractFollowUpdate] TO dbr_webuser
	
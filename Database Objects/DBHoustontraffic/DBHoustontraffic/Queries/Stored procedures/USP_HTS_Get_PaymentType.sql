/*        
Altered By     : Fahad Muhammad Qureshi.
Created Date   : 07/27/2009  
TasK		   : 6054        
Business Logic : This procedure is used to fetching out all Payment Types according to Credit Category.    
*/

Alter procedure [dbo].[USP_HTS_Get_PaymentType]        
AS

SELECT * FROM tblPaymenttype tp WHERE tp.Paymenttype_PK = 0 AND tp.IsActive=1 -- Abbas Qamar 9957 25/01/2012 checking IsActive flag
UNION ALL
SELECT * FROM tblPaymenttype tp WHERE tp.Paymenttype_PK = 10 AND tp.IsActive=1 -- Abbas Qamar 9957 25/01/2012 checking IsActive flag
UNION ALL
SELECT * FROM tblPaymenttype tp WHERE  tp.Paymenttype_PK NOT IN(0,9,11,104,6,10,98,99,100) -- Fahad 6054 08/20/2009 including partner firm credit payment type
AND tp.IsActive=1 -- Abbas Qamar 9957 25/01/2012 checking IsActive flag

UNION ALL
SELECT * FROM tblPaymenttype tp WHERE tp.Paymenttype_PK = 10 AND tp.IsActive=1 -- Abbas Qamar 9957 25/01/2012 checking IsActive flag
UNION ALL
SELECT * FROM tblPaymenttype tp WHERE tp.Paymenttype_PK IN(9,11,104) -- Fahad 6054 08/20/2009 including partner firm credit payment type
AND tp.IsActive=1 -- Abbas Qamar 9957 25/01/2012 checking IsActive flag


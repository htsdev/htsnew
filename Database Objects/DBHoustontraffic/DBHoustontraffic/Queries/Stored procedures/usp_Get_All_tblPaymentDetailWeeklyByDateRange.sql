USE TrafficTickets
GO
/***
* Business Logic: This procedure is used to get the weekly payment detail based on the spacified transactio date range.
***/

ALTER PROCEDURE [dbo].[usp_Get_All_tblPaymentDetailWeeklyByDateRange] 

@TransDate datetime,
@TransDateTo DATETIME,
@branchID INT =0 -- Sabir Khan 10920 05/27/2013 Branch id added.

AS

Select EmployeeID, TransDate, ActualCash, ActualCheck, Notes
From  tblPaymentDetailWeekly
Where 
	
/*( Convert(varchar(12),TransDate,101) >= Convert(varchar(12), @TransDate,101)
	   AND Convert(varchar(12),TransDate,101) <= Convert(varchar(12), @TransDateTo,101))  */
 ( TransDate  Between  @TransDate AND @TransDateTo )
AND Branchid IN (SELECT branchid FROM fn_get_branchIDs(@branchID)) -- Sabir Khan 10920 05/27/2013 Branch Id check added.
go





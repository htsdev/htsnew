SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_UpdateCourtStatusCategoryID]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_UpdateCourtStatusCategoryID]
GO


CREATE procedure USP_HTS_UpdateCourtStatusCategoryID --2,'6,'
@Typeid int,
@CourtStatusID as varchar(200)

as

update tblcourtviolationstatus 
set Categoryid = @Typeid
where courtviolationstatusid in (select * from dbo.Sap_String_Param(@CourtStatusID))



--select * from tblcourtviolationstatus


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


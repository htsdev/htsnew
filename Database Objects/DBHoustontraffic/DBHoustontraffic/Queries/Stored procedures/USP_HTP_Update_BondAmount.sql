/**
* Created By : Noufil Khan 5807 05/30/2009
* Business Logic : This procedure update Bond amount for the given ticket id of client.
* Parameter :	@ticketid_pk INT,
				@casenumber varchar(50),
				@bondamount MONEY
**/


alter procedure [dbo].[USP_HTP_Update_BondAmount]
@ticketid_pk INT,
@casenumber varchar(50),
@bondamount MONEY,
--Yasir Kamal 7058 07/12/2009 update violation description.
@ViolationDesp nvarchar(4000) 
AS

DECLARE @ViolNum INT 

SELECT @ViolNum = ViolationNumber_PK  FROM tblticketsviolations WHERE violationdescription like '%@ViolationDesp%'

UPDATE tblTicketsViolations
SET
BondAmount = @bondamount,
ViolationNumber_PK = @ViolNum

WHERE TicketID_PK = @ticketid_pk and rtrim(ltrim(casenumassignedbycourt)) = @casenumber
USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[usp_Mailer_Send_HMC_Arraignment_New]    Script Date: 09/05/2011 23:17:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/****** 
Business Logic:	The procedure is used by LMS application to get data for HMC Arrigment letter
				and to create LMS history for this letter for the selected date range.
				
List of Parameters:
	@catnum:		Category number of the selected letter type, if printing for 
					all courts of the selected court category
	@LetterType:	Selected letter type in LMS
	@CrtId:			Court id of the selected court category if printing for individual court hosue.
	@startListdate:	Comma separated dates selected to print letter.
	@EmpId:			Employee information who is printing the letter.
	@PrintType:		Flag to just preview the letter or to mark letters as sent. If 1 then LMS letter history will be created.
	@SearchType:	Flag that identifies if searching by court date or by list date. 0= list date, 1 = court date
	@isPrinted:		Flag to include already printed letters in the batch.

List of Columns:	
	LetterId:		Letter ID generated for each letter. It will be "NOT PRINTABLE"	if @PrintType = 0
	RecordId:		Identity value to look up the case in non-clients. used to group the records in report file.
	FirstName:		Person's first name that will appear on the letter
	LastName:		Person's last name that will appear on the letter
	Address:		Person's Home address that will appear on the letter
	City:			Person's Home City that will appear on the letter
	State:			Person's Home State that will appear on the letter
	FineAmount:		Fine amount for each violation that will appear on the letter
	ViolationDescritpion: violation description that will appear on the letter
	Court Name:		Name of court with which the violation is associated. Used in report file
	DPC:			Address status that is used in barcode font on the letter
	DPTwo:			Address status that is used in barcode font on the letter
	TicketNumber:	Case number for the violation
	ZipCode:		Person's Home Zip code.
	MidNumber:		MidNumber of the person associated with the case.
	CourtId:		Court identification associated with the violation.
	ZipMid:			First five characters of zip code and midnumber for grouping of data in report file.
	CourtDate:		Court date associated with the violation.
	Abb:			Short abbreviation of employee who is printing the letter

*******/

-- usp_Mailer_Send_HMC_Arraignment_New 1, 40, 1, '1/1/2010,' , 3991,  0 ,0 , 0,0
ALTER    procedure [dbo].[usp_Mailer_Send_HMC_Arraignment_New]
@catnum int=1,                                    
@LetterType int=9,                                    
@crtid int=1, 
@startListdate varchar (500) ,                                    
@empid int=2006,                              
@printtype int =0 ,
@searchtype int = 0 ,
@isprinted bit   ,
@language int = 0                         
                                    
                                    
as
declare @officernum varchar(50),                                    
@officeropr varchar(50),                                    
@tikcetnumberopr varchar(50),                                    
@ticket_no varchar(50),                                                                    
@zipcode varchar(50),                                             
@zipcodeopr varchar(50),                                    
                                    
@fineamount money,                                            
@fineamountOpr varchar(50) ,                                            
@fineamountRelop varchar(50),                   
              
@singleviolation money,                                            
@singlevoilationOpr varchar(50) ,                                            
@singleviolationrelop varchar(50),                                    
                                    
@doubleviolation money,                                            
@doublevoilationOpr varchar(50) ,                                            
@doubleviolationrelop varchar(50),                                  
@count_Letters int,                  
@violadesc varchar(500),                  
@violaop varchar(50)                                     
                                    
declare @sqlquery varchar(8000),
	@sqlquery2 varchar(8000)
                                            
Select @officernum=officernum,                                    
@officeropr=oficernumOpr,                                    
@tikcetnumberopr=tikcetnumberopr,                                    
@ticket_no=ticket_no,                                                                      
@zipcode=zipcode,                                    
@zipcodeopr=zipcodeLikeopr,                                    
@singleviolation =singleviolation,                                    
@singlevoilationOpr =singlevoilationOpr,                                    
@singleviolationrelop =singleviolationrelop,                                    
@doubleviolation = doubleviolation,                                    
@doublevoilationOpr=doubleviolationOpr,                                            
@doubleviolationrelop=doubleviolationrelop,                  
@violadesc=violationdescription,                  
@violaop=violationdescriptionOpr ,              
@fineamount=fineamount ,                                            
@fineamountOpr=fineamountOpr ,                                            
@fineamountRelop=fineamountRelop              
                                 
from tblmailer_letters_to_sendfilters                                    
where courtcategorynum=@catnum                                    
and Lettertype=@LetterType                                  

Select @sqlquery =''  , @sqlquery2 = ''
  
declare @user varchar(10)
select @user = upper(abbreviation) from tblusers where employeeid = @empid
                                  
set @sqlquery=@sqlquery+'                                    
Select	distinct ta.recordid, 
	convert(varchar(10),ta.listdate,101) as listdate,
	count(tva.ViolationNumber_PK) as ViolCount,
	isnull(sum(isnull(tva.fineamount,0)),0) as Total_Fineamount
into #temptable                                          
FROM    dbo.tblTicketsViolationsArchive tva 
INNER JOIN
	dbo.tblTicketsArchive ta 
ON 	tva.RecordID = ta.RecordID 
INNER JOIN
	dbo.tblCourtViolationStatus tcvs 
ON 	tva.violationstatusid = tcvs.CourtViolationStatusID 
where  tcvs.CategoryID=2   
and 	Flag1 in (''Y'',''D'',''S'')
and 	donotmailflag = 0 
and 	ta.clientflag=0
--Muhammad Muneer 8465 10/28/2010 added the new functionality that is the null checks for the first name and the last name
and LEN(ISNULL(RTRIM(LTRIM(ta.lastname)),''''))<>0
and LEN(ISNULL(RTRIM(LTRIM(ta.firstname)),''''))<>0  
and isnull(ta.ncoa48flag,0) = 0




--Rab Nawaz Khan 9119 04/04/2011 added attorney association logic.
and len(isnull(tva.attorneyname, '''')+isnull(tva.barcardnumber,'''')) = 0 
and CHARINDEX(''KUBOSH'',isnull(tva.BondingCompanyName,''''))=0

'               
          
if(@crtid=1 )              
	set @sqlquery=@sqlquery+' 
and 	tva.courtlocation In (Select courtid from tblcourts where courtcategorynum =  '+ convert(varchar(10),@crtid) +' )'                  


if(@crtid <> 1  )              
	set @sqlquery =@sqlquery +'                                  
and 	tva.courtlocation =  '+ convert(varchar(10),@crtid)
                                  
                          
                     

set @sqlquery=@sqlquery+'
and   ' + dbo.Get_String_Concat_With_DatePart_ver2 ( ''+  @startlistdate + '', 'ta.listdate' ) 


                                    
if(@officernum<>'')                                    
begin                                    
set @sqlquery =@sqlquery +                                     
            '  
and 	ta.officerNumber_Fk '+@officeropr +'('+ @officernum+')'                                     
end   

if(@fineamount<>0 and @fineamountOpr<>'not'  )              
begin              
set @sqlquery =@sqlquery+ '     '+                                    
      '  and tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                   
end                                             

if(@fineamount<>0 and @fineamountOpr = 'not'  )              
begin              
set @sqlquery =@sqlquery+ '     '+                                    
      '  and not tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                   
end                                             
              
              
                                    
if(@ticket_no<>'')                                    
set @sqlquery=@sqlquery+ '
and  	('+ dbo.Get_String_Concat_With_op(''+@ticket_no+'' , 'tva.TicketNumber_PK' ,+ ''+ @tikcetnumberopr)+')'                                          
                                  
                                    
if(@zipcode<>'')                                                          
set @sqlquery=@sqlquery+ ' 
and	('+  dbo.Get_String_Concat_With_op(''+@zipcode+'' , 'left(ta.zipcode,3)' ,+ ''+ @zipcodeopr) +')'             
                                    
set @sqlquery =@sqlquery+'      
group by  
	ta.recordid, convert(varchar(10),ta.listdate,101)
having 1 = 1 '              


if(@singleviolation<>0 and @doubleviolation=0)                                
	set @sqlquery =@sqlquery+ '
	and (  '+ @singlevoilationOpr+  '  (isnull(sum(isnull(tva.fineamount,0)),0) '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and count(tva.ViolationNumber_PK)=1))
      '                                
if(@doubleviolation<>0 and @singleviolation=0 )                                
	set @sqlquery =@sqlquery + '
	and ('+ @doublevoilationOpr+  '   (isnull(sum(isnull(tva.fineamount,0)),0) '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ')and count(tva.ViolationNumber_PK)=2) )
      '                                                    
                                
if(@doubleviolation<>0 and @singleviolation<>0)                                
	set @sqlquery =@sqlquery+ '
	and ('+ @singlevoilationOpr+  '  (isnull(sum(isnull(tva.fineamount,0)),0) '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and count(tva.ViolationNumber_PK)=1))                                
    and ('+ @doublevoilationOpr+  '  (isnull(sum(isnull(tva.fineamount,0)),0) '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ') and count(tva.ViolationNumber_PK)=2))
        '                                  

set @sqlquery =@sqlquery+ ' 
delete from #temptable where recordid in (
		select n.recordid from tblletternotes n inner join tblticketsarchive t on t.recordid =  n.recordid
		where lettertype =  '+ convert(varchar(10), @LetterType)  +' 
		) '                                          


set @sqlquery=@sqlquery+'                                    
Select	distinct ta.recordid, 
	convert(varchar(10),ta.listdate,101) as listdate,
	tva.courtdate,
	ta.courtid, 
	flag1 = isnull(ta.Flag1,''N'') ,
	ta.officerNumber_Fk,                                          
	tva.TicketNumber_PK,
	ta.donotmailflag,
	ta.clientflag,
	upper(firstname) as firstname,
	upper(lastname) as lastname,
	upper(address1) + '''' + isnull(ta.address2,'''') as address,                      
	upper(ta.city) as city,
	S.state as state,
	dbo.tblCourts.CourtName,
	zipcode,
	midnumber,
	dp2 as dptwo,
	dpc,                
	violationdate,
	violationstatusid,
	left(zipcode,5) + rtrim(ltrim(midnumber)) as zipmid,
	ViolationDescription, 
	case when isnull(tva.fineamount,0)=0 then 100 else tva.fineamount end as FineAmount,                                    
	a.ViolCount,
	a.Total_Fineamount
into #temp                                          
FROM    dbo.tblTicketsViolationsArchive tva 
INNER JOIN
	dbo.tblTicketsArchive ta 
ON 	tva.RecordID = ta.RecordID 
INNER JOIN
tblstate S ON
ta.stateid_fk = S.stateID
inner join #temptable a on a.recordid = ta.recordid
inner join dbo.tblCourts on dbo.tblCourts.courtid = tva.courtlocation

-- Abbas Qamar 9991 10-01-2012 join for Dispose Category Id
INNER JOIN	dbo.tblCourtViolationStatus tcvs ON tva.violationstatusid = tcvs.CourtViolationStatusID 

where  1=1 

--Rab Nawaz Khan 9119 04/04/2011 added attorney association logic.
and len(isnull(tva.attorneyname, '''')+isnull(tva.barcardnumber,'''')) = 0 
and CHARINDEX(''KUBOSH'',isnull(tva.BondingCompanyName,''''))=0

-- Abbas Qamar 9991 10-01-2012 Disposed cases should be excluded.
and tcvs.CategoryID <> 50   
'               
 

              

-- VIOLATION DESCRIPTION FILTER.....
if(@violadesc <> '')
	BEGIN
		-- NOT LIKE FILTER.......
		if(charindex('not',@violaop)<> 0 )              
			BEGIN
				set @sqlquery=@sqlquery+'
				-- Rab Nawaz Khan 9659 09/06/2011 Filter Setting has been changed   
				and	 not (('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ 'like')+'))'           			
			END
		-- LIKE FILTER.......
		--if(charindex('not',@violaop) =  0 )              
		ELSE
			BEGIN
				set @sqlquery=@sqlquery+'
				-- Rab Nawaz Khan 9659 09/06/2011 Filter Setting has been changed   
				and	 (  ('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ 'like')+'))'           				
			END
	END


set @sqlquery=@sqlquery+'

Select distinct recordid, violationstatusid,ViolCount,Total_Fineamount, courtdate, listdate as mdate, FirstName,LastName,address,                              
 city,state,FineAmount,violationdescription,CourtName,dpc,dptwo,Flag1,officerNumber_Fk,TicketNumber_PK,                                     
 courtid,zipmid,donotmailflag,clientflag, zipcode,
 dbo.fn_Mailer_Get_IsConflictingCourtdate(recordid) as dateconflict
 into #temp1  from #temp                            
 '              
                           
              
if( @printtype<>0)  
begin       
set @sqlquery2 =@sqlquery2 + '
	Declare @ListdateVal DateTime, @totalrecs int, @count int, @p_EachLetter money, @recordid varchar(50),
		@zipcode   varchar(12), @maxbatch int  ,@lCourtId int  ,@dptwo varchar(10),@dpc varchar(10)
	declare @tempBatchIDs table (batchid int)
	Select @totalrecs =count(*) from #temp1 where mdate = @ListdateVal                                 
	Select @count=Count(distinct recordid) from #temp1                                
	if @count>=500                                
		set @p_EachLetter=convert(money,0.3)                                
	if @count<500                                
		set @p_EachLetter=convert(money,0.39)                                
	Declare ListdateCur Cursor for                                  
	Select distinct mdate  from #temp1                                
	open ListdateCur                                   
	Fetch Next from ListdateCur into @ListdateVal                                                      
	while (@@Fetch_Status=0)                              
		begin                                
			Select @totalrecs =count(distinct recordid) from #temp1 where mdate = @ListdateVal                                 
			insert into tblBatchLetter(Empid, Lettertype,Listdate,CourtId,ParentID,LCount,PExpense) values                                  
			( '+ convert(varchar(10),@empid) +'  ,  '+ convert(varchar(10),@lettertype) +'  , @ListdateVal,  '+ convert(varchar(10),@catnum) +', 0, @totalrecs, @p_EachLetter)                                   
			Select @maxbatch=Max(BatchId) from tblBatchLetter
			insert into @tempBatchIDs select @maxbatch                                                           
				Declare RecordidCur Cursor for                               
				Select distinct recordid,zipcode, courtid, dptwo, dpc from #temp1 where datediff(day,  mdate , @ListdateVal ) = 0                              
				open RecordidCur                                                         
				Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc                                          
				while(@@Fetch_Status=0)                              
				begin                              
					insert into tblLetterNotes(BatchId_Fk,listdate,ZipCode,RecordID,PCost,LetterType, courtid, dp2, dpc)                              
					values (@maxbatch,@ListdateVal,@zipcode,@recordid,@p_EachLetter,'+ convert(varchar(10),@lettertype) +' ,@lCourtId, @dptwo, @dpc)                              
					Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc                          
				end                              
				close RecordidCur 
				deallocate RecordidCur                                                               
			Fetch Next from ListdateCur into @ListdateVal                              
		end                                            
	close ListdateCur  deallocate ListdateCur '

	 set @sqlquery2 =@sqlquery2 + '
	 Select distinct convert(varchar(20),n.noteid) as letterid, t.recordid,  t.FirstName,                              
	 t.LastName,t.address,t.city, t.state,FineAmount,violationdescription,CourtName, t.dpc,                              
	 dptwo, TicketNumber_PK, t.zipcode, '''+@user+''' as Abb , courtdate , dateconflict, 0 as language, convert(varchar,t.recordid) + ''0'' as chkgroup                                    
	 from #temp1 t inner join tblletternotes n
	 on t.recordid = n.recordid and t.courtid = n.courtid and n.lettertype = '+ convert(varchar(10),@lettertype) +' 
	 --order by T.zipcode '
if @language = 1 
	 set @sqlquery2 =@sqlquery2 + '
	 union
	 Select distinct convert(varchar(20),n.noteid) as letterid, t.recordid,  t.FirstName,                              
	 t.LastName,t.address,t.city, t.state,FineAmount,violationdescription,CourtName, t.dpc,                              
	 dptwo, TicketNumber_PK, t.zipcode, '''+@user+''' as Abb , courtdate ,dateconflict, 1 as language, convert(varchar,t.recordid) + ''1'' as chkgroup                                    
	 from #temp1 t inner join tblletternotes n
	 on t.recordid = n.recordid and t.courtid = n.courtid and n.lettertype = '+ convert(varchar(10),@lettertype) +' 
	 --order by T.zipcode '


set @sqlquery2 =@sqlquery2 + '
declare @lettercount int, @subject varchar(200), @body varchar(400) , @sql varchar(500)
select @lettercount = count(distinct recordid) from #temp1
select @subject = convert(varchar(20), @lettercount) + '' LMS HMC ARR New Letters Printed''
select @body = convert(varchar(20), @lettercount) + '' LETTERS ('' + CONVERt(VARCHAR(12), GETDATE()) + '') - ''+ upper( '''+@user+''')
exec usp_mailer_send_Email @subject, @body

declare @batchIDs_filname varchar(500)
set @batchIDs_filname = ''''
select @batchIDs_filname = @batchIDs_filname + convert(varchar,batchid) + '',''  from @tempBatchIDs
select isnull(@batchIDs_filname,'''') as batchid

'
end
else
begin
	set @sqlquery2 = @sqlquery2 + '
	 Select distinct ''NOT PRINTABLE'' as letterId,  recordid,  FirstName,                              
	 LastName,address,city, state,FineAmount,violationdescription,CourtName, dpc,                              	 dptwo, TicketNumber_PK, zipcode, '''+@user+''' as Abb , courtdate ,dateconflict, 0 as language, convert(varchar,recordid) + ''0'' as chkgroup                                   
	 from #temp1 
--	 order by zipcode 
 '
	if @language = 1 
	set @sqlquery2 = @sqlquery2 + '
	 union
	 Select distinct ''NOT PRINTABLE'' as letterId,  recordid,  FirstName,                              
	 LastName,address,city, state,FineAmount,violationdescription,CourtName, dpc,                              	 dptwo, TicketNumber_PK, zipcode, '''+@user+''' as Abb , courtdate ,dateconflict, 1 as language, convert(varchar,recordid) + ''1'' as chkgroup                                   
	 from #temp1 
--	 order by zipcode 
 '
end   

set @sqlquery2 = @sqlquery2 + '

drop table #temp 
drop table #temp1'

--print @sqlquery + @sqlquery2
exec (@sqlquery + @sqlquery2)



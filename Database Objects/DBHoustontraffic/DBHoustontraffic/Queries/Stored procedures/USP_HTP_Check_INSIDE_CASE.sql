
Go

CREATE procedure USP_HTP_Check_INSIDE_CASE 
@ticketid int  
as  
Select Count(*)  from tblticketsviolations TV   
-- Abbas Qamar 10088  05-12-2012 HMW-W court Id added 3075
Where TV.CourtID In (3001,3002,3003,3075)    
and @ticketid = TV.TicketID_pk  


GO


GRANT EXEC ON USP_HTP_Check_INSIDE_CASE TO dbr_webuser

GO



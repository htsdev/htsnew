set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


/******************

CREATED BY : SARIM GHANI

BUSINESS LOGIC : This procedure is used to migrate criminal case from Civil Database to Traffic Ticket Database

LIST OF INPUT PARAMETERS :

  @vc_TicketNumber : Case Number          
  @employeeid	   : Sales Rep ID	
  @mailerID		   : Mailer ID Associated With The Case               
  @sOutPut		   : Out Parameter Containing Ticket ID and Active Flag
  @IsTrafficClient: pass one for traffic client and zero for non traffic


List of Columns:

  IsTrafficAlertSignup:  Represents Client Hire from TrafficAlert or not.

Modification Detail :
--------------------

Migrate Arrest Date From criminalcases table [Civil] to TblticketsViolations table [TrafficTickets] ArrestDate Column.

*******************/
ALTER procedure [dbo].[USP_HTS_Insert_CaseInfoFromJIMS_Ver_2]            
 (              
  @vc_TicketNumber varchar(20),                  
  @employeeid   int = 3992 ,          
  @mailerID int = 0,   
  @IsTrafficClient int,           
  @sOutPut varchar(20) ='' output            
 )              
              
as              
            
declare @PlanId int,            
 @RecCount int,            
 @iFlag int ,            
 @TicketId int,            
 @ErrorNumber int,            
 @BookingNumber varchar(30)            
            
            
-- CHECK IF TICKET NUMBER ALREADY EXISTS IN ANY CUSTOMER PROFILE.....            
SELECT  @RecCount = t.ticketid_pk,             
 @iFlag  = ( case t.activeflag when 0 then 1 when 1 then 0 else 2  end)            
FROM   tblTicketsViolations v             
inner join            
 tbltickets t            
on  t.ticketid_pk = v.ticketid_pk            
and v.refcasenumber = @vc_TicketNumber            
            
select  @iFlag = isnull(@iFlag,2),            
 @RecCount = isnull(@RecCount,0)            
            
            
-- CHECKING EXISTANCE OF THE RECORD IN TBLTICKETS......................                 
-- IF NOT FOUND, INSERT THE RECORD IN TBLTICKETS............                
if @reccount <> 0             
            
 update jims.dbo.criminalcases set clientflag = 1 where casenumber = @vc_TicketNumber            
            
else            
 begin                  
            
 -- get the booking number of the case....            
 select @bookingnumber = bookingnumber from jims.dbo.criminalcases             
 where clientflag =  0 and casenumber = @vc_TicketNumber            
             
 -- DECLARING TEMP TABLE..........                
 declare @temp table                
  (                
  [FirstName] [varchar] (20) ,                
  [LastName] [varchar] (20) ,                
  [MiddleName] [varchar] (6) ,                
  [Address] [varchar] (100)  ,                
  [City] [varchar] (50)  ,                
  [State] [varchar] (3)  ,                
  [Zip] [varchar] (10) ,                
  [BookingNumber] [varchar] (20)  ,                
  [Span] [varchar] (20) ,                
  [BookingDate] [datetime] NULL ,                
  [Bookingtime] [varchar] (6)  ,                
  [BookingDeputy] [varchar] (6)  ,                
  [ReleaseDate] [datetime] NULL ,                
  [Releasetime] [varchar] (4) ,                
  [Releasedeputy] [varchar] (6)  ,                
  [reas] [varchar] (5)  ,                
  [casenumber] [varchar] (20) ,                
  [offnumber] [varchar] (5)  ,                
  [bondamount] [money] NULL ,                
  [ccr] [varchar] (4)  ,                
  [JPI] [varchar] (3) ,                
  [JPINumber] [varchar] (6)  ,                
  [arrestdate] [datetime] NULL ,                
  [chargewording] [varchar] (100)  ,                
  [dob] [datetime] NULL ,                
  [race] [varchar] (15) ,                
  [sex] [varchar] (10) ,                
  [status] [varchar] (30)  ,                
  [chargelevel] [varchar] (10),        
  [dp2] [varchar](10),        
  [dpc] [varchar](10),        
  [Flag1] [varchar](2),    
  [CDI] [varchar](3)        
  )                
                   
 -- DECLARING TEMP TABLE..........                
 declare @temp1 table                
  (                
  [FirstName] [varchar] (20) ,                
  [LastName] [varchar] (20) ,                
  [MiddleName] [varchar] (6) ,                
  [Address] [varchar] (100)  ,                
  [City] [varchar] (50)  ,                
  [State] [varchar] (3)  ,                
  [Zip] [varchar] (10) ,                
  [BookingNumber] [varchar] (20)  ,                
  [Span] [varchar] (20) ,                
  [BookingDate] [datetime] NULL ,                
  [Bookingtime] [varchar] (6)  ,                
  [BookingDeputy] [varchar] (6)  ,                
  [ReleaseDate] [datetime] NULL ,                
  [Releasetime] [varchar] (4) ,                
  [Releasedeputy] [varchar] (6)  ,                
  [reas] [varchar] (5)  ,                
  [casenumber] [varchar] (20) ,                
  [offnumber] [varchar] (5)  ,                
  [bondamount] [money] NULL ,                
  [ccr] [varchar] (4)  ,                
  [JPI] [varchar] (3) ,                
  [JPINumber] [varchar] (6)  ,                
  [arrestdate] [datetime] NULL ,                
  [chargewording] [varchar] (100)  ,                
  [dob] [datetime] NULL ,                
  [race] [varchar] (15) ,                
  [sex] [varchar] (10) ,                
  [status] [varchar] (30)  ,                
  [chargelevel] [varchar] (10) ,                
  [dp2] [varchar](10),        
  [dpc] [varchar](10),        
  [Flag1] [varchar](2),    
  [CDI] [varchar](3),             
  [ViolationNumber_pk] [int] --Haris 9709 09/29/2011 datatype changed to int from tinyint
  )                
                 
                 
 --- FETCHING RECORD TO INSERT IN TEMPORARY TABLE...........                
 insert into  @temp                  
 select  distinct                 
  left(C.FirstName,20),            
  left(C.LastName,20),            
  left(C.MiddleName,6),            
  C.Address,                
  C.City,                
  C.State,                
  C.Zip,                
  C.BookingNumber,                
  L.Span,                
  C.BookingDate,                  
  C.Bookingtime,                
  C.BookingDeputy,                
  C.ReleaseDate,                
  C.Releasetime,                
  C.Releasedeputy,                
  C.reas,                
  C.casenumber,                  
  C.offnumber,                
  C.bondamount,                
  C.ccr,                
  C.JPI,                
  C.JPINumber,                
  arrestdate,                
  chargewording,                
  dob,                  
  race =  case race                  
    when 'W' then 'White'                  
    when 'B' then 'Black'                  
    when 'H' then 'Hispanic'                  
    when 'A' then 'Asian'                  
    else 'Other'                  
   end,                  
  sex =   case sex                  
    when 'M' then 'Male'                  
    when 'F' then 'Female'                  
   end,                
  chargecode + '-' + disposition as status,                
  chargelevel ,                 
  --Added By Zeeshan Ahmed For Barcode Modifications        
   C.dp2,C.dpc,C.flag1 , Left(L.CaseNumber,3)                 
 from jims.dbo.criminalcases C left outer join  jims.dbo.Criminal503 L on C.bookingnumber = L.bookingnumber                 
 where  c.bookingnumber = @bookingnumber            
 and disposition not  in ('COMM','DADJ','DISP','DERR','PROB')            
 and    c.clientflag = 0            
               
            
    -- NOW INSERT THE RECORD IN TBLTICKETS FROM TEMPORARY TABLE...............                
begin tran             
  insert into tbltickets                
   (                
   midnum,                
   firstname,                
   lastname,                
   MiddleName,                
   Address1,                
   Address2,                  
   City,                
   Stateid_FK,                
   Zip,                
   DOB,                
   DLNumber,                
   employeeid,                
   Race,                
   Gender,                
   Height,                
   employeeidupdate  ,            
   MailerID,        
   dp2,        
   dpc,        
   flag1,
   IsTrafficAlertSignUp,
   CaseTypeId            
)                  
                    
   select  distinct                 
    Span,                
    Upper(FirstName),            
    Upper(LastName),            
    Upper(MiddleName),            
    Upper(Address),                
    ' ',                 
    Upper(City),                
    45,                
    Zip,                
    dob,                
    '00000000',                
   @employeeid,                
    Upper(race),                
    sex,                
    '0',                
   @employeeid,            
   @mailerID,        
   dp2,        
   dpc,        
   flag1,
   @IsTrafficClient,
   2             
   from  @temp                  
                    
 if @@error = 0               
  begin             
	--ozair Task 4767 09/11/2008 replace @@Identity with Scope_Identity 
	--as @@Identity returns the last inserted identity value instead of the insert statement executed in current scope
	--select @TicketID=@@identity 
	select @TicketID=SCOPE_IDENTITY() 	
    set @ErrorNumber =0            
  end            
 else            
  begin            
   set @ErrorNumber = @@error            
  end            
             
  if @TicketID <> 0            
   begin                
            
     -- FIRST INSERTING VIOLATIONS IN TBLVIOLATIONS FROM TEMPORARY TABLE..................                
     -- IF IT DOESN'T EXISTS IN TBLVIOLATIONS.....            
   insert into dbo.tblviolations                  
    (                
    description,                
    sequenceorder,                
    IndexKey,                
    IndexKeys,                
    ChargeAmount,                
    BondAmount,                  
    Violationtype,                
    ChargeonYesflag,                 
    ShortDesc,                
    ViolationCode,                
    AdditionalPrice  
                 
    )                  
    select  chargewording,                
     null,                
     2,                
     100,                
     100,                
     140,                
     9,                
     1,                
     'None',                
     'none',                
     0                 
    from  @temp                   
    where  chargewording not in (                  
      select description from dbo.tblviolations where violationtype = (            
   select violationtype from tblcourtviolationtype            
   where courtid = 3037            
   )            
     )                  
            
  if @@error <> 0             
   set @errornumber = @@error                     
                  
  -- NOW GETTING VIOLATION DESCRIPTION INTO TEMPORARY TABLE..............                
   insert into  @temp1                 
   select t1.*,                
    v.violationnumber_pk            
   from @temp t1 inner join tblviolations v            
   on t1.chargewording = v.description            
   and violationtype = (            
   select violationtype from tblcourtviolationtype            
   where courtid = 3037            
   )            
            
            
  -- GETTING PRICE PLAN ..................            
  select  @PlanId =             
   (            
   select  top 1 planid             
   from  tblpriceplans             
   where courtid = 3037            
   and  effectivedate <= getdate()            
   and enddate > = getdate()            
   and isActivePlan = 1            
   order by planid            
   )            
             
            
             
     -- INSERTING VIOLATION DETAILS IN TBLTICKETSVIOLATIONS................                  
     insert into tblticketsviolations                
      (                
      TicketID_PK,                
      ViolationNumber_PK,                
      FineAmount,                
      casenumassignedbycourt,                  
      refcasenumber ,       
      bondamount,                
      violationdescription,            
      courtid,            
      PlanId ,            
      recordid ,    
      cdi,  
	  CourtDateMain ,
      ArrestDate,
	  CourtViolationStatusIdmain	             
      )                  
      SELECT  distinct                   
       @TicketID,                
       violationNumber_pk,                
       100,                
       casenumber,                
       '0',                
       140,                
       status ,            
       3037,             
      isnull(@PlanId     ,45),            
      bookingnumber ,    
	  cdi,           
      isnull(BookingDate,'1/1/1900')  ,isnull(ArrestDate,'1/1/1900')
      ,
      --Zeeshan Ahmed 4847 09/24/2008 Set Defauld Pre Trial Status For Criminal Case
      101
      from  @temp1                    
            
            
            
     
    -- Agha Usman 4347 07/22/2008  - Problem Client Detection
	declare @Firstname varchar(20)                                         
	declare @Lastname varchar(20)                                         
	declare @MiddleName  varchar(6)                                          
	declare @DOB datetime
	
	
	select @FirstName = FirstName, @LastName = LastName , @MiddleName = MiddleName, @DOB = DOB from @temp

	exec dbo.USP_HTP_ProblemClient_Manager  @TicketId,@FirstName,@LastName,@MiddleName,@DOB,NULL       
            
  if @@error <> 0             
   set @errornumber = @@error   
                           
     -- UPDATING CLIENT STATUS IN JIMS.............                
     update c                  
     set  c.clientflag = 1                  
     from  @temp1 t2,                 
      jims.dbo.criminalcases C                  
     where  t2.casenumber = C.casenumber                  
              
            
  if @@error <> 0             
   set @errornumber = @@error                     
            
                       
     -- CREATING HISTORY FOR THE CASE...................                
     insert into tblticketsnotes(ticketid,subject,employeeid) values(@TicketID,'INITIAL INQUIRY HARRIS CO : ARR :',@employeeid)                  
              
  if @@error <> 0              set @errornumber = @@error                     
              
     end                
            
  if @Errornumber = 0            
   commit tran            
  else            
   rollback tran            
end                  
            
            
if isnull(@Ticketid ,0) = 0             
 select @Ticketid = @RecCount            
            
-- UPDATE MAILER ID IN TBLTICKETS......            
EXEC dbo.USP_HTS_Update_Mailer_id @TicketID, @employeeid, 3            
            
select @sOutPut = convert(varchar(100),@ticketid) + ','+  convert(varchar(3),@iFlag)            
  



SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Get_BondFlagConfirmedFlag]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Get_BondFlagConfirmedFlag]
GO




Create procedure USP_HTS_Get_BondFlagConfirmedFlag

@ticketid_pk int 

as 


select * from tblticketsflag where ticketid_pk = @ticketid_pk and flagid = 30



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


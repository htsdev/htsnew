

  
  
  
-- USP_HTS_Get_Comments 1110,9  
--grant execute on dbo.USP_HTS_Get_Comments to dbr_webuser  
  
ALTER procedure [dbo].[USP_HTS_Get_Comments]  
@TicketID as int,  
@CommentID as int  
As  
SET NOCOUNT ON;
Declare @SQLQry as varchar(max)  
  
Set @SQLQry = 'Select '  
  
If @CommentID = 1 -- GeneralComments  
 Begin  
  Set @SQLQry = @SQLQry + 'isnull(GeneralComments,'''') As Comments From TblTickets WITH(NOLOCK) Where TicketID_PK = ' + convert(varchar(20),@Ticketid)  
 End  
Else IF @CommentID = 2 -- SettingComments  
 Begin  
  Set @SQLQry = @SQLQry + 'isnull(SettingComments,'''') As Comments From TblTickets WITH(NOLOCK) Where TicketID_PK = ' + convert(varchar(20),@Ticketid)  
 End  
Else If @CommentID = 3 -- ReminderComments  
 Begin  
  Set @SQLQry = @SQLQry + 'Top 1 isnull(ReminderComments,'''') As Comments From TblTicketsViolations WITH(NOLOCK) Where TicketID_PK = ' + convert(varchar(20),@Ticketid)  
 End  
Else If @CommentID = 4 -- TrialComments  
 Begin  
  Set @SQLQry = @SQLQry + 'isnull(TrialComments,'''') As Comments From TblTickets WITH(NOLOCK) Where TicketID_PK = ' + convert(varchar(20),@Ticketid)  
 End  
Else If @CommentID = 5 -- PreTrialComments  
 Begin  
  Set @SQLQry = @SQLQry + 'isnull(PreTrialComments,'''') As Comments From TblTickets WITH(NOLOCK) Where TicketID_PK = ' + convert(varchar(20),@Ticketid)  
 End  
Else If @CommentID = 6 -- ContinuanceComments  
 Begin  
  Set @SQLQry = @SQLQry + 'isnull(ContinuanceComments,'''') As Comments From TblTickets WITH(NOLOCK) Where TicketID_PK = ' + convert(varchar(20),@Ticketid)  
 End  
Else If @CommentID = 7 -- ConsultationComments  
 Begin  
  Set @SQLQry = @SQLQry + 'isnull(Comments,'''') As Comments From tbl_hts_consultationcomments WITH(NOLOCK) Where TicketID_FK = ' + convert(varchar(20),@Ticketid)  
 End  

-- Noufil 4172 06/13/2008 getting setcall comments

Else If @CommentID = 9 -- Setcall Comments
 Begin  
  Set @SQLQry = @SQLQry + 'isnull(setCallComments,'''') as Comments  From tblticketsviolations WITH(NOLOCK) Where TicketID_PK = ' + convert(varchar(20),@Ticketid)  
 End  
-- Zahoor 3997 09/18/08
Else If @CommentID = 10 -- FTA call Comments
 Begin  
  Set @SQLQry = @SQLQry + 'isnull(ftacallcomments,'''') as Comments  From tblticketsviolations WITH(NOLOCK) Where TicketID_PK = ' + convert(varchar(20),@Ticketid)  
 End  

Else  
 Begin  
  Set @SQLQry = @SQLQry + '''Invalid comment ID'' As Comments From TblTickets WITH(NOLOCK)'  
 End  
  
--Select @SQLQry  
Exec (@SQLQry)  
  
  


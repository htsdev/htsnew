﻿ /*
* Created By:			Saeed.
* Creation Date:		07/20/2010.
* Task ID:				8038.
* Business Logic:		The procedure is used to get BondFollowUpDate for the ticket passing as an input parameter.
* List of Parameters:	
*			@Ticketid:	This field is the unique identifier from tblTickets table.
* USP_HTS_Get_BondFollowupDate 316171
* */

CREATE PROCEDURE [dbo].[USP_HTS_Get_BondFollowupDate]
@Ticketid INT
as
DECLARE @PrevBondFollowupDate VARCHAR(20)
SET @PrevBondFollowupDate='1/1/1900'
SELECT @PrevBondFollowupDate=CONVERT(VARCHAR(20),ISNULL(BondFollowUpDate,'1/1/1900'),101)  FROM tblticketsextensions WHERE Ticketid_pk=@Ticketid 
SELECT @PrevBondFollowupDate AS BondFollowupDate
GO
GRANT EXECUTE ON dbo.USP_HTS_Get_BondFollowupDate TO dbr_webuser
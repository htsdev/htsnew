 /************************************************************
* Created By:  Syed Muhammad Ozair 5769 04/13/2009  
* Business Logic:	Non Client Potential Contact ID Association Job
*					Updateing Potential Contact IDs in Archive (tblticketsarchive)
*					where Client flag <> 1 i.e record not yet moved to tbltickets    
*					Also
*					Updateing Potential Contact IDs in Civil (ChildCustodyParties)
*					where Client flag <> 1 i.e record not yet moved to tbltickets
*					Also it sends a summary email
*					
* List of Parameters:	N/A
* 
* List of Columns:   N/A
 ************************************************************/

CREATE PROCEDURE dbo.usp_HTP_NonClient_ContactID_Association

AS

/************************************************************
 * Code formatted by SoftTree SQL Assistant � v4.0.34
 * Time: 4/22/2009 6:30:24 PM
 ************************************************************/

DECLARE @DistinctUpdateCount INT
DECLARE @TotalUpdateCount INT

SET @DistinctUpdateCount = 0
SET @TotalUpdateCount = 0

/************************************************************
 * Non Client Potential Contact ID Association Job
 * Updateing Potential Contact IDs in Archive (tblticketsarchive)
 * where Client flag <> 1 i.e record not yet moved to tbltickets
 ************************************************************/
 
SELECT ROW_NUMBER() OVER(ORDER BY tta.RecordID) AS SNo,
       tta.RecordID,
       c.ContactID
       INTO #temp1
FROM   tblTicketsArchive tta,
       Contact c
WHERE  (
           (
               tta.LastName = c.LastName
               AND tta.DOB = c.DOB
               AND LEFT(LTRIM(RTRIM(tta.Firstname)), 1) = LEFT(LTRIM(RTRIM(c.Firstname)), 1)
               AND tta.MidNumber = c.MidNumber
           )
           OR (
                  tta.LastName = c.LastName
                  AND tta.DOB = c.DOB
                  AND LEFT(LTRIM(RTRIM(tta.Firstname)), 1) = LEFT(LTRIM(RTRIM(c.Firstname)), 1)
                  AND tta.ZipCode = c.Zip
              )
       )
       AND tta.PotentialContactID_FK IS NULL
       AND tta.Clientflag <> 1
       AND DATEDIFF(DAY, tta.RecLoadDate, GETDATE()) = 0
ORDER BY
       tta.RecordID
       

DECLARE @Row_Count INT
SELECT @Row_Count = COUNT(SNo)
FROM   #temp1       

DECLARE @ContactID INT
DECLARE @RecordID INT


WHILE (@Row_Count > 0)
BEGIN
    --Setting @ContactID and @TicketID
    SELECT @ContactID = ContactID,
           @RecordID = RecordID
    FROM   #temp1
    WHERE  SNo = @Row_Count
    
    --Updating Contact ID in tbltickets
    UPDATE tblTicketsArchive
    SET    PotentialContactID_FK = @ContactID
    WHERE  RecordID = @RecordID
    
    --Decresing row count for loop
    SET @Row_Count = @Row_Count -1
END       



/************************************************************
 * Updateing Potential Contact IDs in Civil (ChildCustodyParties)
 * where Client flag <> 1 i.e record not yet moved to tbltickets
 ************************************************************/

SELECT ROW_NUMBER() OVER(ORDER BY ccp.ChildCustodyPartyId) AS SNo,
       ccp.ChildCustodyPartyId,
       c.ContactID
       INTO #temp2
FROM   Civil.dbo.ChildCustody cc
       JOIN Civil.dbo.ChildCustodySettingType ccst
            ON  cc.ChildCustodySettingTypeId = ccst.ChildCustodySettingTypeId
       LEFT OUTER JOIN Civil.dbo.ChildCustodyParties ccp
            ON  cc.ChildCustodyId = ccp.ChildCustodyID
       INNER JOIN Civil.dbo.ChildCustodyConnection ccc
            ON  ccc.ChildCustodyConnectionid = ccp.ChildCustodyConnectionid,
       Contact c
WHERE  (
           ccp.LastName = c.LastName
           AND ccp.FirstName = c.FirstName
           AND LEFT(LTRIM(RTRIM(ccp.ZipCode)), 5) = LEFT(LTRIM(RTRIM(c.Zip)), 5)
       )
       AND cc.ClientFlag <> 1
       AND DATEDIFF(DAY, cc.RecLoadDate, GETDATE()) = 0
ORDER BY
       ccp.ChildCustodyPartyId
       
SET @Row_Count = 0
SELECT @Row_Count = COUNT(SNo)
FROM   #temp2       

SET @ContactID = 0
DECLARE @ChildCustodyPartyId INT


WHILE (@Row_Count > 0)
BEGIN
    --Setting @ContactID and @TicketID
    SELECT @ContactID = ContactID,
           @ChildCustodyPartyId = ChildCustodyPartyId
    FROM   #temp2
    WHERE  SNo = @Row_Count
    
    --Updating Contact ID in tbltickets
    UPDATE Civil.dbo.ChildCustodyParties
    SET    PotentialContactID_FK = @ContactID
    WHERE  ChildCustodyPartyId = @ChildCustodyPartyId
    
    --Decresing row count for loop
    SET @Row_Count = @Row_Count -1
END       

--Setting total updated count
--getting count for Traffic and Criminal Cases from tblticketsarchive
SELECT @TotalUpdateCount = @TotalUpdateCount + COUNT(ContactID)
FROM   #temp1
--getting count for Family Law Cases from Civil
SELECT @TotalUpdateCount = @TotalUpdateCount + COUNT(ContactID)
FROM   #temp2


--getting count for Traffic and Criminal Cases from tblticketsarchive
SELECT @DistinctUpdateCount = @DistinctUpdateCount + COUNT(DISTINCT ContactID)
FROM   #temp1
--getting count for Family Law Cases from Civil
SELECT @DistinctUpdateCount = @DistinctUpdateCount + COUNT(DISTINCT ContactID)
FROM   #temp2


--Dropping table #temp1 and #temp2
DROP TABLE #temp1
DROP TABLE #temp2

--Sending Summary Email
DECLARE @Subject VARCHAR(100)
DECLARE @Body VARCHAR(MAX)

SET @Subject = 'PCID Update loader - ' + CONVERT(VARCHAR, GETDATE(), 101)

SET @Body = '<br>'
SET @Body = @Body + 'Distinct Non Clients PCIDs updated:' + CONVERT(VARCHAR, @DistinctUpdateCount)
SET @Body = @Body + '<br>'
SET @Body = @Body + 'Total Number of Matters updated:' + CONVERT(VARCHAR, @TotalUpdateCount)



DECLARE @EmailAddress VARCHAR(60)

SET @EmailAddress = ''
    
IF (@EmailAddress = '')
BEGIN
    DECLARE @ServerIP VARCHAR(100)    
    CREATE TABLE #tempIP
    (
    	Results VARCHAR(1000) NULL
    ) 
    BULK 
    INSERT #tempIP 
           FROM 'C:\Program Files\Microsoft SQL Server\SQLServerIP.txt'
    SELECT @ServerIP = results
    FROM   #tempIP 
    
    DROP TABLE #tempIP    
    
    IF @ServerIP = '66.195.101.51'
        --Live Settings        
        SELECT @EmailAddress = 'dataloaders@sullolaw.com'
    ELSE 
    IF @ServerIP = '192.168.168.220'
        --Developer Settings    
        SELECT @EmailAddress = 'ozair@lntechnologies.com'
    ELSE
        -- QA Settings    
        SELECT @EmailAddress = 'qa@lntechnologies.com'
END     
    
EXEC msdb.dbo.sp_send_dbmail 
     @profile_name = 'Data Loader',
     @recipients = @EmailAddress,
     @subject = @Subject,
     @body = @Body,
     @body_format = 'HTML';
    

GO


GRANT EXECUTE ON dbo.usp_HTP_NonClient_ContactID_Association TO dbr_webuser
GO
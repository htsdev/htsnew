﻿ USE TrafficTickets
GO

/****** 
Create by		: Farrukh Iftikhar
Created Date	: 10/08/2012
TasK			: 10437

Business Logic : 
			This procedure is used to get all MDL Cases
								
******/

CREATE PROCEDURE [dbo].[USP_HTP_Get_MDLCase]
@isActive BIT = 0
AS
BEGIN

IF @isActive = 0
BEGIN
	SELECT * FROM tblMDLCases
END
ELSE
	BEGIN
		SELECT * FROM tblMDLCases
		WHERE isActive = @isActive
	END    
    
END



GO
GRANT EXECUTE ON [dbo].[USP_HTP_Get_MDLCase] TO dbr_webuser



/****** 
Created by:		Tahir Ahmed
Business Logic:	The procedure is used by LMS application to generate the CASS file for the letters
				printed during selected date range...
				
List of Parameters:
	@PrintDate: list of comma separated dates for letter printing....		
	@Court:		list of comma separated court IDs
	@LetterId:	list of comma separated letter IDs
	
	
List of Columns:	
	clientname: person's first and last name
	address:	person's hoem address
	city:		person's home city
	state:		person's home state
	zipcode:	person's home zip code
	zip3:		first three characters of peron's home zip code
	courtid:	court id of the case related to the person
	recdate:	letter printing date
	lettertype:	mailer type. consists of court category name (court name in case of HCJP) & letter name

******/

-- USP_Mailer_Get_DataSetForTextFile '7/3/2008,', '3037,', '26,'
alter procedure [dbo].[USP_Mailer_Get_DataSetForTextFile]

(
@printdates  varchar(8000),  
@court  varchar(2000),  
@letterid  varchar(2000)  

)

as

declare @tblCourt table(courtid int)  
insert into @tblcourt select distinct * from dbo.sap_string_param(@court)   
 
declare @lettertype  table(lettertype int)  
insert into @lettertype select distinct * from dbo.sap_string_param(@letterid) 

declare @tbldates  table(printdate datetime)
insert into @tbldates select distinct * from dbo.sap_dates(@printdates) 


-- tahir ahmed 4002 05/09/2008
-- replaced the memory table by temporary table...
create table #letters (
	noteid int,
	zipcode varchar(20),
	courtid int,
	recordloaddate datetime,
	recordid int,
	categoryid int,
	--Yasir Kamal 7218 01/13/2010 lms structure changed. 
	locationid_fk INT,
	lettertype int,
	--courtlettername varchar(50)
	courtlettername varchar(100) -- tahir 7085 12/03/2009 fixed the letter name length issue..
	)

-- getting letters in temporary tables.....
insert into #letters (noteid, zipcode,courtid, recordloaddate, recordid, categoryid,locationid_fk,lettertype )
select	distinct n.noteid, n.zipcode, n.courtid, n.recordloaddate, n.recordid, b.courtid,tcc.LocationId_FK, n.lettertype
from	tblletternotes n
inner join
		tblbatchletter b
on		b.batchid = n.batchid_fk
inner join @lettertype l on l.lettertype = n.lettertype 
inner join @tbldates d on datediff(day, d.printdate, n.recordloaddate)=0
inner join @tblcourt c on c.courtid = case when c.courtid <  3000 then b.courtid else n.courtid END
INNER JOIN tblCourtCategories tcc ON tcc.CourtCategorynum = b.courtid


-- updating letter name ....
update l
-- tahir 4193 06/05/2008
-- fixed incorrect number of records bug in CASS file....
--set l.courtlettername = case when l.courtid = 2 then c.courtname else cc.courtcategoryname end  + '-'+ a.lettername
set l.courtlettername = case when l.categoryid = 2 then c.courtname else cc.courtcategoryname end  + '-'+ a.lettername
-- 4193
from #letters l, tblletter a , tblcourts c, tblcourtcategories cc
where l.lettertype = a.letterid_pk
and l.courtid = c.courtid
and l.categoryid = cc.courtcategorynum

-- tahir 6741 10/15/2009 fixed Dallas mailers issue..
-- updating letter name ....
update l
set l.courtlettername = case when l.categoryid = 2 then c.courtname else cc.courtcategoryname end  + '-'+ a.lettername
from #letters l, tblletter a , DallasTrafficTickets.dbo.tblcourts c, tblcourtcategories cc
where l.lettertype = a.letterid_pk
and l.courtid = c.courtid
and l.categoryid = cc.courtcategorynum
and l.courtlettername is null

create table #file (
	clientname varchar(100), 
	address varchar(200), 
	city varchar(50), 
	state varchar(20), 
	zipcode varchar(20), 
	courtid int, 
	recdate datetime, 
	noteid int,
	lettername varchar(100)
	)

-- GETTING ADDRESS RECORDS FROM TRAFFIC TICKETS DATABASE.....
-- FOR THE ABOVE SELECTED LETTERS
insert into #file (clientname, address, city, state, zipcode, courtid, recdate, noteid, lettername)
SELECT DISTINCT 
	t.firstname+' '+t.lastname,
	t.address1+' '+isnull(t.address2,'') ,
	t.city,
	--'tx',
	s.state,
	n.zipcode,
	n.courtid,
	n.recordloaddate,
	n.noteid,
	n.courtlettername
from	tblticketsarchive t, #letters n, tblstate s
where	n.recordid = t.recordid
and		t.stateid_fk = s.stateid
-- tahir 4358 07/04/2008  adding HCC Criminal new letter..
--Sabir Khan 5726 03/30/2009 Fort Worth Municipal Court has been excluded from houston category. (27) is for Fort Worth Municipal Court.
--and		n.categoryid not in (6,11,16,25,27,28)
AND n.locationid_fk <> 2
--and		n.lettertype <> 26

-- TAHIR 4091 05/22/2008
-- GETTING ADDRESS RECORDS FROM CIVIL DATABASE.....
-- FOR THE ABOVE SELECTED LETTERS
insert into #file (clientname, address, city, state, zipcode, courtid, recdate, noteid, lettername)
SELECT DISTINCT 
	t.firstname+' '+t.lastname,
	t.address ,
	t.city,
	s.state,
	n.zipcode,
	n.courtid,
	n.recordloaddate,
	n.noteid,
	n.courtlettername
from	civil.dbo.childcustodyparties t, #letters n, civil.dbo.tblstate s
where	n.recordid = t.childcustodypartyid
and		t.stateid = s.stateid
and		n.categoryid = 26

-- GETTING ADDRESS RECORDS FROM DALLAS DATABASE.....
-- FOR THE ABOVE SELECTED LETTERS
insert into #file (clientname, address, city, state, zipcode, courtid, recdate, noteid, lettername)
SELECT DISTINCT 
	t.firstname+' '+t.lastname,
	t.address1+' '+isnull(t.address2,'') ,
	t.city,
	s.state,
	n.zipcode,
	n.courtid,
	n.recordloaddate,
	n.noteid,
	n.courtlettername
from	dallastraffictickets.dbo.tblticketsarchive t, #letters n, dallastraffictickets.dbo.tblstate s
where	n.recordid = t.recordid
and		t.stateid_fk = s.stateid
--Sabir Khan 5726 03/30/2009 Fort Worth Municipal Court has been included in Dallas Category. (27) is for Fort Worth Municipal Court.
--and		n.categoryid in ( 6,11,16,25,27,28)
AND n.locationid_fk = 2

-- GETTING ADDRESS RECORDS FROM JIMS DATABASE.....
-- FOR THE ABOVE SELECTED LETTERS
-- tahir 6741 10/15/09 no more using JIMS database for harris county criminal records....
/*
insert into #file (clientname, address, city, state, zipcode, courtid, recdate, noteid, lettername)
SELECT DISTINCT 
	replace(t.name,',',''),
	t.address,
	t.city,
	t.state,
	n.zipcode,
	n.courtid,
	n.recordloaddate,
	n.noteid,
	n.courtlettername
from	JIMS.dbo.criminalcases t, #letters n
where	n.recordid = t.bookingnumber
-- tahir 4358 07/04/2008  adding HCC Criminal new letter..
and		n.lettertype = 26
and		n.categoryid = 8
*/

declare @result table (
	clientname varchar(100), 
	address varchar(200), 
	city varchar(50), 
	state varchar(20), 
	zipcode varchar(20), 
	zip3 char(3),
	courtid int, 
	recdate varchar(20), 
	noteid int,
	lettertype varchar(100),
	rowid int identity(1,1)
	)


insert into @result (clientname, address, city, state, zipcode, zip3, courtid, recdate, noteid,lettertype)
select clientname, address, city, state, zipcode, left(zipcode,3), courtid, 
	dbo.formatdateandtimeintoshortdateandtime(recdate) as recdate , noteid, lettername as lettertype
from #file order by noteid

-- tahir 7043 11/23/2009 cass report bug fixed.
select clientname, address, city, state, zipcode, noteid as malerid, zip3, courtid, recdate, lettertype from @result --order by rowid
order by lettertype, convert(int,zip3), zipcode

drop table #letters
drop table #file

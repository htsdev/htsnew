SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Get_LetterOfRef_Batch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Get_LetterOfRef_Batch]
GO


CREATE procedure USP_HTS_Get_LetterOfRef_Batch --3992,6,'117260,'                                  
                     
 @empid int,                    
-- @chkVal int ,                    
 @letterType int,       
-- @printed int  ,                 
-- @filedate datetime = '01/01/1900',                
 @ticketidlist varchar(400) ='0'                
  as                   
                 
                    
-- CREATING TEMP TABLE TO STORE THE TICKET IDs                    
declare @tickets table (                    
 ticketid int                    
 )     
--Displaying records sorted when printed    
declare @ticketsort table    
(    
 ticketsort int,    
        batchid int     
)    
                   
                    
-- INSERTING TICKET IDs AS                     
/*  
if @chkVal = 1                    
begin       
 if @printed=2        --if not printed           
  begin      
   insert into @tickets                    
   SELECT    distinct TicketID_FK                    
   FROM         dbo.tblHTSBatchPrintLetter                    
   WHERE     (LetterID_FK = @letterType)AND (IsPrinted = 0) AND (deleteflag<> 1)                      
   insert into @ticketsort     
   select  t.ticketid,hts.batchid_pk from @tickets t inner join tblhtsbatchprintletter hts    
  on t.ticketid=hts.ticketid_fk and hts.batchid_pk=(  select top 1 tbv.batchID_pk from tblhtsbatchprintletter tbv                                       
     where  tbv.ticketid_fk=hts.ticketid_fk and tbv.LetterID_Fk = @letterType AND (IsPrinted = 0)  and deleteflag<>1   )    
 --   where hts.LetterID_Fk = @letterType and deleteflag<>1 AND (IsPrinted = 0)    
 end      
 else      
  begin      
   insert into @tickets                    
   SELECT    distinct TicketID_FK                    
   FROM         dbo.tblHTSBatchPrintLetter                    
   WHERE     (LetterID_FK = @letterType) AND (deleteflag<> 1)       
   insert into @ticketsort     
   select  t.ticketid,hts.batchid_pk from @tickets t inner join tblhtsbatchprintletter hts    
  on t.ticketid=hts.ticketid_fk and hts.batchid_pk=(  select top 1 tbv.batchID_pk from tblhtsbatchprintletter tbv                                       
     where  tbv.ticketid_fk=hts.ticketid_fk and tbv.LetterID_Fk = @letterType  and deleteflag<>1   )    
--    where hts.LetterID_Fk = @letterType and deleteflag<>1                 
       
  end      
end                     
                    
if @chkVal = 2                    
begin          
 if @printed=2                   
  begin                
   insert into @tickets                    
   SELECT    distinct TicketID_FK                    
   FROM         dbo.tblHTSBatchPrintLetter                    
   WHERE     (LetterID_FK = @letterType)                     
   AND (IsPrinted = 0)  AND (deleteflag<> 1)                      
   AND (datediff(day,BatchDate, @filedate) = 0)      
   insert into @ticketsort     
   select  t.ticketid,hts.batchid_pk from @tickets t inner join tblhtsbatchprintletter hts    
  on t.ticketid=hts.ticketid_fk and hts.batchid_pk=(  select top 1 tbv.batchID_pk from tblhtsbatchprintletter tbv                                       
   where  tbv.ticketid_fk=hts.ticketid_fk and tbv.LetterID_Fk = @letterType AND (IsPrinted = 0)  and deleteflag<>1 AND (datediff(day,BatchDate, @filedate) = 0)    )    
 --   where hts.LetterID_Fk = @letterType and deleteflag<>1 AND (IsPrinted = 0) AND (datediff(day,BatchDate, @filedate) = 0)                    
  end      
 else      
  begin                
   insert into @tickets                    
   SELECT    distinct TicketID_FK                    
   FROM         dbo.tblHTSBatchPrintLetter                    
   WHERE     (LetterID_FK = @letterType)                     
   AND (deleteflag <> 1)                     
   AND (datediff(day,BatchDate, @filedate) = 0)      
  insert into @ticketsort     
   select  t.ticketid,hts.batchid_pk from @tickets t inner join tblhtsbatchprintletter hts    
  on t.ticketid=hts.ticketid_fk and hts.batchid_pk=(  select top 1 tbv.batchID_pk from tblhtsbatchprintletter tbv                                       
   where  tbv.ticketid_fk=hts.ticketid_fk and tbv.LetterID_Fk = @letterType   and deleteflag<>1 AND (datediff(day,BatchDate, @filedate) = 0)    )    
   -- where hts.LetterID_Fk = @letterType and deleteflag<>1 AND (datediff(day,BatchDate, @filedate) = 0)                    
  end                 
          
end                     
                    
if @chkVal = 3    
*/                  
begin                    
 insert into @tickets                    
 select * from dbo.Sap_String_Param(@ticketidlist)    
   insert into @ticketsort     
   select  t.ticketid,hts.batchid_pk from @tickets t inner join tblhtsbatchprintletter hts    
  on t.ticketid=hts.ticketid_fk and hts.batchid_pk=(  select top 1 tbv.batchID_pk from tblhtsbatchprintletter tbv                                       
     where  tbv.ticketid_fk=hts.ticketid_fk and tbv.LetterID_Fk = @letterType and deleteflag<>1   )    
  --  where hts.LetterID_Fk = @letterType and deleteflag<>1     
                           
end                      
--------------------------------------------------------------------------    
--select * from @ticketsort    
--------------------------------------------------------------------------                
                
                
select distinct upper(firstname) as firstname,upper(lastname) as lastname, dbo.formatdate(dob) as dob,dlnumber,           
isnull(casenumassignedbycourt,'N/A') as casenumassignedbycourt,refcasenumber,languagespeak                         --added on 03/29/2006    zee        
, dbo.formatdateandtime_with_at_sign(V.courtdatemain) as currentdateset,  upper(firstname + ' ' + lastname) as fullname,          
'ATTN: ' + upper(C.courtcontact) as attn ,upper(C.judgename) as judgename ,            
'TEL: ' + dbo.formatphonenumbers(convert(varchar(20),C.phone)) as phone,upper(C.courtname) as courtname,           
upper(C.address)+ ' ' + upper(C.address2) as address ,upper(C.city) + ', ' + upper(E.state) + ' ' + C.zip as cityzip ,           
'FAX: ' + dbo.formatphonenumbers(convert(varchar(20),C.fax)) as FAXNO  ,upper(S.description) as description,              
UPPER(firmname) as firstname,UPPER(F.address) as firmaddress, UPPER(F.address2) as firmaddress2,UPPER(F.city) as firmcity,'TX' as firmstate,          
UPPER(F.zip) as firmzip,'TEL: ' + dbo.formatphonenumbers(convert(varchar(20),F.phone)) as firmphone,             
'FAX: ' + dbo.formatphonenumbers(convert(varchar(20),F.fax)) as firmfax,upper(firstname + ' ' + lastname),           
condition1flag ,Attorneyname ,firmsubtitle,(          
SELECT Top 1 dbo.tblCourtSettingRequest.Description          
FROM         dbo.tblCourts INNER JOIN          
              dbo.tblCourtSettingRequest ON dbo.tblCourts.SettingRequestType = dbo.tblCourtSettingRequest.SettingType INNER JOIN          
              dbo.tblTicketsViolations ON dbo.tblCourts.Courtid = dbo.tblTicketsViolations.CourtID          
WHERE     (dbo.tblTicketsViolations.TicketID_PK = t.TicketID_PK)) as courtDesc ,(           
          
SELECT     MAX(dbo.tblTicketsViolations.CourtDateMain) AS Expr1          
FROM         dbo.tblTicketsViolations INNER JOIN          
                      dbo.tblCourtViolationStatus ON dbo.tblTicketsViolations.CourtViolationStatusID = dbo.tblCourtViolationStatus.CourtViolationStatusID          
WHERE     (dbo.tblCourtViolationStatus.CategoryID IN (3, 4, 5)) --AND (dbo.tblTicketsViolations.CourtID = T.currentcourtloc)          
HAVING      (MAX(dbo.tblTicketsViolations.CourtDateMain) > GETDATE() + 21)) as NewCourtDate   ,ts.batchid       
from tbltickets T,tblcourts C,tblticketsviolations V,tblcourtviolationstatus U,          
tblcourtsettingrequest S, tblfirm F,tblstate E ,@ticketsort  ts           
where V.courtid = C.courtid          
and T.ticketid_pk = V.ticketid_pk          
and C.settingrequesttype = S.settingtype          
and c.state = E.stateid          
and T.firmid = F.firmid          
and V.courtviolationstatusidmain = U.courtviolationstatusid          
and activeflag = 1          
and categoryid in (1,2,11,12,0)          
--and c.courttype = 1              
and settingrequesttype not in (0)              
and V.courtid not in (3001,3002)      --zee        
and V.courtviolationstatusid<>80 --newly added so no refcasenumber having 'Disposed' can come  zee                  
and t.ticketid_pk=ts.ticketsort                          
order by batchid                             
--order by isnull(casenumassignedbycourt,'N/A') desc                                            
if @@error = 0 and @@rowcount > 0               
begin             
           
 update tblticketsViolations          
 set courtviolationstatusidmain = 104,          
 courtviolationstatusid = 104,          
 courtviolationstatusidscan = 104          
  from tbltickets T, tblticketsViolations V, tblcourtviolationstatus S          
 where T.ticketid_pk = V.ticketid_pk          
 and V.courtviolationstatusidmain = S.courtviolationstatusid          
 and categoryid in (1,2,11,12,0)          
 and V.courtid not in (3004,3003) --Exclude Pasadena Court    zee      
and T.ticketid_pk  in (                      
  select distinct ticketid from @tickets                      
  )                
 update tbltickets set jpfaxdate = getdate() where ticketid_pk  in (                      
  select distinct ticketid from @tickets                      
  )                 
   
 return 0              
end            
          
            
else              
 return @@error  
                
   
               
                
                
                
                
    
                
                
              
            
          
          
        
      
    
    
  



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


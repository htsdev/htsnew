set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


/******************  
  
CREATED BY : Sarim Ghani  
  
BUSINESS LOGIC : This procedure is used to migrate Archive cases from Traffic Alert  
  
List of Parameters:
---------------------

@IsTrafficClient: pass one for traffic client and zero for non traffic

  
List of Columns:
-----------------

IsTrafficAlertSignUp:	  Reprensts that client hire from TrafficAlert page 

*******************/  
  
--USP_HELPTEXT   USP_HTS_Insert_CaseInfoFromArchive_Ver_2  
    
    
    
--   USP_HTS_Insert_CaseInfoFromArchive_Ver_2  '078193561','03255215','3902 W LITTLE YORK RD APT 1702','77091-1420'    
    
ALTER procedure [dbo].[USP_HTS_Insert_CaseInfoFromArchive_Ver_2]    
  (      
  @TicketNumber varchar(20),      
  @midnumber varchar(20),     
  @address  varchar(50)='',      
  @ZipCode varchar(12) ='',    
  @EmpId  int= 3992, 
  @IsTrafficClient int,   
  @sOutPut varchar(20) ='' output    
  )      
as      
      
set nocount on      
    
declare @Officernumber  int,      
  @courtid  int,      
  @courtdate datetime,    
  @violationdate  datetime,      
  @MailDate datetime,    
  @PlanId int,    
  @RecCount int,    
  @listdate datetime,    
  @TicketId int ,    
  @iFlag int  ,    
  @RecordId int,    
  @ErrorNumber int,    
  @FTALinkId int,    
  @CauseNumber varchar(30)    
    
    
-- CREATING TEMP TABLES..................................    
declare  @tblTicketsArchive table (  [TicketNumber] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,    
 [MidNumber] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,    
 [FirstName] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,    
 [LastName] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,    
 [Initial] [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,    
 [Address1] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,    
 [Address2] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,    
 [City] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,    
 [StateID_FK] [int] NULL ,    
 [ZipCode] [varchar](12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,    
 [PhoneNumber] [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,    
 [DOB] [datetime] NULL,    
 [DLNumber] [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,    
 [ViolationDate] [datetime] NULL,    
 [Race] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,    
 [Gender] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,    
 [Height] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,    
 [ListDate] [datetime] NOT NULL,    
 [LetterCode] [smallint] NULL,    
 [MailDateREGULAR] [datetime] NULL,    
 [MailDateFTA] [datetime] NULL,    
 [Bondflag] [tinyint] NULL ,    
 [officerNumber_Fk] [int] NULL,    
 [RecordID] [int],  
 [DP2] [varchar](10),  
 [DPC] [varchar](10),  
 [Flag1] [varchar](1)          
)    
    
declare  @tblTicketsViolationsArchive table (  [TicketNumber_PK] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,    
 [ViolationNumber_PK] [int]  NULL , --Haris 9709 09/29/2011 datatype changed to int from tinyint
 [FineAmount] [money] NULL,    
 [ViolationDescription] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,    
 [ViolationCode] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,    
 [violationstatusid] [int] NULL,    
 [CourtDate] [datetime] NULL,    
 [Courtnumber] [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,    
 [BondAmount] [money] NULL ,    
 [TicketOfficerNumber] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,    
 [RecordID] [int]  NULL ,    
 [CauseNumber] [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,  
 [OffenseLocation] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,  
 [OffenseTime] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL   
)    
    
    

    
-- FIRST GET THE RECORD ID & VIOLATION DATE OF THE TICKET .......    
select  @RecordId = t.recordid ,    
  @violationdate = ISNULL(t.violationdate,'01/01/1900'),    
  @Officernumber = ISNULL(T.officernumber_fk,962528),    
  @listdate = t.listdate,    
  @courtid = t.courtid,    
  @FTALinkId = v.ftalinkid,    
  @courtdate = t.courtdate,    
  @CauseNumber = v.causenumber    
from  trafficticketsarchive.dbo.tblticketsarchive t     
inner join     
  trafficticketsarchive.dbo.tblticketsviolationsarchive v    
on   t.recordid = v.recordid    
where  v.ticketnumber_pk = @ticketnumber    
    
    
-- CHECK IF CAUSE NUMBER ALREADY EXISTS IN CLIENT/QUOTE DATABASE........    
select @ticketid = t.ticketid_pk,    
  @iFlag = t.activeflag     
from tbltickets t inner join tblticketsviolations v    
on  t.ticketid_pk = v.ticketid_pk    
and  v.casenumassignedbycourt like @causenumber    
    
SELECT @TICKETID = ISNULL(@TICKETID, 0)    
    
-- IF NOT EXISTS THEN........    
if @ticketid = 0     
begin    
 select @iFlag = 2    
    
 -- ROUTINES FOR INSIDE COURTS.................................    
 if @courtid in (3001,3002,3003)    
  begin    
    
    
   -- CONDITION # 1: WHEN FTA LINK ID IS NOT NULL    
   if @ftalinkid is not null    
    begin    
     INSERT into @tblTicketsArchive(    
      TicketNumber, MidNumber, FirstName, LastName, Initial, Address1, Address2, City, StateID_FK, ZipCode,       PhoneNumber, DOB, DLNumber, ViolationDate, Race, Gender, Height, ListDate, MailDateREGULAR, MailDateFTA,       Bondflag, officerNumber_Fk, RecordID , DPC , DP2, FLAG1   
      )    
     select  t.TicketNumber, t.MidNumber, t.FirstName, t.LastName, t.Initial, t.Address1, t.Address2, t.City, t.StateID_FK, t.ZipCode,       t.PhoneNumber, t.DOB, t.DLNumber, t.ViolationDate, t.Race, t.Gender, t.Height, t.ListDate, t.MailDateREGULAR, t.MailDateFTA,       t.Bondflag, t.officerNumber_Fk, t.RecordID  , t.DPC , t.DP2, t.FLAG1   
     from trafficticketsarchive.dbo.tblticketsarchive t, trafficticketsarchive.dbo.tblticketsviolationsarchive v    
     where t.recordid = v.recordid and t.clientflag = 0 and v.ftalinkid = @ftalinkid    
    
     insert into @tblTicketsViolationsArchive (    
       TicketNumber_PK, ViolationNumber_PK, FineAmount, ViolationDescription, ViolationCode,        violationstatusid, CourtDate, Courtnumber, BondAmount, TicketOfficerNumber,        RecordID, CauseNumber, OffenseLocation, OffenseTime
       )      
     select v.TicketNumber_PK, v.ViolationNumber_PK, v.FineAmount, v.ViolationDescription, v.ViolationCode,        v.violationstatusid, v.CourtDate, v.Courtnumber, v.BondAmount, v.TicketOfficerNumber,        v.RecordID, v.CauseNumber, v.OffenseLocation, v.OffenseTime    
     from trafficticketsarchive.dbo.tblticketsviolationsarchive v , @tblTicketsArchive t    
     where t.recordid = v.recordid    
    end    
    
    
    
   -- CONDITION # 2: WHEN FTA LINK ID IS NULL AND VIOLATION DATE IS NULL    
   IF @ftalinkid is null and datediff(day, @violationdate, '01/01/1900') = 0     
    begin    
     INSERT into @tblTicketsArchive(    
      TicketNumber, MidNumber, FirstName, LastName, Initial, Address1, Address2, City, StateID_FK, ZipCode,       PhoneNumber, DOB, DLNumber, ViolationDate, Race, Gender, Height, ListDate, MailDateREGULAR, MailDateFTA,       Bondflag, officerNumber_Fk, RecordID  , DPC , DP2, FLAG1  
      )    
     select  t.TicketNumber, t.MidNumber, t.FirstName, t.LastName, t.Initial, t.Address1, t.Address2, t.City, t.StateID_FK, t.ZipCode,       t.PhoneNumber, t.DOB, t.DLNumber, t.ViolationDate, t.Race, t.Gender, t.Height, t.ListDate, t.MailDateREGULAR, t.MailDateFTA,       t.Bondflag, t.officerNumber_Fk, t.RecordID , t.DPC , t.DP2, t.FLAG1    
     from trafficticketsarchive.dbo.tblticketsarchive t where t.clientflag = 0 and t.recordid = @recordid    
    
     insert into @tblTicketsViolationsArchive (    
       TicketNumber_PK, ViolationNumber_PK, FineAmount, ViolationDescription, ViolationCode,        violationstatusid, CourtDate, Courtnumber, BondAmount, TicketOfficerNumber,        RecordID, CauseNumber, OffenseLocation, OffenseTime
       )      
     select v.TicketNumber_PK, v.ViolationNumber_PK, v.FineAmount, v.ViolationDescription, v.ViolationCode,        v.violationstatusid, v.CourtDate, v.Courtnumber, v.BondAmount, v.TicketOfficerNumber,        v.RecordID, v.CauseNumber, v.OffenseLocation, v.OffenseTime   
     from trafficticketsarchive.dbo.tblticketsviolationsarchive v, @tblTicketsArchive t    
     where t.recordid = v.recordid    
    end    
    
    
    
   -- CONDITION # 3: WHEN FTA LINK ID IS NULL , VIOLATION DATE IS NOT NULL AND OFFICER NUMBER IS NULL       
   IF  @ftalinkid is null and datediff(day, @violationdate, '01/01/1900') <> 0 and @officernumber = 962528     
   begin    
     -- ADD RECORDS IN TEMP TABLES BY MATCHING VIOLATION DATE & MID NUMMBER ONLY....    
     INSERT into @tblTicketsArchive(    
      TicketNumber, MidNumber, FirstName, LastName, Initial, Address1, Address2, City, StateID_FK, ZipCode,       PhoneNumber, DOB, DLNumber, ViolationDate, Race, Gender, Height, ListDate, MailDateREGULAR, MailDateFTA,       Bondflag, officerNumber_Fk, RecordID  , DPC , DP2, FLAG1  
      )    
     select  t.TicketNumber, t.MidNumber, t.FirstName, t.LastName, t.Initial, t.Address1, t.Address2, t.City, t.StateID_FK, t.ZipCode,       t.PhoneNumber, t.DOB, t.DLNumber, t.ViolationDate, t.Race, t.Gender, t.Height, t.ListDate, t.MailDateREGULAR, t.MailDateFTA,       t.Bondflag, t.officerNumber_Fk, t.RecordID   , t.DPC , t.DP2, t.FLAG1  
     from trafficticketsarchive.dbo.tblticketsarchive t where t.clientflag = 0    
     and  datediff(day, t.violationdate, @violationdate) = 0    
     and  t.midnumber = @midnumber    
    
     insert into @tblTicketsViolationsArchive (    
       TicketNumber_PK, ViolationNumber_PK, FineAmount, ViolationDescription, ViolationCode,        violationstatusid, CourtDate, Courtnumber, BondAmount, TicketOfficerNumber,        RecordID, CauseNumber, OffenseLocation, OffenseTime
       )      
     select v.TicketNumber_PK, v.ViolationNumber_PK, v.FineAmount, v.ViolationDescription, v.ViolationCode,        v.violationstatusid, v.CourtDate, v.Courtnumber, v.BondAmount, v.TicketOfficerNumber,        v.RecordID, v.CauseNumber, v.OffenseLocation, v.OffenseTime  
     from trafficticketsarchive.dbo.tblticketsviolationsarchive v, @tblTicketsArchive t    
     where   t.recordid = v.recordid    
    end     
    
    
    
   -- CONDITION # 4: WHEN FTA LINK ID IS NULL , VIOLATION DATE IS NOT NULL AND OFFICER NUMBER IS NOT NULL       
   IF @ftalinkid is null and datediff(day, @violationdate, '01/01/1900') <> 0 and @officernumber <> 962528    
    BEGIN          
     INSERT into @tblTicketsArchive(    
      TicketNumber, MidNumber, FirstName, LastName, Initial, Address1, Address2, City, StateID_FK, ZipCode,       PhoneNumber, DOB, DLNumber, ViolationDate, Race, Gender, Height, ListDate, MailDateREGULAR, MailDateFTA,       Bondflag, officerNumber_Fk, RecordID ,  DPC , DP2, FLAG1  
      )    
     select  t.TicketNumber, t.MidNumber, t.FirstName, t.LastName, t.Initial, t.Address1, t.Address2, t.City, t.StateID_FK, t.ZipCode,       t.PhoneNumber, t.DOB, t.DLNumber, t.ViolationDate, t.Race, t.Gender, t.Height, t.ListDate, t.MailDateREGULAR, 
     t.MailDateFTA,       t.Bondflag, t.officerNumber_Fk, t.RecordID  , t.DPC , t.DP2, t.FLAG1   
     from trafficticketsarchive.dbo.tblticketsarchive t where t.clientflag = 0    
     and  datediff(day, t.violationdate, @violationdate) = 0    
     and  t.midnumber = @midnumber AND t.officernumber_fk = @officernumber    
    
     insert into @tblTicketsViolationsArchive (    
       TicketNumber_PK, ViolationNumber_PK, FineAmount, ViolationDescription, ViolationCode,        violationstatusid, CourtDate, Courtnumber, BondAmount, TicketOfficerNumber,        RecordID, CauseNumber, OffenseLocation, OffenseTime
       )      
     select v.TicketNumber_PK, v.ViolationNumber_PK, v.FineAmount, v.ViolationDescription, v.ViolationCode,        v.violationstatusid, v.CourtDate, v.Courtnumber, v.BondAmount, v.TicketOfficerNumber,        v.RecordID, v.CauseNumber  , v.OffenseLocation, v.OffenseTime  
     from trafficticketsarchive.dbo.tblticketsviolationsarchive v, @tblTicketsArchive t    
     where t.recordid = v.recordid    
    end    
  end    
    
    
    
    
 -- ROUTINES FOR OUTSIDE COURTS.................................    
 else    
  begin    
   INSERT into @tblTicketsArchive(    
    TicketNumber, MidNumber, FirstName, LastName, Initial, Address1, Address2, City, StateID_FK, ZipCode,     PhoneNumber, DOB, DLNumber, ViolationDate, Race, Gender, Height, ListDate, MailDateREGULAR, MailDateFTA,     Bondflag, officerNumber_Fk, RecordID,
    DPC , DP2, FLAG1
    )    
   select  t.TicketNumber, t.MidNumber, t.FirstName, t.LastName, t.Initial, t.Address1, t.Address2, t.City, t.StateID_FK, t.ZipCode,     t.PhoneNumber, t.DOB, t.DLNumber, t.ViolationDate, t.Race, t.Gender, t.Height, t.ListDate, t.MailDateREGULAR, 
   t.MailDateFTA,     t.Bondflag, t.officerNumber_Fk, t.RecordID   , t.DPC , t.DP2, t.FLAG1  
   from trafficticketsarchive.dbo.tblticketsarchive t    
   where t.midnumber = @midnumber and t.listdate = @listdate and t.clientflag = 0    
    
   insert into @tblTicketsViolationsArchive (    
     TicketNumber_PK, ViolationNumber_PK, FineAmount, ViolationDescription, ViolationCode,      violationstatusid, CourtDate, Courtnumber, BondAmount, TicketOfficerNumber,      RecordID, CauseNumber, OffenseLocation, OffenseTime
     )   
   select v.TicketNumber_PK, v.ViolationNumber_PK, v.FineAmount, v.ViolationDescription, v.ViolationCode,      v.violationstatusid, v.CourtDate, v.Courtnumber, v.BondAmount, v.TicketOfficerNumber,      v.RecordID, v.CauseNumber  , v.OffenseLocation, v.OffenseTime  
   from trafficticketsarchive.dbo.tblticketsviolationsarchive v, @tblTicketsArchive t    
   where t.recordid = v.recordid    
  end    
      
  ------------------------------------------------------------------------------------------------------------------------      
  --    INSERTING MAIN INFORMATION INTO TBLTICKETS FROM TBLTICKETSARCHIVE      
  ------------------------------------------------------------------------------------------------------------------------      
 begin tran     
 insert into tbltickets      
   (      
  midnum, firstname, lastname, MiddleName, Address1, Address2, City, Stateid_FK, Zip, Contact1, ContactType1,    
  DOB, DLNumber, employeeid, Race, Gender, Height, maildate,      
  bondflag, BondsRequiredflag, employeeidupdate, RecordId , DPC , DP2,IsTrafficAlertSignup, FLAG1)      
 select distinct top 1    
  MidNumber, Upper(FirstName), Upper(LastName), Upper(Initial), Upper(Address1), Upper(Address2), Upper(City), StateID_FK, ZipCode, phonenumber, 100,    
  DOB, DLNumber,  @EmpId, Upper(Race), Gender, Height, 
  case when ticketnumber like 'F%' then maildatefta  else maildateregular  end, bondflag, 3,    
  @EmpId, RecordId , DPC , DP2,@IsTrafficClient, FLAG1   
 from   @tblTicketsArchive    
 where recordid = @recordid    
        
 if @@error = 0     
  BEGIN     
   --ozair Task 4767 09/11/2008 replace @@Identity with Scope_Identity 
	--as @@Identity returns the last inserted identity value instead of the insert statement executed in current scope
	--select @TicketID=@@identity 
	select @TicketID=SCOPE_IDENTITY()
   SET @ErrorNumber = 0    
  END    
 else    
  begin    
   set @ErrorNumber  = @@error    
  end    
    
       
 ------------------------------------------------------------------------------------------------------------------------      
 --  INSERTING DETAILED INFORMATION INTO TBLTICKETSVIOLATIONS FROM TBLTICKETSVIOLATIONSARCHIVE      
 ------------------------------------------------------------------------------------------------------------------------      
  IF @TicketId <> 0     
   begin    
    declare @temp1 table       
     (      
     violationnumber  int,      
     bondamount  money,      
     FineAmount   money,      
     TicketNumber   varchar(20),      
     --ozair 4775 09/16/2008 datatype changed to int from tinyint as the data/field inserted in this is of type smallint
     sequenceNumber   int,      
     Violationcode   varchar(10),      
     ViolationStatusID  int,      
     CourtNumber   varchar(10),      
     CourtDate   datetime,      
     ViolationDescription  varchar(200),      
     courtid   int,      
     ticketviolationdate  datetime,      
     ticketofficernum  varchar(20)  ,    
     CauseNumber varchar(25),    
  OffenseLocation varchar(200),  
  OffenseTime varchar(20),  
     RecordId  int    
     )      
          
    declare @temp2 table      
     (      
     violationnumber  int,      
     fineamount  money,      
     ticketnumber  varchar(25),      
     sequencenummber  int,      
     violationcode  varchar(10),      
     bondamount  money,      
     violationstatusid int,      
     courtnumber  int,      
     courtdate  datetime,      
     courtid   int  ,    
     CauseNumber varchar(25),     
  OffenseLocation varchar(200),  
  OffenseTime varchar(20),  
     recordid int    
     )    
    
    if @courtid in (3001,3002,3003)    
     begin    
      insert into @temp1      
      SELECT  distinct       
       isnull(L.ViolationNumber_PK,0) as ViolationNumber,      
       isnull(V.bondamount,0) as BondAmount,       
       V.FineAmount,      
       v.ticketnumber_pk,    
       V.ViolationNumber_PK as sequenceNumber,      
       V.violationcode as Violationcode,       
       v.ViolationStatusID,      
       v.CourtNumber,      
       V.CourtDate,      
       isnull(v.ViolationDescription,'N/A') as violationdescription,       
       @CourtId,       
       t.violationdate,      
       isnull(v.TicketOfficerNumber,962528)  ,    
       v.CauseNumber,    
    v.OffenseLocation,  
    v.OffenseTime,  
       v.recordid    
      from  @tblTicketsArchive t      
      INNER JOIN       
       @tblTicketsViolationsArchive V       
      on ( t.recordid = v.recordid )    
      LEFT OUTER JOIN       
       tblViolations  L       
      ON   V.violationcode = L.violationcode       
     end    
    else    
     begin    
      insert into @temp1      
      SELECT  distinct       
       isnull(L.ViolationNumber_PK,0) as ViolationNumber,      
       isnull(V.bondamount,0) as BondAmount,       
       V.FineAmount,      
       v.ticketnumber_pk,    
       '0' as sequenceNumber,      
       V.violationcode as Violationcode,       
       v.ViolationStatusID,      
       v.CourtNumber,      
       V.CourtDate,      
       isnull(v.ViolationDescription,'N/A') as violationdescription,       
       @CourtId,       
       t.violationdate,      
       isnull(v.TicketOfficerNumber,962528)  ,    
       v.CauseNumber,   
    v.OffenseLocation,  
    v.OffenseTime,   
       v.recordid    
      from  @tblTicketsArchive t      
      INNER JOIN       
       @tblTicketsViolationsArchive V       
      on ( t.recordid = v.recordid )    
      LEFT OUTER JOIN       
       tblViolations  L       
      ON   V.violationdescription  = L.description    
      and  l.violationtype = (    
        select violationtype from tblcourtviolationtype    
        where courtid = @courtid    
        )    
     end    
              
    
    insert into @temp2      
    select V.ViolationNumber_PK ,      
     t1.fineamount,      
     t1.TicketNumber,      
     t1.sequencenumber,      
     t1.violationcode,      
     t1.bondamount,      
     ViolationStatusID,      
     CourtNumber,      
     CourtDate,      
     courtid  ,     
     t1.CauseNumber,    
  t1.OffenseLocation,  
  t1.OffenseTime,  
     t1.recordid    
    from @temp1 t1      
    inner join       
     tblViolations V       
    on  t1.sequencenumber = V.sequenceorder      
    where  t1.violationnumber = 0         
        
    update  t1      
    set t1.violationnumber = t2.violationnumber      
    from   @temp1 t1,       
     @temp2 t2      
    where   t1.violationcode = t2.violationcode      
          
    select  @PlanId =     
     (    
     select  top 1 planid     
     from  tblpriceplans     
     where  courtid =@Courtid    
     and  effectivedate <= @listdate    
     and enddate > = @listdate    
     and isActivePlan = 1    
     order by planid    
     )    
         
       
    insert into tblticketsviolations      
     (      
     TicketID_PK,      
     ViolationNumber_PK,      
     FineAmount,      
     refcasenumber,      
     bondamount,      
     CourtViolationStatusID,      
     CourtNumber,      
     CourtDate,      
     violationdescription,      
     courtid,      
     ticketviolationdate,      
     TicketOfficerNumber,      
     vemployeeid,      
     CourtViolationStatusIDmain,      
     CourtNumbermain,      
     CourtDatemain,      
     CourtViolationStatusIDscan,      
     CourtNumberscan,      
     CourtDatescan ,    
     casenumassignedbycourt,   
  OffenseLocation,  
  OffenseTime,   
     PlanId    
     )      
          
    SELECT  distinct        
     @TicketID,      
     violationNumber,      
     FineAmount,      
     TicketNumber,      
     bondamount,      
     ViolationStatusID,      
     CourtNumber,      
     CourtDate,      
     violationdescription,      
     @courtid,      
     ticketviolationdate,      
     ticketofficernum,      
     @empid,      
     ViolationStatusID,      
     CourtNumber,      
     CourtDate,      
     ViolationStatusID,      
     CourtNumber,      
     CourtDate,    
     CauseNumber,   
  OffenseLocation,  
  OffenseTime,   
     isnull(@PlanId ,45)    
        from  @temp1      
    
    if @@error <> 0     
     set @ErrorNumber  = @@error    
    
    
    --------------------------------------------------------------------------------------------------------------------      
    --    INSERTING HISTORY NOTES      
    --------------------------------------------------------------------------------------------------------------------      
      --insert into tblticketsnotes(ticketid, subject, employeeid) values (@TicketID, 'Initial Inquiry', @empid )      
    
    declare @strNote varchar(100)    
    select  @strNote = 'INITIAL INQUIRY '+ upper(isnull(tc.shortname,'N/A')) + ':' + upper(isnull(c.ShortDescription,'N/A')) + ':' + CONVERT(varchar(10), isnull(v.CourtDateMain,'1/1/1900'), 101) + ' #' +  isnull(v.courtnumbermain ,'0')+ ' @ ' + upper(dbo.
  
formattime(isnull(v.CourtDateMain,'1/1/1900')))    
    FROM tblTicketsViolations v     
    left outer join    
      tblCourtViolationStatus c     
    ON  v.CourtViolationStatusIDmain = c.CourtViolationStatusID    
    left outer  join    
     tblcourts tc    
    on  v.courtid=tc.courtid    
    where  v.ticketid_pk = @TicketID    
    
    
    if @strNote is not null    
       insert into tblticketsnotes(ticketid, subject, employeeid) values (@TicketID, @strNote, @empid )    
    
    if @@error <> 0     
     set @ErrorNumber  = @@error    
    
  
  
  
  
	-- Agha Usman 4347 07/22/2008  - Problem Client Detection
	declare @Firstname varchar(20)                                         
	declare @Lastname varchar(20)                                         
	declare @MiddleName  varchar(6)                                          
	declare @DOB datetime
	
	
	select @FirstName = FirstName, @LastName = LastName , @MiddleName = Initial, @DOB = DOB from @tblTicketsArchive

	exec dbo.USP_HTP_ProblemClient_Manager  @TicketId,@FirstName,@LastName,@MiddleName,@DOB,NULL
    
    --------------------------------------------------------------------------------------------------------------------      
    --    UPDATING CLIENT FLAG IN TBLTICKETSARHCIVE    
    --------------------------------------------------------------------------------------------------------------------      
    update t    
    set  clientflag = 1     
    from   trafficticketsarchive.dbo.tblticketsarchive t inner join @tblTicketsArchive t2    
    on  t.recordid = t2.recordid    
    
    if @@error <> 0     
     set @ErrorNumber  = @@error    
    
    
   end      
    
  if @ErrorNumber = 0     
    commit tran    
  else    
    rollback tran    
END    
    
select @sOutPut = convert(varchar(100),@ticketid) + ','+  convert(varchar(3),@iFlag)    



ALTER procedure USP_HTS_get_receipt_info_batch  --3992,1,3,0               
 @empid int,                  
 @ChkVal  int,                  
 @letterType int ,                  
 @printed int,                
 @filedate datetime ='01/01/1900',                  
 @ticketidlist varchar(2000)  = '0'                      
      
as         
      
-- CREATING TEMP TABLE TO STORE THE TICKET IDs                        
declare @tickets table (                        
 ticketid int                           
 )                      
          
--Displaying records sorted when printed          
declare @ticketsort table          
(          
  rowid int identity(1,1),    
 ticketsort int,          
        batchid int   ,    
 casenumber varchar(200),      
 paymentamount money,    
        paymentinfo varchar(500),    
 contactinfo varchar(500)              
)            
                        
-- INSERTING TICKET IDs AS                         
if @chkVal = 1                        
begin              
 if @printed=2              
  begin                        
   insert into @tickets                        
   SELECT    distinct TicketID_FK                        
   FROM         dbo.tblHTSBatchPrintLetter                        
   WHERE     (LetterID_FK = @letterType) AND (IsPrinted = 0) AND (deleteflag <> 1)          
    
 insert into @ticketsort (ticketsort, batchid)          
   select  t.ticketid,hts.batchid_pk from @tickets t inner join tblhtsbatchprintletter hts          
  on t.ticketid=hts.ticketid_fk and hts.batchid_pk=(  select top 1 tbv.batchID_pk from tblhtsbatchprintletter tbv                                             
     where  tbv.ticketid_fk=hts.ticketid_fk and tbv.LetterID_Fk = @letterType AND (IsPrinted = 0)  and deleteflag<>1   )          
 --   where hts.LetterID_Fk = @letterType and deleteflag<>1 AND (IsPrinted = 0)                       
   end               
 else              
  begin                        
   insert into @tickets                        
   SELECT    distinct TicketID_FK                        
   FROM         dbo.tblHTSBatchPrintLetter                        
   WHERE     (LetterID_FK = @letterType) AND (deleteflag <> 1)            
 insert into @ticketsort    (ticketsort, batchid)          
   select  t.ticketid,hts.batchid_pk from @tickets t inner join tblhtsbatchprintletter hts          
  on t.ticketid=hts.ticketid_fk and hts.batchid_pk=(  select top 1 tbv.batchID_pk from tblhtsbatchprintletter tbv                                             
     where  tbv.ticketid_fk=hts.ticketid_fk and tbv.LetterID_Fk = @letterType  and deleteflag<>1   )          
--    where hts.LetterID_Fk = @letterType and deleteflag<>1                     
   end               
                
end                         
                        
if @chkVal = 2                        
begin                
 if @printed=2              
  begin                          
   insert into @tickets                        
   SELECT    distinct TicketID_FK                        
   FROM         dbo.tblHTSBatchPrintLetter                        
   WHERE     (LetterID_FK = @letterType)                         
   AND (IsPrinted = 0) AND (deleteflag <> 1)                        
   AND (datediff(day,BatchDate, @filedate) = 0)            
  insert into @ticketsort    (ticketsort, batchid)          
   select  t.ticketid,hts.batchid_pk from @tickets t inner join tblhtsbatchprintletter hts          
  on t.ticketid=hts.ticketid_fk and hts.batchid_pk=(  select top 1 tbv.batchID_pk from tblhtsbatchprintletter tbv                                             
   where  tbv.ticketid_fk=hts.ticketid_fk and tbv.LetterID_Fk = @letterType AND (IsPrinted = 0)  and deleteflag<>1 AND (datediff(day,BatchDate, @filedate) = 0)    )          
 --   where hts.LetterID_Fk = @letterType and deleteflag<>1 AND (IsPrinted = 0) AND (datediff(day,BatchDate, @filedate) = 0)                          
  end                      
                
 else              
  begin                          
   insert into @tickets                        
   SELECT    distinct TicketID_FK                        
   FROM         dbo.tblHTSBatchPrintLetter              
   WHERE     (LetterID_FK = @letterType)                         
   AND (deleteflag <> 1)                         
   AND (datediff(day,BatchDate, @filedate) = 0)          
 insert into @ticketsort   (ticketsort, batchid)           
   select  t.ticketid,hts.batchid_pk from @tickets t inner join tblhtsbatchprintletter hts          
  on t.ticketid=hts.ticketid_fk and hts.batchid_pk=(  select top 1 tbv.batchID_pk from tblhtsbatchprintletter tbv                                             
   where  tbv.ticketid_fk=hts.ticketid_fk and tbv.LetterID_Fk = @letterType   and deleteflag<>1 AND (datediff(day,BatchDate, @filedate) = 0)    )          
   -- where hts.LetterID_Fk = @letterType and deleteflag<>1 AND (datediff(day,BatchDate, @filedate) = 0)                          
  end                         
               
               
end                         
                        
if @chkVal = 3                        
begin                        
 insert into @tickets                        
 select * from dbo.Sap_String_Param(@ticketidlist)           
 insert into @ticketsort   (ticketsort, batchid)           
   select  t.ticketid,hts.batchid_pk from @tickets t inner join tblhtsbatchprintletter hts          
  on t.ticketid=hts.ticketid_fk and hts.batchid_pk=(  select top 1 tbv.batchID_pk from tblhtsbatchprintletter tbv                                             
     where  tbv.ticketid_fk=hts.ticketid_fk and tbv.LetterID_Fk = @letterType and deleteflag<>1   )          
  --  where hts.LetterID_Fk = @letterType and deleteflag<>1           
end             
      
----------------------------------------------------------------------------------        
      
--declare @paymentamount money,                
--declare @casenumbers varchar(500),                
--declare @paymentinfo varchar(500),                
--declare @contactinfo varchar(500)                
/*                
--select * from tblcourts                
select @paymentamount = sum(chargeamount)                 
from tblticketspayment tp inner join @ticketsort ts on ts.ticketsort=tp.ticketid       
and paymenttype not in (99,100)                
and paymentvoid not in (1)       
*/    
    
declare @reccount int , @idx int, @tic int, @temptic int, @paymentamount money, @paymentinfo varchar(500)                
select @reccount = count(rowid) , @idx = 1, @temptic = 0 , @paymentamount = 0,@paymentinfo='' from @ticketsort    
   --For payment information    
while @idx <= @reccount    
begin    
 select @tic = ticketid, @paymentamount= sum(chargeamount),@paymentinfo=@paymentinfo + '$' + convert(varchar(20),chargeamount)                  
          + '(' + convert(varchar(12),recdate) + ')' + ';'                   
 from tblticketspayment tp inner join @ticketsort ts on ts.ticketsort=tp.ticketid       
 and paymenttype not in (99,100)                
 and paymentvoid not in (1)        
 and rowid = @idx      
        group by ticketid,chargeamount,recdate    
    
 if @tic <> @temptic    
  begin    
   set @paymentinfo = ltrim(left(@paymentinfo,len(@paymentinfo)-1))                
   update @ticketsort set paymentamount =  @paymentamount,    
            paymentinfo= @paymentinfo    
   where rowid = @idx    
       
   select @temptic = @tic, @paymentamount = 0,@paymentinfo=''       
  end    
 set @idx = @idx + 1     
end             
                
--select * from @ticketsort           
declare @casenumber varchar(200)    --@reccount int , @idx int, @tic int, @temptic int, @casenumber varchar(200)    
select @reccount = count(rowid) , @idx = 1, @temptic = 0 , @casenumber = '' from @ticketsort    
   --For refcasenumber    
while @idx <= @reccount    
begin    
 select @tic = ticketid_pk, @casenumber = @casenumber + isnull(refcasenumber,' ')                 
--  + case                 
--   when courtid not in (3001,3002,3003) or (left(isnull(refcasenumber,' ') ,1) = 'F' and courtid in (3001,3002,3003)) then ','                
--   else '-' + isnull(convert(varchar(2),sequencenumber),' ') + ','                 
--  end                
 from tblticketsviolations tv inner join @ticketsort ts on ts.ticketsort=tv.ticketid_pk       
 and rowid = @idx       
    
 if @tic <> @temptic    
  begin    
   set @casenumber = ltrim(left(@casenumber,len(@casenumber)-1))      
   update @ticketsort set casenumber =  @casenumber    
   where rowid = @idx    
       
   select @temptic = @tic, @casenumber = ''       
  end    
 set @idx = @idx + 1     
end    
declare @contact1 as varchar(15)            
declare @contact2 as varchar(15)            
declare @contact3 as varchar(15)       
declare @contactinfo varchar(500)           
select @reccount = count(rowid) , @idx = 1, @temptic = 0 , @contactinfo = '' from @ticketsort    
    --contact info    
while @idx <= @reccount    
begin    
 select @tic = ticketid_pk, @contact1 = isnull(contact1,''),              
 @contact2=isnull(contact2,''),            
 @contact3=isnull(contact3,'')                    
 from tbltickets T inner join @ticketsort ts on ts.ticketsort=T.ticketid_pk                      
 where (contact1 is not null or contact2 is not null or contact3 is not null )             
 and rowid = @idx               
 set @contactinfo=@contact1            
 if (@contact2<>'')            
 set @contactinfo=@contactinfo+'/'+@contact2            
 if (@contact3<>'')            
 set @contactinfo=@contactinfo+'/'+@contact3            
 if (left(@contactinfo,1)='/')            
 begin            
 set @contactinfo=right(@contactinfo,len(@contactinfo)-1)            
 end      
     
 if @tic <> @temptic    
  begin     
   update @ticketsort set contactinfo =  @contactinfo    
   where rowid = @idx    
       
   select @temptic = @tic, @contactinfo = ''       
  end    
 set @idx = @idx + 1     
end    
    
--select * from @ticketsort                
    
/*    
    
set @casenumbers = ' '                
select  @casenumbers = @casenumbers +  isnull(refcasenumber,' ')                 
+ case                 
--left(isnull(refcasenumber,' ') ,1)                 
--when 'F' and courtid in (3001,3002,3003) then ','                
--when 'F' and courtid in (3001,3002,3003) then ','                
when courtid not in (3001,3002,3003) or (left(isnull(refcasenumber,' ') ,1) = 'F' and courtid in (3001,3002,3003)) then ','                
else '-' + isnull(convert(varchar(2),sequencenumber),' ') + ','                 
end                
                
from tblticketsviolations tv inner join @ticketsort ts on ts.ticketsort=tv.ticketid_pk                  
--where ticketid_pk in (ts.ticketsort)                
--group by ts.ticketsort,refcasenumber,courtid,sequencenumber    
--set @casenumbers = ltrim(left(@casenumbers,len(@casenumbers)-1))                
    
*/                
                
                
  --newly updated  in order to get contacts from tbltickets            
---------------            
/*    
declare @contact1 as varchar(15)            
declare @contact2 as varchar(15)            
declare @contact3 as varchar(15)            
            
select @contact1 = isnull(contact1,''),              
@contact2=isnull(contact2,''),            
@contact3=isnull(contact3,'')                    
from tbltickets T inner join @ticketsort ts on ts.ticketsort=T.ticketid_pk                      
where (contact1 is not null or contact2 is not null or contact3 is not null )             
--and ticketid_pk=@ticketid            
            
set @contactinfo=@contact1            
if (@contact2<>'')            
set @contactinfo=@contactinfo+'/'+@contact2            
if (@contact3<>'')            
set @contactinfo=@contactinfo+'/'+@contact3            
if (left(@contactinfo,1)='/')            
begin            
set @contactinfo=right(@contactinfo,len(@contactinfo)-1)            
end               
---------------                 
*/        
/*                
set @paymentinfo = ' '                
select @paymentinfo = @paymentinfo + '$' + convert(varchar(20),chargeamount)                  
+ '(' + convert(varchar(12),recdate) + ')' + ';'                
from tblticketspayment tp inner join @ticketsort ts on ts.ticketsort=tp.ticketid      --where ticketid = @ticketID                
and paymenttype not in (99,100)                
and paymentvoid not in (1)                
set @paymentinfo = ltrim(left(@paymentinfo,len(@paymentinfo)-1))                
                
 */               
select T.ticketid_pk,firstname,lastname,T.Address1, T.Address2, ts.casenumber as casenumbers,t.languagespeak,                
ts.paymentamount as paymentamount,T.city,S.state as state,T.zip,          
C.Address + ' ' + C.Address2 + ' ' + C.city + ', ' + 'TX ' + C.zip as Address,                
V.CourtDateMain,totalfeecharged,totalfeecharged-ts.paymentamount as dueamount,                
 d.description as settintype, ts.paymentinfo as paymentinfo, ts.contactinfo as contactinfo,ts.batchid          --@casenumber as casenumbers       
from @ticketsort ts,tbltickets T,  tblcourts C, tbldatetype D   ,tblstate S, tblticketsviolations V             
where T.ticketid_pk = V.ticketid_pk                
and                 
V.CourtId = C.courtid                
--and T.ticketid_pk = P.ticketid                
and V.CourtViolationStatusIdMain = typeid         -- Agha Usman 2664 06/13/2008  
and T.stateid_fk=S.stateid              
and T.ticketid_pk in (ts.ticketsort)                
and activeflag = 1                
order by ts.batchid                
                
--if @@rowcount >=1                 
-- exec sp_Add_Notes @TicketID,'Receipt Printed',null,@employeeid                
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_msmq_updatemessagescan]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_msmq_updatemessagescan]
GO


create procedure usp_msmq_updatemessagescan    
    
@id int,    
@Status varchar(50)    
    
as    
    
update tbl_webscan_msmqlog  
set scanstatus=@Status    
where id=@id 


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


﻿ /************************************************************
 * Created By : Noufil Khan 
 * Created Date: 11/1/2010 5:37:42 PM
 * Business Logic :
 * Parameters :
 * Column Return :
 ************************************************************/

ALTER PROCEDURE [dbo].[USP_HTP_UpdatePatientActiveFlag]
	@isactive BIT,
	@patientId INT
AS
	UPDATE patient
	SET    isactive = @isactive
	WHERE  Id = @patientId
GO
 
GRANT EXECUTE ON [dbo].[USP_HTP_UpdatePatientActiveFlag] TO dbr_webuser
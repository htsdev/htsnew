SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_LA_Get_Summary]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_LA_Get_Summary]
GO



CREATE PROCEDURE USP_HTS_LA_Get_Summary
@Group_ID as int

AS

Select * From Tbl_HTS_LA_Summary Where GroupID = @Group_ID


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


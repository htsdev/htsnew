USE TrafficTickets
GO
/****** 
Created by:		Rab Nawaz Khan
Task:			11431
Date:			10/04/2013
Business Logic:	The procedure is used by LMS application to get the number of printable letter
				count for the Harris County Criminal 'Criminal - Updated Address' Mailer for the selected date range.
				
List of Parameters:
	@catnum:		Category number of the selected letter type, IF printing for all courts of the selected court category
	@LetterType:	Selected letter type in LMS
	@CrtId:			Court id of the selected court category IF printing for individual court hosue.
	@startListdate:	Starting list date for Arraignment letter.
	@endListDAte:	Ending list date for Arraignment letter.
	@SearchType:	Flag that identifies IF searching by court date or by list date. 0= list date, 1 = court date

List of Columns:	
	Total_Count:	Total number of letters exists for the selected date range.
	Good_Count:		Total number of letters that have valid addresses
	Bad_Count:		Total Number of letter that have undeliverable addresses
	DonotMail_Count:Total number of letter for addresses that had been marked to stop sending letters.
******/

--  [dbo].[usp_mailer_Get_HCC_Criminal_UpdatedAddress] 8, 147, 8, '10/01/2013', '10/11/2013', 0
ALTER PROCEDURE [dbo].[usp_mailer_Get_HCC_Criminal_UpdatedAddress]
(
	@catnum		INT,
	@LetterType 	INT,
	@crtid 		INT,                                                 
	@startListdate 	DATETIME,
	@endListdate 	DATETIME,                                                      
	@SearchType     INT                                             
)

AS
BEGIN
	       
	SET NOCOUNT ON;     
	-- DECLARING LOCAL VARIABLES FOR FILTERS....
	DECLARE	@officernum VARCHAR(50), 
	@officeropr VARCHAR(50), 
	@tikcetnumberopr VARCHAR(50), 
	@ticket_no VARCHAR(50), 
	@zipcode VARCHAR(50), 
	@zipcodeopr VARCHAR(50), 
	@fineamount MONEY,
	@fineamountOpr VARCHAR(50) ,
	@fineamountRelop VARCHAR(50),
	@singleviolation MONEY, 
	@singlevoilationOpr VARCHAR(50), 
	@singleviolationrelop VARCHAR(50), 
	@doubleviolation MONEY,
	@doublevoilationOpr VARCHAR(50), 
	@doubleviolationrelop VARCHAR(50), 
	@count_Letters INT,
	@violadesc VARCHAR(500),
	@violaop VARCHAR(50),
	@sqlquery NVARCHAR(MAX),
	@sqlquery2 NVARCHAR(MAX),
	@sqlParam NVARCHAR(1000)


	-- SETTING PARAMETER LIST AND ORDER FOR DYNAMIC SQL.....
	SELECT @sqlparam = '@catnum INT, @LetterType INT, @crtid INT, @startListdate DATETIME, 	
				@endListdate DATETIME, @SearchType INT'

	-- INITIALIZING LOCAL VARIABLE FOR DYNAMIC SQL...                                                             
	SELECT @sqlquery ='', @sqlquery2 = ''
	                                                              

	-- GETTING DIFFERENT FILTER TYPES AND THIER VALUES IN THE LOCAL VARIABLES
	-- IF ASSOCIACTED WITH THE LETTER.......
	SELECT	@officernum=officernum,
	@officeropr=oficernumOpr,
	@tikcetnumberopr=tikcetnumberopr,
	@ticket_no=ticket_no,
	@zipcode=zipcode,
	@zipcodeopr=zipcodeLikeopr,
	@singleviolation =singleviolation,
	@singlevoilationOpr =singlevoilationOpr, 
	@singleviolationrelop =singleviolationrelop, 
	@doubleviolation = doubleviolation,
	@doublevoilationOpr=doubleviolationOpr,
	@doubleviolationrelop=doubleviolationrelop,
	@violadesc=violationdescription,
	@violaop=violationdescriptionOpr , 
	@fineamount=fineamount ,
	@fineamountOpr=fineamountOpr ,
	@fineamountRelop=fineamountRelop  
	FROM tblmailer_letters_to_sendfilters WITH(NOLOCK)  
	WHERE courtcategorynum=@catnum 
	AND Lettertype=@LetterType 

	-- MAIN QUERY....
	-- LIST DATE WISE NUMBER OF DISTINCT NON-CLIENTS
	-- FOR THE SELECTED DATE RANGE ONLY 
	-- INSERTING VALUES IN A TEMP TABLE....                                                      
	SET @sqlquery=@sqlquery+'                                          
	SELECT distinct CONVERT(DATETIME, CONVERT(VARCHAR(10),tn.InsertDate1,101)) as mdate,     
	flag1 = ISNULL(tn.AddressStatus1,''N''),                                                      
	tva.recordid,
	count(tva.ViolationNumber_PK) as ViolCount,                                                      
	ISNULL(sum(ISNULL(tva.fineamount,0)),0) as Total_Fineamount
	INTO #temp                                                            
	FROM dbo.tblticketsarchive ta WITH(NOLOCK)
	INNER JOIN tblticketsviolationsarchive tva WITH(NOLOCK) on tva.recordid = ta.recordid
	INNER JOIN tblNewAddresses tn WITH(NOLOCK) ON ta.RecordId = tn.RecordId

	-- NOT MOVED IN PAST 48 MONTHS (ACCUZIP PROCESSED)...
	WHERE ISNULL(ta.ncoa48flag,0) = 0

	-- EXCLUDE CERTAIN DISPOSITION STATUSES.....
	AND tva.hccc_dst not in (''A'',''D'',''P'')
	AND tva.violationstatusid <> 80
	
	-- Address Not Marked As No New Address 
	AND ISNULL(tn.IsNoNewAddressMarked, 0) = 0
	
	-- Only Get the Records which have not sent the Mailer for address 1. . . 
	AND ISNULL(tn.isMailerSentOnAddress1, 0) = 0
	
	-- FOR SELECTED DATE RANGE ONLY...
	AND datediff(day, tn.InsertDate1 , @startListdate)<=0
	AND datediff(day, tn.InsertDate1, @endlistdate) >= 0
	
	AND ISNULL(tva.cad,'''') = ''''
	AND ISNULL(tva.attorneyname,'''') = ''''
	AND (ISNULL(tva.rea,'''') not in (''DISM'',''DADD'', ''DADH'',''DFPD'',''MCH'',''MCHJ'',''MCHR'',''ADGM'',''ADMP'',''APRB'',''DISP'',''EXHR'',''MCCA'',''NGIH'',''PFCS'',''PNDC'',''PNGJ'',''SFBF''))
	'

	-- IF PRINTING FOR ALL COURTS OF THE SELECTED COURT CATEGORY...  
	IF(@crtid= @catnum  )                                    
		SET @sqlquery=@sqlquery+' 
		AND tva.courtlocation In (SELECT courtid FROM tblcourts WITH(NOLOCK) WHERE courtcategorynum = @crtid )'                                        

	-- ELSE PRINTING FOR THE INDIVIDUAL SELECTED COURT OF THE CATEGORY....
	ELSE 
		SET @sqlquery =@sqlquery +' 
		AND tva.courtlocation = @crtid' 
	                                                        
	-- LMS FILTERS SECTION:
	-----------------------------------------------------------------------------------------------------------------------------------------------------
	-- IF THERE IS ANY FILTER SPECIFIED TO INCLUDE/EXCLUDE ONLY PARTICULAR OFFICER NUMBERS....
	IF(@officernum<>'')                                                        
		SET @sqlquery =@sqlquery + '  
		AND ta.officerNumber_Fk '  + @officeropr  +'('+ @officernum+')'                                                         


	-- IF THERE IS ANY FILTER DEFINED FOR INCLUDE/EXCLUDE VIOLATION DESCRIPTIONS...
	IF(@ticket_no<>'')                        
		SET @sqlquery=@sqlquery + ' 
		AND ('+  dbo.Get_String_Concat_With_op(''+@ticket_no+'' , 'tva.TicketNumber_PK' ,+ ''+ @tikcetnumberopr) +')'                                                              
	                                    
	-- ZIP CODE FILTER......
	-- INCLUDE/EXCLUDE THE SPECIFIED ZIP CODES.....
	IF(@zipcode<>'')                                                                              
		SET @sqlquery=@sqlquery+ ' 
		AND ('+ dbo.Get_String_Concat_With_op(''+@zipcode+'' , 'left(ta.zipcode,3)' ,+ ''+ @zipcodeopr) +')'                                   

	-- FINE AMOUNT FILTER.....
	-- INCLUDE ONLY THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT....
	IF(@fineamount<>0 AND @fineamountOpr<>'not'  )                                  
		SET @sqlquery =@sqlquery+ '
		AND tva.fineamount'+ CONVERT (VARCHAR(10),@fineamountRelop) +'CONVERT(MONEY,'+ CONVERT(VARCHAR(10),@fineamount) +')'                                       
	                                                      
	-- EXCLUDE THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT.....
	IF(@fineamount<>0 AND @fineamountOpr = 'not'  )                                  
		SET @sqlquery =@sqlquery+ '
		AND not tva.fineamount'+ CONVERT (VARCHAR(10),@fineamountRelop) +'CONVERT(MONEY,'+ CONVERT(VARCHAR(10),@fineamount) +')'                                       
	-----------------------------------------------------------------------------------------------------------------------------------------------------
	SET @sqlquery =@sqlquery+ ' 
	GROUP BY                                                       
	CONVERT(DATETIME, CONVERT(VARCHAR(10), tn.InsertDate1, 101)),
	  tn.AddressStatus1,      
	  tva.recordid                                                
	HAVING 1 =1 
	'                                  

	-- SINGLE VIOLATION FINE AMOUNT FILTER....
	-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE SINGLE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT
	IF(@singleviolation<>0 AND @doubleviolation=0)                                
		SET @sqlquery =@sqlquery+ '
		AND (  '+ @singlevoilationOpr+  '  (ISNULL(sum(ISNULL(tva.fineamount,0)),0) '+ CONVERT (VARCHAR(10),@singleviolationrelop) +'CONVERT(MONEY,'+ CONVERT(VARCHAR(10),@singleviolation)+') AND count(tva.ViolationNumber_PK)=1))
		  '                                

	-- DOUBLE VIOLATION FINE AMOUNT FILTER.....
	-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE VIOLATIONS 
	-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...
	IF(@doubleviolation<>0 AND @singleviolation=0 )                                
		SET @sqlquery =@sqlquery + '
		AND ('+ @doublevoilationOpr+  '   (ISNULL(sum(ISNULL(tva.fineamount,0)),0) '+CONVERT (VARCHAR (10),@doubleviolationrelop) +'CONVERT (MONEY,'+ CONVERT(VARCHAR(10),@doubleviolation) + ')AND count(tva.ViolationNumber_PK)=2) )
		  '                                                    

	-- BOTH SINGLE & DOUBLE VIOLATION FINE AMOUNT FILTER.....
	-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE AND SINGLE VIOLATIONS 
	-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...                                
	IF(@doubleviolation<>0 AND @singleviolation<>0)                                
		SET @sqlquery =@sqlquery+ '
		AND ('+ @singlevoilationOpr+  '  (ISNULL(sum(ISNULL(tva.fineamount,0)),0) '+ CONVERT (VARCHAR(10),@singleviolationrelop) +'CONVERT(MONEY,'+ CONVERT(VARCHAR(10),@singleviolation)+') AND count(tva.ViolationNumber_PK)=1))                                
		AND ('+ @doublevoilationOpr+  '  (ISNULL(sum(ISNULL(tva.fineamount,0)),0) '+CONVERT (VARCHAR (10),@doubleviolationrelop) +'CONVERT (MONEY,'+ CONVERT(VARCHAR(10),@doubleviolation) + ') AND count(tva.ViolationNumber_PK)=2))
			'                                  


	-- EXCLUDE THE LETTERS THAT HAVE BEEN ALREADY PRINTED....
	SET @sqlquery =@sqlquery+ '  

	-- GETTING THE RESULT SET IN TEMP TABLE.....
	SELECT distinct  a.ViolCount, a.Total_Fineamount, a.mdate ,a.Flag1, a.recordid 
	INTO #temp1  FROM #temp a, tblticketsviolationsarchive tva WITH(NOLOCK)
	WHERE a.recordid  = tva.recordid
	AND tva.violationstatusid <> 80 
	 '                        
	-- VIOLATION DESCRIPTION FILTER.....
	-- EXCLUDE THE LETTER IF SPECIFIED VIOLATION IS THE ONLY VIOLATION IN THE LETTER....
	IF(@violadesc <> '')
		BEGIN
			-- NOT LIKE FILTER.......
			IF(charindex('not',@violaop)<> 0 )              
				BEGIN
					SET @sqlquery=@sqlquery+' 
					AND	 not ( a.violcount =1  AND ('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ 'like')+'))'           
				END
			ELSE
				BEGIN
					SET @sqlquery=@sqlquery+' 
					AND	 ( a.violcount =1  AND ('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ 'like')+'))'           			
				END
		END
	-- SUMMARIZING THE TOTAL NUMBER OF LETTERS......
	-- GETTING LIST DATE WISE TOTAL NUMBER OF LETTERS....

	SET @sqlquery2 = ' delete FROM #temp1 WHERE recordid in (
	SELECT v.recordid FROM tblticketsviolations v WITH(NOLOCK) INNER JOIN tbltickets t WITH(NOLOCK) 
	on t.ticketid_pk = v.ticketid_pk AND t.activeflag = 1 
	INNER JOIN #temp1 a on a.recordid = v.recordid) 
	
	INSERT INTO #temp1 (ViolCount, Total_Fineamount, mdate,Flag1, recordid)

	SELECT t.ViolCount, t.Total_Fineamount, t.mdate, ISNULL(tna.AddressStatus2,''N''), tna.RowId
	FROM tblNewAddresses tna INNER JOIN #temp1 t ON tna.RecordId = t.Recordid
			AND tna.Address2 <> tna.Address1
	WHERE ISNULL(tna.IsNoNewAddressMarked, 0) = 0
	AND LEN(ISNULL(tna.Address2, '''')) > 0 
	AND LEN(ISNULL(tna.city2, '''')) > 0
	AND ISNULL(tna.isMailerSentOnAddress2, 0) = 0
	AND datediff(day, tna.InsertDate2 , @startListdate)<=0
	AND datediff(day, tna.InsertDate2, @endlistdate) >= 0
	 
	ALTER TABLE #temp1
	Add donotmailflag BIT NOT NULL Default 0
	
	UPDATE t
	SET t.donotmailflag = 1
	FROM tblNewAddresses tna INNER JOIN #temp1 t ON tna.RecordId = t.RecordId
	INNER JOIN tbl_hts_DoNotMail_Records r	ON LTRIM(RTRIM(ISNULL(tna.LastName , ''''))) = LTRIM(RTRIM(ISNULL(r.LastName, '''')))
	AND LTRIM(RTRIM(ISNULL(tna.Address1 , ''''))) = LTRIM(RTRIM(ISNULL(r.[Address], '''')))
	AND LTRIM(RTRIM(ISNULL(tna.City1 , ''''))) = LTRIM(RTRIM(ISNULL(r.City, ''''))) 
	AND LEFT(LTRIM(RTRIM(ISNULL(tna.ZipCode1, ''''))),5) = LEFT(LTRIM(RTRIM(ISNULL(r.Zip, ''''))), 5)
	AND ISNULL(tna.State1, 0) = ISNULL(r.StateID_FK, 0)
	AND LEN(ISNULL(tna.Address1, '''')) > 0
	AND LEN(ISNULL(tna.City1, '''')) > 0
	 
	UPDATE t
	SET t.donotmailflag = 1
	FROM tblNewAddresses tna INNER JOIN #temp1 t ON tna.RecordId = t.RecordId
	INNER JOIN tbl_hts_DoNotMail_Records r 
	ON LTRIM(RTRIM(ISNULL(tna.Address1 , ''''))) = LTRIM(RTRIM(ISNULL(r.[Address], '''')))
	AND LTRIM(RTRIM(ISNULL(tna.City1 , ''''))) = LTRIM(RTRIM(ISNULL(r.City, ''''))) 
	AND LEFT(LTRIM(RTRIM(ISNULL(tna.ZipCode1, ''''))),5) = LEFT(LTRIM(RTRIM(ISNULL(r.Zip, ''''))), 5)
	AND ISNULL(tna.State1, 0) = ISNULL(r.StateID_FK, 0)
	AND LEN(ISNULL(r.FirstName,'''') + ISNULL(r.LastName,'''')) = 0	
	AND LEN(ISNULL(tna.Address1, '''')) > 0
	AND LEN(ISNULL(tna.City1, '''')) > 0
	
	UPDATE t
	SET t.donotmailflag = 1
	FROM tblNewAddresses tna INNER JOIN #temp1 t ON tna.RecordId = t.RecordId
	INNER JOIN tbl_hts_DoNotMail_Records r	ON LTRIM(RTRIM(ISNULL(tna.LastName , ''''))) = LTRIM(RTRIM(ISNULL(r.LastName, '''')))
	AND LTRIM(RTRIM(ISNULL(tna.Address2 , ''''))) = LTRIM(RTRIM(ISNULL(r.[Address], '''')))
	AND LTRIM(RTRIM(ISNULL(tna.City2 , ''''))) = LTRIM(RTRIM(ISNULL(r.City, ''''))) 
	AND LEFT(LTRIM(RTRIM(ISNULL(tna.ZipCode2, ''''))),5) = LEFT(LTRIM(RTRIM(ISNULL(r.Zip, ''''))), 5)
	AND ISNULL(tna.State2, 0) = ISNULL(r.StateID_FK, 0)
	AND LEN(ISNULL(tna.Address2, '''')) > 0
	AND LEN(ISNULL(tna.City2, '''')) > 0
	 
	UPDATE t
	SET t.donotmailflag = 1
	FROM tblNewAddresses tna INNER JOIN #temp1 t ON tna.RecordId = t.RecordId
	INNER JOIN tbl_hts_DoNotMail_Records r 
	ON LTRIM(RTRIM(ISNULL(tna.Address2 , ''''))) = LTRIM(RTRIM(ISNULL(r.[Address], '''')))
	AND LTRIM(RTRIM(ISNULL(tna.City2 , ''''))) = LTRIM(RTRIM(ISNULL(r.City, ''''))) 
	AND LEFT(LTRIM(RTRIM(ISNULL(tna.ZipCode2, ''''))),5) = LEFT(LTRIM(RTRIM(ISNULL(r.Zip, ''''))), 5)
	AND ISNULL(tna.State2, 0) = ISNULL(r.StateID_FK, 0)
	AND LEN(ISNULL(r.FirstName,'''') + ISNULL(r.LastName,'''')) = 0	
	AND LEN(ISNULL(tna.Address2, '''')) > 0
	AND LEN(ISNULL(tna.City2, '''')) > 0
	 
	' 

	SET @sqlquery2 =@sqlquery2+ ' 
	SELECT count(distinct recordid) as Total_Count, mdate 
	INTO #temp2 
	FROM #temp1 
	GROUP BY mdate 
	  
	-- GETTTING LIST DATE WISE TOTAL NUMBER OF PRINTABLE LETTERS...
	SELECT count(distinct recordid) as Good_Count, mdate 
	INTO #temp3
	FROM #temp1 
	WHERE Flag1 in (''Y'',''D'',''S'')   
	AND donotmailflag = 0    
	GROUP BY mdate   
	                                                      
	-- GETTING LIST DATE WISE TOTAL NUMBER OF NON-PRINTABLE LETTERS
	SELECT count(distinct recordid) as Bad_Count, mdate  
	INTO #temp4  
	FROM #temp1 
	WHERE Flag1  not in (''Y'',''D'',''S'') 
	AND donotmailflag = 0 
	GROUP BY mdate 
	                   
	-- GETTING LIST DATE WISE TOTAL NUMBER OF LETTERS THAT MARKED 
	-- AS BLOCKED ADDRESSES.....
	SELECT count(distinct recordid) as DonotMail_Count, mdate  
	INTO #temp5   
	FROM #temp1 
	WHERE donotmailflag=1 
	GROUP BY mdate

	-- GETTING ALL THE DATES IN TEMP TABLE THAT LIES BETWEEN THE 
	-- SELECTED DATE RANGE.......
	CREATE TABLE #letterdates (mdate DATETIME)
	while datediff(day, @startListdate, @endlistdate)>=0
	BEGIN
		insert INTO #letterdates (mdate) SELECT CONVERT(VARCHAR(10),@startlistdate,101)
		SET @startlistdate = dateadd(day, 1, @startlistdate)
	END
		
	-- OUTPUTTING THE REQURIED INFORMATION........         
	SELECT 	l.mdate as listdate,
			ISNULL(Total_Count,0) as Total_Count,
			ISNULL(Good_Count,0) Good_Count,  
			ISNULL(Bad_Count,0)Bad_Count,  
			ISNULL(DonotMail_Count,0)DonotMail_Count  
	FROM #letterdates l LEFT OUTER JOIN #Temp2 on #temp2.mdate = l.mdate LEFT OUTER JOIN #temp3 
	on #temp2.mdate=#temp3.mdate  
	LEFT OUTER JOIN #Temp4  
	on #temp2.mdate=#Temp4.mdate    
	LEFT OUTER JOIN #Temp5  
	on #temp2.mdate=#Temp5.mdate
	-- WHERE #temp3.Good_Count > 0
	order by l.mdate

	-- DROPPING THE TEMPORARY TABLES USED ABOVE.....
	DROP TABLE #temp DROP TABLE #temp1 DROP TABLE #temp2  DROP TABLE #temp3  DROP TABLE #temp4  DROP TABLE #temp5  DROP TABLE #letterdates
	'                                                      
	 
	-- CONCATENATING THE QUERY ....
	SET @sqlquery = @sqlquery + @sqlquery2
	
	--PRINT @sqlquery

	-- EXECUTING THE DYNAMIC SQL QUERY...
	EXEC sp_executesql @sqlquery , @sqlparam, @catnum, @LetterType, @crtid, @startListdate, @endListdate, @SearchType
END

GO
	GRANT EXECUTE ON [dbo].[usp_mailer_Get_HCC_Criminal_UpdatedAddress] TO dbr_webuser
GO


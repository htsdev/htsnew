USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTS_Get_New_SchedulePaymentDetail]    Script Date: 01/17/2012 19:11:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
        
ALTER procedure [dbo].[USP_HTS_Get_New_SchedulePaymentDetail]      
@TicketID as int            
as            
select ts.scheduleid, ts.PaymentDate,convert(varchar(20),ts.Amount) as Amount,datediff(day,getdate(),ts.paymentdate) as daysleft, 
tt.LanguageSpeak -- Adil 5987 06/11/2009 include client language for English/Spanish translation display 
-- Muhammad Ali 9949 01/17/2012 Added Language Speak colon.           
from tblschedulepayment  ts
INNER JOIN tblTickets tt
ON ts.TicketID_PK = tt.TicketID_PK           
where ts.ticketid_pk=@TicketID and ts.scheduleflag <>0          
order by ts.paymentdate 
----------------------------------------------------------------------------------------------      

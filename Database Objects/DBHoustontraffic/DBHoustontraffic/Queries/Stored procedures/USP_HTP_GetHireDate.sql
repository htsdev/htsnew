﻿/******   
Created by:  Noufil Khan  
Business Logic : this procedure is used to get hire date of the case
  
List of Parameters:   
	@ticketid : Case Number

List of Columns:    
	Hire Date : Hire Date Of Case
  
******/    

Create procedure [dbo].[USP_HTP_GetHireDate]  
@ticketid int  
as  
  
select isnull(min(RecDate),'1/1/1900') as hiredate from tblTicketsPayment   
where ticketid=@ticketid  
and paymentvoid=0 and paymenttype <> 8


go

grant execute on [dbo].[USP_HTP_GetHireDate]   to dbr_webuser

go
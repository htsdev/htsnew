/******   
Alter by:  Sabir  
Business Logic : this procedure is used to display case summry detail of a client  
List of Parameters:  
 @TicketID   : Ticket Id of a client    
 
  
******/    

ALTER procedure [dbo].[USP_HTS_GET_SummaryInfoDetail]       
 (                  
 @TicketId_pk int                  
 )                  
                  
as                  
                  
                
--SELECT CONVERT(varchar(17), tv.RefCaseNumber)+ '-' +  CONVERT(varchar(10), tv.SequenceNumber)AS RefNum,             
SELECT CONVERT(varchar(17), tv.RefCaseNumber)  AS RefNum,             
 (v.Description +' ('+ vc.ShortDescription +')') AS ViolationDescription,     
  
 d.Description AS DispositionStatus,              
 isnull(o.FirstName +' '+o.LastName,'N/A') as officername,      
tv.CourtDateMain,   
 --Sabir Khan 7979 07/07/2010 type changed    
case when isnull(tv.CourtNumber,'0') != '0' then ('#' + CONVERT(varchar(10),isnull(tv.CourtNumber,''))) else '' end as CourtNumber,         
  
     
  
--(crt.ShortName+'('+tv.OscarCourtDetail+')') as ShortName,             
  
case  when isnull(tv.OscarCourtDetail,'')='' then crt.ShortName   
else  crt.ShortName+  '('+tv.OscarCourtDetail+')'   
end  as ShortName,    
      
        
 cvs.ShortDescription  ,             
 isnull(tv.casenumassignedbycourt,'') as causeNum              
FROM tblTicketsViolations tv             
INNER JOIN                
 tblViolations v             
ON  tv.ViolationNumber_PK = v.ViolationNumber_PK             
inner JOIN                
 tblCaseDispositionstatus d             
ON  tv.violationstatusid = d.CaseStatusID_PK             
INNER JOIN                
 tblCourts crt             
ON  tv.CourtID = crt.Courtid             
INNER JOIN                
 tblCourtViolationStatus cvs             
ON  tv.CourtViolationStatusID = cvs.CourtViolationStatusID           
  
LEFT OUTER JOIN                            
    dbo.tblViolationCategory vc ON v.CategoryID = vc.CategoryId   
  
 inner join tbltickets t  
 on tv.ticketid_pk =t.ticketid_pk      
left join tblofficer o  
 on tv.ticketofficernumber = o.officernumber_pk  
  
WHERE   tv.TicketID_PK = @TicketId_pk  
go 
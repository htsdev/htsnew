USE TrafficTickets
GO  
/********  
Created By:  
Business Logic:  
List of Parameters:   
 @LetterType: Unique letter id  
List of Columns:  
 lettertype:  Uniqe letter id  
 courtcategory:  Court Categories ID    
 lettername:   Name of letter to be print.  
 courtcategoryname: Court Category Name  
 reportfilename: File name of report  
 count_spname: Stored proceedure name for counting  
 data_spname: Stored Proceedure name for data  
 StartDate: Start date for search data  
    EndDate: End date for search data  
 SearchType: Type of search   
 CanPrintForOffDays: If true means off days will be counted and vice versa.
********/

-- USP_Mailer_Get_LetterInfo 30,'12/27/2013'

ALTER PROCEDURE [dbo].[USP_Mailer_Get_LetterInfo] 
(@LetterType INT, @LetterDate DATETIME = NULL)
AS
	IF (@LetterDate IS NULL)
	    SET @LetterDate = GETDATE()  
	
	
	SELECT DISTINCT TOP 1 
	       l.letterid_pk AS lettertype,
	       l.courtcategory AS courtcategoryid,
	       l.lettername,
	       c.courtcategoryname,
	       l.reportfilename,
	       l.count_spname,
	       l.data_spname,
	       0 AS querytype,
	       DATEADD(DAY, l.DefaultStartDate, @LetterDate) AS StartDate,
	       --Zeeshan Haider 11579 12/23/2013 If DMC Appearance Day-Of Mailer is run on Friday, by default date range will be extended till next Monday
	       CASE 
	            WHEN @LetterType = 30 AND DATEPART(dw, GETDATE()) = 6 THEN 
	                 DATEADD(DAY, 3, GETDATE())
	            ELSE DATEADD(DAY, l.DefaultEndDate, @LetterDate)
	       END AS EndDate,
	       l.SearchType,
	       l.CanPrintForOffDays,
	       l.OffDayType
	FROM   dbo.tblletter l
	       INNER JOIN dbo.tblcourtcategories c
	            ON  l.courtcategory = c.courtcategorynum
	WHERE  l.letterid_pk = @lettertype
	       AND l.isactive = 1 

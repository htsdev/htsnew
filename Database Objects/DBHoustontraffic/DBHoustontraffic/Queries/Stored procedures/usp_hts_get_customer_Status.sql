SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_hts_get_customer_Status]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_hts_get_customer_Status]
GO

  

CREATE PROCEDURE [dbo].[usp_hts_get_customer_Status]        
@LastName varchar(50),        
@DLNumber varchar(20),      
@DOB varchar(15),      
@ZipCode varchar(15)      
as        

declare @sqlQuery varchar(4000)

set @sqlQuery = '   

SELECT     t.TicketID_PK,t.Firstname,t.Lastname, t.Address1, t.Address2, st2.State, t.City, t.zip,
 convert(nvarchar,tv.RefCaseNumber) AS TicketNumber,
 ''# ''+ isnull(tv.CourtNumbermain,'''') as CourtNumber,
 cvs.Description AS Status,
 c.shortcourtname AS CourtLocation,
  
 case when len(v.Description) > 20 then (left(v.Description,20) +''....'') else v.Description end  AS ViolationDescription,
 
 tv.CourtDateMain AS CourtDate,
 c.Address +'', '' +c.City +'', ''+st.State+'', '' + c.Zip as CourtAddress,
 t.Address1+'', '' +t.City +'', ''+st2.State+'', ''+t.Zip as ClientAddress,
 tv.courtviolationstatusidmain as ViolationStatusID,
 c.ShortName,
 c.CourtID,
v.Description AS ViolationDescriptionToolTip 
FROM  tblTickets AS t INNER JOIN 
                      tblTicketsViolations AS tv ON t.TicketID_PK = tv.TicketID_PK INNER JOIN
                      tblCourts AS c ON tv.CourtID = c.Courtid INNER JOIN
                      tblCourtViolationStatus AS cvs ON tv.CourtViolationStatusIDmain = cvs.CourtViolationStatusID INNER JOIN
                      tblViolations AS v ON tv.ViolationNumber_PK = v.ViolationNumber_PK INNER JOIN
                      tblState AS st ON c.State = st.StateID INNER JOIN
                      tblState AS st2 ON t.Stateid_FK = st2.StateID
WHERE
	((t.Activeflag = 1) 
		AND (tv.CourtViolationStatusIDMain <> 80) 
			AND t.Lastname ='''+ @LastName+''') AND '

if (@DLNumber <> '')
begin
set @sqlQuery = @sqlQuery + ' t.DLNumber = '''+@DLNumber + ''''
end
else
begin
set @sqlQuery = @sqlQuery + ' DateDiff(day,t.DOB,convert(datetime,'''+@DOB+'''))=0 and t.zip like('''+@ZipCode+'%'')'
end
set @sqlQuery = @sqlQuery + ' 
order by   t.TicketID_PK desc,tv.CourtID asc, tv.CourtDateMain desc'
 
--print (@sqlQuery)
exec(@sqlQuery)


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


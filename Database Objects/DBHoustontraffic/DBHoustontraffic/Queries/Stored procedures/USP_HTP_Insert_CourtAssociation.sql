﻿  /************************************************************
 * Code formatted by SoftTree SQL Assistant © v4.0.34
 * Time: 3/3/2010 5:07:37 PM
 ************************************************************/

CREATE PROCEDURE USP_HTP_Insert_CourtAssociation
	@CourtID INT,
	@StatusID INT,
	@RoomID INT,
	@TimeID INT
	
AS
BEGIN
    IF NOT EXISTS(
           SELECT id
           FROM   CourtAssociation ca
           WHERE  ca.CourtID = @CourtID
                  AND ca.CourtStatusID=@StatusID
                  AND ca.CourtRoomNumberID = @RoomID
                  AND ca.CourtTimeID = @TimeID
       )
    BEGIN
        INSERT INTO CourtAssociation
          (            
            CourtID,
            CourtTimeID,
            CourtRoomNumberID,
            CourtStatusID,
            AllowBond
          )
        VALUES
          (
           @CourtID /* CourtID	*/,
            @TimeID /* CourtTimeID	*/,
            @RoomID/* CourtRoomNumberID	*/,
            @StatusID/* CourtStatusID	*/,
            1
          )
    END
END

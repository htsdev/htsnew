

/****** 
Created by:		Tahir Ahmed
Business Logic:	The procedure is used by LMS application to get the number of printable letter
				count for the HMC DLQ letter for the selected date range.
				
List of Parameters:
	@catnum:		Category number of the selected letter type, if printing for 
					all courts of the selected court category
	@LetterType:	Selected letter type in LMS
	@CrtId:			Court id of the selected court category if printing for individual court hosue.
	@startListdate:	Starting list date for Arraignment letter.
	@endListDAte:	Ending list date for Arraignment letter.
	@SearchType:	Flag that identifies if searching by court date or by list date. 0= list date, 1 = court date

List of Columns:	
	Total_Count:	Total number of letters exists for the selected date range.
	Good_Count:		Total number of letters that have valid addresses
	Bad_Count:		Total Number of letter that have undeliverable addresses
	DonotMail_Count:Total number of letter for addresses that had been marked to stop sending letters.
	

******/


--  usp_mailer_Get_DLQ_Letter 1, 19, 1, '05/18/2012', '05/18/2012', 0  
ALTER Procedure [dbo].[usp_mailer_Get_DLQ_Letter]
 (  
 @catnum  int,  
 @LetterType  int,  
 @crtid   int,                                                   
 @startListdate  Datetime,  
 @endListdate  DateTime,                                                        
 @SearchType     int                                               
 )  
  
as                                                                
  SET NOCOUNT ON ;                                                            
declare @officernum varchar(50),   
 @officeropr varchar(50),   
 @tikcetnumberopr varchar(50),   
 @ticket_no varchar(50),   
 @zipcode varchar(50),   
 @zipcodeopr varchar(50),   
 @fineamount money,  
 @fineamountOpr varchar(50) ,  
 @fineamountRelop varchar(50),  
 @singleviolation money,   
 @singlevoilationOpr varchar(50) ,   
 @singleviolationrelop varchar(50),   
 @doubleviolation money,  
 @doublevoilationOpr varchar(50) ,   
 @doubleviolationrelop varchar(50),   
 @count_Letters int,  
 @violadesc varchar(500),  
 @violaop varchar(50),  
 @sqlquery nvarchar(MAX)  ,  
 @sqlquery2 nvarchar(max)  ,  
 @sqlParam nvarchar(1000)  
  
-- SETTING PARAMETER LIST AND ORDER FOR DYNAMIC SQL.....  
select @sqlparam = '@catnum int, @LetterType int, @crtid int, @startListdate Datetime,    
      @endListdate DateTime, @SearchType int'  
  
select  @sqlquery ='',  @sqlquery2 = ''  

-- GETTING DIFFERENT FILTER TYPES AND THIER VALUES IN THE LOCAL VARIABLES
-- IF ASSOCIACTED WITH THE LETTER.......                                                                                               
Select @officernum=officernum,  
 @officeropr=oficernumOpr,  
 @tikcetnumberopr=tikcetnumberopr,  
 @ticket_no=ticket_no,  
 @zipcode=zipcode,  
 @zipcodeopr=zipcodeLikeopr,  
 @singleviolation =singleviolation,  
 @singlevoilationOpr =singlevoilationOpr,   
 @singleviolationrelop =singleviolationrelop,   
 @doubleviolation = doubleviolation,  
 @doublevoilationOpr=doubleviolationOpr,  
 @doubleviolationrelop=doubleviolationrelop,  
 @violadesc=violationdescription,  
 @violaop=violationdescriptionOpr ,   
 @fineamount=fineamount ,  
 @fineamountOpr=fineamountOpr ,  
 @fineamountRelop=fineamountRelop    
from  tblmailer_letters_to_sendfilters  WITH(NOLOCK)  
where  courtcategorynum=@catnum   
and  Lettertype=@LetterType   
                                                        
-- MAIN QUERY....
-- BOND DATE WISE NUMBER OF DISTINCT NON-CLIENTS
-- FOR THE SELECTED DATE RANGE ONLY DLQ CASES
-- INSERTING VALUES IN A TEMP TABLE....
set  @sqlquery=@sqlquery+'                                            
 Select distinct    
	convert(datetime , convert(varchar(10),tva.bonddate,101)) as mdate, 
	flag1 = isnull(ta.Flag1,''N'') ,    
	ta.donotmailflag,       
	ta.recordid , 
	left(ta.zipcode,5) + ltrim(ta.midnumber) as zipmid, 
	tva.ViolationNumber_PK , 
	case when isnull(tva.fineamount,0)=0 then 100 else tva.fineamount end as FineAmount, 
	tva.violationdescription
into #temp                                                              
FROM dbo.tblCourtViolationStatus tcvs with(nolock)                       
INNER JOIN dbo.tblTicketsViolationsArchive TVA with(nolock)                       
ON tcvs.CourtViolationStatusID = TVA.violationstatusid INNER JOIN                         
dbo.tblTicketsArchive TA ON TVA.recordid = TA.recordid  

-- ONLY DLQ CASES....
WHERE tcvs.courtviolationstatusid = 146

-- FOR THE SELECTED DATE RANGE...
and datediff(day, tva.bonddate, @startlistdate)<=0
and datediff(day, tva.bonddate, @endlistdate)>=0
and isnull(ta.ncoa48flag,0) = 0
--Muhammad Muneer 8465 10/28/2010 added the new functionality that is the null checks for first name and the last name
and LEN(ISNULL(RTRIM(LTRIM(ta.lastname)),''''))<>0
and LEN(ISNULL(RTRIM(LTRIM(ta.firstname)),''''))<>0

--Rab Nawaz Khan 9119 04/04/2011 added attorney association logic.
and len(isnull(tva.attorneyname, '''')+isnull(tva.barcardnumber,'''')) = 0 
and CHARINDEX(''KUBOSH'',isnull(tva.BondingCompanyName,''''))=0
-- Sabir Khan 10245 05/03/2012
and isnull(ta.IsIncorrectMidNum,0) = 0
'  

-- IF PRINTING FOR ALL COURTS OF THE SELECTED COURT CATEGORY...  
if(@crtid = @CATNUM )                                      
	set @sqlquery=@sqlquery+' and tva.courtlocation In (Select courtid from tblcourts with(nolock) where courtcategorynum = @crtid )'                                          

-- ELSE PRINTING FOR THE INDIVIDUAL SELECTED COURT OF THE CATEGORY....
ELSE 
	set @sqlquery =@sqlquery +' and tva.courtlocation = @crtid'   
                                                          
-- LMS FILTERS SECTION:
-----------------------------------------------------------------------------------------------------------------------------------------------------
-- IF THERE IS ANY FILTER SPECIFIED TO INCLUDE/EXCLUDE ONLY PARTICULAR OFFICER NUMBERS....
if(@officernum<>'')                                                          
 set @sqlquery =@sqlquery + '  and ta.officerNumber_Fk '  + @officeropr  +'('+ @officernum+')'                                                           

-- IF THERE IS ANY FILTER DEFINED FOR INCLUDE/EXCLUDE VIOLATION DESCRIPTIONS...                                                                                                                
if(@ticket_no<>'')                          
 set @sqlquery=@sqlquery + ' and ('+  dbo.Get_String_Concat_With_op(''+@ticket_no+'' , 'tva.TicketNumber_PK' ,+ ''+ @tikcetnumberopr) +')'                                                                

-- ZIP CODE FILTER......
-- INCLUDE/EXCLUDE THE SPECIFIED ZIP CODES.....                                    
if(@zipcode<>'')                                                                                
 set @sqlquery=@sqlquery+ ' and ('+ dbo.Get_String_Concat_With_op(''+@zipcode+'' , 'left(ta.zipcode,3)' ,+ ''+ @zipcodeopr) +')'                                     

-- FINE AMOUNT FILTER.....
-- INCLUDE ONLY THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT....
if(@fineamount<>0 and @fineamountOpr<>'not'  )                                    
 set @sqlquery =@sqlquery+ '  
        and tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                                         

-- EXCLUDE THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT.....
if(@fineamount<>0 and @fineamountOpr = 'not'  )                                    
 set @sqlquery =@sqlquery+ '  
        and tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                                         
-----------------------------------------------------------------------------------------------------------------------------------------------------

-- ECLUDE THE LETTERS THAT HAVE BEEN ALREADY PRINTED......
-- OR FTA LETTER PRINTED IN PAST WEEK FOR THE SAME RECORD
set @sqlquery =@sqlquery+ ' 
and ta.recordid not in (      
	-- Rab Nawaz 9951 12/21/2011 Exclude already printed letter                                                               
	select	n.recordid from tblletternotes n with(nolock) 
	where 	n.lettertype =@LetterType   
	union
	-- exclude FTA letters printed between last seven days...
	-- Rab Nawaz 9951 12/21/2011 Exclude the letters which prited in last 3 days
	select nts.recordid from tblletternotes nts with(nolock)  
	where nts.lettertype IN (9, 12, 13, 25, 41, 58)
	AND datediff(day, nts.recordloaddate, dbo.fn_getprevbusinessday(GETDATE(),3)) <=0
	)'


-- GETTING VIOL COUNT & TOTAL FINE AMOUNT FOR EACH CLIENT...
set @sqlquery =@sqlquery+ '
select zipmid , 
	count(violationnumber_pk) as violcount,
	isnull(sum(isnull(fineamount,0)),0) as total_fineamount
into #temptable
from #temp  
group by zipmid

alter table #temp add violcount int , total_fineamount money

update a 
set a.violcount = b.violcount, 
	a.total_fineamount = b.total_fineamount
from #temp a, #temptable b where a.zipmid = b.zipmid
'

-- GETTING RECORDS IN TEMP TABLE.....
set @sqlquery =@sqlquery+ '
select distinct * into #temp1 from #temp where 1 = 1
'

-- SINGLE VIOLATION FINE AMOUNT FILTER....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE SINGLE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT
if(@singleviolation<>0 and @doubleviolation=0)                                
	set @sqlquery =@sqlquery+ '
	and (  '+ @singlevoilationOpr+  '  (total_fineamount '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and violcount=1))
      '                                

-- DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...
if(@doubleviolation<>0 and @singleviolation=0 )                                
	set @sqlquery =@sqlquery + '
	and ('+ @doublevoilationOpr+  '   (total_fineamount '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ')and violcount=2) )
      '                                                    

-- BOTH SINGLE & DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE AND SINGLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...                                      
if(@doubleviolation<>0 and @singleviolation<>0)                                
	set @sqlquery =@sqlquery+ '
	and ('+ @singlevoilationOpr+  '  (total_fineamount '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and violcount=1))                                
    and ('+ @doublevoilationOpr+  '  (total_fineamount '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ') and violcount=2))
        '                                  
                              
-- VIOLATION DESCRIPTION FILTER.....
-- EXCLUDE THE LETTER IF SPECIFIED VIOLATION IS THE ONLY VIOLATION IN THE LETTER....
if(@violadesc <> '')
	BEGIN
		-- NOT LIKE FILTER.......
		if(charindex('not',@violaop)<> 0 )  
			BEGIN
				set @sqlquery=@sqlquery+' 
				-- Rab Nawaz Khan 9659 09/06/2011 Filter Setting has been changed 
				and	 not (  ( '+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  violationdescription  ' ,+ ''+ 'like')+'))'           	
			END  
		-- INCLUDE ONLY THOSE LETTER THAT SINGLE VIOLATION AND MATCHES THE SPECIFIED VIOLATION DESCRIPTION...
		-- LIKE FILTER.......
		--if(charindex('not',@violaop) =  0 )                
		ELSE
			BEGIN        
					set @sqlquery=@sqlquery+' 
					-- Rab Nawaz Khan 9659 09/06/2011 Filter Setting has been changed 
					and	 (  ( '+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  violationdescription  ' ,+ ''+ 'like')+'))'           		
			END
	END

  

-- SUMMARIZING THE TOTAL NUMBER OF LETTERS......
-- GETTING LIST DATE WISE TOTAL NUMBER OF LETTERS....
set @sqlquery2=@sqlquery2 +'  

-- tahir 4624 09/01/2008 exclude client cases
delete from #temp1 where recordid in (
select v.recordid from tblticketsviolations v with(nolock) inner join tbltickets t with(nolock)
on t.ticketid_pk = v.ticketid_pk and t.activeflag = 1 
inner join #temp1 a on a.recordid = v.recordid
)
-- end 4624

Select count(distinct zipmid) as Total_Count, mdate   
into #temp2   
from #temp1   
group by mdate   
    
-- GETTTING LIST DATE WISE TOTAL NUMBER OF PRINTABLE LETTERS...
Select count(distinct zipmid) as Good_Count, mdate   
into #temp3  
from #temp1   
where Flag1 in (''Y'',''D'',''S'')     
and donotmailflag = 0      
group by mdate     

-- GETTING LIST DATE WISE TOTAL NUMBER OF NON-PRINTABLE LETTERS                                                        
Select count(distinct zipmid) as Bad_Count, mdate    
into #temp4    
from #temp1   
where Flag1  not in (''Y'',''D'',''S'')   
and donotmailflag = 0   
group by mdate   

-- GETTING LIST DATE WISE TOTAL NUMBER OF LETTERS THAT MARKED                      
Select count(distinct zipmid) as DonotMail_Count, mdate    
into #temp5     
from #temp1   
where donotmailflag=1   
group by mdate

-- GETTING ALL THE DATES IN TEMP TABLE THAT LIES BETWEEN THE 
-- SELECTED DATE RANGE.......
create table #letterdates (mdate datetime)
while datediff(day, @startListdate, @endlistdate)>=0
begin
	insert into #letterdates (mdate) select convert(varchar(10),@startlistdate,101)
	set @startlistdate = dateadd(day, 1, @startlistdate)
end
 
   
-- OUTPUTTING THE REQURIED INFORMATION........           
Select 	l.mdate as listdate,
        isnull(Total_Count,0) as Total_Count,
        isnull(Good_Count,0) Good_Count,  
        isnull(Bad_Count,0)Bad_Count,  
        isnull(DonotMail_Count,0)DonotMail_Count  
from #letterdates l left outer join #Temp2 on #temp2.mdate = l.mdate left outer join #temp3 
on #temp2.mdate=#temp3.mdate  
left outer join #Temp4  
on #temp2.mdate=#Temp4.mdate    
left outer join #Temp5  
on #temp2.mdate=#Temp5.mdate                                                       
order by l.mdate
select count(distinct zipmid) from #temp1 where Flag1 in (''Y'',''D'',''S'')     
and donotmailflag = 0 

-- DROPPING THE TEMPORARY TABLES USED ABOVE.....
drop table #temp drop table #temp1 drop table #temp2  drop table #temp3  drop table #temp4  drop table #temp5  drop table #letterdates '                                                        
                                                        
--print @sqlquery  + @sqlquery2                                 

-- CONCATENATING THE QUERY ....
set @sqlquery = @sqlquery + @sqlquery2  

-- EXECUTING THE DYNAMIC SQL QUERY...
exec sp_executesql @sqlquery , @sqlparam, @catnum, @LetterType, @crtid, @startListdate, @endListdate, @SearchType


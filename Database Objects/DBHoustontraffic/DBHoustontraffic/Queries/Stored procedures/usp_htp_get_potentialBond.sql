﻿set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go



/*************************************************************************
	Created By : Afaq Ahmed
	Task ID :8521
	Created Date : 12/13/2010
	Bussiness Logic: 1.	Client should be bonded.
					 2.	Reminder call status should be Pending, Left Message, 
						No Answer, Wrong Number or No Response.
					 3.	Court date should be in following three business days.
	Parameter:
		N/A
				
 	Column Return: TicketID_PK,RefCaseNumber,casenumassignedbycourt,Lastname,
 				   Firstname,ShortName,CourtDateMain,ViolationDescription,
 				   Contact1,Contact2,Contact3
 *************************************************************************/

-- [dbo].[usp_htp_get_potentialBond] 0
ALTER PROCEDURE [dbo].[usp_htp_get_potentialBond]
	@showAll BIT = 0
AS
	SELECT tt.TicketID_PK,
	       ttv.RefCaseNumber,
	       ttv.casenumassignedbycourt,
	       tt.Lastname,
	       tt.Firstname,
	       tc.ShortName,		      
	       dbo.fn_DateFormat(ttv.CourtDateMain, 5, '/', ':', 1) AS CourtDateMain, -- kashif 8771 03/07/2011 standardize date format
	       --Farrukh 8521 06/17/2011 Change ttv.ViolationDescription to m.Description
	       m.Description AS ViolationDescription,
	       --ttv.ViolationDescription,
	       ttv.TicketsViolationID,
	       CONVERT(
	           VARCHAR(20),
	           ISNULL(dbo.formatphonenumbers(tt.Contact1), '')
	       ) + '(' + ISNULL(LEFT(tblContactstype_1.Description, 1), '') + ')' AS 
	       contact1,
	       CONVERT(
	           VARCHAR(20),
	           ISNULL(dbo.formatphonenumbers(tt.Contact2), '')
	       ) + '(' + ISNULL(LEFT(tblContactstype_2.Description, 1), '') + ')' AS 
	       contact2,
	       CONVERT(
	           VARCHAR(20),
	           ISNULL(dbo.formatphonenumbers(tt.Contact3), '')
	       ) + '(' + ISNULL(LEFT(tblContactstype_3.Description, 1), '') + ')' AS 
	       contact3
	FROM   tblTickets tt
		   LEFT OUTER JOIN dbo.tblContactstype AS tblContactstype_3
	            ON  tt.ContactType3 = tblContactstype_3.ContactType_PK
	       LEFT OUTER JOIN dbo.tblContactstype AS tblContactstype_2
	            ON  tt.ContactType2 = tblContactstype_2.ContactType_PK
	       LEFT OUTER JOIN dbo.tblContactstype AS tblContactstype_1
	            ON  tt.ContactType1 = tblContactstype_1.ContactType_PK
	       INNER JOIN tblTicketsViolations ttv
	            ON  tt.TicketID_PK = ttv.TicketID_PK
	       INNER JOIN tblCourts tc
	            ON  tc.Courtid = ttv.CourtID
	       INNER JOIN tblContactstype ct1
	            ON  tt.ContactType1 = ct1.ContactType_PK
	            --Farrukh 8521 06/17/2011 included dbo.tblViolations for violation description
		   INNER JOIN  dbo.tblViolations m
				ON m.ViolationNumber_PK = ttv.ViolationNumber_PK
	       -- Rab Nawaz Khan 8879 03/30/2011 Disposed clients excluded
		   INNER JOIN tblCourtViolationStatus csv 
				ON csv.CourtViolationStatusID = ttv.CourtViolationStatusIDmain
	WHERE  DATEDIFF(DAY, ttv.CourtDateMain, GETDATE() + 1) <= 0
	       AND DATEDIFF(
	               DAY,
	               ttv.CourtDateMain,
	               dbo.fn_HTS_GET_Nth_NextBusinessDay(GETDATE(), CASE WHEN @showAll = 0 THEN 3 ELSE 5 END)
	           ) >= 0
	       AND ISNULL(ttv.ReminderCallStatus, 0) != 1  --Sabir Khan 8879 04/01/2011 set reminder call status
	       AND tt.BondFlag = 1
	       AND tt.Activeflag = 1
	       -- Rab Nawaz Khan 8879 03/30/2011 Disposed clients excluded
		   AND csv.CategoryID <> 50
	
	ORDER BY ttv.CourtDateMain, ttv.TicketID_PK ASC -- Adil 8521 07/19/2011 Including Ticket ID in ordering. 



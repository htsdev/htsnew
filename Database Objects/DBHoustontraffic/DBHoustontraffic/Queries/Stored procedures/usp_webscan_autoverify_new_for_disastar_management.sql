SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_webscan_autoverify_new_for_disastar_management]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_webscan_autoverify_new_for_disastar_management]
GO

  
--select * from tbl_webscan_ocr where picid in (571,572)                
                
-- usp_webscan_autoverify_new 331,3991                
                
create Procedure [dbo].[usp_webscan_autoverify_new_for_disastar_management]                 
                
@batchid int,                
@empid  int                
                
                
as                
                
declare @loopcounter as int                
                
create table #temp                
(                
 counter int IDENTITY(1,1) NOT NULL,                
 PicID int,                           
 CauseNo varchar(50),                           
 CourtDate datetime,                           
 CourtTime  varchar(15),                           
 CourtNo int,                           
 Status varchar(50),                           
 Location varchar(50),                           
 CourtDateScan  datetime    ,                      
 EmployeeID int                
)                
                
insert into #temp select o.PicID,o.CauseNo,o.newcourtdate as courtdate,o.Time as courttime,o.CourtNo,o.Status,o.Location,o.newcourtdate+' '+o.Time as courtdatescan,@empid as employeeid                
from tbl_webscan_ocr o inner join                 
  tbl_webscan_pic p on                 
  o.picid=p.id inner join                 
  tbl_webscan_batch b on                
  b.batchid=p.batchid                
where o.checkstatus=3 and                
  b.batchid=@batchid                
                
set @loopcounter=(select count(*) from #temp)                
                
while @loopcounter >=1                
begin                
                
  declare @PicID int                
  declare @CauseNo varchar(50)                
  declare @CourtDate as datetime                
  declare @CourtTime as varchar(15)                
  declare @CourtNo as int                  
  declare @Status as varchar(50)                
  declare @Location as varchar(50)                
  declare @CourtDateScan as datetime                
  declare @EmployeeID as int                
  declare @Msg int              
 ---               
  declare @tempC table (                          
  causeno varchar(50),              
  picid int,      
  batchid int,      
  empid int               
 )               
  declare @causenos varchar(50)                 
 ---              
                  
  select @PicID=PicID,@CauseNo=CauseNo,@CourtDate=courtdate,@CourtTime=courttime,@CourtNo=CourtNo,@Status=Status,@Location=Location,@courtdatescan=courtdatescan,@employeeid=employeeid                
  from #temp                
  where counter = @loopcounter                
                
  declare @TempCauseNo as varchar(50)                          
  select @TempCauseNo =casenumassignedbycourt from tblticketsviolations where casenumassignedbycourt=@CauseNo                          
  set @Msg=1                      
                        
  if    @TempCauseNo <> 'NULL'                          
   begin                          
                            
                                 
    declare @TempCID as varchar(3)                              
    declare @CourtNumScan as varchar(50)                              
                                  
    set @TempCID=@CourtNo                              
                                  
    if @TempCID='13' or @TempCID='14'                              
   begin                              
    set @CourtNumScan='3002'                              
   end                              
    else if @TempCID='18'                              
   begin                              
    set @CourtNumScan='3003'                              
   end                              
    else                              
   begin                              
    set @CourtNumScan='3001'                              
   end                              
                                
    declare @tempCVID int                              
                          
    --Select @tempCVID=courtviolationstatusid from tblcourtviolationstatus where description=@Status                              
    if @Status='JURY TRIAL'             
  set @tempCVID=26            
            
 else if @Status='JUDGE'          
  set @tempCVID=103            
            
 else if @Status='PRETRIAL'             
  set @tempCVID=101            
           
 else if @Status='ARRAIGNMENT'             
 set @tempCVID=3           
            
    declare @TodayDate as datetime                              
    set @TodayDate=getdate()                      
                        
    begin                              
      
                 
 --              
 insert into @tempC select @CauseNo,@PicID,@batchid,@empid      
     
 --              
                               
     update   tbl_webscan_ocr                                  
   set                                  
                     
   checkstatus  =4                       
                           
   where                                  
   picid=@PicID                      
                           
                        
                           
    set @Msg=2                       
    end                      
                        
   end                  
                    
                  
 set @loopcounter=@loopcounter-1                
end                
              
select causeno,picid,batchid,empid from @tempC            
  
  
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


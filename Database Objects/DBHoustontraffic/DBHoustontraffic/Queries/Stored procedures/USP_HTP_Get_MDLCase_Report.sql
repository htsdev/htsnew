﻿ USE [TrafficTickets]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
/****** 
Create by		: Zeeshan Haider
Created Date	: 10/09/2012
Task			: 10437

Business Logic : 
			This procedure retrieve MDL Case Report.			
List of Parameters:	
			@CaseNumber	MDL Case Number
			
USP_HTP_InsertUpdate_MDLCase 2197, '10/02/2012', 3991, 0			
******/

CREATE PROCEDURE [dbo].[USP_HTP_Get_MDLCase_Report]
	@CaseNumber VARCHAR(20)
AS
BEGIN
    SELECT tmd.REF,
           tmf.CauseNumber,
           tmf.Style,
           tmf.Judge,
           CONVERT(VARCHAR(15),tma.AssignedDate, 101) AS [AssignedDate], 
           tma.Attorney,
           tma.LawFirm,
           tma.Street_A AS [Address1],
           tma.Street_B AS [Address2],
           tma.City,
           tma.[State],
           tma.Zip,
           tma.Telephone,
           tma.Fax,
           tma.Email,
           tma.AttorneyType,
           tma.Plaintiff1,
           tma.Plaintiff2,
           tma.Plaintiff3
    FROM   tblMDLCaseDetail tmd
           INNER JOIN tblMDLCaseFile tmf
                ON  tmf.REF = tmd.REF
           INNER JOIN tblMDLCaseAttorney tma
                ON  tma.Ref = tmf.Ref
    WHERE  tmd.MDL_CASENUMBER = @CaseNumber
END

GO

GRANT EXECUTE ON [dbo].[USP_HTP_Get_MDLCase_Report] TO dbr_webuser
GO
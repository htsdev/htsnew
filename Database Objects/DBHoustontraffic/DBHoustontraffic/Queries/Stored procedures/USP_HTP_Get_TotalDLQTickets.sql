SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Sabir Khan
-- Create date: 12/01/2011
-- Task Id: 9933
-- Business Logic:	This procedure is used to get all DLQ tickets issued in the seleceted year.
-- List of Parameters :   
  
-- @year     : Year for FTA Tickets.   
-- =============================================


CREATE PROCEDURE [dbo].[USP_HTP_Get_TotalDLQTickets]
	@year VARCHAR(4)
AS
BEGIN
 DECLARE @Temp TABLE([Month] varchar(20), [Total Tickets] int, insertdate int)

    INSERT INTO @Temp ([Month], [Total Tickets],insertdate)
    SELECT DATENAME(MONTH, insertdate) AS [Month],
           COUNT(DISTINCT causenumber) AS [Total Tickets],
           MONTH(insertdate) AS insertdate           
    FROM   LoaderFilesArchive.dbo.tblDLQ
    WHERE  DATEDIFF(
               YEAR,
               InsertDate,
               CONVERT(DATETIME, LTRIM(RTRIM(LEFT(@year, 4))))
           ) = 0          
    GROUP BY
           DATENAME(MONTH, insertdate),
           MONTH(insertdate)
    
    SELECT [Month],
           [Total Tickets]
    FROM   @Temp
    ORDER BY
           insertdate ASC
    
  
END

GO
GRANT EXECUTE ON [dbo].[USP_HTP_Get_TotalDLQTickets] TO dbr_webuser

GO


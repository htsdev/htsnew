﻿/************************************************************
 * Created By : Noufil Khan 
 * Created Date: 11/1/2010 3:11:39 PM
 * Business Logic :
 * Parameters :
 * Column Return :
 ************************************************************/

alter PROCEDURE [dbo].[USP_HTP_UpdateNotes]
	@patientId INT,
	@notes VARCHAR(MAX),
	@EmployeeId INT
AS

	DECLARE @Employee_ShortName VARCHAR(10)	
	SELECT @Employee_ShortName = u.Abbreviation FROM Users.dbo.[User] u WHERE  u.UserId = @EmployeeId
	SET @notes = @notes + ' (' + dbo.fn_DateFormat(GETDATE(), 2, '/', ':', 1) + ' - ' + @Employee_ShortName + ')'
	
	UPDATE patient
	SET    Notes = isnull(Notes,'') + isnull(@notes,'')
	WHERE  PatientId = @patientId
GO
 
GRANT EXECUTE ON [dbo].[USP_HTP_UpdateNotes] TO dbr_webuser


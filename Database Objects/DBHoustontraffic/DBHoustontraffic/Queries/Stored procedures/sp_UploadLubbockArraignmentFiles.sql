
/****** Object:  StoredProcedure [dbo].[sp_UploadLubbockArraignmentFiles]  ******/

/*  
Updated By : Sabir Khan    
Business Logic : This procedure is used to upload HMC arrignment cases into database.
Parameter:
			@listdate : List date using current date.

*/

  
    
-- sp_UploadLubbockArraignmentFiles '02/23/2012'
ALTER procedure [dbo].[sp_UploadLubbockArraignmentFiles]     
 --declare 
@listdate varchar(12)    
--declaas
as 
set @listdate = getdate()
    
SET NOCOUNT ON     
    
declare @LoaderId tinyint, @CurrDate datetime    
    
SElecT @LoaderId = 1, @CurrDate = getdate()    
    
    
update  tbllubbock  set courtnum = 0 where isnumeric(courtnum) = 0    
update tbllubbock set officernum = 0 where isnumeric(officernum) = 0    
update tbllubbock set misc2 = replace(misc2,' ','')  
update tbllubbock set price = left(ltrim(price),10)

-- tahir 4918 10/06/2008
-- replace ticket numebr with cause number if lenght of ticket number
-- is less than 3 characters....
update tbllubbock 
set ticketnumber = misc2
where len(isnull(ticketnumber,'')) < 3
and len(isnull(misc2,'')) > 0

-- And if cause number is also not avaiable then do not upload.....
delete from tbllubbock where len(isnull(ticketnumber,'')) < 3
-- end 4915
------------------------------------------------------------------------
--Sabir Khan 9924 11/23/2011 Fixed officer number issue.
UPDATE tb
SET
	tb.OfficerNum = o.officernumber_pk
FROM tblLubbock tb WITH(NOLOCK) INNER JOIN tblOfficer o WITH(NOLOCK) ON o.FirstName = tb.officer_firstname AND o.LastName = tb.officer_lastname

declare @officernumber numeric
select @officernumber = max(officernumber_pk) + 1 from tblofficer WITH(NOLOCK)

DECLARE @counter INT
SET @counter = 1
DECLARE @maxCount INT
DECLARE @fname VARCHAR(200)
DECLARE @lname VARCHAR(200)
SELECT @maxCount = COUNT(Officernum) FROM tblLubbock WITH(NOLOCK) WHERE OfficerNum = 0 GROUP BY officer_firstname,officer_lastname
WHILE (@counter <= @maxCount) 
      BEGIN 
		SELECT DISTINCT TOP 1 @fname = officer_firstname,@lname = officer_lastname FROM tblLubbock WITH(NOLOCK) WHERE OfficerNum = 0
		UPDATE tblLubbock
		SET 
			OfficerNum = @officernumber
		WHERE officer_firstname = @fname AND officer_lastname = @lname			
		     
	    SET @counter = @counter + 1 
        SET @officernumber = @officernumber + 1
      END 
------------------------------------------------------------------------

-- INSERT NEW OFFICERS IN OFFICER DATABASE....
insert into tblofficer (officernumber_pk, firstname, lastname, officertype)
select distinct o.officernum, o.officer_firstname, o.officer_lastname, 'N/A'
from tbllubbock o WITH(NOLOCK)
where not exists (
	select * from dbo.tblofficer WITH(NOLOCK) where officernumber_pk =  o.officernum
	)

-- UPDATE NAMES OF EXISTING OFFICERS HAVING CONFLICTS....
update o
set o.firstname = c.officer_firstname,	
	o.lastname = c.officer_lastname
from tblofficer o WITH(NOLOCK), tbllubbock c WITH(NOLOCK)
where c.officernum = o.officernumber_pk
and ( c.officer_firstname <>  o.firstname or c.officer_lastname <>  o.lastname )
and len(isnull(c.officer_firstname,'') + isnull(c.officer_lastname,'')) > 0

------------------------------------------------------------------------------
--Sabir Khan 4413 02/27/2009 Update violation code in tbllubbock from tblviolations...   
update  a
set a.Violationcode = isnull((
				select top 1 ViolationCode 
				from tblviolations WITH(NOLOCK)
				where Description = a.Violation
				and Violationtype = 2
				and len(isnull(violationcode,'')) > 0
				and violationcode not like '99999%'
				), '999999999999')
from tbllubbock a WITH(NOLOCK)
WHERE LEN(ISNULL(a.violationcode,'')) = 0

-----------------------------------------------------------------------------
--Sabir Khan 4413 02/27/2009 Replace unll state with TX...
update tbllubbock set State = 'TX' where len(isnull(State,'')) = 0
-----------------------------------------------------------------------------


-- INSERT NEW VIOLATIONS IN TEMP TABLE.....    
select distinct violation as violations,Violationcode into #temp     
from tbllubbock WITH(NOLOCK)
where violationcode not in(    
		select distinct T.violationcode  from tbllubbock T WITH(NOLOCK), tblviolations V WITH(NOLOCK)   
		where ltrim(T.Violation) = V.description    
		and T.violationcode = V.violationcode    
		and V.violationtype = 2 
		)    
    
    
update tbllubbock set ticketnumber = misc2 where ticketnumber is null    
update tbllubbock set ticketnumber = ltrim(ticketnumber)    
    
   
insert into tblviolations    
(description,sequenceorder,IndexKey,IndexKeys,ChargeAmount,BondAmount,    
Violationtype,ChargeonYesflag,ShortDesc,ViolationCode,AdditionalPrice,courtloc)    
select violations,null,2,100,140,0,2,0,'None',violationcode,0,0 from #temp     
    
update V    
set causenumber = L.misc2,    
 UpdationLoaderId = @LoaderId    
from tbllubbock L WITH(NOLOCK), tblticketsviolationsarchive V WITH(NOLOCK)    
where L.ticketnumber = V.ticketnumber_pk    
and V.causenumber is null    

-------------------------------------------------------------------------------
-- Sabir Khan 9924 11/23/2011 Update Officer Number according to Officer Name
-------------------------------------------------------------------------------
update l
set l.officernum = o.OfficerNumber_PK
from tbllubbock l WITH(NOLOCK) inner join tblofficer o WITH(NOLOCK) 
on LTrim(RTrim(o.FirstName)) = Ltrim(RTrim(l.officer_firstname)) 
and LTrim(RTrim(o.LastName)) = LTrim(RTrim(l.officer_lastname))
-------------------------------------------------------------------------------
-- Sabir Khan 9924 11/23/2011 End
-------------------------------------------------------------------------------
    
--Sabir Khan 4413 02/27/2009 Get data from tbllubbock into temp table...
select  distinct ticketnumber,0 as violationnumber,convert(money,price) as fineamount, violation, violationcode, 
dbo.fn_getviolationstatus(status) as status, left(upper(firstname),20) as firstname, left(upper(lastname),20) as lastname, 
misc3 as bondamount, misc2 as causenumber,null as ftaissuedate, 0 as RecordID, 
midnumber, left(upper(initial),5) as initial,    
upper(left(address,50)) as address,upper(CITY) as city, ISNULL(s.stateid,45) as state, -- phone, Rab Nawaz Khan 10196 04/23/2012 Phone Numbers Excluded from uploading
ZIP, dbo.convertstringtodate(dob) as dob,null as dlnumber,    
dbo.convertstringtodate(violationdate)  as violationdate,    
dbo.convertstringtodate(courtdate) + ' ' + dbo.fn_convert_timestring_to_time(time) as courtdate ,    
'                                                             ' as officername,     
race= case race    
	when 'H' then 'Hispanic'    
	when 'W' then 'White'    
	when 'B' then 'Black'    
	else 'Asian'    
	end,
gender=case gender    
	when 'M' then 'Male'    
	else 'Female'    
	end,    

height = case len(isnull(height,'')) 
	when 3 then left(height,1) + ' ft ' + right(Height,2)
	when 2 then left(height,1) + ' ft ' + right(height,1)
	else null
	end ,
null as associatedcasenumber,     
@listdate as listdate,    
courtnum,    
dbo.fn_convert_timestring_to_time(time) as courttime,    
substring(barcode,10,2) as dp2,right(barcode,1) as dpc,    
null as crrt,    
courtid = case when courtnum IN ('13', '14') then 3002 when courtnum IN ('18') then 3003

--Abbas Qamar 10088  04-25-2012 adding the court number 20 for Montgomry HMC
when courtnum IN ('20') then 3108 ELSE 3001 END,    -- Adil 7685 04/09/2010 When CRT is 18 or 20 then assume Dairy Ashford as court
--End

DPV AS FLAG1,isnull(officernum,962528) as officernum ,    
offenselocation, offensetime    
into #temptbltickets    
from tbllubbock l LEFT OUTER JOIN tblState s ON s.[State] = l.[State]
------------------------------------------------------------------------
--Sabir Khan 4413 02/27/2009 replace null officer name with N/A in temp table...
update #temptbltickets set officername = ltrim(isnull(officername,'N/A'))    
    
update #temptbltickets     
set officername = isnull(O.firstname + ' ' + O.lastname,'N/A')    
 from #temptbltickets T, tblofficer O WITH(NOLOCK)   
where convert(int,T.officernum) = O.officernumber_pk  
-------------------------------------------------------------------------

alter table #temptbltickets add rowid int identity(1,1) 

select distinct causenumber, min(rowid) as rowid 
into #duplicateticket from #temptbltickets     
group by causenumber
having count(causenumber) > 1  


--Sabir Khan 4413 02/27/2009 remove duplicate cause numbers....
delete from #temptbltickets 
from #temptbltickets a inner join  #duplicateticket b
on a.causenumber =  b.causenumber
and a.rowid > b.rowid

-- remove if already exists in our database....
delete from #temptbltickets
from dbo.tblticketsviolationsarchive v 
inner join dbo.tblticketsarchive t on t.recordid = v.recordid 
inner join #temptbltickets a on a.causenumber = v.causenumber

alter table #temptbltickets drop column rowid

select * into #temptblfinaltickets from #temptbltickets order by midnumber

alter table #temptblfinaltickets add rowid int identity(1,1) 

declare @idx int, @reccount int, @midnumber varchar(20), @tempMid varchar(20), @RecordId int
select @idx = 1, @midnumber = '', @tempmid= '', @recordid = 0,  @reccount = count(rowid) from #temptblfinaltickets
DECLARE @temp_ticketnumber varchar(20),    
@temp_violationnmber int,    
@temp_fineamount money,
@tenp_violationcode varchar(10),    
@temp_status int,      
@temp_causenumber varchar(30),
@temp_FTAIssueDate datetime,
@temp_bondamount money    
--Sabir Khan 4413 02/27/2009 using while loop for inserting records in tblticketsarchive and tblticketsviolationsarchive...
While @idx <= @reccount
BEGIN
	
	insert into tblticketsarchive(TicketNumber, MidNumber, FirstName, LastName,     
	Initial, Address1, City, StateID_FK, -- PhoneNumber, Rab Nawaz Khan 10196 04/23/2012 Phone Numbers Excluded from uploading
	ZipCode, DOB, DLNumber, ViolationDate, CourtDate, OfficerName,Race,Gender,Height ,AssociatedCaseNumber,listdate    
	,CourtNumber,Courttime,DP2 ,DPC,CRRT,courtid,flag1,OfficerNumber_FK,insertionloaderid)  

	select distinct ticketnumber, left(midnumber,20), left(firstname,20), left(lastname,20),     
	initial, left(address,100), left(city,50), state, -- left(phone,15), Rab Nawaz Khan 10196 04/23/2012 Phone Numbers Excluded from uploading
	zip, dob, dlnumber, violationdate, courtdate, officername, race, gender, height, associatedcasenumber, listdate,    
	courtnum, courttime, dp2, dpc, crrt, courtid, flag1, officernum  , @LoaderId  
	from #temptblfinaltickets where rowid = @idx

	-------------------------------------------------------------------------------------------------------------------------    
	-- CHANGED BY TAHIR AHMED DT:12/9/06    
	-- TO UPDATE FTAIssueDate, BondAmount AND Status    
	-- IF CASE STATUS IS CAUSE NUMBER CONTAINS 'FTA'    
	------------------------------------------------------------------------------------------------------------------------- 
	--Sabir Khan 4413 02/27/2009 get records for FTA ...
	select @temp_causenumber = causenumber, @tenp_violationcode = left(violationcode,10) 
	from   #temptblfinaltickets where rowid = @idx

	if charindex('FTA', @temp_causenumber,0) > 0    
	BEGIN    
		set @temp_status = 186     
		set @temp_FTAIssueDate = @listdate    

		declare @EffectiveDate datetime,    
				@BondAmount money,    
				@NewEffectiveDate datetime,    
				@NewBondAmount money    

		select  @effectivedate = effectivedate,    
				@bondamount = bondamount,    
				@neweffectivedate = neweffectivedate,    
				@newbondamount = newbondamount    
		from	tblviolations WITH(NOLOCK)    
		where	violationnumber_pk = (     
					select top 1 violationnumber_pk from tblviolations WITH(NOLOCK) where violationcode = @tenp_violationcode    
					)     

		if @listdate < @neweffectivedate    
			set @temp_bondamount = @bondamount    

		if @listdate >= @neweffectivedate     
			set  @temp_bondamount = @newbondamount    
	END    
	-------------------------------------------------------------------------------------------------------------------------    
	-- END CHANGES   --------CHANGED BY TAHIR AHMED DT:12/9/06    
	-------------------------------------------------------------------------------------------------------------------------    
	    

	INSERT INTO tblticketsviolationsarchive(TicketNumber_PK, ViolationNumber_PK, FineAmount,    
	ViolationDescription,ViolationCode,violationstatusid,courtdate,courtnumber,courtlocation,    
	bondamount,ticketviolationdate,ticketofficernumber,CauseNumber, ftaissuedate, InsertionLoaderId,    
	offenselocation, offensetime, recordid)

	select ticketnumber,convert(int,violationnumber),fineamount,violation,
	@tenp_violationcode, status, courtdate,courtnum,courtid,convert(money,bondamount) as bondamount,violationdate,officernum,
	@temp_causenumber, @temp_FTAIssueDate,@LoaderId, offenselocation, offensetime  , 0
	from #temptblfinaltickets where rowid = @idx 

	-- Sabir Khan 4413 02/27/2009 RESET VARIABLES....
	select  @effectivedate = NULL,    
			@bondamount = NULL,     
			@neweffectivedate = NULL,      
			@newbondamount = NULL ,
			@temp_status = NULL ,  
			@temp_FTAIssueDate = NULL,
			@temp_causenumber = NULL,
			@temp_bondamount = NULL,
			@tenp_violationcode = NULL

	set @idx = @idx + 1

END 
    
-- UPDATE DO NOT MAIL FLAG FOR NEWLY INSERTED RECORDS..    
-- IF PERSON'S NAME & ADDRESS MARKED AS DO NOT MAIL...    
EXEC USP_HTS_UPDATE_NOMAILFLAG @CurrDate, '3001,3002,3003,'    
    
-- SENDING UPLOADED NO OF RECORDS THROUGH EMAIL   
EXEC usp_sendemailafterloaderexecutes  @idx,@LoaderId,'houston' 

drop table #temp
drop table #duplicateticket
drop table #temptbltickets
drop table #temptblfinaltickets


﻿set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go
/*        
    
Business Logic  : This procedure is used to insert or update follow up date.      
           
Parameter:       
   @TicketID_PK     :  Ticket ID of the client.
   
     
      
*/


 -- usp_SaveCaseExtentions 4653
ALTER PROCEDURE [dbo].[usp_SaveCaseExtentions] --43

---- This Proceudure is used for insertion in Database
---- First it will check weather Followup date exist or not
---- If followup date isnt exist insert new followup date ( Formula = todays date + 7 )
---- If Followup date exist update followupdate (Fahad - 01/04/08)
   
 @TicketID_PK int    
AS    
    
BEGIN    
    
 

DECLARE @NewFollowUpDate SMALLDATETIME


SET @NewFollowUpDate=dbo.fn_getnextbusinessday(getdate(),2)
    

 
 
 
 
 
 
 declare @L_Exist int     
set @L_Exist =0   


    select  @L_Exist = Count(TicketID_PK) from tblTicketsExtensions    
where TicketID_PK = @TicketID_PK    
-- Agha Usman 4426 07/31/2008 -  Add Business days function in NOS Flag Followupdate

if(@L_Exist=0)    
 begin    
 insert into tblTicketsExtensions(TicketID_PK ,NOSFollowUpDate)    
 values(@TicketID_PK ,@NewFollowUpDate)   -- Sameeullah Daris 8635 01/31/2011 -  Add two Business days function in NOS Flag Followupdatefrom current date  

 
 end    
else    
 begin    
 	
	
 	
  update tblTicketsExtensions    
  set NOSFollowUpDate = @NewFollowUpDate -- Sameeullah Daris 8635 01/31/2011 -  Add two Business days function in NOS Flag Followupdatefrom current date
  where TicketID_PK = @TicketID_PK    
   
 end    
  
  
 select  dbo.fn_dateformat(@NewFollowUpDate,27,'/','/',0) as NOSFollowupDate from tblTicketsExtensions where ticketid_pk =  @TicketID_PK    
    
END   
  
  

  



﻿set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go




ALTER procedure [dbo].[Usp_Htp_insertintofaxletter]  
@ticketid int,
@empid int ,
@ename varchar(200),
@eemail varchar(200),
@faxno varchar(50),
@subject varchar(200),
@body varchar(500),
@status varchar(500),
@faxdate datetime,
@docpath varchar(100)

as

if not exists 
		(select * from faxletter 
			where Ticketid=@ticketid and Employeeid=@empid and EmployeeName=@ename and EmployeeEmail=@eemail and
				  FaxNumber=@faxno and Subject=@subject and Body=@body and status=@status and FaxDate=@faxdate
					and Docpath=@docpath)
begin
insert into faxletter
(Ticketid,Employeeid,EmployeeName,EmployeeEmail,FaxNumber,Subject,Body,status,FaxDate,Docpath)
values
(@ticketid,@empid,@ename,@eemail,@faxno,@subject,@body,@status,@faxdate,@docpath)
end





 
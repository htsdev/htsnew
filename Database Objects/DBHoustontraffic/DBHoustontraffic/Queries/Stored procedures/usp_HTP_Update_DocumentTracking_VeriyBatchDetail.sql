﻿/****** 
Create by:  Syed Muhammad Ozair

Business Logic : this procedure is used to update the verification information the picture name stored in docstorage.

List of Parameters:	
	@ScanBatchID : scan batch id for which records will be fetched
	@DocumentBatchID : document batch id for which records will be fetched
	@VerfiedDate : date time when batch is verified, todays date
	@EmployeeID : current user/sales rep id

******/

Create Procedure dbo.usp_HTP_Update_DocumentTracking_VeriyBatchDetail

@ScanBatchID int,
@DocumentBatchID int,
@VerfiedDate datetime,
@EmployeeID int

as

update tbl_htp_documenttracking_scanbatch_detail
set 
	isverified = 1,
	employeeid = @EmployeeID
where 
	DocumentBatchID = @DocumentBatchID
and ScanBatchID_FK = @ScanBatchID

update tbl_htp_documenttracking_batchtickets
set 
	verifieddate=@VerfiedDate
where batchid_fk = @DocumentBatchID

insert into tblticketsnotes (ticketid,subject,employeeid)
select bd.ticketid_fk, 
'<a href="javascript:window.open(''../documenttracking/PreviewMain.aspx?DBID='+ Convert(varchar,bd.batchid_fk) +'&SBID='+ Convert(varchar, @ScanBatchID) +''');void('''');">' + d.documentname + ' Scanned ID: ' + Convert(varchar,bd.batchid_fk) + '</a>',
@EmployeeID
from tbl_htp_documenttracking_batch b inner join tbl_htp_documenttracking_batchtickets bd
on b.batchid=bd.batchid_fk inner join tbl_htp_documenttracking_documents d
on b.documentid_fk=d.documentid
where b.batchid = @DocumentBatchID

go
           
grant exec on dbo.usp_HTP_Update_DocumentTracking_VeriyBatchDetail to dbr_webuser
go
              
 


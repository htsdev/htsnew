SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HMC_Get_User]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HMC_Get_User]
GO



CREATE Procedure [dbo].[USP_HMC_Get_User]         
as      
 Select u.*,  
 case Status  
 when 0 then 'In Active'  
 when 1 then 'Active'  
 end as Status2,  
   
 case u.AccountType  
 when 1 then 'Administrator'  
 when 2 then 'User'  
 end as AccountType2,

(select t.FirmAbbreviation from tblfirm t where t.FirmID = u.Firm) as FirmAbbr
  
 from tbl_HMC_Users  u



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_GET_SummaryInfo1]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_GET_SummaryInfo1]
GO











CREATE  procedure USP_HTS_GET_SummaryInfo1
 (  
 @TicketId_pk int  
 )  
  
as  
  
declare @PaidAmount money,  
 @AmountOwes money  
  
select @PaidAmount = (  
  select isnull(sum(isnull(chargeAmount,0)) ,0)   
  from tblticketspayment where ticketid = @TicketId_pk   
  )  
select @AmountOwes = calculatedtotalfee - @paidamount   
from  tbltickets where ticketid_pk = @ticketid_pk  
  
  
 Select 
t.Lastname, t.Firstname, d.Description AS CaseStatus, CONVERT(varchar(10), t.CurrentDateset, 101) AS CourtDate, t.CurrentCourtNum, c.CourtName, 
                      c.Address AS CourtAddress, f.FirmAbbreviation, t.ContactComments, t.GeneralComments, t.SettingComments, t.TrialComments, t.Address1, t.Address2, 
                      t.Midnum, t.DLNumber, t.DOB, t.Contact1, t.ContactType1, t.Contact2, t.ContactType2, t.Contact3, t.ContactType3, t.LanguageSpeak, t.CDLFlag, 
                      t.AccidentFlag, tf_1.Description AS FlagEvents, t.calculatedtotalfee, t.InitialTotalFeeCharged, t.RerapAmount, t.ContinuanceAmount, t.Adjustment, 
                      t.Addition, tblContactstype_3.Description as Description1, tblContactstype_1.Description AS Description2, tblContactstype_2.Description AS Description3,  
 @PaidAmount as PaidAmount,  
 convert(varchar(10),@AmountOwes) as AmountOwes   
FROM  dbo.tblEventFlags tf_1 INNER JOIN
                      dbo.tblTicketsFlag tf ON tf_1.FlagID_PK = tf.FlagID RIGHT OUTER JOIN
                      dbo.tblFirm f RIGHT OUTER JOIN
                      dbo.tblCourts c RIGHT OUTER JOIN
                      dbo.tblTickets t LEFT OUTER JOIN
                      dbo.tblContactstype tblContactstype_2 ON t.ContactType3 = tblContactstype_2.ContactType_PK LEFT OUTER JOIN
                      dbo.tblContactstype tblContactstype_1 ON t.ContactType2 = tblContactstype_1.ContactType_PK LEFT OUTER JOIN
                      dbo.tblContactstype tblContactstype_3 ON t.ContactType1 = tblContactstype_3.ContactType_PK LEFT OUTER JOIN
                      dbo.tblDateType d ON t.Datetypeflag = d.TypeID ON c.Courtid = t.CurrentCourtloc ON f.FirmID = t.FirmID ON tf.TicketID_PK = t.TicketID_PK

WHERE   t.TicketID_PK = @TicketId_pk








GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


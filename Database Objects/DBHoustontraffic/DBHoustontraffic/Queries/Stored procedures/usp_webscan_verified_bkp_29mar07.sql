SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_webscan_verified_bkp_29mar07]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_webscan_verified_bkp_29mar07]
GO

  
CREATE procedure [dbo].[usp_webscan_verified_bkp_29mar07]                  
              
@PicID int,                   
@CauseNo varchar(50),                   
@CourtDate as datetime,                   
@CourtTime as varchar(15),                   
@CourtNo as int,                   
@Status as varchar(50),                   
@Location as varchar(50),                   
@CourtDateScan as datetime    ,              
@EmployeeID as int,            
@Msg as int output        
                  
                  
                      
as                      
                   
declare @TempCauseNo as varchar(50)                  
select @TempCauseNo =casenumassignedbycourt from tblticketsviolations where casenumassignedbycourt=@CauseNo                  
set @Msg=1              
              
if    @TempCauseNo <> 'NULL'                  
 begin                  
                  
                       
  declare @TempCID as varchar(3)                      
  declare @CourtNumScan as varchar(50)                      
                        
  set @TempCID=@CourtNo                      
                        
  if @TempCID='13' or @TempCID='14'                      
    begin                      
     set @CourtNumScan='3002'                      
    end                      
  else if @TempCID='18'                      
    begin                      
     set @CourtNumScan='3003'                      
    end                      
  else                      
    begin                      
     set @CourtNumScan='3001'                      
    end                      
                      
  declare @tempCVID int                      
                
  --Select @tempCVID=courtviolationstatusid from tblcourtviolationstatus where description=@Status                      
  if @Status='JURY TRIAL'     
 set @tempCVID=26    
    
  else if @Status='JUDGE'     
 set @tempCVID=103    
    
  else if @Status='PRETRIAL'     
 set @tempCVID=101    
  
  else if @Status='ARRAIGNMENT'     
 set @tempCVID=3    
   
    
  declare @TodayDate as datetime                      
  set @TodayDate=getdate()              
              
  begin                      
   update tblticketsviolations                      
                         
   set                      
                         
   CourtDateMain        =@CourtDateScan,                      
   CourtNumberMain        =@CourtNo,                      
   CourtViolationStatusIDmain     =@tempCVID,                      
   VerifiedStatusUpdateDate     =@TodayDate,--@ScanUpdateDate,                      
   CourtID          =@CourtNumScan,               
   CourtDate         =@CourtDateScan,              
   CourtNumber         =@CourtNo,              
   CourtViolationStatusID      =@tempCVID,              
   CourtDateScan        =@CourtDateScan,              
   CourtViolationStatusIDScan     =@tempCVID,              
   CourtNumberScan        =@CourtNo,              
   ScanUpdatedate        =@TodayDate,              
   CourtIDScan         =@CourtNumScan,            
   Updateddate  =   @TodayDate ,                   
   vemployeeid         =@EmployeeID,      
   BSDAUpdateDate =getdate()                       
                          
                         
   where                       
    casenumassignedbycourt=@CauseNo                       
    and  CourtID in (3001,3002,3003)                      
                       
   update   tbl_webscan_ocr                          
    set                          
    causeno   =@CauseNo,                          
    Status    =@Status,                          
    NewCourtDate  =convert(varchar(12),@CourtDate,101),                          
    CourtNo   =@CourtNo,                          
    Location   =@Location,                          
                              
    Time    =@CourtTime ,              
    checkstatus  =4               
                 
    where                          
    picid=@PicID              
                 
   update tbl_webscan_data              
    set     
    CauseNo    =@CauseNo,                        
    CourtDate   =@CourtDate,                        
    CourtTime   =@CourtTime,                        
    CourtRoomNo   =@CourtNo,                        
    Status    =@Status,                        
    CourtLocation =@Location                        
    where                        
    PicID=@PicID               
                 
     set @Msg=2               
  end              
              
 end          
          
if @Msg=1          
begin          
 update   tbl_webscan_ocr                          
 set                          
 causeno   =@CauseNo,                          
 Status    =@Status,                          
 NewCourtDate  =convert(varchar(12),@CourtDate,101),                          
 CourtNo   =@CourtNo,                          
 Location   =@Location,                          
                             
 Time    =@CourtTime ,              
 checkstatus  =5               
               
 where                
  picid=@PicID              
                
 update tbl_webscan_data              
 set                         
 CauseNo    =@CauseNo,                        
 CourtDate   =@CourtDate,                        
 CourtTime   =@CourtTime,                        
 CourtRoomNo   =@CourtNo,                    
 Status    =@Status,                        
 CourtLocation  =@Location                        
 where                        
  PicID=@PicID              
end     
  
  
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


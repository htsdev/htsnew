SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Insert_PricingTemplate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Insert_PricingTemplate]
GO










CREATE PROCEDURE USP_HTS_Insert_PricingTemplate
	(
	@TemplateName			varchar(50),
	@TemplateDescription	varchar(200),
	@EmployeeID 			int
	)

 AS


declare @count int

--see TemplateName is already exist or not. @count=0 means no
select @count = count(TemplateID)
from tblPricingTemplate
where TemplateName=@TemplateName


--if TemplateName is not already exist, do Insert
if @count=0
	begin
		Insert into tblpricingtemplate (templatename,templatedescription,EmployeeID)
		values (
				@TemplateName,
				@TemplateDescription,
				@EmployeeID
			)

		select max(TemplateID) as 'TemplateID' from tblpricingtemplate
	end
else
	begin
		select 0 as 'TemplateID'
	end





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


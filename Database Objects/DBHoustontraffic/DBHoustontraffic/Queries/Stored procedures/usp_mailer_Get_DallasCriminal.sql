SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_mailer_Get_DallasCriminal]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_mailer_Get_DallasCriminal]
GO

  
  
  
  
  
--  usp_mailer_Get_DallasCriminal 16, 37, 16, '10/1/2007', '10/25/2007', 0  
CREATE  Procedure [dbo].[usp_mailer_Get_DallasCriminal]  
 (  
 @catnum  int,  
 @LetterType  int,  
 @crtid   int,                                                   
 @startListdate  Datetime,  
 @endListdate  DateTime,                                                        
 @SearchType     int                                               
 )  
  
as                                                                
                                                              
declare @officernum varchar(50),   
 @officeropr varchar(50),   
 @tikcetnumberopr varchar(50),   
 @ticket_no varchar(50),   
 @zipcode varchar(50),   
 @zipcodeopr varchar(50),   
 @fineamount money,  
 @fineamountOpr varchar(50) ,  
 @fineamountRelop varchar(50),  
 @singleviolation money,   
 @singlevoilationOpr varchar(50) ,   
 @singleviolationrelop varchar(50),   
 @doubleviolation money,  
 @doublevoilationOpr varchar(50) ,   
 @doubleviolationrelop varchar(50),   
 @count_Letters int,  
 @violadesc varchar(500),  
 @violaop varchar(50),  
 @sqlquery nvarchar(4000)  ,  
 @sqlquery2 nvarchar(4000)  ,  
 @sqlParam nvarchar(1000)  
  
  
select @sqlparam = '@catnum int, @LetterType int, @crtid int, @startListdate Datetime,    
      @endListdate DateTime, @SearchType int'  
  
                                                               
                                                        
select  @endlistdate = @endlistdate+'23:59:59.000',   
 @sqlquery ='',  
 @sqlquery2 = ''  
                                                                
Select @officernum=officernum,  
 @officeropr=oficernumOpr,  
 @tikcetnumberopr=tikcetnumberopr,  
 @ticket_no=ticket_no,  
 @zipcode=zipcode,  
 @zipcodeopr=zipcodeLikeopr,  
 @singleviolation =singleviolation,  
 @singlevoilationOpr =singlevoilationOpr,   
 @singleviolationrelop =singleviolationrelop,   
 @doubleviolation = doubleviolation,  
 @doublevoilationOpr=doubleviolationOpr,  
 @doubleviolationrelop=doubleviolationrelop,  
 @violadesc=violationdescription,  
 @violaop=violationdescriptionOpr ,   
 @fineamount=fineamount ,  
 @fineamountOpr=fineamountOpr ,  
 @fineamountRelop=fineamountRelop    
from  tblmailer_letters_to_sendfilters    
where  courtcategorynum=@catnum   
and  Lettertype=@LetterType   
                                                        
set  @sqlquery=@sqlquery+'                                            
 Select distinct        
 convert(datetime , convert(varchar(10),ta.recloaddate,101)) as mdate,       
 flag1 = isnull(ta.Flag1,''N'') ,                                                        
 isnull(ta.donotmailflag,0) as donotmailflag, tva.recordid as recordid  
into #temp                                                              
FROM dallastraffictickets.dbo.tblticketsarchive  ta
inner join dallastraffictickets.dbo.tblticketsviolationsarchive tva
on tva.recordid = ta.recordid  
where datediff(day, ta.recloaddate , @startListdate)<=0  
and datediff(day, ta.recloaddate, @endlistdate) >=0 
and isnull(ta.ncoa48flag,0) = 0 
and tva.recordid not in (                                                                      
 select recordid from tblletternotes   
 where  lettertype =@LetterType     
 )
'  
  
if @catnum = @crtid
	set @sqlquery=@sqlquery + ' and tva.courtlocation in (select courtid from dallastraffictickets.dbo.tblcourts where courtcategorynum = @catnum )
	'
else
	set @sqlquery=@sqlquery + ' and tva.courtlocation = @crtid 
	'
  
set @sqlquery=@sqlquery + '   
Select distinct   mdate ,Flag1, donotmailflag, recordid into #temp1  from #temp                                                            
 '                                    
  
  
set @sqlquery2=@sqlquery2 +'  
Select count(distinct recordid) as Total_Count, mdate   
into #temp2   
from #temp1   
group by mdate   
    
Select count(distinct recordid) as Good_Count, mdate   
into #temp3  
from #temp1   
where Flag1 in (''Y'',''D'',''S'')     
and donotmailflag = 0      
group by mdate     
                                                        
Select count(distinct recordid) as Bad_Count, mdate    
into #temp4    
from #temp1   
where Flag1  not in (''Y'',''D'',''S'')   
and donotmailflag = 0   
group by mdate   
                     
Select count(distinct recordid) as DonotMail_Count, mdate    
into #temp5     
from #temp1   
where donotmailflag=1   
group by mdate'  
   
           
set @sqlquery2 = @sqlquery2 +'                                                          
Select   #temp2.mdate as listdate,  
        Total_Count,  
        isnull(Good_Count,0) Good_Count,    
        isnull(Bad_Count,0)Bad_Count,    
        isnull(DonotMail_Count,0)DonotMail_Count    
from #Temp2 left outer join #temp3     
on #temp2.mdate=#temp3.mdate    
left outer join #Temp4    
on #temp2.mdate=#Temp4.mdate      
left outer join #Temp5    
on #temp2.mdate=#Temp5.mdate                                                         
where Good_Count > 0 order by #temp2.mdate desc  
drop table #temp drop table #temp1 drop table #temp2  drop table #temp3  drop table #temp4  drop table #temp5   
'                                                        
                                                        
--print @sqlquery  + @sqlquery2                                         
--exec( @sqlquery)  
set @sqlquery = @sqlquery + @sqlquery2  
exec sp_executesql @sqlquery , @sqlparam, @catnum, @LetterType, @crtid, @startListdate, @endListdate, @SearchType  
  
  
  
  
  
  
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


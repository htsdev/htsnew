/*  
* Created By  : Waqas Javed  
* Task ID   : 7077  
* Created Date : 12/14/2009  
* Business logic : This procedure is used to add menu items for menu configuration  
*   
* Parameter List  
* @LevelID   It represents the Level of the Menu  
* @ParentMenuID  It represents the parent menu id of the menu item  
* @Title   It represents the title of menu item  
* @URL    It represents the url of the menu item  
* @IsActive   It represents that menu item is active or not.  
* @IsSelected  It represents that menu item is selected or not.  
* @IsAdminLog  It represents that admin log is allowed or not  
* @Category	   It represents report category, report category can be an Alert=1 or Report=2.
*/  
  
  
ALTER PROCEDURE dbo.usp_htp_Add_NewMenuItems    
 @LevelID AS INT = null,    
 @ParentMenuID AS INT = null,    
 @Title AS VARCHAR(200)= null,    
 @URL AS VARCHAR(2000) = null,    
 @IsActive AS BIT = null,    
 @IsSelected AS INT = null,    
 @IsAdminLog AS BIT = NULL,  
 @IsSubMenuHidden AS BIT = TRUE   ,
 @Category AS INT =NULL
 AS    
BEGIN    
IF @LevelID = 1    
    BEGIN    
        INSERT INTO tbl_TAB_MENU    
        (    
         TITLE,    
         ORDERING,    
         [active],  
         IsSubMenuHidden  
        )             
        SELECT @Title, MAX(ttm.ORDERING) + 1 , @IsActive,@IsSubMenuHidden FROM tbl_TAB_MENU ttm            
            
    END    
ELSE    
    BEGIN    
        INSERT INTO tbl_TAB_SUBMENU    
        (    
         TITLE,    
         [URL],    
         ORDERING,    
         SELECTED,    
         MENUID,    
         [active],    
         IsAdminLog,
         Category  --SAEED 7791 05/25/2010,new field added in for report category,
        )    
        SELECT     
         @Title,    
         @URL,    
         MAX(ttm.ORDERING) + 1 ,    
         @IsSelected,    
         @ParentMenuID,    
         @IsActive,    
         @IsAdminLog,
         @Category --SAEED 7791 05/25/2010,new field added in for report category,   
         FROM tbl_TAB_SUBMENU ttm WHERE ttm.MENUID = @ParentMenuID             
             

declare @MenuID int 
SET @MenuID = CAST(SCOPE_IDENTITY() AS INT)

		BEGIN
            UPDATE tbl_TAB_SUBMENU
            SET    SELECTED = 0
            WHERE  ID IN (SELECT ID
                          FROM   tbl_TAB_SUBMENU ttm
                          WHERE  ttm.MENUID = (
                                     SELECT TOP 1 MENUID
                                     FROM   tbl_TAB_SUBMENU
                                     WHERE  ID = @MenuID
                                 ))
            
            UPDATE tbl_TAB_SUBMENU
            SET    SELECTED = @IsSelected
            WHERE  ID = @MenuID
        END
        
        
     
    END    
END     
  
﻿/****** 
Object:	StoredProcedure [dbo].[USP_HTS_get_system_user_info] 
Altered by:Fahad Muhammad Qureshi
Business Logic : 
		This report display all records of user in which user name,access ,status etc containing all related firms.
Parameters:	
		@employeeid
Output Columns:
		EmployeeID 
		firstname 
		lastname 
		abbreviation 
		username
		password
	    accesstype 
        email
        CanUpdateCloseOutLog 
        flagCanSPNSearch 
        SPNUserName 
        SPNPassword 
        status
        NTUserID 
        IsAttorney,  
        isServiceTicketAdmin
        CloseOutAccessDays
        isLock
        FirmId 
******/       
ALTER procedure [dbo].[USP_HTS_get_system_user_info]    
@employeeid int                      
as                      
select EmployeeID as EmpId,  -- Agha Usman 4271 07/02/2008
		firstname,  
		lastname,  
		abbreviation,  
		username,  
		password,  
	   accesstype ,  
       email,  
       CanUpdateCloseOutLog,  
       ISNULL(flagCanSPNSearch,0) as flagCanSPNSearch,  
       SPNUserName,  
       SPNPassword,  
       isnull(status,0) as status,  
       isnull(NTUserID,'') as NTUserID,  
       IsAttorney,  
       isServiceTicketAdmin,  
       isnull(CloseOutAccessDays,0) as   CloseOutAccessDays,
       isnull(isLock,0) as isLock,
       FirmId, --Fahad 5196 11/28/2008
       ContactNumber --Nasir 6968 added contact number        
from tblusers                      
where employeeid = @employeeid 
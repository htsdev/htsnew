USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTP_Search_AttorneyCases]    Script Date: 02/28/2011 22:28:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Sameeullah>
-- Description:	This procedure is used for Searching the Attorney Cases 
-- =============================================

--USP_HTP_Search_AttorneyCases '','2/28/2011',4123
ALTER  PROCEDURE [dbo].[USP_HTP_Search_AttorneyCases]
@TicketNumber VARCHAR(100),
@UploadedDate VARCHAR(20),
@UploadedBy int
AS
BEGIN
DECLARE @Query nVARCHAR(max)
SET @Query=
    'SELECT  
      [Court]
      ,[ClientBirthDate] 
      ,[StatusReason] 
      ,[CauseNumber] 
      ,[Customer]
      ,[TicketNumber] 
      ,[OfficersName] 
      ,[WarrantCode] 
      ,Case Convert(varchar,[HoustonResetPrintedDate],101) when ''01/01/1900'' then '''' else Convert(varchar,[HoustonResetPrintedDate],101) end as [HoustonResetPrintedDate]
      ,Case Convert(varchar,[MFCRequestedForDate],101) when ''01/01/1900'' then '''' else Convert(varchar,[MFCRequestedForDate],101) end as [MFCRequestedForDate]
      ,[MFCStatus] 
      ,[CourtDate] 
      ,[ClientDLNumber] 
      ,[DLType] 
      ,[IsProcessed] 
      ,[ProcessedDate]
      ,acf.UploadedDate 
     FROM OutsideAttorneyCases ac INNER JOIN OutsideAttorneyCasesFiles acf ON ac.AttorneyCasesFileID=acf.AttorneyCasesFileID WHERE acf.UploadedBy = @UploadedBy '
      
 

      
       
    
      if((@TicketNumber is NOT NULL)AND(LTRIM(RTRIM(@TicketNumber))<>''))
      BEGIN
      	  set @Query=@Query+ ' AND ac.TicketNumber=@TicketNumber '
     
      END
        
        
        
        if((@UploadedDate is NOT NULL)AND(LTRIM(RTRIM(@UploadedDate))<>''))
 
      BEGIN
      
      	
      	
		set @Query=@Query+	 ' AND  CAST (convert(varchar, acf.UploadedDate,101) AS SMALLDATETIME)=CAST (@UploadedDate AS SMALLDATETIME) '
      END
     
      DECLARE           
        @paramlist  nvarchar(200)  
      	
      	SELECT @paramlist = N'@TicketNumber   VARCHAR(100) ,                              
                     @UploadedDate  VARCHAR(20) ,                         
                     @UploadedBy    int '                       
                    
                                                                   
EXEC sp_executesql @Query, @paramlist,                            
                   @TicketNumber, @UploadedDate, @UploadedBy
                 
                

 
--PRINT(@Query)

--EXEC (@Query)  


END


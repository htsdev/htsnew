﻿/*        
Created By     : Fahad Muhammad Qureshi.
Created Date   : 08/17/2009  
TasK		   : 6341        
Business Logic : This procedure is used to fetching out Data containing all Client's information and all associated flag on his/her profile.
@ReportType = 1 : All records
@ReportType = 2 : Disposed
@ReportType = 3 : Waiting
*/
alter PROCEDURE [dbo].[USP_HTP_GetAllDataDumpClients]-- USP_HTP_GetAllDataDumpClients 1
	@ReportType INT
AS

-- Getting only related flags in a temp table
SELECT * INTO #TempFlags
FROM   tblTicketsFlag ttf WHERE ttf.TicketID_PK IN(
SELECT DISTINCT (tv.ticketid_pk) FROM 
tblTickets t
	       INNER JOIN tblTicketsViolations tv
	            ON  tv.TicketID_PK = t.TicketID_PK
	       INNER JOIN tblCourtViolationStatus tcvs
	            ON  tv.CourtViolationStatusIDmain = tcvs.CourtViolationStatusID
	WHERE  t.Activeflag = 1
	       AND (
	               @ReportType = 1
	               AND (tv.CourtViolationStatusIDmain NOT IN (80,122,104,124,123))--Not Include 80-Disposed,122-No-Hire,104-Waiting,124-Pled Guilty,123-ALR Hearing Cases
	           )
	       OR  (@ReportType = 2 AND tcvs.CategoryID = 50 AND tv.CourtViolationStatusIDmain<>122)--80 Disposed,122 No Hire
	       OR  (@ReportType = 3 AND tcvs.CategoryID = 1))--104 Waiting 
	       AND ISNULL(ttf.DeleteFlag, 0) <> 1

-- Generating Actual result-set
	SELECT
		   '<a href="javascript:window.open(''../ClientInfo/ViolationFeeold.aspx?sMenu=61&search=0&casenumber='+convert(varchar(10),t.ticketid_pk)+''');void('''');"''>' +  t.Firstname +'</a>' as [First Name],
		   t.Lastname,
	       CONVERT(VARCHAR(30), t.DOB, 101) AS [Date of Birth],
	       tv.casenumassignedbycourt AS CauseNumber,
	       tv.RefCaseNumber AS TicketNumber,
	       tv2.[Description] AS Violation,
	       tv.CourtNumbermain AS CourtNo,
	       tc.ShortName AS CourtLocation,
	       CourtStatus = (
	           SELECT t1.ShortDescription
	           FROM   tblCourtViolationStatus t1
	           WHERE  t1.CourtViolationStatusID = tv.CourtViolationStatusIDmain),
	       dbo.fn_DateFormat(tv.CourtDateMain,1,'/','',0) AS [Court Date],
	       dbo.fn_DateFormat(tv.CourtDateMain,26,'',':',1) AS [Court Time],
	       t.BondFlag,
	       t.DLNumber,
	       t.Address1,
	       t.Address2,
	       t.City,
	       STATE = (
	           SELECT ts.[State]
	           FROM   tblState ts
	           WHERE  ts.StateID = t.Stateid_FK),
	       t.Zip,
	       t.Contact1,
	       t.Contact2,
	       t.Contact3,
	       t.Email,
	       CASE WHEN EXISTS (SELECT * FROM #TempFlags WHERE flagid = 2 AND ticketid_pk = tv.TicketID_PK)
	            THEN 'YES'
	            ELSE 'NO'
	        END AS Attention,
	       CASE WHEN EXISTS (SELECT * FROM #TempFlags WHERE flagid = 34 AND ticketid_pk = tv.TicketID_PK)
	            THEN 'YES'
	            ELSE 'NO'
	       END AS [Bad Email],
	       CASE WHEN EXISTS (SELECT * FROM #TempFlags WHERE flagid = 30 AND ticketid_pk = tv.TicketID_PK)
	            THEN 'YES'
	            ELSE 'NO'
	       END AS [Bond Amount Confirmed],
	       CASE WHEN EXISTS (SELECT * FROM #TempFlags WHERE flagid = 37 AND ticketid_pk = tv.TicketID_PK)
	            THEN 'YES'
	            ELSE 'NO'
	       END AS [Complaint],
	       CASE WHEN EXISTS (SELECT * FROM #TempFlags WHERE flagid = 18 AND ticketid_pk = tv.TicketID_PK)
	            THEN 'YES'
	            ELSE 'NO'
	       END AS [Contacted Internet Signup],
	       CASE WHEN EXISTS (SELECT * FROM #TempFlags WHERE flagid = 9 AND ticketid_pk = tv.TicketID_PK)
	            THEN 'YES'
	            ELSE 'NO'
	       END AS [Continuance],
	       CASE WHEN EXISTS (SELECT * FROM #TempFlags WHERE flagid = 25 AND ticketid_pk = tv.TicketID_PK)
	            THEN 'YES'
	            ELSE 'NO'
	       END AS [Do Not Reset Judge Trial],
	       CASE WHEN EXISTS (SELECT * FROM #TempFlags WHERE flagid = 19 AND ticketid_pk = tv.TicketID_PK)
	            THEN 'YES'
	            ELSE 'NO'
	       END AS [Name Discrepancy],
	       CASE WHEN EXISTS (SELECT * FROM #TempFlags WHERE flagid = 24 AND ticketid_pk = tv.TicketID_PK)
	            THEN 'YES'
	            ELSE 'NO'
	       END AS [No Advertisement Mailer],
	       CASE WHEN EXISTS (SELECT * FROM #TempFlags WHERE flagid = 32 AND ticketid_pk = tv.TicketID_PK)
	            THEN 'YES'
	            ELSE 'NO'
	       END AS [No Bond Discrepancy],
	       CASE WHEN EXISTS (SELECT * FROM #TempFlags WHERE flagid = 5 AND ticketid_pk = tv.TicketID_PK)
	            THEN 'YES'
	            ELSE 'NO'
	       END AS [No Calls],
	       CASE WHEN EXISTS (SELECT * FROM #TempFlags WHERE flagid = 17 AND ticketid_pk = tv.TicketID_PK)
	            THEN 'YES'
	            ELSE 'NO'
	        END AS [No Check],
	       CASE WHEN EXISTS (SELECT * FROM #TempFlags WHERE flagid = 7 AND ticketid_pk = tv.TicketID_PK)
	            THEN 'YES'
	            ELSE 'NO'
	       END AS [No Letters],
	       CASE WHEN EXISTS (SELECT * FROM #TempFlags WHERE flagid = 26 AND ticketid_pk = tv.TicketID_PK)
	            THEN 'YES'
	            ELSE 'NO'
	       END AS [No Plea Out],
	       CASE WHEN EXISTS (SELECT * FROM #TempFlags WHERE flagid = 12 AND ticketid_pk = tv.TicketID_PK)
	            THEN 'YES'
	            ELSE 'NO'
	       END AS [No Split],
	       CASE WHEN EXISTS (SELECT * FROM #TempFlags WHERE flagid = 16 AND ticketid_pk = tv.TicketID_PK)
	            THEN 'YES'
	            ELSE 'NO'
	       END AS [Not Me],
	       CASE WHEN EXISTS (SELECT * FROM #TempFlags WHERE flagid = 15 AND ticketid_pk = tv.TicketID_PK)
	            THEN 'YES'
	            ELSE 'NO'
	        END AS [Not on Court's System],
	       CASE WHEN EXISTS (SELECT * FROM #TempFlags WHERE flagid = 23 AND ticketid_pk = tv.TicketID_PK)
	            THEN 'YES'
	            ELSE 'NO'
	       END AS [Open Service Ticket],
	       CASE WHEN EXISTS (SELECT * FROM #TempFlags WHERE flagid = 8 AND ticketid_pk = tv.TicketID_PK)
	            THEN 'YES'
	            ELSE 'NO'
	       END AS [Outside Firm's Client],
	       CASE WHEN EXISTS (SELECT * FROM #TempFlags WHERE flagid = 1 AND ticketid_pk = tv.TicketID_PK)
	            THEN 'YES'
	            ELSE 'NO'
	       END AS [Priority],
	       CASE WHEN EXISTS (SELECT * FROM #TempFlags WHERE flagid = 20 AND ticketid_pk = tv.TicketID_PK)
	            THEN 'YES'
	            ELSE 'NO'
	       END AS [Probation Request],
	       CASE WHEN EXISTS (SELECT * FROM #TempFlags WHERE flagid = 36 AND ticketid_pk = tv.TicketID_PK)
	            THEN 'YES'
	            ELSE 'NO'
	       END AS [Problem Client (Allow Hire)],
	       CASE WHEN EXISTS (SELECT * FROM #TempFlags WHERE flagid = 13 AND ticketid_pk = tv.TicketID_PK)
	            THEN 'YES'
	            ELSE 'NO'
	        END AS [Problem Client (No Hire)],
	       CASE WHEN EXISTS (SELECT * FROM #TempFlags WHERE flagid = 31 AND ticketid_pk = tv.TicketID_PK)
	            THEN 'YES'
	            ELSE 'NO'
	       END AS [Read Notes],
	       CASE WHEN EXISTS (SELECT * FROM #TempFlags WHERE flagid = 35 AND ticketid_pk = tv.TicketID_PK)
	            THEN 'YES'
	            ELSE 'NO'
	       END AS [SOL Flag],
	       CASE WHEN EXISTS (SELECT * FROM #TempFlags WHERE flagid = 22 AND ticketid_pk = tv.TicketID_PK)
	            THEN 'YES'
	            ELSE 'NO'
	       END AS [Upset Client],
	       CASE WHEN EXISTS (SELECT * FROM #TempFlags WHERE flagid = 21 AND ticketid_pk = tv.TicketID_PK)
	            THEN 'YES'
	            ELSE 'NO'
	       END AS [Watch],
	       CASE WHEN EXISTS (SELECT * FROM #TempFlags WHERE flagid = 33 AND ticketid_pk = tv.TicketID_PK)
	            THEN 'YES'
	            ELSE 'NO'
	        END AS [Wrong Telephone Number],
	        ISNULL(t.calculatedtotalfee, 0) AS Fee,
	        Paid = (
	           SELECT SUM(ISNULL(p.ChargeAmount, 0))
	           FROM   tblticketspayment p
	           WHERE  p.ticketid = t.ticketid_pk
	                  AND p.paymentvoid = 0),                                     
		   Owed=((ISNULL(t.TotalFeeCharged,0) - (SELECT SUM(ISNULL(p.ChargeAmount, 0))FROM   tblticketspayment p WHERE  p.ticketid = t.ticketid_pk AND p.paymentvoid = 0))), 
		   LEFT(t.GeneralComments, 100) AS GeneralComments,
	        '<a href="javascript:window.open(''../Paperless/Documents.aspx?sMenu=119&search=0&casenumber='+convert(varchar(10),t.ticketid_pk)+''');void('''');"''>' + 'Docs&nbsp</a>' as [Docs]
	FROM   tblTickets t
	       INNER JOIN tblTicketsViolations tv
	            ON  tv.TicketID_PK = t.TicketID_PK
	       INNER JOIN tblCourts tc
	            ON  tc.CourtID = tv.CourtID
	       INNER JOIN tblCourtViolationStatus tcvs
	            ON  tv.CourtViolationStatusIDmain = tcvs.CourtViolationStatusID
	       INNER JOIN tblViolations tv2
	            ON  tv2.ViolationNumber_PK = tv.ViolationNumber_PK
	WHERE  t.Activeflag = 1
	       AND (
	               @ReportType = 1
	               AND (tv.CourtViolationStatusIDmain NOT IN (80,122,104,124,123))--Not Include 80-Disposed,122-No-Hire,104-Waiting,124-Pled Guilty,123-ALR Hearing Cases
	           )
	       OR  (@ReportType = 2 AND tcvs.CategoryID = 50 AND tv.CourtViolationStatusIDmain<>122)--Not Include 122-No Hire 
	       OR  (@ReportType = 3 AND tcvs.CategoryID = 1)--104 Waiting 

	ORDER BY LTRIM(RTRIM(t.Firstname)), LTRIM(RTRIM(t.Lastname))

DROP TABLE #TempFlags
GO
GRANT EXECUTE ON dbo.USP_HTP_GetAllDataDumpClients  TO dbr_webuser
GO 
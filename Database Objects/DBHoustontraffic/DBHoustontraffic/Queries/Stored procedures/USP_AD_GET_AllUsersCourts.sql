﻿/****** 
Create by		: Abid Ali
Created Date	: 11/19/2008
Task ID			: 5137
Business Logic : This procedure simply return the all courts related to a users firm id
					which are active.

Column Return : 
				CourtID
				CourtName
******/

ALTER Procedure [dbo].[USP_AD_GET_AllUsersCourts]
(
	@FirmID int
)
AS

	SELECT
		DISTINCT	c.CourtID,
					c.CourtName
	FROM
					tblUsers u
		--ozair 5305 12/05/2008 covering firm id look up change to tblticketsviolations instead of tbltickets
		INNER JOIN	tblTicketsviolations tv 
				-- Waqas 5461 02/06/2009 all courts for firmid 3000  
				ON (@firmid = 3000 or  u.FIrmID = tv.CoveringFirmID )				
		INNER JOIN	tblCourts c 
				ON	tv.Courtid = c.courtid
		INNER JOIN  tbltickets t
				ON	t.ticketid_pk=tv.ticketid_pk
	WHERE 
		-- Waqas 5461 02/06/2009 all courts for firmid 3000  
		(@firmid = 3000 or tv.coveringfirmid = @firmid) 
		AND
		c.CourtID <> 0		
		AND
		c.InActiveFlag = 0
		AND
		t.ActiveFlag = 1
	ORDER BY c.CourtName ASC
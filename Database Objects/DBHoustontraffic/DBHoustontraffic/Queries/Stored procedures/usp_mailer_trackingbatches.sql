SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_mailer_trackingbatches]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_mailer_trackingbatches]
GO



-- usp_mailer_trackingbatches 1, 10, '01/01/07', '01/20/07'
CREATE proc [dbo].[usp_mailer_trackingbatches]        
@crtid int,        
@LetterType int ,   
@startListdate datetime,    
@endListdate datetime
    
as    
declare @sqlquery varchar(8000)    
set @sqlquery=' '    
set @sqlquery=@sqlquery+'    
	SELECT DISTINCT tln.RecordID, tln.LetterType, ta.Clientflag,isnull(ta.donotmailflag,0) donotmailflag,Batchid,    
		convert (varchar (10),tln.ListDate,101) Listdate, Address1, FirstName, LastName,Midnumber,LCount,
		LCount*Pexpense as MailCost, convert (varchar (10),currentdate,101) as BatchSentdate    
	into #temp    
	FROM     dbo.tblTicketsArchive ta  
	INNER JOIN    
			 dbo.tblLetterNotes tln 
	ON	ta.RecordID = tln.RecordID 
	inner join tblbatchletter tb 
	on	tb.batchid=tln.batchid_fk    
	and tb.lettertype=tb.lettertype '   
if @lettertype = 10
	set @sqlquery =@sqlquery + '
	WHERE   (tln.LetterType =9)'
else if @lettertype = 11
	set @sqlquery =@sqlquery + '
	WHERE   (tln.LetterType =10)'

set @sqlquery =@sqlquery + ' 
	AND  datediff(day, '''+convert(varchar(10), @startlistdate, 101)+''' , tb.batchsentdate) >= 0
	and datediff(day, '''+convert(varchar(10), @endListdate, 101)+''',tb.batchsentdate) <= 0'
    
if(@crtid=1 )                
	set @sqlquery=@sqlquery+' 
	and tln.courtid In (Select courtid from tblcourts where courtcategorynum ='+convert(varchar(10),@crtid ) +')'                    

if (@crtid <>1  )                
	set @sqlquery =@sqlquery +'
	and tln.courtid ='+convert(varchar(10),@crtid )                                    
                
set @sqlquery =@sqlquery+  '                                                     
	and ta.recordid                                                             
       not in (                                                          
			select recordid from tblletternotes where lettertype ='+convert(varchar(10),@LetterType) +'
			)             
	and datediff(day, ta.CourtDate,getdate())>30'     
    
set @sqlquery=@sqlquery+';    
Select distinct  #temp.Recordid,#temp.listdate,#temp.lettertype, #temp.clientflag,#temp.donotmailflag,isnull(activeflag,0) activeflag,Lcount,BatchSentdate,batchid,MailCost    
into #temp1    
from #temp left outer join tbltickets tt on    
#temp.FirstName=tt.FirstName    
And #temp.lastname=tt.lastname    
and #temp.midnumber =tt.midnum    
and #temp.BatchSentdate between  convert (datetime, '''+convert( varchar(10),@startListdate,101)+''') and  convert (datetime,''' +convert (varchar(10),@endListdate,101)    
+''') and #temp.lettertype='+convert ( varchar(10),@LetterType)    
    
    
set @sqlquery=@sqlquery+'; Select distinct LCount Total_Mailers,BatchSentDate,batchid,Listdate    
into #temp2    
from #temp1    
    
;  Select distinct count(*) clients,BatchSentdate,batchid    
into #temp3    
from #temp1    
where clientflag=1    
and donotmailflag=0    
group by BatchSentdate,batchid    
    
;  Select distinct count(*) nonclients,BatchSentdate,batchid    
into #temp4    
from #temp1    
where clientflag=0    
and donotmailflag=0    
group by BatchSentdate,batchid    
    
;  Select distinct count(*) quoteclients,BatchSentdate,batchid    
into #temp5    
from #temp1    
where  clientflag=1    
and activeflag=0    
and donotmailflag=0    
group by BatchSentdate,batchid    
    
    
; Select distinct count(*) donotmail,BatchSentdate,batchid    
into #temp6    
from #temp1    
where donotmailflag=1    
group by BatchSentdate,batchid        
    
; Select distinct #temp1.Listdate,#temp1.BatchSentdate,Total_Mailers,    
isnull(Clients,0) Clients, isnull(NonClients,0) NonClients,    
isnull(quoteclients,0)quoteclients,isnull(donotmail,0)donotmail    
from #temp1 left outer join #temp2    
on #temp1.batchid=#temp2.batchid    
and #temp1.batchsentdate=#temp2.batchsentdate    
left outer join #temp3    
on #temp1.batchid=#temp3.batchid    
and #temp1.batchsentdate=#temp3.batchsentdate    
left outer join #temp4    
on #temp1.batchid=#temp4.batchid    
and #temp1.batchsentdate=#temp4.batchsentdate    
left outer join #temp5    
on #temp1.batchid=#temp5.batchid    
and #temp1.batchsentdate=#temp5.batchsentdate    
left outer join #temp6    
on #temp1.batchid=#temp6.batchid    
and #temp1.batchsentdate=#temp6.batchsentdate    
and NonClients>0    
    
;    
drop table #temp    
drop table #temp1    
drop table #temp2    
drop table #temp3    
drop table #temp4    
drop table #temp5    
drop table #temp6    
    
'    
print @sqlquery    
exec (@sqlquery)    
    
  
  














GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


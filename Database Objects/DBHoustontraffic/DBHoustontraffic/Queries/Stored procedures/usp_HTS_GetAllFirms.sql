﻿/****** 
Altered by:  Syed Muhammad Ozair
Business Logic : this procedure is used to get theall the firms
 
List of Columns: 
		FirmID, 
		FirmAbbreviation 
 
******/

 
Alter PROCEDURE  dbo.usp_HTS_GetAllFirms  
AS  
  
Select  FirmID, 
		FirmAbbreviation
		 
From	tblFirm  
order by	FirmAbbreviation  
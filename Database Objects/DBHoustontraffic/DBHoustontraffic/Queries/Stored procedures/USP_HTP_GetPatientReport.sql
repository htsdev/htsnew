﻿/******  
* Created By :	  Fahad Muhammad Qureshi.
* Create Date :   10/15/2010 7:22:45 PM
* Task ID :		  8422
* Business Logic :  This procedure is used to Get Patient Report.
* List of Parameter :
* Column Return :     
* [USP_HTP_GetPatientReport] 0,'AZIZ SHAREPOINT',1,'11/01/2010','11/11/2010','11/01/2010','11/11/2010',0,0
* '
******/

ALTER PROCEDURE [dbo].[USP_HTP_GetPatientReport]
	@isActive TINYINT,
	@LastName varchar(50)=NULL,
	@PatientStatusId INT=0,
	@FromRecDate DATETIME='1/1/1900',
	@ToRecDate DATETIME='1/1/1900',
	@FromLastContactDate DATETIME='1/1/1900',
	@ToLastContactDate DATETIME='1/1/1900',
	@ManufactuerId INT=0,
	@DoctorContacted TINYINT=2,
	@CompanyIds VARCHAR(10)='0'
	
AS
	SELECT p.Id AS PatientId,
       d.Id AS DoctorId,
       p.FirstName,
       p.LastName,
       CASE 
            WHEN ISNULL(p.SurgeryMonth, 0) = '0' THEN ''
            ELSE p.SurgeryMonth + '/' + p.SurgeryDay + '/' + p.SurgeryYear
       END AS SurgeryDate,
       d.FirstName + ' ' + d.LastName AS [DoctorName],
       DoctorContacted = CASE p.DoctorContacted
                              WHEN '1' THEN 'Y'
                              ELSE ''
                         END,
       CASE 
            WHEN ISNULL(p.ManufacturerComments, '') = '' THEN 'N/A'
            ELSE p.ManufacturerComments
       END AS ManufacturerComments,
       p.RecDate AS CreatedDate,
       p.Email,
       p.City,
       PainSeverity = CASE 
                           WHEN p.PainSeverity < 0 THEN ''
                           ELSE CONVERT(VARCHAR, p.PainSeverity)
                      END,
       ps.Description,
       -- Noufil Khan 8469 11/01/2010 Last contact Date column added	       
       CASE 
            WHEN CONVERT(VARCHAR, ISNULL(p.LastContactDate, '01/01/1900'), 101)
                 = '01/01/1900' THEN ''
            ELSE CONVERT(VARCHAR, ISNULL(p.LastContactDate, '01/01/1900'), 101)
       END AS LastContactDate,
       --Ozair 8660 12/13/2010 Source Column added and isactive removed
       CASE 
            WHEN ISNULL(p.SourceId, 0) = 4 THEN ISNULL(p.OtherSource, '')
            ELSE s.[Description]
       END AS Source,
       CASE ISNULL(SpanishSpeaker, 2)
            WHEN 1 THEN 'Y'
            ELSE ''
       END SpanishSpeaker,
       ISNULL(p.SourceId,0) AS SourceId,
       CASE p.PatientStatusId
       WHEN 1 THEN 1
       ELSE 100
       END AS orderByPatientStatus  
       INTO #ResultSet
FROM   Patient p
       LEFT OUTER JOIN Doctor d
            ON  p.DoctorId = d.Id
       LEFT OUTER JOIN PatientStatus ps
            ON  p.PatientStatusId = ps.Id
       LEFT OUTER JOIN Source s
            ON  s.Id = p.SourceId
	                -- Noufil Khan 8469 11/03/2010 FIlter added
	WHERE  
	(
	           @isActive = 2
	           OR (
	                  @isActive <> 2
	                  AND ISNULL(p.IsActive, 0) = ISNULL(@isActive, 0)
	              )
	)
	AND 
	(
		ISNULL(@LastName,'')=''
		OR (ISNULL(@LastName,'')<>'' AND p.LastName LIKE '%'+@LastName+'%')
	)
	AND 
	(
		ISNULL(@PatientStatusId,0)=0
		OR (ISNULL(@PatientStatusId,0)<>0 AND @PatientStatusId=p.PatientStatusId)
	)
	AND 
	(
		ISNULL(@FromRecDate,'1/1/1900')='1/1/1900'
		OR (ISNULL(@FromRecDate,'')<>'1/1/1900' AND DATEDIFF(dd,@FromRecDate,p.Recdate)>=0)
	)
	AND 
	(
		ISNULL(@ToRecDate,'1/1/1900')='1/1/1900'
		OR (ISNULL(@ToRecDate,'')<>'1/1/1900' AND DATEDIFF(dd,@ToRecDate,p.Recdate)<=0)
	)
	AND 
	(
		ISNULL(@FromLastContactDate,'1/1/1900')='1/1/1900'
		OR (ISNULL(@FromLastContactDate,'')<>'1/1/1900' AND DATEDIFF(dd,@FromLastContactDate,p.LastContactDate)>=0)
	)
	AND 
	(
		ISNULL(@ToLastContactDate,'1/1/1900')='1/1/1900'
		OR (ISNULL(@ToLastContactDate,'')<>'1/1/1900' AND DATEDIFF(dd,@ToLastContactDate,p.LastContactDate)<=0)
	)
	AND 
	(
		ISNULL(@ManufactuerId,0)=0
		OR (ISNULL(@ManufactuerId,0)<>0 AND @ManufactuerId=p.ManufactuerId)
	)
	AND 
	(
		ISNULL(@DoctorContacted ,2)=2
		OR (ISNULL(@DoctorContacted ,2)<>2 AND @DoctorContacted =p.DoctorContacted )
	)

	ORDER BY s.id,orderByPatientStatus,p.LastName
	
IF @CompanyIds='3' --Morairity
BEGIN
 SELECT * FROM #ResultSet WHERE SourceId = 2
 UNION ALL
 SELECT * FROM #ResultSet WHERE SourceId <>2 AND SourceId<>0
 UNION ALL
 SELECT * FROM #ResultSet WHERE SourceId = 0 	   	
 
END
ELSE IF @CompanyIds='4' -- Legal Nation
BEGIN
	SELECT * FROM #ResultSet WHERE SourceId=3	   	
	UNION ALL
	SELECT * FROM #ResultSet WHERE SourceId<>3 AND SourceId<>0
	UNION ALL
	SELECT * FROM #ResultSet WHERE SourceId = 0 
	
END
ELSE
BEGIN
	SELECT * FROM #ResultSet WHERE SourceId<>0
	UNION ALL
	SELECT * FROM #ResultSet WHERE SourceId = 0
END	
DROP TABLE #ResultSet
GO




GRANT EXECUTE ON [dbo].[USP_HTP_GetPatientReport]  TO dbr_webuser
GO 	   

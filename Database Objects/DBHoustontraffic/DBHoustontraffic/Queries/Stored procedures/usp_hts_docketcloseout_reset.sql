﻿/************************************************************
 * Code formatted by SoftTree SQL Assistant © v4.0.34
 * Time: 4/14/2009 9:02:14 PM
 ************************************************************/

/****** 
Alter by:  Syed Muhammad Ozair

Business Logic : this procedure is used to update the selected tickets violations one by one 
				 so that their entry in notes are not missed by trigger. 
				 Also IsCourtSettingInfoChangedDuringBSDA is reset for provided cases and then 
				 it is set in case of any change in court date and court number	from previous record
				 
List of Parameters:	
	@TicketViolationIDs : 	
	@EmpID : 
	@CourtDate :
	@CourtLoc :

******/ 
--ozair 4720 09/01/2008
ALTER PROCEDURE dbo.usp_hts_docketcloseout_reset
	@TicketViolationIDs VARCHAR(MAX),
	@EmpID INT,
	@CourtDate DATETIME,
	@CourtLoc INT
AS
	DECLARE @temp TABLE
	        (RowID INT IDENTITY(1, 1) NOT NULL, TVID INT NOT NULL)
	
	INSERT INTO @temp
	  (
	    TVID
	  )
	SELECT MainCat AS tvid
	FROM   dbo.Sap_String_Param(@TicketViolationIDs)
	
	--Ozair 5799 04/14/2009 Getting Old Settings Info before update
	DECLARE @OldVerifiedCourtDate DATETIME
	DECLARE @OldVerifiedCourtNumber VARCHAR(3)
	
	DECLARE @loopcounter INT
	DECLARE @count INT
	
	SET @loopcounter = 1
	SET @count = (
	        SELECT COUNT(RowID)
	        FROM   @temp
	    ) 
	
	--Ozair 5579 04/14/2009 ReSetting IsCourtSettingInfoChangedDuringBSDA flag
	DECLARE @count_reset INT
	DECLARE @loopcounter_reset INT
	SET @loopcounter_reset = 1
	SELECT @count_reset = COUNT(RowID)
	FROM   @temp
	
	WHILE @count_reset >= @loopcounter_reset
	BEGIN
	    UPDATE tblTickets
	    SET    IsCourtSettingInfoChangedDuringBSDA = 0
	    WHERE  ticketid_pk IN (SELECT DISTINCT ttv.TicketID_PK
	                           FROM   tblTicketsViolations ttv
	                           WHERE  ttv.TicketsViolationID = (
	                                      SELECT tvid
	                                      FROM   @temp
	                                      WHERE  rowid = @loopcounter_reset
	                                  ))
	    
	    SET @loopcounter_reset = @loopcounter_reset + 1
	END
	--end Ozair 5579
	
	
	WHILE @loopcounter <= @count
	BEGIN
	    --Ozair 5799 04/14/2009 Getting Old Settings Info before update
	    SELECT @OldVerifiedCourtDate = tv.CourtDateMain,
	           @OldVerifiedCourtNumber = LTRIM(RTRIM(ISNULL(tv.CourtNumbermain, '')))
	    FROM   tblticketsviolations tv
	    WHERE  tv.TicketsViolationID = (
	               SELECT DISTINCT ttv.TicketID_PK
	               FROM   tblTicketsViolations ttv
	               WHERE  ttv.TicketsViolationID = (
	                          SELECT tvid
	                          FROM   @temp
	                          WHERE  rowid = @loopcounter
	                      )
	           )
	    
	    --ozair 5799 04/14/2009	setting IsCourtSettingInfoChanged field	            	    
	    IF (
	           @OldVerifiedCourtDate <> @CourtDate
	           OR @OldVerifiedCourtNumber <> CONVERT(VARCHAR(3), @CourtLoc)
	       )
	    BEGIN
	        UPDATE tblTickets
	        SET    IsCourtSettingInfoChangedDuringBSDA = 1
	        WHERE  ticketid_pk IN (SELECT DISTINCT ttv.TicketID_PK
	                               FROM   tblTicketsViolations ttv
	                               WHERE  ttv.TicketsViolationID = (
	                                          SELECT tvid
	                                          FROM   @temp
	                                          WHERE  rowid = @loopcounter
	                                      ))
	    END
	    
	    UPDATE tblTicketsViolations
	    SET    --Update Court Date            
	           CourtDate = @CourtDate,
	           CourtDateMain = @CourtDate,
	           CourtDateScan = @CourtDate,	--Update Court Location            
	           CourtNumber = @CourtLoc,
	           CourtNumbermain = @CourtLoc,
	           CourtNumberscan = @CourtLoc,	--EmployeeID        
	           VEmployeeID = @EmpID
	    WHERE  TicketsViolationID = (
	               SELECT tvid
	               FROM   @temp
	               WHERE  rowid = @loopcounter
	           )
	    
	    SET @loopcounter = @loopcounter + 1
	END
	
	--end ozair 4720   
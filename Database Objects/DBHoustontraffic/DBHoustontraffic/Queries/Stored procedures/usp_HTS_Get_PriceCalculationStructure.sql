SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_HTS_Get_PriceCalculationStructure]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_HTS_Get_PriceCalculationStructure]
GO










CREATE procEDURE usp_HTS_Get_PriceCalculationStructure   
	(
	@TicketId	int
	)

as

SELECT	tv.RefCaseNumber, 
	v.Description as ViolationDescription, 
	pc.PriceType, 
	pc.CalculationDescription, 
	convert(numeric(10,2),pc.Amount) as Amount
FROM    dbo.tblViolations v 
INNER JOIN
	dbo.tblTicketsViolations tv 
ON 	v.ViolationNumber_PK = tv.ViolationNumber_PK 
RIGHT OUTER JOIN
	dbo.tblPricingCalculator pc 
ON 	tv.TicketsViolationID = pc.TicketsViolationID
WHERE     pc.TicketId_pk =@Ticketid AND pc.Amount<>0.00 -- Haris Ahmed 10330 10/04/2012 Filter 0 Amount






GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


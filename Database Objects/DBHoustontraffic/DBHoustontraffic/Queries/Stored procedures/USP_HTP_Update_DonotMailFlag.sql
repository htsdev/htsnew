USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTP_Update_DonotMailFlag]    Script Date: 12/29/2011 06:29:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Created By:	Unknown

Business Logic:	The stored procedure is used to update the do not mail flag 
				in different databases for returned mailers and insert records 
				in tbl_hts_donotmail_records.

Parameters: 
	UpdateSource : This column will contain information about the application which marked person's address as 'Do Not Mail' (or bad address). 
				1.	1 for USPSTracking Service.
				2.  0 for HTP /Activities /Upload Returned Mailers.


Output columns: 
	@intreccount:	Number of returned mailers uploaded.

*/

      
-- SP_UpdateDoNotMailFlag      
ALTER PROCEDURE [dbo].[USP_HTP_Update_DonotMailFlag]
--Yasir Kamal 6185 07/20/2009 Insert update source column
@UpdateSource TINYINT = 1
AS
	DECLARE @intreccount INT        
	DECLARE @temp TABLE (
	            mailerid INT,
	            recordid INT,
	            courtcategory INT,
	            lettertype INT,
	            --Yasir Kamal 7218 01/13/2010 mailer structure changes 
	            locationid_fk int
	        ) 
	
	-- FIRST INSERT ALL THE MAILER IDs & RECORD IDs THAT UPLOADED TODAY
	-- IN TEMP TABLE      
	
	INSERT INTO @temp
	SELECT DISTINCT n.noteid,
	       n.recordid,
	       c.courtid,
	       n.lettertype,
	       tcc.LocationId_FK
	FROM   tblletternotes n
	       INNER JOIN tblbadmailers b
	            ON  n.noteid = b.mailerid
	       INNER JOIN tblbatchletter c
	            ON  c.batchid = n.batchid_fk
	       INNER JOIN tblCourtCategories tcc
				ON  tcc.CourtCategorynum = c.CourtID     
	WHERE  DATEDIFF(DAY, B.recdate, GETDATE()) = 0 
	UNION
	SELECT DISTINCT n.noteid,
	       d.recordid,
	       c.courtid,
	       n.lettertype,
	       tcc.LocationId_FK
	FROM   tblletternotes n
	       INNER JOIN tblletternotes_detail d
	            ON  d.noteid = n.noteid
	       INNER JOIN tblbatchletter c
	            ON  c.batchid = n.batchid_fk
	       INNER JOIN tblCourtCategories tcc 
				ON tcc.CourtCategorynum = c.CourtID    
	       INNER JOIN tblbadmailers b
	            ON  n.noteid = b.mailerid
	WHERE  DATEDIFF(DAY, B.recdate, GETDATE()) = 0        
	
	SELECT @intreccount = COUNT(DISTINCT mailerid)
	FROM   @TEMP
	--Sabir Khan 6371 08/13/2009 Declare temp tables for getting update count...
	DECLARE @DoNotMailHTP table(Recordid int) -- For HTP
	DECLARE @DoNotMailDTP table(Recordid int) -- For DTP
	DECLARE @DoNotMailJim table(bookingnumber int) -- For Jim
	DECLARE @DoNotMailCivil table(childcustodypartyid int) -- For Civil	
	DECLARE @DoNotMailAssName table(BusinessID int) --For Assumed Name DB
	
	IF @intreccount > 0
	BEGIN
	    ---------------------------------------------------------------------------------------------------------
	    --    HOUSTON DATABASE
	    ---------------------------------------------------------------------------------------------------------      
	    
	    -- NOW UPDATE DONOTMAILFLAG IN TBLTICKETSARCHIVE BY MATCHING RECORD IDs
	    -- IN HOUSTON TRAFFIC TICKETS DATABASE....      
	    UPDATE V
	    SET    v.Donotmailflag = 1
			   OUTPUT INSERTED.recordid into @DoNotMailHTP  --Sabir Khan 6371 08/13/2009 Get updated count...
	    FROM   tblticketsarchive V
	           INNER JOIN @temp t
	                ON  t.recordid = v.recordid
	    WHERE  ISNULL(v.Donotmailflag, 0) = 0 
	           
	           -- tahir 4417 07/16/2008 -- EXCLUDE DALLAS, CIVIL & JIMS CRIMINIAL COURTS....
	           --AND T.courtcategory NOT IN (6, 11, 16, 25, 26)
	           AND t.locationid_fk <> 2
	           AND t.lettertype <> 26
	    
	    
	    
	    -- NOW GET THE MIDNUMBER, ADDRESS & ZIPCODE OF UPDATED CASES IN TEMP TABLE...        
	    SELECT midnumber,
	           address1,
	           zipcode INTO #temp
	    FROM   @TEMP B,
	           tblticketsarchive V
	    WHERE  B.RECORDID = V.recordid 
	           
	           -- tahir 4417 07/16/2008 -- EXCLUDE DALLAS, CIVIL & JIMS CRIMINIAL COURTS....
	           --AND B.courtcategory NOT IN (6, 11, 16, 25, 26)
	           AND b.locationid_fk <> 2
	           AND B.lettertype <> 26
	    
	    -- UPDATE DONOTMAIL FLAG FOR THE ABOVE MIDNUMBER, ADDRESS & ZIPCODE        
	    UPDATE v
	    SET    v.donotmailflag = 1
			   OUTPUT INSERTED.recordid into @DoNotMailHTP  --Sabir Khan 6371 08/13/2009 Get updated count...
	    FROM   #temp T,
	           tblticketsarchive V
	    WHERE  T.midnumber = V.midnumber
	           AND T.address1 = V.address1
	           AND T.zipcode = V.zipcode
	           AND ISNULL(v.Donotmailflag, 0) = 0 
	    
	    DROP TABLE #temp
	    ---------------------------------------------------------------------------------------------------------
	    --    DALLAS DATABASE
	    ---------------------------------------------------------------------------------------------------------      
	    
	    -- NOW UPDATE DONOTMAILFLAG IN TBLTICKETSARCHIVE BY MATCHING RECORD IDs
	    -- IN DALLAS TRAFFIC TICKETS DATABASE....      
	    UPDATE V
	    SET    v.Donotmailflag = 1
			   OUTPUT INSERTED.recordid into @DoNotMailDTP  --Sabir Khan 6371 08/13/2009 Get updated count...
	    FROM   dallastraffictickets.dbo.tblticketsarchive V
	           INNER JOIN @temp t
	                ON  t.recordid = v.recordid
	    WHERE  ISNULL(v.Donotmailflag, 0) = 0
	           --AND T.courtcategory IN (6, 11, 16, 25) -- ONLY DALLAS COURTS....      
	           AND t.locationid_fk = 2
	    
	    
	    -- NOW GET THE MIDNUMBER, ADDRESS & ZIPCODE OF UPDATED CASES IN TEMP TABLE...        
	    SELECT midnumber,
	           address1,
	           zipcode INTO #temp2
	    FROM   @TEMP B,
	           dallastraffictickets.dbo.tblticketsarchive V
	    WHERE  B.RECORDID = V.recordid
	           --AND B.courtcategory  IN (6, 11, 16, 25) -- ONLY DALLAS COURTS....    
	           AND b.locationid_fk = 2
	    
	    
	    -- UPDATE DONOTMAIL FLAG FOR THE ABOVE MIDNUMBER, ADDRESS & ZIPCODE        
	    UPDATE v
	    SET    donotmailflag = 1
			   OUTPUT INSERTED.recordid into @DoNotMailDTP  --Sabir Khan 6371 08/13/2009 Get updated count...
	    FROM   #temp2 T,
	           dallastraffictickets.dbo.tblticketsarchive V
	    WHERE  T.midnumber = V.midnumber
	           AND T.address1 = V.address1
	           AND T.zipcode = V.zipcode
	           AND ISNULL(v.Donotmailflag, 0) = 0 
	    
	    DROP TABLE #temp2
	    ---------------------------------------------------------------------------------------------------------
	    --    JIMS DATABASE
	    ---------------------------------------------------------------------------------------------------------      
	    
	    -- NOW UPDATE DONOTMAILFLAG IN TBLTICKETSARCHIVE BY MATCHING RECORD IDs
	    -- IN DALLAS TRAFFIC TICKETS DATABASE....      
	    UPDATE V
	    SET    v.Donotmailflag = 1
			   OUTPUT INSERTED.bookingnumber INTO @DoNotMailJim  --Sabir Khan 6371 08/13/2009 Get updated count...
	    FROM   jims.dbo.criminalcases V
	           INNER JOIN @temp t
	                ON  t.recordid = v.bookingnumber
	    WHERE  ISNULL(v.Donotmailflag, 0) = 0
	           AND T.courtcategory = 8 -- EXCLUDE DALLAS & HOUSTON COURTS....
	           AND t.lettertype = 26
	    
	    
	    -- NOW GET THE MIDNUMBER, ADDRESS & ZIPCODE OF UPDATED CASES IN TEMP TABLE...        
	    SELECT SPAN,
	           ADDRESS,
	           zip INTO #temp3
	    FROM   @TEMP B,
	           jims.dbo.criminalcases V
	    WHERE  B.RECORDID = V.BOOKINGNUMBER
	           AND B.courtcategory = 8 -- EXCLUDE DALLAS & HOUSTON COURTS....
	           AND b.lettertype = 26
	    
	    -- UPDATE DONOTMAIL FLAG FOR THE ABOVE MIDNUMBER, ADDRESS & ZIPCODE        
	    UPDATE v
	    SET    donotmailflag = 1
			   OUTPUT INSERTED.bookingnumber INTO @DoNotMailJim  --Sabir Khan 6371 08/13/2009 Get updated count...
	    FROM   #temp3 T,
	           jims.dbo.criminalcases V
	    WHERE  T.SPAN = V.SPAN
	           AND T.address = V.address
	           AND T.zip = V.zip
	           AND ISNULL(v.Donotmailflag, 0) = 0 
	    
	    DROP TABLE #temp3
	    ---------------------------------------------------------------------------------------------------------
	    --    CIVIL DATABASE
	    ---------------------------------------------------------------------------------------------------------      
	    
	    -- tahir 4417 07/16/2008
	    -- integrated Civil database with upload returned mailer application.
	    
	    -- NOW UPDATE DONOTMAILFLAG IN CHILDCUSTODYPARTIES BY MATCHING RECORD IDs
	    -- IN CIVIL DATABASE....      
	    UPDATE V
	    SET    v.Donotmailflag = 1
			   OUTPUT INSERTED.childcustodypartyid INTO @DoNotMailCivil --Sabir Khan 6371 08/13/2009 Get updated count...	
	    FROM   civil.dbo.childcustodyparties V
	           INNER JOIN @temp t
	                ON  t.recordid = v.childcustodypartyid
	    WHERE  ISNULL(v.Donotmailflag, 0) = 0
	           AND T.courtcategory = 26 
	    
	    
	    -- NOW GET THE MIDNUMBER, ADDRESS & ZIPCODE OF UPDATED CASES IN TEMP TABLE...        
	    SELECT DISTINCT NAME,
	           ADDRESS,
	           zipcode INTO #temp4
	    FROM   @TEMP B,
	           civil.dbo.childcustodyparties V
	    WHERE  B.RECORDID = V.childcustodypartyid
	           AND B.courtcategory = 26 
	    
	    
	    -- UPDATE DONOTMAIL FLAG FOR THE ABOVE MIDNUMBER, ADDRESS & ZIPCODE        
	    UPDATE v
	    SET    donotmailflag = 1
			   OUTPUT INSERTED.childcustodypartyid INTO @DoNotMailCivil --Sabir Khan 6371 08/13/2009 Get updated count...	
	    FROM   #temp4 T,
	           civil.dbo.childcustodyparties V
	    WHERE  T.name = V.name
	           AND T.address = V.address
	           AND T.zipcode = V.zipcode
	           AND ISNULL(v.Donotmailflag, 0) = 0 
	    
	    -- end 4417
	    DROP TABLE #temp4
	    ---------------------------------------------------------------------------------------------------------
	    --    ASSUMEDNAMES DATABASE
	    ---------------------------------------------------------------------------------------------------------      
	    -- --- Sabir Khan 9921 12/08/2011 Added Assumed Name data base for marking return mails.
	    -- NOW UPDATE NoMailflag IN Business BY MATCHING Business IDs
	    -- IN ASSUMED NAMES DATABASE....      
	    UPDATE B
	    SET    B.NoMailflag = 1
			   OUTPUT INSERTED.BusinessID into @DoNotMailAssName  
	    FROM   AssumedNames.dbo.Business B
	           INNER JOIN @temp t
	           ON  t.recordid = B.BusinessID
	    WHERE  ISNULL(B.NoMailflag, 0) = 0 
	    AND t.locationid_fk <> 2
	    AND t.lettertype <> 26
	   
	   
	   
	    UPDATE B
	    SET    B.NoMailflag = 1			   
	    FROM   AssumedNames.dbo.BusinessOwner B
	           INNER JOIN @temp t
	           ON  t.recordid = B.BusinessID
	    WHERE  ISNULL(B.NoMailflag, 0) = 0 
	    AND t.locationid_fk <> 2
	    AND t.lettertype <> 26
	           
	    
	    -- NOW GET THE ADDRESS,City, State & ZIPCODE OF UPDATED CASES IN TEMP TABLE...        
	    SELECT V.address,
			   V.City,
			   V.[State],
	           V.zip INTO #tempAss
	    FROM   @TEMP B,
	           AssumedNames.dbo.Business V
	    WHERE  B.RECORDID = V.BusinessID
	    
	      INSERT INTO #tempAss
	      SELECT V.OwnerAddress,
			   V.OwnerCity,
			   V.OwnerState,
	           V.OwnerZip
	    FROM   @TEMP B,
	           AssumedNames.dbo.BusinessOwner V
	    WHERE  B.RECORDID = V.BusinessID
	    AND b.locationid_fk <> 2
	    AND B.lettertype <> 26
	    
	    -- UPDATE NoMailflag FLAG FOR THE ABOVE ADDRESS, City, State & ZIPCODE        
	    UPDATE v
	    SET    v.NoMailflag = 1
			   OUTPUT INSERTED.BusinessID  into @DoNotMailAssName 
	    FROM   #tempAss T,
	           AssumedNames.dbo.Business V
	    WHERE  T.ADDRESS = V.[Address]
	           AND T.City = V.City
	           AND T.[state] = V.[State]
	           AND T.[zip] = V.zip
	           AND ISNULL(v.NoMailflag, 0) = 0 
	    
	    DROP TABLE #tempAss
	    ---------------------------------------------------------------------------------------------------------
	    --    Inserting Records in No Mail Flags Table tbl_hts_donotmail_records
	    ---------------------------------------------------------------------------------------------------------
	  ---------------------------------------------------------------------------------------------------------
	    --    Inserting Records in No Mail Flags Table tbl_hts_donotmail_records
	    ---------------------------------------------------------------------------------------------------------
	  
	    --- Sabir Khan 9921 12/08/2011 Inserting records in Donot mail Table logic updated for all records.
	    --- Assumed Name Data base
	    ---------------------------------------------------------------------------------------------------------
	    -- Assumed Name...
	    SELECT DISTINCT BusinessName,	           
	           Address ,
	           CASE 
	                WHEN LEN(Zip) > 5 THEN SUBSTRING(Zip, 1, 5)
	                ELSE Zip
	           END AS zip,
	           city,
	           State
	           INTO #tempAs
	    FROM   AssumedNames.dbo.Business V
	    WHERE  V.NoMailflag = 1
	           AND NOT EXISTS 
	               (
	                   SELECT *
	                   FROM   tbl_hts_donotmail_records tb INNER JOIN tblState ts ON ts.StateID = tb.StateID_FK  
	                   --Sabir Khan 7601 03/22/2010 Check only first character of first name...
					   WHERE  
					   --SUBSTRING(UPPER(firstname),1,1) = SUBSTRING(UPPER(a.firstname),1,1)
					   --AND lastname = V.lastname
	                   tb.ADDRESS = V.Address
	                   AND tb.zip = CASE  WHEN LEN(V.Zip) > 5 THEN 
	                                   SUBSTRING(V.Zip, 1, 5)
	                             ELSE V.Zip
	                             END
	                          AND tb.city = V.city
	                          AND ts.[State] = V.State
	               )
	           AND V.BusinessID IN (SELECT recordid
	                              FROM   @temp)
	    
	    ALTER TABLE #tempAs ADD statid INT
	    
	    UPDATE tas
	    SET tas.statid = ts.StateID
	    FROM #tempAs tas INNER JOIN tblState ts ON ts.[State] = tas.State
	    
	    INSERT INTO tbl_hts_donotmail_records
	      (
	        firstname,	        
	        ADDRESS,
	        zip,
	        city,
	        stateid_fk,UpdateSource
	      )
	    SELECT DISTINCT LEFT(BusinessName,20),	           
	           ADDRESS,
	           zip,
	           city,
	           statid,@UpdateSource AS UpdateSource
	    FROM   #tempAs
	    
	    DROP TABLE #tempAs
	    
	    ---------------------------------------------------------------------------------------------------------
	    -- traffic tickets...
	    SELECT DISTINCT firstname,
	           lastname,
	           address1 + ISNULL(' ' + address2, '') AS ADDRESS,
	           CASE 
	                WHEN LEN(ZipCode) > 5 THEN SUBSTRING(ZipCode, 1, 5)
	                ELSE ZipCode
	           END AS zipcode,
	           city,
	           stateid_fk
	           INTO #temp5
	    FROM   tblticketsarchive a
	    WHERE  a.donotmailflag = 1
	           AND NOT EXISTS 
	               (
	                   SELECT *
	                   FROM   tbl_hts_donotmail_records  
	                   --Sabir Khan 7601 03/22/2010 Check only first character of first name...
	                   WHERE  SUBSTRING(UPPER(firstname),1,1) = SUBSTRING(UPPER(a.firstname),1,1)
	                          AND lastname = a.lastname
	                          AND ADDRESS = a.address1 + ISNULL(' ' + address2, '')
	                          AND zip = CASE 
	                                         WHEN LEN(a.ZipCode) > 5 THEN 
	                                              SUBSTRING(a.ZipCode, 1, 5)
	                                         ELSE a.ZipCode
	                                    END
	                          AND city = a.city
	                          AND stateid_fk = a.stateid_fk
	               )
	           AND a.recordid IN (SELECT recordid
	                              FROM   @temp)
	    
	    INSERT INTO tbl_hts_donotmail_records
	      (
	        firstname,
	        lastname,
	        ADDRESS,
	        zip,
	        city,
	        stateid_fk,UpdateSource
	      )
	    SELECT DISTINCT firstname,
	           lastname,
	           ADDRESS,
	           zipcode,
	           city,
	           stateid_fk,@UpdateSource AS UpdateSource
	    FROM   #temp5
	    
	    DROP TABLE #temp5
	    
	    
	    
	    -- dallas traffic tickets...
	    SELECT DISTINCT firstname,
	           lastname,
	           address1 + ISNULL(' ' + address2, '') AS ADDRESS,
	           CASE 
	                WHEN LEN(ZipCode) > 5 THEN SUBSTRING(ZipCode, 1, 5)
	                ELSE ZipCode
	           END AS zipcode,
	           city,
	           stateid_fk
	           INTO #temp6
	    FROM   dallastraffictickets.dbo.tblticketsarchive a
	    WHERE  a.donotmailflag = 1
	           AND NOT EXISTS 
	               (
	                   SELECT *
	                   FROM   tbl_hts_donotmail_records
	                   WHERE  firstname = a.firstname
	                          AND lastname = a.lastname
	                          AND ADDRESS = a.address1 + ISNULL(' ' + address2, '')
	                          AND zip = CASE 
	                                         WHEN LEN(a.ZipCode) > 5 THEN 
	                                              SUBSTRING(a.ZipCode, 1, 5)
	                                         ELSE a.ZipCode
	                                    END
	                          AND city = a.city
	                          AND stateid_fk = a.stateid_fk
	               )
	           AND a.recordid IN (SELECT recordid
	                              FROM   @temp)
	    
	    INSERT INTO tbl_hts_donotmail_records
	      (
	        firstname,
	        lastname,
	        ADDRESS,
	        zip,
	        city,
	        stateid_fk,UpdateSource
	      )
	    SELECT DISTINCT firstname,
	           lastname,
	           ADDRESS,
	           zipcode,
	           city,
	           stateid_fk,@UpdateSource AS UpdateSource
	    FROM   #temp6
	    
	    DROP TABLE #temp6
	    
	    
	    -- JIMS...
	    SELECT DISTINCT firstname,
	           lastname,
	           ADDRESS,
	           CASE 
	                WHEN LEN(Zip) > 5 THEN SUBSTRING(Zip, 1, 5)
	                ELSE Zip
	           END AS zip,
	           city,
	           s.stateid 
	           INTO #temp7
	    FROM   jims.dbo.criminalcases a
	           INNER JOIN tblstate s
	                ON  s.state = a.state
	    WHERE  a.donotmailflag = 1
	           AND NOT EXISTS 
	               (
	                   SELECT *
	                   FROM   tbl_hts_donotmail_records
	                   WHERE  firstname = a.firstname
	                          AND lastname = a.lastname
	                          AND ADDRESS = a.address
	                          AND zip = CASE 
	                                         WHEN LEN(a.Zip) > 5 THEN SUBSTRING(a.Zip, 1, 5)
	                                         ELSE a.Zip
	                                    END
	                          AND city = a.city
	                          AND stateid_fk = s.stateid
	               )
	           AND a.bookingnumber IN (SELECT recordid
	                                   FROM   @temp)
	    
	    
	    INSERT INTO tbl_hts_donotmail_records
	      (
	        firstname,
	        lastname,
	        ADDRESS,
	        zip,
	        city,
	        stateid_fk,UpdateSource
	      )
	    SELECT DISTINCT firstname,
	           lastname,
	           ADDRESS,
	           zip,
	           city,
	           stateid,@UpdateSource AS UpdateSource
	    FROM   #temp7
	    
	    DROP TABLE #temp7
	    
	    -- Civil...
	    SELECT DISTINCT firstname,
	           lastname,
	           ADDRESS,
	           CASE 
	                WHEN LEN(Zipcode) > 5 THEN SUBSTRING(Zipcode, 1, 5)
	                ELSE Zipcode
	           END zipcode,
	           city,
	           stateid
	           
	           INTO #temp8
	    FROM   civil.dbo.childcustodyparties a
	    WHERE  a.donotmailflag = 1
	           AND NOT EXISTS 
	               (
	                   SELECT *
	                   FROM   tbl_hts_donotmail_records
	                   WHERE  firstname = a.firstname
	                          AND lastname = LEFT(a.lastname, 20)
	                          AND ADDRESS = a.address
	                          AND zip = CASE 
	                                         WHEN LEN(a.ZipCode) > 5 THEN 
	                                              SUBSTRING(a.ZipCode, 1, 5)
	                                         ELSE a.ZipCode
	                                    END
	                          AND city = a.city
	                          AND stateid_fk = a.stateid
	               )
	           AND a.childcustodypartyid IN (SELECT recordid
	                                         FROM   @temp)   
	    
	    INSERT INTO tbl_hts_donotmail_records
	      (
	        firstname,
	        lastname,
	        ADDRESS,
	        zip,
	        city,
	        stateid_fk,UpdateSource
	      )
	    SELECT DISTINCT firstname,
	           LEFT(lastname, 20),
	           ADDRESS,
	           zipcode,
	           city,
	           stateid,@UpdateSource AS UpdateSource
	    FROM   #temp8
	    
	    
	    
	    DROP TABLE #temp8
	END      
	--Sabir Khan 6371 08/13/2009 Get Total count of updated records...
	DECLARE @updatedrec INT
	SELECT @updatedrec = 
		ISNULL((select COUNT(Distinct Recordid) from @DoNotMailHTP ),0) 
		+
		ISNULL((select COUNT(Distinct Recordid) from @DoNotMailDTP ),0)
		+ 
		ISNULL((select COUNT(Distinct bookingnumber) from @DoNotMailJim),0)
		+ 
		ISNULL((select COUNT(Distinct childcustodypartyid) from @DoNotMailCivil),0)
		+ 
		ISNULL((select COUNT(Distinct BusinessID) from @DoNotMailAssName),0) --Sabir Khan 9921 12/08/2011 Added Count for Assumed Name records.
	
	SELECT ISNULL(@updatedrec,0) AS UpdateCount
	
	
	--SELECT ISNULL(@intreccount, 0) AS UpdateCount

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Get_TrialLetterNotPrinted]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Get_TrialLetterNotPrinted]
GO




Create  procedure [dbo].[USP_HTS_Get_TrialLetterNotPrinted]    
    
as    
    
    
declare @temp table ( ticketid int, batchprint int, individualprint int, noletter int)    
declare @notprinted table ( ticketid int)   
declare @notes table ( ticketid int) 
declare @violations table (ticketid int, violcount int, ticketnumber varchar(30), courtdate datetime, courtlocation varchar(100), courtStatus varchar(20))    
    
insert into @temp (ticketid,batchprint, individualprint,noletter)    
select distinct    
  t.ticketid_pk as Ticketid,    
  0 as batchprint,     
  0 as individualprint,
  0 as noletter    
from tbltickets t    
inner join     
  tblticketsviolations v    
on  t.ticketid_pk = v.ticketid_pk    
inner join    
  tblcourtviolationstatus s    
on  s.courtviolationstatusid = v.courtviolationstatusidmain
where t.activeflag = 1    
and  v.courtdatemain > getdate()    
and  v.courtid in (3001,3002,3003)    
and  s.categoryid = 4  
    
union     
    
select distinct    
  t.ticketid_pk as Ticketid,    
  0 as batchprint,     
  0 as individualprint,
  0 as noletter    
from tbltickets t    
inner join     
  tblticketsviolations v    
on  t.ticketid_pk = v.ticketid_pk    
inner join    
  tblcourtviolationstatus s    
on  s.courtviolationstatusid = v.courtviolationstatusidmain    
where t.activeflag = 1    
and  v.courtdatemain > getdate()    
/* EXCLUDE SPECIFIC COURTS....    
3037 - Harris County Criminal Court     
3036 - Montgomery County Criminal Court    
3043 - Fort Bend County Court    
3047 - ALR Hearing - Conroe      
3052 - Harris County Tax Courtroom    
3055 - JUVENILE COURT HARRIS CTY    
3057 - DPS DL Suspension Hearings    
3058 - Galveston County Criminal Court    
*/    
and  v.courtid not in (3001,3002,3003,3037,3036,3043,3047,3052,3055,3057,3058)    
and  s.categoryid in(2,3,4,5)    
    
order by  [ticketid]    

update t    
set  noletter = 1    
from @temp t    
inner join     
  tblticketsflag f    
on  f.ticketid_pk = t.ticketid    
and  f.flagid=7    

    
update t    
set  batchprint = 1    
from @temp t    
inner join     
  tblhtsbatchprintletter b    
on  b.ticketid_fk = t.ticketid    
and  b.letterid_fk = 2    
and  b.deleteflag =0    
--and  b.isprinted = 0    

insert into @notes(ticketid) 
select distinct n.ticketid from tblticketsnotes n inner join @temp t
on t.ticketid=n.ticketid 
where	(    
  ltrim(n.subject) like 'trial letter printed%'     
  or     
  subject like '%Trial Notification Letter Printed%'    
  ) 
and n.subject not like '%general notes%'    
and		n.subject not like '%contact notes%'    
and		n.subject not like '%trial notes%'    
and		n.subject not like '%setting notes%'

    
update t    
set  individualprint = 1    
from @temp t    
inner join     
  @notes n    
on  n.ticketid = t.ticketid 
   
 
    
insert into @notprinted(ticketid)    
select distinct ticketid from @temp where batchprint = 0 and individualprint = 0 and noletter=0 order by ticketid     
    
insert into @violations (ticketid, violcount, ticketnumber, courtdate, courtlocation, courtStatus)    
select v.ticketid_pk, count(ticketsviolationid), min(refcasenumber), min(courtdatemain),
   MIN(c.ShortName),MIN(cvs.ShortDescription)
from tblticketsviolations v inner join @notprinted t    
on t.ticketid = v.ticketid_pk    INNER JOIN
dbo.tblCourts AS c ON v.CourtID = c.Courtid INNER JOIN
dbo.tblCourtViolationStatus AS cvs ON v.CourtViolationStatusIDmain = cvs.CourtViolationStatusID

group by v.ticketid_pk    
    
select distinct t.ticketid_pk, t.lastname + ', '+t.firstname as ClientName, t.midnum, v.ticketnumber, v.courtdate, 
v.violcount, v.courtlocation, v.courtStatus 
from tbltickets t    
inner join     
  @notprinted a    
on  a.ticketid = t.ticketid_pk    
inner join    
  @violations v    
on  v.ticketid = t.ticketid_pk   
  
order by v.courtdate desc,  [ClientName]    
    
    
    

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


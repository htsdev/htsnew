SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_All_Court__backup]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_All_Court__backup]
GO


CREATE PROCEDURE [dbo].[usp_Get_All_Court] 

AS

Select Courtid, CourtName, ShortName
from tblCourts
Order By Courtid

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


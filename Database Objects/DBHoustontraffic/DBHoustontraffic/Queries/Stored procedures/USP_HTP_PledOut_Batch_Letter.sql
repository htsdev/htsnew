  
  
  
/******     
    
Alter by:  Zeeshan Ahmed    
    
Business Logic : This procedure is used to get cases information for pled out letter     
				 Display Violations which have missed court status    
List of Parameters:         
    
 @empid  : Employee ID     
 @LetterType : Letter Type    
 @TicketIDList : Ticket Id's of the cases. (i.e. primary key of the case).     
    
List of Columns:     
    
 TicketID_PK : TicketId    
 FirstName : First Name     
 LastName : Last Name    
 Address1 : Address     
 City : City    
 Zip : Zip     
 State : State    
 RefcaseNumber : Ref case numbers         
 CourtDate : Court Date    
 CourtPhoneNumber : Court Phone Number    
     
******/    
--USP_HTP_PledOut_Batch_Letter 3992,12,'1639843636,18744043519,1619043637,844543507,15885943502,'                            
  
Alter procedure [dbo].[USP_HTP_PledOut_Batch_Letter]     
@empid int,     
@LetterType int,     
@TicketIDList varchar(8000)     
     
as     
begin     
declare @Counter int    
declare @CaseNumber varchar(1000)    
declare @CourtPhone varchar(200)  
     
declare @tickets table (ticketid numeric)     
     
insert into @tickets     
 select * from dbo.Sap_String_Param_New(@TicketIDList)     
     
create table #Temp_CauseNo    
(TicketID_PK int, FirstName varchar(50), LastName varchar(50), Address1 varchar(150), City varchar(50), Zip varchar(12), State varchar(20), RefcaseNumber varchar(1000),     
languageSpeak varchar(15), CourtDate datetime, CourtNumberMain varchar(10), Location varchar(150),CourtPhoneNumber varchar(50), BatchID_pk varchar(50),RowID int identity (1,1))    
    
insert into #Temp_CauseNo    
select T.TicketID_PK , T.FirstName , T.LastName , T.Address1 ,T.City , T.Zip ,S1.State, TV.RefcaseNumber , T.languageSpeak, Max(TV.CourtDateMain) , TV.CourtNumberMain  
 , C.Address + ', ' + C.City + ', '+S2.State +' ' + C.Zip as Location, dbo.fn_FormatContactNumber_new(isnull(c.Phone,''),-1) ,  
 BP.BatchID_pk from     
tblHTSBatchPrintLetter BP     
join tbltickets T     
on T.TicketID_Pk = BP.Ticketid_FK     
join tblticketsviolations TV     
On T.TicketID_Pk = TV.Ticketid_pk     
join tblcourtviolationstatus CVS     
On CVS.courtviolationstatusid = TV.CourtViolationStatusidmain     
join tblcourts C     
on C.courtID = TV.CourtID     
join tblState S1     
on S1.Stateid = t.StateID_fk     
join tblState S2     
on S2.Stateid = C.State    
where     
cast(convert(varchar(12), BP.ticketid_fk) + convert(varchar(12), BP.BatchID_pk) as numeric)     
IN (select ticketid from @tickets)     
and letterid_fk = @LetterType 
and     TV.CourtViolationStatusidmain      = 105
group by T.TicketID_PK , T.FirstName , T.LastName , T.Address1 ,T.City , T.Zip ,S1.State, TV.RefcaseNumber , T.languageSpeak, TV.CourtDateMain, TV.CourtNumberMain   
 , C.Address + ', ' + C.City + ', '+S2.State +' ' + C.Zip, dbo.fn_FormatContactNumber_new(isnull(c.Phone,''),-1),BP.BatchID_pk    
order by BP.BatchID_pk  
    
Select @Counter = count(RowID) from #Temp_CauseNo    
  
While @Counter > 0    
 Begin    
  Set @CaseNumber = ''    
  Set @CourtPhone = ''  
  -- Selecting distinct court phone number(s) for specific ticket  
  Select @CourtPhone = @CourtPhone + dbo.fn_FormatContactNumber_new(isnull(c.Phone,''),-1) + '/'  
  from tblticketsviolations TV      
  join tblcourts C      
  on C.courtID = TV.CourtID      
  where       
  TV.Ticketid_pk in (Select TicketID_PK from #Temp_CauseNo where RowID = @Counter)  
  group by c.Phone  
  -- triming last '/'  
  set @CourtPhone = left(@CourtPhone, len(@CourtPhone)- 1)  
  
  -- Selecting distinct cause number(s) for specific ticket  
  select @CaseNumber = @CaseNumber + RefcaseNumber + ', ' from #Temp_CauseNo     
  where TicketID_PK in (Select TicketID_PK from #Temp_CauseNo where RowID = @Counter)    
  group by RefcaseNumber    
  -- triming last comma  
  set @CaseNumber = left(@CaseNumber, len(@CaseNumber)- 1)    
  
  update #Temp_CauseNo set RefcaseNumber = @CaseNumber, CourtPhoneNumber = @CourtPhone where TicketID_PK in (Select TicketID_PK from #Temp_CauseNo where RowID = @Counter)    
  
  Set @Counter = @Counter - 1    
 End    
  
Alter Table #Temp_CauseNo Drop Column rowid    
  
  
declare @temp table (   
TicketID_PK int,  
FirstName varchar(50),  
LastName varchar(50),   
Address1 varchar(150),   
City varchar(50),  
Zip varchar(12),   
State varchar(20),   
RefcaseNumber varchar(1000),     
languageSpeak varchar(15),  
CourtDate datetime,   
CourtNumberMain varchar(10),   
Location varchar(150),  
CourtPhoneNumber varchar(50),  
batchId_Pk int,  
[filename] varchar(30),      
imgdata [image] NULL      
)      
  
insert into  @temp    
Select Distinct TicketID_PK ,  FirstName , LastName , Address1 ,City , Zip ,State, RefcaseNumber ,       
languageSpeak,  Max(CourtDate) CourtDate, CourtNumberMain ,  
Address1 + ', ' + City + ', '+State +' ' + Zip  as Location,CourtPhoneNumber,       
BatchID_pk ,  
convert(varchar(20),sb.ScanBookID) +'-'+ convert(varchar(20),sp.DOCID) + '.'+ sp.DocExtension as [filename],      
null as imgdata  
from #Temp_CauseNo t  
left outer join  tblScanBook sb on t.TicketId_PK = sb.TicketID and sb.DocTypeID  = 16          
left outer join  tblScanPIC sp      
on sb.ScanBookId = sp.ScanBookId  and sp.DocExtension = 'JPG'  
group by TicketID_PK ,  FirstName , LastName , Address1 ,City , Zip ,State, RefcaseNumber ,       
languageSpeak, CourtNumberMain ,  
Address1 + ', ' + City + ', '+State +' ' + Zip, CourtPhoneNumber,   
BatchID_pk ,  
convert(varchar(20),sb.ScanBookID) +'-'+ convert(varchar(20),sp.DOCID) + '.'+ sp.DocExtension  
order by BatchID_pk    
  
select * from @temp     
    
Drop Table #Temp_CauseNo    
End  
  
/******   
Added in Source Safe by:  Fahad Muhammad Qureshi
Business Logic : This procedure update the History Note for the given ticket id.
  
List of Parameters: 
		@TicketID   : Ticket ID of the Case.     
		@Subject	: Subject of the Note.	
		@EmpID		: Employee Id of the Employee.  	
		@Note		: Histroy Note which have to update. 
		@ContactID	: Represents ConcatctID, if its values is 0 then single insertion in tblticketsnotes will perform aginst the ticketId,
					  if its value is 1 then multiple the sp will retrieve all the ticket ids from ContactID and then perform all entries in 
					  tblticketsnotes table.
******/ 

ALTER procedure usp_HTS_Insert_CaseHistoryInfo 
@TicketID       int,
@Subject	varchar(500),
@EmpID   	int,
@Note		VARCHAR,
@ContactID INT =0	--SAEED 7844 06/02/2010, contactID parameter added.
AS

--SAEED 7844 06/02/2010, place the code inside contactID check, if contactID='0' which bydefault the same code will run as it was running earlier.
-- in case if contactID<>'0' then it will get all the ticketid_pk from tbltickets and then perform update/insert operation against each ticketid.

IF @ContactID=0
begin
	insert into tblticketsnotes(TicketID,Subject,EmployeeID,Notes) values (@TicketID,@Subject,@EmpID,@Note)
END
ELSE
BEGIN
	DECLARE @temp TABLE	(id INT IDENTITY(1,1),ticketID INT)
	DECLARE @counter INT
	DECLARE @currentTicketID INT
	SET @counter=1
	INSERT INTO @temp
	SELECT ticketid_pk FROM tblTickets tt WHERE tt.ContactID_FK=@ContactID
	WHILE @counter<=(SELECT COUNT(id) FROM @temp)
	BEGIN
		SELECT @currentTicketID=ticketID FROM @temp WHERE id=@counter
		insert into tblticketsnotes(TicketID,Subject,EmployeeID,Notes) values (@currentTicketID,@Subject,@EmpID,@Note)	
		SET @counter=@counter + 1
	END
END
--SAEED 7844 END






﻿/*  
* Created By		: SAEED AHMED
* Task ID			: 7791  
* Created Date		: 05/29/2010  
* Business logic	: This procedure takes subemenu ID as an input parameter and returns true if the parent menu is 'VALIDATION', otherwise returns false.
*					: 
* Parameter List  
* @MENUID			: Represents SubMenuID.
* USP_HTP_ParentMenuIsValidation 18
*/
Create PROC dbo.USP_HTP_ParentMenuIsValidation
@MENUID INT
AS
DECLARE @isExists AS BIT
DECLARE @ParentID AS INT
SET @isExists='0'

	SELECT @ParentID=menuid FROM tbl_TAB_subMENU WHERE id=@MENUID
	IF @ParentID=18 OR @MENUID=18		-- 18 = Validation menu ID 
	BEGIN
		SET @isExists='1'	
	END

SELECT @isExists AS isValidationMenu

GO
GRANT EXECUTE ON dbo.USP_HTP_ParentMenuIsValidation TO dbr_webuser
GO
  
  
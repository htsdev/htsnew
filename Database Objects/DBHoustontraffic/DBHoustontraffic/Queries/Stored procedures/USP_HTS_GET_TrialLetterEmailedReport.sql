
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******  
Altered by:		    Fahad Muhammad Qureshi
Business Logic :    This procedure is used to Get Data for all those client to which we have sent Trial Notification Email.
TaskId:				7167     
List of Parameters: 
					@stEmailedDate    : Starting date to get records for the report.
					@endEmailedDate   : Ending date to get records for the report.
******/
ALTER PROCEDURE [dbo].[USP_HTS_GET_TrialLetterEmailedReport]
	@stEmailedDate DATETIME,
	@endEmailedDate DATETIME
AS
SET @stEmailedDate = @stEmailedDate + '00:00:00'
	SET @endEmailedDate = @endEmailedDate + '23:59:59'
	
	SELECT temail.TicketID_PK,
	       ISNULL(t.Firstname, '') + ' ' + ISNULL(t.Lastname, '') AS ClientName,
	       ISNULL(temail.emailaddress, t.Email) AS Email,
	       dbo.fn_DateFormat(temail.EmailDate, 24, '/', ':', 1) AS 
	       TrialLetterEmailedDate,
	       dbo.tblCourtViolationStatus.ShortDescription AS CourtStatus,
	       dbo.tblCourts.ShortName AS CourtLoc,
	       ISNULL(dbo.tblTicketsViolations.CourtDateMain, '') AS CourtDateMain,
	       ISNULL(dbo.tblTicketsViolations.courtnumbermain, '') AS 
	       courtnumbermain,
	       (
	           dbo.fn_DateFormat(
	               ISNULL(dbo.tblTicketsViolations.CourtDateMain, ''),
	               24,
	               '/',
	               ':',
	               1
	           ) + '  #  '
	       ) + (
	       	-- kashif 8736 02/28/2011 remove convert varchar value to int b/c courtnumbermain may be alphanumeric and conversion failed at procedure call
			-- and replace below code with coalesece in case of null values	       	
--	           dbo.Formatcourtnumbers(
--	               CONVERT(
--	                   INT,
--	                   ISNULL(
--	                       dbo.Formatcourtnumbers(dbo.tblTicketsViolations.courtnumbermain),
--	                       0
--	                   )
--	               )
--	           ) 

			COALESCE(dbo.tblTicketsViolations.courtnumbermain,
	                     '0'
	                     )
	           
	           + '  ' + dbo.tblCourtViolationStatus.ShortDescription
	       ) AS VerifiedCourtInfo,	--Fahad 6429 09/07/2009 Concatenate all setting info of the client
	       ISNULL(tvs.Description, '') AS violationdescription,
	       dbo.tblTicketsViolations.casenumassignedbycourt,
	       dbo.tblTicketsViolations.RefCaseNumber,
	       CASE ISNULL(CONVERT(VARCHAR(30), temail.Confirmationdate), '0')
	            WHEN '0' THEN '0'
	            ELSE '1'
	       END AS confirmationid,
	       --Ozair 7405 02/16/2010 Date Format set accordign to DSD
	       dbo.fn_DateFormat(temail.ConfirmationDate, 24, '/', ':', 1) AS ConfirmationDate,	       
	       ISNULL(temail.emailid, '') AS emailid
	FROM   dbo.tblTicketsViolations
	       INNER JOIN dbo.tblTickets AS t
	            ON  dbo.tblTicketsViolations.TicketID_PK = t.TicketID_PK
	       INNER JOIN dbo.tblCourts
	            ON  dbo.tblTicketsViolations.CourtID = dbo.tblCourts.Courtid
	       INNER JOIN dbo.tblCourtViolationStatus
	            ON  dbo.tblTicketsViolations.CourtViolationStatusID = dbo.tblCourtViolationStatus.CourtViolationStatusID
	       LEFT OUTER JOIN dbo.tblEmailTrialNotification AS temail
	            ON  dbo.tblTicketsViolations.TicketID_PK = temail.TicketID_PK
	       INNER JOIN tblviolations tvs
	            ON  dbo.tblTicketsViolations.ViolationNumber_PK = tvs.ViolationNumber_PK
	WHERE  
	--temail.EmailDate BETWEEN @stEmailedDate AND @endEmailedDate
	--Fahad 02/15/2010 7405 remove between clause and use date diff method
			DATEDIFF(DAY, @stEmailedDate, temail.EmailDate) >= 0
	                  AND DATEDIFF(DAY, temail.EmailDate, @endEmailedDate) >= 0
	ORDER BY
	       temail.EmailDate DESC
GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


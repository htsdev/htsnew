﻿/******    
* Created By :   Saeed Ahmed.  
* Create Date :   11/05/2010   
* Task ID :    8501  
* Business Logic :  This procedure is used to add doctor detial.  
* List of Parameter : @LastName  
*     : @LastName   
     : @FirstName  
     : @Address   
     : @City   
     : @State  
     : @Zip   
  
* Column Return :       
******/  
-- USP_HTP_AddDoctorDetail   
alter PROC  dbo.USP_HTP_AddDoctorDetail  
@LastName VARCHAR(100),  
@FirstName VARCHAR(100),  
@Address VARCHAR(200),  
@City VARCHAR(50),  
@State INT,  
@Zip VARCHAR(10)  ,
@id INT OUTPUT
as  
  
INSERT INTO Doctor  
(  
 LastName,  
 FirstName,  
 [Address],  
 City,  
 [State],  
 Zip  
  
)  
VALUES  
(  
   
 @LastName,  
 @FirstName,   
 @Address,  
 @City,  
 @State,  
 @Zip  
   
)  
SET @id=SCOPE_IDENTITY() 
SELECT @id


go
grant execute on dbo.USP_HTP_AddDoctorDetail to dbr_webuser
go
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_PAYMENTINFO_GET_SPLITCaseCount]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_PAYMENTINFO_GET_SPLITCaseCount]
GO


CREATE Procedure [dbo].[USP_HTS_PAYMENTINFO_GET_SPLITCaseCount]  
  
@ticketid int  
  
as  
-- CHANGED BY TAHIR AHMED DT: 10/06/2006  
-- SPLIT CASE MEANS TICKETID HAVING DIFFERENT UNDERLYING CASE STATUS

/*
select count (distinct convert(varchar(20), courtdatemain,101) + courtnumbermain)  as SplitCount
 from tblticketsviolations  
  where ticketid_pk = @ticketid  
*/

declare @cnt int

select @cnt= count (distinct courtviolationstatusidmain)  
 from tblticketsviolations  
  where ticketid_pk = @ticketid  

if @cnt > 1 
	select 1
else
	select 0
  


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


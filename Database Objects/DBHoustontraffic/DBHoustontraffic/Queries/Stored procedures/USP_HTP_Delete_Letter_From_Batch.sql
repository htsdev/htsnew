/************  

Created By : Zeeshan Ahmed 

Business Logic : This Procedure is used to delete exisiting letter from the batch.

List of Paremeters :

	@LetterType : Letter Type
	@TicketId : Case Number	
	@EmpID : Employee ID 
	
************/
  
Create  procedure dbo.USP_HTP_Delete_Letter_From_Batch    
@LetterType int,      
@TicketId int,
@EmpId int
as    
BEGIN    

	If @LetterType = 11 Or @LetterType = 12
		Begin
			update tblhtsbatchprintletter    
			set deleteflag = 1    
			where letterid_fk In (11 ,12)     
			and ticketid_fk = @TicketId and isnull(deleteflag,0) = 0 and isprinted=0 
		END  
End  

go

grant exec on dbo.USP_HTP_Delete_Letter_From_Batch to dbr_webuser

go
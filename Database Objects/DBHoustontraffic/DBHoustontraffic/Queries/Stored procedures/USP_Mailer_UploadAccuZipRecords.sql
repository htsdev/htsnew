if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_Mailer_UploadAccuZipRecords]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_Mailer_UploadAccuZipRecords]
GO

CREATE procedure [dbo].[USP_Mailer_UploadAccuZipRecords]

(
@first varchar(200), @address varchar(200), @city varchar(200), @middle varchar(200), @last varchar(200), @st varchar(200), @zip varchar(200), @record_id varchar(200), @case_type varchar(200), @crrt varchar(200), @barcode varchar(200), @x varchar(200), @status_ varchar(200), @errno_ varchar(200), @type_ varchar(200), @lacs_ varchar(200), @company varchar(200), @ocompany varchar(200), @oaddress varchar(200), @ocity varchar(200), @ostate varchar(200), @ozipcode varchar(200), @dpv_ varchar(200), @dpvnotes_ varchar(200), @ffapplied_ varchar(200), @movetype_ varchar(200), @movedate_ varchar(200), @matchflag_ varchar(200), @nxi_ varchar(200), @ank_ varchar(200)
)

as

declare @iCaseType int, @iRecordId int

set @iCaseType = convert(int, ltrim(rtrim(replace(@case_type,'"',''))))
set @iRecordId = convert(int, ltrim(rtrim(replace(@record_id,'"',''))))
declare @exists int

select @exists = count(*) from tblmaileraccuziptemp
where case_type = @iCaseType
and record_id = @iRecordId

if @exists = 0 
begin
	insert into dbo.tblmaileraccuziptemp 
		(
		first, address, city, middle, last, st, zip, record_id, case_type, crrt, barcode, x, 
		status_, errno_, type_, lacs_, company, ocompany, oaddress, ocity, ostate, ozipcode, 
		dpv_, dpvnotes_, ffapplied_, movetype_, movedate_, matchflag_, nxi_, ank_
		)
	select replace(@first,'"','') ,replace(@address,'"','') ,replace(@city,'"','') ,replace(@middle,'"','') ,
		replace(@last,'"','') ,replace(@st,'"','') ,replace(@zip,'"','') ,@iRecordId ,
		@iCaseType ,replace(@crrt,'"','') ,replace(@barcode,'"','') ,replace(@x,'"','') ,
		replace(@status_,'"','') ,replace(@errno_,'"','') ,replace(@type_,'"','') ,replace(@lacs_,'"','') ,
		replace(@company,'"','') ,replace(@ocompany,'"','') ,replace(@oaddress,'"','') ,replace(@ocity,'"','') ,
		replace(@ostate,'"','') ,replace(@ozipcode,'"','') ,replace(@dpv_,'"','') ,replace(@dpvnotes_,'"','') ,
		replace(@ffapplied_,'"','') ,replace(@movetype_,'"','') ,replace(@movedate_,'"','') ,replace(@matchflag_,'"','') ,
		replace(@nxi_,'"','') ,replace(@ank_,'"','')
end

GO
grant execute on [dbo].[USP_Mailer_UploadAccuZipRecords] to dbr_webuser
GO


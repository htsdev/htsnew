﻿USE TrafficTickets
GO
/****** 
Create by		: Abid Ali
Created Date	: 11/14/2008
TasK			: 5138

Business Logic : This procedure simply return the violations records having violationstatusID

List of Parameters:	
	@TicketID			: id against which records are updated
	@EmployeeID			: employee id of current logged in user
	@ViolationNumber	: For Violation Status records

Column Return : 
			TicketID_PK
			Firstname
			MiddleName
			Lastname			
			TicketNumber_PK
			FirmName
			Address1
			Address2
			Ccity
			cState
			Czip
			CourtDate
			CourtNumber
			Address
			City
			Zip        
			State   
			Datetypeflag
			Activeflag       
			Languagespeak         
			SequenceNumber
			CategoryID 
			TicketViolationDate
			Description
			CaseStatus    
			courtid
			ViolationNumber_PK
			ViolationDescription
******/


CREATE Procedure [dbo].[USP_HTP_TrialletterDL]
(
	@TicketID int,
	@EmployeeID int,
	@ViolationNumber int = 0
)
AS

	SELECT 
			dbo.tblTickets.TicketID_PK,
			dbo.tblTickets.Firstname,
			dbo.tblTickets.MiddleName,
			dbo.tblTickets.Lastname,			
			dbo.tblTicketsViolations.RefCaseNumber AS TicketNumber_PK,
			(CASE	WHEN dbo.tblTickets.FirmID = 3000 
					THEN (dbo.tblTickets.Firstname +' '+  dbo.tblTickets.MiddleName + ' '+dbo.tblTickets.Lastname) 
					ELSE (SELECT FirmName FROM tblfirm WHERE tblfirm.FirmID =dbo.tblTickets.FirmID)
					END) AS FirmName,    
			(CASE	WHEN dbo.tblTickets.FirmID = 3000 
					THEN dbo.tblTickets.Address1 
					ELSE (SELECT Address FROM tblfirm WHERE tblfirm.FirmID =dbo.tblTickets.FirmID)  
					END) AS Address1,    
			(CASE	WHEN dbo.tblTickets.FirmID = 3000 
					THEN dbo.tblTickets.Address2 
					ELSE (SELECT Address2 FROM tblfirm WHERE tblfirm.FirmID =dbo.tblTickets.FirmID) 
					END) AS Address2,    
			(CASE	WHEN dbo.tblTickets.FirmID = 3000 
					THEN dbo.tblTickets.City	 
					ELSE (SELECT City FROM tblfirm WHERE tblfirm.FirmID =dbo.tblTickets.FirmID) 
					END) AS Ccity,    
			(CASE	WHEN dbo.tblTickets.FirmID = 3000 
					THEN dbo.tblState.State		 
					ELSE (SELECT dbo.tblState.State WHERE dbo.tblState.StateID =(SELECT state FROM dbo.tblfirm WHERE dbo.tblfirm.FirmID =dbo.tblTickets.FirmID)) 
					END) AS cState,    
			(CASE	WHEN dbo.tblTickets.FirmID = 3000 
					THEN dbo.tblTickets.Zip		 
					ELSE (SELECT Zip FROM tblfirm WHERE tblfirm.FirmID =dbo.tblTickets.FirmID) 
					END) AS Czip,
			dbo.tblTicketsViolations.CourtDate,          
			dbo.tblTicketsViolations.CourtNumber,         
			dbo.tblCourts.Address,         
			dbo.tblCourts.City,         
			dbo.tblCourts.Zip,         
			dbo.tblState.State,         
			'0' AS Datetypeflag,         
			dbo.tblTickets.Activeflag,         
			isnull( dbo.tblTickets.LanguageSpeak, 'ENGLISH') AS Languagespeak,         
			'0' AS SequenceNumber,         
			dbo.tblCourtViolationStatus.CategoryID,         
			dbo.tblTicketsViolations.TicketViolationDate,         
			dbo.tblViolations.Description,         
			dbo.tblDateType.Description AS CaseStatus ,    
			dbo.tblTicketsViolations.courtid AS courtid,
			dbo.tblTicketsViolations.ViolationNumber_PK,
			dbo.tblViolations.Description AS ViolationDescription
	FROM    
						dbo.tblTickets
			INNER JOIN	dbo.tblTicketsViolations
					ON	dbo.tblTickets.TicketID_PK = dbo.tblTicketsViolations.TicketID_PK
			INNER JOIN	dbo.tblCourts
					ON  dbo.tblTicketsviolations.courtid = dbo.tblCourts.Courtid
			INNER JOIN	dbo.tblState
					ON  dbo.tblTickets.Stateid_FK = dbo.tblState.StateID
					AND  dbo.tblCourts.State = dbo.tblState.StateID         
			INNER JOIN	dbo.tblCourtViolationStatus         
					ON  dbo.tblTicketsViolations.CourtViolationStatusID = dbo.tblCourtViolationStatus.CourtViolationStatusID         
			INNER JOIN	dbo.tblViolations         
					ON  dbo.tblTicketsViolations.ViolationNumber_PK = dbo.tblViolations.ViolationNumber_PK         
			INNER JOIN	dbo.tblDateType         
					ON  dbo.tblCourtViolationStatus.CategoryID = dbo.tblDateType.TypeID
	WHERE   
			dbo.tblTickets.Activeflag = 1
			AND     
			dbo.tblCourtViolationStatus.CategoryID IN (2, 3, 4, 5)
			AND
			dbo.tblTickets.TicketID_PK = @TicketID
			AND
			dbo.tblTicketsViolations.ViolationNumber_PK = @ViolationNumber
go

grant exec on [dbo].[USP_HTP_TrialletterDL] to dbr_webuser
go
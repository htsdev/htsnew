/**  
* Nasir 5864 06/25/2009  
* Business Logic: This procedure is use to get data for ALR Check Request Letter  
* 
* List of parameter:
* First Name, 
* Last name,
* Rep First Name, 
* Rep Last name,
* Case number,
* Court Date
* Request Date, 6 days before court date

**/    

alter PROCEDURE dbo.USP_HTP_GET_ALR_Check_Request_Letter
	@TicketID INT,
	@EmpID int 
AS
	SELECT distinct tt.ALROfficerName AS OfficerName
	      ,CASE 
			WHEN ISNULL(tt.ALRMileage,0) <= 25 THEN 10
			WHEN ISNULL(tt.ALRMileage,0) > 25 THEN (0.55 * tt.ALRMileage + 10)
			END  AS Amount
	      ,(Select tu.Firstname from tblusers tu where tu.employeeid = @EmpID) as RepFirstName
	      ,(Select tu.Lastname  from tblusers tu where tu.employeeid = @EmpID) AS RepLastName
	      ,tt.Firstname
	      ,tt.Lastname
	      --,CASE ttv.casenumassignedbycourt WHEN '' THEN ttv.RefCaseNumber ELSE ttv.casenumassignedbycourt end AS CaseNumber
	      ,dbo.Fn_HTP_Get_CauseNumbersWithComma(tt.ticketid_pk, ttv.CourtDateMain)  AS CaseNumber
	      ,ttv.CourtDateMain
	      ,dbo.fn_getlastbusinessday(ttv.CourtDateMain,6) AS RequestDate
	      --Waqas 6342 08/13/2009 observing officer
	      ,ISNULL(tt.IsALRArrestingObservingSame, '0') AS IsALRArrestingObservingSame
	      ,tt.ALRObservingOfficerName AS ObservingOfficerName	   
	FROM   tblTickets tt
	       INNER JOIN tblTicketsViolations ttv
	            ON  ttv.TicketID_PK = tt.TicketID_PK	       	       
	WHERE  tt.TicketID_PK = @TicketID
	AND ttv.ViolationNumber_PK=16159
       
GO

GRANT EXECUTE ON dbo.USP_HTP_GET_ALR_Check_Request_Letter TO dbr_webuser

GO




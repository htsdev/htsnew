set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go
/*
Altered By:	Sabir Khan 6371 08/13/2009 

Business Logic:	The stored procedure is used to insert mailer id into No Mailing List
				

Parameters: 
	@MailerID : Mailer ID


Output columns: 
	Return:	1 if mailer id is not already exists in no mailing list otherwise it will return 0

*/

---	Usp_hts_Insert_tblbadmailers 37653

ALTER procedure [dbo].[Usp_hts_Insert_tblbadmailers ]


@MailerID int 

AS

IF NOT EXISTS(SELECT * FROM tblBadMailers WHERE MailerID = @MailerID)
BEGIN	
insert into tblbadmailers (MailerID) values(@MailerID)
SELECT 1
END
ELSE
SELECT 0	



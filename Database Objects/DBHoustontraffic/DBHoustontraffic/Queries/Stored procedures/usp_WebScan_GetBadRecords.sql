SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_WebScan_GetBadRecords]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_WebScan_GetBadRecords]
GO


-- usp_WebScan_GetBadRecords 97
CREATE procedure [dbo].[usp_WebScan_GetBadRecords]
        
@BatchID as int        
        
as         
        
        
select       
  p.id as picid,        
  o.causeno,        
  o.status,   
  o.courtno,        
  convert(varchar(12),        
  o.newcourtdate,101) as courtdate,        
  convert(varchar(12),o.todaydate,101) as todaydate,        
  o.location,        
  o.time        
  from tbl_WebScan_Batch b inner join         
  tbl_WebScan_Pic p on         
  b.batchid=p.batchid        
  inner join tbl_WebScan_Log l on         
  p.id=l.picid left outer join tbl_WebScan_OCR o on         
  l.picid=o.picid        
  where        
  b.batchid=@BatchID    
     
  order by o.picid


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


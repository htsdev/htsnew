
/****** 
Created By:		Sabir khan
Altered by:		Abbas Qamar
Task ID: 8186
Business Logic:	This procedure is used by MISC reporting for getting list of violations which have more uploading.
				
List of Parameters:
	
	@CourtLocation	selected Court Location
	@top			List top number of violations

List of Columns:	
	violationDescription	Violation description.
	totalcount		Total number of occurence of violation.
******/

--[USP_MISC_Violation_Count] '01/01/2010','01/01/2012','ALL DCJP',100

Alter PROCEDURE [dbo].[USP_MISC_Violation_Count]
--Abbas Qamar 10118 03/15/2012 Start Date and End Date Added.
@StartDate datetime,
@EndDate datetime,
@CourtLocation VARCHAR(MAX),
@top  INT
AS 

DECLARE @category INT
DECLARE @courtid INT
IF @CourtLocation = 'ALL DCJP'
	BEGIN
		SET @category = 11	
	END
ELSE IF @CourtLocation = 'ALL HMC'
	BEGIN
		SET @category = 1	
	END
ELSE IF @CourtLocation = 'ALL HCJP'
	BEGIN
		SET @category = 2
	END
ELSE
	BEGIN
		SELECT @category = courtcategorynum FROM tblCourts WHERE CourtName LIKE @CourtLocation
		IF @category IS NULL
			BEGIN
				SELECT @category = courtcategorynum FROM DallasTrafficTickets.dbo.tblCourts WHERE CourtName LIKE @CourtLocation
				SELECT @courtid = courtid FROM DallasTrafficTickets.dbo.tblCourts WHERE CourtName LIKE @CourtLocation
			END
		ELSE
			BEGIN
			 SELECT @courtid = courtid FROM tblCourts WHERE CourtName LIKE @CourtLocation
			END
	END
	
	
IF @category IN (6, 11, 16, 25, 27, 28, 29, 30)
BEGIN
    IF @CourtLocation = 'ALL DCJP'
    BEGIN
        SELECT violationDescription,
               COUNT(DISTINCT TicketNumber_PK) AS totalcount
               INTO #tempDCJP
        FROM   DallasTrafficTickets.dbo.tblTicketsViolationsArchive tva
        WHERE  --Abbas Qamar 10118 03/15/2012 Start Date and End Date filter added.
				datediff(day,tva.InsertDate,@StartDate)<=0
				AND DATEDIFF(DAY,tva.InsertDate,@EndDate)>=0
				and tva.CourtLocation 
								 IN 
									(
									 SELECT courtid 
									 FROM DallasTrafficTickets.dbo.tblCourts  c 
									 WHERE c.courtcategorynum = 11 AND c.InActiveFlag = 0
									 )
        GROUP BY
               tva.ViolationDescription
        
        SELECT top(@top) violationDescription AS [Violation Description], totalcount AS [Total Count] FROM #tempDCJP  ORDER BY totalcount DESC
        DROP TABLE #tempDCJP
    END
    ELSE
    BEGIN
        SELECT violationDescription,
               COUNT(DISTINCT TicketNumber_PK) AS totalcount
               INTO #temp
        FROM   DallasTrafficTickets.dbo.tblTicketsViolationsArchive tva
        WHERE --Abbas Qamar 10118 03/15/2012 Start Date and End Date filter added.
        datediff(day,tva.InsertDate,@StartDate)<=0
		AND DATEDIFF(DAY,tva.InsertDate,@EndDate)>=0 
        AND tva.CourtLocation = @courtid
        
        GROUP BY
               tva.ViolationDescription
        
        SELECT top(@top) violationDescription AS [Violation Description], totalcount AS [Total Count]
        FROM   #temp
        ORDER BY
               totalcount DESC
		DROP TABLE #temp
    END
END
ELSE
BEGIN
    IF @CourtLocation = 'ALL HMC'
    BEGIN
        SELECT violationDescription,
               COUNT(DISTINCT TicketNumber_PK) AS totalcount
               INTO #tempHMC
        FROM   tblTicketsViolationsArchive tva
        WHERE --Abbas Qamar 10118 03/15/2012 Start Date and End Date filter added.
        datediff(day,tva.InsertDate,@StartDate)<=0
		AND DATEDIFF(DAY,tva.InsertDate,@EndDate)>=0 
        AND tva.CourtLocation IN (SELECT courtid FROM tblcourts c WHERE c.courtcategorynum = 1 AND c.InActiveFlag = 0)
        GROUP BY
               tva.ViolationDescription
        
        SELECT top(@top) violationDescription AS [Violation Description], totalcount AS [Total Count]
        FROM   #tempHMC
        ORDER BY
               totalcount DESC
        DROP TABLE #tempHMC
    END
    ELSE 
    IF @CourtLocation = 'ALL HCJP'
    BEGIN
        SELECT violationDescription,
               COUNT(DISTINCT TicketNumber_PK) AS totalcount
               INTO #tempHCJP
        FROM   tblTicketsViolationsArchive tva
        WHERE  --Abbas Qamar 10118 03/15/2012 Start Date and End Date filter added.
        datediff(day,tva.InsertDate,@StartDate)<=0
		AND DATEDIFF(DAY,tva.InsertDate,@EndDate)>=0
        AND tva.CourtLocation IN (SELECT courtid FROM tblcourts c WHERE c.courtcategorynum = 2 AND c.InActiveFlag = 0)
        GROUP BY
               tva.ViolationDescription
        
        SELECT top(@top) violationDescription AS [Violation Description], totalcount AS [Total Count]
        FROM   #tempHCJP
        ORDER BY
               totalcount DESC
        DROP TABLE #tempHCJP
    END
    ELSE
    BEGIN
        SELECT violationDescription,
               COUNT(DISTINCT TicketNumber_PK) AS totalcount
               INTO #temp1
        FROM   tblTicketsViolationsArchive tva
        WHERE  --Abbas Qamar 10118 03/15/2012 Start Date and End Date filter added.
        datediff(day,tva.InsertDate,@StartDate)<=0
		AND DATEDIFF(DAY,tva.InsertDate,@EndDate)>=0
        AND tva.CourtLocation = @courtid
        GROUP BY
               tva.ViolationDescription
        
        SELECT top(@top) violationDescription AS [Violation Description], totalcount AS [Total Count]
        FROM   #temp1
        ORDER BY
               totalcount DESC
    END
END


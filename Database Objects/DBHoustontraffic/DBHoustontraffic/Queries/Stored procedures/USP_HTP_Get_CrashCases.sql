USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTP_Get_CrashCases]    Script Date: 08/27/2013 00:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/****** 
Create by		: Farrukh Iftikhar
Task ID			: 10447
Created Date	: 11/07/2012


Business Logic : 
			This procedure retrieve Crash Cases.
			

			
EXEC [USP_HTP_Get_CrashCases] 
******/

-- [dbo].[USP_HTP_Get_CrashCases] 500, 45
ALTER PROCEDURE [dbo].[USP_HTP_Get_CrashCases]
	@TopNumberOfRecords INT,
	@NumberOfDaysInPast INT
AS
BEGIN	
	
    SELECT DISTINCT TOP(@TopNumberOfRecords) TC.Case_ID,
           TC.Crash_ID
    FROM   Crash.dbo.tbl_Person TP WITH (NOLOCK)
           INNER JOIN Crash.dbo.tbl_Crash TC WITH (NOLOCK)
                ON  TC.Crash_ID = TP.Crash_ID
    WHERE  ISNULL(LEN(TC.Case_ID), 0) = 11
           AND ISNUMERIC(SUBSTRING(ISNULL(TC.Case_ID, ''), 0, 10)) = 1
           AND ISNULL(Tc.Case_ID, '') LIKE '%[A-Za-z]'
		   AND isnull(TC.Rpt_City_ID,0) = 208
           AND ISNULL(TC.IsCIDProcessed, 'FALSE') = 'FALSE'		   
           AND DATEDIFF(DAY, TC.InsertDate, GETDATE()) <= @NumberOfDaysInPast
		   
	ORDER BY TC.Crash_ID DESC
END




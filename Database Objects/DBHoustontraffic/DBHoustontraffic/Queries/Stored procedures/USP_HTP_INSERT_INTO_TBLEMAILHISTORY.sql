/*
* Created By :- Noufil 5884 07/14/2009
* Business Logic : This procedure insert records in table tblEmailHistory for Sms History Purpose
* Parameters : 
*				@EmailTo
				@Client
				@Subject
				@SentInfo
				@recdate
				@Sentflag
				@TicketID
				@CourtDate
				@CourtNumber
				@resendby
				@Smstype
				@PhoneNumber
				@SmsCalltype
				@ticketnumber
* 
*/


ALTER PROCEDURE [dbo].[USP_HTP_INSERT_INTO_TBLEMAILHISTORY]
@EmailTo VARCHAR(1000),
@Client VARCHAR(100),
@Subject VARCHAR(200),
@SentInfo VARCHAR(1000), -- Rab Nawaz 10914 Increase the Size of parameter. . . 
@recdate datetime,
@Sentflag smallint,
@TicketID int,
@CourtDate datetime,
@CourtNumber VARCHAR(6),
@resendby VARCHAR(10),
@Smstype bit,
@PhoneNumber VARCHAR(50),
@SmsCalltype VARCHAR(200),
@ticketnumber VARCHAR(100)

AS
 
 
 IF @Sentflag = 3
	SET @Sentflag=NULL
 -- Rab Nawaz 10914 Replacing the new line with break line for HTML page. . . 
 SET @SentInfo = REPLACE(@SentInfo, '\r\n', '<br />')
 SET @SentInfo = REPLACE(@SentInfo, '\n', ' ')
 
 -- Rab Nawaz Khan 10914 06/06/2013 Added General Comments against clients when called from the SMS Service. . . 
 DECLARE @generalComments VARCHAR (1000)
 IF (LTRIM(RTRIM(@SmsCalltype)) = 'Set Call' OR LTRIM(RTRIM(@SmsCalltype)) = 'Reminder Call')
 BEGIN
 	IF (LTRIM(RTRIM(@SmsCalltype)) = 'Set Call')
 	BEGIN
 		SET @generalComments = ' Court Date Reset Text sent to client via SMS on ' + @PhoneNumber
 	END
 	IF (LTRIM(RTRIM(@SmsCalltype)) = 'Reminder Call')
 	BEGIN
 		SET @generalComments = ' Court Date Reminder Text sent to client via SMS on ' + @PhoneNumber
 	END
 	EXECUTE [TrafficTickets].[dbo].[USP_HTS_UpdateGeneralComments] @TicketID, 3992, 0, @generalComments
 END
 
 -- Rab Nawaz Khan 10914 06/28/2013 Below Code Executes when called from the HTP--> Text Message --> Resend SMS. . . 
 IF (LTRIM(RTRIM(@SmsCalltype)) = 'Invalid Response Text' OR LTRIM(RTRIM(@SmsCalltype)) = 'Reminder Text' OR LTRIM(RTRIM(@SmsCalltype)) = 'Set Text'  OR LTRIM(RTRIM(@SmsCalltype)) = 'Manual' OR LTRIM(RTRIM(@SmsCalltype)) = 'Manual Send')
 BEGIN
 	IF (LTRIM(RTRIM(@SmsCalltype)) = 'Set Text')
 	BEGIN
 		SET @generalComments = ' Court Date Reset Text resent to client via SMS on ' + @PhoneNumber
 		SET @SmsCalltype = 'Set Call'
 	END
 	IF (LTRIM(RTRIM(@SmsCalltype)) = 'Reminder Text')
 	BEGIN
 		SET @generalComments = ' Court Date Reminder Text resent to client via SMS on ' + @PhoneNumber
 		SET @SmsCalltype = 'Reminder Call'
 	END
 	
 	IF (LTRIM(RTRIM(@SmsCalltype)) = 'Invalid Response Text')
 	BEGIN
 		SET @generalComments = ' Invalid Response Text resent to client via SMS on ' + @PhoneNumber
 	END
 	
 	IF (LTRIM(RTRIM(@SmsCalltype)) = 'Manual')
 	BEGIN
 		SET @generalComments = ' Court setting information resent to client via SMS on ' + @PhoneNumber
 	END
 	IF (LTRIM(RTRIM(@SmsCalltype)) = 'Manual Send')
 	BEGIN
 		SET @generalComments = ' Court setting information sent to client via SMS on ' + @PhoneNumber
 	END
 	EXECUTE [TrafficTickets].[dbo].[USP_HTS_UpdateGeneralComments] @TicketID, @resendby, 0, @generalComments
 	
 	IF (LTRIM(RTRIM(@SmsCalltype)) = 'Manual Send')
 	BEGIN
 		SET @resendby = ''
 		SET @SmsCalltype = LTRIM(RTRIM(REPLACE(@SmsCalltype, 'Send', '')))
 	END
 END
 
 INSERT INTO tblEmailHistory
 (
 	EmailTo,
 	Client,
 	[Subject],
 	SentInfo, 	
 	recdate,
 	Sentflag,
 	TicketID,
 	CourtDate,
 	CourtNumber,
 	resendby,
 	Smstype,
 	PhoneNumber,
 	SmsCallType,
 	TicketNumber
 )
 VALUES
 (
 	@EmailTo,
	@Client,
	@Subject,
	@SentInfo,
	@recdate,
	@Sentflag,
	@TicketID,
	@CourtDate,
	@CourtNumber,
	@resendby,
	@Smstype,
	@PhoneNumber,
	@SmsCalltype,
	@ticketnumber
 )
 
 GO
 GRANT EXECUTE ON [dbo].[USP_HTP_INSERT_INTO_TBLEMAILHISTORY] TO dbr_webuser
 
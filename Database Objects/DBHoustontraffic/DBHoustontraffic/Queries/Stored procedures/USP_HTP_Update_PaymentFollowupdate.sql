﻿/**
* Noufil 5618 04/02/2009 
* Business Logic : THis method Update Client's Payment followupdate
* Return : Number of Rows Effected.
**/

CREATE PROCEDURE dbo.[USP_HTP_Update_PaymentFollowupdate]
@ticketid_pk INT,
@paymentfollowupdate DATETIME,
@EmpID INT
AS

DECLARE @oldfollowupdate DATETIME
SET @oldfollowupdate = (SELECT ISNULL(tt.PaymentDueFollowUpDate,'01/01/1900')
                          FROM tblTickets tt WHERE tt.TicketID_PK=@ticketid_pk )
                          
DECLARE @oldfollowupdatevarchar VARCHAR(20)
SET @oldfollowupdatevarchar = CASE @oldfollowupdate WHEN '01/01/1900' THEN 'N/A' ELSE CONVERT(VARCHAR(20),@oldfollowupdate,101) END
                          
IF (@oldfollowupdate <> @paymentfollowupdate)
BEGIN
	UPDATE tblTickets
	SET	
		PaymentDueFollowUpDate = @paymentfollowupdate
	WHERE TicketID_PK=@ticketid_pk
	--RETURN @@ROWCOUNT
	
	INSERT INTO tblTicketsNotes
	(		
		TicketID,
		[Subject],
		Notes,
		Recdate,
		EmployeeID,
		EventID,
		NoteType
	)
	VALUES
	(		
		@ticketid_pk,
		'Payment Follow-Up Date has been changed from '+ @oldfollowupdatevarchar + ' to ' + CONVERT(VARCHAR(20),@paymentfollowupdate,101) ,
		'Payment Follow-Up Date has been changed from '+ @oldfollowupdatevarchar + ' to ' + CONVERT(VARCHAR(20),@paymentfollowupdate,101) ,
		GETDATE(),
		@EmpID,
		NULL,
		NULL
	)
	RETURN 1
END
ELSE
	BEGIN
		RETURN 0
	END


GO
GRANT EXECUTE ON dbo.[USP_HTP_Update_PaymentFollowupdate] TO dbr_webuser

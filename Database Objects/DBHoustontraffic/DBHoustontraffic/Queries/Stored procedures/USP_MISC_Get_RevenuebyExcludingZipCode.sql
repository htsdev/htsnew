USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_MISC_Get_RevenuebyExcludingZipCode]    Script Date: 09/01/2010 21:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--- USP_MISC_Get_RevenuebyExcludingZipCode '1/1/2010','08/27/2010',29,2,'75,76'
ALTER PROCEDURE [dbo].[USP_MISC_Get_RevenuebyExcludingZipCode]

@StartMonth DATETIME,
@EndMonth DATETIME,
@LetterType INT,
@ZipcodeLenght INT,
@Zipcode VARCHAR(MAX) = ''
AS
--
select	noteid,Left(n.ZipCode,@ZipcodeLenght) AS zipcode, convert(datetime, convert(varchar,month(n.recordloaddate)) + '/01/'+ convert(varchar,year(recordloaddate))  ) as mdate, 
		n.lettertype, c.courtcategoryname + ' - ' + l.lettername as MailerName, n.pcost as expense
into #mailers
from tblletternotes n inner join  tblletter l  on n.lettertype = l.letterid_pk
inner join tblcourtcategories c on c.courtcategorynum = l.courtcategory
where datediff(MONTH, recordloaddate, @StartMonth)<= 0
and datediff(MONTH, recordloaddate, @EndMonth)>= 0
and datediff(YEAR , recordloaddate, @StartMonth)<= 0
and datediff(YEAR , recordloaddate, @EndMonth)>= 0
--AND LEFT(n.ZipCode,@ZipcodeLenght
and SUBSTRING(n.ZipCode, 1, @ZipcodeLenght) NOT IN (SELECT sValue FROM SplitString(@Zipcode,','))
and l.letterid_pk = @LetterType
and l.isactive = 1

alter table #mailers add ticketid int, isclient tinyint, isquote tinyint, revenue money, isnonclient tinyint, profit money

update #mailers set ticketid= 0, isclient = 0, isquote= 0, revenue = 0, isnonclient=0, profit = 0

Declare @courtCategory int
select @courtCategory = c.CourtCategorynum from tblcourtcategories c inner join tblletter l on l.courtcategory = c.CourtCategorynum
where l.LetterID_PK = @LetterType
if @courtCategory in (6,16,11,27,28,30,25,29)
BEGIN

update m
set m.ticketid = t.ticketid_pk,
	isclient = case t.activeflag when 1 then 1 else 0 end ,
	isquote = case t.activeflag when 0 then 1 else 0 end
from #mailers m inner join dallastraffictickets.dbo.tbltickets t on t.mailerid = m.noteid

update #mailers set isnonclient = 1 where ticketid = 0

select ticketid, isnull(sum(isnull(chargeamount,0)),0) fee
into #paymentsDallas
from DallasTrafficTickets.dbo.tblticketspayment 
where paymentvoid =0
group by ticketid
having isnull(sum(isnull(chargeamount,0)),0) > 0
order by ticketid

update m
set m.revenue =p.fee
from #mailers m inner join #paymentsDallas p on p.ticketid = m.ticketid

update #mailers set profit = isnull(revenue,0)  - isnull( expense,0)
Drop table #paymentsDallas
END
ELSE
BEGIN

update m
set m.ticketid = t.ticketid_pk,
	isclient = case t.activeflag when 1 then 1 else 0 end ,
	isquote = case t.activeflag when 0 then 1 else 0 end
from #mailers m inner join tbltickets t on t.mailerid = m.noteid

update #mailers set isnonclient = 1 where ticketid = 0

select ticketid, isnull(sum(isnull(chargeamount,0)),0) fee
into #payments
from tblticketspayment 
where paymentvoid =0
group by ticketid
having isnull(sum(isnull(chargeamount,0)),0) > 0
order by ticketid

update m
set m.revenue =p.fee
from #mailers m inner join #payments p on p.ticketid = m.ticketid

update #mailers set profit = isnull(revenue,0)  - isnull( expense,0)

drop table #payments
END

--select * from #mailers order by mdate, mailername
select mdate, ZipCode ,count(m.noteid) mailer, sum(expense) expense, sum(isclient) client, sum(isquote) quote, sum(revenue) revenu, sum(isnonclient) nonclient, sum(profit) profet
into #finalTemp from #mailers m 
group by mdate,ZipCode
order by mdate,ZipCode

select  DATENAME(month, mdate) + '-' + DATENAME(year, mdate) as [Month] ,ZipCode, mailer [Mailer Count],client Client,quote [Call],nonclient [Non-Client],cast([expense] as decimal(10,2)) [Expense($)],cast([revenu] as decimal(10,2)) [Revenue($)],cast([profet] as decimal(10,2)) [Profit($)]  from #finalTemp


--/*
drop  table #mailers

Drop table #finalTemp






--*/
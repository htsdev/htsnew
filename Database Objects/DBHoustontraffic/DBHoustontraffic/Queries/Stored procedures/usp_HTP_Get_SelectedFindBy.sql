USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[usp_HTP_Get_SelectedFindBy]    Script Date: 01/02/2014 02:21:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** 
Create by:  Sabir Khan
Task ID: 11509
Business Logic : This procedure is used to get find by description

******/
ALTER Procedure [dbo].[usp_HTP_Get_SelectedFindBy]
@TicketID INT
as

SELECT fd.FindByID,fb.FindBy FROM FindByClientDetail fd 
INNER JOIN FindBy fb ON fb.ID = fd.FindByID 
WHERE fd.TicketID = @TicketID


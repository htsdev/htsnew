SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_RPT_ClientsInPendingStatus]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_RPT_ClientsInPendingStatus]
GO

create procedure dbo.USP_RPT_ClientsInPendingStatus

as


select	--distinct
		case when len(isnull(v.casenumassignedbycourt,'')) =0  then 'N/A'  else v.casenumassignedbycourt end as [Cause Number],
		v.refcasenumber as [Ticket Number],
		b.description as [Violation Description],
		t.lastname as [Last Name],
		t.firstname as [First Name],
		case when len(isnull(t.dlnumber,'')) = 0  then 'N/A' else t.dlnumber end as [DL Number],
		s.state as [DL State],
		t.dob as [Date Of Birth],
		d.description as [VStatus],
		v.courtdatemain as [VDate],
		dbo.fn_formatetime (v.courtdatemain) as [VTime],
		case when len(isnull(v.courtnumbermain,''))=0 then 'N/A' else v.courtnumbermain end as [VRoom],
		c.shortname as [Location]
from	tbltickets t, 
		tblticketsviolations v,
		tblviolations b,
		tblstate s,
		tblcourts c,
		tblcourtviolationstatus a,
		tbldatetype d
where	t.ticketid_pk = v.ticketid_pk
and		t.dlstate = s.stateid
and		c.courtid = v.courtid
and		v.violationnumber_pk = b.violationnumber_pk
and		v.courtviolationstatusidmain = a.courtviolationstatusid
and		d.typeid = a.categoryid

and		d.typeid = 1
and		t.activeflag = 1
order by [location],[vdate] desc, [last name], [first name]
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


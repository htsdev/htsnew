--usp_helptext   USP_PCS_GET_VIOLATION_PRICES  
--Select * from tblviolations where violationtype = 10        
--Select top 1  *  from tblticketsviolationsarchive where recordid != 0         
--USP_PS_GET_VIOLATION_PRICES  746,'11017,11018,' ,3001 ,1        
            
    
ALTER procedure [dbo].[USP_PS_GET_VIOLATION_PRICES] --0,'11017,11018,' ,3001 ,1                 
                          
(                          
 @RecordID int,          
 @ViolationIDs varchar(2000),          
 @CourtId int,          
 @bondflag bit          
)                          
                          
as                          
          
SET NOCOUNT ON;          
          
          
--Calculate Price For Non Client Cases          
          
If @RecordID <> 0           
 Begin          
          
 declare @temp table (          
          
 recordid [numeric](18, 4),          
 violationnumber_pk [numeric](18, 4),          
 Planid int,          
 BondAmount money ,          
 Bondflag int ,          
 [FineAmount] [numeric](18,4)          
          
 )          
          
    Insert Into @temp           
 Select           
 tv.recordid ,          
 t.violationnumber_pk,          
 isnull(p.planid,45),          
 isnull(t.BondAmount,0),          
 isnull(tv.Bondflag,0) ,          
 isnull(t.FineAmount,0)          
 FROM tblticketsarchive tv WITH(NOLOCK)           
 join dbo.tblTicketsViolationsArchive AS T WITH(NOLOCK)          
 ON TV.RecordID = T.RecordID           
    
 left outer join  tblpriceplans p WITH(NOLOCK)        
 ON p.courtid =   t.courtlocation       
 and p.isactiveplan = 1                                         
    
 where TV.RecordID = @RecordID          
-- and tv.listdate between p.effectivedate and p.enddate                                        
and Datediff(day ,tv.listdate , p.effectivedate) <= 0 and Datediff(day ,tv.listdate , p.enddate ) >= 0  
  
  
      
    --and p.courtid = tv.courtid      
          
          
-- fOR fta TICKET    
    
------------------------------------------------                                  
-- Finding Other FTA Cases Associated with the current case --        
    
        
Declare @FTALinkID as int    
Declare @Recordidtemp as int         
Set @FTALinkID=0      
Set @Recordidtemp = 0     
        
Select Top 1 @FTALinkID = fTALinkID      
FROM tblticketsarchive tv WITH(NOLOCK)           
join dbo.tblTicketsViolationsArchive AS T WITH(NOLOCK)          
ON TV.RecordID = T.RecordID           
where T.RECORDID = @RecordID AND  ftalinkid > 0 and TV.ClientFlag =0  and FTAlinkid is not null     
        
        
if @FTALinkID > 0         
Begin           
       
   Select @Recordidtemp = recordid from tblTicketsViolationsArchive WITH(NOLOCK) where RECORDID <>  @Recordid AND  ftalinkid = @FTALinkID    
    
    Insert Into @temp           
 Select           
 tv.recordid ,          
 t.violationnumber_pk,          
 isnull(p.planid,45),          
 isnull(t.BondAmount,0),          
 isnull(tv.Bondflag,0) ,          
 isnull(t.FineAmount,0)          
 FROM tblticketsarchive tv WITH(NOLOCK)           
 join dbo.tblTicketsViolationsArchive AS T WITH(NOLOCK)          
 ON TV.RecordID = T.RecordID           
    
 left outer join  tblpriceplans p WITH(NOLOCK)        
 ON p.courtid =   tv.courtid       
 and p.isactiveplan = 1     
 where TV.RecordID = @Recordidtemp          
-- and tv.listdate between p.effectivedate and p.enddate                                        
and Datediff(day ,tv.listdate , p.effectivedate) <= 0 and Datediff(day ,tv.listdate , p.enddate ) >= 0  
  
 --and p.isactiveplan = 1                                         
 --and p.courtid = tv.courtid      
        
eND          
          
          
declare @temp2 table (              
 rowid int identity(0,1),              
 [planid] [int] NOT NULL ,              
 [FineAmount] [numeric](18, 0) NOT NULL ,              
 [BasePrice] [money] NOT NULL ,              
 [SecondaryPrice] [money] NOT NULL ,              
 [BasePercentage] [numeric](18, 4) NOT NULL ,              
 [SecondaryPercentage] [numeric](18, 4) NOT NULL ,              
 [BondBase] [money] NOT NULL ,              
 [BondSecondary] [money] NOT NULL ,              
 [BondBasePercentage] [numeric](18, 4) NOT NULL ,              
 [BondSecondaryPercentage] [numeric](18, 4) NOT NULL ,              
 [BondAssumption] [money] NOT NULL ,              
 [FineAssumption] [money] NOT NULL ,              
 [underlyingbondflag] [int] NOT NULL ,              
 [BondAmount] [money] NOT NULL               
 )                
          
          
insert into @temp2          
          
SELECT                    
  tv.planid  ,                             
  isnull(tv.FineAmount,0) as FineAmount,                                      
  isnull(pd.BasePrice,0) as BasePrice,                                     
  isnull(pd.SecondaryPrice,0) as SecondaryPrice,                                     
  isnull(pd.BasePercentage,0) as BasePercentage,         
  isnull(pd.SecondaryPercentage,0)as SecondaryPercentage,                                     
  isnull(pd.BondBase,0) as BondBase,                                     
  isnull(pd.BondSecondary,0) as BondSecondary ,                                     
  isnull(pd.BondBasePercentage,0) as BondBasePercentage,                                     
  isnull(pd.BondSecondaryPercentage,0) as BondSecondaryPercentage,                                     
  isnull(pd.BondAssumption,0) as BondAssumption,                                     
  isnull(pd.FineAssumption, 0) as FineAssumption ,                                  
  isnull(@bondflag,0) as underlyingbondflag ,                                   
  isnull(tv.BondAmount,0) as BondAmount                                   
                
 FROM @temp tv                                 
 INNER JOIN                                
 tblViolations v WITH(NOLOCK)                                 
 ON  tv.violationnumber_pk = v.violationnumber_pk                              
 INNER JOIN                                
 tblPricePlans p WITH(NOLOCK)                                 
 ON   tv.PlanID = p.PlanId                                 
 LEFT OUTER JOIN                                
 tblPricePlanDetail pd WITH(NOLOCK)                                 
 ON  p.PlanId = pd.PlanId                                 
 AND  v.CategoryID = pd.CategoryId                                
 --where  tv.recordid  = @RecordID          
             
          
select * from @temp2          
          
End          
          
Else           
Begin          
          
          
declare @violations table (  rowid int identity(0,1),   violationnumber_pk numeric ( 18,0) , courtid int , planid  int , bondflag bit )          
          
insert into @violations select * , @courtid ,          
 (select  top 1 planid             
  from  tblpriceplans WITH(NOLOCK)             
  where  courtid =@CourtID            
  and  effectivedate <= getdate()            
  and enddate > = getdate()            
  and isActivePlan = 1            
  order by planid  )          
 ,@bondflag          
from  dbo.Sap_String_Param(@ViolationIDs)          
          
declare @temp3 table (              
 rowid int identity(0,1),              
 [planid] [int] NOT NULL ,              
 [FineAmount] [numeric](18, 0) NOT NULL ,              
 [BasePrice] [money] NOT NULL ,              
 [SecondaryPrice] [money] NOT NULL ,              
 [BasePercentage] [numeric](18, 4) NOT NULL ,              
 [SecondaryPercentage] [numeric](18, 4) NOT NULL ,              
 [BondBase] [money] NOT NULL ,              
 [BondSecondary] [money] NOT NULL ,              
 [BondBasePercentage] [numeric](18, 4) NOT NULL ,              
 [BondSecondaryPercentage] [numeric](18, 4) NOT NULL ,              
 [BondAssumption] [money] NOT NULL ,              
 [FineAssumption] [money] NOT NULL ,              
 [underlyingbondflag] [int] NOT NULL ,              
 [BondAmount] [money] NOT NULL               
 )                
          
          
insert into @temp3          
          
SELECT           
 isnull( p.planid ,45),                        
 75,          
 isnull(pd.BasePrice,0) as BasePrice,                         
 isnull(pd.SecondaryPrice,0) as SecondaryPrice,                         
 isnull(pd.BasePercentage,0) as BasePercentage,                         
 isnull(pd.SecondaryPercentage,0)as SecondaryPercentage,                         
 isnull(pd.BondBase,0) as BondBase,                         
 isnull(pd.BondSecondary,0) as BondSecondary ,                         
 isnull(pd.BondBasePercentage,0) as BondBasePercentage,                         
 isnull(pd.BondSecondaryPercentage,0) as BondSecondaryPercentage,                         
 isnull(pd.BondAssumption,0) as BondAssumption,                         
 isnull(pd.FineAssumption, 0) as FineAssumption,                       
 isnull(@bondflag,0) as underlyingbondflag    ,                    
 0                   
FROM      dbo.tblviolations AS v WITH(NOLOCK) INNER JOIN              
          tblPricePlanDetail AS pd WITH(NOLOCK) ON v.CategoryID = pd.CategoryId           
       RIGHT OUTER JOIN @violations AS tv LEFT OUTER JOIN              
          tblPricePlans AS p WITH(NOLOCK) ON tv.PlanId = p.PlanId           
    ON v.violationnumber_pk  = tv.violationnumber_pk           
    AND pd.PlanId = p.PlanId              
                 
where  tv.rowid in ( Select rowid from @violations )           
          
select * from @temp3          
          
End          
          
              
    
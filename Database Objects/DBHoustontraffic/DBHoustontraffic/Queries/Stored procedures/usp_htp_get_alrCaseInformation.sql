/*        
TasK		   : 9949        
Business Logic : This procedure retrived all the information about the Alr Case Information
           
Parameter:       
	@ticketID   : identifiable key  TicketId      
	@employeeid		: Employee which have Right to access the system
*/ 

CREATE PROC [dbo].[usp_htp_get_alrCaseInformation]
(
  @ticketID int   ,                                       
  @employeeid int = 3992
)
as
SELECT    ISNULL(v.RefCaseNumber,v.casenumassignedbycourt) AS CaseNumber,vd.[Description] AS ViolDec,o.ShortDescription AS Status,V.CourtDate,v.CourtDateMain AS CourtTime,c.ShortName +'  '+v.CourtNumbermain AS [CourtLoc and Number],t.LanguageSpeak ,t.IsALRHearingRequired 
from tbltickets T,  tblcourts C, tbldatetype D ,tblstate S , tblticketsviolations V , tblcourtviolationstatus O, tblviolations vd ,tbl_Mailer_County tmc      
where      
T.stateid_fk=S.stateid                                    
and T.ticketid_pk = V.ticketid_pk                              
and V.courtviolationstatusidmain = O.courtviolationstatusid                              
and O.categoryid = D.typeid                                      
and T.ticketid_pk in (@ticketID)                                          
and activeflag = 1                                     
and c.courtid = v.courtid      
and v.ViolationNumber_PK=vd.ViolationNumber_PK
AND tmc.CountyID=c.CountyId


GO
GRANT EXEC ON [dbo].[usp_htp_get_alrCaseInformation] TO dbr_webuser
GO

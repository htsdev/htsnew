USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[Sp_get_Idtickets]    Script Date: 10/23/2008 02:04:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- Sp_get_Idtickets '', '', '',''   
-- Noufil 4432 08/05/2008 html serial number added added for validation report  
  
--Sp_get_Idtickets '','','',''
ALTER PROCEDURE [dbo].[Sp_get_Idtickets]    
  
@TicketNo varchar(20),    
@Causeno varchar(30),  
@firstname varchar(25),    
@lastname varchar(25)  
AS  
  
declare @query varchar(2000)

select distinct  
			a.Lastname + ', ' + a.Firstname AS Fullname,   
			dbo.tblTicketsViolations.RefCaseNumber,  
			dbo.tblTicketsViolations.casenumassignedbycourt,   
			a.ViolationDate,
			dbo.tblTicketsViolations.CourtNumber,   
			dbo.tblCourts.ShortName AS CourtLoc,   
			a.TicketID_PK,   
			(case   
				when dbo.tblViolations.Description = '------Choose---------' then 'N/A'  
						else dbo.tblViolations.Description   
				end) as [Description] ,   
			dbo.tblTicketsViolations.ViolationNumber_PK,   
			dbo.tblTicketsViolations.TicketsViolationID,   
			CONVERT(varchar(12), a.DOB, 101) AS DOB,   
			a.Midnum, 
			dbo.FxJoinCaseNo(a.TicketID_PK) AS oldno,  
			dbo.tblCourtViolationStatus.CategoryID,   
            a.Activeflag,
			dbo.tblCourts.CourtName,  
			dbo.tblCourts.Phone,
			'<a href="http://ln.legalhouston.com/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber='+convert(varchar(30), a.TicketID_PK )+'" >'+convert(varchar(30), ROW_NUMBER() OVER (order by a.Lastname , a.Firstname ))+'</a>' as link  


FROM         dbo.tblTickets a 
			INNER JOIN dbo.tblTicketsViolations ON a.TicketID_PK = dbo.tblTicketsViolations.TicketID_PK 
			INNER JOIN dbo.tblCourts ON dbo.tblTicketsViolations.CourtID = dbo.tblCourts.Courtid 
			INNER JOIN dbo.tblViolations ON dbo.tblTicketsViolations.ViolationNumber_PK = dbo.tblViolations.ViolationNumber_PK 
			INNER JOIN dbo.tblCourtViolationStatus ON dbo.tblTicketsViolations.CourtViolationStatusIDmain = dbo.tblCourtViolationStatus.CourtViolationStatusID  

WHERE     (dbo.tblCourtViolationStatus.CategoryID not in (7,50)  )     
			AND (a.Activeflag = 1)  
			and dbo.tblViolations.ViolationNumber_PK <> 13
			--Sabir Khan 4966 10/22/2008 to get all matching records...	
			and (dbo.tblTicketsViolations.RefCaseNumber LIKE 'ID%' or dbo.tblTicketsViolations.casenumassignedbycourt LIKE 'ID%')
			and (a.Firstname LIKE @firstname+'%') 
			and (a.Lastname LIKE @lastname +'%')
			and (dbo.tblTicketsViolations.casenumassignedbycourt LIKE @Causeno +'%')
			and (dbo.tblTicketsViolations.RefCaseNumber LIKE +@TicketNo+'%')
			--Sabir Khan 4966 10/15/2008 To get only Traffic case types ...  
			and (a.CaseTypeID=1)
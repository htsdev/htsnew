SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_GetTicketsCountforHMC]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_GetTicketsCountforHMC]
GO

create procedure usp_GetTicketsCountforHMC
as
select dateadd(month,-datediff(month,recloaddate,getdate()),getdate()),count(distinct recordid) 
from tblticketsarchive 
where datediff(month,recloaddate,getdate()) <=6
and courtid in (3001,3002,3003)
group by datediff(month,recloaddate,getdate())
order by datediff(month,recloaddate,getdate())
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


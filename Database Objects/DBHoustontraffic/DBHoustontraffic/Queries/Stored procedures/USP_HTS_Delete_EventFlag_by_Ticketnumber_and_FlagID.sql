/******   
  
Alter by:  Tahir Ahmed
Business Logic : 
	The stored procedure is used by Houston Traffic Program 
	to remove a flag for the specified ticket(s).
  
List of Parameters:   
 @TicketID                                                    
 @FlagID  
 @EmpID  
 @Fid
  
******/    

 alter procedure [dbo].[USP_HTS_Delete_EventFlag_by_Ticketnumber_and_FlagID]                                                       
 @ticketid int,                                                      
 @flagid int,                                                    
 @empid int,                                                
 @fid int                                                       
 as                                       
                                      
                                      
----------------------------------Added by M.Azwar Alam----------------------------------------------------------------------                                      
----------------------------------on 29 August 2007--------------------------------------------------------------------------                                      
----------------------------------If No Advertisement Flag removed then update tblticketsarchive and set donotmailflag to 0--                                      
                                      
if @flagid=24                                     
Begin                                 
                            
declare @courtcnt int                            
                            
select   @courtcnt=count(courtid) from tblticketsviolations where ticketid_pk=  @ticketid and courtid=3037--for Jims                            
                            
if    @courtcnt =0                               
      begin                                  
                                 
                                      
 declare @FirstName varchar(20)                                      
 declare @LastName varchar(20)                                      
 declare @Address varchar(50)                                      
 declare @Zip varchar(12)                                      
 declare @City varchar(50)                                      
 declare @StateID_fk int                                      
                                       
                                      
select                                       
 @FirstName = t.FirstName,                                      
 @LastName = t.LastName,                                      
 @Address = isnull(t.Address1,'')+' '+isnull(t.address2,''),                                      
 @Zip  = t.Zip,                                      
 @City  = t.City,                                      
 @StateID_fk = t.StateID_fk                                      
                                        
                                      
from                     
  tbltickets t  inner join                                       
  tblticketsflag f on t.ticketid_pk=f.ticketid_pk                                      
  where f.flagid=@flagid  and t.ticketid_pk=@ticketid                                    
                                      
                                      
delete from tbl_hts_DoNotMail_Records where                                      
                                        
   FirstName = @FirstName and                                      
   LastName = @LastName and                                      
   Address  = @Address and                                      
   Zip   = @Zip and                                      
   City  = @City and                                      
   StateID_fk = @StateID_fk                                      
                                      
                                      
                                      
                                      
update ta set                                       
                                      
 donotmailflag=0                                      
                                      
from tblticketsarchive ta inner join                                      
  tbltickets t on ta.recordid=t.recordid inner join                                       
  tblticketsflag f on t.ticketid_pk=f.ticketid_pk         
  where f.flagid=@flagid  and t.ticketid_pk=@ticketid                                        
          
          
---------------------------Added by M.Azwar Alam on 27 Sep 2007---------------          
---------------------------If No Advertisement Flag remove then it will show on Quote call back and traffic alerts---          
          
declare @contact1 varchar(50)          
declare @contact2 varchar(50)          
declare @contact3 varchar(50)          
  begin          
      select @contact1=contact1 from tbltickets where ticketid_pk=@ticketid          
      select @contact2=contact2 from tbltickets where ticketid_pk=@ticketid          
      select @contact3=contact3 from tbltickets where ticketid_pk=@ticketid          
          
      delete from tblRestrictedPhoneNumbers where phonenumber in (@contact1,@contact2,@contact3)          
          
     end          
          
          
Update tblviolationquote set followupyn=1 where ticketid_fk=@ticketid          
          
          
----------------------------------------------------------------------------------------          
          
          
          
          
  end          
                            
                            
else                            
begin                              
                                
                                
                                
--Added By M.Azwar alam on 7 September 2007---                                
---update dontmailflag for Jims Cases---------                                
                                
                                
 declare @temptable table                            
(                             
 FirstName varchar(20)        ,                              
 LastName varchar(20)          ,                            
 Address varchar(50)         ,                             
 Zip varchar(12)   ,                            
 City varchar(50)          ,                            
 StateID_fk int                                      
)                            
                                       
                    insert into @temptable                            
                             
select                             
                            
 t.FirstName,                                      
 t.LastName,                                      
 isnull(t.Address1,'')+' '+isnull(t.address2,''),                                
 t.Zip ,                                      
 t.City,                                      
 t.StateID_fk                                      
                                        
                                      
                                
                                
FROM         tbltickets t   inner join tblticketsviolations tv on                                 
                    
 t.ticketid_pk=tv.ticketid_pk inner join  tblticketsflag f                                 
 on t.ticketid_pk=f.ticketid_pk                                        
 where f.flagid=@flagid and tv.courtid=3037 and t.ticketid_pk=@ticketid                                       
                            
delete tbl_hts_DoNotMail_Records from tbl_hts_DoNotMail_Records dm inner join    @temptable tt on                             
                                        
  dm.FirstName = tt.FirstName and                                      
   dm.LastName = tt.LastName and                                      
   dm.Address  = tt.Address and                                      
   dm.Zip   = tt.Zip and                                      
   dm.City  = tt.City and                                      
   dm.StateID_fk = tt.StateID_fk                             
                                      
                                      
                                      
                                      
update cc set                                       
                                      
 donotmailflag=0                                      
                     
from jims.dbo.criminalcases cc inner join                                      
 tblticketsviolations tv on cast(cc.bookingnumber as bigint)=tv.recordid inner join tbltickets t on tv.ticketid_pk=t.ticketid_pk                                
  inner join                                 
  tblticketsflag f on t.ticketid_pk=f.ticketid_pk                                      
  where f.flagid=@flagid   and tv.courtid=3037 and t.ticketid_pk=@ticketid                                            
                                
    end                            
                                
                                
                         
                                       
                                      
End                                      
                                      
                                    
                                      
-----------------------------------------------------------------------------------------------------------------------------                                      
                                                     
  delete from tblticketsflag                                                     
   where ticketid_pk = @ticketid and                                                     
         flagid = @flagid and fid = @fid                                                     
                                           
  declare @desc varchar(100)                                                   
                                                      
  select @desc=description from tbleventflags where flagid_pk=@flagid                                                    
                                            
if @flagid <>   8                                            
insert into tblticketsnotes(ticketid,subject,employeeid,Notes) values(@ticketid,'Flag -'+ @desc + ' - Removed',@empid,null)                                                      
                                                      
if @flagid = 8                                             
Begin                                            
declare @firmName varchar(50)                                            
select @firmName=tf.FirmName  from tbltickets T join tblfirm tf                                             
on T.Firmid = tf.FirmID                                            
where ticketid_pk = @ticketid                                              
                                            
insert into tblticketsnotes(ticketid,subject,employeeid,Notes) values(@ticketid,'Flag - '+  @firmName + ' - Removed',@empid,null)                     
end                                            
          
          
-------------------------------------------------Added by Tahir Ahmed -------------------------------------------------------------------------                                              
                                              
            --On Nov 6, 2007          
            --If "No Plea Out" flag is set, put trial notes for the case....          
------------------------------------------------------------------------------------------------------------------------------------------------                                              
if @flagid = 26          
 begin          
  declare @TrialComments varchar(2000)          
  select @TrialComments = isnull(TrialComments,'') from tbltickets where ticketid_pk = @TicketID          
          
  declare @empnm3 as varchar(12)                                                    
                                                
  select @empnm3 = abbreviation from tblusers where employeeid = @empid                                                    
                                           
  select @TrialComments =  @TrialComments + ' NO PLEA OUT Flag removed ' + '(' +dbo.formatdateandtimeintoshortdateandtime(getdate()) + ' - ' +  @empnm3 + ')' + ' '                                                    
                             
  update tbltickets set  TrialComments = @TrialComments where ticketid_pk = @TicketID                                                      
 end          
------------------------------------------------------------------------------------------------------------------------------------------------                                              
                                                    
           
--Added By Zeeshan Ahmed                                                  
--Used For Continuance Flag                                                  
if @flagid = 9                                                  
Begin                                  
                                                
 if (Select Count(*) as ContinuanceCount   from tblticketsflag                                                 
  where ticketid_pk = @ticketid                                                
  and flagid = 9 ) = 0                                                 
 Begin                                                
 Update tbltickets set ContinuanceFlag = 0 , continuanceamount = 0,                                          
 --added by Ozair                                        
 continuancestatus=0,                                          
 continuancedate='1/1/1900',            
 continuancecomments=''                                          
 -- End                                          
 where ticketid_pk =@ticketid                                                   
 end                                                
                                                
End                                                  
                                                  
-- Added By Zeeshan Ahmed                                                  
-- User For Default Court                                          
if @flagid = 8                                                  
Begin                                                  
Update tbltickets set FirmID = 3000 where ticketid_pk =@ticketid                                                   
End                                                  
                                                  
                                            
----------------------------------------------------------------------------------------                                            
----------------------------------------------------------------------------------------          
        
if @flagid = 31       
begin        
update tbltickets set readcomments = null where ticketid_pk = @ticketid         
END     
    
---- Added By Fahad   
---- this code will update delete NOS followupdate from tbltickets extensions against given ticketid    
if @flagid = 15    
begin     
--delete from tblTicketsExtensions  where ticketid_pk = @ticketid 

-- sameeullah daris 8635 01/31/2011 only set nosfollowup date instead of removing whole row with other Dates 
UPDATE tblticketsextensions
SET
	
	NOSFollowUpDate = NULL
WHERE Ticketid_pk = @ticketid


    
END         

-- Agha Usman 4347 07/22/2008 - Add logic for Problem Client
-- tahir 4777 09/10/2008 
-- added same functionality for new flag 'Problem Client (No Hire)'
--if @flagid = 36
if @flagid in (13,36)
begin 

	Declare @FName varchar(20),
	@MName varchar(6),
	@LName varchar(20),
	@refcasenumber varchar(20),
	@DOB datetime 

	select @FName = FirstName,@MName = MiddleName,@LName=LastName,@DOB=dob,@refcasenumber =
	 (select top 1 refcasenumber from tblticketsviolations where ticketId_pk = t.ticketId_pk)  from tbltickets  t where ticketId_Pk = @ticketid

-- Agha Usman 4497  07/31/2008 - Remove Middle Name Check
select distinct t.ticketId_pk into #temp1 from tbltickets t
	where FirstName = @Fname
	and LastName = @LName
	and datediff(day,isnull(dob,'1-1-1900'),isnull(@dob,'1-1-1900'))= 0
--	and isnull(dob,'') = isnull(@DOB,'')
	--and (Select Count(*) from tblticketsFlag  where ticketId_Pk = t.ticketId_pk and FlagId = 36) >= 1

delete tblticketsFlag from tblticketsFlag tf
	inner join #temp1 t on tf.ticketId_pk = t.ticketId_Pk
	where FlagId = @flagid
	and  t.ticketId_Pk != @ticketid

insert into tblticketsnotes(ticketid,subject,employeeid) 
	select ticketId_Pk,'Flag -'+ @desc + ' Removed for ticket number (' + @refcasenumber + ')',@empid
	from #temp1	
	where ticketId_pk != @TicketID

	update tbltickets set isProblemClientManual = NULL where ticketId_pk in (select ticketId_pk from #temp1)
	
	Delete from ProblemClientLookup 
	where FirstName = @Fname
	--and isnull(MiddleName,'') = isnull(@MName,'')
	and LastName = @LName
	and datediff(day,isnull(dob,'1-1-1900'),isnull(@dob,'1-1-1900'))= 0
	
	drop table #temp1

end  







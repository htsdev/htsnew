SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HMC_ADD_CaseToQueue]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HMC_ADD_CaseToQueue]
GO


CREATE Procedure [dbo].[USP_HMC_ADD_CaseToQueue]  
  
(  
  
@empid int,  
@Recordid int  
)  
  
as  
  
insert into tbl_hmc_efile (ticketnumber_fk,ViolationNumber_fk, Recordid, Empid,flag)  
select   tva.ticketnumber_pk ,tva.ViolationNumber_pk , tva.Recordid , @empid,0   
from   dbo.tblTicketsArchive AS ta INNER JOIN  
                      dbo.tblTicketsViolationsArchive AS tva ON ta.RecordID = tva.RecordID INNER JOIN  
                      dbo.tblCourtViolationStatus ON tva.violationstatusid = dbo.tblCourtViolationStatus.CourtViolationStatusID  
WHERE     (dbo.tblCourtViolationStatus.CategoryID = 2)  
and  tva.recordid  = @Recordid  
  
  
--USP_HMC_ADD_CaseToQueue 


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


/*
* Created By :- Noufil 5884 07/14/2009
* Business Logic : This procedure returns records of clients thats apearsin Set call and Reminder call SP respectively.
*/

ALTER PROCEDURE [dbo].[USP_HTP_GET_SET_AND_REMINDER_CALL_FOR_SMS]
AS

EXEC USP_HTP_Get_SetCallsActivities '6/27/2009',5,1,1,-1,3,0,'01/01/1900','01/01/1900',1
EXEC [USP_HTS_Get_remainderSMS_Update] '6/30/2009',5,1,0,-1,3,0,'01/01/1900','01/01/1900',1
--Farrukh 10367 10/12/2012 Added New SP for Reminder SMS
--EXEC USP_HTS_Get_remainderCalls_Update '6/30/2009',5,1,0,-1,3,0,'01/01/1900','01/01/1900',1

GO
GRANT EXECUTE ON  [dbo].[USP_HTP_GET_SET_AND_REMINDER_CALL_FOR_SMS] TO dbr_webuser
GO
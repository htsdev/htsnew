﻿set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

/****** 
Create by:  Fahad Qureshi

Business Logic : This procedure simply Update Batch Trial Print email record's history 

******/

ALTER Procedure [dbo].[USP_HTP_Update_BatchPrintEmail]
	
		@LetterID int,                                              
		@ticketidlist varchar(8000) = '0,',          
		@TicketIDBatch varchar(8000), 
	AS       	  
	UPDATE	
		tblhtsbatchprintletter
	SET       
		IsEmail=1    
			
	WHERE	
		LetterID_Fk = @LetterID                                               
		AND  
		deleteflag<>1                                                             
		--AND 
		--cast(convert(varchar(12), ticketid_fk) + convert(varchar(12), BatchID_PK) as bigint) IN (select ticketid from @tickets)     
		AND 
		tblhtsbatchprintletter.ticketid_fk not in (select distinct ticketid_pk from tblticketsflag where flagid = 7)  
                 
go

grant exec on [dbo].[USP_HTP_Update_BatchPrintEmail] to dbr_webuser
go 
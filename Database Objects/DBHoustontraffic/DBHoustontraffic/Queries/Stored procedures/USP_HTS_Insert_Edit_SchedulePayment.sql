
/**
* Business Logic : This method use to Insert or Update Schedule Payment then it update payment follow update with adding notes in history
* 
**/

        
ALTER PROCEDURE [dbo].[USP_HTS_Insert_Edit_SchedulePayment]
	@TicketID INT,
	@Amount MONEY,
	@PayDate DATETIME,
	@EmpID INT,
	@Scheduleid INT
AS
	DECLARE @owes AS MONEY  
	DECLARE @schedulesum AS MONEY  
	
	IF (@Scheduleid = 0)
	BEGIN
	    INSERT INTO tblschedulepayment
	      (
	        TicketID_PK,
	        Amount,
	        PaymentDate,
	        EmpID,
	        scheduleflag
	      )
	    VALUES
	      (
	        @TicketID,
	        @Amount,
	        @PayDate,
	        @EmpID,
	        1
	      )
	    
	    --Insert into tblnotes                         
	    
	    INSERT INTO tblticketsnotes
	      (
	        ticketid,
	        SUBJECT,
	        employeeid,
	        Notes
	      )
	    SELECT @TicketID,
	           'Scheduled $' + CONVERT(VARCHAR(12), @Amount),
	           @EmpID,
	           NULL
	    FROM   tblusers U
	    WHERE  employeeid = @EmpID
	END
	ELSE
	BEGIN
	    UPDATE tblschedulepayment
	    SET    Amount = @Amount,
	           PaymentDate = @PayDate
	    WHERE  ticketid_pk = @TicketID
	           AND scheduleid = @Scheduleid 
	    --Insert into tblnotes                         
	    
	    INSERT INTO tblticketsnotes
	      (
	        ticketid,
	        SUBJECT,
	        employeeid,
	        Notes
	      )
	    SELECT @TicketID,
	           'Scheduled $' + CONVERT(VARCHAR(12), @Amount),
	           @EmpID,
	           NULL
	    FROM   tblusers U
	    WHERE  employeeid = @EmpID
	END 
	
	-- Noufil 5618 04/02/2009 Update Payment Follow Update and add note in history
	
	DECLARE @oldfollowupdate DATETIME
	SET @oldfollowupdate = (
	        SELECT ISNULL(tt.PaymentDueFollowUpDate, '01/01/1900')
	        FROM   tblTickets tt
	        WHERE  tt.TicketID_PK = @TicketID
	    )
	
	DECLARE @oldfollowvarchar VARCHAR(20) 
	SET @oldfollowvarchar = CASE @oldfollowupdate
	                             WHEN '01/01/1900' THEN 'N/A'
	                             ELSE CONVERT(VARCHAR(20), @oldfollowupdate, 101)
	                        END
	
	DECLARE @NewPaymentFollowupdate DATETIME
	SET @NewPaymentFollowupdate = (
	        SELECT MIN(TSP.PaymentDate)
	        FROM   tblSchedulePayment tsp
	        WHERE  TSP.TicketID_PK = @TicketID
	               AND DATEDIFF(DAY, TSP.PaymentDate, GETDATE()) < 0
	               AND TSP.ScheduleFlag = 1
	    )
	
	IF (@oldfollowupdate <> @NewPaymentFollowupdate)
	BEGIN
	    UPDATE tblTickets
	    SET    PaymentDueFollowUpDate = @NewPaymentFollowupdate
	    WHERE  TicketID_PK = @TicketID 
	    
	    INSERT INTO tblticketsnotes
	      (
	        ticketid,
	        SUBJECT,
	        employeeid,
	        Notes
	      )
	    SELECT @TicketID,
	           'Payment Follow-Up Date has been changed from ' + @oldfollowvarchar
	           + ' to ' + CONVERT(VARCHAR(12), @NewPaymentFollowupdate, 101),
	           @EmpID,
	           NULL
	    FROM   tblusers U
	    WHERE  employeeid = @EmpID
	END----------------------------------------------------------------------------------------------
GO



SET QUOTED_IDENTIFIER OFF 
GO

SET ANSI_NULLS ON 
GO


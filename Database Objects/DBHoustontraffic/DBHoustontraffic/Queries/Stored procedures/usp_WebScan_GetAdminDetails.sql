SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_WebScan_GetAdminDetails]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_WebScan_GetAdminDetails]
GO


CREATE procedure [dbo].[usp_WebScan_GetAdminDetails]  
    
as             
            
select o.picid,            
   o.causeno,            
   o.status,          
   o.courtno,              
   convert(varchar(12),o.newcourtdate,101)as courtdate,            
   convert(varchar(12),o.todaydate,101)as todaydate,            
   o.location,            
   o.time    
     
            
from tbl_WebScan_Batch b            
            
 inner join tbl_WebScan_pic p on    
 b.batchid=p.batchid inner join tbl_webscan_ocr o on     
 p.id=o.picid    
   
    
            
where             
  o.scanverified=1 and o.adminverified =0         
order by o.picid       


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


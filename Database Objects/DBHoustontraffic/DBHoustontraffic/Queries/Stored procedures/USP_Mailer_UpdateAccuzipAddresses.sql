-- =============================================
-- Author:		Tahir Ahmed
-- Create date: 07/21/2007
-- Description:	The stored procedure is used by LMS MSMQ Listener service application to get 
--				to update the addresses in all the databases corrected or updated by ACCUZIP 
--				application.

-- List Of Parameters:
--		None

-- List of output columns:
--		It returns a string contains information how many records updated in each database.
--		the string will be written in application's log file.

--		It also emails the results of the process to the specified address. Email contains the
--		the same information.

-- =============================================


-- USP_Mailer_UpdateAccuzipAddresses
ALTER procedure [dbo].[USP_Mailer_UpdateAccuzipAddresses]

as  
  
declare @sDate  datetime  
set @sDate = getdate()   
  
begin tran  
  
  
/*==================================================================================  
  HOUSTON TRAFFIC TICKETS  
==================================================================================*/  
-- UPDATE ADDRESSES IN TRAFFIC TICKETS DATABASE.....  
select * into #traffic from tblmaileraccuziptemp  
where case_type = 1 and datediff(day, processdate, @sDate)=0  
  
-- UPDATE THE ACCUZIP PROCOESS DATE....  
update t  
set t.accuzipprocessdate = @sDate  
from tblticketsarchive t, #traffic a  
where a.record_id = t.recordid  
and t.accuzipprocessdate is null  
  
-- FIRST UPDATE MOVED ADDRESSES NCOA18.....  
update t  
set t.address1 = a.address,  
 t.address2 = '',  
 t.city = a.city,  
 t.stateid_fk = s.stateid,  
 t.zipcode = a.zip,  
 t.crrt = a.crrt,  
 t.flag1  = 'Y',  
 t.ncoa18flag = 1,  
 t.movedate = a.movedate_,  
 t.dpc = right(replace(isnull(a.barcode,''),'/',''),1),  
 t.dp2 = left(right(replace(isnull(a.barcode,''), '/',''),3),2),  
 t.donotmailflag = case when isnull(t.donotmailflag,0) = 1 then 0 else t.donotmailflag end  
from tblticketsarchive t, #traffic a, tblstate s  
where t.recordid = a.record_id  
and s.state = a.st   
and t.accuzipprocessdate = @sDate  
and a.matchflag_ = 'm'  
AND isnull(a.ank_,'')<> '77'  
  
-- UPDATE NCOA48 FLAG......  
UPDATE t  
set t.ncoa48flag = 1,  
 t.movedate = a.movedate_  
from tblticketsarchive t, #traffic a  
where a.record_id = t.recordid  
and isnull(a.ank_,'') = '77'  
and t.accuzipprocessdate = @sDate  
  
-- NOW UPDATE NON-MOVED ADDRESSES HAVING DISCREPANCY IN DPV FLAG....  
update t  
set t.address1 = a.address,  
 t.address2 = '',  
 t.city = a.city,  
 t.stateid_fk = s.stateid,  
 t.zipcode = a.zip,  
 t.crrt = a.crrt,  
 t.dpv_updated = 1,  
 t.flag1  = left(a.dpv_,1),  
 t.dpc = right(replace(isnull(a.barcode,''),'/',''),1),  
 t.dp2 = left(right(replace(isnull(a.barcode,''), '/',''),3),2),  
 t.donotmailflag = case when isnull(t.donotmailflag,0) = 1 then 0 else t.donotmailflag end  
from tblticketsarchive t, #traffic a, tblstate s  
where t.recordid = a.record_id  
and s.state = a.st   
and t.accuzipprocessdate = @sDate  
and ISNULL(a.matchflag_,'') <> 'm'  
and isnull(a.ank_,'') <> '77'  
AND isnull(t.flag1,'') = 'n'  
and left(a.dpv_,1) in ('y','d','s')  
  
  
-- NOW UPDATE OTHERS RELATED TO LACS_ FLAG = 'L' ....  
update t  
set t.address1 = a.address,  
 t.address2 = '',  
 t.city = a.city,  
 t.stateid_fk = s.stateid,  
 t.zipcode = a.zip,  
 t.crrt = a.crrt,  
 t.flag1  = left(a.dpv_,1),  
 t.dpc = right(replace(isnull(a.barcode,''),'/',''),1),  
 t.dp2 = left(right(replace(isnull(a.barcode,''), '/',''),3),2),  
 t.donotmailflag = case when isnull(t.donotmailflag,0) = 1 then 0 else t.donotmailflag end  
from tblticketsarchive t, #traffic a, tblstate s  
where t.recordid = a.record_id  
and s.state = a.st   
and t.accuzipprocessdate = @sDate  
and ISNULL(a.matchflag_,'') <> 'm'  
and isnull(a.ank_,'') <> '77'  
and left(a.dpv_,1) in ('y','d','s')  
AND ISNULL(a.LACS_,'') = 'L'  
  
  
  
/*==================================================================================  
  DALLAS TRAFFIC TICKETS  
==================================================================================*/  
  
-- UPDATE ADDRESSES IN DALLAS TRAFFIC TICKETS DATABASE.....  
select * into #Dallas from tblmaileraccuziptemp  
where case_type = 2 and datediff(day, processdate, @sDate)=0  
  
-- UPDATE THE ACCUZIP PROCOESS DATE....  
update t  
set t.accuzipprocessdate = @sDate  
from dallastraffictickets.dbo.tblticketsarchive t, #Dallas a  
where a.record_id = t.recordid  
and t.accuzipprocessdate is null  
  
-- FIRST UPDATE MOVED ADDRESSES NCOA18.....  
update t  
set t.address1 = a.address,  
 t.address2 = '',  
 t.city = a.city,  
 t.stateid_fk = s.stateid,  
 t.zipcode = a.zip,  
 t.crrt = a.crrt,  
 t.flag1  = 'Y',  
 t.ncoa18flag = 1,  
 t.movedate = a.movedate_,  
 t.dpc = right(replace(isnull(a.barcode,''),'/',''),1),  
 t.dp2 = left(right(replace(isnull(a.barcode,''), '/',''),3),2),  
 t.donotmailflag = case when isnull(t.donotmailflag,0) = 1 then 0 else t.donotmailflag end  
from dallastraffictickets.dbo.tblticketsarchive t, #Dallas a, dallastraffictickets.dbo.tblstate s  
where t.recordid = a.record_id  
and s.state = a.st  
and t.accuzipprocessdate = @sDate  
and a.matchflag_ = 'm'  
AND isnull(a.ank_,'')<> '77'  
  
-- UPDATE NCOA48 FLAG......  
UPDATE t  
set t.ncoa48flag = 1,  
 t.movedate = a.movedate_  
from dallastraffictickets.dbo.tblticketsarchive t, #Dallas a  
where a.record_id = t.recordid  
and isnull(a.ank_,'') = '77'  
and t.accuzipprocessdate = @sDate  
  
-- NOW UPDATE NON-MOVED ADDRESSES HAVING DISCREPANCY IN DPV FLAG....  
update t  
set t.address1 = a.address,  
 t.address2 = '',  
 t.city = a.city,  
 t.stateid_fk = s.stateid,  
 t.zipcode = a.zip,  
 t.crrt = a.crrt,  
 t.dpv_updated = 1,  
 t.flag1  = left(a.dpv_,1),  
 t.dpc = right(replace(isnull(a.barcode,''),'/',''),1),  
 t.dp2 = left(right(replace(isnull(a.barcode,''), '/',''),3),2),  
 t.donotmailflag = case when isnull(t.donotmailflag,0) = 1 then 0 else t.donotmailflag end  
from dallastraffictickets.dbo.tblticketsarchive t, #Dallas a, dallastraffictickets.dbo.tblstate s  
where t.recordid = a.record_id  
and s.state = a.st  
and t.accuzipprocessdate = @sDate  
and ISNULL(a.matchflag_,'') <> 'm'  
and isnull(a.ank_,'') <> '77'  
AND isnull(t.flag1,'') = 'n'  
and left(a.dpv_,1) in ('y','d','s')  
  
-- NOW UPDATE OTHERS RELATED TO LACS_ FLAG = 'L' ....  
update t  
set t.address1 = a.address,  
 t.address2 = '',  
 t.city = a.city,  
 t.stateid_fk = s.stateid,  
 t.zipcode = a.zip,  
 t.crrt = a.crrt,  
 t.flag1  = left(a.dpv_,1),  
 t.dpc = right(replace(isnull(a.barcode,''),'/',''),1),  
 t.dp2 = left(right(replace(isnull(a.barcode,''), '/',''),3),2),  
 t.donotmailflag = case when isnull(t.donotmailflag,0) = 1 then 0 else t.donotmailflag end  
from dallastraffictickets.dbo.tblticketsarchive t, #Dallas a, dallastraffictickets.dbo.tblstate s  
where t.recordid = a.record_id  
and s.state = a.st  
and t.accuzipprocessdate = @sDate  
and ISNULL(a.matchflag_,'') <> 'm'  
and isnull(a.ank_,'') <> '77'  
and left(a.dpv_,1) in ('y','d','s')  
AND ISNULL(a.lacs_,'') = 'L'  
  
/*==================================================================================  
  JIMS DATABASE
==================================================================================*/  
  
-- UPDATE ADDRESSES IN JIMS DATABASE.....  
select * into #Jims from tblmaileraccuziptemp  
where case_type = 3 and datediff(day, processdate, @sDate)=0  
  
-- UPDATE THE ACCUZIP PROCOESS DATE....  
update t  
set t.accuzipprocessdate = @sDate  
from jims.dbo.criminalcases t, #Jims a  
where a.record_id = t.clientid  
and t.accuzipprocessdate is null  
  
-- FIRST UPDATE MOVED ADDRESSES NCOA18.....  
update t  
set t.address = a.address,  
 t.city = a.city,  
 t.state = a.st,  
 t.zip = a.zip,  
 t.dpv_updated = 1,  
 t.flag1  = 'Y',  
 t.ncoa18flag = 1,  
 t.movedate = a.movedate_,  
 t.dpc = right(replace(isnull(a.barcode,''),'/',''),1),  
 t.dp2 = left(right(replace(isnull(a.barcode,''), '/',''),3),2),  
 t.donotmailflag = case when isnull(t.donotmailflag,0) = 1 then 0 else t.donotmailflag end  
from jims.dbo.criminalcases t, #Jims a  
where t.clientid = a.record_id  
and t.accuzipprocessdate = @sDate  
and a.matchflag_ = 'm'  
AND isnull(a.ank_,'')<> '77'  
  
-- UPDATE NCOA48 FLAG......  
UPDATE t  
set t.ncoa48flag = 1,  
 t.movedate = a.movedate_  
from jims.dbo.criminalcases t, #Jims a  
where a.record_id = t.clientid  
and isnull(a.ank_,'') = '77'  
and t.accuzipprocessdate = @sDate  
  
-- NOW UPDATE NON-MOVED ADDRESSES HAVING DISCREPANCY IN DPV FLAG....  
update t  
set t.address = a.address,  
 t.city = a.city,  
 t.state = a.st,  
 t.zip = a.zip,  
 t.flag1  = left(a.dpv_,1),  
 t.dpv_updated = 1,  
 t.dpc = right(replace(isnull(a.barcode,''),'/',''),1),  
 t.dp2 = left(right(replace(isnull(a.barcode,''), '/',''),3),2),  
 t.donotmailflag = case when isnull(t.donotmailflag,0) = 1 then 0 else t.donotmailflag end  
from jims.dbo.criminalcases t, #Jims a  
where t.clientid = a.record_id  
and t.accuzipprocessdate = @sDate  
and ISNULL(a.matchflag_,'') <> 'm'  
and isnull(a.ank_,'') <> '77'  
AND isnull(t.flag1,'') = 'n'  
and left(a.dpv_,1) in ('y','d','s')  
  
-- NOW UPDATE OTHERS RELATED TO LACS_ FLAG = 'L' ....  
update t  
set t.address = a.address,  
 t.city = a.city,  
 t.state = a.st,  
 t.zip = a.zip,  
 t.flag1  = left(a.dpv_,1),  
 t.dpc = right(replace(isnull(a.barcode,''),'/',''),1),  
 t.dp2 = left(right(replace(isnull(a.barcode,''), '/',''),3),2),  
 t.donotmailflag = case when isnull(t.donotmailflag,0) = 1 then 0 else t.donotmailflag end  
from jims.dbo.criminalcases t, #Jims a  
where t.clientid = a.record_id  
and t.accuzipprocessdate = @sDate  
and ISNULL(a.matchflag_,'') <> 'm'  
and isnull(a.ank_,'') <> '77'  
and left(a.dpv_,1) in ('y','d','s')  
and isnull(a.lacs_,'') = 'L'  
  

-- TAHIR 4419 07/28/2008
-- ADDED CIVIL DATABASE FOR ACCUZIP ADDRESS PROCESSING....

/*==================================================================================  
  CIVIL DATABASE.... 
==================================================================================*/  
-- UPDATE ADDRESSES IN TRAFFIC TICKETS DATABASE.....  
select * into #CIVIL from tblmaileraccuziptemp  
where case_type = 5 and datediff(day, processdate, @sDate)=0  
  
-- UPDATE THE ACCUZIP PROCOESS DATE....  
update t  
set t.accuzipprocessdate = @sDate  
from civil.dbo.childcustodyparties t, #CIVIL a  
where a.record_id = t.childcustodypartyid  
and t.accuzipprocessdate is null  
  
-- FIRST UPDATE MOVED ADDRESSES NCOA18.....  
update t  
set t.address = a.address,  
 t.city = a.city,  
 t.stateid = s.stateid,  
 t.zipcode = a.zip,  
 t.crrt = a.crrt,  
 t.flag1  = 'Y',  
 t.ncoa18flag = 1,  
 t.AccuzipAddressMoveDate = a.movedate_,  
 t.dpc = right(replace(isnull(a.barcode,''),'/',''),1),  
 t.dp2 = left(right(replace(isnull(a.barcode,''), '/',''),3),2),  
 t.donotmailflag = case when isnull(t.donotmailflag,0) = 1 then 0 else t.donotmailflag end  
from civil.dbo.childcustodyparties t, #CIVIL a, civil.dbo.tblstate s  
where t.childcustodypartyid = a.record_id  
and s.state = a.st   
and t.accuzipprocessdate = @sDate  
and a.matchflag_ = 'm'  
AND isnull(a.ank_,'')<> '77'  
  
-- UPDATE NCOA48 FLAG......  
UPDATE t  
set t.ncoa48flag = 1,  
 t.AccuzipAddressMoveDate = a.movedate_  
from civil.dbo.childcustodyparties t, #CIVIL a  
where a.record_id = t.childcustodypartyid  
and isnull(a.ank_,'') = '77'  
and t.accuzipprocessdate = @sDate  
  
-- NOW UPDATE NON-MOVED ADDRESSES HAVING DISCREPANCY IN DPV FLAG....  
update t  
set t.address = a.address,  
 t.city = a.city,  
 t.stateid = s.stateid,  
 t.zipcode = a.zip,  
 t.crrt = a.crrt,  
 t.dpv_updated = 1,  
 t.flag1  = left(a.dpv_,1),  
 t.dpc = right(replace(isnull(a.barcode,''),'/',''),1),  
 t.dp2 = left(right(replace(isnull(a.barcode,''), '/',''),3),2),  
 t.donotmailflag = case when isnull(t.donotmailflag,0) = 1 then 0 else t.donotmailflag end  
from civil.dbo.childcustodyparties t, #CIVIL a, civil.dbo.tblstate s  
where t.childcustodypartyid = a.record_id  
and s.state = a.st   
and t.accuzipprocessdate = @sDate  
and ISNULL(a.matchflag_,'') <> 'm'  
and isnull(a.ank_,'') <> '77'  
AND isnull(t.flag1,'') = 'n'  
and left(a.dpv_,1) in ('y','d','s')  
  
  
-- NOW UPDATE OTHERS RELATED TO LACS_ FLAG = 'L' ....  
update t  
set t.address = a.address,  
 t.city = a.city,  
 t.stateid = s.stateid,  
 t.zipcode = a.zip,  
 t.crrt = a.crrt,  
 t.flag1  = left(a.dpv_,1),  
 t.dpc = right(replace(isnull(a.barcode,''),'/',''),1),  
 t.dp2 = left(right(replace(isnull(a.barcode,''), '/',''),3),2),  
 t.donotmailflag = case when isnull(t.donotmailflag,0) = 1 then 0 else t.donotmailflag end  
from civil.dbo.childcustodyparties t, #CIVIL a, civil.dbo.tblstate s  
where t.childcustodypartyid = a.record_id  
and s.state = a.st   
and t.accuzipprocessdate = @sDate  
and ISNULL(a.matchflag_,'') <> 'm'  
and isnull(a.ank_,'') <> '77'  
and left(a.dpv_,1) in ('y','d','s')  
AND ISNULL(a.LACS_,'') = 'L'  

--Farrukh 9948 12/27/2011 Included AssumedNames database
/*==================================================================================  
  ASSUMED NAMES
==================================================================================*/  
  
-- UPDATE ADDRESSES IN ASSUMED NAMES DATABASE.....  
select * into #AssumedNames from tblmaileraccuziptemp  
where case_type = 6 and datediff(day, processdate, @sDate)=0  
  
-- UPDATE THE ACCUZIP PROCOESS DATE....  
update t  
SET  t.AccuZipProcessDate = @sDate  
FROM AssumedNames.dbo.Business t, #AssumedNames a  
where a.record_id = t.BusinessID  
and t.AccuZipProcessDate is null  
  
-- UPDATE MOVED ADDRESSES .....  
update t  
set t.[Address] = a.address,   
 t.City = a.city,  
 t.[State] = a.ostate,  
 t.Zip = a.zip,   
 t.FLAGS = 'Y',    
 t.DPC = right(replace(isnull(a.barcode,''),'/',''),1),  
 t.DP2 = left(right(replace(isnull(a.barcode,''), '/',''),3),2) 
from AssumedNames.dbo.Business t, #AssumedNames a  
WHERE t.BusinessID = a.record_id  
and t.AccuZipProcessDate = @sDate  


-- Sabir Khan 9975 01/12/2012 Added logic for processing owner address by accuzip.
select * into #AssumedNames1 from tblmaileraccuziptemp  
where case_type = 7 and datediff(day, processdate, @sDate)=0  

update bo  
SET  bo.AccuZipProcessDate = @sDate  
FROM AssumedNames.dbo.BusinessOwner bo , #AssumedNames1 a  
--where a.record_id = bo.BusinessID
where a.record_id = bo.OwnerID--Fahad 10236 04/27/2012 Getting the Correct Owner Id Information for accuzip processing    
and bo.AccuZipProcessDate is null 


update bo  
set bo.OwnerAddress = a.address,   
 bo.OwnerCity =  a.city,  
 bo.OwnerState =  a.ostate,  
 bo.OwnerZip = a.zip,   
 bo.Flags = 'Y',    
 bo.DPC =  right(replace(isnull(a.barcode,''),'/',''),1),  
 bo.DP2 = left(right(replace(isnull(a.barcode,''), '/',''),3),2) 
from AssumedNames.dbo.BusinessOwner bo, #AssumedNames1 a  
WHERE bo.BusinessID = a.record_id  
and bo.AccuZipProcessDate = @sDate 

/*==================================================================================  
  EMAIL THE STATISTICS......  
==================================================================================*/  
declare @subject varchar(100),  
  @body varchar(max)  ,
  @HTMLBody varchar(max)
  
declare 
  @NCOA18 int,  
  @NCOA48 int,  
  @Other int,  
  @Processed int,  
  @Invalid int,  
  @Unchanged int

DECLARE @Temp table (dbname varchar(50), processed int, invalid int, unchanged int, ncoa18 int, ncoa48 int, other int)  
  
-- HOUSTON....  
select @NCOA18 =0,   @NCOA48 =0,   @Other =0,   @Processed =0,   @Invalid =0,   @Unchanged =0

select @Processed = count(distinct recordid ) from tblticketsarchive   
where accuzipprocessdate = @sDate   
  
select @NCOA18 =   count(distinct recordid ) from tblticketsarchive   
where accuzipprocessdate = @sDate and ncoa18flag  =1  
  
select @NCOA48 =   count(distinct recordid ) from tblticketsarchive   
where accuzipprocessdate = @sDate and ncoa48flag  =1  
  
select @Other =   count(distinct recordid ) from tblticketsarchive   
where accuzipprocessdate = @sDate and dpv_updated  = 1  

select @Invalid = count(distinct recordid) from tblticketsarchive t, #traffic a
where a.record_id = t.recordid
and len(replace(isnull(barcode,''),'/','')) <> 12

select @Unchanged = @Processed - (@NCOA18 + @NCOA48 +  @Other+ @invalid)  

  
insert into @temp (dbname, processed, unchanged, ncoa18, ncoa48, other, invalid)  
select 'Houston' ,@Processed, @Unchanged, @NCOA18, @NCOA48, @Other  , @Invalid
  
-- DALLAS....  
select @NCOA18 =0,   @NCOA48 =0,   @Other =0,   @Processed =0,   @Invalid =0,   @Unchanged =0

select @Processed = count(distinct recordid ) from dallastraffictickets.dbo.tblticketsarchive   
where accuzipprocessdate = @sDate   
  
select @NCOA18 =   count(distinct recordid ) from dallastraffictickets.dbo.tblticketsarchive   
where accuzipprocessdate = @sDate and ncoa18flag  =1  
  
select @NCOA48 =   count(distinct recordid ) from dallastraffictickets.dbo.tblticketsarchive   
where accuzipprocessdate = @sDate and ncoa48flag  =1  
  
select @Other =   count(distinct recordid ) from dallastraffictickets.dbo.tblticketsarchive   
where accuzipprocessdate = @sDate and dpv_updated  = 1  

select @Invalid = count(distinct recordid) from tblticketsarchive t, #dallas a
where a.record_id = t.recordid
and len(replace(isnull(barcode,''),'/','')) <> 12

select @Unchanged = @Processed - (@NCOA18 + @NCOA48 +  @Other+ @invalid)  
  
insert into @temp (dbname, processed, unchanged, ncoa18, ncoa48, other, invalid)  
select 'Dallas' , @Processed, @Unchanged, @NCOA18, @NCOA48, @Other   , @Invalid
  
-- JIMS...... 
select @NCOA18 =0,   @NCOA48 =0,   @Other =0,   @Processed =0,   @Invalid =0,   @Unchanged =0
 
select @Processed = count(distinct clientid ) from jims.dbo.criminalcases   
where accuzipprocessdate = @sDate   
  
select @NCOA18 =   count(distinct clientid ) from jims.dbo.criminalcases   
where accuzipprocessdate = @sDate and ncoa18flag  =1  
  
select @NCOA48 =   count(distinct clientid ) from jims.dbo.criminalcases   
where accuzipprocessdate = @sDate and ncoa48flag  =1  
  
select @Other =   count(distinct clientid ) from jims.dbo.criminalcases   
where accuzipprocessdate = @sDate and dpv_updated  = 1  

select @Invalid = count(distinct clientid) from jims.dbo.criminalcases t, #JIMS a
where a.record_id = t.clientid
and len(replace(isnull(barcode,''),'/','')) <> 12

select @Unchanged = @Processed - (@NCOA18 + @NCOA48 +  @Other+ @invalid)  
  
insert into @temp (dbname, processed, unchanged, ncoa18, ncoa48, other, invalid)  
select 'JIMS' , @Processed, @Unchanged,  @NCOA18, @NCOA48, @Other   , @Invalid

-- CIVIL......  
select @NCOA18 =0,   @NCOA48 =0,   @Other =0,   @Processed =0,   @Invalid =0,   @Unchanged =0

select @Processed = count(distinct childcustodypartyid ) from civil.dbo.childcustodyparties   
where accuzipprocessdate = @sDate   
  
select @NCOA18 =   count(distinct childcustodypartyid ) from civil.dbo.childcustodyparties    
where accuzipprocessdate = @sDate and ncoa18flag  =1  
  
select @NCOA48 =   count(distinct childcustodypartyid ) from civil.dbo.childcustodyparties   
where accuzipprocessdate = @sDate and ncoa48flag  =1  
  
select @Other =   count(distinct childcustodypartyid ) from civil.dbo.childcustodyparties  
where accuzipprocessdate = @sDate and dpv_updated  = 1  

select @Invalid = count(distinct childcustodypartyid) from civil.dbo.childcustodyparties t, #civil a
where a.record_id = t.childcustodypartyid
and len(replace(isnull(barcode,''),'/','')) <> 12

select @Unchanged = @Processed - (@NCOA18 + @NCOA48 +  @Other+ @invalid)    
  
insert into @temp (dbname, processed, unchanged, ncoa18, ncoa48, other, invalid)  
select 'Civil' , @Processed, @Unchanged,  @NCOA18, @NCOA48, @Other   , @Invalid
  

--Farrukh 9948 12/27/2011 Included AssumedNames database
-- AssumedNames...... 
select @NCOA18 =0,   @NCOA48 =0,   @Other =0,   @Processed =0,   @Invalid =0,   @Unchanged =0
 
select @Processed = count(DISTINCT BusinessID) FROM AssumedNames.dbo.Business   
WHERE accuzipprocessdate = @sDate  

select @Processed = @Processed + count(DISTINCT BusinessID) FROM AssumedNames.dbo.BusinessOwner    
WHERE accuzipprocessdate = @sDate 

select @Invalid = count(distinct BusinessID) from AssumedNames.dbo.Business t, #AssumedNames a
where a.record_id = t.BusinessID
and len(replace(isnull(barcode,''),'/','')) <> 12

select @Unchanged = @Processed - (@NCOA18 + @NCOA48 +  @Other+ @invalid)  
  
insert into @temp (dbname, processed, unchanged, ncoa18, ncoa48, other, invalid)  
select 'AssumedNames' , @Processed, @Unchanged,  @NCOA18, @NCOA48, @Other   , @Invalid  
  
  
  
--  tahir 4659 08/22/08 changed the format for summary email....
-- GENERATING EMAIL SUBJECT & BODY......  
 set @subject  = 'LMS Accuzip Address Processing '+ convert(varchar, getdate())  
 set @HTMLBody = '<table cellspacing="0" cellpadding="0" rules="all" border="1" style="border-color:Black;border-width:1px;border-style:Solid;width:930px;border-collapse:collapse;">'
 set @HTMLBody = @HTMLBody + '
		<tr>
			<td>&nbsp;<B>Database</B></td>
			<td>&nbsp;<B>Processed</B></td>
			<td>&nbsp;<B>Invalid</B></td>
			<td>&nbsp;<B>Unchanged</B></td>
			<td>&nbsp;<B>NCOA 18</B></td>
			<td>&nbsp;<B>NCOA 48</B></td>
			<td>&nbsp;<B>DPV Changed</B></td>
		</tr>'
 
 -- FOR SUMMARY EMAIL....
 select @HTMLBody = @HTMLBody + 
		'<tr>' +
			'<td>&nbsp;' + dbname+ ' </td>' +
			'<td>&nbsp;' +convert(varchar,processed)+ '</td>' + 
			'<td>&nbsp;' +convert(varchar,invalid)+   '</td>' + 
			'<td>&nbsp;' +convert(varchar,unchanged)+ '</td>' +
			'<td>&nbsp;' +convert(varchar,ncoa18)+    '</td>' +
			'<td>&nbsp;' +convert(varchar,ncoa48)+    '</td>' +
			'<td>&nbsp;' +convert(varchar,other)+     '</td>' +
		'</tr>'
 from @temp  order by dbname

 set @HTMLBody = @HTMLBody + '</table>'
 -- end 4659
 
 -- FOR LOG FILE.... 
 set @body = ''  
 select @body = @body + dbname+ ': Total Processed('+convert(varchar,processed)+'), Invalid('+convert(varchar,invalid)+'), Unchanged('+convert(varchar,unchanged)+'), NCOA18('+convert(varchar,ncoa18)+'), NCOA48('+convert(varchar,ncoa48)+'), DPV Updated('+convert(varchar,other)+') Records updated!' + char(13)  
 from @temp   
  
  
  
  
  
  
 -- EMAIL THE COUNT OF UPDATED RECORDS....  
 exec msdb.dbo.sp_send_dbmail  
  @profile_name = 'Traffic System',  
  @recipients = 'dataloaders@sullolaw.com', 
  @blind_copy_recipients  = 'fahad.qureshi@lntechnologies.com',
  @body = @HTMLBody, 
  @body_format = 'HTML' ,
  @subject = @subject  
  
if @@error <> 0  
 rollback tran  
else  
 commit tran  
  
-- OUT PUT THE RESULTS FOR LOG FILE...
select left(@body, len(@body)-1)  



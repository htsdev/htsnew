SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_WebScan_GetRecordsByPicID]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_WebScan_GetRecordsByPicID]
GO


CREATE procedure [dbo].[usp_WebScan_GetRecordsByPicID]  
@PicID as int   
as  
  
select   
 o.causeno,  
 o.courtno,
 convert(varchar(12),o.newcourtdate,101) as courtdate,  
 convert(varchar(12),o.todaydate,101) as todaydate,  
 o.time,  
 o.status,  
 o.location  
  
from tbl_WebScan_OCR o  
  
where o.picid=@PicID  


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


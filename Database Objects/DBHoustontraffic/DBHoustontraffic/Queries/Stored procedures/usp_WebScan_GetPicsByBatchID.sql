SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_WebScan_GetPicsByBatchID]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_WebScan_GetPicsByBatchID]
GO


CREATE procedure [dbo].[usp_WebScan_GetPicsByBatchID]

@BatchId as int

as 

select id as PicID from tbl_WebScan_Pic
where
BatchID=@BatchId 



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


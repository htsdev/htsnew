/*  
* Created By		: SAEED AHMED
* Task ID			: 7844 
* Created Date		: 06/01/2010  
* Business logic	: This procedure is used update followup date for bad email address.
* Parameter List  
* @FollowUpDate		: Represents followup date for bad email address which need to be set.
* @TicketID			: Represents TicketID against the updation will perform.
* @empid			: Represents EmployeeID which need to be set.
*
*/         
CREATE  PROCEDURE [dbo].[USP_HTP_Update_BadEmailAddressFollowupDate]
	@FollowUpDate DATETIME,
	@TicketID INT,
	@empid INT
AS
	UPDATE tblTickets
	SET    BadEmailAddressFollowupDate = @FollowUpDate,
	       EmployeeID = @empid
	WHERE  TicketID_PK = @TicketID	
GO
GRANT EXECUTE ON dbo.USP_HTP_Update_BadEmailAddressFollowupDate TO dbr_webuser
GO

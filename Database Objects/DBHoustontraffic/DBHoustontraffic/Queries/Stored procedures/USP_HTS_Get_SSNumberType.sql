/*  
Created By : Abbas Qamar
Task Id : 10114
Creation Date : 03-28-2012
Business Logic : This procedure returns SSN Number Type 


*/

CREATE PROCEDURE [dbo].[USP_HTS_Get_SSNumberType]  



AS  

SELECT     SSNTypeId,SSNType
FROM        tblSSNumberType 

GO
GRANT EXECUTE ON [dbo].[USP_HTS_Get_SSNumberType] TO dbr_webuser
GO 




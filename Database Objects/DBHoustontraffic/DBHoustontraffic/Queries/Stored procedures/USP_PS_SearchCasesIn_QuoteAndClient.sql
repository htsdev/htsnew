      
        
        
-- USP_PS_SearchCasesIn_QuoteAndClient  '077186916'         
alter Procedure [dbo].[USP_PS_SearchCasesIn_QuoteAndClient]         
        
(        
@TicketNumber varchar(20)                
)        
        
as        
           
        
        
if Len(@TicketNumber) > 1              
 select @Ticketnumber = left(@Ticketnumber, len (@TicketNumber)-1 ) + '%'           
        
declare @temp table  ( RecordID int ,TicketID int,TicketNumber_Pk varchar(20) ,Name varchar(50),Address varchar(100) , CourtDate datetime,URL varchar(50) , Flag int )                
        
        
        
Insert Into @temp ( RecordID , TicketID,TicketNumber_pk ,Name,Address , CourtDate ,URL, Flag ) select   distinct top 100 v.recordid,0,v.ticketnumber_pk, t.lastname + ',' + t.firstname,isnull(t.address1,'') + isnull(t.address2,''),              
v.courtdate,              
 'violations_fees_pagenew.aspx' ,0        
from  tblticketsviolationsarchive v               
inner join tblticketsarchive t on t.recordid = v.recordid              
and  v.ticketnumber_pk like @ticketnumber               
and t.ClientFlag = 0         
                
        
Insert Into @temp ( RecordID , TicketID,TicketNumber_pk ,Name,Address , CourtDate ,URL, Flag )             
          
Select  Distinct top 100  tv.RecordID , t.TicketID_Pk ,tv.refcaseNumber , t.lastname + ',' + t.firstname , isnull(t.address1,'') + isnull(t.address2,'')        
,tv.courtdate,'QuoteClient.aspx',1        
        
 from tbltickets t                 
join tblticketsviolations  tv                
on                 
t.ticketid_pk = tv.Ticketid_pk                
where tv.refcaseNumber like  @TicketNumber + '%'          
and t.activeFlag = 0            
          
        
            
Select top 100 * from @temp        
        
        
Select Count(*) from @temp        
        
        
        
  
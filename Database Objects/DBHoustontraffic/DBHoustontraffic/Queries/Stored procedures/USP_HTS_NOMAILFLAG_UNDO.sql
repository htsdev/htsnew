SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_NOMAILFLAG_UNDO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_NOMAILFLAG_UNDO]
GO

  
            
              
CREATE PROCEDURE [dbo].[USP_HTS_NOMAILFLAG_UNDO]                    
@FirstName varchar(20),                        
@LastName varchar(20),                        
@Address varchar(50),                      
@Zip varchar(12)                        
as                    
-- For Traffic clients                
update tblticketsarchive                         
 set DoNotMailFlag =0                    
where                    
 firstname = @FirstName and                    
 lastname = @LastName and                    
 address1+' '+address2 = @Address and                    
 ZipCode like(@Zip+'%')                
             
                  
                
-- For criminal clients                
update jims.dbo.criminalcases                         
 set DoNotMailFlag =0                    
where                    
 firstname = @FirstName and                    
 lastname = @LastName and                    
 address = @Address and                    
 Zip like(@Zip+'%')                
              
              
--for Dallas              
update dallastraffictickets.dbo.tblticketsarchive                         
 set dallastraffictickets.dbo.tblticketsarchive.DoNotMailFlag =0                    
where                    
 dallastraffictickets.dbo.tblticketsarchive.firstname = @FirstName and                    
 dallastraffictickets.dbo.tblticketsarchive.lastname = @LastName and                    
dallastraffictickets.dbo.tblticketsarchive.address1+' '+dallastraffictickets.dbo.tblticketsarchive.address2 = @Address and                    
 dallastraffictickets.dbo.tblticketsarchive.ZipCode like(@Zip+'%')              
              
                
delete from tbl_hts_donotmail_records                    
where                    
 firstname = @FirstName and                    
 lastname = @LastName and                    
 isnull(address,'') = @Address and                    
 Zip like(@Zip+'%')                  
              
  
  
  
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


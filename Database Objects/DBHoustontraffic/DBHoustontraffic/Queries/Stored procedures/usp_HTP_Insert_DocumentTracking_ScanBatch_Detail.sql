/****** 
Create by:  Syed Muhammad Ozair

Business Logic : this procedure is used to Insert the information in tbl_htp_documenttracking_scanbatch_detail table and 
				 returns the inserted id back to the calling method

List of Parameters:	
	@ScanBatchDetailID : id against which records are updated
	@DocumentBatchID : docuemnt id 
	@DocumentBatchPageCount : total page count
	@DocumentBatchPageNo : page no
	@EmployeeID : employee id of current logged in user

List of Columns: 	
	ScanBatchDetailID : currently inserted ID, this field will be returned to the calling method. 
	
******/

Create Procedure dbo.usp_HTP_Insert_DocumentTracking_ScanBatch_Detail

@ScanBatchID_FK	int,
@DocumentBatchID	int,
@DocumentBatchPageCount	int,
@DocumentBatchPageNo	int,
@EmployeeID	int

as

Declare @SBDID int

set @SBDID = 0

insert into tbl_htp_documenttracking_scanbatch_detail(ScanBatchID_FK, DocumentBatchID, DocumentBatchPageCount, DocumentBatchPageNo, IsVerified, EmployeeID)
values(@ScanBatchID_FK, @DocumentBatchID, @DocumentBatchPageCount, @DocumentBatchPageNo, 0, @EmployeeID)

select @SBDID = Scope_Identity()

select @SBDID as ScanBatchDetailID

go

grant exec on dbo.usp_HTP_Insert_DocumentTracking_ScanBatch_Detail to dbr_webuser
go
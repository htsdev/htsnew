SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HMC_Administration_Report]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HMC_Administration_Report]
GO



CREATE Procedure [dbo].[USP_HMC_Administration_Report] --'',null,null,3991,0  
(  
@CauseNo varchar(20),  
@StartDate varchar(14) ,   
@EndDate varchar(14),  
@empid int ,  
@Admin int   
)  
  
as  
  
  
Declare  @Query  varchar(3000)  
  
Select @Query =   
'SELECT       
      
   thef.id,    
   thef.PLADocument,
   tva.CauseNumber,        
   thef.TicketNumber_fK,   
   thef.ViolationNumber_fk,   
   ta.LastName + '' '' + ta.FirstName as FullName ,  
   ta.FirstName,       
   ta.LastName,  
   ta.midnumber ,     
   ta.Initial,     
   ta.DOB,      
   thef.Flag,    
   tva.ViolationDescription,          
   tva.TicketViolationDate,       
   tva.Courtnumber,       
   tva.CourtDate,       
   tva.RecordID,       
   ISNULL(tcv.ShortDescription, N''N/A'') AS ShortDescription ,  
   Case when Flag > 0 then ''PLEA OF NOT GUILTY'' end as Documents,  
   thef.EFileDate ,   
   thef.EFileName  ,  
  tf.firmabbreviation as Firm  
       
  FROM          
  
tblticketsviolationsarchive tva  
join tblticketsarchive ta  
on tva.recordid = ta.recordid Join tbl_hmc_efile thef  
on tva.ticketnumber_pk = thef.ticketnumber_fk  
and tva.violationnumber_pk = thef.violationnumber_fk  
join dbo.tblCourtViolationStatus AS tcv  
on  
tcv.CourtViolationStatusID = tva.violationstatusid  
join tbl_hmc_users thu  
on thef.empid = thu.s_no  
join  tblfirm tf  
on thu.firm = tf.firmid  
  
'  
  
  
if @Causeno <> ''     
 Begin   
   
  if  @Admin = 1    
   Begin   
    Select @Query = @Query + ' where tva.courtdate between  ''' + @StartDate  + '''AND''' + @EndDate + ''' or tva.causenumber =''' +  @Causeno + ''' and thef.flag > 0 order by tva.recordid'       
   end   
    
  if @Admin = 0    
   Begin   
    Select @Query = @Query +  ' where tva.courtdate between  ''' + @StartDate  + '''AND''' + @EndDate + ''' or tva.causenumber =''' +  @Causeno + ''' and thef.empid = ' + Convert(varchar(4),@empid )+  '  and  thef.flag > 0 order by tva.recordid'       
   End  
  
 end   
  
else   
begin   
  if  @Admin = 1    
   Begin   
    Select @Query = @Query + ' where tva.courtdate between  ''' + @StartDate  + '''AND''' + @EndDate + ''' and thef.flag > 0 order by tva.recordid'   
  
   end   
    
  if @Admin = 0   
   Begin   
    Select @Query = @Query + ' where tva.courtdate between  ''' + @StartDate  + '''AND''' + @EndDate + ''' and thef.empid = ' + Convert(varchar(4),@empid) +  '  and  thef.flag > 0 order by tva.recordid'   
  
   End  
End  
  
   
exec (@Query)


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTP_CriminalDocketAlert]    Script Date: 08/08/2008 01:16:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Created By : Sabir Khan 4349  07/10/2008
Business Logic : check next day cases of harris county criminal court if doesn't return any record then shot email to blankdocket@sullolaw.com 

List of Parameters:    No Parameters
*/  
ALTER PROCEDURE [dbo].[USP_HTP_CriminalDocketAlert]
AS

DECLARE @IsExist int --check for blank
DECLARE @NextDate datetime
declare @Sub nvarchar(1000)
set @Sub='Harris County Criminal Docket Report ' + dbo.fn_DateFormat(getdate(),1,'/',':',1) + ' No record found.' --Subject Line
SET @NextDate = getdate() + 1 --set to next day
--check cases for the harris county criminal court
EXEC [usp_hts_get_trial_docket_report_new_Update] '3037',@NextDate,@isdatafound=@IsExist OUTPUT
IF @IsExist= 0 --if doesn't return any record
BEGIN
--send email
EXEC msdb.dbo.sp_send_dbmail                                 
  @profile_name = 'Traffic System',                                 
  @recipients ='hcriminal@sullolaw.com', 
  @subject = @Sub,
  -- Use sullo law header and footer.                                
  -- Rab Nawaz 10914 07/30/2013 Removed Disclimer. . . 
  @body = '<style type="text/css">.Label{ FONT-WEIGHT: normal;  COLOR: #000000; FONT-STYLE: normal; FONT-FAMILY: Verdana; FONT-SIZE: 8.5pt; border-style:none; border-width:0px; }.SmallLabel {FONT-WEIGHT: normal;  COLOR:Gray; FONT-STYLE: normal; FONT-FAMILY: Verdana; FONT-SIZE: 7pt; border-style:none; border-width:0px;}</style><table border="0" bgcolor="#D9D9D9" cellpadding="0" cellspacing="0" align="center" height="100%" width="100%"><tr> <td width="100%" style="height: 100%"><br><table border="1" bgcolor="white" bordercolor="Silver" cellpadding="8" cellspacing="0" align="center" height="100%" width="500"><tr> <td align="center" style="height: 5px; border:1;border-color:Silver;"><img src="http://www.sullolaw.com/images/SulloLawlogo.jpg"/> </td></tr><tr> <td valign="top" width="100%" style="height: 400px"> <table border="0" align="center" cellpadding="0" cellspacing="0" width="100%">
			<tr><td class="Label" ></td><td class="Label"><br /><br />No record found.<br /></td></tr><tr> <td class="Label" style="width: 13px"></td><td class="Label"><br /><br />Thank you,<br /><br /><br /></td></tr> <tr><td style="width: 13px"></td><td> <font size=5 face=Calibri><span style=font-size:16.0pt;font-family:Calibri;> Sullo <o:p></o:p></span></font><font size=4 face="Lucida Fax"><span style="font-size:14.0pt;font-family: Lucida Fax";>&</span></font><font size=5 face=Calibri><span style=font-size:16.0pt;font-family:Calibri;> Sullo,</span></font><font size=2 face=Calibri><span style=font-size:9.0pt;font-family:Calibri;><b>LLP</b></span></font></td></tr> <tr><td style="width: 13px; height: 19px"></td> <td class="Label"> <b>Tel #:</b> 713.839.9026</td></tr><tr><td style="width: 13px"></td><td class="Label"> <b>Fax #:</b> 713.523.6634</td></tr><tr> <td style="width: 13px"> </td> <td class="Label">http://www.sullolaw.com<br><br></td></tr></table></td></tr></table></td></tr><tr> <td><table border="0" cellpadding="0" cellspacing="0" align="center" style="width: 452px; height:1px;"><tr><td valign="top" align="center"> 
			--<table style="width: 500px"><tr><td><hr /> </td></tr></table></td></tr> <tr><td align="center" width="100%" class="SmallLabel">This e-mail and the files transmitted with it are the property of Sullo & Sullo <span style=font-size:6pt;>LLP</span> and/or its affiliates, are confidential, and are intended solely for use of the individual or entity to whom this e-mail is addressed. If you are not one of the named recipients or otherwise have reason to believe that you have received this message in error, please notify the sender at 713-839-9026 and delete this message immediately from your computer.&nbsp; Any other use, retention, dissemination, forwarding, printing, or copying of this e-mail is strictly prohibited.<br /><br /></td></tr></table> </td></tr><tr><td align="center" class="SmallLabel">Copyright© 2007, Sullo & Sullo, <span style=font-size:6pt;>LLP</span> All Rights Reserved.<br /><br /><br/>
			</td></tr></table>',                               
  @body_format = 'HTML' ;  

END


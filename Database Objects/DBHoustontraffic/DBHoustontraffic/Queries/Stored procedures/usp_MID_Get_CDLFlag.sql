SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_MID_Get_CDLFlag]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_MID_Get_CDLFlag]
GO

--usp_MID_Get_CDLFlag '35640,','185324,'

  create   procedure [dbo].[usp_MID_Get_CDLFlag]  
 (  
 @TicketIDs varchar(200),  
 @RecordIDs varchar(200)  
 )  
  
as
declare @CDLFlag int  
  
  
-- FIRST GETTING CDL INFORMATION FROM TBLTICKETSARCHIVEPCCR..........  
select	@CDLFlag =  isnull((  
 select top 1 isnull(isCDL,3) from tblticketsarchivePCCR  
 where  recordid_fk  in (select * from dbo.sap_dates(@recordids))  
 order by insertdate desc  
 ) ,3)
  

-- IF CDL INFORMATION IS NOT AVAILALE IN TBLTICKETSARCHIVEPCCR THEN GETTING IT 
-- FROM TBLTICKETS.........
if @cdlflag = 3  
 begin  
  select @CDLFlag = isnull((    
   select top 1 isnull(cdlflag,3) from tbltickets  
   where  ticketid_pk in ( select * from dbo.sap_dates(@ticketids))  
   order by ticketid_pk desc  
   ) ,3)
 end  
  
  
  
select isnull (convert(varchar(5),@cdlflag),3)    --to eliminate null added by zee


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


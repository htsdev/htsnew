﻿     
    
/****** Object:  StoredProcedure [dbo].[USP_HTP_Update_HMCFTAFollowUpdate]  ******/       
/*          
Created By     : Waqas Javed  
Created Date : 03/26/2009    
TasK   : 5697          
Business Logic  : This procedure Updates HMC FTA Follow Up Date and Is removed status.        
             
Parameter:         
   @TicketID  : Updating criteria with respect to TicketId        
   @FollowUpDate : Date of follow Up which has to be set       
   @IsRemoved : IsHMCFTARemoved column 
       
        
*/        
    
CREATE PROCEDURE [dbo].[USP_HTP_Update_HMCFTAFollowUpdate]    
 @FollowUpDate DATETIME,    
 @TicketID INT,
 @IsRemoved bit    
AS    
 UPDATE tblTickets    
 SET    HMCFTAFollowUpDate = @FollowUpDate , IsHMCFTARemoved =  @IsRemoved  
 WHERE  TicketID_PK = @TicketID    
  
 
go

grant execute on [dbo].[USP_HTP_Update_HMCFTAFollowUpdate] to dbr_webuser
go  
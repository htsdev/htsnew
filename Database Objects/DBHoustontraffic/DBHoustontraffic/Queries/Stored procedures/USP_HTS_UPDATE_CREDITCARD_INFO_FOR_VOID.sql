﻿set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go
/*******

Alter By: Noufil Khan 3961 07/16/2008 

Business Logic: This procedure is used to save void credit card transacition detail
 
Parameter : 
			@responsereasoncode : Save Response Reason Code
 
*******/


ALTER  procedure [dbo].[USP_HTS_UPDATE_CREDITCARD_INFO_FOR_VOID]    --for credit card payment being void        
--@ticketid int,                
@invoicenumber varchar(10),                
@status varchar(2),                
@errmsg varchar(255),                
@transnum varchar(50),                
@response_subcode  varchar(20),                    
@response_reason_code   varchar(20),                 
@response_reason_text varchar(100),                 
@auth_code   varchar(6),                           
@avs_code varchar(1),              
@employeeid int,
@responsecode int=0  -- Noufil 3961 New parameter Response_Code added 
          
        
as                
declare @approvedflag int,         
@ticketid int,                 
@chargeamount money     ,           
@paymenttype int,              
@paymentmethod as varchar(50)                      
                 
set @approvedflag = 0                
if @status = '0'   --successfull transaction              
begin                
 set @approvedflag = 1                
end                
update tblticketscybercash set        --If new info is received from the site        
 status = @status,                
 transnum = @transnum,                
 errmsg = @errmsg,                
 approvedflag = @approvedflag,                
 response_subcode  = @response_subcode   ,                 
 response_reason_code  = @response_reason_code ,                   
 response_reason_text  = @response_reason_text  ,                
 auth_code       = @auth_code        ,                 
 avs_code = @avs_code,
 Response_Code=  @responsecode                
 where invnumber= @invoicenumber                  
--updating record in tblticketspayment                
if @approvedflag = 1                 
 begin        
 --Getting info based on invoice num            
 select @ticketid = ticketid,@chargeamount=chargeamount,@paymenttype=paymenttype          
 from tblticketspayment              
 where InvoiceNumber_PK = @invoicenumber              
               
 if isnumeric(@ticketid) = 1               
 begin             
 --Setting transaction as void             
  update tblticketspayment              
   set paymentvoid = 1,voidinitial = @employeeid              
  where InvoiceNumber_PK = @invoicenumber                
                 
             
 --Inserting entry into tbl notes              
  select @paymentmethod = Description from tblpaymenttype where Paymenttype_PK = @paymenttype               
               
  insert into tblticketsnotes(ticketid,subject,employeeid,Notes)              
  select @ticketid,'Void $'+ convert(varchar(12),@chargeamount) + ' - '  + @paymentmethod,@employeeid,null from tblusers U              
  where employeeid = @employeeid              
             
 --Cheking if charge amount in null              
  select  @chargeamount = sum(chargeamount)              
  from tblticketspayment where  ticketid =@ticketid              
  and paymenttype not in (99,100)              
  and paymentvoid = 0              
    
--checking if schedule payment already exist        
select scheduleid from tblschedulepayment    
where ticketid_pk= @ticketid and scheduleflag<>0              
    
  
  
--Code Added by Azwar for scheduling--------------------  
declare @owes as money   
declare @ScheduleDate as datetime        
      set @ScheduleDate=getdate()+15        
              
      SELECT  @owes= ISNULL(SUM(DISTINCT t.TotalFeeCharged) - ISNULL(SUM(tp.ChargeAmount), 0), 0) - ISNULL        
       ((SELECT     SUM(Amount) FROM         dbo.tblSchedulePayment AS sp        
            WHERE     (TicketID_PK = t.TicketID_PK) AND (ScheduleFlag = 1)), 0)         
      FROM         dbo.tblTickets AS t LEFT OUTER JOIN        
        dbo.tblTicketsPayment AS tp ON t.TicketID_PK = tp.TicketID AND tp.PaymentVoid <> 1 AND tp.PaymentType <> 99        
      GROUP BY t.TicketID_PK        
      HAVING      (t.TicketID_PK = @ticketid)        
              
              
        
      if  @owes > 0        
       begin             
        --print @owes        
        exec USP_HTS_Insert_Edit_SchedulePayment @Ticketid,@owes,@ScheduleDate,@EmployeeID,0            
       end     
---------------------------------------------------  
  
  
--if ( @chargeamount is null  and @@rowcount =0  )  comment by azwar  
if ( @chargeamount is null )    
  begin              
   update tbltickets set activeflag = 0 where ticketid_pk = @ticketid              
  end                
 end              
         
 end        
        
    

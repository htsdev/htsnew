SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Mailer_Send_MCOA_Data]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Mailer_Send_MCOA_Data]
GO


/*
 exec usp_Mailer_Send_MCOA_Data 1, 34, 1, '06/25/2007,', 3991,  0 ,0  ,0
*/

CREATE    procedure [dbo].[usp_Mailer_Send_MCOA_Data]
@catnum int=1,                                    
@LetterType int=9,                                    
@crtid int=1, 
@startListdate varchar (500) ,                                    
@empid int=2006,                              
@printtype int =0 ,
@searchtype int = 0 ,
@isprinted bit                            
                                    
                                    
as
declare @officernum varchar(50),                                    
@officeropr varchar(50),                                    
@tikcetnumberopr varchar(50),                                    
@ticket_no varchar(50),                                                                    
@zipcode varchar(50),                                             
@zipcodeopr varchar(50),                                    
                                    
@fineamount money,                                            
@fineamountOpr varchar(50) ,                                            
@fineamountRelop varchar(50),                   
              
@singleviolation money,                                            
@singlevoilationOpr varchar(50) ,                                            
@singleviolationrelop varchar(50),                                    
                                    
@doubleviolation money,                                            
@doublevoilationOpr varchar(50) ,                                            
@doubleviolationrelop varchar(50),                                  
@count_Letters int,                  
@violadesc varchar(500),                  
@violaop varchar(50)                                     
                                    
declare @sqlquery varchar(8000),
	@sqlquery2 varchar(8000)
                                            
Select @officernum=officernum,                                    
@officeropr=oficernumOpr,                                    
@tikcetnumberopr=tikcetnumberopr,                                    
@ticket_no=ticket_no,                                                                      
@zipcode=zipcode,                                    
@zipcodeopr=zipcodeLikeopr,                                    
@singleviolation =singleviolation,                                    
@singlevoilationOpr =singlevoilationOpr,                                    
@singleviolationrelop =singleviolationrelop,                                    
@doubleviolation = doubleviolation,                                    
@doublevoilationOpr=doubleviolationOpr,                                            
@doubleviolationrelop=doubleviolationrelop,                  
@violadesc=violationdescription,                  
@violaop=violationdescriptionOpr ,              
@fineamount=fineamount ,                                            
@fineamountOpr=fineamountOpr ,                                            
@fineamountRelop=fineamountRelop              
                                 
from tblmailer_letters_to_sendfilters                                    
where courtcategorynum=@catnum                                    
and Lettertype=@LetterType                                  

Select @sqlquery =''  , @sqlquery2 = ''
  
declare @user varchar(10)
select @user = upper(abbreviation) from tblusers where employeeid = @empid
                                  
set @sqlquery=@sqlquery+'                                    
Select	distinct ta.recordid, 
	convert(varchar(10),ta.mcoa_updatedate,101) as listdate,
	tva.courtdate,
	ta.courtid, 
	flag1 = isnull(ta.Flag1,''N'') ,
	ta.officerNumber_Fk,                                          
	tva.TicketNumber_PK,
	ta.donotmailflag,
	ta.clientflag,
	upper(firstname) as firstname,
	upper(lastname) as lastname,
	upper(address1) + '''' + isnull(ta.address2,'''') as address,                      
	upper(ta.city) as city,
	S.state as state,
	--''TX'' as state,
	dbo.tblCourts.CourtName,
	zipcode,
	midnumber,
	dp2 as dptwo,
	dpc,                
	violationdate,
	violationstatusid,
	left(zipcode,5) + rtrim(ltrim(midnumber)) as zipmid,
	ViolationDescription, 
	case when isnull(tva.fineamount,0)=0 then 100 else tva.fineamount end as FineAmount,                                    
	count(tva.ViolationNumber_PK) as ViolCount,
	sum(isnull(tva.fineamount,0))as Total_Fineamount 
into #temp                                          
FROM    dbo.tblTicketsViolationsArchive tva 
INNER JOIN
	dbo.tblTicketsArchive ta 
ON 	tva.RecordID = ta.RecordID 
INNER JOIN
	dbo.tblCourts 
ON 	ta.CourtID = dbo.tblCourts.Courtid 
INNER JOIN
	dbo.tblCourtViolationStatus tcvs 
ON 	tva.violationstatusid = tcvs.CourtViolationStatusID 
INNER JOIN
tblstate S ON
ta.stateid_fk = S.stateID


where  tcvs.CategoryID=2   
and 	Flag1 in (''Y'',''D'',''S'')
and 	donotmailflag = 0 
and 	ta.clientflag=0
and isnull(ta.ncoa48flag,0) = 0
'               
          
if(@crtid=1 )              
	set @sqlquery=@sqlquery+' 
and 	ta.courtid In (Select courtid from tblcourts where courtcategorynum =  '+ convert(varchar(10),@crtid) +' )'                  


if(@crtid <> 1  )              
	set @sqlquery =@sqlquery +'                                  
and 	ta.courtid =  '+ convert(varchar(10),@crtid)
                                  
                          
                     

set @sqlquery=@sqlquery+'
and   ' + dbo.Get_String_Concat_With_DatePart_ver2 ( ''+  @startlistdate + '', 'ta.mcoa_updatedate' ) 


                                    
if(@officernum<>'')                                    
begin                                    
set @sqlquery =@sqlquery +                                     
            '  
and 	ta.officerNumber_Fk '+@officeropr +'('+ @officernum+')'                                     
end                                    
              
              
              
                                    
if(@ticket_no<>'')                                    
set @sqlquery=@sqlquery+ '
and  	('+ dbo.Get_String_Concat_With_op(''+@ticket_no+'' , 'tva.TicketNumber_PK' ,+ ''+ @tikcetnumberopr)+')'                                          
                                  
                                    
if(@zipcode<>'')                                                          
set @sqlquery=@sqlquery+ ' 
and	('+  dbo.Get_String_Concat_With_op(''+@zipcode+'' , 'left(ta.zipcode,3)' ,+ ''+ @zipcodeopr) +')'             
if(@violadesc<>'')              
set @sqlquery=@sqlquery+' 
and	('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ @violaop)+')'           
                                    
set @sqlquery =@sqlquery+ ' 
and 	ta.recordid not in (
		select recordid from tblletternotes where lettertype =  '+ convert(varchar(10), @LetterType)  +' ) '                                          
                      

                                    
set @sqlquery =@sqlquery+'      
group by  
	ta.recordid, convert(varchar(10),ta.mcoa_updatedate,101), tva.courtdate, ta.courtid, ta.Flag1, ta.officerNumber_Fk,
	tva.TicketNumber_PK, ta.donotmailflag, ta.clientflag, violationstatusid, upper(firstname) as firstname, upper(lastname) as lastname, upper(address1) + '''' + isnull(ta.address2,'''') ,
	upper(address1), isnull(ta.address2,''''), upper(ta.city),  tblCourts.CourtName, zipcode, midnumber, dp2,
	dpc, violationstatusid,	violationdate, left(zipcode,5) + rtrim(ltrim(midnumber)), ViolationDescription, FineAmount,S.state '              

                            
if(@fineamount<>0 and @fineamountOpr<>'not'  )              
begin              
set @sqlquery =@sqlquery+ '     '+                                    
      '  having sum(tva.fineamount)'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                   
end                                             
set @sqlquery=@sqlquery+'

Select distinct recordid, violationstatusid,ViolCount,Total_Fineamount,  listdate as mdate, FirstName,LastName,address,                              
 city,state,FineAmount,violationdescription,CourtName,dpc,dptwo,Flag1,officerNumber_Fk,TicketNumber_PK,                                     
 courtid,zipmid,donotmailflag,clientflag, zipcode into #temp1  from #temp                                    
 '              
if(@singleviolation<>0 and @doubleviolation=0)              
begin              
set @sqlquery =@sqlquery+ 'Where  '+ @singlevoilationOpr+                                    
      '  (Total_FineAmount '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and ViolCount=1)              
         or violcount>3                                            
      '              
end              
              
                   
if(@doubleviolation<>0 and @singleviolation=0 )              
begin              
set @sqlquery=@sqlquery+                     
'Select distinct recordid, violationstatusid,ViolCount,Total_Fineamount, listdate as mdate,FirstName,LastName,address,                              
 city,state,FineAmount,violationdescription,CourtName,dpc,dptwo,Flag1,officerNumber_Fk,TicketNumber_PK,                                     
 courtid,zipmid,donotmailflag,clientflag, zipcode into #temp1  from #temp                                    
 '              
set @sqlquery =@sqlquery + 'where   '+ @doublevoilationOpr+              
       '   (Total_FineAmount '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ')and ViolCount=2)                                    
      or violcount>3  '                                  
end              
              
if(@doubleviolation<>0 and @singleviolation<>0)              
begin              
set @sqlquery =@sqlquery+ 'Where  '+ @singlevoilationOpr+                                    
      '  (Total_FineAmount '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and ViolCount=1)              
         or   '              
set @sqlquery =@sqlquery +  @doublevoilationOpr+                                     
       '   (Total_FineAmount '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ')and ViolCount=2)                                    
      or violcount>3  '                
              
end              
              
         
if( @printtype<>0)         
set @sqlquery2 =@sqlquery2 + '
	Declare @ListdateVal DateTime, @totalrecs int, @count int, @p_EachLetter money, @recordid varchar(50),
		@zipcode   varchar(12), @maxbatch int  ,@lCourtId int  ,@dptwo varchar(10),@dpc varchar(10)
	declare @tempBatchIDs table (batchid int)
	Select @totalrecs =count(*) from #temp1 where mdate = @ListdateVal                                 
	Select @count=Count(distinct recordid) from #temp1                                
	if @count>=500                                
		set @p_EachLetter=convert(money,0.3)                                
	if @count<500                                
		set @p_EachLetter=convert(money,0.39)                                
	Declare ListdateCur Cursor for                                  
	Select distinct mdate  from #temp1                                
	open ListdateCur                                   
	Fetch Next from ListdateCur into @ListdateVal                                                      
	while (@@Fetch_Status=0)                              
		begin                                
			Select @totalrecs =count(distinct recordid) from #temp1 where mdate = @ListdateVal                                 
			insert into tblBatchLetter(Empid, Lettertype,Listdate,CourtId,ParentID,LCount,PExpense) values                                  
			( '+ convert(varchar(10),@empid) +'  ,  '+ convert(varchar(10),@lettertype) +'  , @ListdateVal,  '+ convert(varchar(10),@catnum) +', 0, @totalrecs, @p_EachLetter)                                   
			Select @maxbatch=Max(BatchId) from tblBatchLetter
			insert into @tempBatchIDs select @maxbatch                                                           
				Declare RecordidCur Cursor for                               
				Select distinct recordid,zipcode, courtid, dptwo, dpc from #temp1 where datediff(day,  mdate , @ListdateVal ) = 0                              
				open RecordidCur                                                         
				Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc                                          
				while(@@Fetch_Status=0)                              
				begin                              
					insert into tblLetterNotes(BatchId_Fk,listdate,ZipCode,RecordID,PCost,LetterType, courtid, dp2, dpc)                              
					values (@maxbatch,@ListdateVal,@zipcode,@recordid,@p_EachLetter,'+ convert(varchar(10),@lettertype) +' ,@lCourtId, @dptwo, @dpc)                              
					Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc                          
				end                              
				close RecordidCur 
				deallocate RecordidCur                                                               
			Fetch Next from ListdateCur into @ListdateVal                              
		end                                            
	close ListdateCur  deallocate ListdateCur 
	 Select distinct convert(varchar(20),n.noteid) as letterid, t.recordid,  t.FirstName,                              
	 t.LastName,t.address,t.city, t.state,FineAmount,violationdescription,CourtName, t.dpc,                              
	 dptwo, TicketNumber_PK, t.zipcode, '''+@user+''' as Abb                                     
	 from #temp1 t inner join tblletternotes n
	 on t.recordid = n.recordid and t.courtid = n.courtid and n.lettertype = '+ convert(varchar(10),@lettertype) +' 
	 order by T.zipcode 

declare @lettercount int, @subject varchar(200), @body varchar(400) , @sql varchar(500)
select @lettercount = count(distinct recordid) from #temp1
select @subject = convert(varchar(20), @lettercount) + '' LMS HMC MCOA Letters Printed''
select @body = convert(varchar(20), @lettercount) + '' LETTERS ('' + CONVERt(VARCHAR(12), GETDATE()) + '') - ''+ upper( '''+@user+''')
exec usp_mailer_send_Email @subject, @body

declare @batchIDs_filname varchar(500)
set @batchIDs_filname = ''''
select @batchIDs_filname = @batchIDs_filname + convert(varchar,batchid) + '',''  from @tempBatchIDs
select isnull(@batchIDs_filname,'''') as batchid

'

else
	set @sqlquery2 = @sqlquery2 + '
	 Select distinct ''NOT PRINTABLE'' as letterId,  recordid,  FirstName,                              
	 LastName,address,city, state,FineAmount,violationdescription,CourtName, dpc,                              	 dptwo, TicketNumber_PK, zipcode, '''+@user+''' as Abb                                     
	 from #temp1 
	 order by zipcode 

 '

set @sqlquery2 = @sqlquery2 + '

drop table #temp 
drop table #temp1'

--print @sqlquery + @sqlquery2
exec (@sqlquery + @sqlquery2)


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


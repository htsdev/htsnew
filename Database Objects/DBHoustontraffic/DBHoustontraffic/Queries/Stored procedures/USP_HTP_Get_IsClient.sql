/****** 
Create by:  Tahir Ahmed
Date:		07/15/2008

Business Logic : The stored procedure is to get information about the 
				 status of the profile.

List of Parameters:	
	@TicketId : Identification of case to fetch general comments

List of Columns: 	
	Column0: Returns true if it is under CLIENTS, false if it is under QUOTE

******/

create procedure dbo.USP_HTP_Get_IsClient
	(
	@TicketId int
	)


as

select convert(bit, activeflag) from tbltickets where ticketid_pk = @TicketId

go

grant execute on dbo.USP_HTP_Get_IsClient to dbr_webuser
go
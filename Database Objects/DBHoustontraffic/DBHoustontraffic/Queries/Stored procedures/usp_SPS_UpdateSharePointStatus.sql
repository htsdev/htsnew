 create PROCEDURE [dbo].[usp_SPS_UpdateSharePointStatus]
(
	@TicketsViolationID int,
	@SharepointStatusId int
)
AS 
update tblTicketsViolations
set SharepointStatusId = @SharepointStatusId
where TicketsViolationID = @TicketsViolationID

GO
GRANT EXEC ON usp_SPS_UpdateSharePointStatus TO dbr_webuser
GO


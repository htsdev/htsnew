set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

--usp_hts_AddServiceTicket 77590,3991,0,2,2,0,0,'Sabir',3991,0,0
  
ALTER procedure [dbo].[usp_hts_AddServiceTicket]  
                      
@TicketID as int,                      
@EmpID as int,                                     
@per_completion as int,                  
@Category int,                
@Priority int,      
@ShowTrialDocket int,                 
@ShowServiceInstruction int ,    
@ServiceInstruction varchar(2000),               
@AssignedTo int ,    
@ShowGeneralComments bit ,
-- Zahoor 4470 09/12/08
@ContinuanceOption int=0 ,
@ServiceTicketfollowup DATETIME = NULL                  
as                      
                      
                      
begin 
--Sabir Khan 4910 10/07/2008
-----------------------------
 declare @empnm as varchar(12)                                                    
 select @empnm = abbreviation from tblusers where employeeid = @EmpID
 IF LEN(@ServiceInstruction) > 0
 BEGIN
	set @ServiceInstruction = @ServiceInstruction +' ' + '(' +dbo.formatdateandtimeintoshortdateandtime(getdate()) + ' - ' +  @empnm + ')' + ' ' 
 END
 --Fahad 6054 08/19/2009 When Assignto is zero then fetch info from table
  IF(@AssignedTo=0)
 BEGIN
 	SET @AssignedTo= (SELECT TOP 1 ISNULL (tstc.AssignedTo,'') FROM tblServiceTicketCategories tstc WHERE tstc.ID=@Category)
 END
-----------------------------
--Sabir Khan 5738 05/25/2009 Modifiy for Service ticket follow update...      
insert into tblticketsflag(ticketid_pk,flagid,empid,assignedto,ServiceTicketCategory,Priority,ShowTrailDocket,  
ContinuanceStatus,Lastupdatedate,ShowServiceInstruction,ShowGeneralComments,ServiceTicketInstruction, ContinuanceOption,ServiceTicketFollowupDate)  
  
values(@ticketid,23,@empid,@AssignedTo,@Category,@Priority,@ShowTrialDocket,  
@per_completion,getdate(),@ShowServiceInstruction,  @ShowGeneralComments,   @ServiceInstruction, @ContinuanceOption,@ServiceTicketfollowup)  
    
  
insert into tblticketsnotes(ticketid,subject,employeeid,Notes)   
values(@ticketid,'Service Ticket Added ',@empid,null)                                                           
  
--Sabir Khan 5738 06/08/2009 Add follow up date note in case history...
insert into tblticketsnotes(ticketid,subject,employeeid,Notes)   
values(@ticketid,'Service Ticket Follow Up date has been set to ' + dbo.fn_DateFormat(@ServiceTicketfollowup,27,'/',':',1)  ,@empid,null)
  
-- declare @empnm as varchar(12)                                                  
-- select @empnm = abbreviation from tblusers where employeeid = @EmpID                                            
                                 

  
  
End    




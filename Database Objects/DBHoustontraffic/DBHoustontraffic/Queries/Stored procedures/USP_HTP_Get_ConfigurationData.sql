﻿ALTER PROCEDURE dbo.USP_HTP_Get_ConfigurationData-- 1
	@DivisionID INT = 0,
	@ModuleID INT = 0,
	@SubModuleID INT = 0,
	@KeyID INT = 0
AS
	SELECT Configuration.dbo.Division.DivisionName,
	       Configuration.dbo.Module.ModuleName,
	       Configuration.dbo.SubModule.SubModuleName,
	       Configuration.dbo.[Key].KeyName,
	       Configuration.dbo.VALUE.Value,
	       Configuration.dbo.VALUE.[Description],
	       Configuration.dbo.Division.DivisionID,
	       Configuration.dbo.Module.ModuleID,
	       Configuration.dbo.SubModule.SubModuleID,
	       Configuration.dbo.[Key].KeyID
	FROM   Configuration.dbo.Division
	       INNER JOIN Configuration.dbo.VALUE
	            ON  Configuration.dbo.Division.DivisionID = Configuration.dbo.VALUE.DivisionID
	       INNER JOIN Configuration.dbo.[Key]
	            ON  Configuration.dbo.VALUE.KeyID = Configuration.dbo.[Key].KeyID
	       INNER JOIN Configuration.dbo.Module
	            ON  Configuration.dbo.VALUE.ModuleID = Configuration.dbo.Module.ModuleID
	       INNER JOIN Configuration.dbo.SubModule
	            ON  Configuration.dbo.VALUE.SubModuleID = Configuration.dbo.SubModule.SubModuleID
	WHERE  (
	           (@DivisionID = 0)
	           AND (@ModuleID = 0)
	           AND (@SubModuleID = 0)
	           AND (@KeyID = 0)
	           AND 1 != 1
	       )
	       OR  (
	               (
	                   (@DivisionID != 0)
	                   OR (@ModuleID != 0)
	                   OR (@SubModuleID != 0)
	                   OR (@KeyID != 0)
	               )
	               AND (
	                       (@DivisionID = 0)
	                       OR (
	                              (@DivisionID <> 0)
	                              AND (Configuration.dbo.Division.DivisionID = @DivisionID)
	                          )
	                   )
	               AND (
	                       (@ModuleID = 0)
	                       OR (
	                              (@ModuleID <> 0)
	                              AND (Configuration.dbo.Module.ModuleID = @ModuleID)
	                          )
	                   )
	               AND (
	                       (@SubModuleID = 0)
	                       OR (
	                              (@SubModuleID <> 0)
	                              AND (Configuration.dbo.SubModule.SubModuleID = @SubModuleID)
	                          )
	                   )
	               AND (
	                       (@KeyID = 0)
	                       OR ((@KeyID <> 0) AND (Configuration.dbo.[Key].KeyID = @KeyID))
	                   )
	           )
	ORDER BY
	       Division.DivisionName
	       GO
	       GRANT EXECUTE ON   dbo.USP_HTP_Get_ConfigurationData TO dbr_webuser
	       GO
/****** 
Created by:		Rab Nawaz Khan
Task Id:		8997
Business Logic:	This procedure is used by HTP application to get status to display the court room numbers check Box for all out side traffic courts and 
				display the Allow Court Room numbers check box in application if the particular traffic court setting allow to display the court room numbers				
List of Parameters:
	@courtId: select the court on the court ID base
				
********/
ALTER PROCEDURE usp_HTP_GetCourtRoomNumberAllowanceStatus
(
	@courtId AS INT	
)
AS
IF EXISTS (SELECT courtid 
           FROM tblCourts 
           WHERE courtid = @courtId 
           -- Must not be inside court of houston
           AND courtcategorynum <> 1 
           -- only for traffice courts 
           AND CaseTypeID = 1 
		   -- Must be active court
		   AND InActiveFlag = 0
			)
BEGIN
	SELECT ISNULL(tc.AllowCourtNumbers, 0) AS AllowCourtNumbers, CONVERT(BIT,1) AS ShowCourtRoomNumbers FROM tblCourts tc
	WHERE tc.Courtid = @courtId
END
ELSE
	BEGIN
		SELECT CONVERT(BIT, ISNULL(t.AllowCourtNumbers, 0)) AS AllowCourtNumbers,  CONVERT(BIT,0) AS ShowCourtRoomNumbers FROM tblCourts t
		WHERE t.Courtid = @courtId
	END

GO
GRANT EXECUTE ON [dbo].[usp_HTP_GetCourtRoomNumberAllowanceStatus] TO dbr_webuser
GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Mailer_Send_Arraignment2_3]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Mailer_Send_Arraignment2_3]
GO








-- usp_Mailer_Send_Arraignment2_3 '01/05/07,', 1, 0, 3992, 10 

CREATE Proc [dbo].[usp_Mailer_Send_Arraignment2_3]
@sendListdate varchar(500)='01/04/2006',
@crtid int =1,
@printtype int=0,
@empid int=2006,
@LetterType int =11 
 
as 
                
declare @sqlquery varchar(8000) 
                
Set @LetterType=@LetterType-1 
set @sqlquery=''
set @sqlquery=@sqlquery+'
SELECT   
distinct tln.recordid,
firstname,
lastname,
tln.listdate,
upper (Address1+''''+isnull(ta.Address2,'''')) Address,
ta.City,
''TX'' STATE,
ta.Zipcode,
ta.ticketnumber,
midnumber,
ta.dp2 as dptwo,
ta.dpc,
violationdate,
left(ta.zipcode,5) + rtrim(ltrim(midnumber)) as zipmid,
violationdescription,                
fineamount,                
violationstatusid,                
dbo.tblCourts.CourtName                    
into #temp                
from 	tblletternotes tln, 
	tblticketsarchive ta,
	tblticketsviolationsarchive tva,
	dbo.tblCourtViolationStatus tcvs,        
	dbo.tblDateType tdt,                                                
	tblbatchletter tbl,
	tblcourts                   
where 	tln.recordid =ta.recordid                
and 	tva.ticketnumber_pk=ta.ticketnumber                
and   	ta.CourtID = dbo.tblCourts.Courtid                
and 	tbl.batchid=tln.batchid_fk    
and 	datediff(day,ta.courtdate ,getdate())>3'     

set @sqlquery=@sqlquery+ ' 
and	tln.LetterType='+convert(varchar(10),@LetterType)+'                
and 	clientflag=0  '               

if(@crtid=1 or @crtid=2 or @crtid=3 )            
begin             
set @sqlquery=@sqlquery+' 
and	tbl.courtid In (Select courtid from tblcourts where courtcategorynum ='+convert(varchar(10),@crtid ) +')'                
end            

if(not (@crtid<>1 or @crtid<>2 or @crtid<>3) )              
begin              
set @sqlquery =@sqlquery +'                                  
         and tbl.courtid =                                
         '+convert(varchar(10),@crtid )                                  
end                 

Set @LetterType=@LetterType+1                                

set @sqlquery=@sqlquery+'  
and 	('+dbo.Get_String_Concat_With_DatePart(''+@sendListdate +'','convert (varchar(10),ta.listdate,101)','In')+')'                

set @sqlquery =@sqlquery +'
and 	tcvs.categoryid =2 and isnull(ta.donotmailflag,0)=0 
and 	flag1 in (''Y'',''D'',''S'')  
and     tln.recordid in(    
		Select recordid from tblletternotes where Not Lettertype ='+ Convert( Varchar (10),@LetterType)     
set @sqlquery=@sqlquery+'  and ('+dbo.Get_String_Concat_With_DatePart(''+@sendListdate +'','convert (varchar(10),tblletternotes.listdate,101)','In')+')
		)'                

set @sqlquery=@sqlquery++' order by tln.listdate desc'                
                
    
set @sqlquery=@sqlquery+'                

if('+convert(varchar(5),@printtype)+'<>0)                         
begin                    
	Declare @ListdateVal DateTime,                        
	@totalrecs int,                      
	@count int,            
	@p_EachLetter money,                    
	@recordid varchar(50),                    
	@zipcode   varchar(12),                    
	@maxbatch int                    

	Select @totalrecs =count(*) from #temp where listdate=@ListdateVal                                  
	Select @count=Count(*) from #temp                

	if @count>=500                       
		set @p_EachLetter=convert(money,0.3)                      
	                      
	if @count<500                      
		set @p_EachLetter=convert(money,0.39)                      
                    
	Declare ListdateCur Cursor for                        
	Select distinct Listdate from #temp                      
	open ListdateCur                         
	Fetch Next from ListdateCur into @ListdateVal                                

	while (@@Fetch_Status=0)                    
		begin                      
			Select @totalrecs =count(distinct recordid) from #temp where listdate=@ListdateVal                       
			insert into tblBatchLetter(Empid, Lettertype,Listdate,CourtId,ParentID,LCount,PExpense) values                        
			(2006,'+Convert(varchar(5),@LetterType)+',@ListdateVal,3001,0,@totalrecs,@p_EachLetter)                         

			Select @maxbatch=Max(BatchId) from tblBatchLetter                               

			Declare RecordidCur Cursor  for                     
			Select distinct recordid,zipcode from #temp where Listdate=@ListdateVal                     
			open RecordidCur                
			Fetch Next from  RecordidCur into @recordid,@zipcode                                   

			while(@@Fetch_Status=0)                    
				begin                    
					insert into tblLetterNotes(BatchId_Fk,ListDate,ZipCode,RecordID,PCost,LetterType)
					values (@maxbatch,@ListdateVal,@zipcode,@recordid,@p_EachLetter,' +  convert(varchar(5),@LetterType)+ ') 

					Fetch Next from  RecordidCur into @recordid,@zipcode                     
				end                    

			close RecordidCur                         
			deallocate RecordidCur                            
			Fetch Next from ListdateCur into @ListdateVal                                
		end                                    

close ListdateCur                         
deallocate ListdateCur   


Select distinct recordid,  
recordid as letterid,              
firstname,                
lastname,                
listdate,                
Address,                
City,                
STATE,                
Zipcode,                
ticketnumber,                
midnumber,                    
dptwo,                    
dpc,                 
courtname,                 
violationdate,                     
zipmid,                
violationdescription,                
fineamount,                
violationstatusid                 
from #temp                    
 order by listdate                             

                      
end    
else
       Select distinct recordid,  
''NOT PRINTABLE'' as letterid,              
firstname,                
lastname,                
listdate,                
Address,                
City,                
STATE,                
Zipcode,                
ticketnumber,                
midnumber,                    
dptwo,                    
dpc,                 
courtname,                 
violationdate,                     
zipmid,                
violationdescription,                
fineamount,                
violationstatusid                 
from #temp                    
 order by listdate           
                    
drop table #temp                         
'                        
                
--print @sqlquery                
exec (@sqlquery)                












GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Attorney_GetAllFirms]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Attorney_GetAllFirms]
GO


CREATE PROCEDURE  [dbo].[usp_Attorney_GetAllFirms]      
 AS      
      
Select  FirmID as id, FirmAbbreviation as name From tblFirm      
      
      
order by FirmAbbreviation      

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


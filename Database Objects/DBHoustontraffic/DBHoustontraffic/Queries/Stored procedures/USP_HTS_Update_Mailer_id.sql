/******************    
    
Created BY : Tahir Ahmed
    
BUSINESS LOGIC : This procedure is used by Houston Traffic Program to associate Mailer Id with the case.    
      
LIST OF INPUT PARAMETERS :    
    
  @TicketId : Case Identification  
  @EmpId    : ID of the user who is creating the profile 
  @DBId     : Database Identification 
    
List of Columns:  
	None
  
*******************/         

-- exec dbo.USP_HTS_Update_Mailer_id 167680, 3991, 1
ALTER procedure [dbo].[USP_HTS_Update_Mailer_id]
	(
		@Ticketid int,
		@EmpID int,
		@DBId int   -- 1 = TrafficTickets; 2 = Dallas; 5= Civil
	)
as

declare @mailerid int,
		@recdate datetime, 
		@DaysToCheck tinyint,
		@bUpdated int

-- SET DATE DIFFERENCE BUSINESS DAYS....
-- Rab Nawaz Khan 10032 02/07/2012 For HTP, the promo pricing will be applicable from the date of mailer sent instead of next day
IF @DBId = 1
	BEGIN
		select	@DaysToCheck = 0,
		@bUpdated = 0
	END
ELSE
	BEGIN
		select	@DaysToCheck = 1,
		@bUpdated = 0 		
	END
-- END 10032 
-- GETTING LETTER TYPES IN TEMP TABLE.....
declare @letter table(lettertype int)
if @dbid = 1 
	-- tahir 4868 09/26/2008 fixed the LID issue for Criminal cases..
	--insert into @letter select letterid_pk from tblletter where courtcategory not in (6,8,11,16,25)
	--Sabir Khan 6888 10/29/2009 Fortworth and Arlington court category id have been added...
	--Yasir Kamal 7218 01/13/2010 mailer structure changes 
	--insert into @letter select letterid_pk from tblletter where courtcategory not in (6,11,16,25,27,28)
	insert into @letter SELECT tl.letterid_pk from tblletter tl INNER JOIN tblCourtCategories tcc ON tl.courtcategory = tcc.CourtCategorynum where tcc.LocationId_FK <> 2
	-- end 4868
else if @dbid = 2
	--insert into @letter select letterid_pk from tblletter where courtcategory  in (6,11,16,25,27,28)
	  insert into @letter SELECT tl.letterid_pk from tblletter tl INNER JOIN tblCourtCategories tcc ON tl.courtcategory = tcc.CourtCategorynum where tcc.LocationId_FK = 2
else if @dbid = 5
	insert into @letter select letterid_pk from tblletter where courtcategory = 26
	  


-- IF MAILER ID DOES NOT EXISTS.....
if isnull(@Ticketid,0) <> 0 and  exists (
		select mailerid from tbltickets where ticketid_pk = @ticketid and isnull(mailerid,0)= 0
		)

begin
	
	-- GET ALL RECORD IDS & PROFILE CREATION DATE IN TEMP TABLE.....
	declare @temp table (recordid int, recdate datetime)
	insert into @temp 
	select distinct v.recordid, t.recdate
	from  tblticketsviolations v, tbltickets t
	where t.ticketid_pk = @ticketid
	and	 t.ticketid_pk = v.ticketid_pk

	select @recdate = recdate from @temp


	-- GET ALL MAILER IDS RELATED TO THE ABOVE RECORD IDS IN TEMP TABLE......
	-- EXCLUDING DALLS & JIMS LETTERS.....
	declare @mailer table (recordid int, noteid int, mailerdate datetime, MailerComparisionDate datetime)

	insert into @mailer 
	select convert(int,n.recordid), n.noteid, n.recordloaddate , dbo.fn_HTS_GET_Nth_NextBusinessDay(n.recordloaddate, @DaysToCheck)
	from tblletternotes n, @temp t
	where convert(int, n.recordid) = t.recordid 
	AND  n.lettertype IN (select lettertype from @letter)
	union
	select convert(int,d.recordid), n.noteid, n.recordloaddate, dbo.fn_HTS_GET_Nth_NextBusinessDay(n.recordloaddate, @DaysToCheck)
	from tblletternotes n, tblletternotes_detail d, @temp t
	where	n.noteid = d.noteid
	and		convert(int,t.recordid) = d.recordid 
	AND  n.lettertype IN (select lettertype from @letter)

	-- GET MAXIMUM OF MAILER ID FROM TEMP TABLE SENT TO CLIENT 
	-- TWO BUSINESS DAYS EARLIER THAN THE PROFILE CREATION DATE....
	set  @mailerid = (
		select top 1 noteid
		from  @mailer 
		where datediff(day, MailerComparisionDate, @recdate ) >= 0
		order by MailerComparisionDate desc
		)
	

	-- UPDATE MAILER ID IN TBLTICKETS.....
	if isnull(@mailerid,0) <> 0
		begin
			update tbltickets 
			set mailerid= @mailerid,
				employeeidupdate = @empid
			where ticketid_pk = @ticketid
	
			set @bUpdated = 1
		end
	else
		set @bUpdated = 0


end

-- tahir 6031 06/25/2009 returning result for data cleaning job.
select isnull(@Ticketid,0) as TicketId,  isnull(@bUpdated,0) as IsUpdated
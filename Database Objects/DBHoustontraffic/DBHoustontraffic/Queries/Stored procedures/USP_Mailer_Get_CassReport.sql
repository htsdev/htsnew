SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_Mailer_Get_CassReport]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_Mailer_Get_CassReport]
GO








--USP_Mailer_Get_ZipLetterCount '7/15/2006','7/22/2006','3049,3041,3048,','17,19,18,'
CREATE procedure [dbo].[USP_Mailer_Get_CassReport]

@sdate  datetime,
@edate  datetime,
@court  varchar(2000),
--@empid int,
@letterid  varchar(2000)

as

select @edate=@edate+'23:59:59.998'

declare @tblCourt table(courtid int)

insert into @tblcourt select * from dbo.sap_string_param(@court) 


declare @tblletter  table(letterid int)

insert into @tblletter select * from dbo.sap_string_param(@letterid) 


SELECT	ta.FirstName, 
	ta.LastName, 
	ta.Address1, 
	ta.City, 
	s.State, 
	ta.ZipCode, 
	n.RecordLoadDate, 
	ta.MidNumber
FROM    dbo.tblLetterNotes n 
INNER JOIN
	dbo.tblTicketsArchive ta 
ON 	n.RecordID = ta.RecordID 
AND 	n.CourtId = ta.CourtID 
INNER JOIN
	dbo.tblState s 
ON 	ta.StateID_FK = s.StateID

where	n.recordloaddate between @sDate and @edate
and 	n.courtid in (select courtid  from @tblcourt )
and 	n.lettertype in (select letterid  from @tblletter )














GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_hts_mcc_email]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_hts_mcc_email]
GO

create procedure usp_hts_mcc_email

@PaymentID	int

as

select   
  m.PaymentID,  
  m.fname +' '+lname as cardholder,  
  dbo.formatdateandtimeintoshortdateandtime(m.recdate) as recdate,  
  m.ccno,  
  m.expdate,  
  m.cin,  
  m.transNum,  
  m.amount,  
  m.description,   
  m.employeeid,  
  m.voidpayment,  
  m.refundamount,  
   --m.response_reason_text,   
   case m.approveflag when 1 then 'Approved' else 'Declined'  end as response_reason_text,  
  u.abbreviation,  
  case m.clienttype   
   when 1 then 'Traffic'  
   when 2 then 'Oscar Client'  
   when 3 then 'Lisa Client'  
   when 4 then 'Other'  
  end as clienttype  
 from tblmanualpayment m inner join tblusers u  
   on m.employeeid =u.employeeid 
where m.PaymentID=@PaymentID
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


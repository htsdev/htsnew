create PROCEDURE [dbo].[usp_SPS_GetUpdatedCriminalCases]
(
	@StartDate DATETIME =NULL,
	@EndDate DATETIME= NULL
)
AS 

SELECT tv.TicketsViolationID, tv.CourtID, t.TicketID_PK, t.Firstname, t.MiddleName, 
t.Lastname, t.Midnum, tv.RefCaseNumber, [CourtDateMain],
t.calculatedtotalfee, sharepointstatusid,
Paid =  (select isnull( SUM(ISNULL(p.ChargeAmount, 0)),0) from tblticketspayment p                                              
 where p.ticketid = t.ticketid_pk  and  p.paymentvoid = 0) 
 
FROM tblTickets t 
INNER JOIN tblTicketsViolations tv ON t.TicketID_PK = tv.TicketID_PK
WHERE tv.SharepointStatusId = 2 
And (tv.CourtID = 3037)
--AND tv.[CourtDateMain] BETWEEN @StartDate AND @EndDate
and t.ActiveFlag = 1
--and t.TicketID_PK = 184764
--and tv.TicketsViolationID = 905621 

GO
GRANT EXEC ON usp_SPS_GetUpdatedCriminalCases TO dbr_webuser
GO


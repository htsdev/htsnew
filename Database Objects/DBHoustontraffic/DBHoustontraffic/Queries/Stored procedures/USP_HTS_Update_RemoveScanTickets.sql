SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Update_RemoveScanTickets]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Update_RemoveScanTickets]
GO


CREATE procedure USP_HTS_Update_RemoveScanTickets
	(
	@RecIDs	varchar(200)
	)


as 

update	tblscanneddocketcourtstatus
set	updateflag = 3
where	recid in ( select * from dbo.Sap_String_Param(@RecIDs) )

if @@error = 0
	select 1
else
	select 0




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


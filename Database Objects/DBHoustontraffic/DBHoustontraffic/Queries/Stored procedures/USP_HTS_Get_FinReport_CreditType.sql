/*
Altered By     : Fahad Muhammad Qureshi.
Created Date   : 07/27/2009  
TasK		   : 6054          
Business Logic : This procedure display the all Credit Types in Our system on the report

List of Input Parameters:
	@sDate:	Start Date.
	@eDate: End Date.
	@FirmId: Firm ID

*/   
-- USP_HTS_Get_FinReport_CreditType '01/01/2006', '04/10/2006'
-- Last Modified By : Zeeshan Ahmed
-- Last Modified Date : 1/18/2008
-- Modify Procedure For Bounce Check Payment Type
ALTER procedure [dbo].[USP_HTS_Get_FinReport_CreditType]    
 (    
 @sDate datetime,    
 @eDate datetime,  
 @firmId int = NULL,
 @BranchID INT=1 -- Sabir Khan 10920 05/27/2013 Branch Id added.    
 )    
as     
    
select @eDate = @eDate + '23:59:59.999'    
    
   
if @firmId is null   
 SELECT pt.Description as CategoryType,     
  count(p.TicketID) as TotalCount,     
  isnull(sum(isnull(p.ChargeAmount,0)),0) as TotalAmount    
 FROM    dbo.tblPaymenttype pt     
 INNER JOIN    
   dbo.tblTicketsPayment p     
 ON  pt.Paymenttype_PK = p.PaymentType    
 and p.recdate between @sDate and @eDate    
 and p.paymenttype in (8,9,11,103,104)  --Fahad 6054 07/30/2009 104-Partner Firm Credit      
 and  p.paymentvoid = 0  
 AND p.Branchid IN (SELECT branchid FROM fn_get_branchIDs(@branchID)) -- Sabir Khan 10920 05/27/2013 Branch Id check added.     
 group by    
  pt.description    
else  
 SELECT pt.Description as CategoryType,     
  count(p.TicketID) as TotalCount,     
  isnull(sum(isnull(p.ChargeAmount,0)),0) as TotalAmount    
 FROM    dbo.tblPaymenttype pt     
 INNER JOIN    
   dbo.tblTicketsPayment p     
 ON  pt.Paymenttype_PK = p.PaymentType    
 and p.recdate between @sDate and @eDate    
 and p.paymenttype in (8,9,11,103,104) --Fahad 6054 07/30/2009 104-Partner Firm Credit   
 and  p.paymentvoid = 0  and (p.ticketId in (select ticketId_pk from tblTicketsviolations where coveringFirmId = @firmId))  
 AND p.Branchid IN (SELECT branchid FROM fn_get_branchIDs(@branchID)) -- Sabir Khan 10920 05/27/2013 Branch Id check added.
 group by    
  pt.description    




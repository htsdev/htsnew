if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_HTS_OutsideCourtLoader_UpdateWarrantInfo]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_HTS_OutsideCourtLoader_UpdateWarrantInfo]
GO


CREATE PROCEDURE [dbo].[usp_HTS_OutsideCourtLoader_UpdateWarrantInfo]
(
@TicketID int,  
@TicketNumber varchar(20),    
@EmpID int,
@CourtStatus int
)
AS    

UPDATE tblticketsviolations  
 set CourtviolationstatusID = @CourtStatus, 
  VEmployeeID = @EmpID  
 where  
  TicketID_PK = @TicketID and  
  casenumassignedbycourt = @TicketNumber    
  
GO
GRANT EXEC ON [dbo].[usp_HTS_OutsideCourtLoader_UpdateWarrantInfo] TO [dbr_webuser]
GO

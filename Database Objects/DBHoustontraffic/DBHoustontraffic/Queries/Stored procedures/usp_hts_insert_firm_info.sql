/*
* Waqas 5864 07/20/2009 
* Business Logic :  This method is used to insert firms through outside firm interface
	
Input Parameters:
@firmname  
@shortname  
@Address1     
@Address2     
@city    
@state  
@zip       
@basefee    
@daybeforefee       
@judgefee     
@firmid    
@phone   
@CanCoverFrom        
@CanCoverBy 
@sendtextmessage 
@textmessagenumber
@SenderEmail
@noticeperioddays 
@AttorneyFName 
@AttorneyLName 
@AttorneyBarCardNumber 
@AttorneyEmailAddress 
@isShortnamefound
@AttorneySignatureImage

*/

--sp_helptext usp_hts_insert_firm_info    
    
-- usp_hts_insert_firm_info 'test','GZA','test','test','test',1,'34543',34.0,34.0,34.0,0,'3453433454',0,0,0  
  
alter  procedure [dbo].[usp_hts_insert_firm_info]        
@firmname varchar(50),        
@shortname varchar(6),        
@Address1 varchar(30),        
@Address2 varchar(20),        
@city varchar(30),        
@state int,        
@zip varchar(20),        
@basefee money,        
@daybeforefee money,        
@judgefee money,        
@firmid int,    
@phone varchar(20),  
@CanCoverFrom bit,        
@CanCoverBy bit,  
-- tahir 11/06/2008 added send text message info...
@sendtextmessage bit, 
@textmessagenumber varchar(100) , 
--Sabir Khan 5298 12/03/2008 parameter for sender address...
@SenderEmail varchar(100),
@noticeperioddays smallint,

--Waqas 5864 06/30/2009 ALR Changes
@AttorneyFName VARCHAR(MAX), 
@AttorneyLName VARCHAR(MAX), 
@AttorneyBarCardNumber VARCHAR(MAX), 
@AttorneyEmailAddress VARCHAR(MAX),
--Yasir Kamal 7150 01/01/2010 insert attorney signature image.
@AttorneySignatureImage image ,
@hasImage BIT,

@isShortnamefound int =0 OUTPUT
      
as        
      
if @firmid = 0  
      
begin   
--Sabir Khan 4990 10/18/2008 Check for existing short name...       
if not exists(select FirmAbbreviation from tblFirm where FirmAbbreviation like @shortname)  
BEGIN  
 declare @newFirmId int      
      
 select @newFirmId = max(isnull(firmid,0)) + 1 from tblfirm      
 
 --Waqas 5864 06/30/2009 ALR Changes     
 insert into tblfirm (firmid, FirmName,FirmAbbreviation,Address,Address2,        
 City,State,Zip,basefee,daybeforefee,judgefee,phone,CanCoverFrom,CanCoverBy,
 sendtextmessage, textmessagenumber, noticeperioddays , SenderEmail, 
 AttorenyFirstName, AttorneyLastName, AttorneyBarCardNumber, AttorneyEmailAddress,AttorneySignatureImage)        
 values ( @newfirmid, @FirmName,@shortname,@Address1,@Address2,        
 @City,@State,@Zip,@basefee,@daybeforefee,@judgefee,@phone,@CanCoverFrom,@CanCoverBy,
 -- tahir 11/06/2008 added send text message info...
 --Sabir Khan 5298 12/15/2008 @SenderEmail has been added for sender address...
 @sendtextmessage, @textmessagenumber, @noticeperioddays , @SenderEmail, 
 --Yasir Kamal 7150 01/01/2010 insert attorney signature image.
 @AttorneyFName, @AttorneyLName, @AttorneyBarCardNumber, @AttorneyEmailAddress,@AttorneySignatureImage)  
   
set @isShortnamefound = 0      
END  
else  
BEGIN  
set @isShortnamefound = 1  
END  
end        
else        
begin   
--Sabir Khan 4990 10/18/2008 Check for existing short name...  
if not exists(select FirmAbbreviation from tblFirm where FirmAbbreviation like @shortname and firmid != @firmid)  
BEGIN  
-----       
update tblfirm        
 set FirmName = @FirmName,        
 FirmAbbreviation = @shortname,        
 Address  = @address1,        
 Address2 = @Address2,        
 City = @city,        
 state = @state,        
 zip = @zip,        
 basefee = @basefee,        
 daybeforefee = @daybeforefee,        
 judgefee = @judgefee ,    
phone = @phone ,  
CanCoverFrom= @CanCoverFrom ,        
CanCoverBy = @CanCoverBy ,
-- tahir 11/06/2008 added send text message info...
sendtextmessage = @sendtextmessage, 
textmessagenumber = @textmessagenumber, 
--Sabir Khan 5298 12/15/2008 SenderEmail has been updated...
SenderEmail = @SenderEmail,  
noticeperioddays = @noticeperioddays ,
--waqas 5864 06/30/2009
AttorenyFirstName = @AttorneyFName , 
AttorneyLastName = @AttorneyLName, 
AttorneyBarCardNumber = @AttorneyBarCardNumber, 
AttorneyEmailAddress = @AttorneyEmailAddress   

 where firmid = @firmid 
 set @isShortnamefound = 0   

--Yasir Kamal 7150 01/01/2010 update attorney signature image.
IF (@hasImage = 1)
BEGIN
UPDATE tblfirm SET 
    AttorneySignatureImage = @AttorneySignatureImage    
    where firmid = @firmid 
END
    
   
----  
END  
ELSE  
BEGIN  
set @isShortnamefound = 1  
END  
end  
  
        
  
      
      
  
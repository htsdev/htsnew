SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_bug_getusers]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_bug_getusers]
GO



create procedure [dbo].[usp_bug_getusers]

as

select Userid,
	   UserName
from tbl_bug_users

order by UserName asc	   

  	
       
       
     





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


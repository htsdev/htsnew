set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

/*  
* Created By		: SAEED AHMED
* Task ID			: 8101  
* Created Date		: 09/27/2010  
* Business logic	: This procedure is used get attribute value.
*					: 
* Parameter List	
  @Report_Id		: Report Id
  @CourtID			: Court Id
  @AttributeID		: Attribute Id
* Example			: [dbo].[USP_ReportSetting_GetAttributeValue]52,3049,1
* 
*/
CREATE PROCEDURE  [dbo].[USP_ReportSetting_GetAttributeValue]
	@Report_Id    INT,
	@CourtID      INT,
	@AttributeID  INT
AS

BEGIN
DECLARE @reportBusinessDays INT    
SET @reportBusinessDays=0
    SELECT @reportBusinessDays=ReportSetting.AttributeValue
    FROM   ReportAttribute
           INNER JOIN ReportSetting
                ON  ReportAttribute.AttributeID = ReportSetting.AttributeId
           INNER JOIN tblCourts
                ON  ReportSetting.CourtID = tblCourts.Courtid
           INNER JOIN tbl_Report
                ON  ReportSetting.ReportID = tbl_Report.Report_ID
    WHERE  tbl_Report.Report_ID = @Report_Id
           AND ReportAttribute.AttributeID = @AttributeID
           AND ReportSetting.CourtID = @CourtID
           
SELECT @reportBusinessDays AS AttributeValue           
    
END
GO
GRANT EXECUTE ON  [dbo].[USP_ReportSetting_GetAttributeValue] TO dbr_webuser
GO


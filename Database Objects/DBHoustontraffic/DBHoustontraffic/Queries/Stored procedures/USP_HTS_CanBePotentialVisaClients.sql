/*    
Created By: Haris Ahmed    
Date:   08/29/2012    
    
Business Logic:    
 The stored procedure is used to check as it can be Potential Visa Clients
    
List of Input Parameters:    
 @TicketId: Case identification for the client. 
*/

ALTER PROCEDURE [dbo].[USP_HTS_CanBePotentialVisaClients] --9486
	@TicketID INT
AS
--Farrukh 11180 07/23/2013 Added "IsImmigration" field
SELECT tt.TicketID_PK, tt.LanguageSpeak, ISNULL(tt.isPR, 2) AS isPR, CAST(DATEDIFF(DAY, tt.DOB, GETDATE())/365.00 AS DECIMAL(18,2)) AS Age, thic.Comments, thic.IsImmigration
  FROM tblTickets tt
INNER JOIN tblTicketsViolations ttv ON ttv.TicketID_PK = tt.TicketID_PK 
INNER JOIN tblViolations tv ON tv.ViolationNumber_PK=ttv.ViolationNumber_PK 
LEFT OUTER JOIN tbl_HTS_ImmigrationComments thic ON thic.TicketID = tt.TicketID_PK 
WHERE tt.TicketID_PK = @TicketID AND tv.Description LIKE '%license%' 
AND CAST(DATEDIFF(DAY, tt.DOB, GETDATE())/365.00 AS DECIMAL(18,2)) BETWEEN 16 AND 30 AND ISNULL(tt.isPR, 2) <> 1

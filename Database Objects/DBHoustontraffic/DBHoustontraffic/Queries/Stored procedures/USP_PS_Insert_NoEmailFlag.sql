set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

--USP_HTS_Insert_NoEmailFlag 0

ALTER procedure [dbo].[USP_PS_Insert_NoEmailFlag]  
  
@ticketid int 
  
AS  
  
Declare @email varchar(100)  
  
select @email =email from tbltickets where ticketid_pk =@ticketid  
  
if((select count(email) from tblnoemailflag where email like @email) = 0)  
   
 begin  
  
  insert into tblnoemailflag (Ticketid_pk,Email,UnsuscribedDate)  
  values(    
    @ticketid,  
    @email,  
    getdate()  
    )  
 END  
 select @@rowcount


  
  
  




 
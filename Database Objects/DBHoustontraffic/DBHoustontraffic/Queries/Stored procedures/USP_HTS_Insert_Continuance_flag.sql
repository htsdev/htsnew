SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Insert_Continuance_flag]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Insert_Continuance_flag]
GO

          
CREATE procedure [dbo].[USP_HTS_Insert_Continuance_flag]    
@TicketID int,                
@FlagID int,        
@ContinuanceDate datetime,    
@ContinuanceStatus int,    
@EmpID as int    
as                
insert into tblticketsflag(ticketid_pk,flagid, ContinuanceDate, ContinuanceStatus,recdate ) values(@ticketid,@flagid,@ContinuanceDate,@ContinuanceStatus,getdate())    
declare @desc varchar(20)                
select @desc=description from tbleventflags where flagid_pk=@flagid              
          
        
           
insert into tblticketsnotes(ticketid,subject,employeeid,Notes) values(@ticketid,'Flag -'+ @desc,@empid,null)               
              
            
--Added By Zeeshan Ahmed            
--Used For Continuance Flag            
if @FlagID = 9            
Begin            
Update tbltickets set ContinuanceFlag = 1 where ticketid_pk =@ticketid             
End          
         
     
          
----------------------------------------------------------------------------------------          
----------------------------------------------------------------------------------------     
    
  

  
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


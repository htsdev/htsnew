USE TrafficTickets
GO
/****** Object:  StoredProcedure [dbo].[usp_htp_update_BadEmailRecords]    Script Date: 12/10/2012 22:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*    
* Created By: Babar Ahmad    
* Task: 9676    
* Date 11/1/2011    
*     
* Business Logic: This stored procedure marks record as "Reviewed Email Address" and deletes its Bad Email flag.    
*     
* Input Parameters:    
*   @ticketid: Ticket ID of record    
*   @empID:    Employee ID    
*/    

--[usp_htp_update_BadEmailRecords] 441112, 4177, "Pending", "fahad.qureshi@lntechnologies.com", 1
ALTER PROCEDURE [dbo].[usp_htp_update_BadEmailRecords]    
@ticketid INT = 0,    
@empID INT,
@CallBackStatus VARCHAR(50),    
@email VARCHAR(200), -- Abbas Qamar 9676 12/01/2011 Added for the E-mail Address
@isContactedPage AS INT,
--Muhammad Nasir 10071 12/19/2012 Added cooments parameter
@comments varchar(2000)

AS    
BEGIN    

 --Muhammad Nasir 10071 11/26/2012 get employee name----------------------------------------------
 DECLARE @empName VARCHAR(100)    
 SELECT @empName = Lastname FROM tblUsers WHERE EmployeeID = @empID

 --Muhammad Nasir 10071 11/26/2012 get reviewed email status id
 DECLARE @ReviewedEmailStatusID int
 SELECT @ReviewedEmailStatusID = isnull(Reminderid_PK,0) from tblReminderStatus
 WHERE rtrim(ltrim([Description])) = rtrim(ltrim(@CallBackStatus))
 
 --Muhammad Nasir 10071 12/20/2012 Keep old status to check
 DECLARE @OLDReviewedEmailStatusID int
 SELECT @OLDReviewedEmailStatusID =  ISNULL(ReviewedEmailStatusID,-1) from tblTickets t where t.ticketID_PK = @ticketid

 --Muhammad Nasir 10071 11/26/2012 update reviewed email status
 UPDATE tblTickets    
 SET    
 ReviewedEmailStatusID = @ReviewedEmailStatusID 
 ,Email = case when email <> @email then @email else email end
 WHERE
 TicketID_PK = @ticketid    

 -------------------------------------------------------------------------------------------------
 select @ReviewedEmailStatusID,@isContactedPage, @CallBackStatus

 IF ((@isContactedPage = 1 OR  @isContactedPage = 0) AND ltrim(rtrim(@CallBackStatus)) = 'Contacted') -- when Call Back Status is contacted    
 BEGIN    
	  -- Babar Ahmad 9676 11/18/2011 Set reminder status as contacted.    
      update tblTickets
      SET ReviewedEmailStatus = 1
      WHERE 
      TicketID_PK = @ticketid   

	  -- Babar Ahmad 9676 11/18/2011 Delete Bad Email flag    
	  DELETE FROM tblTicketsFlag    
	  WHERE FlagID = 34 AND TicketID_PK = @ticketid

	  -- Babar Ahmad 9676 11/18/2011 Insert note in history.    
	  INSERT INTO tblTicketsNotes    
	  (TicketID,[Subject],    
	  Notes,Recdate,EmployeeID,EventID,NoteType,Fk_CommentID,IsComplaint,IsReviewed)    
	  VALUES    
	  (@ticketid,'Email Address reviewed by ' + @empName,    
	  NULL,GETDATE(),@empID,NULL,0,NULL,0,1)

	  --Muhammad Nasir 10071 12/24/2012 Add notes of bad email flag removal
	  INSERT INTO tblTicketsNotes    
	  (TicketID,[Subject],    
	  Notes,Recdate,EmployeeID,EventID,NoteType,Fk_CommentID,IsComplaint,IsReviewed)    
	  VALUES    
	  (@ticketid,'Flag - Bad Email - Removed - ' + @empName,    
	  NULL,GETDATE(),@empID,NULL,0,NULL,0,1) 
    
 END    

--Muhammad Nasir 10071 11/26/2012 
 ELSE IF (@CallBackStatus = 'Left Message' OR @CallBackStatus = 'No Answer' OR @CallBackStatus = 'No Response' OR @CallBackStatus = 'Wrong Telephone Number')
 BEGIN
	  --Muhammad Nasir 10071 12/21/2012 mark reviewed email status
      update tblTickets
      SET ReviewedEmailStatus = 0
      WHERE 
      TicketID_PK = @ticketid 

	  --Muhammad Nasir 10071 12/20/2012 check old value before update
	  if(@OLDReviewedEmailStatusID <> @ReviewedEmailStatusID)
	  BEGIN	
		  --Muhammad Nasir 10071 12/21/2012 check old value before update
		  update tblTicketsFlag
		  SET deleteFlag = 1
		  WHERE 
		  TicketID_PK = @ticketid AND flagID = 34

		  --Muhammad Nasir 10071 12/21/2012 Insert history notes
		  INSERT INTO tblTicketsNotes    
		  (TicketID,[Subject],    
		   Notes,Recdate,EmployeeID,EventID,NoteType,Fk_CommentID,IsComplaint,IsReviewed)    
		  VALUES    
		  (@ticketid,'Bad email address attempted for review - Call back status - ' + @CallBackStatus + ' by ' + @empName,    
		  NULL,GETDATE(),@empID,NULL,0,NULL,0,0)
	  END
 END

--Muhammad Nasir 10071 11/26/2012 
 --ELSE IF (@CallBackStatus = 'Pending' and @comments <> '')
 ELSE
 BEGIN
	  --Muhammad Nasir 10071 12/21/2012 mark reviewed email status
      update tblTickets
      SET ReviewedEmailStatus = 0
      WHERE 
      TicketID_PK = @ticketid

	  --Muhammad Nasir 10071 12/20/2012 check old value before update
	  if(@OLDReviewedEmailStatusID <> @ReviewedEmailStatusID)
	  BEGIN	      
	  INSERT INTO tblTicketsNotes    
	  (TicketID,[Subject],    
	   Notes,Recdate,EmployeeID,EventID,NoteType,Fk_CommentID,IsComplaint,IsReviewed)    
	  VALUES    
	  (@ticketid,'Email address Call back status changes to ' + @CallBackStatus,    
	   NULL,GETDATE(),@empID,NULL,0,NULL,0,0)
	  END 
 END

END



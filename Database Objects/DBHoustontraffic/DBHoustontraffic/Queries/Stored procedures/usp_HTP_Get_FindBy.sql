USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[usp_HTP_Get_FindBy]    Script Date: 01/02/2014 02:19:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** 
Create by:  Sabir Khan
Task ID: 11509
Business Logic : This procedure is used to get find by description

******/
ALTER Procedure [dbo].[usp_HTP_Get_FindBy]

as

SELECT ID,FindBy FROM FindBy WHERE ID IN (1,2,3,4,5,6,7,8,9,10) 


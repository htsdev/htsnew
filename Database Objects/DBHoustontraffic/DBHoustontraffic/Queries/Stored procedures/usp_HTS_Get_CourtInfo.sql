USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[usp_HTS_Get_CourtInfo]    Script Date: 09/29/2010 07:37:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 /******   

Business Logic : This procedure is used to get the court information.  
List of parameter:
@courtid: ID of the court.
  
******/     
--usp_HTS_Get_CourtInfo 3001    
--kamran 3536 04/15/08 date formate change   
 
ALTER procedure [dbo].[usp_HTS_Get_CourtInfo] --0 
@courtid int=0                  
as                  
if @courtid=0              
begin              
select T1.courtid,      
-- Added By Zeeshan For Court Setting Page      
case when len(t1.courtname) > 25 then  substring(t1.courtname,0,25) + '...' else t1.courtname end as ccourtname  ,      
t1.courtname,T1.address,T1.address2,T1.city,T1.state,T1.visitcharges,T1.courttype,T1.settingrequesttype,T1.judgename,T1.zip,            
 T1.courtcontact,T1.phone,T1.fax,      
-- Added By Zeeshan For Court Setting Page      
case when len(t1.shortcourtname) > 20 then  substring(t1.shortcourtname,0,25) + '...' else t1.shortcourtname end as cshortcourtname,      
t1.shortcourtname,  
t1.CaseTypeId,
ct.casetypename as casetypename,-- Noufil 4237 06/28/2008 Get case type Name
T1.bondtype,dbo.fn_DateFormat(RepDate,4,'/',':',1) RepDate,-- Noufil  4945 10/15/2008 rep complete date added
 (case                                           
   when T1.inactiveflag  = 0  then 'No'
   when T1.inactiveflag  = 1  then 'Yes'
  end ) as inactiveflag,
  
(case          
 when T1.IsCriminalCourt = 0 then 'No'
 when T1.IsCriminalCourt = 1 then 'Yes'
end) as IsCriminalCourt,

T1.shortname,             
 (case                                           
   when T2.Description  = '--Choose--' then ''                                          
   when T2.Description  <> '--Choose--' then T2.Description            
  end ) as bondtypeDes,T3.Description AS setreqdes,RepDate as rdate,
  --Yasir Kamal 6109 07/21/2009 missing column issue fixed.
  IsletterofRep , IsLORSubpoena,IsLORMOD,
  -- Sabir Khan 5941 07/24/2009 Get Countyid and countyname...            
  ISNULL(T1.CountyId,0) AS CountyId,
  ISNULL(tmc.CountyName,'N/A') AS CountyName,
  --Asad Ali 8153 09/20/2010 Comment code b/c currently this task is in requirement phase
  --Nasir 7369 02/19/2010 added AllowOnlineSignUp
  -- T1.AllowOnlineSignUp,
  --Asad Ali 8153 09/20/2010 get Associated Court ID
  isnull(T1.AssociatedALRCourtID,0) AssociatedALRCourtID,
  --Asad Ali 8153 09/20/2010 add ALRProcess column in select 
  ISNULL(ALRProcess,0) ALRProcess,LORRepDate2,LORRepDate3,LORRepDate4,LORRepDate5,LORRepDate6,  --Muhammad Muneer 8227 09/27/2010 selecting the newly added LOR Dates
  ISNULL(T1.AllowCourtNumbers, 0) AS AllowCourtNumbers -- Rab Nawaz Khan 8997 08/25/2011 added the logic of Allow Court Room Numbers for the outside courts
FROM         tblCourts T1 
	INNER JOIN tblCourtSettingRequest T3 ON T1.SettingRequestType = T3.SettingType 
	INNER JOIN tblBondType T2 ON T1.BondType = T2.BondID
	Inner join casetype ct on t1.CaseTypeId=ct.CaseTypeId
	LEFT OUTER JOIN tbl_Mailer_County tmc ON t1.CountyId = tmc.CountyID -- Sabir Khan 5941 07/24/2009 join for getting county name...
	

where courtid <> 0 order by courtname    
    
end              
else              
begin           --Nasir 5310 12/29/2008	Add column in select ALRProcess   
select courtid,courtname,address,address2,city,state,visitcharges,courttype,settingrequesttype,judgename,zip,            
 courtcontact,phone,fax,shortcourtname,bondtype,inactiveflag, shortname,IsCriminalCourt,dbo.fn_DateFormat(RepDate,4,'/',':',1) as RepDate,CausenoDuplicate,    
 CaseIdentifiaction,AcceptAppeals,AppealLocation,InitialSetting,LOR,AppearnceHire,JudgeTrialHire,JuryTrialHire,
 --Sabir Khan 5763 04/16/2009 Get LOR subpoena and LOR Motion of discovery flag...    
 HandwritingAllowed,AdditionalFTAIssued,GracePeriod, ISNULL(ALRProcess,0) ALRProcess,Continuance,SupportingDocument,Comments,CaseTypeId,RepDate as rdate,IsletterofRep , IsLORSubpoena,IsLORMOD,  -- Noufil 4237 06/24/2008 Casetype field added to see the nature of court i-e civil,criminal or traffic  
 t1.CountyId,ISNULL(tmc.CountyName,'N/A') AS CountyName --Sabir Khan 5941 07/24/2009 Get CountyID and countyname...
 --Asad Ali 8153 09/20/2010 Comment code b/c currently this task is in requirement phase
 --Nasir 7369 02/19/2010 added AllowOnlineSignUp
  --,T1.AllowOnlineSignUp
   --Asad Ali 8153 09/20/2010 get Associated Court ID
   --Muhammad Muneer 8227 09/27/2010 selecting the newly added LOR Dates
  ,isnull(T1.AssociatedALRCourtID,0) AssociatedALRCourtID,t1.LORRepDate2 as LORRepDate2,t1.LORRepDate3 as LORRepDate3,t1.LORRepDate4 as LORRepDate4,t1.LORRepDate5 as LORRepDate5,t1.LORRepDate6 as LORRepDate6,
  ISNULL(T1.AllowCourtNumbers, 0) AS AllowCourtNumbers -- Rab Nawaz Khan 8997 08/25/2011 added the logic of Allow Court Room Numbers for the outside courts
from tblcourts t1
LEFT OUTER JOIN tbl_Mailer_County tmc ON t1.CountyId = tmc.CountyID -- Sabir Khan 5941 07/24/2009 join for getting county name...                  
WHERE     (Courtid = @courtid)    
order by courtname    
            
end              
    
    
    


﻿/************************************************************
 * Code formatted by SoftTree SQL Assistant © v4.0.34
 * Time: 4/20/2009 11:35:19 AM
 ************************************************************/

    
/*    
Create By: Nasir 4771 04/16/2009  
      
Business Logic:      
 The stored procedure is used to unlink potential contact id from non client table.    
     
      
List of Input Parameters:      
 @RecordID: record id of non client    
                                
 */                           
create PROCEDURE dbo.USP_HTP_Update_UnlinkContact
	@RecordID INT,
	@CaseTypeID INT
AS
IF @CaseTypeID IN (1,2)
BEGIN
	UPDATE tblTicketsArchive
	SET    PotentialContactID_FK = NULL
	WHERE  RecordID = @RecordID 
END
ELSE IF @CaseTypeID in (3,4)
BEGIN
	UPDATE CIVIL.dbo.ChildCustodyParties
	SET PotentialContactID_FK = NULL
	WHERE ChildCustodyPartyId = @RecordID
END
	
	
GO

GRANT EXECUTE ON dbo.USP_HTP_Update_UnlinkContact TO dbr_webuser

GO
    
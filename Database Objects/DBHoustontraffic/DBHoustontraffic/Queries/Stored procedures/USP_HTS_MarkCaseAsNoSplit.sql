SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_MarkCaseAsNoSplit]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_MarkCaseAsNoSplit]
GO


create procedure dbo.USP_HTS_MarkCaseAsNoSplit
	(
	@TicketId int
	)


as 

if not exists (
		select * from tblticketsflag
		where ticketid_pk =  @TicketId
		and flagid = 12
	)
begin
	insert into tblticketsflag(ticketid_pk, flagid, recdate)
	select @TicketId, 12, getdate()
end




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


﻿
 -- ====================================================================================================================================================================================    
 -- Author:  Afaq Ahmed    
 -- Create date: 04/29/2010    
 -- Bussiness Logic: Get  all client cases with verified status  of 'Disposed' but an Auto status of 'Jury, Judge, PreTrial and Arraignment' with future court date    
 -- TasK ID  : 7752  
 -- ====================================================================================================================================================================================    
CREATE PROCEDURE [dbo].[USP_HTS_GET_disposedAlert]    
 AS    
 SELECT tt.TicketID_PK ,    
 ttv.casenumassignedbycourt AS 'CauseNumber',    
 tt.Firstname,    
 tt.Lastname,     
 ttv.RefCaseNumber,    
 tc.ShortName,    
 (SELECT tcvs.[Description]    
  FROM tblCourtViolationStatus tcvs     
  WHERE tcvs.CourtViolationStatusID=ttv.CourtViolationStatusIDmain) AS 'VerifiedStatus',    
 dbo.fn_DateFormat(ttv.CourtDateMain, 25, '/', ':', 1) AS 'VerifiedCourtDate',    
 (SELECT tcvs.[Description]    
  FROM tblCourtViolationStatus tcvs     
  WHERE tcvs.CourtViolationStatusID=ttv.ViolationStatusID) AS 'AutoStatus',
 dbo.fn_DateFormat(ttv.CourtDate, 25, '/', ':', 1) AS 'AutoCourtDate',    
 CONVERT(VARCHAR(10),tt.DisposeAlertFollowUpDate,101) AS 'FollowUpDate'    
 FROM tblTicketsViolations ttv    
 INNER JOIN tblTickets tt    
 ON tt.TicketID_PK = ttv.TicketID_PK    
 INNER JOIN tblCourts tc    
 ON ttv.CourtID= tc.Courtid    
 WHERE ttv.CourtViolationStatusIDmain = 80    
 AND ttv.ViolationStatusID IN (3,101,103,26)   -- 3 = Arraignment, 101 = PreTrial, 103 = JUDGE & 26 = Jury
 AND ttv.CourtDate>GETDATE()   
 ORDER BY tt.DisposeAlertFollowUpDate

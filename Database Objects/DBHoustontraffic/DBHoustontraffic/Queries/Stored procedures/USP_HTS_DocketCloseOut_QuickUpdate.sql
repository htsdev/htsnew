SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_DocketCloseOut_QuickUpdate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_DocketCloseOut_QuickUpdate]
GO

--SELECT TOP 1 * FROM tblticketsviolations order by courtdate desc        
        
CREATE PROCEDURE [dbo].[USP_HTS_DocketCloseOut_QuickUpdate]        
@TicketsViolationID int,        
@CourtDate Datetime,        
@CourtLoc int,        
@EmpID int,    
@Status int,  
@UpdateAll bit,  
@TicketID int,  
@Date datetime        
        
as        
  
if @UpdateAll = 1  
begin  
  
UPDATE tblTicketsViolations        
  set        
   --Update Court Date        
   CourtDate  = @CourtDate,        
   CourtDateMain = @CourtDate,        
   CourtDateScan = @CourtDate,        
        
   --Update Court Location        
   CourtNumber = @CourtLoc,        
   CourtNumbermain = @CourtLoc,        
   CourtNumberscan = @CourtLoc,        
        
   --Update Court Status        
   CourtViolationStatusID = @Status,        
   CourtViolationStatusIDMain = @Status,        
   CourtViolationStatusIDScan = @Status,        
    
 --EmployeeID    
 VEmployeeID = @EmpID        
    
   
WHERE   
  
TicketsViolationID IN (select TicketsViolationID from tbl_hts_docketcloseout_snapshot where datediff(day,courtdate,@Date)=0 and TicketID_PK = @TicketID)          
     
  
end  
        
  
else  
begin  
UPDATE tblTicketsViolations        
  set        
   --Update Court Date        
   CourtDate  = @CourtDate,        
   CourtDateMain = @CourtDate,        
   CourtDateScan = @CourtDate,        
        
   --Update Court Location        
   CourtNumber = @CourtLoc,        
   CourtNumbermain = @CourtLoc,        
   CourtNumberscan = @CourtLoc,        
        
   --Update Court Status        
   CourtViolationStatusID = @Status,        
   CourtViolationStatusIDMain = @Status,        
   CourtViolationStatusIDScan = @Status,        
    
 --EmployeeID    
 VEmployeeID = @EmpID        
    
  WHERE        
           
   TicketsViolationID = @TicketsViolationID         
end

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


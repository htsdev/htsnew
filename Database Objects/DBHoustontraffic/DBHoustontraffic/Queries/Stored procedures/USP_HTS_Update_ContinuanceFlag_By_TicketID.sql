SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Update_ContinuanceFlag_By_TicketID]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Update_ContinuanceFlag_By_TicketID]
GO

--sp_helptext USP_HTS_Insert_flag_by_ticketnumber_and_flagID 122456


Create Procedure [dbo].[USP_HTS_Update_ContinuanceFlag_By_TicketID] --124456
(
@TicketID int
)
as

Update tblticketsflag set isPaid = 1 
where ticketid_pk = @TicketID
and flagid = 9
--and isnull(isPaid,0) = 0 




----------------------------------------------------------------------------------------------      

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


  /*  
* Modified By		: SAEED AHMED
* Task ID			: 7791  
* Created Date		: 07/22/2010  
* Business logic	: Returns list of active users.
*
*/
Create Procedure  dbo.USP_HTP_Get_ActiveUserList
as
select employeeid, username from tblusers WHERE [Status]=1

GO
GRANT EXECUTE ON dbo.USP_HTP_Get_ActiveUserList TO dbr_webuser
GO

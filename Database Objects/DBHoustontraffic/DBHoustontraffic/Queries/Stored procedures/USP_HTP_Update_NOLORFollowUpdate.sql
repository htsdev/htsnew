﻿  
  
/****** Object:  StoredProcedure [dbo].[USP_HTP_Update_NOLORFollowUpdate]  ******/     
/*        
Created By     : Waqas Javed
Created Date : 03/21/2009  
TasK   : 5653        
Business Logic  : This procedure Updates LOR Follow Up Date.      
           
Parameter:       
   @TicketID  : Updating criteria with respect to TicketId      
   @FollowUpDate : Date of follow Up which has to be set     
     
      
*/      
  
CREATE PROCEDURE [dbo].[USP_HTP_Update_NOLORFollowUpdate]  
 @FollowUpDate DATETIME,  
 @TicketID INT  
AS  
 UPDATE tblTickets  
 SET    LORFollowUpDate = @FollowUpDate  
 WHERE  TicketID_PK = @TicketID  


go

grant execute on [dbo].[USP_HTP_Update_NOLORFollowUpdate] to dbr_webuser
go

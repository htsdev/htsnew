/*
* Created By: Babar Ahmad
* Task : 9676
* Date: 10/31/2011
* 
* Business logic: This procedure checks whether a particular record has bad email flag or not.
* 
* Input Parameters:
*			@ticketid = Ticket ID of record
*/


-- [dbo].[usp_htp_CheckBadEmail] 5164 

CREATE PROCEDURE [dbo].[usp_htp_CheckBadEmail]
	@ticketid INT
AS
BEGIN
IF EXISTS 
	(
		SELECT t.TicketID_PK FROM tbltickets t INNER JOIN tblTicketsFlag tf ON tf.TicketID_PK = t.TicketID_PK
		-- Abbas Qamar 9676 12/02/2011 checking Bad Email flag against the clients
		WHERE tf.FlagID = 34 AND t.TicketID_PK = @ticketid AND t.Activeflag = 1
		
	)
	BEGIN
	print 'Record Exists' 
	SELECT 1
	END
ELSE
	BEGIN
	print 'Record does not exist' 
	SELECT 0
	END
END
	

GO
GRANT EXECUTE ON [dbo].[usp_htp_CheckBadEmail] TO dbr_webuser
GO 
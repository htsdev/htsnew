SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Insert_PricingTemplateDetail]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Insert_PricingTemplateDetail]
GO










--Used to Insert values of templates in tblpricingtemplatedetail
CREATE procedure USP_HTS_Insert_PricingTemplateDetail
	(
	@TemplateID					int,
	@CategoryId					int,
	@BasePrice					money=0,
	@SecondaryPrice				money=0,
	@BasePercentage				numeric(10,4)=0,
	@SecondaryPercentage		 		numeric(10,4)=0,
	@BondBase					money=0,
	@BondSecondary				money=0,
	@BondBasePercentage			numeric(10,4)=0,
	@BondSecondaryPercentage 	numeric(10,4)=0,
	@BondAssumption				money=0,
	@FineAssumption				money=0
	)
as
insert into tblpricingtemplatedetail(
							TemplateID,
							CategoryId,
							baseprice,
							secondaryprice,
							basepercentage,
							secondarypercentage,
							bondbase,
							bondsecondary,
							bondbasepercentage,
							bondsecondarypercentage,
							bondassumption,
							fineassumption
							) 

values						(
							@TemplateID,
							@CategoryId,
							@BasePrice,
							@SecondaryPrice,
							@BasePercentage,
							@SecondaryPercentage,
							@BondBase,
							@BondSecondary,
							@BondBasePercentage,
							@BondSecondaryPercentage,
							@BondAssumption,
							@FineAssumption
							)





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_hts_get_mailer_id]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_hts_get_mailer_id]
GO

create procedure usp_hts_get_mailer_id  
@TicketID int  
as  
select isnull(mailerid,0) as MailerID from tbltickets where TicketID_PK = @TicketID

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


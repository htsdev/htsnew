﻿ /******   
Create by:  Noufil Khan  
Business Logic : this procedure is used to check for any HCC (Haris County Civil) court in the given case excluding disposed and no hire violations.   
  
List of Parameters:   
 @Ticketid : Ticket Id of the case  
  
List of Columns:    
 isfound : if found then 1 else 0  
  
******/  

-- [dbo].[usp_HTP_Check_CivilCourt] 196637

Create Procedure [dbo].[usp_HTP_Check_CivilCourt]
  
@Ticketid int  
  
as  
  
declare @count int
  
select @count=count(*) 
	from tblticketsviolations  
		where ticketid_pk=@Ticketid  
				and courtid=3077
				and courtviolationstatusidmain not in (80,236)
  
if @count>0  
	select 1 as isfound  
else  
	select 0 as isfound  

go
	grant exec on [dbo].[usp_HTP_Check_CivilCourt] to dbr_webuser
go
/*
* Created By :- Noufil 5884 07/14/2009
* Business Logic : This procedure returns Cause Number /Ticket number with respect to ticket violation id.
*/

CREATE PROCEDURE [DBO].[USP_HTP_GET_TICKETNUMBER_BY_TICKETVIOLATIONID]
	@ticketviolationid INT
AS
	SELECT CASE RTRIM(LTRIM(ISNULL(ttv.casenumassignedbycourt, '')))
	            WHEN '' THEN ttv.RefCaseNumber
	            ELSE ttv.casenumassignedbycourt
	       END
	FROM   tblTicketsViolations ttv
	WHERE  ttv.TicketsViolationID = @ticketviolationid
GO

  GRANT EXECUTE ON [DBO].[USP_HTP_GET_TICKETNUMBER_BY_TICKETVIOLATIONID] TO dbr_webuser
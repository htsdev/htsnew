﻿        
      
/****** Object:  StoredProcedure [dbo].[USP_HTP_Update_HMCFTAFollowUpdate]  ******/         
/*            
Created By     : Nasir    
Created Date : 05/07/2009      
TasK   : 5697            
Business Logic  : This procedure Updates Contact against ticket.          
               
Parameter:           
   @TicketID  : Updating criteria with respect to TicketId          
                      
*/          
      
CREATE PROCEDURE [dbo].[USP_HTP_Update_ContactDate]      
  @TicketID INT
AS      
 UPDATE tblTickets      
 SET ContactDate = GETDATE() 
 WHERE  TicketID_PK = @TicketID      
    
GO
 
 GRANT EXECUTE ON dbo.USP_HTP_Update_ContactDate TO dbr_webuser
 
 Go   
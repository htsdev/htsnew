SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_HTS_update_ReminderCall_status]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_HTS_update_ReminderCall_status]
GO


CREATE  procedure usp_HTS_update_ReminderCall_status    
 @comments varchar(500),          
 @status int,    
 @ticketid int  ,              
 @employeeid int          
as          
        
--declare @strabbr varchar(5),          
-- @strcomments varchar(550)          
        
--SELECT @strabbr = Abbreviation FROM TBLUSERS where employeeid = @employeeid          

if (right(@comments,1) <> ')' )    --if comments are inserted        
  begin             
    declare @empnm as varchar(12)            
    select @empnm = abbreviation from tblusers where employeeid = @employeeid         

    if len(@comments) > 0
    set @comments = @comments +' ' + '(' +dbo.formatdateandtimeintoshortdateandtime(getdate()) + ' - ' +  @empnm + ')' + ' '            
  end     

update tblTicketsViolations     
-- set ReminderComments = dbo.FormatComments(@comments,@employeeid) ,          
 set ReminderComments = @comments ,          
  ReminderCallStatus = @status,          
  ReminderCallEmpID = @employeeid          
 where ticketid_pk=@ticketid    

        
if @status = 1          
begin              
  insert into tblticketsnotes(ticketid,subject,employeeid,Notes) values(@ticketid,'Trial Reminders',@employeeid,'comments-' + @comments )               
end          
          

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


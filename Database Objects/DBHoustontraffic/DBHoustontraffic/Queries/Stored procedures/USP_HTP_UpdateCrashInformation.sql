USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTP_UpdateCrashInformation]    Script Date: 08/27/2013 00:59:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******    
* Created By :		Kamran Shahid
* Create Date :		11/05/2010   
* Task ID :			10447  
* Business Logic :  This procedure is used to Add Texas Transportation Person Operators 
* List of Parameter : @IncidentNumber  
*					: @OperatorFirstName   
					: @OperatorLastName  
    
* Column Return :       
******/
-- USP_HTP_AddDoctorDetail   
ALTER PROC [dbo].[USP_HTP_UpdateCrashInformation] 
@IncidentNumber   VARCHAR(11), 
@CrashDate      DATETIME, 
@Location       VARCHAR(200), 
@CrashID        VARCHAR(50), 
@MaxRetrycount  INT = 3, 
@IsCIDProcessed BIT
AS
IF (@IsCIDProcessed = 'False')
BEGIN
    DECLARE @RetryCount AS INT
    
    SET @RetryCount = 0
    
    SELECT @RetryCount = ISNULL(CIDRetryCount, 0)
    FROM   Crash.dbo.tbl_Crash 
    WHERE  Crash_ID = @CrashID
           AND Case_ID = @IncidentNumber
    
    IF (@RetryCount < @MaxRetrycount)
    BEGIN
        UPDATE Crash.dbo.tbl_Crash
        SET    CIDRetryCount = @RetryCount + 1,
               UpdateDate = GETDATE(),
               CIDProcessLastDate = GETDATE()
        WHERE  Crash_ID = @CrashID
               AND Case_ID = @IncidentNumber
    END
    ELSE
    BEGIN
        UPDATE Crash.dbo.tbl_Crash
        SET    IsCIDProcessed = 'TRUE',
               CIDRetryCount = @RetryCount + 1,
               UpdateDate = GETDATE(),
               CIDProcessLastDate = GETDATE()
        WHERE  Crash_ID = @CrashID
               AND Case_ID = @IncidentNumber
    END
END
ELSE
BEGIN
    IF (@Location IS NOT NULL OR @CrashDate IS NOT NULL)
    BEGIN
        UPDATE Crash.dbo.tbl_Person
        SET    CrashDate = @CrashDate,
               Location = @Location,
               UpdateDate = GETDATE()
        WHERE  Crash_ID = @CrashID
               
    END
    
    UPDATE Crash.dbo.tbl_Crash
    SET    IsCIDProcessed = @IsCIDProcessed,
           UpdateDate = GETDATE(),
           CIDRetryCount = ISNULL(CIDRetryCount, 0) + 1,
           CIDProcessLastDate = GETDATE()
    WHERE  Crash_ID = @CrashID
           AND Case_ID = @IncidentNumber
END 



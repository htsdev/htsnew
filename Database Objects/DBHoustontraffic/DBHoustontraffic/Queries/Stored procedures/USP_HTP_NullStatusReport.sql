SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*************************************
Created By : Yasir Kamal
Task Id : 6030 
Dated : 06/23/2009 

Business Logic   : This Stored Procedure is used by HTP/Validation/Null Status Report. It is used to Get
				   all records whose violation status is null,zero or whose violation is not present in 
				   tblcourtviolationstatus.

Input Parameters : N/A

Output Columns   : 

Ticketid_pk      : Ticketid associated with case.
Lastname	     : Last name of a person.
DOB			     : Date of birth of a person.
Refcasenumber    : Ticketnumber associated with case.


**************************************/



CREATE PROCEDURE [DBO].[USP_HTP_NullStatusReport]

AS 

SELECT tt.TicketID_PK,tt.lastname AS LastName,tt.Firstname AS FirstName,CONVERT(VARCHAR(10),tt.DOB,101) AS DOB,ttv.RefCaseNumber AS TicketNumber,ttv.casenumassignedbycourt AS CauseNumber        
  FROM tblTickets 
  tt INNER JOIN tblTicketsViolations ttv ON tt.TicketID_PK = ttv.TicketID_PK

WHERE tt.activeflag = 1 
AND 
	( ISNULL(ttv.CourtViolationStatusIDmain,0)= 0 
	OR ttv.CourtViolationStatusIDmain NOT IN ( SELECT CourtViolationStatusID FROM tblcourtviolationstatus )
	)

ORDER BY tt.TicketID_PK


GO 
GRANT EXECUTE ON USP_HTP_NullStatusReport TO webuser_hts 

GO
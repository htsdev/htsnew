SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_PS_Get_Court_Detail]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_PS_Get_Court_Detail]
GO





Create Procedure USP_PS_Get_Court_Detail
@courtid int 

as

declare @Description as varchar(2000)

Set @Description = null

Select @Description =  '<b>' +  CourtName + '</b>, ' + Address     from tblcourts
where courtid = @courtid


Select isnull(@Description,'N/A')



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


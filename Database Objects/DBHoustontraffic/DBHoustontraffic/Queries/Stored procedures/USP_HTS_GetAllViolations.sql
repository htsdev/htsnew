/***********

Alter By : Zeeshan Ahmed 
Business Login : This Procedure is used to get violations by court id and case type.

List of  Parameters :


	@courtid : Court Location
	@CaseType : Case Type 1 : Traffic ,2: Criminal , 3: Civil , 4: Family Law 

List Of Columns
	ID			: Violations ID 
	Description : Description
	
************/
Alter  PROCEDURE [dbo].[USP_HTS_GetAllViolations] --3037,0        
@CaseType int =1, 
@CourtID int = 0       
AS    

--FOR CRIMINAL CASE VIOLATIONS
if @CaseType =2 
BEGIN	--Nasir 5310 12/23/2008  Court is ALR ALR process on
--if @courtid IN(3047,3070,3079) 
		 if @courtid IN(SELECT Courtid FROM tblCourts WHERE ALRProcess = 1) 
			begin  
					SELECT    ViolationNumber_PK as ID, Description FROM  tblViolations 
					where  ViolationNumber_PK in (0,16159)order by Description asc      
			end  
		else  
			begin  
					--Zeeshan Ahmed 4847 09/24/2008 Display New Criminal Violations
					
					SELECT    ViolationNumber_PK as ID, Description FROM   tblViolations              
					where  violationtype=16    or   ViolationNumber_PK in (0,16227)          
					order by Description asc      
			end  
END
--FOR CIVIL and Family Law CASE VIOLATIONS  
--Yasir Kamal 6438 10/13/2009 for family law cases only violations related to Family law should be displayed.
else if @CaseType in (3,4)
BEGIN
	SELECT     ViolationNumber_PK as ID, Description       
	FROM       tblViolations        
	where	violationtype=14 OR ViolationNumber_PK=0  order by ID 

END
Else
Begin
	
		-- If Court Is Criminal Court Display Criminal Court Violations      
		If (select IsCriminalCourt from tblcourts where courtid = @courtid   ) = 1      
		Begin      
			if @courtid=3047  
		  begin  
		   SELECT    ViolationNumber_PK as ID, Description             
		   FROM         tblViolations              
		   where  ViolationNumber_PK in (0,16159)          
		   order by Description asc      
		  end  
		 else  
		  begin  
		   SELECT    ViolationNumber_PK as ID, Description             
		   FROM         tblViolations              
		   where  violationtype=9    --ViolationNumber_PK  NOT in (0,9662,9663,9665,9666,9667,9668,9669)             
		   or   ViolationNumber_PK in (0,16227)          
		   order by Description asc      
		  end  
		End      
		      
		else      
		Begin      
		      
		SELECT     ViolationNumber_PK as ID, Description  FROM         tblViolations              
		-- Zeeshan Haider 11081 05/21/2013 Added FCTS in violation drop down
		where  (violationtype=8 or   ViolationNumber_PK=0 OR ViolationNumber_PK = 17957)  and ViolationNumber_PK  NOT in (9662,9663,9665,9666,9667,9668,9669)             
		order by SequenceOrder      
		      

		End 
	
/*	SELECT     ViolationNumber_PK as ID, Description       
	FROM         tblViolations        
	where  violationtype=8 AND   ViolationNumber_PK  NOT in (0,15931,15932,15933,15934,15935,15936,15937,15938,15939)     
	or   ViolationNumber_PK=0    
	order by SequenceOrder   */
End



 

  

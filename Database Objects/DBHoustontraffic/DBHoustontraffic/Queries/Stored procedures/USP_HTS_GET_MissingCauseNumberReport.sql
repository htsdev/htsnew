/*****************
Business Logic:
1. Display clients that has empty or null cause numbers and the number of characters in cause number are less than 12.
2. Case violation status should not be Disposed.
3. Client should be active client.
4. �Not on Court�s System� flag should not be activated.
5. Case violation status category should be bond, waiting, arriangment, jury trial ,  arriangment waiting, judge trial, pre trial,bond waiting.
6. Missing Cause Number Report Only show those records having Follow up date is null or number business days (specified in show setting) in future
7. Follow Up date can only set to 7 Business days for future. 
* 
*  Parameters:
*  *************
* @Report Url= Give Report Url for Missing Cause;  
*****************/
-- Noufil 4432 08/05/2008 html serial number added added for validation report  
  
  --Muhammad Ali 7747 07/01/2010 -->Add Parameter for Report Url.
ALTER PROCEDURE [dbo].[USP_HTS_GET_MissingCauseNumberReport]
	@ReportURL VARCHAR(MAX) = '/Reports/MissingCauseNumber.aspx'
AS
	--Muhammad Ali 7747 07/01/2010..  gEt Bussiness Days...
	DECLARE @NoOfBusinessDay INT
	
	SELECT @NoOfBusinessDay = attribute_value
	FROM   [dbo].[Tbl_BusinessLogicDay]
	WHERE  Report_Id IN (SELECT report_id
	                     FROM   tbl_Report tr
	                     WHERE  tr.Report_Url = @ReportURL) -- ENd Muhamamd Ali
	SELECT t.ticketid_pk,
	       tv.casenumassignedbycourt,
	       refcasenumber,
	       lastname,
	       firstName,
	       dbo.fn_DateFormat(dob, 27, '/', ':', 0) AS dob,
	       dlnumber,
	       dbo.fn_DateFormat(tv.courtdatemain, 24, '/', ':', 1) AS courtdatemain,
	       cvs.shortdescription,
	       c.shortname,
	       CASE 
	            WHEN t.bondflag = 1 THEN 'B'
	            ELSE ''
	       END AS bondflag,
	       '<a href="http://ln.legalhouston.com/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber='
	       + CONVERT(VARCHAR(30), t.ticketid_pk) + '" >' + CONVERT(VARCHAR(30), ROW_NUMBER() OVER(ORDER BY t.ticketid_pk ASC))
	       + '</a>' AS link,
	       ISNULL(
	           CONVERT(VARCHAR(10), tx.MissingCauseFollowUpDate, 101),
	           'N/A'
	       ) AS followupdate,	--Muhammad Ali 7747 07/01/2010 .-->Add three coloumns for FollowUpdate. 
	       tx.missingcausefollowupdate AS sortfollowupdate,
	       CASE CONVERT(VARCHAR, ISNULL(tx.missingcausefollowupdate, ' '), 101)
	            WHEN '01/01/1900' THEN ' '
	            ELSE CONVERT(VARCHAR, tx.missingcausefollowupdate, 101)
	       END AS FollowUpdateForValidation
	FROM   tbltickets t
	       INNER JOIN tblticketsviolations tv
	            ON  t.ticketid_pk = tv.ticketid_pk
	       INNER JOIN tblcourtviolationstatus cvs
	            ON  tv.courtviolationstatusidmain = cvs.courtviolationstatusid
	       INNER JOIN tblcourts c
	            ON  c.courtid = tv.courtid
	            AND c.courtcategorynum IN (1, 2)
	       INNER JOIN tbldatetype d
	            ON  d.typeid = cvs.categoryid
	       LEFT OUTER JOIN tblticketsextensions tx
	            ON  tx.Ticketid_pk = t.TicketID_PK --Muhammad Ali 7747 07/1/2010 --> Join in TblTickets Extension...
	WHERE  --Ozair 7791 07/26/2010  where clause optimized  
	       (
	           --Muhamamd ali 7747 07/1/2010...--> get records having null and days specified in Show Setting Control. 
	           (tx.MissingCauseFollowUpdate IS NULL)
	           OR (
	                  DATEDIFF(DAY, tx.MissingCauseFollowUpdate, GETDATE()) 
	                  <
	                  = 0
	                  AND DATEDIFF(
	                          DAY,
	                          tx.MissingCauseFollowUpdate,
	                          dbo.fn_getnextbusinessday(GETDATE(), @NoOfBusinessDay)
	                      ) >= 0
	              )
	       )
	       AND t.ticketid_pk NOT IN (SELECT ticketid_pk
	                                 FROM   tblticketsflag
	                                 WHERE  ticketid_pk = t.ticketid_pk
	                                        AND flagid = 15)
	       AND (
	               -- Agha Usman 4426 07/31/2008 -
	               LEN(ISNULL(tv.casenumassignedbycourt, '')) < 12
	           )
	       AND d.typeid IN (1, 2, 3, 4, 5, 12)
	       AND cvs.CourtViolationStatusID <> 80
	       AND activeflag = 1
	ORDER BY
	       t.ticketid_pk -- Adil Aleem 5857 05/15/2009 Include Ticket ID in order by clause 
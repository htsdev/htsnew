﻿/***********************************************************************************
 * Created By		: Abid Ali
 * Created Date		: 02/21/2009
 * Task Id			: 5563
 * 
 * Business Logic	: To insert letter of rep into batch
 * 
 * Parameter List	:
 *						@TicketID			: Ticket ID
 *						@EmpID				: Employee ID
 *						@BatchID			: Batch ID
 ********************************************************************************/

ALTER PROCEDURE [dbo].[USP_HTP_INSERT_LetterOfRepBatch]
(@TicketID INT, @EmpID INT, @BatchID INT)
AS
	DECLARE @strVacationDates VARCHAR(2000)
	SET @strVacationDates = dbo.fn_getvacation()
	
	--Sabir Khan 8899 02/17/2011 Getting spacefic date for PMC court location...
	--Sabir Khan 8899 7/14/2011 Replace the repeated code in function for Getting spacefic date for PMC court location...
	DECLARE @selectedCourtDate DATETIME	
	SET @selectedCourtDate = [dbo].[fn_GetRepDateForPMC](@TicketID) 
	----------------------------------------------------------------------------
	
	SELECT DISTINCT
	       dbo.Fn_htp_get_InitCap(firstname) AS firstname,
	       dbo.Fn_htp_get_InitCap(lastname) AS lastname,
	       dbo.formatdate(dob) AS dob,
	       T.ticketid_pk,
	       dlnumber,
	       @strVacationDates AS vacdate,
	       ticketsviolationid,
	       dbo.Fn_htp_get_InitCap(a.description) AS viol_desc,
	       CASE 
	            WHEN LEN(ISNULL(casenumassignedbycourt, '')) = 0 THEN 
	                 refcasenumber
	            ELSE casenumassignedbycourt
	       END AS casenumassignedbycourt,
	       refcasenumber,
	       languagespeak,
	       dbo.formatdateandtime_with_at_sign(V.courtdatemain) AS currentdateset,
	       V.courtdatemain AS CourtDate,
	       v.CourtNumbermain AS CourtNumber,
	       dbo.Fn_htp_get_InitCap(firstname + ' ' + lastname) AS fullname,
	       C.courtcontact AS attn,
	       C.judgename AS judgename,
	       dbo.formatphonenumbers(CONVERT(VARCHAR(20), C.phone)) AS phone,
	       C.courtname AS courtname,
	       CASE 
	            WHEN LEN(ISNULL(C.address2, '')) = 0 THEN C.address + ' ' + C.address2
	            ELSE C.address + ', ' + C.address2
	       END AS ADDRESS,
	       C.city + ', ' + E.state + ' ' + C.zip AS cityzip,
	       dbo.formatphonenumbers(CONVERT(VARCHAR(20), C.fax)) AS FAXNO,
	       --Zeeshan Haider 11306 08/12/2013 PMC Jury-Trial Cases(Do not modify case setting for sending LOR)
	       CASE 
	            WHEN v.CourtID = 3004 AND v.CourtViolationStatusIDmain = 26 THEN 
	                 'Jury-Trial'
	            ELSE S.description
	       END AS DESCRIPTION,
	       firmname AS firmname,
	       F.address AS firmaddress,
	       F.address2 AS firmaddress2,
	       F.city AS firmcity,
	       'TX' AS firmstate,
	       F.zip AS firmzip,
	       dbo.formatphonenumbers(CONVERT(VARCHAR(20), F.phone)) AS firmphone,
	       dbo.formatphonenumbers(CONVERT(VARCHAR(20), F.fax)) AS firmfax,
	       condition1flag,
	       dbo.Fn_htp_get_InitCap(Attorneyname) AS Attorneyname,
	       dbo.Fn_htp_get_InitCap(firmsubtitle) AS firmsubtitle,
	       --Zeeshan Haider 11306 08/12/2013 PMC Jury-Trial Cases(Do not modify case setting for sending LOR)
	       CASE 
	            WHEN V.CourtID = 3004 AND V.CourtViolationStatusIDmain = 26 THEN 
	                 'Jury-Trial'
	            ELSE (
	                     SELECT TOP 1 dbo.tblCourtSettingRequest.Description
	                     FROM   dbo.tblCourts
	                            INNER JOIN dbo.tblCourtSettingRequest
	                                 ON  dbo.tblCourts.SettingRequestType = dbo.tblCourtSettingRequest.SettingType
	                            INNER JOIN dbo.tblTicketsViolations
	                                 ON  dbo.tblCourts.Courtid = dbo.tblTicketsViolations.CourtID
	                     WHERE  (dbo.tblTicketsViolations.TicketID_PK = t.TicketID_PK)
	                 )
	       END AS courtDesc,
	       (
	           SELECT MAX(dbo.tblTicketsViolations.CourtDateMain) AS Expr1
	           FROM   dbo.tblTicketsViolations
	                  INNER JOIN dbo.tblCourtViolationStatus
	                       ON  dbo.tblTicketsViolations.CourtViolationStatusIDmain = 
	                           dbo.tblCourtViolationStatus.CourtViolationStatusID
	           WHERE  (dbo.tblCourtViolationStatus.CategoryID IN (3, 4, 5))
	           HAVING (
	                      MAX(dbo.tblTicketsViolations.CourtDateMain) > GETDATE() + 21
	                  )
	       ) AS NewCourtDate,
	       CASE 
	            WHEN V.Courtid = 3004 --Sabir Khan 8899 02/17/2011 setting different rep date if court is PMC.
	            --Zeeshan Haider 11306 08/12/2013 PMC Jury-Trial Cases(Do not modify case setting for sending LOR)				
	                 AND V.CourtViolationStatusIDmain <> 26 THEN dbo.fn_DateFormat(@selectedCourtDate, 31, '/', ':', 1)
	            WHEN V.CourtID = 3004 AND V.CourtViolationStatusIDmain = 26 THEN 
	                 dbo.fn_DateFormat(V.CourtDateMain, 31, '/', ':', 1)
	            ELSE dbo.fn_DateFormat(C.RepDate, 31, '/', ':', 1)
	       END AS RepDate,
	       V.courtid,
	       --Zeeshan Haider 11306 08/12/2013 PMC-Jury Trial Cases(Do not modify case setting for sending LOR)
	       CASE 
	            WHEN V.CourtID = 3004 AND V.CourtViolationStatusIDmain = 26 THEN 
	                 'Jury-Trial'
	            ELSE (
	                     SELECT s.description
	                     FROM   tblCourtSettingRequest s,
	                            tblcourts co
	                     WHERE  co.settingrequesttype = s.settingtype
	                            AND co.courtid = V.courtid
	                 )
	       END AS SettingRequest,
	       T.email,
	       @BatchID AS BatchID,
	       ISNULL(v.UnderlyingBondFlag, 0) AS bondprintflag,
	       V.courtviolationstatusidmain AS CourtViolationStatus
	       INTO #temp
	FROM   tbltickets T,
	       tblcourts C,
	       tblticketsviolations V,
	       tblcourtviolationstatus U,
	       tblcourtsettingrequest S,
	       tblfirm F,
	       tblstate E,
	       tblviolations a
	WHERE  V.courtid = C.courtid
	       AND T.ticketid_pk = V.ticketid_pk
	       AND v.violationnumber_pk = a.violationnumber_pk
	       AND C.settingrequesttype = S.settingtype
	       AND c.state = E.stateid
	       AND T.firmid = F.firmid
	       AND V.courtviolationstatusidmain = U.courtviolationstatusid
	       AND activeflag = 1
	       AND settingrequesttype NOT IN (0)
	       AND V.courtid NOT IN (3001, 3002, 3003)
	       AND u.CategoryID <> 50
	       AND T.ticketid_pk = @TicketID
	ORDER BY
	       [casenumassignedbycourt] DESC                                      
	
	DECLARE @Causenumbers VARCHAR(200)            
	SET @Causenumbers = ''            
	
	SELECT @Causenumbers = @Causenumbers + casenumassignedbycourt + ' / '
	FROM   #temp
	WHERE  UPPER(viol_desc) LIKE '%SPEED%' --Sabir Khan 6359 08/10/2009 get cause only for speeding violations...                                  	
	
	INSERT INTO tblLORBatchHistory
	  (
	    [TicketID_PK],
	    [FullName],
	    [DOB],
	    [vacdate],
	    [casenumassignedbycourt],
	    [languagespeak],
	    [currentdateset],
	    [attn],
	    [judgename],
	    [phone],
	    [courtname],
	    [address],
	    [cityzip],
	    [FAXNO],
	    [Attorneyname],
	    [firmsubtitle],
	    [RepDate],
	    [courtid],
	    [SettingRequest],
	    [causenumbers],
	    [BatchID_FK],
	    dlnumber,
	    viol_desc,
	    refcasenumber,
	    IsBond,
	    CourtViolationStatus,
	    CourtNumber,
	    email,
	    CourtDate
	  )
	SELECT ticketid_pk,
	       [FullName],
	       [DOB],
	       [vacdate],
	       [casenumassignedbycourt],
	       [languagespeak],
	       [currentdateset],
	       [attn],
	       [judgename],
	       [phone],
	       [courtname],
	       [address],
	       [cityzip],
	       [FAXNO],
	       [Attorneyname],
	       [firmsubtitle],
	       [RepDate],
	       [courtid],
	       [SettingRequest],
	       CASE 
	            WHEN LEN(@Causenumbers) > 0 THEN LEFT(@Causenumbers, LEN(@Causenumbers) -1)
	            ELSE ''
	       END,	-- Sabir Khan 6359 08/10/2009 Fixed empty cause number issue...
	       BatchID,
	       dlnumber,
	       viol_desc,
	       refcasenumber,
	       bondprintflag,
	       CourtViolationStatus,
	       CourtNumber,
	       email,
	       CourtDate
	FROM   #temp 
	
	DROP TABLE #temp
GO



GRANT EXEC ON [dbo].[USP_HTP_INSERT_LetterOfRepBatch] TO dbr_webuser

GO
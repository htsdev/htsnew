SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Insert_NoEmailFlag]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Insert_NoEmailFlag]
GO



CREATE procedure [dbo].[USP_HTS_Insert_NoEmailFlag]  
  
@ticketid int 
  
AS  
  
Declare @email varchar(100)  
  
select @email =email from tbltickets where ticketid_pk =@ticketid  
  
if((select count(email) from tblnoemailflag where email like @email) = 0)  
   
 begin  
  
  insert into tblnoemailflag (Ticketid_pk,Email,UnsuscribedDate)  
  values(    
    @ticketid,  
    @email,  
    getdate()  
    )  
 END  
  
  
  












GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


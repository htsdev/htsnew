USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTP_GetHCCCScraperLoaderID]    Script Date: 10/23/2013 12:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****************************************************************************************************** 
Created by:		Sabir Khan
Task ID:		11277
Date:			09/20/2013
Business Logic:	This procedure is used to get the loader id of HCCC Scrapper Loader. 
******************************************************************************************************/
  
ALTER PROCEDURE [dbo].[USP_HTP_GetHCCCScraperLoaderID]
AS
BEGIN
	SET NOCOUNT ON
	SELECT Loader_ID  
	FROM LA_Houston.dbo.tbl_HTS_LA_Loaders 
	WHERE Loader_Name LIKE 'HCCC Criminal Filings Scraper Loader'
END

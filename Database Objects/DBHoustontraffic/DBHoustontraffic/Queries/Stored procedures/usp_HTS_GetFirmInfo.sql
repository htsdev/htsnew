SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_HTS_GetFirmInfo]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_HTS_GetFirmInfo]
GO


-- usp_HTS_GetFirmInfo 3006

CREATE PROCEDURE  [dbo].[usp_HTS_GetFirmInfo]

@FirmID int

 AS

Select  FirmID, FirmAbbreviation,FirmName,Address,Address2,City,tblFirm.State,Zip,Phone,Fax,AttorneyName,Firmsubtitle,BaseFee,DayBeforeFee,JudgeFee,st.State 
as StateName  
From tblFirm inner join tblstate st on tblFirm.State =st.StateID
where FirmID=@FirmID



 
-------------------------------------------------------------------------------------------------------------------------   



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


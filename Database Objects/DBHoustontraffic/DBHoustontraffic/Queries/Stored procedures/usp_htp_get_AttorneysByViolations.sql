/*************************************************************************
	Created By : Afaq Ahmed
	Task ID :8521
	Created Date : 01/04/2011
	Bussiness Logic: Return attorney on the basis of ticketid for a particular violation.
	Parameter:
	@TicketId,@ViolationId
				
 	Column Return: Attoryney Name,Attorney EmailAddress,Attorney CellNumber
 *************************************************************************/
CREATE PROCEDURE usp_htp_get_AttorneysByViolations   
@TicketID INT,  
@ViolationId INT   
AS  

SELECT DISTINCT    
ISNULL(tf.AttorenyFirstName + ' ' + tf.AttorneyLastName, tf.FirmName) AS AttorneyName,  
ISNULL(TF.AttorneyEmailAddress, '') AS Email,ISNULL(tu.ContactNumber,'') AS AttorneyCellNumber  
  FROM tblticketsviolations t  
  INNER JOIN tblFirm tf ON t.CoveringFirmID = tf.FirmID  
  LEFT OUTER JOIN tblUsers tu ON tf.FirmID = tu.FirmID  
WHERE t.TicketID_PK = @TicketID AND t.TicketsViolationID=@ViolationId  
and t.ViolationNumber_PK != '16159'  
  
  
GO

GRANT EXECUTE ON usp_htp_get_AttorneysByViolations TO dbr_webuser
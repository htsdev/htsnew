﻿/************************************************************
 * Code formatted by SoftTree SQL Assistant © v4.0.34
 * Time: 4/10/2009 5:40:48 PM
 ************************************************************/

          
/**********        
        
Created By : Waqas Javed 5770 04/09/2009 Address validation Report
        
Business Logic : This Report display client cases with following logics      
* Case should be active.
* Verified status should be in Arraignment, Appearance, waiting, Arrainment or Bond waiting, Pre trial, Judge Trial or Jury Trial.
* whose Last Name, DOB, First Name Initial is same but address in different.
        
List Of Parameters :         
@Validation : if 1 then follow up date will be checked for validation email  

List Of Columns :      
        
ticketid_pk
LastName  
DOB
FirstInitial 
Address
       
**********/         
        
CREATE PROCEDURE [dbo].[usp_htp_get_AddressValidation]
	@Validation INT = 0
AS
	SELECT DISTINCT 
	       tt.TicketID_PK,
	       tt.Lastname,
	       CONVERT(VARCHAR, tt.DOB, 101) AS DOB,
	       SUBSTRING(tt.Firstname, 1, 1) AS FirstNameInitial,
	       tt.Address1 + ' ' + tt.Address2 + ', ' + tt.City + ', ' + s.[State] + 
	       ' ' + tt.Zip AS [Address] 
	       INTO #temp1
	FROM   tblTickets tt
	       INNER JOIN tblTicketsViolations ttv
	            ON  tt.TicketID_PK = ttv.TicketID_PK
	       INNER JOIN tblState s
	            ON  tt.Stateid_FK = s.StateID
	       INNER JOIN tblCourtViolationStatus tcvs
	            ON  ttv.CourtViolationStatusIDmain = tcvs.CourtViolationStatusID
	WHERE  tcvs.CategoryID <> 50
	       AND tt.Activeflag = 1
	ORDER BY
	       tt.Lastname,
	       CONVERT(VARCHAR, tt.DOB, 101),
	       SUBSTRING(tt.Firstname, 1, 1)   
	
	
	SELECT #temp1.Lastname,
	       #temp1.DOB,
	       #temp1.FirstNameInitial,
	       #temp1.Address INTO #temp2
	FROM   #temp1  
	
	
	SELECT DISTINCT MAX(#temp1.ticketid_pk) AS TicketID_PK,
	       #temp1.Lastname,
	       #temp1.DOB,
	       #temp1.FirstNameInitial,
	       #temp1.Address
	FROM   #temp1
	       INNER JOIN #temp2
	            ON  #temp1.Address <> #temp2.Address
	            AND #temp1.Lastname = #temp2.Lastname
	            AND #temp1.DOB = #temp2.DOB
	            AND #temp1.FirstNameInitial = #temp2.FirstNameInitial
	GROUP BY
	       #temp1.Lastname,
	       #temp1.DOB,
	       #temp1.FirstNameInitial,
	       #temp1.Address
	ORDER BY
	       #temp1.lastname 
	
	DROP TABLE #temp1 
	DROP TABLE #temp2
GO


GRANT EXECUTE ON [dbo].[usp_htp_get_AddressValidation] TO dbr_webuser
GO
          


  
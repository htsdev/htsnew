 --****************************************************************************
--Created By   : Afaq Ahmed
--Creation Data: 11/02/2010
--Task ID :8213
--Parameter List : N/A
--Return Column: N/A
--Business Logic : This procedure executes USP_HTS_GET_PR_POA_REPORT and USP_HTP_GET_Trial_Continuance_REPORT.
--****************************************************************************
CREATE PROCEDURE usp_htp_get_JudgeTrial

AS 

EXEC USP_HTS_GET_PR_POA_REPORT
EXEC USP_HTP_GET_Trial_Continuance_REPORT   

 
 GO 
 
 GRANT EXECUTE ON usp_htp_get_JudgeTrial TO webuser_hts
--SELECT tta.FirstName ,tta.lastname,*  FROM tblTicketsArchive tta WHERE tta.MidNumber='OO' and tta.FirstName IS NOT NULL AND tta.LastName IS NOT NULL 

/****** 
Created by:		Tahir Ahmed
Business Logic:	The procedure is used by HTP /Activities /Traffic Alert application to get 
				recently  uploaded violations related to people that hired us in past
				or contacted us in past or previous violations....
				
List of Parameters:
	@casedescid:		INPUT PARAMETER USED TO FILTER RECORDS ON SPECIFIC CASE STATUS..                      
	@sdate:				INPUT PARAMETER TO FILTER RECORDS ON A DATE RANGE....                  
	@edate:				INPUT PARAMETER TO FILTER RECORDS ON A DATE RANGE....                  
	@courtloc:			TO FILTER ON COURT LOCATION...... ( IT WILL BE EITHER INSIDE OR OUTSIDE COURTS OR ALL                  
	@futureDate:		HIGHLIGHT RECORDS HAVING FUTURE COURT DATES ONLY.....                  
	@clienttype:		TO FILTER RECORDS ON CLIENT TYPE i.e HIRED OR QUOTE OR ALL                  
	@pccrstatus:		TO FILTER RECORDS ON QUOTE STATUS TYPES......                  
	@NoContactFilter:	TO FILTER RECORDS ON NO CONTACT TYPES....          


List of Columns:
	ClientName:			Client's first & last name
	ListDate:			Date when data uploaded in the system
	CourtDate:			Court date associated with the violation
	MidNumber:			Mid Number associated with the client
	Language:			Spkoken language by the client
	TicketNumber:		Case number associated with the violation
	[StatusDescription]: PCCR Status of client
 
*******/


--EXEC USP_Mid_Get_ClientLookupReport 0,'07/01/2013','07/28/2013',0,0,1,0,false
ALTER procedure [dbo].[USP_Mid_Get_ClientLookupReport]                      
 (                      
 @casedescid as int,                    
 @sdate as datetime,            
 @edate as datetime,   
 @courtloc as int,         
 @futureDate as int,  
 @clienttype as int,   
 @pccrstatus as int,    
 @NoContactFilter as bit
 )                      
as                        
                      
                    
                      
declare @SqlScript  as nvarchar(4000)                       
declare @param   as nvarchar(200)                      
                   
declare @pendingID int    
set @pendingID = (select StatusID from tblpccrstatus where StatusDescription = 'PENDING')    
       
-- ADDING TIME PART TO THE TODATE.....                  
set @edate = @edate + '23:59:59'                          
                      
-- DECLARING TEMPORARY TABLE........                      
--Nasir 5019 11/21/2008 Adding Colunm StatusDescription
Set @SqlScript ='                   
declare @tblClientLookup TABLE (                      
 [LastName] [varchar] (50),                      
 [FirstName] [varchar] (50),                       
 [ListDate] [datetime] NULL ,                      
 [MidNumber] [varchar] (20)  ,                  
 [CourtDate] [datetime] NULL,
 [TicketNumber] [varchar] (20),
 [StatusDescription] [varchar] (50)                 
) '                  
                  
-- INSERTING RECORDS IN TEMPORARY TABLE ON THE SPECIFIED CRITERIA.........                  
Set @SqlScript = @SqlScript +'                     
INSERT INTO @tblClientLookup                      
SELECT DISTINCT                   
 ta.LastName,                   
 ta.FirstName,                   
 ta.ListDate,                   
 ta.MidNumber,                  
 min(tva.CourtDate),
 ta.TicketNumber,
--Nasir 5019 11/21/2008 Adding Colunm StatusDescription
 PS.StatusDescription            
FROM    dbo.tblDateType dt                   
INNER JOIN                  
 dbo.tblCourtViolationStatus cvs                   
ON  dt.TypeID = cvs.CategoryID                   
RIGHT OUTER JOIN                  
 dbo.tblTicketsArchivePCCR tp 
inner join tblPCCRStatus PS on 
 PS.StatusID=tp.PCCRStatusID                   
RIGHT OUTER JOIN                  
 dbo.tblTicketsViolationsArchive tva                   
INNER JOIN                  
 dbo.tblTicketsArchive ta                   
--ON  tva.TicketNumber_PK = ta.TicketNumber                   
on tva.recordid = ta.recordid      
ON  tp.RecordID_FK = ta.recordID                   
ON  cvs.CourtViolationStatusID = tva.violationstatusid                  
                  
WHERE   ta.Clientflag = 0
and ta.listdate between @sdate and @edate                  
'                  
                  

-- TAHIR 4136 05/29/2008
-- ADDED FTA  CASES TO THE REPORT
                  
-- IF CASE STATUS OTHER THAN 'ALL' AND 'ARRAIGNMENT OR BLANK' IS SELECTED                  
if ( @casedescid <> 0 and @casedescid <> 2 AND  @casedescid <> 186 )                  
 begin                   
  Set @SqlScript = @SqlScript +'AND (dt.typeid = @casedescid)                   
'                  
 end                   
                      
-- IF 'ARRAIGNMENT OR BLANK IS SELECTED AS CASE STATUS.......                  
if @casedescid = 2                       
 begin                       
  Set @SqlScript = @SqlScript + 'AND ((dt.typeid = @casedescid) or (dt.typeid  is null))                  
'                      
 end                       
     
 
 -- IF 'FTA - WARRANT' IS SELECTED AS CASE STATUS.......                  
if @casedescid = 186                       
 begin                       
  Set @SqlScript = @SqlScript + 'AND (tva.violationstatusid = 186)                  
'                      
 end     
                  
-- IF ALL IS SELECTED AS CASE STATUS.......                  
if @casedescid = 0                  
 begin                       
  Set @SqlScript = @SqlScript + 'AND ((dt.typeid in (2,5)) or (dt.typeid  is null) or  tva.violationstatusid = 186 )                  
'                      
 end   
 
-- END 4136 CHANGES....                     
                  
-- IF 'INSIDE COURTS' IS SELECTED IN COURT LOCATION FILTER.....                  
if @courtloc = 1                   
 begin                  
  Set @SqlScript = @SqlScript + 'AND ta.courtid in (3001,3002,3003) '                      
 end                      
                  
-- IF OUTSIDE COURTS IS SELECTED IN COURT LOCATION FILTER........                  
else if @courtloc = 2                  
 begin                  
  Set @SqlScript = @SqlScript + 'AND ta.courtid not in (3001,3002,3003) '                      
 end                      
                  
-- IF RECORDS HAVING FUTURE COURT DATE IS REQUIRED.....                  
if @futureDate = 1                       
 begin                       
  Set @SqlScript = @SqlScript + 'AND (tva.courtdate > getdate()) '                      
 end                       
                  
-- IF QUOTE STATUS OTHER THAN 'ALL' AND 'NO ACTION' IS SELECTED.......                  
if (@pccrstatus <> 0 and @pccrstatus <>1 and @pccrstatus <> -2 and @pccrstatus <> @pendingID/* and @NoContactFilter=0 */ )                  
if @NoContactFilter=0      
begin      
 begin                  
     Set @SqlScript = @SqlScript +'and tp.PCCRStatusID = @pccrstatus '                      
 end                   
end                  
-- IF 'NO ACTION' IS SELECTED AS QUOTE STATUS FILTER.....                  
if (@pccrstatus = 1 )                  
 begin                  
   Set @SqlScript = @SqlScript +'and tp.PCCRStatusID is null '                      
 end                   
    
if(@pccrstatus = -2)    
begin    
set @SqlScript = @SqlScript + 'and tp.PCCRStatusID <> ' + convert(varchar,@pendingID) +' '    
end    
    
if(@pccrstatus = @pendingID)    
begin     
set @SqlScript = @SqlScript + ' and (tp.PCCRStatusID is null or tp.PCCRStatusID = ' +convert(varchar,@pendingID) +') '    
end    
                  
                  
-- GROUPING THE RECORDS TO GET THE DISTINCT RECORDS....   
--Nasir 5019 11/21/2008 Adding Colunm StatusDescription               
Set @SqlScript = @SqlScript + 'group by ta.lastname, ta.firstname, ta.listdate, ta.midnumber, tva.CourtDate, ta.TicketNumber,PS.StatusDescription '                  
                      
-- NOW GETTING THE FINAL OUTPUT BY FETCHING RECORDS FROM THE TEMPORARY TABLE.                  
-- RECORDS HAVING AN ENTRY IN TBLRESTRICTED PHONE NUMBERS WILL NOT BE RETRIEVED...                  
Set @SqlScript = @SqlScript + '                       
SELECT   distinct                     
 b.[LastName]+ '' '' + b.[FirstName] as clientname,                  
 b.ListDate  ,                    
 b.CourtDate ,                   
 b.MidNumber,                  
 isnull(max(t.LanguageSpeak),''N/A'')AS Language,
 b.TicketNumber,
--Nasir 5019 11/21/2008 Adding Colunm StatusDescription 
 isnull(b.StatusDescription,''Pending'') as StatusDescription               
FROM  dbo.tblTickets  t                      
INNER JOIN                      
 @tblClientLookup b                   
ON  t.Midnum = b.MidNumber
--Faique Ali 11490 11/04/2013 prevent for unmatching records 
and b.firstname=t.firstname and b.lastname=t.lastname
'          
          
if(@pccrstatus = -2)    
begin    
set @SqlScript = @SqlScript + ' WHERE 1=1 '    
end    
else    
begin    
IF @NoContactFilter=1          
 begin          
 SET @SqlScript = @SqlScript + '          
where contact1 in (select distinct phonenumber from tblrestrictedphonenumbers)                  
 or  contact2  in (select distinct phonenumber from tblrestrictedphonenumbers)                  
 or contact3  in (select distinct phonenumber from tblrestrictedphonenumbers)                  
'                  
 end          
          
ELSE          
 begin          
   SET @SqlScript = @SqlScript + '                 
 where contact1 not in (select distinct phonenumber from tblrestrictedphonenumbers)                  
 and  contact2 not in (select distinct phonenumber from tblrestrictedphonenumbers)                  
 and contact3 not in (select distinct phonenumber from tblrestrictedphonenumbers)                  
'                  
 end          
end    
          
          
                  
-- IF CLIENT TYPE OTHER THAN 'ALL' IS SELECTED IN CLIENT TYPE FILTER.......                  
if @clienttype <> 2                      
 begin                       
    Set @SqlScript = @SqlScript + 'AND (t.Activeflag = @clienttype)                  
'    
 end               
        
-- Babar Ahmad 9654 09/30/2011 Excluded records with Problem Client (No Hire/Allow Hire) flag.                   
set @SqlScript = @Sqlscript + ' and t.FirmID = 3000 

and t.TicketID_PK NOT IN (SELECT TicketID_PK FROM tblTicketsFlag ttf WHERE ISNULL(ttf.FlagID,0) IN (13,36))

'                    
--Nasir 5019 11/21/2008 Adding Colunm StatusDescription                    
Set @SqlScript = @SqlScript + 'group by b.[LastName]+ '' '' + b.[FirstName], b.listdate, b.courtdate, b.midnumber, b.courtdate, b.TicketNumber,b.StatusDescription                   
order by b.CourtDate desc,[ClientName]'                        
                  
                  
set @param = '@sdate datetime,   @edate datetime, @casedescid int, @clienttype int, @pccrstatus int'                      
exec sp_executesql @SqlScript ,@param, @sdate, @edate, @casedescid, @clienttype, @pccrstatus                      
                  
--print @SqlScript

/******************
Created By: Yasir Kamal 


Business Logic: This procedure is used to get Report Name,Report ID,Report Count By Providing Report Url and Attribute Key.
				
List of Parameters:
-------------------
@Report_Url = Report Url  eg : /Reports/frmRptAppealConfirmation.aspx
@Attribute_key =  eg: 'Days'

List Of Columns
----------------
Report_Name
Report_ID
Report_Count

*****************/


Create PROCEDURE [dbo].[usp_htp_get_reportByUrl] 
 @Report_Url nvarchar(2000),    
 @Attribute_key varchar(50)    
    
AS    
BEGIN    
 SET NOCOUNT ON;    
 Select Report_Name,Report_ID,(select count(*) from tbl_BusinessLogicDay where Report_ID = tr.Report_ID and Attribute_key=@Attribute_key and isinActive=1) as [Report_count] from tbl_Report tr     
 where tr.Report_Url = @Report_Url    
END    


GO
grant execute on [dbo].[usp_htp_get_reportByUrl] to dbr_webuser
GO  
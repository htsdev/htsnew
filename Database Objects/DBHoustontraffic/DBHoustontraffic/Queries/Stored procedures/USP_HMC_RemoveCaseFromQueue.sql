SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HMC_RemoveCaseFromQueue]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HMC_RemoveCaseFromQueue]
GO




CREATE Procedure [dbo].[USP_HMC_RemoveCaseFromQueue]   
(  
  
@RecordIDs varchar(200)  
  
)  
  
as  
  
  
declare @Records  table(ID int)          
insert into @Records select * from dbo.sap_string_param(@RecordIDs)     
  
  
Update tbl_HMC_Efile  
  
Set   
Flag = -1   
where RecordID in ( Select ID from @Records )


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


﻿/****** 
Altered by:		Fahad Muhammad Qureshi.
TaskID:			6934
Altered Date:	12/08/2009
Business Logic:	The procedure is used by Houston Traffic Program for getting all case types.
				
List of Parameters:
					N/A

******/
ALTER PROCEDURE [dbo].[usp_HTS_Get_All_QueryType]
AS
	SELECT QueryTypeID,
	       QueryTypeDesc
	FROM   dbo.tblQueryType
	WHERE  QueryTypeID IN (1,2)--Fahad 6934 01/14/2010 removed excluding 4 check and included in clause --!= 4
	ORDER BY
	       QueryTypeID  
   
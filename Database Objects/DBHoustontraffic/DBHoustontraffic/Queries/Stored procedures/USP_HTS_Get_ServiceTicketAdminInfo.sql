
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_HTS_Get_ServiceTicketAdminInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_HTS_Get_ServiceTicketAdminInfo]
go    
CREATE procedure [dbo].[USP_HTS_Get_ServiceTicketAdminInfo]           
-- khalid 3188 2/19/08 getting serviceticket admin info         
as          
          
select * , employeeid as EmpId          
      
 

from  tblusers          
where isServiceTicketAdmin=1              
and status = 1    
    
go

GO
GRANT EXEC ON [dbo].[USP_HTS_Get_ServiceTicketAdminInfo] TO [dbr_webuser]
GO 
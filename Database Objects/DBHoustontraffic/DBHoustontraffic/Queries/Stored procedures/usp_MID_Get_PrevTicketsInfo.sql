﻿ set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


    
ALTER procedure [dbo].[usp_MID_Get_PrevTicketsInfo] --'00896722'                
 (                
 @MIDNum  varchar(20) -- MID NUMBER WILL BE USED TO FILTER OUT THE RELATED RECORD.....                
 )                
                
as                
                
-- GETTING RECORDS .......                
SELECT distinct                
 upper(convert(varchar(25),tv.refcasenumber) +'-'+                 
 convert(varchar(10),0)) as CaseNo, -- Agha Usman 2664 06/04/2008
 upper(c.ShortName) as CourtName,                  
-- upper(dt.[Description]) as CaseStatus,                 
 upper(isnull(cvs.shortdescription,'N/A')) as CaseStatus,                
-- v.[Description] AS ViolDesc,                 
 upper((case when isnull(v.[description],'') like '%------%-----%' then '-NA-'                
  else (case when isnull(v.[description],'') = '---Choose----' then '-NA-'                
        else isnull(v.[description],'')                
        end)                   
 end)) AS ViolDesc,                
 tv.CourtDate as CourtDate,                 
 upper(cd.[Description]) AS ViolOutCome,                
 t.ticketid_pk as TicketId,              
tv.CourtNumbermain as CourtNumber,              
'$' + Convert(nvarchar,Convert(int,ttp.ChargeAmount)) as AmountPaid,        
isnull(t.CDLFlag,3) as CDLFlag,      
isnull(t.Generalcomments,'') as Generalcomments,       
f.FirmAbbreviation as Firm  
-- JOINING TABLE TO GET VIOLATION OUTCOME.....                
FROM    dbo.tblCaseDispositionstatus cd                
RIGHT OUTER JOIN                
 dbo.tblTickets t                
INNER JOIN                
 dbo.tblTicketsViolations tv                
ON  t.TicketID_PK = tv.TicketID_PK                 
INNER JOIN                
 -- TO GET COURT NAME.....                
 dbo.tblCourts c                
ON  tv.CourtID =c.Courtid                 
INNER JOIN                
 -- TO GET VIOLATION DESCRIPTION.....                
 dbo.tblViolations v                
ON  tv.ViolationNumber_PK = v.ViolationNumber_PK                 
ON  cd.CaseStatusID_PK = tv.ViolationStatusID                 
LEFT OUTER JOIN                
 -- TO GET CASE STATUS....                
 dbo.tblDateType dt                
INNER JOIN                
 dbo.tblCourtViolationStatus cvs                
ON  dt.TypeID = cvs.CategoryID                 
ON  tv.CourtViolationStatusID = cvs.CourtViolationStatusID                
-- TO GET PAYMENT INFO              
INNER JOIN              
dbo.tblticketspayment ttp              
ON tv.TicketID_PK = ttp.TicketID    
INNER JOIN  
tblFirm AS f ON t.FirmID = f.FirmID  
             
              
where  t.midnum= @MidNum                
and tv.refcasenumber <> '0'                
and v.violationtype <> 1              
and ttp.paymentvoid = 0          
  
    
order by t.ticketid_pk      


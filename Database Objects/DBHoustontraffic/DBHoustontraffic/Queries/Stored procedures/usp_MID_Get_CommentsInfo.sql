SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_MID_Get_CommentsInfo]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_MID_Get_CommentsInfo]
GO

-- usp_MID_Get_CommentsInfo '2894352,'    

--select * from tblticketsarchivepccr where recordid_fk = 2894352    

 CREATE  procedure [dbo].[usp_MID_Get_CommentsInfo]           
 (          
 @RecIDs varchar(200)   -- INPUT PARAMETER FOR RECORD ID.. IF MORE THAN ONE CASES INVOLVED THIS       
   -- WILL BE COMMA SEPARATED STRING HAVING RECORD IDs OF ALL THE TICKETS....      
 )          
as          
          
    
-- FETCHING RECORDS RELATED TO THE SPECIFIED RECORD IDs      
-- BY JOINING THE LOGGING TABLE (TBLTICKETSARCHIVEPCCR) WITH THE TBLUSERS TO GET THE USER NAME      
-- AND TBLTICKETSARCHIVE TO GET THE RELATED INFORMATION.....      
    
SELECT     TOP 1 PERCENT ISNULL(CONVERT(varchar(10), pccr.LastUpdateDate, 101), '') AS contactdate,   
    ISNULL(UPPER(u.Abbreviation), '') AS UserName,     
                ISNULL(pccr.PCCRStatusID, 0) AS pccrstatus,   
    ISNULL(UPPER(pccr.Comments), '') AS comments,   
    ISNULL(pccr.PriceShopping, 0) AS PriceShopping,     
                ISNULL(pccr.HiredAnother, 0) AS HiredAnother,   
    ISNULL(pccr.HandleMySelf, 0) AS HandleMySelf,   
    ISNULL(pccr.PaidTicket, 0) AS PaidTicket,   
    ISNULL(pccr.DSCProb, 0)   AS DSCProb,   
    ISNULL(pccr.NoMoney, 0) AS NoMoney,   
    ISNULL(pccr.TooExpensive, 0) AS TooExpensive, 
     ISNULL(pccr.PoorService, 0) AS PoorService,   
    isnull(convert(int, pccr.IsAccident),3) AS IsAccident,   
    ISNULL(pccr.IsCDL, 3) AS IsCDL, ta.RecordID    
FROM         dbo.tblTicketsArchivePCCR AS pccr INNER JOIN    
                      dbo.tblUsers AS u ON pccr.EmployeeID = u.EmployeeID RIGHT OUTER JOIN    
                      dbo.tblTicketsArchive AS ta ON pccr.RecordID_FK = ta.RecordID    
WHERE     (ta.RecordID IN    
                          (SELECT     MainCat    
                            FROM          dbo.Sap_Dates(@RecIDs) AS Sap_Dates_1))    
ORDER BY ta.RecordID    
    


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


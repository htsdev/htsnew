-- =============================================
/***
CREATED BY: ZEESHAN ZAHOOR ON 09/11/2008
BUSINESS LOGIC:
	All outside court cases that have status of Arraignment or Appearance. can be viewed by sales rep(s)
LIST OF PARAMETERS:
			None
LIST OF COLUMNS:
		TICKETID_PK: Primary key or unique id againts any violation
        FIRSTNAME: Cutomer's first name
		LASTNAME: cutomer's Last Name
	    CASENUMASSIGNEDBYCOURT: CAUSENUMBER number assigned by court 
	    COURTDATEMAIN: Court date
	    COURTVIOLATIONSTATUSIDMAIN: court Violation Stats Id
	    COURTNUMBERMAIN: ROOM NUMBER Number of court
	    DESCRIPTION: Court status in this case Arrignment and Appearance required.
        CRT_LOCATION: Court Locations

****/
-- =============================================
-- [dbo].[USP_HTP_Get_CourtArrignmantAppearance_Report] 1		 	 
 
ALTER PROCEDURE [dbo].[USP_HTP_Get_CourtArrignmantAppearance_Report]		 	 
(
@ShowAll	BIT,
@ValidationCategory int=0 -- Saeed 7791 07/09/2010 @ValidationCategory=1 for alert & @ValidationCategory=2 for report
)	
	
AS
BEGIN
		SELECT TT.TICKETID_PK , ISNULL(CONVERT(VARCHAR(20),TT.NonHMCFollowUpDate,101),'') NonHMCFollowUpDate,--Fahad 5722 04/03/2009 if date is null then show space
		dbo.fn_getnextbusinessday(GETDATE(),2) AS NextFollowUpDate,--Fahad 5722 04/03/2009 Allow user to add follow up date upto 2 businessday
		--CASE TT.NonHMCFollowUpDate WHEN NULL THEN 'N/A' ELSE TT.NonHMCFollowUpDate END AS NonHMCFollowUpDate,
		TT.FIRSTNAME, TT.LASTNAME, 
		ISNULL(TV.CASENUMASSIGNEDBYCOURT,'N/A')AS CAUSENUMBER, 
		ISNULL(tv.RefCaseNumber,'N/A')AS TICKETNUMBER,--Fahad 5722 04/09/2009 Use Refcaseno instead of Tickeid_pk for TicketNo
		TV.COURTDATEMAIN AS COURTDATE,
		TV.COURTVIOLATIONSTATUSIDMAIN,
		ISNULL(TV.COURTNUMBERMAIN, 'N/A') AS ROOMNUMBER,
		CVS.DESCRIPTION AS COURTSTATUS,
		ISNULL(C.SHORTNAME, 'N/A') AS CRT_LOCATION
		FROM TBLTICKETS TT INNER JOIN 
		TBLTICKETSVIOLATIONS TV ON TT.TICKETID_PK = TV.TICKETID_PK INNER JOIN 
		--Yasir Kamal 6727 10/09/2009 remove left outer join and apply isnull check
		TBLCOURTVIOLATIONSTATUS CVS ON CVS.COURTVIOLATIONSTATUSID = TV.COURTVIOLATIONSTATUSIDMAIN 
		LEFT OUTER JOIN TBLCOURTS C ON TV.COURTID = C.COURTID
		WHERE 
		--COURTVIOLATIONSTATUSIDMAIN IN(3,116,32)--Fahad 5722 04/09/2008 Filter applied on CategoryId instead of CourtViolationStatusId 
		--Fahad 10378 01/09/2013 HMC-W alos excluded along other HMC Courts.
		 ISNULL(TV.COURTID,0) NOT IN ('3001','3002', '3003','3075')
		--Yasir Kamal 6727 10/09/2009 Only Traffic cases. 
		AND ISNULL(tt.CaseTypeId,0) = 1
		AND CVS.CategoryID=2
		AND DATEDIFF(DAY,'09/01/2008',ISNULL(TV.COURTDATEMAIN,'1/1/1900')) > 0
		--Yasir Kamal 6501 09/01/2009  remove cases if Lor Confirmation Type Document is Uploaded. 
		and ((ISNULL(TV.CoveringFirmID,3000) <> 3043) or (ISNULL(TV.CoveringFirmID,3000) = 3043 AND NOT EXISTS (SELECT tsb.TicketID FROM tblScanBook tsb WHERE tsb.TicketID = tt.TicketID_PK AND tsb.DocTypeID = 18)))
		AND (
			(@ShowAll=1 OR @ValidationCategory=2)	-- Saeed 7791 07/09/2010 display all records if showAll=1 or validation category is 'Report'
			OR
			((@ShowAll=0 OR @ValidationCategory=1) AND DATEDIFF(dd,ISNULL(tt.NonHMCFollowUpDate,'01/01/1900'),GETDATE())>=0)	-- Saeed 7791 07/09/2010 display  if showAll=0 or validation category is 'Alert' then display records which has 'follow up date' null,past or today.
		) --Fahad 5722 04/03/2009 when user not checked all records then past and present records will show
		AND ISNULL(TT.ActiveFlag,0) = 1
		ORDER BY tt.NonHMCFollowUpDate ASC, tv.TicketID_PK -- Adil Aleem 5857 05/15/2009 Include Ticket ID in order by clause 
	
END

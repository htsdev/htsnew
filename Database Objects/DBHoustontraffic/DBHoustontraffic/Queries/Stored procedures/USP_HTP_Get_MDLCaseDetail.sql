﻿ USE [TrafficTickets]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
/****** 
Create by		: Zeeshan Haider
Created Date	: 10/05/2012
Task			: 10437

Business Logic : 
			This procedure retrieve MDL Case detail.
			
List of Parameters:	
			@CaseNumber	MDL Case Number
			
USP_HTP_InsertUpdate_MDLCase 2197, '10/02/2012', 3991, 0			
******/

CREATE PROCEDURE [dbo].[USP_HTP_Get_MDLCaseDetail]
	@CaseNumber BIGINT
AS
BEGIN
    SELECT tmd.*
    FROM   tblMDLCaseDetail tmd
           INNER JOIN tblMDLCases tm
                ON  tm.CaseNumber = tmd.MDL_CASENUMBER
    WHERE  tmd.MDL_CASENUMBER = @CaseNumber
           AND tm.isActive = 1
           AND DATEDIFF(DAY, tmd.INSERT_DATE, GETDATE()) = 0
END
GO

GRANT EXECUTE ON [dbo].[USP_HTP_Get_MDLCaseDetail] TO dbr_webuser
GO
--****************************************************************************
--Created By   : Afaq Ahmed
--Creation Data: 11/02/2010
--Task ID :8213
--Parameter List : 
--@catId : Category Id of the service ticket.
--@ticketId: TicketId.
--Return Column: N/A
--Business Logic : This porcedure return the count of service ticket against the provided 
--				   Service ticket category and ticketId.
--****************************************************************************

ALTER PROCEDURE Usp_Htp_CheckServiceTicket
@catId  INT,
@ticketId  INT 
AS

SELECT COUNT(*) FROM tblTicketsFlag ttf WHERE ttf.ServiceTicketCategory=@catId AND ttf.TicketID_PK = @ticketId  AND ttf.ContinuanceStatus < 100

GO

 GRANT EXECUTE ON Usp_Htp_CheckServiceTicket TO webuser_hts
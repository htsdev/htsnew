SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Update_Continuance_date]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Update_Continuance_date]
GO

  
  
  
Create Procedure [dbo].[USP_HTS_Update_Continuance_date]  
(  
  
@TicketID int,  
@ContinuanceDate datetime,  
@ContinuanceStatus int,  
@EmpID int  
  
)  
  
as  
  
update tblticketsflag        
Set         
ContinuanceDate = @ContinuanceDate ,        
ContinuanceStatus = @ContinuanceStatus        
where ticketid_pk = @TicketID and flagid = 9         
and RecDate = ( Select max(Recdate) from tblticketsflag where ticketid_pk = @TicketID and flagid = 9)     
  
insert into tblticketsnotes(ticketid,subject,employeeid,Notes) values(@TicketID,'Continuance Date Changed' ,@empid,null)  
  
  
  

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_WebScan_GetScanType]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_WebScan_GetScanType]
GO


CREATE procedure [dbo].[usp_WebScan_GetScanType]

@BatchID as int 

as

select 
	b.scantype,
	convert(varchar(12),b.scandate,101) as scandate
from tbl_WebScan_Batch b

where 
	b.batchID=@BatchID




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


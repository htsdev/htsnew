﻿USE TrafficTickets
GO


 /****** 
Created by:		Zeeshan Haider
Task ID:		10595
Date:			01/02/2012
Business Logic:	The procedure is used by LMS application to get the number of printable letter
				count for the AMC Jury 10 Day Before letter for the selected date range.
				
List of Parameters:
	@startdate:	Starting list date for Arraignment letter.
	@enddate:	Ending list date for Arraignment letter.
	@courtid:	Court id of the selected court category if printing for individual court hosue.
	@bondflag:	Flag if printing bond letters else appearance letter.
	@catnum:	Category number of the selected letter type, if printing for 
				all courts of the selected court category

List of Columns:	
	Total_Count:		Total number of letters exists for the selected date range.
	Good_Count:			Total number of letters that have valid addresses
	Bad_Count:			Total Number of letter that have undeliverable addresses
	DonotMail_Count:	Total number of letter for addresses that had been marked to stop sending letters.
******/

CREATE PROCEDURE [dbo].[usp_Mailer_Get_Austin_JuryWeekB4]
	(
	@startdate datetime,
	@enddate datetime,
	@courtid int = 6,
	@bondflag tinyint = 0,
	@catnum int=6
	)
AS
SET NOCOUNT ON; 

-- DECLARING LOCAL VARIABLES FOR FILTERS...
DECLARE
	@officernum varchar(50),
	@officeropr varchar(50),
	@tikcetnumberopr varchar(50),
	@ticket_no varchar(50),
	@zipcode varchar(50),
	@zipcodeopr varchar(50),
	@fineamount money,
	@fineamountOpr varchar(50),
	@fineamountRelop varchar(50),
	@singleviolation money,
	@singlevoilationOpr varchar(50),
	@singleviolationrelop varchar(50),
	@doubleviolation money,
	@doublevoilationOpr varchar(50),
	@doubleviolationrelop varchar(50),
	@count_Letters int,
	@violadesc varchar(500),
	@violaop varchar(50),
	@sqlquery varchar(max),
	@lettertype int

-- INITIALIZING THE LOCAL VARIABLE FOR DYNAMIC SQL        
SET @sqlquery = ''
SET @lettertype = 141

-- GETTING DIFFERENT FILTER TYPES AND THIER VALUES IN THE LOCAL VARIABLES
-- IF ASSOCIACTED WITH THE LETTER.......
SELECT
 	@officernum=officernum,                                                    
	@officeropr=oficernumOpr,                                                    
	@tikcetnumberopr=tikcetnumberopr,                                                    
	@ticket_no=ticket_no,                                                    
	@zipcode=zipcode,                                                    
	@zipcodeopr=zipcodeLikeopr,                                            
	@singleviolation =singleviolation,                                               
	@singlevoilationOpr =singlevoilationOpr,                                                    
	@singleviolationrelop =singleviolationrelop,                                                    
	@doubleviolation = doubleviolation,                                                    
	@doublevoilationOpr=doubleviolationOpr,                                                            
	@doubleviolationrelop=doubleviolationrelop,                                  
	@violadesc=violationdescription,                                  
	@violaop=violationdescriptionOpr ,                              
	@fineamount=fineamount ,                 
	@fineamountOpr=fineamountOpr ,                                                 
	@fineamountRelop=fineamountRelop		
FROM
 	tblmailer_letters_to_sendfilters                               
where 	courtcategorynum=@catnum  
and lettertype = @lettertype                                                  
					
-- MAIN QUERY....
-- COURT DATE WISE NUMBER OF DISTINCT NON-CLIENTS
-- FOR THE SELECTED DATE RANGE ONLY ARRAIGNMENT (PRIMARY STATUS) CASES
-- INSERTING VALUES IN A TEMP TABLE....
set @sqlquery=@sqlquery+'
Select distinct tva.recordid, 
	 convert(datetime, convert(varchar(10), tva.courtdate  ,101)) as mdate, 
	 flag1 = isnull(ta.Flag1,''N'') ,                                                    
	 isnull(ta.donotmailflag,0) as donotmailflag,                                                    	
	 count(tva.ViolationNumber_PK) as ViolCount,                                                    
	 isnull(sum(isnull(tva.fineamount,0)),0) as Total_Fineamount
into #temp                                                          
FROM dallastraffictickets.dbo.tblTicketsViolationsArchive TVA WITH(NOLOCK)                      
INNER JOIN dallastraffictickets.dbo.tblTicketsArchive TA WITH(NOLOCK) ON TVA.recordid= TA.recordid
-- FOR THE SELECTED DATE RANGE ONLY...
where datediff(day, tva.courtdate, ''' + convert(varchar(10),@startDate,101)  + ''' ) <= 0
and datediff(day, tva.courtdate, ''' + convert(varchar(10),@endDate , 101) + ''' ) >= 0

-- LAST NAME SHOULD NOT BE EMPTY..
and isnull(ta.lastname,'''') <> ''''

-- FIRST NAME SHOULD NOT BE EMPTY
and isnull(ta.firstname,'''') <> ''''

-- NOT MOVED IN PAST 48 MONTHS (ACCUZIP PROCESSED)...
and isnull(ta.ncoa48flag,0) = 0    

-- EXCLUDE V TICKETS...
and charindex(''v'',tva.ticketnumber_pk)<> 1    

-- ONLY FUTURE JUDGE & JURY TRIALS
and tva.violationstatusid in (26, 103) and datediff(day, getdate(), tva.courtdate) > 0

-- EXCLUDE BOND CASES....
and datediff(day,isnull(tva.violationbonddate,''1/1/1900''),''1/1/1900'') = 0

-- Exclude Disposed Violations
and tva.violationstatusid <> 80

'

-- IF PRINTING FOR ALL COURTS OF THE SELECTED COURT CATEGORY...  
if(@courtid = @CATNUM)
	set @sqlquery=@sqlquery+'
	and ta.courtid In (Select courtid from dallastraffictickets.dbo.tblcourts where courtcategorynum ='+convert(varchar,@catnum ) +')'

-- ELSE PRINTING FOR THE INDIVIDUAL SELECTED COURT OF THE CATEGORY....
else
	set @sqlquery=@sqlquery+'
	and ta.courtid=' + convert(varchar,@courtid)

-- LMS FILTERS SECTION:
-----------------------------------------------------------------------------------------------------------------------------------------------------
-- IF THERE IS ANY FILTER SPECIFIED TO INCLUDE/EXCLUDE ONLY PARTICULAR OFFICER NUMBERS....
if(@officernum<>'')
	set @sqlquery =@sqlquery + '
	and  ta.officerNumber_Fk ' +@officeropr  +'('+   @officernum+')'

-- IF THERE IS ANY FILTER DEFINED FOR INCLUDE/EXCLUDE VIOLATION DESCRIPTIONS...                                
if(@ticket_no<>'')
	set @sqlquery=@sqlquery + '
	and ('+  dbo.Get_String_Concat_With_op(''+@ticket_no+'' , 'tva.TicketNumber_PK' ,+ ''+ @tikcetnumberopr) +')'
                                  
-- ZIP CODE FILTER......
-- INCLUDE/EXCLUDE THE SPECIFIED ZIP CODES.....
if(@zipcode<>'')
	set @sqlquery=@sqlquery+ '
	and  ('+  dbo.Get_String_Concat_With_op(''+@zipcode+'' , 'left(ta.zipcode,2)' ,+ ''+ @zipcodeopr) +')'
                               
-- FINE AMOUNT FILTER.....
-- INCLUDE ONLY THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT....
if(@fineamount<>0 and @fineamountOpr <> 'not')
set @sqlquery =@sqlquery+ '
      and tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'

-- EXCLUDE THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT.....
if(@fineamount<>0 and @fineamountOpr = 'not')                                
set @sqlquery =@sqlquery+ '
      and   not tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'

-----------------------------------------------------------------------------------------------------------------------------------------------------
-- EXCLUDE THE LETTERS THAT HAVE BEEN ALREADY PRINTED....
SET
 @sqlquery=@sqlquery +'
 group by                                                     
 convert(datetime, convert(varchar(10), tva.courtdate  ,101)),
 ta.flag1,                                                    
 ta.donotmailflag,                                                    
 tva.recordid
 having  1=1
'

-- SINGLE VIOLATION FINE AMOUNT FILTER....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE SINGLE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT
if(@singleviolation<>0 and @doubleviolation=0)
	set @sqlquery =@sqlquery+ '
	and (  '+ @singlevoilationOpr+  '(isnull(sum(isnull(tva.fineamount,0)),0) '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and count(tva.ViolationNumber_PK)=1))
    '
-- DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...
if(@doubleviolation<>0 and @singleviolation=0 )                                
	set @sqlquery =@sqlquery + '
	and ('+ @doublevoilationOpr+  '(isnull(sum(isnull(tva.fineamount,0)),0) '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ')and count(tva.ViolationNumber_PK)=2) )
    '
-- BOTH SINGLE & DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE AND SINGLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...
if(@doubleviolation<>0 and @singleviolation<>0)                                
	set @sqlquery =@sqlquery+ '
	and ('+ @singlevoilationOpr+  '  (isnull(sum(isnull(tva.fineamount,0)),0) '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and count(tva.ViolationNumber_PK)=1))
    and ('+ @doublevoilationOpr+  '  (isnull(sum(isnull(tva.fineamount,0)),0) '+convert (varchar (10),@doubleviolationrelop) +'convert(Money,'+ convert(varchar(10),@doubleviolation) + ') and count(tva.ViolationNumber_PK)=2))
    '
-- GETTING THE RESULT SET IN TEMP TABLE.....
-- GET AND SEND MAILER DIFFERENCES SOLVED...
set @sqlquery=@sqlquery+                                       
'Select distinct a.recordid, a.ViolCount, a.Total_Fineamount,  a.mDate , a.Flag1, a.donotmailflag 
into #temp1  from #temp a, dallastraffictickets.dbo.tblticketsviolationsarchive tva WITH(NOLOCK) where a.recordid = tva.recordid
and tva.violationstatusid in (26 , 103) and datediff(day, getdate(), tva.courtdate) > 0
'

-- VIOLATION DESCRIPTION FILTER.....
-- EXCLUDE THE LETTER IF SPECIFIED VIOLATION IS THE ONLY VIOLATION IN THE LETTER....
IF (@violadesc <> '')
BEGIN
	-- NOT LIKE FILTER.......
	if(charindex('not',@violaop)<> 0 )
		set @sqlquery=@sqlquery+' 
		and	 not ( a.violcount =1  and ('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ 'like')+'))'           

	-- INCLUDE ONLY THOSE LETTER THAT SINGLE VIOLATION AND MATCHES THE SPECIFIED VIOLATION DESCRIPTION...
	-- LIKE FILTER.......
	if(charindex('not',@violaop) =  0 )              
		set @sqlquery=@sqlquery+' 
		and	 ( a.violcount =1  and ('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ 'like')+'))'
END

-- EXCLUDE THE LETTERS THAT HAVE BEEN ALREADY PRINTED......
set @sqlquery =@sqlquery + '
select recordid into #tempvar from tblletternotes where  lettertype = '+convert(Varchar, @lettertype)+'
-- EXCLUDE AUSTIN REGULAR PRINTED IN PAST 5 DAYS..
union  
select recordid from dbo.tblletternotes where lettertype in (139, 140, 142)
and datediff(day, recordloaddate, dbo.fn_getprevbusinessday(GETDATE(),5))<=0    

select T.recordid into #tempvarexist from #temp1 T , #tempvar V
where T.recordid = V.recordid
-- GET AND SEND MAILER DIFFERENCES SOLVED
delete from #temp1 where recordid in (select recordid from #tempvarexist)
'

-- SUMMARIZING THE TOTAL NUMBER OF LETTERS......
-- GETTING LIST DATE WISE TOTAL NUMBER OF LETTERS....
set @sqlquery=@sqlquery+'
-- EXCLUDE CLIENT CASES
DELETE from #temp1 where recordid in (
select v.recordid from
dallastraffictickets.dbo.tblticketsviolations v WITH(NOLOCK) inner join dallastraffictickets.dbo.tbltickets t WITH(NOLOCK) 
on t.ticketid_pk = v.ticketid_pk and t.activeflag = 1 
inner join #temp1 a on a.recordid = v.recordid
)

-- NO ATTORNEY IS ASSIGNED TO THE CASE....
DELETE FROM #temp1 where recordid in (
	select v.recordid from dallastraffictickets.dbo.tblticketsviolationsarchive v WITH(NOLOCK) 
	inner join dallastraffictickets.dbo.Attorneys_CaseAssociation a on a.recordid = v.recordid
	inner join #temp1 b on b.recordid = v.recordid
	)

SELECT count(distinct recordid) as Total_Count, mDate
into #temp2
from #temp1
group by mDate

-- GETTTING LIST DATE WISE TOTAL NUMBER OF PRINTABLE LETTERS...
Select count(distinct recordid) as Good_Count, mDate                                                                                              
into #temp3                                                
from #temp1                                                    
where Flag1 in (''Y'',''D'',''S'')
and donotmailflag =  0                                                 
group by mDate
                                                    
-- GETTING LIST DATE WISE TOTAL NUMBER OF NON-PRINTABLE LETTERS
Select count(distinct recordid) as Bad_Count, mDate                                                  
into #temp4                                                    
from #temp1                                                
where Flag1  not in (''Y'',''D'',''S'')                                                    
and donotmailflag = 0                                                 
group by mDate

-- GETTING LIST DATE WISE TOTAL NUMBER OF LETTERS THAT MARKED
-- AS BLOCKED ADDRESSES.....
SELECT count(distinct recordid) as DonotMail_Count, mDate
INTO #temp5
FROM #temp1
WHERE donotmailflag = 1
--and flag1  in  (''Y'',''D'',''S'')                                                    
GROUP BY mDate

-- OUTPUTTING THE REQURIED INFORMATION........
SELECT  listdate = #temp2.mDate,
        Total_Count,
        isnull(Good_Count,0) Good_Count,
        isnull(Bad_Count,0)Bad_Count,
        isnull(DonotMail_Count,0)DonotMail_Count
FROM #Temp2 left outer join #temp3 
ON #temp2.mDate = #temp3.mDate
                                                   
left outer join #Temp4
on #temp2.mDate = #Temp4.mDate
  
left outer join #Temp5
on #temp2.mDate = #Temp5.mDate                                                 
  
where Good_Count > 0              
order by #temp2.mDate desc                                                     
          

-- DROPPING THE TEMPORARY TABLES USED ABOVE.....
drop table #temp                                                
drop table #temp1                                                
drop table #temp2                                                    
drop table #temp3                                                
drop table #temp4
drop table #temp5
'
-- print @sqlquery
-- EXECUTING THE DYNAMIC SQL QUERY...
EXEC(@sqlquery)


GO
GRANT EXECUTE ON [dbo].[usp_Mailer_Get_Austin_JuryWeekB4] TO dbr_webuser


-- exec USP_HTS_Update_MailerID_JOB
alter procedure [dbo].[USP_HTS_Update_MailerID_JOB]

as 

SET NOCOUNT ON 

declare @rec int, @idx int, @courtid int, @ticketid int

CREATE TABLE #tickets(
	rowid int identity(1,1),
	ticketid int  NOT NULL,
	courtid [int] NULL,
 CONSTRAINT [PK_#tickets] PRIMARY KEY CLUSTERED 
(
	rowid ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]


-- HOUSTON & JIMS...
insert into #tickets (ticketid , courtid)
select distinct  t.ticketid_pk, isnull(v.courtid,0) as courtid
from tbltickets t inner join tblticketsviolations v
on t.ticketid_pk = v.ticketid_pk
where isnull(t.mailerid, 0) = 0
and datediff(day, t.recdate,'6/1/2007') <= 0
order by t.ticketid_pk, [courtid]


select @idx = 1, @rec = count(rowid) from #tickets

while @idx <=  @rec
begin
	select @ticketid = ticketid, @courtid = courtid 
	from #tickets where rowid = @idx

	if @courtid = 3037
		-- jims......
		EXEC dbo.USP_HTS_Update_Mailer_id @TicketID, 3992, 3
	
	-- Civil...
	else if @courtid = 3071
		EXEC dbo.USP_HTS_Update_Mailer_id @TicketID, 3992, 5

	else
		-- houston....
		EXEC dbo.USP_HTS_Update_Mailer_id @TicketID, 3992, 1
	
	set @idx = @idx + 1
end

-- DALLAS
truncate table #tickets

insert into #tickets (ticketid , courtid)
select distinct  t.ticketid_pk, isnull(v.courtid,0) as courtid
from dallastraffictickets.dbo.tbltickets t inner join dallastraffictickets.dbo.tblticketsviolations  v
on t.ticketid_pk = v.ticketid_pk
where isnull(t.mailerid, 0) = 0
and datediff(day, t.recdate,'6/1/2007') <= 0
order by t.ticketid_pk, [courtid]

select @idx = 1, @rec = count(rowid) from #tickets

while @idx <=  @rec
begin
	select @ticketid = ticketid, @courtid = courtid 
	from #tickets where rowid = @idx

	-- dallas ....
	EXEC dallastraffictickets.dbo.USP_HTS_Update_Mailer_id @TicketID, 3992, 2	

	set @idx = @idx + 1
end

drop table #tickets
GO

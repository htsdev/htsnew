USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTS_GET_ALL_FLAGS]    Script Date: 08/13/2008 17:11:01 ******/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[USP_HTS_GET_ALL_FLAGS]  
AS  
select * from tbleventflags where FlagID_pk <> 0 and ActiveStatus = 1 
order by Description asc -- Sabir Khan 4617 08/28/08


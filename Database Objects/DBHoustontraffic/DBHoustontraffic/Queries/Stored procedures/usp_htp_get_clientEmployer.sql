﻿  /*  
 * Task : 5966   
 * Created By : Waqas Javed  
 * Business Logic : This procedure is used to get client employer  
 * Columns:  
 * Employer  
 */  

--Waqas 6666 10/22/2009 With search criteria   
alter  procedure dbo.usp_htp_get_clientEmployer  
@criteria varchar(max) =''
as      
      
begin      
 --Waqas 6894 10/30/2009 calling from table employer
 select DISTINCT TOP 15 Employer as Employer from Employer      
 where Employer like @criteria + '%'
 and Employer is not null and LEN(Employer) > 0
 order by Employer      
end 
   
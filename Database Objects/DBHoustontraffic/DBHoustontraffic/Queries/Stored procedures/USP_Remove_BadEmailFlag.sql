set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

/********
Altered by:			Babar Ahmed 9681 11/10/2011 Used to update the Reviewed Email status in HTP and DTP
Business Logic:		The procedure is used by HTP and DTP application to update the Reviewed Email status for HTP and DTP applications which are Bad E-mail 
					Addresses.
*******/

ALTER PROCEDURE [dbo].[USP_Remove_BadEmailFlag]

AS
------------------------------------------
--Houston Section
------------------------------------------
DECLARE @table AS TABLE (RowID INT IDENTITY(1, 1) NOT NULL, ticketid INT NOT NULL)

INSERT INTO @table
  (
    ticketid
  )
SELECT DISTINCT ticketid
FROM   tblticketsnotes ttn
       INNER JOIN tblticketsflag ttf
            ON  ttf.ticketid_pk = ttn.ticketid
WHERE  (
           ttn.subject = 'Trial Notification email confirmation received'
           OR ttn.subject = 'Client Read Thank you email'
       )
       AND ttf.flagid = 34 -- Bad E-mail

DECLARE @rowno INT
SET @rowno = 1

DECLARE @count INT
SELECT @count = COUNT(*)
FROM   @table
           
WHILE @rowno <= @count
BEGIN
    DECLARE @ticketid INT
    SELECT @ticketid = ticketid
    FROM   @table
    WHERE  RowID = @rowno
    
    DELETE 
    FROM   tblticketsflag 
    WHERE  ticketid_pk = @ticketid
           AND flagid = 34                                                                
    
    DECLARE @desc VARCHAR(100)                                                   
    
    SELECT @desc = DESCRIPTION
    FROM   tbleventflags
    WHERE  flagid_pk = 34                                                    
    
    INSERT INTO tblticketsnotes
      (
        ticketid,
        SUBJECT,
        employeeid,
        Notes
      )
    VALUES
      (
        @ticketid,
        'Flag -' + @desc + ' - Removed',
        3992,
        NULL
      )
    SET @rowno = @rowno + 1
    
    -- Babar Ahmad 9676 04/11/2011 Marked record as reviewed email.
    UPDATE tbltickets SET ReviewedEmailStatus = 1 WHERE TicketID_PK = @ticketid
    
    INSERT INTO tblticketsnotes
      (
        ticketid,
        SUBJECT,
        employeeid,
        Notes
      )
    VALUES
      (
        @ticketid,
        'Email Address reviewed by SYSTEM',
        3992,
        NULL
      )
END 

---------------------------------------------------
--Dallas Section
---------------------------------------------------          
DECLARE @table1 AS TABLE (RowID INT IDENTITY(1, 1) NOT NULL, ticketid INT NOT NULL)

INSERT INTO @table1
  (
    ticketid
  )
SELECT DISTINCT ticketid
FROM   DallasTrafficTickets.dbo.tblticketsnotes ttn
       INNER JOIN DallasTrafficTickets.dbo.tblticketsflag ttf
            ON  ttf.ticketid_pk = ttn.ticketid
WHERE  (
           ttn.subject = 'Trial Notification email confirmation received'
           OR ttn.subject = 'Client Read Thank you email'
       )
       AND ttf.flagid = 38

DECLARE @rowno1 INT
SET @rowno1 = 1

DECLARE @count1 INT
SELECT @count1 = COUNT(*)
FROM   @table1
           
WHILE @rowno1 <= @count1
BEGIN
    DECLARE @ticketid1 INT
    SELECT @ticketid1 = ticketid
    FROM   @table1
    WHERE  RowID = @rowno1
    
    DELETE 
    FROM   DallasTrafficTickets.dbo.tblticketsflag
    WHERE  ticketid_pk = @ticketid1
           AND flagid = 38                                                                
    
    DECLARE @desc1 VARCHAR(100)                                                   
    
    SELECT @desc1 = DESCRIPTION
    FROM   DallasTrafficTickets.dbo.tbleventflags
    WHERE  flagid_pk = 38                                                    
    
    INSERT INTO DallasTrafficTickets.dbo.tblticketsnotes
      (
        ticketid,
        SUBJECT,
        employeeid,
        Notes
      )
    VALUES
      (
        @ticketid1,
        'Flag -' + @desc1 + ' - Removed',
        3992,
        NULL
      )
    SET @rowno1 = @rowno1 + 1
     -- Babar Ahmad 9676 04/11/2011 Marked record as reviewed email.
    UPDATE DallasTrafficTickets.dbo.tblTickets SET ReviewedEmailStatus = 1 WHERE TicketID_PK = @ticketid1
END 

          
          

GO
GRANT EXECUTE ON [dbo].[USP_Remove_BadEmailFlag] TO dbr_webuser
GO 
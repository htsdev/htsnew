USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTP_GET_Crash_TodayCreatedRecords]    Script Date: 08/27/2013 00:57:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** 
Created by		:		Sabir Khan Miani
Task ID			:		Not yet created 
Business Logic	:		The Procedure is used to get updated Texas Department of Transportation Information. 					
				
List of Parameters:	
	@downloaddate			 	
*******/

-- Connections: Client (F,L,YR,Zip), Client (F,L,YR), Client (L,Zip), Quote (F,L,YR,Zip), Quote (F,L,YR), Quote (L,Zip), Non Client (F,L,YR,Zip)
-- Non Client (F,L,YR), Non Client (L,Zip), All Clients, All Quote Clients, All Non Clients 

-- EXEC [dbo].[USP_HTP_GET_Crash_TodayCreatedRecords]
ALTER PROCEDURE [dbo].[USP_HTP_GET_Crash_TodayCreatedRecords]
	
AS
BEGIN
SET NOCOUNT ON;
SELECT DISTINCT CONVERT(VARCHAR(10), TP.CrashDate, 101) AS CrashDate,CONVERT(VARCHAR(10), TP.InsertDate, 101) AS DownloadDate,TPO.IncidentNumber,
	       TPO.OperatorLastName AS LastName,TPO.OperatorFirstName AS FirstName,ISNULL(TP.Prsn_Age, '') AS Age,(CASE WHEN ISNULL(TP.Prsn_Age, 0) = 0 THEN ''
	       ELSE DATEPART(yy, TP.InsertDate) - ISNULL(TP.Prsn_Age, 0) END) AS DOB,UF.Tot_Injry_Cnt,UF.Poss_Injry_Cnt,TC.Thousand_Damage_Fl,TC.Crash_Sev_ID,UF.Veh_Dmag_Scl_1_ID,UF.Veh_Dmag_Scl_2_ID,TC.Medical_Advisory_Fl,
	       TC.Crash_Fatal_Fl,TC.Death_Cnt,TC.Cmv_Involv_Fl,UF.Veh_CMV_Fl,UF.Drvr_Lic_Cls_ID,UF.VIN,UF.Drvr_Zip,UF.Ownr_Zip,TC.Crash_ID,lkp.INSURANCE_PROOF_DESC, TC.Case_ID AS CaseId, TC.CIDProcessLastDate AS RecordCreation,TC.RecordID, g.GNDR_DESC AS Gender
	       INTO #tempCrash 
FROM   Crash.dbo.tblTexasTransportationPersonOperators TPO INNER JOIN Crash.dbo.tbl_Person TP ON  TPO.CrashID = TP.Crash_ID
	       INNER JOIN Crash.dbo.tbl_Crash TC ON  TC.Crash_ID = TP.Crash_ID AND TC.Case_ID = TPO.IncidentNumber
	       LEFT OUTER JOIN Crash.dbo.tbl_Unit UF ON  UF.Crash_ID = TC.Crash_ID LEFT OUTER JOIN Crash.dbo.INSURANCE_PROOF_LKP LKP ON  UF.Fin_Resp_Proof_ID = LKP.INSURANCE_PROOF_ID
	       LEFT OUTER JOIN Crash.dbo.GNDR_LKP g ON g.GNDR_ID = tp.Prsn_Gndr_ID
WHERE  ISNULL(TPO.OperatorFirstName, '') <> '' AND ISNULL(TPO.OperatorLastName, '') <> '' AND DATEDIFF(DAY, TC.CIDProcessLastDate, GETDATE()) = 0 AND isnull(TC.Rpt_City_ID,0) = 208

CREATE NONCLUSTERED INDEX idx_LastName ON #tempCrash(LastName)
CREATE NONCLUSTERED INDEX idx_FirstName ON #tempCrash(FirstName)
CREATE NONCLUSTERED INDEX idx_DOB ON #tempCrash(DOB)
CREATE NONCLUSTERED INDEX idx_Drvr_Zip ON #tempCrash(Drvr_Zip)
CREATE NONCLUSTERED INDEX idx_CrashID ON #tempCrash(Crash_Id)
CREATE NONCLUSTERED INDEX idx_Recordid ON #tempCrash(Recordid)
CREATE NONCLUSTERED INDEX idx_IncidentNumber ON #tempCrash(IncidentNumber)

SELECT DISTINCT REPLACE(causenumber, ' ', '') AS causenumber,attorneyname INTO #eventTemp
FROM   loaderfilesarchive.dbo.tblEventExtractTemp WITH(NOLOCK)	    

CREATE NONCLUSTERED INDEX idx_CauseNumber ON #eventTemp(causenumber)

SELECT * INTO #tblTickets FROM tblTickets 
CREATE NONCLUSTERED INDEX idx_LastName ON #tblTickets(LastName)
CREATE NONCLUSTERED INDEX idx_FirstName ON #tblTickets(firstname)	

--Client (F, L, YR, G, Zip)
SELECT DISTINCT Cr.*,(CASE WHEN ISNULL(CT.CaseTypeName, '') <> '' THEN 'Client' ELSE 'N/A' END) AS [Status],CT.CaseTypeName,t.Midnum AS midnumber,
t.Contact1 AS Tel1,t.Contact2 AS Tel2,t.Contact3 AS Tel3,t.Zip AS ZipCode,tm.AttorneyName,'1' AS flag,'Client (F, L, YR, G, Zip)' AS connection,
t.TicketID_PK AS ticketid,(t.Address1 + ISNULL(t.Address2,'')) AS Address1,t.Email as EmailAddress INTO #tempFLYGZ 
FROM   #tempCrash Cr  
INNER JOIN #tblTickets t ON  t.lastname  = cr.lastname AND t.firstname = cr.firstname AND DATEPART(YEAR, T.DOB)  IN (Cr.DOB,Cr.DOB+1,Cr.DOB-1) AND left(t.Zip,5) = left(CR.Drvr_Zip,5) AND ISNULL(T.Activeflag, 0) = 1 AND Lower(t.gender) = LOWER(Cr.Gender) 
INNER JOIN tblticketsviolations tv ON tv.TicketID_PK = t.TicketID_PK
LEFT OUTER JOIN #eventTemp tm ON tm.causenumber = tv.casenumassignedbycourt
LEFT OUTER JOIN tblCourts C	ON  TV.CourtID = C.Courtid  
LEFT OUTER JOIN CaseType CT ON  C.CaseTypeID = Ct.CaseTypeId


	
--Client (F,L,YR,Zip)
SELECT DISTINCT Cr.*,(CASE WHEN ISNULL(CT.CaseTypeName, '') <> '' THEN 'Client' ELSE 'N/A' END) AS [Status],CT.CaseTypeName,t.Midnum AS midnumber,
t.Contact1 AS Tel1,t.Contact2 AS Tel2,t.Contact3 AS Tel3,t.Zip AS ZipCode,tm.AttorneyName,'1' AS flag,'Client (F,L,YR,Zip)' AS connection,
t.TicketID_PK AS ticketid,(t.Address1 + ISNULL(t.Address2,'')) AS Address1,t.Email as EmailAddress INTO #tempFLYZ 
FROM   #tempCrash Cr  
INNER JOIN #tblTickets t ON  t.lastname  = cr.lastname AND t.firstname = cr.firstname AND DATEPART(YEAR, T.DOB)  IN (Cr.DOB,Cr.DOB+1,Cr.DOB-1) AND left(t.Zip,5) = left(CR.Drvr_Zip,5) AND ISNULL(T.Activeflag, 0) = 1
INNER JOIN tblticketsviolations tv ON tv.TicketID_PK = t.TicketID_PK
LEFT OUTER JOIN #eventTemp tm ON tm.causenumber = tv.casenumassignedbycourt
LEFT OUTER JOIN tblCourts C	ON  TV.CourtID = C.Courtid  
LEFT OUTER JOIN CaseType CT ON  C.CaseTypeID = Ct.CaseTypeId

--Client (F,L,YR)
SELECT DISTINCT Cr.*,(CASE WHEN ISNULL(CT.CaseTypeName, '') <> '' THEN 'Client' ELSE 'N/A' END) AS [Status],CT.CaseTypeName,t.Midnum AS midnumber,
t.Contact1 AS Tel1,t.Contact2 AS Tel2,t.Contact3 AS Tel3,t.Zip AS ZipCode,tm.AttorneyName,'1' AS flag,'Client (F,L,YR)' AS connection,
t.TicketID_PK AS ticketid,(t.Address1 + ISNULL(t.Address2,'')) AS Address1,t.Email as EmailAddress INTO #tempFLY 
FROM   #tempCrash Cr  
INNER JOIN #tblTickets t ON  t.lastname  = cr.lastname AND t.firstname = cr.firstname AND DATEPART(YEAR, T.DOB)  IN (Cr.DOB,Cr.DOB+1,Cr.DOB-1) AND ISNULL(T.Activeflag, 0) = 1
INNER JOIN tblticketsviolations tv ON tv.TicketID_PK = t.TicketID_PK
LEFT OUTER JOIN #eventTemp tm ON tm.causenumber = tv.casenumassignedbycourt
LEFT OUTER JOIN tblCourts C	ON  TV.CourtID = C.Courtid  
LEFT OUTER JOIN CaseType CT ON  C.CaseTypeID = Ct.CaseTypeId


--Client (L,Zip)
SELECT DISTINCT Cr.*,(CASE WHEN ISNULL(CT.CaseTypeName, '') <> '' THEN 'Client' ELSE 'N/A' END) AS [Status],CT.CaseTypeName,t.Midnum AS midnumber,
t.Contact1 AS Tel1,t.Contact2 AS Tel2,t.Contact3 AS Tel3,t.Zip AS ZipCode,tm.AttorneyName,'1' AS flag,'Client (L,Zip)' AS connection,
t.TicketID_PK AS ticketid,(t.Address1 + ISNULL(t.Address2,'')) AS Address1,t.Email as EmailAddress INTO #tempLZ 
FROM   #tempCrash Cr  
INNER JOIN #tblTickets t ON  t.lastname  = cr.lastname AND left(t.Zip,5) = left(CR.Drvr_Zip,5) AND ISNULL(T.Activeflag, 0) = 1
INNER JOIN tblticketsviolations tv ON tv.TicketID_PK = t.TicketID_PK
LEFT OUTER JOIN #eventTemp tm ON tm.causenumber = tv.casenumassignedbycourt
LEFT OUTER JOIN tblCourts C	ON  TV.CourtID = C.Courtid  
LEFT OUTER JOIN CaseType CT ON  C.CaseTypeID = Ct.CaseTypeId




SELECT DISTINCT * into #tempClientfinal from #tempFLYGZ 
UNION ALL SELECT DISTINCT * FROM #tempFLYZ
UNION ALL SELECT DISTINCT * FROM #tempFLY
UNION ALL SELECT DISTINCT * FROM #tempLZ


SELECT MAX(t.ticketid_pk) AS ticketid,f.firstname,f.lastname,f.Tel1, f.Tel2, f.Tel3,f.DOB,f.age AS age INTO #tmp
FROM #tempClientfinal f INNER JOIN tbltickets t ON t.TicketID_PK = f.ticketid
GROUP BY f.firstname,f.lastname,f.Tel1, f.Tel2, f.Tel3,f.DOB,f.age


ALTER TABLE #tmp ADD CrashDate DATETIME,DownloadDate DATETIME,IncidentNumber VARCHAR(200),connection VARCHAR(200),CaseTypeName VARCHAR(200),AttorneyName VARCHAR(200), midnumber VARCHAR(200),ZipCode VARCHAR(200),Tot_Injry_Cnt VARCHAR(100), Poss_Injry_Cnt VARCHAR(100), 
Thousand_Damage_Fl VARCHAR(100), Crash_Sev_ID VARCHAR(100),Veh_Dmag_Scl_1_ID VARCHAR(100), Veh_Dmag_Scl_2_ID VARCHAR(100),Medical_Advisory_Fl VARCHAR(100),Crash_Fatal_Fl VARCHAR(100), Death_Cnt VARCHAR(100), Cmv_Involv_Fl VARCHAR(100), 
Veh_CMV_Fl VARCHAR(100),INSURANCE_PROOF_DESC VARCHAR(200),Drvr_Lic_Cls_ID VARCHAR(100),VIN VARCHAR(100), Drvr_Zip VARCHAR(100), Ownr_Zip VARCHAR(100),Crash_ID	VARCHAR(100),
RecordCreation DATETIME ,Address1 VARCHAR(300),EmailAddress VARCHAR(200)
	
UPDATE t
SET    t.CrashDate = tt.CrashDate,t.DownloadDate = tt.DownloadDate,t.IncidentNumber = tt.IncidentNumber,t.connection = tt.connection,t.CaseTypeName = tt.CaseTypeName,t.AttorneyName = tt.AttorneyName,t.midnumber = tt.midnumber,t.ZipCode = tt.ZipCode,
       t.Tot_Injry_Cnt = tt.Tot_Injry_Cnt,t.Poss_Injry_Cnt = tt.Poss_Injry_Cnt,t.Thousand_Damage_Fl = tt.Thousand_Damage_Fl,t.Crash_Sev_ID = tt.Crash_Sev_ID,t.Veh_Dmag_Scl_1_ID = tt.Veh_Dmag_Scl_1_ID,t.Veh_Dmag_Scl_2_ID = tt.Veh_Dmag_Scl_2_ID,
       t.Medical_Advisory_Fl = tt.Medical_Advisory_Fl,t.Crash_Fatal_Fl = tt.Crash_Fatal_Fl,t.Death_Cnt = tt.Death_Cnt,t.Cmv_Involv_Fl = tt.Cmv_Involv_Fl,t.Veh_CMV_Fl = tt.Veh_CMV_Fl,t.INSURANCE_PROOF_DESC = tt.INSURANCE_PROOF_DESC,
       t.Drvr_Lic_Cls_ID = tt.Drvr_Lic_Cls_ID,t.VIN = tt.VIN,t.Drvr_Zip = tt.Drvr_Zip,t.Ownr_Zip = tt.Ownr_Zip,t.Crash_ID = tt.Crash_ID,
	   t.RecordCreation = tt.RecordCreation,t.Address1 = tt.Address1,t.EmailAddress = tt.EmailAddress  FROM   #tmp t
       INNER JOIN #tempClientfinal tt  ON  ISNULL(tt.LastName, '') = ISNULL(t.LastName, '') AND ISNULL(tt.FirstName, '') = ISNULL(t.FirstName, '') AND ISNULL(tt.DOB, '') = ISNULL(t.DOB, '') AND ISNULL(tt.Tel1, '') = ISNULL(t.Tel1, '')
       AND ISNULL(tt.Tel2, '') = ISNULL(t.Tel2, '')  AND ISNULL(tt.Tel3, '') = ISNULL(t.Tel3, '')


SELECT DISTINCT f.ticketid,RecordCreation,CrashDate,DownloadDate, IncidentNumber,connection AS Connection,CaseTypeName AS dataset,AttorneyName AS AttyLink,
f.LastName,f.FirstName,f.Age,f.DOB AS [DOB(YR)],midnumber AS [Mid(X-ref)],f.Tel1,f.Tel2,f.Tel3,EmailAddress,Address1,ZipCode AS zip,Tot_Injry_Cnt,Poss_Injry_Cnt,
Thousand_Damage_Fl,Crash_Sev_ID,Veh_Dmag_Scl_1_ID,Veh_Dmag_Scl_2_ID,Medical_Advisory_Fl,Crash_Fatal_Fl,Death_Cnt,Cmv_Involv_Fl,
Veh_CMV_Fl,INSURANCE_PROOF_DESC,Drvr_Lic_Cls_ID,VIN,Drvr_Zip,Ownr_Zip,Crash_ID FROM   #tmp f 
ORDER BY f.RecordCreation DESC

DROP TABLE #eventTemp
drop table #tempClientfinal
drop table #tmp
drop table #tempCrash
drop table #tempFLYGZ
drop table #tempFLYZ
drop table #tempFLY
drop table #tempLZ
DROP TABLE #tblTickets
END

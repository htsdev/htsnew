USE [TrafficTickets]
GO
/******  
* Created By :	Sabir Khan.
* Create Date : 2/3/2011 4:22:15 AM
* Task ID :	8882
* Business Logic :  This procedure is used to insert new cases into Houston Traffic program and also update quote cases by moving into client cases.
* List of Parameter :
* @fileID : Path of the file.
* @EmployeeID:  Name of file.
* @Court:	Discription from user.
* @CourtNumber: ID of user whose inserting the file.
* @firstname:	Output Paramter.
* @lastname: Path of the file.
* @ClientBirthDate: Name of file.
* @CauseNumber:	Discription from user.
* @TicketNumber:	ID of user whose inserting the file.
* @OfficerLastName:	Output Paramter.
* @OfficerfirstName  : Path of the file.
* @CourtDate : Path of the file.
* @ClientDLNumber: Name of file.
* @DLType r:	Discription from user.
*       
******/
--USP_HTP_Update_OutsideAttorneyClientInfo 1,4123,'City of Houston Municipal Court ', '1','Bradley','T. Paris','1991-07-08 00:00:00','2077ABC0023737','Jury','124741696-0','K','Campbell','','','','','1/28/2011 12:00:00 AM','25971121','Class C'
ALTER PROCEDURE [dbo].[USP_HTP_Update_OutsideAttorneyClientInfo]
	@fileID INT,
	@EmployeeID INT,
	@Court VARCHAR(300),
	@CourtNumber VARCHAR(10),
	@firstname VARCHAR(100),
	@lastname VARCHAR(100),
	@ClientBirthDate SMALLDATETIME,
	@CauseNumber VARCHAR(100),
	@ViolationStatus VARCHAR(50),
	@TicketNumber VARCHAR(100),
	@OfficerLastName VARCHAR(100),
	@OfficerfirstName VARCHAR(100),	
	@CourtDate SMALLDATETIME,
	@ClientDLNumber VARCHAR(100),
	@DLType VARCHAR(100)
AS
BEGIN
    -- Variable Declaration
    DECLARE @SystemTicketNumber VARCHAR(100),
            @midnumber VARCHAR(20),
            @ZipCode VARCHAR(12),
            @Address VARCHAR(50),
            @TicketID INT,
            @ContactID INT,
            @AttorneyPhone VARCHAR(50),
            @AttorneyAbb VARCHAR(20),
            @firmID INT,
            @ViolationsStatusID INT,
            @OfficerNumber VARCHAR(100),
            @NewContactID INT,
            @FirmAddress1 VARCHAR(200),
            @FirmAddress2 VARCHAR(200),
            @FirmCity VARCHAR(100),
            @FirmState VARCHAR(50),
            @FirmZip VARCHAR(50),
            @TicketNumberFromNonClient VARCHAR(100),
            @empnm VARCHAR(20) -- Sabir Khan 9063 03/22/2011 getting Employee Abbrivation for time stamp in trail comments.
    set @TicketID = 0      
    
    -- Getting Attorney Phone number and firm ID
    SELECT @AttorneyPhone = f.Phone,
           @firmID = f.FirmID,@AttorneyAbb = f.FirmAbbreviation, @FirmAddress1 = ISNULL(f.[Address],''), @FirmAddress2 =  ISNULL(f.Address2,''), @firmCity = f.City, @FirmState = f.[State], @FirmZip = f.Zip, @empnm = u.Abbreviation
    FROM   tblFirm f
           INNER JOIN tblUsers u
                ON  u.FirmID = f.FirmID
    WHERE  u.EmployeeID = @EmployeeID
    
    --Getting Violation Status ID
    IF LTRIM(RTRIM(@ViolationStatus)) = 'Jury'
    SET @ViolationsStatusID = 26
    ELSE IF LTRIM(RTRIM(@ViolationStatus)) = 'Judge'
    SET @ViolationsStatusID = 103
    ELSE
    SELECT @ViolationsStatusID = CourtViolationStatusID
    FROM   tblCourtViolationStatus
    WHERE  [Description] LIKE LTRIM(RTRIM(@ViolationStatus)) 
    
    
    --IF Record exist as non client in the data base 
    IF EXISTS(
           SELECT *
           FROM   tblticketsviolationsarchive tva
                  INNER JOIN tblticketsarchive ta
                       ON  ta.RecordID = tva.RecordID
           WHERE  tva.CauseNumber = LTRIM(RTRIM(@CauseNumber))
                  AND ta.Clientflag = 0
       )
    BEGIN
    	
    	
    	
        -- Getting required values in variables and also migrate profile
        SELECT @midnumber = ta.MidNumber,
               @ZipCode = ta.ZipCode,
               @Address = ta.Address1,
               @TicketNumberFromNonClient = tva.TicketNumber_PK
        FROM   tblticketsarchive ta
               INNER JOIN tblticketsviolationsarchive tva
                    ON  tva.RecordID = ta.recordID
        WHERE  tva.CauseNumber = LTRIM(RTRIM(@CauseNumber))
               AND ta.Clientflag = 0
               
             

     
        -- Migrate Profile
        EXEC USP_HTS_Insert_CaseInfoFromNonClients_Ver_2 
			@TicketNumberFromNonClient,
             @midnumber,                         
             @Address,
             @ZipCode,
             3992,
             0,
             1,
             ''
             
    
    END 
    
    --If Record is already exist in Data base as a Quote client
    IF EXISTS(
           SELECT *
           FROM   tblticketsviolations tv
                  INNER JOIN tbltickets t
                       ON  t.TicketID_PK = tv.TicketID_PK
           WHERE  RTRIM(LTRIM(tv.casenumassignedbycourt)) = RTRIM(LTRIM(@CauseNumber))
                  AND ISNULL(t.Activeflag, 0) = 0
       )
    BEGIN    	
    	
        --Getting ticket id			
        SELECT @TicketID = t.TicketID_PK
        FROM   tbltickets t
               INNER JOIN tblticketsviolations tv
                    ON  tv.TicketID_PK = t.TicketID_PK
        WHERE  RTRIM(LTRIM(tv.casenumassignedbycourt)) = RTRIM(LTRIM(@CauseNumber))
               AND ISNULL(t.Activeflag, 0) = 0
        
        --Getting Attorney phone number.
        IF EXISTS(
               SELECT *
               FROM   tbltickets
               WHERE  TicketID_PK = @TicketID
                      AND LEN(ISNULL(Contact1, '')) = 0
                      AND LEN(ISNULL(Contact2, '')) = 0
                      AND LEN(ISNULL(Contact3, '')) = 0
           )
        BEGIN
            -- Update contact number...
            UPDATE tbltickets
            SET    Contact1 = @AttorneyPhone,
                   ContactType1 = 100,
                   EmployeeID = 3992
            WHERE  TicketID_PK = @TicketID
        END
        --Update Client Information informations....
        UPDATE tbltickets
        SET    firstname = @firstname,
			   lastname = @lastname,
			   LanguageSpeak = 'SPANISH',
               IsUnemployed = 1,
               OccupationID = 3669,
               BondFlag = 0,
               VehicleType = CASE @DLType
                                  WHEN 'Class C' THEN 3
                                  WHEN 'CDL' THEN 1
                             END,
               CDLFlag = CASE @DLType
                              WHEN 'Class C' THEN 0
                              WHEN 'CDL' THEN 1
                         END,
               AccidentFlag = 0,
               LateFlag = 0,
               NoDL = CASE 
                           WHEN LEN(ISNULL(@ClientDLNumber, '')) > 0 THEN 0
                           ELSE 1
                      END,
               DLNumber = CASE WHEN LEN(ISNULL(@ClientDLNumber, '')) > 0 THEN LTRIM(RTRIM(@ClientDLNumber)) ELSE '' END,
               IsPR = 3, 
               WalkInClientFlag = 0,
               Addressconfirmflag = 1,
               hasConsultationComments = 0, --Sabir Khan 9063 03/22/2011 Update Trial comments
               TrialComments = ISNULL(TrialComments,'') + ' ' + ISNULL(@AttorneyAbb,'')+ ' ' + 'client ' + '(' +dbo.formatdateandtimeintoshortdateandtime(getdate()) + ' - ' +  upper(@empnm) + ')' + ' ' ,
               LockFlag = 1,
               calculatedtotalfee = 1.00,               
               TotalFeeCharged = 1.0,
               InitialAdjustmentInitial = 'SYS'
        WHERE  TicketID_PK = @TicketID 
        --Update Call back status...				
   



			EXEC USP_HTS_INSERTORUPDATE_FOLLOWUP @TicketID,'1900-01-01 00:00:00.000',0

        
        --Update Client informations...
        UPDATE tblticketsviolations
        SET    CourtDate = @CourtDate,
               CourtDateScan = @CourtDate,
               CourtDateMain = @CourtDate,
               CourtViolationStatusID = @ViolationsStatusID,
               CourtViolationStatusIDScan = @ViolationsStatusID,
               CourtViolationStatusIDmain = @ViolationsStatusID,
               CourtID = 3001,
               CourtNumber = LTRIM(RTRIM(@CourtNumber)),
               CourtNumbermain = LTRIM(RTRIM(@CourtNumber)),
               CourtNumberscan  =  LTRIM(RTRIM(@CourtNumber)),
               RefCaseNumber = @TicketNumber, 
               ViolationStatusID = 1,
			   CoveringFirmID = @firmID --Sabir Khan 9063 03/22/2011 update firm id
        WHERE  TicketID_PK = @TicketID
               AND casenumassignedbycourt = LTRIM(RTRIM(@CauseNumber))
        
        --Getting First Name and Last Name....			
        SELECT @FirstName = FirstName,
               @LastName = LastName
        FROM   tbltickets
        WHERE  TicketID_PK = @TicketID
      
        --Update Email Address not avaliable flag when email is not avaliable.
        IF EXISTS (
               SELECT *
               FROM   tbltickets
               WHERE  TicketID_PK = @TicketID
                      AND LEN(LTRIM(RTRIM(ISNULL(Email, '')))) = 0
           )
        BEGIN
            UPDATE tbltickets
            SET    EmailNotAvailable = 1
            WHERE  TicketID_PK = @TicketID
        END
        
         --Make payment for the client
         if not exists(select * from tblticketspayment where ticketid = @TicketID)
         BEGIN
         INSERT INTO tblticketspayment(PaymentType,Ticketid,ChargeAmount,EmployeeID,PaymentReference)
	     VALUES(104,@TicketID,CONVERT(MONEY, 1.00),3992,@firmID)
	     
	     UPDATE tbltickets SET FirmID = @firmID WHERE TicketID_PK = @TicketID
		 END     

        if not exists(select * from tblticketsflag where ticketid_pk = @TicketID and flagid = 8)
        BEGIN        
        insert into tblticketsflag(ticketid_pk, flagid, empid, assignedto,ShowTrailDocket) 
        values(@TicketID, 8, 3992, @firmID,1)
        END
                     
          --Creating new Contract and associate them with the ticket....
        EXEC @ContactID = usp_htp_add_new_contact @FirstName,
             @LastName,
             @ClientBirthDate,
             3992,
             @TicketID,
             0
             
        SELECT @NewContactID = MAX(contactID) FROM Contact
        
        if exists(select * from tbltickets where isnull(ContactID_FK,'') = '' and TicketID_PK = @TicketID)
        BEGIN        
        EXEC USP_HTP_Associate_ContactID @NewContactID,@TicketID,3992
        END
        
        --Update information into Dump Table.
        UPDATE Ac
        SET    AC.IsProcessed = 1,
               ProcessedDate = GETDATE()
        FROM OutsideAttorneyCases Ac
               INNER JOIN OutsideAttorneyCasesFiles ACF
                    ON  ACF.AttorneyCasesFileID = AC.AttorneyCasesFileID
        WHERE  ACF.UploadedBy = @EmployeeID
               AND AC.AttorneyCasesFileID = @fileID
               AND LTRIM(RTRIM(REPLACE(AC.CauseNumber,' ',''))) = LTRIM(RTRIM(@CauseNumber))
        
        --Getting Ticket Number and Ticket ID
        SELECT @SystemTicketNumber = RefCaseNumber
        FROM   tblticketsviolations tv
               INNER JOIN tbltickets t
                    ON  t.TicketID_PK = tv.ticketid_pk
        WHERE  tv.casenumassignedbycourt = LTRIM(RTRIM(@CauseNumber))
               AND tv.TicketID_PK = @TicketID
        
        --Returning Ticket Number and Ticket ID
        SELECT @TicketID AS TicketID,
               @SystemTicketNumber AS TicketNumber
    END
    ELSE
-- If Record is not exist in our system... 
    IF NOT EXISTS(
           SELECT *
           FROM   tblticketsviolations tv
                  INNER JOIN tbltickets t
                       ON  t.TicketID_PK = tv.TicketID_PK
           WHERE  RTRIM(LTRIM(tv.casenumassignedbycourt)) = RTRIM(LTRIM(@CauseNumber))
       )
   BEGIN
        	
		--Getting Office Number       
        SELECT @OfficerNumber = OfficerNumber_PK
        FROM   tblOfficer
        WHERE  LastName = LTRIM(RTRIM(@OfficerLastName))
               AND FirstName = LTRIM(RTRIM(@OfficerfirstName))
        --If not exist in Data Base
        IF NOT EXISTS(
               SELECT t.*
               FROM   tbltickets t inner join OutsideAttorneyCases A on A.ClientBirthDate = t.DOB and A.Customer =  Convert(varchar,LTrim(Rtrim(@firstname))) + ' '+ Convert(varchar,LTrim(Rtrim(@lastname)))
               WHERE  t.Firstname = @firstname
                      AND t.Lastname = @lastname
                      AND t.DOB = @ClientBirthDate
                      and a.AttorneyCasesFileID = @fileID
                      and isnull(A.IsProcessed,0) = 1 
                     
           )
        BEGIN -- Insert client Information
            INSERT INTO tblTickets
              (
                Firstname,
                MiddleName,
                Lastname,                
                DOB,
                ADDRESS1,
                Address2,
                City,
                Stateid_FK,
                Zip,
                OfficerNumber,
                LanguageSpeak,
                TrialComments,
                EmployeeID,
                Recdate,
                Activeflag,
                Addressconfirmflag,
                BondFlag,
                calculatedtotalfee,
                AccidentFlag,
                CDLFlag,
                Contact1,
                ContactType1,
                LockFlag,
                EmailNotAvailable,
                NoDL, 
                DLNumber,
                LateFlag,
                WalkInClientFlag,
                isPR,
                hasConsultationComments,
                CaseTypeId,
                Occupation,
                VehicleType,
                OccupationID,
                TotalFeeCharged,
                IsUnemployed,
                InitialAdjustmentInitial
              )
            VALUES
              (
                @firstname,
                '',
                @lastname,                
                @ClientBirthDate,
                @FirmAddress1,
                @FirmAddress2,
                @FirmCity,
                @FirmState,
                @FirmZip,
                isnull(@OfficerNumber,0),
                'SPANISH',
                ISNULL(@AttorneyAbb,'') + ' ' + 'client ' + '(' +dbo.formatdateandtimeintoshortdateandtime(getdate()) + ' - ' +  upper(@empnm) + ')' + ' ' ,
                3992,
                GETDATE(),
                1,
                1,
                0,
                1.0,
                0,
                CASE LTRIM(RTRIM(@DLType))
                     WHEN 'Class C' THEN 0
                     WHEN 'CDL' THEN 1
                END,
                @AttorneyPhone,
                100,
                1,
                1,
                CASE 
                     WHEN LEN(ISNULL(@ClientDLNumber, '')) > 0 THEN 0
                     ELSE 1
                END,
                CASE WHEN LEN(ISNULL(@ClientDLNumber, '')) > 0 THEN LTRIM(RTRIM((ISNULL(@ClientDLNumber, '')))) ELSE '' END,
                0,
                0,
                3,
                0,
                1,
                'refuse to give',
                CASE @DLType
                     WHEN 'Class C' THEN 3
                     WHEN 'CDL' THEN 1
                END,
                @firmID,
                1.0,
                1,
                'SYS'
              )
            
          SET @TicketID = SCOPE_IDENTITY()          
       END
        --Insert Case Infomration
		if isnull(@TicketID,0) = 0
		BEGIN
        SELECT top 1 @TicketID = t.ticketid_pk FROM   tbltickets t inner join OutsideAttorneyCases A on A.ClientBirthDate = t.DOB and A.Customer =  Convert(varchar,LTrim(Rtrim(@firstname))) + ' '+ Convert(varchar,LTrim(Rtrim(@lastname)))
               WHERE  t.Firstname = @firstname
                      AND t.Lastname = @lastname
                      AND t.DOB = @ClientBirthDate
                      and a.AttorneyCasesFileID = @fileID
                      and isnull(A.IsProcessed,0) = 1 AND DATEDIFF(DAY,t.Recdate,GETDATE()) = 0 ORDER BY t.TicketID_PK DESC --Sabir Khan 9063 03/30/2011 get most recent ticket ID.
        END
        INSERT INTO tblTicketsViolations
          (
          	ticketid_pk,
            ViolationNumber_PK,
            RefCaseNumber,
            casenumassignedbycourt,
            ViolationStatusID,
            ViolationDescription,
            CourtDate,
            CourtNumber,
            CourtViolationStatusID,
            UpdatedDate,
            CourtID,
            TicketOfficerNumber,
            CourtDateMain,
            CourtViolationStatusIDmain,
            CourtNumbermain,
            CourtDateScan,
            CourtViolationStatusIDScan,
            CourtNumberscan,
            VerifiedStatusUpdateDate,
            VEmployeeID,
            planid,
            UnderlyingBondFlag,
            CoveringFirmID
          )
        VALUES
          (
          	@TicketID,
            10,
            @TicketNumber,
            LTRIM(RTRIM(@CauseNumber)),
            1,
            'Other',
            @CourtDate,
            LTRIM(RTRIM(@CourtNumber)),
            @ViolationsStatusID,
            GETDATE(),
            3001,
            isnull(@OfficerNumber,0),
            @CourtDate,
            @ViolationsStatusID,
            LTRIM(RTRIM(@CourtNumber)),
            @CourtDate,
            @ViolationsStatusID,
            LTRIM(RTRIM(@CourtNumber)),
            GETDATE(),
            3992,
            45,
            0,
            @firmID
          )
        
        --Getting First Name and Last Name of Client
        SELECT @FirstName = FirstName,
               @LastName = LastName
        FROM   tbltickets
        WHERE  TicketID_PK = @TicketID
        
        --Update Call Back Status
       EXEC USP_HTS_INSERTORUPDATE_FOLLOWUP @TicketID,'1900-01-01 00:00:00.000',0
        
         if not exists(select * from tblticketspayment where ticketid = @TicketID)
         BEGIN
         --Make payment for the client
         INSERT INTO tblticketspayment(PaymentType,Ticketid,ChargeAmount,EmployeeID,PaymentReference)
	     VALUES(104,@TicketID,CONVERT(MONEY, 1.00),3992,@firmID)
	     
	     UPDATE tbltickets SET FirmID = @firmID WHERE TicketID_PK = @TicketID
        END
        
        if not exists(select * from tblticketsflag where ticketid_pk = @TicketID and flagid = 8)
        BEGIN 
        insert into tblticketsflag(ticketid_pk, flagid, empid, assignedto,ShowTrailDocket) 
        values(@TicketID, 8, 3992, @firmID,1)
        END
        --Associate Contract
        if exists(select * from tbltickets where isnull(ContactID_FK,'') = '' and TicketID_PK = @TicketID)
        BEGIN
        --Create New Contract
        EXEC @ContactID = usp_htp_add_new_contact @FirstName,
             @LastName,
             @ClientBirthDate,
             3992,
             @TicketID,
             0
        
        SELECT @NewContactID = MAX(contactID) FROM Contact
        
        EXEC USP_HTP_Associate_ContactID @NewContactID,@TicketID,3992
        END       
        --Update Dump Table Infomration
        UPDATE Ac
        SET    AC.IsProcessed = 1,
               ProcessedDate = GETDATE()
        FROM   OutsideAttorneyCases Ac
               INNER JOIN OutsideAttorneyCasesFiles ACF
                    ON  ACF.AttorneyCasesFileID = AC.AttorneyCasesFileID
        WHERE  ACF.UploadedBy = @EmployeeID
               AND AC.AttorneyCasesFileID = @fileID
               AND LTRIM(RTRIM(REPLACE(AC.CauseNumber,' ',''))) = LTRIM(RTRIM(@CauseNumber))
        
        --Getting Ticket Number
        SELECT @SystemTicketNumber = RefCaseNumber
        FROM   tblticketsviolations tv
               INNER JOIN tbltickets t
                    ON  t.TicketID_PK = tv.ticketid_pk
        WHERE  tv.casenumassignedbycourt = LTRIM(RTRIM(@CauseNumber))
               AND tv.TicketID_PK = @TicketID
        --Returning Result
        SELECT @TicketID AS TicketID,
               @SystemTicketNumber AS TicketNumber
   END
   ELSE -- Sabir khan 9017 03/14/2011 Need to set trial status and date for the case if its already a sprecher case and having past disposed case.
    -----------------------------------------------------------------------------------------------------------------------------------------------
    	IF EXISTS (
   		        SELECT * FROM   tbltickets t  INNER JOIN tblticketsviolations tv ON  tv.TicketID_PK = t.TicketID_PK INNER JOIN tblTicketsPayment tp ON  tp.TicketID = t.TicketID_PK
				INNER JOIN tblTicketsFlag tf ON  tf.TicketID_PK = t.TicketID_PK INNER JOIN tblFirm tfm  ON  tfm.FirmID = t.FirmID
				WHERE  tfm.FirmID = 3090 AND tv.CoveringFirmID = 3090 AND tp.PaymentReference = 3090 AND tp.PaymentType = 104
				AND t.LanguageSpeak = 'SPANISH' AND tv.CourtID = 3001 AND tf.FlagID = 8 AND tf.assignedto = 3090 AND t.Activeflag = 1
				AND tv.CourtViolationStatusIDmain = 80 AND DATEDIFF(DAY, tv.CourtDateMain, GETDATE()) > 0 
				AND tv.casenumassignedbycourt = LTRIM(RTRIM(@CauseNumber))
   			  )
   	BEGIN
   		--Getting Office Number       
        SELECT @OfficerNumber = OfficerNumber_PK
        FROM   tblOfficer
        WHERE  LastName = LTRIM(RTRIM(@OfficerLastName))
               AND FirstName = LTRIM(RTRIM(@OfficerfirstName))
        --If not exist in Data Base
        IF NOT EXISTS(
               SELECT t.*
               FROM   tbltickets t inner join OutsideAttorneyCases A on A.ClientBirthDate = t.DOB and A.Customer =  Convert(varchar,LTrim(Rtrim(@firstname))) + ' '+ Convert(varchar,LTrim(Rtrim(@lastname)))
               WHERE  t.Firstname = @firstname
                      AND t.Lastname = @lastname
                      AND t.DOB = @ClientBirthDate
                      and a.AttorneyCasesFileID = @fileID
                      and isnull(A.IsProcessed,0) = 1 
                     
           )
        BEGIN -- Insert client Information
            INSERT INTO tblTickets
              (
                Firstname,
                MiddleName,
                Lastname,                
                DOB,
                ADDRESS1,
                Address2,
                City,
                Stateid_FK,
                Zip,
                OfficerNumber,
                LanguageSpeak,
                TrialComments,
                EmployeeID,
                Recdate,
                Activeflag,
                Addressconfirmflag,
                BondFlag,
                calculatedtotalfee,
                AccidentFlag,
                CDLFlag,
                Contact1,
                ContactType1,
                LockFlag,
                EmailNotAvailable,
                NoDL, 
                DLNumber,
                LateFlag,
                WalkInClientFlag,
                isPR,
                hasConsultationComments,
                CaseTypeId,
                Occupation,
                VehicleType,
                OccupationID,
                TotalFeeCharged,
                IsUnemployed,
                InitialAdjustmentInitial
              )
            VALUES
              (
                @firstname,
                '',
                @lastname,                
                @ClientBirthDate,
                @FirmAddress1,
                @FirmAddress2,
                @FirmCity,
                @FirmState,
                @FirmZip,
                isnull(@OfficerNumber,0),
                'SPANISH',
                ISNULL(@AttorneyAbb,'') + ' ' + 'client ' + '(' +dbo.formatdateandtimeintoshortdateandtime(getdate()) + ' - ' +  upper(@empnm) + ')' + ' ' ,
                3992,
                GETDATE(),
                1,
                1,
                0,
                1.0,
                0,
                CASE LTRIM(RTRIM(@DLType))
                     WHEN 'Class C' THEN 0
                     WHEN 'CDL' THEN 1
                END,
                @AttorneyPhone,
                100,
                1,
                1,
                CASE 
                     WHEN LEN(ISNULL(@ClientDLNumber, '')) > 0 THEN 0
                     ELSE 1
                END,
                CASE WHEN LEN(ISNULL(@ClientDLNumber, '')) > 0 THEN LTRIM(RTRIM((ISNULL(@ClientDLNumber, '')))) ELSE '' END,
                0,
                0,
                3,
                0,
                1,
                'refuse to give',
                CASE @DLType
                     WHEN 'Class C' THEN 3
                     WHEN 'CDL' THEN 1
                END,
                @firmID,
                1.0,
                1,
                'SYS'
              )
            
          SET @TicketID = SCOPE_IDENTITY()          
       END
        --Insert Case Infomration
		if isnull(@TicketID,0) = 0
		BEGIN
        SELECT top 1 @TicketID = t.ticketid_pk FROM   tbltickets t inner join OutsideAttorneyCases A on A.ClientBirthDate = t.DOB and A.Customer =  Convert(varchar,LTrim(Rtrim(@firstname))) + ' '+ Convert(varchar,LTrim(Rtrim(@lastname)))
               WHERE  t.Firstname = @firstname
                      AND t.Lastname = @lastname
                      AND t.DOB = @ClientBirthDate
                      and a.AttorneyCasesFileID = @fileID
                      and isnull(A.IsProcessed,0) = 1 AND DATEDIFF(DAY,t.Recdate,GETDATE()) = 0 ORDER BY t.TicketID_PK DESC --Sabir Khan 9063 03/30/2011 get most recent ticket ID.
        END
        INSERT INTO tblTicketsViolations
          (
          	ticketid_pk,
            ViolationNumber_PK,
            RefCaseNumber,
            casenumassignedbycourt,
            ViolationStatusID,
            ViolationDescription,
            CourtDate,
            CourtNumber,
            CourtViolationStatusID,
            UpdatedDate,
            CourtID,
            TicketOfficerNumber,
            CourtDateMain,
            CourtViolationStatusIDmain,
            CourtNumbermain,
            CourtDateScan,
            CourtViolationStatusIDScan,
            CourtNumberscan,
            VerifiedStatusUpdateDate,
            VEmployeeID,
            planid,
            UnderlyingBondFlag,
            CoveringFirmID
          )
        VALUES
          (
          	@TicketID,
            10,
            @TicketNumber,
            LTRIM(RTRIM(@CauseNumber)),
            1,
            'Other',
            @CourtDate,
            LTRIM(RTRIM(@CourtNumber)),
            @ViolationsStatusID,
            GETDATE(),
            3001,
            isnull(@OfficerNumber,0),
            @CourtDate,
            @ViolationsStatusID,
            LTRIM(RTRIM(@CourtNumber)),
            @CourtDate,
            @ViolationsStatusID,
            LTRIM(RTRIM(@CourtNumber)),
            GETDATE(),
            3992,
            45,
            0,
            @firmID
          )
        
        --Getting First Name and Last Name of Client
        SELECT @FirstName = FirstName,
               @LastName = LastName
        FROM   tbltickets
        WHERE  TicketID_PK = @TicketID
        
        --Update Call Back Status
       EXEC USP_HTS_INSERTORUPDATE_FOLLOWUP @TicketID,'1900-01-01 00:00:00.000',0
        
         if not exists(select * from tblticketspayment where ticketid = @TicketID)
         BEGIN
         --Make payment for the client
         INSERT INTO tblticketspayment(PaymentType,Ticketid,ChargeAmount,EmployeeID,PaymentReference)
	     VALUES(104,@TicketID,CONVERT(MONEY, 1.00),3992,@firmID)
	     
	     UPDATE tbltickets SET FirmID = @firmID WHERE TicketID_PK = @TicketID
        END
        
        if not exists(select * from tblticketsflag where ticketid_pk = @TicketID and flagid = 8)
        BEGIN 
        insert into tblticketsflag(ticketid_pk, flagid, empid, assignedto,ShowTrailDocket) 
        values(@TicketID, 8, 3992, @firmID,1)
        END
        --Associate Contract
        if exists(select * from tbltickets where isnull(ContactID_FK,'') = '' and TicketID_PK = @TicketID)
        BEGIN
        --Create New Contract
        EXEC @ContactID = usp_htp_add_new_contact @FirstName,
             @LastName,
             @ClientBirthDate,
             3992,
             @TicketID,
             0
        
        SELECT @NewContactID = MAX(contactID) FROM Contact
        
        EXEC USP_HTP_Associate_ContactID @NewContactID,@TicketID,3992
        END       
        --Update Dump Table Infomration
        UPDATE Ac
        SET    AC.IsProcessed = 1,
               ProcessedDate = GETDATE()
        FROM   OutsideAttorneyCases Ac
               INNER JOIN OutsideAttorneyCasesFiles ACF
                    ON  ACF.AttorneyCasesFileID = AC.AttorneyCasesFileID
        WHERE  ACF.UploadedBy = @EmployeeID
               AND AC.AttorneyCasesFileID = @fileID
               AND LTRIM(RTRIM(REPLACE(AC.CauseNumber,' ',''))) = LTRIM(RTRIM(@CauseNumber))
        
        --Getting Ticket Number
        SELECT @SystemTicketNumber = RefCaseNumber
        FROM   tblticketsviolations tv
               INNER JOIN tbltickets t
                    ON  t.TicketID_PK = tv.ticketid_pk
        WHERE  tv.casenumassignedbycourt = LTRIM(RTRIM(@CauseNumber))
               AND tv.TicketID_PK = @TicketID
        --Returning Result
        SELECT @TicketID AS TicketID,
               @SystemTicketNumber AS TicketNumber
	   
	   
   	END
   	-----------------------------------------------------------------------------------------------------------------------------------------------
    ELSE -- Already Client
    	BEGIN
    		-- Sabir Khan 9063 03/22/2011 Check if any other cause number is already processed from the same file then update its firm information.
    		IF EXISTS(SELECT * FROM tblticketsviolations tv INNER JOIN OutsideAttorneyCases oc ON LTRIM(RTRIM(oc.CauseNumber)) = tv.casenumassignedbycourt
    		          WHERE oc.AttorneyCasesFileID = @fileID)
    		          BEGIN
    		          	--Update Client informations...
    		          	SELECT @TicketID = ticketid_pk FROM tblTicketsViolations WHERE casenumassignedbycourt = LTRIM(RTRIM(@CauseNumber))
    		          	
						UPDATE tblticketsviolations
						SET    CourtDate = @CourtDate,
							   CourtDateScan = @CourtDate,
							   CourtDateMain = @CourtDate,
							   CourtViolationStatusID = @ViolationsStatusID,
							   CourtViolationStatusIDScan = @ViolationsStatusID,
							   CourtViolationStatusIDmain = @ViolationsStatusID,
							   CourtID = 3001,
							   CourtNumber = LTRIM(RTRIM(@CourtNumber)),
							   CourtNumbermain = LTRIM(RTRIM(@CourtNumber)),
							   CourtNumberscan  =  LTRIM(RTRIM(@CourtNumber)),
							   RefCaseNumber = @TicketNumber, 
							   ViolationStatusID = 1,
							   CoveringFirmID = @firmID
						WHERE  TicketID_PK = @TicketID
							   AND casenumassignedbycourt = LTRIM(RTRIM(@CauseNumber))
    		          	
    		          	UPDATE Ac
									SET    AC.IsProcessed = 1,
										   ProcessedDate = GETDATE()
									FROM   OutsideAttorneyCases Ac
										   INNER JOIN OutsideAttorneyCasesFiles ACF
												ON  ACF.AttorneyCasesFileID = AC.AttorneyCasesFileID
									WHERE  ACF.UploadedBy = @EmployeeID
										   AND AC.AttorneyCasesFileID = @fileID
										   AND LTRIM(RTRIM(REPLACE(AC.CauseNumber,' ',''))) = LTRIM(RTRIM(@CauseNumber))
							        
									--Getting Ticket Number
									SELECT @SystemTicketNumber = RefCaseNumber
									FROM   tblticketsviolations tv
										   INNER JOIN tbltickets t
												ON  t.TicketID_PK = tv.ticketid_pk
									WHERE  tv.casenumassignedbycourt = LTRIM(RTRIM(@CauseNumber))
										   AND tv.TicketID_PK = @TicketID
									--Returning Result
									SELECT @TicketID AS TicketID,
										   @SystemTicketNumber AS TicketNumber
    		          	
    		          	
    		          END
    		          ELSE
    		          	BEGIN
    		          		SELECT @TicketID = ticketid_pk FROM tblTicketsViolations WHERE casenumassignedbycourt = LTRIM(RTRIM(@CauseNumber))
    						SELECT 'Already Client-No Action Performed ' + CONVERT(VARCHAR,@TicketID) AS TicketID,
							'Already Client-No Action Performed ' + CONVERT(VARCHAR,@TicketID) AS TicketNumber
    		          	END   		          
    		
    	END
END








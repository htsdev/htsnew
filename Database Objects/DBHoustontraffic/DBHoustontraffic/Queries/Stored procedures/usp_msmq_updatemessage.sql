SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_msmq_updatemessage]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_msmq_updatemessage]
GO


CREATE procedure usp_msmq_updatemessage
    
@id int,    
@Status varchar(50)    
    
as    
    
update tbl_webscan_msmqlog  
set trialstatus=@Status    
where id=@id 


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


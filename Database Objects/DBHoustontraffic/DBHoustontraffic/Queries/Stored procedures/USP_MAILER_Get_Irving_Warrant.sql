set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go



/****** 
Business Logic:	The procedure is used by LMS application to get the number of printable letter
				count for the Irving Warrant letter for the selected date range.
				
List of Parameters:
	@catnum:	 Category number of the selected letter type, if printing for 
					all courts of the selected court category
	@courtid:	 Court id of the selected court category if printing for individual court hosue.
	@startdate:	 Starting list date for letter.
	@enddate:	 Ending list date for letter.
	@SearchType: Flag that identifies if searching by court date or by list date. 0= list date, 1 = court date

List of Columns:	
	Total_Count:	Total number of letters exists for the selected date range.
	Good_Count:		Total number of letters that have valid addresses
	Bad_Count:		Total Number of letter that have undeliverable addresses
	DonotMail_Count:Total number of letter for addresses that had been marked to stop sending letters.
	
******/

 --  USP_MAILER_Get_Irving_Warrant '2/2/08', '3/22/08', 25, 0, 25  
ALTER procedure [dbo].[USP_MAILER_Get_Irving_Warrant]  
 (  
 @startdate datetime,  
 @enddate datetime,  
 @courtid int = 6,  
 @bondflag tinyint = 0,  
 @catnum int=6  
 )  
  
as  
  
declare @officernum varchar(50),                                                      
 @officeropr varchar(50),                                                      
 @tikcetnumberopr varchar(50),                                                      
 @ticket_no varchar(50),                                                                                                    
 @zipcode varchar(50),                                                               
 @zipcodeopr varchar(50),                                                      
 @fineamount money,                                                              
 @fineamountOpr varchar(50) ,                                                              
 @fineamountRelop varchar(50),                                     
 @singleviolation money,                                                              
 @singlevoilationOpr varchar(50) ,                                                              
 @singleviolationrelop varchar(50),                                                      
 @doubleviolation money,                                                              
 @doublevoilationOpr varchar(50) ,                                                              
 @doubleviolationrelop varchar(50),                                                    
 @count_Letters int,                                    
 @violadesc varchar(500),                                    
 @violaop varchar(50),   
 @sqlquery varchar(8000) ,   
 @lettertype int                                                   
  
set @enddate =@enddate+'23:59:59.000'                                                           
set @sqlquery = ''                                                   
set @lettertype = 51
  
Select  @officernum=officernum,                                                      
 @officeropr=oficernumOpr,                                                      
 @tikcetnumberopr=tikcetnumberopr,                                                      
 @ticket_no=ticket_no,                                                      
 @zipcode=zipcode,                                                      
 @zipcodeopr=zipcodeLikeopr,                                              
 @singleviolation =singleviolation,                                                 
 @singlevoilationOpr =singlevoilationOpr,                                                      
 @singleviolationrelop =singleviolationrelop,                                                      
 @doubleviolation = doubleviolation,                                                      
 @doublevoilationOpr=doubleviolationOpr,                                                              
 @doubleviolationrelop=doubleviolationrelop,                                    
 @violadesc=violationdescription,                                    
 @violaop=violationdescriptionOpr ,                                
 @fineamount=fineamount ,                   
 @fineamountOpr=fineamountOpr ,                                                   
 @fineamountRelop=fineamountRelop                   
from  tblmailer_letters_to_sendfilters                                 
where  courtcategorynum=@catnum      
and   lettertype =   @lettertype                                                
       
declare @dateVar varchar(30)  
  
set @dateVar = ' tva.violationbonddate '  
set @sqlquery=@sqlquery+'                                          
Select distinct tva.recordid,   
     convert(datetime,convert(varchar(10),tva.violationbonddate,101)) as mdate,                       
  flag1 = isnull(ta.Flag1,''N'') ,                                                      
  isnull(ta.donotmailflag,0) as donotmailflag,                                          
  count(tva.ViolationNumber_PK) as ViolCount,                                                      
  isnull(sum(isnull(tva.bondamount,0)),0)as Total_Fineamount  
into #temp                                                            
FROM dallastraffictickets.dbo.tblTicketsViolationsArchive TVA                       
INNER JOIN                       
 dallastraffictickets.dbo.tblTicketsArchive TA ON TVA.recordid= TA.recordid  
where ta.lastname is not null   
and ta.firstname is not null  
and isnull(ta.ncoa48flag,0) = 0  and charindex(''v'',tva.ticketnumber_pk)<>1
and tva.violationbonddate is not null  
and ta.bondflag  = 1 
and datediff(day, ' + @datevar + ' , ''' + convert(varchar(10),@startDate,101)  + ''') <= 0  
and datediff(day, ' + @datevar + ' , ''' + convert(varchar(10),@endDate,101)  + ''' )>= 0 

-- Rab Nawaz Khan 8754 02/23/2011 Exclude Disposed Violations
and tva.violationstatusid <> 80
-- End 8754
  
'              
  
if(@courtid=@catnum)  
 set @sqlquery=@sqlquery+'   
 and ta.courtid In (Select courtid from dallastraffictickets.dbo.tblcourts where courtcategorynum ='+convert(varchar(10),@catnum ) +')'            
else  
 set @sqlquery=@sqlquery+'   
 and ta.courtid=' + convert(varchar,@courtid)  
  
                                      
if(@officernum<>'')                                                        
 set @sqlquery =@sqlquery + '    
 and  ta.officerNumber_Fk ' +@officeropr  +'('+   @officernum+')'                                                         
                                  
                                                       
if(@ticket_no<>'')                        
 set @sqlquery=@sqlquery + '   
 and ('+  dbo.Get_String_Concat_With_op(''+@ticket_no+'' , 'tva.TicketNumber_PK' ,+ ''+ @tikcetnumberopr) +')'                                                              
                                    
                                    
if(@zipcode<>'')                                                                              
 set @sqlquery=@sqlquery+ '   
 and  ('+  dbo.Get_String_Concat_With_op(''+@zipcode+'' , 'left(ta.zipcode,2)' ,+ ''+ @zipcodeopr) +')'  
                                 
if(@fineamount<>0 and @fineamountOpr<>'not'  )                                  
set @sqlquery =@sqlquery+ '  
      and tva.bondamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                                       
  
if(@fineamount<>0 and @fineamountOpr = 'not'  )                                  
set @sqlquery =@sqlquery+ '  
      and not tva.bondamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                                       
  
  --Yasir Kamal 7365 02/01/2010 exclude letters
  -- Babar Ahmad 8597	07/12/2011 Changed number of days from 3 to 5.
-- EXCLUDE THE LETTERS THAT HAVE BEEN ALREADY PRINTED......
-- OR APP OR APP DAY OF LETTER PRINTED IN PAST 5 DAYS FOR THE SAME RECORD.. 
set @sqlquery =@sqlquery + '                                                
and ta.recordid  not in   
 (                                                                    
 	select n.recordid FROM dbo.tblLetterNotes n inner join dallastraffictickets.dbo.tblticketsarchive t
		on t.recordid  = n.recordid  where lettertype = 51
    union
	select n.recordid FROM dbo.tblLetterNotes n inner join dallastraffictickets.dbo.tblticketsarchive t
	on t.recordid  = n.recordid  where lettertype in  (49,50)
	and datediff(day, n.recordloaddate, dbo.fn_getprevbusinessday(GETDATE(),5)) <=0    
 )     
'  
  
set @sqlquery =@sqlquery + '   
 group by                                                       
 convert(datetime, convert(varchar(10),' + @dateVar + ',101)),  
 ta.flag1,  ta.donotmailflag,  tva.recordid  
 having  1=1   
'                                  
  
if(@singleviolation<>0 and @doubleviolation=0)                                  
 set @sqlquery =@sqlquery+ '  
 and (  '+ @singlevoilationOpr+  '  (isnull(sum(isnull(tva.bondamount,0)),0) '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and count(tva.ViolationNumber_PK)=1))  
      '                                  
if(@doubleviolation<>0 and @singleviolation=0 )                                  
 set @sqlquery =@sqlquery + '  
 and ('+ @doublevoilationOpr+  '   (isnull(sum(isnull(tva.bondamount,0)),0) '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ')and count(tva.ViolationNumber_PK)=2) )  
      '                                                      
                    
if(@doubleviolation<>0 and @singleviolation<>0)                                  
 set @sqlquery =@sqlquery+ '  
 and ('+ @singlevoilationOpr+  '  (isnull(sum(isnull(tva.bondamount,0)),0) '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and count(tva.ViolationNumber_PK)=1))  
    and ('+  @doublevoilationOpr+  '   (isnull(sum(isnull(tva.bondamount,0)),0) '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ') and count(tva.ViolationNumber_PK)=2))  
        '                                    
                                                      
  
  
set @sqlquery=@sqlquery+                                         
'Select distinct a.recordid, a.ViolCount, a.Total_Fineamount,  a.mDate , a.Flag1, a.donotmailflag   
into #temp1  from #temp a, dallastraffictickets.dbo.tblticketsviolationsarchive tva where a.recordid = tva.recordid   

	-- Rab Nawaz Khan 8754 02/23/2011 Exclude Disposed Violations
	and tva.violationstatusid <> 80
	-- End 8754

 '                                  
  
-- VIOLATION DESCRIPTION FILTER.....  
if(@violadesc <> '')  
begin  
-- NOT LIKE FILTER.......  
if(charindex('not',@violaop)<> 0 )                
 set @sqlquery=@sqlquery+'   
 and  not ( a.violcount =1  and ('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ 'like')+'))'             
  
-- LIKE FILTER.......  
if(charindex('not',@violaop) =  0 )                
 set @sqlquery=@sqlquery+'   
 and  ( a.violcount =1  and ('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ 'like')+'))'             
end  

--Yasir Kamal 7119 12/10/2009 send warrant letters to existing clients.
set @sqlquery=@sqlquery+                       
'
-- Babar Ahmad 9348 08/19/2011 Excluded client cases 
-- Sabir Khan 7099 12/07/2009 exclude client cases
delete from #temp1 where recordid in (
select v.recordid from dallastraffictickets.dbo.tblticketsviolations v inner join dallastraffictickets.dbo.tbltickets t 
on t.ticketid_pk = v.ticketid_pk and t.activeflag = 1 
inner join #temp1 a on a.recordid = v.recordid
)
-- ' 
  
set @sqlquery=@sqlquery+'

Select count(distinct recordid) as Total_Count, mDate                                                    
into #temp2                                                  
from #temp1                                                  
group by mDate                                                       
  
Select count(distinct recordid) as Good_Count, mDate                                                                                                
into #temp3                                                  
from #temp1                                                      
where Flag1 in (''Y'',''D'',''S'')     
and donotmailflag =  0                                                   
group by mDate   
                                                      
Select count(distinct recordid) as Bad_Count, mDate                                                    
into #temp4                                                      
from #temp1                                                  
where Flag1  not in (''Y'',''D'',''S'')                                                      
and donotmailflag = 0                                                   
group by mDate   
                   
Select count(distinct recordid) as DonotMail_Count, mDate                                                   
 into #temp5                                                      
from #temp1          
where donotmailflag = 1    
--and flag1  in  (''Y'',''D'',''S'')                                                      
group by mDate   
         
                                                      
Select   listdate = #temp2.mDate ,  
        Total_Count,  
        isnull(Good_Count,0) Good_Count,  
        isnull(Bad_Count,0)Bad_Count,  
        isnull(DonotMail_Count,0)DonotMail_Count  
from #Temp2 left outer join #temp3   
on #temp2.mDate = #temp3.mDate  
                                                     
left outer join #Temp4  
on #temp2.mDate = #Temp4.mDate  
    
left outer join #Temp5  
on #temp2.mDate = #Temp5.mDate                                                   
    
where Good_Count > 0                
order by #temp2.mDate desc                                                       
            
                                                
drop table #temp                                                  
drop table #temp1                                                  
drop table #temp2                                                   
drop table #temp3                                                  
drop table #temp4  
drop table #temp5  
'                                                      
                                                      
--print @sqlquery  
exec(@sqlquery)  







SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_All_PaymentDetailByRecDate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_All_PaymentDetailByRecDate]
GO








CREATE procedure [dbo].[usp_Get_All_PaymentDetailByRecDate]   
  
@RecDate DateTime  

  
AS  
  
CREATE TABLE #TempFinReportA  
(  
   EmployeeID  int NOT NULL CONSTRAINT PK1_EmployeeID PRIMARY KEY,  
   CashAmount      money     Not NULL DEFAULT(0),  
)  

CREATE TABLE #TempFinReportB  
(  
   EmployeeID  int NOT NULL CONSTRAINT PK2_EmployeeID PRIMARY KEY,  
   CheckAmount     money     Not NULL DEFAULT(0)  
)  
  
INSERT INTO #TempFinReportA  (EmployeeID, CashAmount)  
SELECT  EmployeeID, Sum(ChargeAmount) As Amount   
FROM         dbo.tblTicketsPayment   
Where	-- RecDate Between  @RecDate   AND   @RecDate+1)  
	datediff(day, recdate, @recdate) = 0
And PaymentType = 1    
AND  PaymentVoid <> 1   
Group By EmployeeID   
Order By EmployeeID   
  
  
INSERT INTO #TempFinReportB  (EmployeeID, CheckAmount)  
SELECT  EmployeeID, Sum(ChargeAmount) As Amount   
FROM         dbo.tblTicketsPayment   
Where	--( RecDate Between  @RecDate   AND   @RecDate+1)  
	datediff(day, recdate, @recdate) = 0
--And PaymentType IN (2,5,6,7,9)    
And PaymentType = 2  
AND  PaymentVoid <> 1   
Group By EmployeeID   
Order By EmployeeID   
  
  
SELECT	ISNULL(dbo.#TempFinReportA.EmployeeID, dbo.#TempFinReportB.EmployeeID) AS EmpID, 
	ISNULL(CashAmount,0)as CashAmount, 
	ISNULL(CheckAmount,0) as CheckAmount  
FROM	dbo.#TempFinReportA 
FULL OUTER JOIN  
	dbo.#TempFinReportB 
ON 	dbo.#TempFinReportA.EmployeeID = dbo.#TempFinReportB.EmployeeID  









GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


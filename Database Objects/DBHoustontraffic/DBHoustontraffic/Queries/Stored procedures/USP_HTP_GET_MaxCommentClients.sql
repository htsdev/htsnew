set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go



/*    
Created By: Sabir Khan    
Date:   06/15/2010    
    
Business Logic:    
 This stored procedure is used to get active with maximum number of comments    
    
List of Input Parameters:    
 @TopNum: Total number for getting
 @Location: Location is either dallas or houston    
    
List of Output Columns:                                                                   
 Firstname,                                                                                                                
 Lastname,                                                                   
 Hire Date  
 Comment Length   
*/      

 --USP_HTP_GET_MaxCommentClients 10,2 

CREATE PROCEDURE [dbo].[USP_HTP_GET_MaxCommentClients]


@TopNum INT,
@Location INT
AS


IF ( @Location=2)
BEGIN


SELECT  DISTINCT TOP(@TopNum)  tt.Firstname [First Name],tt.Lastname [Last Name],dbo.fn_DateFormat(CONVERT(DATETIME,MIN(ttp.RecDate)),27,'/',':',1) AS [Hire Date],LEN(tt.GeneralComments) [Comments Length]
FROM DallasTrafficTickets.dbo.tbltickets tt INNER JOIN DallasTrafficTickets.dbo.tblTicketsPayment ttp ON ttp.TicketID = tt.TicketID_PK  
INNER JOIN DallasTrafficTickets.dbo.tblTicketsViolations tv ON tv.TicketID_PK = tt.TicketID_PK 
INNER JOIN DallasTrafficTickets.dbo.tblCourtViolationStatus tvs ON tv.CourtViolationStatusIDmain = tvs.CourtViolationStatusID 
WHERE (CHARINDEX(tt.Firstname,'Test')=0 AND  CHARINDEX(tt.Lastname,'Test')=0)
AND tt.Activeflag = 1
AND tvs.CategoryID <> 80
GROUP BY tt.Firstname,tt.Lastname,LEN(tt.GeneralComments),tt.TicketID_PK
ORDER BY LEN(tt.GeneralComments) DESC 

	
END

ELSE
BEGIN
SELECT  DISTINCT TOP(@TopNum)  tt.Firstname [First Name],tt.Lastname [Last Name],dbo.fn_DateFormat(CONVERT(DATETIME,MIN(ttp.RecDate)),27,'/',':',1) AS [Hire Date],LEN(tt.GeneralComments) [Comments Length]
FROM tbltickets tt INNER JOIN tblTicketsPayment ttp ON ttp.TicketID = tt.TicketID_PK 
INNER JOIN tblTicketsViolations tv ON tv.TicketID_PK = tt.TicketID_PK 
INNER JOIN tblCourtViolationStatus tvs ON tv.CourtViolationStatusIDmain = tvs.CourtViolationStatusID       
WHERE (CHARINDEX(tt.Firstname,'Test')=0 AND  CHARINDEX(tt.Lastname,'Test')=0)
AND tt.Activeflag = 1
AND tvs.CategoryID <> 80
GROUP BY tt.Firstname,tt.Lastname,LEN(tt.GeneralComments),tt.TicketID_PK
ORDER BY LEN(tt.GeneralComments) DESC 
END
GO
GRANT EXECUTE ON  [dbo].[USP_HTP_GET_MaxCommentClients] TO dbr_webuser
GO
 





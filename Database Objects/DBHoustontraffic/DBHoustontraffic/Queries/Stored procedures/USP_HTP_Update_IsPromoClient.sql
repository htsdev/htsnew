USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTP_Update_IsPromoClient]    Script Date: 01/02/2014 03:41:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/****** 
Sabir Khan 11460 10/01/2013: this procedure is used to update promo field for promo clients.
*******/

ALTER PROCEDURE [dbo].[USP_HTP_Update_IsPromoClient]
@LetterType INT
AS
BEGIN	
SET NOCOUNT ON;

DECLARE @temp TABLE(RecordID int, CauseNumber VARCHAR(30), MidNumber VARCHAR(30))

INSERT INTO @temp(RecordID,CauseNumber,MidNumber)
SELECT ta.RecordID, tva.CauseNumber, ta.MidNumber 
FROM tblTicketsArchive ta INNER JOIN tblTicketsViolationsArchive tva ON ta.RecordID = tva.RecordID
WHERE ta.MidNumber IN (SELECT midnumber FROM HMCCurrentMailerRecords)

DECLARE @tempEvent TABLE(CauseNumber VARCHAR(30), InsertDate DATETIME, attorneyname VARCHAR(100), barnumber VARCHAR(100),MidNumber VARCHAR(30))

INSERT INTO @tempEvent(CauseNumber,InsertDate,attorneyname,barnumber,MidNumber)
SELECT t.causenumber,t.InsertDate,t.attorneyname,t.barnumber,tm.MidNumber
FROM LoaderFilesArchive.dbo.tblEventExtractTemp t 
inner join @temp tm on tm.CauseNumber = replace(t.causenumber,' ','')

DECLARE @tmpMid TABLE(MidNumber VARCHAR(30))

INSERT INTO @tmpMid(MidNumber)
SELECT distinct MidNumber FROM  @tempEvent 
WHERE (ISNULL(attorneyname,'') LIKE '%SULLO%' OR ISNULL(barnumber,'') = '24026218')

DELETE t
FROM @temp t INNER JOIN @tmpMid m ON m.MidNumber = t.MidNumber

DECLARE @tmpFinal TABLE(CauseNumber VARCHAR(30), InsertDate DATETIME, attorneyname VARCHAR(100), barnumber VARCHAR(100),MidNumber VARCHAR(30))

INSERT INTO @tmpFinal(CauseNumber,InsertDate,attorneyname,barnumber,MidNumber)
SELECT causenumber,insertdate,attorneyname,barnumber,midnumber FROM @tempEvent
WHERE DATEDIFF(DAY,InsertDate,DATEADD(YEAR,-3,GETDATE()))<=0
AND (LEN(ISNULL(attorneyname,''))>0 OR LEN(ISNULL(barnumber,''))>0)


DECLARE @tmpMaxRec TABLE (Midnumber VARCHAR(30),InsertDate DATETIME)

INSERT INTO @tmpMaxRec(Midnumber,InsertDate)
SELECT Midnumber, MAX(insertdate) FROM @tmpFinal
WHERE  (LEN(ISNULL(attorneyname,''))>0 OR LEN(ISNULL(barnumber,''))>0)
GROUP BY MidNumber


UPDATE h SET h.IsPromo = 1, h.PromoAttorney = te.attorneyname,h.PromoAttorneyBarCard = te.barnumber 
FROM HMCCurrentMailerRecords h INNER JOIN @temp t ON h.midnumber = t.midnumber	
INNER JOIN @tmpFinal te ON REPLACE(te.causenumber,' ','') = t.CauseNumber
INNER JOIN @tmpMaxRec r ON r.Midnumber = h.midnumber AND r.InsertDate= te.InsertDate
where (te.attorneyname Not Like '%DAVID SPRECHER%')


UPDATE h SET h.IsPromo = 0 ,h.PromoAttorney = '', h.PromoAttorneyBarCard = ''
FROM HMCCurrentMailerRecords h WHERE h.PromoAttorney NOT IN ('William Citizen','Lennon Prince') AND h.ViolCount>5


update t set t.PromoID = a.PromoID,t.PromoMessage = a.PromoMessageForLatter 
from HMCCurrentMailerRecords t 
inner join AttorneyPromoTemplates a on a.MailerID = @LetterType
AND (a.AttorneyName = t.PromoAttorney OR a.AttorneyBarCode = t.PromoAttorneyBarCard)
and t.IsPromo = 1 
AND t.PromoMessage IS NULL
AND t.ViolCount = a.ViolationCount


update t set t.PromoID = a.PromoID,t.PromoMessage = a.PromoMessageForLatter 
from HMCCurrentMailerRecords t 
inner join AttorneyPromoTemplates a on a.MailerID = @LetterType
AND (a.AttorneyName = t.PromoAttorney OR a.AttorneyBarCode = t.PromoAttorneyBarCard)
and t.IsPromo = 1
AND a.ViolationCount = 0

update t set t.PromoID = a.PromoID,t.PromoMessage = a.PromoMessageForLatter 
from HMCCurrentMailerRecords t 
inner join AttorneyPromoTemplates a on a.MailerID = @LetterType 
AND (a.AttorneyName in ('Paul Kubosh')) 
and t.IsPromo = 1 
AND t.PromoMessage IS NULL
AND t.ViolCount = a.ViolationCount

update HMCCurrentMailerRecords set IsPromo = 0, PromoID = null where len(isnull(PromoMessage,''))=0

END


﻿
ALTER procedure [dbo].[usp_webscan_ComparisonWithPending]               
              
@PicID as int,              
--ozair 4330 07/02/2008 causeno length increased to 200 from 50
@CauseNo as varchar(200),     
@CourtDate as datetime,              
@CourtTime as varchar(15),              
@CourtNo as int, 
@Status as varchar(50),              
@Location as varchar(50)  ,            
@CourtDateScan as datetime ,        
@Msg as int output             
as              
              
declare @TempPicID as int              
select @TempPicID=PicID from tbl_webscan_data where PicID= @PicID              
              
if @TempPicID != ''              
begin              
 update tbl_Webscan_data              
 set               
 CauseNo=@CauseNo,              
 CourtDate=@CourtDate,              
 CourtTime=@CourtTime,              
 CourtRoomNo=@CourtNo,              
 Status=@Status,              
 CourtLocation=@Location              
 where              
 PicID=@PicID              
end              
              
else              
begin              
              
insert into tbl_webscan_data              
(              
 PicID,              
 CauseNo,              
 CourtDate,              
 CourtTime,              
 CourtRoomNo,              
 Status,              
 CourtLocation              
              
)              
values              
(              
 @PicID,              
 @CauseNo,              
 @CourtDate,              
 @CourtTime,              
 @CourtNo,              
 @Status,              
 @Location              
              
)              
end              
--ozair 4330 07/02/2008 causeno length increased to 200 from 50
declare @ocr_causeno as varchar(200)              
declare @ocr_courtdate as varchar(50)              
declare @ocr_time as varchar(50)              
declare @ocr_courtno as varchar(50)              
declare @ocr_status as varchar(50)              
declare @ocr_crtlocation as varchar(50)              
              
--ozair 4330 07/02/2008 causeno length increased to 200 from 50              
declare @data_causeno as varchar(200)              
declare @data_courtdate as varchar(12)              
declare @data_time as varchar(12)              
declare @data_courtno as varchar(5)               
declare @data_status as varchar(50)              
declare @data_crtlocation as varchar(50)              
declare @Compare as int              
              
set @Compare=2              
 Select @ocr_causeno=causeno,              
   @ocr_status=status,              
   @ocr_courtdate=case when newcourtdate='01/01/1900' then '' else newcourtdate end,              
   @ocr_courtno=courtno,              
   @ocr_crtlocation=location,              
   @ocr_time=Time              
              
   from tbl_webscan_ocr              
              
 where               
                
  PicID=@PicID              
              
              
 Select @data_causeno=CauseNo,              
   @data_courtdate =case when convert(varchar(12),CourtDate,101)='01/01/1900' then '' else convert(varchar(12),CourtDate,101) end,              
   --@data_time =convert(varchar(12),CourtTime,108),              
   @data_time =CourtTime,              
   @data_courtno =convert(varchar(5),CourtRoomNo),              
   @data_status =Status,              
   @data_crtlocation =CourtLocation              
              
   from tbl_webscan_data              
              
 where              
  PicID=@PicID              
               
              
              
              
begin              
 if @ocr_causeno = @data_causeno              
                
  if @ocr_courtdate = @data_courtdate              
                 
   if @ocr_time = @data_time              
                  
    if @ocr_courtno = @data_courtno              
                  
    if @ocr_status = @data_status              
                   
      if @ocr_crtlocation = @data_crtlocation              
                      
       set @Compare=3              
              
              
end              
              
if @Compare=3              
begin            
              
 update tbl_webscan_ocr              
 set              
 CheckStatus=3              
 where              
 PicID=@PicID              
            
   set @Msg=3         
end               
              
if @Compare=2              
begin              
 update tbl_webscan_ocr              
 set              
 CheckStatus=2              
 where              
 PicID=@PicID          
set @Msg=2            
end               
        
return @Msg      
  
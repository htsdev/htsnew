SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USp_HTS_Get_Information_For_SPNSearch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USp_HTS_Get_Information_For_SPNSearch]
GO


CREATE Procedure dbo.USp_HTS_Get_Information_For_SPNSearch   
@TicketID int,   
@EmpID int  
as  
  
  
declare @Midnum as varchar(20)  
declare @SPNUser as varchar(20)  
declare @SPNPassword varchar(20)  
  
Select @Midnum = Midnum from tbltickets where ticketid_pk = @TicketID  
  
Select @SPNUser = SPNUSerName , @SPNPassword = SPNPassword  from tblusers where employeeid = @EmpID  
  
Select isnull (@Midnum,'0') as SPN ,  isnull(@SPNUser,'') as SPNUser , isnull(@SPNPassword,'')  as SPNPassword   
  


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


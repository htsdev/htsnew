SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_UPDATE_GeneralComments]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_UPDATE_GeneralComments]
GO

--	USP_HTS_UPDATE_GeneralComments

CREATE Procedure [dbo].[USP_HTS_UPDATE_GeneralComments]

@TicketID int,
@GeneralComments varchar(1000)

AS

declare @empid int

select @empid = employeeid from tbltickets where ticketid_pk =@TicketID

if ((right(@GeneralComments,1) <> ')')  )   --if comments are updated            
 begin                 
   declare @empnm as varchar(12)                
                   
   select @empnm = abbreviation from tblusers where employeeid = @empid                
             
   if len(@GeneralComments) > 0       
 set @GeneralComments = @GeneralComments +' ' + '(' +dbo.formatdateandtimeintoshortdateandtime(getdate()) + ' - ' +  @empnm + ')' + ' '                
                
   update tbltickets set  generalcomments = @GeneralComments                   
                      
   where ticketid_pk = @TicketID                
 end    

select generalcomments from tbltickets where ticketid_pk =@TicketID


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_PS_GetCaseStatusInfo]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_PS_GetCaseStatusInfo]
GO




-- USp_helptext USP_PS_GetCaseStatusInfo  
    
    
CREATE Procedure USP_PS_GetCaseStatusInfo --'5M00328486'        
(        
        
@TicketNumber varchar(20)        
)        
        
as        
        
  
declare @len int   
  
set @TicketNumber = ltrim(rtrim(@TicketNumber))    
set @len = len(@TicketNumber)    
  
  
if @len > 9    
 begin    
  set @len = @len -1    
  set @TicketNumber = left(@TicketNumber,@len) + '%'    
    
Select top 1 isNull(t.ActiveFlag , 2 ) as Status , t.RecordID , t.TicketID_Pk from tbltickets t         
join tblticketsviolations  tv        
on         
t.ticketid_pk = tv.Ticketid_pk        
where tv.refcaseNumber  like   @TicketNumber      
end  
 else  
begin  
  
Select top 1 isNull(t.ActiveFlag , 2 ) as Status , t.RecordID , t.TicketID_Pk from tbltickets t         
join tblticketsviolations  tv        
on         
t.ticketid_pk = tv.Ticketid_pk        
where tv.refcaseNumber =  @TicketNumber     
  
end    
    
    

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


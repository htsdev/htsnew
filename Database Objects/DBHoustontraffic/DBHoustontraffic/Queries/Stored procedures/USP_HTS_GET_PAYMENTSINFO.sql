    
    
-- [USP_HTS_GET_PAYMENTSINFO] 124801      
      
ALTER Procedure [dbo].[USP_HTS_GET_PAYMENTSINFO]      
        (                            
 @ticketID int                                    
 )                                    
as                                    
                          
declare @status as int                          
                
select @status= activeflag from tbltickets where ticketid_pk=@ticketID                          
                          
if (@status=1)    --if activeflag ==1 i.e client                      
 begin                                   
 SELECT                               
  T.TicketID_PK,                          
        
  ISNULL(SUM(DISTINCT T.totalfeecharged), 0) AS TotalFee,                          
  ISNULL(SUM(TP.ChargeAmount), 0) AS Paid,                             
--ISNULL(SUM(DISTINCT T.calculatedtotalfee) - SUM(TP.ChargeAmount), 0) AS Owes,           REMARKED TO GET LOCK AMOUNT...                
  ISNULL(SUM(DISTINCT T.totalfeecharged) - isnull (SUM(TP.ChargeAmount),0), 0) AS Owes,                          
/*        
  ISNULL(SUM( isnull(T.totalfeecharged,0) ), 0) AS TotalFee,                          
  ISNULL(SUM( isnull(TP.ChargeAmount,0)), 0) AS Paid,                             
  ISNULL(SUM( isnull(T.totalfeecharged,0) ) - isnull (SUM( isnull(TP.ChargeAmount,0) ),0), 0) AS Owes,                          
*/        
    
  T.Activeflag,              
    
/*  (select count(tv.ticketid_pk) from tblticketsviolations tv      
 left outer join tblcourtviolationstatus cvs on       
 tv.courtviolationstatusidmain=cvs.courtviolationstatusid      
 where (cvs.violationcategory in(0,2) and isnull(statustype,0) <>1)         
  and tv.ticketid_pk=@ticketID      
 ) as toProcess      
*/      
(select count(v.ticketid_pk)    
FROM    dbo.tblCourtViolationStatus AS c INNER JOIN    
  dbo.tblDateType AS d ON c.CategoryID = d.TypeID RIGHT OUTER JOIN    
  dbo.tblTicketsViolations AS v ON c.CourtViolationStatusID = v.CourtViolationStatusIDmain    
where v.ticketid_pk = @ticketID    
and  isnull(d.isactive,0) = 0    
) as toprocess    
    
    
    
FROM dbo.tblTicketsPayment TP         
right OUTER JOIN                            
 dbo.tblTickets T         
ON  TP.TicketID = T.TicketID_PK         
AND  (TP.PaymentVoid <> 1)          
and  TP.paymenttype <>99       
      
WHERE (T.TicketID_PK = @ticketID)  --not to include balance due amount         
GROUP BY         
 T.TicketID_PK, T.Activeflag   
end                          
                          
else          --if quote or non-client                
 begin                           
 SELECT                               
  T.TicketID_PK,                          
  ISNULL(T.totalfeecharged, 0) AS TotalFee,                           
  T.Activeflag,              
    
/*  (select count(tv.ticketid_pk) from tblticketsviolations tv      
 left outer join tblcourtviolationstatus cvs on       
 tv.courtviolationstatusidmain=cvs.courtviolationstatusid      
 where (cvs.violationcategory in(0,2) and isnull(statustype,0) <>1)        
  and tv.ticketid_pk=@ticketID      
 ) as toProcess                          
*/    
(select count(v.ticketid_pk)    
FROM    dbo.tblCourtViolationStatus AS c INNER JOIN    
  dbo.tblDateType AS d ON c.CategoryID = d.TypeID RIGHT OUTER JOIN    
  dbo.tblTicketsViolations AS v ON c.CourtViolationStatusID = v.CourtViolationStatusIDmain    
where v.ticketid_pk = @ticketID    
and  isnull(d.isactive,0) = 0    
) as toprocess    
 FROM                           
                       dbo.tblTickets T                                                  
 WHERE     (T.TicketID_PK = @ticketID)                             
 end              
          
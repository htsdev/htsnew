﻿set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

/****** 
Created by:   Fahad Muhammad Qureshi.
Created Date: 10/01/2009
Task ID    :  5376

Business Logic : this procedure is update the bussiness logic.

List of Parameters:	
			@MenuID : Menu id 
			@BussinessLogic	: Bussiness logic of a page

List of Columns:

******/   
 
CREATE PROCEDURE [dbo].[USP_HTP_Update_BussinessLogic]
@MenuID int ,
@BussinessLogic VARCHAR(MAX) 
as  
IF EXISTS (SELECT * FROM tbl_Report tr WHERE tr.Menu_ID=@MenuID)
BEGIN
UPDATE tbl_Report
SET
	BusinessLogic = @BussinessLogic
	
WHERE 
	 Menu_ID=@MenuID
	 
END
ELSE
	BEGIN
		INSERT INTO tbl_Report
		(			
			Report_Name,
			Report_Url,
			Menu_ID,
			BusinessLogic
		)
		SELECT tts.TITLE,tts.[URL],tts.id,@BussinessLogic FROM tbl_TAB_SUBMENU tts WHERE tts.ID=@MenuID		
	END
	 
SELECT @@ROWCOUNT

GO
GRANT EXECUTE ON [dbo].[USP_HTP_Update_BussinessLogic] TO dbr_webuser
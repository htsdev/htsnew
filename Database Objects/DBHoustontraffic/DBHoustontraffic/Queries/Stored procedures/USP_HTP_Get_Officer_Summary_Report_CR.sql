﻿
--[USP_HTP_Get_Officer_Summary_Report_CR] '04/30/2009','LocalHost'
 /****** 
 * Created by: Muhammad Nasir
 * Business Logic : This Stored Procedure is used to Generate Officer summary Report
 
		1. Defendants should be our clients. 
		2. Violation’s type should not be in the following:
					   i Was there an Accident?
					   ii Do you have a CDL?
					   iii Are you on probation?
					   iv Tickets Start with N.
					   v Tickets Start with T.
		3. Violation category should be 'Non-Moving' and court violation status should not be in the following: 
					   i ARRAIGNMENT WAITING.
					   ii WAITING. 
		4. Case must be associated with any HMC court.
		5. Court date should match with selected date.		
		7. Payment should not be "Void".
		8. Payment type should not be "Balance Due" or "Checks in Mail".
		9. Default firm should be assumed as "Sullo & Sullo".
		10. court location should be inside court(HMC)

List of Parameters :

@courtdate 
@URL 
@isdatafound 

List of Columns

casenumber 
Client Name 
CDL/Bond
Court room
Court Time
Violation description

Task: Nasir 5817 04/29/2009
******/

--

ALTER procedure [dbo].[USP_HTP_Get_Officer_Summary_Report_CR]
@courtloc varchar(10) = 'None',     
@courtdate varchar(12) = '05/11/2009',     
@URL varchar(200) = 'ln.legalhouston.com'    

as     
--Fahad 10378 01/09/2013 HMC-W also excluded along other HMC Courts
if @Courtloc!='0' AND @Courtloc!='Traffic' and @Courtloc!='none' and @Courtloc!='OU' and @Courtloc!='3001' and @Courtloc!='3002' and @Courtloc!='3003' and @Courtloc!='3075'
BEGIN
	SET @courtdate='09/09/1900'
END
    
create table #TempExists  
(TableName varchar(20),  
IsExists int)   
   
Declare @NameDiscrepancy int     
set @NameDiscrepancy =19     

set nocount on     
     
SELECT dbo.tblTickets.TicketID_PK, dbo.tblTickets.Firstname,  dbo.tblTickets.Lastname,     
Ofirstname =     
case     
when left(dbo.tblcourtviolationstatus.Description,3) = 'ARR' then dbo.FN_GetCaseSequences(dbo.tblTickets.ticketid_pk)     
else     
isnull(dbo.tblOfficer.FirstName,'N/A')     
end     
,     
Olastname = case     
when left(dbo.tblcourtviolationstatus.Description,3) = 'ARR' then ' '     
else     
     
isnull(dbo.tblOfficer.LastName,'N/A')     
end,     
   dbo.tblTicketsViolations.ticketOfficerNumber AS OfficerNumber,  
 dbo.tblTickets.CaseTypeId as CaseType,   
dbo.tblViolations.ShortDesc,   
 dbo.tblTicketsViolations.courtdatemain as currentdateset,     
dbo.tblTicketsViolations.courtid as currentcourtloc,     
--Sabir Khan 7979 07/12/2010 fix trial docket issue...   
case when len(ltrim(rtrim(isnull(dbo.tblTicketsViolations.courtnumbermain,'')))) = 0 then '0' else convert(varchar,dbo.tblTicketsViolations.courtnumbermain) end as currentcourtnum,  
dbo.tblcourtviolationstatus.ShortDescription as description,activeflag,     
  
bondflag, CDLFlag AS CFlag,  
speeding = dbo.getviolationspeed_ver1(0,refcasenumber,fineamount,violationcode),     
convert(varchar(2),month(dbo.tblTicketsViolations.courtdatemain)) + '/' + convert(varchar(2),day(dbo.tblTicketsViolations.courtdatemain)) + '/' + convert(varchar(4),year(dbo.tblTicketsViolations.courtdatemain)) as courtarrdate,     
dbo.formattime(courtdatemain) AS courttime,
dbo.tblCaseDispositionstatus.description as ViolationStatusID,     
dbo.tblTicketsViolations.violationnumber_pk as violationnumber,address1,     
case     
when dbo.tblTicketsViolations.courtid in (3001, 3002,3003,3075) then midnum  --Fahad 10378 01/09/2013 HMC-W also included along other HMC Courts   
when dbo.tblTicketsViolations.courtid not in (3001, 3002,3003,3075) and (casenumassignedbycourt not like '' and not (casenumassignedbycourt is null)) then casenumassignedbycourt     --Fahad 10378 01/09/2013 HMC-W also excluded along other HMC Courts
when dbo.tblTicketsViolations.courtid not in (3001, 3002,3003,3075) and (casenumassignedbycourt like ' ' or casenumassignedbycourt like '' or casenumassignedbycourt is null) then RefCasenumber     --Fahad 10378 01/09/2013 HMC-W also excluded along other HMC Courts
else ' ' end as midnum,     
     
violationtype, dbo.tblCourts.Address as courtaddress,     
dbo.tblcourtviolationstatus.categoryid as courtviolationstatusid     
,refcasenumber,ISNULL(dbo.tblTickets.TrialComments,'') AS TrialComments -- Noufil 5895 05/11/2009 Trialc omments added
into #temp     
FROM dbo.tblTickets INNER JOIN     -- Adil 7635 04/09/2010 changes in joins
 dbo.tblTicketsViolations ON dbo.tblTickets.TicketID_PK = dbo.tblTicketsViolations.TicketID_PK INNER JOIN     
 dbo.tblCaseDispositionstatus ON dbo.tblTicketsViolations.violationstatusid = dbo.tblCaseDispositionstatus.casestatusid_pk INNER JOIN     
 dbo.tblfirm ON dbo.tblTickets.firmid = dbo.tblfirm.firmid INNER JOIN     
 dbo.tblViolations ON dbo.tblTicketsViolations.ViolationNumber_PK = dbo.tblViolations.ViolationNumber_PK INNER JOIN     
 dbo.tblcourtviolationstatus ON dbo.tblTicketsviolations.courtviolationstatusidmain = dbo.tblcourtviolationstatus.courtviolationstatusID INNER JOIN     
 dbo.tblCourts ON dbo.tblTicketsViolations.courtid = dbo.tblCourts.Courtid LEFT OUTER JOIN     
 dbo.tblOfficer ON dbo.tblTicketsViolations.ticketOfficerNumber = dbo.tblOfficer.OfficerNumber_PK       
 LEFT OUTER JOIN tblReminderstatus ON tblTicketsViolations.ReminderCallStatus = tblReminderstatus.Reminderid_pk     
WHERE (dbo.tblViolations.Violationtype not IN (1) or dbo.tblViolations.violationnumber_pk in (11,12) ) AND (dbo.tblcourtviolationstatus.categoryid IN (select distinct categoryid from tblcourtviolationstatus where violationcategory in (0,2)     
    
and categoryid not in (7,50) and courtviolationstatusid not in (104, 201) ))     
AND (dbo.tblTickets.Activeflag = 1)     
and dbo.tblTicketsViolations.courtid <> 0     
and datediff(DAY,dbo.tblTicketsViolations.courtdatemain,@courtdate) = 0     
and dbo.tblTicketsViolations.ticketOfficerNumber is not null
 declare @sqlquery varchar(max)     
 declare @sqlquery1 varchar(max)     
     
 set @sqlquery = ' '     
     
 SELECT #temp.*,count(*) as clientcount into #temp4     
 FROM #temp RIGHT OUTER JOIN     
 tblTickets T ON #temp.Firstname = T.Firstname AND #temp.Lastname = T.Lastname AND     
 (isnull(#temp.Address1,'N/A') = isnull(T.Address1,'N/A') or #temp.midnum = T.midnum)     
 where t.activeflag = 1     
 GROUP BY     
 #temp.TicketID_PK, #temp.Firstname, #temp.Lastname, #temp.Ofirstname,     
 #temp.Olastname,#temp.OfficerNumber, #temp.ShortDesc, #temp.currentdateset, #temp.currentcourtloc,     
 #temp.currentcourtnum, #temp.Description, #temp.activeflag,      
 #temp.bondflag,#temp.CFlag  ,    
 #temp.speeding, #temp.courtarrdate, #temp.courttime, #temp.ViolationStatusID,     
 #temp.violationnumber, #temp.address1, #temp.midnum, #temp.violationtype, #temp.courtaddress,     
 #temp.courtviolationstatusid, #temp.refcasenumber,#temp.TrialComments
 ,#temp.CaseType         
                         
 set @sqlquery = 'select P.TicketID_PK,P.Firstname,P.Lastname,P.Ofirstname,P.Olastname,p.OfficerNumber,P.CaseType,      
 replace(P.ShortDesc,''None'',''Other'') as shortdesc,P.currentdateset,     
 P.currentcourtloc,P.currentcourtnum,P.Description,P.activeflag,    
P.bondflag,p.CFlag,P.speeding,     
 P.courtarrdate,P.courttime,P.ViolationStatusID,     
 P.violationnumber,P.clientcount,T.coveringfirmid,dbo.fn_HTS_GetCoveringFirm(p.ticketid_pk,''0'') as coveringfirm,     
 P.violationtype,courtaddress,cdlflag,courtviolationstatusid,     
 convert(varchar(20),P.TicketID_PK) + isnull(convert(varchar(4),courtviolationstatusid),0) as ticketstatusid,  
  
AccidentFlag, p.midnum, p.refcasenumber,p.TrialComments, t.dob   
  into #temp5     
 from #temp4 P,dbo.tbltickets T,dbo.tblfirm F where P.ticketid_pk = T.ticketid_pk and   
 case when T.coveringfirmid = 0 then 3000 else T.coveringfirmid end = F.firmid and P.ticketid_pk is not null '   
   
 set @sqlquery = @sqlquery + ' and P.currentcourtloc in (3001,3002,3003,3075)'  --Fahad 10378 01/09/2013 HMC-W also Included along other HMC Courts  
  

 if @courtdate is not null and @courtdate <> '01/01/1900'     
 begin     
 set @sqlquery = @sqlquery + ' and datediff(day,P.currentdateset,''' + @courtdate + ''') = 0'     
 end     

 set @sqlquery = @sqlquery + ' and ((P.ShortDesc not in (''CDL'',''ACC'',''PROB'')) or (P.ShortDesc is null)) order by P.courtarrdate,     
 P.currentcourtloc,convert(varchar,P.currentcourtnum) asc,P.courttime,P.lastname,P.firstname
 '     
     
 set @sqlquery = @sqlquery + 'select * into #violations from tblviolations where description like ''%speeding%'' and violationtype <> 0 '     
     
 set @sqlquery = @sqlquery + 'select T.*, isnull(V.violationnumber_pk,0) as speedingvnum     
 into #temp6 from #temp5 T     
 left outer join #violations V on T.violationnumber = V.violationnumber_pk     
 order by case T.courtviolationstatusid when 9 then 9     
 when 0 then 9 else 1 end     
 ,T.courtarrdate,T.currentcourtloc,convert(varchar,T.currentcourtnum) asc,     
 T.courttime,T.lastname,T.firstname,T.courtviolationstatusid     
 drop table #temp5     
 declare @ticketID int     
 declare @tbl1 table ( ticketid int, shortdesc1 varchar(1000))     
 declare @strVaue varchar(200)     
 declare Crtemp cursor for     
 select distinct ticketID_pk from #temp6     
   
 open Crtemp     
 fetch next from Crtemp     
 into @ticketid     
 WHILE @@FETCH_STATUS = 0     
 BEGIN     
 set @strVaue = ''&nbsp;&nbsp;''     
 select @strVaue = @strVaue+ case isnull(shortdesc,'''') when ''spd'' then ''spd,'' else isnull(shortdesc,'''')+'','' end from #temp6 where ticketID_pk = @ticketid     
 select distinct @strVaue = case @strVaue when ''spd,'' then ''spd,'' else @strVaue end +case abs(speeding)when '' '' then '' '' else ''(''+convert(varchar(20),abs(speeding))+ '')'' end from #temp6 where ticketID_pk = @ticketid     
 if len(@strvaue) > 50     
 set @strvaue = stuff(@strvaue,charindex('','',@strvaue,50)+1,0,'' '')     
 insert into @tbl1 values(@ticketid, @strVaue)     
 fetch next from Crtemp     
 into @ticketid     
 END     
 close Crtemp     
 DEALLOCATE Crtemp     
 SELECT TicketId_Pk as CaseNumber,     
 left(t2.shortdesc1,len(t2.shortdesc1)-1) as shortdesc1, TicketID_PK ,     
 Firstname+'' ''+lastName as FMname,     
 case Olastname+'',''+Ofirstname when ''N/A,N/A'' then '''' else Olastname+'' ''+Ofirstname end OLFNAME , OfficerNumber,    
 currentcourtloc,     
 currentcourtnum, Description,       
 bondflag,CFlag, courttime,     
   
 case clientcount when 1 then '''' else ''*''+convert(varchar(5),clientcount) end ClientCount,     
coveringfirm,dbo.formattime(t1.currentdateset) as currentdateset      
 , midnum, refcasenumber,TrialComments 
 into #temp7     
 FROM #temp6 t1, @tbl1 t2     
 where t2.ticketid = t1.TicketID_PK     
 order by currentcourtnum     
   
 SELECT casenumber, isnull(shortdesc1,'''') shortdesc1, TicketID_PK ,     
 FMname,OLFNAME ,OfficerNumber, currentcourtloc,    
case currentcourtnum     
 when '''' then '''' else currentcourtnum end     
currentcourtnum    
, Description, case CFlag when 0 then ''''  
    else ''CDL''  
    end   
    + case CFlag when 1 then  case BondFlag when 1 then ''/'' else '''' end else '''' end   
    +  
    case BondFlag when 0 then ''''  
    else ''Bond''  
    end   
    as  BondFlag,  
'    
    
 set @sqlquery = @sqlquery +'  courttime,     
 clientcount as clientcount,coveringfirm as CoveringFirm,currentdateset,midnum ,refcasenumber,TrialComments  
 into #temp8 FROM #temp7     
 order by OLFNAME     
     
 '     
 --print @sqlquery     
     
 set @sqlquery1='';     
   
   
 set @sqlquery1=@sqlquery1+'     
  
SELECT distinct casenumber,     
  isnull(shortdesc1,'''') shortdesc1, TicketID_PK ,     
  FMname,OLFNAME ,OfficerNumber, currentcourtloc,     
 currentcourtnum, Description,   BondFlag,     
  courttime,     
 clientcount, CoveringFirm,currentdateset,     
 midnum, refcasenumber,TrialComments  into #temp9 FROM #temp8     
   
 ;  
   
   
     
 SELECT distinct casenumber,shortdesc1,TicketID_PK ,     
 FMname, OLFNAME, OfficerNumber,currentcourtloc,currentcourtnum, left(Description, 3) as Description,  BondFlag, TrialComments,    
  courttime,clientcount,CoveringFirm ,currentdateset,     
   isnull(midnum, right(refcasenumber,7)) as midnum    
 into #temp10    
 from #temp9 where Description Not In(''BOND'',''COM'',''DSCD'', ''DAR'',''DPX'',''CLO'',''B/W'',''WAIT'''     
 
 
     
 set @sqlquery1=@sqlquery1+ ' ) and Description in (''ARR'',''PRE'',''JUR'',''JUR2'',''JUD'',''BF'',''A/W'',''TRI'') order by currentcourtloc, currentcourtnum, courttime  ;     
 --Sabir Khan 6556 09/09/2009 column sequance has been set for insertion.
 --insert into #temp10     
 insert into #temp10(casenumber,shortdesc1,Ticketid_pk,FMname,OLFNAME,OfficerNumber,currentcourtloc,currentcourtnum,description,BondFlag,courttime,clientcount,CoveringFirm,currentdateset,TrialComments,midnum)	
 SELECT distinct casenumber,shortdesc1,TicketID_PK ,     
 left(FMname,13) as FMname ,left(OLFNAME,13) as OLFNAME ,OfficerNumber, currentcourtloc,currentcourtnum, left(Description, 3) as Description, BondFlag,     
 courttime,clientcount,CoveringFirm, currentdateset, TrialComments,    
   isnull(midnum, right(refcasenumber,7)) as midnum    
 from #temp9 where Description Not In(''BOND'',''COM'',''JUR2'',''DSCD'',''DAR'',''DPX'',''CLO'',''B/W'',''WAIT'' 
 
 '   
    
    
     
 set @sqlquery1=@sqlquery1+ ' ) and Description not in (''ARR'',''PRE'',''JUR'',''JUD'',''BF'',''A/W'',''TRI'') order by currentcourtloc, currentcourtnum, courttime ;     
 
 SELECT COUNT(t.OfficerNumber) as countofficer  
      ,t.OfficerNumber as OfficerNumber   
      ,t.currentcourtnum as  currentcourtnum into #tempgro  
FROM #temp10 t  
GROUP BY         
      t.currentcourtnum  
      ,t.OfficerNumber 


      SELECT OfficerNumber as OfficerNumber INTO #tempgrc   
FROM   #tempgro  
GROUP BY  
       OfficerNumber
HAVING COUNT(OfficerNumber)>1 
 
 if exists(select ''temp9'', * from #temp9) begin insert into #TempExists values(''temp9'', 1) end   
 select *, ''' + @URL + ''' as URL from #temp10 where OLFNAME != '''' and OfficerNumber in (select OfficerNumber from #tempgrc)   order by  OLFNAME,currentcourtloc, currentcourtnum, courttime ;     
 if exists(select ''temp10'', * from #temp10) begin insert into #TempExists values(''temp10'', 1) end '  
  
     
     
 set @sqlquery1=@sqlquery1+ 'drop table #temp6;drop table #temp7;drop table #temp8;drop table #temp9;drop table #temp10;'      
     
--select len(@sqlquery) + len(@sqlquery1)     
     
 print @sqlquery  
 print @sqlquery1     
 exec (@sqlquery+ ' '+@sqlquery1)     
    
 drop table #temp4     
     
--end     
drop table #temp     
    
    


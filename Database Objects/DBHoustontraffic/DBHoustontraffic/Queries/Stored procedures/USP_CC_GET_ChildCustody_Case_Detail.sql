set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

/*********

Created By		: Zeeshan Ahmed

Business Logic	: This Procedure is used to get list of child custody case details

List of Input Parameters

		@ChildCustodyID Child Custody Case Id

List Of Columns :

		CaseNumber
		Style
		PartyRequest
		Result
		SettingDate
		CurrentCourt
		FileCourt
		NextSettingDate
		FileLocation
		Plaintiffs
		Defendants
		JudgementFor
		JudgementDate
		ImageNumber
		Volume
		PageNo
		Pgs
		Time
		CaseStatus
		CaseType
		SettingType     
		CourtName
		RecLoadDate
		ClientFlag

*********/

Create PROCEDURE [dbo].[USP_CC_GET_ChildCustody_Case_Detail] 
(
	@ChildCustodyID int
)AS


SELECT 

CC.CaseNumber,
CC.Style,
CC.PartyRequest,
CC.Result,
case when datediff( day , '01/01/1900',isnull(CC.SettingDate,'01/01/1900'))=0 then 'N/A' else convert(varchar , CC.SettingDate,101) end as SettingDate , 
CC.CurrentCourt,
CC.FileCourt,
case when datediff( day , '01/01/1900',isnull(CC.NextSettingDate,'01/01/1900'))=0 then 'N/A' else convert(varchar , CC.NextSettingDate,101) end as NextSettingDate , 
CC.FileLocation,
CC.Plaintiffs,
CC.Defendants,
CC.JudgementFor,
case when datediff( day , '01/01/1900',isnull(CC.JudgementDate,'01/01/1900'))=0 then 'N/A' else convert(varchar , CC.JudgementDate,101) end as JudgementDate , 
CC.ImageNumber,
CC.Volume,
CC.PageNo,
CC.Pgs,
CC.Time,
CCS.CaseStatusName as CaseStatus  ,  

CT.CaseTypeName as CaseType,
CST.SettingTypeName as SettingType ,     
CRT.CourtName,
case when datediff( day , '01/01/1900',isnull(CC.RecLoadDate,'01/01/1900'))=0 then 'N/A' else convert(varchar , CC.RecLoadDate,101) end as RecLoadDate , 
ClientFlag
FROM 
ChildCustody CC    
    
Join    
ChildCustodySettingType CST     
 ON CC.ChildCustodySettingTypeID = CST.ChildCustodySettingTypeID    
Join     
ChildCustodyCaseStatus CCS    
On CC.ChildCustodyCaseStatusId = CCS.ChildCustodyCaseStatusId   

join 
CaseType CT
On Ct.CaseTypeid = cc.CaseTypeID

join 
childcustodyCourt CRT
On CRT.ChildCustodyCourtid = CC.ChildCustodyCourtId


WHERE 
	CC.ChildCustodyID = @ChildCustodyID 
Order by ChildCustodyId desc 




go


grant execute on dbo.USP_CC_GET_ChildCustody_Case_Detail  to dbr_webuser

go
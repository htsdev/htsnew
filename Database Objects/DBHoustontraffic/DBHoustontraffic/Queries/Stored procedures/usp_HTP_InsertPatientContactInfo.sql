﻿/******  
* Created By :	  Syed Muhammad Ozair.
* Create Date :   11/05/2010 
* Task ID :		  8501
* Business Logic :  This procedure is used to insert patient contact information along with source and status and returns inserted pateint id.
* List of Parameter :
* Column Return :     
******/

ALTER PROCEDURE dbo.usp_HTP_InsertPatientContactInfo
	@TicketId INT,
	@LastName VARCHAR(50),
	@FirstName VARCHAR(50),
	@Address VARCHAR(200),
	@City VARCHAR(50),
	@StateId INT,
	@Zip VARCHAR(10),
	@Contact1 VARCHAR(15),
	@ContactType1 INT,
	@Contact2 VARCHAR(15),
	@ContactType2 INT,
	@Email VARCHAR(255),
	@Email2 VARCHAR(255),
	@DOB DATETIME,
	@Occupation VARCHAR(100),
	@Height VARCHAR(10),
	@Weight VARCHAR(10),
	@EmployeeId INT,
	@SpanishSpeaker TINYINT, --Saeed 8585 12/04/2010 update spanish speaker field.
	@SsnNo VARCHAR(100)
AS
	INSERT INTO Patient
	  (
	    TicketId,
	    LastName,
	    FirstName,
	    [Address],
	    City,
	    StateId,
	    Zip,
	    Contact1,
	    ContactType1,
	    Contact2,
	    ContactType2,
	    Email,
	    Email2,
	    DOB,
	    Occupation,
	    Height,
	    [Weight],
		EmployeeId,
		SpanishSpeaker,
		SsnNo
	  )
	VALUES
	  (
	  @TicketId,
	    @LastName,
	    @FirstName,
	    @Address,
	    @City,
	    @StateId,
	    @Zip,
	    @Contact1,
	    @ContactType1,
	    @Contact2,
	    @ContactType2,
	    @Email,
	    @Email2,
	    @DOB,
	    @Occupation,
	    @Height,
	    @Weight,
	    @EmployeeId,
	    @SpanishSpeaker,
	    @SsnNo
	  )
	
	SELECT SCOPE_IDENTITY()


/*
* Business Logic : This Store Procedure is used to display the Dallas Municipal Court Mailer Break down for the last sended mailer. . . 
* 
*/
/*
* Parameters	:	@MailerPrintDate DATETIME
* 
*/
-- USP_Mailer_DMC_ARR_BreakDown '11/07/2012'
CREATE PROCEDURE USP_Mailer_DMC_ARR_BreakDown
(
	@MailerPrintDate DATETIME
)
AS 
BEGIN
Declare @TotalRecordsInDataFile int
Declare @TotalRecordsUploaded int
Declare @TotalNumOfClientInDataFile int
Declare @TotalNumOfClientUploaded int
Declare @TotalInArr int
Declare @ExcludedDoNotMail int
Declare @ExcludedBadAddress int
Declare @ExcludedClientAndQuote int
Declare @ExcludedAddressChangeinPast48Months int
Declare @ExcludedTicketNumberStartsWith_V int
Declare @ExcludedNotNullViolationDate int
Declare @ExcludeAlreadyPrinted int
Declare @ExcldeWebSiteStatuses int


SELECT DISTINCT  Groupid AS  Groupids
INTO #groupIdsTable
from LoaderFilesArchive.dbo.Dallas_Arraingment_DataArchive where datediff(day,Insert_Date,@MailerPrintDate) = 0

-- Total Records in data file
select  @TotalRecordsInDataFile = Count(DISTINCT [Ticket Number])  
from LoaderFilesArchive.dbo.Dallas_Arraingment_DataArchive where datediff(day,Insert_Date,@MailerPrintDate) = 0


-- Total Records Uploaded
select @TotalRecordsUploaded = Count(DISTINCT TicketNumber_pk) 
from DallasTrafficTickets.dbo.tblticketsviolationsarchive where datediff(DAY, InsertDate,@MailerPrintDate) = 0
AND InsertionLoaderId = 3


-- Total Number of clients in data file
SELECT @TotalNumOfClientInDataFile = COUNT( DISTINCT [First Name] +' '+ [Last Name] + ' ' + [Address Part I])
FROM LoaderFilesArchive.dbo.Dallas_Arraingment_DataArchive
WHERE GroupID IN (SELECT Groupids FROM #groupIdsTable)

-- Total Number of Clients Uploaded

SELECT @TotalNumOfClientUploaded = COUNT(DISTINCT ta.RecordID)
from DallasTrafficTickets.dbo.tblticketsviolationsarchive tva 
		inner join DallasTrafficTickets.dbo.tblticketsarchive ta on ta.recordid = tva.recordid
where datediff(DAY, ta.RecLoadDate,@MailerPrintDate) = 0
AND tva.InsertionLoaderId = 3





select DISTINCT REPLACE( dad.[Ticket Number], '-' , '') AS midnumber, ta.RecordID into #temp 
from DallasTrafficTickets.dbo.tblticketsviolationsarchive tva inner join DallasTrafficTickets.dbo.tblticketsarchive ta on ta.recordid = tva.recordid
INNER JOIN LoaderFilesArchive.dbo.Dallas_Arraingment_DataArchive dad ON REPLACE( dad.[Ticket Number], '-' , '') = tva.TicketNumber_PK
where datediff(DAY, dad.Insert_Date,@MailerPrintDate) = 0
AND tva.InsertionLoaderId = 3


select @TotalInArr = Count(Distinct ta.recordid)  
from DallasTrafficTickets.dbo.tblticketsviolationsarchive tva
inner join DallasTrafficTickets.dbo.tblticketsarchive ta on ta.recordid = tva.recordid
inner join #temp t on t.midnumber = tva.TicketNumber_PK
where datediff(DAY, ta.RecLoadDate,@MailerPrintDate) = 0
AND tva.InsertionLoaderId = 3
and tva.violationstatusid = 116
and tva.courtlocation = 3049
and LEN(ISNULL(RTRIM(LTRIM(ta.lastname)),''))<>0
and LEN(ISNULL(RTRIM(LTRIM(ta.firstname)),''))<>0



select @ExcludedAddressChangeinPast48Months = Count(Distinct ta.recordid)  
from DallasTrafficTickets.dbo.tblticketsviolationsarchive tva
inner join DallasTrafficTickets.dbo.tblticketsarchive ta on ta.recordid = tva.recordid
inner join #temp t on t.midnumber = tva.TicketNumber_PK
where datediff(DAY, ta.RecLoadDate,@MailerPrintDate) = 0
AND tva.InsertionLoaderId = 3
AND tva.violationstatusid = 116
and tva.courtlocation = 3049
and LEN(ISNULL(RTRIM(LTRIM(ta.lastname)),''))<>0
and LEN(ISNULL(RTRIM(LTRIM(ta.firstname)),''))<>0
and isnull(ta.ncoa48flag,0) = 0


select @ExcludedNotNullViolationDate = Count(Distinct ta.recordid)  
from DallasTrafficTickets.dbo.tblticketsviolationsarchive tva
inner join DallasTrafficTickets.dbo.tblticketsarchive ta on ta.recordid = tva.recordid
inner join #temp t on t.midnumber = tva.TicketNumber_PK
where datediff(DAY, ta.RecLoadDate,@MailerPrintDate) = 0
AND tva.InsertionLoaderId = 3
AND tva.violationstatusid = 116
and tva.courtlocation = 3049
and LEN(ISNULL(RTRIM(LTRIM(ta.lastname)),''))<>0
and LEN(ISNULL(RTRIM(LTRIM(ta.firstname)),''))<>0
and isnull(ta.ncoa48flag,0) = 0
and  tva.violationbonddate is null


select @ExcludedTicketNumberStartsWith_V = Count(Distinct ta.recordid)  
from DallasTrafficTickets.dbo.tblticketsviolationsarchive tva
inner join DallasTrafficTickets.dbo.tblticketsarchive ta on ta.recordid = tva.recordid
inner join #temp t on t.midnumber = tva.TicketNumber_PK
where datediff(DAY, ta.RecLoadDate,@MailerPrintDate) = 0
AND tva.InsertionLoaderId = 3
AND tva.violationstatusid = 116
and tva.courtlocation = 3049
and LEN(ISNULL(RTRIM(LTRIM(ta.lastname)),''))<>0
and LEN(ISNULL(RTRIM(LTRIM(ta.firstname)),''))<>0
and isnull(ta.ncoa48flag,0) = 0
and  tva.violationbonddate is null
and charindex('v',tva.ticketnumber_pk)<>1


select @ExcludedBadAddress = Count(Distinct ta.recordid)  
from DallasTrafficTickets.dbo.tblticketsviolationsarchive tva
inner join DallasTrafficTickets.dbo.tblticketsarchive ta on ta.recordid = tva.recordid
inner join #temp t on t.midnumber = tva.TicketNumber_PK
where datediff(DAY, ta.RecLoadDate,@MailerPrintDate) = 0
AND tva.InsertionLoaderId = 3
AND tva.violationstatusid = 116
and tva.courtlocation = 3049
and LEN(ISNULL(RTRIM(LTRIM(ta.lastname)),''))<>0
and LEN(ISNULL(RTRIM(LTRIM(ta.firstname)),''))<>0
and isnull(ta.ncoa48flag,0) = 0
and  tva.violationbonddate is null
and charindex('v',tva.ticketnumber_pk)<>1
and Flag1 in ('Y','D','S')


SELECT @ExcludedDoNotMail = Count(Distinct ta.recordid)  
FROM DallasTrafficTickets.dbo.tblticketsviolationsarchive tva
inner join DallasTrafficTickets.dbo.tblticketsarchive ta ON ta.recordid = tva.recordid
inner join #temp t ON t.midnumber = tva.TicketNumber_PK
where datediff(DAY, ta.RecLoadDate,@MailerPrintDate) = 0
AND tva.InsertionLoaderId = 3
AND tva.violationstatusid = 116
and tva.courtlocation = 3049
and LEN(ISNULL(RTRIM(LTRIM(ta.lastname)),''))<>0
and LEN(ISNULL(RTRIM(LTRIM(ta.firstname)),''))<>0
and isnull(ta.ncoa48flag,0) = 0
and  tva.violationbonddate is null
and charindex('v',tva.ticketnumber_pk)<>1
and Flag1 in ('Y','D','S')
and donotmailflag = 0


select @ExcldeWebSiteStatuses = Count(Distinct ta.recordid)  
from DallasTrafficTickets.dbo.tblticketsviolationsarchive tva
inner join DallasTrafficTickets.dbo.tblticketsarchive ta on ta.recordid = tva.recordid
inner join #temp t on t.midnumber = tva.TicketNumber_PK
where datediff(DAY, ta.RecLoadDate,@MailerPrintDate) = 0
AND tva.InsertionLoaderId = 3
AND tva.violationstatusid = 116
and tva.courtlocation = 3049
and LEN(ISNULL(RTRIM(LTRIM(ta.lastname)),''))<>0
and LEN(ISNULL(RTRIM(LTRIM(ta.firstname)),''))<>0
and isnull(ta.ncoa48flag,0) = 0
and  tva.violationbonddate is null
and charindex('v',tva.ticketnumber_pk)<>1
and Flag1 in ('Y','D','S')
and donotmailflag = 0
AND isnull(tva.WebSiteCourtStatus,0) not in ((SELECT wscs.ID  FROM DallasTrafficTickets.dbo.WebSiteCourtStatus wscs WHERE (wscs.MailAllowed=0 AND wscs.WebSite='DMC')))


select @ExcludedClientAndQuote = Count(Distinct ta.recordid)  
from DallasTrafficTickets.dbo.tblticketsviolationsarchive tva
inner join DallasTrafficTickets.dbo.tblticketsarchive ta on ta.recordid = tva.recordid
inner join #temp t on t.midnumber = tva.TicketNumber_PK
where datediff(DAY, ta.RecLoadDate,@MailerPrintDate) = 0
AND tva.InsertionLoaderId = 3
AND tva.violationstatusid = 116
and tva.courtlocation = 3049
and LEN(ISNULL(RTRIM(LTRIM(ta.lastname)),''))<>0
and LEN(ISNULL(RTRIM(LTRIM(ta.firstname)),''))<>0
and isnull(ta.ncoa48flag,0) = 0
and  tva.violationbonddate is null
and charindex('v',tva.ticketnumber_pk)<>1
and Flag1 in ('Y','D','S')
and donotmailflag = 0
AND isnull(tva.WebSiteCourtStatus,0) not in ((SELECT wscs.ID  FROM DallasTrafficTickets.dbo.WebSiteCourtStatus wscs WHERE (wscs.MailAllowed=0 AND wscs.WebSite='DMC')))
AND ta.RecordID NOT IN(
select v.recordid from dallastraffictickets.dbo.tblticketsviolations v inner join dallastraffictickets.dbo.tbltickets t 
on t.ticketid_pk = v.ticketid_pk and t.activeflag = 1 
inner join #temp a on isnull(a.recordid,0) = isnull(v.recordid,0))



select @ExcludeAlreadyPrinted = Count(Distinct ta.recordid)  
from DallasTrafficTickets.dbo.tblticketsviolationsarchive tva
inner join DallasTrafficTickets.dbo.tblticketsarchive ta on ta.recordid = tva.recordid
inner join #temp t on t.midnumber = tva.TicketNumber_PK
where datediff(DAY, ta.RecLoadDate,@MailerPrintDate) = 0
AND tva.InsertionLoaderId = 3
AND tva.violationstatusid = 116
and tva.courtlocation = 3049
and LEN(ISNULL(RTRIM(LTRIM(ta.lastname)),''))<>0
and LEN(ISNULL(RTRIM(LTRIM(ta.firstname)),''))<>0
and isnull(ta.ncoa48flag,0) = 0
and  tva.violationbonddate is null
and charindex('v',tva.ticketnumber_pk)<>1
and Flag1 in ('Y','D','S')
and donotmailflag = 0
AND isnull(tva.WebSiteCourtStatus,0) not in ((SELECT wscs.ID  FROM DallasTrafficTickets.dbo.WebSiteCourtStatus wscs WHERE (wscs.MailAllowed=0 AND wscs.WebSite='DMC')))
AND ta.RecordID NOT IN (
select v.recordid from dallastraffictickets.dbo.tblticketsviolations v inner join dallastraffictickets.dbo.tbltickets t 
on t.ticketid_pk = v.ticketid_pk and t.activeflag = 1 
inner join #temp a on isnull(a.recordid,0) = isnull(v.recordid,0))
AND ta.recordid not in (                                                                    
		select	recordid from tblletternotes 
		where 	lettertype = 29
		union  
 select recordid from dbo.tblletternotes where lettertype in (18,30)   
 and datediff(day, recordloaddate, dbo.fn_getprevbusinessday(GETDATE(),5)) <=0  
)




CREATE TABLE #TempFinal(Filters varchar(max),TotalCount varchar(100))


Insert into #TempFinal (Filters,TotalCount)
Select 'Total records in Data file', @TotalRecordsInDataFile 

Insert into #TempFinal (Filters,TotalCount)
select 'Total number of clients in data file',@TotalNumOfClientInDataFile

Insert into #TempFinal (Filters,TotalCount)
Select 'Total number of records uploaded',@TotalRecordsUploaded

Insert into #TempFinal (Filters,TotalCount)
select 'Total number of clients uploaded',@TotalNumOfClientUploaded

Insert into #TempFinal (Filters,TotalCount)
Select 'Total Clients in Arraignment Status',@TotalInArr

Insert into #TempFinal (Filters,TotalCount)
Select 'Total Client after excluding cases whose address is changed in past 48 months',@ExcludedAddressChangeinPast48Months

Insert into #TempFinal (Filters,TotalCount)
Select 'Total Clients after excluding cases having Violation Date Is NOT NULL',@ExcludedNotNullViolationDate

Insert into #TempFinal (Filters,TotalCount)
Select 'Total Clients after excluding Records which ticket Number Start with ''V''',@ExcludedTicketNumberStartsWith_V

Insert into #TempFinal (Filters,TotalCount)
Select 'Total Clients after excluding Bad Address records',@ExcludedBadAddress

Insert into #TempFinal (Filters,TotalCount)
Select 'Total Clients after exluding Do Not Mail records',@ExcludedDoNotMail

Insert into #TempFinal (Filters,TotalCount)
Select 'Total Clients after excluding Website Status',@ExcldeWebSiteStatuses

Insert into #TempFinal (Filters,TotalCount)
Select 'Total Clients after excluding Active Clients',@ExcludedClientAndQuote


Insert into #TempFinal (Filters,TotalCount)
Select 'Total Clients after excluding already printed letter',@ExcludeAlreadyPrinted

Insert into #TempFinal (Filters,TotalCount)
Select 'Total letters available to print',@ExcludeAlreadyPrinted
Select Filters,TotalCount as [Total Count] from #TempFinal

drop table #TempFinal
drop table #temp
DROP TABLE #groupIdsTable
END

GO
GRANT EXECUTE ON USP_Mailer_DMC_ARR_BreakDown TO dbr_webuser
GO
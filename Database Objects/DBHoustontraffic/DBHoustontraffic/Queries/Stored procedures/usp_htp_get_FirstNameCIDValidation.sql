﻿/************************************************************
 * Code formatted by SoftTree SQL Assistant © v4.0.34
 * Time: 4/22/2009 4:47:23 PM
 ************************************************************/

/**********          
          
Created By : Waqas Javed 5770 04/09/2009 First Name CID validation Report  
          
Business Logic : This Report display client cases with following logics        
* Case should be active.  
* Verified status should be in Arraignment, Appearance, waiting, Arrainment or Bond waiting, Pre trial, Judge Trial or Jury Trial.  
* whose CID is same but First Name is different.  
          
List Of Parameters :           
@Validation : if 1 then follow up date will be checked for validation email    
  
List Of Columns :        
          
ticketid_pk  
Contact
LastName    
DOB  
FirstInitial  
FirstName
ContactFirstName    
 
         
**********/          
          
ALTER PROCEDURE [dbo].[usp_htp_get_FirstNameCIDValidation]
	@Validation INT = 0
AS
	SELECT DISTINCT MAX(temp1.ticketid_pk) AS TicketID_PK,
	       temp1.ContactID,
	       temp1.Lastname,
	       temp1.DOB,
	       temp1.FirstNameInitial,
	       temp1.Firstname,
	       temp1.ContactFirstName
	FROM   (
	           SELECT DISTINCT 
	                  tt.TicketID_PK,
	                  c.ContactID,
	                  tt.Lastname,
	                  CONVERT(VARCHAR, tt.DOB, 101) AS DOB,
	                  SUBSTRING(tt.Firstname, 1, 1) AS FirstNameInitial,
	                  tt.firstname,
	                  c.firstname AS ContactFirstName
	           FROM   tblTickets tt
	                  INNER JOIN Contact c
	                       ON  tt.ContactID_FK = c.ContactID
	                  INNER JOIN tblTicketsViolations ttv
	                       ON  tt.TicketID_PK = ttv.TicketID_PK
	                  INNER JOIN tblState s
	                       ON  tt.Stateid_FK = s.StateID
	                  INNER JOIN tblCourtViolationStatus tcvs
	                       ON  ttv.CourtViolationStatusIDmain = tcvs.CourtViolationStatusID
	           WHERE  tcvs.CategoryID <> 50
	                  AND tt.Activeflag = 1
	       ) AS temp1
	       INNER JOIN (
	                SELECT DISTINCT 
	                       c.ContactID,
	                       tt.firstname
	                FROM   tblTickets tt
	                       INNER JOIN Contact c
	                            ON  tt.ContactID_FK = c.ContactID
	                       INNER JOIN tblTicketsViolations ttv
	                            ON  tt.TicketID_PK = ttv.TicketID_PK
	                       INNER JOIN tblState s
	                            ON  tt.Stateid_FK = s.StateID
	                       INNER JOIN tblCourtViolationStatus tcvs
	                            ON  ttv.CourtViolationStatusIDmain = tcvs.CourtViolationStatusID
	                WHERE  tcvs.CategoryID <> 50
	                       AND tt.Activeflag = 1
	            ) AS temp2
	            ON  temp1.Firstname <> temp2.Firstname
	            AND temp1.ContactID = temp2.ContactID
	GROUP BY
	       temp1.ContactID,
	       temp1.Lastname,
	       temp1.DOB,
	       temp1.FirstNameInitial,
	       temp1.Firstname,
	       temp1.ContactFirstName
	ORDER BY
	       temp1.lastname,
	       --ozair 5929 05/21/2009
	       temp1.ContactID
GO



GRANT EXECUTE ON [dbo].[usp_htp_get_FirstNameCIDValidation] TO dbr_webuser
GO
          


  
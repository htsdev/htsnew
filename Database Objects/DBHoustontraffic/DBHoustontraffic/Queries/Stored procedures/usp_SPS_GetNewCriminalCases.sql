create PROCEDURE [dbo].[usp_SPS_GetNewCriminalCases]
(
	@StartDate DATETIME=NULL,
	@EndDate DATETIME=NULL
)
AS 

SELECT  tv.TicketsViolationID, tv.CourtID, t.TicketID_PK, t.Firstname, t.MiddleName, 
t.Lastname, t.Midnum, tv.RefCaseNumber, [CourtDateMain],
t.calculatedtotalfee, 
Paid =  (                                               
 select isnull( SUM(ISNULL(p.ChargeAmount, 0)),0) from tblticketspayment p                                              
 where p.ticketid = t.ticketid_pk                                              
 and  p.paymentvoid = 0                                              
 ) 
 
FROM tblTickets t 
INNER JOIN tblTicketsViolations tv ON t.TicketID_PK = tv.TicketID_PK
WHERE (tv.CourtID = 3037)
And (tv.SharepointStatusId = 0 OR tv.SharepointStatusId is null)
AND tv.[CourtDateMain] BETWEEN @StartDate AND @EndDate
and t.ActiveFlag = 1
order by ticketid_pk desc

GO


GRANT EXEC ON usp_SPS_GetNewCriminalCases  TO dbr_webuser

GO


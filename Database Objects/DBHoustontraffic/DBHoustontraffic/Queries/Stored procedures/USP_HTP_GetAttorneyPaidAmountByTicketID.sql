/*
* Created By : Noufil Khan against 6052
* Business Logic : This procedure returns attorney pait information for the given ticketid( criminal client)
* Parameter : @TicketID
* Column Returns :
*		   TicketID_PK,
	       TicketsViolationID,
	       Cause Number,
	       Violation Description,
	       LevelCode,
	       AttorneyPaidDate,
	       AttorneyPaidAmount,
	       dispositiondate,
	       empid
*/
ALTER PROCEDURE [dbo].[USP_HTP_GetAttorneyPaidAmountByTicketID]
	@TicketID INT
AS
	SELECT ttv.TicketID_PK,
	       ttv.TicketsViolationID,
	       ttv.casenumassignedbycourt,
	       tv.[Description],
	       tc.LevelCode,
	       ttv.AttorneyPaidDate,
	       ttv.AttorneyPaidAmount,
	       (
	           SELECT TOP 1 ttvl.Recdate
	           FROM   tblTicketsViolationLog ttvl
	           WHERE  ttvl.oldverifiedstatus <> ttvl.newverifiedstatus
	                  AND ttvl.newverifiedstatus = 80
	                  AND ttvl.TicketviolationID = ttv.TicketsViolationID
	           ORDER BY
	                  ttvl.Recdate DESC
	       ) AS dispositiondate,
	       -- Noufil 7055 11/25/2009 Show rep full name
	       tu.Firstname+' '+tu.Lastname  AS empid
	FROM   tblTicketsViolations ttv
	       INNER JOIN tblTickets tt
	            ON  tt.TicketID_PK = ttv.TicketID_PK
	       INNER JOIN tblViolations tv
	            ON  tv.ViolationNumber_PK = ttv.ViolationNumber_PK
	       LEFT OUTER JOIN tblChargelevel tc
	            ON  tc.ID = ttv.ChargeLevel
	       LEFT OUTER JOIN AttorneyPayoutHistory aph
	            ON  aph.PrintDatetime = ttv.AttorneyPaidDate
	       LEFT JOIN tblUsers tu
	            ON  tu.EmployeeID = ISNULL(aph.EmpID, 3992)
	WHERE  ttv.IsAttorneyPayoutPrinted = 1
	       AND ttv.TicketID_PK = @TicketID
	       AND tt.CaseTypeId = 2
	ORDER BY
	       ttv.TicketID_PK DESC




USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_hts_Add_donotmail_records_ByAddress]    Script Date: 01/10/2012 18:22:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/************************************************************
 * Code formatted by SoftTree SQL Assistant � v4.0.34
 * Time: 12/24/2011 9:12:48 AM
 ************************************************************/

-- =============================================
-- Author:		Sabir Khan
-- Task Id:		9726
-- Create date: 11/02/2011
-- Business Logic:	The stored procedure is used in HTP /Activies /Donot Mail Update application.
--				It is used to insert the records in tbl_hts_donotmail_records if this record doest 
--				not exsit in the tblticketsarchive
--				
--
-- List Of Parameters:
--	@FirstName:		First name of the person
--	@LastName:		last name of the person
--	@Address:		mailing address of the person
--	@Zip:			zip code associated with the mailing address
--	@City:			city associated with the mailing address
--	@stateid_fk:	state id related to the mailing address
--	@UpdateSource:	use for updating source athat From which source records has been updated
--
-- List of output columns: 
--	@FirstName:		First name of the person
--	@LastName:		last name of the person
--	@Address:		mailing address of the person
--	@Zip:			zip code associated with the mailing address
--	@City:			city associated with the mailing address
--	@stateid_fk:	state id related to the mailing address
--	@UpdateSource:	use for updating source athat From which source records has been updated

--
-- =============================================

-- USP_hts_Add_donotmail_records_ByAddress '','','1919 SMITH ST','77002-8028','HOUSTON',45,0
ALTER PROCEDURE [dbo].[USP_hts_Add_donotmail_records_ByAddress]
	@FirstName VARCHAR(20),
	@LastName VARCHAR(20),
	@Address VARCHAR(50) ,
	@Zip VARCHAR(12) ,
	@City VARCHAR(50),
	@stateid_fk INT,
	@UpdateSource INT
AS
BEGIN
    IF EXISTS (SELECT t.ADDRESS1,t.city,t.StateID_FK,t.zipcode FROM   tblTicketsArchive t
                  WHERE  
                  CASE WHEN LEN(LTRIM(RTRIM(@FirstName))) > 0 THEN LTRIM(RTRIM(t.firstname)) ELSE '' END  =  LTRIM(RTRIM(@FirstName))							
				  AND CASE WHEN LEN(LTRIM(RTRIM(@LastName))) > 0 THEN LTRIM(RTRIM(t.lastname)) ELSE '' END  =  LTRIM(RTRIM(@LastName))				                         
				  AND LTRIM(RTRIM(t.address1 + ' ' + ISNULL(t.Address2,''))) like LTRIM(RTRIM(@Address))
			      AND LEFT(LTRIM(RTRIM(t.ZipCode)),5) = LEFT(LTRIM(RTRIM(@Zip)),5)                       				  
				  AND CASE WHEN LEN(LTRIM(RTRIM(@City))) > 0 THEN LTRIM(RTRIM(t.City)) ELSE '' END = LTRIM(RTRIM(@City))				  
				  AND CASE WHEN @stateid_fk > 0 THEN t.StateID_FK ELSE 0 END  = @stateid_fk)
 
    BEGIN
 
        IF NOT EXISTS(SELECT * FROM   tbl_hts_donotmail_records
               WHERE  LTRIM(RTRIM(ADDRESS)) LIKE LTRIM(RTRIM(@Address))
                      AND LEFT(LTRIM(RTRIM(Zip)),5) = LEFT(LTRIM(RTRIM(@Zip)),5))
        BEGIN        	
            INSERT INTO tbl_hts_donotmail_records(FirstName,LastName,ADDRESS,Zip,City,StateID_FK,InsertDate,UpdateSource)
            VALUES(@FirstName,@LastName,@Address,@Zip,@City,@stateid_fk,GETDATE(),@UpdateSource)        
   
        
        UPDATE tblTicketsArchive  SET    donotMailflag = 1
        WHERE  CASE WHEN LEN(LTRIM(RTRIM(@FirstName))) > 0 THEN LTRIM(RTRIM(firstname)) ELSE '' END  =  LTRIM(RTRIM(@FirstName))							
			   AND CASE WHEN LEN(LTRIM(RTRIM(@LastName))) > 0 THEN LTRIM(RTRIM(lastname)) ELSE '' END  =  LTRIM(RTRIM(@LastName))				                         
			   AND LTRIM(RTRIM(address1 + ' ' + ISNULL(Address2,''))) like LTRIM(RTRIM(@Address))
			   AND LEFT(LTRIM(RTRIM(ZipCode)),5) = LEFT(LTRIM(RTRIM(@Zip)),5)                       
			   AND CASE WHEN LEN(LTRIM(RTRIM(@City))) > 0 THEN LTRIM(RTRIM(City)) ELSE '' END = LTRIM(RTRIM(@City))				  
			   AND CASE WHEN @stateid_fk > 0 THEN StateID_FK ELSE 0 END  = @stateid_fk
    
		SELECT 0 AS sNo,0 AS dbid,0 AS recordid,'' AS casetype,FirstName,LastName,ADDRESS AS Address1,City,zip AS zipcode,
			   ISNULL(StateID_FK, 0) AS STATE,ISNULL(CONVERT(VARCHAR(10), insertdate, 101), 'Not Set') AS DoNotMailUpdateDate,'Successfully marked as Do Not Mail!' AS ErrMessage
		FROM   tbl_hts_donotmail_records
		WHERE  
			  CASE WHEN LEN(LTRIM(RTRIM(@FirstName))) > 0 THEN LTRIM(RTRIM(Firstname)) ELSE '' END  =  LTRIM(RTRIM(@FirstName))							
			  AND CASE WHEN LEN(LTRIM(RTRIM(@LastName))) > 0 THEN LTRIM(RTRIM(lastname)) ELSE '' END  =  LTRIM(RTRIM(@LastName))                      
			  AND LTRIM(RTRIM(ADDRESS)) LIKE LTRIM(RTRIM(@Address))
			  AND LEFT(LTRIM(RTRIM(Zip)),5) = LEFT(LTRIM(RTRIM(@Zip)),5)
			  AND CASE WHEN LEN(LTRIM(RTRIM(@City))) > 0 THEN LTRIM(RTRIM(City)) ELSE '' END = LTRIM(RTRIM(@City))   
			  AND CASE WHEN @stateid_fk > 0 THEN StateID_FK ELSE 0 END  = @stateid_fk  
        END
        ELSE
        BEGIN
				
        	   UPDATE tblTicketsArchive  SET    donotMailflag = 1
				   WHERE  CASE WHEN LEN(LTRIM(RTRIM(@FirstName))) > 0 THEN LTRIM(RTRIM(firstname)) ELSE '' END  =  LTRIM(RTRIM(@FirstName))							
				   AND CASE WHEN LEN(LTRIM(RTRIM(@LastName))) > 0 THEN LTRIM(RTRIM(lastname)) ELSE '' END  =  LTRIM(RTRIM(@LastName))				                         
				   AND LTRIM(RTRIM(address1 + ' ' + ISNULL(Address2,''))) like LTRIM(RTRIM(@Address))
				   AND LEFT(LTRIM(RTRIM(ZipCode)),5) = LEFT(LTRIM(RTRIM(@Zip)),5)                       
				   AND CASE WHEN LEN(LTRIM(RTRIM(@City))) > 0 THEN LTRIM(RTRIM(City)) ELSE '' END = LTRIM(RTRIM(@City))				  
				   AND CASE WHEN @stateid_fk > 0 THEN StateID_FK ELSE 0 END  = @stateid_fk
				   
        		SELECT 0 AS sNo,0 AS dbid,0 AS recordid,'' AS casetype,FirstName,LastName,ADDRESS AS Address1,City,zip AS zipcode,
			    ISNULL(StateID_FK, 0) AS STATE,ISNULL(CONVERT(VARCHAR(10), insertdate, 101), 'Not Set') AS DoNotMailUpdateDate,'Already exists' AS ErrMessage
				FROM   tbl_hts_donotmail_records
				WHERE LTRIM(RTRIM(ADDRESS)) LIKE LTRIM(RTRIM(@Address))
				  AND LEFT(LTRIM(RTRIM(Zip)),5) = LEFT(LTRIM(RTRIM(@Zip)),5)
				 
       END
    END
    ELSE
    IF NOT EXISTS(SELECT * FROM   tbl_hts_donotmail_records
               WHERE  LTRIM(RTRIM(ADDRESS)) LIKE LTRIM(RTRIM(@Address))
                      AND LEFT(LTRIM(RTRIM(Zip)),5) = LEFT(LTRIM(RTRIM(@Zip)),5))
    BEGIN
    	  INSERT INTO tbl_hts_donotmail_records(FirstName,LastName,ADDRESS,Zip,City,StateID_FK,InsertDate,UpdateSource)
          VALUES(@FirstName,@LastName,@Address,@Zip,@City,@stateid_fk,GETDATE(),@UpdateSource) 
         
          SELECT 0 AS sNo,0 AS dbid,0 AS recordid,'' AS casetype,FirstName,LastName,ADDRESS AS Address1,City,zip AS zipcode,
			   ISNULL(StateID_FK, 0) AS STATE,ISNULL(CONVERT(VARCHAR(10), insertdate, 101), 'Not Set') AS DoNotMailUpdateDate,'Address has been added sucessfully!' AS ErrMessage
		       FROM   tbl_hts_donotmail_records    
    END
    BEGIN
			
    		SELECT 0 AS sNo,0 AS dbid,0 AS recordid,'' AS casetype,'' AS FirstName,'' AS LastName,'' AS Address1,
           '' AS City,'' AS zipcode, 0 AS STATE,'' AS DoNotMailUpdateDate,'Already Exist.' AS ErrMessage    
    END
    
    
END

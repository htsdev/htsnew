  
  --usp_hts_get_ServiceTicketEmail 126130 78921
ALTER procedure [dbo].[usp_hts_get_ServiceTicketEmail] --126130                      
@Ticketid int                      
as                       
declare @date datetime                    
                    
select   
  t.firstname + ' ' + t.lastname as clientName,                  
  t.firstname as firstname ,    
  t.lastname as lastname,  
  t.bondflag,
-- Noufil 4787 09/19/2008 Top 1 Refcase number added  
 (select top 1 refcasenumber from tblticketsviolations where ticketid_pk=@Ticketid) as ref, 
  refcasenumber as refcasenumber,                       
  dbo.fn_DateFormat(isnull(courtdatemain,Convert(datetime,'1/1/1900')),27,'/','/',0) as courtDate ,                     
  dbo.fn_DateFormat(isnull(courtdatemain,convert(datetime,'1/1/1900')),26,':',':',1) as courtTime,                       
  courtnumbermain,  
  cvs.shortDescription as Courtviolationstatus,  
  --modified by ozair for Task 2710                    
  c.shortname as courtLocation,  
  --end task 2710                  
  t.generalcomments,  
  t.activeflag  
  
from tblticketsviolations tv  
   inner join tblcourts c on tv.courtid = c.courtid  
   inner join tblcourtviolationstatus cvs on tv.courtviolationstatusidmain = cvs.courtviolationstatusid  
   inner join tbltickets t on t.ticketid_pk = tv.ticketid_pk  
   INNER JOIN dbo.tblViolations ON TV.ViolationNumber_PK = dbo.tblViolations.ViolationNumber_PK                                             
   LEFT OUTER JOIN dbo.tblViolationCategory vc ON dbo.tblViolations.CategoryID = vc.CategoryId  
where tv.ticketid_pk = @Ticketid

group by refcasenumber,t.firstname ,t.lastname, t.bondflag,courtdatemain, courtnumbermain,  
  cvs.shortDescription, c.shortname,t.generalcomments,t.activeflag



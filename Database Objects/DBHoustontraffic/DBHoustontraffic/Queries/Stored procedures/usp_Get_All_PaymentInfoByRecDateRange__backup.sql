SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_All_PaymentInfoByRecDateRange__backup]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_All_PaymentInfoByRecDateRange__backup]
GO








CREATE PROCEDURE [dbo].[usp_Get_All_PaymentInfoByRecDateRange] 

@RecDate DateTime,
@RecTo DateTime

AS

Select PaymentType, Count(ticketID) As TotalCount, Sum(ChargeAmount) As Amount 
From tblTicketsPayment
Where  PaymentVoid <> 1  And  
/*
( CONVERT(varchar(12),RecDate, 101)   >= CONVERT(varchar(12), @RecDate, 101)    
And  CONVERT(varchar(12),RecDate, 101)   <=  CONVERT(varchar(12), @RecTo, 101)    )
*/
( RecDate  Between  @RecDate AND @RecTo+1)

Group by PaymentType
Order By PaymentType





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


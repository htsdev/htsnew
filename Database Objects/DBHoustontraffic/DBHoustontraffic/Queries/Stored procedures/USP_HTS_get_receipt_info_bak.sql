  --USP_HTS_get_receipt_info 118606    
    
ALTER procedure dbo.USP_HTS_get_receipt_info_bak            
@ticketID int,            
@employeeid int = 3992            
as            
declare @paymentamount money            
declare @casenumbers varchar(500),            
@paymentinfo varchar(500),            
@contactinfo varchar(500)            
            
--select * from tblcourts            
select @paymentamount = sum(chargeamount)             
from tblticketspayment where ticketid = @ticketID            
and paymenttype not in (99,100)            
and paymentvoid not in (1)            
            
            
set @casenumbers = ' '            
select @casenumbers = @casenumbers + isnull(refcasenumber,' ')             
--+ case             
----left(isnull(refcasenumber,' ') ,1)             
----when 'F' and courtid in (3001,3002,3003) then ','            
----when 'F' and courtid in (3001,3002,3003) then ','            
--when courtid not in (3001,3002,3003) or (left(isnull(refcasenumber,' ') ,1) = 'F' and courtid in (3001,3002,3003)) then ','            
--else '-' + isnull(convert(varchar(2),sequencenumber),' ') + ','             
--end            
            
from tblticketsviolations            
where ticketid_pk = @ticketid            
set @casenumbers = ltrim(left(@casenumbers,len(@casenumbers)-1))            
            
            
            
  --newly updated  in order to get contacts from tbltickets        
---------------        
declare @contact1 as varchar(15)        
declare @contact2 as varchar(15)        
declare @contact3 as varchar(15)        
        
select @contact1 = isnull(contact1,''),          
@contact2=isnull(contact2,''),        
@contact3=isnull(contact3,'')                
from tbltickets T             
where (contact1 is not null or contact2 is not null or contact3 is not null )         
and ticketid_pk=@ticketid        
        
set @contactinfo=@contact1        
if (@contact2<>'')        
set @contactinfo=@contactinfo+'/'+@contact2        
if (@contact3<>'')        
set @contactinfo=@contactinfo+'/'+@contact3        
if (left(@contactinfo,1)='/')        
begin        
set @contactinfo=right(@contactinfo,len(@contactinfo)-1)        
end           
---------------             
    
            
set @paymentinfo = ' '            
select @paymentinfo = @paymentinfo + '$' + convert(varchar(20),chargeamount)              
+ '(' + convert(varchar(12),recdate) + ')' + ';'            
from tblticketspayment where ticketid = @ticketID            
and paymenttype not in (99,100)            
and paymentvoid not in (1)            
set @paymentinfo = ltrim(left(@paymentinfo,len(@paymentinfo)-1))            
            
            
select T.ticketid_pk,firstname,lastname,T.Address1, T.Address2, @casenumbers as ticketnumber_pk,            
@paymentamount as paymentamount,T.city,S.state as state,T.zip,            
C.Address + ' ' + C.Address2 + ' ' + C.city + ', ' + 'TX ' + C.zip as Address,            
V.CourtDateMain,totalfeecharged,totalfeecharged-@paymentamount as dueamount,            
@casenumbers as casenumbers, d.description as settintype, @paymentinfo as paymentinfo, @contactinfo as contactinfo            
from tbltickets T,  tblcourts C, tbldatetype D   ,tblstate S , tblticketsviolations V , tblcourtviolationstatus O       
where           
V.courtid = C.courtid              
     
and T.stateid_fk=S.stateid          
and T.ticketid_pk = V.ticketid_pk    
and V.courtviolationstatusidmain = O.courtviolationstatusid    
and O.categoryid = D.typeid    
and T.ticketid_pk in (@ticketID)            
and activeflag = 1            
            
            
if @@rowcount >=1             
 exec sp_Add_Notes @TicketID,'Receipt Printed',null,@employeeid            
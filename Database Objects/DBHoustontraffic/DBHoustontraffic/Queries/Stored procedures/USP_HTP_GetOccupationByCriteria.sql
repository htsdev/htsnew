﻿/******  
* Created By :	Fahad Muhammad Qureshi.
* Create Date :   10/19/2010 12:44:00 PM
* Task ID :				6766
* Business Logic :  This procedure is used to Get .
* List of Parameter :
* Column Return : 
      
******/
 
CREATE PROCEDURE dbo.USP_HTP_GetOccupationByCriteria 
@Criteria VARCHAR(MAX) = ''
AS      
      
BEGIN
    SELECT DISTINCT TOP 15 p.Occupation AS Occupation
    FROM Patient p
    WHERE  p.Occupation  LIKE @criteria + '%'
           AND p.Occupation  IS NOT NULL
           AND LEN(p.Occupation) > 0
    ORDER BY
           p.Occupation 
END 
GO
GRANT EXECUTE ON dbo.USP_HTP_GetOccupationByCriteria TO dbr_webuser
GO 	 
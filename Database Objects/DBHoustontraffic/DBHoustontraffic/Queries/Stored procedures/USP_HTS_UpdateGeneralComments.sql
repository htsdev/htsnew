﻿use TrafficTickets
go


/************************************************************  
 * Code formatted by SoftTree SQL Assistant © v4.8.29  
 * Time: 7/7/2010 2:12:50 PM  
 ************************************************************/  
  
 /*      
* Modified By  : SAEED AHMED    
* Task ID   : 7844     
* Created Date  : 06/01/2010      
* Business logic : This procedure is used update general comments in tblticket. It also add entries in 'tblticketsnotes' table for history purpose.  
* Parameter List      
* @TicketID_PK  : Represents TicketID against the updation will perform.    
* @empid   : Represents EmployeeID which need to be set.    
* @ContactID  : Represents ContactID against which the updation is performed if contactID is not 0  
* @GeneralComments : Represents general comments which need to be set.   
*    
*/      
  
  --[USP_HTS_UpdateGeneralComments] 195804,3991,'Noufil'  
  --[USP_HTS_UpdateGeneralComments] 195810,3991,'Noufil'  
    
ALTER PROCEDURE [dbo].[USP_HTS_UpdateGeneralComments]  
 @TicketID_PK INT,  
 @empid INT,  
 @ContactID INT = 0 , --SAEED 7844 06/02/2010, contactID is added.  
 @GeneralComments VARCHAR(MAX) -- Adil - For 2585  
AS  
 SET @generalcomments = LTRIM(RTRIM(@generalcomments))   
   
 PRINT @GeneralComments  
 

 IF(rtrim(ltrim(@GeneralComments)) <> '')
 BEGIN  
 IF ((RIGHT(@GeneralComments, 1) <> ')')) --if comments are updated  
 BEGIN  
     DECLARE @empnm AS VARCHAR(12)                    
     SELECT @empnm = abbreviation  
     FROM   tblusers  
     WHERE  employeeid = @empid   
     --print @empnm                  
     IF @empnm IS NULL  
     BEGIN  
         SET @empnm = 'N/A'  
     END  
       
     IF LEN(@GeneralComments) > 0  
         --print len(@GeneralComments)  
         SET @GeneralComments = @GeneralComments + ' ' + '(' + dbo.formatdateandtimeintoshortdateandtime(GETDATE())   
             + ' - ' + UPPER(@empnm) + ')' + ' '   
     --print @GeneralComments  
     --SAEED 7844 06/02/2010, place the code inside contactID check, if contactID='0' which bydefault the same code will run as it was running earlier.  
     -- in case if contactID<>'0' then it will get all the ticketid_pk from tbltickets and then perform update/insert operation against each ticketid.  
     IF @ContactID = 0  
     BEGIN  
         UPDATE tbltickets  
         SET    generalcomments = ISNULL(generalcomments, '') + @GeneralComments,  
                employeeidupdate = @empid  
         WHERE  ticketid_pk = @TicketID_PK   
         -- Noufil 4980 10/31/2008 Adding Notes in history  
         INSERT INTO tblticketsnotes  
           (  
             ticketid,  
             [subject],  
             recdate,  
             EmployeeID,  
             fk_commentid  
           )  
         VALUES  
           (  
             @ticketid_pk,  
             'General Notes: ' + @GeneralComments,  
             GETDATE(),  
             @empid,  
             1  
           )  
     END  
     ELSE  
     BEGIN  
         DECLARE @temp             TABLE (id INT IDENTITY(1, 1), ticketID INT)  
         DECLARE @counter          INT  
         DECLARE @currentTicketID  INT  
         SET @counter = 1  
         INSERT INTO @temp  
         SELECT ticketid_pk  
         FROM   tblTickets tt  
         WHERE  tt.ContactID_FK = @ContactID  
           
         WHILE @counter <= (  
                   SELECT COUNT(id)  
                   FROM   @temp  
               )  
         BEGIN  
             SELECT @currentTicketID = ticketID  
             FROM   @temp  
             WHERE  id = @counter  
               
             UPDATE tbltickets  
             SET    generalcomments = ISNULL(generalcomments, '') + @GeneralComments,  
                    employeeidupdate = @empid  
             WHERE  ticketid_pk = @currentTicketID   
             -- 7994 Sarim 07/08/2010 Add NoteType in the tblticketnotestable.  
             INSERT INTO tblticketsnotes  
               (  
                 ticketid,  
                 [subject],  
                 recdate,  
                 EmployeeID,  
                 fk_commentid  
               )  
             VALUES  
        (  
                 @currentTicketID,  
                 'General Notes: ' + @GeneralComments,  
                 GETDATE(),  
                 @empid,  
                 1  
               )  
             SET @counter = @counter + 1  
         END  
     END  
     --SAEED 7844  
 END      
 END
  
 SELECT generalcomments  
 FROM   tbltickets  
 WHERE  ticketid_pk = @TicketID_PK                   
      
    
/*
* 
* Zeeshan Haider 11323 08/02/2013
* 
* Description:
* This procedure is used to get last printed cover sheet file name
* 
* */
USE TrafficTickets

GO

ALTER PROCEDURE USP_HTP_GET_Updated_Cover_Sheet_Name
(
	@ticketID INT = 0,
	@letterID INT = 0
)
AS
	SELECT TOP 1 th.DocPath
	FROM   tblHTSNotes th
	WHERE  th.TicketID_FK = @ticketID
	       AND th.LetterID_FK = @letterID
	ORDER BY
	       th.PrintDate DESC
GO

GRANT EXECUTE ON [dbo].[USP_HTP_GET_Updated_Cover_Sheet_Name] TO dbr_webuser 
GO



/**
Business Logic : This function updates the information for the last update inserted record from Cute chat in Sullolaw
Parameter : 
			@email = Eail adress 
			@name = name
			@title = title
			@siteid= site id 1 for ohuston/2 for dallas
			@phone = Phone number
COlumn Return:
			@@rowcount = return number of column effected.
**/

-- [dbo].[USP_HTS_UPDATE_FEEDBACK] 'noufil@lntechnologies.com','Noufil khan','Test Subject',2,''

Create procedure [dbo].[USP_HTP_UPDATE_FEEDBACK]
@email nvarchar(100),
@name nvarchar(100),
@title nvarchar(400),
@siteid int,
@phone varchar(30)

as

update CuteSupportFeedback
set 
	siteid=@siteid,
	callback = CASE @siteid when 1 then 0 when 2 then 1 end,
	phone=@phone

where feedbackid = (select max(feedbackid) from CuteSupportFeedback where rtrim(ltrim(email))like @email and rtrim(ltrim(name))like @name and rtrim(ltrim(title))like @title)
return @@rowcount

go
grant exec on [dbo].[USP_HTP_UPDATE_FEEDBACK] to dbr_webuser

/****** 
Alter by:  Muhammad Nasir
Business Logic : This Procedure update status and comments against ticketid  
task : 5690

List of Parameters:	
@comments 
 @status 
 @ticketid 
 @employeeid
 @callType 
 @gComments
  @TicketViolationIds 

******/   

ALTER PROCEDURE [dbo].[usp_HTS_update_ReminderCall_status_Update] --'abc', 0, 190351, 4083, 2, "new comments"
	@comments VARCHAR(500),
	@status INT,
	@ticketid INT ,
	@employeeid INT,
	@callType INT,
	@gComments VARCHAR(500) = NULL, -- Added zahoor for FTA Call
	@TicketViolationIds
AS
	VARCHAR(1000) = '' --Zeeshan 4837 09/23/2008
	AS       
	
	IF (@callType = 0)
	BEGIN
	    EXECUTE USP_HTS_Insert_Comments @ticketid, @employeeid, 3, @comments
	    UPDATE tblTicketsViolations
	    SET    ReminderCallStatus = @status,
	           ReminderCallEmpID = @employeeid
	    WHERE  ticketid_pk = @ticketid 
	           --Zeeshan 4837 09/23/2008 Update Record By Ticket Violation Id
	           AND ticketsviolationid IN (SELECT *
	                                      FROM   [dbo].[Sap_String_Param_bigint](@TicketViolationIds))
	END
	ELSE 
	IF (@callType = 1)
	BEGIN
	    EXECUTE USP_HTS_Insert_Comments @ticketid, @employeeid, 9, @comments
	    UPDATE tblTicketsViolations
	    SET    SetCallStatus = @status,
	           ReminderCallEmpID = @employeeid
	    WHERE  ticketid_pk = @ticketid 
	           --Zeeshan 4837 09/23/2008 Update Record By Ticket Violation Id
	           AND ticketsviolationid IN (SELECT *
	                                      FROM   [dbo].[Sap_String_Param_bigint](@TicketViolationIds))
	END-- Added Zahoor 3977 08/19/2008
	ELSE 
	IF (@callType = 2)
	BEGIN
	    EXECUTE USP_HTS_Insert_Comments @ticketid, @employeeid, 10, @comments
	    EXECUTE USP_HTS_Insert_Comments @ticketid, @employeeid, 1, @gComments
	    
	    UPDATE tblTicketsViolations
	    SET    FTACallStatus = @status,
	           ReminderCallEmpID = @employeeid
	    WHERE  ticketid_pk = @ticketid 
	           --Zeeshan 4837 09/23/2008 Update Record By Ticket Violation Id
	           AND ticketsviolationid IN (SELECT *
	                                      FROM   [dbo].[Sap_String_Param_bigint](@TicketViolationIds))
	END--Nasir 5690 03/25/2009 new call type HMCAWDLQCALL
	ELSE 
	IF (@callType = 3)
	BEGIN
	    EXECUTE USP_HTS_Insert_Comments @ticketid, @employeeid, 1, @gComments  
	    
	    UPDATE tblTicketsViolations
	    SET    HMCAWDLQCallStatus = @status,
	           ReminderCallEmpID = @employeeid
	    WHERE  ticketid_pk = @ticketid
	           AND ticketsviolationid IN (SELECT *
	                                      FROM   [dbo].[Sap_String_Param_bigint](@TicketViolationIds))
	END
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_CHECK_SERVICETICKET_CATEGORY]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_CHECK_SERVICETICKET_CATEGORY]
GO

CREATE PROCEDURE USP_HTS_CHECK_SERVICETICKET_CATEGORY 
@CategoryID int
as
select count(TicketID_PK) as RecCount from tblticketsflag where ServiceTicketCategory = @CategoryID

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


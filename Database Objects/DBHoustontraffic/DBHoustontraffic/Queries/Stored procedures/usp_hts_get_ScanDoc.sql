SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_hts_get_ScanDoc]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_hts_get_ScanDoc]
GO





CREATE  procedure usp_hts_get_ScanDoc      
@docid int,      
@docnum int      
as      
declare @ticketnumber varchar(50),  
        @seqnum       varchar(2)   
  
select  @ticketnumber=        
  case left(ticketnumber,1)       
  when 'F' then ticketnumber else left(ticketnumber,len(ticketnumber)-1)   
  end  
  
 from tblScannedDocketCourtStatus S, tblcourtviolationstatus T      
 where S.violationstatus = T.courtviolationstatusid      
 and updateflag in  (0,1)      
 and doc_id = @docid and docnum = @docnum           
  
select  @ticketnumber as ticketnumber,  
	causenumber,
convert (varchar(22),courtdate,0) as courtdate,  
courtno,  
ViolationStatus,  
Updateflag = case updateflag      
when 0 then 'Pending'       
else 'Updated' end,RecDate,  
doc_id,  
docnum,  
description,  
seqnum = case left(ticketnumber,1) when 'F' then null else right(ticketnumber,1) end,  
  
courtname=( select  shortcourtname from tblcourts  --newly added in order to get courtname  
 where courtid =  
  (select top 1 courtid from tblticketsviolations ttv  
   where ttv.refcasenumber=@ticketnumber  
 -- and ttv.sequencenumber=seqnum        
  )   
 ),  
courtid=(select top 1 courtid from tblticketsviolations ttv  
   where ttv.refcasenumber=@ticketnumber  
 )  
  
from tblScannedDocketCourtStatus S, tblcourtviolationstatus T      
where S.violationstatus = T.courtviolationstatusid      
and updateflag in  (0,1)      
and doc_id = @docid and docnum = @docnum       
      










GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


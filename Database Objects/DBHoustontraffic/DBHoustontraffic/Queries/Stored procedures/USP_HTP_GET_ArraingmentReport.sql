/**
* -- Noufil 5772 04/12/2009 
* Business Logic : This procedure returns all HMC clients whose court violation status in "ArraignmentWaiting"
* Parameters : 
*				@courtdate : Court Date
				@Url : URL which will navigate to matter's Page of client from Rpt.
**/

-- [dbo].[USP_HTP_GET_ArraingmentReport] '06/10/2011', 'newpakhouston.legalhouston.com'
ALTER PROCEDURE [dbo].[USP_HTP_GET_ArraingmentReport]
	@courtdate DATETIME,
	@Url VARCHAR(MAX),
	@Courtloc varchar(10)='None'
AS


	SELECT 
		   tt.TicketID_PK,
	       tt.Firstname,
	       tt.Lastname,
	       tt.dob,
	       ISNULL(tt.Midnum, '') AS Midnum,
	       ISNULL(ttv.casenumassignedbycourt, '') AS causenumber,
	       ISNULL(ttv.RefCaseNumber, '') AS ticketnumber,
	       CASE WHEN tv.[Description] LIKE '%No Child Safety Seat%' THEN 'CHILD SEAT' ELSE tv.[Description] END as ShortDescription, -- Haris Ahmed 10460 10/02/2012 On Trial Docket for “No Child Safety Seat” use the abbreviation “CHLD SEAT”
	       [dbo].[fn_DateFormat](ttv.CourtDateMain, 25, '/', ':', 1) + ' # ' + ttv.CourtNumbermain AS CourtDate,			
		   --case when datepart(hh,ttv.CourtDateMain) > 12 then convert(varchar, (datepart(hh,ttv.CourtDateMain) - 12)) else convert(varchar,datepart(hh,ttv.CourtDateMain)) end as sortt,		   
			datepart(hh,ttv.CourtDateMain) as sortt,		   -- Adil 7606 03/29/2010
			--Abbas Shahid Khwaja 06/07/2011 Include order by condition on the basis of time, previously it was sorted on hourly basis
			CAST (CAST(CONVERT(datetime,ttv.CourtDateMain) as FLOAT) AS DATETIME) AS NewSort,
	       @url AS url
	into #temp1
	FROM   tblTicketsViolations ttv
	       INNER JOIN tblTickets tt
	            ON  tt.TicketID_PK = ttv.TicketID_PK
	       INNER JOIN tblCourtViolationStatus tcvs
	            ON  tcvs.CourtViolationStatusID = ttv.CourtViolationStatusIDmain
	       INNER JOIN tblViolations tv ON tv.ViolationNumber_PK = ttv.ViolationNumber_PK
	WHERE  ttv.CourtViolationStatusIDmain = 201
	       AND ttv.CourtID IN (3001, 3002, 3003,3075)--Fahad 10378 01/09/2013 HMC-W also included along other HMC Courts
	       AND DATEDIFF(DAY, ttv.CourtDateMain, @courtdate) = 0
	       --Yasir Kamal 5886 05/07/2009 Eliminate Cases having future Auto Court Date.
	       -- Tahir 6033 06/11/2009 fixed the court date bug....
	       --AND DATEDIFF(DAY,ttv.CourtDate,GETDATE()) >= 0
	       AND DATEDIFF(DAY,ttv.CourtDate,@courtdate) >= 0
	       --5886 End
	       AND tt.Activeflag=1

--Abbas Shahid Khwaja 06/07/2011 Include order by condition on the basis of time (NewSort), previously it was sorted on hourly basis
--Fahad 10378 01/09/2013 HMC-W alos excluded along other HMC Courts	
if @Courtloc!='0' AND @Courtloc!='Traffic' and @Courtloc!='none' and @Courtloc!='OU' and @Courtloc!='3001' and @Courtloc!='3002' and @Courtloc!='3003' and @Courtloc!='3075'
	BEGIN
		SELECT * FROM #temp1 WHERE #temp1.TicketID_PK = 0 ORDER BY NewSort
	END
ELSE
	BEGIN
		select * from #temp1 order by NewSort
	END
	

﻿/******  
* Created By :	Fahad Muhammad Qureshi.
* Create Date :   10/19/2010 8:53:55 PM
* Task ID :				6766
* Business Logic :  This procedure is used to Get .
* List of Parameter :
* Column Return :       
******/


ALTER PROCEDURE dbo.USP_HTP_GetPatientDetailById
	@PatientId INT
AS
	SELECT p.TicketId,
	       p.LastName,
	       p.FirstName,
	       p.[Address],
	       p.City,
	       p.[StateId] AS StateId,
	       p.Zip,
	       p.DOB,
	       p.Contact1,
	       p.ContactType1,
	       p.Contact2,
	       p.ContactType2,
	       p.Contact3,
	       p.ContactType3,
	       p.Email,
	       p.IsClient,
	       p.PatientStatusId,
	       p.SurgeryDate,
	       p.DoctorContacted,
	       p.ContactComment,
	       p.IsSchedule,
	       p.ScheduleDate,
	       p.IsProblem,
	       p.ProblemComments,
	       p.PainSeverity,
	       p.ManufacturerComments,
	       p.GeneralComments,
	       p.DoctorId,	       
	       d.Id AS DrId, -- Noufil Khan 8469 11/03/2010 Added all below parameter
	       d.LastName AS DrLastName,
	       d.FirstName AS DrFirstName,
	       d.[Address] AS DrAddress,
	       d.City AS DrCity,
	       d.[State] AS DrState,
	       d.Zip AS DrZip,
	       d.DOB AS DrDOB,
	       ISNULL(p.HadBloodTest, 0) AS HadBloodTest,
	       p.BloodTestDate,
	       p.BloodTestConductor,
	       ISNULL(p.AbnormalLevelsNoted, 0) AS AbnormalLevelsNoted,
	       p.AbnormalMetalsFound,
	       p.SurgeryHospitalName,
	       p.SurgeryHospitalCity,
	       p.SurgeryHospitalState,
	       p.ImpactMaterial,
	       p.WereScrewImplanted,
	       p.ImplantDefect,
	       ISNULL(p.HasImplantRemoved,0) AS HasImplantRemoved,
	       p.ImplantRemoveDate,
	       p.WhyImplantRemoved,
	       p.ImplantRemoveHospName,
	       p.ImplantRemoveHospCity,
	       p.ImplantRemoveHospState,
	       p.ImplantDoctorName,
	       p.ImplantDoctorCity,
	       p.ImplantDoctorState,
	       p.MedicalReleaseSent,
	       p.MedicalReleaseRecieved,
	       p.ClientContractSent,
	       p.ClientContractRecieved,
	       p.MedicalRecordRequestDate,
	       p.MedicalRecordRecievedDate,
	       p.Occupation,
		   ISNULL(p.IsUnEmployed,0) AS IsUnEmployed,
		   p.LastJobLeftDate,
		   p.LastJobLeftReason,
		   ISNULL(p.Notes, '') AS Notes
	FROM   Patient p
	       LEFT OUTER JOIN Doctor d
	            ON  p.DoctorId = d.Id
	WHERE  p.Id = @PatientId
GO



GRANT EXECUTE ON dbo.USP_HTP_GetPatientDetailById TO dbr_webuser
GO 	 
USE [TrafficTickets]
GO
/****** Object:  Table [dbo].[ShowSettingHistory]    Script Date: 12/16/2009 16:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ShowSettingHistory](
	[ShowSettingHistoryID] [int] IDENTITY(1,1) NOT NULL,
	[ReportID] [int] NOT NULL,
	[Note] [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[RecDate] [datetime] NOT NULL CONSTRAINT [DF_ShowSettingHistory_RecDate]  DEFAULT (getdate()),
	[UpdatedByEmployeeID] [int] NOT NULL,
 CONSTRAINT [PK_ShowSettingHistory] PRIMARY KEY CLUSTERED 
(
	[ShowSettingHistoryID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
USE [TrafficTickets]
GO
ALTER TABLE [dbo].[ShowSettingHistory]  WITH CHECK ADD  CONSTRAINT [FK_ShowSettingHistory_tblUsers1] FOREIGN KEY([UpdatedByEmployeeID])
REFERENCES [dbo].[tblUsers] ([EmployeeID])
GO
ALTER TABLE [dbo].[ShowSettingHistory]  WITH CHECK ADD  CONSTRAINT [FK_ShowSettingHistory1_tbl_TAB_SUBMENU] FOREIGN KEY([ReportID])
REFERENCES [dbo].[tbl_TAB_SUBMENU] ([ID])
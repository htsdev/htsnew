SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_LA_Update_Tickets_Events]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_LA_Update_Tickets_Events]
GO


CREATE PROCEDURE USP_HTS_LA_Update_Tickets_Events

@Record_Date datetime,  
@List_Date datetime,  
@Cause_Number as Varchar(20),
@Ticket_Number as Varchar(15),
@Court_Date Smalldatetime,
@Updated_Date Smalldatetime,
@Court_Room_Number int,
@Court_Time Smalldatetime,
@ViolationStatus as Varchar(1000),  
@GroupID as Int,
@Flag_Updated int Output

AS  
Declare @IDFound as int
Declare @ViolationStatusID as int
Declare @CourtViolationStatusID as int
Declare @CourtID as int
Declare @PlanID as int

If @Updated_Date = '1/1/1900'
Begin
	Set @Updated_Date = Null
End
If @Court_Date = '1/1/1900'
Begin
	Set @Court_Date = Null
End

Set @Cause_Number = Left(RTrim(LTrim(@Cause_Number)), 30)
Set @Ticket_Number = Left(RTrim(LTrim(@Ticket_Number)), 20)
Set @Court_Date = @Court_Date + Convert(datetime, @Court_Time)
Set @Flag_Updated = 0
Set @Cause_Number = Replace(@Cause_Number, ' ', '')
Set @IDFound = 0
Set @PlanID  = 0

If Len(@Ticket_Number) < 1
	Begin
		Set @Ticket_Number = @Cause_Number
	End

If @Court_Room_Number = 13 or @Court_Room_Number = 14
	Begin
		Set @CourtID = 3002
	End
Else If @Court_Room_Number = 18
	Begin
		Set @CourtID = 3003
	End
Else 
	Begin
		Set @CourtID = 3001
	End

Set @PlanID = (Select Top 1 PlanID From tblPricePlans Where CourtID = @CourtID)

Set @ViolationStatusID = (Select Top 1 (CourtViolationStatusID) From TblCourtViolationStatus Where Replace(Description,' ','') = Replace(@ViolationStatus, ' ', ''))
If @ViolationStatusID Is Null
	Begin
		Insert Into TblCourtViolationStatus (Description, ShortDescription, ViolationCategory, SortOrder) Values (LTrim(RTrim(@ViolationStatus)), LTrim(RTrim(Left(@ViolationStatus, 4))), 0, 0)
		Set @ViolationStatusID = Scope_Identity()
	End

Set @IDFound = (Select top 1 RecordID From tblTicketsViolationsArchive Where (CauseNumber = @Cause_Number Or (TicketNumber_LA = @Ticket_Number And TicketNumber_LA <> '' And TicketNumber_LA Is Not Null)) And (UpdatedDate < @Updated_Date Or UpdatedDate Is Null))

		If @IDFound Is Not Null
			Begin
				Update tblTicketsArchive Set CourtDate = @Court_Date, GroupID = @GroupID, CRRT = @Court_Room_Number, CourtID = @CourtID Where (RecordID = @IDFound)
				Update tblTicketsViolationsArchive Set CourtDate = @Court_Date, ViolationStatusID = @ViolationStatusID, UpdatedDate = @Updated_Date, CourtNumber = @Court_Room_Number, CourtLocation = @CourtID, LoaderUpdateDate = GetDate() Where (CauseNumber = @Cause_Number Or (TicketNumber_LA = @Ticket_Number And TicketNumber_LA <> '' And TicketNumber_LA Is Not Null)) And (UpdatedDate < @Updated_Date  Or UpdatedDate Is Null)
				Update tblTicketsViolations Set CourtDate = @Court_Date, CourtViolationStatusID = @ViolationStatusID, UpdatedDate = @Updated_Date, CourtNumber = @Court_Room_Number, CourtID = @CourtID, PlanID = @PlanID Where (CaseNumAssignedByCourt = @Cause_Number) And (UpdatedDate < @Updated_Date  Or UpdatedDate Is Null)
				Set @Flag_Updated = 1
			End

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


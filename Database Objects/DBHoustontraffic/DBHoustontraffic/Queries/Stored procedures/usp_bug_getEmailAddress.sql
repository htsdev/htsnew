SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_bug_getEmailAddress]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_bug_getEmailAddress]
GO



CREATE procedure [dbo].[usp_bug_getEmailAddress] 

@DeveloperID as int

as 

Select USerName,EmailAddress from
	tbl_bug_users
	where
	Userid=@DeveloperID







GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


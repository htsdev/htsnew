IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'USP_HTS_GET_RECEIPT')
	BEGIN
		DROP  Procedure [dbo].[USP_HTS_GET_RECEIPT]
	END

GO

GO


--- Fahad 2769 (2-1-08) 
--- Error Fixed in Procedure TicketIdList have greater values i have changed ticketid to bigint in table @tickets
Create procedure USP_HTS_GET_RECEIPT -- 3991,3,'123028,'      
@empid  int,            
@LetterType int,            
@TicketIDList varchar(8000)            
            
as              
begin              
              
 declare @tickets table (                                        
 ticketid bigint                                            
 )               
              
insert into @tickets                                     
 select * from dbo.Sap_String_Param_bigint(@TicketIDList)                 
              
              
select * from tbl_hts_batch_receipt           
where cast(convert(varchar(12), ticketid_pk) + convert(varchar(12), BatchID) as bigint) IN (select ticketid from @tickets)              
order by BatchID              
end   


GO

GO


GRANT EXECUTE ON [dbo].[USP_HTS_GET_RECEIPT] TO [dbr_webuser]

GO


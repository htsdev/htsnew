USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_MAILER_Send_TrackBatches]    Script Date: 01/02/2014 03:55:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** 
Altered by:		Sabir Khan
Business Logic:	The procedure is used by LMS application to get data for selected date range.
				
List of Parameters:
	@lettertype:	Type of letter.            
	@catnum:		Category number.            
	@crtid :		Court ID 
	@BatchIDs :		Batch IDs             
	@printtype:		Type of Print.	
	
*******/
-- USP_MAILER_Send_TrackBatches 135,36,36, '22836,', 0  
ALTER procedure [dbo].[USP_MAILER_Send_TrackBatches]            
 (            
 @lettertype int,            
 @catnum int,            
 @crtid int,            
 @BatchIDs varchar(1000),            
 @printtype int = 0  -- language 0 english 1 spanish          
 )            
as            
       
--for barry mail piece      
declare @mailbarry int
-- tahir 5701 03/25/2009 changed the logic..     
set @mailbarry = 1 
-- 5701 end     
  
      
if @lettertype in (17,18)      
begin      
 select @mailbarry=count(batchid) from tblbatchletter      
 where lettertype=@lettertype      
 and datediff(month,batchsentdate,getdate())=0      
 and datediff(year,batchsentdate,getdate())=0      
 and datediff(day,batchsentdate,getdate()-1)>0      
 and batchsentdate>'7/3/2007 23:59:59:998'      
end      
else      
begin      
 select @mailbarry=count(batchid) from tblbatchletter      
 where lettertype=@lettertype      
 and datediff(month,batchsentdate,getdate())=0      
 and datediff(year,batchsentdate,getdate())=0      
 and datediff(day,batchsentdate,getdate()-1)>=0      
end      
--       

-- tahir 5701 03/25/2009 changed the logic..  
set @mailbarry = 1      
--5701 end

create table #letters (recordid  int , noteid int primary key not null)        
        
CREATE NONCLUSTERED INDEX IX_Table_1 ON #letters        
 (        
 recordid        
 )         
 declare @recCount int            
            
            
declare @user varchar(10)            
            
  
create table #warrants (recordid int, noteid int , midnumber varchar(20), zipcode varchar(20))        
CREATE NONCLUSTERED INDEX IX_Table_1 ON #warrants        
 (        
 recordid        
 )       
CREATE NONCLUSTERED INDEX IX2_Table_1 ON #warrants        
 (        
 noteid        
 )         
  
--Sabir Khan 6410 08/19/2009 Three new warrant letter added for HMC...  
if @lettertype in (41,42,43,65,66,67)  
 begin  
  
  if @catnum = @crtid            
   insert into #warrants             
   select distinct recordid,n.noteid  , n.midnumber, n.zipcode           
   from tblletternotes n, tblbatchletter b            
   where n.batchid_fk = b.batchid            
   and b.lettertype = @lettertype            
   and b.courtid = @catnum            
   and b.batchid in ( select * from dbo.sap_string_param(@batchids) )            
   union   
   select distinct d.recordid,n.noteid   , n.midnumber, n.zipcode             
   from tblletternotes n, tblbatchletter b , tblletternotes_detail d  
   where n.batchid_fk = b.batchid  and d.noteid= n.noteid  
   and b.lettertype = @lettertype            
   and b.courtid = @catnum            
   and b.batchid in ( select * from dbo.sap_string_param(@batchids) )            
  else            
   insert into #warrants             
   select distinct recordid,n.noteid  , n.midnumber, n.zipcode              
   from tblletternotes n, tblbatchletter b            
   where n.batchid_fk = b.batchid            
   and b.lettertype = @lettertype            
   and n.courtid = @crtid            
   and b.courtid = @catnum            
   and b.batchid in ( select * from dbo.sap_string_param(@batchids) )     
   union  
   select distinct d.recordid,n.noteid  , n.midnumber, n.zipcode              
   from tblletternotes n, tblbatchletter b  , tblletternotes_detail d          
   where n.batchid_fk = b.batchid    and d.noteid= n.noteid         
   and b.lettertype = @lettertype            
   and n.courtid = @crtid            
   and b.courtid = @catnum            
   and b.batchid in ( select * from dbo.sap_string_param(@batchids) )     
 end  
else  
 begin            
  if @catnum = @crtid            
   insert into #letters             
   select distinct recordid,n.noteid             
   from tblletternotes n, tblbatchletter b            
   where n.batchid_fk = b.batchid            
   and b.lettertype = @lettertype            
   and b.courtid = @catnum            
   and b.batchid in ( select * from dbo.sap_string_param(@batchids) )            
  else            
   insert into #letters             
   select distinct recordid,n.noteid             
   from tblletternotes n, tblbatchletter b            
   where n.batchid_fk = b.batchid            
   and b.lettertype = @lettertype            
   and n.courtid = @crtid            
   and b.courtid = @catnum            
   and b.batchid in ( select * from dbo.sap_string_param(@batchids) )            
 end  
            
select @user = upper(abbreviation) from tblusers where employeeid  in (            
 select top 1 empid from tblbatchletter where batchid in (select * from dbo.sap_string_param(@batchids))            
 )            
                    
            
-- HOUSTON ARR.  LETTERS            
if @lettertype = 9            
begin            
 Select  convert(varchar(20),n.noteid) as letterid, t.recordid,  upper(t.FirstName) as firstname,                                          
  upper(t.LastName) as lastname , upper(t.address1+ isnull(t.address2,'')) as address,upper(t.city) as city, s.state,             
  -- Rab Nawaz Khan 9704 09/26/2011 Default fine amount 100 logic is commented. . . 
  --case when isnull(v.fineamount,0) = 0 then 100 else v.fineamount end as FineAmount,
  isnull(v.fineamount,0) as FineAmount,violationdescription,c.CourtName, t.dpc,  
  -- Rab Nawaz Khan 9682 10/14/2011 Columns added for the global templates for HMC mailers  
  t.dp2 as dptwo, TicketNumber_PK, t.zipcode, 'ARRAIGNMEMT 1' AS lettername,  @user as Abb 
 -- tahir 5449 01/23/2009
 , dbo.GetPromoPriceTemplate(T.zipcode,@lettertype) AS promotemplate, c.Courtid, t.ZipCode AS zipmid, t.MidNumber, t.MidNumber AS midnum, v.CourtDate,ISNULL(n.isPromo,0) AS isPromo, isnull(apt.PromoMessageForLatter,'') as PromoMessage
 INTO #letter9            
 from 
 tblletternotes n 
 INNER JOIN tblticketsarchive t ON n.recordid = t.recordid 
 INNER JOIN tblticketsviolationsarchive v ON t.recordid = v.recordid
 INNER JOIN #letters tt ON n.noteid=tt.noteid
 INNER JOIN tblcourts c ON t.courtid = c.courtid 
 INNER JOIN tblcourtviolationstatus tcvs ON tcvs.courtviolationstatusid = v.violationstatusid  AND tcvs.CategoryID=2
 INNER JOIN tblstate s ON t.stateid_fk = s.stateid
 LEFT OUTER JOIN AttorneyPromoTemplates apt ON apt.PromoID = n.PromoID        
 where 
 t.Flag1 in ('Y','D','S')            
 and t.donotmailflag = 0             
 and t.clientflag=0 
 --Rab Nawaz Khan 9119 04/04/2011 added attorney association logic.
and len(isnull(v.attorneyname, '')+isnull(v.barcardnumber,'')) = 0 
and CHARINDEX('KUBOSH',isnull(v.BondingCompanyName,''))=0 

            
--and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
ALTER TABLE #letter9 ADD isFineAmount BIT NOT NULL DEFAULT 1
SELECT REcordid INTO #HmcAr FROM #letter9 WHERE ISNULL(FineAmount,0) = 0

UPDATE T
SET
	isFineAmount = 0
FROM #letter9 t INNER JOIN #HmcAr f ON f.recordid = t.recordid

SELECT * FROM #letter9 t
order by left(t.zipcode, 5), recordid   

DROP TABLE #HmcAr
DROP TABLE #letter9
          
end            
            
            
-- ARRAIGNMENT 2 & 3 LETTERS...            
if @lettertype in (10,11)            
begin            
 Select  convert(varchar(20),n.noteid) as letterid,  t.recordid , firstname,                            
 lastname, t.listdate, t.Address1 + isnull(t.address2,'') as address, t.City, s.STATE, t.Zipcode, v.ticketnumber_pk, t.midnumber,                                
 t.dp2 as dptwo, t.dpc, c.courtname, t.violationdate, left(t.zipcode, 5)+t.midnumber as zipmid, violationdescription, case when isnull(v.fineamount,0)=0 then 100 else v.fineamount end as FineAmount, violationstatusid,@user as Abb               
  from tblletternotes n , tblticketsarchive t, tblticketsviolationsarchive v, #letters tt, tblcourts c , tblstate s           
 where  n.noteid=tt.noteid            
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.courtid = c.courtid            
 and t.stateid_fk = s.stateid         
 order by left(t.zipcode, 5), t.midnumber            
end            
            
            
-- HOUSTON  DAYB4 ARR. LETTERS            
if @lettertype = 12            
begin            
 Select  convert(varchar(20),n.noteid) as letterid, t.recordid, upper( t.FirstName) as firstname,                                          
  upper(t.LastName) as lastname , upper(t.address1+ isnull(t.address2,'')) as address, upper(t.city) as city, s.state,             
 -- Rab Nawaz Khan 9704 09/26/2011 Default fine amount 100 logic is commented. . .
 -- case when isnull(v.fineamount,0) = 0 then 100 else v.fineamount end as FineAmount,
 isnull(v.fineamount,0) as FineAmount,
 violationdescription,c.CourtName, t.dpc, 
 -- Rab Nawaz Khan 9682 10/14/2011 Columns added for the global templates for HMC mailers                                           
  t.dp2 as dptwo, TicketNumber_PK, t.zipcode, 'WEEK BEFORE ARRAIGNMEMT' AS lettername, @user as Abb
 -- tahir 5449 01/23/2009
 , dbo.GetPromoPriceTemplate(T.zipcode,@lettertype) AS promotemplate, t.MidNumber, t.MidNumber AS midnum, t.ZipCode AS zipmid, t.CourtDate, c.Courtid,ISNULL(n.isPromo,0) AS isPromo, isnull(apt.PromoMessageForLatter,'') as PromoMessage
 into #tempHarris 
 from 
 tblletternotes n INNER JOIN tblticketsarchive t ON n.recordid = t.recordid
 inner join tblticketsviolationsarchive v ON t.recordid = v.recordid 
 INNER JOIN  #letters tt ON n.noteid=tt.noteid
 INNER JOIN tblcourts c ON t.courtid = c.courtid
 INNER JOIN tblcourtviolationstatus tcvs ON tcvs.courtviolationstatusid = v.violationstatusid AND tcvs.CategoryID=2
 INNER JOIN tblstate s ON t.stateid_fk = s.stateid
 LEFT OUTER JOIN AttorneyPromoTemplates apt ON apt.PromoID = n.PromoID          
 where    
 t.Flag1 in ('Y','D','S')            
 and t.donotmailflag = 0             
 and t.clientflag=0 
 --Rab Nawaz Khan 9119 04/04/2011 added attorney association logic.
and len(isnull(v.attorneyname, '')+isnull(v.barcardnumber,'')) = 0 
and CHARINDEX('KUBOSH',isnull(v.BondingCompanyName,''))=0 

        
--and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
 order by left(t.zipcode, 5), t.midnumber               

ALTER TABLE #tempHarris ADD isFineAmount BIT NOT NULL DEFAULT 1
SELECT REcordid INTO #fineHarris FROM #tempHarris WHERE ISNULL(FineAmount,0) = 0

UPDATE T
SET
	isFineAmount = 0
FROM #tempHarris t INNER JOIN #fineHarris f ON f.recordid = t.recordid

SELECT * FROM #tempHarris

DROP TABLE #fineHarris
DROP TABLE #tempHarris
            
end                    

-- Haris Ahmed 12/31/2011 HOUSTON  DAYB4 ARR. LETTERS            
if @lettertype = 127           
begin            
 Select  convert(varchar(20),n.noteid) as letterid, t.recordid, upper( t.FirstName) as firstname,                                          
  upper(t.LastName) as lastname , upper(t.address1+ isnull(t.address2,'')) as address, upper(t.city) as city, s.state,             
 -- Rab Nawaz Khan 9704 09/26/2011 Default fine amount 100 logic is commented. . .
 -- case when isnull(v.fineamount,0) = 0 then 100 else v.fineamount end as FineAmount,
 isnull(v.fineamount,0) as FineAmount,
 violationdescription,c.CourtName, t.dpc, 
 -- Rab Nawaz Khan 9682 10/14/2011 Columns added for the global templates for HMC mailers                                           
  t.dp2 as dptwo, TicketNumber_PK, t.zipcode, 'DAY BEFORE ARRAIGNMEMT' AS lettername, @user as Abb
 -- tahir 5449 01/23/2009
  , dbo.GetPromoPriceTemplate(T.zipcode,@lettertype) AS promotemplate, t.MidNumber, t.MidNumber AS midnum, t.ZipCode AS zipmid, t.CourtDate, c.Courtid,ISNULL(n.isPromo,0) AS isPromo, '' as PromoMessage
 into #tempHarris127 from tblletternotes n , tblticketsarchive t, tblticketsviolationsarchive v, #letters tt, tblcourts c, tblcourtviolationstatus tcvs  , tblstate s          
 where n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.courtid = c.courtid            
 and t.stateid_fk = s.stateid        
 and tcvs.courtviolationstatusid = v.violationstatusid          
 and tcvs.CategoryID=2               
 and t.Flag1 in ('Y','D','S')            
 and t.donotmailflag = 0             
 and t.clientflag=0 
 --Rab Nawaz Khan 9119 04/04/2011 added attorney association logic.
and len(isnull(v.attorneyname, '')+isnull(v.barcardnumber,'')) = 0 
and CHARINDEX('KUBOSH',isnull(v.BondingCompanyName,''))=0 

        
--and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
 order by left(t.zipcode, 5), t.midnumber                

ALTER TABLE #tempHarris127 ADD isFineAmount BIT NOT NULL DEFAULT 1
SELECT REcordid INTO #fineHarris127 FROM #tempHarris127 WHERE ISNULL(FineAmount,0) = 0

UPDATE T
SET
	isFineAmount = 0
FROM #tempHarris127 t INNER JOIN #fineHarris127 f ON f.recordid = t.recordid

SELECT * FROM #tempHarris127

DROP TABLE #fineHarris127
DROP TABLE #tempHarris127
            
end        
            
-- HOUSTON JUDGE LETTERS            
if @lettertype =13            
begin            
 Select distinct convert(varchar(20),n.noteid) as letterid, t.recordid,  upper( t.FirstName) as firstname,                                          
  upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,'')) as address, upper(t.city) as city, s.state,        
 -- Rab Nawaz Khan 9704 09/26/2011 Default fine amount 100 logic is commented. . .
 -- case when isnull(v.fineamount,0) = 0 then 100 else v.fineamount end as FineAmount,
  isnull(v.fineamount,0) as FineAmount, violationdescription,c.CourtName, t.dpc,                                          
  t.dp2 as dptwo, TicketNumber_PK as ticketnumber, t.zipcode, t.midnumber , t.midnumber as midnum, v.courtlocation as courtid,             
  left(n.zipcode,5)+ t.midnumber as zipmid, v.courtdate,@user as Abb,left(t.zipcode, 5) AS forOderby            
  INTO #letter13 from tblletternotes n , tblticketsarchive t, tblticketsviolationsarchive v, #letters tt, tblcourts c, tblletternotes_detail d, tblcourtviolationstatus tcvs    , tblstate s        
 where n.noteid=tt.noteid              
 and n.noteid = d.noteid            
 and d.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and t.courtid = c.courtid            
 and tcvs.courtviolationstatusid = v.violationstatusid            
 and tcvs.CategoryID=5             
 and t.Flag1 in ('Y','D','S')            
 and t.donotmailflag = 0             
 and t.clientflag=0 
 --Rab Nawaz Khan 9119 04/04/2011 added attorney association logic.
and len(isnull(v.attorneyname, '')+isnull(v.barcardnumber,'')) = 0 
and CHARINDEX('KUBOSH',isnull(v.BondingCompanyName,''))=0 

--and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
 and len(isnull(t.firstname,'')+isnull(t.lastname,''))>0           
 
ALTER TABLE #letter13 ADD isFineAmount BIT NOT NULL DEFAULT 1
SELECT REcordid INTO #tempjudge FROM #letter13 WHERE ISNULL(FineAmount,0) = 0

UPDATE T
SET
	isFineAmount = 0
FROM #letter13 t INNER JOIN #tempjudge f ON f.recordid = t.recordid

SELECT * FROM #letter13 t
order by t.forOderby, t.midnumber  

DROP TABLE #tempjudge
DROP TABLE #letter13
           
end            
            
            
-- DALLAS REGULAR & BOND LETTERS            
if @lettertype in (17,18)            
begin            
 Select  convert(varchar(20),tt.noteid) as letterid, t.recordid, upper( t.FirstName) as firstname,                                          
  upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,'')) as address, upper(t.city) as city, s.state,       
   case when isnull(v.bondamount,0)=0 then 100 else v.bondamount end as FineAmount,  
 violationdescription, c.CourtName, t.dpc,
 --Yasir Kamal 7226 01/18/2010 get promotional price                                          
 t.dp2 as dptwo, TicketNumber_PK, t.zipcode,@user as Abb,dbo.GetPromoPriceTemplate(T.zipcode,@lettertype) AS promotemplate                                                      
 into #temp2            
-- from  tblletternotes n , dallastraffictickets.dbo.tblticketsarchive t, dallastraffictickets.dbo.tblticketsviolationsarchive v,             
 from   dallastraffictickets.dbo.tblticketsarchive t, dallastraffictickets.dbo.tblticketsviolationsarchive v,             
  #letters tt, dallastraffictickets.dbo.tblcourts c  , dallastraffictickets.dbo.tblstate s          
 where  tt.recordid = t.recordid            
 --and n.noteid=tt.noteid              
      
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and t.courtid = c.courtid            
 and t.Flag1 in ('Y','D','S')            
 and t.donotmailflag = 0             
 and len(isnull(t.firstname,'')+isnull(t.lastname,''))>0   
--and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
 order by left(t.zipcode, 5), t.midnumber            
            
            
 select @recCount = count(distinct recordid) from #temp2              
 declare @table4 TABLE  (            
    [letterId] [varchar] (30)   ,            
    [recordid] [int] ,            
    [FirstName] [varchar] (20)  ,            
    [LastName] [varchar] (20)  ,            
    [address] [varchar] (100) ,            
    [city] [varchar] (50) ,            
    [state] [varchar] (2)  ,         
    [FineAmount] [money]  ,            
    [violationdescription] [varchar] (200) ,            
    [CourtName] [varchar] (50) ,            
    [dpc] [varchar] (10)  ,            
    [dptwo] [varchar] (10)  ,            
    [TicketNumber_PK] [varchar] (20)  ,            
    [zipcode] [varchar] (15) ,            
    [language] [int],            
    [Abb] [varchar] (10),
    --Yasir Kamal 7226 01/18/2010 get promotional price
	promotemplate [varchar] (100)                     
   )            
     
 if @lettertype =18    
 begin    
 insert into @table4 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
   violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, Abb,promotemplate)              
 select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount,            
   violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 0, Abb,promotemplate from #temp2              
            
 if @printtype = 1            
 -- DOUBLE SIDE PRINT........            
  begin            
   insert into @table4 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, Abb,promotemplate)              
   select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount,            
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 1, Abb,promotemplate from #temp2 ;             
  end         
 end       
 else    
 begin    
 insert into @table4 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
   violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, Abb,promotemplate)              
 select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount,            
   violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 0, Abb,promotemplate from #temp2              
            
 if @printtype = 1            
 -- DOUBLE SIDE PRINT........            
  begin            
   insert into @table4 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, Abb,promotemplate)              
   select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount,            
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 1, Abb,promotemplate from #temp2 ;             
  end      
 end     
        
 if @mailbarry=0         
 begin      
 select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table4           
    --order by left(t.zipcode, 5), t.midnumber  
    -- Rab Nawaz Khan 10187 04/12/2012 Address, City and Zip Code Changed for the Testing Advertisment mailer which send to Barry              
 union      
 select convert(varchar(20),@recCount) + ' letters sent' as letterid  , 0 as recordid , 'BARRY', 'BOBBIT', '7 Butterfly Place', 'Austin', 'TX', 0, 'N/A', 'DALLAS MUNICIPAL COURT', '0', '00' , 'N/A', '78738-1343' as zipcode, 0,@user,'0,35','0'      
 end      
 else      
    select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table4           
    --order by left(t.zipcode, 5), t.midnumber                
             
  --Yasir Kamal 7226 end          
end            
            
            
            
-- HOUSTON DLQ LETTERS         
if @lettertype in (19)            
begin            
 Select  convert(varchar(20),n.noteid) as letterid, t.recordid,  upper( t.FirstName) as firstname,                                          
  upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,'')) as address, upper(t.city) as city, s.state,       
 --case when isnull(v.bondamount,0)=0 then 100 else v.bondamount end as FineAmount,
 ISNULL(v.FineAmount, 0) AS FineAmount,
 violationdescription,c.CourtName, t.dpc, t.dp2 as dptwo, TicketNumber_PK, t.zipcode , v.courtdate, left(t.zipcode,5) +t.midnumber as zipmid,@user as Abb
 INTO #letter19            
  from tblletternotes n , tblticketsarchive t, tblticketsviolationsarchive v, #letters tt, tblcourts c , tblstate s           
 where n.noteid=tt.noteid              
 and n.recordid = t.recordid    
 and t.recordid = v.recordid            
  and t.stateid_fk = s.stateid        
 and t.courtid = c.courtid            
 and t.Flag1 in ('Y','D','S')            
 and t.donotmailflag = 0             
 and v.violationstatusid = 146
 --Rab Nawaz Khan 9119 04/04/2011 added attorney association logic.
and len(isnull(v.attorneyname, '')+isnull(v.barcardnumber,'')) = 0 
and CHARINDEX('KUBOSH',isnull(v.BondingCompanyName,''))=0 

-- Rab Nawaz Khan 10331 11/12/2012 added to hide the entire columm in mailer if fine amount is zero agains any violation . . .  
ALTER TABLE #letter19 ADD isFineAmount BIT NOT NULL DEFAULT 1
SELECT REcordid INTO #HmcDLQ FROM #letter19 WHERE ISNULL(FineAmount,0) = 0

UPDATE T
SET
	isFineAmount = 0
FROM #letter19 t INNER JOIN #HmcDLQ f ON f.recordid = t.recordid

SELECT * FROM #letter19 t
order by [zipmid]    

DROP TABLE #HmcDLQ
DROP TABLE #letter19

--and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)                
           
end            
            
-- HARRIS ARR. LETTER            
if @lettertype in (20)            
begin            
 Select  convert(varchar(20),n.noteid) as letterid, t.recordid, upper( t.FirstName) as firstname,                                          
  upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,'')) as address, upper(t.city) as city, s.state,       
  -- Rab Nawaz Khan 9704 09/26/2011 Default fine amount 100 logic is commented. . .
 --case when isnull(v.fineamount,0)=0 then 100 else v.fineamount end as FineAmount,
isnull(v.fineamount,0) as FineAmount,violationdescription,c.CourtName, t.dpc, 
-- Rab Nawaz Khan 9682 10/14/2011 Columns added for the global templates for HMC mailers  
 t.dp2 as dptwo, case when len(isnull(v.causenumber,''))>0 then v.causenumber else TicketNumber_PK end as TicketNumber_PK, t.zipcode , t.midnumber, t.courtid,@user as Abb, 
0 AS promotemplate , 'APPEARANCE' AS lettername, t.MidNumber AS midnum, t.ZipCode AS zipmid, t.ListDate AS courtdate
 INTO #letter20 from tblletternotes n , tblticketsarchive t, tblticketsviolationsarchive v, #letters tt, tblcourts c , tblstate s           
 where  n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and t.courtid = c.courtid            
 and t.Flag1 in ('Y','D','S')            
 and t.donotmailflag = 0             
 and t.clientflag = 0 
 --Muhammad Ali 8175 Update Businesss logic according to send SP..
 and v.violationstatusid  in (3,32,88,127)
 and isnull(v.juvenileflag,0) = 0  
 -- and (t.bonddate is null or datediff(day, t.bonddate,'1/1/1900')=0)  
--End 8175           
--and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
 order by left(t.zipcode, 5), t.midnumber  

ALTER TABLE #letter20 ADD isFineAmount BIT NOT NULL DEFAULT 1
SELECT REcordid INTO #HarisArr FROM #letter20 WHERE ISNULL(FineAmount,0) = 0

UPDATE T
SET
	isFineAmount = 0
FROM #letter20 t INNER JOIN #HarisArr f ON f.recordid = t.recordid

SELECT * FROM #letter20

DROP TABLE #HarisArr
DROP TABLE #letter20
        
end            
            
-- HARRIS BOND LETTER            
if @lettertype in (21)            
begin            
 Select  convert(varchar(20),n.noteid) as letterid, t.recordid,  upper( t.FirstName) as firstname,                                          
  upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,'')) as address, upper(t.city) as city, s.state,       
 -- Rab Nawaz Khan 9704 09/26/2011 Default fine amount 100 logic is commented. . .   
 --case when isnull(v.fineamount,0)=0 then 100 else v.fineamount end as FineAmount, 
 isnull(v.fineamount,0) as FineAmount,violationdescription,c.CourtName, t.dpc,                                          
  t.dp2 as dptwo, case when len(isnull(v.causenumber,''))>0 then v.causenumber else TicketNumber_PK end as TicketNumber_PK, t.zipcode , left(t.zipcode, 5) + ltrim(rtrim(t.midnumber)) as zipmid, t.violationdate as courtdate, t.courtid,@user as Abb            
 INTO #letter21 from tblletternotes n , tblticketsarchive t, tblticketsviolationsarchive v, #letters tt, tblcourts c , tblstate s           
 where  n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.courtid = c.courtid            
 and t.stateid_fk = s.stateid        
 and left(v.ticketnumber_pk, 1) <> 'F'            
 and t.Flag1 in ('Y','D','S')            
 and t.donotmailflag = 0             
 and t.clientflag = 0            
--and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
 order by left(t.zipcode, 5), t.midnumber 
 
 ALTER TABLE #letter21 ADD isFineAmount BIT NOT NULL DEFAULT 1
SELECT REcordid INTO #HarrisBond FROM #letter21 WHERE ISNULL(FineAmount,0) = 0

UPDATE T
SET
	isFineAmount = 0
FROM #letter21 t INNER JOIN #HarrisBond f ON f.recordid = t.recordid

SELECT * FROM #letter21

DROP TABLE #HarrisBond
DROP TABLE #letter21
            
end            
            
-- PASADENA ARR. LETTER            
if @lettertype in (22)            
begin            
 Select  convert(varchar(20),n.noteid) as letterid, t.recordid, upper( t.FirstName) as firstname,                                          
  upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,'')) as address, upper(t.city) as city, s.state,       
  -- Rab Nawaz Khan 9704 09/26/2011 Default fine amount 100 logic is commented. . . 
  -- case when isnull(v.fineamount,0)=0 then 100 else v.fineamount end as FineAmount,
  isnull(v.fineamount,0) as FineAmount,violationdescription,c.CourtName, t.dpc, 
  -- Rab Nawaz Khan 9682 10/14/2011 Columns added for the global templates for HMC mailers  
  t.dp2 as dptwo, TicketNumber_PK, t.zipcode , v.courtdate,@user as Abb ,
    0 AS promotemplate , 'APPEARANCE' AS lettername,  t.MidNumber AS midnum, t.ZipCode AS zipmid, c.Courtid
  INTO #letter22 from tblletternotes n , tblticketsarchive t, tblticketsviolationsarchive v, #letters tt, tblcourts c    , tblstate s        
 where n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and v.courtlocation = c.courtid            
 and t.Flag1 in ('Y','D','S')            
 and t.donotmailflag = 0             
 and t.clientflag = 0 
 and v.violationstatusid <> 80 --Abbas Qamar 9991 01/17/2012 Exclude dispose cases.         
--and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
 order by left(t.zipcode, 5), t.midnumber 
 
 ALTER TABLE #letter22 ADD isFineAmount BIT NOT NULL DEFAULT 1
SELECT REcordid INTO #pasadenaApp FROM #letter22 WHERE ISNULL(FineAmount,0) = 0

UPDATE T
SET
	isFineAmount = 0
FROM #letter22 t INNER JOIN #pasadenaApp f ON f.recordid = t.recordid

SELECT * FROM #letter22

DROP TABLE #pasadenaApp
DROP TABLE #letter22
            
end            
--Sabir Khan 7017 11/24/2009 Changes made for             
-- MONTGOMERY Criminal. LETTER            
if @lettertype in (24)            
begin            
 Select distinct convert(varchar(20),n.noteid) as letterid, upper(ltrim(rtrim(c.firstname))+ ' ' +ltrim(rtrim(c.lastname))) as name, 
 upper(c.address1 + isnull(' ' + c.address2,'')) as address, upper(c.city) as city, s.state, C.zipcode as zip , N.recordid, c.midnumber as span, 
 l.violationdescription as chargewording,  l.ticketviolationdate as bookingdate, l.causenumber as casenumber, c.clientflag, 
 left(c.zipcode,5) + ltrim(rtrim(c.midnumber)) as zipspan, n.listdate,            
 c.dp2 as dptwo, c.dpc,@user as Abb 
 from dbo.tblticketsarchive c   
 inner join  dbo.tblticketsviolationsarchive l on c.recordid = l.recordid
 inner join #letters tt on tt.recordid = c.recordid   
 inner join  tblletternotes n on n.noteid=tt.noteid    
 inner join dbo.tblstate s on s.stateid = c.stateid_fk
 where c.Flag1 in ('Y','D','S')   
 and l.violationstatusid <> 80 --Abbas Qamar 9991 01/17/2012 Exclude dispose cases.                 
--and (c.ncoa18flag = 1 or c.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
 order by ZIPSPAN             
end            
            
            
-- HOUSTON FTA LETTERS            
if @lettertype = 25             
begin            
            
 if @catnum = @crtid            
  begin            
   Select distinct convert(varchar(20),n.noteid) as letterid, t.recordid, upper(t.FirstName) as firstname, charindex('FTA', ticketnumber_pk) as isFTA,                                             
   upper(t.LastName) as lastname, upper(t.address1 + isnull(t.address2,'')) as  address, upper(t.city) as city, s.state,
   -- Rab Nawaz Khan 9704 09/26/2011 Default fine amount 100 logic is commented. . .   
   -- case when isnull(v.fineamount,0)=0 then 100 else v.fineamount end as FineAmount, 
	isnull(v.fineamount,0) as FineAmount, v.violationnumber_pk,v.violationdescription,CourtName, t.dpc,                                            
   t.dp2 as dptwo, TicketNumber_PK, n.zipcode, left(n.zipcode,5) + ltrim(rtrim(n.midnumber)) as zipmid,         
   -- Rab Nawaz Khan 9682 10/14/2011 Columns added for the global templates for HMC mailers      
   v.courtdate, t.courtid, n.midnumber, n.midnumber as midnum ,@user as Abb , v.ftalinkid, 'WARRANT' AS lettername                                            
   into #tmpFTA        
   from tblletternotes n , tblticketsarchive t, tblticketsviolationsarchive v, tblcourts c, tblletternotes_detail d, tblbatchletter b , tblstate s           
   where d.noteid = n.noteid             
   and d.recordid = t.recordid            
   and t.recordid = v.recordid            
   and v.courtlocation = c.courtid            
 and t.stateid_fk = s.stateid        
   and b.batchid = n.batchid_Fk            
   and n.batchid_fk in (select * from dbo.sap_string_param(@BatchIDs) )            
   and b.courtid = @catnum            
   and  t.donotmailflag = 0            
   and  t.Flag1 in ('Y','D','S')              
   and charindex('FTA', v.causenumber) <> 0            
   and v.FTAlinkid is not null             
   and v.violationstatusid = 186
   --Rab Nawaz Khan 9119 04/04/2011 added attorney association logic.
and len(isnull(v.attorneyname, '')+isnull(v.barcardnumber,'')) = 0 
and CHARINDEX('KUBOSH',isnull(v.BondingCompanyName,''))=0 
      
--and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
        
 insert into #tmpFTA        
   Select distinct a.letterid, t.recordid,  a.FirstName, charindex('FTA', v.ticketnumber_pk) as isFTA,                                             
   a.LastName, a.address, a.city, a.state,   
	--case when isnull(v.fineamount,0)=0 then 100 else v.fineamount end as FineAmount
	isnull(v.fineamount,0) as FineAmount, v.violationnumber_pk,v.violationdescription,c.CourtName, a.dpc,                                            
   a.dptwo, v.TicketNumber_PK, a.zipcode, a.zipmid,             
   v.courtdate, t.courtid, a.midnumber, a.midnumber as midnum ,@user as Abb , v.ftalinkid, lettername                                           
   from #tmpFTA a , tblticketsarchive t, tblticketsviolationsarchive v, tblcourts c           
   where a.ftalinkid = v.ftalinkid          
   and t.recordid = v.recordid            
   and v.courtlocation = c.courtid            
   and charindex('FTA', v.causenumber) = 0                
   and v.violationstatusid = 186
   --Rab Nawaz Khan 9119 04/04/2011 added attorney association logic.
and len(isnull(v.attorneyname, '')+isnull(v.barcardnumber,'')) = 0 
and CHARINDEX('KUBOSH',isnull(v.BondingCompanyName,''))=0 

--Rab Nawaz Khan 9685 09/14/20211 Remove cases having attorney association...
select tva.recordid,tva.violationnumber_pk,tva.violationdescription into #tempAttorney 
from #tmpFTA t inner join tblticketsviolationsarchive tva on tva.ftalinkid = t.ftalinkid 
where len(isnull(tva.attorneyname, '')+isnull(tva.barcardnumber,'')) <> 0

delete  f 
from #tmpFTA f
inner join #tempAttorney a on a.recordid = f.recordid and a.violationnumber_pk = f.violationnumber_pk
and a.violationdescription = f.violationdescription

       
--and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
       
-- select * from #tmpfta order by [zipmid] , [isFTA] desc 
--SELECT * INTO #tempHarris FROM #tmpFTA
ALTER TABLE #tmpFTA ADD isFineAmount BIT NOT NULL DEFAULT 1
SELECT REcordid INTO #HmcFta FROM #tmpFTA WHERE ISNULL(FineAmount,0) = 0

UPDATE T
SET
	isFineAmount = 0
FROM #tmpFTA t INNER JOIN #HmcFta f ON f.recordid = t.recordid

SELECT * FROM #tmpFTA
order by [zipmid] , [isFTA] desc 

DROP TABLE #tmpFTA
DROP TABLE #HmcFta
        
  end            
 else            
  begin            
Select distinct convert(varchar(20),n.noteid) as letterid, t.recordid,  upper( t.FirstName) as firstname, charindex('FTA', ticketnumber_pk) as isFTA,                                             
   upper(t.LastName) as lastname, upper(t.address1 + isnull(t.address2,'')) as  address, upper(t.city) as city, s.state,   
case when isnull(v.fineamount,0)=0 then 100 else v.fineamount end as FineAmount, v.violationdescription,CourtName, t.dpc,                                            
   t.dp2 as dptwo, TicketNumber_PK, n.zipcode, left(n.zipcode,5) + ltrim(rtrim(n.midnumber)) as zipmid,             
   v.courtdate, t.courtid, n.midnumber, n.midnumber as midnum,@user as Abb , v.ftalinkid                                             
   into #tmpFTA2        
   from tblletternotes n , tblticketsarchive t, tblticketsviolationsarchive v, tblcourts c, tblletternotes_detail d , tblstate s           
   where d.noteid = n.noteid             
   and d.recordid = t.recordid            
   and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
   and v.courtlocation = c.courtid            
   and n.batchid_fk in (select * from dbo.sap_string_param(@BatchIDs) )            
   and n.courtid = @crtid            
   and  t.donotmailflag = 0            
   and  t.Flag1 in ('Y','D','S')              
   and charindex('FTA', v.causenumber) <> 0            
   and v.FTAlinkid is not null             
   and v.violationstatusid = 186
      --Rab Nawaz Khan 9119 04/04/2011 added attorney association logic.
and len(isnull(v.attorneyname, '')+isnull(v.barcardnumber,'')) = 0 
and CHARINDEX('KUBOSH',isnull(v.BondingCompanyName,''))=0    
--and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
        
 insert into #tmpfta2        
   Select distinct a.letterid, t.recordid,  a.FirstName, charindex('FTA', v.ticketnumber_pk) as isFTA,                                             
   a.LastName, a.address, a.city, a.state, case when isnull(v.fineamount,0)=0 then 100 else v.fineamount end as FineAmount, v.violationdescription,c.CourtName, a.dpc,                                            
   a.dptwo, v.TicketNumber_PK, a.zipcode, a.zipmid,             
   v.courtdate, t.courtid, a.midnumber, a.midnumber as midnum ,@user as Abb , v.ftalinkid                                            
   from #tmpFTA2 a , tblticketsarchive t, tblticketsviolationsarchive v, tblcourts c           
   where a.ftalinkid = v.ftalinkid          
   and t.recordid = v.recordid            
   and v.courtlocation = c.courtid            
  and charindex('FTA', v.causenumber) = 0                
   and v.violationstatusid = 186
 --Rab Nawaz Khan 9119 04/04/2011 added attorney association logic.
and len(isnull(v.attorneyname, '')+isnull(v.barcardnumber,'')) = 0 
and CHARINDEX('KUBOSH',isnull(v.BondingCompanyName,''))=0   
 --and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
        
        
   select * from #tmpfta2 order by [zipmid] , [isFTA] desc        
  end            
END             
            
            
-- JIMS CRIMINAL NEW LETTER......            
if @lettertype = 26            
BEGIN            
            
 Select distinct convert(varchar(20),n.noteid) as letterid, c.clientid, upper(C.Name) as name, upper(c.address) as address, upper(c.city) as city, c.state, C.zip as zip , N.recordid, isnull(l.span,c.bookingnumber) as span, c.arrestdate,            
 chargewording, chargelevel, disposition, c.bookingdate, c.casenumber, clientflag, left(c.zip,5) + ltrim(rtrim(isnull(l.span,c.bookingnumber))) as zipspan, n.listdate,            
 c.dp2 as dptwo, c.dpc,@user as Abb            
 from jims.dbo.criminalcases c   
 left outer join  jims.dbo.criminal503 l on c.bookingnumber = l.bookingnumber    
 inner join #letters tt on tt.recordid = c.bookingnumber   
 inner join  tblletternotes n on n.noteid=tt.noteid    
 where c.Flag1 in ('Y','D','S')           
--and (c.ncoa18flag = 1 or c.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
 order by ZIPSPAN            
            
END            
            
-- SUGAR LAND LETTER...... 
--Sabir Khan 9683 09/15/2011 Need to remove last two character from ticket number and cancatinate -0x             
if @lettertype = 27            
BEGIN            
 Select distinct convert(varchar(20),n.noteid) as letterid, t.recordid,v.courtdate, upper(t.FirstName) as firstname,                                          
 upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,''))  as  address, upper(t.city) as city,  s.state,   
 -- Rab Nawaz Khan 9704 09/26/2011 Default fine amount 100 logic is commented. . .
 -- case when isnull(v.fineamount,0)=0 then 100 else v.fineamount end as FineAmount,
 isnull(v.fineamount,0) as FineAmount, violationdescription, t.dpc, 
 -- Rab Nawaz Khan 9682 10/14/2011 Columns added for the global templates for HMC mailers  
 t.dp2 as dptwo, SUBSTRING(v.ticketnumber_pk,0,LEN(v.ticketnumber_pk)-1) + '-0x' as TicketNumber_PK, t.zipcode, left(t.zipcode,5) + t.midnumber as zipmid ,@user as Abb,
  0 AS promotemplate,  t.midnumber, t.MidNumber AS midnum, c.courtid, 'APPEARANCE' as letterName
  INTO #lettert7  from tblletternotes n , tblticketsarchive t, tblticketsviolationsarchive v, #letters tt, tblcourts c , tblstate s           
 where  n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and t.courtid = c.courtid            
 and t.Flag1 in ('Y','D','S')            
 and t.donotmailflag = 0 
 and v.violationstatusid <> 80 --Abbas Qamar 9991 01/17/2012 Exclude dispose cases.            
 and t.clientflag = 0      
--and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
 order by [zipmid]  
 
 ALTER TABLE #lettert7 ADD isFineAmount BIT NOT NULL DEFAULT 1
SELECT REcordid INTO #sugerland FROM #lettert7 WHERE ISNULL(FineAmount,0) = 0

UPDATE T
SET
	isFineAmount = 0
FROM #lettert7 t INNER JOIN #sugerland f ON f.recordid = t.recordid

SELECT * FROM #lettert7

DROP TABLE #sugerland
DROP TABLE #lettert7
           
end   

--Sabir Khan 9329 05/26/2011 South Houston Appearance Mailer Added.
if @lettertype = 121            
BEGIN            
 Select distinct convert(varchar(20),n.noteid) as letterid, t.recordid,v.courtdate, upper(t.FirstName) as firstname,                                          
 upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,''))  as  address, upper(t.city) as city,  s.state,   
 -- Rab Nawaz Khan 9704 09/26/2011 Default fine amount 100 logic is commented. . .
 --case when isnull(v.fineamount,0)=0 then 100 else v.fineamount end as FineAmount,violationdescription, t.dpc, 
isnull(v.fineamount,0) as FineAmount,violationdescription, t.dpc, 
-- Rab Nawaz Khan 9682 10/14/2011 Columns added for the global templates for HMC mailers  
 t.dp2 as dptwo, v.ticketnumber_pk, t.zipcode, left(t.zipcode,5) + t.midnumber as zipmid ,@user as Abb,
  0 AS promotemplate,  t.MidNumber, t.MidNumber AS midnum, c.courtid, c.CourtName, 'APPEARANCE' as letterName
 INTO #tlettertype121 from tblletternotes n , tblticketsarchive t, tblticketsviolationsarchive v, #letters tt, tblcourts c , tblstate s           
 where  n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and t.courtid = c.courtid            
 and t.Flag1 in ('Y','D','S')            
 and t.donotmailflag = 0             
 and t.clientflag = 0  
--and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
 order by [zipmid]  
 
 ALTER TABLE #tlettertype121 ADD isFineAmount BIT NOT NULL DEFAULT 1
SELECT REcordid INTO #sohoApp FROM #tlettertype121 WHERE ISNULL(FineAmount,0) = 0

UPDATE T
SET
	isFineAmount = 0
FROM #tlettertype121 t INNER JOIN #sohoApp f ON f.recordid = t.recordid

SELECT * FROM #tlettertype121

DROP TABLE #sohoApp
DROP TABLE #tlettertype121
           
end        
            
            
-- MISSOURI ARR LETTER......            
if @lettertype = 28            
BEGIN            
 Select distinct convert(varchar(20),n.noteid) as letterid, t.recordid, v.courtdate, upper(t.FirstName) as firstname, 
 upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,'')) as  address, upper(t.city) as city, s.state, 
 isnull(v.fineamount, 0) as fineamount, violationdescription, t.dpc, t.dp2 as dptwo, v.ticketnumber_pk, t.zipcode, left(t.zipcode,5) + t.midnumber as zipmid,
 @user as Abb, 'Missouri City' as CourtName, 
'ARRAIGNMENT' as lettername, 0 as promotemplate, t.midnumber, t.midnumber as midnum, t.courtid,
CAST(0 AS BIT) as isSpanish
into #MissouryData
from tblletternotes n , tblticketsarchive t, tblticketsviolationsarchive v, #letters tt, tblcourts c  , tblstate s 
 where  n.noteid=tt.noteid 
 and n.recordid = t.recordid 
 and t.recordid = v.recordid 
 and t.courtid = c.courtid 
 and t.stateid_fk = s.stateid 
 and t.Flag1 in ('Y','D','S') 
 and t.donotmailflag = 0 
 and t.clientflag = 0 
 and v.violationstatusid = 116
 
 ALTER TABLE #MissouryData 
 ADD isfineamount BIT NOT NULL DEFAULT 1  
 
 select recordid into #MissouriEmptyfine from #MissouryData   WHERE ISNULL(FineAmount,0) = 0  
 
 update t   
 set t.isfineAmount = 0  
 from #MissouryData t inner join #MissouriEmptyfine f on t.recordid = f.recordid
 
 Select letterid, recordid, courtdate, firstname, lastname, address, city, state, fineamount, violationdescription, dpc, 
 dptwo, ticketnumber_pk, zipcode, zipmid, Abb, CourtName, lettername, promotemplate, midnumber, midnum, courtid, isSpanish, isfineamount
 FROM  #MissouryData
 ORDER BY zipmid
 
END            
            
-- DALLAS APPEARANCE (NEW) LETTER            
if @lettertype = 29            
begin            
 Select  convert(varchar(20),n.noteid) as letterid, t.recordid,  upper(t.FirstName) as firstname,                                          
 upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,''))  as  address, upper(t.city) as city,  s.state,   
case when isnull(v.fineamount,0)=0 then 100 else v.fineamount end as FineAmount,violationdescription, c.CourtName, t.dpc,                                          
 t.dp2 as dptwo, TicketNumber_PK, t.zipcode , dateadd(day, 21, t.violationdate) as courtdate ,@user as Abb ,
 -- Sabir Khan 6361 08/11/2009 Get promotional price scheme...
 dbo.GetPromoPriceTemplate(T.zipcode,@lettertype) AS promotemplate                                               
 into #temp3            
 from  tblletternotes n , dallastraffictickets.dbo.tblticketsarchive t, dallastraffictickets.dbo.tblticketsviolationsarchive v,             
  #letters tt, dallastraffictickets.dbo.tblcourts c    , dallastraffictickets.dbo.tblstate s        
 where  n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and t.courtid = c.courtid            
 and t.lastname is not null              
 and t.firstname is not null            
 and t.Flag1 in ('Y','D','S')            
 and isnull(t.donotmailflag,0)   = 0             
 and v.violationbonddate is null     
--and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
 order by left(t.zipcode, 5), t.midnumber            
            
            
 select @recCount = count(distinct recordid) from #temp3              
 declare @table5 TABLE  (            
    [letterId] [varchar] (30)   ,            
    [recordid] [int] ,            
    [FirstName] [varchar] (20)  ,            
    [LastName] [varchar] (20)  ,            
    [address] [varchar] (100) ,            
    [city] [varchar] (50) ,            
    [state] [varchar] (2)  ,            
    [FineAmount] [money]  ,            
    [violationdescription] [varchar] (200) ,            
    [CourtName] [varchar] (50) ,            
    [dpc] [varchar] (10)  ,            
    [dptwo] [varchar] (10)  ,            
    [TicketNumber_PK] [varchar] (20)  ,            
    [zipcode] [varchar] (15) ,            
    [language] [int],            
    [courtdate] [datetime],            
    [Abb] [varchar] (10),
    --Yasir Kamal 7226 01/18/2010 datatype changed from int to varchar
    [promotemplate] [varchar] (100)          
   )            
            
 insert into @table5 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
   violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb,promotemplate)              
 select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount,            
   violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 0, courtdate, Abb, promotemplate from #temp3              
            
 if @printtype = 1            
 -- DOUBLE SIDE PRINT........            
  begin            
   insert into @table5 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb, promotemplate)              
   select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount,            
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 1, courtdate, Abb, promotemplate from #temp3 ;             
  end            
 
 if @mailbarry=0          
 begin      
 select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table5             
 union      
 -- Rab Nawaz Khan 10187 04/12/2012 Address, City and Zip Code Changed for the Testing Advertisment mailer which send to Barry         
  select convert(varchar(20),@recCount) + ' letters sent' as letterid  , 0 as recordid , 'BARRY', 'BOBBIT', '7 Butterfly Place', 'Austin', 'TX', 0, 'N/A', 'DALLAS MUNICIPAL COURT', '0', '00' , 'N/A', '78738-1343' as zipcode, 0, '1/1/1900',@user ,'0,35.00', '0'  
  --Yasir Kamal 7226 end
     
      
 end      
 else      
 select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table5             
         
             
            
end            
            
            
-- DALLAS APPEARANCE DAY OF LETTER            
if @lettertype = 30            
begin            
 Select  convert(varchar(20),n.noteid) as letterid, t.recordid,   upper(t.FirstName) as firstname,                                          
 upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,''))  as  address, upper(t.city) as city,  s.state,   
case when isnull(v.fineamount,0)=0 then 100 else v.fineamount end as FineAmount,violationdescription, c.CourtName, t.dpc,                                          
 t.dp2 as dptwo, TicketNumber_PK, t.zipcode , dateadd(day, 21, t.violationdate) as courtdate ,@user as Abb,
 -- Sabir Khan 6361 08/11/2009 Get promotional price scheme...
 dbo.GetPromoPriceTemplate(T.zipcode,@lettertype) AS promotemplate                                               
 into #temp4            
 from  tblletternotes n , dallastraffictickets.dbo.tblticketsarchive t, dallastraffictickets.dbo.tblticketsviolationsarchive v,             
  #letters tt, dallastraffictickets.dbo.tblcourts c, dallastraffictickets.dbo.tblstate s            
 where  n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and t.courtid = c.courtid            
 and t.lastname is not null              
 and t.firstname is not null            
 and t.Flag1 in ('Y','D','S')            
 and isnull(t.donotmailflag,0)   = 0             
and v.violationbonddate is null         
--and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
 order by left(t.zipcode, 5), t.midnumber            
            
            
 select @recCount = count(distinct recordid) from #temp4              
 declare @table6 TABLE  (            
    [letterId] [varchar] (30)   ,            
    [recordid] [int] ,            
    [FirstName] [varchar] (20)  ,            
    [LastName] [varchar] (20)  ,            
    [address] [varchar] (100) ,            
    [city] [varchar] (50) ,            
    [state] [varchar] (2)  ,            
    [FineAmount] [money]  ,            
    [violationdescription] [varchar] (200) ,            
    [CourtName] [varchar] (50) ,            
    [dpc] [varchar] (10)  ,            
    [dptwo] [varchar] (10)  ,            
    [TicketNumber_PK] [varchar] (20)  ,            
    [zipcode] [varchar] (15) ,            
    [language] [int],            
    [courtdate] [datetime],            
    [Abb] [varchar] (10) ,
    --Yasir Kamal 7226 01/18/2010 data type changed from int to varchar
    [promotemplate] [varchar] (100)           
   )            
            
 insert into @table6 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
   violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb, promotemplate)              
 select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount+60,            
   violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 0, courtdate, Abb, promotemplate from #temp4              
            
 if @printtype = 1            
 -- DOUBLE SIDE PRINT........            
  begin            
   insert into @table6 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb, promotemplate)              
   select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount+60,            
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 1, courtdate, Abb, promotemplate from #temp4 ;            
  end            
       
 if @mailbarry=0        
 begin             
 select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table6       
 union 
 -- Rab Nawaz Khan 10187 04/12/2012 Address, City and Zip Code Changed for the Testing Advertisment mailer which send to Barry       
 select convert(varchar(20),@recCount) + ' letters sent' as letterid  , 0 as recordid , 'BARRY', 'BOBBIT', '7 Butterfly Place', 'Austin', 'TX', 0, 'N/A', 'DALLAS MUNICIPAL COURT', '0', '00' , 'N/A', '78738-1343' as zipcode, 0, '1/1/1900', @user ,'0,35.00', '0'   
  
    --Yasir Kamal 7226 end
            
 end      
 else      
 select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table6             
         
             
            
end            
            
            
-- DALLAS JP APPEARANCE LETTER            
if @lettertype = 31            
begin           
 Select  convert(varchar(20),n.noteid) as letterid, t.recordid,  upper(t.FirstName) as firstname,                                          
 upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,''))  as  address, upper(t.city) as city,  s.state,   
case when isnull(v.fineamount,0)=0 then 100 else v.fineamount end as FineAmount,violationdescription, c.CourtName, t.dpc,                                          
 t.dp2 as dptwo, isnull(v.causenumber,v.ticketnumber_pk) as  TicketNumber_PK, t.zipcode , dateadd(day, 21, t.violationdate) as courtdate ,@user as Abb                                              
 into #temp5            
 from  tblletternotes n , dallastraffictickets.dbo.tblticketsarchive t, dallastraffictickets.dbo.tblticketsviolationsarchive v,             
  #letters tt, dallastraffictickets.dbo.tblcourts c  , dallastraffictickets.dbo.tblstate s          
 where  n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and t.courtid = c.courtid            
 and len(isnull(t.lastname,'') + isnull(t.firstname,'') ) > 0             
 and t.Flag1 in ('Y','D','S')            
 and isnull(t.donotmailflag,0)   = 0        
--and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
 order by left(t.zipcode, 5), t.midnumber            
            
            
 select @recCount = count(distinct recordid) from #temp5              
 declare @table7 TABLE  (            
    [letterId] [varchar] (30)   ,            
    [recordid] [int] ,            
    [FirstName] [varchar] (20)  ,            
    [LastName] [varchar] (20)  ,            
    [address] [varchar] (100) ,            
    [city] [varchar] (50) ,            
    [state] [varchar] (2)  ,          
    [FineAmount] [money]  ,            
    [violationdescription] [varchar] (200) ,            
    [CourtName] [varchar] (50) ,            
    [dpc] [varchar] (10)  ,            
    [dptwo] [varchar] (10)  ,            
    [TicketNumber_PK] [varchar] (20)  ,            
    [zipcode] [varchar] (15) ,            
    [language] [int],            
    [courtdate] [datetime],            
    [Abb] [varchar] (10)            
   )            
            
 insert into @table7 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
   violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb)              
 select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount,            
   violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 0, courtdate, Abb from #temp5              
            
 if @printtype = 1            
 -- DOUBLE SIDE PRINT........            
  begin            
   insert into @table7 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb)              
   select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount,            
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 1, courtdate, Abb from #temp5 ;             
  end            
       
 if @mailbarry=0         
 begin        
 select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table7        
    union 
    -- Rab Nawaz Khan 10187 04/12/2012 Address, City and Zip Code Changed for the Testing Advertisment mailer which send to Barry 
    select convert(varchar(20),@recCount) + ' letters sent' as letterid  , 0 as recordid , 'BARRY', 'BOBBIT', '7 Butterfly Place', 'Austin', 'TX', 0, 'N/A', 'DALLAS MUNICIPAL COURT', '0', '00' , 'N/A', '78738-1343' as zipcode, 0, '1/1/1900',@user , '0' 
 
    
      
 end      
 else        
    select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table7                
            
end            
            
            
-- STAFFORD ARR LETTER......            
if @lettertype = 32            
BEGIN            
 Select distinct convert(varchar(20),n.noteid) as letterid, t.recordid,v.courtdate, upper(t.FirstName) as firstname,                            
 upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,'')) as  address, upper(t.city) as city, s.state,             
 -- Rab Nawaz Khan 9704 09/26/2011 Default fine amount 100 logic is commented. . . 
 -- case when isnull(v.fineamount,0)=0 then 100 else v.fineamount end as FineAmount
 isnull(v.fineamount,0) as FineAmount,violationdescription, t.dpc,   
 -- Rab Nawaz Khan 9682 10/14/2011 Columns added for the global templates for HMC mailers  
 t.dp2 as dptwo, v.ticketnumber_pk, t.zipcode, left(t.zipcode,5) + t.midnumber as zipmid,            
 v.ticketviolationdate as courtdate2 , t.midnumber ,@user as Abb,
  0 AS promotemplate, 'ARRAIGNMENT' AS lettername, t.MidNumber AS midnum,  c.Courtid
 INTO #lettertype32 from tblletternotes n , tblticketsarchive t, tblticketsviolationsarchive v, #letters tt, tblcourts c , tblstate s           
 where  n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and t.courtid = c.courtid            
 and t.Flag1 in ('Y','D','S')            
 and t.donotmailflag = 0             
 and t.clientflag = 0 
--and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
 order by [zipmid]   
 
ALTER TABLE #lettertype32 ADD isFineAmount BIT NOT NULL DEFAULT 1
SELECT REcordid INTO #staffordArr FROM #lettertype32 WHERE ISNULL(FineAmount,0) = 0

UPDATE T
SET
	isFineAmount = 0
FROM #lettertype32 t INNER JOIN #staffordArr f ON f.recordid = t.recordid

SELECT * FROM #lettertype32

DROP TABLE #staffordArr
DROP TABLE #lettertype32
          
END            
                 
-- MISSOURI WARRANT LETTER......            
if @lettertype = 33            
BEGIN            
 Select distinct convert(varchar(20),n.noteid) as letterid, t.recordid,v.courtdate, upper(t.FirstName) as firstname,                                          
 upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,'')) as  address, upper(t.city) as city, s.state,             
 case when isnull(v.fineamount,0)=0 then 100 else v.fineamount end as FineAmount,violationdescription, t.dpc,                                          
 t.dp2 as dptwo, v.ticketnumber_pk  as ticketnumber, t.zipcode, left(t.zipcode,5) + t.midnumber as zipmid,            
 v.ticketviolationdate as violationdate , t.midnumber ,@user as Abb                                              
  from tblletternotes n , tblticketsarchive t, tblticketsviolationsarchive v, #letters tt, tblcourts c  , tblstate s          
 where  n.noteid=tt.noteid       
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and t.courtid = c.courtid            
 and t.Flag1 in ('Y','D','S')            
 and t.donotmailflag = 0             
 and t.clientflag = 0         
 and v.violationstatusid=209         
--and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
 order by [zipmid]            
            
            
END          
        
-- HMC NCOA   LETTERS            
if @lettertype = 34            
begin            
 Select  convert(varchar(20),n.noteid) as letterid, t.recordid,  upper(t.FirstName) as firstname,                                          
 upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,''))  as  address, upper(t.city) as city,  s.state,          
 case when isnull(v.fineamount,0) = 0 then 100 else v.fineamount end as FineAmount,violationdescription,c.CourtName, t.dpc,                                          
  t.dp2 as dptwo, TicketNumber_PK, t.zipcode,@user as Abb             
  from tblletternotes n , tblticketsarchive t, tblticketsviolationsarchive v, #letters tt, tblcourts c, tblcourtviolationstatus tcvs    , tblstate s        
 where n.noteid=tt.noteid          
  and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and t.courtid = c.courtid            
 and tcvs.courtviolationstatusid = v.violationstatusid            
 and tcvs.CategoryID=2               
 and t.Flag1 in ('Y','D','S')            
 and t.donotmailflag = 0             
 and t.clientflag=0           
--and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
 order by left(t.zipcode, 5), t.midnumber            
end      
            
-- HOUSTON MCOA  LETTERS            
if @lettertype = 35        
begin            
 Select  convert(varchar(20),n.noteid) as letterid, t.recordid,  upper(t.FirstName) as firstname,                                          
 upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,''))  as  address, upper(t.city) as city,  s.state,          
 case when isnull(v.fineamount,0) = 0 then 100 else v.fineamount end as FineAmount,violationdescription,c.CourtName, t.dpc,                                          
  t.dp2 as dptwo, TicketNumber_PK, t.zipcode,@user as Abb             
  from tblletternotes n , tblticketsarchive t, tblticketsviolationsarchive v, #letters tt, tblcourts c, tblcourtviolationstatus tcvs    , tblstate s        
 where n.noteid=tt.noteid          
  and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and t.courtid = c.courtid            
 and tcvs.courtviolationstatusid = v.violationstatusid            
 and tcvs.CategoryID=2               
 and t.Flag1 in ('Y','D','S')            
 and t.donotmailflag = 0             
 and t.clientflag=0            
--and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
 order by left(t.zipcode, 5), t.midnumber            
end            
      
-- STAFFORD WARRANT LETTER......            
if @lettertype = 36            
BEGIN            
 Select distinct convert(varchar(20),n.noteid) as letterid, t.recordid,v.courtdate, upper(t.FirstName) as firstname,                                          
 upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,'')) as  address, upper(t.city) as city, s.state,             
  case when isnull(v.fineamount,0) = 0 then 100 else v.fineamount end as FineAmount,violationdescription,c.CourtName, t.dpc,                                          
 t.dp2 as dptwo, v.ticketnumber_pk  as ticketnumber, t.zipcode, left(t.zipcode,5) + t.midnumber as zipmid,            
 T.violationdate as violationdate , t.midnumber ,@user as Abb                                              
 from tblletternotes n , tblticketsarchive t, tblticketsviolationsarchive v, #letters tt, tblcourts c  , tblstate s          
 where  n.noteid=tt.noteid             
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and t.courtid = c.courtid            
 and t.Flag1 in ('Y','D','S')            
 and t.donotmailflag = 0             
 and t.clientflag = 0         
--and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
 order by [zipmid]            
           
END   
  
-- DALLAS CRIMINAL LETTER......            
if @lettertype = 37            
BEGIN            
            
Select  convert(varchar(20),n.noteid) as letterid, t.recordid,   upper(t.FirstName) + ' '+   
 upper(t.LastName) as name, upper(t.address1+ isnull(' ' + t.address2,''))  as  address, upper(t.city) as city,  s.state,   
violationdescription as chargewording, c.CourtName, t.dpc,   t.clientflag, t.midnumber as span, left(t.zipcode,5) + t.midnumber as zipspan,                                       
 t.dp2 as dptwo, isnull(v.causenumber,v.ticketnumber_pk)  as casenumber, t.zipcode as zip ,v.courtdate as bookingdate ,@user as Abb  
  from  tblletternotes n , dallastraffictickets.dbo.tblticketsarchive t, dallastraffictickets.dbo.tblticketsviolationsarchive v,             
  #letters tt, dallastraffictickets.dbo.tblcourts c  , dallastraffictickets.dbo.tblstate s          
 where  n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and t.courtid = c.courtid            
 and len(isnull(t.lastname,'') + isnull(t.firstname,'') ) > 0             
 and t.Flag1 in ('Y','D','S')            
 and isnull(t.donotmailflag,0)   = 0        
--and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
 order by left(t.zipcode, 5), t.midnumber            
            
                  
END      
  
  
-- DALLAS JP APPEARANCE DAY OF LETTER            
if @lettertype = 38            
begin            
 Select  convert(varchar(20),n.noteid) as letterid, t.recordid,   upper(t.FirstName) as firstname,                                          
 upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,''))  as  address, upper(t.city) as city,  s.state,   
case when isnull(v.fineamount,0)=0 then 100 else v.fineamount end as FineAmount,violationdescription, c.CourtName, t.dpc, 
--Yasir Kamal 7226 01/18/2010 get promotional price                   
 t.dp2 as dptwo,  isnull(v.causenumber,v.ticketnumber_pk)  as TicketNumber_PK, t.zipcode , dateadd(day, 21, t.violationdate) as courtdate ,@user as Abb, dbo.GetPromoPriceTemplate(T.zipcode,@lettertype) AS promotemplate          
 --Yasir Kamal 6838 10/23/2009 IMB added.
 --Sabir Khan 6912 11/11/2009 remove IMBImage field...
--master.dbo.fn_getIMBImage('00040001120' + RIGHT(REPLICATE('0',9) + CAST(n.noteid AS VARCHAR(9)),9)   ,replace(n.zipcode, '-','')+isnull(n.dp2,'')) as IMBImage                                               
 into #temp14            
 from  tblletternotes n , dallastraffictickets.dbo.tblticketsarchive t, dallastraffictickets.dbo.tblticketsviolationsarchive v,             
  #letters tt, dallastraffictickets.dbo.tblcourts c, dallastraffictickets.dbo.tblstate s            
 where  n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and t.courtid = c.courtid            
 and t.lastname is not null              
 and t.firstname is not null            
 and t.Flag1 in ('Y','D','S')            
 and isnull(t.donotmailflag,0)   = 0             
and v.violationbonddate is null         
--and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
 order by left(t.zipcode, 5), t.midnumber            
            
            
 select @recCount = count(distinct recordid) from #temp14              
 declare @table16 TABLE  (            
    [letterId] [varchar] (30)   ,            
    [recordid] [int] ,            
    [FirstName] [varchar] (20)  ,            
    [LastName] [varchar] (20)  ,            
    [address] [varchar] (100) ,            
    [city] [varchar] (50) ,            
    [state] [varchar] (2)  ,            
    [FineAmount] [money]  ,            
    [violationdescription] [varchar] (200) ,            
    [CourtName] [varchar] (50) ,            
    [dpc] [varchar] (10)  ,            
    [dptwo] [varchar] (10)  ,            
    [TicketNumber_PK] [varchar] (20)  ,            
    [zipcode] [varchar] (15) ,            
    [language] [int],            
    [courtdate] [datetime],            
    [Abb] [varchar] (10),
    promotemplate [varchar] (100)
    --Yasir Kamal 6838 10/23/2009 IMB added.
    --Sabir Khan 6912 11/11/2009 remove IMBImage field...
    --[IMBImage] [Image]            
   )            
            
 insert into @table16 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
   violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb,promotemplate)              
 select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount+60,            
   violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 0, courtdate, Abb,promotemplate from #temp14              
            
 if @printtype = 1            
 -- DOUBLE SIDE PRINT........            
  begin            
   insert into @table16 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb,promotemplate)              
   select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount+60,            
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 1, courtdate, Abb,promotemplate from #temp14 ;            
  end            
       
 if @mailbarry=0        
 begin             
 select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table16       
 UNION ALL    
 -- Rab Nawaz Khan 10187 04/12/2012 Address, City and Zip Code Changed for the Testing Advertisment mailer which send to Barry    
 select convert(varchar(20),@recCount) + ' letters sent' as letterid  , 0 as recordid , 'BARRY', 'BOBBIT', '7 Butterfly Place', 'Austin', 'TX', 0, 'N/A', 'DALLAS MUNICIPAL COURT', '0', '00' , 'N/A', '78738-1343' as zipcode, 0, '1/1/1900', @user ,'0,35.00', '0'  
   
    
            
 end      
 else      
 select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table16             
         
             
            
end            
  
-- DALLAS JP APPEARANCE LETTER NEW           
if @lettertype = 39            
begin           
 Select  convert(varchar(20),n.noteid) as letterid, t.recordid,   upper(t.FirstName) as firstname,                                          
 upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,''))  as  address, upper(t.city) as city,  s.state,   
case when isnull(v.fineamount,0)=0 then 100 else v.fineamount end as FineAmount,violationdescription, c.CourtName, t.dpc,                                          
 t.dp2 as dptwo, isnull(v.causenumber,v.ticketnumber_pk)  as TicketNumber_PK, t.zipcode , dateadd(day, 21, t.violationdate) as courtdate ,@user as Abb,
 --Yasir Kamal 6838 10/23/2009 IMB added.
 --Sabir Khan 6912 11/11/2009 remove IMBImage field...
--master.dbo.fn_getIMBImage('00040001120' + RIGHT(REPLICATE('0',9) + CAST(n.noteid AS VARCHAR(9)),9)   ,replace(n.zipcode, '-','')+isnull(n.dp2,'')) as IMBImage,   
case when charindex('sheriff',complainant_agency) <> 0 or charindex('constable',complainant_agency) <> 0 then 1 else 0 end as IsSheriff,dbo.GetPromoPriceTemplate(T.zipcode,@lettertype) AS promotemplate                                                        
 into #temp15            
 from  tblletternotes n , dallastraffictickets.dbo.tblticketsarchive t, dallastraffictickets.dbo.tblticketsviolationsarchive v,             
  #letters tt, dallastraffictickets.dbo.tblcourts c  , dallastraffictickets.dbo.tblstate s          
 where  n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and t.courtid = c.courtid            
 and len(isnull(t.lastname,'') + isnull(t.firstname,'') ) > 0             
 and t.Flag1 in ('Y','D','S')            
 and isnull(t.donotmailflag,0)   = 0        
--and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
 order by left(t.zipcode, 5), t.midnumber            
            
            
 select @recCount = count(distinct recordid) from #temp15              
 declare @table17 TABLE  (            
    [letterId] [varchar] (30)   ,            
    [recordid] [int] ,            
    [FirstName] [varchar] (20)  ,            
    [LastName] [varchar] (20)  ,            
    [address] [varchar] (100) ,            
    [city] [varchar] (50) ,            
    [state] [varchar] (2)  ,          
    [FineAmount] [money]  ,            
    [violationdescription] [varchar] (200) ,            
    [CourtName] [varchar] (50) ,            
    [dpc] [varchar] (10)  ,            
    [dptwo] [varchar] (10)  ,            
    [TicketNumber_PK] [varchar] (20)  ,            
    [zipcode] [varchar] (15) ,            
    [language] [int],            
    [courtdate] [datetime],            
    [Abb] [varchar] (10) ,
    --Yasir Kamal 6838 10/23/2009 IMB added.
    --Sabir Khan 6912 11/11/2009 remove IMBImage field...
    --[IMBImage]  [Image] ,
 issheriff INT,
 promotemplate [varchar] (100)            
   )            
            
 insert into @table17 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
   violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb,issheriff,promotemplate)              
 select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount,            
   violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 0, courtdate, Abb,issheriff,promotemplate from #temp15              
            
 if @printtype = 1            
 -- DOUBLE SIDE PRINT........            
  begin            
   insert into @table17 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb,issheriff,promotemplate)              
   select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount,            
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 1, courtdate, Abb,issheriff,promotemplate from #temp15 ;             
  end            
       
 if @mailbarry=0         
 begin        
 select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table17        
    UNION ALL 
    -- Rab Nawaz Khan 10187 04/12/2012 Address, City and Zip Code Changed for the Testing Advertisment mailer which send to Barry 
    select convert(varchar(20),@recCount) + ' letters sent' as letterid  , 0 as recordid , 'BARRY', 'BOBBIT', '7 Butterfly Place', 'Austin', 'TX', 0, 'N/A', 'DALLAS MUNICIPAL COURT', '0', '00' , 'N/A', '78738-1343' as zipcode, 0, '1/1/1900',@user,0,'0,35.00', '0'  
    --Yasir Kamal 7226 end
    --Yasir Kamal 6838 end.
      
 end      
 else        
    select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table17                
            
end            
  
-- HOUSTON ARR.  LETTERS   
-- Sabir Khan 7063 12/01/2009 Fixed arr new 1 bug.         
if @lettertype = 40            
begin            
 Select  convert(varchar(20),n.noteid) as letterid, t.recordid,   upper(t.FirstName) as firstname,                                          
 upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,''))  as  address, upper(t.city) as city,  s.state,
 -- Rab Nawaz Khan 9704 09/26/2011 Default fine amount 100 logic is commented. . .             
 -- case when isnull(v.fineamount,0) = 0 then 100 else v.fineamount end as FineAmount,
 isnull(v.fineamount,0) as FineAmount,violationdescription,c.CourtName, t.dpc,                                          
  t.dp2 as dptwo, TicketNumber_PK, t.zipcode,@user as Abb, v.courtdate,dbo.fn_Mailer_Get_IsConflictingCourtdate(t.recordid) as dateconflict, 0 as language, convert(varchar,t.recordid)+'0' as chkgroup  
into #letter40           
  from tblletternotes n , tblticketsarchive t, tblticketsviolationsarchive v, #letters tt, tblcourts c, tblcourtviolationstatus tcvs, tblstate s        
 where n.noteid=tt.noteid          
 and t.stateid_fk = s.stateid        
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.courtid = c.courtid            
 and tcvs.courtviolationstatusid = v.violationstatusid            
 and tcvs.CategoryID=2               
 and t.Flag1 in ('Y','D','S')            
 and t.donotmailflag = 0             
 and t.clientflag=0 
    --Rab Nawaz Khan 9119 04/04/2011 added attorney association logic.
and len(isnull(v.attorneyname, '')+isnull(v.barcardnumber,'')) = 0 
and CHARINDEX('KUBOSH',isnull(v.BondingCompanyName,''))=0          
--and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
 order by left(t.zipcode, 5), t.midnumber            
if @printtype = 1  
 insert into #letter40  
 select letterid, recordid, firstname, lastname, address, city, state, fineamount, violationdescription, courtname, dpc, dptwo,  
  ticketnumber_pk, zipcode, abb, courtdate, 1 as language, convert(varchar,recordid)+'1' as chkgroup  
 from #letter40  
  
ALTER TABLE #letter40 ADD isFineAmount BIT NOT NULL DEFAULT 1
SELECT REcordid INTO #HmcArr FROM #letter40 WHERE ISNULL(FineAmount,0) = 0

UPDATE T
SET
	isFineAmount = 0
FROM #letter40 t INNER JOIN #HmcArr f ON f.recordid = t.recordid

SELECT * FROM #letter40

DROP TABLE #HmcArr
DROP TABLE #letter40

end            
  
-- HMC WARRANTS NEW LETTERS  
-- Sabir Khan 6410 08/19/2009 Three new Warrant Letter added for HMC...          
if @lettertype in(41,42,43,65,66,67)             
begin            
 Select distinct convert(varchar(20),n.noteid) as letterid, t.recordid,  upper(t.FirstName) as firstname, charindex('FTA', ticketnumber_pk) as isFTA,                                             
    upper(t.LastName) as lastname, upper(t.address1 + isnull(t.address2,'')) as  address, upper(t.city) as city, s.state,   
	-- Rab Nawaz Khan 9704 09/26/2011 Default fine amount 100 logic is commented. . . 
    --case when isnull(v.fineamount,0)=0 then 100 else v.fineamount end as FineAmount,  
    isnull(v.fineamount,0) as FineAmount,v.violationnumber_pk, v.violationdescription,CourtName, t.dpc,   
    t.dp2 as dptwo, TicketNumber_PK, n.zipcode, left(n.zipcode,5) + ltrim(rtrim(n.midnumber)) as zipmid, 
    v.courtdate, t.courtid, n.midnumber, n.midnumber as midnum ,@user as Abb , v.ftalinkid                                            
    into #tmpFTANEw        
    from #warrants n , tblticketsarchive t, tblticketsviolationsarchive v, tblcourts c,  tblstate s           
    where  n.recordid = t.recordid            
    and t.recordid = v.recordid            
    and v.courtlocation = c.courtid    
    and t.stateid_fk = s.stateid        
    and  t.donotmailflag = 0            
    and  t.Flag1 in ('Y','D','S')              
    and charindex('FTA', v.causenumber) <> 0            
    and v.FTAlinkid is not null             
    and v.violationstatusid = 186 
 --and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
         
  insert into #tmpFTANEw        
    Select distinct a.letterid, t.recordid,  a.FirstName, charindex('FTA', v.ticketnumber_pk) as isFTA,                                             
    a.LastName, a.address, a.city, a.state, case when isnull(v.fineamount,0)=0 then 100 else v.fineamount end as FineAmount,v.violationnumber_pk, v.violationdescription,c.CourtName, a.dpc,                                            
    a.dptwo, v.TicketNumber_PK, a.zipcode, a.zipmid,             
    v.courtdate, t.courtid, a.midnumber, a.midnumber as midnum ,@user as Abb , v.ftalinkid                                            
    from #tmpFTANEw a , tblticketsarchive t, tblticketsviolationsarchive v, tblcourts c           
    where a.ftalinkid = v.ftalinkid          
    and t.recordid = v.recordid            
    and v.courtlocation = c.courtid            
    and charindex('FTA', v.causenumber) = 0                
    and v.violationstatusid = 186
       --Rab Nawaz Khan 9119 04/04/2011 added attorney association logic.
and len(isnull(v.attorneyname, '')+isnull(v.barcardnumber,'')) = 0 
and CHARINDEX('KUBOSH',isnull(v.BondingCompanyName,''))=0 

--Rab Nawaz Khan 9685 09/14/20211 Remove cases having attorney association...
select tva.recordid,tva.violationnumber_pk,tva.violationdescription into #tempAttorney1 
from #tmpFTANEw t inner join tblticketsviolationsarchive tva on tva.ftalinkid = t.ftalinkid 
where len(isnull(tva.attorneyname, '')+isnull(tva.barcardnumber,'')) <> 0

delete  f 
from #tmpFTANEw f
inner join #tempAttorney1 a on a.recordid = f.recordid and a.violationnumber_pk = f.violationnumber_pk
and a.violationdescription = f.violationdescription
           
 --and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
  
ALTER TABLE #tmpFTANEw ADD isFineAmount BIT NOT NULL DEFAULT 1
SELECT REcordid INTO #Hmc_WarrNum FROM #tmpFTANEw WHERE ISNULL(FineAmount,0) = 0

UPDATE T
SET
	isFineAmount = 0
FROM #tmpFTANEw t INNER JOIN #Hmc_WarrNum f ON f.recordid = t.recordid

--Sabir Khan 6503 09/01/2009 The warrant letter (180 - 270) has been changed into Warrant (180 - 220)      
select *, Case @lettertype   
     when 41 then 'HMC WARRANT (30-45)'  
     when 42 then 'HMC WARRANT (60-75)'  
     when 43 then 'HMC WARRANT (90-105)'
     WHEN 65 THEN 'HMC WARRANT (180-220)'
     WHEN 66 THEN 'HMC WARRANT (360-390)'
     WHEN 67 THEN 'HMC WARRANT (720-750)'  
    end as lettername  
  from #tmpFTANEw order by [zipmid] , [isFTA] desc   

DROP TABLE #Hmc_WarrNum
DROP TABLE #tmpFTANEw
drop table #warrants  
  
   
END             
  
-- HCJP  DAYB4 APP. LETTERS            
if @lettertype = 44            
begin            
 Select  convert(varchar(20),n.noteid) as letterid, t.recordid, upper( t.FirstName) as firstname,                                          
 upper(t.LastName) as lastname , upper(t.address1+ isnull(t.address2,'')) as address, upper(t.city) as city, s.state,             
 -- Rab Nawaz Khan 9704 09/26/2011 Default fine amount 100 logic is commented. . .  
 -- case when isnull(v.fineamount,0) = 0 then 100 else v.fineamount end as FineAmount, 
 isnull(v.fineamount,0) as FineAmount,violationdescription,c.CourtName, t.dpc,   
 t.dp2 as dptwo, case when len(isnull(v.causenumber,''))>0 then v.causenumber else TicketNumber_PK end as TicketNumber_PK, t.zipcode, t.midnumber,@user as Abb             
 INTO #lettertype from tblletternotes n , tblticketsarchive t, tblticketsviolationsarchive v, #letters tt, tblcourts c, tblcourtviolationstatus tcvs  , tblstate s          
 where n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.courtid = c.courtid            
 and t.stateid_fk = s.stateid        
 and tcvs.courtviolationstatusid = v.violationstatusid            
 and tcvs.CategoryID=2               
 and t.Flag1 in ('Y','D','S')            
 and t.donotmailflag = 0             
 and t.clientflag=0        
--and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
 order by left(t.zipcode, 5), t.midnumber            
 
ALTER TABLE #lettertype ADD isFineAmount BIT NOT NULL DEFAULT 1
SELECT REcordid INTO #HcjpDaybefore FROM #lettertype WHERE ISNULL(FineAmount,0) = 0

UPDATE T
SET
	isFineAmount = 0
FROM #lettertype t INNER JOIN #HcjpDaybefore f ON f.recordid = t.recordid

SELECT * FROM #lettertype

DROP TABLE #HcjpDaybefore
DROP TABLE #lettertype
 
end            
  
-- DALLAS COUNTY CRIMINAL SHERIFF LETTER......            
if @lettertype = 45  
BEGIN            
            
Select  convert(varchar(20),n.noteid) as letterid, t.recordid,   upper(t.FirstName) as firstname,   
 upper(t.LastName) as lastname, upper(t.address1+ isnull(' ' + t.address2,''))  as  address, upper(t.city) as city,  s.state,   
violationdescription , c.CourtName, t.dpc,   t.clientflag,                                        
 t.dp2 as dptwo, isnull(v.causenumber,v.ticketnumber_pk)  as ticketnumber_pk, t.zipcode ,v.courtdate ,@user as Abb,@lettertype as LetterType  
 into #temp45
  from  tblletternotes n , dallastraffictickets.dbo.tblticketsarchive t, dallastraffictickets.dbo.tblticketsviolationsarchive v,             
  #letters tt, dallastraffictickets.dbo.tblcourts c  , dallastraffictickets.dbo.tblstate s          
 where  n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and t.courtid = c.courtid            
 and len(isnull(t.lastname,'') + isnull(t.firstname,'') ) > 0             
 and t.Flag1 in ('Y','D','S')            
 and isnull(t.donotmailflag,0)   = 0        
--and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
 order by left(t.zipcode, 5), t.midnumber            
 
 select @recCount = count(distinct recordid) from #temp45              
 declare @table45 TABLE  (            
    [letterId] [varchar] (13)  ,          
    [recordid] [int] ,          
    [FirstName] [varchar] (20)  ,          
    [LastName] [varchar] (20)  ,    
	[clientflag] [tinyint],          
    [address] [varchar] (100) ,          
    [city] [varchar] (50) ,          
    [state] [varchar] (2)  ,          
    [violationdescription] [varchar] (200) ,          
    [CourtName] [varchar] (50) ,          
    [dpc] [varchar] (10)  ,          
    [dptwo] [varchar] (10)  ,          
    [TicketNumber_PK] [varchar] (20)  ,          
    [zipcode] [varchar] (15) ,          
    [language] [varchar] (10),           
    [courtdate] [datetime],          
    [Abb] [varchar] (10),
    [LetterType] [int]            
   )            
        
	insert into @table45 (letterid, recordid, firstname, lastname, clientflag, address, city, state,    
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb,LetterType)            
          
   select letterid, recordid, firstname, lastname, clientflag, address, city, state,    
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 0 , courtdate, Abb,LetterType from #temp45             
            
 if @printtype = 1            
 -- DOUBLE SIDE PRINT........            
  begin        
   insert into @table45 (letterid, recordid, firstname, lastname, clientflag, address, city, state,    
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb,LetterType)            
          
   select letterid, recordid, firstname, lastname, clientflag, address, city, state,    
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 1 , courtdate, Abb,LetterType from #temp45 ;     
  end            
     
	select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table45           
   order by [zipcode], [language]            
END   
-- DALLAS COUNTY CRIMINAL PASS TO HIRE......            
if @lettertype = 46  
BEGIN            
            
Select  convert(varchar(20),n.noteid) as letterid, t.recordid,   upper(t.FirstName) as firstname,   
 upper(t.LastName) as lastname, upper(t.address1+ isnull(' ' + t.address2,''))  as  address, upper(t.city) as city,  s.state,   
violationdescription , c.CourtName, t.dpc,   t.clientflag,                                       
 t.dp2 as dptwo, isnull(v.causenumber,v.ticketnumber_pk)  as ticketnumber_pk, t.zipcode ,v.courtdate ,@user as Abb  
 into #temp46
  from  tblletternotes n , dallastraffictickets.dbo.tblticketsarchive t, dallastraffictickets.dbo.tblticketsviolationsarchive v,               #letters tt, dallastraffictickets.dbo.tblcourts c  , dallastraffictickets.dbo.tblstate s          
 where  n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and t.courtid = c.courtid            
 and len(isnull(t.lastname,'') + isnull(t.firstname,'') ) > 0             
 and t.Flag1 in ('Y','D','S')            
 and isnull(t.donotmailflag,0)   = 0        
--and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
 order by left(t.zipcode, 5), t.midnumber            

select @recCount = count(distinct recordid) from #temp46              
 declare @table46 TABLE  (            
    [letterId] [varchar] (13)  ,          
    [recordid] [int] ,          
    [FirstName] [varchar] (20)  ,          
    [LastName] [varchar] (20)  ,    
	[clientflag] [tinyint],          
    [address] [varchar] (100) ,          
    [city] [varchar] (50) ,          
    [state] [varchar] (2)  ,          
    [violationdescription] [varchar] (200) ,          
    [CourtName] [varchar] (50) ,          
    [dpc] [varchar] (10)  ,          
    [dptwo] [varchar] (10)  ,          
    [TicketNumber_PK] [varchar] (20)  ,          
    [zipcode] [varchar] (15) ,          
    [language] [int],           
    [courtdate] [datetime],          
    [Abb] [varchar] (10)            
   )            
        
	insert into @table46 (letterid, recordid, firstname, lastname, clientflag, address, city, state,    
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb)            
          
   select letterid, recordid, firstname, lastname, clientflag, address, city, state,    
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 0 , courtdate, Abb from #temp46             
            
 if @printtype = 1            
 -- DOUBLE SIDE PRINT........            
  begin        
   insert into @table46 (letterid, recordid, firstname, lastname, clientflag, address, city, state,    
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb)            
          
   select letterid, recordid, firstname, lastname, clientflag, address, city, state,    
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 1 , courtdate, Abb from #temp46 ;     
  end            
     
	select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table46           
   order by [zipcode], [language]             
                  
END      
  
-- HARRIS BUSINESS LETTER......            
if @lettertype = 47            
BEGIN            
 Select distinct convert(varchar(20),n.noteid) as letterid, t.businessid, 
 -- Rab Nawaz 9921 01/05/2011 Rab Nawaz 9921 01/05/2011 Fix the (No Letter Found for selected Batch) issues in track batches 
 CASE  WHEN LTRIM(RTRIM(businessName)) = '' THEN 'BUSINESS STARTUP' ELSE businessName END as businessName,
 upper(v.ownerName) as ownername, upper(t.address) as  address, upper(t.city) as city, upper(t.state) as state,             
 t.dpc, t.dp2 , t.zip ,@user as Abb, upper(v.OwnerAddress) as OwnerAddress, v.dp2 as Ownerdptwo, t.FileNumber, t.err_stat,
 v.OwnerCity, v.OwnerState, v.OwnerZip, v.DPC AS ownerdpc, 0 as ISAddressNull, 0 as IsOwnerAddress
 from tblletternotes n , assumednames.dbo.business t, assumednames.dbo.businessowner v, #letters tt   
 where  n.noteid=tt.noteid             
 and n.recordid = t.businessid            
 and t.businessid = v.businessid  
 -- and v.ownernumber = 1 -- Rab Nawaz 9921 01/05/2011 Fix the (No Letter Found for selected Batch) issues in track batches 
 and t.Flags in ('Y','D','S') 
 and v.Flags in ('Y','D','S') -- Sabir Khan 9948 12/28/2011 Fixed barcode issue.          
 and t.nomailflag = 0             
--and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
 order by t.zip  
end  
  
  
-- HOUSTON  DAY OF ARRAIGNMENT LETTERS            
if @lettertype = 48  
begin            
 Select  convert(varchar(20),n.noteid) as letterid, t.recordid, upper( t.FirstName) as firstname,                                          
  upper(t.LastName) as lastname , upper(t.address1+ isnull(t.address2,'')) as address, upper(t.city) as city, s.state,             
 case when isnull(v.fineamount,0) = 0 then 100 else v.fineamount end as FineAmount,violationdescription,c.CourtName, t.dpc,                                          
  t.dp2 as dptwo, TicketNumber_PK, t.zipcode,@user as Abb, convert(varchar(10),n.listdate, 101) as courtdate, 0 as language, '0' as chkgroup  
  into #letter48  
  from tblletternotes n , tblticketsarchive t, tblticketsviolationsarchive v, #letters tt, tblcourts c, tblcourtviolationstatus tcvs  , tblstate s          
 where n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.courtid = c.courtid            
 and t.stateid_fk = s.stateid        
 and tcvs.courtviolationstatusid = v.violationstatusid            
 and tcvs.CategoryID=2               
 and t.Flag1 in ('Y','D','S')            
 and t.donotmailflag = 0             
 and t.clientflag=0        
--and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
 order by left(t.zipcode, 5), t.midnumber            
   
 if @printtype = 1   
 begin  
 insert into #letter48  
 select letterid, recordid, firstname, lastname, address, city, state, fineamount, violationdescription, courtname, dpc,  
 dptwo, ticketnumber_pk, zipcode, abb, courtdate, 1 , '1'  
 from #letter48  
 end  
   
 select * from #letter48  
end            
  
  
-- IRVING APPEARANCE LETTER            
if @lettertype = 49            
begin            
 Select  convert(varchar(20),n.noteid) as letterid, t.recordid,  upper(t.FirstName) as firstname,                                          
 upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,''))  as  address, upper(t.city) as city,  s.state,   
case when isnull(v.fineamount,0)=0 then 100 else v.fineamount end as FineAmount,violationdescription, c.CourtName, t.dpc,
--Yasir Kamal 7226  01/21/2010 get promotional price                                        
 t.dp2 as dptwo, TicketNumber_PK, t.zipcode , dateadd(day, 21, t.violationdate) as courtdate ,@user as Abb,dbo.GetPromoPriceTemplate(T.zipcode,@lettertype) AS promotemplate                                              
 into #temp49            
 from  tblletternotes n , dallastraffictickets.dbo.tblticketsarchive t, dallastraffictickets.dbo.tblticketsviolationsarchive v,             
  #letters tt, dallastraffictickets.dbo.tblcourts c    , dallastraffictickets.dbo.tblstate s        
 where  n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and t.courtid = c.courtid            
 and t.lastname is not null              
 and t.firstname is not null            
 and t.Flag1 in ('Y','D','S')            
 and isnull(t.donotmailflag,0)   = 0             
 and v.violationbonddate is null     
--and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
 order by left(t.zipcode, 5), t.midnumber            
            
            
 select @recCount = count(distinct recordid) from #temp49              
 declare @table49 TABLE  (            
    [letterId] [varchar] (30)   ,            
    [recordid] [int] ,            
    [FirstName] [varchar] (20)  ,            
    [LastName] [varchar] (20)  ,            
    [address] [varchar] (100) ,            
    [city] [varchar] (50) ,            
    [state] [varchar] (2)  ,            
    [FineAmount] [money]  ,            
    [violationdescription] [varchar] (200) ,            
    [CourtName] [varchar] (50) ,            
    [dpc] [varchar] (10)  ,            
    [dptwo] [varchar] (10)  ,            
    [TicketNumber_PK] [varchar] (20)  ,            
    [zipcode] [varchar] (15) ,            
    [language] [int],            
    [courtdate] [datetime],            
    [Abb] [varchar] (10),
    promotemplate [varchar] (100)            
   )            
            
 insert into @table49 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
   violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb,promotemplate)              
 select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount,            
   violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 0, courtdate, Abb,promotemplate from #temp49              
            
 if @printtype = 1            
 -- DOUBLE SIDE PRINT........            
  begin            
   insert into @table49 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb,promotemplate)              
   select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount,            
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 1, courtdate, Abb,promotemplate from #temp49 ;             
  end            
         
 if @mailbarry=0          
 begin      
 select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table49             
 union  
 -- Rab Nawaz Khan 10187 04/12/2012 Address, City and Zip Code Changed for the Testing Advertisment mailer which send to Barry     
  select convert(varchar(20),@recCount) + ' letters sent' as letterid  , 0 as recordid , 'BARRY', 'BOBBIT', '7 Butterfly Place', 'Austin', 'TX', 0, 'N/A', 'DALLAS MUNICIPAL COURT', '0', '00' , 'N/A', '78738-1343' as zipcode, 0, '1/1/1900',@user ,'0,35.00', '0'   
 
     
      
 end      
 else      
 select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table49             
         
             
            
end            
            
  
-- IRVING APPEARANCE DAY OF LETTER            
if @lettertype = 50            
begin            
 Select  convert(varchar(20),n.noteid) as letterid, t.recordid,   upper(t.FirstName) as firstname,                                          
 upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,''))  as  address, upper(t.city) as city,  s.state,   
case when isnull(v.fineamount,0)=0 then 100 else v.fineamount end as FineAmount,violationdescription, c.CourtName, t.dpc,                                          
 t.dp2 as dptwo, TicketNumber_PK, t.zipcode , dateadd(day, 21, t.violationdate) as courtdate ,@user as Abb,dbo.GetPromoPriceTemplate(T.zipcode,@lettertype) AS promotemplate                                               
 into #temp50            
 from  tblletternotes n , dallastraffictickets.dbo.tblticketsarchive t, dallastraffictickets.dbo.tblticketsviolationsarchive v,             
  #letters tt, dallastraffictickets.dbo.tblcourts c, dallastraffictickets.dbo.tblstate s            
 where  n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and t.courtid = c.courtid            
 and t.lastname is not null              
 and t.firstname is not null            
 and t.Flag1 in ('Y','D','S')            
 and isnull(t.donotmailflag,0)   = 0             
and v.violationbonddate is null         
--and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
 order by left(t.zipcode, 5), t.midnumber            
            
            
 select @recCount = count(distinct recordid) from #temp50              
 declare @table50 TABLE  (            
    [letterId] [varchar] (30)   ,            
    [recordid] [int] ,            
    [FirstName] [varchar] (20)  ,            
    [LastName] [varchar] (20)  ,            
    [address] [varchar] (100) ,            
    [city] [varchar] (50) ,            
    [state] [varchar] (2)  ,            
    [FineAmount] [money]  ,            
    [violationdescription] [varchar] (200) ,            
    [CourtName] [varchar] (50) ,            
    [dpc] [varchar] (10)  ,            
    [dptwo] [varchar] (10)  ,            
    [TicketNumber_PK] [varchar] (20)  ,            
    [zipcode] [varchar] (15) ,            
    [language] [int],            
    [courtdate] [datetime],            
    [Abb] [varchar] (10),
    promotemplate [varchar] (100)              
   )            
            
 insert into @table50 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
   violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb,promotemplate)              
 select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount+60,            
   violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 0, courtdate, Abb,promotemplate from #temp50              
            
 if @printtype = 1            
 -- DOUBLE SIDE PRINT........            
  begin            
   insert into @table50 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb,promotemplate)              
   select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount+60,            
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 1, courtdate, Abb,promotemplate from #temp50 ;            
  end            
       
 if @mailbarry=0        
 begin             
 select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table50       
 union 
 -- Rab Nawaz Khan 10187 04/12/2012 Address, City and Zip Code Changed for the Testing Advertisment mailer which send to Barry 
 select convert(varchar(20),@recCount) + ' letters sent' as letterid  , 0 as recordid , 'BARRY', 'BOBBIT', '7 Butterfly Place', 'Austin', 'TX', 0, 'N/A', 'DALLAS MUNICIPAL COURT', '0', '00' , 'N/A', '78738-1343' as zipcode, 0, '1/1/1900', @user ,'0,35.00', '0'  
   
    
            
 end      
 else      
 select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table50             
         
  
end  
  
  
-- IRVING WARRANT LETTER         
if @lettertype = 51          
begin            
 Select  convert(varchar(20),tt.noteid) as letterid, t.recordid, upper( t.FirstName) as firstname,                                          
  upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,'')) as address, upper(t.city) as city, s.state,       
   case when isnull(v.bondamount,0)=0 then 100 else v.bondamount end as FineAmount,  
 violationdescription, c.CourtName, t.dpc,                                          
 t.dp2 as dptwo, TicketNumber_PK, t.zipcode,@user as Abb,dbo.GetPromoPriceTemplate(T.zipcode,@lettertype) AS promotemplate                          
 into #temp51            
-- from  tblletternotes n , dallastraffictickets.dbo.tblticketsarchive t, dallastraffictickets.dbo.tblticketsviolationsarchive v,             
 from   dallastraffictickets.dbo.tblticketsarchive t, dallastraffictickets.dbo.tblticketsviolationsarchive v,             
  #letters tt, dallastraffictickets.dbo.tblcourts c  , dallastraffictickets.dbo.tblstate s          
 where  tt.recordid = t.recordid            
 --and n.noteid=tt.noteid              
      
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and t.courtid = c.courtid     
 and v.violationbonddate is not null       
 and t.Flag1 in ('Y','D','S')            
 and t.donotmailflag = 0             
 and len(isnull(t.firstname,'')+isnull(t.lastname,''))>0   
--and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
 order by left(t.zipcode, 5), t.midnumber            
            
            
 select @recCount = count(distinct recordid) from #temp51              
 declare @table51 TABLE  (            
    [letterId] [varchar] (30)   ,            
    [recordid] [int] ,            
    [FirstName] [varchar] (20)  ,            
    [LastName] [varchar] (20)  ,            
    [address] [varchar] (100) ,            
    [city] [varchar] (50) ,            
    [state] [varchar] (2)  ,            
    [FineAmount] [money]  ,            
    [violationdescription] [varchar] (200) ,            
    [CourtName] [varchar] (50) ,            
    [dpc] [varchar] (10)  ,            
    [dptwo] [varchar] (10)  ,            
    [TicketNumber_PK] [varchar] (20)  ,            
    [zipcode] [varchar] (15) ,            
    [language] [int],            
    [Abb] [varchar] (10),
    promotemplate [varchar] (100)             
   )            
     
 insert into @table51 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
 violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, Abb,promotemplate)              
 select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount,            
 violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 0, Abb,promotemplate from #temp51              
         
 if @printtype = 1            
 -- DOUBLE SIDE PRINT........            
 begin            
  insert into @table51 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
  violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, Abb,promotemplate)              
  select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount,            
  violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 1, Abb,promotemplate from #temp51 ;             
 end         
   
        
 if @mailbarry=0         
 begin      
 select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table51           
    --order by left(t.zipcode, 5), t.midnumber               
 union 
 -- Rab Nawaz Khan 10187 04/12/2012 Address, City and Zip Code Changed for the Testing Advertisment mailer which send to Barry      
 select convert(varchar(20),@recCount) + ' letters sent' as letterid  , 0 as recordid , 'BARRY', 'BOBBIT', '7 Butterfly Place', 'Austin', 'TX', 0, 'N/A', 'DALLAS MUNICIPAL COURT', '0', '00' , 'N/A', '78738-1343' as zipcode, 0,@user,'0,35.00', '0'      
 end      
 else      
    select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table51           
    --order by left(t.zipcode, 5), t.midnumber                
--7226  end                                                     
            
end            
         
         
-- HCJP  JUDGE LETTERS            
if @lettertype = 52
begin            
 Select  convert(varchar(20),n.noteid) as letterid, t.recordid, upper( t.FirstName) as firstname,                                          
  upper(t.LastName) as lastname , upper(t.address1+ isnull(t.address2,'')) as address, upper(t.city) as city, s.state,             
 -- Rab Nawaz Khan 9704 09/26/2011 Default fine amount 100 logic is commented. . .  
 -- case when isnull(v.fineamount,0) = 0 then 100 else v.fineamount end as FineAmount, 
 isnull(v.fineamount,0)  as FineAmount,violationdescription,c.CourtName, t.dpc,                                          
 t.dp2 as dptwo, case when len(isnull(v.causenumber,''))>0 then v.causenumber else TicketNumber_PK end as TicketNumber_PK, t.zipcode, t.midnumber,@user as Abb             
 INTO #letter52 from tblletternotes n , tblticketsarchive t, tblticketsviolationsarchive v, #letters tt, tblcourts c, tblcourtviolationstatus tcvs  , tblstate s          
 where n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.courtid = c.courtid            
 and t.stateid_fk = s.stateid        
 and tcvs.courtviolationstatusid = v.violationstatusid            
 and tcvs.CategoryID= 5
 and t.Flag1 in ('Y','D','S')            
 and t.donotmailflag = 0             
 and t.clientflag=0        
--and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
 order by left(t.zipcode, 5), t.midnumber   
 
 ALTER TABLE #letter52 ADD isFineAmount BIT NOT NULL DEFAULT 1
SELECT REcordid INTO #Hcjp_judge FROM #letter52 WHERE ISNULL(FineAmount,0) = 0

UPDATE T
SET
	isFineAmount = 0
FROM #letter52 t INNER JOIN #Hcjp_judge f ON f.recordid = t.recordid

SELECT * FROM #letter52

DROP TABLE #Hcjp_judge
DROP TABLE #letter52
          
end            
         
             
            
-- HARRIS DLQ LETTER            
if @lettertype = 53
begin            
 Select  convert(varchar(20),n.noteid) as letterid, t.recordid,  upper( t.FirstName) as firstname,                                          
  upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,'')) as address, upper(t.city) as city, s.state,       
 case when isnull(v.fineamount,0)=0 then 100 else v.fineamount end as FineAmount,violationdescription,c.CourtName, t.dpc,                                          
  t.dp2 as dptwo, case when len(isnull(v.causenumber,''))>0 then v.causenumber else TicketNumber_PK end as TicketNumber_PK, t.zipcode , left(t.zipcode, 5) + ltrim(rtrim(t.midnumber)) as zipmid, t.violationdate as courtdate, t.courtid,@user as Abb
  into #letter53
  from tblletternotes n , tblticketsarchive t, tblticketsviolationsarchive v, #letters tt, tblcourts c , tblstate s           
 where  n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.courtid = c.courtid            
 and t.stateid_fk = s.stateid        
 and t.Flag1 in ('Y','D','S')            
 and t.donotmailflag = 0             
 and t.clientflag = 0            
--and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
order by left(t.zipcode, 5), t.midnumber            
 
 if @printtype = 0
	begin
		select *,  0 as language , convert(varchar(15), recordid) + '0' as ChkGroup  from #letter53
	end
 else
	begin
		select *,  0 as language , convert(varchar(15), recordid) + '0' as ChkGroup  from #letter53
		union all
		select *,  1 as language , convert(varchar(15), recordid) + '1' as ChkGroup  from #letter53
	end
 
	
end       
 ---HCJP Warrant Letters---------------------------------------------------------------------------------
 ---Sabir Khan 8971 03/18/2011 HCJP Warrant (30-90) and HCJP Warrant (90-180)
if @lettertype IN (90,91)
begin            
 Select  convert(varchar(20),n.noteid) as letterid, t.recordid,  upper( t.FirstName) as firstname,                                          
  upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,'')) as address, upper(t.city) as city, s.state,       
case when isnull(v.fineamount,0)=0 then 100 else v.fineamount end as FineAmount,violationdescription,c.CourtName, t.dpc,                                          
  t.dp2 as dptwo, case when len(isnull(v.causenumber,''))>0 then v.causenumber else TicketNumber_PK end as TicketNumber_PK, t.zipcode , left(t.zipcode, 5) + ltrim(rtrim(t.midnumber)) as zipmid, t.violationdate as courtdate, t.courtid,@user as Abb,
  CASE @lettertype WHEN 90 THEN ' HCJP Warrant (30-90) ' ELSE ' HCJP Warrant (90-180) ' END AS LetterName  
  into #letterHCJPWarrant
  from tblletternotes n , tblticketsarchive t, tblticketsviolationsarchive v, #letters tt, tblcourts c , tblstate s           
 where  n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.courtid = c.courtid            
 and t.stateid_fk = s.stateid        
 and t.Flag1 in ('Y','D','S')            
 and t.donotmailflag = 0               
 order by left(t.zipcode, 5), t.midnumber            
 
 if @printtype = 0
	begin
		select *,  0 as language , convert(varchar(15), recordid) + '0' as ChkGroup  from #letterHCJPWarrant
	end
 else
	begin
		select *,  0 as language , convert(varchar(15), recordid) + '0' as ChkGroup  from #letterHCJPWarrant
		union all
		select *,  1 as language , convert(varchar(15), recordid) + '1' as ChkGroup  from #letterHCJPWarrant
	end

DROP TABLE #letterHCJPWarrant
 
end 
--------------------------------------------------------------------------------------------------------        
-- HARRIS CIVIL CHILD CUSTODY LETTER
if @lettertype = 54
begin            
 Select  convert(varchar(20),n.noteid) as letterid, 
		ccp.ChildCustodyPartyId as recordid, ccc.ConnectionName, c.CaseNumber, cst.SettingTypeName, c.SettingDate, 
		isnull(ccp.firstname,'') + ' ' + isnull(ccp.middlename,'') + ' '+  ccp.lastname as Name, ccp.Address, ccp.AddressDate, CCP.City, tblState.State, ccp.ZipCode, ccp.DP2 as dptwo, 
		ccp.DPC, 3071 as courtid, isnull(ccp.flag1,'N') as flag1, @user as Abb
		--Yasir Kamal 6648 10/07/2009 display IMB Barcode
		--Sabir Khan 6912 11/06/2009 IMBImage column has been removed as using new solution for IMB...
		--master.dbo.fn_getIMBImage('00040001120' + RIGHT(REPLICATE('0',9) + CAST(n.noteid AS VARCHAR(9)),9)   ,replace(n.zipcode, '-','')+isnull(n.dp2,'')) as IMBImage             
FROM	civil.dbo.ChildCustody AS c 
INNER JOIN
		civil.dbo.ChildCustodySettingType AS cst 
ON		c.ChildCustodySettingTypeId = cst.ChildCustodySettingTypeId 
INNER JOIN
		civil.dbo.ChildCustodyParties AS ccp 
ON		c.ChildCustodyId = ccp.ChildCustodyId 
INNER JOIN
		civil.dbo.ChildCustodyConnection ccc
ON		ccp.ChildCustodyConnectionID = ccc.ChildCustodyConnectionID 
INNER JOIN
        tblState ON ccp.StateId = tblState.StateID
inner join tblletternotes n 
		on ccp.childcustodypartyid = n.recordid
inner join #letters l
	on l.noteid = n.noteid
WHERE   ccp.flag1 in ('y','d','s')  
       
end  

-- HARRIS COUNTY CRIMINAL NEW LETTER......            
if @lettertype = 55
BEGIN            
 --ozair 4342 07/03/2008 trim method applied in first and last name, causenumber is fetched instead of ticketnumber
 Select distinct convert(varchar(20),n.noteid) as letterid, upper(ltrim(rtrim(c.firstname))+ ' ' +ltrim(rtrim(c.lastname))) as name, 
 upper(c.address1 + isnull(' ' + c.address2,'')) as address, upper(c.city) as city, s.state, C.zipcode as zip , N.recordid, c.midnumber as span, 
 l.violationdescription as chargewording,  l.ticketviolationdate as bookingdate, l.causenumber as casenumber, c.clientflag, 
 --Farrukh 10438 12/05/2012 Replace sort order field midnumber with recordid
 --left(c.zipcode,5) + ltrim(rtrim(c.midnumber)) as zipspan,
 left(c.zipcode,5) + ltrim(rtrim(c.RecordID)) as zipspan,
  n.listdate,            
 --Farrukh 10438 11/15/2012 Added New Field lettertype
 c.dp2 as dptwo, c.dpc,@user as Abb , '55' as lettertype, ISNULL(l.ChargeLevel, 0) AS ChargeLevel -- Rab Nawaz Khan 10349 09/05/2013 Added Charge Level
 -- Tahir 5982 06/05/2009 IMB Image Impplemented
  --Sabir Khan 6912 11/11/2009 remove IMBImage field...
--master.dbo.fn_getIMBImage('00040001120' + RIGHT(REPLICATE('0',9) + CAST(n.noteid AS VARCHAR(9)),9)   ,replace(n.zipcode, '-','')+isnull(n.dp2,'')) as IMBImage
 INTO #temp55          
 from dbo.tblticketsarchive c   
 inner join  dbo.tblticketsviolationsarchive l on c.recordid = l.recordid
 inner join #letters tt on tt.recordid = c.recordid   
 inner join  tblletternotes n on n.noteid=tt.noteid    
 inner join dbo.tblstate s on s.stateid = c.stateid_fk
 where c.Flag1 in ('Y','D','S') 
 and l.violationstatusid <> 80 --Abbas Qamar 9991 01/17/2012 Exclude dispose cases.
--and (c.ncoa18flag = 1 or c.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    

-- Rab Nawaz Khan 10349 09/06/2013 Added Max Fine Amount and Max Jail Time Columns values. . . 
 ALTER TABLE #temp55 
 ADD isMaxFineAmountNotfound BIT NOT NULL DEFAULT 1, HcccMailerVioDesc Varchar(500), MaxFineAmount Money, MaxJailTimePeriod Varchar(100)
 
  -- Rab Nawaz Khan  11459 09/24/2013 Setting the default values to null for the mailer violations . . . 
 UPDATE #temp55 SET HcccMailerVioDesc = ''
 
 --Updating the matched Records on Charge level basis from HCCC table
 Update t
 SET t.HcccMailerVioDesc = UPPER(hc.MailerViolationDescription), t.MaxFineAmount = hc.Maxfine, t.MaxJailTimePeriod = UPPER(hc.MaxJail), isMaxFineAmountNotfound = 0
 FROM #temp55 t INNER JOIN tblHCCCViolationsInfo hc on REPLACE(t.chargewording, ' ', '') = REPLACE(hc.ViolationDescription, ' ', '') 
 WHERE t.ChargeLevel = hc.ChargeLevel
 
 -- Rab Nawaz Khan  11459 09/24/2013 Updating the matched Records on violation description basis from HCCC table
 Update t
 SET t.HcccMailerVioDesc = UPPER(hc.MailerViolationDescription)
 FROM #temp55 t INNER JOIN tblHCCCViolationsInfo hc on REPLACE(t.chargewording, ' ', '') = REPLACE(hc.ViolationDescription, ' ', '') 
 
 -- Getting the the Record if multiplie violations and any of the violation not found in HCCC table
 SELECT tt.Recordid INTO #tempFineAmount FROM #temp55 tt
 WHERE tt.Recordid IN (SELECT t.Recordid FROM #temp55 t WHERE t.isMaxFineAmountNotfound = 0)
 AND tt.isMaxFineAmountNotfound = 1
 
 -- Updating the Records
 UPDATE #temp55
 SET isMaxFineAmountNotfound = 1
 WHERE Recordid in (SELECT t.Recordid FROM #tempFineAmount t)
 -- END 10349

SELECT * FROM #temp55 ORDER BY ZIPSPAN 

DROP TABLE #temp55
DROP TABLE #tempFineAmount
-- END 10349
END  

-- HARRIS COUNTY CRIMINAL Booking In,Reset, Week Before LETTER......            
if @lettertype IN (136, 137, 138) --Farrukh 10438 11/23/2012 New Check Added For HCCC Booking In,Reset, Week Before Letter
BEGIN             
 Select distinct convert(varchar(20),n.noteid) as letterid, upper(ltrim(rtrim(c.firstname))+ ' ' +ltrim(rtrim(c.lastname))) as name, 
 upper(c.address1 + isnull(' ' + c.address2,'')) as address, upper(c.city) as city, s.state, C.zipcode as zip , N.recordid, c.midnumber as span, 
 l.violationdescription as chargewording,  l.ticketviolationdate as bookingdate, l.causenumber as casenumber, c.clientflag, 
 --Farrukh 10438 12/05/2012 Replace sort order field midnumber with recordid
-- left(c.zipcode,5) + ltrim(rtrim(c.midnumber)) as zipspan, 
 left(c.zipcode,5) + ltrim(rtrim(c.RecordID)) as zipspan,
 n.listdate,             
 c.dp2 as dptwo, c.dpc,@user as Abb , CONVERT(VARCHAR(20),@lettertype) as lettertype, ISNULL(l.ChargeLevel, 0) AS ChargeLevel   
 INTO #temp136
 from dbo.tblticketsarchive c   
 inner join  dbo.tblticketsviolationsarchive l on c.recordid = l.recordid
 inner join #letters tt on tt.recordid = c.recordid   
 inner join  tblletternotes n on n.noteid=tt.noteid    
 inner join dbo.tblstate s on s.stateid = c.stateid_fk
 where c.Flag1 in ('Y','D','S') 
 and l.violationstatusid <> 80 

-- Rab Nawaz Khan 10349 09/06/2013 Added Max Fine Amount and Max Jail Time Columns values. . . 
 ALTER TABLE #temp136 
 ADD isMaxFineAmountNotfound BIT NOT NULL DEFAULT 1, HcccMailerVioDesc Varchar(500), MaxFineAmount Money, MaxJailTimePeriod Varchar(100)
 
 -- Rab Nawaz Khan  11459 09/24/2013 Setting the default values to null for the mailer violations . . . 
 UPDATE #temp136 SET HcccMailerVioDesc = ''
 --Updating the matched Records on Charge level basis from HCCC table
 Update t
 SET t.HcccMailerVioDesc = UPPER(hc.MailerViolationDescription), t.MaxFineAmount = hc.Maxfine, t.MaxJailTimePeriod = UPPER(hc.MaxJail), isMaxFineAmountNotfound = 0
 FROM #temp136 t INNER JOIN tblHCCCViolationsInfo hc on REPLACE(t.chargewording, ' ', '') = REPLACE(hc.ViolationDescription, ' ', '') 
 WHERE t.ChargeLevel = hc.ChargeLevel
 
 -- Rab Nawaz Khan  11459 09/24/2013 Updating the matched Records on violation description basis from HCCC table
 Update t
 SET t.HcccMailerVioDesc = UPPER(hc.MailerViolationDescription)
 FROM #temp136 t INNER JOIN tblHCCCViolationsInfo hc on REPLACE(t.chargewording, ' ', '') = REPLACE(hc.ViolationDescription, ' ', '') 
 
 
 -- Getting the the Record if multiplie violations and any of the violation not found in HCCC table
 SELECT tt.Recordid INTO #tempHccFineAmount FROM #temp136 tt
 WHERE tt.Recordid IN (SELECT t.Recordid FROM #temp136 t WHERE t.isMaxFineAmountNotfound = 0)
 AND tt.isMaxFineAmountNotfound = 1
 
 -- Updating the Records
 UPDATE #temp136
 SET isMaxFineAmountNotfound = 1
 WHERE Recordid in (SELECT t.Recordid FROM #tempHccFineAmount t)
 -- END 10349

SELECT * FROM #temp136 ORDER BY zipspan 

DROP TABLE #temp136
DROP TABLE #tempHccFineAmount
-- END 10349            
            
END            

 
 -- PASADENA WARRANT LETTER            
if @lettertype in (56)            
begin            
 Select  convert(varchar(20),n.noteid) as letterid, t.recordid, upper( t.FirstName) as firstname,                                          
  upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,'')) as address, upper(t.city) as city, s.state,       
case when isnull(v.fineamount,0)=0 then 100 else v.fineamount end as FineAmount,violationdescription,c.CourtName, t.dpc,                                          
  t.dp2 as dptwo, TicketNumber_PK, t.zipcode , v.ticketviolationdate as courtdate,  @user as Abb            
  from tblletternotes n , tblticketsarchive t, tblticketsviolationsarchive v, #letters tt, tblcourts c    , tblstate s        
 where n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and v.courtlocation = c.courtid            
 and t.Flag1 in ('Y','D','S')            
 and t.donotmailflag = 0             
 and t.clientflag = 0 
 and v.violationstatusid <> 80 --Abbas Qamar 9991 01/17/2012 Exclude dispose cases.           
--and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
 order by left(t.zipcode, 5), t.midnumber            
end            
                      
-- HARRIS CIVIL CHILD CUSTODY DAY BEFORE LETTER
if @lettertype = 57
begin            
	 Select  convert(varchar(20),n.noteid) as letterid, 
			ccp.ChildCustodyPartyId as recordid, ccc.ConnectionName, c.CaseNumber, cst.SettingTypeName, c.SettingDate, 
			isnull(ccp.firstname,'') + ' ' + isnull(ccp.middlename,'') + ' '+  ccp.lastname as Name, ccp.Address, ccp.AddressDate, CCP.City, tblState.State, ccp.ZipCode, ccp.DP2 as dptwo, 
			ccp.DPC, 3071 as courtid, isnull(ccp.flag1,'N') as flag1, @user as Abb
			--Yasir Kamal 6648 10/07/2009 display IMB Barcode
			--Sabir Khan 6912 11/06/2009 IMBImage column has been removed as using new solution for IMB...
			--master.dbo.fn_getIMBImage('00040001120' + RIGHT(REPLICATE('0',9) + CAST(n.noteid AS VARCHAR(9)),9)   ,replace(n.zipcode, '-','')+isnull(n.dp2,'')) as IMBImage             
	FROM	civil.dbo.ChildCustody AS c 
	INNER JOIN
			civil.dbo.ChildCustodySettingType AS cst 
	ON		c.ChildCustodySettingTypeId = cst.ChildCustodySettingTypeId 
	INNER JOIN
			civil.dbo.ChildCustodyParties AS ccp 
	ON		c.ChildCustodyId = ccp.ChildCustodyId 
	INNER JOIN
			civil.dbo.ChildCustodyConnection ccc
	ON		ccp.ChildCustodyConnectionID = ccc.ChildCustodyConnectionID 
	INNER JOIN
			tblState ON ccp.StateId = tblState.StateID
	inner join tblletternotes n 
			on ccp.childcustodypartyid = n.recordid
	inner join #letters l
		on l.noteid = n.noteid
	WHERE   ccp.flag1 in ('y','d','s')  
END 


-- HOUSTON JURY TRIAL LETTERS            
-- TAHIR 4514 08/04/2008
if @lettertype =58            
begin            
 Select distinct convert(varchar(20),n.noteid) as letterid, t.recordid,  upper( t.FirstName) as firstname,                                          
  upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,'')) as address, upper(t.city) as city, s.state,        
 -- Rab Nawaz Khan 9704 09/26/2011 Default fine amount 100 logic is commented. . .
 -- case when isnull(v.fineamount,0) = 0 then 100 else v.fineamount end as FineAmount,
 isnull(v.fineamount,0) as FineAmount, violationdescription,c.CourtName, t.dpc,                                          
  t.dp2 as dptwo, TicketNumber_PK, t.zipcode, t.midnumber , t.midnumber as midnum, v.courtlocation as courtid, 
  -- Rab Nawaz Khan 9682 10/14/2011 Columns added for the global templates for HMC mailers              
    left(n.zipcode,5)+ t.midnumber as zipmid, v.courtdate,@user as Abb ,left(t.zipcode, 5) AS forOrderBy, 0 AS promotemplate , 'HMC JURY TRAIL' AS lettername,ISNULL(n.isPromo,0) AS isPromo, '' as PromoMessage
  INTO #letter58 from tblletternotes n , tblticketsarchive t, tblticketsviolationsarchive v, #letters tt, tblcourts c, tblletternotes_detail d, tblcourtviolationstatus tcvs    , tblstate s        
 where n.noteid=tt.noteid              
 and n.noteid = d.noteid            
 and d.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and v.courtlocation = c.courtid            
 and tcvs.courtviolationstatusid = v.violationstatusid            
 and tcvs.CategoryID=4             
 and t.Flag1 in ('Y','D','S')            
 and t.donotmailflag = 0             
 and t.clientflag=0 
--and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
 and len(isnull(t.firstname,'')+isnull(t.lastname,''))>0   
-- Rab Nawaz Khan 9690 10/04/2011 Do not send mailer to the person who has an attorney assigned to "any" companion case
AND LEN(isnull(v.attorneyname, '')+isnull(v.barcardnumber,'')) = 0         
order by left(t.zipcode, 5), t.midnumber 
 
 ALTER TABLE #letter58 ADD isFineAmount BIT NOT NULL DEFAULT 1
SELECT REcordid INTO #hmcJury FROM #letter58 WHERE ISNULL(FineAmount,0) = 0

UPDATE T
SET
	isFineAmount = 0
FROM #letter58 t INNER JOIN #hmcJury f ON f.recordid = t.recordid

SELECT * FROM #letter58

DROP TABLE #hmcJury
DROP TABLE #letter58
            
end            

-- tahir 4961 10/22/08 
-- DMC Judge Week Before LETTER     
-- tahir 5052 11/03/2008 added DMC Judge Regular letter...     
--Yasir Kamal 6648 10/07/2009 display IMB Barcode 
----Sabir Khan 6912 11/11/2009 remove IMBImage field... 
if @lettertype = 59
begin            
 Select  convert(varchar(20),n.noteid) as letterid, t.recordid,   upper(t.FirstName) as firstname,                                          
 upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,''))  as  address, upper(t.city) as city,  s.state,   
case when isnull(v.fineamount,0)=0 then 100 else v.fineamount end as FineAmount,violationdescription, c.CourtName, t.dpc,
--Yasir Kamal 7226 01/18/2010 get promotional price                                          
 t.dp2 as dptwo, TicketNumber_PK as ticketnumber, t.zipcode , v.courtdate , v.courtlocation as courtid, t.midnumber ,@user as Abb,dbo.GetPromoPriceTemplate(T.zipcode,@lettertype) AS promotemplate
--master.dbo.fn_getIMBImage('00040001120' + RIGHT(REPLICATE('0',9) + CAST(n.noteid AS VARCHAR(9)),9)   ,replace(n.zipcode, '-','')+isnull(n.dp2,'')) as IMBImage                                                           
 into #temp59            
 from  tblletternotes n , dallastraffictickets.dbo.tblticketsarchive t, dallastraffictickets.dbo.tblticketsviolationsarchive v,             
  #letters tt, dallastraffictickets.dbo.tblcourts c, dallastraffictickets.dbo.tblstate s            
 where  n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and t.courtid = c.courtid            
 and t.lastname is not null              
 and t.firstname is not null            
 and t.Flag1 in ('Y','D','S')            
 and isnull(t.donotmailflag,0)   = 0
 -- Rab Nawaz Khan 8754 04/22/2011 added to Dispay only Judge Week Before cases
 AND v.violationstatusid = 103             
 -- End 8754
and v.violationbonddate is null         
--and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
 order by left(t.zipcode, 5), t.midnumber            
            
            
 select @recCount = count(distinct recordid) from #temp59              
 declare @table59 TABLE  (            
    [letterId] [varchar] (30)   ,            
    [recordid] [int] ,            
    [FirstName] [varchar] (20)  ,            
    [LastName] [varchar] (20)  ,            
    [address] [varchar] (100) ,            
    [city] [varchar] (50) ,            
    [state] [varchar] (2)  ,            
    [FineAmount] [money]  ,            
    [violationdescription] [varchar] (200) ,            
    [CourtName] [varchar] (50) ,            
    [dpc] [varchar] (10)  ,            
    [dptwo] [varchar] (10)  ,            
    [TicketNumber] [varchar] (20)  ,            
    [zipcode] [varchar] (15) ,            
    [language] [int],            
    [courtdate] [datetime],            
    [Abb] [varchar] (10)  ,
    promotemplate [varchar] (100),
    --Yasir Kamal 6648 10/07/2009 display IMB Barcode
    --Sabir Khan 6912 11/11/2009 remove IMBImage field...
    --[IMBImage] [IMAGE],
	midnumber varchar(20),
	courtid int       
   )            
            
 insert into @table59 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
   violationdescription, courtname, dpc, dptwo, ticketnumber, zipcode, language, courtdate, Abb,promotemplate, midnumber, courtid)              
 select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount,            
   violationdescription, courtname, dpc, dptwo, ticketnumber, zipcode, 0, courtdate, Abb,promotemplate, midnumber, courtid from #temp59              
            
 if @printtype = 1            
 -- DOUBLE SIDE PRINT........            
  begin            
   insert into @table59 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
     violationdescription, courtname, dpc, dptwo, ticketnumber, zipcode, language, courtdate, Abb,promotemplate, midnumber, courtid)              
   select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount,            
     violationdescription, courtname, dpc, dptwo, ticketnumber, zipcode, 1, courtdate, Abb,promotemplate, midnumber, courid from #temp59 ;            
  end            
       
 if @mailbarry=0        
 begin             
 select *, midnumber as midnum, left(zipcode,5) + midnumber as zipmid, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table59       
 union ALL 
 -- Rab Nawaz Khan 10187 04/12/2012 Address, City and Zip Code Changed for the Testing Advertisment mailer which send to Barry 
 select convert(varchar(20),@recCount) + ' letters sent' as letterid  , 0 as recordid , 'BARRY', 'BOBBIT', '7 Butterfly Place', 'Austin', 'TX', 0, 'N/A', 'DALLAS MUNICIPAL COURT', '0', '00' , 'N/A', '78738-1343' as zipcode, 0, '1/1/1900', @user ,'0,35.00', '00000000' as midnumber, 0 as courtid, '00000000' as midnum, '7522300000000' as zipmid, '0'   
  --Yasir Kamal 7226 end
    
            
 end      
 else      
 select *, midnumber as midnum, left(zipcode,5) + midnumber as zipmid, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table59             
            
end            

if @lettertype = 61
begin            
 Select  convert(varchar(20),n.noteid) as letterid, t.recordid,   upper(t.FirstName) as firstname,                                          
 upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,''))  as  address, upper(t.city) as city,  s.state,   
case when isnull(v.fineamount,0)=0 then 100 else v.fineamount end as FineAmount,violationdescription, c.CourtName, t.dpc,                                          
 t.dp2 as dptwo, TicketNumber_PK as ticketnumber, t.zipcode , v.courtdate , v.courtlocation as courtid, t.midnumber ,@user as Abb                                              
 into #temp61            
 from  tblletternotes n , dallastraffictickets.dbo.tblticketsarchive t, dallastraffictickets.dbo.tblticketsviolationsarchive v,             
  #letters tt, dallastraffictickets.dbo.tblcourts c, dallastraffictickets.dbo.tblstate s            
 where  n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and t.courtid = c.courtid            
 and t.lastname is not null              
 and t.firstname is not null            
 and t.Flag1 in ('Y','D','S')            
 and isnull(t.donotmailflag,0)   = 0             
and v.violationbonddate is null         
--and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
 order by left(t.zipcode, 5), t.midnumber            
            
            
 select @recCount = count(distinct recordid) from #temp61             
 declare @table61 TABLE  (            
    [letterId] [varchar] (30)   ,            
    [recordid] [int] ,            
    [FirstName] [varchar] (20)  ,            
    [LastName] [varchar] (20)  ,            
    [address] [varchar] (100) ,            
    [city] [varchar] (50) ,            
    [state] [varchar] (2)  ,            
    [FineAmount] [money]  ,            
    [violationdescription] [varchar] (200) ,            
    [CourtName] [varchar] (50) ,            
    [dpc] [varchar] (10)  ,            
    [dptwo] [varchar] (10)  ,            
    [TicketNumber] [varchar] (20)  ,            
    [zipcode] [varchar] (15) ,            
    [language] [int],            
    [courtdate] [datetime],            
    [Abb] [varchar] (10)  ,
	midnumber varchar(20),
	courtid int       
   )            
            
 insert into @table61 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
   violationdescription, courtname, dpc, dptwo, ticketnumber, zipcode, language, courtdate, Abb, midnumber, courtid)              
 select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount,            
   violationdescription, courtname, dpc, dptwo, ticketnumber, zipcode, 0, courtdate, Abb, midnumber, courtid from #temp61              
            
 if @printtype = 1            
 -- DOUBLE SIDE PRINT........            
  begin            
   insert into @table61 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
     violationdescription, courtname, dpc, dptwo, ticketnumber, zipcode, language, courtdate, Abb, midnumber, courtid)              
   select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount,            
     violationdescription, courtname, dpc, dptwo, ticketnumber, zipcode, 1, courtdate, Abb, midnumber, courid from #temp61 ;            
  end            
       
 if @mailbarry=0        
 begin             
 select *, midnumber as midnum, left(zipcode,5) + midnumber as zipmid, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table61       
 union 
 -- Rab Nawaz Khan 10187 04/12/2012 Address, City and Zip Code Changed for the Testing Advertisment mailer which send to Barry       
 select convert(varchar(20),@recCount) + ' letters sent' as letterid  , 0 as recordid , 'BARRY', 'BOBBIT', '7 Butterfly Place', 'Austin', 'TX', 0, 'N/A', 'DALLAS MUNICIPAL COURT', '0', '00' , 'N/A', '78738-1343' as zipcode, 0, '1/1/1900', @user , '00000000' as midnumber, 0 as courtid, '00000000' as midnum, '7522300000000' as zipmid, '0'   
  
    
            
 end      
 else      
 select *, midnumber as midnum, left(zipcode,5) + midnumber as zipmid, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table61             
         
             
            
end            
----Yasir Kamal 6648 end

-- tahir 4961 10/22/08 
-- DMC Jury Week Before LETTER
-- tahir 5052 11/03/2008 added DMC Jury Regular letter...                   
if @lettertype in( 60, 62)
begin            
 Select  convert(varchar(20),n.noteid) as letterid, t.recordid,   upper(t.FirstName) as firstname,                                          
 upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,''))  as  address, upper(t.city) as city,  s.state,   
case when isnull(v.fineamount,0)=0 then 100 else v.fineamount end as FineAmount,violationdescription, c.CourtName, t.dpc,  
--Yasir Kamal 7226 01/18/2010 get promotional price                                        
 t.dp2 as dptwo, TicketNumber_PK as ticketnumber, t.zipcode , v.courtdate , v.courtlocation as courtid, t.midnumber ,@user as Abb,dbo.GetPromoPriceTemplate(T.zipcode,@lettertype) AS promotemplate                                              
 into #temp60            
 from  tblletternotes n , dallastraffictickets.dbo.tblticketsarchive t, dallastraffictickets.dbo.tblticketsviolationsarchive v,             
  #letters tt, dallastraffictickets.dbo.tblcourts c, dallastraffictickets.dbo.tblstate s            
 where  n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and t.courtid = c.courtid            
 and t.lastname is not null              
 and t.firstname is not null            
 and t.Flag1 in ('Y','D','S')            
 and isnull(t.donotmailflag,0)   = 0             
and v.violationbonddate is null         
--and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
 order by left(t.zipcode, 5), t.midnumber            
            
            
 select @recCount = count(distinct recordid) from #temp60              
 declare @table60 TABLE  (            
    [letterId] [varchar] (30)   ,            
    [recordid] [int] ,            
    [FirstName] [varchar] (20)  ,            
    [LastName] [varchar] (20)  ,            
    [address] [varchar] (100) ,            
    [city] [varchar] (50) ,            
    [state] [varchar] (2)  ,            
    [FineAmount] [money]  ,            
    [violationdescription] [varchar] (200) ,            
    [CourtName] [varchar] (50) ,            
    [dpc] [varchar] (10)  ,            
    [dptwo] [varchar] (10)  ,            
    [TicketNumber] [varchar] (20)  ,            
    [zipcode] [varchar] (15) ,            
    [language] [int],            
    [courtdate] [datetime],            
    [Abb] [varchar] (10)  ,
    promotemplate [varchar] (100),
	midnumber varchar(20),
	courtid int       
   )            
            
 insert into @table60 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
   violationdescription, courtname, dpc, dptwo, ticketnumber, zipcode, language, courtdate, Abb,promotemplate, midnumber, courtid)              
 select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount,            
   violationdescription, courtname, dpc, dptwo, ticketnumber, zipcode, 0, courtdate, Abb,promotemplate, midnumber, courtid from #temp60              
            
 if @printtype = 1            
 -- DOUBLE SIDE PRINT........            
  begin            
   insert into @table60 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
     violationdescription, courtname, dpc, dptwo, ticketnumber, zipcode, language, courtdate, Abb,promotemplate ,midnumber, courtid)              
   select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount,            
     violationdescription, courtname, dpc, dptwo, ticketnumber, zipcode, 1, courtdate, Abb,promotemplate ,midnumber, courid from #temp60 ;            
  end            
       
 if @mailbarry=0        
 begin             
 select *, midnumber as midnum, left(zipcode,5) + midnumber as zipmid, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table60       
 union   
 -- Rab Nawaz Khan 10187 04/12/2012 Address, City and Zip Code Changed for the Testing Advertisment mailer which send to Barry     
 select convert(varchar(20),@recCount) + ' letters sent' as letterid  , 0 as recordid , 'BARRY', 'BOBBIT', '7 Butterfly Place', 'Austin', 'TX', 0, 'N/A', 'DALLAS MUNICIPAL COURT', '0', '00' , 'N/A', '78738-1343' as zipcode, 0, '1/1/1900', @user ,'0,35.00,' ,'00000000' as midnumber, 0 as courtid, '00000000' as midnum, '7522300000000' as zipmid, '0'   
  
   --Yasir Kamal 7226 end 
            
 end      
 else      
 select *, midnumber as midnum, left(zipcode,5) + midnumber as zipmid, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table60             
         
             
            
end            

-- tahir 5701 03/25/2009 changed the logic..  
-- FORT WORTH APPEARANCE LETTER            
if @lettertype = 63      
begin            
 Select  convert(varchar(20),n.noteid) as letterid, t.recordid,  upper(t.FirstName) as firstname,                                          
 upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,''))  as  address, upper(t.city) as city,  s.state,   
case when isnull(v.fineamount,0)=0 then 100 else v.fineamount end as FineAmount,violationdescription, c.CourtName, t.dpc, 
--Yasir Kamal 7365 0201/2010 get court date                                         
 t.dp2 as dptwo, TicketNumber_PK, t.zipcode , v.CourtDate as courtdate ,@user as Abb,dbo.GetPromoPriceTemplate(T.zipcode,@lettertype) AS promotemplate                                                             
 into #temp63           
 from  tblletternotes n , dallastraffictickets.dbo.tblticketsarchive t, dallastraffictickets.dbo.tblticketsviolationsarchive v,             
  #letters tt, dallastraffictickets.dbo.tblcourts c    , dallastraffictickets.dbo.tblstate s        
 where  n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and t.courtid = c.courtid            
 and t.lastname is not null              
 and t.firstname is not null            
 and t.Flag1 in ('Y','D','S')            
 and isnull(t.donotmailflag,0)   = 0             
 -- tahir 7966 07/16/2010 exclude bond cases....
 and v.violationbonddate is null  
-- Rab Nawaz Khan 10516 10/25/2012 Exclude the ordinance cases and juvenile cases . . . 
AND (SUBSTRING(ISNULL(v.CauseNumber, 'TaskO_10516'), 5, 1) <> 'O' AND SUBSTRING(ISNULL(v.CauseNumber, 'TaskO_10516'), 5, 1) <> 'M')    
--and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
 order by left(t.zipcode, 5), t.midnumber            
            
            
 select @recCount = count(distinct recordid) from #temp63              
 declare @table63 TABLE  (            
    [letterId] [varchar] (30)   ,            
    [recordid] [int] ,            
    [FirstName] [varchar] (20)  ,            
    [LastName] [varchar] (20)  ,            
    [address] [varchar] (100) ,            
    [city] [varchar] (50) ,            
    [state] [varchar] (2)  ,            
    [FineAmount] [money]  ,            
    [violationdescription] [varchar] (200) ,            
    [CourtName] [varchar] (50) ,            
    [dpc] [varchar] (10)  ,            
    [dptwo] [varchar] (10)  ,            
    [TicketNumber_PK] [varchar] (20)  ,            
    [zipcode] [varchar] (15) ,            
    [language] [int],            
    [courtdate] [datetime],            
    [Abb] [varchar] (10),
    promotemplate [varchar] (100)            
   )            
            
 insert into @table63 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
   violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb,promotemplate)              
 select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount,            
   violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 0, courtdate, Abb,promotemplate from #temp63              
            
 if @printtype = 1            
 -- DOUBLE SIDE PRINT........            
  begin            
   insert into @table63 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb,promotemplate)              
   select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount,            
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 1, courtdate, Abb,promotemplate from #temp63 ;             
  end            
         
 if @mailbarry=0          
 begin      
 select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table63           
 union 
 -- Rab Nawaz Khan 10187 04/12/2012 Address, City and Zip Code Changed for the Testing Advertisment mailer which send to Barry      
  select convert(varchar(20),@recCount) + ' letters sent' as letterid  , 0 as recordid , 'BARRY', 'BOBBIT', '7 Butterfly Place', 'Austin', 'TX', 0, 'N/A', 'DALLAS MUNICIPAL COURT', '0', '00' , 'N/A', '78738-1343' as zipcode, 0, '1/1/1900',@user ,'0,35.00', '0'  
  
     
      
 end      
 else      
 select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table63             
         
             
            
end            


-- FORT WORTH APPEARANCE DAY OF LETTER  
-- Sabir Khan 7370 02/02/2010 Court date has been used instead of violation date...          
if @lettertype = 64            
begin            
 Select  convert(varchar(20),n.noteid) as letterid, t.recordid,   upper(t.FirstName) as firstname,                                          
 upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,''))  as  address, upper(t.city) as city,  s.state,   
case when isnull(v.fineamount,0)=0 then 100 else v.fineamount end as FineAmount,violationdescription, c.CourtName, t.dpc,                                          
 t.dp2 as dptwo, TicketNumber_PK, t.zipcode , v.courtdate as courtdate ,@user as Abb,dbo.GetPromoPriceTemplate(T.zipcode,@lettertype) AS promotemplate                                                             
 into #temp64            
 from  tblletternotes n , dallastraffictickets.dbo.tblticketsarchive t, dallastraffictickets.dbo.tblticketsviolationsarchive v,             
  #letters tt, dallastraffictickets.dbo.tblcourts c, dallastraffictickets.dbo.tblstate s            
 where  n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and t.courtid = c.courtid            
 and t.lastname is not null              
 and t.firstname is not null            
 and t.Flag1 in ('Y','D','S')            
 and isnull(t.donotmailflag,0)   = 0             
--and v.violationbonddate is null         
--and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP) 
-- Rab Nawaz Khan 10516 10/25/2012 Exclude the ordinance cases and juvenile cases . . . 
AND (SUBSTRING(ISNULL(v.CauseNumber, 'TaskO_10516'), 5, 1) <> 'O' AND SUBSTRING(ISNULL(v.CauseNumber, 'TaskO_10516'), 5, 1) <> 'M')   
 order by left(t.zipcode, 5), t.midnumber            
            
            
 select @recCount = count(distinct recordid) from #temp64              
 declare @table64 TABLE  (            
    [letterId] [varchar] (30)   ,            
    [recordid] [int] ,            
    [FirstName] [varchar] (20)  ,            
    [LastName] [varchar] (20)  ,            
    [address] [varchar] (100) ,            
    [city] [varchar] (50) ,            
    [state] [varchar] (2)  ,            
    [FineAmount] [money]  ,            
    [violationdescription] [varchar] (200) ,            
    [CourtName] [varchar] (50) ,            
    [dpc] [varchar] (10)  ,            
    [dptwo] [varchar] (10)  ,            
    [TicketNumber_PK] [varchar] (20)  ,            
    [zipcode] [varchar] (15) ,            
    [language] [int],            
    [courtdate] [datetime],            
    [Abb] [varchar] (10),
    [promotemplate] [varchar] (100)                 
   )            
            
 insert into @table64 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
   violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb,promotemplate)              
 select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount,            
   violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 0, courtdate, Abb,promotemplate from #temp64              
            
 if @printtype = 1            
 -- DOUBLE SIDE PRINT........            
  begin            
   insert into @table64 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb,promotemplate)              
   select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount,            
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 1, courtdate, Abb,promotemplate from #temp64 ;            
  end            
       
 if @mailbarry=0        
 begin             
 select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table64       
 union  
 -- Rab Nawaz Khan 10187 04/12/2012 Address, City and Zip Code Changed for the Testing Advertisment mailer which send to Barry      
 select convert(varchar(20),@recCount) + ' letters sent' as letterid  , 0 as recordid , 'BARRY', 'BOBBIT', '7 Butterfly Place', 'Austin', 'TX', 0, 'N/A', 'DALLAS MUNICIPAL COURT', '0', '00' , 'N/A', '78738-1343' as zipcode, 0, '1/1/1900', @user ,'0,35.00', '0'   
  
    
            
 end      
 else      
 select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table64             
         
             
            
end            

--5701 end

-- tahir 6481 08/31/2009 new mailer...
-- SUGAR LAND WEEK BEFORE LETTER...... 
--Sabir Khan 9683 09/15/2011 Need to remove last two character from ticket number and cancatinate -0x            
if @lettertype = 68      
BEGIN            
 Select distinct convert(varchar(20),n.noteid) as letterid, t.recordid,v.courtdate, upper(t.FirstName) as firstname,                                          
 upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,''))  as  address, upper(t.city) as city,  s.state,   
 -- Rab Nawaz Khan 9704 09/26/2011 Default fine amount 100 logic is commented. . .  
 --case when isnull(v.fineamount,0)=0 then 100 else v.fineamount end as FineAmount,
 isnull(v.fineamount,0) as FineAmount,violationdescription, t.dpc,  
 t.dp2 as dptwo, SUBSTRING(v.ticketnumber_pk,0,LEN(v.ticketnumber_pk)-1) + '-0x' as ticketnumber, t.zipcode, left(t.zipcode,5) + t.midnumber as zipmid ,@user as Abb                                               
 INTO #letter68 from tblletternotes n , tblticketsarchive t, tblticketsviolationsarchive v, #letters tt, tblcourts c , tblstate s           
 where  n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and V.courtlocation = c.courtid            
 and t.Flag1 in ('Y','D','S')            
 and t.donotmailflag = 0             
 --and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
 order by [zipmid]  
 
ALTER TABLE #letter68 ADD isFineAmount BIT NOT NULL DEFAULT 1
SELECT REcordid INTO #sugarlandWeek FROM #letter68 WHERE ISNULL(FineAmount,0) = 0

UPDATE T
SET
	isFineAmount = 0
FROM #letter68 t INNER JOIN #sugarlandWeek f ON f.recordid = t.recordid

SELECT * FROM #letter68

DROP TABLE #sugarlandWeek
DROP TABLE #letter68
           
end   
-- 6481 ends....       

-- Sabir Khan 9329 05/26/2011 South Houston Week Before Mailer has been added.
if @lettertype = 122      
BEGIN            
 Select distinct convert(varchar(20),n.noteid) as letterid, t.recordid,v.courtdate, upper(t.FirstName) as firstname,                                          
 upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,''))  as  address, upper(t.city) as city,  s.state, 
-- Rab Nawaz Khan 9704 09/26/2011 Default fine amount 100 logic is commented. . .   
-- case when isnull(v.fineamount,0)=0 then 100 else v.fineamount end as FineAmount,violationdescription, t.dpc,        
isnull(v.fineamount,0) as FineAmount,violationdescription, t.dpc,                                          
 t.dp2 as dptwo, v.ticketnumber_pk as ticketnumber, t.zipcode, left(t.zipcode,5) + t.midnumber as zipmid ,@user as Abb                                               
 INTO #letter122 from tblletternotes n , tblticketsarchive t, tblticketsviolationsarchive v, #letters tt, tblcourts c , tblstate s           
 where  n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and V.courtlocation = c.courtid            
 and t.Flag1 in ('Y','D','S')            
 and t.donotmailflag = 0   
 and v.violationstatusid <> 80 --Abbas Qamar 9991 01/17/2012 Exclude dispose cases.                     
 --and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
 order by [zipmid]  
 
 ALTER TABLE #letter122 ADD isFineAmount BIT NOT NULL DEFAULT 1
SELECT REcordid INTO #Soho FROM #letter122 WHERE ISNULL(FineAmount,0) = 0

UPDATE T
SET
	isFineAmount = 0
FROM #letter122 t INNER JOIN #Soho f ON f.recordid = t.recordid

SELECT * FROM #letter122

DROP TABLE #Soho
DROP TABLE #letter122
           
end 

 -- Rab Nawaz Khan 9329 06/17/2011 South Houston App day Of Mailer has been added.
if @lettertype = 123      
BEGIN            
 Select distinct convert(varchar(20),n.noteid) as letterid, t.recordid,v.courtdate, upper(t.FirstName) as firstname,                                          
 upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,''))  as  address, upper(t.city) as city,  s.state,   
 -- Rab Nawaz Khan 9704 09/26/2011 Default fine amount 100 logic is commented. . .  
 --case when isnull(v.fineamount,0)=0 then 100 else v.fineamount end as FineAmount
 isnull(v.fineamount,0) as FineAmount,violationdescription, t.dpc, 
 t.dp2 as dptwo, v.ticketnumber_pk as ticketnumber, t.zipcode, left(t.zipcode,5) + t.midnumber as zipmid ,@user as Abb                                               
 INTO #letter123 from tblletternotes n , tblticketsarchive t, tblticketsviolationsarchive v, #letters tt, tblcourts c , tblstate s           
 where  n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and V.courtlocation = c.courtid            
 and t.Flag1 in ('Y','D','S')            
 and t.donotmailflag = 0             
 --and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
 order by [zipmid]
 
ALTER TABLE #letter123 ADD isFineAmount BIT NOT NULL DEFAULT 1
SELECT REcordid INTO #soho123 FROM #letter123 WHERE ISNULL(FineAmount,0) = 0

UPDATE T
SET
	isFineAmount = 0
FROM #letter123 t INNER JOIN #soho123 f ON f.recordid = t.recordid

SELECT * FROM #letter123

DROP TABLE #soho123
DROP TABLE #letter123            
end 


-- Rab Nawaz Khan 9329 06/21/2011 South Houston Warrant Mailer has been added.
if @lettertype = 124      
BEGIN            
 Select distinct convert(varchar(20),n.noteid) as letterid, t.recordid,v.courtdate, upper(t.FirstName) as firstname,                                          
 upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,''))  as  address, upper(t.city) as city,  s.state,   
 case when isnull(v.fineamount,0)=0 then 100 else v.fineamount end as FineAmount,violationdescription, t.dpc,                                          
 t.dp2 as dptwo, v.ticketnumber_pk as ticketnumber_pk, t.zipcode, left(t.zipcode,5) + t.midnumber as zipmid ,@user as Abb                                               
  from tblletternotes n , tblticketsarchive t, tblticketsviolationsarchive v, #letters tt, tblcourts c , tblstate s           
 where  n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and V.courtlocation = c.courtid            
 and t.Flag1 in ('Y','D','S')            
 and t.donotmailflag = 0             
  order by [zipmid]  
  
end 


-- Rab Nawaz Khan 9329 06/17/2011 South Houston DLQ Mailer has been added.
if @lettertype = 125      
BEGIN            
 Select distinct convert(varchar(20),n.noteid) as letterid, t.recordid,v.courtdate, upper(t.FirstName) as firstname,                                          
 upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,''))  as  address, upper(t.city) as city,  s.state,   
 -- Rab Nawaz Khan 9704 09/26/2011 Default fine amount 100 logic is commented. . .  
 -- case when isnull(v.fineamount,0)=0 then 100 else v.fineamount end as FineAmount,
 isnull(v.fineamount,0) as FineAmount,violationdescription, t.dpc, 
 t.dp2 as dptwo, v.ticketnumber_pk as ticketnumber, t.zipcode, left(t.zipcode,5) + t.midnumber as zipmid ,@user as Abb                                               
 INTO #letter125 from tblletternotes n , tblticketsarchive t, tblticketsviolationsarchive v, #letters tt, tblcourts c , tblstate s           
 where  n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and V.courtlocation = c.courtid            
 and t.Flag1 in ('Y','D','S')            
 and t.donotmailflag = 0 
 and v.violationstatusid <> 80 --Abbas Qamar 9991 01/17/2012 Exclude dispose cases.                       
  order by [zipmid] 
  
ALTER TABLE #letter125 ADD isFineAmount BIT NOT NULL DEFAULT 1
SELECT REcordid INTO #soho125 FROM #letter125 WHERE ISNULL(FineAmount,0) = 0

UPDATE T
SET
	isFineAmount = 0
FROM #letter125 t INNER JOIN #soho125 f ON f.recordid = t.recordid

SELECT * FROM #letter125

DROP TABLE #soho125
DROP TABLE #letter125
end 



-- tahir 6610 09/24/2009 new mailer for arlington court...

-- ARLINGTON APPEARANCE LETTER            
if @lettertype = 69            
begin            
 Select  convert(varchar(20),n.noteid) as letterid, t.recordid,  upper(t.FirstName) as firstname,                                          
 upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,''))  as  address, upper(t.city) as city,  s.state,   
case when isnull(v.fineamount,0)=0 then 100 else v.fineamount end as FineAmount,violationdescription, c.CourtName, t.dpc,                                          
 t.dp2 as dptwo, TicketNumber_PK, t.zipcode , V.courtdate ,@user as Abb,
 --Yasir Kamal 6838 10/23/2009 IMB added.
 --Sabir Khan 6912 11/04/2009 remove IMBImage field...
 --master.dbo.fn_getIMBImage('00040001120' + RIGHT(REPLICATE('0',9) + CAST(n.noteid AS VARCHAR(9)),9)   ,replace(n.zipcode, '-','')+isnull(n.dp2,'')) as IMBImage,
 dbo.GetPromoPriceTemplate(T.zipcode,@lettertype) AS promotemplate                                               
 into #temp69           
 from  tblletternotes n , dallastraffictickets.dbo.tblticketsarchive t, dallastraffictickets.dbo.tblticketsviolationsarchive v,             
  #letters tt, dallastraffictickets.dbo.tblcourts c    , dallastraffictickets.dbo.tblstate s        
 where  n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and t.courtid = c.courtid            
 and t.lastname is not null              
 and t.firstname is not null            
 and t.Flag1 in ('Y','D','S')            
 and isnull(t.donotmailflag,0)   = 0             
 and v.violationbonddate is null     
 order by left(t.zipcode, 5), t.midnumber            
            
            
 select @recCount = count(distinct recordid) from #temp69             
 declare @table69 TABLE  (            
    [letterId] [varchar] (30)   ,            
    [recordid] [int] ,            
    [FirstName] [varchar] (20)  ,            
    [LastName] [varchar] (20)  ,            
    [address] [varchar] (100) ,            
    [city] [varchar] (50) ,            
    [state] [varchar] (2)  ,            
    [FineAmount] [money]  ,            
    [violationdescription] [varchar] (200) ,            
    [CourtName] [varchar] (50) ,            
    [dpc] [varchar] (10)  ,            
    [dptwo] [varchar] (10)  ,            
    [TicketNumber_PK] [varchar] (20)  ,            
    [zipcode] [varchar] (15) ,            
    [language] [int],            
    [courtdate] [datetime],            
    [Abb] [varchar] (10),
    --Yasir Kamal 6838 10/23/2009 IMB added.
    --Sabir Khan 6912 11/04/2009 remove IMBImage field...
    --[IMBImage][Image],
    --Yasir Kamal 7226 01/18/2010 datatype changed from int to varchar
    [promotemplate] [varchar] (100)      
   )            
            
 insert into @table69 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
   violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb,promotemplate)              
 select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount,            
   violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 0, courtdate, Abb, promotemplate from #temp69              
            
 if @printtype = 1            
 -- DOUBLE SIDE PRINT........            
  begin            
   insert into @table69 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb, promotemplate)              
   select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount,            
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 1, courtdate, Abb, promotemplate from #temp69              
  end            
         
 if @mailbarry=0          
 begin      
 select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table69             
 union ALL  
 -- Rab Nawaz Khan 10187 04/12/2012 Address, City and Zip Code Changed for the Testing Advertisment mailer which send to Barry    
  select convert(varchar(20),@recCount) + ' letters sent' as letterid  , 0 as recordid , 'BARRY', 'BOBBIT', '7 Butterfly Place', 'Austin', 'TX', 0, 'N/A', 'DALLAS MUNICIPAL COURT', '0', '00' , 'N/A', '78738-1343' as zipcode, 0, '1/1/1900',@user ,'0,75.00', '0'  
  --Yasir Kamal 7226 end
     
      
 end      
 else      
 select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table69             
         
             
            
end            
            
            
-- ARLINGTON APPEARANCE DAY OF LETTER            
if @lettertype = 70            
begin            
 Select  convert(varchar(20),n.noteid) as letterid, t.recordid,   upper(t.FirstName) as firstname,                                          
 upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,''))  as  address, upper(t.city) as city,  s.state,   
case when isnull(v.fineamount,0)=0 then 100 else v.fineamount end as FineAmount,violationdescription, c.CourtName, t.dpc,                                          
 t.dp2 as dptwo, TicketNumber_PK, t.zipcode , V.courtdate ,@user as Abb,
 --Yasir Kamal 6838 10/23/2009 IMB added.
 --Sabir Khan 6912 11/11/2009 remove IMBImage field...
 --master.dbo.fn_getIMBImage('00040001120' + RIGHT(REPLICATE('0',9) + CAST(n.noteid AS VARCHAR(9)),9)   ,replace(n.zipcode, '-','')+isnull(n.dp2,'')) as IMBImage,
 dbo.GetPromoPriceTemplate(T.zipcode,@lettertype) AS promotemplate --, t.zip                                              
 into #temp70            
 from  tblletternotes n , dallastraffictickets.dbo.tblticketsarchive t, dallastraffictickets.dbo.tblticketsviolationsarchive v,             
  #letters tt, dallastraffictickets.dbo.tblcourts c, dallastraffictickets.dbo.tblstate s            
 where  n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and t.courtid = c.courtid            
 and t.lastname is not null              
 and t.firstname is not null            
 and t.Flag1 in ('Y','D','S')            
 and isnull(t.donotmailflag,0)   = 0             
and v.violationbonddate is null         
 order by left(t.zipcode, 5), t.midnumber            
            
            
 select @recCount = count(distinct recordid) from #temp70
 declare @table70 TABLE  (            
    [letterId] [varchar] (30)   ,            
    [recordid] [int] ,            
    [FirstName] [varchar] (20)  ,            
    [LastName] [varchar] (20)  ,            
    [address] [varchar] (100) ,            
    [city] [varchar] (50) ,            
    [state] [varchar] (2)  ,            
    [FineAmount] [money]  ,            
    [violationdescription] [varchar] (200) ,            
    [CourtName] [varchar] (50) ,            
    [dpc] [varchar] (10)  ,            
    [dptwo] [varchar] (10)  ,            
    [TicketNumber_PK] [varchar] (20)  ,            
    [zipcode] [varchar] (15) ,            
    [language] [int],            
    [courtdate] [datetime],            
    [Abb] [varchar] (10) ,
    --Yasir Kamal 6838 10/23/2009 IMB added.
    --Sabir Khan 6912 11/11/2009 remove IMBImage field...
    --[IMBImage] [Image],
    --Yasir Kamal 7226 01/18/2010 datatype changed from int to varchar
    [promotemplate] [varchar] (100)           
   )            
            
 insert into @table70 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
   violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb, promotemplate)              
 select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount+60,            
   violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 0, courtdate, Abb, promotemplate from #temp70              
            
 if @printtype = 1            
 -- DOUBLE SIDE PRINT........            
  begin            
   insert into @table70 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb, promotemplate)              
   select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount+60,            
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 1, courtdate, Abb, promotemplate from #temp70 ;            
  end            
       
 if @mailbarry=0        
 begin             
 select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table70       
 union ALL  
 -- Rab Nawaz Khan 10187 04/12/2012 Address, City and Zip Code Changed for the Testing Advertisment mailer which send to Barry     
 select convert(varchar(20),@recCount) + ' letters sent' as letterid  , 0 as recordid , 'BARRY', 'BOBBIT', '7 Butterfly Place', 'Austin', 'TX', 0, 'N/A', 'DALLAS MUNICIPAL COURT', '0', '00' , 'N/A', '78738-1343' as zipcode, 0, '1/1/1900', @user ,'0,75.00', '0'   
  
        --Yasir Kamal 7226 end
            
 end      
 else      
 select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table70             
         
             
            
end 

--Sabir Khan 6610 10/01/2009 Arlington Warrant Letter added into track batches...
-- ARLINGTON WARRANT LETTER            
if @lettertype = 71            
begin            
 Select  convert(varchar(20),tt.noteid) as letterid, t.recordid, upper( t.FirstName) as firstname,                                          
  upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,'')) as address, upper(t.city) as city, s.state,       
   case when isnull(v.bondamount,0)=0 then 100 else v.bondamount end as FineAmount,  
 violationdescription, c.CourtName, t.dpc,   
 --Yasir Kamal 7226  01/21/2010 get promotional price                                                                               
 t.dp2 as dptwo, TicketNumber_PK, t.zipcode,@user as Abb,dbo.GetPromoPriceTemplate(T.zipcode,@lettertype) AS promotemplate
 --Yasir Kamal 6838 10/23/2009 IMB added.
 --Sabir Khan 6912 11/11/2009 remove IMBImage field...
 --master.dbo.fn_getIMBImage('00040001120' + RIGHT(REPLICATE('0',9) + CAST(n.noteid AS VARCHAR(9)),9)   ,replace(n.zipcode, '-','')+isnull(n.dp2,'')) as IMBImage                                                
 into #temp71            
-- from  tblletternotes n , dallastraffictickets.dbo.tblticketsarchive t, dallastraffictickets.dbo.tblticketsviolationsarchive v,             
 from   dallastraffictickets.dbo.tblticketsarchive t, dallastraffictickets.dbo.tblticketsviolationsarchive v,             
  #letters tt, dallastraffictickets.dbo.tblcourts c  , dallastraffictickets.dbo.tblstate s          
 where  tt.recordid = t.recordid            
 --and n.noteid=tt.noteid              
      
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and t.courtid = c.courtid            
 and t.Flag1 in ('Y','D','S')            
 and t.donotmailflag = 0             
 and len(isnull(t.firstname,'')+isnull(t.lastname,''))>0   
--and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
 order by left(t.zipcode, 5), t.midnumber            
            
            
 select @recCount = count(distinct recordid) from #temp71              
 declare @table71 TABLE  (            
    [letterId] [varchar] (30)   ,            
    [recordid] [int] ,            
    [FirstName] [varchar] (20)  ,            
    [LastName] [varchar] (20)  ,            
    [address] [varchar] (100) ,            
    [city] [varchar] (50) ,            
    [state] [varchar] (2)  ,         
    [FineAmount] [money]  ,            
    [violationdescription] [varchar] (200) ,            
    [CourtName] [varchar] (50) ,            
    [dpc] [varchar] (10)  ,            
    [dptwo] [varchar] (10)  ,            
    [TicketNumber_PK] [varchar] (20)  ,            
    [zipcode] [varchar] (15) ,            
    [language] [int],            
    [Abb] [varchar] (10),
    [promotemplate] [varchar] (100)   
    --Yasir Kamal 6838 10/23/2009 IMB added.
    --Sabir Khan 6912 11/11/2009 remove IMBImage field...
    --[IMBImage] [Image]            
   )            
   
 insert into @table71 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
   violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, Abb,promotemplate)              
 select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount,            
   violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 0, Abb,promotemplate from #temp71              
            
 if @printtype = 1            
 -- DOUBLE SIDE PRINT........            
  begin            
   insert into @table71 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, Abb,promotemplate)              
   select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount,            
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 1, Abb,promotemplate from #temp71 ;             
  end         
   
 else    
 begin    
 insert into @table71 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
   violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, Abb,promotemplate)              
 select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount,            
   violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 0, Abb,promotemplate from #temp71              
            
 if @printtype = 1            
 -- DOUBLE SIDE PRINT........            
  begin            
   insert into @table71 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, Abb,promotemplate)              
   select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount,            
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 1, Abb,promotemplate from #temp71 ;             
  end      
 end     
        
 if @mailbarry=0         
 begin      
 select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table71           
    --order by left(t.zipcode, 5), t.midnumber               
 UNION ALL  
 -- Rab Nawaz Khan 10187 04/12/2012 Address, City and Zip Code Changed for the Testing Advertisment mailer which send to Barry     
 select convert(varchar(20),@recCount) + ' letters sent' as letterid  , 0 as recordid , 'BARRY', 'BOBBIT', '7 Butterfly Place', 'Austin', 'TX', 0, 'N/A', 'DALLAS MUNICIPAL COURT', '0', '00' , 'N/A', '78738-1343' as zipcode, 0,@user,'0,75.00', '0'      
 end      
 else      
    select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table71           
    --order by left(t.zipcode, 5), t.midnumber                
--7226  end                                                     
 end           
--Yasir Kamal 6838 end.                       
--Sabir Khan 6995 11/19/2009 FELONY MAILER ADDED....
-- DALLAS COUNTY CRIMINAL FELONY LETTER......            
if @lettertype = 72  
BEGIN            
            
Select  convert(varchar(20),n.noteid) as letterid, t.recordid,   upper(t.FirstName) as firstname,   
 upper(t.LastName) as lastname, upper(t.address1+ isnull(' ' + t.address2,''))  as  address, upper(t.city) as city,  s.state,   
violationdescription , c.CourtName, t.dpc,   t.clientflag,                                        
 t.dp2 as dptwo, isnull(v.causenumber,v.ticketnumber_pk)  as ticketnumber_pk, t.zipcode ,v.courtdate ,@user as Abb,@lettertype AS LetterType  
 into #temp72
  from  tblletternotes n , dallastraffictickets.dbo.tblticketsarchive t, dallastraffictickets.dbo.tblticketsviolationsarchive v,             
  #letters tt, dallastraffictickets.dbo.tblcourts c  , dallastraffictickets.dbo.tblstate s          
 where  n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and t.courtid = c.courtid            
 and len(isnull(t.lastname,'') + isnull(t.firstname,'') ) > 0             
 and t.Flag1 in ('Y','D','S')            
 and isnull(t.donotmailflag,0)   = 0        
 order by left(t.zipcode, 5), t.midnumber            
 
 select @recCount = count(distinct recordid) from #temp72              
 declare @table72 TABLE  (            
    [letterId] [varchar] (13)  ,          
    [recordid] [int] ,          
    [FirstName] [varchar] (20)  ,          
    [LastName] [varchar] (20)  ,    
	[clientflag] [tinyint],          
    [address] [varchar] (100) ,          
    [city] [varchar] (50) ,          
    [state] [varchar] (2)  ,          
    [violationdescription] [varchar] (200) ,          
    [CourtName] [varchar] (50) ,          
    [dpc] [varchar] (10)  ,          
    [dptwo] [varchar] (10)  ,          
    [TicketNumber_PK] [varchar] (20)  ,          
    [zipcode] [varchar] (15) ,          
    [language] [varchar] (10),           
    [courtdate] [datetime],          
    [Abb] [varchar] (10),
    [LetterType] [int]            
   )            
        
	insert into @table72 (letterid, recordid, firstname, lastname, clientflag, address, city, state,    
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb,LetterType)            
          
   select letterid, recordid, firstname, lastname, clientflag, address, city, state,    
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 0 , courtdate, Abb,LetterType from #temp72             
            
 if @printtype = 1            
 -- DOUBLE SIDE PRINT........            
  begin        
   insert into @table72 (letterid, recordid, firstname, lastname, clientflag, address, city, state,    
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb,LetterType)            
          
   select letterid, recordid, firstname, lastname, clientflag, address, city, state,    
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 1 , courtdate, Abb,LetterType from #temp72 ;     
  end            
     
	select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table72           
   order by [zipcode], [language]            
END 

--Yasir Kamal 7057 11/25/2009 pasadena day of letter added
if @lettertype = 74  
BEGIN            
         
 Select  convert(varchar(20),n.noteid) as letterid, t.recordid, upper( t.FirstName) as firstname,                                          
  upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,'')) as address, upper(t.city) as city, s.state,       
case when isnull(v.fineamount,0)=0 then 100 else v.fineamount end as FineAmount,violationdescription,c.CourtName, t.dpc,                                          
  t.dp2 as dptwo, TicketNumber_PK, t.zipcode , v.courtdate,@user as Abb            
  from tblletternotes n , tblticketsarchive t, tblticketsviolationsarchive v, #letters tt, tblcourts c    , tblstate s        
 where n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and v.courtlocation = c.courtid            
 and t.Flag1 in ('Y','D','S')            
 and t.donotmailflag = 0             
 and t.clientflag = 0 
 and v.violationstatusid <> 80 --Abbas Qamar 9991 01/17/2012 Exclude dispose cases.                      
 order by left(t.zipcode, 5), t.midnumber            

END

-- Sabir Khan 7019 12/18/2009 SUGAR LAND DAY OF MAILER ADDED....
--Sabir Khan 9683 09/15/2011 Need to remove last two character from ticket number and cancatinate -0x  
-- SUGAR LAND DAY OF LETTER......            
if @lettertype = 75            
BEGIN            
 Select  convert(varchar(20),n.noteid) as letterid, t.recordid, upper( t.FirstName) as firstname,                                          
  upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,'')) as address, upper(t.city) as city, s.state,       
  -- Rab Nawaz Khan 9704 09/26/2011 Default fine amount 100 logic is commented. . .  
  -- case when isnull(v.fineamount,0)=0 then 100 else v.fineamount end as FineAmount, 
  isnull(v.fineamount,0) as FineAmount,violationdescription,c.CourtName, t.dpc, 
  t.dp2 as dptwo, SUBSTRING(v.ticketnumber_pk,0,LEN(v.ticketnumber_pk)-1) + '-0x' as TicketNumber_PK, t.zipcode , v.courtdate,@user as Abb            
 INTO #letter75 from tblletternotes n , tblticketsarchive t, tblticketsviolationsarchive v, #letters tt, tblcourts c    , tblstate s        
 where n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and v.courtlocation = c.courtid            
 and t.Flag1 in ('Y','D','S')            
 and t.donotmailflag = 0           
 order by left(t.zipcode, 5), t.midnumber 
 
ALTER TABLE #letter75 ADD isFineAmount BIT NOT NULL DEFAULT 1
SELECT REcordid INTO #sugarlandDay FROM #letter75 WHERE ISNULL(FineAmount,0) = 0

UPDATE T
SET
	isFineAmount = 0
FROM #letter75 t INNER JOIN #sugarlandDay f ON f.recordid = t.recordid

SELECT * FROM #letter75

DROP TABLE #sugarlandDay
DROP TABLE #letter75                     
end 


--Sabir Khan 6995 11/19/2009 FELONY MAILER ADDED....
-- DALLAS COUNTY CRIMINAL BONDED OUT LETTER......            
if @lettertype = 73  
BEGIN            
            
Select  convert(varchar(20),n.noteid) as letterid, t.recordid,   upper(t.FirstName) as firstname,   
 upper(t.LastName) as lastname, upper(t.address1+ isnull(' ' + t.address2,''))  as  address, upper(t.city) as city,  s.state,   
violationdescription , c.CourtName, t.dpc,   t.clientflag,                                        
 t.dp2 as dptwo, isnull(v.causenumber,v.ticketnumber_pk)  as ticketnumber_pk, t.zipcode ,v.courtdate ,@user as Abb, @lettertype AS LetterType 
 into #temp73
  from  tblletternotes n , dallastraffictickets.dbo.tblticketsarchive t, dallastraffictickets.dbo.tblticketsviolationsarchive v,             
  #letters tt, dallastraffictickets.dbo.tblcourts c  , dallastraffictickets.dbo.tblstate s          
 where  n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and t.courtid = c.courtid            
 and len(isnull(t.lastname,'') + isnull(t.firstname,'') ) > 0             
 and t.Flag1 in ('Y','D','S')            
 and isnull(t.donotmailflag,0)   = 0    
 order by left(t.zipcode, 5), t.midnumber            
 
 select @recCount = count(distinct recordid) from #temp73              
 declare @table73 TABLE  (            
    [letterId] [varchar] (13)  ,          
    [recordid] [int] ,          
    [FirstName] [varchar] (20)  ,          
    [LastName] [varchar] (20)  ,    
	[clientflag] [tinyint],          
    [address] [varchar] (100) ,          
    [city] [varchar] (50) ,          
    [state] [varchar] (2)  ,          
    [violationdescription] [varchar] (200) ,          
    [CourtName] [varchar] (50) ,          
    [dpc] [varchar] (10)  ,          
    [dptwo] [varchar] (10)  ,          
    [TicketNumber_PK] [varchar] (20)  ,          
    [zipcode] [varchar] (15) ,          
    [language] [varchar] (10),           
    [courtdate] [datetime],          
    [Abb] [varchar] (10),
    [LetterType] [int]            
   )            
        
	insert into @table73 (letterid, recordid, firstname, lastname, clientflag, address, city, state,    
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb,LetterType)            
          
   select letterid, recordid, firstname, lastname, clientflag, address, city, state,    
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 0 , courtdate, Abb, LetterType from #temp73             
            
 if @printtype = 1            
 -- DOUBLE SIDE PRINT........            
  begin        
   insert into @table73 (letterid, recordid, firstname, lastname, clientflag, address, city, state,    
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb,LetterType)            
          
   select letterid, recordid, firstname, lastname, clientflag, address, city, state,    
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 1 , courtdate, Abb,LetterType from #temp73 ;     
  end            
     
	select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table73           
   order by [zipcode], [language]            
END 


-- 6610 ends 

--Yasir Kamal 7231 01/20/2010 forth worth warrant , Grand prarie letters added

--GRAND PRAIRIE MUNICIPAL COURT Appearance letter
if @lettertype = 77            
begin            
 Select  convert(varchar(20),n.noteid) as letterid, t.recordid,  upper(t.FirstName) as firstname,                                          
 upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,''))  as  address, upper(t.city) as city,  s.state,   
case when isnull(v.fineamount,0)=0 then 100 else v.fineamount end as FineAmount,violationdescription, c.CourtName, t.dpc,                                          
 t.dp2 as dptwo, TicketNumber_PK, t.zipcode , v.CourtDate ,@user as Abb ,
 dbo.GetPromoPriceTemplate(T.zipcode,@lettertype) AS promotemplate                                               
 into #temp77            
 from  tblletternotes n , dallastraffictickets.dbo.tblticketsarchive t, dallastraffictickets.dbo.tblticketsviolationsarchive v,             
  #letters tt, dallastraffictickets.dbo.tblcourts c    , dallastraffictickets.dbo.tblstate s        
 where  n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and t.courtid = c.courtid            
 and t.lastname is not null              
 and t.firstname is not null            
 and t.Flag1 in ('Y','D','S')            
 and isnull(t.donotmailflag,0)   = 0             
 and v.violationbonddate is null     
 order by left(t.zipcode, 5), t.midnumber            
            
            
 select @recCount = count(distinct recordid) from #temp77              
 declare @table77 TABLE  (            
    [letterId] [varchar] (30)   ,            
    [recordid] [int] ,            
    [FirstName] [varchar] (20)  ,            
    [LastName] [varchar] (20)  ,            
    [address] [varchar] (100) ,            
    [city] [varchar] (50) ,            
    [state] [varchar] (2)  ,            
    [FineAmount] [money]  ,            
    [violationdescription] [varchar] (200) ,            
    [CourtName] [varchar] (50) ,            
    [dpc] [varchar] (10)  ,            
    [dptwo] [varchar] (10)  ,            
    [TicketNumber_PK] [varchar] (20)  ,            
    [zipcode] [varchar] (15) ,            
    [language] [int],            
    [courtdate] [datetime],            
    [Abb] [varchar] (10),
    [promotemplate] [varchar] (100)          
   )            
            
 insert into @table77 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
   violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb,promotemplate)              
 select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount,            
   violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 0, courtdate, Abb, promotemplate from #temp77              
            
 if @printtype = 1            
 -- DOUBLE SIDE PRINT........            
  begin            
   insert into @table77 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb, promotemplate)              
   select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount,            
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 1, courtdate, Abb, promotemplate from #temp77 ;             
  end            
         
 if @mailbarry=0          
 begin      
 select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table77             
 union  
 -- Rab Nawaz Khan 10187 04/12/2012 Address, City and Zip Code Changed for the Testing Advertisment mailer which send to Barry     
  select convert(varchar(20),@recCount) + ' letters sent' as letterid  , 0 as recordid , 'BARRY', 'BOBBIT', '7 Butterfly Place', 'Austin', 'TX', 0, 'N/A', 'GRAND PRAIRIE MUNICIPAL COURT', '0', '00' , 'N/A', '78738-1343' as zipcode, 0, '1/1/1900',@user ,'0,35.00', '0'  
      
 end      
 else      
 select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table77             
         
             
            
end            


--GRAND PRAIRIE MUNICIPAL COURT warrant letter
if @lettertype = 78            
begin            
 Select  convert(varchar(20),tt.noteid) as letterid, t.recordid, upper( t.FirstName) as firstname,                                          
  upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,'')) as address, upper(t.city) as city, s.state,       
   case when isnull(v.bondamount,0)=0 then 100 else v.bondamount end as FineAmount,  
 violationdescription, c.CourtName, t.dpc,
 t.dp2 as dptwo, TicketNumber_PK, t.zipcode,@user as Abb,dbo.GetPromoPriceTemplate(T.zipcode,@lettertype) AS promotemplate                                                      
 into #temp78            
 from   dallastraffictickets.dbo.tblticketsarchive t, dallastraffictickets.dbo.tblticketsviolationsarchive v,             
  #letters tt, dallastraffictickets.dbo.tblcourts c  , dallastraffictickets.dbo.tblstate s          
 where  tt.recordid = t.recordid            
     
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and t.courtid = c.courtid            
 and t.Flag1 in ('Y','D','S')            
 and t.donotmailflag = 0             
 and len(isnull(t.firstname,'')+isnull(t.lastname,''))>0   
 order by left(t.zipcode, 5), t.midnumber            
            
            
 select @recCount = count(distinct recordid) from #temp78              
 declare @table78 TABLE  (            
    [letterId] [varchar] (30)   ,            
    [recordid] [int] ,            
    [FirstName] [varchar] (20)  ,            
    [LastName] [varchar] (20)  ,            
    [address] [varchar] (100) ,            
    [city] [varchar] (50) ,            
    [state] [varchar] (2)  ,         
    [FineAmount] [money]  ,            
    [violationdescription] [varchar] (200) ,            
    [CourtName] [varchar] (50) ,            
    [dpc] [varchar] (10)  ,            
    [dptwo] [varchar] (10)  ,            
    [TicketNumber_PK] [varchar] (20)  ,            
    [zipcode] [varchar] (15) ,            
    [language] [int],            
    [Abb] [varchar] (10),
    promotemplate [varchar] (100)                     
   )            

 insert into @table78 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
   violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, Abb,promotemplate)              
 select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount,            
   violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 0, Abb,promotemplate from #temp78              
            
 if @printtype = 1            
 -- DOUBLE SIDE PRINT........            
  begin            
   insert into @table78 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, Abb,promotemplate)              
   select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount,            
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 1, Abb,promotemplate from #temp78 ;             
  end         
       
        
 if @mailbarry=0         
 begin      
 select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table78           
    
 union  
 -- Rab Nawaz Khan 10187 04/12/2012 Address, City and Zip Code Changed for the Testing Advertisment mailer which send to Barry     
 select convert(varchar(20),@recCount) + ' letters sent' as letterid  , 0 as recordid , 'BARRY', 'BOBBIT', '7 Butterfly Place', 'Austin', 'TX', 0, 'N/A', 'GRAND PRAIRIE MUNICIPAL COURT', '0', '00' , 'N/A', '78738-1343' as zipcode, 0,@user,'0,35','0'      
 end      
 else      
    select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table78           
  
end            

--Yasir Kamla 7391 02/10/2010 Grand Prairie Appearance Day of letter
-- Grand Prairie APPEARANCE DAY OF LETTER  
if @lettertype = 79            
begin            
 Select  convert(varchar(20),n.noteid) as letterid, t.recordid,   upper(t.FirstName) as firstname,                                          
 upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,''))  as  address, upper(t.city) as city,  s.state,   
case when isnull(v.fineamount,0)=0 then 100 else v.fineamount end as FineAmount,violationdescription, c.CourtName, t.dpc,                                          
 t.dp2 as dptwo, TicketNumber_PK, t.zipcode , v.courtdate as courtdate ,@user as Abb,dbo.GetPromoPriceTemplate(T.zipcode,@lettertype) AS promotemplate                                                             
 into #temp79            
 from  tblletternotes n , dallastraffictickets.dbo.tblticketsarchive t, dallastraffictickets.dbo.tblticketsviolationsarchive v,             
  #letters tt, dallastraffictickets.dbo.tblcourts c, dallastraffictickets.dbo.tblstate s            
 where  n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and t.courtid = c.courtid            
 and t.lastname is not null              
 and t.firstname is not null            
 and t.Flag1 in ('Y','D','S')            
 and isnull(t.donotmailflag,0)   = 0             
--and v.violationbonddate is null         
--and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP)    
 order by left(t.zipcode, 5), t.midnumber            
            
            
 select @recCount = count(distinct recordid) from #temp79              
 declare @table79 TABLE  (            
    [letterId] [varchar] (30)   ,            
    [recordid] [int] ,            
    [FirstName] [varchar] (20)  ,            
    [LastName] [varchar] (20)  ,            
    [address] [varchar] (100) ,            
    [city] [varchar] (50) ,            
    [state] [varchar] (2)  ,            
    [FineAmount] [money]  ,            
    [violationdescription] [varchar] (200) ,            
    [CourtName] [varchar] (50) ,            
    [dpc] [varchar] (10)  ,            
    [dptwo] [varchar] (10)  ,            
    [TicketNumber_PK] [varchar] (20)  ,            
    [zipcode] [varchar] (15) ,            
    [language] [int],            
    [courtdate] [datetime],            
    [Abb] [varchar] (10),
    [promotemplate] [varchar] (100)                 
   )            
            
 insert into @table79 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
   violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb,promotemplate)              
 select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount,            
   violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 0, courtdate, Abb,promotemplate from #temp79              
            
 if @printtype = 1            
 -- DOUBLE SIDE PRINT........            
  begin            
   insert into @table79 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb,promotemplate)              
   select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount,            
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 1, courtdate, Abb,promotemplate from #temp79 ;            
  end            
       
 if @mailbarry=0        
 begin             
 select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table79       
 union 
 -- Rab Nawaz Khan 10187 04/12/2012 Address, City and Zip Code Changed for the Testing Advertisment mailer which send to Barry       
 select convert(varchar(20),@recCount) + ' letters sent' as letterid  , 0 as recordid , 'BARRY', 'BOBBIT', '7 Butterfly Place', 'Austin', 'TX', 0, 'N/A', 'GRAND PRAIRIE MUNICIPAL COURT', '0', '00' , 'N/A', '78738-1343' as zipcode, 0, '1/1/1900', @user ,'0,75.00', '0'   
  
    
            
 end      
 else      
 select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table79                      
             
            
end

-- tahir 7231 01/11/2010 
-- FORT WORTH WARRANT LETTER
-- Noufil 8199 08/23/2010 FORT WORTH DLQ LETTER clause added
if @lettertype in (76,86)
begin            
 Select  convert(varchar(20),tt.noteid) as letterid, t.recordid, upper( t.FirstName) as firstname,                                          
  upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,'')) as address, upper(t.city) as city, s.state,       
   case when isnull(v.bondamount,0)=0 then 100 else v.bondamount end as FineAmount,  
 violationdescription, c.CourtName, t.dpc,                                          
 t.dp2 as dptwo, TicketNumber_PK, t.zipcode,@user as Abb,dbo.GetPromoPriceTemplate(T.zipcode,@lettertype) AS promotemplate                                                
 into #temp76            
-- from  tblletternotes n , dallastraffictickets.dbo.tblticketsarchive t, dallastraffictickets.dbo.tblticketsviolationsarchive v,             
 from   dallastraffictickets.dbo.tblticketsarchive t, dallastraffictickets.dbo.tblticketsviolationsarchive v,             
  #letters tt, dallastraffictickets.dbo.tblcourts c  , dallastraffictickets.dbo.tblstate s          
 where  tt.recordid = t.recordid            
 --and n.noteid=tt.noteid              
      
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and t.courtid = c.courtid            
 and t.Flag1 in ('Y','D','S')            
 and t.donotmailflag = 0             
 and len(isnull(t.firstname,'')+isnull(t.lastname,''))>0   
--and (t.ncoa18flag = 1 or t.dpv_updated = 1) -- Commented by Sarim (print only those records that was updated by ACCUZIP) 
-- Rab Nawaz Khan 10516 10/25/2012 Exclude the ordinance cases and juvenile cases . . . 
AND (SUBSTRING(ISNULL(v.CauseNumber, 'TaskO_10516'), 5, 1) <> 'O' AND SUBSTRING(ISNULL(v.CauseNumber, 'TaskO_10516'), 5, 1) <> 'M')
order by left(t.zipcode, 5), t.midnumber            
            
            
 select @recCount = count(distinct recordid) from #temp76             
 declare @table76 TABLE  (            
    [letterId] [varchar] (30)   ,            
    [recordid] [int] ,            
    [FirstName] [varchar] (20)  ,            
    [LastName] [varchar] (20)  ,            
    [address] [varchar] (100) ,            
    [city] [varchar] (50) ,            
    [state] [varchar] (2)  ,         
    [FineAmount] [money]  ,            
    [violationdescription] [varchar] (200) ,            
    [CourtName] [varchar] (50) ,            
    [dpc] [varchar] (10)  ,            
    [dptwo] [varchar] (10)  ,            
    [TicketNumber_PK] [varchar] (20)  ,            
    [zipcode] [varchar] (15) ,            
    [language] [int],            
    [Abb] [varchar] (10),
    [promotemplate] [varchar] (100)            
   )            
     
   
  
 insert into @table76 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
   violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, Abb,promotemplate)              
 select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount,            
   violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 0, Abb,promotemplate from #temp76              
            
 if @printtype = 1            
 -- DOUBLE SIDE PRINT........            
  begin            
   insert into @table76 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, Abb,promotemplate)              
   select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount,            
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 1, Abb,promotemplate from #temp76 ;             
  end         
      
   
        
 if @mailbarry=0         
 begin      
 select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table76           
    --order by left(t.zipcode, 5), t.midnumber               
 union 
 -- Rab Nawaz Khan 10187 04/12/2012 Address, City and Zip Code Changed for the Testing Advertisment mailer which send to Barry      
 select convert(varchar(20),@recCount) + ' letters sent' as letterid  , 0 as recordid , 'BARRY', 'BOBBIT', '7 Butterfly Place', 'Austin', 'TX', 0, 'N/A', 'FORT WORTH MUNICIPAL COURT', '0', '00' , 'N/A', '78738-1343' as zipcode, 0,@user,'0,35.00', '0'      
 end      
 else      
    select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table76           
    --order by left(t.zipcode, 5), t.midnumber                
             
            
end         
--7231 end
--Yasir Kamal 7411 03/05/2010 HMC Past arraginment letter
if @lettertype = 80  
BEGIN            
         
 Select  convert(varchar(20),n.noteid) as letterid, t.recordid, upper( t.FirstName) as firstname,                                          
  upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,'')) as address, upper(t.city) as city, s.state,       
case when isnull(v.fineamount,0)=0 then 100 else v.fineamount end as FineAmount,violationdescription,c.CourtName, t.dpc,                                          
  t.dp2 as dptwo, TicketNumber_PK, t.zipcode , v.courtdate,@user as Abb            
  from tblletternotes n , tblticketsarchive t, tblticketsviolationsarchive v, #letters tt, tblcourts c    , tblstate s        
 where n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and v.courtlocation = c.courtid            
 and t.Flag1 in ('Y','D','S')            
 and t.donotmailflag = 0             
 and t.clientflag = 0 
 and v.violationstatusid <> 80 --Abbas Qamar 9991 01/17/2012 Exclude dispose cases.                      
 order by left(t.zipcode, 5), t.midnumber            

END
--Sabir Khan 7522 03/11/2010 Stafford Day Of Mailer
if @lettertype = 81 
BEGIN            
         
 Select  convert(varchar(20),n.noteid) as letterid, t.recordid, upper( t.FirstName) as firstname,                                          
  upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,'')) as address, upper(t.city) as city, s.state,       
-- Rab Nawaz Khan 9704 09/26/2011 Default fine amount 100 logic is commented. . .  
-- case when isnull(v.fineamount,0)=0 then 100 else v.fineamount end as FineAmount,
isnull(v.fineamount,0) as FineAmount,violationdescription,c.CourtName, t.dpc,                                         
  t.dp2 as dptwo, TicketNumber_PK, t.zipcode , v.courtdate,@user as Abb            
 INTO #letter81 from tblletternotes n , tblticketsarchive t, tblticketsviolationsarchive v, #letters tt, tblcourts c    , tblstate s        
 where n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and v.courtlocation = c.courtid            
 and t.Flag1 in ('Y','D','S')            
 and t.donotmailflag = 0  
 and v.violationstatusid <> 80 --Abbas Qamar 9991 01/17/2012 Exclude dispose cases.                      
 and t.clientflag = 0            
 order by left(t.zipcode, 5), t.midnumber  
 
 ALTER TABLE #letter81 ADD isFineAmount BIT NOT NULL DEFAULT 1
SELECT REcordid INTO #stafford FROM #letter81 WHERE ISNULL(FineAmount,0) = 0

UPDATE T
SET
	isFineAmount = 0
FROM #letter81 t INNER JOIN #stafford f ON f.recordid = t.recordid

SELECT * FROM #letter81

DROP TABLE #stafford
DROP TABLE #letter81          

END

--7231 end
--Asad Ali 7702 04/16/2010 HCJP Past arraginment letter
if @lettertype = 83  
BEGIN            
         
 Select  convert(varchar(20),n.noteid) as letterid, t.recordid, upper( t.FirstName) as firstname,                                          
  upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,'')) as address, upper(t.city) as city, s.state,       
 -- Rab Nawaz Khan 9704 09/26/2011 Default fine amount 100 logic is commented. . .  
 -- case when isnull(v.fineamount,0)=0 then 100 else v.fineamount end as FineAmount, 
 isnull(v.fineamount,0) as FineAmount,violationdescription,c.CourtName, t.dpc,                                          
  t.dp2 as dptwo, TicketNumber_PK, t.zipcode , v.courtdate,@user as Abb            
 INTO #letter83 from tblletternotes n , tblticketsarchive t, tblticketsviolationsarchive v, #letters tt, tblcourts c    , tblstate s        
 where n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and v.courtlocation = c.courtid            
 and t.Flag1 in ('Y','D','S')            
 and t.donotmailflag = 0             
 and t.clientflag = 0            
 order by left(t.zipcode, 5), t.midnumber            

ALTER TABLE #letter83 ADD isFineAmount BIT NOT NULL DEFAULT 1
SELECT REcordid INTO #HcjpPast FROM #letter83 WHERE ISNULL(FineAmount,0) = 0

UPDATE T
SET
	isFineAmount = 0
FROM #letter83 t INNER JOIN #HcjpPast f ON f.recordid = t.recordid

SELECT * FROM #letter83

DROP TABLE #HcjpPast
DROP TABLE #letter83

END

--Asad Ali 8176 08/30/2010 sugarland Pre Trial
if @lettertype = 85  
BEGIN            
--Sabir Khan 9683 09/15/2011 Need to remove last two character from ticket number and cancatinate -0x  
 Select  convert(varchar(20),n.noteid) as letterid, t.recordid, upper( t.FirstName) as firstname,                                          
  upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,'')) as address, upper(t.city) as city, s.state,       
-- Rab Nawaz Khan 9704 09/26/2011 Default fine amount 100 logic is commented. . .  
-- case when isnull(v.fineamount,0)=0 then 100 else v.fineamount end as FineAmount,violationdescription,c.CourtName, t.dpc, 
isnull(v.fineamount,0) as FineAmount,violationdescription,c.CourtName, t.dpc, 
  t.dp2 as dptwo, 
  --Asad Ali 8227 08/30/2010 showing cause number in Mailer and ticket number in batch print now it showing on both places with same logic
  case when len(isnull(causenumber,''''))>0 then causenumber else SUBSTRING(v.ticketnumber_pk,0,LEN(v.ticketnumber_pk)-1) + '-0x'  end as TicketNumber_PK, 
  t.zipcode , v.courtdate,@user as Abb            
INTO #letter85  from tblletternotes n , tblticketsarchive t, tblticketsviolationsarchive v, #letters tt, tblcourts c    , tblstate s        
 where n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and v.courtlocation = c.courtid            
 and t.Flag1 in ('Y','D','S')            
 and t.donotmailflag = 0             
 and t.clientflag = 0            
 order by left(t.zipcode, 5), t.midnumber            

ALTER TABLE #letter85 ADD isFineAmount BIT NOT NULL DEFAULT 1
SELECT REcordid INTO #sugerlandPreT FROM #letter85 WHERE ISNULL(FineAmount,0) = 0

UPDATE T
SET
	isFineAmount = 0
FROM #letter85 t INNER JOIN #sugerlandPreT f ON f.recordid = t.recordid

SELECT * FROM #letter85

DROP TABLE #sugerlandPreT
DROP TABLE #letter85

END  

--Asad Ali 8176 08/30/2010 sugarland Jury Trial
if @lettertype = 84  
BEGIN            
 --Sabir Khan 9683 09/15/2011 Need to remove last two character from ticket number and cancatinate -0x        
 Select  convert(varchar(20),n.noteid) as letterid, t.recordid, upper( t.FirstName) as firstname,                                          
  upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,'')) as address, upper(t.city) as city, s.state,       
-- Rab Nawaz Khan 9704 09/26/2011 Default fine amount 100 logic is commented. . .    
-- case when isnull(v.fineamount,0)=0 then 100 else v.fineamount end as FineAmount, 
isnull(v.fineamount,0) as FineAmount,violationdescription,c.CourtName, t.dpc, 
  t.dp2 as dptwo,  --Asad Ali 8227 08/30/2010 showing cause number in Mailer and ticket number in batch print now it showing on both places with same logic
  case when len(isnull(causenumber,''''))>0 then causenumber else SUBSTRING(v.ticketnumber_pk,0,LEN(v.ticketnumber_pk)-1) + '-0x' end as TicketNumber_PK, t.zipcode , v.courtdate,@user as Abb            
 INTO #letter84 from tblletternotes n , tblticketsarchive t, tblticketsviolationsarchive v, #letters tt, tblcourts c    , tblstate s        
 where n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and v.courtlocation = c.courtid            
 and t.Flag1 in ('Y','D','S')            
 and t.donotmailflag = 0             
 and t.clientflag = 0            
 order by left(t.zipcode, 5), t.midnumber            

ALTER TABLE #letter84 ADD isFineAmount BIT NOT NULL DEFAULT 1
SELECT REcordid INTO #sugarlandJury FROM #letter84 WHERE ISNULL(FineAmount,0) = 0

UPDATE T
SET
	isFineAmount = 0
FROM #letter84 t INNER JOIN #sugarlandJury f ON f.recordid = t.recordid

SELECT * FROM #letter84

DROP TABLE #sugarlandJury
DROP TABLE #letter84

END  

-- Babar Ahmad 9699 09/27/2011 Added Jersey Village Municiple Court.
if @lettertype = 126            
begin            
 Select  convert(varchar(20),n.noteid) as letterid, t.recordid,  upper(t.FirstName) as firstname,                                          
  upper(t.LastName) as lastname , upper(t.address1+ isnull(t.address2,'')) as address,upper(t.city) as city, s.state,             
  -- Rab Nawaz Khan 9704 10/27/2011 Default fine amount 100 logic is commented. . .   
 -- case when isnull(v.fineamount,0) = 0 then 100 else v.fineamount end as FineAmount,
 isnull(v.fineamount,0) as FineAmount,
 violationdescription,c.CourtName, t.dpc,                                          
  t.dp2 as dptwo, TicketNumber_PK, t.zipcode,@user as Abb  
 , dbo.GetPromoPriceTemplate(T.zipcode,@lettertype) AS promotemplate,
 -- Rab Nawaz 9954 12/24/2011 Jersey Village Mailer added in the Global Template mailer of HMC 
  'APPEARANCE' AS lettername, '1/1/1900' AS courtdate, 3034 AS courtid, 0 AS zipmid, 0 as midnumber, 0 as midnum
 INTO #letter126 
  from tblletternotes n , tblticketsarchive t, tblticketsviolationsarchive v, #letters tt, tblcourts c, tblcourtviolationstatus tcvs, tblstate s        
 where n.noteid=tt.noteid          
 and t.stateid_fk = s.stateid        
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.courtid = c.courtid            
 and tcvs.courtviolationstatusid = v.violationstatusid            
 and tcvs.CategoryID=2               
 and t.Flag1 in ('Y','D','S')            
 and t.donotmailflag = 0             
 and t.clientflag=0 
and len(isnull(v.attorneyname, '')+isnull(v.barcardnumber,'')) = 0 
and CHARINDEX('KUBOSH',isnull(v.BondingCompanyName,''))=0           
order by left(t.zipcode, 5), t.midnumber            

ALTER TABLE #letter126 ADD isFineAmount BIT NOT NULL DEFAULT 1
SELECT REcordid INTO #JerseyV FROM #letter126 WHERE ISNULL(FineAmount,0) = 0

UPDATE T
SET
	isFineAmount = 0
FROM #letter126 t INNER JOIN #JerseyV f ON f.recordid = t.recordid

SELECT * FROM #letter126


DROP TABLE #JerseyV
DROP TABLE #letter126

end  


--Muhammad Ali 8334 09/28/2010 Add track batch logic for PLANO COURT  apperance, apperance day of, warrant..
--PLANO COURT Appearance letter
if @lettertype = 87            
begin            
 Select  convert(varchar(20),n.noteid) as letterid, t.recordid,  upper(t.FirstName) as firstname,                                          
 upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,''))  as  address, upper(t.city) as city,  s.state,   
case when isnull(v.fineamount,0)=0 then 100 else v.fineamount end as FineAmount,violationdescription, c.CourtName, t.dpc,                                          
 t.dp2 as dptwo, TicketNumber_PK, t.zipcode , v.CourtDate ,@user as Abb ,
 dbo.GetPromoPriceTemplate(T.zipcode,@lettertype) AS promotemplate                                               
 into #temp87            
 from  tblletternotes n , dallastraffictickets.dbo.tblticketsarchive t, dallastraffictickets.dbo.tblticketsviolationsarchive v,             
  #letters tt, dallastraffictickets.dbo.tblcourts c    , dallastraffictickets.dbo.tblstate s        
 where  n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and t.courtid = c.courtid            
 and t.lastname is not null              
 and t.firstname is not null            
 and t.Flag1 in ('Y','D','S')            
 and isnull(t.donotmailflag,0)   = 0             
 and v.violationbonddate is null     
 order by left(t.zipcode, 5), t.midnumber            
            
            
 select @recCount = count(distinct recordid) from #temp87              
 declare @table87 TABLE  (            
    [letterId] [varchar] (30)   ,            
    [recordid] [int] ,            
    [FirstName] [varchar] (20)  ,            
    [LastName] [varchar] (20)  ,            
    [address] [varchar] (100) ,            
    [city] [varchar] (50) ,            
    [state] [varchar] (2)  ,            
    [FineAmount] [money]  ,            
    [violationdescription] [varchar] (200) ,            
    [CourtName] [varchar] (50) ,            
    [dpc] [varchar] (10)  ,            
    [dptwo] [varchar] (10)  ,            
    [TicketNumber_PK] [varchar] (20)  ,            
    [zipcode] [varchar] (15) ,            
    [language] [int],            
    [courtdate] [datetime],            
    [Abb] [varchar] (10),
    [promotemplate] [varchar] (100)          
   )            
            
 insert into @table87 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
   violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb,promotemplate)              
 select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount,            
   violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 0, courtdate, Abb, promotemplate from #temp87              
          
         
 if @mailbarry=0          
 begin      
 select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table87             
 union  
 -- Rab Nawaz Khan 10187 04/12/2012 Address, City and Zip Code Changed for the Testing Advertisment mailer which send to Barry     
  select convert(varchar(20),@recCount) + ' letters sent' as letterid  , 0 as recordid , 'BARRY', 'BOBBIT', '7 Butterfly Place', 'Austin', 'TX', 0, 'N/A', 'PLANO MUNICIPAL COURT', '0', '00' , 'N/A', '78738-1343' as zipcode, 0, '1/1/1900',@user ,'0,60.00', '0'  
      
 end      
 else      
 select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table87             
         
             
            
end            
--PLANO MUNICIPAL COURT warrant letter
if @lettertype = 88            
begin            
 Select  convert(varchar(20),tt.noteid) as letterid, t.recordid, upper( t.FirstName) as firstname,                                          
  upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,'')) as address, upper(t.city) as city, s.state,       
   case when isnull(v.bondamount,0)=0 then 100 else v.bondamount end as FineAmount,  
 violationdescription, c.CourtName, t.dpc,
 t.dp2 as dptwo, TicketNumber_PK, t.zipcode,@user as Abb,dbo.GetPromoPriceTemplate(T.zipcode,@lettertype) AS promotemplate                                                      
 into #temp88            
 from   dallastraffictickets.dbo.tblticketsarchive t, dallastraffictickets.dbo.tblticketsviolationsarchive v,             
  #letters tt, dallastraffictickets.dbo.tblcourts c  , dallastraffictickets.dbo.tblstate s          
 where  tt.recordid = t.recordid            
     
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and t.courtid = c.courtid            
 and t.Flag1 in ('Y','D','S')            
 and t.donotmailflag = 0             
 and len(isnull(t.firstname,'')+isnull(t.lastname,''))>0   
 order by left(t.zipcode, 5), t.midnumber            
            
            
 select @recCount = count(distinct recordid) from #temp88              
 declare @table88 TABLE  (            
    [letterId] [varchar] (30)   ,            
    [recordid] [int] ,            
    [FirstName] [varchar] (20)  ,            
    [LastName] [varchar] (20)  ,            
    [address] [varchar] (100) ,            
    [city] [varchar] (50) ,            
    [state] [varchar] (2)  ,         
    [FineAmount] [money]  ,            
    [violationdescription] [varchar] (200) ,            
    [CourtName] [varchar] (50) ,            
    [dpc] [varchar] (10)  ,            
    [dptwo] [varchar] (10)  ,            
    [TicketNumber_PK] [varchar] (20)  ,            
    [zipcode] [varchar] (15) ,            
    [language] [int],            
    [Abb] [varchar] (10),
    promotemplate [varchar] (100)                     
   )            

 insert into @table88 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
   violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, Abb,promotemplate)              
 select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount,            
   violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 0, Abb,promotemplate from #temp88              
          
 if @mailbarry=0         
 begin      
 select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table88           
    
 union
 -- Rab Nawaz Khan 10187 04/12/2012 Address, City and Zip Code Changed for the Testing Advertisment mailer which send to Barry       
 select convert(varchar(20),@recCount) + ' letters sent' as letterid  , 0 as recordid , 'BARRY', 'BOBBIT', '7 Butterfly Place', 'Austin', 'TX', 0, 'N/A', 'PLANO MUNICIPAL COURT', '0', '00' , 'N/A', '78738-1343' as zipcode, 0,@user,'0,60','0'      
 end      
 else      
    select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table88           
  
end            

--Plano Appearance Day of letter
  
if @lettertype = 89            
begin            
 Select  convert(varchar(20),n.noteid) as letterid, t.recordid,   upper(t.FirstName) as firstname,                                          
 upper(t.LastName) as lastname, upper(t.address1+ isnull(t.address2,''))  as  address, upper(t.city) as city,  s.state,   
case when isnull(v.fineamount,0)=0 then 100 else v.fineamount end as FineAmount,violationdescription, c.CourtName, t.dpc,                                          
 t.dp2 as dptwo, TicketNumber_PK, t.zipcode , v.courtdate as courtdate ,@user as Abb,dbo.GetPromoPriceTemplate(T.zipcode,@lettertype) AS promotemplate                                                             
 into #temp89            
 from  tblletternotes n , dallastraffictickets.dbo.tblticketsarchive t, dallastraffictickets.dbo.tblticketsviolationsarchive v,             
  #letters tt, dallastraffictickets.dbo.tblcourts c, dallastraffictickets.dbo.tblstate s            
 where  n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and t.courtid = c.courtid            
 and t.lastname is not null              
 and t.firstname is not null            
 and t.Flag1 in ('Y','D','S')            
 and isnull(t.donotmailflag,0)   = 0               
 order by left(t.zipcode, 5), t.midnumber            
            
            
 select @recCount = count(distinct recordid) from #temp89              
 declare @table89 TABLE  (            
    [letterId] [varchar] (30)   ,            
    [recordid] [int] ,            
    [FirstName] [varchar] (20)  ,            
    [LastName] [varchar] (20)  ,            
    [address] [varchar] (100) ,            
    [city] [varchar] (50) ,            
    [state] [varchar] (2)  ,            
    [FineAmount] [money]  ,            
    [violationdescription] [varchar] (200) ,            
    [CourtName] [varchar] (50) ,            
    [dpc] [varchar] (10)  ,            
    [dptwo] [varchar] (10)  ,            
    [TicketNumber_PK] [varchar] (20)  ,            
    [zipcode] [varchar] (15) ,            
    [language] [int],            
    [courtdate] [datetime],            
    [Abb] [varchar] (10),
    [promotemplate] [varchar] (100)                 
   )            
            
 insert into @table89 (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
   violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb,promotemplate)              
 select convert(varchar(30),letterid), recordid, firstname, lastname, address, city, state, fineamount,            
   violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 0, courtdate, Abb,promotemplate from #temp89              
            
           
       
 if @mailbarry=0        
 begin             
 select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table89       
 union 
 -- Rab Nawaz Khan 10187 04/12/2012 Address, City and Zip Code Changed for the Testing Advertisment mailer which send to Barry       
 select convert(varchar(20),@recCount) + ' letters sent' as letterid  , 0 as recordid , 'BARRY', 'BOBBIT', '7 Butterfly Place', 'Austin', 'TX', 0, 'N/A', 'GRAND PRAIRIE MUNICIPAL COURT', '0', '00' , 'N/A', '78738-1343' as zipcode, 0, '1/1/1900', @user ,'0,60.00', '0'   
  
    
            
 end      
 else      
 select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table89                      
             
            
end

-- END 8334  




--Zeeshan 10304 08/09/2012
-- TARRANT COUNTY CRIMINAL BOOKING LIST...
if @lettertype = 128  
BEGIN            
            
Select  convert(varchar(20),n.noteid) as letterid, t.recordid,   upper(t.FirstName) as firstname,   
 upper(t.LastName) as lastname, upper(t.address1+ isnull(' ' + t.address2,''))  as  address, upper(t.city) as city,  s.state,   
violationdescription , c.CourtName, t.dpc,   t.clientflag,                                        
 t.dp2 as dptwo, isnull(v.causenumber,v.ticketnumber_pk)  as ticketnumber_pk, t.zipcode ,v.courtdate ,@user as Abb, @lettertype AS LetterType 
 into #temp128
  from  tblletternotes n , dallastraffictickets.dbo.tblticketsarchive t, dallastraffictickets.dbo.tblticketsviolationsarchive v,             
  #letters tt, dallastraffictickets.dbo.tblcourts c  , dallastraffictickets.dbo.tblstate s          
 where  n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and t.courtid = c.courtid            
 and len(isnull(t.lastname,'') + isnull(t.firstname,'') ) > 0             
 and t.Flag1 in ('Y','D','S')            
 and isnull(t.donotmailflag,0)   = 0    
 order by left(t.zipcode, 5), t.midnumber            
 
 select @recCount = count(distinct recordid) from #temp128              
 declare @table128 TABLE  (            
    [letterId] [varchar] (13)  ,          
    [recordid] [int] ,          
    [FirstName] [varchar] (20)  ,          
    [LastName] [varchar] (20)  ,    
	[clientflag] [tinyint],          
    [address] [varchar] (100) ,          
    [city] [varchar] (50) ,          
    [state] [varchar] (2)  ,          
    [violationdescription] [varchar] (200) ,          
    [CourtName] [varchar] (50) ,          
    [dpc] [varchar] (10)  ,          
    [dptwo] [varchar] (10)  ,          
    [TicketNumber_PK] [varchar] (20)  ,          
    [zipcode] [varchar] (15) ,          
    [language] [varchar] (10),           
    [courtdate] [datetime],          
    [Abb] [varchar] (10),
    [LetterType] [int]            
   )            
        
	insert into @table128 (letterid, recordid, firstname, lastname, clientflag, address, city, state,    
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb,LetterType)            
          
   select letterid, recordid, firstname, lastname, clientflag, address, city, state,    
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 0 , courtdate, Abb, LetterType from #temp128             
            
	select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table128           
   order by [zipcode], [language]            
END 



--Zeeshan 10304 08/09/2012
-- TARRANT COUNTY CRIMINAL BONDED OUT...
if @lettertype = 129  
BEGIN            
            
Select  convert(varchar(20),n.noteid) as letterid, t.recordid,   upper(t.FirstName) as firstname,   
 upper(t.LastName) as lastname, upper(t.address1+ isnull(' ' + t.address2,''))  as  address, upper(t.city) as city,  s.state,   
violationdescription , c.CourtName, t.dpc,   t.clientflag,                                        
 t.dp2 as dptwo, isnull(v.causenumber,v.ticketnumber_pk)  as ticketnumber_pk, t.zipcode ,v.courtdate ,@user as Abb, @lettertype AS LetterType 
 into #temp129
  from  tblletternotes n , dallastraffictickets.dbo.tblticketsarchive t, dallastraffictickets.dbo.tblticketsviolationsarchive v,             
  #letters tt, dallastraffictickets.dbo.tblcourts c  , dallastraffictickets.dbo.tblstate s          
 where  n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and t.courtid = c.courtid            
 and len(isnull(t.lastname,'') + isnull(t.firstname,'') ) > 0             
 and t.Flag1 in ('Y','D','S')            
 and isnull(t.donotmailflag,0)   = 0    
 order by left(t.zipcode, 5), t.midnumber            
 
 select @recCount = count(distinct recordid) from #temp129              
 declare @table129 TABLE  (            
    [letterId] [varchar] (13)  ,          
    [recordid] [int] ,          
    [FirstName] [varchar] (20)  ,          
    [LastName] [varchar] (20)  ,    
	[clientflag] [tinyint],          
    [address] [varchar] (100) ,          
    [city] [varchar] (50) ,          
    [state] [varchar] (2)  ,          
    [violationdescription] [varchar] (200) ,          
    [CourtName] [varchar] (50) ,          
    [dpc] [varchar] (10)  ,          
    [dptwo] [varchar] (10)  ,          
    [TicketNumber_PK] [varchar] (20)  ,          
    [zipcode] [varchar] (15) ,          
    [language] [varchar] (10),           
    [courtdate] [datetime],          
    [Abb] [varchar] (10),
    [LetterType] [int]            
   )            
        
	insert into @table129 (letterid, recordid, firstname, lastname, clientflag, address, city, state,    
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb,LetterType)            
          
   select letterid, recordid, firstname, lastname, clientflag, address, city, state,    
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 0 , courtdate, Abb, LetterType from #temp129             
            
	select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table129           
   order by [zipcode], [language]            
END 

--Farrukh 10431 09/13/2012 Added Tarrant County Criminal First Setting
if @lettertype = 132  
BEGIN            
            
Select  convert(varchar(20),n.noteid) as letterid, t.recordid,   upper(t.FirstName) as firstname,   
 upper(t.LastName) as lastname, upper(t.address1+ isnull(' ' + t.address2,''))  as  address, upper(t.city) as city,  s.state,   
violationdescription , c.CourtName, t.dpc,   t.clientflag,                                        
 t.dp2 as dptwo, isnull(v.causenumber,v.ticketnumber_pk)  as ticketnumber_pk, t.zipcode ,v.courtdate ,@user as Abb, @lettertype AS LetterType 
 into #temp132
  from  tblletternotes n , dallastraffictickets.dbo.tblticketsarchive t, dallastraffictickets.dbo.tblticketsviolationsarchive v,             
  #letters tt, dallastraffictickets.dbo.tblcourts c  , dallastraffictickets.dbo.tblstate s          
 where  n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and t.courtid = c.courtid            
 and len(isnull(t.lastname,'') + isnull(t.firstname,'') ) > 0             
 and t.Flag1 in ('Y','D','S')            
 and isnull(t.donotmailflag,0)   = 0    
 order by left(t.zipcode, 5), t.midnumber            
 
 select @recCount = count(distinct recordid) from #temp132              
 declare @table132 TABLE  (            
    [letterId] [varchar] (13)  ,          
    [recordid] [int] ,          
    [FirstName] [varchar] (20)  ,          
    [LastName] [varchar] (20)  ,    
	[clientflag] [tinyint],          
    [address] [varchar] (100) ,          
    [city] [varchar] (50) ,          
    [state] [varchar] (2)  ,          
    [violationdescription] [varchar] (200) ,          
    [CourtName] [varchar] (50) ,          
    [dpc] [varchar] (10)  ,          
    [dptwo] [varchar] (10)  ,          
    [TicketNumber_PK] [varchar] (20)  ,          
    [zipcode] [varchar] (15) ,          
    [language] [varchar] (10),           
    [courtdate] [datetime],          
    [Abb] [varchar] (10),
    [LetterType] [int]            
   )            
        
	insert into @table132 (letterid, recordid, firstname, lastname, clientflag, address, city, state,    
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb,LetterType)            
          
   select letterid, recordid, firstname, lastname, clientflag, address, city, state,    
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 0 , courtdate, Abb, LetterType from #temp132             
            
	select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table132           
   order by [zipcode], [language]            
END 



--Farrukh 10431 09/13/2012 Added Tarrant County Criminal Pass To Hire
if @lettertype = 133  
BEGIN            
            
Select  convert(varchar(20),n.noteid) as letterid, t.recordid,   upper(t.FirstName) as firstname,   
 upper(t.LastName) as lastname, upper(t.address1+ isnull(' ' + t.address2,''))  as  address, upper(t.city) as city,  s.state,   
violationdescription , c.CourtName, t.dpc,   t.clientflag,                                        
 t.dp2 as dptwo, isnull(v.causenumber,v.ticketnumber_pk)  as ticketnumber_pk, t.zipcode ,v.courtdate ,@user as Abb, @lettertype AS LetterType 
 into #temp133
  from  tblletternotes n , dallastraffictickets.dbo.tblticketsarchive t, dallastraffictickets.dbo.tblticketsviolationsarchive v,             
  #letters tt, dallastraffictickets.dbo.tblcourts c  , dallastraffictickets.dbo.tblstate s          
 where  n.noteid=tt.noteid              
 and n.recordid = t.recordid            
 and t.recordid = v.recordid            
 and t.stateid_fk = s.stateid        
 and t.courtid = c.courtid            
 and len(isnull(t.lastname,'') + isnull(t.firstname,'') ) > 0             
 and t.Flag1 in ('Y','D','S')            
 and isnull(t.donotmailflag,0)   = 0    
 order by left(t.zipcode, 5), t.midnumber            
 
 select @recCount = count(distinct recordid) from #temp133              
 declare @table133 TABLE  (            
    [letterId] [varchar] (13)  ,          
    [recordid] [int] ,          
    [FirstName] [varchar] (20)  ,          
    [LastName] [varchar] (20)  ,    
	[clientflag] [tinyint],          
    [address] [varchar] (100) ,          
    [city] [varchar] (50) ,          
    [state] [varchar] (2)  ,          
    [violationdescription] [varchar] (200) ,          
    [CourtName] [varchar] (50) ,          
    [dpc] [varchar] (10)  ,          
    [dptwo] [varchar] (10)  ,          
    [TicketNumber_PK] [varchar] (20)  ,          
    [zipcode] [varchar] (15) ,          
    [language] [varchar] (10),           
    [courtdate] [datetime],          
    [Abb] [varchar] (10),
    [LetterType] [int]            
   )            
        
	insert into @table133 (letterid, recordid, firstname, lastname, clientflag, address, city, state,    
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb,LetterType)            
          
   select letterid, recordid, firstname, lastname, clientflag, address, city, state,    
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 0 , courtdate, Abb, LetterType from #temp133             
            
	select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table133           
   order by [zipcode], [language]            
END


--Farrukh 10595 01/28/2013 Added Austin Municipal Court Letters
IF @lettertype IN (139, 140, 141, 142)
BEGIN            
            
SELECT CONVERT(VARCHAR(20),n.noteid) AS letterid, t.recordid,  UPPER(t.FirstName) AS firstname,                                          
 UPPER(t.LastName) AS lastname, UPPER(t.address1+ ISNULL(t.address2,''))  AS address, UPPER(t.city) AS city,  s.state,   
CASE WHEN ISNULL(v.fineamount,0)=0 THEN 100 ELSE v.fineamount END AS FineAmount,violationdescription, c.CourtName, t.dpc,                                          
 t.dp2 AS dptwo, TicketNumber_PK, t.zipcode , DATEADD(DAY, 21, v.CourtDate) AS courtdate ,@user AS Abb ,
 left(t.zipcode,5) + rtrim(ltrim(t.midnumber)) as zipmid, 
 dbo.GetPromoPriceTemplate(T.zipcode,@lettertype) AS promotemplate                                               
 INTO #tempAustin            
FROM  tblletternotes n , dallastraffictickets.dbo.tblticketsarchive t, dallastraffictickets.dbo.tblticketsviolationsarchive v,             
  #letters tt, dallastraffictickets.dbo.tblcourts c, dallastraffictickets.dbo.tblstate s        
WHERE  n.noteid=tt.noteid              
 AND n.recordid = t.recordid            
 AND t.recordid = v.recordid            
 AND t.stateid_fk = s.stateid        
 AND t.courtid = c.courtid            
 AND ISNULL(t.lastname,'') <> ''
 AND ISNULL(t.firstname,'') <> ''
 AND t.Flag1 IN ('Y','D','S')            
 AND ISNULL(t.donotmailflag,0)   = 0              
ORDER BY LEFT(t.zipcode, 5), t.midnumber            
            
            
 SELECT  @recCount = COUNT(DISTINCT recordid) FROM #tempAustin              
 DECLARE @tableAustin TABLE  (            
    [letterId] [varchar] (30)   ,            
    [recordid] [int] ,            
    [FirstName] [varchar] (20)  ,            
    [LastName] [varchar] (20)  ,            
    [address] [varchar] (100) ,            
    [city] [varchar] (50) ,            
    [state] [varchar] (2)  ,            
    [FineAmount] [money]  ,            
    [violationdescription] [varchar] (200) ,            
    [CourtName] [varchar] (50) ,            
    [dpc] [varchar] (10)  ,            
    [dptwo] [varchar] (10)  ,            
    [TicketNumber_PK] [varchar] (20)  ,            
    [zipcode] [varchar] (15) ,            
    [language] [int],            
    [courtdate] [datetime],            
    [Abb] [varchar] (10),    
    [promotemplate] [varchar] (100),
    [zipmid] [varchar] (100)  
            
   )            
            
 INSERT INTO @tableAustin (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
   violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb,promotemplate,zipmid)              
 SELECT CONVERT(VARCHAR(30),letterid), recordid, firstname, lastname, address, city, state, fineamount,            
   violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 0, courtdate, Abb, promotemplate,zipmid FROM #tempAustin              
            
 IF @printtype = 1            
 -- DOUBLE SIDE PRINT........            
  BEGIN            
   INSERT INTO @tableAustin (letterid, recordid, firstname, lastname, address, city, state, fineamount,            
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb, promotemplate,zipmid)              
   SELECT CONVERT(VARCHAR(30),letterid), recordid, firstname, lastname, address, city, state, fineamount,            
     violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 1, courtdate, Abb, promotemplate,zipmid FROM #tempAustin ;             
  END             
 
 IF @mailbarry=0          
 BEGIN       
	SELECT *, CONVERT(VARCHAR(15), recordid) +  CONVERT(VARCHAR(5),LANGUAGE) AS ChkGroup FROM @tableAustin             
	UNION        
	SELECT CONVERT(VARCHAR(20),@recCount) + ' letters sent' AS letterid  , 0 AS recordid , 'BARRY', 'BOBBIT', '7 Butterfly Place', 'Austin', 'TX', 0, 'N/A', 'AUSTIN MUNICIPAL COURT', '0', '00' , 'N/A', '78738-1343' AS zipcode, 0, '1/1/1900',@user ,'0,35.00', '0'  ,'0'
 END       
 ELSE       
	SELECT *, CONVERT(VARCHAR(15), recordid) +  CONVERT(VARCHAR(5),LANGUAGE) AS ChkGroup FROM @tableAustin             
         
             
                     
END  


if @lettertype = 130  
BEGIN            
            
SELECT  convert(varchar(20),n.noteid) as letterid, t.recordid,  v.CourtDate,  upper(t.FirstName) as firstname,   
 upper(t.LastName) as lastname, upper(t.address1+ isnull(' ' + t.address2,''))  as  address, upper(t.city) as city,  s.state,                                           
  t.zipcode, @user as Abb, t.dp2 as dptwo, t.CourtID,  CONVERT(VARCHAR(20),t.DOB,101) AS DOB,
  DATEDIFF(DAY, ISNULL(t.DOB, '01/01/1900'), GETDATE()) / 365 AS AgeYears, 130 AS LetterType
 into #ImmigrationLettersFonNonClients
  from  tblletternotes n , tblticketsarchive t, tblticketsviolationsarchive v,             
  #letters tt , dallastraffictickets.dbo.tblstate s          
 where  n.noteid=tt.noteid 
 and n.recordid = t.recordid 
 and t.recordid = v.recordid 
 and t.stateid_fk = s.stateid    
 ORDER BY t.zipcode 
 
 SELECT DISTINCT letterid, recordid,  CourtDate,  firstname, lastname, address, city,  state, zipcode, Abb, dptwo, CourtID, AgeYears, DOB, LetterType
 FROM #ImmigrationLettersFonNonClients
 
 DROP TABLE #ImmigrationLettersFonNonClients                   
END 

 --Sabir Khan 7063 12/01/2009 fixed warrant letter bug...
 --drop table #warrants  
 
-- Rab Nawaz Khan 10442 09/26/2012 Sugarland Warrant Mailer . . . 
IF @lettertype = 134
BEGIN
	SELECT DISTINCT CONVERT(VARCHAR(20),n.noteid) AS letterid, t.recordid, UPPER(t.FirstName) AS firstname,                                              
	UPPER(t.LastName) AS lastname, UPPER(t.address1 + ISNULL(t.address2,'')) AS address, UPPER(t.city) AS city, s.state,
	ISNULL(v.fineamount,0) AS FineAmount, v.violationnumber_pk,v.violationdescription,CourtName, t.dpc,                                            
	t.dp2 AS dptwo, TicketNumber_PK, n.zipcode, LEFT(n.zipcode,5) + LTRIM(LTRIM(n.midnumber)) AS zipmid,         
	v.courtdate, t.courtid, n.midnumber, n.midnumber AS midnum ,@user AS Abb                                            
	INTO #SugerlandWarrant        
	FROM tblletternotes n INNER JOIN tblletternotes_detail d ON d.noteid = n.noteid
			INNER JOIN tblticketsarchive t ON d.recordid = t.recordid
			INNER JOIN tblticketsviolationsarchive v ON t.recordid = v.recordid
			INNER JOIN tblcourts c ON v.courtlocation = c.courtid  
			INNER JOIN tblstate s ON t.stateid_fk = s.stateid  
	WHERE n.batchid_fk in (select * from dbo.sap_string_param(@BatchIDs))
	AND v.CourtLocation IN (SELECT t.Courtid FROM tblcourts t WHERE t.courtcategorynum = @catnum)
	AND t.donotmailflag = 0 
	AND t.Flag1 in ('Y','D','S')              	            
	AND v.violationstatusid = 186

   SELECT * FROM #SugerlandWarrant ORDER BY [zipmid] DESC
   
   DROP TABLE #SugerlandWarrant
END
-- END 10442


-- Rab Nawaz Khan 10436 09/27/2012 Baytown Arraignment Mailer . . . 
IF @lettertype = 135
BEGIN
	SELECT DISTINCT CONVERT(VARCHAR(20),n.noteid) AS letterid, t.recordid, v.courtdate, UPPER(t.FirstName) as firstname, 
	UPPER(t.LastName) AS lastname, UPPER(t.address1+ ISNULL(t.address2,'')) AS  address, UPPER(t.city) AS city, s.state, 
	ISNULL(v.fineamount, 0) AS fineamount, violationdescription, t.dpc, t.dp2 AS dptwo, v.ticketnumber_pk, t.zipcode, LEFT(t.zipcode,5) + t.midnumber AS zipmid,
	@user AS Abb, c.CourtName AS CourtName, 'ARRAIGNMENT' AS lettername, 0 AS promotemplate, t.midnumber, t.midnumber AS midnum, t.courtid, CAST(0 AS BIT) AS isSpanish
	INTO #BayttownArrData
	FROM tblletternotes n INNER JOIN tblticketsarchive t ON n.recordid = t.recordid
		INNER JOIN tblticketsviolationsarchive v ON t.recordid = v.recordid 
		INNER JOIN tblcourts c ON t.courtid = c.courtid 
		INNER JOIN tblstate s ON t.stateid_fk = s.stateid
	WHERE t.Flag1 in ('Y','D','S') 
	AND t.donotmailflag = 0 
	AND t.clientflag = 0 
	AND v.violationstatusid = 116
	AND n.LetterType = @lettertype
	AND n.BatchId_Fk IN (select * from dbo.sap_string_param(@batchids) )
	
	 
	ALTER TABLE #BayttownArrData 
	ADD isfineamount BIT NOT NULL DEFAULT 1  
	 
	SELECT recordid INTO #BaytownEmptyfine from #BayttownArrData   WHERE ISNULL(FineAmount,0) = 0  
	 
	UPDATE t   
	SET t.isfineAmount = 0  
	FROM #BayttownArrData t INNER JOIN #BaytownEmptyfine f ON t.recordid = f.recordid
	 
	SELECT letterid, recordid, courtdate, firstname, lastname, address, city, state, fineamount, violationdescription, dpc, 
	dptwo, ticketnumber_pk, zipcode, zipmid, Abb, CourtName, lettername, promotemplate, midnumber, midnum, courtid, isSpanish, isfineamount
	FROM  #BayttownArrData
	ORDER BY zipmid
	
	DROP TABLE #BayttownArrData
	DROP TABLE #BaytownEmptyfine
END       
-- END 10436

-- Pearland Arrignment 1 LETTER...... 
--Rab Nawaz Khan 11108 08/05/2013 Add Pearland Arrignment 1 mailer. . . 
IF @lettertype = 143           
BEGIN            
	 SELECT DISTINCT CONVERT(VARCHAR(20),n.noteid) AS letterid, t.recordid,v.courtdate, UPPER(t.FirstName) AS firstname,                                          
	 UPPER(t.LastName) AS lastname, UPPER(t.address1+ isnull(t.address2,''))  AS  address, UPPER(t.city) AS city,  s.state,   
	 isnull(v.fineamount,0) AS FineAmount, violationdescription, t.dpc, 
	 t.dp2 AS dptwo, v.ticketnumber_pk AS TicketNumber_PK, t.zipcode, LEFT(t.zipcode,5) + t.midnumber AS zipmid ,@user AS Abb,
	 0 AS promotemplate,  t.midnumber, t.MidNumber AS midnum, c.courtid, 'ARRIGNMENT1' AS letterName
	 INTO #lettert143 
	 FROM tblletternotes n INNER JOIN #letters tt ON n.noteid=tt.noteid
		INNER JOIN tblticketsarchive t ON n.recordid = t.recordid
		INNER JOIN tblticketsviolationsarchive v ON t.recordid = v.recordid
		INNER JOIN tblstate s ON t.stateid_fk = s.stateid
		INNER JOIN tblcourtviolationstatus tcvs ON tcvs.courtviolationstatusid = v.violationstatusid
		INNER JOIN tblcourts c ON t.courtid = c.courtid               
	 WHERE t.Flag1 in ('Y','D','S')            
	 AND t.donotmailflag = 0 
	 AND tcvs.CategoryID=2 
	 AND t.clientflag = 0      
	 order by [zipmid]  
	 
	ALTER TABLE #lettert143 ADD isFineAmount BIT NOT NULL DEFAULT 1
	SELECT REcordid INTO #pearland FROM #lettert143 WHERE ISNULL(FineAmount,0) = 0

	UPDATE T
	SET isFineAmount = 0
	FROM #lettert143 t INNER JOIN #pearland f ON f.recordid = t.recordid

	SELECT * FROM #lettert143

	DROP TABLE #pearland
	DROP TABLE #lettert143
END

-- Rab Nawaz Khan 08/05/2013 Pearland DAYB4 ARR. LETTERS            
IF @lettertype = 144           
BEGIN
	SELECT CONVERT(VARCHAR(20),n.noteid) AS letterid, t.recordid, UPPER( t.FirstName) AS firstname,                                          
	UPPER(t.LastName) AS lastname , UPPER(t.address1+ isnull(t.address2,'')) AS address, UPPER(t.city) AS city, s.state,             
	ISNULL(v.fineamount,0) AS FineAmount, violationdescription,c.CourtName, t.dpc, 
	t.dp2 AS dptwo, TicketNumber_PK, t.zipcode, 'DAY BEFORE ARRAIGNMEMT' AS lettername, @user AS Abb, 
	dbo.GetPromoPriceTemplate(T.zipcode,@lettertype) AS promotemplate, t.MidNumber, t.MidNumber AS midnum, t.ZipCode AS zipmid, t.CourtDate, c.Courtid
	INTO #letter144 
	FROM tblletternotes n INNER JOIN #letters tt ON n.noteid=tt.noteid
		INNER JOIN tblticketsarchive t ON n.recordid = t.recordid
		INNER JOIN tblticketsviolationsarchive v ON t.recordid = v.recordid 
		INNER JOIN tblcourts c ON t.courtid = c.courtid 
		INNER JOIN tblstate s ON t.stateid_fk = s.stateid
		INNER JOIN tblcourtviolationstatus tcvs ON tcvs.courtviolationstatusid = v.violationstatusid
	WHERE tcvs.CategoryID = 2               
	AND t.Flag1 IN ('Y','D','S')            
	AND t.donotmailflag = 0             
	AND t.clientflag=0 
	AND len(isnull(v.attorneyname, '')+isnull(v.barcardnumber,'')) = 0 
	AND CHARINDEX('KUBOSH',isnull(v.BondingCompanyName,''))=0 
	ORDER BY LEFT(t.zipcode, 5), t.midnumber                

	ALTER TABLE #letter144 ADD isFineAmount BIT NOT NULL DEFAULT 1
	SELECT REcordid INTO #pearlandDaybefore FROM #letter144 WHERE ISNULL(FineAmount,0) = 0

	UPDATE T
	SET	isFineAmount = 0
	FROM #letter144 t INNER JOIN #pearlandDaybefore f ON f.recordid = t.recordid

	SELECT * FROM #letter144

	DROP TABLE #pearlandDaybefore
	DROP TABLE #letter144  
END                    
IF @lettertype = 147 -- Rab Nawaz Khan 11431 10/14/2013 Added HCCC Updatd Addresses Mailers. . . 
BEGIN             
	 SELECT DISTINCT CONVERT(VARCHAR(20),n.noteid) AS letterid, UPPER(LTRIM(RTRIM(tna.FirstName))+ ' ' +LTRIM(RTRIM(tna.lAStname))) AS [NAME], 
	 UPPER(tna.Address1) AS [ADDRESS], UPPER(tna.City1) AS city, s.state, tna.ZipCode1 AS zip , N.recordid, c.midnumber AS span, 
	 l.ViolationDescription AS chargewording, l.TicketViolationDate AS bookingdate, l.causenumber AS casenumber, c.clientflag, 
	 left(tna.ZipCode1,5) +'1' + LTRIM(RTRIM(tna.RecordID)) AS zipspan,
	 n.listdate,             
	 tna.DP2_1 AS dptwo, tna.DPC_1 AS dpc,@user AS Abb , CONVERT(VARCHAR(20),@lettertype) AS lettertype, ISNULL(l.ChargeLevel, 0) AS ChargeLevel
	 INTO #temp147   
	 FROM dbo.tblticketsarchive c   
	 INNER JOIN  dbo.tblticketsviolationsarchive l ON c.recordid = l.recordid
	 INNER JOIN #letters tt ON tt.recordid = c.recordid
	 INNER JOIN tblNewAddresses tna ON tt.recordid = tna.RecordId
	 INNER JOIN  tblletternotes n ON n.noteid=tt.noteid
	 INNER JOIN dbo.tblstate s ON s.stateid = c.stateid_fk
	 WHERE tna.AddressStatus1 in ('Y','D','S') 
	 AND l.violatiONstatusid <> 80
	 AND ISNULL(tna.isMailerSentOnAddress1, 0) = 1
	 AND LEN(ISNULL(tna.Address1, '')) > 0
	 
	 INSERT INTO #temp147
	 SELECT DISTINCT CONVERT(VARCHAR(20),n.noteid) AS letterid, UPPER(LTRIM(RTRIM(tna.FirstName))+ ' ' +LTRIM(RTRIM(tna.lAStname))) AS [NAME], 
	 UPPER(tna.Address2) AS [ADDRESS], UPPER(tna.City2) AS city, s.state, tna.ZipCode2 AS zip , N.recordid, c.midnumber AS span, 
	 l.ViolationDescription AS chargewording, l.TicketViolationDate AS bookingdate, l.causenumber AS casenumber, c.clientflag, 
	 left(tna.ZipCode2,5) +'2'+ LTRIM(RTRIM(tna.RecordID)) AS zipspan,
	 n.listdate,             
	 tna.DP2_2 AS dptwo, tna.DPC_2 AS dpc,@user AS Abb , CONVERT(VARCHAR(20),@lettertype) AS lettertype, ISNULL(l.ChargeLevel, 0) AS ChargeLevel   
	 FROM dbo.tblticketsarchive c   
	 INNER JOIN  dbo.tblticketsviolationsarchive l ON c.recordid = l.recordid
	 INNER JOIN #letters tt ON tt.recordid = c.recordid
	 INNER JOIN tblNewAddresses tna ON tt.recordid = tna.RecordId
	 INNER JOIN  tblletternotes n ON n.noteid=tt.noteid    
	 INNER JOIN dbo.tblstate s ON s.stateid = c.stateid_fk
	 WHERE tna.AddressStatus2 in ('Y','D','S') 
	 AND l.violatiONstatusid <> 80
	 AND ISNULL(tna.isMailerSentOnAddress2, 0) = 1
	 AND LEN(ISNULL(tna.Address2, '')) > 0
	 
	 ALTER TABLE #temp147 
	 ADD isMaxFineAmountNotfound BIT NOT NULL DEFAULT 1, HcccMailerVioDesc VARCHAR(500), MaxFineAmount MONEY, MaxJailTimePeriod VARCHAR(100)
	 
	 -- Setting the default values to null for the mailer violatiONs . . . 
	 UPDATE #temp147 SET HcccMailerVioDesc = ''
	 --Updating the matched Records ON Charge level bASis FROM HCCC table
	 UPDATE t
	 SET t.HcccMailerVioDesc = UPPER(hc.MailerViolatiONDescriptiON), t.MaxFineAmount = hc.Maxfine, t.MaxJailTimePeriod = UPPER(hc.MaxJail), isMaxFineAmountNotfound = 0
	 FROM #temp147 t INNER JOIN tblHCCCViolatiONsInfo hc ON REPLACE(t.chargewording, ' ', '') = REPLACE(hc.ViolatiONDescriptiON, ' ', '') 
	 WHERE t.ChargeLevel = hc.ChargeLevel
	 
	 -- Updating the matched Records ON violatiON descriptiON bASis FROM HCCC table
	 UPDATE t
	 SET t.HcccMailerVioDesc = UPPER(hc.MailerViolatiONDescriptiON)
	 FROM #temp147 t INNER JOIN tblHCCCViolatiONsInfo hc ON REPLACE(t.chargewording, ' ', '') = REPLACE(hc.ViolatiONDescriptiON, ' ', '') 
	 
	 -- Getting the the Record if multiplie violatiONs and any of the violatiON not found in HCCC table
	 SELECT tt.Recordid INTO #tempHccFineAmountTable FROM #temp147 tt
	 WHERE tt.Recordid IN (SELECT t.Recordid FROM #temp147 t WHERE t.isMaxFineAmountNotfound = 0)
	 AND tt.isMaxFineAmountNotfound = 1
	 
	 -- Updating the Records
	 UPDATE #temp147
	 SET isMaxFineAmountNotfound = 1
	 WHERE Recordid in (SELECT t.Recordid FROM #tempHccFineAmountTable t)
	 
	 UPDATE t
	SET t.letterId = l.noteid
	FROM #temp147 t INNER JOIN #letters l ON t.letterId = l.noteid
	INNER JOIN tblLetterNotes n ON n.NoteId = t.letterid
	AND t.zip = n.ZipCode
	 
	 SELECT DISTINCT *
	 FROM #temp147

	DROP TABLE #temp147
	DROP TABLE #tempHccFineAmountTable
END
DROP TABLE #letters   
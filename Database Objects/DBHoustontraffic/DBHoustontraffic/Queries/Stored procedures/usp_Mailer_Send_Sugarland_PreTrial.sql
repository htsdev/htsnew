/****** 
Created by:		Asad Ali 
Business Logic:	The procedure is used by LMS application to get data for Pre Trial letter
				and to create LMS history for this letter for the selected date range.
				
List of Parameters:
	@catnum:		Category number of the selected letter type, if printing for 
					all courts of the selected court category
	@LetterType:	Selected letter type in LMS
	@CrtId:			Court id of the selected court category if printing for individual court hosue.
	@startListdate:	Comma separated dates selected to print letter.
	@EmpId:			Employee information who is printing the letter.
	@PrintType:		Flag to just preview the letter or to mark letters as sent. If 1 then LMS letter history will be created.
	@SearchType:	Flag that identifies if searching by court date or by list date. 0= list date, 1 = court date
	@isPrinted:		Flag to include already printed letters in the batch.

List of Columns:	
	LetterId:		Letter ID generated for each letter. It will be "NOT PRINTABLE"	if @PrintType = 0
	RecordId:		Identity value to look up the case in non-clients. used to group the records in report file.
	FirstName:		Person's first name that will appear on the letter
	LastName:		Person's last name that will appear on the letter
	Address:		Person's Home address that will appear on the letter
	City:			Person's Home City that will appear on the letter
	State:			Person's Home State that will appear on the letter
	FineAmount:		Fine amount for each violation that will appear on the letter
	ViolationDescritpion: violation description that will appear on the letter
	CourtName:		Name of court with which the violation is associated. Used in report file
	DPC:			Address status that is used in barcode font on the letter
	DPTwo:			Address status that is used in barcode font on the letter
	TicketNumber_pk:Case number for the violation
	ZipCode:		Person's Home Zip code.
	MidNumber:		MidNumber of the person associated with the case.
	Abb:			Short abbreviation of employee who is printing the letter

*******/

-- usp_Mailer_Send_HCJP_Judge 2, 52, 2, '5/12/2008,' , 3992, 0, 0, 0
ALTER proc [dbo].[usp_Mailer_Send_Sugarland_PreTrial]
@catnum int=1,                                    
@LetterType int=9,                                    
@crtid int=1,                                    
@startListdate varchar (500) ='01/04/2006',                                    
@empid int=2006,                              
@printtype int =0 ,
@searchtype int ,
@isprinted bit                                
                                    
as                                    

-- DECLARING LOCAL VARIABLES FOR THE FILTERS....                                          
declare @officernum varchar(50),                                    
@officeropr varchar(50),                                    
@tikcetnumberopr varchar(50),                                    
@ticket_no varchar(50),                                                                    
@zipcode varchar(50),                                             
@zipcodeopr varchar(50),                                    
                                    
@fineamount money,                                            
@fineamountOpr varchar(50) ,                                            
@fineamountRelop varchar(50),                   
              
@singleviolation money,                                            
@singlevoilationOpr varchar(50) ,                                            
@singleviolationrelop varchar(50),                                    
                                    
@doubleviolation money,                                            
@doublevoilationOpr varchar(50) ,                                            
@doubleviolationrelop varchar(50),                                  
@count_Letters int,                  
@violadesc varchar(500),                  
@violaop varchar(50)                                     
                          
-- DECLARING & INITIALIZING THE VARIABLE FOR DYNAMIC SQL...          
declare @sqlquery varchar(max), @sqlquery2 varchar(max) 
Select @sqlquery =''  , @sqlquery2 = ''
        
-- ASSIGNING VALUES TO THE VARIABLES 
-- BY GETTTING INFORMATION FROM LMS FILTER STRUCTURE...
-- FOR THIS LETTER TYPE....
Select @officernum=officernum,                                    
@officeropr=oficernumOpr,                                    
@tikcetnumberopr=tikcetnumberopr,                                    
@ticket_no=ticket_no,                                                                      
@zipcode=zipcode,                                    
@zipcodeopr=zipcodeLikeopr,                                    
@singleviolation =singleviolation,                                    
@singlevoilationOpr =singlevoilationOpr,                                    
@singleviolationrelop =singleviolationrelop,                                    
@doubleviolation = doubleviolation,                                    
@doublevoilationOpr=doubleviolationOpr,                                            
@doubleviolationrelop=doubleviolationrelop,                  
@violadesc=violationdescription,                  
@violaop=violationdescriptionOpr ,              
@fineamount=fineamount ,                                            
@fineamountOpr=fineamountOpr ,                                            
@fineamountRelop=fineamountRelop              
                                 
from tblmailer_letters_to_sendfilters                                    
where courtcategorynum=@catnum                                    
and Lettertype=@LetterType                                  

-- GETTING THE SHORT ABBREVIATION OF THE CURRENTLY LOGGED IN 
-- EMPLOYEE IN THE LOCAL VARIABLE....
declare @user varchar(10)
select @user = upper(abbreviation) from tblusers where employeeid = @empid
                                    

-- CREATING DYNAMIC SQL FOR THE MAIN QUERY...
set @sqlquery=@sqlquery+'                                    
Select	distinct ta.recordid,
convert(varchar(10),tva.courtdate,101) as listdate, 
count(tva.ViolationNumber_PK) as ViolCount,
isnull(sum(isnull(tva.fineamount,0)),0) as Total_Fineamount 
into #temp2                                          
FROM    dbo.tblTicketsViolationsArchive tva 
INNER JOIN
dbo.tblTicketsArchive ta 
ON 	tva.RecordID = ta.RecordID 

INNER JOIN
dbo.tblCourtViolationStatus tcvs 
ON 	tva.violationstatusid = tcvs.CourtViolationStatusID 

-- ONLY Pre Trial CASES...
where  	tcvs.CategoryID=3

-- ONLY VERIFIED AND VALID ADDRESSES
--and 	Flag1 in (''Y'',''D'',''S'') 

-- NOT MARKED AS STOP MAILING...
and donotmailflag = 0 

--also send it to qoute client
-- SHOULD BE NON-CLIENT
--and 	ta.clientflag=0

-- PERSONS ADDRESS IS NOT CHANGED IN PAST 48 MONTHS (ACCUZIP)...
and isnull(ta.ncoa48flag,0) = 0  

-- EXCLUDE JUVENILE CASES.......
--and isnull(tva.juvenileflag,0) =0 
'               
          
-- IF PRINTING FOR ALL ACTIVE COURTS OF THE SELECTED CATEGORY.....
if(@crtid=@catnum )              
	set @sqlquery=@sqlquery+' 
	and 	tva.courtlocation In (Select courtid from tblcourts where courtcategorynum = ' + convert(varchar(10),@catnum)+ ' and inactiveflag = 0 )'                  

-- PRINTING FOR AN INDIVIDUAL COURT OF THE SELECTED CATEGORY...
else
	set @sqlquery =@sqlquery +'                                  
	and 	tva.courtlocation =  ' + convert(varchar(10),@crtid)                                 
                                  

-- ONLY FOR THE SELECTED DATE RANGE.....
if(@startListdate<>'')                                    
	set @sqlquery=@sqlquery+' 
	and   ' + dbo.Get_String_Concat_With_DatePart_ver2 ( ''+  @startlistdate + '', 'tva.courtdate' ) 


-- LMS FILTERS SECTION:
-----------------------------------------------------------------------------------------------------------------------------------------------------
-- IF THERE IS ANY FILTER SPECIFIED TO INCLUDE/EXCLUDE ONLY PARTICULAR OFFICER NUMBERS....
if(@officernum<>'')                                    
	set @sqlquery =@sqlquery +'  
	and 	ta.officerNumber_Fk '+@officeropr +'('+ @officernum+')'                                     

-- IF THERE IS ANY FILTER DEFINED FOR INCLUDE/EXCLUDE TICKET NUMBER...                                    
if(@ticket_no<>'')                                    
	set @sqlquery=@sqlquery+ '
	and  	('+ dbo.Get_String_Concat_With_op(''+@ticket_no+'' , 'tva.TicketNumber_PK' ,+ ''+ @tikcetnumberopr)+')'                                          
                                    
-- ZIP CODE FILTER......
-- INCLUDE/EXCLUDE THE SPECIFIED ZIP CODES.....
if(@zipcode<>'')                                                          
	set @sqlquery=@sqlquery+ ' 
	and	('+  dbo.Get_String_Concat_With_op(''+@zipcode+'' , 'left(ta.zipcode,3)' ,+ ''+ @zipcodeopr) +')' 


-- FINE AMOUNT FILTER.....
-- INCLUDE ONLY THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT....
if(@fineamount<>0 and @fineamountOpr<>'not'  )              
	set @sqlquery =@sqlquery+ ' 
	and tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                   


-- EXCLUDE THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT.....
if(@fineamount<>0 and @fineamountOpr='not'  )              
	set @sqlquery =@sqlquery+ '
    and not tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                   


-- ECLUDE THE LETTERS THAT HAVE BEEN ALREADY PRINTED......
set @sqlquery =@sqlquery+ ' 
and ta.recordid not in (                                                                    
	select	n.recordid from tblletternotes n, tblticketsarchive t 
	where n.recordid =t.recordid and lettertype ='+convert(varchar(10),@lettertype)+'  
	)     


group by  
	ta.recordid, 
	convert(varchar(10),tva.courtdate,101) having 1=1   '

-- SINGLE VIOLATION FINE AMOUNT FILTER....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE SINGLE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT                            
if(@singleviolation<>0 and @doubleviolation=0)                                
	set @sqlquery =@sqlquery+ '
	and (  '+ @singlevoilationOpr+  '  (isnull(sum(isnull(tva.fineamount,0)),0) '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and count(tva.ViolationNumber_PK)=1))
    '      

-- DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...                          
if(@doubleviolation<>0 and @singleviolation=0 )                                
	set @sqlquery =@sqlquery + '
	and ('+ @doublevoilationOpr+  '   (isnull(sum(isnull(tva.fineamount,0)),0) '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ')and count(tva.ViolationNumber_PK)=2) )
    '                                                    
                                
-- BOTH SINGLE & DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE SINGLE OR DOUBLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...
if(@doubleviolation<>0 and @singleviolation<>0)                                
	set @sqlquery =@sqlquery+ '
	and ('+ @singlevoilationOpr+  '  (isnull(sum(isnull(tva.fineamount,0)),0) '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and count(tva.ViolationNumber_PK)=1))
    and ('+  @doublevoilationOpr+  '   (isnull(sum(isnull(tva.fineamount,0)),0) '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ') and count(tva.ViolationNumber_PK)=2))
        '                                                                   


-- GETTING THE CLIENT'S PERSONAL & VIOLATION INFORMATION 
-- IN TEMP TABLE FOR RECORDS FILTERED IN THE ABOVE QUERY...
set @sqlquery=@sqlquery+'                                    
--Asad Ali 8277 09/14/2010  no letter of other mailer type will be send in the next 5 working days from th previous sent date. 
Delete from #temp2
FROM #temp2 a INNER JOIN tblletternotes n ON n.RecordID = a.recordid 
INNER JOIN tblletter l ON l.LetterID_PK = n.LetterType 
where 	
		l.courtcategory = 9
		AND datediff(day, recordloaddate, dbo.fn_getprevbusinessday(GETDATE(),5)) <=0

Select	distinct ta.recordid, 
	a.listdate,
	ta.courtdate,
	tva.courtlocation as Courtid, 
	flag1 = isnull(ta.Flag1,''N'') ,
	ta.officerNumber_Fk,                                          
	--Sabir Khan 9683 09/15/2011 Need to remove last two character from ticket number and cancatinate -0x                                        
	case when len(isnull(tva.causenumber,''''))>0 then tva.causenumber else SUBSTRING(tva.TicketNumber_PK,0,LEN(tva.ticketnumber_pk)-1) + ''-0x'' end as TicketNumber_PK,
	ta.donotmailflag,
	ta.clientflag,
	upper(firstname) as firstname,
	upper(lastname) as lastname,
	upper(address1) + '''' + isnull(ta.address2,'''') as address,                      
	upper(ta.city) as city,
	S.state as state,
	c.CourtName,
	zipcode,
	midnumber,
	dp2 as dptwo,
	dpc,                
	violationdate,
	violationstatusid,
	left(zipcode,5) + rtrim(ltrim(midnumber)) as zipmid,
	ViolationDescription, 
	-- Rab Nawaz Khan 9704 09/26/2011 Default fine amount 100 logic is commented. . .  
	-- case when isnull(tva.fineamount,0)=0 then 100 else tva.fineamount end as FineAmount, 
	isnull(tva.fineamount,0) as FineAmount, 
	a.ViolCount,
	a.Total_Fineamount 
into #temp                                          
from tblticketsarchive ta, tblticketsviolationsarchive tva, #temp2 a, tblcourts c, tblstate s, tblcourtviolationstatus cs
where ta.recordid = tva.recordid and ta.recordid = a.recordid
and tva.courtlocation = c.courtid and c.inactiveflag = 0
and ta.stateid_fk = S.stateID and cs.courtviolationstatusid = tva.violationstatusid 
and cs.categoryid = 3
and 	Flag1 in (''Y'',''D'',''S'') 
--and 	donotmailflag = 0
--Sabir Khan 8757 01/03/2010 send it to qoute client
--and 	ta.clientflag=0
and isnull(ta.ncoa48flag,0) = 0 
--and isnull(tva.juvenileflag,0) =0 
'

-- VIOLATION DESCRIPTION FILTER.....
-- EXCLUDE THE LETTER IF SPECIFIED VIOLATION IS THE ONLY VIOLATION IN THE LETTER....
if(@violadesc <> '')
begin
	-- NOT LIKE FILTER.......
	if(charindex('not',@violaop)<> 0 )              
		set @sqlquery=@sqlquery+' 
		and	 not ( a.violcount =1  and ('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ 'like')+'))'           

	-- INCLUDE ONLY THOSE LETTER THAT SINGLE VIOLATION AND MATCHES THE SPECIFIED VIOLATION DESCRIPTION...
	-- LIKE FILTER.......
	if(charindex('not',@violaop) =  0 )              
		set @sqlquery=@sqlquery+' 
		and	 ( a.violcount =1  and ('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ 'like')+'))'           
end

-- GETTING THE FINAL RESULSTS IN ANOTHER TEMP TABLE.....                                                                 
set @sqlquery=@sqlquery+'

	  


Select distinct recordid, violationstatusid,ViolCount,Total_Fineamount,  listdate as mdate,FirstName,LastName,address,                              
city,state,FineAmount,violationdescription,CourtName,dpc,dptwo,Flag1,officerNumber_Fk,TicketNumber_PK,                                     
courtid,zipmid,donotmailflag,clientflag, zipcode, midnumber into #temp1  from #temp                                    
'              

--Yasir Kamal 6898 11/04/2009 Exclude client cases.
set @sqlquery=@sqlquery +'  
delete from #temp1 where recordid in (
select v.recordid from tblticketsviolations v inner join tbltickets t 
on t.ticketid_pk = v.ticketid_pk and t.activeflag = 1 
inner join #temp1 a on a.recordid = v.recordid
)

-- Rab Nawaz Khan 9704 09/28/2011 added to hide the entire columm in mailer if fine amount is zero agains any violation of a recored . . . 

 alter table #temp1 
 ADD isfineamount BIT NOT NULL DEFAULT 1  
 
 select recordid into #fine from #temp1    WHERE ISNULL(FineAmount,0) = 0  
 
 update t   
 set t.isfineAmount = 0  
 from #temp1 t inner join #fine f on t.recordid = f.recordid   

'                     
         
-- IF "MARK LETTER AS SENT" OPTION IS SELECTED....
-- CREATE LETTER HISTORY FOR EACH PERSON....
if( @printtype = 1 )  
	begin       
		set @sqlquery2 =@sqlquery2 +                                    
		'
		Declare @ListdateVal DateTime,                                  
		@totalrecs int,                                
		@count int,                                
		@p_EachLetter money,                              
		@recordid varchar(50),                              
		@zipcode   varchar(12),                              
		@maxbatch int  ,
		@lCourtId int  ,
		@dptwo varchar(10),
		@dpc varchar(10)

		declare @tempBatchIDs table (batchid int)

		-- GETTING TOTAL LETTERS AND COSTING ......
		Select @totalrecs =count(*) from #temp1 where mdate = @ListdateVal                                 
		Select @count=Count(*) from #temp1                                

		if @count>=500                                
		set @p_EachLetter=convert(money,0.3)                                

		if @count<500                                
		set @p_EachLetter=convert(money,0.39)                                

		-- DECLARING CURSOR TO INSERT THE RECORDS IN LETTER HISTORY ONE BY ONE.....
		-- THERE WILL A SEPARATE BATCH FOR EACH LIST/COURT DATE VALUE ......
		Declare ListdateCur Cursor for                                  
		Select distinct mdate  from #temp1                                
		open ListdateCur                                   
		Fetch Next from ListdateCur into @ListdateVal                                                      
		while (@@Fetch_Status=0)                              
			begin                                

				-- GETTING TOTAL LETTERS FOR THE LIST/COURT DATE ......	                     
				Select @totalrecs =count(distinct recordid) from #temp1 where mdate = @ListdateVal                                 

				-- INSERTING RECORD IN BATCH TABLE.....
				insert into tblBatchLetter(Empid, Lettertype,Listdate,CourtId,ParentID,LCount,PExpense) values                                  
				('+convert(varchar(10),@empid)+' ,'+convert(varchar(10),@lettertype)+', @ListdateVal, '+convert(varchar(10),@catnum)+', 0, @totalrecs, @p_EachLetter)                                   

				-- GETTING BATCH ID OF THE INSERTED RECORD.....
				Select @maxbatch=Max(BatchId) from tblBatchLetter 
				insert into @tempBatchIDs select @maxbatch                                                     

				-- CREATING CURSOR TO INSERT RECORDS FOR EACH LETTER FOR THE NEWLY INSERTED BATCH....
				Declare RecordidCur Cursor for                               
				Select distinct recordid,zipcode, courtid, dptwo, dpc from #temp1 where mdate = @ListdateVal                               
				open RecordidCur                                                         
				Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc                                          
				while(@@Fetch_Status=0)                              
					begin                              

						-- INSERTING LETTERS IN LETTER HISTORY FOR THE SPECIFIED BATCH...
						insert into tblLetterNotes(BatchId_Fk,listdate,ZipCode,RecordID,PCost,LetterType, courtid, dp2, dpc)                              
						values (@maxbatch,@ListdateVal,@zipcode,@recordid,@p_EachLetter,'+convert(varchar(10),@lettertype)+' ,@lCourtId, @dptwo, @dpc)                              
						Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc                          
					end                              
				close RecordidCur 
				deallocate RecordidCur                                                               
				Fetch Next from ListdateCur into @ListdateVal                              
			end                                            

		close ListdateCur  
		deallocate ListdateCur 

		-- OUTPUTTING THE DATA TO THE APPLICATION FOR REPROT FILE.....
		Select distinct convert(varchar(20),n.noteid) as letterid, t.recordid,  t.FirstName,                              
		t.LastName,t.address,t.city, t.state,FineAmount,violationdescription,CourtName, t.dpc,                              
		dptwo, TicketNumber_PK, t.zipcode, t.midnumber, '''+@user+''' as Abb , isfineamount                                   
		from #temp1 t , tblletternotes n, @tempBatchIDs tb
		where t.recordid = n.recordid and t.courtid = n.courtid and n.batchid_fk = tb.batchid and n.lettertype = '+convert(varchar(10),@lettertype)+' 
		order by T.recordid 

		-- GENERATING AND SENDING EMAIL FOR LETTER PRINTING NOTIFICATION........
		declare @lettercount int, @subject varchar(200), @body varchar(400) , @sql varchar(500)
		select @lettercount = count(distinct n.noteid) from tblletternotes n, @tempBatchIDs b where n.batchid_fk = b.batchid
		select @subject = convert(varchar(20), @lettercount) +  '' Sugarland Pre Trial Letter(s) Printed''
		select @body = convert(varchar(20), @lettercount) + '' LETTERS ('' + CONVERt(VARCHAR(12), GETDATE()) + '') - ''+ upper( '''+@user+''')
		exec usp_mailer_send_Email @subject, @body

		-- OUTPUTING THE PDF FILE NAME TO THE APPLICATION...
		declare @batchIDs_filname varchar(500)
		set @batchIDs_filname = ''''
		select @batchIDs_filname = @batchIDs_filname + convert(varchar,batchid) + '',''  from @tempBatchIDs
		select isnull(@batchIDs_filname,'''') as batchid

		'
	end
else if( @printtype = 0 )

	-- IF "MARK LETTER AS SENT" OPTION IS NOT SELECTED....
	-- THEN DO NOT CREATE LETTER HISTORY. JUST OUTPUT THE DATA FOR REPORT FILE 
	-- TO DISPLAY THE LETTER...
	BEGIN
		set @sqlquery2 = @sqlquery2 + '
		Select distinct ''NOT PRINTABLE'' as letterId,  recordid,  FirstName,  LastName,address,city, state,FineAmount,
		violationdescription,CourtName, dpc, dptwo, TicketNumber_PK, zipcode, midnumber, '''+@user+''' as Abb, isfineamount                                    
		from #temp1 
		order by recordid 
		'
	END

-- DROPPING TEMPORARY TABLES ....
set @sqlquery2 = @sqlquery2 + '

drop table #temp 
drop table #temp1'
                                    
print @sqlquery + @sqlquery2

-- EXECUTING THE DYNAMIC SQL ....
set @sqlquery = @sqlquery + @sqlquery2
exec (@sqlquery)

go

grant exec on dbo.usp_Mailer_Send_Sugarland_PreTrial to dbr_webuser 
go


/*
Author: Muhammad Adil Aleem
Description: To get ALR records 14 days before the court date
Parameters: NIL
Type : Report
Created date : Jul-1st-2008
*/
ALTER  procedure [dbo].[USP_HTP_ALR_Before14_Alert]
as
SELECT 
--Sabir Khan 6433 09/07/2009 S.No genrating has been removed and also the required filed has been added to select clause...    
t.TicketID_PK,
tv.RefCaseNumber AS [TICKETNO], t.Lastname AS [LNAME], t.Firstname AS [FNAME], dbo.fn_DateFormat(tv.CourtDateMain, 24, '/', ':', 1) 
                      AS [CRTDATE], CONVERT(int, tv.CourtNumbermain) AS [CRTNO], c.ShortName AS [CRTLOC], tcvs.ShortDescription AS [TRIALCATEGORY]
FROM         tblTickets AS t INNER JOIN
                      tblTicketsViolations AS tv ON t.TicketID_PK = tv.TicketID_PK INNER JOIN
                      tblCourtViolationStatus AS tcvs ON tv.CourtViolationStatusIDMain = tcvs.CourtViolationStatusID INNER JOIN
                      tblCourts AS c ON tv.CourtID = c.Courtid
WHERE     (t.Activeflag = 1) AND (tv.CourtViolationStatusIDMain = 237) AND (tv.CourtID IN (3047, 3048, 3070)) 
					AND (DATEDIFF(day, tv.CourtDateMain, GETDATE()) = - 14) 
                      AND
                          ((SELECT     COUNT(*) AS Expr1
                              FROM         tblTicketsFlag
                              WHERE     (TicketID_PK = t.TicketID_PK) AND (FlagID = 27)) <> 0)




set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go



/*********

Created By		: Zeeshan Ahmed

Business Logic	: This Procedure is used to get list of child custody setting type.

List Of Columns :

		ChildCustodySettingTypeId 
		ChildCustodySettingTypeName

*********/
Create  PROCEDURE [dbo].[USP_CC_GET_SettingType]
AS


SELECT * FROM [dbo].[ChildCustodySettingType]
Order by ChildCustodySettingTypeId desc 


go

grant execute on [dbo].[USP_CC_GET_SettingType] to dbr_webuser

go

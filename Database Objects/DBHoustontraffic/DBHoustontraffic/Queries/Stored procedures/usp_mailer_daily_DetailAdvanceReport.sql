

/*
* Business Logic:	The procedure is used by LMS application to get the returns on the selected mailer type.

				
List of Parameters:
	@catnum:		Category number of the selected letter type, if printing for 
					all courts of the selected court category
	@LetterType:	Selected letter type in LMS Reports
	@sDate:			Starting date for search criteria.
	@eDate:			Ending date for search criteria.
	@SearchType:	Flag that identifies if searching by upload date or by mailer date. 0= upload date, 1 = mailer date

List of Columns:	
	Ticket ID : ticket id for Client
	Mailer ID : Mailer Id for Letter that we send to that Client.
	First Name: First Name of Client.
	Last Name : Last Name of Client.
	Recdate	: Mailer Date.
	Project : 1 for hoston and 2 for Dallas 
	Causenumber: Case Number of Client.
	Hire DAte: Hire Date for Client,
	

******/

-- [usp_mailer_daily_DetailAdvanceReport] '','6633626,6633849,6653230,6653243,6653295,6653331,6695573,6695586,6695701,6695717,6695794,6755603,6755676,6784616,6784626,6784698,6793019,6793041,6793153,6793206,6793263,6793273,6804509,6804543,6804627,6804702,6813300,6813360,6813443,',1
CREATE PROCEDURE [dbo].[usp_mailer_daily_DetailAdvanceReport]
(
	@clientsids VARCHAR(MAX)='',
	@nonclientsids VARCHAR(MAX)='',
	@projectType INT
)
AS
CREATE TABLE #temp 
 (
 	TicketID_PK INT, MailerID VARCHAR(20),Firstname VARCHAR(50),Lastname VARCHAR(50), Recdate VARCHAR(15),
	project INT
 	
 )
CREATE TABLE #temp2
		(
			tickets VARCHAR(20),
			causenumber VARCHAR(20)
		)
		CREATE TABLE #temp3 
		(
			ticketid VARCHAR(20),
			causenumber VARCHAR(20)
		)
		CREATE TABLE #temp4 
     (
     	ticketid VARCHAR(20),
     	hiredate VARCHAR(15)
     )
		
IF(@projectType=1)
BEGIN
	IF(@clientsids<>'')
	BEGIN
				
		INSERT INTO #temp (TicketID_PK,MailerID,Firstname,Lastname,Recdate,project)
		SELECT DISTINCT tt.TicketID_PK, tt.MailerID,tt.Firstname,tt.Lastname,  CONVERT(VARCHAR,tn.RecordLoadDate,101) ,@projectType 
 		FROM tblTickets tt INNER JOIN tblLetterNotes tn ON tn.NoteId = tt.MailerID WHERE tt.MailerID IN (SELECT distinct sValue FROM dbo.SplitString(@clientsids,',')) AND tt.Activeflag=1
		
		INSERT INTO #temp2 (tickets,causenumber)
		SELECT DISTINCT ttv.TicketID_PK,
		(SELECT TOP 1 CASE WHEN LEN(ISNULL(casenumassignedbycourt,''))>0 THEN casenumassignedbycourt ELSE refcasenumber END FROM tblTicketsViolations WHERE TicketID_PK = t.TicketID_PK)
		FROM tblticketsviolations ttv INNER JOIN #temp t ON ttv.TicketID_PK=t.TicketID_PK
		
     
     INSERT INTO #temp4 (ticketid,hiredate)
     SELECT t.tickets, CONVERT(VARCHAR,MIN(p.recdate),101) 
     FROM tblticketspayment p INNER JOIN #temp2 t ON p.TicketID=t.tickets 
     WHERE p.PaymentVoid=0
     GROUP BY t.tickets
     

 SELECT DISTINCT t.*,t2.causenumber AS causenumber,t4.hiredate AS hiredate
   FROM #temp t INNER JOIN #temp2 t2 ON t.TicketID_PK=t2.tickets
 LEFT OUTER JOIN #temp4 t4 ON t4.ticketid=t.TicketID_PK	

END
	ELSE 
		BEGIN
				SET @nonclientsids=SUBSTRING(@nonclientsids,1,LEN(@nonclientsids)-1)
				
		INSERT INTO #temp (TicketID_PK,MailerID,Firstname,Lastname,Recdate,project)
		SELECT tt.TicketID_PK,   tt.MailerID,tt.Firstname,tt.Lastname,  CONVERT(VARCHAR,tt.Recdate,101),@projectType 
 		FROM tblTickets tt  INNER JOIN tblLetterNotes tln ON tt.MailerID=tln.NoteId
 		WHERE tt.MailerID IN (SELECT distinct DATA FROM dbo.fn_SplitString(@nonclientsids,',')) AND tt.Activeflag=0
		
		
		INSERT INTO #temp2 (tickets,causenumber)
		SELECT DISTINCT ttv.TicketID_PK,
		(SELECT TOP 1 CASE WHEN LEN(ISNULL(casenumassignedbycourt,''))>0 THEN casenumassignedbycourt ELSE refcasenumber END FROM tblTicketsViolations WHERE TicketID_PK = t.TicketID_PK)
		FROM tblticketsviolations ttv INNER JOIN #temp t ON ttv.TicketID_PK=t.TicketID_PK
--Abbas Shahid Khwaja 4900 12/04/2011 INSERT LEFT OUTER JOIN #temp4 t4 ON t4.ticketid=t.TicketID_PK and t4.hiredate by removing ''.
   SELECT t.*,t2.causenumber AS causenumber,t4.hiredate AS hiredate
   FROM #temp t INNER JOIN #temp2 t2 ON t.TicketID_PK=t2.tickets
   LEFT OUTER JOIN #temp4 t4 ON t4.ticketid=t.TicketID_PK	
		END
END
ELSE
	BEGIN
		IF(@clientsids<>'')
	BEGIN
		SET @clientsids=SUBSTRING(@clientsids,1,LEN(@clientsids)-1)
		INSERT INTO #temp (TicketID_PK,MailerID,Firstname,Lastname,Recdate,project)
		SELECT DISTINCT tt.TicketID_PK,   tt.MailerID,tt.Firstname,tt.Lastname,  CONVERT(VARCHAR,tln.RecordLoadDate,101) ,@projectType 
 		FROM DallasTrafficTickets.dbo.tblTickets tt  INNER JOIN tblLetterNotes tln ON tt.MailerID=tln.NoteId
		WHERE tt.MailerID IN (SELECT distinct DATA FROM dbo.fn_SplitString(@clientsids,',')) AND tt.Activeflag=1
		INSERT INTO #temp2 (tickets,causenumber)
		SELECT DISTINCT ttv.TicketID_PK,
		(SELECT TOP 1 CASE WHEN LEN(ISNULL(casenumassignedbycourt,''))>0 THEN casenumassignedbycourt ELSE refcasenumber END FROM DallasTrafficTickets.dbo.tblTicketsViolations WHERE TicketID_PK = t.TicketID_PK)
		FROM DallasTrafficTickets.dbo.tblticketsviolations ttv INNER JOIN #temp t ON ttv.TicketID_PK=t.TicketID_PK
     
     INSERT INTO #temp4 (ticketid,hiredate)
     SELECT t.tickets, CONVERT(VARCHAR,MIN(p.recdate),101) 
     FROM DallasTrafficTickets.dbo.tblticketspayment p INNER JOIN #temp2 t ON p.TicketID=t.tickets 
     WHERE p.PaymentVoid=0
     GROUP BY t.tickets
     
 SELECT t.*,t2.causenumber AS causenumber,t4.hiredate AS hiredate
   FROM #temp t INNER JOIN #temp2 t2 ON t.TicketID_PK=t2.tickets
 LEFT OUTER JOIN #temp4 t4 ON t4.ticketid=t.TicketID_PK	
		

	END
	ELSE 
		BEGIN
				SET @nonclientsids=SUBSTRING(@nonclientsids,1,LEN(@nonclientsids)-1)
				INSERT INTO #temp (TicketID_PK,MailerID,Firstname,Lastname,Recdate,project)
				SELECT DISTINCT tt.TicketID_PK,   tt.MailerID,tt.Firstname,tt.Lastname,  CONVERT(VARCHAR,tln.RecordLoadDate,101) ,@projectType 
 				FROM DallasTrafficTickets.dbo.tblTickets tt  INNER JOIN tblLetterNotes tln ON tt.MailerID=tln.NoteId
				WHERE tt.MailerID IN (SELECT distinct DATA FROM dbo.fn_SplitString(@nonclientsids,',')) AND tt.Activeflag=0
				
					INSERT INTO #temp2 (tickets,causenumber)
					SELECT DISTINCT ttv.TicketID_PK,
					(SELECT TOP 1 CASE WHEN LEN(ISNULL(casenumassignedbycourt,''))>0 THEN casenumassignedbycourt ELSE refcasenumber END FROM DallasTrafficTickets.dbo.tblTicketsViolations WHERE TicketID_PK = t.TicketID_PK)
					FROM DallasTrafficTickets.dbo.tblticketsviolations ttv INNER JOIN #temp t ON ttv.TicketID_PK=t.TicketID_PK
--Abbas Shahid Khwaja 4900 12/04/2011 INSERT t4.hiredate by removing ''.
 SELECT t.*,t2.causenumber AS causenumber,t4.hiredate AS hiredate
   FROM #temp t INNER JOIN #temp2 t2 ON t.TicketID_PK=t2.tickets
 LEFT OUTER JOIN #temp4 t4 ON t4.ticketid=t.TicketID_PK	
			END
	END
	


GO
GRANT EXECUTE ON [dbo].[usp_mailer_daily_DetailAdvanceReport] TO dbr_webuser
GO
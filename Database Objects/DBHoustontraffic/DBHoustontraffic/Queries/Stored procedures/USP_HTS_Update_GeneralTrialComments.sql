SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Update_GeneralTrialComments]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Update_GeneralTrialComments]
GO


CREATE procedure [dbo].[USP_HTS_Update_GeneralTrialComments]
	(
	@TicketId	int,
	@CustomerId	int
	)


as

declare @tFirstName	varchar(50),
	@tLastName	varchar(50),
	@cFirstName	varchar(50),
	@cLastName	varchar(50),
	@tAddress varchar(100),
	@cAddress varchar(100),
	@tCity	varchar(100),
	@cCity varchar(100),
	@tState int,
	@cState int,
	@tZip varchar(20),
	@cZip varchar(20),
	@IsDifferent	bit,
	@IsAddessDifferent bit,
	@Comments	varchar(200),
	@AddressComments varchar(200),
	@EmpAbb		varchar(10),
	@sName varchar(50)

select @EmpAbb = abbreviation from tblusers where employeeid = 3992

select	@tFirstName =  ltrim(rtrim(t.firstname)),
	@tLastName = ltrim(rtrim(t.lastname)),
	@cFirstName = ltrim(rtrim(c.firstname)),
	@cLastName = ltrim(rtrim(c.lastname)),
	@tAddress = ltrim(rtrim(t.address1)),
	@cAddress = ltrim(rtrim(c.homeaddress)),
	@tCity = ltrim(rtrim(t.city)),
	@cCity = ltrim(rtrim(c.city)),
	@tState = t.stateid_fk,
	@cState = c.stateid_fk,
	@tZip = ltrim(rtrim(left(t.zip,5))),
	@cZip  = ltrim(rtrim(left(c.zip,5)))
from 	tbltickets t inner join
	legalhouston.dbo.tblcustmer c 
on	c.id_customer = @CustomerId 
and 	t.ticketid_pk = @TicketId

select @sName = state from tblstate where stateid =  @tstate

if @tfirstname <>  @cfirstname
	set @IsDifferent = 1
else
	set @IsDifferent = 0

if @tlastname <> @clastname
	set @IsDifferent = 1
else
	set @IsDifferent = 0

set @comments  =     ' Diff. in client first/last name: Name in court data was ' + @tlastname + ', '+ @tfirstname +' (' +  dbo.formatdateandtimeintoshortdateandtime(getdate())  + '-' +  @empabb +  ')'
if @IsDifferent = 1
begin
	update  t 
	set 	t.firstname = c.firstname,
		t.lastname = c.lastname,
		t.generalcomments = isnull(t.generalcomments,'') + @comments,
		t.trialcomments = isnull(t.trialcomments,'') + @comments
	from 	tbltickets t inner join legalhouston.dbo.tblcustmer c 
	on	t.ticketid_pk = @ticketid
	and 	c.id_customer = @CustomerId
end


if @tAddress <> @cAddress
	set @IsAddessDifferent = 1
else
	set @IsAddessDifferent = 0

if @tCity <> @cCity
	set @IsAddessDifferent = 1
else
	set @IsAddessDifferent = 0

if @tState <> @cState
	set @IsAddessDifferent = 1
else
	set @IsAddessDifferent = 0

if @tZip <> @cZip
	set @IsAddessDifferent = 1
else
	set @IsAddessDifferent = 0

set @AddressComments=' Diff. in client address: Address in court data was '+ @tAddress + ', ' + @tCity + ', ' + @sName + ' ' + @tZip +' (' +  dbo.formatdateandtimeintoshortdateandtime(getdate())  + '-' +  @empabb +  ')'
if @IsAddessDifferent = 1
begin
	update  t 
	set 	t.address1 = c.homeaddress,
		t.address2 = '',
		t.city = c.city,
		t.stateid_fk = c.stateid_fk,
		t.zip = c.zip,
		t.generalcomments = isnull(t.generalcomments,'') + @AddressComments,
		t.trialcomments = isnull(t.trialcomments,'') + @AddressComments
	from 	tbltickets t inner join legalhouston.dbo.tblcustmer c 
	on	t.ticketid_pk = @ticketid
	and 	c.id_customer = @CustomerId
end













GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


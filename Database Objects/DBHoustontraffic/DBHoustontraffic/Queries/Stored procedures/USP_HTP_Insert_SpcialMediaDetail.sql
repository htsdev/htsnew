USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTP_Insert_SpcialMediaDetail]    Script Date: 01/02/2014 02:24:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** 
Create by:  Sabir Khan
Task ID: 11509
Business Logic : This procedure is used to save the social media detail with client profile.

******/

ALTER PROCEDURE [dbo].[USP_HTP_Insert_SpcialMediaDetail]      
@TicketID INT,
@MediaID INT,
@EmployeeID INT,     
@Description VARCHAR(500),
@IsChecked BIT
as      

DECLARE @ID INT
SET @ID = NULL

IF(@IsChecked = 1)
BEGIN
	IF NOT EXISTS(SELECT ID FROM SocialMedia WHERE description = LTRIM(RTRIM(@Description)))
	BEGIN
		INSERT INTO SocialMedia(description,InsertedBy,UpdatedBy)
		VALUES(@Description,@EmployeeID,@EmployeeID)	
		select @ID = @@IDENTITY
	
		INSERT INTO SocialMediaDetail(TicketID,MediaID,InsertedBy,UpdatedBy)
		VALUES(@TicketID,@ID,@EmployeeID,@EmployeeID)
		
		INSERT INTO tblTicketsNotes(TicketID,[Subject],EmployeeID)
		VALUES(@TicketID,'Social media site - ' + @Description + ' - added',@EmployeeID)
	END
	ELSE
	BEGIN
		IF(@MediaID = 0)
			BEGIN
				SELECT @ID = ID FROM SocialMedia WHERE DESCRIPTION = LTRIM(RTRIM(@Description))
				IF NOT EXISTS(SELECT ID FROM SocialMediaDetail WHERE ticketid = @TicketID AND mediaID = @ID)
				BEGIN
					INSERT INTO SocialMediaDetail(TicketID,MediaID,InsertedBy,UpdatedBy)
					VALUES(@TicketID,@ID,@EmployeeID,@EmployeeID)
					
					INSERT INTO tblTicketsNotes(TicketID,[Subject],EmployeeID)
					VALUES(@TicketID,'Social media site - ' + @Description + ' - added',@EmployeeID)	
				END						
			END
			ELSE
			BEGIN
				IF NOT EXISTS(SELECT ID FROM SocialMediaDetail WHERE ticketid = @TicketID AND mediaID = @MediaID)
				BEGIN
					INSERT INTO SocialMediaDetail(TicketID,MediaID,InsertedBy,UpdatedBy)
					VALUES(@TicketID,@MediaID,@EmployeeID,@EmployeeID)
					
					INSERT INTO tblTicketsNotes(TicketID,[Subject],EmployeeID)
					VALUES(@TicketID,'Social media site - ' + @Description + ' - added',@EmployeeID)	
				END					
			END
	END
	
	
	
END
ELSE
BEGIN
	IF(@MediaID = 0)
	BEGIN
		DECLARE @MediaIDD INT		
		SELECT TOP 1 @MediaIDD = ID FROM SocialMedia WHERE [Description] LIKE LTRIM(RTRIM(@Description))		
			
		IF EXISTS(SELECT ID FROM SocialMediaDetail WHERE ticketID = @TicketID AND mediaID = @MediaIDD)
		BEGIN			
			DELETE FROM SocialMediaDetail WHERE TicketID = @TicketID AND mediaID = @MediaIDD
			
			INSERT INTO tblTicketsNotes(TicketID,[Subject],EmployeeID)
			VALUES(@TicketID,'Social media site - ' + @Description + ' - removed',@EmployeeID)
		END
	END
	ELSE
		BEGIN
			IF EXISTS(SELECT ID FROM SocialMediaDetail WHERE ticketID = @TicketID AND mediaID = @MediaID)
			BEGIN
							
				DELETE FROM SocialMediaDetail WHERE TicketID = @TicketID AND mediaID = @MediaID
				
				INSERT INTO tblTicketsNotes(TicketID,[Subject],EmployeeID)
				VALUES(@TicketID,'Social media site - ' + @Description + ' - removed',@EmployeeID)
			END
		END
			
END


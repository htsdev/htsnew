﻿ 

/******   
Alter by:  Noufil Khan  
Business Logic : this procedure is used to Insert Comments and updating history with comments changes  
List of Parameters:   
 @TicketID   : By ticket id We can search the colunms including tickets information   
 @EmployeeID : By Employee ID We can search the colunms including tickets information  
 @CommentID : different comment id wrok differently with respect to comment id  
     * @CommentID = 2 -- SettingComments   
     * @CommentID = 3 -- ReminderComments   
     * @CommentID = 4 -- TrialComments   
     * @CommentID = 5 -- PreTrialComments   
     * @CommentID = 6 -- ContinuanceComments  
     * @CommentID = 7 -- ConsultationComments   
     * @CommentID = 8 -- ReadComments   
     * @CommentID = 6 -- ContinuanceComments   
     * @CommentID = 9 -- SetCallComments   
     * @CommentID = 10 -- FTACallComments   
  
 @Comments   : Comments That Need to Update or Insert  
  
******/    
--USP_HTS_Insert_Comments 184335, 3991,3, 'test3'  
    
--grant execute on dbo.USP_HTS_Insert_Comments to dbr_webuser    
-- procedure insert comments in database by choosing one of the type define below  
    
ALTER procedure [dbo].[USP_HTS_Insert_Comments] ----5497,3991,1,'sdsadadadaa'    
@TicketID as int,    
@EmployeeID as int,    
@CommentID as int,    
@Comments as varchar(max),  -- Adil - For 2585    
@IsComplaint AS BIT=0 --Nasir 6098 08/06/2009 add is complaint
As 
SET NOCOUNT ON;   
Declare @SQLQry as varchar(max)   
Declare @Employee_ShortName as varchar(5)   
Declare @ExistingComments as varchar(max)  -- Adil - For 2585  
  
Set @Comments = Replace(@Comments, '<', '< ')  
  
--Getting User Name [Short Name]   
Select @Employee_ShortName = Abbreviation From TblUsers WITH(NOLOCK) Where EmployeeID = @EmployeeID   
If @Employee_ShortName is null   
 Begin   
  Set @Employee_ShortName = 'N/A'   
 End   
If Len(@Comments) > 0   
 Begin   
  -- MERGING SYSTEM DATETIME AND USER WITH NEW COMMENTS   
select @Comments  
select len(@Comments)  
--Nasir 6098 08/06/2009 add is complaint
IF(@IsComplaint = 1)
BEGIN
	DECLARE @CommentType VARCHAR(100)
	SELECT @CommentType=CommentType FROM Comments  WITH(NOLOCK) WHERE CommentID = @CommentID
	Set @Comments = upper( @Comments + ' (' + dbo.fn_DateFormat(getdate(),2,'/',':',1) + ': Complaint - ' + @Employee_ShortName + ')' )  --Fahad 6054 08/19/2009 New Date format function added
	
	--	exec USP_HTS_Insert_flag_by_ticketnumber_and_flagID @TicketID,37,@EmployeeID
	INSERT INTO tblTicketsFlag(TicketID_PK,FlagID,RecDate,Empid,lastupdatedate) VALUES(@TicketID,37,GETDATE(),@EmployeeID,GETDATE())		
	insert into tblticketsnotes(ticketid,subject,employeeid,Notes) values(@ticketid,'Flag - Complaint ['+ @CommentType + ']',@EmployeeID,null) 
END
ELSE
	BEGIN			
  Set @Comments = upper( @Comments + ' (' + dbo.fn_DateFormat(getdate(),2,'/',':',1) + ' - ' + @Employee_ShortName + ')' )  
  END
    
  Select @ExistingComments   
  If @CommentID = 1 -- GeneralComments   
   Begin   
    -- GETTING OLD COMMENTS  
    Select @ExistingComments = (Select isnull( GeneralComments,'') As Comments from TblTickets WITH(NOLOCK) Where TicketID_PK = @TicketID)   
select len(@Comments)  
select len(@ExistingComments)  
    Set @ExistingComments = isnull(@ExistingComments,'') + ' ' + @Comments  
    -- UPDATING TBLTICKETS FOR NEW COMMENTS   
select len(@ExistingComments)  
select @ExistingComments  
    Update TblTickets Set GeneralComments = @ExistingComments, EmployeeIDUpdate = Convert(Varchar(10),@EmployeeID) Where TicketID_PK = @TicketID  
    -- INSERTING NOTES FOR CASE HISTORY   
select 'General Notes: ' + @Comments  --Nasir 6098 08/07/2009 insert also iscomplaint flag and comment type id
    Insert Into tblTicketsNotes(TicketID,Subject,EmployeeID,Fk_CommentID,IsComplaint) Values(@TicketID, 'General Notes: ' + @Comments , Convert(Varchar(10),@EmployeeID),1,@IsComplaint)   
   End   
  Else IF @CommentID = 2 -- SettingComments   
   Begin   
    -- GETTING OLD COMMENTS  
    Select @ExistingComments = (Select isnull( SettingComments,'') As Comments from TblTickets WITH(NOLOCK) Where TicketID_PK = @TicketID)   
    Set @ExistingComments = isnull(@ExistingComments,'') + ' ' + @Comments  
    -- UPDATING TBLTICKETS FOR NEW COMMENTS   
    Update TblTickets Set SettingComments = @ExistingComments, EmployeeIDUpdate = Convert(Varchar(10),@EmployeeID) Where TicketID_PK = @TicketID   
    -- INSERTING NOTES FOR CASE HISTORY   --Nasir 6098 08/07/2009 insert comment type id
    Insert Into tblTicketsNotes(TicketID,Subject,EmployeeID,Fk_CommentID) Values(@TicketID, 'Setting Notes: ' + @Comments , Convert(Varchar(10),@EmployeeID),2)   
   End   
  Else If @CommentID = 3 -- ReminderComments   
   Begin   
    -- GETTING OLD COMMENTS  
    Select @ExistingComments = (Select Top 1 isnull( ReminderComments,'') As Comments from TblTicketsViolations WITH(NOLOCK) Where TicketID_PK = @TicketID)   
    Set @ExistingComments = isnull(@ExistingComments,'') + ' ' + @Comments  
    -- UPDATING TBLTICKETS FOR NEW COMMENTS   
    Update TblTicketsViolations Set ReminderComments = @ExistingComments, ReminderCallEmpID = Convert(Varchar(10),@EmployeeID) Where TicketID_PK = @TicketID   
    -- INSERTING NOTES FOR CASE HISTORY   --Nasir 6098 08/07/2009 insert iscomplaint flag and comment type id
    Insert Into tblTicketsNotes(TicketID,Subject,EmployeeID,Fk_CommentID,IsComplaint) Values(@TicketID, 'Reminder Notes: ' + @Comments , Convert(Varchar(10),@EmployeeID),3,@IsComplaint)   
   End   
  Else If @CommentID = 4 -- TrialComments   
   Begin   
    -- GETTING OLD COMMENTS  
    Select @ExistingComments = (Select isnull( TrialComments,'') As Comments from TblTickets WITH(NOLOCK) Where TicketID_PK = @TicketID)   
    Set @ExistingComments = isnull(@ExistingComments,'') + ' ' + @Comments  
    -- UPDATING TBLTICKETS FOR NEW COMMENTS   
    Update TblTickets Set TrialComments = @ExistingComments, EmployeeIDUpdate = Convert(Varchar(10),@EmployeeID) Where TicketID_PK = @TicketID   
    -- INSERTING NOTES FOR CASE HISTORY   --Nasir 6098 08/07/2009 insert comment type id
    Insert Into tblTicketsNotes(TicketID,Subject,EmployeeID,Fk_CommentID) Values(@TicketID, 'Trial Notes: ' + @Comments , Convert(Varchar(10),@EmployeeID),4)   
   End   
  Else If @CommentID = 5 -- PreTrialComments   
   Begin   
    -- GETTING OLD COMMENTS  
	-- Agha Usman 2664 06/04/2008
    Select @ExistingComments = (Select isnull('','') As Comments from TblTickets WITH(NOLOCK) Where TicketID_PK = @TicketID)   
    Set @ExistingComments = isnull(@ExistingComments,'') + ' ' + @Comments  
    -- UPDATING TBLTICKETS FOR NEW COMMENTS   
    Update TblTickets Set  EmployeeIDUpdate = Convert(Varchar(10),@EmployeeID) Where TicketID_PK = @TicketID   
    -- INSERTING NOTES FOR CASE HISTORY   --Nasir 6098 08/07/2009 insert comment type id
    Insert Into tblTicketsNotes(TicketID,Subject,EmployeeID,Fk_CommentID) Values(@TicketID, 'Pre-Trial Notes: ' + @Comments , Convert(Varchar(10),@EmployeeID),5)   
   End   
  Else If @CommentID = 6 -- ContinuanceComments   
   Begin   
    -- GETTING OLD COMMENTS  
    Select @ExistingComments = (Select isnull( ContinuanceComments,'') As Comments from TblTickets WITH(NOLOCK) Where TicketID_PK = @TicketID)   
    Set @ExistingComments = isnull(@ExistingComments,'') + ' ' + @Comments  
    -- UPDATING TBLTICKETS FOR NEW COMMENTS   
    Update TblTickets Set ContinuanceComments = @ExistingComments, EmployeeIDUpdate = Convert(Varchar(10),@EmployeeID) Where TicketID_PK = @TicketID   
    -- INSERTING NOTES FOR CASE HISTORY   --Nasir 6098 08/07/2009 insert comment type id
    Insert Into tblTicketsNotes(TicketID,Subject,EmployeeID,Fk_CommentID) Values(@TicketID, 'Continuance Notes: ' + @Comments , Convert(Varchar(10),@EmployeeID),6)   
   End   
  Else If @CommentID = 7 -- ConsultationComments   
   Begin   
    -- GETTING OLD COMMENTS  
    Select @ExistingComments = (Select isnull( Comments,'') As Comments from tbl_hts_consultationcomments WITH(NOLOCK) Where TicketID_FK = @TicketID)   
    Set @ExistingComments = isnull(@ExistingComments,'') + ' ' + @Comments  
    -- UPDATING tbl_hts_consultationcomments FOR NEW COMMENTS   
    declare @count int set @count = (select count(TicketID_FK) from tbl_hts_ConsultationComments where TicketID_FK=@TicketID)   
    if(@count>0)   
     Begin   
      Update tbl_hts_consultationcomments Set Comments = @ExistingComments, CommentsDate = GetDate() Where TicketID_FK = @TicketID   
     End   
    Else   
     Begin   
      INSERT INTO tbl_hts_ConsultationComments(TicketID_FK,CommentsDate, Comments) VALUES (@TicketID, getdate(), @ExistingComments)   
     End  
    -- INSERTING NOTES FOR CASE HISTORY   --Nasir 6098 08/07/2009 insert comment type id
    Insert Into tblTicketsNotes(TicketID,Subject,EmployeeID,Fk_CommentID) Values(@TicketID, 'Consultation Notes: ' + @Comments , Convert(Varchar(10),@EmployeeID),7)   
   End   
  Else If @CommentID = 8 -- ReadComments   
   Begin   
    -- GETTING OLD COMMENTS  
    Select @ExistingComments = (Select isnull( Readcomments,'') As Comments from TblTickets Where TicketID_PK = @TicketID)   
    Set @ExistingComments = isnull(@ExistingComments,'') + ' ' + @Comments  
    -- UPDATING TBLTICKETS FOR NEW COMMENTS   
    Update TblTickets Set Readcomments = @ExistingComments, EmployeeIDUpdate = Convert(Varchar(10),@EmployeeID) Where TicketID_PK = @TicketID   
    -- INSERTING NOTES FOR CASE HISTORY   --Nasir 6098 08/07/2009 insert comment type id
    Insert Into tblTicketsNotes(TicketID,Subject,EmployeeID,Fk_CommentID) Values(@TicketID, 'Read Notes: ' + @Comments , Convert(Varchar(10),@EmployeeID),8)   
   End   
  Else If @CommentID = 9 -- SetCallComments   
   Begin   
    -- GETTING OLD COMMENTS  
    Select @ExistingComments = (Select Top 1 isnull( SetCallComments,'') As Comments from TblTicketsViolations WITH(NOLOCK) Where TicketID_PK = @TicketID)   
    Set @ExistingComments = isnull(@ExistingComments,'') + ' ' + @Comments  
    -- UPDATING TBLTICKETS FOR NEW COMMENTS   
    Update TblTicketsViolations Set SetCallComments = @ExistingComments, ReminderCallEmpID = Convert(Varchar(10),@EmployeeID) Where TicketID_PK = @TicketID   
    -- INSERTING NOTES FOR CASE HISTORY   --Nasir 6098 08/07/2009 insert comment type id
    Insert Into tblTicketsNotes(TicketID,Subject,EmployeeID,Fk_CommentID) Values(@TicketID, 'Set Call Notes: ' + @Comments , Convert(Varchar(10),@EmployeeID),9)   
   End   
   
   -- Added Zahoor 3977
   Else If @CommentID = 10 -- FTACallComments   
   Begin   
   -- GETTING EXISTING NOTES 
	Select @ExistingComments = (Select Top 1 isnull( FTACallComments,'') As Comments from TblTicketsViolations  WITH(NOLOCK) Where TicketID_PK = @TicketID)   
    -- SETTING EXISTING NOTES
    Set @ExistingComments = isnull(@ExistingComments,'') + ' ' + @Comments  
    
 -- UPDATING TBLTICKETS FOR NEW COMMENTS   
    Update TblTicketsViolations Set FTACallComments = @ExistingComments, ReminderCallEmpID = Convert(Varchar(10),@EmployeeID) Where TicketID_PK = @TicketID   
    
    -- INSERTING NOTES FOR CASE HISTORY   --Nasir 6098 08/07/2009 insert comment type id
    Insert Into tblTicketsNotes(TicketID,Subject,EmployeeID,Fk_CommentID) Values(@TicketID, 'FTA Call Notes: ' + @Comments , Convert(Varchar(10),@EmployeeID),10)   
   End      	
   -- ended 3977
 End  
  
  
  
  
  
  
  
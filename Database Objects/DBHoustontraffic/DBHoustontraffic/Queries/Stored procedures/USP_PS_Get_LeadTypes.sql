USE TrafficTickets
GO
/****** Object:  StoredProcedure [dbo].[USP_PS_Get_LeadTypes]    Script Date: 11/07/2013 12:44:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rab Nawaz Khan
-- Task ID:		11473
-- Create date: 10/22/2013
-- Description:	This Store Procedure is used to get the Leads Types from Database
-- =============================================
CREATE PROCEDURE [dbo].[USP_PS_Get_LeadTypes] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT tr.Reminderid_PK AS [leadId], tr.[Description] AS [lead]
        FROM TrafficTickets.dbo.tblReminderstatus tr
	WHERE tr.Reminderid_PK IN (0, 9, 10)
END
GO
	GRANT EXECUTE ON USP_PS_Get_LeadTypes TO dbr_webuser
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Resets_Add_ScanImage]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Resets_Add_ScanImage]
GO



--sp_helptext usp_Add_ScanImage        
        
     
CREATE  PROCEDURE [dbo].[usp_Resets_Add_ScanImage]                     
@updated smalldatetime,                    
@extension nvarchar(4),                    
@Description varchar(150),                  
@DocType nvarchar(50),                    
@Employee int,                    
@TicketID int,                    
@Count int,                    
@Book int,    
@oldDocid int,    
@olddocnum int,                    
@BookID nvarchar(50) OUTPUT                    
AS                    
declare @docID int, @picID nvarchar(10)                    
                    
 select @BookID = @Book                    
                    
 if (@Book = 0)                    
 BEGIN                    
                      
                      
  select @docID = (select DocTypeID from tblDocType where lower(DocType) = lower(@DocType)) --Get Document id of a given document type                    
                      
  insert into tblScanBook (EmployeeID,TicketID,DocTypeID,updatedatetime,DocCount,ResetDesc) values(@employee,@TicketID,@docID,null,0,@Description) -- insert into book                    
                
  select @BookID =  Scope_Identity() --Get Book Id of the latest inserted                    
                 
  declare @temp_description varchar(150)         
  declare @typeofdoc varchar(50)                
  declare @rectype varchar(1)                
        
          
  select @temp_description = @Description        
        
  select @temp_description =  case when @temp_description <> '' then '[' + @temp_description + ']' else @temp_description end         
        
  select @typeofdoc=doctype from tbldoctype where doctypeid=@docID                
  --if @typeofdoc='Resets'                    
   --Set @rectype='0'                
  --else                
    Set @rectype='1'                
                
  insert into tblTicketsNotes (TicketID,Subject,EmployeeID) values (@TicketID,'<a href="javascript:window.open(''../paperless/PreviewMain.aspx?DocID='+@BookID+'&RecType='+@rectype+'&DocNum=0&DocExt='+@extension+''');void('''');"''>Scanned Document - ' + @typeofdoc + @temp_description +'</a>',@employee)               
             
                      
                 
                     
 END                    
                     
  update tblScanBook set updatedatetime = getdate(),DocCount = @Count where ScanBookID = @BookID -- Update the book table                    
                      
  insert into tblScanPIC (ScanBookID,updatedatetime,DocExtension,olddocid,olddocnum) values( @BookID,@updated,@extension,@olddocid,@olddocnum) --insert new image into database                    
                    
  select @picID =  Scope_Identity() --Get Book Id of the latest inserted                    
  --Docpic = @image                        
                    
                    
select @BookID = @BookID + '-' + @picID                    
return     
  




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


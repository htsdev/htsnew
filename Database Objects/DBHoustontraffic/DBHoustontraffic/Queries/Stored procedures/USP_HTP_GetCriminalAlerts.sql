﻿/************************************************************************************************    
--Created By   : Zeeshan Haider    
--Creation Data: 09/07/2012    
--Task ID :10421    
--Modified By	: Mohammad Nasir
--Modified Date	: 11/13/2012
--TaskID of modification	: 10528
--Parameter List     
--@UploadDate : List of record Filtered by the date of upload    
--@CaseType : List of record filtered by the case type    
--@SearchType: Search Criteria     
--Return Column: 
	ticketID_PK			: ticketID from HTP database 
	CaseNumber          : casenumassignedbycourt from HTP database 
	LastName            : last name of client from HTP database
	FirstName           : first name of client from HTP database
	DOB                 : DOB of client from HTP database           
	CriminalViolation   : Violation of client from HTP database                                                                                                         
	Address1            : Address1 of client from HTP database                               
	Address2            : Address2 of client from HTP database 
	City                : City of client from HTP database                               
	StateID_FK			: StateID_FK of client from HTP database
	Zip					: ZIP of client from HTP database
	CourtDate           : Court Date of client from HTP database           
	CaseTypeName        : Case Type Name of client from HTP database                               
	ClientStatus		: Client Status from HTP database
	lastName_HCC        : Last Name of client from Loaders Database                                                     
	firstName_HCC       : First Name of client from Loaders Database                                                                                 
	DOB_HCC             : DOB of client from Loaders Database           
	Zip_HCC				: ZIP of client from Loaders Database
	HCDCCaseNo_HCC      : Case Number of client from Loaders Database                                                                                 
	VDesc_HCC           : Violation description of client from Loaders Database                                                                                                                                                                                                                                     
	courtLocation		: Court Lolcation/ Court Name
--Business Logic : Display criminal alerts information on the basis of upload date and case type.    
--Execute: USP_HTP_GetCriminalAlerts '09/07/2012',0,1    
 ************************************************************************************************/    
--[USP_HTP_GetCriminalAlerts] '06/03/2013',1,4 
--Farrukh 10528 05/28/2013 Changed the business logic of the report 
ALTER PROCEDURE [dbo].[USP_HTP_GetCriminalAlerts]
	@UploadDate DATETIME,
	@CaseType INT, -- [0='Client and Quote', 1='Client', 2='Quote']
	@SearchType INT -- [1='LastName,FirstName,DOB', 2='LastName,DOB', 3='LastName,Zip', 4='Address,Zip']
AS
	IF @CaseType = 1
	BEGIN
	    --Mohammad Nasir 10528 11/12/2012 updated search type case to get criminal data record by matching Last Name, First Name & DOB (ignored) with loaders database  
	    IF @SearchType = 1---LastName,FirstName & DOB
	    BEGIN
	        SELECT DISTINCT t.ticketID_PK,
	               CASE 
	                    WHEN ISNULL(tv.casenumassignedbycourt, '') = '' THEN 
	                         'N/A'
	                    ELSE tv.casenumassignedbycourt
	               END AS CaseNumber,
	               t.LastName,
	               t.FirstName,
	               CONVERT(VARCHAR, ISNULL(t.DOB, '1/1/1900'), 101) AS DOB,
	               ISNULL(
	                   CASE 
	                        WHEN LEN(tv.ViolationDescription) >= 5 THEN LEFT(tv.ViolationDescription, 12) 
	                             + ' . . .'
	                        ELSE tv.ViolationDescription
	                   END,
	                   'N/A'
	               ) AS CriminalViolation,
	               t.Address1,
	               t.Address2,
	               t.City,
	               t.StateID_FK,
	               LEFT(ISNULL(t.Zip, ''), 5) AS [Zip],
	               CONVERT(VARCHAR, ISNULL(tv.CourtDate, '1/1/1900'), 101) AS 
	               CourtDate,
	               ct.CaseTypeName,
	               'Client' AS ClientStatus,
	               ta.LastName AS lastName_HCC,
	               ta.FirstName AS firstName_HCC,
	               CONVERT(VARCHAR, ISNULL(ta.DOB, '1/1/1900'), 101) AS DOB_HCC,
	               LEFT(ISNULL(ta.ZipCode, ''), 5) AS Zip_HCC,
	               ta.TicketNumber AS HCDCCaseNo_HCC,
	               ISNULL(
	                   CASE 
	                        WHEN LEN(tva.ViolationDescription) >= 5 THEN LEFT(tva.ViolationDescription, 12) 
	                             + ' . . .'
	                        ELSE tva.ViolationDescription
	                   END,
	                   'N/A'
	               ) AS VDesc_HCC,
	               ISNULL(
	                   CASE 
	                        WHEN LEN(tc.shortCourtName) >= 5 THEN LEFT(tc.shortCourtName, 12) 
	                             + ' . . .'
	                        ELSE tc.shortCourtName
	                   END,
	                   'N/A'
	               ) AS courtLocation
	        FROM   tblTickets t
	               INNER JOIN dbo.tblTicketsViolations tv
	                    ON  tv.ticketID_PK = t.ticketID_PK
	               INNER JOIN dbo.tblTicketsArchive ta
	                    ON  LTRIM(RTRIM(t.firstName)) = LTRIM(RTRIM(ta.FirstName))
	                    AND LTRIM(RTRIM(t.lastName)) = LTRIM(RTRIM(ta.LastName))
	                    AND DATEDIFF(DAY, t.DOB, ta.DOB) = 0
	                    --AND DATEDIFF(DAY, ISNULL(t.DOB,'1/1/1900'), ISNULL(ta.DOB,'1/1/1900')) = 0 
	               INNER JOIN tblTicketsViolationsArchive tva
						ON ta.RecordID = tva.RecordID     
	               INNER JOIN CaseType ct
	                    ON  ct.CaseTypeId = t.CaseTypeId
	               INNER JOIN tblState ts
	                    ON  ts.StateID = t.Stateid_FK
	               INNER JOIN tblCourts tc
	                    ON  tc.courtID = tv.courtID	               
	               	                    
	        WHERE  
				   DATEDIFF(DAY,ta.RecLoadDate,@UploadDate) = 0	              
	               AND t.FirmID = 3000
	               AND t.Activeflag = 1 	        
	               AND tva.CourtLocation IN (SELECT tc.Courtid FROM tblCourts tc WHERE tc.IsCriminalCourt = 1)
	        ORDER BY
	               t.TicketID_PK ASC
	    END 
	    
	    --Mohammad Nasir 10528 11/12/2012 updated search type case to get criminal data record by matching Last Name & DOB (ignored) with loaders database  
	    IF @SearchType = 2---LastName & DOB
	    BEGIN
	        SELECT DISTINCT t.ticketID_PK,
	               CASE 
	                    WHEN ISNULL(tv.casenumassignedbycourt, '') = '' THEN 
	                         'N/A'
	                    ELSE tv.casenumassignedbycourt
	               END AS CaseNumber,
	               t.LastName,
	               t.FirstName,
	               CONVERT(VARCHAR, ISNULL(t.DOB, '1/1/1900'), 101) AS DOB,
	               ISNULL(
	                   CASE 
	                        WHEN LEN(tv.ViolationDescription) >= 5 THEN LEFT(tv.ViolationDescription, 12) 
	                             + ' . . .'
	                        ELSE tv.ViolationDescription
	                   END,
	                   'N/A'
	               ) AS CriminalViolation,
	               t.Address1,
	               t.Address2,
	               t.City,
	               t.StateID_FK,
	               LEFT(ISNULL(t.Zip, ''), 5) AS [Zip],
	               CONVERT(VARCHAR, ISNULL(tv.CourtDate, '1/1/1900'), 101) AS 
	               CourtDate,
	               ct.CaseTypeName,
	               'Client' AS ClientStatus,
	               ta.LastName AS lastName_HCC,
	               ta.FirstName AS firstName_HCC,
	               CONVERT(VARCHAR, ISNULL(ta.DOB, '1/1/1900'), 101) AS DOB_HCC,
	               LEFT(ISNULL(ta.ZipCode, ''), 5) AS Zip_HCC,
	               ta.TicketNumber AS HCDCCaseNo_HCC,
	               ISNULL(
	                   CASE 
	                        WHEN LEN(tva.ViolationDescription) >= 5 THEN LEFT(tva.ViolationDescription, 12) 
	                             + ' . . .'
	                        ELSE tva.ViolationDescription
	                   END,
	                   'N/A'
	               ) AS VDesc_HCC,
	               ISNULL(
	                   CASE 
	                        WHEN LEN(tc.shortCourtName) >= 5 THEN LEFT(tc.shortCourtName, 12) 
	                             + ' . . .'
	                        ELSE tc.shortCourtName
	                   END,
	                   'N/A'
	               ) AS courtLocation
	        FROM   tblTickets t
	               INNER JOIN dbo.tblTicketsViolations tv
	                    ON  tv.ticketID_PK = t.ticketID_PK
	               INNER JOIN dbo.tblTicketsArchive ta 
	                    ON  LTRIM(RTRIM(t.lastName)) = LTRIM(RTRIM(ta.LastName))	                     
	                    AND DATEDIFF(DAY, t.DOB, ta.DOB) = 0
	                    --AND DATEDIFF(DAY, ISNULL(t.DOB,'1/1/1900'), ISNULL(ta.DOB,'1/1/1900')) = 0
	                    AND LTRIM(RTRIM(t.firstName)) <> LTRIM(RTRIM(ta.FirstName))
	                    AND LEFT(ISNULL(t.Zip, ''), 5) <> LEFT(ISNULL(ta.ZipCode, ''), 5)
	                    
	                    
	               INNER JOIN tblTicketsViolationsArchive tva
						ON ta.RecordID = tva.RecordID 
	               INNER JOIN CaseType ct
	                    ON  ct.CaseTypeId = t.CaseTypeId
	               INNER JOIN tblState ts
	                    ON  ts.StateID = t.Stateid_FK
	               INNER JOIN tblCourts tc
	                    ON  tc.courtID = tv.courtID	               
	        WHERE  
	               DATEDIFF(DAY,ta.RecLoadDate,@UploadDate) = 0	               
	               AND t.FirmID = 3000
	               AND t.Activeflag = 1 
	               AND tva.CourtLocation IN (SELECT tc.Courtid FROM tblCourts tc WHERE tc.IsCriminalCourt = 1)	               
	        
	        ORDER BY
	               t.TicketID_PK ASC
	    END
	    
	    --Mohammad Nasir 10528 11/12/2012 updated search type case to get criminal data record by matching Last Name, DOB (ignored) & ZIP with loaders database  
	    IF @SearchType = 3---LastName, DOB & ZIP
	    BEGIN
	        SELECT DISTINCT t.ticketID_PK,
	               CASE 
	                    WHEN ISNULL(tv.casenumassignedbycourt, '') = '' THEN 
	                         'N/A'
	                    ELSE tv.casenumassignedbycourt
	               END AS CaseNumber,
	               t.LastName,
	               t.FirstName,
	               CONVERT(VARCHAR, ISNULL(t.DOB, '1/1/1900'), 101) AS DOB,
	               ISNULL(
	                   CASE 
	                        WHEN LEN(tv.ViolationDescription) >= 5 THEN LEFT(tv.ViolationDescription, 12) 
	                             + ' . . .'
	                        ELSE tv.ViolationDescription
	                   END,
	                   'N/A'
	               ) AS CriminalViolation,
	               t.Address1,
	               t.Address2,
	               t.City,
	               t.StateID_FK,
	               LEFT(ISNULL(t.Zip, ''), 5) AS [Zip],
	               CONVERT(VARCHAR, ISNULL(tv.CourtDate, '1/1/1900'), 101) AS 
	               CourtDate,
	               ct.CaseTypeName,
	               'Client' AS ClientStatus,
	               ta.LastName AS lastName_HCC,
	               ta.FirstName AS firstName_HCC,
	               CONVERT(VARCHAR, ISNULL(ta.DOB, '1/1/1900'), 101) AS DOB_HCC,
	               LEFT(ISNULL(ta.ZipCode, ''), 5) AS Zip_HCC,
	               ta.TicketNumber AS HCDCCaseNo_HCC,
	               ISNULL(
	                   CASE 
	                        WHEN LEN(tva.ViolationDescription) >= 5 THEN LEFT(tva.ViolationDescription, 12) 
	                             + ' . . .'
	                        ELSE tva.ViolationDescription
	                   END,
	                   'N/A'
	               ) AS VDesc_HCC,
	               ISNULL(
	                   CASE 
	                        WHEN LEN(tc.shortCourtName) >= 5 THEN LEFT(tc.shortCourtName, 12) 
	                             + ' . . .'
	                        ELSE tc.shortCourtName
	                   END,
	                   'N/A'
	               ) AS courtLocation
	        FROM   tblTickets t
	               INNER JOIN dbo.tblTicketsViolations tv
	                    ON  tv.ticketID_PK = t.ticketID_PK
	               INNER JOIN dbo.tblTicketsArchive ta
	                    ON  LTRIM(RTRIM(t.lastName)) = LTRIM(RTRIM(ta.LastName))
	                    AND LEFT(ISNULL(t.Zip, ''), 5) = LEFT(ISNULL(ta.ZipCode, ''), 5)
	                    AND DATEDIFF(DAY, t.DOB, ta.DOB) = 0
	                    --AND DATEDIFF(DAY, ISNULL(t.DOB,'1/1/1900'), ISNULL(ta.DOB,'1/1/1900')) = 0
	                    AND LTRIM(RTRIM(t.firstName)) <> LTRIM(RTRIM(ta.FirstName))
	                    
	               INNER JOIN tblTicketsViolationsArchive tva
						ON ta.RecordID = tva.RecordID
	               INNER JOIN CaseType ct
	                    ON  ct.CaseTypeId = t.CaseTypeId
	               INNER JOIN tblState ts
	                    ON  ts.StateID = t.Stateid_FK
	               INNER JOIN tblCourts tc
	                    ON  tc.courtID = tv.courtID
	               
	        WHERE  
	               DATEDIFF(DAY,ta.RecLoadDate,@UploadDate) = 0	               
	               AND t.FirmID = 3000
	               AND t.Activeflag = 1 
	               AND tva.CourtLocation IN (SELECT tc.Courtid FROM tblCourts tc WHERE tc.IsCriminalCourt = 1)	               	               
	        
	        ORDER BY
	               t.TicketID_PK ASC
	    END
	    IF @SearchType = 4---LastName & ZIP
	    BEGIN
	        SELECT DISTINCT t.ticketID_PK,
	               CASE 
	                    WHEN ISNULL(tv.casenumassignedbycourt, '') = '' THEN 
	                         'N/A'
	                    ELSE tv.casenumassignedbycourt
	               END AS CaseNumber,
	               t.LastName,
	               t.FirstName,
	               CONVERT(VARCHAR, ISNULL(t.DOB, '1/1/1900'), 101) AS DOB,
	               ISNULL(
	                   CASE 
	                        WHEN LEN(tv.ViolationDescription) >= 5 THEN LEFT(tv.ViolationDescription, 12) 
	                             + ' . . .'
	                        ELSE tv.ViolationDescription
	                   END,
	                   'N/A'
	               ) AS CriminalViolation,
	               ISNULL(
	                   CASE 
	                        WHEN LEN(t.Address1) >= 8 THEN LEFT(t.Address1, 12) 
	                             + ' . . .'
	                        ELSE t.Address1
	                   END,
	                   'N/A'
	               ) AS [Address],
	               t.City,
	               t.StateID_FK,
	               LEFT(ISNULL(t.Zip, ''), 5) AS [Zip],
	               CONVERT(VARCHAR, ISNULL(tv.CourtDate, '1/1/1900'), 101) AS 
	               CourtDate,
	               ct.CaseTypeName,
	               'Client' AS ClientStatus,
	               ta.LastName AS lastName_HCC,
	               ta.FirstName AS firstName_HCC,
	               CONVERT(VARCHAR, ISNULL(ta.DOB, '1/1/1900'), 101) AS DOB_HCC,
	               LEFT(ISNULL(ta.ZipCode, ''), 5) AS Zip_HCC,
	               ISNULL(
	                   CASE 
	                        WHEN LEN(ta.Address1) >= 8 THEN LEFT(ta.Address1, 12) 
	                             + ' . . .'
	                        ELSE ta.Address1
	                   END,
	                   'N/A'
	               ) AS Address_HCC,
	               ta.TicketNumber AS HCDCCaseNo_HCC,
	               ISNULL(
	                   CASE 
	                        WHEN LEN(tva.ViolationDescription) >= 5 THEN LEFT(tva.ViolationDescription, 12) 
	                             + ' . . .'
	                        ELSE tva.ViolationDescription
	                   END,
	                   'N/A'
	               ) AS VDesc_HCC,
	               ISNULL(
	                   CASE 
	                        WHEN LEN(tc.shortCourtName) >= 5 THEN LEFT(tc.shortCourtName, 12) 
	                             + ' . . .'
	                        ELSE tc.shortCourtName
	                   END,
	                   'N/A'
	               ) AS courtLocation
	        FROM   tblTickets t
	               INNER JOIN dbo.tblTicketsViolations tv
	                    ON  tv.ticketID_PK = t.ticketID_PK
	               INNER JOIN dbo.tblTicketsArchive ta
	                    ON  LTRIM(RTRIM(t.lastName)) = LTRIM(RTRIM(ta.LastName))
	                    AND LEFT(ISNULL(t.Zip, ''), 5) = LEFT(ISNULL(ta.ZipCode, ''), 5)
	                    AND DATEDIFF(DAY, t.DOB, ta.DOB) <> 0
	                    --AND DATEDIFF(DAY, ISNULL(t.DOB,'1/1/1900'), ISNULL(ta.DOB,'1/1/1900')) <> 0
	                    AND LTRIM(RTRIM(t.firstName)) <> LTRIM(RTRIM(ta.FirstName))
	                    AND LEFT(ISNULL(t.Address1, ''), 8) = LEFT(ISNULL(ta.Address1, ''), 8)	                    
	               INNER JOIN tblTicketsViolationsArchive tva
						ON ta.RecordID = tva.RecordID
	               INNER JOIN CaseType ct
	                    ON  ct.CaseTypeId = t.CaseTypeId
	               INNER JOIN tblState ts
	                    ON  ts.StateID = t.Stateid_FK
	               INNER JOIN tblCourts tc
	                    ON  tc.courtID = tv.courtID
	               
	        WHERE  
	               DATEDIFF(DAY,ta.RecLoadDate,@UploadDate) = 0	               
	               AND t.FirmID = 3000
	               AND t.Activeflag = 1 
	               AND tva.CourtLocation IN (SELECT tc.Courtid FROM tblCourts tc WHERE tc.IsCriminalCourt = 1)	               	               
	        
	        ORDER BY
	               t.TicketID_PK ASC
	    END
	END
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_SP_NumberProject_bkp]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_SP_NumberProject_bkp]
GO

CREATE procedure [dbo].[USP_HTS_SP_NumberProject_bkp]

@sdate  as datetime,
@edate as datetime 

as 
--set @sdate = '06/11/2007'
--set  @edate ='06/16/2007'
set @edate = @edate + '23:59:59.999'

select cc.courtcategoryname,  l.lettername, convert(varchar(12), n.recordloaddate, 101) as [Date of Upload], count(distinct n.recordid) as lettercount 
into #t1
	from	tblbatchletter b, tblletternotes  n ,tblletter l ,tblcourtCategories cc
where	b.lettertype = l.letterid_pk
			and		b.batchid = n.batchid_fk
			and		b.courtid = cc.courtcategorynum
			and		b.batchsentdate between @sdate and @edate
			and		l.letterid_pk in (9, 17, 18)
group by cc.courtcategoryname, l.lettername, convert(varchar(12), n.recordloaddate, 101) 
 
--select * from tblletter
--select * from tblcourtCategories

select convert(varchar(12), bonddate, 101) as bonddate, count(distinct midnumber) as recordupload 
into #t2
from dallastraffictickets.dbo.tblticketsarchive 
where bonddate between   @sdate and @edate
group by convert(varchar(12), bonddate, 101)


select convert(varchar(12), listdate, 101) as listdate, count(distinct midnumber) as recordupload 
into #t3
from dallastraffictickets.dbo.tblticketsarchive 
where listdate between   @sdate and @edate
group by convert(varchar(12), listdate, 101)

select convert(varchar(12), t.listdate, 101) as listdate, count(distinct t.midnumber) as recordupload 
into #t4
from tblticketsviolationsarchive tv, tblticketsarchive t
where t.listdate between   @sdate and @edate
and t.recordid= tv.recordid
and violationstatusid = 3
group by convert(varchar(12), t.listdate, 101)




declare   @joindata  table
(
bonddate  varchar(12),
records	int,
lettername varchar(20)
)

insert into @joindata 
select *, 'bond' as  lettername from #T2 
union all
select *, 'Appearance' as  lettername from #t3 
Union all
select *, 'Arraignment 1' as  lettername from #t4 

select t.courtcategoryname + ' ' + t.lettername as [Letter Name], t.[Date of Upload], 
	j.records as [Records Uploaded], t.lettercount as [Letters Generated], 
round(cast(t.lettercount as float) / cast(j.records as float),2) as [Ratio of Uploaded Data to Letters Generated]
from #t1 t, @joindata j
	where t.[Date of Upload] = j.bonddate 
		and t.lettername  = j.lettername
order by t.[Date of Upload] desc


drop table #t1
drop table #t2
drop table #t3
drop table #t4

--select * from tblbatchletter 
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


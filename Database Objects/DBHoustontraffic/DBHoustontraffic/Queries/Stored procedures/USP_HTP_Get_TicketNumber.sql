set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

/************************************************************
 * Code formatted by SoftTree SQL Assistant © v4.0.34
 * Time: 5/25/2009 4:38:09 PM
 ************************************************************/

   
/****** Object: ******/     
/*        
Created By     : Fahad Muhammad Qureshi.
Created Date   : 05/29/2009  
TasK		   : 5807
Business Logic : This method is use to return cause number of a client.
           
Parameter:       
   @TicketID   : Ticket id of client.      
  
     
      
*/      


create PROCEDURE [dbo].[USP_HTP_Get_TicketNumber](@TicketID INT)
AS
	SELECT ROW_NUMBER() OVER(ORDER BY ttv.TicketID_PK) AS SNo,
	       case rtrim(ltrim(ttv.casenumassignedbycourt)) when '' then 'N/A' else  ISNULL(ttv.casenumassignedbycourt, 'N/A') end AS TicketNumber
	FROM   tblTicketsViolations ttv
	WHERE  ttv.TicketID_PK = @TicketID


go
grant execute on [dbo].[USP_HTP_Get_TicketNumber] to dbr_webuser 
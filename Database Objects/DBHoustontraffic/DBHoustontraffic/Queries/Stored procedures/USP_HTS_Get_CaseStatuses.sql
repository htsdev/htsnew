SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Get_CaseStatuses]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Get_CaseStatuses]
GO


create procedure dbo.USP_HTS_Get_CaseStatuses

as 


select description , typeid as statusid  from tbldatetype 
where isactive =1 
union
select description, courtviolationstatusid as statusid from tblcourtviolationstatus 
where courtviolationstatusid in (146,186)
order by [Description]


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


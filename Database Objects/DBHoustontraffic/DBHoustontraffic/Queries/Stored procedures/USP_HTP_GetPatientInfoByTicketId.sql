﻿/******  
* Created By :	Fahad Muhammad Qureshi.
* Create Date :   10/19/2010 9:04:38 PM
* Task ID :				6766
* Business Logic :  This procedure is used to Get .
* List of Parameter :
* Column Return : 	      
******/




ALTER PROCEDURE dbo.USP_HTP_GetPatientInfoByTicketId
	@TicketId INT
AS
	SELECT DISTINCT TOP 1 
	       tt.Firstname,
	       tt.Lastname,
	       CONVERT(VARCHAR, tt.DOB, 101) AS DOB,
	       ISNULL(tt.Address1,'') + ISNULL(tt.Address2,'') AS [ADDRESS],
	       tt.Occupation,
	       tt.Height,
	       tt.[Weight],
	       tt.Email,
	       tt.City,
	       tt.Zip,
	       tt.Contact1,
	       tt.Contact2,
	       tt.Height,
	       tt.[Weight],
	       ts.StateID,
	       tc.ContactType_PK AS ContactType1,
	       tc1.ContactType_PK AS ContactType2,
	       CASE ISNULL(tt.LanguageSpeak,'2') WHEN 'Spanish' THEN '1' WHEN '2' THEN '2' ELSE '0' END SpanishSpeaker, --Saeed 8585 12/04/2010 add new field.
		   '' AS SsnNo 		        --Saeed 8585 12/04/2010 SsnNo added.
	       
	FROM   tblTickets tt
	       LEFT OUTER JOIN tblContactstype tc
	            ON  tt.ContactType1 = tc.ContactType_PK
	       LEFT OUTER JOIN tblContactstype tc1
	            ON  tt.ContactType2 = tc1.ContactType_PK
	       LEFT OUTER JOIN tblState ts
	            ON  ts.StateID = tt.Stateid_FK
	WHERE  tt.TicketID_PK = @TicketId


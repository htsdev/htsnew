
-- =============================================
-- Author:		<Author-Asghar>
-- Create date: <Create Date-03/03/2007,>
-- Description:	<Description-This is proc fill drop down with courtname including courtid 3001,3002,3003>
-- =============================================
ALTER PROCEDURE [dbo].[usp_Get_CourtName] 
	
AS
BEGIN
	
	create table #temptable
    (
		rowid int identity,		
        courtname varchar(100),
		courtid int
    )
	insert into #temptable
	-- Abbas Qamar 10088  05-12-2012 HMW-W court Id added 3075
	select courtname,courtid from tblcourts where courtid in(3001,3002,3003,3075) order by courtid
    
    insert into #temptable
    -- Abbas Qamar 10088  05-12-2012 HMW-W court Id added 4000
	select courtname,courtid from tblcourts where inactiveflag =0 and courtid not in(0,3001,3002,3003,3075) order by courtname

   select * from #temptable order by rowid
   drop table #temptable	 
END



set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON

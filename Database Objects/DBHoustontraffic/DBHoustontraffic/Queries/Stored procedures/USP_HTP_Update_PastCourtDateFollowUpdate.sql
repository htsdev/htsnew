 -- Noufil 5691 04/06/2009 
 /**
 * Business Logic : This procedure Update PastCourtDateFollowUpDate
	Parameter : @ticketid : TicketID of Clients
				@PastCourtDateHMCFollowUpDate : Followupdate 
**/
CREATE PROCEDURE [dbo].[USP_HTP_Update_PastCourtDateFollowUpdate]
@ticketid INT,
@PastCourtDateHMCFollowUpDate DATETIME
AS

UPDATE tblTickets
SET	
	PastCourtDateFollowUpDate = @PastCourtDateHMCFollowUpDate
WHERE TicketID_PK=@ticketid

GO
GRANT EXECUTE ON [dbo].[USP_HTP_Update_PastCourtDateFollowUpdate] TO dbr_webuser


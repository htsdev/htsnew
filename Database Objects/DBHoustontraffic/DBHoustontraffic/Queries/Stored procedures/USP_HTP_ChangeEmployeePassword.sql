
/****** Object:  StoredProcedure [dbo].[USP_HTP_ChangeEmployeePassword]    Script Date: 07/23/2008 13:04:26 ******/
/**
Business Logic : Thsi procedure updates the Employee password
Parameter : 
		@oldpasspword : current password of employee
		@newpassword  : New password enter by employee
		@empid : id of employee

Column Return :
		@@rowcount : return the number of rows update
**/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[USP_HTP_ChangeEmployeePassword]
@oldpasspword varchar(200),
@newpassword varchar(200),
@empid int=0

as
if exists (select password from tblusers where employeeid=@empid and Password=@oldpasspword)
	begin	
		update tblusers set Password=@newpassword where employeeid=@empid
		select @@rowcount
	end
else
	select @@rowcount


go
grant exec on [dbo].[USP_HTP_ChangeEmployeePassword] to dbr_webuser



﻿/******************    
    
Alter BY : ZEESHAN AHMED    
    
BUSINESS LOGIC : This procedure is used to migrate criminal case from Civil Database to Traffic Ticket Database    
      
LIST OF INPUT PARAMETERS :    
    
  @vc_TicketNumber : Case Number              
  @employeeid      : Sales Rep ID     
  @mailerID        : Mailer ID Associated With The Case    
  @IsTrafficClient : Pass one for Traffic Client and zero for non traffic.                  
  @Partyid     : Party ID whose address is associated with the case.  
  @sOutPut         : Out Parameter Containing Ticket ID and Active Flag    
    
List of Columns:  
IsTrafficAlertSignUp:   Reprensts that client hire from TrafficAlert page   
  
*******************/         
   
--[USP_HTP_Insert_CaseInfoFromCivil_Ver_2] '199708757 ',3991,0,0,''
       
Alter procedure [dbo].[USP_HTP_Insert_CaseInfoFromCivil_Ver_2]   -- '199008451',3991,0,''              
 (                      
  @vc_TicketNumber varchar(20),                          
  @employeeid   int = 3992 ,                  
  @mailerID int = 0,                 
  @IsTrafficClient int,  
  @Partyid int,       
  @sOutPut varchar(20) ='' output                    
 )                      
                      
as                      
                    
declare @PlanId int,                    
 @RecCount int,                    
 @iFlag int ,                    
 @TicketId int,                    
 @ErrorNumber int,                    
 @BookingNumber varchar(30)                    
                    
                    
-- CHECK IF TICKET NUMBER ALREADY EXISTS IN ANY CUSTOMER PROFILE.....                    
SELECT  @RecCount = t.ticketid_pk,                     
 @iFlag  = ( case t.activeflag when 0 then 1 when 1 then 0 else 2  end)                    
FROM   tblTicketsViolations v                     
	inner join tbltickets t on  t.ticketid_pk = v.ticketid_pk                    
			and v.refcasenumber = @vc_TicketNumber

select  @iFlag = isnull(@iFlag,2), @RecCount = isnull(@RecCount,0)

-- CHECKING EXISTANCE OF THE RECORD IN TBLTICKETS......................                         
-- IF NOT FOUND, INSERT THE RECORD IN TBLTICKETS............                        
if @reccount <> 0                     
 update Civil.dbo.ChildCustody set clientflag = 1 where casenumber = @vc_TicketNumber                    
else                    
 begin
               
 -- DECLARING TEMP TABLE..........                        
 declare @temp table                        
  (                        
 [CaseNumber] [varchar](20) ,        
 [Style] [varchar](100),        
 [PartyRequest] [varchar](500),        
 [Result] [varchar](100) ,        
 [SettingDate] [datetime] ,        
 [CurrentCourt] [int] ,        
 [FileCourt] [int] ,        
 [NextSettingDate] [datetime] ,        
 [FileLocation] [varchar](100),        
 [Plaintiffs] [int] ,        
 [Defendants] [int] ,        
 [JudgementFor] [varchar](50),        
 [JudgementDate] [datetime] ,        
 [ImageNumber] [int] ,        
 [Volume] [int] ,        
 [PageNo] [int] ,        
 [Pgs] [int] ,        
 [Time] [varchar](20) ,        
 [CaseStatusName] [varchar](100) ,        
 [CaseTypeName] [varchar](100),        
 [SettingTypeName] [varchar](100) ,        
 [ChildCustodyCourtId] [int],        
 [RecLoadDate] [datetime] ,        
 [ClientFlag] [bit]        
  )                        
                     
        
           
         
-- DECLARING TEMP TABLE..........                        
        
 declare @temp1 table                        
  (                        
 [CaseNumber] [varchar](20) ,        
 [Style] [varchar](100),        
 [PartyRequest] [varchar](500),        
 [Result] [varchar](100) ,        
 [SettingDate] [datetime] ,        
 [CurrentCourt] [int] ,        
 [FileCourt] [int] ,        
 [NextSettingDate] [datetime] ,        
 [FileLocation] [varchar](100),        
 [Plaintiffs] [int] ,        
 [Defendants] [int] ,        
 [JudgementFor] [varchar](50),        
 [JudgementDate] [datetime] ,        
 [ImageNumber] [int] ,        
 [Volume] [int] ,        
 [PageNo] [int] ,        
 [Pgs] [int] ,        
 [Time] [varchar](20) ,        
 [CaseStatusName] [varchar](100) ,        
 [CaseTypeName] [varchar](100),        
 [SettingTypeName] [varchar](100) ,        
 [ChildCustodyCourtId] [int],        
 [RecLoadDate] [datetime] ,        
 [ClientFlag] [bit],                     
  [ViolationNumber_pk] [smallint]                         
  )                      
                        
                         
 --- FETCHING RECORD TO INSERT IN TEMPORARY TABLE...........                        
 insert into  @temp                          
 Select         
        
CaseNumber,Style,        
PartyRequest  ,          
Result   ,          
SettingDate   ,          
CurrentCourt    ,          
FileCourt    ,        
NextSettingDate   ,        
FileLocation ,        
Plaintiffs    ,        
Defendants    ,        
JudgementFor  ,        
JudgementDate   ,        
ImageNumber    ,        
Volume    ,        
PageNo    ,        
Pgs    ,        
Time  ,        
CaseStatusName    ,        
CT.CaseTypeName   ,        
SettingTypeName   ,        
3077,  --3071   tahir 5851 04/28/2009 fixed the court id issue  ,        
RecLoadDate   ,        
ClientFlag         
        
from Civil.dbo.ChildCustody CC        
Join Civil.dbo.CaseType CT        
On CC.CaseTypeID = CT.CaseTypeID        
Join Civil.dbo.ChildCustodySettingType ST        
On ST.ChildCustodySettingTypeID = CC.ChildCustodySettingTypeID        
Join Civil.dbo.ChildCustodyCaseStatus CS        
On CS.ChildCustodyCaseStatusID = CC.ChildCustodyCaseStatusID                 
where casenumber = @vc_TicketNumber                       
         
        
  --Get Party Information  
    
 declare   
 @FirstName as varchar(20),      
 @LastName as varchar(20),      
 @MiddleName as varchar(6),  
 @ZipCode as varchar(20),  
 @StateId as int,  
 @City as varchar(50),  
 @PhoneNumber as varchar(15),  
 @Address as varchar(50)  
      
     
 -- Get Party Detail       
 if ( @Partyid != -1 )  
 Begin  
    
  Select   
  @FirstName  = FirstName,  
  @LastName   = LastName ,  
  @MiddleName = MiddleName,  
  @ZipCode = ZipCode,  
  @StateId = StateId,  
  @City  = City,  
  @PhoneNumber= case when PhoneNumber ='N/A' then '' else PhoneNumber end,  
  @Address = Address  
  from civil.dbo.ChildCustodyParties where ChildCustodyPartyId = @Partyid  
    
  if ( @PhoneNumber <> '')  
  Begin  
  --Replace (,)&- Symbols From   
  SELECT @PhoneNumber =  REPLACE( REPLACE(REPLACE(@PhoneNumber,'-',''),')',''),'(','');  
    
  End  
    
 End      
 Else  
 Begin  
  Select   
  @FirstName  = '',  
  @LastName   = '' ,  
  @MiddleName = '',  
  @ZipCode = '',  
  @StateId = 0,  
  @City  = '',  
  @PhoneNumber= '',  
  @Address = ''        
    End    
                   
    -- NOW INSERT THE RECORD IN TBLTICKETS FROM TEMPORARY TABLE...............                        
begin tran                     
 insert into tbltickets                        
   (                        
   midnum,                        
   firstname,                        
   lastname,                        
   MiddleName,                        
   Address1,                        
   Address2,                          
   City,                        
   Stateid_FK,                        
   Zip,                        
            
   DLNumber,                        
   employeeid,                        
   Race,                        
   Gender,                        
   Height,                        
   employeeidupdate  ,                    
   MailerID,                
   dp2,                
   dpc,                
   flag1,  
   IsTrafficAlertSignup,         
   CaseTypeiD,  
  --Contact1, -- Abbas 10093 04/10/2012 No need to move phone number in migration.  
  --ContactType1  
                        
)                          
                            
   select    distinct top 1                         
    '',                        
    @FirstName,                    
    @LastName,                        @MiddleName,                    
    @Address,                        
    '',                         
    @City,                        
    @StateId,                        
    @ZipCode,                        
                       
    '00000000',                        
   @employeeid,                        
    '',                        
    '',                        
    '0',                        
   @employeeid,                    
   @mailerID,                
   '',                
   '',                
   '',        
   @IsTrafficClient,  
 4,  -- Noufil 4782 09/23/2008 Family Law Case type added
 --@PhoneNumber,  -- Abbas 10093 04/10/2012 No need to move phone number in migration. 
 --100                         
   from @temp         
         
          
        
                            
 if @@error = 0                       
  begin             
 --ozair Task 4767 09/11/2008 replace @@Identity with Scope_Identity   
 --as @@Identity returns the last inserted identity value instead of the insert statement executed in current scope  
 --select @TicketID=@@identity   
 select @TicketID=SCOPE_IDENTITY()             
   set @ErrorNumber =0                    
  end                    
 else                    
  begin                    
   set @ErrorNumber = @@error                    
  end                    
                     
  if @TicketID <> 0                    
   begin                        
        
--------------------------------------------------------------------------------------                  
--------- FIRST INSERTING VIOLATIONS IN TBLVIOLATIONS FROM TEMPORARY TABLE------------                       
------------------- IF IT DOESN'T EXISTS IN TBLVIOLATIONS-----------------------------        
--------------------------------------------------------------------------------------        
insert into dbo.tblviolations ( description, sequenceorder, IndexKey,IndexKeys,ChargeAmount,BondAmount,Violationtype,ChargeonYesflag,                         
        ShortDesc,ViolationCode,AdditionalPrice )                          
      select top 1  SettingTypeName,null,2,100,100,140,14,1,         
        'None','none',0                         
      from  @temp                           
      where  SettingTypeName not in (                          
        select description from dbo.tblviolations where violationtype = (                    
        select violationtype from tblcourtviolationtype                    
        where courtid = 3077 -- 3071 tahir 5851 04/28/2009 fixed the courtid issue....
        )                    
       )                          
                    
  if @@error <> 0                     
   set @errornumber = @@error                             
                          
-----------------------------------------------------------------------        
-- NOW GETTING VIOLATION DESCRIPTION INTO TEMPORARY TABLE..............                        
-----------------------------------------------------------------------        
   insert into  @temp1                         
   select t1.*,                        
    v.violationnumber_pk                    
   from @temp t1 inner join tblviolations v                    
   on t1.SettingTypeName = v.description                    
   and violationtype = (                    
   select violationtype from tblcourtviolationtype                    
   where courtid = 3077 -- 3071 tahir 5851 04/28/2009 fixed the courtid issue...
   )                    
                    
-------------------------------------------                
--------- GETTING PRICE PLAN --------------        
-------------------------------------------                   
  select  @PlanId =                     
   (                    
   select  top 1 planid                     
   from  tblpriceplans                     
   -- tahir 
   where courtid = 3077 --3071  tahir 5851 04/28/2009 fixed the court id issue..
   and  effectivedate <= getdate()                    
   and enddate > = getdate()                    
   and isActivePlan = 1                    
   order by planid                    
   )                    
-------------------------------------------------------            
--NOW SAVING CASE STATUS IN TRAFFIC SYSTEM IF NOT EXIST        
-------------------------------------------------------          
print('Test')        
        
declare @CourtViolationStatusID as int         
        
  if not exists ( Select top 1  Description from tblcourtviolationstatus where Description in ( Select top 1  CaseStatusName from @temp ))        
 Begin        
  Insert Into tblcourtviolationstatus ( Description ,ShortDescription, CategoryID ,CourtStatusUpdateDate,SortOrder)        
   Select  top 1   left ( CaseStatusName,50) , left(CaseStatusName,3) , 60 , getdate(),0 from @temp         
    
    --ozair Task 4767 09/11/2008 replace @@Identity with Scope_Identity   
    --as @@Identity returns the last inserted identity value instead of the insert statement executed in current scope  
    --Set @CourtViolationStatusID = @@identity         
    Set @CourtViolationStatusID = SCOPE_IDENTITY()         
 End        
 else        
 Begin        
  Select top 1  @CourtViolationStatusID = CourtViolationStatusId from tblcourtviolationstatus where Description in ( Select top 1 CaseStatusName from @temp )        
 End          
        
print('Tst Emd')                  
--------------------------------------------------------                    
-- INSERTING VIOLATION DETAILS IN TBLTICKETSVIOLATIONS--        
--------------------------------------------------------                        
insert into tblticketsviolations                        
(                        
 TicketID_PK,         
 ViolationNumber_PK,                        
 FineAmount,                        
 casenumassignedbycourt,                          
 refcasenumber ,               
 bondamount,                        
 violationdescription,                    
-- tahir 4763 09/08/2008 default court & coverage attorney...  
 courtid, coveringfirmid,  
-- end 4763      
 PlanId ,                    
 recordid ,            
 CourtDateMain ,        
 CourtViolationStatusIDMain,      
CourtNumbermain,      
CourtDate ,        
CourtViolationStatusID,      
CourtNumber,      
      
CourtDateScan ,        
CourtViolationStatusIDScan,      
CourtNumberScan      
                      
)                          
SELECT  distinct top 1                           
 @TicketID,                        
 violationNumber_pk,                        
 0,                        
 casenumber,                        
 casenumber,                        
 0,                        
 SettingTypeName ,  
-- tahir 4763 09/08/2008 default court & coverage attorney...                  
-- 3071,                     
3077, 3000, --Sabir Khan 6025 07/02/2009 Default covering firm has been changed for Family cases...  
-- end 4763         
 isnull(@PlanId,45),                    
 -- tahir 4868 09/26/2008 
 --0,            
 @Partyid, 
 -- end 4868
--Sabir Khan 5010 10/21/2008 Set Pre Trial status and 09:00AM court time for family law cases  
--Sabir Khan 6264 08/12/2009 Court has been changed from 09:00:00 to 08:00:00
 isnull(convert(Varchar(10), nextsettingdate, 101)  + ' 08:00:00','1/1/1900 09:00:00.000'),          
101 ,        
Substring (Convert(varchar,ChildCustodyCourtId ) ,4,1)         
,              
 isnull(convert(Varchar(10), nextsettingdate, 101)  + ' 08:00:00','1/1/1900 09:00:00.000'),          
101 ,        
Substring (Convert(varchar,ChildCustodyCourtId ) ,4,1)         
,              
 isnull(convert(Varchar(10), nextsettingdate, 101)  + ' 08:00:00','1/1/1900 09:00:00.000'),          
101 ,        
Substring (Convert(varchar,ChildCustodyCourtId ) ,4,1)         
from  @temp1                              
                   
if @@error <> 0                     
set @errornumber = @@error           
                                   
     -- UPDATING CLIENT STATUS IN JIMS.............                        
     update c                          
     set  c.clientflag = 1                          
     from  @temp1 t2,                         
      Civil.dbo.ChildCustody C                          
     where  t2.casenumber = C.casenumber                          
                      
                    
  if @@error <> 0                     
   set @errornumber = @@error                             
                    
                               
     -- CREATING HISTORY FOR THE CASE...................                        
     insert into tblticketsnotes(ticketid,subject,employeeid) values(@TicketID,'INITIAL INQUIRY CIVIL COURT : ARR :',@employeeid)                          
                 
  if @@error <> 0              set @errornumber = @@error                             
                      
     end                        
                    
  if @Errornumber = 0                    
   commit tran                    
  else                    
   rollback tran                    
end                          
                    
                    
if isnull(@Ticketid ,0) = 0                     
 select @Ticketid = @RecCount                    
   
-- tahir 4868 09/26/2008 update mailer id...                 
-- UPDATE MAILER ID IN TBLTICKETS......                    
EXEC dbo.USP_HTS_Update_Mailer_id @TicketID, @employeeid,  5                    
-- end 4868
                    
select @sOutPut = convert(varchar(100),@ticketid) + ','+  convert(varchar(3),@iFlag)                    
          
          
   
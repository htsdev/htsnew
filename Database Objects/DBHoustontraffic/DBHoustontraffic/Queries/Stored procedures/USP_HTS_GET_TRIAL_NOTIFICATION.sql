﻿/************************************************************
    Business Logic: This procedure is used to get data for Trial Notification Letter.
    
    Input Parameters:
    
    @empid 
	@LetterType 
	@TicketIDList 
 ************************************************************/

alter PROCEDURE [dbo].[USP_HTS_GET_TRIAL_NOTIFICATION] --3991,2,'4707104201,'
	@empid INT,
	@LetterType INT,
	@TicketIDList VARCHAR(MAX)
AS
BEGIN
    DECLARE @tickets TABLE (ticketid NUMERIC)             
    
    INSERT INTO @tickets
    SELECT *
    FROM   dbo.Sap_String_Param_New(@TicketIDList) 
    
    --select * from @tickets          
    
    /*select *  from tbl_hts_batch_trialletter           
    where cast(convert(varchar(12), ticketid_pk) + convert(varchar(12), BatchID) as int) IN (select ticketid from @tickets)            
    order by BatchID      */ --original code comment by Azwer..      
    --Yasir Kamal 7590 04/01/2010 check Discrepancy in court date.          
    SELECT * INTO #temp
    FROM   tbl_hts_batch_trialletter
    WHERE  CAST(
               CONVERT(VARCHAR(12), tbl_hts_batch_trialletter.ticketid_pk) + 
               CONVERT(VARCHAR(12), tbl_hts_batch_trialletter.BatchID) AS 
               NUMERIC
           )       
           
           IN (SELECT ticketid
               FROM   @tickets)
           AND tbl_hts_batch_trialletter.ticketid_pk NOT IN (SELECT DISTINCT 
                                                                    ticketid_pk
                                                             FROM   
                                                                    tblticketsflag
                                                             WHERE  flagid = 7)
    ORDER BY
           BatchID
  
  --Yasir Kamal 7590 04/01/2010 check Discrepancy in court date.          
                      
ALTER TABLE #temp 
	ADD isDiscrip INT 
	
UPDATE #temp SET 
	isDiscrip = 0
	 

 
 
SELECT distinct a.TicketID_PK INTO #temp2 
from #temp a inner join #temp b on a.TicketID_PK = b.TicketID_PK
where a.courtdate <> b.courtdate


UPDATE a SET  a.isDiscrip = 1
FROM  #temp a INNER JOIN #temp2 v ON a.TicketID_PK = v.TicketID_PK

 
--Farrukh 10272 05/10/2012 Replaced violationDate from 1/1/9100 to empty string       
SELECT batchid,TicketID_PK,TicketNumber_PK,FirstName,MiddleName,LastName,Address1,Address2,Ccity,Cstate,CZip,
CourtDate,CourtNumber,[ADDRESS],City,[STATE],Zip,DateTypeFlag,ActiveFlag,Languagespeak,SequenceNumber,CategoryID,
(CASE WHEN DATEDIFF(DAY,ISNULL(TicketViolationDate,'1/1/1900'),'1/1/1900') <> 0 THEN CONVERT(VARCHAR(10),TicketViolationDate,101) ELSE '' END) AS TicketViolationDate,
[DESCRIPTION],CaseStatus,courtid,courtviolationstatusid,isDiscrip FROM #temp         

           

DROP TABLE #temp           
DROP TABLE #temp2    
END
GO


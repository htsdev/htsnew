Alter procedure [dbo].[USP_HTS_GET_ViolationUpdateInfo]           
          
 (          
 @TicketsViolationID int          
 )          
          
as          
          
  declare @fticketCount as int        
 set  @fticketCount = 0        
        
SELECT 
 tv.RefCaseNumber as RefCaseNumber,  
 isnull(tv.casenumassignedbycourt,'') as CauseNumber,           
 isnull(convert(varchar,tv.FineAmount),0) as FineAmount,           
 isnull(convert(varchar,tv.BondAmount),0) as BondAmount,           
 isnull(v.ViolationNumber_PK,0) as ViolationID,          
 v.Description as ViolationDescription,           
 isnull(tv.UnderlyingBondFlag,0) as BondFlag,           
 isnull(tv.PlanID,0) as PlanId,           
 isnull(c1.[Description],'') AS AutoStatus,     
 isnull(c1.CourtViolationStatusID,0) AS AutoStatusID,                
 isnull(c3.CourtViolationStatusID,0) As VerifiedStatusID,          
 isnull(c3.[Description],'') AS VerifiedStatus,           
 isnull(c2.[Description],'') AS ScanStatus,           
 Convert(varchar,isnull(tv.CourtDate,''),101)+ ' ' + substring(CONVERT(varchar,tv.CourtDate),13,7) AS CourtDateAuto,           
 Convert(varchar,isnull(tv.CourtDateMain,''),101)+ ' ' + substring(CONVERT(varchar,tv.CourtDateMain),13,7) AS CourtDateVerified,           
 Convert(varchar,isnull(tv.CourtDateScan,''),101)+ ' ' + substring(CONVERT(varchar,tv.CourtDateScan),13,7) AS CourtDateScan,           
 isnull(tv.CourtNumber,'') AS CourtNumberAuto,           
 isnull(tv.CourtNumbermain,'') AS CourtNumberVerified,           
 isnull(tv.CourtNumberscan,'') AS CourtNumberScan,           
 crt.CourtID as CourtID,          
 crt.shortcourtname AS CourtName,
 isnull(crt.inactiveflag,0) as inactive,           
 ltrim(isnull(o.LastName,'')+' '+isnull(o.FirstName,'')) as OfficerName,            
 isnull(t.BondFlag,0) AS BondFlagMain,           
 isnull(o.OfficerType,'N/A') as OfficerDay,          
 SetDate = isnull(Convert(varchar,dbo.fn_courtdesiredsetdate(t.ticketid_pk),101),''),         
 FticketCount =  (        
  case          
   when dbo.FTicketsCountbyViolationID(ticketsviolationid) > 0 then 1          
   else 0          
  end  )   ,
isnull( C3.DESCRIPTION,'N/A') AS CASEDESCRIPTION 
           
FROM dbo.tblTicketsViolations tv           
INNER JOIN          
 dbo.tblViolations v           
ON   tv.ViolationNumber_PK = v.ViolationNumber_PK           
left outer JOIN          
 dbo.tblCourts crt           
ON   tv.CourtID = crt.Courtid           
INNER JOIN          
 dbo.tblTickets t          
ON   tv.TicketID_PK = t.TicketID_PK           
LEFT OUTER JOIN          
 dbo.tblOfficer  o          
ON   t.OfficerNumber = o.OfficerNumber_PK           
LEFT OUTER JOIN          
 dbo.tblCourtViolationStatus c3           
INNER JOIN          
 dbo.tblDateType d3           
ON   c3.CategoryID = d3.TypeID           
ON   tv.CourtViolationStatusIDmain = c3.CourtViolationStatusID           
LEFT OUTER JOIN          
 dbo.tblDateType d1           
INNER JOIN          
  dbo.tblCourtViolationStatus c1           
ON   d1.TypeID = c1.CategoryID           
ON   tv.CourtViolationStatusID = c1.CourtViolationStatusID           
LEFT OUTER JOIN          
  dbo.tblCourtViolationStatus c2           
INNER JOIN          
  dbo.tblDateType d2           
ON   c2.CategoryID = d2.TypeID           
ON   tv.CourtViolationStatusIDScan = c2.CourtViolationStatusID          
          
where   tv.TicketsViolationID = @TicketsViolationID    
    
GO

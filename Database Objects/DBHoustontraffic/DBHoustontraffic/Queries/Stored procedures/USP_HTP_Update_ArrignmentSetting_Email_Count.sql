﻿/************

Zeeshan 4420 08/11/2008 Create procedure to update Arrignment Setting Email Count

Created By Zeeshan Ahmed 

Business Logic : This procedure is used to update email count of the cases for which Arraignment Setting Request has sent.

List of Parameters 
------------------
@Ticketids : Case Numbers

List Of Columns
---------------
No of Records Affected

*************/

Create Procedure dbo.USP_HTP_Update_ArrignmentSetting_Email_Count
@Ticketids  varchar(max)

as

Update tbltickets set ArrSetEmailCount = isnull(ArrSetEmailCount,0) + 1 
where ticketid_pk in (select maincat from dbo.Sap_String_Param_bigint(@Ticketids)) 

select @@rowcount

go

grant execute on USP_HTP_Update_ArrignmentSetting_Email_Count to dbr_webuser

go
USE TrafficTickets
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rab Nawaz Khan
-- Create date: 10/02/2013
-- Task ID:		11431
-- Description:	This Procedure is used to update the No new address flage agains the non-clients who has'nt any new address
-- =============================================
ALTER PROCEDURE USP_HTP_UpdateNoNewAddress 
	
	@recordId INT,
	@lastName VARCHAR(100) = NULL,
	@firstName VARCHAR(100) = NULL,
	@empId INT = 3992
AS
BEGIN
	DECLARE @historyNote VARCHAR(500)
	SET @historyNote = ''
	-- If Record Already exists in new address then update else Insert record and mark as No New Address.
	IF ((SELECT COUNT(RecordId) FROM tblNewAddresses WHERE RecordId = @recordId) > 0)
	BEGIN
		UPDATE tblNewAddresses
		SET IsNoNewAddressMarked = 1, UpdatedBy = @empId, UpdateDate = GETDATE(),
		[Address1] = NULL, [City1] = NULL, [State1] = NULL, [ZipCode1] = NULL, [DP2_1] = NULL, [DPC_1] = NULL, [AddressStatus1] = NULL, 
		[Address2] = NULL ,[City2] = NULL, [State2] = NULL, [ZipCode2] = NULL, [DP2_2] = NULL,  [DPC_2] = NULL, [AddressStatus2] = NULL
		WHERE RecordId = @recordId
		
		SET @historyNote = 'Record Has been marked as ''No New Address'''
		INSERT INTO [TrafficTickets].[dbo].[tblNewAddressesHisotry] (Activity, RecordId, Insertdate, UserId)
		VALUES (@historyNote, @RecordId, GETDATE(), @empId)
	END
    ELSE
    	BEGIN
    		INSERT INTO tblNewAddresses (RecordId, LastName, FirstName, InsertedBy1, InsertDate1, IsNoNewAddressMarked)
    		VALUES (@recordId, @lastName, @firstName, @empId, GETDATE(), 1)
    		
    		SET @historyNote = 'Record Has been inserted as ''No New Address'''
			INSERT INTO [TrafficTickets].[dbo].[tblNewAddressesHisotry] (Activity, RecordId, Insertdate, UserId)
			VALUES (@historyNote, @RecordId, GETDATE(), @empId)
    	END
END
GO
	GRANT EXECUTE ON USP_HTP_UpdateNoNewAddress TO dbr_webuser
GO 

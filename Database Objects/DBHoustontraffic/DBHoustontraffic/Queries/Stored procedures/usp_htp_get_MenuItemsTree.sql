﻿/*  
* Created By  : Waqas Javed  
* Task ID   : 7077  
* Created Date : 12/14/2009  
* Business logic : This procedure is used to get menu items all for menu configuration  
*/  
  
  
ALTER PROCEDURE [dbo].[usp_htp_get_MenuItemsTree]    
AS    
    
BEGIN    
     
    
SELECT (CASE LTRIM(RTRIM(ISNULL(ttm.TITLE, ''))) WHEN '' THEN 'Not Available' ELSE ttm.TITLE END ) AS [FIRST],   
(CASE LTRIM(RTRIM(ISNULL(tts.TITLE, ''))) WHEN '' THEN 'Not Available' ELSE tts.TITLE END ) AS [SECOND], ttm.ID AS FirstID, tts.ID AS SecondID    
  FROM tbl_TAB_SUBMENU tts RIGHT JOIN tbl_TAB_MENU ttm ON ttm.ID = tts.MENUID    
      
ORDER BY ttm.ORDERING, tts.ORDERING    
    
END    
  
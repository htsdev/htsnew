SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_HTS_Check_TicketNumber_Existance]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_HTS_Check_TicketNumber_Existance]
GO







/* procedure to check the existance of ticket number.*/            
            
CREATE     procedure [dbo].usp_HTS_Check_TicketNumber_Existance           
            
@tvid	int,
@ticketnumber varchar(20),   --ticket number entered
@SeqNo	varchar(5),	--seq no to check in existing ticket            
@ticketid int,        --current ticket id            
@courtid int,   --current courtid            
@existingticket varchar(20)   -- return zero if not found            
            
as              
            
set nocount on              
            
declare @tempticket varchar(20)              
declare @type int      
            
if @tvid=0
begin
	select 	@tempticket = tv.RefCaseNumber,      
		@type=isnull(t.activeflag,0)      
	from 	tblticketsviolations tv inner join       
		tbltickets t on       
		tv.ticketid_pk=t.ticketid_pk      
	where 	tv.RefCaseNumber = @ticketnumber             
	and 	isnull(tv.sequencenumber,'') = @SeqNo
	and 	tv.courtid=@courtid             
	and 	tv.ticketid_pk in (@ticketid)               
end

if @tvid<>0
begin
	select 	@tempticket = tv.RefCaseNumber,      
		@type=isnull(t.activeflag,0)      
	from 	tblticketsviolations tv inner join       
		tbltickets t on       
		tv.ticketid_pk=t.ticketid_pk      
	where 	tv.RefCaseNumber = @ticketnumber             
	and 	isnull(tv.sequencenumber,'') = @SeqNo
	and 	tv.courtid=@courtid             
	and 	tv.ticketid_pk in (@ticketid)
	and	tv.ticketsviolationid <>@tvid               
end

if isnull(@tempticket,'0') = '0' 
begin
	select 	@tempticket = tv.RefCaseNumber,      
 		@type=isnull(t.activeflag,0)      
	from 	tblticketsviolations tv inner join       
 		tbltickets t on       
 		tv.ticketid_pk=t.ticketid_pk      
	where 	tv.RefCaseNumber = @ticketnumber             
	and 	tv.courtid=@courtid             
	and 	tv.ticketid_pk not in (@ticketid)               
end            
/*bug#1212 changes by saher    
if isnull(@tempticket,'0') = '0'              
 begin              
   select @tempticket = isnull(ticketnumber,0),      
 @type=2             
   from tblticketsarchive             
   where ticketnumber = @ticketnumber             
   and courtid=@courtid             
   and clientflag = 0              
 end              
*/    
set @existingticket = isnull(@tempticket,'0')           
          
select @existingticket,@type          
        
        
      
      
    
  





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


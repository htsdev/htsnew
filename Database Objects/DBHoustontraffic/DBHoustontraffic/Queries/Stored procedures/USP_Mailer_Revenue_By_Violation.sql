--USE TrafficTickets
/*
* Sabir Khan 7723 05/07/2010
* Business Logic:
* This stored procedure is used to get total number of clients, non-clients, quote clients, Mailer Name and also can find the total expense, revenue and profit 
* for sepecific violations description of spacific mailer in the a spacified date range.
*/
--USP_Mailer_Revenue_By_Violation '06/01/2010','06/30/2010','Failure To Appear',18
--Sabir Khan 7978 07/29/2010 Modifed whole procedure

ALTER PROCEDURE USP_Mailer_Revenue_By_Violation

@startdate DATETIME,
@enddate DATETIME,
@letterid INT,
@violations VARCHAR(MAX),
@SingleMultiple INT

AS

DECLARE @categoryID INT
---Get Category ID based on letter type.
IF @letterid = -1
	BEGIN
		SET @categoryID = -1
		
	END
ELSE
	BEGIN
		SELECT @categoryID = courtcategory FROM tblLetter WHERE LetterID_PK = @letterid	
		 	
	END

---Dallas Section
IF @categoryID IN (6,11,16,25,27,28,-1)
BEGIN
	--For All Mailers
	IF @letterid = -1
	BEGIN
			---For Dallas when all mailer is selected
			CREATE TABLE #tempDallas(LetterName VARCHAR(200),NoteID INT,RecordID int,expense MONEY)
			INSERT INTO #tempDallas
			select DISTINCT c.courtcategoryname + ' - '+ l.lettername AS LetterName, tn.NoteId,tn.RecordID, tn.pcost as expense
			from tblletternotes tn 
			inner JOIN DallasTrafficTickets.dbo.tblticketsviolationsarchive tva on tva.recordid = tn.recordid
			inner join DallasTrafficTickets.dbo.tblticketsarchive tv on tv.recordid = tva.recordid
			inner join tblletter l on l.letterid_pk = tn.lettertype
			inner join tblcourtcategories c on c.courtcategorynum = l.courtcategory 
			where tva.violationdescription like @violations + '%'
			and isnull(l.isactive,0) = 1
			and c.isactive =1
			and datediff(day,recordloaddate,@startdate) <=0
			and datediff(day,recordloaddate,@enddate)>=0
			
			---For houston when all mailer is selected
			CREATE TABLE #tempHouston(LetterName VARCHAR(200),NoteID INT,RecordID int,expense MONEY)	
			INSERT INTO #tempHouston
			select distinct c.courtcategoryname + ' - '+ l.lettername AS LetterName,tn.NoteId,tn.RecordID, tn.pcost as expense from tblletternotes tn 
			inner join tblticketsviolationsarchive tva on tva.recordid = tn.recordid
			inner join tblticketsarchive tv on tv.recordid = tva.recordid
			inner join tblletter l on l.letterid_pk = tn.lettertype
			inner join tblcourtcategories c on c.courtcategorynum = l.courtcategory 
			where tva.violationdescription like @violations + '%'
			AND c.CourtCategorynum NOT IN (6,11,16,25,27,28)
			--and tn.lettertype = @letterid 
			and isnull(l.isactive,0) = 1
			and datediff(day,recordloaddate,@startdate) <=0
			and datediff(day,recordloaddate,@enddate)>=0	
	END
	ELSE
	BEGIN -- For Selected Mailer for dallas
			CREATE table #temp(NoteID INT,RecordID int,expense MONEY)		
			INSERT INTO #temp
			select distinct tn.NoteId,tn.RecordID, tn.pcost as expense from tblletternotes tn 
			inner JOIN DallasTrafficTickets.dbo.tblticketsviolationsarchive tva on tva.recordid = tn.recordid
			inner join DallasTrafficTickets.dbo.tblticketsarchive tv on tv.recordid = tva.recordid
			inner join tblletter l on l.letterid_pk = tn.lettertype
			where tva.violationdescription like @violations + '%'
			and tn.lettertype = @letterid 
			and isnull(l.isactive,0) = 1
			and datediff(day,recordloaddate,@startdate) <=0
			and datediff(day,recordloaddate,@enddate)>=0
	END
	---For Selected mailer
	IF @letterid <> -1
	BEGIN
		IF @SingleMultiple = 1 --When single violation is selected for dallas mailers
			BEGIN
				SELECT count(tva.ticketnumber_pk) AS ticketnumber,t.noteid, tva.recordid INTO #single  
				FROM dallastraffictickets.dbo.tblTicketsViolationsArchive tva INNER JOIN #temp t ON tva.RecordID = t.recordid 
				GROUP BY tva.recordid,t.noteid HAVING COUNT(tva.TicketNumber_PK)>1			
				
				DELETE FROM #temp WHERE recordid IN (SELECT recordid FROM #single) 
				DROP TABLE #single
			END
		ELSE
			BEGIN  ---When multiple violation is seleceted for Dallas Mailers
				SELECT count(tva.ticketnumber_pk) AS ticketnumber,t.noteid, tva.recordid INTO #multiple  
				FROM dallastraffictickets.dbo.tblTicketsViolationsArchive tva INNER JOIN #temp t ON tva.RecordID = t.recordid 
				GROUP BY tva.recordid,t.noteid HAVING COUNT(tva.TicketNumber_PK) = 1
				DELETE FROM #temp WHERE recordid IN (SELECT recordid FROM #multiple) 
				DROP TABLE #multiple
			END	
			
		--Add Columns 
		alter table #temp add ticketid int, isclient tinyint, isquote tinyint, revenue money, isnonclient tinyint, profit money
		update #temp set ticketid= 0, isclient = 0, isquote= 0, revenue = 0, isnonclient=0, profit = 0
		
		--Creating index
		CREATE UNIQUE CLUSTERED INDEX IX_1 on #temp(noteid)
		
		-- Update Ticket ID ,clients and Quote Client...
		update m
		set m.ticketid = t.ticketid_pk,
		isclient = case t.activeflag when 1 then 1 else 0 end ,
		isquote = case t.activeflag when 0 then 1 else 0 end
		from #temp m inner JOIN DallasTrafficTickets.dbo.tbltickets t on t.mailerid = m.noteid

		-- Updating Non clients
		update #temp set isnonclient = 1 where ticketid = 0
		select ticketid, isnull(sum(isnull(chargeamount,0)),0) fee
		into #payments
		FROM DallasTrafficTickets.dbo.tblticketspayment 
		where paymentvoid =0
		group by ticketid
		having isnull(sum(isnull(chargeamount,0)),0) > 0
		order by ticketid
	
		-- Calculating revenue
		update m
		set m.revenue =p.fee
		from #temp m inner join #payments p on p.ticketid = m.ticketid

		-- Calculating Profit
		update #temp set profit = isnull(revenue,0)  - isnull( expense,0)
		
		--Displaying result
		select count(m.noteid) [Mailer Count], cast(sum(expense) as decimal(10,2)) [Expense($)], sum(isclient) [Hires], sum(isquote) [Calls],sum(isnonclient) [Non Client], cast(sum(revenue) as decimal(10,2)) [Revenue($)], cast(sum(profit) as decimal(10,2)) [Profit($)]
		from #temp m 

		drop table #temp
		drop table #payments
		
	END
	ELSE
	BEGIN --For All Mailers
		IF @SingleMultiple = 1 --For Single Violation for dallas for all mailers
		BEGIN
			SELECT count(tva.ticketnumber_pk) AS ticketnumber,t.noteid, tva.recordid INTO #singleDallas  
			FROM dallastraffictickets.dbo.tblTicketsViolationsArchive tva INNER JOIN #tempDallas t ON tva.RecordID = t.recordid 
			GROUP BY tva.recordid,t.noteid HAVING COUNT(tva.TicketNumber_PK)>1 
					
			DELETE FROM #tempDallas WHERE recordid IN (SELECT RecordID FROM #singleDallas)
			DROP TABLE #singleDallas
					 
			--For Single Violation for houston for all mailers
			SELECT count(tva.ticketnumber_pk) AS ticketnumber,t.noteid, tva.recordid INTO #singleHouston  
			FROM tblTicketsViolationsArchive tva INNER JOIN #tempHouston t ON tva.RecordID = t.recordid 
			GROUP BY tva.recordid,t.noteid HAVING COUNT(tva.ticketnumber_pk)>1 
			
			DELETE FROM #tempHouston WHERE recordid IN (SELECT recordid FROM #singleHouston)
			DROP TABLE #singleHouston 
		END
		ELSE
			BEGIN  --For Multiple Violation for dallas for all mailers
				SELECT count(tva.ticketnumber_pk) AS ticketnumber,t.noteid, tva.recordid INTO #MultipleDallas  
				FROM dallastraffictickets.dbo.tblTicketsViolationsArchive tva INNER JOIN #tempDallas t ON tva.RecordID = t.recordid 
				GROUP BY tva.recordid,t.noteid HAVING COUNT(tva.ticketnumber_pk)=1 
						
				DELETE FROM #tempDallas WHERE recordid IN (SELECT RecordID FROM #MultipleDallas)
				DROP TABLE #MultipleDallas
				--For Multiple Violation for houston for all mailers		 
				SELECT count(tva.ticketnumber_pk) AS ticketnumber,t.noteid, tva.recordid INTO #MultipleHouston  
				FROM tblTicketsViolationsArchive tva INNER JOIN #tempHouston t ON tva.RecordID = t.recordid 
				GROUP BY tva.recordid,t.noteid HAVING COUNT(tva.ticketnumber_pk)=1 
				
				DELETE FROM #tempHouston WHERE recordid IN (SELECT recordid FROM #MultipleHouston)
				DROP TABLE #MultipleHouston 
			END
		
		alter table #tempDallas add ticketid int, isclient tinyint, isquote tinyint, revenue money, isnonclient tinyint, profit MONEY
		alter table #tempHouston add ticketid int, isclient tinyint, isquote tinyint, revenue money, isnonclient tinyint, profit money
		update #tempDallas set ticketid= 0, isclient = 0, isquote= 0, revenue = 0, isnonclient=0, profit = 0
		update #tempHouston set ticketid= 0, isclient = 0, isquote= 0, revenue = 0, isnonclient=0, profit = 0
		
		update m
		set m.ticketid = t.ticketid_pk,
		isclient = case t.activeflag when 1 then 1 else 0 end ,
		isquote = case t.activeflag when 0 then 1 else 0 end
		from #tempDallas m inner JOIN DallasTrafficTickets.dbo.tbltickets t on t.mailerid = m.noteid
		
		
		update m
		set m.ticketid = t.ticketid_pk,
		isclient = case t.activeflag when 1 then 1 else 0 end ,
		isquote = case t.activeflag when 0 then 1 else 0 end
		from #tempHouston m inner JOIN tbltickets t on t.mailerid = m.noteid
		
		
	
		-- Updating Non clients
		update #tempDallas set isnonclient = 1 where ticketid = 0
		select ticketid, isnull(sum(isnull(chargeamount,0)),0) fee
		into #paymentsDallas
		FROM DallasTrafficTickets.dbo.tblticketspayment 
		where paymentvoid =0
		group by ticketid
		having isnull(sum(isnull(chargeamount,0)),0) > 0
		order by ticketid
		
		-- Updating Non clients
		update #tempHouston set isnonclient = 1 where ticketid = 0
		select ticketid, isnull(sum(isnull(chargeamount,0)),0) fee
		into #paymentsHouston
		FROM tblticketspayment 
		where paymentvoid =0
		group by ticketid
		having isnull(sum(isnull(chargeamount,0)),0) > 0
		order by ticketid
	
		-- Calculating revenue
		update m
		set m.revenue =p.fee
		from #tempDallas m inner join #paymentsDallas p on p.ticketid = m.ticketid
		
		-- Calculating revenue
		update m
		set m.revenue =p.fee
		from #tempHouston m inner join #paymentsHouston p on p.ticketid = m.ticketid
		
		
		-- Calculating Profit
		update #tempDallas set profit = isnull(revenue,0)  - isnull( expense,0)
		
		-- Calculating Profit
		update #tempHouston set profit = isnull(revenue,0)  - isnull( expense,0)
		
		CREATE TABLE #result(LetterName VARCHAR(200), noteid  INT ,expense  DECIMAL,hire  INT,call  INT,nonclient INT,revenue  DECIMAL, profit  DECIMAL)
		--Displaying result
		INSERT INTO #result
		SELECT lettername, count(m.noteid) AS noteid, cast(sum(expense) as decimal(10,2)) AS expense, sum(isclient) AS hire, sum(isquote) AS call ,sum(isnonclient) AS nonclient, cast(sum(revenue) as decimal(10,2)) AS revenue, cast(sum(profit) as decimal(10,2)) AS profit
		from #tempDallas m GROUP BY lettername
		
		INSERT INTO #result
		SELECT lettername, count(m.noteid) AS noteid, cast(sum(expense) as decimal(10,2)) AS expense, sum(isclient) AS hire, sum(isquote) AS call ,sum(isnonclient) AS nonclient, cast(sum(revenue) as decimal(10,2)) AS revenue, cast(sum(profit) as decimal(10,2)) AS profit
		from #tempHouston m GROUP BY lettername
		
		--Displaying result
		SELECT LetterName, noteid [Mailer Count], expense [Expense($)], hire [Hires], call [Calls],nonclient [Non Client], revenue [Revenue($)], profit [Profit($)]
		from #result m

		drop table #result
		drop table #paymentsDallas
		drop table #paymentsHouston
		DROP TABLE #tempHouston
		DROP TABLE #tempDallas
		
		END
		
END
ELSE
BEGIN
			CREATE table #temp1(NoteID INT,RecordID int,expense MONEY)		
			INSERT INTO #temp1
			select distinct tn.NoteId,tn.RecordID, tn.pcost as expense from tblletternotes tn 
			inner join tblticketsviolationsarchive tva on tva.recordid = tn.recordid
			inner join tblticketsarchive tv on tv.recordid = tva.recordid
			inner join tblletter l on l.letterid_pk = tn.lettertype
			where tva.violationdescription like @violations + '%' 
			and tn.lettertype = @letterid 
			and isnull(l.isactive,0) = 1
			and datediff(day,recordloaddate,@startdate) <=0
			and datediff(day,recordloaddate,@enddate)>=0



		IF @SingleMultiple = 1
		BEGIN
			SELECT count(tva.ticketnumber_pk) AS ticketnumber,t.noteid, tva.recordid INTO #single1  
			FROM tblTicketsViolationsArchive tva INNER JOIN #temp1 t ON tva.RecordID = t.recordid 
			GROUP BY tva.recordid,t.noteid HAVING COUNT(tva.ticketnumber_pk)>1 
			DELETE FROM #temp1 WHERE recordid IN (SELECT recordid FROM #single1) 
			DROP TABLE #single1
		END
		ELSE
		BEGIN	
			SELECT count(tva.ticketnumber_pk) AS ticketnumber,t.noteid, tva.recordid INTO #single2  
			FROM tblTicketsViolationsArchive tva INNER JOIN #temp1 t ON tva.RecordID = t.recordid 
			GROUP BY tva.recordid,t.noteid HAVING COUNT(tva.ticketnumber_pk)=1 
			DELETE FROM #temp1 WHERE recordid IN (SELECT recordid FROM #single2) 
			DROP TABLE #single2
		END
		alter table #temp1 add ticketid int, isclient tinyint, isquote tinyint, revenue money, isnonclient tinyint, profit money
		update #temp1 set ticketid= 0, isclient = 0, isquote= 0, revenue = 0, isnonclient=0, profit = 0
	
		CREATE UNIQUE CLUSTERED INDEX IX_1 on #temp1(noteid)
	
		update m
		set m.ticketid = t.ticketid_pk,
		isclient = case t.activeflag when 1 then 1 else 0 end ,
		isquote = case t.activeflag when 0 then 1 else 0 end
		from #temp1 m inner join tbltickets t on t.mailerid = m.noteid

		update #temp1 set isnonclient = 1 where ticketid = 0
		select ticketid, isnull(sum(isnull(chargeamount,0)),0) fee
		into #payments1
		from tblticketspayment 
		where paymentvoid =0
		group by ticketid
		having isnull(sum(isnull(chargeamount,0)),0) > 0
		order by ticketid
	
	
		update m
		set m.revenue =p.fee
		from #temp1 m inner join #payments1 p on p.ticketid = m.ticketid

		update #temp1 set profit = isnull(revenue,0)  - isnull( expense,0)

		select count(m.noteid) [Mailer Count], cast(sum(expense) as decimal(10,2)) [Expense($)], sum(isclient) [Hires], sum(isquote) [Calls],sum(isnonclient) [Non Client], cast(sum(revenue) as decimal(10,2)) [Revenue($)], cast(sum(profit) as decimal(10,2)) [Profit($)]
		from #temp1 m 

		drop table #temp1
		drop table #payments1
		
END



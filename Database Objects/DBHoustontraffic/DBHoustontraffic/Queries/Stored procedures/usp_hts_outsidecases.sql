USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[usp_hts_outsidecases]    Script Date: 01/02/2014 04:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************
Modified By: Noufil Khan
Create Date: 09/08/2007

Business Logic:
	This report display all records 
			Whose verified status in waiting or AutoStatus in 'DLQ' or 'WAR' or 'BONDS' or 'Dispose' or 'NonIssue'			

Input Parameters:
	None.

Output Columns:
	ticketid_pk:		identification of the case in database    
	lastname:			last name of the client
	firstname:			first name of the client
	refcasenumber:		ticket number for the case	
	shortname:			short court name
	autostatus:			case status updated by the loader
	verifiedcourtstatus:verified case status udpated by the user
	autocourtdate:		court date updated by the loader
	verifiedcourtdate:	court date updated by the user	
	WaitTime:			time in days for which the case is in discrepancy	
	bondflag:			flag for bond status if client needs bonds
	LORPrintDate:		date when last time Letter of Rep printed
	LORSortDate:		max. LOR print date

********************************************/

       
ALTER PROCEDURE [dbo].[usp_hts_outsidecases]
	@showAll BIT = 0
AS
SET NOCOUNT ON;

	-- Saeed 8101 09/28/2010 get report id from page url.
	DECLARE @reportId INT
	SELECT @reportId = Report_ID
	FROM   tbl_Report WITH(NOLOCK)
	WHERE  Report_Url = '/Reports/Outsidecases.aspx'
	
	SELECT DISTINCT 
	       t.TicketID_PK AS ticketid_pk,
	       lastname = CASE 
	                       WHEN LEN(ISNULL(t.Lastname, '')) > 10 THEN SUBSTRING(t.Lastname, 0, 10)
	                            + '...'
	                       ELSE t.Lastname
	                  END,
	       firstname = CASE 
	                        WHEN LEN(ISNULL(t.Firstname, '')) > 10 THEN 
	                             SUBSTRING(t.Firstname, 0, 10) 
	                             + '...'
	                        ELSE t.Firstname
	                   END,
	       refcasenumber = CASE 
	                            WHEN LEN(ISNULL(tv.casenumassignedbycourt, '')) 
	                                 = 0 THEN tv.refcasenumber
	                            ELSE tv.casenumassignedbycourt
	                       END,
	       c.ShortName AS shortname,
	       cvs.ShortDescription AS autostatus,
	       tcv2.ShortDescription AS verifiedcourtstatus
	       -- Asad 6507 03/18/2010 Date Format issue fixed
	       ,
	       dbo.fn_DateFormat(tv.CourtDate, 30, '/', ':', 1) AS autocourtdate,
	       dbo.fn_DateFormat(tv.CourtDateMain, 30, '/', ':', 1) AS 
	       verifiedcourtdate,
	       WaitTime = ISNULL(
	           DATEDIFF(
	               DAY,
	               (
	                   SELECT MIN(recdate)
	                   FROM   tblticketsviolationlog WITH(NOLOCK)
	                   WHERE  ticketviolationid = tv.ticketsviolationid
	                          AND oldverifiedstatus <> newverifiedstatus
	                          AND newverifiedstatus = tv.courtviolationstatusidmain
	               ),
	               GETDATE()
	           ),
	           0
	       ),
	       (CASE WHEN tv.underlyingbondflag = 1 THEN 'B' ELSE '' END) AS 
	       bondflag,
	       LORPrintDate = (
	           SELECT CONVERT(VARCHAR(12), MAX(printdate), 101) -- Sabir Khan 6381 08/20/2009 date format has been changed into standard format...
	           FROM   tblHTSNotes n WITH(NOLOCK)
	           WHERE  t.ticketid_pk = n.ticketid_fk
	                  AND n.letterid_fk = 6
	       ),
	       LORSortDate = (
	           SELECT CONVERT(VARCHAR(12), MAX(printdate), 101) -- Sabir Khan 6381 08/20/2009 date format has been changed into standard format...
	           FROM   tblHTSNotes n WITH(NOLOCK)
	           WHERE  t.ticketid_pk = n.ticketid_fk
	                  AND n.letterid_fk = 6
	       ),
	       ISNULL(tv.casenumassignedbycourt, '') AS casenumassignedbycourt,	--Saeed 8101 09/28/2010 casenumassignedbycourt, FollowUpDate,Courtid colums added. 
	       ISNULL(
	           CONVERT(VARCHAR(20), t2.HCJPAutoAlertFollowUpDate, 101),
	           ''
	       ) AS FollowUpDate,	       
	       c.Courtid, -- Saeed 8101 09/27/2010 court id added.
	       t2.HCJPAutoAlertFollowUpDate --Saeed 8101 08/30/2010 follow up date column added.
	FROM   dbo.tblTickets  AS t WITH(NOLOCK)
	       INNER JOIN dbo.tblTicketsViolations AS tv WITH(NOLOCK)
	            ON  t.TicketID_PK = tv.TicketID_PK
	       INNER JOIN dbo.tblCourts AS c WITH(NOLOCK)
	            ON  tv.CourtID = c.Courtid
	       INNER JOIN dbo.tblCourtViolationStatus AS cvs WITH(NOLOCK)
	            ON  tv.CourtViolationStatusID = cvs.CourtViolationStatusID
	       INNER JOIN dbo.tblCourtViolationStatus AS tcv2 WITH(NOLOCK)
	            ON  tv.CourtViolationStatusIDmain = tcv2.CourtViolationStatusID
	       LEFT OUTER JOIN tblticketsextensions t2 WITH(NOLOCK)	-- Saeed 8101 09/28/2010 apply join with tblticketsextensions to get follow up date
	            ON  t.TicketID_PK = t2.TicketID_PK
	       INNER  JOIN ReportSetting rs	WITH(NOLOCK)			-- Saeed 8101 09/28/2010 apply inner join with report setting to get only those record which are assigned using report setting control.
	            ON  c.Courtid = rs.CourtID
	       
	WHERE  --Ozair 7791 07/24/2010 Where clause optimized
	       rs.ReportID = @reportId
	       AND (
	       			-- Saeed 8101 09/28/2010 if @showAll=1 display all records without any filter else display only those records whose follow up date is null,past or today.
	               (@showAll = 1)
	               OR (
	                      @showAll = 0
	                      AND DATEDIFF(DAY,ISNULL(t2.HCJPAutoAlertFollowUpDate, '1/1/1900'),GETDATE()) >= 0
	                  )
	           )
	       AND --Ozair 4848 09/24/2008 Remove Bond Status In Verfied Statuses Check
	           (
	               -- Sabir Khan 6381 08/14/2009 Verified status is WAIT or Pretrial or Appearance, Jury Trial or Judge Trial.
	               (					
						(
							tv.CourtViolationStatusIDmain IN (104)
							OR 
							tcv2.CategoryID IN (2, 3, 4, 5)
						) 
						-- tahir 6168 07/16/2009 fixed the primary case status issue....
						--and (tv.CourtViolationStatusID in (146,37,135,80,161,186)
						AND 
						(
							cvs.categoryid IN (12, 50, 7, 52)
							AND 
							cvs.courtviolationstatusid != 236
						)
				)
				or
				(
						CHARINDEX('JCW',UPPER(isnull(tv.LastOpenWarrType,'')))>0
				)
	           )
	       AND t.activeflag = 1
	       
	ORDER BY t2.HCJPAutoAlertFollowUpDate	--Saeed 8101 08/30/2010 order by follow up date.
	   
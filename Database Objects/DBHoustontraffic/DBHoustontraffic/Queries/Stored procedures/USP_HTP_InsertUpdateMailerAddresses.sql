USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTP_InsertUpdateMailerAddresses]    Script Date: 10/02/2013 11:31:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rab Nawaz Khan
-- Create date: 09/30/2013
-- Description:	This Procedure is used to Insert/update the new addresses agains the non-clients.
-- =============================================
/*************************************Parameter List ********************************************* 
*	@RecordId 
*	@firstName 
*	@lastName 
*	@NewAddress1 
*	@city1 
*	@state1 
*	@zipcode1 
*	@DP2_1 
*	@DPC_1 
*	@NewAddress2 
*	@city2 
*	@state2 
*	@zipcode2 
*	@DP2_2 
*	@DPC_2 
*	@InsertedOrUpdatedBy 
*	@IsNewAddress2Checked
*  ******/
ALTER PROCEDURE [dbo].[USP_HTP_InsertUpdateMailerAddresses] 
	-- Add the parameters for the stored procedure here
	@RecordId INT,
	@firstName VARCHAR(100),
	@lastName VARCHAR (100),
	@NewAddress1 VARCHAR(500),
	@city1 VARCHAR (100),
	@state1 INT,
	@zipcode1 VARCHAR(50),
	@DP2_1 VARCHAR(10) = '',
	@DPC_1 VARCHAR(10) = '',
	@zp4AddressStatus1 VARCHAR(1),
	@NewAddress2 VARCHAR(500),
	@city2 VARCHAR (100),
	@state2 INT,
	@zipcode2 VARCHAR(50),
	@DP2_2 VARCHAR(10) = '',
	@DPC_2 VARCHAR(10) = '',
	@zp4AddressStatus2 VARCHAR(1),
	@InsertedOrUpdatedBy INT,
	@IsNewAddress2Checked BIT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @PreviousNewAddress1 VARCHAR(500)
	DECLARE @PreviousNewAddress2 VARCHAR(500)
	DECLARE @PreviousCity1 VARCHAR (100)
	DECLARE @PreviousCity2 VARCHAR (100)
	DECLARE @PreviousState1 INT
	DECLARE @PreviousState2 INT
	DECLARE @PreviousZipcode1 VARCHAR(50)
	DECLARE @PreviousZipcode2 VARCHAR(50)
	DECLARE @historyNote VARCHAR (1000)
	DECLARE @recCount INT
	DECLARE @stateName1 VARCHAR(50)
	DECLARE @stateName2 VARCHAR(50)
	
	IF (@DP2_1 = '')
	BEGIN
		SET @DP2_1 = NULL
	END
	IF (@DPC_1 = '')
	BEGIN
		SET @DPC_1 = NULL
	END
	IF (@DP2_2 = '')
	BEGIN
		SET @DP2_2 = NULL
	END
	IF (@DPC_2 = '')
	BEGIN
		SET @DPC_2 = NULL
	END
	IF (@zp4AddressStatus1 = '')
	BEGIN
		SET @zp4AddressStatus1 = NULL
	END
	IF (@zp4AddressStatus2 = '')
	BEGIN
		SET @zp4AddressStatus2 = NULL
	END
	-- Getting the Existing Record from new address update table to check weather record already exists. . . 
	SELECT @recCount = COUNT(*) FROM tblNewAddresses tna WHERE tna.RecordId = @RecordId
	
	SELECT @stateName1 = ts.[State] FROM tblState ts WHERE ts.StateID = @state1
	SELECT @stateName2 = ts.[State] FROM tblState ts WHERE ts.StateID = @state2
	
	IF (@recCount > 0)
	BEGIN
		SELECT TOP 1 @PreviousNewAddress1 = ISNULL(LTRIM(RTRIM(tna.Address1)), ''), @PreviousNewAddress2 = ISNULL(LTRIM(RTRIM(tna.Address2)), ''), 
		@PreviousCity1 = ISNULL(LTRIM(RTRIM(tna.City1)), ''), @PreviousCity2 = ISNULL(LTRIM(RTRIM(tna.City2)), ''),	@PreviousState1 = ISNULL(tna.State1, 0), 
		@PreviousState2 = ISNULL(tna.State2, 0), @PreviousZipcode1 = ISNULL(LTRIM(RTRIM(tna.ZipCode1)), ''), @PreviousZipcode2 = ISNULL(LTRIM(RTRIM(tna.ZipCode2)), '') 
		FROM tblNewAddresses tna
		WHERE tna.RecordId = @RecordId
	END
	
	
	--If New Address2 checkbox is selected as checked then only insert the the new Address 1 values. . . 
	IF(@IsNewAddress2Checked = 0)
	BEGIN
		-- if previously no new address exists, then insert the new address
		IF (@recCount = 0)
			BEGIN
    			INSERT INTO [TrafficTickets].[dbo].[tblNewAddresses]
				   ([RecordId] ,[FirstName] ,[LastName] ,[Address1] ,[City1] ,[State1] ,[ZipCode1], [InsertedBy1] ,[InsertDate1], [DP2_1], [DPC_1], AddressStatus1)
			 VALUES (@RecordId, @firstName, @lastName, @NewAddress1, @city1, @state1, @zipcode1, @InsertedOrUpdatedBy, GETDATE(), @DP2_1, @DPC_1, @zp4AddressStatus1)
			 
			 SET @historyNote = 'New Address 1: Added [' + @NewAddress1 + ' ' + @city1 + ' ' + @stateName1 + ' ' + @zipcode1 + ']'
			 INSERT INTO [TrafficTickets].[dbo].[tblNewAddressesHisotry] (Activity, RecordId, Insertdate, UserId)
			 VALUES (@historyNote, @RecordId, GETDATE(), @InsertedOrUpdatedBy)
			END
		ELSE
			BEGIN
				UPDATE [TrafficTickets].[dbo].[tblNewAddresses]
				   SET Address1 = @NewAddress1 ,[City1] = @city1, [State1] = @state1 ,[ZipCode1] = @zipcode1 ,[UpdatedBy] = @InsertedOrUpdatedBy
					  ,[UpdateDate] = GETDATE(), [DP2_1] = @DP2_1, [DPC_1] = @DPC_1, AddressStatus1 = @zp4AddressStatus1,
					  IsNoNewAddressMarked = 0
				 WHERE [RecordId] = @RecordId
				 
				 IF ((LTRIM(RTRIM(@NewAddress1)) NOT LIKE LTRIM(RTRIM(@PreviousNewAddress1))) OR (LTRIM(RTRIM(@city1)) NOT LIKE LTRIM(RTRIM(@PreviousCity1))) 
				 OR (LTRIM(RTRIM(@zipcode1)) NOT LIKE LTRIM(RTRIM(@PreviousZipcode1))) OR (@state1 != @PreviousState1) )
				 BEGIN
				 	SET @historyNote = 'New Address 1: Updated [' + @NewAddress1 + ' ' + @city1 + ' ' + @stateName1 + ' ' + @zipcode1 + ']'
					INSERT INTO [TrafficTickets].[dbo].[tblNewAddressesHisotry] (Activity, RecordId, Insertdate, UserId)
					VALUES (@historyNote, @RecordId, GETDATE(), @InsertedOrUpdatedBy)
				 END
			END
	END
	ELSE
	BEGIN
		-- If Inserting both New address 1 and New address 2 and both are not exists already, then insert the both new address. . .
		IF (@recCount = 0)
			BEGIN
				INSERT INTO [TrafficTickets].[dbo].[tblNewAddresses]
			   ([RecordId] ,[FirstName] ,[LastName] ,[Address1] ,[City1] ,[State1] ,[ZipCode1], [DP2_1], [DPC_1], [AddressStatus1], [Address2] ,[City2],[State2],[ZipCode2],
			   [DP2_2], [DPC_2], AddressStatus2, [InsertedBy1],[InsertDate1], [InsertedBy2],[InsertDate2])
				VALUES (@RecordId,@firstName,@lastName,@NewAddress1,@city1,@state1,@zipcode1, @DP2_1, @DPC_1, @zp4AddressStatus1, @NewAddress2,@city2,@state2,@zipcode2,
				@DP2_2, @DPC_2, @zp4AddressStatus2, @InsertedOrUpdatedBy, GETDATE(), @InsertedOrUpdatedBy, GETDATE())
				
				SET @historyNote = 'New Address 1: Added [' + @NewAddress1 + ' ' + @city1 + ' ' + @stateName1 + ' ' + @zipcode1 + ']'
				INSERT INTO [TrafficTickets].[dbo].[tblNewAddressesHisotry] (Activity, RecordId, Insertdate, UserId)
				VALUES (@historyNote, @RecordId, GETDATE(), @InsertedOrUpdatedBy)

				SET @historyNote = 'New Address 2: Added [' + @NewAddress2 + ' ' + @city2 + ' ' + @stateName2 + ' ' + @zipcode2 + ']'
				INSERT INTO [TrafficTickets].[dbo].[tblNewAddressesHisotry] (Activity, RecordId, Insertdate, UserId)
				VALUES (@historyNote, @RecordId, GETDATE(), @InsertedOrUpdatedBy)
				
			END
			-- If Record Already Exits then update the addresses. . . 
		ELSE
		BEGIN
			IF EXISTS (SELECT tna.RowId FROM tblNewAddresses tna WHERE tna.RecordId = @RecordId AND tna.InsertDate2 IS NULL)
			BEGIN
				UPDATE [TrafficTickets].[dbo].[tblNewAddresses]
				SET	[InsertedBy2] = @InsertedOrUpdatedBy, [InsertDate2] = GETDATE()	
				
				SET @historyNote = 'New Address 2: Added [' + @NewAddress2 + ' ' + @city2 + ' ' + @stateName2 + ' ' + @zipcode2 + ']'
				INSERT INTO [TrafficTickets].[dbo].[tblNewAddressesHisotry] (Activity, RecordId, Insertdate, UserId)
				VALUES (@historyNote, @RecordId, GETDATE(), @InsertedOrUpdatedBy)
			END 
			
			UPDATE [TrafficTickets].[dbo].[tblNewAddresses]
			SET Address1 = @NewAddress1 ,[City1] = @city1, [State1] = @state1 ,[ZipCode1] = @zipcode1 ,[UpdatedBy] = @InsertedOrUpdatedBy
			,[UpdateDate] = GETDATE(), [DP2_1] = @DP2_1, [DPC_1] = @DPC_1, AddressStatus1 = @zp4AddressStatus1,
			Address2 = @NewAddress2, City2 = @city2, State2 = @state2, ZipCode2 = @zipcode2, DP2_2 = @DP2_2, DPC_2 = @DPC_2, AddressStatus2 = @zp4AddressStatus2,
			IsNoNewAddressMarked = 0
			WHERE [RecordId] = @RecordId
			
			IF ((LTRIM(RTRIM(@NewAddress1)) NOT LIKE LTRIM(RTRIM(@PreviousNewAddress1))) OR (LTRIM(RTRIM(@city1)) NOT LIKE LTRIM(RTRIM(@PreviousCity1))) 
				 OR (LTRIM(RTRIM(@zipcode1)) NOT LIKE LTRIM(RTRIM(@PreviousZipcode1))) OR (@state1 != @PreviousState1) )
				 BEGIN
				 	SET @historyNote = 'New Address 1: Updated [' + @NewAddress1 + ' ' + @city1 + ' ' + @stateName1 + ' ' + @zipcode1 + ']'
					INSERT INTO [TrafficTickets].[dbo].[tblNewAddressesHisotry] (Activity, RecordId, Insertdate, UserId)
					VALUES (@historyNote, @RecordId, GETDATE(), @InsertedOrUpdatedBy)
				 END
			
			IF (((LTRIM(RTRIM(@NewAddress2)) NOT LIKE LTRIM(RTRIM(@PreviousNewAddress2))) OR (LTRIM(RTRIM(@city2)) NOT LIKE LTRIM(RTRIM(@PreviousCity2))) 
				 OR (LTRIM(RTRIM(@zipcode2)) NOT LIKE LTRIM(RTRIM(@PreviousZipcode2))) OR (@state2 != @PreviousState2)) AND LEN(ISNULL(@PreviousNewAddress2, '')) > 0)
				  
				 BEGIN
				 	SET @historyNote = 'New Address 2: Updated [' + @NewAddress2 + ' ' + @city2 + ' ' + @stateName2 + ' ' + @zipcode2 + ']'
					INSERT INTO [TrafficTickets].[dbo].[tblNewAddressesHisotry] (Activity, RecordId, Insertdate, UserId)
					VALUES (@historyNote, @RecordId, GETDATE(), @InsertedOrUpdatedBy)
				 END
		END	
	END
END

GO
GRANT EXECUTE ON [dbo].[USP_HTP_InsertUpdateMailerAddresses] TO dbr_webuser
GO 

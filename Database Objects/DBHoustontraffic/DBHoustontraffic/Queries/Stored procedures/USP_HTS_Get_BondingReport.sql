SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Get_BondingReport]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Get_BondingReport]
GO


-- USP_HTS_Get_BondingReport
CREATE procedure dbo.USP_HTS_Get_BondingReport

as


select	distinct
		t.ticketid_pk,
		t.lastname + ', '+ t.firstname as clientname, 
		refcasenumber,
		casenumassignedbycourt, 
		d1.description as autostatus, 
		d2.description as verifiedstatus	,
		a.barcardnumber, 
		a.bondingnumber	
FROM         
	dbo.tblTickets AS t INNER JOIN
	dbo.tblTicketsViolations AS v ON t.TicketID_PK = v.TicketID_PK INNER JOIN
	dbo.tblTicketsViolationsArchive AS a ON v.RecordID = a.RecordID LEFT OUTER JOIN
	dbo.tblDateType AS d2 INNER JOIN
	dbo.tblCourtViolationStatus AS s2 ON d2.TypeID = s2.CategoryID ON v.CourtViolationStatusIDmain = s2.CourtViolationStatusID LEFT OUTER JOIN
	dbo.tblDateType AS d1 INNER JOIN
	dbo.tblCourtViolationStatus AS s1 ON d1.TypeID = s1.CategoryID ON v.CourtViolationStatusID = s1.CourtViolationStatusID
WHERE  (LEN(ISNULL(a.BondingNumber, '')) > 0) OR (LEN(ISNULL(a.BarCardNumber, '')) > 0)
and	t.activeflag =1
order by [autostatus], [verifiedstatus], [clientname]
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


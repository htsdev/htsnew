SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_WebScan_Login]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_WebScan_Login]
GO


CREATE procedure [dbo].[usp_WebScan_Login]      
      
@UserName as varchar(50),      
@Password as varchar(50)      
      
as      
      
Select employeeid,accesstype, Upper(lastname+', '+firstname) as UserName
from tblusers where      
 username=@UserName and       
 password=@Password and   
 status=1      


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


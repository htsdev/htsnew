CREATE PROCEDURE [dbo].[USP_HTP_Update_PastCourtDateNONHMCFollowUpdate]
@ticketid INT,
@PastCourtDateNonHMCFollowUpDate DATETIME
AS

UPDATE tblTickets
SET	
	PastCourtDateNonHMCFollowUpDate = @PastCourtDateNonHMCFollowUpDate
WHERE TicketID_PK=@ticketid

GO
GRANT EXECUTE ON [dbo].[USP_HTP_Update_PastCourtDateNONHMCFollowUpdate] TO dbr_webuser
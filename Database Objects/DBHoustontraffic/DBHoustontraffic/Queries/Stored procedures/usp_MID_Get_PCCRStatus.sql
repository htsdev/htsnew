SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_MID_Get_PCCRStatus]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_MID_Get_PCCRStatus]
GO

-- PROCEDURE USED TO POPULATE QUOTE STATUSES IN DROP DOWN    
-- WHICH WILL BE USED AS SELECTION CRITERIA.....    
CREATE    PROCEDURE [dbo].[usp_MID_Get_PCCRStatus] AS    
SELECT convert(varchar(10),StatusID) as StatusID,     
 StatusDescription    
FROM    dbo.tblPCCRStatus    
where statusdescription not like '%--------%'    
order by    
 orderfield    
    
    


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


  
  
/****** Object:  StoredProcedure [dbo].[USP_HTP_Get_PromotionalMessage]    Script Date: 01/24/2009 04:30:54 ******/  
/*    
Created By : Sabir Khan    
Business Logic : This Procedure return the promotional message.  
Parametr:  
   @NoteID : Note ID  
   @ZipCode : ZipCode    
  
column : Messageformetterpage.  
*/  
--USP_HTP_Get_PromotionalMessage 23903 
CREATE procedure [dbo].[USP_HTP_Get_PromotionalMessage]     
  
 --@ZipCode VARCHAR(10),  
 --@NoteID INT   
 @TicketId int  
   
AS    
  
DECLARE @zipcode VARCHAR(5), @LID INT  
  
SELECT @zipcode = LEFT(ISNULL(zip,''),5) , @LID = ISNULL(mailerid,0) FROM tbltickets WHERE TicketID_PK = @TicketId  
  
  
SELECT TOP 1  ISNULL(s.MessageForMatterPage,'') AS PromotionalMessage  
FROM   dbo.PromotionalSchemes AS s INNER JOIN  
dbo.PromotionalSchemes_MailerType AS m ON s.SchemeId = m.SchemeId INNER JOIN  
dbo.PromotionalSchemes_ZipCodes AS z ON m.SchemeMailerId = z.SchemeMailerId INNER JOIN  
dbo.tblLetter ON m.MailerType = dbo.tblLetter.LetterID_PK INNER JOIN  
dbo.tblLetterNotes AS n ON dbo.tblLetter.LetterID_PK = n.LetterType  
WHERE     (s.IsActiveScheme = 1)   
AND (m.IsActive = 1)   
AND (z.IsActiveZip = 1)   
AND (DATEDIFF(DAY, s.EffectiveFromDate, GETDATE()) >= 0)   
AND (DATEDIFF(DAY, s.EffecttiveToDate, GETDATE()) <= 0)  
AND LEFT (z.ZipCode,5) = @ZipCode  
AND n.noteid = @LID   
ORDER BY s.SchemeId DESC  
  
GO

grant exec on dbo.USP_HTP_Get_PromotionalMessage	 to dbr_webuser

GO
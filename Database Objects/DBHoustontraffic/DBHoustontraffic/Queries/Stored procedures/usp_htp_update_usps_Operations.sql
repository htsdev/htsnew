﻿
/***
* Waqas 5932 06/10/2009 
* Business Logic : This procedure is used to update returned/delivered faclilities and the date time. 
***/

CREATE PROCEDURE dbo.usp_htp_update_usps_Operations
	@LetterID INT,
	@ReturnedDate DATETIME = NULL,
	@ReturnedFacilityID VARCHAR(20) = NULL,
	@StopTheClockDate DATETIME = NULL,
	@StopTheClockFacilityID VARCHAR(20) = NULL
AS
BEGIN
	
	UPDATE tblLetterNotes
	SET
		USPSStopTheClockDate = @StopTheClockDate,
		USPSStopTheClockFacilityID = @StopTheClockFacilityID,
		USPSReturnedDate = @ReturnedDate,
		USPSReturnedFacilityID = @ReturnedFacilityID 
	WHERE 
	NoteId = @LetterID
END
GO

GRANT EXECUTE ON dbo.usp_htp_update_usps_Operations TO dbr_webuser


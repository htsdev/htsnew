﻿/******  
Created by:		    Fahad Muhammad Qureshi
Business Logic :    This procedure is used to get Data for Thank You Email Service for Dallas and Houston.
TaskId:				6429     
List of Parameters: N/A  
    
******/

ALTER PROCEDURE dbo.USP_HTP_GET_TodaysHireClient
AS
	SELECT tt.TicketID_PK,
	       LOWER(tt.Email) AS Email,
	       tt.Firstname,
	       tt.Lastname,
	       'Houston' AS Area
	FROM   Traffictickets.dbo.tblTickets tt
	WHERE  tt.TicketID_PK IN (SELECT ttp.TicketID
	                          FROM   Traffictickets.dbo.tblTicketsPayment ttp
	                          WHERE  ttp.PaymentVoid = 0
	                          GROUP BY
	                                 ttp.PaymentVoid,
	                                 ttp.TicketID
	                          HAVING DATEDIFF(DAY, MIN(ttp.RecDate), GETDATE()) 
	                                 = 0)
	       AND tt.TicketID_PK NOT IN (SELECT ttf.TicketID_PK
	                                  FROM   Traffictickets.dbo.tblTicketsFlag 
	                                         ttf
	                                  WHERE  ttf.FlagID = 34)
	       AND ISNULL(tt.Email, '') <> ''
	       AND tt.ThankYouEmailSentDate IS NULL
	UNION
	SELECT tt.TicketID_PK,
	       LOWER(tt.Email) AS Email,
	       tt.Firstname,
	       tt.Lastname,
	       'Dallas' AS Area
	FROM   Dallastraffictickets.dbo.tblTickets tt
	WHERE  tt.TicketID_PK IN (SELECT ttp.TicketID
	                          FROM   Dallastraffictickets.dbo.tblTicketsPayment 
	                                 ttp
	                          WHERE  ttp.PaymentVoid = 0
	                          GROUP BY
	                                 ttp.PaymentVoid,
	                                 ttp.TicketID
	                          HAVING DATEDIFF(DAY, MIN(ttp.RecDate), GETDATE()) 
	                                 = 0)
	       AND tt.TicketID_PK NOT IN (SELECT ttf.TicketID_PK
	                                  FROM   Dallastraffictickets.dbo.tblTicketsFlag 
	                                         ttf
	                                  WHERE  ttf.FlagID = 38)
	       AND ISNULL(tt.Email, '') <> ''
	       AND tt.ThankYouEmailSentDate IS NULL
GO
GRANT EXECUTE ON dbo.USP_HTP_GET_TodaysHireClient TO dbr_webuser
GO
  
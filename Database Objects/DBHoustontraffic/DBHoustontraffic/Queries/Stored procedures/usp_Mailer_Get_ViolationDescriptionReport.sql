

-- exec usp_Mailer_Get_ViolationDescriptionReport '03/06/2008','03/06/2008','RUNNING A RED LIGHT (INTERSECTION)|FAILURE TO DISPLAY A VALID TEXAS DRIVERS LICENSE|FAILURE TO ESTABLISH FINANCIAL RESPONSIBILITY|',1  
    
CREATE PROCEDURE [dbo].[usp_Mailer_Get_ViolationDescriptionReport]            
                  
 @startDate  datetime,                  
 @endDate  datetime,              
 @violations varchar(max) ,             
 @violcount tinyint         
                  
AS                  
        
declare @tblviolations  table(id int IDENTITY PRIMARY KEY,ViolationDescription varchar(800))                        
insert into @tblviolations select * from dbo.Sap_Dates_Pipe(@violations)                   
                
-- FIRST GET THE CASES FROM NON-CLIENTS DATABASE FOR SELECTED VIOLATIONS.....          
-- FOR SINGLE/DOUBLE VIOLATIONS COUNT......          
select distinct recordid     
into #temp    
from tblticketsviolationsarchive  
where courtlocation in (3001,3002,3003)         
group by recordid           
having count(violationdescription) = @violcount    
         
       
create table #temp2 (ViolationDescription varchar(800), recordid int, clientflag int, noteid int, mailerdate datetime)          
          
insert into #temp2 (ViolationDescription, recordid, clientflag, noteid, mailerdate)          
select a.ViolationDescription, isnull(v.recordid,0) as recordid, t.clientflag, isnull(n.noteid,0), n.recordloaddate          
from tblticketsviolationsarchive v inner join tblticketsarchive t          
 on t.recordid = v.recordid RIGHT OUTER JOIN  @tblviolations a          
 on a.ViolationDescription = v.violationdescription      
 left outer join tblletternotes n    
 on n.recordid = v.recordid    
where       
v.recordid in (select * from #temp)        
and datediff(day, n.recordloaddate, @startdate)<=0          
and datediff(day, n.recordloaddate, @enddate  )>=0          
and n.courtid in (3001,3002,3003)    
          
union          
select a.ViolationDescription, isnull(v.recordid,0) as recordid, t.clientflag, isnull(n.noteid,0), n.recordloaddate          
FROM dbo.tblLetterNotes AS n           
INNER JOIN          
        dbo.tblletternotes_detail AS d           
ON  n.NoteId = d.noteid           
RIGHT OUTER JOIN          
        tblticketsviolationsarchive v ON  d.recordid = v.RecordID     
 inner join tblticketsarchive t          
 on t.recordid = v.recordid RIGHT OUTER JOIN  @tblviolations a          
 on a.ViolationDescription = v.violationdescription               
where       
v.recordid in (select * from #temp)        
and  datediff(day, n.recordloaddate, @startdate)<=0          
and  datediff(day, n.recordloaddate, @enddate  )>=0          
and  n.courtid in (3001,3002,3003)          
    
--select * from #temp2    
    
-- NOW, GET THE CLIENT/QUOTE INFORMATION.........          
create table #temp3 (ViolationDescription varchar(800), recordid int, clientflag int, noteid int, mailerdate datetime, activeflag int, ticketid_pk int)          
          
insert into #temp3 (ViolationDescription, recordid, clientflag, noteid, mailerdate, activeflag, ticketid_pk)          
select distinct a.ViolationDescription, a.recordid, a.clientflag, a.noteid, a.mailerdate, t.activeflag, t.ticketid_pk          
from #temp2 a          
left outer join           
 tbltickets t          
on t.mailerid = a.noteid          
--inner join           
-- tblviolations v          
--on v.Description = a.ViolationDescription      
    
--select * from #temp3        
          
-- NOW SUMMARIZE THE INFORMATION.....          
SELECT a.ViolationDescription,            
  year(a.mailerdate) as [year],          
  month(a.mailerdate) as [month],          
  count(distinct a.ticketid_pk) as calls,          
  count(distinct a.noteid) as lettercount,          
  count(distinct case when a.activeflag=0 then  a.ticketid_pk end) as quotecount,          
  count(distinct case when a.activeflag=1 then  a.ticketid_pk end) as clientcount          
from #temp3 a         
group by           
  a.ViolationDescription, year(a.mailerdate), month(mailerdate)          
ORDER BY           
  a.ViolationDescription, [year], [month]          
                    
          
drop table #temp          
drop table #temp2          
drop table #temp3   
GO

grant exec on [dbo].[usp_Mailer_Get_ViolationDescriptionReport] to dbr_webuser  
go
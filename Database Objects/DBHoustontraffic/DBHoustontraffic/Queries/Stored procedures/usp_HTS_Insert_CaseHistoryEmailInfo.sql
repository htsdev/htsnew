/*
* Created By: Sekhani
Business Logic  :   This Procedure is used to insert note on history page for email Recipt and Trail letter.
Parameter:
-----------
       
@TicketID: This parameter contains the ticket id
@Subject : This parameter contains the subject for note in history page
@EmpID   :This parameter contains the employ id 
@lettertype : This parameter contains the letter type (2 for trail letter and 3 for Receipt)
@emailid : This parameter contains the email id.
   
*/
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

--if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_HTS_Insert_CaseHistoryEmailInfo]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
--drop procedure [dbo].[usp_HTS_Insert_CaseHistoryEmailInfo]
--GO

ALTER procedure usp_HTS_Insert_CaseHistoryEmailInfo --12457,'ze@zee.com',3992,3,1  
  
@TicketID       int,  
@Subject varchar(500),  
@EmpID    int,
@lettertype int,
@emailid varchar(50)
  
as  

if(@lettertype=3)  --Receipt
begin  
 insert into tblticketsnotes(TicketID,Subject,EmployeeID) 
 values (@TicketID,'Receipt Emailed To: '+'<a href="javascript:window.open(''../reports/Previewemail.aspx?EmailID='+convert(varchar,@emailid)+''');void('''');"''>'+@subject + '</a>',@EmpID)  
					  
end  
else if (@lettertype=2)
BEGIN
--Muhammad Ali 8370 10/11/2010 -- Add Link to TRail letter..
DECLARE @recordid INT
SELECT @recordid = recordid FROM tblHTSNotes 
WHERE DocPath IN (SELECT REPLACE(SUBSTRING([filename],CHARINDEX('Reports\\',[filename]),LEN([filename])),'Reports\\','') FROM tblEmailNotes WHERE Emailid = @emailid AND LetterID_FK = @lettertype AND TicketID_FK = @TicketID)
AND TicketID_FK = @TicketID AND LetterID_FK = @lettertype 

-- Rab Nawaz Khan 8370 05/12/2011 E-mails Address Hyperlink issue in History page resolved
insert into tblticketsnotes(TicketID,Subject,EmployeeID) 
VALUES( @TicketID,
 '<a href="javascript:window.open(''../clientinfo/frmPreview.aspx?RecordID='+CONVERT(VARCHAR,@recordid)+''');void('''');"''>'+'Trial Letter' + '</a>'
 + ' sent via email to '+ @subject ,@EmpID)	
  --END 8370

end
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


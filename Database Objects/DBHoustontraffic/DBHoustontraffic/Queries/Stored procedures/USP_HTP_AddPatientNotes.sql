﻿ /******  
* Created By :	  Saeed Ahmed.
* Create Date :   11/08/2010 
* Task ID :		  8501
* Business Logic :  This procedure is used to add patient notes.
* List of Parameter : @PatientID 
*					: @Notes 
*					: @InsertedBy 
* Column Return :     
******/
 alter PROC dbo.USP_HTP_AddPatientNotes
 @PatientId INT,
 @Notes VARCHAR(MAX),
 @InsertedBy INT
 AS
 
 INSERT INTO PatientNotes(PatientId,Notes,InsertedBy) VALUES(@PatientId,@Notes,@InsertedBy)
 
 GO
 GRANT EXECUTE ON dbo.USP_HTP_AddPatientNotes TO dbr_webuser
 GO
 
 
-- exec dbo.USP_HTS_MISC_AttorneyJuryTrialTracker '4/1/08', '4/30/08', 'andrew sullo'
ALTER procedure [dbo].[USP_HTS_MISC_AttorneyJuryTrialTracker]
	(
	@sdate datetime,
	@edate datetime,
	@Attorney varchar(200) 
	)

as

--select convert(datetime, convert(varchar(10),v.initialeventdate_attorney,101)) [Date],
select convert(datetime, convert(varchar(10),v.courtdate,101)) [Date],
	count(distinct t.midnumber) as [Client Count]
into #temp
from tblticketsviolationsarchive v 
inner join 
	tblticketsarchive t
on t.recordid = v.recordid
inner join 
	tblcourtviolationstatus s
on s.courtviolationstatusid = v.violationstatusid
and v.courtlocation in (3001,3002,3003)
and s.categoryid = 4
--and datediff(day, initialeventdate_attorney, @sdate) <= 0
--and datediff(day, initialeventdate_attorney, @edate) >= 0
and datediff(day, v.courtdate, @sdate) <= 0
and datediff(day, v.courtdate, @edate) >= 0
and attorneyname like @attorney
--group by convert(datetime, convert(varchar(10),v.initialeventdate_attorney,101))
group by convert(datetime, convert(varchar(10),v.courtdate,101))
order by [date]

select convert(varchar(10), date,101) [Date] , [client count] from #temp


drop table #temp
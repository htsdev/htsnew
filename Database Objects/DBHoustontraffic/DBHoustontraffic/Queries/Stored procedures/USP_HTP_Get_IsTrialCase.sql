/****** 
Create by:  Tahir Ahmed
Dated:		07/15/2008

Business Logic : The stored procedure is to get case status information

List of Parameters:	
	@TicketId : Identification of case to fetch general comments

List of Columns: 	
	Column0: Returns true if it contains JUD, JUR, or PRE trial cases else false.

******/

create procedure dbo.USP_HTP_Get_IsTrialCase
	(
	@TicketID int
	)


as

declare @IsTrialCase bit

if exists (
		select ticketsviolationid 
		from tblticketsviolations v
		inner join tblcourtviolationstatus cvs 
		on cvs.courtviolationstatusid  = v.courtviolationstatusidmain
		where ticketid_pk = @TicketID
		and cvs.categoryid in (3,4,5)
		)
	set @IsTrialCase = 1
else
	set @IsTrialCase = 0

select @IsTrialCase


go

grant execute on dbo.USP_HTP_Get_IsTrialCase to dbr_webuser
go
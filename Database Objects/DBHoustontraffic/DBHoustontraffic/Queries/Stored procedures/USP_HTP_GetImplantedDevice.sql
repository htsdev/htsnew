﻿  /******  
* Created By :	  Saeed Ahmed
* Create Date :   12/03/2010 
* Task ID :		  8585
* Business Logic : This procedure returns all available other implanted device in our system.
* List of Parameter :
* Column Return :     
******/
CREATE PROCEDURE [dbo].[USP_HTP_GetImplantedDevice]
AS
SELECT id,description FROM OtherImplantedDevice 


GO
GRANT EXECUTE ON [dbo].[USP_HTP_GetImplantedDevice] TO dbr_webuser
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_UPDATE_GENERAL_INFO_FLAG]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_UPDATE_GENERAL_INFO_FLAG]
GO

  
    
CREATE PROCEDURE [dbo].[USP_UPDATE_GENERAL_INFO_FLAG]    
@fid int,@priority int, @ServiceTicketCategory int   
AS    
    
BEGIN    
update tblticketsflag 
set 
	priority=@priority,
	ServiceTicketCategory = @ServiceTicketCategory
where 
	fid=@fid    
END    

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


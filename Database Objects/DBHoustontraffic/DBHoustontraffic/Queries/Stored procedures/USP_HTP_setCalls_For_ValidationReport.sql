﻿

/*  
Created By : Noufil Khan  
Business Logic : Display all cases who are in JURY, JUDGE or PRETRIAL bond flag is true and set call status is pending  

List of Parameters:    No Parameters

List of Columns : 
	ticketId : Display Ticket Id 
	Name : Display the name of Client
	Language : Display Language of Client
	Bond : Display is client status bond (Y,N)
	Comments : Display Comment on the case
	CRT : Display Court Date
	Insurance : Display Insurance type 
	TrialDesc : Display Trial Description 
*/  
  
  

ALTER PROCEDURE [dbo].[USP_HTP_setCalls_For_ValidationReport]
        
AS        
-- Adil 08/05/2008 4272
Declare @SetCallDate datetime

--Yasir Kamal 5555 03/11/2009 ShowSetting Control added.
Set @SetCallDate = Getdate() - abs((Select [dbo].[fn_GetReportValue] ('days',105))) -- 105 is report_id of set call validation report.
--5555 end
Set @SetCallDate = (case when datepart(dw,@SetCallDate) = 1 then @SetCallDate - 2 when datepart(dw,@SetCallDate) = 7 then @SetCallDate - 1 else @SetCallDate end)
Set @SetCallDate = CONVERT(VARCHAR(20), @SetCallDate, 110) + ' 00:00:00'

--Farrukh 9332 08/27/2011 SetCallStatus added in the report.
select ticketid, Name, Language, Bond, contact1, contact2, contact3, contact, Comments, CRT, ResetDate, Insurance, TrialDesc, 
'<a href="http://ln.legalhouston.com/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber='+convert(varchar(30), TicketID)+'" >' + convert(varchar(30), max(rownumber))+'</a>' as link, followupdate, Recdate,SetCallStatus
 from (
SELECT DISTINCT
		 t.TicketID_PK as ticketid,
		 lastname +', '+firstname as [Name],
		 t.LanguageSpeak as [Language],
		 case bondflag when 1 then 'Y' else 'N' end as Bond, 
		 case when isnull(T.Contact1,'') <> '' then convert(varchar(20),ISNULL(dbo.formatphonenumbers(T.Contact1),''))+' '+'('+ISNULL(LEFT(Ct1.Description, 1),'')+')' else '' end as contact1,  
		 case when isnull(T.Contact2,'') <> '' then convert(varchar(20),ISNULL(dbo.formatphonenumbers(T.Contact2),''))+' '+'('+ISNULL(LEFT(Ct2.Description, 1),'')+')' else '' end as contact2,   
		 case when isnull(T.Contact3,'') <> ''then convert(varchar(20),ISNULL(dbo.formatphonenumbers(T.Contact3),''))+' '+'('+ISNULL(LEFT(Ct3.Description, 1),'')+')' else '' end as contact3,
		(contact1 + ' <br/> ' + contact2 + ' <br/> '+ contact3) as contact,
		 tv.setCallComments as Comments,
		 c.ShortName as CRT, convert(varchar(20),tvl.Recdate, 110) as ResetDate,     
		Insurance=(  select top 1 vv.shortdesc+isnull('('+convert(varchar,tvv.ticketviolationdate,101)+')','')             
	
				from tblviolations vv inner join tblticketsviolations tvv            
			  on vv.violationnumber_pk=tvv.violationnumber_pk             
				where tvv.ticketid_pk=t.ticketid_pk            
			  and vv.shortdesc='ins'), 		
		TrialDesc = dbo.fn_DateFormat(tv.courtdatemain,30,'/',':',1)+' ' + isnull(cvs.shortdescription,''),
		ROW_NUMBER() OVER (order by (dbo.fn_DateFormat(tv.courtdatemain,30,'/',':',1)+' ' + isnull(cvs.shortdescription,'')) ) as RowNumber,
		t.Criminalfollowupdate AS followupdate, convert(varchar(20),tvl.Recdate, 110) as Recdate,
		--Farrukh 9332 08/27/2011 SetCallStatus added in the report.
		tr.[Description] AS SetCallStatus
                                              
FROM  tblTicketsViolations TV                                 
		INNER JOIN dbo.tblTickets t ON  TV.TicketID_PK = t.TicketID_PK     
		inner join  dbo.tblCourtViolationStatus cvs	ON  TV.CourtViolationStatusIDMain = cvs.CourtViolationStatusID
		INNER JOIN  dbo.tblCourts c	ON  TV.CourtID = c.Courtid
		LEFT OUTER JOIN  dbo.tblContactstype ct2 ON t.ContactType2 = ct2.ContactType_PK
		LEFT OUTER JOIN  dbo.tblContactstype ct1 ON t.ContactType1 = ct1.ContactType_PK
		LEFT OUTER JOIN  dbo.tblContactstype ct3 ON t.ContactType3 = ct3.ContactType_PK                                           
        inner join tblticketsviolationlog tvl on tv.TicketsViolationID = tvl.TicketviolationID
		-- TAHIR 5507 02/05/2009 fixed the join clause...
		left OUTER JOIN dbo.tblTicketsFlag TF ON TV.TicketID_PK = TF.TicketID_PK AND ISNULL(TF.FlagID,0) != 5
		--left OUTER JOIN dbo.tblTicketsFlag TF ON TV.TicketID_PK = TF.TicketID_PK 
		--Farrukh 9332 08/27/2011 SetCallStatus added in the report.
		LEFT OUTER JOIN dbo.tblReminderstatus tr ON TV.setcallstatus = tr.Reminderid_PK
WHERE 
		t.Activeflag = 1 
		--Sabir Khan 4876  09/29/2008
		and t.CaseTypeId=1
-- Adil 08/05/2008 4272
		And isnull(t.firmid,3000) = 3000 
		--and TF.FlagID not in (5)
-- Tahir 4755 09/05/2008 only pending set calls....
		--AND isnull(tv.setcallstatus,0)<>1
		AND isnull(tv.setcallstatus,0) = 0
-- Adil 08/05/2008 4272
		AND isnull(TV.ReminderCallStatus,0) <> 1
		AND  cvs.categoryid IN (3,4,5)   
-- Adil 08/05/2008 4272
-- Sabir Khan 5365 12/30/2008 Set call display proper records...
and datediff(day,tvl.recdate,'07/31/2008')<=0 and datediff(day,tvl.recdate,@SetCallDate)>=0
--and tvl.Recdate >= '7/31/2008' and tvl.Recdate <= @SetCallDate
--and datediff(day, '07/31/2008', tvl.Recdate) >= 0
---------------------------------------------------------
--Sabir Khan 5467 01/29/2009 Only check bond flag...
			--and tvl.oldverifiedstatus = 202   
----------------------------------------------------------------
and tvl.newverifiedstatus = tv.courtviolationstatusidmain  
and tv.TicketsViolationID = tvl.TicketViolationID 

 -- Waqas 5467 01/28/2009
	and isnull(t.BondFlag,0) = 1 ) a
--Farrukh 9332 08/27/2011 SetCallStatus added in the report.
group by ticketid, Name, Language, Bond, contact1, contact2, contact3, contact, Comments, CRT, ResetDate, Insurance, TrialDesc, followupdate, Recdate,SetCallStatus
ORDER BY a.[Name] -- tahir 5507 02/06/2009 set the default sort order








SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Bug_UpdateAttachment]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Bug_UpdateAttachment]
GO



--usp_bug_InsertNewAttachment  'dmb.doc','test description',3028,'10/20/2006','msword',3991,1  
CREATE procedure [dbo].[usp_Bug_UpdateAttachment]    
@FileName as varchar(50),    
@Description as varchar(100),    
@Size as int,    
@UploadDate as datetime,    
@ContentType as varchar(15),    
@EmployeeID as int,    
@BugID as int    
    
as    
    
update tbl_bug_attachment set   
 
 AttachFileName=@FileName,    
 description=@Description,    
 file_size=@Size,    
 uploadeddate=@UploadDate,    
 contenttype=@ContentType,    
 EmployeeID=@EmployeeID    

 where 
	bug_id=@BugID








GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


﻿/****** Object:  StoredProcedure [dbo].[USP_HTP_Update_NonHMCFollowUpdate]  ******/     
/*        
Created By     : Fahad Muhammad Qureshi.
Created Date   : 04/10/2009  
TasK   : 5753        
Business Logic  : This procedure Updates Split Cases Follow Up Date.      
           
Parameter:       
   @TicketID     : Updating criteria with respect to TicketId      
   @FollowUpDate : Date of follow Up which has to be set     
     
      
*/      


CREATE PROCEDURE USP_HTP_Update_SplitCaseFollowUpdate
(
	@TicketID INT, 
	@FollowUpDate DATETIME
)
AS
BEGIN
	

UPDATE tblTickets
SET
	
	SplitCaseFollowupDate = @FollowUpDate
	WHERE TicketID_PK=@TicketID
	
END
GO
grant exec on [dbo].[USP_HTP_Update_SplitCaseFollowUpdate] to dbr_webuser

GO 
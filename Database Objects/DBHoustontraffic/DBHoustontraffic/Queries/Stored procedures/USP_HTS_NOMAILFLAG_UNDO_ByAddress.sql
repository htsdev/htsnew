/*****************************  
* Author : Abbas Qamar
* Task Id: 9726 
* Creation Date : 11/03/2011
* 
* Business Logic : This Stored procedure is used by HTP/Activites/DoNotMailUpdate/NoMailFlagRecords. to remove the recrods from the 
* on the base of Flag.
* 	
* Input Parameters:
* @Address = address of a person.
* @Zip = zip code of city.
* @StateID = id of state.
* @City = city of a peroson.
* 
* Output Columns:
* ADDRESS
* ZIP
* City
* State
*************************/  
  
            
              
CREATE PROCEDURE [dbo].[USP_HTS_NOMAILFLAG_UNDO_ByAddress]                    
@Address varchar(50),
@City VARCHAR(50),                      
@Zip varchar(12)                        
as                    
-- For Traffic clients                
update tblticketsarchive                         
 set DoNotMailFlag =0, DoNotMailUpdateDate = NULL                    
where                    
 LTRIM(RTRIM(address1+ ' ' + ISNULL(address2,''))) = LTRIM(RTRIM(@Address))                    
 AND LEFT(LTRIM(RTRIM(ZipCode)),5) = LEFT(LTRIM(RTRIM(@Zip)),5)
 AND CASE WHEN LEN(LTRIM(RTRIM(@City))) > 0 THEN LTRIM(RTRIM(City)) ELSE '' END  =  LTRIM(RTRIM(@City))                
             
                  
                
-- For criminal clients                
update jims.dbo.criminalcases                         
 set DoNotMailFlag =0
where                    
 LTRIM(RTRIM(ADDRESS)) = LTRIM(RTRIM(@Address))
 AND LEFT(LTRIM(RTRIM(Zip)),5) = LEFT(LTRIM(RTRIM(@Zip)),5)
 AND CASE WHEN LEN(LTRIM(RTRIM(@City))) > 0 THEN LTRIM(RTRIM(City)) ELSE '' END  =  LTRIM(RTRIM(@City))             
              
--for Dallas              
update dallastraffictickets.dbo.tblticketsarchive                         
 set DoNotMailFlag =0,DoNotMailUpdateDate = NULL                    
where                    
LTRIM(RTRIM(address1 + ' ' + ISNULL(address2,''))) = LTRIM(RTRIM(@Address))                    
AND LEFT(LTRIM(RTRIM(ZipCode)),5) = LEFT(LTRIM(RTRIM(@Zip)),5)
AND CASE WHEN LEN(LTRIM(RTRIM(@City))) > 0 THEN LTRIM(RTRIM(City)) ELSE '' END  =  LTRIM(RTRIM(@City))            
              
                
delete from tbl_hts_donotmail_records                    
where                    
 LTRIM(RTRIM(isnull(address,''))) = LTRIM(RTRIM(@Address))                                      
 AND LEFT(LTRIM(RTRIM(Zip)),5) = LEFT(LTRIM(RTRIM(@Zip)),5)
 AND CASE WHEN LEN(LTRIM(RTRIM(@City))) > 0 THEN LTRIM(RTRIM(City)) ELSE '' END  =  LTRIM(RTRIM(@City))            


GO

GRANT EXECUTE ON [dbo].[USP_HTS_NOMAILFLAG_UNDO_ByAddress] TO dbr_webuser

GO




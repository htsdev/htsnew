
/****** 
Created by:		Rab Nawaz Khan
Task ID   :		10442
Date	  :		09/17/2012

Business Logic:	The procedure is used by LMS application to get the number of printable letter
				count for the Sugar Land Warrant letter for the selected date range.
				
List of Parameters:
	@catnum:		Category number of the selected letter type, if printing for 
					all courts of the selected court category
	@LetterType:	Selected letter type in LMS
	@CrtId:			Court id of the selected court category if printing for individual court hosue.
	@startListdate:	Starting list date for Arraignment letter.
	@endListDAte:	Ending list date for Arraignment letter.
	@SearchType:	Flag that identifies if searching by court date or by list date. 0= list date, 1 = court date

List of Columns:	
	Total_Count:	Total number of letters exists for the selected date range.
	Good_Count:		Total number of letters that have valid addresses
	Bad_Count:		Total Number of letter that have undeliverable addresses
	DonotMail_Count:Total number of letter for addresses that had been marked to stop sending letters.
	

******/

--/  usp_mailer_Get_Sugarland_Warrant_Count  9, 134, 9, '09/24/2012', '09/24/2012', 0
ALTER PROCEDURE [dbo].[usp_mailer_Get_Sugarland_Warrant_Count]
	(
	@catnum		int,
	@LetterType 	int,
	@crtid 		int   ,                                              
	@startListdate 	Datetime,
	@endListdate 	DateTime,                                                      
	@SearchType     int
	)

as                                                              
          

                                                  
declare	@officernum varchar(50), 
	@officeropr varchar(50), 
	@tikcetnumberopr varchar(50), 
	@ticket_no varchar(50), 
	@zipcode varchar(50), 
	@zipcodeopr varchar(50), 
	@fineamount money,
	@fineamountOpr varchar(50) ,
	@fineamountRelop varchar(50),
	@singleviolation money, 
	@singlevoilationOpr varchar(50) , 
	@singleviolationrelop varchar(50), 
	@doubleviolation money,
	@doublevoilationOpr varchar(50) , 
	@doubleviolationrelop varchar(50), 
	@count_Letters int,
	@violadesc varchar(500),
	@violaop varchar(50),
	@sqlquery nvarchar(max)  ,
	@sqlquery2 nvarchar(max)  ,
	@sqlParam nvarchar(1000)

-- SETTING PARAMETER LIST AND ORDER FOR DYNAMIC SQL.....
select @sqlparam = '@catnum int, @LetterType int, @crtid int, @startListdate Datetime, 	
		    @endListdate DateTime, @SearchType int'

select @sqlquery ='', @sqlquery2 = ''
                                                              
-- GETTING DIFFERENT FILTER TYPES AND THIER VALUES IN THE LOCAL VARIABLES
-- IF ASSOCIACTED WITH THE LETTER.......                                 
Select	@officernum=officernum,
	@officeropr=oficernumOpr,
	@tikcetnumberopr=tikcetnumberopr,
	@ticket_no=ticket_no,
	@zipcode=zipcode,
	@zipcodeopr=zipcodeLikeopr,
	@singleviolation =singleviolation,
	@singlevoilationOpr =singlevoilationOpr, 
	@singleviolationrelop =singleviolationrelop, 
	@doubleviolation = doubleviolation,
	@doublevoilationOpr=doubleviolationOpr,
	@doubleviolationrelop=doubleviolationrelop,
	@violadesc=violationdescription,
	@violaop=violationdescriptionOpr , 
	@fineamount=fineamount ,
	@fineamountOpr=fineamountOpr ,
	@fineamountRelop=fineamountRelop  
from 	tblmailer_letters_to_sendfilters  
where 	courtcategorynum=@catnum 
and 	Lettertype=@LetterType 
                        


-- MAIN QUERY....
-- FTA ISSUE DATE WISE NUMBER OF DISTINCT NON-CLIENTS
-- FOR THE SELECTED DATE RANGE ONLY FTA CASES
-- INSERTING VALUES IN A TEMP TABLE....                              
set 	@sqlquery=@sqlquery+'                                          
 Select distinct 
	convert(datetime , convert(varchar(10),ta.ListDate,101)) as mdate,   
	flag1 = isnull(ta.Flag1,''N'') , 
	ta.donotmailflag, 
	TA.recordid AS RECORDID,
	left(TA.zipcode,5) + ta.midnumber as zipmid, 
	isnull(tva.fineamount,0) as fineamount,  
	tva.violationnumber_pk, 
	tva.violationdescription,
	tva.TicketNumber_PK,
	ta.FirstName,
	ta.LastName,
	upper(ta.address1) + '''' + isnull(ta.address2,'''') as address1
		
into #temp                                                            
FROM tblTicketsViolationsArchive tva, tblTicketsArchive ta,  dbo.tblcourts, tblstate s


where tva.RecordID = ta.RecordID 
and dbo.tblcourts.courtid = tva.courtlocation   
and s.stateid =  ta.stateid_Fk

-- PERSON NOT MOVED IN LAST 48 MONTHS (ACCUZIP)
and isnull(ta.ncoa48flag,0) = 0

-- ONLY FTA VIOLATIONS...
and TVA.violationstatusid = 186

-- First name and last name should not be null. . . 
and LEN(ISNULL(RTRIM(LTRIM(ta.lastname)),''''))<>0
and LEN(ISNULL(RTRIM(LTRIM(ta.firstname)),''''))<>0

-- Update DATE LIES BETWEEN SELECTED DATE RANGE...
and datediff(Day, ta.ListDate, @startlistdate)<= 0 
and datediff(day, ta.ListDate, @endlistdate)>=0

-- Only Non-Clients
AND ISNULL(ta.Clientflag, 0) = 0
 
 '
  
-- IF PRINTING FOR ALL COURTS OF THE SELECTED CATEGORY...
if(@crtid = @catnum  )                                    
	set @sqlquery=@sqlquery+' and tva.courtlocation In (Select courtid from tblcourts where courtcategorynum = @crtid )'                                        

-- ELSE PRINTING FOR THE INDIVIDUAL SELECTED COURT OF THE CATEGORY....
ELSE if( @crtid <>  @catnum )                                    
	set @sqlquery =@sqlquery +' and tva.courtlocation = @crtid' 
                                                        
-- LMS FILTERS SECTION:
-----------------------------------------------------------------------------------------------------------------------------------------------------
-- IF THERE IS ANY FILTER SPECIFIED TO INCLUDE/EXCLUDE ONLY PARTICULAR OFFICER NUMBERS....                                                     
if(@officernum<>'')                                                        
	set @sqlquery =@sqlquery + '  and ta.officerNumber_Fk '  + @officeropr  +'('+ @officernum+')'                                                         
                                                       
-- IF THERE IS ANY FILTER DEFINED FOR INCLUDE/EXCLUDE VIOLATION DESCRIPTIONS...
if(@ticket_no<>'')                        
	set @sqlquery=@sqlquery + ' and ('+  dbo.Get_String_Concat_With_op(''+@ticket_no+'' , 'tva.TicketNumber_PK' ,+ ''+ @tikcetnumberopr) +')'                                                              
                                    
-- ZIP CODE FILTER......
-- INCLUDE/EXCLUDE THE SPECIFIED ZIP CODES.....
if(@zipcode<>'')                                                                              
	set @sqlquery=@sqlquery+ ' and ('+ dbo.Get_String_Concat_With_op(''+@zipcode+'' , 'left(ta.zipcode,3)' ,+ ''+ @zipcodeopr) +')'                                   

-- FINE AMOUNT FILTER.....
-- INCLUDE ONLY THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT....
if(@fineamount<>0 and @fineamountOpr<>'not'  )                                  
	set @sqlquery =@sqlquery+ '
        and tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                                       

-- EXCLUDE THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT.....
if(@fineamount<>0 and @fineamountOpr = 'not'  )                                  
	set @sqlquery =@sqlquery+ '
        and not  tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                                       

-----------------------------------------------------------------------------------------------------------------------------------------------------

-- GETTING VIOL COUNT & TOTAL FINE AMOUNT FOR EACH LETTER...
set @sqlquery =@sqlquery+ '

select distinct *,  ROW_NUMBER() OVER (ORDER BY (SELECT 1)) AS RowNum into #tempsugar from #temp where 1 = 1
'

-- SINGLE VIOLATION FINE AMOUNT FILTER....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE SINGLE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT                                      
if(@singleviolation<>0 and @doubleviolation=0)                                
	set @sqlquery =@sqlquery+ '
	and (  '+ @singlevoilationOpr+  '  (total_fineamount '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and violcount=1))
      '                                

-- DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...
if(@doubleviolation<>0 and @singleviolation=0 )                                
	set @sqlquery =@sqlquery + '
	and ('+ @doublevoilationOpr+  '   (total_fineamount '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ')and violcount=2) )
      '                                                    

-- BOTH SINGLE & DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE AND SINGLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...                                      
if(@doubleviolation<>0 and @singleviolation<>0)                                
	set @sqlquery =@sqlquery+ '
	and ('+ @singlevoilationOpr+  '  (total_fineamount '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and violcount=1))                                
    and ('+ @doublevoilationOpr+  '  (total_fineamount '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ') and violcount=2))
        '                                  
                              
-- VIOLATION DESCRIPTION FILTER.....
-- EXCLUDE THE LETTER IF SPECIFIED VIOLATION IS THE ONLY VIOLATION IN THE LETTER....
if(@violadesc <> '')
	BEGIN
		-- NOT LIKE FILTER.......
		if(charindex('not',@violaop)<> 0 )              
			BEGIN
				set @sqlquery=@sqlquery+' 
				and	 not (  ( '+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  violationdescription  ' ,+ ''+ 'like')+'))'   	
			END
			-- INCLUDE ONLY THOSE LETTER THAT SINGLE VIOLATION AND MATCHES THE SPECIFIED VIOLATION DESCRIPTION...
			-- LIKE FILTER.......
			--if(charindex('not',@violaop) =  0 )              			
		ELSE
			BEGIN
				set @sqlquery=@sqlquery+' 
				and	 (  ( '+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  violationdescription  ' ,+ ''+ 'like')+'))'   	
			END
	END

-- ECLUDE THE LETTERS THAT HAVE BEEN ALREADY PRINTED......
-- OR DLQ LETTER IS PRINTED IN PAST 2 DAYS FOR THE SAME RECORD...
set @sqlquery =@sqlquery+ ' 

delete from #tempsugar 
from #tempsugar a inner join tblletternotes_detail d on d.recordid = a.recordid inner join 
tblletternotes n on n.noteid=  d.noteid
where (n.lettertype = @lettertype) 

-- Rab Nawaz Khan 10442 12/05/2012 Removing Duplicate Profiles. . . 
SELECT DISTINCT t1.FIRSTName, t1.lastName, t1.ADDRESS1, MIN(t1.Recordid) AS Recordid
INTO #duplicationProfiles
FROM #tempsugar t1
GROUP BY t1.FIRSTName, t1.lastName, t1.ADDRESS1
HAVING COUNT(DISTINCT t1.Recordid) > 1
ORDER BY t1.FIRSTName, t1.lastName, t1.ADDRESS1

UPDATE t
SET t.Recordid = d.Recordid
FROM #tempsugar t INNER JOIN  #duplicationProfiles d ON t.Recordid <> d.recordid
WHERE  t.FIRSTName = d.FIRSTName 
AND t.lastName = d.lastName
AND t.ADDRESS1 = d.ADDRESS1


SELECT DISTINCT Ticketnumber_pk, MAX(rownum) AS RowNum 
INTO #deleteDuplicate
FROM #tempsugar
WHERE Ticketnumber_pk IN (
SELECT DISTINCT Ticketnumber_pk
FROM #tempsugar t1
GROUP BY t1.FIRSTName, t1.lastName, t1.ADDRESS1, Ticketnumber_pk, t1.recordid
HAVING COUNT(t1.Ticketnumber_pk) > 1)
GROUP BY  Ticketnumber_pk

DELETE FROM #tempsugar WHERE RowNum IN (SELECT d.RowNum FROM #deleteDuplicate d)

select * into #temp1 from #tempsugar

-- END 10442

Select count(distinct RECORDID) as Total_Count, mdate 
into #temp2 
from #temp1 
group by mdate 
  
-- GETTTING LIST DATE WISE TOTAL NUMBER OF PRINTABLE LETTERS...
Select count(distinct RECORDID) as Good_Count, mdate 
into #temp3
from #temp1 
where Flag1 in (''Y'',''D'',''S'')   
and donotmailflag = 0    
group by mdate   
                                                      
-- GETTING LIST DATE WISE TOTAL NUMBER OF NON-PRINTABLE LETTERS                                                      
Select count(distinct RECORDID) as Bad_Count, mdate  
into #temp4  
from #temp1 
where Flag1  not in (''Y'',''D'',''S'') 
and donotmailflag = 0 
group by mdate 
                   
-- GETTING LIST DATE WISE TOTAL NUMBER OF LETTERS THAT MARKED 
-- AS BLOCKED ADDRESSES.....
Select count(distinct RECORDID) as DonotMail_Count, mdate  
into #temp5   
from #temp1 
where donotmailflag=1 
group by mdate

-- GETTING ALL THE DATES IN TEMP TABLE THAT LIES BETWEEN THE 
-- SELECTED DATE RANGE.......
create table #letterdates (mdate datetime)
while datediff(day, @startListdate, @endlistdate)>=0
begin
	insert into #letterdates (mdate) select convert(varchar(10),@startlistdate,101)
	set @startlistdate = dateadd(day, 1, @startlistdate)
end
'
	
-- OUTPUTTING THE REQURIED INFORMATION........         
set @sqlquery2 = @sqlquery2 +'                                                        
Select 	l.mdate as listdate,
        isnull(Total_Count,0) as Total_Count,
        isnull(Good_Count,0) Good_Count,  
        isnull(Bad_Count,0)Bad_Count,  
        isnull(DonotMail_Count,0)DonotMail_Count  
from #letterdates l left outer join #Temp2 on #temp2.mdate = l.mdate left outer join #temp3 
on #temp2.mdate=#temp3.mdate  
left outer join #Temp4  
on #temp2.mdate=#Temp4.mdate    
left outer join #Temp5  
on #temp2.mdate=#Temp5.mdate 
WHERE isnull(Good_Count,0) > 0
order by l.mdate


-- GETTING TOTAL NUMBER OF PRINTIABLE LETTERS FOR ALL THE SELECTED
-- DATE RANGES. TO DETERMINE THE EXISTANCE OF SAME CLIENT IN MULTIPLE LETTERS...
Select count(distinct zipmid) from #temp1 
where Flag1 in (''Y'',''D'',''S'') and donotmailflag = 0  

-- DROPPING THE TEMPORARY TABLES USED ABOVE.....
drop table #temp drop table #temp1 drop table #temp2  drop table #temp3  drop table #temp4  drop table #temp5  drop table #letterdates
drop table #tempsugar Drop Table #deleteDuplicate Drop Table #duplicationProfiles
'                                                      

SET @sqlquery  = @sqlquery  + @sqlquery2                                 
--PRINT @sqlquery  

-- CONCATENATING THE QUERY ....

-- EXECUTING THE DYNAMIC SQL QUERY...
exec sp_executesql @sqlquery , @sqlparam, @catnum, @LetterType, @crtid, @startListdate, @endListdate, @SearchType


GO
GRANT EXECUTE ON [dbo].[usp_mailer_Get_Sugarland_Warrant_Count] TO dbr_webuser
GO
﻿ set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

/******   
Created by:   Sarim Ghani   
Business Logic: The procedure is used to send Trial Notification Letters to clients.   
    HTP /Activities/Batch Print application uses this stored procedure to   
    generate emails and send it to the selected clients on the batch print  
    application.  
    It do not return any data but for each selected client, generates an   
    email in HTML format and send it to the client's email address by   
    using Database Mail.  
  
List of Parameters:   
 @Ticketid_pk : It expects a list of comma separated ticket IDs   
     selected to send emails on batch print application  
  
List of Columns: 
			    recipients
				subject                                                                                                                                                                                                                                                          
				body
				bodyformat  
   
  
  
******/  
  
 --[USP_HTS_Send_EmailTrialLetter_BatchReport] '12421,125054,6593,'
 --[USP_HTS_Send_EmailTrialLetter_BatchReport] '249215,'  
ALTER PROCEDURE [dbo].[USP_HTS_Send_EmailTrialLetter_BatchReport]    
@Ticketid_pk varchar(MAX)    -- Haris Ahmed 10337 07/04/2012 Fix bug for Email not going to Clients
as  
  
-- CREATING TEMP TABLE TO STORE CLIENT'S PERSONAL AND CASE INFORMATION.....  
create table #TempTable      
(      
 ID int identity, TicketID int,ClientName varchar(250),CustomerName varchar(250), EmployeeID int, EMailAddress varchar(250), CourtDateMain datetime      
)      
     

create table #MailTable      
(      
 [recipients] varchar(1000), subject varchar(500),body text,bodyformat varchar(4)
)      

insert into #TempTable      
--- Geting those records which has future date and status in JURRY,ARR and courts are out side courts      
SELECT    distinct     
 t.TicketID_PK As TicketID,      
 isnull(t.LastName,'') + ', ' + isnull(t.FirstName,'') As ClientName,      
 isnull(t.LastName,'') + ' ' + isnull(t.FirstName,'') As CustomerName,      
 t.EmployeeID,      
 t.Email AS EMailAddress,  
tv.CourtDateMain        
FROM    tblTickets t         
INNER JOIN        
 tblTicketsViolations tv         
ON  t.TicketID_PK = tv.TicketID_PK         
LEFT OUTER JOIN        
 tblCourtViolationStatus cvs         
ON  tv.CourtViolationStatusIDmain = cvs.CourtViolationStatusID      
where (charindex('@',isnull(t.Email,''),1)>0)       
      
and t.ticketid_pk in (Select MainCat from dbo.Sap_String_Param(@Ticketid_pk))     
      
and tv.courtviolationstatusidmain not in(50,80)-- Case should not be disposed      
and tv.CourtDateMain = (Select min(CourtDateMain) from tblticketsviolations where ticketid_pk = t.ticketid_pk)
--Yasir Kamal 7417 02/16/2010 do not send trial letter if Bad Email flag is added.     
and t.ticketid_pk not in (select ticketid_pk from tblticketsflag where ticketid_pk =t.ticketid_pk and flagid IN (7,34))--Checking No Letters and Bad email      
-- Status of all violations shuld be same    
and dbo.fn_get_split_records_count(t.ticketid_pk)=0     
--and tv.ticketid_pk=125616    
      
create table #TempTable2      
(      
 ID int identity, TicketID int,ClientName varchar(250),CustomerName varchar(250), EmployeeID int, EMailAddress varchar(250), CourtDateMain datetime  
)      
-- Inserting both query records to single temp table for geting distinct records      
insert into #TempTable2 select distinct TicketID, ClientName,CustomerName, EmployeeID, EMailAddress, CourtDateMain from #TempTable order by TicketID      
   
        
------- Sending Email To Clients -------------      
      
-- Local Variables      
Declare @Body varchar(max)      
Declare @email varchar(250)      
Declare @TicketID varchar(100)      
Declare @Subject varchar(250)      
Declare @count int      
Declare @no int      
Declare @EmpID int      
Declare @ClientName varchar(250)      
      
--- Subject Of Email
--select @Subject ='Sullo & Sullo Attorneys – Trial Notification Email'
--Ozair 7821 07/29/2010 Subject changed for Bad Email Detection
select @Subject ='Trial Notification' 

      
-- Set count with total numbers of records      
select @count =count(ID) from #TempTable2      
-- no at the starting of while loop      
set @no =1      
  
--print(@count)  
while @no <= @count      
begin      
      
-- Geting Current Email,TicketID,ClientName and EmpID from temp table      
select @email =EMailAddress from #TempTable2 where ID =@no      
select @TicketID =TicketID from #TempTable2 where ID =@no      
select @ClientName =CustomerName from #TempTable2 where ID =@no      
select @EmpID =3992-- System ID      
      
       
-- Genrate EmailID by removing spactioal chracters from datetime      
Declare @EmailID varchar(250)      
Declare @CurrentDateTime varchar(100)      
set @CurrentDateTime =convert(varchar(30),getdate()) + convert(varchar(2),DatePart(s, getdate()))      
set @CurrentDateTime = replace(@CurrentDateTime,'-','')      
set @CurrentDateTime = replace(@CurrentDateTime,' ','')      
set @CurrentDateTime = replace(@CurrentDateTime,'.','')      
set @CurrentDateTime = replace(@CurrentDateTime,':','')      
set @EmailID =@TicketID + @CurrentDateTime -- Combining TicketID and CurrentDateTime      
--------------------      
      
-- GENERATING HTML CODE AND CONTENTS FOR THE EMAIL........  
set @Body ='<style type="text/css">    
.Label{ FONT-WEIGHT: normal;  COLOR: #000000; FONT-STYLE: normal; FONT-FAMILY: Verdana; FONT-SIZE: 8.5pt; border-style:none; border-width:0px; }    
.SmallLabel {FONT-WEIGHT: normal;  COLOR:Gray; FONT-STYLE: normal; FONT-FAMILY: Verdana; FONT-SIZE: 7pt; border-style:none; border-width:0px;}</style>    
<table border="0" bgcolor="#D9D9D9" cellpadding="0" cellspacing="0" align="center" height="100%" width="100%">    
<tr><td class="SmallLabel" align="center"><br /> If you are having problems viewing this email, you can view it online at<br />    
<a href="http://www.sullolaw.com/Reports/TroubleViewingLink.aspx?casenumber='+dbo.Encrypt(@TicketID)+'&ClientName='+dbo.Encrypt(@ClientName)+'&EmailID='+dbo.Encrypt(@EmailID)+'">http://www.sullolaw.com/EmailLink/TroubleViewingLink?RefrenceCode  
='+dbo.Encrypt(@TicketID)+' </a></td></tr>    
<tr> <td width="100%" style="height: 100%"> <br>    
<table border="1" bgcolor="white" bordercolor="Silver" cellpadding="8" cellspacing="0" align="center" height="100%" width="500">    
<tr> <td align="center" style="height: 5px; border:1;border-color:Silver;"><img src="http://www.sullolaw.com/images/SulloLawlogo.jpg"/> </td></tr>    
<tr> <td valign="top" width="100%" style="height: 400px"> <table border="0" align="center" cellpadding="0" cellspacing="0" width="100%"><tr>    
<td class="Label" > </td><td class="Label"> <br /><br /> DEAR : '+upper(@ClientName)+',<br /><br />    
Thank you for hiring Sullo & Sullo, LLP. Please click on the link below to review important information regarding your case(s)    
including court date(s), time(s), location(s), and items you must bring to court with you.    
</td></tr><tr> <td align="center" style="width: 13px"> </td><td align="center" class="Label">    
<br /><br /><a href="http://www.sullolaw.com/Reports/frmTrialNotification.aspx?casenumber='+dbo.Encrypt(@TicketID)+'&EmailID='+dbo.Encrypt(@EmailID)+'">    
<span style=color:#4F81BD>Please click here to view your Trial Notification Letter</span> </a>    
</td></tr><tr> </tr><tr> <td class="Label" style="width: 13px"></td><td class="Label"><br /><br />Thank you,<br /><br /><br /></td></tr> <tr>    
<td style="width: 13px"></td><td> <font size=5 face=Calibri><span style=font-size:16.0pt;font-family:Calibri;> Sullo <o:p></o:p></span></font>    
<font size=4 face="Lucida Fax"><span style="font-size:14.0pt;font-family: Lucida Fax";>&</span></font>    
<font size=5 face=Calibri><span style=font-size:16.0pt;font-family:Calibri;> Sullo,</span></font>    
<font size=2 face=Calibri><span style=font-size:9.0pt;font-family:Calibri;><b>LLP</b></span></font>    
</td></tr> <tr><td style="width: 13px; height: 19px"></td> <td class="Label"> <b>Tel #:</b> 713.839.9026</td></tr><tr>    
<td style="width: 13px"></td><td class="Label"> <b>Fax #:</b> 713.523.6634</td></tr>    
<tr> <td style="width: 13px"> </td> <td class="Label">http://www.sullolaw.com<br><br></td></tr>    
</table></td></tr></table></td></tr><tr> <td><table border="0" cellpadding="0" cellspacing="0" align="center" style="width: 452px; height:1px;">    
<tr><td valign="top" align="center"></td></tr></table>' 
-- Rab Nawaz 10914 07/30/2013 Removed Disclimer. . . 
--<table style="width: 500px"><tr><td><hr /> </td></tr></table></td></tr> 
--<tr><td align="center" width="100%" class="SmallLabel">    
--This e-mail and the files transmitted with it are the property of Sullo & Sullo <span style=font-size:6pt;>LLP</span> and/or its affiliates, are confidential, and are intended solely for use of the individual or entity to whom this e-mail is addressed. If
  
  
 --you are not one of the named recipients or otherwise have reason to believe that you have received this message in error, please notify the sender at 713-839-9026 and delete this message immediately from your computer.&nbsp; Any other use, retention, dis
  
    
--semination, forwarding, printing, or copying of this e-mail is strictly prohibited.    
--<br /><br /></td></tr>
--</table> </td></tr><tr><td align="center" class="SmallLabel">Copyright© 2007, Sullo & Sullo, <span style=font-size:6pt;>LLP</span> All Rights Reserved.<br /><br /><br/>

     
print ('Before Mail Block')   
  
-- IF THE CURRENT CLIENT'S EMAIL ADDRESS IS NOT MARKED AS "NO EMAIL"   
-- THEN EMAIL THE TRIAL LETTER LINK TO THE CLIENT....  
if((select count(email) from tblnoemailflag where email = @email) = 0) --The clients email which are exist in tblnoemailflag wont send email      
 BEGIN      
  
 -- BY USING DATABASE MAIL AND 'TRIAL NOTIFICATIONS PROFILE'   
 -- MAILS WILL BE DELIVERED FROM 'TrialNotifications@SulloLaw.com' ADDRESS  


--   EXEC msdb.dbo.sp_send_dbmail         
-- CHANGED BY TAHIR AHMED DT:4/29/08 TASK 3887     
--------------------------------------------------       
--   @profile_name = 'Traffic System',          
--     @profile_name = 'Trial Notifications',          
--------------------------------------------------       
	insert into #MailTable ([recipients],subject,body,bodyformat) values (@email,@Subject,@Body,'HTML')
--     @recipients = @email,         
--     @subject = @Subject,         
--     @body = @Body ,        
--     @body_format = 'HTML' ;
	
 
        
--Muhammad Ali 8370 10/19/2010 --> Stop History note from this sp and this work is done in "[USP_HTS_UPDATE_LetterBatch]"
-- Sending Note to Case history        
--  insert into tblticketsnotes (ticketid,subject,employeeid) values(@TicketID,'Trial Letter sent to ('+@email+')' ,@EmpID)        
          
  -- Add entry in tblemailtrialnotification with EmailID        
  Insert into tblemailtrialnotification(TicketID_pk,EmailDate,EmailID,EmailAddress)        
  values(@TicketID,getdate(),@EmailID,@email)          
      
  -- update trial letter date and isemailedtriallter field even email is not send        
  update tbltickets set IsEmailedTrialLetter =1,TrialLetterEmailedDate =getdate() where ticketid_pk = @TicketID        

  
           
  ---- It will make a gap between bulk sending emails         
  --waitfor delay '0:0:20'        
 END        
        
set @no = @no +1        
        
end        
  
select * from #MailTable
  
drop table #TempTable        
drop table #TempTable2        
drop table #MailTable
             
     
  
  
  
  
 
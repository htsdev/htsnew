/****** Object:  UserDefinedFunction [dbo].[Fticketscount]    Script Date: 01/25/2008 20:29:21 ******/
DROP FUNCTION [dbo].[Fticketscount]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--select dbo.Fticketscount(59454)

CREATE    Function [dbo].[Fticketscount](@ticketid int)
Returns int
AS
BEGIN
	declare @ticketcount int
	select @ticketcount = count(refcasenumber) 
	from tblticketsviolations V, tblticketsarchive T
	where V.refcasenumber = T.ticketnumber
	and refcasenumber like 'F%'
	and datediff(month,violationdate,'09/01/2004') <=0 
	and V.ticketid_pk = @ticketid

	return isnull(@ticketcount,0)
END
GO

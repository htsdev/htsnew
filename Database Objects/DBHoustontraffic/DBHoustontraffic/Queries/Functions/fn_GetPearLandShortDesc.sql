USE [TrafficTickets]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetPearLandShortDesc]    Script Date: 08/26/2013 09:08:42 
* CREATED BY :	RAB NAWAZ KHAN
* TASK		: 11108
* DATE		: 08/24/2013
* 
* ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER FUNCTION [dbo].[fn_GetPearLandShortDesc] (@TicketNumber as VARCHAR(200),@ViolationDescription VARCHAR(200) )
Returns VARCHAR(50)
As
BEGIN

DECLARE @ChargeAmount INT
DECLARE @MPH INT
DECLARE @ViolationDesc VARCHAR(200)
DECLARE @shortDesc VARCHAR(20)
SELECT top 1 @ChargeAmount = CourtCharges 
FROM LoaderFilesArchive.dbo.tblPearland_ArrignmentOne_DataArchive 
WHERE TicketNumber = @TicketNumber AND ViolationDESCRIPTION LIKE '%SPEEDING%'
ORDER BY InsertDate DESC
SET @MPH = 9+ (@ChargeAmount - 75)/5

IF(@ViolationDescription LIKE '%SPEEDING%SCHOOL%')
SET  @shortDesc = 'sp-sz +' + Convert(varchar,@MPH)
ELSE IF(@ViolationDescription LIKE '%SPEEDING%TC 545%')
SET  @shortDesc =  'sp +'  + Convert(varchar,@MPH) 

RETURN @shortDesc
END 


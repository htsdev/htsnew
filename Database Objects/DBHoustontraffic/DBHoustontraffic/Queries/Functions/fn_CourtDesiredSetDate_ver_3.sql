/****** Object:  UserDefinedFunction [dbo].[fn_CourtDesiredSetDate_ver_3]    Script Date: 01/25/2008 20:28:44 ******/
DROP FUNCTION [dbo].[fn_CourtDesiredSetDate_ver_3]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
--select * from tblticketsviolations
create      function [dbo].[fn_CourtDesiredSetDate_ver_3] (@ticketsviolationid int,@officerday varchar(12),@courtdate datetime)
RETURNS Datetime
AS
BEGIN
declare @arrdate datetime
declare @officerid int
declare @actdate datetime
declare @count int

IF ISDATE(@courtdate)= 1  
	BEGIN
		SET @actdate= dateadd(day,21,@courtdate)
	    	
		IF NOT (ISNULL(@officerday,'NODAY') = 'NODAY')
		BEGIN
            		WHILE (datename(weekday,@actdate) <> UPPER(@officerday))
				BEGIN
					SET @actdate = dateadd(day,1,@actdate)
					CONTINUE
				END
			END
		ELSE
			BEGIN
				RETURN NULL
				
			END
		
                    
			
			SELECT @count = ISNULL(PeopleAssigned830,0)  from dbo.tblcourtsettings where datediff(dd,CourtDate_PK  ,@actdate) = 0 
				
			WHILE (@Count >= 150 )              
				
				BEGIN
				SET @actdate = dateadd(day,7,@actdate)
				SELECT @count = ISNULL(PeopleAssigned830,0)  from dbo.tblcourtsettings where datediff(dd,CourtDate_PK  ,@actdate) = 0 
				END
		         
				SELECT  @actdate  =      @actdate
		
	END
	
ELSE
	BEGIN
		-- if no courtdate is available , then set it to 21 days out from today
		Select @actdate = Date from dbo.currentdate
	        SET @actdate = dateadd(day,21, @actdate)		
	END
	
	RETURN   @actdate
	
END
GO

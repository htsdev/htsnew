/****** Object:  UserDefinedFunction [dbo].[fn_getticketNumberbyTicketId]    Script Date: 01/17/2008 06:04:18 ******/
DROP FUNCTION [dbo].[fn_getticketNumberbyTicketId]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_getticketNumberbyTicketId]
(
	@TicketId_Pk int
)
RETURNS varchar(max)
AS
BEGIN
	
	DECLARE @ticketnumbers varchar(max)
	set @ticketnumbers = ''
	
	SELECT @ticketnumbers = @ticketnumbers + ' <a href=''/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber='+ CONVERT(varchar(50),tb.TicketId_Pk) +  ''' target=''_blank''>'+CONVERT(varchar(50),tbv.refcasenumber) + '</a>' + ',' from tbl_JPBatchPrint tbp inner join tbltickets  tb on tbp.Ticket_ID = tb.TicketId_Pk   inner join tblticketsviolations tbv on tb.TicketId_pk = tbv.TicketId_pk  where tbp.Ticket_Id = @TicketId_Pk
	
	set @ticketnumbers = substring(@ticketnumbers,0,len(@ticketnumbers)-1)	

	RETURN @ticketnumbers
END
GO

﻿/************************************************************
 * Code formatted by SoftTree SQL Assistant © v4.0.34
 * Time: 5/18/2009 12:49:42 PM
 ************************************************************/

/*        
 Created By     : Fahad Muhammad Qureshi.
 Created Date   : 05/18/2009  
 TasK		    : 5807        
 Business Logic : This function check the FTA violation in Non-Client DataBase and return true and false according to result. 
				
Parameter:       
  @ticketid_pk	: Ticket id      
  
*/

CREATE FUNCTION [dbo].[fn_CheckFTAViolationForNonClient]
(
	@RecordID   INT,
	@Midnumber  VARCHAR(20)
)
RETURNS INT
AS


BEGIN
    DECLARE @Res INT	
    IF (
           SELECT COUNT(ttva.recordid)
           FROM   tblTicketsArchive tta
                  INNER JOIN tblTicketsViolationsArchive ttva
                       ON  tta.RecordID = ttva.RecordID
           WHERE  (
                      tta.RecordID = @RecordID
                      OR tta.MidNumber = @Midnumber --ISNULL((SELECT tta2.MidNumber FROM tblTicketsArchive tta2 WHERE tta2.RecordID=@Recordid),'')
                  )
                  AND tta.Clientflag = 0
                  AND CHARINDEX('FTA', ttva.CauseNumber) > 0
                  AND ISNULL(ttva.FTALinkId, 0) <> 0
       ) > 0
    BEGIN
        SET @Res = 1
    END
    ELSE
    BEGIN
        SET @Res = 0
    END
    
    
    RETURN @Res
END  
GO
GRANT EXECUTE ON dbo.fn_CheckFTAViolationForNonClient TO dbr_webuser
GO 
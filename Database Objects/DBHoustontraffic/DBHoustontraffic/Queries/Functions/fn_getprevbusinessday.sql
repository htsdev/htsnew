/****** Object:  UserDefinedFunction [dbo].[fn_getprevbusinessday]    Script Date: 01/25/2008 20:29:00 ******/
DROP FUNCTION [dbo].[fn_getprevbusinessday]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select * from tblcourtviolationstatus where description like '%arraignment%'

--sp_getnextdayarraignment '02/03/2005',4
--select dbo.fn_getprevbusinessday (getdate(),2)
--select DATEPART(dw, '02/04/2004')

CREATE    FUNCTION [dbo].[fn_getprevbusinessday] (@Startdate datetime,@numofdays int)
RETURNS datetime
AS
BEGIN

declare @count int
declare @NextDay datetime
set @count = 1
set @nextday = @Startdate
WHILE @count <= @numofdays
BEGIN
        SET @NextDay = DATEADD(day, -1, @NextDay)
        SET @NextDay = CASE DATEPART(dw, @NextDay)
        WHEN 1 THEN DATEADD(d, -2, @NextDay)
        WHEN 7 THEN DATEADD(d, -1, @NextDay)
        ELSE @NextDay
        END
	set @count = @count + 1
END

return @NextDay
end
GO

/****** Object:  UserDefinedFunction [dbo].[GetViolationSpeed]    Script Date: 01/25/2008 20:29:35 ******/
DROP FUNCTION [dbo].[GetViolationSpeed]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE          Function [dbo].[GetViolationSpeed](@SeqNumber int,@RefCaseNumber varchar(20),@FineAmount money)
Returns varchar(10)
AS
BEGIN
--initialize
Declare @Speed varchar(10)
Select @Speed = ''
	IF(@SeqNumber=1 OR @SeqNumber=2) 
		
		BEGIN
			IF @FineAmount <> 0 
				BEGIN
				IF(Left(@RefCaseNumber,1) ='0' OR Left(@RefCaseNumber,1) ='M')
					BEGIN
					IF(@FineAmount <= 160)
						BEGIN	
							Set @Speed = '10'
						END
					ELSE
						BEGIN
							if @SeqNumber=2
								begin
									Set @Speed = '+' + convert(varchar(10),ceiling(round(((@FineAmount - 160)/5),0)))
								end
							else
								begin
									Set @Speed = '+' + convert(varchar(10),ceiling(round(((@FineAmount - 160)/5)+10,0)))
								end
						
						END
					END
				END
				ELSE
				BEGIN
					Set @Speed = '0'
				END
		END
		
	RETURN  @Speed 
END
GO

/****** Object:  UserDefinedFunction [dbo].[formatcourtnum]    Script Date: 01/25/2008 20:29:14 ******/
DROP FUNCTION [dbo].[formatcourtnum]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE   Function [dbo].[formatcourtnum](@courtnum as varchar(6))
returns varchar(6)
as
begin
	--declare @formatteddate datetime
	if len(@courtnum) = 1
	begin
		set @courtnum = '0' + @courtnum
	
	end
	return @courtnum
end
GO

USE [TrafficTickets]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_Is_PromoClient_HMC]    Script Date: 10/22/2013 06:09:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Sabir Khan
-- Create date: 09/24/2013
-- Description:	This function is used to check for promo price
-- =============================================
--
ALTER FUNCTION [dbo].[fn_Is_PromoClient_HMC]
(
	@TicketID as INT
)
RETURNS  bit
AS
BEGIN

IF EXISTS(SELECT t.ticketid_pk FROM tbltickets t 
		  INNER JOIN tblticketsviolations tv ON tv.TicketID_PK = t.TicketID_PK
		  INNER JOIN tblLetterNotes tn ON tn.RecordID = tv.RecordID AND DATEDIFF(DAY,tn.RecordLoadDate,'10/04/2013')<=0
          INNER JOIN tblCourts c ON c.Courtid = tv.CourtID
          INNER JOIN tblCourtCategories tc ON tc.CourtCategorynum = c.courtcategorynum
          WHERE tc.CourtCategorynum = 1 AND t.TicketID_PK = @TicketID
		  )
		   
BEGIN
	DECLARE @midnumber VARCHAR(200)
	SELECT TOP 1  @midnumber = isnull(t.Midnum,'') FROM tbltickets t 
	INNER JOIN tblTicketsViolations tv ON tv.TicketID_PK = t.TicketID_PK
	INNER JOIN tblCourts c ON c.Courtid = tv.CourtID
	INNER JOIN tblCourtCategories tc ON tc.CourtCategorynum = c.courtcategorynum AND tc.CourtCategorynum = 1
	INNER JOIN tblLetterNotes tn ON tn.RecordID = tv.RecordID AND DATEDIFF(DAY,tn.RecordLoadDate,'10/04/2013')<=0
	WHERE t.TicketID_PK = @TicketID 
	
	DECLARE @temp TABLE(CauseNumber VARCHAR(30))
	INSERT INTO @temp(CauseNumber)
	SELECT DISTINCT tva.CauseNumber FROM tblTicketsArchive ta 
	INNER JOIN tblTicketsViolationsArchive tva ON tva.RecordID = ta.RecordID
	INNER JOIN tblCourts c ON c.Courtid = tva.CourtLocation
	INNER JOIN tblCourtCategories tc ON tc.CourtCategorynum = c.courtcategorynum AND tc.CourtCategorynum = 1
	WHERE ta.MidNumber = ltrim(rtrim(@midnumber))

	DECLARE @tempEvent TABLE(CauseNumber VARCHAR(30),attorneyname VARCHAR(500),barnumber VARCHAR(100),InsertDate DATETIME)
	
	INSERT INTO @tempEvent(CauseNumber,attorneyname, barnumber,InsertDate)
	SELECT replace(te.causenumber,' ',''),attorneyname,barnumber, InsertDate FROM LoaderFilesArchive.dbo.tblEventExtractTemp te
	WHERE (Ltrim(Rtrim(isnull(te.attorneyname,''))) <> '' OR ltrim(rTrim(isnull(te.barnumber,''))) <> '') 
		   and DATEDIFF(DAY,InsertDate,DATEADD(YEAR,-3,GETDATE()))<=0 

IF EXISTS(
		  SELECT te.CauseNumber 
		  FROM @tempEvent te INNER JOIN @temp t ON t.CauseNumber =  te.CauseNumber
		  WHERE (te.attorneyname LIKE '%SULLO%' OR te.barnumber LIKE '24026218')
		)
BEGIN
	RETURN 0
END
ELSE IF EXISTS(
     			SELECT te.CauseNumber FROM @tempEvent te 
     			INNER JOIN @temp t ON t.CauseNumber =  te.CauseNumber
     			WHERE (Ltrim(Rtrim(isnull(te.attorneyname,''))) <> '' OR ltrim(rTrim(isnull(te.barnumber,''))) <> '')     		
     			AND DATEDIFF(DAY,te.InsertDate,DATEADD(YEAR,-3,GETDATE()))<=0     			
			  )
     BEGIN
     	RETURN 1
     END
     ELSE
 	 BEGIN
		RETURN 0
 	 END	
 RETURN 0 
END
ELSE
	BEGIN
		RETURN 0
	END
RETURN 0
END






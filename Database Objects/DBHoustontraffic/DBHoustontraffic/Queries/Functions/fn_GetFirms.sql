/****** Object:  UserDefinedFunction [dbo].[fn_GetFirms]    Script Date: 01/25/2008 20:28:57 ******/
DROP FUNCTION [dbo].[fn_GetFirms]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Function [dbo].[fn_GetFirms](@TicketID int)  
Returns varchar(100)  
AS  
BEGIN  
  
declare @Firms varchar(100)  
  
declare @temp table  
(  
Firm varchar(20)  
)  
  
  
insert into @temp  
select distinct f.FirmAbbreviation from tblticketsviolations tv inner join tblfirm f on tv.coveringfirmid = f.FirmID  
where tv.TicketID_PK = @TicketID and tv.Courtviolationstatusidmain <> 80 AND f.FirmID <> 3000  
  
set @Firms = ''  
  
select @firms = @Firms + t.Firm + ','  
from @temp t  
  
if(len(@Firms)>0)  
set @Firms = substring(@Firms,1,len(@Firms)-1)  
  
return @Firms  
END
GO

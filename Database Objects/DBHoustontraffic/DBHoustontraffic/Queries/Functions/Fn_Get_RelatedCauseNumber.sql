﻿/****** Object:  UserDefinedFunction [dbo].[Fn_Get_RelatedCauseNumber]    Script Date: 11/29/2008 06:04:17 ******/
DROP FUNCTION [dbo].[Fn_Get_RelatedCauseNumber] 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  function [dbo].[Fn_Get_RelatedCauseNumber] (@ticketid int)  
returns varchar(100)  
as  
begin  
  
declare @CauseNumbers varchar(100)  
set @CauseNumbers = ''  
select @CauseNumbers = @CauseNumbers + casenumassignedbycourt +','  from tblticketsviolations where ticketid_pk = @ticketid  
  
if(len(@CauseNumbers)>0)  
set @CauseNumbers = substring(@CauseNumbers,1,len(@CauseNumbers)-1)  
  
return @CauseNumbers  
end
GO
 
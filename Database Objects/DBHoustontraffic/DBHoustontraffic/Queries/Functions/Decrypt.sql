/****** Object:  UserDefinedFunction [dbo].[Decrypt]    Script Date: 01/25/2008 20:28:35 ******/
DROP FUNCTION [dbo].[Decrypt]
GO
CREATE FUNCTION [dbo].[Decrypt](@value [nvarchar](200))
RETURNS [nvarchar](200) WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [Decrypting].[Decrypt.CLRClass].[DecryptFunction]
GO

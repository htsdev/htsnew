﻿/****** 
Create by		: Fahad Muhammad Qureshi
Created Date	: 12/29/2008 
TasK			: 5098

Business Logic  : This Function simply return the CauseNumbers records not having violationstatusID 80 and 236 and active flag must be 1(i-e Client)

List of Parameters:	
	@TicketID	: id against which records are retrived
	
Column Return : 
	CauseNumbers
			
******/
--[dbo].[Fn_HTP_Get_SpecificCauseNumber](47402)
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go



--Ozair 5505 02/09/2009 Violation Number Replaced with Violation Category ID and removed courtid
ALTER function [dbo].[Fn_HTP_Get_SpecificCauseNumber] (@ticketid int,@ViolationCategoryID int, @CaseTypeID int, @CategoryID int)  
returns varchar(max)  
as  
begin  
declare @CauseNumbers varchar(max)  
set @CauseNumbers = ''  
	--Ozair 5505 02/09/2009 space given after ","
	select @CauseNumbers = @CauseNumbers + ISNULL(tv.casenumassignedbycourt,'')+', ' 
	from 		
			dbo.tblTickets t
			INNER JOIN	dbo.tblTicketsViolations tv	
					ON	t.TicketID_PK = tv.TicketID_PK
			INNER JOIN	dbo.tblCourts c	
					ON  tv.courtid = c.Courtid
			LEFT OUTER JOIN	dbo.tblState s 
					ON  t.Stateid_FK = s.StateID			
			INNER JOIN	dbo.tblCourtViolationStatus cvs	
					ON  tv.CourtViolationStatusID = cvs.CourtViolationStatusID         
			INNER JOIN	dbo.tblViolations v	
					ON  tv.ViolationNumber_PK = v.ViolationNumber_PK         
			INNER JOIN	dbo.tblDateType dt 
					ON  cvs.CategoryID = dt.TypeID
	where 
			t.Activeflag = 1
			AND 
			cvs.CourtViolationStatusID not IN(80,236) 
			-- Ozair 5505 02/09/2009 removed courtid check
			AND
			t.TicketID_PK = @TicketID 
			AND --Ozair 5505 02/09/2009 Violation Number Replaced with Violation Category ID 
			((@ViolationCategoryID=0) OR (@ViolationCategoryID !=0 AND v.CategoryID = @ViolationCategoryID))
			AND
			((@CaseTypeID=0)OR (@CaseTypeID!=0 AND c.CaseTypeid=@CaseTypeID))
			AND
			((@CategoryID=0)OR (@CategoryID!=0 AND ((@CategoryID<>0 or (@CategoryID=4 or @CategoryID=5)) and cvs.CategoryID=@CategoryID) )) 

--Ozair 5505 02/09/2009 right spaces removed			
SET @CauseNumbers=RTRIM(@CauseNumbers)

if(len(@CauseNumbers)>0)  
set @CauseNumbers = substring(@CauseNumbers,1,len(@CauseNumbers)-1) 
  
return @CauseNumbers  

 

END

go


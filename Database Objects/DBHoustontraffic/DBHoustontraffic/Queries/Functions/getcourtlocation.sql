/****** Object:  UserDefinedFunction [dbo].[getcourtlocation]    Script Date: 01/25/2008 20:29:27 ******/
DROP FUNCTION [dbo].[getcourtlocation]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--select * from tblcourts

--
--select dbo.getcourtlocation(4)

CREATE    Function [dbo].[getcourtlocation](@courtnumber as varchar(4),@tempcourtloc int)
returns int
as
begin
	
declare @courtlocation int	

if @tempcourtloc in (3001,3002,3003)
begin

	if @courtnumber in ('13','14')
		set @courtlocation = 3002
	if @courtnumber in ('18')
		set @courtlocation = 3003
	if @courtnumber not in ('13','14','18')
		set @courtlocation = 3001
end
else
	set @courtlocation = @tempcourtloc
	return @courtlocation
end


--select * from tblcourtviolationstatus where description like '%jury%'
GO

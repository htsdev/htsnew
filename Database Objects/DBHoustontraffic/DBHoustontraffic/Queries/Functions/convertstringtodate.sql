/****** Object:  UserDefinedFunction [dbo].[convertstringtodate]    Script Date: 01/25/2008 20:28:33 ******/
DROP FUNCTION [dbo].[convertstringtodate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE    Function [dbo].[convertstringtodate](@strdate as varchar(8))
returns datetime
as
begin
	--declare @formatteddate datetime
	declare @tempdate varchar(12)
	if len(@strdate) = 8
	begin
		if @strdate = '00000000'
			set @tempdate = '01/01/1900'
		else
			set @tempdate = left(@strdate,2) + '/' + substring(@strdate,3,2) + '/' + right(@strdate,4)
	end
	
	else
		set @tempdate = '01/01/1900'

	if isdate(@tempdate) = 0
		set @tempdate = '01/01/1900'
		
	return convert(datetime,@tempdate)
end
GO

﻿/******

Business Logic : This Function is used to Disctint the List and remove repetitive records from the given list.    

List of Parameters: 
		@List : List of srting which contains all records which has to be distinct through the function.
		@Delim: Character on which string will break and will distinct the list.
******/

ALTER FUNCTION dbo.Get_DistinctList
(
	@List   VARCHAR(MAX),
	@Delim  CHAR
)
RETURNS VARCHAR(MAX)
AS
BEGIN
    DECLARE @ParsedList TABLE
            (Item VARCHAR(MAX))
    
    DECLARE @list1 VARCHAR(MAX),
            @Pos INT,
            @rList VARCHAR(MAX)
    
    SET @list = LTRIM(RTRIM(@list)) + @Delim
    SET @pos = CHARINDEX(@delim, @list, 1)
    WHILE @pos > 0
    BEGIN
        SET @list1 = LTRIM(RTRIM(LEFT(@list, @pos - 1)))
        IF @list1 <> ''
            INSERT INTO @ParsedList
            VALUES
              (
                CAST(@list1 AS VARCHAR(MAX))
              )
        
        SET @list = SUBSTRING(@list, @pos + 1, LEN(@list))
        SET @pos = CHARINDEX(@delim, @list, 1)
    END
    SELECT @rlist = COALESCE(@rlist, '') + item + ',' --Haris Ahmed 9885 Changes for distinct list
    FROM   (
               SELECT DISTINCT Item
               FROM   @ParsedList
           ) t
    
    RETURN @rlist
END
GO 
GRANT EXECUTE ON dbo.Get_DistinctList TO dbr_webuser
GO 
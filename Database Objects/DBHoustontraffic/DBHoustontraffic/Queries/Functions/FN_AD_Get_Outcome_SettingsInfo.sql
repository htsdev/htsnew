﻿/****** 
Created by:		Syed Muhammad Ozair
Business Logic:	The function is used in Auto Dialer OutCome Details. It returns a string that contains 
				complete case information (court date, time, status, room number, court short name) separated
				by a line break.
				e.g. 10/10/2008 @ 8:00AM - HMC-L #1 - Jury
				 
				
List of Parameters:
	@callid:	Call ID 	

List of Columns:	
	@string: e.g. 10/10/2008 @ 8:00AM - HMC-L #1 - Jury <br>
*******/

alter function [dbo].[FN_AD_Get_Outcome_SettingsInfo] 
(
	@callid int		
)          

returns varchar(500)          

as  

begin 
declare Cur_Violations cursor for        
select distinct  
	d.courtdatetime,
	courtno = 
		(		
		case isnull(d.courtno,'0')
			when '0' then ''  
			else '#'+d.courtno  
		end
		),  
	dt.description,	
	c.shortname         
from 
	autodialercalloutcomedetail d 
inner join         
    tblcourtviolationstatus cvs 
on	d.courtviolationstatusid_fk=cvs.CourtViolationStatusID 
INNER JOIN                        
    dbo.tblDateType dt 
ON	cvs.CategoryID = dt.TypeID
inner join 
	tblcourts c
on  c.courtid=d.courtid          

where	d.callid_fk= @callid    
        
declare @date as datetime          
declare @cnum as varchar(3)        
declare @description as varchar(50)
declare @court as varchar(9)       
declare @string as varchar(500)              
set @string=' '         
        
OPEN Cur_Violations        
        
-- Get the first row.           
FETCH NEXT FROM Cur_Violations          
INTO @date, @cnum,@description,@court      
        
WHILE (@@FETCH_STATUS = 0)          
begin       
set @string=@string+left(convert(varchar,@date,101),10)+' @'+right (convert(varchar,@date,0),7)+' - '+@court+' '+@cnum+' - '+@description+'<br>'        
        
FETCH NEXT FROM Cur_Violations          
INTO @date, @cnum,@description,@court   
          
END           
          
CLOSE Cur_Violations          
DEALLOCATE Cur_Violations        
        
return @string                

end

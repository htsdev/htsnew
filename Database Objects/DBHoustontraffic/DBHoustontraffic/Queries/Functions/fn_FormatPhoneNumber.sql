/****** Object:  UserDefinedFunction [dbo].[fn_FormatPhoneNumber]    Script Date: 01/25/2008 20:28:51 ******/
DROP FUNCTION [dbo].[fn_FormatPhoneNumber]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_FormatPhoneNumber]  (@ContactType varchar(10),@contact varchar(14))  
RETURNS varchar(35)  
AS  
BEGIN  
     
declare @FormattedNumber varchar(35)  
  
if @ContactType like('%Choose%')  
begin  
	 if (isnull(@contact,'') = '')  
		 begin  
		  set @FormattedNumber = ' '  
		 end  
	 else  
		 begin  
			  if(len(@contact))>10  
				  begin  
					set @FormattedNumber = '(' +substring(@contact,1,3) +') ' + substring (@contact,4,3) + '-' + substring(@contact,7,4) + '(' + substring (@contact,11, len(@contact)-10) + ')'  
				  end  
			 else if (len(@contact)=10)  
				  begin  
					set @FormattedNumber = '(' +substring(@contact,1,3) +') ' + substring (@contact,4,3) + '-' + substring(@contact,7,4)  
				  end  
		 end  
end  
  
else if @ContactType not like('%Choose%') and isnull(@contact,'') = ''  
	begin  
		set @FormattedNumber = ' '  
	end  
  
else  
	begin  
		if(len(@contact))>10  
			  begin  
				set @FormattedNumber = @ContactType + ' - (' +substring(@contact,1,3) +') ' + substring (@contact,4,3) + '-' + substring(@contact,7,4) + '(' + substring (@contact,11, len(@contact)-10) + ')'  
			  end  
		  else if (len(@contact)=10)  
			  begin  
				set @FormattedNumber = @ContactType + ' - (' +substring(@contact,1,3) +') ' + substring (@contact,4,3) + '-' + substring(@contact,7,4)  
			  end  
	end  
  
   RETURN(@FormattedNumber)  
END
GO

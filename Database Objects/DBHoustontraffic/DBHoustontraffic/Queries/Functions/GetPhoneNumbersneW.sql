/****** Object:  UserDefinedFunction [dbo].[GetPhoneNumbersneW]    Script Date: 01/25/2008 20:29:30 ******/
DROP FUNCTION [dbo].[GetPhoneNumbersneW]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE        function [dbo].[GetPhoneNumbersneW](@MIDNUMBER AS VARCHAR(15),@TicketNumber varchar(25))  

returns varchar(100)  
as
begin  

DECLARE @TicketID numeric,
	@PhoneNo varchar(100)
	
   

set  	@phoneNo=''  
   
SELECT @TicketID = TicketID_PK FROM TBLTICKETS WHERE MIDNUM = @midnumber
--SELECT  TicketID_PK FROM TBLTICKETS WHERE MIDNUM ='00473926' @midnumber

--select @TicketNumber = ticketnumber from tblticketsarchive where recordid= @recid

declare	@tmpphone	table   
	(   
	PhNumber varchar(50)   
	)   

insert into @tmpphone
	select 	distinct ltrim(rtrim(contact)) as phnumber
	from  	tblticketscontact  tc
	where 	ticketid_pk=@ticketid
	and 	contact not like '00000%'
	and 	contact not like '%0000000%'
	and 	contact not like '01000%'
	and 	ltrim(rtrim(contact)) not in (
			select distinct ltrim(rtrim(phonenumber)) from tblrestrictedphonenumbers
			)
	union

	select  distinct ltrim(rtrim(phonenumber))  as phnumber
	from  	tblticketsarchive
	where 	ticketnumber=@TicketNumber  
	and 	phonenumber not like '%0000000%'  
	and 	phonenumber not like '00000%'  
	and 	phonenumber not like '01000%'       
	and 	phonenumber is not null  
	and 	ltrim(rtrim(phonenumber)) not in (
			select distinct ltrim(rtrim(phonenumber)) from tblrestrictedphonenumbers
			)

select	@phoneno=@phoneno + 
		(case 	when c.[description] is null then  m.phnumber
			else  m.phnumber+'-'+c.[description]
		 end)+ '<br>'
	
	from	@tmpphone m
	left outer join 
		tblticketscontact t
	on	t.contact = m.phnumber
	inner join
		tblcontactstype c
	on 	c.ContactType_pk = t.ContactType_pk
	
if (len(@phoneno) > 4)
	begin
		select @phoneno = left(@phoneno,len(@phoneno)-4)
	end

return( @phoneno)
end
/*



	DECLARE @TicketID numeric  
	declare @RowCnt int   
	declare @MaxRows int   
	declare @PhoneNo varchar(100)   
	declare @temp varchar(20)  
   
 set @temp=''  
 set  @phoneNo=''  
 select  @RowCnt = 1  
   
SELECT @TicketID = TicketID_PK FROM TBLTICKETS WHERE MIDNUM =  @MIDNUMBER
 
 declare @Import table   
   (   
     rownum int IDENTITY (1, 1) Primary key NOT NULL ,   
     PhNo varchar(50)   
    )   
   
 insert into @import (phno)  
     select distinct contact+'-'+[description]
     from  tblticketscontact  tc
     inner join tblcontactstype ctp
     on ctp.contacttype_pk=tc.contacttype_pk 
     where ticketid_pk=@ticketid
     and contact not like '00000%'
     and contact not like '%0000000%'
     and contact not like '01000%'
     and contact not in (
		select distinct phonenumber from tblrestrictedphonenumbers
		)

     union

     select  distinct phonenumber
     from  tblticketsarchive
     where ticketnumber=@ticketnumber  
     and phonenumber not like '%0000000%'  
     and phonenumber not like '00000%'  
     and phonenumber not like '01000%'       
     and phonenumber is not null  
     and phonenumber not in (
		select distinct phonenumber from tblrestrictedphonenumbers
		)

   
 declare @test table   
  (   
  phonenumber varchar(100)  
  )   


delete from @import where rownum in (
	select rownum from @import
	where substring(phno,0,charindex('-',phno,0)) =''
	)

declare @Import2 table   
   (   
     rownum int IDENTITY (1, 1) Primary key NOT NULL ,   
     PhNo varchar(50)   
    )   
   
insert into @import2 (phno) select distinct phno from @import


 set @MaxRows=(select count(*) from @Import2 )  
      
 while @RowCnt <= @MaxRows   
 begin   
  select @temp=phno  
  from  @Import   
  where  rownum = @RowCnt   
  if @rowcnt=1  
   begin  
    insert into @test values (@temp)  
   end  
  else  
   begin  
    update @test set PhoneNumber=PhoneNumber + '<br>' + @temp   
   end  
        Set @RowCnt = @RowCnt + 1   
 end  
 set @phoneno= (select * from @test)  
 return @phoneno  
end 


*/
GO

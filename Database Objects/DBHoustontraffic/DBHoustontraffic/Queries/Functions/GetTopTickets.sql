/****** Object:  UserDefinedFunction [dbo].[GetTopTickets]    Script Date: 01/25/2008 20:29:34 ******/
DROP FUNCTION [dbo].[GetTopTickets]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select dbo.GetTopTickets('234234234-898,')    
CREATE FUNCTION [dbo].[GetTopTickets] (@str varchar(8000))              
RETURNS varchar(50)              
                 
AS              
              
BEGIN              
        
declare @tblletter  table(ticketid varchar(50) )              
            
insert into @tblletter select * from dbo.sap_string_param_forstring(@str)           
    
--if ( select count(*) from @tblletter )= 0         
--return 'polka'        
    
--return (select top 1 ticketid from @tblletter)        
--        
--return ''        
        
declare @temp varchar(50)  
  
select @temp= ticketid from @tblletter  
  
  
if @temp <> ''  
begin  
  
if charindex('-',@temp) !=0  
Begin  
select @temp = substring(@temp,0,charindex('-',@temp))  
--return @temp  
end  
  
--return @temp     
end  
  
  
return @temp  
  
END
GO

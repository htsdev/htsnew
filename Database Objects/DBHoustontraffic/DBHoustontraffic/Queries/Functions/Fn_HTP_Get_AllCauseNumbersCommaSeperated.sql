USE [TrafficTickets]
GO
/****** Object:  UserDefinedFunction [dbo].[Fn_HTP_Get_AllCauseNumbersCommaSeperated]    Script Date: 12/13/2013 04:25:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 /******  
* Rab Nawaz Khan 11431 12/13/2013
* Business Logic : This function is use to get comma seperated Cause Numbers for clients/non-clients
*
* Input Parameters : 
* @TicketID
* @isNonClient
* @RecordId
* Returns : @QUERY       
******/  
  
CREATE FUNCTION [dbo].[Fn_HTP_Get_AllCauseNumbersCommaSeperated] 
(  
 @TicketID     INT,
 @isNonClient BIT, 
 @RecordId INT

)  
RETURNs VARCHAR(MAX)  
AS  
BEGIN  
    DECLARE @QUERY VARCHAR(MAX)  
      
    SET @QUERY = ''  
    -- get the Cause numbers from non clients for LMS address Check. . . 
    IF (@isNonClient = 1) -- Getting the Cause numbers from Non-Clients
		BEGIN
    		SELECT @QUERY = @QUERY +  CASE ttv.CauseNumber WHEN '' THEN ttv.TicketNumber_PK ELSE ttv.CauseNumber end  + ', '  
			FROM   tblTicketsViolationsArchive ttv  
			WHERE  ttv.RecordID = @RecordId
		END
    ELSE
    	BEGIN
    		SELECT @QUERY = @QUERY +  CASE ttv.casenumassignedbycourt WHEN '' THEN ttv.RefCaseNumber ELSE ttv.casenumassignedbycourt end  + ', '  
			FROM   tblTicketsViolations ttv  
			WHERE  ttv.TicketID_PK = @TicketID
    	END     
	
	IF(LEN(@QUERY) > 1)  
    SET @QUERY = LEFT(@QUERY, LEN(@QUERY) -1)  
      
    RETURN @QUERY  
END  


GO
GRANT EXECUTE ON dbo.Fn_HTP_Get_AllCauseNumbersCommaSeperated TO dbr_webuser
GO
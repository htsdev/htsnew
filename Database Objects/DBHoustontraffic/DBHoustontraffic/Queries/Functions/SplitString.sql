IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SplitString]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[SplitString]
go

create function [dbo].[SplitString]
	(
	@str varchar(max),
	@sep char(1)
	)

RETURNS @res TABLE
   (
    sValue     Varchar(8000)
   )
AS

BEGIN

declare @idx int
declare @pos int
declare @len int
declare @temp varchar(8000)

select @idx = 1, @len = len(isnull(@str,'')), @pos = 1, @temp = ''

if @len > 0
	while @idx <= @len
	begin
		if substring(@str, @pos,1) = @sep
			begin
				insert into @res select @temp
				set @temp = ''
			end
		else
			begin
				set @temp = @temp  +  substring(	@str, @pos, 1)	
			end

		if @idx = @len
			insert into @res select @temp

		set @idx = @idx + 1 
		set @pos = @pos  + 1
	end

delete from @res where len(svalue) = 0

return


END
GO

grant execute on dbo.[SplitString] to dbr_webuser
go

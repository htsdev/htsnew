/***********************************************************************************
 * Created By		: Sabir Khan
 * Created Date		: 7/5/2011
 * Task Id			: 8899
 * 
 * Business Logic	: This function is used to get Rep Date for PMC Court
 * 
 * Parameter List	:
 *						@TicketID			: Ticket ID 
 ********************************************************************************/

ALTER Function [dbo].[fn_GetRepDateForPMC] (@ticketid INT)
Returns DATETIME
As
BEGIN
	--Variable declaration
	DECLARE @Maxout                       INT
	DECLARE @RepDateCheck                 DATETIME
	DECLARE @LORRepDateCheck2             DATETIME
	DECLARE @LORRepDateCheck3             DATETIME
	DECLARE @LORRepDateCheck4             DATETIME
	DECLARE @LORRepDateCheck5             DATETIME
	DECLARE @LORRepDateCheck6             DATETIME
	
	-- Getting max out value for PMC pre trial cases...
	SET @Maxout = (SELECT v.[Value]  FROM   Configuration.dbo.[Value] v WHERE  v.DivisionID = 1 AND v.ModuleID = 4
	AND v.SubModuleID = 14 AND v.KeyID = 26)
	DECLARE @selectedCourtDate DATETIME
	SET @selectedCourtDate = '1/1/1900'
	
	-- Getting all rep dates for PMC court
	SELECT 
	         @RepDateCheck = (CONVERT(DATETIME,(tc.RepDate),101)),@LORRepDateCheck2 = (CONVERT(DATETIME,(tc.LORRepDate2),101)),
	         @LORRepDateCheck3 = (CONVERT(DATETIME,(tc.LORRepDate3),101)),@LORRepDateCheck4 = (CONVERT(DATETIME,(tc.LORRepDate4),101)),
	         @LORRepDateCheck5 = (CONVERT(DATETIME,(tc.LORRepDate5),101)),@LORRepDateCheck6 = (CONVERT(DATETIME,(tc.LORRepDate6),101))
	         FROM   tblCourts tc WHERE  tc.Courtid = 3004
	-- check for PMC court
	IF EXISTS(SELECT * FROM tblticketsviolations WHERE TicketID_PK = @ticketid AND CourtID = 3004)
	BEGIN         
	   -- getting max out count for PMC for first rep date in fax LOR
	   DECLARE @TotalCount INT
	   SET @TotalCount = 0
	   SELECT @TotalCount = COUNT(DISTINCT t.TicketID_PK)
	   FROM   tblTicketsViolations tv INNER JOIN tbltickets t  ON  t.TicketID_PK = tv.TicketID_PK
	   --Sabir Khan 9917 11/18/2011 Faxmaker DB has been removed.
	   INNER JOIN faxLetter fl ON fl.Ticketid = t.TicketID_PK		   
	   WHERE  t.Activeflag = 1 AND tv.CourtID = 3004 AND DATEDIFF(DAY, fl.PMCRepDate, @RepDateCheck) =0
	   AND DATEDIFF(DAY,@RepDateCheck,GETDATE())<0
	
	   -- getting max out count for PMC for first rep date in batch LOR	
	   SELECT @TotalCount = @TotalCount + COUNT(DISTINCT t.TicketID_PK)
	   FROM   tblTicketsViolations tv INNER JOIN tbltickets t  ON  t.TicketID_PK = tv.TicketID_PK
	   INNER JOIN tblLORBatchHistory tlh ON tlh.TicketID_PK = t.TicketID_PK	      
	   WHERE  t.Activeflag = 1 AND tv.CourtID = 3004 AND DATEDIFF(DAY,tlh.RepDate,@RepDateCheck) = 0
	   AND DATEDIFF(DAY,@RepDateCheck,GETDATE())<0
	   -- Compare max out value and set the rep date for PMC court
	   IF(@TotalCount<@Maxout AND DATEDIFF(DAY,@selectedCourtDate,'1/1/1900') = 0 AND DATEDIFF(DAY,@RepDateCheck,GETDATE())<0)
	   BEGIN
	        SET @selectedCourtDate = @RepDateCheck                     	
	   END
	   -- getting max out count for PMC for second rep date in fax LOR
	   SET @TotalCount = 0	   
	   SELECT @TotalCount = COUNT(DISTINCT t.TicketID_PK)
	   FROM   tblTicketsViolations tv INNER JOIN tbltickets t  ON  t.TicketID_PK = tv.TicketID_PK
	   --Sabir Khan 9917 11/18/2011 Faxmaker DB has been removed.
	   INNER JOIN faxLetter fl ON fl.Ticketid = t.TicketID_PK		   
	   WHERE  t.Activeflag = 1 AND tv.CourtID = 3004 AND DATEDIFF(DAY, fl.PMCRepDate, @LORRepDateCheck2) =0
	   AND DATEDIFF(DAY,@LORRepDateCheck2,GETDATE())<0
	   -- getting max out count for PMC for second rep date in batch LOR
	   SELECT @TotalCount = @TotalCount + COUNT(DISTINCT t.TicketID_PK)
	   FROM   tblTicketsViolations tv INNER JOIN tbltickets t  ON  t.TicketID_PK = tv.TicketID_PK
	   INNER JOIN tblLORBatchHistory tlh ON tlh.TicketID_PK = t.TicketID_PK	      
	   WHERE  t.Activeflag = 1 AND tv.CourtID = 3004 AND DATEDIFF(DAY,tlh.RepDate,@LORRepDateCheck2) = 0
	   AND DATEDIFF(DAY,@LORRepDateCheck2,GETDATE())<0
	   
	   -- Compare max out value and set the rep date for PMC court
	   IF (@TotalCount<@Maxout AND DATEDIFF(DAY,@selectedCourtDate,'1/1/1900') = 0 AND DATEDIFF(DAY,@LORRepDateCheck2,GETDATE())<0)
	   BEGIN
	   	SET @selectedCourtDate = @LORRepDateCheck2            
	   END
	   -- getting max out count for PMC for second rep date in fax LOR
	   SET @TotalCount = 0
	   SELECT @TotalCount = COUNT(DISTINCT t.TicketID_PK)
	   FROM   tblTicketsViolations tv INNER JOIN tbltickets t  ON  t.TicketID_PK = tv.TicketID_PK
	   --Sabir Khan 9917 11/18/2011 Faxmaker DB has been removed.
	   INNER JOIN faxLetter fl ON fl.Ticketid = t.TicketID_PK		   
	   WHERE  t.Activeflag = 1 AND tv.CourtID = 3004 AND DATEDIFF(DAY, fl.PMCRepDate, @LORRepDateCheck3) =0
	   AND DATEDIFF(DAY,@LORRepDateCheck3,GETDATE())<0
	
	   -- getting max out count for PMC for second rep date in batch LOR	
	   SELECT @TotalCount = @TotalCount + COUNT(DISTINCT t.TicketID_PK)
	   FROM   tblTicketsViolations tv INNER JOIN tbltickets t  ON  t.TicketID_PK = tv.TicketID_PK
	   INNER JOIN tblLORBatchHistory tlh ON tlh.TicketID_PK = t.TicketID_PK	      
	   WHERE  t.Activeflag = 1 AND tv.CourtID = 3004 AND DATEDIFF(DAY,tlh.RepDate,@LORRepDateCheck3) = 0
	   AND DATEDIFF(DAY,@LORRepDateCheck3,GETDATE())<0
	     
	   -- Compare max out value and set the rep date for PMC court
	   IF (@TotalCount<@Maxout  AND DATEDIFF(DAY,@selectedCourtDate,'1/1/1900') = 0  AND DATEDIFF(DAY,@LORRepDateCheck3,GETDATE())<0)
	   BEGIN
	   	SET @selectedCourtDate = @LORRepDateCheck3            
	   END
	   -- getting max out count for PMC for second rep date in FAX LOR
	   SET @TotalCount = 0
	   SELECT @TotalCount = COUNT(DISTINCT t.TicketID_PK)
	   FROM   tblTicketsViolations tv INNER JOIN tbltickets t  ON  t.TicketID_PK = tv.TicketID_PK
	   --Sabir Khan 9917 11/18/2011 Faxmaker DB has been removed.
	   INNER JOIN faxLetter fl ON fl.Ticketid = t.TicketID_PK		   
	   WHERE  t.Activeflag = 1 AND tv.CourtID = 3004 AND DATEDIFF(DAY, fl.PMCRepDate, @LORRepDateCheck4) =0
	   AND DATEDIFF(DAY,@LORRepDateCheck4,GETDATE())<0
	   
	   -- getting max out count for PMC for second rep date in batch LOR
	   SELECT @TotalCount = @TotalCount + COUNT(DISTINCT t.TicketID_PK)
	   FROM   tblTicketsViolations tv INNER JOIN tbltickets t  ON  t.TicketID_PK = tv.TicketID_PK
	   INNER JOIN tblLORBatchHistory tlh ON tlh.TicketID_PK = t.TicketID_PK	      
	   WHERE  t.Activeflag = 1 AND tv.CourtID = 3004 AND DATEDIFF(DAY,tlh.RepDate,@LORRepDateCheck4) = 0
	   AND DATEDIFF(DAY,@LORRepDateCheck4,GETDATE())<0
	   
	   -- Compare max out value and set the rep date for PMC court
	   IF (@TotalCount<@Maxout   AND DATEDIFF(DAY,@selectedCourtDate,'1/1/1900') = 0  AND DATEDIFF(DAY,@LORRepDateCheck4,GETDATE())<0)
	   BEGIN
	   	SET @selectedCourtDate = @LORRepDateCheck4            
	   END
	   -- getting max out count for PMC for second rep date in fax LOR
	   SET @TotalCount = 0
	   SELECT @TotalCount = COUNT(DISTINCT t.TicketID_PK)
	   FROM   tblTicketsViolations tv INNER JOIN tbltickets t  ON  t.TicketID_PK = tv.TicketID_PK
	   --Sabir Khan 9917 11/18/2011 Faxmaker DB has been removed.
	   INNER JOIN faxLetter fl ON fl.Ticketid = t.TicketID_PK		   
	   WHERE  t.Activeflag = 1 AND tv.CourtID = 3004 AND DATEDIFF(DAY, fl.PMCRepDate, @LORRepDateCheck5) =0
	   AND DATEDIFF(DAY,@LORRepDateCheck5,GETDATE())<0
		
	   -- getting max out count for PMC for second rep date in batch LOR
	   SELECT @TotalCount = @TotalCount + COUNT(DISTINCT t.TicketID_PK)
	   FROM   tblTicketsViolations tv INNER JOIN tbltickets t  ON  t.TicketID_PK = tv.TicketID_PK
	   INNER JOIN tblLORBatchHistory tlh ON tlh.TicketID_PK = t.TicketID_PK	      
	   WHERE  t.Activeflag = 1 AND tv.CourtID = 3004 AND DATEDIFF(DAY,tlh.RepDate,@LORRepDateCheck5) = 0
	   AND DATEDIFF(DAY,@LORRepDateCheck5,GETDATE())<0
	   
	   -- Compare max out value and set the rep date for PMC court
	   IF (@TotalCount<@Maxout AND DATEDIFF(DAY,@selectedCourtDate,'1/1/1900') = 0  AND DATEDIFF(DAY,@LORRepDateCheck5,GETDATE())<0)
	   BEGIN
	   	SET @selectedCourtDate = @LORRepDateCheck5            
	   END
	   -- Compare max out value and set the rep date for PMC court	   
	   IF(DATEDIFF(DAY,@selectedCourtDate, '1/1/1900') = 0)	   
	   	BEGIN
	   		SET @selectedCourtDate = @LORRepDateCheck6  
	   	END         
	
	END
	
RETURN @selectedCourtDate
END  


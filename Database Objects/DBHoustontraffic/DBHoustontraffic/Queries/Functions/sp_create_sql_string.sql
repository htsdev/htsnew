/****** Object:  UserDefinedFunction [dbo].[sp_create_sql_string]    Script Date: 01/25/2008 20:29:41 ******/
DROP FUNCTION [dbo].[sp_create_sql_string]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   function [dbo].[sp_create_sql_string]
(@fieldtosearch varchar(100),
@stringtosearch varchar(5000))
RETURNS varchar(8000)
begin
declare @SelectString varchar(8000),
@CriteriaString varchar(5000),
@CategoryIDStr varchar(5000),
@TempString varchar(5000)



--set @stringtosearch = 'cfgrowth;freecf;fcfearn;hirevgpm'
--set @fieldtosearch = 'SearchCriteria'


set  @SelectString = '( '
WHILE CHARINDEX( ';', @stringtosearch) <> 0
	BEGIN  
		SELECT @CategoryIDStr = SUBSTRING(@stringtosearch, 1,  CHARINDEX( ';', @stringtosearch) - 1) 
	 	SELECT @TempString = SUBSTRING(@stringtosearch, CHARINDEX( ';', @stringtosearch) + 1, LEN(@stringtosearch))
 		SELECT @stringtosearch = @TempString
 		SELECT @SelectString = @SelectString + '(' + @fieldtosearch + ' = ''' + @CategoryIDStr + ''') or '
		
	END
	IF (CHARINDEX( ';',@stringtosearch) = 0 AND @stringtosearch <> '')
	BEGIN
 		SELECT @CategoryIDStr = @stringtosearch
		SELECT @SelectString = @SelectString + '(' + @fieldtosearch + ' = ''' + @CategoryIDStr + ''') ) ' 
	END 
	

return @SelectString
end
GO

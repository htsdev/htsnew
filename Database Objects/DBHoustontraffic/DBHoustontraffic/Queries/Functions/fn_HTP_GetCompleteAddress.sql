USE [TrafficTickets]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_HTP_GetCompleteAddress]    Script Date: 09/27/2013 11:11:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rab Nawaz Khan
-- Task ID:		11431
-- Create date: 09/25/2013
-- Description:	This Function is used to get the completed non-client addresses on RecordId basis. . . 
-- =============================================
ALTER FUNCTION [dbo].[fn_HTP_GetCompleteAddress] 
(
	@recordId INT,
	@IsForNewAddress BIT,
	@addressNumber INT
)
RETURNS VARCHAR(1000)
AS
BEGIN
	
	DECLARE @address VARCHAR(1000)
	-- This clause will get the Existing Addresses of Non-clients
	IF (@IsForNewAddress = 0)
	BEGIN
		SELECT @address = ISNULL(tta.Address1,' ')+ ' ' + ISNULL(tta.Address2,' ') 
		+ ' ' + ISNULL(tta.City, ' ') + ' ' + ts.[State] + ', ' +  ISNULL(tta.ZipCode,' ')
		FROM tblTicketsArchive tta INNER JOIN tblState ts
		ON ISNULL(tta.StateID_FK, 45) = ts.StateID
		WHERE tta.RecordID = @recordId
	END
	
	ELSE
	BEGIN
		IF(@addressNumber = 1)
		BEGIN
			SELECT @address = ISNULL(na.Address1,' ')+ ' '  + ISNULL(na.City1, ' ') + ' ' + ts.[State] + ', ' +  ISNULL(na.ZipCode1,' ')
			FROM tblNewAddresses na INNER JOIN tblState ts
			ON ISNULL(na.State1, 45) = ts.StateID
			WHERE na.RecordID = @recordId
		END
		ELSE IF (@addressNumber = 2)
			BEGIN
				SELECT @address = ISNULL(na.Address2,' ')+ ' '  + ISNULL(na.City2, ' ') + ' ' + ts.[State] + ', ' +  ISNULL(na.ZipCode2,' ')
				FROM tblNewAddresses na INNER JOIN tblState ts
				ON ISNULL(na.State2, 45) = ts.StateID
				WHERE na.RecordID = @recordId
			END
	END
	
	-- Removing the double spaces. . . 
	SET @address = REPLACE(LTRIM(RTRIM(@address)), '  ', ' ')
	
	-- if zip code is not exists then remove the comma from end
	IF (RIGHT(@address, 1) = ',')
	BEGIN
		SET @address = SUBSTRING(@address, 1, LEN(@address) -1)
	END
	-- if address is not available then Return N/A
	IF (LEN(@address) < 5)
	BEGIN
		SET @address = 'N/A'
	END
	RETURN @address
END

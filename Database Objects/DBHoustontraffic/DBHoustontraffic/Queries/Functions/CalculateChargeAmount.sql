/****** Object:  UserDefinedFunction [dbo].[CalculateChargeAmount]    Script Date: 01/25/2008 20:28:33 ******/
DROP FUNCTION [dbo].[CalculateChargeAmount]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE     Function [dbo].[CalculateChargeAmount](@SeqNumber int,@RefCaseNumber varchar(20),@FineAmount money,@chargedatabaseprice money)
Returns money
AS
BEGIN
--initialize
Declare @chargePrice money
Select @chargePrice = 0
	IF(@SeqNumber=1 OR @SeqNumber=2)
		
		BEGIN
			IF(Left(@RefCaseNumber,1) ='0' OR Left(@RefCaseNumber,1) ='M')
			BEGIN
				IF(@FineAmount <= 170)
					BEGIN	
						Set @chargePrice = 75
					END
				ELSE
					BEGIN
						Set @chargePrice = 5*ceiling((0.60 * @fineamount)/5)
					END
			END
			ELSE
			BEGIN
				IF @chargedatabaseprice = 0
					BEGIN
						Set @chargePrice = round(75 + (0.35 * @fineamount),0)
					END
				ELSE
					BEGIN
						Set @chargePrice = @chargedatabaseprice
					END
			END
		END
	ELSE
		BEGIN
			IF @chargedatabaseprice = 0
				BEGIN
					Set @chargePrice = round(75 + (0.35 * @fineamount),0)
				END
			ELSE
				BEGIN
					Set @chargePrice = @chargedatabaseprice
				END
		END		
	return @chargePrice
END
GO

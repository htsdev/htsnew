/****** Object:  UserDefinedFunction [dbo].[Fn_CheckCaseDiscrepency]    Script Date: 01/25/2008 20:28:36 ******/
DROP FUNCTION [dbo].[Fn_CheckCaseDiscrepency]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[Fn_CheckCaseDiscrepency](@TicketID_pk as int,@CourtDate as Datetime)      
returns bit      
as      
begin      
    
    
declare @returnValue as Bit    
declare @ticketsviolationid int    
    
    
set @returnValue = 0    
    
declare Crtemp cursor for                
Select Ticketsviolationid from tblticketsviolations where TicketID_pk=@TicketID_pk and    
datediff(DAY,courtdatemain,@courtdate) = 0     
    
open Crtemp                
fetch next from Crtemp                
 into @ticketsviolationid                
WHILE @@FETCH_STATUS = 0                
 BEGIN                
    
 if ( dbo.fn_CheckDiscrepency(@ticketsviolationid) = 1 )    
 set @returnValue = 1    
    
fetch next from Crtemp                
  into @ticketsviolationid                
 END                 
close Crtemp                
DEALLOCATE Crtemp        
     
return @returnValue    
      
end
GO

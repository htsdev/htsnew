/****** Object:  UserDefinedFunction [dbo].[fn_convert_timestring_to_time]    Script Date: 01/25/2008 20:28:40 ******/
DROP FUNCTION [dbo].[fn_convert_timestring_to_time]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select dbo.fn_convert_timestring_to_time('8000')

CREATE     function [dbo].[fn_convert_timestring_to_time](@courttime varchar(4))
returns varchar(10)
as
begin
declare
--@CourtDate varchar(20),
@strhour int,
@strmin varchar(2),
--@courttime varchar(4),
@strtimestring varchar(10)

--set @courttime = '1030'
--set @CourtDate = '04/12/2004'


set @strhour = convert(int,left(@courttime,2))
set @strmin = right(@courttime,2)
if convert(int,@strmin) >= 60
begin
	set @strmin = '00'
end

if @strhour > 24
begin
	set @strtimestring = '00' + ':' +   '00' + ' ' + 'AM'
end
else
begin
if @strhour >= 12 
begin
	if @strhour = 12
	begin
		set @strtimestring = convert(varchar(2),@strhour) + ':' +   convert(varchar(2),@strmin) + ' ' + 'PM'
	end 
	else
	begin
		set @strtimestring = convert(varchar(2),@strhour-12) + ':' +   convert(varchar(2),@strmin) + ' ' + 'PM'
	end
end
else
begin
	set @strtimestring = convert(varchar(2),@strhour) + ':' +   convert(varchar(2),@strmin) + ' ' + 'AM'
end
end
return @strtimestring

end
GO

/****** Object:  UserDefinedFunction [dbo].[formatdateandtime]    Script Date: 01/25/2008 20:29:15 ******/
DROP FUNCTION [dbo].[formatdateandtime]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE    Function [dbo].[formatdateandtime](@date as datetime)
returns varchar(30)
as
begin
	--declare @formatteddate datetime
	declare @tempdate varchar(30)
	declare @strhour int,
	@strmin varchar(2)
	declare @strtimestring varchar(20)
	if isdate(@date) = 1
	begin
		set @strhour = datepart(hour,@date)
		set @strmin = convert(varchar(2),datepart(n,@date))
		if len(@strmin) = 1
			begin
				set @strmin = '0' + @strmin
			end
		
		if @strhour >= 12 
			begin
				if @strhour = 12
					begin
						set @strtimestring = convert(varchar(2),@strhour) + ':' +   @strmin + ' ' + 'PM'
					end 
				else
					begin
						set @strtimestring = convert(varchar(2),@strhour-12) + ':' +   @strmin + ' ' + 'PM'
					end
			end
		else
			begin
				set @strtimestring = convert(varchar(2),@strhour) + ':' +   @strmin + ' ' + 'AM'
			end
		set @tempdate = convert(varchar(2),month(@date)) + '/' + convert(varchar(2),day(@date)) + '/' + convert(varchar(4),year(@date)) + ' ' + @strtimestring
	end
	else
	set @tempdate = '01/01/1900'
	return @tempdate
end
GO

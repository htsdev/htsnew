USE TrafficTickets
-- ================================================
-- Created by Rab Nawaz
-- Task ID : 10729
-- Date :	04/15/2013
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- =============================================
CREATE FUNCTION [dbo].[fn_HTP_Get_ClientNumbers] 
(
	@ticketId INT
)
RETURNS VARCHAR (500)
AS
BEGIN
	DECLARE @numbers VARCHAR (500)

	DECLARE @temp TABLE (
		c1 VARCHAR (50),
		t1 INT,
		c2 VARCHAR (50),
		t2 INT,
		c3 VARCHAR (50),
		t3 INT)
		
	INSERT INTO @temp
	SELECT tt.Contact1 AS c1, tt.ContactType1 AS t1, tt.Contact2 AS c2, tt.ContactType2 AS t2, tt.Contact3 AS c3, tt.ContactType3 AS t3 
	FROM TrafficTickets.dbo.tblTickets tt 
	WHERE tt.TicketID_PK = @ticketId


	DECLARE @nums TABLE (
		c1 VARCHAR (50),
		t1 VARCHAR (50),
		c2 VARCHAR (50),
		t2 VARCHAR (50),
		c3 VARCHAR (50),
		t3 VARCHAR (50)
	)
	INSERT INTO @nums
	SELECT TrafficTickets.dbo.formatphonenumbers(c1) AS c1, tc.[Description] AS t1,  TrafficTickets.dbo.formatphonenumbers(c2) AS c2, 
	tc2.[Description] AS t2,
	TrafficTickets.dbo.formatphonenumbers(c3) AS c3, tc3.[Description] AS t3
	FROM TrafficTickets.dbo.tblContactstype tc RIGHT OUTER JOIN @temp t ON tc.ContactType_PK = t.t1
	LEFT OUTER JOIN  TrafficTickets.dbo.tblContactstype tc2 ON tc2.ContactType_PK = t.t2
	LEFT OUTER JOIN  TrafficTickets.dbo.tblContactstype tc3 ON tc3.ContactType_PK = t.t3

	SET @numbers = ''
	IF ((SELECT LEN(c1) FROM @nums) > 0)
	BEGIN
		SELECT @numbers = c1 + ' ('+SUBSTRING(t1, 1, 1)+')' FROM @nums 
	END

	IF ((SELECT LEN(c2) FROM @nums) > 0)
	BEGIN
		DECLARE @num2 VARCHAR (100)
		SET @num2 = '<br />'
		SELECT @num2 = ' <br /> ' + c2 + ' ('+SUBSTRING(t2, 1, 1)+')' FROM @nums
		SET @numbers = @numbers + @num2 
	END

	IF ((SELECT LEN(c3) FROM @nums) > 0)
	BEGIN
		DECLARE @num3 VARCHAR (100)
		SET @num3 = '<br />'
		SELECT @num3 = ' <br /> ' + c3 + ' ('+SUBSTRING(t3, 1, 1)+')' FROM @nums
		SET @numbers = @numbers + @num3 
	END	

	-- Return the result of the function
	RETURN @numbers
END
GO
GRANT EXECUTE ON [dbo].[fn_HTP_Get_ClientNumbers] TO dbr_webuser
GO


 /******  
* Babar Ahmad 9660 09/07/2011 
* Business Logic : This function is use to get comma seperated Cause Numbers through TicketID
*
* Input Parameters : 
* @TicketID
* 
* Returns : @QUERY       
******/  
  
ALTER FUNCTION dbo.Fn_HTP_Get_AllCauseNumbersWithComma 
(  
 @TicketID     INT

)  
RETURNs VARCHAR(MAX)  
AS  
BEGIN  
    DECLARE @QUERY VARCHAR(MAX)  
      
    SET @QUERY = ''  
      
    SELECT @QUERY = @QUERY +  CASE ttv.casenumassignedbycourt WHEN '' THEN ttv.RefCaseNumber ELSE ttv.casenumassignedbycourt end  + ', '  
	FROM   tblTicketsViolations ttv  
    WHERE  ttv.TicketID_PK = @TicketID             
	
	IF(LEN(@QUERY) > 1)  
    SET @QUERY = LEFT(@QUERY, LEN(@QUERY) -1)  
      
    RETURN @QUERY  
END  


GO
GRANT EXECUTE ON dbo.Fn_HTP_Get_AllCauseNumbersWithComma TO dbr_webuser
GO

-- =============================================
-- Author:		Tahir Ahmed
-- Create date: 01/23/2009
-- Description:	The functions gets the promotional pricing template
-- =============================================

-- select dbo.[GetPromoPriceTemplate]('75210',78)
alter FUNCTION [dbo].[GetPromoPriceTemplate] 
(
	-- Add the parameters for the function here
	@ZipCode varchar(10), 
	@MailerType TINYINT
	
)
--Yasir Kamal 7226 01/18/2010 return mailer id and price.
RETURNS VARCHAR(100)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @TemplateId TINYINT
	DECLARE @TemplatePrice MONEY
	DECLARE @TemplateInfo VARCHAR(100)
	SET @TemplateId = 0
	SET @TemplatePrice = 35

	SET @zipcode = LEFT(@zipcode, 5)

	SELECT	TOP 1 @TemplateId =  s.PromotionalTemplateId,
				  @TemplatePrice = s.SchemePrice
	FROM    dbo.PromotionalSchemes AS s INNER JOIN
			dbo.PromotionalSchemes_MailerType AS m ON s.SchemeId = m.SchemeId INNER JOIN
			dbo.PromotionalSchemes_ZipCodes AS z ON m.SchemeMailerId = z.SchemeMailerId
	WHERE   s.IsActiveScheme = 1 
	AND		m.IsActive = 1
	AND		z.IsActiveZip = 1
	AND DATEDIFF(DAY, s.EffectiveFromDate, GETDATE()) >= 0
	AND DATEDIFF(DAY, s.EffecttiveToDate, GETDATE()) <= 0
	AND ( ISNULL(z.IsForAllZipCodes,0) = 1 OR  z.ZipCode = @zipcode)
	AND m.MailerType = @mailertype
	ORDER BY s.SchemeId DESC

	SET @TemplateInfo = CONVERT(VARCHAR, @TemplateId ) +',' +  CONVERT(VARCHAR, @TemplatePrice)
	
	-- Return the result of the function
	RETURN @TemplateInfo

END

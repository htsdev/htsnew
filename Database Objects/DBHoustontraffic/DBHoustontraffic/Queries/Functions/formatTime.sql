/****** Object:  UserDefinedFunction [dbo].[formatTime]    Script Date: 01/25/2008 20:29:20 ******/
DROP FUNCTION [dbo].[formatTime]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---Select dbo.formattime(getdate())



CREATE    Function [dbo].[formatTime](@date as datetime)
returns varchar(30)
as
begin
	--declare @formatteddate datetime
	declare @tempdate varchar(30)
	declare @strhour int,
	@strmin varchar(2)
	declare @strtimestring varchar(20)
	if isdate(@date) = 1
	begin
		set @strhour = datepart(hour,@date)
		set @strmin = convert(varchar(2),datepart(n,@date))
		if len(@strmin) = 1
			begin
				set @strmin = '0' + @strmin
			end
		
		if @strhour >= 12 
			begin
				if @strhour = 12
					begin
						set @strtimestring = convert(varchar(2),@strhour) + ':' +   @strmin + ' ' + 'pm'
					end 
				else
					begin
						set @strtimestring = convert(varchar(2),@strhour-12) + ':' +   @strmin + ' ' + 'pm'
					end
			end
		else
			begin
				set @strtimestring = convert(varchar(2),@strhour) + ':' +   @strmin + ' ' + 'am'
			end
		set @tempdate =@strtimestring
	end
	else
	set @tempdate = '01/01/1900'
	return @tempdate
end
GO

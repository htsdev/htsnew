/******   
Alter by:  Zeeshan Ahmed  
Business Logic: The function is used in reminder calls. It returns a string that contains   
    complete case information (court date, time, status, room number) separated  
    by a line break.  
    Zeeshan 4837 09/23/2008 Display Other violations of case on reminder call whose Court Date is within 7 days.  
    ozair 4837 10/08/2008 included court short name and now the out put should be like this   
    e.g. 10/10/2008 @ 8:00AM - HMC-L #1 - Jury  
      
      
List of Parameters:  
 @TicketID: Ticket ID associated with the case.  
 @CourtDate: Court date associated with the violation.  
  
List of Columns:   
  
*******/  
  
ALTER function [dbo].[fn_hts_get_TicketsViolations]   
 (  
  @ticketid int,  
  @courtdate datetime  
 )            
  
returns varchar(500)            
  
as            
  
begin                    
declare Cur_Violations cursor for          
select distinct    
--Sabir Khan 7979 07/14/2010 check for sugarland court for couurt room number
 case when len(isnull(ttv.refcasenumber,'')) = 0 then ttv.casenumassignedbycourt else ttv.refcasenumber end , 
 ttv.courtdatemain,
 courtnumbermain =   
  (  
  -- tahir 4359 07/04/2008  
  -- fixed null value issue for court number .... 
  -- Sabir Khan 7979 07/13/2010 Fix court number issues 
  case isnull(ttv.courtnumbermain,'0')  
   when '0' then ''    
   else '#'+ttv.courtnumbermain    
  end  
  ),    
 dt.description,  
 --ozair 4837 10/08/2008  
 c.shortname           
from   
 tblticketsviolations ttv   
inner join           
    tblcourtviolationstatus cvs   
on ttv.courtviolationstatusidmain=cvs.CourtViolationStatusID   
INNER JOIN                          
    dbo.tblDateType dt   
ON cvs.CategoryID = dt.TypeID  
--ozair 4837 10/08/2008  
inner join   
 tblcourts c  
on  c.courtid=ttv.courtid            
  
where ttv.courtviolationstatusidmain<>80      
and  ticketid_pk=@ticketid       
--ozair 4837 10/07/2008 Court Date is within 7 days  
AND  DATEDIFF(day, ttv.CourtDateMain,@courtdate) between -7 and 0     
          
declare @date as datetime            
declare @cnum as varchar(3)          
declare @description as varchar(50)  
--ozair 4837 10/08/2008  
declare @court as varchar(9)         

declare @string as varchar(500)                
declare @TicketNumber varchar(500)
set @string=' '           
          
OPEN Cur_Violations          
          
-- Get the first row. 
--Sabir Khan 5361 12/26/2008 Ticket Number has been included...            
FETCH NEXT FROM Cur_Violations            
INTO @TicketNumber,@date,@cnum,@description,@court      --ozair 4837 10/07/2008  
          
WHILE (@@FETCH_STATUS = 0)            
begin        --ozair 4837 10/07/2008 
set @string=@string+@TicketNumber + ' - ' +left(convert(varchar,@date,101),10)+' @'+right (convert(varchar,@date,0),7)+' - '+@court+' '+@cnum+' - '+@description+'<br>'          
          
FETCH NEXT FROM Cur_Violations            
INTO @TicketNumber,@date, @cnum,@description,@court   --ozair 4837 10/07/2008           
            
END             
            
CLOSE Cur_Violations            
DEALLOCATE Cur_Violations          
          
return @string          
          
end  
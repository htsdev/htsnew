/****** Object:  UserDefinedFunction [dbo].[formatdate]    Script Date: 01/25/2008 20:29:14 ******/
DROP FUNCTION [dbo].[formatdate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE Function [dbo].[formatdate](@date as datetime)
returns varchar(10)
as
begin
	--declare @formatteddate datetime
	declare @tempdate varchar(20)
	if isdate(@date) = 1
	begin
		set @tempdate = convert(varchar(2),month(@date)) + '/' + convert(varchar(2),day(@date)) + '/' + convert(varchar(4),year(@date))
	end
	else
	set @tempdate = '01/01/1900'
	return left(@tempdate,10)
end
GO

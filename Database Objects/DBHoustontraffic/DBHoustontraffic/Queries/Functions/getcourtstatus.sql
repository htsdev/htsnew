/****** Object:  UserDefinedFunction [dbo].[getcourtstatus]    Script Date: 01/25/2008 20:29:27 ******/
DROP FUNCTION [dbo].[getcourtstatus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--select * from tblcourtviolationstatus where description like 'jury trial%'
--select * from tblcourtviolationstatus where description like 'non jury%'
--select dbo.getcourtstatus(4)

CREATE      Function [dbo].[getcourtstatus](@statusid as int)
returns varchar(6)
as
begin
	declare @description varchar(100),
	@violationsttausid int
	select @description = description from tblcourtviolationstatus where courtviolationstatusid  = @statusid
	

	if @description like '%arraignment%'
		set @violationsttausid = 3
	if @description like 'jury trial%'
		set @violationsttausid =  26
	if @description like 'non jury%'
		set @violationsttausid =  103
	if @description like '%pre%'
		set @violationsttausid =  101
	if @violationsttausid is null
		set @violationsttausid =  @statusid
	return @violationsttausid
end


--select * from tblcourtviolationstatus where description like '%jury%'
GO

/*********************
Business Logic: this function is used to get minimum payment date 
input parameters: ticketid
returns: minimum payment date.
*********************/

/****** Object:  UserDefinedFunction [dbo].[Fn_GetMinPaymentDate]    Script Date: 01/17/2008 06:04:17 ******/

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--Yasir Kamal 6050 07/20/2009 check payment is not void

ALTER FUNCTION [dbo].[Fn_GetMinPaymentDate](@ticketid as int)
returns datetime
as
begin
	declare @paymentdate datetime
	
	select @paymentdate = min(recdate) from tblticketspayment 
	where ticketid   =@ticketid
	
	 AND PaymentVoid = 0
	group by ticketid
	

	return @paymentdate
end

GO

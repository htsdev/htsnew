﻿/************************************************************
 * CREATED BY		: Abid Ali
 * CREATED DATE		: 01/06/2009
  * Task Id			: 5359
 * 
 * Business Logic	: Get concatd refcasenumbers from Letter of Rep batch history
 * 
 * Parameter List	:
 *						@CertifiedMailNumber	: Certified Mail Number	
 *						@TicketID				: Ticket Id
 *						@BatchID				: Batch ID
 ************************************************************/

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[FN_GET_ConcatedRefCaseNumbersFromLORBatchHistory]
(
	@BatchID              INT
)
RETURNS VARCHAR(MAX)
AS
BEGIN
    DECLARE @RefCaseNumbers VARCHAR(MAX)
    SET @RefCaseNumbers = ''
    
    SELECT @RefCaseNumbers = @RefCaseNumbers + (CASE WHEN LEN(@RefCaseNumbers) > 0 THEN ', ' ELSE '' END) 
           + (
               CASE 
                    WHEN LEN(ISNULL(dbo.tblLORBatchHistory.casenumassignedbycourt, '')) > 
                         0 THEN (
                             ISNULL(dbo.tblLORBatchHistory.casenumassignedbycourt, '') + (
                                 CASE 
                                      WHEN dbo.tblLORBatchHistory.IsBond = 1 THEN 
                                           ' (BOND)'
                                      ELSE ''
                                 END
                             )
                         )
                    ELSE ''
               END
           )
    FROM   dbo.tblLORBatchHistory          
    WHERE  dbo.tblLORBatchHistory.BatchID_FK = @BatchID
    
    RETURN ISNULL(@RefCaseNumbers, '')
END
GO




GRANT EXECUTE ON [dbo].[FN_GET_ConcatedRefCaseNumbersFromLORBatchHistory] TO 
dbr_webuser

GO
 
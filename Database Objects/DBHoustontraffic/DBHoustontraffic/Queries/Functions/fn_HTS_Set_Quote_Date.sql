/****** Object:  UserDefinedFunction [dbo].[fn_HTS_Set_Quote_Date]    Script Date: 01/25/2008 20:29:08 ******/
DROP FUNCTION [dbo].[fn_HTS_Set_Quote_Date]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Sarim>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[fn_HTS_Set_Quote_Date]
(
	 @date datetime
)
RETURNS datetime
AS
BEGIN
	-- Declare the return variable here
	

--declare @date datetime
declare @daynumber int

---set @date = '10/24/2007'

set @daynumber =  DATEPART(dw, @date) 



if @daynumber < 5 
begin
set @date = @date + 3
end

if @daynumber = 5 
begin
set @date = @date + 5
end

if @daynumber = 6
begin
set @date = @date + 4
end


if @daynumber = 7
begin
set @date = @date + 3
end


	RETURN @date

END
GO

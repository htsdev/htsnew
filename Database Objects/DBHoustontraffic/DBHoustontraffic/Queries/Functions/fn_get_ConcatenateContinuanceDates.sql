/*
* Created By: Muhammad Ali
* Business Logic:
* This Function is use to get all Continuance date Againts ticketID and Result Will be Concatenate i.e Cont-09/06/2010;Cont-09/19/2010....
* Input Parameters:
* @ticketID
* Ouput:
* Cont-09/06/2010;Cont-09/19/2010....
*/
ALTER FUNCTION fn_get_ConcatenateContinuanceDates(@ticketID INT)
    RETURNS VARCHAR(max)
AS
BEGIN
DECLARE @Continuance VARCHAR(max)
SET @Continuance =''
SELECT @Continuance =@Continuance+'Cont-'+ dbo.fn_DateFormat(ContinuanceDate,33,'/','',0)+';' FROM  tblTicketsFlag WHERE TicketID_PK=@ticketID 
AND FlagID = 9 AND ISNULL(IsPaid,0)=1 ORDER BY RecDate
RETURN @Continuance
END


set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

/******     
Create by  : Sabir Khan    
Created Date : 01/16/2009    
Task ID   :     
Business Logic : This function is used to Get date from reminder comments and return max date.
    
Parameters :
------------     
@ticketid : Ticketid.
******/    
 
CREATE Function [dbo].[fn_htp_Get_Max_ReminderCommentDate](@ticketid int)
returns VARCHAR(30)
as
begin
	
declare @reminderdate VARCHAR(20)	

DECLARE @temp table(Reminderdate DATETIME);

INSERT INTO @temp(ReminderDate)

SELECT CONVERT(DATETIME,dbo.GetDateFromComments(remindercomments)) FROM tblticketsviolations WHERE ticketid_pk = @ticketid

SELECT @reminderdate = CONVERT(VARCHAR(20),MAX(Reminderdate)) FROM @temp

return dbo.fn_dateformat(CONVERT(DATETIME,@reminderdate),4,'/',':',0)

end

GO

GRANT EXEC ON [dbo].[fn_htp_Get_Max_ReminderCommentDate] TO dbr_webuser

GO


/****** Object:  UserDefinedFunction [dbo].[USP_HTS_Get_offenceDates]    Script Date: 01/25/2008 20:29:42 ******/
DROP FUNCTION [dbo].[USP_HTS_Get_offenceDates]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[USP_HTS_Get_offenceDates] (@ticketid int)

RETURNS varchar(200)  AS  

BEGIN 
	--DECLARE @ticketid int
	DECLARE @ViolationDate varchar(200)
	set @ViolationDate = ''
	--SET @ticketid = 4363

	declare @temp1 table
	(ticketviolationdate DATETIME)

	insert into @temp1
		select distinct ticketviolationdate
			from tblticketsviolations 
				where 	ticketid_pk = @ticketid 
				AND ticketviolationdate <> '1900-01-01 00:00:00.000' 
				and refcasenumber not like 'F%'


	select @ViolationDate = @ViolationDate + 

	convert(varchar(6),ticketviolationdate,101)+right(convert(varchar(10),ticketviolationdate,101),2) + ' - ' 
		from @temp1


	if @ViolationDate = ''
		begin
			set @ViolationDate = ' '
		end	
	else 
		begin
			set @ViolationDate = left(@ViolationDate, len(@ViolationDate)-2)
		end

	RETURN @ViolationDate
END
GO

﻿ /******   
Created by:  Zeeshan 4837 09/23/2008  
Business Logic: The function is used in reminder calls. It returns violationids in cvs format   
      

List of Parameters:  
 @TicketID: Ticket ID associated with the case.  
 @CourtDate: Court date associated with the violation.  
  
List of Columns:   

*******/  
  
alter function [dbo].[fn_hts_get_TicketsViolationIds]   
 (  
  @ticketid int,  
  @courtdate datetime  
 )            

--Yasir Kamal 7010 11/24/2009 set to max inorder to get all violaton id's
returns varchar(MAX)            
  
as            
  
begin            
          
declare @string as varchar(MAX)          
set @string = ''

select 

 @string =  @string + Convert(varchar,ttv.Ticketsviolationid ) + ','
        
from   
 tblticketsviolations ttv   
inner join           
    tblcourtviolationstatus cvs   
on ttv.courtviolationstatusidmain=cvs.CourtViolationStatusID   
INNER JOIN                          
    dbo.tblDateType dt   
ON cvs.CategoryID = dt.TypeID             
  
where ttv.courtviolationstatusidmain<>80      
and  ticketid_pk=@ticketid       
AND (	DATEDIFF([day], ttv.CourtDateMain,@courtdate) = 0   or DATEDIFF([day], ttv.CourtDateMain,@courtdate) >=-7 )

return @string

end

/****** Object:  UserDefinedFunction [dbo].[listdatereport1]    Script Date: 01/25/2008 20:29:37 ******/
DROP FUNCTION [dbo].[listdatereport1]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE     Function [dbo].[listdatereport1](@datediff as int,@todaysdate as datetime)
returns int
as
begin
	declare @totalcount int
	
	select @totalcount = count(distinct midnum) 
	from tbltickets T, tblticketsviolations V
	where T.ticketid_pk = V.ticketid_pk
	and midnum in 
	(select distinct midnumber from tblticketsarchive 
	where datediff(day,listdate,@todaysdate) = @datediff
	)
	and activeflag = 1
	and refcasenumber not in (
	select distinct ticketnumber  from tblticketsarchive 
	where  datediff(day,listdate,@todaysdate) <= @datediff )
	return @totalcount
end
GO

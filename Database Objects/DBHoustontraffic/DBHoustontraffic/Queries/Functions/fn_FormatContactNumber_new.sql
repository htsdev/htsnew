set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


/****** 

Create by:  Sarim Ghani

Business Logic : This function is used to format contact number

List of Parameters:     

	@ContactNumber  : Contact Number 
	@ContactType : Contct Type i.e. Home , Mobile etc

List of Columns: 

	Return Contact Number in the following format (713) 247-5100 X-281   
	
******/


--select dbo.fn_FormatContactNumber_new('7132475100281',-4)

ALTER FUNCTION [dbo].[fn_FormatContactNumber_new](@ContactNumber as varchar(20), @ContactType as int) 
RETURNS varchar(40) AS  
BEGIN 
Declare @FormatedContactNumber as varchar(40)
Declare @ContactTypeDesc as varchar(20)
Set @ContactTypeDesc = (Select Description from tblContactsType where ContactType_PK = @ContactType)
Set @ContactTypeDesc = @ContactTypeDesc

if len(@ContactNumber)>10
	Set @FormatedContactNumber = '(' + isnull(SubString(@ContactNumber,1,3), 'XXX')+')' + ' ' + isnull(SubString(@ContactNumber,4,3), 'XXX') + '-' + isnull(SubString(@ContactNumber,7, 4), 'XXXX') + ' ' +'X-' + isnull(SubString(@ContactNumber,11, 4), 'XXXX') + ''+ (case when @ContactTypeDesc is null then '' else '(' + @ContactTypeDesc + ')' end)
else if len(@ContactNumber)= 0
    Set @FormatedContactNumber = ''
else
	Set @FormatedContactNumber ='(' + isnull(SubString(@ContactNumber,1,3), 'XXX') +')' + ' ' + isnull(SubString(@ContactNumber,4,3), 'XXX') + '-' + isnull(SubString(@ContactNumber,7, 4), 'XXXX') +   (case when @ContactTypeDesc is null then '' else '(' + @ContactTypeDesc + ')' end)

Return @FormatedContactNumber
END
















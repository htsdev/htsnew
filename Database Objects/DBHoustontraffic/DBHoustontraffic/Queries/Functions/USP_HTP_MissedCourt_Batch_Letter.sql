/****** 

Create by:  Zeeshan Ahmed

Business Logic : This procedure is used to get cases information for missed court letter 

List of Parameters:     

	@empid  : Employee ID 
	@LetterType : Letter Type
	@TicketIDList : Ticket Id's of the cases. (i.e. primary key of the case). 

List of Columns: 

	TicketID_PK : TicketId
	FirstName : First Name 
	LastName : Last Name
	Address1 : Address 
	City : City
	Zip : Zip 
	State : State
	RefcaseNumber : Ref case numbers     
	CourtDate : Court Date
	CourtPhoneNumber : Court Phone Number
	
******/

--USP_HTP_MissedCourt_Batch_Letter 3992, 2, '797343647, 1638043645, 6850843648'
ALTER procedure [dbo].[USP_HTP_MissedCourt_Batch_Letter] 
@empid int, 
@LetterType int, 
@TicketIDList varchar(8000) 
 
as 
begin 

declare @Counter int
declare @CaseNumber varchar(1000)
set @CaseNumber = ''
 
declare @tickets table ( 
 ticketid numeric 
 ) 
 
insert into @tickets 
 select * from dbo.Sap_String_Param_New(@TicketIDList) 
 
create table #Temp_CauseNo
(TicketID_PK int, FirstName varchar(50), LastName varchar(50), Address1 varchar(150), City varchar(50), Zip varchar(12), State varchar(20), RefcaseNumber varchar(1000), 
languageSpeak varchar(15), CourtDate datetime, CourtNumberMain varchar(10), Status varchar(15), Location varchar(150),CourtPhoneNumber varchar(50), BatchID_pk varchar(50), RowID int identity (1,1)) 
 
insert into #Temp_CauseNo
select T.TicketID_PK , T.FirstName , T.LastName , T.Address1 ,T.City , T.Zip ,S1.State, TV.RefcaseNumber , T.languageSpeak, Max(TV.CourtDateMain) , TV.CourtNumberMain , CVS.ShortDescription as Status
 , C.Address + ', ' + C.City + ', '+S2.State +' ' + C.Zip as Location, dbo.fn_FormatContactNumber_new(isnull(c.Phone,''),-1) , BP.BatchID_pk from
tblHTSBatchPrintLetter BP 
join tbltickets T 
on T.TicketID_Pk = BP.Ticketid_FK 
join tblticketsviolations TV 
On T.TicketID_Pk = TV.Ticketid_pk 
join tblcourtviolationstatus CVS 
On CVS.courtviolationstatusid = TV.CourtViolationStatusidmain 
join tblcourts C 
on C.courtID = TV.CourtID 
join tblState S1 
on S1.Stateid = t.StateID_fk 
join tblState S2 
on S2.Stateid = C.State 
where 
cast(convert(varchar(12), BP.ticketid_fk) + convert(varchar(12), BP.BatchID_pk) as numeric) 
IN (select ticketid from @tickets) 
and letterid_fk = @LetterType 
group by T.TicketID_PK , T.FirstName , T.LastName , T.Address1 ,T.City , T.Zip ,S1.State, TV.RefcaseNumber , T.languageSpeak, TV.CourtNumberMain , CVS.ShortDescription
 , C.Address + ', ' + C.City + ', '+S2.State +' ' + C.Zip, dbo.fn_FormatContactNumber_new(isnull(c.Phone,''),-1), BP.BatchID_pk

Select @Counter = count(RowID) from #Temp_CauseNo
While @Counter > 0
	Begin
		Set @CaseNumber = ''

		select @CaseNumber = @CaseNumber + RefcaseNumber + ', ' from #Temp_CauseNo 
		where TicketID_PK in (Select TicketID_PK from #Temp_CauseNo where RowID = @Counter)
		group by RefcaseNumber

		set @CaseNumber = left(@CaseNumber, len(@CaseNumber)- 2)

		update #Temp_CauseNo set RefcaseNumber = @CaseNumber where TicketID_PK in (Select TicketID_PK from #Temp_CauseNo where RowID = @Counter)

		Set @Counter = @Counter - 1
	End

Alter Table #Temp_CauseNo Drop Column rowid

Select Distinct * from #Temp_CauseNo order by BatchID_pk

Drop Table #Temp_CauseNo
End




 
/* 
Author : Muhammad Adil Aleem
Date Created: 2-July-2007
 
1 03/24/08  dbo.fn_DateFormat(getdate(),1,'/',':',1) 
2 03/24/08 05:21:3 PM dbo.fn_DateFormat(getdate(),2,'/',':',1) 
3 03/24/08 17:21:3 PM dbo.fn_DateFormat(getdate(),3,'/',':',1) 
4 03/24/2008  dbo.fn_DateFormat(getdate(),4,'/',':',1) 
5 03/24/2008 05:21:3 PM dbo.fn_DateFormat(getdate(),5,'/',':',1) 
6 03/24/2008 17:21:3 PM dbo.fn_DateFormat(getdate(),6,'/',':',1) 
7 Mar/24/08  dbo.fn_DateFormat(getdate(),7,'/',':',1) 
8 Mar/24/08 05:21:3 PM dbo.fn_DateFormat(getdate(),8,'/',':',1) 
9 Mar/24/08 17:21:3 PM dbo.fn_DateFormat(getdate(),9,'/',':',1) 
10 Mar/24/2008  dbo.fn_DateFormat(getdate(),10,'/',':',1) 
11 Mar/24/2008 05:21:3 PM dbo.fn_DateFormat(getdate(),11,'/',':',1) 
12 Mar/24/2008 17:21:3 PM dbo.fn_DateFormat(getdate(),12,'/',':',1) 
13 24/03/08  dbo.fn_DateFormat(getdate(),13,'/',':',1) 
14 24/03/08 05:21:3 PM dbo.fn_DateFormat(getdate(),14,'/',':',1) 
15 24/03/08 17:21:3 PM dbo.fn_DateFormat(getdate(),15,'/',':',1) 
16 24/03/2008 05:21:3 PM dbo.fn_DateFormat(getdate(),16,'/',':',1) 
17 24/03/2008 17:21:3 PM dbo.fn_DateFormat(getdate(),17,'/',':',1) 
18 24/Mar/08  dbo.fn_DateFormat(getdate(),18,'/',':',1) 
19 24/Mar/08 05:21:3 PM dbo.fn_DateFormat(getdate(),19,'/',':',1) 
20 24/Mar/08 17:21:3 PM dbo.fn_DateFormat(getdate(),20,'/',':',1) 
21 24/Mar/2008 05:21:3 PM dbo.fn_DateFormat(getdate(),21,'/',':',1) 
22 24/Mar/2008 17:21:3 PM dbo.fn_DateFormat(getdate(),22,'/',':',1) 
23 032408 05213 PM  dbo.fn_DateFormat(getdate(),23,'/',':',1) 
24 03/24/2008 @ 05:21 PM dbo.fn_DateFormat(getdate(),24,'/',':',1) 
25 03/24/08 @ 05:21 PM dbo.fn_DateFormat(getdate(),25,'/',':',1) 
26 05:21 PM  dbo.fn_DateFormat(getdate(),26,'/',':',1) 
27 03/24/2008  dbo.fn_DateFormat(getdate(),27,'/',':',1) 
28 03/24/2008 05:21 PM dbo.fn_DateFormat(getdate(),28,'/',':',1) 
29 03/24/08 05:21 PM dbo.fn_DateFormat(getdate(),29,'/',':',1) 
30 03/24/2008@05:21 PM dbo.fn_DateFormat(getdate(),30,'/',':',1) 
31 03/24/2008 05:21 PM dbo.fn_DateFormat(getdate(),31,'/',':',1) 
32 March/2008 dbo.fn_DateFormat(getdate(),32,'-','',0) -- Adil 8159 08/12/2010
33 3/5/10 or 3/25/10 dbo.fn_DateFormat(getdate(),33,'/','',0) --Muhammad Ali 8236 10/06/2010 Space Save Date format.
*/ 

ALTER Function [dbo].[fn_DateFormat] (@Date as datetime, @StyleID as int, @DateSeparator char, @TimeSeparator char, @ShowMeridiem bit)
Returns varchar(25)
As
Begin
--Author : Muhammad Adil Aleem
--Date Created: 2-July-2007

Declare @ProcessedDate varchar(25), @Day varchar(2), @Month varchar(2), @MonthName varchar(3), @YearShort varchar(4), @YearFull varchar(4), @HourSmall varchar(2), @HourBig varchar(2), @Minute varchar(2), @Second varchar(2), @Meridiem char(2)
--Muhammad Ali 8236 10/06/2010 --> Decleare variable for Date with out zero and month with out zero.
,@DayWithoutZero VARCHAR(2),@monthWithoutZero VARCHAR(2)

Set @Day = convert(varchar,datepart(d,@Date))
Set @Month = convert(varchar,datepart(m,@Date))
Set @MonthName = left(convert(varchar,@Date,0),3)
Set @YearShort = right(convert(varchar,datepart(yy,@Date)),2)
Set @YearFull = convert(varchar,datepart(yy,@Date))
--Muhammad Ali 8236 10/06/2010 --> set variable for Date with out zero and month with out zero.
SET @DayWithoutZero=convert(varchar(2),day(@Date))
SET @monthWithoutZero=convert(varchar(2),month(@Date))
-- End 8236

Set @HourBig = convert(varchar,datepart(hh,@Date))
Set @HourSmall = case 
when datepart(hh,@Date) > 12 then convert(varchar, (datepart(hh,@Date) - 12)) 
when datepart(hh,@Date) = 0 then '12' -- Waqas 5864 07/07/2009 Email Case Summary
else convert(varchar,datepart(hh,@Date)) end
Set @Minute = convert(varchar,datepart(mi,@Date)) 
-- -- Fahad 3089 2-13-08 
-- ADDED BY FAHAD
If len(@Minute) = 1      
Begin     
 Set @Minute = '0' + @Minute     
End   
 
If len(@Month) = 1      
Begin     
 Set @Month = '0' + @Month     
End  
 
If len(@Day) = 1      
Begin     
 Set @Day = '0' + @Day     
End     
     
If len(@HourSmall) = 1      
Begin     
 Set @HourSmall = '0' + @HourSmall     
End  
------ END FAHAD     
     
Set @Second = convert(varchar,datepart(ss,@Date))

if @ShowMeridiem = 1
 Begin
 Set @Meridiem = right(convert(varchar,@Date),2)
 End
Else
 Begin
 Set @Meridiem = ''
 End

if @StyleID = 1
 Set @ProcessedDate = @Month + @DateSeparator + @Day + @DateSeparator + @YearShort
Else if @StyleID = 2
 Set @ProcessedDate = @Month + @DateSeparator + @Day + @DateSeparator + @YearShort + ' ' + @HourSmall + @TimeSeparator + @Minute + @TimeSeparator + @Second + ' ' + @Meridiem
Else if @StyleID = 3
 Set @ProcessedDate = @Month + @DateSeparator + @Day + @DateSeparator + @YearShort + ' ' + @HourBig + @TimeSeparator + @Minute + @TimeSeparator + @Second + ' ' + @Meridiem
Else if @StyleID = 4
 Set @ProcessedDate = @Month + @DateSeparator + @Day + @DateSeparator + @YearFull
Else if @StyleID = 5
 Set @ProcessedDate = @Month + @DateSeparator + @Day + @DateSeparator + @YearFull + ' ' + @HourSmall + @TimeSeparator + @Minute + @TimeSeparator + @Second + ' ' + @Meridiem
Else if @StyleID = 6
 Set @ProcessedDate = @Month + @DateSeparator + @Day + @DateSeparator + @YearFull + ' ' + @HourBig + @TimeSeparator + @Minute + @TimeSeparator + @Second + ' ' + @Meridiem
Else if @StyleID = 7
 Set @ProcessedDate = @MonthName + @DateSeparator + @Day + @DateSeparator + @YearShort
Else if @StyleID = 8
 Set @ProcessedDate = @MonthName + @DateSeparator + @Day + @DateSeparator + @YearShort + ' ' + @HourSmall + @TimeSeparator + @Minute + @TimeSeparator + @Second + ' ' + @Meridiem
Else if @StyleID = 9
 Set @ProcessedDate = @MonthName + @DateSeparator + @Day + @DateSeparator + @YearShort + ' ' + @HourBig + @TimeSeparator + @Minute + @TimeSeparator + @Second + ' ' + @Meridiem
Else if @StyleID = 10
 Set @ProcessedDate = @MonthName + @DateSeparator + @Day + @DateSeparator + @YearFull
Else if @StyleID = 11
 Set @ProcessedDate = @MonthName + @DateSeparator + @Day + @DateSeparator + @YearFull + ' ' + @HourSmall + @TimeSeparator + @Minute + @TimeSeparator + @Second + ' ' + @Meridiem
Else if @StyleID = 12
 Set @ProcessedDate = @MonthName + @DateSeparator + @Day + @DateSeparator + @YearFull + ' ' + @HourBig + @TimeSeparator + @Minute + @TimeSeparator + @Second + ' ' + @Meridiem
Else if @StyleID = 13
 Set @ProcessedDate = @Day + @DateSeparator + @Month + @DateSeparator + @YearShort
Else if @StyleID = 14
 Set @ProcessedDate = @Day + @DateSeparator + @Month + @DateSeparator + @YearShort + ' ' + @HourSmall + @TimeSeparator + @Minute + @TimeSeparator + @Second + ' ' + @Meridiem
Else if @StyleID = 15
 Set @ProcessedDate = @Day + @DateSeparator + @Month + @DateSeparator + @YearShort + ' ' + @HourBig + @TimeSeparator + @Minute + @TimeSeparator + @Second + ' ' + @Meridiem
Else if @StyleID = 16
 Set @ProcessedDate = @Day + @DateSeparator + @Month + @DateSeparator + @YearFull + ' ' + @HourSmall + @TimeSeparator + @Minute + @TimeSeparator + @Second + ' ' + @Meridiem
Else if @StyleID = 17
 Set @ProcessedDate = @Day + @DateSeparator + @Month + @DateSeparator + @YearFull + ' ' + @HourBig + @TimeSeparator + @Minute + @TimeSeparator + @Second + ' ' + @Meridiem
Else if @StyleID = 18
 Set @ProcessedDate = @Day + @DateSeparator + @MonthName + @DateSeparator + @YearShort
Else if @StyleID = 19
 Set @ProcessedDate = @Day + @DateSeparator + @MonthName + @DateSeparator + @YearShort + ' ' + @HourSmall + @TimeSeparator + @Minute + @TimeSeparator + @Second + ' ' + @Meridiem
Else if @StyleID = 20
 Set @ProcessedDate = @Day + @DateSeparator + @MonthName + @DateSeparator + @YearShort + ' ' + @HourBig + @TimeSeparator + @Minute + @TimeSeparator + @Second + ' ' + @Meridiem
Else if @StyleID = 21
 Set @ProcessedDate = @Day + @DateSeparator + @MonthName + @DateSeparator + @YearFull + ' ' + @HourSmall + @TimeSeparator + @Minute + @TimeSeparator + @Second + ' ' + @Meridiem
Else if @StyleID = 22
 Set @ProcessedDate = @Day + @DateSeparator + @MonthName + @DateSeparator + @YearFull + ' ' + @HourBig + @TimeSeparator + @Minute + @TimeSeparator + @Second + ' ' + @Meridiem
Else if @StyleID = 23
 Set @ProcessedDate = @Month + @Day + @YearShort + ' ' + @HourSmall + @Minute + @Second + ' ' + @Meridiem
--Ozair 7149 Date Format fixed space given after @
Else if @StyleID = 24
 Set @ProcessedDate = @Month + @DateSeparator + @Day + @DateSeparator + @YearFull + ' ' + '@' + ' ' + @HourSmall + @TimeSeparator + @Minute + ' ' + @Meridiem
--Ozair 7149 Date Format fixed double space after @    
Else if @StyleID = 25
 Set @ProcessedDate = @Month + @DateSeparator + @Day + @DateSeparator + @YearShort + ' ' + '@' + ' ' + @HourSmall + @TimeSeparator + @Minute + ' ' + @Meridiem
     
    
Else if @StyleID = 26
 Set @ProcessedDate = @HourSmall + @TimeSeparator + @Minute + ' ' + @Meridiem     
Else if @StyleID = 27
 Set @ProcessedDate = @Month + @DateSeparator + @Day + @DateSeparator + @YearFull    
    
Else if @StyleID = 28      
 Set @ProcessedDate = @Month + @DateSeparator + @Day + @DateSeparator + @YearFull + '' + ' ' + '' + @HourSmall + @TimeSeparator + @Minute + ' ' + @Meridiem  
----- Fahad 3089 2-13-08 
----- StyleID added By fahad   
Else if @StyleID = 29
 Set @ProcessedDate = @Month + @DateSeparator + @Day + @DateSeparator + @YearShort + ' ' + @HourSmall + @TimeSeparator + @Minute + ' ' + @Meridiem     
     
 
Else if @StyleID =30 
begin 
if(len(@day)=1) 
set @Day='0'+@Day 
 
if(len(@Month)=1) 
set @Month='0'+@Month 
 
 Set @ProcessedDate = @Month + @DateSeparator + @Day + @DateSeparator + @YearFull + '' + '@' + '' + @HourSmall + @TimeSeparator + @Minute + ' ' + @Meridiem   
end 
 
 
Else if @StyleID = 31 
begin 
if(len(@day)=1) 
set @Day='0'+@Day 
 
if(len(@Month)=1) 
set @Month='0'+@Month 
 
 Set @ProcessedDate = @Month + @DateSeparator + @Day + @DateSeparator + @YearFull + ' '+ @HourSmall + @TimeSeparator + @Minute + ' ' + @Meridiem  
end 

Else if @StyleID = 32 -- Adil 8159 08/12/2010 Style ID added.
 Set @ProcessedDate = @MonthName + @DateSeparator + @YearFull
 --Muhammad Ali 8236 10/06/2010 return date with out zero in days and month.
 ELSE IF @StyleID =33
SET @ProcessedDate=@monthWithoutZero+@DateSeparator+@DayWithoutZero+@DateSeparator+@YearShort
--END 8236

      
Return @ProcessedDate

End
    
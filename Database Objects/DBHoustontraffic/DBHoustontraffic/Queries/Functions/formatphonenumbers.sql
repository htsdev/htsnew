/****** Object:  UserDefinedFunction [dbo].[formatphonenumbers]    Script Date: 01/25/2008 20:29:19 ******/
DROP FUNCTION [dbo].[formatphonenumbers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  function [dbo].[formatphonenumbers] (@phonenumber varchar(20))
returns varchar(20)
as
begin
	declare @formattedphonenumber varchar(20)

if isnumeric(@phonenumber) = 1
	begin
		if len(@phonenumber) > 10
			set @formattedphonenumber = substring(@phonenumber,1,3) + '-' + substring(@phonenumber,4,3) + '-' + substring(@phonenumber,7,4) + ' x ' + substring(@phonenumber,11,4) 
		else
			set @formattedphonenumber = substring(@phonenumber,1,3) + '-' + substring(@phonenumber,4,3) + '-' + substring(@phonenumber,7,4) 
	end
else if isnumeric(left(@phonenumber,10)) = 1 
	begin
		if len(@phonenumber) > 10
			set @formattedphonenumber = substring(@phonenumber,1,3) + '-' + substring(@phonenumber,4,3) + '-' + substring(@phonenumber,7,4) + ' ' + upper(substring(@phonenumber,11,4)) 
		else
			set @formattedphonenumber = substring(@phonenumber,1,3) + '-' + substring(@phonenumber,4,3) + '-' + substring(@phonenumber,7,4) 
	end
else
	set @formattedphonenumber = @phonenumber

	return @formattedphonenumber
end
GO

/****** Object:  UserDefinedFunction [dbo].[FxJoinCaseNo]    Script Date: 01/25/2008 20:29:22 ******/
DROP FUNCTION [dbo].[FxJoinCaseNo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE function [dbo].[FxJoinCaseNo] (@ticketID  int)
returns varchar(4000) 
as 
begin

Declare 
@val  varchar(4000)

SET @val = '' 

SELECT     @val = @val+ b.RefCaseNumber + ', ' 
FROM          dbo.tblTicketsViolations AS b
WHERE      (b.TicketID_PK =@ticketID ) AND (NOT (b.RefCaseNumber LIKE 'ID%'))

--return substring(@val,0,len(@val)-1)
--select @val= substring(@val,0,len(@val)-1)
IF LEN(@val) > 0 
BEGIN
	Select @val= LEFT ( @val, LEN(@val)-1 )
END


return @val 

end
GO

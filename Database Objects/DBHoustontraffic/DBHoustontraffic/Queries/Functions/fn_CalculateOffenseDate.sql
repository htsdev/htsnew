USE [TrafficTickets]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CalculateOffenseDate]    Script Date: 05/23/2011 16:07:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** 
Created by:		Abbas Shahid
Business Logic:	This function is used to get the total difference of year in violation date (in past) and the current date.
*******/
CREATE FUNCTION [dbo].[fn_CalculateOffenseDate]
(
  @OffenseDate datetime,
  @CurrentDate datetime
)
RETURNS int

AS

BEGIN

IF @OffenseDate > @CurrentDate
BEGIN
  DECLARE @TempDate datetime
  SELECT
    @TempDate = @OffenseDate,
    @OffenseDate = @CurrentDate,
    @CurrentDate = @TempDate
END

DECLARE @Year int
SELECT @Year = DATEDIFF(YY, @OffenseDate, @CurrentDate) - 
  CASE WHEN(
    (MONTH(@OffenseDate)*100 + DAY(@OffenseDate)) >
    (MONTH(@CurrentDate)*100 + DAY(@CurrentDate))
  ) THEN 1 ELSE 0 END
RETURN @Year

END

GO

GRANT EXECUTE ON [dbo].[fn_CalculateOffenseDate] TO dbr_webuser
GO
/*
* Created By: Sabir Khan
* Task:		  9711
* Date:		  11/17/2011
* 
* Business Logic: This function is used to return all promotional zip code based promotional scheme.
* 
* Input Parameters: 
* @SchemeID -- ID of Promotional Scheme
* @SchemeMailerID -- Scheme Mailer ID
* @letterType	--	Mailer ID
* @courtCategory -- Court Category
*/
CREATE function [dbo].[fn_hts_get_AllPromoZipCodes]   
 (  
  @SchemeID int,  
   @SchemeMailerID int,
    @letterType int,
     @courtCategory int
 )            

returns varchar(MAX)            
  
as            
  
begin 

declare @string as varchar(MAX)          
set @string = ''

       		SELECT @string = @string + pzz.ZipCode + ',' 
       		FROM dbo.PromotionalSchemes ps
			INNER JOIN dbo.PromotionalSchemes_MailerType pm
				ON  ps.SchemeId = pm.SchemeId
			INNER JOIN dbo.PromotionalSchemes_ZipCodes pzz
				ON  pm.SchemeMailerId = pzz.SchemeMailerId
			INNER JOIN dbo.tblLetter l
				ON  pm.MailerType = l.LetterID_PK
			INNER JOIN dbo.tblCourtCategories cc
				ON  l.courtcategory = cc.CourtCategorynum
WHERE  LEN(ISNULL(pzz.ZipCode, '')) > 0
AND ps.SchemeId = @SchemeID
AND pm.SchemeMailerId = @SchemeMailerID
AND l.LetterID_PK = @letterType
AND cc.CourtCategorynum = @courtCategory
--AND ISNULL(ps.IsActiveScheme,0) = 1


 IF(LEN(@string) > 1)  
    SET @string = LEFT(@string, LEN(@string) -1) 
  IF(LEN(@string)>=70)
  BEGIN    	
	SELECT @string  = STUFF(@string, 70, 6, CHAR(13))
  END
   IF(LEN(@string)>=140)
  BEGIN 
	SELECT @string  = STUFF(@string, 140, 6, CHAR(13))  
  END
   IF(LEN(@string)>=210)
  BEGIN 
		SELECT @string  = STUFF(@string, 210, 6, CHAR(13))
	END
	IF(LEN(@string)>=280)
  BEGIN 
		SELECT @string  = STUFF(@string, 280, 6, CHAR(13))  
  END


    
return @string

END


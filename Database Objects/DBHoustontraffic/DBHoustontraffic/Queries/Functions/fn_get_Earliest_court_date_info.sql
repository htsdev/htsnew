/****** Object:  UserDefinedFunction [dbo].[fn_get_Earliest_court_date_info]    Script Date: 01/25/2008 20:28:52 ******/
DROP FUNCTION [dbo].[fn_get_Earliest_court_date_info]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  function [dbo].[fn_get_Earliest_court_date_info] 
(@ticketnumber varchar(20))


returns varchar(100)
as
begin
set @ticketnumber = left(@ticketnumber,9)
declare @courtinfo varchar(100)
select distinct @courtinfo = isnull(courtdate,'N/A') + ';' + isnull(time,'N/A')+ ';' +isnull(status,'N/A')+ ';' +isnull(courtnum,'N/A') from tbllubbock where courtdate in (
select min(courtdate) from tbllubbock where left(ticketnumber,9) = @ticketnumber
--@tempticketnumber
group by left(ticketnumber,9) )
and left(ticketnumber,9) = @ticketnumber
return @courtinfo
end
GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetReportValue]    Script Date: 01/25/2008 20:29:01 ******/
DROP FUNCTION [dbo].[fn_GetReportValue]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_GetReportValue]  
(  
 -- Add the parameters for the function here  
 @Attribute_Key varchar(100),   
 @Report_ID int  
)  
RETURNS varchar(100)  
AS  
BEGIN  
 declare @Attribute_Value varchar(100)  
   
 SELECT @Attribute_Value  = [Attribute_Value] FROM [dbo].[Tbl_BusinessLogicDay]    
 WHERE [Attribute_Key] = @Attribute_Key     
 AND [Report_Id] = @Report_Id     
 AND [IsInActive] = 1    
  
 -- Return the result of the function  
 RETURN @Attribute_Value  
  
END
GO

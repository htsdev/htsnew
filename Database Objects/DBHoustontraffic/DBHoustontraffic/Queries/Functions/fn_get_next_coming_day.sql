﻿/*
* Business Logic : This funciton is used to get coming week day that user need
* Parameters : 
* @date : This is any date in the week
* @day : This is the day that user need next of the provided date. User will be provide the day in number as given below.
*	Sunday    = 1
*	Monday    = 2
*	Tuesday   = 3
*	Wednesday = 4
*	Thursday  = 5
*	Friday    = 6
*	Saturday  = 7
*/


Create function dbo.fn_get_next_coming_day(@date as datetime, @day as int)
returns datetime

as


Begin
declare @count int  
declare @finaldate datetime
set @count = convert(int, datepart(dw, @date))  


if(@count >= @day)
begin
set @finaldate =  DATEADD(wk, DATEDIFF(wk, 5, @date), 5) + @day 
end
else
begin
set @finaldate =  DATEADD(wk, DATEDIFF(wk, 6, @date), 6) + @day -1
end


return @finaldate
End

GO
grant execute on dbo.fn_get_next_coming_day to dbr_webuser
GO
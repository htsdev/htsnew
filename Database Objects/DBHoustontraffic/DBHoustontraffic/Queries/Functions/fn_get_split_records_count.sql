/****** Object:  UserDefinedFunction [dbo].[fn_get_split_records_count]    Script Date: 01/25/2008 20:28:54 ******/
-- =============================================  
-- Author:  Zeeshan Ahmed  
-- Create date: 21 st August 2007 ( Hurricane Rubina )   
-- Description: This Procerdure Check That Case Has Split Records  
-- =============================================  
ALTER FUNCTION [dbo].[fn_get_split_records_count]  
(  
 @ticketid int   
)  
RETURNS  int  
AS  
BEGIN  
  
declare @SplitCount as int  
  
  
SELECT @SplitCount = Count(TT.TicketID_PK)              
   FROM    dbo.tblDateType dt               
  INNER JOIN              
  dbo.tblCourtViolationStatus cvs               
  ON  dt.TypeID = cvs.CategoryID               
  INNER JOIN              
  dbo.tblTickets TT               
  INNER JOIN              
  dbo.tblTicketsViolations TTV               
  ON  TT.TicketID_PK = TTV.TicketID_PK               
  ON  cvs.CourtViolationStatusID = TTV.CourtViolationStatusIDmain              
  INNER JOIN dbo.tblcourts TC              
  ON  TTV.courtid=TC.courtid              
              
 WHERE   dt.typeid in (2,3,4,5,12)        --status of arrignment,jury,pretrial,judge,bond      
  AND    TTV.RefCaseNumber <>'0'              
  AND  TT.Activeflag<>0          --only clients       
  AND TTV.TicketID_PK IN (                --in order to look for tickets with same casenumber but having different Verified Statuses       
   SELECT  v.ticketid_pk              
   FROM dbo.tblTicketsViolations v               
   INNER JOIN           
                       dbo.tblCourtViolationStatus cvs2               
   ON  v.CourtViolationStatusIDmain = cvs2.CourtViolationStatusID               
   INNER JOIN              
                       dbo.tblDateType dt2               
   ON  cvs2.CategoryID = dt2.TypeID              
   WHERE   v.ticketid_pk = ttv.ticketid_pk               
   AND    V.RefCaseNumber <>'0'              
   and dt2.typeid in (2,3,4,5,12)             
  and  v.ticketid_pk = @ticketid  
  and                 
  (               
  (datediff(day,TTV.courtdatemain,V.courtdatemain) <> 0)   --Compares one ticket Verified Court Date with the another ticket                 
   or                
  (datediff(hour,TTV.courtdatemain,V.courtdatemain) <> 0)   -- Compares one ticket Verified Court time with the another ticket                 
  or              
  (datediff(minute,TTV.courtdatemain,V.courtdatemain) <> 0)                 
  or   --Sabir Khan 7979 07/14/2010 Court Number type has been changed..           
  (convert(varchar,TTV.courtnumbermain) <> convert(varchar,V.courtnumbermain)) -- Compares one ticket Verified Court num with the another ticket                        
  or                 
  (dt.typeid <> dt2.typeid)                             --Compare Statuses      
  )                
 )              
  --order by tt.ticketid_pk       
  
  
  
  
if ( @SplitCount = 0 )   
return 0  
  
else  
return 1   
  
  
return 1  
  
  
END
GO

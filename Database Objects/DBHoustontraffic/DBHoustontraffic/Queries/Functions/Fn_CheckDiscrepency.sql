/****** Object:  UserDefinedFunction [dbo].[Fn_CheckDiscrepency]    Script Date: 01/17/2008 06:04:12 ******/
DROP FUNCTION [dbo].[Fn_CheckDiscrepency]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[Fn_CheckDiscrepency](@TicketsViolationID as int)      
returns bit      
as      
begin      
      
      
declare @autoCourtDate datetime      
declare @autoCourtViolationstatusid int       
declare @autocourtnumber VARCHAR(3) -- Sabir Khan 7979 07/02/2010 Type has been changed into varchar
declare @AutoTypeId int      
      
declare @VerifiedCourtDate datetime      
declare @VerifiedCourtViolationstatusid int      
declare @Verifiedcourtnumber VARCHAR(3) -- Sabir Khan 7979 07/02/2010 Type has been changed into varchar       
declare @VerifiedTypeId int      
      
      
declare @ScanCourtDate datetime      
declare @ScanCourtViolationstatusid int      
declare @Scancourtnumber VARCHAR(3) -- Sabir Khan 7979 07/02/2010 Type has been changed into varchar     
declare @ScanTypeId int        
      
      
      
Select @autoCourtDate = CourtDate ,     
 @autoCourtViolationstatusid = CourtViolationStatusid,     
 @autocourtnumber = CourtNumber,      
 @VerifiedCourtDate = CourtDateMain ,     
 @VerifiedCourtViolationstatusid = CourtViolationStatusidMain,     
 @Verifiedcourtnumber = CourtNumberMain,       
        @ScanCourtDate = CourtDateScan ,     
 @ScanCourtViolationstatusid = CourtViolationStatusidscan,     
 @Scancourtnumber = CourtNumberscan      
from tblticketsviolations       
where ticketsviolationid = @TicketsViolationID      
    
    
    
-----------------------------------------------------------------------------------------------------------------------    
-- ADDED BY TAHIR DT:4/11/07    
-- DO NOT DISPLAY DISCREPANCY IF CATEGORY ID IS SAME....    
-----------------------------------------------------------------------------------------------------------------------    
select @AutoTypeId = categoryid from tblcourtviolationstatus where courtviolationstatusid =   @autoCourtViolationstatusid    
select @VerifiedTypeId = categoryid from tblcourtviolationstatus where courtviolationstatusid =   @VerifiedCourtViolationstatusid    
select @ScanTypeId = categoryid from tblcourtviolationstatus where courtviolationstatusid =   @ScanCourtViolationstatusid    
    
select  @AutoTypeId = isnull(@AutoTypeId,0),     
 @VerifiedTypeId = isnull(@VerifiedTypeId,0),    
 @ScanTypeId = isnull(@ScanTypeId,0)    
    
-- DO NOT CONSIDER DISCREPANCY     
-- IF ALL THE THREE STATUSES ARE DISPOSED    
if (@AutoTypeid = 50 and @VerifiedTypeId = 50 and @ScanTypeId = 50)    
 return 0    
    
-- DO NOT CONSIDER DISCREPANCY     
-- IF AUTO STATUS = 'ARRAIGNMENT' AND VERIFIED STATUS = 'ARRAIGNMENT WAITING'    
else if ( @AutoTypeId = 2 ) and  @VerifiedCourtViolationstatusid = 201       
 return 0       
    
-- DO NOT CONSIDER DISCREPANCY     
-- IF AUTO STATUS = 'FTA' OR 'DLQ' AND VERIFIED STATUS = 'BOND'      
else if ( @autoCourtViolationstatusid = 146 or @autoCourtViolationstatusid = 43 or @autoCourtViolationstatusid = 186 or @autoCourtViolationstatusid = 175 ) and  @VerifiedCourtViolationstatusid = 135       
 return 0       
    
-- DO NOT CONSIDER DISCREPANCY     
-- IF AUTO STATUS =  'FTA', 'BOND' OR 'DLQ' AND VERIFIED STATUS = 'BOND WAITING'      
else if ( @autoCourtViolationstatusid = 186 or @autoCourtViolationstatusid = 135 or @autoCourtViolationstatusid = 43 or @autoCourtViolationstatusid = 146  or @autoCourtViolationstatusid = 175 ) and  @VerifiedCourtViolationstatusid = 202       
 return 0     
    
  
-------------------------------------------------------------------------  
-- ADDED BY ZEESHAN 18/4/2007  
-------------------------------------------------------------------------  
-- DO NOT CONSIDER DISCREPANCY    
-- IF AUTO STATUS = 'FTA' AND VERIFIED STATUS = 'MIS'    
else if @autoCourtViolationstatusid = 186 and   @VerifiedCourtViolationstatusid = 105  
return 0  
  
-- DO NOT CONSIDER DISCREPANCY    
-- IF AUTO STATUS = 'DLQ'  AND VERIFIED STATUS = 'MIS'    
else if ( @autoCourtViolationstatusid = 146 or @autoCourtViolationstatusid = 175 )  and   @VerifiedCourtViolationstatusid = 105  
return 0  

-------------------------------------------------------------------------  
-- ADDED BY ZEESHAN 11/6/2007  
-------------------------------------------------------------------------  
-- DO NOT CONSIDER DISCREPANCY    
-- IF AUTO STATUS = 'WAIT' AND VERIFIED STATUS = 'WAIT'    

else if ( @autoCourtViolationstatusid = 104  and   @VerifiedCourtViolationstatusid = 104  )
return 0  

-- DO NOT CONSIDER DISCREPANCY 
-- If auto status = NON and verified status = DISPOSED
else if ( @autoCourtViolationstatusid = 15 or @autoCourtViolationstatusid = 153) and @VerifiedCourtViolationstatusid = 80
return 0

-- DO NOT CONSIDER DISCREPANCY 
-- if  auto status = DEF and verified status = DISPOSED
else if (  @autoCourtViolationstatusid = 152 or @autoCourtViolationstatusid = 162 or @autoCourtViolationstatusid = 204) and @VerifiedCourtViolationstatusid = 80 
return 0

--------------------------------------------------------------------------


-- CONSIDER DISCREPANCY     
-- IF VERIFIED & AUTO COURT NUMBERS & COURT DATES ARE NOT SAME    
else if (@autoCourtDate != @VerifiedCourtDate or @autocourtnumber !=  @Verifiedcourtnumber) and  @VerifiedCourtViolationstatusid != 105      
 return 1      
  
-- DO NOT CONSIDER DISCREPANCY     
-- IF AUTO STATUS = VERIFIED STATUS    
else if @AutoTypeid = @VerifiedTypeId    
 return 0    
  
-- ELSE CONSIDER AS DISCREPANCY     
else      
 return 1      
      
      
return 0      
    
end
GO

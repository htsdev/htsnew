/****** Object:  UserDefinedFunction [dbo].[fn_hts_get_TicketViolationDescription]    Script Date: 01/17/2008 06:04:20 ******/
DROP FUNCTION [dbo].[fn_hts_get_TicketViolationDescription]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_hts_get_TicketViolationDescription]
(
	@ticketViolationId int
)
RETURNS varchar(200)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ResultVar varchar(200)

	select @ResultVar = isnull(tbldatetype.Description,'N/A') from tblticketsviolations 
	left outer join tblcourtviolationstatus on tblticketsviolations.courtviolationstatusidmain = tblcourtviolationstatus.CourtViolationStatusId
	left outer join tbldatetype on tblcourtviolationstatus.CategoryId = tbldatetype.TypeID where tblticketsviolations.TicketsViolationID = @ticketViolationId

	
	-- Return the result of the function
	RETURN @ResultVar

END
GO

/****** Object:  UserDefinedFunction [dbo].[fn_HMC_Get_StringDate]    Script Date: 01/25/2008 20:29:04 ******/
DROP FUNCTION [dbo].[fn_HMC_Get_StringDate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_HMC_Get_StringDate]  
(  
@dt datetime  
)  
RETURNS varchar(10)
AS  
BEGIN  



	declare @dt_day as int  
	declare @dt_year as int  
	declare @dt_month as int  
	declare @dt_str as varchar(12)  
	declare @dt_str_mm as varchar(3)  
	declare @dt_str_dd as varchar(3)  
	declare @dt_str_yy as varchar(3)  
if convert(varchar(12), @dt, 101) <> '01/01/1900'
	begin 
		set @dt_day = day(@dt)  
		set @dt_month = month(@dt)  
		set @dt_year = year(@dt)  
		  
		set @dt_str_mm = convert(varchar(2),@dt_month) + '/'  
		if len(@dt_str_mm) = 2  
		begin  
		 set @dt_str_mm = '0' + @dt_str_mm  
		end  
		  
		set @dt_str_dd = convert(varchar(2),@dt_day) + '/'  
		if len(@dt_str_dd) = 2  
		begin  
		set @dt_str_dd = '0' + @dt_str_dd  
		end  
		  
		set @dt_str_yy = substring(convert(varchar(4),@dt_year),3,2)  
		set @dt_str = @dt_str_mm +@dt_str_dd +@dt_str_yy  
		  
		 
	end 
else
	begin 
		set @dt_str = ''
	--return blank in case
		
	End 
  RETURN @dt_str 
END
GO

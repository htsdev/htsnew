/****** Object:  UserDefinedFunction [dbo].[Fn_WriteStatusChangeNotesString]    Script Date: 01/25/2008 20:29:11 ******/
DROP FUNCTION [dbo].[Fn_WriteStatusChangeNotesString]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE    function [dbo].[Fn_WriteStatusChangeNotesString](@Olddate as varchar(20),@ticketnumber as varchar(20),@Datetype as varchar(3),@violationstatus as varchar(3),@newdate as varchar(20))
returns varchar(30)
as
begin
	declare @ReturnString varchar(100)

	set @ReturnString = @Olddate + ' ' + @ticketnumber + ' ' + @Datetype +  ' ' + @violationstatus  + ' ' + @newdate 
	return @ReturnString
end
GO

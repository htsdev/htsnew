/****** Object:  UserDefinedFunction [dbo].[fn_HTS_Get_MissedCourtCaseStatus]    Script Date: 01/25/2008 20:29:07 ******/
DROP FUNCTION [dbo].[fn_HTS_Get_MissedCourtCaseStatus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_HTS_Get_MissedCourtCaseStatus] (@TicketID_PK as int, @CourtDate as datetime)  
RETURNS Varchar(1000) AS  
BEGIN 
Declare @CaseStatus as varchar(5000)
Declare @Contact1 as varchar(20)
Declare @Contact2 as varchar(20)
Declare @Contact3 as varchar(20)

Set @CaseStatus = ''

SELECT  distinct @CaseStatus = @CaseStatus +  CONVERT(varchar(20), @CourtDate) 
	+ ' #' + dbo.tblTicketsViolations.CourtNumber + ' ' + dbo.tblCourtViolationStatus.Description + '<br>' 
FROM         dbo.tblCourtViolationStatus RIGHT OUTER JOIN
	        dbo.tblTicketsViolations ON dbo.tblCourtViolationStatus.CourtViolationStatusID = dbo.tblTicketsViolations.CourtViolationStatusID
WHERE	 dbo.tblTicketsViolations.TicketID_PK = @TicketID_PK

Set @CaseStatus = Left(@CaseStatus,Len(@CaseStatus)-4)

Return @CaseStatus

END
GO

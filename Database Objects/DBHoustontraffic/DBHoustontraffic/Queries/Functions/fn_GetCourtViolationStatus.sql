/****** Object:  UserDefinedFunction [dbo].[fn_GetCourtViolationStatus]    Script Date: 01/25/2008 20:28:57 ******/
DROP FUNCTION [dbo].[fn_GetCourtViolationStatus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--
/*
select * from tbldatetype
select courtviolationstatusid from tbldatetype D, tblcourtviolationstatus S
where rtrim(replace(D.description,'2nd',' ')) = S.description
and typeid = 4
*/

create    Function [dbo].[fn_GetCourtViolationStatus]
(@datetype as int)
returns int
as
begin
declare @courtviolationstatusid int

select @courtviolationstatusid = courtviolationstatusid  
from tbldatetype D, tblcourtviolationstatus S
where rtrim(replace(D.description,'2nd',' ')) = S.description
and typeid = @datetype
return  @courtviolationstatusid
	
end
GO

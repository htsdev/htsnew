/****** Object:  UserDefinedFunction [dbo].[CalculateBondingAmount]    Script Date: 01/25/2008 20:28:32 ******/
DROP FUNCTION [dbo].[CalculateBondingAmount]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE      Function [dbo].[CalculateBondingAmount](@violationcode varchar(10),@RefCaseNumber varchar(20),@FineAmount money,@bonddatabaseamount money)
Returns money
AS
BEGIN
--initialize
Declare @bondamount money
declare @tempbondamount varchar(5)
Select @bondamount = 0
set @tempbondamount  = left(@violationcode,5)
	IF(@tempbondamount='43030' OR @tempbondamount='43031') 
		
		BEGIN
			IF(@tempbondamount='43030')
			BEGIN
				IF(@FineAmount <= 165)
					BEGIN	
						Set @bondamount = 225
					END
				ELSE
					BEGIN
						Set @bondamount = 225 + (@FineAmount - 165)
							
						
					END
			END

			IF(@tempbondamount='43031')
			BEGIN
				IF(@FineAmount <= 215)
					BEGIN	
						Set @bondamount = 275
					END
				ELSE
					BEGIN
						Set @bondamount = 275 + (@FineAmount - 215)
							
						
					END
			END
		END
	else
		BEGIN
			Set @bondamount = @bonddatabaseamount
		END
	RETURN  @bondamount
END
GO

/****** Object:  UserDefinedFunction [dbo].[GetBondAmount]    Script Date: 01/25/2008 20:29:26 ******/
DROP FUNCTION [dbo].[GetBondAmount]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE       Function [dbo].[GetBondAmount](@dailyupdatebondamount money,@bonddatabaseamount money,@fticketcount int)
Returns money
AS
BEGIN
declare @bondamount money

if @fticketcount > 0
	set @bondamount = @bonddatabaseamount
else
	set @bondamount = @dailyupdatebondamount

return @bondamount

	
END
GO

/****** Object:  UserDefinedFunction [dbo].[fn_convert_string_to_date]    Script Date: 01/25/2008 20:28:40 ******/
DROP FUNCTION [dbo].[fn_convert_string_to_date]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_convert_string_to_date](@CourtDate varchar(20),@courttime varchar(4))
returns varchar(30)
as
begin
declare
--@CourtDate varchar(20),
@strhour int,
@strmin int,
--@courttime varchar(4),
@strtimestring varchar(10),
@tempcourtdate varchar(20)

--set @courttime = '1030'
--set @CourtDate = '04/12/2004'


set @strhour = convert(int,left(@courttime,2))
set @strmin = convert(int,right(@courttime,2))

if @strhour >= 12 
begin
	if @strhour = 12
	begin
		set @strtimestring = convert(varchar(2),@strhour) + ':' +   convert(varchar(2),@strmin) + ' ' + 'PM'
	end 
	else
	begin
		set @strtimestring = convert(varchar(2),@strhour-12) + ':' +   convert(varchar(2),@strmin) + ' ' + 'PM'
	end
end
else
begin
	set @strtimestring = convert(varchar(2),@strhour) + ':' +   convert(varchar(2),@strmin) + ' ' + 'AM'
end
set @tempcourtdate = convert(datetime,isnull(@CourtDate,'01/01/1900') + ' ' + isnull(@strtimestring,'00:00:00'))
return @tempcourtdate

end
GO

﻿/******
* Task ID: 5864
* This funtion is use to get comma seperated ticket number against ticket ID and violation ID
******/

Alter FUNCTION dbo.Fn_HTP_Get_CommaSeparatedCauseNumbersByTicketIDAndViolationID
(
	@TicketID     INT,
	@ViolationID  INT
)
RETURNs VARCHAR(MAX)
AS
BEGIN
    DECLARE @QUERY VARCHAR(200)
    
    SET @QUERY = ''
    
    SELECT  @QUERY = @QUERY +  ttv.casenumassignedbycourt + ','
    FROM   tblTicketsViolations ttv
    WHERE  ttv.TicketID_PK = @TicketID
           AND ttv.ViolationNumber_PK = @ViolationID
           AND ttv.casenumassignedbycourt <> ''
    
    IF(LEN(@QUERY) > 1)
    SET @QUERY = LEFT(@QUERY, LEN(@QUERY) -1)
    
    RETURN @QUERY
END

 
 GO 
 GRANT EXECUTE ON dbo.Fn_HTP_Get_CommaSeparatedCauseNumbersByTicketIDAndViolationID TO dbr_webuser
 GO
 
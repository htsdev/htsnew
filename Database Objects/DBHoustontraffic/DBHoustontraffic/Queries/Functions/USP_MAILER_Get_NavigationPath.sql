/****** 
Altered by:		Sabir Khan
Business Logic:	This procedure is used by LMS to get the titles in navigation control.

List of parameters: 
	@option:	Specificies the main menu i.e. Mailer or Reports
	@court:		court category number for the selected report
	@letter:	letter id for the selected mailer or report	
		
*******/

-- USP_MAILER_Get_NavigationPath 0,6, 17
ALTER procedure [dbo].[USP_MAILER_Get_NavigationPath]
	(
	@option int,
	@court int,
	@letter int
	)


as

declare @sOption varchar(50),
	@scourt varchar(100),
	@sLetter varchar(100)

if @option  = 0 
begin
	select @sOption = 'Mailer'
	select @scourt  = courtcategoryname from tblcourtcategories where courtcategorynum = @court
end
else if @option = 1
begin
	select @soption = 'Reports'
	select @scourt  = courtcategoryname from tblmailerreports where courtcategorynum = @court
	-- tahir 7218 01/20/2010 
	if len(isnull(@scourt,''))=0
		select @scourt  = courtcategoryname from tblcourtcategories where courtcategorynum = @court
end

select @sletter = lettername from tblletter where letterid_pk = @letter

select isnull(@soption,''),isnull(@scourt,''),isnull(@sletter,'')

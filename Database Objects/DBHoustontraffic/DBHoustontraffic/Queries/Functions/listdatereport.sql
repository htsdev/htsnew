/****** Object:  UserDefinedFunction [dbo].[listdatereport]    Script Date: 01/25/2008 20:29:36 ******/
DROP FUNCTION [dbo].[listdatereport]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE     Function [dbo].[listdatereport](@listdate as datetime)
returns int
as
begin
	declare @totalcount int
	select @totalcount = count(distinct midnumber) 
	from tblticketsarchive T, tblticketsviolations V, tblTickets S
	where T.ticketnumber = V.refcasenumber
	and V.ticketid_pk = S.ticketid_pk
	and midnumber in (
	select distinct midnumber from tblticketsarchive where listdate = @listdate)
	and listdate < @listdate
	and activeflag = 1
	
	return @totalcount
end
GO

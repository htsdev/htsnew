/****** Object:  UserDefinedFunction [dbo].[Fn_GetSplitInfo]    Script Date: 01/25/2008 20:29:02 ******/
DROP FUNCTION [dbo].[Fn_GetSplitInfo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select dbo.Fn_GetSplitInfo(82715)

CREATE  Function [dbo].[Fn_GetSplitInfo](@ticketid as int)
returns tinyint
as
BEGIN

DECLARE tickets_cursor CURSOR FOR
SELECT isnull(courtdatemain,'01/01/1900'),isnull(courtnumbermain,0),
isnull(categoryID,0)
from tblTicketsViolations V, tblcourtviolationstatus C
where isnull(V.courtviolationstatusidmain,0) = C.courtviolationstatusid
--and      TicketID_PK = 82715
and isnull(V.courtviolationstatusidmain,0) <> 80 

--select courtviolationstatusidmain ,* from tblticketsviolations where ticketid_pk = 82563

--select * from tblcourtviolationstatus where description like 'dis%'

declare @courtdatemain datetime,
@courtnumbermain varchar(3),
@category int,
@tempcourtdatemain datetime,
@tempcourtnumbermain varchar(3),
@tempcategory int,
@retstatus int

set @retstatus = 0
OPEN tickets_cursor
  FETCH NEXT FROM tickets_cursor INTO 
	@courtdatemain,@courtnumbermain,@category

	IF @@FETCH_STATUS = 0     
		begin
			set @tempcourtdatemain = @courtdatemain
			set @tempcourtnumbermain = @courtnumbermain
			set @tempcategory = @category
			WHILE @@FETCH_STATUS = 0
				BEGIN

					if  @tempcourtdatemain  <> @courtdatemain
					begin
						set @retstatus = 1
					end

					FETCH NEXT FROM tickets_cursor INTO 
					@courtdatemain,@courtnumbermain,@category

					
				END 
		end
CLOSE tickets_cursor
DEALLOCATE tickets_cursor

return @retstatus 

END
GO

/****** Object:  UserDefinedFunction [dbo].[FormatComments]    Script Date: 01/25/2008 20:29:13 ******/
DROP FUNCTION [dbo].[FormatComments]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select * from dbo.currentdate
CREATE      Function [dbo].[FormatComments](@comments as varchar(2000),@employeeid as int)
returns varchar(2000)
as
begin
	declare @strcomments varchar(2000),
	@strabbr varchar(3),
	@tempdate datetime
	Select @tempdate = Date from dbo.currentdate
	SELECT @strabbr = Abbreviation FROM TBLUSERS where employeeid = @employeeid
	if @comments <> '' and @comments is not null and right(@comments,1) <> ')'
	begin
		set @strcomments  = @comments + ' (' + dbo.formatdateandtimeintoshortdateandtime(@tempdate) + ' - ' + @strabbr + ')' 
	end
	else
	begin
		set @strcomments  = @comments
	end
	return @strcomments
end
GO

/****** Object:  UserDefinedFunction [dbo].[GetTicketInfo]    Script Date: 01/25/2008 20:29:32 ******/
DROP FUNCTION [dbo].[GetTicketInfo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE            function [dbo].[GetTicketInfo] 
-- FUNCTION FOR GETTING INFORMATION OF ALL TICKETS RELATED TO A CUSTOMERS 
-- THIS FUNCTION IS SPECIFICALLY WORKS FOR CLIENTS RELATED TO OUTSIDE COURTS.......


	(

	-- DECLARING VARIABLES FOR INPUT......
	@firstname 	varchar(35),
	@lastname	varchar(35),
	@address1	varchar(50),
	@address2	varchar(50),
	@listdate	datetime,
	@courtid	int,
	@checkcourtdate	int,
	@currentdate	datetime,
	@casetype	int
	)
returns varchar (500)
-- IT WILL RETURN AN STRING THAT WILL HOLD TICKET NUMBER, 
-- CASE STATUS, COURT ROOM NO. AND COURT DATE..
as 

begin

	declare	@TicketInfo	varchar(500)
	set 	@ticketinfo=''

-- CONDITION FOR CHECKING FETCHING RECORDS WITH FUTURE COURT DATES....
-- @CheckCourtDate=0 MEANS DO NOT CHECK FOR FUTURE COURT DATES...
-- @CheckCourtDate=1 MEANS CHECK FOR FUTURE COURT DATES...

if @checkcourtdate=0
	begin	

		-- CONDITION FOR CHECKING A SINGLE CASE TYPE
		if @casetype<>0
			begin

				if @casetype = 2 
					begin
						-- SELECT RECORDS AND CONCATENATING IN A SINGLE STRING THAT WILL BE RETURNED BY THE FUNCITON....
		--				select	@ticketinfo=@ticketinfo + '<br>' +(
		--					convert (varchar(25),tva.ticketnumber_pk)+'-'+
		--					convert(varchar(25),tva.violationnumber_pk) + ' , ' + 
						select	@ticketinfo=(
							convert (varchar(25),tva.ticketnumber_pk)+' , '+
							(case when isnull(dt.[description],'') like '%------%-----%' then '-NA-'
								else (case when isnull(dt.[description],'') = '---Choose----' then '-NA-'
								      else isnull(dt.[description],'')
								      end)   
							end)+ ' , ' + 
--							isnull(convert(varchar(10),ta.courtnumber),'-NA-') +' , ' + 
							isnull(convert(varchar(10),ta.courtdate,101),'-NA-') ) 
						from	tblticketsarchive ta
						left outer join
							tblticketsviolationsarchive tva
						on 	ta.ticketnumber=tva.ticketnumber_pk
							left outer join
								tblcourtviolationstatus cvs
							on	tva.violationstatusid=cvs.courtviolationstatusid
								left outer join
									tbldatetype dt
								on 	cvs.categoryid=dt.typeid
						where	ta.firstname=@firstname
						and	ta.lastname=@lastname
						aND	ta.address1=@address1
						and	isnull(ta.address2,'')=isnull(@address2,'')
						and	ta.listdate=@listdate
						and	ta.courtid=@courtid
						and	(dt.typeid=@casetype or tva.violationstatusid is null)
					end
				else
					begin
						-- SELECT RECORDS AND CONCATENATING IN A SINGLE STRING THAT WILL BE RETURNED BY THE FUNCITON....
		--				select	@ticketinfo=@ticketinfo + '<br>' +(
		--					convert (varchar(25),tva.ticketnumber_pk)+'-'+
		--					convert(varchar(25),tva.violationnumber_pk) + ' , ' + 
						select	@ticketinfo=(
							convert (varchar(25),tva.ticketnumber_pk)+' , '+
							(case when isnull(dt.[description],'') like '%------%-----%' then '-NA-'
								else (case when isnull(dt.[description],'') = '---Choose----' then '-NA-'
								      else isnull(dt.[description],'')
								      end)   
							end)+ ' , ' + 
--							isnull(convert(varchar(10),ta.courtnumber),'-NA-') +' , ' + 
							isnull(convert(varchar(10),ta.courtdate,101),'-NA-') ) 
						from	tblticketsarchive ta
						left outer join
							tblticketsviolationsarchive tva
						on 	ta.ticketnumber=tva.ticketnumber_pk
							left outer join
								tblcourtviolationstatus cvs
							on	tva.violationstatusid=cvs.courtviolationstatusid
								left outer join
									tbldatetype dt
								on 	cvs.categoryid=dt.typeid
						where	ta.firstname=@firstname
						and	ta.lastname=@lastname
						aND	ta.address1=@address1
						and	isnull(ta.address2,'')=isnull(@address2,'')
						and	ta.listdate=@listdate
						and	ta.courtid=@courtid
						and	dt.typeid=@casetype 
					end


			end
		else

			-- FOR ALL CASE TYPES........
			begin
--				select	@ticketinfo=@ticketinfo + '<br>' +(
--					convert (varchar(25),tva.ticketnumber_pk)+'-'+
--					convert(varchar(25),tva.violationnumber_pk) + ' , ' + 
				select	@ticketinfo=(
					convert (varchar(25),tva.ticketnumber_pk)+' , '+
					(case when isnull(dt.[description],'') like '%------%-----%' then '-NA-'
						else (case when isnull(dt.[description],'') = '---Choose----' then '-NA-'
						      else isnull(dt.[description],'')
						      end)   
					end)+ ' , ' + 
--					isnull(convert(varchar(10),ta.courtnumber),'-NA-') +' , ' + 
					isnull(convert(varchar(10),ta.courtdate,101),'-NA-') ) 
				from	tblticketsarchive ta
				left outer join
					tblticketsviolationsarchive tva
				on 	ta.ticketnumber=tva.ticketnumber_pk
					left outer join
						tblcourtviolationstatus cvs
					on	tva.violationstatusid=cvs.courtviolationstatusid
						left outer join
							tbldatetype dt
						on 	cvs.categoryid=dt.typeid
				where	ta.firstname=@firstname
				and	ta.lastname=@lastname
				aND	ta.address1=@address1
				and	isnull(ta.address2,'')=isnull(@address2,'')
				and	ta.listdate=@listdate
				and	ta.courtid=@courtid

			end

	end

else  
	-- IF ONLY FUTURE COURT DATE RECORDS ARE REQURIED......
	begin	

		-- CONDITION FOR CHECKING A SINGLE CASE TYPE
		if @casetype<>0
			begin

				if @casetype = 2 
					begin
						-- SELECT RECORDS AND CONCATENATING IN A SINGLE STRING THAT WILL BE RETURNED BY THE FUNCITON....
		--				select	@ticketinfo=@ticketinfo + '<br>' +(
		--					convert (varchar(25),tva.ticketnumber_pk)+'-'+
		--					convert(varchar(25),tva.violationnumber_pk) + ' , ' + 
						select	@ticketinfo=(
							convert (varchar(25),tva.ticketnumber_pk)+' , '+
							(case when isnull(dt.[description],'') like '%------%-----%' then '-NA-'
								else (case when isnull(dt.[description],'') = '---Choose----' then '-NA-'
								      else isnull(dt.[description],'')
								      end)   
							end)+ ' , ' + 
--							isnull(convert(varchar(10),ta.courtnumber),'-NA-') +' , ' + 
							isnull(convert(varchar(10),ta.courtdate,101),'-NA-') ) 
						from	tblticketsarchive ta
						left outer join
							tblticketsviolationsarchive tva
						on 	ta.ticketnumber=tva.ticketnumber_pk
							left outer join
								tblcourtviolationstatus cvs
							on	tva.violationstatusid=cvs.courtviolationstatusid
								left outer join
									tbldatetype dt
								on 	cvs.categoryid=dt.typeid
						where	ta.firstname=@firstname
						and	ta.lastname=@lastname
						aND	ta.address1=@address1
						and	isnull(ta.address2,'')=isnull(@address2,'')
						and	ta.listdate=@listdate
						and	ta.courtid=@courtid
						and	(dt.typeid=@casetype or tva.violationstatusid is null)
						and	ta.courtdate  > @currentdate

					end
				else
					begin
						-- SELECT RECORDS AND CONCATENATING IN A SINGLE STRING THAT WILL BE RETURNED BY THE FUNCITON....
		--				select	@ticketinfo=@ticketinfo + '<br>' +(
		--					convert (varchar(25),tva.ticketnumber_pk)+'-'+
		--					convert(varchar(25),tva.violationnumber_pk) + ' , ' + 
						select	@ticketinfo=(
							convert (varchar(25),tva.ticketnumber_pk)+' , '+
							(case when isnull(dt.[description],'') like '%------%-----%' then '-NA-'
								else (case when isnull(dt.[description],'') = '---Choose----' then '-NA-'
								      else isnull(dt.[description],'')
								      end)   
							end)+ ' , ' + 
--							isnull(convert(varchar(10),ta.courtnumber),'-NA-') +' , ' + 
							isnull(convert(varchar(10),ta.courtdate,101),'-NA-') ) 
						from	tblticketsarchive ta
						left outer join
							tblticketsviolationsarchive tva
						on 	ta.ticketnumber=tva.ticketnumber_pk
							left outer join
								tblcourtviolationstatus cvs
							on	tva.violationstatusid=cvs.courtviolationstatusid
								left outer join
									tbldatetype dt
								on 	cvs.categoryid=dt.typeid
						where	ta.firstname=@firstname
						and	ta.lastname=@lastname
						aND	ta.address1=@address1
						and	isnull(ta.address2,'')=isnull(@address2,'')
						and	ta.listdate=@listdate
						and	ta.courtid=@courtid
						and	dt.typeid=@casetype 
						and	ta.courtdate  > @currentdate					
					end


			end
		else

			-- FOR ALL CASE TYPES........
			begin
--				select	@ticketinfo=@ticketinfo + '<br>' +(
--					convert (varchar(25),tva.ticketnumber_pk)+'-'+
--					convert(varchar(25),tva.violationnumber_pk) + ' , ' + 
				select	@ticketinfo=(
					convert (varchar(25),tva.ticketnumber_pk)+' , '+
					(case when isnull(dt.[description],'') like '%------%-----%' then '-NA-'
						else (case when isnull(dt.[description],'') = '---Choose----' then '-NA-'
						      else isnull(dt.[description],'')
						      end)   
					end)+ ' , ' + 
--					isnull(convert(varchar(10),ta.courtnumber),'-NA-') +' , ' + 
					isnull(convert(varchar(10),ta.courtdate,101),'-NA-') ) 
				from	tblticketsarchive ta
				left outer join
					tblticketsviolationsarchive tva
				on 	ta.ticketnumber=tva.ticketnumber_pk
					left outer join
						tblcourtviolationstatus cvs
					on	tva.violationstatusid=cvs.courtviolationstatusid
						left outer join
							tbldatetype dt
						on 	cvs.categoryid=dt.typeid
				where	ta.firstname=@firstname
				and	ta.lastname=@lastname
				aND	ta.address1=@address1
				and	isnull(ta.address2,'')=isnull(@address2,'')
				and	ta.listdate=@listdate
				and	ta.courtid=@courtid
				and	ta.courtdate  > @currentdate

			end

	end



	-- REMOVING EXTRA <BR> TAG FROM THE RETURNING STRING .......
/*	if left(@ticketinfo,4)='<br>'
		begin
			set @ticketinfo=right(@ticketinfo,len(@ticketinfo)-4)
		end	
*/


-- RETURNING FROM THE FUNCTION......
return	@ticketinfo

end
GO

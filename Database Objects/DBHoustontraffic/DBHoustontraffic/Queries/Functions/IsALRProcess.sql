﻿/****** 
Created by:		Muhammad Nasir
Business Logic:	The function is used in to check the court is ALR Process or not
				
				
List of Parameters:
	@CourtID:	Call ID 	

List of Columns:	
	@IsALR: if court is ALR Process return 1 else 0
*******/
alter FUNCTION dbo.IsALRProcess(@CourtID INT)
RETURNs BIT
AS
BEGIN
	DECLARE @IsALR BIT
	
		IF(EXISTS(SELECT * FROM tblCourts WHERE Courtid=@CourtID AND ALRProcess=1))
			SET @IsALR= 1
		ELSE
			SET @IsALR= 0 
	RETURN @IsALR
END
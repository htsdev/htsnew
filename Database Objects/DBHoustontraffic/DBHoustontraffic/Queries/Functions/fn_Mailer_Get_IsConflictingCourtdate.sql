/****** Object:  UserDefinedFunction [dbo].[fn_Mailer_Get_IsConflictingCourtdate]    Script Date: 01/25/2008 20:29:10 ******/
DROP FUNCTION [dbo].[fn_Mailer_Get_IsConflictingCourtdate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_Mailer_Get_IsConflictingCourtdate]
	(
	@RecordId int
	)
returns int
as 
	
begin
	declare @datecount int, @output int
	select @datecount = count(distinct datediff(day, courtdate, getdate()))
	from tblticketsviolationsarchive where recordid = @recordid

	if @datecount = 0  or @datecount = 1
		set @output = 0
	else 
		set @output = 1

	return @output
end
GO

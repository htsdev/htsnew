/****** Object:  UserDefinedFunction [dbo].[fn_GetRelatedCaseNumbers]    Script Date: 01/25/2008 20:29:00 ******/
DROP FUNCTION [dbo].[fn_GetRelatedCaseNumbers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  function [dbo].[fn_GetRelatedCaseNumbers](@ticketid int)  
returns varchar(100)  
as  
begin  
  
declare @CaseNumbers varchar(100)  
set @CaseNumbers = ''  
select @CaseNumbers = @CaseNumbers + refcasenumber +','  from tblticketsviolations where ticketid_pk = @ticketid  
  
if(len(@CaseNumbers)>0)  
set @CaseNumbers = substring(@CaseNumbers,1,len(@CaseNumbers)-1)  
  
return @CaseNumbers  
end
GO

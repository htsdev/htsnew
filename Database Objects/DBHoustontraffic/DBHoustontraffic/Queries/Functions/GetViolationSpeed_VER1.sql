/****** Object:  UserDefinedFunction [dbo].[GetViolationSpeed_VER1]    Script Date: 01/25/2008 20:29:36 ******/
DROP FUNCTION [dbo].[GetViolationSpeed_VER1]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--select distinct violationcode from tblviolations where description like '%speeding%' and violationtype = 2 and violationcode = '43030K'

--select dbo.GetViolationSpeed_VER1(1,'103048321',235,'43030K') 

--select violationcode from tblviolations where description like '%speeding%' and violationtype = 2
--and 



CREATE            Function [dbo].[GetViolationSpeed_VER1](@SeqNumber int,@RefCaseNumber varchar(20),@FineAmount money,@ViolationCode varchar(20))
Returns varchar(10)
AS
BEGIN
--initialize
Declare @Speed varchar(10)
Select @Speed = ''
	IF(@SeqNumber=1 OR @SeqNumber=2) and @ViolationCode in (select distinct violationcode from tblviolations where description like '%speeding%' and violationtype = 2)
		
		BEGIN
			IF @FineAmount <> 0 
				BEGIN
				IF(Left(@RefCaseNumber,1) ='0' OR Left(@RefCaseNumber,1) ='M' or Left(@RefCaseNumber,1) ='1')
					BEGIN
					IF(@FineAmount <= 160)
						BEGIN	
							Set @Speed = '10'
						END
					ELSE
						BEGIN
							if @SeqNumber=2
								begin
									Set @Speed = '+' + convert(varchar(10),ceiling(round(((@FineAmount - 160)/5),0)))
								end
							else
								begin
									Set @Speed = '+' + convert(varchar(10),ceiling(round(((@FineAmount - 160)/5)+10,0)))
								end
						
						END
					END
				END
				ELSE
				BEGIN
					Set @Speed = '0'
				END
		END
		
	RETURN  @Speed 
END
GO

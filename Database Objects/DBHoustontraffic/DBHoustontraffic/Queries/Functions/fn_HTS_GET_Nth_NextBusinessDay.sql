/****** Object:  UserDefinedFunction [dbo].[fn_HTS_GET_Nth_NextBusinessDay]    Script Date: 01/25/2008 20:29:07 ******/
DROP FUNCTION [dbo].[fn_HTS_GET_Nth_NextBusinessDay]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fn_HTS_GET_Nth_NextBusinessDay]
	(
	@inputDate	datetime,
	@NoOfDays	int
	)
returns datetime

as

begin
	declare	@Count		int
	declare	@NextDay	datetime
	
	
	set	@count = 1
	set	@nextday = @inputDate
	WHILE @count <= @NoOfDays
	BEGIN
	        SET @NextDay = DATEADD(day, 1, @NextDay)
	        SET @NextDay = CASE DATEPART(dw, @NextDay)
	        WHEN 1 THEN DATEADD(d, 1, @NextDay)
	        WHEN 7 THEN DATEADD(d, 2, @NextDay)
	        ELSE @NextDay
	        END
		set @count = @count + 1
	end
	return @NextDay
END
GO

/******
Business logic : This function is used to get customer information by mid number
Parameters: 
----------
@ticketid :  It takes Ticket ID
This function returns the number of times client has hired us.
*******/

    alter    function fn_HTS_Get_CustomerInfoByMidnumber     
 (  
 @ticketid int    
 )  
returns  int  
  
as  
  
begin  
  
declare @paymentcount int,    
 @midnum varchar(20),    
 @lastname varchar(20),    
 @firstname varchar(20)    
-- Waqas 5463 02/06/2009 adding check for mid number
if(len(REPLACE(isnull(@midnum,''),'''','')) = 0)
begin
 set @paymentcount =0
end
else  
begin
declare @temp table  
 ( midnum   varchar(20) )  
  
insert into @temp  
select Midnum from tbltickets  where ticketid_pk = @ticketid     
  
select  @paymentcount = count(*) from tbltickets T, @temp a   
where  a.midnum = T.midnum    
and  activeflag = 1    
and  ticketid_pk <> @ticketid    
  
end
-- Waqas 5463 02/06/2009 
return  @PaymentCount+1
    
end    
    
    
    
    
  
  
  
  
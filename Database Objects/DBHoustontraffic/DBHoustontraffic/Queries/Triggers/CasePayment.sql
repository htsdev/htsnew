/*        
Created By     : Fahad Muhammad Qureshi.
Created Date   : 07/27/2009  
TasK		   : 6054        
Business Logic : This Trigger Fired when pyament will Take place and it will also insert history note in Client History also register the appropriate letter.      
*/

GO
/****** Object:  Trigger [dbo].[CasePayment]    Script Date: 06/21/2011 11:42:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER TRIGGER [dbo].[CasePayment]
ON [dbo].[tblTicketsPayment]
FOR  UPDATE, INSERT
AS
	DECLARE @TicketID INT,
	        @paymentmethod VARCHAR(50),
	        @PaymentType INT,
	        @ChargeAmount MONEY,
	        @paymentvoid TINYINT,
	        @PaymentReference VARCHAR(50)
	
	
-- Abbas Qamar 10024 02/02/2012 Added for Internet hired clients. "Internet" notee will be added along with Date Time and Employee ID in the Trail comments   
	IF EXISTS (
		SELECT Ticketid_pk  FROM tbltickets t 
				   INNER JOIN INSERTED i 
				   ON i.ticketid = t.TicketID_PK 
	               WHERE 
	               --ISNULL(t.Activeflag,0) = 0 And  -- Check that newly inserted payment is the first payment.
				   i.PaymentType = 7 -- make sure that the newly insert payment type is internet.
				   AND ISNULL(i.paymentvoid,0) = 0 -- make sure that the newly insert payment is not voided.
				   AND i.ChargeAmount>0
	
	)
	
	    BEGIN   	
	        	-- Declare variable for getting Employee Abbrivation
	            DECLARE @EmpName VARCHAR(200)
	            -- Get abbrivation of the current employee
	            SELECT @EmpName = u.Abbreviation FROM   tblusers u WHERE u.EmployeeID = 3992
	            -- Update client profile by adding trial comments "Internet".
	            UPDATE t
	            SET    t.TrialComments = CASE 
	                                          WHEN LEN(LTRIM(RTRIM(ISNULL(t.TrialComments, ''))))
	                                               > 0 THEN 
	                                               -- Cancatinate existing trial comments with newly added trial comments.
	                                               LTRIM(RTRIM(ISNULL(t.TrialComments, ''))) 
	                                               + ' Internet (' + dbo.fn_DateFormat(GETDATE(), 2, '/', ':', 1) 
	                                               + ' - ' + @EmpName + ')'
	                                               -- Add newly trial comments.
	                                          ELSE 'Internet (' + dbo.fn_DateFormat(GETDATE(), 2, '/', ':', 1) 
	                                               + ' - ' + @EmpName + ')'
	                                     END
	            FROM   tbltickets t
	                   INNER JOIN INSERTED i
	                   ON  i.ticketid = t.ticketid_pk
	            WHERE  t.ticketid_pk = i.ticketid
	            
	            INSERT INTO tblticketsnotes
					  (
						ticketid,
						SUBJECT,
						employeeid,Fk_CommentID
					  )
				SELECT i.ticketid,
						   'Trial Notes: ' + 'Internet (' + dbo.fn_DateFormat(GETDATE(), 2, '/', ':', 1) + ' - ' + @EmpName + ')',
						   3992,4
				FROM   INSERTED i	
	     END
	   -- END 10024   
	        
	
	IF EXISTS(
	       SELECT DISTINCT i.TicketID
	       FROM   INSERTED i
	       WHERE  (i.PaymentType NOT IN (0, 8, 99, 100, 98)) AND i.isRemoved <> 1 -- Rab Nawaz Khan 10284 05/17/2012 Fix the double entry payment note issue on history page. . . 
	   )
	BEGIN
	    SELECT DISTINCT @TicketID = i.TicketID,
	           @PaymentType = Paymenttype,
	           @paymentvoid = paymentvoid,
	           @PaymentReference = PaymentReference
	    FROM   INSERTED i
	    
	    SELECT @paymentmethod = DESCRIPTION
	    FROM   tblpaymenttype
	    WHERE  Paymenttype_PK = @PaymentType
	    
	    UPDATE tbltickets
	    SET    activeflag = 1
	    WHERE  ticketid_pk = @TicketID
	           AND activeflag = 0
	    
	    IF @paymentvoid <> 1
	    BEGIN
	        INSERT INTO tblticketsnotes
	          (
	            ticketid,
	            SUBJECT,
	            employeeid,
	            Notes
	          )
	        -- removed user abbreviation by TAHIR AHMED from case history note as per Saher's request (bug # 1071)
	        -- select @ticketid,'Paid $'+ convert(varchar(12),i.chargeamount) + ' - '  + @paymentmethod + ' - ' + u.abbreviation ,i.Employeeid,null from inserted i,tblusers U
	        SELECT @ticketid, --Afaq 9398 06/20/2011 add payment Identifier
	               'Paid $' + CONVERT(VARCHAR(12), i.chargeamount) + ' - ' + @paymentmethod +' ['+(CASE p.Id WHEN 0 THEN 'N/A' ELSE p.[Name] END)+'] '--Fahad 9398 09/22/2011 add case in  payment Identifier
	               + ' ' + CASE --Fahad 6054 07/27/2009 Case added on Payemt Description
	                            WHEN LEN(ISNULL(@PaymentReference, '')) = 0 THEN 
	                                 ''
	                            WHEN @PaymentType = 104 AND LEN(ISNULL(@PaymentReference, ''))-- 104-Partener Firm Credit
	                                 > 0 THEN '[' + (
	                                     SELECT TOP 1 tf.FirmAbbreviation
	                                     FROM   tblFirm tf
	                                     WHERE  tf.FirmID = @PaymentReference
	                                 ) + ']'
	                            WHEN @PaymentType = 11 AND LEN(ISNULL(@PaymentReference, ''))-- 11-Attorney Credit
	                                 > 0 THEN '[C/O ' + (
	                                     SELECT TOP 1 tu.UserName
	                                     FROM   tblusers tu
	                                     WHERE  tu.EmployeeID = @PaymentReference
	                                 ) + ']'
	                            WHEN @PaymentType = 9 AND LEN(ISNULL(@PaymentReference, ''))-- 9-Friend Credit
	                                 > 0 THEN '[' + @PaymentReference + ']'
	                            ELSE ''
	                       END,
	               i.Employeeid,
	               NULL
	        FROM   INSERTED i,
	               tblusers U,paymentIdentifier p
	        WHERE  i.employeeid = u.employeeid AND i.paymentIdentifier=p.Id
	    END
	    
	    IF NOT EXISTS (
	           SELECT *
	           FROM   dbo.tblLetterOutgoing
	           WHERE  TicketID_PK = @TicketID
	                  AND LetterID = 1
	                  AND PrintFlag = 0
	       )
	       -- if not letter for payment receipt is in the que then, enter one.
	    BEGIN
	        INSERT INTO dbo.tblLetterOutgoing
	          (
	            TicketID_PK,
	            LetterID,
	            PrintFlag
	          )
	        VALUES
	          (
	            @TicketID,
	            1,
	            0
	          )
	    END
	END
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


ALTER TRIGGER [dbo].[trg_tblViolations_Events] ON [dbo].[tblViolations] 
FOR INSERT, UPDATE, DELETE 
AS


if exists (
	select i.* from inserted i inner join deleted d
	on i.violationnumber_pk = d.violationnumber_pk
	and ( 
		    isnull(i.chargeamount,0) <>isnull(d.chargeamount ,0)
		or  isnull(i.bondamount,0) <> isnull(d.bondamount,0)
		or  isnull(i.effectivedate ,'1/1/1900')<> isnull(d.effectivedate,'1/1/1900')
		or  isnull(i.newbondamount,0) <> isnull(d.newbondamount,0)
		or  isnull(i.neweffectivedate ,'1/1/1900')<> isnull(d.neweffectivedate,'1/1/1900')
		)
	)
	begin
		insert into tblViolationsHistory (
			violationnumber_pk, chargeamount, bondamount, effectivedate, newbondamount, neweffectivedate)
		select violationnumber_pk, chargeamount, bondamount, effectivedate, newbondamount, neweffectivedate
		from inserted
	end






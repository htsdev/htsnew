USE [traffictickets]
GO
/****** Object:  Trigger [dbo].[trg_FaxStatus]    Script Date: 11/15/2011 19:09:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Sabir Khan 9917 11/18/2011 
--Trigger to update faxletter table to be sure if the fax is delivered or not and if delivered then add the record in history to

CREATE TRIGGER [dbo].[trg_FaxStatus]
   ON  [dbo].[fm_faxout]
   AFTER update
AS 
BEGIN
	
SET NOCOUNT ON;

declare @result varchar(100) ,
@FullSubject varchar(250),
@letterid int

select @result = result ,@FullSubject = full_subject from inserted--from inserted
set @FullSubject = Reverse(@FullSubject)
select  @letterid= Substring(@FullSubject ,0,charindex('#',@FullSubject ))

update traffictickets.dbo.faxletter 
set status=@result
where Reverse(FaxLetterid)=@letterid

	DECLARE	@return_value int,
			@return_doc varchar(max),
			@ticketid int,
			@empid int,
			@faxnumber varchar(30),
			@subject varchar(500),
			@docpath varchar(100),

			@Updated datetime,                        
			@Extension nvarchar(4),
			@description varchar(50),
			@docType nvarchar(50),
			@subDocTypeID int,			
			@count int,			
			@book int,
			@events varchar(50),
			@bookID nvarchar(50),
			@courtname varchar(50)

			set @ticketid=(select top 1 ticketid from traffictickets.dbo.faxletter  where Reverse(FaxLetterid)=@letterid)
			set @empid=(select top 1 employeeid from traffictickets.dbo.faxletter  where Reverse(FaxLetterid)=@letterid)

			
			set @faxnumber = (select top 1 tblcourts.fax from traffictickets.dbo.tblticketsviolations inner join traffictickets.dbo.tblcourts on traffictickets.dbo.tblcourts.courtid=traffictickets.dbo.tblticketsviolations.courtid where traffictickets.dbo.tblticketsviolations.ticketid_pk=@ticketid)
			set @courtname=	(select  top 1 tblcourts.shortname from traffictickets.dbo.tblticketsviolations inner join traffictickets.dbo.tblcourts on traffictickets.dbo.tblcourts.courtid=traffictickets.dbo.tblticketsviolations.courtid where traffictickets.dbo.tblticketsviolations.ticketid_pk=@ticketid) 

			set @subject ='Letter of Rep :REF#' + Reverse(@letterid)+ ' Confirmation to Fax Number:'+ traffictickets.dbo.formatphonenumbers(@faxnumber)+ ' for court house '+@courtname+' - Confirmed'
			set @docpath=(select top 1 docpath from traffictickets.dbo.faxletter where Reverse(FaxLetterid)=@letterid)
			
			set @Updated=getdate()
			set @Extension='pdf'			
			set	@description='Fax Letter Confirm'
			set	@docType='Fax Letter'			
			set	@subDocTypeID=0
			set	@count=(select count(*) from  traffictickets.[dbo].[tblticketsviolations]   where ticketid_pk=@ticketid)
			set	@book=0
			set	@events='Print'
			set	@bookID=''
 
if @result='SUCCESS'
	begin			

			EXEC	@return_value = traffictickets.[dbo].[usp_HTS_Insert_CaseHistoryInfo]
					@TicketID=@ticketid,
					@Subject=@subject,
					@EmpID = @empid,
					@Note='null'
			SELECT	'Return Value' = @return_value  

			Exec traffictickets.[dbo].[usp_hts_NewAddScan]
					@updated= @Updated,
					@extension=@Extension ,
					@Description=@description ,
					@DocType=@docType ,
					@SubDocTypeID=@subDocTypeID ,
					@Employee=@empid ,
					@TicketID=@ticketid ,
					@Count=@count ,
					@Book =@book,
					@Events=@events,
					@BookID=@return_doc output
			

			declare @filename varchar (200)
			set @filename= (select docpathforautofax from traffictickets.[dbo].[tblConfigurationSetting] where id=2) + @return_doc + (select extension from traffictickets.[dbo].[tblConfigurationSetting] where id=2)			
			print @return_doc
			print @docpath
			print @filename
			
			exec traffictickets.[dbo].copyfile @docpath,@filename
			
			update traffictickets.dbo.faxletter 
				set docpath=@filename
				where Reverse(FaxLetterid)=@letterid
	end
	else
	begin
	
 	set @subject ='Letter of Rep :REF# ' + Reverse(@letterid)+' '+ @result +' to Fax Number:'+traffictickets.dbo.formatphonenumbers(@faxnumber)+ ' for court house '+@courtname

	EXEC	@return_value = traffictickets.[dbo].[usp_HTS_Insert_CaseHistoryInfo]
					@TicketID=@ticketid,
					@Subject=@subject,
					@EmpID = @empid,
					@Note='null'
			SELECT	'Return Value' = @return_value  
	end



END
USE [TrafficTickets]
GO
/****** Object:  Trigger [dbo].[trg_tblTicketsViolations_Events]    Script Date: 07/25/2011 12:46:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Zeeshan 4420 08/08/2008 Update ArrSetEmailCount to 0 when court is reset.
       --Zeeshan Ahmed 3825 04/23/2008
       --Comment Covering Firm ID Code and Moved It In Update Violation Procedure.
ALTER TRIGGER [dbo].[trg_tblTicketsViolations_Events]
ON [dbo].[tblTicketsViolations]
FOR  INSERT, UPDATE
AS
	DECLARE @newcontactcomments VARCHAR(500),
	        @oldcontactcomments VARCHAR(500),
	        @ticketid_pk INT,
	        @empid INT,
	        @ChangeFlag INT,
	        @ULBond INT,
	        @ULBondYes INT,
	        @OldULBond INT,
	        @NewULBond INT
	
	---------------------------------------------------------------------------------------------------------
	-- UPDATING LOCK FLAG IF COURT,  VIOLATION, PLAN ID CHANGES......
	---------------------------------------------------------------------------------------------------------
	IF EXISTS (
	       SELECT i.ticketid_pk
	       FROM   INSERTED i
	              INNER JOIN DELETED d
	                   ON  i.ticketsviolationid = d.TicketsViolationId
	                   AND (
	                           i.courtid <> d.courtid
	                           OR i.violationnumber_pk <> d.violationnumber_pk
	                           OR i.planid <> d.planid
	                       )
	   )
	    UPDATE t
	    SET    t.lockflag = 0,
	           t.employeeidupdate = i.vemployeeid
	    FROM   tbltickets t
	           INNER JOIN INSERTED i
	                ON  t.ticketid_pk = i.ticketid_pk
	
	
	---------------------------------------------------------------------------------------------------------
	--UPDATING BONDDATE IF STATUS CHANGE TO BOND
	---------------------------------------------------------------------------------------------------------
	BEGIN
	    SELECT @NewULBond = CONVERT(INT, i.COURTVIOLATIONSTATUSIDMAIN)
	    FROM   INSERTED i
	    
	    SELECT @OldULBond = CONVERT(INT, d.COURTVIOLATIONSTATUSIDMAIN)
	    FROM   DELETED d
	    
	    IF (@OldULBond != 135 AND @NewULBond = 135)
	    BEGIN
	        UPDATE tvv
	        SET    tvv.BondDate = GETDATE(),
	               tvv.BondPrintFlag = 0,
	               tvv.vemployeeid = i.vemployeeid
	        FROM   tblticketsViolations tvv
	               INNER JOIN INSERTED i
	                    ON  i.TicketsViolationID = tvv.TicketsViolationID
	    END
	END
	---------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------
	
	/*-- INSERTING NOTE IN CASE HISTORY IF CASE DATA CHANGES.......
	* INSERT NOTE IF COURT DATE & TIME CHANGES
	* INSERT NOTE IF COURT HOUSE CHANGES
	* INSERT NOTE IF COURT NUMBER CHANGES
	* INSERT NOTE IF CASE STATUS CHANGES
	* INSERT NOTE IF VIOLATION DESCRIPTION CHANGES     
	---------------------------------------------------------------------------------------------------------*/
	
	DECLARE @PrevCourtDate DATETIME,
	        @PrevCourtNumber VARCHAR(3),	-- Sabir Khan 7979 07/02/2010 type has been changed from int into varchar.
	        @PrevStatus INT,
	        @PrevCourtId INT,
	        @PrevViolation INT
	
	DECLARE @NewCourtDate DATETIME,
	        @NewCourtNumber VARCHAR(3),	-- Sabir Khan 7979 07/02/2010 type has been changed from int into varchar.
	        @NewStatus INT,
	        @NewCourtId INT,
	        @NewViolation INT,
	        @NewEmpId INT,
	        @RefCaseNumber VARCHAR(25),
	        @CourtName VARCHAR(50),
	        @CaseDesc VARCHAR(50),
	        @CaseDescAuto VARCHAR(50),
	        @ViolDesc VARCHAR(100)
	
	DECLARE @PrevAutoCourtDate DATETIME,
	        @PrevAutoCourtNumber VARCHAR(3),	-- Sabir Khan 7979 07/02/2010 type has been changed from int into varchar.
	        @PrevAutoStatus INT,
	        @PrevAutoCourtId INT,
	        @PrevAutoViolation INT
	
	DECLARE @NewAutoCourtDate DATETIME,
	        @NewAutoCourtNumber VARCHAR(3),	-- Sabir Khan 7979 07/02/2010 type has been changed from int into varchar.
	        @NewAutoStatus INT,
	        @NewAutoCourtId INT,
	        @NewAutoViolation INT
	
	DECLARE @PrevLoaderUpdateDate DATETIME,
	        @NewLoaderUpdateDate DATETIME,
	        @PrevBSDAUpdateDate DATETIME,
	        @NewBSDAUpdateDate DATETIME
	
	SELECT @PrevCourtDate = ISNULL(d.courtdatemain, '01/01/1900'),
	       @PrevCourtNumber = ISNULL(d.courtnumbermain, '0'),	-- Sabir Khan 7979 07/02/2010 type has been changed from int into varchar.
	       @PrevStatus = d.courtviolationstatusidmain,
	       @PrevCourtId = d.courtid,
	       @PrevViolation = d.violationnumber_pk,
	       @NewCourtDate = ISNULL(i.courtdatemain, '01/01/1900'),
	       @NewCourtNumber = ISNULL(i.courtnumbermain, '0'),	-- Sabir Khan 7979 07/02/2010 type has been changed from int into varchar.
	       @NewStatus = i.courtviolationstatusidmain,
	       @NewCourtId = i.courtid,
	       @NewViolation = i.violationnumber_pk,
	       @PrevAutoCourtDate = ISNULL(d.courtdate, '01/01/1900'),
	       @PrevAutoCourtNumber = ISNULL(d.courtnumber, '0'),	-- Sabir Khan 7979 07/02/2010 type has been changed from int into varchar.
	       @PrevAutoStatus = d.courtviolationstatusid,
	       @NewAutoCourtDate = ISNULL(i.courtdate, '01/01/1900'),
	       @NewAutoCourtNumber = ISNULL(i.courtnumber, '0'),	-- Sabir Khan 7979 07/02/2010 type has been changed from int into varchar.
	       @NewAutoStatus = i.courtviolationstatusid,
	       @PrevLoaderUpdateDate = ISNULL(d.loaderupdatedate, '01/01/1900'),
	       @NewLoaderUpdateDate = ISNULL(i.loaderupdatedate, '01/01/1900'),
	       @PrevBSDAUpdateDate = ISNULL(d.bsdaupdatedate, '01/01/1900'),
	       @NewBSDAUpdateDate = ISNULL(i.bsdaupdatedate, '01/01/1900'),
	       @NewEmpId = i.vemployeeid,
	       @RefCaseNumber = i.refcasenumber,
	       @CourtName = ISNULL(c.shortname, 'N/A') + CASE 
	                                                      WHEN ISNULL(i.OscarCourtDetail, '') 
	                                                           <> '' THEN ' (' +
	                                                           i.OscarCourtDetail 
	                                                           + ')'
	                                                      ELSE ''
	                                                 END,
	       @CaseDesc = ISNULL(s.shortdescription, 'N/A'),
	       @CaseDescAuto = ISNULL(s2.shortdescription, 'N/A'),
	       @ViolDesc = ISNULL(v.shortdesc, 'N/A'),
	       @ticketid_pk = i.ticketid_pk
	FROM   INSERTED i
	       INNER JOIN DELETED d
	            ON  i.ticketsviolationid = d.ticketsviolationid
	       LEFT OUTER JOIN tblcourts c
	            ON  c.courtid = i.courtid
	       LEFT OUTER JOIN tblcourtviolationstatus s
	            ON  s.courtviolationstatusid = i.courtviolationstatusidmain
	       LEFT OUTER JOIN tblcourtviolationstatus s2
	            ON  s2.courtviolationstatusid = i.courtviolationstatusid
	       LEFT OUTER JOIN tblviolations v
	            ON  v.violationnumber_pk = i.violationnumber_pk
	
	-- IF RECORD NOT UPDATED THROUGH LOADER THEN INSERT NOTES IN HISTORY WITH VERIFIED STATUS AND COURT DATE....
	IF @PrevLoaderUpdateDate = @NewLoaderUpdateDate
	BEGIN
	    IF (
	           DATEDIFF(minute, @PrevCourtDate, @NewCourtDate) <> 0
	           OR @PrevCourtNumber <> @NewCourtNumber
	           OR @PrevStatus <> @NewStatus
	           OR @PrevCourtId <> @NewCourtId
	           OR @PrevViolation <> @NewViolation
	       )
	    BEGIN
	        IF @PrevBSDAUpdateDate = @NewBSDAUpdateDate
	        BEGIN
	            INSERT INTO tblticketsnotes
	              (
	                ticketid,
	                SUBJECT,
	                employeeid
	              )
	            SELECT @TicketID_PK,
	                   'CASE DATA CHANGED - ' +
	                   UPPER(@RefCaseNumber) + ': ' +
	                   UPPER(@CourtName) + ': ' +
	                   UPPER(@CaseDesc) + ': ' +
	                   CONVERT(VARCHAR(10), @NewCourtDate, 101) + ' #' + CONVERT(VARCHAR(10), @NewCourtNumber) 
	                   +
	                   ' @' + LTRIM(RIGHT(CONVERT(VARCHAR(30), @NewCourtDate, 100), 8)) 
	                   +
	                   ' (' + UPPER(@ViolDesc) + ')',
	                   @NewEmpId
	        END
	        ELSE
	        BEGIN
	            INSERT INTO tblticketsnotes
	              (
	                ticketid,
	                SUBJECT,
	                employeeid
	              )
	            SELECT @TicketID_PK,
	                   'CASE DATA CHANGED BSDA- ' +
	                   UPPER(@RefCaseNumber) + ': ' +
	                   UPPER(@CourtName) + ': ' +
	                   UPPER(@CaseDesc) + ': ' +
	                   CONVERT(VARCHAR(10), @NewCourtDate, 101) + ' #' + CONVERT(VARCHAR(10), @NewCourtNumber) 
	                   +
	                   ' @' + LTRIM(RIGHT(CONVERT(VARCHAR(30), @NewCourtDate, 100), 8)) 
	                   +
	                   ' (' + UPPER(@ViolDesc) + ')',
	                   @NewEmpId
	        END
	    END
	END-- IF RECORD  UPDATED THROUGH LOADER THEN INSERT NOTES IN HISTORY WITH AUTO STATUS AND COURT DATE....
	ELSE
	BEGIN
	    IF (
	           DATEDIFF(minute, @PrevAutoCourtDate, @NewAutoCourtDate) <> 0
	           OR @PrevAutoCourtNumber <> @NewAutoCourtNumber
	           OR @PrevAutoStatus <> @NewAutoStatus
	           OR @PrevCourtId <> @NewCourtId
	           OR @PrevViolation <> @NewViolation
	       )
	    BEGIN
	        INSERT INTO tblticketsnotes
	          (
	            ticketid,
	            SUBJECT,
	            employeeid,
	            notetype
	          )
	        SELECT @TicketID_PK,
	               'CASE DATA CHANGED - ' +
	               UPPER(@RefCaseNumber) + ': ' +
	               UPPER(@CourtName) + ': ' +
	               UPPER(@CaseDescAuto) + ': ' +
	               CONVERT(VARCHAR(10), @NewAutoCourtDate, 101) + ' #' + CONVERT(VARCHAR(10), @NewautoCourtNumber) 
	               +
	               ' @' + LTRIM(RIGHT(CONVERT(VARCHAR(30), @NewAutoCourtDate, 100), 8)) 
	               +
	               ' (' + UPPER(@ViolDesc) + ')',
	               3992,
	               1
	    END
	END
	-- If court date or status is reset then change the call status to Pending. Modified By Agha Usman Task Id 3882
	-- Tahir 4755 09/05/2008
	--IF (@PrevCourtDate <> @NewCourtDate OR @PrevStatus <> @NewStatus OR  @PrevCourtNumber <> @NewCourtNumber) 
	IF EXISTS (
	       SELECT i.ticketsviolationid
	       FROM   INSERTED i
	              INNER JOIN DELETED d
	                   ON  i.ticketsviolationid = d.ticketsviolationid
	       WHERE  (
	                  ISNULL(i.courtnumbermain, '') <> ISNULL(d.courtnumbermain, '')
	                  OR ISNULL(i.courtdatemain, '1/1/1900 00:00:00.000') <>
	                     ISNULL(d.courtdatemain, '1/1/1900 00:00:00.000')
	                  OR ISNULL(i.courtviolationstatusidmain, 0) <> ISNULL(d.courtviolationstatusidmain, 0)
	              )
	   )
	BEGIN
	    -- tahir 4981 10/16/2008
	    -- reset set call, reminder call and FTA call status to pending
	    -- if court date , courtnumber or status changes....	
	    
	    --update tblticketsviolations set setcallstatus = 0 where ticketId_pk = @ticketid_pk
	    
	    UPDATE v
	    SET    v.setcallstatus = 0,
	           v.remindercallstatus = 0,
	           --Farrukh 9332 08/27/2011 CallBack Statuses set to pending when court setting has been changed
	           v.AutoDialerSetCallStatus = 0,
	           v.AutoDialerReminderCallStatus = 0,
	           v.ftacallstatus = 0,
	           v.vemployeeid = i.vemployeeid
	    FROM   tblticketsviolations v
	           INNER JOIN INSERTED i
	                ON  i.ticketsviolationid = v.ticketsviolationid
	    
		--Ozair 9671 09/09/2011 inserting note in history for status reset to pending of Set/Reminder Call and Auto Dialer Set/Reminder Call status
		INSERT INTO tblticketsnotes
		(
			ticketid,
			SUBJECT,
			employeeid
		)
		SELECT DISTINCT i.ticketid_pk,
			'Set/Reminder Call and Auto Dialer Set/Reminder Call status reset to Pending',
			3992 -- System user id
		FROM INSERTED i
            
	    
	    --------------------------------------------------------------------------------------
	    -- ADDED BY Sarim Ghani DT:08/07/08 TASK 4430
	    -- When ever court date and Status changed this count will reset to Zero --------
	    -- This field shows how many time we send request to court to reset his case.----
	    --------------------------------------------------------------------------------------
	    --Update tbltickets set ArrSetEmailCount = 0 where ticketid_pk = @ticketid_pk
	    UPDATE t
	    SET    t.ArrSetEmailCount = 0
	    FROM   tbltickets t
	           INNER JOIN INSERTED i
	                ON  i.ticketid_pk = t.ticketid_pk
	END
	
	
	--------------------------------------------------------------------------------------
	-- ADDED BY TAHIR AHMED DT:3/18/08 TASK 3464
	-- NEED TO ADD A NOTE IN CASE HISTORY IF TICKET NUMBER OR CAUSE NUMBER CHANGES....
	--------------------------------------------------------------------------------------
	
	-- checking for ticket number change.....
	IF EXISTS (
	       SELECT i.ticketsviolationid
	       FROM   INSERTED i,
	              DELETED d
	       WHERE  i.ticketsviolationid = d.ticketsviolationid
	              AND ISNULL(i.refcasenumber, '') <> ISNULL(d.refcasenumber, '')
	   )
	    INSERT INTO tblticketsnotes
	      (
	        ticketid,
	        SUBJECT,
	        employeeid
	      )
	    SELECT i.ticketid_pk,
	           'Ticket number changed from [ ' + UPPER(ISNULL(d.refcasenumber, 'N/A')) 
	           + ' ] to [ ' + UPPER(ISNULL(i.refcasenumber, 'N/A')) + ' ]',
	           i.vemployeeid
	    FROM   INSERTED i,
	           DELETED d
	    WHERE  i.ticketsviolationid = d.ticketsviolationid
	
	-- checking for cause number change.....
	IF EXISTS (
	       SELECT i.ticketsviolationid
	       FROM   INSERTED i,
	              DELETED d
	       WHERE  i.ticketsviolationid = d.ticketsviolationid
	              AND ISNULL(i.casenumassignedbycourt, '') <> ISNULL(d.casenumassignedbycourt, '')
	   )
	    INSERT INTO tblticketsnotes
	      (
	        ticketid,
	        SUBJECT,
	        employeeid
	      )
	    SELECT i.ticketid_pk,
	           'Cause number changed from [ ' + UPPER(ISNULL(d.casenumassignedbycourt, 'N/A')) 
	           + ' ] to [ ' + UPPER(ISNULL(i.casenumassignedbycourt, 'N/A')) +
	           ' ]',
	           i.vemployeeid
	    FROM   INSERTED i,
	           DELETED d
	    WHERE  i.ticketsviolationid = d.ticketsviolationid
	--------------------------------------------------------------------------------------
	
	-- Noufil 6523 10/08/2009 Update setdate when coutsetting information changes.	
	IF NOT EXISTS (
	       SELECT i.ticketsviolationid
	       FROM   DELETED d
	              INNER JOIN INSERTED i
	                   ON  i.ticketsviolationid = d.ticketsviolationid
	   )
	BEGIN
	    --Ozair 6523 10/08/2009 subquery changed to join
	    UPDATE t
	    SET    t.SettingDateChanged = GETDATE()
	    FROM   INSERTED i
	           INNER JOIN tbltickets t
	                ON  i.ticketid_pk = t.TicketID_PK
	END
	ELSE
	BEGIN
	    IF EXISTS (
	           SELECT i.ticketsviolationid
	           FROM   INSERTED i
	                  INNER JOIN DELETED d
	                       ON  i.ticketsviolationid = d.ticketsviolationid
	           WHERE  (
	                      i.CourtDateMain <> d.CourtDateMain
	                      OR i.CourtNumbermain <> d.CourtNumbermain
	                      OR i.CourtViolationStatusIDmain <> d.CourtViolationStatusIDmain
	                  )
	       )
	    BEGIN
	        --Ozair 6523 10/08/2009 subquery changed to join
	        UPDATE t
	        SET    t.SettingDateChanged = GETDATE()
	        FROM   INSERTED i
	               INNER JOIN tbltickets t
	                    ON  i.ticketid_pk = t.TicketID_PK                  
	    END
	END
	
	-- Noufil 8268 09/17/2010 Reset Jury Status when ever court setting information changes
	IF EXISTS (
	       SELECT i.ticketid_pk
	       FROM   INSERTED i
	              INNER JOIN DELETED d
	                   ON  i.ticketsviolationid = d.TicketsViolationId
	                   AND (
	                           ISNULL(i.CourtDateMain, '01/01/1900') <> ISNULL(d.CourtDateMain, '01/01/1900')
	                           OR ISNULL(i.CourtViolationStatusIDmain, 0) <> 
	                              ISNULL(d.CourtViolationStatusIDmain, 0)
	                           OR ISNULL(i.CourtNumbermain, 0) <> ISNULL(d.CourtNumbermain, 0)
	                       )
	   )
	BEGIN
	    UPDATE t
	    SET    t.jurystatus = 0,
	           t.employeeidupdate = i.vemployeeid
	    FROM   INSERTED i
	           INNER JOIN tbltickets t
	                ON  i.ticketid_pk = t.TicketID_PK
	END
	--Afaq 9521 07/25/2011 Need to hide record from continuance report when court date is changed
	IF EXISTS (
	       SELECT i.ticketid_pk
	       FROM   INSERTED i
	              INNER JOIN DELETED d
	                   ON  i.ticketsviolationid = d.TicketsViolationId
	                   AND (
	                           ISNULL(i.CourtDateMain, '01/01/1900') <> ISNULL(d.CourtDateMain, '01/01/1900')
	                       )
	              INNER JOIN tblTicketsFlag ttf ON ttf.TicketID_PK = i.ticketid_pk AND ttf.FlagID = 9 and isnull(ttf.showcontinuance,0) = 1
	   )
	BEGIN
	    UPDATE tblticketsflag
	    SET    showcontinuance = 0
	    WHERE  TicketID_PK = @ticketid_pk
	           AND FlagID = 9
	END
	
	-- Babar Ahmad 9223 10/07/2011 Check condition for auto court date in future by system user
	IF EXISTS( 
		       SELECT i.ticketsviolationid
			   FROM   INSERTED i
			   INNER JOIN DELETED d
			   ON  i.ticketsviolationid = d.ticketsviolationid
			   WHERE  (
						i.CourtDate <> d.CourtDate  -- Babar Ahmad 9223 10/07/2011 Filter for Auto Court Date
						AND i.vemployeeID = 3992 -- Babar Ahmad 9223 10/07/2011 Filter for system user
						AND DATEDIFF(DAY,i.CourtDate,GETDATE()) <0	-- Babar Ahmad 9223 10/07/2011 Filter for future auto date 								  
					  )	
	        )
	        BEGIN
	        	  UPDATE tv
	        	  SET tv.NoChangeInCourtSetting = 0  -- Reset the court setting flag
	        	  FROM tblticketsviolations tv 
	        	  INNER JOIN INSERTED i ON tv.TicketsViolationID = i.ticketsviolationid 
	        	  AND tv.ticketid_pk = i.ticketid_pk
	        	  INNER JOIN DELETED d
									   ON  i.ticketsviolationid = d.ticketsviolationid  
	        END



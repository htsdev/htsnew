
/****** Object:  Trigger [trg_tblTickets_Bondflagmonitor]    Script Date: 01/28/2008 13:06:33 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[trg_tblTickets_Bondflagmonitor]'))
DROP TRIGGER [dbo].[trg_tblTickets_Bondflagmonitor]

go


/****** Object:  Trigger [dbo].[trg_tblTickets_Bondflagmonitor]    Script Date: 01/28/2008 13:06:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE trigger [dbo].[trg_tblTickets_Bondflagmonitor] on [dbo].[tblTickets]  
for update  

AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

declare @dBondFlag   int 
declare @iBondFlag   int
declare @ticketid_pk  int 

select @iBondFlag = i.bondflag, @dBondFlag = d.bondflag, @ticketid_pk = i.ticketid_pk from inserted i inner join deleted d 
	on i.ticketid_pk = d.ticketid_pk

if @dBondFlag <> @iBondFlag 
begin
insert into tblbondmonitor values(@ticketid_pk, @dBondFlag, @iBondFlag , getdate())

--declare @subject varchar(2000)
--
--
--set @subject = 'Bond Flag changed' + convert(varchar(12),@ticketid_pk) 
--
--EXEC	msdb.dbo.sp_send_dbmail                         
--			@profile_name = 'Traffic System',                             
--			@recipients =  'sarim@lntechnologies.com;sarimg@hotmail.com' ,                            
--			@subject = @subject,                             
--			@body = 'Bond Flag changed',                            
--			@body_format = 'HTML' ; 
--
end



    -- Insert statements for trigger here

END

go
/************************************************************
 * Created By : Noufil Khan 
 * Created Date: 10/22/2010 11:28:27 AM
 * Business Logic :
 * Parameters :
 * Column Return :
 ************************************************************/

/*
* Business Logic : This trigger Use to Insert Notes in history and update required data 
*/
/****** Object:  Trigger [dbo].[trg_tblTickets_Events] ******/

ALTER TRIGGER [dbo].[trg_tblTickets_Events]
ON [dbo].[tblTickets]
FOR  INSERT, UPDATE
AS
	--------------------------------------------------------------
	-- VARIABLE DECLARATIONS......................................
	--------------------------------------------------------------  
	DECLARE @BondMain INT,
	        @ULBondYes INT,
	        @Active INT,
	        @DateType INT,
	        @d_DateType INT,	        
	        @BondFlag INT,
	        @CourtChange INT,
	        @oldbondflag TINYINT,
	        @employeeid INT,
	        @priceChange MONEY,
	        @strinitial VARCHAR(12),
	        @feeinitial VARCHAR(12),
	        @initialfeeinitial VARCHAR(12),
	        @contpricechange MONEY,
	        @statusdescription VARCHAR(100),
	        @changeactiveflag INT,
	        @coveringfirmid INT,
	        @newgeneralcomments VARCHAR(MAX),
	        @oldgeneralcomments VARCHAR(MAX),
	        @newtrialcomments VARCHAR(1000),
	        @oldtrialcomments VARCHAR(1000),
	        @newcontinuancecomments VARCHAR(2000),
	        @oldcontinuancecomments VARCHAR(2000),
	        @newsettingcomments VARCHAR(100),
	        @oldsettingcomments VARCHAR(100),
	        @newsalesemployeeid INT,
	        @oldsalesemployeeid INT,
	        @str VARCHAR(1000),
	        @ticketnumber VARCHAR(25),
	        @IsCDL INT,
	        @IsAccident INT,
	        @RecordId INT,
	        @Firstname VARCHAR(20),
	        @Lastname VARCHAR(20),
	        @Address1 VARCHAR(50),
	        @Address2 VARCHAR(20),
	        @Contact1 VARCHAR(15),
	        @oldContactType1 VARCHAR(15),
	        @ContactType1 VARCHAR(15),
	        @Contact2 VARCHAR(15),
	        @oldContactType2 VARCHAR(15),
	        @ContactType2 VARCHAR(15),
	        @Contact3 VARCHAR(15),
	        @oldContactType3 VARCHAR(15),
	        @ContactType3 VARCHAR(15),
	        @City VARCHAR(50),
	        @StateID INT,
	        @Zip VARCHAR(20) 
	
	--------------------------------------------------------------
	-- INITIALIZING VARIABLES........................................
	--------------------------------------------------------------  
	SELECT @contpricechange = i.continuanceamount - d.continuanceamount,
	       @priceChange = i.totalfeecharged - d.totalfeecharged,
	       @feeinitial = RTRIM(i.feeinitial),
	       @initialfeeinitial = i.initialAdjustmentinitial,
	       @changeactiveflag = CONVERT(
	           INT,
	           CONVERT(INT, i.activeflag) -CONVERT(INT, d.activeflag)
	       ),
	       @coveringfirmid = i.coveringfirmid,
	       @newgeneralcomments = i.generalcomments,
	       @newcontinuancecomments = i.ContinuanceComments,
	       @oldcontinuancecomments = d.ContinuanceComments,
	       @oldgeneralcomments = d.generalcomments,
	       @newtrialcomments = i.trialcomments,
	       @oldtrialcomments = d.trialcomments,
	       @newsettingcomments = i.SettingComments,
	       @oldsettingcomments = d.SettingComments,
	       @newsalesemployeeid = ISNULL(0, 0),	--Agha Usman 2664 06/04/2008
	       @oldsalesemployeeid = ISNULL(0, 0),
	       @employeeid = i.employeeidupdate,	      
	       @RecordId = i.recordid,
	       @Firstname = i.Firstname,
	       @Lastname = i.Lastname,
	       @Address1 = i.Address1,
	       @Address2 = i.Address2,
	       @Contact1 = i.Contact1,
	       @oldContactType1 = d.ContactType1,
	       @ContactType1 = i.ContactType1,
	       @Contact2 = i.Contact2,
	       @oldContactType2 = d.ContactType2,
	       @ContactType2 = i.ContactType2,
	       @Contact3 = i.Contact3,
	       @oldContactType3 = d.ContactType3,
	       @ContactType3 = i.ContactType3,
	       @City = i.City,
	       @StateID = i.Stateid_FK,
	       @Zip = i.Zip
	FROM   INSERTED i,
	       DELETED d
	WHERE  i.TicketID_PK = d.TicketID_PK  
	
	SELECT @RecordId = i.recordid
	FROM   INSERTED i 
	
	-------------------------------------------------------------------------------------
	-- UPDATE LOCK FLAG IF SPEEDING, ACC FLAG, CDL FLAG, BOND, PAYMENT PLAN QUESTION FLAG CHANGES.........
	-------------------------------------------------------------------------------------
	-----------------------Added By Zeeshan Ahmed On 1/4/2008----------------------------
	------------------Do Not Reset Lock Flag If Case Is Criminal Case--------------------
	-------------------------------------------------------------------------------------
	-- Noufil Khan 8136 10/21/2010 Remove function which check criminal court and clause added in IF EXISTS clause.
	IF EXISTS (
	       SELECT i.ticketid_pk
	       FROM   INSERTED i
	              INNER JOIN DELETED d
	                   ON  i.ticketid_pk = d.ticketid_pk
	              INNER JOIN tblticketsviolations tv
	                   ON  tv.TicketID_PK = i.ticketid_pk
	              INNER JOIN tblcourts tc
	                   ON  tv.CourtID = tc.CourtID
	       WHERE  (
	                  ISNULL(i.SpeedingID, 0) <> ISNULL(d.SpeedingID, 0)
	                  OR ISNULL(i.Accidentflag, 0) <> ISNULL(d.Accidentflag, 0)
	                  OR ISNULL(i.CDLflag, 0) <> ISNULL(d.CDLflag, 0)
	                  OR ISNULL(i.BondFlag, 0) <> ISNULL(d.BondFlag, 0)
	                  OR ISNULL(i.haspaymentplan, 0) <> ISNULL(d.haspaymentplan, 0)
	              )
	              AND tc.IsCriminalCourt = 0
	   )
	BEGIN
	    UPDATE t
	    SET    t.lockflag = 0,
	           -- Noufil Khan 8136 10/21/2010 Employee Id update check added.
	           t.EmployeeIDUpdate = i.EmployeeIDUpdate
	    FROM   tbltickets t
	           INNER JOIN INSERTED i
	                ON  t.ticketid_pk = i.ticketid_pk
	END 
	
	-- tahir 8311 09/23/2010 removed the bond update logic.	
	INSERT INTO tblticketsnotes
	  (
	    ticketid,
	    SUBJECT,
	    employeeid
	  )
	SELECT i.ticketid_pk,
	       'Bond Flag Set to: ' + CASE ISNULL(i.bondflag, 0)
	                                   WHEN 0 THEN 'NO'
	                                   ELSE 'YES'
	                              END,
	       i.employeeidupdate
	FROM   INSERTED i
	       INNER JOIN DELETED d
	            ON  i.ticketid_pk = d.ticketid_pk
	WHERE  ISNULL(i.bondflag, 0) <> ISNULL(d.bondflag, 0)
	
	-------------------------------------------------------------------------------
	-- INSERTING NOTE IN CASE HISTORY IF PRICE QUOTE CHANGES......
	-------------------------------------------------------------------------------
	-- Noufil Khan 8136 10/21/2010 Remove variable @priceChange with IF EXISTS clause
	IF EXISTS (
	       SELECT i.ticketid_pk
	       FROM   INSERTED i
	              INNER JOIN DELETED d
	                   ON  d.ticketid_pk = i.ticketid_pk
	       WHERE  ISNULL(i.totalfeecharged, 0) - ISNULL(d.totalfeecharged, 0) <> 
	              0
	   )
	BEGIN
	    INSERT INTO tblticketsnotes
	      (
	        ticketid,
	        SUBJECT,
	        employeeid
	      )
	    SELECT DISTINCT i.ticketid_pk,
	           'PRICE LOCKED $' + CONVERT(VARCHAR(10), CONVERT(INT, i.totalfeecharged)),
	           i.employeeidupdate
	    FROM   INSERTED i
	END 
	
	-------------------------------------------------------------------------------------
	--Sabir Khan 6583 09/16/2009
	--INSERTING A NOTE TO CASE HISTORY IF CDL FLAG IS CHANGED
	-------------------------------------------------------------------------------------
	IF EXISTS(
	       SELECT i.ticketid_pk
	       FROM   INSERTED i
	              INNER JOIN DELETED d
	                   ON  d.ticketid_pk = i.ticketid_pk
	                   AND ISNULL(i.cdlflag, 0) <> ISNULL(d.cdlflag, 0)
	       WHERE  ISNULL(i.cdlflag, 0) = 1
	   )
	BEGIN
	    INSERT INTO tblticketsnotes
	      (
	        TicketID,
	        SUBJECT,
	        EmployeeID
	      )
	    SELECT i.ticketid_pk,
	           'CDL flag added',
	           i.employeeidupdate
	    FROM   INSERTED i
	END
	
	IF EXISTS(
	       SELECT i.ticketid_pk
	       FROM   INSERTED i
	              INNER JOIN DELETED d
	                   ON  d.ticketid_pk = i.ticketid_pk
	                   AND ISNULL(i.cdlflag, 0) <> ISNULL(d.cdlflag, 0)
	                   AND i.cdlflag = 0
	   )
	BEGIN
	    INSERT INTO tblticketsnotes
	      (
	        TicketID,
	        SUBJECT,
	        EmployeeID
	      )
	    SELECT DISTINCT i.ticketid_pk,
	           'CDL flag removed',
	           i.employeeidupdate
	    FROM   INSERTED i
	END
	
	--INSERTING A NOTE TO CASE HISTORY IF ACCIDENT FLAG CHANGED
	IF EXISTS(
	       SELECT i.ticketid_pk
	       FROM   INSERTED i
	              INNER JOIN DELETED d
	                   ON  d.ticketid_pk = i.ticketid_pk
	                   AND ISNULL(i.accidentflag, 0) <> ISNULL(d.accidentflag, 0)
	       WHERE  ISNULL(i.accidentflag, 0) = 1
	   )
	BEGIN
	    INSERT INTO tblticketsnotes
	      (
	        TicketID,
	        SUBJECT,
	        EmployeeID
	      )
	    SELECT DISTINCT i.ticketid_pk,
	           'Accident flag added',
	           i.employeeidupdate
	    FROM   INSERTED i
	END
	
	IF EXISTS(
	       SELECT i.ticketid_pk
	       FROM   INSERTED i
	              INNER JOIN DELETED d
	                   ON  d.ticketid_pk = i.ticketid_pk
	                   AND ISNULL(i.accidentflag, 0) <> ISNULL(d.accidentflag, 0)
	                   AND i.accidentflag = 0
	   )
	BEGIN
	    INSERT INTO tblticketsnotes
	      (
	        TicketID,
	        SUBJECT,
	        EmployeeID
	      )
	    SELECT DISTINCT i.ticketid_pk,
	           'Accident flag removed',
	           i.employeeidupdate
	    FROM   INSERTED i
	END
	
	--INSERTING A NOTE TO CASE HISTORY IF LATE FEE FLAG CHANGED
	IF EXISTS(
	       SELECT i.ticketid_pk
	       FROM   INSERTED i
	              INNER JOIN DELETED d
	                   ON  d.ticketid_pk = i.ticketid_pk
	                   AND ISNULL(i.Lateflag, 0) <> ISNULL(d.Lateflag, 0)
	       WHERE  ISNULL(i.Lateflag, 0) = 1
	   )
	BEGIN
	    INSERT INTO tblticketsnotes
	      (
	        TicketID,
	        SUBJECT,
	        EmployeeID
	      )
	    SELECT DISTINCT i.ticketid_pk,
	           'Late fee of $' + CONVERT(VARCHAR, ts.amount) + ' updated',
	           i.employeeidupdate
	    FROM   INSERTED i
	           INNER JOIN tblSpeeding ts
	                ON  ts.SpeedingID_PK = 11
	END
	
	--INSERTING A NOTE TO CASE HISTORY IF SPEEDING DETIAL CHANGED
	IF EXISTS(
	       SELECT i.ticketid_pk
	       FROM   INSERTED i
	              INNER JOIN DELETED d
	                   ON  d.ticketid_pk = i.ticketid_pk
	                   AND ISNULL(i.SpeedingID, 0) <> ISNULL(d.SpeedingID, 0)
	   )
	BEGIN
	    INSERT INTO tblticketsnotes
	      (
	        TicketID,
	        SUBJECT,
	        EmployeeID
	      )
	    SELECT DISTINCT i.ticketid_pk,
	           'Speeding of ' + s.DESCRIPTION + ' updated',
	           i.employeeidupdate
	    FROM   INSERTED i
	           INNER JOIN tblSpeeding s
	                ON  i.SpeedingID = s.SpeedingID_PK
	END
	
	--INSERTING A NOTE TO CASE HISTORY IF INITIAL ADJUSMENT CHANGED
	IF EXISTS(
	       SELECT i.ticketid_pk
	       FROM   INSERTED i
	              INNER JOIN DELETED d
	                   ON  d.ticketid_pk = i.ticketid_pk
	                   AND ISNULL(i.InitialAdjustment, 0) <> ISNULL(d.InitialAdjustment, 0)
	   )
	BEGIN
	    INSERT INTO tblticketsnotes
	      (
	        TicketID,
	        SUBJECT,
	        EmployeeID
	      )
	    SELECT DISTINCT i.ticketid_pk,
	           'Initial Adjustment of $' + CONVERT(VARCHAR, i.InitialAdjustment) 
	           + ' updated',
	           i.employeeidupdate
	    FROM   INSERTED i
	END
	
	--INSERTING A NOTE TO CASE HISTORY IF FINAL ADJUSTMENT CHANGED
	IF EXISTS(
	       SELECT i.ticketid_pk
	       FROM   INSERTED i
	              INNER JOIN DELETED d
	                   ON  d.ticketid_pk = i.ticketid_pk
	                   AND ISNULL(i.Adjustment, 0) <> ISNULL(d.Adjustment, 0)
	   )
	BEGIN
	    INSERT INTO tblticketsnotes
	      (
	        TicketID,
	        SUBJECT,
	        EmployeeID
	      )
	    SELECT DISTINCT i.ticketid_pk,
	           'Final adjustment of $' + CONVERT(VARCHAR, i.Adjustment) + 
	           ' updated',
	           i.employeeidupdate
	    FROM   INSERTED i
	END
	
	-------------------------------------------------------------------------------
	-- INSERTING NOTE IN CASE HISTORY IF GENERAL COMMENT CHANGES......
	-------------------------------------------------------------------------------
	-- Noufil Khan 8136 10/21/2010 remove variables and use joins
	IF EXISTS (
	       SELECT i.ticketid_pk
	       FROM   INSERTED i
	              INNER JOIN DELETED d
	                   ON  i.ticketid_pk = d.ticketid_pk
	       WHERE  (
	                  ISNULL(d.generalcomments, '') <> ISNULL(i.generalcomments, '')
	              )
	              AND i.generalcomments IS NOT NULL
	   )
	BEGIN
	    UPDATE t
	    SET    t.LastGerenalcommentsUpdatedate = GETDATE(),
	           -- Noufil Khan 8136 10/21/2010 Employee Id update check added.
	           t.EmployeeIDUpdate = i.EmployeeIDUpdate
	    FROM   tbltickets t
	           INNER JOIN INSERTED i
	                ON  t.ticketid_pk = i.ticketid_pk
	END 
	
	-------------------------------------------------------------------------------
	-- INSERTING NOTE IN CASE HISTORY IF TRIAL COMMENT CHANGES......
	-------------------------------------------------------------------------------
	-- Noufil Khan 8136 10/21/2010 remove variable and use join clause with if exists
	IF EXISTS (
	       SELECT i.ticketid_pk
	       FROM   INSERTED i
	              INNER JOIN DELETED d
	                   ON  i.ticketid_pk = d.ticketid_pk
	       WHERE  (ISNULL(d.trialcomments, '') <> ISNULL(i.trialcomments, ''))
	              AND LEN(ISNULL(i.trialcomments, '')) = 0
	   )
	BEGIN
	    INSERT INTO tblticketsnotes
	      (
	        ticketid,
	        SUBJECT,
	        employeeid
	      )
	    SELECT i.ticketid_pk,
	           'Trial comments deleted',
	           i.employeeidupdate
	    FROM   INSERTED i
	END
	
	-------------------------------------------------------------------------------
	-- INSERTING NOTE IN CASE HISTORY IF SETTING COMMENT CHANGES......
	-------------------------------------------------------------------------------
	-- Noufil Khan 8136 10/21/2010 remove variable and use join clause with if exists	
	IF EXISTS (
	       SELECT i.ticketid_pk
	       FROM   INSERTED i
	              INNER JOIN DELETED d
	                   ON  i.ticketid_pk = d.ticketid_pk
	       WHERE  (
	                  ISNULL(d.SettingComments, '') <> ISNULL(i.SettingComments, '')
	              )
	              AND LEN(ISNULL(i.SettingComments, '')) > 0
	   )
	BEGIN
	    INSERT INTO tblticketsnotes
	      (
	        ticketid,
	        SUBJECT,
	        employeeid
	      )
	    SELECT i.ticketid_pk,
	           'Setting Notes:' + @newsettingcomments,
	           i.employeeidupdate
	    FROM   INSERTED i
	END 
	
	
	IF EXISTS (
	       SELECT i.ticketid_pk
	       FROM   INSERTED i
	              INNER JOIN DELETED d
	                   ON  i.ticketid_pk = d.ticketid_pk
	       WHERE  (
	                  ISNULL(d.SettingComments, '') <> ISNULL(i.SettingComments, '')
	              )
	              AND LEN(ISNULL(i.SettingComments, '')) = 0
	   )
	BEGIN
	    INSERT INTO tblticketsnotes
	      (
	        ticketid,
	        SUBJECT,
	        employeeid
	      )
	    SELECT i.ticketid_pk,
	           'Setting Comments deleted',
	           i.employeeidupdate
	    FROM   INSERTED i
	END
	
	-------------------------------------------------------------------------------
	-- INSERTING NOTE IN CASE HISTORY IF CONTINUANCE COMMENT CHANGES......
	-------------------------------------------------------------------------------
	IF EXISTS (
	       SELECT i.ticketid_pk
	       FROM   INSERTED i
	              INNER JOIN DELETED d
	                   ON  i.ticketid_pk = d.ticketid_pk
	       WHERE  (
	                  ISNULL(d.ContinuanceComments, '') <> ISNULL(i.ContinuanceComments, '')
	              )
	              AND LEN(ISNULL(i.ContinuanceComments, '')) = 0
	   )
	BEGIN
	    INSERT INTO tblticketsnotes
	      (
	        ticketid,
	        SUBJECT,
	        employeeid
	      )
	    SELECT i.ticketid_pk,
	           'Continuance comments deleted',
	           i.employeeidupdate
	    FROM   INSERTED i
	END
	
	
	--Muhammad Muneer 8247 09/22/2010 Email addition or upgradation is added in tblticketsnotes	
	--Sabir Khan 8247 12/23/2010 Changed the logic of email address note...
	IF EXISTS (
				SELECT i.ticketid_pk
				FROM   INSERTED i INNER JOIN DELETED d ON  i.ticketid_pk = d.ticketid_pk
				WHERE  (ISNULL(i.Email, '') <> ISNULL(d.Email, ''))
	   )
	BEGIN
	    INSERT INTO tblticketsnotes
	      (
	        ticketid,
	        SUBJECT,
	        employeeid
	      )
	      
	      -- Babar Ahmad 8247 05/23/2011 Space after [to: ] added similar to [from: ] text.
	    SELECT i.ticketid_pk,
				CASE ISNULL(d.Email,'') WHEN '' THEN 'Email Address Added:[' + ISNULL(i.Email, '') + ']'
				ELSE 'Email Address Updated from: [' + d.Email + ']' + ' to: ['+ CASE ISNULL(i.Email, '') WHEN '' THEN 'N/A'ELSE i.Email END + ']' END,	          
	           i.employeeidupdate
	    FROM   INSERTED i
	           INNER JOIN DELETED d
	                ON  d.ticketid_pk = i.ticketid_pk
	END 
	
	--Muhammad Muneer 8247 09/24/2010 Email addition is added in tblticketsnotes for the firt time
	IF NOT EXISTS (SELECT d.ticketid_pk FROM   DELETED d)
	BEGIN
	    INSERT INTO tblticketsnotes
	      (
	        ticketid,
	        SUBJECT,
	        employeeid
	      )
	    SELECT i.ticketid_pk,
	           'Email Address Added:[' + ISNULL(i.Email, '') + ']',
	           i.employeeidupdate
	    FROM   INSERTED i
	    WHERE  LEN(ISNULL(i.Email, '')) > 0
	END 
	
	
	-------------------------------------------------------------------------------
	-- SEND EMAILS TO ATTORNEY IF CLIENT SIGNUP......
	-------------------------------------------------------------------------------  
	
	IF EXISTS (
				 SELECT i.ticketid_pk FROM INSERTED i
	             INNER JOIN DELETED d ON  i.ticketid_pk = d.ticketid_pk
				 WHERE  CONVERT(INT,CONVERT(INT, i.activeflag) - CONVERT(INT, d.activeflag)) = 1
			  )
	BEGIN		
		DECLARE @insertedCount INT, @ticketId INT
		SET @insertedCount = (SELECT COUNT(i.ticketid_pk) FROM INSERTED i)
		DECLARE @ticketsTable TABLE (Id int IDENTITY(1,1),ticketid_pk INT)
		
		INSERT INTO @ticketsTable 
		select i.ticketid_pk FROM INSERTED i
		
		WHILE (@insertedCount > 0)
		BEGIN
			SET @ticketId = (SELECT ticketid_pk FROM @ticketsTable i WHERE i.Id= @insertedCount)
			EXEC ('sp_SendClientSignUpNotficationEmails ' + @ticketId)
			SET @insertedCount = @insertedCount - 1	
		END
	    
	END
	
	-------------------------------------------------------------------------------
	-- INSERTING A NOTE TO CASE HISTORY IF CLIENT DATA CHANGES ......
	-------------------------------------------------------------------------------
	-- INSERTING A NOTE TO CASE HISTORY IF NAME IS CHANGED
	IF EXISTS (
				  SELECT i.ticketid_pk FROM INSERTED i
	              INNER JOIN DELETED d ON  i.ticketid_pk = d.ticketid_pk
				  WHERE  (ISNULL(i.firstname, '') <> ISNULL(d.firstname, '')OR ISNULL(i.lastname, '') <> ISNULL(d.lastname, ''))
			  )
	BEGIN
	    INSERT INTO tblticketsnotes
	      (
	        ticketid,
	        SUBJECT,
	        employeeid
	      )
	    SELECT i.ticketid_pk,
	           'Name changed - From:[' + ISNULL(d.lastname, '') + ', ' + ISNULL(d.firstname, '') 
	           + '] To:[' 
	           + ISNULL(i.lastname, '') + ' ,' + ISNULL(i.firstname, '') + ']',
	           i.employeeidupdate
	    FROM   INSERTED i
	    INNER JOIN DELETED d ON  i.ticketid_pk = d.ticketid_pk
	END
	
	
	--INSERTING A NOTE TO CASE HISTORY IF ADDRESS1 IS CHANGED
	IF EXISTS (
				 SELECT i.ticketid_pk FROM INSERTED i
	             INNER JOIN DELETED d ON i.ticketid_pk = d.ticketid_pk
				 WHERE  (ISNULL(i.address1, '') <> ISNULL(d.address1, ''))
			  )
	BEGIN
	    INSERT INTO tblticketsnotes
	      (
	        ticketid,
	        SUBJECT,
	        employeeid
	      )
	    SELECT i.ticketid_pk,
	           'Address1 changed - From:[' + ISNULL(d.address1, '') + '] To:[' + 
	           ISNULL(i.address1, '') + ']',
	           i.employeeidupdate
	    FROM   INSERTED i
	    INNER JOIN DELETED d ON  d.ticketid_pk = i.ticketid_pk
	END
	
	--INSERTING A NOTE TO CASE HISTORY IF ADDRESS2 IS CHANGED
	IF EXISTS (
				 SELECT i.ticketid_pk FROM INSERTED i
	             INNER JOIN DELETED d ON  i.ticketid_pk = d.ticketid_pk
				 WHERE (ISNULL(i.address2, '') <> ISNULL(d.address2, ''))
			  )
	BEGIN
	    INSERT INTO tblticketsnotes
	      (
	        ticketid,
	        SUBJECT,
	        employeeid
	      )
	    SELECT i.ticketid_pk,
	           'Address2 changed - From:[' + ISNULL(d.address2, '') + '] To:[' + ISNULL(i.address2, '') + ']',
	           i.employeeidupdate
	    FROM   INSERTED i
	    INNER JOIN DELETED d ON  d.ticketid_pk = i.ticketid_pk
	END
	
	--INSERTING A NOTE TO CASE HISTORY IF CONTACT NUMBER 1 IS CHANGED
	IF EXISTS (
				SELECT i.ticketid_pk FROM   INSERTED i
				INNER JOIN DELETED d ON  i.ticketid_pk = d.ticketid_pk
				WHERE (ISNULL(i.contact1, '') <> ISNULL(d.contact1, '')
						OR ISNULL(i.contacttype1, 0) <> ISNULL(d.contacttype1, 0))
			  )
	BEGIN
	    INSERT INTO tblticketsnotes
	      (
	        ticketid,
	        SUBJECT,
	        employeeid
	      )
	    SELECT i.ticketid_pk,
	           'Contact Number 1 changed - From:[' + dbo.fn_FormatContactNumber(ISNULL(d.contact1, ''), ISNULL(d.contacttype1, 0))
	           + '] To:[' + dbo.fn_FormatContactNumber(ISNULL(i.contact1, ''), ISNULL(i.contacttype1, 0)) + ']',
	           i.employeeidupdate
	    FROM   INSERTED i
	    INNER JOIN DELETED d ON  i.ticketid_pk = d.ticketid_pk
	END
	
	--INSERTING A NOTE TO CASE HISTORY IF CONTACT NUMBER 2 IS CHANGED
	IF EXISTS (
				SELECT i.ticketid_pk FROM INSERTED i
	            INNER JOIN DELETED d ON  i.ticketid_pk = d.ticketid_pk
				WHERE  (ISNULL(i.contact2, '') <> ISNULL(d.contact2, '') OR ISNULL(i.contacttype2, 0) <> ISNULL(d.contacttype2, 0))
			  )
	BEGIN
	    INSERT INTO tblticketsnotes
	      (
	        ticketid,
	        SUBJECT,
	        employeeid
	      )
	    SELECT i.ticketid_pk,
	           'Contact Number 2 changed - From:[' + dbo.fn_FormatContactNumber(ISNULL(d.contact2, ''), ISNULL(d.contacttype2, 0))
	           + '] To:[' + dbo.fn_FormatContactNumber(ISNULL(i.contact2, ''), ISNULL(i.contacttype2, 0)) 
	           + ']',
	           i.employeeidupdate
	    FROM   INSERTED i
	           INNER JOIN DELETED d
	                ON  i.ticketid_pk = d.ticketid_pk
	END
	
	--INSERTING A NOTE TO CASE HISTORY IF CONTACT NUMBER 3 IS CHANGED
	IF EXISTS (
				SELECT i.ticketid_pk FROM INSERTED i
				INNER JOIN DELETED d ON i.ticketid_pk = d.ticketid_pk
				WHERE (ISNULL(i.contact3, '') <> ISNULL(d.contact3, '') 
						OR ISNULL(i.contacttype3, 0) <> ISNULL(d.contacttype3, 0))
			  )
	BEGIN
	    INSERT INTO tblticketsnotes
	      (
	        ticketid,
	        SUBJECT,
	        employeeid
	      )
	    SELECT i.ticketid_pk,
	           'Contact Number 3 changed - From:[' + dbo.fn_FormatContactNumber(ISNULL(d.contact3, ''), ISNULL(d.contacttype3, 0))+ '] To:[' 
	           + dbo.fn_FormatContactNumber(ISNULL(i.contact3, ''), ISNULL(i.contacttype3, 0))+ ']',
	           i.employeeidupdate
	    FROM   INSERTED i
	    INNER JOIN DELETED d ON  i.ticketid_pk = d.ticketid_pk
	END
	
	--INSERTING A NOTE TO CASE HISTORY IF CITY IS CHANGED
	IF EXISTS (
			    SELECT i.ticketid_pk FROM INSERTED i
			    INNER JOIN DELETED d ON  i.ticketid_pk = d.ticketid_pk
			    WHERE  (ISNULL(i.city, '') <> ISNULL(d.city, ''))
			  )
	BEGIN
	    INSERT INTO tblticketsnotes
	      (
	        ticketid,
	        SUBJECT,
	        employeeid
	      )
	    SELECT i.ticketid_pk,
	           'City changed - From:[' + ISNULL(d.city, '') + '] To:[' + ISNULL(i.city, '') + ']',
	           i.employeeidupdate
	    FROM   INSERTED i
	    INNER JOIN DELETED d ON  d.ticketid_pk = i.ticketid_pk
	END
	
	--INSERTING A NOTE TO CASE HISTORY IF STATE IS CHANGED
	IF EXISTS (
				SELECT i.ticketid_pk FROM INSERTED i
	            INNER JOIN DELETED d ON  i.ticketid_pk = d.ticketid_pk
				WHERE  ISNULL(i.stateid_fk, 0) <> ISNULL(d.stateid_fk, 0)
			  )
	BEGIN
	    INSERT INTO tblticketsnotes
	      (
	        ticketid,
	        SUBJECT,
	        employeeid
	      )
	    SELECT i.ticketid_pk,
	           'State changed - From:[' + ISNULL(s2.state, '') + '] To:[' + 
	           ISNULL(s1.state, '') + ']',
	           i.employeeidupdate
	    FROM   tblState AS s1
	           RIGHT OUTER JOIN DELETED AS d
	           INNER JOIN INSERTED AS i ON  d.ticketid_pk = i.ticketid_pk ON  s1.StateID = i.StateID_FK
	           LEFT OUTER JOIN tblState AS s2 ON  d.Stateid_FK = s2.StateID
	END
	
	--INSERTING A NOTE TO CASE HISTORY IF ZIP IS CHANGED
	IF EXISTS (
				SELECT i.ticketid_pk FROM INSERTED i
	            INNER JOIN DELETED d ON  i.ticketid_pk = d.ticketid_pk
				WHERE (ISNULL(i.zip, '') <> ISNULL(d.zip, ''))
			  )
	BEGIN
	    INSERT INTO tblticketsnotes
	      (
	        ticketid,
	        SUBJECT,
	        employeeid
	      )
	    SELECT i.ticketid_pk,
	           'ZIP changed - From:[' + ISNULL(d.zip, '') + '] To:[' + ISNULL(i.zip, '') + ']',
	           i.employeeidupdate
	    FROM   INSERTED i
	           INNER JOIN DELETED d ON  d.ticketid_pk = i.ticketid_pk
	END
	
	-------------------------------------------------------------------------------
	-- Update at 01/15/2008//Task 2598/ REMOVE NO BOND DISCREPANCY FLAG IF EXIST WHEN MAIN BOND FLAG IS CHANGED......
	-------------------------------------------------------------------------------
	-- Noufil Khan 8136 10/21/2010 Remove variables and clause added in IF EXISTS clause.
	--check for bond flag change
	IF EXISTS (
				SELECT i.ticketid_pk FROM INSERTED i
	            INNER JOIN DELETED d ON  i.ticketid_pk = d.ticketid_pk AND ISNULL(i.bondflag, 0) <> ISNULL(d.bondflag, 0)
				INNER JOIN tblticketsflag f ON f.TicketID_PK = i.ticketid_pk AND f.FlagID = 32
			  )
	BEGIN
	        -- removing no bond discrepancy flag
	        DELETE FROM tblticketsflag 
	        FROM tblTicketsFlag f 
	        INNER JOIN INSERTED i ON i.ticketid_pk = f.TicketID_PK	        
            AND f.flagid = 32
	        
	        -- inserting notes
	        INSERT INTO tblticketsnotes
	          (
	            ticketid,
	            SUBJECT,
	            employeeid
	          )
	           SELECT i.ticketid_pk,
	           'Flag - No Bond Discrepancy - Removed',
	           i.employeeidupdate
			   FROM   INSERTED i
	           INNER JOIN DELETED d ON  d.ticketid_pk = i.ticketid_pk
	END
	-----------------------------------END---------------------------------------------
	
	--Yasir Kamal 6430 11/16/2009 code moved into trigger from sp and covering firm note bug fixed.	
	-- checking if payment is made for the first time for Family law case type. 
	IF EXISTS (
					SELECT i.ticketid_pk FROM INSERTED i
	                INNER JOIN DELETED d ON  d.ticketid_pk = i.ticketid_pk
					WHERE  ISNULL(i.activeflag, 0) <> ISNULL(d.activeflag, 0)
					AND ISNULL(i.activeflag, 0) = 1
					AND ISNULL(i.casetypeid, 0) = 4
			  )
	BEGIN
	    -- Add note into case history for each violation whose current covering firm is other than sull.
	    INSERT INTO tblticketsnotes
	      (
	        TicketID,
	        [Subject],
	        EmployeeID
	      )
	    SELECT i.ticketid_pk,
	           ISNULL(v.casenumassignedbycourt, v.refcasenumber) + 
	           ' - Firm Changed from [' + ISNULL(f1.firmname, 'N/A') + '] to ['+ ISNULL(f2.firmname, 'N/A') + ']',
	           i.employeeidupdate
	    FROM   INSERTED i
	           INNER JOIN tblticketsviolations v ON  v.TicketID_PK = i.ticketid_pk AND v.CoveringFirmID <> 3000
	           LEFT OUTER JOIN tblfirm f1 ON  f1.firmid = v.CoveringFirmID
	           LEFT OUTER JOIN tblfirm f2 ON  f2.firmid = 3000 
	    
	    -- tahir 8311 09/24/10 fixed dead lock issues;
	    -- update coverage firm to Sull if current covering firm is other than Sull.
	    UPDATE v
	    SET    v.coveringfirmid = 3000,
	           v.vemployeeid = i.employeeidupdate
	    FROM   tblticketsviolations v
	           INNER JOIN tblticketsviolations v2 ON  v2.TicketsViolationID = v.ticketsviolationid
	           INNER JOIN INSERTED i ON  i.ticketid_pk = v2.ticketid_pk
	    WHERE  ISNULL(v.coveringfirmid, 0) <> 3000
	END
	
	-- Noufil 8089 08/02/2010
	-- Add note to history if mailer id is updated and new mailer id is not (null or zero). Old and new mailer id's must also not equal.
	IF EXISTS (
				SELECT i.ticketid_pk FROM INSERTED i
				INNER JOIN DELETED d ON  i.ticketid_pk = d.ticketid_pk
				WHERE  ISNULL(i.mailerid, 0) <> ISNULL(d.mailerid, 0) AND ISNULL(i.mailerid, 0) > 0
			  )
	BEGIN
	    -- Add Updated note to history
	    INSERT INTO tblTicketsNotes
	      (
	        TicketID,
	        [Subject],
	        EmployeeID
	      )
	    SELECT i.ticketid_pk,
	           'LID updated [' + ISNULL(CONVERT(VARCHAR, i.mailerid), 'N/A') + '] - [' 
	           + ISNULL(c.CourtCategoryName, '') + ' - ' + ISNULL(tl.LetterName, 'N/A') + ']',
	           i.employeeidupdate
	    FROM   INSERTED i
	           LEFT OUTER JOIN tblLetterNotes tln ON  tln.NoteId = i.mailerid
	           LEFT OUTER JOIN tblLetter tl ON  tl.LetterID_PK = tln.LetterType
	           INNER JOIN tblCourtCategories c ON  c.CourtCategorynum = tl.courtcategory
	END
	
	
	-- -- Add note to history if new mailer id is updated to (null or zero). Old and new mailer id's must also not equal.
	IF EXISTS (
				SELECT i.ticketid_pk FROM INSERTED i
	            INNER JOIN DELETED d ON  i.ticketid_pk = d.ticketid_pk
				WHERE ISNULL(i.mailerid, 0) <> ISNULL(d.mailerid, 0) AND ISNULL(i.mailerid, 0) = 0
			  )
	BEGIN
	    -- Add deleted note to history
	    INSERT INTO tblTicketsNotes
	      (
	        TicketID,
	        [Subject],
	        EmployeeID
	      )
	    SELECT d.ticketid_pk,
	           'LID deleted [' + ISNULL(CONVERT(VARCHAR, d.mailerid), 'N/A') + '] - [' 
	           + ISNULL(c.CourtCategoryName, '') + ' - ' + ISNULL(tl.LetterName, 'N/A') + ']',
	           d.employeeidupdate
	    FROM   DELETED d
	           LEFT OUTER JOIN tblLetterNotes tln ON  tln.NoteId = d.mailerid
	           LEFT OUTER JOIN tblLetter tl ON  tl.LetterID_PK = tln.LetterType
	           INNER JOIN tblCourtCategories c ON  c.CourtCategorynum = tl.courtcategory
	END
	
	
	-- Add note to history if mailer id is inserted for the first time.
	IF NOT EXISTS (SELECT d.ticketid_pk FROM DELETED d)
	BEGIN
	    -- Add associated note to history
	    INSERT INTO tblTicketsNotes
	      (
	        TicketID,
	        [Subject],
	        EmployeeID
	      )
	    SELECT i.ticketid_pk,
	           'LID associated [' + (CASE ISNULL(i.mailerid, 0) WHEN 0 THEN 'N/A' ELSE CONVERT(VARCHAR, i.mailerid)END) + '] - [' 
	           + ISNULL(c.CourtCategoryName, '') + ' - ' + ISNULL(tl.LetterName, 'N/A') + ']',
	           i.employeeidupdate
	    FROM   INSERTED i
	           LEFT OUTER JOIN tblLetterNotes tln ON  tln.NoteId = i.mailerid
	           LEFT OUTER JOIN tblLetter tl ON  tl.LetterID_PK = tln.LetterType
	           INNER JOIN tblCourtCategories c ON  c.CourtCategorynum = tl.courtcategory
	END
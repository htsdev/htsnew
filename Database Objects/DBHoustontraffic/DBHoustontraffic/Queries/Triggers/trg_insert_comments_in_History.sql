/****** Object:  Trigger [trg_insert_comments_in_History]    Script Date: 01/28/2008 13:03:36 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[trg_insert_comments_in_History]'))
DROP TRIGGER [dbo].[trg_insert_comments_in_History]

go

USE [TrafficTickets]
GO
/****** Object:  Trigger [dbo].[trg_insert_comments_in_History]    Script Date: 01/28/2008 13:03:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Sarim Shekhani
-- Create date: <5.5.2007>

-- =============================================
CREATE TRIGGER [dbo].[trg_insert_comments_in_History]
   ON [dbo].[tblHTSNotes]
for 
INSERT
AS 
BEGIN
	
	SET NOCOUNT ON;
		
declare @docid int
declare @letterid int
declare @TempDOCID varchar(12)

select 	@letterid = i.letterid_fk from inserted i 
  


if @letterid = 10 
	begin 
		
			select top 1 @docid = twl.batchid from tblwaitinglogs twl, inserted i 

			where  

			i.ticketid_fk = twl.ticketid and

			right(left(i.docpath,len(i.docpath)-4), len(left(i.docpath,len(i.docpath)-4))-6) = twl.batchid and

			i.letterid_fk = 10
			
	
/*		select top 1 @docid = twl.batchid from tblwaitinglogs twl inner join 
				tblbatchsummary bs on 
			twl.batchid=bs.batchid inner join inserted i 
			on i.ticketid_fk=twl.ticketid where bs.docpicflag=1	
			
*/			
	

	    
	set @TempDOCID=Convert(varchar(12),@docid)
		
		insert into tblTicketsNotes (TicketID,Subject,EmployeeID)    
			select
				i.ticketid_fk,
				'<a href="javascript:window.open(''frmPreview.aspx?RecordID='+Convert(varchar(12),i.RecordID)+''');void('''');"''>Document Scanned DOC ID - ' + @TempDOCID +'</a>', 
				i.empid
			from inserted i	
	end		
END





go

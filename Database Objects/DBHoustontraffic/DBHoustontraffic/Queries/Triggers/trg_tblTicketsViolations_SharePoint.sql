-- =============================================
-- Author:	Aziz ur Rahman
-- =============================================
CREATE TRIGGER [dbo].[trg_tblTicketsViolations_SharePoint] ON [dbo].[tblTicketsViolations] 
FOR  UPDATE 



AS 
declare	@NewCourtDateMain datetime
declare @OldCourtDateMain datetime
declare @TicketsViolationID int

select  @NewCourtDateMain = i.CourtDateMain,
		@OldCourtDateMain = d.CourtDateMain,
		@TicketsViolationID = i.TicketsViolationID
from 	inserted i, deleted d
where 	i.TicketsViolationID = d.TicketsViolationID


if @OldCourtDateMain <> @NewCourtDateMain
begin	
		update [tblTicketsViolations]
		set SharePointStatusId =2 
		where TicketsViolationID = @TicketsViolationID		
end

--GO
--GRANT EXEC ON trg_tblTicketsViolations_SharePoint TO dbr_webuser
--GO


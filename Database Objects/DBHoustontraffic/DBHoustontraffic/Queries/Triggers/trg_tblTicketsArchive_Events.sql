-- =============================================
-- Author:	Yasir Kamal
-- Create date: 08/13/2009
-- Task ID: 6170
-- Description:	set Current date in DoNotMailUpdateDate when DoNotMailFlag is set to yes
-- =============================================
ALTER TRIGGER [trg_tblTicketsArchive_Events] ON [dbo].[tblTicketsArchive] 
  FOR  UPDATE 
AS 
if exists (
	select 	i.RecordID
	--Yasir Kamal 6927 11/04/2009 do not mail date bug fixed. 
	from	inserted i INNER join deleted d 
	on	i.RecordID = d.RecordID
	and	i.donotmailflag = 1
	AND ISNULL(d.donotmailflag,0) = 0 
	AND DATEDIFF ( DAY, '1/1/1900', ISNULL(d.donotmailupdatedate, '1/1/1900') )= 0
    )
	update	t 
	set 	t.DoNotMailUpdateDate=  GETDATE()
	from tblTicketsArchive t inner join inserted i	
	on t.RecordID = i.RecordID



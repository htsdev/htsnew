using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;
using lntechNew.WebControls;
//Waqas 5057 03/19/2009 Changed to HTP
namespace HTP.Reports
{
    public partial class AppearanceSummary : System.Web.UI.Page
    {
        clsSession ClsSession = new clsSession();
        clsLogger bugTracker = new clsLogger();
        clsENationWebComponents clsdb = new clsENationWebComponents();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Waqas 5057 03/17/2009 Checking employee info in session
                if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else //To stop page further execution
                {
                    if (!IsPostBack)
                    {
                        BindGrid();
                        lbl_CurrDateTime.Text = String.Concat("Date: ", DateTime.Now.ToString());
                    }

                }
                HPARRSumm.Attributes.Add("onclick", "OpenSummary('frmAppearance.aspx');");
            }
            catch (Exception ex)
            {

                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void BindGrid()
        {
            try
            {
                DataSet DS = clsdb.Get_DS_BySP("[USP_HTS_APPEARANCE_REPORT]");
                if (DS.Tables[0].Rows.Count > 0)
                {
                    DG.DataSource = DS;
                    DG.DataBind();
                    GenerateSerialNo();
                    hp.Visible = true;
                    HPARRSumm.Visible = true;
                }
                else
                {
                    DG.DataSource = DS;
                    DG.DataBind();

                }
            }
            catch (Exception ex)
            {

                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        private void GenerateSerialNo()
        {
            try
            {
                int no = 1;
                string ticketid = "";
                foreach (DataGridItem ItemX in DG.Items)
                {
                    HyperLink sno = (HyperLink)ItemX.FindControl("lblSNo");
                    Label tid = (Label)ItemX.FindControl("ASTIDS");
                    if (tid.Text != "")
                    {
                        if (no == 1)
                        {
                            sno.Text = no.ToString();
                            ticketid = tid.Text;
                            no = no + 1;
                        }
                        else
                        {
                            if (ticketid != tid.Text)
                            {
                                sno.Text = Convert.ToString(no);
                                ticketid = tid.Text;
                                no = no + 1;
                            }
                            else
                            {
                                sno.Text = "";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;
            }
        }

        protected void DG_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {

                    string ticketid = ((Label)e.Item.FindControl("ASTIDS")).Text.ToString();
                    ((HyperLink)e.Item.FindControl("lblSNo")).NavigateUrl = "../clientinfo/ViolationFeeold.aspx?search=0&caseNumber=" + ticketid;
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }


    }
}

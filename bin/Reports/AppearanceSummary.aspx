<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AppearanceSummary.aspx.cs" Inherits="HTP.Reports.AppearanceSummary" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Appearance Summary</title>
    <LINK href="../Styles.css" type="text/css" rel="stylesheet">
  <script language="javascript">
        function OpenSummary(path)
		{
		    
		    window.open(path,'',"fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=yes");
		    
		}
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
         <table border="0" cellpadding="0" cellspacing="0" width="780" align="center">
        <tr>
            <td>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <TR>
						<TD ><uc1:activemenu id="ActiveMenu1" runat="server"></uc1:activemenu></TD>
					</TR>
                    <tr>
                        <td>
                            <asp:label id="lbl_CurrDateTime" runat="server" Font-Names="Times New Roman" Font-Bold="True"></asp:label>
                        </td>
                    </tr>
                    
                    <TR>
						    <TD align="center"><asp:label id="lbl_Message" runat="server" CssClass="Label" Font-Bold="True" ForeColor="Green"
								    Visible="true"></asp:label></TD>
					    </TR>
					    <TR >
						    <TD style="HEIGHT: 9px" width="100%" background="../../images/separator_repeat.gif" colSpan="5">
						    </TD>
					    </TR>
					    <TR>
						    <TD align="right">
                                <asp:HyperLink ID="hp" runat="server"  Visible="False" NavigateUrl="~/Reports/PrintHistory.aspx">Show Print History</asp:HyperLink><asp:Label
                                    ID="lblSep" runat="server" ForeColor="Blue" Text="|" Visible="true"></asp:Label>&nbsp;<asp:HyperLink
                                        ID="HPARRSumm" runat="server" NavigateUrl="#" Visible="False">Print All</asp:HyperLink>
                                
                                
							    
                                
                               
						    </TD>
					    </TR>
					    <TR >
						    <TD style="HEIGHT: 9px" width="100%" background="../../images/separator_repeat.gif" colSpan="5"
							    height="9">
						    </TD>
					    </TR>
					     <tr>
				       <td id="Td1" width="780"><STRONG>HMC Appearances</STRONG></td>
				    </tr>
				    <TR >
						    <TD style="HEIGHT: 9px" width="100%" background="../../images/separator_repeat.gif" colSpan="5">
						    </TD>
					    </TR>
                    <tr>
                        <td>
                            <asp:datagrid id="DG" runat="server" AutoGenerateColumns="False"
											    BorderColor="Black" BorderStyle="Solid" Width="780px" OnItemDataBound="DG_ItemDataBound" >
										<ItemStyle Font-Names="Times New Roman"></ItemStyle>
											    <HeaderStyle ForeColor="Black" BackColor="#FFCC00"></HeaderStyle>   
							    <Columns>
							    <asp:TemplateColumn HeaderText="ticketviolationid" Visible="False">
									    <ItemTemplate>
										    <asp:Label id="ASTVIDS" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ticketsviolationid") %>' >
										    </asp:Label>
									    </ItemTemplate>
								    </asp:TemplateColumn>
							    
							    <asp:TemplateColumn HeaderText="ticketid" Visible="False" >
									    <ItemTemplate>
										    <asp:Label id="ASTIDS" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ticketid") %>' >
										    </asp:Label>
									    </ItemTemplate>
								    </asp:TemplateColumn>
								    <asp:TemplateColumn HeaderText="NO" >
								    
								             <HeaderStyle Font-Names="Times New Roman" Font-Bold="True" ></HeaderStyle>
													    <ItemStyle Font-Names="Times New Roman"></ItemStyle>
									    <ItemTemplate>
										    <asp:HyperLink id="lblSNo" runat="server" NavigateUrl="#" Text='<%# DataBinder.Eval(Container, "DataItem.FLAGID") %>'></asp:HyperLink>
									    </ItemTemplate>
								    </asp:TemplateColumn>
                                    
                                    <asp:TemplateColumn HeaderText="Flag">
                                         <HeaderStyle Font-Names="Times New Roman" Font-Bold="True" ></HeaderStyle>
													    <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblflag" Text='<%# DataBinder.Eval(Container, "DataItem.FLAGID") %>'></asp:Label>
                                        </ItemTemplate>                                        
                                    </asp:TemplateColumn>
                                    
								    <asp:TemplateColumn HeaderText="TICKET NO" >
								             <HeaderStyle Font-Names="Times New Roman" Font-Bold="True" ></HeaderStyle>
													    <ItemStyle Font-Names="Times New Roman"></ItemStyle>
									    <ItemTemplate>
										    <asp:Label id="Asticketno" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TICKETNO") %>' >
										    </asp:Label>
									    </ItemTemplate>
								    </asp:TemplateColumn>
								    <asp:TemplateColumn HeaderText="LAST NAME" >
								         <HeaderStyle Font-Names="Times New Roman" Font-Bold="True" ></HeaderStyle>
													    <ItemStyle Font-Names="Times New Roman"></ItemStyle>
									    <ItemTemplate>
										    <asp:Label id="ASlastname" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.lastname") %>'>
										    </asp:Label>
									    </ItemTemplate>
								    </asp:TemplateColumn>
								     <asp:TemplateColumn HeaderText="FIRST NAME" >
								             <HeaderStyle Font-Names="Times New Roman" Font-Bold="True" ></HeaderStyle>
													    <ItemStyle Font-Names="Times New Roman"></ItemStyle>
									    <ItemTemplate>
										    <asp:Label id="ASfirstname" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.firstname") %>'>
										    </asp:Label>
									    </ItemTemplate>
								    </asp:TemplateColumn>
								    
								    <asp:TemplateColumn HeaderText="DOB" >
								             <HeaderStyle Font-Names="Times New Roman" Font-Bold="True" ></HeaderStyle>
													    <ItemStyle Font-Names="Times New Roman"></ItemStyle>
									    <ItemTemplate>
										    <asp:Label id="ASDOB" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DOB") %>'>
										    </asp:Label>
									    </ItemTemplate>
								    </asp:TemplateColumn>
								    <asp:TemplateColumn HeaderText="Status" >
								             <HeaderStyle Font-Names="Times New Roman" Font-Bold="True" ></HeaderStyle>
													    <ItemStyle Font-Names="Times New Roman"></ItemStyle>
									    <ItemTemplate>
										    <asp:Label id="ASStatus" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.status") %>'>
										    </asp:Label>
									    </ItemTemplate>
								    </asp:TemplateColumn>
								    <asp:TemplateColumn HeaderText="CRT D" >
								             <HeaderStyle Font-Names="Times New Roman" Font-Bold="True" ></HeaderStyle>
													    <ItemStyle Font-Names="Times New Roman"></ItemStyle>
									    <ItemTemplate>
										    <asp:Label id="ASCourtDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.COURTDATE") %>'>
										    </asp:Label>
									    </ItemTemplate>
								    </asp:TemplateColumn>
							    </Columns>
											    
											    
											    </asp:datagrid>
                        
                        </td>
                    </tr>
                    <tr>
									<td background="../images/separator_repeat.gif" height="11"></td>
								</tr>
								<TR>
									<TD ><uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
								</TR>
                </table>
            
            </td>
            
        </tr>
    
    </table>
    </div>
    </form>
</body>
</html>

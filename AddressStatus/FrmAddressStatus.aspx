<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Page language="c#" AutoEventWireup="false" Inherits="AddressStatus.WebForm1" Codebehind="FrmAddressStatus.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="Footer" src="~/WebControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Address Status Report </title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<SCRIPT src="../Scripts/Dates.js" type="text/javascript"></SCRIPT>
		<SCRIPT src="../Scripts/jsDate.js" type="text/javascript"></SCRIPT>
		<SCRIPT src="../Scripts/cBoxes.js" type="text/javascript"></SCRIPT>
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<script>
		
			function BindDate()
			{
				if (document.Form1.dMonth.value!=1 && document.Form1.dDay.value!=0 && document.Form1.dYear.value!=0)				
				{
					if (Len(document.Form1.dMonth.options[document.Form1.dMonth.selectedIndex-1].value) == 1 )				
					{				
						strTemp= "0" + document.Form1.dMonth.options[document.Form1.dMonth.selectedIndex-1].value;				
					}
					else
					{	strTemp = document.Form1.dMonth.options[document.Form1.dMonth.selectedIndex-1].value;}
				
					if (Len(document.Form1.dDay.options[document.Form1.dDay.selectedIndex].value) == 1 )				
					{				
						strTemp= strTemp + "/0" + document.Form1.dDay.options[document.Form1.dDay.selectedIndex].value;				
					}
					else
					{	strTemp = strTemp + "/" + document.Form1.dDay.options[document.Form1.dDay.selectedIndex].value;}								
				
					strTemp = strTemp + "/" + document.Form1.dYear.options[document.Form1.dYear.selectedIndex].value;
					document.Form1.txtSdate.value = strTemp;
				}
				else
				{
					document.Form1.txtSdate.value = '';
				}
												
				//*******************
				if (document.Form1.ToMonth.value!=1 && document.Form1.ToDay.value!=0 && document.Form1.ToYear.value!=0)
				{
					if (Len(document.Form1.ToMonth.options[document.Form1.ToMonth.selectedIndex-1].value) == 1 )				
					{				
						strTemp= "0" + document.Form1.ToMonth.options[document.Form1.ToMonth.selectedIndex-1].value;				
					}
					else
					{	strTemp = document.Form1.ToMonth.options[document.Form1.ToMonth.selectedIndex-1].value;}
				
					if (Len(document.Form1.ToDay.options[document.Form1.ToDay.selectedIndex].value) == 1 )				
					{				
						strTemp= strTemp + "/0" + document.Form1.ToDay.options[document.Form1.ToDay.selectedIndex].value;				
					}
					else
					{	strTemp = strTemp + "/" + document.Form1.ToDay.options[document.Form1.ToDay.selectedIndex].value;}								
				
					strTemp = strTemp + "/" + document.Form1.ToYear.options[document.Form1.ToYear.selectedIndex].value;
				
					document.Form1.txtEndDate.value = strTemp;				
				}				
				else
				{
					document.Form1.txtEndDate.value = '';
				}
								
			}
		
		
function SplitDate()
			{
		       var time=new Date();
               var date=time.getDate();
               var year=time.getYear();
               if (year < 2000)    // Y2K Fix, Isaac Powell
               year = year + 1900; // http://onyx.idbsu.edu/~ipowell
		       
		       var d1 = new dateObj(document.Form1.dMonth, document.Form1.dDay, document.Form1.dYear);			   
			   
			   if ((document.Form1.txtSdate.value != "") || (document.Form1.txtSdate.value != (eval(document.Form1.dMonth.options[document.Form1.dMonth.selectedIndex].value) + "/" +  eval(document.Form1.dDay.options[document.Form1.dDay.selectedIndex].value) + "/" + document.Form1.dYear.options[document.Form1.dYear.selectedIndex].value)))
			   {			   		   
					var strDOB = new String(document.Form1.txtSdate.value)
					var stringArray = strDOB.split("/"); 
					initDates(2002 ,  year, stringArray[2], stringArray[1], stringArray[0], d1);
			   }			   
			   var d2 = new dateObj(document.Form1.ToMonth,document.Form1.ToDay, document.Form1.ToYear);
			   
			   if ((document.Form1.txtEndDate.value != "") || (document.Form1.txtEndDate.value != (document.Form1.ToMonth.options[document.Form1.ToMonth.selectedIndex].value + "/" +  document.Form1.ToDay.options[document.Form1.ToDay.selectedIndex].value + "/" + document.Form1.ToYear.options[document.Form1.ToYear.selectedIndex].value)))
			   {
					var strAppt = new String(document.Form1.txtEndDate.value)
					var stringArray = strAppt .split("/"); 
					//initDates(1930, year, stringArray[2], stringArray[0], stringArray[1], d2);
					initDates(2002, year, stringArray[2], stringArray[1], stringArray[0], d2);	           
			   }			   
			   
			} 
					
		function HideBox()
		{			
		    element = document.getElementById('TRHIDE').style;
		    element.display='none';		    
		    /*
		    element = document.getElementById('txtSdate').style;
		    element.display='none';	
		    element = document.getElementById('txtEndDate').style;
		    element.display='none';	
		    */		    
		}
		
		function ValidateMe()
		{    
			if (IsDatesEqualOrGrater(document.Form1.calTo.value,'MM/dd/yyyy',document.Form1.calFrom.value,'MM/dd/yyyy')==false)
			{
				alert("Please enter valid date, To-Date must be grater then or equal to From-Date");
				document.Form1.calTo.focus(); 
				return false;
			}		
		}
			
		</script>

        
	</HEAD>
	<body onload="HideBox();">
		<form id="Form1" method="post" runat="server">
			<TABLE id="TableMain" style="Z-INDEX: 101; WIDTH: 768px" cellSpacing="0" cellPadding="0" align="center" border="0">
				<TBODY>
					
					<tr>
						<td colSpan="8">
							<uc1:ActiveMenu id="ActiveMenu1" runat="server"></uc1:ActiveMenu>
						</td>
					</tr>
					

					<TR>
						<TD class="clsmainhead" style="HEIGHT: 6px; width: 766px;" background="../images/separator_repeat.gif"
							colSpan="1"></TD>
					</TR>
					
					<TR ><td><Table width=100%>
					    <tr>
						<td class="clsLeftPaddingTable"  >
							From: </td>
							<td style="width: 155px; height: 23px;" class="clsLeftPaddingTable"><ew:calendarpopup id="calFrom" runat="server" Width="118px" DisplayOffsetY="-130" ToolTip="Call Back Date"
								PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00" ShowClearDate="True" Nullable="True"
								Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Left" ImageUrl="../images/calendar.gif"
								ControlDisplay="TextBoxImage" Font-Size="8pt" Font-Names="Tahoma">
									
                                <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                    ForeColor="Black" />
                                <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                    ForeColor="Black" />
                                <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                    ForeColor="Black" />
                                <TextboxLabelStyle CssClass="clstextarea" />
                                <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    Font-Size="XX-Small" ForeColor="Black" />
                                <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                    ForeColor="Black" />
                                <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                    ForeColor="Black" />
                                <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    Font-Size="XX-Small" ForeColor="Gray" />
                                <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                    ForeColor="Black" />
                                <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    Font-Size="XX-Small" ForeColor="Black" />
                                <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    Font-Size="XX-Small" ForeColor="Black" />
							</ew:calendarpopup>
							</td>
							<td style="height: 23px; width: 51px;" class="clsLeftPaddingTable">
                                To:</td>
							<td class="clsLeftPaddingTable" style="height: 23px"><ew:calendarpopup id="calTo" runat="server" Width="137px" DisplayOffsetY="-130" ToolTip="Call Back Date"
								PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00" ShowClearDate="True" Nullable="True"
								Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Left" ImageUrl="../images/calendar.gif"
								ControlDisplay="TextBoxImage" Font-Size="8pt" Font-Names="Tahoma">
                                <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    Font-Size="XX-Small" ForeColor="Black" />
                                <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                    ForeColor="Black" />
                                <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                    ForeColor="Black" />
                                <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                    ForeColor="Black" />
                                <TextboxLabelStyle CssClass="clstextarea" />
                                <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    Font-Size="XX-Small" ForeColor="Black" />
                                <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                    ForeColor="Black" />
                                <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                    ForeColor="Black" />
                                <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    Font-Size="XX-Small" ForeColor="Gray" />
                                <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                    ForeColor="Black" />
                                <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    Font-Size="XX-Small" ForeColor="Black" />
							</ew:calendarpopup>
							</TD>
						<TD class="clsLeftPaddingTable" vAlign="middle" style="width: 78px; height: 23px;">
							<asp:button id="btnSearch" runat="server" CssClass="clsbutton" Text="Search"></asp:button>
							</TD>
					</TR>
					</Table>
					</td>
					</TR>
					<TR id="TRHIDE">
						<TD class="clsLeftPaddingTable" >
						
							<asp:textbox id="txtEndDate" runat="server" Width="24px" CssClass="clsInputnamefield" TextMode="SingleLine"></asp:textbox><asp:textbox id="txtSdate" runat="server" Width="16px" CssClass="clsInputnamefield" TextMode="SingleLine"></asp:textbox></TD>
						
					</TR>
					<tr>
						<td ><asp:table id="oTable" runat="server" CssClass="clsLeftPaddingTable" Width="100%" BorderWidth="1px"
								Visible="true"></asp:table>
					<TR>
						<TD align="center" ><asp:label id="lblMessage" runat="server"  ForeColor="Red"></asp:label></TD>
					</TR>
					<tr>
												<td  background="../images/separator_repeat.gif"  height="11"></td>
											</tr>
											<TR>
												<TD ><uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
											</TR>
				</TBODY>
			</TABLE>
		</form>
		
	</body>
</HTML>

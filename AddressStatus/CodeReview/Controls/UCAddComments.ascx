<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCAddComments.ascx.cs" Inherits="lntechNew.CodeReview.Controls.WebUserControl1" %>
 <LINK href="../../Styles.css" type="text/css" rel="stylesheet">
&nbsp;<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
 
<table border="0" cellpadding="0" cellspacing="0" style="width: 65%" summary="test description">
    <tr>
        <td align="right" class="clsaspcolumnheader" style="width: 133px; height: 18px">
            Class Name</td>
        <td align="left" style="width: 100px; height: 18px">
            &nbsp;<asp:DropDownList ID="ddl_Class" runat="server" CssClass="clsrlist">
            </asp:DropDownList></td>
    </tr>
    <tr>
        <td align="right" class="clsaspcolumnheader" style="width: 133px; height: 19px">
            Reviewed By :
        </td>
        <td align="left" style="width: 100px; height: 19px">
            &nbsp;<asp:DropDownList ID="ddl_Reviewedby" runat="server" CssClass="clsrlist">
            </asp:DropDownList></td>
    </tr>
    <tr>
        <td align="right" class="clsaspcolumnheader" style="width: 133px; height: 95px" valign="top">
            User Review :
        </td>
        <td align="left" class="clsaspcolumnheaderblack" style="width: 100px; height: 95px" valign="top">
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 100px">
                        <asp:TextBox ID="TextBox2" runat="server" CssClass="clstextarea" Height="86px" TextMode="MultiLine" Width="411px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="clsaspcolumnheaderblack" style="width: 100px">
                        **Please Add Your comments here </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="1">
    
    </td>
        <td colspan="2">
            <asp:CheckBox ID="CheckBox1" runat="server" Text="Code Well Commented" />
            <asp:CheckBox ID="CheckBox2" runat="server" Text="Error log" />
        </td>
    </tr>
    <tr>
        <td style="width: 133px">
        </td>
        <td>
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 100px; height: 20px">
                        <asp:Button ID="Button1" runat="server" CssClass="clsbutton" Text="Reset" /></td>
                    <td style="width: 100px; height: 20px">
                        <asp:Button ID="Button2" runat="server" CssClass="clsbutton" Text="Save" /></td>
                </tr>
            </table>
            &nbsp;
        </td>
    </tr>
</table>
    </ContentTemplate>
     <Triggers>
            <asp:AsyncPostBackTrigger ControlID="UCaddClass1$Button2" EventName="Click" />
                        
            </Triggers>
</asp:UpdatePanel>


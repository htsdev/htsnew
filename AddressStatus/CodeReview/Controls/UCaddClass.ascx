<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCaddClass.ascx.cs" Inherits="lntechNew.CodeReview.Controls.UCaddClass" %>
 <LINK href="../../Styles.css" type="text/css" rel="stylesheet">
<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
    <ContentTemplate>

<table border="0" cellpadding="0" cellspacing="0" style="width: 65%" summary="test description">
    <tr>
        <td align="right" class="clsaspcolumnheader" style="width: 143px; height: 18px">
            Class Name</td>
        <td align="left" style="width: 432px; height: 18px">
            <asp:TextBox ID="txtClsName" runat="server" CssClass="clstextarea" Width="242px"></asp:TextBox></td>
    </tr>
    <tr>
        <td align="right" class="clsaspcolumnheader" style="width: 143px; height: 95px">
            Class Description
        </td>
        <td align="left" class="clsaspcolumnheaderblack" style="width: 432px; height: 95px">
            <asp:TextBox ID="txtClsDesc" runat="server" CssClass="clstextarea" Height="86px" TextMode="MultiLine"
                Width="434px"></asp:TextBox></td>
    </tr>
    <tr>
        <td colspan="2">
        </td>
    </tr>
    <tr>
        <td style="width: 143px">
        </td>
        <td style="width: 432px" align="center">
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 100px; height: 20px" align="center">
                        <asp:Button ID="Button1" runat="server" CssClass="clsbutton" Text="Reset" OnClick="Button1_Click" /></td>
                    <td style="width: 100px; height: 20px" align="center">
                        <asp:Button ID="Button2" runat="server" CssClass="clsbutton" Text="Save" OnClick="Button2_Click" /></td>
                </tr>
            </table>
            <asp:Label ID="lblMessage" runat="server" Text="Label"></asp:Label></td>
    </tr>
</table>
    </ContentTemplate>
    <Triggers>
            <asp:AsyncPostBackTrigger ControlID="Button2" EventName="Click" />
           
            </Triggers>
</asp:UpdatePanel>

﻿using System;
using System.Web.UI.WebControls;
using lntechNew.Components.ClientInfo;
using HTP.ClientController;
using System.Collections.Generic;
using POLMDTO;
using HTP.WebComponents;

namespace HTP
{
    public partial class DivisionSetting : RoleBasePage
    {
        #region Variables

        PolmController htpController = new PolmController();

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    SetAccessRights();
                    FillGrid();
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (btnAdd.Text == "Add")
                {
                    if (Add())
                    {
                        SetEmptyControl();
                        FillGrid();
                        lbl_Message.Text = "Record added successfully.";
                    }
                    else
                        lbl_Message.Text = "Record not added successfully.";
                }
                else if (btnAdd.Text == "Update")
                {
                    if (Update())
                    {
                        SetEmptyControl();
                        FillGrid();
                        lbl_Message.Text = "Record updated successfully.";
                    }
                    else
                        lbl_Message.Text = "Record not updated successfully.";
                }

            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        protected void gvDivision_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                lbl_Message.Text = string.Empty;
                if (e.CommandName == "ImgDelete")
                {
                    int associatedCases = Convert.ToInt32((((HiddenField)gvDivision.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("hfcases")).Value));
                    if (associatedCases == 0)
                    {
                        int ID = Convert.ToInt32((((HiddenField)gvDivision.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("hfValueId")).Value));
                        htpController.DeleteDivision(ID);
                        lbl_Message.Text = "Record deleted successfully";
                        FillGrid();
                    }
                    else
                        lbl_Message.Text = "Division Cannot be deleted as it is associated with case. Please update case first then remove again.";
                }
                else if (e.CommandName == "lnkbutton")
                {
                    txtDivision.Text = ((LinkButton)gvDivision.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("lnkbtnValue")).Text;
                    ViewState["ValueID"] = ((HiddenField)gvDivision.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("hfValueId")).Value;
                    chkIsActive.Checked = Convert.ToBoolean(((HiddenField)gvDivision.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("hfIsactive")).Value);
                    btnAdd.Text = "Update";
                    SetAccessRights();
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void gvDivision_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ((LinkButton)e.Row.FindControl("lnkbtnValue")).CommandArgument = Convert.ToString(e.Row.RowIndex);
                    ((ImageButton)e.Row.FindControl("ImgDelete")).CommandArgument = Convert.ToString(e.Row.RowIndex);
                    ((ImageButton)e.Row.FindControl("ImgDelete")).Enabled = base.CanDelete;
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        protected void lnk_AddNewRecord_Click(object sender, EventArgs e)
        {
            try
            {
                SetEmptyControl();
                btnAdd.Text = "Add";
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        #endregion

        #region Method

        private void SetAccessRights()
        {
            if(btnAdd.Text.ToLower()=="add")
            {
                btnAdd.Enabled = base.CanAdd;
            }
            else
            {
                 btnAdd.Enabled = base.CanEdit;
            }
            btnTrial.Enabled = base.ProcessAccessRight(Process.Print_Trial_Docket);
        }

        private void SetEmptyControl()
        {
            lbl_Message.Text = string.Empty;
            txtDivision.Text = string.Empty;
            chkIsActive.Checked = false;
        }

        private void FillGrid()
        {
            List<PolmDto> Divisionlist = htpController.GetAllDivision(null);
            if (Divisionlist != null)
            {
                gvDivision.DataSource = Divisionlist;
                gvDivision.DataBind();
            }
            else
            {
                gvDivision.DataSource = null;
                gvDivision.DataBind();
            }
        }

        private bool Add()
        {
            return htpController.AddDivision(Server.HtmlEncode(txtDivision.Text), chkIsActive.Checked);
        }

        private bool Update()
        {
            return htpController.UpdateDivision((Convert.ToInt32(ViewState["ValueID"])), Server.HtmlEncode(txtDivision.Text), chkIsActive.Checked);
        }

        
        #endregion
    }
}

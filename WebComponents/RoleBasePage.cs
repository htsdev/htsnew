using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using lntechNew.Components.ClientInfo;
using System.Collections.Generic;
using FrameWorkEnation.Components;

namespace HTP.WebComponents
{
    public class RoleBasePage : System.Web.UI.Page
    {

        clsSession cSession = new clsSession();
        clsENationWebComponents ClsDb = new clsENationWebComponents("Users");

        public bool CanView
        {
            get;
            private set;
        }

        public bool CanAdd
        {
            get;
            private set;
        }

        public bool CanEdit
        {
            get;
            private set;
        }

        public bool CanDelete
        {
            get;
            private set;
        }

        List<UserAccessRightsDto> _ulist;
        protected override void OnLoad(EventArgs e)
        {

            if (cSession.IsValidSession(Request, Response, Session) == false)
            {
                Response.Redirect("../frmlogin.aspx", false);
                goto EndOfMethod;
            }
            _ulist = GetAllAccessRights(new UserDto { ApplicationId = 1, UserId = 1 });

            var result = _ulist.FindAll(
                bk => bk.URL == "/DivisionSettig.aspx"
                );

            result.ForEach(SetAccessRights);

            base.OnLoad(e);

        EndOfMethod: { }
        }

        public bool ProcessAccessRight(Process p)
        {
            UserAccessRightsDto result = _ulist.Find(
                delegate(UserAccessRightsDto bk)
                {
                    if (bk.ProcessId != null)
                    {
                        return (Process)bk.ProcessId == p;
                    }
                    else
                        return false;
                }
                );
            return true;
        }

        private void SetAccessRights(UserAccessRightsDto result)
        {
            CanView = (CanView == false) ? result.View : true;
            CanAdd = (CanAdd == false) ? result.Add : true;
            CanEdit = (CanEdit == false) ? result.Edit : true;
            CanDelete = (CanDelete == false) ? result.Delete : true;
        }

        public List<UserAccessRightsDto> GetAllAccessRights(UserDto userDto)
        {
            string[] key = { "@UserId", "@ApplicationID" };
            object[] value1 = { userDto.UserId, userDto.ApplicationId };
            return ConvertToList(ClsDb.Get_DT_BySPArr("usp_Users_Get_AllAccessRights", key, value1));
        }

        private List<UserAccessRightsDto> ConvertToList(DataTable dataReturn)
        {
            List<UserAccessRightsDto> list = null;
            if (dataReturn != null)
            {
                list = new List<UserAccessRightsDto>();
                foreach (DataRow dr in dataReturn.Rows)
                {
                    UserAccessRightsDto userAccessRightsDto = new UserAccessRightsDto
                    {
                        MenuId = dr["MenuId"] as int?,
                        ProcessId = dr["ProcessId"] as int?,
                        ProcessName = dr["ProcessName"] as string,
                        CanExecute = dr["CanExecute"] as bool?,
                        View = Convert.ToBoolean(dr["View"]),
                        Add = Convert.ToBoolean(dr["Add"]),
                        Edit = Convert.ToBoolean(dr["Edit"]),
                        Delete = Convert.ToBoolean(dr["Delete"]),
                        UserName = dr["UserName"] as string,
                        RoleName = dr["RoleName"] as string,
                        PageAppName = dr["PageAppName"] as string,
                        PageApplicationId = dr["PageApplicationId"] as int?,
                        ProcessAppId = dr["ProcessAppId"] as int?,
                        ProcessAppName = dr["ProcessAppName"] as string,
                        URL = dr["URL"] as string
                    };
                    list.Add(userAccessRightsDto);
                }
            }
            return list;
        }
    }

    public class UserDto
    {
        public int UserId { get; set; }

        public int ApplicationId { get; set; }
    }

    public class UserAccessRightsDto
    {
        public int? MenuId { get; set; }
        public int? ProcessId { get; set; }
        public string ProcessName { get; set; }
        public bool? CanExecute { get; set; }
        public bool View { get; set; }
        public bool Add { get; set; }
        public bool Edit { get; set; }
        public bool Delete { get; set; }
        public string UserName { get; set; }
        public string RoleName { get; set; }
        public string PageAppName { get; set; }
        public int? PageApplicationId { get; set; }
        public int? ProcessAppId { get; set; }
        public string ProcessAppName { get; set; }
        public string URL { get; set; }
    }

    public enum Process : int
    {
        Test_You_Framework = 1,
        Review_the_Changes = 2,
        Print_Trial_Docket = 3
    }
}

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ScrolGrid.aspx.cs" Inherits="ScrollableDataGrid.ScrollGrid" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Scrollable Data Grid</title>
    <style type="text/css">
            .DataGridFixedHeader { height:20px; POSITION: relative; ; TOP: expression(this.offsetParent.scrollTop-1.0) }
    </style>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div id="panel1" style="width:816px; height:400px; overflow:auto ">
        <asp:DataGrid ID="dg" runat="server" AutoGenerateColumns="False" Width="800px"  >
        <HeaderStyle CssClass="DataGridFixedHeader" />
            <Columns>
                <asp:BoundColumn DataField="empid" HeaderText="Employee ID">
                <HeaderStyle CssClass="GrdAdminHeader" />
                </asp:BoundColumn>
                <asp:BoundColumn DataField="name" HeaderText="Employee Name">
                <HeaderStyle CssClass="GrdAdminHeader" />
                </asp:BoundColumn>
                <asp:BoundColumn DataField="username" HeaderText="User Name">
                <HeaderStyle CssClass="GrdAdminHeader" />
                </asp:BoundColumn>
            </Columns>
            
        </asp:DataGrid>
    </div>
    </form>
    
</body>
</html>

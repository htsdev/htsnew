using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;

namespace ScrollableDataGrid
{
    public partial class ScrollGrid : System.Web.UI.Page
    {
        clsENationWebComponents clsdb = new clsENationWebComponents();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                dg.DataSource = clsdb.Get_DS_BySP("usp_hts_get_scrollgrid_users");
                dg.DataBind();
            }
        }
    }
}

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LogicPage.aspx.cs" Inherits="HTP.BusinessLogic.LogicPage" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Business Logic</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
    
    <!-- CORE CSS TEMPLATE - END -->

    <script language="javascript" type="text/javascript">
//    Fahad 16/01/2009 5376 --ask to delete from User---- 
    function DeleteCheck()
	{
		var x=confirm("Are you sure you want to delete this document.[OK] Yes [Cancel] No");
		if(x)
		{	
			//document.getElementById("FPUpload").outerHTML = "";  
			document.getElementById("FPUpload").outerHTML = document.getElementById("FPUpload").outerHTML.substring(0,56)+">"
            document.getElementById("hf_CheckDelete").value ="1";
            return true;	        
		}
		else
		{
			 document.getElementById("hf_CheckDelete").value ="0";	
			 return false;
		}
				
	}
//    Fahad 16/01/2009 5376 --Open new window for bussinessLogic---- 
	function OpenPopUpNew(path)  //(var path,var name)
	{

		    window.open(path,'',"height=540,width=760,resizable=no,status=no,toolbar=no,scrollbars=yes,menubar=no,location=center");
		    //window.closed();
		    return false;
		
    }
    function ViewCheck()
    {
        return true;
    }

    //    Fahad 18/02/2009 5376 --Allowed file extensions and file size checking---- 
    function CheckEmpty()
    { 
   
         if (document.getElementById("FPUpload").value.length==0)
         {
            alert("Please select file to Upload!");
            return false;
         }
         else if(document.getElementById("FPUpload").value.length!="")
         {
            if(document.getElementById("FPUpload").value!=null)
            {
          
                var Filepath = document.getElementById('FPUpload').value;
                var filext = Filepath.substring(Filepath.lastIndexOf(".")+1);
                if (filext != "pdf" && filext != "html" && filext != "mhtml" && filext != "doc" && filext != "docx" && filext != "xls" && filext != "xlsx" && filext != "vsd")
                {
                    alert("Invalid file format! Supported file formats are .pdf, .html, .mhtml, .doc, .docx, .xls, .xlsx and .vsd"); 
                    document.getElementById("FPUpload").outerHTML = document.getElementById("FPUpload").outerHTML.substring(0,56)+">"    
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                alert("Please insert Correct File!");
                return false;
            }
       }
      
    }
    
    </script>

</head>
<body style="margin-bottom: 0px; margin-left: 0px; margin-right: 0px; margin-top: 0px">
    <form id="form1" runat="server">
   
         <div class="page-container row-fluid container-fluid">
              <section id="main-contentpopup" class="">
    <section class="wrapper main-wrapper row" style="">
        <%--<div class="col-xs-12">
        <div class="page-title">

            <div class="pull-left">
                <!-- PAGE HEADING TAG - START --><h1 class="title">Read Comments</h1><!-- PAGE HEADING TAG - END -->                           

            </div>
            </div>


            </div>--%>
        <%--<div class="clearfix"></div>--%>

         <div class="col-xs-12">

             <section class="box" id="">
                 <header class="panel_header">
                     <h2 class="title pull-left">Business Logic</h2>
                     <div class="actions panel_actions pull-right">
                     <asp:HyperLink ID="hlk_EditBusinessLogic" runat="server" Font-Underline="True">Edit Help Content</asp:HyperLink>



                         <a class="box_toggle fa fa-chevron-down"></a>
                    
                </div>
            </header>
                 <div class="content-body">
            <div class="row">

                 <div class="col-md-12">
                                                        <div class="form-group">
                            <label class="form-label">Read Comments</label>
                            <span class="desc"></span>
                            <div class="controls">
                                <asp:Label ID="lblMessage" runat="server" Text="" ForeColor="red"> </asp:Label>
                              <%-- <div id="divBusinessLogic" runat="server" style="overflow: scroll; width: 750px;
                    height: 200px">--%>
                                 <div id="divBusinessLogic" runat="server">
                </div>
                                </div>
                                                            </div>
                     </div>
                </div>
                     </div>
                 </section>

             <div class="clearfix"></div>

              <section class="box" id="">
                 <header class="panel_header">
                     <h2 class="title pull-left">Associate Documents</h2>
                     <div class="actions panel_actions pull-right">
                     
                         <a class="box_toggle fa fa-chevron-down"></a>
                    
                </div>
            </header>
                 <div class="content-body" >
            <div class="row">
                
                 <table>
                    <tr id="trBusinessLogic" runat="server">
                        <td>
                 <div class="col-md-12" >
                                                        <div class="form-group">
                            <label class="form-label"></label>
                            <span class="desc"></span>
                            <div class="controls">
                                <div class="table-responsive" data-pattern="priority-columns">
                                 <asp:DataGrid ID="dgrdDoc" CssClass="table table-small-font table-bordered table-striped" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        Font-Names="Verdana" Font-Size="2px" PageSize="20" Width="100%" OnItemCommand="dgrdDoc_ItemCommand">
                        <FooterStyle CssClass="GrdFooter" />
                        <PagerStyle CssClass="GrdFooter" Font-Names="Arial" HorizontalAlign="Center" Mode="NumericPages"
                            NextPageText="" PageButtonCount="50" PrevPageText="" />
                        <AlternatingItemStyle BackColor="#EEEEEE" />
                        <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Center" VerticalAlign="Middle" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="ID" Visible="false">
                                <ItemStyle HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lblDocID" runat="server" CssClass="form-label" Font-Size="Smaller" Text='<%# DataBinder.Eval(Container, "DataItem.HelpDocID_PK") %>'
                                        Visible="False"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Left" Font-Size="Smaller" Width="160px" />
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Uploaded Date">
                                <ItemStyle HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lblDateTime" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.UploadedDate") %>'
                                        Font-Size="Smaller"></asp:Label>&nbsp; &nbsp;
                                </ItemTemplate>
                                <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Left" Font-Size="Smaller" Width="160px" />
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Document Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblDocName" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.DocName") %>'
                                        Font-Size="Smaller"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Left" Font-Size="Smaller" />
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Rep">
                                <ItemTemplate>
                                    <asp:Label ID="lblAbb1" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.UserName") %>'
                                        Font-Size="Smaller"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Left" Font-Size="Smaller" />
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="View">
                                <ItemStyle HorizontalAlign="Center" />
                                <%--<ItemTemplate>
                                    <a onclick='OpenPopUpNew("previewdoc.aspx?docname=<%# DataBinder.Eval(Container, "DataItem.SavedDocName") %>");'>
                                        <asp:Image ImageUrl="../images/Preview.gif" BorderWidth="0" runat="server" ID="img" /></a>
                                </ItemTemplate>--%>
                                <ItemTemplate>
                                    <asp:Label ID="lblSaveDoc" runat="server" CssClass="form-label" Font-Size="Smaller" Text='<%# DataBinder.Eval(Container, "DataItem.SavedDocName") %>'
                                        Visible="False"></asp:Label>
                                    <asp:LinkButton ID="lnkbtn_View" runat="server" CommandName="View" Font-Size="Smaller">
                                        <asp:Image ImageUrl="../images/Preview.gif" BorderWidth="0" runat="server" ID="img" />
                                    </asp:LinkButton>&nbsp;
                                </ItemTemplate>
                                <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Left" />
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Options">
                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkbtn_Delete" runat="server" OnClientClick="return DeleteCheck();"
                                        CommandName="Delete" Font-Size="Smaller">Delete</asp:LinkButton>&nbsp;
                                </ItemTemplate>
                                <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Left" Font-Size="Smaller" />
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                                    </div>
                              
                                </div>
                                                            </div>
                     </div>
                            </td>
                        </tr>
                     </table>
                     

                <div class="clearfix"></div>
                 <table>
                    <tr id="UploadSection" runat="server">
                        <td>
                <div class="col-md-12">
                                                        <div class="form-group">
                            <%--<label class="form-label"></label>--%>
                                                            <asp:Label ID="lblUpload" CssClass="form-label" runat="server" Text="Upload File"></asp:Label>
                            <span class="desc"></span>
                            <div class="controls">

                                  <asp:FileUpload ID="FPUpload" runat="server" CssClass="clsInputcommentsfield" />
                                 <asp:Button ID="btnUpload" runat="server" CssClass="btn btn-primary" OnClick="btnUpload_Click"
                                Text="Upload" OnClientClick="return CheckEmpty();" />
                                <asp:HiddenField ID="hf_CheckDelete" runat="server" />
                               
                              
                                </div>
                                                            </div>
                     </div>
                            </td>
                        </tr>
                     </table>

                </div>
                     </div>
                 </section>



         </div>
        </section>
                  </section>
             </div>
    </form>
    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE JS FRAMEWORK - END -->

    <!-- CORE TEMPLATE JS - START -->
    <script src="../assets/js/scripts.js" type="text/javascript"></script>
    <!-- END CORE TEMPLATE JS - END -->
</body>
</html>

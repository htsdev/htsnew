using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using lntechNew.Components.ClientInfo;
using System.Security.Principal;
using FrameWorkEnation.Components;
namespace HTP
{
    /// <summary>
    /// Summary description for frmLogin.
    /// </summary>
    public partial class frmLogin : System.Web.UI.Page
    {

        #region Controls

        protected System.Web.UI.WebControls.CheckBox chk_SPassword;
        protected System.Web.UI.WebControls.Label lblMsg;
        protected System.Web.UI.WebControls.TextBox txtUsrId;
        protected System.Web.UI.WebControls.TextBox txt_Password;
        protected System.Web.UI.WebControls.Button btn_Login;

        #endregion

        #region Variables

        //Use To Get Information About The Current User
        WindowsIdentity winID;
        clsSession uuSession = new clsSession();
        bool IsAuthenticated = false;
        bool IsAnonymous = true;
        static String prvurl; 
        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Login.Click += new System.EventHandler(this.btn_Login_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion

        #region Events

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                      
                if (!IsPostBack)
                {
                    // kamran 3666 04/10/08
                    try
                    {
                        //Sabir Khan 10920 05/16/2013 Get Branches name.
                        GetBranchesName();
                        //prvurl = Request.UrlReferrer.AbsoluteUri.ToString();
                        prvurl = null;
                    }
                    catch (Exception) { }
                    clsUser iUser = new clsUser();
                    //Waqas 5057 03/17/2009 Checking employee info in session
                    ////Getting cookie values on start  
                    //iUser.LoginID = uuSession.GetCookie("sUserID", this.Request);
                    //iUser.Password = uuSession.GetCookie("Password", this.Request);

                    //if (uuSession.GetCookie("sEmpID", this.Request) == "")
                    //{
                    if (!uuSession.IsValidSession(this.Request, this.Response, this.Session))
                    {
                        iUser.EmpID = 0;
                    }
                    else
                    {

                        iUser.EmpID = Convert.ToInt32(uuSession.GetCookie("sEmpID", this.Request));
                    }

                    //Verifying User Login By Values From Cookie
                    //////////////if (ValidateUserByNTCredentials())
                    //////////////{
                    //////////////    Session["fromLoginPage"] = "1";
                    //////////////    //Fahad 13/01/2009 5376 Concatenate Menuid
                    //////////////   Response.Redirect("frmMain.aspx?sMenu=67", false);
                    //////////////    return;
                    //////////////}
                    //Added by Zeeshan Ahmed
                    //else 
                    if (IsAuthenticated == false || IsAnonymous == true)
                    {
                        //Verifying User Login By Values From Cookie
                        ValidateLogin(iUser, uuSession);
                    }
                    else if (IsAuthenticated)
                    {
                        lblMsg.Text = "For the first time please enter your login information to associate it with your NT login id.";
                        lblMsg.Visible = true;
                    }
                }
                // kamran 3666 04/10/08
                //btn_Login.Attributes.Add("Onclick", "return ValidateLogin();");
            }
            catch (Exception pload)
            {
                lblMsg.Text = pload.Message;
            }
        }
        
        // Event when Login button is clicked		
        private void btn_Login_Click(object sender, System.EventArgs e)
        {
            //clsUser class object
            clsUser ccUser = new clsUser();
            //clsSession class object
            //clsSession uuSession= new clsSession();							
            lblMsg.Visible = true;
            ValidateLogin(ccUser, uuSession);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Call authenticate,cookie,seesion function of User Class  
        /// </summary>
        /// <param name="cUser"></param>
        private void ValidateLogin(clsUser cUser, clsSession uSession)
        {
            try
            {
                int Empid = 0;

                //Sabir Khan 10923 05/16/2013 declare variables for branch info.
                string IPAddress = String.Empty;

                if ((txtUsrId.Text != "") && (txt_Password.Text != ""))
                {
                    cUser.LoginID = txtUsrId.Text.Trim();
                    cUser.Password = txt_Password.Text.Trim();
                }
                Empid = cUser.AuthenticateUser();
                if (Empid == 0)
                {
                    lblMsg.Text = "Invalid UserID or Password";
                }
                else
                {
                    if (!uSession.IsValidSession(this.Request, this.Response, this.Session))
                    {
                        //Zahoor 4676 09/17/2008 creating session variable for employee id which will be used to check validsession
                        uSession.SetSessionVariable("sEmpID", cUser.EmpID, this.Session);
                        
                        //In order to create employee cookie					
                        uSession.CreateCookie("sEmpID", cUser.EmpID.ToString(), Page.Request, Page.Response);
                        uSession.CreateCookie("sAccessType", cUser.AccessType.ToString(), Page.Request, Page.Response);
                        uSession.CreateCookie("sIsSPNUser", cUser.IsSPNUser.ToString(), Page.Request, Page.Response);
                        //Update the NTLoginID of the user in the database
                        //so that he doesn't need to log-in again
                        System.Security.Principal.IIdentity wid = HttpContext.Current.User.Identity;

                        if (wid.IsAuthenticated)
                        {
                            System.Security.Principal.WindowsIdentity winID = (System.Security.Principal.WindowsIdentity)wid;
                            cUser.UpdateNTUserID(cUser.EmpID, winID.Name.Remove(0, winID.Name.LastIndexOf(@"\") + 1));
                        }

                        //In order to save userid, password,  
                        if (chk_SPassword.Checked == true)
                        {
                            uSession.CreateCookie("sUserID", cUser.LoginID, Page.Request, Page.Response);
                            uSession.CreateCookie("Password", cUser.Password, Page.Request, Page.Response);
                        }
                        //Creating Session variables On Start 				
                        //this.Context.Session;
                        //					uSession.SetSessionVariable("sEmpLastName",cUser.LastName,this.Session);
                        //					uSession.SetSessionVariable("sEmpFirstName",cUser.FirstName,this.Session);
                        //					uSession.SetSessionVariable("sCanUpdateCloseOut",cUser.canUpdateCloseOut,this.Session);
                        uSession.CreateCookie("sEmpName", cUser.LastName + ", " + cUser.FirstName, this.Request, this.Response);
                        uSession.CreateCookie("sCanUpdateCloseOut", cUser.canUpdateCloseOut.ToString(), this.Request, this.Response);
                       
                    }

                    //					if (uSession.SetSessionVariable(cUser.LoginID,cUser.EmpID,cUser.Abbreviation,cUser.AccessType,this.Request, this.Response )==false)
                    //					{
                    //						Response.Redirect("frmlogin.aspx",false);
                    //					}
                    //					else
                    //					{					
                    //						clsSession.SessionID=Page.Session.SessionID;
                    //						clsSession.SessionTimeOut=Page.Session.Timeout=480;
                    //						
                    //						if(Convert.ToString(Session["topMenu"])=="")
                    //						{
                    //							Response.Cookies["topMenu"].Value="16";
                    //							Response.Cookies["subMenu"].Value="0";	
                    //							Response.Cookies["LastPage"].Value="";
                    //						}
                    //	
                    //						//Check for Non Client, archive, jims 
                    //						//uSession.SetSessionVariable("sMoved","False",this.Session);
                    //						
                    //						//Redirect to Main Page
                    //						Response.Redirect("frmMain.aspx",false);
                    //					
                    //						
                    //					}

                    //commented
                    //uSession.CreateCookie("sUserId", cUser.LoginID, this.Request, this.Response);

                    //commented by ozair not used
                    //uSession.CreateCookie("sAbbreviation", cUser.Abbreviation, this.Request, this.Response);

                    clsSession.SessionID = Page.Session.SessionID;
                    clsSession.SessionTimeOut = Page.Session.Timeout = 480;

                    if (Convert.ToString(Session["topMenu"]) == "")
                    {
                        Response.Cookies["topMenu"].Value = "16";
                        Response.Cookies["subMenu"].Value = "0";
                        Response.Cookies["LastPage"].Value = "";
                    }

                    //Check for Non Client, archive, jims 
                    //uSession.SetSessionVariable("sMoved","False",this.Session);
                    // kamran 3666 04/10/08

                    if (prvurl != null)
                    {
                        Response.Redirect(prvurl, false);
                    }
                    else
                    {
                        //Sabir Khan 10920 05/16/2013 Getting Local Machine Name..
                        //////localmachinename = Request.ServerVariables["REMOTE_HOST"];
                        //////uSession.CreateCookie("LocalMachineName", localmachinename, Request, Response);
                        //Sabir Khan 10920 05/16/2013 Get branch name and create cookie for branch.
                        string branchname = ddl_Branch.SelectedItem.Text.Trim();
                        uSession.CreateCookie("BranchName", branchname, this.Request, this.Response);
                        //Sabir Khan 10923 05/16/2013 get IP Address of Client Machine 
                        IPAddress = Request.ServerVariables["REMOTE_ADDR"];
                        uSession.CreateCookie("LocalIPAddress", IPAddress, Request, Response);
                        //Sabir Khan 10920 05/27/2013 check for selected branch and machine name.
                        clsENationWebComponents cls_db = new clsENationWebComponents();
                        string[] key1 = { "@ClientIPAddress" };
                        object[] value2 = { IPAddress };
                        DataTable dt1 = cls_db.Get_DT_BySPArr("USP_HTP_Check_ForOutSideMachine", key1, value2);
                        ClientScriptManager CSM = Page.ClientScript;
                        if (dt1.Rows.Count == 0)
                        {
                            string strconfirm = "<script>window.location.href='frmMain.aspx?sMenu=67'</script>";
                            CSM.RegisterClientScriptBlock(this.GetType(), "Confirm", strconfirm, false);
                        }
                        string[] key = { "@IPAddress", "@BranchId" };
                        object[] value1 = { IPAddress, Convert.ToInt32(ddl_Branch.SelectedValue) };
                        DataTable dt = cls_db.Get_DT_BySPArr("USP_HTP_Check_MachineName", key, value1);

                        if (dt.Rows.Count > 0)
                        { 
                            
                            string strconfirm = "<script>window.location.href='frmMain.aspx?sMenu=67'</script>";
                            CSM.RegisterClientScriptBlock(this.GetType(), "Confirm", strconfirm, false);
                        }
                        if (dt.Rows.Count == 0 && ddl_Branch.SelectedItem.Text == "Houston (2020)")
                        {
                            string strconfirm = "<script>if(window.confirm('You are currently trying to log on at the Houston (2020) branch but your computer is located at the Houston (611) branch location.  Would you like to continue?')){window.location.href='frmMain.aspx?sMenu=67'}</script>";
                            CSM.RegisterClientScriptBlock(this.GetType(), "Confirm", strconfirm, false);
                        }
                        if (dt.Rows.Count == 0 && ddl_Branch.SelectedItem.Text == "Houston (611)")
                        {
                            string strconfirm = "<script>if(window.confirm('You are currently trying to log on at the Houston (611) branch but your computer is located at the Houston (2020) branch location.  Would you like to continue?')){window.location.href='frmMain.aspx?sMenu=67'}</script>";
                            CSM.RegisterClientScriptBlock(this.GetType(), "Confirm", strconfirm, false);
                        }
                        
                        //Redirect to Main Page
                        //Fahad 13/01/2009 5376 Concatenate Menuid
                        //Waqas 5057 03/17/2009 Checking employee info in session
                        ///Response.Redirect("frmMain.aspx?sMenu=67", false);
                    }
                }
            }
            catch (Exception evalidate)
            {
                lblMsg.Visible = true;
                lblMsg.Text = evalidate.Message;
            }
        }

        //Verify NT User Credentials
        private bool ValidateUserByNTCredentials()
        {
            IIdentity wid = HttpContext.Current.User.Identity;

            //Changed By Zeeshan Ahmed On 14th November
            // If User Is Not Authenticated
            if (wid.IsAuthenticated == false)
            {              
                IsAuthenticated = false;
                IsAnonymous = true;
                return false;
            }
            
            winID = (WindowsIdentity)wid;
            
            //Set Authentication Variables 
            IsAuthenticated = winID.IsAuthenticated;
            IsAnonymous = winID.IsAnonymous;
            
            clsUser cUser = new clsUser();
            clsSession uSession = new clsSession();

            int Empid = 0;

            if (winID.IsAuthenticated == false || winID.IsAnonymous == true)
                return false;

            Empid = cUser.AuthenticateUserByNTCredentials(winID.Name.Remove(0, winID.Name.LastIndexOf(@"\") + 1));


            if (Empid == 0)
            {
                return false;
            }
            else
            {
                //Waqas 5057 03/17/2009
                uSession.SetSessionVariable("sEmpID", cUser.EmpID, this.Session);
                //In order to create employee cookie					
                uSession.CreateCookie("sEmpID", cUser.EmpID.ToString(), Page.Request, Page.Response);
                uSession.CreateCookie("sAccessType", cUser.AccessType.ToString(), Page.Request, Page.Response);
                uSession.CreateCookie("sIsSPNUser", cUser.IsSPNUser.ToString(), Page.Request, Page.Response);
                //In order to save userid, password,  
                if (chk_SPassword.Checked == true)
                {
                    uSession.CreateCookie("sUserID", cUser.LoginID, Page.Request, Page.Response);
                    uSession.CreateCookie("Password", cUser.Password, Page.Request, Page.Response);
                }

                uSession.CreateCookie("sEmpName", cUser.LastName + ", " + cUser.FirstName, this.Request, this.Response);
                uSession.CreateCookie("sCanUpdateCloseOut", cUser.canUpdateCloseOut.ToString(), this.Request, this.Response);


                clsSession.SessionID = Page.Session.SessionID;
                clsSession.SessionTimeOut = Page.Session.Timeout = 480;

                if (Convert.ToString(Session["topMenu"]) == "")
                {
                    Response.Cookies["topMenu"].Value = "16";
                    Response.Cookies["subMenu"].Value = "0";
                    Response.Cookies["LastPage"].Value = "";
                }


                //Redirect to Main Page
                return true;


            }
        }

        #endregion
        /// <summary>
        ///Sabir Khan 10920 05/16/2013 Get branch names and populate in the combo box
        /// </summary>
        private void GetBranchesName()
        {
            clsENationWebComponents objEnationFramework = new clsENationWebComponents();
            DataTable dtBranches = objEnationFramework.Get_DT_BySPArr("USP_HTP_Get_BranchesName");
            if (dtBranches.Rows.Count > 0)
            {
                ddl_Branch.Items.Clear();

                foreach (DataRow dr in dtBranches.Rows)
                {
                    ListItem item;
                    item = dr["Branchid"].ToString().Equals("1") ? new ListItem("--Choose--", "All") : new ListItem(dr["BranchName"].ToString(), dr["BranchId"].ToString());
                    ddl_Branch.Items.Add(item);
                }

            }
        }

    }
}


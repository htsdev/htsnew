<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.backroom.CategoryInfo" Codebehind="CategoryInfo.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Category Info</title>
		<%--<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">--%>

        <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
        <meta cont  nt="C#" name="CODE_LANGUAGE" />
        <meta content="JavaScript" name="vs_defaultClientScript" />
        <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
        <meta http-equiv="X-UA-Compatible" content="IE=5; IE=8; IE=7.5;" />
        <meta http-equiv="X-UA-Compatible" content="chrome=1">
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
        <!-- Favicon -->
        <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
        <!-- For iPhone -->
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
        <!-- For iPhone 4 Retina display -->
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
        <!-- For iPad -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
        <!-- For iPad Retina display -->




        <!-- CORE CSS FRAMEWORK - START -->
        <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
        <!-- CORE CSS FRAMEWORK - END -->

        <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


        <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


        <!-- CORE CSS TEMPLATE - START -->
        <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />

		<script>
	/*	function OpenWinEdit(strForm)
	      {  
	          var Win;
		      //Win = window.open("FrmAddNewCategory.aspx","","fullscreen=no,toolbar=no,width=450,height=180,left=250,top=250,status=no,menubar=no,scrollbars=yes,resizable=yes");
		      //Win = window.open('"strForm"','ds',"scrollbars=yes,width=600,height=300");
		      Win = window.open("'"+strForm+"'",'ds',"fullscreen=no,toolbar=no,width=570,height=350,left=120,top=200,status=no,menubar=no,scrollbars=yes,resizable=yes");				

		      return false;				      
	       }   
	      */
	      function OpenDelWin(intCategoryID)
	      {  
	          var Win;
		      //Win = window.open("FrmAddNewCategory.aspx","","fullscreen=no,toolbar=no,width=450,height=180,left=250,top=250,status=no,menubar=no,scrollbars=yes,resizable=yes");
		      //Win = window.open('"strForm"','ds',"scrollbars=yes,width=600,height=300");
		      //Win = window.open("FrmDelCategory.aspx?CategoryID="+intCategoryID,'ds',"fullscreen=no,toolbar=no,width=2,height=2,left=0,top=0,status=no,menubar=no,resizable=no");				
		      Win = window.open("FrmDelCategory.aspx?CategoryID="+intCategoryID,'ds',"fullscreen=no,toolbar=no,width=700,height=500,left=120,top=200,status=no,menubar=no,resizable=no");				
		      return false;				      
	       }   
	     
		  function OpenWinEdit(intCategoryID)
	      {  
	          var Win;
		      //Win = window.open("FrmAddNewCategory.aspx","","fullscreen=no,toolbar=no,width=450,height=180,left=250,top=250,status=no,menubar=no,scrollbars=yes,resizable=yes");
		      //Win = window.open('"strForm"','ds',"scrollbars=yes,width=600,height=300");
		      Win = window.open("FrmAddNewCategory.aspx?CategoryID="+intCategoryID,'ds',"fullscreen=no,toolbar=no,width=700,height=500,left=120,top=200,status=no,menubar=no,resizable=no");				
		      return false;				      
	       }   

		function OpenWin()
		{
	          var Win;
		      Win = window.open("FrmAddNewCategory.aspx",'ds',"fullscreen=no,toolbar=no,width=500,height=225,left=120,top=200,status=no,menubar=no,scrollbars=yes,resizable=yes");				
		      return false;				      
		}
	       
	     
		</script>


	</HEAD>

    <body class=" ">

        <form id="Form1" method="post" runat="server">            

            <!-- START CONTAINER -->
            <div class="page-container row-fluid container-fluid">

                <asp:Panel ID="pnl" runat="server">
                    <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                </asp:Panel>


                <!-- START CONTENT -->
                <section id="main-content" class="categoryInfoPage">
                    <section class="wrapper main-wrapper row" style=''>

                    <div class='col-xs-12'>
                        <div class="page-title">

                            <div class="pull-left">
                                <!-- PAGE HEADING TAG - START --><h1 class="title">Category Information</h1><!-- PAGE HEADING TAG - END -->                            </div>

                                        
                                
                        </div>
                        <asp:label id="lblMessage" runat="server" EnableViewState="False" Font-Names="Verdana" ForeColor="Red" Font-Bold="True" CssClass="form-label"></asp:label>
                    </div>
                    <div class="clearfix"></div>
                    <!-- MAIN CONTENT AREA STARTS -->
    
                        <div class="col-lg-12">
                <section class="box ">
                    <header class="panel_header">
                        <div class="actions panel_actions pull-right">
                            <asp:linkbutton id="lnkb_addnewcat" runat="server" CssClass="btn btn-primary customBtn2">Add New Category</asp:linkbutton>
                        </div>
                    </header>
                    <div class="content-body">
                        <div class="row">
                            <div class="col-xs-12">

                                <asp:datagrid id="dg_catinfo" runat="server" CssClass="table" AutoGenerateColumns="False">
								    <Columns>
									    <asp:TemplateColumn Visible="False" HeaderText="Category ID">
										    <ItemTemplate>
											    <asp:Label id=lbl_id runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.CategoryID") %>'>
											    </asp:Label>
										    </ItemTemplate>
									    </asp:TemplateColumn>
									    <asp:TemplateColumn HeaderText="Category Name">
										    <ItemTemplate>
											    <asp:Label id=lbl_categorynam runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.CategoryName") %>'>
											    </asp:Label>
										    </ItemTemplate>
									    </asp:TemplateColumn>
									    <asp:TemplateColumn HeaderText="Category Description">
										    <ItemTemplate>
											    <asp:Label id=lbl_catdesc runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.CategoryDescription") %>'>
											    </asp:Label>
										    </ItemTemplate>
									    </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Short Description">
                                            <ItemTemplate>
                                                <asp:Label ID="lblshortdesc" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.shortdescription") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
									    <asp:TemplateColumn HeaderText="Edit">
										    <ItemTemplate>
											    <!--<A onclick="OpenEditWin();"></A> -->
											    <asp:LinkButton id="Linkbutton1" runat="server" Text="Edit">Edit</asp:LinkButton>
										    </ItemTemplate>
									    </asp:TemplateColumn>
									    <asp:TemplateColumn HeaderText="Delete">
										    <ItemTemplate>
											    <asp:LinkButton id="LinkButton2" runat="server">Delete</asp:LinkButton>
										    </ItemTemplate>
									    </asp:TemplateColumn>
								    </Columns>
							    </asp:datagrid>

                                <%--<table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Username</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th scope="row">1</th>
                                            <td>Mark</td>
                                            <td>Otto</td>
                                            <td>@mdo</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">2</th>
                                            <td>Jacob</td>
                                            <td>Thornton</td>
                                            <td>@fat</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">3</th>
                                            <td>Larry</td>
                                            <td>the Bird</td>
                                            <td>@twitter</td>
                                        </tr>
                                    </tbody>
                                </table>--%>

                            </div>
                        </div>
                    </div>
                </section>
            </div>



                <!-- MAIN CONTENT AREA ENDS -->
                    </section>
                    </section>
                <!-- END CONTENT -->
               
            </div>
            <!-- END CONTAINER -->

        </form>

        <!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->


        <!-- CORE JS FRAMEWORK - START --> 
        <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script> 
        <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script> 
        <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>  
        <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script> 
        <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>  
        <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
        <!-- CORE JS FRAMEWORK - END --> 


        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 

        <script src="../assets/plugins/autosize/autosize.min.js" type="text/javascript"></script>
        <script src="../assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>
        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


        <!-- CORE TEMPLATE JS - START --> 
        <script src="../assets/js/scripts.js" type="text/javascript"></script> 
        <!-- END CORE TEMPLATE JS - END --> 


        <!-- General section box modal start -->
        <div class="modal" id="section-settings" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog animated bounceInDown">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Section Settings</h4>
                    </div>
                    <div class="modal-body">

                        Body goes here...

                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <button class="btn btn-success" type="button">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- modal end -->
        </body>




















	<%--<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE id="tblMain" cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
				<TBODY>
					<tr>
					</tr>
					<TR>
						<TD>
							<TABLE id="tblsub" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
								<tr>
									<td><uc1:activemenu id="ActiveMenu1" runat="server"></uc1:activemenu></td>
								</tr>
								<tr>
									<td background="../../images/separator_repeat.gif"  height="11"></td>
								</tr>
								<tr>
									<td align="center">
										<table id="gridhead" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
											<tr>
												<td background="../../images/headbar_headerextend.gif" colSpan="4" height="5"></td>
											</tr>
											<tr>
												<td class="clssubhead" height=34  background="../Images/subhead_bg.gif"
													colSpan="3" >&nbsp;Category Information</td>
												<td class="clssubhead" vAlign="middle" align="right"background="../Images/subhead_bg.gif" ><asp:linkbutton id="lnkb_addnewcat" runat="server"  >Add New Category</asp:linkbutton></td>
											</tr>
											<tr>
												<td background="../../images/headbar_footerextend.gif" height: 25px;></td>
											</tr>
										</table>
										<asp:label id="lblMessage" runat="server" EnableViewState="False" Font-Size="XX-Small"
											Font-Names="Verdana" ForeColor="Red" Font-Bold="True"></asp:label></td>
								</tr>
								<tr>
									<td>
										<table id="tblgrid" cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
											<TR>
												<TD align="center"><asp:datagrid id="dg_catinfo" runat="server" Width="780px"  CssClass="clsLeftPaddingTable"
														AutoGenerateColumns="False">
														<Columns>
															<asp:TemplateColumn Visible="False" HeaderText="Category ID">
																<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
																<ItemStyle HorizontalAlign="Left" VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id=lbl_id runat="server" Width="116px" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.CategoryID") %>'>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Category Name">
																<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
																<ItemStyle HorizontalAlign="Left" VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id=lbl_categorynam runat="server" Width="116px" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.CategoryName") %>'>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Category Description">
																<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
																<ItemStyle HorizontalAlign="Left" VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id=lbl_catdesc runat="server" Width="116px" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.CategoryDescription") %>'>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Short Description">
                                                            <HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblshortdesc" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.shortdescription") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Edit">
																<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
																<ItemStyle HorizontalAlign="Left" VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<!--<A onclick="OpenEditWin();"></A> -->
																	<asp:LinkButton id="Linkbutton1" runat="server" Text="Edit">Edit</asp:LinkButton>&nbsp;&nbsp;
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Delete">
																<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
																<ItemStyle HorizontalAlign="Left" VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<asp:LinkButton id="LinkButton2" runat="server">Delete</asp:LinkButton>
																</ItemTemplate>
															</asp:TemplateColumn>
														</Columns>
													</asp:datagrid></TD>
											</TR>
										</table>
									</td>
								</tr>
								<tr>
									<td background="../../images/separator_repeat.gif"  height="11"></td>
								</tr>
								<tr>
									<TD colSpan="5"><uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
								</tr>
							</TABLE>
						</TD>
					</TR>
				</TBODY>
			</TABLE>
		</form>
	</body>--%>
</HTML>

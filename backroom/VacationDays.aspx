<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VacationDays.aspx.cs" Inherits="lntechNew.backroom.VacationDays" %>

<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc3" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register Src="../WebControls/MenuTop.ascx" TagName="MenuTop" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
   
   <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
   
    <title>Vacation Days</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>
   
   

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
   
   
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="TableMain" align="center" border="0" cellpadding="0" cellspacing="0" width="780">
            <tr>
                <td colspan="4" style="width: 815px">
                </td>
            </tr>
            <tr>
                <td align="left" colspan="4" style="width: 816px">
                    <table id="Table1" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td align="left" colspan="4" valign="top">
                                <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td background="../Images/separator_repeat.gif" colspan="4" height="11" width="100%">
                            </td>
                        </tr>
                        <tr>
                            <td align="left" colspan="4" rowspan="1" width="100%" >
                                <table width="100%" class="clsleftpaddingtable">
                                    <tr>
                                        <td class="clsleftpaddingtable" style="width: 10px">
                                            <asp:Label ID="Label1" runat="server" CssClass="clssubhead" Text="Select Vacation Date"
                                                Width="132px"></asp:Label></td>
                                        <td  style="width: 132px">
                                            &nbsp;<ew:CalendarPopup ID="cal_EffectiveFrom" runat="server" AllowArbitraryText="False"
                                                CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Culture="(Default)" EnableHideDropDown="True"
                                                Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif" Nullable="True"
                                                PadSingleDigits="True" ShowClearDate="True" ShowGoToToday="True" Text=" " ToolTip="Vacation Date"
                                                UpperBoundDate="12/31/9999 23:59:00" Width="100px">
                                                <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    Font-Size="XX-Small" ForeColor="Black" />
                                                <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                                <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                                <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                                <TextboxLabelStyle CssClass="clstextarea" />
                                                <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    Font-Size="XX-Small" ForeColor="Black" />
                                                <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                                <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                                <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    Font-Size="XX-Small" ForeColor="Gray" />
                                                <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                                <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    Font-Size="XX-Small" ForeColor="Black" />
                                            </ew:CalendarPopup>
                                        </td>
                                        <td class="clsleftpaddingtable">
                                            <asp:Button ID="btn_refresh" runat="server" CommandName="search" CssClass="clsbutton"
                                                OnClick="btn_refresh_Click" Text="Add" Width="72px"  />
                                            <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="cal_EffectiveFrom"
                                                ErrorMessage="Please enter future date" Type="Date" Display="None">*</asp:RangeValidator>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="cal_EffectiveFrom"
                                                Display="None" ErrorMessage="Please enter date">*</asp:RequiredFieldValidator></td> 
                                        <td class="clsleftpaddingtable" style="width: 100px">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" background="../Images/separator_repeat.gif" colspan="4" style="height: 13px">
                            </td>
                        </tr>
                        <tr>
                            <td align="left"  colspan="4" style="width: 100%;">
                                <asp:GridView ID="gv_list" runat="server" AutoGenerateColumns="False" BorderStyle="None" OnRowCommand="gv_list_RowCommand" OnRowDeleting="gv_list_RowDeleting" Width="100%" CssClass="clsleftpaddingtable" OnSelectedIndexChanged="gv_list_SelectedIndexChanged">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Vacation Dates">
                                            <HeaderTemplate>
                                                <asp:Label ID="lbl_eadd" runat="server" CssClass="clssubhead" Text="Vacation Dates"
                                                    Width="165px"></asp:Label>
                                            </HeaderTemplate>
                                            <HeaderStyle CssClass="clsleftpaddingtable" Width="725px" Height="20px" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Date" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container,"DataItem.vacationdates") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderStyle CssClass="clsleftpaddingtable" Width="100px" />
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btn_delete" runat="server" CommandArgument='<%# DataBinder.Eval(Container,"DataItem.Id") %>'
                                                    CssClass="button" CausesValidation="false" CommandName="delete" OnClientClick="javscript:return confirm('Are you sure you want to delete ?')">Delete</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" background="../Images/separator_repeat.gif" colspan="4" style="width: 760px;
                                height: 13px">
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="4" style="width: 760px">
                                &nbsp;<asp:Label ID="lbl_error" runat="server" CssClass="clslabel" Font-Bold="True"
                                    ForeColor="Red"></asp:Label>
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="List"
                                    ShowMessageBox="True" ShowSummary="False" />
                            </td>
                        </tr>
                    </table>
                    <uc3:Footer ID="Footer1" runat="server" />
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace HTP.backroom
{
	/// <summary>
	/// Summary description for Courts.
	/// </summary>
	public partial class OutsideFirm : System.Web.UI.Page
    {

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Submit.Click += new System.EventHandler(this.btn_Submit_Click);
            this.btn_delete.Click += new System.EventHandler(this.btn_delete_Click);
            this.dgResult.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgResult_ItemCommand);
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion
		
        #region Controls

        protected System.Web.UI.WebControls.TextBox txt_fname;
		protected System.Web.UI.WebControls.TextBox txt_sname;
		protected System.Web.UI.WebControls.TextBox txt_add1;
		protected System.Web.UI.WebControls.TextBox txt_add2;
		protected System.Web.UI.WebControls.TextBox txt_city;
		protected System.Web.UI.WebControls.DropDownList ddl_state;
		protected System.Web.UI.WebControls.TextBox txt_zip;
		protected System.Web.UI.WebControls.Button btn_clear;
		protected System.Web.UI.WebControls.Button btn_Submit;
		protected System.Web.UI.WebControls.TextBox txt_basefee;
		protected System.Web.UI.WebControls.TextBox txt_daybeforefee;
		protected System.Web.UI.WebControls.TextBox txt_juryfee;
		protected System.Web.UI.WebControls.Button btn_delete;
		protected System.Web.UI.WebControls.Label lbl_message;
		protected System.Web.UI.WebControls.DataGrid dgResult;
		protected System.Web.UI.WebControls.TextBox txt_firmid;

        #endregion
        
        #region Variables 

        clsENationWebComponents ClsDB= new clsENationWebComponents();
		clsSession ClsSession=new clsSession();
		clsLogger clog = new clsLogger();
        DataView dv;
        int ActiveInactiveFlag = 1;
        #endregion

        #region Events

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                //Waqas 5057 03/17/2009 Checking employee info in session
                if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else //To stop page further execution
                {
                    // tahir 5682 03/20/2009 moved to reports menu.
                    //if (ClsSession.GetCookie("sAccessType", this.Request).ToString() != "2")
                    //{
                    //    Response.Redirect("../LoginAccesserror.aspx", false);
                    //    //Response.End();
                    //}
                    //else //To stop page further execution
                    //{
                        ImageUpload.Attributes.Add("onkeypress", "return false;");
                        ImageUpload.Attributes.Add("onkeydown", "return false;");
                        //Sabir Khan 4990 11/13/2008 add attridutes to check boxes...
                        chkActiveFirm.Attributes.Add("onclick", "IsCheck(2)");
                        chkInactive.Attributes.Add("onclick", "IsCheck(1)");
                        //Running for the first time
                        if (!IsPostBack)
                        {
                            //Filling States Dropdown
                            DataSet ds_state = ClsDB.Get_DS_BySP("USP_HTS_Get_State");
                            ddl_state.DataSource = ds_state;
                            ddl_state.DataTextField = ds_state.Tables[0].Columns[1].ColumnName;
                            ddl_state.DataValueField = ds_state.Tables[0].Columns[0].ColumnName;
                            ddl_state.DataBind();
                            //Calling method
                            FillGrid();
                            //Initializing firmid textbox with 0 for validation
                            txt_firmid.Text = "0";
                            btn_delete.Enabled = false;
                        }
                        btn_Submit.Attributes.Add("onclick", "return ValidateControls();");
                        //	btn_clear.Attributes.Add("onclick"," return check();");	
                    //}
                }
            }
            catch (Exception epload)
            {
                lbl_message.Text = epload.Message;
                clog.ErrorLog(epload.Message, epload.Source, epload.TargetSite.ToString(), epload.StackTrace);
            }
        }

        // Fire when Firm Name is clicked
        private void dgResult_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            //Change by Ajmal
            //System.Data.SqlClient.SqlDataReader dr = null;

            IDataReader dr = null;
            try
            {
                int firmid = 0;
                if (e.CommandName == "FNameClicked")
                {
                    firmid = Convert.ToInt32(((Label)(e.Item.FindControl("lbl_FirmID"))).Text);
                    string[] key ={ "@firmid" };
                    object[] values ={ firmid };
                    dr = ClsDB.Get_DR_BySPArr("usp_hts_get_firm_info", key, values);
                    if (dr.Read())
                    {
                        //Assigning values related to firm ID
                        txt_fname.Text = dr["FirmName"].ToString();
                        txt_sname.Text = dr["FirmAbbreviation"].ToString();
                        txt_add1.Text = dr["Address"].ToString();
                        txt_add2.Text = dr["Address2"].ToString();
                       
                        txt_city.Text = dr["City"].ToString();
                        cb_coverby.Checked = Convert.ToBoolean(dr["CanCoverBy"]);
                        cb_coverfrom.Checked = Convert.ToBoolean(dr["CanCoverFrom"]);

                        //Kamran 3580 03/31/08 Remove onpayroll and email code
                        

                        try
                        {
                            string phone = dr["Phone"].ToString();
                            if (phone.Length > 0)
                            {
                                txt_cc11.Text = phone.Substring(0, 3);
                                txt_cc12.Text = phone.Substring(3, 3);
                                txt_cc13.Text = phone.Substring(6, 4);
                                txt_cc14.Text = phone.Substring(10);
                            }
                        }
                        catch { }


                        try
                        {
                            ddl_state.SelectedValue = dr["State"].ToString();
                        }
                        catch { }
                        txt_zip.Text = dr["Zip"].ToString();
                        //	int fee=0;
                        //fee=String.Format("{0:G6}",Convert.ToInt32(dr["basefee"]));
                        txt_basefee.Text = Convert.ToInt32(dr["basefee"]).ToString();
                        //txt_basefee.Text=fee.ToString();
                        txt_daybeforefee.Text = Convert.ToInt32(dr["daybeforefee"]).ToString();
                        txt_juryfee.Text = Convert.ToInt32(dr["judgefee"]).ToString();
                        txt_firmid.Text = firmid.ToString();

                        // tahir 5084 10/06/2008 added send text message question...
                        cb_textmessage.Checked = Convert.ToBoolean(dr["sendtextmessage"].ToString());
                        txtCellNumber.Text = dr["textmessagenumber"].ToString();
                        //Sabir Khan 5298 12/03/2008 set sender number ...
                        txtFromnumber.Text = dr["SenderEmail"].ToString();
                        //txtNoticeDays.Text = dr["noticeperioddays"].ToString();
                        ddlNoticeDays.SelectedValue = dr["noticeperioddays"].ToString();

                        tr_textmessage.Style["display"] = cb_textmessage.Checked ? "block" : "none";
                        //Sabir Khan 5298 12/03/2008 set sender address...
                        if (tr_textmessage.Style["display"] == "none")
                            txtFromnumber.Text = "info@sullolaw.com";
                        // end 5084

                        btn_delete.Enabled = true;
                        //HttpContext.Current.Response.Write("<script> ShowHideRow(); </script>");

                        //Waqas 5864 06/30/2009
                        txt_AttFirstName.Text = dr["AttorenyFirstName"].ToString();
                        txt_AttLastName.Text = dr["AttorneyLastName"].ToString();
                        txt_AttBarNumber.Text = dr["AttorneyBarCardNumber"].ToString();
                        txt_AttEmailAddress.Text = dr["AttorneyEmailAddress"].ToString();
                        //Yasir Kamal 7150 01/01/2010 display attorney signature image.
                        SignatureImage.ImageUrl = "ShowImage.aspx?firmid=" + firmid;
                        SignatureImage.Style[HtmlTextWriterStyle.Display] = "block";
                        if(dr["AttorneySignatureImage"].ToString() != "" && dr["AttorneySignatureImage"] != null)
                        {
                            lblUpload.Text = "Change Signature Image:";
                        }
                        else
                        {
                            lblUpload.Text = "Upload Signature Image:";
                            SignatureImage.ImageUrl = "/Images/DefaultSignature.bmp";
                        }
                    }
                    lbl_message.Text = "";
                }
                else 
                {
                    FillGrid(e.CommandName);
                }
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
            finally
            {
                if (dr != null)
                {
                    dr.Close();
                }
            }
        }
        //Insert /Update Firm
        private void btn_Submit_Click(object sender, System.EventArgs e)
        {
            //Yasir Kamal 7150 01/01/2010 save attorney signature image.
            try
            {
            FileUpload img = (FileUpload)ImageUpload;
            Byte[] imgByte = null;
            bool hasImage = false;
            string imageType = string.Empty;
            string ext = string.Empty;
            string fileName = img.PostedFile.FileName;
          
          
            if (img.HasFile && img.PostedFile != null)
            {
                ext = fileName.Substring(fileName.LastIndexOf("."));
                ext = ext.ToLower();

                if (ext == ".jpg" || ext == ".jpeg" || ext == ".png" || ext == ".bmp" || ext == ".gif")
                {
                    //To create a PostedFile
                    HttpPostedFile File = ImageUpload.PostedFile;
                    //Create byte Array with file len
                    imgByte = new Byte[File.ContentLength];
                    //force the control to load data in array
                    File.InputStream.Read(imgByte, 0, File.ContentLength);
                    hasImage = true;
                }
                else
                {
                    HttpContext.Current.Response.Write("<script language='javascript'>alert('Please upload correct image file.')</script>");
                    return;
                }
            }

                // tahir 5084 11/06/2008 added send text message question.
                //Sabir Khan 5298 12/03/2008 @SenderEmail has been added for sender address...
                string[] key1 ={"@firmname","@shortname","@Address1","@Address2",
									"@city","@state","@zip",
									"@basefee","@daybeforefee","@judgefee","@firmid","@phone","@CanCoverFrom","@CanCoverBy",
                                    "@sendtextmessage","@textmessagenumber", "@SenderEmail","@noticeperioddays",
                               "@AttorneyFName", "@AttorneyLName", "@AttorneyBarCardNumber", "@AttorneyEmailAddress","@AttorneySignatureImage","@hasImage", "@isShortnamefound",};
                //active added on 6-11-07
                //Waqas 5864 06/30/2009 ALR
                //Fahad 7008 11/17/2009 ToUpper method added in FirmName in order to get FirmName in Capital Letters
                object[] value1 ={txt_fname.Text.Trim().ToUpper(),txt_sname.Text.Trim(),txt_add1.Text.Trim(),txt_add2.Text.Trim(),
									txt_city.Text.Trim(),Convert.ToInt32(ddl_state.SelectedValue),txt_zip.Text.Trim(),
									Convert.ToDouble(txt_basefee.Text),Convert.ToDouble(txt_daybeforefee.Text),
									Convert.ToDouble(txt_juryfee.Text),Convert.ToInt32(txt_firmid.Text),txt_cc11.Text+ txt_cc12.Text+ txt_cc13.Text+ txt_cc14.Text,cb_coverfrom.Checked,cb_coverby.Checked,
                                    cb_textmessage.Checked, txtCellNumber.Text.Trim(),txtFromnumber.Text.Trim(),Convert.ToInt16( ddlNoticeDays.SelectedValue ),txt_AttFirstName.Text, txt_AttLastName.Text, txt_AttBarNumber.Text, txt_AttEmailAddress.Text,imgByte,hasImage, 0};
                //7150 end.
                //ClsDB.InsertBySPArr("usp_hts_insert_firm_info", key1, value1);
                int exist = Convert.ToInt32(ClsDB.InsertBySPArrRet("usp_hts_insert_firm_info", key1, value1,true));
                //Sabir Khan 4990 10/28/2008 Display message if already exist...
                if (exist == 1)
                    lbl_message.Text = "Short name already exist.";
                else
                    lbl_message.Text = "";
                //Initializing
                txt_firmid.Text = "0";
                FillGrid();
                clear();
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
            
  


        }

        private void btn_delete_Click(object sender, System.EventArgs e)
        {
            try
            {
                string[] key1 = { "@firmid", "@Ret" };
                object[] value1 = { txt_firmid.Text, 0 };
                object retStr = ClsDB.InsertBySPArrRet("USP_HTS_delete_firm_Info", key1, value1);
                int ret = Convert.ToInt32(retStr);
                if (ret == 0)
                {
                    clear();
                    Response.Write("<script language='" + "javascript" + "'> alert('" + "Firm cannot be deleted,it contains records" + "'); </script>");

                }
                else if (ret == 1)
                {
                    FillGrid();
                    clear();

                    Response.Write("<script language='" + "javascript" + "'> alert('" + "Firm Deleted" + "'); </script>");

                }
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message.ToString();
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
       

        #endregion

        #region Methods

        //Fill Data Grid
		private void FillGrid()
		{
			try
			{
                //Sabir Khan 4990 10/18/2008 add parameter for Active and Inactive
                string[] key1 = { "@FirmFlag"};
                object[] value1 = { ActiveInactiveFlag };
                DataSet tempDs = ClsDB.Get_DS_BySPArr("usp_hts_get_list_all_firms", key1, value1);
                dv = new DataView(tempDs.Tables[0]);
                dgResult.DataSource = dv;
				dgResult.DataBind();
				if (dgResult.Items.Count<1)
				{
                    lbl_message.Text="No Record Found";
				}
			}
			catch(Exception ex)
			{
				lbl_message.Text=ex.Message;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}

        private void FillGrid(string sortExp)
        {
            try
            {
                if (Session["sortOrder"] == null)
                {
                    Session.Add("sortOrder", " ASC");
                }
                string s = Session["sortOrder"].ToString();
                if (s == " ASC")
                    Session["sortOrder"] = " DESC";
                else
                    Session["sortOrder"] = " ASC";

                sortExp += s;
                //Sabir Khan 4990 10/18/2008 add parameter for Active and Inactive
                string[] key1 = { "@FirmFlag" };
                object[] value1 = { ActiveInactiveFlag };
                DataSet tempDs = ClsDB.Get_DS_BySPArr("usp_hts_get_list_all_firms", key1, value1);
                dv = new DataView(tempDs.Tables[0]);
                dgResult.DataSource = dv;
                dv.Sort = sortExp;
                dgResult.DataBind();
                if (dgResult.Items.Count < 1)
                {
                    lbl_message.Text = "No Record Found";
                }
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

	    // Clear All Controls	
        private void clear()
		{
			// Clearing all the Controls & reset the firm ID to 0
			try
			{
				txt_fname.Text ="";
				txt_sname.Text ="";
				txt_add1.Text ="";
				txt_add2.Text ="";        
				txt_city.Text ="";
				ddl_state.SelectedIndex=0;
				txt_zip.Text="";
				txt_basefee.Text="";
				txt_daybeforefee.Text="";
				txt_juryfee.Text="";
				txt_firmid.Text= "0";
                txt_cc11.Text = "";
                txt_cc12.Text = "";
                txt_cc13.Text = "";
                txt_cc14.Text = "";
                //Sabir Khan 5298 12/03/2008 Set info@sullolaw.com
                txtFromnumber.Text = "info@sullolaw.com";
				btn_delete.Enabled  =false;
                cb_coverby.Checked = false;
                cb_coverfrom.Checked = false;
                cb_textmessage.Checked = false;
                txtCellNumber.Text = "";
                ddlNoticeDays.SelectedValue= "0";

                //Waqas 5864 06/30/2009 ALR Changes
                txt_AttFirstName.Text = "";
                txt_AttLastName.Text = "";
                txt_AttBarNumber.Text = "";
                txt_AttEmailAddress.Text = "";
                //Yasir Kamal 7150 01/01/2010 display attorney signature image.
                SignatureImage.ImageUrl = "";
                SignatureImage.Style[HtmlTextWriterStyle.Display] = "none";
               
			}
			catch(Exception ex)
			{
				lbl_message.Text = ex.Message.ToString();
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
        }

        //Sabir Khan 4990 10/18/2008 Active check box
        protected void chkActiveFirm_CheckedChanged(object sender, EventArgs e)
        {
            GetFirm();
        }
        //Sabir Khan 4990 10/18/2008 Inactive check box
        protected void chkInactive_CheckedChanged(object sender, EventArgs e)
        {
            GetFirm();
        }
        //Sabir Khan 4990 10/18/2008 Set flag for out side clients
        private void GetFirm()
        {
            try
            {
                if (chkActiveFirm.Checked == true && chkInactive.Checked == false)
                    ActiveInactiveFlag = 1;
                else
                    if (chkActiveFirm.Checked == true && chkInactive.Checked == true)
                        ActiveInactiveFlag = 0;
                    else
                        if (chkActiveFirm.Checked == false && chkInactive.Checked == true)
                            ActiveInactiveFlag = 2;
                FillGrid();
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        #endregion

       

        
    }
}

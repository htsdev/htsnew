using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using System.Configuration;
using lntechNew.Components;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;




namespace lntechNew.backroom
{
	/// <summary>
	/// Summary description for frmReportDesc.
	/// </summary>
	public partial class frmReportDesc : System.Web.UI.Page
	{
		string  ConStr = (string)ConfigurationSettings.AppSettings["connString"];
		protected System.Data.SqlClient.SqlConnection conn;
		protected System.Web.UI.WebControls.Label lblmsg;
		protected System.Web.UI.WebControls.Label lblreportname;
		protected System.Web.UI.WebControls.TextBox txtreportname;
		protected System.Web.UI.WebControls.Label Lblloc;
		protected System.Web.UI.WebControls.DropDownList dlistloc;
		protected System.Web.UI.WebControls.Label lblreportdesc;
		protected System.Web.UI.WebControls.TextBox txtreportdesc;
		protected System.Web.UI.WebControls.TextBox txtSpName;
		protected System.Web.UI.WebControls.TextBox txtchk;
		protected System.Web.UI.WebControls.TextBox txtNoParam;
		protected System.Web.UI.WebControls.TextBox TextBox2;
		clsLogger clog = new clsLogger();		
		string spName;
		protected System.Web.UI.WebControls.TextBox txtsp;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.TextBox txtRptID;
		int RPTID;
		int chkpage;
		DataSet dsSp = new DataSet();
		//SqlParameter [] param = new SqlParameter();
		protected System.Web.UI.WebControls.Label lblFooterMsg;
		protected System.Web.UI.WebControls.DataGrid Dg_Output;
		protected System.Web.UI.WebControls.TextBox txtchksp;
		protected System.Web.UI.WebControls.Button btnReset;
		protected System.Web.UI.WebControls.Button btnsubmit;
		protected System.Web.UI.WebControls.Button btnupdate;
		protected System.Web.UI.WebControls.HyperLink hlback;
        clsENationWebComponents clsdb = new clsENationWebComponents();
//		clsSession cSession = new clsSession();



		SqlTransaction SaveTrans;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			try
			{
				
				// Check Session 
				

				if ( Request.QueryString.Count == 3)
				hlback.NavigateUrl="../BackRoom/FrmReports.aspx";

				else
				hlback.NavigateUrl="../BackRoom/FrmSpList.aspx";

		 
				
				
				conn= new SqlConnection(ConStr);
				conn.Open(); 		
				btnsubmit.Attributes.Add("onclick","javascript: return ValidateInput();");
				btnupdate.Attributes.Add("onclick","javascript: return ValidateInput();");
                //Code edit by Azee
				//string sName = Request.QueryString["spName"];
                string sName = "";
                if (Request.QueryString.Count > 0)
                {
                    sName = Request.QueryString["spName"];
                }
                else
                {
                    sName = "%";    
                }
                //
				string stpName = Request.QueryString["spname"]; 
				string parm = Request.QueryString["prm"];

				txtsp.Text = sName;
                if (Request.QueryString["RptId"] != null)
                {
                    RPTID = Convert.ToInt32(Request.QueryString["RptId"]);				 // Type Casting
                }
                else
                {
                    RPTID = 0;
                }
                if (Request.QueryString["chkpage"] != null)
                {
                    chkpage = Convert.ToInt32(Request.QueryString["chkpage"]);
                }
                else
                {
                    chkpage = 0; 
                }
				lblmsg.Visible =true;
				lblmsg.Text = " ";
				lblreportname.Visible = true;
				Lblloc.Visible = true;
				lblreportdesc.Visible =true;
				txtreportname.Visible =true;
				txtreportdesc.Visible =true;
				// Comment By Zeeshan 
				//	Label3.Visible = true;
				btnReset.Visible = true;
				dlistloc.Visible = true;
				Dg_Output.Visible = true;
				btnsubmit.Visible = true;
				btnupdate.Visible = false;
				txtNoParam.Text="0";
				txtchk.Text = "0";
				spName = sName;
				
				if (chkpage == 1)
				{
					btnupdate.Visible = true;
					btnsubmit.Visible = false;
					txtsp.Text	= stpName;
				}
				
				if (IsPostBack !=true)
				{
					
					if(chkpage == 1)
					{
						GetValue(RPTID);
						txtRptID.Text = RPTID.ToString();
					}
					else 
					{	
						ListParameters(spName );
					}
				
				     FillCategory();
				}
				AddScripts();


               
               


			}	
			catch (Exception ex)
			{
				lblmsg.Text  = ex.Message ;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
				
			}
		}

		private void Update()
		{
			int Rptid = (int) Convert.ChangeType(txtRptID.Text,typeof(int)); 
			SaveTrans = conn.BeginTransaction();
			try
			{
               
            int catid = (int)(Convert.ChangeType(ddl_reportcategory.SelectedValue, typeof(int)));
            int usertypeid = (int)(Convert.ChangeType(ddl_usertype.SelectedValue, typeof(int)));

			SqlParameter [] param = new SqlParameter[6];

			param[0] = new SqlParameter("@RptID",Rptid);
			param[1] = new SqlParameter("@RptName",txtreportname.Text.Trim());
			param[2] = new SqlParameter("@RptDescription",txtreportdesc.Text.Trim());
			param[3] = new SqlParameter("@ReportLoc",(int) (Convert.ChangeType(dlistloc.SelectedValue,typeof(int)))); // Type Casting
            param[4] = new SqlParameter("@ReportCategoryID",catid.ToString());
            param[5] = new SqlParameter("@UserType", usertypeid.ToString());
			
				
				SqlHelper.ExecuteScalar(SaveTrans,CommandType.StoredProcedure,"usp_Update_Report_Procedure", param);				
				//if(lblmsg.Text == "No Parameter")
                if (TextBox2.Text == "0")
				{
				 
				}
				else
				{
					foreach(DataGridItem ItemX in Dg_Output.Items )	
					{
						string SpParamName = ((Label) (ItemX.FindControl("lblParamName")) ).Text; 
						string SpParamType = ((Label) (ItemX.FindControl("lblParamType")) ).Text;
						string SpParamAlias = ((TextBox) (ItemX.FindControl("txtParamAlias")) ).Text; 
						string SpParamDefValue;
						if(SpParamType.ToUpper()=="DATETIME")
						{
							//ObjType==3 OR SpParamType=="DATETIME" means CalendarPopup of eworld.ui
							SpParamDefValue = ((eWorld.UI.CalendarPopup)((ItemX.FindControl("dtPicker")))).SelectedDate.ToString("MM/dd/yyyy");
						}
						else
						{
							SpParamDefValue = ((TextBox) (ItemX.FindControl("txtParamDef")) ).Text;
						}


						bool Visibility = ((CheckBox) (ItemX.FindControl("chkVisibility")) ).Checked;
						int SpParamObjType = int.Parse(((DropDownList) (ItemX.FindControl("DDLObjType"))  ).SelectedValue);
						int SpParamCmdType = int.Parse(((DropDownList) (ItemX.FindControl("DDLCmdType"))  ).SelectedValue);
						string SpParamCmdText = ((TextBox) (ItemX.FindControl("txtCmd")) ).Text;

						//Check: Cmd Text is valid or not
						if(SpParamObjType==2)
						{
							DataSet ds = ExecuteCommandText(SpParamCmdType,SpParamCmdText);
							if(ds.Tables.Count==0) // for invalid Commmand Text
							{
								lblFooterMsg.Text = "Please enter valid Command Text";
								SaveTrans.Rollback(); 
								return;

							}
							else if(ds.Tables[0].Columns.Count<2)
							{
								lblFooterMsg.Text = "Please enter valid Command Text which must return 2 fields";
								SaveTrans.Rollback(); 
								return;
							}
						}

						int SpDetailID=Convert.ToInt32(((TextBox)(ItemX.FindControl("txt_DetailID")) ).Text);

						SqlParameter [] paramDetail = new SqlParameter[10]; 

						paramDetail[0] = new SqlParameter("@RptID",Rptid);
						paramDetail[1] = new SqlParameter("@ParamName",SpParamName);
						paramDetail[2] = new SqlParameter("@ParamType",SpParamType);
						paramDetail[3] = new SqlParameter("@ParamAlias ",SpParamAlias);
						paramDetail[4] = new SqlParameter("@DefaultVal",SpParamDefValue);
						paramDetail[5] = new SqlParameter("@ColVisibility",Visibility);
						paramDetail[6] = new SqlParameter("@ObjectType",SpParamObjType);
						paramDetail[7] = new SqlParameter("@CommandType",SpParamCmdType);
						paramDetail[8] = new SqlParameter("@CommandText",SpParamCmdText);
                        paramDetail[9] = new SqlParameter("@SpDetailID",SpDetailID);

						SqlHelper.ExecuteScalar(SaveTrans,CommandType.StoredProcedure,"usp_Update_Report_ProcedureDetail", paramDetail);				
					}
				}
				SaveTrans.Commit(); 
				ResetValue();	
				Response.Redirect("frmReports.aspx",false); 
			}		
			catch (Exception ex)
			{
				lblmsg.Text = ex.Message.ToString();
				SaveTrans.Rollback(); 
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}

		}
		// Fill datagrid and other fields for updating the records.
		private void GetValue(int Rptid)
		{
            //Change by Ajmal
            //SqlDataReader dr = SqlHelper.ExecuteReader(ConStr, CommandType.StoredProcedure, "usp_Get_Report_Procedure", new SqlParameter("@RptID", Rptid));
            IDataReader dr = SqlHelper.ExecuteReader(ConStr,CommandType.StoredProcedure ,"usp_Get_Report_Procedure", new SqlParameter("@RptID",Rptid ) );
			if (dr.Read())
			{
				txtreportname.Text = dr.GetString(0);
				txtreportdesc.Text = dr.GetString(1);
				//dlistloc.Items[dr.GetInt32(2)].Selected=true;
				dlistloc.SelectedValue=dr.GetInt32(2).ToString();
                ddl_reportcategory.SelectedValue = dr.GetInt32(3).ToString();
                ddl_usertype.SelectedValue = dr.GetInt32(4).ToString();
			}

			dr.Close(); 	
	
			try
			{
				DataSet dsGetParam =  SqlHelper.ExecuteDataset(ConStr,CommandType.StoredProcedure ,"usp_GetDetail_Report_Procedure", new SqlParameter("@RptID",Rptid ));
				
				Dg_Output.DataSource = dsGetParam;
				Dg_Output.DataBind();

				TextBox2.Text = Dg_Output.Items.Count.ToString();
			}
			catch(Exception ex)
			{
				lblmsg.Text = ex.Message;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}

		
		}

		// Javascript checking
		private void AddScripts() 
       	{
			try
			{
				foreach(DataGridItem ItemX in Dg_Output.Items )
				{
					DropDownList ddl = 	((DropDownList)(ItemX.FindControl("DDLObjType")));
					DropDownList ddl2 = ((DropDownList)(ItemX.FindControl("DDLCmdType")) );
					TextBox txt		 =	((TextBox)(ItemX.FindControl("txtCmd"))) ;
					string str = "ShowHide('"   + ((DropDownList)(ItemX.FindControl("DDLObjType"))).ClientID +   "','"   + ((TextBox)(ItemX.FindControl("txtCmd"))).ClientID +   "','"   + ((DropDownList)(ItemX.FindControl("DDLCmdType")) ).ClientID +  "')";
					((DropDownList)(ItemX.FindControl("DDLObjType"))).Attributes.Add("onclick",str);
				}
			}
			catch ( Exception ex )
			{
			lblmsg.Text=ex.Message;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
                
			}

		}
		
		// Invisible datagrid and other fields
		private void visible()
		{
			lblreportname.Visible = false;
			lblreportdesc.Visible =false;
			txtreportname.Visible = false;
			txtreportdesc.Visible = false;
			Lblloc.Visible = false;
			//Comment By Zeeshan Jur
			//Label3.Visible = false;
			btnReset.Visible = false;
			dlistloc.Visible = false;
			btnsubmit.Visible = false;
			Dg_Output.Visible=false;
		}

		// Get parameter name and type in datagrid
		private void ListParameters(string spName)
		{
			try
			{
                // Code Modified BY Fahad perviously we are not using out database component here . now i comment old code & use our DB component ( Fahad - 26th-Jan 2008 )budid = 2484
				//DataSet ds =  SqlHelper.ExecuteDataset(ConStr,CommandType.StoredProcedure ,"usp_sp_help", new SqlParameter("@objname",spName ));
                string[] keys = {"@objname"};
                object[] values = {spName};
                DataSet ds = clsdb.Get_DS_BySPArr("usp_sp_help", keys, values);
                //End ( Fahad - 26th-Jan 2008 )budid = 2484
				if (ds.Tables[0].Rows.Count > 0) 
				{
					lblmsg.Text = "";
					Dg_Output.DataSource = ds ;
					Dg_Output.DataBind();
					TextBox2.Text = ds.Tables[0].Rows.Count.ToString();
				}
				else
				{
					lblmsg.Text = "No Parameter";
					txtNoParam.Text="0";
					visibility();
					return;
				}
			}
			catch (Exception ex)
			{
				lblmsg.Text  = ex.Message ;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}		
		}
		
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Dg_Output.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.Dg_Output_ItemDataBound);
			this.Dg_Output.SelectedIndexChanged += new System.EventHandler(this.Dg_Output_SelectedIndexChanged);
			this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
			this.btnupdate.Click += new System.EventHandler(this.btnupdate_Click);
			this.btnsubmit.Click += new System.EventHandler(this.btnsubmit_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		
		//unvisible datagrid
		private void visibility()
		{
			Dg_Output.Visible=false;
		}
		

		private void btnsubmit_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			try
			{
				checkSP(spName);
				if ( dsSp.Tables.Count <= 0)
				{	
					if (lblmsg.Text.Length  > 18)
					{
						if(lblmsg.Text.Substring(0,19)  == "Invalid object name" )
						{
							lblmsg.Text = lblmsg.Text + " in this procedure " + spName;
						}
					}
					else
					{
						lblmsg.Text = "You can't save this report as the selected procedure inserts or updates the database. "; 
					}
				}
				else
				SaveReportStructure();
				
			}
			catch(Exception ex)
			{
				lblmsg.Text =  ex.Message ; 
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}

		}

		// Restrict non returnable stored procedure
		private void checkSP(string spName)
		{
			try
			{
				
				int idx=0;
				SqlParameter [] param = SqlHelperParameterCache.GetSpParameterSet(ConStr,spName,false);
				foreach(DataGridItem ItemX in Dg_Output.Items )	
				{		
					string SpParamName = ((Label) (ItemX.FindControl("lblParamName"))).Text; 
					string SpParamType = ((Label) (ItemX.FindControl("lblParamType"))).Text;
					string SpParamAlias = ((TextBox) (ItemX.FindControl("txtParamAlias"))).Text; 
					string SpParamDefValue;
					if(SpParamType.ToUpper()=="DATETIME")
					{
						SpParamDefValue = ((eWorld.UI.CalendarPopup)((ItemX.FindControl("dtPicker")))).SelectedDate.ToString("MM/dd/yyyy");
					}
					else
					{
						SpParamDefValue = ((TextBox) (ItemX.FindControl("txtParamDef")) ).Text;
					}
					param[idx] =  new SqlParameter(SpParamName,SpParamDefValue);		
					idx++;
				}
				dsSp = SqlHelper.ExecuteDataset(ConStr,CommandType.StoredProcedure,spName,param);	
			}
	
			catch(Exception ex)
			{
				lblmsg.Text =  ex.Message ; 
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
				
		
		}
	
		// Save Report
		private void SaveReportStructure()
		{
			SaveTrans = conn.BeginTransaction();
			int rptId;
			rptId=0;            
            
            int catid = (int)(Convert.ChangeType(ddl_reportcategory.SelectedValue, typeof(int)));
            int usertypeid = (int)(Convert.ChangeType(ddl_usertype.SelectedValue, typeof(int)));
            
			SqlParameter [] param = new SqlParameter[6];
			param[0] = new SqlParameter("@spname",spName);
			param[1] = new SqlParameter("@rptname",txtreportname.Text.Trim());
			param[2] = new SqlParameter("@RptDescription",txtreportdesc.Text.Trim());
			param[3] = new SqlParameter("@ReportLoc",(int) (Convert.ChangeType(dlistloc.SelectedValue,typeof(int)))); // Type Casting
            param[4] = new SqlParameter("@ReportCategoryID", catid.ToString());
            param[5] = new SqlParameter("@UserType", usertypeid.ToString());

			try
			{
				rptId= (int) (Convert.ChangeType( SqlHelper.ExecuteScalar(SaveTrans,CommandType.StoredProcedure,"usp_Save_Report_Procedure", param),typeof(int))) ;				
				if(lblmsg.Text == "No Parameter")
				{
				 
				}
				else
				{
					foreach(DataGridItem ItemX in Dg_Output.Items )	
					{
						string SpParamName = ((Label) (ItemX.FindControl("lblParamName"))  ).Text; 
						string SpParamType = ((Label) (ItemX.FindControl("lblParamType"))  ).Text;
						string SpParamAlias = ((TextBox) (ItemX.FindControl("txtParamAlias")) ).Text;
						string SpParamDefValue;
 						if(SpParamType.ToUpper()=="DATETIME")
						{
							//ObjType==3 OR SpParamType=="DATETIME" means CalendarPopup of eworld.ui
							SpParamDefValue = ((eWorld.UI.CalendarPopup)((ItemX.FindControl("dtPicker")))).SelectedDate.ToString("MM/dd/yyyy");
						}
						else
						{
							SpParamDefValue = ((TextBox) (ItemX.FindControl("txtParamDef")) ).Text;
						}

						//string temp =  ((TextBox) (ItemX.FindControl("temp"))).Text ;
						bool   Visibility = ((CheckBox) (ItemX.FindControl("chkVisibility")) ).Checked;
						int SpParamObjType = int.Parse(((DropDownList) (ItemX.FindControl("DDLObjType"))  ).SelectedValue);
						int SpParamCmdType = int.Parse(((DropDownList) (ItemX.FindControl("DDLCmdType"))  ).SelectedValue);
						string SpParamCmdText = ((TextBox) (ItemX.FindControl("txtCmd")) ).Text;
						//Check: Cmd Text is valid or not
						if(SpParamObjType==2)
						{
							DataSet ds = ExecuteCommandText(SpParamCmdType,SpParamCmdText);
							if(ds.Tables.Count==0) // for invalid Commmand Text
							{
								lblFooterMsg.Text = "Please enter valid Command Text";
								SaveTrans.Rollback(); 
								return;
							}
							else if(ds.Tables[0].Columns.Count<2)
							{
								lblFooterMsg.Text = "Please enter valid Command Text which must return 2 fields";
								SaveTrans.Rollback(); 
								return;
							}
						}
						SqlParameter [] paramDetail = new SqlParameter[9]; 
						paramDetail[0] = new SqlParameter("@rptid",rptId);
						paramDetail[1] = new SqlParameter("@ParamName",SpParamName);
						paramDetail[2] = new SqlParameter("@ParamType",SpParamType);
						paramDetail[3] = new SqlParameter("@ParamAlias ",SpParamAlias);
						paramDetail[4] = new SqlParameter("@DefaultVal",SpParamDefValue);
						paramDetail[5] = new SqlParameter("@ColVisibility",Visibility);
						paramDetail[6] = new SqlParameter("@ObjectType",SpParamObjType);
						paramDetail[7] = new SqlParameter("@CommandType",SpParamCmdType);
						paramDetail[8] = new SqlParameter("@CommandText",SpParamCmdText);
						SqlHelper.ExecuteScalar(SaveTrans,CommandType.StoredProcedure,"usp_SaveDetail_Report_Procedure", paramDetail);				
					}
				}
				SaveTrans.Commit(); 
				Response.Redirect("frmReports.aspx",false); 
			}		
			catch (Exception ex)
			{
				lblmsg.Text = ex.Message.ToString();
				SaveTrans.Rollback(); 
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}

		// Reset The Value of controls
		private void ResetValue()
		{
			try
			{
				txtreportname.Text = "";
				txtreportdesc.Text = "";
				dlistloc.SelectedIndex = 0;
				if (lblmsg.Text == "No Parameter")
				{
				}
				else
				{
					foreach(DataGridItem ItemX in Dg_Output.Items)
					{
						((TextBox)      (ItemX.FindControl("txtParamAlias")) ).Text = ""; 
						((TextBox)      (ItemX.FindControl("txtParamDef")  ) ).Text= "";
						((DropDownList) (ItemX.FindControl("DDLObjType")   ) ).SelectedIndex   = 0;
						((DropDownList) (ItemX.FindControl("DDLCmdType")   ) ).SelectedIndex   = 0;
						((TextBox)      (ItemX.FindControl ("txtCmd")      ) ).Text ="";
 
					}
				}
			}
			catch (Exception ex)
			{
				lblmsg.Text = ex.Message.ToString();
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}
		

		private void Dg_Output_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			try
			{
				//if Param Type is datetime
				if(e.Item.ItemIndex!=-1)   //for Header and Footer
				{
					string strType = ((Label)e.Item.FindControl("lblParamType")).Text;
					if( strType.ToUpper()=="DATETIME")
					{
						((TextBox)(e.Item.FindControl("txtParamDef"))).Visible = false;
						((eWorld.UI.CalendarPopup)((e.Item.FindControl("dtPicker")))).Visible = true;
						//for assigning Default value into Calender
                        
						string strDefVal = ((TextBox)e.Item.FindControl("txtParamDef")).Text;
                        try
                        {
                            ((eWorld.UI.CalendarPopup)((e.Item.FindControl("dtPicker")))).SelectedDate = Convert.ToDateTime(strDefVal);
                        }
                        catch { }
						((DropDownList)e.Item.FindControl("DDLObjType")).SelectedIndex=2;
						((DropDownList)e.Item.FindControl("DDLObjType")).Enabled=false;
					}
					else
					{
						((TextBox)(e.Item.FindControl("txtParamDef"))).Visible = true;
						((eWorld.UI.CalendarPopup)((e.Item.FindControl("dtPicker")))).Visible = false;
					}
					if(chkpage == 1)
					{
						string ObjType = ((string) (Convert.ChangeType( DataBinder.Eval(e.Item.DataItem, "ObjectType") ,typeof(string))));  
									
						ResetDropDownSelect(((DropDownList) (e.Item.FindControl("DDLObjType"))));
						((DropDownList) (e.Item.FindControl("DDLObjType"))).Items.FindByValue(ObjType).Selected=true;


						string CmdType = ((string) (Convert.ChangeType( DataBinder.Eval(e.Item.DataItem, "CommandType") ,typeof(string))));  
						ResetDropDownSelect(((DropDownList) (e.Item.FindControl("DDLCmdType"))));
						((DropDownList) (e.Item.FindControl("DDLCmdType"))).Items.FindByValue(CmdType).Selected=true;
					}
				}
			}
			catch(Exception ex)
			{
				lblmsg.Text = ex.Message;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}
		private void ResetDropDownSelect(DropDownList ddl)
		{
		
			try
			{
				int idx=0;
				for (idx = 0; idx< ddl.Items.Count; idx++)
				{
					ddl.Items[idx].Selected=false;
				}
			}
			catch ( Exception ex )
			{
				lblmsg.Text=ex.Message + "Line :592";
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
                
			}

		}

		// Update the Records
		private void btnupdate_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
            //Update();
		}

		
		private void Dg_Output_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}

		private DataSet ExecuteCommandText(int CmdType,string CmdText)
		{
			DataSet ds = new DataSet();
			try
			{
				if (CmdType == 1)
				{
					// CmdType == 1 means inline Qry
					ds = SqlHelper.ExecuteDataset(ConStr,CommandType.Text,CmdText.Trim());
				}
				else if (CmdType == 2)
				{
					// CmdType == 2 means SP
					ds = SqlHelper.ExecuteDataset(ConStr,CommandType.StoredProcedure,CmdText.Trim());
				}
			}
			catch(Exception ex)
			{
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
				return ds;
			}

			return ds;
		}
		
		// Reset all fields

        private void btnupdate_Click(object sender, System.EventArgs e)
        {
            Update();
        }

		private void btnReset_Click(object sender, System.EventArgs e)
		{
			ResetValue();
		}

		private void btnsubmit_Click(object sender, System.EventArgs e)
		{
			try
			{
				checkSP(spName);
				if ( dsSp.Tables.Count <= 0)
				{	
					if (lblmsg.Text.Length  > 18)
					{
						if(lblmsg.Text.Substring(0,19)  == "Invalid object name" )
						{
							lblmsg.Text = lblmsg.Text + " in this procedure " + spName;
						}
					}
					else
					{
						lblmsg.Text = "You can't save this report as the selected procedure inserts or updates the database. "; 
					}
				}
				else
					SaveReportStructure();
				
			}
			catch(Exception ex)
			{
				lblmsg.Text =  ex.Message ; 
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}





        private void FillCategory()
        {
            try
            {
                // Fill DropDown 

                ddl_reportcategory.Items.Clear();
                
                DataSet ds = SqlHelper.ExecuteDataset(ConStr, CommandType.StoredProcedure, "usp_Get_AllReports");
                                
                ddl_reportcategory.DataTextField = "CategoryName";
                ddl_reportcategory.DataValueField = "CategoryID";
                ddl_reportcategory.DataSource = ds.Tables[1];             
                ddl_reportcategory.DataBind();

                
               
            }
            catch (Exception ex)
            {
                lblmsg.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
		


	
	}
}

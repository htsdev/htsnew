<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.backroom.SqlSrvMrg.frmExeSP" Codebehind="frmExeSP.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>frmExeSP</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
				<TR>
					<TD>
						<P><STRONG>&nbsp;<IMG height="18" src="../../Images/head_icon.gif" width="25">&nbsp;List 
								of Parameter</STRONG></P>
					</TD>
					<TD style="WIDTH: 619px" background="../../images/headbar_footerextend.gif" colSpan="2"
						height="10"></TD>
				</TR>
				<TR>
					<TD><asp:datagrid id="Dg_Output" runat="server" CssClass="clsLeftPaddingTable" Font-Size="Smaller"
							CellPadding="3" BorderWidth="1px" BorderStyle="None" Width="780px" AutoGenerateColumns="False">
							<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#669999"></SelectedItemStyle>
							<ItemStyle ForeColor="#000066"></ItemStyle>
							<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
							<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Parameter Name">
									<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=lblParamName runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Parameter_name") %>' CssClass="label">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Parameter Type">
									<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=lblParamType runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Type") %>' CssClass="label">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Parameter Value">
									<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
									<ItemTemplate>
										<asp:TextBox id="txtParamValue" Width="90px" CssClass="clsinputadministration" EnableViewState="True"
											Visible="True" Runat="server"></asp:TextBox>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False" HeaderText="Parameter Length">
									<ItemTemplate>
										<asp:Label id=lblParamLength runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Length") %>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Left" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
						</asp:datagrid></TD>
				<tr>
					<td class="clsleftpaddingtable"><asp:button id="Button1" runat="server" CssClass="clsbutton" Text="Run Procedure"></asp:button></td>
					</TD></tr>
				<TR>
					<TD align="center"><asp:label id="lblmsg" runat="server" CssClass="label"></asp:label><asp:datagrid id="DataGrid1" runat="server"></asp:datagrid></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>

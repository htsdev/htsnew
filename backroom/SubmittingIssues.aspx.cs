using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using System.IO;
using lntechNew.Components;

namespace lntechNew.backroom
{
    public partial class SubmittingIssues : System.Web.UI.Page
    {
        clsENationWebComponents ClsDb = new clsENationWebComponents();		

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindControls();
            }
        }
        private void BindControls()
        {
            try
            {
                #region
                DataSet DS_Matter = ClsDb.Get_DS_BySP("usp_hts_get_matterpages");
                if (DS_Matter.Tables[0].Rows.Count > 0)
                {
                    ddl_matter.Items.Clear();
                    for (int i = 0; i < DS_Matter.Tables[0].Rows.Count; i++)
                    {
                        string PageName = DS_Matter.Tables[0].Rows[i]["title"].ToString().Trim();
                        string id = DS_Matter.Tables[0].Rows[i]["id"].ToString().Trim();
                        ddl_matter.Items.Add(new ListItem(PageName, id));
                    }
                }
                #endregion

               #region
                DataSet DS_Activities = ClsDb.Get_DS_BySP("usp_hts_get_activitiespages");
                if (DS_Activities.Tables[0].Rows.Count > 0)
                {
                    ddl_activities.Items.Clear();
                    for (int i = 0; i < DS_Activities.Tables[0].Rows.Count; i++)
                    {
                        string PageName = DS_Activities.Tables[0].Rows[i]["title"].ToString().Trim();
                        string id = DS_Activities.Tables[0].Rows[i]["id"].ToString().Trim();
                        ddl_activities.Items.Add(new ListItem(PageName, id));
                    }
                }
               #endregion

                #region
                DataSet DS_Reports = ClsDb.Get_DS_BySP("usp_hts_get_reportpages");
                if (DS_Reports.Tables[0].Rows.Count > 0)
                {
                    ddl_reports.Items.Clear();
                    for (int i = 0; i < DS_Reports.Tables[0].Rows.Count; i++)
                    {
                        string PageName = DS_Reports.Tables[0].Rows[i]["title"].ToString().Trim();
                        string id = DS_Reports.Tables[0].Rows[i]["id"].ToString().Trim();
                        ddl_reports.Items.Add(new ListItem(PageName, id));
                    }
                
                }
                #endregion

                #region
                DataSet DS_Validation = ClsDb.Get_DS_BySP("usp_hts_get_validationpages");
                if (DS_Validation.Tables[0].Rows.Count > 0)
                {
                    ddl_validation.Items.Clear();
                    for (int i = 0; i < DS_Validation.Tables[0].Rows.Count; i++)
                    {
                        string PageName = DS_Validation.Tables[0].Rows[i]["title"].ToString().Trim();
                        string id = DS_Validation.Tables[0].Rows[i]["id"].ToString().Trim();
                        ddl_validation.Items.Add(new ListItem(PageName, id));
                    }
                    
                }

                #endregion

                #region
                DataSet DS_Backroom = ClsDb.Get_DS_BySP("usp_hts_get_backroompages");
                if (DS_Backroom.Tables[0].Rows.Count > 0)
                {
                    ddl_backroom.Items.Clear();
                    for (int i = 0; i < DS_Backroom.Tables[0].Rows.Count; i++)
                    {
                        string PageName = DS_Backroom.Tables[0].Rows[i]["title"].ToString().Trim();
                        string id = DS_Backroom.Tables[0].Rows[i]["id"].ToString().Trim();
                        ddl_backroom.Items.Add(new ListItem(PageName, id));
                    }
                }

                #endregion

            }
            catch (Exception ex)
            {   
                
            }
        }
    }
}

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using AjaxControlToolkit;
using HTP.Components;

namespace HTP.backroom
{
    public partial class AutoDialerSettings_bkp : System.Web.UI.Page
    {
        #region Variables

        AutoDialer clsAD = new AutoDialer();

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetEventTypes();
            }
        }

        private void GetEventTypes()
        {
            DataTable dt = clsAD.GetEventConfigSettings();
            string eventTypeName = String.Empty;
            short eventTypeID = 0;

            if (dt.Rows.Count > 0)
            {
                tabs_EventType.Tabs.Clear();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    eventTypeID = Convert.ToInt16(dt.Rows[i]["EventTypeID"]);
                    eventTypeName = dt.Rows[i]["EventTypeName"].ToString();
                    CreateTabs(eventTypeID, eventTypeName);
                }
                tabs_EventType.ActiveTabIndex = 0; 
            }  
        }
        private void CreateTabs(short id, string name)
        {
            TabPanel myTab = new TabPanel();
            myTab.HeaderText = name;
            myTab.ID = id.ToString();
            Session["EventTypeID"] = id;
            myTab.ContentTemplate = Page.LoadTemplate("~/webcontrols/AutoDialerSettings.ascx");
            tabs_EventType.Controls.Add(myTab);                                
        }
    }
}

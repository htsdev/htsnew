using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;
using Encrypt;
using lntechNew.Components;

namespace HTP.backroom
{
    public partial class frmEmailedTrialLetter : System.Web.UI.Page
    {
        clsSession ClsSession = new clsSession(); //Fahad 6429 09/14/2009 Session Varibale Declared
        clsLogger clog = new clsLogger();
        TrialLetter trial = new TrialLetter();

        DataView DV;
        string StrAcsDec = String.Empty;
        string StrExp = String.Empty;


        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false)//Fahad 6429 09/14/2009 if Session is not exist then rdirect to login page
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else
                {
                    if (!IsPostBack)
                    {
                        cal_fromDate.Reset();
                        cal_todate.Reset();
                        cal_fromDate.SelectedDate = DateTime.Now;
                        cal_todate.SelectedDate = DateTime.Now;
                        FillGrid();

                    }

                    //Fahad 6429 09/18/2009 Categorised the Type "TrialNotification" and "ThankEmail" 
                    if (ddlEmailType.SelectedItem.Value == "Trial")
                    {
                        Pagingctrl.GridView = DG_TrialLetterEmailed;
                    }
                    else
                    {
                        Pagingctrl.GridView = gv_Data;
                    }
                    Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
                    Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
                    //DG_TrialLetterEmailed.PageSize = 20;
                    //gv_Data.PageSize = 20;
                    //FillGrid();

                }
            }
            catch (Exception ex)
            {
                lbl_Msg.Text = ex.Message;
                lbl_Msg.Visible = true;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }




        /// <summary>
        /// Method when Page index changed
        /// </summary>
        void Pagingctrl_PageIndexChanged()
        {
            try
            {
                //Fahad 6429 09/18/2009 Categorised the Type "TrialNotification" and "ThankEmail" 
                if (ddlEmailType.SelectedItem.Value == "Trial")
                {
                    DG_TrialLetterEmailed.PageIndex = Pagingctrl.PageIndex - 1;
                    FillGrid();
                    Pagingctrl.SetPageIndex();
                }
                else
                {
                    gv_Data.PageIndex = Pagingctrl.PageIndex - 1;
                    FillGrid();
                    Pagingctrl.SetPageIndex();
                }

            }
            catch (Exception ex)
            {
                lbl_Msg.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }


        /// <summary>
        /// Method when page size will be changed
        /// </summary>
        /// <param name="pageSize"></param>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            try
            {
                //Fahad 6429 09/18/2009 Categorised the Type "TrialNotification" and "ThankEmail" 
                if (ddlEmailType.SelectedItem.Value == "Trial")
                {
                    if (pageSize > 0)
                    {
                        DG_TrialLetterEmailed.PageIndex = 0;
                        DG_TrialLetterEmailed.PageSize = pageSize;
                        DG_TrialLetterEmailed.AllowPaging = true;
                    }
                    else
                    {
                        DG_TrialLetterEmailed.AllowPaging = false;
                    }
                }
                else
                {
                    if (pageSize > 0)
                    {
                        gv_Data.PageIndex = 0;
                        gv_Data.PageSize = pageSize;
                        gv_Data.AllowPaging = true;
                    }
                    else
                    {
                        gv_Data.AllowPaging = false;
                    }

                }

                FillGrid();
            }
            catch (Exception ex)
            {
                lbl_Msg.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }


        private void FillGrid()
        {
            try
            {
                lbl_Msg.Text = "";
                //Ozair 7405 02/16/2010 selectedvalue used instead of seleteditem.value
                if (ddlEmailType.SelectedValue == "Trial")
                {
                    trial.StEmailedDate = cal_todate.SelectedDate;
                    trial.EndEmailedDate = cal_fromDate.SelectedDate;
                    DataSet ds = trial.GetTrialLetterEmail();

                    if (ds != null)
                    {
                        // checking if dataset return any rows then genrate serial no
                        if (ds.Tables[0].Rows.Count > 0)
                        {

                            lbl_Msg.Visible = false;
                            if (Session["sDV"] == null)
                            {
                                DV = new DataView(ds.Tables[0]);
                                Session["sDV"] = DV;
                                GenerateSerial(ds.Tables[0], "Trial");
                            }
                            else
                            {
                                DV = (DataView)Session["sDV"];
                            }

                        }
                        else
                        {
                            Pagingctrl.Visible = false;
                            lbl_Msg.Visible = true;
                            lbl_Msg.Text = "No Record Found";
                        }

                        DG_TrialLetterEmailed.DataSource = DV;
                        DG_TrialLetterEmailed.DataBind();
                        DG_TrialLetterEmailed.Visible = true;

                        gv_Data.Visible = false;
                        Pagingctrl.Visible = true;
                        Pagingctrl.PageIndex = DG_TrialLetterEmailed.PageIndex;
                        Pagingctrl.PageCount = DG_TrialLetterEmailed.PageCount;
                        Pagingctrl.SetPageIndex();

                    }
                }
                else//Fahad 6429 09/18/2009 Calling of Fill Grid method for ThankEmail
                {
                    trial.ThankEmailStDate = cal_todate.SelectedDate;
                    trial.ThankEmailEndDate = cal_fromDate.SelectedDate;
                    DataSet ds = trial.GetThankEmail();

                    if (ds != null)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {

                            lbl_Msg.Visible = false;
                            if (Session["sDV"] == null)
                            {
                                DV = new DataView(ds.Tables[0]);
                                Session["sDV"] = DV;
                                GenerateSerial(ds.Tables[0], "Thank");
                            }
                            else
                            {
                                DV = (DataView)Session["sDV"];
                            }

                        }
                        else
                        {
                            //Pagingctrl.Visible = false;
                            lbl_Msg.Visible = true;
                            lbl_Msg.Text = "No Record Found";
                        }

                        gv_Data.DataSource = DV;
                        gv_Data.DataBind();
                        DG_TrialLetterEmailed.Visible = false;
                        gv_Data.Visible = true;

                        Pagingctrl.PageIndex = gv_Data.PageIndex;
                        Pagingctrl.PageCount = gv_Data.PageCount;
                        Pagingctrl.SetPageIndex();
                    }

                }


            }

            catch (Exception ex)
            {
                lbl_Msg.Text = ex.Message;
                lbl_Msg.Visible = true;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                Session["sDV"] = null;
                DG_TrialLetterEmailed.PageIndex = 0;
                FillGrid();
            }
            catch (Exception ex)
            {
                lbl_Msg.Text = ex.Message;
                lbl_Msg.Visible = true;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        //It will genrate serial numbers according to same mudnumber 
        private void GenerateSerial(DataTable dt, string ReportType)
        {
            try
            {
                if (ReportType == "Trial")
                {
                    int no = 1;
                    string ticketid = "";
                    if (dt.Columns["SNO"] == null)
                    {
                        dt.Columns.Add("SNO");
                        dt.Rows[0]["SNO"] = no;
                    }
                    else
                    {
                        dt.Columns.Remove("SNO");
                        dt.Columns.Add("SNO");
                        dt.Rows[0]["SNO"] = no;
                    }
                    ticketid = dt.Rows[0]["TicketID_PK"].ToString();

                    for (int i = 1; i < dt.Rows.Count; i++)
                    {

                        string tid = dt.Rows[i]["TicketID_PK"].ToString();
                        if (tid != "")
                        {
                            if (ticketid != tid)
                            {
                                no++;
                                dt.Rows[i]["SNO"] = no;
                                ticketid = tid;

                            }
                        }
                    }
                }
                else if (ReportType == "Thank")
                {
                    int no = 1;
                    string ticketid = "";
                    if (dt.Columns["SNO"] == null)
                    {
                        dt.Columns.Add("SNO");
                        dt.Rows[0]["SNO"] = no;
                    }
                    else
                    {
                        dt.Columns.Remove("SNO");
                        dt.Columns.Add("SNO");
                        dt.Rows[0]["SNO"] = no;
                    }
                    //Fahad 12/26/2009 7189 Removed PersonID and included "TicketID_PK" for generating serial Number.
                    ticketid = dt.Rows[0]["TicketID_PK"].ToString();

                    for (int i = 1; i < dt.Rows.Count; i++)
                    {

                        string tid = dt.Rows[i]["TicketID_PK"].ToString();
                        if (tid != "")
                        {
                            if (ticketid != tid)
                            {
                                no++;
                                dt.Rows[i]["SNO"] = no;
                                ticketid = tid;

                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                lbl_Msg.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void DG_TrialLetterEmailed_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;
                if (drv == null)
                    return;

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ((HyperLink)e.Row.FindControl("hp_sno")).NavigateUrl = "../ClientInfo/violationfeeold.aspx?search=0&casenumber=" + (int)drv["ticketid_pk"];

                    HiddenField hf_confirmationid = (HiddenField)e.Row.FindControl("hf_emailconfirmation");
                    if (hf_confirmationid.Value == "0")
                        ((Image)e.Row.FindControl("img_status")).ImageUrl = "../Images/cross.GIF";

                    HiddenField hfticketid = (HiddenField)e.Row.FindControl("hf_ticketid");
                    string ticketid = hfticketid.Value;

                    ticketid = CLRClass.EncryptFunction(ticketid);
                    string emailid = CLRClass.EncryptFunction("123");
                    ((HyperLink)e.Row.FindControl("hl_clientname")).NavigateUrl = "http://www.sullolaw.com/Reports/frmTrialNotification.aspx?casenumber=" + ticketid + "&EmailID=" + emailid + "&Callertype=0";
                }



            }
            catch (Exception ex)
            {
                lbl_Msg.Text = ex.Message;
                lbl_Msg.Visible = true;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void BindReport()
        {

            try
            {
                long sNo = (DG_TrialLetterEmailed.PageIndex) * (DG_TrialLetterEmailed.PageSize);

                foreach (GridViewRow ItemX in DG_TrialLetterEmailed.Rows)
                {
                    sNo += 1;

                    ((HyperLink)(ItemX.FindControl("hp_sno"))).Text = sNo.ToString();
                }
            }
            catch (Exception ex)
            {
                lbl_Msg.Text = ex.Message;
                lbl_Msg.Visible = true;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }


        private void SetAcsDesc(string Val)
        {
            try
            {
                StrExp = Session["StrExp"].ToString();
                StrAcsDec = Session["StrAcsDec"].ToString();
            }
            catch (Exception ex)
            {
                lbl_Msg.Text = ex.Message;
                lbl_Msg.Visible = true;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

            if (StrExp == Val)
            {
                if (StrAcsDec == "ASC")
                {
                    StrAcsDec = "DESC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
                else
                {
                    StrAcsDec = "ASC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
            }
            else
            {
                StrExp = Val;
                StrAcsDec = "ASC";
                Session["StrExp"] = StrExp;
                Session["StrAcsDec"] = StrAcsDec;
            }
        }

        private void SortGrid(string SortExp)
        {
            try
            {
                SetAcsDesc(SortExp);
                DV = (DataView)Session["sDV"];
                DV.Sort = StrExp + " " + StrAcsDec;

                FillGrid();

            }
            catch (Exception ex)
            {
                lbl_Msg.Text = ex.Message;
                lbl_Msg.Visible = true;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void DG_TrialLetterEmailed_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                DG_TrialLetterEmailed.PageIndex = e.NewPageIndex;
                FillGrid();

            }
            catch (Exception ex)
            {
                lbl_Msg.Text = ex.Message;
                lbl_Msg.Visible = true;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        protected void ddlEmailType_SelectedIndexChanged(object sender, EventArgs e)
        {

            cal_fromDate.Reset(DateTime.Now);
            cal_todate.Reset(DateTime.Now);
            cal_todate.Clear();
            cal_fromDate.Clear();
            //cal_fromDate.SelectedDate = DateTime.Now;
            //cal_todate.SelectedDate = DateTime.Now;
            //Fahad 6429 09/18/2009 Categorised the Type "TrialNotification" and "ThankEmail" 
            //Ozair 7405 02/16/2010 Code refactored
            if (ddlEmailType.SelectedValue == "Trial")
            {
                hp_TrialletterBatch.Visible = true;
            }
            else
            {
                hp_TrialletterBatch.Visible = false;
            }
            DG_TrialLetterEmailed.Visible = false;
            gv_Data.Visible = false;
            Session["sDV"] = null;
            FillGrid();
        }

        protected void gv_Data_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_Data.PageIndex = e.NewPageIndex;
                FillGrid();

            }
            catch (Exception ex)
            {
                lbl_Msg.Text = ex.Message;
                lbl_Msg.Visible = true;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }



        }

        protected void DG_TrialLetterEmailed_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                SortGrid(e.SortExpression);
            }
            catch (Exception ex)
            {
                lbl_Msg.Text = ex.Message;
                lbl_Msg.Visible = true;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

    }
}

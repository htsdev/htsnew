﻿<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.frmTemplateDetail" Codebehind="frmTemplateDetail.aspx.cs" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Template Detail</title>
		<%--<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">--%>


        <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
        <meta cont  nt="C#" name="CODE_LANGUAGE" />
        <meta content="JavaScript" name="vs_defaultClientScript" />
        <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
        <meta http-equiv="X-UA-Compatible" content="IE=5; IE=8; IE=7.5;" />
        <meta http-equiv="X-UA-Compatible" content="chrome=1">
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
        <!-- Favicon -->
        <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
        <!-- For iPhone -->
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
        <!-- For iPhone 4 Retina display -->
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
        <!-- For iPad -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
        <!-- For iPad Retina display -->




        <!-- CORE CSS FRAMEWORK - START -->
        <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
        <!-- CORE CSS FRAMEWORK - END -->

        <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


        <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


        <!-- CORE CSS TEMPLATE - START -->
        <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />



		<SCRIPT src="../Scripts/Validationfx.js" type="text/javascript"></SCRIPT>
		<script language="javascript">

		function ValidateInput()
		{	
			var error = "Please enter correct value.Only float and numeric value is allowed";
			var error1 = "Please type Template Name";
		
			
			var lblMessage = document.getElementById("lblMessage");
			
			var txtTemplateName = document.getElementById("txtTemplateName");
			if (txtTemplateName.value=="")
			{
				//lblMessage.innerText = error1;
				alert(error1)
				txtTemplateName.focus();
				return false;
			}
			
			var cnt = parseInt(document.getElementById("txtHidden").value);
			var grdName = "dg_newplan";
			var idx=2;
			var bol = true;
			//alert("asli");
			for (idx=2; idx < (cnt+2); idx++)
			{
				var ctlBasePrice = "";
				var ctlsecprice = "";
				var ctlbasepcent = "";
				var ctlsecpcent = "";
				var ctlbondbase = "";
				var ctlbondsec = "";
				var ctlbondbasepcent= ""; 
				var ctlbondsecpcent = "";
				var ctlbondassump = "";
				var ctlfineassump = "";
				
				if( idx < 10)
				{
				 ctlBasePrice = grdName+ "_ctl0" + idx + "_txt_BasePrice";
				 ctlsecprice = grdName+ "_ctl0" + idx + "_txt_secprice";
				 ctlbasepcent = grdName+ "_ctl0" + idx + "_txt_basepcent";
				 ctlsecpcent = grdName+ "_ctl0" + idx + "_txt_secpcent";
				 ctlbondbase = grdName+ "_ctl0" + idx + "_txt_bondbase";
				 ctlbondsec = grdName+ "_ctl0" + idx + "_txt_bondsec";
				 ctlbondbasepcent = grdName+ "_ctl0" + idx + "_txt_bondsecpcent";
				 ctlbondassump = grdName+ "_ctl0" + idx + "_txt_bondassump";
				 ctlfineassump = grdName+ "_ctl0" + idx + "_txt_fineassump";
				}
				else
				{
				ctlBasePrice = grdName+ "_ctl" + idx + "_txt_BasePrice";
				 ctlsecprice = grdName+ "_ctl" + idx + "_txt_secprice";
				 ctlbasepcent = grdName+ "_ctl" + idx + "_txt_basepcent";
				 ctlsecpcent = grdName+ "_ctl" + idx + "_txt_secpcent";
				 ctlbondbase = grdName+ "_ctl" + idx + "_txt_bondbase";
				 ctlbondsec = grdName+ "_ctl" + idx + "_txt_bondsec";
				 ctlbondbasepcent = grdName+ "_ctl" + idx + "_txt_bondsecpcent";
				 ctlbondassump = grdName+ "_ctl" + idx + "_txt_bondassump";
				 ctlfineassump = grdName+ "_ctl" + idx + "_txt_fineassump";
				}

				/* testing for changing color
				txt_BasePrice.runtimeStyle.backgroundColor
				txt_BasePrice.currentStyle.color
					color	"#123160"	Variant
				txt_BasePrice.style.backgroundColor
				*/
				var txt_BasePrice = document.getElementById(ctlBasePrice);	
				var txt_secprice =document.getElementById(ctlsecprice);	
				var txt_basepcent = document.getElementById(ctlbasepcent);	
				var txt_secpcent = document.getElementById(ctlsecpcent);	
				var txt_bondbase =document.getElementById(ctlbondbase);	
				var txt_bondsec =document.getElementById(ctlbondsec);	
				var txt_bondbasepcent = document.getElementById(ctlbondbasepcent);	
				var txt_bondsecpcent = document.getElementById(ctlbondsecpcent);	
				var txt_bondassump = document.getElementById(ctlbondassump);	
				var txt_fineassump =document.getElementById(ctlfineassump);	
				
				bol = true;
				
				bol = ValidationFloat(txt_BasePrice);
				if (bol==false)
				{
					alert(error)
					return false;
				}

				bol = ValidationFloat(txt_secprice);
				if (bol==false)
				{
					alert(error)
					return false;
				}

				bol = ValidationFloat(txt_basepcent);
				if (bol==false)
				{
					alert(error)
					return false;
				}

				bol = ValidationFloat(txt_secpcent);
				if (bol==false)
				{
					alert(error)
					return false;
				}

				bol = ValidationFloat(txt_bondbase);
				if (bol==false)
				{
					alert(error)
					return false;
				}

				bol = ValidationFloat(txt_bondsec);
				if (bol==false)
				{
					alert(error)
					return false;
				}

				bol = ValidationFloat(txt_bondbasepcent);
				if (bol==false)
				{
					alert(error)
					return false;
				}

				bol = ValidationFloat(txt_bondsecpcent);
				if (bol==false)
				{
					alert(error)
					return false;
				}

				bol = ValidationFloat(txt_bondassump);
				if (bol==false)
				{
					alert(error)
					return false;
				}

				bol = ValidationFloat(txt_fineassump);
				if (bol==false)
				{
					alert(error)
					return false;
				}
			
				if (bol==false)
				{
					alert(error)
					return false;
				}
			}
			return bol;
		}
		
		function ValidationFloat(TextBox)
		{
				//alert(TextBox.value);
				//var error = "Please correct value. Only float value is allowed";
				var lblMessage = document.getElementById("lblMessage");

				if(TextBox.value=="")
				{
					TextBox.value = 0;
					return true;
				}
				else
				{
					if (isFloat(TextBox.value)==false) 
					{
						//if -ve and not float
						//lblMessage.innerText = error;
						TextBox.focus();
						
						return false;
					}
					else
					{
						lblMessage.innerText = "";
						return true;
					}
				}
				
		}


/*

			function ValidateBasePrice(crtl_BasePrice)
			{
				var error = "Please correct value";
				var txt_BasePrice = document.getElementById(crtl_BasePrice);
				var lblMessage = document.getElementById("lblMessage");
			
				if(txt_BasePrice.value!="")
				{

					if (isFloat(txt_BasePrice.value)==false) //if -ve
					{
						lblMessage.innerText = error;
						txt_BasePrice.focus();
					}
					else
					{
						lblMessage.innerText = "";
					}
					
				}
				else
				{
					txt_BasePrice.value = 0;
				}
			}
		
			function ValidateSecPrice()
			{
			var error = "Please correct value";
				var txt_BasePrice = document.getElementById(crtl_BasePrice);
				var lblMessage = document.getElementById("lblMessage");
			
				if(txt_BasePrice.value!="")
				{

					if (isFloat(txt_BasePrice.value)==false) //if -ve
					{
						lblMessage.innerText = error;
						txt_BasePrice.focus();
					}
					
				}
				else
				{
					txt_BasePrice.value = 0;
				}

			}
			function ValidateBasePcent()
			{
				var error = "Please correct value";
				var txt_BasePrice = document.getElementById(crtl_BasePrice);
				var lblMessage = document.getElementById("lblMessage");
			
				if(txt_BasePrice.value!="")
				{


					if (isNonnegativeInteger(txt_BasePrice.value)==false)
					{
						lblMessage.innerText = error;
						txt_BasePrice.focus();
						alert(error);
					}
					
				}
				else
				{
					txt_BasePrice.value = 0;
				}
			}
		*/
		</script>

        <style>
            .table input {
                font-size: 70%;
            }
        </style>
	</HEAD>

    <body class=" ">

        <form id="Form1" method="post" runat="server">

            <!-- START CONTAINER -->
            <div class="page-container row-fluid container-fluid">

                <asp:Panel ID="pnl" runat="server">
                    <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                </asp:Panel>

                <!-- START CONTENT -->
                <section id="main-content" class=" ">
                    <section class="wrapper main-wrapper row" style=''>

                    <div class='col-xs-12'>
                        <div class="page-title">

                            <div class="pull-left">

                                <!-- PAGE HEADING TAG - START -->
                                <h1 class="title">Pricing Templates</h1>
                                <!-- PAGE HEADING TAG - END -->                            

                            </div>

                                
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <!-- MAIN CONTENT AREA STARTS -->
    
                    <div class="col-xs-12">
                        <section class="box ">
                            <header class="panel_header">
                                    
                                <div class="actions panel_actions pull-right">
                	                <asp:hyperlink id="HyperLink1" CssClass="form-label" runat="server" NavigateUrl="TemplateMain.aspx">Back to Template Main</asp:hyperlink>
                                </div>
                            </header>
                            <div class="content-body">

                                <div class="row">
                                    <div class="col-md-6 col-sm-7 col-xs-8">

                                        <div class="form-group">
                                            <label class="form-label" for="field-1">Template Name</label>
                                            <div class="controls">
                                                <asp:textbox id="txtTemplateName" runat="server" CssClass="form-control"></asp:textbox>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-6 col-sm-7 col-xs-8">

                                        <div class="form-group">
                                            <label class="form-label" for="field-1">Template Description</label>
                                            <div class="controls">
                                                <asp:textbox id="txtTemplateDesc" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:textbox>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6 col-sm-7 col-xs-8">

                                        <div class="form-group">
                                            <asp:label id="lblMessage" runat="server" CssClass="form-label" ForeColor="Red" EnableViewState="False"></asp:label>
                                        </div>

                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12">

                                        <div class="form-group">
                                            <div class="controls">
                                                <asp:button id="btnSave" runat="server" CssClass="btn btn-primary pull-right" Text="Save Changes"></asp:button>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </section>
                    </div>

                    <div class="col-lg-12">
                        <section class="box ">
                            
                            <div class="content-body">
                                <div class="row">
                                    <div class="col-xs-12">

                                        <asp:datagrid id="dg_newplan" runat="server" CssClass="table" AutoGenerateColumns="False" style="font-size:100%">
										    <Columns>
											    <asp:TemplateColumn HeaderText="Violation Category">
												    <ItemTemplate>
													    <asp:Label id=lbl_voilcategory runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.categoryname") %>'>
													    </asp:Label>
												    </ItemTemplate>
											    </asp:TemplateColumn>
											    <asp:TemplateColumn Visible="False" HeaderText="DetailID">
												    <ItemTemplate>
													    <asp:Label id=lbl_DetailID runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DetailID") %>'>
													    </asp:Label>
												    </ItemTemplate>
											    </asp:TemplateColumn>
											    <asp:TemplateColumn Visible="False" HeaderText="TemplateID">
												    <ItemTemplate>
													    <asp:Label id=lbl_TemplateID runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TemplateID") %>'>
													    </asp:Label>
												    </ItemTemplate>
											    </asp:TemplateColumn>
											    <asp:TemplateColumn Visible="False" HeaderText="CategoryID">
												    <ItemTemplate>
													    <asp:Label id=lbl_CategoryID runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CategoryID") %>'>
													    </asp:Label>
												    </ItemTemplate>
											    </asp:TemplateColumn>
											    <asp:TemplateColumn HeaderText="Base Price">
												    <ItemTemplate>
													    <asp:TextBox id=txt_BasePrice runat="server" CssClass="form-control" Text='<%# DataBinder.Eval(Container, "DataItem.BasePrice", "{0:F}") %>' MaxLength="5">
													    </asp:TextBox>
												    </ItemTemplate>
											    </asp:TemplateColumn>
											    <asp:TemplateColumn HeaderText="Secondary Price">
												    <ItemTemplate>
													    <asp:TextBox id=txt_secprice runat="server" CssClass="form-control" Text='<%# DataBinder.Eval(Container, "DataItem.SecondaryPrice", "{0:F}") %>' MaxLength="5">
													    </asp:TextBox>
												    </ItemTemplate>
											    </asp:TemplateColumn>
											    <asp:TemplateColumn HeaderText="Base%">
												    <ItemTemplate>
													    <asp:TextBox id=txt_basepcent runat="server" CssClass="form-control" Text='<%# DataBinder.Eval(Container, "DataItem.BasePercentage", "{0:F}") %>' MaxLength="5">
													    </asp:TextBox>
												    </ItemTemplate>
											    </asp:TemplateColumn>
											    <asp:TemplateColumn HeaderText="Secondary%">
												    <ItemTemplate>
													    <asp:TextBox id=txt_secpcent runat="server" CssClass="form-control" Text='<%# DataBinder.Eval(Container, "DataItem.SecondaryPercentage", "{0:F}") %>' MaxLength="5">
													    </asp:TextBox>
												    </ItemTemplate>
											    </asp:TemplateColumn>
											    <asp:TemplateColumn HeaderText="Bond Base">
												    <ItemTemplate>
													    <asp:TextBox id=txt_bondbase runat="server" CssClass="form-control" Text='<%# DataBinder.Eval(Container, "DataItem.BondBase", "{0:F}") %>' MaxLength="5">
													    </asp:TextBox>
												    </ItemTemplate>
											    </asp:TemplateColumn>
											    <asp:TemplateColumn HeaderText="Bond Secondary">
												    <ItemTemplate>
													    <asp:TextBox id=txt_bondsec runat="server" CssClass="form-control" Text='<%# DataBinder.Eval(Container, "DataItem.BondSecondary", "{0:F}") %>' MaxLength="5">
													    </asp:TextBox>
												    </ItemTemplate>
											    </asp:TemplateColumn>
											    <asp:TemplateColumn HeaderText="Bond Base%">
												    <ItemTemplate>
													    <asp:TextBox id=txt_bondbasepcent runat="server" CssClass="form-control" Text='<%# DataBinder.Eval(Container, "DataItem.BondBasePercentage", "{0:F}") %>' MaxLength="5">
													    </asp:TextBox>
												    </ItemTemplate>
											    </asp:TemplateColumn>
											    <asp:TemplateColumn HeaderText="Bond Secondary%">
												    <ItemTemplate>
													    <asp:TextBox id=txt_bondsecpcent runat="server" CssClass="form-control" Text='<%# DataBinder.Eval(Container, "DataItem.BondSecondaryPercentage", "{0:F}") %>' MaxLength="5">
													    </asp:TextBox>
												    </ItemTemplate>
											    </asp:TemplateColumn>
											    <asp:TemplateColumn HeaderText="Bond Assumption">
												    <ItemTemplate>
													    <asp:TextBox id=txt_bondassump runat="server" CssClass="form-control" Text='<%# DataBinder.Eval(Container, "DataItem.BondAssumption", "{0:F}") %>' MaxLength="5">
													    </asp:TextBox>
												    </ItemTemplate>
											    </asp:TemplateColumn>
											    <asp:TemplateColumn HeaderText="Fine Assumption">
												    <ItemTemplate>
													    <asp:TextBox id=txt_fineassump runat="server" CssClass="form-control" Text='<%# DataBinder.Eval(Container, "DataItem.FineAssumption", "{0:F}") %>' MaxLength="5">
													    </asp:TextBox>
												    </ItemTemplate>
											    </asp:TemplateColumn>
										    </Columns>
									    </asp:datagrid>
                                        <INPUT id="txtHidden" type="hidden" runat="server">
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>

                    <!-- MAIN CONTENT AREA ENDS -->

                    </section>
                </section>
                <!-- END CONTENT -->

            </div>
            <!-- END CONTAINER -->

        </form>

        <!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->


        <!-- CORE JS FRAMEWORK - START --> 
        <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script> 
        <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script> 
        <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>  
        <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script> 
        <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>  
        <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
        <!-- CORE JS FRAMEWORK - END --> 


        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 

        <script src="../assets/plugins/autosize/autosize.min.js" type="text/javascript"></script>
        <script src="../assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>
        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


        <!-- CORE TEMPLATE JS - START --> 
        <script src="../assets/js/scripts.js" type="text/javascript"></script> 
        <!-- END CORE TEMPLATE JS - END --> 


        <!-- General section box modal start -->
        <div class="modal" id="section-settings" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog animated bounceInDown">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Section Settings</h4>
                    </div>
                    <div class="modal-body">

                        Body goes here...

                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <button class="btn btn-success" type="button">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- modal end -->
        </body>



















	<%--<body >
		<form id="Form1" method="post" runat="server">
			<TABLE id="TableMain" style="Z-INDEX: 101" cellSpacing="0" cellPadding="0" width="780"
				align="center" border="0">
				<TR>
					<TD><uc1:activemenu id="ActiveMenu1" runat="server"></uc1:activemenu></TD>
				</TR>
				<tr>
					<td background="../../images/separator_repeat.gif" colSpan="6" height="11"></td>
				</tr>
				<TR>
					<TD>
						<TABLE id="TableHeader" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
							<TR>
								<TD class="clsLeftPaddingTable" style="WIDTH: 140px">Template Name:</TD>
								<TD class="clsLeftPaddingTable" style="WIDTH: 227px">&nbsp;
									<asp:textbox id="txtTemplateName" runat="server" CssClass="clsinputadministration" Width="144px"></asp:textbox></TD>
								<TD class="clsLeftPaddingTable" align="left">&nbsp;
									<asp:hyperlink id="HyperLink1" runat="server" NavigateUrl="TemplateMain.aspx">Back to Template Main</asp:hyperlink></TD>
							</TR>
							<TR>
								<TD class="clsLeftPaddingTable" style="WIDTH: 140px">Template Description:</TD>
								<TD class="clsLeftPaddingTable" style="WIDTH: 227px">&nbsp;
									<asp:textbox id="txtTemplateDesc" runat="server" CssClass="clsinputadministration" Width="144px"
										TextMode="MultiLine"></asp:textbox></TD>
								<TD class="clsLeftPaddingTable">&nbsp;</TD>
							</TR>
							<TR>
								<TD class="clsLeftPaddingTable" style="WIDTH: 140px">&nbsp;</TD>
								<TD class="clsLeftPaddingTable" style="WIDTH: 227px">&nbsp;</TD>
								<TD class="clsLeftPaddingTable" align="left"><asp:button id="btnSave" runat="server" CssClass="clsbutton" Text="Save Changes"></asp:button></TD>
							</TR>
						</TABLE>
						<asp:label id="lblMessage" runat="server" Width="320px" ForeColor="Red" EnableViewState="False"></asp:label></TD>
				</TR>
				<tr>
					<td background="../../images/headbar_headerextend.gif" height="5"></td>
				</tr>
				<tr>
					<td class="clssubhead" background="../../images/headbar_midextend.gif" height="13"></td>
					<td class="clssubhead" background="../../images/headbar_midextend.gif" height="13"></td>
				</tr>
				<tr>
					<td background="../../images/headbar_footerextend.gif" height="10"></td>
				</tr>
				<tr>
					<td style="HEIGHT: 172px">
						<table id="tblgrid" cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
							<TR>
								<TD align="center"><asp:datagrid id="dg_newplan" runat="server" CssClass="clsLeftPaddingTable" Width="780px" AutoGenerateColumns="False">
										<Columns>
											<asp:TemplateColumn HeaderText="Violation Category">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id=lbl_voilcategory runat="server" Width="116px" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.categoryname") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn Visible="False" HeaderText="DetailID">
												<ItemTemplate>
													<asp:Label id=lbl_DetailID runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DetailID") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn Visible="False" HeaderText="TemplateID">
												<ItemTemplate>
													<asp:Label id=lbl_TemplateID runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TemplateID") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn Visible="False" HeaderText="CategoryID">
												<ItemTemplate>
													<asp:Label id=lbl_CategoryID runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CategoryID") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Base Price">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<asp:TextBox id=txt_BasePrice runat="server" Width="45px" CssClass="clsinputadministration" Text='<%# DataBinder.Eval(Container, "DataItem.BasePrice", "{0:F}") %>' MaxLength="5">
													</asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Secondary Price">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<asp:TextBox id=txt_secprice runat="server" Width="45px" CssClass="clsinputadministration" Text='<%# DataBinder.Eval(Container, "DataItem.SecondaryPrice", "{0:F}") %>' MaxLength="5">
													</asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Base%">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<asp:TextBox id=txt_basepcent runat="server" Width="45px" CssClass="clsinputadministration" Text='<%# DataBinder.Eval(Container, "DataItem.BasePercentage", "{0:F}") %>' MaxLength="5">
													</asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Secondary%">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<asp:TextBox id=txt_secpcent runat="server" Width="45px" CssClass="clsinputadministration" Text='<%# DataBinder.Eval(Container, "DataItem.SecondaryPercentage", "{0:F}") %>' MaxLength="5">
													</asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Bond Base">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<asp:TextBox id=txt_bondbase runat="server" Width="45px" CssClass="clsinputadministration" Text='<%# DataBinder.Eval(Container, "DataItem.BondBase", "{0:F}") %>' MaxLength="5">
													</asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Bond Secondary">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<asp:TextBox id=txt_bondsec runat="server" Width="45px" CssClass="clsinputadministration" Text='<%# DataBinder.Eval(Container, "DataItem.BondSecondary", "{0:F}") %>' MaxLength="5">
													</asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Bond Base%">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<asp:TextBox id=txt_bondbasepcent runat="server" Width="45px" CssClass="clsinputadministration" Text='<%# DataBinder.Eval(Container, "DataItem.BondBasePercentage", "{0:F}") %>' MaxLength="5">
													</asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Bond Secondary%">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<asp:TextBox id=txt_bondsecpcent runat="server" Width="45px" CssClass="clsinputadministration" Text='<%# DataBinder.Eval(Container, "DataItem.BondSecondaryPercentage", "{0:F}") %>' MaxLength="5">
													</asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Bond Assumption">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<asp:TextBox id=txt_bondassump runat="server" Width="45px" CssClass="clsinputadministration" Text='<%# DataBinder.Eval(Container, "DataItem.BondAssumption", "{0:F}") %>' MaxLength="5">
													</asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Fine Assumption">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<asp:TextBox id=txt_fineassump runat="server" Width="45px" CssClass="clsinputadministration" Text='<%# DataBinder.Eval(Container, "DataItem.FineAssumption", "{0:F}") %>' MaxLength="5">
													</asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
									</asp:datagrid></TD>
							</TR>
						</table>
					</td>
				</tr>
				<tr>
					<td background="../../images/separator_repeat.gif" colSpan="6" height="11"></td>
				</tr>
				<TR>
					<TD colSpan="5"><uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
			<INPUT id="txtHidden" type="hidden" runat="server">
		</form>
	</body>--%>
</HTML>

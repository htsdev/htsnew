﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Web.UI;
using System.Web.UI.WebControls;
using HTP.ClientController;
using Lntech.Logging.Dto.DataTransferObjects;
using OboutInc.Flyout2;

namespace HTP.backroom
{
    // Noufil 7897 06/17/2010 New page added
    public partial class LoggingReport : Page
    {
        #region Declaration

        // Delegate of page method handler created 
        public delegate void PageMethodHandler();
        // Controller object delcare
        readonly LoggingController _loggingController = new LoggingController();

        #endregion

        #region Events

        /// <summary>
        ///     This event fires every time page loads.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    // Set start date and end date calender to todays date
                    CalFrom.SelectedDate = CalTo.SelectedDate = DateTime.Today;
                    // Fill all dropdowns
                    FillDropdowns();
                    BindGrid();
                }
                //Assigned all methods to paging Control.
                Pagingctrl.PageIndexChanged += Pagingctrl_PageIndexChanged;
                Pagingctrl.PageSizeChanged += Pagingctrl_PageSizeChanged;
                Pagingctrl.Visible = true;
                Pagingctrl.GridView = gvException;
            }
            catch (ApplicationException aex)
            {
                lblMessage.Text = aex.Message;
            }
            catch (Exception ex)
            {
                OnException(ex.Message);
            }
        }

        /// <summary>
        ///     Event fires when ever search button will be pressed.
        /// </summary>
        /// <param name="sender">control which is sending request</param>
        /// <param name="e">event aurgument</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                // Make label text empty
                lblMessage.Text = string.Empty;
                // Bind grid
                BindGrid();
            }
            catch (ApplicationException aex)
            {
                lblMessage.Text = aex.Message;
            }
            catch (Exception ex)
            {
                OnException(ex.Message);
            }
        }

        /// <summary>
        ///     Event fires when ever page index changed in the grid.
        /// </summary>
        /// <param name="sender">Controls which is firing the event.</param>
        /// <param name="e">Event argument</param>
        protected void gvException_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                // Setting new page index
                gvException.PageIndex = e.NewPageIndex;
                // Bind grid to get new page index
                BindGrid();
            }
            catch (ApplicationException aex)
            {
                lblMessage.Text = aex.Message;
            }
            catch (Exception ex)
            {
                OnException(ex.Message);
            }
        }

        /// <summary>
        ///     Event fires when ever grid trie to bind the records. this event will be fire for all available rows.
        /// </summary>
        /// <param name="sender">control which is sending the request</param>
        /// <param name="e">Event arguments</param>
        protected void gvException_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                // Perform action if only exist datarow
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    // Get all sessions, cookies and Applications for current row.
                    ArrayList list = ((LoggingReportDto)(e.Row.DataItem)).Sessions;
                    ArrayList cookielist = ((LoggingReportDto)(e.Row.DataItem)).Cookies;
                    ArrayList applicationList = ((LoggingReportDto)(e.Row.DataItem)).Application;
                    // FInd control in grid.
                    HiddenField hfSession = e.Row.FindControl("HfSession") as HiddenField;
                    HiddenField hfComments = e.Row.FindControl("HfComments") as HiddenField;

                    if (list != null && hfSession != null)
                    {
                        // Append all sessions value from array to hidden field
                        foreach (var arrayList in list)
                            hfSession.Value = Convert.ToString(arrayList).Trim() + Environment.NewLine;
                    }

                    if (cookielist != null && hfSession != null)
                    {
                        // Append all cookie value from array to hidden field
                        foreach (var arrayList in cookielist)
                        {
                            hfSession.Value += Convert.ToString(arrayList).Trim() + Environment.NewLine;
                        }
                    }

                    if (applicationList != null && hfSession != null)
                    {
                        // Append all application value from array to hidden field
                        foreach (var arrayList in applicationList)
                        {
                            hfSession.Value += Convert.ToString(arrayList).Trim() + Environment.NewLine;
                        }
                    }


                    if (hfComments != null && hfComments.Value.Trim().Length > 0)
                    {
                        // hide label
                        ((Label)e.Row.FindControl("LblStack")).Style["display"] = "none";
                        string commentsValue = hfComments.Value;
                        // set flyout with textbox
                        Flyout toolTipComments = ((Flyout)e.Row.FindControl("ToolTipComments"));
                        Panel pnlTooltipComments = ((Panel)toolTipComments.FindControl("PnlTooltipComments"));
                        TextBox txtToolTipComments = new TextBox
                        {
                            Text = string.IsNullOrEmpty(commentsValue) ? "N/A" : commentsValue.Trim(),
                            Columns = 49,
                            Rows = 11,
                            TextMode = TextBoxMode.MultiLine,
                            ReadOnly = true
                        };
                        pnlTooltipComments.Controls.Add(txtToolTipComments);
                    }
                    else
                    {
                        // Hide image  and show label
                        ((Image)e.Row.FindControl("ImgComments")).Style["display"] = "none";
                        ((Label)e.Row.FindControl("LblStack")).Style["display"] = "";
                    }


                    if (hfSession != null && hfSession.Value.Trim().Length > 0)
                    {
                        // hide label
                        ((Label)e.Row.FindControl("LblOtherInfo")).Style["display"] = "none";
                        // set flyout with textbox
                        Flyout toolTipSession = ((Flyout)e.Row.FindControl("ToolTipSession"));
                        Panel pnlTooltipSession = ((Panel)toolTipSession.FindControl("PnlTooltipSession"));
                        TextBox txtToolTipSession = new TextBox
                        {
                            Text = string.IsNullOrEmpty(hfSession.Value) ? "N/A" : hfSession.Value.Trim(),
                            Columns = 49,
                            Rows = 11,
                            TextMode = TextBoxMode.MultiLine,
                            ReadOnly = true
                        };
                        pnlTooltipSession.Controls.Add(txtToolTipSession);
                    }
                    else
                    {
                        // Hide image  and show label
                        ((Image)e.Row.FindControl("ImgSession")).Style["display"] = "none";
                        ((Label)e.Row.FindControl("LblOtherInfo")).Style["display"] = "";
                    }
                }
            }
            catch (ApplicationException aex)
            {
                lblMessage.Text = aex.Message;
            }
            catch (Exception ex)
            {
                OnException(ex.Message);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Fills Severity and Application dropdowns.
        /// </summary>
        private void FillDropdowns()
        {
            FillSeverity();
            FillApplications();
        }

        /// <summary>
        ///     Fill Severity dropdown
        /// </summary>
        private void FillSeverity()
        {
            //ddSeverity.Items.Clear();

            //Array itemValues = Enum.GetValues(typeof(TraceEventType));
            //Array itemNames = Enum.GetNames(typeof(TraceEventType));

            //for (int i = 0; i <= itemNames.Length - 1; i++)
            //{
            //    ListItem item = new ListItem(itemNames.GetValue(i).ToString(), itemValues.GetValue(i).ToString());
            //    ddSeverity.Items.Add(item);
            //}

            ddSeverity.DataSource = Enum.GetNames(typeof(TraceEventType));
            ddSeverity.AppendDataBoundItems = true;
            ddSeverity.DataBind();

            // Add chooose to the top of dropdown list
            ddSeverity.Items.Insert(0, new ListItem("--- Choose --", "-1"));
        }

        /// <summary>
        ///     Fill Application dropdown
        /// </summary>
        private void FillApplications()
        {
            ddApplications.DataSource = _loggingController.GetAllApplication(new ApplicationDto { ApplicationName = null });
            ddApplications.AppendDataBoundItems = true;
            ddApplications.DataBind();
            // Add chooose to the top of dropdown list
            ddApplications.Items.Insert(0, new ListItem("--- Choose --", "-1"));
        }

        /// <summary>
        ///     Binds grid with data
        /// </summary>
        private void BindGrid()
        {
            // Extract severity selected value from dropdown
            string severity = (string.IsNullOrEmpty(ddSeverity.SelectedValue) || ddSeverity.SelectedValue == "-1") ? null : ddSeverity.SelectedValue;

            // Extract application selected value from dropdown
            string application = (string.IsNullOrEmpty(ddApplications.SelectedValue) || ddApplications.SelectedValue == "-1") ? null : ddApplications.SelectedValue;
            string exceptionId;
            long id;
            // If Input exception Id  is valid then accpet it else throw exception.
            if (TxtExceptionId.Text.Trim() == string.Empty)
                exceptionId = null;
            else if (long.TryParse(TxtExceptionId.Text, out id))
            {
                exceptionId = id.ToString();
            }
            else
            {
                throw new ApplicationException("Exception Id accepts only integer.");
            }
            // Call method from controller to get all logs.
            LoggingDto loggingDto = !String.IsNullOrEmpty(severity) ? new LoggingDto { Severity = (TraceEventType)Enum.Parse(typeof(TraceEventType), severity, true) } : null;
            ApplicationDto applicationDto = new ApplicationDto { ApplicationName = application };
            List<LoggingReportDto> list = _loggingController.GetAllLogs(new LoggingReportDto
                                                                                 {
                                                                                     StartDate = CalFrom.SelectedDate,
                                                                                     EndDate = CalTo.SelectedDate,
                                                                                     Logging = loggingDto,
                                                                                     ExceptionId = exceptionId,
                                                                                     ApplicationName = applicationDto,
                                                                                     ShowAll = ChkShowAll.Checked
                                                                                 });

            // If logs list is not null then bind it with data.
            if (list != null)
            {
                gvException.Visible = true;
                gvException.DataSource = list;
                gvException.DataBind();
                Pagingctrl.Visible = true;
                lblMessage.Text = string.Empty;
                Pagingctrl.GridView = gvException;
                Pagingctrl.PageCount = gvException.PageCount;
                Pagingctrl.PageIndex = gvException.PageIndex;
                Pagingctrl.SetPageIndex();
            }
            // Show no records found meessage 
            else
            {
                gvException.Visible = false;
                Pagingctrl.Visible = false;
                lblMessage.Text = "No records found.";
            }
        }

        /// <summary>
        ///     Method that performs an action when an exception occurs.
        /// </summary>
        /// <param name="message">Exception message</param>
        private void OnException(string message)
        {
            // show error meesage on label
            lblMessage.Text = message;
        }

        /// <summary>
        ///     Method runs when page index changed.
        /// </summary>
        void Pagingctrl_PageIndexChanged()
        {
            // Setting new page index from paging control selected value
            gvException.PageIndex = Pagingctrl.PageIndex - 1;
            // Bind grid to get new page index
            BindGrid();
        }

        /// <summary>
        ///     Method runs when page size changed.
        /// </summary>
        /// <param name="pageSize">No of records per page.</param>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            // Allow paging if page size index is greater then zero
            if (pageSize > 0)
            {
                // set pageindex to zero
                gvException.PageIndex = 0;
                // set page size to input size
                gvException.PageSize = pageSize;
                // Show grid with paging allowed
                gvException.AllowPaging = true;
            }
            else
            {
                // Hide grid
                gvException.AllowPaging = false;
            }
            // Bind grid
            BindGrid();
        }

        #endregion
    }
}

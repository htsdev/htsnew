using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;

namespace lntechNew.backroom
{
	/// <summary>
	/// Summary description for frmReports.
	/// </summary>
	public partial class frmReports : System.Web.UI.Page
	{
		string StrExp="" ;
		string StrAcsDec="" ;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.TextBox txtSearchSp;
		protected System.Web.UI.WebControls.DropDownList cboLoc;
		protected System.Web.UI.WebControls.Button imgbtnGo;
		protected System.Web.UI.WebControls.Label lblCurrPage;
		protected System.Web.UI.WebControls.Label lblPNo;
		protected System.Web.UI.WebControls.Label lblGoto;
		protected System.Web.UI.WebControls.DropDownList cmbPageNo;
		protected System.Web.UI.WebControls.Label lblMessage;
		protected System.Web.UI.WebControls.DataGrid DgSqlProcedure;
		string ConStr = (string) ConfigurationSettings.AppSettings["connString"];
	//DataSet	Ds1;
		// Create Object for Getting Session Informaiton
		clsSession ClsSession=new clsSession();
		clsLogger clog = new clsLogger();

		clsENationWebComponents clsDB=new clsENationWebComponents();
        int categoryid = 0;
        

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.imgbtnGo.Click += new System.EventHandler(this.imgbtnGo_Click);
			this.cmbPageNo.SelectedIndexChanged += new System.EventHandler(this.cmbPageNo_SelectedIndexChanged);
			this.DgSqlProcedure.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.DgSqlProcedure_PageIndexChanged);
			this.DgSqlProcedure.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.DgSqlProcedure_ItemDataBound);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				if (ClsSession.IsValidSession(this.Request)==false)
				{
					Response.Redirect("../frmlogin.aspx",false);
				}
				else //To stop page further execution
				{
					if (ClsSession.GetCookie("sAccessType",this.Request).ToString()!="2")
					{
						Response.Redirect("../LoginAccesserror.aspx",false);
						Response.End();
					}
					else //To stop page further execution
					{
                        //Request.Url.ToString().Contains("Reports");
						if (IsPostBack!=true)
						{
					
							//string QueryText = "select * from tbl_Sp_Name";
							DataSet	Ds1= clsDB.Get_DS_BySP("usp_Get_AllReports");
                                                        
							fill_Grid(FormatDataSet(Ds1));                           
						}
					}
				}
			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}

		}

		
		
	#region Functions
		private void fill_Grid(DataSet Ds1)
		{
			try
			{
               
				DgSqlProcedure.DataSource = Ds1;	
				DgSqlProcedure.DataBind();
			
				FillPageList();
				SetNavigation();

               //  Fill DropDown 
                string[] key ={ "@RptName", "@ReportLoc", "@CategoryID" };
                object[] value1 ={ "", 0, 0 };
                DataSet ds = clsDB.Get_DS_BySPArr("getReports", key, value1); 

                ddl_category.Items.Clear();

                ddl_category.DataTextField = "CategoryName";
                ddl_category.DataValueField = "CategoryID";
                ddl_category.DataSource = ds.Tables[1];
                ddl_category.DataBind();

               // ddl_category.Items.Insert(0, "All Categories");
                //ListItem lstremove = new ListItem("--- Chose Category --- ", "0");
                //ddl_category.Items.Remove(lstremove);
                //ListItem lst = new ListItem("All Categories", "0");
                //ddl_category.Items.Insert(0, lst);
                //ddl_category.SelectedValue = categoryid.ToString();

				//int intAccessType = Convert.ToInt32(ClsSession.GetCookie("sAccessType",this.Request));
				// Check if AccessType==2 then Edit Column must be visible otherwise invsisble
                //if(intAccessType==2)
                //{
                //    DgSqlProcedure.Columns[8].Visible = true;
                //}
                //else
                //{
                //    DgSqlProcedure.Columns[8].Visible = false;
                //}


			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}

		//Fill cmb with no of pages in grid
		private void FillPageList()
		{
			try
			{
				Int16 idx =0;
				cmbPageNo.Items.Clear();
				for(idx=1; idx<=DgSqlProcedure.PageCount;idx++ )
				{
					cmbPageNo.Items.Add("Page - " + idx.ToString());
					cmbPageNo.Items[idx-1].Value = idx.ToString();
				}
				BindReport();// for S.No.
			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}	

		// Procedure for setting data grid's current  page number on header ........
		private void SetNavigation()
		{
			try
			{			
				// setting current page number
				lblPNo.Text =Convert.ToString(DgSqlProcedure.CurrentPageIndex+1);
				lblCurrPage.Visible=true;
				lblPNo.Visible=true;

				// filling combo with page numbers
				lblGoto.Visible=true;
				cmbPageNo.Visible=true;
				//FillPageList();

				cmbPageNo.SelectedIndex =DgSqlProcedure.CurrentPageIndex;
			}
			catch (Exception ex)
			{
					lblMessage.Text = ex.Message ;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}

		//Method to generate S# for Data Grid
		private void BindReport()
		{
			try
			{
                int sNo=0;
                if (hf_sno.Value != null && hf_sno.Value != "")
                    sNo = Convert.ToInt32(hf_sno.Value); // (DgSqlProcedure.CurrentPageIndex);      
          
				foreach (DataGridItem ItemX in DgSqlProcedure.Items) 
				{
                    if (((HiddenField)(ItemX.FindControl("hf_rptid"))).Value == "-1")
                    {
                        sNo = 0;// (DgSqlProcedure.CurrentPageIndex); // (DgSqlProcedure.CurrentPageIndex) * (DgSqlProcedure.PageSize); ;
                    }
                    if (((HiddenField)(ItemX.FindControl("hf_rptid"))).Value != "-1")
                    {
                        
                        sNo += 1;
                        hf_sno.Value = sNo.ToString();
                        ((Label)(ItemX.FindControl("lblSno"))).Text = sNo.ToString();
                    }                    
				}
			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message ;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}		
		}

	#endregion

	#region Events
//		private void cmdSearchSp_Click(object sender, System.EventArgs e)
//		{
//			try
//			{
//				lblMessage.Text = "";
//				DgSqlProcedure.CurrentPageIndex = 0; //to search from start
//				//getReports get all reports on name specified txtSearchSp.Text and Location specified cboLoc
//				int iLoc = cboLoc.SelectedIndex;
//				SqlParameter [] arParm = new SqlParameter[2];
//				arParm[0] = new SqlParameter("@RptName", txtSearchSp.Text);
//				arParm[1] = new SqlParameter("@ReportLoc",iLoc);
//				DataSet ds = SqlHelper.ExecuteDataset(ConStr,CommandType.StoredProcedure,"getReports",arParm); 
//				if (ds.Tables[0].Rows.Count==0)
//				{
//					lblMessage.Text = "No record found";
//					fill_Grid(ds);
//				}
//				else
//				{
//					fill_Grid(ds);
//				}
//			}
//			catch(Exception ex)
//			{
//				lblMessage.Text = ex.Message;
//			}
//		}
//
		private void cmbPageNo_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try
			{
				DgSqlProcedure.CurrentPageIndex = cmbPageNo.SelectedIndex;
				//string QueryText = "select * from tbl_Sp_Name";
				DataSet	Ds1= SqlHelper.ExecuteDataset(ConStr,CommandType.StoredProcedure,"usp_Get_AllReports");
                fill_Grid(FormatDataSet(Ds1));
			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}

		private void DgSqlProcedure_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			//			DataRowView drv = (DataRowView) e.Item.DataItem;
			//			if (drv == null) 
			//				return;
			//			((HyperLink) e.Item.FindControl("HLSpnm")).NavigateUrl = "frmExeSP.aspx?spnm=" + (string) drv["name"];

            

			DataRowView drv = (DataRowView) e.Item.DataItem; 
			if (drv == null) return;

			((HyperLink)e.Item.FindControl("HLSpnm")).NavigateUrl = "frmExeSpByRptId.aspx?RptId=" + drv["RptId"];  ///(string) drv["RptId"];
			((HyperLink)e.Item.FindControl("HLEdit")).NavigateUrl = "frmReportDesc.aspx?chkpage=1&RptId=" + drv["RptId"] + "&spname=" + drv["SpName"];  ///(string) drv["RptId"];

            ((LinkButton)e.Item.FindControl("lbkbtn_delete")).Attributes.Add("onclick", "return DeleteConfirmation();");

            #region Formatting the header row to display court location
            HiddenField tID = (HiddenField)e.Item.FindControl("hf_rptid");
             HiddenField sn = (HiddenField)e.Item.FindControl("hf_catn");
            
            if (tID != null)
                if (tID.Value == "-1")
                {
                    HtmlTable tbl = (HtmlTable)e.Item.FindControl("tbl_categoryname");
                    tbl.Visible = true;

                    HtmlTable tblh = (HtmlTable)e.Item.FindControl("tblheader");
                    tblh.Visible = true;

                    HtmlTable tbl2 = (HtmlTable)e.Item.FindControl("tbl_info");
                    tbl2.Style[HtmlTextWriterStyle.Display] = "none";
                    //HtmlTable tbl3 = (HtmlTable)e.Row.FindControl("tbl_CourtDetails");
                    // tbl3.Style[HtmlTextWriterStyle.Display] = "none";

                    if (sn.Value == "-1")
                    {
                        HtmlTableRow row = (HtmlTableRow)e.Item.FindControl("tr_Sep");
                        row.Visible = true;
                    }
                   
                }
            #endregion
			
		}

		private void DgSqlProcedure_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{            
			DgSqlProcedure.CurrentPageIndex = e.NewPageIndex ;
			//string QueryText = "select * from tbl_Sp_Name";
			DataSet	Ds1= SqlHelper.ExecuteDataset(ConStr,CommandType.StoredProcedure,"usp_Get_AllReports");
            fill_Grid(FormatDataSet(Ds1));
		
		}
	#endregion 

				
		private void DgSqlProcedure_SortCommand(object source, System.Web.UI.WebControls.DataGridSortCommandEventArgs e)
		{
			string SortNm = e.SortExpression;		

			SetAcsDesc(SortNm);
			SortDataGrid(SortNm);

		}

		//Method to set 'ASC' 'DESC' 
		private void SetAcsDesc(string Val)
		{
			try
			{
				StrExp = Session["StrExp"].ToString();
				StrAcsDec = Session["StrAcsDec"].ToString();
			}
			catch
			{

			}

			if (StrExp == Val)
			{
				if (StrAcsDec == "ASC")
				{
					StrAcsDec = "DESC";
					Session["StrAcsDec"] = StrAcsDec ;
				}
				else 
				{
					StrAcsDec = "ASC";
					Session["StrAcsDec"] = StrAcsDec ;
				}
			}
			else 
			{
				StrExp = Val;
				StrAcsDec = "ASC";
				Session["StrExp"] = StrExp ;
				Session["StrAcsDec"] = StrAcsDec ;				
			}
		}

		
		
		//Method to sort Data Grid
		private void SortDataGrid(string Sortnm)
		{
			DataSet	ds= clsDB.Get_DS_BySP("usp_Get_AllReports");
			DataView Dv = ds.Tables[0].DefaultView;
			Dv.Sort = Sortnm + " " + StrAcsDec ;
			DgSqlProcedure.DataSource= Dv;
			DgSqlProcedure.DataBind();	
			BindReport();			
		}

		private void imgbtnGo_Click(object sender, System.EventArgs e)
		{
			try
			{
                DataSet ds = new DataSet();               
				lblMessage.Text = "";
				DgSqlProcedure.CurrentPageIndex = 0; //to search from start
				//getReports get all reports on name specified txtSearchSp.Text and Location specified cboLoc
				int iLoc = cboLoc.SelectedIndex;

                categoryid =(int) Convert.ChangeType(ddl_category.SelectedValue,typeof(int));

                string[] key ={ "@RptName", "@ReportLoc", "@CategoryID" };
                object[] value1 ={ txtSearchSp.Text.Trim(), iLoc, categoryid };
				ds = clsDB.Get_DS_BySPArr("getReports",key,value1); 
				if (ds.Tables[0].Rows.Count==0)
				{
					lblMessage.Text = "No record found";
                    fill_Grid(ds);
                    ddl_category.SelectedValue = categoryid.ToString();

                 }
				else
				{
                    fill_Grid(FormatDataSet(ds));
                    ddl_category.SelectedValue = categoryid.ToString();                   
				}
			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}


        private DataSet FormatDataSet(DataSet source)
        {
            try
            {                
                int current_categoryid = 0;
                int last_categoryid = 0;
                bool unassigned = false;
               

                //// If there is no any category created then it will execute and make heading of "Unassigned Category"
                if (source.Tables[1].Rows.Count == 1)
                {
                    if (source.Tables[0].Rows.Count > 0)
                    {
                        DataRow dr = source.Tables[0].Rows[0];
                        DataRow blank = source.Tables[0].NewRow();
                        blank[0] = -1;
                        blank[1] = "0";
                        blank[2] = source.Tables[0].Rows[0]["CategoryName"].ToString();

                        blank[2] = "Unassigned Reports";

                        source.Tables[0].Rows.InsertAt(blank, 0);

                        return source;
                    }
                }

                //// It will will make group of report according catgories
                for (int i = 0; i < source.Tables[0].Rows.Count; i++)
                {
                    DataRow dr = source.Tables[0].Rows[i];
                    current_categoryid = Convert.ToInt32(dr["ReportCategoryID"]);
                    if (current_categoryid != last_categoryid)
                    {
                        unassigned = true;
                            string categoryname = source.Tables[0].Rows[i]["CategoryName"].ToString();
                            DataRow blank = source.Tables[0].NewRow();
                            blank[0] = -1;
                            blank[1] = Convert.ToInt32(source.Tables[0].Rows[i]["ReportCategoryID"]);
                            blank[2] = source.Tables[0].Rows[i]["CategoryName"].ToString();

                            if (current_categoryid != last_categoryid && i != 0)
                                blank["ShortName"] = "-1";
                            else
                                blank["ShortName"] = "0";
                            if (current_categoryid != last_categoryid && i != 0 && current_categoryid ==0)
                                blank[2] = "Unassigned Reports";
                            
                            source.Tables[0].Rows.InsertAt(blank, i);

                    }                    
                    last_categoryid = current_categoryid;
                }
                // If there are categories which are not assigned any report then this code will execute and make heading of "Unassigned Reports"
                if (unassigned == false)
                {
                    if (source.Tables[0].Rows.Count > 0)
                    {
                        DataRow dr = source.Tables[0].Rows[0];
                        DataRow blank = source.Tables[0].NewRow();
                        blank[0] = -1;
                        blank[1] = "0";
                        blank[2] = source.Tables[0].Rows[0]["CategoryName"].ToString();

                        blank[2] = "Unassigned Reports";

                        source.Tables[0].Rows.InsertAt(blank, 0);
                    }
                }
                /////
                return source;
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                DataSet ds1 = new DataSet();
                return ds1;
            }
        }

        protected void lbkbtn_delete_Click(object sender, EventArgs e)
        {
            try
            {
                string[] keys ={ "@rptid" };
                object[] values ={ "" };
                
                clsDB.ExecuteSP("USP_HTS_DELETE_TBLSPNAME", keys, values);
                lblMessage.Text = "Report has been deleted.";
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
     
        protected void DgSqlProcedure_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void DgSqlProcedure_DeleteCommand(object source, DataGridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Delete")
                {
                    string rptid = ((HiddenField)e.Item.FindControl("hf_rptid")).Value;

                    string[] keys ={ "@rptid" };
                    object[] values ={ rptid };

                    clsDB.ExecuteSP("USP_HTS_DELETE_TBLSPNAME", keys, values);

                    DataSet Ds1 = clsDB.Get_DS_BySP("usp_Get_AllReports");
                    fill_Grid(FormatDataSet(Ds1));

                    lblMessage.Text = "Report has been deleted.";
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

                   		
		//		private void cboLoc_SelectedIndexChanged(object sender, System.EventArgs e)
//		{
//			try
//			{
//				//getReports get all reports of Location specified cboLoc
//				int iLoc = cboLoc.SelectedIndex;
//				SqlParameter [] arParm = new SqlParameter[1];
//				arParm[0] = new SqlParameter("@ReportLoc",iLoc);
//				DataSet ds = SqlHelper.ExecuteDataset(ConStr,CommandType.StoredProcedure,"getReports",arParm); 
//				fill_Grid(ds);
//			}
//			catch(Exception ex)
//			{
//				lblMessage.Text = ex.Message;
//			}
//		}
//



	}
}

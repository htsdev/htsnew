﻿<%@ Page Language="c#" AutoEventWireup="false" Inherits="HTP.backroom.Courts" CodeBehind="Courts.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="gw" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">--%>
<html>
<head>
    <title>Courts</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <meta http-equiv="X-UA-Compatible" content="IE=5; IE=8; IE=7.5;" />
    <meta http-equiv="X-UA-Compatible" content="chrome=1">

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />


    <script language="javascript" type="text/javascript">

        //Kazim 4071 5/22/2008 Write the code to handle the enter key 

        function KeyDownHandler() {
            // process only the Enter key
            if (event.keyCode == 13) {

                event.returnValue = false;
                event.cancel = true;
                document.getElementById("btn_submit").click();
            }
        }

        //Kazim 3697 5/6/2008 Used to open popup 

        function OpenPopUpNew(path)  //(var path,var name)
        {

            window.open(path, '', "height=505,width=600,resizable=yes, status=no,toolbar=no,scrollbars=yes,menubar=no,location=no");
            return false;
        }

        function Submit() {

            var ctrl = null;

            // Get control.

            ctrl = $find('CBEGeneralInfo');
            if (!ctrl)
                return;

            if (frmcourts.txt_CourtName.value.trim() == "") {

                alert("Please specify Court Name");
                ctrl._doOpen();
                frmcourts.txt_CourtName.focus();
                return false;
            }

            if (frmcourts.txt_JudgeName.value.trim() == "") {
                alert("Please specify Judge Name");
                ctrl._doOpen();
                frmcourts.txt_JudgeName.focus();
                return false;
            }

            if (frmcourts.txt_ShortName.value.trim() == "") {
                alert("Please specify Court Short Name");
                ctrl._doOpen();
                frmcourts.txt_ShortName.focus();
                return false;
            }

            if (frmcourts.txt_Address1.value.trim() == "") {
                alert("Please specify your Address");
                ctrl._doOpen();
                frmcourts.txt_Address1.focus();
                return false;
            }

            if (frmcourts.txt_City.value.trim() == "") {
                alert("Please specify Court City");
                ctrl._doOpen();
                frmcourts.txt_City.focus();
                return false;
            }

            if (frmcourts.ddl_States.value == 0) {
                alert("Please specify Court State");
                ctrl._doOpen();
                frmcourts.ddl_States.focus();
                return false;
            }

            if (frmcourts.txt_ZipCode.value.trim() == "") {
                alert("Please specify Court Zip");
                ctrl._doOpen();
                frmcourts.txt_ZipCode.focus();
                return false;
            }

            if (frmcourts.txt_DDName.value.trim() == "") {
                alert("Please specify Drop Down Name");
                ctrl._doOpen();
                frmcourts.txt_DDName.focus();
                return false;
            }

            //		if (document.getElementById("txt_hour").value.trim()=="" )
            //		{
            //		    alert("Please add LOR Time");
            //		    document.getElementById("txt_hour").focus();
            //		    ctrl._doOpen();
            //			return false;
            //		}
            //		if (document.getElementById("txt_min").value.trim()=="")
            //		{
            //		    alert("Please add LOR Minutes ");
            //		    document.getElementById("txt_min").focus();
            //		    ctrl._doOpen();
            //			return false;
            //		}


            //Muhammad Muneer 8227 09/27/2010 adding the new functionality for LOR Pasedina 
            if (document.getElementById("txt_CourtName").value == "Pasadena Municipal Court") {
                //Muhammad ALi 8227 10/06/2010 Add Validation for "." cout time and hours 
                if ((isNaN(document.getElementById("txt_hour").value.trim()) == true) || ((document.getElementById("txt_hour").value).indexOf(".") != -1) || (isNaN(document.getElementById("txt_hour6").value.trim()) == true) || ((document.getElementById("txt_hour6").value).indexOf(".") != -1) || (isNaN(document.getElementById("txt_hour5").value.trim()) == true) || ((document.getElementById("txt_hour5").value).indexOf(".") != -1) || (isNaN(document.getElementById("txt_hour4").value.trim()) == true) || ((document.getElementById("txt_hour4").value).indexOf(".") != -1) || (isNaN(document.getElementById("txt_hour3").value.trim()) == true) || ((document.getElementById("txt_hour3").value).indexOf(".") != -1) || (isNaN(document.getElementById("txt_hour2").value.trim()) == true) || ((document.getElementById("txt_hour2").value).indexOf(".") != -1)) {
                    alert("Invalid Time. Please use numbers");
                    ctrl._doOpen();
                    return false;
                }
                if ((isNaN(document.getElementById("txt_min").value.trim()) == true) || ((document.getElementById("txt_min").value).indexOf(".") != -1) || (isNaN(document.getElementById("txt_min6").value.trim()) == true) || ((document.getElementById("txt_min6").value).indexOf(".") != -1) || (isNaN(document.getElementById("txt_min5").value.trim()) == true) || ((document.getElementById("txt_min5").value).indexOf(".") != -1) || (isNaN(document.getElementById("txt_min4").value.trim()) == true) || ((document.getElementById("txt_min4").value).indexOf(".") != -1) || (isNaN(document.getElementById("txt_min3").value.trim()) == true) || ((document.getElementById("txt_min3").value).indexOf(".") != -1) || (isNaN(document.getElementById("txt_min2").value.trim()) == true) || ((document.getElementById("txt_min2").value).indexOf(".") != -1)) {
                    alert("Invalid Time. Please use numbers");
                    ctrl._doOpen();
                    return false;
                }
                //End Muhammad Ali 8227

                if (document.getElementById("cb_rep").checked == true) {


                    if (document.getElementById("dtpCourtDate").value.trim() == "") {
                        alert("Invalid Date. Please enter date.");
                        document.getElementById("dtpCourtDate").focus();
                        ctrl._doOpen();
                        return false;
                    }

                    else if (document.getElementById("dtpCourtDate2").value.trim() == "") {
                        alert("Invalid Date. Please enter date.");
                        ctrl._doOpen();
                        return false;
                    }

                    else if (document.getElementById("dtpCourtDate3").value.trim() == "") {
                        alert("Invalid Date. Please enter date.");
                        ctrl._doOpen();
                        return false;
                    }

                    else if (document.getElementById("dtpCourtDate4").value.trim() == "") {
                        alert("Invalid Date. Please enter date.");
                        ctrl._doOpen();
                        return false;
                    }

                    else if (document.getElementById("dtpCourtDate5").value.trim() == "") {
                        alert("Invalid Date. Please enter date.");
                        ctrl._doOpen();
                        return false;
                    }

                    else if (document.getElementById("dtpCourtDate6").value.trim() == "") {
                        alert("Invalid Date. Please enter date.");
                        ctrl._doOpen();
                        return false;
                    }
                    else { }

                    if ((document.getElementById("txt_hour").value.trim() > 12) || (document.getElementById("txt_hour").value.trim() == 0) || (document.getElementById("txt_hour").value.trim() < 0) && (document.getElementById("dtpCourtDate").value != "")) {
                        alert("Invalid Time. Hour should between 1 and 12.");
                        ctrl._doOpen();
                        return false;
                    }
                    else if ((document.getElementById("txt_hour2").value.trim() > 12) || (document.getElementById("txt_hour2").value.trim() == 0) || (document.getElementById("txt_hour2").value.trim() < 0) && (document.getElementById("dtpCourtDate2").value != "")) {
                        alert("Invalid Time. Hour should between 1 and 12.");
                        ctrl._doOpen();
                        return false;
                    }
                    else if ((document.getElementById("txt_hour3").value.trim() > 12) || (document.getElementById("txt_hour3").value.trim() == 0) || (document.getElementById("txt_hour3").value.trim() < 0) && (document.getElementById("dtpCourtDate3").value != "")) {
                        alert("Invalid Time. Hour should between 1 and 12.");
                        ctrl._doOpen();
                        return false;
                    }
                    else if ((document.getElementById("txt_hour4").value.trim() > 12) || (document.getElementById("txt_hour4").value.trim() == 0) || (document.getElementById("txt_hour4").value.trim() < 0) && (document.getElementById("dtpCourtDate4").value != "")) {
                        alert("Invalid Time. Hour should between 1 and 12.");
                        ctrl._doOpen();
                        return false;
                    }
                    else if ((document.getElementById("txt_hour5").value.trim() > 12) || (document.getElementById("txt_hour5").value.trim() == 0) || (document.getElementById("txt_hour5").value.trim() < 0) && (document.getElementById("dtpCourtDate5").value != "")) {
                        alert("Invalid Time. Hour should between 1 and 12.");
                        ctrl._doOpen();
                        return false;
                    }
                    else if ((document.getElementById("txt_hour6").value.trim() > 12) || (document.getElementById("txt_hour6").value.trim() == 0) || (document.getElementById("txt_hour6").value.trim() < 0) && (document.getElementById("dtpCourtDate6").value != "")) {
                        alert("Invalid Time. Hour should between 1 and 12.");
                        ctrl._doOpen();
                        return false;
                    }
                    else { }

                    if ((document.getElementById("txt_min").value.trim() > 59) || (document.getElementById("txt_min").value.trim() < 0) || (document.getElementById("txt_min").value.trim() == "")) {
                        alert("Invalid Time. Minutes should between 0 and 59.");
                        ctrl._doOpen();
                        return false;
                    }
                    else if ((document.getElementById("txt_min2").value.trim() > 59) || (document.getElementById("txt_min2").value.trim() < 0) || (document.getElementById("txt_min2").value.trim() == "")) {
                        alert("Invalid Time. Minutes should between 0 and 59.");
                        ctrl._doOpen();
                        return false;
                    }
                    else if ((document.getElementById("txt_min3").value.trim() > 59) || (document.getElementById("txt_min3").value.trim() < 0) || (document.getElementById("txt_min3").value.trim() == "")) {
                        alert("Invalid Time. Minutes should between 0 and 59.");
                        ctrl._doOpen();
                        return false;
                    }
                    else if ((document.getElementById("txt_min4").value.trim() > 59) || (document.getElementById("txt_min4").value.trim() < 0) || (document.getElementById("txt_min4").value.trim() == "")) {
                        alert("Invalid Time. Minutes should between 0 and 59.");
                        ctrl._doOpen();
                        return false;
                    }
                    else if ((document.getElementById("txt_min5").value.trim() > 59) || (document.getElementById("txt_min5").value.trim() < 0) || (document.getElementById("txt_min5").value.trim() == "")) {
                        alert("Invalid Time. Minutes should between 0 and 59.");
                        ctrl._doOpen();
                        return false;
                    }
                    else if ((document.getElementById("txt_min6").value.trim() > 59) || (document.getElementById("txt_min6").value.trim() < 0) || (document.getElementById("txt_min6").value.trim() == "")) {
                        alert("Invalid Time. Minutes should between 0 and 59.");
                        ctrl._doOpen();
                        return false;
                    }
                    else { }

                    if ((document.getElementById("dtpCourtDate").value.trim() == document.getElementById("dtpCourtDate2").value.trim()) || (document.getElementById("dtpCourtDate").value.trim() == document.getElementById("dtpCourtDate3").value.trim()) || (document.getElementById("dtpCourtDate").value.trim() == document.getElementById("dtpCourtDate4").value.trim()) || (document.getElementById("dtpCourtDate").value.trim() == document.getElementById("dtpCourtDate5").value.trim()) || (document.getElementById("dtpCourtDate").value.trim() == document.getElementById("dtpCourtDate6").value.trim())) {
                        alert("Date cannot be same. Please check all the dates");
                        ctrl._doOpen();
                        return false;

                    }
                    else if ((document.getElementById("dtpCourtDate2").value.trim() == document.getElementById("dtpCourtDate").value.trim()) || (document.getElementById("dtpCourtDate2").value.trim() == document.getElementById("dtpCourtDate3").value.trim()) || (document.getElementById("dtpCourtDate2").value.trim() == document.getElementById("dtpCourtDate4").value.trim()) || (document.getElementById("dtpCourtDate2").value.trim() == document.getElementById("dtpCourtDate5").value.trim()) || (document.getElementById("dtpCourtDate2").value.trim() == document.getElementById("dtpCourtDate6").value.trim())) {
                        alert("Date cannot be same. Please check all the dates");
                        ctrl._doOpen();
                        return false;
                    }
                    else if ((document.getElementById("dtpCourtDate3").value.trim() == document.getElementById("dtpCourtDate").value.trim()) || (document.getElementById("dtpCourtDate3").value.trim() == document.getElementById("dtpCourtDate2").value.trim()) || (document.getElementById("dtpCourtDate3").value.trim() == document.getElementById("dtpCourtDate4").value.trim()) || (document.getElementById("dtpCourtDate3").value.trim() == document.getElementById("dtpCourtDate5").value.trim()) || (document.getElementById("dtpCourtDate3").value.trim() == document.getElementById("dtpCourtDate6").value.trim())) {
                        alert("Date cannot be same. Please check all the dates");
                        ctrl._doOpen();
                        return false;
                    }
                    else if ((document.getElementById("dtpCourtDate4").value.trim() == document.getElementById("dtpCourtDate").value.trim()) || (document.getElementById("dtpCourtDate4").value.trim() == document.getElementById("dtpCourtDate2").value.trim()) || (document.getElementById("dtpCourtDate4").value.trim() == document.getElementById("dtpCourtDate3").value.trim()) || (document.getElementById("dtpCourtDate4").value.trim() == document.getElementById("dtpCourtDate5").value.trim()) || (document.getElementById("dtpCourtDate4").value.trim() == document.getElementById("dtpCourtDate6").value.trim())) {
                        alert("Date cannot be same. Please check all the dates");
                        ctrl._doOpen();
                        return false;
                    }
                    else if ((document.getElementById("dtpCourtDate5").value.trim() == document.getElementById("dtpCourtDate").value.trim()) || (document.getElementById("dtpCourtDate5").value.trim() == document.getElementById("dtpCourtDate3").value.trim()) || (document.getElementById("dtpCourtDate5").value.trim() == document.getElementById("dtpCourtDate4").value.trim()) || (document.getElementById("dtpCourtDate5").value.trim() == document.getElementById("dtpCourtDate6").value.trim()) || (document.getElementById("dtpCourtDate5").value.trim() == document.getElementById("dtpCourtDate2").value.trim())) {
                        alert("Date cannot be same. Please check all the dates");
                        ctrl._doOpen();
                        return false;
                    }
                    else if ((document.getElementById("dtpCourtDate6").value.trim() == document.getElementById("dtpCourtDate").value.trim()) || (document.getElementById("dtpCourtDate6").value.trim() == document.getElementById("dtpCourtDate2").value.trim()) || (document.getElementById("dtpCourtDate6").value.trim() == document.getElementById("dtpCourtDate3").value.trim()) || (document.getElementById("dtpCourtDate6").value.trim() == document.getElementById("dtpCourtDate4").value.trim()) || (document.getElementById("dtpCourtDate6").value.trim() == document.getElementById("dtpCourtDate5").value.trim())) {
                        alert("Date cannot be same. Please check all the dates");
                        ctrl._doOpen();
                        return false;
                    }
                    else { }



                    //Muhammad Muneer 8227 09/27/2010 adding the new functionality for LOR Pasedina 
                    //Muhammad Ali 8227  10/06/2010 Correct Alert Msg Language...
                    if (document.getElementById("txt_CourtName").value == "Pasadena Municipal Court") {
                        if (validateIsWorkingDay('dtpCourtDate') == false) {
                            alert("Please select any business day for LOR date");
                            return false;
                        }
                        if (validateIsWorkingDay('dtpCourtDate2') == false) {
                            alert("Please select any business day for LOR date 2");
                            return false;
                        }
                        if (validateIsWorkingDay('dtpCourtDate3') == false) {
                            alert("Please select any business day for LOR date 3");
                            return false;
                        }
                        if (validateIsWorkingDay('dtpCourtDate4') == false) {
                            alert("Please select any business day for LOR date 4");
                            return false;
                        }
                        if (validateIsWorkingDay('dtpCourtDate5') == false) {
                            alert("Please select any business day for LOR date 5");
                            return false;
                        }
                        if (validateIsWorkingDay('dtpCourtDate6') == false) {
                            alert("Please select any business day for LOR date 6");
                            return false;
                        }
                        if (validateIsFutureDate('dtpCourtDate') == false) {
                            alert("Please select LOR date in future");
                            return false;
                        }
                        if (validateIsFutureDate('dtpCourtDate2') == false) {
                            alert("Please select LOR date 2 in future");
                            return false;
                        }
                        if (validateIsFutureDate('dtpCourtDate3') == false) {
                            alert("Please select LOR date 3 in future");
                            return false;
                        }
                        if (validateIsFutureDate('dtpCourtDate4') == false) {
                            alert("Please select LOR date 4 in future");
                            return false;
                        }
                        if (validateIsFutureDate('dtpCourtDate5') == false) {
                            alert("Please select LOR date 5 in future");
                            return false;
                        }
                        if (validateIsFutureDate('dtpCourtDate6') == false) {
                            alert("Please select LOR date 6 in future");
                            return false;
                        }
                    }
                }

            }
            else {
                if (isNaN(document.getElementById("txt_hour").value.trim()) == true) {
                    alert("Invalid Time. Please use numbers");
                    document.getElementById("txt_hour").focus();
                    ctrl._doOpen();
                    return false;
                }
                if (isNaN(document.getElementById("txt_min").value.trim()) == true) {
                    alert("Invalid Time. Please use numbers");
                    document.getElementById("txt_min").focus();
                    ctrl._doOpen();
                    return false;
                }

                //		if ((document.getElementById("txt_hour").value.trim()>0)||(document.getElementById("txt_hour").value.trim()<0)&&(document.getElementById("dtpCourtDate").value ==""))
                //		{
                //		    alert("Invalid Time. Please make time 0 if no letter of Rep date selected");
                //		    document.getElementById("txt_hour").focus();
                //		    ctrl._doOpen();
                //			return false;
                //		}


                if (document.getElementById("cb_rep").checked == true) {
                    if (document.getElementById("dtpCourtDate").value.trim() == "") {
                        alert("Invalid Date. Please enter date.");
                        document.getElementById("dtpCourtDate").focus();
                        ctrl._doOpen();
                        return false;
                    }

                    if ((document.getElementById("txt_hour").value.trim() > 12) || (document.getElementById("txt_hour").value.trim() == 0) || (document.getElementById("txt_hour").value.trim() < 0) && (document.getElementById("dtpCourtDate").value != "")) {
                        alert("Invalid Time. Hour should between 1 and 12.");
                        document.getElementById("txt_hour").focus();
                        ctrl._doOpen();
                        return false;
                    }

                    if ((document.getElementById("txt_min").value.trim() > 59) || (document.getElementById("txt_min").value.trim() < 0) || (document.getElementById("txt_min").value.trim() == "")) {
                        alert("Invalid Time. Minutes should between 0 and 59.");
                        document.getElementById("txt_min").focus();
                        ctrl._doOpen();
                        return false;
                    }
                }
            }
            var intphonenum1 = frmcourts.txt_CC11.value;
            var intphonenum2 = frmcourts.txt_CC12.value;
            var intphonenum3 = frmcourts.txt_CC13.value;
            var intphonenum4 = frmcourts.txt_CC14.value;

            if ((intphonenum1 == "") || (intphonenum2 == "") || (intphonenum3 == "")) {
                alert("Invalid Phone Number. Please don't use any dashes or space.");
                ctrl._doOpen();
                frmcourts.txt_CC11.focus();
                return false;
            }

            if (isNaN(intphonenum1) == true) {
                alert("Invalid Phone Number. Please don't use any dashes or space");
                ctrl._doOpen();
                frmcourts.txt_CC11.focus();
                return false;
            }
            if (isNaN(intphonenum2) == true) {
                alert("Invalid Phone Number. Please don't use any dashes or space");
                ctrl._doOpen();
                frmcourts.txt_CC12.focus();
                return false;
            }
            if (isNaN(intphonenum3) == true) {
                alert("Invalid Phone Number. Please don't use any dashes or space");
                ctrl._doOpen();
                frmcourts.txt_CC13.focus();
                return false;
            }
            if (intphonenum4 != "" && isNaN(intphonenum4) == true) {
                alert("Invalid Phone Number. Please don't use any dashes or space");
                return false;
            }

            var phonelength = intphonenum1.length + intphonenum2.length + intphonenum3.length + intphonenum4.length

            if (phonelength < 10) {
                alert("Invalid Phone Number");
                ctrl._doOpen();
                frmcourts.txt_CC11.focus();
                return false;
            }

            var intphonenum1 = frmcourts.txt_CC1.value;
            var intphonenum2 = frmcourts.txt_CC2.value;
            var intphonenum3 = frmcourts.txt_CC3.value;

            if (isNaN(intphonenum1) == true) {
                alert("Invalid Fax Number. Please don't use any dashes or space");
                ctrl._doOpen();
                frmcourts.txt_CC1.focus();
                return false;
            }
            if (isNaN(intphonenum2) == true) {
                alert("Invalid Fax Number. Please don't use any dashes or space");
                ctrl._doOpen();
                frmcourts.txt_CC2.focus();
                return false;
            }
            if (isNaN(intphonenum3) == true) {
                alert("Invalid Fax Number. Please don't use any dashes or space");
                ctrl._doOpen();
                frmcourts.txt_CC3.focus();
                return false;
            }

            var faxlength = intphonenum1.length + intphonenum2.length + intphonenum3.length;

            if (faxlength < 10) {
                alert("Invalid Fax Number");
                ctrl._doOpen();
                frmcourts.txt_CC1.focus();
                return false;
            }

            if (frmcourts.ddl_SettingRequest.value == 0) {
                alert("Please specify Setting Request");
                ctrl._doOpen();
                frmcourts.ddl_SettingRequest.focus();
                return false;
            }

            if (frmcourts.ddl_BondType.value == 0) {
                alert("Please specify Bond Type");
                ctrl._doOpen();
                frmcourts.ddl_BondType.focus();
                return false;
            }

            if (isNaN(frmcourts.txt_VisitCharges.value) == true) {
                alert("Sorry Invalid Additional Amount");
                frmcourts.txt_VisitCharges.focus();
                ctrl._doOpen();
                return false;
            }
            if (frmcourts.ddl_status.value == -1) {
                alert("Please specify Status");
                frmcourts.ddl_status.focus();
                ctrl._doOpen();
                return false;
            }

            //Sabir Khan 6000 06/04/2009 Validation for court name...				
            var regex = /^[0-9a-zA-Z\s()-.:,]+$/;
            if (!(regex.test(document.getElementById("txt_CourtName").value))) {
                alert("Court name can only contain alpha numeric and following special characters:\n(  ) : . -  , ");
                document.getElementById("txt_CourtName").focus();
                return false;
            }
            else {
                var crtname = document.getElementById("txt_CourtName").value;
                var count = crtname.length;
                for (var i = 0; i < count; i++) {
                    if (crtname.charAt(i) == '+' || crtname.charAt(i) == '*') {
                        alert("Court name can only contain alpha numeric and following special characters:\n(  ) : . -  , ");
                        document.getElementById("txt_CourtName").focus();
                        return false;
                    }
                }
            }

            //Sabir Khan 5941 07/24/2009 check for county Name.
            if (frmcourts.ddl_CountyName.value == 0) {
                alert("Please specify County Name");
                ctrl._doOpen();
                frmcourts.ddl_CountyName.focus();
                return false;
            }




            //		
            /*
            ctrl = $find('CBECaseIdentification');   
            if (!ctrl)
            return;
		
		if (frmcourts.rblSupportingDoc_0.checked !=true && frmcourts.rblSupportingDoc_1.checked !=true) 
            {
            alert("Please specify Supporting Document");
            ctrl._doOpen();
            frmcourts.rblSupportingDoc_0.focus();
            return false;
            }
		
		if (frmcourts.rblFTAIssues_0.checked !=true && frmcourts.rblFTAIssues_1.checked !=true) 
            {
            alert("Please specify Additonal FTA Isuues");
            ctrl._doOpen();
            frmcourts.rblFTAIssues_0.focus();
            return false;
            }		
		         
            if (frmcourts.rblHandwriting_0.checked !=true && frmcourts.rblHandwriting_1.checked !=true) 
            {
            alert("Please specify HandWriting Allowed");
            ctrl._doOpen();
            frmcourts.rblHandwriting_0.focus();
            return false;
            }
		
		if (frmcourts.rblGracePeriod_0.checked !=true && frmcourts.rblGracePeriod_1.checked !=true) 
            {
            alert("Please specify Grace Period");
            ctrl._doOpen();
            frmcourts.rblGracePeriod_0.focus();
            return false;
            }
		
		if (frmcourts.ddlCaseIdentification.value == -1) 
            {
            alert("Please specify Case Identification");
            ctrl._doOpen();
            frmcourts.ddlCaseIdentification.focus();
            return false;
            }
		
		if (frmcourts.ddlAcceptAppeals.value == -1) 
            {
            alert("Please specify Accept Appeals");
            ctrl._doOpen();
            frmcourts.ddlAcceptAppeals.focus();
            return false;
            }
		
		if (frmcourts.ddlInitialSetting.value == -1) 
            {
            alert("Please specify Initial Setting");
            ctrl._doOpen();
            frmcourts.ddlInitialSetting.focus();
            return false;
            }
		
		if (frmcourts.ddlLOR.value == -1) 
            {
            alert("Please specify LOR Method");
            ctrl._doOpen();
            frmcourts.ddlLOR.focus();
            return false;
            }*/


        }

        function CommentsLength() {
            if (frmcourts.txtComments.value.length > 1000) {
                alert("Sorry You cannot type in more than 1000 characters in Comments");
                return false;
            }

        }



        function validateIsFutureDate(var_date) {
            var txtLORDate = document.getElementById(var_date);
            var today = new Date();
            var LORDate = new Date(txtLORDate.value);

            if (LORDate < today) {

                txtLORDate.focus();
                return false;
            }

        }

        function validateIsWorkingDay(var_date) {
            var txtLORDate = document.getElementById(var_date).value;
            var weekday = new Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday")
            var newseldatestr = formatDate((Date.parseInvariant(txtLORDate).getMonth() + 1) + "/" + Date.parseInvariant(txtLORDate).getDate() + "/" + Date.parseInvariant(txtLORDate).getFullYear(), "MM/dd/yyyy");
            newseldate = Date.parseInvariant(newseldatestr, "MM/dd/yyyy");
            if (weekday[newseldate.getDay()] == "Saturday" || weekday[newseldate.getDay()] == "Sunday") {

                return false;
            }

        }

        function OpenGeneralInfoSectionPanel() {

            ctrl = $find('CBEGeneralInfo');
            if (!ctrl)
                return;
            ctrl._doOpen();



            ctrl = $find('CBECaseIdentification');
            if (!ctrl)
                return;
            ctrl._doClose();

        }

        function ResetControls(type) {

            document.getElementById("divDocumentDetails").style.height = "10px";
            OpenGeneralInfoSectionPanel();
            document.getElementById("txt_CourtName").value = "";
            document.getElementById("txt_JudgeName").value = "";
            document.getElementById("txt_ShortName").value = "";
            document.getElementById("txt_Address1").value = "";
            document.getElementById("txt_Address2").value = "";
            document.getElementById("txt_City").value = "";
            document.getElementById("ddl_States").selectedIndex = 0;
            document.getElementById("txt_ZipCode").value = "";
            document.getElementById("txt_DDName").value = "";
            document.getElementById("dd_time").selectedIndex = 0;
            document.getElementById("chk_duplicateCauseno").checked = false;
            document.getElementById("txt_CourtContact").value = "";
            document.getElementById("txt_CC11").value = "";
            document.getElementById("txt_CC12").value = "";
            document.getElementById("txt_CC13").value = "";
            document.getElementById("txt_CC14").value = "";
            document.getElementById("txt_CC1").value = "";
            document.getElementById("txt_CC2").value = "";
            document.getElementById("txt_CC3").value = "";
            document.getElementById("ddl_SettingRequest").selectedIndex = 0;
            document.getElementById("ddl_BondType").selectedIndex = 0;
            document.getElementById("txt_VisitCharges").value = "";
            document.getElementById("ddl_status").value = -1;
            document.getElementById("cb_rep").checked = false;
            var d = new Date();
            var curr_date = d.getDate();
            var curr_month = d.getMonth();
            curr_month++;
            var curr_year = d.getFullYear();
            document.getElementById("dtpCourtDate").value = curr_month + "/" + curr_date + "/" + curr_year;
            // Noufil 4945 10/16/2008 Set hour and minutes values
            document.getElementById("txt_hour").value = "01";
            document.getElementById("txt_min").value = "00";
            document.getElementById("dd_time")[1].selected = true;
            //document.getElementById("tr_rep").style.display = "none";
            // Rab Nawaz Khan 8870 02/09/2011 included due to alignment issue in chrome and safari browsers
            document.getElementById("tr_rep1").style.display = "none";
            // End 8870
            document.getElementById("tr_rep2").style.display = "none";
            document.getElementById("tr_rep3").style.display = "none";
            document.getElementById("tr_rep4").style.display = "none";
            document.getElementById("tr_rep5").style.display = "none";
            document.getElementById("tr_rep6").style.display = "none";
            document.getElementById("lbl_Msg").innerText = "";
            document.getElementById("ddlCaseIdentification").value = -1;
            document.getElementById("ddlAcceptAppeals").value = -1;
            document.getElementById("ddlInitialSetting").value = -1;
            document.getElementById("ddlLOR").value = -1;
            document.getElementById("rblHandwriting_0").checked = false;
            document.getElementById("rblHandwriting_1").checked = false;
            document.getElementById("rblFTAIssues_0").checked = false;
            document.getElementById("rblFTAIssues_1").checked = false;
            document.getElementById("rblGracePeriod_0").checked = false;
            document.getElementById("rblGracePeriod_1").checked = false;
            document.getElementById("rblSupportingDoc_0").checked = false;
            document.getElementById("rblSupportingDoc_1").checked = false;
            document.getElementById("txtAppealLocation").value = "";
            document.getElementById("txtAppearanceHire").value = "";
            document.getElementById("txtJugeTrial").value = "";
            document.getElementById("txtJuryTrial").value = "";
            document.getElementById("txtContinuance").value = "";
            document.getElementById("txtComments").value = "";

            //Sabir Khan 5763 04/22/2009 reset LOR Subpoena and LOR Motion for discovery.
            document.getElementById("chkIsLorSubpoena").checked = false;
            document.getElementById("chkIsLorMod").checked = false;

            if (document.getElementById("gvCourtFiles") != null) {
                document.getElementById("gvCourtFiles").style.display = "none";
            }
            document.getElementById("hfCourtid").value = "0";
            document.getElementById("btn_Delete").disabled = true;

            var modalPopupBehavior = $find('CBECaseIdentification');
            //Kazim 4087 5/21/2008 Set the expanded size of collapsible panel 
            modalPopupBehavior._expandedSize = "270";
            //Sabir Khan 5941 07/31/2009 Reset county drop down...
            document.getElementById("ddl_CountyName").selectedIndex = 0;
            //Asad Ali 8153 09/20/2010  check condition on reset
            ddl_CaseType_IndexChange();
            return false;
        }

        function showrep() {

            //Muhammad Muneer 8227 09/27/2010 adding the new functionality for LOR Pasedina
            if (document.getElementById("txt_CourtName").value != "Pasadena Municipal Court") {
                if (document.getElementById("cb_rep").checked == true) {
                    document.getElementById("tr_rep").style.display = "block";
                    // Rab Nawaz Khan 8870 02/09/2011 included due to alignment issue in chrome and safari browsers
                    document.getElementById("tr_rep1").style.display = "block";
                    // End 8870
                }
                else {
                    //document.getElementById("tr_rep").style.display = "none";
                    // Rab Nawaz Khan 8870 02/09/2011 included due to alignment issue in chrome and safari browsers
                    document.getElementById("tr_rep1").style.display = "none";
                    // End 8870
                }
            }
            else  //Muhammad Muneer 8227 09/27/2010 adding the new functionality for LOR Pasedina
            {
                if (document.getElementById("cb_rep").checked == true) {
                    document.getElementById("tr_rep").style.display = "block";
                    // Rab Nawaz Khan 8870 02/09/2011 included due to alignment issue in chrome and safari browsers
                    document.getElementById("tr_rep1").style.display = "block";
                    //End 8870
                    document.getElementById("tr_rep2").style.display = "block";
                    document.getElementById("tr_rep3").style.display = "block";
                    document.getElementById("tr_rep4").style.display = "block";
                    document.getElementById("tr_rep5").style.display = "block";
                    document.getElementById("tr_rep6").style.display = "block";
                    //tr_rep2
                }
                else {
                    //document.getElementById("tr_rep").style.display = "none";
                    // Rab Nawaz Khan 8870 02/09/2011 included due to alignment issue in chrome and safari browsers
                    document.getElementById("tr_rep1").style.display = "none";
                    // End 8870
                    document.getElementById("tr_rep2").style.display = "none";
                    document.getElementById("tr_rep3").style.display = "none";
                    document.getElementById("tr_rep4").style.display = "none";
                    document.getElementById("tr_rep5").style.display = "none";
                    document.getElementById("tr_rep6").style.display = "none";

                }
                //  alert('This is Pasedena');
            }


        }
        //Asad Ali 8153 09/20/2010 on Case type dropdown index change 
        function ddl_CaseType_IndexChange() {
            var ddlCasetype = document.getElementById('<%=ddl_casetype.ClientID%>');
            var tr_AssociatedCourts = document.getElementById('<%=tr_AssociatedALRCourt.ClientID%>');
            var rblALRProcess = document.getElementById('rblALRProcess_0');
            // 8997
            var tr_allowCourtNumbers = document.getElementById('<%=tr_allowCourtNumbers.ClientID%>');

            if (ddlCasetype.value == "2" && rblALRProcess.checked == false) {
                tr_AssociatedCourts.style.display = "block";
            }
            else {
                tr_AssociatedCourts.style.display = "none";
            }
            // 8997 
            if (ddlCasetype.value != "1")
                tr_allowCourtNumbers.style.display = "none";
            else
                tr_allowCourtNumbers.style.display = "block";

        }








    </script>

    <style>
        .stratightDate {
            display: initial !important;
        }
    </style>

</head>

<body onkeypress="KeyDownHandler()">
    <!-- START TOPBAR -->

    <form id="frmcourts" method="post" runat="server">

        <aspnew:ScriptManager ID="ScriptManager1" runat="server">
            <Scripts>
                <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
            </Scripts>
        </aspnew:ScriptManager>

        <!-- START CONTAINER -->
        <div class="page-container row-fluid container-fluid">
            <asp:Panel ID="pnl" runat="server">
                <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
            </asp:Panel>
            <!-- START CONTENT -->
            <section id="main-content" class="backroom-courts-page">
    <section class="wrapper main-wrapper row" style=''>

    <div class='col-xs-12'>
        <div class="page-title">

            <div class="pull-left">
                <!-- PAGE HEADING TAG - START --><h1 class="title">Court</h1><!-- PAGE HEADING TAG - END -->                            </div>

                            
                                
        </div>
    </div>
    <div class="clearfix"></div>
    <!-- MAIN CONTENT AREA STARTS -->
    
        <aspnew:UpdatePanel ID="upnlcourt" runat="server" UpdateMode="Conditional">
            <ContentTemplate>

                <div class="col-lg-12">
                    <section class="box ">
                        <header class="panel_header">
                            <h2 class="title pull-left">Court Details</h2>
                            <div class="actions panel_actions pull-right">
                                <asp:LinkButton ID="lbtnAddNewCourt" runat="server" Text="Add New Court" CssClass="form-label" OnClientClick="return ResetControls();" />
                            </div>
                        </header>
                        <div class="content-body">
                            <div class="row">
                                <asp:Label ID="lblMessage" runat="server" TabIndex="40"></asp:Label>
                                <asp:Label ID="lbl_Msg" runat="server" CssClass="Label" ForeColor="Red" TabIndex="27"></asp:Label>
                                <div class="col-xs-12">

                                    <asp:DataGrid ID="dg_Result" runat="server" CssClass="table" PageSize="20" AutoGenerateColumns="False" OnItemDataBound="dg_Result_ItemDataBound"
                                        AllowSorting="True" OnSortCommand="dg_Result_SortCommand" TabIndex="92">
                                        <Columns>
                                            <asp:TemplateColumn HeaderText="&lt;u&gt;Court Name&lt;/u&gt;" SortExpression="courtname">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkbtnCourtNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ccourtname") %>'
                                                        CommandName="CourtNo" TabIndex="93"></asp:LinkButton>
                                                    <asp:Label ID="lbl_CourtID" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.courtid") %>'
                                                        Visible="False">
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="&lt;u&gt;Drop Down Name&lt;/u&gt;" SortExpression="shortcourtname">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label5" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.cshortcourtname")  %>'
                                                        TabIndex="94"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="&lt;u&gt;Short Name&lt;/u&gt;" SortExpression="shortname">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblsname" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.shortname") %>'
                                                        TabIndex="95">
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="&lt;u&gt;Setting Request&lt;/u&gt;" SortExpression="setreqdes">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label2" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.setreqdes") %>'
                                                        TabIndex="96">
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="&lt;u&gt;InActive&lt;/u&gt;" SortExpression="inactiveflag">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblInactive" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.inactiveflag") %>'
                                                        TabIndex="97">
                                                    </asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="&lt;u&gt;Case Type&lt;/u&gt;" SortExpression="casetypename">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_IsCriminalCourt" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.casetypename") %>'
                                                        TabIndex="98">
                                                    </asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="&lt;u&gt;Rep'sLetterDate&lt;/u&gt;" SortExpression="RepDate">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_letterofrep" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.RepDate","{0:d}") %>'
                                                        TabIndex="99">
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                        <PagerStyle NextPageText="Next&amp;gt" PrevPageText="&amp;lt;Previous" HorizontalAlign="Center">
                                        </PagerStyle>
                                    </asp:DataGrid>

                                </div>
                            </div>
                        </div>
                    </section>
                </div>


       





                <asp:Panel ID="pnlModalPopUp" runat="server">
            
                        <div class="col-xs-12">

                            <section class="box ">
                                <asp:Panel ID="panelTitleGeneralInfo" runat="server">
                        
                                    <header class="panel_header">

                                        <h2 class="title pull-left">Court Details</h2>
                                        <div class="actions panel_actions pull-right">
                                            <asp:LinkButton ID="lnkbtnCancelPopUp" runat="server">X</asp:LinkButton>
                                        </div>

                                        <ajaxToolkit:CollapsiblePanelExtender ID="CBECaseIdentification" runat="Server" TargetControlID="pnlCaseIdentification"
                                            CollapsedSize="0" ExpandedSize="265" Collapsed="false" ExpandControlID="panelTitleCaseIdentification"
                                            CollapseControlID="panelTitleCaseIdentification" AutoCollapse="False" AutoExpand="False"
                                            CollapsedText="Show Details..." ExpandedText="Hide Details" ImageControlID="imgCaseInfo"
                                            CollapsedImage="../Images/collapse.jpg" ExpandedImage="../Images/expand.jpg" />
                                        <ajaxToolkit:CollapsiblePanelExtender ID="CBEGeneralInfo" runat="Server" TargetControlID="pnlGeneralInfo"
                                            CollapsedSize="0" ExpandedSize="1100" Collapsed="True" ExpandControlID="panelTitleGeneralInfo"
                                            CollapseControlID="panelTitleGeneralInfo" AutoCollapse="False" AutoExpand="False"
                                            CollapsedText="Show Details..." ExpandedText="Hide Details" ImageControlID="imgGInfo"
                                            ExpandDirection="Vertical" CollapsedImage="../Images/collapse.jpg" ExpandedImage="../Images/expand.jpg" />
                                        <ajaxToolkit:ModalPopupExtender ID="MPECourtDetailPopup" runat="server" BackgroundCssClass="modalBackground"
                                            CancelControlID="lnkbtnCancelPopUp" PopupControlID="pnlModalPopUp" TargetControlID="lbtnAddNewCourt">
                                        </ajaxToolkit:ModalPopupExtender>
                
                                    </header>
                                </asp:Panel>
                                <asp:Panel ID="pnlGeneralInfo" runat="Server" Width="100%" Height="600px">
                                    <div class="content-body">
                                
                                            <div class="row">
                                                <div class="col-md-6 col-sm-7 col-xs-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">Court Name</label>
                                                        <div class="controls">
                                                            <%--<input type="text" class="form-control" id="field-1" >--%>
                                                            <asp:TextBox ID="txt_CourtName" runat="server" CssClass="form-control" MaxLength="50" TabIndex="1"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6 col-sm-7 col-xs-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">Court Contact</label>
                                                        <div class="controls">
                                                            <%--<input type="text" class="form-control" id="field-1" >--%>
                                                            <asp:TextBox ID="txt_CourtContact" runat="server" CssClass="form-control" MaxLength="50" TabIndex="13"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                        
        
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6 col-sm-7 col-xs-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">Judge Name</label>
                                                        <div class="controls">
                                                            <%--<input type="text" class="form-control" id="field-1" >--%>
                                                            <asp:TextBox ID="txt_JudgeName" runat="server" CssClass="form-control" MaxLength="50" TabIndex="2"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6 col-sm-7 col-xs-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">Telephone</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txt_CC11" onkeyup="return autoTab(this, 3, event)" runat="server" Width="70px" CssClass="form-control pull-left" TabIndex="14"></asp:TextBox>
                                                            <p class="pull-left">-</p>
                                                            <asp:TextBox ID="txt_CC12" onkeyup="return autoTab(this, 3, event)" runat="server" Width="32px" CssClass="form-control pull-left" TabIndex="15"></asp:TextBox>
                                                            <p class="pull-left">-</p>
                                                            <asp:TextBox ID="txt_CC13" onkeyup="return autoTab(this, 4, event)" runat="server" Width="32px" CssClass="form-control pull-left" TabIndex="16"></asp:TextBox>
                                                            <p class="pull-left">x</p>
                                                            <asp:TextBox ID="txt_CC14" onkeyup="return autoTab(this, 4, event)" runat="server" Width="40px" CssClass="form-control pull-left" TabIndex="17"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                        
        
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6 col-sm-7 col-xs-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">Short Name</label>
                                                        <div class="controls">
                                                            <%--<input type="text" class="form-control" id="field-1" >--%>
                                                            <asp:TextBox ID="txt_ShortName" runat="server" CssClass="form-control" MaxLength="20" TabIndex="3"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6 col-sm-7 col-xs-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">Fax</label>
                                                        <div class="controls">
                                                            <%--<input type="text" class="form-control" id="field-1" >--%>
                                                            <asp:TextBox ID="txt_CC1" onkeyup="return autoTab(this, 3, event)" runat="server" Width="32px" CssClass="form-control pull-left" TabIndex="18"></asp:TextBox>
                                                            <p class="pull-left">-</p>
                                                            <asp:TextBox ID="txt_CC2" onkeyup="return autoTab(this, 3, event)" runat="server" Width="32px" CssClass="form-control pull-left" TabIndex="19"></asp:TextBox>
                                                            <p class="pull-left">-</p>
                                                            <asp:TextBox ID="txt_CC3" onkeyup="return autoTab(this, 4, event)" runat="server" Width="32px" CssClass="form-control pull-left" TabIndex="20"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                        
        
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6 col-sm-7 col-xs-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">Address #1</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txt_Address1" runat="server" CssClass="form-control" MaxLength="100" TabIndex="4"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6 col-sm-7 col-xs-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">Setting Request</label>
                                                        <div class="controls">
                                                            <asp:DropDownList ID="ddl_SettingRequest" runat="server" CssClass="form-control m-bot15" TabIndex="21"></asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                        
        
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6 col-sm-7 col-xs-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">Address #2</label>
                                                        <div class="controls">
                                                            <%--<input type="text" class="form-control" id="field-1" >--%>
                                                            <asp:TextBox ID="txt_Address2" runat="server" CssClass="form-control" MaxLength="100" TabIndex="5"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6 col-sm-7 col-xs-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">Bond Type</label>
                                                        <div class="controls">
                                                            <asp:DropDownList ID="ddl_BondType" runat="server" CssClass="form-control m-bot15" TabIndex="22">
                                                                <asp:ListItem Value="0">--Choose--</asp:ListItem>
                                                                <asp:ListItem Value="1">Original</asp:ListItem>
                                                                <asp:ListItem Value="2">Bonds</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                        
        
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6 col-sm-7 col-xs-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">City,State,Zip</label>
                                                        <div class="controls">
                                                            <%--<input type="text" class="form-control" id="field-1" >--%>
                                                            <asp:TextBox ID="txt_City" runat="server" Width="76px" CssClass="form-control pull-left" MaxLength="12" TabIndex="6"></asp:TextBox>&nbsp;
                                                            <asp:DropDownList ID="ddl_States" runat="server" Width="76px" CssClass="m-bot15 form-control pull-left" TabIndex="7"></asp:DropDownList>
                                                            &nbsp;
                                                            <asp:TextBox ID="txt_ZipCode" runat="server" Width="76px" CssClass="form-control pull-left" MaxLength="12" TabIndex="8"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6 col-sm-7 col-xs-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">Status</label>
                                                        <div class="controls">
                                                            <%--<input type="text" class="form-control" id="field-1" >--%>
                                                            <asp:DropDownList ID="ddl_status" runat="server" CssClass="form-control m-bot15" TabIndex="23">
                                                                <asp:ListItem Value="-1">--Choose--</asp:ListItem>
                                                                <asp:ListItem Value="0">Active</asp:ListItem>
                                                                <asp:ListItem Value="1">InActive</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                        
        
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6 col-sm-7 col-xs-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">Drop Down Name</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txt_DDName" runat="server" CssClass="form-control m-bot15" MaxLength="50" TabIndex="9"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6 col-sm-7 col-xs-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">Case Type</label>
                                                        <div class="controls">
                                                            <asp:DropDownList ID="ddl_casetype" runat="server" CssClass="form-control m-bot15" TabIndex="23" onchange="ddl_CaseType_IndexChange();" >
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                        
        
                                            </div>
                    
                                            <div class="row">
                                                <div class="col-md-6 col-sm-7 col-xs-8">
                                                    <div id="tr_rep" runat="server">
                                                    <div class="form-check form-check-inline">
  <label class="form-check-label">
    
      <asp:CheckBox ID="cb_rep" CssClass="form-check-input" runat="server" Text="" onclick="showrep();" />Is Letter of Rep
  </label>
</div>
<div class="form-check form-check-inline">
  <label class="form-check-label">
    
       <asp:CheckBox ID="chk_duplicateCauseno" runat="server" CssClass="form-check-input" TabIndex="12" Text="" />Duplicate CauseNo
  </label>
</div>
</div>
                                            
                            
                                                </div>

                                                <div class="col-md-6 col-sm-7 col-xs-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">Additional ($)</label>
                                                        <div class="controls">
                                                             <asp:TextBox ID="txt_VisitCharges" runat="server" CssClass="form-control" MaxLength="5" TabIndex="24"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                        
        
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6 col-sm-7 col-xs-8">
                                                    <table>
                                                        <tr id = "tr_rep1" runat ="server">
                                                            <td id = "tr_allowCourtNumbers" runat = "server" >
                                                                <asp:CheckBox ID = "chkAllowCourtNumbers" runat = "server" CssClass ="Label" Text= "Allow Court Numbers" />
                                                            </td>
                                                            <td>
                                                                <div class="form-group">
                                                                    <label class="form-label" for="field-1">

                                                                        <asp:CheckBox ID="chkIsLorSubpoena" runat="server" Text="Is LOR Subpoena"></asp:CheckBox>
                                                                    </label>
                                                                    <div class="controls">
                                                                        <asp:CheckBox ID="chkIsLorMod" runat="server" CssClass="form-label" Text="Is LOR Motion for Discovery"/>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                            
                                                </div>

                                                <div class="col-md-6 col-sm-7 col-xs-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">Latter of Rep</label>
                                                        <div class="controls dtpCourtDate_outer2">
                                                            <ew:CalendarPopup ID="dtpCourtDate" runat="server" AllowArbitraryText="False" CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Culture="(Default)" LowerBoundDate="1900-01-01" Nullable="True" PadSingleDigits="True"  ShowGoToToday="True" TabIndex="10" Text=" " ToolTip="Call Back Date" UpperBoundDate="12/31/9999 23:59:00" ShowClearDate="True" Width="90px" >
                                                                <TextboxLabelStyle CssClass="form-control pull-left" />
                                                            </ew:CalendarPopup>
                                                            <asp:Label ID="lblcourtID" runat="server" CssClass="Label" Style="display: none">0</asp:Label>
                                                            <asp:TextBox ID="txt_hour" runat="server" CssClass="form-control pull-left" MaxLength="2" Width="50px"></asp:TextBox>
                                                            <p class="pull-left">:</p>
                                                            <asp:TextBox ID="txt_min" runat="server" CssClass="form-control pull-left" MaxLength="2" Width="50px"></asp:TextBox>
                                                            <asp:DropDownList ID="dd_time" runat="server" CssClass="clsInputCombo form-control pull-left">
                                                                <asp:ListItem Selected="True" Text="AM" Value="AM"></asp:ListItem>
                                                                <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                                                            </asp:DropDownList>

                                                        </div>
                                                    </div>
                                                </div>
                        
        
                                            </div>
                    
                                            <div class="row">
                                                <div class="col-md-6 col-sm-7 col-xs-8">
                                                    <tr id="tr_rep2" runat="server"   align="left">
                                                        <td>
                                                            <div class="col-md-12" style="padding:0;">
                                                                <div class="form-group">
                                                                    <label class="form-label" for="field-1">Latter Of Rep 2</label>

                                                                    <div class="controls">
                                                                        <ew:CalendarPopup ID="dtpCourtDate2" runat="server" AllowArbitraryText="False"
                                                                            CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma"
                                                                            Font-Size="8pt" ImageUrl="../images/calendar.gif" LowerBoundDate="1900-01-01"
                                                                            Nullable="True" PadSingleDigits="True"  ShowGoToToday="True"
                                                                            TabIndex="10" Text=" " ToolTip="Call Back Date" UpperBoundDate="12/31/9999 23:59:00" Width="90px">
                                                                            <TextboxLabelStyle CssClass="form-control pull-left" />
                                                                        </ew:CalendarPopup>
                                                                        <asp:Label ID="lblcourtID2" runat="server" CssClass="form-label" Style="display: none">0</asp:Label>
                                                                        <asp:TextBox ID="txt_hour2" runat="server" CssClass="form-control pull-left" MaxLength="2" Width="50px"></asp:TextBox>

                                                                        <p class="pull-left">:</p>
                                                                        
                                                                        <asp:TextBox ID="txt_min2" runat="server" CssClass="form-control pull-left" MaxLength="2" Width="50px"></asp:TextBox>
                                                                        <asp:DropDownList ID="dd_time2" runat="server" CssClass="form-control pull-left m-bot15" Width="70px">
                                                                            <asp:ListItem Selected="True" Text="AM" Value="AM"></asp:ListItem>
                                                                            <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                            </div>
                                
                                                        </td>
                                                    </tr>
                            
                                                </div>

                        
                        
        
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6 col-sm-7 col-xs-8">
                                                    <table>
                                                        <tr id="tr_rep3" runat="server" style="display: none;"  align="left">
                                                            <td>
                                                                <div class="form-group">
                                

                                                                    <label class="form-label" for="field-1">Latter Of Rep 3</label>
                                                                    <div class="controls">
                                                                        <ew:CalendarPopup ID="dtpCourtDate3" runat="server" AllowArbitraryText="False"
                                                                            CalendarLocation="Left" ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma"
                                                                            Font-Size="8pt" ImageUrl="../images/calendar.gif" LowerBoundDate="1900-01-01"
                                                                            Nullable="True" PadSingleDigits="True"  ShowGoToToday="True"
                                                                            TabIndex="10" Text=" " ToolTip="Call Back Date" UpperBoundDate="12/31/9999 23:59:00">
                                                                            <TextboxLabelStyle CssClass="form-label" />
                                                                        </ew:CalendarPopup>
                                                                        <asp:Label ID="lblcourtID3" runat="server" CssClass="form-label" Style="display: none">0</asp:Label>
                                                                        <asp:TextBox ID="txt_hour3" runat="server" CssClass="form-control" MaxLength="2"
                                                                            Width="15px"></asp:TextBox>
                                                                        <span class="clssubhead">&nbsp;:&nbsp;</span>
                                                                        <asp:TextBox ID="txt_min3" runat="server" CssClass="form-control" MaxLength="2"
                                                                            Width="15px"></asp:TextBox>
                                                                        <asp:DropDownList ID="dd_time3" runat="server" CssClass="form-control m-bot15">
                                                                            <asp:ListItem Selected="True" Text="AM" Value="AM"></asp:ListItem>
                                                                            <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                            
                                                </div>

                                                <div class="col-md-6 col-sm-7 col-xs-8">
                                                    <table>
                                                        <tr id="tr_rep4" runat="server" style="display: none;">
                                                            <td>
                                                                <div class="form-group">
                                                                    <label class="form-label" for="field-1">Latter Of Rep 4</label>
                                                                    <div class="controls">
                                                                        <ew:CalendarPopup ID="dtpCourtDate4" runat="server" AllowArbitraryText="False"
                                                                            CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma"
                                                                            Font-Size="8pt" ImageUrl="../images/calendar.gif" LowerBoundDate="1900-01-01"
                                                                            Nullable="True" PadSingleDigits="True"  ShowGoToToday="True"
                                                                            TabIndex="10" Text=" " ToolTip="Call Back Date" UpperBoundDate="12/31/9999 23:59:00">
                                                                            <TextboxLabelStyle CssClass="form-label" />
                                                                        </ew:CalendarPopup>
                                                                        <asp:Label ID="lblcourtID4" runat="server" CssClass="form-label" Style="display: none">0</asp:Label>
                                                                        <asp:TextBox ID="txt_hour4" runat="server" CssClass="form-label" MaxLength="2"
                                                                            Width="15px"></asp:TextBox>
                                                                        <span class="clssubhead">&nbsp;:&nbsp;</span>
                                                                        <asp:TextBox ID="txt_min4" runat="server" CssClass="form-control" MaxLength="2"
                                                                            Width="15px"></asp:TextBox>
                                                                        <asp:DropDownList ID="dd_time4" runat="server" CssClass="form-control m-bot15">
                                                                            <asp:ListItem Selected="True" Text="AM" Value="AM"></asp:ListItem>
                                                                            <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                            
                                                </div>
                        
        
                                            </div>
                    
                                            <div class="row">
                                                <div class="col-md-6 col-sm-7 col-xs-8">
                                                    <table>
                                                        <tr id="tr_rep5" runat="server" style="display: none;">
                                            
                                                            <td class="Label">
                                                                <div class="form-group">
                                                                    <label class="form-label" for="field-1">Letter Of Rep 5</label>
                                                                    <div class="controls">
                                                                        <ew:CalendarPopup ID="dtpCourtDate5" runat="server" AllowArbitraryText="False"
                                                                            CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma"
                                                                            Font-Size="8pt" ImageUrl="../images/calendar.gif" LowerBoundDate="1900-01-01"
                                                                            Nullable="True" PadSingleDigits="True"  ShowGoToToday="True"
                                                                            TabIndex="10" Text=" " ToolTip="Call Back Date" UpperBoundDate="12/31/9999 23:59:00">
                                                                            <TextboxLabelStyle CssClass="form-label" />
                                                                        </ew:CalendarPopup>
                                                                        <asp:Label ID="lblcourtID5" runat="server" CssClass="form-label" Style="display: none">0</asp:Label>
                                                                        <asp:TextBox ID="txt_hour5" runat="server" CssClass="form-control" MaxLength="2"
                                                                            Width="15px"></asp:TextBox>
                                                                        <span class="clssubhead">&nbsp;:&nbsp;</span>
                                                                        <asp:TextBox ID="txt_min5" runat="server" CssClass="form-control" MaxLength="2"
                                                                            Width="15px"></asp:TextBox>
                                                                        <asp:DropDownList ID="dd_time5" runat="server" CssClass="form-control m-bot15">
                                                                            <asp:ListItem Selected="True" Text="AM" Value="AM"></asp:ListItem>
                                                                            <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                           
                                                            </td>
                                                        </tr>
                                                    </table>
                            
                                                </div>

                                                <div class="col-md-6 col-sm-7 col-xs-8">
                                                    <table>
                                                        <tr id="tr_rep6" runat="server" style="display: none;">
                                                            <td class="Label">
                                                                <div class="form-group">
                                                                    <label class="form-label" for="field-1">Letter of Rep 6</label>
                                                                    <div class="controls">
                                                                        <ew:CalendarPopup ID="dtpCourtDate6" runat="server" AllowArbitraryText="False"
                                                                            CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma"
                                                                            Font-Size="8pt" ImageUrl="../images/calendar.gif" LowerBoundDate="1900-01-01"
                                                                            Nullable="True" PadSingleDigits="True"  ShowGoToToday="True"
                                                                            TabIndex="10" Text=" " ToolTip="Call Back Date" UpperBoundDate="12/31/9999 23:59:00">
                                                                            <TextboxLabelStyle CssClass="form-label" />
                                                                        </ew:CalendarPopup>
                                                                        <asp:Label ID="lblcourtID6" runat="server" CssClass="form-label" Style="display: none">0</asp:Label>
                                                                        <asp:TextBox ID="txt_hour6" runat="server" CssClass="form-control" MaxLength="2"
                                                                            Width="15px"></asp:TextBox>
                                                                        <span class="clssubhead">&nbsp;:&nbsp;</span>
                                                                        <asp:TextBox ID="txt_min6" runat="server" CssClass="form-control" MaxLength="2"
                                                                            Width="15px"></asp:TextBox>
                                                                        <asp:DropDownList ID="dd_time6" runat="server" CssClass="form-control m-bot15">
                                                                            <asp:ListItem Selected="True" Text="AM" Value="AM"></asp:ListItem>
                                                                            <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                            
                                        
                                                            </td>
                                                        </tr>
                                                    </table>
                            
                                                </div>
                        
        
                                            </div>
                    
                                            <div class="row">
                                                <div class="col-md-6 col-sm-7 col-xs-8">
                            
                                                    <div class="form-group">
                                                        <asp:Label ID="Label1" Text="County Name" CssClass="form-label" runat="server"></asp:Label>
                                                        <div class="controls">
                                                            <asp:DropDownList ID="ddl_CountyName" runat="server" CssClass="form-control"  TabIndex="23">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                        
                                         
                            
                                                </div>

                                                <div class="col-md-6 col-sm-7 col-xs-8">
                                                    <table>
                                                        <tr runat="server" id="tr_AssociatedALRCourt">
                                                            <td >
                                                                <div class="form-group">
                                                                    <asp:Label ID="lblAssociatedALRCourts" Text="Associated ALR Court" CssClass="form-label" runat="server"></asp:Label>
                                                                    <div class="controls">
                                                                        <asp:DropDownList ID="ddl_AssociatedCourts" runat="server" CssClass="form-control m-bot15"></asp:DropDownList>
                                                                    </div>
                                                                </div>
                                        
                                                                                            
                                                            </td>
                                                        </tr>
                                                    </table>
                            
                                                </div>
                                                <asp:HiddenField ID="hfCourtid" runat="server" Value="1" />
                                                <asp:HiddenField ID="hf_IsALRCourt" runat="server" Value="0" />
                        
                        
        
                                            </div>
                    
                                            <div class="row">
                                                <div class="col-md-6 col-sm-7 col-xs-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">Court Name</label>
                                                        <div class="controls">
                                                            <%--<input type="text" class="form-control" id="field-1" >--%>
                                                            <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" MaxLength="50" TabIndex="1"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6 col-sm-7 col-xs-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">Court Contact</label>
                                                        <span class="desc">e.g. "Beautiful Mind"</span>
                                                        <div class="controls">
                                                            <%--<input type="text" class="form-control" id="field-1" >--%>
                                                            <asp:TextBox ID="TextBox2" runat="server" CssClass="form-control" MaxLength="50" TabIndex="13"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                        
        
                                            </div>


                                

                                    </div>
                                </asp:Panel>
                            </section>

                        </div>
            
        
            

                        <div class="col-xs-12">

                            <section class="box ">
                                <asp:Panel ID="panelTitleCaseIdentification" runat="server" Height="30px" BackImageUrl="~/images/subhead_bg.gif" >
                                    <header class="panel_header">
                                        <h2 class="title pull-left">Case Processing Detail</h2>
                                        <div class="actions panel_actions pull-right">
                	                        <a class="box_toggle fa fa-chevron-down"></a>
                                        </div>
                                    </header>
                                </asp:Panel>
                                <asp:Panel ID="pnlCaseIdentification" runat="Server" Width="100%">
                                    <div class="content-body">
                            

                                            <div class="row">

                                                <div class="col-md-6 col-sm-7 col-xs-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">Appeal Location</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txtAppealLocation" runat="server" CssClass="form-control" MaxLength="200" TabIndex="25"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6 col-sm-7 col-xs-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">Appearnace Hire</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txtAppearanceHire" runat="server" CssClass="form-control" MaxLength="200" TabIndex="34"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">

                                                <div class="col-md-6 col-sm-7 col-xs-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">Judge Trial Hire</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txtJugeTrial" runat="server" CssClass="form-control" MaxLength="200" TabIndex="25"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6 col-sm-7 col-xs-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">Jury Trial hire</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txtJuryTrial" runat="server" CssClass="form-control" MaxLength="200" TabIndex="34"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">

                                                <div class="col-md-6 col-sm-7 col-xs-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">Continuance</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txtContinuance" runat="server" CssClass="form-control" MaxLength="200" TabIndex="25"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6 col-sm-7 col-xs-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">Case Identification</label>
                                                        <div class="controls">
                                                            <asp:DropDownList ID="ddlCaseIdentification" runat="server" CssClass="form-control m-bot15" TabIndex="36">
                                                                <asp:ListItem Selected="True" Value="-1">--Choose--</asp:ListItem>
                                                                <asp:ListItem Value="0">Ticket</asp:ListItem>
                                                                <asp:ListItem Value="1">Cause</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">

                                                <div class="col-md-6 col-sm-7 col-xs-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">Supporting Docs</label>
                                                        <div class="controls">
                                                            <asp:RadioButtonList ID="rblSupportingDoc" runat="server" RepeatDirection="Horizontal" TabIndex="28">
                                                                <asp:ListItem Value="1">&lt;span class=&quot;Label&quot;&gt;Yes&lt;/span&gt;</asp:ListItem>
                                                                <asp:ListItem Value="0">&lt;span class=&quot;Label&quot;&gt;No&lt;/span&gt;</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6 col-sm-7 col-xs-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">Accept Appeals</label>
                                                        <div class="controls">
                                                            <asp:DropDownList ID="ddlAcceptAppeals" runat="server" CssClass="form-control m-bot15" TabIndex="37">
                                                                <asp:ListItem Value="-1">--Choose--</asp:ListItem>
                                                                <asp:ListItem Value="2">Ticket &amp; Cause No</asp:ListItem>
                                                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">

                                                <div class="col-md-6 col-sm-7 col-xs-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1"> Additinal FTA issued</label>
                                                        <div class="controls">
                                                            <asp:RadioButtonList ID="rblFTAIssues" runat="server" RepeatDirection="Horizontal" TabIndex="29">
                                                                <asp:ListItem Value="1">&lt;span class=&quot;Label&quot;&gt;Yes&lt;/span&gt;</asp:ListItem>
                                                                <asp:ListItem Value="0">&lt;span class=&quot;Label&quot;&gt;No&lt;/span&gt;</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6 col-sm-7 col-xs-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">Initial Setting</label>
                                                        <div class="controls">
                                                            <asp:DropDownList ID="ddlInitialSetting" runat="server" CssClass="form-control m-bot15" TabIndex="38">
                                                                <asp:ListItem Selected="True" Value="-1">--Choose--</asp:ListItem>
                                                                <asp:ListItem Value="0">Appearance</asp:ListItem>
                                                                <asp:ListItem Value="1">PreTrial</asp:ListItem>
                                                                <asp:ListItem Value="2">Jury Trial</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">

                                                <div class="col-md-6 col-sm-7 col-xs-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">Handwriting Allowed</label>
                                                        <div class="controls">
                                                            <asp:RadioButtonList ID="rblHandwriting" runat="server" RepeatDirection="Horizontal" TabIndex="30">
                                                                <asp:ListItem Value="1">&lt;span class=&quot;Label&quot;&gt;Yes&lt;/span&gt;</asp:ListItem>
                                                                <asp:ListItem Value="0">&lt;span class=&quot;Label&quot;&gt;No&lt;/span&gt;</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6 col-sm-7 col-xs-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">LOR Method</label>
                                                        <div class="controls">
                                                            <asp:DropDownList ID="ddlLOR" runat="server" CssClass="form-control m-bot15" TabIndex="39">
                                                                <asp:ListItem Selected="True" Value="-1">--Choose--</asp:ListItem>
                                                                <asp:ListItem Value="0">Fax</asp:ListItem>
                                                                <asp:ListItem Value="1">Certified Mail</asp:ListItem>
                                                                <asp:ListItem Value="2">Hand Delivery</asp:ListItem>
                                                                <asp:ListItem Value="3">Mail</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">

                                                <div class="col-md-6 col-sm-7 col-xs-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">Grace Period</label>
                                                        <div class="controls">
                                                            <asp:RadioButtonList ID="rblGracePeriod" runat="server" RepeatDirection="Horizontal" TabIndex="31">
                                                                <asp:ListItem Value="1">&lt;span class=&quot;Label&quot;&gt;Yes&lt;/span&gt;</asp:ListItem>
                                                                <asp:ListItem Value="0">&lt;span class=&quot;Label&quot;&gt;No&lt;/span&gt;</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6 col-sm-7 col-xs-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">ALR Process</label>
                                                        <div class="controls">
                                                            <asp:RadioButtonList ID="rblALRProcess" runat="server" RepeatDirection="Horizontal" TabIndex="31">
                                                                <asp:ListItem Value="1" onclick="ddl_CaseType_IndexChange();">&lt;span class=&quot;Label&quot;&gt;Yes&lt;/span&gt;</asp:ListItem>
                                                                <asp:ListItem Value="0" Selected="True" onclick="ddl_CaseType_IndexChange();">&lt;span class=&quot;Label&quot;&gt;No&lt;/span&gt;</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">

                                                <div class="col-md-6 col-sm-7 col-xs-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">Upload File</label>
                                                        <div class="controls">
                                                            <asp:FileUpload ID="FileUpload1" runat="server" BorderWidth="1px" CssClass="form-control" TabIndex="32" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6 col-sm-7 col-xs-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">Comments</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txtComments" runat="server" CssClass="form-control" MaxLength="1000" TabIndex="33" TextMode="MultiLine" onkeyup="return CommentsLength();"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                    
                            
    
                                    </div>
                                </asp:Panel>
                            </section>

                        </div>

                        <div class="col-lg-12">
                            <section class="box ">
                                <header class="panel_header">
                                    <h2 class="title pull-left">Document Details</h2>
                                </header>
                                <div class="content-body">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div id="divDocumentDetails" runat="server" style="overflow: auto">
                                                <asp:GridView ID="gvCourtFiles" runat="server" AllowSorting="True" AutoGenerateColumns="False" CssClass="table" OnRowCommand="gvCourtFiles_RowCommand" OnRowDataBound="gvCourtFiles_RowDataBound" OnRowDeleting="gvCourtFiles_RowDeleting">
                                        
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Uploaded Date">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDateTime" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.Uploaded Date") %>'></asp:Label>
                                                                &nbsp; &nbsp;
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Document Name">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDocumentName" runat="server" CssClass="form-label"
                                                                    Text='<%# DataBinder.Eval(Container, "DataItem.DocumentName") %>'></asp:Label>
                                                                &nbsp; &nbsp;
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Rep">
                                                            <ItemStyle HorizontalAlign="Left" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblRep" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.rep") %>'></asp:Label>
                                                                &nbsp; &nbsp;
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="View">
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="imgPreviewDoc" runat="server" ImageUrl="../images/Preview.gif"
                                                                    Width="19" />
                                                                <asp:HiddenField ID="hfFileId" runat="server" Value='<%# bind("id") %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lbtnDelete" runat="server" CommandArgument='<%# bind("id") %>'
                                                                    CommandName="Delete" Text="Delete" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                

                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>

                        <div class="col-xs-12">

                            <section class="box ">
                                <div class="content-body">
                        

                                    <div class="row">

                                        <div class="col-md-12 text-center">
                                            <div class="form-group">
                                                <div class="controls">
                                                    <asp:Button ID="btn_Submit" runat="server" CssClass="btn btn-primary" OnClick="btn_Submit_Click" TabIndex="40" Text="Submit" />
                                                    &nbsp;
                                                    <asp:Button ID="btn_Delete" runat="server" CssClass="btn btn-primary" Enabled="False" TabIndex="41" Text="Delete" />
                                                    &nbsp;
                                                    <asp:Button ID="btnResetAdvance" runat="server" CssClass="btn btn-primary" OnClick="btnResetAdvance_Click" OnClientClick="return ResetControls();" TabIndex="42" Text="Reset" />
                                                </div>
                                            </div>
                                        </div>

                            
                                    </div>
                    
    
                                </div>
                            </section>

                        </div>
        
                </asp:Panel>
                        
    <!-- end pnlModalPopUp -->
<!-- modal end -->



        </ContentTemplate>
                    <Triggers>
                        <aspnew:PostBackTrigger ControlID="btn_Submit" />
                    </Triggers>
                </aspnew:UpdatePanel>

<!-- MAIN CONTENT AREA ENDS -->
    </section>
    </section>
            <!-- END CONTENT -->

        </div>
        <!-- END CONTAINER -->
        <!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->
    </form>

    <!-- CORE JS FRAMEWORK - START -->
    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE JS FRAMEWORK - END -->


    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START -->
    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE TEMPLATE JS - START -->
    <script src="../assets/js/scripts.js" type="text/javascript"></script>
    <!-- END CORE TEMPLATE JS - END -->



</body>










































<%--<body onkeypress="KeyDownHandler()">
    <form id="frmcourts" method="post" runat="server">
    <aspnew:ScriptManager ID="ScriptManager1" runat="server">
        <Scripts>
            <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
        </Scripts>
    </aspnew:ScriptManager>
    <table id="TableMain" cellspacing="0" cellpadding="0" width="780" align="center"
        border="0">
        <tr>
            <td>
                <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblMessage" runat="server" TabIndex="40"></asp:Label>&nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lbl_Msg" runat="server" CssClass="Label" ForeColor="Red" TabIndex="27"></asp:Label>
            </td>
        </tr>
        <tr>
            <td background="../../images/separator_repeat.gif" height="10">
            </td>
        </tr>
        <tr>
            <td>
                <aspnew:UpdatePanel ID="upnlcourt" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table>
                            <tr>
                                <td class="clssubhead" valign="middle" align="left" background="../../images/subhead_bg.gif"
                                    style="height: 32px">
                                    <table style="width: 100%; height: 100%" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td class="clssubhead" style="width: 100px">
                                                &nbsp;Court Details
                                            </td>
                                            <td class="clssubhead" align="right">
                                                <asp:LinkButton ID="lbtnAddNewCourt" runat="server" Text="Add New Court" CssClass="clsbutton"
                                                    OnClientClick="return ResetControls();" />&nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 144px">
                                    <table id="tblgrid" cellspacing="0" cellpadding="0" border="0">
                                        <tr>
                                            <td style="width: 100%">
                                                <asp:DataGrid ID="dg_Result" runat="server" CssClass="clsLeftPaddingTable" BorderStyle="None"
                                                    PageSize="20" AutoGenerateColumns="False" BorderColor="White" OnItemDataBound="dg_Result_ItemDataBound"
                                                    AllowSorting="True" OnSortCommand="dg_Result_SortCommand" Width="820px" TabIndex="92">
                                                    <Columns>
                                                        <asp:TemplateColumn HeaderText="&lt;u&gt;Court Name&lt;/u&gt;" SortExpression="courtname">
                                                            <HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkbtnCourtNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ccourtname") %>'
                                                                    CommandName="CourtNo" TabIndex="93"></asp:LinkButton>
                                                                <asp:Label ID="lbl_CourtID" runat="server" CssClass="Labelfrmmain" Text='<%# DataBinder.Eval(Container, "DataItem.courtid") %>'
                                                                    Visible="False">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="&lt;u&gt;Drop Down Name&lt;/u&gt;" SortExpression="shortcourtname">
                                                            <HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="Label5" runat="server" CssClass="Labelfrmmain" Text='<%# DataBinder.Eval(Container, "DataItem.cshortcourtname")  %>'
                                                                    TabIndex="94"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="&lt;u&gt;Short Name&lt;/u&gt;" SortExpression="shortname">
                                                            <HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblsname" runat="server" CssClass="Labelfrmmain" Text='<%# DataBinder.Eval(Container, "DataItem.shortname") %>'
                                                                    TabIndex="95">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="&lt;u&gt;Setting Request&lt;/u&gt;" SortExpression="setreqdes">
                                                            <HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="Label2" runat="server" CssClass="Labelfrmmain" Text='<%# DataBinder.Eval(Container, "DataItem.setreqdes") %>'
                                                                    TabIndex="96">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="&lt;u&gt;InActive&lt;/u&gt;" SortExpression="inactiveflag">
                                                            <HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblInactive" runat="server" CssClass="Labelfrmmain" Text='<%# DataBinder.Eval(Container, "DataItem.inactiveflag") %>'
                                                                    TabIndex="97">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="&lt;u&gt;Case Type&lt;/u&gt;" SortExpression="casetypename">
                                                            <HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_IsCriminalCourt" runat="server" CssClass="Labelfrmmain" Text='<%# DataBinder.Eval(Container, "DataItem.casetypename") %>'
                                                                    TabIndex="98">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="&lt;u&gt;Rep'sLetterDate&lt;/u&gt;" SortExpression="RepDate">
                                                            <HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_letterofrep" runat="server" CssClass="Labelfrmmain" Text='<%# DataBinder.Eval(Container, "DataItem.RepDate","{0:d}") %>'
                                                                    TabIndex="99">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        </asp:TemplateColumn>
                                                    </Columns>
                                                    <PagerStyle NextPageText="Next&amp;gt" PrevPageText="&amp;lt;Previous" HorizontalAlign="Center">
                                                    </PagerStyle>
                                                </asp:DataGrid>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                    <Triggers>
                        <aspnew:PostBackTrigger ControlID="btn_Submit" />
                    </Triggers>
                </aspnew:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td background="../../images/separator_repeat.gif" height="11">
            </td>
        </tr>
        <tr>
            <td>
                <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
            </td>
        </tr>
    </table>
    </form>

    <script>
    
    // Rab Nawaz 8870 03/10/2011 zindex has been added due to google chrom and safari browsers alignment issues 
        document.getElementById("dtpCourtDate_div").style.zIndex = 100003;
        document.getElementById("dtpCourtDate_monthYear").style.zIndex = 100004;                
        document.getElementById("dtpCourtDate2_div").style.zIndex = 100003;
        document.getElementById("dtpCourtDate2_monthYear").style.zIndex = 100004;
        document.getElementById("dtpCourtDate3_div").style.zIndex = 100003;
        document.getElementById("dtpCourtDate3_monthYear").style.zIndex = 100004;
        document.getElementById("dtpCourtDate4_div").style.zIndex = 100003;
        document.getElementById("dtpCourtDate4_monthYear").style.zIndex = 100004;
        document.getElementById("dtpCourtDate5_div").style.zIndex = 100003;
        document.getElementById("dtpCourtDate5_monthYear").style.zIndex = 100004;
        document.getElementById("dtpCourtDate6_div").style.zIndex = 100003;
        document.getElementById("dtpCourtDate6_monthYear").style.zIndex = 100004;
   // end 8870
    </script>

</body>--%>
</html>

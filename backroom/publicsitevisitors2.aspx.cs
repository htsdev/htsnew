using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using lntechNew.Components.ClientInfo;
using HTP.Components;
using System.IO;

namespace HTP.backroom
{
    public partial class publicsitevisitors2 : System.Web.UI.Page
    {
        clsSession ClsSession = new clsSession();
        DataView DV;
        string LastCustomerID = "0";


        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (ClsSession.IsValidSession(this.Request) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else
                {
                    if (!IsPostBack)
                    {
                        calDateTo.SelectedDate = DateTime.Now;
                        calDateFrom.SelectedDate = DateTime.Now;
                    }
                    PagingControl1.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(PagingControl1_PageIndexChanged);
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            gv_Results.PageIndex = 0;
            Session["PSV"] = null;
            FillGrid();
        }

        private void FillGrid()
        {
            try
            {
                lblMessage.Text = "";
                DataTable dt = SullolawReports.GetPubicSiteVisitorsDetail(calDateFrom.SelectedDate.ToShortDateString(), calDateTo.SelectedDate.ToShortDateString(), ddlClientType.SelectedValue);

                if (dt != null && dt.Rows.Count > 0)
                {

                    //Display Last Column CaseType If All Case Selected
                    if (ddlClientType.SelectedValue == "0")
                        gv_Results.Columns[gv_Results.Columns.Count - 1].Visible = true;
                    else
                        gv_Results.Columns[gv_Results.Columns.Count - 1].Visible = false;

                    if (Session["PSV"] == null)
                    {
                        dt = generateSerialNo(dt);
                        DV = new DataView(dt);
                        Session["PSV"] = DV;
                    }
                    else
                    {
                        DV = (DataView)Session["PSV"];
                    }

                    gv_Results.DataSource = DV;
                    gv_Results.DataBind();
                    gv_Results.Visible = true;

                    PagingControl1.PageIndex = gv_Results.PageIndex;
                    PagingControl1.PageCount = gv_Results.PageCount;
                    PagingControl1.SetPageIndex();

                }
                else
                {
                    lblMessage.Text = "No Record Found";
                    gv_Results.Visible = false;
                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }


        }

        void PagingControl1_PageIndexChanged()
        {
            gv_Results.PageIndex = PagingControl1.PageIndex - 1;
            FillGrid();
        }

        protected void gv_Results_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gv_Results.PageIndex = e.NewPageIndex;
            FillGrid();
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                string pf = ((HiddenField)e.Row.FindControl("hf_pf")).Value;
                string cid = ((HiddenField)e.Row.FindControl("hf_cid")).Value;



                if (pf == "True" && cid != "0")
                {
                    Label lbl = (Label)e.Row.FindControl("lblEmail");
                    HiddenField caseno = (HiddenField)e.Row.FindControl("hfTicketId");
                    LinkButton hl = (LinkButton)e.Row.FindControl("hlkEmail");
                    hl.Visible = true;
                    lbl.Visible = false;
                    hl.OnClientClick = "return OpenPopup('" + ConfigurationManager.AppSettings["SullolawURL"] + "/Reports/OnlineReport.aspx?CustomerID=" + cid + "')";

                    if (caseno.Value != "")
                    {

                        if (((HiddenField)e.Row.FindControl("hf_siteid")).Value == "1")
                            ((HyperLink)e.Row.FindControl("hlkSNo")).NavigateUrl = "../ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + caseno.Value;
                        else if (((HiddenField)e.Row.FindControl("hf_siteid")).Value == "2")
                            ((HyperLink)e.Row.FindControl("hlkSNo")).NavigateUrl = ConfigurationManager.AppSettings["DallasURL"] + "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + caseno.Value;
                        else
                            ((HyperLink)e.Row.FindControl("hlkSNo")).CssClass = "Label";

                    }
                    else
                        ((HyperLink)e.Row.FindControl("hlkSNo")).CssClass = "Label";

                }
                else
                {
                    ((HyperLink)e.Row.FindControl("hlkSNo")).CssClass = "Label";
                }


                if (cid != "0")
                {

                    Label lastpage = (Label)e.Row.FindControl("lblLastPage");
                    string path = Server.MapPath("/DOCSTORAGE") + "\\ScanDoc\\images\\" + "C-" + cid + ".pdf";

                    if (((HiddenField)e.Row.FindControl("hf_siteid")).Value == "1")
                    {
                        //lastpage.Text = path;
                        if (File.Exists(path))
                        {
                            lastpage.Text = "<a href=\"#\" onClick=OpenPopup('" + "../DOCSTORAGE/ScanDoc/images/" + "C-" + cid + ".pdf" + "')>" + lastpage.Text + "</a>";

                        }
                    }
                    else
                    {
                        //Zeeshan 4853 09/10/2008 Display Dallas Confirmation Page From Dallas Dockstorage folder.
                        lastpage.Text = "<a href=\"#\" onClick=OpenPopup('" + ConfigurationManager.AppSettings["DallasURL"] + "/DOCSTORAGE/ScanDoc/images/" + "C-" + cid + ".pdf" + "')>" + lastpage.Text + "</a>";
                    }
                }

                if (LastCustomerID == cid)
                {
                    e.Row.Cells[1].Text = "";
                    for (int i = 4; i < gv_Results.Columns.Count; i++)
                        e.Row.Cells[i].Text = "";
                }
                LastCustomerID = cid;

            }
        }

        public DataTable generateSerialNo(DataTable dt)
        {
            if (dt.Columns.Contains("sno") == false)
            {
                dt.Columns.Add("sno");
                int sno = 1;

                //added By Aziz to check if rows existed
                if (dt.Rows.Count >= 1)
                    dt.Rows[0]["sno"] = 1;

                if (dt.Rows.Count >= 2)
                {
                    for (int i = 1; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[i - 1]["CustomerId"].ToString() != dt.Rows[i]["CustomerId"].ToString())
                        {
                            dt.Rows[i]["sno"] = ++sno;
                        }
                    }
                }
            }
            return dt;
        }

        protected void gv_Results_PageIndexChanging1(object sender, GridViewPageEventArgs e)
        {
            gv_Results.PageIndex = e.NewPageIndex;
            FillGrid();
        }

    }
}

<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.frmAddPricePlan" Codebehind="frmAddPricePlan.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Add Price Plan</title>
		<%--<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">--%>

        <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
        <meta cont  nt="C#" name="CODE_LANGUAGE" />
        <meta content="JavaScript" name="vs_defaultClientScript" />
        <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
        <meta http-equiv="X-UA-Compatible" content="IE=5; IE=8; IE=7.5;" />
        <meta http-equiv="X-UA-Compatible" content="chrome=1">
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
        <!-- Favicon -->
        <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
        <!-- For iPhone -->
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
        <!-- For iPhone 4 Retina display -->
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
        <!-- For iPad -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
        <!-- For iPad Retina display -->




        <!-- CORE CSS FRAMEWORK - START -->
        <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
        <!-- CORE CSS FRAMEWORK - END -->

        <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


        <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


        <!-- CORE CSS TEMPLATE - START -->
        <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />

		<SCRIPT src="../Scripts/Validationfx.js" type="text/javascript"></SCRIPT>
		<script language="javascript">

		function ValidateInput()
		{	
			var error = "Please enter correct value.Only float and numeric value are allowed";
			var cnt = parseInt(document.getElementById("txtHidden").value);
			var grdName = "dg_PlanDetail";
			var idx=2;
			var bol = true;
			//alert("asli");
			
			for (idx=2; idx < (cnt+2); idx++)
			{
//				var ctlBasePrice = grdName+ "__ctl" + idx + "_tb_BasePrice";
//				var ctlsecprice = grdName+ "__ctl" + idx + "_tb_secprice";
//				var ctlbasepcent = grdName+ "__ctl" + idx + "_tb_basepcent";
//				var ctlsecpcent = grdName+ "__ctl" + idx + "_tb_secpcent";
//				var ctlbondbase = grdName+ "__ctl" + idx + "_tb_bondbase";
//				var ctlbondsec = grdName+ "__ctl" + idx + "_tb_bondsec";
//				var ctlbondbasepcent = grdName+ "__ctl" + idx + "_tb_bondbasepcent";
//				var ctlbondsecpcent = grdName+ "__ctl" + idx + "_tb_bondsecpcent";
//				var ctlbondassump = grdName+ "__ctl" + idx + "_tb_bondassump";
//				var ctlfineassump = grdName+ "__ctl" + idx + "_tb_fineassump";
//				

				/* testing for changing color
				txt_BasePrice.runtimeStyle.backgroundColor
				txt_BasePrice.currentStyle.color
					color	"#123160"	Variant
				txt_BasePrice.style.backgroundColor
				*/
				var ctlBasePrice = ""; 
				var ctlsecprice = "";
				var ctlbasepcent = "";
				var ctlsecpcent = "";
				var ctlbondbase = "";
				var ctlbondsec = "";
				var ctlbondbasepcent = "";
				var ctlbondsecpcent = "";
				var ctlbondassump = "";
				var ctlfineassump = "";
				
				if( idx < 10)
				{
				 ctlBasePrice = grdName+ "_ctl0" + idx + "_tb_BasePrice";
				 ctlsecprice = grdName+ "_ctl0" + idx + "_tb_secprice";
				 ctlbasepcent = grdName+ "_ctl0" + idx + "_tb_basepcent";
				 ctlsecpcent = grdName+ "_ctl0" + idx + "_tb_secpcent";
				 ctlbondbase = grdName+ "_ctl0" + idx + "_tb_bondbase";
				 ctlbondsec = grdName+ "_ctl0" + idx + "_tb_bondsec";
				 ctlbondbasepcent = grdName+ "_ctl0" + idx + "_tb_bondbasepcent";
				 ctlbondsecpcent = grdName+ "_ctl0" + idx + "_tb_bondsecpcent";
				 ctlbondassump = grdName+ "_ctl0" + idx + "_tb_bondassump";
				 ctlfineassump = grdName+ "_ctl0" + idx + "_tb_fineassump";
				
				
				}
				else
				{
				 ctlBasePrice = grdName+ "_ctl" + idx + "_tb_BasePrice";
				 ctlsecprice = grdName+ "_ctl" + idx + "_tb_secprice";
				 ctlbasepcent = grdName+ "_ctl" + idx + "_tb_basepcent";
				 ctlsecpcent = grdName+ "_ctl" + idx + "_tb_secpcent";
				 ctlbondbase = grdName+ "_ctl" + idx + "_tb_bondbase";
				 ctlbondsec = grdName+ "_ctl" + idx + "_tb_bondsec";
				 ctlbondbasepcent = grdName+ "_ctl" + idx + "_tb_bondbasepcent";
				 ctlbondsecpcent = grdName+ "_ctl" + idx + "_tb_bondsecpcent";
				 ctlbondassump = grdName+ "_ctl" + idx + "_tb_bondassump";
				 ctlfineassump = grdName+ "_ctl" + idx + "_tb_fineassump";
				
				
				}
				
				var txt_BasePrice = document.getElementById(ctlBasePrice);	
				var txt_secprice =document.getElementById(ctlsecprice);	
				var txt_basepcent = document.getElementById(ctlbasepcent);	
				var txt_secpcent = document.getElementById(ctlsecpcent);	
				var txt_bondbase =document.getElementById(ctlbondbase);	
				var txt_bondsec =document.getElementById(ctlbondsec);	
				var txt_bondbasepcent = document.getElementById(ctlbondbasepcent);	
				var txt_bondsecpcent = document.getElementById(ctlbondsecpcent);	
				var txt_bondassump = document.getElementById(ctlbondassump);	
				var txt_fineassump =document.getElementById(ctlfineassump);	
				
				bol = true;
				
				bol = ValidationFloat(txt_BasePrice);
				if (bol==false)
				{
					alert(error)
					return false;
				}

				bol = ValidationFloat(txt_secprice);
				if (bol==false)
				{
					alert(error)
					return false;
				}

				bol = ValidationFloat(txt_basepcent);
				if (bol==false)
				{
					alert(error)
					return false;
				}

				bol = ValidationFloat(txt_secpcent);
				if (bol==false)
				{
					alert(error)
					return false;
				}

				bol = ValidationFloat(txt_bondbase);
				if (bol==false)
				{
					alert(error)
					return false;
				}

				bol = ValidationFloat(txt_bondsec);
				if (bol==false)
				{
					alert(error)
					return false;
				}

				bol = ValidationFloat(txt_bondbasepcent);
				if (bol==false)
				{
					alert(error)
					return false;
				}

				bol = ValidationFloat(txt_bondsecpcent);
				if (bol==false)
				{
					alert(error)
					return false;
				}

				bol = ValidationFloat(txt_bondassump);
				if (bol==false)
				{
					alert(error)
					return false;
				}

				bol = ValidationFloat(txt_fineassump);
				if (bol==false)
				{
					alert(error)
					return false;
				}
			}
			
			
				bol = NullValidation();
				if (bol==false)
				{
					
					return false;
				}
				
				bol = NullValidationShort();
				if (bol==false)
				{
					
					return false;
				}
				
				bol = ddlValidation();
				if (bol==false)
				{
					
					return false;
				}
				
				var calfrm = "calFrom";
				bol = calValidation(calfrm);
				if (bol==false)
				{
					
					return false;
				}
				calT = "calTo";
				bol = calValidation(calT);
				if (bol==false)
				{
					
					return false;
				}
			return bol;
		}
		
		function ValidationFloat(TextBox)
		{
				//alert(TextBox.value);
				//var error = "Please correct value. Only float value is allowed";
				var lblMessage = document.getElementById("lblMessage");

				if(TextBox.value=="")
				{
					TextBox.value = 0;
					return true;
				}
				else
				{
					if (isFloat(TextBox.value)==false) 
					{
						//if -ve and not float
						//lblMessage.innerText = error;
						TextBox.focus();
						
						return false;
					}
					else
					{
						lblMessage.innerText = "";
						return true;
					}
				}
				
		}
		
		function NullValidation()
		{
			
			var lblMessage = document.getElementById("lblMessage");

			var txt = document.getElementById("txtPlanName");
			
			
			if (txt.value == "")
				{		
						var error = "Please enter Plan Name";
						//lblMessage.innerText = error;
						alert(error);
						txt.focus();
						
						return false;
				}
			else
				{
						lblMessage.innerText = "";
						return true;
				}
			}
			
			function NullValidationShort()
		{
			
			var lblMessage = document.getElementById("lblMessage");

			var txt = document.getElementById("txt_ShortName");
			
			
			if (txt.value == "")
				{		
						var error = "Please enter Plan Short Name";
						//lblMessage.innerText = error;
						alert(error);
						txt.focus();
						
						return false;
				}
			else
				{
						lblMessage.innerText = "";
						return true;
				}
			}
			function ddlValidation()	
			{
			var lblMessage = document.getElementById("lblMessage");

			var ddlcrtloc = document.getElementById("ddl_crtloc");
			if (ddlcrtloc.selectedIndex == "0")
			
			{	
			//alert(crtloc.value);
			var error = "Please select Court Location";
			//lblMessage.innerText = error;
			alert(error);
			ddlcrtloc.focus();
						
			return false;
		}
		else
		{
			lblMessage.innerText = "";
			return true;
		} 
		}
		function calValidation(cal)
		{
			
			var lblMessage = document.getElementById("lblMessage");

			var txt = document.getElementById(cal);
			
			
			if (txt.value == "")
				{		
						var error = "Please enter Date";
						//lblMessage.innerText = error;
						alert(error);
						txt.focus();
						
						return false;
				}
			else
				{
						lblMessage.innerText = "";
						return true;
				}
			}
		</script>
	</HEAD>

    <body class=" ">

        <form id="Form1" method="post" runat="server">

            <div class="page-container row-fluid container-fluid">

                <asp:Panel ID="pnl" runat="server">
                    <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                </asp:Panel>

                <!-- START CONTENT -->
                <section id="main-content" class="addPricePlanPage">
                    <section class="wrapper main-wrapper row" style=''>

                    <div class='col-xs-12'>
                        <div class="page-title">

                            <div class="pull-left">
                                <!-- PAGE HEADING TAG - START --><h1 class="title">Add Price Plan</h1><!-- PAGE HEADING TAG - END -->                            </div>

                                            
                                
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <!-- MAIN CONTENT AREA STARTS -->
    
                <div class="col-xs-12">
                    <section class="box ">
                            <header class="panel_header">
                                
                                <div class="actions panel_actions pull-right">
                                    <asp:hyperlink id="HyperLink1" runat="server" NavigateUrl="PrincingMain.aspx">Back to Pricing Main</asp:hyperlink>
                                </div>
                            </header>
                            <div class="content-body">
                                <div class="row">

                        <div class="col-md-6 col-sm-7 col-xs-8">

                            <div class="form-group">
                                <label class="form-label" for="field-1">Name</label>
                                <div class="controls">
                                    <asp:textbox id="txtPlanName" runat="server" CssClass="form-control"></asp:textbox>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-6 col-sm-7 col-xs-8">

                            <div class="form-group">
                                <label class="form-label" for="field-1">Short Name</label>
                                <div class="controls">
                                    <asp:textbox id="txt_ShortName" runat="server" CssClass="form-control"></asp:textbox>
                                </div>
                            </div>

                        </div>

                    </div>

                                <div class="row">

                                    <div class="col-md-3 col-sm-4 col-xs-5">

                                        <div class="form-group">
                                            <label class="form-label" for="field-1">Start Date</label>
                                            <div class="controls">
                                                <ew:calendarpopup id="calFrom" runat="server" EnableHideDropDown="True" ToolTip="Call Back Date" PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00" ShowClearDate="True" Nullable="True" Culture="(Default)"
										            AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Left" ControlDisplay="TextBoxImage">
										            <TextboxLabelStyle CssClass="form-control"></TextboxLabelStyle>
									            </ew:calendarpopup>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-3 col-sm-4 col-xs-5">

                                        <div class="form-group">
                                            <label class="form-label" for="field-1">Start Time</label>
                                            <div class="controls">
                                                <asp:dropdownlist id="ddl_TimeFrom" runat="server" CssClass="form-control m-bot15"></asp:dropdownlist>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-3 col-sm-4 col-xs-5">

                                        <div class="form-group">
                                            <label class="form-label" for="field-1">End Date</label>
                                            <div class="controls">
                                                <ew:calendarpopup id="calTo" runat="server" EnableHideDropDown="True" ToolTip="Call Back Date" PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00" ShowClearDate="True" Nullable="True" Culture="(Default)"
										            AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Left" ControlDisplay="TextBoxImage">
										            <TextboxLabelStyle CssClass="form-control"></TextboxLabelStyle>
									            </ew:calendarpopup>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-3 col-sm-4 col-xs-5">

                                        <div class="form-group">
                                            <label class="form-label" for="field-1">End Time</label>
                                            <div class="controls">
                                                <asp:dropdownlist id="ddl_TimeTo" runat="server" CssClass="form-control m-bot15"></asp:dropdownlist>
                                            </div>
                                        </div>

                                    </div>                        

                                </div>

                                <div class="row">

                                    <div class="col-md-6 col-sm-7 col-xs-8">

                                        <div class="form-group">
                                            <label class="form-label" for="field-1">Court Location</label>
                                            <div class="controls">
                                                <asp:dropdownlist id="ddl_crtloc" runat="server" CssClass="form-control m-bot15"></asp:dropdownlist>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-6 col-sm-7 col-xs-8">

                                        <div class="form-group">
                                            <label class="form-label" for="field-1">Active/Inactive</label>
                                            <div class="controls">
                                                <asp:radiobutton id="rdo_Active" runat="server" CssClass="form-label" Text="Active" GroupName="Planstat"></asp:radiobutton>
                                                <asp:radiobutton id="rdo_Inactive" runat="server" CssClass="form-label" Text="Inactive" GroupName="Planstat"></asp:radiobutton>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                                <div class="row">

                                    <div class="col-md-6 col-sm-7 col-xs-8">

                                        <div class="form-group">
                                            <asp:label id="lblMessage" runat="server" CssClass="form-label" ForeColor="Red" EnableViewState="False"></asp:label>
                                        </div>

                                    </div>

                                </div>

                                <div class="row">

                                    <div class="col-md-6 col-sm-7 col-xs-8">

                                        <div class="form-group">
                                            <div class="controls">
                                                <asp:checkbox id="chkb_savetemplate" runat="server" CssClass="form-label pull-right" Text="Save As Template"></asp:checkbox>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-6 col-sm-7 col-xs-8">

                                        <div class="form-group">
                                            <div class="controls">
                                                <asp:button id="btn_Save" runat="server" CssClass="btn btn-primary pull-right" Text="Save Changes"></asp:button>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                    </div>
                        </section></div>

                        <div class="col-lg-12">
            <section class="box ">
                <header class="panel_header">

                    <h2 class="title pull-left">Plan Name</h2>
                    <asp:dropdownlist id="ddl_templetes" runat="server" style="margin-top: 8px; margin-left: 10px;" Width="35%" CssClass="form-control m-bot15 pull-left col-md-6" AutoPostBack="True"></asp:dropdownlist>
                    
                </header>
                <div class="content-body">
                    <div class="row">
                        <div class="col-xs-12">

                            <asp:datagrid id="dg_PlanDetail" runat="server" CssClass="table" style="font-size:85%" AutoGenerateColumns="False">
								<Columns>
									<asp:TemplateColumn HeaderText="Violation Category">
										<ItemTemplate>
											<asp:Label id=lbl_voilcategory runat="server" CssClass="form-label" style="font-weight: bold" Text='<%# DataBinder.Eval(Container, "DataItem.categoryname") %>'>
											</asp:Label>
											<asp:Label id=lbl_hidCategoryID runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.categoryid") %>' Visible="False">
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Base Price">
										<ItemTemplate>
											<asp:TextBox id=tb_baseprice runat="server" style="font-size:100%" CssClass="form-control" Text='<%# DataBinder.Eval(Container, "DataItem.BasePrice", "{0:F}") %>'>
											</asp:TextBox>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Secondary Price">
										<ItemTemplate>
											<asp:TextBox id=tb_secprice runat="server" style="font-size:100%" CssClass="form-control" Text='<%# DataBinder.Eval(Container, "DataItem.SecondaryPrice", "{0:F}") %>' MaxLength="5">
											</asp:TextBox>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Base%">
										<ItemTemplate>
											<asp:TextBox id=tb_basepcent runat="server" style="font-size:100%" CssClass="form-control" Text='<%# DataBinder.Eval(Container, "DataItem.BasePercentage", "{0:F}") %>' MaxLength="5">
											</asp:TextBox>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Secondary%">
										<ItemTemplate>
											<asp:TextBox id=tb_secpcent runat="server" CssClass="form-control" Text='<%# DataBinder.Eval(Container, "DataItem.SecondaryPercentage", "{0:F}") %>' MaxLength="5">
											</asp:TextBox>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Bond Base">

										<ItemTemplate>
											<asp:TextBox id=tb_bondbase runat="server" style="font-size:100%" CssClass="form-control" Text='<%# DataBinder.Eval(Container, "DataItem.BondBase", "{0:F}") %>' MaxLength="5">
											</asp:TextBox>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Bond Secondary">
										<ItemTemplate>
											<asp:TextBox id=tb_bondsec runat="server" style="font-size:100%" CssClass="form-control" Text='<%# DataBinder.Eval(Container, "DataItem.BondSecondary", "{0:F}") %>' MaxLength="5">
											</asp:TextBox>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Bond Base%">
										<ItemTemplate>
											<asp:TextBox id=tb_bondbasepcent runat="server" style="font-size:100%" CssClass="form-control" Text='<%# DataBinder.Eval(Container, "DataItem.BondBasePercentage", "{0:F}") %>' MaxLength="5">
											</asp:TextBox>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Bond Secondary%">
										<ItemTemplate>
											<asp:TextBox id=tb_bondsecpcent runat="server" style="font-size:100%" CssClass="form-control" Text='<%# DataBinder.Eval(Container, "DataItem.BondSecondaryPercentage", "{0:F}") %>' MaxLength="5">
											</asp:TextBox>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Bond Assumption">
										<ItemTemplate>
											<asp:TextBox id=tb_bondassump runat="server" style="font-size:100%" CssClass="form-control" Text='<%# DataBinder.Eval(Container, "DataItem.BondAssumption", "{0:F}")%>' MaxLength="5">
											</asp:TextBox>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Fine Assumption">
										<ItemTemplate>
											<asp:TextBox id=tb_fineassump runat="server" style="font-size:100%" CssClass="form-control" Text='<%# DataBinder.Eval(Container, "DataItem.FineAssumption", "{0:F}") %>' MaxLength="5">
											</asp:TextBox>
										</ItemTemplate>
									</asp:TemplateColumn>
								</Columns>
							</asp:datagrid>

                            <INPUT id="txtHidden" type="hidden" runat="server"><asp:label id="test" runat="server" Height="8px" Visible="False">Label</asp:label>

                            

                        </div>
                    </div>
                </div>
            </section>
        </div>




                <!-- MAIN CONTENT AREA ENDS -->
                    </section>
                    </section>
                <!-- END CONTENT --> 

            </div>
            <!-- END CONTAINER -->

        </form>

        <!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->


        <!-- CORE JS FRAMEWORK - START --> 
        <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script> 
        <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script> 
        <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>  
        <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script> 
        <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>  
        <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
        <!-- CORE JS FRAMEWORK - END --> 


        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 

        <script src="../assets/plugins/autosize/autosize.min.js" type="text/javascript"></script>
        <script src="../assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>
        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


        <!-- CORE TEMPLATE JS - START --> 
        <script src="../assets/js/scripts.js" type="text/javascript"></script> 
        <!-- END CORE TEMPLATE JS - END --> 


        <!-- General section box modal start -->
        <div class="modal" id="section-settings" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog animated bounceInDown">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Section Settings</h4>
                    </div>
                    <div class="modal-body">

                        Body goes here...

                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <button class="btn btn-success" type="button">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- modal end -->
    </body>



















	<%--<body >
		<form id="Form1" method="post" runat="server">
			<TABLE id="TableMain" style="Z-INDEX: 101" cellSpacing="0" cellPadding="0" width="780"
				align="center" border="0">
				<TR>
					<TD><uc1:activemenu id="ActiveMenu1" runat="server"></uc1:activemenu></TD>
				</TR>
				<tr>
					<td background="../../images/separator_repeat.gif"  height="11"></td>
				</tr>
				<TR>
					<TD>
						<TABLE id="TableHeader" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
							<TR>
								<TD class="clsLeftPaddingTable" style="WIDTH: 75px; HEIGHT: 10px" colSpan="1" rowSpan="1"><STRONG></STRONG></TD>
								<TD class="clsLeftPaddingTable" style="WIDTH: 172px; HEIGHT: 10px" colSpan="1">&nbsp;</TD>
								<TD class="clsLeftPaddingTable" style="WIDTH: 83px; HEIGHT: 10px"></TD>
								<TD class="clsLeftPaddingTable" style="HEIGHT: 10px" align="right" colSpan="3"><asp:hyperlink id="HyperLink1" runat="server" NavigateUrl="PrincingMain.aspx">Back to Pricing Main</asp:hyperlink></TD>
							</TR>
							<TR>
								<TD class="clsLeftPaddingTable" style="WIDTH: 75px; HEIGHT: 18px">Name</TD>
								<TD class="clsLeftPaddingTable" style="WIDTH: 172px; HEIGHT: 18px"><asp:textbox id="txtPlanName" runat="server" Width="176px" CssClass="clsinputadministration"></asp:textbox></TD>
								<TD class="clsLeftPaddingTable" style="WIDTH: 83px"></TD>
								<TD class="clsLeftPaddingTable" style="WIDTH: 133px" colSpan="2"></TD>
							</TR>
							<TR>
								<TD class="clsLeftPaddingTable" style="WIDTH: 75px; HEIGHT: 14px">Start&nbsp;Date</TD>
								<TD class="clsLeftPaddingTable" style="WIDTH: 172px; HEIGHT: 14px"><ew:calendarpopup id="calFrom" runat="server" Width="80px" EnableHideDropDown="True" Font-Size="8pt"
										Font-Names="Tahoma" ToolTip="Call Back Date" PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00" ShowClearDate="True" Nullable="True" Culture="(Default)"
										AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Left" ImageUrl="../images/calendar.gif" ControlDisplay="TextBoxImage">
										<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
										<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></WeekdayStyle>
										<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></MonthHeaderStyle>
										<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
											BackColor="AntiqueWhite"></OffMonthStyle>
										<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></GoToTodayStyle>
										<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGoldenrodYellow"></TodayDayStyle>
										<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Orange"></DayHeaderStyle>
										<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGray"></WeekendStyle>
										<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></SelectedDateStyle>
										<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></ClearDateStyle>
										<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></HolidayStyle>
									</ew:calendarpopup><asp:dropdownlist id="ddl_TimeFrom" runat="server" Width="70px" CssClass="clsinputcombo"></asp:dropdownlist><textboxlabelstyle cssclass="clstextarea"></TEXTBOXLABELSTYLE><WEEKDAYSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
										BackColor="White"></WEEKDAYSTYLE><MONTHHEADERSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
										BackColor="Yellow"></MONTHHEADERSTYLE><OFFMONTHSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
										BackColor="AntiqueWhite"></OFFMONTHSTYLE><GOTOTODAYSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
										BackColor="White"></GOTOTODAYSTYLE><TODAYDAYSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
										BackColor="LightGoldenrodYellow"></TODAYDAYSTYLE><DAYHEADERSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
										BackColor="Orange"></DAYHEADERSTYLE><WEEKENDSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
										BackColor="LightGray"></WEEKENDSTYLE><SELECTEDDATESTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
										BackColor="Yellow"></SELECTEDDATESTYLE><CLEARDATESTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
										BackColor="White"></CLEARDATESTYLE><HOLIDAYSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
										BackColor="White"></HOLIDAYSTYLE></TD>
								<TD class="clsLeftPaddingTable" style="WIDTH: 83px; HEIGHT: 14px">Court Location</TD>
								<TD class="clsLeftPaddingTable" style="WIDTH: 144px; HEIGHT: 14px"><TEXTBOXLABELSTYLE CssClass="clstextarea"><asp:dropdownlist id="ddl_crtloc" runat="server" CssClass="clsinputcombo"></asp:dropdownlist>
									</TEXTBOXLABELSTYLE><WEEKDAYSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
										BackColor="White"></WEEKDAYSTYLE><MONTHHEADERSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
										BackColor="Yellow"></MONTHHEADERSTYLE><OFFMONTHSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
										BackColor="AntiqueWhite"></OFFMONTHSTYLE><GOTOTODAYSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
										BackColor="White"></GOTOTODAYSTYLE><TODAYDAYSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
										BackColor="LightGoldenrodYellow"></TODAYDAYSTYLE><DAYHEADERSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
										BackColor="Orange"></DAYHEADERSTYLE><WEEKENDSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
										BackColor="LightGray"></WEEKENDSTYLE><SELECTEDDATESTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
										BackColor="Yellow"></SELECTEDDATESTYLE><CLEARDATESTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
										BackColor="White"></CLEARDATESTYLE><HOLIDAYSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
										BackColor="White"></HOLIDAYSTYLE></TD>
								<TD class="clsLeftPaddingTable" style="HEIGHT: 14px" align="right"><asp:checkbox id="chkb_savetemplate" runat="server" CssClass="clslabelnew" Text="Save As Template"></asp:checkbox>&nbsp;</TD>
							</TR>
							<TR>
								<TD class="clsLeftPaddingTable" style="WIDTH: 75px">End Date</TD>
								<TD class="clsLeftPaddingTable" style="WIDTH: 172px"><ew:calendarpopup id="calTo" runat="server" Width="80px" EnableHideDropDown="True" Font-Size="8pt"
										Font-Names="Tahoma" ToolTip="Call Back Date" PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00" ShowClearDate="True" Nullable="True" Culture="(Default)"
										AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Left" ImageUrl="../images/calendar.gif" ControlDisplay="TextBoxImage">
										<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
										<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></WeekdayStyle>
										<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></MonthHeaderStyle>
										<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
											BackColor="AntiqueWhite"></OffMonthStyle>
										<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></GoToTodayStyle>
										<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGoldenrodYellow"></TodayDayStyle>
										<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Orange"></DayHeaderStyle>
										<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGray"></WeekendStyle>
										<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></SelectedDateStyle>
										<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></ClearDateStyle>
										<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></HolidayStyle>
									</ew:calendarpopup><asp:dropdownlist id="ddl_TimeTo" runat="server" Width="70px" CssClass="clsinputcombo"></asp:dropdownlist></TD>
								<TD class="clsLeftPaddingTable" style="WIDTH: 83px"></TD>
								<TD class="clsLeftPaddingTable" style="WIDTH: 144px"><asp:radiobutton id="rdo_Active" runat="server" CssClass="clslabelnew" Text="Active" GroupName="Planstat"></asp:radiobutton><asp:radiobutton id="rdo_Inactive" runat="server" CssClass="clslabelnew" Text="Inactive" GroupName="Planstat"></asp:radiobutton></TD>
								<TD class="clsLeftPaddingTable" align="right"><asp:button id="btn_Save" runat="server" CssClass="clsbutton" Text="Save Changes"></asp:button>&nbsp;</TD>
							</TR>
							<TR>
								<TD class="clsLeftPaddingTable" style="WIDTH: 75px; HEIGHT: 18px">Short Name</TD>
								<TD class="clsLeftPaddingTable" style="WIDTH: 172px; HEIGHT: 18px"><asp:textbox id="txt_ShortName" runat="server" Width="176px" CssClass="clsinputadministration"></asp:textbox></TD>
								<TD class="clsLeftPaddingTable" style="WIDTH: 83px"></TD>
								<TD class="clsLeftPaddingTable" style="WIDTH: 133px" colSpan="2"></TD>
							</TR>
						</TABLE>
						<asp:label id="lblMessage" runat="server" Width="320px" ForeColor="Red" EnableViewState="False"></asp:label></TD>
				</TR>
				<TR>
					<TD background="../../images/headbar_headerextend.gif" height="5"></TD>
				</TR>
				<TR>
					<TD  class="clssubhead" background="../../Images/subhead_bg.gif" height="34px" style="width: 883px">&nbsp;
						<asp:label id="Label1" runat="server" Width="72px" CssClass="clssubhead" BackColor="#EEC75E"
							BorderColor="Transparent">Plan Name</asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<asp:dropdownlist id="ddl_templetes" runat="server" Width="152px" CssClass="clsinputcombo" AutoPostBack="True"></asp:dropdownlist></TD>
					<TD  class="clssubhead" background="../../Images/subhead_bg.gif" height="34px" style="width: 883px"></TD>
				</TR>
				<TR>
					<TD background="../../images/headbar_footerextend.gif" height="10"></TD>
				</TR>
				<TR>
					<TD>
						<TABLE id="tblgrid" cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
							<TR>
								<TD align="center"><asp:datagrid id="dg_PlanDetail" runat="server" Width="780px" CssClass="clsLeftPaddingTable" AutoGenerateColumns="False">
										<Columns>
											<asp:TemplateColumn HeaderText="Violation Category">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id=lbl_voilcategory runat="server" CssClass="label" Width="116px" Text='<%# DataBinder.Eval(Container, "DataItem.categoryname") %>'>
													</asp:Label>
													<asp:Label id=lbl_hidCategoryID runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.categoryid") %>' Visible="False">
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Base Price">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<asp:TextBox id=tb_baseprice runat="server" CssClass="clsinputadministration" Width="45px" Text='<%# DataBinder.Eval(Container, "DataItem.BasePrice", "{0:F}") %>'>
													</asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Secondary Price">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<asp:TextBox id=tb_secprice runat="server" CssClass="clsinputadministration" Width="45px" Text='<%# DataBinder.Eval(Container, "DataItem.SecondaryPrice", "{0:F}") %>' MaxLength="5">
													</asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Base%">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<asp:TextBox id=tb_basepcent runat="server" Width="45px" CssClass="clsinputadministration" Text='<%# DataBinder.Eval(Container, "DataItem.BasePercentage", "{0:F}") %>' MaxLength="5">
													</asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Secondary%">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<asp:TextBox id=tb_secpcent runat="server" Width="45px" CssClass="clsinputadministration" Text='<%# DataBinder.Eval(Container, "DataItem.SecondaryPercentage", "{0:F}") %>' MaxLength="5">
													</asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Bond Base">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<asp:TextBox id=tb_bondbase runat="server" Width="45px" CssClass="clsinputadministration" Text='<%# DataBinder.Eval(Container, "DataItem.BondBase", "{0:F}") %>' MaxLength="5">
													</asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Bond Secondary">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<asp:TextBox id=tb_bondsec runat="server" Width="45px" CssClass="clsinputadministration" Text='<%# DataBinder.Eval(Container, "DataItem.BondSecondary", "{0:F}") %>' MaxLength="5">
													</asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Bond Base%">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<asp:TextBox id=tb_bondbasepcent runat="server" Width="45px" CssClass="clsinputadministration" Text='<%# DataBinder.Eval(Container, "DataItem.BondBasePercentage", "{0:F}") %>' MaxLength="5">
													</asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Bond Secondary%">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<asp:TextBox id=tb_bondsecpcent runat="server" Width="45px" CssClass="clsinputadministration" Text='<%# DataBinder.Eval(Container, "DataItem.BondSecondaryPercentage", "{0:F}") %>' MaxLength="5">
													</asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Bond Assumption">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<asp:TextBox id=tb_bondassump runat="server" Width="45px" CssClass="clsinputadministration" Text='<%# DataBinder.Eval(Container, "DataItem.BondAssumption", "{0:F}")%>' MaxLength="5">
													</asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Fine Assumption">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<asp:TextBox id=tb_fineassump runat="server" Width="45px" CssClass="clsinputadministration" Text='<%# DataBinder.Eval(Container, "DataItem.FineAssumption", "{0:F}") %>' MaxLength="5">
													</asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
									</asp:datagrid></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD align="right"></TD>
				</TR>
				<TR>
					<TD background="../../images/separator_repeat.gif"  height="11"></TD>
				</TR>
				<TR>
					<TD ><uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
			<div><INPUT id="txtHidden" type="hidden" runat="server"><asp:label id="test" runat="server" Height="8px" Visible="False">Label</asp:label></div>
		</form>
	</body>--%>
</HTML>

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using HTP.Components;
using lntechNew.Components;

namespace HTP.backroom
{
    public partial class publicsitecallquote : System.Web.UI.Page
    {
        clsSession ClsSession = new clsSession();
        int statuscount = 0;
        DataView DV;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (ClsSession.IsValidSession(this.Request) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }

                if (!IsPostBack)
                {
                    calstartdate.SelectedDate = DateTime.Today;
                    calenddate.SelectedDate = DateTime.Today;

                    FillGrids();
                }
                PagingControl1.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(PagingControl1_PageIndexChanged);
            }
            catch (Exception Ex)
            {
                lblMessage.Text = Ex.Message;
                clsLogger.ErrorLog(Ex);
            }
        }

        private void FillGrids()
        {
            try
            {

                lblMessage.Text = "";
                DataTable dt = SullolawReports.GetCallQuoteDetail(calstartdate.SelectedDate.ToShortDateString(), calenddate.SelectedDate.ToShortDateString(), ddlClientType.SelectedValue);

                if (dt != null && dt.Rows.Count > 0)
                {

                    //Display Last Column CaseType If All Case Selected
                    if (ddlClientType.SelectedValue == "0")
                        gv_Results.Columns[gv_Results.Columns.Count - 1].Visible = true;
                    else
                        gv_Results.Columns[gv_Results.Columns.Count - 1].Visible = false;


                    lblMessage.Visible = false;
                    if (Session["CallQuote"] == null)
                    {
                        ValidationReports validationReport = new ValidationReports();

                        validationReport.generateAllserials(dt.DataSet);

                        DV = new DataView(dt);
                        Session["CallQuote"] = DV;
                    }
                    else
                    {
                        DV = (DataView)Session["CallQuote"];
                    }


                    gv_Results.DataSource = DV;
                    gv_Results.DataBind();
                    gv_Results.Visible = true;

                    PagingControl1.PageIndex = gv_Results.PageIndex;
                    PagingControl1.PageCount = gv_Results.PageCount;
                    PagingControl1.SetPageIndex();

                }
                else
                {
                    lblMessage.Visible = true;
                    lblMessage.Text = "No Record Found";
                    gv_Results.Visible = false;
                }

            }
            catch (Exception Ex)
            {
                lblMessage.Text = Ex.Message;
                clsLogger.ErrorLog(Ex);
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Session["CallQuote"] = null;
            FillGrids();
        }

        protected void gv_Results_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    statuscount++;
                    Label status = ((Label)e.Row.FindControl("lblhirestatus"));
                    HiddenField hfstatus = ((HiddenField)e.Row.FindControl("hfStatus"));
                    HiddenField hfticketid = ((HiddenField)e.Row.FindControl("hfTicketId"));
                    HiddenField hf_siteid = ((HiddenField)e.Row.FindControl("hf_siteid"));

                    if (hfstatus.Value == "1")
                        status.Text = "Hired";
                    else if (hfstatus.Value == "0" && hfticketid.Value != "0")
                        status.Text = "Quote";
                    else
                        status.Text = "None";

                    if (hfticketid.Value != "0")
                    {
                        ((LinkButton)e.Row.FindControl("lnkbtnClientname")).Visible = true;
                        ((Label)e.Row.FindControl("lbltktno")).Visible = false;

                        if (hf_siteid.Value == "1")
                            ((LinkButton)e.Row.FindControl("lnkbtnClientname")).PostBackUrl = "~/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + hfticketid.Value;
                        else
                            ((LinkButton)e.Row.FindControl("lnkbtnClientname")).PostBackUrl = ConfigurationManager.AppSettings["DallasURL"] + "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + hfticketid.Value;
                    }
                    else
                    {
                        ((LinkButton)e.Row.FindControl("lnkbtnClientname")).Visible = false;
                        ((Label)e.Row.FindControl("lbltktno")).Visible = true;

                    }

                }
            }
            catch (Exception Ex)
            {
                lblMessage.Text = Ex.Message;
                clsLogger.ErrorLog(Ex);
            }
        }

        void PagingControl1_PageIndexChanged()
        {
            gv_Results.PageIndex = PagingControl1.PageIndex - 1;
            FillGrids();
        }

        protected void gv_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gv_Results.PageIndex = e.NewPageIndex;
            FillGrids();
        }
    }
}
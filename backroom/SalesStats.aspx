<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.Backroom.SalesStats" Codebehind="SalesStats.aspx.cs" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Sales Stats</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<SCRIPT src="../Scripts/Validationfx.js" type="text/javascript"></SCRIPT>
		<SCRIPT src="../Scripts/Dates.js" type="text/javascript"></SCRIPT>
		<script language="javascript">
		
	      function OpenScanPreview(DocID,DocNum)
	      {	      
	     //a;
	          var PDFWin
		      PDFWin = window.open("../../ClientInfo/updateviolation.asp?txtviolationid="+violationid,"","fullscreen=no,toolbar=no,width=640,height=620,left=120,top=100,status=no,menubar=no,scrollbars=yes,resizable=yes");				
		      return false;				      
	       } 
	       
	       function validate()
		{
			//a;
			var d1 = document.getElementById("calQueryFrom").value
			var d2 = document.getElementById("calQueryTo").value			
			
			if (d1 == d2)
				return true;
			if (compareDates(d2,'MM/dd/yyyy',d1,'MM/dd/yyyy')==false)
				{
					alert("start date cannot greater than end date");
					document.getElementById("calQueryTo").focus(); 
					return false;
				}
				return true;
		}
	  
		</script>
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
			<TABLE id="TableMain" style="Z-INDEX: 200" cellSpacing="0" cellPadding="0" width="780"
				align="center" border="0">
				<tbody>
					<tr>
						<td style="HEIGHT: 14px" colSpan="4">
						<uc1:activemenu id="ActiveMenu1" runat="server"></uc1:activemenu>
						</td>
					</tr>
					<TR>
						<TD>
							<TABLE id="TableSub" cellSpacing="0" cellPadding="0" width="100%" border="0">
								<tr>
									<td background="../../images/separator_repeat.gif" style="height: 7"></td>
								</tr>
								<TR>
									<TD align="center" style="height: 50px">
										<TABLE id="Table6" cellSpacing="0" cellPadding="0" bgColor="white" border="0" style="width: 777px; height: 1px">
											<tr>
												<TD style="WIDTH: 257px; HEIGHT: 13px" width="257" height="13"><STRONG>&nbsp;Date 
														:&nbsp;</STRONG>&nbsp;
													<ew:calendarpopup id="calQueryFrom" runat="server" Nullable="True" Width="80px" ImageUrl="../images/calendar.gif"
														Font-Names="Tahoma" Font-Size="8pt" ControlDisplay="TextBoxImage" CalendarLocation="Left" ShowGoToToday="True"
														AllowArbitraryText="False" Culture="(Default)" UpperBoundDate="12/31/9999 23:59:00" PadSingleDigits="True"
														ToolTip="Call Back Date" ShowClearDate="True">
														<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
														<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></WeekdayStyle>
														<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="Yellow"></MonthHeaderStyle>
														<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
															BackColor="AntiqueWhite"></OffMonthStyle>
														<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></GoToTodayStyle>
														<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="LightGoldenrodYellow"></TodayDayStyle>
														<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="Orange"></DayHeaderStyle>
														<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="LightGray"></WeekendStyle>
														<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="Yellow"></SelectedDateStyle>
														<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></ClearDateStyle>
														<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></HolidayStyle>
													</ew:calendarpopup>&nbsp;-&nbsp;</TD>
												<TD style="HEIGHT: 13px" width="200"><ew:calendarpopup id="calQueryTo" runat="server" Nullable="True" Width="80px" ImageUrl="../images/calendar.gif"
														Font-Names="Tahoma" Font-Size="8pt" ControlDisplay="TextBoxImage" CalendarLocation="Left" ShowGoToToday="True" AllowArbitraryText="False"
														Culture="(Default)" UpperBoundDate="12/31/9999 23:59:00" PadSingleDigits="True" ToolTip="Call Back Date"
														ShowClearDate="True">
														<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
														<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></WeekdayStyle>
														<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="Yellow"></MonthHeaderStyle>
														<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
															BackColor="AntiqueWhite"></OffMonthStyle>
														<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></GoToTodayStyle>
														<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="LightGoldenrodYellow"></TodayDayStyle>
														<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="Orange"></DayHeaderStyle>
														<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="LightGray"></WeekendStyle>
														<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="Yellow"></SelectedDateStyle>
														<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></ClearDateStyle>
														<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></HolidayStyle>
													</ew:calendarpopup></TD>
												<TD style="HEIGHT: 13px" width="200"><asp:dropdownlist id="ddlEmployee" runat="server" Width="78px" CssClass="clsinputcombo"></asp:dropdownlist></TD>
												<TD style="HEIGHT: 13px" width="200"><asp:dropdownlist id="DdlCity" runat="server" Width="76px" CssClass="clsinputcombo">
														<asp:ListItem Value="0">Houston</asp:ListItem>
														<asp:ListItem Value="1">Dallas</asp:ListItem>
													</asp:dropdownlist></TD>
												<TD style="HEIGHT: 13px" width="200"><asp:dropdownlist id="ddl_rpttype" runat="server" CssClass="clsinputcombo">
														<asp:ListItem Value="0">Sales Report</asp:ListItem>
														<asp:ListItem Value="1">Retention Report</asp:ListItem>
													</asp:dropdownlist></TD>
												<TD style="HEIGHT: 13px" align="left" width="80" height="13"><asp:button id="btn_submit" runat="server" CssClass="clsbutton" Text="Submit"></asp:button></TD>
												<td style="HEIGHT: 13px" align="right" width="10%" height="13">&nbsp;&nbsp;
												</td>
											</tr>
											<tr>
												<td width="780" background="../../images/separator_repeat.gif" colSpan="6" style="height: 6"></td>
												<TD width="780" background="../../images/separator_repeat.gif" style="height: 11px"></TD>
											</tr>
										</TABLE>
										<asp:label id="lbl_message" runat="server" CssClass="label" ForeColor="Red"></asp:label></TD>
								</TR>
								<TR>
									<TD>
										<TABLE id="TableGrid" cellSpacing="0" cellPadding="0" width="100%" bgColor="white" border="0">
											<tr>
												<td vAlign="top" align="center" colSpan="2"><asp:datagrid id="dg_valrep" runat="server" Width="100%" CssClass="clsLeftPaddingTable" PageSize="20"
														BackColor="#EFF4FB" AutoGenerateColumns="False" BorderColor="White" BorderStyle="None">
														<Columns>
															<asp:TemplateColumn HeaderText="Employee">
																<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
																<ItemStyle VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id=lbl_abbrev runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.abbreviation") %>'>
																	</asp:Label>
																</ItemTemplate>
																<FooterTemplate>
																	<asp:Label id="lbl" runat="server">Total</asp:Label>
																</FooterTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Total Clicks">
																<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
																<ItemStyle VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id=lbl_clickscount runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.clickscount") %>'>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Total Quotes">
																<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
																<ItemStyle VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id=lbl_totalquote runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.totalquote") %>'>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Total Clients">
																<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
																<ItemStyle VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id=lbl_totalclient runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.totalClient") %>'>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Total Calls">
																<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
																<ItemTemplate>
																	<asp:Label id="lbl_totalcalls" runat="server" CssClass="label"></asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Retention">
																<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
																<ItemTemplate>
																	<asp:Label id=lbl_retention runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.totalfee") %>'>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Rev Generated">
																<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
																<ItemStyle VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id=lbl_totalfee runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.totalfee", "{0:C2}") %>'>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Potential Rev">
																<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
																<ItemTemplate>
																	<asp:Label id=lbl_potrev runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.potrev","{0:C2}") %>'>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
														</Columns>
														<PagerStyle VerticalAlign="Middle" NextPageText="   Next &gt;" PrevPageText="  &lt; Previous        "
															HorizontalAlign="Center"></PagerStyle>
													</asp:datagrid></td>
											</tr>
											<tr>
												<td width="780" background="../../images/separator_repeat.gif"  height="11"></td>
											</tr>
											<TR>
												<TD ><uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
							</TABLE>
						</TD>
					</TR>
				</tbody>
			</TABLE>
		</form>
	</body>
</HTML>

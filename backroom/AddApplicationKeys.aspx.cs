using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.backroom
{
    public partial class AddApplicationKeys : System.Web.UI.Page
    {
        
        ProjectSettings ps = new ProjectSettings();
        clsLogger clog = new clsLogger();

        string concatinate=String .Empty;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["AddKeys"] = null;
            }
        }

        protected void btn_Submit_Click(object sender, EventArgs e)
        {

            try
            {
                ps.Key = tbKey.Text;
                concatinate = concatinate.Insert(concatinate.Length, "<Name>") + tbName.Text + concatinate.Insert(concatinate.Length, "</Name>") + concatinate.Insert(concatinate.Length, "<Value>") + tbValue.Text + concatinate.Insert(concatinate.Length, "</Value>");
                Session["AddKeys"] += concatinate;
                ps.Value = Convert.ToString(Session["AddKeys"]);
                ps.AddKeys();
                lblMessage .Text ="Key has been Sucessfully Added";
                
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
    }
}

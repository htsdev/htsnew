﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CriminalPaymentReport.aspx.cs"
    Inherits="lntechNew.backroom.CriminalPaymentReport" %>

<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>

    <script src="../Scripts/Dates.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">		
			
        function show1(divid)
        {
        //a;
	       divid.style.display = 'block';
        }
        
        function hide(divid)
        {
            divid.style.display = 'none';	       
        }
	
    </script>

    <style type="text/css">
        TABLE
        {
            font: 11px verdana,arial,helvetica,sans-serif;
        }
        TD
        {
            font: 10pt Tahoma;
        }
        TD
        {
            font: 10pt Tahoma;
        }
        body, td, th
        {
            color: #000000;
        }
        A:link
        {
            font-size: 8pt;
            font-family: Verdana,Arial, Helvetica, sans-serif;
            color: 3366cc;
            text-decoration: none;
            font-weight: bold;
        }
        A
        {
            font-size: 8pt;
            font-family: Verdana,Arial, Helvetica, sans-serif;
            color: 3366cc;
            text-decoration: none;
            font-weight: bold;
        }
        .clsmainhead
        {
            text-decoration: none;
            font-weight: bold;
            font-size: 10pt;
            font-family: Tahoma /* COLOR: #ffffff; */;
        }
        .clsmainhead
        {
            text-decoration: none;
            font-weight: bold;
            font-size: 10pt;
            font-family: Tahoma /* COLOR: #ffffff; */;
        }
        .style1
        {
            width: 342px;
        }
        </style>
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <div>
        <table id="tblMain" cellspacing="0" cellpadding="0" width="780" align="center" border="0"
            runat="server">
            <tr>
                <td>
                    <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td class="TDHeading" colspan="5" >
                    <div id="HC1">
                        &nbsp;<span onclick="show1(HC2);hide(HC1);">+</span>&nbsp;&nbsp;<asp:TextBox ID="Textbox5" 
                            runat="server" CssClass="ProductHead" ReadOnly="True"> Attorney Detail</asp:TextBox></div>
                    <div class="divHidden" id="HC2">
                        &nbsp;<span onclick="show1(HC1);hide(HC2);">-</span>&nbsp;&nbsp;<asp:TextBox ID="Textbox6"
                            runat="server" CssClass="ProductHead" ReadOnly="True"> Attorney Detail</asp:TextBox></div>
                </td>
            </tr>
            <tr>
                <td colspan="5" height="7">
                </td>
            </tr>
            <tr>
                <td width="100%" colspan="5">
                    <div class="divHidden" id="HC3">
                        <table id="Table61" cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tr>
                                <td valign="top" align="left">
                                    <table id="Table21" cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tr>
                                            <td height="20">
                                                <asp:DataGrid ID="dgrdCourt" runat="server" Width="100%" Font-Names="Verdana" Font-Size="2px"
                                                    border="0" AutoGenerateColumns="False" ShowFooter="True" CellPadding="0" CellSpacing="0">
                                                    <AlternatingItemStyle BackColor="#EEEEEE"></AlternatingItemStyle>
                                                    <HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle">
                                                    </HeaderStyle>
                                                    <FooterStyle CssClass="GrdFooter"></FooterStyle>
                                                    <Columns>
                                                        <asp:TemplateColumn>
                                                            <HeaderTemplate>
                                                                <table style="font-size: 2px; width: 100%; font-family: Verdana; border-collapse: collapse"
                                                                    cellspacing="0" rules="cols" border="1">
                                                                    <tr>
                                                                        <td class="GrdHeader" align="left" width="60%">
                                                                            Court Location
                                                                        </td>
                                                                        <td class="GrdHeader" align="center" width="15%">
                                                                            Transactions
                                                                        </td>
                                                                        <td class="GrdHeader" align="center" width="20%">
                                                                            Fee Amount&nbsp;&nbsp;
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <table style="font-size: 2px; width: 100%; font-family: Verdana; border-collapse: collapse"
                                                                    bordercolor="gainsboro" cellspacing="0" cellpadding="0" rules="all" border="1">
                                                                    <tr id="trDetail" runat="server">
                                                                        <td valign="top" align="left" width="60%">
                                                                            <table style="border-top-style: none; border-right-style: none; border-left-style: none;
                                                                                border-bottom-style: none" cellspacing="0" cellpadding="0" width="416" border="0">
                                                                                <tr>
                                                                                    <td id="iParent" style="display: none" align="left" width="10" runat="server">
                                                                                        
                                                                                    </td>
                                                                                    <td id="iLink" style="display: none" align="left" width="10" runat="server">
                                                                                        
                                                                                    </td>
                                                                                    <td id="iChild" style="display: none" align="left" width="10" runat="server">
                                                                                        
                                                                                    </td>
                                                                                    <td class="style1">
                                                                                        &nbsp;<asp:Label ID="lblCourtID" CssClass="GrdLbl" runat="server" Visible="False"
                                                                                            Text='<%# DataBinder.Eval(Container, "DataItem.CurrentCourtloc") %>'>
                                                                                        </asp:Label>
                                                                                        <asp:Label ID="lblIsCourtCategory" CssClass="GrdLbl" runat="server" Style="display: none"
                                                                                            Text='<%# DataBinder.Eval(Container, "DataItem.IsCourtCategory") %>'>
                                                                                        </asp:Label>
                                                                                        <asp:Label ID="lblCategoryNumber" CssClass="GrdLbl" runat="server" Style="display: none"
                                                                                            Text='<%# DataBinder.Eval(Container, "DataItem.CategoryNumber") %>'>
                                                                                        </asp:Label>
                                                                                        <asp:Label ID="lblCourtName" CssClass="GrdLbl" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.CourtName") %>'>
                                                                                        </asp:Label>
                                                                                        <asp:LinkButton ID="lnkbtnCourt" runat="server" ForeColor="black" CssClass="ProductGrdLinks"
                                                                                            CommandName="DoGetCourt">
																		<%# DataBinder.Eval(Container, "DataItem.CourtName") %>
                                                                                        </asp:LinkButton>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                        <td align="center" width="15%">
                                                                            <asp:Label ID="lblTrans" CssClass="GrdLbl" runat="server" Visible="True" Text='<%# DataBinder.Eval(Container, "DataItem.Trans") %>'>
                                                                            </asp:Label>
                                                                        </td>
                                                                        <td align="right" width="20%">
                                                                            <asp:Label ID="lblFee" CssClass="GrdLbl" runat="server" Visible="true" Text='<%# DataBinder.Eval(Container, "DataItem.Amount") %>'>
                                                                            </asp:Label>&nbsp;&nbsp;&nbsp;
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <table style="font-size: 2px; width: 100%; font-family: Verdana; border-collapse: collapse"
                                                                    bordercolor="silver" cellspacing="0" rules="cols" border="1">
                                                                    <tr>
                                                                        <td align="left" width="60%">
                                                                            <asp:Label ID="Label1" runat="server" CssClass="grdheader">Total</asp:Label>
                                                                        </td>
                                                                        <td align="center" width="15%">
                                                                            <asp:Label ID="lbl_Count" runat="server" CssClass="GrdLbl"></asp:Label>
                                                                        </td>
                                                                        <td align="right" width="20%">
                                                                            <asp:Label ID="lbl_Amount" runat="server" CssClass="GrdLbl"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </FooterTemplate>
                                                        </asp:TemplateColumn>
                                                    </Columns>
                                                    <PagerStyle NextPageText="" PrevPageText="" HorizontalAlign="Center" Mode="NumericPages">
                                                    </PagerStyle>
                                                </asp:DataGrid>
                                                <br />
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td height="20">
                                                <asp:TextBox
                        ID="Textbox7" runat="server" CssClass="ProductHead" ReadOnly="True" Height="25px" Width="954px"> Court Summary</asp:TextBox></td>
                                        </tr>
                                        
                                        <tr>
                            <td>
                                <asp:DataGrid ID="dgCategoryType" runat="server" Width="100%" Font-Names="Verdana"
                                    Font-Size="2px" AutoGenerateColumns="False" ShowFooter="True">
                                    <AlternatingItemStyle BackColor="#EEEEEE"></AlternatingItemStyle>
                                    <HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle">
                                    </HeaderStyle>
                                    <FooterStyle CssClass="GrdFooter"></FooterStyle>
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="Category Type">
                                            <HeaderStyle HorizontalAlign="Left" Width="65%" CssClass="GrdHeader"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="Label2" runat="server" Width="20px" CssClass="ProductGrdLinks" Visible="False"></asp:Label>
                                                <asp:Label ID="lblCategoryType" runat="server" CssClass="ProductGrdLinks" Visible="True"
                                                    Text='<%# DataBinder.Eval(Container, "DataItem.CategoryType") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="Label3" runat="server" CssClass="grdheader">Total</asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Count">
                                            <HeaderStyle HorizontalAlign="Center" Width="15%" CssClass="GrdHeader"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblCount2" runat="server" CssClass="GrdLbl" Visible="True" Text='<%# DataBinder.Eval(Container, "DataItem.TotalCount") %>'>
                                                </asp:Label>
                                                <asp:Label ID="lblDisplayCol" runat="server" CssClass="GrdLbl" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.displaycol") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterStyle HorizontalAlign="Center"></FooterStyle>
                                            <FooterTemplate>
                                                <asp:Label ID="lbl_SumCount" runat="server" CssClass="GrdLbl"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Amount">
                                            <HeaderStyle HorizontalAlign="Center" Width="20%" CssClass="GrdHeader"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblAmount2" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.TotalAmount", "{0:C}") %>'>
                                                </asp:Label>&nbsp;&nbsp;&nbsp;
                                            </ItemTemplate>
                                            <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                            <FooterTemplate>
                                                <asp:Label ID="lbl_SumAmount" runat="server" CssClass="GrdLbl"></asp:Label>&nbsp;&nbsp;&nbsp;
                                            </FooterTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                    <PagerStyle NextPageText="" PrevPageText="" HorizontalAlign="Center" Mode="NumericPages">
                                    </PagerStyle>
                                </asp:DataGrid>
                            &nbsp;&nbsp;
                            </td>
                        </tr>
                        
                                        </tr>
                                        
                                         <tr>
                                        
                            <td align="center">
                            
                                <asp:DropDownList ID="DropDownList2" runat="server">
                                    <asp:ListItem>All Reps</asp:ListItem>
                                </asp:DropDownList>
                                <asp:DropDownList ID="DropDownList3" runat="server" Width="170">
                                    <asp:ListItem>Show all</asp:ListItem>
                                </asp:DropDownList>
                                <asp:DropDownList ID="DropDownList4" runat="server">
                                    <asp:ListItem Value="All Courts">All Courts </asp:ListItem>
                                </asp:DropDownList>
                                <br />
                                <asp:TextBox ID="Textbox1" runat="server" CssClass="ProductHead" 
                                    ReadOnly="True" Width="950px">Transaction Details</asp:TextBox>
                            <tr>
                                <asp:DataGrid ID="dgrdPayDetail" runat="server" Width="100%" Font-Names="Verdana"
                                    Font-Size="2px" AutoGenerateColumns="False" 
                                    AllowCustomPaging="True" PageSize="100" AllowSorting="True">
                                    <AlternatingItemStyle BackColor="#EEEEEE"></AlternatingItemStyle>
                                    <HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle">
                                    </HeaderStyle>
                                    <FooterStyle CssClass="GrdFooter"></FooterStyle>
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="No.">
                                            <HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                Font-Strikeout="False" Font-Underline="False" Wrap="False"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblTicketID" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.TicketID") %>'
                                                    Visible="false">
                                                </asp:Label>
                                                <asp:Label ID="lblCardTypeID" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.CardType") %>'
                                                    Visible="false">
                                                </asp:Label>
                                                <asp:Label ID="lblNo" runat="server" CssClass="GrdLbl" Text="" Visible="True"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Date">
                                            <HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                Font-Strikeout="False" Font-Underline="False" Wrap="False"></ItemStyle>
                                            <ItemTemplate>
                                                &nbsp;<asp:Label ID="lblDate" runat="server" CssClass="GrdLbl" Visible="true" Text='<%# DataBinder.Eval(Container, "DataItem.RecDate") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Time" SortExpression="sorttime">
                                            <HeaderStyle HorizontalAlign="Center" CssClass="TaskGrid_Head"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Right" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                Font-Strikeout="False" Font-Underline="False" Wrap="False"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblTime" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.RecTime") %>'
                                                    Visible="true">
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn SortExpression="CustomerName" HeaderText="Client Name">
                                            <HeaderStyle HorizontalAlign="Left" CssClass="TaskGrid_Head"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblCustomer" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.CustomerName") %>'
                                                    Visible="false">
                                                </asp:Label>
                                                <asp:LinkButton ID="lnkCustomer" runat="server" CssClass="ProductGrdLinks" ForeColor="black"
                                                    CommandName="DoGetCustomer">
															<%# DataBinder.Eval(Container, "DataItem.CustomerName") %>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                Font-Underline="False" Wrap="False" />
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn SortExpression="Lastname" HeaderText="Rep">
                                            <HeaderStyle HorizontalAlign="Left" CssClass="TaskGrid_Head"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblRep" runat="server" CssClass="GrdLbl" Visible="true" Text='<%# DataBinder.Eval(Container, "DataItem.Lastname") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                Font-Underline="False" Wrap="False" />
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn SortExpression="bondflag" HeaderText="Bond">
                                            <HeaderStyle HorizontalAlign="Left" CssClass="TaskGrid_Head"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblBond" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.BondFlag") %>'
                                                    Visible="true">
                                                </asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                Font-Underline="False" Wrap="False" />
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn SortExpression="ChargeAmount" HeaderText="Paid">
                                            <HeaderStyle HorizontalAlign="Left" CssClass="TaskGrid_Head"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblPaidAmount" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.ChargeAmount", "{0:C0}") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                Font-Underline="False" Wrap="False" />
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Adj1">
                                            <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="Label4" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.adj1") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                Font-Underline="False" Wrap="False" />
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Adj2">
                                            <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="Label5" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.adj2") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                Font-Underline="False" Wrap="False" />
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn SortExpression="PaymentType" HeaderText="Payment Type">
                                            <HeaderStyle HorizontalAlign="Left" CssClass="TaskGrid_Head"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblPayTypeFR" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.CCType") %>'
                                                    Visible="true">
                                                </asp:Label>
                                                <asp:Label ID="lblPTypeId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentType") %>'
                                                    Visible="False">
                                                </asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                Font-Underline="False" Wrap="False" />
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Card">
                                            <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblCardType" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.ChargeAmount") %>'
                                                    Visible="true">
                                                </asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                Font-Underline="False" Wrap="False" />
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Court">
                                            <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblCourt" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.Court") %>'
                                                    Visible="true">
                                                </asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                Font-Underline="False" Wrap="False" />
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="List">
                                            <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblListDate" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.ListDate") %>'
                                                    Visible="true">
                                                </asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                Font-Underline="False" Wrap="False" />
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Letter">
                                            <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_MailType" Text='<%# DataBinder.Eval(Container, "DataItem.MailType") %>'
                                                    runat="server" CssClass="GrdLbl" Visible="true">
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Date">
                                            <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_MailDate" Text='<%# DataBinder.Eval(Container, "DataItem.MailDate") %>'
                                                    runat="server" CssClass="GrdLbl" Visible="true">
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                    <PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Center">
                                    </PagerStyle>
                                </asp:DataGrid>
                                </tr>
                            </td>
                        </tr>
                                        
                                        <tr>
                <td height="7">
                </td>
                                        </tr>
                                        <tr>
                <td width="100%">
                    &nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td height="20">
                                                &nbsp;</td>
                                        </tr>
                                        </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

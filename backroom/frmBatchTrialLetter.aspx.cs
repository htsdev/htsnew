using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;

namespace lntechNew.backroom
{
    public partial class frmBatchTrialLetter : System.Web.UI.Page
    {
        clsLogger clog = new clsLogger();
        Components.TrialLetter trial = new lntechNew.Components.TrialLetter();

        DataView DV;
        string StrAcsDec = String.Empty;
        string StrExp = String.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    FillGrid();
                }
            }
            catch (Exception ex)
            {
                lbl_Msg.Text = ex.Message;
                lbl_Msg.Visible = true;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }


        private void FillGrid()
        {
            try
            {
                trial.StBatchDate = cal_todate.SelectedDate;
                trial.EndBatchDate = cal_fromDate.SelectedDate;

                DataSet ds = trial.GetTrialLetterBatch();

                if (ds != null)
                {
                    DG_TrialLetterBatch.DataSource = ds;
                    DV = new DataView(ds.Tables[0]);//data view paging      
                    Session["sDV"] = DV;
                    DG_TrialLetterBatch.DataBind();

                    FillPageList();//fill dropdown with page numbers    
                    SetNavigation();//navigation through page numbers
                }
            }
            catch (Exception ex)
            {
                lbl_Msg.Text = ex.Message;
                lbl_Msg.Visible = true;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                FillGrid();
            }
            catch (Exception ex)
            {
                lbl_Msg.Text = ex.Message;
                lbl_Msg.Visible = true;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void GenrateSerialNo(DataSet ds)
        {
            try
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {

                }
            }
            catch (Exception ex)
            {
                lbl_Msg.Text = ex.Message;
                lbl_Msg.Visible = true;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        protected void DG_TrialLetterBatch_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            try
            {
                BindReport();

                DataRowView drv = (DataRowView)e.Item.DataItem;
                if (drv == null)
                    return;

                if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
                {
                    ((HyperLink)e.Item.FindControl("hp_sno")).NavigateUrl = "../ClientInfo/violationfeeold.aspx?search=0&casenumber=" + (int)drv["ticketid_pk"];
                }
            }
            catch (Exception ex)
            {
                lbl_Msg.Text = ex.Message;
                lbl_Msg.Visible = true;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void BindReport()
        {

            try
            {
                long sNo = (DG_TrialLetterBatch.CurrentPageIndex) * (DG_TrialLetterBatch.PageSize);

                foreach (DataGridItem ItemX in DG_TrialLetterBatch.Items)
                {
                    sNo += 1;

                    ((HyperLink)(ItemX.FindControl("hp_sno"))).Text = sNo.ToString();
                }
            }
            catch (Exception ex)
            {
                lbl_Msg.Text = ex.Message;
                lbl_Msg.Visible = true;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void DG_TrialLetterBatch_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            try
            {
                DG_TrialLetterBatch.CurrentPageIndex = e.NewPageIndex;
                SetNavigation();
                DV = (DataView)Session["sDV"];
                DG_TrialLetterBatch.DataSource = DV;
                DG_TrialLetterBatch.DataBind();
            }
            catch (Exception ex)
            {
                lbl_Msg.Text = ex.Message;
                lbl_Msg.Visible = true;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        //Method for filling pagenumber in dropdown.
        private void FillPageList()
        {
            Int16 idx;

            cmbPageNo.Items.Clear();
            for (idx = 1; idx <= DG_TrialLetterBatch.PageCount; idx++)
            {
                cmbPageNo.Items.Add("Page - " + idx.ToString());
                cmbPageNo.Items[idx - 1].Value = idx.ToString();
            }
        }

        private void SetNavigation()
        {
            try
            {
                // setting current page number
                lblPNo.Text = Convert.ToString(DG_TrialLetterBatch.CurrentPageIndex + 1);
                lblCurrPage.Visible = true;
                lblPNo.Visible = true;

                // filling combo with page numbers
                lblGoto.Visible = true;
                cmbPageNo.Visible = true;
                //FillPageList();

                cmbPageNo.SelectedIndex = DG_TrialLetterBatch.CurrentPageIndex;
            }
            catch (Exception ex)
            {
                lbl_Msg.Text = ex.Message;
                lbl_Msg.Visible = true;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void cmbPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                lbl_Msg.Text = "";
                DG_TrialLetterBatch.CurrentPageIndex = cmbPageNo.SelectedIndex;

                DV = (DataView)Session["sDV"];
                DG_TrialLetterBatch.DataSource = DV;
                DG_TrialLetterBatch.DataBind();
                SetNavigation();
            }
            catch (Exception ex)
            {
                lbl_Msg.Text = ex.Message;
                lbl_Msg.Visible = true;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void SetAcsDesc(string Val)
        {
            try
            {
                StrExp = Session["StrExp"].ToString();
                StrAcsDec = Session["StrAcsDec"].ToString();
            }
            catch
            {

            }

            if (StrExp == Val)
            {
                if (StrAcsDec == "ASC")
                {
                    StrAcsDec = "DESC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
                else
                {
                    StrAcsDec = "ASC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
            }
            else
            {
                StrExp = Val;
                StrAcsDec = "ASC";
                Session["StrExp"] = StrExp;
                Session["StrAcsDec"] = StrAcsDec;
            }
        }

        private void SortGrid(string SortExp)
        {
            try
            {
                SetAcsDesc(SortExp);
                DV = (DataView)Session["sDV"];
                DV.Sort = StrExp + " " + StrAcsDec;
                DG_TrialLetterBatch.DataSource = DV;
                DG_TrialLetterBatch.DataBind();
            }
            catch (Exception ex)
            {
                lbl_Msg.Text = ex.Message;
                lbl_Msg.Visible = true;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void DG_TrialLetterBatch_SortCommand(object source, DataGridSortCommandEventArgs e)
        {
            try
            {
                SortGrid(e.SortExpression);
            }
            catch (Exception ex)
            {
                lbl_Msg.Text = ex.Message;
                lbl_Msg.Visible = true;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }


    }
}

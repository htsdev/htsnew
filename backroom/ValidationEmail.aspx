﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ValidationEmail.aspx.cs"
    Inherits="HTP.backroom.ValidationEmail" %>

<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ValidationEmail</title>

    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <meta http-equiv="X-UA-Compatible" content="IE=5; IE=8; IE=7.5;" />
    <meta http-equiv="X-UA-Compatible" content="chrome=1">

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>


    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <%--<link href="../Styles.css" type="text/css" rel="stylesheet">--%>

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->

    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->



    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>

    <script type="text/javascript">
    
        
        function ValidationEmail()
		{		
		var email = form1.tb_emailaddress;		
		if (  email.value != "")
		{
		    if( isEmail(email.value)== false)
			{
			    alert ("Please enter Email Address in Correct format.");
			    email.focus(); 
			    return false;			   
			}		
		}		
		
		 var doyou = confirm("Are you sure you want to run Validation Email Report (OK = Yes   Cancel = No)"); 
         if (doyou == true)
            {return true;}         
         return false;			         
		}
		
		
		function showbobbittAddress()
		{		
		    document.form1.tb_emailaddress.value='<%= ViewState["ValidationEmailOthers"] %>';
		}
		
		
		function ShowWait()
		{		
		    document.getElementById('tr_PleaesWait').style.display = "none";    //Saeed 6255 07/14/2010 hide please wait message.
		    document.getElementById("tdWait").style.display = ""
		    document.getElementById("tdcontrols").style.display = "none"
		    return true;
		}
		//Saeed 6255 07/14/2010 method use to display 'please wait..' message when click on Send email button.
		function ShowProggress()
		{
		    document.getElementById('tr_PleaesWait').style.display = "";
		    return true;
		}
		
		
    </script>

    <%--<link href="../Styles.css" rel="stylesheet" type="text/css" />--%>
</head>

<body class=" ">

    <form id="form1" runat="server">
        <aspnew:ScriptManager ID="ScriptManager1" AsyncPostBackTimeout="0" runat="server">
            <Scripts>
                <aspnew:ScriptReference Name="MicrosoftAjax.js"  Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
            </Scripts>
        </aspnew:ScriptManager>
    
        <!-- START CONTAINER -->
        <div class="page-container row-fluid container-fluid">

            <asp:Panel ID="pnl" runat="server">
                <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
            </asp:Panel>
        
            <!-- START CONTENT -->
            <section id="main-content" class=" ">
                <section class="wrapper main-wrapper row" style=''>

                    <div class='col-xs-12'>
                        <div class="page-title">

                            <div class="pull-left">
                                <!-- PAGE HEADING TAG - START -->
                                <h1 class="title">Validation Report</h1>
                                <!-- PAGE HEADING TAG - END -->
                            </div>
                        
                        </div>
                    </div>
                    
                    <div class="clearfix"></div>
                    
                    <!-- MAIN CONTENT AREA STARTS -->

                    <aspnew:UpdatePanel ID="upValidationEmail" runat="server" RenderMode="Inline">
                        <ContentTemplate>
    
                            <div class="col-xs-12">
                                <section class="box ">
                                    <header class="panel_header">
                                        <div class="actions panel_actions pull-right">
                	                        <asp:CheckBox ID="chkDetail" Text="Detail" runat="server" />
                                        </div>
                                    </header>
                                    <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upValidationEmail">
                                        <ProgressTemplate>
                                            <img src="../images/plzwait.gif" alt="" />
                                            <asp:Label ID="lblWait" runat="server" Text="Please wait... while system process the request." CssClass="form-label"></asp:Label>
                                        </ProgressTemplate>
                                    </aspnew:UpdateProgress>
                                    <asp:Label ID="lbl_message" runat="server" ForeColor="Red"></asp:Label>
                                    <div class="content-body">

                                        <div class="row">

                                            <div class="col-md-3 col-sm-4 col-xs-5">

                                                <div class="form-group">
                                                    <div class="controls">
                                                        <asp:RadioButton ID="rb_demail" runat="server" Checked="True" GroupName="Email" CssClass="form-label" Text="sales@abclawfirm.com" />
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="col-md-3 col-sm-4 col-xs-5">

                                                <div class="form-group">
                                                    <div class="controls">
                                                        <asp:RadioButton ID="rb_other" runat="server" GroupName="Email" Text="Others" CssClass="form-label" onclick="showbobbittAddress()" />
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="col-md-6 col-sm-7 col-xs-8">

                                                <div class="form-group">
                                                    <div class="controls">
                                                        <asp:TextBox ID="tb_emailaddress" runat="server" CssClass="form-control"></asp:TextBox>
                                                        <asp:Button ID="btn_email" runat="server" CssClass="btn btn-primary" OnClientClick="return ShowProggress();" OnClick="LinkButton1_Click" Text="Send Email" />
                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="form-group">
                                                    <div class="controls">
                                                        <asp:Button ID="btn_displayreport" runat="server" CssClass="btn btn-primary" OnClick="btn_displayreport_Click"
                                                            Text="Generate Validation  Report" OnClientClick="return ShowWait();" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="form-group">
                                                    <asp:Label ID="lbl_result" runat="server" ForeColor="Red" CssClass="form-label" ></asp:Label>
                                                </div>
                                            </div>
                                        </div>

                                        <table>
                                            <tr>
                                                <td align="center" id="tdWait" runat="server" style="display: none">                                
                                                    Please wait...<br /><br />
                                                    <img id="imgProgressbar" runat="server" src="../Images/Progressbar.gif" alt="Please wait.."  />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" id="tdReport" runat="server" style="display:inline">
                                                    <asp:Panel ID="pnl_report" runat="server" Height="600px" ScrollBars="Both" Visible="False"
                                                        Width="100%" Wrap="False">
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>

                                    </div>
                                </section>
                            </div>

                        </ContentTemplate>
                    </aspnew:UpdatePanel>
                    
                    <!-- MAIN CONTENT AREA ENDS -->

                </section>
            </section>
            <!-- END CONTENT -->

        </div>
        <!-- END CONTAINER -->
    </form>

    <!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->


    <!-- CORE JS FRAMEWORK - START --> 
    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script> 
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>  
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>  
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE JS FRAMEWORK - END --> 


    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 

    <script src="../assets/plugins/autosize/autosize.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>
    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


    <!-- CORE TEMPLATE JS - START --> 
    <script src="../assets/js/scripts.js" type="text/javascript"></script> 
    <!-- END CORE TEMPLATE JS - END --> 


</body>
























<%--<body>
    <form id="form1" runat="server">
    <aspnew:ScriptManager ID="ScriptManager1" AsyncPostBackTimeout="0" runat="server">
            <Scripts>
                <aspnew:ScriptReference Name="MicrosoftAjax.js"  Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
            </Scripts>
        </aspnew:ScriptManager>
    <div>
        <table id="TableMain" align="center" border="0" cellpadding="0" cellspacing="0" style="z-index: 101"
            width="780">
            <tbody>
                <tr>
                    <td colspan="4" style="height: 14px">
                        <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                    <aspnew:UpdatePanel ID="upValidationEmail" runat="server" RenderMode="Inline">
                    <ContentTemplate>
                                            
                        <table id="TableSub" border="0" cellpadding="0" cellspacing="0" width="100%">
                        
                            <tr>
                                <td style="background:url(../images/separator_repeat.gif) repeat;"  colspan="7" height="11">                                
                                </td>
                            </tr>
                            <tr>
                                <td style="background:url(../Images/subhead_bg.gif) repeat;"    height="34" class="clssubhead" align="right">
                                    <asp:CheckBox ID="chkDetail" Text="Detail" runat="server" />
                                    &nbsp;&nbsp;
                                </td>
                            </tr>
                            <tr id="tr_PleaesWait" style="display:none" >
                        <td align="center">
                        <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upValidationEmail">
                                <ProgressTemplate>
                                    <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lblWait" runat="server" Text="Please wait... while system process the request."
                                        CssClass="clsLabel"></asp:Label>
                                </ProgressTemplate>
                       </aspnew:UpdateProgress>
                       </td></tr>
                            <tr>
                                <td align="center">
                                    <asp:Label ID="lbl_message" runat="server" ForeColor="Red"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" id="tdcontrols" runat="server">
                                    <table style="width: 100%;">
                                        <tr>                                        
                                            <td align="left" class="clslabel" style="width: 72%">
                                                <asp:RadioButton ID="rb_demail" runat="server" Checked="True" GroupName="Email" Text="sales@abclawfirm.com" />&nbsp;
                                                <asp:RadioButton ID="rb_other" runat="server" GroupName="Email" Text="Others" onclick="showbobbittAddress()" />
                                                <asp:TextBox ID="tb_emailaddress" runat="server" CssClass="clsInputadministration"
                                                    Width="250px"></asp:TextBox>
                                                <asp:Button ID="btn_email" runat="server" CssClass="clsbutton" OnClientClick="return ShowProggress();" OnClick="LinkButton1_Click"
                                                    Text="Send Email" />
                                            </td>
                                            <td align="left" style="width: 28%" valign="middle">
                                                <asp:Button ID="btn_displayreport" runat="server" CssClass="clsbutton" OnClick="btn_displayreport_Click"
                                                    Text="Generate Validation  Report" OnClientClick="return ShowWait();" Width="205px" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" style="background:url(../images/separator_repeat.gif) repeat;" style="height: 12px">
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Label ID="lbl_result" runat="server" ForeColor="Red"></asp:Label>
                                </td>
                            </tr>
                            
                            <tr>
                                <td align="center" class="clssubhead" id="tdWait" runat="server" style="display: none">                                
                                    Please wait...<br /><br />
                                    <img id="imgProgressbar" runat="server" src="../Images/Progressbar.gif" alt="Please wait.."  />
                                </td>
                            </tr>
                            <tr>
                                <td align="center" id="tdReport" runat="server" style="display:inline">
                                    <asp:Panel ID="pnl_report" runat="server" Height="600px" ScrollBars="Both" Visible="False"
                                        Width="100%" Wrap="False">
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table id="TableGrid" bgcolor="white" border="0" cellpadding="0" cellspacing="0"
                                        width="100%">
                                        <tr>
                                            <td background="../images/separator_repeat.gif" colspan="5" height="11" width="780">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="5">
                                                <uc1:Footer ID="Footer1" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>                    
                    </ContentTemplate>
                    </aspnew:UpdatePanel>                    
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    </form>
</body>--%>

</html>

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;
using HTP.Components.Services;
using Configuration.Helper;
using Configuration.Client;
using MetaBuilders.WebControls;
using Configuration.Common;

namespace HTP.backroom
{
    public partial class ConfigurationSettingReport : System.Web.UI.Page
    {
        // Fahad Muhammad Qureshi 7287 01/22/2010  grouping variables
        #region Variables
        clsSession ClsSession = new clsSession();
        clsLogger clog = new clsLogger();
        clsCase cCase = new clsCase();
        clsLogger bugTracker = new clsLogger();
        DataSet DS;
        ValidationReports reports = new ValidationReports();
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        //IConfigurationClient configClient;


        #endregion

        // Fahad Muhammad Qureshi 7287 01/22/2010   grouping Event Handlers
        #region Event Handler
        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {

                if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else
                {
                    //configClient = ConfigurationClientFactory.GetConfigurationClient();            
                    if (!IsPostBack)
                    {
                        FillComboBoxValues();
                        FillGrid();
                        gv_Records.Visible = false;
                    }
                    Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
                    Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
                    Pagingctrl.GridView = gv_Records;

                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }
        }

        protected void gv_Records_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    //LinkButton editbutton=((LinkButton)e.Row.FindControl("lnkEdit");//.CommandArgument).Text = e.Row.RowIndex.ToString();
                    ((LinkButton)e.Row.FindControl("lnkEdit")).CommandArgument = e.Row.RowIndex.ToString();
                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }
        }

        protected void gv_Records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_Records.PageIndex = e.NewPageIndex;
                FillGrid();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }
        }

        protected void gv_Records_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "btnEditclick")
                {

                    int RowID = Convert.ToInt32(e.CommandArgument);
                    //ddlprogramPop.SelectedValue = (((HiddenField)gv_Records.Rows[RowID].FindControl("hf_Program")).Value);
                    //ddlModulePop.SelectedValue = (((HiddenField)gv_Records.Rows[RowID].FindControl("hf_Module")).Value);
                    //ddlSubModulePop.SelectedValue = (((HiddenField)gv_Records.Rows[RowID].FindControl("hf_SubModule")).Value);
                    //ddlKeyPop.SelectedValue = (((HiddenField)gv_Records.Rows[RowID].FindControl("hf_Key")).Value);

                    //txtProgram.Text = (((Label)gv_Records.Rows[RowID].FindControl("lbl_Division")).Text);
                    //txtModule.Text = (((Label)gv_Records.Rows[RowID].FindControl("lbl_Module")).Text);
                    //txtSubModule.Text = (((Label)gv_Records.Rows[RowID].FindControl("lbl_SubModuleName")).Text);
                    //txtKey.Text = (((Label)gv_Records.Rows[RowID].FindControl("lbl_Key")).Text);

                    lblProgramType.Text = (((HiddenField)gv_Records.Rows[RowID].FindControl("hf_Program")).Value);
                    lblModuleType.Text = (((HiddenField)gv_Records.Rows[RowID].FindControl("hf_Module")).Value);
                    lblSubModuleType.Text = (((HiddenField)gv_Records.Rows[RowID].FindControl("hf_SubModule")).Value);
                    lblKey.Text = (((HiddenField)gv_Records.Rows[RowID].FindControl("hf_Key")).Value);

                    txtEditValue.Text = (((Label)gv_Records.Rows[RowID].FindControl("lbl_Value")).Text);
                    txtEditDesc.Text = (((Label)gv_Records.Rows[RowID].FindControl("lbl_Description")).Text);
                    mpeConfigEdit.Show();

                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }

        }

        void Pagingctrl_PageIndexChanged()
        {

            gv_Records.PageIndex = Pagingctrl.PageIndex - 1;
            FillGrid();

        }

        // Fahad Muhammad Qureshi 7287 01/22/2010  page size change event handler
        /// <summary>
        /// Page Size change event handler
        /// </summary>
        /// <param name="pageSize"></param>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            if (pageSize > 0)
            {
                gv_Records.PageIndex = 0;
                gv_Records.PageSize = pageSize;
                gv_Records.AllowPaging = true;
            }
            else
            {
                gv_Records.AllowPaging = false;
            }
            FillGrid();
            FillComboBoxValues();

        }

        // Fahad Muhammad Qureshi 7287 01/22/2010 Filter family report
        /// <summary>
        /// Event handler to filter report according to criteria
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnFamilySubmit_Click(object sender, EventArgs e)
        {
            try
            {

                gv_Records.PageIndex = 0;
                FillGrid();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }

        }



        #endregion

        // Fahad Muhammad Qureshi 7287 01/22/2010  grouping Methods
        #region Methods
        private void FillGrid()
        {
            // Fahad Muhammad Qureshi 7287 01/22/2010 Show Record according to report selection
            try
            {
                lblMessage.Text = string.Empty;
                gv_Records.Visible = true;
                //string[] keys = { "@DivisionID", "@ModuleID", "@SubModuleID", "@KeyID" };
                //object[] values = { Convert.ToInt32(ddlDivison.SelectedValue.ToString()), Convert.ToInt32(ddlModule.SelectedValue.ToString()), Convert.ToInt32(ddlSubModule.SelectedValue.ToString()), Convert.ToInt32(ddlKey.SelectedValue.ToString()) };
                //reports.getRecords("USP_HTP_Get_ConfigurationData", keys, values);
                //DS = reports.records;

                IConfigurationClient configClient;
                configClient = ConfigurationClientFactory.GetConfigurationClient();
                ConfigurationData configData = new ConfigurationData();

                if (ddlDivison.SelectedValue != "0")
                {
                    configData.Division = (Division)Enum.Parse(typeof(Division), ddlDivison.SelectedItem.ToString().ToUpper());
                }
                if (ddlModule.SelectedValue != "0")
                {
                    configData.Module = (Module)Enum.Parse(typeof(Module), ddlModule.SelectedItem.ToString());
                }
                if (ddlSubModule.SelectedValue != "0")
                {
                    configData.SubModule = (SubModule)Enum.Parse(typeof(SubModule), ddlSubModule.SelectedItem.ToString());
                }
                if (ddlKey.SelectedValue != "0")
                {
                    configData.Key = (Key)Enum.Parse(typeof(Key), ddlKey.SelectedItem.ToString());
                }
                DataTable dt = configClient.GetSelectedData(configData);


                if (dt.Rows.Count > 0)
                {
                    // Fahad Muhammad Qureshi 7287 01/22/2010 Set grivview page size 
                    dt.Columns.Add("sno");
                    //DataView dv = dt.DefaultView;
                    //DataTable dt = dv.ToTable();


                    // Fahad Muhammad Qureshi 7287 01/22/2010 GridView Name Changed in PaggingControl
                    Pagingctrl.GridView = gv_Records;
                    gv_Records.DataSource = dt;
                    gv_Records.DataBind();
                    Pagingctrl.PageCount = gv_Records.PageCount;
                    Pagingctrl.PageIndex = gv_Records.PageIndex;
                    Pagingctrl.SetPageIndex();
                }
                else
                {
                    // Fahad Muhammad Qureshi 7287 01/22/2010 Set grivview page size
                    Pagingctrl.PageCount = 0;
                    Pagingctrl.PageIndex = 0;
                    Pagingctrl.SetPageIndex();
                    lblMessage.Text = "No Records Found";
                    gv_Records.Visible = false;
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        private void FillDropDownList()
        {
            DataSet dsModule = ClsDb.Get_DS_BySP("USP_HTP_GET_AllModule");
            ddlModule.DataSource = dsModule;
            ddlModule.DataTextField = dsModule.Tables[0].Columns[1].ColumnName;
            ddlModule.DataValueField = dsModule.Tables[0].Columns[0].ColumnName;
            ddlModule.DataBind();

            ddlModulePop.DataSource = dsModule;
            ddlModulePop.DataTextField = dsModule.Tables[0].Columns[1].ColumnName;
            ddlModulePop.DataValueField = dsModule.Tables[0].Columns[0].ColumnName;
            ddlModulePop.DataBind();


            DataSet dsDivison = ClsDb.Get_DS_BySP("USP_HTP_GET_AllDivision");
            ddlDivison.DataSource = dsDivison;
            ddlDivison.DataTextField = dsDivison.Tables[0].Columns[1].ColumnName;
            ddlDivison.DataValueField = dsDivison.Tables[0].Columns[0].ColumnName;
            ddlDivison.DataBind();

            ddlprogramPop.DataSource = dsDivison;
            ddlprogramPop.DataTextField = dsDivison.Tables[0].Columns[1].ColumnName;
            ddlprogramPop.DataValueField = dsDivison.Tables[0].Columns[0].ColumnName;
            ddlprogramPop.DataBind();


            DataSet dsSubModule = ClsDb.Get_DS_BySP("USP_HTP_GET_AllSubModule");
            ddlSubModule.DataSource = dsSubModule;
            ddlSubModule.DataTextField = dsSubModule.Tables[0].Columns[1].ColumnName;
            ddlSubModule.DataValueField = dsSubModule.Tables[0].Columns[0].ColumnName;
            ddlSubModule.DataBind();

            ddlSubModulePop.DataSource = dsSubModule;
            ddlSubModulePop.DataTextField = dsSubModule.Tables[0].Columns[1].ColumnName;
            ddlSubModulePop.DataValueField = dsSubModule.Tables[0].Columns[0].ColumnName;
            ddlSubModulePop.DataBind();


            DataSet dsKey = ClsDb.Get_DS_BySP("USP_HTP_GET_AllKey");
            ddlKey.DataSource = dsKey;
            ddlKey.DataTextField = dsKey.Tables[0].Columns[1].ColumnName;
            ddlKey.DataValueField = dsKey.Tables[0].Columns[0].ColumnName;
            ddlKey.DataBind();

            ddlKeyPop.DataSource = dsKey;
            ddlKeyPop.DataTextField = dsKey.Tables[0].Columns[1].ColumnName;
            ddlKeyPop.DataValueField = dsKey.Tables[0].Columns[0].ColumnName;
            ddlKeyPop.DataBind();

            ddlDivison.Items.Insert(0, new ListItem("--Select--", "0"));
            ddlModule.Items.Insert(0, new ListItem("--Select--", "0"));
            ddlSubModule.Items.Insert(0, new ListItem("--Select--", "0"));
            ddlKey.Items.Insert(0, new ListItem("--Select--", "0"));

            ddlprogramPop.Items.Insert(0, new ListItem("--Select--", "0"));
            ddlModulePop.Items.Insert(0, new ListItem("--Select--", "0"));
            ddlSubModulePop.Items.Insert(0, new ListItem("--Select--", "0"));
            ddlKeyPop.Items.Insert(0, new ListItem("--Select--", "0"));

        }

        #endregion

        protected void ddlDivison_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                FillGrid();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }

        }

        protected void ddlModule_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                FillGrid();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }
        }

        protected void ddlSubModule_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                FillGrid();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }
        }

        protected void ddlKey_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                FillGrid();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            //ConfigurationService objconfigService = new ConfigurationService();
            //objconfigService.SetKeyValue((global::Configuration.Helper.Division)Convert.ToInt32(ddlDivison.SelectedValue), (global::Configuration.Helper.Module)Convert.ToInt32(ddlModule.SelectedValue), (global::Configuration.Helper.SubModule)Convert.ToInt32(ddlSubModule.SelectedValue), (global::Configuration.Helper.Key)Convert.ToInt32(ddlKey.SelectedValue), txtValue.Text.Trim(), txtDescription.Text.Trim());
            //objconfigService.SetKeyValue(
            //objconfigService.SetKeyValue((global::Configuration.Helper.Division)Convert.ToInt32(ddlDivison.SelectedValue.ToString()), ddlModule.SelectedItem.Text.ToString(), ddlSubModule.SelectedItem.Text.ToString(), ddlKey.SelectedItem.Text.ToString(), txtValue.Text.Trim(), txtDescription.Text.Trim());

            IConfigurationClient configClient;
            configClient = ConfigurationClientFactory.GetConfigurationClient();
            Division division = (Division)Enum.Parse(typeof(Division), ddlDivison.SelectedItem.ToString());
            Module module = (Module)Enum.Parse(typeof(Module), ddlModule.SelectedItem.ToString());
            SubModule subModule = (SubModule)Enum.Parse(typeof(SubModule), ddlSubModule.SelectedItem.ToString());
            Key key = (Key)Enum.Parse(typeof(Key), ddlKey.SelectedItem.ToString());
            string valueOfKey = txtEditValue.Text.Trim();
            string description = txtEditDesc.Text.Trim();
            //configClient.SetKeyValue(division, module, subModule, key, valueOfKey,description);
            //MessageBox.Show("Value added successfully.");
            txtEditValue.Text = string.Empty;
            txtEditDesc.Text = string.Empty;
        }

        protected void lnkbtnAddNewRecord_Click(object sender, EventArgs e)
        {
            mpeConfigAdd.Show();

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            IConfigurationClient configClient;
            configClient = ConfigurationClientFactory.GetConfigurationClient();
            Division division = (Division)Enum.Parse(typeof(Division), ddlprogramPop.SelectedItem.ToString());
            Module module = (Module)Enum.Parse(typeof(Module), ddlSubModulePop.SelectedItem.ToString());
            SubModule subModule = (SubModule)Enum.Parse(typeof(SubModule), ddlSubModulePop.SelectedItem.ToString());
            Key key = (Key)Enum.Parse(typeof(Key), ddlKeyPop.SelectedItem.ToString());
            string valueOfKey = txtAddValue.Text.Trim();
            string description = txtAddDescription.Text.Trim();
            //`configClient.GetSelectedData(division, module, subModule, key, valueOfKey, description);
            //MessageBox.Show("Value added successfully.");
            txtAddValue.Text = string.Empty;
            txtAddDescription.Text = string.Empty;

        }

        private void FillComboBoxValues()
        {
            PopulateList(ddlDivison, Enum.GetNames(typeof(Division)));
            PopulateList(ddlModule, Enum.GetNames(typeof(Module)));
            PopulateList(ddlSubModule, Enum.GetNames(typeof(SubModule)));
            PopulateList(ddlKey, Enum.GetNames(typeof(Key)));

            PopulateList(ddlprogramPop, Enum.GetNames(typeof(Division)));
            PopulateList(ddlModulePop, Enum.GetNames(typeof(Module)));
            PopulateList(ddlSubModulePop, Enum.GetNames(typeof(SubModule)));
            PopulateList(ddlKeyPop, Enum.GetNames(typeof(Key)));

            ddlDivison.Items.Insert(0, new ListItem("--Select--", "0"));
            ddlModule.Items.Insert(0, new ListItem("--Select--", "0"));
            ddlSubModule.Items.Insert(0, new ListItem("--Select--", "0"));
            ddlKey.Items.Insert(0, new ListItem("--Select--", "0"));

            ddlprogramPop.Items.Insert(0, new ListItem("--Select--", "0"));
            ddlModulePop.Items.Insert(0, new ListItem("--Select--", "0"));
            ddlSubModulePop.Items.Insert(0, new ListItem("--Select--", "0"));
            ddlKeyPop.Items.Insert(0, new ListItem("--Select--", "0"));
        }

        private void PopulateList(DropDownList cmb, string[] values)
        {
            cmb.DataSource = values;
            cmb.DataBind();
        }


    }
}

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddApplicationKeys.aspx.cs" Inherits="lntechNew.backroom.AddApplicationKeys" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <link href="../Styles.css" type="text/css" rel="stylesheet"/>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
   
   <script type ="text/javascript" language ="javascript">
   
   function CleanControls()
   {
        document.getElementById("tbKey").disabled=true;
        document.getElementById("tbName").value="";
        document.getElementById("tbValue").value="";
        return false;
   }
   
   </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        &nbsp;<table id="TableMain" align="center" border="0" cellpadding="0" cellspacing="0"
            width="780">
            <tr>
                <td>
                    <uc1:ActiveMenu ID="Activemenu2" runat="server" />
                </td>
            </tr>
            <tr>
                <td id="lbl_Message" align="center" background="../../images/separator_repeat.gif"
                    height="11" width="780">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td>
                    <table border="0" class="clsleftpaddingtable" width="100%">
                        <tr>
                            <td class="clsLabelNew" colspan="4" style="height: 40px">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="clsLabelNew" style="height: 23px; width: 53px;">
                                Key:
                            </td>
                            <td style="width: 207px; height: 23px">
                                <asp:TextBox ID="tbKey" runat="server" CssClass="clsinputadministration"
                                    MaxLength="30" Width="200px"></asp:TextBox></td>
                            <td class="clsLabelNew" style="height: 23px" colspan="2">
                            </td>
                        </tr>
                        <tr>
                            <td class="clsLabelNew" style="height: 29px; width: 53px;">
                                Value:
                            </td>
                            <td class="clsLabelNew" style="width: 207px; height: 29px">
                                Name &nbsp; &nbsp; &nbsp;
                                <asp:TextBox ID="tbName" runat="server" CssClass="clsinputadministration" Width="150px" MaxLength="30"></asp:TextBox></td>
                            
                            <td class="clsLabelNew" style="height: 29px; width: 75px;">
                                Value:
                            </td>
                            <td style="height: 29px">
                            
                                <asp:TextBox ID="tbValue" runat="server" CssClass="clsinputadministration" MaxLength="30"
                                    Width="200px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="clsLabelNew" colspan="3" style="height: 22px">
                            </td>
                            <td style="height: 22px">
                                &nbsp;
                                <asp:Button ID="btn_Submit" runat="server" CssClass="clsbutton"
                                    Text="Submit" OnClick="btn_Submit_Click" /></td>
                        </tr>
                        <tr>
                            <td class="clsLabelNew" colspan="3" style="height: 22px">
                            </td>
                            <td style="height: 22px">
                                &nbsp;
                                <asp:Button ID="btnAddAnotherValue" runat="server" CssClass="clsbutton"
                                    OnClientClick="return CleanControls();" Text="Add Another Value" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lbl_Msg" runat="server" CssClass="label" ForeColor="Red"></asp:Label></td>
            </tr>
            <tr>
            </tr>
            <tr>
                <td background="../../images/separator_repeat.gif" height="11">
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>

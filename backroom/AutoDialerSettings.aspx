﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AutoDialerSettings.aspx.cs"
    Inherits="HTP.backroom.AutoDialerSettings" %>

<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Auto Dialer Settings</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        function check()
        {
            if(document.getElementById("rb_StartAll").checked==false && document.getElementById("rb_StopAll").checked==false)
            {
                alert("Please select an option first.");
                document.getElementById("rb_StartAll").focus();
                return false;
            }
            else
            {
                return true;            
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <aspnew:ScriptManager ID="scriptManager" runat="server">
    </aspnew:ScriptManager>
    <div>
        <table id="TableMain" cellspacing="0" cellpadding="0" width="780px" align="center"
            border="0">
            <tr>
                <td style="width: 100%">
                    <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                </td>
            </tr>
            <tr>
                <td background="../Images/subhead_bg.gif" style="height: 34px; width: 100%" class="clssubhead">
                    <table style="width: 100%">
                        <tr>
                            <td class="clssubhead" align="left">
                                Auto Dialer Settings
                            </td>
                            <td align="right">
                                <asp:HyperLink ID="hlk_History" runat="server">History</asp:HyperLink>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%" align="center">
                    <asp:Label ID="lbl_Message" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="height: 11px; width: 100%" background="../Images/separator_repeat.gif">
                </td>
            </tr>
            <tr>
                <td style="width: 100%" class="clsLeftPaddingTable" align="right">
                    <table border="0" cellpadding="2" cellspacing="2">
                        <tr>
                            <td>
                                <asp:RadioButton ID="rb_StartAll" runat="server" CssClass="clsLabelNew" GroupName="App"
                                    Text="Start All Events" />
                            </td>
                            <td>
                                <asp:RadioButton ID="rb_StopAll" runat="server" CssClass="clsLabelNew" GroupName="App"
                                    Text="Stop All Events" />
                            </td>
                            <td>
                                <asp:Button ID="btn_UpdateAll" CssClass="clsbutton" runat="server" OnClientClick="return check();"
                                    Text="Update" onclick="btn_UpdateAll_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 11px; width: 100%" background="../Images/separator_repeat.gif">
                </td>
            </tr>
            <tr>
                <td>
                    <ajaxToolkit:TabContainer ID="tabs_EventType" runat="server" Width="100%" ScrollBars="Auto">
                        <ajaxToolkit:TabPanel ID="tabPan_1" runat="server" TabIndex="0">
                            <ContentTemplate>
                                <table cellpadding="2" cellspacing="2" align="left">
                                    <tr>
                                        <td style="width: 40px">
                                        </td>
                                        <td align="right">
                                            <asp:Label ID="lbl_CallDays1" CssClass="clsLabel" runat="server" Text="Call Days :"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddl_Days1" CssClass="clsInputadministration" runat="server">
                                                <asp:ListItem>1</asp:ListItem>
                                                <asp:ListItem>2</asp:ListItem>
                                                <asp:ListItem>3</asp:ListItem>
                                                <asp:ListItem>4</asp:ListItem>
                                                <asp:ListItem>5</asp:ListItem>
                                                <asp:ListItem>6</asp:ListItem>
                                                <asp:ListItem>7</asp:ListItem>
                                                <asp:ListItem>8</asp:ListItem>
                                                <asp:ListItem>9</asp:ListItem>
                                                <asp:ListItem>10</asp:ListItem>
                                                <asp:ListItem>11</asp:ListItem>
                                                <asp:ListItem>12</asp:ListItem>
                                                <asp:ListItem>13</asp:ListItem>
                                                <asp:ListItem>14</asp:ListItem>
                                                <asp:ListItem>15</asp:ListItem>
                                                <asp:ListItem>16</asp:ListItem>
                                                <asp:ListItem>17</asp:ListItem>
                                                <asp:ListItem>18</asp:ListItem>
                                                <asp:ListItem>19</asp:ListItem>
                                                <asp:ListItem>20</asp:ListItem>
                                                <asp:ListItem>21</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Label ID="lbl_CallDaysBusinessDays1" CssClass="clsLabel" runat="server" Text="Business Days"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td align="right">
                                            <asp:Label ID="lbl_Courts1" runat="server" CssClass="clsLabel" Text="Courts :"></asp:Label>
                                        </td>
                                        <td colspan="2">
                                            <asp:DropDownList ID="ddl_Courts1" CssClass="clsInputadministration" runat="server">
                                                <asp:ListItem Value="0">All Courts</asp:ListItem>
                                                <asp:ListItem Value="1">HMC Courts</asp:ListItem>
                                                <asp:ListItem Value="2">Non HMC Courts</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td align="right">
                                            <asp:Label ID="lbl_Range1" runat="server" CssClass="clsLabel" Text="Setting Date Range :"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddl_Range1" CssClass="clsInputadministration" runat="server">
                                                <asp:ListItem>1</asp:ListItem>
                                                <asp:ListItem>2</asp:ListItem>
                                                <asp:ListItem Selected="True">3</asp:ListItem>
                                                <asp:ListItem Value="4"></asp:ListItem>
                                                <asp:ListItem>5</asp:ListItem>
                                                <asp:ListItem>6</asp:ListItem>
                                                <asp:ListItem>7</asp:ListItem>
                                                <asp:ListItem>8</asp:ListItem>
                                                <asp:ListItem>9</asp:ListItem>
                                                <asp:ListItem>10</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Label ID="lbl_RangeBusinessDays1" CssClass="clsLabel" runat="server" Text="Business Days"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td align="right">
                                            <asp:Label ID="lbl_EventState1" CssClass="clsLabel" runat="server" Text="Event State :"></asp:Label>
                                        </td>
                                        <td colspan="2">
                                            <asp:RadioButton ID="rb_Start1" CssClass="clsLabelNew" runat="server" GroupName="state"
                                                Text="Start" />
                                            <asp:RadioButton ID="rb_Stop1" CssClass="clsLabelNew" runat="server" GroupName="state"
                                                Text="Stop" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td align="right">
                                            <asp:Label ID="lbl_DialTime1" CssClass="clsLabel" runat="server" Text="Dial Time :"></asp:Label>
                                        </td>
                                        <td colspan="2">
                                             <div style="width:375px; overflow: auto; height:95px;">
                                            <asp:CheckBoxList ID="tp_DialTime1" runat="server" RepeatColumns="4" RepeatDirection="Horizontal">
                                                <asp:ListItem Text="8:00 AM" Value="8:00 AM"></asp:ListItem>
                                                <asp:ListItem Text="8:15 AM" Value="8:15 AM"></asp:ListItem>
                                                <asp:ListItem Text="8:30 AM" Value="8:30 AM"></asp:ListItem>
                                                <asp:ListItem Text="8:45 AM" Value="8:45 AM"></asp:ListItem>
                                                <asp:ListItem Text="9:00 AM" Value="9:00 AM"></asp:ListItem>
                                                <asp:ListItem Text="9:15 AM" Value="9:15 AM"></asp:ListItem>
                                                <asp:ListItem Text="9:30 AM" Value="9:30 AM"></asp:ListItem>
                                                <asp:ListItem Text="9:45 AM" Value="9:45 AM"></asp:ListItem>
                                                <asp:ListItem Text="10:00 AM" Value="10:00 AM"></asp:ListItem>
                                                <asp:ListItem Text="10:15 AM" Value="10:15 AM"></asp:ListItem>
                                                <asp:ListItem Text="10:30 AM" Value="10:30 AM"></asp:ListItem>
                                                <asp:ListItem Text="10:45 AM" Value="10:45 AM"></asp:ListItem>
                                                <asp:ListItem Text="11:00 AM" Value="11:00 AM"></asp:ListItem>
                                                <asp:ListItem Text="11:15 AM" Value="11:15 AM"></asp:ListItem>
                                                <asp:ListItem Text="11:30 AM" Value="11:30 AM"></asp:ListItem>
                                                <asp:ListItem Text="11:45 AM" Value="11:45 AM"></asp:ListItem>
                                                <asp:ListItem Text="12:00 PM" Value="12:00 PM"></asp:ListItem>
                                                <asp:ListItem Text="12:15 PM" Value="12:15 PM"></asp:ListItem>
                                                <asp:ListItem Text="12:30 PM" Value="12:30 PM"></asp:ListItem>
                                                <asp:ListItem Text="12:45 PM" Value="12:45 PM"></asp:ListItem>
                                                <asp:ListItem Text="1:00 PM" Value="1:00 PM"></asp:ListItem>
                                                <asp:ListItem Text="1:15 PM" Value="1:15 PM"></asp:ListItem>
                                                <asp:ListItem Text="1:30 PM" Value="1:30 PM"></asp:ListItem>
                                                <asp:ListItem Text="1:45 PM" Value="1:45 PM"></asp:ListItem>
                                                <asp:ListItem Text="2:00 PM" Value="2:00 PM"></asp:ListItem>
                                                <asp:ListItem Text="2:15 PM" Value="2:15 PM"></asp:ListItem>
                                                <asp:ListItem Text="2:30 PM" Value="2:30 PM"></asp:ListItem>
                                                <asp:ListItem Text="2:45 PM" Value="2:45 PM"></asp:ListItem>
                                                <asp:ListItem Text="3:00 PM" Value="3:00 PM"></asp:ListItem>
                                                <asp:ListItem Text="3:15 PM" Value="3:15 PM"></asp:ListItem>
                                                <asp:ListItem Text="3:30 PM" Value="3:30 PM"></asp:ListItem>
                                                <asp:ListItem Text="3:45 PM" Value="3:45 PM"></asp:ListItem>
                                                <asp:ListItem Text="4:00 PM" Value="4:00 PM"></asp:ListItem>
                                                <asp:ListItem Text="4:15 PM" Value="4:15 PM"></asp:ListItem>
                                                <asp:ListItem Text="4:30 PM" Value="4:30 PM"></asp:ListItem>
                                                <asp:ListItem Text="4:45 PM" Value="4:45 PM"></asp:ListItem>
                                                <asp:ListItem Text="5:00 PM" Value="5:00 PM"></asp:ListItem>
                                                <asp:ListItem Text="5:15 PM" Value="5:15 PM"></asp:ListItem>
                                                <asp:ListItem Text="5:30 PM" Value="5:30 PM"></asp:ListItem>
                                                <asp:ListItem Text="5:45 PM" Value="5:45 PM"></asp:ListItem>
                                                <asp:ListItem Text="6:00 PM" Value="6:00 PM"></asp:ListItem>
                                                <asp:ListItem Text="6:15 PM" Value="6:15 PM"></asp:ListItem>
                                                <asp:ListItem Text="6:30 PM" Value="6:30 PM"></asp:ListItem>
                                                <asp:ListItem Text="6:45 PM" Value="6:45 PM"></asp:ListItem>
                                                <asp:ListItem Text="7:00 PM" Value="7:00 PM"></asp:ListItem>
                                                <asp:ListItem Text="7:15 PM" Value="7:15 PM"></asp:ListItem>
                                                <asp:ListItem Text="7:30 PM" Value="7:30 PM"></asp:ListItem>
                                                <asp:ListItem Text="7:45 PM" Value="7:45 PM"></asp:ListItem>
                                                <asp:ListItem Text="8:00 PM" Value="8:00 PM"></asp:ListItem>
                                            </asp:CheckBoxList>
                                            </div>  
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <asp:HiddenField ID="hf_EventID1" runat="server" />
                                        </td>
                                        <td colspan="2">
                                            <asp:Button ID="btn_Update1" CssClass="clsbutton" runat="server" Text="Update" OnClick="btn_Update1_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="tabPan_2" runat="server">
                            <ContentTemplate>
                                <table cellpadding="2" cellspacing="2" align="left">
                                    <tr>
                                        <td style="width: 40px">
                                        </td>
                                        <td align="right">
                                            <asp:Label ID="lbl_CallDays2" CssClass="clsLabel" runat="server" Text="Call Days :"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddl_Days2" CssClass="clsInputadministration" runat="server">
                                                <asp:ListItem>1</asp:ListItem>
                                                <asp:ListItem>2</asp:ListItem>
                                                <asp:ListItem>3</asp:ListItem>
                                                <asp:ListItem>4</asp:ListItem>
                                                <asp:ListItem>5</asp:ListItem>
                                                <asp:ListItem>6</asp:ListItem>
                                                <asp:ListItem>7</asp:ListItem>
                                                <asp:ListItem>8</asp:ListItem>
                                                <asp:ListItem>9</asp:ListItem>
                                                <asp:ListItem>10</asp:ListItem>
                                                <asp:ListItem>11</asp:ListItem>
                                                <asp:ListItem>12</asp:ListItem>
                                                <asp:ListItem>13</asp:ListItem>
                                                <asp:ListItem>14</asp:ListItem>
                                                <asp:ListItem>15</asp:ListItem>
                                                <asp:ListItem>16</asp:ListItem>
                                                <asp:ListItem>17</asp:ListItem>
                                                <asp:ListItem>18</asp:ListItem>
                                                <asp:ListItem>19</asp:ListItem>
                                                <asp:ListItem>20</asp:ListItem>
                                                <asp:ListItem>21</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Label ID="lbl_CallDaysBusinessDays2" CssClass="clsLabel" runat="server" Text="Business Days"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td align="right">
                                            <asp:Label ID="lbl_Courts2" runat="server" CssClass="clsLabel" Text="Courts :"></asp:Label>
                                        </td>
                                        <td colspan="2">
                                            <asp:DropDownList ID="ddl_Courts2" CssClass="clsInputadministration" runat="server">
                                                <asp:ListItem Value="0">All Courts</asp:ListItem>
                                                <asp:ListItem Value="1">HMC Courts</asp:ListItem>
                                                <asp:ListItem Value="2">Non HMC Courts</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td align="right">
                                            <asp:Label ID="lbl_Range2" runat="server" CssClass="clsLabel" Text="Setting Date Range :"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddl_Range2" CssClass="clsInputadministration" runat="server">
                                                <asp:ListItem>1</asp:ListItem>
                                                <asp:ListItem>2</asp:ListItem>
                                                <asp:ListItem Selected="True">3</asp:ListItem>
                                                <asp:ListItem Value="4"></asp:ListItem>
                                                <asp:ListItem>5</asp:ListItem>
                                                <asp:ListItem>6</asp:ListItem>
                                                <asp:ListItem>7</asp:ListItem>
                                                <asp:ListItem>8</asp:ListItem>
                                                <asp:ListItem>9</asp:ListItem>
                                                <asp:ListItem>10</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Label ID="lbl_RangeBusinessDays2" CssClass="clsLabel" runat="server" Text="Business Days"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td align="right">
                                            <asp:Label ID="lbl_EventState2" CssClass="clsLabel" runat="server" Text="Event State :"></asp:Label>
                                        </td>
                                        <td colspan="2">
                                            <asp:RadioButton ID="rb_Start2" CssClass="clsLabelNew" runat="server" GroupName="state"
                                                Text="Start" />
                                            <asp:RadioButton ID="rb_Stop2" CssClass="clsLabelNew" runat="server" GroupName="state"
                                                Text="Stop" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td align="right">
                                            <asp:Label ID="lbl_DialTime2" CssClass="clsLabel" runat="server" Text="Dial Time :"></asp:Label>
                                        </td>
                                        <td colspan="2">
                                            <div style="width:375px; overflow: auto; height:95px;">
                                            <asp:CheckBoxList ID="tp_DialTime2" runat="server" RepeatColumns="4" RepeatDirection="Horizontal">
                                                <asp:ListItem Text="8:00 AM" Value="8:00 AM"></asp:ListItem>
                                                <asp:ListItem Text="8:15 AM" Value="8:15 AM"></asp:ListItem>
                                                <asp:ListItem Text="8:30 AM" Value="8:30 AM"></asp:ListItem>
                                                <asp:ListItem Text="8:45 AM" Value="8:45 AM"></asp:ListItem>
                                                <asp:ListItem Text="9:00 AM" Value="9:00 AM"></asp:ListItem>
                                                <asp:ListItem Text="9:15 AM" Value="9:15 AM"></asp:ListItem>
                                                <asp:ListItem Text="9:30 AM" Value="9:30 AM"></asp:ListItem>
                                                <asp:ListItem Text="9:45 AM" Value="9:45 AM"></asp:ListItem>
                                                <asp:ListItem Text="10:00 AM" Value="10:00 AM"></asp:ListItem>
                                                <asp:ListItem Text="10:15 AM" Value="10:15 AM"></asp:ListItem>
                                                <asp:ListItem Text="10:30 AM" Value="10:30 AM"></asp:ListItem>
                                                <asp:ListItem Text="10:45 AM" Value="10:45 AM"></asp:ListItem>
                                                <asp:ListItem Text="11:00 AM" Value="11:00 AM"></asp:ListItem>
                                                <asp:ListItem Text="11:15 AM" Value="11:15 AM"></asp:ListItem>
                                                <asp:ListItem Text="11:30 AM" Value="11:30 AM"></asp:ListItem>
                                                <asp:ListItem Text="11:45 AM" Value="11:45 AM"></asp:ListItem>
                                                <asp:ListItem Text="12:00 PM" Value="12:00 PM"></asp:ListItem>
                                                <asp:ListItem Text="12:15 PM" Value="12:15 PM"></asp:ListItem>
                                                <asp:ListItem Text="12:30 PM" Value="12:30 PM"></asp:ListItem>
                                                <asp:ListItem Text="12:45 PM" Value="12:45 PM"></asp:ListItem>
                                                <asp:ListItem Text="1:00 PM" Value="1:00 PM"></asp:ListItem>
                                                <asp:ListItem Text="1:15 PM" Value="1:15 PM"></asp:ListItem>
                                                <asp:ListItem Text="1:30 PM" Value="1:30 PM"></asp:ListItem>
                                                <asp:ListItem Text="1:45 PM" Value="1:45 PM"></asp:ListItem>
                                                <asp:ListItem Text="2:00 PM" Value="2:00 PM"></asp:ListItem>
                                                <asp:ListItem Text="2:15 PM" Value="2:15 PM"></asp:ListItem>
                                                <asp:ListItem Text="2:30 PM" Value="2:30 PM"></asp:ListItem>
                                                <asp:ListItem Text="2:45 PM" Value="2:45 PM"></asp:ListItem>
                                                <asp:ListItem Text="3:00 PM" Value="3:00 PM"></asp:ListItem>
                                                <asp:ListItem Text="3:15 PM" Value="3:15 PM"></asp:ListItem>
                                                <asp:ListItem Text="3:30 PM" Value="3:30 PM"></asp:ListItem>
                                                <asp:ListItem Text="3:45 PM" Value="3:45 PM"></asp:ListItem>
                                                <asp:ListItem Text="4:00 PM" Value="4:00 PM"></asp:ListItem>
                                                <asp:ListItem Text="4:15 PM" Value="4:15 PM"></asp:ListItem>
                                                <asp:ListItem Text="4:30 PM" Value="4:30 PM"></asp:ListItem>
                                                <asp:ListItem Text="4:45 PM" Value="4:45 PM"></asp:ListItem>
                                                <asp:ListItem Text="5:00 PM" Value="5:00 PM"></asp:ListItem>
                                                <asp:ListItem Text="5:15 PM" Value="5:15 PM"></asp:ListItem>
                                                <asp:ListItem Text="5:30 PM" Value="5:30 PM"></asp:ListItem>
                                                <asp:ListItem Text="5:45 PM" Value="5:45 PM"></asp:ListItem>
                                                <asp:ListItem Text="6:00 PM" Value="6:00 PM"></asp:ListItem>
                                                <asp:ListItem Text="6:15 PM" Value="6:15 PM"></asp:ListItem>
                                                <asp:ListItem Text="6:30 PM" Value="6:30 PM"></asp:ListItem>
                                                <asp:ListItem Text="6:45 PM" Value="6:45 PM"></asp:ListItem>
                                                <asp:ListItem Text="7:00 PM" Value="7:00 PM"></asp:ListItem>
                                                <asp:ListItem Text="7:15 PM" Value="7:15 PM"></asp:ListItem>
                                                <asp:ListItem Text="7:30 PM" Value="7:30 PM"></asp:ListItem>
                                                <asp:ListItem Text="7:45 PM" Value="7:45 PM"></asp:ListItem>
                                                <asp:ListItem Text="8:00 PM" Value="8:00 PM"></asp:ListItem>
                                            </asp:CheckBoxList>
                                            </div>  
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <asp:HiddenField ID="hf_EventID2" runat="server" />
                                        </td>
                                        <td colspan="2">
                                            <asp:Button ID="btn_Update2" CssClass="clsbutton" runat="server" Text="Update" OnClick="btn_Update2_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                    </ajaxToolkit:TabContainer>
                </td>
            </tr>
            <tr>
                <td background="../Images/separator_repeat.gif" style="height: 11px; width: 100%">
                </td>
            </tr>
            <tr>
                <td style="width: 100%">
                    <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

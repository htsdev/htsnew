using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;

namespace lntechNew.backroom
{
    public partial class frmManageCategory : System.Web.UI.Page
    {
        clsLogger clog = new clsLogger();
        Components.ReportCategory cat = new lntechNew.Components.ReportCategory();

        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                imgbtn_assigned.Attributes.Add("OnClick", "return addItem();");
                imgbtn_unassigned.Attributes.Add("OnClick", "return removeItem();");

                if (!IsPostBack)
                {
                    FillCategory();                  
                    FillUnassigned();  
                    ddl_reportcategory.SelectedIndex = 1;
                    GetByCategoryID();
                }
            }
            catch (Exception ex)
            {
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        private void FillUnassigned()
        {
            try
            {
                lstunassigned.Items.Clear();
                DataSet ds = cat.GetReportByCategory();

                lstunassigned.DataTextField = "RptName";
                lstunassigned.DataValueField = "RptID";
                lstunassigned.DataSource = ds.Tables[1];
                lstunassigned.DataBind();
            }
            catch (Exception ex)
            {
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
               
        private void FillCategory()
        {
            try
            {
                ddl_reportcategory.Items.Clear();
                DataSet ds = cat.GetCategory();

                ddl_reportcategory.DataTextField = "CategoryName";
                ddl_reportcategory.DataValueField = "CategoryID";
                ddl_reportcategory.DataSource = ds;
                ddl_reportcategory.DataBind();

                ListItem lst = new ListItem("--- Select Report Category ---", "0");
                ddl_reportcategory.Items.Insert(0, lst);                
            }
            catch (Exception ex)
            {
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void ddl_reportcategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GetByCategoryID();
                FillUnassigned();
            }
            catch (Exception ex)
            {
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void FillAssigned()
        {
            try
            {
                lstassigned.Items.Clear();
                DataSet ds = cat.GetReportByCategory();

                lstassigned.DataTextField = "RptName";
                lstassigned.DataValueField = "RptID";
                lstassigned.DataSource = ds.Tables[0];
                lstassigned.DataBind();
            }
            catch (Exception ex)
            {
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            try
            {
                //string[] rptidArr;
                //string rptids = "";
                ////string rptnames = Request.Params.Get("addtext");
                ////rptnames = rptnames.Substring(0, rptnames.Length - 1);
                ////char[] sep ={ ',' };
                ////rptnameArr = rptnames.Split(sep);

                //rptids = Request.Params.Get("addvalue");
                //if (rptids != "" && rptids.Length > 0)
                //{
                //    rptids = rptids.Substring(0, rptids.Length - 1);
                //    char[] sep ={ ',' };
                //    rptidArr = rptids.Split(sep);


                //    /// Assigned Category
                //    for (int i = 0; i <= rptidArr.Length-1; i++)
                //    {
                //        cat.AssignedUnassinged = 1;
                //        cat.RptID = Convert.ToInt32(rptidArr[i].ToString());
                //        cat.CategoryID = Convert.ToInt32(ddl_reportcategory.SelectedValue);
                //        cat.UpdateAssignedUnassined();
                //    }
                //}
               
                //rptids = Request.Params.Get("addUnassignedvalue");
                //if (rptids != "" && rptids.Length > 0)
                //{
                //    rptids = rptids.Substring(0, rptids.Length - 1);
                //    char[] sep ={ ',' };
                //    rptidArr = rptids.Split(sep);
                //    /// Unassigned Category
                //    for (int i = 0; i <= rptidArr.Length-1; i++)
                //    {
                //        cat.AssignedUnassinged = 0;
                //        cat.RptID = Convert.ToInt32(rptidArr[i].ToString());
                //        cat.CategoryID = Convert.ToInt32(ddl_reportcategory.SelectedValue);
                //        cat.UpdateAssignedUnassined();
                //    }

                //}
                                
                //FillUnassigned();              
                //GetByCategoryID();
            }
            catch (Exception ex)
            {
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void GetByCategoryID()
        {
            try
            {
                lstassigned.Items.Clear();
                string svalue = ddl_reportcategory.SelectedItem.Text;

                if (svalue != "--- Select Report Category ---")
                {                    
                    cat.CategoryID = Convert.ToInt32(ddl_reportcategory.SelectedValue);
                    DataSet ds = cat.GetReportByCategoryID();

                    lstassigned.DataTextField = "RptName";
                    lstassigned.DataValueField = "RptID";
                    lstassigned.DataSource = ds;
                    lstassigned.DataBind();
                }
            }
            catch (Exception ex)
            {
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void imgbtn_unassigned_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (lstassigned.SelectedIndex >= 0)
                {
                    cat.AssignedUnassinged = 0;
                    cat.RptID = Convert.ToInt32(lstassigned.SelectedValue);
                    cat.CategoryID = Convert.ToInt32(ddl_reportcategory.SelectedValue);
                    cat.UpdateAssignedUnassined();
                    FillUnassigned();
                    GetByCategoryID();
                }
            }
            catch (Exception ex)
            {
                lblmsg.Text = ex.Message.ToString();
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void imgbtn_assigned_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (lstunassigned.SelectedIndex >= 0)
                {
                    cat.AssignedUnassinged = 1;
                    cat.RptID = Convert.ToInt32(lstunassigned.SelectedValue);
                    cat.CategoryID = Convert.ToInt32(ddl_reportcategory.SelectedValue);
                    cat.UpdateAssignedUnassined();
                    FillUnassigned();
                    GetByCategoryID();
                }
            }
            catch (Exception ex)
            {
                lblmsg.Text = ex.Message.ToString();
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
            
        }

    }
}

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
//using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;


namespace lntechNew.backroom
{
	/// <summary>
	/// Summary description for TemplateMain.
	/// </summary>
	public partial class TemplateMain : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.DataGrid dg_template;
		//clsENationWebComponents ClsDb = new clsENationWebComponents();
		protected System.Web.UI.WebControls.Label lblMessage;
		clsPricingPlans ClsPricingPlans = new clsPricingPlans();
		clsSession ClsSession=new clsSession();
		clsLogger clog= new clsLogger();
		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				if (ClsSession.IsValidSession(this.Request)==false)
				{
					Response.Redirect("../frmlogin.aspx",false);
				}
				else //To stop page further execution
				{
					if (ClsSession.GetCookie("sAccessType",this.Request).ToString()!="2")
					{
						Response.Redirect("../LoginAccesserror.aspx",false);
						Response.End();
					}
					else //To stop page further execution
					{
						BindingGrid();
					}
				}
			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dg_template.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dg_template_PageIndexChanged);
			this.dg_template.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dg_template_ItemDataBound);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void BindingGrid()
		{
			try
			{
				//dg_template.DataSource = ClsDb.Get_DS_BySP("usp_tempprice");
				DataSet ds =  ClsPricingPlans.GetPricingTemplates();
				if( ds.Tables[0].Rows.Count>0 )
				{
					dg_template.DataSource = ds;
					dg_template.DataBind();
				}
				else
				{
					lblMessage.Text = "Record not found";
				}
			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		
		}

		private void dg_template_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			try
			{
				dg_template.CurrentPageIndex = e.NewPageIndex;
				BindingGrid();
			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		
		}

		private void dg_template_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			try
			{			
				DataRowView drv = (DataRowView) e.Item.DataItem; 
				if (drv == null) return;

				((HyperLink)e.Item.FindControl("hlnkPriceTemplates")).NavigateUrl = "frmTemplateDetail.aspx?TemplateID=" + drv["TemplateID"]; 
			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		
		}
	}
}

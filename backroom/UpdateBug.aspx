<%@ Page Language="c#" Codebehind="UpdateBug.aspx.cs" AutoEventWireup="True" Inherits="lntechNew.backroom.UpdateBug" %>

<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="~/WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<script runat="server">

    protected void dl_comments_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    
</script>

<html>
<head>
    <title>UpdateBug</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="Styles.css" type="text/css" rel="stylesheet">

    <script language="javascript">
		function ShowHide()
		{ 
			var td=document.getElementById("tdattachment");
			var txt=document.getElementById("TextBox1").value;
			var tdcomments=document.getElementById("tdComments");
			var txtcomments=document.getElementById("TextBox2").value;
			
			if(txt == "1")
			{ 
				td.style.display='block';
			}
			if(txt == "0")
			{ 
				td.style.display='none';
			}
			if(txtcomments == "1") 
			{ 
				tdcomments.style.display='block';
			}
			if(txtcomments == "0")
			{ 
				tdcomments.style.display='none';
			} 
		}
		
		function Open(attachmentid)
		{
		
		window.open('../backroom/ViewAttachment.aspx?attachmentid=' + attachmentid ,'','height=600,width=400,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=yes');
		return(false);
		}
		function Validation()
		{ 
			var dllusers=document.getElementById("ddl_users").value;
			if(dllusers == 0)
			{ 
				alert("Please Select Assigned To User");
				document.getElementById("ddl_users").focus();
				return false;
			}
			
		//var fname=document.getElementById(fp_file.PostedFile.FileName);
	   
	     //if(fname=="")
		//	{ 
		//		alert("Please Select A File");
		//		document.getElementById("fp_file").focus();
		//		return false;
		//	}
		
		
		}
		function OpenPop(path)
		{ 
		    
		     
		    
		    window.open(path,'',"height=130,width=400,resizable=yes,status=no,scrollbar=no,menubar=no,location=no");
		    return false;
		    
		
		}
		
    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body bottommargin="0" topmargin="0" onload="ShowHide();" ms_positioning="GridLayout">
    <form id="Form1" method="post" enctype="multipart/form-data" runat="server">
        <table id="TableMain" cellspacing="0" cellpadding="0" width="780" align="center"
            border="0">
            <tr>
                <td colspan="2">
                    <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                </td>
            </tr>
            <tr>
                <td background="../../images/separator_repeat.gif" height="11" colspan="2">
                </td>
            </tr>
            <tr>
                <td class="clssubhead" background="../Images/subhead_bg.gif" style="height: 34px">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td class="clssubhead" style="height: 13px">
                                &nbsp;Update Bugs
                            </td>
                            <td align="right" class="clssubhead" style="height: 13px">
                                <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click">Back</asp:LinkButton>&nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td class="clsLeftPaddingTable" style="height: 22px">
                                <span class="clssubhead">BugID</span>
                            </td>
                            <td class="clsLeftPaddingTable" style="height: 22px">
                                <span class="clssubhead">Ticket Number</span>
                            </td>
                            <td class="clsLeftPaddingTable" style="height: 22px">
                                <span class="clssubhead">Priority</span>
                            </td>
                            <td class="clsLeftPaddingTable" style="height: 22px">
                                <span class="clssubhead">Status</span>
                            </td>
                            <td class="clsLeftPaddingTable" style="height: 22px">
                                <span class="clssubhead">Assigned To</span>
                            </td>
                            <td class="clsLeftPaddingTable" style="height: 22px">
                                <asp:HyperLink ID="hpcomments" runat="server" NavigateUrl="#">Add Comments</asp:HyperLink></td>
                        </tr>
                        <tr>
                            <td class="clsLeftPaddingTable" style="height: 28px">
                                <asp:Label ID="lblBugId" runat="server" CssClass="label"></asp:Label>
                            </td>
                            <td style="height: 28px" class="clsLeftPaddingTable">
                                <asp:Label ID="TicketNo" runat="server" CssClass="label"></asp:Label>
                            </td>
                            <td class="clsLeftPaddingTable" style="height: 28px">
                                <asp:DropDownList ID="ddl_priority" runat="server" CssClass="clsinputcombo">
                                </asp:DropDownList>
                            </td>
                            <td style="height: 28px" class="clsLeftPaddingTable">
                                <asp:DropDownList ID="ddl_Status" runat="server" CssClass="clsinputcombo">
                                </asp:DropDownList>
                            </td>
                            <td class="clsLeftPaddingTable" style="height: 28px">
                                <asp:DropDownList ID="ddl_users" runat="server" CssClass="clsinputcombo">
                                </asp:DropDownList></td>
                            <td style="height: 28px" class="clsLeftPaddingTable">
                                &nbsp;<asp:Button ID="btnSubmit" runat="server" CssClass="clsButton" OnClick="btnSubmit_Click"
                                    OnClientClick="return  Validation();" Text="Update" /></td>
                            </tr>
                        <tr>
                        </tr>
                        <tr>
                            <td class="clsleftpaddingtable" style="height: 23px">
                                <span class="clssubhead">Page Url </span>
                            </td>
                            <td colspan="4" class="clsLeftPaddingTable" style="height: 23px">
                            
                            
                            
                                <asp:LinkButton ID="hlPageurl" runat="server" OnClick="hlPageurl_Click">...</asp:LinkButton></td>
                                
                            <td style="height: 23px" class="clsleftpaddingtable">
                                &nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td class="clssubhead" background="../Images/subhead_bg.gif" style="height: 35px">
                    &nbsp;Upload File
                </td>
            </tr>
            <tr>
                <td valign="top" style="height: 45px" class="clsLeftPaddingTable" >
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="height: 29px">
                                    <tr>
                                        <td class="clsleftpaddingtable" width="12%" style="height: 20px">
                                            &nbsp;File Description&nbsp;
                                        </td>
                                        <td class="clsLeftPaddingTable" align="left" style="width: 20%; height: 20px;">
                                            <asp:TextBox ID="txt_filedesc" runat="server" Width="150px" MaxLength="100" CssClass="clstextarea"></asp:TextBox></td>
                                        <td class="clsleftpaddingtable" align="right" width="13%" style="height: 20px">
                                            Upload File&nbsp;</td>
                                        <td class="clsLeftPaddingTable" width="25%" style="height: 20px">
                                            <input id="fp_file" type="file" name="File1" runat="server" class="clstextarea"></td>
                                        <td class="clsLeftPaddingTable" colspan="5" style="width: 232px; height: 20px;">
                                            &nbsp;<asp:Button ID="btn_upload" runat="server" Text="Upload" CssClass="clsButton" OnClick="btn_upload_Click"
                                                OnClientClick="return  Validation();"></asp:Button></td>
                                    </tr>
                                </table>
                </td>
            </tr>
            
             
            <tr>
                <td valign="top" colspan="2">
                    <table id="tbl1" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td class="clsLeftPaddingTable" align="center">
                                <asp:Label ID="lblMessage" runat="server" ForeColor="Red" CssClass="label"></asp:Label></td>
                        </tr>
                        <tr>
                            <td class="clssubhead" id="tdattachment" background="../Images/subhead_bg.gif" colspan="2"
                                height="34">
                                &nbsp;Attachment&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="clsLeftPaddingTable" align="left" colspan="2">
                                <asp:DataGrid ID="dg_attachment" runat="server" Width="100%" AutoGenerateColumns="False">
                                    <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader" VerticalAlign="Middle"></HeaderStyle>
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="File Description">
                                            <HeaderStyle HorizontalAlign="Left" CssClass="clssubhead"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblfiledesc" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="File Name">
                                            <HeaderStyle HorizontalAlign="Left" CssClass="clssubhead"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblfilename" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.attachfilename") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Uploaded Date">
                                            <HeaderStyle HorizontalAlign="Left" CssClass="clssubhead"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblUploadedDate" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container,"DataItem.uploadeddate") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="UserName">
                                            <HeaderStyle HorizontalAlign="Left" CssClass="clssubhead"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblusername" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.username") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkView" runat="server" CommandName="View" >View</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn Visible="False" HeaderText="attachid">
                                            <ItemTemplate>
                                                <asp:Label ID="lblattachmentid" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.attachmentid") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn Visible="False" HeaderText="bugid">
                                            <ItemTemplate>
                                                <asp:Label ID="lblbugid" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.attachmentid") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid></td>
                        </tr>
                        <tr>
                <td class="clssubhead" background="../Images/subhead_bg.gif" style="height: 35px">
                    &nbsp;Bug Description</td>
            </tr>
             <tr>
                <td valign="top" >
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="height: 29px">
                                    <tr>
                                      
                                        <td class="clsLeftPaddingTable" align="left" style="width: 20%; height: 20px;">
                                            <asp:Label ID="lbldesc" runat="server" Width="774px" MaxLength="100" CssClass="clstextarea" Height="120px" Font-Bold="True" ForeColor="Gray"></asp:Label
                                            ></td>
                                     
                                    </tr>
                                </table>
                </td>
            </tr>
                        <tr>
                            <td class="clssubhead" id="tdComments" background="../Images/subhead_bg.gif" colspan="2"
                                height="34">
                                &nbsp;Comments
                            </td>
                        </tr>
                        <tr>
                            <td class="clsLeftPaddingTable" align="left" colspan="2">
                            </td>
                        </tr>
                        <tr>
                            <td class="clsLeftPaddingTable" align="left" colspan="2">
                                <asp:DataList ID="dl_comments" runat="server" Width="100%" OnSelectedIndexChanged="dl_comments_SelectedIndexChanged">
                                    <AlternatingItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                        Font-Strikeout="False" Font-Underline="False"></AlternatingItemStyle>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False"></ItemStyle>
                                    <ItemTemplate>
                                        <table id="tbllist" cellspacing="0" cellpadding="0" border="0">
                                            <tr>
                                                <td class="clsLeftPaddingTable">
                                                    <span style="color: red">Comment Posted By :</span>
                                                    <asp:Label ID="lblName" runat="server" ForeColor="red" CssClass="clsLeftPaddingTable"
                                                        Text='<%# DataBinder.Eval(Container, "DataItem.username") %>'>
                                                    </asp:Label>&nbsp;<span style="color: red">on</span>
                                                    <asp:Label ID="lbldatetime" runat="server" ForeColor="red" CssClass="clsLeftPaddingTable"
                                                        Text='<%# DataBinder.Eval(Container, "DataItem.commentsdate") %>'>
                                                    </asp:Label>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblcomments" runat="server" Font-Size="10pt" CssClass="clslabel" Text='<%# DataBinder.Eval(Container,"DataItem.comments") %>'></asp:Label></td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False"></HeaderStyle>
                                </asp:DataList></td>
                        </tr>
                        <tr>
                            <td background="../../images/separator_repeat.gif" colspan="2" height="11">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td valign="top" colspan="2">
                    <asp:TextBox ID="TextBox1" runat="server" Width="0px">0</asp:TextBox><asp:TextBox
                        ID="Textbox2" runat="server" Width="0px">0</asp:TextBox></td>
            </tr>
        </table>
        &nbsp;</form>
</body>
</html>

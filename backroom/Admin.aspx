﻿<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>

<%@ Page Language="c#" AutoEventWireup="false" Inherits="HTP.backroom.Admin" CodeBehind="Admin.aspx.cs" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>User Management</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../Styles.css" type="text/css" rel="stylesheet" />

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>

    <script src="../Scripts/jsDate.js" type="text/javascript"></script>

    <%--<link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />--%>


    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="../Styles.css" type="text/css" rel="stylesheet">

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
<%--    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />--%>

</head>

<body  onload="javascript:TD1TD2();">

    <form id="Form1" method="post" runat="server">

        <script type="text/javascript" language="javascript">
            function accesstype() {
                if (document.getElementById("ddl_accesstype").value == 1) {
                    document.getElementById("td_closeout").style.visibility = 'visible';
                    document.getElementById("ddl_closeout").style.visibility = 'visible';
                }
                else {
                    document.getElementById("td_closeout").style.visibility = 'hidden';
                    document.getElementById("ddl_closeout").style.visibility = 'hidden';
                    document.getElementById("tb_NoofDays").style.visibility = 'hidden';
                    document.getElementById("td_nodays").style.visibility = 'hidden';
                }
            }

            function showdaysbox() {
                if (document.getElementById("ddl_closeout").value == 1) {
                    document.getElementById("tb_NoofDays").style.visibility = 'visible';
                    document.getElementById("td_nodays").style.visibility = 'visible';
                }
                else {
                    document.getElementById("tb_NoofDays").style.visibility = 'hidden';
                    document.getElementById("td_nodays").style.visibility = 'hidden';
                }
            }
            function TD1TD2() {
                if (document.getElementById("ddl_accesstype").value != 1) {
                    document.getElementById("td_closeout").style.visibility = 'hidden';
                    document.getElementById("ddl_closeout").style.visibility = 'hidden';
                }

                if (document.getElementById("ddl_closeout").value != 1) {
                    document.getElementById("tb_NoofDays").style.visibility = 'hidden';
                    document.getElementById("td_nodays").style.visibility = 'hidden';
                }


                if (document.Form1.ddl_spnsearch.value != 1) {
                    document.getElementById("TD1").style.visibility = 'hidden';
                    document.getElementById("TD2").style.visibility = 'hidden';
                }
                    //if(document.getElementById("chkb_CanSPNSearch").checked==true)
                else {
                    document.getElementById("TD1").style.visibility = 'visible';
                    document.getElementById("TD2").style.visibility = 'visible';
                }
                //ozair 5196 11/29/2008
                ShowHideFirmddl();
            }

            var whitespace = " \t\n\r";


            function isWhitespace(s) {
                var i;


                if (isEmpty(s)) return true;
                for (i = 0; i < s.length; i++) {
                    var c = s.charAt(i);
                    if (whitespace.indexOf(c) == -1) return false;
                }

                return true;
            }



            function stripWhitespace(s) {
                return stripCharsInBag(s, whitespace)
            }

            function isEmpty(s) {
                return ((s == null) || (s.length == 0))
            }

            function isEmail(s) {
                if (isEmpty(s))
                    if (isEmail.arguments.length == 1) return false;
                    else return (isEmail.arguments[1] == true);


                if (isWhitespace(s)) return false;



                var i = 1;
                var sLength = s.length;


                while ((i < sLength) && (s.charAt(i) != "@")) {
                    i++
                }

                if ((i >= sLength) || (s.charAt(i) != "@")) return false;
                else i += 2;


                while ((i < sLength) && (s.charAt(i) != ".")) {
                    i++
                }


                if ((i >= sLength - 1) || (s.charAt(i) != ".")) return false;
                else return true;
            }
            // Noufil 4232 07/07/2008 Function to check the letter in the input
            function alphanumeric(alphane) {
                var numaric = alphane;
                for (var j = 0; j < numaric.length; j++) {
                    var alphaa = numaric.charAt(j);
                    var hh = alphaa.charCodeAt(0);
                    //if((hh > 47 && hh<58) || (hh > 64 && hh<91) || (hh > 96 && hh<123))
                    //ASCII code for aphabhets
                    if (!((hh > 64 && hh < 91) || (hh > 96 && hh < 123))) {
                        return false;
                    }
                }
            }


            function trimAll(sString) {
                while (sString.substring(0, 1) == ' ') {
                    sString = sString.substring(1, sString.length);
                }
                while (sString.substring(sString.length - 1, sString.length) == ' ') {
                    sString = sString.substring(0, sString.length - 1);
                }
                return sString;
            }
            // Noufil 4378 08/06/2008 Function to accpet only alphabets and numbers removing special character
            function CheckName(name) {
                for (i = 0 ; i < name.length ; i++) {
                    var asciicode = name.charCodeAt(i)
                    //If not valid alphabet 
                    if (!((asciicode >= 64 && asciicode <= 90) || (asciicode >= 97 && asciicode <= 122) || (asciicode >= 48 && asciicode <= 57)))
                        return false;
                }
                return true;
            }

            function formSubmit() {

                var lname = document.getElementById("tb_lname").value;

                if (trimAll(lname) == "") {
                    alert("Please specify Last Name");
                    document.getElementById("tb_lname").focus();
                    return false;
                }

                if (!isNaN(lname)) {
                    alert("Name should only contains alphabets.");
                    return false;
                }

                if (alphanumeric(lname) == false) {
                    alert("Name should only contains alphabets.");
                    return false;
                }
                var fname = document.getElementById("tb_fname").value;

                if (trimAll(fname) == "") {
                    alert("Please specify First Name");
                    document.getElementById("tb_fname").focus();
                    return false;
                }

                if (!isNaN(fname)) {
                    alert("Name should only contains alphabets.");
                    return false;
                }

                if (alphanumeric(fname) == false) {
                    alert("Name should only contains alphabets.");
                    return false;
                }

                if (trimAll(document.getElementById("tb_abbrev").value) == "") {
                    alert("Please Users Abbreviation");
                    document.getElementById("tb_abbrev").focus();
                    return false;
                }

                if (trimAll(document.getElementById("tb_uname").value) == "") {
                    alert("Please specify UserName");
                    document.getElementById("tb_uname").focus();
                    return false;
                }
                if (CheckName(document.getElementById("tb_uname").value) == false) {
                    alert("User name must only contain alphabets and numbers");
                    document.getElementById("tb_uname").focus();
                    return false;
                }

                if (trimAll(document.getElementById("tb_password").value) == "") {
                    alert("Please specify Password");
                    document.getElementById("tb_password").focus();
                    return false;
                }
                // Noufil 4378 08/06/2008 Allow only alphabets and number in password n user name
                if (CheckName(document.getElementById("tb_password").value) == false) {

                    alert("Password must only contain alphabets and numbers");
                    document.getElementById("tb_password").focus();
                    return false;
                }

                if (trimAll(document.getElementById("txt_email").value) == "") {
                    alert("Please specify EmailAddress");
                    document.getElementById("txt_email").focus();
                    return false;
                }
                else {
                    if (isEmail(document.getElementById("txt_email").value) == false) {
                        alert("Please enter Email Address in Correct format.");
                        document.getElementById("txt_email").focus();
                        return false;
                    }
                }

                if (document.getElementById("ddl_accesstype").value == 0) {
                    alert("Please specify Access Type");
                    document.getElementById("ddl_accesstype").focus();
                    return false;
                }

                if (document.getElementById("ddl_accesstype").value == "1") {
                    if (document.getElementById("ddl_closeout").value == -1) {
                        alert("Please specify Close Out Access Type");
                        document.getElementById("ddl_closeout").focus();
                        return false;
                    }
                    //ozair 4812 09/13/2008 validation check implemented
                    if (document.getElementById("ddl_closeout").value == "1") {
                        if (document.getElementById("tb_NoofDays").value == "") {
                            alert("Please enter Close Out Access days");
                            document.getElementById("tb_NoofDays").focus();
                            return false;
                        }
                    }
                }
                if (document.getElementById("ddl_status").value == -1) {
                    alert("Please specify user status");
                    document.getElementById("ddl_status").focus();
                    return false;
                }

                if (document.getElementById("ddl_spnsearch").value == -1) {
                    alert("Please specify SPN search status");
                    document.getElementById("ddl_spnsearch").focus();
                    return false;
                }
                else if (document.getElementById("ddl_spnsearch").value == 1) {

                    if (document.getElementById("txt_SPNUserName").value == "") {
                        alert("Please enter SPN UserName");
                        document.getElementById("txt_SPNUserName").focus();
                        return false;
                    }

                    if (document.getElementById("txt_SPNPassword").value == "") {
                        alert("Please enter SPN Password");
                        document.getElementById("txt_SPNPassword").focus();
                        return false;
                    }
                }
                var cb = document.getElementById("chkAttorney");
                if (cb.checked) {

                    //Nasir 6968 01/22/2010 validate contact number
                    //var txtContactID1 = document.getElementById('txt_CC11');
                    //var txtContactID2 = document.getElementById('txt_CC12');
                    //var txtContactID3 = document.getElementById('txt_CC13');
                    //var txtContactID4 = document.getElementById('txt_CC14');


                    var txt_date = document.getElementById('txt_Date');

                    var ContactNumber = txt_date.value;
                    if (ContactNumber != "") {
                        if (ContactNumber.length < 10 || isNaN(txt_date.value)) {
                            alert("Please Enter Correct Phone Number");
                            txtContactID1.focus();
                            return false;
                        }
                    }
                }

                return true;
            }
            //Fahad 5196 11/28/2008 Function to Show or Hide the Firm ddl according to the 
            function ShowHideFirmddl() {
                var cb = document.getElementById("chkAttorney");

                if (cb.checked) {

                    document.getElementById("ddl_Firm").style.display = "block";
                    document.getElementById("lblFirm").style.display = "block";
                    //Nasir 6968 11/23/2009
                    document.getElementById("tdAttorCol").colSpan = "2";
                    document.getElementById("tdContNum").style.display = "block";
                    document.getElementById("tdContactNum").style.display = "block";
                }
                else if (!cb.checked) {
                    document.getElementById("ddl_Firm").style.display = "None";
                    document.getElementById("lblFirm").style.display = "None";
                    document.getElementById("ddl_Firm").Value = "3000";
                    //Nasir 6968 11/23/2009
                    document.getElementById("tdAttorCol").colSpan = "5";
                    document.getElementById("tdContNum").style.display = "None";
                    document.getElementById("tdContactNum").style.display = "None";
                }
            }



    </script>

<div class="page-container row-fluid container-fluid">
    <aspnew:ScriptManager ID="smFrmMain" runat="server">
                <Scripts>
                    <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
                </Scripts>
            </aspnew:ScriptManager>

            <asp:Panel ID="pnl" runat="server">
                <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
            </asp:Panel>

    <!-- START CONTENT -->
<section id="main-content" class="backroom-admin-page">
    <section class="wrapper main-wrapper row" style=''>

    <div class='col-xs-12'>
        <div class="page-title">

            <div class="pull-left">
                <!-- PAGE HEADING TAG - START -->
                <h1 class="title">User Management</h1>
                <!-- PAGE HEADING TAG - END -->   
            </div>                            
                                
        </div>
    </div>
    <div class="clearfix"></div>
    <!-- MAIN CONTENT AREA STARTS -->
    
    <div class="col-xs-12">
        <section class="box ">

            <div class="content-body">
            
                <div class="row">

                    <div class="col-md-4 col-sm-5 col-xs-6">
                        <div class="form-group">
                            <label class="form-label" for="field-1">Last Name</label>
                            <div class="controls">
                                <asp:TextBox ID="tb_lname" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-5 col-xs-6">
                        <div class="form-group">
                            <label class="form-label" for="field-1">First Name</label>
                            <div class="controls">
                                <asp:TextBox ID="tb_fname" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-5 col-xs-6">
                        <div class="form-group">
                            <label class="form-label" for="field-1">Short</label>
                            <div class="controls">
                                <asp:TextBox ID="tb_abbrev" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row">
                
                    <div class="col-md-3 col-sm-4 col-xs-5">
                        <div class="form-group">
                            <label class="form-label" for="field-1">User Name</label>
                            <div class="controls">
                                <asp:TextBox ID="tb_uname" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-4 col-xs-5">
                        <div class="form-group">
                            <label class="form-label" for="field-1">Password</label>
                            <div class="controls">
                                <asp:TextBox ID="tb_password" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-4 col-xs-5">
                        <div class="form-group">
                            <label class="form-label" for="field-1">Email Address</label>
                            <div class="controls">
                                <asp:TextBox ID="txt_email" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-4 col-xs-5">
                        <div class="form-group">
                            <label class="form-label" for="field-1">NT User ID</label>
                            <div class="controls">
                                <asp:TextBox ID="txt_NTUserID" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-md-4 col-sm-5 col-xs-6">
                        <div class="form-group">
                            <label class="form-label" for="field-1">Status</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddl_status" runat="server" CssClass="form-control m-bot15">
                                    <asp:ListItem Value="-1">--Choose--</asp:ListItem>
                                    <asp:ListItem Value="1">Active</asp:ListItem>
                                    <asp:ListItem Value="0">InActive</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-5 col-xs-6">
                        <div class="form-group">
                            <label class="form-label" for="field-1">Access Type</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddl_accesstype" runat="server" CssClass="form-control m-bot15" onchange="return accesstype();">
                                    <asp:ListItem Value="0">--Choose--</asp:ListItem>
                                    <asp:ListItem Value="1">Secondary</asp:ListItem>
                                    <asp:ListItem Value="2">Primary</asp:ListItem>
                                    <asp:ListItem Value="3">Read Only</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                            
                    <div class="col-md-4 col-sm-5 col-xs-6">
                        <div class="form-group">
                            <label class="form-label" for="field-1">SPN Search</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddl_spnsearch" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="-1">--Choose--</asp:ListItem>
                                    <asp:ListItem Value="1">Access</asp:ListItem>
                                    <asp:ListItem Value="0">Not Access</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-md-6 col-sm-7 col-xs-8">
                        <div class="form-group">
                            <table>
                                <tr>
                                    <td id="td_closeout" runat="server">                            
                                        <label class="form-label" for="field-1">Close Out Page</label>
                                    </td>
                                </tr>
                            </table>
                        
                            <div class="controls">
                            
                                <asp:DropDownList ID="ddl_closeout" runat="server" CssClass="form-control m-bot15" onchange="return showdaysbox();">
                                    <asp:ListItem Value="-1">--Choose--</asp:ListItem>
                                    <asp:ListItem Value="1">Access</asp:ListItem>
                                    <asp:ListItem Value="0">Not Access</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                
                    <div class="col-md-6 col-sm-7 col-xs-8">
                        <div class="form-group">
                            <table>
                                <tr>
                                    <td id="td_nodays" runat="server">
                                        <label class="form-label" for="field-1">No of Days</label>
                                    </td>
                                </tr>
                            </table>
                        
                            <div class="controls">
                                <asp:TextBox ID="tb_NoofDays" runat="server" CssClass="form-control" MaxLength="20" ></asp:TextBox>
                            </div>
                        </div>
                    </div>

                </div>

            

                <div class="row">
                    <div class="col-md-6 col-sm-7 col-xs-8">
                    <table style="width: 100%">
                        <tr>
                            <td id="TD2" runat="server">
                            
                                    <div class="form-group">
                                        <label class="form-label" for="field-1">SPN User Name</label>
                                        <div class="controls">
                            
                                            <asp:TextBox ID="txt_SPNUserName" runat="server" CssClass="form-control"></asp:TextBox>
                                   
                                        </div>
                                    </div>
                            
                            </td>
                        </tr>
                    </table>
                    </div>
                    <div class="col-md-6 col-sm-7 col-xs-8">
                    <table>
                        <tr>
                            <td id="TD1" runat="server">

                            
                                    <div class="form-group">
                                        <label class="form-label" for="field-1">SPN Password</label>
                                        <div class="controls">
                                        
                                                        <asp:TextBox ID="txt_SPNPassword" runat="server" CssClass="form-control" Columns="80" MaxLength="80" Wrap="False"></asp:TextBox>
                                                
                                        </div>
                                    </div>
                            
                            </td>
                        </tr>
                    </table>
                    </div>
                      

                </div>              
                   
                            

                <div class="row">

                    <div class="col-md-6 col-sm-7 col-xs-8">
                        <div class="form-group">
                            <label class="form-label" for="field-1">SPN Search</label>
                            <div class="controls">
                                <asp:CheckBox ID="chk_StAdmin" runat="server" CssClass="form-control" Text=" Service Ticket Admin" />
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-7 col-xs-8">
                        <div class="form-group">
                            <label class="form-label" for="field-1">&nbsp;</label>
                            <div class="controls">
                            
                                <asp:CheckBox ID="chkAttorney" runat="server" CssClass="form-control" Text=" Is Attorney " onclick="ShowHideFirmddl();" />
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-md-6 col-sm-7 col-xs-8">
                        <div class="form-group">
                            <span id="lblFirm" runat="server" class="form-label" style="display: None;">
                                Associated Firm
                            </span>
                            <div class="controls">
                                <asp:DropDownList ID="ddl_Firm" runat="server" CssClass="form-control m-bot15" Style="display: None;">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                
                    <div class="col-md-6 col-sm-7 col-xs-8">
                        <div class="form-group">
                            <table>
                                <tr>
                                    <td id="tdAttorCol">
                                        <span runat="server" style="display: block"></span>
                                    </td>
                                    <td id="tdContNum" style="display:none;">
                                        <span  runat="server" class="form-label">Contact Number</span>
                                    </td>
                                </tr>
                            </table>
                            <div class="controls">
                                <table>
                                    <tr>
                                        <td id="tdContactNum" colspan="2" style="display: None;" >


                                            <asp:TextBox ID="txt_Date" runat="server" CssClass="form-control" ></asp:TextBox>

                                            <%--<asp:TextBox ID="txt_CC11" onkeyup="return autoTab(this, 3, event)" runat="server" Width="90px" CssClass="form-control pull-left" TabIndex="14" Text="CC11"></asp:TextBox>
                                            <p class="pull-left">&nbsp;-&nbsp;</p>
                                            <asp:TextBox ID="txt_CC12" onkeyup="return autoTab(this, 3, event)" runat="server" Width="90px" CssClass="form-control pull-left" TabIndex="15" Text="CC12"></asp:TextBox>
                                            <p class="pull-left">&nbsp;-&nbsp;</p>
                                            <asp:TextBox ID="txt_CC13" onkeyup="return autoTab(this, 4, event)" runat="server" Width="90px" CssClass="form-control pull-left" TabIndex="16" Text="CC13"></asp:TextBox>
                                            <p class="pull-left">&nbsp;X&nbsp;</p>
                                            <asp:TextBox ID="txt_CC14" onkeyup="return autoTab(this, 4, event)" runat="server" Width="100px" CssClass="form-control pull-left" TabIndex="17" Text="CC14"></asp:TextBox>--%>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-7 col-xs-8">
                        <div class="form-group">
                            <div class="controls">
                            
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-md-6 col-sm-7 col-xs-8">
                        <div class="form-group">
                            <asp:Label ID="lblMessage" runat="server" CssClass="form-label"></asp:Label>
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-md-6 col-sm-7 col-xs-8">
                        <div class="form-group">
                            <div class="controls">
                                <asp:Button ID="btn_update" runat="server" CssClass="btn btn-primary" Text="Update" OnClick="btn_update_Click1">
                                </asp:Button>
                                <asp:Button ID="btn_clear" runat="server" CssClass="btn btn-primary" Text="Clear">
                                </asp:Button>
                            </div>
                        </div>
                    </div>


                </div>


            </div>
        </section>

    </div>



    <div class="col-lg-12">
        <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">Results</h2>
                <div class="">
                    <div class="col-md-8 col-sm-7 col-xs-8 pull-right actions" style="margin-top: 16px;">

                        <div class="col-md-6 col-sm-7 col-xs-8">

                            <asp:RadioButtonList ID="rbl_status" runat="server" CssClass="" RepeatDirection="Horizontal"
                                AutoPostBack="True" OnSelectedIndexChanged="rbl_status_SelectedIndexChanged">
                                <asp:ListItem Value="1" Selected="True">&lt;span class=&quot;clssubhead&quot; &gt;Active Users &lt;/span&gt;</asp:ListItem>
                                <asp:ListItem Value="0">&lt;span class=&quot;clssubhead&quot; &gt;Inactive Users&lt;/span&gt;</asp:ListItem>
                            </asp:RadioButtonList>

                        </div>

                        <div class="col-md-6 col-sm-7 col-xs-8">
                            <asp:DropDownList ID="ddl_sort" runat="server" CssClass="form-control m-bot5" AutoPostBack="True" OnSelectedIndexChanged="ddl_sort_SelectedIndexChanged">
                                <asp:ListItem Value="lastname">Sort By</asp:ListItem>
                                <asp:ListItem Value="LastName">Last Name</asp:ListItem>
                                <asp:ListItem Value="AccessType">AccessType</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </header>
            <div class="content-body">
                <div class="row">
                    <div class="col-xs-12">

                        <asp:DataGrid ID="dg_results" runat="server" CssClass="table" AutoGenerateColumns="False">
                                <Columns>
                                    <asp:TemplateColumn HeaderText="Last Name">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlnk_lname" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.lastname") %>' Visible="False">
                                            </asp:HyperLink>
                                            <asp:LinkButton ID="lnkbtn_lname" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.lastname") %>' CommandName="click">
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="First Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_fname" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.firstname") %>'>
                                            </asp:Label>
                                            <asp:Label ID="lbl_eid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.employeeid") %>' Visible="False">
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Short">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_abbrev" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.abbreviation") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="User Name">
                                        <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_uname" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.username") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Access Type">
                                        <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_accesstype" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.access") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Close Out Page">
                                        <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_Canview" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.CanUpdateCloseOutLog") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="SPN Search">
                                        <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_CanSPNSearch" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.CanSPNSearch") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="NTUserID">
                                        <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_NTUserID" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.NTUserID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Status">
                                        <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_status" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Status") %>' CssClass="form-label"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Attorney">
                                        <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_Attorney" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.IsAttorney") %>' CssClass="form-label"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Access ST">
                                        <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_stAdmin" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AccessST") %>' CssClass="form-label"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        <asp:Label ID="lblRecCount" runat="server" ForeColor="White"></asp:Label>

                    </div>
                </div>
            </div>
        </section>
    </div>


    <!-- MAIN CONTENT AREA ENDS -->
    </section>
</section>

</div>
<!-- END CONTAINER -->
<!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->


<!-- CORE JS FRAMEWORK - START --> 
<script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script> 
<script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script> 
<script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
<script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>  
<script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script> 
<script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>  
<script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
<!-- CORE JS FRAMEWORK - END --> 


<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 

<script src="../assets/plugins/autosize/autosize.min.js" type="text/javascript"></script>
<script src="../assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


<!-- CORE TEMPLATE JS - START --> 
<script src="../assets/js/scripts.js" type="text/javascript"></script> 
<!-- END CORE TEMPLATE JS - END --> 


<!-- General section box modal start -->
<div class="modal" id="section-settings" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog animated bounceInDown">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Section Settings</h4>
            </div>
            <div class="modal-body">

                Body goes here...

            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                <button class="btn btn-success" type="button">Save changes</button>
            </div>
        </div>
    </div>
</div>
<!-- modal end -->
        </form>
</body>
















<%--<body onload="javascript:TD1TD2();">

    <script type="text/javascript" language="javascript">
    function accesstype()
    {
       if (document.getElementById("ddl_accesstype").value== 1)
       {
            document.getElementById("td_closeout").style.visibility = 'visible';
		    document.getElementById("ddl_closeout").style.visibility = 'visible';            
	   }
	   else
	   {
		    document.getElementById("td_closeout").style.visibility = 'hidden';
		    document.getElementById("ddl_closeout").style.visibility = 'hidden';
		    document.getElementById("tb_NoofDays").style.visibility = 'hidden';
		    document.getElementById("td_nodays").style.visibility = 'hidden';
	   }
    }
    
    function showdaysbox()
    {
        if (document.getElementById("ddl_closeout").value== 1)
       {
            document.getElementById("tb_NoofDays").style.visibility = 'visible';
		    document.getElementById("td_nodays").style.visibility = 'visible';            
	   }
	   else
	   {
		    document.getElementById("tb_NoofDays").style.visibility = 'hidden';
		    document.getElementById("td_nodays").style.visibility = 'hidden';
	   }
    }
		function TD1TD2()
		{
		    if (document.getElementById("ddl_accesstype").value != 1)
		    {
		        document.getElementById("td_closeout").style.visibility = 'hidden';
		        document.getElementById("ddl_closeout").style.visibility = 'hidden';
		    }
		    
		    if (document.getElementById("ddl_closeout").value!= 1)
            {
                document.getElementById("tb_NoofDays").style.visibility = 'hidden';
		        document.getElementById("td_nodays").style.visibility = 'hidden';      
	        }
		   	   
		    
		    if ( document.Form1.ddl_spnsearch.value != 1 )
		    {		    
		        document.getElementById("TD1").style.visibility = 'hidden';
		        document.getElementById("TD2").style.visibility = 'hidden';
		    }
		    //if(document.getElementById("chkb_CanSPNSearch").checked==true)
		    else
		    {
		        document.getElementById("TD1").style.visibility = 'visible';
		        document.getElementById("TD2").style.visibility = 'visible';
		    }
		    //ozair 5196 11/29/2008
		     ShowHideFirmddl();
		}
		
		var whitespace = " \t\n\r";

		
		function isWhitespace (s)
           {   var i;

    
            if (isEmpty(s)) return true;
            for (i = 0; i < s.length; i++)
            {          
                var c = s.charAt(i);
                if (whitespace.indexOf(c) == -1) return false;
            }
     
            return true;
           }

		
		
function stripWhitespace (s)
{   return stripCharsInBag (s, whitespace)
}
		
		function isEmpty(s)
        {   return ((s == null) || (s.length == 0))
        }
		
		function isEmail (s)
{   if (isEmpty(s)) 
       if (isEmail.arguments.length == 1) return false;
       else return (isEmail.arguments[1] == true);
   
    
    if (isWhitespace(s)) return false;
       
    
    
    var i = 1;
    var sLength = s.length;

    
    while ((i < sLength) && (s.charAt(i) != "@"))
    { i++
    }

    if ((i >= sLength) || (s.charAt(i) != "@")) return false;
    else i += 2;

    
    while ((i < sLength) && (s.charAt(i) != "."))
    { i++
    }

    
    if ((i >= sLength - 1) || (s.charAt(i) != ".")) return false;
    else return true;
}
    // Noufil 4232 07/07/2008 Function to check the letter in the input
	function alphanumeric(alphane)
    {
	    var numaric = alphane;
	    for(var j=0; j<numaric.length; j++)
		    {
		          var alphaa = numaric.charAt(j);
		          var hh = alphaa.charCodeAt(0);
		          //if((hh > 47 && hh<58) || (hh > 64 && hh<91) || (hh > 96 && hh<123))
		          //ASCII code for aphabhets
		          if (!((hh > 64 && hh<91) || (hh > 96 && hh<123)))
		          {
		          return false;
		          }    
		    }
	}
	
		
		function trimAll(sString) 
		{
			while (sString.substring(0,1) == ' ')
			{
				sString = sString.substring(1, sString.length);
			}
			while (sString.substring(sString.length-1, sString.length) == ' ')
			{
				sString = sString.substring(0,sString.length-1);
			}
			return sString;
		}
		// Noufil 4378 08/06/2008 Function to accpet only alphabets and numbers removing special character
		function CheckName(name)
        {       
            for ( i = 0 ; i < name.length ; i++)
            {
                var asciicode =  name.charCodeAt(i)
                //If not valid alphabet 
                if (  ! ((asciicode >= 64 && asciicode <=90) || ( asciicode >= 97 && asciicode <=122)||(asciicode >= 48 && asciicode <=57)))
                return false;
           }
            return true;
        }
		
		function formSubmit() 
		{
		    	    
            var lname=document.getElementById("tb_lname").value;
            
            if (trimAll(lname) == "") {
	            alert("Please specify Last Name");
	            document.getElementById("tb_lname").focus();
	            return false;
            }
            
            if (!isNaN(lname))
            {
                alert("Name should only contains alphabets.");
                return false;
            }           
            
            if(alphanumeric(lname)==false)
            {
                alert("Name should only contains alphabets.");
                return false;
            }	
            var fname=document.getElementById("tb_fname").value;
            
            if (trimAll(fname) == "") 
            {
	            alert("Please specify First Name");
	            document.getElementById("tb_fname").focus();
	            return false;
            }
            
            if (!isNaN(fname))
            {
                alert("Name should only contains alphabets.");
                return false;
            }           
            
            if(alphanumeric(fname)==false)
            {
                alert("Name should only contains alphabets.");
                return false;
            }	

            if (trimAll(document.getElementById("tb_abbrev").value) == "") {
	            alert("Please Users Abbreviation");
	            document.getElementById("tb_abbrev").focus();
	            return  false;
            }

            if (trimAll(document.getElementById("tb_uname").value) == "") {
	            alert("Please specify UserName");
	            document.getElementById("tb_uname").focus();
	            return  false;
            }
            if (CheckName (document.getElementById("tb_uname").value)== false)
            {
                alert("User name must only contain alphabets and numbers");
	            document.getElementById("tb_uname").focus();
                return false;
            }
            
            if (trimAll(document.getElementById("tb_password").value) == "") {
	            alert("Please specify Password");
	            document.getElementById("tb_password").focus();
	            return  false;
            }
            // Noufil 4378 08/06/2008 Allow only alphabets and number in password n user name
            if (CheckName (document.getElementById("tb_password").value)== false)
            {
            
                alert("Password must only contain alphabets and numbers");
	            document.getElementById("tb_password").focus();
                return false;
            }

            if(trimAll(document.getElementById("txt_email").value) == "")
            { 
	            alert("Please specify EmailAddress");
	            document.getElementById("txt_email").focus();
	            return false;
            }
            else
            { 
	           if(isEmail(document.getElementById("txt_email").value)== false)
			   {
			       alert ("Please enter Email Address in Correct format.");
			       document.getElementById("txt_email").focus(); 
			       return false;			   
			   }
            }

            if (document.getElementById("ddl_accesstype").value == 0) {
	            alert("Please specify Access Type");
	            document.getElementById("ddl_accesstype").focus();
	            return  false;
            }

            if( document.getElementById("ddl_accesstype").value == "1")
            {
                if (document.getElementById("ddl_closeout").value == -1) {
	                alert("Please specify Close Out Access Type");
	                document.getElementById("ddl_closeout").focus();
	                return  false;
                }
                //ozair 4812 09/13/2008 validation check implemented
                if(document.getElementById("ddl_closeout").value=="1")
                {
                    if (document.getElementById("tb_NoofDays").value == "") 
                    {
	                    alert("Please enter Close Out Access days");
	                    document.getElementById("tb_NoofDays").focus();
	                    return  false;
                    }
                }                
            }
            if (document.getElementById("ddl_status").value == -1) {
	            alert("Please specify user status");
	            document.getElementById("ddl_status").focus();
	            return  false;
            }

            if (document.getElementById("ddl_spnsearch").value == -1) {
	            alert("Please specify SPN search status");
	            document.getElementById("ddl_spnsearch").focus();
	            return  false;
            }
            else if (document.getElementById("ddl_spnsearch").value == 1)
            {

                            if(document.getElementById("txt_SPNUserName").value == "")
		                    {
		                        alert("Please enter SPN UserName");
	                            document.getElementById("txt_SPNUserName").focus();
	                            return false;
		                    }
            		        
		                    if(document.getElementById("txt_SPNPassword").value == "")
		                    {
		                        alert("Please enter SPN Password");
	                            document.getElementById("txt_SPNPassword").focus();
	                            return false;
		                    }
            }
             var cb = document.getElementById("chkAttorney");
            if (cb.checked)
                {
                   
                    //Nasir 6968 01/22/2010 validate contact number
                     var txtContactID1 = document.getElementById('txt_CC11');
                    var txtContactID2 = document.getElementById('txt_CC12');
                    var txtContactID3 = document.getElementById('txt_CC13');
                    var txtContactID4 = document.getElementById('txt_CC14');
                    
                    var ContactNumber = txtContactID1.value + txtContactID2.value+ txtContactID3.value;
                     if(ContactNumber!="")
                        {
                           if(ContactNumber.length < 10 || isNaN(txtContactID1.value) || isNaN(txtContactID2.value) || isNaN(txtContactID3.value) ||isNaN(txtContactID4.value))
                           {
                            alert("Please Enter Correct Phone Number");
                            txtContactID1.focus();
                            return false;
                           }
                        }       
                }

                return true;
}
//Fahad 5196 11/28/2008 Function to Show or Hide the Firm ddl according to the 
function ShowHideFirmddl()
{
    var cb = document.getElementById("chkAttorney");
       
    if (cb.checked)
    {
 
        document.getElementById("ddl_Firm").style.display = "block";
        document.getElementById("lblFirm").style.display = "block";
        //Nasir 6968 11/23/2009
        document.getElementById("tdAttorCol").colSpan = "2";
        document.getElementById("tdContNum").style.display = "block";
        document.getElementById("tdContactNum").style.display = "block";
    }  
    else if (!cb.checked)
    {
       document.getElementById("ddl_Firm").style.display = "None";
       document.getElementById("lblFirm").style.display = "None";
       document.getElementById("ddl_Firm").Value="3000";
       //Nasir 6968 11/23/2009
       document.getElementById("tdAttorCol").colSpan = "5";
        document.getElementById("tdContNum").style.display = "None";
        document.getElementById("tdContactNum").style.display = "None";
    }
}


	
    </script>

    <div ms_positioning="GridLayout">
        <form id="Form1" method="post" runat="server">
        <table id="TableMain" cellspacing="0" cellpadding="0" width="780" align="center"
            border="0">
            <tbody>
                <tr>
                    <td colspan="4">
                        <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                    </td>
                </tr>
                <tr>
                    <td style="height: 14px" width="100%" background="../Images/separator_repeat.gif"
                        height="14">
                    </td>
                </tr>
                <tr>
                    <td id="pri" colspan="4">
                        <table id="Table1" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
                            <tr>
                                <td class="clsleftpaddingtable" valign="top" colspan="4" style="height: 30px">
                                    <table id="Table2" bordercolor="#ffffff" cellspacing="1" cellpadding="1" width="100%"
                                        border="0">
                                        <tr>
                                            <td class="clssubhead" style="width: 111px; height: 22px">
                                                Last Name
                                            </td>
                                            <td class="clssubhead" style="width: 116px; height: 22px" align="left">
                                                <asp:TextBox ID="tb_lname" runat="server" CssClass="clsinputadministration" Width="104px"
                                                    MaxLength="20"></asp:TextBox>
                                            </td>
                                            <td class="clssubhead" style="width: 96px; height: 22px">
                                                First Name
                                            </td>
                                            <td class="clssubhead" style="width: 152px; height: 22px" align="left">
                                                <asp:TextBox ID="tb_fname" runat="server" CssClass="clsinputadministration" Width="104px"
                                                    MaxLength="20"></asp:TextBox>
                                            </td>
                                            <td class="clssubhead" style="width: 105px; height: 22px">
                                                Short
                                            </td>
                                            <td class="clssubhead" style="height: 22px; width: 136px;">
                                                <asp:TextBox ID="tb_abbrev" runat="server" CssClass="clsinputadministration" Width="112px"
                                                    MaxLength="20"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="clssubhead" style="width: 111px; height: 22px">
                                                User Name
                                            </td>
                                            <td align="left" class="clssubhead" style="width: 116px; height: 22px">
                                                <asp:TextBox ID="tb_uname" runat="server" CssClass="clsinputadministration" Width="104px"
                                                    MaxLength="20"></asp:TextBox>
                                            </td>
                                            <td class="clssubhead" style="width: 96px; height: 22px">
                                                Password
                                            </td>
                                            <td align="left" class="clssubhead" style="width: 152px; height: 22px">
                                                <asp:TextBox ID="tb_password" runat="server" CssClass="clsinputadministration" Width="104px"
                                                    MaxLength="20"></asp:TextBox>
                                            </td>
                                            <td class="clssubhead" style="width: 105px; height: 22px">
                                                Email Address
                                            </td>
                                            <td class="clssubhead" style="height: 22px; width: 136px;">
                                                <asp:TextBox ID="txt_email" runat="server" CssClass="clsinputadministration" Width="112px"
                                                    MaxLength="100"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 111px; height: 22px" class="clssubhead">
                                                NT User ID
                                            </td>
                                            <td style="width: 116px; height: 22px">
                                                <asp:TextBox ID="txt_NTUserID" runat="server" CssClass="clsinputadministration" MaxLength="20"
                                                    Width="104px"></asp:TextBox>
                                            </td>
                                            <td style="width: 96px; height: 22px" class="clssubhead">
                                                Status
                                            </td>
                                            <td style="width: 152px; height: 22px">
                                                <asp:DropDownList ID="ddl_status" runat="server" CssClass="clsinputcombo" Width="104px">
                                                    <asp:ListItem Value="-1">--Choose--</asp:ListItem>
                                                    <asp:ListItem Value="1">Active</asp:ListItem>
                                                    <asp:ListItem Value="0">InActive</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 105px; height: 22px; direction: ltr;" class="clssubhead">
                                                Access Type
                                            </td>
                                            <td style="width: 136px; height: 22px">
                                                <asp:DropDownList ID="ddl_accesstype" runat="server" CssClass="clsinputcombo" Width="112px"
                                                    onchange="return accesstype();">
                                                    <asp:ListItem Value="0">--Choose--</asp:ListItem>
                                                    <asp:ListItem Value="1">Secondary</asp:ListItem>
                                                    <asp:ListItem Value="2">Primary</asp:ListItem>
                                                    <asp:ListItem Value="3">Read Only</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 111px; height: 22px" class="clssubhead">
                                                SPN Search
                                            </td>
                                            <td style="width: 116px; height: 22px">
                                                <asp:DropDownList ID="ddl_spnsearch" runat="server" CssClass="clsinputcombo" Width="104px">
                                                    <asp:ListItem Value="-1">--Choose--</asp:ListItem>
                                                    <asp:ListItem Value="1">Access</asp:ListItem>
                                                    <asp:ListItem Value="0">Not Access</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td id="td_closeout" style="width: 96px; height: 22px" class="clssubhead" runat="server">
                                                Close Out Page
                                            </td>
                                            <td style="width: 152px; height: 22px" runat="server">
                                                <asp:DropDownList ID="ddl_closeout" runat="server" CssClass="clsinputcombo" Width="104px"
                                                    onchange="return showdaysbox();">
                                                    <asp:ListItem Value="-1">--Choose--</asp:ListItem>
                                                    <asp:ListItem Value="1">Access</asp:ListItem>
                                                    <asp:ListItem Value="0">Not Access</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td id="td_nodays" style="width: 105px; height: 22px" class="clssubhead" runat="server">
                                                <span id="lblNoofDaysTitle" runat="server" style="display: block">No of Days</span>
                                            </td>
                                            <td style="width: 136px; height: 22px">
                                                <asp:TextBox ID="tb_NoofDays" runat="server" CssClass="clsinputadministration" MaxLength="20"
                                                    Width="112px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td id="TD2" style="width: 111px; height: 22px" class="clssubhead" runat="server">
                                                <asp:TextBox ID="txt_SPNUserName" runat="server" CssClass="clsinputadministration"
                                                    Width="104px"></asp:TextBox>
                                            </td>
                                            <td id="TD1" style="width: 116px; height: 22px" class="clssubhead" runat="server">
                                                <asp:TextBox ID="txt_SPNPassword" runat="server" CssClass="clsinputadministration"
                                                    Width="104px" Columns="80" MaxLength="80" Wrap="False"></asp:TextBox>
                                            </td>
                                            <td style="width: 186px; height: 22px">
                                                <asp:CheckBox ID="chk_StAdmin" runat="server" CssClass="clssubhead" Text=" Service Ticket Admin" />
                                            </td>
                                            <td style="width: 152px; height: 22px" runat="server">
                                                <asp:CheckBox ID="chkAttorney" runat="server" CssClass="clssubhead" Text=" Is Attorney "
                                                    Width="97px" onclick="ShowHideFirmddl();" />
                                            </td>
                                            <td style="width: 105px; height: 22px" class="clssubhead">
                                                <span id="lblFirm" runat="server" class="clssubhead" style="display: None;">Associated
                                                    Firm</span>
                                            </td>
                                            <td style="width: 136px; height: 22px">
                                                <asp:DropDownList ID="ddl_Firm" runat="server" CssClass="clsinputcombo" Width="112px"
                                                    Style="display: None;">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td id="tdAttorCol" colspan="5" align="right">
                                                <span runat="server" style="display: block"></span>
                                            </td>
                                            <td id="tdContNum" class="Label" style="width: 186px;display:none;">
                                                <span  runat="server" class="clssubhead">Contact Number</span>
                                            </td>
                                            <td id="tdContactNum" colspan="2" style="height: 22px;width:318px;display: None;" >
                                                <asp:TextBox ID="txt_CC11" onkeyup="return autoTab(this, 3, event)" runat="server"
                                                    Width="32px" CssClass="clsInputadministration" TabIndex="14"></asp:TextBox>
                                                -
                                                <asp:TextBox ID="txt_CC12" onkeyup="return autoTab(this, 3, event)" runat="server"
                                                    Width="32px" CssClass="clsInputadministration" TabIndex="15"></asp:TextBox>
                                                -
                                                <asp:TextBox ID="txt_CC13" onkeyup="return autoTab(this, 4, event)" runat="server"
                                                    Width="32px" CssClass="clsInputadministration" TabIndex="16"></asp:TextBox>
                                                x
                                                <asp:TextBox ID="txt_CC14" onkeyup="return autoTab(this, 4, event)" runat="server"
                                                    Width="40px" CssClass="clsInputadministration" TabIndex="17"></asp:TextBox>
                                            </td>
                                            
                                            <td style="width: 136px; height: 22px">
                                                <asp:Button ID="btn_update" runat="server" CssClass="clsbutton" Text="Update" OnClick="btn_update_Click1">
                                                </asp:Button>
                                                <asp:Button ID="btn_clear" runat="server" CssClass="clsbutton" Width="48px" Text="Clear">
                                                </asp:Button>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="clsLeftPaddingTable" style="height: 16px" background="../Images/separator_repeat.gif">
                                </td>
                            </tr>
                            <tr>
                                <td class="clssubhead" valign="middle" align="left" background="../Images/subhead_bg.gif"
                                    colspan="4">
                                    <table width="100%">
                                        <tr>
                                            <td class="clssubhead" style="width: 449px;" valign="middle">
                                                Results
                                            </td>
                                            <td>
                                                <asp:RadioButtonList ID="rbl_status" runat="server" CssClass="clssubhead" RepeatDirection="Horizontal"
                                                    AutoPostBack="True" OnSelectedIndexChanged="rbl_status_SelectedIndexChanged">
                                                    <asp:ListItem Value="1" Selected="True">&lt;span class=&quot;clssubhead&quot; &gt;Active Users &lt;/span&gt;</asp:ListItem>
                                                    <asp:ListItem Value="0">&lt;span class=&quot;clssubhead&quot; &gt;Inactive Users&lt;/span&gt;</asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                            <td style="width: 100px;" valign="middle" align="right">
                                                <asp:DropDownList ID="ddl_sort" runat="server" CssClass="clsinputcombo" Width="88px"
                                                    AutoPostBack="True" OnSelectedIndexChanged="ddl_sort_SelectedIndexChanged">
                                                    <asp:ListItem Value="lastname">Sort By</asp:ListItem>
                                                    <asp:ListItem Value="LastName">Last Name</asp:ListItem>
                                                    <asp:ListItem Value="AccessType">AccessType</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" colspan="4">
                                    <asp:DataGrid ID="dg_results" runat="server" CssClass="clsleftpaddingtable" Width="100%"
                                        BorderColor="White" AutoGenerateColumns="False">
                                        <Columns>
                                            <asp:TemplateColumn HeaderText="Last Name">
                                                <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="hlnk_lname" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.lastname") %>'
                                                        Visible="False">
                                                    </asp:HyperLink>
                                                    <asp:LinkButton ID="lnkbtn_lname" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.lastname") %>'
                                                        CommandName="click">
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="First Name">
                                                <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_fname" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.firstname") %>'>
                                                    </asp:Label>
                                                    <asp:Label ID="lbl_eid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.employeeid") %>'
                                                        Visible="False">
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Short">
                                                <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_abbrev" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.abbreviation") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="User Name">
                                                <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_uname" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.username") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Access Type">
                                                <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_accesstype" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.access") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Close Out Page">
                                                <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_Canview" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.CanUpdateCloseOutLog") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="SPN Search">
                                                <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CanSPNSearch" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.CanSPNSearch") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="NTUserID">
                                                <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_NTUserID" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.NTUserID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Status">
                                                <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_status" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Status") %>'
                                                        CssClass="label"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Attorney">
                                                <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_Attorney" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.IsAttorney") %>'
                                                        CssClass="label"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Access ST">
                                                <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_stAdmin" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AccessST") %>'
                                                        CssClass="label"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </td>
                            </tr>
                            <tr>
                                <td width="100%" background="../Images/separator_repeat.gif" height="11">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 760px" align="center">
                                    <asp:Label ID="lblMessage" runat="server" Width="538px" ForeColor="Red" Font-Size="X-Small"
                                        Font-Names="Arial"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 760px" align="left" colspan="4">
                                    <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                                    <asp:Label ID="lblRecCount" runat="server" ForeColor="White"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
        </form>
    </div>
</body>--%>
</html>

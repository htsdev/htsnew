<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Page language="c#" AutoEventWireup="false" Inherits="HTP.Backroom.ExchangeDocket" Codebehind="ExchangeDocket.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<title>Exchange Docket</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<SCRIPT src="../Scripts/dates.js" type="text/javascript"></SCRIPT>
		<script language="javascript">
		function validate()
		{
			//a;
			var d1 = document.getElementById("calQueryFrom").value
			var d2 = document.getElementById("calqueryTo").value			
			
			if (d1 == d2)
				return true;
				
			if (compareDates(d2,'MM/dd/yyyy',d1,'MM/dd/yyyy')==false)
				{
					alert("Please Enter start which is less than end date");
					document.getElementById("calqueryTo").focus(); 
					return false;
				}
				return true;
		}
	</script>
</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE id="TableMain" style="Z-INDEX: 101" cellSpacing="0" cellPadding="0" width="780"
				align="center" border="0">
				<tbody>
					<tr>
						<td style="HEIGHT: 14px" colSpan="4"><uc1:activemenu id="ActiveMenu1" runat="server"></uc1:activemenu></td>
					</tr>
					<TR>
						<TD>
							<TABLE id="TableSub" cellSpacing="0" cellPadding="0" width="100%" border="0">
								<!--<tr vAlign="middle">
									<td><IMG height="18" src="../Images/head_icon.gif" width="25">&nbsp; <STRONG><font color="#3366cc">
												Exchange Docket </font></STRONG>
									</td>
								</tr>-->
								<tr>
									<td background="../../images/separator_repeat.gif" height="11"></td>
								</tr>
								<TR>
									<TD align="center">
										<TABLE id="Table6" cellSpacing="0" cellPadding="0" width="780" bgColor="white" border="0">
											<tr>
												<TD width="30%" height="22">&nbsp;
													<ew:calendarpopup id="calQueryFrom" runat="server" SelectedDate="2005-09-01"
														ToolTip="Select Report Date Range" PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00"
														Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Bottom"
														ControlDisplay="TextBoxImage" Font-Size="8pt" Font-Names="Tahoma" ImageUrl="../images/calendar.gif"
														Width="90px" Nullable="True">
														<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
														<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></WeekdayStyle>
														<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="Yellow"></MonthHeaderStyle>
														<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
															BackColor="AntiqueWhite"></OffMonthStyle>
														<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></GoToTodayStyle>
														<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="LightGoldenrodYellow"></TodayDayStyle>
														<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="Orange"></DayHeaderStyle>
														<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="LightGray"></WeekendStyle>
														<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="Yellow"></SelectedDateStyle>
														<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></ClearDateStyle>
														<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></HolidayStyle>
													</ew:calendarpopup>-
													<ew:calendarpopup id="calqueryTo" runat="server" SelectedDate="2005-09-01"
														ToolTip="Select Report Date Range" PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00"
														Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Bottom"
														ControlDisplay="TextBoxImage" Font-Size="8pt" Font-Names="Tahoma" ImageUrl="../images/calendar.gif"
														Width="90px" Nullable="True">
														<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
														<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></WeekdayStyle>
														<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="Yellow"></MonthHeaderStyle>
														<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
															BackColor="AntiqueWhite"></OffMonthStyle>
														<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></GoToTodayStyle>
														<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="LightGoldenrodYellow"></TodayDayStyle>
														<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="Orange"></DayHeaderStyle>
														<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="LightGray"></WeekendStyle>
														<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="Yellow"></SelectedDateStyle>
														<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></ClearDateStyle>
														<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></HolidayStyle>
													</ew:calendarpopup></TD>
												<TD width="10%" height="22"><asp:dropdownlist id="ddl_attorney" runat="server" CssClass="clsinputcombo"></asp:dropdownlist>&nbsp;</TD>
												<TD align="left" width="10%" height="22"><asp:button id="btn_submit" runat="server" CssClass="clsbutton" Text="Submit"></asp:button></TD>
												<td align="right" width="39%" height="22"><asp:label id="lblCurrPage" runat="server" Font-Size="Smaller" Width="83px" CssClass="cmdlinks"
														Height="8px" Font-Bold="True" ForeColor="#3366cc">Current Page :</asp:label><asp:label id="lblPNo" runat="server" Font-Size="Smaller" Width="9px" CssClass="cmdlinks" Height="10px"
														Font-Bold="True" ForeColor="#3366cc">a</asp:label>&nbsp;<asp:label id="lblGoto" runat="server" Font-Size="Smaller" Width="16px" CssClass="cmdlinks"
														Height="7px" Font-Bold="True" ForeColor="#3366cc">Goto</asp:label>&nbsp;<asp:dropdownlist id="cmbPageNo" runat="server" Font-Size="Smaller" CssClass="clinputcombo" Font-Bold="True"
														ForeColor="#3366cc" AutoPostBack="True"></asp:dropdownlist>
												</td>
											</tr>
											<tr>
												<td background="../../images/separator_repeat.gif" colSpan="6" height="11"></td>
											</tr>
										</TABLE>
										<asp:label id="lbl_message" runat="server" CssClass="label" ForeColor="Red"></asp:label></TD>
								</TR>
								<TR>
									<TD>
										<TABLE id="TableGrid" cellSpacing="0" cellPadding="0" width="100%" bgColor="white" border="0">
											<tr>
												<td style="HEIGHT: 136px" vAlign="top" align="center" colSpan="2" height="136"><asp:datagrid id="dg_valrep" runat="server" Width="100%" CssClass="clsLeftPaddingTable" PageSize="20"
														AllowPaging="True" BackColor="#EFF4FB" AutoGenerateColumns="False" BorderColor="White" BorderStyle="None">
<Columns>
<asp:TemplateColumn HeaderText="S No.">
<HeaderStyle CssClass="clsaspcolumnheader">
</HeaderStyle>

<ItemStyle VerticalAlign="Top">
</ItemStyle>

<ItemTemplate>
																	<asp:Label id="lbl_Sno" runat="server" CssClass="label"></asp:Label>
																
</ItemTemplate>
</asp:TemplateColumn>
<asp:TemplateColumn HeaderText="Client Name">
<HeaderStyle CssClass="clsaspcolumnheader">
</HeaderStyle>

<ItemStyle VerticalAlign="Top">
</ItemStyle>

<ItemTemplate>
																	<asp:HyperLink id=hnk_clientname runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.clientname") %>'>
																	</asp:HyperLink>
																
</ItemTemplate>
</asp:TemplateColumn>
<asp:TemplateColumn HeaderText="Whose Client">
<HeaderStyle CssClass="clsaspcolumnheader">
</HeaderStyle>

<ItemStyle VerticalAlign="Top">
</ItemStyle>

<ItemTemplate>
																	<asp:Label id=lbl_upload runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.firmname") %>' CssClass="Label">
																	</asp:Label>
																
</ItemTemplate>
</asp:TemplateColumn>
<asp:TemplateColumn HeaderText="Hire Date">
<HeaderStyle CssClass="clsaspcolumnheader">
</HeaderStyle>

<ItemStyle VerticalAlign="Top">
</ItemStyle>

<ItemTemplate>
																	<asp:Label id=lbl_adescription runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.clientdate") %>' CssClass="Label">
																	</asp:Label>
																
</ItemTemplate>
</asp:TemplateColumn>
<asp:TemplateColumn HeaderText="Location">
<HeaderStyle CssClass="clsaspcolumnheader">
</HeaderStyle>

<ItemStyle VerticalAlign="Top">
</ItemStyle>

<ItemTemplate>
																	<asp:Label id=lbl_acourtname runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.shortcourtname") %>' CssClass="Label">
																	</asp:Label>
																
</ItemTemplate>
</asp:TemplateColumn>
<asp:TemplateColumn HeaderText="Coverage">
<HeaderStyle CssClass="clsaspcolumnheader">
</HeaderStyle>

<ItemTemplate>
																	<asp:Label id=lbl_coverage runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.coveringfirm") %>' CssClass="Label">
																	</asp:Label>
																
</ItemTemplate>
</asp:TemplateColumn>
<asp:TemplateColumn HeaderText="Fee">
<HeaderStyle HorizontalAlign="Right" CssClass="clsaspcolumnheader">
</HeaderStyle>

<ItemStyle HorizontalAlign="Right">
</ItemStyle>

<ItemTemplate>
<asp:Label id=lbl_fee runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.TotalFeeCharged","{0:C0}") %>'></asp:Label>
</ItemTemplate>
</asp:TemplateColumn>
</Columns>

<PagerStyle VerticalAlign="Middle" NextPageText="   Next &gt;" PrevPageText="  &lt; Previous        " HorizontalAlign="Center">
</PagerStyle>
													</asp:datagrid></td>
											</tr>
											<tr>
												<td width="780" background="../../images/separator_repeat.gif"  height="11"></td>
											</tr>
											<TR>
												<TD ><uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
							</TABLE>
						</TD>
					</TR>
				</tbody>
			</TABLE>
		</form>
	</body>
</HTML>

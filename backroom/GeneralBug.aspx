<%@ Page language="c#" Codebehind="GeneralBug.aspx.cs" AutoEventWireup="True" Inherits="lntechNew.backroom.GeneralBug" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="~/WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>GeneralBug</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript">
		function Validation()
		{ 
			var txtShortDesc=document.getElementById("txt_shortdesc").value;
			
			
			if(txtShortDesc == "")
			{ 
				alert("PLease Enter Short Description");
				document.getElementById("txt_shortdesc").focus();
				return false;
			}
			
		}
		</script>

        <link href="../Styles.css" rel="stylesheet" type="text/css" />
	</HEAD>
	<body bottomMargin="0" topMargin="0" MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE id="TableMain" cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
			<tr>
			    <td>
			    
			    <uc1:activemenu id="ActiveMenu1" runat="server"></uc1:activemenu>
			</td>
			
			
			</tr>
			<TR>
								<TD background="../../images/separator_repeat.gif" Height="11" ></TD>
							</TR>
				<TR>
					
					<td>
					
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
					
					<tr>
					<TD class="clssubhead" background="../Images/subhead_bg.gif" height="34" colspan="2">
					    <table border="0" cellpadding="0" cellspacing="0" width="100%">
					    <tr>
					    <td class="clssubhead">
						&nbsp;Online Issues Submission
						</td>
						<td align="right">
						<asp:hyperlink id="Hyperlink1" Runat="server" NavigateUrl="ViewBugs.aspx">Developers Page</asp:hyperlink>&nbsp;<span style="color:RoyalBlue">|</span>&nbsp;<asp:hyperlink id="hp_Viewissues" Runat="server" NavigateUrl="GeneralView.aspx">View Bugs</asp:hyperlink>&nbsp;
						</td>
						</tr>
						</table>
					</TD>
					
			
					
					</tr>
					
					</table>
				</td>
						
				</TR>
				<TR>
					<TD vAlign="top" colSpan="2" style="height: 302px">
						<TABLE id="tbl1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD align="right" width="50%" class="clsLeftPaddingTable" style="height: 19px">Short Description&nbsp;</TD>
								<TD class="clsLeftPaddingTable"  style="height: 19px"><asp:textbox id="txt_shortdesc" runat="server" Width="252px" CssClass="clstextarea" MaxLength="100"></asp:textbox>
                                    &nbsp; * </TD>
							</TR>
							<TR>
								<TD align="right" class="clsLeftPaddingTable">Priority&nbsp;</TD>
								<TD class="clsLeftPaddingTable"><asp:dropdownlist id="ddl_priority" runat="server" Width="80px"></asp:dropdownlist>&nbsp;&nbsp;Status
									<asp:dropdownlist id="ddl_status" runat="server" Width="135px"></asp:dropdownlist>&nbsp;</TD>
							</TR>
							<TR>
								<TD align="right" class="clsLeftPaddingTable">Page Url&nbsp;</TD>
								<TD class="clsLeftPaddingTable"><asp:textbox id="txt_pageurl" runat="server" Width="252px" CssClass="clstextarea" MaxLength="100"></asp:textbox></TD>
							</TR>
							<TR>
								<TD align="right" class="clsLeftPaddingTable">
                                    Ticket Number&nbsp;</TD>
								<TD class="clsLeftPaddingTable"><asp:textbox id="txt_ticketno" runat="server" Width="96px" MaxLength="15" CssClass="clstextarea"></asp:textbox>
                                    Cause No
                                    <asp:TextBox ID="txtCauseNo" runat="server" CssClass="clstextarea" MaxLength="15" Width="103px"></asp:TextBox></TD>
							</TR>
							<TR>
								<TD class="clsLeftPaddingTable" align="right">PLease Enter the Bug 
									Description&nbsp;
								</TD>
								<TD class="clsLeftPaddingTable"><asp:textbox id="txt_Desc" runat="server" Width="252px" CssClass="clstextarea" Height="65px"
										TextMode="MultiLine" MaxLength="1000"></asp:textbox></TD>
							</TR>
							<TR>
								<TD class="clsLeftPaddingTable" align="right">Upload File&nbsp;</TD>
								<TD class="clsLeftPaddingTable"><INPUT id="fp_file" type="file" name="File1" runat="server" style="width: 252px" class="clstextarea"></TD>
							</TR>
							<TR>
								<TD class="clsLeftPaddingTable" align="right">File Description&nbsp;
								</TD>
								<TD class="clsLeftPaddingTable"><asp:textbox id="txt_filedesc" runat="server" Width="252px" MaxLength="100" CssClass="clstextarea"></asp:textbox></TD>
							</TR>
							<tr>
							<TD class="clsLeftPaddingTable" align="right" style="height:5px"></TD>
							<TD class="clsLeftPaddingTable" align="right" style="height:5px"></TD>
							</tr>
							<TR>
								<TD class="clsLeftPaddingTable" align="right"></TD>
								<TD class="clsLeftPaddingTable" align="left">
                                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                    &nbsp; &nbsp;&nbsp;
                                    <asp:button id="btnSubmit" runat="server" Text="Submit" CssClass="clsButton" onclick="btnSubmit_Click"></asp:button></TD>
							</TR>
							<TR>
								<TD class="clsLeftPaddingTable" align="center" colSpan="2"><asp:label id="lblMessage" runat="server" ForeColor="Red"></asp:label></TD>
							</TR>
							<tr>
									<td background="../../images/separator_repeat.gif" colSpan="6" height="11"></td>
								</tr>
								<tr>
									<TD colSpan="5"><uc1:footer id="Footer1" runat="server"></uc1:footer></td>
								</tr>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
			</form>
	</body>
</HTML>

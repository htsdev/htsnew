<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Page language="c#" AutoEventWireup="false" Inherits="SPUtility.WebForm1" Codebehind="Search_SP.aspx.cs" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<title>Search Procedures</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
		<SCRIPT src="../Scripts/dates.js" type="text/javascript"></SCRIPT>
		<script language="javascript">
		function validate()
		{
			//a;
			var d1 = document.getElementById("cal_FromDate").value
			var d2 = document.getElementById("cal_ToDate").value			
			
			if (d1 == d2)
				return true;
			if (compareDates(d2,'MM/dd/yyyy',d1,'MM/dd/yyyy')==false)
				{
					alert("start date cannot greater than end date");
					document.getElementById("cal_ToDate").focus(); 
					return false;
				}
				return true;
		}
	</script>
  </HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
			<table  cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
			<tbody>
				<tr>
						<td style="HEIGHT: 14px" colSpan="4">
						<uc1:activemenu id="ActiveMenu1" runat="server"></uc1:activemenu>
						</td>
				</tr>		
				<TR>
				<td>    <TABLE style="width: 775px">
				            <tr>
				
								<TD class="clsLeftPaddingTable" style="HEIGHT: 21px">&nbsp;<strong>From :</strong>
								</TD>
								<TD class="clsLeftPaddingTable" style="HEIGHT: 21px; width: 284px;"><ew:calendarpopup id="cal_FromDate" runat="server" EnableHideDropDown="True" ShowGoToToday="True"
										ControlDisplay="TextBoxImage" PadSingleDigits="True" Width="139px" ImageUrl="../Images/calendar.gif" ShowClearDate="True" SelectedDate="2006-06-19">
										<TextboxLabelStyle CssClass="clsinputadministration"></TextboxLabelStyle>
										<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></WeekdayStyle>
										<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></MonthHeaderStyle>
										<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
											BackColor="AntiqueWhite"></OffMonthStyle>
										<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></GoToTodayStyle>
										<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGoldenrodYellow"></TodayDayStyle>
										<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Orange"></DayHeaderStyle>
										<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGray"></WeekendStyle>
										<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></SelectedDateStyle>
										<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></ClearDateStyle>
										<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></HolidayStyle>
									</ew:calendarpopup>
									</TD>
								<TD class="clsLeftPaddingTable" style="HEIGHT: 21px">&nbsp;<strong>To :</strong></TD>
								<TD class="clsLeftPaddingTable" style="HEIGHT: 21px; width: 270px;"><ew:calendarpopup id="cal_ToDate" runat="server" EnableHideDropDown="True" ShowGoToToday="True" ControlDisplay="TextBoxImage"
										PadSingleDigits="True" Width="139px" ImageUrl="../Images/calendar.gif" SelectedDate="2006-06-19">
										<TextboxLabelStyle CssClass="clsinputadministration"></TextboxLabelStyle>
										<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></WeekdayStyle>
										<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></MonthHeaderStyle>
										<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
											BackColor="AntiqueWhite"></OffMonthStyle>
										<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></GoToTodayStyle>
										<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGoldenrodYellow"></TodayDayStyle>
										<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Orange"></DayHeaderStyle>
										<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGray"></WeekendStyle>
										<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></SelectedDateStyle>
										<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></ClearDateStyle>
										<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></HolidayStyle>
									</ew:calendarpopup></TD>
							
							
								
							</tr>
							<TR>
								<TD class="clsLeftPaddingTable">&nbsp;<strong>Stored Procedure :</strong>
								</TD>
								<td class="clsLeftPaddingTable" style="width: 284px"><asp:textbox id="txt_SP" runat="server" Width="139px" CssClass="clsinputadministration"></asp:textbox></td>
								<td class="clsLeftPaddingTable">&nbsp;<strong>Users :</strong>
								</td>
								<td class="clsLeftPaddingTable" style="width: 270px"><asp:dropdownlist id="ddl_User" runat="server" Width="139px" CssClass="clsinputcombo"></asp:dropdownlist></td>
							</TR>
							
							<tr>
								<td class="clsLeftPaddingTable"></td>
								<td class="clsLeftPaddingTable" style="width: 284px"></td>
								<td class="clsLeftPaddingTable"></td>
								<td class="clsLeftPaddingTable" style="width: 270px"><asp:button id="btn_Search" runat="server" CssClass="clsbutton" Text="Search"></asp:button></td>
							</tr>
							</TABLE>
							</td>
							</TR>
							
							<tr>
								<td class="clsLeftPaddingTable" align="center" colSpan="4"><asp:label id="lbl_Msg" runat="server" CssClass="normalfont" ForeColor="Red"></asp:label></td>
							<TR>
								<td colspan=4></td>
							</TR>
							
					
						
						
							<tr>
								<TD align="right"><asp:datagrid id="DG_SP" runat="server" Width="100%" AllowPaging="True" Font-Names="Verdana" Font-Size="2px"
										AutoGenerateColumns="False" PageSize="30">
										<EditItemStyle BackColor="White"></EditItemStyle>
										<AlternatingItemStyle BackColor="#EEEEEE"></AlternatingItemStyle>
										<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle"></HeaderStyle>
										<FooterStyle CssClass="GrdFooter"></FooterStyle>
										<Columns>
											<asp:TemplateColumn HeaderText="SNO">
												<HeaderStyle HorizontalAlign="Left" Width="5%" CssClass="clssubhead"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id="lbl_Sno" runat="server" CssClass="label"></asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Date">
												<HeaderStyle HorizontalAlign="Left" Width="20%" CssClass="clssubhead"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id=lbl_Date runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.Date", "{0:D}") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn SortExpression="Received Date" HeaderText="Stored Procedure Name">
												<HeaderStyle HorizontalAlign="Left" Width="25%" CssClass="clssubhead"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id=lbl_SPName runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.SPName") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Time">
												<HeaderStyle HorizontalAlign="Left" Width="5%" CssClass="clssubhead"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id=lbl_Time runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.Date","{0:t}") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="View">
												<HeaderStyle HorizontalAlign="Left" Width="5%" CssClass="clssubhead"></HeaderStyle>
												<ItemTemplate>
													<asp:ImageButton id="img_view" runat="server" ImageUrl="../Images/preview.gif" CommandName="view"></asp:ImageButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn Visible="False" HeaderText="SID">
												<ItemTemplate>
													<asp:Label id=lbl_SID runat="server" CssClass="smallfont" Text='<%# DataBinder.Eval(Container, "Dataitem.SourceSafeID") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
										<PagerStyle NextPageText=" Next &amp;gt;" PrevPageText="&amp;lt; Previous " HorizontalAlign="Center"
											CssClass="smallfont"></PagerStyle>
									</asp:datagrid></TD>
							</tr>
						
					
				
				
				</tbody>
			</table>
			</form>
	</body>
</HTML>

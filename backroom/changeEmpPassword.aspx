﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="changeEmpPassword.aspx.cs"
    Inherits="lntechDallasNew.backroom.changeEmpPassword" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Change Password</title>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS TEMPLATE - END -->
    <link href="../Styles.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
        function CheckName(name) {
            for (i = 0 ; i < name.length ; i++) {
                var asciicode = name.charCodeAt(i)
                //If not valid alphabet 
                if (!((asciicode >= 64 && asciicode <= 90) || (asciicode >= 97 && asciicode <= 122) || (asciicode >= 48 && asciicode <= 57)))
                    return false;
            }
            return true;
        }

        function Checkpasswordlenght() {

            var Plenght = document.getElementById("txt_password").value.length;
            var nPlenght = document.getElementById("txt_newpassword").value.length;
            var nPvalue = document.getElementById("txt_newpassword").value;
            var cvalue = document.getElementById("txt_confrimpasswrod").value;

            if (Plenght == 0) {
                alert("Please enter all Fields");
                document.getElementById("txt_password").focus();
                return false;
            }

            if (nPlenght == 0) {
                alert("Please enter all Fields");
                document.getElementById("txt_newpassword").focus();
                return false;
            }

            if (document.getElementById("txt_confrimpasswrod").value.length == 0) {
                alert("Please enter all Fields");
                document.getElementById("txt_confrimpasswrod").focus();
                return false;
            }

            if (Plenght < 4 || Plenght > 20) {
                alert("Please enter old password greater then 3 and less then 20 digits");
                document.getElementById("txt_password").focus();
                return false;
            }

            if (nPlenght < 4 || nPlenght > 20) {
                alert("Please enter new password greater then 3 and less then 20 digits");
                document.getElementById("txt_newpassword").focus();
                return false;
            }

            if (nPvalue != cvalue) {
                alert("Both passwords are not same");
                document.getElementById("txt_confrimpasswrod").focus();
                return false;
            }

            if (CheckName(document.getElementById("txt_password").value) == false) {
                alert("Old password must only contain alphabets and numbers");
                document.getElementById("txt_password").focus();
                return false;
            }

            if (CheckName(nPvalue) == false) {
                alert("New password must only contain alphabets and numbers");
                document.getElementById("txt_newpassword").focus();
                return false;
            }

            if (CheckName(cvalue) == false) {
                alert("Confrim new password must only contain alphabets and numbers");
                document.getElementById("txt_confrimpasswrod").focus();
                return false;
            }


        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <section id="main-content-popup" class="">
                <section class="wrapper main-wrapper row changePasswordCustom" style="">
                    <div class="col-xs-12">
                         <div class="page-title" style="display:none;">
                
                              <h1 class="title">Change Password</h1>
                    
              
              
            
                        </div>

                      </div>
                    <div class="clearfix"></div>
                    <div class="col-xs-12">
                         <section class="box">
                        <header class="panel_header">
                            <h2 class="title pull-left">Change Password</h2>

                            <div class="actions panel_actions remove-absolute pull-right">
                       
                	            <a class="box_toggle fa fa-chevron-down"></a>
                            </div>
                      </header>
                        <div class="content-body">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Old Password</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                      <asp:TextBox ID="txt_password" runat="server" CssClass="form-control" Width="150px"
                                        Text="" TextMode="Password" MaxLength="20"></asp:TextBox>
                                                              
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">New Password</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <asp:TextBox ID="txt_newpassword" runat="server" CssClass="form-control"
                                        Width="150px" Text="" TextMode="Password" MaxLength="20"></asp:TextBox>
                                                              
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Confirm Password</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                          <asp:TextBox ID="txt_confrimpasswrod" runat="server" CssClass="form-control"
                                        Width="150px" Text="" TextMode="Password" MaxLength="20"></asp:TextBox>
                                                              
                                    </div>
                                </div>
                            </div>
                             <hr />
                            <div class="clearfix"></div>
                             <asp:Button runat="server" ID="btn_Submit" Text="Submit" CssClass="btn btn-primary" ValidationGroup="RFV"
                                        OnClick="btn_Submit_Click" OnClientClick="return Checkpasswordlenght();" />
                            </div>
                                
                                
                </section>
                     </div>
                    </section>
        </section>
        <div>
        </div>
    </form>
    <!-- CORE JS FRAMEWORK - START -->
    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE JS FRAMEWORK - END -->

    <!-- CORE TEMPLATE JS - START -->
    <script src="../assets/js/scripts.js" type="text/javascript"></script>
    <!-- END CORE TEMPLATE JS - END -->
</body>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoggingReport.aspx.cs"
    Inherits="HTP.backroom.LoggingReport" MaintainScrollPositionOnPostback="true" %>

<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="obout" Namespace="OboutInc.Flyout2" Assembly="obout_Flyout2_NET" %>
<%@ Register Src="~/WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Logging Report</title>
    <link href="../Styles.css" type="text/css" rel="stylesheet" />

    <script language="javascript" type="text/javascript">
        function ValidateDate()
        {
            var wed= document.getElementById("CalFrom");
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp35:ScriptManager ID="ScriptManager1" runat="server">
            <Scripts>
                <asp35:ScriptReference Name="MicrosoftAjax.js" Path="~/Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
            </Scripts>
        </asp35:ScriptManager>
        <asp35:UpdatePanel ID="PnlMain" runat="server">
            <ContentTemplate>
                <table cellpadding="0" cellspacing="0" width="980px" align="center" border="0">
                    <tr>
                        <td style="width: 100%">
                            <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100%" background="../Images/separator_repeat.gif" height="11">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100%" align="center">
                            <asp:RegularExpressionValidator ID="reExceptionId" runat="server" ControlToValidate="TxtExceptionId"
                                Display="Dynamic" ValidationExpression="^[0-9]*$" ErrorMessage="Exception Id accepts only integer value."></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="clsLeftPaddingTable" align="center">
                            <table>
                                <tr>
                                    <td style="width: 80px">
                                        <span class="clssubhead">Start Date :</span>
                                    </td>
                                    <td align="left" style="width: 100px" class="clsLeftPaddingTable">
                                        <ew:CalendarPopup Visible="true" ID="CalFrom" runat="server" AllowArbitraryText="False"
                                            CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Culture="(Default)" EnableHideDropDown="True"
                                            Font-Names="Tahoma" Font-Size="6pt" ImageUrl="/images/calendar.gif" Nullable="True"
                                            PadSingleDigits="True" ShowClearDate="True" ShowGoToToday="True" Text=" " ToolTip="Date"
                                            UpperBoundDate="9999-12-29" Width="65px">
                                            <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Black" />
                                            <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Gray" />
                                            <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Black" />
                                            <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Black" />
                                            <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                        </ew:CalendarPopup>
                                    </td>
                                    <td style="width: 70px">
                                        <span class="clssubhead">End Date :</span>
                                    </td>
                                    <td align="left" style="width: 100px">
                                        <ew:CalendarPopup Visible="true" ID="CalTo" runat="server" AllowArbitraryText="False"
                                            CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Culture="(Default)" EnableHideDropDown="True"
                                            Font-Names="Tahoma" Font-Size="6pt" ImageUrl="/images/calendar.gif" Nullable="True"
                                            PadSingleDigits="True" ShowClearDate="True" ShowGoToToday="True" Text=" " ToolTip="Date"
                                            UpperBoundDate="9999-12-29" Width="65px">
                                            <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Black" />
                                            <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Gray" />
                                            <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Black" />
                                            <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Black" />
                                            <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                        </ew:CalendarPopup>
                                    </td>
                                    <td align="left" style="width: 60px">
                                        <span class="clssubhead">Severity :</span>
                                    </td>
                                    <td align="left" style="width: 140px">
                                        <asp:DropDownList ID="ddSeverity" CssClass="clsInputCombo" runat="server" Width="120px">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" style="width: 82px">
                                        <span class="clssubhead">Application : </span>
                                    </td>
                                    <td align="left" style="width: 200px">
                                        <asp:DropDownList ID="ddApplications" CssClass="clsInputCombo" runat="server" DataTextField="ApplicationName"
                                            DataValueField="ApplicationName" Width="180px">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="right" style="width: 80px">
                                        <asp:CheckBox ID="ChkShowAll" CssClass="clssubhead" runat="server" Text="Show All" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6">
                                    </td>
                                    <td align="left" style="width: 82px">
                                        <span class="clssubhead">Exception Id : </span>
                                    </td>
                                    <td align="left" style="width: 200px">
                                        <asp:TextBox ID="TxtExceptionId" CssClass="clsInputadministration" runat="server"
                                            Width="175px"></asp:TextBox>
                                    </td>
                                    <td align="right">
                                        <asp:Button ID="BtnSearch" CssClass="clsbutton" runat="server" Text="Search" OnClick="BtnSearch_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100%" background="../Images/separator_repeat.gif" height="11">
                        </td>
                    </tr>
                    <tr>
                        <td background="../Images/subhead_bg.gif" height="34" class="clssubhead" align="right">
                            <uc3:PagingControl ID="Pagingctrl" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100%" background="../Images/separator_repeat.gif" height="11">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100%" align="center">
                            <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="width: 100%">
                            <asp35:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="1" DynamicLayout="true"
                                AssociatedUpdatePanelID="PnlMain">
                                <ProgressTemplate>
                                    <img alt="" src="../images/plzwait.gif" />&nbsp;
                                    <asp:Label ID="Lblplzwait" runat="server" CssClass="clssubhead" Text="Please Wait ..."></asp:Label>
                                </ProgressTemplate>
                            </asp35:UpdateProgress>
                            <asp:GridView ID="gvException" runat="server" AutoGenerateColumns="False" Width="980px"
                                CssClass="clsLeftPaddingTable" AllowSorting="True" AllowPaging="True" PageSize="20"
                                OnPageIndexChanging="gvException_PageIndexChanging" OnRowDataBound="gvException_RowDataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="Exception Id" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="LblExceptionId" CssClass="clsLeftPaddingTable" runat="server" Text='<%# Eval("ExceptionId") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date/Time" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="130px">
                                        <ItemTemplate>
                                            <asp:Label ID="LblDateTime" runat="server" CssClass="clsLeftPaddingTable" Text='<%# Eval("Logging.DateTime","{0: MM/dd/yyyy} @ {0:hh:mm tt}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Message" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="LblTitle" runat="server" CssClass="clsLeftPaddingTable" Text='<%#  Convert.ToString(Eval("Logging.Title")).Trim() %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Severity" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="LblSeverity" runat="server" CssClass="clsLeftPaddingTable" Text='<%# Eval("Logging.Severity") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="clssubhead" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Application" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="LblApplicationName" runat="server" CssClass="clsLeftPaddingTable"
                                                Text='<%# Eval("ApplicationName.ApplicationName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="IP Address" HeaderStyle-CssClass="clssubhead" ItemStyle-HorizontalAlign="Left"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="LblIpAddress" runat="server" CssClass="clsLeftPaddingTable" Text='<%# Eval("IpAddress") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Machine Name" HeaderStyle-CssClass="clssubhead" ItemStyle-HorizontalAlign="Left"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="LblMachineName" runat="server" CssClass="clsLeftPaddingTable" Text='<%# Eval("MachineName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Stack Trace" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="HfComments" runat="server" Value='<%# Eval("StackTrace") %>' />
                                            <asp:Image ID="ImgComments" ImageUrl="~/Images/comments.png" runat="server" />
                                            <asp:Label ID="LblStack" runat="server" CssClass="clsLeftPaddingTable" Text="N/A"></asp:Label>
                                            <obout:Flyout runat="server" ID="ToolTipComments" AttachTo="ImgComments" Align="TOP"
                                                FadingEffect="false" FlyingEffect="BOTTOM_LEFT" Position="TOP_LEFT">
                                                <asp:Panel ID="PnlTooltipComments" HorizontalAlign="Center" runat="server" CssClass="QuickLinksFlyoutPopup"
                                                    Height="200px" Width="420px" ScrollBars="Auto">
                                                </asp:Panel>
                                            </obout:Flyout>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Other Information" HeaderStyle-CssClass="clssubhead"
                                        HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="HfSession" runat="server" />
                                            <asp:HiddenField ID="HfCookie" runat="server" />
                                            <asp:HiddenField ID="HfApplication" runat="server" />
                                            <asp:Label ID="LblOtherInfo" runat="server" CssClass="clsLeftPaddingTable" Text="N/A"></asp:Label>
                                            <asp:Image ID="ImgSession" ImageUrl="~/Images/comments.png" runat="server" />
                                            <obout:Flyout runat="server" ID="ToolTipSession" AttachTo="ImgSession" Align="TOP"
                                                FadingEffect="false" FlyingEffect="BOTTOM_LEFT" Position="TOP_LEFT">
                                                <asp:Panel ID="PnlTooltipSession" runat="server" HorizontalAlign="Center" CssClass="QuickLinksFlyoutPopup"
                                                    Height="200px" Width="420px" ScrollBars="Auto">
                                                </asp:Panel>
                                            </obout:Flyout>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle HorizontalAlign="Center" CssClass="clssubhead" />
                                <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                    FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100%" background="../Images/separator_repeat.gif" height="11">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100%">
                            <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
            <Triggers>
                <asp35:AsyncPostBackTrigger ControlID="BtnSearch" EventName="Click" />
                <asp35:AsyncPostBackTrigger ControlID="Pagingctrl" />
            </Triggers>
        </asp35:UpdatePanel>
    </div>
    </form>
</body>
</html>

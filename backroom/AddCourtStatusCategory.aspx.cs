using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.backroom
{
	/// <summary>
	/// Summary description for AddCourtStatusCategory.
	/// </summary>
	public partial class AddCourtStatusCategory : System.Web.UI.Page
	{

        protected System.Web.UI.WebControls.Label lblMessage;
        protected System.Web.UI.WebControls.ListBox listAssigned;
        protected System.Web.UI.WebControls.ImageButton imgbtnDel;
        protected System.Web.UI.WebControls.ImageButton imgbtnAdd;
        protected System.Web.UI.WebControls.ListBox listUnassigned;
        protected System.Web.UI.WebControls.DropDownList ddlCategory;
		

		clsENationWebComponents ClsDb = new clsENationWebComponents();
		clsSession ClsSession=new clsSession();
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{ 
				if (ClsSession.IsValidSession(this.Request)==false)
				{
					Response.Redirect("../frmlogin.aspx",false);
				}
				else //To stop page further execution
				{
					if (ClsSession.GetCookie("sAccessType",this.Request).ToString()!="2")
					{
						Response.Redirect("../LoginAccesserror.aspx",false);
						Response.End();
					}
					else
					{
						imgbtnAdd.Attributes.Add("OnClick","return isUnassignedSelected();"); 
						imgbtnDel.Attributes.Add("OnClick","return isAssignedSelected();"); 


						if(!IsPostBack)
						{ 
							GetCategory();
							FillUnassigned();
							FIllAssigned(Convert.ToInt32(ddlCategory.SelectedValue));
						}
					}
			
				}
				}
			catch(Exception ex)
			{ 
				lblMessage.Text = ex.Message;
				//clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
			
		}
		public void FillUnassigned()
		{  
			try
			{ 
				
				DataSet ds =ClsDb.Get_DS_BySP("USP_Get_UnassignedCourt");
				listUnassigned.DataSource = ds;
				listUnassigned.DataValueField = ds.Tables[0].Columns[0].ColumnName;	//ViolationNumber_PK
				listUnassigned.DataTextField = ds.Tables[0].Columns[1].ColumnName;	//Description
				listUnassigned.DataBind();
			}
			catch(Exception ex)
			{ 
				lblMessage.Text = ex.Message;
			}
		}
		public void FIllAssigned(int typeid)
		{  
			try
			{ 
				DataSet ds =ClsDb.Get_DS_BySPByOneParmameter("USP_GET_AssignedCourt","@Typeid",typeid);
				listAssigned.DataSource = ds;
				listAssigned.DataValueField = ds.Tables[0].Columns[0].ColumnName;	//ViolationNumber_PK
				listAssigned.DataTextField = ds.Tables[0].Columns[1].ColumnName;	//Description
				listAssigned.DataBind();
			}
			catch(Exception ex)
			{ 
				lblMessage.Text = ex.Message;
			}
		}
		public void GetCategory()
		{
			try
			{
				

				DataSet ds =ClsDb.Get_DS_BySP("USP_Get_CourtCategory");
				ddlCategory.DataSource  = ds;
			
				ddlCategory.DataValueField = ds.Tables[0].Columns[0].ColumnName; // value=Typeid
				ddlCategory.DataTextField = ds.Tables[0].Columns[1].ColumnName;	 // Text=Description
				ddlCategory.DataBind();
				

				
				
			
				
				
			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message;
				//clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}
		

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.ddlCategory.SelectedIndexChanged += new System.EventHandler(this.ddlCategory_SelectedIndexChanged);
			this.imgbtnAdd.Click += new System.Web.UI.ImageClickEventHandler(this.imgbtnAdd_Click);
			this.imgbtnDel.Click += new System.Web.UI.ImageClickEventHandler(this.imgbtnDel_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		protected void ddlCategory_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			FIllAssigned(Convert.ToInt32(ddlCategory.SelectedValue));
		}

		private void imgbtnAdd_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			try
			{ 
				if(listUnassigned.SelectedIndex > -1)
				{  
					string strCourtStatusID="";
					foreach(ListItem li in listUnassigned.Items)
					{ 
						if(li.Selected==true)
						{ 
							strCourtStatusID += li.Value + ",";
						}
					} 
					
					int typeid=Convert.ToInt32(ddlCategory.SelectedValue);
					string[]key={"@Typeid","@CourtStatusID"};
					object[] value1={typeid,strCourtStatusID};
					ClsDb.ExecuteSP("USP_HTS_UpdateCourtStatusCategoryID",key,value1);
					FillUnassigned();
					FIllAssigned(Convert.ToInt32(ddlCategory.SelectedValue));

				} 
			}
			catch(Exception ex)
			{ 
				lblMessage.Text=ex.Message;
			}
		}

		private void imgbtnDel_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			try
			{ 
				if(listAssigned.SelectedIndex > -1)
				{ 
					string strCourtStatusID="";
					foreach(ListItem li in listAssigned.Items)
					{
                        if (li.Selected == true)
                        {
                            if (li.Value == "3" || li.Value == "135" || li.Value == "101" || li.Value == "103" || li.Value == "26" || li.Value == "104" || li.Value == "161" || li.Value == "27" || li.Value == "105" || li.Value == "147" || li.Value == "80")
                            {
                                HttpContext.Current.Response.Write("<script language=javascript> alert('" + li.Text + " is a primary case status and you can not unassign this case status.'); </script>");
                                return;
                            }
                            else
                            {
                                strCourtStatusID += li.Value + ",";
                            }
                        }
                    }
					string[]key={"@Typeid","@CourtStatusID"};
					object[] value1={0,strCourtStatusID};
					ClsDb.ExecuteSP("USP_HTS_UpdateCourtStatusCategoryID",key,value1);
					FillUnassigned();
					FIllAssigned(Convert.ToInt32(ddlCategory.SelectedValue));

				}
			}
			catch(Exception ex)
			{ 
			lblMessage.Text=ex.Message;
			}
		}



       
}
}

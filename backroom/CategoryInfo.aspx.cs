using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;


namespace lntechNew.backroom
{
	/// <summary>
	/// Summary description for CategoryInfo.
	/// </summary>
	public partial class CategoryInfo : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.LinkButton lnkb_addnewcat;
		protected System.Web.UI.WebControls.DataGrid dg_catinfo;
		protected System.Web.UI.WebControls.Label lblMessage;
		clsENationWebComponents ClsDb = new clsENationWebComponents();
		clsSession ClsSession=new clsSession();
		clsLogger clog = new clsLogger();
		clsViolationCategory ClsViolationCategory=new clsViolationCategory();
		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				if (ClsSession.IsValidSession(this.Request)==false)
				{
					Response.Redirect("../frmlogin.aspx",false);
				}
				else //To stop page further execution
				{
					if (ClsSession.GetCookie("sAccessType",this.Request).ToString()!="2")
					{
						Response.Redirect("../LoginAccesserror.aspx",false);
						Response.End();
					}
					else //To stop page further execution
					{
				
						if(!IsPostBack)
						{
							lnkb_addnewcat.Attributes.Add("OnClick","return OpenWin();");
							Fill_Grid();
						}
					}
				}
			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{   		
			this.dg_catinfo.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dg_catinfo_ItemDataBound);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void Fill_Grid()
		{
            //Change by Ajmal
			//SqlDataReader rdrCheck=null;
            IDataReader rdrCheck = null;
			try
			{
				dg_catinfo.DataSource=ClsViolationCategory.GetCategory();
				dg_catinfo.DataBind();
				String Query;
				//Query = "SELECT DISTINCT tblViolationCategory.CategoryId FROM tblViolationCategory INNER JOIN tblViolations ON tblViolations.CategoryID = tblViolationCategory.CategoryId order by tblViolationCategory.CategoryId";
				//rdrCheck =ClsDb.Get_All_RecordsByQuery(Query);
                rdrCheck = ClsDb.Get_DR_BySP("USP_HTS_GET_TaggedCategories");
			
				while(rdrCheck.Read())
				{
					foreach(DataGridItem items in dg_catinfo.Items )
					{
						if (((Label) items.FindControl("lbl_id")).Text  == rdrCheck["CategoryID"].ToString())
						{
							((LinkButton) (items.FindControl("LinkButton2"))).Enabled = false;
							break;
						}										
				
					}
				}
			
				foreach(DataGridItem dgItems in dg_catinfo.Items )
				{
					if (((LinkButton) (dgItems.FindControl("LinkButton2"))).Enabled == true)
					{
						((LinkButton) dgItems.FindControl("LinkButton2")).Attributes.Add("OnClick","return OpenDelWin("+ ((Label) dgItems.FindControl("lbl_id")).Text +");");
					}
				}
			}
			catch(Exception ex)
			{
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
			finally
			{
				rdrCheck.Close();
			}
		}

		private void dg_catinfo_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			try
			{
				if(e.Item.ItemIndex>-1) // to not bind to itemIndex=-1
				{
					string strCategoryID = ((Label) e.Item.FindControl("lbl_id")).Text; // lbl_id is bind to CategoryId
					int intCategoryID = Convert.ToInt32(strCategoryID);

					((LinkButton) e.Item.FindControl("Linkbutton1")).Attributes.Add("OnClick","return OpenWinEdit("+ intCategoryID +");");
					//((LinkButton) e.Item.FindControl("LinkButton2")).Attributes.Add("OnClick","return OpenDelWin("+ intCategoryID +");");
				}
			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}	
		
	}
		

	}


using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using System.IO;


namespace lntechNew.backroom
{
    public partial class PublicSiteVisitors : System.Web.UI.Page
    {

        clsENationWebComponents clsdb = new clsENationWebComponents("traffic");
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                datefrom.SelectedDate = DateTime.Now;
                dateto.SelectedDate = DateTime.Now;
            }
        }

        protected void btn_search_Click(object sender, EventArgs e)
        {
            fillGrid();
        }

        public void fillGrid()
        {

            try
            {
                lbl_message.Text = "";

                string[] keys = { "@StartDate", "@EndDate" };
                object[] values = {datefrom.SelectedDate.ToShortDateString(),dateto.SelectedDate.ToShortDateString()};
                DataSet ds = clsdb.Get_DS_BySPArr("USP_PS_GetDailyVisitors", keys, values);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    GridView1.DataSource = ds.Tables[0];
                    GridView1.DataBind();
                    GridView1.Visible = true;
                }
                else
                {
                    lbl_message.Text = "No Record Found";
                    GridView1.Visible = false;
                }
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
            }

        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.Header && e.Row.RowType != DataControlRowType.Footer)
            {

                string pf = ((HiddenField)e.Row.FindControl("hf_pf")).Value;
                string cid = ((HiddenField)e.Row.FindControl("hf_cid")).Value;
                ((HyperLink)e.Row.FindControl("hl_s")).Text = (e.Row.RowIndex + 1).ToString();

                if (pf == "True" && cid != "0")
                {
                    Label lbl = (Label)e.Row.FindControl("lbl_email");
                    Label caseno = (Label)e.Row.FindControl("lbl_s");
                    LinkButton hl = (LinkButton)e.Row.FindControl("hl_email");
                    hl.Visible = true;
                    lbl.Visible = false;
                    //hl.NavigateUrl = "http://new.legalhouston.com/files/report.aspx?CustomerID=" + cid;
                    hl.OnClientClick = "return OpenPopup('" + ConfigurationSettings.AppSettings["PublicSiteURL"] + "/files/report.aspx?CustomerID=" + cid + "')";

                    if (caseno.Text != "")
                        ((HyperLink)e.Row.FindControl("hl_s")).NavigateUrl = "../ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + caseno.Text;
                    else
                        ((HyperLink)e.Row.FindControl("hl_s")).CssClass = "Label";

                }
                else
                {
                    ((HyperLink)e.Row.FindControl("hl_s")).CssClass = "Label";
                }
                
                Label ticketid = (Label)e.Row.FindControl("lbl_ticketid");

                if (ticketid.Text == "")
                {
                    Label violations = (Label)e.Row.FindControl("lbl_violations");

                    char[] test = { ',' };
                    string[] arr = violations.Text.Split(test, violations.Text.Length);

                    foreach (string str in arr)
                    {
                        Label lbl = new Label();
                        lbl.Text =str.Substring(2);
                        lbl.CssClass = "label";
                        e.Row.Cells[3].Controls.Add(lbl);
                        e.Row.Cells[3].Controls.Add(new LiteralControl("<br/>"));
                    }

                }
                else if (ticketid.Text != "")
                {
                    Label violations = (Label)e.Row.FindControl("lbl_violations");

                    char[] test = { ',' };
                    string[] arr = violations.Text.Split(test, violations.Text.Length);

                    foreach (string str in arr)
                    {
                        Label lbl = new Label();
                        lbl.Text = str;
                        lbl.CssClass = "label";
                        e.Row.Cells[3].Controls.Add(lbl);
                        e.Row.Cells[3].Controls.Add(new LiteralControl("<br/>"));
                    }

                    arr = ticketid.Text.Split(test, ticketid.Text.Length);

                    foreach (string str in arr)
                    {
                        Label lbl = new Label();
                        lbl.Text = str;
                        lbl.CssClass = "label";
                        e.Row.Cells[2].Controls.Add(lbl);
                        e.Row.Cells[2].Controls.Add(new LiteralControl("<br/>"));
                    }
                    ticketid.Visible = false;

                }

                // Added By Zeeshan Ahmed To Display Confirmation Page

                if (cid != "0")
                {

                    Label lastpage = (Label)e.Row.FindControl("lbl_lastpage");
                    string path = Server.MapPath("/DOCSTORAGE") + "\\ScanDoc\\images\\" + "C-" + cid + ".pdf";
                    //lastpage.Text = path;
                    if (File.Exists(path))
                    {
                        lastpage.Text = "<a href=\"#\" onClick=OpenPopup('" + "../DOCSTORAGE/ScanDoc/images/" + "C-" + cid + ".pdf" + "')>" + lastpage.Text + "</a>";

                    }


                }


            }
        }


    }
}

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Microsoft.ApplicationBlocks.Data;
using System.Configuration;
using System.Data.SqlClient;
using lntechNew.Components.ClientInfo;


namespace lntechNew.backroom.SqlSrvMrg
{
	/// <summary>
	/// Summary description for WebForm1.
	/// </summary>
	/// 
	

	public partial class StoredProceduresList : System.Web.UI.Page
	{
		string  ConStr = System.Configuration.ConfigurationSettings.AppSettings["connString"].ToString();
        protected System.Data.SqlClient.SqlConnection conn;


        protected System.Web.UI.WebControls.Label lblreportname;
        protected System.Web.UI.WebControls.TextBox txtreportname;
        protected System.Web.UI.WebControls.Label Lblloc;
        protected System.Web.UI.WebControls.DropDownList dlistloc;
        protected System.Web.UI.WebControls.Label lblreportdesc;
        protected System.Web.UI.WebControls.TextBox txtreportdesc;
        protected System.Web.UI.WebControls.DataGrid Dg_Output;
        protected System.Web.UI.WebControls.TextBox txtchk;
        protected System.Web.UI.WebControls.TextBox txtNoParam;
        protected System.Web.UI.WebControls.TextBox TextBox2;
        protected System.Web.UI.WebControls.TextBox txtSpName;
        protected System.Web.UI.WebControls.Label lblmsg;
        protected System.Web.UI.WebControls.DataGrid DgSqlProcedure;
        protected System.Web.UI.WebControls.Label lblNoRec;
        protected System.Web.UI.WebControls.Label Label4;
        protected System.Web.UI.WebControls.TextBox txtSearchSp;
        protected System.Web.UI.WebControls.Label lblCurrPage;
        protected System.Web.UI.WebControls.Label lblPNo;
        protected System.Web.UI.WebControls.Label lblGoto;
        protected System.Web.UI.WebControls.DropDownList cmbPageNo;
        protected System.Web.UI.WebControls.Button cmdSearchSp;
        protected System.Web.UI.WebControls.Button btnReset;
        protected System.Web.UI.WebControls.Button btnsubmit;  

		clsSession cSession = new clsSession();
		clsLogger clog= new clsLogger();
		SqlTransaction SaveTrans;
				 
		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				// Check Session 
				if (cSession.IsValidSession(this.Request)==false)
				{
					Response.Redirect("../frmlogin.aspx",false);
				}
				else //To stop page further execution
				{
					if (cSession.GetCookie("sAccessType",this.Request).ToString()!="2")
					{
						Response.Redirect("../LoginAccesserror.aspx",false);
						Response.End();
					}
					else //To stop page further execution
					{
				
						conn= new SqlConnection(ConStr);
						conn.Open(); 		
						btnsubmit.Attributes.Add("onclick","javascript: return DoValidate();");  // Validating Controls
						if (IsPostBack!=true)
						{
					
					
							string QueryText = "usp_SQLMGR_Get_Procedures" + txtSearchSp.Text.Trim() ;
							DataSet	Ds1= SqlHelper.ExecuteDataset(conn,CommandType.Text,QueryText);
							fill_Grid(Ds1);
							txtNoParam.Text="1";
						}
					}
				}
			}
			catch (Exception ex)
			{
				lblmsg.Text  = ex.Message ;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
			
		}

		private void fill_Grid(DataSet Ds1)
		{
			
			DgSqlProcedure.DataSource = Ds1;	
			DgSqlProcedure.DataBind();
			FillPageList();
			SetNavigation();
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.cmdSearchSp.Click += new System.EventHandler(this.Button1_Click);
			this.DgSqlProcedure.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.DgSqlProcedure_ItemCommand);
			this.DgSqlProcedure.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.DgSqlProcedure_PageIndexChanged);
			this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
			this.btnsubmit.Click += new System.EventHandler(this.btnSubmit_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void ListParameters(string sp_name)  // Fill DataGrid
		{
			try
			{
				DataSet ds =  SqlHelper.ExecuteDataset(conn,CommandType.StoredProcedure ,"sp_help", new SqlParameter("@objname",sp_name ));
				if (ds.Tables.CanRemove(ds.Tables["table1"])) 
				{
					ds.Tables.Remove("Table");
					lblmsg.Text = "";
					Dg_Output.DataSource = ds ;
					Dg_Output.DataBind();
				}
				else
				{
					lblmsg.Text = "No Parameter";
					txtNoParam.Text="0";
					visibility();
					return;
				}
			}

			catch (Exception ex)
			{
				lblmsg.Text  = ex.Message ;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}		
		}

		// Go to the Next Page or Selected Page
		private void GotoPage(int intNewPageNo) 
		{
			try
			{	
				lblmsg.Text  ="";
				DgSqlProcedure.CurrentPageIndex = intNewPageNo ;
				string QueryText = "usp_SQLMGR_Get_Procedures " + txtSearchSp.Text.Trim() ;
				DataSet	Ds1= SqlHelper.ExecuteDataset(conn,CommandType.Text,QueryText);
				fill_Grid(Ds1);
				DgSqlProcedure.SelectedIndex=-1;
				visibility();
			}
			catch (Exception ex)
			{
				lblmsg.Text  = ex.Message ;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}
		
		// Procedure call when new page is come in the Grid
		private void DgSqlProcedure_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
		visible();
		GotoPage(e.NewPageIndex);
		}

		// For searching the storedProcedure
		

		private void cmdSearchSp_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			try
			{
				visible();
				ResetValue();
				
				DgSqlProcedure.CurrentPageIndex = 0;
				string QueryText = "usp_SQLMGR_Get_Procedures " + txtSearchSp.Text.Trim() ;
				DataSet	Ds1= SqlHelper.ExecuteDataset(conn,CommandType.Text,QueryText);
				DgSqlProcedure.SelectedIndex=-1;
				fill_Grid(Ds1);
				if (DgSqlProcedure.Items.Count == 0)
				{
					lblNoRec.Visible = true;
					lblNoRec.Text = "No Record Found";
					lblmsg.Text ="";
					txtNoParam.Text="1";
					txtchk.Text = "1";
				}
				
			}
			catch (Exception ex)
			{
				lblmsg.Text  = ex.Message ;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}

		}

		
		
		//fill dropdownlist with no of pages in grid
		private void FillPageList()
		{
			Int16 idx =0;
			cmbPageNo.Items.Clear();
			for(idx=1; idx<=DgSqlProcedure.PageCount;idx++ )
			{
				cmbPageNo.Items.Add("Page - " + idx.ToString());
				cmbPageNo.Items[idx-1].Value = idx.ToString();
			}
		 }	

		// Procedure for setting data grid's current  page number on header ........
		private void SetNavigation()
		{
			try
			{			
				// setting current page number
				lblPNo.Text =Convert.ToString(DgSqlProcedure.CurrentPageIndex+1);
				lblCurrPage.Visible=true;
				lblPNo.Visible=true;
				// filling combo with page numbers
				lblGoto.Visible=true;
				cmbPageNo.Visible=true;
				//FillPageList();
				cmbPageNo.SelectedIndex =DgSqlProcedure.CurrentPageIndex;
			}
			catch (Exception ex)
			{
				lblmsg.Text  = ex.Message ;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}

		// Procedure for setting the grid page number as per selected from page no. combo.......
		private void cmbPageNo_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try
			{
				visible();				
				ResetValue();
				GotoPage(cmbPageNo.SelectedIndex);
				txtNoParam.Text="1";
				//txtchk.Text = "1";
			}
			catch(Exception ex)
			{
				lblmsg.Text  = ex.Message;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}

		// Fires when an event is generated within the datagrid
		private void DgSqlProcedure_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			try
			{

				if (e.CommandName == "ShowParam")
				{
					lblNoRec.Visible = false;
					lblmsg.Visible =true;
					lblreportname.Visible = true;
					Lblloc.Visible = true;
					lblreportdesc.Visible =true;
					txtreportname.Visible =true;
					txtreportdesc.Visible =true;
					//Comments By Zeeshan Jur
					//Label3.Visible = true;
					btnReset.Visible = true;
					dlistloc.Visible = true;
					Dg_Output.Visible = true;
					btnsubmit.Visible = true;
					txtreportname.Text = "";
					txtreportdesc.Text = "";
					txtNoParam.Text="0";
					txtchk.Text = "0";
					ListParameters( ((LinkButton)( e.Item.FindControl("lnkSPName"))).Text);
					txtSpName.Text = ((LinkButton)( e.Item.FindControl("lnkSPName"))).Text;
					DgSqlProcedure.SelectedIndex = e.Item.ItemIndex;
					TextBox2.Text = Dg_Output.Items.Count.ToString();
					AddScripts();
				}
			}
			catch (Exception ex)
			{
				lblmsg.Text  = ex.Message ;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}
		
		// For Enabling and Disabling DataGrid Controls 
		private void AddScripts() 
		{
			foreach(DataGridItem ItemX in Dg_Output.Items )
			{
				DropDownList ddl = 	((DropDownList)(ItemX.FindControl("DDLObjType")));
				DropDownList ddl2 = ((DropDownList)(ItemX.FindControl("DDLCmdType")) );
				TextBox txt		 =	((TextBox)(ItemX.FindControl("txtCmd"))) ;
				// ShowHide() is a function of ClientSide (JavaScripting)
				string str = "ShowHide('"   + ((DropDownList)(ItemX.FindControl("DDLObjType"))).ClientID +   "','"   + ((TextBox)(ItemX.FindControl("txtCmd"))).ClientID +   "','"   + ((DropDownList)(ItemX.FindControl("DDLCmdType")) ).ClientID +  "')";
				((DropDownList)(ItemX.FindControl("DDLObjType"))).Attributes.Add("onclick",str);
			
			}
		}
		
				
		//Invisible Controls
		private void visible()
		{
			lblNoRec.Visible = false;
			lblreportname.Visible = false;
			lblreportdesc.Visible =false;
			txtreportname.Visible = false;
			txtreportdesc.Visible = false;
			Lblloc.Visible = false;
			//Comments By Zeeshan Jur	
			//Label3.Visible = false;
			btnReset.Visible = false;
			dlistloc.Visible = false;
			btnsubmit.Visible = false;
			Dg_Output.Visible=false;
		}
		// In Visible DataGrid
		private void visibility()
		{
			Dg_Output.Visible=false;
		}
		// Save the Parameter Fields in the database
		private void SaveReportStructure()
		{
			SaveTrans = conn.BeginTransaction();
			int rptId;
			rptId=0;
			SqlParameter [] param = new SqlParameter[4];

			param[0] = new SqlParameter("@spname",txtSpName.Text.Trim());
			param[1] = new SqlParameter("@rptname",txtreportname.Text.Trim());
			param[2] = new SqlParameter("@RptDescription",txtreportdesc.Text.Trim());
			param[3] = new SqlParameter("@ReportLoc",(int) (Convert.ChangeType(dlistloc.SelectedValue,typeof(int)))); // Type Casting
			try
			{
		    rptId= (int) (Convert.ChangeType( SqlHelper.ExecuteScalar(SaveTrans,CommandType.StoredProcedure,"usp_Save_Report_Procedure", param),typeof(int))) ;				
				if(lblmsg.Text == "No Parameter")
				{
				 
				}
				else
				{
				  foreach(DataGridItem ItemX in Dg_Output.Items )	
					{

						string SpParamName = ((Label) (ItemX.FindControl("lblParamName"))  ).Text; // Boxing
						string SpParamType = ((Label) (ItemX.FindControl("lblParamType"))  ).Text;
						string SpParamAlias = ((TextBox) (ItemX.FindControl("txtParamAlias")) ).Text; 
						string SpParamDefValue = ((TextBox) (ItemX.FindControl("txtParamDef")) ).Text;
				
						bool   Visibility = ((CheckBox) (ItemX.FindControl("chkVisibility")) ).Checked;
		
						int SpParamObjType = int.Parse(((DropDownList) (ItemX.FindControl("DDLObjType"))  ).SelectedValue);
						int SpParamCmdType = int.Parse(((DropDownList) (ItemX.FindControl("DDLCmdType"))  ).SelectedValue);
						string SpParamCmdText = ((TextBox) (ItemX.FindControl("txtCmd")) ).Text;

						SqlParameter [] paramDetail = new SqlParameter[9]; 
						paramDetail[0] = new SqlParameter("@rptid",rptId);
						paramDetail[1] = new SqlParameter("@ParamName",SpParamName);
						paramDetail[2] = new SqlParameter("@ParamType",SpParamType);
						paramDetail[3] = new SqlParameter("@ParamAlias ",SpParamAlias);
						paramDetail[4] = new SqlParameter("@DefaultVal",SpParamDefValue);
						paramDetail[5] = new SqlParameter("@ColVisibility",Visibility);
						paramDetail[6] = new SqlParameter("@ObjectType",SpParamObjType);
						paramDetail[7] = new SqlParameter("@CommandType",SpParamCmdType);
						paramDetail[8] = new SqlParameter("@CommandText",SpParamCmdText);
						SqlHelper.ExecuteScalar(SaveTrans,CommandType.StoredProcedure,"usp_SaveDetail_Report_Procedure", paramDetail);				
					}
				}
			SaveTrans.Commit(); 
			ResetValue();	
			}		
			catch (Exception ex)
			{
				lblmsg.Text = ex.Message.ToString();
				SaveTrans.Rollback(); 
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}

		// Reset The Value of controls
		private void ResetValue()
		{
			try
			{
				txtreportname.Text = "";
				txtreportdesc.Text = "";
				dlistloc.SelectedIndex = 0;
				if (lblmsg.Text == "No Parameter")
				{
				}
				else
				{
					foreach(DataGridItem ItemX in Dg_Output.Items)
					{
						((TextBox)      (ItemX.FindControl("txtParamAlias")) ).Text = ""; 
						((TextBox)      (ItemX.FindControl("txtParamDef")  ) ).Text= "";
				
						((CheckBox)     (ItemX.FindControl("chkVisibility")) ).Checked = false;
						((DropDownList) (ItemX.FindControl("DDLObjType")   ) ).SelectedIndex   = 0;
						((DropDownList) (ItemX.FindControl("DDLCmdType")   ) ).SelectedIndex   = 0;
						((TextBox)      (ItemX.FindControl ("txtCmd")      ) ).Text ="";
 
					}
				}
			}
			catch (Exception ex)
			{
				lblmsg.Text = ex.Message.ToString();
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}


		

		
			
		// Save Report
		private void btnsubmit_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			SaveReportStructure();
		}
		
		// Reset All fields
		private void btnReset_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			ResetValue();
		}

		private void btnSubmit_Click(object sender, System.EventArgs e)
		{
			SaveReportStructure();
		}

		private void Button1_Click(object sender, System.EventArgs e)
		{
			try
			{
				visible();
				ResetValue();
				
				DgSqlProcedure.CurrentPageIndex = 0;
				string QueryText = "usp_SQLMGR_Get_Procedures " + txtSearchSp.Text.Trim() ;
				DataSet	Ds1= SqlHelper.ExecuteDataset(conn,CommandType.Text,QueryText);
				DgSqlProcedure.SelectedIndex=-1;
				fill_Grid(Ds1);
				if (DgSqlProcedure.Items.Count == 0)
				{
					lblNoRec.Visible = true;
					lblNoRec.Text = "No Record Found";
					lblmsg.Text ="";
					txtNoParam.Text="1";
					txtchk.Text = "1";
				}
				
			}
			catch (Exception ex)
			{
				lblmsg.Text  = ex.Message ;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}

		private void btnReset_Click(object sender, System.EventArgs e)
		{
			ResetValue();
		}

		

		
		

		
		


	

		
		

		

	

		
		
	}
}

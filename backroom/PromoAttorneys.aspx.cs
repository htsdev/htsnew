using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;
using System.Data.Common;
using System.Configuration;
using HTP.Components.ClientInfo;
using HTP.WebComponents;
using HTP.Components.Services;
using HTP.Components.Entities;
using HTP.Components;
namespace HTP.backroom
{
    /// <summary>
    /// Summary description for Courts.
    /// </summary>
    public partial class PromoAttorneys : MatterBasePage
    {

        private static DataTable dtMain = new DataTable();
        private void Page_Load(object sender, System.EventArgs e)
        {
            if (!IsPostBack)
            {
                FillGrid();
                FillControls();
            }
            Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
            Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
            Pagingctrl.GridView = gv_records;
        }

        private void FillControls()
        {
            Attorneys obj_Att = new Attorneys();
            DataTable dtAtt = obj_Att.GetAttorneyNames();
            DataTable dtMail = obj_Att.GetPromoMailersNames();
            DataRow drAtt = dtAtt.NewRow();
            drAtt["AttID"] = "--Select Attorney--";
            drAtt["AttorneyName"] = "--Select Attorney--";
            dtAtt.Rows.InsertAt(drAtt, 0);
            DataRow drMail = dtMail.NewRow();
            drMail["LetterID_PK"] = 0;
            drMail["MailerName"] = "--Select Mailer--";
            dtMail.Rows.InsertAt(drMail, 0);

            ddlAttName.DataSource = dtAtt;
            ddlAttName.DataTextField = "AttorneyName";
            ddlAttName.DataValueField = "AttID";
            ddlAttName.DataBind();
            ddlMailName.DataSource = dtMail;
            ddlMailName.DataTextField = "MailerName";
            ddlMailName.DataValueField = "LetterID_PK";
            ddlMailName.DataBind();
        }
        private void FillGrid()
        {
            Attorneys obj_Att = new Attorneys();
            DataTable dt = obj_Att.GetPromoInfo();
            if (dt.Rows.Count > 0)
            {
                gv_records.DataSource = dt;
                gv_records.DataBind();
                dtMain = dt;
                Pagingctrl.PageCount = gv_records.PageCount;
                Pagingctrl.PageIndex = gv_records.PageIndex;
                Pagingctrl.SetPageIndex();
                Session["PromoInfo"] = dt.DefaultView;
            }
            else
            {                
                Pagingctrl.PageCount = 0;
                Pagingctrl.PageIndex = 0;
                Pagingctrl.SetPageIndex();
                lblMessage.Text = "No Records Found";
                gv_records.Visible = false;
            }
        }
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            //Kamran 3536 04/09/08 this event genrate a double click because client side also call this button Onclick=btn_Submit_Click
            //this.btn_Submit.Click += new System.EventHandler(this.btn_Submit_Click);
            
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion

        
        
        void Pagingctrl_PageIndexChanged()
        {

            gv_records.PageIndex = Pagingctrl.PageIndex - 1;
            FillGrid();

        }
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            if (pageSize > 0)
            {
                gv_records.PageIndex = 0;
                gv_records.PageSize = pageSize;
                gv_records.AllowPaging = true;
                if (Pagingctrl.PageIndex == 0)
                {
                    Pagingctrl.PageIndex = 1;
                }
                gv_records.PageIndex = Pagingctrl.PageIndex - 1;
                Pagingctrl.PageCount = gv_records.PageCount;
                Pagingctrl.PageIndex = gv_records.PageIndex;
                Pagingctrl.SetPageIndex();
            }
            else
            {
                gv_records.AllowPaging = false;
            }
            FillGrid();

        }         

        protected void gv_records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gv_records.PageIndex = e.NewPageIndex;             
            FillGrid();
            Pagingctrl.PageCount = gv_records.PageCount;    
            Pagingctrl.PageIndex = gv_records.PageIndex;    
            Pagingctrl.SetPageIndex();
        }

        protected void gv_records_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "PromoUpdate")
            {
                int promoID = Convert.ToInt32(e.CommandArgument);
                Attorneys obj_Att = new Attorneys();
                obj_Att.PromoID = promoID;
                DataTable dt = obj_Att.GetSelectedPromoScheme();
                hf_PromoId.Value = promoID.ToString();
                ddlAttName.SelectedValue = dt.Rows[0]["AttorneyName"].ToString().Trim();
                ddlMailName.SelectedValue = dt.Rows[0]["MailerID"].ToString().Trim();
                txtPromoMsgForLatter.Text = dt.Rows[0]["PromoMessageForLatter"].ToString().Trim();
                txtPromoMsgForHTP.Text = dt.Rows[0]["PromoMessageForHTP"].ToString().Trim();
                ddlViolCount.SelectedValue = dt.Rows[0]["ViolationCount"].ToString().Trim(); 
                btnAddUpdate.Text = "Update";
                lbl_Message.Text = "";
                lblTitle.Text = "Update Promo Scheme";
                MPEPromoPopup.Show();               

            }
            else if (e.CommandName == "Delete")
            {
                int promoID = Convert.ToInt32(e.CommandArgument);
                Attorneys obj_Att = new Attorneys();
                obj_Att.PromoID = promoID;
                obj_Att.DeletePromoScheme();
                FillGrid();
            }
        }
        protected void gv_records_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            
        }
        protected void gv_records_Sorting(object sender, GridViewSortEventArgs e)
        {
            
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            MPEPromoPopup.Hide();
        }

        protected void btnAddUpdate_Click(object sender, EventArgs e)
        {
            Attorneys obj_Att = new Attorneys();
            bool isAlreadyExists = false;
            int rowCount = 0;
            if (btnAddUpdate.Text == "Update")
            {
                obj_Att.PromoID = Convert.ToInt32(hf_PromoId.Value.ToString());  
            }            
            obj_Att.AttorneyName = ddlAttName.SelectedItem.Text.Trim();
            obj_Att.MailerName = Convert.ToInt32(ddlMailName.SelectedValue.Trim());
            obj_Att.LatterMessage = txtPromoMsgForLatter.Text.Trim();
            obj_Att.MatterMessage = txtPromoMsgForHTP.Text.Trim();
            obj_Att.ViolationCount = Convert.ToInt32(ddlViolCount.SelectedValue.Trim());
            foreach (DataRow dr in dtMain.Rows)
            {
                rowCount++;
                if (
                   dr["AttorneyName"].ToString().ToUpper().Trim() == ddlAttName.SelectedItem.Text.ToUpper().Trim() &&
                   Convert.ToInt32(dr["LetterID_PK"].ToString().Trim()) == Convert.ToInt32(ddlMailName.SelectedValue) &&
                   Convert.ToInt32(dr["ViolationCount"].ToString().Trim()) == Convert.ToInt32(ddlViolCount.SelectedValue.Trim())
                   )
                {
                    isAlreadyExists = true;
                }

            }
            if (isAlreadyExists)
            {
                lbl_Message.Text = "This scheme is already exists at " + rowCount.ToString() + " , please modify or add another";
                MPEPromoPopup.Show();
                return;
            }
            else
            {
                lbl_Message.Text = "";
            }
            if (btnAddUpdate.Text.Trim() == "Add New Scheme")
            {
               obj_Att.AddPromoScheme();
               FillGrid();
            }
            else
            {               
                obj_Att.UpdatePromoInfo();
                FillGrid();
            }
        }
      
    }
}

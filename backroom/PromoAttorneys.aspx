<%@ Page Language="c#" AutoEventWireup="false" Inherits="HTP.backroom.PromoAttorneys" CodeBehind="PromoAttorneys.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>Attorney Promotional Information</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <meta http-equiv="X-UA-Compatible" content="IE=5; IE=8; IE=7.5;" />
    <meta http-equiv="X-UA-Compatible" content="chrome=1" /> 
    <link href="../Styles.css" type="text/css" rel="stylesheet" />  
    <script language="javascript" type="text/javascript">
    function ResetControls(type) {
    document.getElementById("btnAddUpdate").value = "Add New Scheme";
    document.getElementById("lblTitle").value = "Add New Promo Scheme";
    document.getElementById("txtPromoMsgForLatter").value = "";
    document.getElementById("txtPromoMsgForHTP").value = "";
    document.getElementById("ddlAttName").selectedIndex=0;
    document.getElementById("ddlMailName").selectedIndex=0;
    document.getElementById("ddlViolCount").selectedIndex=0;
    
    
    return false;
    }
    function SchemeValidation(type) {    
    if(document.getElementById("ddlAttName").selectedIndex==0)
    {
        alert("Please select attorney name for the scheme");
        document.getElementById("ddlAttName").focus();
        return false;
    }
    if(document.getElementById("ddlMailName").selectedIndex==0)
    {
        alert("Please select mailer for the scheme");
        document.getElementById("ddlMailName").focus();
        return false;
    }
    if(document.getElementById("txtPromoMsgForLatter").value == "")
    {
        alert("Please enter message for mailer");
        document.getElementById("txtPromoMsgForLatter").focus();
        return false;
    }
    if(document.getElementById("txtPromoMsgForHTP").value == "")
    {
        alert("Please enter message for matter page");
        document.getElementById("txtPromoMsgForHTP").focus();
        return false;
    }    
    if(document.getElementById("ddlViolCount").selectedIndex==0)
    {
        alert("Please select violation count for the scheme");
        document.getElementById("ddlViolCount").focus();
        return false;
    }    
    }
    
    </script>
</head>
<body>
    <form id="frmAttorneyPromo" method="post" runat="server">
     <aspnew:ScriptManager ID="ScriptManager1" runat="server">
            <Scripts>
                <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
            </Scripts>
        </aspnew:ScriptManager>
    <table id="TableMain" cellspacing="0" cellpadding="0" width="780" align="center"
        border="0">
        <tr>
            <td>
                    
                <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
            </td>
        </tr>
        <tr>
            <td>               
                <asp:Label ID="lblMessage" runat="server" TabIndex="40"></asp:Label>
            </td>
        </tr>
        <tr>
            <td background="../Images/subhead_bg.gif" class="clssubhead" align="right" style="height: 34px">
                <table style="width: 100%">
                    <tr>
                        <td>
                        </td>
                        <td style="text-align: right">
                            <%--<aspnew:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>--%>
                                    <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                <%--</ContentTemplate>
                            </aspnew:UpdatePanel>--%>
                        </td>
                        <td>
                            <asp:LinkButton ID="btnAddNewPromoTemplate" runat="server" Text="Add New Promo Template" CssClass="clsbutton"
                            OnClientClick="return ResetControls();" />
                        </td>
                    </tr>
                </table>
                                    
            </td>
        </tr>
        <tr>
            <td background="../../images/separator_repeat.gif" height="10">
            </td>
        </tr>
        <tr>
            <td>
           <%--<aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlResult">
                <ProgressTemplate>
                    <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl1" runat="server" Text="Please Wait ......"
                        CssClass="clsLabel"></asp:Label>
                </ProgressTemplate>
             </aspnew:UpdateProgress>--%>
           <%--<aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline">
                <ContentTemplate>--%>
                <asp:GridView ID="gv_records" runat="server" AutoGenerateColumns="False" Width="100%"
                        AllowPaging="True" DataKeyNames="PromoID" OnPageIndexChanging="gv_records_PageIndexChanging" PageSize="20"
                        CssClass="clsLeftPaddingTable" OnRowCommand="gv_records_RowCommand"
                        AllowSorting="True" OnSorting="gv_records_Sorting" OnRowDeleting ="gv_records_RowDeleting">
                        <Columns>                            
                            <asp:TemplateField HeaderText="Attorney Name">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lblAttorneyName" runat="server" CssClass="clsLabel" 
                                        Text='<%# Bind("AttorneyName") %>'></asp:Label>
                                </ItemTemplate>                                
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Mailer">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lbl_MailerName" runat="server" CssClass="clsLabel" 
                                        Text='<%# Bind("MailerName") %>'></asp:Label>
                                        <asp:Label ID="lbl_Mailer" runat="server" Visible="false" CssClass="clsLabel" 
                                        Text='<%# Bind("LetterID_PK") %>'></asp:Label>
                                </ItemTemplate>                               
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Message on Latter">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lbl_PromoMailerMsg" runat="server" CssClass="clsLabel" 
                                        Text='<%# Bind("PromoMessageForLatter") %>'></asp:Label>
                                </ItemTemplate>                                 
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Message on matter page">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lbl_PromoHtpMsg" runat="server" CssClass="clsLabel" 
                                        Text='<%# Bind("PromoMessageForHTP") %>'></asp:Label>
                                </ItemTemplate>                               
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Violation Count">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lbl_ViolationCount" runat="server" CssClass="clsLabel" 
                                        Text='<%# Bind("ViolCount") %>'></asp:Label>
                                    
                                </ItemTemplate>                                
                            </asp:TemplateField>                            
                            <asp:TemplateField>
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                <ItemTemplate>
                                <asp:Label ID="lblPromoID" runat="server" Visible="false" CssClass="clsLabel" Text='<%# Bind("PromoID") %>'></asp:Label>                                                                  
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                <ItemTemplate>
                                <asp:LinkButton ID="lnkbtnCourtNo" runat="server" Text="Update"
                                CommandName="PromoUpdate" TabIndex="93" CommandArgument='<%# Bind("PromoID") %>'></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                           <asp:TemplateField>
                           <ItemTemplate>   
                               <asp:LinkButton ID="btnDelete" commandName="Delete" Text="Delete" CommandArgument='<%# Bind("PromoID") %>' runat="server" />   
                               <ajaxToolkit:ConfirmButtonExtender TargetControlId="btnDelete" ConfirmText="Are you sure to delete the selected scheme?" runat="server" />
                           </ItemTemplate>   
                        </asp:TemplateField> 
                            
                        </Columns>
                        <PagerStyle HorizontalAlign="Center" />
                        <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                            FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" 
                            LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                    </asp:GridView>                                   
               <%-- </ContentTemplate>
              </aspnew:UpdatePanel>--%>
           <ajaxToolkit:ModalPopupExtender ID="MPEPromoPopup" runat="server" BackgroundCssClass="modalBackground"
            CancelControlID="lnkbtnCancelPopUp" PopupControlID="pnlModalPopUp" TargetControlID="btnAddNewPromoTemplate">
           </ajaxToolkit:ModalPopupExtender>
           <asp:Panel ID="pnlModalPopUp" runat="server"> 
                          
               <table border="1" enableviewstate="true" style="border-left-color: navy; border-bottom-color: navy;
               border-top-color: navy; border-collapse: collapse; border-right-color: navy; background-color:#EFF4FB">
               <tr>
                <td background="../Images/subhead_bg.gif" class="clssubhead" align="right" style="height: 34px; background-color: #99CCFF;" colspan="2" align="right" bgcolor="#99CCFF">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <asp:label id="lblTitle" CssClass="clssubhead" runat="server" Text="Add New Promo Scheme"></asp:label>
                        </td>
                        <td align="right">
                            <asp:LinkButton ID="lnkbtnCancelPopUp" runat="server">X</asp:LinkButton>&nbsp;            
                        </td>
                    </tr>
                </table>
                
                </td>
               </tr>
               <tr>               
                <td><asp:Label id="lblAtt" runat="server" Text="Select Attorney" CssClass="Label"></asp:Label></td>
                <td> <asp:DropDownList id="ddlAttName" EnableViewState="true" runat="server" CssClass="clsInputCombo" Width="100%">
                    </asp:DropDownList>
                    <asp:HiddenField id="hf_PromoId" runat="server" />
                </td>
               </tr>
               <tr>
                <td><asp:Label id="Label1" runat="server" Text="Select Mailer" CssClass="Label"></asp:Label></td>
                <td><asp:DropDownList id="ddlMailName" EnableViewState="true" runat="server" CssClass="clsInputCombo" Width="100%">
                    </asp:DropDownList>
                </td>
               </tr>
               <tr>
                <td><asp:Label id="Label2" runat="server" Text="Enter Message for Mailer" CssClass="Label"></asp:Label></td>
                <td><asp:TextBox ID="txtPromoMsgForLatter" EnableViewState="true" CssClass="clsInputadministration" Width="99%" runat="server"></asp:TextBox></td>
               </tr>
               <tr>
                <td><asp:Label id="Label3" runat="server" Text="Enter Message for Matter page" CssClass="Label"></asp:Label></td>
                <td><asp:TextBox ID="txtPromoMsgForHTP" EnableViewState="true" CssClass="clsInputadministration" Width="99%" runat="server"></asp:TextBox>
                </td>
                </tr>
                <tr>
                <td><asp:Label id="Label4" runat="server" Text="Select Violations" CssClass="Label"></asp:Label></td>
                <td>
                <asp:DropDownList id="ddlViolCount" EnableViewState="true" runat="server" CssClass="clsInputCombo" Width="100%">
                    <asp:ListItem Text="--Select Violation Count--" Value="-1" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="One Violation" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Two Violations" Value="2"></asp:ListItem>
                    <asp:ListItem Text="Three Violations" Value="3"></asp:ListItem>
                    <asp:ListItem Text="Four Violations" Value="4"></asp:ListItem>
                    <asp:ListItem Text="Five Violations" Value="5"></asp:ListItem>
                    <asp:ListItem Text="Six/More Violations" Value="6"></asp:ListItem>
                    <asp:ListItem Text="All" Value="0"></asp:ListItem>
                </asp:DropDownList>
               </td>
               </tr> 
               <tr>
                <td colspan="2">
                    <asp:Label id="lbl_Message" ForeColor="Red" runat="server"></asp:Label>
                   
                </td>
               </tr> 
               <tr>
                <td >
                    
                </td>
                <td align="right">
                    <asp:Button id="btnAddUpdate" runat="server" Text="Add New Scheme" CssClass="clsbutton" 
                        onclick="btnAddUpdate_Click" OnClientClick="return SchemeValidation();" />&nbsp;
                    <asp:Button id="btnCancel" runat="server" Text="Cancel" CssClass="clsbutton" 
                        onclick="btnCancel_Click" />
                </td>
               </tr>                  
                </table>
                
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td background="../../images/separator_repeat.gif" height="11">          
            </td>
        </tr>
        <tr>
            <td>
                <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
            
            </td>
        </tr>
    </table>
     
    </form>

    

</body>
</html>

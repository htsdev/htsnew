using System;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using lntechNew.Components.ClientInfo;
using HTP.Components;

namespace HTP.backroom
{
    /// <summary>
    /// Summary description for AutoDialerLog.
    /// </summary>
    public partial class AutoDialerLog : System.Web.UI.Page
    {
        #region Variables/others

        DataTable _dtLog;
        readonly AutoDialer _clsAD = new AutoDialer();
        readonly clsSession _cSession = new clsSession();
        readonly clsLogger _bugTracker = new clsLogger();
        DataView _dvResult;
        string _strExp = String.Empty;
        string _strAcsDec = String.Empty;

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            this.ddlPageNo.SelectedIndexChanged += new System.EventHandler(this.ddlPageNo_SelectedIndexChanged);
            this.dg_OutCome.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dg_OutCome_PageIndexChanged);
            this.dg_OutCome.SortCommand += new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.dg_OutCome_SortCommand);
            this.dg_OutCome.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dg_OutCome_ItemDataBound);
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion

        #region Events

        private void Page_Load(object sender, EventArgs e)
        {
            //Waqas 5057 03/17/2009 Checking employee info in session
            if (_cSession.IsValidSession(Request, Response, Session) == false)
            {
                Response.Redirect("../frmlogin.aspx", false);
            }
            else //To stop page further execution
            {
                if (!IsPostBack)
                {
                    btnSubmit.Attributes.Add("OnClick", "return validate();");

                    dtTo.SelectedDate = DateTime.Today;
                    dtFrom.SelectedDate = DateTime.Today;

                    FillEvents();
                    Search();
                }
            }
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                Search();
            }
            catch (Exception ex)
            {
                lblMsg.Text = ex.Message;
                _bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void dg_OutCome_ItemDataBound(object sender, DataGridItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                try
                {
                    var iTicketId = (int)(Convert.ChangeType(((Label)(e.Item.FindControl("lbl_Ticketid"))).Text, typeof(int)));

                    ((HyperLink)(e.Item.FindControl("hlk_LName"))).NavigateUrl = "../ClientInfo/ViolationFeeOld.aspx?search=0&casenumber=" + iTicketId;

                    // changing row color on mouse hover.....
                    e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='#FFF2D9'");
                    e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#EFF4FB'");

                    var img = (ImageButton)e.Item.FindControl("img_Record");
                    img.Visible = img.CommandArgument != "";
                }
                catch
                {

                }
            }
        }

        private void dg_OutCome_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dg_OutCome.CurrentPageIndex = e.NewPageIndex;
            FillGrid();
        }

        private void dg_OutCome_SortCommand(object source, DataGridSortCommandEventArgs e)
        {
            lblMsg.Text = "";
            SortGrid(e.SortExpression);
        }

        private void ddlPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            dg_OutCome.CurrentPageIndex = ddlPageNo.SelectedIndex;
            FillGrid();
        }

        protected void dg_OutCome_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            if (e.CommandName == "Listen")
            {
                string recPath = e.CommandArgument.ToString();
                recPath = recPath.Remove(0, recPath.ToUpper().IndexOf("DOCSTORAGE"));
                recPath = recPath.Replace("\\", "/");
                recPath = "../" + recPath;
                HttpContext.Current.Response.Write("<script>window.open('" + recPath + "','','fullscreen=no,toolbar=no,width=2,height=2,left=0,top=0,status=no,menubar=no,resizable=no');</script>");
            }
        }

        #endregion

        #region Methods

        private void FillEvents()
        {
            var dt = _clsAD.GetAllEventTypes();

            int idx;

            for (idx = 0; idx < dt.Rows.Count; idx++)
            {
                ddl_Events.Items.Add(new ListItem(dt.Rows[idx]["eventtypename"].ToString(), dt.Rows[idx]["eventtypeid"].ToString()));
            }


        }

        private void FillGrid()
        {
            try
            {
                //Fahad 8556 12/20/2010 Renamed the DataView 
                var dvTemp = (DataView)_cSession.GetSessionObject("dvLogResult", Session);
                dg_OutCome.DataSource = dvTemp;
                dg_OutCome.DataBind();
                GenerateSerial();
                FillPageList();
                SetNavigation();
                if (dg_OutCome.Items.Count == 0)
                    lblMsg.Text = "No record found.";
            }
            catch (Exception ex)
            {
                if (dg_OutCome.CurrentPageIndex > dg_OutCome.PageCount - 1)
                {
                    dg_OutCome.CurrentPageIndex = 0;
                    dg_OutCome.DataBind();
                    GenerateSerial();
                    SetNavigation();
                }
                else
                {
                    lblMsg.Text = ex.Message;
                    _bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

                }
            }

        }

        private void PullData()
        {
            _clsAD.EventTypeID = Convert.ToInt16(ddl_Events.SelectedValue);

            if ((dtFrom.SelectedDate.ToShortDateString() == "1/1/0001" && dtTo.SelectedDate.ToShortDateString() == "1/1/0001"))
            {
                _clsAD.StartDate = DateTime.Now;
                _clsAD.EndDate = DateTime.Now;
                _dtLog = _clsAD.GetAutoDialerLog();
            }
            else
            {
                _clsAD.StartDate = dtFrom.SelectedDate;
                _clsAD.EndDate = dtTo.SelectedDate;
                _dtLog = _clsAD.GetAutoDialerLog();
            }
            //Fahad 8556 12/20/2010 Renamed the DataView 
            _cSession.SetSessionVariable("dvLogResult", _dtLog.DefaultView, Session);
        }

        private void GenerateSerial()
        {
            // Procedure for writing serial numbers in "S.No." column in data grid.......	


            long sNo = (dg_OutCome.CurrentPageIndex) * (dg_OutCome.PageSize);
            try
            {
                // looping for each row of record.............
                foreach (DataGridItem itemX in dg_OutCome.Items)
                {

                    try
                    {
                        int iTicketId = (int)(Convert.ChangeType(((Label)(itemX.FindControl("lbl_CallID"))).Text, typeof(int)));
                        sNo += 1;
                        // setting text of hyper link to serial number after bouncing of hyper link...
                        ((Label)(itemX.FindControl("lblSerial"))).Text = (string)Convert.ChangeType(sNo, typeof(string));
                    }
                    catch
                    { }


                }
            }
            catch (Exception ex)
            {
                _bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

                lblMsg.Text = ex.Message;
            }
        }

        private void SortGrid(string sortExp)
        {
            try
            {
                SetAcsDesc(sortExp);
                //Fahad 8147 09/27/2010 Renamed the DataView 
                _dvResult = (DataView)(_cSession.GetSessionObject("dvLogResult", Session));
                _dvResult.Sort = _strExp + " " + _strAcsDec;
                //Fahad 8147 09/27/2010 Renamed the DataView 
                _cSession.SetSessionVariable("dvLogResult", _dvResult, Session);
                FillGrid();

            }
            catch (Exception ex)
            {
                lblMsg.Text = ex.Message;
                _bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

            }
        }

        private void SetAcsDesc(string val)
        {
            try
            {
                //Fahad 8147 09/27/2010 Renamed the DataView 
                _strExp = _cSession.GetSessionVariable("StrLogExp", Session);
                //Fahad 8147 09/27/2010 Renamed the DataView 
                _strAcsDec = _cSession.GetSessionVariable("StrLogAcsDec", Session);
            }
            catch
            {

            }

            if (_strExp == val)
            {
                if (_strAcsDec == "ASC")
                {
                    _strAcsDec = "DESC";
                    //Fahad 8147 09/27/2010 Renamed the DataView 
                    _cSession.SetSessionVariable("StrLogAcsDec", _strAcsDec, Session);
                }
                else
                {
                    _strAcsDec = "ASC";
                    //Fahad 8147 09/27/2010 Renamed the DataView 
                    _cSession.SetSessionVariable("StrLogAcsDec", _strAcsDec, Session);
                }
            }
            else
            {
                _strExp = val;
                _strAcsDec = "ASC";
                //Fahad 8147 09/27/2010 Renamed the DataView 
                _cSession.SetSessionVariable("StrLogExp", _strExp, Session);
                //Fahad 8147 09/27/2010 Renamed the DataView 
                _cSession.SetSessionVariable("StrLogAcsDec", _strAcsDec, Session);
            }
        }

        private void SetNavigation()
        {
            // Procedure for setting data grid's current  page number on header ........

            try
            {
                // setting current page number
                lblCurrPage.Text = Convert.ToString(dg_OutCome.CurrentPageIndex + 1);

                for (var idx = 0; idx < ddlPageNo.Items.Count; idx++)
                {
                    ddlPageNo.Items[idx].Selected = false;
                }
                ddlPageNo.Items[dg_OutCome.CurrentPageIndex].Selected = true;

            }
            catch (Exception ex)
            {
                lblMsg.Text = ex.Message;
                _bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

            }
        }

        private void FillPageList()
        {
            try
            {
                int i;
                ddlPageNo.Items.Clear();
                for (i = 1; i <= dg_OutCome.PageCount; i++)
                {
                    ddlPageNo.Items.Add("Page - " + i);
                    ddlPageNo.Items[i - 1].Value = i.ToString();
                }
            }
            catch (Exception ex)
            {
                lblMsg.Text = ex.Message;
                _bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

            }

        }

        private void Search()
        {
            lblMsg.Text = "";
            dg_OutCome.CurrentPageIndex = 0;
            PullData();
            FillGrid();
        }

        #endregion

    }
}

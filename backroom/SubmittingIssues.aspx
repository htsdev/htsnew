<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SubmittingIssues.aspx.cs" Inherits="lntechNew.backroom.SubmittingIssues" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Submitting Online Issues</title>
    <LINK href="../Styles.css" type="text/css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server" enctype="multipart/form-data">
    <div>
    <TABLE id="TableMain" cellSpacing="0" cellPadding="0" width="780"
				align="center" border="0">
				<tr>
				<td class="clssubhead" background="../Images/subhead_bg.gif" colSpan="2" height="34px">
				    Submitt Your Issues
				</td>
				</tr>
				<tr>
				    <td valign="top" colspan="2">
				        <table id="tbl1" border="0" cellpadding="0" cellspacing="0" width="100%">
				        <tr class="clsLeftPaddingTable">
				        <td align=right>
                            Matter</td>
                            <td>
                                <asp:DropDownList ID="ddl_matter" runat="server">
                                </asp:DropDownList>
                            </td>
				        </tr>
                            <tr>
                                <td align="right" style="height: 22px">
                                    Activities</td>
                                <td style="height: 22px">
                                    &nbsp;<asp:DropDownList ID="ddl_activities" runat="server">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td align="right" style="height: 16px">
                                    Reports</td>
                                <td style="height: 16px">
                                    &nbsp;<asp:DropDownList ID="ddl_reports" runat="server">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td align="right" style="height: 16px">
                                    Validation</td>
                                <td style="height: 16px">
                                    &nbsp;<asp:DropDownList ID="ddl_validation" runat="server">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td align="right" style="height: 16px">
                                    Backroom</td>
                                <td style="height: 16px">
                                    &nbsp;<asp:DropDownList ID="ddl_backroom" runat="server">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td align="right" style="height: 16px">
                                    Priority</td>
                                <td style="height: 16px">
                                    &nbsp;<asp:DropDownList ID="ddl_priority" runat="server" Width="76px">
                                        <asp:ListItem>Low</asp:ListItem>
                                        <asp:ListItem>High</asp:ListItem>
                                        <asp:ListItem>Medium</asp:ListItem>
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td align="right" style="height: 16px">
                                    Issue Type</td>
                                <td style="height: 16px">
                                    &nbsp;<asp:DropDownList ID="ddl_issuetype" runat="server" Width="75px">
                                        <asp:ListItem>Design</asp:ListItem>
                                        <asp:ListItem>Logical</asp:ListItem>
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td align="right" style="height: 16px">
                                    Short Description</td>
                                <td style="height: 16px">
                                    &nbsp;<asp:TextBox ID="txt_shortdesc" runat="server" CssClass="clstextarea" Width="220px"></asp:TextBox></td>
                            </tr>
				        <tr>
				        <td class="clsLeftPaddingTable" align=right>
				           PLease Enter the Bug Description&nbsp;
				        </td>
				        <td class="clsLeftPaddingTable">
				            <asp:TextBox ID="txt_Desc" runat="server" TextMode="MultiLine" CssClass="clstextarea" Height="65px" Width="219px"></asp:TextBox>
				        </td>
				        </tr>
                            <tr>
                                <td align="right" class="clsLeftPaddingTable">
                                    Upload file</td>
                                <td class="clsLeftPaddingTable">
                                    <asp:FileUpload ID="fp_file" runat="server" /></td>
                            </tr>
                            <tr>
                                <td align="right" class="clsLeftPaddingTable">
                                </td>
                                <td class="clsLeftPaddingTable">
                                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" /></td>
                            </tr>
				        </table>
				    </td>
				    </tr>
				
				</TABLE>
    </div>
    </form>
</body>
</html>

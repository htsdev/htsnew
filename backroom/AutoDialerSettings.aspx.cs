﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using AjaxControlToolkit;
using HTP.Components;
using lntechNew.Components.ClientInfo;

namespace HTP.backroom
{
    public partial class AutoDialerSettings : System.Web.UI.Page
    {
        #region Variables

        AutoDialer clsAD = new AutoDialer();
        clsSession cSession = new clsSession();
        clsLogger BugTracker = new clsLogger();

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            //Waqas 5057 03/17/2009 Checking employee info in session
            if (cSession.IsValidSession(this.Request, this.Response, this.Session) == false)
            {
                Response.Redirect("../frmlogin.aspx", false);
            }
            else //To stop page further execution
            {
                if (cSession.GetCookie("sAccessType", this.Request).ToString() != "2")
                {
                    Response.Redirect("../LoginAccesserror.aspx", false);
                    Response.End();
                }
                else //To stop page further execution
                {
                    if (!IsPostBack)
                    {
                        if (Request.QueryString["sMenu"] != "" & Request.QueryString["sMenu"] != null)
                        {
                            hlk_History.NavigateUrl = "~/backroom/AutoDialerSettingsHistory.aspx?sMenu=" + Request.QueryString["sMenu"].ToString();
                        }
                        GetEventTypes();
                    }
                }
            }
        }        

        protected void btn_Update1_Click(object sender, EventArgs e)
        {
            try
            {
                lbl_Message.Text = String.Empty;

                clsAD.EventTypeID = Convert.ToInt16(hf_EventID1.Value);
                clsAD.EmpID = Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request));
                clsAD.CallDays = Convert.ToInt16(ddl_Days1.SelectedValue);
                clsAD.CourtID = Convert.ToInt32(ddl_Courts1.SelectedValue);
                clsAD.SettingDateRange = Convert.ToInt16(ddl_Range1.SelectedValue);
                clsAD.EventState = rb_Start1.Checked;
                clsAD.DialTime = Convert.ToDateTime(tp_DialTime1.SelectedValue);
                //Farrukh 9332 07/28/2011 replaced timepicker with listbox for multi selection
                InsertEventCallTime(tp_DialTime1);
                clsAD.UpdateEventConfigSettingsByID();
                lbl_Message.Text = "Settings Updated Successfully.";
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;
            }
        }

        protected void btn_Update2_Click(object sender, EventArgs e)
        {
            try
            {
                lbl_Message.Text = String.Empty;

                clsAD.EventTypeID = Convert.ToInt16(hf_EventID2.Value);
                clsAD.EmpID = Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request));
                clsAD.CallDays = Convert.ToInt16(ddl_Days2.SelectedValue);
                clsAD.CourtID = Convert.ToInt32(ddl_Courts2.SelectedValue);
                clsAD.SettingDateRange = Convert.ToInt16(ddl_Range2.SelectedValue);
                clsAD.EventState = rb_Start2.Checked;
                //Farrukh 9332 07/28/2011 replaced timepicker with listbox for multi selection
                InsertEventCallTime(tp_DialTime2);
                clsAD.UpdateEventConfigSettingsByID();

                lbl_Message.Text = "Settings Updated Successfully.";
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;
            }
        }
    
        protected void btn_UpdateAll_Click(object sender, EventArgs e)
        {
            try
            {
                clsAD.EventState = rb_StartAll.Checked;
                clsAD.EmpID = Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request));

                clsAD.UpdateAllEventState();

                GetEventTypes();
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;
            }
        }
        #endregion

        #region Methods

        private void FillDays(int noOfDays, DropDownList ddl)
        {
            ddl.Items.Clear();

            for (int i = 1; i <= noOfDays; i++)
            {
                ddl.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
        }

        private void GetEventTypes()
        {
            try
            {
                lbl_Message.Text = String.Empty;
                
                DataTable dt = clsAD.GetEventConfigSettings();
                
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (i == 0)
                        {
                            hf_EventID1.Value = dt.Rows[i]["EventTypeID"].ToString();
                            tabPan_1.HeaderText = dt.Rows[i]["EventTypeName"].ToString();
                            ddl_Courts1.SelectedValue = dt.Rows[i]["CourtID"].ToString();
                            ddl_Range1.SelectedValue = dt.Rows[i]["SettingDateRange"].ToString();
                            bool eventState = Convert.ToBoolean(dt.Rows[i]["EventState"]);
                            if (eventState == true)
                            {
                                rb_Start1.Checked = true;
                                rb_Stop1.Checked = false;
                            }
                            else
                            {
                                rb_Start1.Checked = false;
                                rb_Stop1.Checked = true;
                            }
                            //Farrukh 9332 07/28/2011 replaced timepicker with listbox for multi selection
                            clsAD.EventTypeID = short.Parse(dt.Rows[i]["EventTypeID"].ToString());
                            DataTable dtEventTime = clsAD.GetEventDialTimeByEventTypeID();
                            if (dtEventTime.Rows.Count > 0)
                            {
                                SetEventCallTime(dtEventTime, tp_DialTime1);
                            }
                            lbl_CallDays1.Text = tabPan_1.HeaderText + " Days :";

                            if (tabPan_1.HeaderText == "Set Call")
                            {
                                lbl_CallDaysBusinessDays1.Text = "Business Days After Set Date";
                                FillDays(14, ddl_Days1);
                                ddl_Days1.SelectedValue = dt.Rows[i]["EventTypeDays"].ToString();
                            }
                            else if (tabPan_1.HeaderText == "Reminder Call")
                            {
                                lbl_CallDaysBusinessDays1.Text = "Business Days Before Trial Setting";
                                FillDays(21, ddl_Days1);
                                ddl_Days1.SelectedValue = dt.Rows[i]["EventTypeDays"].ToString();
                            }
                        }
                        else if (i == 1)
                        {
                            hf_EventID2.Value = dt.Rows[i]["EventTypeID"].ToString();
                            tabPan_2.HeaderText = dt.Rows[i]["EventTypeName"].ToString();
                            ddl_Courts2.SelectedValue = dt.Rows[i]["CourtID"].ToString();
                            ddl_Range2.SelectedValue = dt.Rows[i]["SettingDateRange"].ToString();
                            bool eventState = Convert.ToBoolean(dt.Rows[i]["EventState"]);
                            if (eventState == true)
                            {
                                rb_Start2.Checked = true;
                                rb_Stop2.Checked = false;
                            }
                            else
                            {
                                rb_Start2.Checked = false;
                                rb_Stop2.Checked = true;
                            }
                            //Farrukh 9332 07/28/2011 replaced timepicker with listbox for multi selection
                            clsAD.EventTypeID = short.Parse(dt.Rows[i]["EventTypeID"].ToString());
                            DataTable dtEventTime = clsAD.GetEventDialTimeByEventTypeID();
                            if (dtEventTime.Rows.Count > 0)
                            {
                                SetEventCallTime(dtEventTime, tp_DialTime2);
                            }

                            lbl_CallDays2.Text = tabPan_2.HeaderText + " Days :";
                            if (tabPan_2.HeaderText == "Set Call")
                            {
                                lbl_CallDaysBusinessDays2.Text = "Business Days After Set Date";
                                FillDays(14, ddl_Days2);
                                ddl_Days2.SelectedValue = dt.Rows[i]["EventTypeDays"].ToString();
                            }
                            else if (tabPan_2.HeaderText == "Reminder Call")
                            {
                                lbl_CallDaysBusinessDays2.Text = "Business Days Before Trial Setting";
                                FillDays(21, ddl_Days2);
                                ddl_Days2.SelectedValue = dt.Rows[i]["EventTypeDays"].ToString();
                            }
                        }
                    }
                    tabs_EventType.ActiveTabIndex = 0;
                }

            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;
            }
        }

        private void SetEventCallTime(DataTable dt, CheckBoxList tp_DialTime)
        {
            if (dt.Rows.Count > 0)
            {
                for (int counter = 0; counter < dt.Rows.Count; counter++)
                {
                    tp_DialTime.Items.FindByText(
                        Convert.ToDateTime(dt.Rows[counter]["DialTime"].ToString()).ToShortTimeString()).Selected = true;
                }
            }
        }

        private void InsertEventCallTime(CheckBoxList tp_DialTime)
        {
            clsAD.DeleteEventCallTime();
            for (int counter = 0; counter < tp_DialTime.Items.Count - 1; counter++)
            {
                if (tp_DialTime.Items[counter].Selected)
                {
                    clsAD.DialTime = Convert.ToDateTime(tp_DialTime.Items[counter].Text);
                    clsAD.InsertEventCallTime();
                }
            }

        }

        #endregion

        
    }
}

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;
using System.Data.SqlClient;

namespace HTP.backroom
{
    //Agha Usman 4271 07/02/2008
    public partial class ValidationEmailCivil : System.Web.UI.Page
    {
        clsLogger clog = new clsLogger();
        ValidationReports VReport = new ValidationReports();

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                btn_email.Attributes.Add("onclick", "return ValidationEmail();");
                ViewState["ValidationEmailOthers"] = ConfigurationSettings.AppSettings["ValidationEmailOthers"].ToString();
            }
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            try
            {
                //Clear Messages 
                lbl_message.Text = "";
                lbl_result.Text = "";

                string email = "";

                if (rb_demail.Checked == false)
                    email = tb_emailaddress.Text;

                //Send Validation Report To The User
                VReport.GetValidationReport(email, false, CaseType.Civil);
                lbl_result.Text = "Execution Successful!!";

            }

            catch (SqlException ex)
            {
                lbl_message.Text = ex.Message.ToString();
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_result.Text = "Error In Procedure USP_HTS_ValidationEmailReportHouston";
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message.ToString();
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_result.Text = "Error In Procedure USP_HTS_ValidationEmailReportHouston";
            }
        }

        protected void btn_displayreport_Click(object sender, EventArgs e)
        {
            try
            {
                //Clear Messages 
                lbl_message.Text = "";
                lbl_result.Text = "";
                //checked by khalid for bug 2260 2-1-08
                string ReportContent = VReport.GetValidationReport("", true, CaseType.Civil);
                //Display Report If Content Exist
                if (ReportContent != "")
                {
                    pnl_report.Controls.Add(new LiteralControl(ReportContent));
                    pnl_report.Visible = true;
                }
                else
                {
                    pnl_report.Visible = false;
                }

            }
            catch (Exception ex)
            {
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_message.Text = ex.Message.ToString();
            }
        }

        #endregion
    }
}

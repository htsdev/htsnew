using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;


namespace lntechNew
{
    public partial class frmMainNew : System.Web.UI.Page
    {
        clsENationWebComponents ClsDb = new clsENationWebComponents();       
        clsCase ClsCase = new clsCase();
        clsLogger bugTracker = new clsLogger();
        clsSession ClsSession = new clsSession();
        DataSet ds_Result;
        DataView dv_Result;
        string StrExp = String.Empty;
        string StrAcsDec = String.Empty;

        #region Variables

        private static string TicketNo = String.Empty;
        private static string SearchKeyWord1 = String.Empty;
        private static int SearchKeyType1;
        private static string SearchKeyWord2 = String.Empty;
        private static int SearchKeyType2;
        private static string SearchKeyWord3 = String.Empty;
        private static int SearchKeyType3;
        protected System.Web.UI.WebControls.Label lblMessage;
        protected System.Web.UI.WebControls.Label Message;
        protected System.Web.UI.WebControls.LinkButton lnkbtn_AddNewTicket;       
        protected System.Web.UI.WebControls.Button btn_Reset;
        protected System.Web.UI.WebControls.DropDownList ddlSearchKeyType1;
        private static int FilterType = 0;

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
           try
           {
              
               imgbtn_searchnor.Attributes.Add("OnClick", "javascript: return ValidateInput();");
               imgbtn_Search.Attributes.Add("onclick", "javascript: return ValidateInput();");

               lnkbtn_reset.Attributes.Add("OnClick", "return ResetControls();");
               btn_Reset.Attributes.Add("OnClick", "return ResetControls();");
               lnkbtn_adnvace.Attributes.Add("OnClick", "return ChangeCriteria('Advance');");

               // Check for valid Session if not then redirect to login page					
               if (ClsSession.IsValidSession(this.Request) == false)
               {
                   Response.Redirect("frmLogin.aspx", false);
               }
               else //To stop page further execution
               {
                   if (!IsPostBack)
                   {
                                                                                       
                       //Mid# Link Functionality			
                       if (Request.QueryString.Count == 3)
                       {                         
                           txtSearchKeyWord1.Text = "";
                           txtSearchKeyWord3.Text = ""; 
                           ddlSearchKeyType1.ClearSelection();
                           ddlSearchKeyType3.ClearSelection();
                           txtSearchKeyWord2.Text = Request.QueryString["lstcriteriaValue3"];
                           ddlSearchKeyType2.SelectedValue = Request.QueryString["lstcriteria3"];
                           int search = Convert.ToInt32(Request.QueryString["search"]);
                           if (search == 0)//client
                           {
                               rdbtnClientad.Checked = true;
                               rdbtnJimsad.Checked = false;
                               rdbtnNonClientad.Checked = false;
                               rdbtnQuotead.Checked = false;

                               rdbtnClient.Checked = true;                               
                               rdbtnJims.Checked = false;
                               rdbtnNonClient.Checked = false;
                               rdbtnQuote.Checked = false;                               
                           }
                           else if (search == 1)//quote
                           {
                               rdbtnQuotead.Checked = true;
                               rdbtnClientad.Checked = false;
                               rdbtnJimsad.Checked = false;
                               rdbtnNonClientad.Checked = false;

                               rdbtnQuote.Checked = true;
                               rdbtnClient.Checked = false;                               
                               rdbtnJims.Checked = false;
                               rdbtnNonClient.Checked = false;
                           }
                           else if (search == 2)//non client
                           {
                               rdbtnNonClientad.Checked = true;
                               rdbtnClientad.Checked = false;
                               rdbtnJimsad.Checked = false;
                               rdbtnQuotead.Checked = false;

                               rdbtnNonClient.Checked = true;
                               rdbtnClient.Checked = false;                               
                               rdbtnJims.Checked = false;
                               rdbtnQuote.Checked = false;
                           }
                           else if (search == 3)//archive
                           {
                               rdbtnClientad.Checked = false;
                               rdbtnJimsad.Checked = false;
                               rdbtnNonClientad.Checked = false;
                               rdbtnQuotead.Checked = false;

                               rdbtnClient.Checked = false;
                               rdbtnJims.Checked = false;
                               rdbtnNonClient.Checked = false;
                               rdbtnQuote.Checked = false;

                           }
                           else if (search == 4)//jims
                           {
                               rdbtnJimsad.Checked = true;
                               rdbtnClientad.Checked = false;
                               rdbtnNonClientad.Checked = false;
                               rdbtnQuotead.Checked = false;

                               rdbtnJims.Checked = true;
                               rdbtnClient.Checked = false;                               
                               rdbtnNonClient.Checked = false;
                               rdbtnQuote.Checked = false;

                           }
                           SetParameters();
                           GetResult();
                       }
                   }
               }
           }
           catch (Exception ex)
           {
               lblMessage.Text = ex.Message;
               bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
           }
        }
      
        //Sets the parameters which are used in sp
		private void SetParameters()
		{
			try
			{
				FilterType=0;				
				SearchKeyWord1=txtSearchKeyWord1.Text.Trim();
				SearchKeyType1=Convert.ToInt32(ddlSearchKeyType1.SelectedValue );
                //// if advance button is pressed 
                if (hf_btnpressed.Value == "Advance")
                {
                    SearchKeyWord1 = txtSearchKeyWord1.Text.Trim();
                    SearchKeyType1 = Convert.ToInt32(ddlSearchKeyType1.SelectedValue);
                    SearchKeyWord2 = txtSearchKeyWord2ad.Text.Trim();
                    SearchKeyType2 = Convert.ToInt32(ddlSearchKeyType2ad.SelectedValue);                    
                    SearchKeyWord3 = txtSearchKeyWord3.Text.Trim();
                    SearchKeyType3 = Convert.ToInt32(ddlSearchKeyType3.SelectedValue);
                    
                }
                    /// if normal button is pressed
                else if (hf_btnpressed.Value == "NormalMode")
                {
                    SearchKeyType3 = 0;
                    SearchKeyType1 = 0;
                    SearchKeyWord1 = String.Empty;
                    SearchKeyWord3 =String.Empty;

                    SearchKeyWord2 = txtSearchKeyWord2.Text.Trim();
                    SearchKeyType2 = Convert.ToInt32(ddlSearchKeyType2.SelectedValue);
                }		
		        ///// If addvance button is pressed
                if(hf_btnpressed.Value  == "Advance")
                {
                    if (rdbtnClientad.Checked == true)
                    {
                        FilterType = 0;
                    }

                    if (rdbtnQuotead.Checked == true)
                    {
                        FilterType = 1;
                    }

                    if (rdbtnNonClientad.Checked == true)
                    {
                        FilterType = 2;
                    }
                    if (rdbtnJimsad.Checked == true)
                    {
                        FilterType = 4;
                    }
                }
                ////// if normal button is pressed
                if (hf_btnpressed.Value == "NormalMode")
                {
                    if (rdbtnClient.Checked == true)
                    {
                        FilterType = 0;
                    }

                    if (rdbtnQuote.Checked == true)
                    {
                        FilterType = 1;
                    }

                    if (rdbtnNonClient.Checked == true)
                    {
                        FilterType = 2;
                    }
                    if (rdbtnJims.Checked == true)
                    {
                        FilterType = 4;
                    }
                }				
			}
			catch(Exception ex)
			{
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);			}
				
		}

        //Gets the result from sp
        private void GetResult()
        {
          TicketNo = String.Empty;
            try
            {
                ds_Result = ClsCase.SearchCaseInfo(TicketNo, SearchKeyWord1, SearchKeyType1, SearchKeyWord2, SearchKeyType2, SearchKeyWord3, SearchKeyType3, FilterType);

                if (ds_Result.Tables.Count > 0)
                {
                    //if sp return one or more rows
                    if (ds_Result.Tables[0].Rows.Count > 0)
                    {
                        // changed by tahir ahmed....
                        // to implement sorting on grid....
                        //dgResult.DataSource=ds_Result;

                        // getting data view from data set for sorting purpose...
                        dv_Result = ds_Result.Tables[0].DefaultView;

                        // storing data view in session....
                        ClsSession.SetSessionVariable("dv_Result", dv_Result, this.Session);

                        // binding grid with the data view....
                        dgResult.DataSource = dv_Result;
                        dgResult.DataBind();
                        dgResult.Visible = true;
                        lblMessage.Text = "";
                    }
                    else
                    {
                        lblMessage.Text = "No record found.";
                        dgResult.DataBind();
                        dgResult.Visible = false;
                    }
                }
                else
                {
                    lblMessage.Text = "No record found.";
                    dgResult.DataBind();
                    dgResult.Visible = false;
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        protected void imgbtn_Search_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                lblMessage.Text = "";
                SetParameters();
                GetResult();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void btn_Reset_Click(object sender, EventArgs e)
        {

        }
       
        
        ////// Grid events //////////
        private void SortGrid(string SortExp)
        {
            try
            {

                if (ClsSession.GetSessionObject("dv_Result", this.Session) != null)
                {
                    SetAcsDesc(SortExp);
                    dv_Result = (DataView)(ClsSession.GetSessionObject("dv_Result", this.Session));
                    dv_Result.Sort = StrExp + " " + StrAcsDec;
                    dgResult.DataSource = dv_Result;
                    dgResult.DataBind();
                }
                else
                {
                    Response.Redirect("frmlogin.aspx", false);
                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void SetAcsDesc(string Val)
        {
            try
            {
                //StrExp = Session["StrExp"].ToString();
                //StrAcsDec = Session["StrAcsDec"].ToString();
                StrExp = ViewState["StrExp"].ToString();
                StrAcsDec = ViewState["StrAcsDec"].ToString();

            }
            catch { }

            if (StrExp == Val)
            {
                if (StrAcsDec == "ASC")
                {
                    StrAcsDec = "DESC";
                    //Session["StrAcsDec"] = StrAcsDec ;
                    ViewState["StrAcsDec"] = StrAcsDec;
                }
                else
                {
                    StrAcsDec = "ASC";
                    //Session["StrAcsDec"] = StrAcsDec ;
                    ViewState["StrAcsDec"] = StrAcsDec;
                }
            }
            else
            {
                StrExp = Val;
                StrAcsDec = "ASC";
                //Session["StrExp"] = StrExp ;
                //Session["StrAcsDec"] = StrAcsDec ;
                ViewState["StrExp"] = StrExp;
                ViewState["StrAcsDec"] = StrAcsDec;

            }
        }
              
        protected void dgResult_SortCommand(object source, DataGridSortCommandEventArgs e)
        {
            try
            {
                lblMessage.Text = "";
                SortGrid(e.SortExpression);
                 
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void dgResult_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            try
			{
				if (e.Item.ItemType == ListItemType.Header) 
				{
		
				}
				else
				{
					if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
					{
						e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='#FFF2D9'");
					}
					
					if (e.Item.ItemType == ListItemType.Item)
					{
						e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#EFF4FB'");
					}
					else
					{
						e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#EFF4FB'");
					}
				}

				DataRowView drv = (DataRowView) e.Item.DataItem;
				if( drv==null) return ;
			
				string strCaseNumber = drv["CaseNumber"].ToString();
				
				if (( rdbtnClient.Checked == true) || ( rdbtnClient.Checked == true))//Client
				{
					((HyperLink) e.Item.FindControl("HLMidNumber")).NavigateUrl = "frmMainNew.aspx?search=0&lstcriteriaValue3=" + drv["MIDNo"] + "&lstcriteria3=3";
				}
				if (( rdbtnQuote.Checked == true) ||( rdbtnQuotead.Checked == true))//Quote
				{					
					((HyperLink) e.Item.FindControl("HLMidNumber")).NavigateUrl = "frmMainNew.aspx?search=1&lstcriteriaValue3=" + drv["MIDNo"] + "&lstcriteria3=3";
				}
				if (( rdbtnNonClient.Checked == true) || ( rdbtnNonClientad.Checked == true))//NonClient
				{
					if( strCaseNumber.StartsWith("F")==true )
					{
						LinkButton lbtn=	(LinkButton) e.Item.FindControl("lnkbtnCaseNo");
						HyperLink hlnk=	(HyperLink) e.Item.FindControl("HLCaseNo");
						lbtn.Visible=false;
						hlnk.Visible=true;
					}
					((HyperLink) e.Item.FindControl("HLMidNumber")).NavigateUrl = "frmMainNew.aspx?search=2&lstcriteriaValue3=" + drv["MIDNo"] + "&lstcriteria3=3"  ;
					if( strCaseNumber.StartsWith("F")!=true ) // if caseNo starts with F (Failure to appear), has no link
					{
						if (strCaseNumber.IndexOf('-')!=-1)// -1 means '-' is found in strCaseNumber
						{
							strCaseNumber = strCaseNumber.Substring(0, strCaseNumber.IndexOf('-') );
						}
						
					}
				}
                //if ( rdbtnArchive.Checked == true)//Archive
                //{
                //    if( strCaseNumber.StartsWith("F")==true )
                //    {
                //        LinkButton lbtn=	(LinkButton) e.Item.FindControl("lnkbtnCaseNo");
                //        HyperLink hlnk=	(HyperLink) e.Item.FindControl("HLCaseNo");
                //        lbtn.Visible=false;
                //        hlnk.Visible=true;
                //    }
                //    ((HyperLink) e.Item.FindControl("HLMidNumber")).NavigateUrl = "frmMain.aspx?search=3&lstcriteriaValue3=" + drv["MIDNo"] + "&lstcriteria3=3"  ;
                //    if( strCaseNumber.StartsWith("F")!=true ) // if caseNo starts with F (Failure to appear), has no link
                //    {
                //        if (strCaseNumber.IndexOf('-')!=-1)// -1 means '-' is found in strCaseNumber
                //        {
                //            strCaseNumber = strCaseNumber.Substring(0, strCaseNumber.IndexOf('-') );
                //        }
						
                //    }
                //}
				if (( rdbtnJims.Checked == true) || ( rdbtnJimsad.Checked == true))//Jims
				{
					if( strCaseNumber.StartsWith("F")==true )
					{
						LinkButton lbtn=	(LinkButton) e.Item.FindControl("lnkbtnCaseNo");
						HyperLink hlnk=	(HyperLink) e.Item.FindControl("HLCaseNo");
						lbtn.Visible=false;
						hlnk.Visible=true;
					}
					((HyperLink) e.Item.FindControl("HLMidNumber")).NavigateUrl = "frmMainNew.aspx?search=4&lstcriteriaValue3=" + drv["MIDNo"] + "&lstcriteria3=3"  ;
					if( strCaseNumber.StartsWith("F")!=true ) // if caseNo starts with F (Failure to appear), has no link
					{
						if (strCaseNumber.IndexOf('-')!=-1) // -1 means '-' is found in strCaseNumber
						{
							strCaseNumber = strCaseNumber.Substring(0, strCaseNumber.IndexOf('-') );
						}
						
					}
				}
				//Status Fore Color
				Label Status=(Label) e.Item.FindControl("Label1");
				string strStatus = drv["CaseStatus"].ToString().Trim();
				switch (strStatus)
				{
					case "Non Client":
						Status.ForeColor=System.Drawing.Color.Black; 
						break;
					case "Arraignment":
						Status.ForeColor=System.Drawing.Color.Green; 
						break;
					case "Jury Trial Setting":
						Status.ForeColor=System.Drawing.Color.Green;
						break;
					case "Pre Trial":
						Status.ForeColor=System.Drawing.Color.Green; 
						break;
					case "Judge Trial":
						Status.ForeColor=System.Drawing.Color.Green; 
						break;
					case "PreTrial Setting":
						Status.ForeColor=System.Drawing.Color.Green; 
						break;
					case "Return":
						Status.ForeColor=System.Drawing.Color.Green;
						break;
					case "Closed":
						Status.ForeColor=System.Drawing.Color.Brown; 
						break;
					case "Missed Court":
						Status.ForeColor=System.Drawing.Color.Brown; 
						break;
					case "DISPOSED":
						Status.ForeColor=System.Drawing.Color.Purple; 
						break;
					case "Quote":
						Status.ForeColor=System.Drawing.Color.Blue; 
						break;
					case "---Choose----":
						Status.Text="Choose";
						Status.ForeColor=System.Drawing.Color.Orange;
						break;
					default:
						Status.ForeColor=System.Drawing.Color.Orange; 
						break;
				}

			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message;
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);	}
        }

        protected void dgResult_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "CaseNo")
                {

                    string strCaseNumber = ((LinkButton)e.Item.FindControl("lnkbtnCaseNo")).Text;
                    string ticketid = ((Label)e.Item.FindControl("lblTicketId")).Text;
                    string courtid = ((Label)e.Item.FindControl("lblCourtId")).Text;
                    string zipcode = ((Label)e.Item.FindControl("lblZip")).Text;
                    string Add1 = ((Label)e.Item.FindControl("lblAdd1")).Text;
                    string MidNum = ((HyperLink)e.Item.FindControl("HLMidNumber")).Text;
                    if ((rdbtnClient.Checked == true) || (rdbtnClientad.Checked == true))//Client
                    {
                        Response.Redirect("ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + ticketid, false);
                    }
                    else if (rdbtnQuote.Checked == true)//Quote
                    {
                        Response.Redirect("ClientInfo/ViolationFeeold.aspx?search=1&caseNumber=" + ticketid, false);
                    }
                    else if ((rdbtnNonClient.Checked == true) || (rdbtnNonClientad.Checked == true))//NonClient
                    {
                        if (strCaseNumber.StartsWith("F") != true) // if caseNo starts with F (Failure to appear), has no link
                        {
                            if (strCaseNumber.IndexOf('-') != -1)// -1 means '-' is found in strCaseNumber
                            {
                                strCaseNumber = strCaseNumber.Substring(0, strCaseNumber.IndexOf('-'));
                            }

                            //
                            string[] ticketidFlag = ClsCase.GetCaseInfoFromAllNonClients(999, strCaseNumber, Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request)), Add1, zipcode, 0, MidNum);
                            if (ticketidFlag[1] != "2")
                            {
                                HttpContext.Current.Response.Write("<script language='javascript'>alert('Profile Already Exists. Please search this case in CLIENT or QUOTE By Ticket Number or by Cause Number.'); void(0); </script>");
                                //Response.Redirect("ClientInfo/ViolationFeeold.aspx?search=" + ticketidFlag[1] + "&caseNumber=" + ticketidFlag[0],false);
                            }
                            else
                            {
                                Response.Redirect("ClientInfo/ViolationFeeold.aspx?search=1&caseNumber=" + ticketidFlag[0], false);
                            }
                            //							
                        }
                    }
                    //else if (rdbtnArchive.Checked == true)//Archive
                    //{
                    //    if (strCaseNumber.StartsWith("F") != true) // if caseNo starts with F (Failure to appear), has no link
                    //    {
                    //        if (strCaseNumber.IndexOf('-') != -1)// -1 means '-' is found in strCaseNumber
                    //        {
                    //            strCaseNumber = strCaseNumber.Substring(0, strCaseNumber.IndexOf('-'));
                    //        }

                    //        //
                    //        string[] ticketidFlag = ClsCase.GetCaseInfoFromAllNonClients(5, strCaseNumber, Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request)), Add1, zipcode, MidNum);
                    //        if (ticketidFlag[1] != "2")
                    //        {
                    //            HttpContext.Current.Response.Write("<script language='javascript'>alert('Profile Already Exists. Please search this case in CLIENT or QUOTE By Ticket Number or by Cause Number.'); void(0); </script>");
                    //            //Response.Redirect("ClientInfo/ViolationFeeold.aspx?search=1&caseNumber=" + ticketidFlag[0],false);
                    //        }
                    //        else
                    //        {
                    //            //Response.Redirect("ClientInfo/ViolationFeeold.aspx?search=" + ticketidFlag[1] + "&caseNumber=" + ticketidFlag[0],false);
                    //            Response.Redirect("ClientInfo/ViolationFeeold.aspx?search=1&caseNumber=" + ticketidFlag[0], false);
                    //        }
                    //        //

                    //        //Response.Redirect("ClientInfo/ViolationFeeold.aspx?search=5&caseNumber=" + strCaseNumber,false);													
                    //    }
                    //}
                    else if ((rdbtnJims.Checked == true) ||(rdbtnJimsad.Checked == true))//Jims
                    {
                        if (strCaseNumber.StartsWith("F") != true) // if caseNo starts with F (Failure to appear), has no link
                        {
                            if (strCaseNumber.IndexOf('-') != -1) // -1 means '-' is found in strCaseNumber
                            {
                                strCaseNumber = strCaseNumber.Substring(0, strCaseNumber.IndexOf('-'));
                            }

                            //
                            string[] ticketidFlag = ClsCase.GetCaseInfoFromAllNonClients(3, strCaseNumber, Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request)), "", "",0, "");
                            if (ticketidFlag[1] != "2")
                            {
                                HttpContext.Current.Response.Write("<script language='javascript'>alert('Profile Already Exists. Please search this case in CLIENT or QUOTE By Ticket Number or by Cause Number.'); void(0); </script>");
                                //Response.Redirect("ClientInfo/ViolationFeeold.aspx?search=" + ticketidFlag[1] + "&caseNumber=" + ticketidFlag[0],false);
                            }
                            else
                            {
                                Response.Redirect("ClientInfo/ViolationFeeold.aspx?search=1&caseNumber=" + ticketidFlag[0], false);
                            }
                            //
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
       
    }
}

﻿<%@ Register TagPrefix="uc1" TagName="Footer" Src="WebControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="WebControls/ActiveMenu.ascx" %>

<%@ Page Language="c#" AutoEventWireup="false" Inherits="HTP.frmMain" SmartNavigation="False"
    CodeBehind="frmMain.aspx.cs" ValidateRequest="false" EnableEventValidation="false"  %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Traffic Ticket CRM | Home</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <link href="Styles.css" type="text/css" rel="stylesheet" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />

    <script src="Scripts/Validationfx.js" type="text/javascript"></script>

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="assets/images/apple-touch-icon-57-precomposed.png" />
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/images/apple-touch-icon-114-precomposed.png" />
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/images/apple-touch-icon-72-precomposed.png" />
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/images/apple-touch-icon-144-precomposed.png" />
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS TEMPLATE - END -->
    <style type="text/css">
        .content-body table tr {
            background-color: transparent !important;
        }

        #dgResult tr {
            background: transparent;
        }
        #tblColorCode {
            display:inline !important;
        }
    </style>

    <script lang="javascript" type="text/javascript">


        //ozair 4625 08/27/2008 removed classic search option
        function KeyDownHandler() {
            //Process only the Enter key
            if (event.keyCode == 13) {
                //Cancel the default submit
                event.returnValue = false;
                event.cancel = true;

                //Submit the form by programmatically clicking the specified button
                document.getElementById("btnSearchAdvance").click();
            }
        }

        function ValidateInputAdvance() {
            //error list
            var error1 = 'Choose one of 3 criterias';
            var error2 = 'Ticket Number must be empty';
            var error3 = 'Choose any criteria from dropdownlist';

            //var txtTicketNumber = document.getElementById("txtTicketNumber").value;
            var txtSearchKeyWord1ad = document.getElementById("txtSearchKeyWord1ad").value;
            var txtSearchKeyWord2ad = document.getElementById("txtSearchKeyWord2ad").value;
            var txtSearchKeyWord3ad = document.getElementById("txtSearchKeyWord3ad").value;
            var txtTicketNumberad = document.getElementById("txtTicketNumberad").value;
            var txtlid = document.getElementById("txtLidad").value;

            var ddlSearchKeyType1ad = document.getElementById("ddlSearchKeyType1ad");
            var ddlSearchKeyType2ad = document.getElementById("ddlSearchKeyType2ad");
            var ddlSearchKeyType3ad = document.getElementById("ddlSearchKeyType3ad");

            var Message = document.getElementById("Message");

            var flag = false;
            var V;
            if (txtlid != "") {
                for (i = 0; i < txtlid.length; i++) {
                    V = txtlid.charAt(i);
                    if (isNaN(V)) {
                        flag = true;
                        break;
                    }
                }
            }
            if (flag == true) {
                /*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
                Message.innerHTML = "Invalid Value";
                document.getElementById("grid").style.display = 'none';
                return false;
            }


            if (txtTicketNumberad == "") {
                if ((txtSearchKeyWord1ad != "") && (ddlSearchKeyType1ad.value == -1)) {/*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
                    Message.innerHTML = error3;
                    document.getElementById("grid").style.display = 'none';
                    return false;
                }

                if ((txtSearchKeyWord2ad != "") && (ddlSearchKeyType2ad.value == -1)) {/*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
                    Message.innerHTML = error3;
                    document.getElementById("grid").style.display = 'none';
                    return false;
                }

                if ((txtSearchKeyWord3ad != "") && (ddlSearchKeyType3ad.value == -1)) {/*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
                    Message.innerHTML = error3;
                    document.getElementById("grid").style.display = 'none';
                    return false;
                }
            }

            if (isNaN(txtlid)) {/*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
                Message.innerHTML = "Please enter numeric value.";
                document.getElementById("grid").style.display = 'none';
                return false;
            }

            if (txtlid != "" && txtlid.length > 8) {/*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
                Message.innerHTML = "Letter Id cannot be greater than eight character.";
                document.getElementById("grid").style.display = 'none';
                return false;
            }

            if ((txtSearchKeyWord1ad != "") && (ddlSearchKeyType1ad.value == -1)) {/*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
                Message.innerHTML = error3;
                document.getElementById("grid").style.display = 'none';
                return false;
            }

            if ((txtSearchKeyWord2ad != "") && (ddlSearchKeyType2ad.value == -1)) {/*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
                Message.innerHTML = error3;
                document.getElementById("grid").style.display = 'none';
                return false;
            }
            if ((txtSearchKeyWord3ad != "") && (ddlSearchKeyType3ad.value == -1)) {/*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
                Message.innerHTML = error3;
                document.getElementById("grid").style.display = 'none';
                return false;
            }

            //Zeeshan Ahmed 3124 02/19/2008				
            //Validate First Keyword
            if (!ValidateKeyWord('txtSearchKeyWord1ad', 'ddlSearchKeyType1ad')) return false;

            //Validate Second Keyword
            if (!ValidateKeyWord('txtSearchKeyWord2ad', 'ddlSearchKeyType2ad')) return false;

            //Validate Third Keyword
            if (!ValidateKeyWord('txtSearchKeyWord3ad', 'ddlSearchKeyType3ad')) return false;

            document.getElementById("grid").style.display = 'block';
            /*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
            Message.innerHTML = "";

            //Nasir 5863 05/30/2009 payment date and hire date cannot be seach at the same time
            if ($get("rdbtnQuotead").checked) {
                var ddlSearchKeyType1 = document.getElementById("ddlSearchKeyType1ad");
                var ddlSearchKeyType2 = document.getElementById("ddlSearchKeyType2ad");
                var ddlSearchKeyType3 = document.getElementById("ddlSearchKeyType3ad");
                var ddlSearchKeyWord1 = document.getElementById("txtSearchKeyWord1ad");
                var ddlSearchKeyWord2 = document.getElementById("txtSearchKeyWord2ad");
                var ddlSearchKeyWord3 = document.getElementById("txtSearchKeyWord3ad");
                if (ddlSearchKeyType1.options[11].selected && ddlSearchKeyWord1.value != "") {
                    if ((ddlSearchKeyType2.options[12].selected && ddlSearchKeyWord2.value != "") || (ddlSearchKeyType3.options[12].selected && ddlSearchKeyWord3.value != "")) {/*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
                        Message.innerHTML = "System not allow to group search on hire date and payment date simultaneously"
                        return false;
                    }
                }
                if (ddlSearchKeyType2.options[11].selected && ddlSearchKeyWord2.value != "") {
                    if ((ddlSearchKeyType1.options[12].selected && ddlSearchKeyWord1.value != "") || (ddlSearchKeyType3.options[12].selected && ddlSearchKeyWord3.value != "")) {/*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
                        Message.innerHTML = "System not allow to group search on hire date and payment date simultaneously"
                        return false;
                    }
                }
                if (ddlSearchKeyType3.options[11].selected && ddlSearchKeyWord3.value != "") {
                    if ((ddlSearchKeyType1.options[12].selected && ddlSearchKeyWord1.value != "") || (ddlSearchKeyType2.options[12].selected && ddlSearchKeyWord2.value != "")) {/*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
                        Message.innerHTML = "System not allow to group search on hire date and payment date simultaneously"
                        return false;
                    }
                }
                if (ddlSearchKeyType1.options[12].selected && ddlSearchKeyWord1.value != "") {
                    if ((ddlSearchKeyType2.options[11].selected && ddlSearchKeyWord2.value != "") || (ddlSearchKeyType3.options[11].selected && ddlSearchKeyWord3.value != "")) {/*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
                        Message.innerHTML = "System not allow to group search on hire date and payment date simultaneously"
                        return false;
                    }
                }
                if (ddlSearchKeyType2.options[12].selected && ddlSearchKeyWord2.value != "") {
                    if ((ddlSearchKeyType1.options[11].selected && ddlSearchKeyWord1.value != "") || (ddlSearchKeyType3.options[11].selected && ddlSearchKeyWord3.value != "")) {/*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
                        Message.innerHTML = "System not allow to group search on hire date and payment date simultaneously"
                        return false;
                    }
                }
                if (ddlSearchKeyType3.options[12].selected && ddlSearchKeyWord3.value != "") {
                    if ((ddlSearchKeyType1.options[11].selected && ddlSearchKeyWord1.value != "") || (ddlSearchKeyType2.options[11].selected && ddlSearchKeyWord2.value != "")) {/*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
                        Message.innerHTML = "System not allow to group search on hire date and payment date simultaneously"
                        return false;
                    }
                }
            }

            //Nasir 4921 07/15/2009 check for enter at least one value to search

            if ($get("txtLidad").value == "" && $get("txtcausenumberad").value == "" && $get("txtTicketNumberad").value == "" && $get("txtSearchKeyWord1ad").value == "" && $get("txtSearchKeyWord2ad").value == "" && $get("txtSearchKeyWord3ad").value == "") {
                alert("Blank search is not allowed, Please enter some value.")
                return false;
            }

        }
    </script>

    <script type="text/javascript">


        //ozair 4625 08/27/2008 removed classic search option      
        function ResetControls() {
            document.getElementById("txtSearchKeyWord3ad").value = "";
            document.getElementById("txtSearchKeyWord2ad").value = "";
            document.getElementById("txtSearchKeyWord1ad").value = "";
            document.getElementById("txtLidad").value = "";
            document.getElementById("txtTicketNumberad").value = "";
            document.getElementById("txtcausenumberad").value = "";
            document.getElementById("ddlSearchKeyType1ad").selectedIndex = 3;
            document.getElementById("ddlSearchKeyType2ad").selectedIndex = 2;
            document.getElementById("ddlSearchKeyType3ad").selectedIndex = 5;
            document.getElementById("ddlCourtsAd").selectedIndex = 0;

            document.getElementById("rdbtnClientad").checked = true;
            document.getElementById("rdbtnQuotead").checked = true;
            document.getElementById("rdbtnNonClientad").checked = false;
            document.getElementById("rdbtnJimsad").checked = false;

            if (document.getElementById("dgResult") != null)
                document.getElementById("dgResult").style.display = "none";

            if (document.getElementById("lblResults") != null)
                document.getElementById("lblResults").style.display = "none";

            if (document.getElementById("tblColorCode") != null)
                document.getElementById("tblColorCode").style.display = "none";
            /*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
            document.getElementById("Message").innerHTML = "";
            //Nasir 4921 07/16/2009 resolve reset issue
            AddSearchOption();
            return false;
        }

        function CheckCaseType() {
            if (document.getElementById("ddlCaseType").value == "0") {
                alert("Please select case type.");
                document.getElementById("ddlCaseType").focus();
                return false;
            }
            closeViolationAmountPopup();
            return true;
        }

        function closeViolationAmountPopup() {
            var modalPopupBehavior = $find('MPECaseCategory');
            modalPopupBehavior.hide();
            return false;
        }

    </script>

    <script id="SmartValidations">


        function ValidateKeyWord(textboxid, listid) {

            var searchText = document.getElementById(textboxid).value;
            var option = document.getElementById(listid).value;
            var Message = document.getElementById("Message");

            if (option != -1 && searchText != "") {

                //Validate DOB , Court Date and Contact Date
                //Nasir 5863 05/06/2009 check for hire date and payment date and contact date
                if (option == 4 || option == 6 || option == 8 || option == 11 || option == 12 || option == 13) {
                    if (chkdob(searchText) == false) {/*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
                        Message.innerHTML = "Invalid Date";
                        document.getElementById("grid").style.display = 'none';
                        return false;
                    }

                    if (!CheckDate(searchText)) {        /*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
                        Message.innerHTML = "Date should be greater than or equal to 1/1/1900.";
                        return false;
                    }

                    if (option == 4 && CheckDOB(searchText) == false) {/*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
                        Message.innerHTML = "Date of birth can not be in future.";
                        document.getElementById("grid").style.display = 'none';
                        return false;
                    }
                }

                //Validate Phone Number
                if (option == 5) {
                    if (!checknumber(searchText))
                        //if (isNaN(searchText))
                    {          /*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
                        Message.innerHTML = "Invalid phone number.";
                        return false;
                    }
                }

                // Noufil 3478 03/24/2008 number are not allowed to input
                //Validate First Name and Last Name       
                if (option == 1 || option == 2) {
                    if (!isNaN(searchText)) {/*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
                        Message.innerHTML = "Name should only contains alphabets.";
                        return false;
                    }
                    if (alphanumeric(searchText) == false) {/*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
                        Message.innerHTML = "Name should only contains alphabets.";
                        return false;
                    }

                    /*if ( !CheckName(searchText))
                    {
                        Message.innerText = "Name should only contains alphabets.";            
                        return false;
                    }*/
                }
                //Validate Letter ID
                //ozair 4625 08/26/2008 input string error resolved
                //excluded check for criminal case type as we are also sending letters to them.
                if (option == 9) {

                    if (searchText != "") {
                        for (i = 0; i < searchText.length; i++) {
                            V = searchText.charAt(i);
                            if (isNaN(V)) {/*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
                                Message.innerHTML = "Invalid Value";
                                document.getElementById("grid").style.display = 'none';
                                return false;
                            }
                        }
                    }

                    if (searchText != "" && isNaN(searchText)) {/*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
                        Message.innerHTML = "Please enter numeric value.";
                        return false;
                    }

                    if (searchText != "" && searchText.length > 8) {/*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
                        Message.innerHTML = "Letter Id cannot be greater than eight character.";
                        return false;
                    }
                }

                if (option == 10) {
                    if (document.getElementById("rdbtnJimsad").checked == true) {/*Nasir 4921 07/16/2009 change innertext to innerHTML to show label in fire fix and chrome*/
                        Message.innerHTML = "Cause number can not be searched for Jims";
                        document.getElementById("grid").style.display = 'none';
                        return false;
                    }
                }
                //Fahad 9725 11/21/2011 Added section for Email address option
                if (option == 14)//When email address selected
                {
                    if (isEmail(searchText) == false) {
                        alert("Please enter Email Address in Correct format.");
                        document.getElementById(textboxid).focus();
                        return false;
                    }
                }

            }
            return true;
        }

        // noufil 4275 06/20/2008 This function checks the whether the input are numbers or not
        function checknumber(number) {
            var numaric = number;
            for (var j = 0; j < numaric.length; j++) {
                var alphaa = numaric.charAt(j);
                var hh = alphaa.charCodeAt(0);
                //if((hh > 47 && hh<58) || (hh > 64 && hh<91) || (hh > 96 && hh<123))
                // ASCII code for numbers
                if (!(hh > 47 && hh < 58)) {
                    return false;
                }
            }
            return true;
        }

        // noufil 4275 06/20/2008 This function checks the whether the input are alphanumeric or not
        function alphanumeric(alphane) {
            var numaric = alphane;
            for (var j = 0; j < numaric.length; j++) {
                var alphaa = numaric.charAt(j);
                var hh = alphaa.charCodeAt(0);
                //if((hh > 47 && hh<58) || (hh > 64 && hh<91) || (hh > 96 && hh<123))
                //ASCII code for aphabhets
                if (!((hh > 64 && hh < 91) || (hh > 96 && hh < 123))) {
                    return false;
                }
            }
        }


        //Check Date Of Birth
        function CheckDOB(dateString) {
            var dob = new Date(formatDate(dateString));
            var today = new Date();

            if (dob > today)
                return false;

            return true;
        }

        //Check Court Date Should Be Greater Than 1/1/1900
        function CheckDate(dateString) {
            var mindate = new Date("1/1/1900")
            var userDate = new Date(dateString);

            if (parseInt(userDate - mindate) < 0)
                return false;

            return true;
        }

        //Check Name
        function CheckName(name) {
            for (i = 0 ; i < name.length ; i++) {
                var asciicode = name.charCodeAt(i)
                //If not valid alphabet 
                if (!((asciicode >= 64 && asciicode <= 90) || (asciicode >= 97 && asciicode <= 122)))
                    return false;
            }
            return true;
        }
        //Nasir 5863 05/05/2009 remove option hire date payment date and contact date
        function RemoveSearchOption() {
            if (document.getElementById("ddlSearchKeyType1ad").options[11] != null) {
                if (document.getElementById("ddlSearchKeyType1ad").options[11].selected || document.getElementById("ddlSearchKeyType1ad").options[12].selected || document.getElementById("ddlSearchKeyType1ad").options[13].selected || document.getElementById("ddlSearchKeyType1ad").options[14].selected) {
                    document.getElementById("ddlSearchKeyType1ad").options[3].selected = true;
                    document.getElementById("txtSearchKeyWord1ad").value = '';
                }
                if (document.getElementById("ddlSearchKeyType1ad").options[11].selected || document.getElementById("ddlSearchKeyType2ad").options[12].selected || document.getElementById("ddlSearchKeyType2ad").options[13].selected || document.getElementById("ddlSearchKeyType2ad").options[14].selected) {
                    document.getElementById("ddlSearchKeyType2ad").options[2].selected = true;
                    document.getElementById("txtSearchKeyWord2ad").value = '';
                }
                if (document.getElementById("ddlSearchKeyType3ad").options[11].selected || document.getElementById("ddlSearchKeyType3ad").options[12].selected || document.getElementById("ddlSearchKeyType3ad").options[13].selected || document.getElementById("ddlSearchKeyType3ad").options[14].selected) {
                    document.getElementById("ddlSearchKeyType3ad").options[5].selected = true;
                    document.getElementById("txtSearchKeyWord3ad").value = '';
                }
                //Afaq 9245 07/21/2011 add one more line in 3 dropdown to remove email address.
                RemoveOptionsFromEnd(document.getElementById("ddlSearchKeyType1ad"), 4);
                RemoveOptionsFromEnd(document.getElementById("ddlSearchKeyType2ad"), 4);
                RemoveOptionsFromEnd(document.getElementById("ddlSearchKeyType3ad"), 4);
            }
        }

        //Nasir 5863 05/05/2009 add option hire date payment date and contact date
        function AddSearchOption() {
            //Afaq 9245 07/21/2011 add email address in the dropdown lists and call removesearchoption to remove the already added values.
            RemoveSearchOption();
            CreateAndAddOption('ddlSearchKeyType1ad', 'Hire Date', '11');
            CreateAndAddOption('ddlSearchKeyType1ad', 'Payment Date', '12');
            CreateAndAddOption('ddlSearchKeyType1ad', 'Last Contact Date', '13');
            CreateAndAddOption('ddlSearchKeyType1ad', 'Email address', '14');

            CreateAndAddOption('ddlSearchKeyType2ad', 'Hire Date', '11');
            CreateAndAddOption('ddlSearchKeyType2ad', 'Payment Date', '12');
            CreateAndAddOption('ddlSearchKeyType2ad', 'Last Contact Date', '13');
            CreateAndAddOption('ddlSearchKeyType2ad', 'Email address', '14');

            CreateAndAddOption('ddlSearchKeyType3ad', 'Hire Date', '11');
            CreateAndAddOption('ddlSearchKeyType3ad', 'Payment Date', '12');
            CreateAndAddOption('ddlSearchKeyType3ad', 'Last Contact Date', '13');
            CreateAndAddOption('ddlSearchKeyType3ad', 'Email address', '14');

        }

    </script>

    <%--Muhammad Nadir Siddiqui 9245 05/04/2011 Increased textbox length for Email address option--%>

    <script type="text/javascript">
        function setTextBoxLength(val, txtbox) {

            if (val == '14') {
                document.getElementById(txtbox).maxLength = 100;
            }
        }
    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="MainForm" method="post" runat="server" onkeypress="KeyDownHandler();">
        <div class="page-container row-fluid container-fluid">
            <aspnew:ScriptManager ID="smFrmMain" runat="server">
                <Scripts>
                    <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
                </Scripts>
            </aspnew:ScriptManager>

            <asp:Panel ID="pnl" runat="server">
                <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
            </asp:Panel>
            <section id="main-content" class="" id="TableMain">
        <section class="wrapper main-wrapper row homePageMain" id="" style="">

             <div class="col-xs-12">
        <div class="page-title">

            <div class="pull-left">
                <!-- PAGE HEADING TAG - START --><h1 class="title">SEARCH</h1><!-- PAGE HEADING TAG - END -->                           

            </div>
            </div>
                 </div>


            <div class="clearfix"></div>

            <section class="box" id="">

                <header class="panel_header">

                         <h2 class="title pull-left">
                             Search
                           

                         </h2>
                         <div class="actions panel_actions pull-right">
                      
                             <a class="box_toggle fa fa-chevron-down"></a>
                    
                    </div>
                </header>
                     

                 <div class="content-body" id="dSearchCriteria2" runat="server" enableviewstate="true">
                <div class="row">

                     <div class="col-md-4">
                                                            <div class="form-group">
                                <label class="form-label">LID Number</label>
                                <span class="desc"></span>
                                <div class="controls">
                                     <asp:TextBox CssClass="form-control" ID="txtLidad" runat="server" TabIndex="1"
                                     MaxLength="8" ></asp:TextBox>
                                    </div>
                                                                </div>
                         </div>
                   

                      <div class="col-md-4">
                                                            <div class="form-group">
                                <label class="form-label">Cause Number</label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <asp:TextBox CssClass="form-control" ID="txtcausenumberad" runat="server"
                                    TabIndex="2"  MaxLength="20" ></asp:TextBox>
                                    
                                    </div>
                                                                </div>
                         </div>


                      <div class="col-md-4">
                                                            <div class="form-group">
                                <label class="form-label">Ticket Number</label>
                                <span class="desc"></span>
                                <div class="controls">
                                      <asp:TextBox CssClass="form-control" ID="txtTicketNumberad" runat="server"
                                    TabIndex="3"  MaxLength="20" ></asp:TextBox>
                                    </div>
                                                                </div>
                         </div>

                    <div class="clearfix"></div>

                     <div class="col-md-4">
                                                            <div class="form-group searchCustom1">
                                <asp:DropDownList ID="ddlSearchKeyType2ad" runat="server" CssClass="form-control"
                                    TabIndex="4"  onchange="setTextBoxLength(this.value,'txtSearchKeyWord2ad');">
                                    <asp:ListItem Value="-1">&lt; Choose &gt;</asp:ListItem>
                                    <asp:ListItem Value="0">Ticket Number</asp:ListItem>
                                    <asp:ListItem Value="1" Selected="True">Last Name</asp:ListItem>
                                    <asp:ListItem Value="2">First Name</asp:ListItem>
                                    <asp:ListItem Value="3">MID Number</asp:ListItem>
                                    <asp:ListItem Value="4">DOB</asp:ListItem>
                                    <asp:ListItem Value="5">Phone No</asp:ListItem>
                                    <asp:ListItem Value="6">Court Date</asp:ListItem>
                                    <asp:ListItem Value="7">Driver License</asp:ListItem>
                                    <asp:ListItem Value="9">LID</asp:ListItem>
                                    <asp:ListItem Value="10">Cause Number</asp:ListItem>
                                    <asp:ListItem Value="11">Hire Date</asp:ListItem>
                                    <asp:ListItem Value="12">Payment Date</asp:ListItem>
                                    <asp:ListItem Value="13">Last Contact Date</asp:ListItem>
                                    <asp:ListItem Value="14">Email address</asp:ListItem>
                                </asp:DropDownList>
                                <span class="desc"></span>
                                <div class="controls">
                                      <asp:TextBox ID="txtSearchKeyWord2ad" runat="server"  CssClass="form-control"
                                    TabIndex="5" MaxLength="50" Columns="30" ></asp:TextBox>
                                    </div>
                                                                </div>
                         </div>

                     <div class="col-md-4">
                                                            <div class="form-group searchCustom1">
                               <asp:DropDownList ID="ddlSearchKeyType1ad" runat="server" CssClass="form-control"
                                    TabIndex="6" onchange="setTextBoxLength(this.value,'txtSearchKeyWord1ad');">
                                    <asp:ListItem Value="-1">&lt; Choose &gt;</asp:ListItem>
                                    <asp:ListItem Value="0">Ticket Number</asp:ListItem>
                                    <asp:ListItem Value="1">Last Name</asp:ListItem>
                                    <asp:ListItem Value="2" Selected="True">First Name</asp:ListItem>
                                    <asp:ListItem Value="3">MID Number</asp:ListItem>
                                    <asp:ListItem Value="4">DOB</asp:ListItem>
                                    <asp:ListItem Value="5">Phone No</asp:ListItem>
                                    <asp:ListItem Value="6">Court Date</asp:ListItem>
                                    <asp:ListItem Value="7">Driver License</asp:ListItem>
                                    <asp:ListItem Value="9">LID</asp:ListItem>
                                    <asp:ListItem Value="10">Cause Number</asp:ListItem>
                                    <asp:ListItem Value="11">Hire Date</asp:ListItem>
                                    <asp:ListItem Value="12">Payment Date</asp:ListItem>
                                    <asp:ListItem Value="13">Last Contact Date</asp:ListItem>
                                    <asp:ListItem Value="14">Email address</asp:ListItem>
                                </asp:DropDownList>
                                <span class="desc"></span>
                                <div class="controls">
                                    <asp:TextBox ID="txtSearchKeyWord1ad" runat="server" CssClass="form-control"
                                    TabIndex="7"  MaxLength="50" Columns="30" ></asp:TextBox>
                                    </div>
                                                                </div>
                         </div>

                   

                     <div class="col-md-4">
                                                            <div class="form-group searchCustom1">
                               <asp:DropDownList ID="ddlSearchKeyType3ad" runat="server" CssClass="form-control"
                                    TabIndex="8"  onchange="setTextBoxLength(this.value,'txtSearchKeyWord3ad');">
                                    <asp:ListItem Value="-1">&lt; Choose &gt;</asp:ListItem>
                                    <asp:ListItem Value="0">Ticket Number</asp:ListItem>
                                    <asp:ListItem Value="1">Last Name</asp:ListItem>
                                    <asp:ListItem Value="2">First Name</asp:ListItem>
                                    <asp:ListItem Value="3">MID Number</asp:ListItem>
                                    <asp:ListItem Value="4" Selected="True">DOB</asp:ListItem>
                                    <asp:ListItem Value="5">Phone No</asp:ListItem>
                                    <asp:ListItem Value="6">Court Date</asp:ListItem>
                                    <asp:ListItem Value="7">Driver License</asp:ListItem>
                                    <asp:ListItem Value="9">LID</asp:ListItem>
                                    <asp:ListItem Value="10">Cause Number</asp:ListItem>
                                    <asp:ListItem Value="11">Hire Date</asp:ListItem>
                                    <asp:ListItem Value="12">Payment Date</asp:ListItem>
                                    <asp:ListItem Value="13">Last Contact Date</asp:ListItem>
                                    <asp:ListItem Value="14">Email address</asp:ListItem>
                                </asp:DropDownList>
                                <span class="desc"></span>
                                <div class="controls">
                                      <asp:TextBox ID="txtSearchKeyWord3ad" runat="server" CssClass="form-control"
                                    TabIndex="9" MaxLength="50" Columns="30" ></asp:TextBox>
                                    </div>
                                                                </div>
                         </div>

                    <div class="clearfix"></div>
                     <div class="col-md-4">
                                                            <div class="form-group">
                               <label class="form-label">Court Location</label>
                                <span class="desc"></span>
                                <div class="controls">
                                      <asp:DropDownList ID="ddlCourtsAd" runat="server" CssClass="form-control" TabIndex="10"
                                    >
                                    <asp:ListItem Value="0">&lt; All Courts &gt;</asp:ListItem>
                                    <asp:ListItem Value="-1">&lt; All HMC Courts &gt;</asp:ListItem>
                                    <asp:ListItem Value="-2">&lt; All HCJP Courts &gt;</asp:ListItem>
                                </asp:DropDownList>
                                    </div>
                                                                </div>
                         </div>

                     <div class="col-md-8">
                                                            <div class="form-group">
                              <asp:RadioButton ID="rdbtnClientad" runat="server" CssClass="checkbox-custom" Text="Client"
                                    GroupName="optCriteriaad" Height="16px" TabIndex="11" Style="display: none">
                                </asp:RadioButton>&nbsp;
                                
                                <span class="desc"></span>
                                <div class="controls" style="margin-top: 17px;">
                                    <asp:RadioButton ID="rdbtnQuotead" runat="server" Checked="True"
                                    CssClass="checkbox-custom" Text="Client/Quote" GroupName="optCriteriaad" onClick="AddSearchOption();"
                                    Height="16px" TabIndex="12"></asp:RadioButton>&nbsp;
                                    <asp:RadioButton ID="rdbtnNonClientad" runat="server" CssClass="checkbox-custom" Text="Traffic"
                                    GroupName="optCriteriaad" Height="16px" TabIndex="13" onClick="RemoveSearchOption();"
                                   ></asp:RadioButton>&nbsp;
                                <asp:RadioButton ID="rdbtnJimsad" runat="server" CssClass="checkbox-custom" Text="Criminal"
                                    GroupName="optCriteriaad" Height="16px" onClick="RemoveSearchOption();" TabIndex="14">
                                </asp:RadioButton>
                                <asp:RadioButton ID="rdbtnCivilad" runat="server" CssClass="checkbox-custom" GroupName="optCriteriaad"
                                    Height="16px" TabIndex="14" onClick="RemoveSearchOption();" Text="Family Law" />
                                    </div>
                                                                </div>
                         </div>

                     <div class="clearfix"></div>
                     <div class="col-md-12">
                                                            <div class="form-group">
                             
                                
                                <span class="desc"></span>
                                <div class="controls">
                                    <asp:Button ID="btnSearchAdvance" runat="server" CssClass="btn btn-primary" OnClick="btnSearchAdvance_Click"
                                    Text="Lookup" TabIndex="15" Width="85px" />
                                <asp:Button ID="btnResetAdvance" runat="server" CssClass="btn btn-primary" OnClick="btnResetAdvance_Click"
                                    Text="Reset" TabIndex="16" Width="85px" />
                                      <asp:Button ID="lnkbtn_AddNewTicket" runat="server" CssClass="btn btn-primary" TabIndex="17" style="color:#ffffff" Text="Add a New Case"></asp:Button>&nbsp;
                                    <asp:Label ID="lblResults" runat="server" Text="Results" Visible="False" CssClass="form-label"></asp:Label>
                                    </div>
                                                                </div>
                         </div>

                      <div class="clearfix"></div>

                     <div class="col-md-12" id="Table5">
                                                            <div class="form-group">
                             
                                
                                <span class="desc"></span>
                                <div class="controls">
                                    <asp:Label ID="lblMessage" runat="server" Font-Size="XX-Small" Font-Names="Verdana"
                    ForeColor="Red" Font-Bold="True"></asp:Label><asp:Label ID="Message" runat="server"
                        Font-Size="XX-Small" Font-Names="Verdana" ForeColor="Red" Font-Bold="True"></asp:Label>
                                    </div>
                                                                </div>
                         </div>












                    </div>
                     </div>

                 </section>

             <div class="clearfix"></div>

             <section class="box" id="">
                     

                 <header class="panel_header">
                     <h4 class="title pull-left">List </h4>
                            
                         <div class="actions panel_actions pull-right">
                             <table id="tblColorCode" runat="server" visible="false"  style="display: inline;">
                                <tr>
                                    <td>
                                        <asp:Label ID="lbl_greencolor" runat="server" Style="background-color: #A9F5A9; height: 15px;"
                                            BorderColor="Black" BorderWidth="1px" Width="20px"></asp:Label>&nbsp;
                                        <asp:Label ID="lbl_greentext" runat="server" Text="Clients"></asp:Label>&nbsp;&nbsp;
                                        <asp:Label ID="lbl_defaulfcolor" runat="server" Style="background-color: #E6EEF9;
                                            height: 15px;" BackColor="#E6EEF9" Width="20px" BorderColor="Black" BorderWidth="1px"></asp:Label>
                                        &nbsp;
                                        <asp:Label ID="lblellowtext" runat="server" Text="Quotes"></asp:Label>
                                        &nbsp;
                                        <asp:Label ID="lbl_yellowcolor" runat="server" Style="background-color: #F5E5B8;
                                            height: 15px;" Width="20px" BorderColor="Black" BorderWidth="1px"></asp:Label>&nbsp;
                                        <asp:Label ID="lbl_defaulttext" runat="server" Text="Non Clients"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                    

                            
                    
                    </div>
                </header>
                  <div class="content-body">
                      <%--<div class="row">
                          <div class="col-md-12">
                              <table id="tblColorCode">
	                            <tbody><tr>
		                            <td>
                                        <span id="lbl_greencolor" style="display:inline-block;border-color:Black;border-width:1px;border-style:solid;width:20px;background-color: #A9F5A9; height: 15px;"></span>&nbsp;
                                        <span id="lbl_greentext">Clients</span>&nbsp;&nbsp;
                                        <span id="lbl_defaulfcolor" style="display:inline-block;background-color:#E6EEF9;border-color:Black;border-width:1px;border-style:solid;width:20px;background-color: #E6EEF9;
                                            height: 15px;"></span>
                                        &nbsp;
                                        <span id="lblellowtext">Quotes</span>
                                        &nbsp;
                                        <span id="lbl_yellowcolor" style="display:inline-block;border-color:Black;border-width:1px;border-style:solid;width:20px;background-color: #F5E5B8;
                                            height: 15px;"></span>&nbsp;
                                        <span id="lbl_defaulttext">Non Clients</span>
                                    </td>
	                            </tr>
                            </tbody></table>
                          </div>
                      </div>--%>
                <div class="row">

                     <div class="col-md-12">
                                                            <div class="form-group">
                               
                                <span class="desc"></span>
                                <div class="controls">
                                    <div class="table-responsive" data-pattern="priority-columns">
                                     <asp:DataGrid ID="dgResult" runat="server" CssClass="table table-small-font table-bordered table-striped"
                    AutoGenerateColumns="False" PageSize="20" BorderColor="DarkGray" BorderStyle="Solid"
                    AllowSorting="True" TabIndex="13" BorderWidth="1px" CellPadding="0" OnRowDataBound="dgResult_RowDataBound">
                    <Columns>
                        <asp:TemplateColumn SortExpression="causenumber" HeaderText="Cause Number">
                            <HeaderStyle Width="12%" CssClass="clsaspcolumnheaderblack" Height="26px"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblcausenumber" runat="server" CssClass="Labelfrmmain" Text='<%# DataBinder.Eval(Container, "DataItem.causenumber") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="CaseNumber" HeaderText="Ticket Number" >
                            <HeaderStyle Width="12%" CssClass="clsaspcolumnheaderblack"></HeaderStyle>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkbtnCaseNo" runat="server"  Text='<%# DataBinder.Eval(Container, "DataItem.CaseNumber") %>'
                                    CommandName="CaseNo">
                                </asp:LinkButton>
                                <asp:HyperLink ID="HLCaseNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CaseNumber") %>'
                                    Visible="False">
                                </asp:HyperLink>
                                <asp:Label ID="lblTicketId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TicketId") %>'
                                    Visible="False">
                                </asp:Label>
                                <asp:Label ID="lblCourtId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CourtId") %>'
                                    Visible="False">
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="FirstName" HeaderText="First Name">
                            <HeaderStyle Width="10%" CssClass="clsaspcolumnheaderblack"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label5" runat="server" CssClass="Labelfrmmain" Text='<%# DataBinder.Eval(Container, "DataItem.Firstname") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="LastName" HeaderText="Last Name">
                            <HeaderStyle Width="10%" CssClass="clsaspcolumnheaderblack"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" CssClass="Labelfrmmain" Text='<%# DataBinder.Eval(Container, "DataItem.Lastname") %>'>
                                </asp:Label>
                                <asp:HiddenField ID="hf_MailerIDFlag" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.MailerIDFlag") %>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="DOB" HeaderText="DOB">
                            <HeaderStyle Width="8%" CssClass="clsaspcolumnheaderblack"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" CssClass="Labelfrmmain" Text='<%# DataBinder.Eval(Container, "DataItem.DOB", "{0:MM/dd/yyyy}") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn HeaderText="Matter" SortExpression="Matter" HeaderStyle-Width="20%"
                            DataField="Matter" HeaderStyle-CssClass="clssubhead" ItemStyle-CssClass="GridItemStyleBig">
                        </asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Court" SortExpression="Court" HeaderStyle-Width="9%"
                            DataField="Court" HeaderStyle-CssClass="clssubhead" ItemStyle-CssClass="GridItemStyleBig">
                        </asp:BoundColumn>
                        <asp:TemplateColumn SortExpression="CourtDate" HeaderText="Court Date">
                            <HeaderStyle Width="9%" CssClass="clsaspcolumnheaderblack"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" CssClass="Labelfrmmain" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDate", "{0:MM/dd/yyyy}") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="Address" HeaderText="Address" Visible="False">
                            <HeaderStyle Width="24%" CssClass="clsaspcolumnheaderblack"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label6" runat="server" CssClass="Labelfrmmain" Text='<%# DataBinder.Eval(Container, "DataItem.address") %>'>
                                </asp:Label>
                                <asp:Label ID="lblAdd1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.address1") %>'
                                    Visible="False">
                                </asp:Label>
                                <asp:Label ID="lblZip" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.zipcode") %>'
                                    Visible="False">
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="CaseStatus" HeaderText="Status">
                            <HeaderStyle Width="5%" CssClass="clsaspcolumnheaderblack"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" CssClass="Labelfrmmain" Text='<%# DataBinder.Eval(Container, "DataItem.CaseStatus") %>'>
                                </asp:Label>
                                <asp:Label ID="lbl_dbid" runat="server" CssClass="Labelfrmmain" Text='<%# DataBinder.Eval(Container, "DataItem.dbid") %>'
                                    Visible="False"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="CaseType" HeaderText="CaseType">
                            <HeaderStyle Width="7%" CssClass="clsaspcolumnheaderblack"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="lbl_casetype" runat="server" CssClass="Labelfrmmain" Text='<%# DataBinder.Eval(Container, "DataItem.CaseType") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False" SortExpression="MIDNo" HeaderText="MID #">
                            <HeaderStyle CssClass="clsaspcolumnheaderblack"></HeaderStyle>
                            <ItemTemplate>
                                <asp:HyperLink ID="HLMidNumber" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MIDNo") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle NextPageText="Next&amp;gt" PrevPageText="&amp;lt;Previous" HorizontalAlign="Center">
                    </PagerStyle>
                </asp:DataGrid>
                                    </div>

                                    </div>
                                                                </div>
                         </div>
                    </div>
                      </div>





                 </section>

             <div class="clearfix"></div>

         
                                     <asp:Panel ID="pnlCaseCategory" runat="server" Height="50px" Width="350px">

                                             <section class="box" id="">
                 <header class="panel_header">
                     <h2 class="title pull-left">Case Type Info</h2>
                     <div class="actions panel_actions pull-right">
                     <asp:LinkButton ID="lnkbtnclose" runat="server">X</asp:LinkButton>
                       
                    
                </div>
            </header>
                 <div class="content-body">
            <div class="row">

                 <div class="col-md-12">
                                                        <div class="form-group">
                            <label class="form-label">Case Type</label>
                            <span class="desc"></span>
                            <div class="controls">
                                 <asp:DropDownList ID="ddlCaseType" runat="server" CssClass="form-control"
                                                DataTextField="casetypename" DataValueField="casetypeid" Width="150px">
                                            </asp:DropDownList>
                                

                                </div>
                                                            </div>
                     </div>
                 <div class="clearfix"></div>

                 <div class="col-md-12">
                                                        <div class="form-group">
                            
                            <span class="desc"></span>
                            <div class="controls">
                                 
                                <asp:Button ID="btnAddCase" runat="server" CssClass="btn btn-primary" Text="Add" Width="60px"
                                                OnClick="btnAddCase_Click" OnClientClick="return CheckCaseType();" />

                                </div>
                                                            </div>
                     </div>

               

                      </div>




                </div>
                     
                 </section>
                  
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="MPECaseCategory" runat="server" BackgroundCssClass="modalBackground"
                    CancelControlID="lnkbtnclose" PopupControlID="pnlCaseCategory" TargetControlID="lnkbtn_AddNewTicket">
                </ajaxToolkit:ModalPopupExtender>
                               

             <div class="clearfix"></div>
               <section class="box" >
            
            <div class="content-body" style="display:none">
                <div class="row">
                      <div class="col-md-12">
                                                            <div class="form-group">
                                <label class="form-label"></label>
                                <span class="desc"></span>
                                <div class="controls">
                                    
                                     <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                                    </div>
                                                                </div>
                          </div>
                    </div>
                </div>
                </section>




            </section>
                 </section>
        </div>

    </form>
    <!-- CORE JS FRAMEWORK - START -->
    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE JS FRAMEWORK - END -->

    <!-- CORE TEMPLATE JS - START -->
    <script src="../assets/js/scripts.js" type="text/javascript"></script>
    <!-- END CORE TEMPLATE JS - END -->
</body>
</html>

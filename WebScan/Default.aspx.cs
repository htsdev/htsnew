using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;
using lntechNew.Components;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using System.Text.RegularExpressions;

//Waqas 5057 03/19/2009 Changed to HTP
namespace HTP.WebScan
{
    public partial class _Default : System.Web.UI.Page
    {
        #region Variables

        Batch NewBatch = new Batch();
        clsGeneralMethods CGeneral = new clsGeneralMethods();
        NewOCR _cGeneral = new NewOCR();
        Picture NewPic = new Picture();
        Log NewLog = new Log();
        OCR NewOCR = new OCR();
        clsLogger BugTracker = new clsLogger();
        clsENationWebComponents clsDb = new clsENationWebComponents();
        clsSession cSession = new clsSession();
        DataView DV;
        string ocr_data = String.Empty;
        string picDestination = String.Empty;
        string CourtStatus = String.Empty;
        string causeno = "";
        string courtdate = "";
        string time = "";
        //Ozair 5057 03/24/2009 Warnings Removed
        string TempString = "";
        string strServer;
        string searchpath = "";
        string picName = "";
        string picid;
        string courtno = "";
        // Babar Ahmad 8553 07/07/2011 boolean variable declared for CheckStatus.
        bool isDiscrepancy = false;

        //Ozair 5057 03/24/2009 Warnings Removed
        int PicID = 0;
        int Batchid = 0;
        StringBuilder sb = new StringBuilder();
        string StrAcsDec = String.Empty;
        string StrExp = String.Empty;
        string status = String.Empty;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            //Waqas 5057 03/17/2009 Checking employee info in session
            if (cSession.IsValidSession(this.Request, this.Response, this.Session) == false)
            {
                Response.Redirect("../frmlogin.aspx", false);
            }
            else
            {
                if (!IsPostBack)
                {
                    lblMessage.Visible = false;
                    
                    BindGrid();//bind Grid With Batch Records..
                    FillCourtLocation();
                    cal_scandate.Text = System.DateTime.Today.ToShortDateString();
                    
                    
                    txtSrv.Text = Request.ServerVariables["SERVER_NAME"].ToString();//for scanning function in javascript.
                    strServer = "http://" + Request.ServerVariables["SERVER_NAME"];//for scanning function in javascript.
                    Session["objTwain"] = "<OBJECT id='OZTwain1' classid='" + strServer + "/OZTwain_1.dll#OZTwain.OZTwain' height='1' width='1' VIEWASTEXT> </OBJECT>";//for scanning function call in javascript.

                    txtsessionid.Text = Session.SessionID.ToString();//for scanning function in javascript.
                    txtempid.Text = cSession.GetCookie("sEmpID", this.Request).ToString();//for scanning function in javascript.
                    ViewState["vNTPATHScanTemp"] = ConfigurationSettings.AppSettings["NTPATHScanTemp2"].ToString();//path to and get save images
                    ViewState["vNTPATHScanImage"] = ConfigurationSettings.AppSettings["NTPATHScanImage2"].ToString();//path to save and get images
                    btnScan.Attributes.Add("onclick", "return StartScan();");

                    
                }
                
    }
            
        }
        private void FillCourtLocation()
        {
            try
            {
                DataSet ds_Court = clsDb.Get_DS_BySP("usp_HTS_GetShortCourtName");
                ddlCourtLoc.Items.Clear();


                ddlCourtLoc.DataSource = ds_Court.Tables[0];
                ddlCourtLoc.DataTextField = "shortname";
                ddlCourtLoc.DataValueField = "courtid";
                ddlCourtLoc.DataBind();

                ddlCourtLoc.Items[0].Text = "---Choose---";
                ddlCourtLoc.Items[1].Text = "HMC";
                ddlCourtLoc.Items[1].Value = "HMC";
                ddlCourtLoc.SelectedValue = "HMC";
                ddlCourtLoc.Items.RemoveAt(2);
                ddlCourtLoc.Items.RemoveAt(2);


                // Abbas qamar 10088  05-12-2012 HMC-W court merged under HMC
                ListItem item = ddlCourtLoc.Items.FindByValue("3075");
                ddlCourtLoc.Items.Remove(item);

            }
            catch (Exception ex)
            {
                lblMessage.Visible = true;
                lblMessage.Text = ex.Message;

                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        
        private void BindGrid()
        {
            try
            {
                DataSet DS_GetBatch = NewBatch.GetCounts();//Display Records of current date when page loaded.
                if (DS_GetBatch.Tables[0].Rows.Count > 0)
                {
                    gv_Scan.DataSource = DS_GetBatch;
                    DV = new DataView(DS_GetBatch.Tables[0]);
                    Session["DV"] = DV;
                    gv_Scan.DataBind();
                }
                else
                {
                    lblMessage.Text = "No Record Found";
                    
                }
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }

        }
        #region SortingLogic 
        private void SortGrid(string SortExp)
        {
            try
            {
                SetAcsDesc(SortExp);
                DV = (DataView)Session["DV"];
                DV.Sort = StrExp + " " + StrAcsDec;
                gv_Scan.DataSource = DV;
                gv_Scan.DataBind();
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }
        }
        private void SetAcsDesc(string Val)
        {
            try
            {
                StrExp = Session["StrExp"].ToString();
                StrAcsDec = Session["StrAcsDec"].ToString();
            }
            catch
            {

            }

            if (StrExp == Val)
            {
                if (StrAcsDec == "ASC")
                {
                    StrAcsDec = "DESC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
                else
                {
                    StrAcsDec = "ASC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
            }
            else
            {
                StrExp = Val;
                StrAcsDec = "ASC";
                Session["StrExp"] = StrExp;
                Session["StrAcsDec"] = StrAcsDec;
            }
        }

        #endregion 
        private void OCR(int batchid)
        {

            

            try
            {

                DataSet DS=NewPic.GetPicByBatchID(batchid);//get PIcID's of scan pages from database..

                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {

                    try
                    {
                        int TempFlag=0;
                        picid = DS.Tables[0].Rows[i]["PicID"].ToString();
                        string pName = batchid.ToString() + "-" + picid.ToString();
                        picDestination = ViewState["vNTPATHScanImage"].ToString() + pName + ".jpg";//get Image from folder for OCR..
                        picDestination = picDestination.Replace("\\\\", "\\");

                        try
                        {
                            //ocr_data = CGeneral.OcrIt(picDestination);//OCR Process..
                            ocr_data = _cGeneral.OCRImage(picDestination);
                        }
                        catch 
                        {
                            
                            NewOCR.PicID = Convert.ToInt32(picid);
                                                 
                            NewOCR.Location = ddlCourtLoc.SelectedItem.Text.ToString();
                            NewOCR.Status = ddl_ScanType.SelectedItem.Text.ToString();
                            // Babar Ahmad 8553 07/07/2011 Set status of pending/discrepancy
                            SetCheckStatus();
                            NewOCR.OCRData = ocr_data;
                            NewOCR.InsertPendingOCR();
                            TempFlag = 1;
                            continue;
                        }
                        
                        if (ocr_data.Length > 0)
                        {

                            TempString = ocr_data.ToUpper();
                            TempString = TempString.Replace("\r\n", " ");
                            // Haris Ahmed 10361 06/27/2012 Fix issue of data parsing
                            #region Cause No
                            try
                            {

                                //if (TempString.Contains("CAUSE NUMBER") == true)
                                if (Regex.IsMatch(TempString, "(.A.(.|..|...). N...(.|..|...)R(:|;))"))
                                {
                                    TempString = Regex.Replace(TempString, "(.A.(.|..|...). N...(.|..|...)R(:|;))", "CAUSE NUMBER");
                                    causeno = TempString.Substring(TempString.LastIndexOf("CAUSE NUMBER") + 13, 16);
                                    if (causeno.LastIndexOf(" ") > -1 && causeno.Substring(causeno.LastIndexOf(" ")).Length <= 2)
                                    {
                                        causeno = causeno.Remove(causeno.LastIndexOf(" "));
                                    }
                                    causeno = causeno.Replace(" ", "");
                                    causeno = causeno.Replace(".", "");
                                    causeno = causeno.Replace("-", "");
                                    causeno.Trim();

                                    if (causeno.IndexOf("O") > -1)
                                    {
                                        causeno = causeno.Replace("O", "0");
                                    }
                                    else if (causeno.IndexOf("o") > -1)
                                    {
                                        causeno = causeno.Replace("o", "0");
                                    }
                                    if (causeno.IndexOf("I") > -1)
                                    {
                                        causeno = causeno.Replace("I", "1");
                                    }
                                    else if (causeno.IndexOf("i") > -1)
                                    {
                                        causeno = causeno.Replace("i", "1");
                                    }

                                    if (causeno.IndexOf("L") > -1)
                                    {
                                        causeno = causeno.Replace("L", "1");
                                    }
                                    else if (causeno.IndexOf("l") > -1)
                                    {
                                        causeno = causeno.Replace("l", "1");
                                    }

                                    //ozair 4790 09/12/2008 tunning OCR Process
                                    if (causeno.IndexOf("]") > -1)
                                    {
                                        causeno = causeno.Replace("]", "1");
                                    }

                                    if (causeno.IndexOf("S") > -1)
                                    {
                                        causeno = causeno.Replace("S", "5");
                                    }
                                    else if (causeno.IndexOf("s") > -1)
                                    {
                                        causeno = causeno.Replace("s", "5");
                                    }
                                    //end ozair 4790 

                                    if (causeno.IndexOf("$") > -1)
                                    {
                                        causeno = causeno.Replace("$", "5");
                                    }

                                    #region Cause no start year
                                    //ozair 3333 on 03/05/2008
                                    string currYear = DateTime.Now.Year.ToString();
                                    for (int y = 1; y <= 9; y++)
                                    {
                                        if ((causeno.Substring(0, 4) != currYear) & (causeno.Substring(0, 4).LastIndexOf(currYear.Substring(3)) == 3))
                                        {
                                            causeno = causeno.Remove(0, 4);
                                            causeno = causeno.Insert(0, currYear);
                                        }
                                        currYear = Convert.ToString(DateTime.Now.Year - y);
                                    }
                                    #endregion

                                    if ((causeno.Substring(4, 1) == "T" || causeno.Substring(5, 1) == "R" || causeno.Substring(4, 1) == "1" || causeno.Substring(5, 1) == "K" || causeno.Substring(5, 1) == "k") & causeno.Substring(4, 2) != "TR")
                                    {
                                        causeno = causeno.Remove(4, 2);
                                        causeno = causeno.Insert(4, "TR");
                                    }

                                    else if (causeno.Substring(4, 1) == "N" & causeno.Substring(4, 2) != "NT")
                                    {
                                        causeno = causeno.Remove(4, 2);
                                        causeno = causeno.Insert(4, "NT");
                                    }

                                    else if (causeno.Substring(4, 1) == "F" & causeno.Substring(4, 3) != "FTA")
                                    {
                                        causeno = causeno.Remove(4, 3);
                                        causeno = causeno.Insert(4, "FTA");
                                    }
                                    else if (causeno.Substring(4, 3).IndexOf("A") > -1)
                                    {
                                        causeno = causeno.Remove(4, 3);
                                        causeno = causeno.Insert(4, "FTA");
                                    }
                                }
                                else
                                {
                                    causeno = "";
                                }

                            }
                            catch
                            {
                                causeno = "";
                            }
                            #endregion
                            #region Court No
                            try
                            {
                                //if (TempString.Contains("COURT NO") == true)
                                if (Regex.IsMatch(TempString, "((.|)O((.|..|...)R.)(.|)NO)"))
                                {
                                    TempString = Regex.Replace(TempString, "((.|)O((.|..|...)R.)(.|)NO)", "COURT NO");

                                    courtno = TempString.Substring(TempString.LastIndexOf("COURT NO") + 9, 4);

                                    if (courtno.Substring(2,1) == "(")
                                        courtno = courtno.Remove(2);
                                    if (courtno.Contains("("))
                                        courtno = courtno.Replace("(", "");
                                    if (courtno.Contains(":"))
                                        courtno = courtno.Replace(":", "");
                                    if (courtno.Contains(" "))
                                        courtno = courtno.Replace(" ", "");

                                    if (courtno.IndexOf("O") > -1)
                                    {
                                        courtno = courtno.Replace("O", "0");
                                    }
                                    else if (courtno.IndexOf("o") > -1)
                                    {
                                        courtno = courtno.Replace("o", "0");
                                    }

                                    if (courtno.IndexOf("I") > -1)
                                    {
                                        courtno = courtno.Replace("I", "1");
                                    }
                                    else if (courtno.IndexOf("i") > -1)
                                    {
                                        courtno = courtno.Replace("i", "1");
                                    }

                                    if (courtno.IndexOf("L") > -1)
                                    {
                                        courtno = courtno.Replace("L", "1");
                                    }
                                    else if (courtno.IndexOf("l") > -1)
                                    {
                                        courtno = courtno.Replace("l", "1");
                                    }
                                    if (courtno.IndexOf("(") > -1)
                                    {
                                        courtno = courtno.Replace("(", "");
                                    }

                                    //ozair 4790 09/12/2008 tunning OCR Process
                                    if (courtno.IndexOf("]") > -1)
                                    {
                                        courtno = courtno.Replace("]", "1");
                                    }
                                    if (courtno.IndexOf("!") > -1)
                                    {
                                        courtno = courtno.Replace("!", "1");
                                    }
                                    if (courtno.IndexOf("S") > -1)
                                    {
                                        courtno = courtno.Replace("S", "5");
                                    }
                                    else if (courtno.IndexOf("s") > -1)
                                    {
                                        courtno = courtno.Replace("s", "5");
                                    }
                                    //end ozair 4790    

                                    try
                                    {
                                        courtno = Convert.ToInt32(courtno.Trim()).ToString();
                                    }
                                    catch
                                    {
                                        courtno = "1";
                                    }
                                }
                                else
                                {
                                    courtno = "1";
                                }
                            }
                            catch
                            {
                                courtno = "1";
                            }
                            #endregion
                            #region Jury Trial

                            if (ocr_data.ToUpper().Contains("JURY"))
                            {
                                status = "JURY TRIAL";
                            }
                            #endregion
                            #region Court Date
                            try
                            {
                                //if (TempString.Contains("COURT DATE") == true)
                                if (Regex.IsMatch(TempString, @"(N.W(.|).O(.|..).(.|..)(\ |)(.|..)A..(.|..))"))
                                {
                                    TempString = Regex.Replace(TempString, @"(N.W(.|).O(.|..).(.|..)(\ |)(.|..)A..(.|..))", "NEW COURT DATE");
                                    courtdate = TempString.Substring(TempString.IndexOf("NEW COURT DATE") + 15, 11);
                                    if (courtdate.LastIndexOf("T") > -1)
                                    {
                                        courtdate = courtdate.Remove(courtdate.LastIndexOf("T"));
                                    }
                                    courtdate = courtdate.Trim();
                                    courtdate = courtdate.Replace(" ", "");

                                    if (courtdate.IndexOf("*") > -1)
                                    {
                                        courtdate = courtdate.Replace(" ", "");
                                        courtdate = courtdate.Replace("*", "0");
                                    }
                                    if (courtdate.IndexOf("G") > -1)
                                    {
                                        courtdate = courtdate.Replace(" ", "");
                                        courtdate = courtdate.Replace("G", "0");
                                    }
                                    if (courtdate.IndexOf("t") > -1)
                                    {
                                        courtdate = courtdate.Replace(" ", "");
                                        courtdate = courtdate.Replace("t", "1");
                                    }

                                    if (courtdate.IndexOf("L") > -1)
                                    {
                                        courtdate = courtdate.Replace(" ", "");
                                        courtdate = courtdate.Replace("L", "1");
                                    }
                                    else if (courtdate.IndexOf("l") > -1)
                                    {
                                        courtdate = courtdate.Replace(" ", "");
                                        courtdate = courtdate.Replace("l", "1");
                                    }
                                    if (courtdate.IndexOf("Z") > -1)
                                    {
                                        courtdate = courtdate.Replace(" ", "");
                                        courtdate = courtdate.Replace("Z", "2");
                                    }
                                    else if (courtdate.IndexOf("z") > -1)
                                    {
                                        courtdate = courtdate.Replace(" ", "");
                                        courtdate = courtdate.Replace("z", "2");
                                    }
                                    if (courtdate.IndexOf("()") > -1)
                                    {
                                        courtdate = courtdate.Replace(" ", "");
                                        courtdate = courtdate.Replace("()", "0");
                                    }
                                    if (courtdate.IndexOf("(") > -1)
                                    {
                                        courtdate = courtdate.Replace(" ", "");
                                        courtdate = courtdate.Replace("(", "1");
                                    }
                                    if (courtdate.IndexOf(")") > -1)
                                    {
                                        courtdate = courtdate.Replace(" ", "");
                                        courtdate = courtdate.Replace(")", "1");
                                    }

                                    if (courtdate.IndexOf("I") > -1)
                                    {
                                        courtdate = courtdate.Replace(" ", "");
                                        courtdate = courtdate.Replace("I", "1");
                                    }
                                    else if (courtdate.IndexOf("i") > -1)
                                    {
                                        courtdate = courtdate.Replace(" ", "");
                                        courtdate = courtdate.Replace("i", "1");
                                    }

                                    if (courtdate.IndexOf("O") > -1)
                                    {
                                        courtdate = courtdate.Replace("O", "0");
                                    }
                                    else if (courtdate.IndexOf("o") > -1)
                                    {
                                        courtdate = courtdate.Replace("o", "0");
                                    }

                                    if (courtdate.IndexOf("U") > -1)
                                    {
                                        courtdate = courtdate.Replace("U", "0");
                                    }
                                    else if (courtdate.IndexOf("u") > -1)
                                    {
                                        courtdate = courtdate.Replace("u", "0");
                                    }

                                    //ozair 4790 09/12/2008 tunning ocr process
                                    if (courtdate.IndexOf("S") > -1)
                                    {
                                        courtdate = courtdate.Replace("S", "5");
                                    }
                                    else if (courtdate.IndexOf("s") > -1)
                                    {
                                        courtdate = courtdate.Replace("s", "5");
                                    }

                                    if (courtdate.IndexOf("]") > -1)
                                    {
                                        courtdate = courtdate.Replace("]", "1");
                                    }
                                    //end ozair 4790

                                    DateTime dtresult;

                                    if (!DateTime.TryParse(courtdate, out dtresult))
                                    {
                                        if (courtdate.Substring(2, 1) != "/")
                                        {
                                            courtdate = courtdate.Remove(2, 1);
                                            courtdate = courtdate.Insert(2, "/");
                                        }
                                        if (courtdate.Substring(5, 1) != "/")
                                        {
                                            courtdate = courtdate.Remove(5, 1);
                                            courtdate = courtdate.Insert(5, "/");
                                        }
                                        courtdate = courtdate.Trim();
                                        if (courtdate.Length > 10)
                                        {
                                            courtdate = courtdate.Substring(0, 10);
                                        }

                                        if (courtdate.Substring(courtdate.LastIndexOf("/")).Contains("12"))
                                        {
                                            courtdate = courtdate.Remove(courtdate.LastIndexOf("/") + 1);
                                            courtdate = courtdate + "2012";
                                        }
                                        else if (courtdate.Substring(courtdate.LastIndexOf("/")).Contains("13"))
                                        {
                                            courtdate = courtdate.Remove(courtdate.LastIndexOf("/") + 1);
                                            courtdate = courtdate + "2013";
                                        }
                                    }
                                    // Babar Ahmad 8553 07/07/2011 If court no is 18 and status is "Jury Trial" then court date should be thursday else move to discrepancy
                                    //Babar Ahmad 9891 10/31/2011 Added Friday for HMC-D for court # 18 and Jury Trial.
                                    if (courtno == "18" && NewOCR.Status == "Jury Trial")
                                    {
                                        if (Convert.ToDateTime(courtdate).DayOfWeek != DayOfWeek.Thursday && Convert.ToDateTime(courtdate).DayOfWeek != DayOfWeek.Friday)
                                        {
                                            isDiscrepancy = true;
                                        }
                                        else
                                        {
                                            isDiscrepancy = false;
                                        }
                                    }

                                }
                                else
                                {
                                    courtdate = "";
                                }
                            }
                            catch
                            {
                                courtdate = "";
                            }
                            #endregion
                            #region Time
                            try
                            {
                                //if (TempString.Contains("TIME") == true)
                                if (Regex.IsMatch(TempString, @"((TIME:)|.IM(.|..|...)( |: ))"))
                                {
                                    TempString = Regex.Replace(TempString, @"((TIME:)|.IM(.|..|...)( |: ))", "TIME");
                                    time = TempString.Substring(TempString.IndexOf("TIME") + 5, 9);
                                    if (time.Contains("AND") == true)
                                    {
                                        time = TempString.Substring(TempString.LastIndexOf("TIME") + 5, 9);
                                    }
                                    if (time.Contains("AND") == true)
                                    {
                                        time = "8:00 AM";
                                    }
                                    else if (ddlCourtLoc.SelectedValue == "HMC" & ddl_ScanType.SelectedItem.Text.ToString().ToUpper() == "JURY TRIAL")
                                    {

                                        if (time.IndexOf("8") > -1)
                                        {
                                            time = "8:00 AM";
                                        }
                                        else if (time.IndexOf("9") > -1)
                                        {
                                            time = "9:00 AM";
                                        }
                                        else if (time.IndexOf("1") > -1)
                                        {
                                            time = "10:30 AM";
                                        }
                                        else if (time.IndexOf("3") > -1)
                                        {
                                            time = "10:30 AM";
                                        }
                                        else
                                        {
                                            time = "8:00 AM";
                                        }
                                    }
                                    else
                                    {
                                        if (time.IndexOf("7") > -1)
                                        {
                                            if (time.IndexOf("A") > -1)
                                            {
                                                if (time.IndexOf("3") > -1)
                                                {
                                                    time = "7:30 AM";
                                                }
                                                else
                                                {
                                                    time = "7:00 AM";
                                                }
                                            }
                                            else if (time.IndexOf("P") > -1)
                                            {
                                                if (time.IndexOf("3") > -1)
                                                {
                                                    time = "7:30 PM";
                                                }
                                                else
                                                {
                                                    time = "7:00 PM";
                                                }
                                            }
                                            else
                                            {
                                                time = "7:00 AM";
                                            }
                                        }
                                        else if (time.IndexOf("8") > -1)
                                        {
                                            if (time.IndexOf("A") > -1)
                                            {
                                                if (time.IndexOf("3") > -1)
                                                {
                                                    time = "8:30 AM";
                                                }
                                                else
                                                {
                                                    time = "8:00 AM";
                                                }
                                            }
                                            else if (time.IndexOf("P") > -1)
                                            {
                                                if (time.IndexOf("3") > -1)
                                                {
                                                    time = "8:30 PM";
                                                }
                                                else
                                                {
                                                    time = "8:00 PM";
                                                }
                                            }
                                            else
                                            {
                                                time = "8:00 AM";
                                            }
                                        }
                                        else if (time.IndexOf("9") > -1)
                                        {
                                            if (time.IndexOf("A") > -1)
                                            {
                                                if (time.IndexOf("3") > -1)
                                                {
                                                    time = "9:30 AM";
                                                }
                                                else
                                                {
                                                    time = "9:00 AM";
                                                }
                                            }
                                            else if (time.IndexOf("P") > -1)
                                            {
                                                if (time.IndexOf("3") > -1)
                                                {
                                                    time = "9:30 PM";
                                                }
                                                else
                                                {
                                                    time = "9:00 PM";
                                                }
                                            }
                                            else
                                            {
                                                time = "9:00 AM";
                                            }
                                        }
                                        else if (time.IndexOf("10") > -1)
                                        {
                                            if (time.IndexOf("A") > -1)
                                            {
                                                if (time.IndexOf("3") > -1)
                                                {
                                                    time = "10:30 AM";
                                                }
                                                else
                                                {
                                                    time = "10:00 AM";
                                                }
                                            }
                                            else if (time.IndexOf("P") > -1)
                                            {
                                                time = "10:00 PM";
                                            }
                                            else
                                            {
                                                time = "10:00 AM";
                                            }
                                        }
                                        else if (time.IndexOf("11") > -1)
                                        {
                                            if (time.IndexOf("A") > -1)
                                            {
                                                if (time.IndexOf("3") > -1)
                                                {
                                                    time = "11:30 AM";
                                                }
                                                else
                                                {
                                                    time = "11:00 AM";
                                                }
                                            }
                                            else
                                            {
                                                time = "11:00 AM";
                                            }

                                        }
                                        else if (time.IndexOf("12") > -1)
                                        {
                                            if (time.IndexOf("3") > -1)
                                            {
                                                time = "12:30 PM";
                                            }
                                            else
                                            {
                                                time = "12:00 PM";
                                            }
                                        }
                                        else if (time.IndexOf("1") > -1)
                                        {
                                            if (time.IndexOf("3") > -1)
                                            {
                                                time = "1:30 PM";
                                            }
                                            else
                                            {
                                                time = "1:00 PM";
                                            }
                                        }
                                        else if (time.IndexOf("2") > -1)
                                        {
                                            if (time.IndexOf("3") > -1)
                                            {
                                                time = "2:30 PM";
                                            }
                                            else
                                            {
                                                time = "2:00 PM";
                                            }
                                        }
                                        else if (time.IndexOf("3") > -1)
                                        {
                                            if (time.LastIndexOf("3") > time.IndexOf("3"))
                                            {
                                                time = "3:30 PM";
                                            }
                                            else
                                            {
                                                time = "3:00 PM";
                                            }
                                        }
                                        else if (time.IndexOf("4") > -1)
                                        {
                                            if (time.IndexOf("3") > -1)
                                            {
                                                time = "4:30 PM";
                                            }
                                            else
                                            {
                                                time = "4:00 PM";
                                            }
                                        }
                                        else if (time.IndexOf("5") > -1)
                                        {
                                            if (time.IndexOf("3") > -1)
                                            {
                                                time = "5:30 PM";
                                            }
                                            else
                                            {
                                                time = "5:00 PM";
                                            }
                                        }
                                        else if (time.IndexOf("6") > -1)
                                        {
                                            if (time.IndexOf("3") > -1)
                                            {
                                                time = "6:30 PM";
                                            }
                                            else
                                            {
                                                time = "6:00 PM";
                                            }
                                        }
                                        else
                                        {
                                            time = "8:00 AM";
                                        }

                                    }
                                }
                                else
                                {
                                    time = "8:00 AM";
                                }
                            }
                            catch
                            {
                                time = "8:00 AM";
                            }
                            #endregion
                            

                            if (ChechForUnsucessOCR(causeno, courtdate, time, courtno) == false)//If all variables are empty then it is bad scan insert it in bad scan.
                            {
                                
                                //Insertion in tbl_webscan_ocr 
                                NewOCR.CauseNo = causeno.Trim();
                                NewOCR.Location = ddlCourtLoc.SelectedItem.Text.ToString();
                                NewOCR.NewCourtDate = courtdate.Trim();
                                NewOCR.PicID = Convert.ToInt32(picid);
                                NewOCR.Time = time.Trim();
                                NewOCR.Status = ddl_ScanType.SelectedItem.Text.ToString();
                                NewOCR.CourtNo = courtno.Trim();
                                // Babar Ahmad 8553 07/07/2011 Set status of pending/discrepancy
                                SetCheckStatus();
                     
                                NewOCR.OCRData = ocr_data;
                                NewOCR.InsertInOCR();

                            }
                            else
                            {
                                NewOCR.PicID = Convert.ToInt32(picid);
                                NewOCR.Location = ddlCourtLoc.SelectedItem.Text.ToString();
                                NewOCR.Status = ddl_ScanType.SelectedItem.Text.ToString();
                                // Babar Ahmad 8553 07/07/2011 Set status of pending/discrepancy
                                SetCheckStatus();
                                NewOCR.OCRData = ocr_data;
                                NewOCR.InsertPendingOCR();
                            }



                        }
                        else
                        {
                            if (TempFlag == 1)//If OCR not done sucessfully then insert it into tbl_webscan_log as a bad scan 
                            {
                                NewOCR.PicID = Convert.ToInt32(picid);
                                
                                
                                NewOCR.Location = ddlCourtLoc.SelectedItem.Text.ToString();
                                NewOCR.Status = ddl_ScanType.SelectedItem.Text.ToString();

                                //Abbas Qamar  10088 04/24/2012 Added Friday for HMC-W for court # 20 and Jury Trial.
                                //Fahad 11254 07/01/2013 Removed Jury Trial Status check for HMC-W for court # 20 and Jury Trial.
                                //if (NewOCR.CourtNo == "20" && status == "JURY TRIAL")
                                //{
                                //    isDiscrepancy = true;

                                //}

                                // Babar Ahmad 8553 07/07/2011 Set status of pending/discrepancy
                                SetCheckStatus();
                                NewOCR.OCRData = ocr_data;
                                NewOCR.InsertPendingOCR();
                            }

                        }
                    }
                    catch 
                    {//Insertion in tbl_webscan_ocr 
                        NewOCR.CauseNo = causeno.Trim();
                        NewOCR.Location = ddlCourtLoc.SelectedItem.Text.ToString();
                        NewOCR.NewCourtDate = courtdate.Trim();
                        NewOCR.PicID = Convert.ToInt32(picid);
                        NewOCR.Time = time.Trim();
                        //NewOCR.TodayDate = todaysdate;
                        NewOCR.Status = ddl_ScanType.SelectedItem.Text.ToString();
                        NewOCR.CourtNo = courtno.Trim();
                        // Babar Ahmad 8553 07/07/2011 Set status of pending/discrepancy
                        SetCheckStatus();
                       
                        NewOCR.OCRData = ocr_data;
                        NewOCR.InsertInOCR();

                        
                    }
                    
                    }
                    BindGrid();//update grid..
                }
               
             
                
                
            
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }

        }
        private bool ChechForUnsucessOCR(string CauseNo,string CourtDate,string Time,string CourtNo)
        {

            if (CauseNo == "")
            {
                if (CourtDate == "")
                {
                    if (Time == "")
                    {
                        if (CourtNo == "")
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        private string[] sortfilesbydate(string[] filename)
        {
            for (int i = 0; i < filename.Length; i++)
            {
                DateTime cDateTime = File.GetCreationTime(filename[i]);
                for (int j = i + 1; j < filename.Length; j++)
                {
                    DateTime cDateTime1 = File.GetCreationTime(filename[j]);
                    if (DateTime.Compare(cDateTime1, cDateTime) < 0)
                    {
                        string fname = filename[j];
                        filename[j] = filename[i];
                        filename[i] = fname;
                        i = -1;
                        break;
                    }
                }
            }
            return filename;
        }

        protected void btnScan_Click(object sender, EventArgs e)
        {
            lblMessage.Visible = false;

            try
            {
                searchpath = "*" + Session.SessionID + txtempid.Text + "*.jpg";                
                string[] filename = Directory.GetFiles(ViewState["vNTPATHScanTemp"].ToString(), searchpath);//get scann images from WebScanDocs..
                if (filename.Length > 1)
                {
                    filename = sortfilesbydate(filename);//sort files by date..
                }
                if (filename.Length >= 1)
                {
                    NewBatch.ScanDate = Convert.ToDateTime(cal_scandate.Text);
                    NewBatch.ScanType = ddl_ScanType.SelectedItem.Text.ToString();
                    NewBatch.EmpID = Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request)); 
                    Batchid = NewBatch.InsertInBatch();//Insertion in tbl_webscan_batch..

                    for (int i = 0; i < filename.Length; i++)
                    {
                        NewPic.BatchID = Batchid;
                        PicID = NewPic.InsertInPicture();
                        picName = Batchid.ToString() + "-" + PicID.ToString();
                        //picDestination = Server.MapPath("WebScanDocs/Temp/") + picName + ".jpg";
                        picDestination = ViewState["vNTPATHScanImage"].ToString() + picName + ".jpg";
                        File.Copy(filename[i].ToString(), picDestination);//save images with batchid and picid like 1-1.jpg.
                        File.Delete(filename[i].ToString());//delete from intial position.

                    }

                }

                //Batchid = 62;
                OCR(Batchid);//OCR process..

            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }
        }

        protected void lnk_View_Click(object sender, EventArgs e)
        {
            Response.Redirect("ViewScanDocs.aspx");
        }

        
        protected void gv_Scan_Sorting(object sender, GridViewSortEventArgs e)
        {
            SortGrid(e.SortExpression);//sorting
        }

        protected void gv_Scan_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_Scan.PageIndex = e.NewPageIndex;//paging
                DV = (DataView)Session["DV"];
                gv_Scan.DataSource = DV;
                gv_Scan.DataBind();
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;

            }
        }

        protected void logout_Click(object sender, EventArgs e)
        {
            try
            {
                Session.Abandon();//session clear..
                Response.Redirect("Login.aspx");
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }
        }

        protected void GoodCount(object sender, CommandEventArgs e)
        {
            string batchid = e.CommandArgument.ToString();
            Response.Redirect("ViewGoodScan.aspx?batchid=" + batchid);
        }

        protected void BadCount(object sender, CommandEventArgs e)
        {
            string batchid = e.CommandArgument.ToString();
            Response.Redirect("ViewBadScan.aspx?batchid=" + batchid);
        }

        protected void gv_Scan_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string batchid = "";

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                batchid = ((Label)e.Row.FindControl("lblbatchid")).Text.ToString();
                if (((HyperLink)e.Row.FindControl("verified")).Text == "0")
                { ((HyperLink)e.Row.FindControl("verified")).NavigateUrl = ""; }
                else
                { ((HyperLink)e.Row.FindControl("verified")).NavigateUrl = "NewVerified.aspx?batchid=" + batchid; }
                if (((HyperLink)e.Row.FindControl("queued")).Text == "0")
                { ((HyperLink)e.Row.FindControl("queued")).NavigateUrl = ""; }
                else
                { ((HyperLink)e.Row.FindControl("queued")).NavigateUrl = "NewQueued.aspx?batchid=" + batchid; }
                if (((HyperLink)e.Row.FindControl("discrepancy")).Text == "0")
                { ((HyperLink)e.Row.FindControl("discrepancy")).NavigateUrl = ""; }
                else
                { ((HyperLink)e.Row.FindControl("discrepancy")).NavigateUrl = "NewDiscrepancy.aspx?batchid=" + batchid; }
                if (((HyperLink)e.Row.FindControl("pending")).Text == "0")
                { ((HyperLink)e.Row.FindControl("pending")).NavigateUrl = ""; }
                else
                { ((HyperLink)e.Row.FindControl("pending")).NavigateUrl = "NewPending.aspx?batchid=" + batchid; }

                if (((HyperLink)e.Row.FindControl("nocause")).Text == "0")
                { ((HyperLink)e.Row.FindControl("nocause")).NavigateUrl = ""; }
                else
                { ((HyperLink)e.Row.FindControl("nocause")).NavigateUrl = "NewNoCause.aspx?batchid=" + batchid; }

                //if (cSession.GetCookie("sAccessType", this.Request).ToString() != "2")
                //{

                //    ((HyperLink)e.Row.FindControl("queued")).NavigateUrl = "";
                //    ((HyperLink)e.Row.FindControl("verified")).NavigateUrl = "";
                //    ((HyperLink)e.Row.FindControl("nocause")).NavigateUrl = "";
                //}
            } 
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            
        }
        
        //Babar Ahmad 8553 07/07/2011 Set discrepancy/pending status.
        /// <summary>
        /// Set CheckStatus to Discrepancy if _isDiscrepancy check is true.
        /// </summary>
        private void SetCheckStatus()
        {
            if (isDiscrepancy)
            {
                NewOCR.CheckStatus = 2;  // Set to discrepancy
            }
            else
            {
                NewOCR.CheckStatus = 1;  // Set to pending
            }
        }
        
    }
}

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="HTP.WebScan._Default" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="~/WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Default</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <meta http-equiv="X-UA-Compatible" content="IE=5; IE=8; IE=7.5;" />
    <meta http-equiv="X-UA-Compatible" content="chrome=1">

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>


    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <%--<link href="../Styles.css" type="text/css" rel="stylesheet">--%>

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->

    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->



    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
  
    <script type="text/javascript">
        function hide() {
            var td = document.getElementById("tdProcess");
            td.style.display = 'none';

        }
        
    </script>
  
</head>
<body >
    <form id="form1" runat="server">
        <aspnew:ScriptManager ID="ScriptManager1" runat="server" />
        <!-- START CONTAINER -->
        <div class="page-container row-fluid container-fluid">
             <asp:Panel ID="pnl" runat="server">
                <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
            </asp:Panel>
             <section id="main-content" class=" ">
                <section class="wrapper main-wrapper row" style=''>
                    
                <div class='col-xs-12'>
                        <div class="page-title">

                            <div class="pull-left">
                                <!-- PAGE HEADING TAG - START --><h1 class="title">BSDA (HMC)</h1><!-- PAGE HEADING TAG - END -->
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-12">
                       
                              <section class="box ">
                                   <header class="panel_header">
                                <h2 class="title pull-left">Scan/Search Documents</h2>
                                <div class="actions pane</section>l_actions pull-right">
                                    <asp:HyperLink ID="hpSearch" runat="server" NavigateUrl="~/WebScan/NewSearch.aspx">Search</asp:HyperLink>
                                </div>
                            </header>
                              </section>
                       
                   <div class="content-body">
                       <div class="row">
                            <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
                       </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <section class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="form-label" for="field-1">Court House</label>
                                                    <div class="controls">
                                                         <asp:DropDownList ID="ddlCourtLoc" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                             <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="form-label" for="field-1">Scan Type</label>
                                                    <div class="controls">
                                                         <asp:DropDownList ID="ddl_ScanType" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="47" Selected="True">JURY TRIAL</asp:ListItem>
                                            <asp:ListItem Value="103">JUDGE</asp:ListItem>
                         <asp:ListItem Value="101">PRETRIAL</asp:ListItem>
                         <asp:ListItem Value="3">ARRAIGNMENT</asp:ListItem>
                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="form-label" for="field-1">Scan
                        Date</label>
                                                    <div class="controls">
                                                         <asp:Label ID="cal_scandate" runat="server" CssClass="form-label"></asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="form-label" for="field-1" style="color:#fff;">button</label>
                                                    <div class="controls">
                                                         
                                                        <asp:Button ID="btnScan" runat="server" CssClass="btn btn-primary" Text="Scan " OnClick="btnScan_Click" />
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        <section class="row">
                                            <div class="col-xs-12">
                                                 <aspnew:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
					<table id="tbl_plzwait1"   style="display: none" width="800px">
                          <tr>
                          <td class="clssubhead" valign="middle" align="center">
                           <img src="../Images/plzwait.gif" />
                             Please wait while scanning and OCR is in process..
                             </td>
                                                                     
                             </tr>
                                                                    
                                                                    
                          </table>
                          </ContentTemplate>
                          <Triggers >
                            <aspnew:AsyncPostBackTrigger ControlID="btnScan" EventName="Click" />
                          </Triggers>
                          </aspnew:UpdatePanel>
                                            </div>
                                        </section>
                                         <section class="row">
                                            <div class="col-xs-12">
                                                <aspnew:UpdatePanel ID="panel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                    <asp:GridView ID="gv_Scan" runat="server" Width="100%" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" OnPageIndexChanging="gv_Scan_PageIndexChanging" OnSorting="gv_Scan_Sorting" PageSize="15" OnRowDataBound="gv_Scan_RowDataBound">
                                    <AlternatingRowStyle BackColor="#EEEEEE" />
                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" CssClass="GrdHeader" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="BatchID" SortExpression="batchid">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblBatchID" CssClass="label" runat="server" Text='<%# bind("batchid") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle ForeColor="#006699" />
                                            </asp:TemplateField>
                                           <asp:TemplateField HeaderText="Images" SortExpression="ImagesCount">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblimages" CssClass="label" runat="server" Text='<%# bind("ImagesCount") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle ForeColor="#006699" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Scan Type" SortExpression="scantype">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblScanType" CssClass="label" runat="server" Text='<%# bind("scantype") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle ForeColor="#006699" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Scan Date" SortExpression="scandate">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDate" CssClass="label" runat="server" Text='<%# bind("ScanDate") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle ForeColor="#006699" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Verified" SortExpression="verifiedCount">
                                             <HeaderStyle ForeColor="#006699" />
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="verified"  ForeColor="RoyalBlue" runat="server" Text='<%# bind("verifiedCount") %>'></asp:HyperLink>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="No Cause" SortExpression="NoCauseCount">
                                             <HeaderStyle ForeColor="#006699" />
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="nocause" ForeColor="RoyalBlue" runat="server" Text='<%# bind("NoCauseCount") %>'></asp:HyperLink>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Queued" SortExpression="QueuedCount">
                                             <HeaderStyle ForeColor="#006699" />
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="queued" ForeColor="RoyalBlue" runat="server" Text='<%# bind("QueuedCount") %>'></asp:HyperLink>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Discrepancy" SortExpression="DiscrepancyCount">
                                            <HeaderStyle ForeColor="#006699" />
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="discrepancy" ForeColor="RoyalBlue" runat="server" Text='<%# bind("DiscrepancyCount") %>'></asp:HyperLink>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Pending" SortExpression="PendingCount">
                                            <HeaderStyle ForeColor="#006699" />
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="pending" ForeColor="RoyalBlue" runat="server" Text='<%# bind("PendingCount") %>'></asp:HyperLink>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <PagerStyle HorizontalAlign="Center" />
                                    </asp:GridView>
                                    </ContentTemplate>
                                <Triggers>
                                    <aspnew:AsyncPostBackTrigger ControlID="btnScan" EventName="Click" />
                                </Triggers>
                                </aspnew:UpdatePanel>
                                            </div>
                                        </section>
                                    </div>

                                </div>

                   </div>
                        </div>
                </section>
             </section>

			<TABLE id="TableMain" cellspacing="0" cellpadding="0" width="780" align="center" border="0">
			    
				
				
             
				
				
				<TR>
					<TD vAlign="top" colSpan="2" style="height: 144px">
						<TABLE id="tbl1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							
							
							<%--<tr><td>
							    
                               <uc1:footer id="Footer1" runat="server"></uc1:footer>
                                </td></tr>--%>
							<tr>
							<td style="visibility:hidden">
                                <asp:TextBox ID="txtsessionid" runat="server"></asp:TextBox>
                                <asp:TextBox ID="txtempid" runat="server"></asp:TextBox>
                                <asp:TextBox ID="txtSrv" runat="server"></asp:TextBox></td>
							</tr>
						</TABLE>
                        
					</TD>
				</TR>
			</TABLE>
    
    </div>
       
    </form>
    <script type="text/javascript">
function StartScan()
        {
            debugger;
            var ddlcrtloc=document.getElementById("ddlCourtLoc").value  ;
            var td=document.getElementById("tdProcess"); 
            var sid=document.getElementById("txtsessionid").value;
            var eid=document.getElementById("txtempid").value;
            var sSrv=document.getElementById("txtSrv").value;
            var type='network';
            var path = "<%=ViewState["vNTPATHScanTemp"]%>";
            
            if(ddlcrtloc == 0)
            {
               alert("PLease Select Court House") ;
               document.getElementById("ddlCourtLoc").focus(); 
               return false;
            }
            var sel=OZTwain1.SelectScanner();
	        if(sel=="Success")
	        {
               document.getElementById("tbl_plzwait1").style.display = "block";
		       document.getElementById("tbl_plzwait1").focus();
                OZTwain1.Acquire(sid,eid,path,type,-1);
            }
            else if(sel=="Cancel")
            {
                alert("Operation canceled by user!");
                return false;
            }
            else 
            {
                alert("Scanner not installed.");
	            return false;
            }
        }
    </script>
    <%=Session["objTwain"].ToString()%>
     <!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->


    <!-- CORE JS FRAMEWORK - START --> 
    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script> 
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>  
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>  
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE JS FRAMEWORK - END --> 


    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 

    <script src="../assets/plugins/autosize/autosize.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>
    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


    <!-- CORE TEMPLATE JS - START --> 
    <script src="../assets/js/scripts.js" type="text/javascript"></script> 
    <!-- END CORE TEMPLATE JS - END --> 
</body>
</html>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewNoCause.aspx.cs" Inherits="HTP.WebScan.NewNoCause" %>

<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="~/WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>No Cause</title>

    <script src="../Scripts/jsDate.js" type="text/javascript"></script>

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
    function openW()
    {
        window.open("previewpdf.aspx");
        return false;
    }
     
     
    function Validation()
    { 
         //Farrukh 9451 07/04/2011 replaced month textbox with dropdown
        document.getElementById("lblMessage").innerHTML="";
        
        var txtcauseno=document.getElementById("txtCauseNo").value;
        var ddlmm=document.getElementById("ddlmm").value;
        var txtdd=document.getElementById("txtdd").value;
        var txtyy=document.getElementById("txtyy").value;
        var txtRoomNo=document.getElementById("txtRoomNo").value;
        var ddlCrtLoc=document.getElementById("ddlCrtLoc").value;
        var ddlStatus=document.getElementById("ddlstatus").value;
        var ddltime=0;
        
        // Rab Nawaz Khan 10007 01/31/2012 Added to select the court location 
       if(ddlCrtLoc == "0")
        {
            alert('Please select court location');
            document.getElementById("ddlCrtLoc").focus();
            return false;
            
        }
               
       // Rab Nawaz Khan 10007 01/31/2012 display HMC courts location individualy for HMC-L HMC-D & HMC-M 
        // Abbas Qamar 10088 05-09-2012 Adding HMC-W court Id 3075
        if (ddlStatus == "47" && ((ddlCrtLoc == "3001") || (ddlCrtLoc == "3002") || (ddlCrtLoc == "3003") || (ddlCrtLoc == "3075"))) 
        {
            ddltime=document.getElementById("ddltime").value;
        }
        else
        {
            ddltime=document.getElementById("ddl_Time").value;
        }
       
       //ozair 4387 07/11/2008
        if(trim(txtcauseno) == "")
        { 
            alert("Please Enter Cause Number");
            document.getElementById("txtCauseNo").focus();
            return false;
        } 
        
        if(ddlmm!="" && txtdd!="" && txtyy!="")
		{
			var dob=ddlmm+"/"+txtdd+"/"+txtyy				
			if (!MMDDYYYYDate(dob))
			{	
				
				document.getElementById("ddlmm").focus(); 
			    return(false);
		    }			   
		}
		else
		{
			alert("Please Enter Court Date");
			document.getElementById("ddlmm").focus(); 
		    return(false);
        }

       if(trim(txtRoomNo) == "")
       { 
            alert("Please Enter the Court Room Number");
            document.getElementById("txtRoomNo").focus();
            return false;
       }
       
       if(isNaN(trim(txtRoomNo)))
       {
            alert("Please Enter Valid Court Room Number");
            document.getElementById("txtRoomNo").focus();
            return false;
       }
       //end ozair 4387
        // Rab Nawaz Khan 10007 01/31/2012 display HMC courts location individualy for HMC-L HMC-D & HMC-M
         if (ddlCrtLoc == "3001" || ddlCrtLoc == "3002" || ddlCrtLoc == "3003" || ddlCrtLoc == "3075") {
		{ 		
		    // Rab Nawaz Khan 01/31/2012 Check added for HMC-M court on court room numbers 
		        if(((txtRoomNo == 13)||(txtRoomNo== 14)) && (ddlCrtLoc == "3002") && (ddlStatus == "47"))
			    { 
					alert("Jury Trial is not a Valid Status");
					document.getElementById("ddlStatus").focus();
					return false;
			    }
			    
			     // Abbas qamar 05-16-2012 Checking the room no 20 for HMC-W and validation for Jury Trial.
			     // Zeeshan Haider 11254 07/31/2013 Commented as per requested from BackOffice team
                /*if ((txtRoomNo == 20) && (ddlCrtLoc == "3075") && (ddlStatus == "47")) {
                    alert("Jury Trial is not a Valid Status");
                    document.getElementById("ddlStatus").focus();
                    return false;
                }*/
            
            if (ddlCrtLoc == "3002" && txtRoomNo != "18" && ddlStatus != "47") 
			{
			    if ((txtRoomNo != "13") && (txtRoomNo != "14") ) 
			    {
				    alert("Court number 13 or 14 can only be set for Mykawa.");
				    document.getElementById("txtRoomNo").focus();
				    return false;
			    }
	        }
	        
	           // Abbas Qamar 10088 05-16-2012 validation for the room no 20 for HMC-W
                if (ddlCrtLoc == "3075" && txtRoomNo != "20") {

                    alert("Court number 20 can only be set for HMC-W.");
                    document.getElementById("txtRoomNo").focus();
                    return false;
                }
                // Abbas Qamar 10088 05-16-2012 validation for the room no 20 for HMC-W
                if ((ddlCrtLoc == "3002" || ddlCrtLoc == "3001" || ddlCrtLoc == "3003") && txtRoomNo == "20") {
                    alert("Court number 20 can only be set for HMC-W.");
                    document.getElementById("txtRoomNo").focus();
                    return false;
                }
            //ozair 4511 08/05/2008 court 20 check included
            if (ddlCrtLoc == "3001")
            {
                if (!(txtRoomNo >= 0 && txtRoomNo <= 12) && !(txtRoomNo >= 15 && txtRoomNo <= 17) && (ddlStatus != "47")) // Rab Nawaz Khan 10007 02/01/2012 Added Status with wrong court room number 
			    {
				    alert("Please Enter 0 to 12 or 15 to 17 for Lubbock Court");
				    return false;
			    }
			}
//		    if (((txtRoomNo < 0) || (txtRoomNo > 20) || (txtRoomNo==19)) && ((!(ddlStatus=="47")) && (ddlCrtLoc == "3001"))) // Rab Nawaz Khan 10007 02/01/2012 Case Status must not be jury trail check added
//			{
//				alert("Please Enter 0 to 18 or 20 for Houston Municipal Courts");
//				document.getElementById("txtRoomNo").focus();
//				return false;
//			}
			
			if (ddlCrtLoc == "3002"  && txtRoomNo != "18") 
			{
			    if ((txtRoomNo != "13") && (txtRoomNo != "14") ) 
			    {
				    alert("Court number 13 or 14 can only be set for Mykawa.");
				    document.getElementById("txtRoomNo").focus();
				    return false;
			    }
	        }
	        if ((ddlCrtLoc == "3002" || ddlCrtLoc == "3001")  && txtRoomNo == "18") 
			{
			        alert("Court number 18 can only be set for Dairy Ashford Court.");
				    document.getElementById("txtRoomNo").focus();
				    return false;
			}
			//end ozair 4511
		    //Asad Ali 8553 11/23/2010 Allow room no 18 for Jury Trial Status
		    //if((txtRoomNo == 13)||(txtRoomNo== 14) || (txtRoomNo== 18))
		   if ((ddlStatus == "47")) 
			{ 
			    if ((ddltime != "8:00 AM") && (ddltime != "9:00 AM") && (ddltime != "10:30 AM")) 
				{
					alert("Jury Trial must be at one of the following times: 8am, 9am or 10:30am");
					document.getElementById("ddltime").focus();
					return false;
				}
				// Rab Nawaz Khan 01/31/2012 Check added for HMC-M court on court room numbers 
		        if((txtRoomNo == 18) && ((ddlCrtLoc == "3001") || (ddlCrtLoc == "3002")))
			    { 
					alert("Court number 18 can only be set for Dairy Ashford Court");
					document.getElementById("txtRoomNo").focus();
					return false;
			    }
				//Asad Ali 8553 11/23/2010 Allow room no 18 for Jury Trial Status
				//Sabir Khan 9972 01/07/2012 Court No 5 has been added.
				// Rab Nawaz Khan 01/31/2012 Check added for HMC-M court on court room numbers 
				if ((ddlCrtLoc == "3001") && (txtRoomNo != "1")  && (txtRoomNo != "2") && (txtRoomNo != "3") && (txtRoomNo != "5") && (txtRoomNo != "6") && (txtRoomNo != "8") && (txtRoomNo != "11") && (txtRoomNo != "12")) 
				{
				    alert("Jury Trial sets at this location must be in one of the following court rooms: 1,2,3,5,6,8,11,12");
				    document.getElementById("txtRoomNo").focus();
				    return false;
				}
				// Rab Nawaz Khan 01/31/2012 Check added for HMC-M court on court room numbers 
//		        if((!((txtRoomNo == 13)||(txtRoomNo== 14))) && (ddlCrtLoc == "3002"))
//			    { 
//					alert("Court number 13 or 14 can only be set for Mykawa.");
//					document.getElementById("txtRoomNo").focus();
//					return false;
//			    }
				 // Rab Nawaz Khan 01/31/2012 Check added for HMC-M court on court room numbers 
		        if(((txtRoomNo == 13)||(txtRoomNo== 14)) && (ddlCrtLoc == "3002"))
			    { 
					alert("Jury Trial is not a Valid Status");
					document.getElementById("txtRoomNo").focus();
					return false;
			    }
			    // Rab Nawaz Khan 01/31/2012 Check added for HMC-M court on court room numbers 
		        if((txtRoomNo != 18) && (ddlCrtLoc == "3003"))
			    { 
					alert("Court number 18 can only be set for Dairy Ashford Court.");
					document.getElementById("txtRoomNo").focus();
					return false;
			    }
			}
			
			if (ddlStatus == "101" ) 
			{
				alert("This court location cannot have a status of Pre-Trial");
				document.getElementById("ddlstatus").focus();
				return false;
		    }
		    // Rab Nawaz Khan 10007 02/01/2012 check for other then HMC-L courts 
			if (((txtRoomNo < 0) || (txtRoomNo > 20) || (txtRoomNo==19)) && (ddlCrtLoc != "3001")) 
			{
				alert("Please Enter 0 to 18 or 20 for Houston Municipal Courts");
				document.getElementById("txtRoomNo").focus();
				return false;
			}
		}
							
		document.getElementById("tbl_plzwait1").style.display = 'block';
	    document.getElementById("tbl_plzwait1").focus();
    }
     
    function DeleteConfirm()
    {
        var isDelete=confirm("Are You Sure You Want To Delete This Record");
        if(isDelete== true)
        {           
        }        
        else
        {
            return false;
        }
    }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="0">
    </asp:ScriptManager>
    <div>
        <table id="TableMain" align="center" border="0" cellpadding="0" cellspacing="0" width="780">
            <tr>
                <td colspan="2">
                    <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                </td>
            </tr>
            <tr>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td class="clssubhead" height="34" background=" ../Images/subhead_bg.gif">
                                &nbsp;No Cause
                            </td>
                            <td align="right" class="clssubhead" height="34" style="font-weight: bold; font-size: 9pt;"
                                background=" ../Images/subhead_bg.gif">
                                &nbsp;<asp:HyperLink ID="search" runat="server" NavigateUrl="~/WebScan/NewSearch.aspx">Search</asp:HyperLink>
                                <asp:Label ID="lblsp" runat="server" Text="|"></asp:Label>
                                <asp:HyperLink ID="scan" runat="server" NavigateUrl="~/WebScan/Default.aspx">Scan</asp:HyperLink>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="font-family: Tahoma">
                <td class="clsLeftPaddingTable" colspan="2">
                </td>
            </tr>
            <tr style="font-family: Tahoma">
                <td align="center" class="clsLeftPaddingTable" colspan="2">
                    <asp:UpdatePanel ID="panel5" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <table id="tbl_plzwait1" style="display: none" width="800px">
                                <tr>
                                    <td class="clssubhead" valign="middle" align="center">
                                        <img src="../Images/plzwait.gif" />
                                        Please wait while we update information.
                                    </td>
                                </tr>
                            </table>
                            <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnverify" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="ImgMoveFirst" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="ImgMovePrev" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="ImgMoveNext" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="ImgMoveLast" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="img_delete" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr style="font-family: Tahoma">
                <td colspan="2" valign="top" style="height: 841px">
                    <table id="tbl1" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td width="100%">
                                <asp:UpdatePanel ID="panel1" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:GridView ID="gv_queued" runat="server" AutoGenerateColumns="False" PageSize="15"
                                            Width="100%" OnRowDataBound="gv_queued_RowDataBound">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Batch" SortExpression="batchid">
                                                    <HeaderStyle ForeColor="#006699" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblbatchid" runat="server" Text='<%# bind("batchid") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Count" SortExpression="imagecount">
                                                    <HeaderStyle ForeColor="#006699" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCount" runat="server" CssClass="Label" Text='<%# bind("Count") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Scan Type" SortExpression="scantype">
                                                    <HeaderStyle ForeColor="#006699" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblScanType" runat="server" CssClass="Label" Text='<%# bind("scantype") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Scan Date" SortExpression="scandate">
                                                    <HeaderStyle ForeColor="#006699" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDate" runat="server" CssClass="Label" Text='<%# bind("ScanDate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Verified">
                                                    <HeaderStyle ForeColor="#006699" />
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="verified" runat="server" ForeColor="RoyalBlue" Text='<%# bind("verifiedCount") %>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="No Cause" SortExpression="NoCauseCount">
                                                    <HeaderStyle ForeColor="#006699" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="nocause" runat="server" Text='<%# bind("NoCauseCount") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Queued">
                                                    <HeaderStyle ForeColor="#006699" />
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="queued" ForeColor="RoyalBlue" runat="server" Text='<%# bind("QueuedCount") %>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Discrepancy">
                                                    <HeaderStyle ForeColor="#006699" />
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="discrepancy" runat="server" ForeColor="RoyalBlue" Text='<%# bind("DiscrepancyCount") %>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Pending">
                                                    <HeaderStyle ForeColor="#006699" />
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="pending" runat="server" ForeColor="RoyalBlue" Text='<%# bind("PendingCount") %>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerStyle HorizontalAlign="Center" />
                                            <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Left" VerticalAlign="Middle" />
                                            <AlternatingRowStyle BackColor="#EEEEEE" />
                                        </asp:GridView>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btnverify" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="img_delete" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td background="../Images/separator_repeat.gif" style="height: 11px">
                            </td>
                        </tr>
                        <tr>
                            <td width="100%">
                                <asp:UpdatePanel ID="panel2" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td class="clsLeftPaddingTable" width="13%">
                                                    Crt Loc
                                                </td>
                                                <td class="clsLeftPaddingTable" width="13%">
                                                    Status
                                                </td>
                                                <td class="clsLeftPaddingTable" width="16%">
                                                    Cause Number
                                                </td>
                                                <td class="clsLeftPaddingTable" width="16%">
                                                    Crt Date
                                                </td>
                                                <td class="clsLeftPaddingTable" width="13%">
                                                    Crt Time
                                                </td>
                                                <td class="clsLeftPaddingTable" width="8%">
                                                    Crt Room
                                                </td>
                                                <td class="clsLeftPaddingTable">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr style="height: 2%">
                                                <td colspan="7">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="clsLeftPaddingTable">
                                                    <asp:DropDownList ID="ddlCrtLoc" runat="server" CssClass="clsInputCombo">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="clsLeftPaddingTable">
                                                    <asp:DropDownList ID="ddlstatus" runat="server" CssClass="clsInputCombo" AutoPostBack="True"
                                                        OnSelectedIndexChanged="ddlstatus_SelectedIndexChanged">
                                                        <asp:ListItem Value="101">PRETRIAL</asp:ListItem>
                                                        <asp:ListItem Value="103">JUDGE</asp:ListItem>
                                                        <asp:ListItem Value="47">JURY TRIAL</asp:ListItem>
                                                        <asp:ListItem Value="3">ARRAIGNMENT</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="clsLeftPaddingTable">
                                                    <asp:TextBox ID="txtCauseNo" runat="server" CssClass="clstextarea" Width="100px"
                                                        onkeyup="return autoTab(this, 20, event)"></asp:TextBox>
                                                </td>
                                                <td class="clsLeftPaddingTable">
                                                    <asp:DropDownList ID="ddlmm" Width="45px" runat="server" CssClass="clsInputCombo">
                                                        <asp:ListItem Text="" Value=""></asp:ListItem>
                                                        <asp:ListItem Text="Jan" Value="01"></asp:ListItem>
                                                        <asp:ListItem Text="Feb" Value="02"></asp:ListItem>
                                                        <asp:ListItem Text="Mar" Value="03"></asp:ListItem>
                                                        <asp:ListItem Text="Apr" Value="04"></asp:ListItem>
                                                        <asp:ListItem Text="May" Value="05"></asp:ListItem>
                                                        <asp:ListItem Text="Jun" Value="06"></asp:ListItem>
                                                        <asp:ListItem Text="Jul" Value="07"></asp:ListItem>
                                                        <asp:ListItem Text="Aug" Value="08"></asp:ListItem>
                                                        <asp:ListItem Text="Sep" Value="09"></asp:ListItem>
                                                        <asp:ListItem Text="Oct" Value="10"></asp:ListItem>
                                                        <asp:ListItem Text="Nov" Value="11"></asp:ListItem>
                                                        <asp:ListItem Text="Dec" Value="12"></asp:ListItem>
                                                    </asp:DropDownList>/
                                                    <asp:TextBox ID="txtdd" runat="server" CssClass="clstextarea" Width="25px" MaxLength="2"
                                                        onkeyup="return autoTab(this, 2, event)"></asp:TextBox>/
                                                    <asp:TextBox ID="txtyy" runat="server" CssClass="clstextarea" Width="33px" MaxLength="4"
                                                        onkeyup="return autoTab(this, 4, event)"></asp:TextBox>
                                                </td>
                                                <td class="clsLeftPaddingTable">
                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td id="time1" runat="server" style="height: 19px">
                                                                <asp:DropDownList ID="ddltime" runat="server" CssClass="clsinputcombo">
                                                                    <asp:ListItem>8:00 AM</asp:ListItem>
                                                                    <asp:ListItem>9:00 AM</asp:ListItem>
                                                                    <asp:ListItem>10:30 AM</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td id="time2" runat="server" style="height: 19px">
                                                                <asp:DropDownList ID="ddl_Time" runat="server" CssClass="clsinputcombo">
                                                                    <asp:ListItem Value="7:00 AM">7:00 AM</asp:ListItem>
                                                                    <asp:ListItem Value="7:30 AM">7:30 AM</asp:ListItem>
                                                                    <asp:ListItem Value="8:00 AM">8:00 AM</asp:ListItem>
                                                                    <asp:ListItem Value="8:30 AM">8:30 AM</asp:ListItem>
                                                                    <asp:ListItem Value="9:00 AM">9:00 AM</asp:ListItem>
                                                                    <asp:ListItem Value="9:30 AM">9:30 AM</asp:ListItem>
                                                                    <asp:ListItem Value="10:00 AM">10:00 AM</asp:ListItem>
                                                                    <asp:ListItem Value="10:30 AM">10:30 AM</asp:ListItem>
                                                                    <asp:ListItem Value="11:00 AM">11:00 AM</asp:ListItem>
                                                                    <asp:ListItem Value="11:30 AM">11:30 AM</asp:ListItem>
                                                                    <asp:ListItem Value="12:00 PM">12:00 PM</asp:ListItem>
                                                                    <asp:ListItem Value="12:30 PM">12:30 PM</asp:ListItem>
                                                                    <asp:ListItem Value="1:00 PM">1:00 PM</asp:ListItem>
                                                                    <asp:ListItem Value="1:30 PM">1:30 PM</asp:ListItem>
                                                                    <asp:ListItem Value="2:00 PM">2:00 PM</asp:ListItem>
                                                                    <asp:ListItem Value="2:30 PM">2:30 PM</asp:ListItem>
                                                                    <asp:ListItem Value="3:00 PM">3:00 PM</asp:ListItem>
                                                                    <asp:ListItem Value="3:30 PM">3:30 PM</asp:ListItem>
                                                                    <asp:ListItem Value="4:00 PM">4:00 PM</asp:ListItem>
                                                                    <asp:ListItem Value="4:30 PM">4:30 PM</asp:ListItem>
                                                                    <asp:ListItem Value="5:00 PM">5:00 PM</asp:ListItem>
                                                                    <asp:ListItem Value="5:30 PM">5:30 PM</asp:ListItem>
                                                                    <asp:ListItem Value="6:00 PM">6:00 PM</asp:ListItem>
                                                                    <asp:ListItem Value="6:30 PM">6:30 PM</asp:ListItem>
                                                                    <asp:ListItem Value="7:00 PM">7:00 PM</asp:ListItem>
                                                                    <asp:ListItem Value="7:30 PM">7:30 PM</asp:ListItem>
                                                                    <asp:ListItem Value="8:00 PM">8:00 PM</asp:ListItem>
                                                                    <asp:ListItem Value="8:30 PM">8:30 PM</asp:ListItem>
                                                                    <asp:ListItem Value="9:00 PM">9:00 PM</asp:ListItem>
                                                                    <asp:ListItem Value="9:30 PM">9:30 PM</asp:ListItem>
                                                                    <asp:ListItem Value="10:00 PM">10:00 PM</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td class="clsLeftPaddingTable">
                                                    <asp:TextBox ID="txtRoomNo" runat="server" CssClass="clstextarea" Width="30px" onkeyup="return autoTab(this, 2, event)"></asp:TextBox>
                                                </td>
                                                <td align="center" class="clsLeftPaddingTable">
                                                    <asp:Button ID="btnverify" runat="server" CssClass="clsbutton" Text="Verify" OnClick="btnverify_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btnverify" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="ImgMoveFirst" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="ImgMovePrev" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="ImgMoveNext" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="ImgMoveLast" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="img_delete" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td background="../Images/separator_repeat.gif" style="height: 11px">
                            </td>
                        </tr>
                        <tr>
                            <td width="100%">
                                <asp:UpdatePanel ID="panel4" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="left" width="33%">
                                                    <asp:ImageButton ID="ImgMoveFirst" runat="server" ImageUrl="~/Images/MoveFirst.gif"
                                                        ToolTip="Move First" OnClick="ImgMoveFirst_Click" />&nbsp;
                                                    <asp:ImageButton ID="ImgMovePrev" runat="server" ImageUrl="~/Images/MovePrevious.gif"
                                                        ToolTip="Previous" OnClick="ImgMovePrev_Click" />
                                                </td>
                                                <td width="33%">
                                                    <asp:Label ID="lblpageno" runat="server" CssClass="clsLabel"></asp:Label>&nbsp;
                                                    of &nbsp;
                                                    <asp:Label ID="lblCount" runat="server" CssClass="clsLabel"></asp:Label>&nbsp;<asp:DropDownList
                                                        ID="ddlImgsize" runat="server" OnSelectedIndexChanged="ddlImgsize_SelectedIndexChanged"
                                                        AutoPostBack="True">
                                                        <asp:ListItem Value="500">500%</asp:ListItem>
                                                        <asp:ListItem Value="400">400%</asp:ListItem>
                                                        <asp:ListItem Value="200">200%</asp:ListItem>
                                                        <asp:ListItem Value="175">175%</asp:ListItem>
                                                        <asp:ListItem Value="150" Selected="True">150%</asp:ListItem>
                                                        <asp:ListItem Value="100">100%</asp:ListItem>
                                                        <asp:ListItem Value="75">75%</asp:ListItem>
                                                        <asp:ListItem Value="50">50%</asp:ListItem>
                                                        <asp:ListItem Value="25">25%</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td width="8%">
                                                    <asp:ImageButton ID="img_delete" runat="server" ImageUrl="~/Images/cross.gif" OnClick="img_delete_Click" />
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="img_Print" runat="server" ImageUrl="~/Images/preview.gif" OnClientClick="return openW();" />
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="img_flip" runat="server" ImageUrl="~/Images/Rotate.bmp" OnClick="img_flip_Click"
                                                        ToolTip="Flip Image" />
                                                </td>
                                                <td align="right" width="33%">
                                                    <asp:ImageButton ID="ImgMoveNext" runat="server" ImageUrl="~/Images/MoveNext.gif"
                                                        ToolTip="Next" OnClick="ImgMoveNext_Click" />&nbsp;
                                                    <asp:ImageButton ID="ImgMoveLast" runat="server" ImageUrl="~/Images/MoveLast.gif"
                                                        ToolTip="Move Last" OnClick="ImgMoveLast_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btnverify" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="img_delete" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td width="100%">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="center">
                                            <asp:UpdatePanel ID="panel3" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <div style="vertical-align: middle; overflow: auto; width: 780px; height: 450px;
                                                        text-align: center">
                                                        <asp:Image ID="img_docs" runat="server" Height="840px" Width="798px" /></div>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="btnverify" EventName="Click" />
                                                    <asp:AsyncPostBackTrigger ControlID="ImgMoveFirst" EventName="Click" />
                                                    <asp:AsyncPostBackTrigger ControlID="ImgMovePrev" EventName="Click" />
                                                    <asp:AsyncPostBackTrigger ControlID="ImgMoveNext" EventName="Click" />
                                                    <asp:AsyncPostBackTrigger ControlID="ImgMoveLast" EventName="Click" />
                                                    <asp:AsyncPostBackTrigger ControlID="ddlImgsize" EventName="SelectedIndexChanged" />
                                                    <asp:AsyncPostBackTrigger ControlID="img_delete" EventName="Click" />
                                                    <asp:AsyncPostBackTrigger ControlID="img_flip" EventName="Click" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td background="../Images/separator_repeat.gif" style="height: 11px">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <uc1:Footer ID="Footer1" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td style="display: none">
                                <asp:Label ID="lbl_IsAlreadyInBatchPrint" runat="server" Text="Label"></asp:Label><asp:Label
                                    ID="lbl_IsSplit" runat="server" Text="Label"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

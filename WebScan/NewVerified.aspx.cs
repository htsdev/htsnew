using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;
using System.IO;

namespace HTP.WebScan
{
    public partial class NewVerified : System.Web.UI.Page
    {
        #region Variables

        clsLogger BugTracker = new clsLogger();
        clsENationWebComponents clsDb = new clsENationWebComponents();
        PendingClass NewPendInsert = new PendingClass();
        PendingClass Verified = new PendingClass();
        clsSession cSession = new clsSession();

        string PicID = "";
        string path = "";
        string dpath = "";
        int pageno = 0;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            //Waqas 5057 03/17/2009 Checking employee info in session
            if (cSession.IsValidSession(this.Request, this.Response, this.Session) == false)
            {
                Response.Redirect("../frmlogin.aspx", false);
            }
            //else if (cSession.GetCookie("sAccessType", this.Request).ToString() != "2")
            //{
            //    Response.Redirect("../LoginAccesserror.aspx", false);
            //}
            else
            {
                if (!IsPostBack)
                {

                    if (Request.QueryString["batchid"] != "" & Request.QueryString["batchid"] != null)
                    {
                        ViewState["BatchID"] = Request.QueryString["batchid"].ToString();
                        ViewState["vNTPATHScanImage"] = ConfigurationSettings.AppSettings["NTPATHScanImage2"].ToString();//get path from web config..
                        BindGrid();

                        FillCourtLocation();
                        DisplayOCR();
                    }
                    txtCauseNo.Focus();
                }
            }
            btnUpdate.Attributes.Add("onclick", "return Validation();");
        }

        protected void img_flip_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                string fPath = ViewState["vNTPATHScanImage"].ToString() + ViewState["BatchID"].ToString() + "-" + ViewState["PicID"].ToString() + ".jpg";
                string dpath = fPath;
                fPath = fPath.Replace("\\\\", "\\");
                System.Drawing.Image img;
                img = System.Drawing.Image.FromFile(fPath);

                img.RotateFlip(System.Drawing.RotateFlipType.RotateNoneFlipXY);

                if (File.Exists(dpath))
                {
                    File.Delete(dpath);
                    img.Save(fPath, System.Drawing.Imaging.ImageFormat.Jpeg);
                }
                else
                {
                    img.Save(fPath, System.Drawing.Imaging.ImageFormat.Jpeg);
                }

                img_docs.ImageUrl = fPath;
                Session["sBSDApath"] = fPath;
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }
        }

        protected void ddlstatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Rab Nawaz Khan 10007 01/31/2012 Court location added for the HMC courts
            if (ddlstatus.SelectedItem.Text == "JURY TRIAL" & ((ddlCrtLoc.SelectedItem.Value == "3001") || (ddlCrtLoc.SelectedItem.Value == "3002") || (ddlCrtLoc.SelectedItem.Value == "3003")))
            {
                ddltime.Visible = true;
                ddl_Time.Visible = false;
            }
            else
            {
                ddltime.Visible = false;
                ddl_Time.Visible = true;
            }
        }

        protected void ddlImgsize_SelectedIndexChanged(object sender, EventArgs e)
        {
            int Width = 0;
            int Height = 0;
            int FixWidth = 798;
            int FixHeight = 840;
            int ddlsize = 0;
            try
            {

                ddlsize = Convert.ToInt32(ddlImgsize.SelectedValue);
                ddlsize = ddlsize - 150;

                Width = FixWidth / 100 * ddlsize;
                Width = Width + FixWidth;

                Height = FixHeight / 100 * ddlsize;
                Height = Height + FixHeight;

                img_docs.Width = Width;
                img_docs.Height = Height;
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }
        }

        protected void gv_verified_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string batchid = "";

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                batchid = ((Label)e.Row.FindControl("lblbatchid")).Text.ToString();

                if (((HyperLink)e.Row.FindControl("queued")).Text == "0")
                { ((HyperLink)e.Row.FindControl("queued")).NavigateUrl = ""; }
                else
                { ((HyperLink)e.Row.FindControl("queued")).NavigateUrl = "NewQueued.aspx?batchid=" + batchid; }

                if (((HyperLink)e.Row.FindControl("nocause")).Text == "0")
                { ((HyperLink)e.Row.FindControl("nocause")).NavigateUrl = ""; }
                else
                { ((HyperLink)e.Row.FindControl("nocause")).NavigateUrl = "NewNoCause.aspx?batchid=" + batchid; }

                if (((HyperLink)e.Row.FindControl("discrepancy")).Text == "0")
                { ((HyperLink)e.Row.FindControl("discrepancy")).NavigateUrl = ""; }
                else
                { ((HyperLink)e.Row.FindControl("discrepancy")).NavigateUrl = "NewDiscrepancy.aspx?batchid=" + batchid; }
                if (((HyperLink)e.Row.FindControl("pending")).Text == "0")
                { ((HyperLink)e.Row.FindControl("pending")).NavigateUrl = ""; }
                else
                { ((HyperLink)e.Row.FindControl("pending")).NavigateUrl = "NewPending.aspx?batchid=" + batchid; }

            }
        }

        protected void ImgMoveFirst_Click(object sender, ImageClickEventArgs e)
        {
            lblMessage.Text = "";
            string CourtDate = "";
            try
            {
                RefreshControls();
                string[] key = { "@BatchID" };
                object[] value = { Convert.ToInt32(ViewState["BatchID"]) };
                DataSet DS_MoveFirst = clsDb.Get_DS_BySPArr("Usp_WebScan_GetVerifiedData", key, value);//Usp_WebScan_GetVerifiedPics
                if (DS_MoveFirst.Tables[0].Rows.Count > 0)
                {
                    ViewState["PicID"] = DS_MoveFirst.Tables[0].Rows[0]["picid"].ToString();

                    txtCauseNo.Text = DS_MoveFirst.Tables[0].Rows[0]["CauseNo"].ToString();
                    CourtDate = DS_MoveFirst.Tables[0].Rows[0]["CourtDate"].ToString();
                    if (CourtDate != "" && CourtDate.Length == 10)
                    {
                        //Farrukh 9451 07/04/2011 Replaced month textbox with dropdown
                        ddlmm.SelectedValue = CourtDate.Substring(0, 2);
                        txtdd.Text = CourtDate.Substring(3, 2);
                        txtyy.Text = CourtDate.Substring(6);
                    }
                    txtRoomNo.Text = DS_MoveFirst.Tables[0].Rows[0]["CourtRoomNo"].ToString();
                    // Rab Nawaz Khan 10007 01/31/2012 FindByText Replaced with FindByValue
                    //ddlCrtLoc.Items.FindByValue("0").Selected = true;
                    ddlstatus.Items.FindByText(DS_MoveFirst.Tables[0].Rows[0]["Status"].ToString()).Selected = true;
                    if (DS_MoveFirst.Tables[0].Rows[0]["Status"].ToString().ToUpper() == "JURY TRIAL" & DS_MoveFirst.Tables[0].Rows[0]["CourtLocation"].ToString().ToUpper() == "HMC")
                    {
                        //ozair 4386 07/11/2008 time selection issue
                        ddltime.SelectedValue = DS_MoveFirst.Tables[0].Rows[0]["CourtTime"].ToString();
                        //end ozair 4386             
                        ddltime.Visible = true;
                        ddl_Time.Visible = false;
                    }
                    else
                    {
                        //ozair 4386 07/11/2008 time selection issue
                        ddl_Time.SelectedValue = DS_MoveFirst.Tables[0].Rows[0]["CourtTime"].ToString();
                        //end ozair 4386
                        ddltime.Visible = false;
                        ddl_Time.Visible = true;
                    }
                    dpath = ViewState["vNTPATHScanImage"].ToString() + ViewState["BatchID"].ToString() + "-" + DS_MoveFirst.Tables[0].Rows[0]["picid"].ToString() + ".jpg";

                    if (!File.Exists(dpath))
                    {
                        img_docs.ImageUrl = "~/Images/not-available.gif";
                        Session["sBSDApath"] = Server.MapPath("/Images/") + "not-available.gif";
                    }
                    else
                    {
                        img_docs.ImageUrl = dpath;
                        Session["sBSDApath"] = dpath;
                    }
                    lblpageno.Text = "1";
                }
                DisabledImageButtons();
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;

            }
        }

        protected void ImgMoveLast_Click(object sender, ImageClickEventArgs e)
        {
            lblMessage.Text = "";
            string CourtDate = "";
            try
            {
                RefreshControls();
                string[] key = { "@BatchID" };
                object[] value = { Convert.ToInt32(ViewState["BatchID"]) };
                DataSet DS_MoveLast = clsDb.Get_DS_BySPArr("Usp_WebScan_GetVerifiedData", key, value);
                if (DS_MoveLast.Tables[0].Rows.Count > 0)
                {
                    int cnt = Convert.ToInt32(DS_MoveLast.Tables[0].Rows.Count);
                    ViewState["PicID"] = DS_MoveLast.Tables[0].Rows[cnt - 1]["picid"].ToString();

                    txtCauseNo.Text = DS_MoveLast.Tables[0].Rows[cnt - 1]["CauseNo"].ToString();
                    CourtDate = DS_MoveLast.Tables[0].Rows[cnt - 1]["CourtDate"].ToString();
                    if (CourtDate != "" && CourtDate.Length == 10)
                    {
                        //Farrukh 9451 07/04/2011 Replaced month textbox with dropdown
                        ddlmm.SelectedValue = CourtDate.Substring(0, 2);
                        txtdd.Text = CourtDate.Substring(3, 2);
                        txtyy.Text = CourtDate.Substring(6);
                    }
                    txtRoomNo.Text = DS_MoveLast.Tables[0].Rows[cnt - 1]["CourtRoomNo"].ToString();
                    // Rab Nawaz Khan 10007 01/31/2012 FindByText Replaced with FindByValue
                    //ddlCrtLoc.Items.FindByValue("0").Selected = true;
                    ddlstatus.Items.FindByText(DS_MoveLast.Tables[0].Rows[cnt - 1]["Status"].ToString()).Selected = true;
                    if (DS_MoveLast.Tables[0].Rows[cnt - 1]["Status"].ToString().ToUpper() == "JURY TRIAL" & DS_MoveLast.Tables[0].Rows[cnt - 1]["CourtLocation"].ToString().ToUpper() == "HMC")
                    {
                        //ozair 4386 07/11/2008 time selection issue
                        ddltime.SelectedValue = DS_MoveLast.Tables[0].Rows[cnt - 1]["CourtTime"].ToString();
                        //end ozair 4386
                        ddltime.Visible = true;
                        ddl_Time.Visible = false;
                    }
                    else
                    {
                        //ozair 4386 07/11/2008 time selection issue
                        ddl_Time.SelectedValue = DS_MoveLast.Tables[0].Rows[cnt - 1]["CourtTime"].ToString();
                        //end ozair 4386
                        ddltime.Visible = false;
                        ddl_Time.Visible = true;
                    }
                    dpath = ViewState["vNTPATHScanImage"].ToString() + ViewState["BatchID"].ToString() + "-" + DS_MoveLast.Tables[0].Rows[cnt - 1]["picid"].ToString() + ".jpg";

                    if (!File.Exists(dpath))
                    {
                        img_docs.ImageUrl = "~/Images/not-available.gif";
                        Session["sBSDApath"] = Server.MapPath("/Images/") + "not-available.gif";
                    }
                    else
                    {
                        img_docs.ImageUrl = dpath;
                        Session["sBSDApath"] = dpath;
                    }
                    lblpageno.Text = cnt.ToString();
                }
                DisabledImageButtons();
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;

            }
        }

        protected void ImgMovePrev_Click(object sender, ImageClickEventArgs e)
        {
            lblMessage.Text = "";
            string CourtDate = "";
            try
            {
                RefreshControls();
                string[] key = { "@BatchID" };
                object[] value = { Convert.ToInt32(ViewState["BatchID"]) };
                DataSet DS_MovePrevious = clsDb.Get_DS_BySPArr("Usp_WebScan_GetVerifiedData", key, value);
                if (DS_MovePrevious.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < DS_MovePrevious.Tables[0].Rows.Count; i++)
                    {
                        string pid = DS_MovePrevious.Tables[0].Rows[i]["picid"].ToString();

                        if (Convert.ToInt32(pid) == Convert.ToInt32(ViewState["PicID"]))
                        {
                            ViewState["PicID"] = DS_MovePrevious.Tables[0].Rows[i - 1]["picid"].ToString();

                            txtCauseNo.Text = DS_MovePrevious.Tables[0].Rows[i - 1]["CauseNo"].ToString();
                            CourtDate = DS_MovePrevious.Tables[0].Rows[i - 1]["CourtDate"].ToString();
                            if (CourtDate != "" && CourtDate.Length == 10)
                            {
                                //Farrukh 9451 07/04/2011 Replaced month textbox with dropdown
                                ddlmm.SelectedValue = CourtDate.Substring(0, 2);
                                txtdd.Text = CourtDate.Substring(3, 2);
                                txtyy.Text = CourtDate.Substring(6);
                            }
                            txtRoomNo.Text = DS_MovePrevious.Tables[0].Rows[i - 1]["CourtRoomNo"].ToString();
                            // Rab Nawaz Khan 10007 01/31/2012 FindByText Replaced with FindByValue
                           // ddlCrtLoc.Items.FindByValue("0").Selected = true;
                            ddlstatus.Items.FindByText(DS_MovePrevious.Tables[0].Rows[i - 1]["Status"].ToString()).Selected = true;
                            if (DS_MovePrevious.Tables[0].Rows[i - 1]["Status"].ToString().ToUpper() == "JURY TRIAL" & DS_MovePrevious.Tables[0].Rows[i - 1]["CourtLocation"].ToString().ToUpper() == "HMC")
                            {
                                //ozair 4386 07/11/2008 time selection issue
                                ddltime.SelectedValue = DS_MovePrevious.Tables[0].Rows[i - 1]["CourtTime"].ToString();
                                //end ozair 4386
                                ddltime.Visible = true;
                                ddl_Time.Visible = false;
                            }
                            else
                            {
                                //ozair 4386 07/11/2008 time selection issue
                                ddl_Time.SelectedValue = DS_MovePrevious.Tables[0].Rows[i - 1]["CourtTime"].ToString();
                                //end ozair 4386
                                ddltime.Visible = false;
                                ddl_Time.Visible = true;
                            }

                            dpath = ViewState["vNTPATHScanImage"].ToString() + ViewState["BatchID"].ToString() + "-" + DS_MovePrevious.Tables[0].Rows[i - 1]["picid"].ToString() + ".jpg";
                            pageno = Convert.ToInt32(lblpageno.Text);
                            pageno = pageno - 1;
                            lblpageno.Text = pageno.ToString();
                            if (!File.Exists(dpath))
                            {
                                img_docs.ImageUrl = "~/Images/not-available.gif";
                                Session["sBSDApath"] = Server.MapPath("/Images/") + "not-available.gif";
                            }
                            else
                            {
                                img_docs.ImageUrl = dpath;
                                Session["sBSDApath"] = dpath;
                            }
                            break;
                        }
                    }
                }
                DisabledImageButtons();
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;

            }
        }

        protected void ImgMoveNext_Click(object sender, ImageClickEventArgs e)
        {
            lblMessage.Text = "";
            string CourtDate = "";

            try
            {
                RefreshControls();
                string[] key = { "@BatchID" };
                object[] value = { Convert.ToInt32(ViewState["BatchID"]) };
                DataSet DS_MoveNext = clsDb.Get_DS_BySPArr("Usp_WebScan_GetVerifiedData", key, value);
                if (DS_MoveNext.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < DS_MoveNext.Tables[0].Rows.Count; i++)
                    {
                        string pid = DS_MoveNext.Tables[0].Rows[i]["picid"].ToString();
                        if (ViewState["PicID"] == null)
                        {
                            ViewState["PicID"] = DS_MoveNext.Tables[0].Rows[i + 1]["picid"].ToString();

                            txtCauseNo.Text = DS_MoveNext.Tables[0].Rows[i + 1]["CauseNo"].ToString();
                            CourtDate = DS_MoveNext.Tables[0].Rows[i + 1]["CourtDate"].ToString();
                            if (CourtDate != "" && CourtDate.Length == 10)
                            {
                                //Farrukh 9451 07/04/2011 Replaced month textbox with dropdown
                                ddlmm.SelectedValue = CourtDate.Substring(0, 2);
                                txtdd.Text = CourtDate.Substring(3, 2);
                                txtyy.Text = CourtDate.Substring(6);
                            }
                            txtRoomNo.Text = DS_MoveNext.Tables[0].Rows[i + 1]["CourtRoomNo"].ToString();
                            // Rab Nawaz Khan 10007 01/31/2012 FindByText Replaced with FindByValue
                            //ddlCrtLoc.Items.FindByValue("0").Selected = true;
                            ddlstatus.Items.FindByText(DS_MoveNext.Tables[0].Rows[i + 1]["Status"].ToString()).Selected = true;
                            if (DS_MoveNext.Tables[0].Rows[i + 1]["Status"].ToString().ToUpper() == "JURY TRIAL" & DS_MoveNext.Tables[0].Rows[i + 1]["CourtLocation"].ToString().ToUpper() == "HMC")
                            {
                                //ozair 4386 07/11/2008 time selection issue
                                ddltime.SelectedValue = DS_MoveNext.Tables[0].Rows[i + 1]["CourtTime"].ToString();
                                //end ozair 4386                    
                                ddltime.Visible = true;
                                ddl_Time.Visible = false;
                            }
                            else
                            {
                                //ozair 4386 07/11/2008 time selection issue
                                ddl_Time.SelectedValue = DS_MoveNext.Tables[0].Rows[i + 1]["CourtTime"].ToString();
                                //end ozair 4386
                                ddltime.Visible = false;
                                ddl_Time.Visible = true;
                            }

                            dpath = ViewState["vNTPATHScanImage"].ToString() + ViewState["BatchID"].ToString() + "-" + DS_MoveNext.Tables[0].Rows[i + 1]["picid"].ToString() + ".jpg";
                            pageno = Convert.ToInt32(lblpageno.Text);
                            pageno = pageno + 1;
                            lblpageno.Text = pageno.ToString();
                            if (!File.Exists(dpath))
                            {
                                img_docs.ImageUrl = "~/Images/not-available.gif";
                                Session["sBSDApath"] = Server.MapPath("/Images/") + "not-available.gif";
                            }
                            else
                            {
                                img_docs.ImageUrl = dpath;
                                Session["sBSDApath"] = dpath;
                            }
                            break;

                        }
                        else
                        {
                            if (Convert.ToInt32(pid) == Convert.ToInt32(ViewState["PicID"]))
                            {
                                ViewState["PicID"] = DS_MoveNext.Tables[0].Rows[i + 1]["picid"].ToString();

                                txtCauseNo.Text = DS_MoveNext.Tables[0].Rows[i + 1]["CauseNo"].ToString();
                                CourtDate = DS_MoveNext.Tables[0].Rows[i + 1]["CourtDate"].ToString();
                                if (CourtDate != "" && CourtDate.Length == 10)
                                {
                                    //Farrukh 9451 07/04/2011 Replaced month textbox with dropdown
                                    ddlmm.SelectedValue = CourtDate.Substring(0, 2);
                                    txtdd.Text = CourtDate.Substring(3, 2);
                                    txtyy.Text = CourtDate.Substring(6);
                                }
                                txtRoomNo.Text = DS_MoveNext.Tables[0].Rows[i + 1]["CourtRoomNo"].ToString();
                                //ddlCrtLoc.Items.FindByText(DS_MoveNext.Tables[0].Rows[i + 1]["CourtLocation"].ToString()).Selected = true;
                                ddlstatus.Items.FindByText(DS_MoveNext.Tables[0].Rows[i + 1]["Status"].ToString()).Selected = true;
                                if (DS_MoveNext.Tables[0].Rows[i + 1]["Status"].ToString().ToUpper() == "JURY TRIAL" & DS_MoveNext.Tables[0].Rows[i + 1]["CourtLocation"].ToString().ToUpper() == "HMC")
                                {
                                    //ozair 4386 07/11/2008 time selection issue
                                    ddltime.SelectedValue = DS_MoveNext.Tables[0].Rows[i + 1]["CourtTime"].ToString();
                                    //end ozair 4386                                 
                                    ddltime.Visible = true;
                                    ddl_Time.Visible = false;
                                }
                                else
                                {
                                    //ozair 4386 07/11/2008 time selection issue
                                    ddl_Time.SelectedValue = DS_MoveNext.Tables[0].Rows[i + 1]["CourtTime"].ToString();
                                    //end ozair 4386
                                    ddltime.Visible = false;
                                    ddl_Time.Visible = true;
                                }
                                dpath = ViewState["vNTPATHScanImage"].ToString() + ViewState["BatchID"].ToString() + "-" + DS_MoveNext.Tables[0].Rows[i + 1]["picid"].ToString() + ".jpg";
                                pageno = Convert.ToInt32(lblpageno.Text);
                                pageno = pageno + 1;
                                lblpageno.Text = pageno.ToString();
                                if (!File.Exists(dpath))
                                {
                                    img_docs.ImageUrl = "~/Images/not-available.gif";
                                    Session["sBSDApath"] = Server.MapPath("/Images/") + "not-available.gif";
                                }
                                else
                                {
                                    img_docs.ImageUrl = dpath;
                                    Session["sBSDApath"] = dpath;
                                }
                                break;
                            }
                        }
                    }
                }
                DisabledImageButtons();
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;

            }
        }

        protected void logout_Click(object sender, EventArgs e)
        {
            try
            {
                Session.Abandon();//session clear..
                Response.Redirect("Login.aspx");
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            Verifie();
            txtCauseNo.Focus();
        }

        #endregion

        #region Methods

        private void FillCourtLocation()
        {
            try
            {
                DataSet ds_Court = clsDb.Get_DS_BySP("usp_HTS_GetShortCourtName");
                ddlCrtLoc.Items.Clear();
                if(ds_Court.Tables[0].Rows[0]["Courtid"].ToString() == "0")
                {   
                    ds_Court.Tables[0].Rows[0].Delete();
                }

                ddlCrtLoc.DataSource = ds_Court.Tables[0];
                ddlCrtLoc.DataTextField = "shortname";
                ddlCrtLoc.DataValueField = "courtid";
                ddlCrtLoc.DataBind();

                // Rab Nawaz Khan 10007 01/31/2012 Comment the code to display HMC courts individualy 
                //ddlCrtLoc.Items[0].Text = "---Choose---";
                //ddlCrtLoc.Items[1].Text = "HMC";
                //ddlCrtLoc.Items[1].Value = "HMC";
                //ddlCrtLoc.SelectedValue = "HMC";
                //ddlCrtLoc.Items.RemoveAt(2);
                //ddlCrtLoc.Items.RemoveAt(2);

            }
            catch (Exception ex)
            {
                lblMessage.Visible = true;
                lblMessage.Text = ex.Message;

                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void BindGrid()
        {
            try
            {
                string[] key = { "@BatchID" };
                object[] value = { Convert.ToInt32(ViewState["BatchID"]) };
                DataSet DS_Verified = clsDb.Get_DS_BySPArr("Usp_WebScan_GetPending", key, value);
                if (DS_Verified.Tables[0].Rows.Count > 0)
                {
                    gv_verified.DataSource = DS_Verified;
                    gv_verified.DataBind();
                    DisplayImage();
                }
                else
                {
                    lblMessage.Text = "No Record in Pending";
                    lblMessage.Visible = true;
                }
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }

        }

        private void DisplayImage()
        {
            try
            {
                string[] key = { "@BatchID" };
                object[] value = { Convert.ToInt32(ViewState["BatchID"]) };
                DataSet DS_Display = clsDb.Get_DS_BySPArr("Usp_WebScan_GetVerifiedPics", key, value);
                if (DS_Display.Tables[0].Rows.Count > 0)
                {
                    PicID = DS_Display.Tables[0].Rows[0]["PicID"].ToString();
                    path = ViewState["vNTPATHScanImage"].ToString() + ViewState["BatchID"].ToString() + "-" + PicID + ".jpg";
                    ViewState["PicID"] = PicID;
                    if (!File.Exists(path))
                    {
                        img_docs.ImageUrl = "~/Images/not-available.gif";
                        Session["sBSDApath"] = Server.MapPath("/Images/") + "not-available.gif";
                    }
                    else
                    {
                        img_docs.ImageUrl = path;
                        Session["sBSDApath"] = path;
                    }
                    lblpageno.Text = "1";
                    lblCount.Text = DS_Display.Tables[0].Rows.Count.ToString();
                    DisabledImageButtons();
                }
                else
                {
                    lblpageno.Text = "0";
                    lblCount.Text = "0";
                    DisabledImageButtons();
                    img_docs.Visible = false;
                    ddlImgsize.Enabled = false;

                    ddlCrtLoc.ClearSelection();
                    ddltime.ClearSelection();
                    ddl_Time.ClearSelection();
                    ddlstatus.ClearSelection();

                    txtCauseNo.Text = "";
                    txtdd.Text = "";
                    //Farrukh 9451 07/04/2011 Replaced month textbox with dropdown
                    ddlmm.ClearSelection();
                    txtyy.Text = "";
                    txtRoomNo.Text = "";

                    img_flip.Enabled = false;
                }


            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }
        }

        private void DisabledImageButtons()
        {
            if (lblpageno.Text == lblCount.Text)
            {
                ImgMoveLast.Enabled = false;
                ImgMoveNext.Enabled = false;
            }
            else
            {
                ImgMoveLast.Enabled = true;
                ImgMoveNext.Enabled = true;
            }
            if (lblpageno.Text == "1")
            {
                ImgMoveFirst.Enabled = false;
                ImgMovePrev.Enabled = false;
            }
            else if (lblpageno.Text == "0")
            {
                ImgMoveFirst.Enabled = false;
                ImgMovePrev.Enabled = false;
            }
            else
            {
                ImgMoveFirst.Enabled = true;
                ImgMovePrev.Enabled = true;
            }
        }

        private void DisplayOCR()
        {
            string CourtDate = "";
            try
            {

                string[] key = { "@BatchID" };
                object[] value = { Convert.ToInt32(ViewState["BatchID"]) };
                DataSet DS_DisplayOCR = clsDb.Get_DS_BySPArr("Usp_WebScan_GetVerifiedData", key, value);
                if (DS_DisplayOCR.Tables[0].Rows.Count > 0)
                {
                    txtCauseNo.Text = DS_DisplayOCR.Tables[0].Rows[0]["CauseNo"].ToString();
                    CourtDate = DS_DisplayOCR.Tables[0].Rows[0]["CourtDate"].ToString();
                    if (CourtDate != "" && CourtDate.Length == 10)
                    {
                        //Farrukh 9451 07/04/2011 Replaced month textbox with dropdown
                        ddlmm.SelectedValue = CourtDate.Substring(0, 2);
                        txtdd.Text = CourtDate.Substring(3, 2);
                        txtyy.Text = CourtDate.Substring(6);
                    }
                    txtRoomNo.Text = DS_DisplayOCR.Tables[0].Rows[0]["CourtRoomNo"].ToString();
                    // Rab Nawaz Khan 10007 01/31/2012 Find by text replaced by Find by Value 
                    //ddlCrtLoc.Items.FindByValue("3001").Selected = true;
                    ddlstatus.Items.FindByText(DS_DisplayOCR.Tables[0].Rows[0]["Status"].ToString()).Selected = true;
                    if (DS_DisplayOCR.Tables[0].Rows[0]["Status"].ToString().ToUpper() == "JURY TRIAL" & DS_DisplayOCR.Tables[0].Rows[0]["CourtLocation"].ToString().ToUpper() == "HMC")
                    {
                        //ozair 4386 07/11/2008 time selection issue
                        ddltime.SelectedValue = DS_DisplayOCR.Tables[0].Rows[0]["CourtTime"].ToString();
                        //end ozair 4386              
                        ddltime.Visible = true;
                        ddl_Time.Visible = false;
                    }
                    else
                    {
                        //ozair 4386 07/11/2008 time selection issue
                        ddl_Time.SelectedValue = DS_DisplayOCR.Tables[0].Rows[0]["CourtTime"].ToString();
                        //end ozair 4386
                        ddltime.Visible = false;
                        ddl_Time.Visible = true;
                    }
                }
                else
                {
                    ddltime.Visible = true;
                    ddl_Time.Visible = false;
                }

            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }
        }

        private void Verifie()
        {
            lblMessage.Text = "";
            //Farrukh 9451 07/04/2011 Replaced month textbox with dropdown
            string CourtDate = ddlmm.SelectedValue + "/" + txtdd.Text + "/" + txtyy.Text;
            try
            {
                Verified.PicID = Convert.ToInt32(ViewState["PicID"]);
                Verified.CauseNo = txtCauseNo.Text.ToString().Replace(" ", "").ToUpper();
                Verified.CourtDateMain = Convert.ToDateTime(CourtDate);
                // Rab Nawaz Khan 10007 01/31/2012 Replace the drop down selected Text to Selected Value and added the HMC court ids 
                if (ddlstatus.SelectedItem.Text.ToString().ToUpper() == "JURY TRIAL" & ((ddlCrtLoc.SelectedItem.Value == "3001") || (ddlCrtLoc.SelectedItem.Value == "3002") || (ddlCrtLoc.SelectedItem.Value == "3003")))
                {
                    Verified.Time = ddltime.SelectedValue.ToString();
                    Verified.CourtDateScan = Convert.ToDateTime(CourtDate + " " + ddltime.SelectedValue);
                }
                else
                {
                    Verified.Time = ddl_Time.SelectedValue.ToString();
                    Verified.CourtDateScan = Convert.ToDateTime(CourtDate + " " + ddl_Time.SelectedValue);
                }
                //ozair 4387 07/11/2008 Input string format issue handled in java script too
                Verified.CourtNo = Convert.ToInt32(txtRoomNo.Text.Trim());
                //end ozair 4387
                Verified.Status = ddlstatus.SelectedItem.Text.ToString();
                // Rab Nawaz Khan 10007 01/31/2012 Insert HMC in database as court location insted of seprate HMC court Names
                if (ddlCrtLoc.SelectedItem.Value == "3001" || ddlCrtLoc.SelectedItem.Value == "3002" || ddlCrtLoc.SelectedItem.Value == "3003")
                    Verified.Location = "HMC";
                else
                    Verified.Location = ddlCrtLoc.SelectedItem.Text.ToString();
                // END 10007   
                
                Verified.EmployeeID = Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request));
                int i = Verified.Verify();
                if (i == 1)
                {
                    lblMessage.Text = "Cause No Doest Not Exist in the System. Record Moved to No Cause Section.";
                    lblMessage.Visible = true;
                }
                //ozair 4330 07/02/2008 now the procedure will return 0 too for verified record.
                else if (i == 2 || i == 0)
                {
                    lblMessage.Text = "Record Verified.";
                    lblMessage.Visible = true;
                    //Scan Document Entry

                    try
                    {
                        int empid;
                        int TicketID;
                        string[] key = { "@CauseNo" };

                        object[] value1 = { txtCauseNo.Text.ToString().Replace(" ", "").ToUpper() };
                        DataSet ds = clsDb.Get_DS_BySPArr("usp_webscan_GetTicketIDByCauseNo", key, value1);
                        if (ds.Tables.Count > 0)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int l = 0; l < ds.Tables[0].Rows.Count; l++)
                                {
                                    clsCase ClsCase = new clsCase();
                                    TicketID = Convert.ToInt32(ds.Tables[0].Rows[l]["TicketID"].ToString());
                                    ViewState["vTicketID"] = TicketID;
                                    //Perform database Uploading Task

                                    string searchpat = ViewState["BatchID"].ToString() + "-" + ViewState["PicID"].ToString() + ".jpg";
                                    //string[] fileName = Directory.GetFiles(Server.MapPath("tempimages/"), searchpat);
                                    string[] fileName = Directory.GetFiles(ViewState["vNTPATHScanImage"].ToString(), searchpat);
                                    empid = Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request));

                                    string bType = "Resets";
                                    int BookId = 0, picID = 0;

                                    string picName, picDestination;

                                    int DocCount = 1;
                                    for (int ii = 0; ii < fileName.Length; ii++)
                                    {
                                        string[] key1 = { "@updated", "@extension", "@Description", "@DocType", "@Employee", "@TicketID", "@Count", "@Book", "@BookID" };
                                        object[] value11 = { DateTime.Now, "JPG", "BSDA - Batch ID: " + ViewState["BatchID"].ToString(), bType, empid, TicketID, DocCount, BookId, "" };
                                        //call sP and get the book ID back from sP
                                        picName = clsDb.InsertBySPArrRet("usp_Add_ScanImage", key1, value11).ToString();
                                        string BookI = picName.Split('-')[0];
                                        string picI = picName.Split('-')[1];
                                        BookId = (int)Convert.ChangeType(BookI, typeof(int)); ; //
                                        picID = (int)Convert.ChangeType(picI, typeof(int)); ; //

                                        //Move file
                                        picDestination = ConfigurationSettings.AppSettings["NTPATHScanImage"].ToString() + picName + ".jpg"; //DestinationImage

                                        System.IO.File.Copy(fileName[ii].ToString(), picDestination);

                                        //Ozair 5799 04/14/2009 Checking For Court Settings info chnaged
                                        if (ClsCase.IsCourtSettingInfoChangedDuringBSDA(TicketID))
                                        {
                                            //Modified by ozair on 02/29/2008
                                            // Sending the trial notification to Batch
                                            //Outside court and status in Arr,Pre,Jury,Judge today or future courtdate
                                            DateTime dcourt = Convert.ToDateTime(ds.Tables[0].Rows[l]["CourtDateMain"]);
                                            if (Verified.NoLetterFlagCheck(TicketID) == true)//if TicketID has no letter flag then restrict trial letter for sending it to batch..
                                            {
                                                if ((dcourt >= DateTime.Now.Date) && (Convert.ToInt32(ds.Tables[0].Rows[l]["categoryid"]) == 2 || Convert.ToInt32(ds.Tables[0].Rows[l]["categoryid"]) == 3 || Convert.ToInt32(ds.Tables[0].Rows[l]["categoryid"]) == 4 || Convert.ToInt32(ds.Tables[0].Rows[l]["categoryid"]) == 5) && (Convert.ToInt32(ds.Tables[0].Rows[l]["CourtID"]) != 3001 && Convert.ToInt32(ds.Tables[0].Rows[l]["CourtID"]) != 3002 && Convert.ToInt32(ds.Tables[0].Rows[l]["CourtID"]) != 3003))
                                                {
                                                    if (ClsCase.ValidateTrialLetterPrintOption(TicketID))
                                                    {
                                                        //
                                                        IsAlreadyInBatchPrint();
                                                        if (lbl_IsAlreadyInBatchPrint.Text == "1")
                                                        {
                                                            RemoveExistingEntry();
                                                        }
                                                        string[] key2 = { "@TicketIDList", "@empid" };
                                                        object[] value12 = { TicketID, empid };
                                                        Int32 LetterType = 2;
                                                        //Yasir Kamal 7590 03/29/2010 trial letter modifications
                                                        //string filename = Server.MapPath("..\\Reports") + "\\Trial_Notification.rpt";
                                                        clsCrsytalComponent ClsCr = new clsCrsytalComponent();
                                                        //ClsCr.CreateReportForEntry(filename, "USP_HTS_Trialletter", key2, value12, "true", LetterType, this.Session, this.Response);
                                                        ClsCr.InsertIntoBatch(Convert.ToInt32(value12[0]), Convert.ToInt32(value12[1]), LetterType);

                                                        //
                                                    }

                                                }
                                                else
                                                    //Inside court status in Arr
                                                    if ((dcourt >= DateTime.Now.Date) && Convert.ToInt32(ds.Tables[0].Rows[l]["categoryid"]) == 4 && (Convert.ToInt32(ds.Tables[0].Rows[l]["CourtId"]) == 3001 || Convert.ToInt32(ds.Tables[0].Rows[l]["CourtId"]) == 3002 || Convert.ToInt32(ds.Tables[0].Rows[l]["CourtId"]) == 3003))
                                                    {
                                                        if (ClsCase.ValidateTrialLetterPrintOption(TicketID))
                                                        {
                                                            //
                                                            IsAlreadyInBatchPrint();
                                                            if (lbl_IsAlreadyInBatchPrint.Text == "1")
                                                            {
                                                                RemoveExistingEntry();
                                                            }
                                                            string[] key2 = { "@TicketIDList", "@empid" };
                                                            object[] value12 = { TicketID, empid };
                                                            Int32 LetterType = 2;
                                                            //Yasir Kamal 7590 03/29/2010 trial letter modifications
                                                            //string filename = Server.MapPath("..\\Reports") + "\\Trial_Notification.rpt";
                                                            clsCrsytalComponent ClsCr = new clsCrsytalComponent();
                                                            //ClsCr.CreateReportForEntry(filename, "USP_HTS_Trialletter", key2, value12, "true", LetterType, this.Session, this.Response);
                                                            ClsCr.InsertIntoBatch(Convert.ToInt32(value12[0]), Convert.ToInt32(value12[1]), LetterType);

                                                            //
                                                        }

                                                    }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        lblMessage.Text = ex.Message;
                        BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                    }

                    //
                }
                BindGrid();
                RefreshControls();
                DisplayOCR();

            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }
        }

        private void IsAlreadyInBatchPrint()
        {
            string[] keys = { "@TicketID" };
            object[] values = { Convert.ToInt32(ViewState["vTicketID"].ToString()) };
            DataTable dtBatchPrint = new DataTable();
            dtBatchPrint = clsDb.Get_DT_BySPArr("USP_HTS_GET_BATCHLETTER_CHECK_BATCHPRINT", keys, values);
            if (Convert.ToInt32(dtBatchPrint.Rows[0][0].ToString()) > 0)
                lbl_IsAlreadyInBatchPrint.Text = "1";
            else
                lbl_IsAlreadyInBatchPrint.Text = "0";
        }

        private void RemoveExistingEntry()
        {
            //FileInfo fi = new FileInfo("");
            //fi.Delete(); 
            string[] keys = { "@TicketID" };
            object[] values = { Convert.ToInt32(ViewState["vTicketID"].ToString()) };
            clsDb.ExecuteSP("USP_HTS_BATCHLETTERS_DELETE_EXISTING", keys, values);

        }

        private void RefreshControls()
        {
            try
            {

                ddlCrtLoc.ClearSelection();
                ddltime.ClearSelection();
                ddl_Time.ClearSelection();
                ddlstatus.ClearSelection();
            }
            catch
            {
            }
        }

        #endregion
    }
}

<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PagingControl.ascx.cs"
    Inherits="lntechNew.WebControls.PagingControl" %> 
<link href="../Styles.css" type="text/css" rel="stylesheet">
<table border="0" cellpadding="0" cellspacing="0" style="width: 500px">
    <tr>
        <td align="right" style='display: <%= (IsRecordsSizeVisible ? "":"none") %>'>
            <asp:Label ID="lblNoOfRecords" runat="server" CssClass="form-label" Text="No. of Records : "></asp:Label>&nbsp;
        </td>
        <td align="right" style='width: 65px; display: <%= (IsRecordsSizeVisible ? "":"none") %>'>
            <asp:DropDownList ID="DL_Records" runat="server" AutoPostBack="True" CssClass="form-control inline-textbox"
                Width="70px" OnSelectedIndexChanged="DL_Records_SelectedIndexChanged">
                
                <%--Yasir Kamal 5555 03/03/2009 Allow user to select 10, 20, 30, 40, 50, 100 and All--%>
                <asp:ListItem Text="All" Value="0"></asp:ListItem>
                <asp:ListItem Text="10" Value="10"></asp:ListItem>
                <asp:ListItem Text="20" Value="20" Selected="True"></asp:ListItem>
                <asp:ListItem Text="30" Value="30"></asp:ListItem>
                <asp:ListItem Text="40" Value="40"></asp:ListItem>
                <asp:ListItem Text="50" Value="50"></asp:ListItem>
                <asp:ListItem Text="100" Value="100"></asp:ListItem>
                
                <%--5555 end--%>
                
            </asp:DropDownList>
        </td>
        <td align="right" style='width: 60px; display: <%= (IsRecordsSizeVisible ? "":"none") %>'>
            <asp:Label ID="lblPerPage" runat="server" CssClass="form-label" Text="Per Page"></asp:Label>&nbsp;
        </td>
        <td align="right" style='<%=(IsRecordsSizeVisible ? "width: 100px;":"") %> display: <%= (IsPagingVisible ? "":"none") %>'>
            <asp:Label ID="lblCurrentPage" runat="server" CssClass="clssubhead" Text="Current Page : "></asp:Label>&nbsp;
        </td>
        <td align="right" style='width: 30px; display: <%= (IsPagingVisible ? "":"none") %>'>
            <asp:Label ID="LB_curr" runat="server" CssClass="form-label"></asp:Label>&nbsp;
        </td>
        <td align="right" style='width: 50px; display: <%= (IsPagingVisible ? "":"none") %>'>
            <asp:Label ID="lblGoTo" runat="server" CssClass="form-label" Text="Go To :"></asp:Label>&nbsp;
        </td>
        <td align="right" style='width: 65px; display: <%= (IsPagingVisible ? "":"none") %>'>
            <asp:DropDownList ID="DL_pages" runat="server" AutoPostBack="True" CssClass="form-control inline-textbox"
                OnSelectedIndexChanged="DL_pages_SelectedIndexChanged" Width="61px">
            </asp:DropDownList>
        </td>
    </tr>
</table>

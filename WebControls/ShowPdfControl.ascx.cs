﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using lntechNew.Components;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;
using lntechNew.Components.ClientInfo;
using System.Net;

namespace lntechNew.WebControls
{
    public partial class ShowPdfControl : System.Web.UI.UserControl
    {
        #region variables

        int ticketid;
        int empid;
        int lettertype;

        #endregion

        #region Setting Property
        public int Ticketid
        {
            get { return ticketid; }
            set { ticketid = value; }
        }
       

        public int Empid
        {
            get { return empid; }
            set { empid = value; }
        }
       

        public int Lettertype
        {
            get { return lettertype; }
            set { lettertype = value; }
        }

        #endregion

        #region events
        
        protected void Page_Load(object sender, EventArgs e)
        {
            getreport();            
        }

        #endregion

        #region Methods
        public void getreport()
        {
            string filepath;          
            //clsCrsytalComponent Cr = new clsCrsytalComponent();
            //string[] key = {"@ticketid","@empid"};
            //object[] value1 = { ticketid, empid };
            //string filename;
            //if (lettertype == 6)
            //{
            //    filename = Server.MapPath("") + "\\Wordreport.rpt";
            //    filepath=Cr.CreateReport(filename, "USP_HTS_LETTER_OF_REP", key, value1, "false", lettertype, empid, this.Session, this.Response);
            //    ReadPdfFile(filepath);
            //}
            //else if (lettertype == 7)
            //{
            //    filename = Server.MapPath("") + "\\Motion_Limine.rpt";
            //    filepath=Cr.CreateReport(filename, "USP_HTS_Motion_Limine", key, value1, "false", lettertype, empid, this.Session, this.Response);
            //}
            //else if (lettertype == 8)
            //{
            //    filename = Server.MapPath("") + "\\Motion_Discovery.rpt";
            //    filepath=Cr.CreateReport(filename, "USP_HTS_Motion_Discovery", key, value1, "false", lettertype, empid, this.Session, this.Response);
            //}

        }

        private void ReadPdfFile(string path)
        {
           // string path = @"C:\Swift3D.pdf";
            //WebClient client = new WebClient();
            //Byte[] buffer = client.DownloadData(path);

            //if (buffer != null)
            //{
//                Response.ContentType = "application/pdf";
//                Response.AddHeader("content-length", buffer.Length.ToString());
////                Response.BinaryWrite(buffer);
//                Response.WriteFile(path);
                div_content.Attributes.Add("src", path);

            //}

        }


        #endregion
    }
}
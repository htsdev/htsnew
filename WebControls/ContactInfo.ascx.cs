﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using HTP.Components.ClientInfo;
using lntechNew.Components.ClientInfo;

namespace HTP.WebControls
{
    //Created By Waqas 5771 04/20/2009
    public partial class ContactInfo : System.Web.UI.UserControl
    {
        public event PageMethodHandler ContactPageMethod;

        #region Properties
        public int TicketID
        {
            set
            {
                hdnTicketID.Value = value.ToString();
            }
        }
        clsContact clscontact;
        clsSession ClsSession = new clsSession();
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            int Empid = 0;
            if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false || (!int.TryParse(ClsSession.GetCookie("sEmpID", this.Request), out Empid)))
            {
                Response.Redirect("../frmlogin.aspx", false);
            }
            else
            {

            }
        }
        protected void btnDisAssociate_Click(object sender, EventArgs e)
        {
            int Empid = 0;
            int.TryParse(ClsSession.GetCookie("sEmpID", this.Request), out Empid); 
            clscontact = new clsContact();
            clscontact.DisassociateContactID(Convert.ToInt32(hdnTicketID.Value), Empid);

             if (ContactPageMethod != null)
                 ContactPageMethod();

        }

        #endregion

        #region Methods
        /// <summary>
        /// To bind with CID Control and show Contact information
        /// Mode 1 : For Matter Page.
        /// Mode 2 : For History Page.
        /// </summary>
        /// <param name="ContactID"></param>
        /// <param name="PanelID"></param>
        /// <param name="Mode"></param>
        /// 
        public void BindContactInfo(string ContactID, string PanelID, int Mode, int activeflag)
        {
            //Primary user (Admin) can disassociate contact only.
            // tahir 6207 07/22/2009 allow all users to disassociate CID.
            //if (activeflag == 1 && ClsSession.GetCookie("sAccessType", this.Request).ToString() != "2")
            //{
            //    btnDisAssociate.Attributes.Add("OnClick", "return ConfirmClient();");
            //}
            //else
            //{
                btnDisAssociate.Attributes.Add("OnClick", "return ConfirmDisAssociation();");
            //}
            
            if (Mode == 1)
            {
                btnClose.Visible = false;                   
            }
            else if (Mode == 2)
            {
                btnDisAssociate.Visible = false;
                btnCancel.Visible = false;
            }

            clscontact = new clsContact();
            if (ContactID != null || ContactID != string.Empty)
            {
                clscontact.GetContactInfo(Convert.ToInt32(ContactID));
            }

            if (ContactID.ToString() != "")
            {
                lblCID.Text = ContactID.ToString();
            }
            else
            {
                lblCID.Text  = "N/A";
            }
            if (clscontact.FirstName != "")
            {
                lblFirstName.Text = clscontact.FirstName;
            }
            else
            {
                lblFirstName.Text = "N/A";
            }
            if (clscontact.MiddleName != "")
            {
                lblMiddleName.Text = clscontact.MiddleName;
            }
            else
            {
                lblMiddleName.Text = "N/A";
            }
            if (clscontact.LastName != "")
            {
                lblLastName.Text = clscontact.LastName;
            }
            else
            {
                lblLastName.Text = "N/A";
            }

            if (clscontact.DLNumber != "")
            {
                lblDLNumber.Text = clscontact.DLNumber;
            }
            else
            {
                lblDLNumber.Text = "N/A";
            }
            if (clscontact.DLState != "")
            {
                lblDLState.Text = clscontact.DLState;
            }
            else
            {
                lblDLState.Text = "N/A";
            }
            if (clscontact.Address.Trim() != "")
            {
                lblAddress.Text = clscontact.Address;
            }
            else
            {
                lblAddress.Text = "N/A";
            }
            if (clscontact.CDLFlag != "")
            {
                lblCDLFlag.Text = clscontact.CDLFlag;
            }
            else
            {
                lblCDLFlag.Text = "N/A";
            }

            if (clscontact.Language != "")
            {
                lblLanguage.Text = clscontact.Language;
            }
            else
            {
                lblLanguage.Text = "N/A";
            }
            if (clscontact.Race != "")
            {
                lblRace.Text = clscontact.Race;
            }
            else
            {
                lblRace.Text = "N/A";
            }
            if (clscontact.Gender != "")
            {
                lblGenderRace.Text = clscontact.Gender ;
            }
            else
            {
                lblGenderRace.Text = "N/A";
            }
            if (clscontact.HairColor != "")
            {
                lblHair.Text = clscontact.HairColor;
            }
            else
            {
                lblHair.Text = "N/A";
            }

            if (clscontact.Height != "")
            {
                string height = clscontact.Height;
                if (height != "" && height.StartsWith("0") == false)
                {
                    try
                    {
                        string h = "";
                        for (int i = 0; i < height.Length; i++)
                        {
                            if (height[i] != ' ')
                                h += height[i];
                        }
                        height = h;
                        if (height != "")
                        {
                            if (height.Contains("ft") == true)
                            {
                                if (height.Replace("ft", "").Trim() != "")
                                {
                                    lblHeight.Text = height.Substring(0, height.IndexOf("ft")).Trim() + "' ";
                                }
                                if (height.Substring(height.IndexOf("ft") + 2).Trim() != "")
                                {
                                    lblHeight.Text += height.Substring(height.IndexOf("ft") + 2).Trim() + "''";
                                }
                            }
                            else
                                lblHeight.Text = height.Trim();
                        }

                    }
                    catch (Exception ex)
                    {
                        lblHeight.Text = clscontact.Height;
                    }
                }
            }
            else 
            {
                lblHeight.Text = "N/A";
            }

            if (lblHeight.Text == "")
            {
                lblHeight.Text = "N/A";
            }

            if (clscontact.Weight != "")
            {
                lblWeight.Text = clscontact.Weight;
            }
            else
            {
                lblWeight.Text = "N/A";
            }
            if (clscontact.Email != "")
            {
                lblEmail.Text = clscontact.Email;
            }
            else
            {
                lblEmail.Text = "N/A";
            }
            if (clscontact.Language != "")
            {
                lblLanguage.Text = clscontact.Language;
            }
            else
            {
                lblLanguage.Text= "N/A";
            }
            if (clscontact.Phone1 != "")
            {
                lblPhone1.Text = clscontact.Phone1;
            }
            else
            {
                lblPhone1.Text = "N/A";
            }
            if (clscontact.Phone2 != "")
            {
                lblPhone2.Text = clscontact.Phone2;
            }
            else
            {
                lblPhone2.Text = "N/A";
            }
            if (clscontact.Phone3 != "")
            {
                lblPhone3.Text = clscontact.Phone3;
            }
            else
            {
                lblPhone3.Text = "N/A";
            }
            if (clscontact.Eyes != "")
            {
                lblEye.Text = clscontact.Eyes;
            }
            else
            {
                lblEye.Text= "N/A";
            }
            if (clscontact.MidNumber != "")
            {
                lblMidNumber.Text = clscontact.MidNumber;
            }
            else
            {
                lblMidNumber.Text= "N/A";
            }
            if (clscontact.DOB != "")
            {
                lblDOB.Text = clscontact.DOB;
            }
            else
            {
                lblDOB.Text = "N/A";
            }

            

            btnCancel.Attributes.Remove("onClick");
            lbtn_close.Attributes.Remove("onClick");

            btnCancel.Attributes.Add("onClick", "return closeModalPopup('" + PanelID + "');");
            lbtn_close.Attributes.Add("onClick", "return closeModalPopup('" + PanelID + "');");
            pnl_CID.Attributes.Add("style", "display:block");
        }

        #endregion


    }
}